# MyTower

## Wiki

http://gitlab.adias.fr/root/mytower-generique/wikis/home

## Launch app for debugging from terminal

### Backend

Run the following command on ROOT folder `gradlew clean bootRun`

### Frontend

You can use `npm start` command on the frontend module as usual (after installed Node.JS and `npm install`)

Or you can run the gradle wrapper to download a Node.JS version for the project and run the frontend automatically
Run the following command on ROOT folder `gradlew frontStart`

## Build war for a (test|validation|production) environment

War are now environment specific as requested in Jira task [MTG-165](http://jira.adias.fr/browse/MTG-165)

4 build tasks are available, you SHOULD run them from ROOT folder and from a terminal

- `gradlew clean warProd` -> Production
- `gradlew clean warRecetteClient` -> Customer recipe
- `gradlew clean warDev` -> Dev
- `gradlew clean warRecetteInterne` -> Internal recipe

The war is generated in forder `backend/build/libs/`. The war name include version and environment.

## Develop using Eclipse

Please use at least **Spring Tools Suite 4+** : https://spring.io/tools4
Not tested with Spring Tools Suite 3.9.x
Also not tested with Eclipse for Java EE and Spring Tools Plugin

### Import project

If you have the old Maven build already integrated in your Eclipse workspace, you will need to delete all related projects firts (don't delete from disk of course)

You must have the official Gradle plugin installed on your Eclipse.
Install it from marketplace **Buildship Gradle Integration** : https://projects.eclipse.org/projects/tools.buildship

After :

- Restart Eclipse
- Important ! Run the command `gradlew clean bootRun` ont the root folder BEFORE importing to Eclipse
- File -> Import
- Gradle -> Existing Gradle project
- Select the ROOT folder
- Select use Gradle Wrapper

### Run/Debug

Create a Run/Debug configuration of king Spring Boot App
Select profile dev

Don't simply launch by Right Click > Run as or it will don't select the dev profiles just the default

### Rebuild QueryDSL

You should need to rebuild QueryDSL when someone change something on entities.
It can be required after a pull or a checkout

- Stop currently running Spring Boot in Eclipse or Gradle
- Run the command `gradlew clean bootRun` to regenerate QueryDSL
- Right Click on backend project and click Refresh (The project refresh or hotkey F5, not the Gradle Refresh)

### Refresh Gradle dependencies on Eclipse

You should need to rebuild QueryDSL when someone change something on build.gradle.
It can be required after a pull or a checkout

- Stop currently running Spring Boot in Eclipse or Gradle
- Right Click on backend project and click Refresh (The project refresh or hotkey F5, not the Gradle Refresh)
- Right Click on backend project and click Gradle > Refresh Gradle Project

## Code Format

The code formatter is indispensable to work on MyTower Generique
Please folow this documentation http://gitlab.adias.fr/root/mytower-generique/wikis/Developpers/Tools/Formatage-du-code
