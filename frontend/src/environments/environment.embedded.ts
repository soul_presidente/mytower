import { environmentCommon } from "./environment.common";

export const environment = {
	tokenVersion: environmentCommon.tokenVersion,
	supportMail: "support@alis-intl.com",
	production: getEnvValue("frt_env_modeprod", true) === "true",
	apiUrl: getEnvValue("frt_env_apiurl", true),
	jwtWhitelist: getEnvValue("frt_env_whitelist", true).split("|"),

};

export function getCookie(name: string): string {
	const nameLenPlus = name.length + 1;
	return (
		document.cookie
			.split(";")
			.map((c) => c.trim())
			.filter((cookie) => {
				return cookie.substring(0, nameLenPlus) === `${name}=`;
			})
			.map((cookie) => {
				return decodeURIComponent(cookie.substring(nameLenPlus));
			})[0] || null
	);
}

export function getEnvValue(key: string, required: boolean = false): string {
	let value = getCookie(key);
	if (value == null && required) {
		throw "Can't load environment value. Missing key : " + key;
	}
	return value;
}
