/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
	id: string;
}
// to be able to use require
// code commenté pour fixer le problème de duplication de type déclaré "require" avec global.d.ts
//declare function require(name: string);

// to be able to import json files
declare module "*.json" {
	const value: any;
	export default value;
}
