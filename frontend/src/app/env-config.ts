import { environment } from "../environments/environment";

export class EnvConfig {
	public static hostename: string = window.location.hostname;
	public static urlRecetteInterne: string = "http://mytower-easy.adias.fr"; //url de recette utilisé en interne
	public static urlRecetteClient: string = "http://mytower.adias.fr"; //url de recette client
	public static urlLocalhost: string = "http://localhost:8080"; //url de recette client
	public static urlProd: string = "http://mytower.adias.fr"; //url de recette client

	public static isRecetteClient() {
		return window.location.hostname.indexOf(EnvConfig.urlRecetteClient.substring(7)) > -1;
	}

	public static isRecetteInterne() {
		return window.location.hostname.indexOf(EnvConfig.urlRecetteInterne.substring(7)) > -1;
	}

	public static isProd() {
		return !this.isRecetteClient() && !this.isRecetteInterne();
	}

	public static getCurrentUrl() {
		if (!environment.production) {
			return EnvConfig.urlLocalhost;
		}
		if (EnvConfig.isRecetteInterne()) {
			return EnvConfig.urlRecetteInterne;
		}
		if (EnvConfig.isRecetteClient()) {
			return EnvConfig.urlRecetteClient;
		}

		//par defaut on retourne l'url de la prod
		return EnvConfig.urlProd;
	}
}
