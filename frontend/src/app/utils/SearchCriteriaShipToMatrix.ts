import { SearchCriteria } from "./searchCriteria";

export class SearchCriteriaShipToMatrix extends SearchCriteria {
	criteria: string;
	typeUnitCode: string;
	ebUnitNum: Number;
}
