import { SearchCriteria } from './searchCriteria';
import { EbCompagnie } from '@app/classes/compagnie';
import { EbTtCompanyPsl } from '@app/classes/ttCompanyPsl';
import { EbTtCategorieDeviation } from '@app/classes/categorieDeviation';

export class SearchCriteriaMappingCodePsl extends SearchCriteria{
    ediMapPslNum: number;
    dateCreationSys: Date;
    dateMajSys: Date;
    codePslNormalise: String;
    codePslTransporteur: String;
    typeEvent: number;
    xEbCompany: EbCompagnie;
    xEbCompanyTransporteur: EbCompagnie;
    xEbCompanyPsl: EbTtCompanyPsl;
    numCodeTransporteur: number;
    numCodeRaison: number;
    codePslTransporteurDescriptif: String;
    xEbCategorieDeviation: EbTtCategorieDeviation;
    codeRaison: String;
    categorieDeviationLibelle: String;
    codeRaisonDescriptif: String;

    listCodePslTransporteur: string[];
	listCodePslNormalise: string[];
    listTypeEvent: number[];
	listCodeRaison: string[];
	listCategorieDeviationLibelle: string[];
	listCodePslTransporteurDescriptif: string[];
	listCodeRaisonDescriptif: string[];

	constructor() {
        super();
		this.ediMapPslNum = null;
        this.dateCreationSys = new Date();
        this.dateMajSys = new Date();
        this.codePslNormalise = null;
        this.codePslTransporteur = null;
        this.typeEvent = null;
        this.xEbCompany = null;
        this.xEbCompanyTransporteur = null;
        this.xEbCompanyPsl = null;
        this.numCodeTransporteur = null;
        this.numCodeRaison = null;
        this.codePslTransporteurDescriptif = null;
        this.xEbCategorieDeviation = null;
        this.codeRaison = null;
        this.categorieDeviationLibelle = null;
        this.codeRaisonDescriptif = null;

        this.listCodePslTransporteur = null;
        this.listCodePslNormalise = null;
        this.listTypeEvent = null;
        this.listCodeRaison = null;
        this.listCategorieDeviationLibelle = null;
        this.listCodePslTransporteurDescriptif = null;
		this.listCodeRaisonDescriptif = null;
    }
    
    constructorCopy(advancedOptions: SearchCriteriaMappingCodePsl) {
		super.constructorCopy(advancedOptions);

        this.codePslNormalise = advancedOptions.codePslNormalise;
        this.codePslTransporteur = advancedOptions.codePslTransporteur;
        this.typeEvent = advancedOptions.typeEvent;
        this.numCodeTransporteur = advancedOptions.numCodeTransporteur;
        this.numCodeRaison = advancedOptions.numCodeRaison;
        this.codePslTransporteurDescriptif = advancedOptions.codePslTransporteurDescriptif;
        this.codeRaison = advancedOptions.codeRaison;
        this.categorieDeviationLibelle = advancedOptions.categorieDeviationLibelle;
        this.codeRaisonDescriptif = advancedOptions.codeRaisonDescriptif;

        this.listCodePslTransporteur = advancedOptions.listCodePslTransporteur;
        this.listCodePslNormalise = advancedOptions.listCodePslNormalise;
        this.listTypeEvent = advancedOptions.listTypeEvent;
        this.listCodeRaison = advancedOptions.listCodeRaison;
        this.listCategorieDeviationLibelle = advancedOptions.listCategorieDeviationLibelle;
        this.listCodePslTransporteurDescriptif = advancedOptions.listCodePslTransporteurDescriptif;
        this.listCodeRaisonDescriptif = advancedOptions.listCodeRaisonDescriptif;
        
	}

}