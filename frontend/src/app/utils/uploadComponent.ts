import { UploadFile, UploadInput, UploadOutput, NgUploaderService } from "ngx-uploader";

import { UploadDirectory } from "./enumeration";

import { DomSanitizer } from "@angular/platform-browser";
import { EventEmitter } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { Statique } from "@app/utils/statique";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import SingletonStatique from "./SingletonStatique";
import { EcMimetype } from "@app/classes/EcMimetype";
import { FichierJoint } from "@app/classes/fichiersJoint";
import {TOKEN_NAME} from "@app/services/auth.constant";

export class UploadComponent extends ConnectedUserComponent {
	imageToShow: any = null;
	files: UploadFile[] = [];
	uploadInput: EventEmitter<UploadInput> = new EventEmitter<UploadInput>();
	dragOver: boolean;
	componentObject: any;
	UploadDirectory = UploadDirectory;
	isGallery: Boolean = false;
	isUpdate: Boolean = null;
	contentTypes: any;
	ngUploaderService: NgUploaderService;
	documentFiles: Array<Array<UploadFile>> = new Array<Array<UploadFile>>();
	filesReturn: any = {}; // Anonymous object that will be used as dictionnary of property each contains Array<FichierJoint>
	fileTypeDocumentNum: string;
	isFileNotToUpload: Boolean = false;
	constructor(
		protected sanitizer?: DomSanitizer,
		protected translate?: TranslateService,
		protected modalService?: ModalService
	) {
		super();
		this.ngUploaderService = new NgUploaderService();
		this.contentTypes = new Array<string>();
		SingletonStatique.getListFilsFormats().subscribe((data: Array<EcMimetype>) => {
			for (let item of data) {
				this.contentTypes.push(item.mimetype);
			}
			this.ngUploaderService.setContentTypes(this.contentTypes);
		});
	}

	fileMaxSizeError() {
		let title = null;
		let body = null;
		this.translate.get("MODAL.ERROR").subscribe((res: string) => {
			title = res;
		});
		this.translate.get("MODAL.SIZE_ALLOWED_5MB").subscribe((res: string) => {
			body = res;
		});
		this.modalService.information(title, body, null);
	}

	fileSizeZeroError() {
		let title = null;
		let body = null;
		this.translate.get("MODAL.ERROR").subscribe((res: string) => {
			title = res;
		});
		this.translate.get("MODAL.SIZE_ZERO").subscribe((res: string) => {
			body = res;
		});
		this.modalService.information(title, body, null);
	}

	fileTypeError() {
		let body = this.translate.instant("MimeType.MESSAGE");
		body =
			"<div class='text-left padding-left-10 padding-bottom-6 bold fz-16'> " + body + " </div>";

		let title = this.translate.instant("MODAL.DOCUMENT_TYPE");

		SingletonStatique.getListFilsFormats().subscribe((data: Array<EcMimetype>) => {
			for (let item of data) {
				let text = this.translate.instant("MimeType." + item.description);
				text = "<div class='text-left padding-left-10 padding-bottom-4'>- " + text + "</div>";
				if (body.indexOf(text) < 0) body += text;
			}

			this.modalService.template(title, body, null);
		});
	}

	// Même j'ai laissé ce code en commentaire on peut réutiliser s'il génère un problème au niveau upload
	// sur les dashboards

	/* onUploadOutput(output: UploadOutput): void {
		// https://stackoverflow.com/a/5515349/6079771
		if (output.file && !this.ngUploaderService.isContentTypeAllowed(output.file.type)) {
			this.fileTypeError();
			this.isFileNotToUpload = true;
			output.file.nativeFile = null;
			return;
		} else {
			if (output.type === "allAddedToQueue") {
				this.allAddedToQueue();
			} else if (output.type === "addedToQueue") {
				this.addedToQueue(output);
			} else if (output.type === "dragOver") {
				this.dragOver = true;
			} else if (output.type === "dragOut") {
				this.dragOver = false;
			} else if (output.type === "drop") {
				this.dragOver = false;
			}
			if (output.file) {
				if (output.type === "uploading") {
					const index = this.files.findIndex((file) => file.id === output.file.id);
					this.files[index] = output.file;
					this.onUploadProgress(output.file);
				} else if (output.type === "done") {
					this.onUploadComplete(output.file);
					this.onUploadProgress(null);
				} else if (output.type === "removed") {
					this.files = this.files.filter((file: UploadFile) => file !== output.file);
				}
			}
		}
	} */

	onUploadOutput(output: UploadOutput): void {
		let fileTypeDocument = this.documentFiles[this.fileTypeDocumentNum];

		let fileType: string = "";
		if (output.file != null && output.file.type != null) {
			// Prevent false result if the mimetype is suffixed (like ;Charset)
			fileType = output.file.type.split(";")[0];
		}

		if (output.file && !this.ngUploaderService.isContentTypeAllowed(fileType)) {
			this.componentObject.fileTypeError();
			this.isFileNotToUpload = true;
			output.file.nativeFile = null;
			return;
		}
		// https://stackoverflow.com/a/5515349/6079771
		if (output.file || output.type === "allAddedToQueue") {
			if (output.type !== "allAddedToQueue" && output.file.size > Statique.$5MbToBytes) {
				if (!this.isFileNotToUpload) {
					this.componentObject.fileMaxSizeError();
					this.isFileNotToUpload = true;
					output.file.nativeFile = null;
					return;
				}
			} else {
				this.isFileNotToUpload = false;
				switch (output.type) {
					case "allAddedToQueue":
						this.allAddedToQueue();
						if (this.isFileNotToUpload) {
							this.isFileNotToUpload = false;
						}
						break;
					case "addedToQueue":
						this.addedToQueue(output);
						break;
					case "uploading":
						const index = fileTypeDocument.findIndex((file) => file.id === output.file.id);
						fileTypeDocument[index] = output.file;
						break;
					case "done":
						fileTypeDocument.forEach((file) => {
							this.filesReturn[this.fileTypeDocumentNum] = [
								...this.filesReturn[this.fileTypeDocumentNum],
								...file.response.map((it) => it.fichier),
							];
							this.documentFiles[this.fileTypeDocumentNum] = new Array<UploadFile>();

							this.onUploadComplete(file);
						});
						if (output.file.size == Statique.$0MbToBytes) {
							this.componentObject.fileSizeZeroError();
						}
						break;
					case "removed":
						fileTypeDocument = fileTypeDocument.filter((file: UploadFile) => file !== output.file);
						break;
					case "dragOver":
						this.dragOver = true;
						break;
					case "dragOut":
						this.dragOver = false;
						break;
					case "drop":
						this.dragOver = false;
						break;
					case "start":
						setTimeout(() => {
							if (this.documentFiles[this.fileTypeDocumentNum].length != 0) {
								if (output.file.response) {
									output.type = "done";
								} else {
									output.type = "start";
								}
								this.onUploadOutput(output);
							}
						}, 500);
						break;
					default:
						break;
				}
			}
		}
	}

	onUploadProgress(file: UploadFile) {}

	emitUploadInputEvent(endPoint: string) {
		const slash = "/";
		const authHeader = localStorage.getItem(TOKEN_NAME);
		const event: UploadInput = {
			type: "uploadAll",
			url: slash + Statique.controllerUpload + slash + endPoint,
			method: "POST",
			data: this.componentObject.dataUpload,
			headers: {
				Authorization : "Bearer " + authHeader
	}
		};
		this.uploadInput.emit(event);
	}

	allAddedToQueue() {
		switch (this.componentObject.dataUpload.typeFile.toString()) {
			case UploadDirectory.BULK_IMPORT.toString():
				this.emitUploadInputEvent("upload-file-import");
				break;
			case UploadDirectory.FREIGHT_AUDIT.toString():
				this.emitUploadInputEvent("upload-file-invoice");
				break;
			case UploadDirectory.PRICING_BOOKING.toString():
				this.emitUploadInputEvent("upload-file");
				break;
			case UploadDirectory.LOGO.toString():
				if (this.isUpdate) this.emitUploadInputEvent("upload-file");
				break;
			default:
				break;
		}
	}

	addedToQueue(output: UploadOutput) {
		// let fileType = this.documentFiles[this.fileTypeDocumentNum];
		// fileType.push(output.file);
		this.files.push(output.file);
		switch (this.componentObject.dataUpload.typeFile) {
			case UploadDirectory.LOGO:
				if (!this.isGallery) this.createImageFromBlob(output.file.nativeFile);
				break;
			// todo
			case UploadDirectory.PRICING_BOOKING:
				break;
			default:
				break;
		}
	}

	startUpload(): void {
		const event: UploadInput = {
			type: "uploadAll",
			url: "/api/upload/upload-file",
			method: "POST",
			data: this.componentObject.dataUpload,
		};

		this.uploadInput.emit(event);
	}

	createImageFromBlob(image: any) {
		let reader = new FileReader();
		reader.addEventListener(
			"load",
			() => {
				this.imageToShow = this.sanitizer.bypassSecurityTrustResourceUrl(reader.result as string);
			},
			false
		);

		if (image) {
			reader.readAsDataURL(image);
		}
	}

	onUploadComplete(file: UploadFile) {}

	cancelUpload(id: string): void {
		this.uploadInput.emit({ type: "cancel", id: id });
	}

	removeFile(id: string): void {
		this.uploadInput.emit({ type: "remove", id: id });
	}

	removeAllFiles(): void {
		this.uploadInput.emit({ type: "removeAll" });
	}
}
