import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
	name: "numberFormat",
})
export class NumberFormatPipe implements PipeTransform {
	transform(input: number, args?: any): any {
		var exp;

		if (Number.isNaN(input)) {
			return null;
		}

		if (input < 1000) {
			return input;
		}

		exp = Math.floor(Math.log(input) / Math.log(1000));

		let result = input.toString();

		let index = 0;
		let count = 0;

		for (let i = 1; i <= exp; i++) {
			index = result.length - (i * 3 + count);
			result = result.slice(0, index) + " " + result.slice(index);
			count++;
		}

		return result;
	}
}
