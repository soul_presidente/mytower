import { SearchCriteria } from "./searchCriteria";
import { Statique } from "@app/utils/statique";

export class SearchCriteriaPricingBooking extends SearchCriteria {
	listTypeDemandePlugin: Array<any>;
	listTypeRegionPlugin: Array<any>;

	listTypeDemande: Array<number>;
	listTypeRegion: Array<number>;
	listQrGroupe: Array<number>;
	listQrGroupePlugin: Array<any>;

	dateOfGoodsAvailabilityPlugin: String;
	dateOfGoodsAvailability: String;

	listModeTransportPlugin: Array<any>;
	listModeTransport: Array<number>;

	listNumOrderSAP: Array<number>;
	listNumOrderEDI: Array<any>;
	listNumOrderCustomer: Array<any>;
	listCampaignCode: Array<any>;
	listCampaignName: Array<any>;
	listReferenceDelivery: Array<any>;
	listRefCustomerDelivery: Array<any>;
	listNameCustomerDelivery: Array<any>;
	requestedDeliveryDateFrom: String;
	requestedDeliveryDateTo: String;
	listUnitReference: Array<any>;
	listLot: Array<any>;

	originCity: String;
	destinationCity: String;

	constructor() {
		super();
		this.listTypeDemande = new Array<number>();
		this.listTypeRegion = new Array<number>();
		this.listQrGroupe = Array<number>();
		this.listQrGroupePlugin = Array<any>();

		this.dateOfGoodsAvailabilityPlugin = null;
		this.dateOfGoodsAvailability = null;

		this.listModeTransport = new Array<number>();
		this.listModeTransportPlugin = new Array<any>();

		this.listNumOrderSAP = new Array<any>();
		this.listNumOrderEDI = new Array<any>();
		this.listNumOrderCustomer = new Array<any>();
		this.listCampaignCode = new Array<any>();
		this.listCampaignName = new Array<any>();
		this.listReferenceDelivery = new Array<any>();
		this.listRefCustomerDelivery = new Array<any>();
		this.listNameCustomerDelivery = new Array<any>();
		this.requestedDeliveryDateFrom = null;
		this.requestedDeliveryDateTo = null;
		this.listUnitReference = new Array<any>();
		this.listLot = new Array<any>();
		this.originCity = null;
		this.destinationCity = null;
	}

	constructorCopy(advancedOptions: SearchCriteriaPricingBooking) {
		super.constructorCopy(advancedOptions);

		this.listTypeDemandePlugin = advancedOptions.listTypeDemandePlugin;
		this.listQrGroupePlugin = advancedOptions.listQrGroupe;
		this.listTypeRegionPlugin = advancedOptions.listTypeRegionPlugin;
		this.listModeTransportPlugin = advancedOptions.listModeTransportPlugin;

		advancedOptions.listTypeDemandePlugin.forEach((typeDemande, index) => {
			this.listTypeDemande.push(typeDemande.code);
		});

		if (advancedOptions.listQrGroupePlugin) {
			advancedOptions.listQrGroupePlugin.forEach((EbQrGroupe, index) => {
				this.listQrGroupe.push(EbQrGroupe.ebQrGroupeNum);
			});
		}
		if (advancedOptions.listModeTransportPlugin) {
			advancedOptions.listModeTransportPlugin.forEach((listModeTransport, index) => {
				this.listModeTransport.push(listModeTransport.code);
			});
		}

		this.dateOfGoodsAvailabilityPlugin = advancedOptions.dateOfGoodsAvailability;
		this.dateOfGoodsAvailability = advancedOptions.dateOfGoodsAvailability;

		this.listNumOrderSAP = advancedOptions.listNumOrderSAP;
		this.listNumOrderEDI = advancedOptions.listNumOrderEDI;
		this.listNumOrderCustomer = advancedOptions.listNumOrderCustomer;
		this.listCampaignCode = advancedOptions.listCampaignCode;
		this.listCampaignName = advancedOptions.listCampaignName;
		this.listReferenceDelivery = advancedOptions.listReferenceDelivery;
		this.listRefCustomerDelivery = advancedOptions.listRefCustomerDelivery;
		this.listNameCustomerDelivery = advancedOptions.listNameCustomerDelivery;
		this.requestedDeliveryDateFrom = advancedOptions.requestedDeliveryDateFrom;
		this.requestedDeliveryDateTo = advancedOptions.requestedDeliveryDateTo;
		this.listUnitReference = advancedOptions.listUnitReference;
		this.listLot = advancedOptions.listLot;

		this.originCity = advancedOptions.originCity;
		this.destinationCity = advancedOptions.destinationCity;
	}

	formatDates() {
		super.formatDates();

		if (this.dateOfGoodsAvailabilityPlugin != null)
			this.dateOfGoodsAvailability = Statique.formatDateFromDatePicker(
				this.dateOfGoodsAvailabilityPlugin
			);

		return this;
	}
}
