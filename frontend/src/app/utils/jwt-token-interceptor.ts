import { Injectable } from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import { TOKEN_NAME } from "@app/services/auth.constant";
import { AuthenticationService } from '@app/services/authentication.service';

@Injectable()
export class JwtTokenInterceptor implements HttpInterceptor {
	constructor(private authService: AuthenticationService) {}

	intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		let authHeader = localStorage.getItem(TOKEN_NAME);
		let urlsToFilter = ["api/list/countries", "api/inscription"];

		urlsToFilter.some((it) => req.url.indexOf(it) >= 0) && (authHeader = null);
		if (req.url.indexOf("api/list/countries") >= 0) authHeader = null;
		else if (req.url.indexOf("api/inscription") >= 0) authHeader = null;
		else if (req.url.indexOf("api/chanel") >= 0) authHeader = null;
		else if (req.url.indexOf("api/public") >= 0) authHeader = null;
		else if (req.url.indexOf("api/track-trace/list-track-smart") >= 0) authHeader = null;
		else if(!authHeader) {
			authHeader = this.authService.theConnectedUser;
		}
		if (authHeader) {
			const authReq = req.clone({
				headers: req.headers.set("Authorization", "Bearer " + authHeader),
			});
			return next.handle(authReq);
		}

		return next.handle(req);
	}
}
