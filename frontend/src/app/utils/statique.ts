import { EcCountry } from "@app/classes/country";
import {
	FlagIcon,
	ModeTransport,
	Modules,
	SavedFormIdentifier,
	ServiceType,
	GroupePropositionStatut,
} from "./enumeration";
import { environment } from "../../environments/environment";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-struct";
import { SerializationHelper } from "./serializationHelper";
import { NgbTimeStruct } from "@ng-bootstrap/ng-bootstrap";
import { EbEtablissement } from "@app/classes/etablissement";
import { TOKEN_NAME, WS_SESSION_ID } from "@app/services/auth.constant";
import { EbUser } from "@app/classes/user";
import { EcCurrency } from "@app/classes/currency";
import { EbDemande } from "@app/classes/demande";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { EbPlGrilleTransportDTO } from "@app/classes/trpl/EbPlGrilleTransportDTO";
import { EbEntrepotDTO } from "@app/classes/dock/EbEntrepotDTO";
import { EbDockDTO } from "@app/classes/dock/EbDockDTO";
import { EbTypeUnit } from "@app/classes/typeUnit";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { EbTtCompanyPsl } from "@app/classes/ttCompanyPsl";
import { EbQrGroupeProposition } from "@app/classes/qrGroupeProposition";
import { EbQrGroupe } from "@app/classes/qrGroupe";
import SingletonStatique from "./SingletonStatique";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { Router } from "@angular/router";
import { PrMtcTransporteur } from "@app/classes/PrMtcTransporteur";
let moment = require("moment");
import SockJS from "sockjs-client";
import { GenericEnum } from "@app/classes/GenericEnum";
import { EcFuseauxHoraire } from "@app/classes/fuseauHoraire";
import { EbCompagnie } from "@app/classes/compagnie";

export class Statique {
	public static timedOutInMin: number = 15;
	public static tokenVersion: number = environment.tokenVersion;
	private static urlBackEnd: String = environment.apiUrl;
	public static path: String = environment.apiUrl;
	// ----------------------- Url Controllers-------------------------------- //
	public static controllerDouane: string = "api/custom";
	public static controllerCommunity: string = "api/community";
	public static controllerComplianceMatrix: string = "api/compliance-matrix";
	static AUTH_TOKEN = environment.production ? Statique.urlBackEnd + "/auth" : "/auth";
	static KIBANA_AUTH = environment.production ? Statique.urlBackEnd + "/api/kibana" : "/api/kibana";

	public static hideSideBar: boolean;
	public static controllerDeliveryOverView: string = "api/delivery-overview";
	public static controllerOrderOverView: string = "api/order-overview";
	public static controllerShipToMatrix: string = "/api/shiptomatrix";
	public static controllerPricing: string = "api/pricing";
	public static controllerQualityManagement: string = "api/quality-management";
	public static controllerAccount: string = "/api/account";
	public static controllerInscription: string = "/api/inscription";
	public static controllerStatiqueListe: string = "/api/list";
	public static controllerEtablissement: string = "/api/etablissement";
	public static controllerEtablissementCurrency: string = "/api/etablissementCurrency";
	public static controllerDock: string = "/api/dock";
	public static controllerFlag: string = "/api/flag";
	public static controllerUserTag: string = "/api/user-tag";
	public static controllerComptaMatiere: string = "/api/compta-matiere";
	public static controllerBoutonAction: string = "/api/bouton-action";
	public static controllerCategorie: string = "/api/categorie";
	public static controllerExport: string = "/api/export";
	public static controllerSavedForm: string = "/api/savedform";
	public static controllerCompagnie: string = "/api/compagnie";
	public static controllerCostCenter: string = "/api/costcenter";
	public static controllerFreightAudit: string = "/api/freight-audit";
	public static controllerTrackTrace: string = "/api/track-trace";
	public static controllerTypeDocument: string = "/api/typeDocument";
	public static dataSourceFavoris: string = "api/account";
	public static controllerTransportationPlan: string = "api/transportation-plan";
	public static controllerAnalytics: string = "api/analytics";
	public static controllerDeclarationDouane: string = "/api/custom";
	public static controllerConfigCustom: string = "/api/custom-setting";
	public static controllerGenericTable: string = "/api/generic-table";
	public static controllerEmailHistorique: string = "api/email-historique";
	public static controllerThemesConfig: string = "/api/themes-config";
	public static controllerReceiptScheduling: string = "api/receipt-scheduling";
	public static controllerVehicule: string = "api/vehicule";
	public static controllerTypeUnit: string = "api/type-unit";
	public static controllerPublic: string = "api/public";
	public static controllerUnit: string = "api/unit";
	public static controllerCompagniePsl: string = "api/config-psl";
	public static controllerTypeFlux: string = "api/type-flux";
	public static controllerIncoterm: string = "api/incoterm";
	public static controllerDocumentTemplates: string = "api/document-templates";
	public static controllerTypeRequest: string = "api/type-request";
	public static controllerDataRecovery: string = "api/data-recovery";
	public static controllerTypeGoods: string = "api/type-goods";
	public static controllerWeekTemplate: string = "api/week-template";
	public static controllerPlanTransportNew: string = "api/plan-transport-new";
	public static controllerAcl: string = "api/acl";
	public static controllerPrGlobal: string = "api/pr-mtc";
	public static controllerAdditionalCosts: string = "api/additional-cost";
	public static controllerCostItems: string = "api/cost-item";

	public static controllerTemplatesLexique: string = "/app/user/settings/templates/lexique";

	public static controllerUserProfile: String = "api/user-profile";
	public static controllerUpload: String = "api/upload";

	public static controllerCronjob: String = "api/cronjob";
	public static controllerAdresse: String = "api/adresse";
	public static controllerQrConsolidation: String = "api/qr-consolidation";
	public static controllerMappingCodePsl: String = "api/mapping-code-psl";
	public static controllerControlRule: string = "api/control-rule";
	public static controllerNotification: string = "/api/notification";

	public static controllerBaseArticle: String = "api/partTable";

	// socket controllers
	public static controllerWebSocketPricing: string = "/pricing/edit";
	// MTC-MTG Mapping Controller
	public static controllerMtcMappingParam = "/api/v1/mtc/mapping-params";

	// ------------------------------------------------------------------------//
	public static parametrageDroitsUser: keyValue[] = [
		{ key: 1, value: "SETTINGS.ASSOCIER_A_UN_PROFIL" },
		{ key: 2, value: "SETTINGS.PERSONNALISER_LE_PARAMETRAGE" },
		{ key: 3, value: "SETTINGS.PERSONNALISER_A_PARTIR_D_UN_PROFIL" },
	];
	public static listJours: Map<number, String> = new Map([
		[1, "Mon."],
		[2, "Tue."],
		[3, "Wed."],
		[4, "Thu."],
		[5, "Fri."],
		[6, "Sat."],
		[7, "Sun."],
	]);
	public static civilite = ["Mr", "Mme"];
	public static yesOrNo: keyValue[] = [{ key: true, value: "Yes" }, { key: false, value: "No" }];
	public static majeurMineur: keyValue[] = [
		{ key: 1, value: "Majeur" },
		{ key: 2, value: "Mineur" },
	];
	public static yesOrNoWithNumberKey: keyValue[] = [
		{ key: 1, value: "Yes" },
		{ key: 0, value: "No" },
	];
	public static yesOrNoWithBooleanKey: keyValue[] = [
		{ key: true, value: "Yes" },
		{ key: false, value: "No" },
	];
	public static delaiRange: number[] = [1, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24];
	public static patternTime: string = "([01]?[0-9]|2[0-3]):[0-5][0-9]";
	public static maskTime: Array<string | RegExp> = [/\d/, /\d/, ":", /\d/, /\d/];
	public static patternSIRET: string = "[0-9]{14}";
	public static maskSIRET: Array<string | RegExp> = [
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
	];
	public static patternPhone: string = "";
	public static maskPhone: Array<string | RegExp> = [
		/\d/,
		/\d/,
		"-",
		/\d/,
		/\d/,
		"-",
		/\d/,
		/\d/,
		"-",
		/\d/,
		/\d/,
		"-",
		/\d/,
		/\d/,
	];
	public static maskLeadTime: Array<string | RegExp> = [
		/\d/,
		/\d/,
		":",
		/\d/,
		/\d/,
		":",
		/\d/,
		/\d/,
	];
	public static patterEmail: string =
		"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
	public static patternIBAN: string = "FR[0-9]{2}[0-9]{5}[0-9]{5}[A-Z0-9]{11}[0-9]{2}";
	public static patternReference: string = "^[a-zA-Z0-9._-]+$";
	public static maskIBAN: Array<string | RegExp> = [
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
		/[a-zA-Z0-9]/,
	];
	public static patternNumber: string = "[1-9][0-9]*";
	public static patternDecimalNumber: string = "[0-9]+(.[0-9]*)?";
	public static decimalPipePrecision: string = "1.2-6";
	public static days = ["Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam"];
	public static months = [
		"Janv",
		"Févr",
		"Mars",
		"Avril",
		"Mai",
		"Juin",
		"Juil",
		"Août",
		"Sept",
		"Oct",
		"Nov",
		"Déc",
	];
	public static $5MbToBytes: number = 5324800;
	public static listEtatHoraire: keyValue[] = [
		{ key: 0, value: "All Days" },
		{ key: 1, value: "Customise" },
	];
	public static daysEnum = [
		"ALL",
		"MONDAY",
		"TUESDAY",
		"WEDNESDAY",
		"THURSDAY",
		"FRIDAY",
		"SATURDAY",
		"SUNDAY",
	];
	public static dowEnum = [
		"MONDAY",
		"TUESDAY",
		"WEDNESDAY",
		"THURSDAY",
		"FRIDAY",
		"SATURDAY",
		"SUNDAY",
	];
	public static patternSIREN: string = "[0-9]{9}";
	public static maskSIREN: Array<string | RegExp> = [
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
		/\d/,
	];
	public static patternNIC: string = "[0-9]{5}";
	public static maskNIC: Array<string | RegExp> = [/\d/, /\d/, /\d/, /\d/, /\d/];
	public static roleUser: string[] = ["Chargeur", "Prestataire", "Control Tower"];
	public static Service_Type: string[] = [
		"MY_ACCOUNT.SERVICE_TYPE.CARRIER",
		"MY_ACCOUNT.SERVICE_TYPE.BROKER",
		"MY_ACCOUNT.SERVICE_TYPE.CARRIER_BROKER",
	];
	public static Status_User_Type: string[] = [
		"MY_ACCOUNT.STATUS_TYPE.ACTIVE",
		"MY_ACCOUNT.STATUS_TYPE.DISABLED",
		"MY_ACCOUNT.STATUS_TYPE.WAITING_FOR_ACTIVATION",
	];

	public static etatRelation: string[] = ["Actif", "Blacklist"];
	public static $1MbToBytes: number = 1048576;
	public static $0MbToBytes: number = 0;
	public static $3MbToBytes: number = 3 * Statique.$1MbToBytes;
	public static typeDemande: keyValue[] = [
		{ key: 1, value: "Standard" },
		{ key: 2, value: "Urgent" },
	];
	public static modeTransport: keyValue[] = [
		{ key: 1, value: "Air", icon: "fa fa-plane", order: 1 },
		{ key: 2, value: "Sea", icon: "fa fa-ship", order: 2 },
		{ key: 3, value: "Road", icon: "fa fa-truck", order: 3 },
		{ key: 4, value: "Integrator", icon: "fa fa-globe", order: 5 },
		{ key: 5, value: "Rail", icon: "fa fa-train", order: 4 },
	];
	public static OperationCustum: keyValue[] = [
		{ key: 1, value: "Import" },
		{ key: 2, value: "Export" },
	];

	public static TtPsl: keyValue[] = [
		{ key: 0, value: "Waiting for pickup", codeAlpha: "WPU" },
		{ key: 1, value: "Pickup", codeAlpha: "PIC" },
		{ key: 2, value: "Departure", codeAlpha: "DEP" },
		{ key: 3, value: "Arrival", codeAlpha: "ARR" },
		{ key: 4, value: "Customs", codeAlpha: "CUS" },
		{ key: 5, value: "Delivery", codeAlpha: "DEL" },
		{ key: 6, value: "Unloading", codeAlpha: "UNL" },
	];
	public static CostItemType: keyValue[] = [
		{ key: 1, value: "Other" },
		{ key: 2, value: "Origin handling" },
		{ key: 3, value: "Main transport" },
		{ key: 4, value: "Fuel surcharge" },
		{ key: 5, value: "Others surcharges(IRC)" },
		{ key: 6, value: "Insurance" },
		{ key: 7, value: "Destination Handling" },
		{ key: 8, value: "Import Customs" },
		{ key: 9, value: "Storage" },
		{ key: 10, value: "Demurrage" },
		{ key: 11, value: "Detention" },
		{ key: 12, value: "DGR" },
		{ key: 13, value: "Post carriage" },
		{ key: 14, value: "Export Customs" },
		{ key: 15, value: "Management Fees" },
		{ key: 16, value: "Fee AWB" },
		{ key: 17, value: "Fee Security Screenling" },
		{ key: 18, value: "Fee AOG" },
		{ key: 19, value: "Fee Airline Express" },
		{ key: 20, value: "Pickup" },
		{ key: 21, value: "Transit Fee" },
		{ key: 22, value: "OFR" },
		{ key: 23, value: "Delivery" },
		{ key: 24, value: "NSTI" },
		{ key: 25, value: "BL Fee" },
		{ key: 26, value: "Transport document Fee" },
		{ key: 27, value: "ISPS" },
	];

	/*     public static TtDeviationCategorie: keyValue[] = [
        {key: 1, value: "Missing or damaged goods"},
        {key: 2, value: "Information flow issues"},
        {key: 3, value: "Operational issues"},
        {key: 4, value: "Other issues"},
        {key: 5, value: "Customs issue"},
        {key: 6, value: "Goods availibility issues"},
        {key: 7, value: "Hazardous goods issues"},
        {key: 8, value: "Force majeure"},
        {key: 9, value: "Cold chain issues"}
    ]; */
	/*   public static TypeTransport: TypeTransportkeyValue[] = [{ key: 1, value: ["Consolidation", 1] }, { key: 2, value: ["Direct", 2] }, { key: 3, value: ["LCL", 1] }, { key: 4, value: ["FCL", 2] }, { key: 5, value: ["Consolidation", 1] }, { key: 6, value: ["Dedicated", 2] }];
	 */
	public static patternPassword: string =
		"(?=^.{8,}$)((?=.*d)|(?=.*W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$";

	public static MarchandiseTypeOfUnit: keyValue[] = [
		{ key: 1, value: "Cardboard box" },
		{ key: 25, value: "Suspendu" },
		{ key: 26, value: "Colis" },
		{ key: 2, value: "Pallet" },
		{ key: 3, value: "Crate" },
		{ key: 4, value: "Case" },
		{ key: 5, value: "Bundle" },
		{ key: 6, value: "Barrel" },
		{ key: 7, value: "Container 20' x 8'6' Dry" },
		{ key: 8, value: "Container 20' x 9'6' Dry High Cube" },
		{ key: 9, value: "Container 20' x 8'6' Reefer" },
		{ key: 10, value: "Container 20' x 9'6' Reefer High Cube" },
		{ key: 11, value: "Container 20' Open Top" },
		{ key: 12, value: "Container 20' Hard Top" },
		{ key: 13, value: "Container 20' Flat Rack" },
		{ key: 14, value: "Container 40'x8'6' Dry" },
		{ key: 15, value: "Container 40'x9'6' Dry High Cube" },
		{ key: 16, value: "Container 40'x8'6' Reefer" },
		{ key: 17, value: "Container 40'x9'6' Reefer High Cube" },
		{ key: 18, value: "Container 40' Open Top" },
		{ key: 19, value: "Container 40' Hard Top" },
		{ key: 20, value: "Container 40' Flat Rack" },
		{ key: 21, value: "Container 45'x9'6' Dry High Cube" },
		{ key: 22, value: "Container 45'x9'6' Reefer High Cube" },
		{ key: 23, value: "Complete truck" },
		{ key: 24, value: "Camion complet" },
	];

	// l'enumération de la liste des MarchandiseDangerousGood est déplacée vers le backend on peut la récupérer depuis "SingletonStatique"

	public static pathToFiles: String = Statique.path.toString().concat("/file/?filePath=");

	public static StatutDemande: keyValue[] = [
		{ value: "IN_PROCESS_WAITING_FOR_QUOTE", key: 1 },
		{ value: "WAITING_FOR_RECOMMENDATION", key: 2 },
		{ value: "WAITING_FOR_CONFIRMATION", key: 3 },
		{ value: "CONFIRMED", key: 4 },
		{ value: "NOT_AWARDED", key: 5 },
		{ value: "CANCELLATION_OF_QUOTATION_REQUEST", key: 6 },
		{ value: "ARCHIVED", key: 7 },
		{ value: "Prebooking cancelled ", key: 8 },
		{ value: "Waiting for cancellation confirmation ", key: 9 },
		{ value: "WAITING_DOCUMENT_TRANSPORT_CUSTOMS", key: 10 },
		{ value: "WAITING_DOCUMENT_CUSTOMS", key: 101 },
		{ value: "WAITING_DOCUMENT_TRANSPORT", key: 102 },
		{ value: "WAITING_PICKUP", key: 11 },
		{ value: "TRANSPORT_ONGOING", key: 12 },
		{ value: "COMPLETED", key: 13 },
		{ value: "Waiting for transportation request ", key: 14 },
		{ value: "CONFIRMED_PLAN_TRANSPORT", key: 15 },
		{ value: "REPLACED", key: 201 },
	];

	public static LibelleStatut: keyValue[] = [
		{ value: "In process waiting for quote", key: "IN_PROCESS_WAITING_FOR_QUOTE" },
		{ value: "Waiting for recommendation", key: "WAITING_FOR_RECOMMENDATION" },
		{ value: "Waiting for confirmation", key: "WAITING_FOR_CONFIRMATION" },
		{ value: "Waiting for pick up", key: "WAITING_PICKUP" },
		{ value: "Confirmed", key: "CONFIRMED" },
		{ value: "Completed", key: "COMPLETED" },
		{ value: "Replaced", key: "REPLACED" },
		{ value: "Not awarded", key: "NOT_AWARDED" },
		{ value: "Cancelled", key: "CANCELLATION_OF_QUOTATION_REQUEST" },
		{ value: "Archived", key: "ARCHIVED" },
		{
			value: "Waiting for document (Customs or transport)",
			key: "WAITING_DOCUMENT_TRANSPORT_CUSTOMS",
		},
		{ value: "Waiting for document (Customs)", key: "WAITING_DOCUMENT_CUSTOMS" },
		{ value: "Waiting for document (Transport)", key: "WAITING_DOCUMENT_TRANSPORT" },
		{ value: "Transport on going", key: "TRANSPORT_ONGOING" },
		{ value: "Transport on going(DELIVERY)", key: "TRANSPORT_ONGOING_DELIVERY" },
		{ value: "Transport on going(Delivery)", key: "TRANSPORT_ONGOING_DELIVERY" },
		{ value: "Transport on going(Departure)", key: "TRANSPORT_ONGOING_DEPARTURE" },
		{ value: "Transport on going(PICKUP)", key: "TRANSPORT_ONGOING_PICKUP" },
		{ value: "Transport on going(Pickup)", key: "TRANSPORT_ONGOING_PICKUP" },
		{ value: "Completed(DELIVERY)", key: "COMPLETED_DELIVERY" },
		{ value: "Completed(PICKUP)", key: "COMPLETED_PICKUP" },
		{ value: "Completed(Arrival)", key: "COMPLETED_ARRIVAL" },
		{ value: "Completed(STORE)", key: "COMPLETED_STORE" },
		{ value: "Transport on going(STORE)", key: "TRANSPORT_ONGOING_STORE" },
		{ value: "Confirmed (Transport plan)", key: "CONFIRMED_PLAN_TRANSPORT" },
		{
			value: "Cancellation of quotation request",
			key: "CANCELLATION_OF_QUOTATION_REQUEST",
		},
	];

	public static StatutGroupage: keyValue[] = [
		{ value: "NP", key: 1 },
		{ value: "PRS", key: 2 },
		{ value: "PPV", key: 3 },
		{ value: "PVC", key: 4 },
		{ value: "PCO", key: 5 },
	];

	public static CarrierStatus: keyValue[] = [
		{ key: 1, value: "STATUS.REQUEST_RECEIVED_BY_FF" },
		{ key: 2, value: "STATUS.QUOTE_SENT_BY_FF" },
		{ key: 3, value: "STATUS.RECOMMENDATION_BY_CT" },
		{ key: 4, value: "STATUS.FINAL_CHOICE" },
		{ key: 5, value: "STATUS.NOT_AWAREDED" },
		{ key: 6, value: "STATUS.TRANSPORT_PLAN" },
		{ key: 7, value: "STATUS.FINAL_CHOICE_TRANSPORT_PLAN" },
	]; // FF = Freight Forwarder
	public static CarrierStatusCreationTM: keyValue[] = [
		{ key: null, value: "" },
		{ key: 1, value: "STATUS.REQUEST_RECEIVED_BY_FF" },
		{ key: 2, value: "STATUS.QUOTE_SENT_BY_FF" },
		{ key: 4, value: "STATUS.FINAL_CHOICE" },
	]; // FF = Freight Forwarder

	public static InvoiceStatus: keyValue[] = [
		{ key: 1, value: "Waiting for confirmation" },
		{ key: 2, value: "Waiting for Payment" },
		{ key: 3, value: "Waiting for information" },
		{ key: 4, value: "Approved" },
		{ key: 5, value: "Payment sent" },
	];
	public static Flag: keyValue[] = [
		{ key: 0, value: " " },
		{ key: 1, value: "V" },
		{ key: 2, value: "X" },
		{ key: 3, value: "?" },
		{ key: 4, value: "!" },
	];

	public static TypeDemandeTimeRemaining = { 1: 48 * 3600000, 2: 24 * 3600000 };

	public static OriginCustomsBroker: keyValue[] = [
		{ key: 1, value: "MyTower operation" },
		{ key: 2, value: "Carrier" },
		{ key: 3, value: "Not applicable" },
	];

	public static iconsPath: string = "assets/images/pictos/pictos-metier/";
	public static moduleIcon = {
		PRICING: "order",
		TRANSPORT_MANAGEMENT: "trans-management",
		TRACK: "track",
		QUALITY_MANAGEMENT: "claims",
		FREIGHT_AUDIT: "freight",
		COMPLIANCE_MATRIX: "freight",
		DELIVERY_MANAGEMENT: "delivery-management",
		ORDER_MANAGEMENT: "order-management",
		COMPTA_MATIERE: "custom",
	};
	public static moduleNamePath = {
		PRICING: "pricing",
		TRANSPORT_MANAGEMENT: "transport-management",
		TRACK: "track-trace",
		QUALITY_MANAGEMENT: "quality-management",
		FREIGHT_AUDIT: "freight-audit",
		FREIGHT_ANALYTICS: "freight-analytics",
		COMPLIANCE_MATRIX: "compilance-matrix",
		pricingMask: "pricing/creation",
		transportationMask: "transport-management/creation",
		CUSTOM: "custom",
		RECEIPT_SCHEDULING: "receipt-scheduling",
		ORDER_MANAGEMENT: "delivery-overview/order",
		USER_MANAGEMENT: "users",
		DELIVERY_MANAGEMENT: "delivery-overview/delivery",
	};

	public static dashboardTable = { ORDER: 1, ORDER_DETAILS: 2, DELIVERY: 3 };

	public static moduleDetailsPath = {
		PRICING: "details",
		TRANSPORT_MANAGEMENT: "details",
		TRACK: "details",
		QUALITY_MANAGEMENT: "details",
		FREIGHT_AUDIT: "details",
		FREIGHT_ANALYTICS: "",
		COMPLIANCE_MATRIX: "",
	};

	public static invoiceMonths: keyValue[] = [
		{ key: 1, value: "Jan" },
		{ key: 2, value: "Fev" },
		{ key: 3, value: "Mars" },
		{ key: 4, value: "Avril" },
		{ key: 5, value: "Mai" },
		{ key: 6, value: "Juin" },
		{ key: 7, value: "Juil." },
		{ key: 8, value: "Aout" },
		{ key: 9, value: "Sept." },
		{ key: 10, value: "Oct" },
		{ key: 11, value: "Nov" },
		{ key: 12, value: "Dec" },
	];
	//le invoiceMonths ne marche pas par ce que les nom des mois son en francais
	public static invoiceMonths2: keyValue[] = [
		{ key: 1, value: "Jan" },
		{ key: 2, value: "Feb" },
		{ key: 3, value: "March" },
		{ key: 4, value: "April" },
		{ key: 5, value: "May" },
		{ key: 6, value: "June" },
		{ key: 7, value: "July." },
		{ key: 8, value: "August" },
		{ key: 9, value: "Sept." },
		{ key: 10, value: "Oct" },
		{ key: 11, value: "Nov" },
		{ key: 12, value: "Dec" },
	];

	public static Year: keyValue[] = [
		{ key: 1, value: "2017" },
		{ key: 2, value: "2018" },
		{ key: 3, value: "2019" },
		{ key: 4, value: "2020" },
		{ key: 5, value: "2021" },
		{ key: 6, value: "2022" },
	];

	public static GroupePropositionStatut: keyValue[] = [
		{ key: 1, value: "Confirmed" },
		{ key: 2, value: "Rejected" },
		{ key: 3, value: "Waiting for confirmation" },
	];

	public static DataRecoveryTypeList: keyValue[] = [
		{ key: 2, value: "Type request", codeAlpha: "TYPE_REQUEST" },
		{ key: 3, value: "Type unit", codeAlpha: "TYPE_UNIT" },
		{ key: 5, value: "Type document", codeAlpha: "TYPE_DOCUMENT" },
		{ key: 1, value: "Psl / Schema PSL", codeAlpha: "PSL_SCHEMA" },
		{ key: 7, value: "Type Flux", codeAlpha: "TYPE_FLUX" },
		{ key: 4, value: "Demande Quote", codeAlpha: "DEMANDE_QUOTE" },
	];
	public static ListPriority: keyValue[] = [
		{ key: 4, value: "GENERAL.PRIORITY.HIGHEST" },
		{ key: 3, value: "GENERAL.PRIORITY.HIGH" },
		{ key: 2, value: "GENERAL.PRIORITY.MEDIUM" },
		{ key: 1, value: "GENERAL.PRIORITY.LOW" },
		{ key: 0, value: "GENERAL.PRIORITY.LOWEST" },
	];

	public static getPriorityByKey(key: number) {
		let priority = "";
		this.ListPriority.forEach((p) => {
			if (p.key == key) {
				priority = p.value;
			}
		});
		return priority;
	}
	public static ListIncidentStatus: keyValue[] = [
		{ key: 1, value: "QUALITY_MANAGEMENT.INCIDENT_STATUT.INTENT_TO_CLAIM" },
		{ key: 2, value: "QUALITY_MANAGEMENT.INCIDENT_STATUT.CLAIM" },
		{ key: 3, value: "QUALITY_MANAGEMENT.INCIDENT_STATUT.RECOVERED" },
		{ key: 4, value: "QUALITY_MANAGEMENT.INCIDENT_STATUT.CLOSED" },
	];
	public static yesOrNoOrEmptyWithNumberKey: keyValue[] = [
		{ key: null, value: "" },
		{ key: 1, value: "GENERAL.YES" },
		{ key: 0, value: "GENERAL.NO" },
	];
	public static gridVersion = { PREVIOUS: 0, VALID: 1, UPCOMING: 2 };
	public static getIncidentStatusByKey(key: number) {
		let statut = "";
		this.ListIncidentStatus.forEach((p) => {
			if (p.key == key) {
				statut = p.value;
			}
		});
		return statut;
	}

	public static UNKNOWN_USER_NUM: number = -2;

	public static getRootCauseStatusByKey(key: number) {
		let statut = "";
		this.RootCauseStatus.forEach((p) => {
			if (p.key == key) {
				statut = p.value;
			}
		});
		return statut;
	}
	public static getLegs(min: number = 0, max: number = 12): Array<GenericEnum> {
		let i: number,
			arr = new Array<GenericEnum>();

		for (i = min; i <= max; i++) {
			arr.push({
				key: i + "",
				code: i,
			});
		}
		arr.push({
			key: 99 + "",
			code: 99,
		});

		return arr;
	}

	public static getModuleNameByModuleNum(ebModuleNum) {
		return Modules[ebModuleNum];
	}

	public static getComponentPathByCompIdAndModuleId(compId, moduleId): string {
		switch (moduleId) {
			case Modules.PRICING:
				if (compId === SavedFormIdentifier.PRICING_MASK) return Statique.moduleNamePath.pricingMask;
				if (compId === SavedFormIdentifier.SAVED_SEARCH)
					return Statique.moduleNamePath[Statique.getModuleNameByModuleNum(moduleId)];
			case Modules.TRANSPORT_MANAGEMENT:
				if (compId === SavedFormIdentifier.PRICING_MASK)
					return Statique.moduleNamePath.transportationMask;
				else return;
			default:
				return;
		}
	}

	public static getComponentPathByModuleId(moduleId): string {
		return (
			Statique.moduleNamePath[Statique.getModuleNameByModuleNum(moduleId)] +
			"/" +
			Statique.moduleDetailsPath[Statique.getModuleNameByModuleNum(moduleId)]
		);
	}

	public static getModuleNumFromUrl(url: string): number {
		let moduleName = Statique.getModuleNameFromUrl(url);
		if (moduleName) return Statique.getModuleNumByModuleName(moduleName);
		return null;
	}
	public static getModuleNumByModuleName(moduleName: string): number {
		let moduleNum = null;

		Object.keys(Statique.moduleNamePath).some((mod) => {
			if (mod == moduleName && Modules[mod]) {
				moduleNum = Number(Modules[mod]);
				return true;
			}
		});

		return moduleNum;
	}
	public static getModuleNameFromUrl(url: string): string {
		let moduleName = Statique.getPathFromUrl(url);

		Object.keys(this.moduleNamePath).some((it) => {
			if (url.indexOf(this.moduleNamePath[it]) >= 0) {
				moduleName = it;
				return true;
			}
		});

		if (Statique.moduleNamePath[moduleName]) return moduleName;

		return null;
	}

	public static getModuleIconByModuleNum(ebModuleNum): string {
		let moduleName: string = Statique.getModuleNameByModuleNum(ebModuleNum);

		if (!moduleName) return null;

		return Statique.moduleIcon[moduleName];
	}

	public static getPathFromUrl(url) {
		let path = url.replace(/^[a-z]{4}\:\/{2}[a-z]{1,}\:[0-9]{1,4}.(.*)/, "$1");
		if (path[path.length - 1] == "/") return path.substring(0, path.length - 1);
		return path;
	}

	public static isDefined(obj: any): boolean {
		return obj != null && obj != undefined;
	}

	public static isValidString(obj: String): boolean {
		return this.isDefined(obj) && obj.length > 0 && obj.trim() != "";
	}

	public static validateNumber(event: any) {
		return event.charCode >= 48 && event.charCode <= 57;
	}

	public static formatDatewithHours(date: any) {
		let dt = new Date(date);
		return this.formatDate(dt) + " " + dt.getHours() + ":" + dt.getMinutes();
	}

	public static formatDate(date: any) {
		if (date) {
			// date = this.stringToDate(date);
			// let curr_day = date.getDate();
			// let curr_month = date.getMonth() + 1; //Months are zero based
			// let curr_year = date.getFullYear();
			// let day = curr_day <= 9 ? "0" + curr_day : curr_day;
			// let month = curr_month <= 9 ? "0" + curr_month : curr_month;

			// return day + "/" + month + "/" + curr_year;
			return moment(date).format("DD/MM/YYYY");
		} else {
			return null;
		}
	}

	public static formatDateTimeStr(date: any) {
		if (date) {
			date = new Date(date);
			let curr_day = date.getDate();
			let curr_month = date.getMonth() + 1; //Months are zero based
			let curr_year = date.getFullYear();
			let day = curr_day <= 9 ? "0" + curr_day : curr_day;
			let month = curr_month <= 9 ? "0" + curr_month : curr_month;

			let hours = date.getHours();
			let minutes = date.getMinutes();

			return `${day}/${month}/${curr_year} ${this.lpad(hours)}:${this.lpad(minutes)}`;
		} else {
			return null;
		}
	}

	public static stringToDate(date) {
		if (date == null) {
			return null;
		} else if (typeof date == "object") {
			return date;
		} else {
			let pattern = null;
			let strDate = null;

			if ((pattern = /(\d{2})\/(\d{2})\/(\d{4})/).test(date))
				strDate = date.replace(pattern, "$3-$2-$1");
			else strDate = date;

			return new Date(strDate);
		}
	}

	public static equalsObject(obj1: any, obj2: any) {
		return JSON.stringify(obj1) === JSON.stringify(obj2);
	}

	public static byEcCurrencyCode(obj1: EcCurrency, obj2: EcCurrency) {
		if (obj1 != undefined && obj2 != undefined && (obj1 != null && obj2 != null)) {
			return obj1.code == obj2.code;
		} else return false;
	}

	public static byEcCountryNum(obj1: EcCountry, obj2: EcCountry) {
		if (obj1 != undefined && obj2 != undefined && (obj1 != null && obj2 != null)) {
			return obj1.ecCountryNum == obj2.ecCountryNum;
		} else return false;
	}

	public static byEbEtablissementNum(obj1: EbEtablissement, obj2: EbEtablissement) {
		if (obj1 != undefined && obj2 != undefined && (obj1 != null && obj2 != null)) {
			return obj1.ebEtablissementNum == obj2.ebEtablissementNum;
		} else return false;
	}
	public static byEbPlZoneDTONum(obj1: EbPlZoneDTO, obj2: EbPlZoneDTO) {
		if (obj1 != undefined && obj2 != undefined && (obj1 != null && obj2 != null)) {
			return obj1.ebZoneNum == obj2.ebZoneNum;
		} else return false;
	}
	public static byEbCompagnieNum(obj1: EbCompagnie, obj2: EbCompagnie) {
		if (obj1 != undefined && obj2 != undefined && (obj1 != null && obj2 != null)) {
			return obj1.ebCompagnieNum == obj2.ebCompagnieNum;
		} else return false;
	}
	public static byPrMtcTransporteurNum(obj1: PrMtcTransporteur, obj2: PrMtcTransporteur) {
		if (obj1 != undefined && obj2 != undefined && (obj1 != null && obj2 != null)) {
			return obj1.prMtcTransporteurNum == obj2.prMtcTransporteurNum;
		} else return false;
	}
	public static byEbGrilleNum(obj1: EbPlGrilleTransportDTO, obj2: EbPlGrilleTransportDTO) {
		if (obj1 != undefined && obj2 != undefined && (obj1 != null && obj2 != null)) {
			return obj1.ebGrilleTransportNum == obj2.ebGrilleTransportNum;
		} else return false;
	}
	public static byEbUserNum(obj1: EbUser, obj2: EbUser) {
		if (obj1 != undefined && obj2 != undefined && (obj1 != null && obj2 != null)) {
			return obj1.ebUserNum == obj2.ebUserNum;
		} else return false;
	}
	public static byEbEntrepotNum(obj1: EbEntrepotDTO, obj2: EbEntrepotDTO) {
		if (obj1 != undefined && obj2 != undefined && (obj1 != null && obj2 != null)) {
			return obj1.ebEntrepotNum == obj2.ebEntrepotNum;
		} else return false;
	}

	public static byEbDockNum(obj1: EbDockDTO, obj2: EbDockDTO) {
		if (obj1 != undefined && obj2 != undefined && (obj1 != null && obj2 != null)) {
			return obj1.ebDockNum == obj2.ebDockNum;
		} else return false;
	}

	public static byKeyValue(obj1: keyValue, obj2: keyValue) {
		if (obj1 != undefined && obj2 != undefined && (obj1 != null && obj2 != null)) {
			return obj1.key == obj2.key;
		} else return false;
	}

	public static getStyleServiceType(role: number) {
		let _class = "";
		if (role == ServiceType.TRANSPORTEUR) _class = "label label-primary";
		else if (role == ServiceType.BROKER) _class = "label label-primary";
		else if (role == ServiceType.TRANSPORTEUR_BROKER) _class = "label label-primary";
		return _class;
	}

	public static formatDateFromDatePicker(date) {
		return date ? this.formatDate(new Date(date.year, date.month - 1, date.day)) : null;
	}

	public static fromDatePicker(date) {
		return date ? new Date(date.year, date.month - 1, date.day) : null;
	}

	public static getDateFromDatetime(dt): Date {
		let date = null;
		if (dt) date = new Date(dt);
		else return null;
		return new Date(date.getFullYear(), date.getMonth(), date.getDate());
	}

	public static formatDateToStructure(dt): NgbDateStruct {
		let date = null;
		if (dt) date = new Date(dt);
		else return { day: null, month: null, year: null };
		return { day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear() };
	}
	public static formatStrTimeToStructure(textTime): NgbTimeStruct {
		let obj = { hour: null, minute: null, second: null };
		if (!textTime) return obj;

		let arrTime = textTime.split(":");
		if (arrTime.length >= 2) {
			return { hour: arrTime[0], minute: arrTime[1], second: 0 };
		}

		return obj;
	}
	public static formatTimeToStructure(dt): NgbTimeStruct {
		let date = null;
		if (dt) date = new Date(dt);
		else return { hour: null, minute: null, second: null };
		return { hour: date.getHours(), minute: date.getMinutes(), second: date.getSeconds() };
	}
	public static formatDateTime(date: Date, time: NgbTimeStruct) {
		if (!date) return null;
		date.setHours(0);
		date.setMinutes(0);
		date.setSeconds(0);

		if (time) {
			date.setHours(time.hour);
			date.setMinutes(time.minute);
			date.setSeconds(time.second);
		}

		return date;
	}

	public static removeNullList(DataJson: any) {
		for (var propName in DataJson) {
			if (DataJson[propName] === null || DataJson[propName] === undefined) {
				delete DataJson[propName];
			}
		}

		return DataJson;
	}

	public static formatMillisToObject(milliseconds: number, ignoreDays = false) {
		//Get hours from milliseconds
		var days = milliseconds / (1000 * 60 * 60 * 24);
		var absoluteDays = Math.floor(days);

		var hours = days > 0 ? (days - absoluteDays) * 24 : milliseconds / (1000 * 60 * 60);
		var absoluteHours = Math.floor(hours);

		//Get remainder from hours and convert to minutes
		var minutes = hours > 0 ? (hours - absoluteHours) * 60 : milliseconds / (1000 * 60);
		var absoluteMinutes = Math.floor(minutes);

		//Get remainder from minutes and convert to seconds
		var seconds = minutes > 0 ? (minutes - absoluteMinutes) * 60 : milliseconds / 1000;
		var absoluteSeconds = Math.floor(seconds);

		return {
			days: Statique.lpad(absoluteDays),
			hours: Statique.lpad(absoluteHours + (ignoreDays ? absoluteDays * 24 : 0)),
			minutes: Statique.lpad(absoluteMinutes),
			seconds: Statique.lpad(absoluteSeconds),
		};
	}

	public static getDays(t) {
		var days;
		days = Math.floor(t / (24 * 60 * 60));

		return this.format2numbers(days);
	}

	public static getHours(t) {
		var days, hours;
		days = Math.floor(t / (24 * 60 * 60));
		t -= days * (24 * 60 * 60);
		hours = Math.floor(t / (60 * 60)) % 24;

		return this.format2numbers(hours);
	}

	public static getMinutes(t) {
		var days, hours, minutes;
		days = Math.floor(t / (24 * 60 * 60));
		t -= days * (24 * 60 * 60);
		hours = Math.floor(t / (60 * 60)) % 24;
		t -= hours * (60 * 60);
		minutes = Math.floor(t / 60) % 60;

		return this.format2numbers(minutes);
	}

	public static convertToMinutes(value: string) {
		// convert from hh:mm to minutes
		if (!this.isDefined(value)) return;

		let houreAsMinutes: number = null;
		let result: number = null;
		let time = value.split(":");

		if (time[0].charAt(0) == "0") {
			houreAsMinutes = 60 * Number(time[0].charAt(1));
		} else {
			houreAsMinutes = 60 * Number(time[0]);
		}

		if (time[1].charAt(0) == "0") {
			result = houreAsMinutes + Number(time[1].charAt(1));
		} else {
			result = houreAsMinutes + Number(time[1]);
		}

		return result;
	}

	public static convertminToHHMM(value: number) {
		let hours = Math.floor(value / 60);
		let minutes: number = value % 60;
		let hoursAsString: string = null;
		let minutesAsString: string = null;
		if (String(hours).length == 1) {
			hoursAsString = "0" + hours;
		} else {
			hoursAsString = String(hours);
		}

		if (String(minutes).length == 1) {
			minutesAsString = "0" + minutes;
		} else {
			minutesAsString = String(minutes);
		}

		return hoursAsString + ":" + minutesAsString;
	}

	public static convertHHMMtoTimeZone(time: string, decalage: number, isUserTimeZone: boolean) {
		let totalDayMinutes = 24 * 60;
		let timeConvertedAsMinutes = isUserTimeZone
			? this.convertToMinutes(time) + decalage
			: this.convertToMinutes(time) - decalage;
		if (timeConvertedAsMinutes == 0) return "00:00";
		else if (timeConvertedAsMinutes < 0) {
			timeConvertedAsMinutes = totalDayMinutes + timeConvertedAsMinutes;
		}
		if (timeConvertedAsMinutes > totalDayMinutes)
			timeConvertedAsMinutes = timeConvertedAsMinutes - totalDayMinutes;

		return Statique.convertminToHHMM(timeConvertedAsMinutes);
	}

	public static getUrlFile(path: string): string {
		if (path != null) {
			const encodedPath = encodeURIComponent(path.toString().replace("\\", "/"));
			return this.pathToFiles.concat(encodedPath);
		} else return path;
	}

	public static getSeconds(t) {
		var days, hours, minutes, seconds;
		days = Math.floor(t / 86400);
		t -= days * 86400;
		hours = Math.floor(t / 3600) % 24;
		t -= hours * 3600;
		minutes = Math.floor(t / 60) % 60;
		t -= minutes * 60;
		seconds = t % 60;

		return this.format2numbers(seconds);
	}

	public static format2numbers(number) {
		return number > 9 ? number : "0" + number;
	}

	public static planify(data) {
		if (data.order) {
			for (var i = 0; i < data.order.length; i++) {
				let order = data.order[i];
				let column = data.columns[order.column];
				if (column) {
					order.columnName = column.data;
				}
			}
		}
		if (data.columns) {
			for (var i = 0; i < data.columns.length; i++) {
				/* let column = data.columns[i];
               column.searchRegex = column.search.regex;
               column.searchValue = column.search.value;
               column = data.columns[i];
               delete (column.search);*/
				delete data.columns[i];
			}
		}
	}

	public static copyObject(obj: object, includeObjects: boolean = false) {
		let newObj = {};

		if (obj && typeof obj === "object") {
			Object.keys(obj).forEach((it) => {
				if (typeof obj[it] !== "object" || includeObjects) newObj[it] = obj[it];
			});
		}

		return newObj;
	}

	public static isNotDefined(data): boolean {
		return (
			data === null ||
			typeof data === "undefined" ||
			(typeof data === "string" && data.trim().length == 0)
		);
	}

	public static constructObject<T>(type: new () => T): T {
		return new type();
	}

	/**
	 * Clone an object to an anonymous or a destination object
	 * @param obj The source object
	 * @param destination If null, return a new anonymous object,
	 * else copy in this reference, you can call new T() to clone to a new reference
	 */
	public static cloneObject<T>(obj: any, destination?: T): T {
		try {
			if (destination && obj)
				return SerializationHelper.toInstance(destination, JSON.stringify(obj));
			else return JSON.parse(JSON.stringify(obj));
		} catch (e) {
			console.error("Cannot clone object");
			console.error(e);
		}

		return null;
	}

	/**
	 * Return new list of new object instances
	 * @param array Source list (can be anonymous type)
	 * @param type Type of object in list
	 */
	public static cloneListObject<T>(array: any, type: new () => T): Array<T> {
		try {
			let rList = [];

			array.forEach((item) => {
				rList.push(Statique.cloneObject<T>(item, new type()));
			});

			return rList;
		} catch (e) {
			console.error("Cannot clone object list");
			console.error(e);
		}

		return null;
	}

	public static truncate2Decimal(value: number, decimal:number = 2) {
		let length = 10**decimal;
		return Math.round(value * length) / length;
	}
	//arrondi au 0.5 supérieur
	public static arrondiToZeroFive(value: number) {
		if (value != Math.floor(value)) {
			let list = String(Math.trunc(value * 10) / 10).split(".");
			if (list.length == 2) {
				let d = +list[1];
				if (d > 5) {
					return Math.floor(value) + 1;
				} else {
					return Math.floor(value) + 0.5;
				}
			} else {
				return Math.floor(value) + 0.5;
			}
		} else {
			return value;
		}
	}
	public static lpad(str, padString = "0", length = 2) {
		str = "" + str;
		while (str.length < length) str = padString + str;
		return str;
	}

	public static getValue(list: any, key: any) {
		return list.find((item) => item.key == key).value;
	}

	public static waitForTimemout(millis: number = 500) {
		return new Promise((resolve, reject) => {
			setTimeout(() => {
				resolve(millis);
			}, millis);
		});
	}

	/**
	 * promise to stop and wait until callback (cb) returns true
	 * millis (optional): interval in ms of waiting each time cb is false
	 */
	public static waitForAvailability(cb: any, millis: number = 300) {
		return new Promise(async (resolve, reject) => {
			let timeout = 0;
			while (!cb(timeout)) {
				await Statique.waitForTimemout(millis);
				timeout += millis;
			}
			resolve(timeout);
		});
	}
	public static async differExecution(cb: Function, millis: number = 300) {
		await Statique.waitForTimemout(millis);
		cb();
	}

	public static convertNewlines(value: string): any {
		if (value == null) return "";
		return value.replace(/(?:\r\n|\r|\n)/g, "<br />");
	}

	public static overlapTwoDateRanges(startA: Date, endA: Date, startB: Date, endB: Date) {
		return startA.getTime() <= endB.getTime() && endA.getTime() >= startB.getTime();
	}

	public static forceUTC(date: Date): Date {
		return new Date(date.getTime() + -new Date().getTimezoneOffset() * 60 * 1000);
	}

	public static isDate(object: any): boolean {
		let parsedDate = Date.parse(object);
		return !isNaN(parsedDate);
	}

	public static moveDomElement(el, container) {
		let oldParent = el.parentNode;
		// let lastChild = container.children[0];
		container.insertBefore(el, null);
		// oldParent.removeChild(el);
	}

	public static findAncestor(el, cls) {
		while ((el = el.parentElement) && !el.classList.contains(cls));
		return el;
	}

	public static findValueByKey(key: number, list: keyValue[]): string {
		var rValue = "";
		list.forEach((kv) => {
			if (kv.key === key) {
				rValue = kv.value;
			}
		});
		return rValue;
	}

	public static isEmptyObject(obj) {
		let key;
		for (key in obj) {
			if (obj.hasOwnProperty(key)) return false;
		}
		return true;
	}

	public static getUnique(arr, comp) {
		return arr
			.map((e) => e[comp])
			.map((e, i, final) => final.indexOf(e) === i && i)
			.filter((e) => arr[e])
			.map((e) => arr[e]);
	}

	public static async waitUntilElementExists(
		eltQuery: string,
		firstElt = true,
		timeout: number = 500
	) {
		if (!eltQuery) throw Error("No element query provided");

		let totalTimeout = 0;
		let el;
		while (!(el = document.querySelectorAll(eltQuery)[0]) && totalTimeout < 15000) {
			totalTimeout += timeout;
			await Statique.waitForTimemout(timeout);
		}

		if (!el) throw Error("Element not loaded/found in the dom for 15s");

		if (!firstElt) el = document.querySelectorAll(eltQuery);

		return el;
	}

	public static triggerEvent(eventName: string) {
		let event;
		if (typeof Event === "function") {
			event = new Event(eventName);
		} else {
			// IE 11
			event = document.createEvent("Event");
			event.initEvent(eventName, true, true);
		}
		document.dispatchEvent(event);
	}

	public static getAuthToken() {
		return { name: "Authorization", value: "Bearer " + localStorage.getItem(TOKEN_NAME) };
	}

	public static getAuthTokenStr(): string {
		return localStorage.getItem(TOKEN_NAME);
	}

	public static setTokenForCt() {
		let tok: string = this.getAuthTokenStr();
		localStorage.setItem(TOKEN_NAME + "_CT", tok);
	}

	public static deleteTokenForCt() {
		localStorage.removeItem(TOKEN_NAME + "_CT");
	}

	public static getTokenForCt() {
		return localStorage.getItem(TOKEN_NAME + "_CT");
	}

	public static removeLSItemsWithPrefix(prefix) {
		var arr = []; // Array to hold the keys

		// Iterate over localStorage and insert the keys that meet the condition into arr
		for (var i = 0; i < localStorage.length; i++) {
			if (localStorage.key(i).substring(0, prefix.length) == prefix) {
				arr.push(localStorage.key(i));
			}
		}

		// Iterate over arr and remove the items by key
		for (var i = 0; i < arr.length; i++) {
			localStorage.removeItem(arr[i]);
		}
	}

	public static compareByField(val1, val2, field: string = null) {
		if (Statique.isDefined(val1) && Statique.isDefined(val2)) {
			return field ? val1[field] == val2[field] : val1 == val2;
		}
		return false;
	}

	public static byEbZoneNum(val1, val2) {
		return Statique.compareByField(val1, val2, "ebZoneNum");
	}

	public static byExportControlNum(val1, val2) {
		return val1 && val2 ? val1 == val2 : false;
	}

	public static compareCostCenter(val1, val2) {
		return Statique.compareByField(val1, val2, "ebCostCenterNum");
	}
	public static compareTypeRequest(val1, val2) {
		return Statique.compareByField(val1, val2, "ebTypeRequestNum");
	}
	public static compareIncoterm(val1, val2) {
		return Statique.compareByField(val1, val2, "ebIncotermNum");
	}
	public static compareByFieldKey(val1, val2) {
		return Statique.compareByField(val1, val2, "key");
	}
	public static compareSchemaPsl(val1, val2) {
		return Statique.compareByField(val1, val2, "ebTtSchemaPslNum");
	}
	public static compareTypeFlux(val1, val2) {
		return Statique.compareByField(val1, val2, "ebTypeFluxNum");
	}
	public static compareByFieldCode(val1, val2) {
		return Statique.compareByField(val1, val2, "code");
	}
	public static compareByUserNum(val1, val2) {
		return Statique.compareByField(val1, val2, "ebUserNum");
	}
	public static compareByCompagnieNum(val1, val2) {
		return Statique.compareByField(val1, val2, "ebCompagnieNum");
	}

	public static compareCurrency(elt1: EcCurrency, elt2: EcCurrency) {
		if (Statique.isDefined(elt1) && Statique.isDefined(elt2)) {
			return elt1.ecCurrencyNum === elt2.ecCurrencyNum;
		}
	}

	public static compareCountry(country1: EcCountry, country2: EcCountry) {
		if (country2 !== undefined && country2 !== null) {
			return country1.ecCountryNum === country2.ecCountryNum;
		}
	}

	public static compareTypeUnit(type1: EbTypeUnit, type2: EbTypeUnit) {
		if (type2 !== undefined && type2 !== null) {
			return type1.ebTypeUnitNum === type2.ebTypeUnitNum;
		}
	}

	public static getCurrentEbDemande(): EbDemande {
		let strDemande = localStorage.getItem("currentEbDemande");
		let ebDemande: EbDemande = null;

		try {
			ebDemande = JSON.parse(strDemande);
		} catch (e) {
			console.error("Cannot parse ebDemande from localStorage.");
		}

		return ebDemande;
	}
	public static setCurrentEbDemande(data: EbDemande) {
		try {
			let strData = JSON.stringify(data);
			localStorage.setItem("currentEbDemande", strData);
		} catch (e) {
			console.error("Cannot stringify/store ebDemande in localStorage.");
		}
	}
	public static resetCurrentEbDemande() {
		try {
			localStorage.removeItem("currentEbDemande");
		} catch (e) {
			console.error("Cannot delete ebDemande from localStorage.");
		}
	}

	public static RootCauseStatus: keyValue[] = [
		{ key: 1, value: "To Do" },
		{ key: 2, value: "In Progress" },
		{ key: 3, value: "Waiting For" },
		{ key: 4, value: "Completed" },
	];

	public static iconCheckOneToggle(event: any) {
		if (event.checked == true) {
			event.classList.add("fa-check");
		} else if (event.checked == false && event.classList.contains("fa-check")) {
			event.classList.remove("fa-check");
		}
	}

	public static iconCheckAllToggle(event: any) {
		if (event.target.checked == true) {
			event.target.classList.add("fa-check");
		} else if (event.target.checked == false && event.target.classList.contains("fa-check")) {
			event.target.classList.remove("fa-check");
		}
	}
	public static getZoneDisplayString(zone?: EbPlZoneDTO): string {
		if (zone) {
			if (zone.designation) {
				return zone.ref + " (" + zone.designation + ")";
			} else {
				return zone.ref;
			}
		} else {
			return "";
		}
	}

	public static getTypeDemandeStringFromKey(key: number): string {
		var rValue = "";

		Statique.typeDemande.some((t, index, array) => {
			if (t.key === key) {
				rValue = t.value;
				return true;
			}
		});

		return rValue;
	}
	public static getTypeTransportStringFromKey(code: number, listTypeTransports: any): string {
		let rValue = "";

		Object.assign([], listTypeTransports).some((t, index, array) => {
			t.some((e, index, array) => {
				if (e.ebTypeTransportNum === code) {
					rValue = e.libelle;
					return true;
				}
			});
		});

		return rValue;
	}
	public static getModeTransportStringFromKey(code: number, listModeTransports: any): string {
		let rValue = "";

		listModeTransports.some((t, index, array) => {
			if (t.code === code) {
				rValue = t.libelle;
				return true;
			}
		});

		return rValue;
	}

	public static getModeTransportLibelle(code: number): string {
		return ModeTransport[code];
		if ((code = ModeTransport.AIR)) {
		}
		if ((code = ModeTransport.ROAD)) {
			return ModeTransport[code];
		}
		if ((code = ModeTransport.SEA)) {
			return ModeTransport[code];
		}
		if ((code = ModeTransport.INTEGRATOR)) {
			return ModeTransport[code];
		}
		if ((code = ModeTransport.RAIL)) {
			return ModeTransport[code];
		}
	}

	// prettier-ignore
	public static getFaIconByFlagIconCode(flagIconCode: number): string {
    switch (flagIconCode) {
      case FlagIcon.ROAD: return "fa-road";
      case FlagIcon.SHIP: return "fa-ship";
      case FlagIcon.PLANE: return "fa-plane";
      case FlagIcon.TRAIN: return "fa-train";
      case FlagIcon.CAR: return "fa-car";
      case FlagIcon.COG: return "fa-cog";
      case FlagIcon.SQUARE: return "fa-square";
      case FlagIcon.CIRCLE: return "fa-circle";
      case FlagIcon.TRUCK: return "fa-truck";
      case FlagIcon.ADDRESS_CARD: return "fa-address-card";
      case FlagIcon.EXCLAMATION_TRIANGLE: return "fa-exclamation-triangle";
      case FlagIcon.ORDER: return "my-icon-order-management";
      case FlagIcon.DELIVERY: return "my-icon-delivery-management";
      case FlagIcon.PRICING: return "my-icon-order";
      case FlagIcon.TRANSPORT: return "my-icon-trans-management";
      case FlagIcon.TRACK_TRACE: return "my-icon-track";
      case FlagIcon.QUALITY: return "my-icon-claims";
      case FlagIcon.FREIGTH_AUDIT: return "my-icon-freight";
      case FlagIcon.FREIGTH_ANALYTICS: return "my-icon-analytics";
      case FlagIcon.CUSTOMS: return "my-icon-custom";
      case FlagIcon.RECEIPT_SCHEDULING: return "fa-calendar";
      case FlagIcon.ADD: return "fa-plus-circle";
      case FlagIcon.SEARCH: return "fa-search-plus";
      default: return "";
    }
  }

	/*
    public static ConnectedUser = {
        get: (): EbUser => {
            let strUser = localStorage.getItem("currentEbUser");
            let ebUser: EbUser = null;

            try{
                ebUser = JSON.parse(strUser);
            } catch(e){
                console.error("Cannot parse connected user from localStorage.")
            }

            return ebUser;
        },
        remove: () => {
            try{
                localStorage.removeItem("currentEbUser");
            } catch(e){
                console.error("Cannot delete connected user from localStorage.")
            }
        },
        set: (user: EbUser) => {
            try{
                let strData = JSON.stringify(user);
                localStorage.setItem("currentEbUser", strData);
            } catch(e){
                console.error("Cannot stringify/store connected user in localStorage.")
            }
        }
    }*/

	public static getStatusLibelle(xEcStatut) {
		let res = Statique.StatutDemande.find((it) => it.key == xEcStatut);
		return res ? res.value : "";
	}

	public static getStatusByLibelle(xEcStatut) {
		let res = Statique.LibelleStatut.find((it) => it.value == xEcStatut || it.key == xEcStatut);
		return res ? res.key : "";
	}

	public static getGroupageStatusLibelle(status) {
		let res = Statique.StatutGroupage.find((it) => it.key == status);
		return res ? res.value : "";
	}

	public static compareFuseauHoraire(
		ecFuseauxHoraire1: EcFuseauxHoraire,
		ecFuseauxHoraire2: EcFuseauxHoraire
	) {
		if (ecFuseauxHoraire2 !== undefined && ecFuseauxHoraire2 !== null) {
			return ecFuseauxHoraire1.ecFuseauHoraireNum === ecFuseauxHoraire2.ecFuseauHoraireNum;
		}
	}

	public static SenseChronoPricing: any[] = [
		{ code: 1, key: "TIME_REMAINING" },
		{ code: 2, key: "TYPE_SINCE_ISSUANCE" },
	];
	public static BlocageResponsePricing: any[] = [
		{ code: true, key: "YES" },
		{ code: false, key: "NO" },
	];

	public static ActivatedRequest: any[] = [{ code: true, key: "YES" }, { code: false, key: "NO" }];

	public static getListSenseChronoPricing() {
		let list = [];
		this.SenseChronoPricing.forEach((kv) => {
			list.push(kv);
		});
		return list;
	}
	public static getListBlocageResponsePricing() {
		let list = [];
		this.BlocageResponsePricing.forEach((kv) => {
			list.push(kv);
		});
		return list;
	}

	public static getListActivatedRequest() {
		let list = [];
		this.ActivatedRequest.forEach((kv) => {
			list.push(kv);
		});
		return list;
	}

	public static getListActivePsl(schemaPsl: EbTtSchemaPsl): Array<EbTtCompanyPsl> {
		let data = schemaPsl.listPsl.filter((it) => it.isChecked);
		if (!data || !data.length) return schemaPsl.listPsl.filter((it) => it.schemaActif);
	}

	//cette méthode sert a recalculer la largeur du element passer en parametre pour éviter le double scroll et l'adapter avec l'écran
	public static onResizeAdapter(
		restOfEltsClass: Array<string>,
		eltToAdaptClass: string,
		padding: number,
		windowHeight: number
	) {
		let sommeEltsHeight = 0;
		restOfEltsClass.forEach((elementClass) => {
			const eltByClassName = <HTMLElement>document.querySelector(elementClass);
			if (eltByClassName) {
				const elementHeight = eltByClassName.offsetHeight;
				sommeEltsHeight += elementHeight;
			}
		});
		const eltToAdapt = <HTMLElement>document.querySelector(eltToAdaptClass);
		if (eltToAdapt && windowHeight == null) {
			eltToAdapt.style.maxHeight = "calc(100vh - " + (sommeEltsHeight + padding) + "px)";
		} else if (eltToAdapt) {
			eltToAdapt.style.height = windowHeight - (sommeEltsHeight + padding) + "px";
		}
	}

	public static getGainDemandeResult(propo: EbQrGroupeProposition, demande: EbDemande): string {
		if (!propo) return;
		let str = "",
			price = 0,
			gain = 0;

		let sumPrice = 0;
		propo.listEbDemande
			.filter((it) => it.isChecked)
			.forEach((d) => {
				d.exEbDemandeTransporteurs.forEach((q) => (sumPrice += q.price));
			});

		if (!demande.consolidatedPrice && propo.statut == GroupePropositionStatut.CONFIRMED) {
			if (demande.exEbDemandeTransporteurs && demande.exEbDemandeTransporteurs.length) {
				demande.consolidatedPrice = demande.exEbDemandeTransporteurs[0].price;
			}
		}

		let consolidatedPrice = demande.consolidatedPrice;
		if (sumPrice && demande.consolidatedPrice) {
			if (Statique.isDefined(consolidatedPrice)) {
				price = +(sumPrice - consolidatedPrice).toFixed(2);
				gain = Math.floor((consolidatedPrice / sumPrice) * 100);

				str = price + "";

				demande.ventilationPrice = price;

				if (demande.xecCurrencyInvoice) str += " " + demande.xecCurrencyInvoice.code;

				str += " (" + gain + "%)";

				return str;
			}
		}

		return "-";
	}

	public static getVentilation(grp: EbQrGroupe) {
		if (SingletonStatique.listVentilation) {
			if (!Statique.isDefined(grp)) return "";
			let res = SingletonStatique.listVentilation.find((it) => it.code == grp.ventilation);
			return res ? res.libelle : "";
		}
		return "";
	}

	public static getInnerObject(context: any, ...args: Array<string>): any {
		if (!args && !args.length) return null;

		let obj = context;
		args.some((it) => {
			if (obj[it]) obj = obj[it];
			else {
				obj = null;
				return true;
			}
		});

		return obj;
	}

	public static getInnerObjectStr(context: any, argStr: string): any {
		if (!argStr && !argStr.length) return null;
		let args = argStr.split(".");

		let obj = context;
		args.some((it) => {
			if (obj[it]) obj = obj[it];
			else {
				obj = null;
				return true;
			}
		});

		return obj;
	}

	public static reqAnimatFrameAsync() {
		return new Promise((resolve) => {
			requestAnimationFrame(resolve);
		});
	}

	public static async checkElementExist(selector) {
		const querySelector = document.querySelector(selector);
		while (querySelector === null) {
			await this.reqAnimatFrameAsync();
		}
		return querySelector;
	}

	public static get isIE11(): boolean {
		let ua = window.navigator.userAgent;
		let msie = ua.indexOf("MSIE ");
		return msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./);
	}

	public static toggleClassOnHover(event, classHover) {
		let element = event.target;
		if (!element.classList.contains(classHover)) {
			element.classList.add(classHover);
		} else {
			element.classList.remove(classHover);
		}
	}

	public static removeBlankValues(obj: any): any {
		Object.entries(obj).forEach(([key, val]) => {
			if (val && typeof val === "object") {
				if (!Object.keys(val).length) delete obj[key];
				else Statique.removeBlankValues(val);
			} else if (!Statique.isDefined(val)) delete obj[key];
		});
		return obj;
	}

	public static getModeTransportIcon(xEcModeTransport: number) {
		if (!Statique.isDefined(xEcModeTransport)) return "";
		let v = Statique.modeTransport.find((it) => it.key == xEcModeTransport);
		return v ? v.icon : "";
	}

	public static getTypeOfRequest(
		listTypeDemandeByCompagnie: Array<EbTypeRequestDTO>,
		xEbTypeRequest: number
	) {
		if (!Statique.isDefined(xEbTypeRequest)) return "";
		if (!listTypeDemandeByCompagnie) return "";
		let res = listTypeDemandeByCompagnie.find((it) => it.ebTypeRequestNum == xEbTypeRequest);
		return res ? res.reference : "";
	}

	public static refreshComponent(router: Router, url: string = null, refreshPage: boolean = false) {
		if (refreshPage) location.reload();
		else {
			router.navigateByUrl(url || router.url, { skipLocationChange: true }).then(() => {
				router.navigate([url || router.url]);
			});
		}
	}

	public static routeDetailDemandeInPricing(): string {
		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(Modules.PRICING)];
		return "/app/" + moduleRoute + "/details/";
	}

	public static routeDetailTracing(): string {
		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(Modules.TRACK)];
		return "/app/" + moduleRoute + "/details/";
	}

	public static isValueIn(listValues: string, value: string): boolean {
		return listValues.indexOf(":" + value + ":") >= 0;
	}

	public static statusBackground: keyValue[] = [
		{ key: 1, value: "bleu" },
		{ key: 2, value: "gray" },
		{ key: 3, value: "bleu-dark" },
		{ key: 4, value: "green" },
		{ key: 5, value: "orange" },
		{ key: 6, value: "bleu-green" },
		{ key: 7, value: "bleu-green-dark" },
	];

	public static getListWord(pText: string, sepLeft: string, sepRight: string): Array<string> {
		if (!pText || !pText.length) return null;

		let text = pText.toString(),
			word = "",
			listWord = new Array<string>();

		while (text.length > 0) {
			word = "";
			text = text.substring(text.indexOf(sepLeft));
			if (text.length) text = text.substring(sepLeft.length);

			let delim = text.indexOf(sepRight);
			if (delim >= 0) {
				word = text.substring(0, delim);
				text = text.substring(delim);
			} else {
				text = "";
			}
			if (word.length) listWord.push(word);
		}

		return listWord;
	}

	public static copyTextToClipboard(text: string) {
		let txtArea = document.createElement("textarea");
		txtArea.id = "txt-clipboard-tmp";
		txtArea.style.position = "fixed";
		txtArea.style.top = "0";
		txtArea.style.left = "0";
		txtArea.style.opacity = "0";
		txtArea.value = text;
		document.body.appendChild(txtArea);
		txtArea.select();

		try {
			document.execCommand("copy");
		} catch (err) {
			console.error("nable to copy to clipboard", err);
		} finally {
			document.body.removeChild(txtArea);
		}
		return false;
	}

	public static random_string(length) {
		var result = "";
		var characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		var charactersLength = characters.length;
		for (var i = 0; i < length; i++) {
			result += characters.charAt(Math.floor(Math.random() * charactersLength));
		}
		return result;
	}

	public static getWebSocket(ebUserNum: number) {
		let ws = null;
		let url = null;
		if (!environment.production) {
			url = environment.apiUrl + "/socket";
		} else {
			url = "/socket";
		}
		ws = new SockJS(url, null, {
			sessionId: function() {
				let sessionId = ebUserNum + "-" + Statique.random_string(8);
				localStorage.setItem(WS_SESSION_ID, sessionId);
				return sessionId;
			},
		});
		return ws;
	}

	public static getWsSessionId() {
		return localStorage.getItem(WS_SESSION_ID);
	}

	public static getWsSessionIdUserNum() {
		return this.getUserNumFromSessionId(localStorage.getItem(WS_SESSION_ID));
	}

	public static getUserNumFromSessionId(sessionId: string) {
		return sessionId ? sessionId.split("-")[0] : "";
	}

	public static listThemes: Theme[] = [
		{ name: "default-theme", color: "text-blue", i18n: "DEFAULT_THEME" },
		{ name: "dark-theme", color: "text-black", i18n: "BLACK_THEME" },
		{ name: "red-theme", color: "text-red", i18n: "RED_THEME" },
		{ name: "aurora-theme", color: "text-aurora-blue", i18n: "AURORA_THEME" },
	];
	public static removeThemes() {
		this.listThemes.forEach((theme) =>
			document.getElementsByTagName("body")[0].classList.remove(theme.name)
		);
	}
	public static addCostItemDetailToDtConfig(dtConfig: any) {
		const lastIndex = dtConfig.length;
		dtConfig.push({
			title: "PRICING_BOOKING.COST_ITEM",
			name: "costItem",
			order: lastIndex + 1,
		});
		dtConfig.push({
			title: "FREIGHT_AUDIT.CURRENCY",
			name: "costCurrency",
			order: lastIndex + 2,
		});
		dtConfig.push({
			title: "FREIGHT_AUDIT.NEGOTIATED_PRICE",
			name: "negotiatedPrice",
			order: lastIndex + 3,
		});
		dtConfig.push({
			title: "FREIGHT_AUDIT.NEGOTIATED_EXCHANGE_RATE",
			name: "negociatedExchangeRate",
			order: lastIndex + 4,
		});
		dtConfig.push({
			title: "FREIGHT_AUDIT.NEGOTIATED_PRICE_CONVERTED",
			name: "negociatedPriceConverted",
			order: lastIndex + 5,
		});
		dtConfig.push({
			title: "FREIGHT_AUDIT.INVOICED_PRICE",
			name: "invoicedPrice",
			order: lastIndex + 6,
		});
		dtConfig.push({
			title: "FREIGHT_AUDIT.INVOICED_EXCHANGE_RATE",
			name: "costItemExchangeRate",
			order: lastIndex + 7,
		});
		dtConfig.push({
			title: "FREIGHT_AUDIT.INVOICED_PRICE_CONVERTED",
			name: "invoicePriceConverted",
			order: lastIndex + 8,
		});
		dtConfig.push({
			title: "FREIGHT_AUDIT.ITEM_GAP",
			name: "costItemGap",
			order: lastIndex + 9,
		});
		dtConfig.push({
			title: "FREIGHT_AUDIT.GAP_CONVERTED",
			name: "gapConverted",
			order: lastIndex + 10,
		});
		return dtConfig;
	}

	public static formatNumberValue(value: number) {
		if (Number.isNaN(value)) return 0;
		if (value != null) return value.toString().replace(/,/g, ".");
	}
}

export interface keyValue {
	key: any;
	code?: string;
	value?: string;
	codeAlpha?: string;
	order?: number;
	icon?: string;
}

export type Theme = {
	name: string;
	color: string;
	i18n: string;
};

/* export interface TypeTransportkeyValue {
    key: any;
    value: any;
} */

export interface idText {
	id: number;
	text: String;
}
