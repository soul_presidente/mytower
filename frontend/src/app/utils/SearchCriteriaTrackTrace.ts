import { SearchCriteria } from "./searchCriteria";
import { Statique } from "@app/utils/statique";

export class SearchCriteriaTrackTrace extends SearchCriteria {
	ebTtTracingNum: number;

	listUnitReference: Array<any>;
	listCustomerRefAndtransportRefPlugin: Array<any>;
	datePickUpFrom: String;
	datePickUpTo: String;
	datePickUpToPlugin: String;
	datePickUpFromPlugin: String;

	constructor() {
		super();

		this.listCustomerRefAndtransportRefPlugin = new Array<any>();
		this.listUnitReference = new Array<any>();
		this.datePickUpTo = null;
		this.datePickUpFrom = null;
		this.datePickUpToPlugin = null;
		this.datePickUpFromPlugin = null;
	}

	constructorCopy(advancedOptions: SearchCriteriaTrackTrace) {
		this.datePickUpToPlugin = advancedOptions.datePickUpToPlugin;
		this.datePickUpFromPlugin = advancedOptions.datePickUpFromPlugin;
		this.listCustomerRefAndtransportRefPlugin =
			advancedOptions.listCustomerRefAndtransportRefPlugin;
		this.datePickUpTo = advancedOptions.datePickUpTo;
		this.datePickUpFrom = advancedOptions.datePickUpFrom;
		this.listUnitReference = advancedOptions.listUnitReference;

		if (advancedOptions.listCustomerRefAndtransportRefPlugin) {
			advancedOptions.listCustomerRefAndtransportRefPlugin.forEach(
				(listCustomerRefAndtransportRef, index) => {
					this.listCustomerRefAndtransportRefPlugin.push(
						listCustomerRefAndtransportRef.ebTtTracingNum
					);
				}
			);
		}
	}

	formatDates() {
		if (this.datePickUpToPlugin != null)
			this.datePickUpTo = Statique.formatDateFromDatePicker(this.datePickUpToPlugin);
		if (this.datePickUpFromPlugin != null)
			this.datePickUpFrom = Statique.formatDateFromDatePicker(this.datePickUpFromPlugin);
		return this;
	}
}
