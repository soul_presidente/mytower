import { SearchCriteria } from "./searchCriteria";
import { DetailUnitDTO } from "@app/classes/trpl/DetailUnitDTO";

export class SearchCriteriaPlanTransport extends SearchCriteria {
	ebZoneNumOrigin: number;
	ebZoneNumDest: number;
	typeDemande: number;
	modeTransport: number;
	typeTransport: number;
	modeTransportNonNullOnly: boolean;
	typeGoodsNum: number;
	dangerous: boolean;
	totalWeight: number;
	totalUnit: number;
	totalVolume: number;
	currencyCibleNum: number;
	ebEtablissementNum: number;
	dateWaitingForPickup: Date;
	datePickup: Date;
	dateDeparture: Date;
	dateArrival: Date;
	dateCustoms: Date;
	dateDeliery: Date;
	dateUnloading: Date;
	ebTypeUnitNum: number;
	listUnit: Array<DetailUnitDTO>;
	ebTransporteurNum: number;
	isForValorisation: boolean;
	constructor() {
		super();
		this.listUnit = new Array<DetailUnitDTO>();
	}
}
