import { SearchCriteria } from "./searchCriteria";

export class SearchCriteriaQualityManagement extends SearchCriteria {
	listIncidentRefs: Array<number>;
	listCarrierRefs: Array<string>;
	listCutomerRefs: Array<string>;
	listShipToRefs: Array<number>;
	listCarriers: Array<string>;
	listOperationalGroupings: Array<string>;
	listDestinationCountries: Array<string>;
	insuranceChoice: boolean;
	listIncidentQualifs: Array<string>;
	listIncidentCategories: Array<string>;
	listIncidentStatus: Array<string>;
	listInsuranceIncident: Array<number>;
	listInsuranceIncidentPlugin: Array<any>;
	ebQmIncidentNum: number;
	semiAutoMailNum: number;
	listStatusIncident: Array<number>;
	listStatutIncidentPlugin: Array<any>;
	ebQmRootCauseNum: number;
	ebQmDeviationNum: number;
	eventType: number;
	incidentStatus:number;


	marchandiseIdNumbers: Array<number> = new Array<number>();

	constructor() {
		super();
		this.listIncidentRefs = new Array<number>();
		this.listCarrierRefs = new Array<string>();
		this.listCutomerRefs = new Array<string>();
		this.listShipToRefs = new Array<number>();
		this.listCarriers = new Array<string>();
		this.listOperationalGroupings = new Array<string>();

		this.insuranceChoice = false;
		this.listIncidentQualifs = new Array<string>();
		this.listIncidentCategories = new Array<string>();
		this.listIncidentStatus = Array<string>();
		this.ebUserNum = null;
		this.ebQmIncidentNum = null;
		this.semiAutoMailNum = null;
		this.ebQmIncidentNum = null;
		this.ebQmDeviationNum = null;

		this.marchandiseIdNumbers = new Array<number>();
		this.listInsuranceIncident = new Array<number>();
		this.listInsuranceIncidentPlugin = new Array<any>();

		this.listStatusIncident = new Array<number>();
		this.listStatutIncidentPlugin = new Array<any>();
		this.incidentStatus = null;
	}

	constructorCopy(advancedOptions: SearchCriteriaQualityManagement) {
		super.constructorCopy(advancedOptions);
		this.listStatutIncidentPlugin = advancedOptions.listStatutIncidentPlugin;
		this.listInsuranceIncidentPlugin = advancedOptions.listInsuranceIncidentPlugin;

		if (advancedOptions.listStatutIncidentPlugin) {
			advancedOptions.listStatutIncidentPlugin.forEach((statut, index) => {
				this.listStatusIncident.push(statut.code);
			});
		}

		if (advancedOptions.listInsuranceIncidentPlugin) {
			advancedOptions.listInsuranceIncidentPlugin.forEach((insurance, index) => {
				this.listInsuranceIncident.push(insurance.code);
			});
		}
	}
}
