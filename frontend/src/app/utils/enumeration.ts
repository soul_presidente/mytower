export enum GenericEnums {
	DOC_TemplateModels = "DOC.TemplateModels",
}

export enum TransportConfirmationEnums {
	TransportConfirmation = "ENUM.TransportConfirmation",
}
export enum EtatHoraire {
	ALL_DAYS = 0,
	CUSTOMISE = 1,
}

export enum ExportControlStatut {
	ACE = 1,
	LICENCE_OK = 2,
	LICENCE_NOK = 3,
	ALD = 4,
}

export enum TypeDialog {
	CONFIRMATION_WITH_MOTIF,
	DELETE_CONFIRMATION,
	EROOR_WITH_MSG,
	CONFIRMATION,
	OPENING_HOURS,
	CONFIRMATION_INSCRIPTION,
}

export enum UserRole {
	CHARGEUR = 1,
	PRESTATAIRE = 2,
	CONTROL_TOWER = 3,
	UNKNOWN = 4,
}

export enum DataRecoveryType {
	PSL_SCHEMA = 1,
	TYPE_REQUEST = 2,
	TYPE_UNIT = 3,
	DEMANDE_QUOTE = 4,
	TYPE_DOCUMENT = 5,
	MASQUE = 6,
}

export enum ServiceType {
	TRANSPORTEUR = 0,
	BROKER = 1,
	TRANSPORTEUR_BROKER = 2,
	CONTROL_TOWER = 3,
}

export enum UploadDirectory {
	LOGO = 1,
	PRICING_BOOKING = 2,
	BULK_IMPORT = 3,
	FREIGHT_AUDIT = 4,
	CHANEL_PB = 5,
	QUALITY_MANAGEMENT = 6,
	TEMPLATE = 7,
	CUSTOM = 8,
	DELIVERY = 9,
	ORDER = 10,
}

export enum EtatRelation {
	EN_COURS = 0,
	ACTIF = 1,
	IGNORER = 2,
	ANNULER = 3,
	BLACKLIST = 4,
}

export enum StatusDemande {
	ENCOURS = 0,
	ACCPETER = 1,
	IGNORER = 2,
	ANNULER = 3,
}

export enum Modules {
	PRICING = 1,
	TRANSPORT_MANAGEMENT = 2,
	QUALITY_MANAGEMENT = 3,
	COMPLIANCE_MATRIX = 4,
	TRACK = 5,
	CLAIMS = 6,
	FREIGHT_AUDIT = 7,
	FREIGHT_ANALYTICS = 8,
	CUSTOM = 9,
	ORDER_MANAGEMENT = 10,
	RECEIPT_SCHEDULING = 11,
	USER_MANAGEMENT = 16,
	ORDRE_TRANSPORT = 12,
	DELIVERY_MANAGEMENT = 18,
	COMPTA_MATIERE = 19,
	ETABLISSEMENT_INFORMATION = 20,
	ORDER_LINE = 21,
	LOAD_PLANNINNG = 22,
	ADDITIONAL_COST = 23,
	COST_ITEM = 24,
}

export enum SavedFormIdentifier {
	SAVED_SEARCH = 1,
	PRICING_MASK = 2,
	FAVORIS = 3,
	HISTORY = 4,
	QUALITY_MANAGEMENT_MASK = 5,
	SAVED_SEARCH_ACTION = 6,
}

export enum SavedSearchAction {
	CHANGE_STATUS = 1,
	ADD_FLAG = 2,
	SEND_ALERT_TO = 3,
}

export enum SavedSearchActionTrigger {
	ON_CREATE = 1,
	ON_UPDATE = 2,
	DAILY = 3,
}

export enum ModeTransport {
	AIR = 1,
	SEA = 2,
	ROAD = 3,
	INTEGRATOR = 4,
	RAIL = 5,
}

export enum TypeModal {
	INFORMATION = 0,
	CONFIRMATION = 1,
}

export enum DemandeStatus {
	PND = -1,
	IN_PROCESS = 1,
	WAITING_FOR_RECOMMENDATION = 2, // en cas de recommendation coché par le chargeur
	RECOMMENDATION = 3, // waiting for confirmation

	FINAL_CHOICE = 4, // devenu inutile
	NOT_AWARDED = 5,
	CAN = 6, // devenu inutile
	//ARC = 7, // n'est plus un réel statut

	WDCT = 10,
	WPU = 11,
	TO = 12,
	DLVRY = 13,
	FINPLAN = 15, //confirmed from transport plan

	RPL = 201,
}

export enum CancelStatus {
	CANCELLED = 1,
}

export enum CarrierStatus {
	REQUEST_RECEIVED = 1,
	QUOTE_SENT = 2,
	RECOMMENDATION = 3,
	FINAL_CHOICE = 4,
	NOT_AWARDED = 5,
	TRANSPORT_PLAN = 6,
	FIN_PLAN = 7,
}

export enum FileType {
	INTERCO_INVOICE = 1,
	COMMERCIAL_INVOICE = 2,
	CUSTOMS = 3,
	TRANSPORT_DOCUMENT = 4,
	OTHER = 5,
	POD = 6,
	DELIVERY = 7,
	ORDER = 8,
}

export enum EtatUser {
	ACTIF = 1,
	DESACTIVE = 2,
	NON_ACTIF = 3, //en attente d'activation
	ACCEPTER_PAR_ADMIN = 4,
}

export enum TypeDemande {
	STANDARD = 1,
	URGENT = 2,
}

export enum GenericTableScreen {
	PRICING = 1,
	BOOKING = 2,
	QUALITY = 3,
	FREIGHT_AUDIT = 4,
	TRACK = 5,
	FREIGHT_ANALYTICS = 8,
	CUSTOM = 9,
	SETTINGS_TRANCHE = 13,
	SETTINGS_GRILLE = 14,
	SETTINGS_TRANSPORT_PLAN = 15,
	USER_MANAGEMENT = 16,
	SETTINGS_ENTREPOT = 17,
	RECEIPT_SCHEDULING = 18,
	SHIP_TO_MATRIX = 19,
	TYPE_UNIT = 20,
	DEVIATION_SETTINGS = 21,
	FLAG_MANAGEMENT = 22,
	FAVORIS = 23,
	DOCK_MANAGEMENT = 24,
	SETTINGS_SCHEMA_PSL = 25,
	CONFIG_PSL = 26,
	PROFILE_MANAGEMENT = 27,
	CONFIG_QR_GROUPE = 28,
	BOUTON_ACTION = 29,
	SETTINGS_REGIMES_TEMPORAIRES = 30,
	COMPTA_MATIERE = 31,
	TYPE_OF_GOODS = 32,
	COMPLIANCE_MATRIX = 33,
	SETTINGS_TYPE_OF_REQUEST = 34,
	SETTINGS_ZONE = 35,
	SETTINGS_DOCUMENT_TEMPLATES = 36,
	TYPE_OF_FLUX = 37,
	INCOTERM = 38,
	TYPE_DOCUMENT = 39,
	TRANSPORT_TABLE = 40,
	CONFIGURABLE_FIELD = 41,
	ADRESSES = 42,
	ETABLISSEMENT = 43,
	PRICING_UNITS = 44,
	ETABLISSEMENTCURRENCY = 46,
	PR_MTC_TRANSPORTEUR = 47,
	WEEKTEMPLATE = 48,
	LOAD_PLANNING = 50,
	MAPPING_CODE_PSL = 51,
	ROOT_CAUSE = 53,
	ORDER_DASHBOARD = 60,
	DELIVERY_DASHBOARD = 61,
	ORDER_UNITS = 62,
	DELIVERY_UNITS = 63,
	ORDER_PLANIFICATION_TAB1 = 64,
	ORDER_PLANIFICATION_TAB2 = 65,
	ORDER_LIST_DELIVERY = 66,
	ADDITIONAL_COST = 67,
	COSTS_ITEMS = 68,
	MTC_MAPPING_PARAMS = 69,
}

export enum OriginCustomsBroker {
	MY_TOWER_OPERATION = 1,
	CARRIER = 2,
	NOT_APPLICABLE = 3,
}

export enum InvoiceStatus {
	WAITING_FOR_CARRIER_INPUT = 1,
	WAITING_FOR_CHARGEUR_CONFIRMATION = 2,
	WAITING_FOR_CARRIER_CONFIRMATION = 3,
	CONFIRMED_BY_CHARGEUR = 4,
}

export enum RoleSociete {
	CHARGEUR = 1,
	PRESTATAIRE = 2,
	CONTROL_TOWER = 3,
}

export enum AccessRights {
	CONTRIBUTION = 0,
	VISUALISATION = 1,
	NON_VISIBLE = 2,
}

export enum Late {
	LATE_TRACK = 1,
	NOT_LATE = 2,
}

export enum IDChatComponent {
	FREIGHT_AUDIT = 1,
	TRANSPORT_MANAGEMENT = 2,
	TRACK = 3,
	PRICING = 4,
	CUSTOM = 5,
	QUALITY_MANAGEMENT = 6,
	ORDER_MANAGEMENT = 7,
	DELIVERY = 8,
}

export enum EnumPsl {
	PICKUP = 1,
	DEPARTURE = 2,
	ARRIVAL = 3,
	CUSTOMS = 4,
	DELIVERY = 5,
}

export enum TypeChamp {
	normal = 1,
	special = 2,
}

export enum TypeCustomsBroker {
	MY_TOWER_OPERATION = 1,
	CARRIE = 2,
	NOT_APPLICABLE = 3,
}

export enum ConfigZoneCustom {
	HEADER = 1,
	REQUEST_INFORMATION = 2,
	UNITS_INFORMATION = 3,
	DOCUMENTS = 4,
	ADDITIONAL_INFORMATION = 5,
	CANCELLATION_INFORMATION = 6,
	CHAT_HISTORY = 7,
}

export enum StatusIncident {
	INTENT_TO_CLAIM = 1,
	CLAIM = 2,
	RECOVERED = 3,
	CLOSED = 4,
}

export enum IncidentStatus {
	INTENT_TO_CLAIM = "Intent to claim",
	CLAIM = "Claim",
	RECOVERED = "Recovered",
	CLOSED = "Closed",
}

export enum AdresseEligibility {
	INTERMEDIATE_POINT = "INP",
}

/*export enum ActionType {
	sharedStatus= "sharedStatus" ,
	sharedPrice= "sharedPrice" ,
	sharedNumber= "sharedNumber" ,
	sharedMonthAndYear= "sharedMonthAndYear" ,
	updatePriceArraw= "updatePriceArraw" ,
	updateNumberArraw= "updateNumberArraw" ,
	applyListDateChargeur= "applyListDateChargeur" ,
	applyListDateCarrier= "applyListDateCarrier" ,
	applyListInvoicePriceChargeur= "applyListInvoicePriceChargeur" ,
	applyListInvoicePriceCarrier= "applyListInvoicePriceCarrier" ,
	applyListInvoiceNumberChargeur= "applyListInvoiceNumberChargeur" ,
	applyListInvoiceNumberCarrier= "applyListInvoiceNumberCarrier" ,
	applyListStatutChargeur= "applyListStatutChargeur" ,
	applyListStatutCarrier= "applyListStatutCarrier" ,
	applyListFlagChargeur= "applyListFlagChargeur" ,
	applyListFlagCarrier= "applyListFlagCarrier" ,
	addInvoiceDateChargeur= "addInvoiceDateChargeur" ,
	addInvoiceDateCarrier= "addInvoiceDateCarrier",
};*/
export enum TypeDataEbDemande {
	EXPEDITION_INFORMATION = 1,
	REFERENCE_INFORMATION = 2,
	GOODS_INFORMATION = 3,
	EXTERNAL_USERS = 4,
}

export enum ConfigViewCustom {
	HEADER = 1,
	REFERENCE_INFORMATION = 2,
	SHIPPING_INFORMATION = 3,
	CUSTOMS_INFORMATION = 4,
	CARRIER_INFORMATION = 5,
	INVOICE_INFORMATION = 6,
	OTHER_INFORMATION = 7,
	COMMENTS = 8,
	TOTAL = 9,
	TABLE = 10,
	TABLE_DOCUMENTS = 11,
	ADDITIONAL_INFORMATION = 12,
	CANCELLATION_INFORMATION = 13,
}

export enum OrderStatus {
	PENDING = "PENDING", //create
	NOTDLV = "NOTDLV", // waiting for delivery
	PARTIALDLV = "PARTIALDLV", // partially delivered
	TDLV = "TDLV", // totally delivered
}

export enum DeliveryStatus {
	PICKED = 0,
	PACKED = 1,
	Shipped = 2,
	DELIVERED = 3,
	CANCELED = 4,
}

export enum TypeContainer {
	ARTICLE = 4,
	PARCEL = 3,
	PALETTE = 2,
	CONTAINER = 1,
	ALL = 0,
}

export enum Extractors {
	PRICING = 1,
	TRANSPORT_MANAGEMENT = 2,
	INCOERENCE_STATUT_TM_TT = 3,
	INCOERENCE_STATUT_TT_PSL = 4,
	SAVINGS = 5,
}

export enum ReceiptSchedulingStatus {
	NONTERMINES = 1,
	APLANIFIER = 2,
	TERMINES = 3,
	TOUS = 4,
}

export enum ReceiptSchedulingArrivantAvant {
	AUJOURDHUI = 1,
	UNJOURS = 2,
	DEUXJOURS = 3,
	TROIXJOURS = 4,
	UNESEMAINE = 5,
	DEUXSEMAINES = 6,
	UNMOIS = 7,
	SIXMOIS = 8,
	TOUS = 9,
}

export enum NatureDemandeTransport {
	NORMAL = 0,
	TRANSPORT_FILE = 1,
	MASS_IMPORT = 2,
	MASTER_OBJECT = 3,
	INITIAL = 4,
	TRANSPORT_PRINCIPAL = 5,
	POST_ACHEMINEMENT = 6,
	CONSOLIDATION = 7,
	EDI = 8,
}

export namespace NatureDemandeTransport {
	export function values() {
		let tab = Object.keys(NatureDemandeTransport).filter(
			(type) => isNaN(<any>type) && type !== "values"
		);
		return Object.keys(tab).map((item) => ({
			value: "NATURE_DEMANDE." + NatureDemandeTransport[item],
			key: item,
		}));
	}
}

export enum MasterObjectType {
	MULTI_SEGMENT = 0,
	MULTI_SEGMENT_DOCK = 1,
}

export enum RschUnitScheduleStatus {
	TO_PLANIFY = 0,
	PLANIFIED = 1,
	CALCULATED = 2,
}

export enum FlagIcon {
	ROAD = 1,
	SHIP = 2,
	PLANE = 3,
	TRAIN = 4,
	CAR = 5,
	COG = 6,
	SQUARE = 7,
	CIRCLE = 8,
	TRUCK = 9,
	ADDRESS_CARD = 10,
	EXCLAMATION_TRIANGLE = 11,
	ORDER = 12,
	DELIVERY = 13,
	PRICING = 14,
	TRANSPORT = 15,
	TRACK_TRACE = 16,
	QUALITY = 17,
	FREIGTH_AUDIT = 18,
	FREIGTH_ANALYTICS = 19,
	CUSTOMS = 20,
	RECEIPT_SCHEDULING = 21,
	ADD = 22,
	SEARCH = 23,
}

export enum PageUnitOrder {
	CREATE_DELIVERY = 1,
	CREATE_ORDER = 2,
	EDIT_ORDER = 3,
}

export enum ActionFreightAudit {
	applyListInvoicePrice = 1,
	applyListInvoiceNumber = 2,
	applyListStatut = 3,
	applyListDate = 4,
	applyListMemo = 5,
	sharedPrice = 6,
	sharedNumber = 7,
	sharedStatus = 8,
	sharedMonthAndYear = 9,
}

export enum ViewType {
	TABLE = 1,
	CARD = 2,
	TREE = 3,
}

export enum PriseRDVStatus {
	INISTIAL = 0,
	ANNULEE = 1,
	VALIDEE = 2,
}

export enum TypeDatePriseRDV {
	PICKUP = 1,
	LAST_EXPECTED_PSL = 2,
}

export enum TypeImportance {
	MAJEUR = 1,
	MINEUR = 2,
}

export enum GroupePropositionStatut {
	CONFIRMED = 1,
	REJECTED = 2,
	WAITING_FOR_CONFIRMATION = 3,
}

export enum StatutGroupage {
	NP = 1,
	PRS = 2,
	PPV = 3,
	PVC = 4,
	PCO = 5,
}

export enum GroupeVentilation {
	AU_PRORATA_DU_POIDS_TAXABLE = 1,
	AU_PRORATA_DES_QUANTITES = 2,
	EQUITABLEMENT_PAR_DOSSIER_DE_TRANSPORT = 3,
	SANS_PONDERATION = 4,
}

export enum TypeOfEvent {
	INCIDENT = 1,
	EVENT = 2,
	UNCONFORMITY = 3,
}

export enum QrCreationStep {
	EXPEDITION = "expedition",
	REFERENCE = "reference",
	UNITS = "units",
	DOCUMENTS = "documents",
	PRICING = "pricing",
	INFOS_ABOUT_TRANSPORT = "infos-about-transport",
	ORDER = "order",
	CONSOLIDATION = "Consolidation",
}

export enum YesOrNo {
	NO = 0,
	YES = 1,
}

export enum FileDownLoadStatus {
	DEBUT = 0,
	EN_COURS = 1,
	FIN = 2,
	ERROR = 3,
	LIMITE_DEPASSE = 4,
}

export enum TrackingLevel {
	TRANSPORT = 0,
	CONTAINER = 1,
	PALETTE = 2,
	COLIS = 3,
	ARTICLE = 4,
	UNITS = 5,
	UNIT_WITHOUT_PARENT = 6,
}
export enum OrderStep {
	ORDER = "order",
	REFERENCE = "reference",
	UNITS = "units",
	DELIVERY = "delivery",
}

export enum DeliveryStep {
	DELIVERY = "delivery",
	REFERENCE = "reference",
	UNITS = "units",
}
export enum ModeOfTransport {
	AIR = "Air",
	SEA = "Sea",
	ROAD = "Road",
	INTEGRATOR = "Integrator",
	RAIL = "Rail",
}

export enum TransfertActionChoice {
	NONE = 1,
	ALL = 2,
	CHOOSE = 3,
}

export enum Ventilation {
	TAXABLE_WEIGHT = 1,
	QUANTITY = 2,
	EQUITABLE = 3,
	NO_WEIGHTING = 4,
}
export enum WeekType {
	NORMAL = 1,
	SPECIAL = 2,
}

export enum GenericEnumPlanTrans {
	WeekType = "PlanTrans.WeekType",
	TransportConfirmation = "ENUM.TransportConfirmation",
}

export enum CrOperator {
	ABSENT = 3,
}
export enum CrRuleCategory {
	DATE = 1,
	DOCUMENT = 2,
}

export enum CrTrGenerationOperator {
	INTERCHANGE = 201,
	BECOMES = 202,
	IS_DELETED = 203,
	IS_CONSERVED = 204,
}

export enum CrConditionOperator {
	IS_IN = 101,
	IS_NOT_IN = 102,
	BEGINS_WITH = 103,
	AVAILABLE = 104,
	NOT_AVAILABLE = 105,
}

export enum CrRuleOperandType {
	PAYS_ORIGIN = 3,
	DESTINATION_COUNTRY = 4,
	LEG = 20,
	CUSTOMFIELD = 12,
	PARTNUMBER = 15,
	SERIALNUMBER = 16,
	ARTICLE_REF = 10,
	NBR_PLTR = 21,
}

export enum CrOperationTypes {
	BOTH_OPERATIONS = 0,
	IS_TR_GENERATION = 1,
	IS_FILE_CONDITION = 2,
	IS_DATE_CONDITION = 3,
}

export enum CrFieldTypes {
	ALL = 0,
	STRING = 1,
	NUMBER = 2,
	EBPARTY = 3,
	CAT = 4,
	CUSTOMFIELD = 5,
	TYPE_DE_FLUX = 6,
	MODE_TR = 7,
	STATUS = 9,
	TYPE_OF_REQ = 10,
	REF_ADR = 12,
	PSL_SCHEMA = 13,
	UNITS = 14,
	LIST = 15,
	COUNTRY = 17,
	UNIT_TYPE = 18,
	GOOD_TYPE = 19,
	OVERSIZED_ITEM = 20,
	LARGE_ITEM = 21,
	DGR = 22,
	CLASS = 23,
	MANDATORY_FIELDS = 24,
	DATE = 25,
	COST_CENTER = 26,
	TYPE_OF_DOC = 27,
	UNKOWN = -1,
}

export enum NatureDateEvent {
	NEGOTIATED = 3,
	NOW = 4,
	CHAMP = 5,
}

export enum NotificationType {
	ALERT = 1,
	NOTIFY = 2,
	MSG = 3,
}

export enum TemplateModels {
	PricingItem = 0,
	Order = 1,
	Delivery = 2,
}

export enum ResultEntityType {
	SUCCESS = 1,
	ERROR = 2,
}

export enum ActionType {
	NEW = "New",
	DASHBOARD = "Dashboard",
	FILTER = "Filter",
	MASQUE = "Masque",
}

export enum CompanyRole{
	CHARGEUR = 1,
	PRESTATAIRE = 2,
}
