import { EbParty } from '@app/classes/party';
import { SearchCriteria } from "./searchCriteria";
import { EbUser } from '@app/classes/user';

export class SearchCriteriaOrder extends SearchCriteria {
	ebDelOrderNum: number;
	numOrderSAP: string;
	refDelivery: string;
	refArticle: string;
	numOrderEDI: string;
	numOrderCustomer: string;
	orderCustomerCode: string;
	orderCustomerName: string;
	nameClientDelivered: string;
	refClientDelivered: string;
	campaignCode: string;
	campaignName: string;
	orderCreationDate: Date;
	suggestedDeliveryDate: Date;
	deliveryCreationDate: Date;
	estimatedShipDate: Date;
	effectiveShipDate: Date;
	pickupSite: string;
	endOfPackDate: Date;
	deliveryStatus: number;
	transportStatus: number;
	numParcels: string;
	orderCreationDateSearch: string;
	countryClientDelivered: string;
	customerOrderReference: string;
	customFields: string;
	customerCreationDate: string;

	listCampaignCode: string[];
	listCampaignName: string[];
	listNumOrderCustomer: string[];
	listOrderCustomerName: string[];
	listNumOrderEDI: string[];
	listNumOrderSAP: string[];
	listRefArticle: string[];
	listOrderCustomerCode: string[];
	listcustomerOrderReference: string[];
	listcustomFields: string[];
	listPartyOrigin:string [];
	listPartyDestination: string[];
	listOwnerOfTheRequests: string[];

	xEbPartyOrigin: EbParty;
	xEbPartyDestination: EbParty;
	xEbOwnerOfTheRequest: EbUser;

	includeLines: boolean;

	constructor() {
		super();
		this.countryClientDelivered = null;
		this.ebDelOrderNum = null;
		this.refDelivery = null;
		this.numOrderSAP = null;
		this.refArticle = null;
		this.orderCustomerCode = null;
		this.numOrderEDI = null;
		this.numOrderCustomer = null;
		this.orderCustomerName = null;
		this.nameClientDelivered = null;
		this.refClientDelivered = null;
		this.campaignCode = null;
		this.campaignName = null;
		this.orderCreationDate = null;
		this.suggestedDeliveryDate = null;
		this.deliveryCreationDate = null;
		this.estimatedShipDate = null;
		this.effectiveShipDate = null;
		this.pickupSite = null;
		this.endOfPackDate = null;
		this.deliveryStatus = null;
		this.transportStatus = null;
		this.numParcels = null;
		this.orderCreationDateSearch = null;

		this.listCampaignCode = null;
		this.listOrderCustomerCode = null;
		this.listCampaignName = null;
		this.listNumOrderCustomer = null;
		this.listOrderCustomerName = null;
		this.listNumOrderEDI = null;
		this.listNumOrderSAP = null;
		this.listRefArticle = null;
		this.customerOrderReference = null;
		this.customFields = null;
		this.customerCreationDate = null;
		this.listcustomerOrderReference = null;
		this.listcustomFields = null;
		this.xEbPartyOrigin = null;
		this.xEbPartyDestination = null;
		this.xEbOwnerOfTheRequest = null;
		this.listPartyDestination = null;
		this.listPartyOrigin = null;
		this.listOwnerOfTheRequests = null;
	}
}
