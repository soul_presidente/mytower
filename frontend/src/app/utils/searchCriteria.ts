import { EbUser } from "@app/classes/user";
import { Statique } from "@app/utils/statique";
import { EbDemande } from "@app/classes/demande";

export class SearchCriteria {
	public ebUserNum: number;
	public ebUserProfileNum: number;
	public ebUserPassword: String;
	public email: string;
	public username: string;
	public ebEtablissementNum: number;
	public withListAdresse: boolean;
	public withListCostCenter: boolean;
	public withListContacts: boolean;
	public withAccessRights: boolean;
	public withPassword: boolean;
	public searchterm: string;
	public pageNumber: number;
	public size: number;
	public roleBroker: boolean;
	public roleTransporteur: boolean;
	public roleControlTower: boolean;
	public roleChargeur: boolean;
	public status: number;
	public demandeEnCours: boolean;
	public admin: boolean;
	public superAdmin: boolean;
	public ecRoleNum: number;
	public forInvite: boolean;
	public ebDestinationCountryNum: number;
	public ebCompagnieNum: number;
	public ebCompagnieGuestNum: number;
	public receivedRequest: boolean;
	public contactable: boolean;
	public contact: boolean;
	public blackList: boolean;
	public notBlackList: boolean;
	public sentRequests: boolean;
	public emailNomPrenom: string;
	public ebDemandeNum: number;
	public ebDelOrderNum: number;
	public ebDemande: EbDemande;
	public module: number;
	public ecCountryNum: number;
	public userService: number;
	public hsCode: string;
	public dateDeb: Date;
	public dateFin: Date;
	public etablissementRequest: boolean;
	public excludeConnectedUser: boolean;
	public relationWIthEtablissement: boolean;
	public user: string;
	public forChargeur: boolean;
	public forControlTower: boolean;
	public ignoreContact: boolean;
	public currencyCibleNum: number;
	public isCompagnie: boolean;
	public communityCompany: boolean;
	public fromCommunity: boolean;
	public excludeCt: boolean;
	public getAll: boolean;
	public withDeactive: boolean;
	public simpleSearch: boolean;
	public ascendant: boolean;
	public orderedColumn: string;
	public extractData: boolean;
	public dangerous: boolean;
	public modeTransport: number;
	public typeDemande: number;
	public typeDocumentNum: number;
	public totalWeight: number;
	public nbPackages: number;
	public searchInput: string;
	public connectedUserNum: number;
	public connectedControlTower: number;
	public emailUnknownUser: string;
	public idObject: number;
	public boutonAction: boolean;
	public includeGlobalValues: boolean;
	public activated: boolean;
	public showGlobals: boolean;
	public listIdObject: number[];
	public currencyCostItem: number;

	listTransportRefPlugin: Array<any>;
	listCustomerReferencePlugin: Array<any>;
	listDestinationsPlugin: Array<any>;
	listOriginsPlugin: Array<any>;
	listDestinationCityPlugin: Array<any>;
	listOriginCityPlugin: Array<any>;
	listCarrierPlugin: Array<any>;
	listCategoriePlugin: Array<any>;
	listStatutPlugin: Array<any>;
	listCostCenterPlugin: Array<any>;
	champListCategorie: Array<number>;
	listTransportRef: Array<string>;
	listCustomerReference: Array<string>;
	listDestinations: Array<number>;
	listOrigins: Array<number>;
	listDestinationCity: Array<string>;
	listOriginCity: Array<string>;
	listCarrier: Array<number>;
	listCategorie: Array<number>;
	listStatut: Array<number>;
	listCostCenter: Array<string>;
	listIncotermPlugin: Array<any>;
	listIncoterm: Array<string>;
	listTypeFlux: Array<number>;
	listTypeFluxPlugin: Array<any>;
	listModule: Array<number>;
	listGroupeNum: Array<number>;
	withListMarchandise: boolean;
	ebCategorieNum: number;

	datePickUpFrom: String;
	datePickUpTo: String;
	datePickUpToPlugin: String;
	datePickUpFromPlugin: String;

	invoiceDateFrom: String;
	invoiceDateTo: String;

	expectedDateFrom: String;
	expectedDateTo: String;
	expectedDateToPlugin: String;
	expectedDateFromPlugin: String;

	dateCreationFrom: String;
	dateCreationTo: String;
	dateCreationToPlugin: String;
	dateCreationFromPlugin: String;

	finalDeliveryFrom: String;
	finalDeliveryTo: String;
	finalDeliveryToPlugin: String;
	finalDeliveryFromPlugin: String;

	dtConfig: Object;
	listFlag: String;
	withEtablissement: boolean;
	withEtablissementDetail: boolean;
	typeContener: number;
	listOfExistingTransporteur: Array<number>;
	companyRole: number;
	withActiveUsers: boolean;
	constructor() {
		this.emailNomPrenom = null;
		this.ebUserNum = null;
		this.ebUserProfileNum = null;
		this.email = null;
		this.ebUserPassword = null;
		this.ebEtablissementNum = null;
		this.withListAdresse = false;
		this.withListCostCenter = false;
		this.withListContacts = false;
		this.withAccessRights = false;
		this.withPassword = false;
		this.searchterm = null;
		this.pageNumber = 0;
		this.typeContener = 0;
		this.size = null;
		this.roleBroker = false;
		this.roleTransporteur = false;
		this.roleControlTower = false;
		this.roleChargeur = false;
		this.status = null;
		this.demandeEnCours = true;
		this.admin = true;
		this.superAdmin = false;
		this.ecRoleNum = null;
		this.forInvite = null;
		this.ebDestinationCountryNum = null;
		this.ebCompagnieNum = null;
		this.ebCompagnieGuestNum = null;
		this.receivedRequest = false;
		this.contactable = false;
		this.contact = false;
		this.blackList = false;
		this.notBlackList = false;
		this.sentRequests = false;
		this.ebDemandeNum = null;
		this.ebDelOrderNum = null;
		this.ebDemande = null;
		this.module = null;
		this.ecCountryNum = null;
		this.userService = null;
		this.hsCode = null;
		this.dateDeb = null;
		this.dateFin = null;
		this.etablissementRequest = false;
		this.excludeConnectedUser = false;
		this.relationWIthEtablissement = false;
		this.user = null;
		this.forChargeur = false;
		this.forControlTower = false;
		this.ignoreContact = false;
		this.communityCompany = false;
		this.fromCommunity = false;
		this.excludeCt = false;
		this.currencyCibleNum = null;
		this.getAll = null;
		this.simpleSearch = null;
		this.dangerous = null;
		this.modeTransport = null;
		this.typeDemande = null;
		this.totalWeight = null;
		this.nbPackages = null;
		this.searchInput = null;
		this.connectedUserNum = null;
		this.connectedControlTower = null;
		this.emailUnknownUser = null;
		this.idObject = null;
		this.boutonAction = false;

		this.listTransportRef = new Array<string>();
		this.listCustomerReference = new Array<string>();
		this.listDestinations = new Array<number>();
		this.listOrigins = new Array<number>();
		this.listDestinationCity = new Array<string>();
		this.listOriginCity = new Array<string>();
		this.listCarrier = new Array<number>();
		this.listCategorie = new Array<number>();
		this.listStatut = new Array<number>();
		this.listCostCenter = new Array<string>();

		this.listTransportRefPlugin = new Array<any>();
		this.listCustomerReferencePlugin = new Array<any>();
		this.listDestinationsPlugin = new Array<any>();
		this.listOriginsPlugin = new Array<any>();
		this.listCarrierPlugin = new Array<any>();
		this.listDestinationCityPlugin = new Array<any>();
		this.listOriginCityPlugin = new Array<any>();
		this.listCategoriePlugin = new Array<any>();
		this.listStatutPlugin = new Array<any>();
		this.listCostCenterPlugin = new Array<any>();
		this.listIncotermPlugin = new Array<any>();
		this.listIncoterm = Array<string>();
		this.listModule = new Array<number>();
		this.listGroupeNum = null;
		this.listTypeFluxPlugin = new Array<any>();
		this.listTypeFlux = Array<number>();
		this.withDeactive = null;
		this.datePickUpTo = null;
		this.datePickUpFrom = null;
		this.datePickUpToPlugin = null;
		this.datePickUpFromPlugin = null;

		this.dateCreationTo = null;
		this.dateCreationFrom = null;
		this.dateCreationToPlugin = null;
		this.dateCreationFromPlugin = null;

		this.expectedDateTo = null;
		this.expectedDateFrom = null;
		this.expectedDateToPlugin = null;
		this.expectedDateFromPlugin = null;
		this.invoiceDateFrom = null;
		this.invoiceDateTo = null;

		this.finalDeliveryTo = null;
		this.finalDeliveryFrom = null;
		this.finalDeliveryToPlugin = null;
		this.finalDeliveryFromPlugin = null;

		this.dtConfig = null;
		this.extractData = false;
		this.ascendant = false;
		this.listFlag = null;

		this.ebCategorieNum = null;
		this.includeGlobalValues = false;
		this.typeDocumentNum = null;
		this.activated = null;
		this.withListMarchandise = false;
		this.listOfExistingTransporteur = null;
		this.currencyCostItem = null;
		this.companyRole = null;
		this.withActiveUsers = false;
	}

	constructorCopy(advancedOptions: SearchCriteria) {
		this.listTransportRefPlugin = advancedOptions.listTransportRefPlugin;
		this.listCustomerReferencePlugin = advancedOptions.listCustomerReferencePlugin;
		this.listDestinationsPlugin = advancedOptions.listDestinationsPlugin;
		this.listOriginsPlugin = advancedOptions.listOrigins;
		this.listDestinationCityPlugin = advancedOptions.listDestinationCity;
		this.listOriginCityPlugin = advancedOptions.listOriginCity;
		this.listCarrierPlugin = advancedOptions.listCarrierPlugin;
		this.listCategoriePlugin = advancedOptions.listCategoriePlugin;
		this.listStatutPlugin = advancedOptions.listStatutPlugin;
		this.listCostCenterPlugin = advancedOptions.listCostCenterPlugin;
		this.listIncotermPlugin = advancedOptions.listIncoterm;
		this.listTypeFluxPlugin = advancedOptions.listTypeFlux;
		advancedOptions.listTransportRefPlugin.forEach((transportref, index) => {
			this.listTransportRef.push(transportref.ebDemandeNum);
		});

		advancedOptions.listCustomerReferencePlugin.forEach((customerref, index) => {
			this.listCustomerReference.push(customerref.ebCommandeNum);
		});

		advancedOptions.listDestinationsPlugin.forEach((dest, index) => {
			this.listDestinations.push(dest.ecCountryNum);
		});

		advancedOptions.listCategoriePlugin.forEach((category, index) => {
			this.listCategorie.push(category.ebCategorieNum);
		});

		advancedOptions.listOriginsPlugin.forEach((origi, index) => {
			this.listOrigins.push(origi.ecCountryNum);
		});

		advancedOptions.listOriginCityPlugin.forEach((origi, index) => {
			this.listOriginCity.push(origi.city);
		});

		advancedOptions.listDestinationCityPlugin.forEach((dest, index) => {
			this.listDestinationCity.push(dest.city);
		});

		if (advancedOptions.listTransportRefPlugin) {
			advancedOptions.listTransportRefPlugin.forEach((transportref, index) => {
				this.listTransportRef.push(transportref.ebDemandeNum);
			});
		}
		if (advancedOptions.listCustomerReferencePlugin) {
			advancedOptions.listCustomerReferencePlugin.forEach((customerref, index) => {
				this.listCustomerReference.push(customerref.ebCommandeNum);
			});
		}
		if (advancedOptions.listDestinationsPlugin) {
			advancedOptions.listDestinationsPlugin.forEach((dest, index) => {
				this.listDestinations.push(dest.ecCountryNum);
			});
		}
		if (advancedOptions.listCarrierPlugin) {
			advancedOptions.listCarrierPlugin.forEach((carrier, index) => {
				this.listCarrier.push(carrier.ebUserNum);
			});
		}
		if (advancedOptions.listCategoriePlugin) {
			advancedOptions.listCategoriePlugin.forEach((category, index) => {
				this.listCategorie.push(category.ebCategorieNum);
			});
		}
		if (advancedOptions.listStatutPlugin) {
			advancedOptions.listStatutPlugin.forEach((statut, index) => {
				this.listStatut.push(statut.code);
			});
		}
		if (advancedOptions.listCostCenterPlugin) {
			advancedOptions.listCostCenterPlugin.forEach((cost, index) => {
				this.listCostCenter.push(cost.ebCostCenterNum);
			});
		}

		if (advancedOptions.listIncotermPlugin) {
			advancedOptions.listIncotermPlugin.forEach((Incoterm, index) => {
				this.listIncoterm.push(Incoterm.ebIncotermNum);
			});
		}

		if (advancedOptions.listTypeFluxPlugin) {
			advancedOptions.listTypeFluxPlugin.forEach((typeFlux, index) => {
				this.listTypeFlux.push(typeFlux.ebTypeFluxNum);
			});
		}

		this.datePickUpToPlugin = advancedOptions.datePickUpToPlugin;
		this.datePickUpFromPlugin = advancedOptions.datePickUpFromPlugin;
		this.datePickUpTo = advancedOptions.datePickUpTo;
		this.datePickUpFrom = advancedOptions.datePickUpFrom;

		this.dateCreationToPlugin = advancedOptions.dateCreationToPlugin;
		this.dateCreationFromPlugin = advancedOptions.dateCreationFromPlugin;
		this.dateCreationTo = advancedOptions.dateCreationTo;
		this.dateCreationFrom = advancedOptions.dateCreationFrom;

		this.expectedDateToPlugin = advancedOptions.expectedDateToPlugin;
		this.expectedDateFromPlugin = advancedOptions.expectedDateFromPlugin;
		this.expectedDateTo = advancedOptions.expectedDateTo;
		this.expectedDateFrom = advancedOptions.expectedDateFrom;

		this.invoiceDateFrom = advancedOptions.invoiceDateFrom;
		this.invoiceDateTo = advancedOptions.invoiceDateTo;

		this.finalDeliveryToPlugin = advancedOptions.finalDeliveryToPlugin;
		this.finalDeliveryFromPlugin = advancedOptions.finalDeliveryFromPlugin;
		this.finalDeliveryTo = advancedOptions.finalDeliveryTo;
		this.finalDeliveryFrom = advancedOptions.finalDeliveryFrom;

		this.dtConfig = null;
		this.listFlag = advancedOptions.listFlag;
		this.companyRole = advancedOptions.companyRole;
		this.withActiveUsers = advancedOptions.withActiveUsers;
	}

	formatDates() {
		if (this.datePickUpToPlugin != null)
			this.datePickUpTo = Statique.formatDateFromDatePicker(this.datePickUpToPlugin);
		if (this.datePickUpFromPlugin != null)
			this.datePickUpFrom = Statique.formatDateFromDatePicker(this.datePickUpFromPlugin);
		if (this.dateCreationToPlugin != null)
			this.dateCreationTo = Statique.formatDateFromDatePicker(this.dateCreationToPlugin);
		if (this.dateCreationFromPlugin != null)
			this.dateCreationFrom = Statique.formatDateFromDatePicker(this.dateCreationFromPlugin);
		if (this.expectedDateToPlugin != null)
			this.expectedDateTo = Statique.formatDateFromDatePicker(this.expectedDateToPlugin);
		if (this.expectedDateFromPlugin != null)
			this.expectedDateFrom = Statique.formatDateFromDatePicker(this.expectedDateFromPlugin);
		if (this.finalDeliveryToPlugin != null)
			this.finalDeliveryTo = Statique.formatDateFromDatePicker(this.finalDeliveryToPlugin);
		if (this.finalDeliveryFromPlugin != null)
			this.finalDeliveryFrom = Statique.formatDateFromDatePicker(this.finalDeliveryFromPlugin);
		return this;
	}

	setUser(user: EbUser) {
		if (user != null) {
			try {
				let newUser = new EbUser();
				newUser.constructorCopyLite(user);
				this.user = JSON.stringify(newUser);
			} catch (e) {}
		}
		return this;
	}
}

export class SearchCriteriaLite {
	listIdObject: Array<number>;
	connectedUserNum: number;

	constructor(ebUserNum: number = null) {
		this.connectedUserNum = ebUserNum;
	}
}
