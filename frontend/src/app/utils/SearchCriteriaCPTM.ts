import { SearchCriteria } from "./searchCriteria";

export class SearchCriteriaCPTM extends SearchCriteria {
	private leg2NotFilled: boolean;
	private refProcedure: string;
	private leg1Location: string;
	private leg1TransitNumber: string;
	private leg2Location: string;
	private leg2TransitNumber: string;
	private nbAlerts: number;
	private statutProcedure: number;
	private deadlineProcedure: Date;
	private leg1ExpectedCustomerDeclarationDate: Date;
	private leg2ExpectedCustomerDeclarationDate: Date;
	private xLeg1UnitRef: string;
	private xLeg2UnitRef: string;
	private xRegimeTemporaireLeg1Name: string;
	private xRegimeTemporaireLeg2Name: string;
	private xLeg1DemandeRef: string;
	private xLeg2DemandeRef: string;
	private leg1ActualCustomerDeclarationDate: Date;
	private leg2ActualCustomerDeclarationDate: Date;
	private leg1DeclarationNumber: string;
	private leg2DeclarationNumber: string;
	private leg1TransitStatus: number;
	private leg2TransitStatus: number;
	private leg1TracingStatus: string;
	private leg2TracingStatus: string;
	private searchField: boolean;
	private withoutLeg2: boolean;
	constructor() {
		super();
	}
}
