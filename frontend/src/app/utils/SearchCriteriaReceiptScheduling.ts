import { SearchCriteria } from "./searchCriteria";

export class SearchCriteriaReceiptScheduling extends SearchCriteria {
	listEbRschUnitScheduleNum: Array<number>;
	maxArrivalDateTp: Date;
	minArrivalDateTp: Date;
	maxArrivalDatePa: Date;
	minArrivalDatePa: Date;
	selected: boolean;
	constructor() {
		super();
		this.listEbRschUnitScheduleNum = new Array<number>();
		this.maxArrivalDateTp = null;
		this.minArrivalDateTp = null;
		this.maxArrivalDatePa = null;
		this.minArrivalDatePa = null;
		this.selected = null;
	}

	static constructorCopy(other: SearchCriteriaReceiptScheduling): SearchCriteriaReceiptScheduling {
		const newItem = new SearchCriteriaReceiptScheduling();
		newItem.listEbRschUnitScheduleNum = other.listEbRschUnitScheduleNum;
		newItem.maxArrivalDateTp = other.maxArrivalDateTp;
		newItem.minArrivalDateTp = other.minArrivalDateTp;
		newItem.maxArrivalDatePa = other.maxArrivalDatePa;
		newItem.minArrivalDatePa = other.minArrivalDatePa;
		newItem.listStatut = other.listStatut;
		newItem.selected = other.selected;
		return newItem;
	}
}
