import { SearchCriteria } from "./searchCriteria";
import { StatusDelivery } from "@app/classes/StatusDeliveryEnum";
import { EbUser } from "@app/classes/user";

export class SearchCriteriaDelivery extends SearchCriteria {
	ebDelLivraisonNum: number;
	ebDelOrderNum: number;
	excludeCanceled: Boolean;
	listPartyOrigin: string[];
	listPartyDestination: string[];
	listOwnerOfTheRequests: string[];
	customerOrderReference: String;
	dateOfGoodsAvailability: Date;
	dateCreationSys: Date;
	ebDelReference: string;
	listStatusDelivery: any;

	constructor() {
		super();

		this.ebDelLivraisonNum = null;
		this.ebDelOrderNum = null;
		this.excludeCanceled = null;
		this.listPartyDestination = null;
		this.listPartyOrigin = null;
		this.listOwnerOfTheRequests = null;
		this.customerOrderReference = null;
		this.dateOfGoodsAvailability = null;
		this.dateCreationSys = null;
		this.ebDelReference = null;
		this.listStatusDelivery = null;
	}
}
