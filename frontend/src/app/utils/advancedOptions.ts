import { CodeLibelle } from "./codeLibelle";
import { Statique } from "@app/utils/statique";

export class AdvancedOptions {
	listTypeDemande: Array<CodeLibelle>;
	listTypeRegion: Array<CodeLibelle>;
	listTransportRef: Array<CodeLibelle>;
	listCustomerReference: Array<CodeLibelle>;
	listDestinations: Array<CodeLibelle>;
	listCarrier: Array<CodeLibelle>;
	listStatut: Array<CodeLibelle>;

	datePickUpFrom: String;
	datePickUpTo: String;
	datePickUpToPlugin: String;
	datePickUpFromPlugin: String;

	constructor() {
		this.listTypeDemande = new Array<CodeLibelle>();
		this.listTypeRegion = new Array<CodeLibelle>();
		this.listTransportRef = new Array<CodeLibelle>();
		this.listCustomerReference = new Array<CodeLibelle>();
		this.listDestinations = new Array<CodeLibelle>();
		this.listCarrier = new Array<CodeLibelle>();
		this.listStatut = new Array<CodeLibelle>();

		this.datePickUpTo = null;
		this.datePickUpFrom = null;
		this.datePickUpToPlugin = null;
		this.datePickUpFromPlugin = null;
	}

	constructorCopy(advancedOptions: AdvancedOptions) {
		this.listTypeDemande = advancedOptions.listTypeDemande;
		this.listTypeRegion = advancedOptions.listTypeRegion;
		this.listTransportRef = advancedOptions.listTransportRef;
		this.listCustomerReference = advancedOptions.listCustomerReference;
		this.listDestinations = advancedOptions.listDestinations;
		this.listCarrier = advancedOptions.listCarrier;
		this.listStatut = advancedOptions.listStatut;

		this.datePickUpToPlugin = advancedOptions.datePickUpToPlugin;
		this.datePickUpFromPlugin = advancedOptions.datePickUpFromPlugin;

		this.datePickUpTo = advancedOptions.datePickUpTo;
		this.datePickUpFrom = advancedOptions.datePickUpFrom;
	}

	formatDates() {
		if (this.datePickUpToPlugin != null)
			this.datePickUpTo = Statique.formatDateFromDatePicker(this.datePickUpToPlugin);
		if (this.datePickUpFromPlugin != null)
			this.datePickUpFrom = Statique.formatDateFromDatePicker(this.datePickUpFromPlugin);
		return this;
	}
}
