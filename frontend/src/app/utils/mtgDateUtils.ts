import { NgbDateStruct, NgbTimeStruct } from "@ng-bootstrap/ng-bootstrap";
import { Statique } from "./statique";

import moment, { Moment } from "moment";
import "moment-timezone";

export class MtgDateUtils {
	public static isoDateFormatDev: string = "YYYY-MM-DD HH:mm:ss ZZ";
	public static isoDateFormatTz: string = "YYYY-MM-DD HH:mm ZZ";
	public static isoDateFormat: string = "YYYY-MM-DD HH:mm";

	public static NB_TZ_MINS: number = 3600000;

	/** ##### START - USED IN DATETIME PICKER COMPONENT */

	// convert date moment to date object for datetimepicker
	public static momentDateToStructure(dt: Moment): NgbDateStruct {
		if (!dt) return { day: null, month: null, year: null };
		let arr = dt.toArray();
		return { day: arr[2], month: arr[1] + 1, year: arr[0] };
	}

	// convert date moment to time object for datetimepicker
	public static momentTimeToStructure(dt: Moment): NgbTimeStruct {
		if (!dt) return { hour: null, minute: null, second: null };
		let arr = dt.toArray();
		return { hour: arr[3], minute: arr[4], second: arr[5] };
	}

	// convert date output from dateTimePicker object into moment with timezone
	public static dateTimePickerToMoment(
		date: NgbDateStruct,
		time: NgbTimeStruct,
		newTz: number
	): Moment {
		// let dt = date ? `${date.year}-${date.month}-${date.day} ${time.hour}:${time.minute}:${time.second} UTC${(Statique.isDefined(oldTz) ? oldTz : newTz)>0?'+':''}${(Statique.isDefined(oldTz) ? oldTz : newTz) != 0 ? (Statique.isDefined(oldTz) ? oldTz : newTz)/60 : ''}` : null;
		let tz =
			Statique.lpad(Math.abs(Math.floor(newTz / 60)), "0", 2) +
			Statique.lpad(Math.abs(newTz % 60), "0", 2);
		let str = `${date.year}-${date.month}-${date.day} ${time.hour}:${time.minute}:${
			time.second
		} UTC${newTz > 0 ? "+" : "-"}${tz}`;
		let m = moment(str);
		return m;
	}

	// convert date to moment with offset
	public static dateToMomentWithOffset(dt: any, offset: number): Moment {
		let d = moment(dt).toDate();
		let str = moment(d.getTime())
			.utcOffset(offset, true)
			.toString();

		return moment(str);
	}

	public static getMomentWithTimezone(dt: number, offset: number): Moment {
		let m = moment(
			dt +
				(MtgDateUtils.NB_TZ_MINS * offset) / 60 -
				(moment().utcOffset() * MtgDateUtils.NB_TZ_MINS) / 60
		);

		return m;
	}

	/** ##### END - USED IN DATETIME PICKER COMPONENT */

	// convert date string to new timezone moment
	public static convertDateStrToTimezone(dt: string, newFz: number, oldFz: number): Moment {
		let res = moment(dt);
		if (oldFz)
			res = res
				.utc(false)
				.utcOffset(oldFz)
				.utc(false)
				.utcOffset(newFz);
		return res;
	}

	public static dateToDaysAgoString(dt: string): string {
		let res = moment(dt);
		return res.fromNow();
	}

	public static dateToUserTzString(dt: string, withTz: boolean): string {
		if (!dt) return "";
		let res = moment(dt);
		let value = res.utcOffset() == -0 ? 0 : (moment().utcOffset() * MtgDateUtils.NB_TZ_MINS) / 60;
		res = moment(res.toDate().getTime() + (MtgDateUtils.NB_TZ_MINS ) / 60 - value);
		return res.format(withTz ? MtgDateUtils.isoDateFormatTz : MtgDateUtils.isoDateFormat);
	}
}
