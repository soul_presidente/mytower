import { Subject } from "rxjs";
import { DataTableDirective } from "angular-datatables";
import { Statique } from "./statique";

export class DataTableConfig {
	public dtOptions: any = {};
	public dtTrigger: Subject<any>;

	constructor(URL?: String) {
		let $this = this;
		if (URL != null)
			this.dtOptions = {
				ajax: {
					url: URL,
					data: {},
				},
				pagingType: "full_numbers",
				pageLength: 25,
				processing: true,
				stateSave: true,
				scrollX: true,
				// ScrollXInner: "100%",
				//scrollY: "1000px",
				//scrollCollapse: true,
				lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
				language: {
					paginate: {
						first: '<i class="fa fa-angle-double-left"></i>',
						last: '<i class="fa fa-angle-double-right"></i>',
						next: '<i class="fa fa-angle-right"></i>',
						previous: '<i class="fa fa-angle-left"></i>',
					},
				},
				dom: "lBfrtip",
				buttons: [
					{
						extend: "colvis",
						//columns: ':gt(0)'
					},
				],
				colReorder: {
					/**
					 * MAIS POURQUOI CET ORDRE LÀ A ETE AJOUTE ?? C'EST CE QUI BLOQUAIT L'ORDRE DES DATATABLES DEPUIS TOUT CE TEMPS !!
					 */
					// order: [1, 0, 2],
					//fixedColumnsRight: 2
				},
			};
		else
			this.dtOptions = {
				pagingType: "full_numbers",
				pageLength: 5,
				stateSave: true,
				lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
				/*language: {
                    paginate: {
                        first: "",
                        last: "",
                        next: "",
                        previous: "",
                    },
                },*/
				dom: "Clfrtip",
				buttons: ["colvis"],
				colReorder: {
					//order: [1, 0, 2],
					//fixedColumnsRight: 2
				},
			};

		this.dtTrigger = new Subject();
	}

	rerender(dtElement: DataTableDirective): void {
		dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
			// Destroy the table first
			dtInstance.destroy();
			// Call the dtTrigger to rerender again
			this.dtTrigger.next();
		});
	}

	/**
	 * Methodes utilitaires pour manipuler les données de la datatable
	 */
	static getState(dtElement: DataTableDirective): any {
		if (dtElement) return Statique.cloneObject(dtElement["dt"]["state"]());
		return null;
	}
	static getRowIndexByEntityId(dtElement, id: number, entityIdName: string) {
		let data = dtElement["dt"]["rows"]().data();
		let i,
			n = data.length;
		for (i = 0; i < n; i++) {
			if (data[i][entityIdName] === id) return i;
		}
		return null;
	}
	static getDataRow(dtElement: DataTableDirective, id: number, entityIdName: string = null) {
		let data = null;
		let index = entityIdName ? this.getRowIndexByEntityId(dtElement, id, entityIdName) : id;
		if (index !== undefined && index != null && dtElement && dtElement["dt"]["row"](index)) {
			data = dtElement["dt"]["row"](index).data();
		}
		return Statique.cloneObject(data);
	}
	static setDataRow(
		dtElement: DataTableDirective,
		data: any,
		id: number,
		entityIdName: string = null
	) {
		let index = entityIdName ? this.getRowIndexByEntityId(dtElement, id, entityIdName) : id;
		if (
			index !== undefined &&
			index != null &&
			dtElement &&
			data &&
			dtElement["dt"]["row"](index)
		) {
			dtElement["dt"]["row"](index).data(data);
		}
	}
	static getStateColumns(dtElement: DataTableDirective) {
		let state = this.getState(dtElement);
		let result = [];
		if (state) {
			let columns = dtElement["dt"]["settings"]().init().columns;
			let i,
				n = columns.length;
			for (i = 0; i < n; i++) {
				if (
					state.columns[i] &&
					columns[i].name &&
					columns[i].name.trim().length > 0 &&
					columns[i].name.indexOf("btn-") < 0 &&
					state.columns[i].visible
				) {
					result.push({
						title: columns[i].title,
						name: columns[i].name,
						order: state.ColReorder.findIndex((colIndex) => colIndex == i),
					});
				}
			}
			result = result.sort((it1, it2) => {
				return it1.order - it2.order;
			});
			return Statique.cloneObject(result);
		}
	}
	static getSettingsColumns(dtElement: DataTableDirective) {
		// window["dtElement"] = dtElement;
		// window["config"] = DataTableConfig;

		let columns = dtElement["dt"]["settings"]().init().columns;

		return Statique.cloneObject(columns);
	}
}
