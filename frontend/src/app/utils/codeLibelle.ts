export class CodeLibelle {
	code: number;
	libelle: String;

	constructor() {
		this.code = null;
		this.libelle = null;
	}
}
