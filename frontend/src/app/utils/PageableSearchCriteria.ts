export class PageableSearchCriteria {
    public start: number;
    public orderedColumn: string;
    public size: number;
    public searchterm: string;
    public ascendant: boolean;
}
