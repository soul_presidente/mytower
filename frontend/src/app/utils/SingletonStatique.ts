import { EbUser } from "@app/classes/user";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import { EbTtPsl } from "@app/classes/Ttpsl";
import { EcCountry } from "@app/classes/country";
import { EcCurrency } from "@app/classes/currency";
import { EbTtCategorieDeviation } from "@app/classes/categorieDeviation";
import { Statique } from "./statique";
import { StatiqueService } from "@app/services/statique.service";
import { EcFuseauxHoraire } from "../classes/fuseauHoraire";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { SearchCriteria } from "./searchCriteria";
import { TypeRequestService } from "@app/services/type-request.service";
import { ModeTransport } from "@app/utils/enumeration";
import { EbTypeTransport } from "@app/classes/EbTypeTransport";
import { Observable } from "rxjs";
import { MTEnum } from "@app/classes/mtEnum";
import { EcMimetype } from "@app/classes/EcMimetype";

let instance = null;

export enum LoadStatus {
	IDLE = 1, // requete non lancé
	LOADING = 2, // en cours de chargement
	LOADED = 3, // chargé
	ERROR = 4, // erreur de chargement
}
export class SingleDataLoader<T> {
	public static MAX_NB_ERROR: number = 20;

	public data: T;
	private status: LoadStatus;
	private nbError: number = 0;
	private stackResolver: Array<Map<String, Function>> = []; // {resolve, reject}

	constructor(loadStatus: LoadStatus = LoadStatus.LOADING) {
		this.data = null;
		this.status = loadStatus;
		this.nbError = 0;
	}

	get canLoad(): boolean {
		return (this.isError || this.isIdle) && this.nbError < SingleDataLoader.MAX_NB_ERROR;
	}
	get isLoaded(): boolean {
		return this.status == LoadStatus.LOADED && !!this.data;
	}
	get isLoading(): boolean {
		return this.status == LoadStatus.LOADING;
	}
	get isError(): boolean {
		return this.status == LoadStatus.ERROR;
	}
	get isIdle(): boolean {
		return this.status == LoadStatus.IDLE;
	}

	setError() {
		this.status = LoadStatus.ERROR;
		this.nbError++;
	}
	setData(pData: T) {
		this.data = pData;
		this.status = LoadStatus.LOADED;

		this.callResolvers();
	}

	addToStack(resolve: Function, reject: Function) {
		let map = new Map<String, Function>();
		map.set("resolve", resolve);
		map.set("reject", reject);
		this.stackResolver.push(map);
	}

	callResolvers() {
		this.stackResolver.forEach((map) => {
			map.get("resolve")(this.data);
		});
		this.stackResolver = new Array<Map<String, Function>>();
	}
}

class SingletonStatique {
	private statiqueService: StatiqueService;
	private typeRequestService: TypeRequestService;

	public typeTransport = null;
	public listVentilation: Array<MTEnum> = new Array<MTEnum>();
	public properties: any;
	private _userConnected: EbUser;
	private _module: number;
	private _lang: string;

	private _listModeTransport: SingleDataLoader<Array<EbModeTransport>>;
	private _listTypeTransport: SingleDataLoader<Array<EbTypeTransport>>;
	private _listTypeTransportAll: SingleDataLoader<Map<number, Array<EbTypeTransport>>>;
	private _listEcCountry: SingleDataLoader<Array<EcCountry>>;
	private _listTtPsl: Array<EbTtPsl>;
	private _listEcCurrency: Array<EcCurrency>;
	private _listCategorieDeviation: Array<EbTtCategorieDeviation>;
	private _listTypeDemande: SingleDataLoader<Array<EbModeTransport>>;
	private _listEcFuseauxHoraire: Array<EcFuseauxHoraire>;
	private _listEbTypeRequest: Array<EbTypeRequestDTO>;

	constructor(
		private pStatiqueService?: StatiqueService,
		private pTypeRequestService?: TypeRequestService,
	) {
		if (instance) {
			if (pStatiqueService) instance.statiqueService = pStatiqueService;
			if (pTypeRequestService) instance.typeRequestService = pTypeRequestService;
			return instance;
		}

		this.statiqueService = pStatiqueService;
		this.typeRequestService = pTypeRequestService;
		instance = this;
	}

	get userConnected(): EbUser {
		let user = new EbUser();
		user.constructorCopy(this._userConnected);
		return user;
	}
	get module(): number {
		return this._module;
	}
	get lang(): string {
		return this._lang;
	}
	set userConnected(user: EbUser) {
		this._userConnected = new EbUser();
		this._userConnected.constructorCopy(user);
	}
	set module(mod: number) {
		this._module = mod;
	}
	set lang(lng: string) {
		this._lang = lng;
	}

	public async getListModeTransport(): Promise<Array<EbModeTransport>> {
		if (this._listModeTransport && this._listModeTransport.isLoaded) {
			return Promise.resolve(
				Statique.cloneObject(this._listModeTransport.data, new Array<EbModeTransport>())
			);
		} else {
			return new Promise<Array<EbModeTransport>>((resolve, reject) => {
				if (!this._listModeTransport)
					this._listModeTransport = new SingleDataLoader<Array<EbModeTransport>>();
				else if (this._listModeTransport && this._listModeTransport.isLoading) {
					this._listModeTransport.addToStack(resolve, reject);
					return;
				}

				this.statiqueService.getListModeTransport().subscribe(
					(res: Array<EbModeTransport>) => {
						this._listModeTransport.setData(res);

						resolve(res);
					},
					(err) => {
						this._listModeTransport.setError();
						reject("SingletonStatique: Error loading listModeTransport");
					}
				);
			});
		}
	}
	public async getListTypeTransport(): Promise<Array<EbTypeTransport>> {
		if (this._listTypeTransport && this._listTypeTransport.isLoaded) {
			return Promise.resolve(
				Statique.cloneObject(this._listTypeTransport.data, new Array<EbTypeTransport>())
			);
		} else {
			return new Promise<Array<EbTypeTransport>>((resolve, reject) => {
				if (!this._listTypeTransport)
					this._listTypeTransport = new SingleDataLoader<Array<EbTypeTransport>>();
				else if (this._listTypeTransportAll && this._listTypeTransportAll.isLoading) {
					this._listTypeTransportAll.addToStack(resolve, reject);
					return;
				}

				this.statiqueService.getListTypeTransports().subscribe(
					(res: Array<EbTypeTransport>) => {
						this._listTypeTransport.setData(res);

						resolve(res);
					},
					(err) => {
						this._listTypeTransport.setError();
						reject("SingletonStatique: Error loading listTypeTransport");
					}
				);
			});
		}
	}

	public async getListTypeTransportByModeTransport(
		modeTransport: ModeTransport
	): Promise<Array<EbTypeTransport>> {
		if (!modeTransport) return null;

		if (this._listTypeTransportAll && this._listTypeTransportAll.isLoaded && this._listTypeTransportAll.data
						&& this._listTypeTransportAll.data[modeTransport]) {
			return Promise.resolve(
				Statique.cloneObject(
					this._listTypeTransportAll.data[modeTransport],
					new Array<EbTypeTransport>()
				)
			);
		} else {
			return new Promise<Array<EbTypeTransport>>((resolve, reject) => {
				if (!this._listTypeTransportAll)
					this._listTypeTransportAll = new SingleDataLoader<Map<number, Array<EbTypeTransport>>>();
				else if (this._listTypeTransportAll && this._listTypeTransportAll.isLoading) {
					this._listTypeTransportAll.addToStack(resolve, reject);
					return;
				}

				this.statiqueService.getListTypeTransports().subscribe(
					(res: Map<number, Array<EbTypeTransport>>) => {
						this._listTypeTransportAll.setData(res);
						resolve(res[modeTransport]);
					},
					(err) => {
						this._listTypeTransportAll.setError();
						reject("SingletonStatique: Error loading listTypeTransport");
					}
				);
			});
		}
	}

	public async getListTypeTransportMap(): Promise<Map<number, Array<EbTypeTransport>>> {
		if (this._listTypeTransportAll && this._listTypeTransportAll.isLoaded) {
			return Promise.resolve(
				Statique.cloneObject(
					this._listTypeTransportAll.data,
					new Map<number, Array<EbTypeTransport>>()
				)
			);
		} else {
			return new Promise<Map<number, Array<EbTypeTransport>>>(async (resolve, reject) => {
				if (!this._listTypeTransportAll)
					this._listTypeTransportAll = new SingleDataLoader<Map<number, Array<EbTypeTransport>>>();
				else if (this._listTypeTransportAll && this._listTypeTransportAll.isLoading) {
					this._listTypeTransportAll.addToStack(resolve, reject);
					return;
				}

				this.statiqueService.getListTypeTransports().subscribe(
					(res: Map<number, Array<EbTypeTransport>>) => {
						this._listTypeTransportAll.setData(res);
						resolve(res);
					},
					(err) => {
						this._listTypeTransportAll.setError();
						reject("SingletonStatique: Error loading listTypeTransportMap");
					}
				);
			});
		}
	}

	public async getListTypeDemande(): Promise<any> {
		if (this._listTypeDemande && this._listTypeDemande.isLoaded) {
			return Promise.resolve(Statique.cloneObject(this._listTypeDemande.data, new Array<any>()));
		} else {
			return new Promise<any>((resolve, reject) => {
				if (!this._listTypeDemande) this._listTypeDemande = new SingleDataLoader<Array<any>>();
				else if (this._listTypeDemande && this._listTypeDemande.isLoading) {
					this._listTypeDemande.addToStack(resolve, reject);
					return;
				}

				this.statiqueService.getListTypeDemande().subscribe(
					(res: Array<any>) => {
						this._listTypeDemande.setData(res);
						resolve(res);
					},
					(err) => {
						this._listTypeDemande.setError();
						reject("SingletonStatique: Error loading listTypeDemande");
					}
				);
			});
		}
	}
	public async getListEcCountry(): Promise<Array<EcCountry>> {
		if (this._listEcCountry && this._listEcCountry.isLoaded) {
			return Promise.resolve(
				Statique.cloneObject(this._listEcCountry.data, new Array<EcCountry>())
			);
		} else {
			return new Promise<Array<EcCountry>>((resolve, reject) => {
				if (!this._listEcCountry) this._listEcCountry = new SingleDataLoader<Array<EcCountry>>();
				else if (this._listEcCountry && this._listEcCountry.isLoading) {
					this._listEcCountry.addToStack(resolve, reject);
					return;
				}

				this.statiqueService.getListPays().subscribe(
					(res: Array<EcCountry>) => {
						this._listEcCountry.setData(res);
						resolve(res);
					},
					(err) => {
						this._listEcCountry.setError();
						reject("SingletonStatique: Error loading listEcCountry");
					}
				);
			});
		}
	}
	public getListTtPsl(): Promise<Array<EbTtPsl>> {
		return new Promise((resolve, reject) => {
			if (this._listTtPsl) resolve(JSON.parse(JSON.stringify(this._listTtPsl)));
			else {
				this.statiqueService.getValueTtPsl().subscribe((res: Array<EbTtPsl>) => {
					// this._listTtPsl = res;
					resolve(res);
				});
			}
		});
	}
	public getListEcCurrency(): Promise<Array<EcCurrency>> {
		return new Promise((resolve, reject) => {
			if (this._listEcCurrency) resolve(JSON.parse(JSON.stringify(this._listEcCurrency)));
			else {
				this.statiqueService.getListEcCurrency().subscribe((res: Array<EcCurrency>) => {
					// this._listEcCurrency = res;
					resolve(res);
				});
			}
		});
	}
	public getListCategorieDeviation(
		typeEvent: number = null
	): Promise<Array<EbTtCategorieDeviation>> {
		return new Promise((resolve, reject) => {
			if (this._listCategorieDeviation)
				resolve(JSON.parse(JSON.stringify(this._listCategorieDeviation)));
			else {
				this.statiqueService
					.getListCategorieDeviation(typeEvent)
					.subscribe((res: Array<EbTtCategorieDeviation>) => {
						// this._listCategorieDeviation = res;
						resolve(res);
					});
			}
		});
	}

	public getListFuseauxHoraire(): Promise<Array<EcFuseauxHoraire>> {
		return new Promise((resolve, reject) => {
			if (this._listEcFuseauxHoraire)
				resolve(JSON.parse(JSON.stringify(this._listEcFuseauxHoraire)));
			else {
				this.statiqueService.getListFuseauxHoraire().subscribe((res: Array<EcFuseauxHoraire>) => {
					resolve(res);
				});
			}
		});
	}

	public getListTypeRequest(): Promise<Array<EbTypeRequestDTO>> {
		return new Promise((resolve, reject) => {
			if (this._listEbTypeRequest)
				resolve(Statique.cloneObject(this._listEbTypeRequest, new Array<EbTypeRequestDTO>()));
			else {
				let criteriaTypeReq = new SearchCriteria();
				criteriaTypeReq.connectedUserNum = this.userConnected.ebUserNum;
				this.typeRequestService
					.getListTypeRequests(criteriaTypeReq)
					.subscribe((res: Array<EbTypeRequestDTO>) => {
						this._listEbTypeRequest = res;
						resolve(res);
					});
			}
		});
	}

	public getIconModeTransportById(modeTransportId: number): string {
		let modeTransportIcon = "";
		if (this._listModeTransport && this._listModeTransport.canLoad) {
			this.getListModeTransport();
		} else if (this._listModeTransport && this._listModeTransport.data) {
			this._listModeTransport.data.forEach((element) => {
				if (element.code == modeTransportId) {
					modeTransportIcon = element.icon;
				}
			});
		}
		return modeTransportIcon;
	}

	getModeTransportString(
		modeTransportCode: any,
		row: any,
		listTypeTransportMap: Map<number, Array<EbTypeTransport>>
	) {
		let modeTransportString = ModeTransport[modeTransportCode];

		const typeTransportList = listTypeTransportMap[modeTransportCode];

		if (typeTransportList && typeTransportList.length > 0) {
			this.typeTransport = typeTransportList.filter((tt) => {
				return tt.ebTypeTransportNum == row.xEbTypeTransport;
			});
		}
		// if typeTransport array contains values
		if (this.typeTransport && this.typeTransport.length > 0) {
			modeTransportString = modeTransportString.concat(" (" + this.typeTransport[0].libelle + ")");
		}
		return modeTransportString;
	}

	public getListMarchandiseDangerousGoods(): Promise<Map<number, string>> {
		return this.statiqueService
			.getListMarchandiseDangerousGood().toPromise();
	}
	public getListMarchandiseClassGoods(): Promise<Array<any>> {
		return this.statiqueService.getListMarchandiseClassGood().toPromise()
				
	}

	public getListFilsFormats(): Observable<Array<EcMimetype>> {
		return this.statiqueService.getListFilesFormats();
	}

	public getListVentilation(): Promise<Array<MTEnum>> {
		return new Promise((resolve) => {
			this.statiqueService.getListGroupeVentilation().subscribe((res) => {
				this.listVentilation = res;
				resolve(res);
			});
		});
	}
}

export const ISingletonStatique = SingletonStatique;

export default new SingletonStatique();
