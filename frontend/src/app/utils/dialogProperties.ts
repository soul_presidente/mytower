import { TypeDialog } from "./enumeration";
import { NgForm } from "@angular/forms";
import { EbEtablissement } from "@app/classes/etablissement";
import { Horaire } from "@app/classes/horaire";

export class DialogProperties {
	typeDialog: TypeDialog;
	modelForm: NgForm;
	errorMsg: String;
	confirmationMsg: String;
	ebEtablissement: EbEtablissement;
	openingHours: Horaire;

	constructor() {
		this.typeDialog = null;
		this.modelForm = null;
		this.errorMsg = null;
		this.confirmationMsg = null;
		this.ebEtablissement = null;
		this.openingHours = null;
	}
}
