import { SearchCriteria } from "./searchCriteria";
import { Statique } from "@app/utils/statique";

export class SearchCriteriaCustom extends SearchCriteria {
	listAgentBrokerPlugin: Array<any>;
	listClearanceCountryPlugin: Array<any>;
	listclearanceIdPlugin: Array<any>;
	listIncotermPlugin: Array<any>;
	listvilleIncotermPlugin: Array<any>;
	listShipperPlugin: Array<any>;

	listAgentBroker: Array<number>;
	listClearanceCountry: Array<number>;
	listclearanceId: Array<number>;
	listIncoterm: Array<string>;
	listvilleIncoterm: Array<number>;
	listShipper: Array<number>;
	ebCustomDeclarationNum: number;

	requestDateFrom: String;
	requestDateFromPlugin: String;

	requestDateTo: String;
	requestDateToPlugin: String;

	constructor() {
		super();

		this.listAgentBrokerPlugin = new Array<any>();
		this.listClearanceCountryPlugin = new Array<any>();
		this.listclearanceIdPlugin = new Array<any>();
		this.listIncotermPlugin = new Array<any>();
		this.listvilleIncotermPlugin = new Array<any>();
		this.listShipperPlugin = new Array<any>();

		this.listAgentBroker = new Array<number>();
		this.listClearanceCountry = new Array<number>();
		this.listclearanceId = new Array<number>();
		this.listIncoterm = Array<string>();
		this.listvilleIncoterm = Array<number>();
		this.listShipper = Array<number>();

		this.requestDateFrom = null;
		this.requestDateFromPlugin = null;

		this.requestDateTo = null;
		this.requestDateToPlugin = null;
	}

	constructorCopy(advancedOptions: SearchCriteriaCustom) {
		this.listAgentBrokerPlugin = advancedOptions.listAgentBroker;
		this.listClearanceCountryPlugin = advancedOptions.listClearanceCountry;
		this.listclearanceIdPlugin = advancedOptions.listclearanceId;
		this.listIncotermPlugin = advancedOptions.listIncoterm;
		this.listvilleIncotermPlugin = advancedOptions.listvilleIncoterm;
		this.listShipperPlugin = advancedOptions.listShipper;

		if (advancedOptions.listAgentBrokerPlugin) {
			advancedOptions.listAgentBrokerPlugin.forEach((agentBroker, index) => {
				this.listAgentBroker.push(agentBroker.ebUserNum);
			});
		}

		if (advancedOptions.listClearanceCountryPlugin) {
			advancedOptions.listClearanceCountryPlugin.forEach((ClearanceCou, index) => {
				this.listClearanceCountry.push(ClearanceCou.ecCountryNum);
			});
		}

		if (advancedOptions.listclearanceIdPlugin) {
			advancedOptions.listclearanceIdPlugin.forEach((clearanceID, index) => {
				this.listclearanceId.push(clearanceID.ebCustomDeclarationNum);
			});
		}

		if (advancedOptions.listIncotermPlugin) {
			advancedOptions.listIncotermPlugin.forEach((Incoterm, index) => {
				this.listIncoterm.push(Incoterm.ebIncotermNum);
			});
		}

		if (advancedOptions.listvilleIncotermPlugin) {
			advancedOptions.listvilleIncotermPlugin.forEach((villeIncoterm, index) => {
				this.listvilleIncoterm.push(villeIncoterm.ebCustomDeclarationNum);
			});
		}

		if (advancedOptions.listShipperPlugin) {
			advancedOptions.listShipperPlugin.forEach((shipper, index) => {
				this.listShipper.push(shipper.ebUserNum);
			});
		}

		this.requestDateFromPlugin = advancedOptions.requestDateFromPlugin;
		this.requestDateFrom = advancedOptions.requestDateFrom;

		this.requestDateToPlugin = advancedOptions.requestDateToPlugin;
		this.requestDateTo = advancedOptions.requestDateTo;
	}

	formatDates() {
		if (this.requestDateFromPlugin != null)
			this.requestDateFrom = Statique.formatDateFromDatePicker(this.requestDateFromPlugin);
		if (this.requestDateToPlugin != null)
			this.requestDateTo = Statique.formatDateFromDatePicker(this.requestDateToPlugin);
		return this;
	}
}
