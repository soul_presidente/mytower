import { SearchCriteria } from "./searchCriteria";
import { Statique } from "@app/utils/statique";

export class SearchCriteriaFreightAudit extends SearchCriteria {
	listDestCountryPlugin: Array<any>;
	listDestCountry: Array<string>;
	listOrigineCountryPlugin: Array<any>;
	listOrigineCountry: Array<number>;
	listModeTransportPlugin: Array<any>;
	listModeTransport: Array<number>;
	listTypeDemandePlugin: Array<any>;
	listTypeDemande: Array<number>;

	dateDeliveryFrom: String;
	dateDeliveryTo: String;
	dateDeliveryToPlugin: String;
	dateDeliveryFromPlugin: String;

	refDemande: String;
	refDemandePlugin: String;

	constructor() {
		super();
		this.listDestCountry = new Array<string>();
		this.listDestCountryPlugin = new Array<any>();
		this.listOrigineCountry = new Array<number>();
		this.listOrigineCountryPlugin = new Array<any>();
		this.listModeTransport = new Array<number>();
		this.listModeTransportPlugin = new Array<any>();
		this.listTypeDemande = new Array<number>();
		this.listTypeDemandePlugin = new Array<any>();

		this.dateDeliveryTo = null;
		this.dateDeliveryFrom = null;
		this.dateDeliveryToPlugin = null;
		this.dateDeliveryFromPlugin = null;

		this.refDemande = null;
		this.refDemandePlugin = null;
	}

	constructorCopy(advancedOptions: SearchCriteriaFreightAudit) {
		super.constructorCopy(advancedOptions);

		this.listDestCountryPlugin = advancedOptions.listDestCountryPlugin;
		this.listOrigineCountryPlugin = advancedOptions.listOrigineCountryPlugin;
		this.listModeTransportPlugin = advancedOptions.listModeTransportPlugin;
		this.listTypeDemandePlugin = advancedOptions.listTypeDemandePlugin;

		this.refDemandePlugin = advancedOptions.refDemandePlugin;

		this.refDemande = advancedOptions.refDemande;

		//  this.refDemande.push(refDemande);

		this.dateDeliveryToPlugin = advancedOptions.dateDeliveryToPlugin;
		this.dateDeliveryFromPlugin = advancedOptions.dateDeliveryFromPlugin;
		this.dateDeliveryTo = advancedOptions.dateDeliveryTo;
		this.dateDeliveryFrom = advancedOptions.dateDeliveryFrom;

		if (advancedOptions.listTypeDemandePlugin) {
			advancedOptions.listTypeDemandePlugin.forEach((listTypeDemande, index) => {
				this.listTypeDemande.push(listTypeDemande.code);
			});
		}
		if (advancedOptions.listModeTransportPlugin) {
			advancedOptions.listModeTransportPlugin.forEach((listModeTransport, index) => {
				this.listModeTransport.push(listModeTransport.code);
			});
		}
		if (advancedOptions.listDestCountryPlugin) {
			advancedOptions.listDestCountryPlugin.forEach((listDestCountry, index) => {
				this.listDestCountry.push(listDestCountry.libelle);
			});
		}
		if (advancedOptions.listOrigineCountryPlugin) {
			advancedOptions.listOrigineCountryPlugin.forEach((listOrigineCountry, index) => {
				this.listOrigineCountry.push(listOrigineCountry.libelle);
			});
		}
	}

	formatDates() {
		if (this.dateDeliveryToPlugin != null)
			this.dateDeliveryTo = Statique.formatDateFromDatePicker(this.dateDeliveryToPlugin);
		if (this.dateDeliveryFromPlugin != null)
			this.dateDeliveryFrom = Statique.formatDateFromDatePicker(this.dateDeliveryFromPlugin);
		return this;
	}
}
