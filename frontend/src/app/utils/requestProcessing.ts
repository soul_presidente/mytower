export class RequestProcessing {
	private initialContent: any;

	public beforeSendRequest(elementHTML: any, isOuterHTML: boolean = false) {
		// For example desactivate the btn
		elementHTML.target.disabled = true;
		this.initialContent = elementHTML.target.innerHTML;
		elementHTML.target.innerHTML = spinnerElement;
	}

	public afterGetResponse(elementHTML: any, isOuterHTML: boolean = false) {
		// For example enable the btn
		elementHTML.target.disabled = false;
		elementHTML.target.innerHTML = this.initialContent;
	}

	public beforeSendRequestBtn(elementHTML: any) {
		// For example desactivate the btn
		elementHTML.disabled = true;
		this.initialContent = elementHTML.innerHTML;
		elementHTML.innerHTML = spinnerElement;
	}

	public afterGetResponseBtn(elementHTML: any) {
		// For example enable the btn
		elementHTML.disabled = false;
		elementHTML.innerHTML = this.initialContent;
	}

	constructor() {
		this.initialContent = "";
	}
}

const spinnerElement: string = "<i class='fa fa-spinner fa-spin fa-15x' style='color:black'></i>";
