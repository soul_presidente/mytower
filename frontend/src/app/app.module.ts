import { BrowserModule } from "@angular/platform-browser";
import { NgModule, APP_INITIALIZER } from "@angular/core";
import { DatePipe } from "@angular/common";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { SortablejsModule } from "ngx-sortablejs";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { HttpClient, HTTP_INTERCEPTORS } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppComponent } from "./app.component";
import { SideBarComponent } from "@app/shared/side-bar/side-bar.component";
import { UserService } from "@app/services/user.service";
import { AuthenticationService } from "@app/services/authentication.service";
import { CorpModule } from "@app/corp/corp.module";
import { HeaderModule } from "@app/shared/header/header.module";
import { StatiqueService } from "@app/services/statique.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { CostCenterService } from "@app/services/cost-center.service";
import { appRouter } from "./app.routes";
import { InscriptionService } from "@app/services/inscription.service";
import { SharedModule } from "@app/shared/module/shared.module";
import { NgbActiveModal, NgbModule, NgbTabsetModule } from "@ng-bootstrap/ng-bootstrap";
import { TOKEN_NAME } from "@app/services/auth.constant";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";
import { PricingComponent } from "@app/component/pricing-booking/pricing/pricing.component";
import { NgSelectModule } from "@ng-select/ng-select";
import { AdvancedSearchModule } from "@app/shared/advanced-search/advanced-search.module";
import { SavedFormsModule } from "@app/component/saved-forms/saved-forms.module";
import { ArchwizardModule } from "angular-archwizard";
import { DialogsModule } from "@app/shared/dialogs/dialog.module";
import { SavedSearchComponent } from "@app/component/saved-forms/saved-search/saved-search.component";
import { DemandeprefereComponent } from "@app/component/saved-forms/demandeprefere/demandeprefere.component";
import { HistoryComponent } from "@app/component/saved-forms/history/history.component";
import { ExportService } from "@app/services/export.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { CompagnieService } from "@app/services/compagnie.service";
import { TransportManagementComponent } from "@app/component/pricing-booking/transport-management/transport-management.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";
import { AccountComponent } from "@app/component/account/account.component";
import { LoginComponent } from "@app/component/account/login/login.component";
import { ResetPasswordComponent } from "@app/component/account/login/reset-password/reset-password.component";
import { PricingBookingComponent } from "@app/component/pricing-booking/pricing-booking.component";
import { SharedComponentComponent } from "@app/component/pricing-booking/shared-component/shared-component.component";
import { FreightAuditService } from "@app/services/freight-audit.service";
import { FavoriService } from "@app/services/favori.service";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { TrackTraceService } from "@app/services/trackTrace.service";
import { DouaneService } from "@app/services/douane.service";
import { ReceiptSchedulingService } from "@app/services/receipt-scheduling.service";
import { dockManagementModule } from "@app/component/dock-management/dock-management.module";
import { CollectionService } from "@app/services/collection.service";
import { TypesDocumentService } from "@app/services/types-document.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { DatatableFilterService } from "@app/services/datatable-filter.service";
import { EmailHistoriqueService } from "@app/services/emailHistorique.service";
import { RedirectDossier } from "@app/corp/redirection-dossier-inexistant/redirection-dossier-inexistant.component";
import { OverlayModule } from "@angular/cdk/overlay";
import { ThemesConfigService } from "@app/services/themes-config.service";
import { ReceiptSchedulingComponent } from "@app/component/receipt-scheduling/receipt-scheduling.component";
import { MessageService } from "primeng/api";

import { DocumentsModule } from "@app/component/documents/documents.module";
import { SharedComponentModule } from "@app/component/pricing-booking/shared-component/shared-component.module";
import { environment } from "@src/environments/environment";
import { SmartTrackingComponent } from "./component/account/login/smart-tracking/smart-tracking.component";
import { ConfigPslCompanyService } from "./services/config-psl-company.service";
import { DocumentTemplatesService } from "./services/document-templates.service";
import { UserProfileService } from "@app/services/user-profile.service";
import { TypeRequestService } from "./services/type-request.service";
import { NgxUploaderModule } from "ngx-uploader";
import { HeaderService } from "./services/header.service";
import { LoginChanelComponent } from "@app/component/account/login-chanel/login-chanel.component";
import { ResetPasswordChanelComponent } from "@app/component/account/login-chanel/reset-password-chanel/reset-password-chanel.component";
import { LoginCommonModule } from "@app/component/account/common/login.common.module";
import { ToastModule } from "primeng/toast";
import { MessageModule, MessagesModule } from "primeng/primeng";
import { DialogModule } from "primeng/dialog";
import { LoginTalaComponent } from "./component/account/login-tala/login-tala.component";
import { ResetPasswordTalaComponent } from "./component/account/login-tala/reset-password-tala/reset-password-tala.component";
import { CronjobMonitoringService } from "./services/cronjob-monitoring.service";
import { TypeGoodsService } from "./services/type-goods.service";
import { ConfigIncotermService } from "./services/config-incoterm.service";
import { ConfigTypeFluxService } from "./services/config-type-flux.service";
import { AclService } from "./services/acl.service";
import { UserTagService } from "./services/userTag.service";
import { JwtTokenInterceptor } from "./utils/jwt-token-interceptor";
import { QrConsolidationService } from "./services/qrConsolidation.service";
import { PrMtcTransporteurService } from "@app/services/pr-mtc-transporteur.service";
import { WeekTemplateService } from "./services/WeekTemplateService";
import { PlanTransportNewService } from "./services/planTransportNew.service";
import { MappingCodePslService } from "./services/mapping-code-psl.service";
import { registerLocaleData } from "@angular/common";
import localeFr from "@angular/common/locales/fr";
import { ControlRuleService } from "./services/control-rule.service";
import { NotificationService } from "./services/notification.service";
import { MarkdownModule } from "ngx-markdown";

registerLocaleData(localeFr, "fr-FR");
import { WebSocketService } from "./services/web-socket.service";

export function HttpLoaderFactory(http: HttpClient) {
	return new TranslateHttpLoader(http, "../assets/i18n/", ".json");
}

/* export function authHttpServiceFactory(http: Http) {
	return new AuthHttp(new AuthConfig({
		headerPrefix: 'Bearer',
		tokenName: TOKEN_NAME,
		globalHeaders: [{ 'Content-Type': 'application/json' }],
		noJwtError: false,
		noTokenScheme: true,
		tokenGetter: (() => localStorage.getItem(TOKEN_NAME))
	}), http);
} */

export function jwtTokenGetter() {
	return localStorage.getItem(TOKEN_NAME);
}

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		LoginChanelComponent,
		LoginTalaComponent,
		ResetPasswordChanelComponent,
		ResetPasswordComponent,
		ResetPasswordTalaComponent,
		SideBarComponent,
		PricingComponent,
		DemandeprefereComponent,
		SavedSearchComponent,
		HistoryComponent,
		TransportManagementComponent,
		AccountComponent,
		PricingBookingComponent,
		SharedComponentComponent,
		RedirectDossier,
		ReceiptSchedulingComponent,
		SmartTrackingComponent,
	],

	imports: [
		/* Replaced by custom interceptor defined un shared module for the moment, some issue with multi module app */
		/*JwtModule.forRoot({
			config: {
				tokenGetter: jwtTokenGetter,
				whitelistedDomains: environment.jwtWhitelist
			}
		}),*/
		BrowserModule,
		BrowserAnimationsModule,
		NgSelectModule,
		ArchwizardModule,
		SharedModule,
		TranslateModule.forRoot({
			loader: {
				provide: TranslateLoader,
				useFactory: HttpLoaderFactory,
				deps: [HttpClient],
			},
		}),
		MarkdownModule.forRoot(),
		appRouter,
		HeaderModule,
		CorpModule,
		ToastModule,
		MessageModule,
		MessagesModule,
		AdvancedSearchModule,
		SavedFormsModule,
		DialogsModule,
		dockManagementModule,
		OverlayModule,
		DocumentsModule,
		SharedComponentModule,
		NgxUploaderModule,
		LoginCommonModule,
		NgbTabsetModule,
		DialogModule,
		SortablejsModule,
	],

	providers: [
		{
			provide: HTTP_INTERCEPTORS,
			useClass: JwtTokenInterceptor,
			multi: true,
		},
		InscriptionService,
		AuthenticationService,
		UserService,
		UserTagService,
		StatiqueService,
		EtablissementService,
		TypesDocumentService,
		CostCenterService,
		DatePipe,
		generaleMethodes,
		ExportService,
		ModalService,
		NgbActiveModal,
		CompagnieService,
		UserAuthGuardService,
		FreightAuditService,
		FavoriService,
		QualityManagementService,
		TrackTraceService,
		CollectionService,
		DouaneService,
		TransportationPlanService,
		PrMtcTransporteurService,
		DatatableFilterService,
		EmailHistoriqueService,
		ThemesConfigService,
		ReceiptSchedulingService,
		MessageService,
		ConfigPslCompanyService,
		ConfigIncotermService,
		ConfigTypeFluxService,
		DocumentTemplatesService,
		UserProfileService,
		TypeRequestService,
		HeaderService,
		CronjobMonitoringService,
		TypeGoodsService,
		AclService,
		QrConsolidationService,
		WeekTemplateService,
		PlanTransportNewService,
		MappingCodePslService,
		ControlRuleService,
		NotificationService,
		WebSocketService,
	],
	bootstrap: [AppComponent],
})
export class AppModule {
	constructor() {
		console.log("Base Api :" + environment.apiUrl + " production? " + environment.production);
	}
}
