import { EcCountry } from "./country";
import { EbEtablissement } from "./etablissement";
import { EbUser } from "./user";

export class EbDestinationCountry {
	ebDestinationCountryNum: number;
	generalComments: String;
	processLeadTimes: String;
	preferredPartners: String;
	documentationRequired: String;
	country: EcCountry;
	lastDateModified: Date;
	lastUserModified: EbUser;
	createdBy: EbUser;
	etablissement: EbEtablissement;

	constructor() {
		this.init();
	}

	private init() {
		this.ebDestinationCountryNum = null;
		this.generalComments = null;
		this.processLeadTimes = null;
		this.preferredPartners = null;
		this.documentationRequired = null;
		this.country = null;
		this.lastDateModified = null;
		this.lastUserModified = null;
		this.createdBy = null;
		this.etablissement = null;
	}

	constructorCopy(ebDestinationCountry: EbDestinationCountry) {
		this.init();
		this.ebDestinationCountryNum = ebDestinationCountry.ebDestinationCountryNum;
		this.generalComments = ebDestinationCountry.generalComments;
		this.processLeadTimes = ebDestinationCountry.processLeadTimes;
		this.preferredPartners = ebDestinationCountry.preferredPartners;
		this.documentationRequired = ebDestinationCountry.documentationRequired;
		this.lastDateModified = ebDestinationCountry.lastDateModified;

		if (ebDestinationCountry.lastUserModified != null) {
			this.lastUserModified = new EbUser();
			this.lastUserModified.ebUserNum = ebDestinationCountry.lastUserModified.ebUserNum;
			this.lastUserModified.nom = ebDestinationCountry.lastUserModified.nom;
		}

		if (ebDestinationCountry.createdBy != null) {
			this.createdBy = new EbUser();
			this.createdBy.ebUserNum = ebDestinationCountry.createdBy.ebUserNum;
			this.createdBy.nom = ebDestinationCountry.createdBy.nom;
		}
	}
}
