export class CronjobMonitoring {
	lastExecutionDate: Date;
	nextExecutionDate: Date;
	status: Boolean;
	jobExecutionFailed: Boolean;

	constructor() {
		this.lastExecutionDate = null;
		this.nextExecutionDate = null;
		this.status = null;
		this.jobExecutionFailed = null;
	}
}
