import { DataNestedItemsColumn } from "./dataNestedItemsColumn";
import { EbMarchandise } from "./marchandise";

export class DataNestedItems {
	listColumns: Array<DataNestedItemsColumn>;
	childreen: Array<DataNestedItems>;
	data: EbMarchandise;

	constructor() {
		this.listColumns = new Array<DataNestedItemsColumn>();
		this.childreen = new Array<DataNestedItems>();
		this.data = new EbMarchandise();
	}
}
