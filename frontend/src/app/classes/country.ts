export class EcCountry {
	ecCountryNum: number;
	libelle: string;
	code: String;
	codeLibelle: String;
	libelleCode: String;

	constructor() {
		this.ecCountryNum = null;
		this.libelle = null;
		this.code = null;
	}

	constructorCopy(country: EcCountry) {
		this.ecCountryNum = country.ecCountryNum;
		this.libelle = country.libelle;
		this.code = country.code;
		this.codeLibelle = country.codeLibelle;
		this.libelleCode = country.libelleCode;
	}

	public static create(ecCountryNum, libelle, code?): EcCountry {
		let c = new EcCountry();
		c.ecCountryNum = ecCountryNum;
		c.libelle = libelle;
		c.code = code;
		return c;
	}
}
