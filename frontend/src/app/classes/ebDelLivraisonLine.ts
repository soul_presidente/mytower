import { OrderLine } from "./ebDelOrderLine";
import { EbTypeUnit } from "./typeUnit";

export class DeliveryLine {
	ebDelLivraisonLineNum: number;

	idLivraisonLine: number;

	itemNumber: number;

	itemName: string;

	customsCode: string;

	batch: string;

	refArticle: string;

	articleName: string;

	originCountry: string;

	quantity: number;

	dateAvailability: string;

	xEbOrderLigne: OrderLine;

	xEbtypeOfUnit: EbTypeUnit;

	palletHeight: number;
	partNumber: string;
	serialNumber: string;
	dateOfRequest: Date;
	priority: number;
	comment: string;
	specification: string;

	price: number;
	currency: number;
	eccn: string;

	constructor() {
		this.ebDelLivraisonLineNum = null;

		this.idLivraisonLine = null;

		this.customsCode = null;

		this.batch = null;

		this.refArticle = null;

		this.articleName = null;

		this.originCountry = null;

		this.quantity = null;

		this.dateAvailability = null;

		this.xEbOrderLigne = null;

		this.xEbtypeOfUnit = null;

		this.palletHeight = null;
		this.partNumber = null;
		this.serialNumber = null;
		this.dateOfRequest = null;
		this.priority = null;
		this.comment = null;
		this.specification = null;

		this.itemNumber = null;
		this.itemName = null;

		this.price = null;
		this.currency = null;
		this.eccn = "";
	}
}
