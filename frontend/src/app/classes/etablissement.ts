import { EcCountry } from "./country";
import { EbCompagnie, EbCompagnieLite } from "@app/classes/compagnie";
import { EbAdresse } from "./adresse";
import { EbCostCenter } from "./costCenter";
import { EbCategorie } from "./categorie";
import { Statique } from "@app/utils/statique";

export class EbEtablissement {
	ebEtablissementNum: number;
	siret: string;
	nic: string;
	nom: string;
	adresse: string;
	telephone: string;
	email: string;
	siteWeb: string;
	logo: string;
	ebCompagnie: EbCompagnie;
	listAdresses: Array<EbAdresse>;
	costCenters: Array<EbCostCenter>;
	categories: Array<EbCategorie>;
	city: string;
	ecCountry: EcCountry;
	nmbrUtilisateurs: number;
	service: number;
	role: number;
	typeEtablissement: string;
	canShowPrice: boolean;

	constructor() {
		this.init();
	}

	private init() {
		this.ebEtablissementNum = null;
		this.siret = null;
		this.nom = null;
		this.adresse = null;
		this.telephone = null;
		this.email = null;
		this.siteWeb = null;
		this.logo = null;
		this.ebCompagnie = new EbCompagnie();
		this.listAdresses = new Array<EbAdresse>();
		this.ecCountry = null;
		this.costCenters = new Array<EbCostCenter>();
		this.nmbrUtilisateurs = null;
		this.service = null;
		this.role = null;
		this.categories = new Array<EbCategorie>();
		this.city = null;
		this.nic = null;
		this.typeEtablissement = null;
		this.canShowPrice = true;
	}

	constructorCopy(ebEtablissement: EbEtablissement) {
		this.init();
		if (ebEtablissement == null) {
			return;
		}

		this.ebEtablissementNum = ebEtablissement.ebEtablissementNum;
		this.siret = ebEtablissement.siret;
		this.nom = ebEtablissement.nom;
		this.adresse = ebEtablissement.adresse;
		this.telephone = ebEtablissement.telephone;
		this.email = ebEtablissement.email;
		this.siteWeb = ebEtablissement.siteWeb;
		this.logo = ebEtablissement.logo;
		this.ebCompagnie = ebEtablissement.ebCompagnie;
		this.listAdresseCopy(ebEtablissement.listAdresses);
		this.ecCountry = ebEtablissement.ecCountry;
		this.costCentersCopy(ebEtablissement.costCenters);
		this.nmbrUtilisateurs = ebEtablissement.nmbrUtilisateurs;
		this.service = ebEtablissement.service;
		this.role = ebEtablissement.role;
		this.categorieCopy(ebEtablissement.categories);
		this.city = ebEtablissement.city;
		this.nic = ebEtablissement.nic;
		this.canShowPrice = ebEtablissement.canShowPrice;
		this.typeEtablissement = ebEtablissement.typeEtablissement;

		// this.listUsersCopy(ebEtablissement.listUsers);
	}

	constructorCopyLite(ebEtablissement: EbEtablissement) {
		this.init();
		this.ebEtablissementNum = ebEtablissement.ebEtablissementNum;
	}

	listAdresseCopy(listAdresses: Array<EbAdresse>) {
		let _adresse;
		if (Statique.isDefined(listAdresses) && listAdresses.length !== 0) {
			for (let adresse of listAdresses) {
				_adresse = new EbAdresse();
				_adresse.constructorCopy(adresse);
				this.listAdresses.push(_adresse);
			}
		} else {
			this.listAdresses = new Array<EbAdresse>();
			// this.adresses.push(new Adresse());
			// push only when necessary in your component
		}
	}

	costCentersCopy(costCenters: Array<EbCostCenter>) {
		let _costCenter;
		if (Statique.isDefined(costCenters) && costCenters.length !== 0) {
			for (let costCenter of costCenters) {
				_costCenter = new EbCostCenter();
				_costCenter.constructorCopy(costCenter);
				this.costCenters.push(_costCenter);
			}
		} else {
			this.costCenters = new Array<EbCostCenter>();
		}
	}

	categorieCopy(categories: Array<EbCategorie>) {
		let _categorie;
		if (Statique.isDefined(categories) && categories.length !== 0) {
			for (let categorie of categories) {
				_categorie = new EbCategorie();
				_categorie.constructorCopy(categorie);
				this.categories.push(_categorie);
			}
		} else {
			this.categories = new Array<EbCategorie>();
		}
	}

	public static create(ebEtablissementNum: number): EbEtablissement {
		let et = new EbEtablissement();
		et.ebEtablissementNum = ebEtablissementNum;
		return et;
	}
	/*
    listUsersCopy(listUsers: Array<EbUser>) {
        let _user;
        if (listUsers !== undefined && listUsers !== null && listUsers.length !== 0) {
            for (let user of listUsers) {
                _user = new EbUser();
                _user.constructorCopy(user);
                console.log(_user);
                this.listUsers.push(_user);
            }
        }
        else {
            this.listUsers = new Array<EbUser>();
        }
    }*/
}

export class EbEtablissementLite {
	ebEtablissementNum: number;
	ebCompagnie: EbCompagnieLite;

	constructor(ebEtablissement: EbEtablissement = null) {
		if (ebEtablissement && ebEtablissement.ebEtablissementNum) {
			this.ebEtablissementNum = ebEtablissement.ebEtablissementNum;
		}
		if (
			ebEtablissement &&
			ebEtablissement.ebCompagnie &&
			ebEtablissement.ebCompagnie.ebCompagnieNum
		) {
			this.ebCompagnie = new EbCompagnieLite(ebEtablissement.ebCompagnie);
		}
	}
}
