import { EbCompagnie } from "./compagnie";
import { EbTtCategorieDeviation } from './categorieDeviation';
import { EbTtCompanyPsl } from './ttCompanyPsl';

export class EdiMapPsl {
    ediMapPslNum: number;
    dateCreationSys: Date;
    dateMajSys: Date;
    codePslNormalise: String;
    codePslTransporteur: String;
    typeEvent: number;
    xEbCompany: EbCompagnie;
    xEbCompanyTransporteur: EbCompagnie;
    xEbCompanyPsl: EbTtCompanyPsl;
    numCodeTransporteur: number;
    numCodeRaison: number;
    codePslTransporteurDescriptif: String;
    xEbCategorieDeviation: EbTtCategorieDeviation;
    codeRaison: String;
    categorieDeviationLibelle: String;
    codeRaisonDescriptif: String;

	constructor() {
		this.ediMapPslNum = null;
        this.dateCreationSys = new Date();
        this.dateMajSys = new Date();
        this.codePslNormalise = null;
        this.codePslTransporteur = null;
        this.typeEvent = null;
        this.xEbCompany = null;
        this.xEbCompanyTransporteur = null;
        this.xEbCompanyPsl = null;
        this.numCodeTransporteur = null;
        this.numCodeRaison = null;
        this.codePslTransporteurDescriptif = null;
        this.xEbCategorieDeviation = null;
        this.codeRaison = null;
        this.categorieDeviationLibelle = null;
        this.codeRaisonDescriptif = null;
	}

	constructorCopy(ediMapPsl: EdiMapPsl) {
		return this.copy(ediMapPsl);
	}

	copy(ediMapPsl: EdiMapPsl) {
        this.ediMapPslNum = ediMapPsl.ediMapPslNum;
        this.dateCreationSys = ediMapPsl.dateCreationSys;
        this.dateMajSys = ediMapPsl.dateMajSys;
        this.codePslNormalise = ediMapPsl.codePslNormalise;
        this.codePslTransporteur = ediMapPsl.codePslTransporteur;
        this.typeEvent = ediMapPsl.typeEvent;
        this.xEbCompany = ediMapPsl.xEbCompany;
        this.xEbCompanyTransporteur = ediMapPsl.xEbCompanyTransporteur;
        this.xEbCompanyPsl = ediMapPsl.xEbCompanyPsl;
        this.numCodeTransporteur = ediMapPsl.numCodeTransporteur;
        this.numCodeRaison = ediMapPsl.numCodeRaison;
        this.codePslTransporteurDescriptif = ediMapPsl.codePslTransporteurDescriptif;
        this.xEbCategorieDeviation = ediMapPsl.xEbCategorieDeviation;
        this.codeRaison = ediMapPsl.codeRaison;
        this.categorieDeviationLibelle = ediMapPsl.categorieDeviationLibelle;
        this.codeRaisonDescriptif = ediMapPsl.codeRaisonDescriptif;
	}
}
