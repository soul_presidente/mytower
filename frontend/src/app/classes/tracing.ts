import { EbMarchandise } from "./marchandise";
import { EbDemande } from "./demande";
import { EcCountry } from "./country";
import { EbUser } from "./user";
import { EbEtablissement } from "./etablissement";
import { EbCompagnie } from "./compagnie";
import { EbPslApp } from "./pslApp";
import { TTEvent } from "./tTEvent";
import { Statique } from "@app/utils/statique";
import { MTEnum } from "./mtEnum";
import { EbFlag } from "./ebFlag";
import { EbTtCompanyPsl } from "./ttCompanyPsl";
import { RegroupmtPslEvent } from "@app/classes/regroupmtPslEvent";
import { EbTtSchemaPsl } from "./EbTtSchemaPsl";

export class EbTracing {
	ebTtTracingNum: number;
	xEbMarchandise: EbMarchandise;
	listEbPslApp: Array<EbPslApp>;
	dateCreation: Date;
	xEbDemande: EbDemande;
	xEcCountryOrigin: EcCountry;
	xEcCountryDestination: EcCountry;
	datePickup: Date;
	dateModification: Date;
	toDelete: boolean;
	toAdd: boolean;

	totalWeight: number;
	refTransport: string;
	customerReference: string;
	partNumber: string;
	serialNumber: string;
	shippingType: string;
	orderNumber: string;
	customRef: String;
	xEbTransporteur: EbUser;
	xEbEtablissementTransporteur: EbEtablissement;
	nomEtablissementTransporteur: string;
	xEbCompagnieTransporteur: EbCompagnie;

	listEvent: Array<TTEvent>;
	listPslEvent: Array<RegroupmtPslEvent>;
	xEbChargeur: EbUser;
	xEbEtablissementChargeur: EbEtablissement;
	nomEtablissementChargeur: string;
	xEcModeTransport: number;
	xEbChargeurCompagnie: EbCompagnie;
	late: number;

	configPsl: string;
	leadtime: number;

	originPlace: string;
	destinationPlace: string;

	listFlag: String;

	listEbFlagDTO: Array<EbFlag>;

	listPsl: Array<EbTtCompanyPsl>;

	codeAlphaPslCourant: string;
	libellePslCourant: string;
	percentPslCourant: number;

	codeAlphaLastPsl: string;
	libelleLastPsl: string;
	dateLastPsl: Date;

	xEbSchemaPsl: EbTtSchemaPsl;
	transportStatusPerUnit: string;
	expectedDateLastPsl: Date;

	constructor() {
		this.ebTtTracingNum = null;
		this.xEbMarchandise = new EbMarchandise();
		this.listEbPslApp = new Array<EbPslApp>();
		this.dateCreation = null;
		this.xEbDemande = new EbDemande();
		this.xEcCountryOrigin = new EcCountry();
		this.xEcCountryDestination = new EcCountry();
		this.datePickup = null;
		this.dateModification = null;

		this.totalWeight = null;
		this.refTransport = null;
		this.customerReference = null;
		this.partNumber = null;
		this.serialNumber = null;
		this.orderNumber = null;
		this.shippingType = null;
		this.customRef = null;
		this.xEcModeTransport = null;
		this.xEbTransporteur = new EbUser();
		this.xEbEtablissementTransporteur = new EbEtablissement();
		this.nomEtablissementTransporteur = null;
		this.xEbCompagnieTransporteur = new EbCompagnie();
		this.toDelete = false;
		this.toAdd = false;

		this.listEvent = new Array<TTEvent>();
		this.listPslEvent = new Array<RegroupmtPslEvent>();
		this.xEbChargeur = new EbUser();
		this.xEbEtablissementChargeur = new EbEtablissement();
		this.nomEtablissementChargeur = null;
		this.xEbChargeurCompagnie = new EbCompagnie();
		this.late = null;

		this.configPsl = null;
		this.leadtime = null;

		this.originPlace = null;
		this.destinationPlace = null;
		this.listFlag = null;
		this.listEbFlagDTO = null;

		this.dateLastPsl = null;
		this.libelleLastPsl = null;
		this.listPsl = null;
		this.codeAlphaPslCourant = null;
		this.libellePslCourant = null;
		this.codeAlphaLastPsl = null;
		this.xEbSchemaPsl = null;
		this.transportStatusPerUnit = null;
		this.expectedDateLastPsl = null;
		this.percentPslCourant = null;
	}

	clone(data) {
		this.ebTtTracingNum = data.ebTtTracingNum;
		this.xEbMarchandise = data.xEbMarchandise;
		this.listEbPslApp = new Array<EbPslApp>();
		if (data.listEbPslApp) {
			data.listEbPslApp.forEach((element) => {
				let ebpslApp = new EbPslApp();
				ebpslApp.clone(element);
				this.listEbPslApp.push(ebpslApp);
			});
		}
		this.dateCreation = data.dateCreation;
		this.xEbDemande = data.xEbDemande;
		this.xEcCountryOrigin = data.xEcCountryOrigin;
		this.xEcCountryDestination = data.xEcCountryDestination;
		this.datePickup = data.datePickup;
		this.dateModification = data.dateModification;

		this.totalWeight = data.totalWeight;
		this.refTransport = data.refTransport;
		this.customerReference = data.customerReference;
		this.shippingType = data.shippingType;
		this.partNumber = data.partNumber;
		this.serialNumber = data.serialNumber;
		this.orderNumber = data.orderNumber;
		this.customRef = data.customRef;
		this.xEbTransporteur = data.xEbTransporteur;
		this.xEbEtablissementTransporteur = data.xEbEtablissementTransporteur;
		this.nomEtablissementTransporteur = data.nomEtablissementTransporteur;
		this.xEbCompagnieTransporteur = data.xEbCompagnieTransporteur;

		if (!data.listEvent) this.listEvent = new Array();
		else this.listEvent = Statique.cloneObject(data.listEvent);

		this.xEbChargeur = data.xEbChargeur;
		this.xEbEtablissementChargeur = data.xEbEtablissementChargeur;
		this.nomEtablissementChargeur = data.nomEtablissementChargeur;
		this.xEbChargeurCompagnie = data.xEbChargeurCompagnie;
		this.late = data.late;

		this.configPsl = data.configPsl;
		this.leadtime = data.leadtime;

		this.originPlace = data.originPlace;
		this.destinationPlace = data.destinationPlace;
		this.listFlag = data.listFlag;
		this.listEbFlagDTO = data.listEbFlagDTO;

		this.dateLastPsl = data.dateLastPsl;
		this.libelleLastPsl = data.libelleLastPsl;
		this.listPsl = Statique.cloneObject(data.listPsl, new Array<EbTtCompanyPsl>());
		this.codeAlphaPslCourant = data.codeAlphaPslCourant;
		this.libellePslCourant = data.libellePslCourant;
		this.codeAlphaLastPsl = data.codeAlphaLastPsl;
		this.xEbSchemaPsl = data.xEbSchemaPsl;
		this.expectedDateLastPsl = data.expectedDateLastPsl;
		this.percentPslCourant = data.percentPslCourant;
	}
}
