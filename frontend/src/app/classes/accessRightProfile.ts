import { EcModule } from "./EcModule";
import { EbUserProfile } from "@app/classes/userProfile";

export class ExUserProfileModule {
	module: EcModule;
	ebUserProfile: EbUserProfile;
	accessRight: number;
	ecModuleNum: number;

	constructor(xEcModule?: number) {
		this.ebUserProfile = null;
		this.accessRight = null;
		this.ecModuleNum = xEcModule || null;
		if (xEcModule) {
			this.module = new EcModule();
			this.ecModuleNum = xEcModule;
		} else this.module = null;
	}

	constructorCopy(exUserProfileModule: ExUserProfileModule) {
		this.module = exUserProfileModule.module;
		this.ebUserProfile = exUserProfileModule.ebUserProfile;
		this.accessRight = exUserProfileModule.accessRight;
	}
}
