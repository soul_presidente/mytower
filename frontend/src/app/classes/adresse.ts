import { EcCountry } from "./country";
import { Horaire } from "./horaire";
import { EbEtablissement } from "./etablissement";
import { EbCompagnie } from "./compagnie";
import { EbUser } from "./user";
import { EbPlZoneDTO } from "./trpl/EbPlZoneDTO";
import { Eligibility } from "./eligibility";
import { Statique } from "@app/utils/statique";
import { EbParty } from "./party";
import { EbEntrepotDTO } from './dock/EbEntrepotDTO';

export class EbAdresse {
	ebAdresseNum: number;
	reference: String;
	city: String;
	phone: String;
	airport: String;
	country: EcCountry;
	zipCode: String;
	email: String;
	street: String;
	company: String;
	type: number;
	dateAjout: Date;
	state: String;
	openingHours: Horaire;
	openingHoursFreeText: String;
	xEbUserVisibility: EbUser;
	etablissementAdresse: Array<EbEtablissement>;
	ebCompagnie: EbCompagnie;
	createdBy: EbUser;
	commentaire: String;
	zones: Array<EbPlZoneDTO>;
	defaultZone: EbPlZoneDTO;
	listEligibilitys: Array<Eligibility>;
	listEgibility: String;
	listEgibilityLibelle: String;
	isUpdateUserVisibility: boolean;
	listEtablissementsNum: string;
	listZonesNum: string;
	entrepot: EbEntrepotDTO;

	constructor() {
		this.ebAdresseNum = null;
		this.reference = null;
		this.city = null;
		this.phone = null;
		this.airport = null;
		this.country = null;
		this.zipCode = null;
		this.email = null;
		this.street = null;
		this.company = null;
		this.type = null;
		this.dateAjout = null;
		this.state = null;
		this.openingHours = new Horaire();
		this.openingHoursFreeText = null;
		this.xEbUserVisibility = null;
		this.etablissementAdresse = null;
		this.ebCompagnie = null;
		this.createdBy = null;
		this.commentaire = null;
		this.zones = null;
		this.defaultZone = new EbPlZoneDTO();
		this.listEligibilitys = new Array<Eligibility>();
		this.listEgibility = null;
		this.listEgibilityLibelle = null;
		this.isUpdateUserVisibility = false;
		this.listEtablissementsNum = null;
		this.listZonesNum = null;
		this.entrepot = new EbEntrepotDTO();
	}

	constructorCopy(adresse: EbAdresse) {
		return this.copy(adresse);
	}

	copy(adresse: EbAdresse) {
		this.ebAdresseNum = adresse.ebAdresseNum;
		this.reference = adresse.reference;
		this.city = adresse.city;
		this.phone = adresse.phone;
		this.airport = adresse.airport;
		if (adresse.country) {
			this.country = new EcCountry();
			this.country.constructorCopy(adresse.country);
		} else this.country = null;
		this.zipCode = adresse.zipCode;
		this.email = adresse.email;
		this.street = adresse.street;
		this.company = adresse.company;
		this.type = adresse.type;
		this.dateAjout = adresse.dateAjout;
		this.state = adresse.state;
		this.openingHoursCopy(adresse);
		this.openingHoursFreeText = adresse.openingHoursFreeText;
		this.commentaire = adresse.commentaire;
		this.xEbUserVisibility = EbUser.createUser(adresse.xEbUserVisibility);

		if (adresse.etablissementAdresse && adresse.etablissementAdresse.length > 0) {
			this.etablissementAdresse = new Array<EbEtablissement>();
			let etablissement: EbEtablissement = null;
			adresse.etablissementAdresse.forEach((etb) => {
				etablissement = new EbEtablissement();
				etablissement.ebEtablissementNum = etb.ebEtablissementNum;
				etablissement.nom = etb.nom;
				this.etablissementAdresse.push(etablissement);
			});
		}
		if (adresse.ebCompagnie) {
			this.ebCompagnie = new EbCompagnie();
			this.ebCompagnie.ebCompagnieNum = adresse.ebCompagnie.ebCompagnieNum;
		}
		if (adresse.createdBy) {
			this.createdBy = new EbUser();
			this.createdBy.ebUserNum = adresse.createdBy.ebUserNum;
		}
		if (adresse.zones !== null && adresse.zones.length > 0) {
			this.zones = new Array<EbPlZoneDTO>();
			adresse.zones.forEach((zone) => {
				this.zones.push(Statique.cloneObject<EbPlZoneDTO>(zone, new EbPlZoneDTO()));
			});
		} else this.zones = null;
		this.listEligibilitys = adresse.listEligibilitys;
		this.listEgibility = adresse.listEgibility;
		this.listEgibilityLibelle = adresse.listEgibilityLibelle;
		this.defaultZone = adresse.defaultZone;
		this.listEtablissementsNum = adresse.listEtablissementsNum;
		this.listZonesNum = adresse.listZonesNum;
		if(adresse.entrepot){
			this.entrepot.ebEntrepotNum = adresse.entrepot.ebEntrepotNum
		}

	}

	openingHoursCopy(adresse: EbAdresse) {
		if (adresse.openingHours === undefined || adresse.openingHours === null) {
			this.openingHours = new Horaire();
		} else {
			this.openingHours.constructorCopy(adresse.openingHours);
			this.openingHours.jours.sort((obj1, obj2) => {
				if (obj1.jourEnum.code > obj2.jourEnum.code) {
					return 1;
				}

				if (obj1.jourEnum.code < obj2.jourEnum.code) {
					return -1;
				}

				return 0;
			});
		}
	}

	ebPartyToEbAdress(adresse: EbParty, userConnected: EbUser) {
		let userId = adresse.xEbUserVisibility
			? adresse.xEbUserVisibility.ebUserNum
			: userConnected.ebUserNum;
		this.xEbUserVisibility = EbUser.createUser(null, userId);

		this.etablissementAdresse = new Array<EbEtablissement>();
		let etablissement: EbEtablissement = new EbEtablissement();
		etablissement.ebEtablissementNum = userConnected.ebEtablissement.ebEtablissementNum;
		this.etablissementAdresse.push(etablissement);

		this.ebCompagnie = new EbCompagnie();
		this.ebCompagnie.ebCompagnieNum = userConnected.ebCompagnie.ebCompagnieNum;
		this.createdBy = new EbUser();
		this.createdBy.ebUserNum = userConnected.ebUserNum;
		this.reference = adresse.reference;
		this.company = adresse.company;
		this.street = adresse.adresse;
		this.city = adresse.city;
		this.zipCode = adresse.zipCode;
		this.country = adresse.xEcCountry;
		if (adresse.zone && adresse.zone.ebZoneNum) {
			this.zones = new Array<EbPlZoneDTO>();
			this.zones.push(adresse.zone);
		}
		this.openingHoursFreeText = adresse.openingHours;
		this.airport = adresse.airport;
		this.commentaire = adresse.commentaire;
		this.ebAdresseNum = adresse.ebAdresseNum;
		this.defaultZone = adresse.zone;
	}
}
