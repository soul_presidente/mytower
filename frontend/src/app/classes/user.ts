import { EbCompagnie, EbCompagnieLite } from "@app/classes/compagnie";
import { EbEtablissement, EbEtablissementLite } from "@app/classes/etablissement";
import { Input } from "@app/utils/Input";
import { EbRelation } from "./relation";
import { EbContactRequest } from "./contactRequest";
import { ExUserModule } from "./accessRight";
import { EbCategorie } from "./categorie";
import { ParamsMail } from "./paramsMail";
import { AccessRights } from "@app/utils/enumeration";
import { EbUserProfile } from "@app/classes/userProfile";
import { AuthenticationService } from "@app/services/authentication.service";
import { EbPlCostItemDTO } from "./trpl/EbPlCostItemDTO";
import { EbUserTag } from "./EbUserTag";

export class EbUser {
	ebUserNum: number;
	username: string;
	nom: string;
	prenom: string;
	adresse: string;
	email: string;
	telephone: string;
	password: string;
	confirmPassword: string;
	admin: boolean;
	//xEcRoleUser: role;
	inputs: Array<Input>;
	ebEtablissement: EbEtablissement;
	ebCompagnie: EbCompagnie;
	ebUserProfile: EbUserProfile;
	resetToken: string;
	contacts: Array<EbUser>;
	sendingRequest: Array<EbContactRequest>;
	receivingResquest: Array<EbContactRequest>;
	service: number;
	status: number;
	superAdmin: boolean;
	ebRelation: EbRelation;
	ebUserTag: EbUserTag;
	etat: number;
	role: number;
	serviceTransporteur: boolean;
	serviceBroker: boolean;
	listCategories: Array<EbCategorie>;
	language: string;
	roleLibelle: string;
	roleChargeur: Boolean;
	chargeur: boolean;
	rolePrestataire: Boolean;
	groupeName: string;
	uniqueAdminOfEtablissement: Boolean;
	isCt?: boolean;
	fuseauHoraire: number;

	realUserNum: number;
	realUserNom: string;
	realUserPrenom: string;
	realUserEmail: string;
	realUserPassword: string;
	realUserEtabNum: number;
	realUserEtabNom: string;
	realUserCompNum: number;
	realUserCompNom: string;
	connectedUserAuthToken: string;

	listParamsMail: Array<ParamsMail>;
	isFromTransPlan: boolean;
	calculatedPrice: number;
	transitTime: number;
	refPlan: string;
	refGrille: string;
	coment: string;
	exchangeRateFound: boolean;
	listPlCostItem: Array<EbPlCostItemDTO>;
	selected: boolean = false;
	groupes: String;
	isUnknownUser: boolean = false;
	selectedForQuotation: boolean = false;
	selectedProposedQuotation: boolean = false;
	_listAccessRights: ExUserModule[];
	listLabels: string;
	horsGrille: boolean = false;

	get listAccessRights(): ExUserModule[] {
		if (this._listAccessRights) return this._listAccessRights;

		let rights = null;
		const rightsJSON = localStorage.getItem("access_rights");
		if (rightsJSON) {
			rights = JSON.parse(rightsJSON);
			this._listAccessRights = rights;
		}
		return rights;
	}

	set listAccessRights(rights) {
		if (rights && rights.length) {
			this._listAccessRights = rights;
			localStorage.setItem("access_rights", JSON.stringify(rights));
		}
	}

	constructor() {
		this.ebUserNum = null;
		this.username = null;
		this.nom = null;
		this.prenom = null;
		this.adresse = null;
		this.email = null;
		this.telephone = null;
		this.admin = null;
		this.password = null;
		this.confirmPassword = null;
		this.inputs = new Array<Input>();
		this.ebEtablissement = new EbEtablissement();
		this.ebUserProfile = new EbUserProfile();
		//this.ebCompagnie = new EbCompagnie();
		//this.xEcRoleUser = new role();
		this.resetToken = null;
		this.contacts = new Array<EbUser>();
		this.sendingRequest = new Array<EbContactRequest>();
		this.receivingResquest = new Array<EbContactRequest>();
		this.service = null;
		this.status = null;
		this.superAdmin = null;
		this.ebRelation = new EbRelation();
		this.ebUserTag = new EbUserTag();
		this.etat = null;
		this.role = null;
		this.serviceTransporteur = false;
		this.serviceBroker = false;
		this.language = null;
		this.roleLibelle = null;
		this.roleChargeur = false;
		this.rolePrestataire = false;
		this.groupeName = null;
		this.uniqueAdminOfEtablissement = false;

		this.realUserNum = null;
		this.realUserNom = null;
		this.realUserPrenom = null;
		this.realUserEmail = null;
		this.realUserPassword = null;
		this.realUserEtabNum = null;
		this.realUserEtabNom = null;
		this.realUserCompNum = null;
		this.realUserCompNom = null;
		this.connectedUserAuthToken = null;

		this.listParamsMail = new Array<ParamsMail>();
		this.isFromTransPlan = false;
		this.calculatedPrice = null;
		this.listPlCostItem = new Array<EbPlCostItemDTO>();
		this.transitTime = null;
		this.refPlan = null;
		this.refGrille = null;
		this.coment = null;
		this.exchangeRateFound = null;
		this.groupes = null;
		this.selectedForQuotation = false;
		this.selectedProposedQuotation = false;
		this.fuseauHoraire = null;
		this.listLabels = null;
	}

	constructorCopy(user: EbUser) {
		this.ebUserNum = user.ebUserNum;
		this.username = user.username;
		this.language = user.language;
		this.nom = user.nom;
		this.prenom = user.prenom;
		this.adresse = user.adresse;
		this.email = user.email;
		this.telephone = user.telephone;
		this.admin = user.admin;
		this.password = user.password;
		this.confirmPassword = user.confirmPassword;
		this.inputs = user.inputs;
		this.ebEtablissement = user.ebEtablissement;
		this.ebEtablissement = new EbEtablissement();
		this.ebEtablissement.constructorCopy(user.ebEtablissement);

		this.ebCompagnie = user.ebCompagnie;
		//this.xEcRoleUser = user.xEcRoleUser;
		this.resetToken = user.resetToken;
		this.listAccessRightsCopy(user.listAccessRights);
		this.status = user.status;
		this.service = user.service;
		this.admin = user.admin;
		this.superAdmin = user.superAdmin;
		this.role = user.role;
		this.serviceTransporteur = user.serviceTransporteur;
		this.serviceBroker = user.serviceBroker;
		this.roleLibelle = user.roleLibelle;
		this.roleChargeur = user.roleChargeur;
		this.chargeur = user.chargeur;

		this.rolePrestataire = user.rolePrestataire;
		if (this.rolePrestataire == undefined) {
			this.rolePrestataire = (user as any).prestataire;
		}
		this.groupeName = user.groupeName;

		this.realUserNum = user.realUserNum;
		this.realUserNom = user.realUserNom;
		this.realUserPrenom = user.realUserPrenom;
		this.realUserEmail = user.realUserEmail;
		this.realUserPassword = user.realUserPassword;
		this.realUserEtabNum = user.realUserEtabNum;
		this.realUserEtabNom = user.realUserEtabNom;
		this.realUserCompNum = user.realUserCompNum;
		this.realUserCompNom = user.realUserCompNom;
		this.connectedUserAuthToken = user.connectedUserAuthToken;

		this.isFromTransPlan = user.isFromTransPlan;
		this.calculatedPrice = user.calculatedPrice;
		this.listPlCostItem = user.listPlCostItem;
		this.transitTime = user.transitTime;
		this.refPlan = user.refPlan;
		this.refGrille = user.refGrille;
		this.coment = user.coment;
		this.exchangeRateFound = user.exchangeRateFound;
		this.fuseauHoraire = user.fuseauHoraire;
		this.listLabels = user.listLabels;
		return this;
	}

	constructorCopyLite(user: EbUser) {
		this.ebUserNum = user.ebUserNum;
		this.username = user.username;
		this.language = user.language;
		this.nom = user.nom;
		this.email = user.email;
		this.role = user.role;
		this.service = user.service;
		this.status = user.status;
		this.admin = user.admin;
		this.superAdmin = user.superAdmin;
		if (user.ebEtablissement != null) {
			this.ebEtablissement = new EbEtablissement();
			this.ebEtablissement.ebEtablissementNum = user.ebEtablissement.ebEtablissementNum;
		}

		if (user.ebCompagnie != null) {
			this.ebCompagnie = new EbCompagnie();
			this.ebCompagnie.ebCompagnieNum = user.ebCompagnie.ebCompagnieNum;
		}
	}

	listAccessRightsCopy(listAccessRights: Array<ExUserModule>) {
		let _accessRight;
		if (listAccessRights && listAccessRights.length > 0) {
			for (let right of listAccessRights) {
				_accessRight = new ExUserModule();
				_accessRight.constructorCopy(right);
				if (this.listAccessRights) this.listAccessRights.push(_accessRight);
			}
		} else {
			this.listAccessRights = new Array<ExUserModule>();
		}
	}

	get canShowPrice(): boolean {
		return this.ebEtablissement.canShowPrice;
	}

	hasContribution(ecModuleNum: number): boolean {
		if (!this.listAccessRights) return false;

		if (this.listAccessRights && this.listAccessRights.length == 0) return false;
		return (
			this.listAccessRights.find((access) => {
				return (
					access.ecModuleNum === ecModuleNum && access.accessRight === AccessRights.CONTRIBUTION
				);
			}) != undefined
		);
	}

	hasAccess(ecModuleNum: number): boolean {
		if (!this.listAccessRights) return false;

		if (this.listAccessRights && this.listAccessRights.length == 0) return false;
		let md = this.listAccessRights.find((access) => {
			return (
				access.ecModuleNum === ecModuleNum &&
				(access.accessRight == AccessRights.CONTRIBUTION ||
					access.accessRight == AccessRights.VISUALISATION)
			);
		});

		return md != undefined;
	}

	public static createUser(pUser?, ebUserNum?, email?, nom?, prenom?) {
		if (pUser == null && ebUserNum == null) return null;

		let user = new EbUser();
		if (pUser) user.constructorCopy(pUser);
		else {
			user.ebUserNum = ebUserNum;
			user.email = email;
			user.nom = nom;
			user.prenom = prenom;
		}
		return user;
	}

	get nomPrenom(): string {
		let str = "";

		if (this.nom && this.nom.length) str = this.nom;
		if (this.prenom && this.prenom.length) {
			if (str.length > 0) str += " ";
			str += this.prenom;
		}

		return str;
	}
	set nomPrenom(nomPrenom) {}
}

export class EbUserLite {
	ebUserNum: number;
	ebCompagnie: EbCompagnieLite;
	ebEtablissement: EbEtablissementLite;

	constructor(ebUser: EbUser = null) {
		if (ebUser && ebUser.ebUserNum) this.ebUserNum = ebUser.ebUserNum;

		if (ebUser && ebUser.ebEtablissement && ebUser.ebEtablissement.ebEtablissementNum) {
			this.ebEtablissement = new EbEtablissementLite();
			this.ebEtablissement.ebEtablissementNum = ebUser.ebEtablissement.ebEtablissementNum;
		}
		if (ebUser && ebUser.ebCompagnie && ebUser.ebCompagnie.ebCompagnieNum) {
			this.ebCompagnie = new EbCompagnieLite(ebUser.ebCompagnie);
		}
	}
}
