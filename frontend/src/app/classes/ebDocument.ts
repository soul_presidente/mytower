import { EbUser } from "./user";

export class EbDocument {
	ebDocumentNum: number;
	ebUserNum: number;

	ebEntityNum: number;
	chemin: String;
	originFileName: String;
	typeDoc: number;

	fileName: String;
	commentaire: String;
	dateModif: Date;
	dateFinValidite: Date;
	auteur: EbUser;

	constructor() {
		this.ebDocumentNum = null;
		this.ebUserNum = null;
		this.ebEntityNum = null;
		this.chemin = null;
		this.fileName = null;
		this.originFileName = null;
		this.typeDoc = null;

		this.commentaire = null;
		this.dateModif = null;
		this.dateFinValidite = null;
	}
}
