import { MTEnum } from "./mtEnum";
import { EbPslApp } from "./pslApp";
import { EbTracing } from "./tracing";
import { EbTtCategorieDeviation } from "./categorieDeviation";

export class TTEvent {
	ebTtEventNum: number;
	dateCreation: Date;
	dateEvent: Date;
	typeEvent: number; // type d'evenement
	categorie: MTEnum;
	commentaire: String;
	xEbTtPslApp: EbPslApp;
	xEbTtTracing: EbTracing;
	quantity: number;
	validateByChamp: Boolean;
	nature?: string;
	ebQmIncidentNum: number;
	ebQmDeviationNum: number;
	typeDate: string;

	constructor() {
		this.ebTtEventNum = null;
		this.dateCreation = null;
		this.dateEvent = null;
		this.typeEvent = null; // type d'evenement
		this.categorie = null;
		this.commentaire = null;
		this.xEbTtPslApp = new EbPslApp();
		this.xEbTtTracing = new EbTracing();
		this.quantity = null;
		this.validateByChamp = null;
		this.nature = null;
		this.ebQmIncidentNum = null;
		this.ebQmDeviationNum = null;
		this.typeDate = null;
	}

	clone(data) {
		this.ebTtEventNum = data.ebTtEventNum;
		this.dateCreation = data.dateCreation;
		this.dateEvent = data.dateEvent;
		this.typeEvent = data.typeEvent; // type d'evenement
		this.categorie = data.categorie;
		this.commentaire = data.commentaire;
		this.xEbTtPslApp = new EbPslApp();
		this.xEbTtPslApp.clone(data.xEbTtPslApp);
		this.xEbTtTracing = new EbTracing();
		this.xEbTtTracing.clone(data.xEbTtTracing);
		this.quantity = data.quantity;
		this.validateByChamp = data.validateByChamp;
		this.typeDate = data.typeDate;
	}
}
