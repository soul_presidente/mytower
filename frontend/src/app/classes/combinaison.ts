import { EcCountry } from "./country";
import { EbDestinationCountry } from "./destinationCountry";

export class EbCombinaison {
	ebCombinaisonNum: number;
	hsCode: string;
	origin: EcCountry;
	taxes: number;
	duties: string;
	otherTaxes: String;
	requiredDocument: String;
	importLicense: Boolean;
	destinationCountry: EbDestinationCountry;
	dateAjout: Date;

	constructor(data?: Object) {
		if (data) {
			this.ebCombinaisonNum = data["ebCombinaisonNum"];
			this.hsCode = data["hsCode"];
			this.origin = data["origin"];
			this.taxes = data["taxes"];
			this.duties = data["duties"];
			this.otherTaxes = data["otherTaxes"];
			this.requiredDocument = data["requiredDocument"];
			this.importLicense = data["importLicense"];
			this.destinationCountry = data["destinationCountry"];
			this.dateAjout = data["dateAjout"];
		}
	}
}
