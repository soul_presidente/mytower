export class EbVehicule {
	ebVehiculeNum: number;
	//poids
	weight: number;

	libelle: string;

	//largeur
	width: number;

	//longueur
	lengh: number;

	//hauteur
	height: number;
	dateCreation: Date;
	maj: Date;
	xEbEtablissement: number;
	xModeTransport: number;
	xEbCompagnie: number;

	tare: number;

	xParticularite: number;

	xType: number;

	constructor() {
		this.ebVehiculeNum = null;
		this.width = null;
		this.lengh = null;
		this.height = null;
		this.weight = null;
		this.maj = null;
		this.dateCreation = null;
		this.libelle = null;
		this.xEbEtablissement = null;
		this.xModeTransport = null;
		this.xEbCompagnie = null;
		this.tare = null;
		this.xParticularite = null;
		this.xType = null;
	}
}
