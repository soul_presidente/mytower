import { SearchCriteriaPricingBooking } from '@app/utils/SearchCriteriaPricingBooking';
import { EbWeekTemplateDTO } from './week-template/EbWeekTemplateDTO';
import { EbCompagnie } from './compagnie';
import { EbCategorie } from './categorie';


export class PlanTransportNew{
    reference: string;
    ebPlanTransportNum:number;
    label:string;
    input: SearchCriteriaPricingBooking;
    listCategories: Array<EbCategorie>;
    xEbCompagnieChargeur: EbCompagnie ;
    xEbCompagnieCarrier: EbCompagnie ;
    weekTemplate: EbWeekTemplateDTO;
}