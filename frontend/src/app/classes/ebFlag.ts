export class EbFlag {
	ebFlagNum: number;
	name: string;
	icon: number;
	color: string;
	reference: string;

	constructor() {
		this.ebFlagNum = null;
		this.name = null;
		this.icon = null;
		this.color = null;
		this.reference = null;
	}

	copy(other: EbFlag) {
		this.ebFlagNum = other.ebFlagNum;
		this.color = other.color;
		this.icon = other.icon;
		this.name = other.name;
		this.reference = other.reference;

		return this;
	}

	static constructorCopy(other: EbFlag): EbFlag {
		return new EbFlag().copy(other);
	}

	static copyList(otherList: Array<EbFlag>): Array<EbFlag> {
		const newArray = new Array<EbFlag>();

		otherList &&
			otherList.forEach((other) => {
				newArray.push(EbFlag.constructorCopy(other));
			});

		return newArray;
	}

	static convertListToCommaSeparated(list: Array<EbFlag>): string {
		let listFlagCode = "";
		list.forEach((f) => {
			if (listFlagCode) listFlagCode += ",";
			listFlagCode += f.ebFlagNum;
		});
		return listFlagCode;
	}
}
