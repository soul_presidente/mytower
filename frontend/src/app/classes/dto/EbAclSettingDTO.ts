export class EbAclSettingDTO {
	ecAclRuleNum: number;
	type: number;
	themes: Array<string>;
	code: string;
	values: Array<string>;
	defaultValue: string;
	selectedValue: string;
	experimental: boolean;
}
