import { EbTypeDocumentsDTO } from "./ebTypeDocumentsDTO";

export class EbTemplateDocumentsDTO {
	ebTemplateDocumentsNum: number;

	libelle: string;

	reference: string;

	dateAjout: Date;

	dateMaj: Date;

	listEbTypeDocumentsNum: Array<number>;

	typesDocument: Array<EbTypeDocumentsDTO>;

	originFileName: String;

	cheminDocument: String;

	xEbUserNum: number;

	xEbCompagnieNum: number;

	module: Array<number>;

	isGlobal: boolean;

	constructor() {
		this.listEbTypeDocumentsNum = new Array<number>();
		this.typesDocument = new Array<EbTypeDocumentsDTO>();
	}
}
