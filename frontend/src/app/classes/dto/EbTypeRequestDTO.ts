export class EbTypeRequestDTO {
	ebTypeRequestNum: number;

	libelle: string;

	reference: string;

	dateAjout: Date;

	dateMaj: Date;

	delaiChronoPricing: number;

	sensChronoPricing: number;

	blocageResponsePricing: Boolean;

	activated: boolean;

	xEbUserNum: number;

	xEbCompagnieNum: number;
	xEbCompagnieNom: string;

	refLibelle: string;

	constructor() {}
}
