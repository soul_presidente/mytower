import {Input} from "@app/utils/Input";
import {EbEtablissement} from "@app/classes/etablissement";
import {EbCompagnie} from "@app/classes/compagnie";

export class EbUserDTO {
	username: string;
	nom: string;
	prenom: string;
	adresse: string;
	email: string;
	telephone: string;
	password: string;
	confirmPassword: string;
	inputs: Array<Input>;
	ebEtablissement: EbEtablissement;
	service: number;
	role: number;
	serviceTransporteur: boolean;
	serviceBroker: boolean;

	constructor(){
		this.username = null;
		this.nom = null;
		this.prenom = null;
		this.adresse = null;
		this.email = null;
		this.telephone = null;
		this.password = null;
		this.confirmPassword = null;
		this.inputs = new Array<Input>();
		this.ebEtablissement = new EbEtablissement();
		this.role = null;
		this.service =null;
		this.serviceTransporteur = null;
		this.serviceBroker = null

	}
}
