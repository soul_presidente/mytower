export class EbTypeDocumentsDTO {
	ebTypeDocumentsNum: number;

	nom: string;

	code: string;

	ordre: number;

	ebTypeDocumentsDateCreation: Date;

	ebTypeDocumentsDateModification: Date;

	module: Array<number>;

	listModuleStr: string;

	ebTypeDocumentsEbUserNum: number;

	xEbCompagnie: number;

	nbrDoc: number;

	constructor() {}
}
