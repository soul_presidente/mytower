export class CustomDeclarationDocumentDTO {
	ebDemandeFichierJointNum: number;
	selectedForDeclaration: boolean;
	typeDocument: string;
	filename: string;
}
