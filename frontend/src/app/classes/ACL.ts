export class ACL {
	public static RuleValues = {
		Settings_MyCommunity_MyTowerNetwork: {
			NotVisible: "NOT_VISIBLE",
			Visible: "VISIBLE",
			Write: "WRITE",
		},
		Module_RSCH: {
			NotVisible: "NOT_VISIBLE",
			Visible: "VISIBLE",
			Write: "WRITE",
		},
	};
}

export namespace ACL {
	export enum RuleType {
		BOOLEAN = 0,
		LIST = 1,
	}

	export enum Rule {
		Settings_MyCommunity_MyTowerNetwork = "settings.mycommunity.mytowernetwork",
		Demande_Visualisation_Trpl_Enable = "demande.visualisation.trpl.enable",
		Demande_Step_Documents_Enable = "demande.step.documents.enable",
		Module_RSCH = "module.rsch",
		Demande_Dashboard_Scheduling_Enable = "demande.dashboard.scheduling.enable",
		Pricing_Carrier_Show_Allow = "pricing.carrier.show.allow",
		Demande_Additional_Cost_Enable = "demande.additional.cost.enable",
		Settings_Users_Change_Etablissemet_Enable = "setting.users.changeEtablissement",
		Settings_Company_Automatic_Actions_Enable = "setting.company.automatic.actions.enable",
		Settings_Company_Consolidations_Enable = "setting.company.consolidations.enable",
		Settings_Company_Loading_Schedule_Enable = "setting.company.loading.schedule.enable",
		Pricing_Carrier_Confirm_Choice = "pricing.carrier.confirm.choice",
		Demande_Creation_Draft_Enable = "demande.creation.draft.enable",
		Home_Notifiction_Enable = "home.notification",
		Demo_Features_Enable = "demo.features.enable",
		Delivery_consolidated_Enable = "delivery.consolidated",
		Delivery_Pricing_Enable = "delivery.pricing",
		InitialTrs_selection_and_checkbox_Enable = "demande.initial_trs_checkbox_select"
	}
}
