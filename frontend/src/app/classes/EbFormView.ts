import { ebFormZone } from "./ebFormZone";

export class ebFormView {
	ebFormViewNum: number;
	ebFormView: string;
	xEbFormZone: ebFormZone;

	constructor() {
		this.ebFormViewNum = null;
		this.ebFormView = null;
		this.xEbFormZone = new ebFormZone();
	}
}
