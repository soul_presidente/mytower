import { EbUser } from "./user";
import { EbEtablissement } from "./etablissement";
import { Statique } from "@app/utils/statique";

export class EbRelation {
	ebRelationNum: number;
	host: EbUser;
	guest: EbUser;
	typeRelation: number;
	etat: number;
	email: string;
	xEbEtablissementHost: EbEtablissement;
	xEbEtablissement: EbEtablissement;
	flagEtablissement: boolean;
	lastModifiedBy: EbUser;

	constructor() {
		this.ebRelationNum = null;
		this.host = null;
		this.guest = null;
		this.typeRelation = null;
		this.etat = null;
		this.email = null;
		this.xEbEtablissementHost = null;
		this.xEbEtablissement = null;
		this.lastModifiedBy = null;
		this.flagEtablissement = null;
	}

	constructorCopy(ebRelation: EbRelation) {
		this.ebRelationNum = ebRelation.ebRelationNum;
		this.typeRelation = ebRelation.typeRelation;
		this.etat = ebRelation.etat;
		this.email = ebRelation.email;
		this.flagEtablissement = ebRelation.flagEtablissement;

		if (ebRelation.host != null && ebRelation.host.ebUserNum != null) {
			this.host = new EbUser();
			this.host.constructorCopyLite(ebRelation.host);
		}

		if (ebRelation.guest != null && ebRelation.guest.ebUserNum != null) {
			this.guest = new EbUser();
			this.guest.constructorCopyLite(ebRelation.guest);
		}

		if (ebRelation.xEbEtablissement != null) {
			this.xEbEtablissement = new EbEtablissement();
			this.xEbEtablissement.constructorCopyLite(ebRelation.xEbEtablissement);
		}

		if (ebRelation.xEbEtablissementHost != null) {
			this.xEbEtablissementHost = new EbEtablissement();
			this.xEbEtablissementHost.constructorCopyLite(ebRelation.xEbEtablissementHost);
		}
	}

	copyFrom(obj, includeObjects = false) {
		if (!obj || typeof obj !== "object") return;

		Object.keys(obj).forEach((it) => {
			if (!Statique.isNotDefined(obj[it])) {
				if (typeof obj[it] !== "object" || includeObjects) this[it] = obj[it];
			}
		});
	}
}
