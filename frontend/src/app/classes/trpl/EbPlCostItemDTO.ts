import { EbPlLigneCostItemTrancheDTO } from "./EbPlLigneCostItemTrancheDTO";
import { EbTypeUnit } from "../typeUnit";
import { NgItemLabelDirective } from "@ng-select/ng-select/lib/ng-templates.directive";
import { EcCurrency } from "../currency";

export class EbPlCostItemDTO {
	costItemNum: number;
	fixedFees: number;
	calculationMode: number;
	trancheValues: Array<EbPlLigneCostItemTrancheDTO>;
	type: number;
	startingPsl: number;
	endPsl: number;
	comment: String;
	minFrees: number;
	maxFrees: number;
	typeUnit: EbTypeUnit;
	price: number;
	currency: EcCurrency;
	code : string;

	priceEuro?: number;
	priceReal?: number;
	priceEuroReal?: number;
	priceGab?: number;
	totalPriceConverted?: number;
	libelle?: string;
	xEcCurrencyNum?: number;
	euroExchangeRate?: number;
	codeCurrency?: String;
	xEcCurrencyNumReal?: number;
	priceGabConverted?: number;
	euroExchangeRateReal?: number;
	priceConverted?: number;
	priceRealConverted?: number;
	currencyInvoice?: string;
	ebCompagnieNum?: string;
	libelleCostItem?: string;

	constructor() {
		this.trancheValues = new Array<EbPlLigneCostItemTrancheDTO>();
		this.typeUnit = new EbTypeUnit();
		this.currency = new EcCurrency();
		this.euroExchangeRate = null;
	}
}
