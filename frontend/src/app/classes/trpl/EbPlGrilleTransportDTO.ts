import { EcCurrency } from "../currency";
import { EbPlCostItemDTO } from "./EbPlCostItemDTO";
import { EbUser } from "../user";

export class EbPlGrilleTransportDTO {
	ebGrilleTransportNum: number;
	designation: string;
	reference: string;
	currency: EcCurrency;
	costItems: Array<EbPlCostItemDTO>;

	transporteurNum: number;
	transporteurNom: string;
	transporteurPrenom: string;

	// UI management only
	isEditing: boolean = false;
	transporteur: EbUser;
	dateDebut: Date | string;
	dateFin: Date | string;
	dateDebutAsNumber: number;
	dateFinAsNumber: number;
	origineGrlNum: number;

	constructor() {
		this.costItems = new Array<EbPlCostItemDTO>();
	}
}
