export class EbPlZoneDTO {
	ebZoneNum: number;
	ref: string;
	designation: string;
	companyName: string;

	constructor() {
		this.ebZoneNum = null;
		this.ref = null;
		this.designation = null;
	}
}
