export class EbPlTrancheDTO {
	ebTrancheNum: number;
	minimum: number;
	maximum: number;

	constructor() {
		this.ebTrancheNum = null;
		this.minimum = null;
		this.maximum = null;
	}
}
