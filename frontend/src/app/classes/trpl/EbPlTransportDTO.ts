import { EbPlZoneDTO } from "./EbPlZoneDTO";
import { ModeTransport, TypeDemande } from "@app/utils/enumeration";
import { EbUser } from "../user";
import { EbPlGrilleTransportDTO } from "./EbPlGrilleTransportDTO";

export class EbPlTransportDTO {
	ebPlanTransportNum: number;
	reference: string;
	designation: string;
	origine: EbPlZoneDTO;
	destination: EbPlZoneDTO;
	typeDemande: TypeDemande;
	typeTransport: number;
	modeTransport: ModeTransport;
	isDangerous: Boolean;
	transporteur: EbUser;
	grilleTarif: EbPlGrilleTransportDTO;
	transitTime: number;
	comment: string;
	typeGoodsNum: number;

	constructor() {}
}
