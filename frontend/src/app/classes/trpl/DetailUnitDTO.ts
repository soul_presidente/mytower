export class DetailUnitDTO {
	weight: number;
	volume: number;
	nombre: number;
	day: number;
	ebTypeUnitNum: number;
	currencyCibleNum: number;
	calculatedPrice: number;
}
