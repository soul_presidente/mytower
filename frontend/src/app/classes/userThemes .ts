import { EbCompagnie } from "./compagnie";
import { EbUser } from "./user";

export class EbUserThemes {
	ebUserThemesNum: number;
	xEbCompagnie: EbCompagnie;
	xEbUser: EbUser;
	activeTheme: string;
	dateMaj: Date;

	constructor() {
		this.ebUserThemesNum = null;
		this.xEbCompagnie = null;
		this.xEbUser = null;
		this.activeTheme = null;
		this.dateMaj = null;
	}
}
