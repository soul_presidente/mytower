import { EbEtablissement } from "./etablissement";

export class EcQmRootCause {
	qmRootCauseNum: number;
	label: String;
	name: String;
	ref: String;
	groupe: String;
	relatedIncident: String;
	listIncidentNum: String;
	eventType: number;
	perimetreImpacte: String;
	userImpacter: String;
	severite: number;
	dateCreationSys: Date;
	relatedActions: String;
	createur: String;
	montantCumuleCIL: String;
	statut: number;
	etablissement: EbEtablissement;
	responsable: number;
	rootCauseCategory: number;
	actions: string;

	description : string ;


	constructor() {
		this.qmRootCauseNum = null;
		this.etablissement = null;
		this.label = null;
		this.statut = null;
		this.name = null;
		this.ref = null;
		this.groupe = null;
		this.relatedIncident = null;
		this.perimetreImpacte = null;
		this.userImpacter = null;
		this.severite = null;
		this.dateCreationSys = null;
		this.relatedActions = null;
		this.createur = null;
		this.montantCumuleCIL = null;
		this.responsable = null;
		this.rootCauseCategory = null;
		this.actions = null;
		this.description = null;
	}
}
