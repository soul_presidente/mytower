export class EbTypeConteneur {
	key: number;
	label: string;
	nbr: number;

	constructor() {
		this.key = null;
		this.label = null;
		this.nbr = null;
	}
}
