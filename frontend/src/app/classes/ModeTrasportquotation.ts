import { CostCategorie } from "./costCategorie";

export class ModeTrasnportQuotation {
	xModeTranspo: number;
	ListCat?: Array<CostCategorie>;
	constructor() {
		this.xModeTranspo = null;
		this.ListCat = new Array<CostCategorie>();
	}
}
