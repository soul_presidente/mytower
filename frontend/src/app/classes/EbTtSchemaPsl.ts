import { EbCompagnie } from "@app/classes/compagnie";
import { EbTtCompanyPsl } from "./ttCompanyPsl";

export class EbTtSchemaPsl {
	ebTtSchemaPslNum: number;
	xEcModeTransport: number;
	designation: string;
	listEtablissement: string;
	listModeTransport: string;
	isEditable: boolean;
	listPsl: Array<EbTtCompanyPsl>;
	xEcIncotermLibelle: string;
	xEbCompagnie: EbCompagnie;

	constructor() {
		this.ebTtSchemaPslNum = null;
		this.xEcModeTransport = null;
		this.designation = null;
		this.listEtablissement = null;
		this.listModeTransport = null;
		this.isEditable = null;
		this.listPsl = null;
		this.xEcIncotermLibelle = null;
		this.xEbCompagnie = null;
	}

	constructorLite(ebTtSchemaPsl: EbTtSchemaPsl) {
		this.ebTtSchemaPslNum = ebTtSchemaPsl.ebTtSchemaPslNum;
	}
}
