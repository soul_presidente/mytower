export class SocketSessionData{
	pricingDetailsLocked: boolean;
	module: number;
	recordId: number;
	sessionId: string;
	free: boolean;
}
