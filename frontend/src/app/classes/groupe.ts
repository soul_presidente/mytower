export class ebGroupe {
	ebGroupeNum: number;
	nom: string;

	constructor() {
		this.init();
	}

	private init() {
		this.ebGroupeNum = null;
		this.nom = null;
	}

	constructorCopy(ebGoupe: ebGroupe) {
		this.init();
		this.ebGroupeNum = ebGoupe.ebGroupeNum;
		this.nom = ebGoupe.nom;
	}
}
