import { OrderLine } from "./ebDelOrderLine";
import { jsonIgnore } from "json-ignore";

export class EbOrderLinePlanificationDTO {
	ebDelOrderLineNum: number;
	quantityToPlan: number;
	ebDelLivraisonLineNum: number;

	// Front only
	@jsonIgnore() orderLine: OrderLine;
	itemNum: string;
	itemName: string;

	constructor() {
		this.ebDelOrderLineNum = null;
		this.quantityToPlan = null;
		this.ebDelLivraisonLineNum = null;
		this.itemNum = null;
		this.itemName = null;
	}
}
