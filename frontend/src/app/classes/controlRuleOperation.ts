export class EbControlRuleOperation {
	ebControlRuleOperationNum: number;

	leftOperands: string;
	rightOperands: string;

	operator: number;
	ruleOperator: number;
	isOperation: boolean;

	leftOperandObj?: any;
	rightOperandObj?: any;

	operationType: number;

	leftOperandValues: any;
	listOfOperators: any;
	rightOperandValues: any;
	leftOperandType: number;

	ebCategorieNum: number;
	ebLeftCustomFieldNum: number;
	ebRightCustomFieldNum: number;
	leftUnitFieldCode: number;
	selectedTypeDocNum: number;

	listCategories: any;
	listCustomFields: any;
	listCategoriesOrCF: any;
	listUnits: any;
	listDocTypes: any;

	constructor() {
		this.leftOperandObj = {};
		this.rightOperandObj = {};
	}
}
