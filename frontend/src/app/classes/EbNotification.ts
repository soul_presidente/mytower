import { EbUser } from "./user";
import { EbCompagnie } from "./compagnie";

export class EbNotification {
	ebNotificationNum: Number;
	header: string;
	body: string;
	type: Number;
	user: EbUser;
	compagnie: EbCompagnie;
	dateCreationSys: Date;
	module: number;
	numEntity: number;

	constructor() {
		this.ebNotificationNum = null;
		this.header = null;
		this.body = null;
		this.type = null;
		this.user = null;
		this.compagnie = null;
		this.dateCreationSys = null;
		this.module = null;
		this.numEntity = null;
	}
}
