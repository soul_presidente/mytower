export class ParamsMail {
	ebParamEmailNum: number;
	emailName: String;
	createdByMe: boolean;
	createdByOtherUser: boolean;
	emailId: number;
	xEcModule: number;
	listEmailEnCopie: Array<string>;
	listEmailEnCopieStr: string;
	roles: string;
	services: string;
	hidden: boolean;

	constructor() {
		this.ebParamEmailNum = null;
		this.emailName = null;
		this.createdByMe = false;
		this.createdByOtherUser = false;
		this.emailId = null;
		this.xEcModule = null;
		this.roles = null;
		this.services = null;
		this.listEmailEnCopieStr = "";
		this.listEmailEnCopie = new Array<string>();
		this.hidden = false;
	}
}
