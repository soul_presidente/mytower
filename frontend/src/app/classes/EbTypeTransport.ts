export class EbTypeTransport {
	ebTypeTransportNum: number;
	xEcModeTransport: number;
	libelle: string;
	creationDate: Date;
	lastUpdateDate: Date;

	constructor() {
		this.ebTypeTransportNum = null;
		this.xEcModeTransport = null;
		this.libelle = null;
		this.creationDate = null;
		this.lastUpdateDate = null;
	}
}
