import { EbUser } from "./user";
import { EbEtablissement } from "./etablissement";
import { EbCompagnie } from "./compagnie";

export class EbUserTag {
	ebUserTagNum: number;
	host: EbUser;
	guest: EbUser;
	tags: string;
	comment: string;
	xEbEtablissementHost: EbEtablissement;
	xEbEtablissementGuest: EbEtablissement;
	xEbCompanieHost: EbCompagnie;
	xEbCompagnieGuest: EbCompagnie;
	activated: boolean;

	constructor() {
		this.ebUserTagNum = null;
		this.host = null;
		this.guest = null;
		this.tags = null;
		this.comment = null;
		this.xEbEtablissementHost = null;
		this.xEbEtablissementGuest = null;
		this.xEbCompanieHost = null;
		this.xEbCompagnieGuest = null;
		this.activated = false;
	}

	constructorGuest(ebUser: EbUser) {
		this.activated = true;
		this.comment = ebUser.ebUserTag.comment;
		this.tags = ebUser.ebUserTag.tags;
		this.guest = new EbUser();
		this.guest.constructorCopyLite(ebUser);
		this.xEbEtablissementGuest = new EbEtablissement();
		this.xEbEtablissementGuest.constructorCopyLite(ebUser.ebEtablissement);
		this.xEbCompagnieGuest = new EbCompagnie();
		this.xEbCompagnieGuest.constructorCopy(ebUser.ebCompagnie);
	}

	constructorHost(ebUser: EbUser) {
		this.host = new EbUser();
		this.host.ebUserNum = ebUser.ebUserNum;
		this.xEbEtablissementHost = new EbEtablissement();
		this.xEbEtablissementHost.ebEtablissementNum = ebUser.ebEtablissement.ebEtablissementNum;
		this.xEbCompanieHost = new EbCompagnie();
		this.xEbCompanieHost.ebCompagnieNum = ebUser.ebCompagnie.ebCompagnieNum;
	}
}
