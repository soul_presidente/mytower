import { Jour } from "./jour";
import { Statique } from "@app/utils/statique";

export class Horaire {
	etatHoraireNum: number;
	jours: Array<Jour>;
	horaireAllDays: Jour;

	constructor() {
		this.init();
	}

	private init() {
		this.etatHoraireNum = null;
		this.jours = new Array<Jour>();
		this.horaireAllDays = new Jour();
		this.horaireAllDays.jourEnum.code = 0;
		this.horaireAllDays.jourEnum.libelle = "All";
		this.horaireAllDays.jourEnum.name = Statique.daysEnum[0];
		Statique.listJours.forEach((value: string, key: number) => {
			let jour = new Jour();
			jour.jourEnum.code = key;
			jour.jourEnum.libelle = value;
			jour.jourEnum.name = Statique.daysEnum[key];
			this.jours.push(jour);
		});
	}

	constructorCopy(horaire: Horaire) {
		this.init();
		let _jour;
		this.etatHoraireNum = horaire.etatHoraireNum;
		this.jours = new Array<Jour>();
		if (horaire.horaireAllDays != null) {
			this.horaireAllDays.constructorCopy(horaire.horaireAllDays);
		}
		for (let jour of horaire.jours) {
			_jour = new Jour();
			_jour.constructorCopy(jour);
			this.jours.push(_jour);
		}

		this.jours.sort((obj1, obj2) => {
			if (obj1.jourEnum.code > obj2.jourEnum.code) {
				return 1;
			}

			if (obj1.jourEnum.code < obj2.jourEnum.code) {
				return -1;
			}

			return 0;
		});
	}

	checkHoraire(form: any) {
		for (let i = 0; i <= 6; i++) {
			this.jours[i].checkHoraire(form, i);
		}
	}
}
