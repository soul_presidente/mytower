export class DataRecoveryParam {
	dataType: Array<number>;
	compagnies: Array<number>;

	constructor() {
		this.dataType = null;
		this.compagnies = null;
	}
}
