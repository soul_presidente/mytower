import { EbLabel } from "./label";
import { EbEtablissement } from "./etablissement";
import { EbCompagnie } from "./compagnie";

export class EbCategorie {
	ebCategorieNum: number;
	libelle: string;
	etablissement: EbEtablissement;
	compagnie: EbCompagnie;
	labels: Array<EbLabel>;
	selectedLabel: EbLabel;
	isAll: boolean;
	deleted: boolean;

	constructor() {
		this.init();
	}

	private init() {
		this.ebCategorieNum = null;
		this.libelle = null;
		this.etablissement = null;
		this.compagnie = null;
		this.labels = new Array<EbLabel>();
		this.selectedLabel = null;
		this.isAll = null;
		this.deleted = null;
	}

	constructorCopy(ebCategorie: EbCategorie) {
		this.init();
		this.ebCategorieNum = ebCategorie.ebCategorieNum;
		this.libelle = ebCategorie.libelle;
		this.isAll = ebCategorie.isAll;
		this.deleted = ebCategorie.deleted;
		if (ebCategorie.etablissement != null) {
			ebCategorie.etablissement.categories = null;
			this.etablissement = ebCategorie.etablissement;
		}
		this.labelCopy(ebCategorie.labels);
	}

	constructorCopyLite(ebCategorie: EbCategorie) {
		this.init();
		this.ebCategorieNum = ebCategorie.ebCategorieNum;
		this.libelle = ebCategorie.libelle;
		this.isAll = ebCategorie.isAll;
		this.deleted = ebCategorie.deleted;
	}

	labelCopy(labels: Array<EbLabel>) {
		let _label;
		if (labels !== undefined && labels !== null && labels.length !== 0) {
			for (let label of labels) {
				_label = new EbLabel();
				_label.constructorCopy(label);
				this.labels.push(_label);
			}
		} else {
			this.labels = new Array<EbLabel>();
		}
	}

	static getCopyOfListCategorie(src: Array<EbCategorie>): Array<EbCategorie> {
		let listCategories = JSON.parse(JSON.stringify(src));
		return (function(data: Array<EbCategorie>) {
			return data;
		})(listCategories);
	}
}
