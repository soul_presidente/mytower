export interface PrMtcMappingParam {
	prMtcMappingParamNum: number;
	configCode: string;
	mtcAttribute: string;
	mtgAttribute: string;
	type: ParamType;
}

export interface ParamType {
	code: number;
	libelle: string;
}
