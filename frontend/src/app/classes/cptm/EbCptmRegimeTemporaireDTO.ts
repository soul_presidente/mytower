export default class EbCptmRegimeTemporaireDTO {
	ebCptmRegimeTemporaireNum: number;
	type: string;
	description: string;
	deadlineDays: number;
	alert1Days: number;
	alert2Days: number;
	alert3Days: number;
	leg1Name: string;
	leg1Code: string;
	leg2Name: string;
	leg2Code: string;

	constructor() {}
}
