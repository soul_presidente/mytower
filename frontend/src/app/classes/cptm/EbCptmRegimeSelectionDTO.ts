export class EbCptmRegimeSelectionDTO {
	label: string;
	labelForSearch: string; // contains label + additional value (ex procedure operation number or ref unit)
	ebCptmProcedureNum: number;
	ebCptmRegimeTemporaireNum: number;
	procedureOperationNumber: string;
	leg1RefUnit: string;
}
