import { EbDemande } from "../demande";

export class EbCptmProcedureDTO {
	ebCptmProcedureNum: number;

	xRegimeTemporaireLeg1Name: string;
	xRegimeTemporaireLeg2Name: string;
	xRegimeTemporaireLeg1Code: string;
	xRegimeTemporaireLeg2Code: string;
	xRegimeTemporaireAlert1Days: number;
	xRegimeTemporaireAlert2Days: number;
	xRegimeTemporaireAlert3Days: number;

	// Leg1
	xLeg1UnitRef: string;
	xLeg1DemandeRef: string;
	xLeg1DemandeStatus: number;
	leg1ExpectedCustomerDeclarationDate: Date;
	leg1ActualCustomerDeclarationDate: Date;
	leg1Location: string;
	leg1TransitNumber: string;
	leg1TransitStatus: boolean;
	leg1UnitIndex: number;
	leg1DeclarationNumber: string;
	leg1TracingStatus: string;
	xLeg1DemandeNum: number;

	// Leg2
	xLeg2UnitRef: string;
	xLeg2DemandeRef: string;
	xLeg2DemandeStatus: number;
	leg2ExpectedCustomerDeclarationDate: Date;
	leg2ActualCustomerDeclarationDate: Date;
	leg2Location: string;
	leg2TransitNumber: string;
	leg2TransitStatus: boolean;
	leg2UnitIndex: number;
	leg2DeclarationNumber: string;
	leg2TracingStatus: string;
	xLeg2DemandeNum: number;

	comment: string;
	deadline: Date;
	status: number;
	nbAlerts: number;
	operationNumber: string;

	//Calculated fields
	scheduledAlert: Date;
	daysSinceImport: number;
	daysBeforeDeadline: number;
	alertNumber: number;
}
