import { EbCompagnie } from "./compagnie";

export class EbIncoterm {
	ebIncotermNum: number;
	code: string;
	libelle: string;
	xEbCompagnie: EbCompagnie;

	constructor() {
		this.ebIncotermNum = null;
		this.code = null;
		this.libelle = null;
		this.xEbCompagnie = null;
	}
}
