export class EbRangeHourDTO {
	rangeHoursNum: number;
	startHour: Date;
	endHour: Date;
	constructor() {
		this.rangeHoursNum = null;
		this.startHour = null;
	}
}
