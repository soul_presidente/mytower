import { EbUser } from "./user";
import { TransfertAction } from "./transfertAction";
import { EbEtablissement } from "./etablissement";
import { EbCategorie } from "./categorie";
import { EbLabel } from "./label";

export class TransfertUserObject {
	ebUserNum: number;
	oldEtablissement: number;
	oldListCategories: Array<EbCategorie>;
	oldListLabels: String;

	newEtablissement: number;
	newListCategories: Array<EbCategorie>;
	newListLabels: String;

	orders: TransfertAction;
	transportFiles: TransfertAction;
	community: TransfertAction;

	constructor() {
		this.ebUserNum = null;
		this.oldEtablissement = null;
		this.oldListCategories = new Array<EbCategorie>();
		this.oldListLabels = null;

		this.newEtablissement = null;
		this.newListCategories = new Array<EbCategorie>();
		this.newListLabels = null;
		this.orders = new TransfertAction();
		this.transportFiles = new TransfertAction();
		this.community = new TransfertAction();
	}
}
