export enum StatusDelivery {
	PENDING = 1,
	CONFIRMED = 2,
	SHIPPED = 3,
	DELIVERED = 4,
	CANCELED = 5
}
