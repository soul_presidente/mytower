export class EbQmDeviationSettiongs {
	ebQmDeviationSettingsNum: number;
	identifiant: string;
	libelle: string;
	type: string;
	sousType: string;
	xModule: number;
	objet: string;
	champs: string;
	detail: string;
	description: string;
	active: Boolean;
	createDeviation: Boolean;
	sendAlert: Boolean;
	changeStatus: Boolean;
	xEbCompagnie: number;

	constructor() {
		this.ebQmDeviationSettingsNum = null;
		this.identifiant = null;
		this.libelle = null;
		this.type = null;
		this.sousType = null;
		this.xModule = null;
		this.objet = null;
		this.champs = null;
		this.detail = null;
		this.description = null;
		this.active = null;
		this.createDeviation = null;
		this.sendAlert = null;
		this.changeStatus = null;
		this.xEbCompagnie = null;
	}
}
