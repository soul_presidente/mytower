import { EbUser } from "@app/classes/user";

export class EbFavori {
	ebFavoriNum: number;
	idObject: number;
	refObject: string;
	module: number;
	user: EbUser;
	dateCreation: Date;
	toDelete: boolean;
	toAdd: boolean;

	constructor() {
		this.ebFavoriNum = null;
		this.idObject = null;
		this.refObject = null;
		this.dateCreation = new Date();
		this.module = null;
		this.user = new EbUser();
		this.toDelete = false;
		this.toAdd = false;
	}
}
