import { EbCategorie } from "@app/classes/categorie";
import { ExUserProfileModule } from "@app/classes/accessRightProfile";
import { EbEtablissement } from "./etablissement";
import { EbCompagnie } from "./compagnie";

export class EbUserProfile {
	ebUserProfileNum: number;
	nom: string;
	descriptif: string;
	listAccessRights: Array<ExUserProfileModule>;
	listCategories: Array<EbCategorie>;
	ebCompagnie: EbCompagnie;
	constructor() {
		this.ebUserProfileNum = null;
		this.nom = null;
		this.descriptif = null;
		this.listAccessRights = new Array<ExUserProfileModule>();
		this.listCategories = new Array<EbCategorie>();
		this.ebCompagnie = null;
	}
}
