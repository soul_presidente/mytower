import { EbDemande } from "./demande";
import { EbUser } from "./user";

export class FichierJoint {
	ebDemandeFichierJointNum;
	xEbUser: EbUser;
	xEbDemande: EbDemande;
	idCategorie: String;
	xEbEtablissemnt: Number;
	chemin: String;
	fileName: String;
	originFileName: String;
	attachmentDate: Date;
	xEbCompagnie: Number;
	pslEvent: string;

	constructor(nic?: string, siren?: string) {
		this.ebDemandeFichierJointNum = null;
		this.pslEvent = null;
		this.xEbUser = null;
		this.xEbDemande = new EbDemande();
		this.idCategorie = null;
		this.xEbEtablissemnt = null;
		this.chemin = null;
		this.fileName = null;
		this.originFileName = null;
		this.attachmentDate = null;
		this.xEbCompagnie = null;
	}
}
