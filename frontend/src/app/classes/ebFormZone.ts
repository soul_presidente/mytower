import { ebFormView } from "./EbFormView";

export class ebFormZone {
	ebFormZoneNum: number;
	ebFormName: string;
	listFormView: Array<ebFormView>;

	constructor() {
		this.ebFormZoneNum = null;
		this.ebFormName = null;
		this.listFormView = new Array<ebFormView>();
	}
}
