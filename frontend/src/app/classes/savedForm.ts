import { EbUser } from "./user";
import { EbCompagnie } from "./compagnie";

export class SavedForm {
	ebSavedFormNum: number;
	ebModuleNum: number;
	ebCompNum: number;
	ebUser: EbUser;
	ebUserNum: number;
	formName: string;
	formValues: string;
	dateAjout: Date;
	url: string;
	action: number;
	actionTrigger: String;
	actionTargets: string;
	actionParameters: string;
	ordre: number;
	compagnie: EbCompagnie;
	listCodeActionTrigger: Array<number>;
}
