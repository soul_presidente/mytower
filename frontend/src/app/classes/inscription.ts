import { EbUser } from "./user";
import {EbUserDTO} from "@app/classes/dto/eb-user-dto";

export class Inscription {
	ebUser: EbUserDTO;
	friendInvitationEmails: Array<string>;
	collegueInvitationEmails: Array<string>;

	constructor() {
		this.friendInvitationEmails = null;
		this.collegueInvitationEmails = null;
	}
}
