export class EcFuseauxHoraire {
	ecFuseauHoraireNum: number;
	decalage: number;
	code: String;
	libelle: String;

	constructor() {
		this.ecFuseauHoraireNum = null;
		this.decalage = null;
		this.code = null;
		this.libelle = null;
	}

	constructorCopy(fuseauxHoraire: EcFuseauxHoraire) {
		this.ecFuseauHoraireNum = fuseauxHoraire.ecFuseauHoraireNum;
		this.decalage = fuseauxHoraire.decalage;
		this.code = fuseauxHoraire.code;
		this.libelle = fuseauxHoraire.libelle;
	}

	public static create(ecFuseauHoraireNum, decalage, code?, libelle?): EcFuseauxHoraire {
		let f = new EcFuseauxHoraire();
		f.ecFuseauHoraireNum = ecFuseauHoraireNum;
		f.decalage = decalage;
		f.code = code;
		f.libelle = libelle;
		return f;
	}
}
