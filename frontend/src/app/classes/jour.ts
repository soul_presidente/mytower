import { JourEnum } from "./jourEnum";

export class Jour {
	jourEnum: JourEnum;
	heure1: String;
	heure2: String;
	heure3: String;
	heure4: String;
	ouvert: Boolean;
	ish3h4actif: Boolean;
	isConge: Boolean;
	date: Date;
	isJourPropose: Boolean;
	showRange1: Boolean;
	showRange2: Boolean;

	constructor() {
		this.init();
	}

	private init() {
		this.jourEnum = new JourEnum();
		this.heure1 = null;
		this.heure2 = null;
		this.heure3 = null;
		this.heure4 = null;
		this.ouvert = null;
		this.ish3h4actif = null;
		this.isConge = null;
		this.date = null;
		this.isJourPropose = null;
		this.showRange1 = true;
		this.showRange2 = true;
	}

	constructorCopy(jour: Jour) {
		this.init();
		this.jourEnum.constructorCopy(jour.jourEnum);
		this.heure1 = jour.heure1;
		this.heure2 = jour.heure2;
		this.heure3 = jour.heure3;
		this.heure4 = jour.heure4;
		this.ouvert = jour.ouvert;
		this.ish3h4actif = jour.ish3h4actif;
	}

	clearTiming() {
		this.heure1 = null;
		this.heure2 = null;
		this.heure3 = null;
		this.heure4 = null;
	}

	disable() {
		this.heure1 = null;
		this.heure2 = null;
		this.heure3 = null;
		this.heure4 = null;
	}

	static disable(jour: Jour) {
		jour.heure1 = null;
		jour.heure2 = null;
		jour.heure3 = null;
		jour.heure4 = null;
	}

	enableH3_H4() {
		this.ish3h4actif = true;
	}

	static enableH3H4(jour: Jour) {
		jour.ish3h4actif = true;
	}

	disableH3_H4() {
		this.ish3h4actif = false;
		this.heure3 = null;
		this.heure4 = null;
	}

	static disableH3H4(jour: Jour) {
		jour.ish3h4actif = false;
		jour.heure3 = null;
		jour.heure4 = null;
	}

	checkHoraire(form: any, i: any) {
		Jour.checkHoraire(this, form, i);
	}

	static checkHoraire(jour: Jour, form: any, i: any) {
		let h1 = null;
		let m1 = null;
		let error1 = true;
		let h2 = null;
		let m2 = null;
		let error2 = true;
		let h3 = null;
		let m3 = null;
		let error3 = true;
		let h4 = null;
		let m4 = null;
		let error4 = true;

		if (jour.heure1 !== null) {
			h1 = parseInt(jour.heure1.split(":")[0]);
			m1 = parseInt(jour.heure1.split(":")[1]);
			if (!isNaN(h1) && !isNaN(m1)) error1 = false;
		}

		if (jour.heure2 !== null) {
			h2 = parseInt(jour.heure2.split(":")[0]);
			m2 = parseInt(jour.heure2.split(":")[1]);
			if (!isNaN(h2) && !isNaN(m2)) error2 = false;
		}

		if (jour.heure3 !== null) {
			h3 = parseInt(jour.heure3.split(":")[0]);
			m3 = parseInt(jour.heure3.split(":")[1]);
			if (!isNaN(h3) && !isNaN(m3)) error3 = false;
		}

		if (jour.heure4 !== null) {
			h4 = parseInt(jour.heure4.split(":")[0]);
			m4 = parseInt(jour.heure4.split(":")[1]);
			if (!isNaN(h4) && !isNaN(m4)) error4 = false;
		}

		if (!error1 && !error2 && !error3 && !error4) {
			if (h1 > h2 || (h1 === h2 && m1 >= m2))
				form.controls["heure2".concat(i)].setErrors("timeNotValid", true);
			else form.controls["heure2".concat(i)].setValue(form.controls["heure2".concat(i)].value);
			if (h2 > h3 || (h2 === h3 && m2 >= m3))
				form.controls["heure3".concat(i)].setErrors("timeNotValid", true);
			else form.controls["heure3".concat(i)].setValue(form.controls["heure3".concat(i)].value);
			if (h3 > h4 || (h3 === h4 && m3 >= m4))
				form.controls["heure4".concat(i)].setErrors("timeNotValid", true);
		} else if (!error1 && !error2 && !error3) {
			if (h1 > h2 || (h1 === h2 && m1 >= m2))
				form.controls["heure2".concat(i)].setErrors("timeNotValid", true);
			if (h2 > h3 || (h2 === h3 && m2 >= m3))
				form.controls["heure3".concat(i)].setErrors("timeNotValid", true);
		} else if (!error1 && !error2) {
			if (h1 > h2 || (h1 === h2 && m1 >= m2))
				form.controls["heure2".concat(i)].setErrors("timeNotValid", true);
		}
	}

	dontShowHoraire() {
		this.showRange1 = false;
		this.showRange2 = false;
	}

	checkHoraireLivraison() {
		if (this.date != null) {
			let dateTime1: Date;
			let dateTime2: Date;
			let dateTime3: Date;
			let dateTime4: Date;

			if (this.heure1 != null && this.heure2 != null) {
				dateTime1 = new Date(
					this.date.getFullYear(),
					this.date.getMonth(),
					this.date.getDate(),
					parseInt(this.heure1.split(":")[0]),
					parseInt(this.heure1.split(":")[1]),
					0,
					0
				);
				dateTime2 = new Date(
					this.date.getFullYear(),
					this.date.getMonth(),
					this.date.getDate(),
					parseInt(this.heure2.split(":")[0]),
					parseInt(this.heure2.split(":")[1]),
					0,
					0
				);

				if (!(dateTime1 <= this.date && this.date <= dateTime2)) {
					this.showRange1 = false;
				}
			}
			if (this.heure3 != null && this.heure4 != null && !this.showRange1) {
				dateTime3 = new Date(
					this.date.getFullYear(),
					this.date.getMonth(),
					this.date.getDate(),
					parseInt(this.heure3.split(":")[0]),
					parseInt(this.heure3.split(":")[1]),
					0,
					0
				);
				dateTime4 = new Date(
					this.date.getFullYear(),
					this.date.getMonth(),
					this.date.getDate(),
					parseInt(this.heure4.split(":")[0]),
					parseInt(this.heure4.split(":")[1]),
					0,
					0
				);

				if (!(this.date <= dateTime4)) {
					this.showRange2 = false;
				}
			}
		}
	}
}
