import { EbInvoice } from "./invoice";

export class InvoiceWrapper {
	listEbInvoiceNumber: Array<number>;
	selectValue: string;
	ebInvoice: EbInvoice;
	lisAction: Array<number>;

	constructor() {
		this.listEbInvoiceNumber = new Array<number>();
		this.selectValue = null;
		this.ebInvoice = new EbInvoice();
		this.lisAction = new Array<number>();
	}
}
