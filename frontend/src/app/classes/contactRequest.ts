import { EbUser } from "./user";
import { EbEtablissement } from "./etablissement";

export class EbContactRequest {
	ebContactRequestNum: number;
	expediteur: EbUser;
	destinataire: EbUser;
	dateCreation: Date;
	status: number;
	ebEtablissementDestinataire: EbEtablissement;
	typeRelation: number;

	constructor() {
		this.init();
	}

	private init() {
		this.ebContactRequestNum = null;
		this.expediteur = null;
		this.destinataire = null;
		this.status = null;
		this.ebEtablissementDestinataire = null;
		this.typeRelation = null;
	}

	constructorCopy(ebContactRequest: EbContactRequest) {
		this.init();
		this.ebContactRequestNum = ebContactRequest.ebContactRequestNum;
		this.destinataire = ebContactRequest.destinataire;
		this.expediteur = ebContactRequest.expediteur;
		this.ebEtablissementDestinataire = ebContactRequest.ebEtablissementDestinataire;
		this.dateCreation = ebContactRequest.dateCreation;
		this.typeRelation = ebContactRequest.typeRelation;
	}
}
