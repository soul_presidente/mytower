import { EbEtablissement } from "./etablissement";
import { EcCurrency } from "./currency";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { Statique } from "@app/utils/statique";
import { EbCompagnie } from "./compagnie";

export class EbCompagnieCurrency {
	ebCompagnieCurrencyNum: number;
	euroExchangeRate: number;
	dateDebut: Date;
	dateFin: Date;
	dateCreation: number;
	dateModification: string;
	ebEtablissement: EbEtablissement;
	ecCurrency: EcCurrency;
	_dateDebut: NgbDateStruct;
	_dateFin: NgbDateStruct;
	isUsed: boolean;
	ebCompagnie: EbCompagnie;
	ebCompagnieGuest: EbCompagnie;
	xecCurrencyCible: EcCurrency;
	compagnieCurrencyCode: string;
	isCreatedFromIHM: boolean;

	constructor() {
		this.ebCompagnieCurrencyNum = null;
		this.euroExchangeRate = null;
		this.dateDebut = null;
		this.dateFin = null;
		this.dateCreation = null;
		this.dateModification = null;
		this.ebEtablissement = new EbEtablissement();
		this.ecCurrency = new EcCurrency();
		this._dateDebut = null;
		this._dateFin = null;
		this.isUsed = false;
		this.ebCompagnie = new EbCompagnie();
		this.ebCompagnieGuest = new EbCompagnie();
		this.xecCurrencyCible = new EcCurrency();
		this.compagnieCurrencyCode = null;
		this.isCreatedFromIHM = true;
	}

	constructorCopy(ebCompagnieCurrency: EbCompagnieCurrency) {
		this.ebCompagnieCurrencyNum = ebCompagnieCurrency.ebCompagnieCurrencyNum;
		this.euroExchangeRate = ebCompagnieCurrency.euroExchangeRate;
		this.dateDebut = ebCompagnieCurrency.dateDebut;
		this.dateFin = ebCompagnieCurrency.dateFin;
		this.dateCreation = ebCompagnieCurrency.dateCreation;
		this.dateModification = ebCompagnieCurrency.dateModification;
		this.ebEtablissement = ebCompagnieCurrency.ebEtablissement;
		this.ecCurrency = ebCompagnieCurrency.ecCurrency;
		this.isUsed = ebCompagnieCurrency.isUsed;
		this.ebCompagnie = ebCompagnieCurrency.ebCompagnie;
		this.ebCompagnieGuest = ebCompagnieCurrency.ebCompagnieGuest;
		this.xecCurrencyCible = ebCompagnieCurrency.xecCurrencyCible;
		this.compagnieCurrencyCode = ebCompagnieCurrency.compagnieCurrencyCode;
		this.isCreatedFromIHM = ebCompagnieCurrency.isCreatedFromIHM;
	}

	formatToDate() {
		if (this._dateDebut != null) this.dateDebut = Statique.fromDatePicker(this._dateDebut);
		if (this._dateFin != null) this.dateFin = Statique.fromDatePicker(this._dateFin);
	}

	formatToDatePicker() {
		if (this.dateDebut != null) this._dateDebut = Statique.formatDateToStructure(this.dateDebut);
		if (this.dateFin != null) this._dateFin = Statique.formatDateToStructure(this.dateFin);
	}

	checkInterval() {
		this.formatToDate();
		if (
			this.dateDebut != null &&
			this.dateFin != null &&
			this.dateDebut.getTime() <= this.dateFin.getTime()
		)
			return true;
		else return false;
	}
}
