import { EbQmIncident } from "./ebIncident";

export class EmailHistorique {
	module: number;
	dateEnvoi: Date;
	typeEmail: string;

	tto: string;
	subject: string;
	text: string;
	enCopie: string;
	enCopieCachee: string;

	ebIncident: EbQmIncident;
	ebIncidentNum: number;

	objectNum: number;

	constructor() {
		this.module = null;
		this.dateEnvoi = null;
		this.typeEmail = null;
		this.tto = null;
		this.subject = null;
		this.text = null;
		this.enCopie = null;
		this.enCopieCachee = null;
		this.ebIncident = null;
		this.ebIncidentNum = null;
		this.objectNum = null;
	}
}
