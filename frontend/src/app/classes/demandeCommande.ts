import { ExDemandeCommandeId } from "./demandeCommandeId";

export class ExEbDemandeCommande {
	private dateCreation: Date;
	exDemandeCommandeId: ExDemandeCommandeId;

	constructor() {
		this.dateCreation = null;
		this.exDemandeCommandeId = new ExDemandeCommandeId();
	}
}
