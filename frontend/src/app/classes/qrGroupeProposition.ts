import { EbDemande } from "./demande";

export class EbQrGroupeProposition {
	ebQrGroupePropositionNum: number;

	statut: number;

	listDemandeNum: string; // :a::b::c::d::e: ...

	listSelectedDemandeNum: string; // :a::b::c::d::e: ...

	listEbDemande: Array<EbDemande>;

	ebDemandeResult: EbDemande;

	nombreDemandesPerPage: number;
	nombreDemandes: number;
	nombrePages: number;
	currentPage: number;

	constructor() {}
}
