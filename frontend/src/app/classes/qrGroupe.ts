import { EbCompagnie } from "./compagnie";
import { EbQrGroupeProposition } from "./qrGroupeProposition";
import { QrGroupeCriteria } from "./qrGroupeCriteria";

export class EbQrGroupe {
	ebQrGroupeNum: number;
	libelle: string;
	description: string;
	grpOrder: number;

	xEbCompagnie: EbCompagnie;

	compatibilityCriteria: Array<QrGroupeCriteria>;
	exclusionCriteria: Array<any>;

	triggers: any;
	ventilation: number;
	output: number;

	listPropositions: Array<EbQrGroupeProposition>;

	updatePropositions: boolean;
	isChecked: boolean;
	isActive:boolean;

	constructor() {}
}
