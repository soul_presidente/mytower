import { EbCompagnie } from "./compagnie";

export class EbTypeUnit {
	ebTypeUnitNum: number;

	libelle: string;

	weight: number;

	length: number;

	width: number;

	height: number;

	gerbable: string;

	gerbableValue: boolean;

	containerLabel: string;

	xEbTypeContainer: number;

	code: string;

	activated: boolean;
	xEbCompagnie: EbCompagnie;

	constructor() {
		this.ebTypeUnitNum = null;
		this.libelle = null;
		this.weight = null;
		this.length = null;
		this.width = null;
		this.height = null;
		this.gerbable = null;
		this.gerbableValue = null;
		this.containerLabel = null;
		this.xEbTypeContainer = null;
		this.code = null;
		this.activated = null;
		this.xEbCompagnie = new EbCompagnie();
	}
}
