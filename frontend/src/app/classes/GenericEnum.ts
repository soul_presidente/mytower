export class GenericEnum {
	code: number;
	key: string;

	static findCodeByKey(key: string, list: Array<GenericEnum>): number {
		let rValue: number = null;
		list.forEach((kv) => {
			if (kv.key === key) {
				rValue = kv.code;
			}
		});
		return rValue;
	}

	static findKeyByCode(code: number, list: Array<GenericEnum>): string {
		let rValue: string = null;
		list.forEach((kv) => {
			if (kv.code === code) {
				rValue = kv.key;
			}
		});
		return rValue;
	}
}
