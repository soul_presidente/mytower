export class EbTtPsl {
	code: number;
	libelle: string;
	ordre: number;
	codeAlpha: string;

	constructor() {
		this.code = null;
		this.libelle = null;
		this.ordre = null;
		this.codeAlpha = null;
	}
}
