import { EbTracing } from "./tracing";

import { EbUser } from "./user";
import { EcCountry } from "./country";
import { TTEvent } from "./tTEvent";
import { Statique } from "@app/utils/statique";
import { MTEnum } from "./mtEnum";
import { EbTtCompanyPsl } from "./ttCompanyPsl";

export class EbPslApp {
	ebTtPslAppNum: number;
	dateCreation: Date;
	dateActuelle: Date;
	dateNegotiation: Date;
	dateEstimee: Date;
	xEcCountry: EcCountry;
	city: string;
	xEbUser: EbUser;
	xEbTrackTrace: EbTracing;
	codePslLibelle?: String;
	validated: Boolean;
	commentaire: string;
	listEvent: Array<TTEvent>;
	exists: Boolean;
	active: Boolean;
	quantity: number;
	fuseauHoraire: number;
	companyPsl: EbTtCompanyPsl;
	validateByChamp: Boolean;
	date?: Date;
	isActual?: boolean;
	nom?: string;
	nature?: string;
	nbDoc?: number;

	importance?: number;
	constructor() {
		this.codePslLibelle = null;
		this.ebTtPslAppNum = null;
		this.dateCreation = null;
		this.dateActuelle = null;
		this.dateNegotiation = null;
		this.dateEstimee = null;
		this.xEcCountry = new EcCountry();
		this.city = null;
		this.xEbUser = new EbUser();
		this.xEbTrackTrace = null;
		this.codePslLibelle = null;
		this.validated = null;
		this.commentaire = null;
		this.listEvent = new Array();
		this.exists = false;
		this.active = false;
		this.quantity = null;
		this.fuseauHoraire = null;
		this.companyPsl = null;
		this.validateByChamp = null;
		this.date = null;
		this.isActual = null;
		this.nom = null;
		this.nature = null;
	}

	clone(data: EbPslApp) {
		this.codePslLibelle = data.codePslLibelle;
		this.ebTtPslAppNum = data.ebTtPslAppNum;
		if (data.xEcCountry) this.xEcCountry = data.xEcCountry;
		this.city = data.city;
		if (data.xEbUser) this.xEbUser = data.xEbUser;
		this.xEbTrackTrace = data.xEbTrackTrace;
		this.validated = data.validated;
		this.commentaire = data.commentaire;
		this.exists = data.exists;
		if (data.listEvent) this.listEvent = Statique.cloneObject(data.listEvent);
		this.active = data.active;

		this.dateEstimee = data.dateEstimee ? new Date(data.dateEstimee) : null;
		this.dateCreation = data.dateCreation ? new Date(data.dateCreation) : null;
		this.dateActuelle = data.dateActuelle ? new Date(data.dateActuelle) : null;
		this.dateNegotiation = data.dateNegotiation ? new Date(data.dateNegotiation) : null;
		this.quantity = data.quantity;
		this.fuseauHoraire = data.fuseauHoraire;
		this.companyPsl = Statique.cloneObject(data.companyPsl, new EbTtCompanyPsl());
		this.validateByChamp = data.validateByChamp;
	}
}
