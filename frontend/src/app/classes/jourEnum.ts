export class JourEnum {
	code: number;
	libelle: string;
	name: string;

	constructor() {
		this.code = null;
		this.libelle = null;
		this.name = null;
	}
	constructorCopy(jourEnum: JourEnum) {
		this.code = jourEnum.code;
		this.libelle = jourEnum.libelle;
		this.name = jourEnum.name;
	}
}
