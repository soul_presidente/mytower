import { EbUser } from "./user";

export default class EbCustomField {
	ebCustomFieldNum: number;
	ebUser: EbUser;
	xEbCompagnie: number;
	fields: string;
	nomCompagnie: string;
}

export interface IField {
	name: string;
	num: number;
	value: string;
	label: string;
	isdefault: boolean;
}

export class CustomField implements IField {
	name: string;
	num: number;
	value: string;
	label: string;
	isdefault: boolean;
}
