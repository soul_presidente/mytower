import { ebFormField } from "./ebFormField";
import { EbCompagnie } from "./compagnie";

export class EbFormFieldCompagnie {
	ebFormFieldCompagnieNum: number;
	listFormField: Array<ebFormField>;
	xEbCompagnie: EbCompagnie;

	constructor() {
		this.listFormField = new Array<ebFormField>();
		this.xEbCompagnie = null;
	}
}
