import { QrGroupeObject } from "./qrGroupeObject";

export class QrGroupeValue {
	code: number;
	libelle: string;
	listValues: Array<QrGroupeObject>;

	categorieValue: QrGroupeObject;
	value: QrGroupeObject;
	sousFunction: QrGroupeObject;

	constructor() {
		this.value = new QrGroupeObject();
	}
}
