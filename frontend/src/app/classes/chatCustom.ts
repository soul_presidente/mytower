import { EbUser } from "@app/classes/user";

export class EbChatCustom {
	EbChatCustomNum: number;
	text: string;
	dateCreation: Date;
	idFiche: number;
	module: number;
	idChatComponent: number;
	user: EbUser;
	userName: string;
	userAvatar: string;
	xEbUser: number;
	isHistory: boolean;

	constructor() {
		this.EbChatCustomNum = null;
		this.text = null;
		this.dateCreation = new Date();
		this.module = null;
		this.idChatComponent = null;
		this.idFiche = null;
		this.user = new EbUser();
		this.userName = null;
		this.userAvatar = null;
		this.xEbUser = null;
		this.isHistory = false;
	}
}
