import { EbUser } from "@app/classes/user";

export class Delegation {
	ebDelegationNum: number;
	userBackUp: EbUser;
	userConnect: EbUser;
	dateDebut: Date;
	dateFin: Date;
	sDateDebut: string;
	sDateFin: string;

	constructor() {
		this.ebDelegationNum = null;
		this.userBackUp = new EbUser();
		this.userConnect = new EbUser();
		this.dateDebut = null;
		this.dateFin = null;
		this.sDateDebut = null;
		this.sDateFin = null;
	}
}
