export class ebFormType {
	ebFormTypeNum: number;
	ebFormTypeName: string;
	listDropDownListValues: Array<object>;
	dataLink: string;

	constructor() {
		this.ebFormTypeNum = null;
		this.ebFormTypeName = null;
		this.listDropDownListValues = new Array<object>();
		this.dataLink = null;
	}
}
