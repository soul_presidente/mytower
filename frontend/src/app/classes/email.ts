export class Email {
	to: string;
	subject: string;
	text: string;
	enCopie: Array<string>;
	enCopieCachee: Array<string>;
	mailType: number;
	constructor() {
		this.to = null;
		this.subject = null;
		this.text = null;
		this.enCopie = new Array<string>();
		this.enCopieCachee = new Array<string>();
		this.mailType = null;
	}
}
