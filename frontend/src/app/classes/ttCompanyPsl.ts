import { EbCompagnie } from "./compagnie";

export class EbTtCompanyPsl {
	ebTtCompanyPslNum: number;
	libelle: string;
	dateAttendue: boolean;
	quantiteAttendue: boolean;
	refDocumentAttendue: boolean;
	dateCreationSys: Date;
	dateMajSys: Date;
	codeAlpha: string;

	schemaActif: boolean;
	segmentName: string;
	importance: number;
	startPsl: boolean;
	endPsl: boolean;
	order: number;
	expectedDate: Date;
	isChecked: boolean;
	ebCompagnie: EbCompagnie;
	expectedDateTime: number;


	constructor() {
		this.ebTtCompanyPslNum = null;
		this.libelle = null;
		this.quantiteAttendue = null;
		this.refDocumentAttendue = null;
		this.dateCreationSys = null;
		this.dateAttendue = null;
		this.dateMajSys = null;
		this.codeAlpha = null;

		this.schemaActif = null;
		this.segmentName = null;
		this.importance = null;
		this.startPsl = null;
		this.endPsl = null;
		this.order = null;
		this.expectedDate = null;
		this.isChecked = null;
		this.ebCompagnie = null;
	}
}
