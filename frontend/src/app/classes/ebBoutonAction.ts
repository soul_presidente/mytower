export class EbBoutonAction {
	ebBoutonActionNum: number;
	type: String;
	module: String;
	action: String;
	active: boolean;
	ordre: number;
	color: String;
	icon: String;
	libelle: String;

	constructor() {
		this.ebBoutonActionNum = null;
		this.type = null;
		this.module = null;
		this.active = false;
		this.ordre = null;
		this.color = null;
		this.icon = null;
		this.libelle = null;
		this.action = null;
	}

	copy(other: EbBoutonAction) {
		this.ebBoutonActionNum = other.ebBoutonActionNum;
		this.type = other.type;
		this.module = other.module;
		this.active = other.active;
		this.ordre = other.ordre;
		this.color = other.color;
		this.icon = other.icon;
		this.libelle = other.libelle;
		this.action = other.action;
		return this;
	}

	static constructorCopy(other: EbBoutonAction): EbBoutonAction {
		return new EbBoutonAction().copy(other);
	}

	static copyList(otherList: Array<EbBoutonAction>): Array<EbBoutonAction> {
		const newArray = new Array<EbBoutonAction>();

		otherList &&
			otherList.forEach((other) => {
				newArray.push(EbBoutonAction.constructorCopy(other));
			});

		return newArray;
	}

	static convertListToCommaSeparated(list: Array<EbBoutonAction>): string {
		let listBoutonActionCode = "";
		list.forEach((f) => {
			if (listBoutonActionCode) listBoutonActionCode += ",";
			listBoutonActionCode += f.ebBoutonActionNum;
		});
		return listBoutonActionCode;
	}
}
