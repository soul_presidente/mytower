export class EbTtCategorieDeviation {
	ebTtCategorieDeviationNum: number;
	code: number;
	libelle: string;
	typeEvent: number;

	constructor() {
		this.ebTtCategorieDeviationNum = null;
		this.code = null;
		this.libelle = null;
		this.typeEvent = null;
	}
}
