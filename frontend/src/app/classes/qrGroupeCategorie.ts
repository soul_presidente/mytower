import { QrGroupeObject } from "./qrGroupeObject";

export class QrGroupeCategorie {
	value: QrGroupeObject;
	labels: Array<QrGroupeObject>;

	constructor() {}
}
