export class EcQmCategory {
	ecQmCategorieNum: number;
	label: string;
	code: number;

	constructor() {
		this.ecQmCategorieNum = null;
		this.label = null;
		this.code = null;
	}
}
