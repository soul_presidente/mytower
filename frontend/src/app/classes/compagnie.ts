import { ebGroupe } from "./groupe";
import { EbEtablissement } from "./etablissement";
import { EbCategorie } from "./categorie";
import { Statique } from "@app/utils/statique";

export class EbCompagnie {
	ebCompagnieNum: number;
	nom: string;
	siren: string;
	ebGroupe: ebGroupe;
	listEtablissements: Array<EbEtablissement>;
	categories: Array<EbCategorie>;
	code: string;
	compagnieRole: number;
	groupeName: string;
	logo: string;

	constructor() {
		this.init();
	}

	private init() {
		this.ebCompagnieNum = null;
		this.nom = null;
		this.siren = null;
		this.ebGroupe = new ebGroupe();
		this.listEtablissements = new Array<EbEtablissement>();
		this.code = null;
		this.compagnieRole = null;
		this.groupeName = null;
		this.logo = null;
		this.categories = new Array<EbCategorie>();
	}

	constructorCopy(ebCompagnie: EbCompagnie) {
		this.init();
		this.copy(ebCompagnie);
	}

	copy(ebCompagnie: EbCompagnie): EbCompagnie {
		this.ebCompagnieNum = ebCompagnie.ebCompagnieNum;
		this.nom = ebCompagnie.nom;
		this.siren = ebCompagnie.siren;
		this.ebGroupe = ebCompagnie.ebGroupe;
		this.listEtablissementsCopy(ebCompagnie.listEtablissements);
		this.code = ebCompagnie.code;
		this.compagnieRole = ebCompagnie.compagnieRole;
		this.groupeName = ebCompagnie.groupeName;
		this.categorieCopy(ebCompagnie.categories);
		return this;
	}

	listEtablissementsCopy(listEtablissements: Array<EbEtablissement>) {
		let _etablissement;
		if (
			listEtablissements !== undefined &&
			listEtablissements !== null &&
			listEtablissements.length !== 0
		) {
			for (let etablissement of listEtablissements) {
				_etablissement = new EbEtablissement();
				_etablissement.constructorCopy(etablissement);
				this.listEtablissements.push(_etablissement);
			}
		} else {
			this.listEtablissements = new Array<EbEtablissement>();
		}
	}

	categorieCopy(categories: Array<EbCategorie>) {
		let _categorie;
		if (Statique.isDefined(categories) && categories.length !== 0) {
			for (let categorie of categories) {
				_categorie = new EbCategorie();
				_categorie.constructorCopy(categorie);
				this.categories.push(_categorie);
			}
		} else {
			this.categories = new Array<EbCategorie>();
		}
	}
}

export class EbCompagnieLite {
	ebCompagnieNum: number;

	constructor(ebCompagnie: EbCompagnie = null) {
		if (ebCompagnie && ebCompagnie.ebCompagnieNum) this.ebCompagnieNum = ebCompagnie.ebCompagnieNum;
	}
}
