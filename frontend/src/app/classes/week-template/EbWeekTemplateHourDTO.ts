import { EbWeekTemplateDayDTO } from "./EbWeekTemplateDayDTO";

export class EbWeekTemplateHourDTO {
	ebWeekTemplateHourNum: number;
	startHour: String;
	endHour: String;
	day: EbWeekTemplateDayDTO;
	cutOffTime: String;

	EbWeekTemplateHourDTO() {
		this.EbWeekTemplateHourDTO = null;
		this.startHour = null;
		this.endHour = null;
		this.day = null;
		this.cutOffTime = null;
	}
}
