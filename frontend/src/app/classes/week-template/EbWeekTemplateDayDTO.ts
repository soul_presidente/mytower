import { EbWeekTemplateHourDTO } from "./EbWeekTemplateHourDTO";

export class EbWeekTemplateDayDTO {
	ebWeekTemplateDayNum: number;
	code: number;
	open: boolean;
	hours: Array<EbWeekTemplateHourDTO>;

	EbWeekTemplateDayDTO() {
		this.ebWeekTemplateDayNum = null;
		this.code = null;
		this.open = null;
		this.hours = null;
	}
}
