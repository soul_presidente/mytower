import { EbWeekTemplateDTO } from "./EbWeekTemplateDTO";
import { EbCompagnie } from "../compagnie";

export class EbWeekTemplatePeriodDTO {
	ebWeektemplateNum: number;
	label: string;
	dateDebut: Date;
	dateFin: Date;
	typePeriod: number;
	margin: string;
	week: EbWeekTemplateDTO;
	xEbCompagnie: EbCompagnie;
}
