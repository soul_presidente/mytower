import { EbCompagnie } from "./../compagnie";
import { EbWeekTemplateDayDTO } from "./EbWeekTemplateDayDTO";
import { EbWeekTemplatePeriodDTO } from "./EbWeekTemplatePeriodDTO";

export class EbWeekTemplateDTO {
	ebWeektemplateNum: number;
	label: string;
	dateDebut: Date;
	dateFin: Date;
	type: number;
	margin: string;
	xEbCompagnie: EbCompagnie;
	days: Array<EbWeekTemplateDayDTO>;
	EbWeekTemplateDTO() {
		this.ebWeektemplateNum = null;
		this.label = null;
		this.dateDebut = null;
		this.dateFin = null;
		this.type = null;
		this.xEbCompagnie = null;
		this.days = null;
	}
}
