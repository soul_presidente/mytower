export class ArticleDims {
	width: number;
	height: number;
	length: number;
	weight: number;
	quantity: number;

	constructor() {
		this.width = null;
		this.height = null;
		this.length = null;
		this.weight = null;
		this.quantity = null;
	}

	public static create(width, height, length, weight, quantity) {
		let artd = new ArticleDims();

		artd.width = width;
		artd.height = height;
		artd.length = length;
		artd.weight = weight;
		artd.quantity = quantity;

		return artd;
	}
}
