export class EbTtCompagnyPslLightDto {
	id: number;
	libelle: string;
	codeAlpha: string;
	schemaActif: boolean;
	startPsl: boolean;
	endPsl: boolean;
	order: number;
	expectedDate: Date;
	isChecked: boolean;

	constructor() {
		this.id = null;
		this.libelle = null;
		this.codeAlpha = null;
		this.schemaActif = null;
		this.startPsl = null;
		this.endPsl = null;
		this.order = null;
		this.expectedDate = null;
		this.isChecked = null;
	}
}
