import { ebFormType } from "./ebFormType";
import { ebFormView } from "./EbFormView";

export class ebFormField {
	ebFormFieldNum: number;
	name: string;
	code: string;
	value: boolean;
	xEbFormType: ebFormType;
	motTechnique: string;
	xEbViewForm: ebFormView;
	visible: boolean;
	ordre: number;
	translate: string;
	valueAvailable: string;
	valeurUnit: string;
	srcListItems: string;
	fetchAttribute: string;
	fetchValue: string;
	required: boolean;
	constructor() {
		this.ebFormFieldNum = null;
		this.name = null;
		this.code = null;
		this.value = null;
		this.xEbFormType = null;
		this.motTechnique = null;
		this.xEbViewForm = null;
		this.visible = null;
		this.translate = null;
		this.valueAvailable = null;
		this.ordre = null;
		this.valeurUnit = null;
		this.srcListItems = null;
		this.fetchAttribute = null;
		this.fetchValue = null;
		this.required = null;
	}

	copy(data: ebFormField) {
		this.ebFormFieldNum = data.ebFormFieldNum;
		this.name = data.name;
		this.code = data.code;
		this.value = data.value;
		this.xEbFormType = data.xEbFormType;
		this.motTechnique = data.motTechnique;
		this.xEbViewForm = data.xEbViewForm;
		this.visible = data.visible;
		this.translate = data.translate;
		this.valueAvailable = data.valueAvailable;
		this.ordre = data.ordre;
		this.valeurUnit = data.valeurUnit;
		this.srcListItems = data.srcListItems;
		this.fetchAttribute = data.fetchAttribute;
		this.fetchValue = data.fetchValue;
		this.required = data.required;
	}
}
