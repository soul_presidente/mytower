import { EbParty } from "./party";
import { OrderLine } from "./ebDelOrderLine";
import { EbEtablissement } from "./etablissement";
import { EbCompagnie } from "./compagnie";
import { EbFlag } from "./ebFlag";
import { EbUser } from "./user";
import { IField } from "./customField";
import { EbCategorie } from "./categorie";
import { jsonIgnore } from "json-ignore";
import { EbTtSchemaPsl } from "./EbTtSchemaPsl";
import { Delivery } from "./ebDelLivraison";
import { OrderStatus } from "@app/utils/enumeration";

export class Order {
	//	user: EbUser;
	xEbUserOrigin: EbUser;
	xEbUserDest: EbUser;
	xEbUserOriginCustomsBroker: EbUser;
	xEbUserDestCustomsBroker: EbUser;
	xEbUserObserver: Array<EbUser>;

	xEbSchemaPsl: EbTtSchemaPsl;
	ebDelOrderNum: number;

	numOrderSAP: string;

	numOrderEDI: string;

	numOrderCustomer: string;

	orderType: string;

	campaignCode: string;

	campaignName: string;

	orderStatus: string;

	customerCreationDate: string;

	deliveryDate: string;

	etd: string;

	eta: string;

	meanOfTransport: string;

	numberOfItems: number;

	departureSite: string;

	grossWeight: number;

	netWeight: number;

	deliveryCity: string;

	numberOfEdit: number;

	deliveryBlockage: string;

	invoicingBlockage: string;

	soldToCustomerRef: string;

	shippedToCustomerRef: string;

	xEbEtablissement: EbEtablissement;

	xEbCompagnie: EbCompagnie;

	listCustomsFields: Array<IField>;

	listCategories: Array<EbCategorie>;

	listCategoriesStr: string;

	orderLines: Array<OrderLine>;

	livraisons: Array<Delivery>;

	listLabels: string;

	crossDock: string;

	customFields: string;

	xEbPartyOrigin: EbParty;
	xEbPartyDestination: EbParty;
	xEbPartySale: EbParty;

	listFlag: string;

	listEbFlagDTO: Array<EbFlag>;

	eligibleForConsolidation: boolean;

	// Front only
	@jsonIgnore() nbUnit: number;

	totalQuantityOrdered: number;
	totalQuantityPlannable: number;
	totalQuantityPlanned: number;
	totalQuantityPending: number;
	totalQuantityConfirmed: number;
	totalQuantityShipped: number;
	totalQuantityDelivered: number;
	totalQuantityCanceled: number;

	targetDate: string;

	orderDate: string;

	xEbPartyVendor: EbParty;
	xEbPartyIssuer: EbParty;
	xEbOwnerOfTheRequest: EbUser;
	customerOrderReference: string;
	xEbIncotermNum: number;
	xEcIncotermLibelle: string;
	xEbTypeFluxNum: number;
	complementaryInformations: string;

	reference: string;

	requestOwnerEmail: string;

	constructor() {
		this.xEbPartyOrigin = null;
		this.xEbPartyDestination = null;
		this.xEbPartySale = null;
		this.crossDock = null;

		this.ebDelOrderNum = null;

		this.numOrderSAP = null;

		this.numOrderEDI = null;

		this.numOrderCustomer = null;

		this.orderType = null;

		this.campaignCode = null;

		this.campaignName = null;
		//
		//	this.user = null;

		this.xEbUserOrigin = null;

		this.xEbUserDest = null;

		this.xEbUserObserver = null;

		this.xEbUserOriginCustomsBroker = null;

		this.xEbUserDestCustomsBroker = null;

		this.orderStatus = OrderStatus.PENDING;

		this.customerCreationDate = null;

		this.deliveryDate = null;

		this.deliveryBlockage = null;

		this.invoicingBlockage = null;

		this.soldToCustomerRef = null;

		this.shippedToCustomerRef = null;

		this.xEbEtablissement = null;

		this.xEbCompagnie = null;

		this.listLabels = null;

		this.orderLines = new Array<OrderLine>();

		this.livraisons = new Array<Delivery>();

		this.listCustomsFields = null;

		this.etd = null;

		this.eta = null;

		this.meanOfTransport = null;

		this.numberOfItems = null;

		this.departureSite = null;

		this.grossWeight = null;

		this.netWeight = null;

		this.deliveryCity = null;

		this.numberOfEdit = null;

		this.listFlag = null;

		this.listEbFlagDTO = null;

		this.listCategories = null;

		this.listCategoriesStr = null;

		this.customFields = null;

		this.eligibleForConsolidation = false;

		this.nbUnit = 0;

		this.targetDate = null;
		this.orderDate = null;
		this.xEbOwnerOfTheRequest = null;
		this.customerOrderReference = null;
		this.xEbIncotermNum = null;
		this.xEcIncotermLibelle = null;
		this.xEbTypeFluxNum = null;
		this.complementaryInformations = null;
		this.xEbSchemaPsl = null;
		this.reference = null;
		this.requestOwnerEmail = null;
	}
	cloneData(data) {
		this.ebDelOrderNum = data.ebDelOrderNum;
		this.xEbPartyOrigin = new EbParty();
		if (data.ebPartyOrigin) this.xEbPartyOrigin.constructorCopy(data.xEbPartyOrigin);
		this.xEbPartyDestination = new EbParty();
		if (data.ebPartyDest) this.xEbPartyDestination.constructorCopy(data.ebPartyDest);
		this.xEbPartySale = new EbParty();
		if (data.ebPartyNotif) this.xEbPartySale.constructorCopy(data.ebPartyNotif);
		this.listCategories = data.listCategories || new Array<EbCategorie>();
		this.listCategoriesStr = data.listCategoriesStr || "";
		this.listLabels = data.listLabels || "";
		//	this.user = data.user;
		this.customFields = data.customFields;
		this.etd = data.etd;
		this.eta = data.eta;
		this.xEbEtablissement = data.xEbEtablissement;
		this.listCustomsFields = data.listCustomsFields;
		this.xEbUserOrigin = EbUser.createUser(data.xEbUserOrigin);
		this.xEbUserDest = EbUser.createUser(data.xEbUserDest);
		this.xEbUserOriginCustomsBroker = EbUser.createUser(data.xEbUserOriginCustomsBroker);
		this.xEbUserDestCustomsBroker = EbUser.createUser(data.xEbUserDestCustomsBroker);
		this.xEbUserObserver = data.xEbUserObserver || new Array<EbUser>();
		this.listFlag = data.listFlag;
		this.listEbFlagDTO = data.listEbFlagDTO;
		this.eligibleForConsolidation = data.eligibleForConsolidation;

		this.targetDate = data.targetDate;
		this.orderDate = data.orderDate;
		this.xEbOwnerOfTheRequest = data.xEbOwnerOfTheRequest;
		this.xEbIncotermNum = data.xEbIncotermNum;
		this.xEcIncotermLibelle = data.xEcIncotermLibelle;
		this.xEbTypeFluxNum = data.xEbTypeFluxNum;
		this.complementaryInformations = data.complementaryInformations;
		this.xEbSchemaPsl = data.xEbSchemaPsl;
	}
}
