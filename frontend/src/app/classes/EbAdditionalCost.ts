import { EbCompagnie } from "./compagnie";

export class EbAdditionalCost {
	ebAdditionalCostNum: number;
	code: string;
	libelle: string;
	xEbCompagnie: EbCompagnie;
	actived: boolean;

	constructor() {
		this.ebAdditionalCostNum = null;
		this.code = null;
		this.libelle = null;
		this.xEbCompagnie = null;
		this.actived = null;
	}
}
