export class EcCurrency {
	ecCurrencyNum: number;
	libelle: string;
	code: string;
	symbol: string;
	dateCreation: Date;
	codeLibelle: string;

	constructor() {
		this.ecCurrencyNum = null;
		this.libelle = null;
		this.code = null;
		this.symbol = null;
		this.dateCreation = null;
		this.codeLibelle = null;
	}
}
