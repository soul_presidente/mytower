import { EbOrderLinePlanificationDTO } from "./EbOrderLinePlanificationDTO";

export class EbOrderPlanificationDTO {
	ebDelOrderNum: number;
	ebDelLivraisonNum: number;
	dateOfGoodsAvailability: Date;
	listToPlan: Array<EbOrderLinePlanificationDTO>;

	constructor() {
		this.ebDelOrderNum = null;
		this.ebDelLivraisonNum = null;
		this.dateOfGoodsAvailability = null;
		this.listToPlan = new Array<EbOrderLinePlanificationDTO>();
	}
}
