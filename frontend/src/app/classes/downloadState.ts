export class DownloadState {
	state: number;
	fileUrl: string;

	constructor() {
		this.state = null;
		this.fileUrl = null;
	}
}
