export class EcDeviationQm {
	qmDeviationQmNum: number;
	deviationType: string;
	deviationRef: string;
	transportRef: string;
	UnitRef: string;
	deviationDate: Date;
	deviationDescription: string;
	details: string;
	originCountry: string;
	destinationCountry: string;
	module: string;
	carrier: string;
	shiper: string;
	timeInsurance: string;
	status: string;
	constructor() {
		this.qmDeviationQmNum = null;
		this.deviationType = null;
		this.deviationRef = null;
		this.transportRef = null;
		this.UnitRef = null;
		this.deviationDate = new Date();
		this.deviationDescription = null;
		this.details = null;
		this.originCountry = null;
		this.destinationCountry = null;
		this.module = null;
		this.carrier = null;
		this.shiper = null;
		this.timeInsurance = null;
		this.status = null;
	}
}
