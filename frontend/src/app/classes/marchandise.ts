import { EbDemande } from "./demande";
import { EbTracing } from "./tracing";
import { EbCptmRegimeSelectionDTO } from "./cptm/EbCptmRegimeSelectionDTO";
import { EbTypeGoods } from "./EbTypeGoods";
import { EcCountry } from "./country";
import { DeliveryLine } from "./ebDelLivraisonLine";
import { IField } from "./customField";

export class EbMarchandise {
	ebMarchandiseNum: number;
	storageTemperature: string;
	weight: number;
	length: number;
	width: number;
	heigth: number;
	dg: string;
	dryIce: string;
	classGood: string;
	un: number;
	batch: string;
	hsCode: string;
	equipment: string;
	sensitive: string;
	customerReference: string;
	customerOrderReference: string;
	quantityDryIce: string;
	numberOfUnits: number;
	volume: number;
	dangerousGood: number;
	packaging: string;
	comment: string;
	stackable: boolean;
	exportControl: boolean;
	exportControlStatut: number;
	ebDemande: EbDemande;
	unitReference: string;
	lot: string;
	sscc: string;
	olpn: string;
	tracking_number: string;
	etat: string;
	pood: string;
	parent: EbMarchandise;
	xEbTracing: EbTracing;
	partNumber: string;
	orderNumber: string;
	shippingType: string;
	xEbTypeConteneur: number;
	xEbTypeUnit: number;
	typeContainer: string;
	labelTypeUnit: string;
	cptmProcedureNum: number;
	cptmRegimeTemporaireNum: number;
	cptmRegimeDouanierLabel: string;
	typeGoodsNum: number;
	typeGoodsLabel: string;
	cptmOperationNumber: string;
	originCountryName: string;
	xEcCountryOrigin: number;
	countryOrigin: EcCountry;
	hasChildren: boolean;
	serialNumber: string;
	price: number;
	binLocation: string;
	exportRef: string;
	refArticle: number;

	// Front only
	selectedRegimeDouanier: EbCptmRegimeSelectionDTO;
	selectedForDeclaration: boolean;
	selectedTypeGoods: EbTypeGoods;
	children: Array<EbMarchandise>;

	xEbDemandeInitial: number;
	refTransportInitial: number;
	xEbLivraisonLines: Array<DeliveryLine>;
	idEbDelLivraison: number;
	serialized: Boolean;

	itemNumber: number;
	itemName: string;

	// champs reservé pour customFields
	listCustomsFields: Array<IField>;
	customFields: string;

	constructor() {
		this.ebMarchandiseNum = null;
		this.storageTemperature = null;
		this.weight = null;
		this.length = null;
		this.width = null;
		this.heigth = null;
		this.dg = null;
		this.dryIce = null;
		this.classGood = null;
		this.un = null;
		this.batch = null;
		this.hsCode = null;
		this.equipment = null;
		this.sensitive = null;
		this.customerReference = null;
		this.customerOrderReference = null;
		this.quantityDryIce = null;
		this.numberOfUnits = null;
		this.volume = null;
		this.dangerousGood = null;
		this.packaging = null;
		this.comment = null;
		this.stackable = null;
		this.ebDemande = new EbDemande();
		this.unitReference = null;
		this.lot = null;
		this.sscc = null;
		this.olpn = null;
		this.tracking_number = null;
		this.etat = null;
		this.pood = null;
		this.xEbTypeConteneur = null;
		this.parent = null;
		this.xEbTracing = null;
		this.partNumber = null;
		this.orderNumber = null;
		this.shippingType = null;
		this.xEbTypeUnit = null;
		this.labelTypeUnit = null;
		this.typeContainer = null;
		this.cptmOperationNumber = null;
		this.xEcCountryOrigin = null;

		this.selectedForDeclaration = false;
		this.xEbDemandeInitial = null;
		this.refTransportInitial = null;
		this.children = new Array<EbMarchandise>();
		this.price = null;
		this.binLocation = null;
		this.exportControl = null;
		this.exportControlStatut = null;
		this.exportRef = null;
		this.refArticle = null;
		this.xEbLivraisonLines = new Array<DeliveryLine>();
		this.idEbDelLivraison = null;
		this.serialized = null;

		this.listCustomsFields = null;
		this.customFields = null;

		this.itemNumber = null;
		this.itemName = null;
	}

	constructorCopy(ebMarchandise: EbMarchandise) {}
}
