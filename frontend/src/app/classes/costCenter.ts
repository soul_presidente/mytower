import { EbEtablissement } from "./etablissement";
import { EbCompagnie } from "./compagnie";

export class EbCostCenter {
	ebCostCenterNum: number;
	libelle: string;
	deleted: boolean;
	compagnie: EbCompagnie;

	constructor() {
		this.init();
	}

	private init() {
		this.ebCostCenterNum = null;
		this.libelle = null;
		this.deleted = null;
		this.compagnie = null;
	}

	constructorCopy(ebCostCenter: EbCostCenter) {
		this.init();
		this.ebCostCenterNum = ebCostCenter.ebCostCenterNum;
		this.deleted = ebCostCenter.deleted;
		this.libelle = ebCostCenter.libelle;
		if (ebCostCenter.compagnie != null) {
			ebCostCenter.compagnie = null;
			this.compagnie = ebCostCenter.compagnie;
		}
	}
}
