import { EbCompagnie } from "@app/classes/compagnie";
import {EbUser} from "@app/classes/user";

export class PrMtcTransporteur {
	prMtcTransporteurNum: number;
	ref: string;
	codeTransporteurMtc: string;
	xEbCompagnieTransporteur: EbCompagnie;
	xEbCompagnieChargeur: EbCompagnie;
	xEbUser : EbUser;
	codeConfigurationEDI : string;


	constructor() {
		this.prMtcTransporteurNum = null;
		this.ref = null;
		this.codeTransporteurMtc = null;
		this.xEbCompagnieTransporteur = null;
		this.xEbCompagnieChargeur = null;
		this.xEbUser = null;
		this.codeConfigurationEDI = null;
	}
}
