import { EbMarchandise } from "./marchandise";
import { EbEtablissement } from "./etablissement";

export class IncidentLine {
	ebQmIncidentLineNum: number;
	refTransport: string;
	ebMarchandiseNum: number;
	unitReference: string;
	typeMarchandise: string;
	numberOfUnits: number;
	dangerousGood: number;
	weight: number;
	comment: string;
	carrierName: String;
	carrier: EbEtablissement;
	eta: Date;
	etd: Date;
	ata: Date;
	atd: Date;
	weightImpacted: number;
	numberOfUnitsImpacted: number;
	delayDepartureDate: Date;
	delayArrivalDate: Date;

	constructor(marchandise: EbMarchandise) {
		this.ebMarchandiseNum = marchandise.ebMarchandiseNum;
		this.refTransport = marchandise.ebDemande.refTransport;
		this.unitReference = marchandise.unitReference;
		this.ebQmIncidentLineNum = null;
		this.typeMarchandise = marchandise.xEbTypeUnit ? marchandise.xEbTypeUnit.toString() : "";
		this.numberOfUnits = marchandise.numberOfUnits;
		this.weight = marchandise.weight;
		this.dangerousGood = marchandise.dangerousGood;
		this.comment = marchandise.comment;
		this.atd = null;
		this.eta = marchandise.ebDemande.eta;
		this.etd = marchandise.ebDemande.etd;
		this.ata = null;
		this.carrierName = null;
		this.carrier = new EbEtablissement();
		this.weightImpacted = null;
		this.numberOfUnitsImpacted = null;
		this.delayDepartureDate = null;
		this.delayArrivalDate = null;
	}
}
