export class NotificationTypeCount {
    nbrAlerts : number;
    nbrNotifys : number;
    nbrMsgs : number;

    constructor(){
        this.nbrAlerts = null;
        this.nbrNotifys = null;
        this.nbrMsgs = null;
    }
}
