import { EbCategorie } from "./categorie";

export class EbLabel {
	ebLabelNum: number;
	libelle: string;
	categorie: EbCategorie;
	ebCategorieNum: number;
	ebCategorieLibelle: string;

	constructor() {
		this.ebLabelNum = null;
		this.libelle = null;
		this.categorie = null;
		this.ebCategorieNum = null;
		this.ebCategorieLibelle = null;
	}

	constructorCopy(ebLabel: EbLabel) {
		this.ebLabelNum = ebLabel.ebLabelNum;
		this.libelle = ebLabel.libelle;
		this.ebCategorieNum = ebLabel.ebCategorieNum;
		this.ebCategorieLibelle = ebLabel.ebCategorieLibelle;
		if (ebLabel.categorie != null) {
			ebLabel.categorie.labels = null;
			this.categorie = ebLabel.categorie;
		}
	}
}
