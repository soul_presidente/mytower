import {EbCompagnie} from "@app/classes/compagnie";

export class CostCategorie {
	code?: string;
	ebCostCategorieNum: number;
	libelle: string;
	listEbCost: Array<Cost>;
	xEcModeTransport: number;
	price?: number;
	priceEuro?: number;
	isCollapsed: boolean;
	priceReal?: number;
	priceEuroReal?: number;
	priceGab?: number;
	totalPriceConverted?: number;
	xEbCompagnie?: EbCompagnie;

	constructor() {
		this.libelle = null;
		this.listEbCost = new Array<Cost>();
		this.isCollapsed = true;
	}
}

export class Cost {
	code?: string;
	ebCostNum?: number;
	ebCostOrder?: number;
	libelle?: String;
	price?: number;
	priceEuro?: number;
	xEcCurrencyNum?: number;
	type?: number;
	euroExchangeRate?: number;
	codeCurrency?: String;
	priceReal?: number;
	priceEuroReal?: number;
	xEcCurrencyNumReal?: number;
	priceGab?: number;
	priceGabConverted?: number;
	euroExchangeRateReal?: number;
	priceConverted?: number;
	priceRealConverted?: number;
	currencyInvoice?: string;
	ebCompagnieNum?: string;
	libelleCostItem?: string;
	actived?: boolean;
	xEbCostCategorie?: CostCategorie;
	listCostCategorie?:Array<CostCategorie>;
	xEbCompagnie?: EbCompagnie;
	listModeTransport?: string;
	constructor() {
		this.xEbCostCategorie = new CostCategorie();
		this.listCostCategorie = new Array<CostCategorie>();
	}
}
