import { EbDkOpeningDayDTO } from "./EbDkOpeningDayDTO";
import { EbDkOpeningPeriodDTO } from "./EbDkOpeningPeriodDTO";

export class EbDkOpeningWeekDTO {
	ebDkOpeningWeekNum: number;
	isDefault: boolean;
	label: String;
	ebEntrepotNum: number;
	days: Array<EbDkOpeningDayDTO>;
	periods: Array<EbDkOpeningPeriodDTO>;
	selected: boolean;
	constructor() {
		this.ebDkOpeningWeekNum = null;
		this.isDefault = null;
		this.label = null;
		this.ebEntrepotNum = null;
		this.days = new Array<EbDkOpeningDayDTO>();
		this.periods = new Array<EbDkOpeningPeriodDTO>();
		this.selected = null;
	}
	constructorCopy(listWeek: EbDkOpeningWeekDTO) {
		this.ebDkOpeningWeekNum = listWeek.ebDkOpeningWeekNum;
		this.isDefault = listWeek.isDefault;
		this.label = listWeek.label;
		this.ebEntrepotNum = listWeek.ebEntrepotNum;
		this.days = new Array<EbDkOpeningDayDTO>();
		if (listWeek.days != null) {
			listWeek.days.forEach((element) => {
				let day: EbDkOpeningDayDTO = new EbDkOpeningDayDTO();
				day.constructorCopy(element);
				this.days.push(day);
			});
		}
		this.periods = listWeek.periods;
		this.selected = listWeek.selected;
	}
}
