import { JourEnum } from "../jourEnum";
import { EbDockDTO } from "./EbDockDTO";
import { EbRangeHourDTO } from "../EbRangeHourDTO";

export class EbDkOpeningDayDTO {
	openingDayNum: number;
	day: JourEnum;
	ebdocks: Array<EbDockDTO>;
	open: boolean;
	hours: Array<EbRangeHourDTO>;
	specialPeriod: any;
	isRowSpan: boolean;
	constructor() {
		this.openingDayNum = null;
		this.day = null;
		this.ebdocks = new Array<EbDockDTO>();
		this.open = null;
		this.hours = new Array<EbRangeHourDTO>();
		this.specialPeriod = null;
		this.isRowSpan = false;
	}
	constructorCopy(listDay: EbDkOpeningDayDTO) {
		this.openingDayNum = listDay.openingDayNum;
		this.day = listDay.day;
		this.ebdocks = listDay.ebdocks;
		this.open = listDay.open;
		this.hours = listDay.hours != null ? listDay.hours : new Array<EbRangeHourDTO>();
		this.specialPeriod = listDay.specialPeriod;
		this.isRowSpan = listDay.isRowSpan;
	}
}
