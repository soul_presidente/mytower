export enum GenericEnumDOCK {
	OpeningPeriodType = "DOCK.OpeningPeriodType",
}

export enum OpeningPeriodType {
	NORMAL = 0,
	EXCEPTIONAL = 1,
}
