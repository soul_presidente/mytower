import { OpeningPeriodType } from "./EnumerationDOCK";

export class EbDkOpeningPeriodDTO {
	ebDkOpeningPeriodNum: number;
	label: string;
	startDate: Date;
	endDate: Date;
	typePeriod: number;
	ebDkOpeningWeekNum: number;

	constructor() {
		this.typePeriod = OpeningPeriodType.NORMAL;
	}
}
