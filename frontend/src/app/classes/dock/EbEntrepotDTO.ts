import { EbDockDTO } from "./EbDockDTO";
import { EbEtablissement } from "../etablissement";

export class EbEntrepotDTO {
	ebEntrepotNum: number;
	reference: string;
	libelle: string;
	deleted: boolean;
	docks: Array<EbDockDTO>;
	ebEtablissementNum: number;

	// View Only
	etablissement: EbEtablissement;

	constructor() {
		this.deleted = false;
		this.docks = [];
	}
	constructorCopy(data: EbEntrepotDTO) {
		this.ebEntrepotNum = data.ebEntrepotNum;
		this.reference = data.reference;
		this.libelle = data.libelle;
		this.deleted = data.deleted;
		this.docks = data.docks;
		this.ebEtablissementNum = data.ebEtablissementNum;
	}
	copy(data: EbEntrepotDTO): EbEntrepotDTO {
		this.ebEntrepotNum = data.ebEntrepotNum;
		this.reference = data.reference;
		this.libelle = data.libelle;
		this.deleted = data.deleted;
		this.docks = data.docks;
		this.ebEtablissementNum = data.etablissement
			? data.etablissement.ebEtablissementNum
			: data.ebEtablissementNum;
		return this;
	}

	static constructorCopyDto(data: EbEntrepotDTO): EbEntrepotDTO {
		let ent: EbEntrepotDTO = new EbEntrepotDTO().copy(data);
		return ent;
	}
}
