export class EbDockDTO {
	ebDockNum: number;
	nom: string;
	comment: string;
	couleur: string;
	ebEntrepotNum: number;
	reference: string;
	constructor() {
		this.ebDockNum = null;
		this.nom = null;
		this.comment = null;
		this.couleur = null;
		this.ebEntrepotNum = null;
		this.reference = null;
	}
}
