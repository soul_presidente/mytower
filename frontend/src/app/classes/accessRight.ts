import { EcModule } from "./EcModule";
import { EbUser } from "./user";

export class ExUserModule {
	module: EcModule;
	ebUser: EbUser;
	ebUserNum: number;
	accessRight: number;
	ecModuleNum: number;

	constructor(xEcModule?: number) {
		this.ebUser = null;
		this.accessRight = null;
		this.ebUserNum = null;
		this.ecModuleNum = xEcModule || null;
		if (xEcModule) {
			this.module = new EcModule();
			this.ecModuleNum = xEcModule;
		} else this.module = null;
	}

	constructorCopy(exUserModule: ExUserModule) {
		this.module = exUserModule.module;
		this.ebUser = exUserModule.ebUser;
		this.ebUserNum = exUserModule.ebUserNum;
		this.accessRight = exUserModule.accessRight;
		this.ecModuleNum = exUserModule.ecModuleNum;
	}
}
