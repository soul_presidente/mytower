import { EbEtablissement } from "./etablissement";
import { EbUser } from "./user";
import { EbCompagnie } from "./compagnie";

export class PricingCarrierSearchResult {
	ebUserNum: number;
	nom: string;
	email: string;
	ebEtablissement: EbEtablissement;
	calculatedPrice: number;
	ebCompagnie: EbCompagnie;
	isFromTransPlan: boolean;

	constructor() {
		this.ebUserNum = null;
		this.nom = null;
		this.email = null;
		this.ebEtablissement = new EbEtablissement();
		this.calculatedPrice = null;
		this.ebCompagnie = null;
		this.isFromTransPlan = null;
	}

	constructorCopy(transporteur: PricingCarrierSearchResult): EbUser {
		return new PricingCarrierSearchResult().copy(transporteur);
	}

	copy(transporteur: PricingCarrierSearchResult): EbUser {
		let user = new EbUser();
		// this.ebUserNum = transporteur.ebUserNum;
		// this.nom = transporteur.nom;
		// this.email = transporteur.email;
		// this.ebEtablissement = transporteur.ebEtablissement;
		// this.calculatedPrice=transporteur.calculatedPrice;
		// this.ebCompagnie=transporteur.ebEtablissement.ebCompagnie;
		// this.isFromTransPlan=true;
		// return this;

		user.ebUserNum = transporteur.ebUserNum;
		user.nom = transporteur.nom;
		user.email = transporteur.email;
		user.ebEtablissement = transporteur.ebEtablissement;
		user.calculatedPrice = transporteur.calculatedPrice;
		user.ebCompagnie = transporteur.ebEtablissement.ebCompagnie;
		user.isFromTransPlan = true;
		return user;
	}
	copyFromUser(user: EbUser): EbUser {
		// this.ebUserNum=user.ebUserNum;
		// this.nom = user.nom;
		// this.email = user.email;
		// this.ebEtablissement = user.ebEtablissement;
		// this.calculatedPrice=null;
		// this.ebCompagnie=user.ebCompagnie;
		// this.isFromTransPlan=false;
		// return this;
		let u = new EbUser();
		u.ebUserNum = user.ebUserNum;
		u.nom = user.nom;
		u.email = user.email;
		u.ebEtablissement = user.ebEtablissement;
		// user.calculatedPrice=transporteur.calculatedPrice;
		u.ebCompagnie = user.ebCompagnie;
		u.isFromTransPlan = false;
		return u;
	}
}
