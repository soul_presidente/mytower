import { EcCurrency } from "./currency";
import { EbCompagnie } from "./compagnie";
import { EbEtablissement } from "./etablissement";
import { EbUser } from "./user";
import { EbDemande } from "./demande";

export class EbCustomsInformation {
	ebCustomsInformationNum: number;

	declarationNumber: string;
	control: number;
	customsDuty: number;
	costCustomsDuty: number;
	xEcCurrencyCustomsDuty: EcCurrency;
	vat: number;
	costVat: number;
	xEcCurrencyVat: EcCurrency;
	otherTaxes: number;
	costOtherTaxes: number;
	xEcCurrencyOtherTaxes: EcCurrency;

	xEbUser: EbUser;
	xEbEtablissement: EbEtablissement;
	xEbCompagnie: EbCompagnie;
	xEbDemande: EbDemande;

	constructor() {
		this.ebCustomsInformationNum = null;

		this.declarationNumber = null;
		this.control = null;
		this.customsDuty = null;
		this.costCustomsDuty = null;
		this.xEcCurrencyCustomsDuty = null;
		this.vat = null;
		this.costVat = null;
		this.xEcCurrencyVat = null;
		this.otherTaxes = null;
		this.costOtherTaxes = null;
		this.xEcCurrencyOtherTaxes = null;

		this.xEbUser = new EbUser();
		this.xEbEtablissement = new EbEtablissement();
		this.xEbCompagnie = new EbCompagnie();
		this.xEbDemande = new EbDemande();
	}

	copy(data: EbCustomsInformation) {
		this.ebCustomsInformationNum = data.ebCustomsInformationNum;

		this.declarationNumber = data.declarationNumber;
		this.control = data.control;
		this.customsDuty = data.customsDuty;
		this.costCustomsDuty = data.costCustomsDuty;
		this.xEcCurrencyCustomsDuty = data.xEcCurrencyCustomsDuty;
		this.vat = data.vat;
		this.costVat = data.costVat;
		this.xEcCurrencyVat = data.xEcCurrencyVat;
		this.otherTaxes = data.otherTaxes;
		this.costOtherTaxes = data.costOtherTaxes;
		this.xEcCurrencyOtherTaxes = data.xEcCurrencyOtherTaxes;

		this.xEbUser = new EbUser();
		this.xEbUser.ebUserNum = data.xEbUser.ebUserNum;
		this.xEbEtablissement = new EbEtablissement();
		this.xEbEtablissement.ebEtablissementNum = data.xEbEtablissement.ebEtablissementNum;
		this.xEbCompagnie = new EbCompagnie();
		this.xEbCompagnie.ebCompagnieNum = data.xEbCompagnie.ebCompagnieNum;
		this.xEbDemande = new EbDemande();
		this.xEbDemande.ebDemandeNum = data.xEbDemande.ebDemandeNum;
	}
}
