import { EbCompagnie } from "./compagnie";
import { EbTtSchemaPsl } from "./EbTtSchemaPsl";
import { EbTypeDocuments } from "./ebTypeDocuments";
import { EbIncoterm } from "./EbIncoterm";

export class EbTypeFlux {
	ebTypeFluxNum: number;
	xEcModeTransport: number;
	designation: string;
	code: string;
	listModeTransport: string;
	listEbTtSchemaPsl: string;
	listEbTypeDocuments: string;
	listEbIncoterm: string;
	xEbCompagnie: EbCompagnie;
	isDeleted: boolean;
	trackingLevel: number;

	constructor() {
		this.ebTypeFluxNum = null;
		this.xEcModeTransport = null;
		this.designation = null;
		this.code = null;
		this.listModeTransport = null;
		this.listEbTtSchemaPsl = null;
		this.listEbTypeDocuments = null;
		this.xEbCompagnie = null;
		this.listEbIncoterm = null;
		this.isDeleted = null;
		this.trackingLevel = null;
	}
}
