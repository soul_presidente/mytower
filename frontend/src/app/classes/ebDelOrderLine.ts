import { EcCountry } from "./country";
import { EbTypeUnit } from "./typeUnit";
import { IField } from "./customField";
import { EbCategorie } from "./categorie";
import { EcCurrency } from "./currency";

export class OrderLine {
	ebDelOrderLineNum: number;
	refArticle: string;
	itemNumber: string;
	articleName: string;
	itemName: string;
	dateOfAvailability: string;
	canceledReason: string;
	originCountry: string;
	xEcCountryOrigin: EcCountry;
	ebTypeUnitNum: number;
	xEbtypeOfUnit: EbTypeUnit;
	weight: number;
	width: number;
	hsCode: string;
	height: number;
	length: number;
	serialNumber: string;
	partNumber: string;
	comment: string;
	specification: string;
	isSelected: boolean;

	quantityOrdered: number;
	quantityPlannable: number;
	quantityPlanned: number;
	quantityPending: number;
	quantityConfirmed: number;
	quantityShipped: number;
	quantityDelivered: number;
	quantityCanceled: number;

	// champs reservé pour customFields
	customFields: string;
	listCustomsFields: Array<IField>;
	listCategories: Array<EbCategorie>;
	serialized: Boolean;
	
	price: number;
	currency: number;
	eccn: string;

	constructor() {
		this.ebDelOrderLineNum = null;
		this.refArticle = "";
		this.articleName = "";
		this.dateOfAvailability = null;
		this.canceledReason = "";
		this.originCountry = "";
		this.xEcCountryOrigin = null;
		this.ebTypeUnitNum = null;
		this.height = 0;
		this.length = 0;
		this.isSelected = false;
		this.itemNumber = null;
		this.itemName = null;
		this.weight = 0;
		this.width = 0;
		this.hsCode = null;
		this.serialNumber = null;
		this.partNumber = null;
		this.comment = null;
		this.specification = null;
		this.serialized = null;
		this.listCustomsFields = null;
		this.customFields = null;
		this.price = null;
		this.currency = null;
		this.eccn = "";
	}
}
