export enum GenericEnumTypeOfRequest {
	SenseChronoPricing = "TYPE_REQUEST.SenseChronoPricing",
	BlocageResponsePricing = "TYPE_REQUEST.BlocageResponsePricing",
}
