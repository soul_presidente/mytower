import { EbDemande } from "./demande";
import { EbCommande } from "./commande";

export class ExDemandeCommandeId {
	xEbdemande: EbDemande;
	xEbCommande: EbCommande;

	constructor() {
		this.xEbdemande = new EbDemande();
		this.xEbCommande = new EbCommande();
	}
}
