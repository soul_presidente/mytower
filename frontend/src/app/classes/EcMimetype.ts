export class EcMimetype {
	ecMimetypeNum: number;
	mimetype: string;
	description: string;

	constructor() {
		this.ecMimetypeNum = null;
		this.mimetype = null;
		this.description = null;
	}
}
