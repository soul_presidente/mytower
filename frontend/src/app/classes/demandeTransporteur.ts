import { EbDemande } from "./demande";
import { EbUser } from "./user";
import { EbEtablissement } from "./etablissement";
import { EcCountry } from "./country";
import { EbCompagnieCurrency } from "./compagnieCurrency";
import { CostCategorie } from "./costCategorie";
import { Statique } from "@app/utils/statique";
import { EbCompagnie } from "./compagnie";
import { EbTtCompagnyPslLightDto } from "./ebTtCompagnyPslLightDto";
import { EbPlCostItemDTO } from "./trpl/EbPlCostItemDTO";

export class ExEbDemandeTransporteur {
	exEbDemandeTransporteurNum: number;
	pickupTime: Date;
	deliveryTime: Date;
	transitTime: number;
	price: number;
	priceEuroReal: number;
	priceGab: number;
	comment: string;
	status: number;
	xEbEtablissement: EbEtablissement;
	xEbCompagnie: EbCompagnie;

	leadTimeDeparture: number;
	xEcCountryDeparture: EcCountry;
	cityDeparture: string;

	leadTimeArrival: number;
	xEcCountryArrival: EcCountry;
	cityArrival: string;

	leadTimePickup: number;
	xEcCountryPickup: EcCountry;
	cityPickup: string;

	leadTimeDelivery: number;
	xEcCountryDelivery: EcCountry;
	cityDelivery: string;

	leadTimeCustoms: number;
	xEcCountryCustoms: EcCountry;
	cityCustoms: string;

	xEbDemande: EbDemande;
	xTransporteur: EbUser;
	listCostCategorie: Array<CostCategorie>;
	strListCostCategorie: string;
	xEbCompagnieCurrency: EbCompagnieCurrency;

	xEbUserCt: EbUser;
	xEbUserConfirmBy: EbUser;

	ctCommunity: boolean;
	isFromTransPlan: boolean;
	exchangeRateFound: boolean;
	priceReal: number;
	refPlan: string;
	refGrille: string;

	emailTransporteur: string;
	nomPrenom: string;

	isChecked: boolean;

	exchangeRate: string;
	infosForCustomsClearance: string;
	listEbTtCompagniePsl: Array<EbTtCompagnyPslLightDto>;
	xEcModeTransport: number;
	listPlCostItem: Array<EbPlCostItemDTO>;
	horsGrille: boolean;

	constructor() {
		this.exEbDemandeTransporteurNum = null;
		this.pickupTime = null;
		this.deliveryTime = null;
		this.price = null;
		this.comment = null;
		this.status = null;
		this.xEbEtablissement = null;
		this.xEbCompagnie = null;
		this.leadTimeDeparture = null;
		this.xEcCountryDeparture = null;
		this.cityDeparture = null;
		this.leadTimeArrival = null;
		this.xEcCountryArrival = null;
		this.cityArrival = null;
		this.leadTimePickup = null;
		this.xEcCountryPickup = null;
		this.cityPickup = null;
		this.leadTimeDelivery = null;
		this.xEcCountryDelivery = null;
		this.cityDelivery = null;
		this.leadTimeCustoms = null;
		this.xEcCountryCustoms = null;
		this.cityCustoms = null;
		this.xEbDemande = null;
		this.xTransporteur = null;
		this.listCostCategorie = null;
		this.xEbCompagnieCurrency = null;
		this.xEbUserCt = null;
		this.xEbUserConfirmBy = null;
		this.strListCostCategorie = null;
		this.ctCommunity = null;
		this.isFromTransPlan = null;
		this.exchangeRateFound = null;
		this.priceReal = null;
		this.emailTransporteur = null;
		this.nomPrenom = null;
		this.refPlan = null;
		this.exchangeRate = null;
		this.infosForCustomsClearance = null;
		this.isChecked = false;
		this.listEbTtCompagniePsl = null;
		this.xEcModeTransport = null;
		this.priceEuroReal = null;
		this.priceGab = null;
		this.horsGrille = false;
	}

	copy(exEbDemandeTransporteur: ExEbDemandeTransporteur) {
		this.exEbDemandeTransporteurNum = exEbDemandeTransporteur.exEbDemandeTransporteurNum;
		this.deliveryTime = exEbDemandeTransporteur.deliveryTime;
		this.pickupTime = exEbDemandeTransporteur.pickupTime;
		this.price = exEbDemandeTransporteur.price;
		this.status = exEbDemandeTransporteur.status;
		this.transitTime = exEbDemandeTransporteur.transitTime;
		this.leadTimeDelivery = exEbDemandeTransporteur.leadTimeDelivery;
		this.leadTimeDeparture = exEbDemandeTransporteur.leadTimeDeparture;
		this.xEcCountryDeparture = exEbDemandeTransporteur.xEcCountryDeparture;
		this.cityDeparture = exEbDemandeTransporteur.cityDeparture;

		this.leadTimeArrival = exEbDemandeTransporteur.leadTimeArrival;
		this.xEcCountryArrival = exEbDemandeTransporteur.xEcCountryArrival;
		this.cityArrival = exEbDemandeTransporteur.cityArrival;

		this.leadTimePickup = exEbDemandeTransporteur.leadTimePickup;
		this.xEcCountryPickup = exEbDemandeTransporteur.xEcCountryPickup;
		this.cityPickup = exEbDemandeTransporteur.cityPickup;

		this.xEcCountryDelivery = exEbDemandeTransporteur.xEcCountryDelivery;
		this.cityDelivery = exEbDemandeTransporteur.cityDelivery;

		this.leadTimeCustoms = exEbDemandeTransporteur.leadTimeCustoms;
		this.xEcCountryCustoms = exEbDemandeTransporteur.xEcCountryCustoms;
		this.cityCustoms = exEbDemandeTransporteur.cityCustoms;

		this.strListCostCategorie = exEbDemandeTransporteur.strListCostCategorie;
		this.priceReal = exEbDemandeTransporteur.priceReal;

		this.ctCommunity = exEbDemandeTransporteur.ctCommunity;
		this.nomPrenom = exEbDemandeTransporteur.nomPrenom;
		this.refPlan = exEbDemandeTransporteur.refPlan;
		this.listEbTtCompagniePsl = exEbDemandeTransporteur.listEbTtCompagniePsl;

		if (exEbDemandeTransporteur.xEbEtablissement) {
			this.xEbEtablissement = new EbEtablissement();
			this.xEbEtablissement.ebEtablissementNum =
				exEbDemandeTransporteur.xEbEtablissement.ebEtablissementNum;
		}

		if (exEbDemandeTransporteur.xEbCompagnie) {
			this.xEbCompagnie = new EbCompagnie();
			this.xEbCompagnie.ebCompagnieNum = exEbDemandeTransporteur.xEbCompagnie.ebCompagnieNum;
			this.xEbCompagnie.code =
				exEbDemandeTransporteur.xEbCompagnie && exEbDemandeTransporteur.xEbCompagnie.code != null
					? exEbDemandeTransporteur.xEbCompagnie.code
					: exEbDemandeTransporteur.xEbEtablissement &&
					  exEbDemandeTransporteur.xEbEtablissement.ebCompagnie
					? exEbDemandeTransporteur.xEbEtablissement.ebCompagnie.code
					: null;
		}

		if (exEbDemandeTransporteur.xTransporteur) {
			this.xTransporteur = new EbUser();
			this.xTransporteur.ebUserNum = exEbDemandeTransporteur.xTransporteur.ebUserNum;
		}

		if (exEbDemandeTransporteur && exEbDemandeTransporteur.xEbDemande) {
			this.xEbDemande = new EbDemande();
			this.xEbDemande.ebDemandeNum = exEbDemandeTransporteur.xEbDemande.ebDemandeNum;
		}

		if (exEbDemandeTransporteur.xEbCompagnieCurrency != null) {
			this.xEbCompagnieCurrency = Statique.cloneObject(
				exEbDemandeTransporteur.xEbCompagnieCurrency
			);
		}

		if (exEbDemandeTransporteur.listCostCategorie != null)
			this.listCostCategorie = exEbDemandeTransporteur.listCostCategorie;

		if (exEbDemandeTransporteur.xEbUserCt) {
			this.xEbUserCt = new EbUser();
			this.xEbUserCt.ebUserNum = exEbDemandeTransporteur.xEbUserCt.ebUserNum;
		}

		if (exEbDemandeTransporteur.xEbUserConfirmBy) {
			this.xEbUserConfirmBy = new EbUser();
			this.xEbUserConfirmBy.ebUserNum = exEbDemandeTransporteur.xEbUserConfirmBy.ebUserNum;
		}
		this.isFromTransPlan = exEbDemandeTransporteur.isFromTransPlan;
		this.exchangeRateFound = exEbDemandeTransporteur.exchangeRateFound;
		this.emailTransporteur = exEbDemandeTransporteur.emailTransporteur;
		this.exchangeRate = exEbDemandeTransporteur.exchangeRate;
		this.infosForCustomsClearance = exEbDemandeTransporteur.infosForCustomsClearance;
		this.xEcModeTransport = exEbDemandeTransporteur.xEcModeTransport;
	}

	copyData(exEbDemandeTransporteur: ExEbDemandeTransporteur) {
		this.deliveryTime = exEbDemandeTransporteur.deliveryTime;
		this.pickupTime = exEbDemandeTransporteur.pickupTime;
		this.price = exEbDemandeTransporteur.price;
		this.status = exEbDemandeTransporteur.status;
		this.transitTime = exEbDemandeTransporteur.transitTime;
		this.leadTimeDelivery = exEbDemandeTransporteur.leadTimeDelivery;
		this.leadTimeDeparture = exEbDemandeTransporteur.leadTimeDeparture;
		this.xEcCountryDeparture = exEbDemandeTransporteur.xEcCountryDeparture;
		this.cityDeparture = exEbDemandeTransporteur.cityDeparture;

		this.leadTimeArrival = exEbDemandeTransporteur.leadTimeArrival;
		this.xEcCountryArrival = exEbDemandeTransporteur.xEcCountryArrival;
		this.cityArrival = exEbDemandeTransporteur.cityArrival;

		this.leadTimePickup = exEbDemandeTransporteur.leadTimePickup;
		this.xEcCountryPickup = exEbDemandeTransporteur.xEcCountryPickup;
		this.cityPickup = exEbDemandeTransporteur.cityPickup;

		this.xEcCountryDelivery = exEbDemandeTransporteur.xEcCountryDelivery;
		this.cityDelivery = exEbDemandeTransporteur.cityDelivery;

		this.leadTimeCustoms = exEbDemandeTransporteur.leadTimeCustoms;
		this.xEcCountryCustoms = exEbDemandeTransporteur.xEcCountryCustoms;
		this.cityCustoms = exEbDemandeTransporteur.cityCustoms;

		this.strListCostCategorie = exEbDemandeTransporteur.strListCostCategorie;

		if (exEbDemandeTransporteur.xEbCompagnieCurrency != null) {
			this.xEbCompagnieCurrency = Statique.cloneObject(
				exEbDemandeTransporteur.xEbCompagnieCurrency
			);
		}

		if (exEbDemandeTransporteur.listCostCategorie != null)
			this.listCostCategorie = exEbDemandeTransporteur.listCostCategorie;

		this.isFromTransPlan = exEbDemandeTransporteur.isFromTransPlan;
		this.exchangeRateFound = exEbDemandeTransporteur.exchangeRateFound;
		this.emailTransporteur = exEbDemandeTransporteur.emailTransporteur;
		this.refPlan = exEbDemandeTransporteur.refPlan;
		this.exchangeRate = exEbDemandeTransporteur.exchangeRate;
		this.infosForCustomsClearance = exEbDemandeTransporteur.infosForCustomsClearance;
		this.xEcModeTransport = exEbDemandeTransporteur.xEcModeTransport;
	}

	get isQuotationFilled(): boolean {
		return this.transitTime != null && this.deliveryTime != null && this.price != null;
	}
}
