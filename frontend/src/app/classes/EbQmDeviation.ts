import { EbEtablissement } from "./etablissement";
import { EcCountry } from "./country";

export class EbQmDeviation {
	ebQmDeviationNum: number;
	typeDeviation: string;
	refDeviation: string;
	refTransport: string;
	unitRef: string;
	deviationDate: Date;
	carrier: EbEtablissement;
	originCountry: EcCountry;
	destinationCountry: EcCountry;
	originCountryLibelle: string;
	destinationCountryLibelle: string;
	carrierName: string;

	constructor() {
		this.ebQmDeviationNum = null;
		this.typeDeviation = null;
		this.refDeviation = null;
		this.refTransport = null;
		this.unitRef = null;
		this.deviationDate = null;
		this.carrier = new EbEtablissement();
		this.originCountry = new EcCountry();
		this.destinationCountry = new EcCountry();
		this.originCountryLibelle = null;
		this.destinationCountryLibelle = null;
		this.carrierName = null;
	}
}
