export class EcIQmCancellationReason {
	ecQmCancellationReasonNum: number;
	label: string;

	constructor() {
		this.ecQmCancellationReasonNum = null;
		this.label = null;
	}
}
