export class TemplateGenParamsDTO {
	selectedTemplate: number;
	selectedFormat: number;
	downloadChecked: boolean;
	attachChecked: boolean;
	emailChecked: boolean;
	selectedTypeDoc: number;
	templateModelNum: number;
	templateModelParams: Array<string>;
	generatedFileName: string;
	emailDest: Array<string>;

	public constructor() {
		this.selectedFormat = 0;
		this.downloadChecked = true;
		this.attachChecked = false;
		this.emailChecked = false;
		this.templateModelParams = new Array<string>();
		this.emailDest = new Array<string>();
	}
}
