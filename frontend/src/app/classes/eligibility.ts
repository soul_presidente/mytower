export class Eligibility {
	code: number;
	libelle: String;
	reference: String;

	constructor() {
		this.code = null;
		this.libelle = null;
		this.reference = null;
	}
}
