export class RegroupmtPslEvent {
	ebTtPslNum: number;
	nom: string;
	date: Date;
	nature: string;
	coment: string;
	nbrDoc: number;
	courant: boolean;
	type: number;
	validateByChamp: Boolean;
	validatePsl: Boolean;

	constructor() {}
}
