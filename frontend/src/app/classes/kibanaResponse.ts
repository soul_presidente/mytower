export class KibanaResponse {
	access_token: string;
	kibana_url: string;

	constructor() {
		this.access_token = null;
		this.kibana_url = null;
	}
}
