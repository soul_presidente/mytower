export class TransportCountryInfo {
	nbExpedition: number;
	countryName: string;
}

export class CostWeightRatio {
	value: number;
	transportType: string;
}

export class TransportActivity {
	nbExpedition: number;
	transportType: string;
}

export class Value {
	value: number;
}

export class Activity {
	value: number;
	month: number;
	year: number;
}

export class CarrierActivity {
	value: number;
	name: string;
}

export class TransportReport {
	topOriginCountries: TransportCountryInfo[];
	transportsActivity: TransportActivity[];
	overallCostWeightRatio: Value;
	totalCosts: Value;
	topDestinationCountries: TransportCountryInfo[];
	costWeightRatios: CostWeightRatio[];
}

export class ActivityReport {
	activityShipmentsCurrentYear: Activity[];
	activityCostCurrentYear: Activity[];
	activityShipmentsLastYear: Activity[];
	activityCostLastYear: Activity[];
	activityPerCarrier: CarrierActivity[];
}

export class SearchCriteriaAnalytics {
	month: number;
	year: number;
	carrier: number;
	shipper: number;
	transportType: number;
	originCountry: number;
	destinationCountry: number;
	startMonth: number;
	endMonth: number;
	startYear: number;
	endYear: number;

	constructor() {
		this.month = null;
		this.year = null;
		this.carrier = null;
		this.shipper = null;
		this.transportType = null;
		this.originCountry = null;
		this.destinationCountry = null;
		this.startMonth = null;
		this.endMonth = null;
		this.startYear = null;
		this.endYear = null;
	}
}

export class SearchForm {
	fromMonth: string;
	toMonth: string;
	originCountry: string;
	destinationCountry: string;
	carrier: string;
}
