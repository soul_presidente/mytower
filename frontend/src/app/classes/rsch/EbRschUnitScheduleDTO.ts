import { EbRangeHourDTO } from "../EbRangeHourDTO";

export class EbRschUnitScheduleDTO {
	ebRschUnitScheduleNum: number;

	status: number;

	// Delivery informations
	demandeTpNum: number;
	demandeTpRefTransport: string;
	demandeTpLibelleOriginCountry: string;
	demandeTpLibelleDestCountry: string;
	demandeTpEntrepotUnloadingNum: number;
	demandeTpEntrepotUnloadingRef: string;
	demandeTpModeTransport: number;

	demandeTpQuoteNum: number;
	demandeTpQuoteDeliveryTime: number;

	demandeTpCurrencyInvoiceNum: number;
	demandeTpCurrencyInvoiceCode: string;
	demandeTpCurrencyInvoiceSymbol: string;

	demandePaNum: number;
	demandePaRefTransport: string;

	tracingTpNum: number;
	tracingTpCustomerReference: string;

	tracingPaNum: number;

	unitTpIndex: number;

	unitPaIndex: number;
	// End Delivery Informations

	// Combined
	zoneIntermediateNum: number;
	zoneIntermediateRef: string;
	zoneDestPaNum: number;
	zoneDestPaRef: string;
	// END Combined

	// Algo Parameters & Solution
	selected: boolean;

	priority: number;

	workDuration: Date;

	modeTransportPa: number;
	modeTransportPaLocked: boolean;

	dateDeparturePa: Date;
	dateDeparturePaLocked: boolean;

	dateArrivalPa: Date;
	dateArrivalPaLocked: boolean;

	dockNum: number;
	dockName: string;
	dockLocked: boolean;

	dateUnloadingPa: Date;
	dateUnloadingPaLocked: boolean;

	workRangeHour: EbRangeHourDTO;
	workRangeHourLocked: boolean;
	// END Algo Parameters & Solution

	// Solution result
	totalCost: number;
	// END Solution result

	constructor() {
		this.workRangeHour = new EbRangeHourDTO();
	}
}
