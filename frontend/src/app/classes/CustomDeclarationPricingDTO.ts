export class CustomDeclarationPricingDTO {
	ebDemandeNum: number;
	declarationNumber: string;
	declarationDate: Date;
	unitsNum: Array<number>;
	docsNum: Array<number>;

	constructor() {
		this.unitsNum = new Array<number>();
		this.docsNum = new Array<number>();
	}
}
