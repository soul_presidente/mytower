import { EbInvoice } from "./invoice";
import { EbUser } from "./user";

export class InvoiceFichierJoint {
	ebInvoiceFichierJointNum;
	xEbUser: EbUser;
	xEbInvoice: EbInvoice;
	idCategorie: Number;
	xEbEtablissemnt: Number;
	chemin: String;
	fileName: String;
	originFileName: String;
	attachmentDate: Date;
	tag: String;
	pslEvent: String;

	constructor(nic?: string, siren?: string) {
		this.ebInvoiceFichierJointNum = null;
		this.xEbUser = new EbUser();
		this.xEbInvoice = new EbInvoice();
		this.idCategorie = null;
		this.xEbEtablissemnt = null;
		this.chemin = null;
		this.fileName = null;
		this.originFileName = null;
		this.attachmentDate = null;
		this.tag = null;
		this.pslEvent = null;
	}
}
