import { EcCountry } from "./country";
import { EbEtablissement } from "./etablissement";
import { EbPlZoneDTO } from "./trpl/EbPlZoneDTO";
import { EbUser } from "./user";
import { EbEntrepotDTO } from "./dock/EbEntrepotDTO";

export class EbParty {
	ebPartyNum: number;
	company: string;
	adresse: String;
	city: String;
	zipCode: String;
	xEcCountry: EcCountry;
	openingHours: string;
	airport: String;
	email: String;
	phone: String;
	xEbUserVisibility: EbUser;
	etablissementAdresse: EbEtablissement;
	commentaire: String;
	reference: string;
	zone: EbPlZoneDTO;
	entrepot: EbEntrepotDTO;

	//Zone backup
	zoneNum: number;
	zoneRef: string;
	zoneDesignation: string;
	ebAdresseNum: number;

	constructor() {
		this.ebPartyNum = null;
		this.company = null;
		this.city = null;
		this.airport = null;
		this.xEcCountry = null;
		this.zipCode = null;
		this.adresse = null;
		this.openingHours = null;
		this.email = null;
		this.phone = null;
		this.xEbUserVisibility = null;
		this.etablissementAdresse = null;
		this.commentaire = null;
		this.reference = null;
		this.zone = null;
		this.entrepot = null;
		this.ebAdresseNum = null;
	}

	constructorCopy(ebParty: EbParty) {
		this.ebPartyNum = ebParty.ebPartyNum;
		this.company = ebParty.company;
		this.city = ebParty.city;
		this.phone = ebParty.phone;
		this.airport = ebParty.airport;
		if (ebParty.etablissementAdresse) {
			this.etablissementAdresse = new EbEtablissement();
			this.etablissementAdresse.ebEtablissementNum =
				ebParty.etablissementAdresse.ebEtablissementNum;
			this.etablissementAdresse.nom = ebParty.etablissementAdresse.nom;
		}
		if (ebParty.xEbUserVisibility) {
			this.xEbUserVisibility = EbUser.createUser(ebParty.xEbUserVisibility);
		}
		if (ebParty.xEcCountry) {
			this.xEcCountry = new EcCountry();
			this.xEcCountry.constructorCopy(ebParty.xEcCountry);
		} else {
			this.xEcCountry = null;
		}
		this.zipCode = ebParty.zipCode;
		this.email = ebParty.email;
		this.openingHours = ebParty.openingHours;
		this.commentaire = ebParty.commentaire;
		this.reference = ebParty.reference;
		this.zone = ebParty.zone;
		this.entrepot = ebParty.entrepot;
		this.adresse = ebParty.adresse;
		this.ebAdresseNum = ebParty.ebAdresseNum;
	}

	public static create(ebPartyNum: number, ecCountryNum: number, libelleCountry: string): EbParty {
		let prt = new EbParty();
		prt.ebPartyNum = ebPartyNum;
		prt.xEcCountry = EcCountry.create(ecCountryNum, libelleCountry);
		return prt;
	}
}
