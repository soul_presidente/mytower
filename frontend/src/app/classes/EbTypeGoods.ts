import { EbCompagnie } from "./compagnie";

export class EbTypeGoods {
	ebTypeGoodsNum: number;
	code: string;
	label: string;

	xEbCompany: EbCompagnie;

	constructor() {}
}
