export class EbModeTransport {
	code: number;
	libelle: string;
	order: number;
	icon: string;

	constructor() {
		this.code = null;
		this.libelle = null;
		this.order = null;
		this.icon = null;
	}

	static constructorCopy(item: EbModeTransport): EbModeTransport {
		const obj = new EbModeTransport();
		obj.copy(item);
		return obj;
	}

	copy(item: EbModeTransport) {
		this.libelle = item.libelle;
		this.code = item.code;
		this.order = item.order;
		this.icon = item.icon;
	}
}
