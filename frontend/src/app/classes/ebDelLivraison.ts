import { EbParty } from "./party";
import { DeliveryLine } from "./ebDelLivraisonLine";
import { EbFlag } from "./ebFlag";
import { Order } from "./ebDelOrder";
import { StatusDelivery } from "./StatusDeliveryEnum";
import { EbUser } from "./user";
import { EbDemande } from "./demande";
import { EbCategorie } from "./categorie";
import { IField } from "./customField";
import { EbCompagnie } from "./compagnie";
import { EbEtablissement } from "./etablissement";

export class Delivery {
	ebDelLivraisonNum: number;

	ebDelReference: string;

	numOrderSAP: string;

	refDelivery: string;

	deliveryNote: string;

	refTransport: string;

	numOrderEDI: string;

	orderCustomerCode: string;

	numOrderCustomer: string;

	orderCustomerName: string;

	refClientDelivered: string;

	nameClientDelivered: string;

	countryClientDelivered: string;

	expectedDeliveryDate: string;

	campaignCode: string;

	campaignName: string;

	orderCreationDate: string;

	requestedDeliveryDate: string;

	deliveryCreationDate: string;

	estimatedShipDate: Date;

	effectiveShipDate: Date;

	pickupSite: string;

	endOfPackDate: Date;

	pickDate: Date;

	invoiceNum: string;

	invoiceDate: Date;

	transportStatus: number;

	totalQuantityProducts: number;

	numParcels: number;

	totalWeight: number;

	totalVolume: number;

	xEbPartyOrigin: EbParty;

	xEbPartyDestination: EbParty;

	xEbPartySale: EbParty;

	pathToBl: string;

	xEbDelFichierJoint: number;

	xEbDelLivraisonLine: Array<DeliveryLine>;

	listFlag: String;

	listEbFlagDTO: Array<EbFlag>;

	xEbDelOrder: Order;

	xEbDemande: EbDemande;

	crossDock: string;

	statusDelivery: StatusDelivery;
	xEbOwnerOfTheRequest: EbUser;
	xEbIncotermNum: number;
	xEcIncotermLibelle: string;
	customerOrderReference: string;
	complementaryInformations: string;

	refOrder: string;

	dateOfGoodsAvailability: Date;

	listCategories: Array<EbCategorie>;

	listCustomsFields: Array<IField>;

	xEbCompagnie: EbCompagnie;

	xEbEtablissement: EbEtablissement;

	constructor() {
		this.crossDock = null;
		this.xEbDelLivraisonLine = new Array<DeliveryLine>();
		this.xEbDelOrder = null;
		this.xEbDemande = null;
		this.refDelivery = null;
		this.refTransport = null;
		this.deliveryNote = null;
		this.statusDelivery = StatusDelivery.PENDING;
		this.xEbOwnerOfTheRequest = new EbUser();
		this.xEbIncotermNum = null;
		this.xEcIncotermLibelle = null;
		this.customerOrderReference = null;
		this.complementaryInformations = null;
		this.ebDelReference = null;
		this.refOrder = null;
		this.dateOfGoodsAvailability = null;
		this.listCategories = null;
		this.listCustomsFields = null;
		this.xEbCompagnie = null;
		this.xEbEtablissement = null;
	}
	cloneData(data) {
		this.ebDelLivraisonNum = data.ebDelLivraisonNum;
		this.listCategories = data.listCategories || new Array<EbCategorie>();
		this.listCustomsFields = data.listCustomsFields;
	}
}
