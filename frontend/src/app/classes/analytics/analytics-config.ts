export class AnalyticsConfigDTO {
    kibanaUrl: string;
    kibanaSecured: boolean;

    dashboardId: AnalyticsConfigDTO.Dashboards;
}

export namespace AnalyticsConfigDTO {
    export class Dashboards {
        transportOverview: string;
        pricing: string;
        activity: string;
        trackTrace: string;
        qualityManagement: string;
        freightAudit: string;
        carrierPerformance: string;
    }
}