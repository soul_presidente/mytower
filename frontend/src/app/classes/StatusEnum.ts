export class StatusEnum {
	code: number;
	key: string;
	order: number;
	enabled: boolean;
}
