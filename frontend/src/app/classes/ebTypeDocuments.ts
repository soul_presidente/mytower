import { EbUser } from "./user";
import { EbCompagnie } from "./compagnie";

export class EbTypeDocuments {
	ebTypeDocumentsNum: number;
	nom: string;
	code: string;
	ordre: number;
	ebTypeDocumentsDateCreation: Date;
	ebTypeDocumentsDateModification: Date;
	module: Array<number>;
	ebTypeDocumentsebUser: EbUser;
	ebTypeDocumentsebCompagnie: EbCompagnie;
	activated: boolean;
	isexpectedDoc: boolean;
	nbrDoc: number;
	isDragOver: boolean;
	status: number;

	constructor() {
		this.ebTypeDocumentsNum = null;
		this.nom = null;
		this.code = null;
		this.ordre = null;
		this.ebTypeDocumentsDateCreation = null;
		this.ebTypeDocumentsDateModification = null;
		this.module = new Array<number>();
		this.ebTypeDocumentsebUser = null;
		this.ebTypeDocumentsebCompagnie = null;
		this.activated = null;
		this.isexpectedDoc = null;
		this.nbrDoc = null;
		this.isDragOver = false;
		this.status = null;
	}
}
