export interface SearchField {
	name: string;
	type: string;
	label: string;
	options?: Array<Object>; // [{libelle, value}] for normal select & [{libelle}] for autocomplete
	placeholder?: string;
	default?: any;
	listItems?: Array<Object>;
	operator?: string;
	srcListItems?: string;
	srcTypeahead?: string;
	fetchAttribute?: string;
	fetchValue?: string;
	multiple?: boolean;
	groupByAttr?: string;
	fetchGroupByAttribute?: string;
	withCriteria?: boolean;
	includeUserCompanyNum?: boolean;
	includeUserNum?: boolean;
	hasCountryTemplate?: boolean;
	hasNoAttribute?: boolean;
	addTag?: any;
	hidden?: boolean;
}
