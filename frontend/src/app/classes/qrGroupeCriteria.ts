import { QrGroupeValue } from "./qrGroupeValue";
import { QrGroupeObject } from "./qrGroupeObject";

export class QrGroupeCriteria {
	id: number;

	code: number;

	libelle: string;

	field: QrGroupeObject;

	groupValues: Array<QrGroupeValue>;

	isAnd: boolean;

	constructor() {}
}
