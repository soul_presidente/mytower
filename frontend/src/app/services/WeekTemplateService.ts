import { Statique } from "@app/utils/statique";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";

import { EbWeekTemplateDTO } from "@app/classes/week-template/EbWeekTemplateDTO";
import { SearchCriteria } from '@app/utils/searchCriteria';

@Injectable()
export class WeekTemplateService {
	constructor(private http: HttpClient) {}

	deleteWeekTemplate(ebWeekTemplate: EbWeekTemplateDTO): Observable<any> {
		return this.http.post(
			Statique.controllerWeekTemplate + "/delete-ebWeekTemplatePeriod",
			ebWeekTemplate
		);
	}
	getListWeekType() {
		return this.http.get(Statique.controllerStatiqueListe + "/weekType");
	}
	getListWeekTemplate(searchCriteria: SearchCriteria): Observable<any>{
		return this.http.post(Statique.controllerWeekTemplate+"/list-week-templates", searchCriteria);
	}
    getListDayByWeekTemplate(ebWeektemplateNum: number){
		return this.http.get(Statique.controllerWeekTemplate + "/list-day-by-weektemplate/"+ ebWeektemplateNum);
	}
	saveWeekTemplate(period: EbWeekTemplateDTO): Observable<any> {
		return this.http.post(Statique.controllerWeekTemplate + "/save-week-template", period);
	}
	updateWeekTemplate(ebWeektemplateNum: number, ebWeekTemplateDTO: EbWeekTemplateDTO) {
		return this.http.put(
			Statique.controllerWeekTemplate + "/update-week-template/" + ebWeektemplateNum,
			ebWeekTemplateDTO
		);
	}
}
