import { EventEmitter, Injectable } from "@angular/core";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { NavigationStart, Router } from "@angular/router";
import { filter } from "rxjs/operators";

/**
 * Singleton Service that store all data about Header
 */
@Injectable()
export class HeaderService {
	protected headerInfos: HeaderInfos;

	constructor(private router: Router) {
		this.headerInfos = new HeaderInfos();
		this.headerInfos.actions = [];
		this.headerInfos.breadCrumbs = new Map();

		this.router.events
			.pipe(filter((event) => event instanceof NavigationStart))
			.subscribe((event: NavigationStart) => this.onRouterNavigationStart(event));
	}

	/**
	 * All public events that can be subscribed form any component
	 */
	public events = {
		actionButtonsChange: new EventEmitter<Array<HeaderInfos.ActionButton>>(),
		breadCrumbsChange: new EventEmitter<Map<number, Array<HeaderInfos.BreadCrumbItem>>>(),
	};

	//region Action buttons

	/**
	 * Replace all Action Buttons in header
	 * @param buttons New action Buttons set
	 */
	public registerActionButtons(buttons: Array<HeaderInfos.ActionButton>) {
		let newButtons = [];
		if (buttons != null) {
			newButtons = buttons;
		}

		this.headerInfos.actions = newButtons;
		this.events.actionButtonsChange.emit(this.headerInfos.actions);
	}

	/**
	 * Clear all Action Buttons
	 */
	public clearActionButtons() {
		this.headerInfos.actions = [];
		this.events.actionButtonsChange.emit(this.headerInfos.actions);
	}

	//endregion

	//region Breadcrumb

	/**
	 * Replace all breadcrumbs items
	 * @param breadcrumbs New breadcrumb items set
	 */
	public registerBreadcrumbItems(
		breadcrumbs: Array<HeaderInfos.BreadCrumbItem>,
		component_id?: number
	) {
		if (!component_id) component_id = BreadcumbComponent.HEADER;

		let newBreadCrumbs = [];
		if (breadcrumbs != null) {
			newBreadCrumbs = breadcrumbs;
		}

		this.headerInfos.breadCrumbs.set(component_id, newBreadCrumbs);
		this.events.breadCrumbsChange.emit(this.headerInfos.breadCrumbs);
	}

	/**
	 * Clear all Breadcrumbs Items
	 */
	public clearBreadcrumbItems() {
		this.headerInfos.breadCrumbs = new Map();
		this.events.breadCrumbsChange.emit(this.headerInfos.breadCrumbs);
	}
	//endregion

	/**
	 * Router listener when a navigation is fired
	 */
	protected onRouterNavigationStart(event: NavigationStart) {
		// Reset header actions on navigation
		this.clearActionButtons();
		this.clearBreadcrumbItems();
	}
}
