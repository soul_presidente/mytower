import { SearchCriteriaQualityManagement } from "@app/utils/SearchCriteriaQualityManagement";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { EbQmIncident } from "@app/classes/ebIncident";
import { Email } from "@app/classes/email";
import { IncidentLine } from "@app/classes/incidentLine";
import { Observable } from "../../../node_modules/rxjs";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { EcQmRootCause } from "@app/classes/rootCause";
import { IncidentStatus } from "@app/utils/enumeration";

@Injectable()
export class QualityManagementService {
	constructor(private authHttp: HttpClient) {}

	searchIncidents(searchCriteriaQM: any): Observable<any> {
		return this.authHttp.post(
			Statique.controllerQualityManagement + "/listIncident",
			searchCriteriaQM
		);
	}

	saveIncidents(incidents: EbQmIncident[], isUpdate: Boolean): Observable<any> {
		return this.authHttp.post(
			Statique.controllerQualityManagement + "/save-incidents?isUpdate=" + isUpdate,
			incidents
		);
	}

	updateIncidents(incident: EbQmIncident): Observable<any> {
		return this.authHttp.put(Statique.controllerQualityManagement + "/updateIncident", incident);
	}

	listMarchandise(searchCriteriaQM: SearchCriteriaQualityManagement): Observable<any> {
		return this.authHttp.post(
			Statique.controllerQualityManagement + "/get-list-marchandise",
			searchCriteriaQM
		);
	}

	listRootCause(eventType: number = null): Observable<any> {
		return this.authHttp.get(Statique.controllerQualityManagement + "/listRootCause" + (eventType ? "?eventType=" + eventType : ""));
	}

	listIncidentCategory(): Observable<any> {
		return this.authHttp.get(Statique.controllerQualityManagement + "/list-incident-category");
	}

	listCriticalityIncidentEnum(): Observable<any> {
		return this.authHttp.get(
			Statique.controllerStatiqueListe + "/list-generic-enum?genericEnumKey=I.CRITICALITY"
		);
	}
	listRootCauseStatusEnum(): Observable<any> {
		return this.authHttp.get(
			Statique.controllerStatiqueListe + "/list-generic-enum?genericEnumKey=I.RootCauseStatus"
		);
	}

	listRootCauseCategoriesEnum(): Observable<any>{
		return this.authHttp.get(
			Statique.controllerStatiqueListe + "/list-generic-enum?genericEnumKey=I.RootCauseCategories"
		);
	}

	listIncidentQualification(): Observable<any> {
		return this.authHttp.get(Statique.controllerQualityManagement + "/list-incident-qualification");
	}

	listIncidentCloseReason(): Observable<any> {
		return this.authHttp.get(Statique.controllerQualityManagement + "/list-incident-close-reason");
	}

	saveIncident(incident: EbQmIncident): Observable<any> {
		return this.authHttp.post(Statique.controllerQualityManagement + "/save-incident", incident);
	}

	updateIncident(incident: EbQmIncident): Observable<any> {
		return this.authHttp.post(Statique.controllerQualityManagement + "/update-incident", incident);
	}

	getIncident(searchCriteria: SearchCriteriaQualityManagement): Observable<any> {
		return this.authHttp.post(
			Statique.controllerQualityManagement + "/get-incident",
			searchCriteria
		);
	}

	getSemiAutoMail(searchCriteria: SearchCriteriaQualityManagement): Observable<any> {
		return this.authHttp.post(
			Statique.controllerQualityManagement + "/get-semi-auto-mail",
			searchCriteria
		);
	}

	sendSemiAutoMail(mailToSend: Email, incidentNum: number): Observable<any> {
		let url = "/send-semi-auto-mail" + "?incidentNum=" + incidentNum;
		return this.authHttp.post(Statique.controllerQualityManagement + url, mailToSend);
	}

	updateListIncidentLine(listIncidentLine: Array<IncidentLine>): Observable<any> {
		return this.authHttp.post(
			Statique.controllerQualityManagement + "/update-list-incident-line",
			listIncidentLine
		);
	}

	updateIncidentComment(incident: EbQmIncident): Observable<any> {
		return this.authHttp.post(
			Statique.controllerQualityManagement + "/update-incident-comment",
			incident
		);
	}

	updateIncidentShipment(incident: EbQmIncident): Observable<any> {
		return this.authHttp.post(
			Statique.controllerQualityManagement + "/update-incident-shipment",
			incident
		);
	}

	updateIncidentDescription(incident: EbQmIncident): Observable<any> {
		return this.authHttp.post(
			Statique.controllerQualityManagement + "/update-incident-description",
			incident
		);
	}

	getHtmlIncident(ebQmIncidentNum: number) {
		let style = "top: 6px;cursor: pointer;",
			_type = "checkbox";
		return (
			"<input class='checkbox-Track' [ngbTooltip]='tipContent'  id=" +
			ebQmIncidentNum +
			"  type='" +
			_type +
			"' style='" +
			style +
			"'onchange='functions.clickCheckbox(" +
			ebQmIncidentNum +
			", event.target)'>"
		);
	}

	getListDocumentsIncident(ecModule, incidentNum, ebUserNum): Observable<any> {
		return this.authHttp.get(
			Statique.controllerStatiqueListe +
				"/listDocumentsIncident?module=" +
				ecModule +
				"&ebQmIncidentNum=" +
				incidentNum +
				"&ebUserNum=" +
				ebUserNum
		);
	}

	demande(searchCriteriaPricingBooking: SearchCriteriaPricingBooking): Observable<any> {
		return this.authHttp.post(
			Statique.controllerPricing + "/demande",
			searchCriteriaPricingBooking
		);
	}

	getNextEbQmIncidentNum(): Observable<any> {
		return this.authHttp.get(Statique.controllerQualityManagement + "/get-next-ebQmIncidentNum");
	}

	getListDoc(incidentNum): Observable<any> {
		let urlParams = "?incidentNum=" + incidentNum;
		return this.authHttp.get(Statique.controllerQualityManagement + "/get-list-doc" + urlParams);
	}

	// prettier-ignore
	saveFlags(ebQmIncidentNum: number, newFlags: string): Observable<any> {
    return this.authHttp.get(
      Statique.controllerQualityManagement +
        "/saveFlags?ebQmIncidentNum=" + ebQmIncidentNum +
        "&newFlags=" + newFlags
    );
  }

	saveRootCause(rootCause: EcQmRootCause): Observable<any> {
		return this.authHttp.post(Statique.controllerQualityManagement + "/save-root-cause", rootCause);
	}

	getAllUsersIntervenantInDemande(ebDemandeNum : number): Observable<any>{
		let params = "?ebDemandeNum="+ebDemandeNum;
		return this.authHttp.get(
			Statique.controllerPricing + "/get-related-users-in-demande" + params
		);
	}

	updateEventTypeStatus(ebQmIncident: number, status: number): Observable<any> {
		let params = "?ebQmIncidentNum=" + ebQmIncident + "&iStatus=" + status;
		return this.authHttp.get(
			Statique.controllerQualityManagement + "/update-eventType-status" + params
		);
	}

	getListIncidentStatus(): Observable<any> {
		return this.authHttp.get(Statique.controllerQualityManagement + "/list-incident-status");
	}

}
