import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Statique } from "@app/utils/statique";
import { EbBoutonAction } from "@app/classes/ebBoutonAction";

@Injectable({
	providedIn: "root",
})
export class BoutonActionService {
	constructor(private http: HttpClient) {}
	addBoutonAction(ebBoutonAction: EbBoutonAction): Observable<any> {
		return this.http.post(Statique.controllerBoutonAction + "/add-bouton-action", ebBoutonAction);
	}
	deleteBoutonAction(ebBoutonAction: number): Observable<any> {
		return this.http.post(
			Statique.controllerBoutonAction + "/delete-bouton-action",
			ebBoutonAction
		);
	}
	getListBoutonAction(): Observable<any> {
		return this.http.get(Statique.controllerBoutonAction + "/get-list-bouton-action");
	}
	getListBoutonActionFromCodes(listBoutonAction: string): Observable<any> {
		return this.http.post(
			Statique.controllerBoutonAction + "/get-list-bouton-action-from-codes",
			listBoutonAction
		);
	}
}
