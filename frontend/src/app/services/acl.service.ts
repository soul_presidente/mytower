import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Statique } from "@app/utils/statique";
import { EbAclSettingDTO } from "@app/classes/dto/EbAclSettingDTO";
import { EbAclRightDTO } from "@app/classes/dto/EbAclRightDTO";

@Injectable({
	providedIn: "root",
})
export class AclService {
	constructor(private http: HttpClient) {}

	listSettingsForUser(ebUserNum: number): Observable<Array<EbAclSettingDTO>> {
		return this.http.get<Array<EbAclSettingDTO>>(Statique.controllerAcl + "/list-settings-user", {
			params: {
				ebUserNum: String(ebUserNum),
			},
		});
	}

	listSettingsForProfile(ebUserProfileNum: number): Observable<Array<EbAclSettingDTO>> {
		return this.http.get<Array<EbAclSettingDTO>>(
			Statique.controllerAcl + "/list-settings-profile",
			{
				params: {
					ebUserProfileNum: String(ebUserProfileNum),
				},
			}
		);
	}

	listRightsForConnectedUser(): Observable<Array<EbAclRightDTO>> {
		return this.http.get<Array<EbAclRightDTO>>(
			Statique.controllerAcl + "/list-rights-connectedUser"
		);
	}

	saveSettings(
		settings: Array<EbAclSettingDTO>,
		ebUserProfileNum: number = undefined,
		ebUserNum: number = undefined
	) {
		let params = {};
		if (ebUserProfileNum != null) params["ebUserProfileNum"] = ebUserProfileNum;
		if (ebUserNum != null) params["ebUserNum"] = ebUserNum;

		return this.http.post<void>(Statique.controllerAcl + "/save-settings", settings, {
			params: params,
		});
	}
}
