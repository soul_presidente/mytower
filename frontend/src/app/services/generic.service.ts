import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
	providedIn: "root",
})
export class GenericService {
	constructor(private httpClient: HttpClient) {}

	runAction(url: string, data: any): Observable<any> {
		return this.httpClient.post(url, data);
	}
}
