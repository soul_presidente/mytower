import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { EbFlag } from "@app/classes/ebFlag";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from "@app/utils/searchCriteria";

@Injectable({
	providedIn: "root",
})
export class FlagService {
	constructor(private http: HttpClient) {}
	addFlag(ebFlag: EbFlag): Observable<any> {
		return this.http.post(Statique.controllerFlag + "/add-flag", ebFlag);
	}
	deleteFlag(ebFlagNum: number): Observable<any> {
		return this.http.post(Statique.controllerFlag + "/delete-flag", ebFlagNum);
	}
	getListFlag(moduleNum: number): Observable<any> {
		return this.http.get(Statique.controllerFlag + "/get-list-flag/"+moduleNum);
	}
	getListFlagFromCodes(listFlag: string): Observable<any> {
		return this.http.post(Statique.controllerFlag + "/get-list-flag-from-codes", listFlag);
	}
}
