import { Inscription } from "@app/classes/inscription";
import { Injectable } from "@angular/core";
import { EbUser } from "@app/classes/user";
import { Statique } from "@app/utils/statique";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import {EbUserDTO} from "@app/classes/dto/eb-user-dto";

@Injectable()
export class InscriptionService {
	Statique = Statique;
	constructor(private http: HttpClient) {}

	inscription(inscription: Inscription) {
		return this.http.post(Statique.controllerInscription + "", inscription);
	}

	listPays(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/countries");
	}

	getCompagnieBySiren(siren: string): Observable<any> {
		let params = "?siren=" + siren;
		return this.http.get(Statique.controllerInscription + "/getCompagnie" + params);
	}

	getEtablissementBySiret(siret: string): Observable<any> {
		let params = "?siret=" + siret;
		return this.http.get(Statique.controllerInscription + "/getEtablissement" + params);
	}

	activerCompte(activeToken: string) {
		let headers = new HttpHeaders();
		headers = headers.append("Content-Type", "application/json");
		return this.http.post(Statique.controllerInscription + "/activate-account", activeToken, {
			headers: headers,
		});
	}

	checkEmail(email: string) {
		let params = new URLSearchParams();
		params.set("email", email);
		return this.http.post(Statique.controllerInscription + "/emailExist", email);
	}

	checkUsername(username: string) {
		let params = new URLSearchParams();
		params.set("username", username);
		return this.http.post(Statique.controllerInscription + "/usernameExist", username);
	}

	saveUser(ebUser: EbUser) {
		return this.http.post(Statique.controllerInscription + "/save-user", ebUser);
	}

	insciptionUser(ebUser: EbUserDTO) {
		return this.http.post(Statique.controllerInscription + "/inscription-user", ebUser);
	}
}
