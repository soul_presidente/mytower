import { Injectable } from "@angular/core";
import { ConfirmDialog } from "@app/shared/dialogs/confirm-dialog/confirm-dialog.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { InputDialog } from "@app/shared/dialogs/input-dialog/input-dialog.component";
import { PeriodeDialog } from "@app/shared/dialogs/periode-dialog/periode-dialog.component";
import { SelectDialog } from "@app/shared/dialogs/select-dialog/select-dialog.component";
import { TranslateService } from "@ngx-translate/core";
import { MessageService } from "primeng/api";

let instance = null;

@Injectable()
export class DialogService {
	constructor(
		private modalService: NgbModal,
		private messageService: MessageService,
		private translate: TranslateService
	) {
		if (!instance) instance = this;

		return instance;
	}

	public async confirm(title: string, message: string) {
		let modalRef = this.modalService.open(ConfirmDialog);
		modalRef.componentInstance.title = title || null;
		modalRef.componentInstance.message = message || "";

		return await this.getResult(modalRef);
	}

	getResult(modalRef) {
		return new Promise((resolve, reject) => {
			modalRef.result
				.then((res) => {
					resolve(res);
				})
				.catch((reason) => {
					reject(reason);
				});
		});
	}

	public async input(title: string, message: string, value?: string) {
		let modalRef = this.modalService.open(InputDialog);
		modalRef.componentInstance.title = title || null;
		modalRef.componentInstance.message = message || "";
		modalRef.componentInstance.value = value || "";

		return await this.getResult(modalRef);
	}

	public async periodeInput(title: string, message: string) {
		let modalRef = this.modalService.open(PeriodeDialog);
		modalRef.componentInstance.title = title || null;
		modalRef.componentInstance.message = message || "";

		return await this.getResult(modalRef);
	}

	public async selectInput(
		title: string,
		message: string,
		listOptions: Array<object>,
		getOptionFunction?: Function
	) {
		let modalRef = this.modalService.open(SelectDialog);
		modalRef.componentInstance.title = title || null;
		modalRef.componentInstance.message = message || "";
		modalRef.componentInstance.listOptions = listOptions || [];
		modalRef.componentInstance.getOptionFunction = getOptionFunction || null;

		return await this.getResult(modalRef);
	}

	public toasterDisplay(event: boolean) {
		if (event == true) {
			this.messageService.add({
				severity: "success",
				summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
				detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
			});
		}
		if (event == false) {
			this.messageService.add({
				severity: "error",
				summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
				detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
			});
		}
	}
}
