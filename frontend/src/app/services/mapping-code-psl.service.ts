import { Injectable } from '@angular/core';
import { Statique } from "@app/utils/statique";
import { HttpClient, HttpParams } from "@angular/common/http";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { EdiMapPsl } from '@app/classes/mappingCodePsl';
@Injectable({
  providedIn: 'root'
})
export class MappingCodePslService {

  Statique = Statique;
  constructor(private http: HttpClient) {}

    getListMappingCodePslTable(criteria: SearchCriteria): Observable<Array<EdiMapPsl>> {
		return this.http.post(Statique.controllerMappingCodePsl + "/list-mapping-code-psl", criteria).pipe(
			map((res: any) => {
				return Statique.cloneListObject(res.data, EdiMapPsl);
			})
		);
	}
	getListMappingCodePslPromise(criteria: SearchCriteria): Promise<Array<EdiMapPsl>> {
		return new Promise((resolve) => {
			this.getListMappingCodePslTable(criteria).subscribe((res) => resolve(res));
		});
	}
  
	updateMappingCodePsl(ediMapPsl: EdiMapPsl): Observable<any> {
		return this.http.post(Statique.controllerMappingCodePsl + "/update-mapping-code-psl", ediMapPsl);
	}

	deleteMappingCodePsl(id: number): Observable<any> {
		return this.http.post(Statique.controllerMappingCodePsl + "/delete-mapping-code-psl", id);
	}
}
