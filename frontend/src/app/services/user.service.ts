import { EbUser } from "@app/classes/user";
import { EbEtablissement } from "@app/classes/etablissement";
import { Delegation } from "@app/classes/delegation";
import { Injectable } from "@angular/core";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Statique } from "@app/utils/statique";
import { EbRelation } from "@app/classes/relation";
import { Inscription } from "@app/classes/inscription";
import { Observable, Subject } from "rxjs";

import { AuthenticationService } from "./authentication.service";
import { EbDemande } from "@app/classes/demande";
import { TransfertUserObject } from "@app/classes/transfertUserobject";

@Injectable()
export class UserService {
	//jwtHelper: JwtHelper = new JwtHelper();
	//accessToken: string;
	constructor(private http: HttpClient, private authenticationService: AuthenticationService) {}

	resetPassword(email: string): Observable<any> {
		let headers = new HttpHeaders();
		headers = headers.append("Content-Type", "application/json");
		return this.http.post(Statique.controllerAccount + "/reset-password", email, {
			headers: headers,
		});
	}

	changePassword(ebUser: EbUser): Observable<any> {
		let headers = new HttpHeaders();
		headers = headers.append("Content-Type", "application/json");
		return this.http.post(Statique.controllerAccount + "/change-password-token", ebUser, {
			headers: headers,
		});
	}

	userInfo(): Observable<any> {
		return this.http.get(Statique.controllerAccount + "/account-settings");
	}

	getAllGroup(user: any): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/get-list-group-user", user);
	}

	changeUserPassword(newUser: EbUser): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/changeUserPassword", newUser);
	}

	updateUser(newUser: EbUser): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/updateUser", newUser);
	}

	updateStatusUser(newUser: EbUser): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/updateStatusUser", newUser);
	}

	updateAdminAndSuperAdminUser(newUser: EbUser): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/updateAdminAndSuperAdminUser", newUser);
	}

	updateUserDetails(newUser: EbUser): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/updateUserDetails", newUser);
	}

	updateEtablissement(etablissement: EbEtablissement): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/updateEtablissement", etablissement);
	}

	matchUserPassword(ebUserNum: number, password: String): Observable<any> {
		let criteria: SearchCriteria = new SearchCriteria();
		criteria.ebUserNum = ebUserNum;
		criteria.ebUserPassword = password;
		return this.http.post(Statique.controllerAccount + "/check-user-password", criteria);
	}

	createDelegation(ebDelegation: Delegation): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/create-delegation", ebDelegation);
	}

	addToAll(ebRelation: EbRelation, selectedChargeurs: Array<EbUser>): Observable<any> {
		let formData: any = {
			selectChargeurs: selectedChargeurs,
			ebRelation: ebRelation,
		};
		return this.http.post(Statique.controllerCommunity + "/add-to-another-chargeurs", formData);
	}

	deleteDelegation(ebDelegationNum: number): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/deleteDelegation", ebDelegationNum);
	}

	getDelegation(criteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/get-delegation", criteria);
	}

	static getConnectedUser(): EbUser {
		return AuthenticationService.getConnectedUser();
	}

	static isAuthenticated(): boolean {
		return AuthenticationService.isAuthenticated();
	}

	getUserInfo(searchCriteria?: SearchCriteria): Observable<any> {
		if (searchCriteria == null) {
			searchCriteria = new SearchCriteria();
		}

		if (UserService.getConnectedUser() && searchCriteria.ebUserNum == null) {
			searchCriteria.ebUserNum = UserService.getConnectedUser().ebUserNum;
		}
		return this.http.post(Statique.controllerAccount + "/get-user", searchCriteria);
	}

	getUserDetails(searchCriteria?: SearchCriteria): Observable<any> {
		if (searchCriteria == null) {
			searchCriteria = new SearchCriteria();
		}

		return this.http.post(Statique.controllerAccount + "/get-user", searchCriteria);
	}

	updateUserEtablissement(transfertUserObject: TransfertUserObject): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissement + "/transfert-user",
			transfertUserObject
		);
	}

	getUserContacts(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/get-contacts", searchCriteria);
	}

	getCarriers(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/listTransporteur", searchCriteria);
	}

	getUserContactsCompagnie(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/get-contacts-compagnie", searchCriteria);
	}

	getUserContactsNewRequests(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/get-contacts", searchCriteria);
	}

	updateUserContacts(relation: EbRelation): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/update-contact", relation);
	}

	getEtablissementsContact(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerCommunity + "/get-etablissements-contact",
			searchCriteria
		);
	}

	getUserSendingRequest(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/get-sending-request", searchCriteria);
	}

	getUserReceivingRequest(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/get-receiving-request", searchCriteria);
	}

	accepterRelation(ebRelation: EbRelation): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/accepter-relation", ebRelation);
	}

	ignorerRelation(ebRelation: EbRelation): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/ignorer-relation", ebRelation);
	}

	annulerRelation(ebRelation: EbRelation): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/annuler-relation", ebRelation);
	}

	getListUser(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/get-list-user", searchCriteria);
	}
	getListObservers(ebDemande: EbDemande): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/get-list-observers", ebDemande);
	}

	getListUserWithPagination(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerCommunity + "/get-list-user-management",
			searchCriteria
		);
	}

	sendInvitaion(ebRelation: EbRelation): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/send-invitaion", ebRelation);
	}

	sendInvitationMail(inscription: Inscription): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/invitation", inscription);
	}

	sendInvitationByMail(ebRelation: EbRelation): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/send-invitaion-bymail", ebRelation);
	}

	updatePasswordUser(ebUser: EbUser): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/update-password-user", ebUser);
	}

	activationCompte(activationCode: Boolean): Observable<any> {
		return this.http.get(Statique.controllerInscription + "/activation-compte/" + activationCode);
	}

	refuseUser(user: EbUser): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/refuse-user", user);
	}

	getUserContactsByCompagnie(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerCommunity + "/get-one-company-contact",
			searchCriteria
		);
	}

	saveConfigEmail(user: EbUser): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/save-config-params-mail", user);
	}

	updateUserLanguage(user: EbUser): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/updateUserLanguage", user, {
			responseType: "text",
		});
	}
	getByProfile(criteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerUserProfile + "/get-users-by-profile", criteria);
	}

	canEditUser(ebUserNum: number) {
		return this.http.get(Statique.controllerAccount + "/can-edit-user/" + ebUserNum);
	}

	canEditProfil(ebUserProfileNum: number) {
		return this.http.get(Statique.controllerAccount + "/can-edit-profil/" + ebUserProfileNum);
	}
	checkAbilityToDeleteUers(users: Array<EbUser>): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/check-ability-to-delete-users", users);
	}
	deleteSelectedUsers(users: Array<EbUser>): Observable<any> {
		return this.http.post(Statique.controllerCommunity + "/delete-selected-users", users, {
			responseType: "text",
		});
	}
	updateUserTimezone(ebUserNum: number, fuseauHoraire: number) {
		return this.http.put(
			Statique.controllerAccount + "/update-user-timezone",
			{
				ebUserNum,
				fuseauHoraire,
			},
			{ responseType: "text" }
		);
	}
	findUserById(userNum: number): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/find-user-ebUserNum", userNum);
	}

	getToken(ebUserNum: number): Observable<any> {
		return this.http.get(Statique.controllerAccount + "/get-token/" + ebUserNum, {
			responseType: "text",
		});
	}
}
