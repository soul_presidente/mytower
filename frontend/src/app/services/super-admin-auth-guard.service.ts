import { Injectable } from "@angular/core";
import { CanActivate } from "@angular/router";
import { UserService } from "./user.service";
import { EbUser } from "@app/classes/user";
import { UserRole } from "@app/utils/enumeration";

@Injectable()
export class AdminAuthGuardService implements CanActivate {
	user: EbUser;
	userRole = UserRole;

	constructor(private userService: UserService) {}

	canActivate() {
		this.user = UserService.getConnectedUser();
		if (this.user.superAdmin) {
			console.log("yes");
			return true;
		} else {
			console.log("no");
			return false;
		}
	}
}
