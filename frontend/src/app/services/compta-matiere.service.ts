import { Injectable } from "@angular/core";
import EbCptmRegimeTemporaireDTO from "@app/classes/cptm/EbCptmRegimeTemporaireDTO";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { Observable } from "rxjs";
import { Statique } from "@app/utils/statique";
import { HttpClient, HttpParams } from "@angular/common/http";
import { map } from "rxjs/operators";
import { EbCptmProcedureDTO } from "@app/classes/cptm/EbCptmProcedureDTO";
import { EbCptmRegimeSelectionDTO } from "@app/classes/cptm/EbCptmRegimeSelectionDTO";
import { Email } from "@app/classes/email";

@Injectable({
	providedIn: "root",
})
export class ComptaMatiereService {
	constructor(protected http: HttpClient) {}
	// Regime
	listRegime(criteria: SearchCriteria): Observable<Array<EbCptmRegimeTemporaireDTO>> {
		return this.http
			.post(Statique.controllerComptaMatiere + "/list-regime-temporaire-table", criteria)
			.pipe(
				map((res) => {
					return Statique.cloneListObject(res, EbCptmRegimeTemporaireDTO);
				})
			);
	}
	updateRegime(ebRegime: EbCptmRegimeTemporaireDTO): Observable<EbCptmRegimeTemporaireDTO> {
		return this.http
			.post(Statique.controllerComptaMatiere + "/save-regime-temporaire", ebRegime)
			.pipe(map((res) => Statique.cloneObject(res, new EbCptmRegimeTemporaireDTO())));
	}

	deleteRegime(regimeNum: number) {
		let params = new HttpParams().set("regimeNum", String(regimeNum));
		return this.http.delete(Statique.controllerComptaMatiere + "/delete-regime-temporaire", {
			params: params,
		});
	}

	dashboardProcedureInlineUpdate(procedure: EbCptmProcedureDTO): Observable<EbCptmProcedureDTO> {
		return this.http
			.post(Statique.controllerComptaMatiere + "/procedure-dashboard-inline-update", procedure)
			.pipe(map((res) => Statique.cloneObject(res, new EbCptmProcedureDTO())));
	}

	listRegimesAndProcedures(): Observable<Array<EbCptmRegimeSelectionDTO>> {
		return this.http.get(Statique.controllerComptaMatiere + "/list-regimes-procedures").pipe(
			map((res) => {
				return Statique.cloneListObject(res, EbCptmRegimeSelectionDTO);
			})
		);
	}

	generateSemiAutoMailTemplateForProcedure(mailId: number, procedureNum: number): Observable<any> {
		let url = Statique.controllerComptaMatiere + "/mail-generation-procedure";
		url += "?mailId=" + mailId;
		url += "&procedureNum=" + procedureNum;

		return this.http.get(url).pipe(map((res) => Statique.cloneObject(res)));
	}

	sendSemiAutoMail(mailToSend: Email, procedureNum: number): Observable<any> {
		let url = "/send-semi-auto-mail" + "?procedureNum=" + procedureNum;
		return this.http.post(Statique.controllerComptaMatiere + url, mailToSend);
	}
}
