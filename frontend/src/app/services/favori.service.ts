import { HttpClient } from "@angular/common/http";
import { EbFavori } from "@app/classes/favori";
import { Statique } from "@app/utils/statique";
import { Injectable } from "@angular/core";
import { EbUser } from "@app/classes/user";
import { Observable, Subject } from "rxjs";

@Injectable()
export class FavoriService {
	private notifyComponent = new Subject<any>();
	private notifyListingComponent = new Subject<any>();

	favorisObservale = this.notifyComponent.asObservable();
	listingFavorisObservale = this.notifyListingComponent.asObservable();

	public notifyComponentFavori(data: any) {
		this.notifyComponent.next(data);
	}

	public notifyListingComponentFavori(data: any) {
		this.notifyListingComponent.next(data);
	}

	constructor(private http: HttpClient) {}

	addOrRemoveFavori(
		listFavoris: Map<number, EbFavori>,
		idObject: number,
		refObject: string,
		module: number,
		userConnected: EbUser,
		event: HTMLElement
	) {
		if (listFavoris.has(idObject)) {
			let ebFavori = listFavoris.get(idObject);
			this.deleteFavori(ebFavori).subscribe((data) => {
				listFavoris.delete(idObject);
				event.classList.remove("fa-star");
				event.classList.add("fa-star-o");
				ebFavori.toAdd = false;
				ebFavori.toDelete = true;
				this.notifyComponentFavori(ebFavori);
			});
		} else {
			let ebFavori: EbFavori = new EbFavori();
			ebFavori.idObject = idObject;
			ebFavori.refObject = refObject;
			ebFavori.module = module;
			ebFavori.user.ebUserNum = userConnected.ebUserNum;
			this.addFavori(ebFavori).subscribe((data: EbFavori) => {
				listFavoris.set(idObject, data);
				event.classList.remove("fa-star-o");
				event.classList.add("fa-star");
				data.toDelete = false;
				data.toAdd = true;
				this.notifyComponentFavori(data);
			});
		}
	}

	getHtmlFavoris(listFavoris: Map<number, EbFavori>, idObject: number, refObject: string) {
		let style = "font-size: 20px;cursor: pointer;";
		let _class = "fa fa-star-o";
		if (listFavoris.has(idObject)) _class += "fa fa-star";
		return (
			"<i id=" +
			refObject +
			" class='" +
			_class +
			"' style='" +
			style +
			"' onclick='functions.clickFavori(" +
			idObject +
			',"' +
			refObject +
			"\",event.target)'></i>"
		);
	}

	addFavori(ebFavori: EbFavori): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/add-favori", ebFavori);
	}

	deleteFavori(ebFavori: EbFavori): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/delete-favori", ebFavori);
	}

	deleteFavoriById(ebFavoriNum: number): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/delete-favori-by-id", ebFavoriNum);
	}

	getListFavoris(dataSource: string): Observable<any> {
		return this.http.get(dataSource);
	}
}
