import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Statique } from "@app/utils/statique";

@Injectable()
export class EmailHistoriqueService {
	statique = Statique;
	constructor(private http: HttpClient) {}

	/*getListEmailHistorique()  {
    return this.http.get(Statique.controllerEmailHistorique + "/get-all-email")
  }*/

	getListEmailHistorique(incidentNum: number): Observable<any> {
		return this.http.post(Statique.controllerEmailHistorique + "/get-all-email", incidentNum);
	}
}
