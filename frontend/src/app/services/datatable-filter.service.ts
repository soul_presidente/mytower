import { ElementRef, Injectable, ViewChild } from "@angular/core";
import { Statique } from "@app/utils/statique";

@Injectable()
export class DatatableFilterService {
	@ViewChild("panel", { static: false })
	panelEl: ElementRef;
	constructor() {}

	async moveDatatableFilter() {
		let filterEl;
		let tmpEl;
		await Statique.waitForTimemout(100);

		let collapsContainer = this.panelEl.nativeElement;

		let timer = 0;
		while (
			!(filterEl = collapsContainer.getElementsByClassName("dataTables_filter")[0]) &&
			timer < 10000
		) {
			await Statique.waitForTimemout(200);
			timer += 200;
		}

		tmpEl = collapsContainer
			? (tmpEl = collapsContainer.getElementsByClassName("search-datatable__wrapper"))
				? tmpEl[0]
				: null
			: null;
		if (tmpEl && filterEl) {
			Statique.moveDomElement(filterEl, tmpEl);
			filterEl.className += " d-inline-block";
			filterEl = filterEl.getElementsByTagName("label");
			if (filterEl && (filterEl = filterEl[0])) {
				filterEl.childNodes.forEach((it) => {
					if (it.nodeType == 3) it.data = "";
				});
				filterEl = (filterEl = filterEl.getElementsByTagName("input")) ? filterEl[0] : null;
				if (filterEl) {
					filterEl.placeholder = "Search";
				}
			}

			filterEl = (filterEl = collapsContainer.getElementsByClassName("search-datatable__wrapper"))
				? filterEl[0]
				: null;
			tmpEl = collapsContainer
				? (tmpEl = collapsContainer.getElementsByClassName("datatable-filter"))
					? tmpEl[0]
					: null
				: null;
			if (filterEl && tmpEl) {
				Statique.moveDomElement(filterEl, tmpEl);
			}
		}
	}
}
