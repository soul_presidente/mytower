import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { EbCostCenter } from "@app/classes/costCenter";

@Injectable()
export class CostCenterService {
	statique = Statique;
	constructor(private http: HttpClient) {}

	getListCostCenterCompagnie(ebCompagnieNum: number): Observable<any> {
		let urlParams = "?ebCompagnieNum=" + ebCompagnieNum;
		return this.http.get(Statique.controllerCostCenter + "/get-list-cost-center" + urlParams);
	}

	addCostCenterCompagnie(ebCostCenter: EbCostCenter): Observable<any> {
		return this.http.post(Statique.controllerCostCenter + "/add-cost-center", ebCostCenter);
	}

	editCostCenterCompagnie(ebCostCenter: EbCostCenter): Observable<any> {
		return this.http.post(Statique.controllerCostCenter + "/edit-cost-center", ebCostCenter);
	}

	deleteCostCenterCompagnie(ebCostCenter: EbCostCenter): Observable<any> {
		return this.http.post(Statique.controllerCostCenter + "/delete-cost-center", ebCostCenter);
	}
}
