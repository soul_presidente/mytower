import { Injectable } from "@angular/core";
import { EbUserProfile } from "@app/classes/userProfile";
import { Observable } from "rxjs/internal/Observable";
import { Statique } from "@app/utils/statique";
import { HttpClient } from "@angular/common/http";
import { SearchCriteria } from "@app/utils/searchCriteria";

@Injectable()
export class UserProfileService {
	constructor(private http: HttpClient) {}

	updateUserProfile(userProfile: EbUserProfile): Observable<any> {
		return this.http.post(Statique.controllerUserProfile + "/update-user-profile", userProfile);
	}
	getDetailUser(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerUserProfile + "/get-user-profile", searchCriteria);
	}
	checkName(name: string) {
		return this.http.post(Statique.controllerUserProfile + "/nom-exist", name);
	}
	deleteUserProfile(ebUserProfileNum: number) {
		return this.http.post(
			Statique.controllerUserProfile + "/delete-user-profile",
			ebUserProfileNum
		);
	}
	getAll(): Observable<any> {
		return this.http.get(Statique.controllerUserProfile + "/get-all-profile");
	}
}
