import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { HttpClient } from "@angular/common/http";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbEtablissement } from "@app/classes/etablissement";
import { Observable } from "rxjs";
import { EbCostCenter } from "@app/classes/costCenter";

@Injectable()
export class CompagnieService {
	statique = Statique;
	constructor(private http: HttpClient) {}

	getListEtablisementCompagnie(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerCompagnie + "/get-list-etablissement-compagnie",
			searchCriteria
		);
	}

	getCompagnie(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerCompagnie + "/get-compagnie", searchCriteria);
	}

	addEtablissementCompagnie(ebEtablissement: EbEtablissement): Observable<any> {
		return this.http.post(Statique.controllerEtablissement + "/add-etablissement", ebEtablissement);
	}

	editEtablissementCompagnie(ebEtablissement: EbEtablissement): Observable<any> {
		return this.http.post(
			Statique.controllerCompagnie + "/update-etablissement-compagnie",
			ebEtablissement
		);
	}

	getListAddressCompagnie(ebCompagnieNum: number): Observable<any> {
		return this.http.post(Statique.controllerCompagnie + "/get-address-compagnie", ebCompagnieNum);
	}

	getListCompagnieContact(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerCompagnie + "/get-list-contact-compagnie",
			searchCriteria
		);
	}

	getListCategories(ebCompagnieNum: number) {
		return this.http.post(Statique.controllerCompagnie + "/get-list-categorie", ebCompagnieNum);
	}

	getListCompagnie(criteria: SearchCriteria) {
		return this.http.post(Statique.controllerCompagnie + "/get-list-compagnie", criteria);
	}
	getListActionChoose() {
		return this.http.get(Statique.controllerStatiqueListe + "/listActionChoose");
	}
}
