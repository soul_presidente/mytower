import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { DialogService } from "./dialog.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { HttpClient } from "@angular/common/http";
import { EbUser } from "@app/classes/user";
import { BehaviorSubject, Observable, of } from "rxjs";
import { AuthenticationService } from "./authentication.service";
import { FileDownLoadStatus } from "@app/utils/enumeration";

let instance = null;

@Injectable()
export class ExportService {
	statique = Statique;

	constructor(private http: HttpClient, private dialogService: DialogService) {
		if (instance) return instance;
		instance = this;
	}
	private subject = new BehaviorSubject<any>(null);

	startExportCsv(
		module: number,
		showPeride: boolean = false,
		criteria: SearchCriteria = null,
		user: EbUser = null,
		config = null
	) {
		// params à changer selon le besoin
		return new Promise(
			async function(resolve, reject) {
				if (!criteria) criteria = new SearchCriteria();
				criteria.module = module;
				criteria.dtConfig = config;

				let token = Statique.getAuthTokenStr();
				let user = AuthenticationService.getUserFromToken(token);

				criteria.connectedUserNum = user.ebUserNum;
				criteria.username = user.nom + "_" + this.getDateMonthYearMinuteSecondsString();

				// Remove the edit column if present
				if (
					criteria.dtConfig != null &&
					criteria.dtConfig[0] != null &&
					criteria.dtConfig[0].name === "edit"
				) {
					(criteria.dtConfig as any).shift();
				}

				try {
					let result;
					if (showPeride) {
						result = await this.dialogService.periodeInput(
							"Periode of dates",
							"Please choose a periode of dates"
						);
						if (result["dateDeb"])
							criteria.dateDeb = new Date(
								result["dateDeb"].year,
								result["dateDeb"].month,
								result["dateDeb"].day
							);
						if (result["dateFin"])
							criteria.dateFin = new Date(
								result["dateFin"].year,
								result["dateFin"].month,
								result["dateFin"].day
							);
					}
					this.http.post("/api/export/start-export", criteria).subscribe((res) => {
						if (res == FileDownLoadStatus.EN_COURS) {
							localStorage.setItem("exportCode", criteria.username);
							localStorage.setItem("moduleExport", module.toString());
							this.repeatChecking(criteria.username);
						}
					});
				} catch (e) {
					console.error(e);
					reject(e);
				}
			}.bind(this)
		);
	}

	checkIfExportIsOver(code: string) {
		return this.http.post("/api/export/check-export-state", code);
	}

	repeatChecking(code: string) {
		this.checkIfExportIsOver(code).subscribe((res: any) => {
			if (res.state == FileDownLoadStatus.FIN) {
				localStorage.removeItem("exportCode");
				localStorage.removeItem("moduleExport");
				this.exportCsvFile(res.fileUrl);
			} else if (
				res.state == FileDownLoadStatus.ERROR ||
				res.state == FileDownLoadStatus.LIMITE_DEPASSE
			) {
				localStorage.removeItem("exportCode");
				localStorage.removeItem("moduleExport");
				document.dispatchEvent(new CustomEvent("export-state", { detail: res.state }));
			} else if (res.state == FileDownLoadStatus.EN_COURS) {
				this.repeatChecking(code);
			}
		});
	}

	exportCsvFile(url: string) {
		document.dispatchEvent(new CustomEvent("export-state", { detail: FileDownLoadStatus.FIN }));
		//var win = window.open(url, "_blank");
		//win.focus();
		this.getFileData(url);
	}
	getFileData(url) {
		return this.http.get(url, { responseType: "blob" }).subscribe((data) => {
			this.downLoadFile(data);
		});
	}

	downLoadFile(data: any) {
		let url = window.URL.createObjectURL(data);
		let pwa = window.open(url);
		if (!pwa || pwa.closed || typeof pwa.closed == "undefined") {
			alert("Please disable your Pop-up blocker and try again.");
		}
	}

	downloadFile(data, module: number, user: EbUser) {
		//module-nom-d-utilisateur-connecté-JJ-MM-AAA-HH-mm
		const filename =
			(Statique.getModuleNameByModuleNum(module) || "export").toLocaleLowerCase() +
			(user
				? "-" + (user.nom.toLocaleLowerCase() || "export") + (user.prenom.toLocaleLowerCase() || "")
				: "") +
			"-" +
			this.getDateMonthYearMinuteSecondsString() +
			".xlsx";

		if (window.navigator && window.navigator.msSaveBlob) {
			// Internet Explorer
			window.navigator.msSaveBlob(data, filename);
		} else {
			// Other Browsers
			let link = document.createElement("a");
			link.setAttribute("type", "hidden");
			link.href = window.URL.createObjectURL(data);
			document.body.appendChild(link);
			link.download = filename;
			link.click();
			link.remove();
		}
		document.dispatchEvent(new CustomEvent("export-succed", { detail: FileDownLoadStatus.FIN }));
	}

	getDateMonthYearMinuteSecondsString() {
		const dt = new Date();
		return (
			dt.getDate() +
			"-" +
			(dt.getMonth() + 1) +
			"-" +
			dt.getFullYear() +
			"-" +
			dt.getHours() +
			"-" +
			dt.getMinutes() +
			"-" +
			dt.getSeconds()
		);
	}

	sendMessage(data: any, searchterm?: string, cb: Function = null) {
		this.subject.next({ data: data, searchterm, cb: cb });
	}

	clearMessage() {
		this.subject.next(null);
	}

	getMessage(): Observable<any> {
		return this.subject.asObservable();
	}

	extractDataAnalysis(type: number): Observable<any> {
		return this.http.get(Statique.controllerExport + "/extract-data-analysis?type=" + type, {
			responseType: "blob",
		});
	}
}
