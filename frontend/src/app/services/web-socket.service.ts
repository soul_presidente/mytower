import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { ResultEntity } from "@app/classes/ResultEntity";
import { ResultEntityType } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { EbUser } from "@app/classes/user";

@Injectable()
export class WebSocketService {
	// Tip for dev: Each time you need a socket for a different topic, you should add a new subject here
	private pricingSubject = new Subject<any>();
	pricingSubjectSubscriber = this.pricingSubject.asObservable();

	private stompClient: any;
	private stompUser: EbUser;
	private sessionId: any;

	constructor() {}

	public notifySubscribers(uri: string, brutData?: any) {
		if (brutData) {
			let data = brutData as ResultEntity;
			if (data && uri == Statique.controllerWebSocketPricing) {
				if (data.type.code == ResultEntityType.SUCCESS) {
					if (uri == Statique.controllerWebSocketPricing) this.pricingSubject.next(data.value);
				}
			}
		}
	}

	public setStompClient(sc: any) {
		this.stompClient = sc;
	}
	public getStompClient(): any {
		return this.stompClient;
	}
	public setSessionId(sid: any) {
		this.sessionId = sid;
	}

	public getSessionId(): string {
		return this.sessionId;
	}

	public async sendPricingMessage(ebDemandeNum: number, lock: boolean = false, free: boolean = false) {

		if (!(this.stompClient && this.sessionId)) {

			await Statique.waitForAvailability(
				function(millis: number = null) {
					if (this.stompClient && this.sessionId) return true;
					return false;
				}.bind(this)
			);
		} 

		if (this.stompClient && this.sessionId) {

			 this.stompClient.send(
				Statique.controllerWebSocketPricing,
				{},
				JSON.stringify({
					data: ebDemandeNum,
					lock: lock,
					sessionId: this.sessionId,
					free: free
				})
			);
		}
	} 
} 
