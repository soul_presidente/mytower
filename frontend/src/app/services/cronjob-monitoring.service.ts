import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable()
export class CronjobMonitoringService {
	Statique = Statique;

	constructor(private http: HttpClient) {}

	getInfosLastExecutionCronjob(): Observable<any> {
		return this.http.get(Statique.controllerCronjob + "/tracing-infos-last-execution");
	}

	changeStatutCronjob(status: Boolean): Observable<any> {
		return this.http.get(
			Statique.controllerCronjob + "/tracing-change-statut-cronjob?status=" + status
		);
	}
	executeGenerationOfEvents(): Observable<any> {
		return this.http.get(
			Statique.controllerCronjob + "/tracing" 
		);
	}
}
