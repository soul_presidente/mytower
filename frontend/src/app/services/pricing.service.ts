import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { SearchCriteria, SearchCriteriaLite } from "@app/utils/searchCriteria";
import {HttpClient, HttpParams} from "@angular/common/http";
import { EbDemande } from "@app/classes/demande";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Statique } from "@app/utils/statique";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { EbCustomsInformation } from "@app/classes/customsInformation";
import { EbQrGroupe } from "@app/classes/qrGroupe";
import { EbQrGroupeProposition } from "@app/classes/qrGroupeProposition";
import { FichierJoint } from "@app/classes/fichiersJoint";
import { map } from "rxjs/operators";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import {Cost, CostCategorie} from "@app/classes/costCategorie";
import { NatureDemandeTransport } from "@app/utils/enumeration";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { EbMarchandise } from "@app/classes/marchandise";
import {EbAdditionalCost} from "@app/classes/EbAdditionalCost";

@Injectable()
export class PricingService {
	Statique = Statique;
	constructor(private http: HttpClient) {}

	getHtmlEbDemande(ebDemandeNum: number) {
		let style = "top: 6px;cursor: pointer;",
			_type = "checkbox";
		return (
			"<input class='checkbox-Track fa' [ngbTooltip]='tipContent'  id=" +
			ebDemandeNum +
			"  type='" +
			_type +
			"' style='" +
			style +
			"'onchange='functions.clickCheckbox(" +
			ebDemandeNum +
			", event.target)'>"
		);
	}

	deleteEbDemande(listEbDemandeNum: number[]): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/deleteDemande", listEbDemandeNum);
	}

	listAdditionalCostsByEbCompagnieNum(ebCompagnieNum: number): Observable<any> {
		return this.http.post(
			Statique.controllerAdditionalCosts + "/list-additionalCostsByebcompagnieNum",
			ebCompagnieNum
		);
	}

	ajouterEbDemande(ebDemande: EbDemande, module: number): Observable<any> {
		ebDemande.module = module;
		return this.http.post(Statique.controllerPricing + "/add", ebDemande);
	}

	confirmPendingDemande(ebDemande: EbDemande): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/confirm-pending-demande", ebDemande);
	}

	saveAllInformations(ebDemande: EbDemande): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/save-all-informations", ebDemande);
	}

	listPays(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/countries");
	}

	listBroker(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/listBroker", searchCriteria);
	}

	listCostCenter(ebEtablissementNum): Observable<any> {
		let params = "?ebEtablissementNum=" + ebEtablissementNum;

		return this.http.get(Statique.controllerPricing + "/listCostCenters" + params);
	}

	listCategorie(ebEtablissementNum): Observable<any> {
		let params = "?ebEtablissementNum=" + ebEtablissementNum;
		return this.http.get(Statique.controllerPricing + "/listCategories" + params);
	}

	listTransporteurControlTower(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerPricing + "/listTransporteurControlTower",
			searchCriteria
		);
	}

	listTransporteur(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/listTransporteur", searchCriteria);
	}

	getEbDemande(
		searchCriteriaPricingBooking: SearchCriteriaPricingBooking,
		isPublic?: Boolean
	): Observable<any> {
		return this.http.post(
			(isPublic ? Statique.controllerPublic : Statique.controllerPricing) + "/demande",
			searchCriteriaPricingBooking
		);
	}

	getListEbDemandeQuote(
		searchCriteriaPricingBooking: SearchCriteriaPricingBooking
	): Observable<any> {
		return this.http.post(
			Statique.controllerPricing + "/listDemandeQuote",
			searchCriteriaPricingBooking
		);
	}

	ajouterQuotation(exEbDemandeTransporteur: ExEbDemandeTransporteur, module?: number, isQuotationRequest?: boolean): Observable<any> {
		let params = {};
		params["module"] = module;
		params["isQuotationRequest"] = isQuotationRequest;
		return this.http.post(Statique.controllerPricing + "/addQuotation", exEbDemandeTransporteur, {params: params});
	}
	editQuotation(exEbDemandeTransporteur: ExEbDemandeTransporteur, module): Observable<any> {
		return this.http.post(
			Statique.controllerPricing + "/editQuotation?module=" + module,
			exEbDemandeTransporteur
		);
	}
	updateStatusQuotation(exEbDemandeTransporteur: ExEbDemandeTransporteur): Observable<any> {
		return this.http.post(
			Statique.controllerPricing + "/updateStatusQuotation",
			exEbDemandeTransporteur
		);
	}

	getListDocumentsByTypeDemande(ecModule, demandeNum, ebUserNum): Observable<any> {
		return this.http.get(
			Statique.controllerStatiqueListe +
				"/listDocuments?module=" +
				ecModule +
				"&demande=" +
				demandeNum +
				"&ebUserNum=" +
				ebUserNum
		);
	}

	getListDemandeInitials(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/listInitialDemande", searchCriteria);
	}

	getListDemande(): Observable<any> {
		return this.http.get(Statique.controllerPricing + "/list");
	}

	deleteDocument(doc: FichierJoint): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/deleteDocument", doc);
	}

	confirmPickup(ebDemande: EbDemande): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/confirmPickup", ebDemande);
	}

	cancelQuotation(ebDemandeNum: number): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/cancelQuotation", ebDemandeNum);
	}

	updateEbDemandeTransportInfos(ebDemande: Object, action: string): Observable<any> {
		return this.http.post(
			Statique.controllerPricing + "/updateEbDemandeTransportInfos?action=" + action,
			ebDemande
		);
	}
	updateEbDemandePropRdv(ebDemande): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/updatePropRdv", ebDemande);
	}

	listAllCostCategorieByTranspMode(modeTransport: number): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/listPostCostCategorie", modeTransport);
	}
	listAllCostCategie(): Observable<any> {
		return this.http.get(Statique.controllerPricing + "/listPostCostCategorie");
	}

	getEbParties(): Observable<any> {
		return this.http.get(Statique.controllerPricing + "/parties");
	}

	getTaskProgress(ident: string): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/getTaskProgress", ident);
	}

	deleteQuotation(ebDemandeTransporteurNum: number): Observable<any> {
		return this.http.post(
			Statique.controllerPricing + "/deleteQuotation",
			ebDemandeTransporteurNum
		);
	}

	updateGoodsPickup(map: Object[]): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/updateGoodsPickup", map);
	}

	updateEbDemande(ebDemande: any): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/updateEbDemande", ebDemande);
	}

	getNextEbDemandeNum(): Observable<any> {
		return this.http.get(Statique.controllerPricing + "/getNextEbDemandeNum");
	}

	updateCustomsInformation(customsInformation: EbCustomsInformation): Observable<any> {
		return this.http.post(
			Statique.controllerPricing + "/updateCustomsInformation",
			customsInformation
		);
	}

	getCustomsInformation(ebDemandeNum: number): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/getCustomsInformation", ebDemandeNum);
	}

	requestCustomsBroker(ebDemandeNum: number): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/requestCustomsBroker", ebDemandeNum);
	}

	searchAdresseAsParty(
		term: string,
		ebCompagnieNum: number,
		ebEtablissementNum: number,
		eligibilities: string,
		ebUserNum?: number,
		ecCountryNum?: number,
		city?: String,
		zipCode?: String,
		ebZoneNum?: number,
		size?: number,
		page?: number
	): Observable<any> {
		let params = new FormData();
		params.append("term", term);
		if (ebCompagnieNum != null) params.append("ebCompagnieNum", String(ebCompagnieNum));
		if (ebEtablissementNum != null) params.append("ebEtablissementNum", String(ebEtablissementNum));
		if (eligibilities != null) params.append("eligibilities", eligibilities);
		if (ebUserNum != null) params.append("ebUserNum", String(ebUserNum));
		if (ecCountryNum != null) params.append("ecCountryNum", String(ecCountryNum));
		if (city != null) params.append("city", String(city));
		if (size != null) params.append("size", String(size));
		if (page != null) params.append("page", String(page));

		return this.http.post(Statique.controllerPricing + "/adresseAsParty", params);
	}
	bulkImport(ident: string): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/bulk-import", ident);
	}

	getListDemandeTransport(criteria: SearchCriteriaPricingBooking): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/list-demande-transport", criteria);
	}

	// prettier-ignore
	saveFlags(ebDemandeNum: number, newFlags: string): Observable<any> {
    return this.http.get(
      Statique.controllerPricing +
        "/saveFlags?ebDemandeNum=" + ebDemandeNum +
        "&newFlags=" + newFlags
    );
  }

	listSchemaPsl(): Observable<any> {
		return this.http.get(Statique.controllerTrackTrace + "/get-All");
	}

	getListMarchandise(ebDemandeNum: number): Observable<any> {
		return this.http.post(Statique.controllerUnit + "/list-marchandise-demande", ebDemandeNum);
	}

	acknowledgeTdc(ebDemandeNum: number): Observable<any> {
		return this.http.get(Statique.controllerPricing + "/acknowledgeTdc", {
			params: {
				ebDemandeNum: String(ebDemandeNum),
			},
		});
	}

	confirmeAskFortrResponsibility(
		ebDemandeNum: number,
		moduleNum: number,
		refTransport: string,
		action: string
	): Observable<any> {
		return this.http.get(
			Statique.controllerPricing +
				"/confirmeAskFtrRes?ebDemandeNum=" +
				ebDemandeNum +
				"&moduleNum=" +
				moduleNum +
				"&refTransport=" +
				refTransport +
				"&action=" +
				action
		);
	}
	confirmeProvideTransport(
		ebDemandeNum: number,
		moduleNum: number,
		refTransport: string,
		action: string
	): Observable<any> {
		return this.http.get(
			Statique.controllerPricing +
				"/confirmeProvideTransport?ebDemandeNum=" +
				ebDemandeNum +
				"&moduleNum=" +
				moduleNum +
				"&refTransport=" +
				refTransport +
				"&action=" +
				action
		);
	}

	updateTransportPlan(
		ebDemandeNum: number,
		ebPartyNum: number,
		oldZoneLabel: string,
		ebZoneNum: number,
		refPlan: string,
		calculatedPrice: number
	) {
		let params = new FormData();
		params.append("ebDemandeNum", String(ebDemandeNum));
		params.append("ebPartyNum", String(ebPartyNum));
		params.append("oldZoneLabel", oldZoneLabel);
		params.append("zone", String(ebZoneNum));
		params.append("refPlan", refPlan);
		params.append("calculatedPrice", String(calculatedPrice));

		return this.http.post(Statique.controllerPricing + "/update-transpot-plan", params);
	}
	getFieldsByCompagnieNumAndTerm(ebCompagnieNum: number, term: String): Observable<any> {
		return this.http.get(
			Statique.controllerPricing +
				"/fields-by-term?term=" +
				term +
				"&ebCompagnieNum=" +
				ebCompagnieNum
		);
	}
	valorisationMTC(ebDemande: EbDemande) {
		return this.http.post(Statique.controllerPricing + "/valorisation-unitaire", ebDemande);
	}

	updateAdditionalCost(ebDemandeNum: number, listAdditionalCost: Array<Cost>): Observable<any> {
		let params = {};
		if (ebDemandeNum != null) params["ebDemandeNum"] = ebDemandeNum;

		return this.http.post(
			Statique.controllerPricing + "/save-additional-cost",
			listAdditionalCost,
			{ params: params }
		);
	}

	getListIncotermCity(ebCompagnieNum: number): Observable<any> {
		return this.http.post(Statique.controllerPricing + "/listIncotermCity", ebCompagnieNum);
	}

	computeQuoteTotalPrice(isForGrouping: Boolean, demande: EbDemande) {
		if (isForGrouping || NatureDemandeTransport.CONSOLIDATION == demande.xEcNature) {
			demande.consolidatedPrice = 0;
			demande.exEbDemandeTransporteurs.forEach((it) => {
				if (it.price) demande.consolidatedPrice += it.price;
			});
		}
	}

	exchangeRate(
		quote: ExEbDemandeTransporteur,
		currencyCibleNum,
		currencyQuoteNum,
		listExchangeRate: Array<EbCompagnieCurrency>
	) {
		if (listExchangeRate && listExchangeRate.length > 0) {
			let currencyComp = listExchangeRate.find(
				(item) =>
					item.xecCurrencyCible &&
					item.ecCurrency &&
					item.ecCurrency.ecCurrencyNum == currencyQuoteNum &&
					item.xecCurrencyCible.ecCurrencyNum == currencyCibleNum
			);
			if (currencyComp != null && currencyComp.euroExchangeRate != null) {
				return quote.price * currencyComp.euroExchangeRate;
			}
		}
		return quote.price;
	}

	calculSaving(demande: EbDemande) {
		if (demande.totalPrice > 0 && demande.consolidatedPrice > 0) {
			let prix = +(demande.totalPrice - demande.consolidatedPrice).toFixed(2);
			let gain = Math.floor((prix / demande.totalPrice) * 100);
			if (demande.xecCurrencyInvoice.code) {
				demande.savingAsString = prix + " " + demande.xecCurrencyInvoice.code + " (" + gain + "%)";
			} else {
				demande.savingAsString = prix + " (" + gain + "%)";
			}
			demande.saving = demande.totalPrice - demande.consolidatedPrice;
		}
	}

	async CalculTotalPrice(
		demande: EbDemande,
		isForGrouping: Boolean,
		listExchangeRate: Array<EbCompagnieCurrency>
	) {
		demande.totalPrice = 0;
		if (!demande.ebQrGroupe || !demande.ebQrGroupe.listPropositions) return;

		let listSelectedDemande = demande.ebQrGroupe.listPropositions[0].listEbDemande.filter(
			(d) => d.isChecked
		);

		this.computeQuoteTotalPrice(isForGrouping, demande);
		if (listSelectedDemande && listSelectedDemande.length > 0) {
			listSelectedDemande.forEach((d) => {
				if (d.exEbDemandeTransporteurs && d.exEbDemandeTransporteurs.length > 0)
					if (d.xecCurrencyInvoice == demande.xecCurrencyInvoice) {
						demande.totalPrice = demande.totalPrice + d.exEbDemandeTransporteurs[0].price;
					} else {
						demande.totalPrice =
							demande.totalPrice +
							this.exchangeRate(
								d.exEbDemandeTransporteurs[0],
								demande.xecCurrencyInvoice.ecCurrencyNum,
								d.xecCurrencyInvoice.ecCurrencyNum,
								listExchangeRate
							);
					}
			});
			demande.totalPrice = Number(demande.totalPrice.toFixed(2));
		}

		this.calculSaving(demande);
	}

	updateListMarchandise(listEbMarchandise: Array<EbMarchandise>, params: any): Observable<any> {
		return this.http.post(
			Statique.controllerPricing + "/update-list-marchandise",
			listEbMarchandise,
			{
				params: params,
			}
		);
	}
}
