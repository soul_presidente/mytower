import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";
import {Cost, CostCategorie} from "@app/classes/costCategorie";
import {Statique} from "@app/utils/statique";
import {SearchCriteria} from "@app/utils/searchCriteria";

@Injectable({
  providedIn: 'root'
})
export class CostItemService {

	constructor(private http: HttpClient) {}

	listCostCategorieByCompagnie(comagnieNum: number,activedOnly?: boolean): Observable<any>{
		return this.http.post(
			Statique.controllerCostItems + "/list-cost-categorie?activedOnly=" + activedOnly,
			comagnieNum
		);
	}

	addCostItem(cost: Cost): Observable<any> {
		return this.http.post(Statique.controllerCostItems + "/add-cost-item", cost);
	}

	updateCostItem(newEbcost: Cost): Observable<any> {
		return this.http.put(Statique.controllerCostItems +"/update-cost-item", newEbcost);
	}

	addCostCategorie(costCategorie: CostCategorie): Observable<any> {
		return this.http.post(Statique.controllerCostItems + "/add-cost-categorie", costCategorie);
	}

	deleteCostItem(ebCostNum: number) {
		let params = new HttpParams().set("ebCostNum", String(ebCostNum));
		return this.http.delete(
			Statique.controllerCostItems + "/delete-cost-item", {
				params: params,
			}
		);
	}

	listCostItems(criteria:SearchCriteria): Observable<any>{
		return this.http.post(Statique.controllerCostItems + "/list-costs-items",criteria);
	}

	verifyEbCostCodeExistance(code: string, ebCompagnieNum: number): Observable<any> {
		let params = new FormData();
		params.append("code", code);
		params.append("ebCompagnieNum", String(ebCompagnieNum));

		return this.http.post(Statique.controllerCostItems + "/verify-code-existance", params);
	}
}
