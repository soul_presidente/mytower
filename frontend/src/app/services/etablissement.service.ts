import { EbCategorie } from "@app/classes/categorie";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { EbAdresse } from "@app/classes/adresse";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbCostCenter } from "@app/classes/costCenter";
import { HttpClient } from "@angular/common/http";
import { EbLabel } from "@app/classes/label";
import { Statique } from "@app/utils/statique";
import CustomField from "@app/classes/customField";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { EbEtablissement } from "@app/classes/etablissement";
import { map } from "rxjs/operators";
import { EbTypeFlux } from "@app/classes/EbTypeFlux";
import { TransfertUserObject } from "@app/classes/transfertUserobject";
import { SearchCriteriaOrder } from "@app/utils/searchCriteriaOrder";

@Injectable()
export class EtablissementService {
	statique = Statique;
	constructor(private http: HttpClient) {}

	getListEtablissementCurrencys(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissementCurrency + "/get-list-exEtablissementCurrencys",
			searchCriteria
		);
	}

	getListEtablissementCurrency(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissementCurrency + "/get-list-exEtablissementCurrency",
			searchCriteria
		);
	}

	getEtablisement(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerEtablissement + "/get-etablisement", searchCriteria);
	}

	getListEtablisement(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissement + "/get-list-etablisement",
			searchCriteria
		);
	}
	getListEtablisementPromise(searchCriteria: SearchCriteria): Promise<Array<EbEtablissement>> {
		return new Promise((resolve) => {
			this.getListEtablisement(searchCriteria).subscribe((res) => resolve(res));
		});
	}

	getListEtablisementCt(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissement + "/getlist-etablisement-ct",
			searchCriteria
		);
	}

	getListAdresseEtablisement(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissement + "/get-list-adresse-etablisement",
			searchCriteria
		);
	}

	addAdresseEtablisement(ebAdresse: EbAdresse): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissement + "/add-adresse-etablisement",
			ebAdresse
		);
	}

	editAdresseEtablisement(ebAdresse: EbAdresse): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissement + "/edit-adresse-etablisement",
			ebAdresse
		);
	}

	deleteAdresseEtablisement(ebAdresse: EbAdresse): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissement + "/delete-adresse-etablisement",
			ebAdresse
		);
	}

	// -------------------------------------------------------------- //

	getListCostCenterEtablisement(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissement + "/get-list-cost-center-etablisement",
			searchCriteria
		);
	}

	addCostCenterEtablisement(ebCostCenter: EbCostCenter): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissement + "/add-cost-center-etablisement",
			ebCostCenter
		);
	}

	editCostCenterEtablisement(ebCostCenter: EbCostCenter): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissement + "/edit-cost-center-etablisement",
			ebCostCenter
		);
	}

	deleteCostCenterEtablisement(ebCostCenter: EbCostCenter): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissement + "/delete-cost-center-etablisement",
			ebCostCenter
		);
	}

	// -------------------------------------------------------------- //

	getAllCategories(criteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerCategorie + "/get-all", criteria);
	}

	getCategorieLabels(criteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerCategorie + "/label/get-all", criteria);
	}

	addCategorie(ebCategorie: EbCategorie): Observable<any> {
		return this.http.post(Statique.controllerCategorie + "/add", ebCategorie);
	}

	deleteCategorie(ebCategorie: EbCategorie): Observable<any> {
		return this.http.post(Statique.controllerCategorie + "/delete", ebCategorie);
	}

	addLabel(ebLabel: EbLabel): Observable<any> {
		return this.http.post(Statique.controllerCategorie + "/label/add", ebLabel);
	}

	deleteLabel(ebLabel: EbLabel): Observable<any> {
		return this.http.post(Statique.controllerCategorie + "/label/delete", ebLabel);
	}

	// -------------------------------------------------------------- //

	saveCustomField(customField: CustomField): Observable<any> {
		return this.http.post(Statique.controllerEtablissement + "/add-customfield", customField);
	}

	getCustomFields(ebCompagnieNum: number): Observable<any> {
		return this.http.post(Statique.controllerEtablissement + "/get-customfield", {
			ebCompagnieNum,
		});
	}

	// -------------------------------------------------------------- //

	saveExEtablissementCurrency(exEtablissementCurrency: EbCompagnieCurrency): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissementCurrency + "/save-exEtablissementCurrency",
			exEtablissementCurrency
		);
	}


	updateExEtablissementCurrency(exEtablissementCurrency: EbCompagnieCurrency): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissementCurrency + "/update-exEtablissementCurrency",
			exEtablissementCurrency
		);
	}

	deleteExEtablissementCurrency(exEtablissementCurrency: EbCompagnieCurrency): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissementCurrency + "/delete-exEtablissementCurrency",
			exEtablissementCurrency
		);
	}

	getListExEtablissementCurrency(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissementCurrency + "/get-etablissementCurrencies",
			searchCriteria
		);
	}

	// TypeFlux
	listTypeFlux(criteria: SearchCriteria): Observable<Array<EbTypeFlux>> {
		return this.http.post(Statique.controllerEtablissement + "/list-type-flux", criteria).pipe(
			map((res) => {
				return Statique.cloneListObject(res, EbTypeFlux);
			})
		);
	}

	getExEtablissementCurrencyActive(ebEtablissementNum: number): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissement + "/get-exEtablissementCurrencyActive",
			ebEtablissementNum
		);
	}

	getDataPreference(criteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerEtablissement + "/get-data-preference", criteria);
	}

	getListExEtablissementCategories(ebEtablissementNum: number): Observable<any> {
		return this.http.post(Statique.controllerEtablissement + "/get-listCategories", {
			ebEtablissementNum,
		});
	}

	getListExEtablissementCurrencyforQuotation(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissementCurrency + "/get-list-exEtablissementCurrency-forquotation",
			searchCriteria
		);
	}

	getListCurrencyforExEtablissementwithStatiqueCurrency(
		searchCriteria: SearchCriteria
	): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissementCurrency + "/get-list-exEtablissementCurrency-forquotation",
			searchCriteria
		);
	}

	getListCurrencyForDemande(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissementCurrency + "/get-list-CurrencyForDemande",
			searchCriteria
		);
	}
	getCategoryCustom(criteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerEtablissement + "/get-category-custom", criteria);
	}

	transfertUserEtablissement(transfertUserObjet: TransfertUserObject): Observable<any> {
		return this.http.post(Statique.controllerEtablissement + "/transfert-user", transfertUserObjet);
	}

	getListCompagnieCurrencysBySetting(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissementCurrency + "/get-list-compagnie-currencys-by-setting",
			searchCriteria
		);
	}

	getCompagnieCurrency(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerEtablissementCurrency + "/get-company-currency",
			searchCriteria
		);
	}
}
