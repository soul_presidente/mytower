import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { HttpClient, HttpParams } from "@angular/common/http";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";

@Injectable()
export class ConfigTypeFluxService {
	Statique = Statique;
	constructor(private http: HttpClient) {}

	getListEbTtSchemaPSl(criteria: SearchCriteria): Observable<Array<EbTtSchemaPsl>> {
		return this.http.post(Statique.controllerCompagniePsl + "/list-schema-psl", criteria).pipe(
			map((res: any) => {
				return Statique.cloneListObject(res.data, EbTtSchemaPsl);
			})
		);
	}

	getListTypeFlux(searchCriteria: any): Observable<any> {
		return this.http.post(Statique.controllerTypeFlux + "/list-type-flux", searchCriteria);
	}

	updateTypeFlux(searchCriteria: any): Observable<any> {
		return this.http.post(Statique.controllerTypeFlux + "/update-type-flux", searchCriteria);
	}

	deleteTypeFlux(id: number): Observable<any> {
		return this.http.post(Statique.controllerTypeFlux + "/delete-type-flux", id);
	}
}
