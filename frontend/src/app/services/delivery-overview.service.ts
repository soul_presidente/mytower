import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { Statique } from "@app/utils/statique";
import { Order } from "@app/classes/ebDelOrder";
import { Delivery } from "@app/classes/ebDelLivraison";
import { SearchCriteriaDelivery } from "@app/utils/SearchCritereaDelivery";
import { map } from "rxjs/operators";
import { DeliveryLine } from "@app/classes/ebDelLivraisonLine";
import { OrderLine } from "@app/classes/ebDelOrderLine";
import { EbOrderPlanificationDTO } from "@app/classes/EbOrderPlanificationDTO";
import { EbDemande } from "@app/classes/demande";
import { SearchCriteriaOrder } from "@app/utils/searchCriteriaOrder";
import { EbOrderLinePlanificationDTO } from "@app/classes/EbOrderLinePlanificationDTO";

@Injectable({
	providedIn: "root",
})
export class DeliveryOverviewService {
	constructor(private httpClient: HttpClient) {}

	createOrder(order: Order): Observable<any> {
		let headers = new HttpHeaders({ "Content-Type": "application/json" });
		return this.httpClient.post(Statique.controllerOrderOverView + "/create-order", order, {
			headers,
		});
	}

	saveOrder(order: Order): Observable<any> {
		let headers = new HttpHeaders({ "Content-Type": "application/json" });
		return this.httpClient.put(Statique.controllerOrderOverView + "/save-order", order, {
			headers,
		});
	}

	getOrders(criteria: SearchCriteriaDelivery): Observable<any> {
		let headers = new HttpHeaders({ "Content-Type": "application/json" });
		return this.httpClient.post(Statique.controllerOrderOverView + "/orders", criteria, {
			headers,
		});
	}

	getOrder(criteria: SearchCriteriaOrder): Observable<Order> {
		return this.httpClient.post<Order>(Statique.controllerOrderOverView + "/get-order", criteria);
	}

	getDelivery(criteria: SearchCriteriaDelivery): Observable<Delivery> {
		return this.httpClient.post<Delivery>(
			Statique.controllerDeliveryOverView + "/get-delivery",
			criteria
		);
	}

	saveDelivery(delivery: Delivery): Observable<any> {
		let headers = new HttpHeaders({ "Content-Type": "application/json" });
		return this.httpClient.post(Statique.controllerDeliveryOverView + "/save-delivery", delivery, {
			headers,
		});
	}

	confirmDelivery(ebDelLivraisonNum: number) {
		return this.httpClient.post(
			Statique.controllerDeliveryOverView + "/confirm-delivery",
			ebDelLivraisonNum
		);
	}

	getListDelivery(criteria: SearchCriteriaDelivery) {
		return this.httpClient.post(Statique.controllerDeliveryOverView + "/list", criteria);
	}

	updateDelivery(delivery: Delivery): Observable<Delivery> {
		return this.httpClient.post<Delivery>(
			Statique.controllerDeliveryOverView + "/update-delivery",
			delivery
		);
	}

	dashboardDeliveryInlineUpdate(deliveryLine: DeliveryLine): Observable<DeliveryLine> {
		return this.httpClient
			.post(Statique.controllerDeliveryOverView + "/delivery-dashboard-inline-update", deliveryLine)
			.pipe(map((res) => Statique.cloneObject(res, new DeliveryLine())));
	}

	getOrderLinesPlanifiable(ebDelOrderNum: number): Observable<Array<OrderLine>> {
		const params = new HttpParams().set("ebDelOrderNum", String(ebDelOrderNum));
		return this.httpClient.get<Array<OrderLine>>(
			Statique.controllerOrderOverView + "/get-order-lines-planifiable",
			{
				params: params,
			}
		);
	}
	savePlanifyOrder(ebOrderPlanification: EbOrderPlanificationDTO): Observable<Delivery> {
		return this.httpClient.post<Delivery>(
			Statique.controllerOrderOverView + "/planify-order",
			ebOrderPlanification
		);
	}
	consolidateDeliveriesToDemande(listDeliveryForConsolidate: Array<number>): Observable<EbDemande> {
		return this.httpClient.post<EbDemande>(
			Statique.controllerDeliveryOverView + "/pre-consolidate-delivery",
			listDeliveryForConsolidate
		);
	}
	saveFlags(ebDelOrderNum: number, newFlags: string): Observable<any> {
		return this.httpClient.get(
			Statique.controllerOrderOverView +
				"/saveFlags?ebDelOrderNum=" +
				ebDelOrderNum +
				"&newFlags=" +
				newFlags
		);
	}

	deleteDelivery(listSelectedDelivery: Array<number>): Observable<any> {
		return this.httpClient.post(
			Statique.controllerDeliveryOverView + "/delete-delivery",
			listSelectedDelivery
		);
	}

	deleteOneDelivery(id: Number): Observable<any> {
		return this.httpClient.post(Statique.controllerDeliveryOverView + "/delete-one-delivery", id);
	}

	getOrderLinesPlanifiableTableau(criteria: SearchCriteriaOrder): Observable<Array<OrderLine>> {
		return this.httpClient.post<Array<OrderLine>>(
			Statique.controllerOrderOverView + "/get-order-lines-planifiable-tableau",
			criteria
		);
	}
	getDeliveryLineByDelivery(ebDelLivraisonNum: number): Observable<Array<DeliveryLine>> {
		return this.httpClient.post<Array<DeliveryLine>>(
			Statique.controllerDeliveryOverView + "/get-delivery-lines",
			ebDelLivraisonNum
		);
	}

	getOrderLineByDeliveryLine(ebDelLivraisonLineNum: number): Observable<OrderLine> {
		return this.httpClient.post<OrderLine>(
			Statique.controllerDeliveryOverView + "/find-orderLine-by-ebLivraisonLineNum",
			ebDelLivraisonLineNum
		);
	}

	deleteDeliveryLineById(ebDelLivraisonLineNum: number): Observable<DeliveryLine> {
		return this.httpClient.post<DeliveryLine>(
			Statique.controllerDeliveryOverView + "/delete-one-delivery-line",
			ebDelLivraisonLineNum
		);
	}

	getDeliveryLinesForGenericTable(criteria: SearchCriteriaDelivery) {
		return this.httpClient.post(
			Statique.controllerDeliveryOverView + "/get-delivery-lines-for-genericTable",
			criteria
		);
	}

	cancelDelivery(ebDelLivraisonNum: number): Observable<Delivery> {
		return this.httpClient.post<Delivery>(
			Statique.controllerDeliveryOverView + "/cancel-delivery",
			ebDelLivraisonNum
		);
	}

	getListDocumentsDelivery(ecModule, ebDelLivraisonNum, ebUserNum): Observable<any> {
		return this.httpClient.get(
			Statique.controllerStatiqueListe +
				"/listDocumentsDelivery?module=" +
				ecModule +
				"&ebDelLivraisonNum=" +
				ebDelLivraisonNum +
				"&ebUserNum=" +
				ebUserNum
		);
	}

	getOrderLines(ebDelOrderNum: number, searchterm: string = null) {
		return this.httpClient.get(
			Statique.controllerOrderOverView + "/list-order-lines?ebDelOrderNum="
			+ebDelOrderNum+"&searchterm="+searchterm
		);
	}
}
