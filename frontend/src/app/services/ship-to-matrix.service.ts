import { Observable } from "rxjs";
import { SearchCriteriaShipToMatrix } from "./../utils/SearchCriteriaShipToMatrix";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable()
export class ShipToMatrixService {
	url: string = "/api/shiptomatrix";

	constructor(private httpClient: HttpClient) {}

	getListShipToMatrix(criteria: SearchCriteriaShipToMatrix): Observable<any> {
		let headers = new HttpHeaders({ "Content-Type": "application/json" });
		return this.httpClient.post(`${this.url}/list-shipto`, criteria, { headers });
	}
}
