import { Injectable } from "@angular/core";
import { SavedForm } from "@app/classes/savedForm";
import { Statique } from "@app/utils/statique";
import { SavedFormIdentifier } from "@app/utils/enumeration";
import { EbUser } from "@app/classes/user";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable, Subject } from "rxjs";

/**
 * SavedFormService here is a singleton shared between components
 */

let instance = null;

@Injectable()
export class SavedFormsService {
	statique = Statique;
	private savedFormSource: BehaviorSubject<SavedForm> = new BehaviorSubject(null);
	selectedSavedForm$: Observable<SavedForm> = this.savedFormSource.asObservable();

	private notifyComponent = new Subject<any>();
	SavedFormsObservale = this.notifyComponent.asObservable();

	constructor(private authHttp: HttpClient) {
		if (!instance) instance = this;

		return instance;
	}

	public notifyComponentSavedForms(data: any) {
		this.notifyComponent.next(data);
	}
	getSavedForms(sf): Observable<any> {
		return this.authHttp.post(Statique.controllerSavedForm + "/getByComponentAndUser", {
			ebCompNum: sf.ebCompNum,
			ebUserNum: sf.ebUserNum,
		});
	}

	saveSavedForm(sf): Observable<any> {
		if (!sf.ebUser) throw new Error("User is undefined");

		return this.authHttp.post(Statique.controllerSavedForm + "/upsert", sf);
	}

	saveHistory(ebUser: EbUser, ebModuleNum: number, path: string, name: string = null) {
		if (!ebUser) throw new Error("User is undefined");

		let sf: SavedForm = new SavedForm();
		sf.ebCompNum = SavedFormIdentifier.HISTORY;
		sf.ebModuleNum = ebModuleNum;
		sf.ebUser = ebUser;
		sf.formName = name || Statique.getModuleNameByModuleNum(ebModuleNum);
		sf.formValues = path;

		this.saveSavedForm(sf).subscribe();
	}

	saveFavoris(ebUser: EbUser, ebModuleNum: number, path: string) {
		if (!ebUser) throw new Error("User is undefined");

		let sf: SavedForm = new SavedForm();
		sf.ebCompNum = SavedFormIdentifier.FAVORIS;
		sf.ebModuleNum = ebModuleNum;
		sf.ebUser = ebUser;
		sf.formName = Statique.getModuleNameByModuleNum(ebModuleNum);
		sf.formValues = path;

		this.saveSavedForm(sf).subscribe();
	}

	deleteSavedForm(sf): Observable<any> {
		return this.authHttp.post(Statique.controllerSavedForm + "/delete", {
			ebSavedFormNum: sf.ebSavedFormNum,
		});
	}

	emitSavedForm(sf: SavedForm) {
		this.savedFormSource.next(sf);
	}

	applyActionToHistory(ebSavedFormNum: number): Observable<any> {
		return this.authHttp.get("/apply-action-to-history?ebSavedFormNum=" + ebSavedFormNum);
	}

	getListRule(): Observable<any> {
		return this.authHttp.get(Statique.controllerSavedForm + "/get-list-rules");
	}

	getListSavedSearchValues(ebSavedFormNum: number) {
		return this.authHttp.get(
			Statique.controllerSavedForm +
				"/get-list-saved-search-values?ebSavedFormNum=" +
				ebSavedFormNum
		);
	}
	applyToHistory(ebSavedFormNum: number) {
		return this.authHttp.get(
			Statique.controllerSavedForm + "/apply-action-to-history?ebSavedFormNum=" + ebSavedFormNum
		);
	}
}
