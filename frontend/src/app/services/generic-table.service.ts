import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from "rxjs";
import { Statique } from "@app/utils/statique";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";

@Injectable({
	providedIn: "root",
})
export class GenericTableService {
	statique = Statique;

	getDatas(url: string, criteria: any): Observable<any> {
		return this.httpClient.post(url, criteria);
	}

	getDetailsDatas(url: string, idDetails: number): Observable<any> {
		return this.httpClient.post(url, idDetails);
	}

	remplissageLivraison(numberLine: number): Observable<any> {
		// je l'ai trouvé nulle part dans le backend 'remplissage-delivery', raté par les cherry-picks ?
		return this.httpClient.post(
			Statique.controllerDeliveryOverView + "/remplissage-delivery",
			numberLine
		);
	}

	runAction(url: string, data: any): Observable<any> {
		return this.httpClient.post(url, data);
	}

	saveTableConfig(config: GenericTableInfos.ConfigData): Observable<any> {
		let headers = new HttpHeaders()
			.set("Content-Type", "application/json")
			.set("Cache-Control", "no-cache");

		let options = {
			headers: headers,
		};
		let url = Statique.controllerGenericTable + "/saveStateTable";
		return this.httpClient.post(url, config, options);
	}

	loadTableConfig(config: GenericTableInfos.ConfigData): Observable<any> {
		let headers = new HttpHeaders()
			.set("Content-Type", "application/json")
			.set("Cache-Control", "no-cache");
		let options = {
			headers: headers,
		};
		let url = Statique.controllerGenericTable + "/loadStateTable";
		return this.httpClient.post(url, config, options);
	}

	constructor(private httpClient: HttpClient) {}
}
