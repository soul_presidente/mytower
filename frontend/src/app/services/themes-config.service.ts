import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Statique } from "@app/utils/statique";
import { EbUserThemes } from "../classes/userThemes ";

@Injectable()
export class ThemesConfigService {
	Statique = Statique;
	rememberMe: boolean;

	setRememberMeValue() {
		
	}
	saveThemesConfig(themeConfig: EbUserThemes): Observable<any> {
		return this.httpClient.post(
			Statique.controllerThemesConfig + "/save-themes-config",
			themeConfig
		);
	}

	getAPITheme(ebCompagnieNum: number): Observable<any> {
		return this.httpClient.post(
			Statique.controllerThemesConfig + "/get-compagnie-theme",
			ebCompagnieNum
		);
	}

	constructor(private httpClient: HttpClient) {}
}
