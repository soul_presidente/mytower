import { EbUserChanel } from "./../classes/user-chanel";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
	providedIn: "root",
})
export class ChanelService {
	url: string = "/api/chanel/register";

	constructor(private httpClient: HttpClient) {}

	registerUser(user: EbUserChanel): Observable<any> {
		return this.httpClient.post(this.url, user, { responseType: "text" });
	}
}
