import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { Statique } from "@app/utils/statique";
import { EbCustomDeclaration } from "@app/classes/declaration";
import { ebFormField } from "@app/classes/ebFormField";
import { EbFormFieldCompagnie } from "@app/classes/ebFormFieldCompagnie";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbCompagnie } from "@app/classes/compagnie";
import { ebFormType } from "@app/classes/ebFormType";
import { EbChatCustom } from "@app/classes/chatCustom";
import { SearchCriteriaCustom } from "@app/utils/SearchCriteriaCustom";
import { CustomDeclarationPricingDTO } from "@app/classes/CustomDeclarationPricingDTO";
import { map } from "rxjs/operators";
import { CustomDeclarationDocumentDTO } from "@app/classes/CustomDeclarationDocumentDTO";
import { FichierJoint } from "@app/classes/fichiersJoint";

@Injectable()
export class DouaneService {
	statique = Statique;
	constructor(private http: HttpClient) {}

	saveListChamp(listDouan: Array<ebFormField>, ebCompagnie: EbCompagnie): Observable<any> {
		let formFieldCompagnie: EbFormFieldCompagnie = new EbFormFieldCompagnie();
		formFieldCompagnie.xEbCompagnie = ebCompagnie;
		formFieldCompagnie.listFormField = listDouan;
		return this.http.post(Statique.controllerConfigCustom + "/save-list-champ", formFieldCompagnie);
	}

	saveEbFormFieldCompagnie(formFieldCompagnie: EbFormFieldCompagnie): Observable<any> {
		return this.http.post(Statique.controllerConfigCustom + "/save-list-champ", formFieldCompagnie);
	}
	dashboardProcedureInlineUpdate(procedure: EbCustomDeclaration): Observable<EbCustomDeclaration> {
		return this.http
			.post(
				Statique.controllerDeclarationDouane + "/procedure-dashboard-declaration-inline-update",
				procedure
			)
			.pipe(map((res) => Statique.cloneObject(res, new EbCustomDeclaration())));
	}
	updateEbFormFieldCompagnie(formFieldCompagnie: EbFormFieldCompagnie): Observable<any> {
		return this.http.post(
			Statique.controllerConfigCustom + "/update-list-champ",
			formFieldCompagnie
		);
	}

	getListChampFormForCompagnie(criteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerConfigCustom + "/get-list-champ", criteria);
	}

	getListChampFormField(): Observable<any> {
		return this.http.get(Statique.controllerConfigCustom + "/get-list-champformfield");
	}

	getListView(): Observable<any> {
		return this.http.get(Statique.controllerConfigCustom + "/getListView");
	}

	getListType(atrType: ebFormType): Observable<any> {
		return this.http.get(Statique.controllerConfigCustom + "/get-list-type");
	}

	listPays(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/countries");
	}

	searchlistAgentBroker(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listAgentBroker");
	}
	searchlistShiper(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listShiper");
	}

	ajouterEbDeclaration(ebDeclaration: EbCustomDeclaration): Observable<any> {
		return this.http.post(Statique.controllerDouane + "/add", ebDeclaration);
	}

	addChatCustom(dataInsertion: string, chat: EbChatCustom): Observable<any> {
		return this.http.post(dataInsertion, chat);
	}

	getListChatCustom(dataSource: string): Observable<any> {
		return this.http.get(dataSource);
	}

	getEbDeclaration(searchCriteria: SearchCriteriaCustom): Observable<any> {
		return this.http.post(Statique.controllerDouane + "/getEbDeclaration", searchCriteria);
	}

	newDeclarationPricing(request: CustomDeclarationPricingDTO): Observable<EbCustomDeclaration> {
		return this.http
			.post(Statique.controllerDouane + "/new-declaration-pricing", request)
			.pipe(map((res) => Statique.cloneObject(res, new EbCustomDeclaration())));
	}

	getListDocumentsByTypeDemande(
		ecModule,
		demandeNum,
		ebUserNum
	): Observable<Array<CustomDeclarationDocumentDTO>> {
		return this.http
			.get(
				Statique.controllerDouane +
					"/listDocuments?module=" +
					ecModule +
					"&demande=" +
					demandeNum +
					"&ebUserNum=" +
					ebUserNum
			)
			.pipe(map((res) => Statique.cloneListObject(res, CustomDeclarationDocumentDTO)));
	}

	uploadFileDeclaration(
		ebCustomDeclarationNum: number,
		codeTypeFile: number,
		ebUserNum: number,
		typeDocCode: string,
		file: any
	): Observable<any> {
		let formData = new FormData();

		formData.append("ebCustomDeclarationNum", String(ebCustomDeclarationNum));
		formData.append("codeTypeFile", String(codeTypeFile));
		formData.append("ebUserNum", String(ebUserNum));
		formData.append("typeDocCode", typeDocCode);
		formData.append("file", file, file.name);

		return this.http.post(Statique.controllerUpload + "/upload-file-custom", formData);
	}

	listAttachedDocuments(ebCustomDeclarationNum: number): Observable<Array<FichierJoint>> {
		return this.http
			.get(
				Statique.controllerDouane +
					"/list-fichierJoints-declaration" +
					"?ebDeclarationNum=" +
					ebCustomDeclarationNum
			)
			.pipe(map((res) => Statique.cloneListObject(res, FichierJoint)));
	}
}
