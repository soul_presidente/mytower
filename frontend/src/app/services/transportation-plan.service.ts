import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { HttpClient, HttpParams } from "@angular/common/http";
import { EbPlTrancheDTO } from "@app/classes/trpl/EbPlTrancheDTO";
import { EbPlGrilleTransportDTO } from "@app/classes/trpl/EbPlGrilleTransportDTO";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { SearchCriteriaPlanTransport } from "@app/utils/searchCriteriaPlanTransport";
import { map } from "rxjs/operators";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { Observable } from "rxjs";
import { EbPlTransportDTO } from "@app/classes/trpl/EbPlTransportDTO";
import { EbUser } from "@app/classes/user";
import { EcCurrency } from "@app/classes/currency";

@Injectable()
export class TransportationPlanService {
	Statique = Statique;
	constructor(private http: HttpClient) {}

	// Zone
	listZone(criteria: SearchCriteria): Observable<Array<EbPlZoneDTO>> {
		return this.http.post(Statique.controllerTransportationPlan + "/list-zone", criteria).pipe(
			map((res) => {
				return Statique.cloneListObject(res, EbPlZoneDTO);
			})
		);
	}

	listZoneAdresse(refAdresse: string, ebCompagnieNum: number) {
		return this.http
			.get(
				Statique.controllerTransportationPlan +
					"/list-zone-adresse?" +
					"refAdresse=" +
					refAdresse +
					"&ebCompagnieNum=" +
					ebCompagnieNum
			)
			.pipe(
				map((res) => {
					return res ? Statique.cloneListObject(res, EbPlZoneDTO) : null;
				})
			);
	}

	listZoneWithCommunity(): Observable<Array<EbPlZoneDTO>> {
		return this.http.get<Array<EbPlZoneDTO>>(
			Statique.controllerTransportationPlan + "/list-zone-contact"
		);
	}
	updateZone(ebZone: EbPlZoneDTO): Observable<EbPlZoneDTO> {
		return this.http
			.post(Statique.controllerTransportationPlan + "/update-zone", ebZone)
			.pipe(map((res) => Statique.cloneObject(res, new EbPlZoneDTO())));
	}
	deleteZone(zoneNum: number) {
		let params = new HttpParams().set("zoneNum", String(zoneNum));
		return this.http.delete(Statique.controllerTransportationPlan + "/delete-zone", {
			params: params,
		});
	}

	// Tranche
	getListTranche(): Observable<Array<EbPlTrancheDTO>> {
		return this.http.get(Statique.controllerTransportationPlan + "/list-tranche").pipe(
			map((res) => {
				return Statique.cloneListObject(res, EbPlTrancheDTO);
			})
		);
	}
	saveTranche(ebTranche: EbPlTrancheDTO): Observable<EbPlTrancheDTO> {
		return this.http
			.post(Statique.controllerTransportationPlan + "/save-tranche", ebTranche)
			.pipe(map((res) => Statique.cloneObject(res, new EbPlTrancheDTO())));
	}
	deleteTranche(trancheNum: number): Observable<any> {
		let params = new HttpParams().set("trancheNum", String(trancheNum));
		return this.http.delete(Statique.controllerTransportationPlan + "/delete-tranche", {
			params: params,
		});
	}

	// Grille
	saveGrilleTransport(grilleTransport: Object): Observable<number> {
		return this.http
			.post(Statique.controllerTransportationPlan + "/save-grille-transport", grilleTransport)
			.pipe(map((res) => +res));
	}
	deleteGrilleTransport(grilleTransportNum: number): Observable<any> {
		let params = new HttpParams().set("grilleTransportNum", String(grilleTransportNum));
		return this.http.delete(Statique.controllerTransportationPlan + "/delete-grille-transport", {
			params: params,
		});
	}
	listGrille(): Observable<Array<EbPlGrilleTransportDTO>> {
		return this.http
			.get(Statique.controllerTransportationPlan + "/all-grille")
			.pipe(map((res) => Statique.cloneListObject(res, EbPlGrilleTransportDTO)));
	}

	//Plan
	updatePlan(ebPlan: EbPlTransportDTO): Observable<EbPlTransportDTO> {
		return this.http
			.post(Statique.controllerTransportationPlan + "/update-plan", ebPlan)
			.pipe(map((res) => Statique.cloneObject(res, new EbPlTransportDTO())));
	}

	deletePlan(planTransportNum: number): Observable<any> {
		let params = new HttpParams().set("planTransportNum", String(planTransportNum));
		return this.http.delete(Statique.controllerTransportationPlan + "/delete-plan", {
			params: params,
		});
	}

	getListTransporteur(): Observable<Array<EbUser>> {
		return this.http
			.get(Statique.controllerTransportationPlan + "/list-transporteur")
			.pipe(map((res) => Statique.cloneListObject(res, EbUser)));
	}

	searchPlanTransport(query: SearchCriteriaPlanTransport) {
		return this.http.post(Statique.controllerTransportationPlan + "/plan/search", query);
	}

	searchPricingTransport(query: SearchCriteriaPlanTransport) {
		return this.http.post(Statique.controllerTransportationPlan + "/carrier/search", query);
	}

	selectListCurrencyByeCompagnieCurrency(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerTransportationPlan + "/get-list-currency-by-stetting",
			searchCriteria
		);
	}

	isMTCEnabled() {
		return this.http.get(Statique.controllerTransportationPlan + "/mtc-enabled");
	}
}
