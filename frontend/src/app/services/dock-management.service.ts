import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { Observable } from "rxjs";
import { HttpClient, HttpParams } from "@angular/common/http";
import { EbDockDTO } from "@app/classes/dock/EbDockDTO";
import { EbEntrepotDTO } from "@app/classes/dock/EbEntrepotDTO";
import { EbDkOpeningDayDTO } from "@app/classes/dock/EbDkOpeningDayDTO";
import { EbDkOpeningWeekDTO } from "@app/classes/dock/EbDkOpeningWeekDTO";
import { EbDkOpeningPeriodDTO } from "@app/classes/dock/EbDkOpeningPeriodDTO";
import { map } from "rxjs/operators";
import { SearchCriteria } from "@app/utils/searchCriteria";
@Injectable({
	providedIn: "root",
})
export class DockManagementService {
	constructor(private http: HttpClient) {}

	//--------entrepot
	getListEntrepotTable(criteria: SearchCriteria): Observable<Array<EbEntrepotDTO>> {
		return this.http
			.post(Statique.controllerDock + "/list-entrepot-table", criteria)
			.pipe(map((res) => Statique.cloneListObject(res["data"], EbEntrepotDTO)));
	}
	getListEntrepot(bindDocks: boolean = false): Observable<Array<EbEntrepotDTO>> {
		return this.http
			.get(Statique.controllerDock + "/get-list-entrepot" + "?bindDocks=" + String(bindDocks))
			.pipe(map((res) => Statique.cloneListObject(res, EbEntrepotDTO)));
	}
	addEntrepot(entrepot: EbEntrepotDTO): Observable<EbEntrepotDTO> {
		return this.http
			.post(Statique.controllerDock + "/add-entrepot", entrepot)
			.pipe(map((res) => Statique.cloneObject(res, new EbEntrepotDTO())));
	}
	deleteEntrepot(ebEntrepotNum: number): Observable<void> {
		return this.http
			.post(Statique.controllerDock + "/delete-entrepot", ebEntrepotNum)
			.pipe(map((res) => null));
	}

	//-------dock

	addDockPeriode(ebDock: EbDockDTO): Observable<EbDockDTO> {
		return this.http
			.post(Statique.controllerDock + "/add-dock-periode", ebDock)
			.pipe(map((res) => Statique.cloneObject(res, new EbDockDTO())));
	}
	deleteDock(ebDockNum: number): Observable<void> {
		let params = new HttpParams().set("ebDockNum", String(ebDockNum));
		return this.http
			.delete(Statique.controllerDock + "/delete-dock", {
				params: params,
			})
			.pipe(map((res) => null));
	}
	getListDock(entrepotNum: number): Observable<Array<EbDockDTO>> {
		return this.http
			.post(Statique.controllerDock + "/get-list-dock", entrepotNum)
			.pipe(map((res) => Statique.cloneListObject(res, EbDockDTO)));
	}

	getListOpeningDay(): Observable<Array<EbDkOpeningDayDTO>> {
		return this.http
			.get(Statique.controllerDock + "/get-list-openingDay")
			.pipe(map((res) => Statique.cloneListObject(res, EbDkOpeningDayDTO)));
	}
	addOpeningDays(listOpeningDay: Array<EbDkOpeningDayDTO>): Observable<void> {
		return this.http
			.post(Statique.controllerDock + "/add-opening-days", listOpeningDay)
			.pipe(map((res) => null));
	}
	deleteOpeningDay(day: EbDkOpeningDayDTO): Observable<void> {
		return this.http
			.post(Statique.controllerDock + "/delete-opening-day", day)
			.pipe(map((res) => null));
	}
	getListOpeningWeek(entrepotNum: number): Observable<Array<EbDkOpeningWeekDTO>> {
		return this.http
			.post(Statique.controllerDock + "/get-list-opening-week", entrepotNum)
			.pipe(map((res) => Statique.cloneListObject(res, EbDkOpeningWeekDTO)));
	}
	getBaseOpeningWeek(entrepotNum: number): Observable<EbDkOpeningWeekDTO> {
		return this.http
			.post(Statique.controllerDock + "/get-base-opening-week", entrepotNum)
			.pipe(map((res) => Statique.cloneObject(res, new EbDkOpeningWeekDTO())));
	}
	getOpeningWeek(openingWeekNum: number): Observable<EbDkOpeningWeekDTO> {
		return this.http
			.post(Statique.controllerDock + "/get-opening-week", openingWeekNum)
			.pipe(map((res) => Statique.cloneObject(res, new EbDkOpeningWeekDTO())));
	}
	getListOpeningWeeks(entrepotNum: number): Observable<Array<EbDkOpeningWeekDTO>> {
		return this.http
			.post(Statique.controllerDock + "/list-opening-weeks", entrepotNum)
			.pipe(map((res) => Statique.cloneListObject(res, EbDkOpeningWeekDTO)));
	}
	saveOpeningWeek(week: EbDkOpeningWeekDTO): Observable<EbDkOpeningWeekDTO> {
		return this.http
			.post(Statique.controllerDock + "/save-opening-week", week)
			.pipe(map((res) => Statique.cloneObject(res, new EbDkOpeningWeekDTO())));
	}
	saveOpeningPeriod(period: EbDkOpeningPeriodDTO): Observable<EbDkOpeningPeriodDTO> {
		return this.http
			.post(Statique.controllerDock + "/save-opening-period", period)
			.pipe(map((res) => Statique.cloneObject(res, new EbDkOpeningPeriodDTO())));
	}
	deleteOpeningPeriod(ebDkOpeningPeriodNum: number): Observable<void> {
		let params = new HttpParams().set("ebDkOpeningPeriodNum", String(ebDkOpeningPeriodNum));
		return this.http
			.delete(Statique.controllerDock + "/delete-opening-period", { params: params })
			.pipe(map((res) => null));
	}
	getListOpeningPeriod(ebDkOpeningWeekNum: number): Observable<Array<EbDkOpeningPeriodDTO>> {
		let params = new HttpParams().set("ebDkOpeningWeekNum", String(ebDkOpeningWeekNum));
		return this.http
			.get(Statique.controllerDock + "/list-opening-period-by-week", { params: params })
			.pipe(map((res) => Statique.cloneListObject(res, EbDkOpeningPeriodDTO)));
	}
	getListOpeningPeriodByEntrepot(ebEntrepotNum: number): Observable<Array<EbDkOpeningPeriodDTO>> {
		let params = new HttpParams().set("ebEntrepotNum", String(ebEntrepotNum));
		return this.http
			.get(Statique.controllerDock + "/list-opening-period-by-entrepot", { params: params })
			.pipe(map((res) => Statique.cloneListObject(res, EbDkOpeningPeriodDTO)));
	}
	deleteOpeningWeek(week: EbDkOpeningWeekDTO): Observable<void> {
		return this.http
			.post(Statique.controllerDock + "/delete-opening-week", week)
			.pipe(map((res) => null));
	}
}
