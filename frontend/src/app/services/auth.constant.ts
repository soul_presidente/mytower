export const TOKEN_NAME = "access_token";
export const KIBANA_TOKEN = "kibana_token";
export const WS_SESSION_ID = "ws_session_id";
