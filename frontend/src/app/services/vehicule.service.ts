import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Statique } from "../utils/statique";
import { EbVehicule } from "../classes/vehicule";
import { Observable } from "rxjs";

@Injectable({
	providedIn: "root",
})
export class VehiculeService {
	constructor(private http: HttpClient) {}
	statique = Statique;

	save(listVehicule: Array<EbVehicule>): Observable<any> {
		return this.http.post(Statique.controllerVehicule + "/save", listVehicule);
	}

	getListVehicule(compagnieNum: number): Observable<any> {
		let urlParams = "?compagnieNum=" + compagnieNum;
		return this.http.get(Statique.controllerVehicule + "/get-vehicule-by-etablissment" + urlParams);
	}

	deleteVehicule(vehiculeNum: number): Observable<any> {
		let urlParams = "?vehiculeNum=" + vehiculeNum;
		return this.http.get(Statique.controllerVehicule + "/delete" + urlParams);
	}

	getListTypeUnit(compagnieNum: number): Observable<any> {
		let urlParams = "?compagnieNum=" + compagnieNum;
		return this.http.get(Statique.controllerTypeUnit + "/get-typeUnit-by-compagnie" + urlParams);
	}
}
