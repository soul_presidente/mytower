import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { SearchCriteriaAnalytics } from "@app/classes/analytics";

import { HttpClient } from "@angular/common/http";
import { Statique } from "@app/utils/statique";
import { AnalyticsConfigDTO } from "@app/classes/analytics/analytics-config";

@Injectable()
export class FreightAnalyticsService {
	getConfig(): Observable<AnalyticsConfigDTO> {
		return this.http.get<AnalyticsConfigDTO>(`${Statique.controllerAnalytics}/config`)
	}

	getTransportData(criteria: SearchCriteriaAnalytics): Observable<any> {
		return this.http.post(
			`http://localhost:8080/${Statique.controllerAnalytics}/transport`,
			criteria
		);
	}

	getActivityData(criteria: SearchCriteriaAnalytics): Observable<any> {
		return this.http.post(
			`http://localhost:8080/${Statique.controllerAnalytics}/activity`,
			criteria
		);
	}

	getAllDataFromKibana(kibanaUrl: string): Observable<any> {
		//envoyer une post a kibana pour generer csv file (il retourne une reponse qui contient le path pour telecharger le csv)
		return this.http.post<any>(kibanaUrl + "/api/reporting/generate/csv?jobParams=(conflictedTypesFields:!(),fields:!(transport_refernce,%27date%20de%20creation%27,chargeur,transporteur,%27mode%20de%20transport%27,statut,%27origine%20pays%27,%27%20origin%20pays%20(coordonnees)%27,%27destination%20pays%27,%27destination%20pays%20(coordonnees)%27,%27prix%20de%20quotation%27,%27poids%20total%27,%27ratio%20quot%2Fpoid%27,saving,%27average%20cost%27,%27origin%20company%27,%27destination%20company%27,%27date%20de%20livraison%27,%27date%20estimee%27,taxes_total,incident,incident_categorie,unconformite,unconformite_categorie,%27origin%20zone%27,%27destination%20zone%27),indexPatternId:%27489e8350-247a-11eb-b5d5-bb1327c5c8a6%27,metaFields:!(_source,_id,_type,_index,_score),searchRequest:(body:(_source:(excludes:!(),includes:!(transport_refernce,%27date%20de%20creation%27,chargeur,transporteur,%27mode%20de%20transport%27,statut,%27origine%20pays%27,%27%20origin%20pays%20(coordonnees)%27,%27destination%20pays%27,%27destination%20pays%20(coordonnees)%27,%27prix%20de%20quotation%27,%27poids%20total%27,%27ratio%20quot%2Fpoid%27,saving,%27average%20cost%27,%27origin%20company%27,%27destination%20company%27,%27date%20de%20livraison%27,%27date%20estimee%27,taxes_total,incident,incident_categorie,unconformite,unconformite_categorie,%27origin%20zone%27,%27destination%20zone%27)),docvalue_fields:!(),query:(bool:(filter:!((match_all:())),must:!(),must_not:!(),should:!())),script_fields:(),sort:!((_score:(order:desc))),stored_fields:!(transport_refernce,%27date%20de%20creation%27,chargeur,transporteur,%27mode%20de%20transport%27,statut,%27origine%20pays%27,%27%20origin%20pays%20(coordonnees)%27,%27destination%20pays%27,%27destination%20pays%20(coordonnees)%27,%27prix%20de%20quotation%27,%27poids%20total%27,%27ratio%20quot%2Fpoid%27,saving,%27average%20cost%27,%27origin%20company%27,%27destination%20company%27,%27date%20de%20livraison%27,%27date%20estimee%27,taxes_total,incident,incident_categorie,unconformite,unconformite_categorie,%27origin%20zone%27,%27destination%20zone%27),version:!t),index:freight_analytics),title:MyTower-Demandes,type:search)",{})
	}

	constructor(private http: HttpClient) {}
}
