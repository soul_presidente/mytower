import { Statique } from "@app/utils/statique";
import { Injectable } from "@angular/core";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";

@Injectable()
export class TypeRequestService {
	statique = Statique;
	constructor(private http: HttpClient) {}

	getListTypeRequests(criterias: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerTypeRequest + "/list-type-request", criterias);
	}

	listTypeRequestsContact(): Observable<any> {
		return this.http.get(Statique.controllerTypeRequest + "/list-type-request-contact");
	}

	addTypeRequest(typeRequest: EbTypeRequestDTO): Observable<any> {
		return this.http.post(Statique.controllerTypeRequest + "/add-type-request", typeRequest);
	}

	deleteTypeRequest(typeRequestNum: number): Observable<any> {
		return this.http.post(Statique.controllerTypeRequest + "/delete-type-request", typeRequestNum);
	}

	getTimeAgo(dateCreation: Date, onlyHours?: boolean): any {
		let diff = Math.abs(new Date(dateCreation).getTime() - new Date().getTime());
		if(onlyHours){
			return diff / (1000 * 3600);
		}
		const nbrTotalOfDays = diff / (1000 * 3600 * 24);
		const nbrOfDays = Math.floor(nbrTotalOfDays);
		const nbrTotalOfHours = Math.abs(nbrTotalOfDays - nbrOfDays) * 24;
		const nbrOfHours = Math.floor(nbrTotalOfHours);
		const nbrTotalOfMin = Math.abs(nbrTotalOfHours - nbrOfHours) * 60;
		const nbrOfMin = Math.ceil(nbrTotalOfMin);
		const result: any = {
			nbrOfDays: nbrOfDays,
			nbrOfHours: nbrOfHours,
			nbrOfMin: nbrOfMin,
		};
		return result;
	}

	getTimeRemaining(delaiH: number) {
		let nbrTotalOfDays = delaiH / 24;
		const nbrOfDays = Math.floor(nbrTotalOfDays);
		const nbrTotalOfHours = Math.abs(nbrTotalOfDays - nbrOfDays) * 24;
		const nbrOfHours = Math.floor(nbrTotalOfHours);
		const nbrTotalOfMin = Math.abs(nbrTotalOfHours - nbrOfHours) * 60;
		const nbrOfMin = Math.ceil(nbrTotalOfMin);
		const result: any = {
			nbrOfDays: nbrOfDays,
			nbrOfHours: nbrOfHours,
			nbrOfMin: nbrOfMin,
		};
		return result;
	}
}
