import { SearchCriteria } from "@app/utils/searchCriteria";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { Statique } from "@app/utils/statique";
import { PrMtcTransporteur } from "@app/classes/PrMtcTransporteur";
import { EbTtCompanyPsl } from "@app/classes/ttCompanyPsl";

@Injectable()
export class PrMtcTransporteurService {
	Statique = Statique;

	constructor(private http: HttpClient) {}

	ListPrMtcTransporteur(criteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerPrGlobal + "/transporteur", criteria).pipe(
			map((res: any) => {
				return Statique.cloneListObject(res.data, PrMtcTransporteur);
			})
		);
	}

	deleteMtcTransporteur(mtcTranspNum: number) {
		let params = new HttpParams().set("mtcTranspNum", String(mtcTranspNum));
		return this.http.delete(Statique.controllerPrGlobal + "/delete-mtc-transporteur", {
			params: params,
		});
	}

	updateMtcTransporteur(prMtcTransporteur: PrMtcTransporteur): Observable<any> {
		return this.http.post(Statique.controllerPrGlobal + "/update-mtc-transporteur", prMtcTransporteur);
	}
}
