import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { HttpClient, HttpParams } from "@angular/common/http";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { EbTtCompanyPsl } from "../classes/ttCompanyPsl";
import { EbIncoterm } from "@app/classes/EbIncoterm";

@Injectable()
export class ConfigIncotermService {
	Statique = Statique;
	constructor(private http: HttpClient) {}

	getListIncotermsTable(criteria: SearchCriteria): Observable<Array<EbIncoterm>> {
		return this.http.post(Statique.controllerIncoterm + "/list-incoterms", criteria).pipe(
			map((res: any) => {
				return Statique.cloneListObject(res.data, EbIncoterm);
			})
		);
	}
	getListIncotermsPromise(criteria: SearchCriteria): Promise<Array<EbIncoterm>> {
		return new Promise((resolve) => {
			this.getListIncotermsTable(criteria).subscribe((res) => resolve(res));
		});
	}

	updateIncoterm(searchCriteria: any): Observable<any> {
		return this.http.post(Statique.controllerIncoterm + "/update-incoterm", searchCriteria);
	}

	deleteIncoterm(id: Number): Observable<any> {
		return this.http.post(Statique.controllerIncoterm + "/delete-incoterm", id);
	}
}
