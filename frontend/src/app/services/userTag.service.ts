import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Statique } from "@app/utils/statique";
import { EbUserTag } from "@app/classes/EbUserTag";
import { Observable } from "rxjs";

@Injectable()
export class UserTagService {
	statique: Statique;
	constructor(private http: HttpClient) {}

	saveUserTag(ebUserTag: EbUserTag, ebCompagnieNum: Number): Observable<any> {
		let params = {};
		if (ebCompagnieNum != null) params["ebCompagnieNum"] = ebCompagnieNum;
		return this.http.post(Statique.controllerUserTag + "/create-user-tag", ebUserTag, {
			params: params,
		});
	}

	deleteTag(tag: string): Observable<any> {
		return this.http.post(Statique.controllerUserTag + "/delete-tag", tag);
	}
}
