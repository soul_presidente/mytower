import { GenericEnum } from "@app/classes/GenericEnum";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Statique } from "@app/utils/statique";
import { EbControlRule } from "@app/classes/controlRule";
import { Observable } from "rxjs";
import { EbDemande } from "@app/classes/demande";

@Injectable()
export class ControlRuleService {
	constructor(private http: HttpClient) {}

	add(data: EbControlRule) {
		return this.http.post(Statique.controllerControlRule, data);
	}

	update(data: EbControlRule) {
		return this.http.put(Statique.controllerControlRule, data);
	}

	getAll(ebCompagnieNum: number) {
		return this.http.get(Statique.controllerControlRule + "all/?ebCompagnieNum=" + ebCompagnieNum);
	}

	getOne(ebControlRuleNum: number) {
		return this.http.get(Statique.controllerControlRule + "?ebControlRuleNum=" + ebControlRuleNum);
	}

	deactivate(ebControlRuleNum: number, activated: boolean) {
		return this.http.get(
			Statique.controllerControlRule +
				"/deactivate/?ebControlRuleNum=" +
				ebControlRuleNum +
				"&activated=" +
				activated
		);
	}

	deleteControlRule(ebControlRuleNum: number) {
		return this.http.post(
			Statique.controllerControlRule + "/delete-control-rule",
			ebControlRuleNum
		);
	}

	// ----------------------------------------------------- ENUM ----------------------------- //

	listPriorityEnum(): Observable<any> {
		return this.http.get(
			Statique.controllerStatiqueListe + "/list-generic-enum?genericEnumKey=CR.Priority"
		);
	}

	listOperatorEnum(): Observable<any> {
		return this.http.get(
			Statique.controllerStatiqueListe + "/list-generic-enum?genericEnumKey=CR.Operator"
		);
	}

	listPslTypeEnum(): Observable<any> {
		return this.http.get(
			Statique.controllerStatiqueListe + "/list-generic-enum?genericEnumKey=TT.NatureDateEvent"
		);
	}

	listRuleOperatorEnum(): Observable<any> {
		return this.http.get(
			Statique.controllerStatiqueListe + "/list-generic-enum?genericEnumKey=CR.RuleOperator"
		);
	}

	listRuleTypeEnum(): Observable<any> {
		return this.http.get(
			Statique.controllerStatiqueListe + "/list-generic-enum?genericEnumKey=CR.RuleType"
		);
	}

	listRuleTriggerEnum(): Observable<any> {
		return this.http.get(
			Statique.controllerStatiqueListe + "/list-generic-enum?genericEnumKey=CR.RuleTrigger"
		);
	}

	listRuleOperandTypeEnum(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listCRFields");
	}

	listRuleDocumentValueEnum(): Observable<any> {
		return this.http.get(
			Statique.controllerStatiqueListe + "/list-generic-enum?genericEnumKey=CR.DocumentStatus"
		);
	}

	getInputData(): Observable<any> {
		return this.http.get(Statique.controllerControlRule + "/doc-rule-inputs-data");
	}

	applyRulesWithExportControlEnabled(demande: EbDemande): Observable<any> {
		return this.http.post(
			Statique.controllerControlRule + "/apply-rules-with-action-exportControl",
			demande
		);
	}

	getListConditionCRTypeEnum() {
		return this.http.get(
			Statique.controllerStatiqueListe + "/list-generic-enum?genericEnumKey=CR.conditions_type"
		);
	}
}
