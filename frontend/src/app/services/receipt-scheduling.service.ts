import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { EbRschUnitScheduleDTO } from "@app/classes/rsch/EbRschUnitScheduleDTO";
import { SearchCriteriaReceiptScheduling } from "@app/utils/SearchCriteriaReceiptScheduling";
import { map } from "rxjs/operators";

@Injectable({
	providedIn: "root",
})
export class ReceiptSchedulingService {
	constructor(private http: HttpClient) {}

	dashboardInlineUpdate(
		ebRschUnitSchedule: EbRschUnitScheduleDTO
	): Observable<EbRschUnitScheduleDTO> {
		return this.http
			.post(
				Statique.controllerReceiptScheduling + "/rsch-dashboard-inline-update",
				ebRschUnitSchedule
			)
			.pipe(map((res) => Statique.cloneObject(res, new EbRschUnitScheduleDTO())));
	}

	getListRschUnitSchedule(
		criterias: SearchCriteriaReceiptScheduling
	): Observable<Array<EbRschUnitScheduleDTO>> {
		return this.http
			.post(Statique.controllerReceiptScheduling + "/list-rschUnit-schedule", criterias)
			.pipe(map((res) => Statique.cloneListObject(res, EbRschUnitScheduleDTO)));
	}

	getCountListRschUnitSchedule(criterias: SearchCriteriaReceiptScheduling): Observable<number> {
		return this.http
			.post(Statique.controllerReceiptScheduling + "/count-list-rschUnit-schedule", criterias)
			.pipe(map((res) => +res));
	}

	listUnifiableRschUnitSchedule(): Observable<Array<EbRschUnitScheduleDTO>> {
		return this.http
			.get(Statique.controllerReceiptScheduling + "/list-unitSchedule-unifiable")
			.pipe(map((res) => Statique.cloneListObject(res, EbRschUnitScheduleDTO)));
	}

	doUnplanify(): Observable<void> {
		return this.http
			.post(Statique.controllerReceiptScheduling + "/do-unplanify", {})
			.pipe(map((res) => null));
	}

	doCalculate(): Observable<void> {
		return this.http
			.post(Statique.controllerReceiptScheduling + "/do-calculate", {})
			.pipe(map((res) => null));
	}
}
