import { HttpClient, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Statique } from "@app/utils/statique";
import { SearchCriteriaTrackTrace } from "@app/utils/SearchCriteriaTrackTrace";
import { EbPslApp } from "@app/classes/pslApp";
import { TTEvent } from "@app/classes/tTEvent";
import { EbTracing } from "@app/classes/tracing";
import { EbDemande } from "@app/classes/demande";
import { TypeOfEvent } from "@app/utils/enumeration";

@Injectable()
export class TrackTraceService {
	Statique = Statique;
	constructor(private authHttp: HttpClient) {}

	getEbTracing(searchCriteria: SearchCriteriaTrackTrace): Observable<any> {
		return this.authHttp.post(Statique.controllerTrackTrace + "/getEbTracing", searchCriteria);
	}

	getListTracingTurboTable(searchCriteria: any): Observable<any> {
		return this.authHttp.post(Statique.controllerTrackTrace + "/listTrack", searchCriteria);
	}

	updateEbPslApp(ebPslApp: EbPslApp): Observable<any> {
		return this.authHttp.post(Statique.controllerTrackTrace + "/updateEbPslApp", ebPslApp);
	}

	updateEbPslAppMasse(listPslApp: Array<EbPslApp>): Observable<any> {
		return this.authHttp.post(Statique.controllerTrackTrace + "/updateEbPslAppMasse", listPslApp);
	}

	addDeviationMasse(
		listTTEvent: Array<TTEvent>,
		typeOfEvent: number = TypeOfEvent.INCIDENT
	): Observable<any> {
		return this.authHttp.post(
			Statique.controllerTrackTrace + "/addDeviationMasse?typeOfEvent=" + typeOfEvent,
			listTTEvent
		);
	}

	addDeviation(tTEvent: TTEvent, typeOfEvent: number = TypeOfEvent.INCIDENT): Observable<any> {
		return this.authHttp.post(
			Statique.controllerTrackTrace + "/addDeviation?typeOfEvent=" + typeOfEvent,
			tTEvent
		);
	}

	addTrackTrace(ebDemande: EbDemande): Observable<any> {
		return this.authHttp.post(Statique.controllerTrackTrace + "/addTrackTrace", ebDemande);
	}

	getListEbTracingByEbDemandeNum(ebDemandeNum: number): Observable<any> {
		return this.authHttp.post(
			Statique.controllerTrackTrace + "/getListEbTracingByEbDemandeNum",
			ebDemandeNum
		);
	}

	addOrRemoveTracing(
		listTracking: Map<number, EbTracing>,
		ebTtTracingNum: number,
		configPsl: string,
		event: HTMLElement
	) {
		if (listTracking.has(ebTtTracingNum)) {
			let ebTracing = listTracking.get(ebTtTracingNum);
			listTracking.delete(ebTtTracingNum);
		} else {
			let ebTracing: EbTracing = new EbTracing();
			ebTracing.ebTtTracingNum = ebTtTracingNum;
			ebTracing.configPsl = configPsl;
			listTracking.set(ebTtTracingNum, ebTracing);
		}
	}

	getHtmlTracing(listTracking: Map<number, EbTracing>, ebTtTracingNum: number, configPsl: string) {
		let style = "font-size: 20px;cursor: pointer;";
		let _type = "checkbox";
		return (
			"<input class='checkbox-Track fa'  id=" +
			ebTtTracingNum +
			"  type='" +
			_type +
			"' style='" +
			style +
			"'onchange='functions.clickCheckbox(" +
			ebTtTracingNum +
			',"' +
			configPsl +
			"\",event.target)'>"
		);
	}

	// getListEbIncotermPsl(){
	// 	return this.authHttp
	// 		.get(Statique.controllerTrackTrace + "/getListEbIncotermPsl")
	// }

	// prettier-ignore
	saveFlags(ebTtTracingNum: number, newFlags: string): Observable<any> {
    return this.authHttp.get(
      Statique.controllerTrackTrace +
        "/saveFlags?ebTtTracingNum=" + ebTtTracingNum +
        "&newFlags=" + newFlags
    );
  }

	getList(searchCriteria: SearchCriteriaTrackTrace): Observable<any> {
		return this.authHttp.post(Statique.controllerTrackTrace + "/list-track-smart", searchCriteria);
	}

	updateRevisedDateEbPslAppMasse(listPslApp: Array<EbPslApp>): Observable<any> {
		return this.authHttp.post(
			Statique.controllerTrackTrace + "/updateRevisedDateEbPslAppMasse",
			listPslApp
		);
	}

	updateRevisedDateEbPslApp(ebPslApp: EbPslApp): Observable<any> {
		return this.authHttp.post(
			Statique.controllerTrackTrace + "/updateRevisedDateEbPslApp",
			ebPslApp
		);
	}

	getMapTracingPslEvent(ebTracing: any): Observable<any> {
		return this.authHttp.post(Statique.controllerTrackTrace + "/getMapTracingPslEvent", ebTracing);
	}
	getTracingPslAndsThiereEventsByEbTtTracingNum(ebTtTracingNum: number): Observable<any> {
		return this.authHttp.post<any>(
			Statique.controllerTrackTrace + "/getTracingPslAndsThiereEvents",
			ebTtTracingNum
		);
	}

	getListDocumentsByTypeDemande(ecModule, demandeNum, ebUserNum): Observable<any> {
		return this.authHttp.get(
			Statique.controllerStatiqueListe +
				"/listDocuments?module=" +
				ecModule +
				"&demande=" +
				demandeNum +
				"&ebUserNum=" +
				ebUserNum
		);
	}
}
