import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Statique } from "@app/utils/statique";

@Injectable({
	providedIn: "root",
})
export class LoadPlanningService {
	statique: Statique;

	constructor(private httpClient: HttpClient) {}

	getListWeekType() {
		return this.httpClient.get(Statique.controllerStatiqueListe + "/weekType");
	}
}
