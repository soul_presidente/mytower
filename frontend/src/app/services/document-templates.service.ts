import { Statique } from "@app/utils/statique";
import { Injectable } from "@angular/core";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { EbTemplateDocumentsDTO } from "@app/classes/dto/ebTemplateDocumentsDTO";
import { TemplateGenParamsDTO } from "@app/classes/TemplateGenParamsDTO";
import {EbDemande} from "@app/classes/demande";

@Injectable()
export class DocumentTemplatesService {
	statique = Statique;
	constructor(private http: HttpClient) {}

	getListDocumentTemplates(criterias: SearchCriteria): Observable<Array<EbTemplateDocumentsDTO>> {
		return this.http.post<Array<EbTemplateDocumentsDTO>>(
			Statique.controllerDocumentTemplates + "/list-document-templates",
			criterias
		);
	}

	addDocumentTemplates(docTemplates: EbTemplateDocumentsDTO): Observable<any> {
		return this.http.post(
			Statique.controllerDocumentTemplates + "/add-document-templates",
			docTemplates
		);
	}

	deleteDocumentTemplates(docTemplatesNum: number): Observable<any> {
		return this.http.post(
			Statique.controllerDocumentTemplates + "/delete-document-templates",
			docTemplatesNum
		);
	}

	fireDocumentGeneration(params: TemplateGenParamsDTO): Observable<Blob> {
		return this.http.post(Statique.controllerDocumentTemplates + "/document-generation", params, {
			responseType: "blob",
			headers: new HttpHeaders().append("Content-Type", "application/json"),
		});
	}

	chekedInformationMandatory(params: TemplateGenParamsDTO):Observable<Boolean>{

		return this.http.post<Boolean>(Statique.controllerDocumentTemplates + "/cheked-information-mandatory", params);

	}
}
