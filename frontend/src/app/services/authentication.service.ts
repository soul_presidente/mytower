import { Injectable, Injector } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { HttpClient } from "@angular/common/http";
import { JwtHelperService } from "@auth0/angular-jwt";
import { Observable, of, Subject } from "rxjs";
import { EbUser } from "@app/classes/user";
import { KIBANA_TOKEN, TOKEN_NAME } from "./auth.constant";
import { EbEtablissement } from "@app/classes/etablissement";
import { catchError, map } from "rxjs/operators";
import { ExUserModule } from "@app/classes/accessRight";
import { environment } from "@src/environments/environment";
import { AclService } from "./acl.service";
import { EbAclRightDTO } from "@app/classes/dto/EbAclRightDTO";

export let InjectorInstance: Injector;

@Injectable()
export class AuthenticationService {
	connectedUser = new Subject();
	private notifySideBar = new Subject<any>();
	sideBarInitUserObservale = this.notifySideBar.asObservable();

	private static KEY_ACLS: string = "mt.acls";

	static getListAccessRights(token: string) {
		let decodedUser = this.decodeTocken(token);
		if (!decodedUser) return;
		const httpClient = InjectorInstance.get<HttpClient>(HttpClient);
		httpClient
			.get(
				Statique.controllerAccount + "/get-user-access-rights?eb_user_num=" + decodedUser.ebUserNum
			)
			.subscribe((rights: ExUserModule[]) => {
				if (rights) localStorage.setItem("access_rights", JSON.stringify(rights));
			});
	}

	static UpdateAccessRightsLocalStorage(rights: ExUserModule[]) {
		if (rights && rights.length) {
			localStorage.setItem("access_rights", JSON.stringify(rights));
		}
	}

	set theConnectedUser(value) {
		this.connectedUser.next(value); // this will make sure to tell every subscriber about the change.
		localStorage.setItem(TOKEN_NAME, value);
		AuthenticationService.getListAccessRights(value);
	}

	public notifySideBarInitUser() {
		this.notifySideBar.next();
	}

	get theConnectedUser() {
		return localStorage.getItem(TOKEN_NAME);
	}

	constructor(
		private http: HttpClient,
		private injector: Injector,
		private aclService: AclService
	) {
		InjectorInstance = this.injector;
	}

	kibanaAuth(): Observable<any> {
		return this.http.post(
			Statique.KIBANA_AUTH + "?token=" + localStorage.getItem(TOKEN_NAME),
			{},
			{ responseType: "json" }
		);
	}

	/**
	 * Cette fonction est responsable de l'authentification. C'est elle qui envoit  le login et le mot de  passe au backend.
	 * Le backend  genere et retourne un tocken en cas de succès
	 * @param username
	 * @param password
	 * @param emailCt
	 * @param passwordCt
	 * @param isControlTower
	 * @param activeToken
	 */
	login(username: string, password: string, isControlTower: boolean = false) {
		let self = this;
		let body = `?username=${encodeURIComponent(username)}&password=${encodeURIComponent(password)}`;
		let tokenStr = Statique.getTokenForCt();

		if (isControlTower) body += `&tokenct=${encodeURIComponent(tokenStr)}`;

		localStorage.setItem("tokenVersion", environment.tokenVersion.toString());

		return this.http.post(Statique.AUTH_TOKEN + body, {}, { responseType: "text" }).pipe(
			map((res) => {
				let accessToken: string = res;

				if (!AuthenticationService.tokenVersionOk()) {
					this.logout();
					window.location.replace("/login");
					return null;
				}

				self.theConnectedUser = accessToken;
				self.refreshACLs();
				//this.itemValue.next(true);
				return accessToken;
			})
		);
	}

	logout() {
		let self = this;
		self.theConnectedUser = null;
		localStorage.clear();
	}

	static tokenVersionOk(token?: string) {
		// check for the token version
		return (
			localStorage.getItem("tokenVersion") &&
			localStorage.getItem("tokenVersion") === Statique.tokenVersion.toString()
		);
	}

	static isAuthenticated(): boolean {
		let jwtHelper: JwtHelperService = new JwtHelperService();
		const token = localStorage.getItem(TOKEN_NAME);
		if (!this.tockenIsNull(token)) {
			let isTokenExpired = jwtHelper.isTokenExpired(token);
			if (isTokenExpired) {
				return false;
			}
			if (!AuthenticationService.tokenVersionOk()) return false;
		}

		let user = AuthenticationService.getConnectedUser();
		return user != null;
	}

	static getConnectedUser(): EbUser {
		const token = localStorage.getItem(TOKEN_NAME);

		return this.getUserFromToken(token);
	}

	public static getUserFromToken(token: string): EbUser {
		let decodedUser = AuthenticationService.decodeTocken(token);

		if (decodedUser) {
			let user: EbUser = new EbUser().constructorCopy(decodedUser);
			if (!user.ebEtablissement) {
				user.ebEtablissement = new EbEtablissement();
			}
			return user;
		} else {
			return decodedUser;
		}
	}

	public static decodeTocken(tockenStr: any): EbUser {
		let jwtHelper: JwtHelperService = new JwtHelperService();
		if (!this.tockenIsNull(tockenStr)) {
			const decodedToken = jwtHelper.decodeToken(tockenStr);
			if (decodedToken.authorities != null) {
				if (!decodedToken.connectedUserKey) decodedToken.connectedUserKey = decodedToken.user;
				if (decodedToken.connectedUserKey)
					decodedToken.connectedUserKey.authorities = decodedToken.authorities;
			}

			return decodedToken.connectedUserKey;
		}
		return null;
	}

	static tockenIsNull(tockenStr) {
		return !(tockenStr != null && tockenStr != "" && tockenStr.indexOf(".") > 0);
	}

	refreshACLs() {
		let user = AuthenticationService.getConnectedUser();
		this.aclService.listRightsForConnectedUser().subscribe(
			(acls) => {
				const cryptedAcls = btoa(JSON.stringify(acls));
				localStorage.setItem(AuthenticationService.KEY_ACLS, cryptedAcls);
			},
			(error) => {
				console.error("Unable de refresh ACL");
				console.error(error);
			}
		);
	}

	static readACLs(): Array<EbAclRightDTO> {
		const crypted = localStorage.getItem(AuthenticationService.KEY_ACLS);
		if (!crypted) {
			console.error("Unable to read ACL from local storage");
			return new Array<EbAclRightDTO>();
		}
		const uncrypted = atob(crypted);
		return Statique.cloneListObject(JSON.parse(uncrypted), EbAclRightDTO);
	}
}
