import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Statique } from "@app/utils/statique";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { Observable } from "rxjs";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { map } from "rxjs/operators";
import { EbTypeDocumentsDTO } from "@app/classes/dto/ebTypeDocumentsDTO";

@Injectable()
export class TypesDocumentService {
	statique = Statique;

	constructor(private http: HttpClient) {}

	getListTypeDocumentFinal(moduleNum?: number, ebDemandeNum?: number, ebCompagnieNum?: number) {
		let params = {};
		if (moduleNum) params["moduleNum"] = moduleNum;
		if (ebDemandeNum) params["ebDemandeNum"] = ebDemandeNum;
		if (ebCompagnieNum) params["ebCompagnieNum"] = ebCompagnieNum;

		return this.http.get(Statique.controllerTypeDocument + "/get-list-doc-final", {
			params: params,
		});
	}

	create(ebTypeDocuments: EbTypeDocuments): Observable<any> {
		return this.http.post(Statique.controllerTypeDocument + "/create", ebTypeDocuments);
	}

	modifier(ebTypeDocuments: EbTypeDocuments) {
		return this.http.post(Statique.controllerTypeDocument + "/modifier", ebTypeDocuments);
	}

	delete(ebTypeDocumentsNum: number) {
		return this.http.post(Statique.controllerTypeDocument + "/supprimer", ebTypeDocumentsNum);
	}

	editActivated(criteria: SearchCriteria) {
		return this.http.post(Statique.controllerTypeDocument + "/editActivated", criteria);
	}

	getListTypeDocumentByModuleNum(moduleNum: number) {
		let urlParams = "?moduleNum=" + moduleNum;
		return this.http.get(
			Statique.controllerTypeDocument + "/get-list-doc-by-module-num" + urlParams
		);
	}

	getListTypeDocDTO(criteria): Observable<Array<EbTypeDocumentsDTO>> {
		return this.http.post(Statique.controllerTypeDocument + "/getListTypeDocDTO", criteria).pipe(
			map((res: any) => {
				return Statique.cloneListObject(res, EbTypeDocumentsDTO);
			})
		);
	}

	getListTypeDoc(criteria): Observable<Array<EbTypeDocuments>> {
		return this.http.post(Statique.controllerTypeDocument + "/getListTypeDoc", criteria).pipe(
			map((res: any) => {
				return Statique.cloneListObject(res, EbTypeDocuments);
			})
		);
	}

	getListTypeDocPromise(criteria: SearchCriteria): Promise<Array<EbTypeDocuments>> {
		return new Promise((resolve) => {
			this.getListTypeDoc(criteria).subscribe((res) => resolve(res));
		});
	}
	uploadFile(uploadUrl, formData): Observable<any> {
		return this.http.post(uploadUrl, formData, { responseType: "text" });
	}

	getInvoiceTypeDocs(module, ebInvoiceNum, ebCompagnieNum): Observable<any> {
		return this.http.get(
			Statique.controllerTypeDocument +
				"/get-invoice-list-doc-final?moduleNum=" +
				module +
				"&ebInvoiceNum=" +
				ebInvoiceNum +
				"&ebCompagnieNum=" +
				ebCompagnieNum
		);
	}
}
