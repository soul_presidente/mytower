import { SearchCriteria, SearchCriteriaLite } from "@app/utils/searchCriteria";
import {HttpClient, HttpParams} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Statique } from "@app/utils/statique";
import { EbQrGroupe } from "@app/classes/qrGroupe";
import { EbQrGroupeProposition } from "@app/classes/qrGroupeProposition";
import { EbDemande } from "@app/classes/demande";

@Injectable()
export class QrConsolidationService {


	Statique = Statique;

	constructor(private http: HttpClient) {}

	getListQrGroupe(searchCriteria: any): Observable<any> {
		return this.http.post(
			Statique.controllerQrConsolidation + "/list-config-qrgroupe",
			searchCriteria
		);
	}
	updateQrGroupe(data: EbQrGroupe): Observable<any> {
		return this.http.post(Statique.controllerQrConsolidation + "/update-qrgroupe", data);
	}

	addQrGroupe(data: EbQrGroupe): Observable<any> {
		return this.http.post(Statique.controllerQrConsolidation + "/add-qrgroupe", data);
	}

	activateQrGroupe(ebQrGroupeNum: number) {
		return this.http.post(Statique.controllerQrConsolidation + "/activate-qrgroupe", ebQrGroupeNum);
	}
	refreshQrGroupe(data: SearchCriteriaLite): Observable<any> {
		return this.http.post(Statique.controllerQrConsolidation + "/refresh-qrgroupe", data);
	}
	rejectPropositionDemande(propoNum: number): Observable<any> {
		return this.http.post(
			Statique.controllerQrConsolidation + "/reject-qrgroupe-proposition",
			propoNum
		);
	}
	consolidatePropositionDemande(ebQrGroupeProposition: EbQrGroupeProposition): Observable<any> {
		return this.http.post(
			Statique.controllerQrConsolidation + "/consolidate-qrgroupe-proposition",
			ebQrGroupeProposition
		);
	}
	deleteQrGroupe(data: any): Observable<any> {
		return this.http.post(Statique.controllerQrConsolidation + "/delete-qrgroupe", data);
	}
	getListQrGroupeData(criteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerQrConsolidation + "/list-qrgroupe-data", criteria);
	}
	getListQrGroupeWithDemande(criteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerQrConsolidation + "/list-qrgroupe-with-demande",
			criteria
		);
	}

	getListDemandesInProposition(prop: EbQrGroupeProposition): Observable<any> {
		return this.http.post(Statique.controllerQrConsolidation + "/list-demandes-proposition", prop);
	}



	getListEbDemandeByQrGroup(data: any): Observable<any> {
		return this.http.post(Statique.controllerQrConsolidation + "/delete-qr-groupe-and-qr-groupe-props", data);
	}



	cancelConsolidation(ebDemandeNum: number): Observable<any> {
		return this.http.get(
			Statique.controllerQrConsolidation + "/cancel-consolidation?ebDemandeNum=" + ebDemandeNum
		);
	}

	setCommonDataInResultingRequest(groupedTr: EbDemande, checkedTrId: number, isChecked: boolean): Observable<any> {
		const params = new HttpParams({ fromObject: {
				checkedTrId : checkedTrId.toString(),
				isChecked : isChecked.toString(),
			} });
		return this.http.post(
			`${Statique.controllerQrConsolidation}/refresh-resulting-request`, groupedTr, {params: params});
	}
}
