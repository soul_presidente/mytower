import { Statique } from "@app/utils/statique";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { EbTypeGoods } from "@app/classes/EbTypeGoods";

@Injectable()
export class TypeGoodsService {
	constructor(private http: HttpClient) {}

	saveTypeGoods(typeRequest: EbTypeGoods): Observable<any> {
		return this.http.post(Statique.controllerTypeGoods + "/save-type-goods", typeRequest);
	}

	deleteTypeGoods(typeGoodsNum: number): Observable<any> {
		return this.http.delete(Statique.controllerTypeGoods + "/delete-type-goods", {
			params: {
				typeGoodsNum: String(typeGoodsNum),
			},
		});
	}

	listTypeGoods(compagnieNum: number): Observable<Array<EbTypeGoods>> {
		return this.http.get<Array<EbTypeGoods>>(Statique.controllerTypeGoods + "/list-type-goods/" + compagnieNum);
	}

	listTypeGoodsContact(): Observable<Array<EbTypeGoods>> {
		return this.http.get<Array<EbTypeGoods>>(
			Statique.controllerTypeGoods + "/list-type-goods-contact"
		);
	}

	listCodeCompany(ebCompagnieNum: number = null) {
		let params = {};
		if (ebCompagnieNum != null) {
			params["ebCompagnieNum"] = ebCompagnieNum;
		}
		return this.http.get<Array<string>>(Statique.controllerTypeGoods + "/list-code-for-company", {
			params: params,
		});
	}

	listExportControls(): Observable<any> {
		return this.http.get(
			Statique.controllerStatiqueListe + "/list-generic-enum?genericEnumKey=EXPORT_CONTROL"
		);
	}
}
