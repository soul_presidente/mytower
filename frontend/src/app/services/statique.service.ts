import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { GenericEnum } from "@app/classes/GenericEnum";
import { StatusEnum } from "@app/classes/StatusEnum";

@Injectable()
export class StatiqueService {
	statique: Statique;
	constructor(private http: HttpClient) {}

	getListPays(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/countries");
	}

	getTypeProcess(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/typeprocess");
	}

	getTransportRef(term): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/transportRef?term=" + term);
	}

	getCustomerReferenceAndTransportRef(term): Observable<any> {
		return this.http.get(
			Statique.controllerStatiqueListe + "/customerRefAndtransportRef?term=" + term
		);
	}

	getCustomerReference(term): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/customerReference?term=" + term);
	}

	getNumAwbBol(term): Observable<string[]> {
		return this.http.get<string[]>(Statique.controllerPricing + "/numAwbBol?term=" + term);
	}

	getListRegions(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/regions");
	}

	getListTypeDemande(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/Typedemande");
	}

	getListStatutTransportManagement(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listStatutTransportManagement");
	}

	getListStatutTracing(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listStatutTracing");
	}
	getListEligibility(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listEligibility");
	}
	getListCarrier(term): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listCarrier?term=" + term);
	}

	getListCostCenter(term): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listCostCenter?term=" + term);
	}

	getListFromSource(apiUrl): Observable<any> {
		return this.http.get(apiUrl);
	}
	getTermsFromSource(term, apiUrl): Observable<any> {
		let separator = "";
		if (apiUrl.indexOf("?") >= 0) separator = "&";
		else separator = "?";
		return this.http.get(apiUrl + separator + "term=" + term);
	}
	getListFromSourceByCriteria(apiUrl, criteria): Observable<any> {
		let headers = new HttpHeaders({ "Content-Type": "application/json" });
		return this.http.post(apiUrl, criteria, { headers });
	}
	getTermsFromSourceByCriteria(apiUrl, criteria): Observable<any> {
		return this.http.post(apiUrl, criteria);
	}

	getCurrentDate(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/getCurrentDate");
	}

	getListEcCurrency(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listCurrency");
	}

	getListCategorieDeviation(typeOfEvent: number = null): Observable<any> {
		let getByTypeEvent: string = typeOfEvent ? "?typeOfEvent=" + typeOfEvent : "";
		return this.http.get(
			Statique.controllerStatiqueListe + "/listCategorieDeviation" + getByTypeEvent
		);
	}

	getValueTtPsl(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/ValueTtPsl");
	}

	getListContact(term): Observable<any> {
		return this.http.post(Statique.controllerStatiqueListe + "/selectListContact", term);
	}

	getListTypeTransports(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/list-type-transports");
	}

	getListModeTransport(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listModeTransport");
	}
	getListExtractors(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listExtractors");
	}
	getListTransportIntegrateur(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listTransportIintegrateur");
	}

	getListChargeur(term): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listChargeur?term=" + term);
	}

	getListEmailParam() {
		return this.http.get(Statique.controllerStatiqueListe + "/list-email-param");
	}

	getListModule(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listModule");
	}

	getListStatusByModule(moduleNum: number): Observable<any> {
		return this.http.get(
			Statique.controllerStatiqueListe + "/listStatusByModule?moduleNum=" + moduleNum
		);
	}

	getListSavedSearchAction(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listSavedSearchAction");
	}

	getListSavedSearchActionTrigger(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listSavedSearchActionTrigger");
	}

	getListFlagIcon(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listFlagIcon");
	}

	getListBoutonAction(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listBoutonAction");
	}

	getListBoutonActionNewTarget(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listBoutonActionNewTarget");
	}

	getListBoutonActionDashboardTarget(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listBoutonActionDashboardTarget");
	}

	getListDeliveryStatus(): Observable<Array<StatusEnum>> {
		return this.http.get<Array<StatusEnum>>(
			Statique.controllerStatiqueListe + "/listStatutDelivery"
		);
	}

	getListOrderStatus(): Observable<Array<StatusEnum>> {
		return this.http.get<Array<StatusEnum>>(Statique.controllerStatiqueListe + "/listStatutOrder");
	}

	getListGenericEnum(genericEnumKey): Observable<Array<GenericEnum>> {
		let params = new HttpParams().set("genericEnumKey", String(genericEnumKey));
		return this.http
			.get(Statique.controllerStatiqueListe + "/list-generic-enum", { params: params })
			.pipe(map((res) => Statique.cloneListObject(res, GenericEnum)));
	}

	getListFuseauxHoraire(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/list-fuseaux-horaire");
	}

	getListGroupeVentilation(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listGroupeVentilation");
	}

	getListMarchandiseDangerousGood(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listMarchandiseDangerousGood");
	}
	getListMarchandiseClassGood(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listMarchandiseClassGood");
	}
	getListFilesFormats(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listFilesFormats");
	}
	consolidateDoc(criteria){
		return this.http.post(Statique.controllerStatiqueListe+"/consolidate-doc",criteria,{
			responseType: "blob",
			headers: new HttpHeaders().append("Content-Type", "application/json")});
	}
}
