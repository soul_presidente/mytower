import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from "@angular/router";
import { UserService } from "./user.service";

import { EbUser } from "@app/classes/user";
import { AccessRights, EtatUser, Modules, UserRole } from "@app/utils/enumeration";
import { AuthenticationService } from "./authentication.service";
import { Statique } from "@app/utils/statique";
import { Observable } from "rxjs";

@Injectable()
export class UserAuthGuardService implements CanActivate {
	userConnected: EbUser;
	etatUser = EtatUser;
	isLoggedIn: Observable<boolean>;

	constructor(
		private auth: AuthenticationService,
		private router: Router,
		private userService: UserService
	) {
		//this.isLoggedIn = this.auth.isLoggedInObservable;
	}

	canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		let isAuthenticated = AuthenticationService.isAuthenticated();
		if (isAuthenticated || state.url == "/inscription-chanel") {
			this.userConnected = AuthenticationService.getConnectedUser();

			let roleCond: boolean =
				!Statique.isDefined(next.data.role) || next.data.role == this.userConnected.role;
			let roleChargeur: boolean =
				!Statique.isDefined(next.data.chargeur) || this.userConnected.role == UserRole.CHARGEUR;
			let roleControlTower: boolean =
				!Statique.isDefined(next.data.controlTower) ||
				this.userConnected.role == UserRole.CONTROL_TOWER;
			let serviceCond: boolean = true;
			let adminCond: boolean =
				!Statique.isDefined(next.data.admin) || next.data.admin == this.userConnected.admin;
			let superAdminCond: boolean =
				!Statique.isDefined(next.data.superAdmin) ||
				next.data.superAdmin == this.userConnected.superAdmin;
			let adminOrSuperAdmin: boolean =
				!Statique.isDefined(next.data.adminOrSuperAdmin) ||
				this.userConnected.admin ||
				this.userConnected.superAdmin;

			let globCond =
				adminOrSuperAdmin &&
				superAdminCond &&
				adminCond &&
				roleCond &&
				serviceCond &&
				roleChargeur &&
				roleControlTower;

			if (globCond && this.hasAccess(state.url)) return true;
			else {
				this.router.navigate(["accueil"]);
				return false;
			}
		} else {
			this.router.navigate(["login"]);
			return false;
		}
	}

	hasAccess(url: string) {
		let ecModule = null;

		if (url.indexOf("/extractors") >= 0 && this.userConnected.role != UserRole.CONTROL_TOWER)
			return false;

		let path;
		for (path in Statique.moduleNamePath) {
			if (
				url.indexOf("/" + Statique.moduleNamePath[path] + "/") >= 0 ||
				url.endsWith("/" + Statique.moduleNamePath[path])
			) {
				ecModule = Modules[path];
				break;
			}
		}
		if (!ecModule) return true;

		if (!this.userConnected) {
			if (url == "/inscription-chanel") return true;
			return false;
		}

		if (ecModule == Modules.COMPLIANCE_MATRIX && this.userConnected.role != UserRole.CHARGEUR)
			return false;

		if (url.indexOf("/pricing/creation") >= 0 && this.userConnected.role == UserRole.PRESTATAIRE)
			return false;
		if (url.indexOf("/users/") >= 0 && !this.userConnected.admin && !this.userConnected.superAdmin)
			return false;

		if (!this.userConnected.listAccessRights || this.userConnected.listAccessRights.length == 0)
			return false;

		return !this.userConnected.listAccessRights.find((access) => {
			return (
				(access.ecModuleNum == ecModule && access.accessRight == AccessRights.NON_VISIBLE) ||
				(access.ecModuleNum == ecModule &&
					url.indexOf("/creation") >= 0 &&
					access.accessRight == AccessRights.VISUALISATION)
			);
		});
	}
}
