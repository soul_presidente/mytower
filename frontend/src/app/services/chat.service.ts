import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { HttpClient } from "@angular/common/http";
import { EbChat } from "@app/classes/chat";
import { Observable } from "rxjs";

@Injectable()
export class ChatService {
	constructor(private http: HttpClient) {}

	getListChat(dataSource: string): Observable<any> {
		return this.http.get(dataSource);
	}

	addChat(dataInsertion: string, chat: EbChat): Observable<any> {
		return this.http.post(dataInsertion, chat);
	}
}
