import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { HttpClient, HttpParams } from "@angular/common/http";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { EbTtCompanyPsl } from "../classes/ttCompanyPsl";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";

@Injectable()
export class ConfigPslCompanyService {
	Statique = Statique;
	constructor(private http: HttpClient) {}

	getListEbTtCompanyPslTable(criteria: SearchCriteria): Observable<Array<EbTtCompanyPsl>> {
		return this.http.post(Statique.controllerCompagniePsl + "/list-track-trace-psl", criteria).pipe(
			map((res: any) => {
				return Statique.cloneListObject(res.data, EbTtCompanyPsl);
			})
		);
	}
	getListEbTtCompanyPslTablePromise(criteria: SearchCriteria): Promise<Array<EbTtCompanyPsl>> {
		return new Promise((resolve) => {
			this.getListEbTtCompanyPslTable(criteria).subscribe((res) => resolve(res));
		});
	}
	updateEbTtCompanyPsl(ebTtCompanyPsl: EbTtCompanyPsl): Observable<EbTtCompanyPsl> {
		return this.http
			.post(Statique.controllerCompagniePsl + "/update-track-trace-psl", ebTtCompanyPsl)
			.pipe(map((res) => Statique.cloneObject(res, new EbTtCompanyPsl())));
	}
	deleteEbTtCompanyPsl(ebTtCompanyPslNum: number) {
		let params = new HttpParams().set("ebTtCompanyPslNum", String(ebTtCompanyPslNum));
		return this.http.delete(Statique.controllerCompagniePsl + "/delete-track-trace-psl", {
			params: params,
		});
	}

	getListSchemaPslTable(criteria: SearchCriteria): Observable<Array<EbTtSchemaPsl>> {
		return this.http.post(Statique.controllerCompagniePsl + "/list-schema-psl", criteria).pipe(
			map((res: any) => {
				return Statique.cloneListObject(res.data, EbTtSchemaPsl);
			})
		);
	}

	getListSchemaPslPromise(criteria: SearchCriteria): Promise<Array<EbTtSchemaPsl>> {
		return new Promise((resolve) => {
			this.getListSchemaPslTable(criteria).subscribe((res) => resolve(res));
		});
	}

	getListSchemaPsl(searchCriteria: any): Observable<any> {
		return this.http.post(Statique.controllerCompagniePsl + "/list-schema-psl", searchCriteria);
	}

	updateSchemaPsl(searchCriteria: any): Observable<any> {
		return this.http.post(Statique.controllerCompagniePsl + "/update-schemapsl", searchCriteria);
	}

	deleteSchemaPsl(id: number): Observable<any> {
		return this.http.post(Statique.controllerCompagniePsl + "/delete-schemapsl", id);
	}

	getSchemaPsl(id: number): Observable<any> {
		return this.http.get(Statique.controllerCompagniePsl + "/?ebTtCompanyPslNum=" + id);
	}
}
