import { Injectable } from "@angular/core";
import { EbMarchandise } from "@app/classes/marchandise";

@Injectable()
export class SharedDatasQualityManagemnt {
	notExpectedID: number[] = [];
	addedMarchandise: Array<EbMarchandise> = new Array<EbMarchandise>();

	insertData(id: number) {
		this.notExpectedID.unshift(id);
	}

	addMarchandise(data) {
		this.addedMarchandise.unshift(data);
	}

	getMarchandiseByNum(xEbMarchandise) {
		let marchandise = this.addedMarchandise.find((il) => il.ebMarchandiseNum == xEbMarchandise);
		return marchandise;
	}

	deleteMArchandise(xEbMarchandise) {
		for (let i = 0; i < this.addedMarchandise.length; i++) {
			if (this.addedMarchandise[i].ebMarchandiseNum == xEbMarchandise) {
				this.addedMarchandise.splice(i, 1);
				i = this.addedMarchandise.length;
			}
		}
	}

	deleteData(id: number) {
		for (let i = 0; i < this.notExpectedID.length; i++) {
			if (this.notExpectedID[i] == id) {
				this.notExpectedID.splice(i, 1);
				i = this.notExpectedID.length;
			}
		}
	}
}
