import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Statique } from "@app/utils/statique";


@Injectable()
export class NotificationService {
	constructor(private http: HttpClient) {}

	getListNotification(userNum : Number, typeNotification: Number, pageNumber: Number): Observable<any> {
		return this.http.get(Statique.controllerNotification + "/getAllNotification?"
			+`userNum=${userNum}&typeNotification=${typeNotification}&pageNumber=${pageNumber}`);
	}

	count(userNum : Number) {
		return this.http.get(Statique.controllerNotification + "/count?"+`userNum=${userNum}`);
	}

	deleteNotification(ebNotificationNum: Number) {
		return this.http.post(
			Statique.controllerNotification + "/deleteNotification",
			ebNotificationNum
		);
	}

	updateNotifNbrForUser(ebUserNum, notifType, nbr) {
		let formData = new FormData();
		formData.append("ebUserNum", ebUserNum);
		formData.append("notifType", notifType);
		formData.append("nbr", nbr);

		return this.http.post(Statique.controllerNotification + "/update-nbr-notif", formData);
	}
}
