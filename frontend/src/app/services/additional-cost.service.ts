import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { EbAdditionalCost } from "@app/classes/EbAdditionalCost";
import { Statique } from "@app/utils/statique";
import { Observable } from "rxjs";

@Injectable({
	providedIn: "root",
})
export class AdditionalCostService {
	constructor(private http: HttpClient) {}

	upsertAdditionalCost(cost: EbAdditionalCost): Observable<any> {
		return this.http.post(Statique.controllerAdditionalCosts + "/upsert-additional-costs", cost);
	}

	desactivateAdditionalCost(ebAdditionalCostNum: number): Observable<any> {
		return this.http.post(
			Statique.controllerAdditionalCosts + "/disable-additional-cost",
			ebAdditionalCostNum
		);
	}

	verifyCodeExistance(code: string, ebCompagnieNum: number): Observable<any> {
		let params = new FormData();
		params.append("codeCost", code);
		params.append("ebCompagnieNum", String(ebCompagnieNum));

		return this.http.post(Statique.controllerAdditionalCosts + "/verify-code-existance", params);
	}
}
