import { Observable } from "rxjs/internal/Observable";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Statique } from "@app/utils/statique";
import { EbInvoice } from "@app/classes/invoice";
import { SearchCriteriaFreightAudit } from "@app/utils/SearchCriteriaFreightAudit";
import { InvoiceFichierJoint } from "@app/classes/InvoiceFichierJoint";
import { InvoiceWrapper } from "@app/classes/invoiceWrapper";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";

@Injectable()
export class FreightAuditService {
	statique = Statique;
	constructor(
		private http: HttpClient,
		protected modalService: ModalService,
		protected translate: TranslateService
	) {}

	getInvoice(searchCriteriaFreightAudit: SearchCriteriaFreightAudit): Observable<any> {
		return this.http.post(
			Statique.controllerFreightAudit + "/get-invoice",
			searchCriteriaFreightAudit
		);
	}

	getListInvoiceTurboTable(searchCriteriaFreightAudit: any): Observable<any> {
		return this.http.post(
			Statique.controllerFreightAudit + "/listinvoice",
			searchCriteriaFreightAudit
		);
	}

	changeStatutInvoice(ebInvoice: EbInvoice): Observable<any> {
		return this.http.post(Statique.controllerFreightAudit + "/change-statut-invoice", ebInvoice);
	}

	listDestCountry(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/countries");
	}
	//load liststatut
	ListStatut(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listStatutFreightAudit");
	}
	//loadlist type demande
	ListTypeDemande(): Observable<any> {
		return this.http.get(Statique.controllerStatiqueListe + "/listTypeDemandeFreightAudit");
	}

	//remplir la table

	getHtmlForCarrierMemo(listInvoice: Map<number, EbInvoice>, eb_invoice_num: number, data: any, disable:string) {
		let style = "font-size: 20px;cursor: pointer;";
		if (data == null) {
			return (
				'<input class="input-show-onhover" '+disable+' type="text" value="" style="" onchange="functions.clickAddMemoCarrier(' +
				eb_invoice_num +
				',event.target)"/>'
			);
		} else {
			return (
				'<input class="input-show-onhover" '+disable+' type="text" value="' +
				data +
				'" style="" onchange="functions.clickAddMemoCarrier(' +
				eb_invoice_num +
				',event.target)"/>'
			);
		}
	}
	//remplir le memo
	getHtmlForChargeurMemo(listInvoice: Map<number, EbInvoice>, eb_invoice_num: number, data: any , disable: string) {
		let style = "font-size: 20px;cursor: pointer;";
		if (data == null) {
			return (
				'<input '+disable+' class="input-show-onhover" type="text" value="" style="" onchange="functions.clickAddMemoChargeur(' +
				eb_invoice_num +
				',event.target)"/>'
			);
		} else {
			return (
				'<input '+disable+' class="input-show-onhover" type="text" value="' +
				data +
				'" style="" onchange="functions.clickAddMemoChargeur(' +
				eb_invoice_num +
				',event.target)"/>'
			);
		}
	}

	//remplir le prix
	getHtmlForChargeurInvoicePrice(
		listInvoice: Map<number, EbInvoice>,
		eb_invoice_num: number,
		data: any,
		priceArraw: number,
		negotiatedPrice: number,
		disable: string
	) {
		let style = "font-size: 20px;cursor: pointer;";
		if (priceArraw == 1) {
			if (data == null) {
				return (
					'<span class="send-arrow fa fa-sign-out"><input '+disable+' data-gap=' +
					negotiatedPrice +
					' class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" value=" "  onchange="functions.clickApplyChargeurInvoicePrice(' +
					eb_invoice_num +
					',event.target)" onblur="functions.gap(' +
					500 +
					"," +
					600 +
					')"/></span>'
				);
			} else {
				return (
					'<span class="send-arrow fa fa-sign-out"><input '+disable+' data-gap=' +
					negotiatedPrice +
					' class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" value="' +
					data +
					'"  onchange="functions.clickApplyChargeurInvoicePrice(' +
					eb_invoice_num +
					',event.target)" onblur="functions.gap(' +
					500 +
					"," +
					600 +
					')"/></span>'
				);
			}
		}
		if (priceArraw == 2) {
			if (data == null) {
				return (
					'<span class="receive-arrow fa fa-sign-out"><input '+disable+' data-gap=' +
					negotiatedPrice +
					' class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" value=" "  onchange="functions.clickApplyChargeurInvoicePrice(' +
					eb_invoice_num +
					',event.target)" onblur="functions.gap(' +
					500 +
					"," +
					600 +
					')"/></span>'
				);
			} else {
				return (
					'<span class="receive-arrow fa fa-sign-out"><input '+disable+' data-gap=' +
					negotiatedPrice +
					' class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" value="' +
					data +
					'"  onchange="functions.clickApplyChargeurInvoicePrice(' +
					eb_invoice_num +
					',event.target)" onblur="functions.gap(' +
					500 +
					"," +
					600 +
					')"/></span>'
				);
			}
		} else {
			if (data == null) {
				return (
					"<input data-gap=" +
					negotiatedPrice +
					' class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" value=" " '+disable+' onchange="functions.clickApplyChargeurInvoicePrice(' +
					eb_invoice_num +
					',event.target)" onblur="functions.gap(' +
					500 +
					"," +
					600 +
					')"/>'
				);
			} else {
				return (
					"<input data-gap=" +
					negotiatedPrice +
					' class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value="' +
					data +
					'"  onchange="functions.clickApplyChargeurInvoicePrice(' +
					eb_invoice_num +
					',event.target)" onblur="functions.gap(' +
					500 +
					"," +
					600 +
					')"/>'
				);
			}
		}
	}
	getHtmlForCarrierInvoicePrice(
		listInvoice: Map<number, EbInvoice>,
		eb_invoice_num: number,
		data: any,
		priceArraw: number,
		negotiatedPrice: number,
		disable: string
	) {
		let style = "font-size: 20px;cursor: pointer;";

		if (priceArraw == 1) {
			if (data == null) {
				return (
					'<span class="receive-arrow fa fa-sign-out"><input data-gap=' +
					negotiatedPrice +
					' class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value=" "  onchange="functions.clickApplyCarrierInvoicePrice(' +
					eb_invoice_num +
					',event.target)"/></span>'
				);
			} else {
				return (
					'<span class="receive-arrow fa fa-sign-out"><input  data-gap=' +
					negotiatedPrice +
					' class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value="' +
					data +
					'"  onchange="functions.clickApplyCarrierInvoicePrice(' +
					eb_invoice_num +
					',event.target)"/></span>'
				);
			}
		}
		if (priceArraw == 2) {
			if (data == null) {
				return (
					'<span class="send-arrow fa fa-sign-out"><input data-gap=' +
					negotiatedPrice +
					' class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value=" "  onchange="functions.clickApplyCarrierInvoicePrice(' +
					eb_invoice_num +
					',event.target)"/></span>'
				);
			} else {
				return (
					'<span class="send-arrow fa fa-sign-out"><input data-gap=' +
					negotiatedPrice +
					' class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value="' +
					data +
					'"  onchange="functions.clickApplyCarrierInvoicePrice(' +
					eb_invoice_num +
					',event.target)"/></span>'
				);
			}
		} else if (data == null) {
			return (
				"<input " +
				disable +
				" data-gap=" +
				negotiatedPrice +
				' class="carrier-invoice-nbr-' +
				eb_invoice_num +
				'" type="text" value=" "  onchange="functions.clickApplyCarrierInvoicePrice(' +
				eb_invoice_num +
				',event.target)"/>'
			);
		} else {
			return (
				"<input " +
				disable +
				" data-gap=" +
				negotiatedPrice +
				' class="carrier-invoice-nbr-' +
				eb_invoice_num +
				'" type="text" value="' +
				data +
				'"  onchange="functions.clickApplyCarrierInvoicePrice(' +
				eb_invoice_num +
				',event.target)"/>'
			);
		}
	}
	//remplir le number
	getHtmlForChargeurInvoiceNumber(
		listInvoice: Map<number, EbInvoice>,
		eb_invoice_num: number,
		data: any,
		numberArraw: number,
		disable: string
	) {
		let style = "font-size: 20px;cursor: pointer;";

		if (numberArraw == 1) {
			if (data == null) {
				return (
					'<span class="send-arrow fa fa-sign-out"><input class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" value="" '+disable+' onchange="functions.clickApplyInvoiceNumberChargeur(' +
					eb_invoice_num +
					',event.target)"/></span>'
				);
			} else {
				return (
					'<span class="send-arrow fa fa-sign-out"><input class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value="' +
					data +
					'"  onchange="functions.clickApplyInvoiceNumberChargeur(' +
					eb_invoice_num +
					',event.target)"/></span>'
				);
			}
		}
		if (numberArraw == 2) {
			if (data == null) {
				return (
					'<span class="receive-arrow fa fa-sign-out"><input class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value="" onchange="functions.clickApplyInvoiceNumberChargeur(' +
					eb_invoice_num +
					',event.target)"/></span>'
				);
			} else {
				return (
					'<span class="receive-arrow fa fa-sign-out"><input class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value="' +
					data +
					'"  onchange="functions.clickApplyInvoiceNumberChargeur(' +
					eb_invoice_num +
					',event.target)"/></span>'
				);
			}
		} else {
			if (data == null) {
				return (
					'<input class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" value="" '+disable+' onchange="functions.clickApplyInvoiceNumberChargeur(' +
					eb_invoice_num +
					',event.target)"/>'
				);
			} else {
				return (
					'<input class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value="' +
					data +
					'"  onchange="functions.clickApplyInvoiceNumberChargeur(' +
					eb_invoice_num +
					',event.target)"/>'
				);
			}
		}
	}
	getHtmlForCarrierInvoiceNumber(
		listInvoice: Map<number, EbInvoice>,
		eb_invoice_num: number,
		data: any,
		numberArraw: number,
		disable: string
	) {
		let style = "font-size: 20px;cursor: pointer;";

		if (numberArraw == 1) {
			if (data == null) {
				return (
					'<span class="receive-arrow fa fa-sign-out"><input class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value=" "  onchange="functions.clickApplyInvoiceNumberCarrier(' +
					eb_invoice_num +
					',event.target)"/></span>'
				);
			} else {
				return (
					'<span class="receive-arrow fa fa-sign-out"><input class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value="' +
					data +
					'"  onchange="functions.clickApplyInvoiceNumberCarrier(' +
					eb_invoice_num +
					',event.target)"/></span>'
				);
			}
		}
		if (numberArraw == 2) {
			if (data == null) {
				return (
					'<span class="send-arrow fa fa-sign-out"><input class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value=" "  onchange="functions.clickApplyInvoiceNumberCarrier(' +
					eb_invoice_num +
					',event.target)"/></span>'
				);
			} else {
				return (
					'<span class="send-arrow fa fa-sign-out"><input class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value="' +
					data +
					'"  onchange="functions.clickApplyInvoiceNumberCarrier(' +
					eb_invoice_num +
					',event.target)"/></span>'
				);
			}
		} else {
			if (data == null) {
				return (
					'<input class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value=" "  onchange="functions.clickApplyInvoiceNumberCarrier(' +
					eb_invoice_num +
					',event.target)"/>'
				);
			} else {
				return (
					'<input class="carrier-invoice-nbr-' +
					eb_invoice_num +
					'" type="text" '+disable+' value="' +
					data +
					'"  onchange="functions.clickApplyInvoiceNumberCarrier(' +
					eb_invoice_num +
					',event.target)"/>'
				);
			}
		}
	}

	updateStatut(ebInvoice: EbInvoice) {
		return this.http.post(Statique.controllerFreightAudit + "/updateStatutFreightAudit", ebInvoice);
	}

	ListInvoiceFichierJoint(eb_invoice_num: number): Observable<any> {
		console.log(eb_invoice_num);
		return this.http.post(
			Statique.controllerFreightAudit + "/getListEbInvoiceByEbInvoiceNum",
			eb_invoice_num
		);
	}

	addFacture(ebDemandeNum: number): Observable<any> {
		console.log(+ebDemandeNum);
		return this.http.post(Statique.controllerFreightAudit + "/generate-invoice", ebDemandeNum);
	}

	//upload
	deleteFichierJoint(ebInvoiceFichierJoint: number): Observable<any> {
		console.log(ebInvoiceFichierJoint);
		return this.http.post(
			Statique.controllerFreightAudit + "/delete-ebInvoiceFichierJoint",
			ebInvoiceFichierJoint
		);
	}

	getDemande(ebDemandeNum: number): Observable<any> {
		return this.http.post(Statique.controllerFreightAudit + "/getDemande", ebDemandeNum);
	}

	deleteNbr(ebInvoiceNum: number): Observable<any> {
		return this.http.post(Statique.controllerFreightAudit + "/deleteNbr", ebInvoiceNum);
	}

	//pour le tag
	setTag(ebInvoiceFichierJointNum: number, tag: String): Observable<any> {
		let inv = new InvoiceFichierJoint();
		inv.ebInvoiceFichierJointNum = ebInvoiceFichierJointNum;
		inv.tag = tag;
		return this.http.post(Statique.controllerFreightAudit + "/setTag", inv);
	}

	getListActionType(): Observable<any> {
		return this.http.get(Statique.controllerFreightAudit + "/list-action-type");
	}

	getListStatus(): Observable<any> {
		return this.http.get(Statique.controllerFreightAudit + "/list-status");
	}

	updateListCostCategorieAndPrice(invoice: EbInvoice): Observable<any> {
		return this.http.post(
			Statique.controllerFreightAudit + "/updateListCostCategorieAndPrice",
			invoice
		);
	}

	updateListTypeDocuments(invoice: EbInvoice): Observable<any> {
		return this.http.post(Statique.controllerFreightAudit + "/updateListTypeDocuments", invoice);
	}

	getListDocumentsInvoice(ecModule, ebInvoiceNum, ebUserNum): Observable<any> {
		return this.http.get(
			Statique.controllerStatiqueListe +
				"/listDocumentsInvoice?module=" +
				ecModule +
				"&ebInvoiceNum=" +
				ebInvoiceNum +
				"&ebUserNum=" +
				ebUserNum
		);
	}

	// prettier-ignore
	saveFlags(ebInvoiceNum: number, newFlags: string): Observable<any> {
    return this.http.get(
      Statique.controllerFreightAudit +
      "/saveFlags?ebInvoiceNum=" + ebInvoiceNum +
      "&newFlags=" + newFlags
    );
  }

	allAction(invoiceWrap: InvoiceWrapper): Observable<any> {
		return this.http.post(Statique.controllerFreightAudit + "/all-action", invoiceWrap);
	}
}
