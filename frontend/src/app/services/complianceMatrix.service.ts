import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { Statique } from "@app/utils/statique";
import { EbDestinationCountry } from "@app/classes/destinationCountry";
import { EbCombinaison } from "@app/classes/combinaison";
import { Observable } from "rxjs";

@Injectable()
export class ComplianceMatrixService {
	constructor(private http: HttpClient) {}

	getDestinationCountry(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerComplianceMatrix + "/get-destination-country",
			searchCriteria
		);
	}

	addDestinationCountry(destinationCountry: EbDestinationCountry): Observable<any> {
		return this.http.post(
			Statique.controllerComplianceMatrix + "/add-destination-country",
			destinationCountry
		);
	}

	listEbDestinationCountry(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerComplianceMatrix + "/list-destination-country",
			searchCriteria
		);
	}

	editDestinationCountry(destinationCountry: EbDestinationCountry): Observable<any> {
		destinationCountry.createdBy = null;
		return this.http.post(
			Statique.controllerComplianceMatrix + "/edit-destination-country",
			destinationCountry
		);
	}

	deleteDestinationCountry(destinationCountry: EbDestinationCountry): Observable<any> {
		return this.http.post(
			Statique.controllerComplianceMatrix + "/delete-destination-country",
			destinationCountry
		);
	}

	exportDestinationCountry(ebDestinationCountryNum: number) {
		window.location.replace(
			Statique.controllerExport + "/ficheDestinationCountry/" + ebDestinationCountryNum
		);
	}

	addCombinaison(combinaison: EbCombinaison): Observable<any> {
		return this.http.post(Statique.controllerComplianceMatrix + "/add-combinaison", combinaison);
	}

	getListCombinaison(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(
			Statique.controllerComplianceMatrix + "/list-combinaison",
			searchCriteria
		);
	}

	deleteCombinaison(ebCombinaison: EbCombinaison): Observable<any> {
		return this.http.post(
			Statique.controllerComplianceMatrix + "/delete-combinaison",
			ebCombinaison
		);
	}

	getAllDestinationCountryByCompagnie(searchCriteria : SearchCriteria){
		return this.http.post(
			Statique.controllerComplianceMatrix + "/list-destination-country-by-company",
			searchCriteria	
		);
	}
}
