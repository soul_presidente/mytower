import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from "@app/utils/searchCriteria";

@Injectable({
	providedIn: "root",
})
export class GlobalService {
	Statique = Statique;
	constructor(private httpClient: HttpClient) {}

	getStatiqueList(url: string): Observable<any> {
		return this.httpClient.get(url);
	}

	runAction(url: string, data: any): Observable<any> {
		return this.httpClient.post(url, data);
	}

	export(searchCriteria: SearchCriteria) {
		return this.httpClient.post(Statique.controllerUnit + "/export-unit", searchCriteria, {
			responseType: "blob",
		});
	}

	exportConsolider(idDemande: number) {
		return this.httpClient.post(Statique.controllerUnit + "/export-consolidation", idDemande, {
			responseType: "blob",
		});
	}

	exportUnitConnsoliderAndPSLSchema(idDemande: number) {
		return this.httpClient.post(Statique.controllerUnit + "/export-consolidation", idDemande, {
			responseType: "blob",
		});
	}

	getSavedFormResultCount(url: string, params: any) {
		return this.httpClient.post(url, params);
	}
}
