import { Injectable } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { DataRecoveryParam } from "@app/classes/DataRecoveryParam";

@Injectable({
	providedIn: "root",
})
export class DataRecoveryService {
	constructor(private http: HttpClient) {}

	applyCompanyRecovery(specificDataToSend: DataRecoveryParam): Observable<any> {
		return this.http.post(Statique.controllerDataRecovery + "/generate-data", specificDataToSend);
	}

	applyCompanyRecoverySEvent(specificDataToSend: DataRecoveryParam): Promise<boolean> {
		return new Promise((resolve, reject) => {
			let params = "";
			params += "compagnies=" + specificDataToSend.compagnies.join(",");
			params += "&dataType=" + specificDataToSend.dataType.join(",");

			let source = new EventSource(Statique.controllerDataRecovery + "/generate-data?" + params);

			source.addEventListener("open", (message) => {
				console.log("Data recovery started.");
			});
			source.addEventListener("error", (e: any) => {
				console.error("Data recovery failed.");

				resolve(false);
			});
			source.addEventListener("message", (message) => {
				console.log(message);
				resolve(true);
			});
		});
	}

	sendGlobalRecovery(globalDataToSend: string) {
		console.log("this the global recovery method from the service ");
		console.log(globalDataToSend);
	}
}
