import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {PrMtcMappingParam} from "@app/classes/mtc/pr-mtc-mapping-param";
import {Statique} from "@app/utils/statique";

@Injectable({
	providedIn: 'root'
})
export class MtcMappingParamService {

	constructor(private http: HttpClient) {
	}

	getListMappingParamByConfigCode(configCode: string): Observable<PrMtcMappingParam[]> {
		return this.http
			.get<PrMtcMappingParam[]>(`${Statique.controllerMtcMappingParam}/by-code/${configCode}`);
	}

	save(params: PrMtcMappingParam[]) {
		return this.http
			.post<PrMtcMappingParam[]>(Statique.controllerMtcMappingParam, params);
	}
}
