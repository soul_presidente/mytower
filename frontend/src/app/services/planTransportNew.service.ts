import { Statique } from "@app/utils/statique";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { PlanTransportNew } from "@app/classes/PlanTansportNew";
import { SearchCriteria } from '@app/utils/searchCriteria';

@Injectable()
export class PlanTransportNewService {
	constructor(private http: HttpClient) {}

	getListPlanningTransport(searchCriteria: SearchCriteria): Observable<any> {
		return this.http.post(Statique.controllerPlanTransportNew + "/list-plan-transport-table", searchCriteria);
	}
	deleteLoadPlanning(planTransportNew: PlanTransportNew): Observable<any> {
		return this.http.post(
			Statique.controllerPlanTransportNew + "/delete",
			planTransportNew
		);
	}
	saveFlags(ebPlanTransportNum: number, newFlags: string): Observable<any> {
		return this.http.get(
		  Statique.controllerPlanTransportNew +
			"/saveFlags?ebPlanTransportNum=" + ebPlanTransportNum +
			"&newFlags=" + newFlags
		);
	  }
	save(planTransportNewFields: any): Observable<any>{
		return this.http.post(
			Statique.controllerPlanTransportNew + "/save",
			planTransportNewFields
		);
	}
}
