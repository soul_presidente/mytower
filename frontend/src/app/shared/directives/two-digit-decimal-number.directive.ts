import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[appTwoDigitDecimalNumber]'
})
export class TwoDigitDecimalNumberDirective {

	private el: HTMLInputElement;

  constructor(private elementRef: ElementRef) {
	  this.el = this.elementRef.nativeElement;
  }

	ngOnInit() {
		this.el.value = new Number(this.el.value).toFixed(2);
	}


	@HostListener("blur", ["$event.target.value"])
	onBlur(value) {
		this.el.value = new Number(this.el.value).toFixed(2);
	}



}
