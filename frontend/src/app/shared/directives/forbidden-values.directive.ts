import { Directive, Input } from "@angular/core";
import { NG_VALIDATORS, Validator, AbstractControl } from "@angular/forms";

@Directive({
	selector: "[forbiddenValues]",
	providers: [{ provide: NG_VALIDATORS, useExisting: ForbiddenValuesDirective, multi: true }],
})
export class ForbiddenValuesDirective implements Validator {
	@Input("forbiddenValues")
	forbiddenValues: Array<any>;

	validate(c: AbstractControl) {
		if (
			c.value != null &&
			this.forbiddenValues != null &&
			this.forbiddenValues.filter((v) => v === c.value).length > 0
		) {
			return { invalid: true };
		}
		return null;
	}
}
