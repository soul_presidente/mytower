import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { EbAdresse } from "@app/classes/adresse";
import { Jour } from "@app/classes/jour";
import { Statique } from "@app/utils/statique";
import { EtatHoraire } from "@app/utils/enumeration";
import { StatiqueService } from "@app/services/statique.service";
import { EbUser } from "@app/classes/user";
import { CompagnieService } from "@app/services/compagnie.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import SingletonStatique from "@app/utils/SingletonStatique";
import { Eligibility } from "@app/classes/eligibility";
import { EbEntrepotDTO } from '@app/classes/dock/EbEntrepotDTO';

import { DockManagementService } from "@app/services/dock-management.service";


@Component({
	selector: "app-form-adresse",
	templateUrl: "./form-adresse.component.html",
	styleUrls: ["./form-adresse.component.scss"],
})
export class FormAdresseComponent implements OnInit {
	@Input("listAdresse")
	listAdresse: Array<EbAdresse>;
	@Input("adresse")
	adresse: EbAdresse;
	@Input("userConnected")
	userConnected: EbUser;
	@Input("displayCancelButton")
	displayCancelButton: boolean = false;
	@Input("enableAdvancedOpeningHours")
	enableAdvancedOpeningHours: boolean = false;

	@Output()
	validateAdd: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	validateEdit: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	cancel: EventEmitter<any> = new EventEmitter<any>();

	Jour = Jour;
	Statique = Statique;
	EtatHoraire = EtatHoraire;
	listPays: any;
	listEtablissement: any;
	listZone: any;
	listEligibility: Array<Eligibility> = new Array<Eligibility>();

	criteriaUser: SearchCriteria = new SearchCriteria();
	rootZone: EbPlZoneDTO = new EbPlZoneDTO();
	listEntrepot: Array<EbEntrepotDTO> = null;

	constructor(
		protected statiqueService?: StatiqueService,
		protected compagnieService?: CompagnieService,
		protected dockManagerService? : DockManagementService,
		protected transportationPlanService?: TransportationPlanService
	) {}

	ngOnInit() {
		(async () => (this.listPays = await SingletonStatique.getListEcCountry()))();
		this.transportationPlanService.listZone(new SearchCriteria()).subscribe((data) => {
			this.listZone = data;
			this.listZone.forEach((zone) => {
				zone.label = zone.designation ? zone.ref + " (" + zone.designation + ")" : zone.ref;
			});
		});
		this.statiqueService.getListEligibility().subscribe((data) => {
			this.listEligibility = data;
		});
		var listEtablissementCritera = new SearchCriteria();
		listEtablissementCritera.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.compagnieService
			.getListEtablisementCompagnie(listEtablissementCritera)
			.subscribe((data) => {
				this.listEtablissement = data;
			});

		this.criteriaUser.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		if (this.adresse.zones && this.adresse.zones.length > 0) {
			// (adding each zone label) label attr is used for displaying purposes
			this.adresse.zones.forEach((zone) => {
				zone["label"] = zone.designation ? zone.ref + " (" + zone.designation + ")" : zone.ref;
			});
		}
		this.rootZone.ebZoneNum = -1;
		this.dockManagerService.getListEntrepot().subscribe((data)=>{
			this.listEntrepot =data;
		})
	}

	fireAdd(event: any) {
		this.validateAdd.next({
			event: event,
			data: this.adresse,
		});
	}

	fireEdit(event: any) {
		this.validateEdit.next({
			event: event,
			data: this.adresse,
		});
	}

	fireCancel(event: any) {
		this.cancel.next(event);
	}

	private getZoneDisplayString(zone?: EbPlZoneDTO): string {
		if (zone) {
			if (zone.designation) {
				return zone.ref + " (" + zone.designation + ")";
			} else {
				return zone.ref;
			}
		} else {
			return "";
		}
	}

	changeModel(form: any) {
		setTimeout(
			function() {
				this.checkFields(form);
			}.bind(this),
			10
		);
	}

	checkFields(form: any) {
		var findOverlap = false;

		this.listAdresse.some((aAdresse, index, array) => {
			if (
				aAdresse.reference &&
				this.adresse.reference &&
				aAdresse.ebAdresseNum !== this.adresse.ebAdresseNum
			) {
				if (
					this.adresse.reference.trim().toLowerCase() === aAdresse.reference.trim().toLowerCase()
				) {
					findOverlap = true;
					return true;
				}
			}
		});

		if (findOverlap) {
			form.controls["reference"].setErrors("overlap", true);
		} else if (this.adresse.reference) {
			form.controls["reference"].setErrors(null);
		}
	}

	updateDefaultZone() {
		if (this.adresse.zones.length == 0) this.adresse.defaultZone = null;
		else {
			let rootZone: EbPlZoneDTO = this.adresse.zones.find((zone) => zone.ebZoneNum == -1);

			if (rootZone) {
				// check if all
				this.adresse.zones = this.listZone;
			} else if (this.adresse.defaultZone) {
				// check if default value is removed
				let defaultZone = this.adresse.zones.find(
					(zone) => zone.ebZoneNum == this.adresse.defaultZone.ebZoneNum
				);
				if (!defaultZone) this.adresse.defaultZone = null;
			}
		}
	}
}
