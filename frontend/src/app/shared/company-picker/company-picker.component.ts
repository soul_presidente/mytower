import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { ConnectedUserComponent } from "../connectedUserComponent";
import { CompagnieService } from "@app/services/compagnie.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbCompagnie } from "@app/classes/compagnie";
import { UserService } from "@app/services/user.service";
import { CostCenterService } from "@app/services/cost-center.service";
import { EbCostCenter } from "@app/classes/costCenter";

@Component({
	selector: "app-company-picker",
	templateUrl: "./company-picker.component.html",
	styleUrls: ["./company-picker.component.scss"],
})
export class CompanyPickerComponent extends ConnectedUserComponent implements OnInit {
	searchCriteria = new SearchCriteria();
	listEbCompagnie: Array<EbCompagnie> = new Array<EbCompagnie>();
	selectConfig: number;
	userConnectedCompagnie: EbCompagnie;
	@Output()
	compagnieNum: EventEmitter<number> = new EventEmitter();

	constructor(
		protected userService: UserService,
		protected compagnieService: CompagnieService,
		protected costCenterService: CostCenterService
	) {
		super();
	}

	ngOnInit() {
		this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
		this.userConnectedCompagnie = this.userConnected.ebCompagnie;
		this.selectConfig = this.userConnectedCompagnie.ebCompagnieNum;
		this.searchCriteria.contact = true;
		this.userService
			.getUserContactsByCompagnie(this.searchCriteria)
			.subscribe((data: Array<EbCompagnie>) => {
				this.listEbCompagnie = data;
				this.listEbCompagnie = this.listEbCompagnie.concat(this.userConnectedCompagnie);
			});
	}

	selectCompany() {
		this.compagnieNum.emit(this.selectConfig);
	}
}
