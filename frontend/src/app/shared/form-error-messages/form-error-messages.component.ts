import { Component, Input } from "@angular/core";

/*
 Displays errors in an ngModel Reference if defined
 either way show the component after a condition "vIf" is met

 @param ShowCondition : Boolean, the condition to show the error message (Optional if NgModelRef is set)
 @param NgModelRef: NgModel Reference (Optional if ShowCondition is defined)
 */
@Component({
	selector: "app-form-error-messages",
	templateUrl: "./form-error-messages.component.html",
	styleUrls: ["./form-error-messages.component.scss"],
})
export class FormErrorMessagesComponent {
	@Input() ShowCondition: boolean = false;
	@Input() NgModelRef: any;
}
