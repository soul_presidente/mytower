import { debounceTime, switchMap, distinctUntilChanged } from "rxjs/operators";
import { Observable } from "rxjs";
import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { ConnectedUserComponent } from "../connectedUserComponent";
import { StatiqueService } from "@app/services/statique.service";
import { Statique, keyValue } from "@app/utils/statique";
import { TranslateService } from "@ngx-translate/core";
import {TypeRequestService} from "@app/services/type-request.service";
@Component({
	selector: "app-old-autocomplete",
	templateUrl: "./old-autocomplete.component.html",
	styleUrls: ["./old-autocomplete.component.css"],
})
export class OldAutocompleteComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	sMultiple: string;
	@Input()
	placeholder: string;
	@Input()
	srcTypeahead: string;
	@Input()
	srcListItems: string;
	@Input()
	name: string;
	@Input()
	defaultValue: any;
	@Input()
	fetchAttribute: string;
	@Input()
	fetchValue: any;
	@Input()
	options: any;
	@Input()
	listItems: any;
	@Input()
	groupByAttr?: string;
	@Input()
	fetchGroupByAttribute?: string;
	@Input()
	hasCountryTemplate?: boolean;
	@Input()
	hasNoAttribute?: boolean;
	@Input()
	translatePrefix: string;

	@Input()
	withCriteria?: boolean;
	@Input()
	addTag?: any;

	@Input()
	criteria?: SearchCriteria;
	@Input()
	includeUserCompanyNum?: boolean;
	@Input()
	includeUserNum?: boolean;

	@Input()
	isDisabled: boolean = false;

	@Input()
	selectedItem: any;

	@Output()
	selectedItemChange: EventEmitter<any> = new EventEmitter<any>();

	public multiple: boolean;

	public listTermsTypeahead: any;

	typeaheadEvent: EventEmitter<String> = new EventEmitter<string>();
	typeaheadEventCustom: EventEmitter<String> = new EventEmitter<string>();
	isLoading: boolean = false;
	currentSearch: string = null;
	currentList: any = null;
	loading = false;

	constructor(
		protected cd: ChangeDetectorRef,
		private statiqueService: StatiqueService,
		protected translate: TranslateService,
		private typeOfRequestService: TypeRequestService
	) {
		super();
	}

	ngOnInit() {
		this.multiple = this.sMultiple == "true";
		if (this.defaultValue) {
			if (this.multiple) this.selectedItem = JSON.parse(this.defaultValue);
			else this.selectedItem = this.defaultValue.length > 0 ? JSON.parse(this.defaultValue)[0] : "";
		}

		if (!this.defaultValue && this.srcTypeahead) this.srcListItems = null;

		this.getListItems();

		this.addTagFn(name);

		this.setTypeahead();

		if (!this.srcListItems && !this.srcTypeahead) {
			this.currentList = this.listItems;
			this.setTypeaheadCustom();
		}
	}

	getListItems() {
		if (this.srcListItems) {
			this.getListFromSource(this.srcListItems).subscribe(
				function(res) {
					this.listItems = res;

					if (this.translatePrefix) {
						this.listItems.forEach((item) => {
							item[this.fetchAttribute] = this.translate.instant(
								this.translatePrefix + item[this.fetchAttribute]
							);
						});
					}
				}.bind(this)
			);

			return true;
		} else if (this.listItems) return true;
		else {
			return this.getListWithoutSource();
		}

		return false;
	}

	addTagFn(name) {
		return name;
	}

	setTypeahead() {
		if (!this.srcTypeahead) return false;

		this.typeaheadEvent
			.pipe(
				distinctUntilChanged(),
				debounceTime(700)
			)
			.pipe(switchMap((term) => this.getTermsFromSource(term, this.srcTypeahead)))
			.subscribe(
				(data) => {
					this.cd.markForCheck();
					this.listItems = data;
					this.isLoading = false;
				},
				(err) => {
					this.listItems = [];
					this.isLoading = false;
				}
			);

		return true;
	}

	setTypeaheadCustom() {
		this.typeaheadEventCustom
			.pipe(distinctUntilChanged())
			.pipe(
				switchMap((term: string) => this.getEmptyGenericItem(term))
			)
			.subscribe(
				(data) => {
					this.cd.markForCheck();
					this.listItems = data;
					this.isLoading = false;
				},
				(err) => {
					this.listItems = [];
					this.isLoading = false;
				}
			);

		return true;
	}

	getListFromSource(apiUrl) {
		if (this.withCriteria) {
			if (!this.criteria) {
				this.criteria = new SearchCriteria();
			}

			if(apiUrl.indexOf("api/type-request") > 0 && this.userConnected.rolePrestataire)
			{
				return this.typeOfRequestService.listTypeRequestsContact()
			}
			this.criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			this.criteria.connectedUserNum = this.userConnected.ebUserNum;
			return this.statiqueService.getListFromSourceByCriteria(apiUrl, this.criteria);
		}
		return this.statiqueService.getListFromSource(apiUrl);
	}

	getListWithoutSource() {
		if (
			this.name === "acknoledgeByCt" ||
			this.name === "eligibleForConsolidation" ||
			this.name === "leg1TransitStatus" ||
			this.name === "leg2TransitStatus" ||
			this.name === "insurance" ||
			this.name === "late"
		) {
			this.listItems = Statique.yesOrNoOrEmptyWithNumberKey;

			// Apply translations of Yes/No value
			this.listItems.forEach((item: keyValue) => {
				if (item.value) {
					item.value = this.translate.instant(item.value);
				}
			});

			return true;
		}
		return false;
	}

	getTermsFromSource(term, apiUrl) {
		// ne pas supprimer le code commenter ici, ça pourrait servir par la suite

		this.currentSearch = term;
		this.listItems = [];

		if (!term || term.trim().length < 2) {
			// this.currentSearch = term;
			// this.currentList = [];
			return new Observable<any>((obs) => obs.next([]));
		}

		this.isLoading = true;
		// let oldSearch = this.currentSearch;
		// this.currentList = this.listItems;

		// if(oldSearch && term.indexOf(oldSearch) >= 0 && this.currentList && this.currentList.length){
		// 	return new Observable<any>(observer => {
		// 		// filter search in local
		// 		let newList = this.currentList.filter(it => it[this.fetchValue].indexOf(term) >= 0);
		// 		// remove dupplicates
		// 		newList = newList.filter((el, i, a) => i === a.indexOf(el));
		// 		observer.next(newList);
		// 	})
		// }

		if (this.withCriteria) {
			//let criteria = new SearchCriteria();
			if (!this.criteria) {
				this.criteria = new SearchCriteria();
			}
			this.criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			this.criteria.connectedUserNum = this.userConnected.ebUserNum;
			this.criteria.searchterm = term;

			return this.statiqueService.getTermsFromSourceByCriteria(apiUrl, this.criteria);
		}
		return this.statiqueService.getTermsFromSource(term, apiUrl);
	}

	get notFoundText(): string {
		if ((!this.currentSearch || this.currentSearch.length < 2) && this.srcTypeahead) {
			return this.translate.instant("ADVANCED_SEARCH.MIN_CHAR");
		}
		return this.translate.instant("ADVANCED_SEARCH.NO_ITEM_FOUND");
	}

	getEmptyGenericItem(term: string): any {

		if (!term && term.length <= 0) {
			return new Observable<any>((observer) => observer.next([]));
		}
		term = term.trim().toLowerCase();

		if(this.currentList && this.currentList.length){
			return new Observable<any>(observer => {
				let newList = this.currentList.filter(it => it[this.fetchAttribute].toLowerCase().indexOf(term) >= 0);
				// remove dupplicates
				newList = newList.filter((el, i, a) => i === a.indexOf(el));
				observer.next(newList);
			})
		}
	}

	selectItem() {
		if (this.currentList) {
			this.listItems = this.currentList.filter(it => !this.selectedItem.includes(it[this.fetchValue]));	
		}
		this.selectedItemChange.emit(this.selectedItem);
	}
}
