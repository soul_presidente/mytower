import { ConnectedUserComponent } from "../connectedUserComponent";
import { OnInit, Input } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { Modules } from "@app/utils/enumeration";
import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";

/**
 * Base component for Creation WF component<br/>
 * Don't inherit directly from this expect for some special cases, use BaseStep or BaseContent component
 */
export abstract class CreationFormStepBaseComponent extends ConnectedUserComponent
	implements OnInit {
	@Input()
	selected: boolean = true;

	// TODO : Determine if read only feature will be maintained or not here
	@Input()
	readOnly: boolean = false;
	optionsDisplayed: boolean = false;

	@Input()
	draftMode: boolean = false;

	module: number;

	Statique = Statique;
	Modules = Modules;

	constructor(protected authenticationService: AuthenticationService, protected router: Router) {
		super(authenticationService);
	}

	ngOnInit() {
		this.module = Statique.getModuleNumFromUrl(this.router.url);
	}

	/**
	 * Check if the data filled is valid
	 */
	public get valid(): boolean {
		return this.checkValidity();
	}

	protected abstract checkValidity(): boolean;

	/**
	 * Return if the parent should show options button or not for this step
	 */
	public get hasOptions(): boolean {
		return this.stepHasOptions();
	}

	protected abstract stepHasOptions(): boolean;

	public toggleDisplayOptions() {
		this.optionsDisplayed = !this.optionsDisplayed;
	}
}
