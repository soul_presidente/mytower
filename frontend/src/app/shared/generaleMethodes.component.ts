import { Injectable } from "@angular/core";

@Injectable()
export class generaleMethodes {
	constructor() {}

	panelCollapse(panel, event) {
		panel.classList.toggle("hidden");
		event.classList.toggle("panel-collapsed");
	}

	panelFullScreen(panel) {
		panel.classList.toggle("full-screen");
	}
}
