import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { Statique } from "@app/utils/statique";
import { EbDemande } from "@app/classes/demande";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ConnectedUserComponent } from "../connectedUserComponent";
import { EbInvoice } from "@app/classes/invoice";
import { EbUser } from "@app/classes/user";
import { EbChat } from "@app/classes/chat";

@Component({
	selector: "app-pop-up-details-demande",
	templateUrl: "./pop-up-details-demande.component.html",
	styleUrls: ["../modal/modal.component.scss", "./pop-up-details-demande.component.css"],
})
export class PopUpDetailsDemandeComponent extends ConnectedUserComponent implements OnInit {
	Statique = Statique;

	@Input()
	ebDemande: EbDemande;
	@Input()
	module: number;
	@Input()
	isForEdit: boolean = false;
	@Input()
	exEbDemandeTransporteur: ExEbDemandeTransporteur;
	@Input()
	isCotationVisible: boolean = true;
	@Input()
	isCostInvoiceVisible: boolean = false;
	@Input()
	public nbColListing: string = "col-md-6";
	@Input()
	public nbColListingGlobal: string = "col-md-12";
	@Output()
	EditEventChange = new EventEmitter<boolean>();
	@Input()
	ebInvoice: EbInvoice = new EbInvoice();
	@Input()
	_userConnected: EbUser;
	@Output()
	confirmEventHandler: EventEmitter<Object> = new EventEmitter();
	@Output()
	updateChatHandlerEvent: EventEmitter<Object> = new EventEmitter();

	@Output()
	InvoiceEventHandler: EventEmitter<Object> = new EventEmitter();

	constructor(private modalService: NgbModal) {
		super();
	}

	ngOnInit() {
		if (this._userConnected != null && this._userConnected.isUnknownUser)
			this.userConnected = this._userConnected;
	}

	confirmModalAddEvent(data: Object) {
		if (this.ebInvoice)
			if (data && data["invoice"]) this.recupInvoiceModalAddEvent(data["invoice"]);
	}

	recupInvoiceModalAddEvent(Invoice: EbInvoice) {
		this.InvoiceEventHandler.emit({ invoice: Invoice });
	}

	closePopUp(status: boolean) {
		this.EditEventChange.emit(status);
	}
	updateChatHandler(event: EbChat) {
		this.updateChatHandlerEvent.emit(event);
	}
}
