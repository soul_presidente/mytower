import { ElementRef, Renderer2, ViewChild } from "@angular/core";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { Statique } from "@app/utils/statique";
import { DataTableDirective } from "angular-datatables";

export abstract class BasicGenericTableScreen {
	@ViewChild("panel", { static: false })
	panelEl: ElementRef;
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	dataTableConfig: DataTableConfig;

	constructor(protected renderer: Renderer2) {
		this.dataTableConfig = new DataTableConfig();
	}

	initDatatable() {
		this.dataTableConfig.dtTrigger.next();
		this.moveDatatableFilter();
	}

	async moveDatatableFilter() {
		let filterEl;
		await Statique.waitForTimemout(100);

		let collapsContainer = this.panelEl.nativeElement;

		let timer = 0;
		while (
			!(filterEl = collapsContainer.getElementsByClassName("dataTables_filter")[0]) &&
			timer < 10000
		) {
			await Statique.waitForTimemout(200);
			timer += 200;
		}

		if (filterEl) {
			this.renderer.addClass(filterEl, "dtFilter_MoveHeader_SimpleAdd");

			let fieldEl = (filterEl = (filterEl = filterEl.getElementsByTagName("input"))
				? filterEl[0]
				: null);
			if (fieldEl) {
				this.renderer.setAttribute(fieldEl, "placeholder", "Search");
			}
		}
	}

	rerenderTable() {
		this.dataTableConfig.rerender(this.dtElement);
		this.moveDatatableFilter();
	}
}
