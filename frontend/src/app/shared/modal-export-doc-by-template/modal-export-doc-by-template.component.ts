import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { DocumentTemplatesService } from "@app/services/document-templates.service";
import { EbTemplateDocumentsDTO } from "@app/classes/dto/ebTemplateDocumentsDTO";
import { TemplateGenParamsDTO } from "@app/classes/TemplateGenParamsDTO";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { TranslateService } from "@ngx-translate/core";
import { MessageService } from "primeng/api";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { Modules, TemplateModels } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";

@Component({
	selector: "app-modal-export-doc-by-template",
	templateUrl: "./modal-export-doc-by-template.component.html",
	styleUrls: ["./modal-export-doc-by-template.component.scss"],
})
export class ModalExportDocByTemplateComponent implements OnInit {
	loading = false;
	templates: Array<EbTemplateDocumentsDTO> = [];
	templatesLoaded = false;

	params: TemplateGenParamsDTO = new TemplateGenParamsDTO();

	@Input()
	itemNum: number;

	@Input()
	module: number;

	@Input()
	showGlobals: boolean;

	@Input()
	listTypeDocuments = new Array<EbTypeDocuments>();
	listTypeDocumentsFiltered = new Array<EbTypeDocuments>();

	@Output()
	closePopup = new EventEmitter<ModalExportDocByTemplateComponent.Result>();

	constructor(
		protected docTemplateService: DocumentTemplatesService,
		protected translate: TranslateService,
		protected messageService: MessageService
	) {}

	ngOnInit() {
		this.getListDocTemplates();
	}

	getListDocTemplates() {
		let criteria: SearchCriteria = new SearchCriteria();
		criteria.orderedColumn = "libelle";
		criteria.showGlobals = this.showGlobals;
		criteria.ascendant = true;
		this.docTemplateService.getListDocumentTemplates(criteria).subscribe((res) => {
			this.templates = res.filter((t) => t.module.filter((m) => m === this.module).length > 0);
			this.templatesLoaded = true;
		});
	}

	modalExportPdfConfirm(event: any) {
		this.loading = true;
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		if (this.module === Modules.PRICING || this.module === Modules.TRANSPORT_MANAGEMENT) {
			this.params.templateModelNum = TemplateModels.PricingItem;
		} else if (this.module === Modules.ORDER_MANAGEMENT) {
			this.params.templateModelNum = TemplateModels.Order;
		} else if (this.module === Modules.DELIVERY_MANAGEMENT) {
			this.params.templateModelNum = TemplateModels.Delivery;
		}

		this.params.templateModelParams = [];
		this.params.templateModelParams.push(String(this.itemNum));

		this.params.generatedFileName =
			"Export" + (this.params.selectedFormat === 0 ? ".pdf" : ".docx");

		this.docTemplateService.fireDocumentGeneration(this.params).subscribe(
			(res) => {
				this.loading = false;
				requestProcessing.afterGetResponse(event);

				if (this.params.downloadChecked) {
					this.doDownload(res, this.params.generatedFileName);
				}

				this.closePopup.emit(new ModalExportDocByTemplateComponent.Result(true, this.params, res));
			},
			(err) => {
				this.loading = false;
				requestProcessing.afterGetResponse(event);

				if (err.headers.has("ERROR_NAME") && err.headers.get("ERROR_NAME") === "INVALID_TEMPLATE") {
					this.messageService.add({
						severity: "error",
						summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
						detail: this.translate.instant("DOCUMENT.ERROR.TEMPLATE"),
					});
				} else {
					this.messageService.add({
						severity: "error",
						summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
						detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
					});
				}
			}
		);
	}
	get selectedTemplateObject(): EbTemplateDocumentsDTO {
		if (this.params.selectedTemplate == null || this.templates == null) return null;

		let rValue: EbTemplateDocumentsDTO;
		this.templates.forEach((c) => {
			if (this.params.selectedTemplate === c.ebTemplateDocumentsNum) rValue = c;
		});

		return rValue;
	}

	get canConfirm(): boolean {
		return (
			this.params.selectedTemplate &&
			(this.params.downloadChecked || this.params.attachChecked) &&
			(!this.params.attachChecked ||
				(this.params.attachChecked && this.params.selectedTypeDoc != null))
		);
	}

	modalExportPdfClose() {
		this.closePopup.emit(new ModalExportDocByTemplateComponent.Result(false, this.params));
	}

	private doDownload(file: any, filename: string) {
		const blob = new Blob([file], { type: "application/octet-stream" });
		if (window.navigator && window.navigator.msSaveOrOpenBlob) {
			window.navigator.msSaveOrOpenBlob(blob, filename);
		} else {
			let a: any = document.createElement("A");
			a.href = URL.createObjectURL(blob);
			a.download = filename;
			document.body.appendChild(a);
			a.click();
			document.body.removeChild(a);
		}
	}
	onSelectedTemplateChange(selectedTemplate: EbTemplateDocumentsDTO) {
		// Reset selected typedoc when changing template
		this.params.selectedTypeDoc = null;

		// Filter list type documents available for templates
		this.listTypeDocumentsFiltered = [];

		if (
			selectedTemplate == null ||
			selectedTemplate.typesDocument == null ||
			selectedTemplate.typesDocument.length === 0
		) {
			// The type document doesn't have filter on type document, we show all type docs
			this.listTypeDocumentsFiltered = this.listTypeDocuments;
			return; // No need to continue here
		}

		// Filter type documents
		this.listTypeDocuments.forEach((td) => {
			const canAdd =
				selectedTemplate.typesDocument.filter(
					(std) => std.ebTypeDocumentsNum === td.ebTypeDocumentsNum
				).length > 0;
			if (canAdd) this.listTypeDocumentsFiltered.push(td);
		});

		// If no type documents available on list, we show all
		if (this.listTypeDocumentsFiltered.length == 0) {
			this.listTypeDocumentsFiltered = this.listTypeDocuments;
		}
	}

	public onEmailDestAdd(event: any) {
		if (!event.value || !(event.value as string).match(Statique.patterEmail)) {
			// Prevent typing non email values
			this.params.emailDest = this.params.emailDest.filter((v) => v !== event.value);
		}
	}
}

export namespace ModalExportDocByTemplateComponent {
	export class Result {
		exported: boolean;
		params?: TemplateGenParamsDTO;
		generatedFile?: Blob;

		constructor(exported: boolean, params?: TemplateGenParamsDTO, generatedFile?: Blob) {
			this.exported = exported;
			this.params = params;
			this.generatedFile = generatedFile;
		}
	}
}
