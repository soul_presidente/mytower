import {Component, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-popup-confirmation-field-mandatory',
  templateUrl: './popup-confirmation-field-mandatory.component.html',
  styleUrls: ['./popup-confirmation-field-mandatory.component.scss']
})
export class PopupConfirmationFieldMandatoryComponent implements OnInit {
	@Input()
	modalFieldMandatory: boolean ;
	isClose  : boolean;
  constructor() { }

  ngOnInit() {
  }
	onCloseModalFieldMandatory() {
		this.modalFieldMandatory = false;
		this.isClose  = true;
	}
}
