import { Component, OnInit, Input } from "@angular/core";
import { EbTemplateDocumentsDTO } from "@app/classes/dto/ebTemplateDocumentsDTO";
import { TemplateGenParamsDTO } from "@app/classes/TemplateGenParamsDTO";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";

@Component({
	selector: "app-modal-export-doc-form",
	templateUrl: "./modal-export-doc-form.component.html",
	styleUrls: ["./modal-export-doc-form.component.scss"],
})
export class ModalExportDocFormComponent implements OnInit {
	@Input()
	templates: Array<EbTemplateDocumentsDTO> = [];

	@Input()
	params: TemplateGenParamsDTO = new TemplateGenParamsDTO();

	@Input()
	listTypeDocuments = new Array<EbTypeDocuments>();

	@Input()
	hideDownloadAction: boolean = false;

	listTypeDocumentsFiltered = new Array<EbTypeDocuments>();

	constructor() {}

	ngOnInit() {}

	ngAfterViewInit() {
		if (
			this.params != null &&
			this.params.selectedTemplate != null &&
			this.templates != null &&
			this.templates.length > 0
		) {
			let selectedTemplate = this.templates.find(
				(tmp) => tmp.ebTemplateDocumentsNum == this.params.selectedTemplate
			);
			this.resetListTypesOfDoc(selectedTemplate);
		}
	}

	onSelectedTemplateChange(selectedTemplate: EbTemplateDocumentsDTO) {
		// Reset selected typedoc when changing template
		this.params.selectedTypeDoc = null;

		this.resetListTypesOfDoc(selectedTemplate);
	}

	resetListTypesOfDoc(selectedTemplate: EbTemplateDocumentsDTO) {
		// Filter list type documents available for templates
		this.listTypeDocumentsFiltered = [];

		if (
			selectedTemplate == null ||
			selectedTemplate.typesDocument == null ||
			selectedTemplate.typesDocument.length === 0
		) {
			// The type document doesn't have filter on type document, we show all type docs
			this.listTypeDocumentsFiltered = this.listTypeDocuments;
			return; // No need to continue here
		}

		// Filter type documents
		this.listTypeDocuments.forEach((td) => {
			const canAdd =
				selectedTemplate.typesDocument.filter(
					(std) => std.ebTypeDocumentsNum === td.ebTypeDocumentsNum
				).length > 0;
			if (canAdd) this.listTypeDocumentsFiltered.push(td);
		});

		// If no type documents available on list, we show all
		if (this.listTypeDocumentsFiltered.length == 0) {
			this.listTypeDocumentsFiltered = this.listTypeDocuments;
		}
	}
}
