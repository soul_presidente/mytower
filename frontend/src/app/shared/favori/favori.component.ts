import { Component, Input, OnDestroy, OnInit } from "@angular/core";
import { EbUser } from "@app/classes/user";
import { Statique } from "@app/utils/statique";
import { EbFavori } from "@app/classes/favori";
import { FavoriService } from "@app/services/favori.service";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";

@Component({
	selector: "app-favori",
	templateUrl: "./favori.component.html",
	styleUrls: ["./favori.component.scss"],
})
export class FavoriComponent implements OnInit, OnDestroy {
	statique = Statique;

	dataSourceList: string = "/list-favori";
	dataSourceOne: string = "/get-favori";
	selectedFavoriFromlist: EbFavori = new EbFavori();
	ebFavori: EbFavori = null;
	subscriptions: Array<Subscription> = new Array<Subscription>();

	@Input()
	public module: number = null;
	@Input()
	public idObject: number;
	@Input()
	public refObject: string;
	@Input()
	public user: EbUser;
	@Input()
	public isListing: boolean = false;
	@Input()
	public isDetail: boolean = false;
	@Input()
	public listFavori = new Array<EbFavori>();

	constructor(private favoriService: FavoriService, protected router: Router) {}

	ngOnInit() {
		this.loadDataFavoris();
		if (this.isListing) {
			this.subscriptions.push(
				this.favoriService.favorisObservale.subscribe((res: EbFavori) => {
					if (res.toAdd) {
						this.listFavori.push(res);
					} else if (res.toDelete) {
						let index = null;
						this.listFavori.forEach((element: EbFavori, i: number) => {
							if (element.ebFavoriNum == res.ebFavoriNum) {
								index = i;
								return false;
							}
						});
						if (index != null) this.listFavori.splice(index, 1);
					}
				})
			);
		} else if (this.isDetail) {
			this.subscriptions.push(
				this.favoriService.listingFavorisObservale.subscribe((res: EbFavori) => {
					if (res.ebFavoriNum == this.ebFavori.ebFavoriNum) {
						this.ebFavori = null;
					}
				})
			);
		}
	}

	loadDataFavoris() {
		if (this.isListing) {
			this.favoriService
				.getListFavoris(
					Statique.controllerAccount +
						this.dataSourceList +
						"?module=" +
						(this.module != null ? this.module : "")
				)
				.subscribe((data) => {
					this.listFavori = data;
				});
		} else if (this.isDetail && this.idObject != null) {
			this.favoriService
				.getListFavoris(
					Statique.controllerAccount +
						this.dataSourceOne +
						"?module=" +
						this.module +
						"&idObject=" +
						this.idObject
				)
				.subscribe((data) => {
					this.ebFavori = data;
				});
		}
	}

	checkFavori() {
		this.ebFavori = new EbFavori();
		this.ebFavori.user.ebUserNum = this.user.ebUserNum;
		this.ebFavori.module = this.module;
		if (this.idObject != null) {
			this.addFavori();
		}
	}

	uncheckFavori() {
		if (this.ebFavori.ebFavoriNum != null) {
			this.deleteFavori(this.ebFavori);
		} else this.ebFavori = null;
	}

	addFavori() {
		this.ebFavori.idObject = this.idObject;
		this.ebFavori.refObject = this.refObject;
		this.favoriService.addFavori(this.ebFavori).subscribe((data) => {
			this.ebFavori = data;
			this.ebFavori.toDelete = false;
			this.ebFavori.toAdd = true;
			this.favoriService.notifyComponentFavori(this.ebFavori);
		});
	}

	deleteFavori(ebFavori: EbFavori) {
		this.favoriService.deleteFavori(ebFavori).subscribe((data) => {
			if (this.selectedFavoriFromlist.ebFavoriNum == ebFavori.ebFavoriNum) {
				this.selectedFavoriFromlist = new EbFavori();
			}
			if (this.isListing) {
				let index = null;
				this.listFavori.forEach((element: EbFavori, i: number) => {
					if (element.ebFavoriNum == ebFavori.ebFavoriNum) {
						index = i;
						return false;
					}
				});
				if (index != null) this.listFavori.splice(index, 1);
				this.favoriService.notifyListingComponentFavori(ebFavori);
			} else if (this.isDetail) {
				ebFavori.toAdd = false;
				ebFavori.toDelete = true;
				this.favoriService.notifyComponentFavori(ebFavori);
				this.ebFavori = null;
			}
		});
	}

	navigateToComponentTarget(favori: EbFavori) {
		let path = "app/" + Statique.getComponentPathByModuleId(favori.module) + "/" + favori.idObject;

		if (!this.router.isActive(path, true)) this.router.navigateByUrl(path);
	}

	showFavori(favori: EbFavori) {
		// design active element in the saved-form list in order to highlight it
		this.selectedFavoriFromlist = favori;

		// navigate to target component/page
		this.navigateToComponentTarget(favori);
		// send data to the target component
		//this.savedFormsService.emitSavedForm(sf);
	}

	getModuleIcon(ebModuleNum): string {
		let moduleName: string = Statique.getModuleNameByModuleNum(ebModuleNum);

		if (!moduleName) return null;

		return /*Statique.iconsPath + */ Statique.moduleIcon[moduleName];
	}

	isIconDefined(ebModuleNum): boolean {
		return Statique.isDefined(this.getModuleIcon(ebModuleNum));
	}

	ngOnDestroy() {
		this.subscriptions.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
	}
}
