import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";

@Component({
	selector: "app-breadcrumb",
	templateUrl: "./breadcrumb.component.html",
	styleUrls: ["./breadcrumb.component.scss"],
})
export class BreadcrumbComponent {
	@Input()
	options: Array<any> = [];

	public get hasItems(): boolean {
		return this.options != null && this.options.length > 0;
	}
	constructor(private router: Router) {}
	goTo(url) {
		this.router
			.navigateByUrl("/", { skipLocationChange: true })
			.then(() => this.router.navigate([url]));
	}
}
