import { Component, Input, OnChanges, OnInit } from "@angular/core";
import { EbVehicule } from "../../classes/vehicule";
import { VehiculeService } from "../../services/vehicule.service";
import { ModeTransport } from "../../utils/enumeration";
import { ArticleDims } from "../../classes/articleDims";
import { Statique } from "../../utils/statique";
import { GenericTableInfos } from "../generic-table/generic-table-infos";

@Component({
	selector: "app-vehicule-content",
	templateUrl: "./vehicule-content.component.html",
	styleUrls: ["./vehicule-content.component.css"],
})
export class VehiculeContentComponent implements OnInit, OnChanges {
	@Input()
	ebCompagnieNum: number;
	@Input()
	modeTransport: number;
	@Input()
	listArticleDims: Array<ArticleDims>;

	listVehicule: Array<EbVehicule> = null;
	content: Array<any> = null;
	cols: Array<GenericTableInfos.Col> = null;
	RenderModeTable = GenericTableInfos.Col.RenderModeTable;

	totalContent: number;

	constructor(private vehiculeService: VehiculeService) {}

	ngOnInit() {
		// this.getListVehicule();

		this.cols = [
			{ field: "name", header: "Contenant" },
			{
				field: "capacity",
				header: "Capacité du contenant",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: (data) => {
					return data ? this.cutNumber(this.convertNumberByMT(data)) + " " + this.getUnit() : "";
				},
			},
			// { field: "totalContent", header: "Total contenu", render: data => data ? data + " " + this.getUnit() : "" },
			{
				field: "ratio",
				header: "Taux du chargement",
				render: (data) => {
					return data ? this.cutNumber(data) + " %" : "";
				},
			},
			{ field: "full", header: "Full" },
			{ field: "recommanded", header: "Recommanded", render: (data) => (data ? "Oui" : "Non") },
		];
	}

	ngOnChanges() {
		this.computeTotal();
		this.getListVehicule();
	}

	getListVehicule() {
		if (!this.ebCompagnieNum) return;
		this.vehiculeService
			.getListVehicule(this.ebCompagnieNum)
			.subscribe((listVehicule: Array<EbVehicule>) => {
				this.listVehicule = listVehicule.filter((it) => it.xModeTransport == this.modeTransport);
				this.computeAll();
			});
	}

	computeAll() {
		this.content = new Array<any>();
		this.listVehicule.forEach((it) => {
			this.content.push({
				vehicule: it,
				name: it.libelle,
				capacity: this.computeCapacity(it),
				totalContent: this.totalContent,
			});
		});

		if (this.listArticleDims) {
			this.content.forEach((it) => {
				it.ratio = this.computeRatio(it);
			});
			this.content.forEach((it) => {
				it.full = this.isFull(it);
				it.recommanded = this.isRecommanded(it);
			});
		}
	}

	computeTotal() {
		let total = 0;
		let size = 0;
		this.listArticleDims &&
			this.listArticleDims.forEach((it) => {
				size = Statique.isDefined(it.length) ? it.length : 0;

				if (this.modeTransport == ModeTransport.SEA)
					size *=
						(Statique.isDefined(it.width) ? it.width : 0) *
						(Statique.isDefined(it.height) ? it.height : 0);

				total += size * it.quantity;
			});

		this.totalContent = total;
	}

	computeCapacity(vehicule: EbVehicule): number {
		let size = 0;
		size = Statique.isDefined(vehicule.lengh) ? vehicule.lengh : 0;

		if (this.modeTransport == ModeTransport.SEA)
			size *=
				(Statique.isDefined(vehicule.width) ? vehicule.width : 0) *
				(Statique.isDefined(vehicule.height) ? vehicule.height : 0);

		// if(this.modeTransport == ModeTransport.SEA) size = size/1000000;
		// if(this.modeTransport == ModeTransport.ROAD) size = size/100;

		return size;
	}

	computeRatio(el): number {
		let cap = el.capacity;
		let ratio = null;
		if (cap) {
			ratio = (this.totalContent / cap) * 10;
		}
		return ratio;
	}

	/*
    Si Maritime et taux de charge < 50% alors "LCL"
    Si Maritime et taux de charge >= 50% alors "FCL"
    Si Route et taux de charge < 50% alors "LTL"
    Si Route et taux de charge >= 50% alors "FTL"
   */
	isFull(el): string {
		if (this.modeTransport == ModeTransport.SEA && el.ratio < 50) return "LCL";
		if (this.modeTransport == ModeTransport.SEA && el.ratio >= 50) return "FCL";
		if (this.modeTransport == ModeTransport.ROAD && el.ratio < 50) return "LTL";
		if (this.modeTransport == ModeTransport.ROAD && el.ratio >= 50) return "FTL";
	}

	getUnit(): string {
		if (this.modeTransport == ModeTransport.ROAD) return "m";
		else return "m<sup>3</sup>";
	}

	isRecommanded(el): boolean {
		let arr = this.content
			.filter((it) => it.ratio != null && it.ratio <= 100)
			.sort((it1, it2) => it1.capacity - it2.capacity);
		if (arr && arr[0]) return arr[0].vehicule.ebVehiculeNum == el.vehicule.ebVehiculeNum;
		if (!arr || arr.length == 0) {
			arr = this.content
				.filter((it) => it.ratio != null)
				.sort((it1, it2) => it2.capacity - it1.capacity);
			if (arr && arr[0]) return arr[0].vehicule.ebVehiculeNum == el.vehicule.ebVehiculeNum;
		}
		return false;
	}

	convertNumberByMT(data) {
		let res = null;
		if (this.modeTransport == ModeTransport.ROAD) res = data / 100;
		else res = data / 1000000;
		return res;
	}

	cutNumber(data) {
		if (data && Number(data)) return data.toFixed(1);
	}
}
