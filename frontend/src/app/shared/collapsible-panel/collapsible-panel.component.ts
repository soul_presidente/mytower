import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnDestroy,
	OnInit,
	Output,
	ViewChild,
} from "@angular/core";
import { generaleMethodes } from "../generaleMethodes.component";
import { Modules, UserRole } from "@app/utils/enumeration";
import { ExportService } from "@app/services/export.service";
import { UploadComponent } from "@app/utils/uploadComponent";
import { UploadFile, UploadOutput } from "ngx-uploader";
import { setTimeout } from "timers-browserify";
import { DomSanitizer } from "@angular/platform-browser";
import { Statique } from "@app/utils/statique";
import { ModalService } from "../modal/modal.service";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { DataTableDirective } from "../../../../node_modules/angular-datatables";
import { PricingBookingSearchComponent } from "@app/component/pricing-booking/shared-component/pricing-booking-search/pricing-booking-search.component";
import { TrackTraceSearchComponent } from "@app/component/track-trace/track-trace-search/track-trace-search.component";
import { FreightAuditSearchComponent } from "@app/component/freight-audit/freight-audit-search/freight-audit-search.component";

@Component({
	selector: "app-collapsible-panel",
	templateUrl: "./collapsible-panel.component.html",
	styleUrls: ["./collapsible-panel.component.css"],
})
export class CollapsiblePanelComponent extends UploadComponent implements OnInit, OnDestroy {
	@Input()
	title: string;
	@Input()
	collapse?: boolean = false;
	@Input()
	fullscreen?: boolean;
	@Input()
	csvExtractModule?: number;
	@Input()
	csvExtractPeriode?: boolean;
	@Input()
	searchInput?: SearchCriteriaPricingBooking;
	@Input()
	dataUpload?: any;
	@Input()
	addLivraisonButton?: any;
	@Input()
	saveStateBtn?: boolean;
	@Input()
	dtElement?: DataTableDirective;
	@Input()
	pricingBookingSearchComponent?: PricingBookingSearchComponent;
	@Input()
	trackTraceSearchComponent?: TrackTraceSearchComponent;
	@Input()
	isNgDataTable?: boolean;
	@Input()
	selectedColumns?: any[];
	@Input()
	columns?: any[];
	@Input()
	freightAuditSearchComponent?: FreightAuditSearchComponent;

	@Output()
	onDataUploadClick = new EventEmitter<Function>();
	@Output()
	onUploadCompleteHandler = new EventEmitter<UploadFile>();
	@Output()
	onUploadProgressHandler = new EventEmitter<UploadFile>();
	@Output()
	onStateDatatableHandler = new EventEmitter<any>();

	@Output()
	onCreateDelivery = new EventEmitter<any>();

	@Input()
	panelClass?: string;
	@Input()
	headingClass?: string;
	@Input()
	bodyClass?: string;

	@ViewChild("panel", { static: true })
	panelEl: ElementRef;
	@ViewChild("fileInput", { static: true })
	fileInput: ElementRef;

	UserRole = UserRole;

	startUploading: number;
	outputFile: UploadOutput;

	idSearchDtInterval: any = null;
	isSearchDtDestroyed: boolean = false;
	isSearchDtBusy: boolean = true;

	searchVisible: boolean = false;

	constructor(
		private generaleMethode: generaleMethodes,
		private exportService: ExportService,
		protected sanitizer: DomSanitizer
	) {
		super(sanitizer);

		this.componentObject = this;
		this.isGallery = null;
	}

	ngOnInit() {
		if (typeof this.collapse === "undefined") this.collapse = null;
		if (typeof this.fullscreen === "undefined") this.fullscreen = true;
		if (typeof this.csvExtractModule === "undefined") this.csvExtractModule = null;
		if (typeof this.dataUpload === "undefined") this.dataUpload = null;

		if (typeof this.panelClass === "undefined") this.panelClass = "panel-table";
		if (typeof this.headingClass === "undefined") this.headingClass = "heading-table";
		if (typeof this.bodyClass === "undefined") this.bodyClass = "body-table";

		this.moveDatatableFilter();
		this.rerenderDatatableFilter();
		this.initAdvancedSearchListener();
	}

	panelFullScreen(panel) {
		this.generaleMethode.panelFullScreen(panel);
		this.fullscreen = !this.fullscreen;
	}

	panelCollapse(panel) {
		this.fullscreen = !this.fullscreen;
		this.collapse = this.collapse == null ? true : !this.collapse;
	}

	async exportCsv(event) {
		let search = null;
		if (
			this.csvExtractModule == Modules.PRICING ||
			this.csvExtractModule == Modules.TRANSPORT_MANAGEMENT
		)
			search = this.pricingBookingSearchComponent[
				"advancedFormSearchComponent" +
					(this.csvExtractModule == Modules.PRICING ? "Pricing" : "Booking")
			].getSearchInput();
		else if (this.csvExtractModule == Modules.TRACK)
			search = this.trackTraceSearchComponent.advancedFormSearchComponentTrack.getSearchInput();
		else if (this.csvExtractModule == Modules.FREIGHT_AUDIT)
			search = this.freightAuditSearchComponent.advancedFormSearchComponentFreight.getSearchInput();
		if (this.dtElement) {
			let $target =
				$(event.target).prop("tagName") != "I" ? $(event.target) : $(event.target).parent();
			let $initHtml = $target.html();
			$target.html("<i class='fa fa-spinner fa-spin fa-15x' style='color:black'></i>");
			let config = DataTableConfig.getStateColumns(this.dtElement);
			await this.exportService.startExportCsv(
				this.csvExtractModule,
				false,
				search || this.searchInput,
				this.userConnected,
				config
			);

			$target.html($initHtml);
		} else
			this.exportService.startExportCsv(
				this.csvExtractModule,
				this.csvExtractPeriode,
				search || this.searchInput,
				this.userConnected
			);
	}

	fileInputHandle() {
		this.startUploading = 0;
		this.fileInput.nativeElement.value = null;
		this.outputFile = null;

		this.fileInput.nativeElement.click();

		const $this = this;
		this.onDataUploadClick.emit(
			function() {
				$this.startUploading++;
				if ($this.startUploading >= 2) {
					setTimeout(
						function() {
							this.startUpload();
						}.bind(this),
						500
					);
				}
			}.bind(this)
		);
	}

	uploadFile(output: UploadOutput) {
		this.onUploadOutput(output);
		this.startUploading++;
		if (this.startUploading >= 2) {
			setTimeout(
				function() {
					this.startUpload();
				}.bind(this),
				500
			);
		}
	}

	rerenderDatatableFilter() {
		const $this = this;
		this.idSearchDtInterval = setInterval(
			async function() {
				try {
					if (!$this.isSearchDtBusy && !$this.isSearchDtDestroyed) {
						$this.isSearchDtBusy = true;

						let collapsContainer = $this.panelEl.nativeElement;
						let allFilters = collapsContainer.getElementsByClassName("dataTables_filter");
						let filterEl = collapsContainer.querySelectorAll(
							".panel-controls .datatable-search .dataTables_filter"
						)[0];
						if (allFilters.length > 1 && filterEl) {
							filterEl.remove();
							await $this.moveDatatableFilter();
						}
						$this.isSearchDtBusy = false;
					}
				} catch (e) {}

				if ($this.isSearchDtDestroyed && $this.idSearchDtInterval) {
					clearInterval($this.idSearchDtInterval);
				}
			}.bind(this),
			400
		);
	}

	async moveDatatableFilter() {
		this.isSearchDtBusy = true;

		try {
			let filterEl;
			let tmpEl;
			let collapsContainer = this.panelEl.nativeElement;

			let timer = 0;
			while (
				!(filterEl = collapsContainer.getElementsByClassName("dataTables_filter")[0]) &&
				timer < 10000
			) {
				await Statique.waitForTimemout(400);
				timer += 400;
			}

			tmpEl = collapsContainer
				? (tmpEl = collapsContainer.getElementsByClassName("datatable-search"))
					? tmpEl[0]
					: null
				: null;
			if (tmpEl && filterEl) {
				Statique.moveDomElement(filterEl, tmpEl);
				filterEl.className += " d-inline-block";
				filterEl = filterEl.getElementsByTagName("label");
				if (filterEl && (filterEl = filterEl[0])) {
					filterEl.childNodes.forEach((it) => {
						if (it.nodeType == 3) it.data = "";
					});
					filterEl = (filterEl = filterEl.getElementsByTagName("input")) ? filterEl[0] : null;
					if (filterEl) {
						filterEl.placeholder = "Search";
					}
				}

				filterEl = (filterEl = collapsContainer.getElementsByClassName("dt-buttons"))
					? filterEl[0]
					: null;
				tmpEl = collapsContainer
					? (tmpEl = collapsContainer.getElementsByClassName("datatable-filter"))
						? tmpEl[0]
						: null
					: null;
				if (filterEl && tmpEl) {
					filterEl.closest(".collapsible-panel").classList.add("has-datatable");
					Statique.moveDomElement(filterEl, tmpEl);
				}
			}
			// adding class fontawesome to datatable buttons
			let optionsBtn = document.getElementsByClassName("buttons-colvis")[0];
			if (optionsBtn != undefined) optionsBtn.classList.add("fa-cog", "fa");
			let fullScreenBtn = document.getElementsByClassName("panel-control-fullscreen")[0];
			if (fullScreenBtn != undefined) fullScreenBtn.classList.add("fa-expand", "fa");
			// let lessBtn = document.getElementsByClassName("panel-control-collapse")[0];
		} catch (e) {}

		this.isSearchDtBusy = false;
	}
	// fonction positionnement du filter des colonnes (dataTable)
	moveDatatableColumnsFilter() {
		let datatableColumnsFilter = <HTMLElement>document.querySelector(".dt-button-collection");
		let datatableColumnsBackground = <HTMLElement>document.querySelector(".dt-button-background");
		let btnPosition = document
			.querySelector(".datatable-filter.d-inline-block")
			.getBoundingClientRect();

		datatableColumnsBackground.appendChild(datatableColumnsFilter);
		datatableColumnsFilter.style.top = btnPosition.top + 15 + "px";
	}

	onUploadComplete(file: UploadFile) {
		this.onUploadCompleteHandler.emit(file);
	}

	ngOnDestroy() {
		this.initAdvancedSearchListener(true);

		if (this.idSearchDtInterval) clearInterval(this.idSearchDtInterval);
		this.idSearchDtInterval = null;
		this.isSearchDtDestroyed = true;

		let filterEl;
		if ((filterEl = this.panelEl.nativeElement.getElementsByClassName("dataTables_filter")[0]))
			filterEl.parentNode.removeChild(filterEl);
	}

	onUploadProgress(file: UploadFile) {
		this.onUploadProgressHandler.emit(file);
	}

	saveStateDatatable(event) {
		this.onStateDatatableHandler.emit(event);
	}

	hideAdvancedSearch($event: MouseEvent) {
		Statique.triggerEvent("closeAdvancedSearch");
	}

	createDelivery() {
		this.onCreateDelivery.emit();
	}

	private initAdvancedSearchListener(remove?: boolean) {
		// listeners
		const openAdvancedSearchListener = () => {
			this.searchVisible = true;
			this.collapse = false;
		};
		const closeAdvancedSearchListener = () => {
			this.searchVisible = false;
			this.collapse = true;
		};

		if (remove) {
			document.removeEventListener("openAdvancedSearch", openAdvancedSearchListener);
			document.removeEventListener("closeAdvancedSearch", closeAdvancedSearchListener);
			return;
		}

		document.addEventListener("openAdvancedSearch", openAdvancedSearchListener);
		document.addEventListener("closeAdvancedSearch", closeAdvancedSearchListener);
	}
}
