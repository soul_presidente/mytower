import { Component, Input, OnInit } from "@angular/core";
import { ModalService } from "./modal.service";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import {
	IDChatComponent,
	Modules,
	PriseRDVStatus,
	TypeDatePriseRDV,
	UserRole,
} from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { EbChat } from "@app/classes/chat";
import { ChatService } from "@app/services/chat.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { MessageService } from "primeng/api";
import { PricingService } from "@app/services/pricing.service";
import { DatePipe } from "@angular/common";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-modal",
	templateUrl: "./modal.component.html",
	styleUrls: ["./modal.component.scss"],
})
export class ModalComponent implements OnInit {
	TypeDatePriseRDV = TypeDatePriseRDV;
	PriseRDVStatus = PriseRDVStatus;
	UserRole = UserRole;

	@Input()
	message: any;
	@Input()
	modalWidth: string;

	numberOfPropPkRd: number = 0;
	numberOfPropDlRv: number = 0;

	constructor(
		public activeModal: NgbActiveModal,
		private modalService: ModalService,
		private chatService: ChatService,
		private messageService: MessageService,
		private pricingService: PricingService,
		private datePipe: DatePipe,
		private translate: TranslateService
	) {}
	Statique: Statique;
	messageChat: string;
	addPkRd = true;
	addDlRv = true;
	toastmsg: boolean = false;
	ebChat: EbChat;
	ngOnInit() {
		this.modalService.getMessage().subscribe((message) => {
			this.message = message;
			this.addPkRd = true;
			this.addDlRv = true;
			if (this.message && this.message.params && this.message.params.listPropRdvPk) {
				this.sortListRDVByCreationDate(this.message.params.listPropRdvPk);
			}
			if (this.message && this.message.params && this.message.params.listPropRdvDl) {
				this.sortListRDVByCreationDate(this.message.params.listPropRdvDl);
			}
			if (this.message && this.message.tabData && this.message.tabData.length > 1) {
				this.addPkRd = false;
				this.addDlRv = false;
			}
		});
	}
	cancelProp(forPk, propRdv, index) {
		let prop = null;
		if (forPk) {
			prop = this.message.params.listPropRdvPk[index];
			prop.statut = PriseRDVStatus.ANNULEE; //prop rdv annulée
			this.message.params.listPropRdvPk[index] = prop;
			this.addPkRd = true;
		} else {
			prop = this.message.params.listPropRdvDl[index];
			prop.statut = PriseRDVStatus.ANNULEE; //prop rdv annulée
			this.message.params.listPropRdvDl[index] = prop;
			this.addDlRv = true;
		}
		let mapResults: any[] = [];
		let mapResult = {
			ebDemandeNum: this.message.params["ebDemandeNum"],
			listPropRdvPk: this.message.params["listPropRdvPk"],
			listPropRdvDl: this.message.params["listPropRdvDl"],
		};
		mapResults.push(mapResult);
		this.pricingService.updateEbDemandePropRdv(mapResults).subscribe((data) => {
			if (forPk) {
				//this.addPkRd=false;
				this.message.params.listPropRdvPk = data.listPropRdvPk;
				let msg = null;
				this.translate
					.get("TRANSPORT_MANAGEMENT.PRISE_RDV.MESSAGE.ANNULATION_PK")
					.subscribe((res: string) => {
						msg = res;
					});
				this.sendChat(null, msg + this.datePipe.transform(prop.date, "yyyy-MM-dd HH:mm"));
			} else {
				//this.addDlRv=false;
				this.message.params.listPropRdvDl = data.listPropRdvDl;
				let msg = null;
				this.translate
					.get("TRANSPORT_MANAGEMENT.PRISE_RDV.MESSAGE.ANNULATION_DL")
					.subscribe((res: string) => {
						msg = res;
					});
				this.sendChat(null, msg + this.datePipe.transform(prop.date, "yyyy-MM-dd HH:mm"));
			}
		});
	}

	checkPropValide(listProp, index) {
		listProp.forEach((p, i) => {
			if (p.statut == PriseRDVStatus.VALIDEE && i != index) {
				p.statut = PriseRDVStatus.ANNULEE;
			}
		});
	}

	validerProp(event, forPk, propRdv, index) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		let prop = null;
		if (forPk) {
			prop = this.message.params.listPropRdvPk[index];
			prop.statut = PriseRDVStatus.VALIDEE; //prop rdv validée
			this.message.params.listPropRdvPk[index] = prop;
			this.checkPropValide(this.message.params.listPropRdvPk, index);
		} else {
			prop = this.message.params.listPropRdvDl[index];
			prop.statut = PriseRDVStatus.VALIDEE; //prop rdv validée
			this.message.params.listPropRdvDl[index] = prop;
			this.checkPropValide(this.message.params.listPropRdvDl, index);
		}
		let mapResults: any[] = [];
		let mapResult = {
			ebDemandeNum: this.message.params["ebDemandeNum"],
			listPropRdvPk: this.message.params["listPropRdvPk"],
			listPropRdvDl: this.message.params["listPropRdvDl"],
		};
		mapResults.push(mapResult);
		this.pricingService.updateEbDemandePropRdv(mapResults).subscribe((data) => {
			requestProcessing.afterGetResponse(event);
			if (forPk) {
				//this.addPkRd=false;
				this.message.params.listPropRdvPk = data.listPropRdvPk;
				let msg = null;
				this.translate
					.get("TRANSPORT_MANAGEMENT.PRISE_RDV.MESSAGE.CONFIRMATION_Pk")
					.subscribe((res: string) => {
						msg = res;
					});
				this.sendChat(null, msg + this.datePipe.transform(prop.date, "yyyy-MM-dd HH:mm"));
			} else {
				//this.addDlRv=false;
				this.message.params.listPropRdvDl = data.listPropRdvDl;
				let msg = null;
				this.translate
					.get("TRANSPORT_MANAGEMENT.PRISE_RDV.MESSAGE.CONFIRMATION_DL")
					.subscribe((res: string) => {
						msg = res;
					});
				this.sendChat(null, msg + this.datePipe.transform(prop.date, "yyyy-MM-dd HH:mm"));
			}
		});
	}
	saveHandler(event) {
		this.validateListProp();
		this.sendMessageforProp();
		let data: any = Statique.cloneObject(this.message.params);
		delete data.userConnected;
		if (this.message.siFn) this.message.siFn(data, event);
	}

	addNewRdvPk() {
		if (!this.message.params.listPropRdvPk) {
			this.message.params.listPropRdvPk = [];
		}
		let newRdvPk = {
			date: null,
			dateCreation: Date.now(),
			statut: PriseRDVStatus.INISTIAL,
			role: this.message.params.userConnected.role,
			nomUser: this.message.params.userConnected.nom,
		};
		this.message.params.listPropRdvPk.unshift(newRdvPk);
		this.numberOfPropPkRd++;
		// this.addPkRd = false;
	}

	addNewRdvDl() {
		if (!this.message.params.listPropRdvDl) {
			this.message.params.listPropRdvDl = [];
		}
		let newRdvDl = {
			date: null,
			dateCreation: Date.now(),
			statut: PriseRDVStatus.INISTIAL,
			role: this.message.params.userConnected.role,
			nomUser: this.message.params.userConnected.nom,
		};
		this.message.params.listPropRdvDl.unshift(newRdvDl);
		this.numberOfPropDlRv++;
		// this.addDlRv = false;
	}

	validateListProp() {
		let props;
		if (this.message.params.listPropRdvPk != null && this.numberOfPropPkRd > 0) {
			props = [];
			for (let i = 0; i < this.message.params.listPropRdvPk.length; i++) {
				if (this.message.params.listPropRdvPk[i].date == null) {
					this.numberOfPropPkRd--;
				} else {
					props.push(this.message.params.listPropRdvPk[i]);
				}
			}
			this.message.params.listPropRdvPk = props;
		}
		if (this.message.params.listPropRdvDl != null && this.numberOfPropDlRv > 0) {
			props = [];
			for (let i = 0; i < this.message.params.listPropRdvDl.length; i++) {
				if (this.message.params.listPropRdvDl[i].date == null) {
					this.numberOfPropDlRv--;
				} else {
					props.push(this.message.params.listPropRdvDl[i]);
				}
			}
			this.message.params.listPropRdvDl = props;
		}
	}

	getListDatePropRdvPk(): String {
		let listDate = "<div>- ";
		for (let i = 0; i < this.numberOfPropPkRd; i++) {
			listDate +=
				this.datePipe.transform(this.message.params.listPropRdvPk[i].date, "yyyy-MM-dd HH:mm") +
				"</div>" +
				(i + 1 < this.numberOfPropPkRd ? "<div>- " : "");
		}
		return listDate;
	}

	getListDatePropRdvDl(): String {
		let listDate = "<div>- ";
		for (let i = 0; i < this.numberOfPropDlRv; i++) {
			listDate +=
				this.datePipe.transform(this.message.params.listPropRdvDl[i].date, "yyyy-MM-dd HH:mm") +
				"</div>" +
				(i + 1 < this.numberOfPropDlRv ? "<div>- " : "");
		}
		return listDate;
	}

	sendMessageforProp() {
		if (this.numberOfPropPkRd > 0) {
			let msg = null;
			this.translate
				.get("TRANSPORT_MANAGEMENT.PRISE_RDV.MESSAGE.PROPOSITION_Pk")
				.subscribe((res: string) => {
					msg = res;
				});
			this.sendChat(null, msg + this.getListDatePropRdvPk());
			this.numberOfPropPkRd = 0;
			this.toastmsg = true;
		}
		if (this.numberOfPropDlRv > 0) {
			let msg = null;
			this.translate
				.get("TRANSPORT_MANAGEMENT.PRISE_RDV.MESSAGE.PROPOSITION_DL")
				.subscribe((res: string) => {
					msg = res;
				});
			this.sendChat(null, msg + this.getListDatePropRdvDl());
			this.toastmsg = true;
			this.numberOfPropDlRv = 0;
		}
	}

	sendMessage(event, type: number = null) {
		if (type == TypeDatePriseRDV.PICKUP) {
			//pickup
			let msg = null;
			this.translate
				.get("TRANSPORT_MANAGEMENT.PRISE_RDV.MESSAGE.AVAILABILITY_REQUEST_PK")
				.subscribe((res: string) => {
					msg = res;
				});
			this.sendChat(null, msg);
		} else if (type == TypeDatePriseRDV.LAST_EXPECTED_PSL) {
			//expected
			let msg = null;
			this.translate
				.get("TRANSPORT_MANAGEMENT.PRISE_RDV.MESSAGE.AVAILABILITY_REQUEST_DL")
				.subscribe((res: string) => {
					msg = res;
				});
			this.sendChat(null, msg);
		} else {
			this.sendChat(event);
		}
	}

	sendChat(event = null, message: string = null) {
		let requestProcessing = null;
		if (event != null) {
			requestProcessing = new RequestProcessing();
			requestProcessing.beforeSendRequest(event);
		}
		let msg: string = null;
		if (message != null) msg = message;
		else msg = this.messageChat;
		for (let demande of this.message.tabData) {
			this.ebChat = new EbChat();
			this.ebChat.text = msg;
			if (!this.ebChat.text || this.ebChat.text.trim().length <= 0) return;

			this.ebChat.userName = demande.user.nom + " " + demande.user.prenom;
			this.ebChat.xEbUser = demande.user;
			this.ebChat.module = Modules.TRANSPORT_MANAGEMENT;
			this.ebChat.idFiche = demande.ebDemandeNum;
			this.ebChat.idChatComponent = IDChatComponent.TRANSPORT_MANAGEMENT;
			this.ebChat.isHistory = false;

			this.chatService
				.addChat(Statique.controllerStatiqueListe + "/addChat", this.ebChat)
				.subscribe((data) => {
					this.ebChat = null;
					if (
						this.message &&
						demande.ebDemandeNum ==
							this.message.tabData[this.message.tabData.length - 1].ebDemandeNum
					) {
						this.messageChat = null;
						if (event != null) requestProcessing.afterGetResponse(event);
					}
				});
		}
	}

	sortListRDVByCreationDate(listPropRdvPk) {
		listPropRdvPk.sort(function(o1, o2) {
			if (o1.dateCreation > o2.dateCreation) return -1;
			else if (o1.dateCreation < o2.dateCreation) return 1;
			else return 0;
		});
	}
}
