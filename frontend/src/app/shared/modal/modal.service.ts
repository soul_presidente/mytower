import { Injectable } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Observable, Subject } from "rxjs";

//import { BehaviorSubject }    from 'rxjs/BehaviorSubject';

@Injectable()
export class ModalService {
	closeResult: string;
	enableLineBreak: boolean = false;
	private subject = new Subject<any>();
	typeChoice:string;

	constructor(protected ngbService: NgbModal) {}

	deleteGroupedRule(
		header: string,
		message: string,
		msgUnconfirmedProposals: string,
		keepFn: Function = () => {},
		deleteFn: Function = () => {},
		noFn: Function = () => {},
		enableLineBreak: boolean = false,
		yesMessage: string = "",
		noMessage: string = "") {
		this.setConfirmationGroupingRule(header, message, msgUnconfirmedProposals, keepFn,deleteFn, noFn, enableLineBreak, yesMessage, noMessage);
		
	
	}



	confirm(
		header: string,
		message: string,
		siFn: Function = () => {},
		noFn: Function = () => {},
		enableLineBreak: boolean = false,
		yesMessage: string = "",
		noMessage: string = ""
	) {
		this.setConfirmation(header, message, siFn, noFn, enableLineBreak, yesMessage, noMessage);
	}

	confirmPromise(
		header: string,
		message: string,
		siFn: Function = () => {},
		noFn: Function = () => {},
		enableLineBreak: boolean = false,
		yesMessage: string = "",
		noMessage: string = ""
	) {
		return new Promise((resolve, reject) => {
			this.setConfirmation(
				header,
				message,
				function() {
					resolve(true);
				},
				function() {
					resolve(false);
				},
				enableLineBreak,
				yesMessage,
				noMessage
			);
		});
	}

	information(header: string, message: string, siFn: Function = () => {}) {
		this.setInformation(header, message, siFn);
	}

	template(header: string, template, siFn: Function = () => {}) {
		let that = this;
		this.subject.next({
			type: "template",
			header: header,
			text: template,
			siFn: function() {
				that.subject.next();
				if (siFn) siFn();
			},
		});
	}

	saveFromQualitaire(params, header, cb: Function = () => {}) {
		let that = this;
		that.subject.next({
			type: "save-form-qualitaire",
			params: params,
			header: header,
			siFn: function(result, event) {
				if (result) cb(result, event, that);
				else that.subject.next();
			},
		});
	}

	goodsPickupUpdater(params, tabData, cb: Function = () => {}) {
		let that = this;
		this.subject.next({
			type: "goods-pickup-updater",
			params: params,
			tabData: tabData,
			siFn: function(result, event) {
				if (result) cb(result, event, that);
				else that.subject.next();
			},
		});
	}

	error(header: string, template, siFn: Function = () => {}) {
		let that = this;
		let text = template;
		if (!text) {
			text = `
				<div class="row">
					<div class="col-md-4"><img class="width-100-important" src="/assets/images/error.png"></img></div>
					<div class="col-md-8">Whoooops, we are sorry, the page cannot be loaded. We are investigating the issue !</div>
				</div>
			`;
		}
		this.subject.next({
			type: "template",
			header: header,
			text: text,
			siFn: function() {
				that.subject.next();
				if (siFn) siFn();
			},
		});
	}


	setConfirmation(
		header: string,
		message: string,
		siFn: Function = () => {},
		noFn: Function = () => {},
		enableLineBreak: boolean = false,
		yesMessage: string,
		noMessage: string
	) {
		let that = this;
		this.subject.next({
			type: "confirm",
			header: header,
			text: message,
			yesMessage: yesMessage,
			noMessage: noMessage,
			enableLineBreak: enableLineBreak,
			siFn: function() {
				that.subject.next();
				if (siFn) siFn();
			},
			noFn: function() {
				that.subject.next();
				if (noFn) noFn();
			},
		});
	}

	setConfirmationGroupingRule(
		header: string,
		message: string,
		msgUnconfirmedProposals: string,
		keepFn: Function = () => {},
		deleteFn: Function = () => {},
		noFn: Function = () => {},
		enableLineBreak: boolean = false,
		yesMessage: string,
		noMessage: string
	) {
		let that = this;
		this.subject.next({
			type: "confirm-grouping-rule",
			header: header,
			text: message,
			msgUnconfirmedProposals: msgUnconfirmedProposals,
			yesMessage: yesMessage,
			noMessage: noMessage,
			enableLineBreak: enableLineBreak,
			keepFn: function() {
				that.subject.next();
				if (keepFn) keepFn();
			},
			deleteFn: function() {
				that.subject.next();
				if (deleteFn) deleteFn();
			},
			noFn: function() {
				that.subject.next();
				if (noFn) noFn();
			},
		});
	}

	setInformation(header: string, message: string, siFn: Function = () => {}) {
		let that = this;
		this.subject.next({
			type: "information",
			header: header,
			text: message,
			siFn: function() {
				that.subject.next();
				if (siFn) siFn();
			},
		});
	}

	getMessage(): Observable<any> {
		return this.subject.asObservable();
	}
}
