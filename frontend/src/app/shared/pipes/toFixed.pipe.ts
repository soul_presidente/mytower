import { Pipe } from "@angular/core";
import { Statique } from '@app/utils/statique';

@Pipe({
	name: "tofixed",
})
export class ToFixedPipe {
	transform(val: any, args: number) {
		let max = args;

		if (!Statique.isDefined(val)) {
			return "";
		}

		if (!Statique.isDefined(args)) {
			max = 2;
		}

		return (+val).toFixed(max);
	}
}
