import { Pipe } from "@angular/core";
import { Statique } from '@app/utils/statique';
import { MtgDateUtils } from '@app/utils/mtgDateUtils';

@Pipe({
	name: "dtFormat",
})
export class DateFormatterPipe {
	constructor() {}

	public transform( value: any, type: string = null ): string
	{
		let withTz = false;

		if (type && type == "tz") {
			withTz = true;
		}
		else if(type && type == "ago"){
			return MtgDateUtils.dateToDaysAgoString(value);
		}
		return MtgDateUtils.dateToUserTzString(value, withTz);
	}
}
