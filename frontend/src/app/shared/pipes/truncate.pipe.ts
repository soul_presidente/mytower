import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'truncate'
})
export class TruncatePipe implements PipeTransform {

  transform(value: string, limit: number = 15): any {
  	if(value && value.length > 18){
  		return value.trim().substring(0,limit) + " ...";
	}
    return value;
  }

}
