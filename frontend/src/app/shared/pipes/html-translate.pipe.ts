import { Pipe } from "@angular/core";
import { TranslateService } from '@ngx-translate/core';
import { Statique } from '@app/utils/statique';

@Pipe({
	name: "htmlTranslate",
})
export class HtmlTranslatePipe {
	constructor(private translate: TranslateService) {}

	// value = xxx{{val_to_translate}}xxx
	public transform( value: string, separator: string = "{{|}}" ): string
	{
		let seps = separator.split("|");
		let res = value ? value.toString() : "";

		if(seps.length != 2)
			return res;


		let listStr = Statique.getListWord(value, seps[0], seps[1]);
		listStr && listStr.forEach(it => {
			res = res.replace(seps[0] + it + seps[1], this.translate.instant(it));
		});

		return res;
	}

}
