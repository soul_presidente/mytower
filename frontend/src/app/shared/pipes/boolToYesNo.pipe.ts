import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "boolToYesNo" })
export class BoolToYesNoPipe implements PipeTransform {
	transform(value) {
		return value ? "GENERAL.YES" : "GENERAL.NO";
	}
}
