import { Pipe, PipeTransform } from '@angular/core';
import {TranslateService} from "@ngx-translate/core";
import {DecimalPipe} from "@angular/common";

@Pipe({
  name: 'localNumberFormat'
})
export class LocalNumberFormatPipe implements PipeTransform {

	constructor(private translateService : TranslateService){}

	transform(value: any, format: string = null) {

		let currentLang = this.translateService.currentLang;
		if (value == null) {
			return "";
		}
		if(currentLang == 'fr'){
			currentLang +='-FR';
			if(value != null)
				value = Number(value.toString().replace(',','.').replace(/\s/g, ''));
		}else{
			value = Number(value.toString().replace(',',''));
		}
		if (!format) {
			format = "1.";
		}
		 return  new DecimalPipe(currentLang).transform(value,format);
	}

}
