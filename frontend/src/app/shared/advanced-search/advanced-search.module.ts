import { NgModule } from "@angular/core";
import { SharedModule } from "@app/shared/module/shared.module";
import { NgSelectModule } from "@ng-select/ng-select";

@NgModule({
	imports: [SharedModule, NgSelectModule],
	declarations: [],
	exports: [NgSelectModule],
})
export class AdvancedSearchModule {}
