import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";

@Component({
	selector: "app-advanced-search",
	templateUrl: "./advanced-search.component.html",
	styleUrls: ["./advanced-search.component.css"],
})
export class AdvancedSearchComponent implements OnInit {
	@Input()
	module: number;
	@Output()
	onSearch = new EventEmitter<SearchCriteriaPricingBooking>();

	constructor(public generaleMethode: generaleMethodes) {}

	ngOnInit() {}

	panelCollapse(panel, event) {
		this.generaleMethode.panelCollapse(panel, event);
	}

	search(search: SearchCriteriaPricingBooking) {
		this.onSearch.emit(search);
	}
}
