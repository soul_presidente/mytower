import {
	ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	Output,
	ElementRef,
	Renderer,
} from "@angular/core";
import { generaleMethodes } from "../generaleMethodes.component";
import { MessageService } from "primeng/api";
import { Router } from "@angular/router";
import { GenericTableService } from "@app/services/generic-table.service";
import { GenericTableComponent } from "../generic-table/generic-table.component";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EtablissementService } from "@app/services/etablissement.service";
import { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { TranslateService } from "@ngx-translate/core";
import { ExportService } from "@app/services/export.service";
import { FavoriService } from "@app/services/favori.service";
import { Modules } from "@app/utils/enumeration";
import { GenericTableInfos } from "../generic-table/generic-table-infos";
import { CustomFieldGenericCell } from "../generic-cell/commons/custom-field.generic-cell";

@Component({
	selector: "app-generic-table-ccp",
	templateUrl: "../generic-table/generic-table.component.html",
	styleUrls: [
		"../generic-table/generic-table.component.scss",
		"./generic-table-ccp.component.scss",
	],
})
export class GenericTableCcpComponent extends GenericTableComponent {
	@Output()
	listCategorieEmitter = new EventEmitter<Array<EbCategorie>>();
	@Output()
	listFieldsEmitter = new EventEmitter<Array<IField>>();
	@Input()
	module: number;
	@Input()
	dataUpload?: any;

	@Input()
	customFieldsEditable: boolean = false;

	@Input()
	showCustomFieldsMainTable: boolean = true;

	@Input()
	showCategoriesMainTable: boolean = true;

	@Input()
	showCustomFieldsSubTable: boolean = false;

	@Input()
	showCategoriesSubTable: boolean = false;

	@Input()
	customFieldsToSkip: Array<string>;

	constructor(
		protected generaleMethode: generaleMethodes,
		protected translate: TranslateService,
		protected exportService: ExportService,
		protected router: Router,
		protected service: GenericTableService,
		protected messageService: MessageService,
		protected favoriService: FavoriService,
		protected cd: ChangeDetectorRef,
		private etablissementService: EtablissementService,
		protected elRef: ElementRef,
		protected renderer: Renderer
	) {
		super(
			generaleMethode,
			translate,
			exportService,
			router,
			service,
			messageService,
			favoriService,
			cd,
			elRef,
			renderer
		);
	}

	loadCustomColumns() {
		return new Promise((resolve) => {
			if (this.isPrestataire) {
				return resolve();
			}

			this.getCustomAndCategorie(
				(listCategorie, listCustomField, cols: Array<GenericTableInfos.Col>) => {
					this.listCategorieEmitter.emit(listCategorie || []);
					this.listFieldsEmitter.emit(listCustomField || []);

					/*
				cols &&
					cols.forEach((col) => {
						let fd = this.dataInfos.cols.find((it) => it.field == col.label);
						!fd && this.dataInfos.cols.push(col);
						if (this.detailsInfos != null) {
							let fdD = this.detailsInfos.cols.find((it) => it.field == col.label);
							!fdD && this.detailsInfos.cols.push(col);
						}
					});
				*/

					if (cols) {
						cols.forEach((col) => {
							let canInsertToMainTable = !this.dataInfos.cols.find((it) => it.field == col.field);
							let canInsertToSubTable =
								this.detailsInfos != null &&
								!this.detailsInfos.cols.find((it) => it.field == col.field);

							if (col.genericCellClass && col.genericCellClass === CustomFieldGenericCell) {
								// It's a custom field
								if (
									this.showCustomFieldsMainTable &&
									canInsertToMainTable &&
									(this.customFieldsToSkip && this.customFieldsToSkip.length > 0
										? !this.customFieldsToSkip.includes(col.field)
										: true)
								)
									this.dataInfos.cols.push(col);
								if (this.showCustomFieldsSubTable && canInsertToSubTable)
									this.detailsInfos.cols.push(col);
							} else {
								// It's a categorie
								if (this.showCategoriesMainTable && canInsertToMainTable)
									this.dataInfos.cols.push(col);
								if (this.showCategoriesSubTable && canInsertToSubTable)
									this.detailsInfos.cols.push(col);
							}
						});
					}

					resolve();
				}
			);
		});
	}

	getCustomAndCategorie(callback: Function) {
		let criteria: SearchCriteria = new SearchCriteria();
		let cols: Array<GenericTableInfos.Col> = [];
		let listCustomField = [];
		let listCategorie = [];
		let ebCustomField = null;
		let listFields = [];

		criteria.size = null;
		criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		criteria.ignoreContact = true;
		criteria.ebUserNum = this.userConnected.ebUserNum;
		this.etablissementService.getCategoryCustom(criteria).subscribe((res) => {
			listCategorie = res && res.listCategorie ? res.listCategorie : new Array<EbCategorie>();
			listCustomField = res.customField;

			if (this.isChargeur && listCustomField && listCustomField[0]) {
				ebCustomField = listCustomField[0];
				listFields =
					ebCustomField && ebCustomField.fields
						? JSON.parse(ebCustomField.fields)
						: new Array<Object>();
				listFields &&
					listFields.forEach((it) => {
						this.addCustomFieldToCols(it, cols);
					});
			}

			if (this.isControlTower && listCustomField) {
				listCustomField.forEach((el) => {
					let listFieldsTmp = el && el.fields ? JSON.parse(el.fields) : null;
					if (listFieldsTmp) {
						listFieldsTmp.forEach((it) => {
							this.addCustomFieldToCols(it, cols, el.nomCompagnie);
						});

						listFields = listFields.concat(listFieldsTmp);
					}
				});
			}

			if (listCategorie && listCategorie.length > 0) {
				listCategorie.forEach((it) => {
					cols.push({
						title: it.nomCompagnie,
						header: it["libelle"],
						field: `category${it.ebCategorieNum}`,
						sortable: false,
						renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
						render: (data, type, row) => {
							let val = "";
							let cats = row.listCategories;
							if (cats && cats.length > 0) {
								let o;
								let l;
								for (o in cats) {
									if (cats[o].labels != null && cats[o].ebCategorieNum == it["ebCategorieNum"]) {
										for (l in cats[o].labels) {
											val += cats[o].labels[l].libelle + "<br>";
										}
										break;
									}
								}
							}
							return val;
						},
					});
				});
			}
			callback(listCategorie, listFields, cols);
		});
	}

	addCustomFieldToCols(field: IField, cols: Array<GenericTableInfos.Col>, title = null) {
		cols.push({
			field: field.name,
			sortable: false,
			renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
			renderModeHead: GenericTableInfos.Col.RenderModeTable.HTML,
			header: field.label,
			title: title ? title : field["name"],
			genericCellClass: CustomFieldGenericCell,
			genericCellParams: new CustomFieldGenericCell.Params({
				field: field,
				toolTipRender:
					this.isControlTower && this.dataInfos.ccpTooltipRender
						? (model) => {
								return this.dataInfos.ccpTooltipRender(model);
						  }
						: null,
				readOnlyOn: (model) => !this.customFieldsEditable,
				showOnHover: true,
			}),
		});
	}
}
