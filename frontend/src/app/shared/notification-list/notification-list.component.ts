import { NotificationType } from "@app/utils/enumeration";
import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from "@angular/core";
import { NgbTabset } from "@ng-bootstrap/ng-bootstrap";
import { NotificationService } from "@app/services/notification.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { TranslateService } from "@ngx-translate/core";
import { EbNotification } from "@app/classes/EbNotification";

@Component({
	selector: "app-notification-list",
	templateUrl: "./notification-list.component.html",
	styleUrls: ["./notification-list.component.scss"],
})
export class NotificationListComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	notifications: Array<EbNotification>;

	@Input()
	alerts : Array<EbNotification>;

	@Input()
	messages : Array<EbNotification>;

	@Input()
	nbrAlerts: number = 0;

	@Input()
	nbrNotifs: number = 0;

	@Input()
	nbrMsgs: number = 0;

	@Output()
	emitTotalNotifNbr: EventEmitter<number> = new EventEmitter<number>();

	@Output()
	emitNbrAlerts: EventEmitter<number> = new EventEmitter<number>();

	@Output()
	emitNbrNotifs: EventEmitter<number> = new EventEmitter<number>();

	@Output()
	emitNbrMsgs: EventEmitter<number> = new EventEmitter<number>();

	totalNotifsNbr: number = 0;

	@ViewChild("tabset", { static: false })
	ngbTabSet: NgbTabset;

	selectedTab: Number = 0;

	pageNumberAlert : number = 1;
	pageNumberNotif : number = 1;
	pageNumberMsg : number = 1;

	constructor(
		protected notificationService: NotificationService,
		protected translate: TranslateService
	) {
		super();
	}

	ngOnInit() {
		this.computeToTalNbrNotifs();
	}

	onTabChange($event) {
		if ($event.nextId == "alert") {
			if (this.nbrAlerts > 0) this.resetNotifNumber(NotificationType.ALERT);
			this.nbrAlerts = 0;
			this.emitNbrAlerts.emit(this.nbrAlerts);
		}
		if ($event.nextId == "notify") {
			if (this.nbrNotifs > 0) this.resetNotifNumber(NotificationType.NOTIFY);
			this.nbrNotifs = 0;
			this.emitNbrNotifs.emit(this.nbrNotifs);
		}
		if ($event.nextId == "msg") {
			if (this.nbrMsgs > 0) this.resetNotifNumber(NotificationType.MSG);
			this.nbrMsgs = 0;
			this.emitNbrMsgs.emit(this.nbrMsgs);
		}

		this.computeToTalNbrNotifs();
	}

	computeToTalNbrNotifs() {
		this.totalNotifsNbr = this.nbrAlerts + this.nbrMsgs + this.nbrNotifs;
		this.emitTotalNotifNbr.emit(this.totalNotifsNbr);
	}

	get alertsTitle(): string {
		return (
			this.translate.instant("GENERAL.ALERT") +
			(this.nbrAlerts > 0 ? "(" + this.nbrAlerts + ")" : "")
		);
	}

	get notifsTitle(): string {
		return (
			this.translate.instant("GENERAL.NOTIF") +
			(this.nbrNotifs > 0 ? "(" + this.nbrNotifs + ")" : "")
		);
	}

	get msgTitle(): string {
		return (
			this.translate.instant("GENERAL.MSG") + (this.nbrMsgs > 0 ? "(" + this.nbrMsgs + ")" : "")
		);
	}

	async resetNotifNumber(notifType: number) {
		this.notificationService
			.updateNotifNbrForUser(this.userConnected.ebUserNum, notifType, 0)
			.subscribe((res) => {});
	}

	onScroll(){
		if (this.ngbTabSet.activeId == "alert") {
			this.pageNumberAlert = this.pageNumberAlert + 1;
			this.notificationService.getListNotification(this.userConnected.ebUserNum, NotificationType.ALERT, this.pageNumberAlert)
			.subscribe((data: Array<EbNotification>) => {
				this.alerts.push(...data);
			});
		}else if (this.ngbTabSet.activeId == "notify") {
			this.pageNumberNotif = this.pageNumberNotif + 1;
			this.notificationService.getListNotification(this.userConnected.ebUserNum, NotificationType.NOTIFY, this.pageNumberNotif)
			.subscribe((data: Array<EbNotification>) => {
				this.notifications.push(...data);
			});
		}else if (this.ngbTabSet.activeId == "msg") {
			this.pageNumberMsg = this.pageNumberMsg + 1;
			this.notificationService.getListNotification(this.userConnected.ebUserNum, NotificationType.MSG, this.pageNumberMsg)
			.subscribe((data: Array<EbNotification>) => {
				this.messages.push(...data);
			});
		}
	}
}
