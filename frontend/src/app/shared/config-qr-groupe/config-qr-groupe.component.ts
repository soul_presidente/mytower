import {
	Component,
	EventEmitter,
	OnInit,
	Output,
	ViewChild,
	OnChanges,
	SimpleChanges,
} from "@angular/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { GenericTableScreen, Modules } from "@app/utils/enumeration";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { EbQrGroupe } from "@app/classes/qrGroupe";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { EbCompagnie } from "@app/classes/compagnie";
import { PricingService } from "@app/services/pricing.service";
import { StatiqueService } from "@app/services/statique.service";
import { QrGroupeCriteria } from "@app/classes/qrGroupeCriteria";
import { HeaderInfos, BreadcumbComponent } from "../header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { TranslateService } from "@ngx-translate/core";
import { QrGroupeValue } from "@app/classes/qrGroupeValue";
import { QrGroupeObject } from "@app/classes/qrGroupeObject";
import { QrGroupeCategorie } from "@app/classes/qrGroupeCategorie";
import { MessageService } from "primeng/api";
import { QrConsolidationService } from "@app/services/qrConsolidation.service";

enum QrGroupeFunctionType {
	ARE_COMPATIBILE = 1,
	ARE_EQUAL = 2,
	ARE_EXCLUDED = 3,
}
enum QrGroupeDragType {
	VALUE = 1,
	FIELD = 2,
	FUNCTION = 3,
	SOUS_FUNCTION = 4,
}
enum GroupeField {
	MODE_DE_TRANSPORT = 1,
	ZONE_ORIGINE = 2,
	ZONE_DESTINATION = 3,
	ORIGINE = 4,
	DESTINATION = 5,
	NIVEAU_DE_SERVICE = 6,
	TYPE_DE_TRANSPORT = 7,
	CUSTOMS_PROCEDURE = 8,
	DGR_TYPE = 9,
	FLAG = 10,
	CATEGORIES = 11,
	COMPANY_ORIGIN = 12,
	COMPANY_DESTINATION = 13,
	INCOTERM = 14,
	VILLE_INCOTERM = 15,
	DATE_AVAILABILITY = 16,
	DATE_ARRIVAL = 17,
	ELIGIBILITE = 18,
	TRANSPORTEUR = 19,
	ZONE_ORIGINE_GT = 20,
	ZONE_DESTINATION_GT = 21,
	CUSTOM_FIELDS = 22,
	PART_NUMBER = 23,
	MAPPING_CODE_PSL = 24,
	STATUS = 25,
}
enum GroupeSousFunction {
	START_BY = 1,
	FINISH_BY = 2,
	CONTAINS_VALUE = 3,
	EXCLUDE = 4,
}

const PADDING_FIELD_PSL = 100;

@Component({
	selector: "app-config-qr-groupe",
	templateUrl: "./config-qr-groupe.component.html",
	styleUrls: ["./config-qr-groupe.component.scss"],
})
export class ConfigQrGroupeComponent extends ConnectedUserComponent implements OnInit {
	Module = Modules;
	Statique = Statique;

	QrGroupeFunctionType = QrGroupeFunctionType;
	QrGroupeDragType = QrGroupeDragType;
	GroupeSousFunction = GroupeSousFunction;
	GroupeField = GroupeField;

	listEbQrGroupe: Array<EbQrGroupe> = new Array<EbQrGroupe>();
	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	isStatiquesLoaded = false;
	isAddObject = false;
	dataObject: EbQrGroupe = new EbQrGroupe();
	backupEditObject: EbQrGroupe;
	editingIndex: number;
	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;
	isActive: boolean;
	listVentilation: Array<any>;
	listOutput: Array<any>;
	listTrigger: Array<any>;
	listField: Array<any>;
	listFunction: Array<QrGroupeObject>;
	listSousFunction: Array<QrGroupeObject>;
	listMainSousFunction: Array<QrGroupeObject>;
	listPriority: Array<number>;
	listQrCategories: Array<QrGroupeCategorie>;
	listQrCustomFields: Array<QrGroupeObject>;
	listQrCustomFieldValues: Array<String>;

	draggedItem: any;
	dragType: number;
	exclusions: Array<QrGroupeCriteria> = new Array<QrGroupeCriteria>();
	inclusions: Array<QrGroupeCriteria> = new Array<QrGroupeCriteria>();
	listIncoterCityFiltred: Array<String> = new Array<String>();
	typeChoice: string;

	constructor(
		protected modalService?: ModalService,
		protected pricingService?: PricingService,
		protected qrConsolidation?: QrConsolidationService,
		protected statiqueService?: StatiqueService,
		protected headerService?: HeaderService,
		protected translate?: TranslateService,
		private messageService?: MessageService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 50;

		this.dataInfos.cols = [
			{
				field: "libelle",
				header: "Libelle",
				translateCode: "GENERAL.LIBELLE",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data: any, d, row: EbQrGroupe) {
					let str = "";
					if (row.libelle !== undefined && row.libelle !== null) {
						if (row.isActive) {
							str += "<div>" + row.libelle + "</div>";
						} else str += '<div style="color : lightgray">' + row.libelle + "</div>";
					}

					return str;
				}.bind(this),
			},
			{
				field: "description",
				header: "Description",
				translateCode: "GENERAL.DESCRIPTION",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data: any, d, row: EbQrGroupe) {
					let str = "";
					if (row.description !== undefined && row.description !== null) {
						if (row.isActive) {
							str += "<div>" + row.description + "</div>";
						} else str += '<div style="color : lightgray">' + row.description + "</div>";
					}

					return str;
				}.bind(this),
			},
			{
				field: "compatibilityCriteria",
				header: "Compatibility criteria",
				translateCode: "GENERAL.COMPTABILITY_CRITERIA",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data: any, d, row: EbQrGroupe) {
					let str = "";
					data &&
						data.forEach((it, index) => {
							if (it.libelle !== undefined && it.libelle !== null) {
								if (row.isActive) {
									str += `<div style="text-align: left; padding-left: 5px;">${
										index
											? it.isAnd
												? this.translate.instant("GROUPING.COMMON.AND")
												: this.translate.instant("GROUPING.COMMON.OR")
											: ""
									} <span style="font-weight: bold"> ${this.translate.instant(
										"GROUPING.COMMON." + it.libelle
									)} (...)</span></div>`;
								} else {
									str += `<div style="text-align: left; padding-left: 10px; color : lightgray">${
										index
											? it.isAnd
												? this.translate.instant("GROUPING.COMMON.AND")
												: this.translate.instant("GROUPING.COMMON.OR")
											: ""
									} <span style="font-weight: bold"> ${this.translate.instant(
										"GROUPING.COMMON." + it.libelle
									)} (...)</span></div>`;
								}
							}
						});
					return str;
				}.bind(this),
			},
			{
				field: "exclusionCriteria",
				header: "Exclusion criteria",
				translateCode: "GENERAL.EXCLUSION_CRITERIA",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data: any, d, row: EbQrGroupe) {
					let str = "";
					data &&
						data.forEach((it) => {
							if (it.libelle !== undefined && it.libelle !== null) {
								if (row.isActive) {
									str += "<div>" + it.libelle + "</div>";
								} else str += '<div style="color : lightgray">' + it.libelle + "</div>";
							}
						});
					return str;
				}.bind(this),
			},
			{
				field: "ventilation",
				header: "Ventilation",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data: any, d, row: EbQrGroupe) {
					let str = "";
					let res: string | undefined;
					res = (this.listVentilation.find((it) => it.code == data) || {}).libelle;
					if (res !== undefined && res !== null) {
						if (row.isActive) {
							str += "<div> " + res + " </div>";
						} else str += '<div style="color : lightgray">' + res + "</div>";
					}
					return str;
				}.bind(this),
			},
			// { TODO: A reprendre après la MEP
			// 	field: "trigger",
			// 	header: "Trigger",
			// 	render: function(data: any, d, row: EbQrGroupe) {
			// 		return data ? (this.listTrigger.find((it) => it.code == data) || {}).libelle : null;
			// 	}.bind(this),
			// },
			// {
			// 	field: "output",
			// 	header: "Output",
			// 	render: function(data: any, d, row: EbQrGroupe) {
			// 		return data ? (this.listOutput.find((it) => it.code == data) || {}).libelle : null;
			// 	}.bind(this),
			// },
		];
		this.dataInfos.dataKey = "ebQrGroupeNum";
		this.dataInfos.showAddBtn = true;
		let criteria = new SearchCriteria();
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerQrConsolidation + "/list-config-qrgroupe";
		this.dataInfos.dataType = EbQrGroupe;
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.lineActions = [];

		let activateTitle: string = "Activate";
		this.translate.get("REPOSITORY_MANAGEMENT.ACTIVATE").subscribe((res: string) => {
			activateTitle = res;
		}),
			this.dataInfos.lineActions.push({
				class: "btn btn-xs  font-size-10",
				icon: "fa fa-power-off",
				title: activateTitle,
				showCondition: (row: EbQrGroupe) => row.isActive != true,
				onClick: function(row: EbQrGroupe, $event) {
					this.activateGroupedRule($event, row);
				}.bind(this),
			});

		let editTitle: string = "Edit";
		this.translate.get("GENERAL.EDIT").subscribe((res: string) => {
			editTitle = res;
		});
		this.dataInfos.lineActions.push({
			class: "btn btn-xs  font-size-10",
			icon: "fa fa-edit",
			title: editTitle,
			showCondition: (row: EbQrGroupe) => row.isActive == true,
			onClick: function(row: EbQrGroupe) {
				this.diplayEdit(row);
			}.bind(this),
		});

		let delegateTitle: string = "Delete the grouping rule";
		this.translate.get("GENERAL.DELEGROUPINGRULE").subscribe((res: string) => {
			delegateTitle = res;
		}),
			this.dataInfos.lineActions.push({
				class: "btn btn-xs  font-size-10",
				icon: "fa fa-ban",
				title: delegateTitle,
				showCondition: (row: EbQrGroupe) => row.isActive == true,
				onClick: function(row: EbQrGroupe) {
					this.deleteGroupedRule(row);
				}.bind(this),
			});

		this.loadData();

		this.loadListIncotercity();
	}

	activateGroupedRule($event, selectedRule: EbQrGroupe) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);
		selectedRule.isActive = true;
		this.qrConsolidation.activateQrGroupe(selectedRule.ebQrGroupeNum).subscribe((data) => {
			requestProcessing.afterGetResponse($event);
			this.isAddObject = false;
			this.editingIndex = null;
			this.backupEditObject = null;
			this.dataObject = null;
			this.genericTable.refreshData();
		});
	}

	async loadData() {
		let criteria = new SearchCriteria();
		// criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		let isActive: boolean;
		this.qrConsolidation.getListQrGroupeData(criteria).subscribe((res) => {
			this.listVentilation = JSON.parse(res.listVentilation);
			this.listTrigger = JSON.parse(res.listTrigger);
			this.listOutput = JSON.parse(res.listOutput);
			this.listFunction = JSON.parse(res.listFunction);
			this.listSousFunction = JSON.parse(res.listSousFunction);
			this.listField = JSON.parse(res.listField);
			//la liste des psl est desactivé pour le moment
			// if (res.listFieldPsl) {
			// 	res.listFieldPsl.forEach((it) => {
			// 		this.listField.push({
			// 			code: it.ebTtCompanyPslNum + PADDING_FIELD_PSL,
			// 			libelle: it.libelle,
			// 		});
			// 	});
			// }

			if (res.fieldValues) {
				for (let o in res.fieldValues) {
					this.listField.forEach((field) => {
						if ("field" + field.code == o && field.code != GroupeField.CATEGORIES) {
							if ("field" + field.code == o && field.code == GroupeField.CUSTOM_FIELDS) {
								this.listQrCustomFields = Statique.cloneListObject(
									JSON.parse(res.fieldValues[o]),
									QrGroupeObject
								);
							} else {
								field.values = Statique.cloneListObject(
									JSON.parse(res.fieldValues[o]),
									QrGroupeObject
								);
							}
						} else if ("field" + field.code == o && field.code == GroupeField.CATEGORIES) {
							this.listQrCategories = Statique.cloneListObject(
								JSON.parse(res.fieldValues[o]),
								QrGroupeCategorie
							);
						}

						if (
							field.code == GroupeField.ZONE_DESTINATION_GT &&
							"field" + GroupeField.ZONE_ORIGINE_GT == o
						) {
							field.values = Statique.cloneListObject(
								JSON.parse(res.fieldValues[o]),
								QrGroupeObject
							);
						}
					});
				}
			}
			if (this.listSousFunction) {
				this.listMainSousFunction = this.listSousFunction.filter((v, i) => i >= 2);
			}
			//traduction des types de ventilation
			if (this.listVentilation) {
				this.listVentilation.forEach((ven) => {
					if (ven.code) {
						ven.libelle = this.translate.instant("GROUPING.QR.VENTILATION.VEN-" + ven.code);
					}
				});
			}
		});

		this.isStatiquesLoaded = true;
	}

	diplayEdit(dataObject: EbQrGroupe) {
		this.dataObject = Statique.cloneObject(dataObject, new EbQrGroupe());
		this.onShowAddEditDisplay(dataObject);
		this.backupEditObject = Statique.cloneObject<EbQrGroupe>(this.dataObject, new EbQrGroupe());
		this.displayAdd(true);
	}

	onShowAddEditDisplay(dataObject: EbQrGroupe) {
		this.inclusions = Statique.cloneObject(
			dataObject.compatibilityCriteria || [],
			new Array<any>()
		);
		this.exclusions = Statique.cloneObject(dataObject.exclusionCriteria || [], new Array<any>());
	}

	displayAdd(isUpdate?: boolean) {
		if (isUpdate) {
		} else this.dataObject = new EbQrGroupe();

		let listGroups = this.genericTable.datas;
		//la liste de priorité doit etre unique par groupe et par compagnie
		//les priorités commancent par 1 jusqu'au 9..
		//si une priorité est deja choisi alors la liste doit etre incrementé par 1.
		// exp: si le 1 est deja choisi alors pour l'ajout d'un nouveau groupe la liste doit etre sous la forme [2, 3, 4, 5, 6, 7, 8, 9,10]
		this.listPriority = [1, 2, 3, 4, 5, 6, 7, 8, 9];
		if (listGroups && listGroups.length > 0) {
			listGroups.forEach((gr) => {
				if (gr.grpOrder && (!isUpdate || this.dataObject.grpOrder != gr.grpOrder)) {
					this.listPriority = this.listPriority.filter((obj) => obj !== gr.grpOrder);
					let priority = this.listPriority[this.listPriority.length - 1] + 1;
					if (gr.grpOrder == priority) {
						priority++;
					}
					this.listPriority.push(priority);
				}
			});
		}
		//set list values
		if (this.dataObject && this.dataObject.compatibilityCriteria) {
			this.dataObject.compatibilityCriteria.forEach((criteria) => {
				if (
					(criteria.code == QrGroupeFunctionType.ARE_COMPATIBILE ||
						criteria.code == QrGroupeFunctionType.ARE_EXCLUDED) &&
					criteria.groupValues &&
					criteria.field.code != GroupeField.CATEGORIES
				) {
					criteria.groupValues.forEach((value) => {
						value.listValues = this.listField.find(
							(elem) => elem.code == criteria.field.code
						).values;
						if (!value.value) {
							value.value = new QrGroupeObject();
						}
					});
				} else if (
					(criteria.code == QrGroupeFunctionType.ARE_COMPATIBILE ||
						criteria.code == QrGroupeFunctionType.ARE_EXCLUDED) &&
					criteria.groupValues &&
					criteria.field.code == GroupeField.CATEGORIES
				) {
					criteria.groupValues.forEach((value) => {
						this.onChangeCatgorie(value);
					});
				}
			});
		}

		this.onShowAddEditDisplay(this.dataObject);

		this.isAddObject = true;
		if (this.dataObject && this.dataObject.triggers)
			this.dataObject.triggers = this.dataObject.triggers.split(",").map(Number);
	}

	backToListQrGroupe() {
		if (this.editingIndex != null) {
			Statique.cloneObject<EbQrGroupe>(
				this.backupEditObject,
				this.listEbQrGroupe[this.editingIndex]
			);
			this.editingIndex = null;
		}
		this.dataObject = null;
		this.backupEditObject = null;
		this.isAddObject = false;
		this.listQrCustomFieldValues = null;
	}
	editProposition($event) {
		this.onAddEditData();
		this.dataObject.xEbCompagnie = new EbCompagnie();
		this.dataObject.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);
		if (this.dataObject && this.dataObject.triggers) {
			this.dataObject.triggers = this.dataObject.triggers.join(",");
		}
		this.qrConsolidation.updateQrGroupe(this.dataObject).subscribe((data) => {
			requestProcessing.afterGetResponse($event);
			this.isAddObject = false;
			this.editingIndex = null;
			this.backupEditObject = null;
			this.dataObject = null;
			this.genericTable.refreshData();
		});
	}
	addProposition($event) {
		this.dataObject.xEbCompagnie = new EbCompagnie();
		this.dataObject.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.onAddEditData();

		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);
		if (this.dataObject && this.dataObject.triggers) {
			this.dataObject.triggers = this.dataObject.triggers.join(",");
		}
		this.qrConsolidation.addQrGroupe(this.dataObject).subscribe(
			(data: EbQrGroupe) => {
				requestProcessing.afterGetResponse($event);
				this.isAddObject = false;
				this.dataObject = null;
				this.genericTable.refreshData();
			},
			(error) => {
				this.showWarning("GROUPING.COMMON.TOASTER_MSG.MSG_WARN_TO_USE_SAME_LIBELLE");
				requestProcessing.afterGetResponse($event);
			}
		);
	}
	onAddEditData() {
		this.dataObject.updatePropositions = true;
		this.dataObject.compatibilityCriteria = this.inclusions;
		this.dataObject.exclusionCriteria = this.exclusions;
	}

	deleteGroupedRule(schemaPsl: EbQrGroupe) {
		if (schemaPsl.isActive) {
			let $this = this;
			this.modalService.deleteGroupedRule(
				$this.translate.instant("GENERAL.CONFIRMATION"),
				$this.translate.instant("REPOSITORY_MANAGEMENT.DELETE_UNCONFIRMED_PROPOSALS"),
				$this.translate.instant("REPOSITORY_MANAGEMENT.KEEP_UNCONFIRMED_PROPOSALS"),
				function() {
					let dataObject = schemaPsl;
					$this.qrConsolidation.deleteQrGroupe(dataObject.ebQrGroupeNum).subscribe((data) => {
						$this.genericTable.refreshData();
					});
				},
				function() {
					let dataObject = schemaPsl;
					$this.qrConsolidation
						.getListEbDemandeByQrGroup(dataObject.ebQrGroupeNum)
						.subscribe((data) => {
							$this.genericTable.refreshData();
						});
				},
				function() {},
				true
			);
		}
	}

	onDragStart($event, item: any, dragType = null) {
		if (!dragType) return;

		this.draggedItem = item;
		this.dragType = dragType;
	}
	onDragEnd($event) {
		this.draggedItem = null;
	}
	onDropFunction($event, holder) {
		if (!holder || !this.draggedItem || this.dragType != QrGroupeDragType.FUNCTION) return;

		let nextId = 1;
		if (holder.length) nextId += holder.reduce((max, it) => (it.id > max.id ? it : max)).id;
		holder.push({
			...this.draggedItem,
			id: nextId,
			fields: [],
			isAnd: false,
		});
	}
	onDropGroupField($event, fn) {
		if (!this.draggedItem || this.dragType != QrGroupeDragType.FIELD) return;

		if (this.draggedItem.code) {
			if (
				fn.code == QrGroupeFunctionType.ARE_COMPATIBILE ||
				fn.code == QrGroupeFunctionType.ARE_EXCLUDED
			) {
				if (
					this.draggedItem &&
					this.draggedItem.code &&
					this.draggedItem.code != GroupeField.DATE_ARRIVAL &&
					this.draggedItem.code != GroupeField.DATE_AVAILABILITY
				) {
					if (fn.field && fn.field.code != this.draggedItem.code) {
						this.showWarning("GROUPING.COMMON.TOASTER_MSG.MSG_WARN_TO_USE_DIFFERENT_TYPE");
					} else {
						fn.field = Statique.cloneObject(this.draggedItem, new QrGroupeObject());
						if (!fn.groupValues) fn.groupValues = [];
						let value = new QrGroupeValue();
						value.sousFunction = Statique.cloneObject(
							this.listSousFunction.find((elem) => elem.code == 3),
							new QrGroupeObject()
						); // contain value
						if (this.draggedItem && this.draggedItem.values) {
							if (this.draggedItem.code != GroupeField.CUSTOM_FIELDS) {
								value.listValues = Statique.cloneListObject(
									this.draggedItem.values,
									QrGroupeObject
								);
							}
						}
						fn.groupValues.push(value);
					}
				} else {
					this.showWarning("GROUPING.COMMON.TOASTER_MSG.MSG_WARN_TO_USE_DATES_IN_ARE_COMPATIBLE");
				}
			} else {
				fn.field = Statique.cloneObject(this.draggedItem, new QrGroupeObject());
			}
		}
	}
	onDropGroupValues($event, func, grp) {
		if (!this.draggedItem || this.dragType != QrGroupeDragType.VALUE) return;

		if (this.draggedItem.code) {
			console.error("Cannot add a field to group or a function of groups");
			return;
		}
		if (!func.fields || !func.fields.length || func.fields[0].code != this.draggedItem.type) {
			console.error("Cannot add a value on a group without a field type or with a different type");
			return;
		}
		let val = grp.values.find((it) => it.value == this.draggedItem.value);
		if (!val) grp.values.push(this.draggedItem);
		else {
			console.error("Cannot add an existing value");
		}
	}

	addValue(func, funcIndex) {
		this.inclusions.forEach((func, index) => {
			if (funcIndex === index) {
				let value = new QrGroupeValue();
				value.sousFunction = this.listSousFunction.find((elem) => elem.code == 3); // contain value
				value.listValues = this.listField.find((elem) => elem.code == func.field.code).values;
				func.groupValues.push(value);
			}
		});
	}

	async deleteFunction(item, isToExclude = false) {
		let $this = this;
		let holder: Array<any> = isToExclude ? this.exclusions : this.inclusions;
		let index = holder.findIndex((fn) => fn.id == item.id);
		if (index >= 0) {
			let res = await this.modalService.confirmPromise(
				$this.translate.instant("GROUPING.COMMON.FUNCTION_CONFIRM_DELETE_HEADER"),
				$this.translate.instant("GROUPING.COMMON.FUNCTION_CONFIRM_DELETE_MSG")
			);
			if (res) holder.splice(index, 1);
		}
	}
	async deleteField(item, field) {
		let $this = this;
		let res = await this.modalService.confirmPromise(
			$this.translate.instant("GROUPING.COMMON.FIELD_CONFIRM_DELETE_HEADER"),
			$this.translate.instant("GROUPING.COMMON.FIELD_CONFIRM_DELETE_MSG")
		);
		if (res) {
			item.field = null;
			if (
				item.code == QrGroupeFunctionType.ARE_COMPATIBILE ||
				item.code == QrGroupeFunctionType.ARE_EXCLUDED
			)
				item.groupValues = [];
		}
	}
	async deleteValue(grp, value) {
		let $this = this;
		let index = grp.values.findIndex((it) => it == value);
		if (index >= 0) {
			let res = await this.modalService.confirmPromise(
				$this.translate.instant("GROUPING.COMMON.VALUE_CONFIRM_DELETE_HEADER"),
				$this.translate.instant("GROUPING.COMMON.VALUE_CONFIRM_DELETE_MSG")
			);
			if (res) grp.values.splice(index, 1);
		}
	}
	async deleteParam(func, grpIndex) {
		let $this = this;
		if (grpIndex >= 0) {
			let res = await this.modalService.confirmPromise(
				$this.translate.instant("GROUPING.COMMON.PARAM_CONFIRM_DELETE_HEADER"),
				$this.translate.instant("GROUPING.COMMON.PARAM_CONFIRM_DELETE_MSG")
			);
			if (res) func.groupValues.splice(grpIndex, 1);
		}
	}

	addFieldValue(inputText, field) {
		if (!inputText.value || !inputText.value.trim().length) return;
		if (!field.values) field.values = [];

		let val = field.values.find((it) => it.value == inputText.value);
		if (!val) {
			field.values.push({
				value: inputText.value,
				type: field.code,
			});
			inputText.value = "";
		}
	}
	private getBreadcrumbItems(isCreation: boolean = false): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.QR_GROUPING",
			},
		];
	}

	public onChangeCatgorie(qrValue) {
		let categorie;
		for (let cat of this.listQrCategories) {
			if (cat.value.code == qrValue.categorieValue.code) {
				categorie = cat;
				break;
			}
		}
		if (categorie && categorie.labels) {
			qrValue.listValues = categorie.labels;
		}
	}
	showWarning(message: string) {
		this.messageService.add({
			life: 10000, //Number of time in milliseconds to wait before closing the message.
			severity: "warn",
			summary: this.translate.instant("GROUPING.COMMON.TOASTER_MSG.WARN_MESSAGE"),
			detail: this.translate.instant(message),
		});
	}
	getListSousFunction(isCompleteList: Boolean): Array<QrGroupeObject> {
		if (isCompleteList) return this.listSousFunction;
		return this.listMainSousFunction;
	}

	onSearchValue(str, qrValue) {
		if (str.term && str.term != "" && qrValue.categorieValue && qrValue.categorieValue.code) {
			let term = "field" + qrValue.categorieValue.code + '":"' + str.term;
			this.pricingService
				.getFieldsByCompagnieNumAndTerm(this.userConnected.ebCompagnie.ebCompagnieNum, term)
				.subscribe((res) => {
					this.listQrCustomFieldValues = res;
				});
		}
	}
	addTag(label: string) {
		return label;
	}
	loadListIncotercity() {
		this.pricingService
			.getListIncotermCity(this.userConnected.ebCompagnie.ebCompagnieNum)
			.subscribe((res) => {
				if (res.length > 0) {
					this.listIncoterCityFiltred = res.filter((elem, i, arr) => {
						if (arr.indexOf(elem) === i) {
							return elem;
						}
					});
				}
			});
	}
}
