import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { EbDemande } from "@app/classes/demande";
import { TypeRequestService } from "@app/services/type-request.service";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbParty } from "@app/classes/party";
import { ConnectedUserComponent } from "../connectedUserComponent";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import { StatiqueService } from "@app/services/statique.service";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbTypeTransport } from "@app/classes/EbTypeTransport";
import {
	CarrierStatus,
	Modules,
	DemandeStatus,
	QrCreationStep,
	NatureDemandeTransport,
	UserRole,
} from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { Router } from "@angular/router";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { EbPslApp } from "@app/classes/pslApp";
import { EbCategorie } from "@app/classes/categorie";

@Component({
	selector: "app-demande-overview",
	templateUrl: "./demande-overview.component.html",
	styleUrls: ["./demande-overview.component.scss"],
})
export class DemandeOverviewComponent extends ConnectedUserComponent implements OnInit {
	NatureDemandeTransport = NatureDemandeTransport;
	DemandeStatus = DemandeStatus;
	Step = QrCreationStep;
	Statique = Statique;
	Syntaxe2Point_Fr_En= localStorage.getItem("currentLang") == "en" ? ":" : " :";
	stepsAccordionIndexes = new Map<number, string>();

	@Input()
	ebDemande: EbDemande;

	@Input()
	typeTransport: string;

	@Input()
	creationMode: boolean = false;

	@Input()
	expandAllAtStart: boolean = false;

	@Input()
	showInfoAboutTransport: boolean;

	@Input()
	showInfoPricing: boolean;

	@Input()
	hideStepArrow: boolean = false;
	@Input()
	canCollapse: boolean = true;
	@Input()
	listModeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();

	@Output()
	onStepSelected: EventEmitter<string> = new EventEmitter<string>();

	listTypeDemandeByCompagnie: Array<EbTypeRequestDTO>;

	listCategories: Array<EbCategorie> = new Array<EbCategorie>();

	listPsl: any;

	incoterm: string;
	activeAccordionIndex: any;
	@Input()
	nbrRequestSent: number = 0;
	@Input()
	nbrReceivedQt: number = 0;
	detailDemandeRoute: string;

	constructor(
		private translate: TranslateService,
		private statiqueService: StatiqueService,
		private typeRequestService: TypeRequestService,
		protected router: Router
	) {
		super();
	}

	ngOnInit() {
		this.detailDemandeRoute = Statique.routeDetailDemandeInPricing();
		this.initAccordionIndex();

		let criteria = new SearchCriteria();
		if (this.ebDemande.user) criteria.connectedUserNum = this.ebDemande.user.ebUserNum;
		this.typeRequestService
			.getListTypeRequests(criteria)
			.subscribe((res: Array<EbTypeRequestDTO>) => {
				this.listTypeDemandeByCompagnie = res;
			});
		if (this.ebDemande.ebDemandeNum) this.getNombreOfRequestSentAndReceivedQt();

		//this.incoterm = this.ebDemande.xEcIncotermLibelle;
		this.getTypeTransportString();
		this.getListCategories();
	}

	initAccordionIndex(){
		let index = 0;
		if (this.expandAllAtStart) {
			this.activeAccordionIndex = "0,1,2,3,4";
		}
		if(this.ebDemande.ebQrGroupe != null){
			this.stepsAccordionIndexes.set(index, QrCreationStep.CONSOLIDATION);
			this.activeAccordionIndex += this.activeAccordionIndex + ",5";
			index=1;
		}
		this.stepsAccordionIndexes.set(index, QrCreationStep.EXPEDITION);
		this.stepsAccordionIndexes.set(index+1, QrCreationStep.REFERENCE);
		this.stepsAccordionIndexes.set(index+2, QrCreationStep.UNITS);
		this.stepsAccordionIndexes.set(index+3, QrCreationStep.PRICING);
		this.stepsAccordionIndexes.set(index+4, QrCreationStep.INFOS_ABOUT_TRANSPORT);
	}

	getListCategories() {
		if (!this.ebDemande.listCategories) return;
		let nbre = this.ebDemande.listCategories.length;
		if (nbre == 1) {
			this.listCategories.push(this.ebDemande.listCategories[0]);
		} else if (nbre > 1) {
			this.listCategories = this.ebDemande.listCategories;
		}
	}

	async getTypeTransportString() {
		let listTypeTransports = await SingletonStatique.getListTypeTransportByModeTransport(
			this.ebDemande.xEcModeTransport
		);
		if (listTypeTransports) {
			const listTypeTr =
				listTypeTransports &&
				listTypeTransports.length &&
				listTypeTransports.filter((typetr) => {
					return typetr.ebTypeTransportNum === this.ebDemande.xEbTypeTransport;
				});
			let message = null;
			this.translate.get("GENERAL.UNDEFINED").subscribe((res: string) => {
				message = res;
			});
			this.typeTransport = listTypeTr && listTypeTr.length ? listTypeTr[0].libelle : message;

		}
	}

	isCarrier(): boolean {
		return this.userConnected.role == UserRole.PRESTATAIRE;
	}

	getTypeRequestRefById(id: number): string {
		if (this.listTypeDemandeByCompagnie == null) return "";
		let ref = "";
		if (id != null)
			for (let elem of this.listTypeDemandeByCompagnie) {
				if (elem.ebTypeRequestNum == id) {
					ref = this.ebDemande.typeRequestLibelle;
					break;
				}
			}
		return ref;
	}

	getModeTransportById(xEcModeTransport) {
		return SingletonStatique.getIconModeTransportById(xEcModeTransport);
	}

	selectStep(stepCode: string) {
		this.onStepSelected.emit(stepCode);
	}

	removeAddressOriginHandler(event: any) {
		this.ebDemande.ebPartyOrigin = new EbParty();
	}

	removeAddressDestHandler(event: any) {
		this.ebDemande.ebPartyDest = new EbParty();
	}

	translatePSL(pslLibelle: string) {
		let psl: string;
		let formatText: string =
			localStorage.getItem("currentLang") == "en"
				? "Estimated " + pslLibelle.toLowerCase() + "Date:"
				: "Date prévue " + pslLibelle.toLowerCase() + " :";
		this.translate
			.get("TRACK_TRACE.ESTIMATED_" + pslLibelle.toUpperCase() + "_DATE")
			.subscribe((data: string) => {
				psl = data.includes("_") ? formatText : data;
			});
		return psl;
	}

	getNombreOfRequestSentAndReceivedQt() {
		let nbrRequestSent = 0;
		let nbrReceivedQt = 0;
		if (
			this.ebDemande.exEbDemandeTransporteurs &&
			this.ebDemande.exEbDemandeTransporteurs.length > 0
		)
			this.ebDemande.exEbDemandeTransporteurs.forEach((dt) => {
				if (dt.status == CarrierStatus.REQUEST_RECEIVED) nbrRequestSent++;
				else if (dt.status == CarrierStatus.QUOTE_SENT || dt.status == CarrierStatus.FINAL_CHOICE)
					nbrReceivedQt++;
			});
		this.nbrRequestSent = nbrRequestSent;
		this.nbrReceivedQt = nbrReceivedQt;
	}

	navigateToInitialtr(ebDemandeNum: number){
		this.router
			.navigateByUrl("/", { skipLocationChange: true })
			.then(() => this.router.navigate([this.detailDemandeRoute + ebDemandeNum]));
	}
}
