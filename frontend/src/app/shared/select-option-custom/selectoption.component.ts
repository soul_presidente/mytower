import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit } from "@angular/core";
import { ConnectedUserComponent } from "../connectedUserComponent";
import { StatiqueService } from "@app/services/statique.service";
import { EbCustomDeclaration } from "@app/classes/declaration";

@Component({
	selector: "app-select-option-custom",
	templateUrl: "./selectoption.component.html",
	styleUrls: ["./selectoption.component.css"],
})
export class SelectOptionComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	sMultiple: string;
	@Input()
	placeholder: string;
	@Input()
	srcTypeahead: string;
	@Input()
	srcListItems: string;
	@Input()
	name: string;
	@Input()
	defaultValue: any;
	@Input()
	fetchAttribute: string;
	@Input()
	fetchValue: any;
	@Input()
	options: any;
	@Input()
	listItems: any;
	@Input()
	groupByAttr?: string;
	@Input()
	fetchGroupByAttribute?: string;
	@Input()
	valueOfModel: string;
	@Input()
	declaration: EbCustomDeclaration;
	@Input()
	indice: any;
	@Input()
	required: boolean;

	test: string;
	declarationDemande: EbCustomDeclaration = new EbCustomDeclaration();

	public multiple: boolean;
	public selectedItem: any;
	public listTermsTypeahead: any;

	typeaheadEvent: EventEmitter<String> = new EventEmitter<string>();

	constructor(protected cd: ChangeDetectorRef, private statiqueService: StatiqueService) {
		super();
	}

	ngOnInit() {
		this.multiple = this.sMultiple == "true";

		if (this.defaultValue) {
			if (this.multiple) this.selectedItem = JSON.parse(this.defaultValue);
			else this.selectedItem = this.defaultValue.length > 0 ? JSON.parse(this.defaultValue)[0] : "";
		}

		if (!this.defaultValue && this.srcTypeahead) this.srcListItems = null;

		this.getListItems();

		//this.setTypeahead();
		this.name = this.name.concat(this.indice);
		if (this.valueOfModel.includes("Curren") || this.valueOfModel.includes("Countr")) {
			this.test = this.valueOfModel.concat("." + this.fetchValue);

			console.log(this.test);
		}
	}

	compareSelectData(it1, it2) {
		if (it1 && it2) {
			return it1[this.fetchAttribute] === it2[this.fetchAttribute];
		}
	}

	getListItems() {
		if (this.srcListItems) {
			this.getListFromSource(this.srcListItems).subscribe(
				function(res) {
					this.listItems = res;
					this.declarationDemande = this.declaration;
				}.bind(this)
			);

			return true;
		} else if (this.listItems) return true;

		return false;
	}

	/* setTypeahead(){
		if(!this.srcTypeahead)
			return false;

		this.typeaheadEvent
			// .distinctUntilChanged()
			// .debounceTime(200)
			.pipe(switchMap(term => this.getTermsFromSource(term, this.srcTypeahead)))
			.subscribe(data => {
				this.cd.markForCheck();

				this.listItems = data;
			}, (err) => {
				this.listItems = [];
			});

		return true;
	} */

	getListFromSource(apiUrl) {
		return this.statiqueService.getListFromSource(apiUrl);
	}
	/* 	getTermsFromSource(term, apiUrl){
		if(term == "" || !term)
			term = "*"
		return this.statiqueService.getTermsFromSource(term, apiUrl);
	} */
}
