import {
	Component,
	ElementRef,
	EventEmitter,
	forwardRef,
	Input,
	OnInit,
	Output,
	HostListener,
	OnChanges,
	SimpleChanges,
} from "@angular/core";
import { NgbDateStruct, NgbTimeStruct } from "@ng-bootstrap/ng-bootstrap";
import { Statique } from "@app/utils/statique";
import {
	ControlValueAccessor,
	FormControl,
	NG_VALIDATORS,
	NG_VALUE_ACCESSOR,
	Validator,
} from "@angular/forms";

const now = new Date();

@Component({
	selector: "app-datetime-picker",
	host: {
		"(document:click)": "onClick($event);onClickOut($event)",
	},
	templateUrl: "./datetime-picker.component.html",
	styleUrls: ["./datetime-picker.component.scss"],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => DatetimePickerComponent),
			multi: true,
		},
		{
			provide: NG_VALIDATORS,
			useExisting: forwardRef(() => DatetimePickerComponent),
			multi: true,
		},
	],
})
export class DatetimePickerComponent implements OnInit, ControlValueAccessor, Validator, OnChanges {
	@Input()
	name?: string;
	@Input()
	disabled?: boolean;
	@Input()
	required?: any;
	@Input()
	placeholder?: string;
	@Input()
	timepicker?: boolean;
	@Input()
	get date(): any {
		return this.dateValue;
	}
	@Input()
	minDate?: any;
	@Input()
	maxDate?: any;
	max = undefined;
	min = undefined;
	dynamicId;
	timepickerToggleOn: boolean;
	firstTimeTp: boolean;
	showDatePickerIcon: boolean = false;
	showTimePickerIcon: boolean = false;
	@Output()
	dateChange = new EventEmitter();
	set date(val: any) {
		if (typeof val === "string") this.dateValue = Statique.stringToDate(val);
		else if (val && (typeof val === "object" || typeof val === "number"))
			this.dateValue = new Date(val);

		if (!val || !this.dateValue) {
			this.dateValue = null;
			this.formattedDate = null;
			this.formattedTime = null;
		} else {
			this.formattedDate = Statique.formatDateToStructure(this.dateValue);
			this.formattedTime = Statique.formatTimeToStructure(this.dateValue);
			this.textFormattedTime =
				Statique.lpad(this.formattedTime.hour) + ":" + Statique.lpad(this.formattedTime.minute);
		}

		this.propagateChange(this.dateValue);
	}

	@Input()
	showErrorMsg: boolean = true;
	time: any;
	dateValue: Date;
	isDateNullValue: Boolean;
	formattedDate: NgbDateStruct;
	formattedTime: NgbTimeStruct;
	textFormattedTime: string;

	constructor(private elementRef: ElementRef) {
		this.timepickerToggleOn = false;
	}
	ngOnChanges(changes: SimpleChanges) {
		if (changes["maxDate"] && changes["maxDate"].previousValue != changes["maxDate"].currentValue) {
			if (typeof this.maxDate.getFullYear === "function") {
				this.max = {
					year: this.maxDate.getFullYear(),
					month: this.maxDate.getMonth() + 1,
					day: this.maxDate.getDate(),
				};
			} else {
				this.max = undefined;
			}
		}

		if (changes["minDate"] && changes["minDate"].previousValue != changes["minDate"].currentValue) {
			if (typeof this.minDate.getFullYear === "function") {
				this.min = {
					year: this.minDate.getFullYear(),
					month: this.minDate.getMonth() + 1,
					day: this.minDate.getDate(),
				};
			}
		} else {
			this.min = undefined;
		}
	}
	ngOnInit() {
		if (typeof this.name === "undefined") this.name = "datetime-" + new Date().toTimeString();
		if (typeof this.disabled === "undefined") this.disabled = false;
		if (typeof this.required === "undefined") this.required = false;
		if (typeof this.placeholder === "undefined") this.placeholder = "dd/mm/yyyy";
		if (typeof this.timepicker === "undefined") this.timepicker = false;
	}

	dateChangeHandler(fd: NgbDateStruct) {
		let dt = Statique.fromDatePicker(fd);
		this.date = Statique.formatDateTime(dt, this.formattedTime);
		if (!this.formattedTime) {
			this.formattedTime = Statique.formatStrTimeToStructure(null);
			this.formattedTime.hour = 0;
			this.formattedTime.minute = 0;
			this.formattedTime.second = 0;
			this.textFormattedTime = "00:00";
		}
		// this.dateChange.emit(Statique.fromDatePicker(fd));
	}

	textTimeChangeHandler(textTime: string) {
		this.formattedTime = Statique.formatStrTimeToStructure(textTime);
		if (this.date) this.date = Statique.formatDateTime(this.date, this.formattedTime);
		// this.dateChange.emit(Statique.fromDatePicker(fd));
	}

	timeChangeHandler(ft: NgbTimeStruct) {
		if (ft) {
			this.formattedTime = ft;
			this.textFormattedTime = Statique.lpad(ft.hour) + ":" + Statique.lpad(ft.minute);
			if (this.date) this.date = Statique.formatDateTime(this.date, this.formattedTime);
			// this.dateChange.emit(Statique.fromDatePicker(fd));
		}
	}

	writeValue(value: any) {
		if (value == undefined) {
			this.dateValue = null;
			this.formattedDate = null;
			this.formattedTime = null;
			this.textFormattedTime = null;
		} else this.date = value;
	}

	propagateChange = (_: any) => {};
	registerOnChange(fn) {
		this.propagateChange = fn;
	}
	registerOnTouched() {}
	public validate(c: FormControl) {
		return this.date || !this.required
			? null
			: {
					jsonParseError: {
						valid: false,
					},
			  };
	}

	public openDatepicker(id) {
		this.dynamicId = id;
	}
	onClick(event) {
		if (this.dynamicId) {
			let parent = event.target.closest("ngb-datepicker");

			if (!this.elementRef.nativeElement.contains(event.target) && !parent) {
				let self = this;
				setTimeout(function() {
					self.dynamicId.close();
				}, 10);
			}
		}
	}

	onClickOut(event) {
		let it = document.querySelector("ngb-timepicker");
		if (it && !it.contains(event.target) && !this.firstTimeTp) {
			this.timepickerToggleOn = false;
		} else {
			this.firstTimeTp = false;
		}
	}

	onTimepickerFocus($event) {
		this.timepickerToggleOn = true;
		this.firstTimeTp = true;
	}

	// @HostListener("document:click")
	// clickout() {
	// 	this.timepickerToggleOn = false;
	// }
	// public selectToday() {
	//     let executed = false;
	//     return function () {
	//         if (!executed) {
	//             executed = true;
	//             this.formattedDate = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate()};
	//         }
	//     };
	// }
}
