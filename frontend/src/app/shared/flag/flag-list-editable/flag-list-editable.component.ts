import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild,
} from "@angular/core";
import { Statique } from "@app/utils/statique";
import { EbFlag } from "@app/classes/ebFlag";
import { FlagMultiSelectComponent } from "../flag-multi-select/flag-multi-select.component";

@Component({
	selector: "app-flag-list-editable",
	templateUrl: "./flag-list-editable.component.html",
	styleUrls: ["./flag-list-editable.component.scss"],
})
export class FlagListEditableComponent implements OnInit, OnChanges {
	@Input()
	iconSize: number = 20;

	@Input()
	listFlag: Array<EbFlag> = [];
	@Input()
	contributionAccess: boolean;
	@Input()
	module: number;

	@Output()
	listFlagChange: EventEmitter<Array<EbFlag>> = new EventEmitter<Array<EbFlag>>();

	@Output()
	change: EventEmitter<any> = new EventEmitter<any>();

	@ViewChild(FlagMultiSelectComponent, { static: false })
	multiSelectComp: FlagMultiSelectComponent;

	Statique = Statique;

	isInEditMode = false;

	ngOnInit() {}

	ngOnChanges(changes: SimpleChanges) {}

	setEditMode() {
		if (this.listFlag == null) {
			this.listFlag = [];
		}
		this.isInEditMode = true;
	}

	cancelEdit() {
		this.isInEditMode = false;
	}

	validEdit() {
		this.listFlagChange.emit(this.listFlag);
		this.change.emit(this.listFlag);
		this.isInEditMode = false;
	}
}
