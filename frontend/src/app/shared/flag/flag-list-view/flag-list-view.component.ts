import { Component, Input, OnInit, SimpleChanges } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { EbFlag } from "@app/classes/ebFlag";
import { FlagService } from "@app/services/flag.service";

@Component({
	selector: "app-flag-list-view",
	templateUrl: "./flag-list-view.component.html",
	styleUrls: ["./flag-list-view.component.scss"],
})
export class FlagListViewComponent implements OnInit {
	Statique = Statique;

	@Input()
	listFlag: Array<EbFlag> = [];

	@Input()
	iconSize: number = 20;

	listFlagOrdered: Array<EbFlag> = [];

	isLoading = false;

	constructor(protected flagService: FlagService) {}

	ngOnInit() {}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["listFlag"] && changes["listFlag"].currentValue != null) {
			this.listFlagOrdered = this.orderListFlag(changes["listFlag"].currentValue);
		}
	}

	private orderListFlag(listToOrder: Array<EbFlag>): Array<EbFlag> {
		const rList = EbFlag.copyList(listToOrder);

		return rList.sort((i1, i2) => {
			if (i1.name == null) {
				return -1;
			}
			if (i2.name == null) {
				return 1;
			}

			if (i1.name > i2.name) {
				return 1;
			}
			if (i1.name < i2.name) {
				return -1;
			}

			return 0;
		});
	}
}
