import { Component, Input } from "@angular/core";
import { Statique } from "@app/utils/statique";

@Component({
	selector: "app-flag",
	templateUrl: "./flag.component.html",
	styleUrls: ["./flag.component.scss"],
})
export class FlagComponent {
	@Input()
	iconCode: number;

	@Input()
	color: string = "#000000";

	@Input()
	iconSize: number = 20;

	@Input()
	label: string = "";

	getFaIcon(): string {
		return Statique.getFaIconByFlagIconCode(this.iconCode);
	}
}
