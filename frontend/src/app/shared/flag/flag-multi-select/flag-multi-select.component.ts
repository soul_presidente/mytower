import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
} from "@angular/core";
import { Statique } from "@app/utils/statique";
import { StatiqueService } from "@app/services/statique.service";
import { FlagService } from "@app/services/flag.service";
import { EbFlag } from "@app/classes/ebFlag";

@Component({
	selector: "app-flag-multi-select",
	templateUrl: "./flag-multi-select.component.html",
	styleUrls: ["./flag-multi-select.component.scss"],
})
export class FlagMultiSelectComponent implements OnInit, OnChanges {
	Statique = Statique;
	listAllFlags: Array<EbFlag>;

	@Output()
	change: EventEmitter<Array<EbFlag>> = new EventEmitter<Array<EbFlag>>();

	@Input()
	listFlag: Array<EbFlag>;
	@Input()
	name: string;
	@Input()
	contributionAccess: boolean
	@Input()
	module: number;
	
	@Output()
	listFlagChange: EventEmitter<Array<EbFlag>> = new EventEmitter<Array<EbFlag>>();

	listFlagNumArray: Array<number>;

	constructor(protected statiqueService?: StatiqueService, protected flagService?: FlagService) {}

	ngOnInit() {
		this.flagService.getListFlag(this.module).subscribe(
			function(data) {
				this.listAllFlags = data;
			}.bind(this)
		);
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["listFlag"] && changes["listFlag"].currentValue != null) {
			// TODO change this check
			// I don't understand why but this on changes is bcalled in loop, so i put this little check for POC AIRBUS but need to investigate more
			if (
				changes["listFlag"].previousValue == null ||
				changes["listFlag"].currentValue.length !== changes["listFlag"].previousValue.length
			) {
				this.listFlagNumArray = [];

				this.listFlag.forEach((f) => {
					this.listFlagNumArray.push(f.ebFlagNum);
				});
			}
		}
	}

	selectFlag() {
		this.listFlag = [];
		this.listFlagNumArray.forEach((num) => {
			this.listAllFlags.forEach((f) => {
				if (num === f.ebFlagNum) {
					this.listFlag.push(f);
				}
			});
		});

		this.listFlagChange.emit(this.listFlag);
		this.change.emit(this.listFlag);
	}
}
