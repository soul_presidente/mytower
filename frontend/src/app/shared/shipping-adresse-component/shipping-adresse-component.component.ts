import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { ModeTransport, UserRole } from "@app/utils/enumeration";
import {
	CompleterData,
	CompleterItem,
	CompleterService,
} from "../../../../node_modules/ng2-completer";
import { EbParty } from "@app/classes/party";
import { EbUser } from "@app/classes/user";
import { EbCostCenter } from "@app/classes/costCenter";
import { EcCountry } from "@app/classes/country";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { EbEntrepotDTO } from "@app/classes/dock/EbEntrepotDTO";
import { EbAdresse } from "@app/classes/adresse";
import { keyValue, Statique } from "@app/utils/statique";
import { EbTypeTransport } from "@app/classes/EbTypeTransport";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbCompagnie } from "@app/classes/compagnie";
import { EbEtablissement } from "@app/classes/etablissement";
import { EcCurrency } from "@app/classes/currency";
import { EbCategorie } from "@app/classes/categorie";
import { IField } from "@app/classes/customField";
import { PricingService } from "@app/services/pricing.service";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { DialogService } from "@app/services/dialog.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { CompagnieService } from "@app/services/compagnie.service";
import { TrackTraceService } from "@app/services/trackTrace.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { DockManagementService } from "@app/services/dock-management.service";
import { UserService } from "@app/services/user.service";
import { ModalService } from "../modal/modal.service";
import { ActivatedRoute } from "@angular/router";
import { CollectionService } from "@app/services/collection.service";
import { TranslateService } from "@ngx-translate/core";
import { StatiqueService } from "@app/services/statique.service";
import { PricingMaskFillerComponent } from "@app/component/saved-forms/pricing-mask/pricing-mask-filler.component";

@Component({
	selector: "app-shipping-adresse-component",
	templateUrl: "./shipping-adresse-component.component.html",
	styleUrls: ["./shipping-adresse-component.component.css"],
})
export class ShippingAdresseComponentComponent extends PricingMaskFillerComponent
	implements OnInit {
	searchOrigin: string;

	@Input()
	hasContributionAccess: number;
	@Input("module")
	ebModuleNum: number;
	@Input()
	ebParty: EbParty;
	@Input()
	userConnected: EbUser;
	@Input()
	title: string;
	@Output()
	onValidate = new EventEmitter<any>();

	UserRole = UserRole;
	listCostCenter: Array<EbCostCenter>;
	listCountry: Array<EcCountry> = new Array<EcCountry>();
	listZone: Array<EbPlZoneDTO> = new Array<EbPlZoneDTO>();
	Statique = Statique;
	incoterms: Array<keyValue> = new Array<keyValue>();
	typeDemande: any = Statique.typeDemande;
	suggestionsEtablissement: CompleterData;

	searchCriteria: SearchCriteria = new SearchCriteria();
	selectedCompagnie: EbCompagnie;
	selectedEtabNum: number;
	listEtablissement: any = new Array<EbEtablissement>();
	selectedUserNum: number;
	selectedUser: EbUser;
	listEtablissementPlateform: Array<EbEtablissement> = new Array<EbEtablissement>();

	toggleEditData: boolean = false;
	listCategorie: Array<EbCategorie>;
	listFields: Array<IField>;

	searchAdress: string;

	constructor(
		private pricingService: PricingService,
		private completerService: CompleterService,
		protected savedFormsService: SavedFormsService,
		protected dialogService: DialogService,
		private etablissementService: EtablissementService,
		private compagnieService: CompagnieService,
		private trackTraceService: TrackTraceService,
		private transportationPlanService: TransportationPlanService,
		protected dockManagementService: DockManagementService,
		protected modalService: ModalService,
		protected cd: ChangeDetectorRef,
		protected statiqueService: StatiqueService,
		protected activatedRoute: ActivatedRoute,
		protected translate: TranslateService
	) {
		super(savedFormsService, dialogService, modalService, translate);
	}

	ngOnInit() {
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteria.setUser(this.userConnected);
		this.etablissementService.getListCurrencyForDemande(this.searchCriteria).subscribe(
			function(data) {
				if (data) {
					this.listCurrencyDemande = data;
				}
			}.bind(this)
		);

		this.setSuggestionsEtablissement();
		this.getCountries();
		this.getEstablishments();
		this.getZones();
	}

	setSuggestionsEtablissement() {
		let autocompleteUrl = `/adresseAsParty`;
		if (!this.isControlTower) {
			autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
			autocompleteUrl += `ebCompagnieNum=${this.userConnected.ebCompagnie.ebCompagnieNum}`;

			if (!this.userConnected.superAdmin) {
				autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
				autocompleteUrl += `ebEtablissementNum=${
					this.userConnected.ebEtablissement.ebEtablissementNum
				}`;
			}
		}

		if (this.selectedCompagnie && this.selectedCompagnie.ebCompagnieNum) {
			autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
			autocompleteUrl += `ebCompagnieNum=${this.selectedCompagnie.ebCompagnieNum}`;

			if (
				this.selectedEtabNum &&
				this.selectedUser &&
				this.selectedUser.ebUserNum == this.selectedUserNum &&
				!this.selectedUser.superAdmin
			) {
				autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
				autocompleteUrl += `ebEtablissementNum=${this.selectedEtabNum}`;
			}
		}

		autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
		autocompleteUrl += `term=`;
		this.suggestionsEtablissement = this.completerService.remote(
			Statique.controllerPricing + autocompleteUrl,
			null,
			"etablissementReference"
		);
	}

	selectedEbParty(selected: CompleterItem) {
		if (selected === null) {
			this.ebParty.ebPartyNum = null;
		} else {
			this.ebParty = selected.originalObject;
		}
		this.onValidate.emit(this.ebParty);
	}

	onValideFields() {
		if (this.ebParty.city != null && this.ebParty.xEcCountry != null)
			this.onValidate.emit(this.ebParty);
	}

	getCountries() {
		this.pricingService.listPays().subscribe((data) => {
			this.listCountry = data;
		});
	}

	getZones() {
		let criteria = new SearchCriteria();
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		this.transportationPlanService.listZone(criteria).subscribe((data: Array<EbPlZoneDTO>) => {
			this.listZone = data;
		});
	}

	getEstablishments() {
		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.getAll = true;
		searchCriteria.size = null;
		this.etablissementService
			.getListEtablisement(searchCriteria)
			.subscribe((data: Array<EbEtablissement>) => {
				this.listEtablissementPlateform = data;
			});
	}

	private getZoneBackupDisplayString(party?: EbParty): string {
		if (party) {
			if (party.zoneDesignation) {
				return party.zoneRef + " (" + party.zoneDesignation + ")";
			} else {
				return party.zoneRef;
			}
		} else {
			return "";
		}
	}

	private getZoneDisplayString(zone?: EbPlZoneDTO): string {
		if (zone) {
			if (zone.designation) {
				return zone.ref + " (" + zone.designation + ")";
			} else {
				return zone.ref;
			}
		} else {
			return "";
		}
	}

	compareEtablissement(etablissement1: EbEtablissement, etablissement2: EbEtablissement) {
		if (etablissement1 && etablissement2) {
			return etablissement1.ebEtablissementNum === etablissement2.ebEtablissementNum;
		}
	}

	compareCountry(country1: EcCountry, country2: EcCountry) {
		if (country2 !== undefined && country2 !== null) {
			return country1.ecCountryNum === country2.ecCountryNum;
		}
	}

	compareZone(zone1: EbPlZoneDTO, zone2: EbPlZoneDTO) {
		if (zone1 && zone2) {
			return zone1.ebZoneNum === zone2.ebZoneNum;
		}
	}
}
