import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { EbUser } from "@app/classes/user";
import { UserService } from "@app/services/user.service";
import { UserRole } from "@app/utils/enumeration";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbRelation } from "@app/classes/relation";
import { EbUserTag } from "@app/classes/EbUserTag";
import { UserTagService } from "@app/services/userTag.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { MessageService } from "primeng/api";
import { HeaderService } from "@app/services/header.service";

@Component({
	selector: "app-tag-list-editable",
	templateUrl: "./tag-list-editable.component.html",
	styleUrls: ["./tag-list-editable.component.scss"],
})
export class TagListEditableComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	userTag: EbUserTag;
	@Input()
	listUserTag: Array<EbUserTag>;

	@Input()
	user: EbUser;
	@Input()
	role: any;
	@Input()
	isForConnectedUser: boolean;
	@Input()
	listOfAllTagsFiltered: Array<string> = [];

	@Output()
	tagSearch = new EventEmitter<string>();

	@Output()
	tagEmit = new EventEmitter<string>();

	testRole: boolean = false;

	listTagArray: Array<string> = [];
	listOfAllTags: Array<string> = [];
	commentEdit: boolean = false;
	tagsEdit: boolean = false;
	commentValue: string;

	Statique = Statique;

	isInEditMode = false;

	constructor(
		protected userService: UserService,
		protected userTagService: UserTagService,
		protected modalService: ModalService,
		protected translate: TranslateService,
		protected messageService: MessageService,
		protected headerService: HeaderService
	) {
		super();
	}
	ngOnInit() {
		this.getUserTags();
		this.getListOfAllTags();
		this.getChargerRole();
		this.commentValue = this.userTag.comment;
	}

	setEditMode() {
		this.isInEditMode = true;
	}

	cancelEdit() {
		this.isInEditMode = false;
	}

	cancelEditComment() {
		let $this = this;
		let activateMessage = null;
		let activateHeader = null;

		if (Statique.isDefined(this.userTag.comment) && this.userTag.comment.trim().length != 0) {
			this.translate.get("SETTINGS.DELETE_COMMENT_MESSAGE").subscribe((res: string) => {
				activateMessage = res;
			});

			this.translate.get("REQUEST_OVERVIEW.WARNING").subscribe((res: string) => {
				activateHeader = res;
			});

			this.modalService.confirm(
				activateHeader,
				activateMessage,

				function() {
					$this.userTag.comment = null;
					$this.userTagService
						.saveUserTag($this.userTag, $this.user.ebCompagnie.ebCompagnieNum)
						.subscribe();
				},
				function() {
					$this.userTag.comment = $this.commentValue;
				},
				true
			);
		}
	}
	validCommentEdit() {
		if (Statique.isDefined(this.userTag.comment) && this.userTag.comment.trim().length != 0) {
			this.userTagService.saveUserTag(this.userTag, this.user.ebCompagnie.ebCompagnieNum).subscribe(
				(data) => {
					this.commentValue = this.userTag.comment;
					this.messageService.add({
						severity: "success",
						summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
						detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
					});
				},
				(error) => {
					this.messageService.add({
						severity: "error",
						summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
						detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
					});
				}
			);
		}
	}

	deleteTag(tag) {
		let index = this.listTagArray.indexOf(tag);
		this.listTagArray.splice(index, 1);
		this.validEdit();
	}
	validEdit() {
		if (this.listTagArray.length > 0) {
			let tags: string = "";
			this.listTagArray.forEach((element) => {
				if (element.length > 0) tags = tags + element + ",";
			});
			tags = tags.substr(0, tags.length - 1);

			this.userTag.tags = tags;
		} else {
			this.userTag.tags = null;
		}

		this.userTag.ebUserTagNum = null;
		this.userTagService.saveUserTag(this.userTag, this.user.ebCompagnie.ebCompagnieNum).subscribe();

		this.isInEditMode = false;
	}

	addTag(event) {
		if (event.type == "keydown") {
			let value = event.target.value;
			if (value != null && value.trim != "") {
				if (this.userTag.tags == null) {
					this.userTag.tags = value;
				} else {
					this.userTag.tags = this.userTag.tags + "," + value;
				}
				this.userTag.ebUserTagNum = null;
				this.userTagService
					.saveUserTag(this.userTag, this.user.ebCompagnie.ebCompagnieNum)
					.subscribe();
				this.listTagArray.push(value);
				this.validEdit();
				this.tagEmit.emit(value);
				this.isInEditMode = false;
			}
		}
	}
	getUserTags() {
		if (this.userTag.tags != null) {
			let tags = this.userTag.tags.trim();

			this.listTagArray = tags.split(",");
		}
	}
	getListOfAllTags() {
		this.listUserTag.forEach((element) => {
			if (element.tags != null) {
				let listTags: Array<string> = [];
				listTags = element.tags.split(",");

				listTags.forEach((element) => {
					this.listOfAllTags.push(element);
				});
			}
		});
	}

	getChargerRole() {
		if (
			this.userConnected.role == UserRole.CONTROL_TOWER ||
			this.userConnected.role == UserRole.CHARGEUR
		) {
			if (Statique.isDefined(this.userTag.comment)) {
				this.commentEdit = false;
			} else {
				this.commentEdit = true;
			}
			this.tagsEdit = true;
		}
	}

	searchByTag(tag: string) {
		this.tagSearch.emit(tag);
	}
}
