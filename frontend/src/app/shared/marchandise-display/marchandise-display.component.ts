import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ConnectedUserComponent } from "../connectedUserComponent";
import { TranslateService } from "@ngx-translate/core";
import { ExportService } from "@app/services/export.service";
import { DataNestedItems } from "@app/classes/dataNestedItems";

@Component({
	selector: "app-marchandise-display",
	templateUrl: "./marchandise-display.component.html",
	styleUrls: ["./marchandise-display.component.css"],
})
export class MarchandiseDisplayComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	title: string;

	@Input()
	listDataNestedItems: Array<DataNestedItems>;

	constructor(
		protected translate: TranslateService,
		protected exportService: ExportService,
		protected router: Router
	) {
		super();
	}

	ngOnInit() {}
}
