import { Directive, Self, Host, HostListener, Input } from "@angular/core";
import { Calendar } from "primeng/primeng";
import { Statique } from "@app/utils/statique";

/**
 * Force the hour to remove the timezone offset
 * Useful when issues appears with p-calendar with timezone
 * Replace deprecated UTC parameter (https://github.com/primefaces/primeng/issues/5853)
 */
@Directive({
	selector: "[forceUTC]",
})
export class PCalForceUTCDirective {
	@Input("forceUTC") enabled: boolean = true;

	constructor(@Host() @Self() private calendar: Calendar) {}

	@HostListener("onSelect", ["$event"]) onSelect() {
		if (this.enabled) this.toUtc();
	}

	@HostListener("onInput", ["$event"]) onInput() {
		if (this.enabled) this.toUtc();
	}

	private toUtc() {
		if (this.calendar.value != null && Statique.isDate(this.calendar.value)) {
			let date = Statique.forceUTC(this.calendar.value);
			this.calendar.value = date;

			this.calendar.updateModel(this.calendar.value);
		}
	}
}
