import { Component, EventEmitter, forwardRef, Input, OnInit, Output } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";

@Component({
	selector: "app-advanced-editor",
	templateUrl: "./advanced-editor.component.html",
	styleUrls: [],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => AdvancedEditorComponent),
			multi: true,
		},
	],
})
export class AdvancedEditorComponent implements OnInit, ControlValueAccessor {
	@Input()
	name: string;
	@Input()
	required: boolean;
	@Input()
	disabled: boolean = false;
	@Input()
	inputModel: any;
	@Output()
	ngModelChange = new EventEmitter<string>();

	editorConfig: any;

	constructor() {}

	ngOnInit() {
		this.editorConfig = {
			editable: true,
			spellcheck: true,
			height: "auto",
			minHeight: "200px",
			width: "auto",
			minWidth: "0",
			translate: "yes",
			enableToolbar: true,
			showToolbar: true,
			placeholder: "",
			imageEndPoint: "",
			toolbar: [
				["bold", "italic", "underline", "strikeThrough", "superscript", "subscript"],
				["fontName", "fontSize", "color"],
				["justifyLeft", "justifyCenter", "justifyRight", "justifyFull", "indent", "outdent"],
				["cut", "copy", "delete", "removeFormat", "undo", "redo"],
				[
					"paragraph",
					"blockquote",
					"removeBlockquote",
					"horizontalLine",
					"orderedList",
					"unorderedList",
				],
				["link", "unlink"],
			],
		};
	}

	updateChanges($event: string) {
		this.inputModel = $event;
		this.ngModelChange.emit($event);
	}

	onChange: (_: any) => void = (_: any) => {};
	onTouched: () => void = () => {};
	// called when new value is input from parent
	writeValue(value: string): void {
		this.updateChanges(value);
	}
	registerOnChange(fn: any): void {
		this.onChange = fn;
	}
	registerOnTouched(fn: any): void {
		this.onTouched = fn;
	}
}
