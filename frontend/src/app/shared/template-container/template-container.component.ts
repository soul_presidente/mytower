import {
	AfterViewInit,
	Compiler,
	Component,
	ComponentRef,
	EventEmitter,
	Input,
	NgModule,
	OnChanges,
	OnDestroy,
	Output,
	ViewChild,
	ViewContainerRef,
} from "@angular/core";
import { Statique } from "@app/utils/statique";
import { SharedModule } from "../module/shared.module";
import { AuthenticationService } from "@app/services/authentication.service";

@Component({
	selector: "app-template-container",
	template: `
		<div class="template-container-root"><div #target></div></div>
	`,
})
export class TemplateContainerComponent implements AfterViewInit, OnChanges, OnDestroy {
	@Input()
	model: any;
	@Input()
	html: string;
	@Input()
	regenerateOnChanges: boolean = false;

	@ViewChild("target", { read: ViewContainerRef, static: true })
	target: any;

	@Output()
	customEvent = new EventEmitter();

	constructor(private compiler: Compiler) {}

	private initialized: boolean = false;
	private componentRef: ComponentRef<any>;

	private updateComponent() {
		if (!this.initialized || !this.html) {
			return;
		}

		if (this.componentRef) {
			if (this.regenerateOnChanges) {
				this.componentRef.destroy();
				if (this.target) {
					this.target.remove();
				}
			} else {
				return;
			}
		}

		this.componentRef = this.createNewComponent(this.html);
		this.componentRef.instance.model = this.model;
		this.componentRef.changeDetectorRef.detectChanges();
	}

	ngOnChanges() {
		this.updateComponent();
	}

	ngAfterViewInit() {
		this.initialized = true;
		this.updateComponent();
	}

	ngOnDestroy() {
		if (this.componentRef) {
			this.componentRef.destroy();
		}
		if (
			this.target &&
			this.target.element &&
			this.target.element.nativeElement &&
			this.target.element.nativeElement.parentElement
		) {
			this.target.element.nativeElement.parentElement.remove();
		}
	}

	protected createNewComponent(tmpl: string) {
		let parentContext = this;

		@Component({
			selector: "app-template-container-dynamic",
			template: tmpl,
		})
		class TemplateContainerDynamicComponent {
			@Input()
			public model: any;

			Statique = Statique;
			AuthenticationService = AuthenticationService;

			constructor(protected authenticationService: AuthenticationService) {}

			outputEvent(key: string, value: any) {
				if (parentContext.customEvent) {
					parentContext.customEvent.emit({
						key: key,
						value: value,
						model: this.model,
					});
				}
			}
		}

		@NgModule({
			declarations: [TemplateContainerDynamicComponent],
			imports: [SharedModule],
		})
		class TemplateContainerDynamicModule {}

		const mod = this.compiler.compileModuleAndAllComponentsSync(TemplateContainerDynamicModule);
		const factory = mod.componentFactories.find(
			(comp) => comp.componentType === TemplateContainerDynamicComponent
		);
		return this.target.createComponent(factory);
	}
}
