import { SemiAutoMailType } from "./semi-auto-mail";
import { Email } from "@app/classes/email";
import { Output, EventEmitter } from "@angular/core";

export abstract class SemiAutoMailPopup {
	popupOpened: boolean = false;

	mail: Email;
	listTypeMail: SemiAutoMailType[];

	selectedMailTypeId: number;

	cc: string;

	@Output()
	onClose: EventEmitter<boolean> = new EventEmitter<boolean>();

	@Output()
	onOpen: EventEmitter<void> = new EventEmitter<void>();

	constructor() {
		this.mail = new Email();
		this.listTypeMail = this.getListTypeMail();
		this.cc = "";
	}

	abstract getListTypeMail(): Array<SemiAutoMailType>;
	abstract generateHtmlFromTemplate(event);
	abstract sendMail(event);

	clickSend(event) {
		this.mail.enCopie = this.cc ? this.cc.split(";") : null;
		this.mail.mailType = this.selectedMailTypeId;
		this.sendMail(event);
	}

	close() {
		this.popupOpened = false;
		this.onClose.emit(false);
	}

	open(mailId) {
		this.mail = new Email();
		if (mailId) {
			this.selectedMailTypeId = this.getListTypeMail()[mailId].mailId;
		} else {
			this.selectedMailTypeId = null;
		}
		this.cc = "";
		this.popupOpened = true;
		this.onOpen.emit();
	}

	get canSend(): boolean {
		return (
			this.mail != null &&
			!!this.mail.text &&
			!!this.mail.to &&
			!!this.mail.mailType &&
			!!this.mail.subject
		);
	}
}
