import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";

@Component({
	selector: "app-comment-panel",
	templateUrl: "./comment-panel.component.html",
	styleUrls: ["./comment-panel.component.scss"],
})
export class CommentPanelComponent implements OnInit {
	@Input()
	panelCollapsis: boolean;
	@Input()
	panelOpen: boolean;
	@Input()
	title: String;
	@Input()
	commentModel: String;
	@Input()
	disabled: boolean;
	@Output()
	sharedVarChange = new EventEmitter();

	constructor(public generaleMethode: generaleMethodes) {
		if (this.disabled == null) this.disabled = false;
	}

	ngOnInit() {}

	change(newValue) {
		this.commentModel = newValue;
		this.sharedVarChange.emit(newValue);
	}

	panelCollapse(panel, event) {
		this.generaleMethode.panelCollapse(event, panel);
	}
}
