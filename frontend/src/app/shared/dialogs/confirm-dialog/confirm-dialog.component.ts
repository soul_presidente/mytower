import { Component, Input } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
	selector: "confirm-dialog",
	templateUrl: "./confirm-dialog.component.html",
	styleUrls: ["./confirm-dialog.component.css"],
})
export class ConfirmDialog {
	@Input()
	title;
	@Input()
	message;

	constructor(public activeModal: NgbActiveModal) {}
}
