import { Component } from "@angular/core";
import { ConfirmDialog } from "../confirm-dialog/confirm-dialog.component";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
	selector: "periode-dialog",
	templateUrl: "./periode-dialog.component.html",
	styleUrls: ["../periode-dialog/periode-dialog.component.css", "./periode-dialog.component.css"],
})
export class PeriodeDialog extends ConfirmDialog {
	public title: string;
	public message: string;
	public dateDeb: string;
	public dateFin: string;

	constructor(public activeModal: NgbActiveModal) {
		super(activeModal);
	}

	saveInput() {
		this.activeModal.close({
			dateDeb: this.dateDeb,
			dateFin: this.dateFin,
		});
	}
}
