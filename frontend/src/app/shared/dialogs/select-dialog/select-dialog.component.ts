import { Component } from "@angular/core";
import { ConfirmDialog } from "../confirm-dialog/confirm-dialog.component";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
	selector: "select-dialog",
	templateUrl: "./select-dialog.component.html",
	styleUrls: ["../select-dialog/select-dialog.component.css", "./select-dialog.component.css"],
})
export class SelectDialog extends ConfirmDialog {
	public title: string;
	public message: string;
	public value: object;
	public listOptions: Array<object>;
	public getOptionFunction: Function;

	constructor(public activeModal: NgbActiveModal) {
		super(activeModal);
	}

	saveInput() {
		this.activeModal.close(this.value);
	}

	getOptionLibelle(option) {
		if (this.getOptionFunction) return this.getOptionFunction(option);
		return option.libelle;
	}
}
