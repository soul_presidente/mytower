import { DialogService } from "@app/services/dialog.service";
import { NgModule } from "@angular/core";

import { ConfirmDialog } from "./confirm-dialog/confirm-dialog.component";
import { InputDialog } from "./input-dialog/input-dialog.component";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { PeriodeDialog } from "./periode-dialog/periode-dialog.component";
import { SharedModule } from "../module/shared.module";
import { SelectDialog } from "./select-dialog/select-dialog.component";

@NgModule({
	imports: [CommonModule, FormsModule, SharedModule],
	exports: [ConfirmDialog, InputDialog, PeriodeDialog, SelectDialog],
	declarations: [ConfirmDialog, InputDialog, PeriodeDialog, SelectDialog],
	providers: [DialogService],
	entryComponents: [ConfirmDialog, InputDialog, PeriodeDialog, SelectDialog],
})
export class DialogsModule {}
