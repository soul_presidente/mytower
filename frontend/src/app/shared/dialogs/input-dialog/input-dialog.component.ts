import { Component } from "@angular/core";
import { ConfirmDialog } from "../confirm-dialog/confirm-dialog.component";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
	selector: "input-dialog",
	templateUrl: "./input-dialog.component.html",
	styleUrls: ["../confirm-dialog/confirm-dialog.component.css", "./input-dialog.component.css"],
})
export class InputDialog extends ConfirmDialog {
	public title: string;
	public message: string;
	public value: string;

	constructor(public activeModal: NgbActiveModal) {
		super(activeModal);
	}

	saveInput() {
		if (this.value && this.value.length > 0) this.activeModal.close(this.value);
	}
}
