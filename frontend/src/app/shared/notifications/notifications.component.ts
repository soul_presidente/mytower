import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { EbUser } from "@app/classes/user";
import { Statique } from "@app/utils/statique";

@Component({
	selector: "app-notifications",
	templateUrl: "./notifications.component.html",
	styleUrls: ["./notifications.component.scss"],
})
export class NotificationsComponent implements OnInit {
	statique = Statique;

	@Input()
	public dataSource: string;
	@Input()
	public dataInsertion: string;
	@Input()
	public module: number;
	@Input()
	public idFiche: number;
	@Input()
	public user: EbUser;
	@Input()
	public nbColTexteditor: string = "col-md-4";
	@Input()
	public nbColListing: string = "col-md-8";
	@Input()
	showModelNote: boolean;

	showMore: boolean = false;
	@Output()
	toggle = new EventEmitter<any>();

	listNotes = new Array<any>();

	constructor() {}

	ngOnInit() {
		this.listNotes = [
			{
				module: "order",
				demande: "EXN-22521",
				name: "John Doe",
				date: "11-11-2018 13:21",
				by: "My Tower",
				for: "John Doe",
			},
			{
				module: "claims",
				demande: "EXN-007",
				name: "Joy Walton",
				date: "05-11-2018 22:40",
				by: "My Tower",
				for: "Joy Walton",
			},
			{
				module: "track",
				demande: "EXN-10078",
				name: "Alejandra Deleon",
				date: "26-10-2018 18:32",
				by: "My Tower",
				for: "Alejandra Deleon",
			},
			{
				module: "order",
				demande: "EXN-14111",
				name: "Lydia Wells",
				date: "12-10-2018 20:05",
				by: "My Tower",
				for: "Lydia Wells",
			},
			{
				module: "order-management",
				demande: "EXN-21151",
				name: "Cole Aguilar",
				date: "05-10-2018 09:25",
				by: "My Tower",
				for: "Cole Aguilar",
			},
			{
				module: "trans-management",
				demande: "EXN-96754",
				name: "Derek Norman",
				date: "01-10-2018 08:01",
				by: "My Tower",
				for: "Derek Norman",
			},
			{
				module: "order",
				demande: "EXN-85507",
				name: "Brittany Graves",
				date: "27-09-2018 14:00",
				by: "My Tower",
				for: "Brittany Graves",
			},
			{
				module: "track",
				demande: "EXN-25079",
				name: "Marisol Gilbert",
				date: "26-09-2018 04:50",
				by: "My Tower",
				for: "Marisol Gilbert",
			},
			{
				module: "claims",
				demande: "EXN-330594",
				name: "Jack Dunn",
				date: "09-09-2018 12:50",
				by: "My Tower",
				for: "Jack Dunn",
			},
		];
	}

	closeModal() {
		let event = new CustomEvent("toggle-model-note", null);
		document.dispatchEvent(event);
	}
	hideThisElt(event) {
		let idElt = event.target.id;
		let eltToHide = document.getElementById("note-" + idElt);
		eltToHide.parentElement.remove();
	}
	showMoreNotes() {
		this.showMore = !this.showMore;
	}
}
