import { DocumentControlRulesFormComponent } from "./../../component/user/settings/control-rules/document-control-rules-form/document-control-rules-form.component";
import { WeekTemplateComponent } from "@app/shared/week-template/week-template.component";
import { SelectButtonModule } from "primeng/selectbutton";
import { TreeModule } from "primeng/tree";
import { AutoCompleteModule } from "primeng/autocomplete";
import { DialogModule } from "primeng/dialog";
import { ContextMenuModule } from "primeng/contextmenu";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { TextMaskModule } from "angular2-text-mask";
import { TranslateModule } from "@ngx-translate/core";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { NgBoostrapModule } from "../ng-bootstrap/ng-bootstrap";
import { NgSelectModule } from "@ng-select/ng-select";
import { ArchwizardModule } from "angular-archwizard";
import { EditorModule } from "primeng/editor";
import { ModalComponent } from "../modal/modal.component";
import { EqualValidator } from "@app/utils/equal-validator.directive";
import { CarrousselAccountComponent } from "@app/component/account/carroussel-account/carroussel-account.component";
import { CommentPanelComponent } from "../comment-panel/comment-panel.component";
import { ChatPanelComponent } from "../chat-panel/chat-panel.component";
import { AdvancedSearchComponent } from "../advanced-search/advanced-search.component";
import { CollapsiblePanelComponent } from "../collapsible-panel/collapsible-panel.component";
import { ChatService } from "@app/services/chat.service";
import { AdvancedFormSearchComponent } from "../advanced-form-search/advanced-form-search.component";
import { OldAutocompleteComponent } from "../old-autocomplete/old-autocomplete.component";
import { FavoriComponent } from "../favori/favori.component";
import { DatetimePickerComponent } from "../datetime-picker/datetime-picker.component";
import { NgxUploaderModule } from "ngx-uploader";
import { DocumentsComponent } from "../documents/documents.component";
import { PopUpDetailsDemandeComponent } from "../pop-up-details-demande/pop-up-details-demande.component";
import { FormAdresseComponent } from "../adresses/form-adresse/form-adresse.component";
import { CotationComponent } from "@app/component/pricing-booking/shared-component/cotation/cotation.component";
import { CalendarModule } from "primeng/calendar";
import { SelectOptionComponent } from "../select-option-custom/selectoption.component";
import { TOKEN_NAME } from "@app/services/auth.constant";
import { JwtTokenInterceptor } from "@app/utils/jwt-token-interceptor";
import { TableModule } from "primeng/table";
import { MultiSelectModule } from "primeng/multiselect";
import { ButtonModule } from "primeng/button";
import { PanelModule } from "primeng/panel";
import { OverlayPanelModule } from "primeng/overlaypanel";
import { ListboxModule } from "primeng/listbox";
import { GenericTableComponent } from "../generic-table/generic-table.component";
import { GenericTableCcpComponent } from "../generic-table-ccp/generic-table-ccp.component";
import { BoolToYesNoPipe } from "../pipes/boolToYesNo.pipe";
import { SafePipe } from "../pipes/safe.pipe";
import { EllipsisPipe } from "../pipes/ellipsis.pipe";
import { VarDirective } from "../directives/ngVar.directive";
import { TabViewModule } from "primeng/tabview";
import { PaginatorModule } from "primeng/paginator";
import { ScheduleModule } from "primeng/schedule";
import { TemplateContainerComponent } from "../template-container/template-container.component";
import {
	CheckboxModule,
	ColorPickerModule,
	ProgressBarModule,
	ProgressSpinnerModule,
	RadioButtonModule,
	SpinnerModule,
	ChipsModule,
} from "primeng/primeng";
import { KeyFilterModule } from "primeng/keyfilter";
import { ToggleButtonModule } from "primeng/togglebutton";
import { AccordionModule } from "primeng/accordion";
import { MarchandiseDisplayComponent } from "../marchandise-display/marchandise-display.component";
import { Ng2CompleterModule } from "ng2-completer";
import { ShippingAdresseComponentComponent } from "../shipping-adresse-component/shipping-adresse-component.component";
import { GenericTableAutocompleteComponent } from "../generic-table/autocomplete/autocomplete.component";
import { FlagComponent } from "../flag/flag.component";
import { FlagMultiSelectComponent } from "../flag/flag-multi-select/flag-multi-select.component";
import { UsersAutocompleteComponent } from "../users-autocomplete/users-autocomplete.component";
import { FlagListViewComponent } from "../flag/flag-list-view/flag-list-view.component";
import { NotificationsComponent } from "../notifications/notifications.component";
import { VehiculeContentComponent } from "../vehicule-content/vehicule-content.component";
import { ColorPickerGenericCell } from "../generic-cell/commons/color-picker.generic-cell";
import { FlagSingleViewGenericCell } from "../generic-cell/commons/flag-single-view.generic-cell";
import { FlagListEditableComponent } from "../flag/flag-list-editable/flag-list-editable.component";
import { GoodsInformationComponent } from "@app/component/pricing-booking/shared-component/goods-information/goods-information.component";
import { ShippingInformationComponent } from "@app/component/pricing-booking/shared-component/shipping-information/shipping-information.component";
import { StepCotationComponent } from "@app/component/pricing-booking/shared-component/step-cotation/step-cotation.component";
import { CarrierChoiceComponent } from "@app/component/pricing-booking/shared-component/carrier-choice/carrier-choice.component";
import { DataTablesModule } from "angular-datatables";
import { ExternalUsersComponent } from "@app/component/pricing-booking/shared-component/external-users/external-users.component";
import { CardListComponent } from "../card-list/card-list.component";
import { AdresseForShippingComponent } from "@app/component/pricing-booking/shared-component/adresse-for-shipping/adresse-for-shipping.component";
import { QmTransportInformationComponent } from "@app/component/quality-management/create/qm-transport-information/qm-transport-information.component";
import { AdvancedEditorComponent } from "../advanced-editor/advanced-editor.component";
import { AutocompleteComponent } from "../autocomplete/autocomplete.component";
import { ForEachItemDirective } from "../../directives/for-each-item.directive";
import { DeliverySearchComponent } from "@app/component/delivery-overview/delivery-search/delivery-search.component";
import { GenericCellHoster } from "../generic-cell/generic-cell";
import { DocTemplateGenDetailsComponent } from "@app/component/documents/template-gen-params/template-gen-params";
import { DragDropModule } from "primeng/dragdrop";
import { DemandeOverviewComponent } from "../demande-overview/demande-overview..component";
import { ModalExportDocByTemplateComponent } from "../modal-export-doc-by-template/modal-export-doc-by-template.component";
import { BreadcrumbComponent } from "@app/shared/breadcrumb/breadcrumb.component";
import { VignetteViewComponent } from "../vignette-view/vignette-view.component";
import { VignetteHeaderComponent } from "../vignette-view/vignette-header/vignette-header.component";
import { VignettePricingComponent } from "../vignette-view/vignette-pricing/vignette-pricing.component";
import { VignetteTracingComponent } from "../vignette-view/vignette-tracing/vignette-tracing.component";
import { FormErrorMessagesComponent } from "@app/shared/form-error-messages/form-error-messages.component";
import { VignetteFavorisComponent } from "../vignette-view/vignette-favoris/vignette-favoris.component";
import { TextInputGenericCell } from "../generic-cell/commons/text-input.generic-cell";
import { DatePickerGenericCell } from "../generic-cell/commons/date-picker.generic-cell";
import { SelectGenericCell } from "../generic-cell/commons/select.generic-cell";
import { CommentIconPopupGenericCell } from "../generic-cell/commons/comment-icon-popup.generic-cell";
import { FlagColumnGenericCell } from "../generic-cell/commons/flag-column.generic-cell";
import { PCalForceUTCDirective } from "../primeng/calendar";
import { CompanyPickerComponent } from "../company-picker/company-picker.component";
import { VignetteCptmComponent } from "../vignette-view/vignette-cptm/vignette-cptm.component";
import { ExternalUserPartnerComponent } from "@app/component/pricing-booking/shared-component/external-user-partner/external-user-partner.component";
import { ButtonGenericCell } from "../generic-cell/commons/button.generic-cell";
import { NgbTabsetModule } from "@ng-bootstrap/ng-bootstrap";
import { TagListEditableComponent } from "@app/shared/tag/tag-list-editable/tag-list-editable.component";
import { RequiredIfDirective } from "../directives/required-if.directive";
import { ForbiddenValuesDirective } from "../directives/forbidden-values.directive";
import { ZoneDialogComponent } from "@app/component/pricing-booking/shared-component/adresse-for-shipping/zone-dialog/zone-dialog.component";
import { ModalConfirmationComponent } from "../modal-confirmation/modal-confirmation.component";
import { OrderCreationFormComponent } from "@app/component/delivery-overview/order-creation-form/order-creation-form.component";
import { OrderStepReferenceComponent } from "@app/component/delivery-overview/order-creation-form/steps/order-step-reference/order-step-reference.component";
import { OrderStepUnitsComponent } from "@app/component/delivery-overview/order-creation-form/steps/order-step-units/order-step-units.component";
import { FormStep } from "../form-step/form-step.component";
import { CountryPickerGenericCell } from "../generic-cell/commons/country-picker.generic-cell";
import { OrderStepOrderComponent } from "@app/component/delivery-overview/order-creation-form/steps/order-step-order/order-step-order.component";
import { ZoneDialogOrderComponent } from "@app/component/delivery-overview/order-creation-form/steps/order-step-order/shared-component/adresse-for-shipping-order/zone-dialog-order/zone-dialog-order.component";
import { ToFixedPipe } from "../pipes/toFixed.pipe";
import { VignetteQrConsolidationComponent } from "../vignette-view/vignette-qr-consolidation/vignette-qr-consolidation.component";
import { AdresseForShippingOrderComponent } from "@app/component/delivery-overview/order-creation-form/steps/order-step-order/shared-component/adresse-for-shipping-order/adresse-for-shipping-order.component";
import { NewEtablissementCurrencyDesignComponent } from "@app/component/user/settings/my-company/new-etablissement-currency-design/new-etablissement-currency-design.component";
import { OrderOverviewComponent } from "../../component/delivery-overview/order-creation-form/steps/order-overview/order-overview.component";
import { CustomFieldGenericCell } from "../generic-cell/commons/custom-field.generic-cell";
import { OrderVisualisationComponent } from "@app/component/delivery-overview/order-visualisation/order-visualisation.component";
import { OrderVisualisationHeaderComponent } from "@app/component/delivery-overview/order-visualisation/order-visualisation-header/order-visualisation-header.component";
import { SortablejsModule } from "ngx-sortablejs";
import { InputSwitchModule } from "primeng/inputswitch";
import { OrderStepDeliveryComponent } from "@app/component/delivery-overview/order-creation-form/steps/order-step-delivery/order-step-delivery.component";
import { PlanTransportNewComponent } from "@app/component/user/settings/my-company/plan-transport-new/plan-transport-new.component";
import { GlobalPlanTransportNewComponent } from "@app/component/user/settings/my-company/global-plan-transport-new/global-plan-transport-new.component";
import { OrderPlannableComponent } from "@app/component/delivery-overview/order-creation-form/steps/order-step-delivery/order-plannable/order-plannable.component";
import { FilterCallbackPipe } from "../pipes/filter-callback.pipe";
import { OrderCreationComponent } from "@app/component/delivery-overview/order-creation/order-creation.component";
import { DocumentConditionRulesFormComponent } from "@app/component/user/settings/control-rules/document-control-rules-form/document-condition-rules-form/document-condition-rules-form.component";
import { ModalExportDocFormComponent } from "../modal-export-doc-by-template/modal-export-doc-form/modal-export-doc-form.component";
// import { TagInputModule } from 'ngx-chips';
import { NotificationItemComponent } from "../notification-item/notification-item.component";
import { NotificationListComponent } from "../notification-list/notification-list.component";
import { VirtualScrollerModule } from "primeng/virtualscroller";
import { UnitChildrenCell } from "@app/component/pricing-booking/shared-component/goods-information/cells/unit-children-select.component";
import { AddressSelectionComponent } from "../address-selection/address-selection.component";
import { SelectSimpleListGenericCellComponent } from "../generic-cell/commons/SelectSimpleListGenericCell/SelectSimpleListGenericCell.component";
import { DateFormatterPipe } from "../pipes/date-formatter.pipe";
import { HtmlTranslatePipe } from "../pipes/html-translate.pipe";
import { LocalNumberFormatPipe } from "../pipes/local-number-format.pipe";
import { TruncatePipe } from "../pipes/truncate.pipe";
import {TwoDigitDecimalNumberDirective} from "@app/shared/directives/two-digit-decimal-number.directive";
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

export function jwtTokenGetter() {
	return localStorage.getItem(TOKEN_NAME);
}

@NgModule({
	imports: [
		HttpClientModule,
		NgSelectModule,
		CommonModule,
		FormsModule,
		EditorModule,
		NgBoostrapModule,
		RouterModule,
		TextMaskModule,
		ReactiveFormsModule,
		TranslateModule,
		OverlayPanelModule,
		MultiSelectModule,
		ArchwizardModule,
		NgxUploaderModule,
		CalendarModule,
		ButtonModule,
		ChipsModule,
		TableModule,
		ListboxModule,
		PanelModule,
		TabViewModule,
		PaginatorModule,
		ContextMenuModule,
		ScheduleModule,
		PaginatorModule,
		ProgressSpinnerModule,
		Ng2CompleterModule,
		DialogModule,
		CheckboxModule,
		AutoCompleteModule,
		ColorPickerModule,
		DataTablesModule,
		ProgressBarModule,
		TreeModule,
		SelectButtonModule,
		RadioButtonModule,
		DragDropModule,
		AccordionModule,
		ToggleButtonModule,
		KeyFilterModule,
		NgbTabsetModule,
		SortablejsModule,
		InputSwitchModule,
		VirtualScrollerModule,
		InfiniteScrollModule,
	],
	declarations: [
		ModalComponent,
		EqualValidator,
		CarrousselAccountComponent,
		ChatPanelComponent,
		CommentPanelComponent,
		AdvancedSearchComponent,
		CollapsiblePanelComponent,
		AdvancedFormSearchComponent,
		OldAutocompleteComponent,
		FavoriComponent,
		DatetimePickerComponent,
		DocumentsComponent,
		PopUpDetailsDemandeComponent,
		FormAdresseComponent,
		NewEtablissementCurrencyDesignComponent,
		CotationComponent,
		BoolToYesNoPipe,
		ToFixedPipe,
		SafePipe,
		EllipsisPipe,
		VarDirective,
		SelectOptionComponent,
		GenericTableComponent,
		GenericTableCcpComponent,
		TemplateContainerComponent,
		MarchandiseDisplayComponent,
		GenericCellHoster,
		ShippingAdresseComponentComponent,
		GenericTableAutocompleteComponent,
		FlagComponent,
		FlagMultiSelectComponent,
		FlagListViewComponent,
		FlagListEditableComponent,
		TagListEditableComponent,
		UsersAutocompleteComponent,
		NotificationsComponent,
		VehiculeContentComponent,
		CustomFieldGenericCell,
		ColorPickerGenericCell,
		FlagSingleViewGenericCell,
		TextInputGenericCell,
		FlagColumnGenericCell,
		CommentIconPopupGenericCell,
		SelectGenericCell,
		UnitChildrenCell,
		ButtonGenericCell,
		DatePickerGenericCell,
		CountryPickerGenericCell,
		GoodsInformationComponent,
		ShippingInformationComponent,
		StepCotationComponent,
		CarrierChoiceComponent,
		ExternalUsersComponent,
		ExternalUserPartnerComponent,
		CardListComponent,
		AdresseForShippingComponent,
		QmTransportInformationComponent,
		AdvancedEditorComponent,
		AutocompleteComponent,
		ForEachItemDirective,
		DeliverySearchComponent,
		DocTemplateGenDetailsComponent,
		DemandeOverviewComponent,
		ModalExportDocByTemplateComponent,
		BreadcrumbComponent,
		VignetteViewComponent,
		VignetteHeaderComponent,
		VignettePricingComponent,
		VignetteCptmComponent,
		VignetteTracingComponent,
		FormErrorMessagesComponent,
		VignetteFavorisComponent,
		PCalForceUTCDirective,
		CompanyPickerComponent,
		RequiredIfDirective,
		ForbiddenValuesDirective,
		ZoneDialogComponent,
		ModalConfirmationComponent,
		OrderCreationComponent,
		OrderCreationFormComponent,
		OrderStepReferenceComponent,
		OrderStepUnitsComponent,
		OrderStepOrderComponent,
		ZoneDialogOrderComponent,
		AdresseForShippingOrderComponent,
		FormStep,
		VignetteQrConsolidationComponent,
		OrderOverviewComponent,
		OrderVisualisationComponent,
		OrderVisualisationHeaderComponent,
		WeekTemplateComponent,
		GlobalPlanTransportNewComponent,
		PlanTransportNewComponent,
		OrderStepDeliveryComponent,
		OrderPlannableComponent,
		FilterCallbackPipe,
		AddressSelectionComponent,
		DocumentControlRulesFormComponent,
		DocumentConditionRulesFormComponent,
		ModalExportDocFormComponent,
		NotificationItemComponent,
		NotificationListComponent,
		DateFormatterPipe,
		HtmlTranslatePipe,
		SelectSimpleListGenericCellComponent,
		LocalNumberFormatPipe,
		TruncatePipe,
		TwoDigitDecimalNumberDirective
	],
	exports: [
		NgSelectModule,
		CommonModule,
		FormsModule,
		EditorModule,
		NgBoostrapModule,
		RouterModule,
		TextMaskModule,
		ReactiveFormsModule,
		TranslateModule,
		NgBoostrapModule,
		ArchwizardModule,
		TableModule,
		ModalComponent,
		EqualValidator,
		CarrousselAccountComponent,
		ChatPanelComponent,
		CommentPanelComponent,
		AdvancedSearchComponent,
		CollapsiblePanelComponent,
		AdvancedFormSearchComponent,
		OldAutocompleteComponent,
		FavoriComponent,
		DatetimePickerComponent,
		DocumentsComponent,
		PopUpDetailsDemandeComponent,
		FormAdresseComponent,
		NewEtablissementCurrencyDesignComponent,
		CotationComponent,
		BoolToYesNoPipe,
		ToFixedPipe,
		SafePipe,
		EllipsisPipe,
		VarDirective,
		SelectOptionComponent,
		GenericTableComponent,
		ScheduleModule,
		GenericTableCcpComponent,
		TemplateContainerComponent,
		MarchandiseDisplayComponent,
		GenericCellHoster,
		ShippingAdresseComponentComponent,
		FlagComponent,
		FlagMultiSelectComponent,
		FlagListViewComponent,
		FlagListEditableComponent,
		TagListEditableComponent,
		UsersAutocompleteComponent,
		NotificationsComponent,
		PanelModule,
		CheckboxModule,
		VehiculeContentComponent,
		ColorPickerGenericCell,
		TextInputGenericCell,
		FlagColumnGenericCell,
		CommentIconPopupGenericCell,
		CountryPickerGenericCell,
		SelectGenericCell,
		UnitChildrenCell,
		ButtonGenericCell,
		DatePickerGenericCell,
		CustomFieldGenericCell,
		ColorPickerModule,
		FlagSingleViewGenericCell,
		GoodsInformationComponent,
		ShippingInformationComponent,
		StepCotationComponent,
		CarrierChoiceComponent,
		ProgressBarModule,
		DataTablesModule,
		CalendarModule,
		ExternalUsersComponent,
		ExternalUserPartnerComponent,
		CardListComponent,
		ProgressSpinnerModule,
		AdresseForShippingComponent,
		QmTransportInformationComponent,
		SelectButtonModule,
		RadioButtonModule,
		AdvancedEditorComponent,
		AutocompleteComponent,
		ForEachItemDirective,
		DeliverySearchComponent,
		DocTemplateGenDetailsComponent,
		DragDropModule,
		AccordionModule,
		ToggleButtonModule,
		DemandeOverviewComponent,
		ModalExportDocByTemplateComponent,
		BreadcrumbComponent,
		VignetteViewComponent,
		FormErrorMessagesComponent,
		PCalForceUTCDirective,
		CompanyPickerComponent,
		NgbTabsetModule,
		RequiredIfDirective,
		ForbiddenValuesDirective,
		ZoneDialogComponent,
		ModalConfirmationComponent,
		OrderStepOrderComponent,
		ZoneDialogOrderComponent,
		AdresseForShippingOrderComponent,
		OrderOverviewComponent,
		WeekTemplateComponent,
		AddressSelectionComponent,
		FilterCallbackPipe,
		DocumentControlRulesFormComponent,
		DocumentConditionRulesFormComponent,
		ModalExportDocFormComponent,
		NotificationItemComponent,
		NotificationListComponent,
		VirtualScrollerModule,
		DateFormatterPipe,
		LocalNumberFormatPipe,
		TruncatePipe,
		TwoDigitDecimalNumberDirective
	],
	entryComponents: [
		ModalComponent,
		ColorPickerGenericCell,
		FlagSingleViewGenericCell,
		TextInputGenericCell,
		FlagColumnGenericCell,
		CommentIconPopupGenericCell,
		SelectGenericCell,
		UnitChildrenCell,
		ButtonGenericCell,
		DatePickerGenericCell,
		CountryPickerGenericCell,
		CustomFieldGenericCell,
		OrderCreationFormComponent,
		OrderStepReferenceComponent,
		OrderStepUnitsComponent,
		FormStep,
		OrderStepDeliveryComponent,
		SelectSimpleListGenericCellComponent,
	],
	providers: [
		ChatService,
		DateFormatterPipe,
		LocalNumberFormatPipe,
		{
			provide: HTTP_INTERCEPTORS,
			useClass: JwtTokenInterceptor,
			multi: true,
		},
	],
})
export class SharedModule {}
