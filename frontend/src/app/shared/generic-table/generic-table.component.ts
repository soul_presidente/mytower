import {
	EtatUser,
	GenericTableScreen,
	ViewType,
	ServiceType,
	FileDownLoadStatus,
} from "@app/utils/enumeration";
import {
	ChangeDetectorRef,
	Component,
	ContentChild,
	ElementRef,
	EventEmitter,
	Input,
	OnDestroy,
	OnInit,
	Output,
	TemplateRef,
	ViewChild,
	Renderer,
	OnChanges,
	SimpleChanges,
} from "@angular/core";
import { generaleMethodes } from "../generaleMethodes.component";
import { LazyLoadEvent, MenuItem, MessageService } from "primeng/api";
import { Router } from "@angular/router";
import { GenericTableService } from "@app/services/generic-table.service";
import { Table } from "primeng/table";
import { TranslateService } from "@ngx-translate/core";
import { Statique } from "@app/utils/statique";
import { ExportService } from "@app/services/export.service";
import { EbFavori } from "@app/classes/favori";
import { FavoriService } from "@app/services/favori.service";
import { UploadFile, UploadOutput } from "ngx-uploader";
import { UploadComponent } from "@app/utils/uploadComponent";
import { ForEachItemDirective } from "../../directives/for-each-item.directive";
import { GenericTableInfos } from "./generic-table-infos";
import { CardListParams } from "../card-list/card-list.params";
import { SearchCriteriaShipToMatrix } from "@app/utils/SearchCriteriaShipToMatrix";
import { Modules } from "@app/utils/enumeration";
import { Observable, Subject } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { GenericCell } from "../generic-cell/generic-cell";
import { Delivery } from '@app/classes/ebDelLivraison';

@Component({
	selector: "app-generic-table",
	templateUrl: "./generic-table.component.html",
	styleUrls: ["./generic-table.component.scss"],
})
export class GenericTableComponent extends UploadComponent implements OnInit, OnDestroy, OnChanges {
	Statique = Statique;
	ViewType = ViewType;
	EtatUser = EtatUser;
	@ViewChild("turboTable", { static: false })
	table: Table;
	globalSearchCriteria: any = {};
	afterFirstCall: boolean = false;
	oldRefreshCount: number = 0;
	oldOrtOrder: number;
	displayDialog: boolean = false;
	selectedRow: any = null;
	subtableWidth: string;

	@ViewChild("fileInput", { static: true })
	fileInput: ElementRef;
	serviceType: ServiceType;
	@Input()
	title: string;
	@Input()
	dataSourceLocal: any = []; // Data to use at first level table if setted to data source type local
	@Input()
	dataInfos: GenericTableInfos;
	@Input()
	detailsInfos: GenericTableInfos;
	@Input()
	component: number;
	@Input()
	dataSourceType: string; // to set to 'json' if many column values come from a single JSONB field as in the PlanTransportNew model with the field input(SearchCriteriaBookingPricing)
	@Input()
	module: number;
	@Input()
	hideTable: boolean;
	@Output()
	hideTableChange = new EventEmitter<boolean>();
	@Input()
	parentContext: any;
	// ce parametre sert a afficher le scroll horizontal sur l'ecran
	@Input()
	isHorizontalScrollAlwaysVisible: boolean;
	// ce parametre prend la valeur en pixel a mettre dans la methode "verticalScrollCalc"
	@Input()
	scrollHeight: number;
	dataUpload?: any;
	@Input()
	emptyDataMsg: string;
	@Input()
	sharedDropDownZIndex: number;
	@Input()
	gridVersion: number = null;
	@Output()
	gridVersionChange = new EventEmitter<number>();
	@Output()
	sharedDropDownZIndexEmit: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onLazyLoadingFirstLevel: EventEmitter<LazyLoadEvent>;
	@Output()
	onLazyLoadingSecondLevel: EventEmitter<LazyLoadEvent>;
	@Output()
	addFormEmitter: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	editFormEmitter: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	cloneFormEmitter: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	deleteEventEmitter: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onDataLoaded: EventEmitter<any> = new EventEmitter<any>();
	dataTableComponent = GenericTableScreen;
	@Output()
	onCheckRow: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onUploadCompleteHandler = new EventEmitter<any>();
	@Output()
	onUploadProgressHandler = new EventEmitter<UploadFile>();
	@Output()
	onHelpBtnClick = new EventEmitter<any>();
	newItem: boolean = false;
	csvDownloadLoading: boolean = false;

	GenericCellParams = GenericCell.Params;

	@Input("searchInput")
	set searchInput(searchInput) {
		this.initialiseGlobalCriteria(searchInput);
		if (this.afterFirstCall) {
			let event: LazyLoadEvent;
			if (this.dataInfos.paginator) {
				this.globalSearchCriteria.size = 10;
				if (this.dataInfos.numberDatasPerPage) {
					if (this.dataInfos.numberDatasPerPage <= this.rowsPerPageOptions[this.rowsPerPageOptions.length-1]) {
						this.globalSearchCriteria.size = this.dataInfos.numberDatasPerPage;		
					}else {
						this.globalSearchCriteria.size = this.rowsPerPageOptions[this.rowsPerPageOptions.length-1]
					}
				}
				
				this.globalSearchCriteria.pageNumber = 0;
			} else {
				this.globalSearchCriteria.size = null;
				this.globalSearchCriteria.pageNumber = null;
			}
			this.emitEventFirstLevel(event);
		}
	}
	datasChecked: any[] = [];
	datas: any[] = [];
	detailsDatas: any[] = [];
	listData: any[] = [];
	detailsDatasAlreadyGet: any[] = [];
	datasTotalRecords: number;
	detailsDatasTotalRecords: number;
	loading: boolean;
	isLoading: boolean = true;
	loadingDetail: boolean;
	showContent: boolean = false;
	icon: string = "pi pi-minus";
	searchterm: string;
	rowsPerPageOptions: number[] = [5, 10, 25, 50];
	fromPage: number;
	toPage: number;

	selectedActionRow: any = null;
	previousRow: any = null;

	loadingDetailDatas: boolean;
	currentIndex: number;

	contextMenuItems: MenuItem[] = [];
	allChecked: boolean = false;
	mapAttributes: any = {};
	listFavoris: Array<EbFavori>;

	startUploading: number;
	outputFile: UploadOutput;

	viewType: number = ViewType.TABLE;
	cardCols: Array<any>;
	cardActions: Array<any>;
	isViewChange: boolean = false;
	ConstantsTranslate: Object;

	RenderModeTable = GenericTableInfos.Col.RenderModeTable;
	RenderModeCard = CardListParams.RenderMode;
	// ce variable concerne la valeur a mettre dans l'attribut scrollHeight avec la propriete css calc()
	scrollHeightCalc: string;
	@Input()
	listEltsToAdjustHeight: string[] = [];

	@ContentChild(ForEachItemDirective, { read: TemplateRef, static: false })
	cardContentTemplate;

	isCodeExist: boolean = false;
	isFieldRequiredFilled: boolean = false;
	Modules = Modules;
	searchInputSubject: Subject<string> = new Subject();

	enableSubTablePagination: boolean = false;

	@Input()
	hookAfterDataLoaded: (data: any) => {} = (data: any) => data;
	@Input()
	componentContext: any;

	canExtractData: String;
	cannotExtractData: String;

	constructor(
		protected generaleMethode: generaleMethodes,
		protected translate: TranslateService,
		protected exportService: ExportService,
		protected router: Router,
		protected service: GenericTableService,
		protected messageService: MessageService,
		protected favoriService: FavoriService,
		protected cd: ChangeDetectorRef,
		protected elRef: ElementRef,
		protected renderer: Renderer
	) {
		super();
		this.searchInputSubject
			.pipe(
				debounceTime(500),
				distinctUntilChanged()
			)
			.subscribe((searchterm) => {
				this.searchAction();
			});
	}
	inputValueChanged(event) {
		this.searchInputSubject.next(event);
	}
	ngOnInit() {
		if (!this.dataInfos.numberDatasPerPage) {
			this.globalSearchCriteria.size = this.rowsPerPageOptions[0];
			this.dataInfos.numberDatasPerPage = this.globalSearchCriteria.size;
		} else {
			this.globalSearchCriteria.size = this.dataInfos.numberDatasPerPage;
		}
		this.globalSearchCriteria.connectedUserNum = this.userConnected.ebUserNum;
		this.globalSearchCriteria.pageNumber = 0;
		this.loadingDetail = false;
		this.loading = false;
		this.isLoading = false;
		this.showContent = false;
		this.initGenericTable();
		this.globalSearchCriteria.withPassword = true;
		this.verticalScrollCalc(this.scrollHeight);
		this.initAdvancedSearchListener();
		this.checkIfExportIsRunning();

		document.addEventListener(
			"export-state",
			function(e) {
				e.preventDefault();
				e.stopPropagation();
				this.stopExport(e);
			}.bind(this),
			false
		);

		this.canExtractData = this.translate.instant("GENERAL.EXTRACT_DATA");
		this.cannotExtractData = this.translate.instant("GENERAL.NOT_EXTRACT_DATA");
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["dataSourceLocal"]) {
			this.refreshLocalDataSource();
		}
	}

	checkIfExportIsRunning() {
		if (
			localStorage.getItem("exportCode") &&
			localStorage.getItem("moduleExport") == this.module.toString()
		) {
			this.csvDownloadLoading = true;
		}
	}

	stopExport(e) {
		if (this.csvDownloadLoading) {
			this.csvDownloadLoading = false;
			if (e.detail == FileDownLoadStatus.FIN) {
				this.messageService.add({
					severity: "success",
					summary: this.translate.instant("GENERAL.EXPORT_FINISHED"),
				});
			} else if (e.detail == FileDownLoadStatus.ERROR) {
				this.messageService.add({
					severity: "error",
					summary: this.translate.instant("GENERAL.EXPORT_FAILED"),
				});
			} else if (e.detail == FileDownLoadStatus.LIMITE_DEPASSE) {
				this.messageService.add({
					severity: "error",
					summary: this.translate.instant("GENERAL.EXPORT_LIMITE_EXCEDED"),
				});
			}
		}
	}

	addNewUser() {
		this.router.navigateByUrl("app/user/settings/company/users/creation");
	}
	profileActionEvent() {
		let CostumEvent = new CustomEvent("profile-action-event");
		document.dispatchEvent(CostumEvent);
	}

	transaleteTexte() {
		this.ConstantsTranslate = {
			CONFIRM_REMOVE_ADMIN_RIGHTS: "",
			CONFIRM_SET_AS_ADMIN: "",
			CONFIRM_DEACTIVATE_USER: "",
			CONFIRM_ACTIVATE_USER: "",
			CONFIRM_RESENT_ACTIVATION_MAIL: "",
		};
		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate
				.get("REPOSITORY_MANAGEMENT." + key)
				.subscribe((res) => (this.ConstantsTranslate[key] = res));
		});
	}

	async initGenericTable() {
		this.isLoading = true;
		if(this.dataInfos.dataType != Delivery)
			await this.loadCustomColumns();
		await this.load();
		this.convertDataInfosAnonymousToObject();

		// Bind context menu items
		this.contextMenuItems = [];
		if (this.dataInfos.contextMenuItems != null) {
			this.dataInfos.contextMenuItems.forEach((cti) => {
				cti.command = (event?) => {
					cti.onClick(this.selectedRow, event);
				};

				if (cti.labelTranslatable && !cti.labelTranslated) {
					cti.label = this.translate.instant(cti.label);
					cti.labelTranslated = true;
				}

				this.contextMenuItems.push(cti);
			});
		}

		this.showContent = true;
	}

	loadFavoris() {
		if (!this.dataInfos.favorisName) return;

		if (this.listFavoris) {
			this.setFavoris();
			return;
		}
		this.favoriService
			.getListFavoris(Statique.dataSourceFavoris + "/list-favori" + "?module=" + this.module)
			.subscribe((favs: Array<EbFavori>) => {
				this.listFavoris = favs;
				this.setFavoris();
			});
	}
	setFavoris() {
		if (this.listFavoris && this.listFavoris.length > 0) {
			this.datas.forEach((it) => {
				let fv = this.listFavoris.find((f) => f.idObject == it[this.dataInfos.dataKey]);
				if (fv) this.setMapAttribute(it[this.dataInfos.dataKey], "xEbFavoriNum", fv.ebFavoriNum);
			});
		}
	}

	loadCustomColumns() {}

	refreshLocalDataSource() {
		if (
			this.dataInfos &&
			this.dataInfos.dataSourceType === GenericTableInfos.DataSourceType.Local
		) {
			this.datas = this.dataSourceLocal ? this.dataSourceLocal : [];
			this.datasTotalRecords = this.datas.length;
			this.onDataLoaded.emit(this.datas);
		}
	}
	async refreshData() {
		let event: LazyLoadEvent;
		await this.emitEventFirstLevel(event);
		this.refreshLocalDataSource();
	}

	download() {
		this.messageService.add({
			severity: "success",
			summary: "Operation Success",
			detail: "Telechargement ...",
		});
	}

	searchAction() {
		this.initialiseGlobalCriteria(null);
		this.searchterm = this.searchterm == "" ? null : this.searchterm;
		this.globalSearchCriteria.searchterm = this.searchterm;
		let event: LazyLoadEvent;
		this.emitEventFirstLevel(event);
	}

	hideAndShowContent() {
		this.showContent = !this.showContent;
		this.icon = this.showContent ? "pi pi-minus" : "pi pi-plus";
	}

	initialiseGlobalCriteria(searchInput: any) {
		if (searchInput != null) {
			this.globalSearchCriteria = searchInput;
		}
		this.globalSearchCriteria.extractData = false;
	}

	goToDetails(ref: any) {
		if (this.dataInfos.actionDetailsLink)
			this.router.navigate([this.dataInfos.actionDetailsLink + ref]);
	}

	getDetailsDatas(index, ref, expanded) {
		this.currentIndex = index;
		this.loadingDetailDatas = true;
		if (this.dataInfos.enableSubTablePagination) {
			this.enableSubTablePagination = true;
		}

		if (this.dataInfos.dataDetailsSourceType === GenericTableInfos.DataSourceType.Local) {
			if (this.datas[index].dataDetailsField != null) {
				this.affecteDetailsInfos(index);
				this.loadingDetailDatas = false;
			}
		} else {
			if (!expanded && this.detailsInfos) {
				if (!this.dataDetailsExist(ref)) {
					if (this.detailsInfos.dataLink) {
						this.service.getDetailsDatas(this.detailsInfos.dataLink, ref).subscribe(
							(res) => {
								this.datas[index].dataDetailsField = res;
								this.loadingDetailDatas = false;
								this.detailsDatasAlreadyGet.push({ key: ref, datas: res });
								this.affecteDetailsInfos(index);
							},
							(error) => {
								this.loadingDetailDatas = false;
								this.tasterErrorLoadingData();
								console.error(error);
							}
						);
					} else {
						this.datas[index].dataDetailsField = this.datas[index][this.dataInfos.dataDetailsField];
						this.loadingDetailDatas = false;
						this.detailsDatasAlreadyGet.push({
							key: ref,
							datas: this.datas[index][this.dataInfos.dataDetailsField],
						});
						this.affecteDetailsInfos(index);
					}
				} else {
					this.datas[index].dataDetailsField = this.detailsDataStored(ref);
					this.loadingDetailDatas = false;
					this.adjustSubTableWidthWithPaginator();
				}
			}
			this.adjustSubTableWidthWithPaginator();
		}
	}

	setDatasChecked(index, ref, row, expanded, e) {
		if (e.target.checked) {
			this.datasChecked.push({ key: ref, datas: this.datas[index] });
		} else {
			this.deleteDatasNotChecked(ref);
		}

		this.onCheckRow.emit({
			datasChecked: this.datasChecked,
			rowModel: row,
		});
	}

	deleteDatasNotChecked(ref) {
		let datas = [];
		this.datasChecked.forEach((d) => {
			if (d.key != ref) datas.push(d);
		});
		this.datasChecked = datas;
	}

	affecteDetailsInfos(index) {
		this.detailsDatasTotalRecords = this.datas[index].dataDetailsField.length;
		if (this.detailsDatasTotalRecords <= this.rowsPerPageOptions[0]) {
			this.enableSubTablePagination = false;
		}
		this.loadingDetail = false;
		this.adjustSubTableWidthWithPaginator();
	}

	dataDetailsExist(ref) {
		let returValue = false;
		this.detailsDatasAlreadyGet.forEach((element) => {
			if (element.key == ref) {
				returValue = true;
			}
		});
		return returValue;
	}

	detailsDataStored(ref) {
		let datas;
		this.detailsDatasAlreadyGet.forEach((element) => {
			if (element.key == ref) {
				datas = element.datas;
			}
		});
		return datas;
	}

	paginate(event) {
		this.globalSearchCriteria.pageNumber = event.page;
		this.globalSearchCriteria.size = event.rows;
		this.dataInfos.numberDatasPerPage = this.globalSearchCriteria.size;
		this.affecteFromAndToValues();
		this.emitEventFirstLevel(event);
	}

	affecteFromAndToValues() {
		this.fromPage = this.globalSearchCriteria.pageNumber * this.dataInfos.numberDatasPerPage + 1;
		let currentMax = (this.globalSearchCriteria.pageNumber + 1) * this.dataInfos.numberDatasPerPage;
		this.toPage = currentMax > this.datasTotalRecords ? this.datasTotalRecords : currentMax;
	}

	emitEventFirstLevel(event: LazyLoadEvent) {
		return new Promise(async (resolve, reject) => {
			if (this.isViewChange && this.datas && this.datas.length) {
				this.isViewChange = false;
				return resolve(this.datas);
			}
			this.isViewChange = false;

			this.isLoading = true;

			if (event != undefined) {
				if (event.sortOrder != undefined) {
					this.globalSearchCriteria.ascendant = event.sortOrder !== 1;
				}
				this.globalSearchCriteria.orderedColumn =
					event.sortField == undefined ? this.globalSearchCriteria.orderedColumn : event.sortField;
				this.globalSearchCriteria.pageNumber = event["page"] || event.first;
			}
			if (this.dataInfos.dataSourceType === GenericTableInfos.DataSourceType.Local) {
				// Nothing to do here, managed by Angular with ngOnChanges
				this.loading = false;
				this.isLoading = false;
				this.afterFirstCall = true;
				resolve(this.dataSourceLocal);
			} else {
				if (this.loading == false && !this.dataInfos.dataLink) {
					// Previously dataLink not checked before calling getDatas,
					// but keep a console.error here to log this king of error
					console.warn("GenericTable : dataLink was null or empty");
				}
				if (this.loading == false && this.dataInfos.dataLink) {
					this.service.getDatas(this.dataInfos.dataLink, this.globalSearchCriteria).subscribe(
						(res) => {
							res = this.hookAfterDataLoaded.call(this.componentContext || this, res);
							if (this.dataInfos.dataType != null) {
								this.datas = Statique.cloneListObject(res.data, this.dataInfos.dataType);
							} else {
								this.datas = res.data;
							}

							// Post Load Data Management
							this.dataInfos.cols.forEach((col) => {
								if (col.field != null && col.postLoadDataConverter != null) {
									this.datas.forEach((data) => {
										if (data[col.field] != null) {
											data[col.field] = col.postLoadDataConverter(data[col.field], data);
										}
									});
								}
							});

							this.datasTotalRecords = res.count;
							this.loading = false;
							this.isLoading = false;
							this.afterFirstCall = true;
							this.loadFavoris();

							this.initIterators();

							this.affecteFromAndToValues();
							this.onDataLoaded.emit(res.data);
							resolve(res.data);
						},
						(error) => {
							this.isLoading = false;
							this.tasterErrorLoadingData();
							console.error(error);
							reject("Error loading data");
						}
					);
				} else {
					this.loading = false;
					reject("Loading deactivated");
				}
			}
		});
	}

	/**
	 * Find row by checking value of the dataKey specified on dataInfos
	 * @param dataKeyValue Value to search in the dataKey field (generally entity identifier)
	 * @returns Found row or undefined
	 */
	findRowByDataKeyValue(dataKeyValue: any): any {
		let returnValue = undefined;

		this.datas.forEach((data) => {
			if (data[this.dataInfos.dataKey] != null && data[this.dataInfos.dataKey] === dataKeyValue) {
				returnValue = data;
			}
		});

		return returnValue;
	}

	/**
	 * Replace row data using dataKey
	 * Search for row that contains the dataKey (specified in dataInfos) value equals the dataKey value of passed object.
	 * If the value not exist, print a warn and do nothing.
	 * During the copy, it will take care od doing all data managements (like postLoadDataConverter)
	 * @param row The row to copy
	 */
	replaceRowUsingDataKey(row: any) {
		if (!row[this.dataInfos.dataKey]) {
			console.warn("GenericTable : dataKey doesn't exist on row : " + this.dataInfos.dataKey);
		}
		const foundRow = this.findRowByDataKeyValue(row[this.dataInfos.dataKey]);
		if (foundRow == null) {
			console.warn(
				"GenericTable : Value not found for dataKey value : " + row[this.dataInfos.dataKey]
			);
			return;
		}

		this.dataInfos.cols.forEach((col) => {
			if (col.field != null) {
				foundRow[col.field] = row[col.field];
				if (col.postLoadDataConverter != null && foundRow[col.field] != null) {
					foundRow[col.field] = col.postLoadDataConverter(foundRow[col.field], foundRow);
				}
			}
		});
	}

	saveData(row: any) {
		this.service.runAction(this.dataInfos.saveLink, row);
	}

	deleteData(row: any) {
		this.service.runAction(this.dataInfos.deleteLink, row);
	}

	async generateXLSX(event) {
		this.csvDownloadLoading = true;
		this.messageService.add({
			severity: "info",
			summary: this.translate.instant("GENERAL.EXPORT_STARTED"),
		});
		let config = this.getDtConfig();
		if(this.module == Modules.FREIGHT_AUDIT){
			config = Statique.addCostItemDetailToDtConfig(config);
		}
		if (!this.dataInfos.extractWithAdvancedSearch) {
			try {
				await this.exportService.startExportCsv(
					this.module,
					false,
					this.globalSearchCriteria,
					null,
					config
				);
			} catch (e) {
				this.csvDownloadLoading = false;
				this.messageService.add({
					severity: "error",
					summary: this.translate.instant("GENERAL.EXPORT_FAILED"),
				});
			}
		} else {
			this.exportService.sendMessage(
				config,
				this.searchterm,
				function() {
					this.csvDownloadLoading = false;
				}.bind(this)
			);
			this.exportService.clearMessage();
		}
	}

	getDtConfig() {
		let dtConfig = [],
			index = 1;
		this.dataInfos.selectedCols.forEach((col) => {
			dtConfig.push({ title: col.header, name: col.field, order: index++ });
		});
		return dtConfig;
	}

	downloadFile(url) {
		window.open(url);
	}

	save(event: Event) {
		let config: GenericTableInfos.TableConfig = new GenericTableInfos.TableConfig();
		config.size = this.globalSearchCriteria.size;
		config.pageNumber = this.globalSearchCriteria.pageNumber;
		config.orderedColumn = this.globalSearchCriteria.orderedColumn;
		config.selectedCols = this.dataInfos.selectedCols.map((it) => ({
			field: it.field,
			header: it.header,
			translateCode: it.translateCode,
			width: (it.width && it.width.includes("-")) ? it.width.replace(/-/gi,"") : it.width,
		}));
		config.viewType = this.viewType;
		config.sortOrder = this.globalSearchCriteria.ascendant;
		let dataSent = JSON.stringify(config);

		let configData: GenericTableInfos.ConfigData = new GenericTableInfos.ConfigData();
		configData.datatableCompId = this.component;
		configData.ebUserNum = this.userConnected.ebUserNum;
		configData.state = dataSent;

		this.service.saveTableConfig(configData).subscribe((res) => {
			this.messageService.add({
				severity: "success",
				summary: "Operation Success",
				detail: "Table state saved in database",
			});
		});
	}

	load() {
		let config: GenericTableInfos.TableConfig = new GenericTableInfos.TableConfig();
		config.size = this.dataInfos.numberDatasPerPage;
		config.pageNumber = this.globalSearchCriteria.pageNumber;
		config.orderedColumn = this.globalSearchCriteria.orderedColumn;
		config.selectedCols = this.dataInfos.selectedCols;
		let configData: GenericTableInfos.ConfigData = new GenericTableInfos.ConfigData();
		configData.datatableCompId = this.component;
		configData.ebUserNum = this.userConnected.ebUserNum;

		return new Promise((resolve) => {
			this.service.loadTableConfig(configData).subscribe((res) => {
				if (res != null) {
					let config: GenericTableInfos.TableConfig = new GenericTableInfos.TableConfig();
					config = res;
					if (config.viewType) this.viewType = config.viewType;
					if (config.selectedCols && config.selectedCols.length > 0) {
						this.dataInfos.selectedCols = [];
						config.selectedCols.forEach((it) => {
							let foundCol;
							if (this.dataSourceType == "json") {
								foundCol = this.dataInfos.cols.find((cl) => cl.header == it.header);
							} else {
								foundCol = this.dataInfos.cols.find((cl) => cl.field == it.field);
							}
							if (foundCol && it.width) foundCol.width = (it.width && it.width.includes("-")) ? it.width.replace(/-/gi,"") : it.width;
							foundCol && this.dataInfos.selectedCols.push(foundCol);
						});
					}
					if (config.size != undefined && 
						config.size > this.rowsPerPageOptions[this.rowsPerPageOptions.length-1]) {
							config.size = this.rowsPerPageOptions[this.rowsPerPageOptions.length-1];
					}

					if (config.size != undefined) this.dataInfos.numberDatasPerPage = config.size;
					this.globalSearchCriteria.pageNumber = this.dataInfos.numberDatasPerPage || 0;
					if (config.size) this.globalSearchCriteria.size = config.size;
				} else {
					this.globalSearchCriteria.size = this.dataInfos.numberDatasPerPage;
					this.globalSearchCriteria.pageNumber = 0;
					this.globalSearchCriteria.ascendant = true;
					this.oldOrtOrder = this.globalSearchCriteria.ascendant ? 1 : 0;
				}

				this.affecteFromAndToValues();
				this.setCardAttributes();

				resolve();
			});
		});
	}

	setDataRow(key: number, data: any, field: string = null) {
		let i,
			n = this.datas.length;
		for (i = 0; i < n; i++) {
			if (this.datas[i][this.dataInfos.dataKey] == key) {
				if (!field) this.datas[i] = data;
				else this.datas[i][field] = data;
				break;
			}
		}
	}
	getDataRow(key: number) {
		let row = JSON.parse(
			JSON.stringify(this.datas.find((it) => it[this.dataInfos.dataKey] == key))
		);
		return row;
	}
	getSelectedRows() {
		let rows: any = JSON.parse(
			JSON.stringify(
				this.datas.filter((it) =>
					this.isMapAttributeExists(it[this.dataInfos.dataKey], "checked", true)
				)
			)
		);
		return rows;
	}
	unselectAll() {
		this.checkAllChanged({ target: { checked: false } });
	}
	selectAll() {
		this.checkAllChanged({ target: { checked: true } });
	}
	checkChanged(row, $event, index, expanded) {
		if ($event.target.checked) {
			this.setMapAttribute(row[this.dataInfos.dataKey], "checked", true);
			if (
				!this.datas.find(
					(it) => !this.isMapAttributeExists(it[this.dataInfos.dataKey], "checked", true)
				)
			)
				this.allChecked = true;
		} else {
			this.deleteMapAttribute(row[this.dataInfos.dataKey], "checked");
			this.allChecked = false;
		}

		if (this.dataInfos.dataCheckedField) {
			row[this.dataInfos.dataCheckedField] =
				$event.target.checked != null ? $event.target.checked : false;
		}

		// TODO: à optimiser, actuellement deux methodes traitant les cases à cocher est implémenté
		// voir laquelle effective pour la garder à long terme
		this.setDatasChecked(index, row[this.dataInfos.dataKey], row, expanded, $event);
	}

	checkAllChanged($event) {
		if ($event.target.checked)
			this.datas.forEach((it) => this.setMapAttribute(it[this.dataInfos.dataKey], "checked", true));
		else this.datas.forEach((it) => this.deleteMapAttribute(it[this.dataInfos.dataKey], "checked"));
		this.allChecked = $event.target.checked;

		this.onCheckRow.emit();
	}

	favorisHandler(row) {
		if (this.isMapAttributeExists(row[this.dataInfos.dataKey], "xEbFavoriNum")) {
			this.favoriService
				.deleteFavoriById(this.mapAttributes[row[this.dataInfos.dataKey]].xEbFavoriNum)
				.subscribe(() => {
					this.deleteMapAttribute(row[this.dataInfos.dataKey], "xEbFavoriNum");
				});
		} else {
			let ebFavori: EbFavori = new EbFavori();
			ebFavori.idObject = row[this.dataInfos.dataKey];
			ebFavori.refObject = this.dataInfos.favorisName(row);
			ebFavori.module = this.module;
			ebFavori.user.ebUserNum = this.userConnected.ebUserNum;
			this.favoriService.addFavori(ebFavori).subscribe((data: EbFavori) => {
				this.setMapAttribute(row[this.dataInfos.dataKey], "xEbFavoriNum", data.ebFavoriNum);
			});
		}
	}
	setMapAttribute(id, attr, val) {
		if (!this.mapAttributes[id]) this.mapAttributes[id] = {};
		this.mapAttributes[id][attr] = val;
	}
	deleteMapAttribute(id, attr) {
		if (this.mapAttributes[id]) {
			delete this.mapAttributes[id][attr];
			let o,
				exists = false;
			for (o in this.mapAttributes[id]) {
				exists = true;
				break;
			}
			if (!exists) delete this.mapAttributes[id];
		}
	}
	isMapAttributeExists(id, attr, val = undefined) {
		return (
			this.mapAttributes[id] &&
			this.mapAttributes[id].hasOwnProperty(attr) &&
			(val == undefined || this.mapAttributes[id][attr] == val)
		);
	}

	initIterators() {
		if (!this.dataInfos.dataIterators) return;

		this.dataInfos.dataIterators.forEach((it) => {
			it.idInterval && clearInterval(it.idInterval);
			it.idInterval = setInterval(
				function() {
					it.handler(this.datas, it);
				}.bind(this),
				it.timer
			);
		});
	}
	clearIterators() {
		this.dataInfos.dataIterators &&
			this.dataInfos.dataIterators.forEach((it) => it.idInterval && clearInterval(it.idInterval));
	}

	ngOnDestroy() {
		this.clearIterators();
		this.initAdvancedSearchListener(true);
	}

	isRowChecked(row, dataInfos): boolean {
		if (dataInfos.dataCheckedField) {
			return row[dataInfos.dataCheckedField] != null ? row[dataInfos.dataCheckedField] : false;
		}
		return this.allChecked || this.isMapAttributeExists(row[dataInfos.dataKey], "checked", true);
	}

	onUploadComplete(file: UploadFile) {
		if (file.response && file.response.ident)
			this.onUploadCompleteHandler.emit({ ident: file.response.ident });
		else this.onUploadCompleteHandler.emit({ error: true });
	}
	onUploadProgress(file: UploadFile) {
		this.onUploadProgressHandler.emit(file);
	}
	initUploading() {
		setTimeout(
			function() {
				this.startUpload();
			}.bind(this),
			500
		);
	}
	async fileInputHandle() {
		this.startUploading = 0;
		this.fileInput.nativeElement.value = null;
		this.outputFile = null;

		this.componentObject = {
			dataUpload: this.dataUpload,
		};

		this.fileInput.nativeElement.click();

		this.initUploading();
	}
	uploadFile(output: UploadOutput) {
		this.onUploadOutput(output);
	}

	@ViewChild("autoComplete", { static: false })
	autocomplete;

	searchVisible: boolean = false;

	display(rowIndex: number) {
		this.selectedActionRow = this.datas[rowIndex];
		if (this.autocomplete != null) {
			this.autocomplete.ngOnInit();
		}
		this.previousRow = Object.assign({}, this.selectedActionRow);
		this.displayDialog = true;
	}

	saveItem() {
		if (!this.isFieldRequiredFilled) {
			this.service.runAction(this.dataInfos.saveLink, this.selectedActionRow).subscribe((res) => {
				if (this.newItem) {
					this.datas.push(res);
					this.newItem = false;
				}
				this.displayDialog = false;
			});
		}
	}
	showGridVersion(version) {
		this.gridVersion = version;
		this.globalSearchCriteria.gridVersion = version;
		let event: LazyLoadEvent;
		this.emitEventFirstLevel(event);
		this.gridVersionChange.emit(version);
		this.hideTableChange.emit(false);
	}
	editRowAction(row) {
		this.isFieldRequiredFilled = false;
		this.editFormEmitter.emit(row);
	}
	cloneRowAction(row) {
		this.isFieldRequiredFilled = false;
		this.cloneFormEmitter.emit(row);
	}
	displayNewItem() {
		this.selectedActionRow = {};
		for (let col of this.dataInfos.cols) {
			if (col.type == "boolean") {
				this.selectedActionRow[col.field] = false;
			} else {
				this.selectedActionRow[col.field] = "";
			}
		}
		this.newItem = true;
		this.displayDialog = true;
		this.isFieldRequiredFilled = true;
	}

	deleteItem(rowIndex: number) {
		let row = this.datas[rowIndex];
		this.service.runAction(this.dataInfos.deleteLink, row).subscribe((res) => {
			if (res) {
				this.datas = this.datas.filter((val, i) => i != rowIndex);
			}
		});
	}

	changeSelectedRow(event, col, colValue) {
		this.selectedActionRow[col] = event.value;
		this.selectedActionRow[colValue] = event.key;
	}

	checkboxChange(event, value) {
		if (!(this.selectedActionRow.hasOwnProperty("gerbable") && value === "activated")) {
			if (event) {
				this.selectedActionRow[value] = "Oui";
			} else {
				this.selectedActionRow[value] = "Non";
			}
		}
	}

	hide() {
		for (let col of this.dataInfos.cols) {
			this.selectedActionRow[col.field] = this.previousRow[col.field];
		}
		this.displayDialog = false;
		this.isCodeExist = false;
	}

	setCardAttributes() {
		if (this.dataInfos.cols) {
			this.cardCols = this.dataInfos.cols.filter((it) => it.isCardCol);
			if (this.cardCols && !this.cardCols.length) this.cardCols = null;
			else {
				// dataInfos.showDetails && goToDetails(row[dataInfos.dataKey])
				this.cardActions = this.dataInfos.lineActions;
			}
		}
	}

	get canShowCardViewButton(): boolean {
		if (!this.dataInfos.cardView) return false;

		if (
			this.dataInfos.cardRenderMode == null ||
			this.dataInfos.cardRenderMode === CardListParams.RenderMode.NORMAL
		) {
			return !!this.cardCols;
		} else {
			return true;
		}
	}

	async changeView($event) {
		this.isViewChange = true;
		this.isLoading = true;
		this.viewType = this.viewType == ViewType.CARD ? ViewType.TABLE : ViewType.CARD;
		if (this.viewType == ViewType.CARD) {
			this.setCardAttributes();
		}
		this.showContent = false;
		await Statique.waitForTimemout(50);
		this.showContent = true;
		this.cd.detectChanges();
		this.isLoading = false;
		this.onResize(null);
	}

	protected convertDataInfosAnonymousToObject() {
		if (this.dataInfos) {
			if (this.dataInfos.cols) {
				let colInternalCols = new Array<GenericTableInfos.Col>();
				this.dataInfos.cols.forEach((c) => {
					colInternalCols.push(new GenericTableInfos.ColInternal(this.translate, c));
				});
				this.dataInfos.cols = colInternalCols;

				let colInternalSelectedCols = new Array<GenericTableInfos.Col>();
				this.dataInfos.selectedCols.forEach((c) => {
					colInternalSelectedCols.push(new GenericTableInfos.ColInternal(this.translate, c));
				});
				this.dataInfos.selectedCols = colInternalSelectedCols;
			}

			if (this.dataInfos.lineActions) {
				let newList = new Array<GenericTableInfos.LineAction>();
				this.dataInfos.lineActions.forEach((c) => {
					newList.push(new GenericTableInfos.LineAction(c));
				});
				this.dataInfos.lineActions = newList;
			}
		}
		if (this.detailsInfos) {
			if (this.detailsInfos.cols) {
				let newList = new Array<GenericTableInfos.Col>();
				this.detailsInfos.cols.forEach((c) => {
					newList.push(new GenericTableInfos.ColInternal(this.translate, c));
				});
				this.detailsInfos.cols = newList;
			}

			if (this.detailsInfos.lineActions) {
				let newList = new Array<GenericTableInfos.LineAction>();
				this.detailsInfos.lineActions.forEach((c) => {
					newList.push(new GenericTableInfos.LineAction(c));
				});
				this.detailsInfos.lineActions = newList;
			}
		}
	}

	dialogOnKeyup(event, col, selectedItemNum) {
		if (col.fieldRequired) {
			if (event.target.value) {
				this.isFieldRequiredFilled = false;
			} else {
				this.isFieldRequiredFilled = true;
			}
		}
		if (col.uniqueValue) {
			let criteria = new SearchCriteriaShipToMatrix();
			criteria.typeUnitCode = event.target.value;
			criteria.ebUnitNum = selectedItemNum;
			this.service.getDatas(col.urlIsUniqueValue, criteria).subscribe(
				(res: boolean) => {
					this.isCodeExist = res;
				},
				(error) => "Error loading data"
			);
		}
	}

	// cette méthode sert a retourner la propriété CSS calc() avec le calcule du pixel passer en parametre
	verticalScrollCalc(scrollHeight) {
		return scrollHeight ? "calc(100vh - " + scrollHeight + "px)" : "";
	}

	showAdvancedSearch($event: MouseEvent) {
		Statique.triggerEvent(this.searchVisible ? "closeAdvancedSearch" : "openAdvancedSearch");
	}

	private initAdvancedSearchListener(remove?: boolean) {
		// listeners
		const openAdvancedSearchListener = () => {
			this.searchVisible = true;
		};
		const closeAdvancedSearchListener = () => {
			this.searchVisible = false;
		};

		if (remove) {
			document.removeEventListener("openAdvancedSearch", openAdvancedSearchListener);
			document.removeEventListener("closeAdvancedSearch", closeAdvancedSearchListener);
			return;
		}

		document.addEventListener("openAdvancedSearch", openAdvancedSearchListener);
		document.addEventListener("closeAdvancedSearch", closeAdvancedSearchListener);
	}

	//cette méthode fait l'appel d'une methode statique sert a recalculer la largeur du tableau pour éviter le double scroll et l'adapter avec l'écran
	onResize($event) {
		let listEltsClass: string[] = [
			".app-header",
			".ui-panel-titlebar",
			".ui-table-summary",
			".ui-table-scrollable-header",
		];
		listEltsClass = listEltsClass.concat(this.listEltsToAdjustHeight);
		this.Statique.onResizeAdapter(listEltsClass, ".ui-table-scrollable-body", 40, null);

		if ($event && $event.element && $event.element.innerText)
			this.updateColumnWidth($event.element.innerText, $event.delta);
	}

	// ce bout de code sert a corriger le probléme d'affichage de la liste déroulante du nombre
	//de lignes a affiché dans une dashboard qui est supperposé sur le dashboard suivant
	//si on a plus qu'un dashboard dans la page
	//(on change le z-index du parent lors du click sur la liste déroulante)
	sharedZIndexIncreament(event) {
		let closetParent = event.target.closest(".p-table-wrapper");
		if (closetParent) {
			closetParent.style.zIndex = this.sharedDropDownZIndex;
		}
		this.sharedDropDownZIndexEmit.emit(this.sharedDropDownZIndex);
	}

	updateColumnWidth(columnName, delta) {
		this.dataInfos.selectedCols.filter((it) => {
			if (it.header == columnName) {
				let colWidth = it.width ? it.width.split("px")[0] : "130";
				let width = String(Number(colWidth) + Number(delta));
				it.width = width + "px";
			}
		});
	}

	async adjustSubTableWidthWithPaginator() {
		await this.Statique.waitUntilElementExists(".subtable-wrapper .ui-table-tbody", true);
		await this.Statique.waitUntilElementExists(".subtable-wrapper .ui-paginator", true);
		this.Statique.checkElementExist(".subtable-wrapper .ui-table-tbody").then((subTable) => {
			this.subtableWidth = subTable.offsetWidth;
		});
		this.Statique.checkElementExist(".subtable-wrapper .ui-paginator").then((paginator) => {
			paginator.style.width = this.subtableWidth + "px";
		});
	}

	deleteSubTableIfExist() {
		let subTable = document.querySelector(".subtable-wrapper");
		if (subTable) subTable.parentNode.removeChild(subTable);
	}

	tasterErrorLoadingData() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.MESSAGE_WHOOOOPS"),
			life: 120000, //Number of time in milliseconds to wait before closing the message.
		});
	}
}
