export abstract class GenericTableDataConverter {
	abstract convert(data: any, model?: any): any;
}
