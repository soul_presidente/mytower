import { GenericTableDataConverter } from "./data-converter-base";

export class ToJsDateDataConverter extends GenericTableDataConverter {
	convert(data: any, model?: any) {
		if (data != null && typeof data !== typeof Date) {
			return new Date(data);
		}
		return data;
	}
}
