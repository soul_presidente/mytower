import { GenericTableService } from "../../../services/generic-table.service";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
	selector: "app-generictable-autocomplete",
	templateUrl: "./autocomplete.component.html",
	styleUrls: ["./autocomplete.component.css"],
})
export class GenericTableAutocompleteComponent implements OnInit {
	@Input()
	selectedId: any;

	selectedItem: any;

	@Input()
	sourceUrl: string;

	@Output()
	onSelectedId = new EventEmitter();

	values: any[] = [];

	filteredValue: any[] = [];

	constructor(private service: GenericTableService) {}

	ngOnInit() {
		this.service.runAction(this.sourceUrl, null).subscribe((res) => {
			this.values = res;
			if (this.selectedId != null) {
				this.selectValue(this.selectedId);
			} else {
				this.selectedItem = null;
			}
		});
	}

	filteredValues(event) {
		this.filteredValue = [];
		for (let i = 0; i < this.values.length; i++) {
			let value = this.values[i];
			if (value.value.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
				this.filteredValue.push(value);
			}
		}
	}

	selectValue(id) {
		for (let i = 0; i < this.values.length; i++) {
			let value = this.values[i];
			if (value.key == id) {
				this.selectedItem = value;
			}
		}
	}

	selectId(event) {
		console.log(event);
		this.onSelectedId.emit(event);
	}
}
