import { EbFlag } from "@app/classes/ebFlag";
import { GenericCell } from "../generic-cell/generic-cell";
import { TranslateService } from "@ngx-translate/core";
import { CardListParams } from "../card-list/card-list.params";
import { MenuItem } from "primeng/api";

export class GenericTableInfos {
	showDetails: any; // Set the line clickable, open new page
	showDeleteBtn: boolean; // Display the delete button
	showAddBtn: boolean; // Display the add button
	showAddNewUserBtn: boolean; // Display the add New User btn on the dashboard header
	showAddNewProfileBtn: boolean; // Display the add New Profile btn on the dashboard header
	showPreviousGrid: boolean; // Display the Previous grid btn on the dashoard header
	showValidGrid: boolean; // Display the Previous grid btn on the dashoard header
	showUpComingVersion: boolean; // Display the Previous grid btn on the dashoard header
	showEditBtn: boolean; // Display the edit button
	showCloneBtn: boolean; //Dispaly the clone button
	showSubTable: boolean; // Enable the + button to display the details table
	enableSubTablePagination: boolean; // Enable pagination into table
	subTableNumberDatasPerPage: number; // Number of datas per page of the subtable
	showHeaderActions: boolean; // Set to false to disable all buttons, without taking care if they are setted to true
	showCollapsibleBtn: boolean; // Hide or Display Content Table
	showColConfigBtn: boolean; // Hide or Display Content Table
	showSaveBtn: boolean; //  Display Save Btn
	showHelpBtn: boolean; //  Display Help Btn
	showExportBtn: boolean; //  Display Export Btn
	showUploadBtn: boolean; //  Display Upload Btn
	showSearchBtn: boolean; //  Display Upload Btn
	showAdvancedSearchBtn: boolean; //  Display Advanced search Btn
	blankBtnCount: number; // Count of empty buttons space to prevent others buttons to be over our buttons (ex: advanced search)

	dataSourceType: GenericTableInfos.DataSourceType; // Method to retrieve data to display
	dataLink: string; // Url to retrieve main data
	dataType: new () => any; // The class to instanciate for each objects (ex: pass the class name like EbDemande)

	saveLink: string;
	deleteLink: string;

	dataDetailsSourceType: GenericTableInfos.DataSourceType; // Method to retrieve data to display in subtable
	dataDetailsLink: string; // Url to retrieve details data
	dataDetailsField: string; // Set the field (of type list) from data to retrieve the detail data (if source type local)

	dataKey: any; // Name of functionnal object identifier (ex ebDemandeNum)
	cols: GenericTableInfos.Col[]; // List of all columns
	selectedCols: any[]; // Columns displayed on the generic table
	paginator: boolean; // Enable pagination
	numberDatasPerPage: number; // Number of data per page
	actionDetailsLink: string; // Link for Datas Details
	showCheckbox: boolean; // Display the checkboc
	showCheckboxGlobal: boolean; // Display the checkbox in table header ! showCheckbox must be true
	colspan: any[]; //TODO a voir ce que c'est et normalement à supprimer
	dataCheckedField: string;
	activateCRUD: boolean;

	contextMenu: boolean;
	contextMenuItems: GenericTableInfos.ContextMenuItem[] = [];

	favorisName: any; // permet de gerer une action checkable specifique (favori par exemple)
	lineActions: Array<GenericTableInfos.LineAction>; // gerer des actions personnalisés pres du checkbox (edit/delete du pricing par exemple) [{class, icon, title, handler(row)}]
	dataIterators: Array<any>; // gerer des actions iteratives (timeremaining par exemple) [{iterator(data), interval}]
	extractWithAdvancedSearch: boolean; // extraction avec advanced search
	dataUpload: any; // import en masse upload data

	showFlags: boolean; // Enable flags column
	dataFlagsField: string; // Field containing Flags DTO
	context: any; // The component containing the generic table

	cardView: boolean; // Card view support
	cardRenderMode: CardListParams.RenderMode; // Rendering mode for the full card content
	cardGenericCellClass: new () => GenericCell<any, any, any>; // Generic to use for card in rendering mode GenericCell
	cardHeight: string; // Card view height, enter value and unit like "200px"

	cellPadding: string; // Global padding for every cell (td) in table body, to use cell by cell, use padding parameter in col param

	ccpTooltipRender: (row: any) => string; // Tooltip rendering of ccp fields

	showCheckboxWidth: String;
	showActionButtonWidth: String; // ???
	actionColumFixed: boolean;

	onFlagChange: (context: any, row: any, listFlag: Array<EbFlag>) => void; // Callback for flag changing method

	constructor(context: any) {
		this.dataLink = null;
		this.dataDetailsLink = null;
		this.saveLink = null;
		this.deleteLink = null;
		this.dataDetailsField = null;
		this.dataKey = null;
		this.cols = [];
		this.selectedCols = [];
		this.paginator = false;
		this.showDetails = false;
		this.showEditBtn = false;
		this.showCloneBtn = false;
		this.showAddBtn = false;
		this.showAddNewUserBtn = false;
		this.showAddNewProfileBtn = false;
		this.showPreviousGrid = false;
		this.showValidGrid = false;
		this.showUpComingVersion = false;
		this.showDeleteBtn = false;
		this.showSubTable = false;
		this.enableSubTablePagination = false;

		this.showHeaderActions = true;
		this.showCollapsibleBtn = false;
		this.showColConfigBtn = false;
		this.showSaveBtn = false;
		this.showHelpBtn = false;
		this.showExportBtn = false;
		this.showUploadBtn = false;
		this.showSearchBtn = false;
		this.showAdvancedSearchBtn = false;
		this.numberDatasPerPage = 20;
		this.subTableNumberDatasPerPage = 100;
		this.showCheckbox = false;
		this.showCheckboxGlobal = true;
		this.colspan = [];
		this.blankBtnCount = 0;

		this.favorisName = null;
		this.lineActions = null;
		this.extractWithAdvancedSearch = null;

		this.showFlags = false;
		this.dataFlagsField = null;
		this.context = context;

		this.cardView = null;
		this.cardRenderMode = CardListParams.RenderMode.NORMAL;

		this.onFlagChange = (context: any, row: any, listFlag: Array<EbFlag>) => {};
		this.ccpTooltipRender = (row: any) => "";

		this.cellPadding = "5px";
		this.showCheckboxWidth = null;
		this.showActionButtonWidth = null;
		this.actionColumFixed = false;

		this.dataSourceType = GenericTableInfos.DataSourceType.Url;
	}

	get blankBtnArray(): number[] {
		if (!this.blankBtnCount) return [];
		return Array(this.blankBtnCount)
			.fill(0)
			.map((x, i) => i);
	}

	// Data commands
	insertDetailsContextItem() {
		if (this.contextMenuItems == null) this.contextMenuItems = [];
		this.contextMenuItems.push({
			label: "GENERAL.ACTION_OPEN_DETAILS",
			labelTranslatable: true,
			icon: "pi pi-chevron-circle-right",
			onClick: (row, event) => {
				window.open(this.actionDetailsLink + row[this.dataKey], "_self");
			},
		});
	}
	insertDetailsNewTabContextItem() {
		if (this.contextMenuItems == null) this.contextMenuItems = [];
		this.contextMenuItems.push({
			label: "GENERAL.ACTION_OPEN_IN_NEW_TAB",
			labelTranslatable: true,
			icon: "pi pi-external-link",
			onClick: (row, event) => {
				window.open(this.actionDetailsLink + row[this.dataKey], "_blank");
			},
		});
	}
	// END Data commands
}

export namespace GenericTableInfos {
	export class Col {
		fieldRequired?: boolean; // Colonne obligatoire lors de la creation
		title?: string; // The value of title infobulle attribute
		field?: string; // The field form the row data model to use
		header?: string; // Non translated header, can be html
		translateCode?: string; // The translate code to use for header text. Replace header field on normal rendering
		renderMode?: Col.RenderModeTable; // Rendering mode for body
		renderModeHead?: Col.RenderModeTable; // Rendering mode for head
		render?: (item: any, renderParams?: any, row?: any, rowParent?: any) => any; // Custom rendering method for col
		headerClass?: string; // CSS class to use for header cell
		fieldClass?: string; // CSS class to use for field cell
		width?: string; // Col width
		minWidth?: string; // Col width
		allowClick?: boolean = true; // Permit click on content
		isCardCol?: boolean; // ???
		cardOrder?: number; // ???
		isCardLabel?: boolean; // ???
		isCardTitle?: boolean; // ???
		genericCellClass?: new () => GenericCell<any, any, any>; // Component class used for rendering GenericCell
		genericCellParams?: GenericCell.Params | ((model: any, context?: any) => GenericCell.Params); // Params passed to component for rendering GenericCell
		genericCellHandler?: (key: string, value: any, model: any, context: any) => void; // rendering GenericCell : Component to GenericTable host
		type?: string; // ???
		renderParam?: any; // Params passed to rendering method
		valueField?: string; // ???
		source?: string; // ???
		url?: string; // ???
		choices?: any; // ???
		blur?: any; // ???
		uniqueValue?: boolean; //unique value in storage
		sortable?: boolean = true; // whether or not activate the sorting behavior for the column (defaults to true)
		urlIsUniqueValue?: string; //url to get response if value already exist or not
		postLoadDataConverter?: (data: any, model?: any) => any; // Manipulate data after load (to have different data on rendering than returned from server)
		public constructor(init?: Partial<Col>) {
			Object.assign(this, init);
		}
	}

	/* Class only used in generic table, the dataInfos.cols and detailsInfos.cols original objects are replaced with this
	 * Include some components pointing to GenericTable, generated properties and methods
	 */
	export class ColInternal extends Col {
		translateService: TranslateService;

		public constructor(translateService: TranslateService, init?: Partial<Col>) {
			super();
			Object.assign(this, init);
			this.translateService = translateService;
		}

		get colSettingsHeader(): string {
			if (this.translateCode) {
				return this.translateService.instant(this.translateCode);
			} else {
				return this.header;
			}
		}
	}

	export namespace Col {
		export enum RenderModeTable {
			NORMAL = "NORMAL",
			HTML = "HTML",
			GenericCell = "GenericCell",
		}
	}

	export class LineAction {
		class?: string;
		icon?: string;
		title?: string;
		conditionalTitle?: (row?: any) => string;
		onClick?: (row?: any, event?: any) => void;
		showCondition?: (row: any) => boolean;
		enableCondition?: (row: any) => boolean; // Button enabled condition method. Return true to enable. By default button is enabled,

		public constructor(init?: Partial<LineAction>) {
			Object.assign(this, init);
		}
	}

	export class TableConfig {
		size: number;
		pageNumber: number;
		orderedColumn: string;
		sortOrder: string;
		selectedCols: any;
		cols: any;
		viewType: number;
	}

	export class ConfigData {
		datatableCompId: number;
		ebUserNum: number;
		state: string;
	}

	export interface ContextMenuItem extends MenuItem {
		onClick: (model: any, event?: any) => any;
		labelTranslatable?: boolean; // Indicate that le label is a i18n key
		labelTranslated?: boolean; // Used internally in generic table
	}

	export enum DataSourceType {
		Url,
		Local,
	}
}
