import { Component, OnInit, Input } from "@angular/core";
import { EbNotification } from "@app/classes/EbNotification";
import { NotificationService } from "@app/services/notification.service";
import { NotificationType, Modules } from "@app/utils/enumeration";
import { Router } from "@angular/router";
import { Statique } from "@app/utils/statique";

@Component({
	selector: "app-notification-item",
	templateUrl: "./notification-item.component.html",
	styleUrls: ["./notification-item.component.scss"],
})
export class NotificationItemComponent implements OnInit {
	@Input()
	notif: EbNotification;

	@Input()
	index: number;

	@Input()
	listAlert: Array<EbNotification>;

	@Input()
	listMessages: Array<EbNotification>;

	@Input()
	listNotification: Array<EbNotification>;

	Statique = Statique;

	NotificationType = NotificationType;

	constructor(protected notificationService: NotificationService, private router: Router) {}

	ngOnInit() {}

	deleteNotif(notif: EbNotification, index: number) {
		this.notificationService.deleteNotification(notif.ebNotificationNum).subscribe(
			function(res) {
				if (notif.type == NotificationType.NOTIFY) {
					this.listNotification.splice(index, 1);
				} else if (notif.type == NotificationType.ALERT) {
					this.listAlert.splice(index, 1);
				} else if (notif.type == NotificationType.MSG) {
					this.listMessages.splice(index, 1);
				}
			}.bind(this)
		);
	}

	redirect(notif: EbNotification) {
		switch(notif.module) { 
			case Modules.FREIGHT_AUDIT: {
				this.router.navigate(["/app/freight-audit" , { ebDemandeNum: notif.numEntity }]);
			   break; 
			}
			default: { 
				if (notif.module) {
					if (notif.type == NotificationType.NOTIFY) {
						this.router.navigate([
							"app/" +
								Statique.moduleNamePath[Statique.getModuleNameByModuleNum(notif.module)] +
								"/dashboard/" +
								notif.numEntity,
						]);
					} else {
						this.router.navigate([
							"app/" + Statique.getComponentPathByModuleId(notif.module) + "/" + notif.numEntity,
						]);
					}
				}
			   break; 
			} 
		} 
	}

	isAlertOrNotif(notif) {
		if (notif.type == NotificationType.ALERT) {
			return true;
		} else if (notif.type == NotificationType.NOTIFY) {
			return true;
		} else return false;
	}
}
