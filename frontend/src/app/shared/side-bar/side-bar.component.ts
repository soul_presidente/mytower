import { ChangeDetectorRef, Component, ElementRef, Input, OnInit } from "@angular/core";
import { AccessRights, Modules, UserRole } from "@app/utils/enumeration";
import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";
import { ConnectedUserComponent } from "../connectedUserComponent";
import { Observable, Subscription } from "rxjs";
import { Statique } from "@app/utils/statique";
import { EbDemande } from "@app/classes/demande";

@Component({
	selector: "app-side-bar",
	templateUrl: "./side-bar.component.html",
	styleUrls: ["./side-bar.component.scss"],
})
export class SideBarComponent extends ConnectedUserComponent implements OnInit {
	isLoggedInScalar: boolean;
	Modules = Modules;
	UserRole = UserRole;
	isLoggedIn: Observable<boolean>;
	@Input()
	module: number = null;
	url: string = "";
	private subscription: Subscription;
	@Input()
	show: boolean;
	showModelNote: boolean = false;
	openedSidebar: boolean = false;
	ebDemande: EbDemande = null;
	savedSearchClickedEvent: boolean = false;
	constructor(
		myElement: ElementRef,
		protected authenticationService: AuthenticationService,
		private router: Router,
		protected cdr: ChangeDetectorRef
	) {
		super();
	}
	ngAfterViewInit(): void {
		this.closeTab();
	}
	ngOnInit() {
		this.isLoggedInScalar = AuthenticationService.isAuthenticated();
		this.authenticationService.connectedUser.subscribe((tocken) => {
			this.isLoggedInScalar = tocken != null;
		});

		this.subscription = this.authenticationService.sideBarInitUserObservale.subscribe((res) =>
			this.checkChange()
		);

		document.addEventListener("click", this.quicknavItemClickHandler);

		document.addEventListener(
			"toggle-model-note",
			function(e) {
				e.preventDefault();
				e.stopPropagation();
				this.toggleModelNote();
			}.bind(this),
			false
		);
	}

	customMenuTogle() {
		if (this.openedSidebar) {
			let sidebar = document.getElementsByClassName("app-sidebar")[0];
			let myElement = document.getElementById("sidebar_left");
			let body = document.getElementsByClassName("app-wrapper")[0];
			let btnAction = document.getElementsByClassName("btn-action-bg")[0];
			let navbarfixedtop = document.getElementsByClassName("navbar-fixed-top")[0];

			if (btnAction != undefined) {
				btnAction.classList.add("btn-action-opened");
			}
			body.classList.remove("opened");
			myElement.classList.remove("closed");
			navbarfixedtop.classList.remove("opened");
			sidebar.classList.remove("app-sidebar-expanded");
		}
	}

	menuClosedToHeader(showHeaderBtn) {
		let event = new CustomEvent("menu-toggle", { detail: showHeaderBtn });
		document.dispatchEvent(event);
	}

	menuToggle(event) {
		let myElement = document.getElementById("sidebar_left");
		//let body = document.getElementById("body_content")
		let body = document.getElementsByClassName("app-wrapper")[0];
		// Bare des buttons action
		let header = document.getElementsByClassName("navbar-fixed-top")[0];
		let btnAction = document.getElementsByClassName("btn-action-bg")[0];
		let sidebar = document.getElementsByClassName("app-sidebar")[0];

		if (btnAction != undefined) {
			if (btnAction.classList.contains("btn-action-opened")) {
				btnAction.classList.remove("btn-action-opened");
			} else {
				btnAction.classList.add("btn-action-opened");
			}
		}

		if (myElement.classList.contains("closed")) {
			body.classList.remove("opened");
			header.classList.remove("opened");
			myElement.classList.remove("closed");
			sidebar.classList.add("app-sidebar-expanded");

			let quicknav = document.getElementById("quicknav");
			let els = quicknav.getElementsByClassName("active");
			if (els && els.length > 1) {
				els[1].classList.remove("active");
				els[0].classList.remove("active");
			}
			let it;
			(it = quicknav.getElementsByTagName("ul")[0].children[0].children[0])
				? it.classList.add("active")
				: null;
			(it = quicknav.getElementsByClassName("tab-content")[0].children[0])
				? it.classList.add("active")
				: null;
		} else {
			body.classList.add("opened");
			header.classList.add("opened");
			myElement.classList.add("closed");
			sidebar.classList.remove("app-sidebar-expanded");

			let quicknav = document.getElementById("quicknav");
			let els = quicknav.getElementsByClassName("active");
			if (els && els.length > 1) {
				els[1].classList.remove("active");
				els[0].classList.remove("active");
			}
		}
		// remove active class after closing sidebar
		let it;
		it = document
			.getElementById("quicknav")
			.getElementsByTagName("li")[0]
			.getElementsByClassName("active")[0];
		if (it != undefined) {
			if (it.classList.contains("active")) {
				it.classList.remove("active");
			}
		}
		if (this.openedSidebar == true) {
			this.closeTab();
		}
		this.openedSidebar = !this.openedSidebar;
		this.menuClosedToHeader(this.openedSidebar);
	}

	async quicknavItemClickHandler(evt) {
		let $el = evt.target;
		let it;

		try {
			let sideBarLeftEl = await Statique.waitUntilElementExists("#sidebar_left");
			let quicknav = await Statique.waitUntilElementExists("#quicknav");

			if (
				document.getElementById("sidebar_left").classList.contains("closed") &&
				!$el.classList.contains("nav-item") &&
				!$el.classList.contains("nav-link") &&
				!$el.classList.contains("fa")
			) {
				(it = document
					.getElementById("quicknav")
					.getElementsByTagName("ul")[0]
					.getElementsByClassName("active")[0])
					? it.classList.remove("active")
					: null;
				(it = document
					.getElementById("quicknav")
					.getElementsByClassName("tab-content")[0]
					.getElementsByClassName("active")[0])
					? it.classList.remove("active")
					: null;
			}
		} catch (e) {}
	}

	closeTab() {
		let it;
		if (
			document.getElementById("sidebar_left") != null &&
			document.getElementById("sidebar_left").classList.contains("closed")
		) {
			it = document
				.getElementById("quicknav")
				.getElementsByTagName("li")[0]
				.getElementsByClassName("active")[0];
			if (it != undefined) {
				if (it.classList.contains("active")) {
					it.classList.remove("active");
				}
			}

			it = document
				.getElementById("quicknav")
				.getElementsByClassName("tab-content")[0]
				.getElementsByClassName("active")[0];
			if (it != undefined) {
				if (it.classList.contains("active")) {
					it.classList.remove("active");
				}
			}
		}
	}

	toggleTab(id) {
		if (id && id == "tab-demandeprefere") {
			this.savedSearchClickedEvent = !this.savedSearchClickedEvent;
		}
		let clickedTab;
		let sidebarClosed;
		sidebarClosed = document.getElementById("sidebar_left").classList.contains("closed");
		if (sidebarClosed) {
			clickedTab = document.getElementById(id);
			if (clickedTab) {
				if (clickedTab.classList.contains("active")) {
					clickedTab.classList.remove("active");
				} else {
					clickedTab.classList.add("active");
				}
			}
			clickedTab = document.getElementById(id + "-panel");
			if (clickedTab) {
				if (clickedTab.classList.contains("active")) {
					clickedTab.classList.remove("active");
				} else {
					clickedTab.classList.add("active");
				}
			}
		}
	}

	toggleActive(el) {
		el.classList.toggle("open");
	}

	makeActive(path) {
		if (path == "accueil") return this.router.url.indexOf("/app") < 0 ? "active" : "";
		return this.router.url.indexOf(path) >= 0 ? "active" : "";
	}

	isDocumentsSubmenuOpen() {
		return this.router.url.indexOf("/compliance-matrix") >= 0 ? "open" : "";
	}

	isContribution(ecModule: number) {
		let acc = this.userConnected.hasContribution(ecModule);
		return acc;
	}
	hasAccesUrl(path: string) {
		if (this.router.url.indexOf(path) >= 0) return true;
		return false;
	}
	hasAccess(ecModuleNum: number) {
		return this.userConnected.hasAccess(ecModuleNum);
	}

	getDashboardPath(module: number) {
		return (
			"/app/" + Statique.moduleNamePath[Statique.getModuleNameByModuleNum(module)] + "/dashboard"
		);
	}

	// toggle select icons
	onSelectOpen() {
		var open = false;
		// just a function to print out message
		function isOpen() {
			if (open) return "menu is open";
			else return "menu is closed";
		}
		// on each click toggle the "open" variable
		$("select").on("click", function() {
			open = !open;
			console.log(isOpen());
		});
		// on each blur toggle the "open" variable
		// fire only if menu is already in "open" state
		$("select").on("blur", function() {
			if (open) {
				open = !open;
				console.log(isOpen());
			}
		});
		// on ESC key toggle the "open" variable only if menu is in "open" state
		$(document).keyup(function(e) {
			if (e.keyCode == 27) {
				if (open) {
					open = !open;
					console.log(isOpen());
				}
			}
		});
	}
	toggleModelNote() {
		this.showModelNote = !this.showModelNote;
	}
	adjustHeight() {
		let ulHeight = document.querySelector(".sidebar-menu")[0].offsetHeight;
		let leftSideHeight = document.querySelector("#sidebar_left")[0].offsetHeight;
		let savedSearchBloc = <HTMLElement>document.getElementsByClassName("savedsearch-list")[0];
		// let eltToGetHeight = document.getElementsByClassName("")[0];
		let heightToGet = Math.abs(ulHeight - leftSideHeight) - 74;
		savedSearchBloc.style.height = heightToGet.toString();
	}
	ngOnDestroy() {
		document.removeEventListener("click", this.quicknavItemClickHandler);
	}
}
