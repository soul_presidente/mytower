import {
	ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	OnInit,
	ViewChild,
	OnDestroy,
} from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbUser } from "@app/classes/user";
import { UserService } from "@app/services/user.service";
import { Router } from "@angular/router";
import { Statique } from "@app/utils/statique";
import { AuthenticationService } from "@app/services/authentication.service";
import { switchMap } from "rxjs/operators";
import {
	AccessRights,
	Modules,
	ServiceType,
	TypeCustomsBroker,
	UserRole,
	NotificationType,
} from "@app/utils/enumeration";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbEtablissement } from "@app/classes/etablissement";
import { EbCompagnie } from "@app/classes/compagnie";
import { EtablissementService } from "@app/services/etablissement.service";
import { EbDemande } from "@app/classes/demande";
import { NgbTabset } from "@ng-bootstrap/ng-bootstrap";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos, BreadcumbComponent } from "./header-infos";
import { BreadcrumbComponent } from "../breadcrumb/breadcrumb.component";
import { ExportService } from "@app/services/export.service";
import { NotificationService } from "@app/services/notification.service";
import { EbNotification } from "@app/classes/EbNotification";
import { ChatService } from "@app/services/chat.service";
import { EbChat } from "@app/classes/chat";
import { WebSocketService } from "@app/services/web-socket.service";
import * as Stomp from "stompjs";
import { NotificationTypeCount } from "@app/classes/notification-type-count";

@Component({
	selector: "app-header",
	templateUrl: "./header.component.html",
	styleUrls: ["./header.component.scss"],
	host: {
		"(document:click)":
			"onCloseOptions($event); onCloseNewOptions($event); onCloseModulesWrapper($event); onCloseNotifsWrapper($event)",
	},
})
export class HeaderComponent extends ConnectedUserComponent implements OnInit, OnDestroy {
	public _newUser: EbUser = null;
	ebEtablissement: EbEtablissement = new EbEtablissement();
	ebCompagny: EbCompagnie = new EbCompagnie();

	Modules = Modules;
	Statique = Statique;
	UserRole = UserRole;
	//isLoggedIn: Subject;
	isLoggedInScalar: boolean = false;
	@Input()
	module: number = null;

	typeaheadUserSelectEvent: EventEmitter<string> = new EventEmitter<string>();
	listUser: any = new Array<EbUser>();
	userSelectCt: EbUser;
	showCommands: boolean = false;
	showNewBtns: boolean = false;
	showModulesWrapper: boolean = false;
	showNotifsWrapper: boolean = false;
	showNav: boolean = true;
	ebDemande: EbDemande = null;
	showSearchBar: boolean = false;

	refTransport = "Ref transport";
	ebDemandeNum: number;
	CustomerReference: string;

	@ViewChild("tabset", { static: false })
	ngbTabSet: NgbTabset;

	@ViewChild(BreadcrumbComponent, { static: true })
	breadcrumb: BreadcrumbComponent;

	actionButtons: Array<HeaderInfos.ActionButton> = [];
	breadcrumbsItemsConfig: Array<HeaderInfos.BreadCrumbItem> = [];

	listNotification: Array<EbNotification> = null;
	listAlert: Array<EbNotification> = null;
	listMessages: Array<EbNotification> = null;

	stompClient: any;
	logOutTimer: any = null;
	selectedTab: Number = 0;

	nbrAlerts: number = 0;
	nbrNotifs: number = 0;
	nbrMsgs: number = 0;
	totalNotifsNbr: number = 0;
	listChatNotif: Array<EbChat> = null;

	constructor(
		protected authenticationService: AuthenticationService,
		protected headerService: HeaderService,
		protected exportService: ExportService,
		private router: Router,
		private userService: UserService,
		protected notificationService?: NotificationService,
		protected etablissementService?: EtablissementService,
		protected cd?: ChangeDetectorRef,
		protected chatService?: ChatService,
		protected wsocket?: WebSocketService
	) {
		super(authenticationService);
	}
	goTo(url) {
		this.router
			.navigateByUrl("/", { skipLocationChange: true })
			.then(() => this.router.navigate([url]));
	}
	ngOnInit() {
		document.addEventListener("get-info-demande", this.handlerInfoDemandeAction.bind(this), false);
		//this.isLoggedIn = this.authenticationService.itemValue;
		let self = this;
		self.isLoggedInScalar = AuthenticationService.isAuthenticated();

		self.authenticationService.connectedUser.subscribe((tocken) => {
			self.isLoggedInScalar = tocken != null;
			self.userConnected = AuthenticationService.decodeTocken(tocken);
		});

		if (self.userConnected && !self.userConnected.listAccessRights) {
			let criteria: SearchCriteria = new SearchCriteria();
			criteria.withAccessRights = true;
			criteria.ebUserNum = self.userConnected.ebUserNum;
			self.userService.getUserInfo(criteria).subscribe((res) => {
				self.userConnected.listAccessRights = res.listAccessRights;
			});
		}

		this.initAutocomplete();

		this.headerService.events.actionButtonsChange.subscribe((actionButtons) => {
			this.actionButtons = actionButtons;
		});
		this.headerService.events.breadCrumbsChange.subscribe((breadcrumbsItemsConfig) => {
			this.breadcrumbsItemsConfig = breadcrumbsItemsConfig.get(BreadcumbComponent.HEADER);
		});
		this.checkIfExportIsRunning();
		this.loadNotifications();
		this.countNotifications();

		if (Statique.getInnerObjectStr(this, "userConnected.ebUserNum")) this.connectAndReconnect();

	}
	private connectAndReconnect() {
		var ws = Statique.getWebSocket(this.userConnected.ebUserNum);
		const sockJsProtocols = ["xhr-streaming", "xhr-polling"];
		this.stompClient = Stomp.over(ws);

		this.stompClient.debug = false;
		let that = this;
		this.stompClient.connect(
			{},
			(frame) => {
				that.wsocket.setStompClient(that.stompClient);
				that.wsocket.setSessionId(Statique.getWsSessionId());
				// add stomp subscriber for each different topic
				that.stompClient.subscribe(
					"/topics" + Statique.controllerWebSocketPricing,
					(message: Response) => {
						that.wsocket.notifySubscribers(
							Statique.controllerWebSocketPricing,
							JSON.parse(new String(message.body).toString())
						);
					}
				);

				that.stompClient.subscribe(
					"/topics/notifications/update",
					(message: Response) => {
						if (String(message.body).toString()=="reloadNotifications" && message.headers["user"] == this.userConnected.ebUserNum.toString())
						this.loadNotifications() ;
						this.countNotifications();
					}
				);


			},
			() => {
				setTimeout(() => {
					this.connectAndReconnect();
				}, 60000);
			}
		);
	}

	checkIfExportIsRunning() {
		if (localStorage.getItem("exportCode")) {
			this.exportService.repeatChecking(localStorage.getItem("exportCode"));
		}
	}

	ngOnDestroy() {
		document.removeEventListener("get-info-demandet", this.handlerInfoDemandeAction.bind(this));
		if (this.stompClient) {
			this.stompClient.disconnect();
			this.wsocket.setStompClient(null);
		}
	}

	gottoLink(link) {
		this.router.navigate([link]);
	}

	isContribution(ecModule: number) {
		return this.userConnected.hasContribution(ecModule);
	}

	hasAccesUrl(path: string) {
		if (this.router.url.indexOf(path) >= 0) return true;
		return false;
	}

	emitClickPopUp() {
		let event = new CustomEvent("btnPopUp", null);
		document.dispatchEvent(event);
	}

	canShowRequestCustomBrokerButton() {
		if (!(this.ebDemande = Statique.getCurrentEbDemande())) return false;

		return (
			this.isContribution(Modules.TRANSPORT_MANAGEMENT) &&
			window.location.href.indexOf("/details/") >= 0 &&
			(this.userConnected.service != ServiceType.BROKER &&
				this.module == Modules.TRANSPORT_MANAGEMENT &&
				(!this.ebDemande.xEcTypeCustomBroker ||
					this.ebDemande.xEcTypeCustomBroker == TypeCustomsBroker.NOT_APPLICABLE)) &&
			!this.ebDemande.flagRequestCustomsBroker
		);
	}
	emitClickRequestCustomBroker($event) {
		let event = new CustomEvent("request-customsbroker-action", { detail: $event });
		document.dispatchEvent(event);
	}

	initAutocomplete() {
		this.typeaheadUserSelectEvent
			// .distinctUntilChanged()
			// .debounceTime(200)
			.pipe(
				switchMap((term) => {
					let criteria = new SearchCriteria();
					criteria.searchterm = term;
					criteria.size = null;
					criteria.withPassword = true;
					criteria.withActiveUsers = true;
					return this.userService.getListUser(criteria);
				})
			)
			.subscribe(
				(data) => {
					this.cd.markForCheck();
					this.listUser = data;
				},
				(err) => {
					this.listUser = [];
				}
			);
	}

	async logout() {
		await this.preLogout();
	    await Statique.waitForTimemout(1000);
		this.authenticationService.logout();
		//this.authenticationService.isLoggedInObservable.n
		this.checkChange();

		let appCorp = document.getElementById("corp");
		if (appCorp.classList.contains("opened")) {
			appCorp.classList.remove("opened");
		}

		// this.router.navigate(['/login']);
		window.location.href = window.location.origin + "/login";
	}

	async preLogout(): Promise<boolean> {
		if (!this.ebDemande || !this.ebDemande.ebDemandeNum) {
			this.ebDemande = Statique.getCurrentEbDemande();
		}

		if (this.ebDemande && this.ebDemande.ebDemandeNum) {
			await this.wsocket.sendPricingMessage(this.ebDemande.ebDemandeNum);
		}
		return true;
	}

	loginAsUser() {
		if (!this.userSelectCt) return;

		localStorage.removeItem("access_rights");

		Statique.setTokenForCt();

		this.authenticationService.login(this.userSelectCt.email, null, true).subscribe(
			(result) => {
				if (result) {
					// redirect to home page
					this.authenticationService.notifySideBarInitUser();
					// this.router.navigate(['home']);
					window.location.href = window.location.origin + "/accueil";
				} else {
					console.log("Email ou mot de passe incorrect");
				}
			},
			(error) => {
				console.log("Email ou mot de passe incorrect");
			}
		);
	}

	loginBackAsCt() {
		if (!this.userConnected.realUserNum) return;

		localStorage.removeItem("access_rights");

		this.authenticationService.login(this.userConnected.realUserEmail, null, true).subscribe(
			(result) => {
				if (result) {
					Statique.deleteTokenForCt();

					// redirect to home page
					this.authenticationService.notifySideBarInitUser();
					// this.router.navigate(['home']);
					window.location.href = window.location.origin + "/accueil";
				} else {
					console.log("Email ou mot de passe incorrect");
				}
			},
			(error) => {
				console.log("Email ou mot de passe incorrect");
			}
		);
	}

	makeActive(path) {
		return this.router.url.indexOf(path) >= 0 ? "active" : "";
	}
	// toggle order managment options
	toggleOptions() {
		this.showCommands = !this.showCommands;
	}
	onCloseOptions(event) {
		let it = document.getElementById("command-options__btn");
		if (it && !it.contains(event.target)) this.showCommands = false;
	}
	// toggle new options
	toggleNewOptions() {
		this.showNewBtns = !this.showNewBtns;
	}
	onCloseNewOptions(event) {
		let it = document.getElementById("navbar-new_btn");
		if (it && !it.contains(event.target)) this.showNewBtns = false;
	}
	toggleSearchBar() {
		this.showSearchBar = !this.showSearchBar;
	}
	toggleModulesWrapper() {
		this.showModulesWrapper = !this.showModulesWrapper;
	}
	onCloseModulesWrapper(event) {
		let it = document.getElementById("modules-wrapper__btn");
		if (it && !it.contains(event.target)) this.showModulesWrapper = false;
	}
	// toggle notifications wrapper
	toggleshowNotifsWrapper() {
		if (this.nbrAlerts > 0) {
			this.resetNotifNumber(NotificationType.ALERT);
		}
		this.showNotifsWrapper = !this.showNotifsWrapper;
		this.nbrAlerts = 0;
		this.totalNotifsNbr = this.nbrAlerts + this.nbrMsgs + this.nbrNotifs;
	}

	async resetNotifNumber(notifType: number) {
		this.notificationService
			.updateNotifNbrForUser(this.userConnected.ebUserNum, notifType, 0)
			.subscribe((res) => {});
	}

	onCloseNotifsWrapper(event) {
		let it = document.getElementById("navbar-notification");
		if (it && !it.contains(event.target)) this.showNotifsWrapper = false;
	}
	activeBtn(event) {
		let el = event.target;
		console.log(el);
		if (el.classList.contains("active")) {
			el.classList.remove("active");
		} else {
			el.classList.add("active");
		}
	}
	isTracingDetails() {
		if (window.location.href.indexOf("track-trace/details/") >= 0) {
			return true;
		}
	}
	private handlerInfoDemandeAction(event) {
		event.preventDefault();
		event.stopPropagation();
		this.ebDemandeNum = event.detail.ebDemandeNum;
		this.refTransport = event.detail.refTranspot;
		this.CustomerReference =
			event.detail.CustomerReference != null
				? event.detail.CustomerReference
				: "Customer Reference";
	}

	/**
	 * Check if the action bar should be displayed
	 * Based on ActionButtons count and breadcrumb items
	 */
	get showActionBar(): boolean {
		return (
			(this.actionButtons != null && this.actionButtons.length > 0) ||
			(this.breadcrumb != null && this.breadcrumb.hasItems)
		);
	}

	/* Rendering Methods for Action Buttons */
	renderlabelEditabel(actionButton: HeaderInfos.ActionButton): string {
		return actionButton.editLabel != null ? actionButton.editLabel() : "";
	}
	renderActionIsDisplayed(actionButton: HeaderInfos.ActionButton): boolean {
		return actionButton.displayCondition != null ? actionButton.displayCondition() : true;
	}
	renderActionIsEnabled(actionButton: HeaderInfos.ActionButton): boolean {
		return actionButton.enableCondition != null ? actionButton.enableCondition() : true;
	}
	renderActionIsLoading(actionButton: HeaderInfos.ActionButton): boolean {
		return actionButton.loadingCondition != null ? actionButton.loadingCondition() : false;
	}
	renderActionNonTranslatedHint(actionButton: HeaderInfos.ActionButton): string {
		if (!this.renderActionIsEnabled(actionButton) && actionButton.disabledHint != null) {
			return actionButton.disabledHint;
		} else if (this.renderActionIsEnabled(actionButton) && actionButton.enabledHint != null) {
			return actionButton.enabledHint;
		} else {
			return actionButton.hint ? actionButton.hint : "";
		}
	}

	loadNotifications() {
		const pageNumber = 1;
		this.notificationService.getListNotification(this.userConnected.ebUserNum, NotificationType.ALERT, pageNumber)
		.subscribe((data: Array<EbNotification>) => {
			this.listAlert = data;
		});
		this.notificationService.getListNotification(this.userConnected.ebUserNum, NotificationType.MSG, pageNumber)
		.subscribe((data: Array<EbNotification>) => {
			this.listMessages = data;
		});
		this.notificationService.getListNotification(this.userConnected.ebUserNum, NotificationType.NOTIFY, pageNumber)
		.subscribe((data: Array<EbNotification>) => {
			this.listNotification = data;
		});
	}

	countNotifications(){
		this.notificationService.count(this.userConnected.ebUserNum).subscribe((data: NotificationTypeCount) => {
			this.nbrAlerts = data.nbrAlerts;
			this.nbrNotifs = data.nbrNotifys;
			this.nbrMsgs = data.nbrMsgs;

			this.totalNotifsNbr = this.nbrAlerts + this.nbrMsgs + this.nbrNotifs;
		});
	}

	updateTotalNotifNbr($event) {
		this.totalNotifsNbr = $event;
	}

	getUserByToken() {
		this.userConnected = AuthenticationService.getConnectedUser();
		return this.userConnected.nomPrenom;
	}
}
