export class HeaderInfos {
	actions?: Array<HeaderInfos.ActionButton>;
	breadCrumbs?: Map<BreadcumbComponent, Array<HeaderInfos.BreadCrumbItem>>;
}
export enum BreadcumbComponent {
	HEADER = 1, //default value
	MY_COMPANY = 2,
}
export namespace HeaderInfos {
	export class ActionButton {
		label?: string; // Translatable label
		icon?: string; // The icon to display in the button (css class)
		action: (jsEvent?: any) => any; // Action handler on click
		displayCondition?: () => boolean; // Button displayed condition method. Return true to display. By default button is visible
		enableCondition?: () => boolean; // Button enabled condition method. Return true to enable. By default button is enabled,
		loadingCondition?: () => boolean; //Button loading condition method. Return true to loading. By default button is loading.
		hint?: string; // Translatable hint displayed every time
		disabledHint?: string; // Translatable hint informations displayed on disabled state (override hint property)
		enabledHint?: string; // Translatable hint informations displayed on enabled state (override hint property)
		btnClass?: string; // class css for button parent
		editLabel?: () => string; //Translatable label.
	}

	export class BreadCrumbItem {
		icon?: string; // Icon to be dispalyed in the breadscumb
		label: string; // Translatable text to be displayed in the breadcrumb
		path?: string; // Tull path to route to when breakcrumb is clicked
	}
}
