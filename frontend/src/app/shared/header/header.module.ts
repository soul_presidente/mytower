import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { HeaderComponent } from "./header.component";
import { SharedModule } from "@app/shared/module/shared.module";

@NgModule({
	imports: [CommonModule, RouterModule, FormsModule, SharedModule],
	declarations: [HeaderComponent],
	exports: [HeaderComponent],
})
export class HeaderModule {}
