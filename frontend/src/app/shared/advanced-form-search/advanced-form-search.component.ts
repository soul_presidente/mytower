import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	Output,
	QueryList,
	SimpleChanges,
	ViewChildren,
} from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { SavedForm } from "@app/classes/savedForm";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { DialogService } from "@app/services/dialog.service";
import { SavedFormIdentifier, SavedSearchAction, Modules } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { SearchField } from "@app/classes/searchField";
import { ConnectedUserComponent } from "../connectedUserComponent";
import { OldAutocompleteComponent } from "../old-autocomplete/old-autocomplete.component";
import { ActivatedRoute, Router } from "@angular/router";
import { DatetimePickerComponent } from "../datetime-picker/datetime-picker.component";
import { ModalService } from "../modal/modal.service";
import { Subscription } from "rxjs";
import { ExportService } from "@app/services/export.service";
import { StatiqueService } from "@app/services/statique.service";
import { ParamsMail } from "@app/classes/paramsMail";
import { FlagService } from "@app/services/flag.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbFlag } from "@app/classes/ebFlag";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { TranslateService } from "@ngx-translate/core";
import { FlagMultiSelectComponent } from "../flag/flag-multi-select/flag-multi-select.component";

@Component({
	selector: "app-advanced-form-search",
	templateUrl: "./advanced-form-search.component.html",
	styleUrls: ["./advanced-form-search.component.css"],
})
export class AdvancedFormSearchComponent extends ConnectedUserComponent
	implements OnInit, OnDestroy, OnChanges {
	Statique = Statique;
	showPopupAddRule: boolean = false;

	protected checkDirty: boolean;
	protected subscribedSelectedSf: Subscription;
	protected selectedSavedForm: SavedForm;

	public form: FormGroup;
	public listFields: Array<SearchField>;
	public listSelectedFields: Array<SearchField>;

	private applySearch: boolean = false;
	private viewInit: boolean = false;

	private SavedSearchAction = SavedSearchAction;

	@Input()
	column?: string; // undefined | all | json
	@Input()
	initFields?: string; // undefined | all | json
	@Input()
	fieldsJsonFile: string;
	@Input()
	ebModuleNum: number; // numéro du module
	@Input()
	ebCompNum: number; // numéro du component
	@Input()
	editable?: boolean; // possibilité d'ajouter/supprimer les champs
	@Input()
	fieldsOnly?: boolean; // possibilité de définir les opérations (contains, equals, etc.)
	//@Input() paramFields: Array<SearchField>;
	@Input()
	initialListFields: any;
	@Input()
	paramFields: any;
	@Input()
	initialListCategorie: any;
	@Output()
	searchHandler = new EventEmitter<any>();

	@ViewChildren(OldAutocompleteComponent)
	autocompChildren: QueryList<OldAutocompleteComponent>;
	@ViewChildren(DatetimePickerComponent)
	datetimePickerChildren: QueryList<DatetimePickerComponent>;
	@ViewChildren(FlagMultiSelectComponent)
	flagMultiSelectChildren: QueryList<FlagMultiSelectComponent>;

	subscription: Subscription;

	ruleSelectedAction: number;
	ruleSelectedActionTrigger: Array<number>;
	ruleSelectedTarget: string;
	ruleSelectedParams: string;
	ruleSelectedName: string;
	isSavingRule: boolean;

	listMails: Array<ParamsMail> = new Array<ParamsMail>();
	listStatusModule: Array<any> = new Array<any>();
	listSavedSearchAction: Array<any> = new Array<any>();
	listSavedSearchActionTrigger: Array<any> = new Array<any>();

	listIcons: Array<EbFlag>;
	listIconsCode: Array<number> = new Array<number>();

	StatusPopUp: any;
	SavedFormIdentifier = SavedFormIdentifier;

	savedForm: SavedForm = null;

	criteriaTypeRequest: SearchCriteria;

	constructor(
		protected fb: FormBuilder,
		protected savedFormsService: SavedFormsService,
		protected dialogService: DialogService,
		public fieldSelector: ElementRef,
		public router: Router,
		protected activatedRoute: ActivatedRoute,
		private modalService: ModalService,
		private exportService?: ExportService,
		protected statiqueService?: StatiqueService,
		private flagService?: FlagService,
		protected translate?: TranslateService
	) {
		super();

		this.checkDirty = false;
		this.listSelectedFields = new Array<SearchField>();
		this.listFields = new Array<SearchField>();

		this.subscription = this.exportService.getMessage().subscribe(async (data: any) => {
			let config: any[] = data ? data.data : null;
			let cb: Function = data ? data.cb : null;
			let searchterm: string = data ? data.searchterm : "";
			if (config == null) return;
			let searchInput: any = this.getSearchInput();
			searchInput.searchterm = searchterm;

			try {
				await this.exportService.startExportCsv(
					this.ebModuleNum,
					false,
					searchInput,
					this.userConnected,
					config
				);
				if (cb) cb();
			} catch (e) {
				if (cb) cb();
			}
		});
	}

	initListFlag() {
		this.flagService.getListFlag(this.ebModuleNum).subscribe(
			function(data) {
				this.listIcons = data;
			}.bind(this)
		);
	}

	ngOnInit() {
		this.initListFlag();
		if (typeof this.fieldsJsonFile == "undefined" && typeof this.initialListFields == "undefined")
			throw new Error("No json file for fields provided");

		if (typeof this.editable == "undefined") this.editable = true;
		if (typeof this.fieldsOnly == "undefined") this.fieldsOnly = true;
		if (typeof this.initialListFields == "undefined") {
			this.initialListFields = require("../../../assets/ressources/jsonfiles/" +
				this.fieldsJsonFile +
				".json");
		}

		this.initialListFields &&
			this.initialListFields.some((it) => {
				if (it.name == "listTypeDemande") {
					this.criteriaTypeRequest = new SearchCriteria();
					this.criteriaTypeRequest.activated = true;
					return true;
				}
			});
	}

	ngAfterViewInit() {
		this.initSavedForm();

		this.initStatique();
	}

	initSavedForm() {
		this.activatedRoute.queryParams.subscribe((params) => {
			let sf = new SavedForm();
			let num = params["sf"];

			if (num) {
				sf = JSON.parse(localStorage.getItem("sf" + num));
				localStorage.removeItem("sf" + num);

				if (sf && sf.ebSavedFormNum == +num) this.loadSavedForm(sf);
			}
		});
	}

	async loadSavedForm(sf: SavedForm) {
		if (
			sf != null &&
			sf.ebModuleNum == this.ebModuleNum &&
			sf.ebCompNum == this.ebCompNum &&
			sf.ebUserNum == this.userConnected.ebUserNum
		) {
			this.selectedSavedForm = sf;
			this.applySearch = true;
			if (Statique.isDefined(this.selectedSavedForm)) {
				this.fillFormWithSf(this.selectedSavedForm);
				this.searchFromSavedSearch(JSON.parse(sf.formValues));
			}
		}
	}

	ngOnChanges(changes: SimpleChanges) {
		let found = this.initialListFields.find(function(item) {
			return (
				item.name == "listStatut" ||
				item.name == "listOrderStatus" ||
				item.name == "listDeliveryStatus"
			);
		});
		this.StatusPopUp = found;
		if (changes["initialListFields"] && changes["initialListFields"].currentValue) {
			this.removeAllFields();
			this.initFormData();
			if (Statique.isDefined(this.selectedSavedForm)) this.fillFormWithSf(this.selectedSavedForm);
		}

		if (!this.subscription || this.subscription.closed) this.initSavedForm();
	}

	addField() {
		let fieldIndex = this.getFieldSelector(".field-selector").selectedIndex;
		fieldIndex = Number(fieldIndex);

		if (this.listFields.length > fieldIndex && fieldIndex >= 0) {
			this.form.addControl(this.listFields[fieldIndex].name, this.fb.control(""));
			this.listSelectedFields.push(this.listFields[fieldIndex]);
			this.listFields.splice(fieldIndex, 1);
		}
	}
	deleteField(index) {
		if (this.listSelectedFields.length > index && index >= 0) {
			this.listFields.push(this.listSelectedFields[index]);
			this.form.removeControl(this.listSelectedFields[index].name);
			this.listSelectedFields.splice(index, 1);
		}
	}

	deleteInput(index) {
		if (this.listSelectedFields.length > index && index >= 0) {
			this.listFields.push(this.listSelectedFields[index]);
			this.form.removeControl(this.listSelectedFields[index].name);
			this.listSelectedFields.splice(index, 1);
			this.initialListFields.splice(index, 1);
			this.paramFields.forEach((el) => {
				el !== this.listSelectedFields[index];
			});
		}
	}

	getFieldSelector(selector: string) {
		return this.fieldSelector.nativeElement.querySelectorAll(selector)[0];
	}

	/**
	 * wait until all components are truly visible in the DOM or until 5seconds has passed
	 */
	async waitForFormViewInit() {
		let timeout = await Statique.waitForAvailability(
			function(timeout: number) {
				let fieldNotFound =
					this.listSelectedFields &&
					this.listSelectedFields.find((field) => {
						let found = false;

						if (field.type == "autocomplete") {
							found =
								this.autocompChildren &&
								this.autocompChildren.find((cmp) => cmp.name == field.name);
						} else if (field.type == "date") {
							found =
								this.datetimePickerChildren &&
								this.datetimePickerChildren.find((cmp) => cmp.name == field.name);
						} else if (field.type == "flag-multi-select") {
							found =
								this.flagMultiSelectChildren &&
								this.flagMultiSelectChildren.find((cmp) => cmp.name == field.name);
						} else {
							found = this.getFieldSelector("[name=" + field.name + "]");
						}

						return !found;
					});

				if (fieldNotFound)
					console.error("AdvancedFormSearch", "Field not found:", fieldNotFound.name, " !");

				return !fieldNotFound || timeout >= 5000;
			}.bind(this),
			500
		);
	}

	// methode pour instancier l'objet du formulaire et charger les données passées en param
	initFormData() {
		this.viewInit = false;

		if (this.initialListFields && typeof this.initFields !== "undefined") {
			if (this.initFields === "all")
				this.listSelectedFields = Statique.cloneObject(this.initialListFields);
			else {
				this.listSelectedFields = JSON.parse(this.initFields);
			}
		}
		//TODO MEP-ce bout de code (filtre) devra être supprimer après MEP
		this.listSelectedFields = this.listSelectedFields.filter(
			(it) => it.label !== "Infos & Docs status"
		);

		// init form
		this.form = this.fb.group({});
		if (this.initialListFields) {
			let formFields = {};

			this.initialListFields.forEach((it, index) => {
				let exists = this.listSelectedFields.some((sit) => {
					if (it.name === sit.name) {
						this.form.addControl(sit.name, this.fb.control(""));
						return true;
					}
				});
				if (!exists) this.listFields.push(it);
			});
		}

		this.viewInit = true;
	}

	// sauvegarder le formulaire
	async saveForm(event: any, isSaveRule: boolean = false) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		var self = this;
		let sf = new SavedForm();

		sf.ebModuleNum = this.ebModuleNum;
		sf.ebCompNum = this.ebCompNum;
		sf.ebUser = this.userConnected;

		if (this.selectedSavedForm) {
			sf.ebSavedFormNum = this.selectedSavedForm.ebSavedFormNum;
			sf.formName = this.selectedSavedForm.formName;
			sf.dateAjout = this.selectedSavedForm.dateAjout;
		}

		if (isSaveRule) {
			this.isSavingRule = true;
			sf.action = this.ruleSelectedAction;
			this.autocompChildren.forEach((cmp) => {
				if (cmp.name == "listStatutPopUp") {
					sf.actionTargets = cmp.selectedItem + "";
				}
			});
			sf.actionTargets = this.ruleSelectedTarget + "";
			sf.actionParameters = this.ruleSelectedParams;
			sf.actionTrigger = String(this.ruleSelectedActionTrigger);
			sf.formName = this.ruleSelectedName;
		}

		if (sf.action) {
			sf.ebCompNum = SavedFormIdentifier.SAVED_SEARCH_ACTION;
		}
		if (sf.actionParameters != null) {
			sf.actionParameters = String(sf.actionParameters);
		}

		if (this.listSelectedFields.length > 0) {
			let fieldSelector = null;
			if (!isSaveRule) {
				if (sf.ebSavedFormNum) {
					let messageHead = null;
					let message = null;
					this.translate.get("MODAL.SAVED_SEARCH").subscribe((res: string) => {
						messageHead = res;
					});
					this.translate.get("MODAL.SAVED_SEARCH_OVERRIDE").subscribe((res: string) => {
						message = res;
					});
					let res = await this.modalService.confirmPromise(
						messageHead,
						message + ` ` + `(${sf.formName})`
					);
					if (!res) {
						sf.formName = null;
						sf.ebSavedFormNum = null;
						sf.dateAjout = null;
					}
				}
			}

			try {
				if (!isSaveRule) {
					let result = await this.dialogService.input(
						"Save search",
						"Please name this search before saving it.",
						sf.formName
					);
					if (!result) return;

					sf.formName = "" + result;
				}
				this.listSelectedFields.forEach((field) => {
					fieldSelector = this.getFieldSelector(".operator-" + field.name);
					if (fieldSelector) field.operator = fieldSelector.value;

					if (field.type == "autocomplete") {
						this.autocompChildren.forEach((cmp) => {
							if (cmp.name == field.name) {
								field.default = cmp.selectedItem;
							}
						});
					} else if (field.type == "flag-multi-select") {
						this.flagMultiSelectChildren.forEach((cmp) => {
							if (cmp.name == field.name) {
								//j'ai ajouté "cmp.selectFlag();" pour remplir cmp.listFlag avec le flag choisi dans la recherche avancé
								cmp.selectFlag();
								field.default = cmp.listFlag
									? cmp.listFlag.map((it) => it.ebFlagNum).join(",")
									: "";
							}
						});
					} else if (field.type == "date") {
						this.datetimePickerChildren.forEach((cmp) => {
							if (cmp.name == field.name) {
								field.default = Statique.formatDate(cmp.date);
							}
						});
					} else {
						let fieldSelector = this.getFieldSelector("[name=" + field.name + "]");
						if (fieldSelector) {
							field.default = fieldSelector.value;
						}
					}
					// field.default = this.form.controls[field.name].value; // for some reason, it always gives empty value
				});

				sf.formValues = JSON.stringify(this.listSelectedFields);
				let newUrl = this.router.url.split("?");
				sf.url = newUrl[0];

				this.savedFormsService.saveSavedForm(sf).subscribe(
					function(res) {
						this.selectedSavedForm = res;

						if (isSaveRule) {
							this.selectedSavedForm.action = null;
							this.selectedSavedForm.actionTargets = "";
							this.selectedSavedForm.actionParameters = "";
							this.selectedSavedForm.actionTrigger = null;
							this.isSavingRule = false;
							this.closeAddRulePopup();
						}
						this.ruleSelectedAction = null;
						this.ruleSelectedTarget = null;
						this.ruleSelectedParams = null;
						this.ruleSelectedActionTrigger = null;
						this.ruleSelectedName = null;
						requestProcessing.afterGetResponse(event);
						this.savedFormsService.notifyComponentSavedForms(res);
					}.bind(this)
				);
			} catch (e) {
				console.error(e);
				// on modal dismiss
				requestProcessing.afterGetResponse(event);
			}
		}
	}

	removeAllFields() {
		this.listSelectedFields = this.listSelectedFields.filter((field) => {
			this.form.removeControl(field.name);
			this.listFields.push(field);
			return false;
		});
	}
	clearAllFields() {
		this.removeAllFields();
		this.initFormData();
		this.searchHandler.emit({});
	}

	// methode de remplissage du formulaire
	async fillFormWithSf(sf: SavedForm) {
		// await this.waitForFormViewInit();

		if (sf && this.ebCompNum == sf.ebCompNum && this.ebModuleNum == sf.ebModuleNum) {
			let parsedValues: Array<SearchField> = JSON.parse(sf.formValues),
				controls = this.form.controls;

			this.removeAllFields();
			// await this.waitForFormViewInit();

			this.listSelectedFields = this.listSelectedFields.filter((field, i) => {
				if (
					controls[field.name] &&
					((this.checkDirty && !controls[field.name].dirty) || !this.checkDirty)
				) {
					this.form.removeControl(field.name);
					this.listFields.push(field);
				} else return true;
			});

			parsedValues.forEach((field) => {
				if (
					controls[field.name] &&
					((this.checkDirty && !controls[field.name].dirty) || !this.checkDirty)
				) {
					// si le champs existe déjà, on remplace le champs existant par le nouveau
					let exists = this.listSelectedFields.some((it, i) => {
						if (it.name == field.name) {
							this.listSelectedFields.splice(i, 1);
							this.form.removeControl(field.name);
							this.form.addControl(field.name, this.fb.control(""));
							this.listSelectedFields.push(field);
							return true;
						}
					});
					if (!exists) {
						this.listFields.some((it, i) => {
							if (it.name == field.name) {
								this.listFields.splice(i, 1);
								this.form.addControl(field.name, this.fb.control(""));
								this.listSelectedFields.push(field);
								return true;
							}
						});
					}
				} else if (!controls[field.name]) {
					// si le champs n'existe pas encore, alors on l'ajoute dans le dropdown
					this.form.addControl(field.name, this.fb.control(""));
					this.listSelectedFields.push(field);
					this.listFields.some((it, i) => {
						if (field.name == it.name) {
							this.listFields.splice(i, 1);
							return true;
						}
					});
				}
			});

			// await this.waitUntilViewIsLoaded();

			if (this.applySearch) {
				this.applySearch = false;
				this.search();
			}
		}
	}

	stringify(data) {
		if (data) return JSON.stringify(data);
		return null;
	}

	search() {
		// await this.waitForFormViewInit();

		if (this.listSelectedFields && this.listSelectedFields.length > 0) {
			let result = {},
				value = null,
				operator = null;

			this.listSelectedFields.forEach((field) => {
				if (!this.fieldsOnly) operator = this.getFieldSelector(".operator-" + field.name).value;

				if (field.type == "autocomplete") {
					if (this.autocompChildren === undefined) {
						value = field.default;
					} else {
						this.autocompChildren.forEach((cmp) => {
							if (cmp.name == field.name) {
								value = cmp.selectedItem;
							}
						});
					}
				} else if (field.type == "date") {
					this.datetimePickerChildren.forEach((cmp) => {
						if (cmp.name == field.name) {
							value = Statique.formatDate(cmp.date);
						}
					});
				} else if (field.type == "flag-multi-select") {
					value = field.default;
				} else {
					let el = this.getFieldSelector("[name=" + field.name + "]");
					value = el ? el.value : null;
					if (value == "") {
						value = null;
					}
				}

				if (!this.fieldsOnly) result[field.name] = { value, operator };
				else result[field.name] = value;
			});

			this.searchHandler.emit(result);
		}
	}

	searchFromSavedSearch(sfValues: Array<any>) {
		let result = {};

		sfValues.forEach((field) => {
			if (Statique.isDefined(field.default)) result[field.name] = field.default;
		});

		this.searchHandler.emit(result);
	}

	getSearchInput(): any {
		if (this.listSelectedFields && this.listSelectedFields.length > 0) {
			let result = {},
				value = null,
				operator = null;

			this.listSelectedFields.forEach((field) => {
				if (!this.fieldsOnly) operator = this.getFieldSelector(".operator-" + field.name).value;
				if (field.type == "autocomplete") {
					if (this.autocompChildren === undefined) {
						value = field.default;
					} else {
						this.autocompChildren.forEach((cmp) => {
							if (cmp.name == field.name) {
								value = cmp.selectedItem;
							}
						});
					}
				} else if (field.type == "date") {
					this.datetimePickerChildren.forEach((cmp) => {
						if (cmp.name == field.name) {
							value = Statique.formatDate(cmp.date);
						}
					});
				} else if (field.type == "flag-multi-select") {
					value = field.default;
				} else {
					value = this.getFieldSelector("[name=" + field.name + "]").value;
					if (value == "") {
						value = null;
					}
				}

				if (!this.fieldsOnly) result[field.name] = { value, operator };
				else result[field.name] = value;
			});

			return result;
		}

		return null;
	}

	async showAddRulePopup() {
		if (this.listSelectedFields.length > 0) {
			this.showPopupAddRule = true;
		}
	}
	async closeAddRulePopup() {
		this.showPopupAddRule = false;
	}

	popupRuleActionOnChange() {
		this.ruleSelectedTarget = "";
		this.ruleSelectedParams = "";
	}

	addRule(event: any) {
		this.saveForm(event, true);
	}

	flagGetListForField(field: SearchField): Array<EbFlag> {
		if (!this.listIcons || !field || !field.default) return [];

		let rList = new Array<EbFlag>();
		let listIconsSelectedNum = field.default.split(",");

		this.listIcons.forEach((f) => {
			listIconsSelectedNum.forEach((ns) => {
				if (ns === new String(f.ebFlagNum)) {
					rList.push(f);
				}
			});
		});

		return rList;
	}

	flagSetListForField(flags: Array<EbFlag>, field: SearchField) {
		field.default = EbFlag.convertListToCommaSeparated(flags);
	}

	flagGetListForRule(): Array<EbFlag> {
		if (!this.listIcons || !this.ruleSelectedTarget) return [];

		let rList = new Array<EbFlag>();
		let listIconsSelectedNum = this.ruleSelectedTarget.split(",");

		this.listIcons.forEach((f) => {
			listIconsSelectedNum.forEach((ns) => {
				if (ns === new String(f.ebFlagNum)) {
					rList.push(f);
				}
			});
		});

		return rList;
	}

	flagSetListForRule(flags: Array<EbFlag>) {
		this.ruleSelectedTarget = EbFlag.convertListToCommaSeparated(flags);
	}

	initStatique() {
		this.statiqueService.getListEmailParam().subscribe((res: Array<ParamsMail>) => {
			this.listMails = res;
		});

		this.statiqueService.getListStatusByModule(this.ebModuleNum).subscribe((res: any) => {
			this.listStatusModule = res;
		});

		this.statiqueService.getListSavedSearchAction().subscribe((res) => {
			this.listSavedSearchAction = res;
		});

		this.statiqueService.getListSavedSearchActionTrigger().subscribe((res) => {
			this.listSavedSearchActionTrigger = res;
		});
	}

	ngOnDestroy() {
		if (this.subscribedSelectedSf) this.subscribedSelectedSf.unsubscribe();
		if (this.subscription) this.subscription.unsubscribe();
	}
}
