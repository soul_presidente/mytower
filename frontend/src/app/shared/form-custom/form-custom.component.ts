import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnInit,
	Output,
	QueryList,
	ViewChild,
	ViewChildren,
} from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { SavedForm } from "@app/classes/savedForm";
import { DialogService } from "@app/services/dialog.service";
import { SearchField } from "@app/classes/searchField";
import { ConnectedUserComponent } from "../connectedUserComponent";
import { AutocompleteComponent } from "../autocomplete/autocomplete.component";
import { Router } from "@angular/router";
import { DatetimePickerComponent } from "../datetime-picker/datetime-picker.component";
import { EbCustomDeclaration } from "@app/classes/declaration";
import { ebFormField } from "@app/classes/ebFormField";
import { EcCountry } from "@app/classes/country";
import { DouaneService } from "@app/services/douane.service";
import { EcCurrency } from "@app/classes/currency";
import { StatiqueService } from "@app/services/statique.service";
import { Subscription } from "rxjs";
import SingletonStatique from "@app/utils/SingletonStatique";

@Component({
	selector: "app-form-custom",
	templateUrl: "./form-custom.component.html",
	styleUrls: ["./form-custom.component.css"],
})
export class AdvancedFormCustomComponent extends ConnectedUserComponent implements OnInit {
	protected checkDirty: boolean;
	protected subscribedSelectedSf: Subscription;
	protected selectedSavedForm: SavedForm;

	public form: FormGroup;
	public listFields: Array<SearchField>;
	public listSelectedFields: Array<SearchField>;

	private applySearch: boolean = false;
	private viewInit: boolean = false;
	@Input()
	declaration: EbCustomDeclaration;
	@Input()
	column?: string; // undefined | all | json
	@Input()
	initFields?: string; // undefined | all | json
	fieldsJsonFile: string;
	@Input()
	ebModuleNum: number; // numéro du module
	@Input()
	editable?: boolean; // possibilité d'ajouter/supprimer les champs
	@Input()
	fieldsOnly?: boolean; // possibilité de définir les opérations (contains, equals, etc.)
	// @Input() paramFields: Array<SearchField>;
	@Input()
	initialListFields: any;
	@Input()
	initialListCategorie: any;
	@Input()
	lisrFormField: Array<ebFormField> = new Array<ebFormField>();
	@Output()
	searchHandler = new EventEmitter<any>();
	listCountry: Array<EcCountry> = new Array<EcCountry>();
	listformFieldTrier: Array<ebFormField> = new Array<ebFormField>();
	listCurrency: Array<EcCurrency> = null;
	@ViewChild("pageValid", { static: false })
	pageValid: ElementRef;

	@ViewChildren("autocomplete")
	autocompChildren: QueryList<AutocompleteComponent>;
	@ViewChildren("datetimepicker")
	datetimePickerChildren: QueryList<DatetimePickerComponent>;

	constructor(
		protected fb: FormBuilder,
		protected dialogService: DialogService,
		private douaneService: DouaneService,
		protected statiqueService: StatiqueService,
		public fieldSelector: ElementRef,
		public router: Router
	) {
		super();
	}

	ngOnInit() {
		setTimeout(() => {
			let listfieldeWithoutOrdre: Array<ebFormField> = new Array<ebFormField>();
			this.lisrFormField.forEach((el) => {
				if (el.ordre == null) listfieldeWithoutOrdre.push(el);
				else if (el.ordre != null) this.listformFieldTrier.push(el);
			});

			this.listformFieldTrier.sort(function(a, b) {
				return a.ordre < b.ordre ? -1 : b.ordre < a.ordre ? 1 : 0;
			});
			listfieldeWithoutOrdre.forEach((it) => {
				this.listformFieldTrier.push(it);
			});
		}, 2500);
		//this.getCountries();
		//this.getCurrency();
	}

	getCountries() {
		this.douaneService.listPays().subscribe((data) => {
			this.listCountry = data;
		});
	}

	async getCurrency() {
		this.listCurrency = await SingletonStatique.getListEcCurrency();
	}
}
