import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
	selector: "app-modal-confirmation",
	templateUrl: "./modal-confirmation.component.html",
	styleUrls: ["./modal-confirmation.component.scss"],
})
export class ModalConfirmationComponent {
	@Input()
	dialogBodyMsg: any;
	@Input()
	isVisible: boolean;
	@Output()
	dialogCanceled: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	dialogConfirmed: EventEmitter<any> = new EventEmitter<any>();

	constructor() {}

	cancelFunction() {
		this.dialogCanceled.emit();
	}

	confirmFunction(event) {
		this.dialogConfirmed.emit(event);
	}
}
