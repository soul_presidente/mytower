import { EbUser } from "@app/classes/user";
import { AccessRights, ServiceType, StatusDemande, UserRole } from "@app/utils/enumeration";
import { AuthenticationService } from "@app/services/authentication.service";
import { ACL } from "@app/classes/ACL";

export class ConnectedUserComponent {
	public isChargeur: boolean = false;
	public isAdmin: boolean = false;
	public isPrestataire: boolean = false;
	public isTransporteur: boolean = false;
	public isBroker: boolean = false;
	public isTransporteurBroker: boolean = false;
	public isControlTower: boolean = false;
	public isCtConnectedAs: boolean = false;
	public userConnected: EbUser = AuthenticationService.getConnectedUser();
	public isAuthenticated: boolean = false;
	public isSuperAdmin: boolean = false;
	public ebUserNum: number;
	public isUnknownUser: boolean = false;
	public emailUnknownUser: string = null;
	public ACL = ACL;

	// Untyped object that will store each ACL code as variable
	// Made static for memory optimizations and for synchronous refresh
	private static aclsStatic: any = {};
	public get acls(): any {
		return ConnectedUserComponent.aclsStatic;
	}
	public set acls(value: any) {
		ConnectedUserComponent.aclsStatic = value;
	}

	constructor(protected authenticationService?: AuthenticationService) {
		this.checkChange();
	}

	checkChange() {
		this.userConnected = AuthenticationService.getConnectedUser();
		this.initUser();
	}

	initUser() {
		if (this.userConnected !== null) {
			this.ebUserNum = this.userConnected.ebUserNum;
			if (this.userConnected.admin) this.isAdmin = true;
			if (this.userConnected.superAdmin) this.isSuperAdmin = true;
			if (this.userConnected.realUserNum && this.userConnected.realUserEtabNum) {
				this.isCtConnectedAs = true;
			}
			if (this.userConnected.role == UserRole.CHARGEUR) this.isChargeur = true;
			else if (this.userConnected.role == UserRole.CONTROL_TOWER) this.isControlTower = true;
			else if (this.userConnected.role == UserRole.PRESTATAIRE) {
				this.isPrestataire = true;
				if (this.userConnected.service == ServiceType.BROKER) this.isBroker = true;
				else if (this.userConnected.service == ServiceType.TRANSPORTEUR) this.isTransporteur = true;
				else if (this.userConnected.service == ServiceType.TRANSPORTEUR_BROKER)
					this.isTransporteurBroker = true;
			}
			this.isAuthenticated = this.userConnected.status == StatusDemande.ACCPETER;

			// Read acls
			let aclRightsDTO = AuthenticationService.readACLs();
			aclRightsDTO.forEach((aclDto) => {
				let code = aclDto.code;
				let value: any;
				if (aclDto.type === ACL.RuleType.BOOLEAN) {
					value = aclDto.value === "true";
				} else {
					value = aclDto.value;
				}

				this.acls[code] = value;
			});
		} else {
			this.isAdmin = false;
			this.isControlTower = false;
			this.isChargeur = false;
			this.isPrestataire = false;
			this.isBroker = false;
			this.isTransporteur = false;
			this.isTransporteurBroker = false;
			this.isSuperAdmin = false;
			this.isCtConnectedAs = false;
		}
		if (this.isUnknownUser) {
			this.isTransporteur = true;
		}
	}

	hasAccess(ecModuleNum: number): boolean {
		let retour = this.userConnected.hasAccess(ecModuleNum);
		return retour;
	}

	hasContribution(ecModuleNum: number): boolean {
		return this.userConnected.hasContribution(ecModuleNum);
	}

	getAccessRight(ecModule) {
		if (!this.userConnected || !this.userConnected.listAccessRights)
			return AccessRights.NON_VISIBLE;

		if (this.userConnected.listAccessRights && this.userConnected.listAccessRights.length == 0)
			return AccessRights.NON_VISIBLE;

		let access = this.userConnected.listAccessRights.find((access) => {
			return access.ecModuleNum == ecModule;
		});

		if (access) return access.accessRight;
		return AccessRights.NON_VISIBLE;
	}

	get isControlTowerAndSuperAdmin(): boolean {
		return this.userConnected.role == ServiceType.CONTROL_TOWER && this.userConnected.superAdmin;
	}

	get isControlTowerAndAdmin(): boolean {
		return this.userConnected.role == ServiceType.CONTROL_TOWER && this.userConnected.admin;
	}

	/**
	 * Return selected value as boolean represent value identifier
	 * @description Use only in special case, on templates use acls[code] to retrieve value for performance optimizations
	 * @param code ACL Code
	 */
	getAclBoolValue(code: string): boolean {
		return this.acls[code] as boolean;
	}

	/**
	 * Return selected value as string represent value identifier
	 * @description Use only in special case, on templates use acls[code] to retrieve value for performance optimizations
	 * @param code ACL Code
	 */
	getAclValue(code: string): string {
		return this.acls[code] as string;
	}
}
