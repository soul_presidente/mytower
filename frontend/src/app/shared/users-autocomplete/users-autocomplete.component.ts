import {
	ChangeDetectorRef,
	Component,
	EventEmitter,
	forwardRef,
	Input,
	OnInit,
	Output,
} from "@angular/core";
import { switchMap } from "rxjs/operators";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { SearchCriteria } from "../../utils/searchCriteria";
import { Statique } from "../../utils/statique";
import { EbUser } from "../../classes/user";
import { StatiqueService } from "../../services/statique.service";
import { TranslateService } from "@ngx-translate/core";
import { ConnectedUserComponent } from "../connectedUserComponent";

@Component({
	selector: "app-users-autocomplete",
	templateUrl: "./users-autocomplete.component.html",
	styleUrls: [],
	providers: [
		{
			provide: NG_VALUE_ACCESSOR,
			useExisting: forwardRef(() => UsersAutocompleteComponent),
			multi: true,
		},
	],
})
export class UsersAutocompleteComponent extends ConnectedUserComponent
	implements OnInit, ControlValueAccessor {
	@Input()
	name: string;
	@Input()
	isRequired: boolean;
	@Input()
	disabled: boolean = false;
	@Input()
	inputModel: any;
	@Output()
	inputModelChange = new EventEmitter<any>();

	@Input()
	criteria: SearchCriteria;

	@Input()
	sMultiple: string;
	@Input()
	placeholder: string;
	@Input()
	srcTypeahead: string;
	@Input()
	srcListItems: string;
	@Input()
	defaultValue: any;
	@Input()
	bindLabel: string;
	@Input()
	bindValue: any;
	@Input()
	options: any;
	@Input()
	listItems: any;
	@Input()
	groupByAttr?: string;
	@Input()
	fetchGroupByAttribute?: string;
	@Input()
	bindObject?: boolean;

	@Output()
	onUpdateChanges = new EventEmitter<any>();

	public multiple: boolean;
	public listTermsTypeahead: any;
	public typeaheadEvent: EventEmitter<String> = new EventEmitter<string>();

	constructor(
		protected cd: ChangeDetectorRef,
		private statiqueService: StatiqueService,
		private translate: TranslateService
	) {
		super();
	}

	ngOnInit() {
		this.multiple = this.sMultiple == "true";

		if (this.defaultValue) {
			if (this.multiple) this.inputModel = JSON.parse(this.defaultValue);
			else this.inputModel = this.defaultValue.length > 0 ? JSON.parse(this.defaultValue)[0] : "";
		}

		if (!this.defaultValue && this.srcTypeahead) this.srcListItems = null;

		if (!this.srcTypeahead) this.srcTypeahead = Statique.controllerCommunity + "/get-list-user";
		if (!this.placeholder) {
			this.translate.get("PRICING_BOOKING.CHOOSE_USER").subscribe((res: string) => {
				this.placeholder = res + " ...";
			});
		}
		if (!this.criteria) this.criteria = new SearchCriteria();

		this.getListItems();

		this.setTypeahead();
	}

	getListItems() {
		if (this.srcListItems) {
			this.getListFromSource(this.srcListItems).subscribe(
				function(res) {
					this.listItems = res;
				}.bind(this)
			);

			return true;
		} else if (this.listItems) return true;

		return false;
	}

	setTypeahead() {
		if (!this.srcTypeahead) return false;

		this.typeaheadEvent
			// .distinctUntilChanged()
			// .debounceTime(200)
			.pipe(switchMap((term) => this.getTermsFromSource(term, this.srcTypeahead)))
			.subscribe(
				(data) => {
					this.cd.markForCheck();
					this.listItems = data;
				},
				(err) => {
					this.listItems = [];
				}
			);

		return true;
	}

	getListFromSource(apiUrl) {
		let searchCriteria = Statique.copyObject(this.criteria);
		return this.statiqueService.getListFromSourceByCriteria(apiUrl, searchCriteria);
	}
	getTermsFromSource(term, apiUrl) {
		if (term == "" || !term) term = "*";
		let searchCriteria = Statique.copyObject(this.criteria);
		searchCriteria["searchterm"] = term;
		return this.statiqueService.getTermsFromSourceByCriteria(apiUrl, searchCriteria);
	}

	updateChanges($event: any) {
		this.inputModel = $event;
		let val = $event ? $event.ebUserNum : $event;
		let obj = this.listItems ? this.listItems.find((it) => it.ebUserNum == val) : null;
		this.inputModelChange.emit(obj);
		this.cd.detectChanges();

		if (this.onUpdateChanges) {
			this.onUpdateChanges.emit(obj);
		}
	}

	onChange: (_: any) => void = (_: any) => {};
	onTouched: () => void = () => {};
	// called when new value is input from parent
	writeValue(value: EbUser): void {
		this.updateChanges(value);
		// if(value && value.ebUserNum){
		//   this.listItems = [value];
		// }
	}
	registerOnChange(fn: any): void {
		this.onChange = fn;
	}
	registerOnTouched(fn: any): void {
		this.onTouched = fn;
	}
}
