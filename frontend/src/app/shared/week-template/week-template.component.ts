import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { StatiqueService } from "@app/services/statique.service";
import { EbWeekTemplateDTO } from "@app/classes/week-template/EbWeekTemplateDTO";
import { EbWeekTemplateDayDTO } from "@app/classes/week-template/EbWeekTemplateDayDTO";
import { EbWeekTemplateHourDTO } from "@app/classes/week-template/EbWeekTemplateHourDTO";
import { GenericTableScreen } from "@app/utils/enumeration";
import { GenericTableInfos } from "../generic-table/generic-table-infos";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { AuthenticationService } from "@app/services/authentication.service";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "../modal/modal.service";
import { HeaderService } from "@app/services/header.service";
import { BreadcumbComponent, HeaderInfos } from "../header/header-infos";
import { WeekTemplateService } from "@app/services/WeekTemplateService";
import { GenericTableComponent } from "../generic-table/generic-table.component";
import { ConnectedUserComponent } from "../connectedUserComponent";
import { GenericEnum } from "@app/classes/GenericEnum";
import { GenericEnumPlanTrans } from "@app/utils/enumeration";
import { EbWeekTemplatePeriodDTO } from "@app/classes/week-template/EbWeekTemplatePeriodDTO";

@Component({
	selector: "app-week-template",
	templateUrl: "./week-template.component.html",
	styleUrls: ["./week-template.component.scss"],
})
export class WeekTemplateComponent extends ConnectedUserComponent implements OnInit {
	Statique = Statique;

	@Input()
	period: EbWeekTemplateDTO;

	@Output()
	confirmeEvent: EventEmitter<any> = new EventEmitter<any>();

	@Output()
	cancelEvent: EventEmitter<any> = new EventEmitter<any>();

	listDay: any;
	@Input()
	ebWeekTemplate: EbWeekTemplateDTO;
	isAddEditMode = false;
	weekTemplate: EbWeekTemplateDTO = new EbWeekTemplateDTO();
	ebWeekTemplateHourDTO: EbWeekTemplateHourDTO = new EbWeekTemplateHourDTO();
	backupWeekTemplate: EbWeekTemplateDTO;
	editingIndex: number;
	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;
	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;
	dataObject: EbWeekTemplateDTO = new EbWeekTemplateDTO();
	isAddObject = false;
	listWeekType: Array<any> = [];
	ebWeekTemplateDTO: EbWeekTemplateDTO = new EbWeekTemplateDTO();
	openingPeriod: EbWeekTemplateDTO;
	backupEditObject: EbWeekTemplateDTO;
	listTypeWeek: Array<GenericEnum> = [];
	@Output("onDataChanged")
	isValideDate: boolean = false;
	onDataChanged = new EventEmitter<any>();
	test = false;
	constructor(
		protected statiqueService: StatiqueService,
		protected headerService: HeaderService,
		protected authenticationService: AuthenticationService,
		protected translate: TranslateService,
		protected modalService: ModalService,
		protected weekTemplateService: WeekTemplateService
	) {
		super();
		this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
	}
	hide_opening_from_hour: boolean = true;
	addRow: boolean = true;

	ngOnInit() {
		this.statiqueService.getListGenericEnum(GenericEnumPlanTrans.WeekType).subscribe((res) => {
			this.listTypeWeek = res;
		});
		this.searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.ebWeekTemplateDTO.xEbCompagnie = this.userConnected.ebCompagnie;
		if (!this.period) {
			this.initialize();
		}

		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 50;

		this.dataInfos.cols = [
			{
				field: "label",
				translateCode: "GENERAL.LABEL",
			},
			{
				field: "type",
				translateCode: "CUSTOM.TYPE",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					let rValue = "";
					let foundKey = GenericEnum.findKeyByCode(data, this.listTypeWeek);

					if (foundKey) {
						rValue = this.translate.instant("GENERAL.WEEKTEMPLATE_TYPE." + foundKey);
					}
					return rValue;
				}.bind(this),
			},

			{
				field: "dateDebut",
				translateCode: "GENERAL.START_DATE",
				render: function(data) {
					return this.Statique.formatDate(data);
				}.bind(this),
			},
			{
				field: "dateFin",
				translateCode: "GENERAL.END_DATE",
				render: function(data) {
					return this.Statique.formatDate(data);
				}.bind(this),
			},
		];

		this.dataInfos.dataKey = "ebWeekTemplateNum";
		this.dataInfos.dataType = EbWeekTemplateDTO;
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerWeekTemplate + "/list-week-template-table";
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
	}

	initialize() {
		this.listDay = Statique.dowEnum;
		this.period = new EbWeekTemplateDTO();

		this.period.days = new Array<EbWeekTemplateDayDTO>();
		this.listDay.forEach((statiqueDay, index) => {
			let day = new EbWeekTemplateDayDTO();
			day.code = index + 1; // MONDAY CODE IS 1...
			this.period.days.push(day);
		});

		//this.period = new EbWeekTemplatePeriodDTO();
		//	this.period.week = new EbWeekTemplateDTO();

		//	this.period.week.days = new Array<EbWeekTemplateDayDTO>();
		//this.listDay.forEach((statiqueDay, index) => {
		//	let day = new EbWeekTemplateDayDTO();
		//	day.code = index + 1; // MONDAY CODE IS 1...
		//	this.period.week.days.push(day);
		//});
	}

	getDayName(codeDay: number) {
		return this.listDay[codeDay - 1];
	}

	updateListeOpeningDay() {}

	deleteRangeHour(day, hour) {
		day.hours.forEach((item, index) => {
			if (item == hour) day.hours.splice(index, 1);
		});
	}

	addRangeHour(day: EbWeekTemplateDayDTO) {
		if (!day.hours) day.hours = new Array<EbWeekTemplateHourDTO>();

		let hour: EbWeekTemplateHourDTO = new EbWeekTemplateHourDTO();
		day.hours.push(hour);
	}

	addRangeHourRow(day: EbWeekTemplateDayDTO) {
		if (this.addRow) {
			this.addRangeHour(day);
			this.addRow = false;
		}
	}

	emitConfirme() {
		this.confirmeEvent.emit(this.period);
	}

	emitCancel() {
		this.cancelEvent.emit();
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.PLANNING_CHARGEMENT",
			},
		];
	}
	deleteWeekTemplate(ebWeekTemplateDTO: EbWeekTemplateDTO) {
		let $this = this;

		this.modalService.confirm(
			this.translate.instant("GENERAL.CONFIRMATION"),
			this.translate.instant("GENERAL.CONFIRM_DELETE_WEEKTEMPLATE"),
			function() {
				$this.weekTemplateService
					.deleteWeekTemplate(ebWeekTemplateDTO)
					.subscribe((data: boolean) => {
						$this.genericTable.refreshData();
					});
			},
			function() {},
			true,
			this.translate.instant("GENERAL.CONFIRM"),
			this.translate.instant("GENERAL.CANCEL")
		);
	}
	displayAdd(isUpdate?: boolean) {
		if (!isUpdate) {
			this.ebWeekTemplateDTO = new EbWeekTemplateDTO();
		}

		this.isAddObject = true;
	}

	diplayEdit(weektemp: EbWeekTemplateDTO) {
		//this.ebWeekTemplateDTO = weektemp;
		this.ebWeekTemplateDTO = Statique.cloneObject(weektemp, new EbWeekTemplateDTO());
		this.backupEditObject = this.ebWeekTemplateDTO;
		this.displayAdd(true);
	}

	backToList() {
		this.isAddObject = false;
		this.genericTable.refreshData();
		this.dataInfos.showAddBtn = true;
	}
	saveWeekTemplate(ebWeekTemplateDTO: EbWeekTemplateDTO) {
		let $this = this;
		ebWeekTemplateDTO.xEbCompagnie = this.userConnected.ebCompagnie;
		if (ebWeekTemplateDTO.ebWeektemplateNum == null) ebWeekTemplateDTO.days = this.period.days;

		this.weekTemplateService.saveWeekTemplate(ebWeekTemplateDTO).subscribe(
			function(data) {
				this.initialize();
				this.ebWeekTemplateDTO = this.period;
				this.backToList();
			}.bind(this)
		);
	}
	getTypeWeekDisplay(typeWeekCode): string {
		let rValue = "";
		let foundKey = GenericEnum.findKeyByCode(typeWeekCode, this.listTypeWeek);

		if (foundKey) {
			rValue = this.translate.instant("GENERAL.WEEKTEMPLATE_TYPE." + foundKey);
		}
		return rValue;
	}
	updateWeekTemplate(ebWeekTemplateDTO: EbWeekTemplateDTO) {
		ebWeekTemplateDTO.xEbCompagnie = this.userConnected.ebCompagnie;

		this.weekTemplateService
			.updateWeekTemplate(this.ebWeekTemplateDTO.ebWeektemplateNum, ebWeekTemplateDTO)
			.subscribe(
				function(data) {
					this.backToList();
				}.bind(this)
			);
	}
	onDataLoaded(data: Array<EbWeekTemplateDTO>) {
		this.onDataChanged.emit(data);
	}

	trackByIndex(index: number, obj: any): any {
		return index;
	}
}
