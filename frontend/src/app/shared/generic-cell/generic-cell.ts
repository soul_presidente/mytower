import {
	AfterViewInit,
	Component,
	ComponentFactoryResolver,
	ComponentRef,
	EventEmitter,
	Input,
	OnChanges,
	OnDestroy,
	Output,
	ViewChild,
	ViewContainerRef,
	Injector,
} from "@angular/core";
import { Statique } from "@app/utils/statique";
import { AuthenticationService } from "@app/services/authentication.service";

export abstract class GenericCell<M, P extends GenericCell.Params, C> {
	model: M;

	customEvent = new EventEmitter();
	params: P;
	context: C;

	Statique = Statique;
	AuthenticationService = AuthenticationService;

	injector: Injector; // Used to inject some dependencies on generic cell as constructor cannot be overriden due to TypeScript limitation

	onModelChanged() {
		// Override me on implementations if required
	}

	setModel(model: M) {
		this.model = model;
		this.onModelChanged();
	}

	constructor() {}

	outputEvent(key: string, value: any) {
		if (this.customEvent) {
			this.customEvent.emit({
				key: key,
				value: value,
				model: this.model,
			});
		}
	}
}

export namespace GenericCell {
	export abstract class Params {
		public constructor(init?: Partial<Params>) {
			Object.assign(this, init);
		}
	}
}

@Component({
	selector: "app-generic-cell",
	templateUrl: "./generic-cell-hoster.html",
	styleUrls: ["./generic-cell-hoster.scss"],
})
export class GenericCellHoster implements AfterViewInit, OnChanges, OnDestroy {
	@Input()
	model: any;
	@Input()
	cellClass: GenericCell<any, any, any>;
	@Input()
	cellParams: any;
	@Input()
	context: any;
	@Input()
	regenerateOnChanges: boolean = false;

	@ViewChild("target", { read: ViewContainerRef, static: true })
	target: any;

	@Output()
	customEvent = new EventEmitter();

	constructor(
		private componentFactoryResolver: ComponentFactoryResolver,
		private injector: Injector
	) {}

	private initialized: boolean = false;
	private componentRef: ComponentRef<any>;

	private updateComponent() {
		if (!this.initialized || !this.cellClass) {
			return;
		}

		if (this.componentRef) {
			if (this.regenerateOnChanges) {
				this.componentRef.destroy();
				if (this.target) {
					this.target.remove();
				}
			} else {
				return;
			}
		}

		let componentFactory = this.componentFactoryResolver.resolveComponentFactory(this
			.cellClass as any);
		this.componentRef = this.target.createComponent(componentFactory);

		// Can be param object directly or close that return the param object
		let finalParams = this.cellParams != null ? this.cellParams : {};
		if (typeof finalParams === "function") {
			finalParams = finalParams(this.model, this.context);
		}
		this.componentRef.instance.params = finalParams;

		this.componentRef.instance.context = this.context;
		this.componentRef.instance.customEvent = this.customEvent;
		this.componentRef.instance.injector = this.injector;

		this.componentRef.instance.setModel(this.model);
		this.componentRef.changeDetectorRef.detectChanges();
	}

	ngOnChanges() {
		this.updateComponent();
	}

	ngAfterViewInit() {
		this.initialized = true;
		this.updateComponent();
	}

	ngOnDestroy() {
		if (this.componentRef) {
			this.componentRef.destroy();
		}
		if (
			this.target &&
			this.target.element &&
			this.target.element.nativeElement &&
			this.target.element.nativeElement.parentElement
		) {
			this.target.element.nativeElement.parentElement.remove();
		}
	}
}
