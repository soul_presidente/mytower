import { Component, OnInit } from "@angular/core";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";

@Component({
	selector: "app-generic-cell-text-input",
	templateUrl: "./text-input.generic-cell.html",
})
export class TextInputGenericCell extends GenericCell<any, TextInputGenericCell.Params, any>
	implements OnInit {
	inputType: string;
	field: string;
	pattern: string | RegExp;
	showOnHover: boolean;

	constructor() {
		super();
	}

	ngOnInit() {
		this.field = this.params.field != null ? this.params.field : "";
		this.inputType = this.params.inputType != null ? this.params.inputType : "text";
		this.showOnHover = this.params.showOnHover != null ? this.params.showOnHover : false;
		this.pattern = this.params.pattern != null ? this.params.pattern : null;
	}

	get required(): boolean {
		if (this.params.requiredOn) {
			return this.params.requiredOn(this.model);
		}
		return this.params.required != null ? this.params.required : false;
	}

	get disabled(): boolean {
		if (this.params.disabledOn) {
			return this.params.disabledOn(this.model);
		}
		return this.params.disabled != null ? this.params.disabled : false;
	}

	get readOnly(): boolean {
		return this.params.readOnlyOn != null ? this.params.readOnlyOn(this.model) : false;
	}

	get max(): number {
		return this.params.maxOn != null ? this.params.maxOn(this.model) : null;
	}

	get keyup(): any {
		return this.params.keyupOn != null ? this.params.keyupOn(event, this.model) : null;
	}

	get title(): any {
		return this.params.getTitle != null ? this.params.getTitle(this.model) : "";
	}
}

export namespace TextInputGenericCell {
	export class Params extends GenericCell.Params {
		field: string;
		inputType: string;
		showOnHover: boolean;
		pattern: string | RegExp;
		required?: boolean;
		requiredOn?: (model: any) => boolean;
		disabled?: boolean;
		disabledOn?: (model: any) => boolean;
		readOnlyOn?: (model: any) => boolean;
		maxOn?: (model: any) => number;
		min?: number;
		keyupOn?: (event: Event, model: any) => any;
		getTitle?: (model: any) => string;
	}
}
