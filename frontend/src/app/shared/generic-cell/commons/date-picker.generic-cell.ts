import { Component, OnInit } from "@angular/core";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";
import { Statique } from "@app/utils/statique";

@Component({
	selector: "app-generic-cell-date-picker",
	templateUrl: "./date-picker.generic-cell.html",
	styleUrls: ["./date-picker.generic-cell.scss"],
})
export class DatePickerGenericCell extends GenericCell<any, DatePickerGenericCell.Params, any>
	implements OnInit {
	field: string;
	readonlyInput: boolean;
	hideIfNull: boolean;
	showOnHover: boolean;
	forceUTC: boolean;
	constructor() {
		super();
	}

	ngOnInit() {
		this.field = this.params.field != null ? this.params.field : "";
		this.readonlyInput = this.params.readonlyInput != null ? this.params.readonlyInput : false;
		this.hideIfNull = this.params.hideIfNull != null ? this.params.hideIfNull : false;
		this.showOnHover = this.params.showOnHover != null ? this.params.showOnHover : false;
		this.forceUTC = this.params.forceUTC != null ? this.params.forceUTC : false;
	}

	outputDate(eventType) {
		this.outputEvent("date-change", eventType);
	}
}

export namespace DatePickerGenericCell {
	export class Params extends GenericCell.Params {
		field: string;
		readonlyInput: boolean;
		hideIfNull: boolean;
		showOnHover: boolean;
		forceUTC: boolean;
	}
}
