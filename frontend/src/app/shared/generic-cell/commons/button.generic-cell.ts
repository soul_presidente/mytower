import { Component, OnInit } from "@angular/core";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";

@Component({
	selector: "app-generic-cell-button",
	templateUrl: "./button.generic-cell.html",
})
export class ButtonGenericCell extends GenericCell<any, ButtonGenericCell.Params, any>
	implements OnInit {
	field: string;
	text: string;
	class: string;
	hideIfNoText: boolean;
	textRender: (model: any) => string;
	routerLink: string;
	link: string;

	constructor() {
		super();
	}

	ngOnInit() {
		this.link = this.params.link != null ? this.params.link : "";
		this.routerLink = this.params.routerLink != null ? this.params.routerLink : "";
		this.field = this.params.field != null ? this.params.field : "";
		this.text = this.params.text != null ? this.params.text : "";
		this.class = this.params.class != null ? this.params.class : "btn";
		this.hideIfNoText = this.params.hideIfNoText != null ? this.params.hideIfNoText : true;
		this.textRender = this.params.textRender != null ? this.params.textRender : null;
	}

	get renderedText(): string {
		if (this.textRender) {
			return this.textRender(this.model);
		}
		if (this.text) {
			return this.text;
		} else if (this.model && this.model[this.field]) {
			return this.model[this.field];
		} else {
			return "";
		}
	}
}

export namespace ButtonGenericCell {
	export class Params extends GenericCell.Params {
		field?: string;
		text?: string;
		class?: string;
		hideIfNoText?: boolean;
		routerLink?: string;
		link?: string;
		textRender?: (model: any) => string; // Custom text rendering
	}
}
