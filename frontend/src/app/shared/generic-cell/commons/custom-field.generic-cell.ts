import { Component, OnInit } from "@angular/core";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";
import { IField } from "@app/classes/customField";

@Component({
	selector: "app-generic-cell-custom-field",
	templateUrl: "./custom-field.generic-cell.html",
})
export class CustomFieldGenericCell extends GenericCell<any, CustomFieldGenericCell.Params, any>
	implements OnInit {
	currentFieldValue: string = "";
	currentTooltipValue: string = "";
	showOnHover: boolean;

	constructor() {
		super();
	}

	ngOnInit() {
		this.showOnHover = this.params.showOnHover != null ? this.params.showOnHover : false;
	}

	get readOnly(): boolean {
		return this.params.readOnlyOn != null ? this.params.readOnlyOn(this.model) : false;
	}

	onModelChanged() {
		// Retrieve field value from model
		let val = "";
		let fields = this.model["listCustomsFields"];
		let compName = this.params.toolTipRender ? this.params.toolTipRender(this.model) : "";
		if (!compName) compName = "";

		if (fields && fields.length > 0) {
			let o;
			for (o in fields) {
				if (fields[o].value && fields[o].name == this.params.field["name"]) {
					val = fields[o].value;
					break;
				}
			}
		}

		this.currentFieldValue = val;
		this.currentTooltipValue = compName ? compName : val;
	}

	commitModelChange() {
		let fields = this.model["listCustomsFields"];
		let compName = this.params.toolTipRender ? this.params.toolTipRender(this.model) : "";
		if (!compName) compName = "";

		let fieldFound = false;
		if (fields && fields.length > 0) {
			let o;
			for (o in fields) {
				if (fields[o].name == this.params.field["name"]) {
					fields[o].value = this.currentFieldValue;
					fieldFound = true;
					break;
				}
			}
		}

		if (!fieldFound) {
			console.error(
				"CustomField to save was not found in listCustomFields => " + this.params.field["name"]
			);
		}

		this.currentTooltipValue = compName ? compName : this.currentFieldValue;
		this.model["listCustomsFields"] = fields;

		this.outputEvent("value-changed", this.currentFieldValue);
	}
}

export namespace CustomFieldGenericCell {
	export class Params extends GenericCell.Params {
		readOnlyOn?: (model: any) => boolean;
		field: IField;
		toolTipRender?: (model: any) => string;
		showOnHover: boolean;
	}
}
