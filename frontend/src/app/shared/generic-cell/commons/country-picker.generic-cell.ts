import { Component, OnInit } from "@angular/core";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";
import { EcCountry } from "@app/classes/country";

@Component({
	selector: "app-generic-cell-country-picker",
	templateUrl: "./country-picker.generic-cell.html",
	styleUrls: ["./country-picker.generic-cell.scss"],
})
export class CountryPickerGenericCell extends GenericCell<any, CountryPickerGenericCell.Params, any>
	implements OnInit {
	field: string;
	showOnHover: boolean;
	multiple: boolean;

	constructor() {
		super();
	}

	ngOnInit() {
		this.field = this.params.field != null ? this.params.field : "";
		this.showOnHover = this.params.showOnHover != null ? this.params.showOnHover : false;
		this.multiple = this.params.multiple;
	}

	get required(): boolean {
		if (this.params.requiredOn) {
			return this.params.requiredOn(this.model);
		}
		return this.params.required != null ? this.params.required : false;
	}

	get disabled(): boolean {
		if (this.params.disabledOn) {
			return this.params.disabledOn(this.model);
		}
		return this.params.disabled != null ? this.params.disabled : false;
	}
	get values(): Array<EcCountry> {
		if (this.params.valuesOn) {
			return this.params.valuesOn(this.model);
		}
		return this.params.values != null ? this.params.values : [];
	}
}

export namespace CountryPickerGenericCell {
	export class Params extends GenericCell.Params {
		field: string;
		values?: Array<EcCountry>;
		valuesOn?: (model: any) => Array<EcCountry>;
		showOnHover: boolean;
		required?: boolean;
		requiredOn?: (model: any) => boolean;
		disabled?: boolean;
		disabledOn?: (model: any) => boolean;
		multiple?: boolean;
	}
}
