import { Component, OnInit } from "@angular/core";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";

@Component({
	selector: "app-generic-cell-flag-single-view",
	templateUrl: "./flag-single-view.generic-cell.html",
})
export class FlagSingleViewGenericCell
	extends GenericCell<any, FlagSingleViewGenericCell.Params, any>
	implements OnInit {
	fieldFlagIcon: string;
	fieldFlagColor: string;

	constructor() {
		super();
	}

	ngOnInit() {
		this.fieldFlagIcon = this.params.fieldFlagIcon;
		this.fieldFlagColor = this.params.fieldFlagColor;
	}
}

export namespace FlagSingleViewGenericCell {
	export class Params extends GenericCell.Params {
		fieldFlagIcon: string;
		fieldFlagColor: string;
	}
}
