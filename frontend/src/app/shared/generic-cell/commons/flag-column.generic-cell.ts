import { Component, OnInit } from "@angular/core";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";

@Component({
	selector: "app-generic-cell-flag-column",
	templateUrl: "./flag-column.generic-cell.html",
})
export class FlagColumnGenericCell extends GenericCell<any, FlagColumnGenericCell.Params, any>
	implements OnInit {
	field: string;
	contributionAccess: boolean;
	moduleNum: number;

	constructor() {
		super();
	}

	ngOnInit() {
		this.field = this.params.field != null ? this.params.field : null;
		this.contributionAccess = this.params.contributionAccess ? this.params.contributionAccess : null;
		this.moduleNum = this.params.moduleNum != null ? this.params.moduleNum : null;
	}
}

export namespace FlagColumnGenericCell {
	export class Params extends GenericCell.Params {
		field: string;
		contributionAccess: boolean;
		moduleNum: number;
	}
}
