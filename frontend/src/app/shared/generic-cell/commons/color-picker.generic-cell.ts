import { Component, OnInit } from "@angular/core";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";

@Component({
	selector: "app-generic-cell-color-picker",
	templateUrl: "./color-picker.generic-cell.html",
})
export class ColorPickerGenericCell extends GenericCell<any, ColorPickerGenericCell.Params, any>
	implements OnInit {
	field: string;

	constructor() {
		super();
	}

	ngOnInit() {
		this.field = this.params.field != null ? this.params.field : "color";
	}
}

export namespace ColorPickerGenericCell {
	export class Params extends GenericCell.Params {
		field: string;
	}
}
