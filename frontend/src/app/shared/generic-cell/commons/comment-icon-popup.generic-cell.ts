import {
	Component,
	OnInit,
	Injector,
	ApplicationRef,
	ComponentFactoryResolver,
	EmbeddedViewRef,
	ViewChild,
	ElementRef,
} from "@angular/core";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";

@Component({
	selector: "app-generic-cell-comment-icon-popup",
	templateUrl: "./comment-icon-popup.generic-cell.html",
	styleUrls: ["./comment-icon-popup.generic-cell.scss"],
})
export class CommentIconPopupGenericCell
	extends GenericCell<any, CommentIconPopupGenericCell.Params, any>
	implements OnInit {
	field: string;
	popupCommentVisible = false;

	textBeforeOpening: string;

	@ViewChild("popupRoot", { static: true })
	popupRoot: ElementRef;

	constructor() {
		super();
	}

	ngOnInit() {
		this.field = this.params.field != null ? this.params.field : "";
	}

	get hasText(): boolean {
		return this.model != null && this.model[this.field] != null && this.model[this.field] !== "";
	}

	openCommentPopup() {
		this.popupCommentVisible = true;
		this.textBeforeOpening = this.model[this.field];

		// Append to body to display on fullscreen instead of inside table cell
		document.body.appendChild(this.popupRoot.nativeElement);
	}

	closeCommentPopup() {
		this.popupCommentVisible = false;

		// Append to body to display on fullscreen instead of inside table cell
		document.body.removeChild(this.popupRoot.nativeElement);
	}

	popupCancel() {
		this.closeCommentPopup();
		this.model[this.field] = this.textBeforeOpening;
		this.outputEvent("popup-action", "cancel");
	}

	popupConfirm() {
		this.closeCommentPopup();
		this.outputEvent("popup-action", "confirm");
	}
}

export namespace CommentIconPopupGenericCell {
	export class Params extends GenericCell.Params {
		field: string;
	}
}
