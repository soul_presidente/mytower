import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ConnectedUserComponent } from "../connectedUserComponent";
import { TranslateService } from "@ngx-translate/core";
import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";
import { ModalService } from "../modal/modal.service";
import { MessageService } from "primeng/api";
import { CreationFormStepBaseComponent } from "../creation-form-step-base/creation-form-step-base.component";
import { OrderStep } from "@app/utils/enumeration";

@Component({
	selector: "app-form-step",
	templateUrl: "./form-step.component.html",
	styleUrls: ["./form-step.component.scss"],
})
export class FormStep extends ConnectedUserComponent implements OnInit {
	isCreation: boolean;
	@Input()
	isTF: boolean = false;

	@Input()
	readOnly: boolean = false;

	@Input()
	showOverview: boolean = true;

	@Input()
	showStepNavigation: boolean = true;

	@Input()
	selectedStep: string;
	@Output()
	selectedStepChange: EventEmitter<string> = new EventEmitter<string>();

	@Input()
	hasContributionAccess: boolean = true;
	@Input()
	displayOptions: boolean = false;

	@Input()
	enableCardStyle: boolean = true;

	stepsOrdered = new Array<string>();
	stepsComponents = new Map<string, any>();
	constructor(
		protected authenticationService: AuthenticationService,
		protected translate?: TranslateService,
		protected router?: Router,
		protected modalService?: ModalService,
		protected messageService?: MessageService
	) {
		super(authenticationService);
	}
	canNavigateToStep(stepCode: string): boolean {
		if (!this.isCreation) return true;

		let steps = this.getVisibleStepsCodeOrdered();
		let result: boolean = undefined;
		steps.forEach((step) => {
			if (step === stepCode && result == undefined) {
				result = true;
			}
			if (!this.stepsComponents.get(step).valid && result == undefined) {
				result = false;
			}
		});

		return result != undefined ? result : true;
	}

	selectStep(stepCode: string) {
		if (this.canNavigateToStep(stepCode)) {
			this.selectedStep = stepCode;
			this.selectedStepChange.emit(this.selectedStep);

			this.postSelectStep(stepCode, true);
		} else {
			this.postSelectStep(stepCode, false);
		}
	}

	postSelectStep(stepCode: string, selectAllowed: boolean) {
		if (!this.stepsComponents.get(this.selectedStep).optionsDisplayed) {
			if (
				this.selectedStep == OrderStep.ORDER &&
				this.stepsComponents.get(this.selectedStep).showAddressPopup
			) {
				this.showOverview = false;
				this.displayOptions = false;
			} else {
				this.showOverview = true;
				this.displayOptions = false;
			}
		} else {
			this.showOverview = false;
			this.displayOptions = true;
		}

		if (!selectAllowed) return;
	}

	selectNextStep() {
		if (!this.selectedStepValidated) {
			this.translate.get("MODAL.FILL_FIELDS").subscribe((text: string) => {
				const title = text.split(";")[0];
				const body = text.split(";")[1];
				this.modalService.error(title, body, null);
			});
			return;
		}
		this.selectStep(this.getNextStep());
	}

	selectPrevStep() {
		this.selectStep(this.getPrevStep());
	}

	clickOptions() {
		this.stepsComponents.get(this.selectedStep).toggleDisplayOptions();
		this.displayOptions = this.stepsComponents.get(this.selectedStep).optionsDisplayed;
		this.showOverview = !this.displayOptions;
	}

	checkGlobalValidity(includeNonVisible = false): boolean {
		let steps: Array<string>;
		if (!includeNonVisible) {
			steps = this.getVisibleStepsCodeOrdered();
		} else {
			steps = this.stepsOrdered;
		}

		let result = true;

		steps.forEach((step) => {
			if (
				this.stepsComponents.get(step) == null ||
				this.stepsComponents.get(step).valid === false
			) {
				result = false;
			}
		});

		return result;
	}

	isStepValidated(stepCode: string): boolean {
		let stepItem = this.stepsComponents.get(stepCode);

		if (!stepItem) return false;
		return stepItem.valid;
	}

	isStepHasOptions(stepCode: string): boolean {
		let stepItem = this.stepsComponents.get(stepCode);

		if (!stepItem) return false;
		return stepItem.hasOptions;
	}

	get selectedStepValidated(): boolean {
		return this.isStepValidated(this.selectedStep);
	}

	get selectedStepHasOptions(): boolean {
		return this.isStepHasOptions(this.selectedStep);
	}
	getVisibleStepsCodeOrdered(): Array<string> {
		let result = new Array<string>();

		this.stepsOrdered.forEach((step) => {
			result.push(step);
		});

		return result;
	}

	isStepEnteredWorkflow(checkedStep: string, includeCurrent: boolean = true): boolean {
		let steps = this.getVisibleStepsCodeOrdered();
		if (checkedStep === this.selectedStep) return includeCurrent;

		let selectedStepAlreadyPassed = false;
		let result: boolean = undefined;

		steps.forEach((step) => {
			if (step === this.selectedStep) {
				selectedStepAlreadyPassed = true;
			}

			if (checkedStep === step && result == undefined) {
				result = !selectedStepAlreadyPassed;
			}
		});

		return result != undefined ? result : false;
	}
	onHideOptionsHandler(event) {
		this.displayOptions = event.displayOptions;
	}
	getPrevStep(): string {
		let steps = this.getVisibleStepsCodeOrdered();
		let selectedStepIndex = steps.indexOf(this.selectedStep);

		let result: string;
		if (selectedStepIndex === 0) {
			result = null;
		} else {
			result = steps[selectedStepIndex - 1];
		}
		return result;
	}

	getNextStep(): string {
		let steps = this.getVisibleStepsCodeOrdered();
		let selectedStepIndex = steps.indexOf(this.selectedStep);

		let result: string;
		if (selectedStepIndex === steps.length - 1) {
			result = null;
		} else {
			result = steps[selectedStepIndex + 1];
		}
		return result;
	}

	get hasPrevStep(): boolean {
		return this.getPrevStep() != null;
	}

	get hasNextStep(): boolean {
		return this.getNextStep() != null;
	}
	ngOnInit() {}
}
