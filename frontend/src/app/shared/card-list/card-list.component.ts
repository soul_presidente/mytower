import { Statique } from "@app/utils/statique";
import {
	Component,
	ContentChild,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
	TemplateRef,
} from "@angular/core";
import { GenericTableInfos } from "../generic-table/generic-table-infos";
import { CardListParams } from "./card-list.params";
import { GenericCell } from "../generic-cell/generic-cell";
import { ForEachItemDirective } from "../../directives/for-each-item.directive";

@Component({
	selector: "app-card-list",
	templateUrl: "./card-list.component.html",
	styleUrls: ["./card-list.component.scss"],
})
export class CardListComponent implements OnInit, OnChanges {
	@ContentChild(ForEachItemDirective, { read: TemplateRef, static: false })
	cardContentTemplate;

	@Input()
	data: Array<any>;
	@Input()
	cols: Array<any>;
	@Input()
	cardActions: Array<any>;
	@Input()
	showCheckbox: boolean;
	@Input()
	img: any;
	@Input()
	showDetails: boolean;
	@Input()
	context: any;
	@Input()
	renderMode: CardListParams.RenderMode;
	@Input()
	genericCellClass: new () => GenericCell<any, any, any>;
	@Input()
	cardHeight: string;
	@Input()
	cardCol: number;
	@Input()
	hideFn: Function;
	@Input()
	dragHandler: any;
	@Input()
	applyFnClass: Function;
	@Input()
	applyClass: string;
	@Input()
	applyListClass: string;

	CARD_HEIGHT_DEFAULT = "150px";
	draggedCard: any = null;
	draggedIndex: number;

	cardItemHeight: number;
	cardItemIndex: number;
	cursorX: number;
	cursorY: number;
	clonedEltToDrag: any;
	clonedEltOffsetX: number;
	clonedEltOffsetY: number;

	@Input()
	allowOrder: boolean = false;
	@Input()
	colMdVignetteForDashboard: string = "";

	@Output()
	showDetailsEmitter: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	orderEndEventEmitter: EventEmitter<any> = new EventEmitter<any>();

	@Output()
	dataChange: EventEmitter<Array<any>> = new EventEmitter<Array<any>>();

	columns: Array<any> = [];

	RenderModeTable = GenericTableInfos.Col.RenderModeTable;
	RenderModeCard = CardListParams.RenderMode;

	isDragging: boolean = false;
	constructor() {}

	ngOnInit() {
		if (this.renderMode == null) {
			this.renderMode = CardListParams.RenderMode.NORMAL;
		}
		if (this.cardHeight == null) {
			this.cardHeight = this.CARD_HEIGHT_DEFAULT;
		}
	}

	public eventSortOptions = {
		onEnd: (e: any) => this.endDraggingElt(e),
		animation: 200,
	};
	endDraggingElt(e) {
		if (e.oldIndex !== e.newIndex) {
			this.data.forEach((elem, i) => {
				i++;
				elem.order = i;
			});
			this.draggedCard = null;
			this.draggedIndex = null;
		}
	}
	ngOnChanges(changes: SimpleChanges) {
		if (this.cols) this.columns = this.cols.sort((it1, it2) => it1.cardOrder - it2.cardOrder);
	}

	showDetailsHandler($event: any, row) {
		if ("BUTTON,I,A".indexOf($($event.target).prop("tagName")) == -1)
			this.showDetailsEmitter.emit(row);
	}

	actionHandler(handler, row, $event) {
		handler(row, $event);
	}
	verifOrder(listCard: Array<any>): Array<any> {
		listCard.forEach((elem, i) => {
			if (elem.order != i + 1) {
				elem.order = i + 1;
			}
		});
		return listCard;
	}
}
