export namespace CardListParams {
	export enum RenderMode {
		NORMAL = "NORMAL",
		GenericCell = "GenericCell",
	}
}
