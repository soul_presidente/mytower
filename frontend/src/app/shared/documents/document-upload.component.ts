import { UploadFile, UploadOutput } from "ngx-uploader";
import { FichierJoint } from "@app/classes/fichiersJoint";
import { Statique } from "@app/utils/statique";
import { UploadComponent } from "@app/utils/uploadComponent";

export class DocumentUploadComponent extends UploadComponent {
	/* documentFiles: Array<Array<UploadFile>> = new Array<Array<UploadFile>>();
	filesReturn: Array<Array<FichierJoint>> = new Array<Array<FichierJoint>>();
	fileTypeDocumentNum: string;
	isFileNotToUpload: Boolean = false; */

	constructor() {
		super();
	}

	// j'ai laissé ce code en commentaire parce qu'on a optimisé le traitement et on a mis sur la méthode
	// onUploadOutput qui sur uploadComponent.ts au cas ou la nouvelle function genere une exception sur l'upload d'un fichier sur les details au niveau TT/pricing/TM

	/* onUploadOutput(output: UploadOutput): void {
		let fileTypeDocument = this.documentFiles[this.fileTypeDocumentNum];

		if (output.file && !this.ngUploaderService.isContentTypeAllowed(output.file.type)) {
			this.componentObject.fileTypeError();
			this.isFileNotToUpload = true;
			output.file.nativeFile = null;
			return;
		}
		// https://stackoverflow.com/a/5515349/6079771
		if (output.file || output.type === "allAddedToQueue") {
			if (output.type !== "allAddedToQueue" && output.file.size > Statique.$3MbToBytes) {
				if (!this.isFileNotToUpload) {
					this.componentObject.fileMaxSizeError();
					this.isFileNotToUpload = true;
					output.file.nativeFile = null;
					return;
				}
			} else {
				this.isFileNotToUpload = false;

				switch (output.type) {
					case "allAddedToQueue":
						this.allAddedToQueue();
						if (this.isFileNotToUpload) {
							this.isFileNotToUpload = false;
						}
						break;
					case "addedToQueue":
						this.addedToQueue(output);
						break;
					case "uploading":
						const index = fileTypeDocument.findIndex((file) => file.id === output.file.id);
						fileTypeDocument[index] = output.file;
						break;
					case "done":
						fileTypeDocument.forEach((file) => {
							this.filesReturn[this.fileTypeDocumentNum] = [
								...this.filesReturn[this.fileTypeDocumentNum],
								...file.response.map((it) => it.fichier),
							];
							this.documentFiles[this.fileTypeDocumentNum] = new Array<UploadFile>();
							if (
								this.componentObject.dataUpload.typeFile == this.UploadDirectory.PRICING_BOOKING
							) {
								this.componentObject.listEbDemandeFichierJointNum.push(
									file.response[0].fichier.ebDemandeFichierJointNum
								);
							} else localStorage.setItem("filesReturn", JSON.stringify(this.filesReturn));

							this.onUploadComplete(file);
						});
						break;
					case "removed":
						fileTypeDocument = fileTypeDocument.filter((file: UploadFile) => file !== output.file);
						break;
					case "dragOver":
						this.dragOver = true;
						break;
					case "dragOut":
						this.dragOver = false;
						break;
					case "drop":
						this.dragOver = false;
						break;
					case "start":
						setTimeout(() => {
							if (this.documentFiles[this.fileTypeDocumentNum].length != 0) {
								if (output.file.response) {
									output.type = "done";
								} else {
									output.type = "start";
								}
								this.onUploadOutput(output);
							}
						}, 500);
						break;
					default:
						break;
				}
			}
		}
	} */

	allAddedToQueue() {
		this.componentObject.dataUpload.fileTypeDemande = this.fileTypeDocumentNum;
		if (this.componentObject.dataUpload.typeFile == this.UploadDirectory.FREIGHT_AUDIT)
			this.emitUploadInputEvent("upload-file-invoice");
		else this.emitUploadInputEvent("upload-file-demande");
	}

	addedToQueue(output: UploadOutput) {
		let fileType = this.documentFiles[this.fileTypeDocumentNum];
		fileType.push(output.file);
	}
}
