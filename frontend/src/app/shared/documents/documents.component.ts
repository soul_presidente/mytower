import { FreightAuditService } from "@app/services/freight-audit.service";
import {
	Component,
	EventEmitter,
	Input,
	OnDestroy,
	OnInit,
	Output,
	QueryList,
	ViewChildren,
	ChangeDetectorRef,
	ViewChild,
} from "@angular/core";
import { AccessRights, Modules, UserRole } from "@app/utils/enumeration";
import { UploadFile, UploadOutput, UploadStatus, NgFileDropDirective } from "ngx-uploader";
import { DocumentUploadComponent } from "@app/shared/documents/document-upload.component";
import { FichierJoint } from "@app/classes/fichiersJoint";
import { StatiqueService } from "@app/services/statique.service";
import { PricingService } from "@app/services/pricing.service";
import { Statique } from "@app/utils/statique";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { EbChat } from "@app/classes/chat";
import { ChatService } from "@app/services/chat.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { TypesDocumentService } from "@app/services/types-document.service";
import { Observable } from "rxjs";
import { EbQmIncident } from "@app/classes/ebIncident";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { HttpClient } from "@angular/common/http";
import { DeliveryOverviewService } from "@app/services/delivery-overview.service";
import { DemandeVisualisationComponent } from "@app/component/pricing-booking/shared-component/demande-visualisation/demande-visualisation.component";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { SearchCriteria } from "@app/utils/searchCriteria";

@Component({
	selector: "app-documents",
	templateUrl: "./documents.component.html",
	styleUrls: ["./documents.component.scss"],
})
export class DocumentsComponent extends DocumentUploadComponent implements OnInit, OnDestroy {
	dataUpload: any;
	listEbDemandeFichierJointNum: Array<number> = new Array<number>();
	documentFiles: Array<Array<UploadFile>> = new Array<Array<UploadFile>>();
	module: number;
	newEbDemandeNum: number;
	ebChat: EbChat;
	ConstantsTranslate: Object;

	csvDownloadLoading = false;
	typesDocument = new Array<EbTypeDocuments>();

	hasContributionAccess?: boolean = null;
	_currentIncidentIndex: number;

	@Input()
	cancelled: boolean = false;
	@Input()
	ebDemandeNum: number;
	@Input()
	ebInvoiceNum: number;
	@Input()
	ebDelLivraisonNum: number;
	@Input()
	ebDelOrderNum: number;

	@Input()
	ebCompagnieNum: number;
	@Input()
	incidents: Array<EbQmIncident>;
	@Input()
	colMd: string = "col-md-12 col-sm-12 col-xs-12";

	@Input()
	fromModuleEvent: boolean = false;
	moduleEventImportedDocument: boolean = false;
	uploadFromEventMoul: boolean = true;
	@Input()
	pslEvent: string;
	@Input()
	multipleUpload: boolean = true;

	@Output()
	newEbDemandeNumRetrieved = new EventEmitter<number>();

	@Output()
	documentsLoadedEmitter = new EventEmitter<FichierJoint>();

	_typeOfDocument: EbTypeDocuments;

	typeDocumentLoaded: boolean = true;
	documentsLoaded: boolean = true;

	@ViewChild("DemandeVisualisationComponent", { static: false })
	demandeVisualisationComponent: DemandeVisualisationComponent;

	@Input()
	set typeOfDocument(doc: EbTypeDocuments) {
		this.typesDocument = new Array<EbTypeDocuments>();
		this._typeOfDocument = doc;
		this.typesDocument.push(this._typeOfDocument);
		this.documentFiles[this._typeOfDocument.code] = new Array<UploadFile>();
		this.filesReturn[this._typeOfDocument.code] = new Array<FichierJoint>();
	}

	@Input()
	set currentIncidentIndex(currentIncidentIndex: number) {
		this._currentIncidentIndex = currentIncidentIndex;
	}

	@Output()
	updateChatEmitter? = new EventEmitter<EbChat>();

	@Output()
	listTypeDocumentsLoaded = new EventEmitter<Array<EbTypeDocuments>>();

	@Output()
	ebFileDocumentEmit = new EventEmitter<FichierJoint>();

	@ViewChildren(NgFileDropDirective)
	fileDropContainers: QueryList<NgFileDropDirective>;

	demandeNum: number;
	visualisationAccessFA: boolean;

	constructor(
		protected statiqueService: StatiqueService,
		private pricingService: PricingService,
		protected modalService: ModalService,
		protected translate: TranslateService,
		protected chatService: ChatService,
		protected typesDocumentService: TypesDocumentService,
		protected qualityManagementService: QualityManagementService,
		protected freightAuditService: FreightAuditService,
		protected deliveryOverviewService: DeliveryOverviewService,
		private router: Router,
		protected cdRef: ChangeDetectorRef,
		protected http: HttpClient,
		private route: ActivatedRoute

	) {
		super();
		this.dataUpload = { typeFile: String(this.UploadDirectory.PRICING_BOOKING) };
		this.componentObject = this;
	}

	ngOnDestroy() {
		// remove the event listener after component Destroys
		// the event listener will be recreated automatically OnInit
		document.removeEventListener("incidentNavigation", this.incidentNavigationEventListener);
	}

	ngOnInit() {
		this.initConstantsTranslate();
		this.getModuleNumFromUrl();
		this.checkContributionAccessRights();

		if (this.ebDemandeNum || this.ebInvoiceNum || this.getEbIncidentNum()) {
			this.retrieveListTypeDocuments();
		}
		this.visualisationAccessFA = this.hasVisualisationAccessFA(this.module);
		this.initIncidentNavigationEventListener();
		this.demandeNum = this.demandeVisualisationComponent
			? this.demandeVisualisationComponent.demandeNum
			: null;
	}

	hasVisualisationAccessFA(module: number){
		if(module == Modules.FREIGHT_AUDIT){
			return !this.hasContribution(module);
		}
		return false;
	}

	onUploadFiles(output: UploadOutput, typeDocument: EbTypeDocuments): void {
		if (this.fromModuleEvent) {
			this.moduleEventImportedDocument = true;
			this.uploadFromEventMoul = false;

			this.componentObject.dataUpload.pslEvent = this.pslEvent;
		}
		this.fileTypeDocumentNum = typeDocument.code;
		this.componentObject.dataUpload.module = this.module;

		this.componentObject.dataUpload.ebDemandeNum = this.ebDemandeNum || this.newEbDemandeNum || 0;

		if (this.incidents != undefined) {
			if (this.incidents[this._currentIncidentIndex] != null) {
				this.componentObject.dataUpload.ebQmIncidentNum = this.incidents[
					this._currentIncidentIndex
				].ebQmIncidentNum;
				this.componentObject.dataUpload.typeFile = this.UploadDirectory.QUALITY_MANAGEMENT;
			}
		}

		if (Statique.isDefined(this.ebInvoiceNum)) {
			this.componentObject.dataUpload.typeFile = this.UploadDirectory.FREIGHT_AUDIT;
			this.componentObject.dataUpload.ebInvoiceNum = this.ebInvoiceNum;
		}

		if (Statique.isDefined(this.ebDelLivraisonNum)) {
			this.componentObject.dataUpload.typeFile = this.UploadDirectory.DELIVERY;
			this.componentObject.dataUpload.ebDelLivraisonNum = this.ebDelLivraisonNum;
		}

		if (Statique.isDefined(this.ebDelOrderNum)) {
			this.componentObject.dataUpload.typeFile = this.UploadDirectory.ORDER;
			this.componentObject.dataUpload.ebDelOrderNum = this.ebDelOrderNum;
		}

		this.componentObject.dataUpload.ebUserNum = this.userConnected.ebUserNum;
		this.componentObject.dataUpload.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.onUploadOutput(output);
	}

	onUploadComplete(file: UploadFile) {
		if (this.newEbDemandeNum == null && this.ebDemandeNum == null && file.response.length > 0) {
			this.newEbDemandeNum = file.response[0].ebDemandeNum;
			this.newEbDemandeNumRetrieved.emit(this.newEbDemandeNum);
		}
		if (this.fromModuleEvent) {
			this.ebFileDocumentEmit.emit(file.response[0].fichier);
		}

		this.updateChatEmitter.emit(
			file.response.length > 0 ? file.response[0].ebChat : file.response.ebChat
		);
	}
	dragOverFunction(element) {
		element.isDragOver = true;
	}
	mouseDragLeave(element) {
		element.isDragOver = false;
	}

	get displayConsolidateDocsBtn() {
		if (!Statique.isDefined(this.typesDocument) || !Statique.isDefined(this.filesReturn))
			return false;
		return this.isDocExistByExtention(["pdf", "docx"]);
	}

	isDocExistByExtention(extentions: Array<String>) {
		let found = false;
		this.typesDocument.forEach((typeDoc) => {
			if (
				Statique.isDefined(this.filesReturn[typeDoc.code]) &&
				this.filesReturn[typeDoc.code].length > 0
			) {
				if (
					this.filesReturn[typeDoc.code].find((doc) =>
						extentions.includes(doc.originFileName.split(".")[1])
					)
				)
					found = true;
			}
		});
		return found;
	}

	retrieveListDocuments() {
		let incidentNum = this.getEbIncidentNum();
		if (!this.fromModuleEvent) {
			if (this.ebDemandeNum) {
				// faut envoyer l id de la demande
				// lors de laffichage
				this.pricingService
					.getListDocumentsByTypeDemande(
						this.module,
						this.ebDemandeNum,
						this.userConnected.ebUserNum
					)
					.subscribe((data) => {
						this.documentsLoadedEmitter.emit(data);
						// get files from type documents
						this.typesDocument.forEach((typeDocument) => {
							if (data[typeDocument.code]) {
								if (!this.fromModuleEvent) {
									this.filesReturn[typeDocument.code] = data[typeDocument.code];
								} else if (this.fromModuleEvent && this.moduleEventImportedDocument) {
									this.filesReturn[typeDocument.code] =
										data[typeDocument.code][data[typeDocument.code].length - 1];
									this.moduleEventImportedDocument = false;
								}
							}
						});
						this.documentsLoaded = true;
					});
			}
		}

		if (incidentNum) {
			this.qualityManagementService
				.getListDocumentsIncident(this.module, incidentNum, this.userConnected.ebUserNum)
				.subscribe((data) => {
					this.typesDocument.forEach((typeDocument) => {
						if (data[typeDocument.code]) {
							this.filesReturn[typeDocument.code] = data[typeDocument.code];
						}
					});
					this.documentsLoaded = true;
				});
		}

		if (this.ebDelLivraisonNum) {
			this.deliveryOverviewService
				.getListDocumentsDelivery(this.module, this.ebDelLivraisonNum, this.userConnected.ebUserNum)
				.subscribe((data) => {
					this.documentsLoaded = true;
					this.typesDocument.forEach((typeDocument) => {
						if (data[typeDocument.code]) {
							this.filesReturn[typeDocument.code] = data[typeDocument.code];
						}
					});
				});
			this.cdRef.detectChanges();
		} else this.documentsLoaded = true;
	}

	getListDocFromIncident() {
		return new Promise<void>((resolve) => {
			let incidentNum = this.getEbIncidentNum();
			if (incidentNum) {
				this.qualityManagementService.getListDoc(incidentNum).subscribe((data) => {
					this.documentFiles = new Array<Array<UploadFile>>();
					this.filesReturn = {};
					if (data == null || data.listTypeDocuments == null) {
						this.typesDocument = new Array<EbTypeDocuments>();
					} else if (data && data.listTypeDocuments) {
						data.listTypeDocuments.forEach((typeDocument) => {
							this.documentFiles[typeDocument.code] = new Array<UploadFile>();
							this.filesReturn[typeDocument.code] = new Array<FichierJoint>();
							this.typesDocument.push(typeDocument);
						});
					}
				});
			}
			resolve();
		});
	}

	getInvoiceDocTypes() {
		let ebCompanieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		return new Promise<void>((resolve) => {
			this.typesDocumentService
				.getInvoiceTypeDocs(this.module, this.ebInvoiceNum, ebCompanieNum)
				.subscribe((data) => {
					this.documentFiles = new Array<Array<UploadFile>>();
					this.filesReturn = {};
					this.typesDocument = new Array<EbTypeDocuments>();
					data.sort((it1, it2) => it1.code - it2.code);

					data.forEach((doc) => {
						this.documentFiles[doc.code] = new Array<UploadFile>();
						this.filesReturn[doc.code] = new Array<FichierJoint>();
						this.typesDocument.push(doc);
					});

					this.listTypeDocumentsLoaded.emit(this.typesDocument);
					resolve();
				});
		});
	}

	async retrieveListTypeDocuments() {
		this.documentsLoaded = false;
		if (!this.fromModuleEvent) {
			this.typeDocumentLoaded = false;
			let incidentNum = this.getEbIncidentNum();
			var requestService: Observable<any>;
			if (incidentNum) await this.getListDocFromIncident();

			if (this.ebDemandeNum) this.typesDocument = new Array<EbTypeDocuments>();

			if (this.ebDemandeNum && (this.typesDocument.length == 0 || this.typesDocument == null)) {
				if (Statique.isDefined(this.module))
					requestService = this.typesDocumentService.getListTypeDocumentFinal(
						this.module,
						this.ebDemandeNum,
						this.ebCompagnieNum
					);
				if (requestService) {
					requestService.subscribe((data) => {
						this.documentFiles = new Array<Array<UploadFile>>();
						this.filesReturn = {};
						this.typesDocument = new Array<EbTypeDocuments>();
						data.sort((it1, it2) => it1.code - it2.code);

						data.forEach((doc) => {
							this.documentFiles[doc.code] = new Array<UploadFile>();
							this.filesReturn[doc.code] = new Array<FichierJoint>();
							this.typesDocument.push(doc);
						});
						this.typeDocumentLoaded = true;
						this.listTypeDocumentsLoaded.emit(this.typesDocument);
					});
				}
			}
		}

		this.retrieveListDocuments();
	}

	private getEbIncidentNum(): number {
		let ebIncidentNum = null;
		if (this.module == Modules.QUALITY_MANAGEMENT) {
			ebIncidentNum = this.incidents[this._currentIncidentIndex].ebQmIncidentNum;
		}
		return ebIncidentNum;
	}

	deleteDocument(doc: FichierJoint, index: number, listDoc: Array<FichierJoint>) {
		doc.xEbUser = null;
		this.modalService.confirm(
			this.ConstantsTranslate["CONFIRMATION"],
			this.ConstantsTranslate["DELETE_FILE_CONFIRMATION"],
			function() {
				this.componentObject.pricingService.deleteDocument(doc).subscribe(
					function(it) {
						listDoc.splice(index, 1);
						this.componentObject.updateChatEmitter.emit(it.ebChat);
						if (this.fromModuleEvent) {
							this.uploadFromEventMoul = true;
						}
					}.bind(this)
				);
			}.bind(this)
		);
	}

	getUrlfile(chemin: string) {
		const realUrl = Statique.getUrlFile(chemin);
		return this.http.get(realUrl, { responseType: "blob" }).subscribe((data) => {
			this.downLoadFile(data);
		});
	}

	downLoadFile(data: any) {
		let url = window.URL.createObjectURL(data);
		let pwa = window.open(url);
		if (!pwa || pwa.closed || typeof pwa.closed == "undefined") {
			alert("Please disable your Pop-up blocker and try again.");
		}
	}

	UserHaveDeleteRight(xEbEtablissemnt) {
		return (
			this.userConnected.ebEtablissement.ebEtablissementNum == xEbEtablissemnt ||
			this.userConnected.role == UserRole.CONTROL_TOWER
		);
	}

	private initConstantsTranslate() {
		this.ConstantsTranslate = {
			CONFIRMATION: "",
			DELETE_FILE_CONFIRMATION: "",
			INTERCO_INVOICE: "",
			COMMERCIAL_INVOICE: "",
			CUSTOMS: "",
			TRANSPORT_DOCUMENT: "",
			POD: "",
			OTHER: "",
		};
		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate
				.get("PRICING_BOOKING." + key)
				.subscribe((res) => (this.ConstantsTranslate[key] = res));
		});
	}

	private checkContributionAccessRights() {
		// contribution access rights to module
		if (this.userConnected.listAccessRights) {
			this.hasContributionAccess =
				this.userConnected.listAccessRights.find((access) => {
					return (
						access.ecModuleNum == this.module && access.accessRight == AccessRights.CONTRIBUTION
					);
				}) != undefined;
		}
	}

	private getModuleNumFromUrl() {
		this.module = Statique.getModuleNumFromUrl(this.router.url);
	}

	// Incident event handler
	protected incidentNavigationEventListener(e) {
		e.preventDefault();
		e.stopPropagation();
		if (this.ebDemandeNum) {
			this.componentObject.retrieveListTypeDocuments();
		}
	}

	private initIncidentNavigationEventListener() {
		// get list Documents for the first time,
		// then initialize the eventListener
		if (this.ebDemandeNum) {
			this.retrieveListTypeDocuments();
		}

		document.addEventListener("incidentNavigation", this.incidentNavigationEventListener, false);
	}

	/**
	 * Fire upload from TypeScript
	 */
	public uploadFile(blob: Blob, filename: string, ebTypeDocumentsNum: number) {
		let fileType = "";
		if (filename.endsWith(".pdf")) fileType = "application/pdf";
		else if (filename.endsWith(".docx"))
			fileType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

		let file: any;
		if (!Statique.isIE11) {
			file = new File([blob], filename, {
				type: fileType,
			});
		} else {
			// IE 11 doesn't support File constructor
			file = blob;
			file.name = filename;
			file.lastModifiedDate = new Date();
			Object.defineProperty(file, "type", {
				value: fileType,
			});
		}

		let dropContainer = this.fileDropContainers.filter(
			(c) => c.el.id === "fileDropContainer-" + ebTypeDocumentsNum
		)[0];

		let event: any = document.createEvent("MouseEvent");
		event.dataTransfer = {
			files: [file],
		};

		dropContainer.onDrop(event);
	}
	toggleHoverDragTextArea(event, classHover) {
		Statique.toggleClassOnHover(event, classHover);
	}
	consolidateDoc() {
		let requestProcessing = new RequestProcessing();
		let criteria = new SearchCriteria();
		criteria.ebDemandeNum = this.ebDemandeNum ? this.ebDemandeNum : this.ebInvoiceNum;
		criteria.listModule.push(this.module);
		this.csvDownloadLoading = true;
		this.statiqueService.consolidateDoc(criteria).subscribe(
			(res) => {
				this.csvDownloadLoading = false;
				requestProcessing.afterGetResponse(event);
				const blob = new Blob([res], { type: "application/octet-stream" });
				const url = window.URL.createObjectURL(blob);

				let a: any = document.createElement("A");
				a.href = url;
				a.download = "Liasse documentaire.pdf";
				document.body.appendChild(a);
				a.click();
				document.body.removeChild(a);
			},
			(err) => {
				requestProcessing.afterGetResponse(event);
			}
		);
	}
}
