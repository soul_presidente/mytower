import {
	ChangeDetectorRef,
	Component,
	EventEmitter,
	forwardRef,
	Input,
	OnInit,
	Output,
	TemplateRef,
	ContentChild,
} from "@angular/core";
import { switchMap } from "rxjs/operators";
import { StatiqueService } from "@app/services/statique.service";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { ForEachItemDirective } from "../../directives/for-each-item.directive";

@Component({
	selector: "app-autocomplete",
	templateUrl: "./autocomplete.component.html",
	styleUrls: [],
})
export class AutocompleteComponent implements OnInit {
	@ContentChild(ForEachItemDirective, { read: TemplateRef, static: false })
	itemContentTemplate;

	@Input()
	name: string;
	@Input()
	isRequired: boolean;
	@Input()
	inputModel: any;
	@Input()
	selectedData: any;
	@Input()
	disabled: boolean = false;
	@Output()
	inputModelChange = new EventEmitter<any>();

	@Input()
	sMultiple: string;
	@Input()
	placeholder: string;
	@Input()
	srcTypeahead: string;
	@Input()
	srcListItems: string;
	@Input()
	defaultValue: any;
	@Input()
	bindLabel: string;
	@Input()
	bindValue: any;
	@Input()
	options: any;
	@Input()
	listItems: any;
	@Input()
	groupByAttr?: string;
	@Input()
	fetchGroupByAttribute?: string;
	@Input()
	bindObject?: boolean;
	@Input()
	searchCriteria?: SearchCriteria;
	@Input()
	useTemplate?: boolean;
	@Input()
	searchFn?: any;
	@Input()
	appendTo = null;

	public multiple: boolean;
	public listTermsTypeahead: any;
	public typeaheadEvent: EventEmitter<String> = new EventEmitter<string>();

	constructor(protected cd: ChangeDetectorRef, private statiqueService: StatiqueService) {}

	ngOnInit() {
		this.multiple = this.sMultiple == "true";

		if (this.defaultValue) {
			if (this.multiple) this.inputModel = JSON.parse(this.defaultValue);
			else
				this.inputModel =
					this.defaultValue.length > 0 && this.defaultValue.indexOf("[") >= 0
						? JSON.parse(this.defaultValue)[0]
						: +this.defaultValue;
		}

		if (!this.defaultValue && this.srcTypeahead) this.srcListItems = null;

		this.getListItems();

		this.setTypeahead();
	}

	getListItems() {
		if (this.srcListItems) {
			this.getListFromSource(this.srcListItems).subscribe(
				function(res) {
					this.listItems = res;
				}.bind(this)
			);

			return true;
		} else if (this.listItems) return true;

		return false;
	}

	setTypeahead() {
		if (!this.srcTypeahead) return false;

		this.typeaheadEvent
			.pipe(switchMap((term) => this.getTermsFromSource(term, this.srcTypeahead)))
			.subscribe(
				(data) => {
					this.cd.markForCheck();
					this.listItems = data;
				},
				(err) => {
					this.listItems = [];
				}
			);

		return true;
	}

	getListFromSource(apiUrl) {
		return this.statiqueService.getListFromSource(apiUrl);
	}
	getTermsFromSource(term, apiUrl) {
		if (term == "" || !term) term = "*";
		if (this.searchCriteria) {
			this.searchCriteria.searchterm = term;
			return this.statiqueService.getTermsFromSourceByCriteria(apiUrl, this.searchCriteria);
		} else {
			return this.statiqueService.getTermsFromSource(term, apiUrl);
		}
	}

	updateChanges($event: any) {
		this.inputModel = $event;
		this.selectedData = $event;

		if (!this.bindObject) {
			this.inputModelChange.emit($event);
		} else {
			let val, obj;
			if (this.multiple && $event && $event.length) {
				val = $event.map((it) => (this.bindObject ? it[this.bindValue] : it));
				obj =
					val && val.length && this.listItems
						? this.listItems.filter((it) => val.find((el) => el == it[this.bindValue]))
						: null;
			} else if (!this.multiple) {
				val = this.bindObject && $event ? $event[this.bindValue] : $event;
				obj = this.listItems ? this.listItems.find((it) => it[this.bindValue] == val) : null;
			}
			this.inputModelChange.emit(obj);
		}

		this.cd.detectChanges();
	}
}
