import { Component, OnInit, Input } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-vignette-tracing",
	templateUrl: "./vignette-tracing.component.html",
	styleUrls: ["./vignette-tracing.component.scss"],
})
export class VignetteTracingComponent implements OnInit {
	@Input()
	tracing: any;

	infosTransport: any;

	constructor(protected translate: TranslateService) {}

	ngOnInit() {
		if (this.tracing) {
			this.infosTransport = {
				origin: {
					city: this.tracing.libelleOriginCity,
					country: this.tracing.xEcCountryOrigin ? this.tracing.xEcCountryOrigin.libelle : "",
				},
				xEcModeTransport: this.tracing.xEcModeTransport,
				destination: {
					city: this.tracing.libelleDestCity,
					country: this.tracing.xEcCountryDestination
						? this.tracing.xEcCountryDestination.libelle
						: "",
				},
			};
		}
	}
}
