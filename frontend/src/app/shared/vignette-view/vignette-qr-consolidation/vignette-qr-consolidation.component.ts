import {
	Component,
	OnInit,
	Input,
	Output,
	EventEmitter,
	OnChanges,
	SimpleChanges,
} from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { TranslateService } from "@ngx-translate/core";
import { Statique } from "@app/utils/statique";
import { EbDemande } from "@app/classes/demande";
import { Modules, Ventilation } from "@app/utils/enumeration";
import { EbQrGroupeProposition } from "@app/classes/qrGroupeProposition";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EtablissementService } from "@app/services/etablissement.service";

@Component({
	selector: "app-vignette-qr-consolidation",
	templateUrl: "./vignette-qr-consolidation.component.html",
	styleUrls: ["./vignette-qr-consolidation.component.scss"],
})
export class VignetteQrConsolidationComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	card: any;

	@Input()
	demande: EbDemande;

	@Output()
	cardEvent: EventEmitter<any> = new EventEmitter<any>();

	@Input()
	listEbDemande: EbQrGroupeProposition;

	listSelectedDemandeNumArray: Array<any>;

	selectedDemandeNumber: number;

	infosTransport: any;
	Statique = Statique;
	Ventilation = Ventilation;
	detailDemandeRoute: string;

	@Input()
	listExchangeRate: Array<EbCompagnieCurrency>;

	@Input()
	message: String;

	@Input()
	converted: Array<number>;

	constructor(
		protected translate: TranslateService,
		protected etablissementService: EtablissementService
	) {
		super();
	}

	ngOnInit() {
		this.getListSelectedDemandeNumber();
		this.detailDemandeRoute = Statique.routeDetailDemandeInPricing();

		if (this.card) {
			this.infosTransport = {
				origin: {
					city: this.card.ebPartyOrigin ? this.card.ebPartyOrigin.city : null,
					country:
						this.card.ebPartyOrigin && this.card.ebPartyOrigin.xEcCountry
							? this.card.ebPartyOrigin.xEcCountry.libelle
							: null,
				},
				xEcModeTransport: this.card.xEcModeTransport,
				icon: this.card.xEcModeTransport,
				destination: {
					city: this.card.ebPartyDest ? this.card.ebPartyDest.city : null,
					country:
						this.card.ebPartyDest && this.card.ebPartyDest.xEcCountry
							? this.card.ebPartyDest.xEcCountry.libelle
							: null,
				},
			};
		}
	}

	onCheckDemande(demande: EbDemande) {
		this.cardEvent.emit(demande);
	}
	ngOnChanges(changes: SimpleChanges) {
		if (
			changes["this.listEbDemande.listSelectedDemandeNum"] &&
			changes["this.listEbDemande.listSelectedDemandeNum"].previousValue !=
				changes["this.listEbDemande.listSelectedDemandeNum"].currentValue
		)
			this.getListSelectedDemandeNumber();
	}

	getListSelectedDemandeNumber() {
		this.listSelectedDemandeNumArray = this.listEbDemande.listSelectedDemandeNum.split(":");
		this.selectedDemandeNumber = (this.listSelectedDemandeNumArray.length - 1) / 2;
	}

	convert(item) {
		if (this.converted.indexOf(item) >= 0) {
			return true;
		} else {
			return false;
		}
	}
}
