import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Modules } from "@app/utils/enumeration";
import { TranslateService } from "@ngx-translate/core";
import { EbCptmProcedureDTO } from "@app/classes/cptm/EbCptmProcedureDTO";
import { Router } from "@angular/router";

@Component({
	selector: "app-vignette-cptm",
	templateUrl: "./vignette-cptm.component.html",
	styleUrls: ["./vignette-cptm.component.scss"],
})
export class VignetteCptmComponent implements OnInit {
	@Input()
	procedure: any;

	@Input()
	module: number;

	@Output()
	generateMailEvent: EventEmitter<{
		value: EbCptmProcedureDTO;
		mailId: number;
	}> = new EventEmitter<{
		value: EbCptmProcedureDTO;
		mailId: number;
	}>();

	infosTransport: any;
	Modules = Modules;

	constructor(protected translate: TranslateService, protected router: Router) {}

	ngOnInit() {}

	generateMail(value, mailId) {
		this.generateMailEvent.emit({ value, mailId });
	}
}
