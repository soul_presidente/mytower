import { Component, OnInit, Input } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { Statique } from "@app/utils/statique";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";

@Component({
	selector: "app-vignette-favoris",
	templateUrl: "./vignette-favoris.component.html",
	styleUrls: ["./vignette-favoris.component.scss"],
})
export class VignetteFavorisComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	card: any;

	infosTransport: any;
	Statique = Statique;

	constructor(protected translate: TranslateService) {
		super();
	}

	ngOnInit() {
		if (this.card) {
			this.infosTransport = {
				origin: {
					city: this.card.origin ? this.card.origin.city : null,
					country: this.card.origin ? this.card.origin.country : null,
				},
				xEcModeTransport: this.card.xEcModeTransport,
				icon: this.card.xEcModeTransport,
				destination: {
					city: this.card.destination ? this.card.destination.city : null,
					country: this.card.destination ? this.card.destination.country : null,
				},
			};
		}
	}
}
