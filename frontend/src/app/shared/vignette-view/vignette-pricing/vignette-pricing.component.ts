import { Component, OnInit, Input } from "@angular/core";
import { Modules } from "@app/utils/enumeration";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-vignette-pricing",
	templateUrl: "./vignette-pricing.component.html",
	styleUrls: ["./vignette-pricing.component.scss"],
})
export class VignettePricingComponent implements OnInit {
	@Input()
	demande: any;

	@Input()
	module: number;

	infosTransport: any;
	Modules = Modules;

	constructor(protected translate: TranslateService) {}

	ngOnInit() {
		if (this.demande) {
			this.infosTransport = {
				origin: {
					city: this.demande.libelleOriginCity,
					country: this.demande.libelleOriginCountry,
				},
				xEcModeTransport: this.demande.xEcModeTransport,
				destination: {
					city: this.demande.libelleDestCity,
					country: this.demande.libelleDestCountry,
				},
			};
		}
	}
}
