import { Component, OnInit, Input } from "@angular/core";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbModeTransport } from "@app/classes/EbModeTransport";

@Component({
	selector: "app-vignette-header",
	templateUrl: "./vignette-header.component.html",
	styleUrls: ["./vignette-header.component.scss"],
})
export class VignetteHeaderComponent implements OnInit {
	@Input()
	infosTransport: any;
	@Input()
	isSmallerSize: boolean = false;

	listModeTransport: Array<EbModeTransport>;

	constructor() {}

	ngOnInit() {
		this.initStatiques();
	}

	async initStatiques() {
		this.listModeTransport = await SingletonStatique.getListModeTransport();
	}

	getModeTransportIcon(xEcModeTransport: number) {
		let icon = "";
		if (xEcModeTransport) {
			icon = SingletonStatique.getIconModeTransportById(xEcModeTransport);
		}
		return icon;
	}
}
