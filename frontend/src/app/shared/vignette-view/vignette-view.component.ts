import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Modules } from "@app/utils/enumeration";
import { EbDemande } from "@app/classes/demande";
import { EbQrGroupeProposition } from "@app/classes/qrGroupeProposition";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";

@Component({
	selector: "app-vignette-view",
	templateUrl: "./vignette-view.component.html",
	styleUrls: ["./vignette-view.component.scss"],
})
export class VignetteViewComponent implements OnInit {
	@Input()
	card: any;

	@Input()
	demande: EbDemande;

	@Input()
	module: number;

	@Input()
	isFavoris: boolean = false;

	@Input()
	isQrConsolidation: boolean = false;
	@Output()
	generateMailEvent: EventEmitter<number> = new EventEmitter<number>();

	@Output()
	cardEvent: EventEmitter<any> = new EventEmitter<any>();
	Modules = Modules;

	@Input()
	listEbDemande: EbQrGroupeProposition;

	@Input()
	listExchangeRate: Array<EbCompagnieCurrency>;

	@Input()
	message: String;

	@Input()
	converted: Array<number>;

	constructor() {}

	ngOnInit() {}

	generateMailEventHandler(value) {
		this.generateMailEvent.emit(value);
	}

	cardChange(event) {
		this.cardEvent.emit(event);
	}
}
