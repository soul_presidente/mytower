import { Directive } from "@angular/core";

@Directive({
	selector: "[forEachItem]",
})
export class ForEachItemDirective {}
