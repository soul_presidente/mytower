import { ModuleWithProviders, Component } from "@angular/core";
import { RouterModule, Routes, PreloadAllModules } from "@angular/router";
import { CorpRoutes } from "@app/corp/corp.routes";
import { HomeComponent } from "@app/corp/home/home.component";
import { PricingMaskFillerComponent } from "@app/component/saved-forms/pricing-mask/pricing-mask-filler.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";
import { RedirectDossier } from "@app/corp/redirection-dossier-inexistant/redirection-dossier-inexistant.component";
import { DocumentComponent } from "@app/component/documents/document.component";
import { accountRoutes } from "@app/component/account/account.routes";

// prettier-ignore
export const APP_ROUTES: Routes = [
	...CorpRoutes,
	...accountRoutes,
	{ path: "", redirectTo: "/accueil", pathMatch: "full" },
	{ path: "accueil", component: HomeComponent, canActivate: [UserAuthGuardService] },

	{
		path: "app/user",
		loadChildren: () => import('../app/component/user/user.module').then(m => m.UserModule),
	},
	{
		path: "app/pricing",
		loadChildren: () => import('../app/component/pricing-booking/pricing/pricing.module').then(m => m.PricingModule),
	},
	{
		path: "public/pricing",
		loadChildren: () => import('../app/component/pricing-booking/shared-component/details-public/details-public.module').then(m => m.DetailsPublicModule),
	},
	{
		path: "app/transport-management",
		loadChildren: () => import('../app/component/pricing-booking/transport-management/transport-management.module').then(m => m.TransportManagementgModule),
	},
	{
		path: "app/quality-management",
		loadChildren: () => import('../app/component/quality-management/quality-management.module').then(m => m.QualityManagementModule),
	},
	{
		path: "app/compliance-matrix",
		loadChildren: () => import('../app/component/compliance-matrix/compliance-matrix.module').then(m => m.ComplianceMatrixModule),
	},
	{
		path: "app/freight-audit",
		loadChildren: () => import('../app/component/freight-audit/freight-audit.module').then(m => m.FreightAuditModule),
	},
	{
		path: "app/track-trace",
		loadChildren: () => import('../app/component/track-trace/track-trace.module').then(m => m.TrackTraceModule),
	},
	{
		path: "app/custom-declaration",
		loadChildren: () => import('../app/component/douane/douane.module').then(m => m.DouaneModule),
	},
	{
		path: "app/dock-management",
		loadChildren: () => import('../app/component/dock-management/dock-management.module').then(m => m.dockManagementModule),
	},
	{
		path: "app/custom",
		loadChildren: () => import('../app/component/douane/douane.module').then(m => m.DouaneModule),
	},
	{
		path: "squelette",
		loadChildren: () => import('../app/component/squelette/squelette.module').then(m => m.SqueletteModule),
	},
	{
		path: "app/freight-analytics",
		loadChildren: () => import('../app/component/freight-analytics/freight-analytics.module').then(m => m.FreightAnalyticsModule),
	},
	{
		path: "app/receipt-scheduling",
		loadChildren: () => import('../app/component/receipt-scheduling/receipt-scheduling.module').then(m => m.ReceiptSchedulingModule),
	},
	{
		path: "app/delivery-overview",
		loadChildren: () => import('../app/component/delivery-overview/delivery-overview.module').then(m => m.DeliveryOverviewModule),
	},
	{
		path: "failedaccess/:variableGeneriqueNum/:variableModule",
		component: RedirectDossier,
	},
	{
		path: "app/type-document",
		loadChildren: () => import('../app/component/type-document/type-document.module').then(m => m.TypeDocumentModule),
	},

	{ path: "app/documents", component: DocumentComponent },
	{ path: "formsearch", component: PricingMaskFillerComponent },
	{ path: "**", redirectTo: "/accueil", pathMatch: "full" },
];

export const appRouter: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES, {
	preloadingStrategy: PreloadAllModules,
});
