import { Component, OnInit } from "@angular/core";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "corrective-action",
	templateUrl: "./corrective-action.component.html",
	styleUrls: ["./corrective-action.component.css"],
})
export class CorrectiveActionComponent implements OnInit {
	listCorrectiveAction = [];
	colonnes: any[];

	ConstantsTranslate: Object = {
		ACTION: "",
		TYPEOFACTION: "",
		BUDGET: "",
		ACTUALSTARTDATE: "",
		ACTUALENDDATE: "",
		LINKEDINCIDENT: "",
	};

	constructor(
		private qualityManagementService: QualityManagementService,
		private translate?: TranslateService
	) {}

	ngOnInit() {
		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate
				.get("QUALITY_MANAGEMENT." + key)
				.subscribe((res) => (this.ConstantsTranslate[key] = res));
		});

		this.colonnes = [
			{ field: "action", header: this.ConstantsTranslate["ACTION"] },
			{ field: "LinkedIncident", header: this.ConstantsTranslate["LINKEDINCIDENT"] },
			{ field: "typeOfAction", header: this.ConstantsTranslate["TYPEOFACTION"] },
			{ field: "budget", header: this.ConstantsTranslate["BUDGET"] },
			{ field: "ActualStartDate", header: this.ConstantsTranslate["ACTUALSTARTDATE"] },
			{ field: "ActualEndtDate", header: this.ConstantsTranslate["ACTUALENDDATE"] },
		];

		this.listCorrectiveAction = [
			{
				action: "Prepare new units",
				LinkedIncident: "QM1-AFH-C04054",
				typeOfAction: "Corrective",
				budget: 0,
				ActualStartDate: "17/02/2019",
				ActualEndtDate: "17/02/2019",
			},
			{
				action: "Resend Unit",
				LinkedIncident: "QM2-AFH-C04055",
				typeOfAction: "Corrective",
				budget: 1000,
				ActualStartDate: "",
				ActualEndtDate: "",
			},
			{
				action: "No action required",
				LinkedIncident: "QM3-AFH-C04056",
				typeOfAction: "",
				budget: 0,
				ActualStartDate: "",
				ActualEndtDate: "",
			},
			{
				action: "Return Unit",
				LinkedIncident: "QM4-AFH-C04057",
				typeOfAction: "Corrective",
				budget: 1200,
				ActualStartDate: "15/08/2018",
				ActualEndtDate: "17/08/2018",
			},
		];
	}
}
