import { ChangeDetectorRef, Component, Input, OnInit } from "@angular/core";
import { EbQmIncident } from "@app/classes/ebIncident";
import { StatiqueService } from "@app/services/statique.service";
import { EcCountry } from "@app/classes/country";
import { Statique } from "@app/utils/statique";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { SearchCriteriaQualityManagement } from "@app/utils/SearchCriteriaQualityManagement";
import { EcQmRootCause } from "@app/classes/rootCause";
import { EcQmCategory } from "@app/classes/category";
import { EcCurrency } from "@app/classes/currency";
import { EbCategorie } from "@app/classes/categorie";
import { IncidentLine } from "@app/classes/incidentLine";
import { EbMarchandise } from "@app/classes/marchandise";
import { Message } from "primeng/components/common/api";
import { MessageService } from "primeng/api";
import SingletonStatique from "@app/utils/SingletonStatique";
import { TypeOfEvent } from '@app/utils/enumeration';

@Component({
	selector: "app-incident-information",
	templateUrl: "./incident-information.component.html",
	styleUrls: ["./incident-information.component.css"],
})
export class IncidentInformationComponent extends ConnectedUserComponent implements OnInit {
	statique = Statique;
	yesOrNo: any = Statique.yesOrNo;

	listDestinations: Array<EcCountry>;
	listRootCause: Array<EcQmRootCause>;
	listIncidentCategory: Array<EcQmCategory>;
	@Input()
	incident: EbQmIncident;

	msgs: Message[] = [];
	totalUnits: number;
	criteria: SearchCriteriaQualityManagement = new SearchCriteriaQualityManagement();
	category: EcQmCategory = new EcQmCategory();
	listCurrency: Array<EcCurrency>;
	isWorking: boolean = false;
	listMarchandises: EbMarchandise[] = [];

	delayRequired: boolean = false;
	unitDamagedRequired: boolean = false;
	unitLostRequired: boolean = false;
	docRequired: boolean = false;
	selectedUnit: number;
	totalQteOfUnit: number = 0;
	totalWeight: number = 0;
	marchandiseTypeOfUnit: any = Statique.MarchandiseTypeOfUnit;
	marchandiseDangerousGood: Map<number, string>;

	showImpact: boolean = false;
	showDesc: boolean = false;

	constructor(
		private statiqueService: StatiqueService,
		private messageService: MessageService,
		private qualityManagementService: QualityManagementService,
		private cd: ChangeDetectorRef
	) {
		super();
	}

	ngOnInit() {
		this.totalQteOfUnit = 0;
		this.totalWeight = 0;
		this.incident.incidentLines.forEach((line) => {
			this.totalQteOfUnit += line.numberOfUnits;
			this.totalWeight += line.weight;
		});

		(async () => (this.listDestinations = await SingletonStatique.getListEcCountry()))();

		(async () => {
			this.listCurrency = await SingletonStatique.getListEcCurrency();
			// this.listCurrency.forEach(currency => {
			// if (currency.libelle == this.incident.currencyLabel) {
			//this.incident.currency = currency;
			// }
			// });
		})();

		this.qualityManagementService.listRootCause().subscribe((data) => {
			this.listRootCause = data;
		});

		this.qualityManagementService.listIncidentCategory().subscribe((data) => {
			this.listIncidentCategory = data;
			// data.forEach(categorie => {
			//   if (categorie.label == this.incident.categoryLabel) {
			//     this.incident.category = categorie;
			//   }
			//});
		});
		this.fillListMarchandiseDangerousGoods();
	}

	addNewIncidentLine() {
		this.criteria.searchterm = this.incident.incidentRef.substring(4).toLocaleLowerCase();
		this.isWorking = true;
		this.criteria.marchandiseIdNumbers = [];
		this.incident.incidentLines.forEach((line) => {
			this.criteria.marchandiseIdNumbers.push(line.ebMarchandiseNum);
		});

		this.qualityManagementService.listMarchandise(this.criteria).subscribe((data) => {
			if (data.length > 0) {
				this.listMarchandises = data;
				let incidentLine = new IncidentLine(new EbMarchandise());
				this.incident.incidentLines.push(incidentLine);
			} else {
				let header = "Action impossible",
					text = "Toutes les marchandises ont déjà été selectionné";
				this.messageService.add({ severity: "info", summary: header, detail: text });
			}
			this.isWorking = false;
		});
	}

	showOrHide(target) {
		this["show" + target] = !this["show" + target];
	}

	affectInsurance() {
		this.incident.insurance = this.incident ? 1 : 0;
	}

	changeCategory(category) {
		let qmCategorieNum = category.qmCategorieNum;
		this.incident.categoryLabel = category.label;
		this.delayRequired = false;
		this.unitLostRequired = false;
		this.unitDamagedRequired = false;
		if (qmCategorieNum == 2) {
			this.delayRequired = true;
		}
		if (qmCategorieNum == 3) {
		}
		if (qmCategorieNum == 4) {
		}
		if (qmCategorieNum == 5) {
			this.docRequired = true;
		}
		if (qmCategorieNum == 6) {
			this.unitDamagedRequired = true;
			this.unitLostRequired = true;
			this.docRequired = true;
		}
		if (qmCategorieNum == 7) {
		}
		if (qmCategorieNum == 8) {
		}
		if (qmCategorieNum == 9) {
			this.unitDamagedRequired = true;
			this.unitLostRequired = true;
		}
		if (qmCategorieNum == 11) {
			this.delayRequired = true;
		}
		if (qmCategorieNum == 12) {
			this.delayRequired = true;
		}
		if (qmCategorieNum == 13) {
			this.delayRequired = true;
		}
	}

	deleteIncident(index) {
		this.incident.incidentLines.splice(index, 1);
	}

	byId(country1: EcCountry, country2: EcCountry) {
		if (country2) {
			return country1.ecCountryNum == country2.ecCountryNum;
		}
		return false;
	}

	sommeUnits() {
		if (!this.incident.incidentLines) return 0;
		return this.incident.incidentLines.reduce((sum: number, it: IncidentLine) => {
			return sum + it.numberOfUnits;
		}, 0);
	}

	sommeWeight() {
		if (!this.incident.incidentLines) return 0;
		return this.incident.incidentLines.reduce((sum: number, it: IncidentLine) => {
			return sum + it.weight;
		}, 0);
	}

	save(event: any) {}

	categoryById(category1: EbCategorie, category2: EbCategorie) {
		if (category2) {
			return category1.ebCategorieNum == category2.ebCategorieNum;
		}
		return false;
	}

	onMarchandiseNumChange(index, unitNum: number) {
		let marchandise = this.listMarchandises.find((el) => el.ebMarchandiseNum == unitNum);
		let incidentLine: IncidentLine = new IncidentLine(marchandise);
		incidentLine.carrierName = this.incident.carrierNom;
		this.incident.incidentLines[index] = incidentLine;
	}

	updateIncident() {
		let Header = "Incident information",
			messageText = "informations modifiiées avec succès";
		console.log(this.incident);
		this.qualityManagementService.updateIncident(this.incident).subscribe((data) => {
			console.log(data);
			this.messageService.add({ severity: "success", summary: Header, detail: messageText });
		});
	}

	changeTotalQte(qte) {
		this.totalQteOfUnit = 0;
		this.incident.incidentLines.forEach((line) => {
			this.totalQteOfUnit += line.numberOfUnits;
		});
	}

	changeTotalWe(weight) {
		this.totalWeight = 0;
		this.incident.incidentLines.forEach((line) => {
			this.totalWeight += line.weight;
		});
	}

	// cette methode recupére la liste des valeurs de l'enumération depuis le backend
	async fillListMarchandiseDangerousGoods() {
		this.marchandiseDangerousGood = await SingletonStatique.getListMarchandiseDangerousGoods();
	}
}
