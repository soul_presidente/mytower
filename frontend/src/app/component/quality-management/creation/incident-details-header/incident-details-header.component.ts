import { Component, Input, OnInit } from "@angular/core";
import { EbQmIncident } from "@app/classes/ebIncident";
import { Modules, TypeOfEvent } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { CodeLibelle } from "@app/utils/codeLibelle";
import { EbDemande } from "@app/classes/demande";
import { DateFormatterPipe } from "@app/shared/pipes/date-formatter.pipe";

@Component({
	selector: "app-incident-details-header",
	templateUrl: "./incident-details-header.component.html",
	styleUrls: ["./incident-details-header.component.css"],
})
export class IncidentDetailsHeaderComponent implements OnInit {
	constructor() {}

	@Input()
	incident: EbQmIncident;

	dtFormat: DateFormatterPipe = new DateFormatterPipe();

	@Input()
	module: number;

	@Input()
	demande: EbDemande;

	@Input()
	eventType: number;

	@Input()
	listIncidentStatusFromBackend: Array<CodeLibelle> = [];

	Modules = Modules;
	dateCreation: string;
	Statique = Statique;
	minutes: any;
	heure: any;
	jour: any;
	ngOnInit() {
		this.dateDisplays();
	}

	dateDisplays() {
		if (this.incident != null) {
			let today: Date = new Date();
			let todayInMilliSecondes = today.getTime();

			let timeSinceInsuance = todayInMilliSecondes - <any>this.incident.dateCreation;
			let data = Statique.formatMillisToObject(timeSinceInsuance);
			this.jour = data.days;
			this.heure = data.hours;
			this.minutes = data.minutes;
		}
	}

	get eventRefLabel(): string {
		return this.eventType == TypeOfEvent.UNCONFORMITY
			? "TRACK_TRACE.UNCONFORMITY_REF"
			: "Incident Ref";
	}
}
