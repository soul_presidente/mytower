import { Component, Input, OnInit } from "@angular/core";
import { EmailHistoriqueService } from "@app/services/emailHistorique.service";
import { EmailHistorique } from "@app/classes/emailHistorique";
import { Statique } from "@app/utils/statique";
import { EbQmIncident } from "@app/classes/ebIncident";

@Component({
	selector: "app-progress-details",
	templateUrl: "./progress-details.component.html",
	styleUrls: ["./progress-details.component.css"],
})
export class ProgressDetailsComponent implements OnInit {
	listEmail: Array<EmailHistorique>;
	selectedEmail: EmailHistorique = null;
	Statique = Statique;
	newEmail: EmailHistorique = new EmailHistorique();
	@Input()
	incident: EbQmIncident;

	constructor(protected emailHistoriqueService?: EmailHistoriqueService) {}

	ngOnInit() {
		this.emailHistoriqueService
			.getListEmailHistorique(this.incident.ebQmIncidentNum)
			.subscribe((data: Array<EmailHistorique>) => {
				this.listEmail = data;
			});
	}

	openPopupEmailHistorique(data: EmailHistorique) {
		this.selectedEmail = data;
		// this.selectedEmail.date.toString
	}
	addEleDom(data) {
		this.listEmail.push(data);
	}

	onEmailHistoriqueSent(email: EmailHistorique) {
		this.listEmail.push(email);
		this.selectedEmail = null;
	}
}
