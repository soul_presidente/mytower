import { Component, Input, OnInit } from "@angular/core";
import { EcCountry } from "@app/classes/country";
import { StatiqueService } from "@app/services/statique.service";
import { EbQmIncident } from "@app/classes/ebIncident";
import { Statique } from "@app/utils/statique";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { Message } from "primeng/components/common/api";
import { MessageService } from "primeng/api";
import SingletonStatique from "@app/utils/SingletonStatique";

@Component({
	selector: "app-shipment-information",
	templateUrl: "./shipment-information.component.html",
	styleUrls: ["./shipment-information.component.css"],
})
export class ShipmentInformationComponent implements OnInit {
	@Input()
	incident: EbQmIncident;
	@Input()
	insurance: boolean;
	listDestinations: Array<EcCountry>;
	listModeTransports: Array<any> = [];

	toggleEditData: boolean = true;
	Statique = Statique;
	yesOrNo: any = Statique.yesOrNo;
	msgs: Message[] = [];

	constructor(
		private messageService: MessageService,
		private qualityManagementService: QualityManagementService,
		private statiqueService: StatiqueService
	) {}

	ngOnInit() {
		(async () => (this.listDestinations = await SingletonStatique.getListEcCountry()))();
		(async () => (this.listModeTransports = await SingletonStatique.getListModeTransport()))();
	}

	affectInsurance() {
		this.incident.insurance = this.insurance ? 1 : 0;
	}

	updateIncidentShipment() {
		let Header = "Shipment related information",
			messageText = "informations modifiiées avec succès";
		this.qualityManagementService.updateIncidentShipment(this.incident).subscribe((data) => {
			this.messageService.add({ severity: "success", summary: Header, detail: messageText });
		});
	}
}
