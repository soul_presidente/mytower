import { Component, Input, OnInit } from "@angular/core";
import { EbQmIncident } from "@app/classes/ebIncident";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { Message } from "primeng/components/common/api";
import { MessageService } from "primeng/api";

@Component({
	selector: "app-commentaires",
	templateUrl: "./commentaires.component.html",
	styleUrls: ["./commentaires.component.css"],
})
export class CommentairesComponent implements OnInit {
	@Input()
	incident: EbQmIncident;
	ocdComment: string;
	thirdPartyComment: string;
	msgs: Message[] = [];

	constructor(
		private messageService: MessageService,
		private qualityManagementService: QualityManagementService
	) {}

	ngOnInit() {}

	updateIncidentComment() {
		let Header = "Modification des commentaires",
			messageText = "Commentaires modifiiées avec succes";
		this.qualityManagementService.updateIncidentComment(this.incident).subscribe((data) => {
			this.messageService.add({ severity: "success", summary: Header, detail: messageText });
		});
	}
}
