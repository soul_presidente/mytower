import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from "@angular/core";
import { Email } from "@app/classes/email";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { SearchCriteriaQualityManagement } from "@app/utils/SearchCriteriaQualityManagement";
import { EbQmIncident } from "@app/classes/ebIncident";
import { EmailHistorique } from "@app/classes/emailHistorique";

@Component({
	selector: "app-qm-semi-automatique-mail",
	templateUrl: "./qm-semi-automatique-mail.component.html",
	styleUrls: ["./qm-semi-automatique-mail.component.css"],
})
export class QMSemiAutomatiqueMailComponent implements OnInit, OnChanges {
	mail: Email = new Email();
	listEnCopieCachee: String;

	@Input()
	incident: EbQmIncident;
	@Input()
	selectedEmail: EmailHistorique;
	@Output()
	newEmail: EventEmitter<EmailHistorique> = new EventEmitter<EmailHistorique>();
	@Output()
	closePopUpMail: EventEmitter<boolean> = new EventEmitter<boolean>();
	criteria: SearchCriteriaQualityManagement = new SearchCriteriaQualityManagement();

	listTypeMail: mailParams[] = [
		{ mailId: 37, mailType: "Intent to claim" },
		{ mailId: 38, mailType: "Incident qualification: claim" },
		{ mailId: 39, mailType: "Incident qualification: statement" },
		{ mailId: 40, mailType: "Finance reminder : waiting for compensation" },
		{ mailId: 41, mailType: "Comfirmation compensation payment" },
		{ mailId: 42, mailType: "Qualification modification statement -> claim" },
		{ mailId: 43, mailType: "Qualification modification claim -> statement" },
	];

	constructor(private qualityManagementService: QualityManagementService) {}

	ngOnInit() {}

	ngOnChanges() {
		/*  if(this.selectedEmail){
      this.mail.enCopie = this.selectedEmail.adresseCc.split(";");
      this.mail.enCopieCachee = this.selectedEmail.adresseCci.split(";");
      this.mail.subject = this.selectedEmail.subject;
      this.mail.text = this.selectedEmail.message;
      this.mail.to = this.selectedEmail.adresseTo;
    }*/
		if (this.selectedEmail) {
			this.mail.enCopie = this.selectedEmail.enCopie.split(";");
			this.mail.enCopieCachee = this.selectedEmail.enCopieCachee.split(";");
			this.mail.subject = this.selectedEmail.subject;
			this.mail.text = this.selectedEmail.text;
			this.mail.to = this.selectedEmail.tto;
			//   for(let ty of this.listTypeMail){
			//     if(ty.mailType == this.selectedEmail.typeEmail)
			//   this.mail.mailType = ty.mailId;}
			//
			//
			//   this.mail.emailType = this.criteria.semiAutoMailNum;

			// this.newEmail.emit(this.selectedEmail);
		}
	}

	close() {
		this.closePopUpMail.emit(false);
	}

	getSemiAutoMail(criteria: SearchCriteriaQualityManagement) {
		criteria.ebQmIncidentNum = this.incident.ebQmIncidentNum;
		criteria.semiAutoMailNum = this.criteria.semiAutoMailNum;
		this.mail.mailType = this.criteria.semiAutoMailNum;
		this.qualityManagementService.getSemiAutoMail(criteria).subscribe((data: Email) => {
			this.mail = data;
			this.mail.mailType = criteria.semiAutoMailNum;
		});
	}

	sendSemiAutoMail(mailTosend: Email) {
		this.mail.enCopieCachee =
			this.listEnCopieCachee != null ? this.listEnCopieCachee.split(" ") : null;
		this.qualityManagementService
			.sendSemiAutoMail(this.mail, this.incident.ebQmIncidentNum)
			.subscribe((data: EmailHistorique) => {
				this.newEmail.emit(data);
				this.listEnCopieCachee = null;
				this.close();
			});
	}

	getTypeMail($event, $selectElt) {
		//    this.mail.mailType = Statique.isDefined(+$($selectElt).val()) ? +$($selectElt).val() : +$($selectElt).val()["split"](':')[1];
	}
}

export interface mailParams {
	mailId: any;
	mailType: string;
}
