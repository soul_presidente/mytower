import { Component, OnInit, ViewChild, Input, ElementRef } from "@angular/core";
import { SearchCriteriaQualityManagement } from "@app/utils/SearchCriteriaQualityManagement";
import { EbQmIncident } from "@app/classes/ebIncident";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { Statique } from "@app/utils/statique";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { IDChatComponent, Modules, TypeOfEvent } from "@app/utils/enumeration";
import { EbDemande } from "@app/classes/demande";
import { RootCausesAnalysisComponent } from "../create/root-causes-analysis/root-causes-analysis.component";
import { FichierJoint } from "@app/classes/fichiersJoint";
import { Message, MessageService } from "primeng/api";
import { ModalService } from "@app/shared/modal/modal.service";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { IncidentRelatedInformationComponent } from "../create/incident-related-information/incident-related-information.component";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { TranslateService } from "@ngx-translate/core";
import { EbChat } from "@app/classes/chat";
import { ChatPanelComponent } from "@app/shared/chat-panel/chat-panel.component";
import { EbTtCategorieDeviation } from "@app/classes/categorieDeviation";
import { StatiqueService } from "@app/services/statique.service";
import { CodeLibelle } from "@app/utils/codeLibelle";
import { EbEtablissement } from "@app/classes/etablissement";

@Component({
	selector: "app-creation",
	templateUrl: "./creation.component.html",
	styleUrls: ["./creation.component.css"],
	host: {
		"(document:click)": "onCloseChatWrapper($event)",
	},
})
export class CreationComponent extends ConnectedUserComponent implements OnInit {
	typeOfEventTranslate : string ;
	incident: EbQmIncident = null;
	ebQmIncidentNum: number;
	module: number;
	IDChatComponent = IDChatComponent;
	Modules = Modules;
	openPopUp: boolean = false;
	Statique = Statique;
	insurance: boolean;
	demande: EbDemande = new EbDemande();
	variableModule: string;

	msgs: Message[] = [];
	dialogHeader: string;
	dialogContent: string;

	@ViewChild("rootCausesAnalysis", { static: false })
	rootCausesAnalysisComponent: RootCausesAnalysisComponent;
	@ViewChild("incidentInformation", { static: false })
	incidentRelatedInformationComponent: IncidentRelatedInformationComponent;
	@ViewChild("chatPanelQualityComponent", { static: false })
	chatPanelPricingComponent: ChatPanelComponent;

	filesReturnToParent: Array<Array<FichierJoint>> = new Array<Array<FichierJoint>>();
	nbColTexteditor: string = "col-md-12";
	nbColListing: string = "col-md-12";

	toggleChat: boolean = false;
	preventCloseChatOnClickOutside: boolean = false;

	listIncidentCategory: Array<EbTtCategorieDeviation> = [];

	openStatusPopUp: boolean;

	listIncidentStatusFromBackend: Array<CodeLibelle> = [];
	selectedStatus: number;
	@ViewChild("pageValid", { static: false })
	pageValid: ElementRef;
	searchCriteriaQualityManagement = new SearchCriteriaQualityManagement();

	constructor(
		private qualityManagementService: QualityManagementService,
		private router: Router,
		protected headerService: HeaderService,
		private route: ActivatedRoute,
		private messageService: MessageService,
		private modalService?: ModalService,
		protected translate?: TranslateService,
		private statiqueService?: StatiqueService
	) {
		super();
	}

	updateChatEmitter(ebChat: EbChat) {
		if (Statique.isDefined(this.chatPanelPricingComponent) && Statique.isDefined(ebChat))
			this.chatPanelPricingComponent.insertChat(ebChat);
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "MODULES.QM",
				path: "/app/quality-management/dashboard",
			},
			{
				label: this.incident.demandeTranportRef,
				path: "/app/transport-management/details/" + this.incident.xEbDemande,
			},
			{
				label: this.incident.incidentRef,
			},
		];
	}

	ngOnInit() {
		let searchCriteriaQualityManagement = new SearchCriteriaQualityManagement();
		let searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();

		this.route.params.subscribe((params: Params) => {
			this.ebQmIncidentNum = +params["ebQmIncidentNum"];
			this.module = Statique.getModuleNumFromUrl(this.router.url);
		});
		let $this = this;
		if (this.ebQmIncidentNum) {
			searchCriteriaQualityManagement.ebQmIncidentNum = this.ebQmIncidentNum;
			this.qualityManagementService
				.getIncident(searchCriteriaQualityManagement)
				.subscribe((data: EbQmIncident) => {
					if (data) {
						$this.incident = data;
						this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());
						this.headerService.registerActionButtons(this.getActionButtons());
						this.insurance = this.incident.insurance == 1 ? true : false;

						searchCriteriaPricingBooking.ebDemandeNum = this.incident.xEbDemande;
						this.qualityManagementService
							.demande(searchCriteriaPricingBooking)
							.subscribe((demande) => {
								this.demande = demande;
								this.demande.libelleOriginCountry = this.incident.originCountryLibelle;
								this.demande.libelleDestCountry = this.incident.destinationCountryLibelle;
								this.demande.xEcModeTransport = this.incident.modeTransport;
								this.demande.insuranceValue = this.incident.insurance;
								this.demande.listCategories = this.incident.listCategories;
								this.demande.listCustomsFields = this.incident.listCustomsFields;
							});

						this.loadStatiques();
					} else {
						this.variableModule = "incident";
						let url: string = "failedaccess";
						this.router.navigate([url, this.ebQmIncidentNum, this.variableModule]);
					}
				});

			//charger la liste des statuts d'incident afin de poppuler la liste deroulant des statuts
			this.qualityManagementService.getListIncidentStatus().subscribe((listIncidentStatut) => {
				$this.listIncidentStatusFromBackend = listIncidentStatut;
			});
		}
	}

	loadStatiques() {
		this.statiqueService
			.getListCategorieDeviation()
			.subscribe((res: Array<EbTtCategorieDeviation>) => {
				this.listIncidentCategory = res.filter(
					(deviation) => deviation.typeEvent === this.incident.eventTypeStatus || !deviation.typeEvent
				);
			});
	}

	getActionButtons(): Array<HeaderInfos.ActionButton> {
		return [
			{
				label: this.eventStatus,
				icon: "fa fa-plus-circle",
				action: () => {
					this.openStatusPopUp = true;
				},
			},
			{
				label: "Mail",
				icon: "fa fa-envelope",
				action: () => {
					this.openPopUp = true;
				},
			},
			{
				label: "Chat",
				icon: "fa fa-comment",
				action: () => {
					this.openChat();
				},
			},
		];
	}

	cancel() {
		this.modalService.confirm(
			this.translate.instant("REQUEST_OVERVIEW.WARNING"),
			this.translate.instant("QUALITY_MANAGEMENT.CONFIRMATION_CANCEL_TEXT"),
			() => {
				this.router.navigateByUrl("app/quality-management/dashboard");
			}
		);
	}

	openChat() {
		this.toggleChat = !this.toggleChat;
		this.preventClosingChat();
	}

	save(incident: any) {
		this.rootCausesAnalysisComponent.rootCausesInformation(0);
		this.incidentRelatedInformationComponent.setIncidentInformation(0);
		let listRootCausesNums: Array<Number> = new Array();
		let that = this;
		if (this.incident != null) {
			if (this.incident.rootCauses && this.incident.rootCauses.length > 0) {
				this.incident.rootCauses.forEach((rootCause) => {
					listRootCausesNums.push(rootCause.qmRootCauseNum);
				});
				incident.listRootCauses = ":" + listRootCausesNums.join(":") + ":";
			}
			this.dialogHeader = this.translate.instant("QUALITY_MANAGEMENT.DIALOG_HEADER_INCIDENT");
			this.dialogContent = this.translate.instant("QUALITY_MANAGEMENT.DIALOG_CONTENT_INCIDENT");

			this.modalService.confirm(this.dialogHeader, this.dialogContent, function() {
				that.updateIncident(incident);
			});
		}
	}
	prepareIncidentToSave(incident: EbQmIncident) {
		let incidentToSave: EbQmIncident = new EbQmIncident();
		incidentToSave.ebQmIncidentNum = incident.ebQmIncidentNum;
		incidentToSave.incidentRef = incident.incidentRef;
		incidentToSave.xEbDemande = incident.xEbDemande;
		incidentToSave.incidentLines = incident.incidentLines;
		incidentToSave.currency = incident.currency;
		incidentToSave.carrier = incident.carrier;
		incidentToSave.listCustomsFields = incident.listCustomsFields;
		incidentToSave.rootCauses = incident.rootCauses;
		incidentToSave.listRootCauses = incident.listRootCauses;
		incidentToSave.listTypeDocuments = incident.listTypeDocuments;
		incidentToSave.othersCost = incident.othersCost;
		incidentToSave.originCountryLibelle = incident.originCountryLibelle;
		incidentToSave.destinationCountryLibelle = incident.destinationCountryLibelle;
		incidentToSave.eventType = incident.eventType;
		incidentToSave.eventTypeStatus = incident.eventTypeStatus;
		incidentToSave.unitRef = incident.unitRef;
		incidentToSave.incidentDesc = incident.incidentDesc;
		incidentToSave.dateCreation = incident.dateCreation;
		incidentToSave.dateMaj = incident.dateMaj;
		incidentToSave.dateCloture = incident.dateCloture;
		incidentToSave.dateIncident = incident.dateIncident;
		incidentToSave.insurance = incident.insurance;
		incidentToSave.insuranceComment = incident.insuranceComment;
		incidentToSave.claimAmount = incident.claimAmount;
		incidentToSave.recovered = incident.recovered;
		incidentToSave.balance = incident.balance;
		incidentToSave.addImpact = incident.addImpact;
		incidentToSave.commentAlis = incident.commentAlis;
		incidentToSave.commentEtablissement = incident.commentEtablissement;
		incidentToSave.commentAssurance = incident.commentAssurance;
		incidentToSave.montantIncident = incident.montantIncident;
		incidentToSave.demandeTranportRef = incident.demandeTranportRef;
		incidentToSave.carrierNom = incident.carrierNom;
		incidentToSave.issuerNomPrenom = incident.issuerNomPrenom;
		incidentToSave.flowType = incident.flowType;
		incidentToSave.cancellationReason = incident.cancellationReason;
		incidentToSave.incidentStatus = incident.incidentStatus;
		incidentToSave.goodValues = incident.goodValues;
		incidentToSave.issuer = incident.issuer;
		incidentToSave.thirdPartyComment = incident.thirdPartyComment;
		incidentToSave.etablissementComment = incident.etablissementComment;
		incidentToSave.icomReference = incident.icomReference;
		incidentToSave.category = incident.category;
		incidentToSave.categoryLabel = incident.categoryLabel;
		incidentToSave.currencyLabel = incident.currencyLabel;
		incidentToSave.modeTransport = incident.modeTransport;
		incidentToSave.daysDelay = incident.daysDelay;
		incidentToSave.hoursDelay = incident.hoursDelay;
		incidentToSave.unitDamaged = incident.unitDamaged;
		incidentToSave.unitDamagedAmount = incident.unitDamagedAmount;
		incidentToSave.unitLost = incident.unitLost;
		incidentToSave.amondOfunitLost = incident.amondOfunitLost;
		incidentToSave.recoveredInsurance = incident.recoveredInsurance;
		incidentToSave.rootCauseStatut = incident.rootCauseStatut;
		incidentToSave.waitingForText = incident.waitingForText;
		incidentToSave.listCategories = incident.listCategories;
		incidentToSave.impactedTransportAmount = incident.impactedTransportAmount;
		incidentToSave.impactedTransportPercentage = incident.impactedTransportPercentage;
		incidentToSave.listFlag = incident.listFlag;
		incidentToSave.listEbFlagDTO = incident.listEbFlagDTO;
		incidentToSave.partNumber = incident.partNumber;
		incidentToSave.serialNumber = incident.serialNumber;
		incidentToSave.shippingType = incident.shippingType;
		incidentToSave.severity = incident.severity;
		incidentToSave.actions = incident.actions;
		incidentToSave.criticality = incident.criticality;
		incidentToSave.responsable = incident.responsable;
		incidentToSave.rootCauseCategory = incident.rootCauseCategory;
		incidentToSave.psl = incident.psl;
		incidentToSave.typeRequestNum = incident.typeRequestNum;
		incidentToSave.typeRequestLibelle = incident.typeRequestLibelle;
		incidentToSave.ownerNomPrenom = incident.ownerNomPrenom;
		return incidentToSave;
	}

	updateIncident(incident: EbQmIncident) {
		let incidentToSave = this.prepareIncidentToSave(incident);
		this.qualityManagementService.updateIncidents(incidentToSave).subscribe((data: EbChat) => {
			if (data) {
				if (data[incident.ebQmIncidentNum]) {
					let rootCauses = data[incident.ebQmIncidentNum].rootCauses;
					let ebChat = data[incident.ebQmIncidentNum].ebChat;
					if (ebChat && this.chatPanelPricingComponent) {
						if (!this.chatPanelPricingComponent.listChat) {
							this.chatPanelPricingComponent.listChat = [];
						}
						this.chatPanelPricingComponent.listChat.unshift(ebChat);
					}
					if (rootCauses) {
						this.incident.rootCauses = rootCauses;
					}
				}
				let detail = this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL");
				let summary = this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY");
				this.messageService.add({
					severity: "success",
					summary: summary,
					detail: detail,
				});
				this.router.navigateByUrl("app/quality-management/dashboard");
			}
		});
	}

	onCloseChatWrapper() {
		if (!this.preventCloseChatOnClickOutside && this.toggleChat) this.toggleChat = false;
		this.preventCloseChatOnClickOutside = false;
	}

	preventClosingChat() {
		this.preventCloseChatOnClickOutside = true;
	}

	get eventInformationTitle(): string {
		return this.incident.eventTypeStatus == TypeOfEvent.UNCONFORMITY
			? "QUALITY_MANAGEMENT.UNCONFORMITY_INFORMATION"
			: "QUALITY_MANAGEMENT.INCIDENT_INFORMATION";
	}

	get eventFilesTitle(): string {
		return this.incident.eventTypeStatus == TypeOfEvent.UNCONFORMITY
			? "QUALITY_MANAGEMENT.UNCONFORMITY_FILES"
			: "QUALITY_MANAGEMENT.INCIDENT_FILES";
	}
	get eventStatus(): string {
		 this.typeOfEventTranslate =  this.incident.eventTypeStatus == TypeOfEvent.UNCONFORMITY
			? "QUALITY_MANAGEMENT.CHANGE_UNCONFORMITY_STATUS"
			: "QUALITY_MANAGEMENT.CHANGE_INCIDENT_STATUS";
			return  this.typeOfEventTranslate;
	}
	close() {
		this.selectedStatus = null;
		this.openStatusPopUp = false;
	}

	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}
	confirmAddEventTypeStatus() {
		this.qualityManagementService
			.updateEventTypeStatus(this.ebQmIncidentNum, this.selectedStatus)
			.subscribe((res) => {
				this.openStatusPopUp = false;
				this.incident.incidentStatus = this.selectedStatus;
				this.showSuccess();
			},
			(err) => {	
				this.showError();
			}
			);
	}
}
