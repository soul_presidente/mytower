import { Component, Input, OnInit } from "@angular/core";
import { EbQmIncident } from "@app/classes/ebIncident";
import { Message } from "primeng/components/common/api";
import { MessageService } from "primeng/api";
import { QualityManagementService } from "@app/services/quality-mangement.service.";

@Component({
	selector: "app-incident-description",
	templateUrl: "./incident-description.component.html",
	styleUrls: ["./incident-description.component.css"],
})
export class IncidentDescriptionComponent implements OnInit {
	@Input()
	incident: EbQmIncident;
	showDesc: boolean = true;
	showImpact: boolean = true;
	msgs: Message[] = [];

	constructor(
		private messageService: MessageService,
		private qualityManagementService: QualityManagementService
	) {}

	ngOnInit() {}

	showOrHide(target) {
		this["show" + target] = !this["show" + target];
	}

	updateIncidentDescription() {
		let Header = "Incident description information",
			messageText = "informations modifiiées avec succès";
		this.qualityManagementService.updateIncidentDescription(this.incident).subscribe((data) => {
			this.messageService.add({ severity: "success", summary: Header, detail: messageText });
		});
	}
}
