import { NgModule } from "@angular/core";

import { _QualityManagementComponent } from "./quality-management.component";
import { QualityManagementRoutes } from "./quality-management.route";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { CreationComponent } from "./creation/creation.component";
import { QualityManagementSearchComponent } from "./quality-management-search/quality-management-search.component";
import { QualityManagementListingComponent } from "./quality-management-listing/quality-management-listing.component";
import { SharedModule } from "@app/shared/module/shared.module";
import { DataTablesModule } from "angular-datatables";
import { IncidentInformationComponent } from "./creation/incident-information/incident-information.component";
import { ShipmentInformationComponent } from "./creation/shipment-information/shipment-information.component";
import { CommentairesComponent } from "./creation/commentaires/commentaires.component";
import { ActionsComponent } from "./creation/actions/actions.component";
import { CreateComponent } from "./create/create.component";
import { SearchUnitComponent } from "./create/search-unit/search-unit.component";
import { IncidentDetailsHeaderComponent } from "./creation/incident-details-header/incident-details-header.component";
import { QMSemiAutomatiqueMailComponent } from "./creation/semi-automatique-mail/qm-semi-automatique-mail.component";
import { ZonesIncidentComponent } from "./create/zones-incident/zones-incident.component";
import { IncidentRelatedInformationComponent } from "./create/incident-related-information/incident-related-information.component";
import { SharedDatasQualityManagemnt } from "@app/services/share.datas-qm";
import { GrowlModule } from "primeng/growl";
import { DialogModule } from "primeng/dialog";
import { TabViewModule } from "primeng/tabview";
import { TableModule } from "primeng/table";
import { RootCausesAnalysisComponent } from "./create/root-causes-analysis/root-causes-analysis.component";
import { CompensationComponent } from "./create/compensation/compensation.component";
import { IncidentDescriptionComponent } from "./creation/incident-description/incident-description.component";
import { RootCauseComponent } from "./root-Cause/root-Cause.component";
import { DeviationQmComponent } from "./deviationQm/deviationQm.component";
import { CorrectiveActionComponent } from "./corrective-action/corrective-action.component";
import { PricingService } from "@app/services/pricing.service";
import { ProgressDetailsComponent } from "./creation/progress-details/progress-details.component";

@NgModule({
	imports: [
		QualityManagementRoutes,
		SharedModule,
		DataTablesModule,
		GrowlModule,
		DialogModule,
		TabViewModule,
		TableModule,
	],
	exports: [],
	declarations: [
		_QualityManagementComponent,
		DashboardComponent,
		CreationComponent,
		QualityManagementSearchComponent,
		QualityManagementListingComponent,
		IncidentInformationComponent,
		ShipmentInformationComponent,
		CommentairesComponent,
		ActionsComponent,
		CreateComponent,
		SearchUnitComponent,
		IncidentDetailsHeaderComponent,
		QMSemiAutomatiqueMailComponent,
		ZonesIncidentComponent,
		IncidentRelatedInformationComponent,
		RootCausesAnalysisComponent,
		CompensationComponent,
		IncidentDescriptionComponent,
		RootCauseComponent,
		DeviationQmComponent,
		CorrectiveActionComponent,
		ProgressDetailsComponent,
	],
	providers: [SharedDatasQualityManagemnt, PricingService],
})
export class QualityManagementModule {}
