import {
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild,
} from "@angular/core";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { TranslateService } from "@ngx-translate/core";
import { Statique } from "@app/utils/statique";
import { GenericTableScreen, Modules } from "@app/utils/enumeration";
import { EbQmDeviation } from "@app/classes/EbQmDeviation";
import { SearchCriteriaQualityManagement } from "@app/utils/SearchCriteriaQualityManagement";
import { GenericTableCcpComponent } from "@app/shared/generic-table-ccp/generic-table-ccp.component";
import { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { DateFormatterPipe } from "@app/shared/pipes/date-formatter.pipe";

@Component({
	selector: "deviationQm",
	templateUrl: "./deviationQm.component.html",
	styleUrls: ["./deviationQm.component.css"],
})
export class DeviationQmComponent extends ConnectedUserComponent implements OnInit {
	dtFormat: DateFormatterPipe = new DateFormatterPipe();

	colonnes: any[];

	statique = Statique;
	Modules = Modules;
	initiateState: boolean = false;
	listDeviation: Map<number, EbQmDeviation> = new Map();

	deviation: EbQmDeviation = new EbQmDeviation();

	@Input()
	module: number;
	globalSearchCriteria: SearchCriteriaQualityManagement = new SearchCriteriaQualityManagement();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableCcpComponent;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	datatableComponent: number;

	@Output()
	listFieldsEmitter = new EventEmitter<Array<IField>>();
	@Output()
	listCategorieEmitter = new EventEmitter<Array<EbCategorie>>();

	@Input("searchInput")
	set searchInput(searchInput: SearchCriteriaQualityManagement) {
		if (searchInput != null) {
			let tmpSearchInput: SearchCriteriaQualityManagement = new SearchCriteriaQualityManagement();

			tmpSearchInput.module = this.module;
			tmpSearchInput.ebUserNum = this.userConnected.ebUserNum;
			tmpSearchInput.searchInput = JSON.stringify(searchInput || {});

			this.globalSearchCriteria = tmpSearchInput;
		}
	}

	getDatatableCompId() {
		if (this.module == Modules.QUALITY_MANAGEMENT) return GenericTableScreen.QUALITY;
		return null;
	}

	constructor(
		private qualityManagementService: QualityManagementService,
		private translate?: TranslateService
	) {
		super();
	}

	ngOnInit() {
		this.datatableComponent = this.getDatatableCompId();
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["module"] || (changes["searchInput"] && changes["searchInput"].firstChange)) {
			this.initDatatable();
		}
	}

	onListCategorieLoaded(listCategorie: Array<EbCategorie>) {
		this.listCategorieEmitter.emit(listCategorie);
	}
	onListFieldLoaded(listField: Array<IField>) {
		this.listFieldsEmitter.emit(listField);
	}

	getListDeviation(): Map<number, EbQmDeviation> {
		let data = this.genericTable.getSelectedRows();
		let mapData = new Map<number, EbQmDeviation>();

		this.listDeviation = new Map<number, EbQmDeviation>();
		data.forEach((it) => {
			mapData.set(it.ebQmDeviationNum, it);
		});

		this.listDeviation = mapData;

		return mapData;
	}

	initDatatable() {
		this.globalSearchCriteria.pageNumber = 1;
		this.globalSearchCriteria.module = this.module;
		this.globalSearchCriteria.ebUserNum = this.userConnected.ebUserNum;

		this.dataInfos.cols = this.getColumns();
		this.dataInfos.dataKey = "ebQmDeviationNum";
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = this.statique.controllerQualityManagement + "/list-deviation";
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.lineActions = [];
		this.dataInfos.extractWithAdvancedSearch = true;
		this.dataInfos.showCheckbox = true;

		this.dataInfos.showDetails = false;
		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(this.module)];
		//this.dataInfos.favorisName = row => row.refTransport;
		//this.dataInfos.actionDetailsLink = "/app/" + moduleRoute + "/details/";
		this.dataInfos.dataFlagsField = "listEbFlagDTO";
		//this.dataInfos.onFlagChange = this.onFlagChange;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.cardView = false;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.blankBtnCount = 1;
	}

	getColumns(): any {
		let cols: any = [
			{
				translateCode: "QUALITY_MANAGEMENT.DEVIATION_TYPE",
				field: "typeDeviation",
				// render: function(data) {
				//   return this.dtFormat.transform(data);
				// }.bind(this)
			},
			{
				translateCode: "QUALITY_MANAGEMENT.DEVIATION_REF",
				field: "refDeviation",
			},
			{
				field: "unitRef",
				translateCode: "QUALITY_MANAGEMENT.UNIT_REF",
			},
			{ field: "refTransport", translateCode: "QUALITY_MANAGEMENT.TRANSPORT_REF" },

			{
				field: "deviationDate",
				translateCode: "QUALITY_MANAGEMENT.DEVIATION_DATE",
				render: function(data) {
					return this.dtFormat.transform(data);
				}.bind(this),
			},

			{ field: "originCountryLibelle", translateCode: "QUALITY_MANAGEMENT.ORIGIN_COUNTRY" },
			{
				field: "destinationCountryLibelle",
				translateCode: "QUALITY_MANAGEMENT.DESTINATION_COUNTRY",
			},
			{
				field: "carrierName",
				translateCode: "QUALITY_MANAGEMENT.CARRIER_LABEL",
			},

			// { field: "", translateCode: "QUALITY_MANAGEMENT.TIME_SINCE_INSSURANCE" },

			// { field: "", translateCode: "QUALITY_MANAGEMENT.CARRIER_LABEL" },
		];
		return cols;
	}
}
