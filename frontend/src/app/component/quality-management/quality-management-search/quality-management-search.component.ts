import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";
import { SearchCriteriaQualityManagement } from "@app/utils/SearchCriteriaQualityManagement";
import { StatiqueService } from "@app/services/statique.service";
import { Modules, SavedFormIdentifier } from "@app/utils/enumeration";
import { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { SearchField } from "@app/classes/searchField";
import { EbLabel } from "@app/classes/label";
import { Statique } from "@app/utils/statique";
import { AdvancedFormSearchComponent } from "@app/shared/advanced-form-search/advanced-form-search.component";
import { SearchCriteriaTrackTrace } from "@app/utils/SearchCriteriaTrackTrace";
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: "app-quality-management-search",
	templateUrl: "./quality-management-search.component.html",
	styleUrls: ["./quality-management-search.component.css"],
})
export class QualityManagementSearchComponent implements OnInit {
	searchCriteriaQM = new SearchCriteriaQualityManagement();

	@Input()
	module: number;
	@Output()
	onSearch = new EventEmitter<SearchCriteriaQualityManagement>();
	@Input()
	listFields: Array<IField>;
	@Input()
	listCategorie: Array<EbCategorie>;

	SavedFormIdentifier = SavedFormIdentifier;
	moduleName = Modules;
	advancedOption: SearchCriteriaQualityManagement = new SearchCriteriaQualityManagement();

	paramFields: Array<SearchField> = new Array<SearchField>();
	initParamFields: Array<SearchField> = new Array<SearchField>();
	listAllCategorie: Array<EbLabel> = new Array<EbLabel>();

	@Input()
	dataInfosItem: Array<any> = [];
	@Input()
	selectedField: any;

	@Input()
	initialListFields: any = [];

	@Input()
	initFields: string = "json"; // undefined | all | json

	public listSelectedFields: Array<SearchField>;
	isAddField: boolean = true;
	Statique = Statique;
	@ViewChild(AdvancedFormSearchComponent, { static: false })
	advancedFormSearch: AdvancedFormSearchComponent;

	constructor(public generaleMethode: generaleMethodes, private statiqueService: StatiqueService, private translate?: TranslateService) {}

	ngOnInit() {
		this.onInputChange(this.selectedField);
		this.initialListFields = [];
		if (this.initialListFields && typeof this.initFields !== "undefined") {
			if (this.initFields === "all")
				this.listSelectedFields = Statique.cloneObject(this.initialListFields);
			else {
				this.listSelectedFields = this.paramFields;
			}
		}
	}
	onClickInput(initParam: boolean = false) {
		if(this.listCategorie && this.listCategorie.length > 0){
			this.listCategorie.splice(1, 1);
		}
		if (this.module == this.moduleName.TRACK) {
			this.initParamFields = require("../../../../assets/ressources/jsonfiles/tracing-searchfields.json");
		}
		if (this.module == this.moduleName.QUALITY_MANAGEMENT) {
			this.initParamFields = require("../../../../assets/ressources/jsonfiles/quality-management-search.json");
		}
		this.initParamFields.forEach((f) =>
			this.translate.get(f.label).subscribe((res) => (f.label = res))
		);
		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));

		if (this.listFields && this.listFields.length > 0) {
			let arr = [];
			this.listFields.forEach((it) => {
				arr.push({
					label: it.label,
					name: it.name,
					type: "text",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
			if (initParam) {
				this.initParamFields = this.initParamFields.concat(arr);
			}
		}
		if (this.listCategorie && this.listCategorie.length > 0) {
			let arr = [];
			let i = 0;
			this.listAllCategorie = [];
			this.listCategorie.forEach((it, i) => {
				arr.push({
					label: it.libelle,
					type: "autocomplete",
					name: "categoryField" + i,
					listItems: it.labels,
					fetchAttribute: "libelle",
					fetchValue: "ebLabelNum",
					multiple: "true",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
			if (initParam) {
				this.initParamFields = this.initParamFields.concat(arr);
			}
		}

		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});
	}

	onInputChange(data: any) {
		this.onClickInput(true);
		if (this.module == this.moduleName.QUALITY_MANAGEMENT) {
			this.initParamFields = require("../../../../assets/ressources/jsonfiles/quality-management-search.json");
		}
		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));

		if (this.listFields && this.listFields.length > 0) {
			let arr = [];
			this.listFields.forEach((it) => {
				arr.push({
					label: it.label,
					name: "field_" + it.name,
					type: "text",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
			this.dataInfosItem.sort((a, b) => (a.label > b.label ? 1 : -1));
			this.initParamFields = this.initParamFields.concat(arr);
		}

		if (this.listCategorie && this.listCategorie.length > 0) {
			let arr = [];
			let i = 0;
			this.listAllCategorie = [];
			this.listCategorie.forEach((it, i) => {
				arr.push({
					label: it.libelle,
					type: "autocomplete",
					name: "categoryField" + i,
					listItems: it.labels,
					fetchAttribute: "libelle",
					fetchValue: "ebLabelNum",
					multiple: "true",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
			this.dataInfosItem.sort((a, b) => (a.label > b.label ? 1 : -1));
			this.initParamFields = this.initParamFields.concat(arr);
		}
		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});

		if (!Statique.isDefined(data)) {
			this.initParamFields = this.initParamFields.filter(
				(field) =>
					field.name === "listTransportRef" ||
					field.name === "listCarrierRefs" ||
					field.name === "listIncidentCategories"
			);
			this.paramFields = Array.from(new Set([].concat(this.initParamFields)));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
			
		} else if (data !== null && !this.paramFields.find((field) => field.name == data)) {
			let result = JSON.stringify(this.advancedFormSearch.getSearchInput());
			let parsedValues: Array<SearchField> = JSON.parse(result);
			this.paramFields.forEach((field) => {
				if (parsedValues[field.name] !== null && parsedValues[field.name] !== undefined) {
					field.default = parsedValues[field.name];
				}
			});
			this.initParamFields = this.initParamFields.filter(
				(field) => field.name === data && field.default !== null
			);
			this.paramFields = this.paramFields.concat(this.initParamFields);
			this.paramFields = Array.from(new Set(this.paramFields));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
			this.selectedField = {};
		}
	}

	search(advancedOption: SearchCriteriaQualityManagement) {
		this.onSearch.emit(advancedOption);
	}
}
