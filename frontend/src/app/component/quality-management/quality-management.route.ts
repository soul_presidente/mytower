import { Route, RouterModule } from "@angular/router";

import { _QualityManagementComponent } from "./quality-management.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { CreationComponent } from "./creation/creation.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";
import { CreateComponent } from "./create/create.component";

export const QUALITY_MANAGEMENT_ROUTES: Route[] = [
	{
		path: "",
		component: _QualityManagementComponent,
		canActivate: [UserAuthGuardService],
	},

	{
		path: "dashboard",
		component: DashboardComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "dashboard/:rootcauseNum",
		component: DashboardComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "details/:ebQmIncidentNum",
		component: CreationComponent,
		canActivate: [UserAuthGuardService],
	},

	{
		path: "create",
		component: CreateComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const QualityManagementRoutes = RouterModule.forChild(QUALITY_MANAGEMENT_ROUTES);
