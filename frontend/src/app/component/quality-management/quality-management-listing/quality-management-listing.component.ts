import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild,
} from "@angular/core";
import { StatiqueService } from "@app/services/statique.service";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { ExportService } from "@app/services/export.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { Statique, keyValue } from "@app/utils/statique";
import { HttpClient } from "@angular/common/http";
import { EbQmIncident } from "@app/classes/ebIncident";
import { TranslateService } from "@ngx-translate/core";
import { SearchCriteriaQualityManagement } from "@app/utils/SearchCriteriaQualityManagement";
import {
	GenericTableScreen,
	ModeTransport,
	Modules,
	TypeOfEvent,
	StatusIncident,
} from "@app/utils/enumeration";
import { EcCountry } from "@app/classes/country";
import { ModalService } from "@app/shared/modal/modal.service";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { EtablissementService } from "@app/services/etablissement.service";
import EbCustomField, { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import { EcQmRootCause } from "@app/classes/rootCause";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbTypeTransport } from "@app/classes/EbTypeTransport";
import { GenericTableCcpComponent } from "@app/shared/generic-table-ccp/generic-table-ccp.component";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { EbChat } from "@app/classes/chat";
import { EbFlag } from "@app/classes/ebFlag";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { FlagColumnGenericCell } from "@app/shared/generic-cell/commons/flag-column.generic-cell";
import { DateFormatterPipe } from "@app/shared/pipes/date-formatter.pipe";
import { Location } from "@angular/common";

@Component({
	selector: "app-quality-management-listing",
	templateUrl: "./quality-management-listing.component.html",
	styleUrls: ["./quality-management-listing.component.css"],
})
export class QualityManagementListingComponent extends ConnectedUserComponent implements OnInit {
	dtFormat: DateFormatterPipe = new DateFormatterPipe();

	Modules = Modules;
	initiateState: boolean = false;
	listDestinations: any;
	count: number;
	diff: number;
	listCountry: Array<EcCountry> = new Array<EcCountry>();
	listIncident: Map<number, EbQmIncident> = new Map();

	@Input()
	module: number;

	@Input()
	eventType: number;

	globalSearchCriteria: SearchCriteriaQualityManagement = new SearchCriteriaQualityManagement();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableCcpComponent;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	datatableComponent: number;

	@Output()
	updateChatEmitter = new EventEmitter<EbChat>();

	listEbQmIncidentNum: number[] = [];

	listFields: Array<IField>;
	private ebCustomField: EbCustomField;
	listCategorie: Array<EbCategorie> = new Array<EbCategorie>();

	@Output()
	listFieldsEmitter = new EventEmitter<Array<IField>>();
	@Output()
	listCategorieEmitter = new EventEmitter<Array<EbCategorie>>();
	private listTypeTransportMap: Map<number, Array<EbTypeTransport>>;

	listCriticality: Array<keyValue> = null;

	@Input("searchInput")
	set searchInput(searchInput: SearchCriteriaQualityManagement) {
		if (searchInput != null) {
			let tmpSearchInput: SearchCriteriaQualityManagement = new SearchCriteriaQualityManagement();

			tmpSearchInput.module = this.module;
			tmpSearchInput.eventType = this.eventType;
			tmpSearchInput.ebUserNum = this.userConnected.ebUserNum;
			tmpSearchInput.searchInput = JSON.stringify(searchInput || {});

			this.globalSearchCriteria = tmpSearchInput;
		}
	}
	ConstantsTranslate: Object = {
		"QUALITY_MANAGEMENT.INCIDENT_STATUT.INTENT_TO_CLAIM": "",
		"QUALITY_MANAGEMENT.INCIDENT_STATUT.CLAIM": "",
		"QUALITY_MANAGEMENT.INCIDENT_STATUT.RECOVERED": "",
		"QUALITY_MANAGEMENT.INCIDENT_STATUT.CLOSED": "",
	};

	constructor(
		private statiqueService: StatiqueService,
		protected headerService: HeaderService,
		private qualityManagementService: QualityManagementService,
		private etablissementService: EtablissementService,
		private elRef: ElementRef,
		public generaleMethode: generaleMethodes,
		protected router: Router,
		private http: HttpClient,
		private modalService: ModalService,
		protected exportService?: ExportService,
		private translate?: TranslateService,
		private route?: ActivatedRoute,
		private location?: Location
	) {
		super();
	}

	clickCheckbox(ebQmIncidentNum: number, event: HTMLInputElement) {
		Statique.iconCheckOneToggle(event);
		if (event.checked) {
			this.listEbQmIncidentNum.push(ebQmIncidentNum);
		} else {
			this.listEbQmIncidentNum.splice($.inArray(ebQmIncidentNum, this.listEbQmIncidentNum), 1);
		}
	}

	ngOnInit() {
		this.route.params.subscribe((params: Params) => {
			this.globalSearchCriteria.ebQmRootCauseNum = Number(params["rootcauseNum"]);
		});
		this.qualityManagementService.listCriticalityIncidentEnum().subscribe((data) => {
			this.listCriticality = data;
		});

		this.getStatiques();
		this.datatableComponent = this.getDatatableCompId();
	}

	getListIncident(): Map<number, EbQmIncident> {
		let data = this.genericTable.getSelectedRows();
		let mapData = new Map<number, EbQmIncident>();

		this.listIncident = new Map<number, EbQmIncident>();
		data.forEach((it) => {
			mapData.set(it.ebQmIncidentNum, it);
		});

		this.listIncident = mapData;

		return mapData;
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["module"] || (changes["searchInput"] && changes["searchInput"].firstChange)) {
			this.initDatatable();
		}
	}

	getColumns(): Array<GenericTableInfos.Col> {
		const $this = this;
		let claim = 0,
			recovered = 0,
			balance = 0;
		let cols: Array<GenericTableInfos.Col> = [
			{
				field: "listEbFlagDTO",
				translateCode: "PRICING_BOOKING.FLAG",
				minWidth: "125px",
				allowClick: false,
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: FlagColumnGenericCell,
				genericCellParams: new FlagColumnGenericCell.Params({
					field: "listEbFlagDTO",
					contributionAccess: true,
					moduleNum : $this.module,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{ field: "qualificationLabel", translateCode: "QUALITY_MANAGEMENT.INCIDENT_QUALIF" },
			{ field: "incidentDesc", translateCode: "QUALITY_MANAGEMENT.INCIDENT_DESCRIPTION" },
			{ field: "incidentRef", isCardCol: true, translateCode: "QUALITY_MANAGEMENT.INCIDENT_REF" },
			{
				translateCode: "QUALITY_MANAGEMENT.TIMESINCEINSUANCE",
				field: "dateCreation",
				render: function(data) {
					let today: Date = new Date();
					let todayInMillisecondes = today.getTime();
					let datecreation = data;
					let timeSinceInsuance = todayInMillisecondes - <any>datecreation;
					let date = Statique.formatMillisToObject(timeSinceInsuance);
					return (
						date.days +
						" " +
						this.translate.instant("QUALITY_MANAGEMENT.DAY_SHORTCUT") +
						" " +
						date.hours +
						" " +
						this.translate.instant("QUALITY_MANAGEMENT.HOUR_SHORTCUT") +
						" " +
						date.minutes +
						" " +
						this.translate.instant("QUALITY_MANAGEMENT.MONTH_SHORTCUT")
					);
				}.bind(this),
			},
			{
				field: "demandeTranportRef",
				translateCode: "QUALITY_MANAGEMENT.TRANSPORT_REF",
				isCardCol: true,
				cardOrder: 2,
			},
			{ field: "carrierNom", translateCode: "QUALITY_MANAGEMENT.CARRIER" },
			{
				translateCode: "QUALITY_MANAGEMENT.INCIDENT_DATE",
				field: "dateIncident",
				render: function(data) {
					return Statique.formatDate(data);
				}.bind(this),
			},
			{
				field: "category", translateCode: "QUALITY_MANAGEMENT.INCIDENT_CAT",
				render: function (data) {
					return data!=null ? data.libelle: "";
				}.bind(this), },
			{ field: "originCountryLibelle", translateCode: "QUALITY_MANAGEMENT.ORIGIN_COUNTRY" },
			{
				field: "destinationCountryLibelle",
				translateCode: "QUALITY_MANAGEMENT.DESTINATION_COUNTRY",
			},
			{
				translateCode: "TRACK_TRACE.MODE_OF_TRANSPORT",
				field: "modeTransport",
				isCardCol: true,
				render: function(modeTransportCode, type, row) {
					if (!modeTransportCode) return "";
					return SingletonStatique.getModeTransportString(
						modeTransportCode,
						row,
						$this.listTypeTransportMap
					);
				}.bind(this),
			},
			{ field: "incidentRef", translateCode: "QUALITY_MANAGEMENT.INSSURANCE" },
			{
				field: "currencyLabel",
				translateCode: "QUALITY_MANAGEMENT.CURRENCY",
				isCardCol: true,
				cardOrder: 4,
			},
			{
				translateCode: "QUALITY_MANAGEMENT.CLAIM_AMMOUNT",
				field: "claimAmount",
				isCardCol: true,
				cardOrder: 3,
				render: function(data) {
					claim = data;
					return data;
				}.bind(this),
			},
			{
				translateCode: "QUALITY_MANAGEMENT.RECOVERED",
				field: "recovered",
				render: function(data) {
					recovered = data;
					return data;
				}.bind(this),
			},
			{
				field: "criticality",
				translateCode: "CONTROL_RULES.CRITICALITY",
				render: function(item) {
					return Statique.isDefined(item)
						? this.translate.instant("GENERAL.PRIORITY." + this.getCriticality(item))
						: "";
				}.bind(this),
			},
			{
				translateCode: "QUALITY_MANAGEMENT.BALANCE",
				field: "balance",
				render: function(data) {
					return claim - recovered;
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.REQUESTOR",
				field: "ownerNomPrenom",
			},

			{
				translateCode: "QUALITY_MANAGEMENT.LASTUPDATE",
				field: "dateMaj",
				render: function(data) {
					return Statique.formatDate(data);
				}.bind(this),
			},
			{ field: "lastContributerNomPremon", translateCode: "QUALITY_MANAGEMENT.LAST_CONTRIBUTOR" },
			{
				field: "rootCauses",
				translateCode: "QUALITY_MANAGEMENT.ROOT_CAUSE",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data: Array<EcQmRootCause>) {
					let str = "<div>";

					data &&
						data.forEach((it) => {
							str += `
              <div title="${it.label}">${it.label}</div>
            `;
						});
					str += "</div>";

					return str;
				}.bind(this),
			},
			{ field: "psl", translateCode: "QUALITY_MANAGEMENT.TIMESTAMP_RELATED" },
			{ field: "typeRequestLibelle", translateCode: "QUALITY_MANAGEMENT.SERVICE_LEVEL" },
			{
				translateCode: "QUALITY_MANAGEMENT.STATUS",
				field: "incidentStatus",
				render: function(data) {
					return data != null
						? this.translate.instant("QUALITY_MANAGEMENT.INCIDENT_STATUS." + data)
						: null;
				}.bind(this),
			},
		];

		cols.forEach((it) => {
			this.translate.get(it.translateCode).subscribe((res) => (it["header"] = res));
		});

		return cols;
	}

	async getStatiques() {
		(async () => (this.listCountry = await SingletonStatique.getListEcCountry()))();
		this.listTypeTransportMap = await SingletonStatique.getListTypeTransportMap();
	}

	getCriticality(item: any): string {
		return Statique.isDefined(this.listCriticality)
			? this.listCriticality.find((v) => v.code == item).key
			: "";
	}

	initDatatable() {
		this.globalSearchCriteria.pageNumber = 1;
		this.globalSearchCriteria.module = this.module;
		this.globalSearchCriteria.ebUserNum = this.userConnected.ebUserNum;
		this.globalSearchCriteria.eventType = this.eventType;
		this.dataInfos.showAddBtn = true;
		this.dataInfos.cols = this.getColumns();
		this.dataInfos.dataKey = "ebQmIncidentNum";
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerQualityManagement + "/listIncident";
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.lineActions = [];
		this.dataInfos.extractWithAdvancedSearch = true;
		this.dataInfos.showAdvancedSearchBtn = true;
		//this.dataInfos.favorisName = row => row.refTransport;
		this.dataInfos.showCheckbox = true;
		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(this.module)];
		this.dataInfos.showDetails = true;
		this.dataInfos.actionDetailsLink = "/app/" + moduleRoute + "/details/";

		this.dataInfos.showFlags = true;
		this.dataInfos.dataFlagsField = "listEbFlagDTO";
		this.dataInfos.onFlagChange = this.onFlagChange;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		//this.dataInfos.cardView = true;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.blankBtnCount = 1;

		this.dataInfos.insertDetailsContextItem();
		this.dataInfos.insertDetailsNewTabContextItem();
	}

	getDatatableCompId() {
		if (this.module == Modules.QUALITY_MANAGEMENT) return GenericTableScreen.QUALITY;
		return null;
	}

	openAddForm(event) {
		this.router.navigateByUrl("app/quality-management/create", {
			state: { eventType: this.eventType },
		});
	}

	clickHandler(data) {
		debugger;
		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(this.module)];
		this.router.navigate(["/app/" + moduleRoute + "/details", data.ebQmIncidentNum]);
	}

	checkAllIncident(event) {
		if (!event) return;

		let o;
		let elts = document.getElementsByClassName("checkbox-Track");

		for (o in elts) {
			if (elts[o]["click"] && (event.target || event).checked != elts[o]["checked"]) {
				elts[o]["click"]();
			}
		}
		Statique.iconCheckAllToggle(event);
	}

	onListCategorieLoaded(listCategorie: Array<EbCategorie>) {
		this.listCategorieEmitter.emit(listCategorie);
	}
	onListFieldLoaded(listField: Array<IField>) {
		this.listFieldsEmitter.emit(listField);
	}

	onFlagChange(context: any, row: EbQmIncident, listFlag: Array<EbFlag>) {
		let listFlagCode = EbFlag.convertListToCommaSeparated(listFlag);
		row.listFlag = listFlagCode;
		context.qualityManagementService.saveFlags(row.ebQmIncidentNum, listFlagCode).subscribe();
	}
	inlineEditingHandler(key: string, value: any, model: any, context: any) {
		if (key === "selection-change") {
			context.autoSaveRow(model, value);
		}
	}

	autoSaveRow(row: EbQmIncident, listFlag: Array<EbFlag>) {
		let listFlagCode = EbFlag.convertListToCommaSeparated(listFlag);
		row.listFlag = listFlagCode;
		this.qualityManagementService.saveFlags(row.ebQmIncidentNum, listFlagCode).subscribe();
	}
}
