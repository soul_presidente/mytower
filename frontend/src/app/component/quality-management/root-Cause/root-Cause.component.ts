import {
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild,
} from "@angular/core";
import { EcQmRootCause } from "@app/classes/rootCause";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { TranslateService } from "@ngx-translate/core";
import { SearchCriteriaQualityManagement } from "@app/utils/SearchCriteriaQualityManagement";
import { GenericTableCcpComponent } from "@app/shared/generic-table-ccp/generic-table-ccp.component";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableScreen, Modules, TypeOfEvent } from "@app/utils/enumeration";
import { Statique, keyValue } from "@app/utils/statique";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbCategorie } from "@app/classes/categorie";
import { IField } from "@app/classes/customField";
import { DateFormatterPipe } from "@app/shared/pipes/date-formatter.pipe";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { MessageService } from "primeng/api";
import { Router, NavigationExtras } from "@angular/router";
import { ButtonGenericCell } from "@app/shared/generic-cell/commons/button.generic-cell";
import { EbQmIncident } from "@app/classes/ebIncident";
import { ExportService } from "@app/services/export.service";

@Component({
	selector: "root-Cause",
	templateUrl: "./root-Cause.component.html",
	styleUrls: ["./root-Cause.component.css"],
})
export class RootCauseComponent extends ConnectedUserComponent implements OnInit {
	statique = Statique;
	Modules = Modules;

	dtFormat: DateFormatterPipe = new DateFormatterPipe();

	initiateState: boolean = false;

	isEditMode: boolean = false;
	//listRootcauses: Array<EcQmRootCause> = [];
	colonnes: any[];
	isAddEditMode = false;
	listRootcauses: Map<number, EcQmRootCause> = new Map();
	isAddMode: boolean = false;
	rootCause: EcQmRootCause = new EcQmRootCause();
	listRootCauseStatus: Array<keyValue> = null;
	listPriority: Array<keyValue> = null;
	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();
	listRootCauseCategories: Array<keyValue> = null;

	ConstantsTranslate: Object = {
		ROOT_CAUSE_LABEL: "",
		ROOT_CAUSE_REF: "",
		INCIDENT_LIES: "",
		IMPACTED_USERS: "",
		IMPACTED_PERIMETER: "",
		SEVERITY: "",
		CREATION_DATE: "",
		ACTIONS: "",
		CLOSED_DATE: "",
		ISSUER: "",
		AMOUNT: "",
		ROOT_CAUSE_STATUT: "",
		COMMENT: "",
		ROOT_CAUSE_CATEGORIE: "",
		ROOT_CAUSE_PARAM_FIELD: "",
	};

	@Input()
	module: number;
	globalSearchCriteria: SearchCriteriaQualityManagement = new SearchCriteriaQualityManagement();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableCcpComponent;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	datatableComponent: number;

	@Output()
	listFieldsEmitter = new EventEmitter<Array<IField>>();
	@Output()
	listCategorieEmitter = new EventEmitter<Array<EbCategorie>>();

	getDatatableCompId() {
		if (this.module == Modules.QUALITY_MANAGEMENT) return GenericTableScreen.ROOT_CAUSE;
		return null;
	}

	constructor(
		private qualityManagementService: QualityManagementService,
		private translate: TranslateService,
		private messageService: MessageService,
		private router: Router,
		protected exportService?: ExportService
	) {
		super();
	}

	ngOnInit() {
		this.qualityManagementService.listRootCauseStatusEnum().subscribe((data) => {
			this.listRootCauseStatus = data;
		});
		this.qualityManagementService.listCriticalityIncidentEnum().subscribe((data) => {
			this.listPriority = data;
		});
		this.qualityManagementService.listRootCauseCategoriesEnum().subscribe((data) => {
			this.listRootCauseCategories = data;
		});
		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate
				.get("QUALITY_MANAGEMENT." + key)
				.subscribe((res) => (this.ConstantsTranslate[key] = res));
		});
		this.datatableComponent = this.getDatatableCompId();
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["module"] || (changes["searchInput"] && changes["searchInput"].firstChange)) {
			this.initDatatable();
		}
	}

	getListRootCause(): Map<number, EcQmRootCause> {
		let data = this.genericTable.getSelectedRows();
		let mapData = new Map<number, EcQmRootCause>();

		this.listRootcauses = new Map<number, EcQmRootCause>();
		data.forEach((it) => {
			mapData.set(it.qmRootCauseNum, it);
		});

		this.listRootcauses = mapData;

		return mapData;
	}

	getColumns(): any {
		let cols: any = [
			//TODO MEP- les colonnes commentées en bas devra être réactivé après MEP
			{
				translateCode: "QUALITY_MANAGEMENT.ROOT_CAUSE_LABEL",
				field: "label",
			},
			{
				translateCode: "QUALITY_MANAGEMENT.REF",
				field: "ref",
			},
			{
				field: "severite",
				translateCode: "QUALITY_MANAGEMENT.SEVERITY",
				render: (item: number) => {
					return item != null ? this.translate.instant(Statique.getPriorityByKey(item)) : "";
				},
			},
			{
				field: "dateCreationSys",
				translateCode: "QUALITY_MANAGEMENT.CREATION_DATE",
				render: function(data) {
					return Statique.formatDate(data);
				}.bind(this),
			},
			{ field: "actions", translateCode: "GENERAL.ACTION" },
			{ field: "createur", translateCode: "QUALITY_MANAGEMENT.ISSUER" },
			{
				field: "statut",
				translateCode: "QUALITY_MANAGEMENT.ROOT_CAUSE_STATUT",
				render: (item: number) => {
					return item != null ? this.translate.instant(Statique.getRootCauseStatusByKey(item)) : "";
				},
			},
			{
				translateCode: "QUALITY_MANAGEMENT.INCIDENT_LIES",
				field: "qmRootCauseNum",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: ButtonGenericCell,
				genericCellParams: new ButtonGenericCell.Params({
					textRender: (row: EcQmRootCause) => {
						return this.translate.instant("GENERAL.ACTION_OPEN_DETAILS");
					},
					class: "btn btn-primary btn-xs",
				}),
				genericCellHandler: (key, value, row: EcQmRootCause, context) => {
					this.showDetailsPrice(row.qmRootCauseNum);
				},
			},
		];
		return cols;
	}

	showDetailsPrice(rootcauseNum: Number) {
		this.router
			.navigateByUrl("app/quality-management/dashboard" + rootcauseNum, {
				skipLocationChange: true,
			})
			.then(() => {
				this.router.navigate(["app/quality-management/dashboard", rootcauseNum]);
			});
	}

	initDatatable() {
		this.globalSearchCriteria.pageNumber = 1;
		this.globalSearchCriteria.module = this.module;
		this.globalSearchCriteria.ebUserNum = this.userConnected.ebUserNum;

		this.dataInfos.cols = this.getColumns();
		this.dataInfos.dataKey = "qmRootCauseNum";
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = this.statique.controllerQualityManagement + "/list-root-cause";
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.lineActions = [];
		this.dataInfos.extractWithAdvancedSearch = true;

		this.dataInfos.showAddBtn = true;
		this.dataInfos.showCheckbox = true;

		this.dataInfos.showDetails = false;
		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(this.module)];
		//this.dataInfos.favorisName = row => row.refTransport;
		//this.dataInfos.actionDetailsLink = "/app/" + moduleRoute + "/details/";
		this.dataInfos.dataFlagsField = "listEbFlagDTO";
		//this.dataInfos.onFlagChange = this.onFlagChange;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.cardView = false;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.blankBtnCount = 1;

		let paramsCallback = (row: EbQmIncident) => {
			return [
				{
					key: "eventType",
					val: row.eventType,
				},
				{
					key: "ebQmIncidentNum",
					val: row.ebQmIncidentNum,
				},
			];
		};
		this.dataInfos.insertDetailsContextItem();
		this.dataInfos.insertDetailsNewTabContextItem();
	}

	onListCategorieLoaded(listCategorie: Array<EbCategorie>) {
		this.listCategorieEmitter.emit(listCategorie);
	}
	onListFieldLoaded(listField: Array<IField>) {
		this.listFieldsEmitter.emit(listField);
	}

	async exportCsv() {
		this.exportService.startExportCsv(Modules.QUALITY_MANAGEMENT, false, null, this.userConnected);
	}

	addRootCause(rootCause: EcQmRootCause) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		rootCause.createur = this.userConnected.nomPrenom;
		rootCause.etablissement = this.userConnected.ebEtablissement;
		this.qualityManagementService.saveRootCause(rootCause).subscribe(
			(data) => {
				requestProcessing.afterGetResponseBtn(event);
				this.isAddEditMode = false;
				this.genericTable.refreshData();
				this.onDataChanged.next();
				this.successfullOperation();
			},
			(error) => {
				console.error(error);
				requestProcessing.afterGetResponse(event);
				this.operationFailed();
			}
		);
	}

	editRootCause(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.rootCause.etablissement = this.userConnected.ebEtablissement;
		this.qualityManagementService.saveRootCause(this.rootCause).subscribe(
			(data) => {
				requestProcessing.afterGetResponseBtn(event);
				this.isAddEditMode = false;
				this.genericTable.refreshData();
				this.onDataChanged.next();
				this.successfullOperation();
			},
			(error) => {
				console.error(error);
				requestProcessing.afterGetResponse(event);
				this.operationFailed();
			}
		);
	}
	backToList() {
		this.isAddEditMode = false;
		this.rootCause = new EcQmRootCause();
	}

	displayAdd() {
		this.rootCause = new EcQmRootCause();
		this.isEditMode = false;
		this.isAddEditMode = true;
	}

	displayEdit(rootcauses: EcQmRootCause) {
		this.rootCause = rootcauses;
		this.isAddEditMode = true;
		this.isEditMode = true;
	}

	successfullOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	operationFailed() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}
}
