import { Component, OnInit, ViewChild, ChangeDetectorRef } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { SearchCriteriaQualityManagement } from "@app/utils/SearchCriteriaQualityManagement";
import { Modules, TypeOfEvent } from "@app/utils/enumeration";
import { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { TranslateService } from "@ngx-translate/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { NgbTabset } from "@ng-bootstrap/ng-bootstrap";
import { Location } from "@angular/common";

@Component({
	selector: "app-dashboard",
	templateUrl: "./dashboard.component.html",
	styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent extends ConnectedUserComponent implements OnInit {
	Modules = Modules;
	searchInput: SearchCriteriaQualityManagement;

	TypeOfEvent = TypeOfEvent;

	globalSearchCriteria: SearchCriteriaQualityManagement;
	currentJustify: true;
	listFields: Array<IField>;
	listCategorie: Array<EbCategorie>;
	rootcauseNum: number;
	index: string = "incidents";

	@ViewChild("tabset", { static: true })
	public tabs: NgbTabset;

	public module: number;

	selectedIncidentNum: number;

	selectedDeviationNum: number;

	constructor(
		protected headerService: HeaderService,
		protected translate: TranslateService,
		protected router: Router,
		protected cd: ChangeDetectorRef,
		protected route: ActivatedRoute,
		protected location: Location
	) {
		super();
	}

	ngOnInit() {
		this.route.params.subscribe((params: Params) => {
			if (params["rootcauseNum"] == "rootCauses") {
				this.tabs.select("rootCauses");
			} else if (params["rootcauseNum"] == "deviations") {
				this.tabs.select("deviations");
			}
		});
		this.module = Modules.QUALITY_MANAGEMENT;
		this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());
	}



	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				icon: "my-icon-claims ",
				label: "MODULES.QM",
			},
		];
	}

	search(searchInput: SearchCriteriaQualityManagement) {
		this.searchInput = searchInput;
	}

	paramFieldsHandler(pListFields) {
		this.listFields = pListFields;
	}
	CategorieHandler(plistCategorie) {
		this.listCategorie = plistCategorie;
	}

	onTabChange($event) {
		if (this.route.snapshot.params["rootcauseNum"]) {
			if ($event.nextId === "rootCauses") {
				this.router.navigateByUrl("app/quality-management/dashboard/rootCauses");
			} else if ($event.nextId === "deviations") {
				this.router.navigateByUrl("app/quality-management/dashboard/deviations");
			}
		}

	}

	onTabClick($event){
		this.location.replaceState('/app/quality-management/dashboard');
		this.router.navigate(['.'], { relativeTo: this.route, queryParams: {} });
	}
}
