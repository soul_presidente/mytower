import { Component, Inject, OnInit, ViewChild, Input } from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { EbQmIncident } from "@app/classes/ebIncident";
import { EbMarchandise } from "@app/classes/marchandise";
import { TranslateService } from "@ngx-translate/core";
import { SharedDatasQualityManagemnt } from "@app/services/share.datas-qm";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { Message } from "primeng/components/common/api";
import { ModalService } from "@app/shared/modal/modal.service";
import { MessageService } from "primeng/api";
import { Router } from "@angular/router";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { EbDemande } from "@app/classes/demande";
import { IField } from "@app/classes/customField";
import { RootCausesAnalysisComponent } from "./root-causes-analysis/root-causes-analysis.component";
import { DocumentsComponent } from "@app/shared/documents/documents.component";
import { FichierJoint } from "@app/classes/fichiersJoint";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { HeaderService } from "@app/services/header.service";
import { BreadcumbComponent, HeaderInfos } from "@app/shared/header/header-infos";
import { EbChat } from "@app/classes/chat";
import { ChatPanelComponent } from "@app/shared/chat-panel/chat-panel.component";
import { Statique } from "@app/utils/statique";
import { IncidentLine } from "@app/classes/incidentLine";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbTtCategorieDeviation } from "@app/classes/categorieDeviation";
import { StatiqueService } from "@app/services/statique.service";
import { TypeOfEvent } from "@app/utils/enumeration";

@Component({
	selector: "app-create",
	templateUrl: "./create.component.html",
	styleUrls: ["./create.component.scss"],
})
export class CreateComponent extends ConnectedUserComponent implements OnInit {
	currentStep: string;
	stepIdentification: boolean;
	stepIndividual: boolean;
	stepDone: boolean;
	nextDisabled: boolean;
	currentIncidentIndex: number = -1;
	incidents: EbQmIncident[] = [];
	incident: EbQmIncident = new EbQmIncident();

	allSteps: string[] = ["Identification", "Individual", "Done"];
	marchandise: EbMarchandise = new EbMarchandise();
	listMarchandise: EbMarchandise[];
	step: number = 0;
	msgs: Message[] = [];
	dialogHeader: string;
	dialogContent: string;

	demande: EbDemande = new EbDemande();

	listFields: Array<IField>;

	@ViewChild(DocumentsComponent, { static: false })
	documentsComponent: DocumentsComponent;

	@ViewChild("rootCausesAnalysis", { static: false })
	rootCausesAnalysisComponent: RootCausesAnalysisComponent;

	@ViewChild("chatPanelComponent", { static: false })
	chatPanelComponent: ChatPanelComponent;

	eventType: number;
	listIncidentCategory: Array<EbTtCategorieDeviation> = [];

	constructor(
		private qualityManagementService: QualityManagementService,
		private shareData: SharedDatasQualityManagemnt,
		private messageService: MessageService,
		private router: Router,
		private headerService: HeaderService,
		@Inject(DOCUMENT) private document: any,
		private translate?: TranslateService,
		private modalService?: ModalService,
		private statiqueService?: StatiqueService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.HEADER
		);
		this.currentStep = "Identification";
		this.goToStep(this.currentStep);

		this.eventType = history.state.eventType;
		if (!this.eventType) this.eventType = TypeOfEvent.INCIDENT;
		this.statiqueService
			.getListCategorieDeviation(this.eventType)
			.subscribe((res: Array<EbTtCategorieDeviation>) => {
				this.listIncidentCategory = res;
			});
	}

	getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "GENERAL.CLAIM",
				path: "app/quality-management/dashboard",
			},
			{
				label: "GENERAL.INCIDENT",
			},
		];
	}
	cancelIncidentsave() {
		let that = this;
		if (that.incidents.length > 0) {
			that.dialogHeader = "Annulation de l'enregistrement";
			that.dialogContent = "Voulez-vous annuler la creation de l'incident ?";

			that.modalService.confirm(
				that.dialogHeader,
				that.dialogContent,
				function() {
					that.router.navigate(["app/quality-management/dashboard"]);
				},
				function() {}
			);
		} else {
			that.router.navigate(["app/quality-management/dashboard"]);
		}
	}

	saveForm() {
		let that = this,
			libelle: string = "";
		this.dialogHeader = "Enregistrement du formulaire";
		that.modalService.saveFromQualitaire(libelle, this.dialogHeader, function(
			result: Object,
			event,
			modalObserver: any
		) {
			that.messageService.add({
				severity: "success",
				summary: "Action reussie",
				detail: "Formulaire enregistré avec success !",
			});
			modalObserver.subject.next();
		});
	}

	saveIncidents() {
		let that = this;
		let requestProcessing = new RequestProcessing();

		if (that.incidents.length > 0) {
			let listRootCausesNums: Array<Number> = new Array();
			for (let incident of that.incidents) {
				incident.eventType = this.eventType;
				if (incident.rootCauses && incident.rootCauses.length > 0) {
					incident.rootCauses.forEach((rootCause) => {
						listRootCausesNums.push(rootCause.qmRootCauseNum);
					});
					incident.listRootCauses = ":" + listRootCausesNums.join(":") + ":";
				}
			}

			that.qualityManagementService.saveIncidents(that.incidents, false).subscribe(
				(res) => {
					requestProcessing.beforeSendRequest(event);
					this.successfullOperation();
					requestProcessing.afterGetResponse(event);
					this.router.navigateByUrl("app/quality-management/dashboard");
				},
				(error) => {
					console.error(error);
					this.echecOperation();
					requestProcessing.afterGetResponse(event);
				}
			);
		}
	}

	echecOperation() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}

	successfullOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	nextStep() {
		if (this.incidents.length > 0) {
			window.scrollTo(0, 0);
			for (let i = 0; i < this.allSteps.length; i++) {
				if (this.allSteps[i] == this.currentStep) {
					localStorage.setItem("currentStep", this.currentStep);
					this.currentStep = this.allSteps[i + 1];
					i = this.allSteps.length;
				}
			}
			this.goToStep(this.currentStep);
		}
	}

	emitIncidentNavigation() {
		let event = new CustomEvent("incidentNavigation", null);
		document.dispatchEvent(event);
	}

	emitNextStepAction() {
		let event = new CustomEvent("nextStep", null);
		document.dispatchEvent(event);
	}

	goToStep(target: string) {
		this.currentStep = target;
		this.allSteps.forEach((step) => {
			if (step == target) {
				this["step" + step] = true;
			} else {
				this["step" + step] = false;
			}
		});

		if (this.currentStep == "Individual") {
			this.step = 3;
			this.currentIncidentIndex = 0;
			let incident = this.incidents[this.currentIncidentIndex];
			let searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();
			searchCriteriaPricingBooking.ebDemandeNum = incident.xEbDemande;
			this.qualityManagementService.demande(searchCriteriaPricingBooking).subscribe((data) => {
				this.demande = data;
				this.listFields =
					data && data.customField && data.customField.fields
						? JSON.parse(data.customField.fields)
						: this.demande.customFields
						? JSON.parse(this.demande.customFields)
						: new Array<IField>();
				this.listFields = this.listFields || new Array<IField>();

				this.demande.listCustomsFields = this.listFields;
				data.listCustomsFields = this.listFields;
				this.incidentNavigation(this.currentIncidentIndex);
				this.emitNextStepAction();
			});
		}
		if (this.currentStep == "Identification") {
			this.step = 1;
		}
		if (this.currentStep == "Done") {
			this.step = 4;
			this.nextDisabled = true;
			this.incidentNavigation(-1);
		} else {
			this.nextDisabled = false;
		}
	}

	menuNavigation(target: string, e) {
		e.preventDefault();
		if (target != "Identification") {
			if (this.incidents.length > 0) {
				this.goToStep(target);
			}
		} else {
			this.goToStep(target);
		}
	}

	previousIncident() {
		this.currentIncidentIndex--;
		this.incidentNavigation(this.currentIncidentIndex);
		this.emitIncidentNavigation();
	}

	nextIncident() {
		this.currentIncidentIndex++;
		this.incidentNavigation(this.currentIncidentIndex);
		this.emitIncidentNavigation();
	}

	incidentNavigation(target: number) {
		this.currentIncidentIndex = target;
		let elts = document.getElementsByClassName("tr-zone");
		for (let i = 0; i < elts.length; i++) {
			if (target == i) {
				elts[i].className = "col-md-2 tr-zone selected";
			} else {
				elts[i].className = "col-md-2 tr-zone";
			}
		}
	}

	eventHandler(event: EbQmIncident[]) {
		this.incidents = event;
	}

	recupMarchandises(event) {
		this.listMarchandise = event;
	}

	displayUnit(event) {
		this.marchandise = event;
	}

	updateUnitValue(event) {
		this.marchandise = event;
	}

	getIncidentListFromZone(event) {
		this.incidents = event;
		this.currentIncidentIndex--;
		if (this.incidents.length == 0) {
			this.goToStep("Identification");
		}
	}

	displayIncidents(event) {
		this.incidents = event;
	}

	updateChatEmitter(ebChat: EbChat) {
		if (Statique.isDefined(this.chatPanelComponent) && Statique.isDefined(ebChat))
			this.chatPanelComponent.insertChat(ebChat);
	}

	deleteIncident(event) {
		if (event) {
			let unit: EbMarchandise = event as EbMarchandise;
			let incident: EbQmIncident = this.incidents.find((incd) =>
				incd.incidentLines.find((il) => il.ebMarchandiseNum == unit.ebMarchandiseNum)
			);
			if (incident) {
				let incidentLine: IncidentLine = incident.incidentLines.find(
					(il) => il.ebMarchandiseNum == unit.ebMarchandiseNum
				);
				let lineIndex: number = incident.incidentLines.indexOf(incidentLine);
				incident.incidentLines.splice(lineIndex, 1);
				if (incident.incidentLines.length == 0) {
					let incidentIndex = this.incidents.indexOf(incident);
					this.incidents.splice(incidentIndex, 1);
				}
				this.shareData.deleteMArchandise(incidentLine.ebMarchandiseNum);
				this.shareData.deleteData(incidentLine.ebMarchandiseNum);
			}
		}
	}

	// displayRootInfo(event) {
	//   let rootInfo = event;
	//   if (rootInfo.step == 3) {
	//     this.setRootCausesInformation(rootInfo.currentIncidentIndex, rootInfo);
	//   }
	//   if (rootInfo.step == 2) {
	//     for (let i = 0; i < this.incidents.length; i++) {
	//       this.setRootCausesInformation(i, rootInfo);
	//     }
	//   }

	//   rootInfo.rootCauses.forEach(root => {
	//     if (!root.qmRootCauseNum) this.newRootCauses.push(root);
	//   });
	// }

	// setRootCausesInformation(i: number, rootInfo) {
	//   this.incidents[i].rootCauses = rootInfo.rootCauses;
	//   this.incidents[i].thirdPartyComment = rootInfo.thirdPartyComment;
	//   this.incidents[i].etablissementComment = rootInfo.etablissementComment;
	//   this.incidents[i].waitingForText = rootInfo.waitingForText;
	//   this.incidents[i].rootCauseStatut = rootInfo.rootCauseStatut;
	// }
}
