import { EbUser } from './../../../../classes/user';
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { EbQmIncident } from "@app/classes/ebIncident";
import { EcQmRootCause } from "@app/classes/rootCause";
import { StatiqueService } from "@app/services/statique.service";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { Statique, keyValue } from "@app/utils/statique";
import { EbDemande } from '@app/classes/demande';

@Component({
	selector: "app-root-causes-analysis",
	templateUrl: "./root-causes-analysis.component.html",
	styleUrls: ["./root-causes-analysis.component.css"],
})
export class RootCausesAnalysisComponent implements OnInit {
	@Input()
	incidents: EbQmIncident[];
	@Input()
	step: number;
	@Input()
	set currentIncidentIndex(currentIncidentIndex: number) {
		this._currentIncidentIndex = currentIncidentIndex;
		this.getActualRootCauses();
	}

	@Output()
	emitRootInfo: EventEmitter<any> = new EventEmitter<any>();

	interneIncidentInfo: EbQmIncident = new EbQmIncident();

	listRootCause: Array<EcQmRootCause>;
	rootInfo: any = {};
	_currentIncidentIndex: number;
	@Input()
	eventType: number;

	statique = Statique;
	listPriority: Array<keyValue> = null;
	listRootCauseStatus: Array<keyValue> = null;
	listRootCauseCategories : Array<keyValue> = null;
	listResponsibles : Array<EbUser> = null;
	responsable : string = "";
	displayResponsableInput : boolean = false;

	constructor(
		private statiqueService: StatiqueService,
		private qualityManagementService: QualityManagementService
	) {}

	ngOnInit() {
		this.qualityManagementService.listRootCauseStatusEnum().subscribe((data) => {
			this.listRootCauseStatus = data;
		});
		this.qualityManagementService.listCriticalityIncidentEnum().subscribe((data) => {
			this.listPriority = data;
		});
		this.qualityManagementService.listRootCause(this.eventType).subscribe((data) => {
			this.listRootCause = data;
		});
		this.qualityManagementService.listRootCauseCategoriesEnum().subscribe((data) => {
			this.listRootCauseCategories = data;
		});
		if(this.incidents && this.incidents[this._currentIncidentIndex] && this.incidents[this._currentIncidentIndex].xEbDemande != null){
			this.qualityManagementService.getAllUsersIntervenantInDemande(this.incidents[this._currentIncidentIndex].xEbDemande).subscribe(data => {
				this.listResponsibles = data;
				this.updateResponsableValue();
			});
		}



		document.addEventListener(
			"nextStep",
			function(e) {
				event.preventDefault();
				event.stopPropagation();
				this.rootCausesInformation(this._currentIncidentIndex);
			}.bind(this),
			false
		);

		document.addEventListener(
			"incidentNavigation",
			function(e) {
				event.preventDefault();
				event.stopPropagation();
				this.rootCausesInformation(this._currentIncidentIndex);
			}.bind(this),
			false
		);
	}

	addTag(label) {
		return { qmRootCauseNum: null, label: label, etablissement: null, statut: null };
	}

	getActualRootCauses() {
		this.interneIncidentInfo = this.incidents[this._currentIncidentIndex];
		this.interneIncidentInfo.rootCauses = this.incidents[this._currentIncidentIndex].rootCauses;
		this.interneIncidentInfo.etablissementComment = this.incidents[
			this._currentIncidentIndex
		].etablissementComment;
		this.interneIncidentInfo.thirdPartyComment = this.incidents[
			this._currentIncidentIndex
		].thirdPartyComment;
		this.interneIncidentInfo.waitingForText = this.incidents[
			this._currentIncidentIndex
		].waitingForText;
		this.interneIncidentInfo.rootCauseStatut = this.incidents[
			this._currentIncidentIndex
		].rootCauseStatut;

		if(this.listResponsibles != null)
			this.updateResponsableValue();
	}

	rootCausesInformation(i: number) {
		this.incidents[i].rootCauses = this.interneIncidentInfo.rootCauses;
		this.incidents[i].etablissementComment = this.interneIncidentInfo.etablissementComment;
		this.incidents[i].thirdPartyComment = this.interneIncidentInfo.thirdPartyComment;
		this.incidents[i].waitingForText = this.interneIncidentInfo.waitingForText;
		this.incidents[i].rootCauseStatut = this.interneIncidentInfo.rootCauseStatut;
		this.emitRootInfo.emit(this.interneIncidentInfo);
		localStorage.setItem("rootCauses", JSON.stringify(this.interneIncidentInfo.rootCauses));
	}

	setResponsableValue(){
		this.displayResponsableInput = this.responsable == null;
		this.interneIncidentInfo.responsable = this.responsable;
	}

	updateResponsableValue(){
		if(Statique.isDefined(this.incidents[this._currentIncidentIndex].responsable)){
			let user = this.listResponsibles.find(u => u.username == this.incidents[this._currentIncidentIndex].responsable);
			if(!Statique.isDefined(user)){
				this.displayResponsableInput = true;
				this.responsable = null;
			}
			else{
				this.responsable = this.incidents[this._currentIncidentIndex].responsable;
				this.displayResponsableInput = false;
			}
		}else{
			this.responsable = "";
			this.displayResponsableInput = false;
		}
	}
}
