import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { EbQmIncident } from "@app/classes/ebIncident";
import { IncidentLine } from "@app/classes/incidentLine";
import { EbMarchandise } from "@app/classes/marchandise";
import { SharedDatasQualityManagemnt } from "@app/services/share.datas-qm";

@Component({
	selector: "app-zones-incident",
	templateUrl: "./zones-incident.component.html",
	styleUrls: ["./zones-incident.component.scss"],
})
export class ZonesIncidentComponent implements OnInit {
	@Input()
	incidents: EbQmIncident[];
	@Input()
	size: number;
	@Input()
	step: number;
	@Output()
	emitmarchandise: EventEmitter<EbMarchandise> = new EventEmitter<EbMarchandise>();
	@Output()
	emitIncidentList: EventEmitter<EbQmIncident[]> = new EventEmitter<EbQmIncident[]>();

	constructor(private shareData: SharedDatasQualityManagemnt) {}

	ngOnInit() {}

	deleteIncidentLines(incidentIndex: number, lineIndex: number) {
		let incident: EbQmIncident = this.incidents[incidentIndex];
		let incidentLine: IncidentLine = incident.incidentLines[lineIndex];
		let marchandise: EbMarchandise = this.shareData.getMarchandiseByNum(
			incidentLine.ebMarchandiseNum
		);
		this.emitmarchandise.emit(marchandise);
		incident.incidentLines.splice(lineIndex, 1);
		if (incident.incidentLines.length == 0) {
			this.incidents.splice(incidentIndex, 1);
		}
		this.shareData.deleteMArchandise(incidentLine.ebMarchandiseNum);
		this.shareData.deleteData(incidentLine.ebMarchandiseNum);
		this.emitIncidentList.emit(this.incidents);
	}
}
