import { Statique } from "@app/utils/statique";
import {
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ViewChild,
	OnChanges,
	SimpleChanges,
} from "@angular/core";
import { EcCountry } from "@app/classes/country";
import { StatiqueService } from "@app/services/statique.service";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { SearchCriteriaQualityManagement } from "@app/utils/SearchCriteriaQualityManagement";
import { EbMarchandise } from "@app/classes/marchandise";
import { IncidentLine } from "@app/classes/incidentLine";
import { EbQmIncident } from "@app/classes/ebIncident";
import { SharedDatasQualityManagemnt } from "@app/services/share.datas-qm";
import { AuthenticationService } from "@app/services/authentication.service";
import SingletonStatique from "@app/utils/SingletonStatique";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { Subject } from "rxjs";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";

@Component({
	selector: "app-search-unit",
	templateUrl: "./search-unit.component.html",
	styleUrls: ["./search-unit.component.css"],
})
export class SearchUnitComponent implements OnInit, OnChanges {
	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	searchCriteria: SearchCriteriaQualityManagement = new SearchCriteriaQualityManagement();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	searchInput: SearchCriteria = new SearchCriteria();

	listPays: Array<EcCountry> = new Array<EcCountry>();
	listMarchandise: Array<EbMarchandise> = new Array<EbMarchandise>();
	listIncidentline: Array<IncidentLine> = new Array<IncidentLine>();

	@Input()
	unit: EbMarchandise;

	@Output()
	incidents: EventEmitter<EbQmIncident[]> = new EventEmitter<EbQmIncident[]>();
	@Input()
	listIncidents: Array<EbQmIncident>;
	@Output()
	emitListMarchandise: EventEmitter<EbMarchandise[]> = new EventEmitter<EbMarchandise[]>();
	@Output()
	emitUpdateUnit: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	emitDeleteIncident: EventEmitter<EbMarchandise> = new EventEmitter<EbMarchandise>();

	StatiqueService: StatiqueService;
	marchandiseIdNumbers: Array<number> = new Array<number>();

	search: Subject<string> = new Subject();
	addedUnits: Array<EbMarchandise> = new Array<EbMarchandise>();

	constructor(
		private statiqueService: StatiqueService,
		private shareData: SharedDatasQualityManagemnt,
		private qualityManagementService: QualityManagementService
	) {
		this.search
			.pipe(
				debounceTime(1000),
				distinctUntilChanged()
			)
			.subscribe((value) => {
				let criteria = new SearchCriteriaQualityManagement();
				criteria.searchterm = value;
				criteria.marchandiseIdNumbers = this.shareData.notExpectedID;
				this.qualityManagementService.listMarchandise(criteria).subscribe((data) => {
					this.listMarchandise = data;
				});
			});
	}

	ngOnInit() {
		(async () => (this.listPays = await SingletonStatique.getListEcCountry()))();
		this.initInfo();
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["unit"] && this.unit) {
			this.unCheckUnit(this.unit);
			if (this.addedUnits.length > 0) {
				let marchandise: EbMarchandise = this.addedUnits.find(
					(u) => u.ebMarchandiseNum == this.unit.ebMarchandiseNum
				);
				let unitIndex: number = this.addedUnits.indexOf(marchandise);
				this.addedUnits.splice(unitIndex, 1);
			}
		}
	}

	initInfo() {
		window.functions = window.functions || {};
		window.functions.addLineToIncident = this.addLineToIncident.bind(this);
		this.searchCriteria.withDeactive = true;
		this.searchCriteria.pageNumber = 1;
		this.dataInfos.cols = [
			{
				field: "refTransport",
				translateCode: "QUALITY_MANAGEMENT.TRANSPORT_REF",
				render: function(data, display, row) {
					return row.ebDemande && row.ebDemande.refTransport ? row.ebDemande.refTransport : "";
				}.bind(this),
			},
			{
				field: "customerReference",
				translateCode: "QUALITY_MANAGEMENT.UNIT_REF_LABEL",
			},
			{
				field: "partNumber",
				translateCode: "CONTROL_RULES.PART",
			},
			{
				field: "serialNumber",
				translateCode: "PRICING_BOOKING.SERIAL_NUMBER",
			},
			{
				field: "numAwbBol",
				translateCode: "PRICING_BOOKING.NUM_AWB_BOL",
				render: function(data, display, row) {
					return row.ebDemande && row.ebDemande.numAwbBol ? row.ebDemande.numAwbBol : "";
				}.bind(this),
			},
			{
				field: "dmdCustomerReference",
				translateCode: "TRACK_TRACE.CUSTOMER_REFERENCE",
				render: function(data, display, row) {
					return row.ebDemande && row.ebDemande.customerReference
						? row.ebDemande.customerReference
						: "";
				}.bind(this),
			},
			{
				field: "libelleOriginCountry",
				translateCode: "QUALITY_MANAGEMENT.ORIGIN_COUNTRY",
				render: function(data, display, row) {
					return row.ebDemande && row.ebDemande.libelleOriginCountry
						? row.ebDemande.libelleOriginCountry
						: "";
				}.bind(this),
			},
		];

		this.dataInfos.dataKey = "ebMarchandiseNum";
		this.dataInfos.dataType = EbMarchandise;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.showAddBtn = false;
		this.dataInfos.showEditBtn = false;
		this.dataInfos.showDeleteBtn = false;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerQualityManagement + "/list-units";
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = false;
		this.dataInfos.showDeleteBtn = false;
		this.dataInfos.showCheckbox = true;
	}

	// getMarchandise(criteria: SearchCriteriaQualityManagement, event : any) {
	// }

	async addLineToIncident(marchandise: EbMarchandise) {
		let incidentLine: IncidentLine = new IncidentLine(marchandise);

		let el = this.listIncidents.find((it) => it.demandeTranportRef == incidentLine.refTransport);
		if (el == undefined || this.listIncidents.length == 0) {
			let incident: EbQmIncident = new EbQmIncident();
			incident.xEbDemande = marchandise.ebDemande.ebDemandeNum;
			incident.demandeTranportRef = marchandise.ebDemande.refTransport;
			incident.incidentRef = "INC-" + incident.demandeTranportRef;
			incident.issuer = AuthenticationService.getConnectedUser();
			incident.issuerNomPrenom = incident.issuer.nom + " " + incident.issuer.prenom;
			incident.currency = JSON.parse(JSON.stringify(marchandise.ebDemande.xecCurrencyInvoice));

			await this.qualityManagementService
				.getNextEbQmIncidentNum()
				.subscribe((ebQmIncidentNum: number) => {
					incident.ebQmIncidentNum = ebQmIncidentNum;
				});

			incident.incidentLines.push(incidentLine);
			this.listIncidents.push(incident);
			this.shareData.insertData(marchandise.ebMarchandiseNum);
			this.shareData.addMarchandise(marchandise);
		} else if (el != undefined) {
			let line = el.incidentLines.find(
				(il) => il.ebMarchandiseNum == incidentLine.ebMarchandiseNum
			);
			if (line == undefined) {
				this.shareData.insertData(marchandise.ebMarchandiseNum);
				this.shareData.addMarchandise(marchandise);
				el.incidentLines.push(incidentLine);
			} else if (line != undefined) {
				console.log("Marchandise déja ajoutée");
			}
		}
		this.marchandiseIdNumbers.push(marchandise.ebMarchandiseNum);
		this.incidents.emit(this.listIncidents);
	}

	onDataLoaded(data: Array<EbMarchandise>) {
		this.listMarchandise = data;
		this.emitListMarchandise.emit(this.listMarchandise);
	}

	onUnitChecked(event) {
		if (event) {
			let unit: EbMarchandise = event.rowModel as EbMarchandise;
			if (this.genericTable.isRowChecked(unit, this.dataInfos)) {
				this.addLineToIncident(unit);
				this.addedUnits.push(unit);
			} else {
				this.deleteIncident(unit);
				let marchandise: EbMarchandise = this.addedUnits.find(
					(u) => u.ebMarchandiseNum == unit.ebMarchandiseNum
				);
				let unitIndex: number = this.addedUnits.indexOf(marchandise);
				this.addedUnits.splice(unitIndex, 1);
			}
		} else {
			if (this.genericTable.allChecked) {
				this.listMarchandise.forEach((u) => {
					if (!this.addedUnits.find((m) => m.ebMarchandiseNum == u.ebMarchandiseNum)) {
						this.addLineToIncident(u);
						this.addedUnits.push(u);
					}
				});
			} else {
				this.addedUnits.forEach((unit) => {
					this.deleteIncident(unit);
				});
				this.addedUnits = new Array<EbMarchandise>();
			}
		}
	}

	unCheckUnit(unit: EbMarchandise) {
		this.genericTable.deleteMapAttribute(unit.ebMarchandiseNum, "checked");
		this.genericTable.allChecked = false;
		this.emitUpdateUnit.emit(null);
	}

	deleteIncident(unit: EbMarchandise) {
		this.emitDeleteIncident.emit(unit);
	}
}
