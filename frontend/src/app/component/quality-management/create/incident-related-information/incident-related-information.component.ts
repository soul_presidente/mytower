import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, OnChanges } from "@angular/core";
import { FileType, TypeOfEvent } from "@app/utils/enumeration";
import { EbQmIncident, OtherCost } from "@app/classes/ebIncident";
import { EcQmRootCause } from "@app/classes/rootCause";
import { EcQmCategory } from "@app/classes/category";
import { EcCurrency } from "@app/classes/currency";
import { StatiqueService } from "@app/services/statique.service";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { IncidentLine } from "@app/classes/incidentLine";
import { Statique, keyValue } from "@app/utils/statique";
import SingletonStatique from "@app/utils/SingletonStatique";
import { Message, MessageService } from "primeng/api";
import { SearchCriteriaQualityManagement } from "@app/utils/SearchCriteriaQualityManagement";
import { EbMarchandise } from "@app/classes/marchandise";
import { Router } from "@angular/router";
import { EbDemande } from "@app/classes/demande";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { EbTtCategorieDeviation } from "@app/classes/categorieDeviation";

@Component({
	selector: "app-incident-related-information",
	templateUrl: "./incident-related-information.component.html",
	styleUrls: ["./incident-related-information.component.css"],
})
export class IncidentRelatedInformationComponent implements OnInit {
	@Input()
	incidents: EbQmIncident[];
	@Input()
	step: number;
	@Input()
	set currentIncidentIndex(currentIncidentIndex: number) {
		this._currentIncidentIndex = currentIncidentIndex;
		this.getIncidentsLines();
	}

	interneIncidentInfo: EbQmIncident = new EbQmIncident();

	@Output()
	emitIncidents: EventEmitter<EbQmIncident[]> = new EventEmitter<EbQmIncident[]>();

	@Output()
	emitIncident: EventEmitter<EbQmIncident> = new EventEmitter<EbQmIncident>();
	listRootCause: Array<EcQmRootCause>;
	@Input()
	listIncidentCategory: Array<EbTtCategorieDeviation>;
	listCurrency: Array<EcCurrency>;
	listIncidentLines: Array<IncidentLine> = new Array<IncidentLine>();
	marchandiseTypeOfUnit: any = Statique.MarchandiseTypeOfUnit;
	// on récupére plus la liste des marchandiseDangerousGood depuis le front .. c'est déplacer dans le back
	marchandiseDangerousGood: Map<number, string>;
	_currentIncidentIndex: number;

	toggleEditData: boolean = true;
	Statique = Statique;
	yesOrNo: any = Statique.yesOrNoWithNumberKey;
	categories: any[] = [];
	insurance: boolean;
	totalQteOfUnit: number = 0;
	totalWeight: number = 0;
	showImpact: boolean = false;
	showDesc: boolean = false;

	documentsToShow: Array<number> = new Array<number>();
	hasContributionAccess: boolean = true;

	delayRequired: boolean = false;
	unitDamagedRequired: boolean = false;
	unitLostRequired: boolean = false;
	docRequired: boolean = false;

	otherCosts: Array<OtherCost> = new Array<OtherCost>();

	msgs: Message[] = [];
	totalUnits: number;
	criteria: SearchCriteriaQualityManagement = new SearchCriteriaQualityManagement();
	category: EcQmCategory = new EcQmCategory();

	isWorking: boolean = false;
	listMarchandises: EbMarchandise[] = [];

	showAddAndDeleteUnit: boolean = false;

	bottom: boolean = true;

	selectedUnit: number;

	listCriticality : Array<keyValue> = null;

	demande: EbDemande;

	percentage = "%";

	totalQteOfUnitImpacted: number;
	totalWeightImpacted: number;

	@Input()
	eventType: number;

	constructor(
		private statiqueService: StatiqueService,
		private qualityManagementService: QualityManagementService,
		private router: Router,
		private messageService: MessageService
	) {}

	ngOnInit() {

		this.qualityManagementService.listCriticalityIncidentEnum().subscribe((data)=>
		{
			this.listCriticality = data;
		});
		(async () => {
			this.listCurrency = await SingletonStatique.getListEcCurrency();
			//this.interneIncidentInfo.currency = (():EcCurrency => JSON.parse(JSON.stringify(this.listCurrency[43])))();
		})();

		this.qualityManagementService.listRootCause(this.eventType).subscribe((data) => {
			this.listRootCause = data;
		});

		this.documentsToShow.push(FileType.POD);

		document.addEventListener(
			"nextStep",
			function(e) {
				event.preventDefault();
				event.stopPropagation();
				this.getIncidentsLines();
			}.bind(this),
			false
		);

		document.addEventListener(
			"incidentNavigation",
			function(e) {
				event.preventDefault();
				event.stopPropagation();
				this.incidentInformation();
			}.bind(this),
			false
		);

		if (this.router.url.includes("app/quality-management/details"))
			this.showAddAndDeleteUnit = true;

		let searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();
		searchCriteriaPricingBooking.ebDemandeNum = this.incidents[0].xEbDemande;
		this.qualityManagementService.demande(searchCriteriaPricingBooking).subscribe((data) => {
			this.demande = data;
		});
		this.fillListMarchandiseDangerousGoods();
	}

	showOrHide(target) {
		this["show" + target] = !this["show" + target];
	}

	affectInsurance() {
		this.interneIncidentInfo.insurance ? 1 : 0;
	}

	changeCategory(qmCategorieNum) {
		this.delayRequired = false;
		this.unitLostRequired = false;
		this.unitDamagedRequired = false;
		if (qmCategorieNum == 2) {
			this.delayRequired = true;
		}
		if (qmCategorieNum == 3) {
		}
		if (qmCategorieNum == 4) {
		}
		if (qmCategorieNum == 5) {
			this.docRequired = true;
		}
		if (qmCategorieNum == 6) {
			this.unitDamagedRequired = true;
			this.unitLostRequired = true;
			this.docRequired = true;
		}
		if (qmCategorieNum == 7) {
		}
		if (qmCategorieNum == 8) {
		}
		if (qmCategorieNum == 9) {
			this.unitDamagedRequired = true;
			this.unitLostRequired = true;
		}
		if (qmCategorieNum == 11) {
			this.delayRequired = true;
		}
		if (qmCategorieNum == 12) {
			this.delayRequired = true;
		}
		if (qmCategorieNum == 13) {
			this.delayRequired = true;
		}
	}

	getIncidentsLines() {
		this.listIncidentLines = [];
		if (this.step == 3) {
			let incident = this.incidents[this._currentIncidentIndex];
			this.listIncidentLines.push.apply(this.listIncidentLines, incident.incidentLines);
			this.interneIncidentInfo = this.incidents[this._currentIncidentIndex];

			let searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();
			searchCriteriaPricingBooking.ebDemandeNum = this.incidents[
				this._currentIncidentIndex
			].xEbDemande;
			this.qualityManagementService.demande(searchCriteriaPricingBooking).subscribe((data) => {
				this.demande = data;
			});
		}
		if (this.step == 2) {
			for (let i = 0; i < this.incidents.length; i++) {
				let incident = this.incidents[i];
				this.listIncidentLines.push.apply(this.listIncidentLines, incident.incidentLines);
			}
		}

		this.totalQteOfUnit = 0;
		this.totalWeight = 0;
		this.totalQteOfUnitImpacted = 0;
		this.totalWeightImpacted = 0;
		this.listIncidentLines.forEach((line) => {
			this.totalQteOfUnit += line.numberOfUnits;
			this.totalWeight += line.weight;
			this.totalQteOfUnitImpacted += line.numberOfUnitsImpacted;
			this.totalWeightImpacted += line.weightImpacted;
		});
	}

	changeTotalQte(qte) {
		this.totalQteOfUnit = 0;
		this.listIncidentLines.forEach((line) => {
			this.totalQteOfUnit += line.numberOfUnits;
		});
	}

	changeTotalWe(weight) {
		this.totalQteOfUnitImpacted = 0;
		this.listIncidentLines.forEach((line) => {
			this.totalQteOfUnitImpacted += line.numberOfUnitsImpacted;
		});
	}

	changeImpactedUnitsTotal() {
		this.totalWeightImpacted = 0;
		this.listIncidentLines.forEach((line) => {
			this.totalWeightImpacted += line.weightImpacted;
		});
	}

	changeImpactedWeightTotal() {
		this.totalWeight = 0;
		this.listIncidentLines.forEach((line) => {
			this.totalWeight += line.weight;
		});
	}

	incidentInformation() {
		if (this.step == 3) {
			this.setIncidentInformation(this._currentIncidentIndex);
		}
		this.emitIncidents.emit(this.incidents);
		localStorage.setItem("interneIncidentInfo", JSON.stringify(this.interneIncidentInfo));
	}

	setIncidentInformation(i: number) {
		this.incidents[i].category = this.interneIncidentInfo.category;
		this.incidents[i].incidentDesc = this.interneIncidentInfo.incidentDesc;
		this.incidents[i].addImpact = this.interneIncidentInfo.addImpact;
		this.incidents[i].claimAmount = this.interneIncidentInfo.claimAmount;
		this.incidents[i].recovered = this.interneIncidentInfo.recovered;
		this.incidents[i].recoveredInsurance = this.interneIncidentInfo.recoveredInsurance;
		if (this.incidents[i].claimAmount) {
			this.incidents[i].balance =
				this.incidents[i].claimAmount -
				this.interneIncidentInfo.recovered -
				this.interneIncidentInfo.recoveredInsurance;
		}
		this.incidents[i].rootCauses = this.interneIncidentInfo.rootCauses;
		this.incidents[i].insurance = this.interneIncidentInfo.insurance;
		this.incidents[i].goodValues = this.interneIncidentInfo.goodValues;
		this.incidents[i].daysDelay = this.interneIncidentInfo.daysDelay;
		this.incidents[i].hoursDelay = this.interneIncidentInfo.hoursDelay;
		this.incidents[i].unitDamaged = this.interneIncidentInfo.unitDamaged;
		this.incidents[i].unitDamagedAmount = this.interneIncidentInfo.unitDamagedAmount;
		this.incidents[i].unitLost = this.interneIncidentInfo.unitLost;
		this.incidents[i].amondOfunitLost = this.interneIncidentInfo.amondOfunitLost;
		this.incidents[i].rootCauseStatut = this.interneIncidentInfo.rootCauseStatut;
		this.incidents[i].waitingForText = this.interneIncidentInfo.waitingForText;

		this.incidents[i].incidentLines.forEach((line) => {
			this.listIncidentLines.forEach((lineUpdate) => {
				if (line.ebMarchandiseNum == lineUpdate.ebMarchandiseNum) {
					line.typeMarchandise = lineUpdate.typeMarchandise;
					line.numberOfUnits = lineUpdate.numberOfUnits;
					line.eta = lineUpdate.eta;
					line.ata = lineUpdate.ata;
					line.etd = lineUpdate.etd;
					line.atd = lineUpdate.etd;
					line.dangerousGood = lineUpdate.dangerousGood;
					line.comment = lineUpdate.comment;
				}
			});
		});
		this.incidents[i].othersCost = this.interneIncidentInfo.othersCost;

		this.emitIncident.emit(this.interneIncidentInfo);
	}

	addNewCost() {
		let otherCost: OtherCost = { libelle: null, cost: null, currencyLabel: null };
		if (this.interneIncidentInfo.currency != null)
			otherCost.currencyLabel = this.interneIncidentInfo.currency.code;
		this.interneIncidentInfo.othersCost.push(otherCost);
	}

	deleteCost(i: number) {
		this.interneIncidentInfo.othersCost.splice(i, 1);
	}

	calculClaimAmount() {
		let res: number = 0;
		this.interneIncidentInfo.othersCost.forEach((oc) => {
			res += oc.cost;
		});
		this.interneIncidentInfo.claimAmount =
			this.interneIncidentInfo.impactedTransportAmount +
			this.interneIncidentInfo.unitDamagedAmount +
			res;
	}

	percentageOrCost(percentage: number, transportAmount: number) {
		if (percentage != null) {
			let res = this.interneIncidentInfo.impactedTransportPercentage / 100;
			this.interneIncidentInfo.impactedTransportAmount =
				this.demande.exEbDemandeTransporteurs[0].price * res;
		}
		if (transportAmount != null) {
			let res = 100 / this.demande.exEbDemandeTransporteurs[0].price;
			this.interneIncidentInfo.impactedTransportPercentage =
				this.interneIncidentInfo.impactedTransportAmount * res;
		}
	}

	async addNewIncidentLine() {
		this.criteria.searchterm = null;
		this.isWorking = true;

		this.criteria.ebDemandeNum = this.incidents[0].xEbDemande;
		this.criteria.marchandiseIdNumbers = [];
		this.listIncidentLines.forEach((line) => {
			this.criteria.marchandiseIdNumbers.push(line.ebMarchandiseNum);
		});

		await this.qualityManagementService.listMarchandise(this.criteria).subscribe((data) => {
			if (data.length > 0) {
				this.listMarchandises = data;
				let incidentLine = new IncidentLine(new EbMarchandise());
				this.listIncidentLines.push(incidentLine);
			} else {
				let header = "Action impossible",
					text = "Toutes les marchandises ont déjà été selectionné";
				this.messageService.add({ severity: "info", summary: header, detail: text });
			}
			this.isWorking = false;
		});
	}

	deleteIncident(index) {
		this.listIncidentLines.splice(index, 1);
	}

	onMarchandiseNumChange(index, unitNum: number) {
		let marchandise = this.listMarchandises.find((el) => el.ebMarchandiseNum == unitNum);
		let incidentLine: IncidentLine = new IncidentLine(marchandise);
		let incident = this.incidents[0];
		incidentLine.carrierName = incident.carrierNom;
		this.listIncidentLines[index] = incidentLine;
		incident.incidentLines[index] = incidentLine;
	}

	// cette methode recupére la liste des valeurs de l'enumération depuis le backend
	async fillListMarchandiseDangerousGoods() {
		this.marchandiseDangerousGood = await SingletonStatique.getListMarchandiseDangerousGoods();
	}

	get incidentCategoryLabel(): string{
		return this.eventType == TypeOfEvent.UNCONFORMITY ? 'QUALITY_MANAGEMENT.UNCONFORMITY_CATEGORY_LABEL' : 'QUALITY_MANAGEMENT.INCIDENT_CATEGORY_LABEL';
	}

	get incidentDesignationLabel(): string{
		return this.eventType == TypeOfEvent.UNCONFORMITY ? 'QUALITY_MANAGEMENT.UNCONFORMITY_DESIGNATION' : 'QUALITY_MANAGEMENT.INCIDENT_DESIGNATION';
	}

	get incidentDescriptionLabel(): string{
		return this.eventType == TypeOfEvent.UNCONFORMITY ? 'QUALITY_MANAGEMENT.UNCONFORMITY_DESCRIPTION' : 'QUALITY_MANAGEMENT.INCIDENT_DESCRIPTION';
	}
}
