import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { EbQmIncident } from "@app/classes/ebIncident";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { EbDemande } from "@app/classes/demande";
import { QualityManagementService } from "@app/services/quality-mangement.service.";
import { IField } from "@app/classes/customField";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { Statique } from "@app/utils/statique";

@Component({
	selector: "app-qm-transport-information",
	templateUrl: "./qm-transport-information.component.html",
	styleUrls: ["./qm-transport-information.component.css"],
})
export class QmTransportInformationComponent implements OnInit {
	@Input()
	step: number;
	@Input()
	incidents: EbQmIncident[] = [];
	@Input()
	set currentIncidentIndex(currentIncidentIndex: number) {
		this._currentIncidentIndex = currentIncidentIndex;
		this.incidentInfo();
	}

	incident: EbQmIncident;

	Statique = Statique;

	_currentIncidentIndex: number;
	@Output()
	emitIncidents: EventEmitter<EbQmIncident[]> = new EventEmitter<EbQmIncident[]>();
	searchCriteriaPricingBooking: SearchCriteriaPricingBooking = new SearchCriteriaPricingBooking();

	@Input()
	demande: EbDemande = null;

	listFields: Array<IField>;

	showPrice: boolean = false;
	showPriceExEbDemandeTransporteur: ExEbDemandeTransporteur = null;
	insurance: string;

	constructor(private qms: QualityManagementService) {}

	ngOnInit() {
		document.addEventListener(
			"incidentNavigation",
			function(e) {
				event.preventDefault();
				event.stopPropagation();
				this.incidentInfo();
			}.bind(this),
			false
		);

		if (this.demande != null)
			this.demande.insurance ? (this.insurance = "Yes") : (this.insurance = "No");
	}

	incidentInfo() {
		if (this.incident && this._currentIncidentIndex) {
			this.incident = this.incidents[this._currentIncidentIndex];

			this.searchCriteriaPricingBooking.ebDemandeNum = this.incident.xEbDemande;
			this.qms.demande(this.searchCriteriaPricingBooking).subscribe((data) => {
				this.demande = data;

				this.demande.insurance ? (this.insurance = "Yes") : (this.insurance = "No");

				this.listFields =
					data && data.customField && data.customField.fields
						? JSON.parse(data.customField.fields)
						: this.demande.customFields
						? JSON.parse(this.demande.customFields)
						: new Array<IField>();
				this.listFields = this.listFields || new Array<IField>();

				this.demande.listCustomsFields = this.listFields;
				data.listCustomsFields = this.listFields;
			});
		} else if (this.demande) {
			this.demande.insurance ? (this.insurance = "Yes") : (this.insurance = "No");

			this.listFields = this.demande.listCustomsFields;
		}
	}

	showDetailsPrice() {
		this.showPriceExEbDemandeTransporteur = this.demande.exEbDemandeTransporteurs[0];
		this.showPrice = true;
	}
}
