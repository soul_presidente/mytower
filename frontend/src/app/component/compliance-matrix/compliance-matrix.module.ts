import { NgModule } from "@angular/core";
import { SharedModule } from "@app/shared/module/shared.module";
import { ComplianceMatrixRoutes } from "./compliance-matrix.route";
import { ComplianceMatrixComponent } from "./compliance-matrix.component";
import { ComplianceMatrixService } from "@app/services/complianceMatrix.service";
import { DataTablesModule } from "angular-datatables/src/angular-datatables.module";

@NgModule({
	imports: [ComplianceMatrixRoutes, SharedModule, DataTablesModule],
	declarations: [ComplianceMatrixComponent],
	exports: [],
	providers: [ComplianceMatrixService],
})
export class ComplianceMatrixModule {}
