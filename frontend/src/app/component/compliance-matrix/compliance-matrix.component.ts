import { Component, EventEmitter, OnInit, ViewChild } from "@angular/core";
import { StatiqueService } from "@app/services/statique.service";
import { EcCountry } from "@app/classes/country";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";
import { AccessRights, Modules } from "@app/utils/enumeration";
import { ComplianceMatrixService } from "@app/services/complianceMatrix.service";
import { EbDestinationCountry } from "@app/classes/destinationCountry";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { Statique } from "@app/utils/statique";
import { EbCombinaison } from "@app/classes/combinaison";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { DataTableDirective } from "angular-datatables";
import { DialogService } from "@app/services/dialog.service";
import { ExportService } from "@app/services/export.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { NgSelectComponent } from "@ng-select/ng-select";
import { switchMap } from "rxjs/operators";
import { Subscription } from "rxjs";
import SingletonStatique from "@app/utils/SingletonStatique";
import { CompagnieService } from "@app/services/compagnie.service";
import { EbEtablissement } from "@app/classes/etablissement";
import { EbCompagnie } from "@app/classes/compagnie";

@Component({
	selector: "app-compliance-matrix",
	templateUrl: "./compliance-matrix.component.html",
	styleUrls: ["./compliance-matrix.component.css"],
})
export class ComplianceMatrixComponent extends ConnectedUserComponent implements OnInit {
	Modules = Modules;
	AccessRights = AccessRights;

	listPays: Array<EcCountry>;
	listEtablissement: Array<EbEtablissement>;
	etablissement: EbEtablissement = null;
	ebCompagnie: EbCompagnie = null;
	filteredListPays: Array<EcCountry>;
	selectedDestinationCountry: EbDestinationCountry = null;
	selectedCountryMadeIn: EcCountry = null;
	ebDestinationCountry: EbDestinationCountry = null;
	isAddFicheDestination: boolean = false;
	isEditFicheDestination: boolean = false;
	listDestinationCountry: Array<EbDestinationCountry>;
	filteredListDestinationCountry: Array<EbDestinationCountry>;
	Statique = Statique;
	oldEbDestinationCountry: EbDestinationCountry = new EbDestinationCountry();
	hsCodeField: string;
	selectedIndexDestinationCountry: number = null;
	ebCombinaison: EbCombinaison = null;
	dataTableConfig: DataTableConfig = new DataTableConfig();
	listCombinaison: Array<EbCombinaison>;

	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	@ViewChild("destSelector", { static: true })
	destSelector: NgSelectComponent;

	subscription: Subscription;
	typeahead = new EventEmitter<string>();
	destCountriesCriteria: SearchCriteria = new SearchCriteria();

	constructor(
		protected complianceMatrixService?: ComplianceMatrixService,
		protected compagnieService?: CompagnieService,
		protected statiqueService?: StatiqueService,
		protected generaleMethode?: generaleMethodes,
		protected modalService?: ModalService,
		protected dialogService?: DialogService,
		protected exportService?: ExportService
	) {
		super();
	}

	ngOnInit(): void {
		this.loadListPays();

		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.pageNumber = null;
		searchCriteria.size = null;
		searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.complianceMatrixService.listEbDestinationCountry(searchCriteria).subscribe((data) => {
			this.filteredListDestinationCountry = data;
			this.listDestinationCountry = data;
			if (this.listPays && !this.filteredListPays) this.filteredListPays = this.filterListPays();
		});

		// this.dataTableConfig = new DataTableConfig(this.Statique.controllerComplianceMatrix + '/list-combinaison');
		// this.dataTableConfig.dtOptions.serverSide = true;

		this.typeahead.pipe(switchMap((term) => this.filterListDestCountry(term))).subscribe(
			(items) => {},
			(err) => {
				this.filteredListDestinationCountry = Object.assign([], this.listDestinationCountry);
			}
		);

		this.loadCountries(this.userConnected.ebCompagnie.ebCompagnieNum);
	}

	async loadListPays() {
		this.listPays = await SingletonStatique.getListEcCountry();
		if (this.listDestinationCountry && !this.filteredListPays)
			this.filteredListPays = this.filterListPays();
	}

	filterListDestCountry(term: string): Array<EbDestinationCountry> {
		if (!term || term.length == 0)
			return (this.filteredListDestinationCountry = Object.assign([], this.listDestinationCountry));

		return (this.filteredListDestinationCountry = this.listDestinationCountry.filter(
			(it, index) => {
				if (!it.country.libelle) return false;
				if (it.country.libelle.toLowerCase().indexOf(term.toLowerCase()) >= 0) {
					return true;
				}
			}
		));
	}

	filterListPays() {
		return this.listPays.filter((pays) => {
			return this.listDestinationCountry.find((dest) => {
				return dest.country.ecCountryNum == pays.ecCountryNum;
			})
				? false
				: true;
		});
	}

	panelCollapse(panel, event) {
		this.generaleMethode.panelCollapse(panel, event);
	}

	panelFullScreen(panel) {
		this.generaleMethode.panelFullScreen(panel);
	}

	newDestinationCountry() {
		this.ebDestinationCountry = new EbDestinationCountry();
		this.listCombinaison = [];
		this.isAddFicheDestination = true;
		this.isEditFicheDestination = false;
		this.ebCombinaison = null;
	}

	annulerAddOrEditDestCountry(content: any) {
		this.modalService.confirm(
			"Cancelling",
			"Do you really want to cancel editing this card ?",
			async function(result) {
				if (this.isAddFicheDestination) {
					this.ebDestinationCountry = null;
				}
				if (this.isEditFicheDestination) {
					//this.ebDestinationCountry.constructorCopy(this.oldEbDestinationCountry);
					this.ebDestinationCountry = null;
				}
				this.isAddFicheDestination = false;
				this.isEditFicheDestination = false;

				await Statique.waitForTimemout();
				try {
					let selectedItem = this.destSelector.itemsList.items.find(
						(it) =>
							it.ebDestinationCountryNum == this.selectedDestinationCountry.ebDestinationCountryNum
					);
					this.destSelector.select(selectedItem);
					this.destSelector.changeEvent.emit({ index: selectedItem.index });
				} catch (error) {}
			}.bind(this),
			() => {}
		);
	}

	async saveDestCountry(event: HTMLElement) {
		event.classList.add("disabled");
		if (this.isAddFicheDestination) {
			this.ebDestinationCountry.etablissement = new EbEtablissement();
			if (this.etablissement != null) {
				this.ebDestinationCountry.etablissement = this.etablissement;
			}

			if (this.destCountriesCriteria.ebCompagnieNum != null) {
				this.ebDestinationCountry.etablissement.ebCompagnie = this.userConnected.ebCompagnie;
			}

			this.complianceMatrixService.addDestinationCountry(this.ebDestinationCountry).subscribe(
				async function(data: EbDestinationCountry) {
					event.classList.remove("disabled");

					if (!data) return;
					this.etablissement = null;
					this.isAddFicheDestination = false;
					this.ebDestinationCountry = null;
					data.etablissement = this.etablissement;
					this.listDestinationCountry.push(data);
					this.listDestinationCountry.sort((a, b) =>
						a.country.libelle.localeCompare(b.country.libelle)
					);
					//this.loadCountries(this.destCountriesCriteria.ebCompagnieNum);
					this.filteredListDestinationCountry = [...this.listDestinationCountry];
					this.filteredListPays = this.filteredListPays.filter(
						(it) => it.ecCountryNum != data.country.ecCountryNum
					);

					if (this.destSelector) {
						this.selectedDestinationCountry = this.filteredListDestinationCountry.find(
							(it) => it.ebDestinationCountryNum == data.ebDestinationCountryNum
						);
						this.ebDestinationCountry = this.selectedDestinationCountry;

						// await Statique.waitForTimemout();

						try {
							let selectedItem = this.destSelector.itemsList.items.find(
								(it) => it.ebDestinationCountryNum == data.ebDestinationCountryNum
							);
							this.destSelector.select(selectedItem);
							this.destSelector.changeEvent.emit({ index: selectedItem.index });
						} catch (error) {}
					}
					this.etablissement = null;
					this.loadCountries(this.destCountriesCriteria.ebCompagnieNum);
				}.bind(this)
			);
			this.destCountryChanged(this.ebDestinationCountry);
		} else if (this.isEditFicheDestination) {
			let _ebDestinationCountry: EbDestinationCountry = new EbDestinationCountry();
			_ebDestinationCountry.constructorCopy(this.ebDestinationCountry);
			this.complianceMatrixService
				.editDestinationCountry(_ebDestinationCountry)
				.subscribe((data: EbDestinationCountry) => {
					this.ebDestinationCountry.lastDateModified = data.lastDateModified;
					this.oldEbDestinationCountry.constructorCopy(this.ebDestinationCountry);

					event.classList.remove("disabled");
					this.isEditFicheDestination = false;
					this.destCountryChanged(this.ebDestinationCountry);
				});
		}
	}

	destCountryChanged(event: any) {
		if (this.selectedDestinationCountry != null) {
			this.selectedIndexDestinationCountry = event.index;
			let searchCriteria: SearchCriteria = new SearchCriteria();
			searchCriteria.pageNumber = null;
			searchCriteria.size = null;
			searchCriteria.ebDestinationCountryNum = this.selectedDestinationCountry.ebDestinationCountryNum;

			this.complianceMatrixService
				.getDestinationCountry(searchCriteria)
				.subscribe((data: EbDestinationCountry) => {
					this.ebDestinationCountry = new EbDestinationCountry();
					this.ebDestinationCountry.constructorCopy(data);
					this.oldEbDestinationCountry.constructorCopy(data);

					this.searchCombinaison();
				});
		} else {
			this.ebDestinationCountry = null;
		}

		this.ebCombinaison = null;
	}

	editDestinationCountry() {
		this.isEditFicheDestination = true;
		this.isAddFicheDestination = false;
		this.ebCombinaison = null;
	}

	clearSearch() {
		this.hsCodeField = null;
		this.selectedCountryMadeIn = null;
		this.searchCombinaison();
	}

	deleteDestinationCountry(modal: any) {
		this.modalService.confirm(
			"Suppression de la fiche destination",
			"Voulez-vous vraiment supprimer cette fiche ?",
			function(result) {
				this.complianceMatrixService
					.deleteDestinationCountry(this.ebDestinationCountry)
					.subscribe((data) => {
						if (data) {
							this.filteredListPays.push(this.ebDestinationCountry.country);
							this.listDestinationCountry.splice(this.selectedIndexDestinationCountry, 1);
							this.filteredListDestinationCountry = Object.assign([], this.listDestinationCountry);
							this.typeahead.emit();
							this.ebDestinationCountry = null;
							this.isEditFicheDestination = false;
							this.selectedDestinationCountry = null;
						}
					});
			}.bind(this),
			() => {}
		);
	}

	downloadFiche() {
		this.complianceMatrixService.exportDestinationCountry(
			this.selectedDestinationCountry.ebDestinationCountryNum
		);
	}

	newCombinaison() {
		this.ebCombinaison = new EbCombinaison();
		this.ebCombinaison.destinationCountry = this.selectedDestinationCountry;
		this.ebCombinaison.origin = null;
		this.ebCombinaison.importLicense = null;
	}

	cancelCombinaison() {
		this.ebCombinaison = null;
	}

	saveCombinaison() {
		if (!this.ebCombinaison.destinationCountry)
			this.ebCombinaison.destinationCountry = this.selectedDestinationCountry;

		if (
			Statique.isNotDefined(this.ebCombinaison.hsCode) ||
			Statique.isNotDefined(this.ebCombinaison.origin)
		)
			return;

		this.complianceMatrixService.addCombinaison(this.ebCombinaison).subscribe(
			function(data) {
				if (!data || !data.ebCombinaisonNum) {
					return;
				}

				if (this.ebCombinaison.ebCombinaisonNum != null) {
					this.listCombinaison.some((it, i) => {
						if (it.ebCombinaisonNum == this.ebCombinaison.ebCombinaisonNum) {
							this.listCombinaison[i] = this.ebCombinaison;
							return true;
						}
					});
				} else if (data && data.ebCombinaisonNum != null) {
					this.listCombinaison.push(new EbCombinaison(data));
				}

				this.ebCombinaison = null;
			}.bind(this)
		);
	}

	searchCombinaison() {
		if (!this.selectedDestinationCountry) return;

		let criterias: SearchCriteria = new SearchCriteria();
		criterias.pageNumber = null;
		criterias.size = null;
		if (this.selectedCountryMadeIn)
			criterias.ecCountryNum = this.selectedCountryMadeIn.ecCountryNum;
		criterias.hsCode = this.hsCodeField;
		criterias.ebDestinationCountryNum = this.selectedDestinationCountry.ebDestinationCountryNum;

		this.complianceMatrixService.getListCombinaison(criterias).subscribe((data) => {
			this.listCombinaison = data;
		});
	}

	ngAfterViewInit(): void {
		this.dataTableConfig.dtTrigger.next();
	}

	editCombinaison(ebCombinaison: EbCombinaison) {
		this.ebCombinaison = new EbCombinaison(Statique.copyObject(ebCombinaison, true));
	}

	deleteCombinaison(ebCombinaison: EbCombinaison, index) {
		this.modalService.confirm(
			"Confirm deleting",
			"Are you sure you want to delete this combination?",
			function() {
				this.complianceMatrixService.deleteCombinaison(ebCombinaison).subscribe((data) => {
					this.listCombinaison.splice(index, 1);
				});
			}.bind(this),
			() => {}
		);
	}

	async exportCsv() {
		this.exportService.startExportCsv(Modules.COMPLIANCE_MATRIX, false, null, this.userConnected);
	}

	loadCountries(ebcompagnieNum: number) {
		this.destCountriesCriteria.ebCompagnieNum = ebcompagnieNum;
		let searchCriteria = new SearchCriteria();
		searchCriteria.ebCompagnieNum = ebcompagnieNum;
		this.filteredListDestinationCountry = [];
		this.selectedDestinationCountry = null;
		this.ebDestinationCountry = null;
		this.ebCombinaison = null;
		this.complianceMatrixService
			.getAllDestinationCountryByCompagnie(this.destCountriesCriteria)
			.subscribe((data) => {
				this.filteredListDestinationCountry = data as Array<EbDestinationCountry>;
				this.listDestinationCountry = data as Array<EbDestinationCountry>;

				if (this.listPays) this.filteredListPays = this.filterListPays();
				this.selectedDestinationCountry = null;
				this.etablissement = null;
			});

		this.compagnieService.getListEtablisementCompagnie(searchCriteria).subscribe((data) => {
			this.listEtablissement = data as Array<EbEtablissement>;
		});
	}
}
