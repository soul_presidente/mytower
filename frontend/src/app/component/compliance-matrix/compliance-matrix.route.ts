import { Route, RouterModule } from "@angular/router";
import { ComplianceMatrixComponent } from "./compliance-matrix.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";

export const COMPLIANCE_MATRIX_ROUTES: Route[] = [
	{
		path: "",
		component: ComplianceMatrixComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const ComplianceMatrixRoutes = RouterModule.forChild(COMPLIANCE_MATRIX_ROUTES);
