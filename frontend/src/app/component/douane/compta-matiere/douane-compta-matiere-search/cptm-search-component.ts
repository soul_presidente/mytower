import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";

import { Modules, SavedFormIdentifier } from "@app/utils/enumeration";
import { SearchCriteriaCustom } from "@app/utils/SearchCriteriaCustom";
import { SearchField } from "@app/classes/searchField";
import { IField } from "@app/classes/customField";
import { Statique } from "@app/utils/statique";
import { AdvancedFormSearchComponent } from "@app/shared/advanced-form-search/advanced-form-search.component";

@Component({
	selector: "app-cptm-search",
	templateUrl: "./cptm-search-component.html",
	styleUrls: ["./cptm-search-component.scss"],
})
export class CptmSearchComponent implements OnInit {
	SavedFormIdentifier = SavedFormIdentifier;
	advancedOption: SearchCriteriaCustom = new SearchCriteriaCustom();

	@ViewChild("advancedFormSearchComponentCPTM", { static: true })
	advancedFormSearchComponentCPTM: AdvancedFormSearchComponent;

	Modules = Modules;
	@Input()
	initParamFields: Array<SearchField> = new Array<SearchField>();
	@Input()
	module: number;
	@Input()
	listFields: Array<IField>;
	@Output()
	onSearch = new EventEmitter<SearchCriteriaCustom>();
	paramFields: Array<SearchField> = new Array<SearchField>();
	dataInfosItem: Array<any> = [];
	@Input()
	selectedField: any;

	constructor(public generaleMethode: generaleMethodes) {}

	ngOnInit() {
		this.initParamFields = [];
		this.initParamFields = require("../../../../../assets/ressources/jsonfiles/compta-matiere-searchfields.json");
		this.onInputChange(null);
	}

	ngOnChanges() {}

	search(advancedOption: SearchCriteriaCustom) {
		this.onSearch.emit(advancedOption);
	}

	onClickInput() {
		this.initParamFields = require("../../../../../assets/ressources/jsonfiles/compta-matiere-searchfields.json");

		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));

		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});
	}

	onInputChange(selectedFields) {
		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));

		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});

		if (!Statique.isDefined(selectedFields)) {
			this.initParamFields = this.initParamFields.filter(
				(field) =>
					field.name === "refProcedure" ||
					field.name === "xLeg1DemandeRef" ||
					field.name === "xLeg2DemandeRef"
			);
			this.paramFields = Array.from(new Set([].concat(this.initParamFields)));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
		} else if (
			selectedFields !== null &&
			!this.paramFields.find((field) => field.name == selectedFields)
		) {
			let result = JSON.stringify(this.advancedFormSearchComponentCPTM.getSearchInput());
			let parsedValues: Array<SearchField> = JSON.parse(result);
			this.paramFields.forEach((field) => {
				if (parsedValues[field.name] !== null && parsedValues[field.name] !== undefined) {
					field.default = parsedValues[field.name];
				}
			});
			this.initParamFields = this.initParamFields.filter(
				(field) => field.name === selectedFields && field.default !== null
			);
			this.paramFields = this.paramFields.concat(this.initParamFields);
			this.paramFields = Array.from(new Set(this.paramFields));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
		}
	}
}
