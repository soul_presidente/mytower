import { SemiAutoMailPopup } from "@app/shared/semi-auto-mail/semi-auto-mail-popup.component";
import { SemiAutoMailType } from "@app/shared/semi-auto-mail/semi-auto-mail";
import { Component } from "@angular/core";
import { EbCptmProcedureDTO } from "@app/classes/cptm/EbCptmProcedureDTO";
import { ComptaMatiereService } from "@app/services/compta-matiere.service";
import { RequestProcessing } from "@app/utils/requestProcessing";

@Component({
	selector: "app-cptm-semi-automatique-mail",
	templateUrl: "../../../../shared/semi-auto-mail/semi-auto-mail-popup.component.html",
	styleUrls: ["../../../../shared/semi-auto-mail/semi-auto-mail-popup.component.scss"],
})
export class CptmSemiAutoMailPopup extends SemiAutoMailPopup {
	selectedProcedure: EbCptmProcedureDTO;

	constructor(protected comptaMatiereService: ComptaMatiereService) {
		super();
	}

	getListTypeMail(): Array<SemiAutoMailType> {
		return [
			{
				mailId: 44,
				label: "CPTM.SEMI_AUTO_MAIL.PROLONGER_IMPORTATION_TEMPORAIRE",
			},
			{
				mailId: 46,
				label: "CPTM.SEMI_AUTO_MAIL.LIQUIDATION_OFFICE",
			},
			{
				mailId: 47,
				label: "CPTM.SEMI_AUTO_MAIL.RELANCE",
			},
			{
				mailId: 48,
				label: "CPTM.SEMI_AUTO_MAIL.DEMANDE_ETAT_AVANCEMENT",
			},
		];
	}

	generateHtmlFromTemplate(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.comptaMatiereService
			.generateSemiAutoMailTemplateForProcedure(
				this.selectedMailTypeId,
				this.selectedProcedure.ebCptmProcedureNum
			)
			.subscribe(
				(res) => {
					requestProcessing.afterGetResponse(event);
					this.mail.text = res.text;
					this.mail.subject = res.subject;
					this.mail.mailType = this.selectedMailTypeId;
				},
				(error) => {
					console.error(error);
					requestProcessing.afterGetResponse(event);
				}
			);
	}

	sendMail(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.comptaMatiereService
			.sendSemiAutoMail(this.mail, this.selectedProcedure.ebCptmProcedureNum)
			.subscribe(
				(result) => {
					requestProcessing.afterGetResponse(event);
					this.close();
				},
				(error) => {
					console.error(error);
					requestProcessing.afterGetResponse(event);
				}
			);
	}

	openForProcedure(procedure: EbCptmProcedureDTO, emailId: number) {
		this.selectedProcedure = procedure;
		this.open(emailId);
	}
}
