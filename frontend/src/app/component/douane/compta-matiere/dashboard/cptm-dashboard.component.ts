import {
	Component,
	OnInit,
	ViewChild,
	NgZone,
	Output,
	EventEmitter,
	Input,
	SimpleChanges,
} from "@angular/core";
import { Statique } from "@app/utils/statique";
import { Modules } from "@app/utils/enumeration";
import { GenericTableScreen } from "@app/utils/enumeration";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { AuthenticationService } from "@app/services/authentication.service";
import { TextInputGenericCell } from "@app/shared/generic-cell/commons/text-input.generic-cell";
import { EbCptmProcedureDTO } from "@app/classes/cptm/EbCptmProcedureDTO";
import { ComptaMatiereService } from "@app/services/compta-matiere.service";
import { GenericEnum } from "@app/classes/GenericEnum";
import { GenericEnumCPTM } from "@app/classes/cptm/EnumerationCPTM";
import { StatiqueService } from "@app/services/statique.service";
import { TranslateService } from "@ngx-translate/core";
import { DatePickerGenericCell } from "@app/shared/generic-cell/commons/date-picker.generic-cell";
import { SelectGenericCell } from "@app/shared/generic-cell/commons/select.generic-cell";
import { ToJsDateDataConverter } from "@app/shared/generic-table/dataconverters/to-js-date.data-converter";
import { CommentIconPopupGenericCell } from "@app/shared/generic-cell/commons/comment-icon-popup.generic-cell";
import { HeaderService } from "@app/services/header.service";
import { CptmSemiAutoMailPopup } from "../send-auto-mail-popup/cptm-semi-auto-mail-popup.component";
import { Router } from "@angular/router";
import { ButtonGenericCell } from "@app/shared/generic-cell/commons/button.generic-cell";
import { SearchCriteriaCustom } from "@app/utils/SearchCriteriaCustom";
import { SearchField } from "@app/classes/searchField";
import { SearchCriteriaCPTM } from "@app/utils/SearchCriteriaCPTM";

@Component({
	selector: "app-compta-matiere-dashboard",
	templateUrl: "./cptm-dashboard.component.html",
	styleUrls: ["./cptm-dashboard.component.scss"],
})
export class CptmDashboardComponent extends ConnectedUserComponent implements OnInit {
	Module = Modules;
	Statique = Statique;

	searchCriteria: SearchCriteriaCPTM = new SearchCriteriaCPTM();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;

	module = Modules.COMPTA_MATIERE;
	globalSearchCriteria: SearchCriteriaCPTM = new SearchCriteriaCPTM();
	listProceduresSelected: Map<number, EbCptmProcedureDTO> = new Map();
	listStatus: Array<GenericEnum> = [];
	listEltsToAdjustHeight: Array<string> = new Array<string>();
	@Input("searchInput")
	set searchInput(searchInput: SearchCriteriaCPTM) {
		if (searchInput != null) {
			let tmpSearchInput: SearchCriteriaCPTM = new SearchCriteriaCPTM();

			tmpSearchInput.module = this.module;
			tmpSearchInput.ebUserNum = this.userConnected.ebUserNum;
			tmpSearchInput.searchInput = JSON.stringify(searchInput || {});

			this.searchCriteria = tmpSearchInput;
		}
	}
	initParamFields: Array<SearchField> = new Array<SearchField>();
	@Output()
	onSearch = new EventEmitter<SearchCriteriaCustom>();

	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	@ViewChild("popupSemiAutoMail", { static: true })
	popupSemiAutoMail: CptmSemiAutoMailPopup;

	constructor(
		protected authenticationService: AuthenticationService,
		protected comptaMatiereService: ComptaMatiereService,
		protected statiqueService: StatiqueService,
		protected translateService: TranslateService,
		protected headerService: HeaderService,
		protected router: Router,
		protected zone: NgZone
	) {
		super(authenticationService);
	}

	ngOnInit() {
		this.initParamFields = require("../../../../../assets/ressources/jsonfiles/compta-matiere-searchfields.json");
		this.initDatatable();

		this.initStatiques(() => {
			this.initTableCols();
		});

		this.headerService.clearActionButtons();
		this.headerService.registerActionButtons([
			{
				icon: "fa fa-envelope",
				label: "SEMI_AUTO_MAIL.GENERATE_MESSAGE",
				enableCondition: () => {
					return this.listProceduresSelected && this.listProceduresSelected.size === 1;
				},
				action: () => {
					this.generateMail(this.listProceduresSelected.values().next().value);
				},
			},
		]);

		this.genericTable.verticalScrollCalc(this.genericTable.scrollHeight);
		this.listEltsToAdjustHeightInit();
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["module"] || (changes["searchInput"] && changes["searchInput"].firstChange))
			this.initDatatable();
	}

	initDatatable() {
		this.searchCriteria.module = this.module;
		this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 50;
		this.dataInfos.dataKey = "ebCptmProcedureNum";
		this.dataInfos.dataType = EbCptmProcedureDTO;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerComptaMatiere + "/list-procedure-table";
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.showCheckbox = true;
		this.dataInfos.showCheckboxWidth = "130px";
		this.dataInfos.showCheckboxGlobal = false;
		this.dataInfos.actionColumFixed = true;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.cardView = true;
		this.dataInfos.showAdvancedSearchBtn = true;
		this.dataInfos.actionColumFixed = false;
	}

	initTableCols() {
		this.dataInfos.cols = [
			{
				field: "refProcedure",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.REF_PROCEDURE",
				isCardCol: true,
			},

			{
				field: "xLeg1UnitRef",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG1_REF_UNIT",
				headerClass: "cell-h-leg1",
			},
			{
				field: "leg1UnitIndex",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG1_UNIT_INDEX",
				headerClass: "cell-h-leg1",
			},
			{
				field: "xRegimeTemporaireLeg1Name",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG1_TYPE_LEG",
				headerClass: "cell-h-leg1",
				render: (name: string, renderParams, model: EbCptmProcedureDTO) => {
					if (!model.xRegimeTemporaireLeg1Code) {
						return model.xRegimeTemporaireLeg1Name;
					} else {
						return `${model.xRegimeTemporaireLeg1Name} (${model.xRegimeTemporaireLeg1Code})`;
					}
				},
			},
			{
				field: "xLeg1DemandeRef",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG1_REF_TRANSPORT",
				headerClass: "cell-h-leg1",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: ButtonGenericCell,
				genericCellParams: (model: EbCptmProcedureDTO) => {
					return new ButtonGenericCell.Params({
						field: "xLeg1DemandeRef",
						class: "btn btn-primary btn-xs",
						routerLink: "/app/transport-management/details/" + model.xLeg1DemandeNum,
					});
				},
			},
			{
				field: "leg1ExpectedCustomerDeclarationDate",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG1_ECDA",
				headerClass: "cell-h-leg1",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: DatePickerGenericCell,
				genericCellParams: new DatePickerGenericCell.Params({
					field: "leg1ExpectedCustomerDeclarationDate",
					showOnHover: true,
					forceUTC: true,
					readonlyInput: true,
				}),
				genericCellHandler: this.inlineEditingHandler,
				postLoadDataConverter: new ToJsDateDataConverter().convert,
			},
			{
				field: "leg1ActualCustomerDeclarationDate",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG1_ACDA",
				headerClass: "cell-h-leg1",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: DatePickerGenericCell,
				genericCellParams: new DatePickerGenericCell.Params({
					field: "leg1ActualCustomerDeclarationDate",
					hideIfNull: true,
					readonlyInput: true,
					forceUTC: true,
				}),
				genericCellHandler: this.inlineEditingHandler,
				postLoadDataConverter: new ToJsDateDataConverter().convert,
			},
			{
				field: "leg1DeclarationNumber",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG1_DEC_NUM",
				headerClass: "cell-h-leg1",
			},
			{
				field: "leg1Location",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG1_LOCATION",
				headerClass: "cell-h-leg1",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "leg1Location",
					inputType: "text",
					required: false,
					showOnHover: true,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{
				field: "leg1TransitNumber",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG1_TRANSIT_NUM",
				headerClass: "cell-h-leg1",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "leg1TransitNumber",
					inputType: "text",
					required: false,
					showOnHover: true,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{
				field: "leg1TransitStatus",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG1_TRANSIT_STATUS",
				headerClass: "cell-h-leg1",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: SelectGenericCell,
				genericCellParams: new SelectGenericCell.Params({
					field: "leg1TransitStatus",
					values: [
						{
							label: this.translateService.instant("GENERAL.NO"),
							value: false,
						},
						{
							label: this.translateService.instant("GENERAL.YES"),
							value: true,
						},
					],
					itemValueField: "value",
					itemTextField: "label",
					showOnHover: true,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{
				field: "leg1TracingStatus",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG1_TRACING_STATUS",
				headerClass: "cell-h-leg1",
			},
			{
				field: "xLeg2UnitRef",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG2_REF_UNIT",
				headerClass: "cell-h-leg2",
			},
			{
				field: "xRegimeTemporaireLeg2Name",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG2_TYPE_LEG",
				headerClass: "cell-h-leg2",
				render: (name: string, renderParams, model: EbCptmProcedureDTO) => {
					if (!model.xRegimeTemporaireLeg2Code) {
						return model.xRegimeTemporaireLeg2Name;
					} else {
						return `${model.xRegimeTemporaireLeg2Name} (${model.xRegimeTemporaireLeg2Code})`;
					}
				},
			},
			{
				field: "xLeg2DemandeRef",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG2_REF_TRANSPORT",
				headerClass: "cell-h-leg2",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: ButtonGenericCell,
				genericCellParams: (model: EbCptmProcedureDTO) => {
					return new ButtonGenericCell.Params({
						field: "xLeg2DemandeRef",
						class: "btn btn-primary btn-xs",
						routerLink: "/app/transport-management/details/" + model.xLeg2DemandeNum,
					});
				},
			},
			{
				field: "leg2ExpectedCustomerDeclarationDate",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG2_ECDA",
				headerClass: "cell-h-leg2",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: DatePickerGenericCell,
				genericCellParams: new DatePickerGenericCell.Params({
					field: "leg2ExpectedCustomerDeclarationDate",
					showOnHover: true,
					forceUTC: true,
					readonlyInput: true,
				}),
				genericCellHandler: this.inlineEditingHandler,
				postLoadDataConverter: new ToJsDateDataConverter().convert,
			},
			{
				field: "leg2ActualCustomerDeclarationDate",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG2_ACDA",
				headerClass: "cell-h-leg2",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: DatePickerGenericCell,
				genericCellParams: new DatePickerGenericCell.Params({
					field: "leg2ActualCustomerDeclarationDate",
					hideIfNull: true,
					readonlyInput: true,
					forceUTC: true,
				}),
				genericCellHandler: this.inlineEditingHandler,
				postLoadDataConverter: new ToJsDateDataConverter().convert,
			},
			{
				field: "leg2DeclarationNumber",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG2_DEC_NUM",
				headerClass: "cell-h-leg2",
			},
			{
				field: "leg2Location",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG2_LOCATION",
				headerClass: "cell-h-leg2",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "leg2Location",
					inputType: "text",
					required: false,
					showOnHover: true,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{
				field: "leg2TransitNumber",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG2_TRANSIT_NUM",
				headerClass: "cell-h-leg2",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "leg2TransitNumber",
					inputType: "text",
					required: false,
					showOnHover: true,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{
				field: "leg2TransitStatus",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG2_TRANSIT_STATUS",
				headerClass: "cell-h-leg2",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: SelectGenericCell,
				genericCellParams: new SelectGenericCell.Params({
					field: "leg2TransitStatus",
					values: [
						{
							label: this.translateService.instant("GENERAL.NO"),
							value: false,
						},
						{
							label: this.translateService.instant("GENERAL.YES"),
							value: true,
						},
					],
					itemValueField: "value",
					itemTextField: "label",
					showOnHover: true,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{
				field: "leg2TracingStatus",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LEG2_TRACING_STATUS",
				headerClass: "cell-h-leg2",
			},
			{
				field: "comment",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.COMMENT",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: CommentIconPopupGenericCell,
				genericCellParams: new CommentIconPopupGenericCell.Params({
					field: "comment",
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{
				field: "scheduledAlert",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.ALERT_SCHEDULED",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				postLoadDataConverter: new ToJsDateDataConverter().convert,
				render: (item: Date, renderParams, row: EbCptmProcedureDTO) => {
					return `<span class="alert${row.alertNumber}-color">
						${item == null ? "" : item.toLocaleDateString()}
						</span>`;
				},
			},
			{
				field: "deadline",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.LIMIT_DATE",
				postLoadDataConverter: new ToJsDateDataConverter().convert,
				render: (item: Date) => {
					return item == null ? "" : item.toLocaleDateString();
				},
			},
			{
				field: "daysSinceImport",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.TIME_SINCE_IMPORT",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: (item: number, renderParams, row) => {
					if (item < 0) {
						return `<span style="color: red;">${item}</span>`;
					}
					return item;
				},
			},
			{
				field: "daysBeforeDeadline",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.TIME_BEFORE_LIMIT",
				sortable: false,
			},
			{
				field: "status",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.STATUS",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: SelectGenericCell,
				genericCellParams: new SelectGenericCell.Params({
					field: "status",
					values: this.listStatus,
					itemValueField: "code",
					itemTextField: "key",
					showOnHover: true,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{
				field: "nbAlerts",
				translateCode: "CPTM.DASHBOARD_SUIVI.FIELDS.NB_ALERTS",
			},
		];
		this.dataInfos.selectedCols = this.dataInfos.cols;
	}

	inlineEditingHandler(key: string, value: any, model: any, context: any) {
		// Auto save
		if (key === "input-event" || key === "date-change") {
			if (value === "blur" || value === "select") {
				context.autoSaveRow(model);
			}
		}
		if (key === "selection-change") {
			context.autoSaveRow(model);
		}
		if (key === "popup-action" && value === "confirm") {
			context.autoSaveRow(model);
		}
	}

	autoSaveRow(row: EbCptmProcedureDTO) {
		this.comptaMatiereService.dashboardProcedureInlineUpdate(row).subscribe((result) => {
			this.genericTable.replaceRowUsingDataKey(result);
		});
	}

	initStatiques(callback: () => any) {
		this.statiqueService.getListGenericEnum(GenericEnumCPTM.ProcedureStatus).subscribe((data) => {
			if (this.listStatus == null) {
				this.listStatus = [];
			}

			// Add translation key for each and add to the unique array reference
			data.forEach((status) => {
				status.key = this.translateService.instant("CPTM.PROCEDURE_STATUS." + status.key);
				this.listStatus.push(status);
			});

			if (callback) {
				callback();
			}
		});
	}

	onCheckRow() {
		let data = this.genericTable.getSelectedRows();
		let mapData = new Map<number, EbCptmProcedureDTO>();

		this.listProceduresSelected = new Map<number, EbCptmProcedureDTO>();
		data.forEach((it) => {
			mapData.set(it.ebCptmProcedureNum, it);
		});

		this.listProceduresSelected = mapData;
	}

	navigateTo(arg: string): void {
		this.zone.run(() => {
			this.router.navigateByUrl(`/${arg}`);
		});
	}
	listEltsToAdjustHeightInit() {
		let listEltsClass: string[] = [".actionbar-wrapper", ".nav-tabs"];
		this.listEltsToAdjustHeight = listEltsClass;
	}

	generateMail(procedure: EbCptmProcedureDTO, mailId = null) {
		this.popupSemiAutoMail.openForProcedure(procedure, mailId);
	}

	search(searchInput: SearchCriteriaCPTM) {
		this.searchInput = searchInput;
	}
}
