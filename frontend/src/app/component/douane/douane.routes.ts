import { Route, RouterModule } from "@angular/router";
import { DashboardComponent } from "./declaration/dashboard/dashboard.component";
import { CreationComponent } from "./declaration/creation/creation.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";
import { DetailsDeclarationComponent } from "./declaration/details-declaration/details-declaration.component";
import { DouaneComponent } from "./douane.component";

export const DOUANE_ROUTES: Route[] = [
	{
		path: "",
		component: DouaneComponent,
	},
	{
		path: "dashboard",
		component: DouaneComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "creation",
		component: CreationComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "details/:ebCustomDeclarationNum",
		component: DetailsDeclarationComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const DouaneRoutes = RouterModule.forChild(DOUANE_ROUTES);
