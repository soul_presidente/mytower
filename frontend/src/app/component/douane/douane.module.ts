import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DouaneRoutes } from "./douane.routes";
import { Ng2CompleterModule } from "ng2-completer";
import { NgSelectModule } from "@ng-select/ng-select";
import { SharedModule } from "@app/shared/module/shared.module";
import { DataTablesModule } from "angular-datatables/src/angular-datatables.module";
import { NgxUploaderModule } from "ngx-uploader";
import { DeclarationComponent } from "./declaration/declaration.component";
import { DouaneService } from "@app/services/douane.service";
import { DeclarationDouaneListComponent } from "./declaration/douane-declaration-list/douane-declaration-list.component";
import { DeclarationDouaneSearchComponent } from "./declaration/douane-declaration-search/douane-declaration-search.component";
import { DashboardComponent } from "./declaration/dashboard/dashboard.component";
import { CreationComponent } from "./declaration/creation/creation.component";
import { HeaderDeclarationComponent } from "./declaration/header-declaration/header-declaration.component";
import { RequestInformationComponent } from "./declaration/request-information/request-information.component";
import { UnitsInformationComponent } from "./declaration/units-information/units-information.component";
import { DocumentsDeclaration } from "./declaration/documents-declaration/documents-declaration.component";
import { AdditionalInformation } from "./declaration/additional-information/additional-information.component";
import { CancellationInformationComponent } from "./declaration/cancellation-information/cancellation-information.component";
import { DetailsDeclarationComponent } from "./declaration/details-declaration/details-declaration.component";
import { AdvancedFormCustomComponent } from "@app/shared/form-custom/form-custom.component";
import { TableModule } from "primeng/table";
import { PanelModule } from "primeng/panel";
import { ButtonModule } from "primeng/button";
import { DouaneComponent } from "./douane.component";
import { CptmDashboardComponent } from "./compta-matiere/dashboard/cptm-dashboard.component";
import { CptmSemiAutoMailPopup } from "./compta-matiere/send-auto-mail-popup/cptm-semi-auto-mail-popup.component";
import { DouaneDocumentGenericCell } from "./declaration/douane-declaration-list/cells/douane-documents-cell/douane-documents-cell.component";
import { CptmSearchComponent } from "./compta-matiere/douane-compta-matiere-search/cptm-search-component";
@NgModule({
	imports: [
		CommonModule,
		DouaneRoutes,
		DataTablesModule,
		NgSelectModule,
		SharedModule,
		Ng2CompleterModule,
		NgxUploaderModule,
		TableModule,
		PanelModule,
		ButtonModule,
	],
	declarations: [
		DouaneComponent,
		DeclarationComponent,
		DeclarationDouaneListComponent,
		DeclarationDouaneSearchComponent,
		DashboardComponent,
		CreationComponent,
		HeaderDeclarationComponent,
		RequestInformationComponent,
		UnitsInformationComponent,
		DocumentsDeclaration,
		AdditionalInformation,
		CancellationInformationComponent,
		DetailsDeclarationComponent,
		AdvancedFormCustomComponent,
		CptmDashboardComponent,
		CptmSemiAutoMailPopup,
		DouaneDocumentGenericCell,
		CptmSearchComponent,
	],
	entryComponents: [DouaneDocumentGenericCell],
	providers: [DouaneService],
	exports: [],
})
export class DouaneModule {}
