import { Component, OnInit } from "@angular/core";
import { Modules } from "@app/utils/enumeration";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { AuthenticationService } from "@app/services/authentication.service";

@Component({
	selector: "app-douane",
	templateUrl: "./douane.component.html",
	styleUrls: ["./douane.component.scss"],
})
export class DouaneComponent extends ConnectedUserComponent implements OnInit {
	Modules = Modules;

	constructor(protected authenticationService: AuthenticationService) {
		super(authenticationService);
	}

	ngOnInit() {}
}
