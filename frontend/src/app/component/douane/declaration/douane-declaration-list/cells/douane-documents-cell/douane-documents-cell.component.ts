import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";
import { FichierJoint } from "@app/classes/fichiersJoint";
import { DouaneService } from "@app/services/douane.service";

@Component({
	selector: "app-douane-document-generic-cell",
	templateUrl: "./douane-documents-cell.component.html",
	styleUrls: ["./douane-documents-cell.component.scss"],
})
export class DouaneDocumentGenericCell
	extends GenericCell<any, DouaneDocumentGenericCell.Params, any>
	implements OnInit {
	countField: string;

	popupVisible = false;
	isLoading = true;
	listFichierJoint: Array<FichierJoint> = [];

	@ViewChild("popupRoot", { static: true })
	popupRoot: ElementRef;

	private douaneService: DouaneService;

	constructor() {
		super();
	}

	ngOnInit() {
		this.douaneService = this.injector.get(DouaneService);
		this.countField =
			this.params.countField != null ? this.params.countField : "attachedFilesCount";
	}

	openPopup() {
		this.popupVisible = true;

		this.isLoading = true;

		// Append to body to display on fullscreen instead of inside table cell
		document.body.appendChild(this.popupRoot.nativeElement);
		this.loadDocuments();
	}

	closePopup() {
		this.popupVisible = false;

		// Append to body to display on fullscreen instead of inside table cell
		document.body.removeChild(this.popupRoot.nativeElement);
	}

	loadDocuments() {
		this.douaneService.listAttachedDocuments(this.model["ebCustomDeclarationNum"]).subscribe(
			(res) => {
				this.listFichierJoint = res;
				this.isLoading = false;
			},
			(error) => {
				console.log(error);
				this.closePopup();
			}
		);
	}
}

export namespace DouaneDocumentGenericCell {
	export class Params extends GenericCell.Params {
		countField: string;
	}
}
