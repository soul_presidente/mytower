import {
	AfterViewInit,
	Component,
	ElementRef,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	SimpleChanges,
	ViewChild,
	NgZone,
} from "@angular/core";
import { Statique } from "@app/utils/statique";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { StatiqueService } from "@app/services/statique.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { SearchCriteriaCustom } from "@app/utils/SearchCriteriaCustom";
import {
	ConfigViewCustom,
	GenericTableScreen,
	ModeTransport,
	Modules,
} from "@app/utils/enumeration";
import { ExportService } from "@app/services/export.service";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { EcCountry } from "@app/classes/country";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { ModalService } from "@app/shared/modal/modal.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";
import { Subscription } from "rxjs";
import { EbCompagnie } from "@app/classes/compagnie";
import { EbFormFieldCompagnie } from "@app/classes/ebFormFieldCompagnie";
import { DouaneService } from "@app/services/douane.service";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbTypeTransport } from "@app/classes/EbTypeTransport";
import { EbUser } from "@app/classes/user";
import { EbCustomDeclaration } from "@app/classes/declaration";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { CommentIconPopupGenericCell } from "@app/shared/generic-cell/commons/comment-icon-popup.generic-cell";
import { HeaderService } from "@app/services/header.service";
import { DouaneDocumentGenericCell } from "./cells/douane-documents-cell/douane-documents-cell.component";
import { EbDemande } from "@app/classes/demande";
import { ButtonGenericCell } from "@app/shared/generic-cell/commons/button.generic-cell";
import { HeaderInfos } from "@app/shared/header/header-infos";

declare global {
	interface Window {
		functions: any;
	}
}

@Component({
	selector: "app-douane-declaration-list",
	templateUrl: "./douane-declaration-list.component.html",
	styleUrls: ["./douane-declaration-list.component.css"],
})
export class DeclarationDouaneListComponent extends ConnectedUserComponent
	implements OnInit, OnDestroy, OnChanges, AfterViewInit {
	statique = Statique;
	Modules = Modules;
	GenericTableScreen = GenericTableScreen;

	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	listDestinations: any;
	subscriptions: Array<Subscription> = new Array<Subscription>();
	listCountry: Array<EcCountry> = new Array<EcCountry>();

	listDeclarationsSelected: Map<number, EbCustomDeclaration> = new Map();
	listEltsToAdjustHeight: Array<string> = new Array<string>();

	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	set bindGenericTable(_genericTable: GenericTableComponent) {
		// Generic Table component loaded after cols loaded (non static)
		this.genericTable = _genericTable;
		if (this.genericTable != null) {
			this.genericTable.verticalScrollCalc(this.genericTable.scrollHeight);
		}
	}

	@Input()
	module: number;

	@Input()
	searchInput: SearchCriteriaCustom;

	ebFormFieldCompagnie: EbFormFieldCompagnie = new EbFormFieldCompagnie();
	initiateState: boolean = false;
	subscriptionList = [];

	private listTypeTransportMap: Map<number, Array<EbTypeTransport>>;

	constructor(
		private statiqueService: StatiqueService,
		private elRef: ElementRef,
		public generaleMethode: generaleMethodes,
		protected router: Router,
		private douaneService: DouaneService,
		protected headerService: HeaderService,
		protected exportService?: ExportService,
		private translate?: TranslateService,
		private modalService?: ModalService,
		protected zone?: NgZone
	) {
		super();
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				icon: "my-icon-custom ",
				label: "MODULES.CUSTOMS",
			},
		];
	}
	ngAfterViewInit() {}

	ngOnInit() {
		this.searchInput.module = this.module;
		this.searchInput.ebUserNum = this.userConnected.ebUserNum;
		this.searchInput.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchInput.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;

		this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());
		this.initDatatable();

		this.getStatics(null);

		this.headerService.clearActionButtons();

		this.genericTable.verticalScrollCalc(this.genericTable.scrollHeight);
		this.listEltsToAdjustHeightInit();
	}

	ngOnChanges(changes: SimpleChanges) {
		/* this.ebFormFieldCompagnie.xEbCompagnie = new EbCompagnie();
		this.ebFormFieldCompagnie.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchInput.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		if (changes["module"] || (changes["searchInput"] && changes["searchInput"].firstChange)) {
			this.douaneService.getListChampFormForCompagnie(this.searchInput).subscribe((res) => {
				this.ebFormFieldCompagnie = res;
				this.initTableCols();
				this.initDatatable();
			});
		} */
	}

	ngOnDestroy() {
		this.subscriptions.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
	}

	clickHandler(data) {
		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(this.module)];
		this.router.navigate(["/app/" + moduleRoute + "/details", data.ebCustomDeclarationNum]);
	}

	async exportCsv() {
		this.exportService.startExportCsv(Modules.CUSTOM, false, null, this.userConnected);
	}

	async getStatics(callback?: () => void) {
		this.listTypeTransportMap = await SingletonStatique.getListTypeTransportMap();
		if (callback) callback();
	}

	async initDatatable() {
		this.searchInput.module = this.module;
		this.searchInput.ebUserNum = this.userConnected.ebUserNum;

		this.dataInfos.dataKey = "ebCustomDeclarationNum";
		this.dataInfos.dataType = EbCustomDeclaration;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerDouane + "/listDeclaration";
		this.dataInfos.numberDatasPerPage = 50;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.showCheckbox = false;
		this.dataInfos.showCheckboxGlobal = false;
		this.dataInfos.showExportBtn = true;
		this.ebFormFieldCompagnie = await this.getListChampFormForCompagnie();
		this.initTableCols();
	}

	initTableCols() {
		let $this = this;
		this.dataInfos.cols = [];

		this.ebFormFieldCompagnie.listFormField &&
			this.ebFormFieldCompagnie.listFormField.forEach((it) => {
				if (
					it.motTechnique == "refDemandeDedouanement" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.REFDEMANDEDEDOUANEMENT",
						field: "refDemandeDedouanement",
					});
				}
				if (
					it.motTechnique == "idDedouanement" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.IDDEDOUANEMENT",
						field: "idDedouanement",
					});
				}
				if (
					it.motTechnique == "demandeur" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DEMANDEUR",
						field: "demandeur",
						render: (data) => {
							if (data == null) return "";
							return data.nomPrenom;
						},
					});
				}
				if (
					it.motTechnique == "refTransport" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.TRANSPORT_REFERENCE",
						field: "xEbDemande",
						renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
						genericCellClass: ButtonGenericCell,
						genericCellParams: new ButtonGenericCell.Params({
							textRender: (model: EbCustomDeclaration) => {
								return model.xEbDemande && model.xEbDemande.refTransport
									? model.xEbDemande.refTransport
									: "";
							},
							class: "btn btn-primary btn-xs",
						}),
						genericCellHandler: (key, value, model: EbCustomDeclaration, context) => {
							context.navigateTo(
								"/app/transport-management/details/" + model.xEbDemande.ebDemandeNum
							);
						},
					});
				}
				if (
					it.motTechnique == "operation" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.OPERATION",
						field: "operation",
					});
				}
				if (it.motTechnique == "projet" && it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.PROJECT",
						field: "projet",
					});
				}
				if (
					it.motTechnique == "codeBAU" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.CODEBAU",
						field: "codeBAU",
					});
				}
				if (it.motTechnique == "eori" && it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.EORI",
						field: "eori",
					});
				}
				if (
					it.motTechnique == "typeDeFlux" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.TYPEDEFLUX",
						field: "typeDeFlux",
					});
				}
				if (
					it.motTechnique == "sitePlant" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.SITEPLANT",
						field: "sitePlant",
					});
				}
				if (
					it.motTechnique == "destinataire" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DESTINATAIRE",
						field: "destinataire",
					});
				}
				if (
					it.motTechnique == "xEbIncoterm" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.XEINCOTERM",
						field: "xEbIncoterm",
						render: (data) => {
							if (data == null) return "";
							return data.libelle;
						},
					});
				}
				if (
					it.motTechnique == "villeIncoterm" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.VILLEINCOTERM",
						field: "villeIncoterm",
					});
				}
				if (
					it.motTechnique == "charger" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.EXPEDITEUR",
						field: "charger",
						render: (data) => {
							if (data == null) return "";
							return data.nomPrenom;
						},
					});
				}
				if (
					it.motTechnique == "nomFournisseur" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.NOMFOURNISSEUR",
						field: "nomFournisseur",
					});
				}
				if (
					it.motTechnique == "xEcModeTransport" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.MODETRANSPORT",
						field: "xEcModeTransport",
						render: (modeTransportCode, type, row) => {
							if (!modeTransportCode) return "";
							return SingletonStatique.getModeTransportString(
								modeTransportCode,
								row,
								$this.listTypeTransportMap
							);
						},
					});
				}
				if (
					it.motTechnique == "immatriculation" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.IMMATRICULATION",
						field: "immatriculation",
					});
				}
				if (
					it.motTechnique == "dateArrivalEsstimed" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DATEARRIVALESSTIMED",
						field: "dateArrivalEsstimed",
						render: (data) => {
							return $this.statique.formatDate(data);
						},
					});
				}
				if (
					it.motTechnique == "airportPortOfArrival" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.AIRPORTPORTOFARRIVAL",
						field: "airportPortOfArrival",
					});
				}
				if (
					it.motTechnique == "xEcCountryProvenances" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.COUNTRYPROVENANCES",
						field: "xEcCountryProvenances",
						render: (data) => {
							if (data == null) return "";
							return data.libelle;
						},
					});
				}
				if (
					it.motTechnique == "agentBroker" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.AGENTBROKER",
						field: "agentBroker",
						render: (data) => {
							if (data == null) return "";
							return data.nomPrenom;
						},
					});
				}
				if (
					it.motTechnique == "xEcCountryDedouanement" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.OUNTRYDEDOUANEMENT",
						field: "xEcCountryDedouanement",
						render: (data) => {
							if (data == null) return "";
							return data.libelle;
						},
					});
				}
				if (
					it.motTechnique == "lieuDedouanement" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.LIEUDEDOUANEMENT",
						field: "lieuDedouanement",
					});
				}
				if (
					it.motTechnique == "biensDoubleUsage" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.BIENSDOUBLEUSAGE",
						field: "biensDoubleUsage",
					});
				}
				if (
					it.motTechnique == "licenceDoubleUsage" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.LICENCEDOUBLEUSAGE",
						field: "licenceDoubleUsage",
					});
				}
				if (
					it.motTechnique == "transit" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.TRANSIT",
						field: "transit",
					});
				}
				if (
					it.motTechnique == "bureauDeTransit" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.BUREAUDETRANSIT",
						field: "bureauDeTransit",
					});
				}
				if (
					it.motTechnique == "warGoods" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.WARGOODS",
						field: "warGoods",
					});
				}
				if (
					it.motTechnique == "warGoodLicenceReference" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.WARGOODLICENCEREFERENCE",
						field: "warGoodLicenceReference",
					});
				}
				if (
					it.motTechnique == "customOffice" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.CUSTOMOFFICE",
						field: "customOffice",
					});
				}
				if (
					it.motTechnique == "carrier" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.NOMTRANSPORTEUR",
						field: "carrier",
						render: (data) => {
							if (data == null) return "";
							return data.nomPrenom;
						},
					});
				}
				if (
					it.motTechnique == "carrier" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.TELEPHONETRANSPORTEUR",
						field: "carrier",
						render: (data) => {
							if (data == null) return "";
							return data.telephone;
						},
					});
				}
				if (
					it.motTechnique == "carrier" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.EMAILTRANSPORTEUR",
						field: "carrier",
						render: (data) => {
							if (data == null) return "";
							return data.email;
						},
					});
				}
				if (
					it.motTechnique == "freightPart" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.FREIGHTPART",
						field: "freightPart",
					});
				}
				if (
					it.motTechnique == "freightPartCurrency" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.FREIGHTPARTCURRENCY",
						field: "freightPartCurrency",
						render: (data) => {
							if (data == null) return "";
							return data.code;
						},
					});
				}
				if (
					it.motTechnique == "insuranceCost" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.INSURANCECOST",
						field: "insuranceCost",
					});
				}
				if (
					it.motTechnique == "insuranceCostCurrency" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.INSURANCECOSTCURRENCY",
						field: "insuranceCostCurrency",
						render: (data) => {
							if (data == null) return "";
							return data.code;
						},
					});
				}
				if (
					it.motTechnique == "invoiceAmount" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.INVOICEAMOUNT",
						field: "invoiceAmount",
					});
				}
				if (
					it.motTechnique == "invoiceCurrency" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.INVOICECURRENCY",
						field: "invoiceCurrency",
						render: (data) => {
							if (data == null) return "";
							return data.code;
						},
					});
				}
				if (
					it.motTechnique == "toolingCost" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.TOOLINGCOST",
						field: "toolingCost",
					});
				}
				if (
					it.motTechnique == "toolingCostCurrency" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.TOOLINGCOSTCURRENCY",
						field: "toolingCostCurrency",
						render: (data) => {
							if (data == null) return "";
							return data.code;
						},
					});
				}
				if (
					it.motTechnique == "totalFreightCost" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.TOTALFREIGHTCOST",
						field: "totalFreightCost",
					});
				}
				if (
					it.motTechnique == "totalFreightCostCurrency" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.TOTALFREIGHTCOSTCURRENCY",
						field: "totalFreightCostCurrency",
						render: (data) => {
							if (data == null) return "";
							return data.code;
						},
					});
				}
				if (it.motTechnique == "numTva" && it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.NUMTVA",
						field: "numTva",
					});
				}
				if (
					it.motTechnique == "traficAtBorder" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.TRAFICATBORDER",
						field: "traficAtBorder",
					});
				}
				if (
					it.motTechnique == "importerOfRecord" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.IMPORTEROFRECORD",
						field: "importerOfRecord",
					});
				}
				if (it.motTechnique == "delay" && it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DELAY",
						field: "delay",
					});
				}
				if (
					it.motTechnique == "documents" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DOCUMENTS",
						field: "documents",
						renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
						genericCellClass: DouaneDocumentGenericCell,
					});
				}
				if (
					it.motTechnique == "comment" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.COMMENT",
						field: "comment",

						renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
						genericCellClass: CommentIconPopupGenericCell,
						genericCellParams: new CommentIconPopupGenericCell.Params({
							field: "comment",
						}),
						genericCellHandler: this.inlineEditingHandler,
					});
				}
				if (
					it.motTechnique == "grossWeight" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.GROSSWEIGHT",
						field: "grossWeight",
					});
				}
				if (
					it.motTechnique == "grossWeightUnit" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.GROSSWEIGHTUNIT",
						field: "grossWeightUnit",
					});
				}
				if (
					it.motTechnique == "numberOfPack" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.NUMBEROFPACK",
						field: "numberOfPack",
					});
				}
				if (
					it.motTechnique == "numberOfLines" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.NUMBEROFLINES",
						field: "numberOfLines",
					});
				}
				if (
					it.motTechnique == "totalQuantity" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.TOTALQUANTITY",
						field: "totalQuantity",
					});
				}
				if (
					it.motTechnique == "valueAmountTotal" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.VALUEAMOUNTTOTAL",
						field: "valueAmountTotal",
					});
				}
				if (
					it.motTechnique == "business" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.BUSINESS",
						field: "business",
					});
				}
				if (
					it.motTechnique == "liquidation" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.LIQUIDATION",
						field: "liquidation",
					});
				}
				if (it.motTechnique == "euCode" && it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.EUCODE",
						field: "euCode",
					});
				}
				if (it.motTechnique == "hsCode" && it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.HSCODE",
						field: "hsCode",
					});
				}
				if (
					it.motTechnique == "partNumber" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.PARTNUMBER",
						field: "partNumber",
					});
				}
				if (
					it.motTechnique == "procedure" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.PROCEDURE",
						field: "procedure",
					});
				}
				if (
					it.motTechnique == "quantity" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.QUANTITY",
						field: "quantity",
					});
				}
				if (
					it.motTechnique == "valueAmount" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.VALUEAMOUNT",
						field: "valueAmount",
					});
				}
				if (
					it.motTechnique == "valueCurrency" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.VALUECURRENCY",
						field: "valueCurrency",
						render: (data) => {
							if (data == null) return "";
							return data.code;
						},
					});
				}
				if (
					it.motTechnique == "exchangeRate" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.EXCHANGERATE",
						field: "exchangeRate",
					});
				}
				if (
					it.motTechnique == "dateUpload" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DATEUPLOAD",
						field: "dateUpload",
						render: (data) => {
							return $this.statique.formatDate(data);
						},
					});
				}
				if (
					it.motTechnique == "typeDoc" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.TYPEDOC",
						field: "typeDoc",
					});
				}
				if (
					it.motTechnique == "numberOrReference1" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.NUMBERORREFERENCE1",
						field: "numberOrReference1",
					});
				}
				if (
					it.motTechnique == "numberOrReference2" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.NUMBERORREFERENCE2",
						field: "numberOrReference2",
					});
				}
				if (
					it.motTechnique == "attroneyText" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.ATTRONEYTEXT",
						field: "attroneyText",
					});
				}
				if (
					it.motTechnique == "cancellationReason" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.CANCELLATIONREASON",
						field: "cancellationReason",
					});
				}
				if (
					it.motTechnique == "dateCancellation" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DATECANCELLATION",
						field: "dateCancellation",
						render: (data) => {
							return $this.statique.formatDate(data);
						},
					});
				}

				if (it.motTechnique == "statut" && it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.STATUS",
						field: "statut",
						render: (data) => {
							if (data == null) return "";
							return $this.translate.instant(data === 1 ? "CUSTOM.APURE" : "CUSTOM.IN_PROGRESS");
						},
					});
				}
				if (it.motTechnique == "leg" && it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.LEG",
						field: "leg",
					});
				}

				if (
					it.motTechnique == "dateEcs" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DATE_ECS",
						field: "dateEcs",
						render: (data) => {
							return $this.statique.formatDate(data);
						},
					});
				}
				if (
					it.motTechnique == "dateDeclaration" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DATE_DECLARATION",
						field: "dateDeclaration",
						render: (data) => {
							return $this.statique.formatDate(data);
						},
					});
				}
				if (
					it.motTechnique == "nbOfTemporaryRegime" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.NB_OF_TEMPORARY_REGIME_LINKED",
						field: "nbOfTemporaryRegime",
					});
				}
				if (
					it.motTechnique == "declarationNumber" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DECLARATION_NUMBER",
						field: "declarationNumber",
					});
				}
				if (
					it.motTechnique == "customRegime" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.CUSTOMREGIME",
						field: "customRegime",
					});
				}
				if (
					it.motTechnique == "declarationReference" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DECLARATION_REFERENCE",
						field: "declarationReference",
					});
				}
				if (
					it.motTechnique == "xEbUserDeclarant" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DECLARANT",
						field: "xEbUserDeclarant",
						render: (data: EbUser) => {
							if (data == null) return "";
							return data.nom + " " + data.prenom;
						},
					});
				}
				if (
					it.motTechnique == "demandeDate" &&
					it.xEbViewForm.ebFormViewNum != ConfigViewCustom.TABLE
				) {
					$this.dataInfos.cols.push({
						translateCode: "CUSTOM.DEMANDEDATE",
						field: "demandeDate",
						render: (data: EbUser) => {
							if (data == null) return "";
							return $this.statique.formatDate(data);
						},
					});
				}
			});
		$this.dataInfos.cols.push({
			translateCode: "CUSTOM.DATE_CREATION",
			field: "dateCreationSys",
			render: (date: Date) => {
				return $this.statique.formatDate(date);
			},
		});

		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.genericTable.refreshData();
	}
	inlineEditingHandler(key: string, value: any, model: any, context: any) {
		// Auto save
		if (key === "input-event" || key === "date-change") {
			if (value === "blur" || value === "select") {
				context.autoSaveRow(model);
			}
		}
		if (key === "selection-change") {
			context.autoSaveRow(model);
		}
		if (key === "popup-action" && value === "confirm") {
			context.autoSaveRow(model);
		}
	}

	autoSaveRow(row: EbCustomDeclaration) {
		this.douaneService.dashboardProcedureInlineUpdate(row).subscribe((result) => {
			// Nothing to do here
		});
	}

	onCheckRow() {
		let data = this.genericTable.getSelectedRows();
		let mapData = new Map<number, EbCustomDeclaration>();

		this.listDeclarationsSelected = new Map<number, EbCustomDeclaration>();
		data.forEach((it) => {
			mapData.set(it.ebCptmProcedureNum, it);
		});

		this.listDeclarationsSelected = mapData;
	}

	listEltsToAdjustHeightInit() {
		let listEltsClass: string[] = [".actionbar-wrapper", ".nav-tabs"];
		this.listEltsToAdjustHeight = listEltsClass;
	}

	navigateTo(arg: string): void {
		this.zone.run(() => {
			this.router.navigateByUrl(`/${arg}`);
		});
	}

	getListChampFormForCompagnie(): Promise<any> {
		return new Promise((resolve) => {
			this.douaneService.getListChampFormForCompagnie(this.searchInput).subscribe((res) => {
				resolve(res);
			});
		});
	}
}
