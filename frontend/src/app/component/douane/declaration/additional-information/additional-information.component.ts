import { SearchCriteria } from "@app/utils/searchCriteria";

import { Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";

import { StatiqueService } from "@app/services/statique.service";
import { EbUser } from "@app/classes/user";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbCustomDeclaration } from "@app/classes/declaration";
import { EbCompagnie } from "@app/classes/compagnie";
import { DouaneService } from "@app/services/douane.service";
import { EbFormFieldCompagnie } from "@app/classes/ebFormFieldCompagnie";
import { ebFormField } from "@app/classes/ebFormField";
import { ConfigViewCustom } from "@app/utils/enumeration";
import { CompleterService } from "../../../../../../node_modules/ng2-completer";

@Component({
	selector: "app-additional-information",
	templateUrl: "./additional-information.component.html",
	styleUrls: ["./additional-information.component.css"],
})
export class AdditionalInformation extends ConnectedUserComponent implements OnInit {
	@Input()
	module: number;
	@Input()
	userConnected: EbUser;
	@Input()
	declaration: EbCustomDeclaration = new EbCustomDeclaration();
	ebFormFieldCompagnie: EbFormFieldCompagnie = new EbFormFieldCompagnie();
	criteria: SearchCriteria = new SearchCriteria();
	listEbFormFieldCustom: Array<ebFormField> = new Array<ebFormField>();
	listAdditionalInformation: Array<ebFormField> = new Array<ebFormField>();
	@ViewChild("pageValid", { static: false })
	pageValid: ElementRef;

	constructor(
		private completerService: CompleterService,
		private douaneService: DouaneService,
		protected statiqueService: StatiqueService
	) {
		super();
	}

	ngOnInit() {
		this.ebFormFieldCompagnie.xEbCompagnie = new EbCompagnie();
		this.ebFormFieldCompagnie.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.douaneService.getListChampFormForCompagnie(this.criteria).subscribe((res) => {
			this.ebFormFieldCompagnie = res;
			if (this.ebFormFieldCompagnie != null) {
				if (this.ebFormFieldCompagnie.listFormField != null) {
					this.listEbFormFieldCustom = this.ebFormFieldCompagnie.listFormField;
				}
			}
			this.listEbFormFieldCustom.forEach((it) => {
				if (it.xEbViewForm.ebFormViewNum == ConfigViewCustom.ADDITIONAL_INFORMATION)
					this.listAdditionalInformation.push(it);
			});
		});
	}
}
