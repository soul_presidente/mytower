import { Component, OnInit } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { Modules } from "@app/utils/enumeration";
import { SearchCriteriaCustom } from "@app/utils/SearchCriteriaCustom";

@Component({
	selector: "app-douane-dashboard",
	templateUrl: "./dashboard.component.html",
	styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent extends ConnectedUserComponent implements OnInit {
	Modules = Modules;
	searchInput: SearchCriteriaCustom;

	public module: number;

	constructor() {
		super();		
	}

	ngOnInit() {
		this.module = Modules.CUSTOM;
		this.searchInput = new SearchCriteriaCustom();
		this.searchInput.pageNumber = 1;
		this.searchInput.size = 50;
	}

	search(searchInput: SearchCriteriaCustom) {
		this.searchInput = searchInput;
	}
}
