import { Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";

import { StatiqueService } from "@app/services/statique.service";
import { EbUser } from "@app/classes/user";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbCustomDeclaration } from "@app/classes/declaration";
import { CompleterService } from "../../../../../../node_modules/ng2-completer";

@Component({
	selector: "app-cancellation-information",
	templateUrl: "./cancellation-information.component.html",
	styleUrls: ["./cancellation-information.component.css"],
})
export class CancellationInformationComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	module: number;
	@Input()
	userConnected: EbUser;
	@Input()
	declaration: EbCustomDeclaration = new EbCustomDeclaration();
	@ViewChild("pageValid", { static: false })
	pageValid: ElementRef;

	constructor(
		private completerService: CompleterService,
		protected statiqueService: StatiqueService
	) {
		super();
	}

	ngOnInit() {}
}
