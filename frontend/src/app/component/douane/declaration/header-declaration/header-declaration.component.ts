import { SearchCriteria } from "@app/utils/searchCriteria";

import { Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";

import { StatiqueService } from "@app/services/statique.service";
import { EbUser } from "@app/classes/user";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbCustomDeclaration } from "@app/classes/declaration";
import { ebFormField } from "@app/classes/ebFormField";
import { DouaneService } from "@app/services/douane.service";
import { EbFormFieldCompagnie } from "@app/classes/ebFormFieldCompagnie";
import { ConfigZoneCustom } from "@app/utils/enumeration";
import { EbCompagnie } from "@app/classes/compagnie";
import { CompleterService } from "../../../../../../node_modules/ng2-completer";

@Component({
	selector: "app-header-declaration",
	templateUrl: "./header-declaration.component.html",
	styleUrls: ["./header-declaration.component.css"],
})
export class HeaderDeclarationComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	module: number;
	@Input()
	userConnected: EbUser;
	@Input()
	declaration: EbCustomDeclaration;
	@ViewChild("pageValid", { static: false })
	pageValid: ElementRef;
	listEbFormHeader: Array<ebFormField> = new Array<ebFormField>();
	ebFormFieldCompagnie: EbFormFieldCompagnie = new EbFormFieldCompagnie();
	criteria: SearchCriteria = new SearchCriteria();
	listEbFormFieldCustom: Array<ebFormField> = new Array<ebFormField>();

	constructor(
		private douaneService: DouaneService,
		private completerService: CompleterService,
		protected statiqueService: StatiqueService
	) {
		super();
	}

	ngOnInit() {
		this.ebFormFieldCompagnie.xEbCompagnie = new EbCompagnie();
		this.ebFormFieldCompagnie.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.douaneService.getListChampFormForCompagnie(this.criteria).subscribe((res) => {
			this.ebFormFieldCompagnie = res;
			if (this.ebFormFieldCompagnie != null) {
				if (this.ebFormFieldCompagnie.listFormField != null) {
					this.listEbFormFieldCustom = this.ebFormFieldCompagnie.listFormField;
				}
			}
			this.listEbFormFieldCustom.forEach((it) => {
				if (it.xEbViewForm.xEbFormZone.ebFormZoneNum == ConfigZoneCustom.HEADER)
					this.listEbFormHeader.push(it);
			});
		});
	}
}
