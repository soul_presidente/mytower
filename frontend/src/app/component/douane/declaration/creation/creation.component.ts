import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { EbCustomDeclaration } from "@app/classes/declaration";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { ActivatedRoute, Router } from "@angular/router";
import { EcCountry } from "@app/classes/country";
import { DouaneService } from "@app/services/douane.service";
import { Statique } from "@app/utils/statique";
import { EcCurrency } from "@app/classes/currency";
import { StatiqueService } from "@app/services/statique.service";
import { ChatService } from "@app/services/chat.service";
import { HeaderDeclarationComponent } from "../header-declaration/header-declaration.component";
import { AccessRights, IDChatComponent, Modules } from "@app/utils/enumeration";
import { RequestInformationComponent } from "../request-information/request-information.component";
import { UnitsInformationComponent } from "../units-information/units-information.component";
import { DocumentsDeclaration } from "../documents-declaration/documents-declaration.component";
import { AdditionalInformation } from "../additional-information/additional-information.component";
import { CancellationInformationComponent } from "../cancellation-information/cancellation-information.component";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { EbChatCustom } from "@app/classes/chatCustom";
import { EbFormFieldCompagnie } from "@app/classes/ebFormFieldCompagnie";
import { ebFormField } from "@app/classes/ebFormField";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbCompagnie } from "@app/classes/compagnie";
import SingletonStatique from "@app/utils/SingletonStatique";
import { ChatPanelComponent } from "@app/shared/chat-panel/chat-panel.component";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";

@Component({
	selector: "app-creation",
	templateUrl: "./creation.component.html",
	styleUrls: ["./creation.component.css"],
})
export class CreationComponent extends ConnectedUserComponent implements OnInit {
	declaration: EbCustomDeclaration = new EbCustomDeclaration();
	listCountry: Array<EcCountry> = new Array<EcCountry>();
	listCurrency: Array<EcCurrency> = null;
	hasContributionAccess: boolean = false;
	Modules = Modules;
	module: number;
	IDChatComponent = IDChatComponent;
	@Output()
	updateChatEmitter = new EventEmitter<EbChatCustom>();
	Statique = Statique;
	ebFormFieldCompagnie: EbFormFieldCompagnie = new EbFormFieldCompagnie();
	listEbFormFieldCustom: Array<ebFormField> = new Array<ebFormField>();
	criteria: SearchCriteria = new SearchCriteria();

	@ViewChild(HeaderDeclarationComponent, { static: false })
	headerDeclarationComponent: HeaderDeclarationComponent;
	@ViewChild(RequestInformationComponent, { static: false })
	requestInformationComponent: RequestInformationComponent;
	@ViewChild(UnitsInformationComponent, { static: false })
	unitsInformationComponent: UnitsInformationComponent;
	@ViewChild(DocumentsDeclaration, { static: false })
	documentsDeclaration: DocumentsDeclaration;
	@ViewChild(AdditionalInformation, { static: false })
	additionalInformation: AdditionalInformation;
	@ViewChild(CancellationInformationComponent, { static: false })
	cancellationInformationComponent: CancellationInformationComponent;
	@ViewChild(ChatPanelComponent, { static: false })
	chatCustomPanelComponent: ChatPanelComponent;

	constructor(
		private douaneService: DouaneService,
		protected headerService: HeaderService,
		private router: Router,
		private route: ActivatedRoute,
		protected statiqueService: StatiqueService,
		protected modalService: ModalService,
		protected translate: TranslateService,
		protected chatService?: ChatService
	) {
		super();
	}
	ngOnInit() {
		this.headerService.registerActionButtons(this.getActionButtons());
		this.declaration = new EbCustomDeclaration();

		// contribution access rights to module
		if (this.userConnected.listAccessRights && this.userConnected.listAccessRights.length > 0) {
			this.hasContributionAccess =
				this.userConnected.listAccessRights.find((access) => {
					return (
						access.ecModuleNum == Modules.CUSTOM && access.accessRight == AccessRights.CONTRIBUTION
					);
				}) !== undefined;
		} else {
			this.hasContributionAccess = false;
		}

		(async () => (this.listCountry = await SingletonStatique.getListEcCountry()))();
		(async () => (this.listCurrency = await SingletonStatique.getListEcCurrency()))();

		this.ebFormFieldCompagnie.xEbCompagnie = new EbCompagnie();
		this.ebFormFieldCompagnie.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.douaneService.getListChampFormForCompagnie(this.criteria).subscribe((res) => {
			this.ebFormFieldCompagnie = res;
			if (this.ebFormFieldCompagnie != null) {
				if (this.ebFormFieldCompagnie.listFormField != null) {
					this.listEbFormFieldCustom = this.ebFormFieldCompagnie.listFormField;
				}
			}
		});
	}

	getActionButtons(): Array<HeaderInfos.ActionButton> {
		return [
			{
				label: "New Declaration",
				icon: "fa fa-plus-circle",
				action: () => {
					this.router.navigate(["/app/custom/creation"]);
				},
				displayCondition: () => {
					return this.userConnected.hasContribution(Modules.CUSTOM);
				},
			},
		];
	}

	addDeclaration(event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		if (this.declaration.insuranceCostCurrency.ecCurrencyNum == null)
			this.declaration.insuranceCostCurrency = null;
		if (this.declaration.invoiceCurrency.ecCurrencyNum == null)
			this.declaration.invoiceCurrency = null;
		if (this.declaration.toolingCostCurrency.ecCurrencyNum == null)
			this.declaration.toolingCostCurrency = null;
		if (this.declaration.totalFreightCostCurrency.ecCurrencyNum == null)
			this.declaration.totalFreightCostCurrency = null;
		if (this.declaration.valueCurrency.ecCurrencyNum == null) this.declaration.valueCurrency = null;
		if (this.declaration.freightPartCurrency.ecCurrencyNum == null)
			this.declaration.freightPartCurrency = null;
		if (this.declaration.xEcCountryProvenances.ecCountryNum == null)
			this.declaration.xEcCountryProvenances = null;
		if (this.declaration.xEcCountryDedouanement.ecCountryNum == null)
			this.declaration.xEcCountryDedouanement = null;
		this.declaration.agentBroker = null;
		this.declaration.demandeur.ebUserNum = this.userConnected.ebUserNum;
		this.declaration.charger = this.userConnected;
		this.declaration.etablissementCharger.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.douaneService.ajouterEbDeclaration(this.declaration).subscribe(
			(ebDeclaration: EbCustomDeclaration) => {
				requestProcessing.afterGetResponse(event);
				this.router.navigate(["/app/custom/dashboard"]);
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				let title = null;
				let body = null;
				this.translate.get("MODAL.ERROR").subscribe((res: string) => {
					title = res;
				});
				this.modalService.error(title, null, function() {});
			}
		);
	}

	isPageValid() {
		let required: boolean = true;
		this.listEbFormFieldCustom.forEach((it) => {
			if (
				it.required == true &&
				(this.declaration[it.motTechnique] == null || this.declaration[it.motTechnique] == "")
			)
				required = false;
		});

		return required;
	}
}
