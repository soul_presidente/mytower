import { AccessRights, IDChatComponent, Modules } from "@app/utils/enumeration";
import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { Statique } from "@app/utils/statique";
import { ModalService } from "@app/shared/modal/modal.service";
import { EbCustomDeclaration } from "@app/classes/declaration";
import { SearchCriteriaCustom } from "@app/utils/SearchCriteriaCustom";
import { DouaneService } from "@app/services/douane.service";
import { ChatPanelComponent } from "@app/shared/chat-panel/chat-panel.component";
import { EbChat } from "@app/classes/chat";

@Component({
	selector: "app-details-declaration",
	templateUrl: "./details-declaration.component.html",
	styleUrls: ["./details-declaration.component.css"],
})
export class DetailsDeclarationComponent extends ConnectedUserComponent
	implements OnInit, OnDestroy {
	toggleChat: boolean = false;
	Modules = Modules;
	Statique = Statique;
	IDChatComponent = IDChatComponent;

	declaration: EbCustomDeclaration = new EbCustomDeclaration();
	ebCustomDeclarationNum: number;
	module: number;
	variableModule: string;
	hasContributionAccess: boolean = false;

	nbColTexteditor: string = "col-md-12";
	nbColListing: string = "col-md-12";

	@ViewChild("chatCustomPanelComponent", { static: false })
	chatCustomPanelComponent: ChatPanelComponent;

	private subscription: any;

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private douaneService: DouaneService,
		private modalService: ModalService
	) {
		super();
	}

	ngOnInit() {
		let searchCriteriaCustom = new SearchCriteriaCustom();
		this.route.params.subscribe((params: Params) => {
			this.ebCustomDeclarationNum = +params["ebCustomDeclarationNum"];
			this.module = Statique.getModuleNumFromUrl(this.router.url);
			if (this.ebCustomDeclarationNum) {
				searchCriteriaCustom.ebCustomDeclarationNum = this.ebCustomDeclarationNum;
				searchCriteriaCustom.module = this.module;
				searchCriteriaCustom.ebUserNum = this.userConnected.ebUserNum;

				this.douaneService
					.getEbDeclaration(searchCriteriaCustom)
					.subscribe((data: EbCustomDeclaration) => {
						if (data && data.ebCustomDeclarationNum) {
							this.declaration = data;
						} else {
							// si la demande n'existe pas, on redirige vers le dashboard
							this.variableModule = "customs request ";
							let url: string = "failedaccess";
							this.router.navigate([url, this.ebCustomDeclarationNum, this.variableModule]);
						}
					});
			}
		});

		// contribution access rights to module
		if (this.userConnected.listAccessRights) {
			this.hasContributionAccess =
				this.userConnected.listAccessRights.find((access) => {
					return (
						access.ecModuleNum == Modules.CUSTOM && access.accessRight == AccessRights.CONTRIBUTION
					);
				}) !== undefined;
		} else {
			this.hasContributionAccess = false;
		}

		document.addEventListener(
			"open-chat",
			function(e) {
				event.preventDefault();
				event.stopPropagation();
				this.openChat();
			}.bind(this),
			false
		);
	}
	openChat() {
		this.toggleChat = !this.toggleChat;
	}
	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
			this.subscription = null;
		}
	}

	updateChatEmitter(ebchat: EbChat) {
		if (this.chatCustomPanelComponent && ebchat.idChatComponent == IDChatComponent.CUSTOM)
			this.chatCustomPanelComponent.listChat.unshift(ebchat);
	}
}
