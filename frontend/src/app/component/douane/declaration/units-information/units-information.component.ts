import { SearchCriteria } from "@app/utils/searchCriteria";

import { Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";

import { StatiqueService } from "@app/services/statique.service";
import { EbUser } from "@app/classes/user";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbCustomDeclaration } from "@app/classes/declaration";
import { EbFormFieldCompagnie } from "@app/classes/ebFormFieldCompagnie";
import { ebFormField } from "@app/classes/ebFormField";
import { EbCompagnie } from "@app/classes/compagnie";
import { DouaneService } from "@app/services/douane.service";
import { ConfigViewCustom } from "@app/utils/enumeration";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { CompleterService } from "../../../../../../node_modules/ng2-completer";

@Component({
	selector: "app-units-information",
	templateUrl: "./units-information.component.html",
	styleUrls: ["./units-information.component.css"],
})
export class UnitsInformationComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	module: number;
	@Input()
	userConnected: EbUser;
	@Input()
	declaration: EbCustomDeclaration = new EbCustomDeclaration();
	ebFormFieldCompagnie: EbFormFieldCompagnie = new EbFormFieldCompagnie();
	listEbFormFieldCustom: Array<ebFormField> = new Array<ebFormField>();
	criteria: SearchCriteria = new SearchCriteria();
	listViewTotal: Array<ebFormField> = new Array<ebFormField>();
	listFieldTable: Array<ebFormField> = new Array<ebFormField>();
	dataTableConfig: DataTableConfig = new DataTableConfig();
	@ViewChild("pageValid", { static: false })
	pageValid: ElementRef;
	listformFieldTrier: Array<ebFormField> = new Array<ebFormField>();
	constructor(
		private douaneService: DouaneService,
		private completerService: CompleterService,
		protected statiqueService: StatiqueService
	) {
		super();
	}

	ngOnInit() {
		if (!this.declaration.listUnit) this.declaration.listUnit = new Array<Array<ebFormField>>();

		this.ebFormFieldCompagnie.xEbCompagnie = new EbCompagnie();
		this.ebFormFieldCompagnie.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.douaneService.getListChampFormForCompagnie(this.criteria).subscribe((res) => {
			this.ebFormFieldCompagnie = res;
			if (this.ebFormFieldCompagnie != null) {
				if (this.ebFormFieldCompagnie.listFormField != null) {
					this.listEbFormFieldCustom = this.ebFormFieldCompagnie.listFormField;
				}
			}
			this.listEbFormFieldCustom.forEach((it) => {
				if (it.xEbViewForm.ebFormViewNum == ConfigViewCustom.TOTAL) this.listViewTotal.push(it);
				if (it.xEbViewForm.ebFormViewNum == ConfigViewCustom.TABLE) this.listFieldTable.push(it);
			});

			this.TrieList();

			if (this.declaration.ebCustomDeclarationNum == null) {
				this.addLineTable();
			}
		});
	}

	TrieList() {
		let listfieldeWithoutOrdre: Array<ebFormField> = new Array<ebFormField>();
		this.listFieldTable.forEach((el) => {
			if (el.ordre == null) listfieldeWithoutOrdre.push(el);
			else if (el.ordre != null) this.listformFieldTrier.push(el);
		});

		this.listformFieldTrier.sort(function(a, b) {
			return a.ordre < b.ordre ? -1 : b.ordre < a.ordre ? 1 : 0;
		});
		listfieldeWithoutOrdre.forEach((it) => {
			this.listformFieldTrier.push(it);
		});
	}

	addLineTable() {
		let copyFormList = new Array<ebFormField>();
		this.listformFieldTrier.forEach((it) => {
			let data = new ebFormField();
			data.copy(it);
			copyFormList.push(data);
		});

		this.declaration.listUnit.push(copyFormList);
	}
}
