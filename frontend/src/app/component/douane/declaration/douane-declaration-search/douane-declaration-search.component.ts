import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";

import { Modules, SavedFormIdentifier } from "@app/utils/enumeration";
import { SearchCriteriaCustom } from "@app/utils/SearchCriteriaCustom";
import { SearchField } from "@app/classes/searchField";

@Component({
	selector: "app-douane-declaration-search",
	templateUrl: "./douane-declaration-search.component.html",
	styleUrls: ["./douane-declaration-search.component.css"],
})
export class DeclarationDouaneSearchComponent implements OnInit {
	SavedFormIdentifier = SavedFormIdentifier;
	moduleName = Modules;
	advancedOption: SearchCriteriaCustom = new SearchCriteriaCustom();
	initParamFields: Array<SearchField> = new Array<SearchField>();

	@Input()
	module: number;
	@Output()
	onSearch = new EventEmitter<SearchCriteriaCustom>();

	constructor(public generaleMethode: generaleMethodes) {}

	ngOnInit() {
		/* if(this.module==this.moduleName.TRACK){
			this.paramFields = require("../../../../assets/ressources/jsonfiles/tracing-searchfields.json");
		} */
	}

	ngOnChanges() {}

	search(advancedOption: SearchCriteriaCustom) {
		this.onSearch.emit(advancedOption);
	}
}
