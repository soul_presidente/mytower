import { SearchCriteria } from "@app/utils/searchCriteria";
import { Component, ElementRef, Input, OnInit, ViewChild } from "@angular/core";

import { StatiqueService } from "@app/services/statique.service";
import { EbUser } from "@app/classes/user";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbCustomDeclaration } from "@app/classes/declaration";
import { DouaneService } from "@app/services/douane.service";
import { ebFormField } from "@app/classes/ebFormField";
import { EbFormFieldCompagnie } from "@app/classes/ebFormFieldCompagnie";
import { EbCompagnie } from "@app/classes/compagnie";
import { ConfigViewCustom } from "@app/utils/enumeration";
import { CompleterService } from "../../../../../../node_modules/ng2-completer";
import { AdvancedFormCustomComponent } from "@app/shared/form-custom/form-custom.component";

@Component({
	selector: "app-request-information",
	templateUrl: "./request-information.component.html",
	styleUrls: ["./request-information.component.css"],
})
export class RequestInformationComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	module: number;
	@Input()
	userConnected: EbUser;
	@ViewChild("pageValid", { static: false })
	pageValid: ElementRef;
	@Input()
	declaration: EbCustomDeclaration = new EbCustomDeclaration();
	ebFormFieldCompagnie: EbFormFieldCompagnie = new EbFormFieldCompagnie();
	listEbFormFieldCustom: Array<ebFormField> = new Array<ebFormField>();
	listReferenceInformation: Array<ebFormField> = new Array<ebFormField>();
	listShippingInformation: Array<ebFormField> = new Array<ebFormField>();
	listCustomsInformation: Array<ebFormField> = new Array<ebFormField>();
	listCarrierInformation: Array<ebFormField> = new Array<ebFormField>();
	listInvoiceInformation: Array<ebFormField> = new Array<ebFormField>();
	listOtherInformation: Array<ebFormField> = new Array<ebFormField>();
	criteria: SearchCriteria = new SearchCriteria();
	@ViewChild(AdvancedFormCustomComponent, { static: true })
	advancedFormCustomComponent: AdvancedFormCustomComponent;

	constructor(
		private douaneService: DouaneService,
		private completerService: CompleterService,
		protected statiqueService: StatiqueService
	) {
		super();
	}

	ngOnInit() {
		this.ebFormFieldCompagnie.xEbCompagnie = new EbCompagnie();
		this.ebFormFieldCompagnie.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.douaneService.getListChampFormForCompagnie(this.criteria).subscribe((res) => {
			this.ebFormFieldCompagnie = res;
			if (this.ebFormFieldCompagnie != null) {
				if (this.ebFormFieldCompagnie.listFormField != null) {
					this.listEbFormFieldCustom = this.ebFormFieldCompagnie.listFormField;
				}
			}
			this.listEbFormFieldCustom.forEach((it) => {
				if (it.xEbViewForm.ebFormViewNum == ConfigViewCustom.REFERENCE_INFORMATION)
					this.listReferenceInformation.push(it);
				if (it.xEbViewForm.ebFormViewNum == ConfigViewCustom.SHIPPING_INFORMATION)
					this.listShippingInformation.push(it);
				if (it.xEbViewForm.ebFormViewNum == ConfigViewCustom.CUSTOMS_INFORMATION)
					this.listCustomsInformation.push(it);
				if (it.xEbViewForm.ebFormViewNum == ConfigViewCustom.CARRIER_INFORMATION)
					this.listCarrierInformation.push(it);
				if (it.xEbViewForm.ebFormViewNum == ConfigViewCustom.INVOICE_INFORMATION)
					this.listInvoiceInformation.push(it);
				if (it.xEbViewForm.ebFormViewNum == ConfigViewCustom.OTHER_INFORMATION)
					this.listOtherInformation.push(it);
			});
		});
	}
}
