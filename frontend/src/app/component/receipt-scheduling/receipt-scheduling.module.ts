import { DataTablesModule } from "angular-datatables";
import { Ng2CompleterModule } from "ng2-completer";
import { SharedModule } from "@app/shared/module/shared.module";
import { AdvancedSearchModule } from "@app/shared/advanced-search/advanced-search.module";
import { ReceiptSchedulingRoutes } from "./receipt-scheduling.route";
import { NgModule } from "@angular/core";
import { RschDashboardComponent } from "./dashboard/rsch-dashboard.component";
import { RschDashboardTableComponent } from "./dashboard/table/rsch-dashboard-table.component";
import { TableModule } from "primeng/table";
import { EditorModule } from "primeng/editor";
import { CalendarModule } from "primeng/calendar";
import { ProgressSpinnerModule } from "primeng/progressspinner";
import { RschDashboardCalendarComponent } from "./dashboard/calendar/rsch-dashboard-calendar.component";
import { RschCellPriorityComponent } from "./dashboard/table/cells/rsch-cell-priority.component";
import { RschCellWorkDurationComponent } from "./dashboard/table/cells/rsch-cell-workduration.component";
import { RschCellModeTransportPaComponent } from "./dashboard/table/cells/rsch-cell-modetransportpa.component";
import { RschCellDateDeparturePaComponent } from "./dashboard/table/cells/rsch-cell-datedeparturepa.component";
import { RschCellDateArrivalPaComponent } from "./dashboard/table/cells/rsch-cell-datearrivalpa.component";
import { RschCellDockComponent } from "./dashboard/table/cells/rsch-cell-dock.component";
import { RschCellWorkRangeHourComponent } from "./dashboard/table/cells/rsch-cell-workrangehour.component";
import { RschCellDateUnloadingPaComponent } from "./dashboard/table/cells/rsch-cell-dateunloadingpa.component";
import { RschCellCardComponent } from "./dashboard/table/cells/rsch-cell-card.component";

@NgModule({
	imports: [
		ReceiptSchedulingRoutes,
		SharedModule,
		AdvancedSearchModule,
		Ng2CompleterModule,
		DataTablesModule,
		TableModule,
		EditorModule,
		CalendarModule,
		ProgressSpinnerModule,
	],
	declarations: [
		RschDashboardComponent,
		RschDashboardTableComponent,
		RschDashboardCalendarComponent,
		RschCellPriorityComponent,
		RschCellWorkDurationComponent,
		RschCellModeTransportPaComponent,
		RschCellDateDeparturePaComponent,
		RschCellDateArrivalPaComponent,
		RschCellDockComponent,
		RschCellWorkRangeHourComponent,
		RschCellDateUnloadingPaComponent,
		RschCellCardComponent,
	],
	exports: [RschDashboardComponent],
	providers: [],
	entryComponents: [
		RschCellPriorityComponent,
		RschCellWorkDurationComponent,
		RschCellModeTransportPaComponent,
		RschCellDateDeparturePaComponent,
		RschCellDateArrivalPaComponent,
		RschCellDockComponent,
		RschCellWorkRangeHourComponent,
		RschCellDateUnloadingPaComponent,
		RschCellCardComponent,
	],
})
export class ReceiptSchedulingModule {}
