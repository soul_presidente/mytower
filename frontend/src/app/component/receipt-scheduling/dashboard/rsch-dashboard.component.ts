import { AfterViewInit, Component, OnInit, ViewChild } from "@angular/core";
import { SearchCriteriaReceiptScheduling } from "@app/utils/SearchCriteriaReceiptScheduling";
import { ReceiptSchedulingService } from "@app/services/receipt-scheduling.service";
import { RschDashboardTableComponent } from "./table/rsch-dashboard-table.component";
import { TranslateService } from "@ngx-translate/core";
import { Modules, RschUnitScheduleStatus } from "@app/utils/enumeration";
import { RschDashboardCalendarComponent } from "./calendar/rsch-dashboard-calendar.component";
import { StatiqueService } from "@app/services/statique.service";
import { GenericEnum } from "@app/classes/GenericEnum";
import { DockManagementService } from "@app/services/dock-management.service";
import moment from "moment";
import { GenericEnumRSCH } from "@app/classes/rsch/EnumerationRSCH";
import { EbEntrepotDTO } from "@app/classes/dock/EbEntrepotDTO";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { AuthenticationService } from "@app/services/authentication.service";

@Component({
	selector: "app-rsch-dashboard",
	templateUrl: "./rsch-dashboard.component.html",
	styleUrls: ["./rsch-dashboard.component.scss"],
})
export class RschDashboardComponent extends ConnectedUserComponent
	implements OnInit, AfterViewInit {
	// Modal
	modalVisible: boolean;
	modalLoading: boolean;
	modalLoadingText: string;
	modalAction: string;
	modalContentText: string;
	modalHeader: string;
	modalCanConfirm: boolean;
	modalCanCancel: boolean;
	modalCanClose: boolean;

	@ViewChild(RschDashboardTableComponent, { static: true })
	tableComponent: RschDashboardTableComponent;

	@ViewChild(RschDashboardCalendarComponent, { static: true })
	calendarComponent: RschDashboardCalendarComponent;

	filterArrivingBefore: string = null;
	filterStatus: number = null;
	listFilterStatus: Array<GenericEnum> = [];
	listFilterArrivingBefore: Array<GenericEnum> = [];
	listModeTransport: Array<any> = [];
	listEntrepots: Array<EbEntrepotDTO> = [];

	viewTypeSelected: string = "table";
	viewTypeOptions = [];

	constructor(
		protected rschService: ReceiptSchedulingService,
		protected translate: TranslateService,
		protected statiqueService: StatiqueService,
		protected dockManagementService: DockManagementService,
		protected headerService: HeaderService,
		protected authenticationService: AuthenticationService
	) {
		super(authenticationService);
	}

	ngOnInit() {
		this.headerService.registerActionButtons(this.actionButtons);
		this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());

		this.viewTypeOptions = [
			{
				label: this.translate.instant("RECEIPT_SCHEDULING.DASHBOARD.DASH_TABLE"),
				value: "table",
				icon: "fa fa-table",
			},
			{
				label: this.translate.instant("RECEIPT_SCHEDULING.DASHBOARD.DASH_CALENDAR"),
				value: "calendar",
				icon: "fa fa-calendar",
			},
		];
	}

	ngAfterViewInit() {
		this.initListStatic(() => {
			this.tableComponent.initRS();
		});
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "MODULES.RSCH",
				path: "/app/receipt-scheduling/dashboard",
			},
		];
	}

	private actionButtons: Array<HeaderInfos.ActionButton> = [
		{
			label: "RECEIPT_SCHEDULING.SIDEBAR.CALCULATE",
			icon: "fa fa-calculator",
			action: this.handlerCalculateAction.bind(this),
			displayCondition: () => {
				return this.acls[this.ACL.Rule.Module_RSCH] === this.ACL.RuleValues.Module_RSCH.Write;
			},
		},
		{
			label: "RECEIPT_SCHEDULING.SIDEBAR.PLANIFY",
			icon: "fa fa-calendar-check-o",
			action: this.handlerPlanifyAction.bind(this),
			displayCondition: () => {
				return this.acls[this.ACL.Rule.Module_RSCH] === this.ACL.RuleValues.Module_RSCH.Write;
			},
		},
		{
			label: "RECEIPT_SCHEDULING.SIDEBAR.UNPLANIFY",
			icon: "fa fa-calendar-times-o",
			action: this.handlerUnplanifyAction.bind(this),
			displayCondition: () => {
				return this.acls[this.ACL.Rule.Module_RSCH] === this.ACL.RuleValues.Module_RSCH.Write;
			},
		},
	];

	private handlerCalculateAction() {
		this.modalVisible = true;
		this.modalHeader = "RECEIPT_SCHEDULING.SIDEBAR.CALCULATE";
		this.modalAction = "calculate";
		this.modalLoading = true;
		this.modalLoadingText = "RECEIPT_SCHEDULING.RSCH_ACTION.LOAD_MSG_CHECK_VALIDITY";
		this.modalCanClose = true;
		this.modalCanCancel = false;
		this.modalCanConfirm = false;

		let criterias: SearchCriteriaReceiptScheduling = new SearchCriteriaReceiptScheduling();
		criterias.selected = true;
		criterias.listStatut.push(RschUnitScheduleStatus.TO_PLANIFY);
		criterias.listStatut.push(RschUnitScheduleStatus.CALCULATED);

		this.rschService.getCountListRschUnitSchedule(criterias).subscribe((count) => {
			if (count > 0) {
				this.modalLoadingText = "RECEIPT_SCHEDULING.RSCH_ACTION.LOAD_MSG_CALCULATING";
				this.rschService.doCalculate().subscribe(
					(count) => {
						this.modalVisible = false;
						this.reloadTable();
					},
					(error) => {
						this.modalLoading = false;
						this.modalContentText = this.translate.instant(
							"RECEIPT_SCHEDULING.RSCH_ACTION.ERROR_DURING_CALCULATE"
						);
					}
				);
			} else {
				this.modalLoading = false;
				this.modalContentText = this.translate.instant(
					"RECEIPT_SCHEDULING.RSCH_ACTION.WARN_CALCULATE_NO_SELECTED"
				);
			}
		});
	}

	private handlerPlanifyAction() {
		this.modalVisible = true;
		this.modalHeader = "RECEIPT_SCHEDULING.SIDEBAR.PLANIFY";
		this.modalAction = "planify";
		this.modalLoading = true;
		this.modalLoadingText = "RECEIPT_SCHEDULING.RSCH_ACTION.LOAD_MSG_CHECK_VALIDITY";
		this.modalCanClose = false;
		this.modalCanCancel = true;
		this.modalCanConfirm = false;

		let criterias: SearchCriteriaReceiptScheduling = new SearchCriteriaReceiptScheduling();
		criterias.selected = true;
		criterias.listStatut.push(RschUnitScheduleStatus.CALCULATED);

		this.rschService.getCountListRschUnitSchedule(criterias).subscribe((count) => {
			if (count > 0) {
				this.modalLoading = false;
				const textUnformatted = this.translate.instant(
					"RECEIPT_SCHEDULING.RSCH_ACTION.CONFIRM_PLANIFY"
				);
				this.modalContentText = count + textUnformatted;
				this.modalCanConfirm = true;
			} else {
				this.modalLoading = false;
				this.modalContentText = this.translate.instant(
					"RECEIPT_SCHEDULING.RSCH_ACTION.WARN_PLANIFY_NO_SELECTED"
				);
				this.modalCanClose = true;
				this.modalCanCancel = false;
			}
		});
	}

	private handlerUnplanifyAction() {
		this.modalVisible = true;
		this.modalHeader = "RECEIPT_SCHEDULING.SIDEBAR.UNPLANIFY";
		this.modalAction = "unplanify";
		this.modalLoading = true;
		this.modalLoadingText = "RECEIPT_SCHEDULING.RSCH_ACTION.LOAD_MSG_CHECK_VALIDITY";
		this.modalCanClose = false;
		this.modalCanCancel = true;
		this.modalCanConfirm = false;

		let criterias: SearchCriteriaReceiptScheduling = new SearchCriteriaReceiptScheduling();
		criterias.selected = true;
		criterias.listStatut.push(RschUnitScheduleStatus.PLANIFIED);

		this.rschService.getCountListRschUnitSchedule(criterias).subscribe((count) => {
			if (count > 0) {
				this.modalLoading = false;
				const textUnformatted = this.translate.instant(
					"RECEIPT_SCHEDULING.RSCH_ACTION.CONFIRM_UNPLANIFY"
				);
				this.modalContentText = count + textUnformatted;
				this.modalCanConfirm = true;
			} else {
				this.modalLoading = false;
				this.modalContentText = this.translate.instant(
					"RECEIPT_SCHEDULING.RSCH_ACTION.WARN_UNPLANIFY_NO_SELECTED"
				);
				this.modalCanClose = true;
				this.modalCanCancel = false;
			}
		});
	}

	modalConfirm() {
		if (this.modalAction === "planify" || this.modalAction === "unplanify") {
			if (this.modalAction === "planify") {
				this.modalLoading = true;
				this.modalLoadingText = "RECEIPT_SCHEDULING.RSCH_ACTION.LOAD_MSG_PLANIFYING";
				this.modalCanClose = true;
				this.modalCanCancel = false;
				this.modalCanConfirm = false;
				this.rschService.listUnifiableRschUnitSchedule().subscribe(
					(result) => {
						this.modalVisible = false;
						this.reloadTable();
					},
					(error) => {
						this.modalLoading = false;
						this.modalContentText = this.translate.instant(
							"RECEIPT_SCHEDULING.RSCH_ACTION.ERROR_DURING_PLANIFY"
						);
					}
				);
			}

			if (this.modalAction === "unplanify") {
				this.modalLoading = true;
				this.modalLoadingText = "RECEIPT_SCHEDULING.RSCH_ACTION.LOAD_MSG_UNPLANIFYING";
				this.modalCanClose = true;
				this.modalCanCancel = false;
				this.modalCanConfirm = false;
				this.rschService.doUnplanify().subscribe(
					(result) => {
						this.modalVisible = false;
						this.reloadTable();
					},
					(error) => {
						this.modalLoading = false;
						this.modalContentText = this.translate.instant(
							"RECEIPT_SCHEDULING.RSCH_ACTION.ERROR_DURING_UNPLANIFY"
						);
					}
				);
			}
		}
	}
	modalClose() {
		this.modalVisible = false;
	}

	reloadTable() {
		if (this.tableComponent) this.tableComponent.reloadTable();
	}

	onChangeFilter(filterName) {
		if (filterName === "status") {
			if (this.filterStatus != null) {
				this.tableComponent.searchInput.listStatut = [this.filterStatus];
				this.calendarComponent.searchInput.listStatut = [this.filterStatus];
			} else {
				this.tableComponent.searchInput.listStatut = [];
				this.calendarComponent.searchInput.listStatut = [];
			}
		}

		if (filterName == "arrivingBefore") {
			let filterDate = this.getArrivingBeforeFilterDate(this.filterArrivingBefore);
			this.tableComponent.searchInput.maxArrivalDateTp = filterDate;
			this.calendarComponent.searchInput.maxArrivalDateTp = filterDate;
		}

		//Fire reload
		this.tableComponent.reloadTable();
		this.calendarComponent.reloadItems();
	}

	initListStatic(callback: () => any = undefined) {
		this.statiqueService.getListModeTransport().subscribe((modeTransports) => {
			this.listModeTransport = modeTransports;

			this.dockManagementService.getListEntrepot(true).subscribe((entrepots) => {
				this.listEntrepots = entrepots;

				this.statiqueService
					.getListGenericEnum(GenericEnumRSCH.UnitScheduleStatus)
					.subscribe((statuses) => {
						this.listFilterStatus = statuses;
						if (callback) callback();
					});
			});

			//These belows are not required for dashboard, can load them asynchronously
			this.statiqueService.getListGenericEnum(GenericEnumRSCH.ArrivingBefore).subscribe((data) => {
				this.listFilterArrivingBefore = data;
			});
		});
	}

	// prettier-ignore
	getArrivingBeforeFilterDate(libelle: string): Date {
    if(libelle == null) return null;

    switch(libelle) {
      case "TODAY":
        return moment().endOf('day').toDate();
      case "DAY_PLUS_1":
        return moment().add(1, "days").endOf('day').toDate();
      case "DAY_PLUS_2":
        return moment().add(2, "days").endOf('day').toDate();
      case "DAY_PLUS_3":
        return moment().add(3, "days").endOf('day').toDate();
      case "WEEK_PLUS_1":
        return moment().add(1, "weeks").endOf('day').toDate();
      case "WEEK_PLUS_2":
        return moment().add(2, "weeks").endOf('day').toDate();
      case "MONTH_PLUS_1":
        return moment().add(1, "months").endOf('day').toDate();
      case "MONTH_PLUS_2":
        return moment().add(2, "months").endOf('day').toDate();
    }
  }

	onTableDataChanged() {
		this.calendarComponent.reloadItems();
	}
}
