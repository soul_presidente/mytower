import { Component, OnInit } from "@angular/core";
import { DatePipe } from "@angular/common";
import { ReceiptSchedulingService } from "@app/services/receipt-scheduling.service";
import { DockManagementService } from "@app/services/dock-management.service";
import moment from "moment";
import { SearchCriteriaReceiptScheduling } from "@app/utils/SearchCriteriaReceiptScheduling";
import { RschUnitScheduleStatus } from "@app/utils/enumeration";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-rsch-dashboard-calendar",
	templateUrl: "./rsch-dashboard-calendar.component.html",
	styleUrls: ["./rsch-dashboard-calendar.component.scss"],
})
export class RschDashboardCalendarComponent implements OnInit {
	events: any = [];
	headerConfig: any;
	resources: any[];
	optionConfig: any;
	defaultView: string;
	displayEvent: any;
	ttEvent: boolean = false;
	eventmodal: string;
	clickDate: Date;
	timezone: any;
	height: any;

	defaultMinDate: Date;
	defaultMaxDate: Date;

	searchInput = new SearchCriteriaReceiptScheduling();

	constructor(
		protected datePipe: DatePipe,
		protected receiptSchedulingService: ReceiptSchedulingService,
		protected dockManagementService: DockManagementService,
		protected translateService: TranslateService
	) {}

	ngOnInit() {
		let self = this;

		this.defaultMinDate = moment()
			.subtract(1, "months")
			.startOf("day")
			.toDate();
		this.defaultMaxDate = moment()
			.add(6, "months")
			.endOf("day")
			.toDate();

		this.timezone = "local";

		this.headerConfig = {
			left: "prev,next today",
			center: "title",
			right: "month,agendaWeek,timelineDay,listWeek",
		};

		this.optionConfig = {
			schedulerLicenseKey: "CC-Attribution-NonCommercial-NoDerivatives",
			header: this.headerConfig,
			defaultView: "timelineDay",
			views: {
				timelineThreeDays: {
					type: "timeline",
					duration: { days: 3 },
				},
			},
			resourceLabelText: "Docks",
			eventLimit: false,
			resources: function(callback) {
				self.dockManagementService.getListEntrepot(true).subscribe((rEntrepots) => {
					let resources = [];
					rEntrepots.forEach((eEntrepot) => {
						let resource = {
							id: "E" + eEntrepot.ebEntrepotNum,
							title: eEntrepot.reference,
							children: [],
						};

						eEntrepot.docks.forEach((eDock) => {
							let childrenResource = {
								id: eDock.ebDockNum,
								title: eDock.reference,
								eventColor: eDock.couleur,
							};
							resource.children.push(childrenResource);
						});

						resources.push(resource);
					});

					callback(resources);
				});
			},
			//events: this.events,
			navLinks: true,
			selectable: true,
			editable: true,
			timezone: this.timezone,
			contentHeight: 500,
			select: function(start, end, jsEvent, view) {},
		};

		this.reloadItems();
	}
	linkDayClick(event, fc) {
		fc.changeView("timelineDay");
		fc.gotoDate(event.weekStart._i);
	}
	handleEventClick(event) {
		this.clickDate = event.date._d;
	}
	handleNewEvent(event) {}
	updateEvent(model: any) {}
	cancelModalAddEvent() {
		this.ttEvent = false;
	}
	confirmModalAddEvent() {
		let datee = this.datePipe.transform(this.clickDate, "yyyy-MM-ddThh:mm:ss");
		this.events.push({
			title: this.eventmodal,
			start: datee,
		});
		this.ttEvent = false;
	}

	reloadItems() {
		this.searchInput.minArrivalDatePa = this.defaultMinDate;

		if (this.searchInput.maxArrivalDatePa == null) {
			this.searchInput.maxArrivalDatePa = this.defaultMaxDate;
		}
		if (!this.searchInput.listStatut || this.searchInput.listStatut.length === 0) {
			this.searchInput.listStatut = [
				RschUnitScheduleStatus.CALCULATED,
				RschUnitScheduleStatus.PLANIFIED,
			];
		}

		this.events = [];
		const $this = this;
		this.receiptSchedulingService
			.getListRschUnitSchedule(this.searchInput)
			.subscribe((rschUnit) => {
				rschUnit.forEach((item) => {
					let dateString = this.datePipe.transform(item.dateArrivalPa, "yyyy-MM-dd");

					let titleRendering: string;
					if (item.status === RschUnitScheduleStatus.CALCULATED) {
						titleRendering =
							item.demandeTpRefTransport +
							" (" +
							$this.translateService.instant("RECEIPT_SCHEDULING.STATUS_VALUES.CALCULATED") +
							")";
					} else {
						titleRendering = item.demandePaRefTransport;
					}

					const colorRendering =
						item.status === RschUnitScheduleStatus.CALCULATED ? "#8e8e8e" : null;

					let event = {
						id: item.ebRschUnitScheduleNum,
						resourceId: item.dockNum,
						start: dateString + "T" + item.workRangeHour.startHour,
						end: dateString + "T" + item.workRangeHour.endHour,
						title: titleRendering,
						eventColor: colorRendering,
					};
					this.events.push(event);
				});
			});
	}
}
