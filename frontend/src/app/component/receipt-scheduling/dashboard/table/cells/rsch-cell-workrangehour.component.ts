import { Component, OnInit } from "@angular/core";
import { RschGenericCell } from "./rsch-generic-cell";

@Component({
	selector: "app-rsch-cell-workrangehour",
	templateUrl: "./rsch-cell-workrangehour.component.html",
	styleUrls: ["./rsch-generic-cell.scss"],
})
export class RschCellWorkRangeHourComponent extends RschGenericCell<any> implements OnInit {
	rawValue: string = "";

	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.rawValue =
			this.model.workRangeHour != null &&
			this.model.workRangeHour.startHour != null &&
			this.model.workRangeHour.endHour != null
				? this.model.workRangeHour.startHour + "-" + this.model.workRangeHour.endHour
				: "";
	}
	chnageWorkRangeHour() {
		if (
			this.model.workRangeHour.startHour == null ||
			this.model.workRangeHour.endHour == null ||
			(this.model.workRangeHour.startHour as any) == "" ||
			(this.model.workRangeHour.endHour as any) == ""
		) {
			this.model.workRangeHourLocked = false;
		} else {
			this.model.workRangeHourLocked = true;
		}
		this.outputEvent("value-change", "workRangeHour");
	}
}
