import { Component, OnInit } from "@angular/core";
import { RschGenericCell } from "./rsch-generic-cell";
import { Statique } from "@app/utils/statique";

@Component({
	selector: "app-rsch-cell-dateunloadingpa",
	templateUrl: "./rsch-cell-dateunloadingpa.component.html",
	styleUrls: ["./rsch-generic-cell.scss"],
})
export class RschCellDateUnloadingPaComponent extends RschGenericCell<any> implements OnInit {
	dateFormatted: string = "";

	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.dateFormatted =
			this.model.dateUnloadingPa != null ? Statique.formatDate(this.model.dateUnloadingPa) : "";
	}
	changeDateUnloadingPa(date) {
		this.model.dateUnloadingPa = date;
		if (
			this.model.dateUnloadingPa == null ||
			(this.model.dateUnloadingPa as any) == "" ||
			(this.model.dateUnloadingPa as any) == "Invalid Date"
		) {
			this.model.dateUnloadingPa = null;
			this.model.dateUnloadingPaLocked = false;
		} else {
			this.model.dateUnloadingPaLocked = true;
		}
		this.outputEvent("value-change", "dateUnloadingPa");
	}
}
