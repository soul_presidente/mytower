import { Component, OnInit } from "@angular/core";
import { RschGenericCell } from "./rsch-generic-cell";
import { Statique } from "@app/utils/statique";
import { EbEntrepotDTO } from "@app/classes/dock/EbEntrepotDTO";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";

@Component({
	selector: "app-rsch-cell-dock",
	templateUrl: "./rsch-cell-dock.component.html",
	styleUrls: ["./rsch-generic-cell.scss"],
})
export class RschCellDockComponent extends RschGenericCell<RschCellDockComponent.Params>
	implements OnInit {
	Statique = Statique;
	listEntrepots: any;
	rawValue: string = "";
	listDockOfCurrentEntrepot: any;

	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.listEntrepots = this.params.listEntrepots ? this.params.listEntrepots : [];
		this.rawValue = this.model.dockName != null ? this.model.dockName : "";

		this.listDockOfCurrentEntrepot = this.getAllDocksForEntrepot(
			this.model.demandeTpEntrepotUnloadingNum
		);
	}

	getAllDocksForEntrepot(ebEntrepotNum: number): Array<any> {
		let returnList = [];
		this.listEntrepots.forEach((entrepot) => {
			if (entrepot.ebEntrepotNum === ebEntrepotNum) {
				returnList = entrepot.docks;
			}
		});
		return returnList;
	}
	changeDock() {
		if (this.model.dockNum != null) {
			this.model.dockLocked = true;
		} else {
			this.model.dockLocked = false;
		}
		this.outputEvent("value-change", "dock");
	}
}

export namespace RschCellDockComponent {
	export class Params extends GenericCell.Params {
		listEntrepots: Array<EbEntrepotDTO>;
	}
}
