import { Component, OnInit } from "@angular/core";
import { RschGenericCell } from "./rsch-generic-cell";
import moment from "moment";

@Component({
	selector: "app-rsch-cell-workduration",
	templateUrl: "./rsch-cell-workduration.component.html",
	styleUrls: ["./rsch-generic-cell.scss"],
})
export class RschCellWorkDurationComponent extends RschGenericCell<any> implements OnInit {
	protected workDurationFormatted = "";

	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		if (this.model.workDuration != null) {
			this.workDurationFormatted = moment(this.model.workDuration, "HH:mm:ss").format("HH:mm");
		}
	}
}
