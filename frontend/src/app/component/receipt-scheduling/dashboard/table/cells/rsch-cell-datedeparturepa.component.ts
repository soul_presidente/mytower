import { Component, OnInit } from "@angular/core";
import { RschGenericCell } from "./rsch-generic-cell";
import { Statique } from "@app/utils/statique";

@Component({
	selector: "app-rsch-cell-datedeparturepa",
	templateUrl: "./rsch-cell-datedeparturepa.component.html",
	styleUrls: ["./rsch-generic-cell.scss"],
})
export class RschCellDateDeparturePaComponent extends RschGenericCell<any> implements OnInit {
	dateFormatted: string = "";

	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.dateFormatted =
			this.model.dateDeparturePa != null ? Statique.formatDate(this.model.dateDeparturePa) : "";
	}
	changeDateDeparturePa(date) {
		this.model.dateDeparturePa = date;
		if (
			this.model.dateDeparturePa == null ||
			(this.model.dateDeparturePa as any) == "" ||
			(this.model.dateDeparturePa as any) == "Invalid Date"
		) {
			this.model.dateDeparturePa = null;
			this.model.dateDeparturePaLocked = false;
		} else {
			this.model.dateDeparturePaLocked = true;
		}
		this.outputEvent("value-change", "dateDeparturePa");
	}
}
