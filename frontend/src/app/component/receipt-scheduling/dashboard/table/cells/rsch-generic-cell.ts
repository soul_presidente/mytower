import { OnInit } from "@angular/core";
import { Modules, RschUnitScheduleStatus } from "@app/utils/enumeration";
import { AuthenticationService } from "@app/services/authentication.service";
import { Statique } from "@app/utils/statique";
import { EbUser } from "@app/classes/user";
import { EbRschUnitScheduleDTO } from "@app/classes/rsch/EbRschUnitScheduleDTO";
import { RschDashboardTableComponent } from "../rsch-dashboard-table.component";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";

export abstract class RschGenericCell<P extends GenericCell.Params>
	extends GenericCell<EbRschUnitScheduleDTO, P, RschDashboardTableComponent>
	implements OnInit {
	Statique = Statique;

	connectedUser: EbUser;
	isRschContributor: boolean = false;

	constructor() {
		super();
	}

	ngOnInit() {
		this.connectedUser = AuthenticationService.getConnectedUser();
		this.isRschContributor = this.connectedUser.hasContribution(Modules.RECEIPT_SCHEDULING);
	}

	isEditable(): boolean {
		return (
			(this.model.status === RschUnitScheduleStatus.TO_PLANIFY ||
				this.model.status === RschUnitScheduleStatus.CALCULATED) &&
			this.isRschContributor
		);
	}
}
