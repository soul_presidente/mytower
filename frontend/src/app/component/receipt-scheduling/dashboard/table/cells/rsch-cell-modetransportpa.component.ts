import { Component, OnInit } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";
import { RschGenericCell } from "./rsch-generic-cell";

@Component({
	selector: "app-rsch-cell-modetransportpa",
	templateUrl: "./rsch-cell-modetransportpa.component.html",
	styleUrls: ["./rsch-generic-cell.scss"],
})
export class RschCellModeTransportPaComponent
	extends RschGenericCell<RschCellModeTransportPaComponent.Params>
	implements OnInit {
	listModeTransp: any;
	rawValue: string = "";

	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.listModeTransp = this.params.listModeTransp ? this.params.listModeTransp : [];
		this.rawValue = Statique.getModeTransportStringFromKey(
			this.model.modeTransportPa,
			this.listModeTransp
		);
	}
	chnageModeTransportPa() {
		if (this.model.modeTransportPa != null) {
			this.model.modeTransportPaLocked = true;
		} else {
			this.model.modeTransportPaLocked = false;
		}
		this.outputEvent("value-change", "modeTransportPa");
	}
}

export namespace RschCellModeTransportPaComponent {
	export class Params extends GenericCell.Params {
		listModeTransp: Array<any>;
	}
}
