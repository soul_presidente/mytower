import { Component } from "@angular/core";
import { RschGenericCell } from "./rsch-generic-cell";

@Component({
	selector: "app-rsch-cell-card",
	templateUrl: "./rsch-cell-card.component.html",
	styleUrls: ["./rsch-cell-card.component.scss"],
})
export class RschCellCardComponent extends RschGenericCell<any> {}
