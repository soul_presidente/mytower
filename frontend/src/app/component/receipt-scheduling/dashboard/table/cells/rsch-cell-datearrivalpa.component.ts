import { Component, OnInit } from "@angular/core";
import { RschGenericCell } from "./rsch-generic-cell";
import { Statique } from "@app/utils/statique";

@Component({
	selector: "app-rsch-cell-datearrivalpa",
	templateUrl: "./rsch-cell-datearrivalpa.component.html",
	styleUrls: ["./rsch-generic-cell.scss"],
})
export class RschCellDateArrivalPaComponent extends RschGenericCell<any> implements OnInit {
	dateFormatted: string = "";

	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.dateFormatted =
			this.model.dateArrivalPa != null ? Statique.formatDate(this.model.dateArrivalPa) : "";
	}
	changeDateArrivalPa(date) {
		this.model.dateArrivalPa = date;
		if (
			this.model.dateArrivalPa == null ||
			(this.model.dateArrivalPa as any) == "" ||
			(this.model.dateArrivalPa as any) == "Invalid Date"
		) {
			this.model.dateArrivalPa = null;
			this.model.dateArrivalPaLocked = false;
		} else {
			this.model.dateArrivalPaLocked = true;
		}
		this.outputEvent("value-change", "dateArrivalPa");
	}
}
