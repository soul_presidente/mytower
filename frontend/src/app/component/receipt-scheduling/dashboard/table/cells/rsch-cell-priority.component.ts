import { Component } from "@angular/core";
import { RschGenericCell } from "./rsch-generic-cell";

@Component({
	selector: "app-rsch-cell-priority",
	templateUrl: "./rsch-cell-priority.component.html",
	styleUrls: ["./rsch-generic-cell.scss"],
})
export class RschCellPriorityComponent extends RschGenericCell<any> {
	constructor() {
		super();
	}
}
