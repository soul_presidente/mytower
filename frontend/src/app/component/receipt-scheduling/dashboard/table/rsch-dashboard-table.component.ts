import {
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	Renderer2,
	ViewChild,
} from "@angular/core";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { GenericTableScreen, Modules } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { EbRschUnitScheduleDTO } from "@app/classes/rsch/EbRschUnitScheduleDTO";
import { EbDemande } from "@app/classes/demande";
import { StatiqueService } from "@app/services/statique.service";
import { DockManagementService } from "@app/services/dock-management.service";
import { ReceiptSchedulingService } from "@app/services/receipt-scheduling.service";
import { SearchCriteriaReceiptScheduling } from "@app/utils/SearchCriteriaReceiptScheduling";
import { RschCellPriorityComponent } from "./cells/rsch-cell-priority.component";
import { RschCellWorkDurationComponent } from "./cells/rsch-cell-workduration.component";
import { RschCellModeTransportPaComponent } from "./cells/rsch-cell-modetransportpa.component";
import { RschCellDateDeparturePaComponent } from "./cells/rsch-cell-datedeparturepa.component";
import { RschCellDateArrivalPaComponent } from "./cells/rsch-cell-datearrivalpa.component";
import { RschCellDockComponent } from "./cells/rsch-cell-dock.component";
import { RschCellWorkRangeHourComponent } from "./cells/rsch-cell-workrangehour.component";
import { GenericEnum } from "@app/classes/GenericEnum";
import { EbEntrepotDTO } from "@app/classes/dock/EbEntrepotDTO";
import { RschCellDateUnloadingPaComponent } from "./cells/rsch-cell-dateunloadingpa.component";
import { CardListParams } from "@app/shared/card-list/card-list.params";
import { RschCellCardComponent } from "./cells/rsch-cell-card.component";

@Component({
	selector: "app-rsch-dashboard-table",
	templateUrl: "./rsch-dashboard-table.component.html",
	styleUrls: ["./rsch-dashboard-table.component.scss"],
})
export class RschDashboardTableComponent implements OnInit {
	Module = Modules;
	Statique = Statique;

	searchInput = new SearchCriteriaReceiptScheduling();

	@Output()
	onDataChanged: EventEmitter<void> = new EventEmitter<void>();

	@Input()
	listFilterStatus: Array<GenericEnum> = [];
	@Input()
	listModeTransport: Array<any> = [];
	@Input()
	listEntrepots: Array<EbEntrepotDTO> = [];

	GenericTableScreen = GenericTableScreen;

	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	listRschUnitSchedule: Array<EbRschUnitScheduleDTO> = new Array<EbRschUnitScheduleDTO>();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	constructor(
		protected renderer: Renderer2,
		protected translate: TranslateService,
		protected modalService: ModalService,
		protected statiqueService: StatiqueService,
		protected dockManagementService: DockManagementService,
		protected receiptSchedulingService: ReceiptSchedulingService
	) {}

	ngOnInit() {}

	async initRS() {
		this.dataInfos.cols = [
			//*********TP */
			{
				field: "demandeTpRefTransport",
				header: "Réference de transport",
				translateCode: "RECEIPT_SCHEDULING.FIELD.TP_REF_TRANSP",
				headerClass: "rsch-table_header_gray",
				render: (item: string) => {
					return item != null ? item : "";
				},
			},
			{
				field: "tracingTpCustomerReference",
				header: "Unit Reference",
				translateCode: "RECEIPT_SCHEDULING.FIELD.TP_UNIT_REF",
				headerClass: "rsch-table_header_gray",
				render: (item) => {
					return item != null ? item : "";
				},
			},
			{
				field: "unitTpIndex",
				header: "Unit number",
				translateCode: "RECEIPT_SCHEDULING.FIELD.TP_UNIT_INDEX",
				headerClass: "rsch-table_header_gray",
				render: (item) => {
					return item;
				},
			},
			{
				field: "demandeTpLibelleOriginCountry",
				header: "Origine",
				translateCode: "RECEIPT_SCHEDULING.FIELD.ORIGIN",
				headerClass: "rsch-table_header_gray",
				render: (item) => {
					return item != null ? item : "";
				},
			},
			{
				field: "demandeTpLibelleDestCountry",
				header: "Destination",
				translateCode: "RECEIPT_SCHEDULING.FIELD.DESTINATION",
				headerClass: "rsch-table_header_gray",
				render: (item) => {
					return item != null ? item : "";
				},
			},
			{
				field: "demandeTpEntrepotUnloadingRef",
				header: "Entrepôt de déchargement",
				translateCode: "DOCK.FIELD.WAREHOUSE_UNLOADING",
				headerClass: "rsch-table_header_gray",
				render: (item) => {
					return item != null ? item : "";
				},
			},

			{
				field: "demandeTpQuoteDeliveryTime",
				header: "Date d'arrivée",
				translateCode: "RECEIPT_SCHEDULING.FIELD.TP_DATE_ARRIVAL",
				headerClass: "rsch-table_header_gray",
				width: "165px",
				render: (item: Date) => {
					return item != null ? Statique.formatDate(item) : "";
				},
			},
			{
				field: "demandeTpModeTransport",
				header: "Mode de transport",
				translateCode: "RECEIPT_SCHEDULING.FIELD.TP_MODE_TRANSP",
				headerClass: "rsch-table_header_gray",
				render: (item: number) => {
					return item != null
						? Statique.getModeTransportStringFromKey(item, this.listModeTransport)
						: "";
				},
			},
			{
				field: "status",
				header: "Status",
				translateCode: "RECEIPT_SCHEDULING.FIELD.STATUS",
				headerClass: "rsch-table_header_gray",
				render: (item: number) => {
					// prettier-ignore
					return item != null ?
            this.translate.instant(
              "RECEIPT_SCHEDULING.STATUS_VALUES." + this.listFilterStatus[item].key
            ) : "";
				},
			},
			//******* */TPA
			{
				field: "priority",
				header: "Priorité",
				translateCode: "RECEIPT_SCHEDULING.FIELD.PRIORITY",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: RschCellPriorityComponent,
				genericCellHandler: this.globalGenericCellHandler,
			},
			{
				field: "workDuration",
				header: "Temps de travail",
				translateCode: "RECEIPT_SCHEDULING.FIELD.WORK_DURATION",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: RschCellWorkDurationComponent,
				genericCellHandler: this.globalGenericCellHandler,
			},
			{
				field: "demandePa",
				header: "Réference de transport",
				translateCode: "RECEIPT_SCHEDULING.FIELD.PA_REF_TRANSP",
				render: (item: EbDemande) => {
					return item != null ? item.refTransport : "";
				},
			},
			{
				field: "modeTransportPa",
				header: "Mode de transport",
				translateCode: "RECEIPT_SCHEDULING.FIELD.PA_MODE_TRANSP",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: RschCellModeTransportPaComponent,
				genericCellParams: new RschCellModeTransportPaComponent.Params({
					listModeTransp: this.listModeTransport,
				}),
				genericCellHandler: this.globalGenericCellHandler,
			},
			{
				field: "dateDeparturePa",
				header: "Date de départ",
				translateCode: "RECEIPT_SCHEDULING.FIELD.PA_DATE_DEPARTURE",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: RschCellDateDeparturePaComponent,
				genericCellHandler: this.globalGenericCellHandler,
			},
			{
				field: "dateArrivalPa",
				header: "Date d'arrivée",
				width: "165px",
				translateCode: "RECEIPT_SCHEDULING.FIELD.PA_DATE_ARRIVAL",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: RschCellDateArrivalPaComponent,
				genericCellHandler: this.globalGenericCellHandler,
			},
			{
				field: "dockNum",
				header: "Quai",
				width: "165px",
				translateCode: "RECEIPT_SCHEDULING.FIELD.DOCK",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: RschCellDockComponent,
				genericCellParams: new RschCellDockComponent.Params({
					listEntrepots: this.listEntrepots,
				}),
				genericCellHandler: this.globalGenericCellHandler,
			},
			{
				field: "dateUnloadingPa",
				header: "Date de déchargement",
				width: "165px",
				translateCode: "RECEIPT_SCHEDULING.FIELD.PA_DATE_UNLOADING",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: RschCellDateUnloadingPaComponent,
				genericCellHandler: this.globalGenericCellHandler,
			},
			{
				field: "workRangeHour",
				header: "Créneau horaire",
				translateCode: "RECEIPT_SCHEDULING.FIELD.WORK_RANGE_HOUR",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: RschCellWorkRangeHourComponent,
				genericCellHandler: this.globalGenericCellHandler,
			},
			{
				field: "totalCost",
				header: "Total Cost",
				translateCode: "RECEIPT_SCHEDULING.FIELD.TOTAL_COST",
				headerClass: "rsch-table_header_gray",
				render: (item: number, type, row) => {
					return item != null
						? item +
								" " +
								(row.demandeTpCurrencyInvoiceSymbol != null
									? row.demandeTpCurrencyInvoiceSymbol
									: row.demandeTpCurrencyInvoiceCode)
						: "";
				},
			},
		];

		this.dataInfos.dataKey = "ebRschUnitScheduleNum";
		this.dataInfos.showAddBtn = false;
		this.dataInfos.showAdvancedSearchBtn = true;
		this.dataInfos.showEditBtn = false;
		this.dataInfos.showDeleteBtn = false;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink =
			Statique.controllerReceiptScheduling + "/list-rschUnit-schedule-table";
		this.dataInfos.dataType = EbRschUnitScheduleDTO;
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.dataCheckedField = "selected";

		this.dataInfos.showCheckbox = true;
		this.dataInfos.showCheckboxGlobal = false;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.blankBtnCount = 0;

		//this.dataInfos.cardView = true;
		this.dataInfos.cardRenderMode = CardListParams.RenderMode.GenericCell;
		this.dataInfos.cardGenericCellClass = RschCellCardComponent;
	}

	changeModel(form: any) {
		setTimeout(
			function() {
				this.checkFields(form);
			}.bind(this),
			10
		);
	}

	rerenderTable() {}

	onDataLoaded(data: Array<EbRschUnitScheduleDTO>) {
		//this.onDataChanged.emit();
	}

	onCheckRow(rowModel: EbRschUnitScheduleDTO) {
		this.autoSaveRow(rowModel);
	}

	globalGenericCellHandler(key: string, value: any, model: any, context: any) {
		if (key === "lock-change") {
			if (model[value] == null) {
				model[value] = true;
			} else {
				model[value] = !model[value];
			}
		}

		// Auto save
		if (key === "lock-change" || key === "value-change") {
			context.autoSaveRow(model);
		}
	}

	autoSaveRow(row: EbRschUnitScheduleDTO) {
		this.receiptSchedulingService.dashboardInlineUpdate(row).subscribe((result) => {
			this.onDataChanged.emit();
		});
	}

	reloadTable() {
		this.searchInput = SearchCriteriaReceiptScheduling.constructorCopy(this.searchInput);
	}
}
