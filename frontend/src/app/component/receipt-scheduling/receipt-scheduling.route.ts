import { Route, RouterModule } from "@angular/router";
import { RschDashboardComponent } from "./dashboard/rsch-dashboard.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";

export const RECEIPTSCHEDULING_ROUTES: Route[] = [
	{
		path: "dashboard",
		component: RschDashboardComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const ReceiptSchedulingRoutes = RouterModule.forChild(RECEIPTSCHEDULING_ROUTES);
