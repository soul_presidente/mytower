import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "@app/shared/module/shared.module";

import { SavedFormsService } from "@app/services/saved-forms.service";
import { FormsModule } from "@angular/forms";
import { PricingMaskFillerComponent } from "./pricing-mask/pricing-mask-filler.component";
import { OldPricingMaskFillerComponent } from "./pricing-mask/old-pricing-mask-filler.component";

@NgModule({
	imports: [CommonModule, SharedModule, FormsModule],
	declarations: [PricingMaskFillerComponent, OldPricingMaskFillerComponent],
	providers: [SavedFormsService],
})
export class SavedFormsModule {}
