import { Component } from "@angular/core";
import { SavedSearchComponent } from "../saved-search/saved-search.component";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { Router } from "@angular/router";
import { SavedFormIdentifier } from "@app/utils/enumeration";
import { ModalService } from "@app/shared/modal/modal.service";

@Component({
	selector: "app-demandeprefere",
	templateUrl: "../saved-search/saved-search.component.html",
	styleUrls: ["../saved-search/saved-search.component.scss"],
	providers: [SavedFormsService],
})
export class DemandeprefereComponent extends SavedSearchComponent {
	constructor(
		protected savedFormService: SavedFormsService,
		protected router: Router,
		protected modalService: ModalService
	) {
		super(savedFormService, router, modalService);

		this.ebCompNum = SavedFormIdentifier.PRICING_MASK;
	}
}
