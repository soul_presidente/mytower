import { Component, OnInit, Input, SimpleChanges } from "@angular/core";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { SavedForm } from "@app/classes/savedForm";
import { NavigationExtras, Router } from "@angular/router";
import { Statique } from "../../../utils/statique";
import { SavedFormIdentifier } from "../../../utils/enumeration";
import { ConnectedUserComponent } from "../../../shared/connectedUserComponent";
import { ModalService } from "../../../shared/modal/modal.service";
import { Subscription } from "rxjs";

@Component({
	selector: "app-saved-search",
	templateUrl: "./saved-search.component.html",
	styleUrls: ["./saved-search.component.scss"],
})
export class SavedSearchComponent extends ConnectedUserComponent implements OnInit {
	protected ebCompNum: number;
	listSavedForm: any;
	selectedSavedForm: SavedForm;
	activeSf: number;
	subscriptions: Array<Subscription> = new Array<Subscription>();
	@Input()
	savedSearchClicked: boolean;
	constructor(
		protected savedFormsService: SavedFormsService,
		protected router: Router,
		protected modalService: ModalService
	) {
		super();

		this.ebCompNum = SavedFormIdentifier.SAVED_SEARCH;
	}

	ngAfterViewInit(): void {
		this.adjustHeight();
	}
	ngOnInit() {
		this.activeSf = 0;
		this.loadSavedForm({
			ebCompNum: this.ebCompNum,
			ebUserNum: this.userConnected ? this.userConnected.ebUserNum : null,
		});
		this.loadListSavedForms();
	}

	ngOnChanges(): void {
		this.loadSavedForm({
			ebCompNum: this.ebCompNum,
			ebUserNum: this.userConnected ? this.userConnected.ebUserNum : null,
		});
	}

	loadListSavedForms() {
		this.subscriptions.push(
			this.savedFormsService.SavedFormsObservale.subscribe((res: SavedForm) => {
				let list = [];
				this.listSavedForm.forEach((elem) => {
					if (elem.ebSavedFormNum != res.ebSavedFormNum) {
						list.push(elem);
					}
				});
				list.push(res);
				this.listSavedForm = list;
			})
		);
	}

	deleteSavedForm(sf: SavedForm, index: number) {
		let messageHead = "GENERAL.CONFIRM_DELETE_HEAD";
		let message = "GENERAL.CONFIRM_DELETE_MESSAGE";
		this.modalService.confirm(
			messageHead,
			message,
			function() {
				this.savedFormsService.deleteSavedForm(sf).subscribe();
				this.listSavedForm.splice(index, 1);
			}.bind(this),
			() => {}
		);
	}

	navigateToComponentTarget(sf: SavedForm) {
		let path = null;
		if (sf.url) path = sf.url;
		else path = "app/" + Statique.getComponentPathByCompIdAndModuleId(sf.ebCompNum, sf.ebModuleNum);

		let navigationExtras: NavigationExtras = { queryParams: { sf: sf.ebSavedFormNum } };
		localStorage.setItem(
			"sf" + sf.ebSavedFormNum,
			JSON.stringify({
				ebCompNum: sf.ebCompNum,
				ebModuleNum: sf.ebModuleNum,
				ebSavedFormNum: sf.ebSavedFormNum,
				ebUserNum: sf.ebUserNum,
				formName: sf.formName,
				formValues: sf.formValues,
				url: sf.url,
			})
		);

		if (this.ebCompNum == SavedFormIdentifier.SAVED_SEARCH)
			window.open(path + "?sf=" + sf.ebSavedFormNum, "_self");
		else this.router.navigate([path], navigationExtras);
	}

	sendSavedForm(sf: SavedForm) {
		// design active element in the saved-form list in order to highlight it
		this.activeSf = sf.ebSavedFormNum;

		if (sf.ebCompNum == SavedFormIdentifier.SAVED_SEARCH) {
			// send data to the target component
			let event = new CustomEvent("savedsearch", { detail: sf });
			document.dispatchEvent(event);
		} else {
			// send data to the target component
			this.savedFormsService.emitSavedForm(sf);
		}

		// navigate to target component/page
		this.navigateToComponentTarget(sf);
	}

	loadSavedForm(sf) {
		return this.savedFormsService.getSavedForms(sf).subscribe((res) => (this.listSavedForm = res));
	}

	getModuleIcon(ebModuleNum): string {
		let moduleName: string = Statique.getModuleNameByModuleNum(ebModuleNum);

		if (!moduleName) return null;

		return Statique.moduleIcon[moduleName];
	}
	isIconDefined(ebModuleNum): boolean {
		return Statique.isDefined(this.getModuleIcon(ebModuleNum));
	}

	adjustHeight() {
		let ulHeight = document.getElementById("sidebar-menu").offsetHeight;
		let leftSideHeight = document.getElementById("sidebar_left").offsetHeight;
		let savedSearchBloc = <HTMLElement>document.getElementsByClassName("savedsearch-list")[0];
		// let eltToGetHeight = document.getElementsByClassName("")[0];
		let heightToGet = Math.abs(ulHeight - leftSideHeight) - 122;
		savedSearchBloc.style.height = heightToGet.toString() + "px";
	}
}
