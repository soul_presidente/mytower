import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { SavedForm } from "@app/classes/savedForm";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { DialogService } from "@app/services/dialog.service";
import { Modules, SavedFormIdentifier } from "@app/utils/enumeration";
import { EbDemande } from "@app/classes/demande";
import { ModalService } from "@app/shared/modal/modal.service";
import { EbCategorie } from "@app/classes/categorie";
import { IField } from "@app/classes/customField";
import { Subscription } from "rxjs";

@Component({
	selector: "app-pricing-mask-filler",
	templateUrl: "./pricing-mask-filler.component.html",
})
export class OldPricingMaskFillerComponent extends ConnectedUserComponent
	implements OnInit, OnDestroy {
	protected ebModuleNum: number; // numéro du module
	protected ebCompNum: number; // numéro du component
	protected checkDirty: boolean;
	protected subscribedSelectedSf: Subscription;
	protected selectedSavedForm: SavedForm;

	ebDemande: EbDemande;
	listCategorie: Array<EbCategorie>;
	listFields: Array<IField>;
	@Input()
	showAddressWrapper: boolean;
	@Input()
	showAddressFields: boolean;
	@Input()
	showAddAddressBtn: boolean;
	constructor(
		protected savedFormsService: SavedFormsService,
		protected dialogService: DialogService,
		protected modalService: ModalService
	) {
		super();
		this.checkDirty = false;
		this.ebCompNum = SavedFormIdentifier.PRICING_MASK;
		this.ebModuleNum = Modules.PRICING;
	}

	ngOnInit() {}

	subscribeToSavedFormService() {
		// subscribtion to fill savedform
		this.subscribedSelectedSf = this.savedFormsService.selectedSavedForm$.subscribe(
			function(sf: SavedForm) {
				if (
					sf != null &&
					sf.ebModuleNum == this.ebModuleNum &&
					sf.ebCompNum == this.ebCompNum &&
					sf.ebUser != null &&
					sf.ebUserNum == this.userConnected.ebUserNum
				) {
					this.selectedSavedForm = sf;
					this.fillFormWithSf(sf);
					this.savedFormsService.emitSavedForm(new SavedForm());
				}
			}.bind(this)
		);
	}

	getDemande(): EbDemande {
		return this.ebDemande ? this.ebDemande : new EbDemande();
	}

	setCatgoriesAndCustomFields() {}

	async saveForm() {
		let sf = new SavedForm();

		sf.ebModuleNum = this.ebModuleNum;
		sf.ebCompNum = this.ebCompNum;
		sf.ebUser = this.userConnected;

		if (this.selectedSavedForm) {
			sf.ebSavedFormNum = this.selectedSavedForm.ebSavedFormNum;
			sf.formName = this.selectedSavedForm.formName;
			sf.dateAjout = this.selectedSavedForm.dateAjout;
		}

		try {
			if (sf.ebSavedFormNum)
				await this.modalService.confirmPromise(
					"Saved search",
					`Do you want to replace the old saved search ? (${sf.formName})`
				);
		} catch (e) {
			sf.formName = null;
			sf.ebSavedFormNum = null;
			sf.dateAjout = null;
		}

		try {
			let result = sf.formName;
			if (!sf.ebSavedFormNum) {
				let newName = await this.dialogService.input(null, "Name...", sf.formName);
				if (!newName) return;
				result = "" + newName;
			}

			sf.formName = "" + (result || "Unnamed");
			this.setCatgoriesAndCustomFields();
			sf.formValues = JSON.stringify(this.ebDemande);
			this.savedFormsService.saveSavedForm(sf).subscribe();
		} catch (e) {
			// on modal dismiss
		}
	}

	// init form with prefilled values if exists
	ngAfterViewInit() {
		if (this.selectedSavedForm && this.selectedSavedForm.ebSavedFormNum)
			this.fillFormWithSf(this.selectedSavedForm);
	}

	fillFormWithSf(sf: SavedForm) {
		if (sf && this.ebCompNum == sf.ebCompNum && this.ebModuleNum == sf.ebModuleNum) {
			let o,
				parsedValues: EbDemande = JSON.parse(sf.formValues),
				controls: any; // à voir

			/*
			for(o in parsedValues){
				if(controls[o] && (this.checkDirty && !controls[o].dirty || !this.checkDirty)){
					controls[o].setValue(parsedValues[o]);
				}
			}
			*/
			this.cloneEbDemandeWithSf(parsedValues);
			// if (parsedValues.ebPartyOrigin.city != null && parsedValues.ebPartyOrigin.city != null) {
			// this.showAddressWrapper = true;
			// this.showAddAddressBtn = false;
			// }

			if (parsedValues.sendEmails == undefined) {
				parsedValues.sendEmails = true;
			}
		}
	}

	cloneEbDemandeWithSf(parsedValues) {}

	ngOnDestroy() {
		if (this.subscribedSelectedSf) {
			this.subscribedSelectedSf.unsubscribe();
		}
	}

	search(data) {
		console.log(data);
	}
}
