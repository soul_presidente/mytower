import { Modules } from "@app/utils/enumeration";

export const HistoryPaths = [
	{
		ebModuleNum: Modules.PRICING,
		path: "/pricing/details/",
	},
	{
		ebModuleNum: Modules.TRANSPORT_MANAGEMENT,
		path: "/transport-management/details/",
	},
	{
		ebModuleNum: Modules.TRACK,
		path: "url not defined yet",
	},
	{
		ebModuleNum: Modules.QUALITY_MANAGEMENT,
		// path: "/quality-management/"
		path: "url not defined yet",
	},
	{
		ebModuleNum: Modules.FREIGHT_AUDIT,
		path: "url not defined yet",
	},
	{
		ebModuleNum: Modules.FREIGHT_ANALYTICS,
		path: "url not defined yet",
	},
];
