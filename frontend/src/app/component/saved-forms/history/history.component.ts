import { Component } from "@angular/core";
import { SavedSearchComponent } from "../saved-search/saved-search.component";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { NavigationEnd, Router } from "@angular/router";
import { SavedFormIdentifier } from "@app/utils/enumeration";
import { SavedForm } from "@app/classes/savedForm";
import { HistoryPaths } from "./paths.config";
import { ModalService } from "@app/shared/modal/modal.service";

@Component({
	selector: "app-history",
	templateUrl: "../saved-search/saved-search.component.html",
	styleUrls: ["../saved-search/saved-search.component.scss"],
	providers: [SavedFormsService],
})
export class HistoryComponent extends SavedSearchComponent {
	private historyPaths: Array<any>;

	constructor(
		protected savedFormService: SavedFormsService,
		protected router: Router,
		protected modalService: ModalService
	) {
		super(savedFormService, router, modalService);

		this.ebCompNum = SavedFormIdentifier.HISTORY;

		this.historyPaths = HistoryPaths;

		this.handleRouterChanges();
	}

	handleRouterChanges() {
		this.router.events.subscribe(
			function(val) {
				if (val instanceof NavigationEnd) {
					this.historyPaths.some((hp) => {
						if (val.url.indexOf(hp.path) >= 0) {
							// getting element id from url
							let name = null,
								index = 0;
							if (
								!(
									(index = val.url.lastIndexOf("/")) >= 0 &&
									(name = val.url.substring(index + 1)).length > 0
								)
							) {
								name = null;
							}
							this.savedFormService.saveHistory(this.userConnected, hp.ebModuleNum, val.url, name);
							return true;
						}
					});
				}
			}.bind(this)
		);
	}

	navigateToComponentTarget(sf: SavedForm) {
		let path: string = null;

		if (!this.router.isActive(sf.formValues, true))
			this.router.navigateByUrl(sf.formValues).then(() => {
				window.location.reload();
			});
	}
}
