import { NgModule } from "@angular/core";
import { PricingBookingListingComponent } from "./pricing-booking-listing/pricing-booking-listing.component";
import { NgSelectModule } from "@ng-select/ng-select";
import { PricingBookingSearchComponent } from "./pricing-booking-search/pricing-booking-search.component";
import { SharedModule } from "@app/shared/module/shared.module";
import { NgxUploaderModule } from "ngx-uploader";
import { DetailsComponent } from "./details/details.component";
import { CustomsInformationComponent } from "../transport-management/customs-information/customs-information.component";
import { Ng2CompleterModule } from "../../../../../node_modules/ng2-completer";
import { TransportInformationComponent } from "../transport-management/transport-information/transport-information.component";
import { DropdownModule } from "primeng/primeng";
import { PricingGroupingPropositionComponent } from "./pricing-grouping-proposition/pricing-grouping-proposition.component";
import { PropositionEditFormComponent } from "./pricing-grouping-proposition/proposition-edit-form/proposition-edit-form.component";
import { TreeModule } from "primeng/tree";
import { DemandeCreationComponent } from "../pricing/creation-wf/demande-creation.component";
import { DemandeCreationFormComponent } from "./creation-form/demande-creation-form.component";
import { StepExpeditionComponent } from "./creation-form/components/step-expedition/step-expedition.component";
import { StepPricingComponent } from "./creation-form/components/step-pricing/step-pricing.component";
import { StepReferenceComponent } from "./creation-form/components/step-reference/step-reference.component";
import { StepUnitComponent } from "./creation-form/components/step-unit/step-unit.component";
import { DemandeVisualisationComponent } from "./demande-visualisation/demande-visualisation.component";
import { DemandeVisualisationHeaderComponent } from "./demande-visualisation/header/demande-visualisation-header.component";
import { StepInfosAboutTransportComponent } from "./creation-form/components/step-infos-about-transport/step-infos-about-transport.component";
import { EntrepotWithVignetteComponent } from "./entrepot-with-vignette/entrepot-with-vignette.component";
import { PopupAssocierDeclarationDouaneComponent } from "./popup-associer-declaration-douane/popup-associer-declaration-douane.component";
import { StepDocumentsComponent } from "./creation-form/components/step-documents/step-documents.component";
import { GoodsRegimeTempCell } from "./goods-information/cells/cell-goods-regime-temp.component";
import { StepConsolidationComponent } from "./creation-form/components/step-consolidation/step-consolidation.component";
import { AdditionalCostComponent } from "./creation-form/components/step-pricing/additional-cost/additional-cost.component";
import { BnNgIdleService } from "bn-ng-idle";
import { PopupConfirmationFieldMandatoryComponent } from "@app/shared/modal-export-doc-by-template/popup-confirmation-field-mandatory/popup-confirmation-field-mandatory.component";

@NgModule({
	imports: [SharedModule, NgSelectModule, Ng2CompleterModule, DropdownModule, TreeModule],
	declarations: [
		PricingBookingListingComponent,
		PricingBookingSearchComponent,
		DetailsComponent,
		CustomsInformationComponent,
		TransportInformationComponent,
		DemandeCreationComponent,
		DemandeCreationFormComponent,
		StepExpeditionComponent,
		StepPricingComponent,
		StepReferenceComponent,
		StepUnitComponent,
		StepDocumentsComponent,
		DemandeVisualisationComponent,
		DemandeVisualisationHeaderComponent,
		PricingGroupingPropositionComponent,
		PropositionEditFormComponent,
		StepInfosAboutTransportComponent,
		EntrepotWithVignetteComponent,
		PopupAssocierDeclarationDouaneComponent,
		StepConsolidationComponent,
		GoodsRegimeTempCell,
		PopupConfirmationFieldMandatoryComponent,
		AdditionalCostComponent,
	],
	exports: [
		PricingBookingListingComponent,
		PricingBookingSearchComponent,
		DetailsComponent,
		NgSelectModule,
		NgxUploaderModule,
		CustomsInformationComponent,
		TransportInformationComponent,
		DemandeCreationComponent,
		DemandeCreationFormComponent,
		StepExpeditionComponent,
		StepPricingComponent,
		StepReferenceComponent,
		StepUnitComponent,
		StepDocumentsComponent,
		DemandeVisualisationComponent,
		DemandeVisualisationHeaderComponent,
		PricingGroupingPropositionComponent,
		PropositionEditFormComponent,
		EntrepotWithVignetteComponent,
		PopupAssocierDeclarationDouaneComponent,
		PopupConfirmationFieldMandatoryComponent,
		GoodsRegimeTempCell,
	],
	entryComponents: [GoodsRegimeTempCell],
	providers: [BnNgIdleService],
})
export class SharedComponentModule {}
