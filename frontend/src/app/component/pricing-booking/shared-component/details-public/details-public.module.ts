import { NgModule } from "@angular/core";
import { NgSelectModule } from "@ng-select/ng-select";
import { SharedModule } from "@app/shared/module/shared.module";
import { Ng2CompleterModule } from "../../../../../../node_modules/ng2-completer";
import { DetailsPublicRoutes } from "./details-public.route";
import { DetailsPublicComponent } from "./details-public.component";

@NgModule({
	imports: [DetailsPublicRoutes, SharedModule, NgSelectModule, Ng2CompleterModule],
	declarations: [DetailsPublicComponent],
	exports: [],
})
export class DetailsPublicModule {}
