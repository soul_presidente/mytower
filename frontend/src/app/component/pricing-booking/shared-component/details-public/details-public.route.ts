import { Route, RouterModule } from "@angular/router";
import { DetailsPublicComponent } from "./details-public.component";

export const DETAILS_PUBLIC_ROUTES: Route[] = [
	{
		path: "details/:ebDemandeNum",
		component: DetailsPublicComponent,
	},
];

export const DetailsPublicRoutes = RouterModule.forChild(DETAILS_PUBLIC_ROUTES);
