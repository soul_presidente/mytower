import {
	ChangeDetectorRef,
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnChanges,
	OnDestroy,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild,
} from "@angular/core";
import { Subscription } from "rxjs";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { Statique } from "@app/utils/statique";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { StatiqueService } from "@app/services/statique.service";
import {
	AccessRights,
	DemandeStatus,
	GenericTableScreen,
	ModeTransport,
	Modules,
	NatureDemandeTransport,
	ServiceType,
	TypeCustomsBroker,
	TypeDemande,
	UploadDirectory,
	UserRole,
} from "@app/utils/enumeration";
import { ActivatedRoute, Router } from "@angular/router";
import { ExportService } from "@app/services/export.service";
import { TranslateService } from "@ngx-translate/core";
import { EbFavori } from "@app/classes/favori";
import { FavoriService } from "@app/services/favori.service";
import { EbDemande } from "@app/classes/demande";
import { PricingService } from "@app/services/pricing.service";
import { UploadFile } from "ngx-uploader";
import { EtablissementService } from "@app/services/etablissement.service";
import EbCustomField, { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { ModalService } from "@app/shared/modal/modal.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { PricingBookingSearchComponent } from "../pricing-booking-search/pricing-booking-search.component";
import { EbTtPsl } from "@app/classes/Ttpsl";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import SingletonStatique from "@app/utils/SingletonStatique";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableCcpComponent } from "@app/shared/generic-table-ccp/generic-table-ccp.component";
import { EbTypeTransport } from "@app/classes/EbTypeTransport";
import { EbFlag } from "@app/classes/ebFlag";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { TypeRequestService } from "@app/services/type-request.service";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { EbTypeDocumentsDTO } from "@app/classes/dto/ebTypeDocumentsDTO";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { EbParty } from "@app/classes/party";
import { FlagColumnGenericCell } from "@app/shared/generic-cell/commons/flag-column.generic-cell";
import { EbQrGroupe } from "@app/classes/qrGroupe";

declare global {
	interface Window {
		functions: any;
	}
}

@Component({
	selector: "app-pricing-booking-listing",
	templateUrl: "./pricing-booking-listing.component.html",
	styleUrls: [
		"../../../../shared/modal/modal.component.scss",
		"./pricing-booking-listing.component.scss",
	],
})
export class PricingBookingListingComponent extends ConnectedUserComponent
	implements OnInit, OnDestroy, OnChanges {
	globalMsg: any;
	statique = Statique;
	Modules = Modules;
	ttPsl: Array<EbTtPsl> = new Array<EbTtPsl>();
	modeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();
	subscriptions: Array<Subscription> = new Array<Subscription>();
	@Input()
	pricingBookingSearchComponent: PricingBookingSearchComponent;
	@Input()
	title: string;
	@Output()
	listFieldsEmitter = new EventEmitter<Array<IField>>();
	@Output()
	listCategorieEmitter = new EventEmitter<Array<EbCategorie>>();
	listFields: Array<IField>;
	listCategorie: Array<EbCategorie> = new Array<EbCategorie>();
	globalSearchCriteria: SearchCriteriaPricingBooking = new SearchCriteriaPricingBooking();
	result: any = {
		nbrOfDays: 0,
		nbrOfHours: 0,
		nbrOfMin: 0,
	};

	// genericTable attributes
	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableCcpComponent;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	@Input()
	module: number;
	datatableComponent: number;
	private listTypeTransportMap: Map<number, Array<EbTypeTransport>>;
	@Input("searchInput")
	set searchInput(searchInput: SearchCriteriaPricingBooking) {
		if (searchInput != null) {
			let tmpSearchInput: SearchCriteriaPricingBooking = new SearchCriteriaPricingBooking();

			tmpSearchInput.module = this.module;
			tmpSearchInput.ebUserNum = this.userConnected.ebUserNum;
			tmpSearchInput.searchInput = JSON.stringify(searchInput || {});

			this.globalSearchCriteria = tmpSearchInput;
		}
	}

	listDestinations: any;

	count: number;
	diff: number;
	dataUpload: any;
	dataUploadResult: any;
	dataUploadFeedback: any;
	dataUploadProgress: any;
	initiateState: boolean = false;
	showQrGroupe: boolean = false;

	@Output()
	toasterDisplay = new EventEmitter<any>();

	ConstantsTranslate: Object = {
		STATUS: "",
		NOT_AWARDED: "",
		DELETE_PRICING_HEAD: "",
		DELETE_PRICING_TEXT: "",
		"NATURE_DEMANDE.NORMAL": "",
		"NATURE_DEMANDE.TRANSPORT_FILE": "",
		"NATURE_DEMANDE.MASS_IMPORT": "",
		"NATURE_DEMANDE.MASTER_OBJECT": "",
		"NATURE_DEMANDE.INITIAL": "",
		"NATURE_DEMANDE.TRANSPORT_PRINCIPAL": "",
		"NATURE_DEMANDE.POST_ACHEMINEMENT": "",
		"PRICING_BOOKING.CANCELLED_FOR_CONSOLIDATION": "",
		"NATURE_DEMANDE.EDI": "",
		"NATURE_DEMANDE.CONSOLIDATION": "",
		"PRICING_BOOKING.CONFIRMED_AND_COMPLETED": "",
	};

	statiquesLoaded: boolean = false;

	actionButtons: Array<HeaderInfos.ActionButton> = [
		{
			label: "PRICING_BOOKING.DUPLICATE",
			icon: "fa fa-files-o",
			btnClass: "btn-tertiary",
			action: (jsEvent) => {
				this.duplicateDemandesAction(jsEvent);
			},
			displayCondition: () => {
				return (
					!this.isPrestataire &&
					((this.module == Modules.PRICING &&
						this.userConnected.hasContribution(Modules.PRICING)) ||
						(this.module == Modules.TRANSPORT_MANAGEMENT &&
							this.userConnected.hasContribution(Modules.TRANSPORT_MANAGEMENT)))
				);
			},
			enableCondition: () => {
				return (
					this.genericTable != null &&
					this.genericTable.getSelectedRows() != null &&
					this.genericTable.getSelectedRows().length == 1
				);
			},
		},

		// enable button making booking sur TM a ce moment de MUP
		/* {
			label: "MODAL.PRISE_RDV",
			icon: "fa fa-calendar",
			action: this.updateAction.bind(this),
			displayCondition: () => {
				return (
					(this.module == Modules.PRICING &&
						!this.isPrestataire &&
						!this.isChargeur &&
						this.isControlTower &&
						this.userConnected.hasContribution(Modules.PRICING)) ||
					(this.module == Modules.TRANSPORT_MANAGEMENT &&
						this.userConnected.hasContribution(Modules.TRANSPORT_MANAGEMENT) &&
						!this.isBroker)
				);
			},
			enableCondition: () => {
				return (
					this.genericTable != null &&
					this.genericTable.getSelectedRows() != null &&
					this.genericTable.getSelectedRows().length > 1
				);
			},
		}, */
		{
			label: "Delete",
			icon: "fa fa-trash",
			btnClass: "btn-tertiary",
			action: this.deleteAction.bind(this),
			displayCondition: () => {
				return (
					(this.isControlTowerAndSuperAdmin &&
						this.module == Modules.TRANSPORT_MANAGEMENT &&
						this.userConnected.hasContribution(Modules.TRANSPORT_MANAGEMENT) &&
						!this.isBroker &&
						!(this.userConnected.ebCompagnie.ebCompagnieNum != -1 && this.isControlTowerAndSuperAdmin)) ||
					(this.isControlTowerAndSuperAdmin &&
						this.module == Modules.PRICING &&
						this.userConnected.hasContribution(Modules.PRICING) &&
						!this.isBroker &&
						!(this.userConnected.ebCompagnie.ebCompagnieNum != -1 && this.isControlTowerAndSuperAdmin))
				);
			},
			enableCondition: () => {
				return (
					this.genericTable != null &&
					this.genericTable.getSelectedRows() != null &&
					this.genericTable.getSelectedRows().length > 0
				);
			},
		},
	];

	constructor(
		private statiqueService: StatiqueService,
		private elRef: ElementRef,
		protected router: Router,
		protected activatedRoute: ActivatedRoute,
		protected headerService: HeaderService,
		protected exportService?: ExportService,
		private translate?: TranslateService,
		private favoriService?: FavoriService,
		protected etablissementService?: EtablissementService,
		private pricingService?: PricingService,
		private modalService?: ModalService,
		private cdr?: ChangeDetectorRef,
		private typeRequestService?: TypeRequestService
	) {
		super();
	}

	deleteGoodsPickup(row: EbDemande) {
		let that = this;
		let message = that.ConstantsTranslate["DELETE_PRICING_TEXT"];
		let tabNum = [];
		tabNum.push(row.ebDemandeNum);
		let data = Statique.copyObject(row, true);
		message += data["refTransport"] + ". Do you want to continue ?";

		that.modalService.confirm(
			that.ConstantsTranslate["DELETE_PRICING_HEAD"],
			message,
			function() {
				that.pricingService.deleteEbDemande(tabNum).subscribe((data) => {
					if (data == 1) {
						that.genericTable.refreshData();
						// that.modalService.information(
						//   "Information",
						//   `File ${row.refTransport} deleted successfully.`
						// );
						that.toasterDisplay.emit(true);
					}
				});
			},
			function() {}
		);
	}

	duplicateDemandesAction(jsEvent) {
		let data = this.genericTable.getSelectedRows();

		if (data && data[0] && data[0].ebDemandeNum) {
			let requestProcessing = new RequestProcessing();
			requestProcessing.beforeSendRequest(jsEvent);

			let searchCriteria = new SearchCriteriaPricingBooking();
			searchCriteria.ebDemandeNum = data[0].ebDemandeNum;
			searchCriteria.module = this.module;
			searchCriteria.withListMarchandise = true;
			searchCriteria.ebUserNum = this.userConnected.ebUserNum;

			this.pricingService.getEbDemande(searchCriteria).subscribe((res: EbDemande) => {
				requestProcessing.afterGetResponse(jsEvent);
				if (this.userConnected.role == UserRole.CHARGEUR) {
					res.user = this.userConnected;
					this.isChargeur = true;
				}

				this.router.navigateByUrl("/app/pricing/creation", { state: res });
			});
		}
	}
	deleteAction() {
		let that = this;
		let message = that.ConstantsTranslate["DELETE_PRICING_TEXT"];
		let data = this.genericTable.getSelectedRows();
		for (let i = 0; i < data.length; i++) {
			if (i == 0) {
				message += data[i].refTransport;
			} else {
				message += ", " + data[i].refTransport;
			}
		}
		message += ". Do you want to continue ?";
		if (data.length > 0) {
			that.modalService.confirm(
				that.ConstantsTranslate["DELETE_PRICING_HEAD"],
				message,
				function() {
					that.pricingService
						.deleteEbDemande(data.map((it) => it.ebDemandeNum))
						.subscribe((data) => {
							if (data == 1) {
								that.genericTable.refreshData();
								that.toasterDisplay.emit(true);
							}
						});
				},
				function() {}
			);
		}
	}

	updateGoodsPickup(row: EbDemande) {
		let tabData: any[] = [],
			data = Statique.copyObject(row, true);

		data["userConnected"] = {
			ebUserNum: this.userConnected.ebUserNum,
			email: this.userConnected.email,
			role: this.userConnected.role,
			service: this.userConnected.service,
			nom: this.userConnected.nom + " " + this.userConnected.prenom,
		};

		if (data["xEbSchemaPsl"] != null) {
			for (let psl of data["xEbSchemaPsl"]["listPsl"]) {
				if (psl.startPsl) {
					data["startPsl"] = {
						startPslLibelle: psl.libelle,
					};
				}
				if (psl.endPsl) {
					data["endPsl"] = {
						endPslLibelle: psl.libelle,
					};
				}
			}
		}

		if (data) {
			tabData.push(data);
			this.modalService.goodsPickupUpdater(
				data,
				tabData,
				function(result: Object, event, modalObserver: any) {
					let requestProcessing = new RequestProcessing();
					requestProcessing.beforeSendRequest(event);

					if (!result) return;

					let mapResults: any[] = [];
					let mapResult = {
						ebDemandeNum: data["ebDemandeNum"],
						listPropRdvPk: data["listPropRdvPk"],
						listPropRdvDl: data["listPropRdvDl"],

						// datePickupTM: data["datePickupTM"],
						// dateOfGoodsAvailability: data["dateOfGoodsAvailability"],
						// finalDelivery: data["finalDelivery"],
						// confirmPickup: data["confirmPickup"],
						// confirmDelivery: data["confirmDelivery"]
					};
					mapResults.push(mapResult);

					// this.pricingService.updateGoodsPickup(mapResults).subscribe(
					this.pricingService.updateEbDemandePropRdv(mapResults).subscribe(
						function() {
							this.genericTable.setDataRow(data["ebDemandeNum"], data);

							modalObserver.subject.next();
							// this.modalService.information("Information", "Data saved");
							this.toasterDisplay.emit(true);
							requestProcessing.afterGetResponse(event);
							this.genericTable.refreshData();
						}.bind(this)
					);
				}.bind(this)
			);
		}
	}

	showQrGroupePropositions() {
		this.router.navigate(["app/pricing/proposition-groupage"]);
	}

	updateAction() {
		let tabData = this.genericTable.getSelectedRows(),
			tabMapResult: any[] = [],
			$this = this;

		if (tabData.length > 0) {
			let data: any = {};
			data.userConnected = {
				ebUserNum: this.userConnected.ebUserNum,
				email: this.userConnected.email,
				role: this.userConnected.role,
				service: this.userConnected.service,
			};
			this.modalService.goodsPickupUpdater(
				data,
				tabData,
				function(result: Object, event, modalObserver: any) {
					let requestProcessing = new RequestProcessing();
					requestProcessing.beforeSendRequest(event);

					if (!result) return;

					let mapResults: any[] = [];

					tabData.forEach((object) => {
						let mapResult = {
							ebDemandeNum: object.ebDemandeNum,
							datePickupTM: result["datePickupTM"],
							dateOfGoodsAvailability: result["dateOfGoodsAvailability"],
							finalDelivery: result["finalDelivery"],
							confirmPickup: result["confirmPickup"],
							confirmDelivery: result["confirmDelivery"],
						};
						mapResults.push(mapResult);
					});

					this.pricingService.updateGoodsPickup(mapResults).subscribe(
						function() {
							tabData.forEach((object) => {
								object.datePickupTM = data.datePickupTM;
								object.finalDelivery = data.finalDelivery;
								object.dateOfGoodsAvailability = data.dateOfGoodsAvailability;
								object.confirmPickup = data.confirmPickup;
								object.confirmDelivery = data.confirmDelivery;

								$this.genericTable.setDataRow(object.ebDemandeNum, object);
							});

							modalObserver.subject.next();
							//this.modalService.information("Information", "Data saved");
							this.toasterDisplay.emit(true);
							requestProcessing.afterGetResponse(event);
						}.bind(this)
					);
				}.bind(this)
			);
		}
	}
	ngAfterViewInit() {
		this.cdr.detectChanges();
	}
	async getStatiques() {
		try {
			this.ttPsl = await SingletonStatique.getListTtPsl();
			this.modeTransport = await SingletonStatique.getListModeTransport();
			this.listDestinations = await SingletonStatique.getListEcCountry();
			this.listTypeTransportMap = await SingletonStatique.getListTypeTransportMap();
		} catch (e) {}

		this.statiquesLoaded = true;
	}
	ngOnInit() {
		this.datatableComponent = this.getDatatableCompId();

		this.headerService.registerActionButtons(this.actionButtons);
		this.getStatiques();

		this.dataUpload = {
			ebUserNum: this.userConnected.ebUserNum,
			typeFile: String(UploadDirectory.BULK_IMPORT),
		};

		Statique.StatutDemande.forEach((it) => (this.ConstantsTranslate[it.value] = ""));
		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate
				.get((key.indexOf(".") < 0 ? "PRICING_BOOKING." : "") + key)
				.subscribe((res) => (this.ConstantsTranslate[key] = res));
		});
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["module"] || (changes["searchInput"] && changes["searchInput"].firstChange))
			this.initDatatable();
	}

	initDatatable() {
		this.activatedRoute.queryParams.subscribe((params) => {
			let numeroStatutDemande = +params["statutdemande"] || null;
			if (numeroStatutDemande != null)
				this.globalSearchCriteria.listStatut.push(numeroStatutDemande);

			this.dataInfos.cols = this.getColumns();
			this.dataInfos.selectedCols = this.dataInfos.cols;
		});

		this.globalSearchCriteria.pageNumber = 1;
		this.globalSearchCriteria.module = this.module;
		this.globalSearchCriteria.connectedUserNum = this.userConnected.ebUserNum;
		this.globalSearchCriteria.ebUserNum = this.userConnected.ebUserNum;

		this.dataInfos.dataKey = "ebDemandeNum";
		this.dataInfos.showSubTable = false;
		this.dataInfos.showAdvancedSearchBtn = true;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = this.statique.controllerPricing + "/list";
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.extractWithAdvancedSearch = true;
		this.dataInfos.favorisName = (row) => row.refTransport;
		this.dataInfos.showDetails = true;
		this.dataInfos.onFlagChange = this.onFlagChange;
		this.dataInfos.contextMenu = true;

		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showUploadBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.blankBtnCount = 0;
		this.dataInfos.cardView = true;

		this.dataInfos.ccpTooltipRender = (row: EbDemande): string => {
			if (row == null || row.user == null || row.user.ebCompagnie == null) return "";
			return row.user.ebCompagnie.nom;
		};

		if (this.userConnected.role == UserRole.CONTROL_TOWER) {
			this.dataInfos.dataUpload = {
				ebUserNum: this.userConnected.ebUserNum,
				typeFile: String(UploadDirectory.BULK_IMPORT),
			};
		}

		this.dataInfos.dataIterators = [
			{
				timer: 1000,
				handler: function(data, intervalConfig) {
					data.forEach((rowData) => {
						if (rowData) {
							if (rowData.torType == TypeDemande.STANDARD) {
								//TIME_REMAINING
								const getTimeAgoByHours: number = this.typeRequestService.getTimeAgo(
									rowData.dateCreation,
									true
								);
								if (getTimeAgoByHours >= rowData.torDuration) {
									this.result = {
										nbrOfDays: 0,
										nbrOfHours: 0,
										nbrOfMin: 0,
									};
								} else {
									this.result = this.typeRequestService.getTimeRemaining(
										rowData.torDuration - getTimeAgoByHours
									);
								}
							} else {
								//TYPE_SINCE_ISSUANCE
								this.result = this.typeRequestService.getTimeAgo(rowData.dateCreation);
							}
							const day = this.translate
								.instant("PRICING_BOOKING.DETAILS_TIME_REMAINING.DAY")
								.toLocaleLowerCase();
							let res =
								this.result.nbrOfDays +
								day +
								" " +
								this.result.nbrOfHours +
								"h " +
								this.result.nbrOfMin +
								"m ";
							rowData.strCurentTimeRemaining = res;
						}
					});
				}.bind(this),
			},
		];

		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(this.module)];
		this.dataInfos.actionDetailsLink = "/app/" + moduleRoute + "/details/";
		this.dataInfos.insertDetailsContextItem();
		this.dataInfos.insertDetailsNewTabContextItem();

		if (this.module == Modules.TRANSPORT_MANAGEMENT || this.module == Modules.PRICING) {
			this.dataInfos.lineActions = [];

			let configRigth = false,
				accessRigthPricing = false,
				accessRigthTransportManagement = false,
				access = this.userConnected.listAccessRights;

			access.forEach((acces) => {
				if (
					acces.ecModuleNum == Modules.PRICING &&
					acces.accessRight == AccessRights.CONTRIBUTION
				) {
					accessRigthPricing = true;
					configRigth = true;
				} else if (
					acces.ecModuleNum == Modules.TRANSPORT_MANAGEMENT &&
					acces.accessRight == AccessRights.CONTRIBUTION
				) {
					accessRigthTransportManagement = true;
					configRigth = true;
				}
			});
			if (this.module == Modules.TRANSPORT_MANAGEMENT) this.dataInfos.actionColumFixed = false;
			if (this.module == Modules.PRICING) this.dataInfos.actionColumFixed = false;
			let $this = this;
			if (configRigth) {
				if (accessRigthPricing) {
					this.dataInfos.showFlags = true;
					this.dataInfos.dataFlagsField = "listEbFlagDTO";
					if (
						this.userConnected.role == ServiceType.CONTROL_TOWER &&
						this.userConnected.superAdmin && 
						!(this.userConnected.ebCompagnie.ebCompagnieNum != -1 && this.isControlTowerAndSuperAdmin)
					) {
						this.dataInfos.showCheckbox = true;

						this.dataInfos.showCheckboxWidth = "130px";
						this.dataInfos.lineActions.push({
							class: "btn-danger",
							icon: "fa fa-trash",
							title: "Delete",
							onClick: function(row: EbDemande) {
								this.deleteGoodsPickup(row);
							}.bind($this),
						});
					} else if (!this.userConnected.rolePrestataire) {
						this.dataInfos.showCheckbox = true;
						this.dataInfos.showCheckboxWidth = "130px";
					} else {
						this.dataInfos.showCheckbox = false;
						this.dataInfos.showSubTable = false;
						this.dataInfos.lineActions = [];
					}
				}
				if (
					this.acls[this.ACL.Rule.Demande_Dashboard_Scheduling_Enable] &&
					accessRigthTransportManagement &&
					Modules.TRANSPORT_MANAGEMENT == this.module
				) {
					this.dataInfos.showFlags = true;
					this.dataInfos.dataFlagsField = "listEbFlagDTO";
					this.dataInfos.showCheckboxWidth = "150px";
					this.dataInfos.lineActions.push({
						class: "btn-primary",
						icon: "fa fa-calendar",
						title: "Prise de rendez-vous",
						onClick: function(row: EbDemande) {
							this.updateGoodsPickup(row);
							//console.log(row.xEbSchemaPsl.listPsl);
							// this.updatePropRdv(row);
						}.bind($this),
					});
				}
			}
		}
	}

	renderDocStatut(classname: string, name: string, number: number): string {
		return `<span class='doc-status ${classname}'>${name}(${number == null ? 0 : number});</span>`;
	}

	getColumns(): any {
		const $this = this;
		let cols: any = [
			{
				field: "listEbFlagDTO",
				translateCode: "PRICING_BOOKING.FLAG",
				minWidth: "125px",
				allowClick: false,
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: FlagColumnGenericCell,
				genericCellParams: new FlagColumnGenericCell.Params({
					field: "listEbFlagDTO",
					contributionAccess: true,
					moduleNum : $this.module,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{
				field: "customerReference",
				translateCode: "TRACK_TRACE.CUSTOMER_REFERENCE",
				width: "150px",
				isCardCol: true,
				cardOrder: 2,
				isCardLabel: false,
			},
			{
				translateCode: "PRICING_BOOKING.STATUT_DOCUMENT",
				field: "listTypeDocuments",
				width: "160px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, row, d) {
					let libelle = "";
					let titleLibelle = "";
					let libelleWrapper = "";
					if (data != undefined && data != null && data.length > 0) {
						data.forEach((element: EbTypeDocuments) => {
							let loopLibelle = "";
							let classname = "";
							if (element.isexpectedDoc) {
								if (element.nbrDoc == 0 || element.nbrDoc == null) {
									classname = "red";
								} else {
									classname = "green";
								}

								loopLibelle = $this.renderDocStatut(classname, element.code, element.nbrDoc);
							} else {
								if (element.nbrDoc > 0) {
									classname = "black";
									loopLibelle = $this.renderDocStatut(classname, element.code, element.nbrDoc);
								}
							}
							titleLibelle += `${element.code}(${element.nbrDoc == null ? 0 : element.nbrDoc}); `;
							libelle += loopLibelle;
						});
						titleLibelle = titleLibelle.substring(0, titleLibelle.length - 2);
						libelleWrapper =
							`<div class='docs__wrapper' title='` + titleLibelle + `'>` + libelle + `</div>`;
					}
					return libelleWrapper;
				}.bind(this),
			},
			{ field: "unitsReference", translateCode: "TRACK_TRACE.UNIT_REFERENCE" },
			{ field: "unitsPackingList", translateCode: "TYPE_UNIT.PACKING_LIST" },
			{ field: "xEcIncotermLibelle", translateCode: "PRICING_BOOKING.INCOTERMS", width: "100px" },
			{
				field: "consolidationRuleLibelle",
				translateCode: "SETTINGS.CONSOLIDATION",
				width: "100px",
			},
			{ 
				field: "numAwbBol", 
				translateCode: "PRICING_BOOKING.NUM_AWB_BOL", 
				width: "110px",
				render: function(data, type, row) {
					if (row.statutStr == Statique.getStatusLibelle(DemandeStatus.NOT_AWARDED)) return;
					return data;
				}.bind(this)
			},
			{
				field: "carrierUniqRefNum",
				translateCode: "REQUEST_OVERVIEW.CARRIER_UNIQ_REF_NUM",
				width: "130px",
				render: function(data, type, row) {
					if (row.statutStr == Statique.getStatusLibelle(DemandeStatus.NOT_AWARDED)) return;
					return data;
				}.bind(this)
			},
			{ field: "libelleOriginCountry", translateCode: "PRICING_BOOKING.ORIGIN" },
			{
				field: "libelleOriginCity",
				translateCode: "PRICING_BOOKING.ORIG_CITY",
				isCardCol: true,
				cardOrder: 3,
				isCardLabel: false,
			},
			{ field: "libelleDestCountry", translateCode: "PRICING_BOOKING.DESTINATION" },
			{
				field: "libelleDestCity",
				translateCode: "PRICING_BOOKING.DEST_CITY",
				isCardCol: true,
				cardOrder: 4,
				isCardLabel: false,
			},
			{
				translateCode: "PRICING_BOOKING.NATURE",
				field: "xEcNature",
				render: function(data) {
					return this.ConstantsTranslate["NATURE_DEMANDE." + NatureDemandeTransport[data]];
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.MASTER_OBJECT",
				field: "ebDemandeMasterObject",
				render: function(data) {
					return data != null ? data.refTransport : "";
				}.bind(this),
			},

			{
				translateCode: "PRICING_BOOKING.MODE_OF_TRANSPORT",
				field: "xEcModeTransport",
				isCardCol: true,
				cardOrder: 5,
				width: "140px",
				render: function(modeTransportCode, type, row) {
					if (!modeTransportCode) return "";
					return SingletonStatique.getModeTransportString(
						modeTransportCode,
						row,
						$this.listTypeTransportMap
					);
				}.bind(this),
			},
			{
				//TODO : what's cost center ?
				translateCode: "PRICING_BOOKING.COST_CENTER",
				field: "xEbCostCenter",
				width: "105px",
				render: function(data) {
					return data ? data.libelle : "";
				},
			},
			{
				translateCode: "PRICING_BOOKING.REQUEST_DATE",
				field: "dateCreation",
				width: "120px",
				render: function(data) {
					return this.statique.formatDateTimeStr(data);
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.DATE_UPDATE",
				field: "updateDate",
				width: "120px",
				render: function(data) {
					return this.statique.formatDateTimeStr(data);
				}.bind(this),
			},
			// {
			//   translateCode: "PRICING_BOOKING.TYPE_OF_REQUEST",
			//   field: "xEcTypeDemande",
			//   width: "80px",
			//   render: function(data) {
			//     let typedemande = this.statique.typeDemande.map(function(value) {
			//       if (value.key == data) return value.value;
			//     });

			//     return typedemande.join("");
			//   }.bind(this)
			// },
			{
				translateCode: "PRICING_BOOKING.SERVICE_LEVEL",
				field: "typeRequestLibelle",
				width: "80px",
				render: (data) => {
					// let typeDemande = "";
					// if (!!data && !!this.listTypeDemandeByCompagnie) {
					//   this.listTypeDemandeByCompagnie.forEach(elem => {
					//     if (elem.ebTypeRequestNum === data)
					//       typeDemande = elem.reference + (elem.libelle ? " - " + elem.libelle : "");
					//   });
					// }
					// return typeDemande;
					return data;
				},
			},
			{
				translateCode: "PRICING_BOOKING.REQUESTOR",
				field: "user",
				width: "180px",
				render: function(data, type, row) {
					return row.user.nomPrenom;
				}.bind(this),
			},
			/* {
				translateCode: "PRICING_BOOKING.TYPE_OF_REQUEST",
				field: "xEcTypeDemande",
				render: function(data) {
					let typedemande = this.statique.typeDemande.map(function(value) {
						if (value.key == data) return value.value;
					});

					return typedemande.join("");
				}.bind(this),
			}, */
			{
				translateCode: "PRICING_BOOKING.STATUS",
				field: "statutStr",
				isCardCol: true,
				cardOrder: 6,
				isCardLabel: true,
				render: function(data: string) {
					if (this.ConstantsTranslate[data]) {
						return this.ConstantsTranslate[data];
					} else return data;
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.TRANSPORT_CONFIRMATION",
				field: "transportConfirmation",
				render: function(data, display, row) {
					let transportConfirmation = "",
						translateCode;
					if (row.askForTransportResponsibility && !row.provideTransport) {
						translateCode = "PRICING_BOOKING.WAITING_FOR_CONFIRMATION";
					} else if (row.askForTransportResponsibility && row.provideTransport) {
						translateCode = "PRICING_BOOKING.CONFIRMED";
					}

					if (row.statutStr == Statique.getStatusLibelle(DemandeStatus.NOT_AWARDED)) {
						translateCode = null;
					}

					if (translateCode) {
						this.translate.get(translateCode).subscribe((res) => (transportConfirmation = res));
					}
					return transportConfirmation;
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.ACKNOWLEDGE_DATE_BY_CT",
				field: "tdcAcknowledgeDate",
				render: function(data) {
					return data ? this.statique.formatDate(data) : "";
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.TOTAL_VOLUME",
				field: "totalVolume",
			},
			{
				translateCode: "PRICING_BOOKING.TOTAL_WEIGHT",
				field: "totalWeight",
			},
			{
				translateCode: "PRICING_BOOKING.SCHEMA_PSL",
				field: "xEbSchemaPsl",
				render: (data) => {
					if(data)
						return data.designation;
					return "";
				},
			},
			{
				translateCode: "PRICING_BOOKING.VILLEINCOTERM",
				field: "city",
			},
			{
				translateCode: "PRICING_BOOKING.NUMBER_OF_UNITS",
				field: "totalNbrParcel",
			},
			{
				translateCode: "PRICING_BOOKING.WAITING_FOR_QUOTE_DATE",
				field: "waitingForQuote",
				render: (data) => {
					return this.statique.formatDateTimeStr(data);
				},
			},
			{
				translateCode: "PRICING_BOOKING.WAITING_FOR_CONFIRMATION_DATE",
				field: "waitingForConf",
				render: (data) => {
					return this.statique.formatDateTimeStr(data);
				},
			},
			{
				translateCode: "PRICING_BOOKING.CONFIRMATION_DATE",
				field: "confirmed",
				render: (data) => {
					return this.statique.formatDateTimeStr(data);
				},
			},
			{
				translateCode: "PRICING_BOOKING.VALUATION_TYPE",
				field: "valuationType",
				render: (status) => {
					let str = Statique.CarrierStatus.find((data) => data.key == status);
					return str ? str.key == 4 ? "Quote" : "Transport Plan" : "";
				},
			},
		];

		if (this.module == Modules.PRICING) {
			let colsPrincing: Array<GenericTableInfos.Col> = [
				{
					field: "refTransport",
					translateCode: "PRICING_BOOKING.PRICING_REFERENCE",
					width: "135px",
					isCardCol: true,
					isCardTitle: true,
					cardOrder: 1,
				},
				{
					translateCode: "PRICING_BOOKING.TIME_REMAINING",
					field: "strCurentTimeRemaining",
					render: (data, type, row) => {
						let timeLeftDefault = "";
						this.translate
							.get("PRICING_BOOKING.DETAILS_TIME_REMAINING.DEFAULT")
							.subscribe((value) => (timeLeftDefault = value));
						if (data) return data;
						else return timeLeftDefault;
					},
				},
				{
					field: "eligibleForConsolidation",
					translateCode: "PRICING_BOOKING.OPTIONS.ELIGIBLE_FOR_CONSOLIDATION",
					width: "135px",
					renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
					render: (data) => {
						let tr = data ? "GENERAL.YES" : "GENERAL.NO";
						this.translate.get(tr).subscribe((res) => (tr = res));
						return tr;
					},
				},

				{
					field: "insurance",
					translateCode: "PRICING_BOOKING.INSURANCE",
					width: "135px",
					renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
					render: (data) => {
						return this.translate.instant(data ? "GENERAL.YES" : "GENERAL.NO");
					},
				},
				{
					field: "xecCurrencyInvoice",
					translateCode: "PRICING_BOOKING.INVOICE_CURRENCY",
					render: (data) => {
						return data.codeLibelle;
					},
				},
				{
					translateCode: "PRICING_BOOKING.NAME_REQUEST",
					field: "xEbUserCt",
					render: function(data, type, row) {
						return row.xEbUserCt.nomPrenom;
					}.bind(this),
				},
			];

			cols = cols.concat(colsPrincing);
		}

		if (this.module == Modules.TRANSPORT_MANAGEMENT) {
			cols = cols.concat([
				{
					translateCode: "PRICING_BOOKING.QUOTATION_REFERENCE",
					field: "refTransport",
					width: "100px",
					render: function(data) {
						return data;
					}.bind(this),
				},
				{
					translateCode: "PRICING_BOOKING.PRICE",
					field: "prixTransporteurFinal",
					width: "70px",
					render: function(data, display, row) {
						return row.prixTransporteurFinal;
					}.bind(this),
				},
				{
					translateCode: "PRICING_BOOKING.ETABLISSEMENT_TRANSPORTEUR",
					field: "etablissementTransporteur",
					width: "115px",
					render: function(data, display, row) {
						let obj =
							row.exEbDemandeTransporteurs &&
							row.exEbDemandeTransporteurs[0] &&
							row.exEbDemandeTransporteurs[0]
								? row.exEbDemandeTransporteurs[0].xEbEtablissement
								: null;
						return obj && obj.nom ? obj.nom : null;
					}.bind(this),
				},
				{
					translateCode: "PRICING_BOOKING.COMPANY_TRANSPORTEUR",
					field: "companyTransporteur",
					render: function(data, display, row) {
						let obj =
							row.exEbDemandeTransporteurs &&
							row.exEbDemandeTransporteurs[0] &&
							row.exEbDemandeTransporteurs[0]
								? row.exEbDemandeTransporteurs[0].xEbCompagnie
								: null;
						return obj && obj.nom ? obj.nom : null;
					}.bind(this),
				},
				{
					translateCode: "PRICING_BOOKING.NAME_REQUEST",
					field: "xEbUserCt",
					render: function(data, type, row) {
						return row.xEbUserCt.nomPrenom;
					}.bind(this),
				},
				// {
				//   translateCode: "PRICING_BOOKING.CARRIERS",
				//   field: "exEbDemandeTransporteurFinal",
				//   render: function(data) {
				//     let transporteurs = "";
				//     if (data != null) return data.nomPrenom;
				//     else return transporteurs;
				//   }.bind(this)
				// },
				{
					translateCode: "PRICING_BOOKING.EMAIL_TRANSPORTEUR",
					field: "emailTransporteur",
					width: "210px",
					styleOverflow: "",
					render: function(data, display, row) {
						return row.exEbDemandeTransporteurs &&
							row.exEbDemandeTransporteurs[0] &&
							row.exEbDemandeTransporteurs[0]
							? row.exEbDemandeTransporteurs[0].xTransporteur.email
							: null;
					}.bind(this),
				},

				//TODO MEP-ce bout de code devra être réactivé après MEP
				/*
				{
					translateCode: "PRICING_BOOKING.DATE_PICKUP_NEGOTIATED",
					field: "datePickup",
					width: "100px",
					render: function(data, display, row) {
						return row.exEbDemandeTransporteurs &&
							row.exEbDemandeTransporteurs[0] &&
							row.exEbDemandeTransporteurs[0]
							? Statique.formatDate(row.exEbDemandeTransporteurs[0].pickupTime)
							: null;
					}.bind(this),
				},
				{
					translateCode: "PRICING_BOOKING.DATE_DELIVERY_NEGOTIATED",
					field: "dateDelivery",
					width: "110px",
					render: function(data, display, row) {
						return row.exEbDemandeTransporteurs &&
							row.exEbDemandeTransporteurs[0] &&
							row.exEbDemandeTransporteurs[0]
							? Statique.formatDate(row.exEbDemandeTransporteurs[0].deliveryTime)
							: null;
					}.bind(this),
				},
				*/
				{
					translateCode: "TRACK_TRACE.LIBELLE_LAST_PSL",
					field: "libelleLastPsl",
					width: "100px",
					render: function(data, display, row) {
						return data;
					}.bind(this),
				},
				{
					translateCode: "TRACK_TRACE.DATE_LAST_PSL",
					field: "dateLastPsl",
					width: "145px",
					render: function(data, display, row) {
						return Statique.formatDate(data);
					}.bind(this),
				},

				{
					translateCode: "PRICING_BOOKING.TRANSPORT_INFOS_STATUS",
					field: "flagTransportInformation",
					width: "100px",
					//renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
					render: function(data, display, row) {
						return (
							// `
							//           <ul class="document-statut">
							//               <li>` +
							// '<span class="padding-right-15-important">' +
							data ? "OK" : "MISSING" //+
							// "</span>" +
							// ` <li>
							//           </ul>
							//       `
						);
					}.bind(this),
				},
				/* {
					translateCode: "PRICING_BOOKING.DOC_CUSTOMS_STATUS",
					field: "flagDocumentCustoms",
					width: "90px",
					//renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
					render: function(data, display, row) {
						return (
							// `
							//               <ul class="document-statut">
							//                   <li>` +
							row.xEcTypeCustomBroker != TypeCustomsBroker.NOT_APPLICABLE
								? //'<span class="padding-right-15-important">' +
								  data
									? "OK"
									: "MISSING" //+
								: // "</span>"
								  "" //+
							// ` </li>
							//               </ul>
							//           `
						);
					}.bind(this),
				}, */

				{
					translateCode: "PRICING_BOOKING.PSL_START",
					field: "datePickupTM",
					width: "105px",
					render: function(data, display, row) {
						return data ? Statique.formatDateTimeStr(data) : "";
					}.bind(this),
				},
				{
					translateCode: "PRICING_BOOKING.DATE_OF_GOODS_AVAILABILITY",
					field: "dateOfGoodsAvailability",
					width: "97px",
					render: function(data, display, row) {
						return data ? Statique.formatDateTimeStr(data) : "";
					}.bind(this),
				},
				{
					translateCode: "PRICING_BOOKING.PSL_END",
					field: "finalDelivery",
					width: "105px",
					render: function(data, display, row) {
						return data ? Statique.formatDateTimeStr(data) : "";
					}.bind(this),
				},
				{
					translateCode: "PRICING_BOOKING.PICKUP_CONFIRMATION",
					field: "confirmPickup",
					width: "106px",
					render: function(data, display, row) {
						if (!Statique.isDefined(data)) return "";
						return data ? "OK" : "KO";
					}.bind(this),
				},
				{
					translateCode: "PRICING_BOOKING.DELIVERY_CONFIRMATION",
					field: "confirmDelivery",
					render: function(data, display, row) {
						if (!Statique.isDefined(data)) return "";
						return data ? "OK" : "KO";
					}.bind(this),
				},
				{
					translateCode: "PRICING_BOOKING.COMPANY_ORIGIN",
					field: "companyOrigin",
				},
			]);
		}

		if (
			(this.isChargeur || this.isControlTower) &&
			(this.module == Modules.PRICING || this.module == Modules.TRANSPORT_MANAGEMENT)
		) {
			cols = cols.concat([
				{
					translateCode: "PRICING_BOOKING.CUSTOMER_CREATION_DATE",
					field: "customerCreationDate",
					width: "80px",
					render: function(data) {
						return Statique.isDefined(data) ? Statique.formatDateTimeStr(data) : "";
					}.bind(this),
				},
			]);
		}

		cols.forEach((it) => {
			this.translate.get(it.translateCode).subscribe((res) => (it["header"] = res));
		});

		return cols;
	}

	getDatatableCompId() {
		if (this.module == Modules.PRICING) return GenericTableScreen.PRICING;
		else if (this.module == Modules.TRANSPORT_MANAGEMENT) return GenericTableScreen.BOOKING;

		return null;
	}

	ngOnDestroy() {
		this.subscriptions.forEach((subscription: Subscription) => {
			subscription.unsubscribe();
		});
	}

	async onDataUploadClick() {
		let currentNbr = 0;

		await Statique.waitForTimemout(500);

		if (!this.dataUpload.ident) return;

		let idInt = window.setInterval(
			function() {
				this.pricingService.getTaskProgress(this.dataUpload.ident).subscribe(
					(res) => {
						// Progress ...
						if (currentNbr < +res.finished && +res.finished < +res.total) {
							currentNbr = +res.finished;
							this.dataUploadFeedback = Object.assign({}, res);
							this.dataUploadFeedback.percent = Math.floor((+res.finished / +res.total) * 100);
						}

						// Progress complete instantly case
						else if (+res.finished >= +res.total) {
							this.dataUploadFeedback = Object.assign({}, res);
							this.dataUploadFeedback.percent = 100;
							clearInterval(idInt);
						}

						// Progress already completed
						else if (Statique.isEmptyObject(res)) {
							this.dataUpload.ident = null;
							clearInterval(idInt);
						}

						// Progress already completed
						else if (Statique.isEmptyObject(res)) {
							this.dataUpload.ident = null;
							clearInterval(idInt);
						}

						// interval overflow case
						if (!this.dataUploadResult && !idInt) {
							clearInterval(idInt);
						}
					},
					(error) => {
						if (!this.dataUploadResult)
							this.dataUploadResult = {
								ident: "",
								error: "L'import n'a pas pu terminer, une erreur est survenue dans le serveur.",
							};
						clearInterval(idInt);
					}
				);
			}.bind(this),
			1000
		);
	}
	onUploadCompleteHandler(res) {
		if (res) {
			this.dataUploadProgress = 100;
			this.dataUpload.ident = res.ident;

			// Starting import
			this.pricingService.bulkImport(this.dataUpload.ident).subscribe((res) => {
				clearInterval(this.dataUpload.ident);
				this.dataUpload.ident = null;
				this.dataUploadResult = res;
			});

			// Bulk import progress feedback
			this.onDataUploadClick();
		}
	}
	onUploadProgressHandler(file: UploadFile) {
		if (file && file.progress && file.progress.data && file.progress.data.percentage) {
			this.dataUploadProgress = file.progress.data.percentage;
		}
	}

	onListCategorieLoaded(listCategorie: Array<EbCategorie>) {
		this.listCategorieEmitter.emit(listCategorie);
	}
	onListFieldLoaded(listField: Array<IField>) {
		this.listFieldsEmitter.emit(listField);
	}

	onFlagChange(context: any, row: EbDemande, listFlag: Array<EbFlag>) {
		let listFlagCode = EbFlag.convertListToCommaSeparated(listFlag);
		row.listFlag = listFlagCode;
		context.pricingService.saveFlags(row.ebDemandeNum, listFlagCode).subscribe();
	}
	inlineEditingHandler(key: string, value: any, model: any, context: any) {
		if (key === "selection-change") {
			context.autoSaveRow(model, value);
		}
	}

	autoSaveRow(row: EbDemande, listFlag: Array<EbFlag>) {
		let listFlagCode = EbFlag.convertListToCommaSeparated(listFlag);
		row.listFlag = listFlagCode;
		this.pricingService.saveFlags(row.ebDemandeNum, listFlagCode).subscribe();
	}
}
