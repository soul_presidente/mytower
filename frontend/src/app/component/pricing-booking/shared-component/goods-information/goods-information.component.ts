import { EbChat } from "@app/classes/chat";
import { EbMarchandise } from "./../../../../classes/marchandise";
import { GlobalService } from "./../../../../services/global.service";
import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild,
	AfterViewInit,
	ChangeDetectorRef,
} from "@angular/core";
import { EbDemande } from "@app/classes/demande";
import { EbUser } from "@app/classes/user";
import { Statique } from "@app/utils/statique";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";
import {
	Modules,
	TypeDataEbDemande,
	UserRole,
	ViewType,
	TypeContainer,
	GenericTableScreen,
	NatureDemandeTransport,
	DemandeStatus,
	YesOrNo,
} from "@app/utils/enumeration";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { PricingService } from "@app/services/pricing.service";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import { ArticleDims } from "../../../../classes/articleDims";
import { TreeNode, MessageService } from "primeng/api";
import { Router } from "@angular/router";
import { SearchCriteria } from "@app/utils/searchCriteria";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbTypeConteneur } from "@app/classes/typeConteneur";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { AuthenticationService } from "@app/services/authentication.service";
import { ComptaMatiereService } from "@app/services/compta-matiere.service";
import { EbCptmRegimeSelectionDTO } from "@app/classes/cptm/EbCptmRegimeSelectionDTO";
import { EbTypeGoods } from "@app/classes/EbTypeGoods";
import { TypeGoodsService } from "@app/services/type-goods.service";
import { EcCountry } from "@app/classes/country";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { TextInputGenericCell } from "@app/shared/generic-cell/commons/text-input.generic-cell";
import { ButtonGenericCell } from "@app/shared/generic-cell/commons/button.generic-cell";
import { SelectGenericCell } from "@app/shared/generic-cell/commons/select.generic-cell";
import { GoodsRegimeTempCell } from "./cells/cell-goods-regime-temp.component";
import { KeyValuePipe } from "@angular/common";
import { TranslateService } from "@ngx-translate/core";
import { CountryPickerGenericCell } from "@app/shared/generic-cell/commons/country-picker.generic-cell";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { Delivery } from "@app/classes/ebDelLivraison";
import { UnitChildrenCell } from "./cells/unit-children-select.component";
import { IField, CustomField } from "@app/classes/customField";
import { CustomFieldGenericCell } from "@app/shared/generic-cell/commons/custom-field.generic-cell";
import { ModalService } from "@app/shared/modal/modal.service";
import { EbTypeUnit } from "@app/classes/typeUnit";
import { SelectSimpleListGenericCellComponent } from "@app/shared/generic-cell/commons/SelectSimpleListGenericCell/SelectSimpleListGenericCell.component";
import { LocalNumberFormatPipe } from "@app/shared/pipes/local-number-format.pipe";

@Component({
	selector: "app-goods-information",
	templateUrl: "./goods-information.component.html",
	styleUrls: ["./goods-information.component.scss"],
	providers: [GlobalService, KeyValuePipe],
})
export class GoodsInformationComponent extends ConnectedUserComponent
	implements OnInit, OnChanges, AfterViewInit {
	trConfirmed: number = DemandeStatus.FINAL_CHOICE;
	UserRole = UserRole;
	statique = Statique;
	Modules = Modules;

	@Input()
	hideHeader: boolean;
	@Input()
	module: number;
	@Input()
	ebDemande: EbDemande;
	@Input()
	unitInfos: any;
	@Input()
	userConnected: EbUser;
	@Input()
	listMarchandise: Array<EbMarchandise>;
	@Input()
	modeTransport: Array<EbModeTransport>;
	@Input()
	showTracings: boolean;
	@Input()
	stepIndex: number;
	@Input()
	isForGrouping: boolean;
	@Input()
	marchandiseTypeOfUnit: Array<any>;

	@Input()
	listCustomsFields: Array<IField> = new Array<IField>();

	@Input()
	readOnly: boolean;
	@Input()
	isPending: boolean;

	@Input()
	marchandiseDangerousGood: Map<number, string>;

	@Input()
	marchandiseClassGood: Array<any>;

	@Input()
	listExportControl: Array<any>;

	@Input()
	listCountry: Array<EcCountry>;

	lisUnitreference: Array<Array<EbMarchandise>>;
	selectedUnitRef: Array<Array<EbMarchandise>>;

	listTypeConteneur: EbTypeConteneur[] = [];
	@Input()
	listTypeGoods: Array<EbTypeGoods> = [];
	@Input()
	listRegimesDouaniers: Array<EbCptmRegimeSelectionDTO> = [];

	selectedNode: TreeNode;
	topParent: TreeNode;
	selectedMarchandise: EbMarchandise;

	@Output()
	onValidate = new EventEmitter<any>();
	@ViewChild("pageValid", { static: false })
	pageValid: ElementRef;

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	@Output()
	updateChatEmitter? = new EventEmitter<EbChat>();

	toggleEditData: boolean = false;
	oldListMarchandise: string = null;

	listArticleDims: Array<ArticleDims> = new Array<ArticleDims>();
	treeData: TreeNode[] = [];
	selectedTreeData: TreeNode;
	selectedType: ViewType = ViewType.TREE;
	typeContener: number = 0;
	showContent: boolean = false;
	icon: string = "pi pi-minus";
	msgs: string = "";
	typeUnit: number;
	filteredListMarchandise: Array<EbMarchandise>;
	pageDatas: Array<EbMarchandise>;
	numberPerPage: number = 5;
	totalMarchandiseRecords: number;
	refUnit: string;
	editting: boolean;
	searchTerm: string;
	ViewType = ViewType;

	extracting: boolean;
	extractingConsolidate: boolean;

	// GenericTable
	GenericTableScreen = GenericTableScreen;
	searchCriteriaTable = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	isLoaded: boolean = false;

	volumeAleadyExists: boolean;

	additionalCols: any;

	@Input()
	requiredReadOnly: boolean = false;

	@Input()
	customFieldsToSkip: Array<string> = new Array<string>();

	constructor(
		private generaleMethode: generaleMethodes,
		private pricingService: PricingService,
		private globalService: GlobalService,
		private router: Router,
		protected authenticationService: AuthenticationService,
		protected comptaMatiereService: ComptaMatiereService,
		protected typeGoodsService: TypeGoodsService,
		protected keyValuePipe: KeyValuePipe,
		protected translate: TranslateService,
		protected cd: ChangeDetectorRef,
		protected modalService: ModalService,
		protected messageService: MessageService,
		protected localNumberFormatPipe: LocalNumberFormatPipe
	) {
		super(authenticationService);
	}

	ngOnInit() {
		if (!this.ebDemande.listMarchandises)
			this.ebDemande.listMarchandises = new Array<EbMarchandise>();
		this.lisUnitreference = new Array<Array<EbMarchandise>>();
		this.selectedUnitRef = new Array<Array<EbMarchandise>>();
		this.changeUnitReference();

		this.addDefaultUnitIfCreationMode();

		this.msgs = this.ebDemande.statsMarchandises;
		// this.initStatiques(); // moved to ngAfterViewInit
		// this.initComponent();
		this.initGenericTableCols();
	}

	compareWith(it1: any, it2: any) {
		return it1 && it2 ? it1.key === it2 : it1 === it2;
	}

	compareWithForSelect(it1: any, it2: any) {
		return it1 && it2 ? it1 == it2 : false;
	}

	addDefaultUnitIfCreationMode() {
		if (!this.ebDemande.ebDemandeNum) {
			this.editting = false;
			if (!this.ebDemande.listMarchandises.length) {
				this.addMarchandise();
			}
		} else {
			this.editting = true;
		}
	}

	ngAfterViewInit() {
		this.initGenericTableCore();
		this.isLoaded = true;
		this.initStatiques();
		this.initComponent();
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["ebDemande"] && changes["ebDemande"].currentValue) {
			this.initComponent();
		}
	}
	changeUnitReference() {
		this.lisUnitreference = [];
		this.ebDemande.listMarchandises.forEach((unit) =>
			this.lisUnitreference.push(this.cloneMarchandise(unit))
		);
	}

	cloneMarchandise(unit) {
		let unitsRef = new Array<EbMarchandise>();
		this.ebDemande.listMarchandises.forEach((e, index) => {
			if (
				e.customerReference &&
				this.isNotParent(index, unit) &&
				(!unit.customerReference || unit.customerReference != e.customerReference) &&
				Statique.isDefined(unit.xEbTypeUnit) &&
				Statique.isDefined(e.xEbTypeUnit) &&
				this.findUnitTypeConteneur(unit.xEbTypeUnit) < this.findUnitTypeConteneur(e.xEbTypeUnit)
			) {
				let m = new EbMarchandise();
				m.customerReference = e.customerReference;
				m.unitReference = e.unitReference;
				m.children = e.children;
				unitsRef.push(m);
			}
		});
		return unitsRef;
	}

	isNotParent(index, unit) {
		if (this.ebDemande.listMarchandises[index] && this.ebDemande.listMarchandises[index].children) {
			let found = this.ebDemande.listMarchandises[index].children.find(
				(e) => e.unitReference == unit.unitReference
			);
			if (found) return false;
		}
		return true;
	}
	initStatiques() {
		// List Conteneur
		this.globalService
			.getStatiqueList(Statique.controllerStatiqueListe + "/list-type-conteneur")
			.subscribe((res) => {
				this.listTypeConteneur = res;
			});
	}

	getListOfExportControls(): Promise<any> {
		return new Promise((resolve) => {
			this.typeGoodsService.listExportControls().subscribe((data) => resolve(data));
		});
	}

	initComponent() {
		// List Unit
		if (this.ebDemande.ebDemandeNum) {
			this.globalService
				.runAction(Statique.controllerUnit + "/list-unit", this.ebDemande.ebDemandeNum)
				.subscribe((res) => {
					this.additionalCols = new Array<GenericTableInfos.Col>();
					if (res.listMarchandise) {
						res.listMarchandise.forEach((marchandise) => {
							let customFields = $.parseJSON(marchandise.customFields);
							if (customFields) {
								customFields.forEach((field) => {
									if (field.value) {
										this.additionalCols = this.additionalCols.filter((it) => {
											return it.field !== field.label;
										});
										this.additionalCols.push({
											field: field.label,
											header: field.label,
											name: field.name,
											sortable: false,
											renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
											genericCellClass: TextInputGenericCell,
											genericCellParams: new TextInputGenericCell.Params({
												field: field.label,
												showOnHover: true,
											}),
										});

										marchandise[field.label] = field.value;
									}
								});
							}
						});
					}

					this.unitInfos = res;

					this.initMarchandises();
				});
		} else {
			this.initMarchandises();
		}
	}

	initMarchandises() {
		this.initMarchandiseDatas();

		if (this.isForGrouping || (this.ebDemande.ebDemandeNum && !this.ebDemande.totalWeight)) {
			this.calculateTotalNumberOfUnit(this.ebDemande.listMarchandises.length - 1);
		}

		if (this.ebDemande.listMarchandises) {
			this.listArticleDims = this.ebDemande.listMarchandises.map((it) => {
				return ArticleDims.create(it.width, it.heigth, it.length, it.weight, it.numberOfUnits);
			});

			// Regime Douanier from mask
			this.ebDemande.listMarchandises.forEach((mar) => {
				if (mar.cptmRegimeTemporaireNum != null) {
					this.listRegimesDouaniers.forEach((r) => {
						if (r.ebCptmRegimeTemporaireNum == mar.cptmRegimeTemporaireNum) {
							mar.selectedRegimeDouanier = r;
						}
					});
				} else if (mar.cptmProcedureNum != null) {
					this.listRegimesDouaniers.forEach((r) => {
						if (r.ebCptmProcedureNum == mar.cptmProcedureNum) {
							mar.selectedRegimeDouanier = r;
						}
					});
				}

				if (mar.xEcCountryOrigin && this.listCountry) {
					mar.countryOrigin = this.listCountry.find((c) => c.ecCountryNum == mar.xEcCountryOrigin);
				}

				this.calculateVolume(mar);
			});
		}
	}

	initMarchandiseDatas() {
		if (this.unitInfos) {
			this.ebDemande.listMarchandises = this.unitInfos.listMarchandise;
			this.treeData = this.unitInfos.listTreeNode;
			this.selectedNode = this.treeData ? this.treeData[0] : null;
			if (this.selectedNode != null) {
				this.selectedMarchandise = this.selectedNode.data;
				this.refUnit = this.selectedMarchandise.unitReference;
				this.typeUnit = this.selectedMarchandise.xEbTypeConteneur;
			}

			if (this.ebDemande.listMarchandises) {
				this.ebDemande.listMarchandises.forEach((marchandise) => {
					if (marchandise.volume != null) {
						this.volumeAleadyExists = true;
					}
				});
			}
		}
		this.filteredListMarchandise = this.ebDemande.listMarchandises || [];
		this.totalMarchandiseRecords = this.filteredListMarchandise.length;
		this.getDataToPage(1);

		this.calculateTotalNumberOfUnit(this.ebDemande.listMarchandises.length - 1);

		this.initGenericTableCols();
	}

	searchUnit(value: string) {
		this.searchTerm = value;
		this.initMarchandiseDatas();

		this.treeData = this.treeData.filter((it) => {
			return it.data.unitReference.indexOf(this.searchTerm) >= 0;
		});

		this.filteredListMarchandise = Object.assign([], this.treeData.map((it) => it.data));
		this.totalMarchandiseRecords = this.filteredListMarchandise.length;

		if (this.treeData.length != 0) {
			this.affecteNodeDatas(this.treeData[this.treeData.length - 1]);
		}
		this.getDataToPage(1);
	}

	showUnitChildren(reference) {
		this.selectedTreeData = null;
		this.findTreeNode(reference, this.treeData);
	}

	goToArbreNode(reference) {
		let actualNode: TreeNode = null;
		this.findTreeNode(reference, this.treeData);
		actualNode = this.selectedTreeData;
		this.findTopFather(this.selectedTreeData);

		this.selectedType = ViewType.TABLE;
		this.affecteNodeDatas(actualNode);
		this.expandRecursive(this.topParent, true);
	}

	expandRecursive(node: TreeNode, isExpand: boolean) {
		node.expanded = isExpand;
		if (node.children) {
			node.children.forEach((childNode) => {
				this.expandRecursive(childNode, isExpand);
			});
		}
	}

	findTopFather(node: TreeNode) {
		this.topParent = null;
		if (node.data.parent == null) this.topParent = node;
		else {
			this.findTreeNode(node.data.parent.unitReference, this.treeData);
			this.findTopFather(this.selectedTreeData);
		}
	}

	findTreeNode(reference, treeData) {
		for (let i = 0; i < treeData.length; i++) {
			if (treeData[i].data.unitReference == reference) {
				this.selectedTreeData = treeData[i];
				i = treeData.length;
			} else {
				if (treeData[i].children.length > 0) {
					this.findTreeNode(reference, treeData[i].children);
				}
			}
		}
	}

	findMarchandiseByReference(reference: any) {
		let marchandise;
		this.ebDemande.listMarchandises.forEach((march) => {
			if (march.unitReference == reference) marchandise = march;
		});
		return marchandise;
	}

	hasChildren(reference: any) {
		let exist = false;
		for (let i = 0; i < 4; i++) {
			if (
				this.ebDemande.listMarchandises[i].parent != null &&
				this.ebDemande.listMarchandises[i].parent.ebMarchandiseNum == reference
			) {
				exist = true;
				i = this.ebDemande.listMarchandises.length;
			}
		}
		return exist;
	}

	showOrHideInformations(typeContainer: any, field: string) {
		let show: boolean = false;
		if (Number(typeContainer) == 4 && field == "article") show = true;
		if ((Number(typeContainer) == 2 || Number(typeContainer) == 3) && field == "colisPalette")
			show = true;
		if (
			(Number(typeContainer) == 1 ||
				Number(typeContainer) == 2 ||
				Number(typeContainer) == 3 ||
				Number(typeContainer) == 0) &&
			field == "parcelPaletteContainer"
		)
			show = true;
		return show;
	}

	paginate(event) {
		this.numberPerPage = event.rows;
		this.getDataToPage(event.page + 1);
	}

	getDataToPage(page: number) {
		this.pageDatas = [];
		let from = (page - 1) * this.numberPerPage,
			to = page * this.numberPerPage - 1;
		if (to >= this.filteredListMarchandise.length - 1) to = this.filteredListMarchandise.length - 1;
		for (let i = from; i <= to; i++) {
			this.pageDatas.push(this.filteredListMarchandise[i]);
		}
		if (to <= 0) this.pageDatas = this.filteredListMarchandise;
	}

	beginMarchandiseFilter(event) {
		this.filterListMarchandise(event.value);
	}

	applySelectedValueToFilter() {
		this.filterListMarchandise(this.typeContener);
	}

	filterListMarchandise(type: any) {
		this.filteredListMarchandise = [];
		this.typeContener = type;
		if (type == 0) {
			this.filteredListMarchandise = this.ebDemande.listMarchandises;
		} else {
			this.ebDemande.listMarchandises.forEach((marchandise) => {
				if (this.findUnitTypeConteneur(marchandise.xEbTypeUnit) == type) {
					this.filteredListMarchandise.push(marchandise);
				}
			});
		}
		this.totalMarchandiseRecords = this.filteredListMarchandise.length;
		this.getDataToPage(1);
	}

	getNumberMarchandiseOftype(key) {
		let numberMarch: number = 0;
		if (this.ebDemande.listMarchandises) {
			this.ebDemande.listMarchandises.forEach((marchandise) => {
				if (
					this.findUnitTypeConteneur(marchandise.xEbTypeUnit) == key &&
					marchandise.numberOfUnits
				) {
					numberMarch += +marchandise.numberOfUnits;
				}
			});
		}
		return numberMarch;
	}

	findUnitTypeConteneur(unitNum) {
		let typeConteneur = 0;
		this.marchandiseTypeOfUnit.forEach((typeMarch) => {
			if (typeMarch.xEbTypeUnit == unitNum) typeConteneur = typeMarch.xEbTypeConteneur;
		});
		return typeConteneur;
	}

	triggerNgForTypeConteneur() {
		this.listTypeConteneur = this.listTypeConteneur.slice();
	}

	changeType() {
		this.selectedType = this.selectedType == ViewType.TREE ? ViewType.TABLE : ViewType.TREE;
	}

	getFormDatas() {
		return !(JSON.stringify(new EbMarchandise()) == JSON.stringify(this.selectedMarchandise));
	}

	nodeSelect(event) {
		this.affecteNodeDatas(event.node);
	}

	affecteNodeDatas(node: TreeNode) {
		this.selectedNode = node;
		this.selectedMarchandise = this.selectedNode.data;
		this.refUnit = this.selectedMarchandise.unitReference;
		this.typeUnit = this.selectedMarchandise.xEbTypeConteneur;
	}

	addMarchandise() {
		let marchandise = new EbMarchandise();
		marchandise.customerReference = "" + this.ebDemande.listMarchandises.length;
		marchandise.numberOfUnits = 1;
		marchandise.volume = 0;
		marchandise.weight = 0;
		marchandise.listCustomsFields = Statique.cloneListObject(this.listCustomsFields, CustomField);
		this.ebDemande.listMarchandises.push(marchandise);
		this.listMarchandise = this.ebDemande.listMarchandises;
		this.calculateTotalNumberOfUnit(this.ebDemande.listMarchandises.length - 1);
		this.filterListMarchandise(this.typeContener);
	}

	removeMarchandise(marchandise: EbMarchandise) {
		let index = this.ebDemande.listMarchandises.indexOf(marchandise);
		if (this.ebDemande.listMarchandises.length == 1) {
			this.ebDemande.listMarchandises[index] = new EbMarchandise();
		} else {
			this.ebDemande.listMarchandises.splice(index, 1);
		}
		this.listMarchandise = this.ebDemande.listMarchandises;
		this.calculateTotalNumberOfUnit(index);
		this.filterListMarchandise(this.typeContener);
		this.changeUnitReference();
	}

	calculateTotalNumberOfUnit(index: number) {
		if (!this.ebDemande.listMarchandises || this.ebDemande.listMarchandises.length <= 0) return;

		this.ebDemande.totalNbrParcel = Statique.truncate2Decimal(
			this.ebDemande.listMarchandises
				.map((marchandise) => marchandise.numberOfUnits)
				.reduce((m1: number, m2: number) => +m1 + +m2)
		);
		this.calculateTotalVolume();
		this.calculateTotalWeight(null);
		this.calculateTotalTaxableWeight();
		this.calculateVolume(this.ebDemande.listMarchandises[index]);

		// Set to null instead of 0 to search plan transport with null
		if (!this.hasAtLeastOneUnitFilled()) {
			this.ebDemande.totalNbrParcel = null;
		}
	}

	calculateTotalVolume() {
		this.ebDemande.totalVolume =
			Math.trunc(
				this.ebDemande.listMarchandises
					.map(
						(marchandise) =>
							Number(Statique.formatNumberValue(marchandise.volume)) * marchandise.numberOfUnits
					)
					.reduce((m1: number, m2: number) => +m1 + +m2) * 1000000
			) / 1000000;
		if (Number.isNaN(this.ebDemande.totalVolume)) {
			this.ebDemande.totalVolume = 0;
		}

		this.calculateTotalTaxableWeight();
	}

	calculateTotalWeight(index: number) {
		this.ebDemande.totalWeight = Statique.truncate2Decimal(
			this.ebDemande.listMarchandises
				.map(
					(marchandise) =>
						Number(Statique.formatNumberValue(marchandise.weight)) * marchandise.numberOfUnits
				)
				.reduce((m1: number, m2: number) => +m1 + +m2)
		);
		if (Number.isNaN(this.ebDemande.totalWeight)) {
			this.ebDemande.totalWeight = 0;
		}
		this.calculateTotalTaxableWeight();
		if (index != null) {
			this.calculateVolume(this.ebDemande.listMarchandises[index]);
		}
	}

	calculateTotalTaxableWeight() {
		if (this.modeTransport && this.modeTransport.length > 0) {
			if (this.ebDemande.xEcModeTransport === this.modeTransport[0].code) {
				//Air
				this.ebDemande.totalTaxableWeight = Statique.truncate2Decimal(
					Math.max(
						this.ebDemande.totalWeight,
						Statique.arrondiToZeroFive((this.ebDemande.totalVolume / 6) * 1000)
					)
				);
			} else if (this.ebDemande.xEcModeTransport === this.modeTransport[1].code) {
				// Sea
				this.ebDemande.totalTaxableWeight = Statique.truncate2Decimal(
					Math.max(
						this.ebDemande.totalWeight,
						Statique.arrondiToZeroFive(this.ebDemande.totalVolume * 1000)
					)
				);
			} else if (this.ebDemande.xEcModeTransport === this.modeTransport[2].code) {
				//Road
				this.ebDemande.totalTaxableWeight = Statique.truncate2Decimal(
					Math.max(
						this.ebDemande.totalWeight,
						Statique.arrondiToZeroFive((this.ebDemande.totalVolume / 3) * 1000)
					)
				);
			} else if (this.ebDemande.xEcModeTransport === this.modeTransport[4].code) {
				//Integrator
				this.ebDemande.totalTaxableWeight = Statique.truncate2Decimal(
					Math.max(
						this.ebDemande.totalWeight,
						Statique.arrondiToZeroFive((this.ebDemande.totalVolume * 1000000) / 5000)
					)
				);
			}
		}

		if (!this.hasAtLeastOneWeightFilled()) {
			this.ebDemande.totalTaxableWeight = null;
		}
	}

	hideAndShowContent() {
		this.showContent = !this.showContent;
		this.icon = this.showContent ? "pi pi-plus" : "pi pi-minus";
	}

	calculateVolume(marchandise: EbMarchandise) {
		if (
			marchandise &&
			!this.volumeAleadyExists &&
			(marchandise.length != null && marchandise.heigth != null && marchandise.width != null)
		) {
			marchandise.volume =
				Math.trunc(
					Number(Statique.formatNumberValue(marchandise.length)) *
						Number(Statique.formatNumberValue(marchandise.heigth)) *
						Number(Statique.formatNumberValue(marchandise.width))
				) / 1000000;
		}
		this.calculateTotalVolume();
	}

	nextPage() {
		this.onValidate.emit();
	}

	isPageValid() {
		return this.pageValid && !this.pageValid.nativeElement.disabled;
	}

	panelCollapse(panel, event) {
		this.generaleMethode.panelCollapse(panel, event);
	}

	panelFullScreen(panel) {
		this.generaleMethode.panelFullScreen(panel);
	}

	showBlock(): boolean {
		return !this.isForGrouping && this.ebDemande.ebDemandeNum == null && this.stepIndex != 2;
	}

	addTagPromise(event: any) {
		let evt = new CustomEvent("addTag", { detail: event });
		document.dispatchEvent(evt);
		return event;
	}

	handleToggleEditData() {
		this.oldListMarchandise = JSON.stringify(this.ebDemande.listMarchandises);
		this.toggleEditData = true;
	}

	handleCancelEditData() {
		this.ebDemande.listMarchandises = this.oldListMarchandise
			? JSON.parse(this.oldListMarchandise)
			: this.ebDemande.listMarchandises;
		this.toggleEditData = false;
	}

	editData(event) {
		if (this.ebDemande && this.ebDemande.ebDemandeNum) {
			let mapData = {};

			let requestProcessing = new RequestProcessing();
			requestProcessing.beforeSendRequest(event);

			let demande = new EbDemande(true);
			demande.ebDemandeNum = this.ebDemande.ebDemandeNum;
			demande.listMarchandises = this.ebDemande.listMarchandises;
			demande.totalNbrParcel = this.ebDemande.totalNbrParcel;
			demande.totalVolume = this.ebDemande.totalVolume;
			demande.totalTaxableWeight = this.ebDemande.totalTaxableWeight;
			demande.totalWeight = this.ebDemande.totalWeight;

			mapData["typeData"] = TypeDataEbDemande.GOODS_INFORMATION;
			mapData["ebDemande"] = demande;
			mapData["module"] = this.module;

			this.pricingService.updateEbDemande(mapData).subscribe((res) => {
				this.toggleEditData = false;
				this.oldListMarchandise = null;
				requestProcessing.afterGetResponse(event);
			});
		}
	}

	hasAtLeastOneWeightFilled(): boolean {
		var found = false;
		this.ebDemande.listMarchandises.forEach((m) => {
			if (m.weight || m.weight === 0) found = true;
		});
		return found;
	}

	hasAtLeastOneUnitFilled(): boolean {
		var found = false;
		this.ebDemande.listMarchandises.forEach((m) => {
			if (m.numberOfUnits || m.numberOfUnits === 0) found = true;
		});
		return found;
	}

	changeDisplayMode(event) {}

	goToTrackingDetails(refTrack) {
		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(Modules.TRACK)];
		let link = "/app/" + moduleRoute + "/details/" + refTrack;
		// this.router.navigate([link]);
		window.open(link, "_blank");
	}

	exportUnit() {
		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.ebDemandeNum = this.ebDemande.ebDemandeNum;
		searchCriteria.typeContener = this.typeContener;
		this.extracting = true;
		this.globalService.export(searchCriteria).subscribe((blob) => {
			this.extracting = false;
			this.downloadFile(blob, this.typeContener);
		});
	}

	exportUnitConnsolider() {
		this.extractingConsolidate = true;
		this.globalService.exportConsolider(this.ebDemande.ebDemandeNum).subscribe((blob) => {
			this.extractingConsolidate = false;
			this.downloadFile(blob, 0);
		});
	}

	downloadFile(data, xEbtypeConteneur: number) {
		const dt = new Date();
		const filename =
			this.getLibellebyTypeConteneur(xEbtypeConteneur).toLocaleLowerCase() +
			"-" +
			dt.getDate() +
			"-" +
			(dt.getMonth() + 1) +
			"-" +
			dt.getFullYear() +
			"-" +
			dt.getHours() +
			"-" +
			dt.getMinutes() +
			".xlsx";

		let link = document.createElement("a");
		link.setAttribute("type", "hidden");
		link.href = window.URL.createObjectURL(data);
		document.body.appendChild(link);
		link.download = filename;
		link.click();
		link.remove();
	}

	getLibellebyTypeConteneur(xEbtypeConteneur: number) {
		let libelle: string = "Liste_Marchandise";
		if (xEbtypeConteneur == TypeContainer.ARTICLE) {
			libelle = "Liste_Article";
		} else if (xEbtypeConteneur == TypeContainer.PARCEL) {
			libelle = "Liste_Parcel";
		} else if (xEbtypeConteneur == TypeContainer.PALETTE) {
			libelle = "Liste_Palette";
		} else if (xEbtypeConteneur == TypeContainer.CONTAINER) {
			libelle = "Liste_Container";
		}
		return libelle;
	}

	customsRegimeChange(marchandise: EbMarchandise) {
		if (marchandise.selectedRegimeDouanier != null) {
			marchandise.cptmProcedureNum = marchandise.selectedRegimeDouanier.ebCptmProcedureNum;
			marchandise.cptmRegimeDouanierLabel = marchandise.selectedRegimeDouanier.label;
			marchandise.cptmRegimeTemporaireNum =
				marchandise.selectedRegimeDouanier.ebCptmRegimeTemporaireNum;

			if (marchandise.selectedRegimeDouanier.procedureOperationNumber) {
				marchandise.cptmOperationNumber =
					marchandise.selectedRegimeDouanier.procedureOperationNumber;
			}
			if (marchandise.selectedRegimeDouanier.leg1RefUnit) {
				marchandise.unitReference = marchandise.selectedRegimeDouanier.leg1RefUnit;
			}
		} else {
			marchandise.cptmProcedureNum = null;
			marchandise.cptmRegimeDouanierLabel = null;
			marchandise.cptmRegimeTemporaireNum = null;
		}
	}

	typeGoodsChange(unit: EbMarchandise) {
		if (unit.typeGoodsLabel != null) {
			const typeGoodsLabel = unit.typeGoodsLabel;
			const typeOfGood = this.listTypeGoods.find(
				(typeOfGood) => typeOfGood.label == typeGoodsLabel
			);
			if (typeOfGood) unit.typeGoodsNum = typeOfGood.ebTypeGoodsNum;
		} else {
			unit.typeGoodsNum = null;
			unit.typeGoodsLabel = null;
		}
	}
	onChangeOriginCoutry(marchandise: EbMarchandise) {
		if (marchandise.countryOrigin) {
			marchandise.xEcCountryOrigin = marchandise.countryOrigin.ecCountryNum;
			marchandise.originCountryName = marchandise.countryOrigin.libelle;
		} else {
			marchandise.xEcCountryOrigin = null;
			marchandise.originCountryName = null;
		}
	}

	initGenericTableCore() {
		this.searchCriteriaTable.module = this.module;

		this.dataInfos.dataKey = "ebMarchandiseNum";
		this.dataInfos.dataSourceType = GenericTableInfos.DataSourceType.Local;
		this.dataInfos.dataType = EbMarchandise;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = false;
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.showCollapsibleBtn = false;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = false;
		this.dataInfos.showCheckbox = false;
		this.dataInfos.showCheckboxGlobal = false;
		this.dataInfos.showExportBtn = false;
		this.dataInfos.cardView = false;
		this.dataInfos.showAdvancedSearchBtn = false;

		this.dataInfos.lineActions = [
			{
				conditionalTitle: function(row: EbMarchandise) {
					if (Statique.isDefined(row.xEbLivraisonLines))
						return row.xEbLivraisonLines.length != null && row.xEbLivraisonLines.length > 0
							? "PRICING_BOOKING.DELVRERY_INFOBULLE"
							: null;
					else return null;
				},
				icon: "fa fa-trash",
				showCondition: (row: EbMarchandise) => {
					return !this.editting || (!this.readOnly && this.ebDemande.xEcStatut < this.trConfirmed);
				},
				onClick: (model: EbMarchandise) => {
					this.removeMarchandise(model);
				},
				enableCondition: (model: EbMarchandise) => {
					const isFromDelivery =
						model.xEbLivraisonLines != null && model.xEbLivraisonLines.length > 0;
					return !isFromDelivery;
				},
			},
		];
	}

	initGenericTableCols() {
		let cols = new Array<GenericTableInfos.Col>();
		cols.push({
			field: "unitReference",
			translateCode: "TRACK_TRACE.UNIT_REFERENCE",
			sortable: false,
			renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
			genericCellClass: TextInputGenericCell,
			genericCellParams: new TextInputGenericCell.Params({
				field: "unitReference",
				showOnHover: true,
				disabledOn: () => this.readOnly,
			}),
			genericCellHandler: (
				key: string,
				value: any,
				model: EbMarchandise,
				context: GoodsInformationComponent
			) => {
				if (key === "input-event" && value === "change") {
					context.changeUnitReference();
				}
			},
		});
		if (this.userConnected.rolePrestataire) {
			cols.push({
				field: "packingList",
				translateCode: "TYPE_UNIT.PACKING_LIST",
				sortable: false,
			});
		} else {
			cols.push({
				field: "packingList",
				translateCode: "TYPE_UNIT.PACKING_LIST",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "packingList",
					showOnHover: true,
					disabledOn: () => this.requiredReadOnly,
				}),
				genericCellHandler: (
					key: string,
					value: any,
					model: EbMarchandise,
					context: GoodsInformationComponent
				) => {
					if (key === "input-event" && value === "change") {
						context.changeUnitReference();
					}
				},
			});
		}
		if (this.readOnly) {
			cols.push({
				field: "parent",
				translateCode: "TYPE_UNIT.IS_IN_UNIT",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: ButtonGenericCell,
				genericCellParams: new ButtonGenericCell.Params({
					field: "parent",
					hideIfNoText: true,
					textRender: (row: EbMarchandise) => {
						if (row.parent == null) return "";
						return row.parent.unitReference;
					},
				}),
				genericCellHandler: (
					key: string,
					value: any,
					model: EbMarchandise,
					context: GoodsInformationComponent
				) => {
					if (key === "click") {
						context.goToArbreNode(model.parent.unitReference);
					}
				},
			});
		}
		if (
			this.getFormDatas() &&
			this.showOrHideInformations(this.typeContener, "parcelPaletteContainer")
		) {
			if (this.readOnly) {
				cols.push({
					field: "listUnitreference",
					translateCode: "TYPE_UNIT.CONTAINS_UNITS",
					sortable: false,
					width: "150px",
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: ButtonGenericCell,
					genericCellParams: new ButtonGenericCell.Params({
						hideIfNoText: false,
						textRender: (row: EbMarchandise) => {
							return this.translate.instant("PRICING_BOOKING.DETAILS");
						},
					}),
					genericCellHandler: (
						key: string,
						value: any,
						model: EbMarchandise,
						context: GoodsInformationComponent
					) => {
						if (key === "click") {
							context.goToArbreNode(model.unitReference);
						}
					},
				});
			} else {
				cols.push({
					field: "children",
					translateCode: "TYPE_UNIT.CONTAINS_UNITS",
					sortable: false,
					width: "150px",
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: UnitChildrenCell,
					genericCellParams: new UnitChildrenCell.Params({
						field: "children",
						valuesOn: (model: EbMarchandise) => {
							if (!this.lisUnitreference) return [];
							return this.ebDemande.listMarchandises
								? this.lisUnitreference[this.ebDemande.listMarchandises.indexOf(model)]
								: null;
						},
						itemTextField: "unitReference",
						multiple: true,
						showOnHover: true,
						compareWith: (a: EbMarchandise, b: EbMarchandise): boolean => {
							if (a == null || b == null) {
								return false;
							}

							return a.customerReference == b.customerReference;
						},
					}),
					genericCellHandler: (
						key: string,
						value: any,
						model: EbMarchandise,
						context: GoodsInformationComponent
					) => {
						if (key === "selection-change") {
							context.changeUnitReference();
						}
					},
				});
			}
		}
		if (this.userConnected.rolePrestataire) {
			cols.push(
				{
					field: "serialNumber",
					translateCode: "PRICING_BOOKING.SERIAL_NUMBER",
					sortable: false,
				},
				{
					field: "serialized",
					translateCode: "CHANEL_PB_ORDER.SERIALIZED",
					sortable: false,
				},
				{
					field: "partNumber",
					translateCode: "PRICING_BOOKING.PART_NUMBER",
					sortable: false,
				},
				{
					field: "itemName",
					translateCode: "CHANEL_PB_DELIVERY.ARTICLE_NAME",
					sortable: false,
				},
				{
					field: "itemNumber",
					translateCode: "ORDER.ITEM_NUMBER",
					sortable: false,
				},
				{
					field: "hsCode",
					header: "HS Code",
					sortable: false,
				},
				{
					field: "price",
					translateCode: "PRICING_BOOKING.PRICE",
					sortable: false,
				},
				{
					field: "customerOrderReference",
					translateCode: "ORDER.CUSTOMER_ORDER_REFERENCE",
					width: "150px",
					isCardCol: true,
					cardOrder: 2,
					isCardLabel: false,
				},
				{
					field: "idEbDelLivraison",
					translateCode: "PRICING_BOOKING.ID_DELIVERY",
					width: "150px",
					isCardCol: true,
					cardOrder: 2,
					isCardLabel: false,
				},
				{
					field: "binLocation",
					header: "Bin Location",
					sortable: false,
				},
				{
					field: "exportControlStatut",
					translateCode: "PRICING_BOOKING.EXPORT_CONTROL.HEADER",
					sortable: false,
				},
				{
					field: "exportRef",
					header: "Export control reference",
					sortable: false,
				}
			);
		} else {
			cols.push(
				{
					field: "serialNumber",
					translateCode: "PRICING_BOOKING.SERIAL_NUMBER",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "serialNumber",
						showOnHover: true,
						keyupOn: (evt, model: EbMarchandise) => {
							if (model.serialNumber && model.serialNumber.length > 0) {
								model.serialized = true;
								model.numberOfUnits = 1;
							} else {
								model.serialized = null;
							}
						},
						disabledOn: () => this.requiredReadOnly,
					}),
				},
				{
					field: "serialized",
					translateCode: "CHANEL_PB_ORDER.SERIALIZED",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: SelectGenericCell,
					genericCellParams: new SelectGenericCell.Params({
						field: "serialized",
						itemValueField: "key",
						itemTextField: "value",
						values: Statique.yesOrNoWithBooleanKey,
						compareWith: Statique.compareByField,
						showOnHover: true,
						disabledOn: (model: EbMarchandise) => {
							return (
								(model.serialNumber != null && model.serialNumber.length > 0) ||
								this.requiredReadOnly
							);
						},
						getTitle: (model: EbMarchandise) => {
							if (model.serialNumber != null && model.serialNumber.length > 0)
								return this.translate.instant("CHANEL_PB_ORDER.ALERT_SERIALIZED_ARTICLE_FOR_SER");
							return "";
						},
					}),
				},
				{
					field: "partNumber",
					translateCode: "PRICING_BOOKING.PART_NUMBER",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "partNumber",
						showOnHover: true,
						disabledOn: () => this.requiredReadOnly,
					}),
				},
				{
					field: "itemName",
					translateCode: "CHANEL_PB_DELIVERY.ARTICLE_NAME",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "itemName",
						showOnHover: true,
					}),
				},
				{
					field: "itemNumber",
					translateCode: "ORDER.ITEM_NUMBER",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "itemNumber",
						showOnHover: true,
					}),
				},
				{
					field: "hsCode",
					header: "HS Code",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "hsCode",
						showOnHover: true,
						disabledOn: () => this.requiredReadOnly,
					}),
				},
				{
					field: "price",
					translateCode: "PRICING_BOOKING.PRICE",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "price",
						showOnHover: true,
						disabledOn: () => this.requiredReadOnly,
					}),
				},
				{
					field: "customerOrderReference",
					translateCode: "ORDER.CUSTOMER_ORDER_REFERENCE",
					width: "150px",
					isCardCol: true,
					cardOrder: 2,
					isCardLabel: false,
				},
				{
					field: "idEbDelLivraison",
					translateCode: "PRICING_BOOKING.ID_DELIVERY",
					width: "150px",
					isCardCol: true,
					cardOrder: 2,
					isCardLabel: false,
				},
				{
					field: "binLocation",
					header: "Bin Location",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "binLocation",
						showOnHover: true,
						disabledOn: () => this.requiredReadOnly,
					}),
				},
				{
					field: "exportControlStatut",
					translateCode: "PRICING_BOOKING.EXPORT_CONTROL.HEADER",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: SelectGenericCell,
					genericCellParams: new SelectGenericCell.Params({
						field: "exportControlStatut",
						values: this.listExportControl,
						itemValueField: "code",
						itemTextField: "key",
						disabledOn: () => this.requiredReadOnly,
						compareWith: Statique.byExportControlNum,
					}),
				},
				{
					field: "exportRef",
					header: "Export control reference",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "exportRef",
						showOnHover: true,
						disabledOn: () => this.requiredReadOnly,
					}),
				}
			);
		}

		if (this.getFormDatas() && this.showOrHideInformations(this.typeContener, "article")) {
			cols.push({
				field: "nomArticle",
				header: "Nom",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "nomArticle",
					showOnHover: true,
				}),
			});
		}
		if (this.userConnected.rolePrestataire) {
			cols.push({
				field: "countryOrigin",
				translateCode: "TRACK_TRACE.ORIGIN_COUNTRY",
				sortable: false,
			});
		} else {
			cols.push({
				field: "countryOrigin",
				translateCode: "TRACK_TRACE.ORIGIN_COUNTRY",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: CountryPickerGenericCell,
				genericCellParams: new CountryPickerGenericCell.Params({
					field: "countryOrigin",
					values: this.listCountry,
					itemValueField: "ecCountryNum",
					itemTextField: "libelle",
					showOnHover: true,
					disabledOn: () => this.requiredReadOnly,
				}),
				genericCellHandler: (
					key: string,
					value: any,
					model: EbMarchandise,
					context: GoodsInformationComponent
				) => {
					if (key === "selection-change") {
						context.onChangeOriginCoutry(model);
					}
				},
			});
		}
		if (this.getFormDatas() && this.showOrHideInformations(this.typeContener, "colisPalette")) {
			cols.push(
				{
					field: "sscc",
					header: "SSCC",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "sscc",
						showOnHover: true,
						disabledOn: () => this.requiredReadOnly,
					}),
				},
				{
					field: "tracking_number",
					header: "N° Tracking",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "tracking_number",
						showOnHover: true,
						disabledOn: () => this.requiredReadOnly,
					}),
				}
			);
		}
		if (this.userConnected.rolePrestataire) {
			cols.push(
				{
					field: "numberOfUnits",
					translateCode: "PRICING_BOOKING.NUMBER_OF_UNITS",
					sortable: false,
					width: "100px",
				},
				{
					field: "xEbTypeUnit",
					translateCode: "PRICING_BOOKING.TYPE_OF_UNIT",
					sortable: false,
					width: "200px",
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: SelectGenericCell,
					genericCellParams: new SelectGenericCell.Params({
						field: "xEbTypeUnit",
						values: this.marchandiseTypeOfUnit,
						itemValueField: "xEbTypeUnit",
						itemTextField: "label",
						showOnHover: true,
						compareWith: this.compareWithForSelect,
						disabledOn: () => this.requiredReadOnly,
					}),
					genericCellHandler: (
						key: string,
						value: any,
						model: EbMarchandise,
						context: GoodsInformationComponent
					) => {
						if (key === "selection-change") {
							context.triggerNgForTypeConteneur();
							context.changeUnitReference();
						}
					},
				}
			);
		} else {
			cols.push(
				{
					field: "numberOfUnits",
					translateCode: "PRICING_BOOKING.NUMBER_OF_UNITS",
					sortable: false,
					width: "100px",
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "numberOfUnits",
						showOnHover: true,
						pattern: Statique.patternNumber,
						inputType: "number",
						min: 0,
						requiredOn: (model: EbMarchandise) => {
							return model.numberOfUnits == null || model.numberOfUnits < 1;
						},
						disabledOn: (model: EbMarchandise) => {
							return this.requiredReadOnly;
						},
					}),
					genericCellHandler: (
						key: string,
						value: any,
						model: EbMarchandise,
						context: GoodsInformationComponent
					) => {
						if (key === "input-event" && value === "blur") {
							context.calculateTotalNumberOfUnit(this.ebDemande.listMarchandises.indexOf(model));
						}
					},
				},
				{
					field: "xEbTypeUnit",
					translateCode: "PRICING_BOOKING.TYPE_OF_UNIT",
					sortable: false,
					width: "200px",
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: SelectGenericCell,
					genericCellParams: new SelectGenericCell.Params({
						field: "xEbTypeUnit",
						values: this.marchandiseTypeOfUnit,
						itemValueField: "xEbTypeUnit",
						itemTextField: "label",
						showOnHover: true,
						compareWith: this.compareWithForSelect,
						disabledOn: () => this.requiredReadOnly,
					}),
					genericCellHandler: (
						key: string,
						value: any,
						model: EbMarchandise,
						context: GoodsInformationComponent
					) => {
						if (key === "selection-change") {
							context.triggerNgForTypeConteneur();
							context.changeUnitReference();
						}
					},
				}
			);
		}
		if (this.ebDemande.xEcStatut > this.trConfirmed || this.userConnected.rolePrestataire) {
			cols.push({
				field: "typeGoodsLabel",
				translateCode: "PRICING_BOOKING.TYPE_OF_GOODS",
				sortable: false,
			});
		} else {
			cols.push({
				field: "typeGoodsLabel",
				translateCode: "PRICING_BOOKING.TYPE_OF_GOODS",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: SelectGenericCell,
				genericCellParams: new SelectGenericCell.Params({
					field: "typeGoodsLabel",
					values: this.listTypeGoods,
					itemValueField: "label",
					itemTextField: "label",
					showOnHover: true,
					disabledOn: () => this.requiredReadOnly,
					compareWith: this.compareWithForSelect,
				}),
				genericCellHandler: (
					key: string,
					value: any,
					model: EbMarchandise,
					context: GoodsInformationComponent
				) => {
					if (key === "selection-change") {
						context.typeGoodsChange(model);
					}
				},
			});
		}
		if (this.userConnected.rolePrestataire) {
			cols.push(
				{
					field: "weight",
					translateCode: "PRICING_BOOKING.WEIGHT_UNIT",
					sortable: false,
					width: "100px",
				},
				{
					field: "length",
					translateCode: "PRICING_BOOKING.LENGTH",
					sortable: false,
					width: "100px",
				},
				{
					field: "width",
					translateCode: "PRICING_BOOKING.WIDTH",
					sortable: false,
					width: "100px",
				},
				{
					field: "heigth",
					translateCode: "PRICING_BOOKING.HEIGHT",
					sortable: false,
					width: "100px",
				}
			);
		} else {
			cols.push(
				{
					field: "weight",
					translateCode: "PRICING_BOOKING.WEIGHT_UNIT",
					sortable: false,
					width: "100px",
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "weight",
						showOnHover: true,
						pattern: Statique.patternDecimalNumber,
						disabledOn: () => this.requiredReadOnly,
					}),
					genericCellHandler: (
						key: string,
						value: any,
						model: EbMarchandise,
						context: GoodsInformationComponent
					) => {
						if (key === "input-event" && value === "blur") {
							context.calculateTotalWeight(this.ebDemande.listMarchandises.indexOf(model));
						}
					},
					render: (weight: number) => {
						if (Number.isNaN(weight)) return 0;
						else return this.localNumberFormatPipe.transform(weight, "1.2-2");
					},
				},
				{
					field: "length",
					translateCode: "PRICING_BOOKING.LENGTH",
					sortable: false,
					width: "100px",
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "length",
						showOnHover: true,
						pattern: Statique.patternDecimalNumber,
						disabledOn: () => this.requiredReadOnly,
					}),
					genericCellHandler: (
						key: string,
						value: any,
						model: EbMarchandise,
						context: GoodsInformationComponent
					) => {
						if (key === "input-event" && value === "blur") {
							this.volumeAleadyExists = false;
							context.calculateVolume(model);
						}
					},
					render: (length: number) => {
						return this.localNumberFormatPipe.transform(length, "1.2-2");
					},
				},
				{
					field: "width",
					translateCode: "PRICING_BOOKING.WIDTH",
					sortable: false,
					width: "100px",
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "width",
						showOnHover: true,
						pattern: Statique.patternDecimalNumber,
						disabledOn: () => this.requiredReadOnly,
					}),
					genericCellHandler: (
						key: string,
						value: any,
						model: EbMarchandise,
						context: GoodsInformationComponent
					) => {
						if (key === "input-event" && value === "blur") {
							this.volumeAleadyExists = false;
							context.calculateVolume(model);
						}
					},
					render: (width: number) => {
						return this.localNumberFormatPipe.transform(width, "1.2-2");
					},
				},
				{
					field: "heigth",
					translateCode: "PRICING_BOOKING.HEIGHT",
					sortable: false,
					width: "100px",
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "heigth",
						showOnHover: true,
						pattern: Statique.patternDecimalNumber,
						disabledOn: () => this.requiredReadOnly,
					}),
					genericCellHandler: (
						key: string,
						value: any,
						model: EbMarchandise,
						context: GoodsInformationComponent
					) => {
						if (key === "input-event" && value === "blur") {
							this.volumeAleadyExists = false;
							context.calculateVolume(model);
						}
					},
					render: (heigth: number) => {
						return this.localNumberFormatPipe.transform(heigth, "1.2-2");
					},
				}
			);
		}
		if (this.getFormDatas() && this.showOrHideInformations(this.typeContener, "article")) {
			if (this.userConnected.rolePrestataire) {
				cols.push(
					{
						field: "hsCode",
						header: "HS Code",
						sortable: false,
					},
					{
						field: "batch",
						header: "Batch",
						sortable: false,
					},
					{
						field: "batchProductionDate",
						header: "Batch production date",
						sortable: false,
					},
					{
						field: "batchExpirationDate",
						header: "Batch expiration date",
						sortable: false,
					}
				);
			} else {
				cols.push(
					{
						field: "hsCode",
						header: "HS Code",
						sortable: false,
						renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
						genericCellClass: TextInputGenericCell,
						genericCellParams: new TextInputGenericCell.Params({
							field: "hsCode",
							showOnHover: true,
							disabledOn: () => this.requiredReadOnly,
						}),
					},
					{
						field: "batch",
						header: "Batch",
						sortable: false,
						renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
						genericCellClass: TextInputGenericCell,
						genericCellParams: new TextInputGenericCell.Params({
							field: "batch",
							showOnHover: true,
							disabledOn: () => this.requiredReadOnly,
						}),
					},
					{
						field: "batchProductionDate",
						header: "Batch production date",
						sortable: false,
						renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
						genericCellClass: TextInputGenericCell,
						genericCellParams: new TextInputGenericCell.Params({
							field: "batchProductionDate",
							showOnHover: true,
							disabledOn: () => this.requiredReadOnly,
						}),
					},
					{
						field: "batchExpirationDate",
						header: "Batch expiration date",
						sortable: false,
						renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
						genericCellClass: TextInputGenericCell,
						genericCellParams: new TextInputGenericCell.Params({
							field: "batchExpirationDate",
							showOnHover: true,
							disabledOn: () => this.requiredReadOnly,
						}),
					}
				);
			}
		}

		if (this.userConnected.rolePrestataire) {
			cols.push(
				{
					field: "volume",
					translateCode: "PRICING_BOOKING.VOLUME_UNIT",
					sortable: false,
					width: "100px",
				},
				{
					field: "dangerousGood",
					translateCode: "PRICING_BOOKING.DANGEROUS_GOOD",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: SelectGenericCell,
					genericCellParams: new SelectGenericCell.Params({
						field: "dangerousGood",
						showOnHover: true,
						disabledOn: () => this.readOnly,
						values: (this.keyValuePipe.transform(this.marchandiseDangerousGood) || []).map(
							(it) => ({
								key: +it.key,
								value: it.value,
							})
						),
						itemValueField: "key",
						itemTextField: "value",
						compareWith: this.compareWithForSelect,
					}),
				},
				{
					field: "un",
					translateCode: "PRICING_BOOKING.UN",
					sortable: false,
				},
				{
					field: "classGood",
					translateCode: "PRICING_BOOKING.CLASS",
					sortable: false,
				},
				{
					field: "packaging",
					translateCode: "PRICING_BOOKING.PACKAGING",
					sortable: false,
				},
				{
					field: "stackable",
					translateCode: "PRICING_BOOKING.STACKABLE",
					sortable: false,
				},
				{
					field: "comment",
					translateCode: "PRICING_BOOKING.COMMENTS",
					sortable: false,
				}
			);
		} else {
			cols.push(
				{
					field: "volume",
					translateCode: "PRICING_BOOKING.VOLUME_UNIT",
					sortable: false,
					width: "100px",
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "volume",
						showOnHover: true,
						pattern: Statique.patternDecimalNumber,
						disabledOn: () => this.requiredReadOnly,
					}),
					genericCellHandler: (
						key: string,
						value: any,
						model: EbMarchandise,
						context: GoodsInformationComponent
					) => {
						if (key === "input-event" && value === "blur") {
							context.calculateTotalWeight(this.ebDemande.listMarchandises.indexOf(model));
						}
					},
					render: (volume: number) => {
						return this.localNumberFormatPipe.transform(volume, "1.2-2");
					},
				},
				{
					field: "dangerousGood",
					translateCode: "PRICING_BOOKING.DANGEROUS_GOOD",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: SelectGenericCell,
					genericCellParams: new SelectGenericCell.Params({
						field: "dangerousGood",
						showOnHover: true,
						disabledOn: () => this.requiredReadOnly,
						values: (this.keyValuePipe.transform(this.marchandiseDangerousGood) || []).map(
							(it) => ({
								key: +it.key,
								value: it.value,
							})
						),
						itemValueField: "key",
						itemTextField: "value",
						compareWith: this.compareWithForSelect,
					}),
				},
				{
					field: "un",
					translateCode: "PRICING_BOOKING.UN",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "un",
						showOnHover: true,
						pattern: /[a-z0-9]{4}/,
						disabledOn: (model: EbMarchandise) => {
							return model.dangerousGood == 1 || this.readOnly;
						},
						requiredOn: (model: EbMarchandise) => {
							return [2, 3].includes(model.dangerousGood);
						},
					}),
				},
				{
					field: "classGood",
					translateCode: "PRICING_BOOKING.CLASS",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: SelectSimpleListGenericCellComponent,
					genericCellParams: new SelectSimpleListGenericCellComponent.Params({
						field: "classGood",
						showOnHover: true,
						values: this.marchandiseClassGood,
						disabledOn: (model: EbMarchandise) => {
							return model.dangerousGood == 1 || this.readOnly;
						},
						requiredOn: (model: EbMarchandise) => {
							return [2, 3].includes(model.dangerousGood);
						},
					}),
				},
				{
					field: "packaging",
					translateCode: "PRICING_BOOKING.PACKAGING",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "packaging",
						showOnHover: true,
						disabledOn: () => this.requiredReadOnly,
					}),
				},
				{
					field: "stackable",
					translateCode: "PRICING_BOOKING.STACKABLE",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: SelectGenericCell,
					genericCellParams: new SelectGenericCell.Params({
						field: "stackable",
						showOnHover: true,
						values: Statique.yesOrNoWithBooleanKey,
						itemValueField: "key",
						itemTextField: "value",
						disabledOn: () => this.requiredReadOnly,
						compareWith: Statique.compareByField,
					}),
				},
				{
					field: "comment",
					translateCode: "PRICING_BOOKING.COMMENTS",
					sortable: false,
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: TextInputGenericCell,
					genericCellParams: new TextInputGenericCell.Params({
						field: "comment",
						showOnHover: true,
						disabledOn: () => this.requiredReadOnly,
					}),
				}
			);
		}
		if (this.showTracings) {
			cols.push({
				translateCode: "TRANSPORT_MANAGEMENT.TRACING",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: ButtonGenericCell,
				genericCellParams: new ButtonGenericCell.Params({
					hideIfNoText: true,
					disabledOn: () => this.requiredReadOnly,
					textRender: (row: EbMarchandise) => {
						if (row.xEbTracing == null) return "";
						return `TRACK_${row.xEbTracing.ebTtTracingNum}`;
					},
				}),
			});
		}
		if (this.hasAccess(Modules.COMPTA_MATIERE)) {
			if (this.editting) {
				cols.push({
					field: "cptmRegimeDouanierLabel",
					translateCode: "CPTM.REGIME_TMP.CUSTOMS_REGIME",
					sortable: false,
					width: "130px",
				});
			} else {
				cols.push({
					field: "selectedRegimeDouanier",
					translateCode: "CPTM.REGIME_TMP.CUSTOMS_REGIME",
					sortable: false,
					width: "130px",
					renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
					genericCellClass: GoodsRegimeTempCell,
					genericCellParams: new GoodsRegimeTempCell.Params({
						field: "selectedRegimeDouanier",
						values: this.listRegimesDouaniers,
						showOnHover: true,
						disabledOn: () => this.requiredReadOnly,
					}),
					genericCellHandler: (
						key: string,
						value: any,
						model: EbMarchandise,
						context: GoodsInformationComponent
					) => {
						if (key === "selection-change") {
							context.customsRegimeChange(model);
						}
					},
				});
			}
		}
		if (this.ebDemande.xEcNature == NatureDemandeTransport.CONSOLIDATION) {
			cols.push({
				field: "refTransportInitial",
				translateCode: "PRICING_BOOKING.INTIAL_TRANSPORT",
				header: "Initial transport",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: ButtonGenericCell,
				genericCellParams: new ButtonGenericCell.Params({
					field: "parent",
					hideIfNoText: true,
					disabledOn: () => this.requiredReadOnly,
					textRender: (row: EbMarchandise) => {
						return row.refTransportInitial;
					},
				}),
				genericCellHandler: (
					key: string,
					value: any,
					model: EbMarchandise,
					context: GoodsInformationComponent
				) => {
					if (key === "click") {
						context.goToPricingDetails(model.xEbDemandeInitial);
					}
				},
			});
		}

		if (this.additionalCols && this.additionalCols.length > 0) {
			let $this = this;
			var additionalNames = this.additionalCols.map((c) => c.name);
			additionalNames.forEach((res) => {
				$this.customFieldsToSkip.push(res);
			});
			this.additionalCols.forEach((el) => {
				cols.push(el);
			});
		}

		this.dataInfos.cols = cols;

		this.dataInfos.selectedCols = this.dataInfos.cols;
	}

	refreshListUnits() {
		this.pageDatas = this.ebDemande.listMarchandises;
	}

	goToPricingDetails(ebDemandeNum: number) {
		this.router.navigate(["/app/pricing/details/", ebDemandeNum]);
	}

	updateUnitInfos() {
		let params = {};
		params["ebDemandeNum"] = this.ebDemande.ebDemandeNum;
		params["totalTaxableWeight"] = Statique.isDefined(this.ebDemande.totalTaxableWeight)
			? this.ebDemande.totalTaxableWeight
			: 0;
		params["totalWeight"] = Statique.isDefined(this.ebDemande.totalWeight)
			? this.ebDemande.totalWeight
			: 0;
		params["totalVolume"] = Statique.isDefined(this.ebDemande.totalVolume)
			? this.ebDemande.totalVolume
			: 0;
		let $this = this;
		this.modalService.confirm(
			$this.translate.instant("GENERAL.CONFIRMATION"),
			$this.translate.instant("PRICING_BOOKING.UPDATE_UNIT_WARNING"),
			function() {
				$this.pricingService
					.updateListMarchandise($this.ebDemande.listMarchandises, params)
					.subscribe(
						(res) => {
							let ebChat = res.ebChat;
							if (Statique.isDefined(ebChat)) $this.updateChatEmitter.emit(ebChat);
							$this.successfullOperation();
						},
						(err) => {
							$this.echecOperation();
						}
					);
			},
			function() {},
			true
		);
	}

	echecOperation() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}

	successfullOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
}
