import { Component, OnInit } from "@angular/core";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";
import { EbMarchandise } from "@app/classes/marchandise";
import { GoodsInformationComponent } from "../goods-information.component";
import EbCptmRegimeTemporaireDTO from "@app/classes/cptm/EbCptmRegimeTemporaireDTO";

@Component({
	selector: "app-goods-regime-temp-cell",
	templateUrl: "./cell-goods-regime-temp.component.html",
})
export class GoodsRegimeTempCell
	extends GenericCell<EbMarchandise, GoodsRegimeTempCell.Params, GoodsInformationComponent>
	implements OnInit {
	field: string;
	values: Array<EbCptmRegimeTemporaireDTO>;
	showOnHover;
	labelForSearch;
	constructor() {
		super();
	}

	ngOnInit() {
		this.field = this.params.field != null ? this.params.field : "";
		this.values = this.params.values != null ? this.params.values : [];
	}
}

export namespace GoodsRegimeTempCell {
	export class Params extends GenericCell.Params {
		field: string;
		values: Array<EbCptmRegimeTemporaireDTO>;
	}
}
