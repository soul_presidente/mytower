import { Component, OnInit } from "@angular/core";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-unit-children-cell-select",
	templateUrl: "./unit-children-select.component.html",
	styleUrls: ["./unit-children-select.component.scss"],
})
export class UnitChildrenCell extends GenericCell<any, UnitChildrenCell.Params, any>
	implements OnInit {
	field: string;
	itemValueField: string;
	itemTextField: string;
	showOnHover: boolean;
	multiple: boolean;
	compareWith: (o1: any, o2: any) => boolean;

	constructor(protected translate?: TranslateService) {
		super();
	}

	ngOnInit() {
		this.field = this.params.field != null ? this.params.field : "";
		this.itemValueField = this.params.itemValueField != null ? this.params.itemValueField : null;
		this.itemTextField = this.params.itemTextField != null ? this.params.itemTextField : null;
		this.showOnHover = this.params.showOnHover != null ? this.params.showOnHover : false;

		this.multiple = this.params.multiple;
		this.compareWith =
			this.params.compareWith != null
				? this.params.compareWith
				: (a, b) => {
						if (this.itemValueField && a != null && b != null) {
							return a[this.itemValueField] == b[this.itemValueField];
						} else {
							return a == b;
						}
				  };
	}

	get required(): boolean {
		if (this.params.requiredOn) {
			return this.params.requiredOn(this.model);
		}
		return this.params.required != null ? this.params.required : false;
	}

	get disabled(): boolean {
		if (this.params.disabledOn) {
			return this.params.disabledOn(this.model);
		}
		return this.params.disabled != null ? this.params.disabled : false;
	}
	get values(): Array<any> {
		if (this.params.valuesOn) {
			return this.params.valuesOn(this.model);
		}
		return this.params.values != null ? this.params.values : [];
	}
}

export namespace UnitChildrenCell {
	export class Params extends GenericCell.Params {
		field: string;
		values?: Array<any>;
		valuesOn?: (model: any) => Array<any>;
		itemValueField: string;
		itemTextField: string;
		showOnHover: boolean;
		required?: boolean;
		requiredOn?: (model: any) => boolean;
		disabled?: boolean;
		disabledOn?: (model: any) => boolean;
		multiple?: boolean;
		compareWith: (o1: any, o2: any) => boolean;
	}
}
