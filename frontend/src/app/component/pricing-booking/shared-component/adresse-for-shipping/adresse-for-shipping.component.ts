import {
	ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ElementRef,
	OnChanges,
	ViewChild,
} from "@angular/core";
import { PricingMaskFillerComponent } from "@app/component/saved-forms/pricing-mask/pricing-mask-filler.component";
import { EbParty } from "@app/classes/party";
import { EbUser } from "@app/classes/user";
import {
	AdresseEligibility,
	ModeTransport,
	UserRole,
	Modules,
	IDChatComponent,
} from "@app/utils/enumeration";
import { EcCountry } from "@app/classes/country";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { Statique } from "@app/utils/statique";
import { EbAdresse } from "@app/classes/adresse";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbCompagnie } from "@app/classes/compagnie";
import { EbEtablissement } from "@app/classes/etablissement";
import { CompleterItem, CompleterService } from "ng2-completer";
import { PricingService } from "@app/services/pricing.service";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { DialogService } from "@app/services/dialog.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { CompagnieService } from "@app/services/compagnie.service";
import { TrackTraceService } from "@app/services/trackTrace.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { DockManagementService } from "@app/services/dock-management.service";
import { UserService } from "@app/services/user.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { StatiqueService } from "@app/services/statique.service";
import { ActivatedRoute, Router } from "@angular/router";
import { CollectionService } from "@app/services/collection.service";
import { TranslateService } from "@ngx-translate/core";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbDemande } from "@app/classes/demande";
import { Subject } from "rxjs";
import { MessageService } from "primeng/api";
import { EbChat } from "@app/classes/chat";
import { SearchCriteriaPlanTransport } from "@app/utils/searchCriteriaPlanTransport";
import { StepPricingComponent } from "../creation-form/components/step-pricing/step-pricing.component";
import { EbMarchandise } from "@app/classes/marchandise";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { SearchCriteriaQualityManagement } from "@app/utils/SearchCriteriaQualityManagement";

@Component({
	selector: "adresse-for-shipping",
	templateUrl: "./adresse-for-shipping.component.html",
	styleUrls: ["./adresse-for-shipping.component.scss"],
})
export class AdresseForShippingComponent extends PricingMaskFillerComponent
	implements OnInit, OnChanges {
	searchOrigin: string;
	@Input()
	hasContributionAccess: number;
	@Input("module")
	ebModuleNum: number;
	@Input()
	ebParty: EbParty; // peut etre les differentes EbParty a envoyé depuis le composant parent

	@Output()
	onValidate = new EventEmitter<any>(); //Pour envoyer EbParty au composant parent

	@Input()
	listCountry: Array<EcCountry>;
	@Input()
	listZone: Array<EbPlZoneDTO>;
	@Input()
	listEtablissementPlateform: Array<EbEtablissement>;
	@Input()
	showOriginInfoWrapper: boolean;
	@Input()
	showAddressWrapper: boolean;
	@Input()
	listMarchandises: any;

	UserRole = UserRole;
	listAdresse: Array<EbAdresse> = new Array<EbAdresse>();
	Statique = Statique;
	adresseToSave: EbAdresse;
	adresseToSaveSection: string;
	ModeTransportEnum = ModeTransport;
	searchCriteria: SearchCriteria = new SearchCriteria();
	element: Element;

	typeaheadEtabEvent: EventEmitter<string> = new EventEmitter<string>();

	selectedUser: EbUser;
	typeaheadUserEvent: EventEmitter<string> = new EventEmitter<string>();
	toggleEditData: boolean = false;
	oldParty: EbParty;
	searchNotify: string;
	listAdressesAsParty = new Array<EbParty>();
	searchAdress: string;
	@Input()
	showAddressFields: boolean = false;
	DestFields: boolean = false;
	showDestInfoWrapper: boolean = false;
	showNotifyFields: boolean = true;
	showAddAddressBtn: boolean = false;
	showEditRemoveIcons: boolean = false;
	showEditAddress: boolean = false;
	searchAddressValue: Subject<string> = new Subject();

	// showAddressPopup: boolean = false;

	@Output()
	showAddressPopup = new EventEmitter();

	@Output()
	suggestionsEtablissement: EventEmitter<Object> = new EventEmitter<Object>();

	@Output()
	removeAddressEmit: EventEmitter<Object> = new EventEmitter<Object>();

	@Output()
	addAddressEmit: EventEmitter<Object> = new EventEmitter<Object>();

	@Output()
	ebPartyEmit: EventEmitter<EbParty> = new EventEmitter<EbParty>();

	@Output()
	saveAddressEmit = new EventEmitter();

	inputBlur: boolean = false;

	showListZoneDialog: boolean = false;
	listZonesAdresse: Array<EbPlZoneDTO>;
	defaultZone: EbPlZoneDTO;
	module: number;

	listTransporteur: Array<EbUser> = [];
	loading = true;
	displayMessage: string = null;
	Modules = Modules;
	fromToLabel: string;

	selectedDemandeTransporteur: ExEbDemandeTransporteur;
	ecCountryNum: number = null;
	city: String = null;
	zipCode: String = null;
	ebZoneNum: number = null;

	@Input()
	isEditMode: boolean = false;
	@Input()
	readOnly: boolean = true;

	keyUpsearch = new Subject<string>();

	constructor(
		private pricingService: PricingService,
		private completerService: CompleterService,
		protected savedFormsService: SavedFormsService,
		protected dialogService: DialogService,
		private etablissementService: EtablissementService,
		private compagnieService: CompagnieService,
		private trackTraceService: TrackTraceService,
		private transportationPlanService: TransportationPlanService,
		protected dockManagementService: DockManagementService,
		private userService: UserService,
		protected modalService: ModalService,
		protected cd: ChangeDetectorRef,
		protected statiqueService: StatiqueService,
		protected activatedRoute: ActivatedRoute,
		private collectionService: CollectionService,
		protected translate: TranslateService,
		elementRef: ElementRef,
		protected router: Router,
		private messageService?: MessageService
	) {
		super(savedFormsService, dialogService, modalService, translate);

		this.keyUpsearch
			.pipe(
				debounceTime(1000),
				distinctUntilChanged()
			)
			.subscribe((value) => {
				this.setSuggestionsEtablissement(value, this.fromOrTo);
			});
	}

	ngOnInit() {
		this.getFromTo();
		this.module = Statique.getModuleNumFromUrl(this.router.url);

		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteria.setUser(this.userConnected);
		this.etablissementService.getListCurrencyForDemande(this.searchCriteria).subscribe(
			function(data) {
				if (data) {
					this.listCurrencyDemande = data;
				}
			}.bind(this)
		);

		if (this.ebDemande && this.ebDemande.user && this.ebParty && this.ebParty.ebPartyNum) {
			this.defaultZone = this.ebParty.zone;
			this.transportationPlanService
				.listZoneAdresse(this.ebParty.reference, this.ebDemande.user.ebCompagnie.ebCompagnieNum)
				.subscribe((data) => {
					this.listZonesAdresse = data as Array<EbPlZoneDTO>;
					if (this.listZonesAdresse != null) {
						this.listZonesAdresse.forEach((zone) => {
							zone["label"] = zone.designation
								? zone.ref + " (" + zone.designation + ")"
								: zone.ref;
						});
					}
				});
		}
	}

	ngOnChanges() {
		//console.log(this.ebParty);
	}

	getFromTo() {
		if (this.fromOrTo == "From") {
			this.fromToLabel = this.translate.instant("PRICING_BOOKING.ADDRESS_FROM");
		} else if (this.fromOrTo == "To") {
			this.fromToLabel = this.translate.instant("PRICING_BOOKING.ADDRESS_TO");
		}
	}

	//recherche eBparty avec auto completion
	setSuggestionsEtablissement(value, fromOrTo) {
		let compagnieNum: number;
		let etabNum: number;
		let userNum: number;

		if (
			this.ebDemande.user &&
			this.ebDemande.user.ebCompagnie &&
			this.ebDemande.user.ebCompagnie.ebCompagnieNum
		) {
			compagnieNum = this.ebDemande.user.ebCompagnie.ebCompagnieNum;
			userNum = this.ebDemande.user.ebUserNum;
			if (!this.ebDemande.user.superAdmin) {
				etabNum = this.ebDemande.user.ebEtablissement.ebEtablissementNum;
			}
		} else {
			userNum = this.userConnected.ebUserNum;
			compagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			if (!this.userConnected.superAdmin) {
				etabNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			}
		}
		let pi = fromOrTo == "Intermediate point" ? AdresseEligibility.INTERMEDIATE_POINT : null;
		this.pricingService
			.searchAdresseAsParty(
				value,
				compagnieNum,
				etabNum,
				pi,
				userNum,
				this.ecCountryNum,
				this.city,
				this.zipCode,
				this.ebZoneNum
			)
			.subscribe((res) => {
				if (res) this.suggestionsEtablissementEmit(res.data, fromOrTo, value, res.count);
			});
	}

	/* Cette fontion est appelée lors de la selection de l'adresse , elle envoie l'adresse choisie au composant
  parent et change le bloc en mode consultation*/
	selectedEbParty(selected: CompleterItem) {
		if (selected === null) {
			this.ebParty.ebPartyNum = null;
		} else {
			this.ebParty = selected.originalObject;
			this.onValidate.emit(this.ebParty);
			this.showAddressWrapper = true;
			this.showAddAddressBtn = false;
		}
	}

	/* Cette fonction verifie si les champs city et country de EbParty sont renseignés et envoie dans ce cas l'adresse
  choisie au composant parent*/
	onValideFields() {
		if (this.ebParty.city != null && this.ebParty.xEcCountry != null)
			this.onValidate.emit(this.ebParty);
	}

	private getZoneBackupDisplayString(party?: EbParty): string {
		if (party) {
			if (party.zoneDesignation) {
				return party.zoneRef + " (" + party.zoneDesignation + ")";
			} else {
				return party.zoneRef;
			}
		} else {
			return "";
		}
	}

	private getZoneDisplayString(zone?: EbPlZoneDTO): string {
		if (zone) {
			if (zone.designation) {
				return zone.ref + " (" + zone.designation + ")";
			} else {
				return zone.ref;
			}
		} else {
			return "";
		}
	}

	private getCountryDisplayString(party?: EbParty): string {
		if (party) {
			if (party.xEcCountry) {
				return party.xEcCountry.libelle;
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	compareEtablissement(etablissement1: EbEtablissement, etablissement2: EbEtablissement) {
		if (etablissement1 && etablissement2) {
			return etablissement1.ebEtablissementNum === etablissement2.ebEtablissementNum;
		}
	}

	compareCountry(country1: EcCountry, country2: EcCountry) {
		if (country2 !== undefined && country2 !== null) {
			return country1.ecCountryNum === country2.ecCountryNum;
		}
	}

	compareZone(zone1: EbPlZoneDTO, zone2: EbPlZoneDTO) {
		if (zone1 && zone2) {
			return zone1.ebZoneNum === zone2.ebZoneNum;
		}
	}

	cancelAddAdress(event: any) {
		this.adresseToSave = null;
		this.adresseToSaveSection = "";
	}

	validateAddAdress(event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.adresseToSave.etablissementAdresse = new Array<EbEtablissement>();
		let etablissement: EbEtablissement = new EbEtablissement();
		etablissement.ebEtablissementNum = this.ebDemande.user.ebEtablissement.ebEtablissementNum;
		this.adresseToSave.etablissementAdresse.push(etablissement);

		this.adresseToSave.ebCompagnie = new EbCompagnie();
		this.adresseToSave.ebCompagnie.ebCompagnieNum = this.ebDemande.user.ebCompagnie.ebCompagnieNum;

		this.adresseToSave.createdBy = new EbUser();
		this.adresseToSave.createdBy.ebUserNum = this.userConnected.ebUserNum;

		this.etablissementService.addAdresseEtablisement(this.adresseToSave).subscribe(
			function(data) {
				requestProcessing.afterGetResponse(event);
				this.adresseToSave.ebAdresseNum = data.ebAdresseNum;

				var ebPartyToApply: EbParty;
				if (this.adresseToSaveSection === "ORIGIN") {
					ebPartyToApply = this.ebDemande.ebPartyOrigin;
				}
				if (this.adresseToSaveSection === "DESTINATION") {
					ebPartyToApply = this.ebDemande.ebPartyDest;
				}
				if (this.adresseToSaveSection === "NOTIFY") {
					ebPartyToApply = this.ebDemande.ebPartyNotif;
				}

				ebPartyToApply.adresse = this.adresseToSave.street;
				ebPartyToApply.xEcCountry = this.adresseToSave.country;
				ebPartyToApply.city = this.adresseToSave.city;
				ebPartyToApply.company = this.adresseToSave.company;
				ebPartyToApply.email = this.adresseToSave.email;
				ebPartyToApply.phone = this.adresseToSave.phone;
				ebPartyToApply.zipCode = this.adresseToSave.zipCode;
				ebPartyToApply.airport = this.adresseToSave.airport;
				ebPartyToApply.openingHours = this.adresseToSave.openingHoursFreeText;
				ebPartyToApply.commentaire = this.adresseToSave.commentaire;

				this.adresseToSaveSection = "";
				this.adresseToSave = null;
			}.bind(this)
		);
	}

	addAddress(showAddressFields, ebParty, fromOrTo) {
		if (ebParty.city && ebParty.xEcCountry) {
			this.showAddressWrapper = true;
		} else {
			this.showAddressWrapper = false;
		}
		this.showAddressFields = true;
		this.showAddressFieldsEmit(this.showAddressFields, ebParty, fromOrTo);
	}

	showAddressFieldsEmit(showAddressFields, ebParty, fromOrTo) {
		ebParty = this.ebParty;
		this.addAddressEmit.emit({ showAddressFields, ebParty, fromOrTo });
	}
	removeAddress(fromOrTo) {
		this.showAddressFields = false;
		this.showAddressWrapper = false;
		if (fromOrTo == "From") {
			this.ebDemande.ebPartyOrigin = new EbParty();
		} else if (fromOrTo == "To") {
			this.ebDemande.ebPartyDest = new EbParty();
		}
		this.removeAddressEmit.emit(this.removeAddress);
	}

	saveAddress(ebParty) {
		this.ebParty = ebParty;
		if (
			this.ebParty.ebPartyNum == null &&
			this.ebParty.adresse == null &&
			this.ebParty.company == null &&
			this.ebParty.xEcCountry == null
		) {
			this.showAddressWrapper = false;
		} else {
			this.showAddressWrapper = true;
			this.showAddAddressBtn = false;
		}
		this.showAddressFields = false;
	}

	openSaveAdresseModel(section: string) {
		this.adresseToSaveSection = section;

		this.adresseToSave = new EbAdresse();

		var ebPartyToCopy: EbParty;
		if (this.adresseToSaveSection === "ORIGIN") {
			ebPartyToCopy = this.ebDemande.ebPartyOrigin;
		}
		if (this.adresseToSaveSection === "DESTINATION") {
			ebPartyToCopy = this.ebDemande.ebPartyDest;
		}
		if (this.adresseToSaveSection === "NOTIFY") {
			ebPartyToCopy = this.ebDemande.ebPartyNotif;
		}

		this.adresseToSave.airport = ebPartyToCopy.airport;
		this.adresseToSave.street = ebPartyToCopy.adresse;
		this.adresseToSave.country = ebPartyToCopy.xEcCountry;
		this.adresseToSave.city = ebPartyToCopy.city;
		this.adresseToSave.company = ebPartyToCopy.company;
		this.adresseToSave.email = ebPartyToCopy.email;
		this.adresseToSave.phone = ebPartyToCopy.phone;
		this.adresseToSave.zipCode = ebPartyToCopy.zipCode;
		this.adresseToSave.openingHoursFreeText = ebPartyToCopy.openingHours;
		this.adresseToSave.commentaire = ebPartyToCopy.commentaire;
		this.adresseToSave.defaultZone = ebPartyToCopy.zone;
		if (ebPartyToCopy.zone && ebPartyToCopy.zone.ebZoneNum) {
			this.adresseToSave.zones = new Array<EbPlZoneDTO>();
			this.adresseToSave.zones.push(ebPartyToCopy.zone);
		}
		this.adresseToSave.xEbUserVisibility = EbUser.createUser(ebPartyToCopy.xEbUserVisibility);

		this.adresseToSave.etablissementAdresse = new Array<EbEtablissement>();
		this.adresseToSave.etablissementAdresse.push(ebPartyToCopy.etablissementAdresse);
	}

	showAddressPopupEmit(showAddressPopup, fromOrTo) {
		this.showAddressPopup.emit({ showAddressPopup, fromOrTo });
	}

	suggestionsEtablissementEmit(
		data: EbParty,
		fromOrTo: string,
		searchInput: string,
		count: number
	) {
		this.suggestionsEtablissement.emit({ data, fromOrTo, searchInput, count });
	}

	hideFields() {
		this.showAddAddressBtn = false;
	}

	hideEditRemoveIcons() {
		this.showEditRemoveIcons = false;
		this.showEditAddress = false;
	}

	setInputBlur() {
		this.inputBlur = true;
	}

	showEdit() {
		this.showEditRemoveIcons = true;
		this.showEditAddress = false;
		if (this.hasContribution(Modules.TRANSPORT_MANAGEMENT)) this.showEditAddress = true;
	}

	hideEdit() {
		this.showEditRemoveIcons = false;
		this.showEditAddress = false;
	}

	cancelEditZone() {
		this.showListZoneDialog = false;
	}

	confirmeEditZone(data: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(data.clickEvent);

		let selectedDemandeTransporteur: ExEbDemandeTransporteur = data.selectedDemandeTransporteur as ExEbDemandeTransporteur;
		this.defaultZone = data.defaultZone as EbPlZoneDTO;
		let oldZoneLabel = this.ebParty.zone
			? this.ebParty.zone.designation
				? this.ebParty.zone.ref + " (" + this.ebParty.zone.designation + ")"
				: this.ebParty.zone.ref
			: null;

		let dfz: EbPlZoneDTO = new EbPlZoneDTO();
		dfz.ebZoneNum = this.defaultZone.ebZoneNum;
		dfz.designation = this.defaultZone.designation;
		dfz.ref = this.defaultZone.ref;

		selectedDemandeTransporteur.xEbDemande = new EbDemande();
		selectedDemandeTransporteur.xEbDemande.ebDemandeNum = this.ebDemande.ebDemandeNum;

		this.pricingService
			.updateTransportPlan(
				this.ebDemande.ebDemandeNum,
				this.ebParty.ebPartyNum,
				oldZoneLabel,
				this.defaultZone.ebZoneNum,
				selectedDemandeTransporteur.refPlan,
				selectedDemandeTransporteur.xTransporteur.calculatedPrice
			)
			.subscribe(
				(response) => {
					this.ebParty.zone = this.defaultZone;
					this.showListZoneDialog = false;
					this.successfulOperation();
					// Force navigation to refresh the page after a save
					let url = this.router.url;
					this.router
						.navigateByUrl("/accueil", { skipLocationChange: true })
						.then(() => this.router.navigate([url]));

					requestProcessing.afterGetResponse(data.clickEvent);
				},
				(err) => {
					this.showListZoneDialog = false;
					this.echecOperation();
				}
			);
	}

	successfulOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	echecOperation() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}
}
