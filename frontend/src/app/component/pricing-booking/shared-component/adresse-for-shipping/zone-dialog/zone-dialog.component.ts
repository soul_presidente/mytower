import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { Statique } from "@app/utils/statique";
import { EbDemande } from "@app/classes/demande";
import { EbEtablissement } from "@app/classes/etablissement";
import { EbCompagnie } from "@app/classes/compagnie";
import { SearchCriteriaPlanTransport } from "@app/utils/searchCriteriaPlanTransport";
import { EbUser } from "@app/classes/user";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { TranslateService } from "@ngx-translate/core";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { CarrierStatus } from "@app/utils/enumeration";
import { EbMarchandise } from "@app/classes/marchandise";
import { EbParty } from "@app/classes/party";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { DetailUnitDTO } from "@app/classes/trpl/DetailUnitDTO";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";

@Component({
	selector: "app-zone-dialog",
	templateUrl: "./zone-dialog.component.html",
	styleUrls: ["./zone-dialog.component.scss"],
})
export class ZoneDialogComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	listZonesAdresse: Array<EbPlZoneDTO>;

	@Input()
	defaultZone: EbPlZoneDTO;

	@Input()
	demande: EbDemande;

	@Input()
	userConnected: EbUser;

	@Input()
	fromOrTo: string;

	@Input()
	listMarchandises: Array<EbMarchandise>;

	@Input()
	ebParty: EbParty;

	@Output()
	closeDialog: EventEmitter<any> = new EventEmitter<any>();

	@Output()
	confirmeEdit: EventEmitter<any> = new EventEmitter();

	listTransporteur: Array<EbUser> = [];
	loading = true;
	displayMessage: string = null;
	listDemandeQuote: Array<ExEbDemandeTransporteur> = [];
	Statique = Statique;
	display: boolean = true;
	selectedDemandeTransporteur: ExEbDemandeTransporteur;

	constructor(
		private transportationPlanService: TransportationPlanService,
		protected translate: TranslateService
	) {
		super();
	}

	ngOnInit() {}

	cancel(event) {
		this.closeDialog.emit(event);
	}

	confirme(event) {
		let data: any = {
			defaultZone: this.defaultZone,
			selectedDemandeTransporteur: this.selectedDemandeTransporteur,
			clickEvent: event,
		};
		this.confirmeEdit.emit(data);
	}

	getListTransportPlan() {
		if (
			((this.fromOrTo == "From" && this.demande.ebPartyDest.zone) || // if it's From, zone dest must exist
				(this.fromOrTo == "To" && this.demande.ebPartyOrigin.zone)) && // if it's To, zone origin must exist
			(!this.ebParty.zone ||
				(this.defaultZone &&
					this.defaultZone.ebZoneNum &&
					this.defaultZone.ebZoneNum != this.ebParty.zone.ebZoneNum))
		)
			this.loadTransportPlanToTransporteur();
		else {
			this.listDemandeQuote = [];
			this.selectedDemandeTransporteur = null;
		}
	}

	private loadTransportPlanToTransporteur() {
		if (!this.demande.xecCurrencyInvoice || !this.listMarchandises) {
			return;
		}
		let dgMar = 0;
		let typeGoodsNum = null;
		const searchCriteriaPl = new SearchCriteriaPlanTransport();

		for (let m of this.listMarchandises) {
			if (m.dangerousGood != null && dgMar < m.dangerousGood) {
				dgMar = m.dangerousGood;
			}

			// Type Of Goods
			if (m.typeGoodsNum != null) {
				if (typeGoodsNum != null) {
					typeGoodsNum = -1; // Fake id to not return results of 2 type of goods by demande
				} else {
					typeGoodsNum = m.typeGoodsNum;
				}
			}

			let detailUnit = new DetailUnitDTO();
			detailUnit.weight = m.weight;
			detailUnit.volume = m.volume;
			detailUnit.nombre = m.numberOfUnits;
			detailUnit.ebTypeUnitNum = m.xEbTypeUnit;
			searchCriteriaPl.listUnit.push(detailUnit);
		}

		if (this.fromOrTo == "From") {
			searchCriteriaPl.ebZoneNumOrigin = this.defaultZone.ebZoneNum;
			if (this.demande.ebPartyDest.zone && this.demande.ebPartyDest.zone.ebZoneNum)
				searchCriteriaPl.ebZoneNumDest = this.demande.ebPartyDest.zone.ebZoneNum;
		} else if (this.fromOrTo == "To") {
			if (this.demande.ebPartyOrigin.zone && this.demande.ebPartyOrigin.zone.ebZoneNum)
				searchCriteriaPl.ebZoneNumOrigin = this.demande.ebPartyOrigin.zone.ebZoneNum;

			searchCriteriaPl.ebZoneNumDest = this.defaultZone.ebZoneNum;
		}

		// searchCriteriaPl.isDangerous = false; //TODO
		if (dgMar > 1) {
			searchCriteriaPl.dangerous = true;
		} else if (dgMar == 1) {
			searchCriteriaPl.dangerous = false;
		} else {
			searchCriteriaPl.dangerous = null;
		}
		searchCriteriaPl.modeTransport = this.demande.xEcModeTransport;
		searchCriteriaPl.typeDemande = this.demande.xEcTypeDemande;
		searchCriteriaPl.totalWeight =
			(this.demande.totalTaxableWeight as any) === "" ? null : this.demande.totalTaxableWeight;

		searchCriteriaPl.totalUnit =
			(this.demande.totalNbrParcel as any) === "" ? null : this.demande.totalNbrParcel;

		searchCriteriaPl.totalVolume =
			(this.demande.totalVolume as any) === "" ? null : this.demande.totalVolume;

		if (this.demande.xecCurrencyInvoice) {
			searchCriteriaPl.currencyCibleNum = this.demande.xecCurrencyInvoice.ecCurrencyNum;
		}

		searchCriteriaPl.typeGoodsNum = typeGoodsNum;

		if (this.demande.user.ebCompagnie)
			searchCriteriaPl.ebCompagnieNum = this.demande.user.ebCompagnie.ebCompagnieNum;
		if (this.demande.user.ebEtablissement)
			searchCriteriaPl.ebEtablissementNum = this.demande.user.ebEtablissement.ebEtablissementNum;

		//on recupére le premier type of unit saisi
		if (this.listMarchandises) {
			for (let marchd of this.listMarchandises) {
				if (marchd.xEbTypeUnit) {
					searchCriteriaPl.ebTypeUnitNum = marchd.xEbTypeUnit;
					break;
				}
			}
		}

		if (this.isTransporteur || this.isTransporteurBroker)
			searchCriteriaPl.ebTransporteurNum = this.userConnected.ebUserNum;
		else if (this.isChargeur || this.isControlTower) {
			searchCriteriaPl.ebTransporteurNum = this.demande.xEbTransporteur;
		}

		this.transportationPlanService.searchPricingTransport(searchCriteriaPl).subscribe(
			(listFromSearch) => {
				let listTranspPlan: Array<EbUser> = [];
				for (let it of listFromSearch as Array<any>) {
					if (it.calculatedPrice) {
						let trUser = new EbUser();
						trUser.ebUserNum = it.carrierUserNum;
						trUser.nom = it.carrierName;
						trUser.email = it.carrierEmail;
						trUser.calculatedPrice = it.calculatedPrice;
						trUser.transitTime = it.transitTime;
						trUser.coment = it.comment;

						if (it.dangerous != null) {
							if (it.dangerous === true) {
								it.comment +=
									" (" + this.translate.instant("PRICING_BOOKING.PLAN_TRANSPORT_DANGEROUS") + ")";
							} else if (it.dangerous === false) {
								it.comment +=
									" (" +
									this.translate.instant("PRICING_BOOKING.PLAN_TRANSPORT_NOT_DANGEROUS") +
									")";
							}
						}

						trUser.refPlan = it.planRef;
						trUser.refGrille = it.refGrille;
						trUser.isFromTransPlan = true;
						trUser.exchangeRateFound = it.exchangeRateFound;

						if (it.carrierEtabNum) {
							trUser.ebEtablissement = new EbEtablissement();
							trUser.ebEtablissement.ebEtablissementNum = it.carrierEtabNum;
							trUser.ebEtablissement.nom = it.carrierEtabName;
							if (it.carrierCompanyNum) {
								trUser.ebEtablissement.ebCompagnie = new EbCompagnie();
								trUser.ebEtablissement.ebCompagnie.ebCompagnieNum = it.carrierCompanyNum;
								trUser.ebEtablissement.ebCompagnie.nom = it.carrierCompanyNum;
							} else trUser.ebEtablissement.ebCompagnie = null;
						} else trUser.ebEtablissement = null;
						listTranspPlan.push(trUser);
					}
				}

				this.listDemandeQuote = this.getListDemandeQuote(listTranspPlan);
			},
			(error) => {
				console.error(error);
			}
		);
	}

	setCarrierData(quote: ExEbDemandeTransporteur, user: EbUser) {
		quote.xTransporteur = new EbUser();
		quote.xTransporteur.ebUserNum = user.ebUserNum;
		quote.xTransporteur.nom = user.nom;
		quote.xTransporteur.prenom = user.prenom;
		quote.xTransporteur.email = user.email;

		quote.xEbEtablissement = new EbEtablissement();
		quote.xEbEtablissement.ebEtablissementNum = user.ebEtablissement.ebEtablissementNum;
		quote.xEbEtablissement.nom = user.ebEtablissement.nom;

		quote.xEbCompagnie = new EbCompagnie();
		if (user.ebCompagnie) {
			quote.xEbCompagnie.ebCompagnieNum = user.ebCompagnie.ebCompagnieNum;
			quote.xEbCompagnie.nom = user.ebCompagnie.nom;
			quote.xEbCompagnie.code = user.ebCompagnie.code;
		} else if (user.ebEtablissement && user.ebEtablissement.ebCompagnie) {
			quote.xEbCompagnie.ebCompagnieNum = user.ebEtablissement.ebCompagnie.ebCompagnieNum;
			quote.xEbCompagnie.nom = user.ebEtablissement.ebCompagnie.nom;
			quote.xEbCompagnie.code = user.ebEtablissement.ebCompagnie.code;
		}
	}

	getListDemandeQuote(listTr: Array<EbUser>): Array<ExEbDemandeTransporteur> {
		let listDemandeQuote = listTr
			? listTr.map((tr) => {
					let dq = new ExEbDemandeTransporteur();

					this.setCarrierData(dq, tr);

					dq.xTransporteur.calculatedPrice = tr.calculatedPrice;
					dq.price = tr.calculatedPrice;
					dq.transitTime = tr.transitTime;
					dq.comment = tr.coment;
					dq.isFromTransPlan = tr.isFromTransPlan;
					dq.exchangeRateFound = tr.exchangeRateFound;
					dq.refGrille = tr.refGrille;
					dq.refPlan = tr.refPlan;
					dq.status = CarrierStatus.REQUEST_RECEIVED;
					return Statique.cloneObject(dq, new ExEbDemandeTransporteur());
			  })
			: null;

		return listDemandeQuote;
	}
}
