import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { EbDemande } from "@app/classes/demande";
import { Statique } from "@app/utils/statique";
import { UserRole } from "@app/utils/enumeration";
import { PricingService } from "@app/services/pricing.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbUser } from "@app/classes/user";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { EbChat } from "@app/classes/chat";
import { UserService } from "@app/services/user.service";
import { EbAdresse } from "@app/classes/adresse";
import { EtablissementService } from "@app/services/etablissement.service";
import { MessageService } from "primeng/api";

@Component({
	selector: "app-external-users",
	templateUrl: "./external-users.component.html",
	styleUrls: ["./external-users.component.css"],
})
export class ExternalUsersComponent extends ConnectedUserComponent implements OnInit {
	UserRole = UserRole;
	statique = Statique;
	listUser: Array<EbUser> = new Array();
	@Input()
	module: number;
	@Input()
	ebDemande: EbDemande;
	@Input()
	disabled: boolean = false;
	@Output()
	updateChatEmitter = new EventEmitter<EbChat>();

	toggleEditData: boolean = false;

	criteria: SearchCriteria = new SearchCriteria();
	criteriaCustomsBroker: SearchCriteria = new SearchCriteria();
	listObserver: EbUser[] = [];
	listUserEmail: EbUser[] = [];

	//Adding or deleting spaces depending on language
	space: string = "";

	constructor(
		private pricingService: PricingService,
		private modalService: ModalService,
		private translate: TranslateService,
		private userService: UserService,
		protected etablissementService: EtablissementService,
		protected messageService: MessageService
	) {
		super();
	}

	ngOnInit() {
		this.getListObservers();
		if (this.userConnected.language == "fr") this.space = " ";

		let user = new EbUser();
		if (!this.ebDemande.user) {
			user.constructorCopyLite(this.userConnected);
		} else {
			user.constructorCopyLite(this.ebDemande.user);
		}

		if (user) {
			this.criteria.ebCompagnieNum = user.ebCompagnie.ebCompagnieNum;
			this.criteria.roleChargeur = true;

			// this.criteriaCustomsBroker.ebCompagnieNum = user.ebCompagnie.ebCompagnieNum;
			this.criteriaCustomsBroker.fromCommunity = true;
			this.criteriaCustomsBroker.roleBroker = true;
			this.refreshListObserver();
		}
	}

	nextPage() {}

	isPageValid() {}
	getListObservers() {
		this.userService.getListObservers(this.ebDemande).subscribe((data) => {
			this.listUser = data;
			data.forEach((dt) => {
				this.listUser = dt;
			});
			this.listUser && this.listUser.forEach((user) => {
				this.listUserEmail.push(user);
			});
			this.ebDemande.xEbUserObserver = this.listUser;
		});
	}

	displayPopupUpdateAdresse($event) {
		if ($event) {
			let $this = this;
			this.modalService.confirm(
				this.translate.instant("REPOSITORY_MANAGEMENT.HEADER_UPDATE_USER_ADRESS"),
				this.translate.instant("REPOSITORY_MANAGEMENT.MESSAGE_UPDATE_USER_ADRESS"),
				function() {
					let adresseToSave = new EbAdresse();
					if ($this.ebDemande.ebPartyOrigin) {
						$this.ebDemande.ebPartyOrigin.xEbUserVisibility = $event;
						adresseToSave.ebPartyToEbAdress($this.ebDemande.ebPartyOrigin, $this.userConnected);
					}
					if ($this.ebDemande.ebPartyDest) {
						$this.ebDemande.ebPartyDest.xEbUserVisibility = $event;
						adresseToSave.ebPartyToEbAdress($this.ebDemande.ebPartyDest, $this.userConnected);
					}
					adresseToSave.isUpdateUserVisibility = true;
					$this.etablissementService.addAdresseEtablisement(adresseToSave).subscribe(
						function(data) {
							$this.successfulOperation();
						}.bind($this)
					);
				},
				function() {},
				true
			);
		}
	}

	successfulOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	refreshListObserver() {
		let criteria = new SearchCriteria();

		criteria.roleChargeur = true;
		criteria.roleControlTower = true

		if (this.isControlTower) {
			criteria.connectedUserNum = this.ebDemande.user.ebUserNum;
			criteria.ebCompagnieNum = this.ebDemande.user.ebCompagnie.ebCompagnieNum;
		} else {
			criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

			criteria.activated = true;

			criteria.connectedUserNum = this.userConnected.ebUserNum;
		}

		this.userService.getListUser(criteria).subscribe((data) => {
			this.listObserver = data;
		});
	}
}
