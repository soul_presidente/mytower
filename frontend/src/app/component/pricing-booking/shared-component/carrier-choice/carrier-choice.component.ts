import { PricingService } from "@app/services/pricing.service";
import { EbDemande } from "@app/classes/demande";
import { DataTableDirective } from "angular-datatables";
import {
	ChangeDetectorRef,
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ViewChild,
} from "@angular/core";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { EbUser } from "@app/classes/user";
import {
	CarrierStatus,
	DemandeStatus,
	IDChatComponent,
	Modules,
	ServiceType,
	UserRole,
} from "@app/utils/enumeration";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { Statique } from "@app/utils/statique";
import { ModalService } from "@app/shared/modal/modal.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { UserService } from "@app/services/user.service";
import { EbEtablissement } from "@app/classes/etablissement";
import { EcCountry } from "@app/classes/country";
import { EbCompagnie } from "@app/classes/compagnie";
import { StatiqueService } from "@app/services/statique.service";
import { debounceTime, distinctUntilChanged, map, switchMap } from "rxjs/operators";
import { TransportationPlanService } from "../../../../services/transportation-plan.service";
import SingletonStatique from "../../../../utils/SingletonStatique";
import { EbChat } from "../../../../classes/chat";
import { SearchCriteriaPlanTransport } from "@app/utils/searchCriteriaPlanTransport";

@Component({
	selector: "app-carrier-choice",
	templateUrl: "./carrier-choice.component.html",
	styleUrls: ["../../../../shared/modal/modal.component.scss", "./carrier-choice.component.css"],
})
export class CarrierChoiceComponent implements OnInit {
	UserRole = UserRole;
	ServiceType = ServiceType;
	DemandeStatus = DemandeStatus;
	CarrierStatus = CarrierStatus;
	CarrierStatusCreationTM = Statique.CarrierStatusCreationTM;
	Statique = Statique;
	IDChatComponent = IDChatComponent;

	@Input("module")
	ebModuleNum: number;
	@Input()
	ebDemande: EbDemande;
	@Input()
	userConnected: EbUser;
	@Input()
	hasContributionAccess?: boolean;
	@Input()
	listTransporteur: Array<EbUser>;
	@Input()
	listTransporteurFromPlanToAdd: Array<EbUser>;
	@Input()
	isForGrouping: boolean;

	totalPrice: number = 0;

	@Output()
	onValidate = new EventEmitter<any>();
	@Output()
	updateCarrierChoiceEmitter = new EventEmitter<void>();
	@Output()
	updateChatEmitter = new EventEmitter<EbChat>();

	@ViewChild("pageValid", { static: false })
	pageValid: ElementRef;
	@ViewChild("openingCost", { static: false })
	openingCostTemplate: ElementRef;
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	@ViewChild("formEmail", { static: false })
	formEmail: any;

	searchCriteria: SearchCriteria = new SearchCriteria();
	dataTableConfig: DataTableConfig = new DataTableConfig();
	emails: Array<String> = new Array<String>();

	recomendation: boolean = false;
	exEbDemandeTransporteurs: ExEbDemandeTransporteur = new ExEbDemandeTransporteur();
	ConstantsTranslate: Object;
	modeTranspo: String = null;
	devise: String = null;
	closeResult: String;
	leadTimeArrival: number = null;
	pkdate: Date = null;
	deliveryTime: any = null;
	transitTime: any = null;
	price: number = null;
	leadTimeDeparture: any = null;
	cityDeparture: String = null;
	isConfirmed = false;

	cityArrival: String = null;
	leadTimePickup: any = null;
	xEcCountryPickup: any = null;
	cityPickup: String = null;
	leadTimeDelivery: any = null;
	cityDelivery: String = null;
	leadTimeCustoms: any = null;
	cityCustoms: String = null;
	showPrice: boolean = false;
	transptFromPlan = false;
	list = [];
	// listTransptfromPlan:Array<PricingCarrierSearchResult> = new Array<PricingCarrierSearchResult>();
	listTransptfromPlan: Array<EbUser> = new Array<EbUser>();

	newDemandeQuote: ExEbDemandeTransporteur = null;

	showPriceExEbDemandeTransporteur: ExEbDemandeTransporteur = null;

	typeaheadUserEvent: EventEmitter<string> = new EventEmitter<string>();
	listUserTypeahead: any = new Array<EbUser>();
	selectedUser: EbUser;
	typeaheadUserCriteria: SearchCriteria;
	nbTypeaheadListUser: number;
	tmpListUserTypeahead: Array<EbUser> = new Array<EbUser>();

	listSelectedDemandeQuote: Array<ExEbDemandeTransporteur> = new Array<ExEbDemandeTransporteur>();
	isEditProcess: boolean = false;

	constructor(
		private pricingService: PricingService,
		private transportationPlanService: TransportationPlanService,
		private modalService: ModalService,
		private translate: TranslateService,
		private modalService1: NgbModal,
		private userService: UserService,
		protected cd: ChangeDetectorRef,
		private statiqueService: StatiqueService
	) {}

	ngOnInit() {
		for (let d of this.ebDemande.exEbDemandeTransporteurs) {
			if (d.isFromTransPlan) {
				this.transptFromPlan = true;
				break;
			}
		}

		if (this.ebDemande.xEcStatut >= 11) {
			this.isConfirmed = true;
		}

		this.searchCriteria.setUser(this.userConnected);

		this.dataTableConfig = new DataTableConfig();
		this.dataTableConfig.dtOptions.searching = false;
		this.dataTableConfig.dtOptions.bInfo = false;
		this.dataTableConfig.dtOptions.serverSide = true;
		this.dataTableConfig.dtOptions.paging = false;
		this.emails.push(new String());
		// this.getListTransporteur();

		if (typeof this.hasContributionAccess == "undefined") this.hasContributionAccess = true;

		// translation
		this.ConstantsTranslate = {
			CONFIRM_RECOMMENDATION: "",
			CONFIRM_FINAL_CHOICE: "",
			CONFIRMATION: "",
			QUOTATION_SENT: "",
		};
		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate
				.get("PRICING_BOOKING." + key)
				.subscribe((res) => (this.ConstantsTranslate[key] = res));
		});

		if ((this.ebDemande && !this.ebDemande.ebDemandeNum) || this.isForGrouping) {
			this.getListFromPlanTransport();
		}

		if (this.ebDemande && this.ebDemande.ebDemandeNum) {
			this.typeaheadUserCriteria = new SearchCriteria();
			this.typeaheadUserCriteria.size = null;
			this.typeaheadUserCriteria.roleTransporteur = true;
			this.typeaheadUserCriteria.roleBroker = false;
			this.typeaheadUserCriteria.contact = true;
			this.typeaheadUserCriteria.ebUserNum = this.ebDemande.user.ebUserNum;
			this.typeaheadUserCriteria.forChargeur = true;
			this.typeaheadUserCriteria.roleControlTower = true;

			if (this.userConnected.role == UserRole.CHARGEUR) {
				this.typeaheadUserEvent
					.pipe(
						distinctUntilChanged(),
						debounceTime(200),
						switchMap((term) => {
							this.typeaheadUserCriteria.searchterm = term;
							if (this.userConnected.role == UserRole.CHARGEUR)
								return this.userService
									.getUserContacts(this.typeaheadUserCriteria)
									.pipe(map((res) => res.records));
							else return this.userService.getListUser(this.typeaheadUserCriteria);
						})
					)
					.subscribe(
						(data) => {
							this.cd.markForCheck();
							this.listUserTypeahead = data;
						},
						(err) => {
							this.listUserTypeahead = [];
						}
					);
			} else if (this.userConnected.role == UserRole.CONTROL_TOWER) {
				this.nbTypeaheadListUser = 2;
				let filterResultAndConcat = function(arr1: Array<EbUser>, arr2: Array<EbUser>) {
					let res = new Array<EbUser>();
					arr1.forEach((it1) => {
						if (arr2.findIndex((it2) => it1.ebUserNum === it2.ebUserNum) == -1) {
							it1.isCt = true;
							res.push(it1);
						}
					});
					return arr2.concat(res);
				}.bind(this);

				this.userService.getUserContacts(this.typeaheadUserCriteria).subscribe(
					function(res) {
						this.nbTypeaheadListUser--;
						if (this.nbTypeaheadListUser) this.tmpListUserTypeahead = res.records;
						else
							this.listUserTypeahead = filterResultAndConcat(
								this.tmpListUserTypeahead,
								res.records
							);
					}.bind(this)
				);

				this.typeaheadUserCriteria.ebUserNum = this.userConnected.ebUserNum;
				this.typeaheadUserCriteria.forChargeur = false;
				this.typeaheadUserCriteria.forControlTower = true;
				this.userService.getUserContacts(this.typeaheadUserCriteria).subscribe(
					function(res) {
						this.nbTypeaheadListUser--;
						if (this.nbTypeaheadListUser) this.tmpListUserTypeahead = res.records;
						else
							this.listUserTypeahead = filterResultAndConcat(
								res.records,
								this.tmpListUserTypeahead
							);
					}.bind(this)
				);
			}
		}
		this.ebDemande.listTransporteurs = [];

		if (!this.disableEditQuotation() && this.userConnected.role == UserRole.PRESTATAIRE) {
			localStorage.setItem("canShowAddQuotation", "1");
			document.addEventListener(
				"add-quotation",
				function(e) {
					event.preventDefault();
					event.stopPropagation();

					this.addEditQuotation();
				}.bind(this),
				false
			);
		}

		(async () => {
			let listCurrency = await SingletonStatique.getListEcCurrency();
			if (this.ebDemande.xecCurrencyInvoice && this.ebDemande.xecCurrencyInvoice.ecCurrencyNum) {
				listCurrency.forEach((it) => {
					if (it.ecCurrencyNum == this.ebDemande.xecCurrencyInvoice.ecCurrencyNum)
						this.ebDemande.xecCurrencyInvoice = it;
				});
			}
		})();

		if (this.isForGrouping) {
			this.totalPrice = 0;
			this.ebDemande.exEbDemandeTransporteurs.forEach((it) => {
				this.totalPrice += it.price;
			});
			// if(!Statique.isDefined(this.ebDemande.consolidatedPrice)){
			//   this.ebDemande.consolidatedPrice = this.totalPrice / this.ebDemande.exEbDemandeTransporteurs.length;
			// }
		}
	}

	ngAfterViewInit() {
		this.checkDefaultCt();
	}

	async checkDefaultCt() {
		if (
			!this.ebDemande.ebDemandeNum &&
			this.listTransporteur &&
			this.listTransporteur.length > 0 &&
			this.userConnected.role == UserRole.CONTROL_TOWER
		) {
			let indexTransp = this.listTransporteur.findIndex(
				(it) => it.ebUserNum == this.userConnected.ebUserNum
			);
			if (indexTransp >= 0) {
				let elt = await Statique.waitUntilElementExists(".checkbox-transpo", false);
				if (elt) {
					elt[indexTransp].setAttribute("checked", "true");
					this.onTransporteurSelected(indexTransp);
				}
			}
		}
	}

	getListFromPlanTransport() {
		let dgMar = 0;
		for (let m of this.ebDemande.listMarchandises) {
			if (m.dangerousGood != null && dgMar < m.dangerousGood) {
				dgMar = m.dangerousGood;
			}
		}
		var searchCriteriaPl = new SearchCriteriaPlanTransport();
		if (this.ebDemande.ebPartyOrigin.zone && this.ebDemande.ebPartyOrigin.zone.ebZoneNum)
			searchCriteriaPl.ebZoneNumOrigin = this.ebDemande.ebPartyOrigin.zone.ebZoneNum;
		if (this.ebDemande.ebPartyDest.zone && this.ebDemande.ebPartyDest.zone.ebZoneNum)
			searchCriteriaPl.ebZoneNumDest = this.ebDemande.ebPartyDest.zone.ebZoneNum;
		// searchCriteriaPl.isDangerous = false; //TODO
		if (dgMar > 1) {
			searchCriteriaPl.dangerous = true;
		} else if (dgMar == 1) {
			searchCriteriaPl.dangerous = false;
		} else {
			searchCriteriaPl.dangerous = null;
		}
		searchCriteriaPl.modeTransport = this.ebDemande.xEcModeTransport;
		searchCriteriaPl.typeDemande = this.ebDemande.xEcTypeDemande;

		searchCriteriaPl.totalWeight =
			(this.ebDemande.totalTaxableWeight as any) === "" ? null : this.ebDemande.totalTaxableWeight;
		searchCriteriaPl.totalUnit =
			(this.ebDemande.totalNbrParcel as any) === "" ? null : this.ebDemande.totalNbrParcel;
		searchCriteriaPl.totalVolume =
			(this.ebDemande.totalVolume as any) === "" ? null : this.ebDemande.totalVolume;

		searchCriteriaPl.currencyCibleNum = this.ebDemande.xecCurrencyInvoice
			? this.ebDemande.xecCurrencyInvoice.ecCurrencyNum
			: null;
		searchCriteriaPl.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;

		this.transportationPlanService
			.searchPricingTransport(searchCriteriaPl)
			.subscribe((data: Array<any>) => {
				if (this.isForGrouping) {
					data &&
						data.some((it) => {
							if (it.calculatedPrice && it.calculatedPrice > 0) {
								this.ebDemande.consolidatedPrice = it.calculatedPrice;
								return true;
							}
						});
				} else {
					this.listTransptfromPlan = this.getListTransForPlan(data);
					for (let trans of this.listTransptfromPlan) {
						if (trans.calculatedPrice && trans.calculatedPrice > 0) {
							// let t=new PricingCarrierSearchResult();
							// this.ebDemande.listTransporteurs.push(t.copy(trans));
							let t = new EbUser();
							this.listTransporteurFromPlanToAdd.push(t.constructorCopy(trans));
						}
					}
					if (this.listTransptfromPlan && this.listTransptfromPlan.length > 0) {
						this.ebDemande.xEcStatut = this.Statique.StatutDemande[2].key;
					}
					for (let trans of this.listTransptfromPlan) {
						// let t=new PricingCarrierSearchResult();
						// this.list.push(t.copy(trans));
						// this.list.push(trans);
						let t = new EbUser();
						this.list.push(t.constructorCopy(trans));
					}

					for (let trans of this.listTransporteur) {
						if (!this.contains(trans, this.list)) {
							// let t=new PricingCarrierSearchResult();
							// this.list.push(t.copyFromUser(trans));
							let t = new EbUser();
							this.list.push(t.constructorCopy(trans));
						}
					}
				}
			});
	}
	getListTransForPlan(list): Array<EbUser> {
		let listTran = [];
		list.forEach((it) => {
			let transp = new EbUser();
			transp.ebUserNum = it.carrierUserNum;
			transp.nom = it.carrierName;
			transp.email = it.carrierEmail;
			transp.calculatedPrice = it.calculatedPrice;
			transp.transitTime = it.transitTime;
			transp.coment = it.comment;

			if (it.dangerous != null) {
				if (it.dangerous === true) {
					it.comment +=
						" (" + this.translate.instant("PRICING_BOOKING.PLAN_TRANSPORT_DANGEROUS") + ")";
				} else if (it.dangerous === false) {
					it.comment +=
						" (" + this.translate.instant("PRICING_BOOKING.PLAN_TRANSPORT_NOT_DANGEROUS") + ")";
				}
			}

			transp.refPlan = it.planRef;
			transp.isFromTransPlan = true;
			transp.exchangeRateFound = it.exchangeRateFound;

			if (it.carrierEtabNum) {
				transp.ebEtablissement = new EbEtablissement();
				transp.ebEtablissement.ebEtablissementNum = it.carrierEtabNum;
				transp.ebEtablissement.nom = it.carrierEtabName;
				if (it.carrierCompanyNum) {
					transp.ebEtablissement.ebCompagnie = new EbCompagnie();
					transp.ebEtablissement.ebCompagnie.ebCompagnieNum = it.carrierCompanyNum;
					transp.ebEtablissement.ebCompagnie.nom = it.carrierCompanyNum;
				} else transp.ebEtablissement.ebCompagnie = null;
			} else transp.ebEtablissement = null;
			listTran.push(transp);
		});
		return listTran;
	}

	getListTransporteur() {
		this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
		this.searchCriteria.user = JSON.stringify(this.userConnected);
		this.searchCriteria.roleTransporteur = true;
		this.searchCriteria.roleControlTower = true;
		this.searchCriteria.size = null;
		this.searchCriteria.pageNumber = null;
		this.searchCriteria.notBlackList = true;
		this.pricingService.listTransporteurControlTower(this.searchCriteria).subscribe((data) => {
			this.listTransporteur = data;
			for (let trans of this.listTransporteur) {
				if (!this.contains(trans, this.list)) {
					// let t=new PricingCarrierSearchResult();
					// this.list.push(t.copyFromUser(trans));
					let t = new EbUser();
					this.list.push(t.constructorCopy(trans));
				}
			}
		});
	}
	contains(trans, list) {
		for (let e of list) {
			if (e.ebUserNum == trans.ebUserNum) {
				return true;
			}
		}
		return false;
	}

	confirmTransporteur(transporteur: EbUser, event) {
		this.ebDemande.confirmedFromTransptPlan = true;
		transporteur.selected = true;
		this.ebDemande.listTransporteurs = [transporteur];
		// this.isConfirmed=true;
		this.onValidate.emit(event);
	}
	changeTransporteur() {
		this.ebDemande.listTransporteurs = [];
		this.ebDemande.xEcStatut = null;
		this.isConfirmed = false;
	}

	onTransporteurSelected(index: number) {
		let indexIn = this.ebDemande.listTransporteurs.indexOf(this.listTransporteur[index]);
		if (indexIn == -1) {
			// this.ebDemande.listTransporteurs.push(this.listTransporteur[index]);
			let trans = this.list[index];
			// if(!this.contains(trans,this.ebDemande.listTransporteurs)){
			trans.isFromTransPlan = false;
			// trans.calculatedPrice=null;
			this.ebDemande.listTransporteurs.push(trans);
			// }
		} else {
			this.ebDemande.listTransporteurs.splice(indexIn, 1);
		}
	}

	onRecomendationChange() {
		if (this.recomendation) {
			this.ebDemande.flagRecommendation = true;
		} else {
			this.ebDemande.flagRecommendation = false;
		}
	}

	addEmail() {
		this.emails.push(new String());
	}

	removeEmail(index: number) {
		if (this.emails.length == 1) {
			this.emails[index] = new String();
		} else this.emails.splice(index, 1);
	}

	nextPage(form: any) {
		if (!this.isConfirmed) {
			for (let trans of this.listTransptfromPlan) {
				if (trans.calculatedPrice && trans.calculatedPrice > 0) {
					// let t=new PricingCarrierSearchResult();
					// this.ebDemande.listTransporteurs.push(t.copy(trans));
					let t = new EbUser();
					this.ebDemande.listTransporteurs.push(t.constructorCopy(trans));
				}
			}
		}
		this.onValidate.emit();
	}

	isPageValid() {
		if (
			!(!this.pageValid || !this.pageValid.nativeElement || this.pageValid.nativeElement.disabled)
		) {
			return true;
		}
		return false;
	}

	confirmChoice(exEbDemandeTransporteur: ExEbDemandeTransporteur, event) {
		if (
			this.userConnected.role == UserRole.CHARGEUR ||
			this.userConnected.role == UserRole.CONTROL_TOWER
		) {
			this.modalService.confirm(
				this.ConstantsTranslate["CONFIRMATION"],
				this.ConstantsTranslate["CONFIRM_FINAL_CHOICE"],
				function() {
					let requestProcessing = new RequestProcessing();
					requestProcessing.beforeSendRequest(event);
					let demandeTransporteur = new ExEbDemandeTransporteur();
					// demandeTransporteur.status = CarrierStatus.FINAL_CHOICE;
					if (exEbDemandeTransporteur.isFromTransPlan) {
						demandeTransporteur.status = CarrierStatus.FIN_PLAN;
					} else {
						demandeTransporteur.status = CarrierStatus.FINAL_CHOICE;
					}
					demandeTransporteur.xEbDemande = new EbDemande();
					demandeTransporteur.xEbDemande.ebDemandeNum = this.ebDemande.ebDemandeNum;
					demandeTransporteur.xTransporteur = new EbUser();
					demandeTransporteur.exEbDemandeTransporteurNum =
						exEbDemandeTransporteur.exEbDemandeTransporteurNum;
					demandeTransporteur.xTransporteur.ebUserNum =
						exEbDemandeTransporteur.xTransporteur.ebUserNum;
					demandeTransporteur.xEbUserConfirmBy = new EbUser();
					demandeTransporteur.xEbUserConfirmBy.ebUserNum = this.userConnected.ebUserNum;
					this.pricingService.updateStatusQuotation(demandeTransporteur).subscribe((res) => {
						exEbDemandeTransporteur.status = demandeTransporteur.status;
						this.ebDemande.xEcStatut = DemandeStatus.WDCT;

						Statique.setCurrentEbDemande(this.ebDemande);

						requestProcessing.afterGetResponse(event);
					});
				}.bind(this)
			);
		}
	}

	confirmRecommendation(exEbDemandeTransporteur: ExEbDemandeTransporteur, event) {
		if (this.userConnected.role == UserRole.CONTROL_TOWER) {
			this.modalService.confirm(
				this.ConstantsTranslate["CONFIRMATION"],
				this.ConstantsTranslate["CONFIRM_RECOMMENDATION"],
				function() {
					let requestProcessing = new RequestProcessing();
					requestProcessing.beforeSendRequest(event);
					let demandeTransporteur = new ExEbDemandeTransporteur();
					demandeTransporteur.status = CarrierStatus.RECOMMENDATION;
					demandeTransporteur.xEbDemande = new EbDemande();
					demandeTransporteur.xEbDemande.ebDemandeNum = this.ebDemande.ebDemandeNum;
					demandeTransporteur.xTransporteur = new EbUser();
					demandeTransporteur.xTransporteur.ebUserNum =
						exEbDemandeTransporteur.xTransporteur.ebUserNum;
					this.pricingService.updateStatusQuotation(demandeTransporteur).subscribe((res) => {
						exEbDemandeTransporteur.status = CarrierStatus.RECOMMENDATION;
						this.ebDemande.xEcStatut = DemandeStatus.RECOMMENDATION;

						requestProcessing.afterGetResponse(event);
					});
				}.bind(this)
			);
		}
	}

	disableRecomendation(demandeTransporteur: ExEbDemandeTransporteur = null) {
		return (
			!(
				this.userConnected.role == UserRole.CONTROL_TOWER &&
				this.ebDemande.flagRecommendation &&
				this.ebDemande.xEcStatut < DemandeStatus.RECOMMENDATION
			) ||
			this.ebDemande.xEcStatut == DemandeStatus.IN_PROCESS ||
			this.ebDemande.xEcStatut >= DemandeStatus.WDCT ||
			(demandeTransporteur != null && demandeTransporteur.status < CarrierStatus.QUOTE_SENT) ||
			!this.hasContributionAccess
		);
	}

	disableConfirmButton(demandeTransporteur: ExEbDemandeTransporteur = null) {
		return (
			(!this.isChargeur() && this.userConnected.role != UserRole.CONTROL_TOWER) ||
			this.ebDemande.xEcStatut == DemandeStatus.IN_PROCESS ||
			this.ebDemande.xEcStatut >= DemandeStatus.WDCT ||
			this.ebDemande.xEcCancelled ||
			(demandeTransporteur != null && demandeTransporteur.status < CarrierStatus.QUOTE_SENT) ||
			!this.hasContributionAccess
		);
	}

	isChargeur() {
		return this.userConnected.role == UserRole.CHARGEUR;
	}

	isControlTower() {
		return this.userConnected.role == UserRole.CONTROL_TOWER;
	}

	get isTransporteur() {
		return (
			this.userConnected.service == ServiceType.TRANSPORTEUR ||
			this.userConnected.service == ServiceType.TRANSPORTEUR_BROKER
		);
	}

	get isModulePricing(): boolean {
		return this.ebModuleNum == Modules.PRICING;
	}

	get isModuleTM(): boolean {
		return this.ebModuleNum == Modules.TRANSPORT_MANAGEMENT;
	}

	get isCreationMode(): boolean {
		return this.ebDemande.ebDemandeNum == null;
	}

	getStatus(status) {
		let st = Statique.CarrierStatus.find((it) => it.key == status);
		if (st) return st.value;
		return "";
	}

	showDetailsPrice(index: number) {
		this.showPriceExEbDemandeTransporteur = this.ebDemande.exEbDemandeTransporteurs[index];

		this.showPrice = true;
	}

	openDialogOpeningHours(tpl) {
		this.modalService.template("Opening cost", tpl);
	}

	disableEditQuotation() {
		return (
			!(
				(this.userConnected.role == UserRole.CONTROL_TOWER &&
					this.ebDemande.listDemandeQuote &&
					this.ebDemande.listDemandeQuote.find((it) => {
						return (
							it.xEbEtablissement &&
							it.xEbEtablissement.ebEtablissementNum ==
								this.userConnected.ebEtablissement.ebEtablissementNum
						);
					})) ||
				(this.userConnected.role == UserRole.PRESTATAIRE &&
					(this.userConnected.service == ServiceType.TRANSPORTEUR ||
						this.userConnected.service == ServiceType.TRANSPORTEUR_BROKER))
			) || this.ebDemande.xEcStatut >= DemandeStatus.FINAL_CHOICE
		);
	}

	addEditQuotation(index: number = null) {
		if (!Statique.isDefined(index)) {
			let i,
				n = this.ebDemande.exEbDemandeTransporteurs.length;
			for (i = 0; i < n; i++) {
				if (
					this.ebDemande.exEbDemandeTransporteurs[i].status == CarrierStatus.REQUEST_RECEIVED &&
					((this.userConnected.role == UserRole.PRESTATAIRE &&
						this.ebDemande.exEbDemandeTransporteurs[i].xTransporteur.ebUserNum ==
							this.userConnected.ebUserNum) ||
						(this.selectedUser != null &&
							this.ebDemande.exEbDemandeTransporteurs[i].xTransporteur.ebUserNum ==
								this.selectedUser.ebUserNum))
				) {
					let quote = new ExEbDemandeTransporteur();
					quote.copy(this.ebDemande.exEbDemandeTransporteurs[i]);
					this.newDemandeQuote = quote;
					this.newDemandeQuote.xTransporteur.nom = (this.selectedUser || this.userConnected).nom;
					this.newDemandeQuote.xTransporteur.prenom = (
						this.selectedUser || this.userConnected
					).prenom;
					this.newDemandeQuote.xTransporteur.email = (
						this.selectedUser || this.userConnected
					).email;
					this.newDemandeQuote.ctCommunity = (this.selectedUser || this.userConnected).isCt;
					this.newDemandeQuote.xEbEtablissement = new EbEtablissement();
					this.newDemandeQuote.xEbEtablissement.ebEtablissementNum = (
						this.selectedUser || this.userConnected
					).ebEtablissement.ebEtablissementNum;
					this.newDemandeQuote.xEbCompagnie = new EbCompagnie();
					this.newDemandeQuote.xEbCompagnie.ebCompagnieNum = (
						this.selectedUser || this.userConnected
					).ebCompagnie.ebCompagnieNum;
					break;
				}
			}
			if (i >= n) {
				let _user: EbUser = new EbUser();
				if (this.userConnected.role == UserRole.PRESTATAIRE) _user = this.userConnected;
				else if (this.selectedUser) _user = this.selectedUser;

				this.newDemandeQuote = new ExEbDemandeTransporteur();
				this.newDemandeQuote.xTransporteur = new EbUser();
				this.newDemandeQuote.xTransporteur.ebUserNum = _user.ebUserNum;
				this.newDemandeQuote.xTransporteur.nom = _user.nom;
				this.newDemandeQuote.xTransporteur.prenom = _user.prenom;
				this.newDemandeQuote.xTransporteur.email = _user.email;
				this.newDemandeQuote.ctCommunity = _user.isCt;

				this.newDemandeQuote.xEbEtablissement = new EbEtablissement();
				this.newDemandeQuote.xEbEtablissement.ebEtablissementNum =
					_user.ebEtablissement.ebEtablissementNum;

				this.newDemandeQuote.xEbCompagnie = new EbCompagnie();
				this.newDemandeQuote.xEbCompagnie.ebCompagnieNum = _user.ebCompagnie.ebCompagnieNum;

				if (this.ebDemande.ebPartyOrigin && this.ebDemande.ebPartyOrigin.city)
					this.newDemandeQuote.cityPickup = "" + this.ebDemande.ebPartyOrigin.city;
				if (
					this.ebDemande.ebPartyOrigin &&
					this.ebDemande.ebPartyOrigin.xEcCountry &&
					this.ebDemande.ebPartyOrigin.xEcCountry.ecCountryNum
				) {
					this.newDemandeQuote.xEcCountryPickup = new EcCountry();
					this.newDemandeQuote.xEcCountryPickup.ecCountryNum = this.ebDemande.ebPartyOrigin.xEcCountry.ecCountryNum;
				}
				if (this.ebDemande.ebPartyDest && this.ebDemande.ebPartyDest.city)
					this.newDemandeQuote.cityDelivery = "" + this.ebDemande.ebPartyDest.city;
				if (
					this.ebDemande.ebPartyDest &&
					this.ebDemande.ebPartyDest.xEcCountry &&
					this.ebDemande.ebPartyDest.xEcCountry.ecCountryNum
				) {
					this.newDemandeQuote.xEcCountryDelivery = new EcCountry();
					this.newDemandeQuote.xEcCountryDelivery.ecCountryNum = this.ebDemande.ebPartyDest.xEcCountry.ecCountryNum;
				}
			}
		} else if (
			this.ebDemande.exEbDemandeTransporteurs &&
			this.ebDemande.exEbDemandeTransporteurs.length > 0
		) {
			let quote = new ExEbDemandeTransporteur();
			quote.copy(this.ebDemande.exEbDemandeTransporteurs[index]);
			this.isEditProcess = true;
			this.newDemandeQuote = quote;
		}
	}

	deleteQuotation(index: number) {
		if (Statique.isDefined(index)) {
			this.pricingService
				.deleteQuotation(this.ebDemande.exEbDemandeTransporteurs[index].exEbDemandeTransporteurNum)
				.subscribe(
					function(res) {
						this.ebDemande.exEbDemandeTransporteurs.splice(index, 1);
					}.bind(this)
				);
		}
	}

	feedbackQuotationHandler(result: ExEbDemandeTransporteur) {
		this.isEditProcess = false;

		if (result) {
			this.newDemandeQuote = null;
			this.modalService.information(
				this.ConstantsTranslate["CONFIRMATION"],
				this.ConstantsTranslate["QUOTATION_SENT"]
			);
		} else {
			this.newDemandeQuote = null;
		}
	}

	sendQuotationUser(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		let demTranspo = new ExEbDemandeTransporteur();
		// demTranspo.status = CarrierStatus.REQUEST_RECEIVED;
		demTranspo.exEbDemandeTransporteurNum = null;

		if (this.ebDemande.ebPartyOrigin && this.ebDemande.ebPartyOrigin.city)
			demTranspo.cityPickup = "" + this.ebDemande.ebPartyOrigin.city;
		if (
			this.ebDemande.ebPartyOrigin &&
			this.ebDemande.ebPartyOrigin.xEcCountry &&
			this.ebDemande.ebPartyOrigin.xEcCountry.ecCountryNum
		) {
			demTranspo.xEcCountryPickup = new EcCountry();
			demTranspo.xEcCountryPickup.ecCountryNum = this.ebDemande.ebPartyOrigin.xEcCountry.ecCountryNum;
		}
		if (this.ebDemande.ebPartyDest && this.ebDemande.ebPartyDest.city)
			demTranspo.cityDelivery = "" + this.ebDemande.ebPartyDest.city;
		if (
			this.ebDemande.ebPartyDest &&
			this.ebDemande.ebPartyDest.xEcCountry &&
			this.ebDemande.ebPartyDest.xEcCountry.ecCountryNum
		) {
			demTranspo.xEcCountryDelivery = new EcCountry();
			demTranspo.xEcCountryDelivery.ecCountryNum = this.ebDemande.ebPartyDest.xEcCountry.ecCountryNum;
		}

		demTranspo.xTransporteur = new EbUser();
		demTranspo.xTransporteur.ebUserNum = this.selectedUser.ebUserNum;
		demTranspo.xTransporteur.nom = this.selectedUser.nom;
		demTranspo.xTransporteur.prenom = this.selectedUser.prenom;
		demTranspo.xTransporteur.email = this.selectedUser.email;
		demTranspo.ctCommunity = this.selectedUser.isCt;

		demTranspo.xEbEtablissement = new EbEtablissement();
		demTranspo.xEbEtablissement.ebEtablissementNum = this.selectedUser.ebEtablissement.ebEtablissementNum;
		demTranspo.xEbCompagnie = new EbCompagnie();
		demTranspo.xEbCompagnie.ebCompagnieNum = this.selectedUser.ebCompagnie.ebCompagnieNum;

		demTranspo.xEbDemande = new EbDemande();
		demTranspo.xEbDemande.ebDemandeNum = this.ebDemande.ebDemandeNum;
		demTranspo.xEbDemande.sendQuotation = true;

		this.pricingService.ajouterQuotation(demTranspo,this.ebModuleNum,false).subscribe(
			function(res) {
				this.updateCarrierChoiceEmitter.emit();
				requestProcessing.afterGetResponse(event);
			}.bind(this)
		);
	}

	showUserSelector() {
		return (
			this.ebDemande.xEcStatut < DemandeStatus.FINAL_CHOICE &&
			(this.userConnected.role == UserRole.CHARGEUR ||
				(this.userConnected.role == UserRole.CONTROL_TOWER &&
					this.ebDemande.listDemandeQuote &&
					this.ebDemande.listDemandeQuote.find((it) => {
						return (
							it.xEbEtablissement &&
							it.xEbEtablissement.ebEtablissementNum ==
								this.userConnected.ebEtablissement.ebEtablissementNum
						);
					})))
		);
	}

	onStatusChange(newStatus: any, index: number) {
		// First prevent double final choice status
		if (newStatus == 4) {
			this.ebDemande.exEbDemandeTransporteurs.forEach((dt) => {
				if (
					dt.status === 4 &&
					dt.xTransporteur.ebUserNum !=
						this.ebDemande.exEbDemandeTransporteurs[index].xTransporteur.ebUserNum
				) {
					dt.status = 2;
				}
			});
		}
	}

	isButtonNextPageDisabled(): boolean {
		if (this.isModuleTM && this.isCreationMode) {
			let foundFinalChoiceDt = null;
			this.ebDemande.exEbDemandeTransporteurs.forEach((dt) => {
				if (dt.status == 4) foundFinalChoiceDt = dt;
			});

			return foundFinalChoiceDt == null || foundFinalChoiceDt.isQuotationFilled == false;
		} else {
			return (
				this.ebDemande.listTransporteurs.length == 0 &&
				this.formEmail != null &&
				!this.formEmail.valid
			);
		}
	}
}
