import {
	AccessRights,
	CancelStatus,
	CarrierStatus,
	DemandeStatus,
	IDChatComponent,
	Modules,
	NatureDemandeTransport,
	ServiceType,
	TypeCustomsBroker,
	UserRole,
} from "@app/utils/enumeration";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { Component, OnDestroy, OnInit, ViewChild } from "@angular/core";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { PricingService } from "@app/services/pricing.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbDemande } from "@app/classes/demande";
import { EbUser } from "@app/classes/user";
import { Statique } from "@app/utils/statique";
import { FichierJoint } from "@app/classes/fichiersJoint";
import { interval } from "rxjs";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbChat } from "@app/classes/chat";
import { ChatPanelComponent } from "@app/shared/chat-panel/chat-panel.component";
import { ModalService } from "@app/shared/modal/modal.service";
import { StatiqueService } from "@app/services/statique.service";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import SingletonStatique from "@app/utils/SingletonStatique";
import { AdresseForShippingComponent } from "../adresse-for-shipping/adresse-for-shipping.component";
import { ShippingInformationComponent } from "../shipping-information/shipping-information.component";
import { DocTemplateGenDetailsComponent } from "@app/component/documents/template-gen-params/template-gen-params";
import { DocumentTemplatesService } from "@app/services/document-templates.service";
import { GlobalService } from "@app/services/global.service";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos } from "@app/shared/header/header-infos";

@Component({
	selector: "app-details",
	templateUrl: "./details.component.html",
	styleUrls: ["./details.component.css"],
})
export class DetailsComponent extends ConnectedUserComponent implements OnInit, OnDestroy {
	DemandeStatus = DemandeStatus;
	Modules = Modules;
	Statique = Statique;
	modeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();
	CarrierStatus = CarrierStatus;
	IDChatComponent = IDChatComponent;
	UserRole = UserRole;
	ServiceType = ServiceType;
	TypeCustomsBroker = TypeCustomsBroker;

	ebDemande: EbDemande = new EbDemande();
	ebDemandeNum: number;
	module: number;
	letiableModule: string;
	hasContributionAccess: boolean = false;
	listTransporteur: Array<EbUser> = null;
	filesReturnToParent: Array<Array<FichierJoint>> = new Array<Array<FichierJoint>>();

	@ViewChild("chatPanelTransportComponent", { static: false })
	chatPanelTransportComponent: ChatPanelComponent;
	@ViewChild("chatPanelPricingComponent", { static: false })
	chatPanelPricingComponent: ChatPanelComponent;
	@ViewChild("AdresseForShippingComponent", { static: false })
	adresseForShippingComponent: AdresseForShippingComponent;

	@ViewChild("cmpTemplateGen", { static: false })
	cmpTemplateGen: DocTemplateGenDetailsComponent;

	toggleEditData: boolean = false;
	private subscription: any;
	nbColTexteditor: string = "col-md-4";
	nbColListing: string = "col-md-8";
	showAddressWrapper: boolean = true;
	unitInfos: any;
	modalExportPdfVisible: boolean = false;

	@ViewChild("shippingInformation", { static: false })
	shippingInformation: ShippingInformationComponent;

	constructor(
		private globalService: GlobalService,
		private statiqueService: StatiqueService,
		private router: Router,
		private route: ActivatedRoute,
		private pricingService: PricingService,
		private modalService: ModalService,
		private docTemplateService: DocumentTemplatesService,
		protected headerService: HeaderService
	) {
		super();
	}

	async getStatiques() {
		this.modeTransport = await SingletonStatique.getListModeTransport();
	}

	ngOnInit() {
		this.headerService.registerActionButtons(this.getActionButtons());

		this.getStatiques();
		localStorage.removeItem("xEcStatut");
		localStorage.removeItem("canShowAddQuotation");
		localStorage.removeItem("currentEbDemande");

		let searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();
		this.ebDemande.exEbDemandeTransporteurs = new Array<ExEbDemandeTransporteur>();
		this.ebDemande.exEbDemandeTransporteur = new ExEbDemandeTransporteur();

		this.route.params.subscribe((params: Params) => {
			this.ebDemandeNum = +params["ebDemandeNum"];
			this.module = Statique.getModuleNumFromUrl(this.router.url);

			if (this.ebDemandeNum) {
				this.getListUnit(this.ebDemandeNum);

				searchCriteriaPricingBooking.ebDemandeNum = this.ebDemandeNum;

				searchCriteriaPricingBooking.module = this.module;
				searchCriteriaPricingBooking.roleTransporteur = true;
				searchCriteriaPricingBooking.ebUserNum = this.userConnected.ebUserNum;
				searchCriteriaPricingBooking.setUser(this.userConnected);

				this.pricingService
					.getEbDemande(searchCriteriaPricingBooking)
					.subscribe((data: EbDemande) => {
						if (data && data.ebDemandeNum) {
							this.ebDemande = data;
							Statique.setCurrentEbDemande(data);
							this.ebDemande.listTransporteurs = [];

							if (this.ebDemande.curentTimeRemainingInMilliSecond > 0) {
								this.subscription = interval(1000).subscribe(
									function(x) {
										this.ebDemande.curentTimeRemainingInMilliSecond -= 1000;

										if (this.ebDemande.curentTimeRemainingInMilliSecond <= 0) {
											this.ebDemande.curentTimeRemainingInMilliSecond = 0;
											this.ngOnDestroy();
										}
									}.bind(this)
								);
							}

							this.ebDemande.exEbDemandeTransporteur = new ExEbDemandeTransporteur();
							if (
								this.userConnected.role == UserRole.CHARGEUR &&
								this.ebDemande.exEbDemandeTransporteurs &&
								this.ebDemande.exEbDemandeTransporteurs.length == 1
							) {
								this.ebDemande.exEbDemandeTransporteur.copy(
									this.ebDemande.exEbDemandeTransporteurs[0]
								);
							} else if (
								this.userConnected.role == UserRole.CONTROL_TOWER &&
								this.ebDemande.listDemandeQuote &&
								this.ebDemande.listDemandeQuote.length == 1
							) {
								this.ebDemande.exEbDemandeTransporteur.copy(this.ebDemande.listDemandeQuote[0]);
							}

							this.initEvents();
						} else {
							// // si la demande n'existe pas, on redirige vers le dashboard
							// this.router.navigate([
							//   "app",
							//   Statique.moduleNamePath[Statique.getModuleNameByModuleNum(this.module)],
							//   "dashboard"
							// ]);
							this.letiableModule = "quotation";
							let url: string = "failedaccess";
							this.router.navigate([url, this.ebDemandeNum, this.letiableModule]);
						}
					});
			}
		});

		// contribution access rights to module
		if (this.userConnected.listAccessRights && this.userConnected.listAccessRights.length > 0) {
			this.hasContributionAccess = !this.userConnected.listAccessRights.find((access) => {
				if (access.ecModuleNum == this.module && access.accessRight != AccessRights.CONTRIBUTION)
					return true;
			});
		} else {
			this.hasContributionAccess = false;
		}
	}

	getListUnit(ebDemandeNum) {
		this.globalService
			.runAction(Statique.controllerUnit + "/list-unit", ebDemandeNum)
			.subscribe((res) => {
				this.unitInfos = res;
			});
	}

	handleCancelPricingEvent(e) {
		event.preventDefault();
		event.stopPropagation();

		this.cancelQuotation(e.detail);
	}

	handleConfirmPickupEvent(e) {
		event.preventDefault();
		event.stopPropagation();

		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(Modules.TRACK)];
		let params = { queryParams: { filterByDemande: this.ebDemande.ebDemandeNum } };
		if (e.detail.isUpdateTT) params.queryParams["isUpdatePsl"] = e.detail.isUpdateTT ? 1 : 0;

		this.router.navigate(["/app/" + moduleRoute + "/dashboard/"], params);

		Statique.setCurrentEbDemande(this.ebDemande);
	}

	initEvents() {
		document.addEventListener("cancel-pricing", this.handleCancelPricingEvent.bind(this), false);
		document.addEventListener(
			"delete-demande-action",
			this.handleDeleteDemandeEvent.bind(this),
			false
		);

		if (this.module == Modules.TRANSPORT_MANAGEMENT) {
			document.addEventListener("confirm-pickup", this.handleConfirmPickupEvent.bind(this), false);
			document.addEventListener(
				"request-customsbroker-action",
				this.handleRequestCustomsBrokerEvent.bind(this),
				false
			);
		}
	}

	onListTransporteurLoadedHandler(listTransporteur) {
		this.listTransporteur = listTransporteur;
	}

	showBlockCotation() {
		return this.isTransporteur || this.isTransporteurBroker;
	}

	cancelQuotation(event) {
		if (this.ebDemande == null || this.ebDemande.ebDemandeNum == null) return;

		this.modalService.confirm(
			"Confirmation",
			"Do you really wish to cancel this cotation request ?",
			function() {
				let requestProcessing = new RequestProcessing();
				requestProcessing.beforeSendRequest(event);

				this.pricingService
					.cancelQuotation(this.ebDemande.ebDemandeNum)
					.subscribe((res: EbChat) => {
						if (res) {
							this.ebDemande.xEcCancelled = CancelStatus.CANCELLED;
							Statique.setCurrentEbDemande(this.ebDemande);
							res.module = this.module;
							res.idChatComponent =
								res.module == Modules.PRICING
									? IDChatComponent.PRICING
									: IDChatComponent.TRANSPORT_MANAGEMENT;
							this.updateChatEmitter(res);
						}
						requestProcessing.afterGetResponse(event);
					});
			}.bind(this)
		);
	}

	updateChatEmitter(ebChat: EbChat) {
		if (
			this.chatPanelPricingComponent &&
			(!ebChat.idChatComponent || ebChat.idChatComponent == IDChatComponent.PRICING)
		)
			this.chatPanelPricingComponent.insertChat(ebChat);
		else if (
			this.chatPanelTransportComponent &&
			(!ebChat.idChatComponent || ebChat.idChatComponent == IDChatComponent.TRANSPORT_MANAGEMENT)
		)
			this.chatPanelTransportComponent.insertChat(ebChat);
	}

	updateCarrierChoiceHandler() {
		let searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();
		if (this.ebDemandeNum) {
			searchCriteriaPricingBooking.ebDemandeNum = this.ebDemandeNum;
			// searchCriteriaPricingBooking.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			searchCriteriaPricingBooking.module = this.module;
			searchCriteriaPricingBooking.roleTransporteur = true;
			searchCriteriaPricingBooking.ebUserNum = this.userConnected.ebUserNum;

			this.pricingService
				.getListEbDemandeQuote(searchCriteriaPricingBooking)
				.subscribe((res: EbDemande) => {
					this.ebDemande.listDemandeQuote = res.listDemandeQuote;
					this.ebDemande.exEbDemandeTransporteurs = res.exEbDemandeTransporteurs;

					this.ebDemande.exEbDemandeTransporteur = new ExEbDemandeTransporteur();
					if (
						this.userConnected.role != UserRole.CONTROL_TOWER &&
						this.ebDemande.exEbDemandeTransporteurs &&
						this.ebDemande.exEbDemandeTransporteurs.length == 1
					) {
						this.ebDemande.exEbDemandeTransporteur.copy(this.ebDemande.exEbDemandeTransporteurs[0]);
					} else if (
						this.userConnected.role == UserRole.CONTROL_TOWER &&
						this.ebDemande.listDemandeQuote &&
						this.ebDemande.listDemandeQuote.length == 1
					) {
						this.ebDemande.exEbDemandeTransporteur.copy(this.ebDemande.listDemandeQuote[0]);
					}
				});
		}
	}

	handleDeleteDemandeEvent(e) {
		event.preventDefault();
		event.stopPropagation();

		this.deleteEbDemande(e.detail);
	}

	deleteEbDemande(event) {
		this.modalService.confirm(
			"Confirmation",
			"Do you really want to delete this pricing ?",
			function() {
				this.pricingService.deleteEbDemande([this.ebDemande.ebDemandeNum]).subscribe((res) => {
					if (res) {
						let curUrl = this.router.url;
						curUrl = curUrl.substr(0, curUrl.indexOf("/details"));
						this.router.navigateByUrl(curUrl + "/dashboard");
					}
				});
			}.bind(this)
		);
	}

	handleRequestCustomsBrokerEvent(e) {
		event.preventDefault();
		event.stopPropagation();

		this.requestCustomsBroker(e.detail);
	}

	requestCustomsBroker(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.pricingService.requestCustomsBroker(this.ebDemande.ebDemandeNum).subscribe((res) => {
			this.ebDemande.flagRequestCustomsBroker = true;
			requestProcessing.afterGetResponse(event);
		});
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
			this.subscription = null;
		}

		localStorage.removeItem("xEcStatut");
		Statique.resetCurrentEbDemande();

		document.removeEventListener("cancel-pricing", this.handleCancelPricingEvent.bind(this));
		document.removeEventListener("confirm-pickup", this.handleConfirmPickupEvent.bind(this));
		document.removeEventListener("delete-demande-action", this.handleDeleteDemandeEvent.bind(this));
		document.removeEventListener(
			"request-customsbroker-action",
			this.handleRequestCustomsBrokerEvent.bind(this)
		);
	}

	get canShowTransportInfos(): boolean {
		return (
			this.module == Modules.TRANSPORT_MANAGEMENT &&
			this.ebDemande.ebDemandeNum != null &&
			(this.ebDemande.xEcNature == null ||
				(this.ebDemande.xEcNature !== NatureDemandeTransport.INITIAL &&
					this.ebDemande.xEcNature !== NatureDemandeTransport.MASTER_OBJECT))
		);
	}

	get canShowDocuments(): boolean {
		return (
			this.ebDemande.xEcNature == null ||
			(this.ebDemande.xEcNature !== NatureDemandeTransport.INITIAL &&
				this.ebDemande.xEcNature !== NatureDemandeTransport.MASTER_OBJECT)
		);
	}

	get canShowCarrierChoice(): boolean {
		return (
			((this.ebDemande && this.ebDemande.ebDemandeNum) || this.listTransporteur) &&
			!this.isBroker &&
			this.userConnected.service != ServiceType.BROKER &&
			(this.ebDemande.xEcNature == null ||
				(this.ebDemande.xEcNature !== NatureDemandeTransport.INITIAL &&
					this.ebDemande.xEcNature !== NatureDemandeTransport.MASTER_OBJECT))
		);
	}

	get canShowUnitInformations(): boolean {
		return this.ebDemande.xEcNature !== NatureDemandeTransport.MASTER_OBJECT;
	}

	get canShowShippingInformations(): boolean {
		return this.ebDemande.xEcNature !== NatureDemandeTransport.MASTER_OBJECT;
	}

	private getActionButtons(): Array<HeaderInfos.ActionButton> {
		return [
			{
				label: "Export",
				icon: "fa fa-file-pdf-o",
				action: () => {
					this.exportPdf();
				},
				displayCondition: () => {
					return this.canShowExportPdfButton();
				},
			},
		];
	}

	canShowExportPdfButton() {
		let ebDemande: EbDemande = null;
		if (!(ebDemande = Statique.getCurrentEbDemande())) return false;

		return this.module == Modules.PRICING || this.module == Modules.TRANSPORT_MANAGEMENT;
	}

	exportPdf() {
		this.modalExportPdfVisible = true;
	}

	modalExportPdfConfirm(event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.cmpTemplateGen.params.templateModelNum = 0; //GenericEnums.DOC_TemplateModels

		this.cmpTemplateGen.params.templateModelParams = [];
		this.cmpTemplateGen.params.templateModelParams.push(String(this.ebDemandeNum));

		this.docTemplateService.fireDocumentGeneration(this.cmpTemplateGen.params).subscribe(
			(res) => {
				requestProcessing.afterGetResponse(event);
				const blob = new Blob([res], { type: "application/octet-stream" });
				const url = window.URL.createObjectURL(blob);

				let a: any = document.createElement("A");
				a.href = url;
				a.download =
					"Export" + (this.cmpTemplateGen.params.selectedFormat === 0 ? ".pdf" : ".docx");
				document.body.appendChild(a);
				a.click();
				document.body.removeChild(a);
			},
			(err) => {
				requestProcessing.afterGetResponse(event);
			}
		);
	}
}
