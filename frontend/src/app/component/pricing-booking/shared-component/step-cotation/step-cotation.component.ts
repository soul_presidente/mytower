import { Component, Input, OnInit, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { EbDemande } from "@app/classes/demande";
import { Statique } from "@app/utils/statique";
import {
	CancelStatus,
	CarrierStatus,
	DemandeStatus,
	Modules,
	ServiceType,
	TypeCustomsBroker,
	UserRole,
} from "@app/utils/enumeration";
import { interval, Subscription } from "rxjs";
import { EbUser } from "@app/classes/user";
import { PricingService } from "@app/services/pricing.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { TranslateService } from "@ngx-translate/core";
import { TrackTraceService } from "@app/services/trackTrace.service";
import { FreightAuditService } from "@app/services/freight-audit.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { map } from "rxjs/operators";
import { EbFlag } from "@app/classes/ebFlag";

@Component({
	selector: "app-step-cotation",
	templateUrl: "./step-cotation.component.html",
	styleUrls: ["./step-cotation.component.css"],
})
export class StepCotationComponent implements OnInit, OnDestroy {
	@Input()
	ebDemande: EbDemande;
	@Input()
	module: number;
	@Input()
	userConnected: EbUser;
	@Input()
	hasContributionAccess?: boolean;
	@Input()
	hideSteps: boolean = false;
	@Input()
	hideRemaining: boolean = false;

	CancelStatus = CancelStatus;
	DemandeStatus = DemandeStatus;
	TypeCustomsBroker = TypeCustomsBroker;
	UserRole = UserRole;
	ServiceType = ServiceType;
	CarrierStatus = CarrierStatus;
	Modules = Modules;

	formattedPickupTime: NgbDateStruct;
	createdBy: string;
	currentStatusTitle: string;
	dateCreation: string;
	dateObj: {
		days: string;
		hours: string;
		minutes: string;
		seconds: string;
	};

	private diff: number;
	private subscription: Subscription;

	constructor(
		private pricingService: PricingService,
		private tracktraceService: TrackTraceService,
		private translate: TranslateService,
		private freighAuditService?: FreightAuditService,
		private router?: Router,
		private modalService?: ModalService
	) {}

	ngOnInit() {
		if (
			(this.userConnected.service == ServiceType.TRANSPORTEUR ||
				this.userConnected.service == ServiceType.TRANSPORTEUR_BROKER) &&
			this.ebDemande.exEbDemandeTransporteurFinal && 
			((
				(this.userConnected.superAdmin &&
					this.ebDemande.exEbDemandeTransporteurFinal.ebCompagnie.ebCompagnieNum !=
						this.userConnected.ebCompagnie.ebCompagnieNum)) ||
				(!this.userConnected.superAdmin &&
					this.ebDemande.exEbDemandeTransporteurFinal.ebEtablissement.ebEtablissementNum !=
						this.userConnected.ebEtablissement.ebEtablissementNum))
		) {
			this.translate
				.get("PRICING_BOOKING.NOT_AWARDED")
				.subscribe((res) => (this.currentStatusTitle = res));
		} else {
			Statique.StatutDemande.some((it) => {
				if (
					(it.key == this.ebDemande.xEcStatut && !this.ebDemande.statusQuote) ||
					(!this.ebDemande.statusQuote && it.key == DemandeStatus.IN_PROCESS)
				) {
					let value = it.value;
					// if(it.key == DemandeStatus.WDCT && this.module == Modules.PRICING)
					// 	value = Statique.StatutDemande.find(it => it.key == 4).value;
					// else if(it.key == DemandeStatus.WDCT && this.ebDemande.flagDocumentTransporteur)
					// 	value = Statique.StatutDemande.find(it => it.key == 101).value;
					// else if(it.key == DemandeStatus.WDCT && this.ebDemande.flagDocumentCustoms)
					// 	value = Statique.StatutDemande.find(it => it.key == 102).value;

					this.translate
						.get("PRICING_BOOKING." + value)
						.subscribe((res) => (this.currentStatusTitle = res));
					return true;
				}
			});
		}

		this.dateObj = {
			days: "00",
			hours: "00",
			minutes: "00",
			seconds: "00",
		};

		this.createdBy = this.ebDemande.user.nom + " " + this.ebDemande.user.prenom;

		this.dateCreation = Statique.formatDate(this.ebDemande.dateCreation);
		if (this.ebDemande.curentTimeRemainingInMilliSecond > 1000) {
			this.subscription = interval(1000)
				.pipe(
					map((x) => {
						this.diff = Math.floor(this.ebDemande.curentTimeRemainingInMilliSecond);
					})
				)
				.subscribe(
					function(x) {
						this.dateObj = Statique.formatMillisToObject(this.diff, true);
					}.bind(this)
				);
		}

		if (this.ebDemande && this.ebDemande.ebDemandeNum) {
			this.formattedPickupTime = Statique.formatDateToStructure(
				this.ebDemande.exEbDemandeTransporteur.pickupTime
			);
		}

		if (typeof this.hasContributionAccess == "undefined") this.hasContributionAccess = true;
	}

	ngOnDestroy() {
		if (this.subscription) {
			this.subscription.unsubscribe();
			this.subscription = null;
		}
	}

	disableField() {
		return (
			this.isPrestataire() ||
			this.ebDemande.xEcStatut != DemandeStatus.WPU ||
			!this.hasContributionAccess
		);
	}

	isPrestataire() {
		return this.userConnected.role == UserRole.PRESTATAIRE;
	}

	isControlTower() {
		return this.userConnected.role == UserRole.CONTROL_TOWER;
	}

	confirmPickup(event) {
		// if(!this.formattedPickupTime) return;

		if (this.ebDemande.xEcStatut == DemandeStatus.WPU) this.ebDemande.xEcStatut = DemandeStatus.TO;

		let dm = new EbDemande();
		dm.ebDemandeNum = this.ebDemande.ebDemandeNum;
		dm.xEcStatut = this.ebDemande.xEcStatut;
		// dm.datePickup = this.ebDemande.datePickup;

		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.pricingService.confirmPickup(dm).subscribe(function() {
			Statique.setCurrentEbDemande(this.ebDemande);

			requestProcessing.afterGetResponse(event);
		});
	}

	addFacture(ebDemandeNum: number) {
		this.freighAuditService.addFacture(ebDemandeNum).subscribe();
		this.ebDemande.flagEbInvoice = true;
	}

	creationTrackTrace(event) {
		let dm = new EbDemande();
		dm = this.ebDemande;
		dm.listCommandes = null;
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.tracktraceService.addTrackTrace(this.ebDemande).subscribe(function() {
			requestProcessing.afterGetResponse(event);
		});
	}

	onChangeFlag(listFlag: Array<EbFlag>) {
		let toSendListFlag = "";
		if (listFlag) {
			listFlag.forEach((flag: EbFlag) => {
				if (toSendListFlag) toSendListFlag += ",";
				toSendListFlag += flag.ebFlagNum;
			});
		}

		this.ebDemande.listFlag = toSendListFlag;

		this.pricingService.saveFlags(this.ebDemande.ebDemandeNum, toSendListFlag).subscribe();
	}
}
