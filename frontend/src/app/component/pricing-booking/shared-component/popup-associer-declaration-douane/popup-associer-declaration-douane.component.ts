import { Component, Output, EventEmitter } from "@angular/core";
import { EbDemande } from "@app/classes/demande";
import { EbMarchandise } from "@app/classes/marchandise";
import { Statique } from "@app/utils/statique";
import { CustomDeclarationPricingDTO } from "@app/classes/CustomDeclarationPricingDTO";
import { DouaneService } from "@app/services/douane.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { AuthenticationService } from "@app/services/authentication.service";
import { PricingService } from "@app/services/pricing.service";
import { FichierJoint } from "@app/classes/fichiersJoint";
import { CustomDeclarationDocumentDTO } from "@app/classes/CustomDeclarationDocumentDTO";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { TypesDocumentService } from "@app/services/types-document.service";
import { Modules, UploadDirectory } from "@app/utils/enumeration";
import { Observable, forkJoin } from "rxjs";
import { MessageService } from "primeng/api";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-popup-associer-declaration-douane",
	templateUrl: "./popup-associer-declaration-douane.component.html",
	styleUrls: ["./popup-associer-declaration-douane.component.scss"],
})
export class PopupAssocierDeclarationDouaneComponent extends ConnectedUserComponent {
	popupOpened: boolean = false;

	@Output()
	onClose: EventEmitter<boolean> = new EventEmitter<boolean>();

	@Output()
	onOpen: EventEmitter<void> = new EventEmitter<void>();

	demande: EbDemande;
	module: number;
	requestDTO = new CustomDeclarationPricingDTO();

	listUnits: Array<EbMarchandise> = [];
	listDocs: Array<CustomDeclarationDocumentDTO> = [];
	listTypeDocument: Array<EbTypeDocuments> = [];

	listDocsToUpload: Array<any> = [];

	isLoadingDocs = true;

	constructor(
		protected douaneService: DouaneService,
		protected typesDocumentService: TypesDocumentService,
		protected authenticationService: AuthenticationService,
		protected messageService: MessageService,
		protected translate: TranslateService
	) {
		super(authenticationService);
	}

	close() {
		this.popupOpened = false;
		this.onClose.emit(false);

		this.listUnits = [];
	}

	open(demande: EbDemande, currentModule: number) {
		this.requestDTO = new CustomDeclarationPricingDTO();
		this.demande = demande;
		this.module = currentModule;
		this.requestDTO.ebDemandeNum = this.demande.ebDemandeNum;
		this.loadDocuments();
		this.loadTypeDocuments();

		// Copy items to prevent main view to be affected from this popup
		this.listUnits = Statique.cloneListObject(this.demande.listMarchandises, EbMarchandise);

		this.popupOpened = true;
		this.onOpen.emit();
	}

	get canValid(): boolean {
		return (
			this.requestDTO != null &&
			this.requestDTO.declarationDate != null &&
			this.requestDTO.declarationNumber &&
			this.listUnits.filter((u) => u.selectedForDeclaration === true).length > 0
		);
	}

	clickValid(event) {
		// Prepare object
		this.listUnits.forEach((u) => {
			if (u.selectedForDeclaration) {
				this.requestDTO.unitsNum.push(u.ebMarchandiseNum);
			}
		});
		this.listDocs.forEach((d) => {
			if (d.selectedForDeclaration) {
				this.requestDTO.docsNum.push(d.ebDemandeFichierJointNum);
			}
		});

		// Send object
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.douaneService.newDeclarationPricing(this.requestDTO).subscribe(
			(result) => {
				// Now send files
				let observableBatch = [];
				this.listDocsToUpload.forEach((file, index) => {
					if (file) {
						observableBatch.push(
							this.douaneService.uploadFileDeclaration(
								result.ebCustomDeclarationNum,
								UploadDirectory.CUSTOM,
								this.userConnected.ebUserNum,
								this.listTypeDocument[index].code,
								file
							)
						);
					}
				});

				if (observableBatch.length === 0) {
					requestProcessing.afterGetResponse(event);
					this.close();
					this.messageService.add({
						severity: "success",
						summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
						detail: this.translate.instant("CUSTOM.MSG_DECLARATION_DONE"),
					});
				} else {
					forkJoin(observableBatch).subscribe(
						(result) => {
							requestProcessing.afterGetResponse(event);
							this.close();
							this.messageService.add({
								severity: "success",
								summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
								detail: this.translate.instant("CUSTOM.MSG_DECLARATION_DONE"),
							});
						},
						(error) => {
							console.error(error);
							this.messageService.add({
								severity: "error",
								summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
								detail: this.translate.instant("CUSTOM.POPUP_DEC_MSG_ERROR_UPLOAD_DOC"),
							});
							requestProcessing.afterGetResponse(event);
							this.close();
						}
					);
				}
			},
			(error) => {
				this.messageService.add({
					severity: "error",
					summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
					detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
				});
				console.error(error);
				requestProcessing.afterGetResponse(event);
			}
		);
	}

	loadDocuments() {
		this.isLoadingDocs = true;

		this.douaneService
			.getListDocumentsByTypeDemande(
				this.module,
				this.demande.ebDemandeNum,
				this.userConnected.ebUserNum
			)
			.subscribe((data) => {
				this.listDocs = data;
				this.isLoadingDocs = false;
			});
	}

	loadTypeDocuments() {
		this.typesDocumentService.getListTypeDocumentByModuleNum(Modules.CUSTOM).subscribe((res) => {
			let listTypeDocument = Statique.cloneListObject(res, EbTypeDocuments);
			this.listDocsToUpload = new Array(listTypeDocument.length);
			this.listTypeDocument = listTypeDocument;
		});
	}

	onFileSelected(event, index: number) {
		if (event && event.target && event.target.files && event.target.files[0]) {
			this.listDocsToUpload[index] = event.target.files[0];
		}
	}
}
