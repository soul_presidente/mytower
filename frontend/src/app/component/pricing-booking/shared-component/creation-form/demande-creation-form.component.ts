import { ExportControlStatut } from "./../../../../utils/enumeration";
import { filter } from "rxjs/operators";
import {
	AfterViewInit,
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ViewChild,
	OnChanges,
	SimpleChanges,
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { EbDemande } from "@app/classes/demande";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { EcCountry } from "@app/classes/country";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { PricingService } from "@app/services/pricing.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { AuthenticationService } from "@app/services/authentication.service";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import { CollectionService } from "@app/services/collection.service";
import { Statique } from "@app/utils/statique";
import { RequestProcessing } from "@app/utils/requestProcessing";
import {
	CarrierStatus,
	DemandeStatus,
	Modules,
	ServiceType,
	AccessRights,
	QrCreationStep,
	NatureDemandeTransport,
} from "@app/utils/enumeration";
import { EbUser } from "@app/classes/user";
import { MessageService } from "primeng/api";
import { EbCostCenter } from "@app/classes/costCenter";
import { IField, CustomField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { StepExpeditionComponent } from "./components/step-expedition/step-expedition.component";
import { StepPricingComponent } from "./components/step-pricing/step-pricing.component";
import { StepReferenceComponent } from "./components/step-reference/step-reference.component";
import { StepUnitComponent } from "./components/step-unit/step-unit.component";
import { ConfigPslCompanyService } from "@app/services/config-psl-company.service";
import { PricingMaskFillerComponent } from "@app/component/saved-forms/pricing-mask/pricing-mask-filler.component";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { DialogService } from "@app/services/dialog.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { SavedForm } from "@app/classes/savedForm";
import { TranslateService } from "@ngx-translate/core";
import { EbChat } from "@app/classes/chat";
import { StepInfosAboutTransportComponent } from "./components/step-infos-about-transport/step-infos-about-transport.component";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { EbQrGroupeProposition } from "@app/classes/qrGroupeProposition";
import { EbQrGroupe } from "@app/classes/qrGroupe";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbTypeTransport } from "@app/classes/EbTypeTransport";
import { EbTypeFlux } from "@app/classes/EbTypeFlux";
import { EbIncoterm } from "@app/classes/EbIncoterm";
import { ConfigIncotermService } from "@app/services/config-incoterm.service";
import { StepDocumentsComponent } from "./components/step-documents/step-documents.component";
import { PricingCreationFormStepBaseComponent } from "./components/core/wf-step-base.component";
import { EbEtablissement } from "@app/classes/etablissement";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { ControlRuleService } from "@app/services/control-rule.service";
import { DocumentsComponent } from "@app/shared/documents/documents.component";
import moment from "moment";
import { map } from "rxjs/operators";
import { EbTypeGoods } from "@app/classes/EbTypeGoods";
import { TypeGoodsService } from "@app/services/type-goods.service";
import { StepConsolidationComponent } from "./components/step-consolidation/step-consolidation.component";
import { UserService } from "@app/services/user.service";
import { Cost, CostCategorie } from "@app/classes/costCategorie";
import { EbPlCostItemDTO } from "@app/classes/trpl/EbPlCostItemDTO";

@Component({
	selector: "app-demande-creation-form",
	templateUrl: "./demande-creation-form.component.html",
	styleUrls: [
		"../../../../shared/modal/modal.component.scss",
		"./demande-creation-form.component.scss",
	],
})
export class DemandeCreationFormComponent extends PricingMaskFillerComponent
	implements OnInit, AfterViewInit, OnChanges {
	ServiceType = ServiceType;
	DemandeStatus = DemandeStatus;
	Modules = Modules;
	NatureDemandeTransport = NatureDemandeTransport;

	Step = QrCreationStep; // Stpe enumeration
	stepsOrdered = new Array<string>();
	stepsComponents = new Map<string, PricingCreationFormStepBaseComponent>();

	@Input()
	isTF: boolean = false;

	@Input()
	isForGrouping: boolean = false;

	optionInfoAboutTransport: boolean = false;

	@Input()
	demande: EbDemande = new EbDemande();

	@Input()
	showOverview: boolean = true;

	@Input()
	showStepNavigation: boolean = true;

	@Input()
	selectedStep: string = QrCreationStep.EXPEDITION;
	@Output()
	selectedStepChange: EventEmitter<string> = new EventEmitter<string>();
	@Output()
	updateChatEmitter: EventEmitter<EbChat> = new EventEmitter<EbChat>();

	@Input()
	enableCardStyle: boolean = true;

	@Input()
	hasContributionAccess: boolean = true;

	// Step Items
	@ViewChild(StepExpeditionComponent, { static: true })
	stepExpedition: StepExpeditionComponent;

	@ViewChild(StepReferenceComponent, { static: true })
	stepReference: StepReferenceComponent;

	@ViewChild(StepUnitComponent, { static: true })
	stepUnit: StepUnitComponent;

	@ViewChild(StepConsolidationComponent, { static: true })
	stepConsolidation: StepConsolidationComponent;

	@ViewChild(StepPricingComponent, { static: true })
	stepPricing: StepPricingComponent;

	@ViewChild(StepInfosAboutTransportComponent, { static: true })
	stepInfosAboutTransport: StepInfosAboutTransportComponent;

	@ViewChild(StepDocumentsComponent, { static: true })
	stepDocuments: StepDocumentsComponent;
	// END Step Items

	@Input()
	listTypeFlux: Array<EbTypeFlux> = new Array<EbTypeFlux>();
	@Input()
	listSchemaPsl: Array<EbTtSchemaPsl> = new Array<EbTtSchemaPsl>();
	@Input()
	listIncoterms: Array<EbIncoterm> = new Array<EbIncoterm>();

	listTypeFluxDataLoaded = false;
	listCustomsFields: Array<IField> = new Array<IField>();

	@Input()
	displayOptions: boolean = false;

	@Input()
	showInfoPricing: boolean = false;

	refreshDisabled: boolean = false;

	showAddressPopup: boolean = false;
	get loading(): boolean {
		return !this.listLoaded;
	}

	// Lists
	listCountry: Array<EcCountry> = new Array<EcCountry>();
	listCountryLoaded = false;
	listZone: Array<EbPlZoneDTO> = new Array<EbPlZoneDTO>();
	listZoneLoaded = false;

	@Input()
	listModeTransport: Array<EbModeTransport>;
	@Input()
	listTypeTransport: Map<number, Array<EbTypeTransport>>;

	listTypeUnit: Array<any>;
	listModeTransportLoaded = false;
	listPsl: Array<string> = new Array<string>();
	listPslLoaded = false;

	listCostCenter: Array<EbCostCenter>;
	//END Lists

	newDemandeQuote: ExEbDemandeTransporteur;
	selectedUser: EbUser = null;

	nbSavedFormLoaded: number = 0;
	listCategorieSelected: Array<EbCategorie>;
	typeTransport: string;
	listSavedForm: SavedForm[] = [];

	listTypeDemandeByCompagnie: Array<EbTypeRequestDTO>;

	@Input()
	groupProposition: EbQrGroupeProposition;
	@Input()
	qrGroup: EbQrGroupe;

	listTransporteurPreference: EbUser[];

	listTransporteurTDC: EbUser[] = null;

	deletedCustomFields: Array<IField>;
	newEbDemandeNum: number;

	draftMode: boolean = false;

	// edit management
	@Input()
	isPendingStatus: boolean = false;
	@Input()
	readOnly: boolean = false;
	@Input()
	requiredReadOnly: boolean = false;

	statusGroupage: number;

	listExportControl: Array<any>;
	listCountries: Array<EcCountry>;

	isCreation: boolean = false;
	listCategoriesEtab: Array<EbCategorie>;

	@Input()
	listExchangeRate: Array<EbCompagnieCurrency>;

	constructor(
		protected translate: TranslateService,
		protected authenticationService: AuthenticationService,
		protected router: Router,
		protected transportationPlanService: TransportationPlanService,
		protected pricingService: PricingService,
		protected etablissementService: EtablissementService,
		protected collectionService: CollectionService,
		protected savedFormsService: SavedFormsService,
		protected dialogService: DialogService,
		protected modalService: ModalService,
		protected activatedRoute: ActivatedRoute,
		protected controlRuleService: ControlRuleService,
		protected configIncoterms?: ConfigIncotermService,
		protected messageService?: MessageService,
		protected typeGoodsService?: TypeGoodsService,
		protected companyPslService?: ConfigPslCompanyService,
		protected userService?: UserService
	) {
		super(savedFormsService, dialogService, modalService, translate);
	}

	ngOnInit() {
		if (window.history.state.demande !== undefined) this.demande = window.history.state.demande;
		this.loadListTransporteurAutocomplete();
		this.initStepInformations();
		this.loadLists();
		this.initSavedFormObserver();
		this.setContributionAccess();
		this.setCreationMode();
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["demande"]) {
			if (this.demande.listCategories && this.listCategoriesEtab) {
				this.demande.listCategories.forEach((it) => {
					let cat: EbCategorie = this.listCategoriesEtab.find(
						(ca) => ca.ebCategorieNum == it.ebCategorieNum
					);
					if (it.labels && it.labels.length) {
						it.selectedLabel = it.labels[0];
					}
					it.labels = cat.labels;
				});
			}
		}
	}

	setCreationMode() {
		this.isCreation = this.router.url.indexOf("/creation") >= 0;
	}
	refreshCost(demande) {
		let _demande = this.prepareEbDemandeForSave(false, false);
		let num = Statique.getInnerObjectStr(_demande, "ebQrGroupe.ebQrGroupeNum");
		if (num) {
			_demande.ebQrGroupe = new EbQrGroupe();
			_demande.ebQrGroupe.ebQrGroupeNum = num;
		}
		if (!_demande.exEbDemandeTransporteurs || _demande.exEbDemandeTransporteurs.length == 0) {
			_demande.exEbDemandeTransporteur.xEbDemande = null;
			_demande.exEbDemandeTransporteurs.push(_demande.exEbDemandeTransporteur);
		}
		this.refreshDisabled = true;
		this.pricingService.valorisationMTC(_demande).subscribe((res: EbDemande) => {
			this.demande.exEbDemandeTransporteurs = res.exEbDemandeTransporteurs;
			this.demande.consolidatedPrice = 0;
			this.demande.exEbDemandeTransporteurs.forEach(
				(q) => (this.demande.consolidatedPrice = this.demande.consolidatedPrice + q.price)
			);
			this.stepPricing.loadListViewOnly();
			this.refreshDisabled = false;
			this.statusGroupage = res.xEcStatutGroupage;
		});
		this.stepPricing.loadListViewOnly();
	}
	setContributionAccess() {
		if (this.userConnected.listAccessRights && this.userConnected.listAccessRights.length > 0) {
			this.hasContributionAccess =
				this.userConnected.listAccessRights.find((access) => {
					return (
						access.ecModuleNum == (this.isTF ? Modules.TRANSPORT_MANAGEMENT : Modules.PRICING) &&
						access.accessRight == AccessRights.CONTRIBUTION
					);
				}) != undefined;
		} else {
			this.hasContributionAccess = false;
		}
	}

	initSavedFormObserver() {
		this.activatedRoute.queryParams.subscribe((params) => {
			let num = params["sf"];
			if (num) {
				let sf: SavedForm = JSON.parse(localStorage.getItem("sf" + num));

				if (sf && sf.ebSavedFormNum == +num) {
					Statique.removeLSItemsWithPrefix("sf");
					this.fillFormWithSf(sf);
					this.demande.exEbDemandeTransporteurs = new Array<ExEbDemandeTransporteur>();
					this.demande.listTransporteurs = new Array<EbUser>();
				}
			}
		});
	}

	//affectation de la statique liste des categories sans avoire perdre le libelle selectionné
	editDemandeCategoriesWithSelectedCategories(newList: Array<EbCategorie>) {
		if (!newList || !newList.length) {
			this.demande.listCategories = this.listCategorieSelected;

			if (this.demande.listCategories) {
				this.demande.listCategories.forEach((it) => {
					if (it.labels && it.labels.length) it.selectedLabel = it.labels[0];
				});
			} else {
				this.demande.listCategories = [];
			}
		} else {
			if (this.listCategorieSelected && this.listCategorieSelected.length > 0) {
				this.listCategorieSelected.forEach((selectedCat) => {
					if (selectedCat.labels && selectedCat.labels.length == 1) {
						let found = false;
						for (let i = 0; i < newList.length; i++) {
							if (newList[i].ebCategorieNum == selectedCat.ebCategorieNum) {
								newList[i].selectedLabel = selectedCat.labels[0];
								found = true;
							}
						}
						if (!found) {
							newList.push(selectedCat);
						}
					}
				});
			}

			this.demande.listCategories =
				newList && newList.length > 0 ? newList : new Array<EbCategorie>();
		}

		this.listCategorie = Statique.cloneObject(
			this.demande.listCategories,
			new Array<EbCategorie>()
		);
	}

	async cloneEbDemandeWithSf(parsedValues: EbDemande, fieldsAndCategoriesOnly = false) {
		this.demande = new EbDemande();

		if (!fieldsAndCategoriesOnly) {
			this.demande.cloneData(parsedValues);
			if (!this.demande.user || !this.demande.user.ebUserNum) {
				if (this.isChargeur) {
					let user = new EbUser();
					user.constructorCopy(this.userConnected);

					this.demande.user = new EbUser();
					this.demande.user.constructorCopy(user);

					this.stepExpedition.handleSelectedUserChange(user);
				}
			}
		}
		if (this.demande.listCategories && this.demande.listCategories.length > 0) {
			this.demande.listCategories.forEach((categorie) => {
				let cat = parsedValues.listCategories.find(
					(it) => it.ebCategorieNum == categorie.ebCategorieNum
				);
				categorie.selectedLabel =
					cat && cat.labels && cat.labels.length > 0 ? cat.labels[0] : categorie.selectedLabel;
			});
		}
		if (this.demande.customFields && this.demande.customFields.length > 0) {
			let fields: Array<IField> = this.demande.customFields
				? JSON.parse(this.demande.customFields)
				: new Array<IField>();

			this.demande.listCustomsFields = fields;
		}

		// Prevent non exist values
		if (this.demande.xecCurrencyInvoice && !this.demande.xecCurrencyInvoice.ecCurrencyNum) {
			this.demande.xecCurrencyInvoice = null;
		}

		this.demande.exEbDemandeTransporteurs = Statique.cloneObject(
			parsedValues.exEbDemandeTransporteurs,
			new Array<ExEbDemandeTransporteur>()
		);

		this.nbSavedFormLoaded++;

		this.getDataPreference();
	}
	// je pense que on peut utiliser ces deux methods
	setCatgoriesAndCustomFields() {
		if (this.demande.listCategories) {
			this.demande.listCategories.forEach((elem) => {
				elem.labels = elem.selectedLabel ? [elem.selectedLabel] : [];
			});
		}
		this.demande.customFields = JSON.stringify(this.demande.listCustomsFields);
	}

	ngAfterViewInit() {
		this.stepsComponents.set(QrCreationStep.EXPEDITION, this.stepExpedition);
		this.stepsComponents.set(QrCreationStep.REFERENCE, this.stepReference);
		this.stepsComponents.set(QrCreationStep.UNITS, this.stepUnit);
		this.stepsComponents.set(QrCreationStep.CONSOLIDATION, this.stepConsolidation);
		this.stepsComponents.set(QrCreationStep.PRICING, this.stepPricing);
		this.stepsComponents.set(QrCreationStep.INFOS_ABOUT_TRANSPORT, this.stepInfosAboutTransport);
		this.stepsComponents.set(QrCreationStep.DOCUMENTS, this.stepDocuments);

		this.subscribeToSavedFormService();
		this.displayOptions = this.stepsComponents.get(QrCreationStep.EXPEDITION).optionsDisplayed;
		if (!this.readOnly) this.showOverview = !this.displayOptions;
	}

	canNavigateToStep(stepCode: string): boolean {
		if (!this.isCreation || this.draftMode) return true;

		let steps = this.getVisibleStepsCodeOrdered();
		let result: boolean = undefined;
		steps.forEach((step) => {
			if (step === stepCode && result == undefined) {
				result = true;
			}
			if (!this.stepsComponents.get(step).valid && result == undefined) {
				result = false;
			}
		});

		return result != undefined ? result : true;
	}

	selectStep(stepCode: string) {
		if (this.canNavigateToStep(stepCode)) {
			this.selectedStep = stepCode;
			this.selectedStepChange.emit(this.selectedStep);

			this.postSelectStep(stepCode, true);
		} else {
			this.postSelectStep(stepCode, false);
		}
	}

	postSelectStep(stepCode: string, selectAllowed: boolean) {
		if (!this.stepsComponents.get(this.selectedStep).optionsDisplayed) {
			this.showOverview = true;
			this.displayOptions = false;
		} else {
			this.showOverview = false;
			this.displayOptions = true;
		}

		if (!selectAllowed) return;

		// Reload demande wahrehouse & sla dates after select
		if (stepCode === QrCreationStep.UNITS && !this.draftMode) {
			this.reApplyRulesWithExportControlEnabled();
		}

		// Reload carriers on step pricing after select
		if (stepCode === QrCreationStep.PRICING && !this.draftMode) {
			this.stepPricing.reloadCarriers(null, false);
		}

		// Info about transport connect step
		if (!this.stepsComponents.get(stepCode) && stepCode === QrCreationStep.INFOS_ABOUT_TRANSPORT) {
			this.displayOptions = this.stepsComponents.get(stepCode).optionsDisplayed;
			this.showOverview = !this.displayOptions;
		}

		// Notification when Leg 2 and quantity > 1
		if (stepCode === QrCreationStep.PRICING && this.demande.ebDemandeNum === null) {
			this.demande.listMarchandises.forEach((marchandise) => {
				let $this = this;
				if (
					stepCode === QrCreationStep.PRICING &&
					this.isCreation &&
					(marchandise.numberOfUnits > 1 && marchandise.cptmProcedureNum)
				) {
					this.modalService.confirm(
						this.translate.instant("REQUEST_OVERVIEW.WARNING"),
						this.translate.instant("REQUEST_OVERVIEW.UNIT_LEG_MESSAGE"),
						function() {},
						function() {
							$this.selectedStep = QrCreationStep.UNITS;
						},
						true,
						this.translate.instant("GENERAL.CONTINUE"),
						this.translate.instant("GENERAL.CANCEL")
					);
				}
			});
		}

		// Hide adresses
		this.showAddressPopup = false;
	}

	selectNextStep() {
		if (!this.selectedStepValidated && !this.draftMode) {
			this.translate.get("MODAL.FILL_FIELDS").subscribe((text: string) => {
				const title = text.split(";")[0];
				const body = text.split(";")[1];
				this.modalService.error(title, body, null);
			});
			return;
		}
		this.selectStep(this.getNextStep());
	}

	selectPrevStep() {
		this.selectStep(this.getPrevStep());
	}

	clickOptions() {
		this.stepsComponents.get(this.selectedStep).toggleDisplayOptions();
		// displayOptions prends l'état du variable optionsDisplayed quand j'affiche ou masque le bloc des options
		this.displayOptions = this.stepsComponents.get(this.selectedStep).optionsDisplayed;
		this.showOverview = !this.displayOptions;
		this.showAddressPopup = false;
	}

	checkGlobalValidity(includeNonVisible = false): boolean {
		let steps: Array<string>;
		if (!includeNonVisible) {
			steps = this.getVisibleStepsCodeOrdered();
		} else {
			steps = this.stepsOrdered;
		}
		let result = true;
		steps.forEach((step) => {
			if (this.stepsComponents.get(step) == null || !this.stepsComponents.get(step).valid) {
				result = false;
			}
		});
		return result;
	}

	isStepValidated(stepCode: string): boolean {
		let stepItem = this.stepsComponents.get(stepCode);

		if (!stepItem) return false;
		return stepItem.valid;
	}

	isStepHasOptions(stepCode: string): boolean {
		let stepItem = this.stepsComponents.get(stepCode);

		if (!stepItem) return false;
		return stepItem.hasOptions;
	}

	get selectedStepValidated(): boolean {
		return this.isStepValidated(this.selectedStep);
	}

	get selectedStepHasOptions(): boolean {
		return this.isStepHasOptions(this.selectedStep);
	}

	public get confirmableCarrier(): ExEbDemandeTransporteur {
		if (!this.demande.exEbDemandeTransporteurs) return null;

		if (this.demande.exEbDemandeTransporteurs.length < 1) return null;

		let foundTransporteur: ExEbDemandeTransporteur = null;
		if (!this.isTF || this.isPendingStatus) {
			if (
				this.demande.exEbDemandeTransporteurs.length == 1 &&
				((this.demande.exEbDemandeTransporteurs[0].xTransporteur &&
					this.demande.exEbDemandeTransporteurs[0].xTransporteur.calculatedPrice) ||
					this.demande.exEbDemandeTransporteurs[0].price)
			)
				foundTransporteur = this.demande.exEbDemandeTransporteurs[0];
		} else {
			foundTransporteur = this.demande.exEbDemandeTransporteurs.find((it) => {
				if (it.status >= CarrierStatus.FINAL_CHOICE || it.status == CarrierStatus.FIN_PLAN)
					return true;
			});
		}

		return foundTransporteur || null;
	}

	//#region Lists
	get listLoaded(): boolean {
		return (
			this.listCountryLoaded &&
			this.listTypeFluxDataLoaded &&
			this.listZoneLoaded &&
			this.listModeTransportLoaded &&
			this.listPslLoaded
		);
	}

	private loadLists() {
		this.getListTypeFluxDatas();
		this.getStatiques();
		this.loadZones();
		this.loadCountries();
	}

	getListTypeFluxDatas() {
		let criteria = new SearchCriteria();
		if (this.demande.user) {
			criteria.ebCompagnieNum = this.demande.user.ebCompagnie.ebCompagnieNum;
		} else {
			criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		}

		(async function() {
			this.listIncoterms = await this.configIncoterms.getListIncotermsPromise(criteria);
			this.listSchemaPsl = await this.companyPslService.getListSchemaPslPromise(criteria);
		}.bind(this)());

		this.companyPslService
			.getListSchemaPslTable(criteria)
			.subscribe((data: Array<EbTtSchemaPsl>) => {
				this.listSchemaPsl = data;
			});
		this.etablissementService.listTypeFlux(criteria).subscribe((data: Array<EbTypeFlux>) => {
			this.listTypeFlux = data;
			this.listTypeFluxDataLoaded = true;
		});
	}

	private loadZones() {
		let criteria = new SearchCriteria();
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		this.transportationPlanService.listZone(criteria).subscribe((data: Array<EbPlZoneDTO>) => {
			this.listZone = data;
			this.listZoneLoaded = true;
		});
	}

	private loadCountries() {
		this.pricingService.listPays().subscribe((data) => {
			this.listCountry = data;
			this.listCountryLoaded = true;
		});
	}

	private getStatiques() {
		(async function() {
			this.listTypeTransport = await SingletonStatique.getListTypeTransportMap();
		}.bind(this)());

		(async function() {
			this.listModeTransport = await SingletonStatique.getListModeTransport();
			this.listModeTransport = this.listModeTransport.sort((a, b) => a.order - b.order);
			this.listModeTransportLoaded = true;
		}.bind(this)());
	}

	//#endregion

	//#region Save
	public prepareEbDemandeForSave(
		isConfirmCarrier: boolean = false,
		isDraft: boolean = false
	): EbDemande {
		let _ebDemande: EbDemande = Statique.cloneObject(this.demande, new EbDemande());
		//Le groupe est mis à null pour eviter un probleme au moment de la sauvegarde de demande lors de la creation de demande ou d'un draft
		_ebDemande.xEbQrGroupe = null;
		if (_ebDemande.ebDemandeNum == null && this.newEbDemandeNum != null) {
			_ebDemande.ebDemandeNum = this.newEbDemandeNum;
		}

		//_ebDemande.ebPartyOrigin = this.stepExpeditionInfosItem.ebDemande.ebPartyOrigin;
		//_ebDemande.ebPartyDest = this.stepExpeditionInfosItem.ebDemande.ebPartyOrigin;
		if (
			_ebDemande.ebPartyDest &&
			(!_ebDemande.ebPartyDest.xEbUserVisibility ||
				!_ebDemande.ebPartyDest.xEbUserVisibility.ebUserNum)
		)
			_ebDemande.ebPartyDest.xEbUserVisibility = null;
		if (
			_ebDemande.ebPartyDest &&
			(!_ebDemande.ebPartyDest.etablissementAdresse ||
				!_ebDemande.ebPartyDest.etablissementAdresse.ebEtablissementNum)
		)
			_ebDemande.ebPartyDest.etablissementAdresse = null;
		if (
			_ebDemande.ebPartyNotif &&
			(!_ebDemande.ebPartyNotif.xEbUserVisibility ||
				!_ebDemande.ebPartyNotif.xEbUserVisibility.ebUserNum)
		)
			_ebDemande.ebPartyNotif.xEbUserVisibility = null;
		if (
			_ebDemande.ebPartyNotif &&
			(!_ebDemande.ebPartyNotif.etablissementAdresse ||
				!_ebDemande.ebPartyNotif.etablissementAdresse.ebEtablissementNum)
		)
			_ebDemande.ebPartyNotif.etablissementAdresse = null;
		if (
			_ebDemande.ebPartyOrigin &&
			(!_ebDemande.ebPartyOrigin.xEbUserVisibility ||
				!_ebDemande.ebPartyOrigin.xEbUserVisibility.ebUserNum)
		)
			_ebDemande.ebPartyOrigin.xEbUserVisibility = null;
		if (
			_ebDemande.ebPartyOrigin &&
			(!_ebDemande.ebPartyOrigin.etablissementAdresse ||
				!_ebDemande.ebPartyOrigin.etablissementAdresse.ebEtablissementNum)
		)
			_ebDemande.ebPartyOrigin.etablissementAdresse = null;

		_ebDemande.listCategoriesStr = "";
		_ebDemande.listLabels = "";
		///supp de la list des categories et mettre a sa place l'element selecter
		if (_ebDemande.listCategories) {
			_ebDemande.listCategories.forEach((elem) => {
				if (elem.selectedLabel) {
					elem.labels = [elem.selectedLabel];
				} else if (elem.labels && elem.labels.length > 1) {
					elem.labels = [];
				}
			});
		}
		_ebDemande.customFields = JSON.stringify(_ebDemande.listCustomsFields);

		let hasFinalChoice = false;
		_ebDemande.exEbDemandeTransporteurs = this.stepPricing.demande.exEbDemandeTransporteurs;
		_ebDemande.exEbDemandeTransporteurs.forEach((tr) => {
			tr.xEbDemande = null;
			if (tr.xEbCompagnieCurrency && !tr.xEbCompagnieCurrency.ebCompagnieCurrencyNum)
				tr.xEbCompagnieCurrency = null;
			if (!tr.xEcModeTransport) tr.xEcModeTransport = _ebDemande.xEcModeTransport;
			if (!hasFinalChoice && tr.status == CarrierStatus.FINAL_CHOICE && this.isTF) {
				hasFinalChoice = true;
				tr.isChecked;
				tr.xTransporteur.selected = true;
				this.addPriceToOtherCost(tr);
			} else if (tr.price || tr.isFromTransPlan) {
				if (isConfirmCarrier && !this.isTF) {
					tr.status = tr.isFromTransPlan ? CarrierStatus.FIN_PLAN : CarrierStatus.FINAL_CHOICE;
					this.addPriceToOtherCost(tr);
				} else if (tr.status != CarrierStatus.QUOTE_SENT) {
					tr.status = CarrierStatus.TRANSPORT_PLAN;
				}
			} else {
				if (!tr.status) tr.status = CarrierStatus.REQUEST_RECEIVED;
			}
		});

		_ebDemande.listMarchandises.forEach((it) => {
			let customFields = JSON.stringify(it.listCustomsFields);
			it.customFields = customFields;

			let d = it.ebDemande;
			if (d && d.ebDemandeNum) {
				it.ebDemande = new EbDemande();
				it.ebDemande.ebDemandeNum = d.ebDemandeNum;
			} else it.ebDemande = null;
		});

		if (_ebDemande.xEbEntrepotUnloading && !_ebDemande.xEbEntrepotUnloading.ebEntrepotNum)
			_ebDemande.xEbEntrepotUnloading = null;
		if (
			_ebDemande.pointIntermediate &&
			_ebDemande.pointIntermediate.zone &&
			!_ebDemande.pointIntermediate.zone.ebZoneNum
		)
			_ebDemande.pointIntermediate.zone = null;

		_ebDemande.xEbSchemaPsl &&
			_ebDemande.xEbSchemaPsl.listPsl &&
			_ebDemande.xEbSchemaPsl.listPsl.forEach((it: any) => {
				if (it.expectedDate) {
					it["expectedDateTime"] = new Date(it.expectedDate).getTime();
				}
			});

		_ebDemande.isDraft = isDraft;

		if (Statique.getInnerObjectStr(_ebDemande, "xEbEtablissement.ebEtablissementNum"))
			_ebDemande.xEbEtablissement = EbEtablissement.create(
				_ebDemande.xEbEtablissement.ebEtablissementNum
			);
		if (this.stepPricing.additionalCostComponent != undefined) {
			_ebDemande.listAdditionalCost = this.stepPricing.additionalCostComponent.ebDemande.listAdditionalCost;
		}

		_ebDemande.dateOfGoodsAvailability = this.demande.dateOfGoodsAvailability;
		_ebDemande.dateOfArrival = this.demande.dateOfArrival;
		_ebDemande.dateDelivery = this.demande.dateDelivery;
		_ebDemande.xEbUserOrigin = this.demande.xEbUserOrigin;
		_ebDemande.xEbUserDest = this.demande.xEbUserDest;
		_ebDemande.customerCreationDate = this.demande.customerCreationDate;
		let startPslEstimDate = null,
			endPslEstimDate = null;
		_ebDemande.xEbSchemaPsl &&
			_ebDemande.xEbSchemaPsl.listPsl &&
			_ebDemande.xEbSchemaPsl.listPsl.forEach((psl) => {
				if (psl.startPsl) {
					startPslEstimDate = psl.expectedDate;
				}
				if (psl.endPsl) {
					endPslEstimDate = psl.expectedDate;
				}
			});
		_ebDemande.datePickupTM = startPslEstimDate;
		_ebDemande.finalDelivery = endPslEstimDate;
		return _ebDemande;
	}


	addPriceToOtherCost(exEbDemandeTransporteur: ExEbDemandeTransporteur) {
		if(exEbDemandeTransporteur.isFromTransPlan){
			let otherCostIndex = exEbDemandeTransporteur.listPlCostItem.findIndex(cost => cost.code == "OC");
			if(otherCostIndex == -1) {
				let costItem = this.fillNewCostItemForTransportPlan(exEbDemandeTransporteur.listPlCostItem);
				exEbDemandeTransporteur.listPlCostItem.push(costItem);
			}
		}else {
			let categorie: CostCategorie = exEbDemandeTransporteur.listCostCategorie[0];
			let otherCostIndex = categorie.listEbCost.findIndex(cost => cost.code == "OC");
			if(otherCostIndex == -1) {
				let costItem = this.fillNewCostItem(categorie);
				costItem.libelle = "Other costs";
				costItem.code = "OC";
				costItem.price = 0;
				categorie.listEbCost.push(costItem);
			}
		}
	}

	fillNewCostItem(categorie : CostCategorie): Cost {
		let cost = new Cost();
		let listCostItem: Array<Cost> = categorie.listEbCost.sort((a, b) => a.ebCostNum - b.ebCostNum);
		cost.ebCostNum = listCostItem[listCostItem.length - 1].ebCostNum + 1;
		cost.ebCostOrder = listCostItem[listCostItem.length - 1].ebCostNum + 1;
		cost.price = 0;
		cost.type = 2;
		return cost;
	}

	fillNewCostItemForTransportPlan(ebPlCostItem : Array<EbPlCostItemDTO>): EbPlCostItemDTO {
		let cost = new EbPlCostItemDTO();
		let listCostItem: Array<EbPlCostItemDTO> = ebPlCostItem.sort((a, b) => a.costItemNum - b.costItemNum);
		cost.costItemNum = listCostItem[listCostItem.length - 1].costItemNum + 1;
		cost.libelle = "Other costs";
		cost.code = "OC";
		cost.price = 0;
		cost.fixedFees = 0;
		cost.type = 2;
		return cost;
	}

	addDemande(
		event: any,
		isConfirmCarrier: boolean = false,
		isTrack: boolean = false,
		isDraft: boolean = false
	) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		let _ebDemande = this.prepareEbDemandeForSave(isConfirmCarrier, isDraft);
		let md = this.isTF || isConfirmCarrier ? Modules.TRANSPORT_MANAGEMENT : Modules.PRICING;

		this.pricingService.ajouterEbDemande(_ebDemande, md).subscribe(
			(ebDemande: EbDemande) => {
				requestProcessing.afterGetResponse(event);
				this.showSuccess();
				if (!isTrack) {
					this.router.navigate(["/app/pricing/details/" + ebDemande.ebDemandeNum]);
				} else {
					this.router.navigate(["/app/track-trace/dashboard"], {
						queryParams: {
							filterByDemande: ebDemande.ebDemandeNum,
							displayAddEvent: false,
						},
					});
				}
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				this.showError();
			}
		);
	}

	createDraft(event: any) {
		this.addDemande(event, false, false, true);
	}

	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}

	confirmDemande(event: any) {
		this.addDemande(event, true);
	}
	createAndTrackAction(event: any) {
		this.addDemande(event, true, true);
	}

	getDataPreference(event: any = null) {
		if (this.demande.listCategories) this.listCategorieSelected = this.demande.listCategories;

		this.demande.listCustomsFields = null;
		this.demande.listCategories = null;
		this.listCostCenter = null;

		let criteria: SearchCriteria = new SearchCriteria();
		criteria.size = null;
		criteria.ebDemandeNum = this.demande.ebDemandeNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		criteria.withActiveUsers = true;

		if (!this.demande.ebDemandeNum) {
			if (this.demande.user && this.demande.user.ebUserNum) {
				criteria.ebCompagnieNum = this.demande.user.ebCompagnie.ebCompagnieNum;
				criteria.ebEtablissementNum = this.demande.user.ebEtablissement.ebEtablissementNum;
				criteria.contact = true;
				criteria.ebUserNum = this.demande.user.ebUserNum;
			}
		} else if (this.demande.ebDemandeNum) {
			criteria.ebCompagnieNum = this.demande.xEbEtablissement.ebCompagnie.ebCompagnieNum;
			criteria.ebEtablissementNum = this.demande.xEbEtablissement.ebEtablissementNum;
			criteria.ebUserNum = this.demande.user.ebUserNum;
			criteria.contact = true;
		} else return;

		if (!criteria.ebEtablissementNum || !criteria.ebCompagnieNum) {
			this.demande.listCustomsFields = new Array<IField>();
			this.demande.listCategories = new Array<EbCategorie>();
			this.listCostCenter = new Array<EbCostCenter>();
			return;
		}

		this.etablissementService.getDataPreference(criteria).subscribe(async (res) => {
			this.listCostCenter = this.getListCostCenterFiltered(res);
			this.listCategoriesEtab = res.listCategorie;
			this.listTransporteurPreference = res.listTransporteur;
			if(this.listTransporteurTDC != null && this.listTransporteurTDC.length > 0)
				this.listTransporteurTDC.forEach((it) => {
					this.listTransporteurPreference.push(it);
					this.listTransporteurPreference = this.listTransporteurPreference.filter(
						(t, i, a) => i === a.findIndex((f) => f.ebUserNum === t.ebUserNum && f.email === t.email)
					);
				});
			if (res && res.listCategorie && res.listCategorie.length)
				this.editDemandeCategoriesWithSelectedCategories(res.listCategorie);
			else this.editDemandeCategoriesWithSelectedCategories(null);

			this.demande.listCustomsFields =
				res && res.customField && res.customField.fields
					? JSON.parse(res.customField.fields)
					: new Array<IField>();

			this.demande.listCustomsFields = this.demande.listCustomsFields || new Array<IField>();

			if (!this.demande.ebDemandeNum) {
				this.demande.listCustomsFields = this.demande.listCustomsFields.filter(
					(cf) => cf.isdefault
				);
			}

			this.listTypeFlux = res.typeOfFlux;
			this.listSchemaPsl = res.listSchemaPsl;
			this.listIncoterms = res.listIncoterm;
			this.listExportControl = res.listExportControl;
			this.listCountries = res.countries;

			this.listTypeDemandeByCompagnie = res.listTypeRequest;

			this.listTypeUnit = res.listTypeUnit;

			if (!this.demande.ebDemandeNum && !this.isForGrouping) {
				this.demande.exEbDemandeTransporteurs = []; // mask quotes conflict with valorised ones
				this.stepPricing.loadListsEdition(this.listTransporteurPreference);
			}

			if (this.demande.listCategories && this.demande.listCategories.length > 0) {
				this.demande.listCategories.forEach((it) => {
					if (it.labels && it.labels.length > 0) {
						it.labels.sort((a, b) => {
							return a.libelle.localeCompare(b.libelle);
						});
					}
				});

				this.demande.listCategories = this.demande.listCategories.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}

			if (this.demande.listCustomsFields && this.demande.listCustomsFields.length > 0) {
				let fields: Array<IField> = this.demande.customFields
					? JSON.parse(this.demande.customFields)
					: new Array<IField>();

				this.demande.listCustomsFields.forEach((field) => {
					let fd = fields.find((it) => it.label == field.label);
					field.value = fd && fd.value ? fd.value : field.value;
				});

				this.deletedCustomFields = new Array<IField>();

				fields.forEach((field) => {
					// adding deleted fields
					let fd = this.demande.listCustomsFields.find((fld) => fld.label == field.label);
					if (!fd) this.deletedCustomFields.push(field);
				});
			}

			// SVP ne pas supprimer cette ligne, elle fait en sorte que les custom fields soient enregistrés dans les units
			this.listCustomsFields = Statique.cloneListObject(
				this.demande.listCustomsFields,
				CustomField
			);
		});
	}

	getListCostCenterFiltered(res) {
		let listCostCenterFiltered: Array<EbCostCenter> = new Array<EbCostCenter>();
		if (res && res.listCostCenter) {
			if (this.demande.ebDemandeNum != null) {
				listCostCenterFiltered = res.listCostCenter;
			} else {
				res.listCostCenter.forEach((item) => {
					if (!item.deleted) listCostCenterFiltered.push(item);
				});
			}
		}
		return listCostCenterFiltered;
	}

	setListTransporteurPreference(cb: Function) {
		cb(this.listTransporteurPreference);
	}

	setTransportTypeFromExpedition(tt: string) {
		this.typeTransport = tt;
	}
	showAddressPopupChangehandler(showAddressPopup) {
		if (showAddressPopup) this.showOverview = false;
		else this.showOverview = true;
	}

	enableDraftMode() {
		this.draftMode = true;
	}

	// Step Management for reading
	/**
	 * Initiate all static steps informations
	 */
	private initStepInformations() {
		this.stepsOrdered = [
			QrCreationStep.EXPEDITION,
			QrCreationStep.REFERENCE,
			QrCreationStep.UNITS,
			QrCreationStep.DOCUMENTS,
			QrCreationStep.PRICING,
			QrCreationStep.INFOS_ABOUT_TRANSPORT,
		];
		if (this.demande.ebQrGroupe != null) {
			this.stepsOrdered.unshift(QrCreationStep.CONSOLIDATION);
		}
	}

	/**
	 * Check if step should be displayed
	 * @param stepCode Code of Step > QrCreationStep.*
	 */
	isStepVisible(stepCode: string): boolean {
		switch (stepCode) {
			case QrCreationStep.CONSOLIDATION:
				return true;
			case QrCreationStep.EXPEDITION:
				return true;
			case QrCreationStep.REFERENCE:
				return true;
			case QrCreationStep.UNITS:
				return true;
			case QrCreationStep.DOCUMENTS:
				if (this.isCreation) {
					return this.acls[this.ACL.Rule.Demande_Step_Documents_Enable];
				} else {
					return false; // Documents already displayed on right side
				}
			case QrCreationStep.PRICING:
				return true;
			case QrCreationStep.INFOS_ABOUT_TRANSPORT:
				if (this.isCreation) {
					return this.isTF && this.optionInfoAboutTransport;
				} else {
					return this.ebModuleNum === Modules.TRANSPORT_MANAGEMENT;
				}
		}
	}

	/**
	 * Get all steps code visible ordered by code
	 */
	getVisibleStepsCodeOrdered(): Array<string> {
		let result = new Array<string>();

		this.stepsOrdered.forEach((step) => {
			if (this.isStepVisible(step)) result.push(step);
		});

		return result;
	}

	/**
	 * Check if a step is already entered in the workflow
	 * @param checkedStep The step to check
	 * @param includeCurrent Mark the selected step as entered in the workflow
	 */
	isStepEnteredWorkflow(checkedStep: string, includeCurrent: boolean = true): boolean {
		let steps = this.getVisibleStepsCodeOrdered();
		if (checkedStep === this.selectedStep) return includeCurrent;

		let selectedStepAlreadyPassed = false;
		let result: boolean = undefined;

		steps.forEach((step) => {
			if (step === this.selectedStep) {
				selectedStepAlreadyPassed = true;
			}

			if (checkedStep === step && result == undefined) {
				result = !selectedStepAlreadyPassed;
			}
		});

		return result != undefined ? result : false;
	}

	/**
	 * Get the previous step code
	 * Return null if the currently selected is the first
	 */
	getPrevStep(): string {
		let steps = this.getVisibleStepsCodeOrdered();
		let selectedStepIndex = steps.indexOf(this.selectedStep);

		let result: string;
		if (selectedStepIndex === 0) {
			result = null;
		} else {
			result = steps[selectedStepIndex - 1];
		}
		return result;
	}

	/**
	 * Get the next step code
	 * Return null if the currently selected is the last
	 */
	getNextStep(): string {
		let steps = this.getVisibleStepsCodeOrdered();
		let selectedStepIndex = steps.indexOf(this.selectedStep);

		let result: string;
		if (selectedStepIndex === steps.length - 1) {
			result = null;
		} else {
			result = steps[selectedStepIndex + 1];
		}
		return result;
	}

	/**
	 * Check if a previous step is available
	 */
	get hasPrevStep(): boolean {
		return this.getPrevStep() != null;
	}

	/**
	 * Check if a next step is available
	 */
	get hasNextStep(): boolean {
		return this.getNextStep() != null;
	}
	// Step Management for reading

	onCheckDemandeInitial() {
		this.stepUnit.refreshListUnits();
		this.stepPricing.loadListViewOnly();
		this.pricingService.CalculTotalPrice(this.demande, this.isForGrouping, this.listExchangeRate);
	}

	getListExchangeRate(listExchangeRate: Array<EbCompagnieCurrency>) {
		this.listExchangeRate = listExchangeRate;
	}

	reApplyRulesWithExportControlEnabled() {
		let d = this.prepareEbDemandeForSave();
		d = Statique.removeBlankValues(d);

		this.controlRuleService.applyRulesWithExportControlEnabled(d).subscribe((res) => {
			let initExportControlStatus = res as boolean;
			if (initExportControlStatus) {
				if (
					this.demande &&
					this.demande.listMarchandises &&
					this.demande.listMarchandises.length > 0
				) {
					this.demande.listMarchandises.forEach((unit) => {
						unit.exportControlStatut = ExportControlStatut.ACE;
					});
					this.demande.exportControlStatut = ExportControlStatut.ACE;
				}
			}
		});
	}
	get isPendingDraft(): boolean {
		return this.demande.xEcNature == NatureDemandeTransport.NORMAL && this.isPendingStatus;
	}
	async loadListTransporteurAutocomplete() {
		let criteria = new SearchCriteria();
		criteria.ebUserNum = this.userConnected.ebUserNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		criteria.includeGlobalValues = true;
		criteria.roleBroker = false;
		criteria.roleTransporteur = true;
		criteria.userService = ServiceType.TRANSPORTEUR;
		criteria.roleControlTower = true;
		criteria.forChargeur = true;
		criteria.contact = true;

		this.userService.getUserContacts(criteria).subscribe((result) => {
			if(this.demande.exEbDemandeTransporteurs != null && this.demande.exEbDemandeTransporteurs.length > 0){
				const toBeRemoved = this.demande.exEbDemandeTransporteurs.map(
					(it) => it.xTransporteur.ebUserNum
				);
			}
			this.listTransporteurTDC = result.records;
		});
	}
}
