import { Statique } from "@app/utils/statique";
import { GlobalService } from "@app/services/global.service";
import { Component, ViewChild, Input, Output, EventEmitter } from "@angular/core";
import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { PricingCreationFormStepBaseComponent } from "../core/wf-step-base.component";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import { GoodsInformationComponent } from "../../../goods-information/goods-information.component";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbChat } from "@app/classes/chat";
import { EcCountry } from "@app/classes/country";
import { ComptaMatiereService } from "@app/services/compta-matiere.service";
import { EbCptmRegimeSelectionDTO } from "@app/classes/cptm/EbCptmRegimeSelectionDTO";
import { IField } from "@app/classes/customField";
import { EbTypeGoods } from "@app/classes/EbTypeGoods";
import { TypeGoodsService } from "@app/services/type-goods.service";

@Component({
	selector: "app-step-unit",
	templateUrl: "./step-unit.component.html",
	styleUrls: ["./step-unit.component.scss"],
})
export class StepUnitComponent extends PricingCreationFormStepBaseComponent {
	cols: Array<GenericTableInfos.Col>;

	@ViewChild("goodsInformation", { static: false })
	goodsInformation: GoodsInformationComponent;

	typeUnitOptions: any[] = [];
	dgrOptions: any[] = [];
	classOptions: any[] = [];
	stackableOptions: any[] = [];

	counter: number = 1;
	RenderModeTable = GenericTableInfos.Col.RenderModeTable;
	@Input()
	listModeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();
	@Input()
	marchandiseTypeUnit: Array<any>;

	@Input()
	listCustomsFields: Array<IField> = new Array<IField>();

	demandes: any[] = [{ label: "test" }];

	// edit management
	@Input()
	readOnly: boolean = false;
	@Input()
	isPending: boolean = false;

	marchandiseDangerousGood: Map<number, string>;
	marchandiseClassGood: any;

	@Input()
	listExportControl: Array<any>;
	@Input()
	listCountries: Array<EcCountry>;

	listTypeGoods: Array<EbTypeGoods> = [];

	@Output()
	updateChatEmitter? = new EventEmitter<EbChat>();

	listRegimesDouaniers: Array<EbCptmRegimeSelectionDTO> = [];

	@Input()
	requiredReadOnly: boolean = false;

	constructor(
		protected globalService: GlobalService,
		protected authenticationService: AuthenticationService,
		protected comptaMatiereService: ComptaMatiereService,
		protected typeGoodsService: TypeGoodsService,
		protected router: Router
	) {
		super(authenticationService, router);
	}

	ngOnInit() {
		super.ngOnInit();
		this.fillListMarchandiseDangerousGoods();
		this.initData();
	}

	get isComponentReady() {
		return (
			this.listModeTransport &&
			this.listModeTransport.length &&
			this.marchandiseTypeUnit &&
			this.listTypeGoods &&
			this.listRegimesDouaniers &&
				this.marchandiseClassGood
		);
	}

	protected checkValidity(): boolean {
		let isValid =
			this.demande && this.demande.listMarchandises && this.demande.listMarchandises.length > 0;
		isValid &&
			this.demande.listMarchandises.forEach((march) => {
				if (march.numberOfUnits == null || march.numberOfUnits < 1) {
					isValid = false;
				}
			});
		return isValid;
	}

	protected stepHasOptions(): boolean {
		return false;
	}

	refreshListUnits() {
		this.goodsInformation.pageDatas = this.demande.listMarchandises;
	}

	async fillListMarchandiseDangerousGoods() {
		this.marchandiseDangerousGood = await SingletonStatique.getListMarchandiseDangerousGoods();
		this.marchandiseClassGood = await SingletonStatique.getListMarchandiseClassGoods();
	}

	initData() {
		// List Type Goods
		this.typeGoodsService.listTypeGoods(this.demande.user.ebCompagnie.ebCompagnieNum).subscribe((res) => {
			this.listTypeGoods = res;
		});
		// List Regimes Douaniers
		this.comptaMatiereService.listRegimesAndProcedures().subscribe((res) => {
			this.listRegimesDouaniers = res;
		});
	}
}
