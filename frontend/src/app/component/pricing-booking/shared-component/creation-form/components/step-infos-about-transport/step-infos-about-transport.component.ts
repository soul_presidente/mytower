import {
	EventEmitter,
	Component,
	Input,
	OnChanges,
	OnInit,
	SimpleChanges,
	Output,
	ViewChild,
} from "@angular/core";
import { PricingCreationFormStepBaseComponent } from "../core/wf-step-base.component";
import { EbUser } from "@app/classes/user";
import { Router } from "@angular/router";
import { AuthenticationService } from "@app/services/authentication.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { PricingService } from "@app/services/pricing.service";
import { TranslateService } from "@ngx-translate/core";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbChat } from "@app/classes/chat";
import { TransportInformationComponent } from "@app/component/pricing-booking/transport-management/transport-information/transport-information.component";

@Component({
	selector: "app-step-infos-about-transport",
	templateUrl: "./step-infos-about-transport.component.html",
	styleUrls: ["./step-infos-about-transport.component.scss"],
})
export class StepInfosAboutTransportComponent extends PricingCreationFormStepBaseComponent
	implements OnInit, OnChanges {
	@Input()
	userConnected: EbUser;

	@Output()
	updateChatEmitter? = new EventEmitter<EbChat>();

	@Input()
	hasContributionAccess: boolean = false;

	@ViewChild("transportInfo", { static: true })
	transportInfo: TransportInformationComponent;

	isStepForm = false;

	// edit management
	@Input()
	readOnly: boolean = false;
	@Input()
	requiredReadOnly: boolean = false;

	constructor(
		protected translate: TranslateService,
		protected pricingService: PricingService,
		protected transportationPlanService: TransportationPlanService,
		protected etablissementService: EtablissementService,
		protected authenticationService: AuthenticationService,
		protected router: Router
	) {
		super(authenticationService, router);
	}

	ngOnInit() {
		if (document.location.href.indexOf("details") >= 0) {
			this.isStepForm = true;
		}
	}
	ngOnChanges(changes: SimpleChanges) {
		// if (this.readOnly && changes["demande"] && changes["demande"].currentValue != null) {
		//   this.loadListViewOnly();
		// }
		// if (!this.readOnly && changes["selected"] && changes["selected"].currentValue) {
		//   this.loadListsEdition();
		// }
	}

	protected checkValidity(): boolean {
		return true;
	}

	protected stepHasOptions(): boolean {
		return false;
	}
	showDocumentsPanel(event) {
		console.log(event);
	}
}
