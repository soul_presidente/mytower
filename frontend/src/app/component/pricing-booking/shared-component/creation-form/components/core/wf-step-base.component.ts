import { EventEmitter, Input, OnInit, Output } from "@angular/core";
import { EbDemande } from "@app/classes/demande";
import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";
import { CreationFormStepBaseComponent } from "@app/shared/creation-form-step-base/creation-form-step-base.component";

export abstract class PricingCreationFormStepBaseComponent extends CreationFormStepBaseComponent
	implements OnInit {
	@Input()
	demande: EbDemande;
	@Output()
	demandeChange = new EventEmitter<EbDemande>();

	@Input()
	selected: boolean = true;

	// TODO : Determine if read only feature will be maintained or not here
	@Input()
	readOnly: boolean = false;
	@Input()
	requiredReadOnly: boolean = false;

	optionsDisplayed: boolean = false;

	@Input()
	draftMode: boolean = false;

	module: number;

	constructor(protected authenticationService: AuthenticationService, protected router: Router) {
		super(authenticationService, router);
	}

	ngOnInit() {
		super.ngOnInit();
	}
}
