import { Component, OnInit, OnChanges, SimpleChanges, EventEmitter, Output, Input } from "@angular/core";
import { Router } from "@angular/router";
import { AuthenticationService } from "@app/services/authentication.service";
import { PricingCreationFormStepBaseComponent } from "../core/wf-step-base.component";

@Component({
	selector: "app-step-documents",
	templateUrl: "./step-documents.component.html",
	styleUrls: ["./step-documents.component.scss"],
})
export class StepDocumentsComponent extends PricingCreationFormStepBaseComponent
	implements OnInit, OnChanges {
	@Output()
	newEbDemandeNumRetrieved = new EventEmitter<number>();

	@Input()
	readOnly: boolean = false;

	constructor(protected authenticationService: AuthenticationService, protected router: Router) {
		super(authenticationService, router);
	}

	ngOnInit() {
		super.ngOnInit();
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes["selected"] && changes["selected"].currentValue) {
			// TODO reload doc when entering
		}
	}

	protected checkValidity(): boolean {
		return true;
	}
	protected stepHasOptions(): boolean {
		return false;
	}

	onNewEbDemandeNumRetrieved(newEbDemandeNum: number) {
		this.newEbDemandeNumRetrieved.next(newEbDemandeNum);
	}
}
