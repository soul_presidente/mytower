import {
	ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild,
	AfterViewInit,
} from "@angular/core";
import { EbParty } from "@app/classes/party";
import { EcCountry } from "@app/classes/country";
import { PricingService } from "@app/services/pricing.service";
import { StatiqueService } from "@app/services/statique.service";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { EbUser } from "@app/classes/user";
import { EbEtablissement } from "@app/classes/etablissement";
import { EtablissementService } from "@app/services/etablissement.service";
import { CompleterData, CompleterItem, CompleterService } from "ng2-completer";
import {
	AdresseEligibility,
	ModeTransport,
	YesOrNo,
	DemandeStatus,
	Modules,
} from "@app/utils/enumeration";
import { EbCompagnie } from "@app/classes/compagnie";
import { Statique } from "@app/utils/statique";
import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";
import { PricingCreationFormStepBaseComponent } from "../core/wf-step-base.component";
import { EbTypeTransport } from "@app/classes/EbTypeTransport";
import { SelectItem, MessageService, LazyLoadEvent } from "primeng/api";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import SingletonStatique from "@app/utils/SingletonStatique";
import { debounceTime, distinctUntilChanged, switchMap } from "rxjs/operators";
import { UserService } from "@app/services/user.service";
import { EbEntrepotDTO } from "@app/classes/dock/EbEntrepotDTO";
import { AdresseForShippingComponent } from "../../../adresse-for-shipping/adresse-for-shipping.component";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbAdresse } from "@app/classes/adresse";
import { TranslateService } from "@ngx-translate/core";
import { EbTypeFlux } from "@app/classes/EbTypeFlux";
import { EbIncoterm } from "@app/classes/EbIncoterm";
import { GlobalService } from "@app/services/global.service";

@Component({
	selector: "app-step-expedition",
	templateUrl: "./step-expedition.component.html",
	styleUrls: ["./step-expedition.component.scss"],
})
export class StepExpeditionComponent extends PricingCreationFormStepBaseComponent
	implements OnInit, OnChanges {
	DemandeStatus = DemandeStatus;

	searchCriteria: SearchCriteria = new SearchCriteria();
	searchOrigin: string;
	searchDest: string;
	searchNotify: string;

	optionNotifyParty = false;
	disableOptionNotifyParty = false;
	optionPointIntermediate = false;
	disableOptionPointIntermediate = false;
	optionTrackingPoint = false;
	optionUnloadingWarehouse = false;
	optionEligibleForConsolidation = false;
	disableOptionEligibleForConsolidation = false;

	showAddressFields: boolean;
	DestFields: boolean = false;

	showDestInfoWrapper: boolean = false;
	showNotifyFields: boolean = true;
	showCategFields: boolean = false;
	showParamsFields: boolean = false;
	libelleOriginCountry: string;

	showAddAddressBtn: boolean = false;

	ebPartyOrigin: EbParty = new EbParty();
	ebPartyDest: EbParty = new EbParty();
	ebPartyNotif: EbParty = new EbParty();
	pointIntermediate: EbParty = new EbParty();
	unloadingWarehouse: EbEntrepotDTO = new EbEntrepotDTO();

	@Input()
	listCountry: Array<EcCountry> = new Array<EcCountry>();
	@Input()
	listZone: Array<EbPlZoneDTO> = new Array<EbPlZoneDTO>();
	@Input()
	listModeTransport: Array<EbModeTransport>;
	@Input()
	listTypeTransport: Map<number, Array<EbTypeTransport>>;

	@Input()
	nbSavedFormLoaded: number;

	listEtablissementPlateform: Array<EbEtablissement> = new Array<EbEtablissement>();
	listEtablissementPlateformPI: Array<EbEtablissement> = new Array<EbEtablissement>();

	from: string = "From";
	to: string = "To";
	notifyParty: string = "Notify Party";
	intermediatePoint: string = "Intermediate point";
	UnloadingWarehouse: string = "Unloading warehouse";

	@Input()
	userConnected: EbUser;
	typeaheadEtabEvent: EventEmitter<string> = new EventEmitter<string>();
	selectedCompagnie: EbCompagnie;
	selectedEtabNum: number;
	selectedUserNum: number;
	selectedUser: EbUser;
	Statique = Statique;

	suggestionsEtablissement: CompleterData;

	typeTransportAir: Array<EbTypeTransport> = new Array<EbTypeTransport>();
	typeTransportSea: Array<EbTypeTransport> = new Array<EbTypeTransport>();
	typeTransportRoad: Array<EbTypeTransport> = new Array<EbTypeTransport>();

	listModeTransportItem: SelectItem[] = [];
	ModeTransportEnum = ModeTransport;

	@Input()
	listTypeFlux: Array<EbTypeFlux> = new Array<EbTypeFlux>();
	listTypeFluxFiltered: Array<EbTypeFlux> = new Array<EbTypeFlux>();
	@Input()
	listSchemaPsl: Array<EbTtSchemaPsl> = new Array<EbTtSchemaPsl>();
	@Input()
	listIncoterms: Array<EbIncoterm> = new Array<EbIncoterm>();
	listIncotermsFiltered: Array<EbIncoterm> = new Array<EbIncoterm>();

	listSchemaPslFiltered: Array<EbTtSchemaPsl> = new Array<EbTtSchemaPsl>();
	listPsl: Array<any> = new Array<any>();
	selectedPsl: Array<any> = new Array<any>();

	selectedSchema: EbTtSchemaPsl = new EbTtSchemaPsl();
	selectedTypeFlux: EbTypeFlux = new EbTypeFlux();
	selectedIncoterm: EbIncoterm = new EbIncoterm();

	@Output()
	partyToParent = new EventEmitter<any>();
	@Output()
	reloadDataPreference = new EventEmitter<void>();

	typeaheadUserEvent: EventEmitter<string> = new EventEmitter<string>();
	listUserEtab: any = new Array<EbUser>();
	listEtablissement: any = new Array<EbEtablissement>();

	@Input()
	showAddressPopup: boolean = false;
	@Output()
	showAddressPopupChange = new EventEmitter<boolean>();

	isAddress: boolean = true;
	suggestionsEtablissementList: Array<EbParty> = null;
	suggestionsEtablissementListCount: number = 0;
	suggestionsWarehouseList: Array<EbEntrepotDTO> = null;

	fromOrToAddressBloc: string;

	showAddressWrapperFrom: boolean;
	showAddressWrapperTo: boolean;
	showAddAddressBtnFrom: boolean = false;
	showAddAddressBtnTo: boolean;
	showAddressWrapperPI: boolean;
	showAddAddressBtnPI: boolean;
	showAddAddressBtnNotif: boolean;
	showAddressWrapperNotif: boolean;

	showWarehouseWrapper: boolean = false;
	selectedIndexFrom: number = null;
	selectedIndexTo: number = null;
	listAddress: Array<EbAdresse> = new Array<EbAdresse>();

	fromOrToFocus: string;
	@Output()
	typeTransportEmitter = new EventEmitter<string>();

	ebPartyAddAddress: EbParty = new EbParty();
	fromOrToAddAddress: any;
	adresseToSave: EbAdresse;

	@ViewChild(AdresseForShippingComponent, { static: false })
	adresseForShipping: AdresseForShippingComponent;

	@ViewChild("incoTermRef", { static: true })
	incoTermRef: any;

	eligibleForConsolidation: any;
	unitInfos: any;

	// edit management
	@Input()
	isPendingStatus: boolean = false;
	@Input()
	readOnly: boolean = false;
	@Input()
	requiredReadOnly: boolean = false;

	isEditMode: boolean = false;

	searchInputAddress: string;
	listAdressesVirtualScroller: any;
	sizeVirtualScroller: number = 10;
	pageNumber: number = 0;

	constructor(
		protected pricingService: PricingService,
		protected statiqueService: StatiqueService,
		protected transportationPlanService: TransportationPlanService,
		protected etablissementService: EtablissementService,
		protected completerService: CompleterService,
		protected authenticationService: AuthenticationService,
		protected router: Router,
		private userService: UserService,
		protected cd: ChangeDetectorRef,
		private globalService: GlobalService,
		private messageService?: MessageService,
		protected translate?: TranslateService
	) {
		super(authenticationService, router);
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.initLabels();
		this.getEstablishments();
		this.getPointIntermediates();
		this.setSuggestionsEtablissement();

		if (this.demande && this.demande.ebDemandeNum && this.demande.user) {
			this.listUserEtab = [this.demande.user];
			this.selectedUserNum = this.demande.user.ebUserNum;
		}

		this.applyAddressWrappers();

		this.init();
		this.initParties();
		this.cd.detectChanges();

		if (this.demande) {
			if (this.demande.eligibleForConsolidation) {
				this.eligibleForConsolidation = YesOrNo.YES;
				this.optionEligibleForConsolidation = true;
				this.disableOptionEligibleForConsolidation = true;
			}
		}

		if (this.demande && this.demande.ebDemandeNum) {
			if (this.hasContribution(Modules.PRICING) && this.module == Modules.PRICING) {
				this.isEditMode = true;
			}
		} else {
			this.eligibleForConsolidation = YesOrNo.NO;
			if (this.demande && this.demande.ebDemandeNum) {
				this.globalService
					.runAction(Statique.controllerUnit + "/list-unit", this.demande.ebDemandeNum)
					.subscribe((res) => {
						this.unitInfos = res;
					});
			}
		}

		this.from = this.translate.instant("PRICING_BOOKING.ADDRESS_FROM");
		this.to = this.translate.instant("PRICING_BOOKING.ADDRESS_TO");
	}

	applyAddressWrappers() {
		if (
			Statique.getInnerObjectStr(this, "demande.ebPartyDest.city") ||
			Statique.getInnerObjectStr(this, "demande.ebPartyDest.xEcCountry.ecCountryNum")
		) {
			this.showAddressWrapperTo = true;
			this.showAddAddressBtnTo = false;
			this.showAddressFields = false;
			this.ebPartyDest = this.demande.ebPartyDest;
		} else if (!this.demande.ebDemandeNum) {
			this.showAddressWrapperTo = false;
			this.showAddAddressBtnTo = false;
		}

		if (
			Statique.getInnerObjectStr(this, "demande.ebPartyOrigin.city") ||
			Statique.getInnerObjectStr(this, "demande.ebPartyOrigin.xEcCountry.ecCountryNum")
		) {
			this.showAddressWrapperFrom = true;
			this.showAddAddressBtnFrom = false;
			this.showAddressFields = false;
			this.ebPartyOrigin = this.demande.ebPartyOrigin;
		} else if (!this.demande.ebDemandeNum) {
			this.showAddressWrapperFrom = false;
			this.showAddAddressBtnFrom = false;
		}

		if (
			this.demande.optionNotifyParty ||
			(Statique.getInnerObjectStr(this, "demande.ebPartyNotif.city") ||
				Statique.getInnerObjectStr(this, "demande.ebPartyNotif.xEcCountry.ecCountryNum"))
		) {
			this.optionNotifyParty = true;
			this.disableOptionNotifyParty = true;
			this.showAddressWrapperNotif = true;
			this.showAddAddressBtnNotif = false;
			this.showAddressFields = false;
			this.ebPartyNotif = this.demande.ebPartyNotif;
		} else if (!this.demande.ebDemandeNum) {
			this.showAddressWrapperNotif = false;
			this.showAddAddressBtnNotif = false;
		}

		if (
			this.demande.optionPointIntermediate ||
			(Statique.getInnerObjectStr(this, "demande.pointIntermediate.city") ||
				Statique.getInnerObjectStr(this, "demande.pointIntermediate.xEcCountry.ecCountryNum"))
		) {
			this.optionPointIntermediate = true;
			this.disableOptionPointIntermediate = true;
			this.showAddressWrapperPI = true;
			this.showAddressFields = false;
			this.showAddAddressBtn = false;
			this.pointIntermediate = this.demande.pointIntermediate;
		} else if (!this.demande.ebDemandeNum) {
			this.showAddressWrapperPI = false;
			this.showAddAddressBtnPI = false;
		}

		if (
			this.demande.optionUnloadingWarehouse ||
			(this.demande.xEbEntrepotUnloading && this.demande.xEbEntrepotUnloading.ebEntrepotNum)
		) {
			this.unloadingWarehouse = this.demande.xEbEntrepotUnloading;
			this.optionUnloadingWarehouse = true;
			if (this.unloadingWarehouse) {
				this.showWarehouseWrapper = true;
			}
		}

		if (!this.readOnly || this.isPendingStatus) this.optionsDisplayed = false;

		this.cd.detectChanges();
	}

	ngOnChanges(changes: SimpleChanges) {
		this.getStatiques();
		if (
			(!this.listTypeFluxFiltered || !this.listTypeFluxFiltered.length) &&
			this.listTypeFlux &&
			this.listTypeFlux.length
		) {
			this.listSchemaPslFiltered = this.listSchemaPsl.map((it) => it);
			if (this.demande.xEbSchemaPsl) {
				this.listSchemaPslFiltered.push(this.demande.xEbSchemaPsl);
			}
			this.selectListTypeFluxWhenModeOfTransportChange(null);
		}

		if (
			changes.nbSavedFormLoaded &&
			changes.nbSavedFormLoaded.previousValue < changes.nbSavedFormLoaded.currentValue
		) {
			this.initParties();
			this.selectListTypeFluxWhenModeOfTransportChange(null);
		}

		if (
			changes.listModeTransport &&
			(!changes.listModeTransport.previousValue ||
				!changes.listModeTransport.previousValue.length) &&
			this.listModeTransport &&
			this.listModeTransport.length
		) {
			this.listModeTransport.forEach((element) => {
				this.translate
					.get("PRICING_BOOKING." + element.libelle.toUpperCase())
					.subscribe((libelle) => {
						this.listModeTransportItem.push({
							label: libelle,
							value: element.code,
							icon: element.icon,
						});
					});
			});

			this.selectListTypeFluxWhenModeOfTransportChange(null);
		}

		this.applyAddressWrappers();

		if (changes["demande"]) {
			if (this.demande.eligibleForConsolidation) {
				this.eligibleForConsolidation = YesOrNo.YES;
			} else {
				this.eligibleForConsolidation = YesOrNo.NO;
			}
		}
	}

	initParties() {
		if (this.demande.ebDemandeNum) return;

		if (this.demande.ebPartyDest && this.demande.ebPartyDest.city != null) {
			this.getOriginParty(this.demande.ebPartyOrigin);
		}
		if (this.demande.ebPartyOrigin && this.demande.ebPartyOrigin.city != null) {
			this.getDestParty(this.demande.ebPartyDest);
		}
		if (this.demande.ebPartyNotif && this.demande.ebPartyNotif.city != null) {
			this.getNotifyParty(this.demande.ebPartyNotif);
		}
		if (this.demande.pointIntermediate && this.demande.pointIntermediate.city != null) {
			this.getPointIntermediate(this.demande.pointIntermediate);
		}
	}

	getEstablishments() {
		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.getAll = true;
		searchCriteria.size = null;
		// searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.etablissementService
			.getListEtablisement(searchCriteria)
			.subscribe((data: Array<EbEtablissement>) => {
				this.listEtablissementPlateform = data;
			});
	}

	getPointIntermediates() {
		let compagnieToFilter: number = null;
		let etablissementToFilter: number = null;
		let userNum: number = null;

		if (
			this.demande.user &&
			this.demande.user.ebCompagnie &&
			this.demande.user.ebCompagnie.ebCompagnieNum
		) {
			userNum = this.demande.user.ebUserNum;
			compagnieToFilter = this.demande.user.ebCompagnie.ebCompagnieNum;
			if (!this.demande.user.superAdmin) {
				etablissementToFilter = this.demande.user.ebEtablissement.ebEtablissementNum;
			}
		} else {
			userNum = this.userConnected.ebUserNum;
			compagnieToFilter = this.userConnected.ebCompagnie.ebCompagnieNum;
			if (!this.userConnected.superAdmin) {
				etablissementToFilter = this.userConnected.ebEtablissement.ebEtablissementNum;
			}
		}

		this.pricingService
			.searchAdresseAsParty(
				"",
				compagnieToFilter,
				etablissementToFilter,
				AdresseEligibility.INTERMEDIATE_POINT,
				userNum
			)
			.subscribe(
				function(data) {
					if (data) {
						this.listEtablissementPlateformPI = data;
					}
				}.bind(this)
			);
	}

	setSuggestionsEtablissement() {
		let autocompleteUrl = `/adresseAsParty`;

		if (this.demande.user) {
			autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
			autocompleteUrl += `ebCompagnieNum=${this.demande.user.ebCompagnie.ebCompagnieNum}`;

			if (!this.demande.user.superAdmin) {
				autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
				autocompleteUrl += `ebEtablissementNum=${this.demande.user.ebEtablissement.ebEtablissementNum}`;
			}
		}

		if (this.demande.user) {
			autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
			autocompleteUrl += `ebCompagnieNum=${this.demande.user.ebCompagnie.ebCompagnieNum}`;

			if (!this.demande.user.superAdmin) {
				autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
				autocompleteUrl += `ebEtablissementNum=${this.demande.user.ebEtablissement.ebEtablissementNum}`;
			}
		}

		autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
		autocompleteUrl += `term=`;

		this.suggestionsEtablissement = this.completerService.remote(
			Statique.controllerPricing + autocompleteUrl,
			null,
			"etablissementReference"
		);
	}

	selectedPartyNotif(selected: CompleterItem) {
		if (selected === null) {
			this.demande.ebPartyNotif.ebPartyNum = null;
		} else {
			this.demande.ebPartyNotif = selected.originalObject;
		}
	}

	public getZoneDisplayString(zone?: EbPlZoneDTO): string {
		if (zone) {
			if (zone.designation) {
				return zone.ref + " (" + zone.designation + ")";
			} else {
				return zone.ref;
			}
		} else {
			return "";
		}
	}

	public getZoneBackupDisplayString(party?: EbParty): string {
		if (party) {
			if (party.zoneDesignation) {
				return party.zoneRef + " (" + party.zoneDesignation + ")";
			} else {
				return party.zoneRef;
			}
		} else {
			return "";
		}
	}

	private getCountryDisplayString(party?: EbParty): string {
		if (party) {
			if (party.xEcCountry) {
				return party.xEcCountry.libelle;
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	compareCountry(country1: EcCountry, country2: EcCountry) {
		if (country2 !== undefined && country2 !== null) {
			return country1.ecCountryNum === country2.ecCountryNum;
		}
	}

	compareZone(zone1: EbPlZoneDTO, zone2: EbPlZoneDTO) {
		if (zone1 && zone2) {
			return zone1.ebZoneNum === zone2.ebZoneNum;
		}
	}

	toggleNotifyFields() {
		this.showNotifyFields = !this.showNotifyFields;
	}
	toggleCategFields() {
		this.showCategFields = !this.showCategFields;
	}

	getOriginParty(data: any) {
		this.ebPartyOrigin = data;
		this.demande.ebPartyOrigin = this.ebPartyOrigin;
		this.showAddressWrapperFrom = true;
		this.showAddAddressBtnFrom = false;
	}

	getPartyAdress(data: any) {
		if (this.fromOrToFocus == this.from) {
			this.ebPartyOrigin = data;
			this.demande.ebPartyOrigin = this.ebPartyOrigin;
			if (data.xEbUserVisibility && data.xEbUserVisibility.ebUserNum)
				this.demande.xEbUserOrigin = EbUser.createUser(data.xEbUserVisibility);
			this.showAddressWrapperFrom = true;
			this.showAddAddressBtnFrom = false;
		} else if (this.fromOrToFocus == this.to) {
			this.ebPartyDest = data;
			this.demande.ebPartyDest = this.ebPartyDest;
			if (data.xEbUserVisibility && data.xEbUserVisibility.ebUserNum)
				this.demande.xEbUserDest = EbUser.createUser(data.xEbUserVisibility);
			this.showAddressWrapperTo = true;
			this.showAddAddressBtnTo = false;
		} else if (this.fromOrToFocus == this.notifyParty) {
			this.getNotifyParty(data);
		} else if (this.fromOrToFocus == this.intermediatePoint) {
			this.getPointIntermediate(data);
		}
		this.showAddressPopup = false;
		this.showAddressPopupChange.emit(this.showAddressPopup);
	}

	getDestParty(data: any) {
		this.ebPartyDest = data;
		this.demande.ebPartyDest = this.ebPartyDest;
		this.showAddressWrapperTo = true;
		this.showAddAddressBtnTo = false;
	}

	getNotifyParty(event: any) {
		this.ebPartyNotif = event;
		this.demande.ebPartyNotif = this.ebPartyNotif;
		this.showAddressWrapperNotif = true;
		this.showAddAddressBtnNotif = false;
	}

	getPointIntermediate(event: any) {
		this.pointIntermediate = event;
		this.demande.pointIntermediate = this.pointIntermediate;
		this.showAddressWrapperPI = true;
		this.showAddAddressBtnPI = false;
	}

	getUnloadingWarehouse(data: any) {
		this.unloadingWarehouse = data;
		this.optionsDisplayed = false;
		this.demande.xEbEntrepotUnloading = this.unloadingWarehouse;
		this.showWarehouseWrapper = true;
		this.showAddressPopup = false;
		this.showAddressPopupChange.emit(false);
	}

	protected checkValidity(): boolean {
		return (
			Statique.getInnerObjectStr(this, "demande.ebPartyDest.city") &&
			Statique.getInnerObjectStr(this, "demande.ebPartyOrigin.city") &&
			Statique.getInnerObjectStr(this, "demande.ebPartyOrigin.xEcCountry.libelle") &&
			Statique.getInnerObjectStr(this, "demande.ebPartyDest.xEcCountry.libelle") &&
			Statique.getInnerObjectStr(this, "demande.xEcModeTransport") &&
			Statique.getInnerObjectStr(this, "demande.dateOfGoodsAvailability") &&
			Statique.getInnerObjectStr(this, "demande.xEbSchemaPsl") &&
			Statique.getInnerObjectStr(this, "demande.xEbTypeFluxNum") &&
			Statique.getInnerObjectStr(this, "demande.xEbIncotermNum") &&
			this.selectedUserNum != null
		);
	}

	init() {
		this.typeaheadUserEvent
			.pipe(
				distinctUntilChanged(),
				debounceTime(200),
				switchMap((term) => {
					let criteria = new SearchCriteria();
					criteria.searchterm = term;
					criteria.size = null;
					criteria.simpleSearch = true;
					criteria.roleChargeur = true;
					if (this.isControlTower) {
						criteria.fromCommunity = true;
					} else {
						criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
						if (!this.userConnected.superAdmin)
							criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
						if (this.selectedEtabNum) criteria.ebEtablissementNum = this.selectedEtabNum;
					}
					try {
						return this.userService.getListUser(criteria);
					} catch (e) {}
				})
			)
			.subscribe(
				(data) => {
					this.cd.markForCheck();
					this.listUserEtab = data;
				},
				(err) => {
					this.listUserEtab = [];
				}
			);

		if (this.demande && this.demande.user) {
			let user = new EbUser();
			user.constructorCopy(this.demande.user);
			this.listUserEtab = new Array<EbUser>();
			this.listUserEtab.push(user);
			this.handleSelectedUserChange(user);
		} else if (this.userConnected.chargeur) {
			let user = new EbUser();
			user.constructorCopy(this.userConnected);
			this.listUserEtab = new Array<EbUser>();
			this.listUserEtab.push(user);
			this.isChargeur = true;
			this.handleSelectedUserChange(user);
		}
	}

	getStatiques() {
		(async () => {
			this.typeTransportAir = await SingletonStatique.getListTypeTransportByModeTransport(
				ModeTransport.AIR
			);
		})();
		(async () =>
			(this.typeTransportSea = await SingletonStatique.getListTypeTransportByModeTransport(
				ModeTransport.SEA
			)))();
		(async () =>
			(this.typeTransportRoad = await SingletonStatique.getListTypeTransportByModeTransport(
				ModeTransport.ROAD
			)))();
	}

	selectListTypeFluxWhenModeOfTransportChange($event) {
		// reset type transport when changed ModeTransport
		if ($event && !this.demande.ebDemandeNum) {
			this.demande.xEbTypeTransport = null;
			this.typeTransportEmitter.emit(null);
		}

		if ($event) this.demande.xEcModeTransport = $event.value;

		if (this.listTypeFlux) {
			this.listTypeFluxFiltered = this.listTypeFlux.filter(
				(it) =>
					it &&
					(!it.listModeTransport ||
						!it.listModeTransport.length ||
						(it.listModeTransport &&
							it.listModeTransport.indexOf(":" + this.demande.xEcModeTransport + ":") >= 0))
			);
			if (this.listTypeFlux.length == 1) {
				this.selectedTypeFlux = this.listTypeFlux[0];
				this.demande.xEbTypeFluxNum = this.selectedTypeFlux.ebTypeFluxNum;
				this.demande.xEbTypeFluxCode = this.selectedTypeFlux.code;
				if (!this.demande.xEbTypeFluxDesignation)
					this.demande.xEbTypeFluxDesignation = this.selectedTypeFlux.designation;
			}
		}

		if (this.demande.xEbTypeFluxNum != null) {
			this.selectedTypeFlux = new EbTypeFlux();
			this.selectedTypeFlux.ebTypeFluxNum = this.demande.xEbTypeFluxNum;
			this.filterSchemaPslAndIncotermsViatypeOfFlux(this.selectedTypeFlux);
		} else {
			this.selectedTypeFlux = new EbTypeFlux();
		}
	}

	// Filter list schema and list incoterms when type of flux change
	filterSchemaPslAndIncotermsViatypeOfFlux(typeFlux: EbTypeFlux) {
		this.demande.xEbTypeFluxNum = typeFlux.ebTypeFluxNum ? typeFlux.ebTypeFluxNum : this.demande.xEbTypeFluxNum;
		this.demande.xEbTypeFluxCode = typeFlux.code ? typeFlux.code : this.demande.xEbTypeFluxCode;
		this.demande.xEbTypeFluxDesignation = typeFlux.designation ? typeFlux.designation : this.demande.xEbTypeFluxDesignation;

		if (this.listSchemaPsl) {
			this.listSchemaPslFiltered = this.listSchemaPsl.filter(
				(it) =>
					it &&
					(!typeFlux.listEbTtSchemaPsl ||
						!typeFlux.listEbTtSchemaPsl.length ||
						(typeFlux.listEbTtSchemaPsl &&
							typeFlux.listEbTtSchemaPsl.indexOf(":" + it.ebTtSchemaPslNum + ":") >= 0))
			);
		}

		if (this.listIncoterms) {
			this.listIncotermsFiltered = this.listIncoterms.filter(
				(it) =>
					it &&
					(!typeFlux.listEbIncoterm ||
						!typeFlux.listEbIncoterm.length ||
						(typeFlux.listEbIncoterm &&
							typeFlux.listEbIncoterm.indexOf(":" + it.ebIncotermNum + ":") >= 0))
			);
		}

		if (this.demande.xEbSchemaPsl && this.listSchemaPslFiltered.length < 1) {
			this.listSchemaPslFiltered.push(this.demande.xEbSchemaPsl);
		}

		if (this.demande.xEbSchemaPsl != null) {
			this.selectedSchema = this.demande.xEbSchemaPsl;
			this.selectConfigPsl(this.selectedSchema);
		} else {
			this.listPsl = null;
			this.selectedPsl = null;
			this.selectedSchema = new EbTtSchemaPsl();
		}

		if (this.demande.xEbIncotermNum != null) {
			this.selectedIncoterm = new EbIncoterm();
			this.selectedIncoterm.ebIncotermNum = this.demande.xEbIncotermNum;
			this.selectedIncoterm.libelle = this.demande.xEcIncotermLibelle;
		} else {
			this.selectedIncoterm = new EbIncoterm();
		}
	}

	// select Incoterm when value change
	selectIncoterm(incoterm: EbIncoterm) {
		this.demande.xEbIncotermNum = incoterm.ebIncotermNum;
		this.demande.xEcIncotermLibelle = incoterm.libelle;
		this.cd.detectChanges();
	}

	// select Config Psl when value change
	selectConfigPsl(schemaPsl: EbTtSchemaPsl) {
		this.demande.xEbSchemaPsl.xEbCompagnie = null;
		if (schemaPsl && schemaPsl.listPsl != null) {
			this.selectedSchema = schemaPsl;
			this.listPsl = [...schemaPsl.listPsl].map(element => {
				if (element.isChecked == null) {
					element.isChecked = true;
					return element;
				}
				return element;
			});

		} else {
			this.listPsl = null;
			this.selectedPsl = null;
		}

		this.cd.detectChanges();
		// Statique.differExecution(this.cd.detectChanges.bind(this));
	}

	changedModeOfTransport($event) {
		// reset type transport when changed ModeTransport
		if ($event && !this.demande.ebDemandeNum) {
			this.demande.xEbTypeTransport = null;
			this.typeTransportEmitter.emit(null);
		}
		if ($event) this.demande.xEcModeTransport = $event.value;
		if (this.listSchemaPsl) {
			this.listSchemaPslFiltered = this.listSchemaPsl.filter(
				(it) =>
					it &&
					(!it.listModeTransport ||
						!it.listModeTransport.length ||
						(it.listModeTransport &&
							it.listModeTransport.indexOf(":" + this.demande.xEcModeTransport + ":") >= 0))
			);

			if (this.listSchemaPslFiltered && this.listSchemaPslFiltered[0]) {
				this.demande.xEbSchemaPsl = Statique.cloneObject(this.listSchemaPslFiltered[0]);
				this.demande.xEcIncotermLibelle = this.demande.xEbSchemaPsl.xEcIncotermLibelle;
				this.selectConfigPsl(this.demande.xEbSchemaPsl);
			} else {
				/*
				 * MTG-1327 :ce fix au niveau overview probleme de visiblite de l'incoterme sur partie step expedition
				 */

				//this.demande.xEcIncotermLibelle = null;
				this.listPsl = null;
				this.selectedPsl = null;
			}
		}
	}

	changePslValue() {
		let configPsl: string = "";
		this.selectedPsl.forEach((e) => {
			if (configPsl == "") configPsl += e;
			else configPsl += "|" + e;
		});
		this.demande.configPsl = configPsl == "" ? null : configPsl;
	}

	protected stepHasOptions(): boolean {
		return true;
	}

	handleSelectedUserChange(user: EbUser) {
		if (!user) {
			this.demande.user = new EbUser();
			return;
		}

		this.demande.user = new EbUser();
		this.demande.user.ebUserNum = user.ebUserNum;
		this.demande.user.nom = user.nom;
		this.demande.user.prenom = user.prenom;
		this.demande.user.admin = user.admin;
		this.demande.user.superAdmin = user.superAdmin;
		this.demande.user.ebEtablissement = new EbEtablissement();
		this.demande.user.ebEtablissement.ebEtablissementNum = user.ebEtablissement.ebEtablissementNum;
		this.demande.user.ebCompagnie = new EbCompagnie();
		this.demande.user.ebCompagnie.ebCompagnieNum = user.ebCompagnie.ebCompagnieNum;
		this.demande.user.ebCompagnie.code = user.ebCompagnie.code;
		this.demande.xEbEtablissement = new EbEtablissement();
		this.demande.xEbEtablissement.ebEtablissementNum = user.ebEtablissement.ebEtablissementNum;
		this.demande.xEbEtablissement.ebCompagnie = new EbCompagnie();
		this.demande.xEbEtablissement.ebCompagnie.ebCompagnieNum = user.ebCompagnie.ebCompagnieNum;
		this.selectedUserNum = user.ebUserNum;

		this.setSuggestionsEtablissement();

		this.reloadDataPreference.emit();
	}

	suggestionsEtablissementHandler(event: any) {
		this.suggestionsEtablissementList = event.data;
		this.suggestionsEtablissementListCount = event.count;
		this.fromOrToAddressBloc = event.fromOrTo;

		this.listAdressesVirtualScroller = Array.from({ length: event.count });
		this.setlistAdresses(0, this.sizeVirtualScroller, event.data);
		this.searchInputAddress = event.searchInput;
	}
	suggestionsWarehouseHandler(event: any) {
		this.suggestionsWarehouseList = event.data;
		this.fromOrToAddressBloc = event.fromOrTo;
	}

	showAddressPopupHandle(event) {
		this.optionsDisplayed = false;
		this.showAddressPopup = true;
		this.showAddressFields = false;
		this.isAddress = true;
		this.fromOrToFocus = event.fromOrTo;
		this.showAddressPopupChange.emit(this.showAddressPopup);
	}
	showWarehousePopupHandle(event: any) {
		this.optionsDisplayed = false;
		this.showAddressPopup = true;
		this.isAddress = false;
		this.showAddressPopupChange.emit(this.showAddressPopup);
	}
	removeAddressOriginHandler(event: any) {
		this.selectedIndexFrom = null;
		this.ebPartyOrigin = new EbParty();
		this.showAddressWrapperFrom = false;
		this.showAddAddressBtnFrom = true;
	}

	removeAddressDestHandler(event: any) {
		this.selectedIndexTo = null;
		this.ebPartyDest = new EbParty();
		this.showAddressWrapperTo = false;
		this.showAddAddressBtnTo = true;
	}
	removeAddressNotifHandler(event: any) {
		this.selectedIndexTo = null;
		this.ebPartyNotif = new EbParty();
		this.showAddressWrapperNotif = false;
		this.showAddAddressBtnNotif = true;
		this.demande.ebPartyNotif = null;
	}
	removeAddressPIHandler(event: any) {
		this.selectedIndexTo = null;
		this.pointIntermediate = new EbParty();
		this.showAddressWrapperPI = false;
		this.showAddAddressBtnPI = true;
		this.demande.pointIntermediate = null;
	}
	removeWarehouseHandler(event: any) {
		this.selectedIndexTo = null;
		this.unloadingWarehouse = new EbEntrepotDTO();
		this.showWarehouseWrapper = false;
		this.demande.xEbEntrepotUnloading = null;
	}
	hideAddressPopup() {
		let showAddressPopup, optionsDisplayed;
		this.showAddressPopup = false;
		this.optionsDisplayed = false;
		showAddressPopup = this.showAddressPopup;
		optionsDisplayed = this.optionsDisplayed;
		this.showAddressPopupChange.emit(showAddressPopup);
	}

	activateClassFrom(index: number) {
		this.selectedIndexFrom = index;
	}

	activateClassTo(index: number) {
		this.selectedIndexTo = index;
	}

	setTypeTransport(libelle: string) {
		this.typeTransportEmitter.emit(libelle);
	}

	addAddress(event) {
		let showAddressPopup, optionsDisplayed;
		if (this.optionsDisplayed == true) this.optionsDisplayed = false;
		this.showAddressPopup = true;
		this.isAddress = true;
		this.showAddressFields = event.showAddressFields;
		this.ebPartyAddAddress = event.ebParty;
		this.fromOrToAddAddress = event.fromOrTo;
		showAddressPopup = this.showAddressPopup;
		optionsDisplayed = this.optionsDisplayed;
		this.showAddressPopupChange.emit(showAddressPopup);
	}

	validateAddress(ebPartyAddAddress) {
		if (ebPartyAddAddress.city == "") {
			ebPartyAddAddress.city = null;
		}
		if (this.ebPartyAddAddress.city != null && this.ebPartyAddAddress.xEcCountry != null) {
			if (this.fromOrToAddAddress == this.from) {
				this.demande.ebPartyOrigin = ebPartyAddAddress;
				this.showAddressWrapperFrom = true;
			} else if (this.fromOrToAddAddress == this.to) {
				this.demande.ebPartyDest = ebPartyAddAddress;
				this.showAddressWrapperTo = true;
			} else if (this.fromOrToAddAddress == this.intermediatePoint) {
				this.demande.pointIntermediate = ebPartyAddAddress;
				this.showAddressWrapperPI = true;
			} else if (this.fromOrToAddAddress == this.notifyParty) {
				this.demande.ebPartyNotif = ebPartyAddAddress;
				this.showAddressWrapperNotif = true;
			}
			this.showAddAddressBtn = false;
			this.hideAddressPopup();
		} else {
			this.failedSaveAdress("", ebPartyAddAddress.city, ebPartyAddAddress.xEcCountry, "validate");
		}
	}

	showNotifyAddress(event) {
		if (event) {
			this.optionsDisplayed = false;
			this.showAddressPopup = true;
			this.adresseForShipping.showAddressPopupEmit(this.showAddressPopup, this.notifyParty);
			this.adresseForShipping.setSuggestionsEtablissement(" ", this.notifyParty);
			this.demande.optionNotifyParty = this.optionNotifyParty;
		}
	}
	showPointIntermediate(event) {
		if (event) {
			this.demande.optionPointIntermediate = this.optionPointIntermediate;
		}
	}
	showEntrepotUnloading(event) {
		if (event) {
			this.demande.optionUnloadingWarehouse = false;
		}
	}
	saveAddress(event, adresse) {
		if (adresse.reference == "") {
			adresse.reference = null;
		}
		if (adresse.city == "") {
			adresse.city = null;
		}
		if (adresse.reference != null && adresse.city != null && adresse.xEcCountry != null) {
			this.adresseToSave = new EbAdresse();
			let requestProcessing = new RequestProcessing();
			requestProcessing.beforeSendRequest(event);
			if (this.fromOrToAddAddress == this.from) {
				adresse.xEbUserVisibility = this.demande.xEbUserOrigin;
			} else if (this.fromOrToAddAddress == this.to) {
				adresse.xEbUserVisibility = this.demande.xEbUserDest;
			}
			this.adresseToSave.ebPartyToEbAdress(adresse, this.userConnected);
			if (this.isEditMode) this.adresseToSave.ebAdresseNum = null;
			let $this = this;
			this.etablissementService.addAdresseEtablisement(this.adresseToSave).subscribe(
				function(data) {
					adresse.ebAdresseNum = data.ebAdresseNum;
					if ($this.fromOrToAddAddress == $this.from) {
						$this.demande.ebPartyOrigin = adresse;
					} else if ($this.fromOrToAddAddress == $this.to) {
						$this.demande.ebPartyDest = adresse;
					} else if ($this.fromOrToAddAddress == $this.intermediatePoint) {
						$this.demande.pointIntermediate = adresse;
					} else if ($this.fromOrToAddAddress == $this.notifyParty) {
						$this.demande.ebPartyNotif = adresse;
					}
					requestProcessing.afterGetResponse(event);
					this.successfulOperation();
				}.bind(this)
			);
			requestProcessing.afterGetResponse(event);
			this.hideAddressPopup();
		} else {
			this.failedSaveAdress(adresse.reference, adresse.city, adresse.xEcCountry, "save");
		}
	}

	failedSaveAdress(ref, city, country, actionOnAddress) {
		let detailMsg = "";
		let errorSummary = "";
		errorSummary = this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY");
		if (ref == null) {
			detailMsg = this.translate.instant(
				"GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SAVE_ADDRESS.REFERENCE_REQUIRED"
			);
		} else if (ref != null && city == null) {
			detailMsg = this.translate.instant(
				"GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SAVE_ADDRESS.CITY_REQUIRED"
			);
		} else if (ref != null && city != null && country == null) {
			detailMsg = this.translate.instant(
				"GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SAVE_ADDRESS.COUNTRY_REQUIRED"
			);
		}
		if (actionOnAddress == "save") {
			detailMsg = this.translate.instant(
				"GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SAVE_ADDRESS.TO_SAVE_ADDRESS"
			);
		} else {
			detailMsg = this.translate.instant(
				"GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SAVE_ADDRESS.TO_VALIDATE_ADDRESS"
			);
		}
		this.messageService.add({
			severity: "error",
			summary: errorSummary,
			detail: detailMsg,
		});
	}
	successfulOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	private initLabels() {
		this.notifyParty = this.translate.instant("PRICING_BOOKING.ADDRESS_NOTIFY_PARTY");
		this.intermediatePoint = this.translate.instant("PRICING_BOOKING.ADDRESS_IPOINT");
		this.UnloadingWarehouse = this.translate.instant("PRICING_BOOKING.ADDRESS_UWAREHOUSE");
	}
	selectEligibleForConsolidation(event) {
		if (event == YesOrNo.YES) {
			this.demande.eligibleForConsolidation = true;
		} else {
			this.demande.eligibleForConsolidation = false;
		}
	}

	loadCarsLazy(event: LazyLoadEvent) {
		setTimeout(() => {
			if (event.first && event.first >= 0 && event.first < this.suggestionsEtablissementListCount) {
				let pageNumber = event.first / this.sizeVirtualScroller;
				let start = event.first;
				this.searchAdresseAsParty(this.sizeVirtualScroller, pageNumber, start);
			}
		}, 1000);
	}
	searchAdresseAsParty(size, pageNumber, start) {
		let compagnieNum: number;
		let etabNum: number;
		let userNum: number;

		if (
			this.demande.user &&
			this.demande.user.ebCompagnie &&
			this.demande.user.ebCompagnie.ebCompagnieNum
		) {
			compagnieNum = this.demande.user.ebCompagnie.ebCompagnieNum;
			userNum = this.demande.user.ebUserNum;
			if (!this.demande.user.superAdmin) {
				etabNum = this.demande.user.ebEtablissement.ebEtablissementNum;
			}
		} else {
			userNum = this.userConnected.ebUserNum;
			compagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			if (!this.userConnected.superAdmin) {
				etabNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			}
		}

		this.pricingService
			.searchAdresseAsParty(
				this.searchInputAddress,
				compagnieNum,
				etabNum,
				null,
				userNum,
				null,
				null,
				null,
				null,
				size,
				pageNumber
			)
			.subscribe((res) => {
				if (res) {
					this.suggestionsEtablissementList = res.data;
					this.setlistAdresses(start, size, res.data);
				}
			});
	}
	setlistAdresses(start, size, data) {
		Array.prototype.splice.apply(this.listAdressesVirtualScroller, [...[start, size], ...data]);
		this.listAdressesVirtualScroller = [...this.listAdressesVirtualScroller];
	}
}
