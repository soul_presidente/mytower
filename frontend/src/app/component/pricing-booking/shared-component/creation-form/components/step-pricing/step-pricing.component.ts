import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
	ChangeDetectorRef,
	ViewChild,
} from "@angular/core";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbUser } from "@app/classes/user";
import {
	CarrierStatus,
	DemandeStatus,
	UserRole,
	StatusDemande,
	ServiceType,
	NatureDemandeTransport,
	StatutGroupage,
	Modules,
	EtatUser,
} from "@app/utils/enumeration";
import { EtablissementService } from "@app/services/etablissement.service";
import { PricingService } from "@app/services/pricing.service";
import { SearchCriteriaPlanTransport } from "@app/utils/searchCriteriaPlanTransport";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { EbEtablissement } from "@app/classes/etablissement";
import { EbCompagnie } from "@app/classes/compagnie";
import { TranslateService } from "@ngx-translate/core";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";
import { PricingCreationFormStepBaseComponent } from "../core/wf-step-base.component";
import { Statique } from "@app/utils/statique";
import { ModalService } from "@app/shared/modal/modal.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbDemande } from "@app/classes/demande";
import { UserService } from "@app/services/user.service";
import { EcCountry } from "@app/classes/country";
import { EbChat } from "@app/classes/chat";
import { EbQrGroupeProposition } from "@app/classes/qrGroupeProposition";
import { PricingGroupingPropositionComponent } from "../../../pricing-grouping-proposition/pricing-grouping-proposition.component";
import { EbQrGroupe } from "@app/classes/qrGroupe";
import { DetailUnitDTO } from "@app/classes/trpl/DetailUnitDTO";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { CostCategorie } from "@app/classes/costCategorie";
import { AdditionalCostComponent } from "./additional-cost/additional-cost.component";
import { QrConsolidationService } from "@app/services/qrConsolidation.service";
import { GroupeVentilation, GroupePropositionStatut } from "@app/utils/enumeration";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { ACL } from "@app/classes/ACL";

@Component({
	selector: "app-step-pricing",
	templateUrl: "./step-pricing.component.html",
	styleUrls: ["./step-pricing.component.scss"],
})
export class StepPricingComponent extends PricingCreationFormStepBaseComponent
	implements OnInit, OnChanges {
	@Input()
	userConnected: EbUser;
	@Input()
	isTF: boolean;
	listExchangeRate: Array<EbCompagnieCurrency> = [];
	@Input()
	hasContributionAccess: boolean;
	@Input()
	optionInfoAboutTransport: boolean;
	@Output()
	isTFChange: EventEmitter<boolean> = new EventEmitter<boolean>();
	@Output()
	optionInfoAboutTransportChange: EventEmitter<boolean> = new EventEmitter<boolean>();
	@Output()
	updateChatEmitter: EventEmitter<EbChat> = new EventEmitter<EbChat>();
	@Output()
	onEditDemandeQuoteEmitter: EventEmitter<ExEbDemandeTransporteur> = new EventEmitter<
		ExEbDemandeTransporteur
	>();

	@Output()
	listExchangeRateEvent: EventEmitter<Array<EbCompagnieCurrency>> = new EventEmitter<
		Array<EbCompagnieCurrency>
	>();

	listTransporteur: Array<EbUser> = [];
	listDemandeQuote: Array<ExEbDemandeTransporteur> = [];
	listTransporteurViewOnly = new Array<ExEbDemandeTransporteur>();
	viewOnlyCarrierVisible: boolean = false;
	listQuote: Array<ExEbDemandeTransporteur> = [];
	loading = true;
	displayMessage: string = null;

	showDetailsPriceModal = false;
	showDetailsPriceItem: ExEbDemandeTransporteur;

	CarrierStatus = CarrierStatus;
	Statique = Statique;
	NatureDemandeTransport = NatureDemandeTransport;
	EtatUser = EtatUser;
	StatutGroupage = StatutGroupage;

	nbrRequestSent: number = 0;
	nbrReceivedQt: number = 0;
	GroupeVentilation = GroupeVentilation;

	listTransporteurAutocomplete: Array<EbUser> = new Array<EbUser>();
	selectedUserAutocomplete: EbUser;
	isListTransporteurAutocomplete: boolean;

	newDemandeQuote: ExEbDemandeTransporteur;
	isDuplicated: boolean = false;

	// edit management
	@Input()
	readOnly: boolean = false;
	@Input()
	requiredReadOnly: boolean = false;
	@Input()
	isPendingStatus: boolean = false;

	@Input()
	isForGrouping: boolean;

	@Input()
	groupProposition: EbQrGroupeProposition;
	@Input()
	qrGroup: EbQrGroupe;

	canConfirmQuote: boolean = true;

	@Output() setListTransporteurPreference: EventEmitter<
		(param1: Array<EbUser>) => void
	> = new EventEmitter();
	@Output()
	refreshCostEvent: EventEmitter<EbDemande> = new EventEmitter<EbDemande>();
	@Input()
	refreshDisabled: boolean;

	DemandeStatus = DemandeStatus;

	@ViewChild(AdditionalCostComponent, { static: false })
	additionalCostComponent: AdditionalCostComponent;

	@Input() listCurrencyDemande: Array<EbCompagnieCurrency>;
	isCommunityQoutesAddes: boolean = false;

	listEbAdditionalCosts = [];
	listTransporteurTDC: EbUser[] = null;

	constructor(
		protected translate: TranslateService,
		protected pricingService: PricingService,
		protected transportationPlanService: TransportationPlanService,
		protected etablissementService: EtablissementService,
		protected authenticationService: AuthenticationService,
		protected router: Router,
		private modalService: ModalService,
		private userService: UserService,
		private cd: ChangeDetectorRef
	) {
		super(authenticationService, router);
	}

	ngOnInit() {
		super.ngOnInit();
		this.loadListTransporteurAutocomplete();
		if (this.demande.isTF && !this.readOnly) {
			this.optionsDisplayed = true;
			this.isTF = this.demande.isTF;
			this.isTFChange.emit(this.isTF);
			this.optionInfoAboutTransport = this.demande.optionInfoAboutTransport;
			this.optionInfoAboutTransportChange.emit(this.optionInfoAboutTransport);
		}
		this.loadDataForGrouping();
		this.initMethods();
		if (
			this.demande.xEcStatut != DemandeStatus.PND &&
			this.demande.xEcStatut < DemandeStatus.FINAL_CHOICE &&
			this.readOnly
		) {
			this.priceChangeBeforeConfirmation();
		}

		this.loadListEbAdditionalCosts();
		this.getDataPreference();
	}

	initMethods() {
		this.loadExchangeRate();
		this.loadListViewOnly();
	}

	refreshCost() {
		if (
			(this.demande.xEcNature == NatureDemandeTransport.CONSOLIDATION &&
				DemandeStatus.PND == this.demande.xEcStatut) ||
			(this.demande.xEcNature == NatureDemandeTransport.NORMAL && this.isFromTransortPlan())
		) {
			this.listExchangeRateEvent.emit(this.listExchangeRate);
			this.pricingService.CalculTotalPrice(this.demande, this.isForGrouping, this.listExchangeRate);
			this.refreshCostEvent.emit(this.demande);
		}
	}

	isFromTransortPlan(): boolean {
		for (let transporteur of this.listTransporteurViewOnly) {
			if (transporteur.status == CarrierStatus.TRANSPORT_PLAN) {
				return true;
			}
			return false;
		}
	}

	loadExchangeRate() {
		let criteria = new SearchCriteria();
		criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.etablissementService.getListExEtablissementCurrency(criteria).subscribe((data) => {
			this.listExchangeRate = data;
			this.listExchangeRateEvent.emit(this.listExchangeRate);
			if (this.demande.xEcNature == NatureDemandeTransport.CONSOLIDATION) {
				this.pricingService.CalculTotalPrice(
					this.demande,
					this.isForGrouping,
					this.listExchangeRate
				);
			}
		});
	}
	loadDataForGrouping() {
		if (this.isForGrouping) {
			this.listDemandeQuote = Statique.cloneObject(
				this.demande.exEbDemandeTransporteurs,
				new Array<ExEbDemandeTransporteur>()
			);
			this.listDemandeQuote.forEach((it) => {
				it.isChecked = false;
				if (it.price) this.demande.totalPrice += it.price;
			});

			this.loadTransportPlanToTransporteur(
				function(listTransporteur) {
					let listQr = this.getListDemandeQuote(listTransporteur);
					if (listQr && listQr.length > 0) {
						this.listDemandeQuote = this.listDemandeQuote.concat(listQr);
					}
				}.bind(this)
			);

			this.demande.exEbDemandeTransporteurs = new Array<ExEbDemandeTransporteur>();
			this.pricingService.computeQuoteTotalPrice(this.isForGrouping, this.demande);
			this.loading = false;
			this.displayMessage = null;
		}
	}

	disableRefreshButton(): boolean{
		return (this.isPendingStatus && this.demande.xEcNature != NatureDemandeTransport.CONSOLIDATION) ||
					(this.demande.xEcNature == NatureDemandeTransport.NORMAL && this.isFromTransortPlan() &&
					this.demande.xEcStatut > DemandeStatus.WPU)
	}

	isCarrier(): boolean {
		return this.userConnected.role == UserRole.PRESTATAIRE;
	}

	ngOnChanges(changes: SimpleChanges) {
		if (
			(this.readOnly ||
				(this.demande.xEcNature &&
					this.demande.xEcNature == NatureDemandeTransport.CONSOLIDATION)) &&
			changes["demande"] &&
			changes["demande"].currentValue != null
		) {
			this.loadListViewOnly();
		}

		if (!this.isListTransporteurAutocomplete && this.demande && this.demande.ebDemandeNum) {
			this.loadListTransporteurAutocomplete();
		}

		if (changes["draftMode"] && changes["draftMode"].currentValue) {
			if (this.isTF) this.onChangeIsTF();

			if (this.listDemandeQuote) {
				this.listDemandeQuote.forEach((dq) => {
					dq.isChecked = false;
				});

				const filteredList = this.listDemandeQuote.filter(
					(dq) => dq.price == null || dq.price === 0
				);
				this.listDemandeQuote = filteredList;
			}
		}
		if (
			changes["refreshDisabled"] &&
			changes["refreshDisabled"].previousValue != changes["refreshDisabled"].currentValue
		) {
			this.pricingService.calculSaving(this.demande);
		}
		if (
			this.demande.xEcStatut != DemandeStatus.PND &&
			this.demande.xEcStatut < DemandeStatus.FINAL_CHOICE &&
			this.readOnly
		) {
			this.priceChangeBeforeConfirmation();
		}
	}

	async loadListTransporteurAutocomplete() {
		let criteria = new SearchCriteria();
		criteria.ebUserNum = this.userConnected.ebUserNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		criteria.includeGlobalValues = true;
		criteria.roleBroker = false;
		criteria.roleTransporteur = true;
		criteria.userService = ServiceType.TRANSPORTEUR;
		criteria.roleControlTower = true;
		criteria.forChargeur = true;
		criteria.contact = true;
		criteria.withActiveUsers = true;

		this.userService.getUserContacts(criteria).subscribe((result) => {
			this.listTransporteurTDC = result.records;
		});
	}

	handleSelectedCarrier($event: any, isForSend: boolean = false) {
		if (this.selectedUserAutocomplete && this.selectedUserAutocomplete.ebUserNum) {
			let tmpArr: Array<ExEbDemandeTransporteur> = this.getListDemandeQuote([
				this.selectedUserAutocomplete,
			]);
			let quote: ExEbDemandeTransporteur = tmpArr[0];
			quote.xEbDemande = new EbDemande();
			quote.xEbDemande.ebDemandeNum = this.demande.ebDemandeNum;

			if (isForSend) {
				let requestProcessing = new RequestProcessing();
				requestProcessing.beforeSendRequest($event);

				this.pricingService.ajouterQuotation(quote, Modules.PRICING, isForSend).subscribe(
					(res) => {
						requestProcessing.afterGetResponse($event);

						quote.exEbDemandeTransporteurNum =
							res.exEbDemandeTransporteur.exEbDemandeTransporteurNum;
						let index = this.listDemandeQuote.findIndex(
							(q) => q.exEbDemandeTransporteurNum == quote.exEbDemandeTransporteurNum
						);
						if (index >= 0) {
							this.listDemandeQuote[index] = quote;
						} else {
							quote.isChecked = true;
							this.listDemandeQuote.push(quote);
							this.listQuote.push(quote);
						}
						
						if (this.getIndex(this.demande.exEbDemandeTransporteurs,quote) >= 0) {
							this.demande.exEbDemandeTransporteurs[index] = quote;
						} else {
							this.demande.exEbDemandeTransporteurs.push(quote);
							if(this.getIndex(this.listTransporteurViewOnly,quote) == -1){
								this.listTransporteurViewOnly.push(quote);
							}
						}
						this.demande.xEcStatut = res.exEbDemandeTransporteur.xEbDemande.xEcStatut;
						this.updateChatEmitter.emit(res.ebChat);
					},
					(err) => {
						requestProcessing.afterGetResponse($event);
					}
				);
			} else {
				this.onEditDemandeQuoteHandler(quote);
			}
		}
	}
	loadListEbAdditionalCosts() {
		this.pricingService
			.listAdditionalCostsByEbCompagnieNum(this.userConnected.ebCompagnie.ebCompagnieNum)
			.subscribe((res) => {
				this.listEbAdditionalCosts = res;
			});
	}

	getIndex(quotes : Array<ExEbDemandeTransporteur>, quote : ExEbDemandeTransporteur) {
		return  quotes.findIndex(
			(q) => q.exEbDemandeTransporteurNum == quote.exEbDemandeTransporteurNum
		);
	}

	onTransporteurSelected(tr: ExEbDemandeTransporteur) {
		if (!tr.isChecked) {
			tr.status = null;
		}
		let foundIndex = this.demande.exEbDemandeTransporteurs
			? this.demande.exEbDemandeTransporteurs.findIndex(
					(it) => this.areQrEquals(tr, it) && tr.price == it.price
			  )
			: -1;
		if (foundIndex >= 0 && !tr.isChecked) {
			this.demande.exEbDemandeTransporteurs.splice(foundIndex, 1);
		} else if (foundIndex < 0 && tr.isChecked) {
			if (!this.isQuotePriceDefined(tr)) tr.status = CarrierStatus.REQUEST_RECEIVED;
			else if (tr.status < CarrierStatus.QUOTE_SENT) tr.status = CarrierStatus.QUOTE_SENT;
			if (this.demande.exEbDemandeTransporteurs == null) {
				this.demande.exEbDemandeTransporteurs = Array<ExEbDemandeTransporteur>();
			}
			this.demande.exEbDemandeTransporteurs.push(tr);
		}
		this.getNombreOfRequestSentAndReceivedQt();

		this.pricingService.computeQuoteTotalPrice(this.isForGrouping, this.demande);
	}

	isQuotePriceDefined(tr: ExEbDemandeTransporteur): boolean {
		return !(
			!Statique.isDefined(tr.price) ||
			(tr.price == 0 &&
				(!Statique.isDefined(tr.listCostCategorie) || !tr.listCostCategorie.length) &&
				(!Statique.isDefined(tr.listPlCostItem) || !tr.listPlCostItem.length))
		);
	}

	carrierStatusChange(tr: ExEbDemandeTransporteur) {
		let nbrRequestSent = 0;
		let nbrReceivedQt = 0;

		if (tr.status == CarrierStatus.FINAL_CHOICE) {
			this.listDemandeQuote.forEach((it) => {
				if (!this.areQrEquals(tr, it) && it.status == CarrierStatus.FINAL_CHOICE) {
					it.status = it.price ? CarrierStatus.QUOTE_SENT : CarrierStatus.REQUEST_RECEIVED;
				}
			});
		}

		this.demande.exEbDemandeTransporteurs &&
			this.demande.exEbDemandeTransporteurs.forEach((it) => {
				let qr = this.listDemandeQuote.find((el) => this.areQrEquals(el, it));
				if (qr) it.status = qr.status;

				if (it.status == CarrierStatus.REQUEST_RECEIVED) {
					nbrRequestSent++;
				} else if (
					it.status == CarrierStatus.QUOTE_SENT ||
					it.status == CarrierStatus.FINAL_CHOICE
				) {
					nbrReceivedQt++;
				}
			});
		this.nbrRequestSent = nbrRequestSent;
		this.nbrReceivedQt = nbrReceivedQt;
	}

	addDays(date: Date, days: number): Date {
		date.setDate(date.getDate() + days);
		return date;
	}

	//#region LoadList
	public loadListViewOnly() {
		if (this.demande.xEcStatut >= DemandeStatus.WPU) {
			this.listTransporteurViewOnly = this.demande.exEbDemandeTransporteurs;
		}

		if (
			this.demande.xEcStatut <= DemandeStatus.RECOMMENDATION &&
			this.demande.xEcNature < NatureDemandeTransport.CONSOLIDATION &&
			this.demande.exEbDemandeTransporteurs != null
		) {
			this.demande.exEbDemandeTransporteurs.forEach((transporteur, index) => {
				let transporteurPrice = transporteur.price;
				//transporteur.price = 0;
				if (transporteur.isFromTransPlan == null && transporteur.listCostCategorie != null) {
					transporteur.listCostCategorie.forEach((costCategorie) => {
						costCategorie.priceEuro = 0;
						costCategorie.listEbCost.forEach((cost) => {
							if (cost.price != null && cost.xEcCurrencyNum != null) {
								if (cost.xEcCurrencyNum == this.demande.xecCurrencyInvoice.ecCurrencyNum) {
									cost.priceEuro = cost.price;
								} else {
									this.listExchangeRate &&
										this.listExchangeRate.forEach((rate) => {
											if (
												transporteur.xEbEtablissement.ebCompagnie.ebCompagnieNum ==
													rate.ebCompagnieGuest.ebCompagnieNum &&
												rate.dateFin >= new Date() &&
												this.addDays(new Date(), 1) >= rate.dateDebut
											) {
												if (
													rate.xecCurrencyCible.ecCurrencyNum ==
														this.demande.xecCurrencyInvoice.ecCurrencyNum &&
													rate.ecCurrency.ecCurrencyNum == cost.xEcCurrencyNum
												) {
													cost.priceEuro = cost.price * rate.euroExchangeRate;
													cost.euroExchangeRate = rate.euroExchangeRate;
													costCategorie.priceEuro += cost.priceEuro;
												}
											}
										});
								}
							}
						});
						if (costCategorie.priceEuro != 0) {
							transporteur.price += costCategorie.priceEuro;
						} else {
							transporteur.price = transporteurPrice;
						}
					});
				}
				this.listTransporteurViewOnly[index] = transporteur;
			});
		}
		if (this.demande.xEcNature >= 7) {
			this.listTransporteurViewOnly = this.demande.exEbDemandeTransporteurs;
		}

		this.loading = false;
	}

	public priceChangeBeforeConfirmation() {
		const criteria = new SearchCriteria();
		criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.etablissementService.getListExEtablissementCurrency(criteria).subscribe((data) => {
			this.listExchangeRate = data;
			
			if (this.demande.exEbDemandeTransporteurs != null) {
				this.demande.exEbDemandeTransporteurs.forEach((transporteur, index) => {
					let total = 0;
					if (transporteur.isFromTransPlan == null && transporteur.listCostCategorie != null) {
						transporteur.listCostCategorie.forEach((costCategorie) => {
							costCategorie.priceEuro = 0;
							costCategorie.listEbCost.forEach((cost) => {
								if (cost.price != null) {
									if (cost.xEcCurrencyNum == null)
										cost.xEcCurrencyNum = this.demande.xecCurrencyInvoice.ecCurrencyNum;
									if (cost.xEcCurrencyNum == this.demande.xecCurrencyInvoice.ecCurrencyNum) {
										cost.priceEuro = cost.price;
										costCategorie.priceEuro += cost.priceEuro;
									} else {
										this.listExchangeRate &&
											this.listExchangeRate.forEach((rate) => {
												if (
													transporteur.xEbEtablissement.ebCompagnie.ebCompagnieNum ==
														rate.ebCompagnieGuest.ebCompagnieNum &&
													rate.dateFin >= new Date() &&
													this.addDays(new Date(), 1) >= rate.dateDebut
												) {
													if (
														rate.xecCurrencyCible.ecCurrencyNum ==
															this.demande.xecCurrencyInvoice.ecCurrencyNum &&
														rate.ecCurrency.ecCurrencyNum == cost.xEcCurrencyNum
													) {
														cost.priceEuro = cost.price * rate.euroExchangeRate;
														cost.euroExchangeRate = rate.euroExchangeRate;
														costCategorie.priceEuro += cost.priceEuro;
													}
												}
											});
									}
								}
							});
							if (costCategorie.priceEuro != 0) {
								total += costCategorie.priceEuro;
								transporteur.price = total;
							}
						});
					}
					this.listTransporteurViewOnly[index] = transporteur;
				});
			}
		});

		this.loading = false;
	}

	public loadListsCarrierViewOnly(listTransporteur: Array<EbUser>, $event: any = null) {
		let requestProcessing = null;
		if ($event) {
			requestProcessing = new RequestProcessing();
			requestProcessing.beforeSendRequest($event);
		}

		this.loadListTransportPlan(listTransporteur, requestProcessing, $event, true);
	}

	public loadListsEdition(listTransporteur: Array<EbUser> = null, $event: any = null) {
		let requestProcessing = null;
		if ($event) {
			requestProcessing = new RequestProcessing();
			requestProcessing.beforeSendRequest($event);
		}
		if (!listTransporteur) {
			this.loadListTransporteur((list: Array<EbUser>) => {
				this.loadListTransportPlan(list, requestProcessing, $event);
			});
		} else {
			this.loadListTransportPlan(listTransporteur, requestProcessing, $event);
		}
	}

	removeListTransportPlan() {
		this.listDemandeQuote = this.listDemandeQuote.filter((it) => !it.isFromTransPlan);
	}

	loadListTransportPlan(
		listTransporteur: Array<EbUser>,
		requestProcessing = null,
		$event = null,
		viewOnlyMode: boolean = false
	) {
		this.listTransporteurViewOnly = [];
		let $this = this;
		this.loadTransportPlanToTransporteur((finalList) => {
			let listQrTr = [];
			if (
				(!listTransporteur || !listTransporteur.length) &&
				this.listDemandeQuote &&
				this.listDemandeQuote.length
			)
				this.removeListTransportPlan();
			else if (listTransporteur && listTransporteur.length) {
				listQrTr = this.getListDemandeQuote(listTransporteur);
				//this.listDemandeQuote = []
			}
			//Garder une seule proposition par transporteur si ses propositions ont le même prix
			let filteredQuote = this.listDemandeQuote.filter((quote) => quote.price == 0 || !quote.price);

			this.listDemandeQuote = filteredQuote.concat(this.getListDemandeQuote(finalList));
			if (listQrTr && listQrTr.length > 0) {
				// code replaced with isCommunityQoutesAddes boolean to prevent quotes duplication in step pricing
				// listQrTr = listQrTr.filter((it: ExEbDemandeTransporteur) => {
				// 	return !this.listDemandeQuote.find(
				// 		(el) => el.xTransporteur.ebUserNum == it.xTransporteur.ebUserNum
				// 	);
				// });
				// search is executed multiple times -> community quotes must be added only once (first search call)
				if (!this.isCommunityQoutesAddes) {
					this.listDemandeQuote = this.listDemandeQuote.concat(listQrTr);
					this.isCommunityQoutesAddes = true;
				}
				// Traitement spécifique viewOnly
				const isNatureNotExistOrIsNotConsolidation: boolean =
					!this.demande.xEcNature || NatureDemandeTransport.CONSOLIDATION != this.demande.xEcNature;
				const isQuoteStatusNotExistOrLessThanFinalChoice =
					!this.demande.statusQuote || this.demande.statusQuote < CarrierStatus.FINAL_CHOICE;
				if (
					isNatureNotExistOrIsNotConsolidation &&
					isQuoteStatusNotExistOrLessThanFinalChoice &&
					viewOnlyMode &&
					this.listTransporteurViewOnly != null
				) {
					for (let tr of $this.listDemandeQuote) {
						this.listTransporteurViewOnly = [];
						if (tr.price && tr.price != 0) {
							$this.listTransporteurViewOnly.push(tr);
						}
					}
				}
			}
			//tri proposition par prix
			this.listDemandeQuote.sort((a, b) => {
				if (!b.xTransporteur.calculatedPrice) return -1;
				else if (!a.xTransporteur.calculatedPrice) return 1;
				else return a.xTransporteur.calculatedPrice - b.xTransporteur.calculatedPrice;
			});

			this.bindTransportPlanSelectedFromDemande();

			// Jira CDT-109 : In mask, we should not have quote with status transport plan
			if (!this.demande.ebDemandeNum && this.isTF && this.listDemandeQuote != null) {
				this.listDemandeQuote.forEach((demandeQuote) => {
					if (demandeQuote.status == CarrierStatus.TRANSPORT_PLAN) {
						// In this case, the status is invalid for pricing vignette, we need to set the correct status for user input
						demandeQuote.status = CarrierStatus.QUOTE_SENT;
					}
				});
			}
			$event && requestProcessing.afterGetResponse($event);
		});
	}

	reloadCarriers($event: any = null, resetQr: boolean = false) {
		if (resetQr) this.demande.exEbDemandeTransporteurs = [];
		this.loadListsEdition();
	}

	reloadCarriersVisualisation($event: any = null) {
		this.loadListsCarrierViewOnly(this.listTransporteurAutocomplete, $event);
		this.viewOnlyCarrierVisible = true;
	}

	private bindTransportPlanSelectedFromDemande() {
		this.displayMessage = "";

		if (this.demande.listTransporteurs == null) this.demande.listTransporteurs = [];
		this.demande.listTransporteurs.forEach((demandeTransporteur, index) => {
			this.listTransporteur.forEach((fli) => {
				if (demandeTransporteur.ebUserNum === fli.ebUserNum) {
					fli.selectedForQuotation = true;
				}
			});
		});

		this.loading = false;
		if (!this.listTransporteur) {
			this.displayMessage = "PRICING_BOOKING.STEP_PRICING_NO_CARRIER";
		}

		this.demande.exEbDemandeTransporteurs &&
			this.demande.exEbDemandeTransporteurs.forEach((it) => {
				let indexTr = this.listDemandeQuote.findIndex((el) => this.areQrEquals(el, it));
				if (indexTr >= 0) {
					this.listDemandeQuote[indexTr] = it;
					this.listDemandeQuote[indexTr].isChecked = true;
				}
			});
	}

	getListDemandeQuote(listTr: Array<EbUser>): Array<ExEbDemandeTransporteur> {
		let listDemandeQuote = listTr
			? listTr.map((tr) => {
					let foundQuote = this.demande.exEbDemandeTransporteurs
						? this.demande.exEbDemandeTransporteurs.find(
								(it) => it.xTransporteur.ebUserNum == tr.ebUserNum
						  )
						: -1;
					if (foundQuote) {
						foundQuote = Statique.cloneObject(foundQuote, new ExEbDemandeTransporteur());
						this.setCarrierData(foundQuote, tr);
						return foundQuote;
					} else {
						let dq = new ExEbDemandeTransporteur();

						this.setCarrierData(dq, tr);

						dq.xTransporteur.calculatedPrice = tr.calculatedPrice;
						dq.price = tr.calculatedPrice;
						dq.transitTime = tr.transitTime;
						dq.comment = tr.coment;
						dq.isFromTransPlan = tr.isFromTransPlan;
						dq.exchangeRateFound = tr.exchangeRateFound;
						dq.refPlan = tr.refPlan;
						dq.refGrille = tr.refGrille;
						dq.status = CarrierStatus.REQUEST_RECEIVED;
						dq.listPlCostItem = tr.listPlCostItem;
						dq.horsGrille = tr.horsGrille;

						return Statique.cloneObject(dq, new ExEbDemandeTransporteur());
					}
			  })
			: null;
		return listDemandeQuote;
	}

	private loadTransportPlanToTransporteur(
		callback?: (finalListTransporteur?: Array<EbUser>) => void
	) {
		if (
			!this.demande.ebPartyOrigin ||
			!this.demande.ebPartyDest ||
			!this.demande.xecCurrencyInvoice ||
			!this.listTransporteur ||
			!this.demande.listMarchandises
		) {
			if (callback) callback(this.listTransporteur);
			return;
		}

		if (this.demande.exEbDemandeTransporteurs == null) {
			this.demande.exEbDemandeTransporteurs = new Array<ExEbDemandeTransporteur>();
		}

		this.demande.exEbDemandeTransporteurs = this.demande.exEbDemandeTransporteurs.filter(
			(it) => !it.isFromTransPlan
		);

		let dgMar = 0;
		let typeGoodsNum = null;
		const searchCriteriaPl = new SearchCriteriaPlanTransport();
		for (let unit of this.demande.listMarchandises) {
			if (unit.dangerousGood != null && dgMar < unit.dangerousGood) {
				dgMar = unit.dangerousGood;
			}

			unit.length = Number(Statique.formatNumberValue(unit.length));
			unit.heigth = Number(Statique.formatNumberValue(unit.heigth));
			unit.weight = Number(Statique.formatNumberValue(unit.weight));
			unit.width = Number(Statique.formatNumberValue(unit.width));

			// Type Of Goods
			if (unit.typeGoodsNum != null) {
				if (typeGoodsNum != null) {
					typeGoodsNum = -1; // Fake id to not return results of 2 type of goods by demande
				} else {
					typeGoodsNum = unit.typeGoodsNum;
				}
			}
			let detailUnit = new DetailUnitDTO();
			detailUnit.weight = unit.weight;
			detailUnit.volume = unit.volume;
			detailUnit.nombre = Number(unit.numberOfUnits);
			detailUnit.ebTypeUnitNum = unit.xEbTypeUnit;
			searchCriteriaPl.listUnit.push(detailUnit);
		}
		if (this.demande.ebPartyOrigin.zone && this.demande.ebPartyOrigin.zone.ebZoneNum)
			searchCriteriaPl.ebZoneNumOrigin = this.demande.ebPartyOrigin.zone.ebZoneNum;
		if (this.demande.ebPartyDest.zone && this.demande.ebPartyDest.zone.ebZoneNum)
			searchCriteriaPl.ebZoneNumDest = this.demande.ebPartyDest.zone.ebZoneNum;
		// searchCriteriaPl.isDangerous = false; //TODO
		if (dgMar > 1) {
			searchCriteriaPl.dangerous = true;
		} else if (dgMar == 1) {
			searchCriteriaPl.dangerous = false;
		} else {
			searchCriteriaPl.dangerous = null;
		}
		searchCriteriaPl.ebUserNum = this.demande.user.ebUserNum;
		searchCriteriaPl.modeTransport = this.demande.xEcModeTransport;
		searchCriteriaPl.typeTransport = this.demande.xEbTypeTransport;
		searchCriteriaPl.typeDemande = this.demande.xEcTypeDemande;
		searchCriteriaPl.totalWeight = Number.isNaN(this.demande.totalTaxableWeight)
			? null
			: Number(this.demande.totalTaxableWeight);
		searchCriteriaPl.totalUnit =
			(this.demande.totalNbrParcel as any) === "" ? null : this.demande.totalNbrParcel;

		searchCriteriaPl.totalVolume = Number.isNaN(this.demande.totalVolume)
			? null
			: Number(this.demande.totalVolume);

		if (this.demande.xecCurrencyInvoice) {
			searchCriteriaPl.currencyCibleNum = this.demande.xecCurrencyInvoice.ecCurrencyNum;
		}
		searchCriteriaPl.typeGoodsNum = typeGoodsNum;
		searchCriteriaPl.datePickup = this.demande.dateOfGoodsAvailability;

		searchCriteriaPl.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		//on recupére le premier type of unit saisi
		if (this.demande.listMarchandises) {
			for (let marchd of this.demande.listMarchandises) {
				if (marchd.xEbTypeUnit) {
					searchCriteriaPl.ebTypeUnitNum = marchd.xEbTypeUnit;
					searchCriteriaPl.typeGoodsNum = marchd.typeGoodsNum;
					break;
				}
			}
		}

		searchCriteriaPl.ebDemande = this.demande.clone();
		//supp de la list des categories et mettre a sa place l'element selecter
		if (searchCriteriaPl.ebDemande.listCategories) {
			searchCriteriaPl.ebDemande.listCategories.forEach((elem) => {
				elem.labels = elem.selectedLabel ? [elem.selectedLabel] : [];
			});
		}
		searchCriteriaPl.isForValorisation = true;

		this.transportationPlanService.searchPricingTransport(searchCriteriaPl).subscribe(
			(listFromSearch) => {
				let listTranspPlan: Array<EbUser> = [];
				for (let it of listFromSearch as Array<any>) {
					if (it.calculatedPrice) {
						let trUser = new EbUser();
						trUser.ebUserNum = it.carrierUserNum;
						trUser.nom = it.carrierName;
						trUser.email = it.carrierEmail;
						trUser.calculatedPrice = it.calculatedPrice;
						trUser.transitTime = it.transitTime;
						trUser.coment = it.comment;
						trUser.listPlCostItem = it.listPlCostItem;
						trUser.horsGrille = it.horsGrille;

						if (it.dangerous != null) {
							if (it.dangerous === true) {
								it.comment +=
									" (" + this.translate.instant("PRICING_BOOKING.PLAN_TRANSPORT_DANGEROUS") + ")";
							} else if (it.dangerous === false) {
								it.comment +=
									" (" +
									this.translate.instant("PRICING_BOOKING.PLAN_TRANSPORT_NOT_DANGEROUS") +
									")";
							}
						}

						trUser.refPlan = it.planRef;
						trUser.refGrille = it.refGrille;
						trUser.isFromTransPlan = true;
						trUser.exchangeRateFound = it.exchangeRateFound;

						if (it.carrierEtabNum) {
							trUser.ebEtablissement = new EbEtablissement();
							trUser.ebEtablissement.ebEtablissementNum = it.carrierEtabNum;
							trUser.ebEtablissement.nom = it.carrierEtabName;
							if (it.carrierCompanyNum) {
								trUser.ebEtablissement.ebCompagnie = new EbCompagnie();
								trUser.ebEtablissement.ebCompagnie.ebCompagnieNum = it.carrierCompanyNum;
								trUser.ebEtablissement.ebCompagnie.nom = it.carrierCompanyNum;
							} else trUser.ebEtablissement.ebCompagnie = null;
						} else trUser.ebEtablissement = null;
						listTranspPlan.push(trUser);
					}
				}

				callback(listTranspPlan);
			},
			(error) => {
				this.loading = false;
				this.displayMessage = "PRICING_BOOKING.WF_STEP_LOAD_ERROR_GLOBAL";
				console.error(error);
			}
		);
	}

	private loadListTransporteur(callback?: (listTransporteur?: Array<EbUser>) => void) {
		let criteria: SearchCriteria = new SearchCriteria();
		criteria.size = null;
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		criteria.withActiveUsers = true;

		if (this.userConnected.role == UserRole.CONTROL_TOWER) {
			if (this.demande.xEbEtablissement) {
				criteria.ebEtablissementNum = this.demande.xEbEtablissement.ebEtablissementNum;
			}
			if (this.demande.xEbEtablissement && this.demande.xEbEtablissement.ebCompagnie) {
				criteria.ebCompagnieNum = this.demande.xEbEtablissement.ebCompagnie.ebCompagnieNum;
			}
			if (this.demande.user) {
				criteria.ebUserNum = this.demande.user.ebUserNum;
			}
		} else if (this.userConnected.role == UserRole.CHARGEUR) {
			criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			criteria.ebUserNum = this.userConnected.ebUserNum;
			criteria.contact = true;
		}

		if (!criteria.ebEtablissementNum || !criteria.ebCompagnieNum) {
			this.listTransporteur = new Array<EbUser>();
			if (callback) callback(this.listTransporteur);
			this.displayMessage = "PRICING_BOOKING.STEP_PRICING_NO_CARRIER";
			this.loading = false;
			return;
		} else {
			this.setListTransporteurPreference.emit(
				function(res) {
					if (res && res.length) {
						this.listTransporteur =
							res && res.listTransporteur ? res.listTransporteur : new Array<EbUser>();
						this.listTransporteur = this.listTransporteurTDC;
						if (callback) callback(this.listTransporteur);
					} else {
						this.loading = false;
						this.displayMessage = "PRICING_BOOKING.STEP_PRICING_NO_CARRIER";
					}
				}.bind(this)
			);
		}
	}
	//#endregion

	showDetailsPrice(index: number) {
		this.showDetailsPriceItem =
			this.demande.exEbDemandeTransporteurs && this.demande.exEbDemandeTransporteurs.length > 0
				? this.demande.exEbDemandeTransporteurs[index]
				: this.listTransporteurViewOnly[index];
		this.showDetailsPriceModal = true;
	}

	readOnlyDatesDisplay(tr: ExEbDemandeTransporteur): string {
		let rStr = "";
		let startDate = null;
		let endDate = null;

		tr.listEbTtCompagniePsl &&
			tr.listEbTtCompagniePsl.forEach((it) => {
				if (it.startPsl) {
					startDate = it.expectedDate;
				}
				if (it.endPsl) {
					endDate = it.expectedDate;
				}
			});

		if (startDate) {
			rStr += this.Statique.formatDate(startDate);
		}
		if (startDate && endDate) {
			rStr += " - ";
		}
		if (endDate) {
			this.Statique.formatDate(endDate);
		}
		return rStr;
	}

	editQuotation(demandeQuote: ExEbDemandeTransporteur) {
		this.onEditDemandeQuoteHandler(demandeQuote);
	}

	quoteEdited($event) {
		this.showDetailsPriceModal = $event;
		this.showDetailsPriceItem = null;

		// TOTO: revert back status basing on price availability for every quote?
	}

	setDemandeQuote(dq: ExEbDemandeTransporteur) {
		let index = this.listDemandeQuote.findIndex(
			(it) => it.exEbDemandeTransporteurNum == dq.exEbDemandeTransporteurNum
		);
		if (index >= 0) {
			this.listDemandeQuote[index] = dq;
		}

		index = this.demande.exEbDemandeTransporteurs.findIndex(
			(it) => it.exEbDemandeTransporteurNum == dq.exEbDemandeTransporteurNum
		);
		if (index >= 0) {
			this.demande.exEbDemandeTransporteurs[index] = dq;
		}
	}

	protected checkValidity(): boolean {
		if (
			this.demande.exEbDemandeTransporteurs == null ||
			this.demande.exEbDemandeTransporteurs.length == 0
		) {
			return false;
		}
		if (this.isTF) {
			let hasOneFinalChoice = false;
			this.demande.exEbDemandeTransporteurs.forEach((dt) => {
				// we test on values of transitTime & price & deliveryTime if the final choice is selected but one of this fields not filled
				if (
					dt.status === CarrierStatus.FINAL_CHOICE &&
					!hasOneFinalChoice &&
					dt.transitTime != null &&
					dt.price != null
				) {
					hasOneFinalChoice = true;
				} else if (dt.status === CarrierStatus.FINAL_CHOICE && hasOneFinalChoice) {
					// This should not happend, but added this security check
					// Can't have 2 items to final choice status
					hasOneFinalChoice = false;
				}
			});

			return hasOneFinalChoice;
		}

		return true;
	}

	protected stepHasOptions(): boolean {
		//Les options de Transport files sont disponibles uniquement pour les users ayant une visibilité sur les prix.
		if (this.userConnected.canShowPrice) return true;
		return false;
	}

	onChangeIsTF() {
		this.isTFChange.emit(this.isTF);
		this.demande.isTF = this.isTF;
		if (!this.isTF) {
			this.listDemandeQuote.forEach((tr) => {
				tr.price = 0;
				tr.transitTime = null;
				tr.isChecked = false;
				tr.comment = null;
				tr.priceGab = 0;
				tr.exchangeRate = null;
			});
		}
	}
	onChangeInformationsAboutTransport() {
		this.optionInfoAboutTransportChange.emit(this.optionInfoAboutTransport);
		this.demande.optionInfoAboutTransport = this.optionInfoAboutTransport;
	}

	confirmChoice(exEbDemandeTransporteur: ExEbDemandeTransporteur, event) {
		if (
			this.userConnected.role == UserRole.CHARGEUR ||
			this.userConnected.role == UserRole.CONTROL_TOWER
		) {
			this.canConfirmQuote = false;

			this.modalService.confirm(
				"confirm",
				this.translate.instant("PRICING_BOOKING.CONFIRM_QUOTATION"),
				function() {
					let requestProcessing = new RequestProcessing();
					requestProcessing.beforeSendRequest(event);
					let demandeTransporteur = new ExEbDemandeTransporteur();
					// demandeTransporteur.status = CarrierStatus.FINAL_CHOICE;
					if (exEbDemandeTransporteur.isFromTransPlan) {
						demandeTransporteur.status = CarrierStatus.FIN_PLAN;
					} else {
						demandeTransporteur.status = CarrierStatus.FINAL_CHOICE;
					}
					if (this.demande.xEcNature == NatureDemandeTransport.CONSOLIDATION)
						this.demande.xEcStatutGroupage = StatutGroupage.PCO;
					demandeTransporteur.xEbDemande = new EbDemande();
					demandeTransporteur.xEbDemande.ebDemandeNum = this.demande.ebDemandeNum;
					demandeTransporteur.xTransporteur = new EbUser();
					demandeTransporteur.exEbDemandeTransporteurNum =
						exEbDemandeTransporteur.exEbDemandeTransporteurNum;
					demandeTransporteur.xTransporteur.ebUserNum =
						exEbDemandeTransporteur.xTransporteur.ebUserNum;
					demandeTransporteur.xEbUserConfirmBy = new EbUser();
					demandeTransporteur.xEbUserConfirmBy.ebUserNum = this.userConnected.ebUserNum;

					demandeTransporteur.listEbTtCompagniePsl = exEbDemandeTransporteur.listEbTtCompagniePsl;

					demandeTransporteur.price = exEbDemandeTransporteur.price;
					demandeTransporteur.transitTime = exEbDemandeTransporteur.transitTime;
					demandeTransporteur.exchangeRate = exEbDemandeTransporteur.exchangeRate;
					demandeTransporteur.infosForCustomsClearance =
						exEbDemandeTransporteur.infosForCustomsClearance;
					demandeTransporteur.comment = exEbDemandeTransporteur.comment;
					demandeTransporteur.horsGrille = exEbDemandeTransporteur.horsGrille;

					if (
						exEbDemandeTransporteur.listCostCategorie &&
						exEbDemandeTransporteur.listCostCategorie != null
					) {
						demandeTransporteur.listCostCategorie = exEbDemandeTransporteur.listCostCategorie;
					}

					if (
						exEbDemandeTransporteur.listPlCostItem &&
						exEbDemandeTransporteur.listPlCostItem != null
					) {
						demandeTransporteur.listPlCostItem = exEbDemandeTransporteur.listPlCostItem;
					}

					this.pricingService.updateStatusQuotation(demandeTransporteur).subscribe(
						(res) => {
							exEbDemandeTransporteur.status = demandeTransporteur.status;
							this.demande.xEcStatut = DemandeStatus.WPU;
							this.canConfirmQuote = true;

							requestProcessing.afterGetResponse(event);

							this.cd.detectChanges();
						},
						(err) => {
							this.canConfirmQuote = true;

							requestProcessing.afterGetResponse(event);

							this.cd.detectChanges();
						}
					);
				}.bind(this),
				function() {
					this.canConfirmQuote = true;
				}.bind(this)
			);
		}
	}

	disableConfirmButton(demandeTransporteur: ExEbDemandeTransporteur = null) {
		return (
			(!this.isChargeur && this.userConnected.role != UserRole.CONTROL_TOWER) ||
			this.demande.xEcStatut == DemandeStatus.IN_PROCESS ||
			this.demande.xEcStatut >= DemandeStatus.WDCT ||
			this.demande.xEcCancelled ||
			(demandeTransporteur != null && demandeTransporteur.status < CarrierStatus.QUOTE_SENT)
			// || !this.hasContributionAccess
		);
	}

	getNombreOfRequestSentAndReceivedQt() {
		let nbrRequestSent = 0;
		let nbrReceivedQt = 0;
		if (this.demande.exEbDemandeTransporteurs && this.demande.exEbDemandeTransporteurs.length > 0)
			this.demande.exEbDemandeTransporteurs.forEach((dt) => {
				if (dt.status == CarrierStatus.REQUEST_RECEIVED) nbrRequestSent++;
				else if (dt.status == CarrierStatus.QUOTE_SENT || dt.status == CarrierStatus.FINAL_CHOICE)
					nbrReceivedQt++;
			});
		this.nbrRequestSent = nbrRequestSent;
		this.nbrReceivedQt = nbrReceivedQt;
	}

	get showCarriersAutocomplete(): boolean {
		return (
			this.hasContributionAccess &&
			this.demande.ebDemandeNum &&
			!this.demande.xEcCancelled &&
			this.demande.xEcStatut < DemandeStatus.FINAL_CHOICE &&
			(this.isChargeur || this.isControlTower)
		);
	}

	onEditDemandeQuoteHandler(dq: ExEbDemandeTransporteur) {
		this.newDemandeQuote = new ExEbDemandeTransporteur();

		if (this.demande.ebPartyOrigin && this.demande.ebPartyOrigin.city)
			this.newDemandeQuote.cityPickup = "" + this.demande.ebPartyOrigin.city;
		if (
			this.demande.ebPartyOrigin &&
			this.demande.ebPartyOrigin.xEcCountry &&
			this.demande.ebPartyOrigin.xEcCountry.ecCountryNum
		) {
			this.newDemandeQuote.xEcCountryPickup = new EcCountry();
			this.newDemandeQuote.xEcCountryPickup.ecCountryNum = this.demande.ebPartyOrigin.xEcCountry.ecCountryNum;
		}
		if (this.demande.ebPartyDest && this.demande.ebPartyDest.city)
			this.newDemandeQuote.cityDelivery = "" + this.demande.ebPartyDest.city;
		if (
			this.demande.ebPartyDest &&
			this.demande.ebPartyDest.xEcCountry &&
			this.demande.ebPartyDest.xEcCountry.ecCountryNum
		) {
			this.newDemandeQuote.xEcCountryDelivery = new EcCountry();
			this.newDemandeQuote.xEcCountryDelivery.ecCountryNum = this.demande.ebPartyDest.xEcCountry.ecCountryNum;
		}

		this.newDemandeQuote.xEbCompagnie = new EbCompagnie();
		this.newDemandeQuote.xEbCompagnie.ebCompagnieNum = dq.xEbCompagnie.ebCompagnieNum;
		this.newDemandeQuote.xEbCompagnie.nom = dq.xEbCompagnie.nom;
		this.newDemandeQuote.xEbCompagnie.code = dq.xEbCompagnie.code;

		this.newDemandeQuote.xEbEtablissement = new EbEtablissement();
		this.newDemandeQuote.xEbEtablissement.ebEtablissementNum =
			dq.xEbEtablissement.ebEtablissementNum;
		this.newDemandeQuote.xEbEtablissement.nom = dq.xEbEtablissement.nom;
		this.newDemandeQuote.xTransporteur = new EbUser();
		this.newDemandeQuote.xTransporteur.ebUserNum = dq.xTransporteur.ebUserNum;
		this.newDemandeQuote.xTransporteur.nom = dq.xTransporteur.nom;
		this.newDemandeQuote.xTransporteur.prenom = dq.xTransporteur.prenom;
		this.newDemandeQuote.xTransporteur.email = dq.xTransporteur.email;
		this.newDemandeQuote.horsGrille = dq.horsGrille;

		if (dq.isFromTransPlan) {
			this.newDemandeQuote.isFromTransPlan = dq.isFromTransPlan;
			this.newDemandeQuote.price = dq.price;
			this.newDemandeQuote.listPlCostItem = dq.listPlCostItem;
			this.newDemandeQuote.transitTime = dq.transitTime;
			this.newDemandeQuote.refGrille = dq.refGrille;
			this.newDemandeQuote.comment = dq.comment;
		}
	}

	onQuoteEditedHandler(dq: ExEbDemandeTransporteur) {
		if (dq === null) return;

		if (dq.xTransporteur.ebUserNum == this.userConnected.ebUserNum)
			this.setCarrierData(dq, this.userConnected);
		else if (this.selectedUserAutocomplete && this.selectedUserAutocomplete.ebUserNum) {
			this.setCarrierData(dq, this.selectedUserAutocomplete);
		}

		if (dq.price >= 0 && (!dq.status || dq.status < CarrierStatus.FINAL_CHOICE)) {
			dq.status = CarrierStatus.QUOTE_SENT;
		}

		if (this.demande.ebDemandeNum && !this.isPendingStatus) {
			let foundIndex = this.demande.exEbDemandeTransporteurs.findIndex(
				(it) => !Statique.isDefined(it.price) && this.areQrEquals(dq, it)
			);
			if (foundIndex >= 0) this.demande.exEbDemandeTransporteurs[foundIndex] = dq;
			else this.demande.exEbDemandeTransporteurs.push(dq);

			if (Statique.isDefined(dq.price)) {
				if (this.demande.xEcStatut <= DemandeStatus.RECOMMENDATION) {
					this.demande.xEcStatut = DemandeStatus.RECOMMENDATION;
					this.demande.statusQuote = CarrierStatus.QUOTE_SENT;
					this.cd.detectChanges();
				}
			}
		} else if (
			Statique.isDefined(dq.transitTime) &&
			dq.transitTime >= 0 &&
			Statique.isDefined(dq.price) &&
			dq.price >= 0
		) {
			let foundIndex = this.listDemandeQuote.findIndex(
				(it) => this.areQrEquals(it, dq) && it.isFromTransPlan == dq.isFromTransPlan
			);
			dq.isChecked = true;
			dq.status = CarrierStatus.FINAL_CHOICE;
			if (foundIndex >= 0) this.listDemandeQuote[foundIndex] = dq;
			this.carrierStatusChange(dq);
			foundIndex = this.demande.exEbDemandeTransporteurs.findIndex((it) =>
				this.areQrEquals(it, dq)
			);
			if (foundIndex >= 0) {
				this.demande.exEbDemandeTransporteurs[foundIndex] = dq;
			} else this.demande.exEbDemandeTransporteurs.push(dq);
		}
	}

	closePopUp() {
		this.newDemandeQuote = null;
	}

	disableEditQuotation() {
		return (
			!(
				(this.userConnected.role == UserRole.CONTROL_TOWER &&
					this.demande.listDemandeQuote &&
					this.demande.listDemandeQuote.find((it) => {
						return (
							it.xEbEtablissement &&
							it.xEbEtablissement.ebEtablissementNum ==
								this.userConnected.ebEtablissement.ebEtablissementNum
						);
					})) ||
				(this.userConnected.role == UserRole.PRESTATAIRE &&
					(this.userConnected.service == ServiceType.TRANSPORTEUR ||
						this.userConnected.service == ServiceType.TRANSPORTEUR_BROKER))
			) || this.demande.xEcStatut >= DemandeStatus.FINAL_CHOICE
		);
	}

	feedbackQuotationHandler(result: ExEbDemandeTransporteur) {
		this.onQuoteEditedHandler(result);
		this.newDemandeQuote = null;
		this.selectedUserAutocomplete = null;
	}

	setCarrierData(quote: ExEbDemandeTransporteur, user: EbUser) {
		quote.xTransporteur = new EbUser();
		quote.xTransporteur.ebUserNum = user.ebUserNum;
		quote.xTransporteur.nom = user.nom;
		quote.xTransporteur.prenom = user.prenom;
		quote.xTransporteur.email = user.email;

		quote.xEbEtablissement = new EbEtablissement();
		quote.xEbEtablissement.ebEtablissementNum = user.ebEtablissement.ebEtablissementNum;
		quote.xEbEtablissement.nom = user.ebEtablissement.nom;
		quote.horsGrille = user.horsGrille;

		quote.xEbCompagnie = new EbCompagnie();
		if (user.ebCompagnie) {
			quote.xEbCompagnie.ebCompagnieNum = user.ebCompagnie.ebCompagnieNum;
			quote.xEbCompagnie.nom = user.ebCompagnie.nom;
			quote.xEbCompagnie.code = user.ebCompagnie.code;
		} else if (user.ebEtablissement && user.ebEtablissement.ebCompagnie) {
			quote.xEbCompagnie.ebCompagnieNum = user.ebEtablissement.ebCompagnie.ebCompagnieNum;
			quote.xEbCompagnie.nom = user.ebEtablissement.ebCompagnie.nom;
			quote.xEbCompagnie.code = user.ebEtablissement.ebCompagnie.code;
		}
	}

	autocompleteSearch(term: string, item: EbUser) {
		return (
			item &&
			(item.email.indexOf(term) >= 0 ||
				item.nom.indexOf(term) >= 0 ||
				item.prenom.indexOf(term) >= 0)
		);
	}

	getCarrierStatutLibelle(status: number): string {
		let str = Statique.CarrierStatus.find((it) => it.key == status);
		return str ? str.value : "";
	}

	updateChatHandler(event: EbChat) {
		this.updateChatEmitter.emit(event);
	}

	areQrEquals(qr1: ExEbDemandeTransporteur, qr2: ExEbDemandeTransporteur) {
		return Statique.isDefined(qr1.exEbDemandeTransporteurNum) &&
			Statique.isDefined(qr2.exEbDemandeTransporteurNum)
			? qr1.exEbDemandeTransporteurNum == qr2.exEbDemandeTransporteurNum
			: qr1.xTransporteur.ebUserNum == qr2.xTransporteur.ebUserNum;
	}

	get tdcAcknowledgeBlock(): boolean {
		if (!this.isControlTower || this.demande.ebDemandeNum == null) return false;
		return (
			this.demande &&
			!this.demande.tdcAcknowledge &&
			this.demande.xEcStatut &&
			this.demande.xEcStatut === 1 &&
			this.userConnected.role &&
			this.userConnected.role === UserRole.CONTROL_TOWER
		);
	}

	canShowPriceInCardForViewOnly(exEbDemandeTransporteur: ExEbDemandeTransporteur) {
		return this.viewOnlyCarrierVisible && exEbDemandeTransporteur.price != null;
	}

	emitSelectedListTransporteurAutocomplete(event) {
		this.isDuplicated = false;
		if (this.demande.exEbDemandeTransporteurs != null) {
			this.demande.exEbDemandeTransporteurs.forEach((element) => {
				if (element.xTransporteur.ebUserNum == event.ebUserNum) {
					this.isDuplicated = true;
				}
			});
		}
	}

	getDataPreference(event: any = null) {
		let criteria: SearchCriteria = new SearchCriteria();
		criteria.size = null;
		criteria.ebDemandeNum = this.demande.ebDemandeNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		criteria.ebCompagnieNum = this.demande.xEbEtablissement.ebCompagnie.ebCompagnieNum;
		criteria.ebEtablissementNum = this.demande.xEbEtablissement.ebEtablissementNum;
		criteria.ebUserNum = this.demande.user.ebUserNum;
		criteria.contact = true;
		criteria.withActiveUsers = true;
		this.etablissementService.getDataPreference(criteria).subscribe(async (res) => {
			this.listTransporteurAutocomplete = res.listTransporteur;
			if (this.listTransporteurTDC != null)
				this.listTransporteurTDC.forEach((it) => {
					this.listTransporteurAutocomplete.push(it);
					this.listTransporteurAutocomplete = this.listTransporteurAutocomplete.filter(
						(t, i, a) =>
							i === a.findIndex((f) => f.ebUserNum === t.ebUserNum && f.email === t.email)
					);
				});
		});
	}
}
