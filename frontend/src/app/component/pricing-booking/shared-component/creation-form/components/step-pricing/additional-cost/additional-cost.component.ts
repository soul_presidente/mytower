import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { CostCategorie, Cost } from "@app/classes/costCategorie";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { EbDemande } from "@app/classes/demande";
import { Statique } from "@app/utils/statique";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EtablissementService } from "@app/services/etablissement.service";
import { EbUser } from "@app/classes/user";
import { CotationComponent } from "@app/component/pricing-booking/shared-component/cotation/cotation.component";
import { Decimal } from "decimal.js";
import { AuthenticationService } from "@app/services/authentication.service";
import {CarrierStatus, Modules, UserRole} from "@app/utils/enumeration";
import { DemandeStatus } from "@app/utils/enumeration";
import { PricingService } from "@app/services/pricing.service";
import { MessageService } from "primeng/api";
import { TranslateService } from "@ngx-translate/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbAdditionalCost } from "@app/classes/EbAdditionalCost";
import {EcCurrency} from "@app/classes/currency";
import {TransportationPlanService} from "@app/services/transportation-plan.service";

@Component({
	selector: "app-additional-cost",
	templateUrl: "./additional-cost.component.html",
	styleUrls: ["./additional-cost.component.scss"],
})
export class AdditionalCostComponent extends ConnectedUserComponent implements OnInit {
	listEbCompagnieCurrency : Array<EbCompagnieCurrency> = new Array<EbCompagnieCurrency>();
	searchCriteria: SearchCriteria = new SearchCriteria();
	@Input()
	isForEdit: boolean;
	@Input()
	isForAdd = false;
	@Input() userConnected: EbUser;
	@Input() ebDemande: EbDemande;
	Statique = Statique;
	@Input("module")
	ebModuleNum: number;
	isNotTransporter  = false;
	//showSaveButton = false;
	activateSaveButton = true;
	selectedCurrency: EbCompagnieCurrency;
	@Input()
	readOnly = false;

	@Input() listExEtablissementCurrency: Array<EbCompagnieCurrency> = new Array<
		EbCompagnieCurrency
	>();

	@Input()
	listEbAdditionalCosts: Array<EbAdditionalCost> = new Array<EbAdditionalCost>();

	listCompanyCurrency = new Array<EcCurrency>();
	i: number = 0;
	@ViewChild(CotationComponent, { static: true })
	cotationComponent: CotationComponent;
	constructor(
		protected etablissementService: EtablissementService,
		private pricingService: PricingService,
		protected transportationPlanService: TransportationPlanService,
		private messageService?: MessageService,
		protected translate?: TranslateService
	) {
		super();
	}

	ngOnInit() {
		if (
			this.userConnected.role == UserRole.CHARGEUR ||
			this.userConnected.role == UserRole.CONTROL_TOWER
		) {
			this.isNotTransporter = true;
		}
		// TODO
		//if (this.ebDemande.xEcStatut != DemandeStatus.PND) {
		//	this.showSaveButton = true;
		//}
		this.getSettingsCurrency();
		this.getListCompanyCurrencies();
	}

	addNewAdditionalCost() {
		if(Statique.isNotDefined(this.ebDemande.listAdditionalCost)){
			this.ebDemande.listAdditionalCost = new Array<Cost>();
		}
		let cost = new Cost();
		cost.ebCostNum = this.ebDemande.listAdditionalCost.length;
		cost.type = 2;
		cost.euroExchangeRate = 1.0;
		this.ebDemande.listAdditionalCost.push(cost);
	}

	deleteField(i: number) {
		this.ebDemande.listAdditionalCost.splice(i, 1);
		this.activateSaveButton = false;
	}

	handleChangePrice(event: number, item) {
		item.price = event;
	}

	calculCost(item: Cost) {
		if (item.price != null) {
			if (this.listExEtablissementCurrency.length > 0) {
				if (item.euroExchangeRate != null) {
					item.priceEuro = +new Decimal(item.price).times(item.euroExchangeRate).valueOf();
				} else {
					item.priceEuro = +new Decimal(item.price).times(1).valueOf();
				}
			} else {
				item.priceEuro = +new Decimal(item.price).times(1).valueOf();
			}
		} else if (item.price == null) item.priceEuro = null;
	}

	checkActivatedButton(cost) {
		if(cost.libelle != null)
		 this.activateSaveButton = (cost.libelle == null && cost.libelle.trim().isEmpty())||
			 cost.price == null || cost.xEcCurrencyNum == null;
	}
	changeCurrency(ebCost){
		this.checkActivatedButton(ebCost);
		if(this.ebDemande.exEbDemandeTransporteurs.length == 1 ){
			this.listCompanyCurrencyFiler(ebCost);
		} else if(this.ebDemande.exEbDemandeTransporteurs.length > 1){
			ebCost.euroExchangeRate = 1;
			this.calculCost(ebCost)
		}
	}


	private listCompanyCurrencyFiler(ebCost) {
		this.initSearchCriteria();
		this.searchCriteria.ebCompagnieGuestNum = this.ebDemande.exEbDemandeTransporteurs[0].xEbCompagnie.ebCompagnieNum;
		this.searchCriteria.currencyCibleNum = this.ebDemande.xecCurrencyInvoice.ecCurrencyNum;
		this.searchCriteria.currencyCostItem = ebCost.xEcCurrencyNum;
		let ebCompanyCurrency: EbCompagnieCurrency= new EbCompagnieCurrency();
		ebCost.euroExchangeRate = 1;
		this.getCompanyCurrency(ebCompanyCurrency, ebCost);
	}

	private getCompanyCurrency(ebCompanyCurrency: EbCompagnieCurrency, ebCost) {
		this.etablissementService
			.getCompagnieCurrency(this.searchCriteria).subscribe((data) => {
			ebCompanyCurrency = data;
			if (ebCompanyCurrency != null) {
				if (ebCompanyCurrency.euroExchangeRate != null) {
					ebCost.euroExchangeRate = ebCompanyCurrency.euroExchangeRate;
					ebCost.euroExchangeRateReal = ebCompanyCurrency.euroExchangeRate;
					this.calculCost(ebCost);
				}
			}
		});
	}

	updateAdditionalCost() {
		this.pricingService
			.updateAdditionalCost(this.ebDemande.ebDemandeNum, this.ebDemande.listAdditionalCost)
			.subscribe(
				(response) => {
					this.successfulOperation();
					this.activateSaveButton = true;
				},
				(err) => {
					this.echecOperation();
				}
			);
	}

	successfulOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	echecOperation() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}

	getSettingsCurrency(){
		let invoiceCurrency = new EbCompagnieCurrency();
		invoiceCurrency.ecCurrency = this.ebDemande.xecCurrencyInvoice;
		this.initSearchCriteria();
		if(this.ebDemande.exEbDemandeTransporteurs != null && this.ebDemande.exEbDemandeTransporteurs.length >= 1) {
			this.etablissementService
				.getListCompagnieCurrencysBySetting(this.searchCriteria)
				.subscribe((data) => {
						this.listEbCompagnieCurrency = data;
						this.listEbCompagnieCurrency.push(invoiceCurrency);
					}
				);
		}
	}
	initSearchCriteria(){
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie ? 
				this.userConnected.ebCompagnie.ebCompagnieNum : null;
		this.searchCriteria.module = Modules.PRICING;
		this.searchCriteria.ebCompagnieGuestNum = this.ebDemande.exEbDemandeTransporteurFinal ? 
				this.ebDemande.exEbDemandeTransporteurFinal.ebCompagnie.ebCompagnieNum : null;
		this.searchCriteria.currencyCibleNum = this.ebDemande.xecCurrencyInvoice ? 
				this.ebDemande.xecCurrencyInvoice.ecCurrencyNum : null;
	}

	getListCompanyCurrencies() {
		this.searchCriteria.communityCompany = true;
		this.searchCriteria.ebEtablissementNum = this.userConnected.ebUserNum;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.transportationPlanService
			.selectListCurrencyByeCompagnieCurrency(this.searchCriteria)
			.subscribe((data) => {
				this.listCompanyCurrency = data;
				let cibleCurrency = 
					(this.listCompanyCurrency.length > 0 && this.ebDemande.xecCurrencyInvoice)
						? this.listCompanyCurrency.find(
						(el) => el.ecCurrencyNum == this.ebDemande.xecCurrencyInvoice.ecCurrencyNum
						)
						: null;
				if (cibleCurrency == null) {
					cibleCurrency = new EcCurrency();
					cibleCurrency.code = this.ebDemande.xecCurrencyInvoice ? 
								this.ebDemande.xecCurrencyInvoice.code : null;
					cibleCurrency.ecCurrencyNum = this.ebDemande.xecCurrencyInvoice ?
								this.ebDemande.xecCurrencyInvoice.ecCurrencyNum : null;
					this.listCompanyCurrency.push(cibleCurrency);
				}
			});
	}

}
