import {
	Component,
	OnInit,
	ChangeDetectorRef,
	Output,
	EventEmitter,
	SimpleChanges,
	OnChanges,
	Input,
	ViewChild,
} from "@angular/core";
import { PricingCreationFormStepBaseComponent } from "../core/wf-step-base.component";
import { TranslateService } from "@ngx-translate/core";
import { PricingService } from "@app/services/pricing.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";
import { ModalService } from "@app/shared/modal/modal.service";
import { UserService } from "@app/services/user.service";
import { QrConsolidationService } from "@app/services/qrConsolidation.service";
import { EbQrGroupeProposition } from "@app/classes/qrGroupeProposition";
import { Statique } from "@app/utils/statique";
import { EbDemande } from "@app/classes/demande";
import {
	GroupeVentilation,
	GroupePropositionStatut,
	NatureDemandeTransport,
	DemandeStatus,
	Ventilation,
} from "@app/utils/enumeration";
import { EbQrGroupe } from "@app/classes/qrGroupe";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbCategorie } from "@app/classes/categorie";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { switchMap } from "rxjs/operators";
import { EbMarchandise } from "@app/classes/marchandise";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { StepUnitComponent } from "../step-unit/step-unit.component";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbUser } from "@app/classes/user";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
@Component({
	selector: "app-step-consolidation",
	templateUrl: "./step-consolidation.component.html",
	styleUrls: ["./step-consolidation.component.scss"],
})
export class StepConsolidationComponent extends PricingCreationFormStepBaseComponent
	implements OnInit, OnChanges {
	isQrGroupeLoading: boolean = false;
	Statique = Statique;
	GroupeVentilation = GroupeVentilation;
	GroupePropositionStatut = GroupePropositionStatut;
	listTypeDemandeByCompagnie: Array<EbTypeRequestDTO>;
	selectedDemande: EbDemande;
	ebDemande: EbDemande;
	listEbMarchandise: Array<EbMarchandise> = new Array<EbMarchandise>();
	listEbDemandeInit: Array<EbDemande>;

	@Input()
	demande: EbDemande;
	listEbDemande: any;
	typeaheadDemandeSelectEvent: EventEmitter<string> = new EventEmitter<string>();

	@Input()
	isForGrouping: boolean;

	@Input()
	listExchangeRate: Array<EbCompagnieCurrency>;
	@Input()
	statusGroupage: number;

	@Output()
	checkEvent = new EventEmitter<any>();

	@ViewChild(StepUnitComponent, { static: true })
	stepUnit: StepUnitComponent;

	message: String;
	converted: Array<number> = [];

	constructor(
		protected translate: TranslateService,
		protected pricingService: PricingService,
		protected transportationPlanService: TransportationPlanService,
		protected etablissementService: EtablissementService,
		protected authenticationService: AuthenticationService,
		protected router: Router,
		protected qrConsolidation: QrConsolidationService
	) {
		super(authenticationService, router);
	}

	ngOnInit() {
		(async function() {
			this.listTypeDemandeByCompagnie = await SingletonStatique.getListTypeRequest();
		}.bind(this)());

		this.verifyCheckedListDemande();
		this.refreshCost();
	}
	ngOnChanges(changes: SimpleChanges) {
		this.verifyCheckedListDemande();
		if (this.statusGroupage) {
			this.demande.xEcStatutGroupage = this.statusGroupage;
		}
	}
	verifyCheckedListDemande() {
		if (this.demande.ebQrGroupe && this.demande.ebQrGroupe.listPropositions[0].listEbDemande) {
			//vérification des demandes checked
			this.demande.ebQrGroupe.listPropositions[0].listEbDemande.forEach((dem) => {
				if (
					this.demande.ebQrGroupe.listPropositions[0].listSelectedDemandeNum &&
					this.demande.ebQrGroupe.listPropositions[0].listSelectedDemandeNum.indexOf(
						":" + dem.ebDemandeNum + ":"
					) >= 0
				) {
					dem.isChecked = true;

					if (this.demande.xecCurrencyInvoice.ecCurrencyNum != null) {
						this.calculateConvertedSaving(this.demande);
						dem.xecCurrencyTrResultante = this.demande.xecCurrencyInvoice;
					} else {
						this.message = this.translate.instant("REQUEST_OVERVIEW.CURRENCY_INVOICE_IS_NULL");
					}

					this.converted = [...new Set(this.converted)];

					if (this.converted.indexOf(0) >= 0) {
						this.converted.splice(this.converted.indexOf(0), 1);
					}
				}
			});
		}
	}

	calculateConvertedSaving(demande: EbDemande) {
		this.refreshCost();
		demande.ebQrGroupe.listPropositions[0].listEbDemande.forEach((dem) => {
			if (demande.ebQrGroupe.ventilation == Ventilation.EQUITABLE && dem.isChecked) {
				dem.saving = +(
					demande.saving /
					((demande.ebQrGroupe.listPropositions[0].listSelectedDemandeNum.split(":").length - 1) /
						2)
				);
			}
			if (demande.ebQrGroupe.ventilation == Ventilation.QUANTITY && dem.isChecked) {
				dem.saving = +((demande.saving * dem.totalNbrParcel) / demande.totalNbrParcel);
			}
			if (demande.ebQrGroupe.ventilation == Ventilation.TAXABLE_WEIGHT && dem.isChecked) {
				if (demande.totalTaxableWeight) {
					dem.saving = +((demande.saving * dem.totalTaxableWeight) / demande.totalTaxableWeight);
				} else {
					this.message = this.translate.instant("REQUEST_OVERVIEW.Weight_IS_NULL");
				}
			}
		});

		demande.ebQrGroupe.listPropositions[0].listEbDemande.forEach((dem) => {
			let convertedPrice =
				(dem.exEbDemandeTransporteurs[0] != undefined ?dem.exEbDemandeTransporteurs[0].price !
					:1) *
				this.exchangeRate(
					this.demande.xecCurrencyInvoice.ecCurrencyNum,
					dem,
					this.listExchangeRate
				);

			if (this.converted.indexOf(dem.ebDemandeNum) >= 0) {
				dem.convertedPrice = convertedPrice;
				dem.convertedSaving = dem.saving;
			}
		});
	}

	exchangeRate(currencyCibleNum, dem, listExchangeRate: Array<EbCompagnieCurrency>) {
		let taux = 1;
		if (listExchangeRate && listExchangeRate.length > 0) {
			let currencyComp = listExchangeRate.find(
				(item) =>
					item.xecCurrencyCible &&
					item.ecCurrency &&
					item.ecCurrency.ecCurrencyNum == dem.xecCurrencyInvoice.ecCurrencyNum &&
					item.xecCurrencyCible.ecCurrencyNum == currencyCibleNum
			);
			if (
				currencyComp != null &&
				currencyComp.euroExchangeRate != null &&
				currencyComp.dateFin >= new Date()
			) {
				this.converted.push(dem.ebDemandeNum);
				taux = currencyComp.euroExchangeRate;
				return taux;
			} else {
				this.converted.push(0);
			}
		}
		return taux;
	}

	protected checkValidity(): boolean {
		return true;
	}
	protected stepHasOptions(): boolean {
		return false;
	}

	getListDemandeInit() {
		this.listEbDemandeInit = [];
		this.typeaheadDemandeSelectEvent
			.pipe(
				switchMap((term) => {
					let criteria = new SearchCriteriaPricingBooking();
					criteria.searchterm = term;
					return this.pricingService.getListDemandeInitials(criteria);
				})
			)
			.subscribe(
				(data: Array<EbDemande>) => {
					this.demande.ebQrGroupe.listPropositions[0].listEbDemande.forEach(function(arrayItem) {
						data.forEach(function(y) {
							if (y.ebDemandeNum == arrayItem.ebDemandeNum) {
								var index = data.indexOf(y);
								data.splice(index, 1);
							}
						});
					});
					this.listEbDemandeInit = data;
				},
				(err) => {
					this.listEbDemandeInit = [];
				}
			);
	}

	async getSelectEbDemande() {
		this.selectedDemande.isChecked = true;
		this.demande.consolidationListTransportReferenceInfos.push({
			ebDemandeNum: this.selectedDemande.ebDemandeNum,
			refTransport: this.selectedDemande.refTransport,
		});
		this.demande.ebQrGroupe.listPropositions[0].listDemandeNum =
			this.demande.ebQrGroupe.listPropositions[0].listDemandeNum +
			":" +
			this.selectedDemande.ebDemandeNum +
			":";
		this.demande.ebQrGroupe.listPropositions[0].nombreDemandes += 1;
		this.demande.ebQrGroupe.listPropositions[0].listEbDemande.push(this.selectedDemande);
		let ListEbMarchandises: Array<EbMarchandise> = this.demande.listMarchandises;
		ListEbMarchandises.forEach((x) => {
			this.demande.totalNbrParcel += x.numberOfUnits;
			this.demande.totalTaxableWeight += x.weight;
			this.demande.totalVolume += x.volume;
			this.demande.totalWeight += x.weight;
		});
		this.onCheckDemande(this.selectedDemande);
		this.refreshCost();
		this.selectedDemande = null;
	}

	refreshCost() {
		this.pricingService.CalculTotalPrice(this.demande, this.isForGrouping, this.listExchangeRate);
	}

	getListDemandesByProp(prop: EbQrGroupeProposition, indice: number) {
		let proposition = Statique.cloneObject(prop, new EbQrGroupeProposition());
		proposition.currentPage += indice;
		proposition.listEbDemande = null;
		proposition.ebDemandeResult = null;

		this.qrConsolidation
			.getListDemandesInProposition(proposition)
			.subscribe((res: Array<EbDemande>) => {
				prop.listEbDemande = res;
				//vérification des demandes checked
				prop.listEbDemande.forEach((dem) => {
					if (
						prop.listSelectedDemandeNum &&
						prop.listSelectedDemandeNum.indexOf(":" + dem.ebDemandeNum + ":") >= 0
					) {
						dem.isChecked = true;
					}
				});
				prop.currentPage += indice;
			});
	}

	applyCardClass(card) {
		let cls = "qrgroup-propo-card";
		if (card.isDemandeResult) {
			cls += " qr-result ";
		}
		return cls;
	}

	getGainDemandeQr(grp: EbQrGroupe, propo: EbQrGroupeProposition, ebDemande: EbDemande): string {
		let result = "",
			priceTFApresConsolidation = 0,
			gain = 0,
			totalNb = 0,
			curNb = 0, //totalWeigth or totalUnit
			totalTf = 0;

		propo.listEbDemande
			.filter((d) => d.isChecked)
			.forEach((d) => {
				totalTf++;
				if (grp.ventilation == GroupeVentilation.AU_PRORATA_DU_POIDS_TAXABLE) {
					if (d.totalTaxableWeight) {
						totalNb += d.totalTaxableWeight;
						if (d.ebDemandeNum == ebDemande.ebDemandeNum) curNb = d.totalTaxableWeight;
					}
				} else if (grp.ventilation == GroupeVentilation.AU_PRORATA_DES_QUANTITES) {
					if (d.totalNbrParcel) {
						totalNb += d.totalNbrParcel;
						if (d.ebDemandeNum == ebDemande.ebDemandeNum) curNb = d.totalNbrParcel;
					}
				}
			});

		let resultatPriceConsolidation = propo.ebDemandeResult.ventilationPrice;
		if (grp.ventilation == GroupeVentilation.EQUITABLEMENT_PAR_DOSSIER_DE_TRANSPORT) {
			curNb = 1;
			totalNb = totalTf;
		}

		if (
			resultatPriceConsolidation &&
			totalNb &&
			curNb &&
			grp.ventilation != GroupeVentilation.SANS_PONDERATION
		) {
			priceTFApresConsolidation = +((resultatPriceConsolidation * curNb) / totalNb).toFixed(2);
			gain = Math.floor((priceTFApresConsolidation / resultatPriceConsolidation) * 100);

			result = priceTFApresConsolidation + "";

			if (propo.ebDemandeResult.xecCurrencyInvoice)
				result += " " + propo.ebDemandeResult.xecCurrencyInvoice.code;

			result += " ( " + gain + "% )";

			return result;
		}

		return "-";
	}
	cardChange(event) {
		this.onCheckDemande(event);
		this.refreshCost();
	}
	onCheckDemande(checkedTr: EbDemande) {
		if (checkedTr) {
			if (checkedTr.isChecked) {
				this.demande.ebQrGroupe.listPropositions[0].listSelectedDemandeNum +=
					":" + checkedTr.ebDemandeNum + ":";
			} else {
				this.demande.ebQrGroupe.listPropositions[0].listSelectedDemandeNum = this.demande.ebQrGroupe.listPropositions[0].listSelectedDemandeNum
					.split(":" + checkedTr.ebDemandeNum + ":")
					.join("");
			}
		}

		if (this.demande.xEbUserOriginCustomsBroker) {
			this.demande.xEbUserOriginCustomsBroker = null;
		}

		if (this.demande.xEbUserDestCustomsBroker) {
			this.demande.xEbUserDestCustomsBroker = null;
		}

		let schemaLite = new EbTtSchemaPsl();
		if (this.demande.xEbSchemaPsl != null) {
			schemaLite.constructorLite(this.demande.xEbSchemaPsl);
			this.demande.xEbSchemaPsl = schemaLite;
		}

		if (this.demande.ebQrGroupe) {
			this.demande.ebQrGroupe.compatibilityCriteria = null;
			this.demande.ebQrGroupe.listPropositions.forEach((value, index1) => {
				this.demande.ebQrGroupe.listPropositions[index1].ebDemandeResult = null;
				if (this.demande.ebQrGroupe.listPropositions[index1].listEbDemande) {
					this.demande.ebQrGroupe.listPropositions[index1].listEbDemande.forEach(
						(value, index2) => {
							let demandeLite = new EbDemande();
							demandeLite.constructorLite(
								this.demande.ebQrGroupe.listPropositions[index1].listEbDemande[index2]
							);
							this.demande.ebQrGroupe.listPropositions[index1].listEbDemande[index2] = demandeLite;
							this.demande.ebQrGroupe.listPropositions[index1].listEbDemande[
								index2
							].xEbSchemaPsl = null;

							this.demande.ebQrGroupe.listPropositions[index1].listEbDemande[
								index2
							].exEbDemandeTransporteurs.forEach((value, index) => {
								if (
									this.demande.ebQrGroupe.listPropositions[index1].listEbDemande[index2]
										.exEbDemandeTransporteurs[index].xEbEtablissement
								) {
									this.demande.ebQrGroupe.listPropositions[index1].listEbDemande[
										index2
									].exEbDemandeTransporteurs[index].xEbEtablissement.ebCompagnie = null;
								}

								this.demande.ebQrGroupe.listPropositions[index1].listEbDemande[
									index2
								].exEbDemandeTransporteurs[index].xEbCompagnie = null;
							});

							if (
								this.demande.ebQrGroupe.listPropositions[index1].listEbDemande[index2]
									.xEbEtablissement
							) {
								this.demande.ebQrGroupe.listPropositions[index1].listEbDemande[
									index2
								].xEbEtablissement.ebCompagnie = null;
							}

							if (
								this.demande.ebQrGroupe.listPropositions[index1].listEbDemande[index2]
									.listCategories
							) {
								this.demande.ebQrGroupe.listPropositions[index1].listEbDemande[
									index2
								].listCategories.forEach((value, index) => {
									this.demande.ebQrGroupe.listPropositions[index1].listEbDemande[
										index2
									].listCategories[index].compagnie = null;
								});
							}
						}
					);
				}
			});
		}

		if (this.demande.user) {
			this.demande.user.ebCompagnie = null;
		}

		if (this.demande.listCategories) {
			this.demande.listCategories.forEach((value, index) => {
				this.demande.listCategories[index].compagnie = null;
			});
		}

		if (this.demande.xEbEtablissement) {
			this.demande.xEbEtablissement.ebCompagnie = null;
		}

		if (this.demande.exEbDemandeTransporteurs) {
			this.demande.exEbDemandeTransporteurs.forEach((value, index) => {
				this.demande.exEbDemandeTransporteurs[index].xEbDemande = null;
				this.demande.exEbDemandeTransporteurs[index].xEbCompagnie = null;
			});
		}

		this.demande.xEbQrGroupe = null;


		this.qrConsolidation
			.setCommonDataInResultingRequest(this.demande, checkedTr.ebDemandeNum, checkedTr.isChecked)
			.subscribe((res: EbDemande) => {
				let oldListCategories: Array<EbCategorie> = this.demande.listCategories,
					newListCategories: Array<EbCategorie> = res.listCategories;

				Statique.cloneObject(res, this.demande);
				this.demande.listCategories = oldListCategories;
				if (this.demande.listCategories) {
					this.demande.listCategories.forEach((it) => {
						let cat: EbCategorie = newListCategories.find(
							(ca) => ca.ebCategorieNum == it.ebCategorieNum
						);
						it.selectedLabel = cat ? cat.labels[0] : null;
					});
				} else {
					this.demande.listCategories = [];
				}
				this.verifyCheckedListDemande();
				this.checkEvent.emit(true);
			});
	}
}
