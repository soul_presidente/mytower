import {
	ChangeDetectorRef,
	Component,
	Input,
	Output,
	OnInit,
	EventEmitter,
	SimpleChanges,
	OnChanges,
} from "@angular/core";
import { Statique } from "@app/utils/statique";
import { PricingService } from "@app/services/pricing.service";
import { EbCostCenter } from "@app/classes/costCenter";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { UserRole, TypeDataEbDemande, DemandeStatus } from "@app/utils/enumeration";
import { EtablissementService } from "@app/services/etablissement.service";
import { EcCurrency } from "@app/classes/currency";
import { Router } from "@angular/router";
import { AuthenticationService } from "@app/services/authentication.service";
import { EbCategorie } from "@app/classes/categorie";
import { IField } from "@app/classes/customField";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { TypeRequestService } from "@app/services/type-request.service";
import { EbCompagnie } from "@app/classes/compagnie";
import { EbChat } from "@app/classes/chat";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { PricingCreationFormStepBaseComponent } from "../core/wf-step-base.component";

@Component({
	selector: "app-step-reference",
	templateUrl: "./step-reference.component.html",
	styleUrls: ["./step-reference.component.scss"],
})
export class StepReferenceComponent extends PricingCreationFormStepBaseComponent
	implements OnInit, OnChanges {
	Statique = Statique;
	UserRole = UserRole;

	optionInformationsComplementaires: boolean = false;
	optionShareMoreUsers = false;
	optionPartner = false;

	@Input()
	listCostCenter: Array<EbCostCenter>;

	@Input()
	hasContributionAccess: boolean = false;

	@Output()
	updateChatEmitter? = new EventEmitter<EbChat>();

	showCategFields: boolean = true;
	required: boolean = true;
	selectedUserNum: number;
	toggleEditData: boolean = false;
	selectedCompagnie: EbCompagnie;
	selectedEtabNum: number;

	listCurrencyDemande: Array<EcCurrency> = new Array<EcCurrency>();

	listCurrencyDemandeFilterd: Array<EcCurrency> = new Array<EcCurrency>();

	@Input()
	listTypeDemandeByCompagnie: Array<EbTypeRequestDTO>;
	@Input()
	listCategorie: Array<EbCategorie>;
	@Input()
	listFields: Array<IField>;

	@Input()
	deletedCustomFields: Array<IField>;

	@Input()
	listExportControl: Array<any>;

	exportControlSelectedValue: any = null;

	invoiceCurrencyBlur: boolean = false;
	insuranceCurrencyBlur: boolean = false;
	DemandeStatus = DemandeStatus;

	// edit management
	@Input()
	readOnly: boolean = false;
	@Input()
	requiredReadOnly: boolean = false;

	constructor(
		protected typeRequestService: TypeRequestService,
		protected pricingService: PricingService,
		protected etablissementService: EtablissementService,
		protected cd: ChangeDetectorRef,
		protected authenticationService: AuthenticationService,
		protected router: Router,
		protected modalService: ModalService,
		protected translate: TranslateService
	) {
		super(authenticationService, router);
	}

	ngOnInit() {
		super.ngOnInit();

		let criteria = new SearchCriteria();
		criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.etablissementService.getListCurrencyForDemande(criteria).subscribe(
			function(data) {
				if (data) {
					this.listCurrencyDemande = data;
					this.listCurrencyDemande = Statique.getUnique(this.listCurrencyDemande, "ecCurrencyNum");
				}
			}.bind(this)
		);
		if (
			this.demande.optionShareMoreUsers ||
			(this.demande &&
				(this.demande.xEbUserDest ||
					this.demande.xEbUserOrigin ||
					this.demande.xEbUserDestCustomsBroker ||
					this.demande.xEbUserOriginCustomsBroker ||
					{}))["ebUserNum"]
		) {
			this.optionShareMoreUsers = true;
			//afficher le bloc options seulement pour la création
			if (!this.readOnly) {
				this.optionsDisplayed = true;
			}
		}
		if (this.demande.optionInformationsComplementaires || this.demande.commentChargeur) {
			this.optionInformationsComplementaires = true;
			if (!this.readOnly) {
				this.optionsDisplayed = true;
			}
		}
		//afficher people et complementary infos dans detail de la vue simplifiée
		if (this.readOnly) {
			this.optionInformationsComplementaires = true;
			this.optionShareMoreUsers = true;
		}

		if (
			this.demande.optionPartner ||
			(this.demande &&
				(this.demande.xEbUserDest ||
					this.demande.xEbUserOrigin ||
					this.demande.xEbUserDestCustomsBroker ||
					this.demande.xEbUserOriginCustomsBroker ||
					{}))["ebUserNum"]
		) {
			this.optionPartner = true;
			//afficher le bloc options seulement pour la création
			if (!this.readOnly) {
				this.optionsDisplayed = true;
			}
		}
	}

	ngOnChanges(sp: SimpleChanges) {
		if (sp.listTypeDemandeByCompagnie) {
			let prevListReq = sp.listTypeDemandeByCompagnie.previousValue;
			let curListReq = sp.listTypeDemandeByCompagnie.currentValue;
			if (
				(!prevListReq && curListReq) ||
				(prevListReq && curListReq && prevListReq.length != curListReq.length)
			) {
				// verifier si le Type of request de la demande est desactivé si oui on va l'ajouter dans la liste des TRs
				// on prend libelle qui enregistrer au niveau de table eb_demande
				let isTypeRequestDisabled = true;
				this.listTypeDemandeByCompagnie.forEach((elm) => {
					if (elm.ebTypeRequestNum == this.demande.xEbTypeRequest) {
						isTypeRequestDisabled = false;
						elm.refLibelle = this.demande.typeRequestLibelle;
					}
				});
				if (isTypeRequestDisabled) {
					let typeOfRequest = new EbTypeRequestDTO();
					typeOfRequest.ebTypeRequestNum = this.demande.xEbTypeRequest;
					typeOfRequest.libelle = this.demande.typeRequestLibelle;
					typeOfRequest.refLibelle = this.demande.typeRequestLibelle;
					if (typeOfRequest.refLibelle && typeOfRequest.ebTypeRequestNum != null)
						this.listTypeDemandeByCompagnie.push(typeOfRequest);
				}
			}
		}

		if (sp.listExportControl) {
			if (this.demande && this.demande.ebDemandeNum && this.listExportControl) {
				let exportControlStatut = this.listExportControl.find(
					(exCtrl) => exCtrl.code == this.demande.exportControlStatut
				);
				this.exportControlSelectedValue =
					exportControlStatut != null ? exportControlStatut.key : null;
			}
		}
	}

	getCategorieFieldsAndCostCenter() {
		this.listFields = null;
		this.listCategorie = null;
		// this.listCostCenter = null;

		let criteria: SearchCriteria = new SearchCriteria();
		criteria.size = null;
		// criteria.roleBroker = true;
		// criteria.userService = ServiceType.BROKER;
		criteria.ebDemandeNum = this.demande.ebDemandeNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;

		if (!this.demande.ebDemandeNum || this.toggleEditData) {
			if (this.toggleEditData && this.demande.user && this.demande.user.ebUserNum) {
				criteria.ebCompagnieNum = this.demande.user.ebCompagnie.ebCompagnieNum;
				criteria.ebEtablissementNum = this.demande.user.ebEtablissement.ebEtablissementNum;
				criteria.contact = true;
				criteria.ebUserNum = this.demande.user.ebUserNum;
			}
		} else {
			criteria.ebCompagnieNum = this.demande.xEbEtablissement.ebCompagnie.ebCompagnieNum;
			criteria.ebEtablissementNum = this.demande.xEbEtablissement.ebEtablissementNum;
			criteria.ebUserNum = this.demande.user.ebUserNum;
			criteria.contact = true;
		}

		if (!criteria.ebEtablissementNum || !criteria.ebCompagnieNum) {
			this.listFields = new Array<IField>();
			this.listCategorie = new Array<EbCategorie>();
			this.listCostCenter = new Array<EbCostCenter>();
			return;
		}

		this.etablissementService.getDataPreference(criteria).subscribe(async (res) => {
			this.listCostCenter =
				res && res.listCostCenter ? res.listCostCenter : new Array<EbCostCenter>();
			this.listCategorie = res && res.listCategorie ? res.listCategorie : new Array<EbCategorie>();
			this.listFields =
				res && res.customField && res.customField.fields
					? JSON.parse(res.customField.fields)
					: new Array<IField>();
			this.listFields = this.listFields || new Array<IField>();

			if (this.listCategorie && this.listCategorie.length > 0) {
				this.listCategorie.forEach((it) => {
					if (it.labels && it.labels.length > 0) {
						it.labels.sort((a, b) => {
							return a.libelle.localeCompare(b.libelle);
						});
					}
				});

				this.listCategorie = this.listCategorie.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}
		});
	}
	showInformationsCompementaires(event) {
		if (event) {
			this.demande.optionInformationsComplementaires = this.optionInformationsComplementaires;
		}
	}
	showPartner(event) {
		if (event) {
			this.demande.optionPartner = this.optionPartner;
		}
	}
	showShareMoreUsers(event) {
		if (event) {
			this.demande.optionShareMoreUsers = this.optionShareMoreUsers;
		}
	}
	//recuperation de text sur select des type of request
	selectchange(args) {
		this.demande.typeRequestLibelle = args.target.options[args.target.selectedIndex].text;
	}

	getTypeOfDemande() {
		let criteria = new SearchCriteria();
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		criteria.activated = true;
		// permet de recupere la liste de type of request activé par companie
		this.typeRequestService
			.getListTypeRequests(criteria)
			.subscribe((res: Array<EbTypeRequestDTO>) => {
				this.listTypeDemandeByCompagnie = res;
				// verifier si le Type of request de la demande est desactivé si oui on va l'ajouter dans la liste des TRs
				// on prend libelle qui enregistrer au niveau de table eb_demande
				let isTypeRequestDisabled = true;
				this.listTypeDemandeByCompagnie.forEach((elm) => {
					if (elm.ebTypeRequestNum == this.demande.xEbTypeRequest) {
						isTypeRequestDisabled = false;
						elm.refLibelle = this.demande.typeRequestLibelle;
					}
				});
				if (isTypeRequestDisabled) {
					let typeOfRequest = new EbTypeRequestDTO();
					typeOfRequest.ebTypeRequestNum = this.demande.xEbTypeRequest;
					typeOfRequest.libelle = this.demande.typeRequestLibelle;
					typeOfRequest.refLibelle = this.demande.typeRequestLibelle;
					this.listTypeDemandeByCompagnie = [typeOfRequest].concat(this.listTypeDemandeByCompagnie);
				}
			});
	}

	protected checkValidity(): boolean {
		return (
			this.demande.xEbTypeRequest != null &&
			this.demande.xecCurrencyInvoice != null &&
			// if insurance is selected
			(this.demande.insurance
				? // check if currency is set
				  this.demande.xecCurrencyInsurance != null &&
				  // check if it has a value and the value is greater than zero
				  (this.demande.insuranceValue != null
						? this.demande.insuranceValue > 0
						: // if not
						  false)
				: // if insurance is not selected it's valid
				  true)
		);
	}

	protected stepHasOptions(): boolean {
		return true;
	}

	setInsurance($event: MouseEvent) {
		// reset insuranceValue when insurance checkBox "NO" is selected (only if we are in creation mode)
		if (!this.demande.ebDemandeNum && $event.returnValue === false) {
			this.demande.insuranceValue = null;
			this.demande.insurance = false;
			this.demande.xecCurrencyInsurance = null;
		}
	}

	setInvoiceCurrencyBlur() {
		this.invoiceCurrencyBlur = true;
	}

	setInsuranceCurrencyBlur() {
		this.insuranceCurrencyBlur = true;
	}
}
