import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbLabel } from "@app/classes/label";

import {
	ChangeDetectorRef,
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	ViewChild,
} from "@angular/core";
import { EbDemande } from "@app/classes/demande";
import { EcCountry } from "@app/classes/country";
import { EbCostCenter } from "@app/classes/costCenter";
import { EbCategorie } from "@app/classes/categorie";
import { PricingService } from "@app/services/pricing.service";
import { EbUser } from "@app/classes/user";
import { keyValue, Statique } from "@app/utils/statique";
import {
	AdresseEligibility,
	ModeTransport,
	NatureDemandeTransport,
	TypeDataEbDemande,
	UserRole,
} from "@app/utils/enumeration";
import { OldPricingMaskFillerComponent } from "../../../saved-forms/pricing-mask/old-pricing-mask-filler.component";
import { DialogService } from "@app/services/dialog.service";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { IField } from "@app/classes/customField";
import { FavoriComponent } from "@app/shared/favori/favori.component";
import { TrackTraceService } from "@app/services/trackTrace.service";
import { EbEtablissement } from "@app/classes/etablissement";
import { UserService } from "@app/services/user.service";
import { EbCompagnie } from "@app/classes/compagnie";
import { ModalService } from "@app/shared/modal/modal.service";
import { EbAdresse } from "@app/classes/adresse";
import { EbParty } from "@app/classes/party";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EcCurrency } from "@app/classes/currency";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { StatiqueService } from "@app/services/statique.service";
import { ActivatedRoute } from "@angular/router";
import { SavedForm } from "@app/classes/savedForm";
import { CollectionService } from "@app/services/collection.service";
import { EbChat } from "@app/classes/chat";
import { TranslateService } from "@ngx-translate/core";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import { debounceTime, distinctUntilChanged, switchMap } from "rxjs/operators";
import {
	CompleterData,
	CompleterItem,
	CompleterService,
} from "../../../../../../node_modules/ng2-completer";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { CompagnieService } from "@app/services/compagnie.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbTypeTransport } from "@app/classes/EbTypeTransport";
import { DockManagementService } from "@app/services/dock-management.service";
import { EbEntrepotDTO } from "@app/classes/dock/EbEntrepotDTO";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { ExternalUsersComponent } from "../external-users/external-users.component";
import { ConfigPslCompanyService } from "@app/services/config-psl-company.service";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { TypeRequestService } from "@app/services/type-request.service";

@Component({
	selector: "app-shipping-information",
	templateUrl: "./shipping-information.component.html",
	styleUrls: ["./shipping-information.component.scss"],
})
export class ShippingInformationComponent extends OldPricingMaskFillerComponent
	implements OnInit, OnChanges {
	@Input()
	hasContributionAccess: number;
	@Input("module")
	ebModuleNum: number;
	@Input()
	ebDemande: EbDemande;
	@Input()
	userConnected: EbUser;
	@Input()
	dataPreference: Object;
	@Output()
	onValidate = new EventEmitter<any>();
	@Output()
	onListTransporteurLoaded = new EventEmitter<Array<EbUser>>();
	@Output()
	updateChatEmitter = new EventEmitter<EbChat>();
	@ViewChild("externalUsersComponent", { static: false })
	externalUsersComponent: ExternalUsersComponent;

	@ViewChild("pageValid", { static: false })
	pageValid: ElementRef;
	@ViewChild(FavoriComponent, { static: true })
	favoriComponentComponent: FavoriComponent;

	UserRole = UserRole;

	listCostCenter: Array<EbCostCenter>;
	listCountry: Array<EcCountry> = new Array<EcCountry>();
	listZone: Array<EbPlZoneDTO> = new Array<EbPlZoneDTO>();
	listEntrepot = new Array<EbEntrepotDTO>();
	listAdresse: Array<EbAdresse> = new Array<EbAdresse>();

	Statique = Statique;
	typeTransportAir: Array<EbTypeTransport> = new Array<EbTypeTransport>();
	typeTransportSea: Array<EbTypeTransport> = new Array<EbTypeTransport>();
	typeTransportRoad: Array<EbTypeTransport> = new Array<EbTypeTransport>();
	modeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();
	yesOrNo: any = Statique.yesOrNo;
	incoterms: Array<keyValue> = new Array<keyValue>();

	adresseToSave: EbAdresse;
	adresseToSaveSection: string;

	typeDemande: any = Statique.typeDemande;
	ModeTransportEnum = ModeTransport;
	suggestionsEtablissement: CompleterData;
	listBrokers: Array<EbUser>;
	listTransporteur: Array<EbUser>;
	searchCriteria: SearchCriteria = new SearchCriteria();

	selectedCompagnie: EbCompagnie;
	selectedEtabNum: number;
	typeaheadEtabEvent: EventEmitter<string> = new EventEmitter<string>();
	listEtablissement: any = new Array<EbEtablissement>();
	selectedUserNum: number;
	selectedUser: EbUser;
	typeaheadUserEvent: EventEmitter<string> = new EventEmitter<string>();
	listUserEtab: any = new Array<EbUser>();
	listCurrencyDemande: Array<EcCurrency> = new Array<EcCurrency>();
	listEtablissementPlateform: Array<EbEtablissement> = new Array<EbEtablissement>();

	toggleEditData: boolean = false;
	oldEbDemande: EbDemande;
	listCategorie: Array<EbCategorie>;
	listFields: Array<IField>;

	searchNotify: string;
	NatureDemandeTransport = NatureDemandeTransport;

	listPointIntermediate = new Array<EbParty>();

	showAddressFields: boolean = false;
	DestFields: boolean = false;
	@Input()
	showAddressWrapper: boolean;
	showDestInfoWrapper: boolean = false;
	showNotifyFields: boolean = true;
	showCategFields: boolean = false;
	showParamsFields: boolean = false;
	libelleOriginCountry: string;

	ebPartyOrigin: EbParty = new EbParty();
	ebPartyDest: EbParty = new EbParty();

	from: string = "From";
	to: string = "To";

	listSchemaPsl: Array<EbTtSchemaPsl> = new Array<EbTtSchemaPsl>();
	listSchemaPslFiltered: Array<EbTtSchemaPsl> = new Array<EbTtSchemaPsl>();
	listPsl: Array<any> = new Array<any>();
	selectedPsl: Array<any> = new Array<any>();
	selectedSchema: EbTtSchemaPsl;

	listTypeDemandeByCompagnie: Array<EbTypeRequestDTO>;

	constructor(
		private pricingService: PricingService,
		private completerService: CompleterService,
		protected savedFormsService: SavedFormsService,
		protected dialogService: DialogService,
		private etablissementService: EtablissementService,
		private compagnieService: CompagnieService,
		private trackTraceService: TrackTraceService,
		private transportationPlanService: TransportationPlanService,
		protected dockManagementService: DockManagementService,
		private userService: UserService,
		protected modalService: ModalService,
		protected cd: ChangeDetectorRef,
		protected statiqueService: StatiqueService,
		protected activatedRoute: ActivatedRoute,
		private collectionService: CollectionService,
		protected translate: TranslateService,
		private cdRef: ChangeDetectorRef,
		private companyPslService: ConfigPslCompanyService,
		private typeRequestService: TypeRequestService
	) {
		super(savedFormsService, dialogService, modalService);

		this.listFields = new Array<IField>();
		this.listCategorie = new Array<EbCategorie>();

		super.cloneEbDemandeWithSf = this.cloneEbDemandeWithSf.bind(this);
	}
	getStatiques() {
		(async () =>
			(this.typeTransportAir = await SingletonStatique.getListTypeTransportByModeTransport(
				ModeTransport.AIR
			)))();
		(async () =>
			(this.typeTransportSea = await SingletonStatique.getListTypeTransportByModeTransport(
				ModeTransport.SEA
			)))();
		(async () =>
			(this.typeTransportRoad = await SingletonStatique.getListTypeTransportByModeTransport(
				ModeTransport.ROAD
			)))();

		(async () => {
			this.modeTransport = await SingletonStatique.getListModeTransport();
			this.modeTransport = this.modeTransport.sort((a, b) => a.order - b.order);
		})();
	}
	ngAfterViewChecked() {
		this.cdRef.detectChanges();
	}
	ngOnChanges() {
		if (!this.selectedPsl || !this.selectedPsl.length) {
			this.selectedPsl = Statique.getListActivePsl(this.ebDemande.xEbSchemaPsl).map(
				(it) => it.ebTtCompanyPslNum
			);
			this.listPsl = this.ebDemande.xEbSchemaPsl.listPsl;
		}
	}
	ngOnInit() {
		if (this.ebDemande.ebPartyNotif == null) {
			this.ebDemande.ebPartyNotif = new EbParty();
		}
		if (this.ebDemande.ebPartyOrigin == null) {
			this.ebDemande.ebPartyOrigin = new EbParty();
		}
		if (this.ebDemande.ebPartyDest == null) {
			this.ebDemande.ebPartyDest = new EbParty();
		}

		this.getStatiques();
		this.subscribeToSavedFormService();
		this.toggleEditData =
			Statique.isDefined(this.ebDemande.ebDemandeNum) &&
			this.hasContributionAccess &&
			((this.isChargeur &&
				this.ebDemande.xEbEtablissement.ebEtablissementNum ==
					this.userConnected.ebEtablissement.ebEtablissementNum) ||
				this.isControlTower);

		this.activatedRoute.queryParams.subscribe((params) => {
			let num = params["sf"];
			if (num) {
				let sf: SavedForm = JSON.parse(localStorage.getItem("sf" + num));
				if (sf && sf.ebSavedFormNum == +num) {
					Statique.removeLSItemsWithPrefix("sf");
					this.fillFormWithSf(sf);
					this.ebDemande.exEbDemandeTransporteurs = new Array<ExEbDemandeTransporteur>();
					this.ebDemande.listTransporteurs = new Array<EbUser>();
				}
			}
		});

		this.ebPartyOrigin = this.ebDemande.ebPartyOrigin;
		this.ebPartyDest = this.ebDemande.ebPartyDest;

		if (
			this.ebDemande.pointIntermediate == null ||
			this.ebDemande.pointIntermediate.ebPartyNum == null
		) {
			this.ebDemande.pointIntermediate = null;
		}

		// this.panelCommentClose();
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteria.setUser(this.userConnected);

		//let listCurrencyloaded: number = 1;
		/* 	this.statiqueService.getListEcCurrency().subscribe(function(data){
				this.listCurrencyBase = data;
				listCurrencyloaded--;
				if(listCurrencyloaded <= 0)
					this.fillListCurrencyDemande();
			}.bind(this)); */

		this.etablissementService.getListCurrencyForDemande(this.searchCriteria).subscribe(
			function(data) {
				if (data) {
					this.listCurrencyDemande = data;
				}
			}.bind(this)
		);

		let criteria = new SearchCriteria();
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		this.typeRequestService
			.getListTypeRequests(criteria)
			.subscribe((res: Array<EbTypeRequestDTO>) => {
				this.listTypeDemandeByCompagnie = res;
			});

		this.setSuggestionsEtablissement();

		// this.getlistinco();

		this.getCountries();

		this.getEstablishments();
		this.getZones();
		this.getEntrepots();
		this.getPointIntermediates();
		this.refreshListAdresse();
		// this.getPslList();

		if (this.isChargeur) {
			let user = new EbUser();
			user.constructorCopy(this.userConnected);
			this.listUserEtab = new Array<EbUser>();
			this.listUserEtab.push(user);
			this.handleSelectedUserChange(user);
		} else {
			this.getDataPreference();
		}

		this.initAutocomplete();
		//this.getIncotermOfModeTransport();

		//this.listPsl1.push("PIC")
	}

	getPointIntermediates() {
		let compagnieToFilter: number = null;
		let etablissementToFilter: number = null;
		if (!this.isControlTower) {
			compagnieToFilter = this.userConnected.ebCompagnie.ebCompagnieNum;
		}
		if (!this.userConnected.superAdmin) {
			etablissementToFilter = this.userConnected.ebEtablissement.ebEtablissementNum;
		}

		this.pricingService
			.searchAdresseAsParty(
				"",
				compagnieToFilter,
				etablissementToFilter,
				AdresseEligibility.INTERMEDIATE_POINT
			)
			.subscribe(
				function(data) {
					if (data) {
						this.listPointIntermediate = data;
					}
				}.bind(this)
			);
	}
	setSuggestionsEtablissement() {
		let autocompleteUrl = `/adresseAsParty`;

		if (!this.isControlTower) {
			autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
			autocompleteUrl += `ebCompagnieNum=${this.userConnected.ebCompagnie.ebCompagnieNum}`;

			if (!this.userConnected.superAdmin) {
				autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
				autocompleteUrl += `ebEtablissementNum=${this.userConnected.ebEtablissement.ebEtablissementNum}`;
			}
		}

		if (this.selectedCompagnie && this.selectedCompagnie.ebCompagnieNum) {
			autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
			autocompleteUrl += `ebCompagnieNum=${this.selectedCompagnie.ebCompagnieNum}`;

			if (
				this.selectedEtabNum &&
				this.selectedUser &&
				this.selectedUser.ebUserNum == this.selectedUserNum &&
				!this.selectedUser.superAdmin
			) {
				autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
				autocompleteUrl += `ebEtablissementNum=${this.selectedEtabNum}`;
			}
		}

		autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
		autocompleteUrl += `term=`;

		this.suggestionsEtablissement = this.completerService.remote(
			Statique.controllerPricing + autocompleteUrl,
			null,
			"etablissementReference"
		);
	}

	async cloneEbDemandeWithSf(parsedValues: EbDemande, fieldsAndCategoriesOnly = false) {
		if (!this.ebDemande) this.ebDemande = new EbDemande();

		if (!fieldsAndCategoriesOnly) {
			let user = null;
			if (!parsedValues.user || !parsedValues.user.ebUserNum)
				user = Statique.cloneObject(this.ebDemande.user, new EbUser());
			else user = Statique.cloneObject(parsedValues.user, new EbUser());

			this.ebDemande.cloneData(parsedValues);
			this.ebDemande.user = user;
			if (this.ebDemande.user && this.ebDemande.user.ebUserNum) {
				this.listUserEtab = new Array<EbUser>();
				this.listUserEtab.push(this.ebDemande.user);
				this.handleSelectedUserChange(this.ebDemande.user);
			}

			this.ebDemande.ebDemandeNum = (this.ebDemande || {})["ebDemandeNum"];
			this.ebPartyOrigin = parsedValues.ebPartyOrigin;
			this.ebPartyDest = parsedValues.ebPartyDest;
			if (
				parsedValues.ebPartyOrigin &&
				parsedValues.ebPartyOrigin.city &&
				parsedValues.ebPartyDest &&
				parsedValues.ebPartyDest.city
			) {
				this.showAddressWrapper = true;
				this.showAddressFields = false;
				this.showAddAddressBtn = false;
			} else {
				this.showAddressWrapper = false;
				this.showAddressFields = true;
				this.showAddAddressBtn = false;
			}
		}

		if (this.ebDemande.listCategories && this.ebDemande.listCategories.length > 0) {
			if (this.listCategorie && this.listCategorie.length > 0) {
				this.listCategorie.forEach((categorie) => {
					let cat = this.ebDemande.listCategories.find(
						(it) => it.ebCategorieNum == categorie.ebCategorieNum
					);
					categorie.selectedLabel =
						cat && cat.labels && cat.labels.length > 0 ? cat.labels[0] : categorie.selectedLabel;
				});
			}
		}
		if (this.ebDemande.customFields && this.ebDemande.customFields.length > 0) {
			if (this.listFields && this.listFields.length > 0) {
				let fields: Array<IField> = this.ebDemande.customFields
					? JSON.parse(this.ebDemande.customFields)
					: new Array<IField>();
				this.listFields.forEach((field) => {
					let fd = fields.find((it) => it.name == field.name);
					field.value = fd && fd.value ? fd.value : field.value;
				});
			}
		}

		// Prevent non exist values
		if (this.ebDemande.xecCurrencyInvoice && this.ebDemande.xecCurrencyInvoice.code == null) {
			this.ebDemande.xecCurrencyInvoice = undefined;
		}
	}

	setCatgoriesAndCustomFields() {
		this.ebDemande = this.getDemande();
		this.ebDemande.listCategories = [];

		if (this.listCategorie.length > 0) {
			let i = 0;
			this.listCategorie.forEach((categorie) => {
				let _categorie: EbCategorie = new EbCategorie();

				if (i == 0) this.ebDemande.listCategoriesStr += categorie.ebCategorieNum;
				else this.ebDemande.listCategoriesStr += "," + categorie.ebCategorieNum;

				// this.getIncotermOfModeTransport();

				_categorie.constructorCopyLite(categorie);

				if (categorie.selectedLabel != null && categorie.selectedLabel.ebLabelNum != null) {
					_categorie.labels.push(categorie.selectedLabel);
					if (this.ebDemande.listLabels && this.ebDemande.listLabels.length > 0)
						this.ebDemande.listLabels += ",";
					else this.ebDemande.listLabels = "";
					this.ebDemande.listLabels += categorie.selectedLabel.ebLabelNum.toString();
				}
				i++;

				this.ebDemande.listCategories.push(_categorie);
			});
		}

		this.ebDemande.customFields = JSON.stringify(this.listFields);
	}

	/* 	fillListCurrencyDemande() {
		let data = Statique.cloneObject(this.listCurrencyDemande);
		let i, found;
		if (data.length > 0) {
			this.listCurrencyBase.forEach(function (it) {
				found = false;
				for (i = 0; i < data.length; i++) {
					if (data[i].ecCurrencyNum == it.ecCurrencyNum) {
						found = true;
						break;
					}
				}
				if (!found) {
					this.listCurrencyDemande.push(it);
				}
			}.bind(this));
		}
		else {
			this.listCurrencyBase.forEach(function (it) {
				this.listCurrencyDemande.push(it);
			}.bind(this))
		}
	} */

	initAutocomplete() {
		this.typeaheadEtabEvent
			.pipe(
				distinctUntilChanged(),
				debounceTime(200),
				switchMap((term) => {
					let criteria = new SearchCriteria();
					criteria.searchterm = term;
					criteria.roleChargeur = true;
					criteria.size = null;
					if (this.isControlTower) {
						criteria.fromCommunity = true;
					} else {
						criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
						// if(!this.userConnected.superAdmin)
						//   criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
					}

					try {
						return this.etablissementService.getListEtablisementCt(criteria);
					} catch (e) {}
				})
			)
			.subscribe(
				(data) => {
					this.cd.markForCheck();
					this.listEtablissement = data;
				},
				(err) => {
					this.listEtablissement = [];
				}
			);
		this.typeaheadUserEvent
			.pipe(
				distinctUntilChanged(),
				debounceTime(200),
				switchMap((term) => {
					let criteria = new SearchCriteria();
					criteria.searchterm = term;
					criteria.size = null;
					criteria.simpleSearch = true;
					criteria.roleChargeur = true;
					if (this.isControlTower) {
						criteria.fromCommunity = true;
					} else {
						criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
						// if(!this.userConnected.superAdmin)
						//   criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
						// if(this.selectedEtabNum)
						//   criteria.ebEtablissementNum = this.selectedEtabNum;
					}
					try {
						return this.userService.getListUser(criteria);
					} catch (e) {}
				})
			)
			.subscribe(
				(data) => {
					this.cd.markForCheck();
					this.listUserEtab = data;
				},
				(err) => {
					this.listUserEtab = [];
				}
			);
	}

	compareCategoryLabel(item1: EbLabel, item2: EbLabel) {
		if (item1 && item2) {
			return item1.ebLabelNum === item2.ebLabelNum;
		}
	}

	compareCurrency(item1: EcCurrency, item2: EcCurrency) {
		if (item1 && item2) {
			return item1.ecCurrencyNum === item2.ecCurrencyNum;
		}
	}

	compareCountry(country1: EcCountry, country2: EcCountry) {
		if (country2 !== undefined && country2 !== null) {
			return country1.ecCountryNum === country2.ecCountryNum;
		}
	}

	compareZone(zone1: EbPlZoneDTO, zone2: EbPlZoneDTO) {
		if (zone1 && zone2) {
			return zone1.ebZoneNum === zone2.ebZoneNum;
		}
	}

	compareEtablissement(etablissement1: EbEtablissement, etablissement2: EbEtablissement) {
		if (etablissement1 && etablissement2) {
			return etablissement1.ebEtablissementNum === etablissement2.ebEtablissementNum;
		}
	}

	compareOriginBroker(val1: any, val2: any) {
		if (val2 !== undefined && val2 !== null) {
			return val1 === val2;
		}
	}

	compareCostCenter(cost1: EbCostCenter, cost2: EbCostCenter) {
		if (cost2 !== undefined && cost2 !== null && cost1 !== null && cost1 !== undefined) {
			return cost1.libelle === cost2.libelle;
		}
	}

	compareIncoterms(incoterm1: number, incoterm2: number) {
		if (incoterm2 !== undefined && incoterm2 !== null) {
			return incoterm1 == incoterm2;
		}
	}

	getCountries() {
		this.pricingService.listPays().subscribe((data) => {
			this.listCountry = data;
		});
	}

	getZones() {
		this.transportationPlanService
			.listZone(new SearchCriteria())
			.subscribe((data: Array<EbPlZoneDTO>) => {
				this.listZone = data;
			});
	}

	getEntrepots() {
		this.dockManagementService.getListEntrepot().subscribe((data: Array<EbEntrepotDTO>) => {
			this.listEntrepot = data;
		});
	}

	refreshListAdresse() {
		let serviceAddress = this.compagnieService.getListAddressCompagnie(
			this.userConnected.ebCompagnie.ebCompagnieNum
		);
		if (!this.userConnected.superAdmin) {
			let criteria = new SearchCriteria();
			criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			serviceAddress = this.etablissementService.getListAdresseEtablisement(criteria);
		}

		serviceAddress.subscribe((res) => {
			this.listAdresse = res;
		});
	}

	getEstablishments() {
		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.getAll = true;
		searchCriteria.size = null;
		// searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.etablissementService
			.getListEtablisement(searchCriteria)
			.subscribe((data: Array<EbEtablissement>) => {
				this.listEtablissementPlateform = data;
			});
	}

	getCostCenters() {
		if (this.ebDemande == null || this.ebDemande.ebDemandeNum == null || this.toggleEditData) {
			if (this.userConnected.roleChargeur) {
				this.pricingService
					.listCostCenter(this.userConnected.ebEtablissement.ebEtablissementNum)
					.subscribe((data) => {
						this.listCostCenter = data;
					});
			}
		} else if (
			this.ebDemande.xEbCostCenter != null &&
			this.ebDemande.xEbCostCenter.libelle != null
		) {
			this.listCostCenter = [this.ebDemande.xEbCostCenter];
		}
	}

	handleSelectedEtabChange(etab: EbEtablissement) {
		if (etab != null) {
			this.selectedCompagnie = etab.ebCompagnie;
			this.ebDemande.xEbEtablissement = new EbEtablissement();
			this.ebDemande.xEbEtablissement.ebEtablissementNum = etab.ebEtablissementNum;
		} else {
			this.listTransporteur = new Array<EbUser>();
			this.listBrokers = new Array<EbUser>();
			this.listCategorie = new Array<EbCategorie>();
			this.listFields = new Array<IField>();
			this.listCostCenter = new Array<EbCostCenter>();
			this.listUserEtab = new Array<EbUser>();
		}
		if (
			etab == null ||
			!this.selectedUser ||
			this.selectedUser.ebEtablissement.ebEtablissementNum != etab.ebEtablissementNum
		) {
			this.selectedUserNum = null;
			this.selectedUser = null;
			this.listUserEtab = new Array<EbUser>();
		}
	}
	handleSelectedUserChange(user: EbUser) {
		if (!user) return;

		// if(!this.listEtablissement || !this.listEtablissement.length){
		let etab = new EbEtablissement();
		etab.ebEtablissementNum = user.ebEtablissement.ebEtablissementNum;
		etab.nom = user.ebEtablissement.nom;
		etab.ebCompagnie = new EbCompagnie();
		etab.ebCompagnie.ebCompagnieNum = user.ebCompagnie.ebCompagnieNum;
		etab.ebCompagnie.nom = user.ebCompagnie.nom;
		etab.ebCompagnie.code = user.ebCompagnie.code;
		this.listEtablissement = new Array<EbEtablissement>();
		this.listEtablissement.push(etab);
		this.selectedEtabNum = etab.ebEtablissementNum;
		this.selectedCompagnie = etab.ebCompagnie;
		// }

		this.ebDemande.user = new EbUser();
		this.ebDemande.user.ebUserNum = user.ebUserNum;
		this.ebDemande.user.nom = user.nom;
		this.ebDemande.user.prenom = user.prenom;
		this.ebDemande.user.ebEtablissement = new EbEtablissement();
		this.ebDemande.user.ebEtablissement.ebEtablissementNum =
			user.ebEtablissement.ebEtablissementNum;
		this.ebDemande.user.ebCompagnie = new EbCompagnie();
		if (this.selectedCompagnie.ebCompagnieNum && this.selectedCompagnie.code) {
			this.ebDemande.user.ebCompagnie.ebCompagnieNum = this.selectedCompagnie.ebCompagnieNum;
			this.ebDemande.user.ebCompagnie.code = this.selectedCompagnie.code;
		} else {
			this.ebDemande.user.ebCompagnie.ebCompagnieNum = user.ebCompagnie.ebCompagnieNum;
			this.ebDemande.user.ebCompagnie.code = user.ebCompagnie.code;
		}
		this.selectedUser = new EbUser();
		this.selectedUser.constructorCopyLite(user);
		this.selectedUserNum = user.ebUserNum;

		this.getDataPreference();
	}

	getDataPreference() {
		this.listFields = null;
		this.listCategorie = null;
		// this.listCostCenter = null;

		let criteria: SearchCriteria = new SearchCriteria();
		criteria.size = null;
		// criteria.roleBroker = true;
		// criteria.userService = ServiceType.BROKER;
		criteria.ebDemandeNum = this.ebDemande.ebDemandeNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;

		if (!this.ebDemande.ebDemandeNum || this.toggleEditData) {
			if (this.userConnected.role == UserRole.CONTROL_TOWER) {
				if (this.toggleEditData && !this.selectedCompagnie) {
					this.selectedCompagnie = new EbCompagnie();
					this.selectedCompagnie.ebCompagnieNum = this.ebDemande.xEbEtablissement.ebCompagnie.ebCompagnieNum;
					this.selectedEtabNum = this.ebDemande.xEbEtablissement.ebEtablissementNum;
					this.selectedUserNum = this.ebDemande.user.ebUserNum;
				}
				if (this.selectedCompagnie) {
					criteria.ebCompagnieNum = this.selectedCompagnie.ebCompagnieNum;
					criteria.ebEtablissementNum = this.selectedEtabNum;
					criteria.contact = true;
					criteria.ebUserNum = this.selectedUserNum;
				}
			} else if (this.userConnected.role == UserRole.CHARGEUR) {
				criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
				criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
				criteria.ebUserNum = this.userConnected.ebUserNum;
				criteria.contact = true;
			}
		} else {
			criteria.ebCompagnieNum = this.ebDemande.xEbEtablissement.ebCompagnie.ebCompagnieNum;
			criteria.ebEtablissementNum = this.ebDemande.xEbEtablissement.ebEtablissementNum;
			criteria.ebUserNum = this.ebDemande.user.ebUserNum;
			criteria.contact = true;
		}

		if (!criteria.ebEtablissementNum || !criteria.ebCompagnieNum) {
			this.listFields = new Array<IField>();
			this.listCategorie = new Array<EbCategorie>();
			this.listBrokers = new Array<EbUser>();
			this.listTransporteur = new Array<EbUser>();
			this.listCostCenter = new Array<EbCostCenter>();
			return;
		}

		this.setSuggestionsEtablissement();

		this.etablissementService.getDataPreference(criteria).subscribe(async (res) => {
			this.listTransporteur =
				res && res.listTransporteur ? res.listTransporteur : new Array<EbUser>();
			this.listBrokers = res && res.listBroker ? res.listBroker : new Array<EbUser>();
			this.listCostCenter =
				res && res.listCostCenter ? res.listCostCenter : new Array<EbCostCenter>();
			this.listCategorie = res && res.listCategorie ? res.listCategorie : new Array<EbCategorie>();
			this.listFields =
				res && res.customField && res.customField.fields
					? JSON.parse(res.customField.fields)
					: new Array<IField>();
			this.listFields = this.listFields || new Array<IField>();

			this.listSchemaPsl = res.schemasPsl;

			this.selectCategorieFromDemande();

			if (this.listCategorie && this.listCategorie.length > 0) {
				this.listCategorie.forEach((it) => {
					if (it.labels && it.labels.length > 0) {
						it.labels.sort((a, b) => {
							return a.libelle.localeCompare(b.libelle);
						});
					}
				});

				this.listCategorie = this.listCategorie.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}

			if (!this.ebDemande.ebDemandeNum) {
				await this.cloneEbDemandeWithSf(this.ebDemande, true);
			} else {
				if (this.listFields && this.listFields.length > 0) {
					let fields: Array<IField> = this.ebDemande.customFields
						? JSON.parse(this.ebDemande.customFields)
						: new Array<IField>();
					this.listFields.forEach((field) => {
						let fd = fields.find((it) => it.name == field.name);
						field.value = fd && fd.value ? fd.value : field.value;
					});
				}

				if (this.ebDemande.xEbSchemaPsl) {
					this.selectedSchema = this.ebDemande.xEbSchemaPsl;

					let schema = this.listSchemaPsl.find(
						(it) => it.ebTtSchemaPslNum == this.ebDemande.xEbSchemaPsl.ebTtSchemaPslNum
					);
					if (!schema)
						this.listSchemaPsl.push(
							Statique.cloneObject(this.ebDemande.xEbSchemaPsl, new EbTtSchemaPsl())
						);
				}
			}

			this.listSchemaPslFiltered = this.listSchemaPsl.map((it) => it);

			if (this.listTransporteur) this.onListTransporteurLoaded.emit(this.listTransporteur);
		});
	}

	selectCategorieFromDemande() {
		if (this.ebDemande.ebDemandeNum && this.ebDemande.listCategories) {
			let demCat;
			this.listCategorie.forEach((cat) => {
				demCat = this.ebDemande.listCategories.find(
					(it) => it.ebCategorieNum == cat.ebCategorieNum
				);
				if (demCat && demCat.labels && demCat.labels.length > 0) {
					cat.selectedLabel = cat.labels.find(
						(label) => label.ebLabelNum == demCat.labels[0].ebLabelNum
					);
					if (!cat.selectedLabel) {
						cat.labels.push(demCat.labels[0]);
						cat.selectedLabel = demCat.labels[0];
					}
				}
			});

			let cat;
			this.ebDemande.listCategories.forEach((demCat) => {
				cat = this.listCategorie.find((it) => it.ebCategorieNum == demCat.ebCategorieNum);
				if (!cat) this.listCategorie.push(demCat);
			});
		}
	}

	getBrokers() {
		this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
		this.searchCriteria.roleBroker = true;
		this.searchCriteria.size = null;
		this.searchCriteria.contact = true;
		this.pricingService.listBroker(this.searchCriteria).subscribe((data) => {
			this.listBrokers = data;
		});
	}

	// originCustomsBrokerChange(event) {
	//   let index = event.target.selectedIndex;
	//   this.ebDemande.xEcTypeCustomBroker = null;
	//   this.ebDemande.xEcBroker = null;
	//   if (index > 2) {
	//     this.ebDemande.xEcBroker = new EbUser();
	//     this.ebDemande.xEcBroker.constructorCopyLite(this.listBrokers[index - 4]);
	//   } else {
	//     this.ebDemande.xEcTypeCustomBroker = this.listOriginCustomsBroker[index].key;
	//   }
	// }

	selectedPartyOrigin(selected: CompleterItem) {
		if (selected === null) {
			this.ebDemande.ebPartyOrigin.ebPartyNum = null;
		} else {
			this.showAddressFields = false;
			this.showAddressWrapper = true;
			let ebParty: EbParty = selected.originalObject;
			this.ebDemande.ebPartyOrigin = ebParty;
			this.ebDemande.xEbUserOrigin = EbUser.createUser(ebParty.xEbUserVisibility);
		}
	}

	selectedPartyDest(selected: CompleterItem) {
		if (selected === null) {
			this.ebDemande.ebPartyDest.ebPartyNum = null;
		} else {
			this.DestFields = false;
			this.showDestInfoWrapper = true;
			let ebParty: EbParty = selected.originalObject;
			this.ebDemande.ebPartyDest = ebParty;
			this.ebDemande.xEbUserDest = EbUser.createUser(ebParty.xEbUserVisibility);
		}
	}

	selectedPartyNotif(selected: CompleterItem) {
		if (selected === null) {
			this.ebDemande.ebPartyNotif.ebPartyNum = null;
		} else {
			this.ebDemande.ebPartyNotif = selected.originalObject;
		}
	}

	getComment(event) {
		this.ebDemande.commentChargeur = event;
	}

	nextPage() {
		if (this.selectedPsl && this.selectedPsl.length) {
			let found = false;
			this.ebDemande.xEbSchemaPsl.listPsl.forEach((it) => {
				let fnd = !!this.selectedPsl.find((el) => it.ebTtCompanyPslNum == el);
				if (fnd) {
					it.isChecked = true;
					found = true;
				} else it.isChecked = false;
			});
			if (!found)
				this.ebDemande.xEbSchemaPsl.listPsl = Statique.getListActivePsl(
					this.ebDemande.xEbSchemaPsl
				);
		} else
			this.ebDemande.xEbSchemaPsl.listPsl.forEach((it) => {
				if (it.schemaActif) it.isChecked = true;
			});

		this.onValidate.emit();
		if (!this.listTransporteur || this.listTransporteur.length <= 0)
			this.modalService.error("Alerte", "Aucun transporteur trouvé dans la communauté");
	}

	isPageValid() {
		return !this.pageValid.nativeElement.disabled;
	}

	showBlock(): boolean {
		return this.ebDemande.ebDemandeNum == null;
	}

	ChangedModeOfTransport() {
		this.listSchemaPslFiltered = this.listSchemaPsl.filter(
			(it) =>
				(it.listModeTransport &&
					it.listModeTransport.indexOf(":" + this.ebDemande.xEcModeTransport + ":") >= 0) ||
				!it.listModeTransport ||
				!it.listModeTransport.length
		);

		if (this.listSchemaPslFiltered && this.listSchemaPslFiltered[0]) {
			this.ebDemande.xEbSchemaPsl = Statique.cloneObject(this.listSchemaPslFiltered[0]);
			this.ebDemande.xEcIncotermLibelle = this.ebDemande.xEbSchemaPsl.xEcIncotermLibelle;
			this.selectConfigPsl(this.ebDemande.xEbSchemaPsl);
		} else {
			this.ebDemande.xEcIncotermLibelle = null;
			this.listPsl = null;
			this.selectedPsl = null;
		}

		//this.selectConfigPsl(this.ebDemande.xEcModeTransport, this.ebDemande.xEcIncoterm);
	}

	//select PSL config via mode de transport et incoterm
	selectConfigPsl(schemaPsl: EbTtSchemaPsl) {
		if (schemaPsl && schemaPsl.listPsl != null) {
			this.selectedSchema = schemaPsl;
			this.ebDemande.xEcIncotermLibelle = schemaPsl.xEcIncotermLibelle;
			this.listPsl = schemaPsl.listPsl;
			this.selectedPsl = this.listPsl
				.filter((it) => it.schemaActif)
				.map((it) => it.ebTtCompanyPslNum);
		} else {
			this.listPsl = null;
			this.selectedPsl = null;
		}
	}

	saveFavori(idObject: number) {
		if (this.favoriComponentComponent.ebFavori != null) {
			this.favoriComponentComponent.idObject = idObject;
			this.favoriComponentComponent.addFavori();
		}
	}

	openSaveAdresseModel(section: string) {
		this.adresseToSaveSection = section;

		this.adresseToSave = new EbAdresse();

		var ebPartyToCopy: EbParty;
		if (this.adresseToSaveSection === "ORIGIN") {
			ebPartyToCopy = this.ebDemande.ebPartyOrigin;
		}
		if (this.adresseToSaveSection === "DESTINATION") {
			ebPartyToCopy = this.ebDemande.ebPartyDest;
		}
		if (this.adresseToSaveSection === "NOTIFY") {
			ebPartyToCopy = this.ebDemande.ebPartyNotif;
		}

		this.adresseToSave.airport = ebPartyToCopy.airport;
		this.adresseToSave.street = ebPartyToCopy.adresse;
		this.adresseToSave.country = ebPartyToCopy.xEcCountry;
		this.adresseToSave.city = ebPartyToCopy.city;
		this.adresseToSave.company = ebPartyToCopy.company;
		this.adresseToSave.email = ebPartyToCopy.email;
		this.adresseToSave.phone = ebPartyToCopy.phone;
		this.adresseToSave.zipCode = ebPartyToCopy.zipCode;
		this.adresseToSave.openingHoursFreeText = ebPartyToCopy.openingHours;
		this.adresseToSave.commentaire = ebPartyToCopy.commentaire;
		this.adresseToSave.defaultZone = ebPartyToCopy.zone;
		if (ebPartyToCopy.zone && ebPartyToCopy.zone.ebZoneNum) {
			this.adresseToSave.zones = new Array<EbPlZoneDTO>();
			this.adresseToSave.zones.push(ebPartyToCopy.zone);
		}
		this.adresseToSave.xEbUserVisibility = EbUser.createUser(ebPartyToCopy.xEbUserVisibility);

		this.adresseToSave.etablissementAdresse = new Array<EbEtablissement>();
		this.adresseToSave.etablissementAdresse.push(ebPartyToCopy.etablissementAdresse);
	}

	cancelAddAdress(event: any) {
		this.adresseToSave = null;
		this.adresseToSaveSection = "";
	}

	validateAddAdress(event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.adresseToSave.etablissementAdresse = new Array<EbEtablissement>();
		let etablissement: EbEtablissement = new EbEtablissement();
		etablissement.ebEtablissementNum = this.ebDemande.user.ebEtablissement.ebEtablissementNum;
		this.adresseToSave.etablissementAdresse.push(etablissement);

		this.adresseToSave.ebCompagnie = new EbCompagnie();
		this.adresseToSave.ebCompagnie.ebCompagnieNum = this.ebDemande.user.ebCompagnie.ebCompagnieNum;

		this.adresseToSave.createdBy = new EbUser();
		this.adresseToSave.createdBy.ebUserNum = this.userConnected.ebUserNum;

		this.etablissementService.addAdresseEtablisement(this.adresseToSave).subscribe(
			function(data) {
				requestProcessing.afterGetResponse(event);
				this.adresseToSave.ebAdresseNum = data.ebAdresseNum;

				var ebPartyToApply: EbParty;
				if (this.adresseToSaveSection === "ORIGIN") {
					ebPartyToApply = this.ebDemande.ebPartyOrigin;
				}
				if (this.adresseToSaveSection === "DESTINATION") {
					ebPartyToApply = this.ebDemande.ebPartyDest;
				}
				if (this.adresseToSaveSection === "NOTIFY") {
					ebPartyToApply = this.ebDemande.ebPartyNotif;
				}

				ebPartyToApply.adresse = this.adresseToSave.street;
				ebPartyToApply.xEcCountry = this.adresseToSave.country;
				ebPartyToApply.city = this.adresseToSave.city;
				ebPartyToApply.company = this.adresseToSave.company;
				ebPartyToApply.email = this.adresseToSave.email;
				ebPartyToApply.phone = this.adresseToSave.phone;
				ebPartyToApply.zipCode = this.adresseToSave.zipCode;
				ebPartyToApply.airport = this.adresseToSave.airport;
				ebPartyToApply.openingHours = this.adresseToSave.openingHoursFreeText;
				ebPartyToApply.commentaire = this.adresseToSave.commentaire;

				this.adresseToSaveSection = "";
				this.adresseToSave = null;
			}.bind(this)
		);
	}

	private getZoneDisplayString(zone?: EbPlZoneDTO): string {
		if (zone) {
			if (zone.designation) {
				return zone.ref + " (" + zone.designation + ")";
			} else {
				return zone.ref;
			}
		} else {
			return "";
		}
	}

	private getZoneBackupDisplayString(party?: EbParty): string {
		if (party) {
			if (party.zoneDesignation) {
				return party.zoneRef + " (" + party.zoneDesignation + ")";
			} else {
				return party.zoneRef;
			}
		} else {
			return "";
		}
	}

	private getCountryDisplayString(party?: EbParty): string {
		if (party) {
			if (party.xEcCountry) {
				return party.xEcCountry.libelle;
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	handleToggleEditData() {
		this.oldEbDemande = new EbDemande();
		this.oldEbDemande.cloneData(this.ebDemande);
		this.toggleEditData = true;
	}

	handleCancelEditData() {
		this.ebDemande = this.oldEbDemande;
		this.oldEbDemande = null;
		this.listFields = this.ebDemande.customFields
			? JSON.parse(this.ebDemande.customFields)
			: this.listFields;
		this.selectCategorieFromDemande();
		this.toggleEditData = false;
	}

	editData(event) {
		if (this.ebDemande && this.ebDemande.ebDemandeNum) {
			let requestProcessing = new RequestProcessing();
			requestProcessing.beforeSendRequest(event);

			let demande = new EbDemande(true);
			demande.ebDemandeNum = this.ebDemande.ebDemandeNum;
			demande.commentChargeur = this.ebDemande.commentChargeur;
			demande.customerReference = this.ebDemande.customerReference;
			if (this.ebDemande.xEbCostCenter && this.ebDemande.xEbCostCenter.ebCostCenterNum)
				demande.xEbCostCenter = this.ebDemande.xEbCostCenter;
			if (this.ebDemande.ebPartyOrigin && this.ebDemande.ebPartyOrigin.ebPartyNum)
				demande.ebPartyOrigin = this.ebDemande.ebPartyOrigin;
			if (this.ebDemande.ebPartyDest && this.ebDemande.ebPartyDest.ebPartyNum)
				demande.ebPartyDest = this.ebDemande.ebPartyDest;
			if (this.ebDemande.ebPartyNotif && this.ebDemande.ebPartyNotif.ebPartyNum)
				demande.ebPartyNotif = this.ebDemande.ebPartyNotif;
			demande.customFields = this.listFields ? JSON.stringify(this.listFields) : null;
			demande.insurance = this.ebDemande.insurance;
			demande.insuranceValue = this.ebDemande.insuranceValue;
			demande.xEcTypeCustomBroker = this.ebDemande.xEcTypeCustomBroker;
			// if (this.ebDemande.xEcBroker && this.ebDemande.xEcBroker.ebUserNum)
			//   demande.xEcBroker = this.ebDemande.xEcBroker;
			if (this.ebDemande.xecCurrencyInvoice && this.ebDemande.xecCurrencyInvoice.ecCurrencyNum)
				demande.xecCurrencyInvoice = this.ebDemande.xecCurrencyInvoice;

			let i = 0;
			demande.listCategories = new Array<EbCategorie>();
			demande.listCategoriesStr = "";
			demande.listLabels = "";
			this.listCategorie.forEach((categorie) => {
				let _categorie: EbCategorie = new EbCategorie();

				if (i == 0) demande.listCategoriesStr += categorie.ebCategorieNum;
				else demande.listCategoriesStr += "," + categorie.ebCategorieNum;

				_categorie.constructorCopyLite(categorie);

				if (categorie.selectedLabel != null && categorie.selectedLabel.ebLabelNum != null) {
					_categorie.labels.push(categorie.selectedLabel);
					if (demande.listLabels && demande.listLabels.length > 0) demande.listLabels += ",";
					demande.listLabels += categorie.selectedLabel.ebLabelNum.toString();
				}
				i++;
				demande.listCategories.push(_categorie);
			});
			this.ebDemande.listCategories = demande.listCategories;
			this.ebDemande.listCategoriesStr = demande.listCategoriesStr;
			this.ebDemande.listLabels = demande.listLabels;

			demande.typeData = TypeDataEbDemande.REFERENCE_INFORMATION;
			demande.module = this.ebModuleNum;

			this.pricingService.updateEbDemande(demande).subscribe(
				(res) => {
					// this.toggleEditData = false;
					if (res && res.ebChat) {
						this.updateChatEmitter.emit(res.ebChat);
					}
					this.oldEbDemande = null;
					requestProcessing.afterGetResponse(event);
				},
				(error) => {
					let title = "";
					this.translate.get("MODAL.ERROR").subscribe((res: string) => {
						title = res;
					});
					this.modalService.error(title, null, function() {});
					requestProcessing.afterGetResponse(event);
				}
			);
		}
	}

	toggleNotifyFields() {
		this.showNotifyFields = !this.showNotifyFields;
	}
	toggleCategFields() {
		this.showCategFields = !this.showCategFields;
	}
	toggleParamsFields() {
		this.showParamsFields = !this.showParamsFields;
	}
}
