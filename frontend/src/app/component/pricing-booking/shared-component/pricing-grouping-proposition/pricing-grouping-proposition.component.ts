import {
	CarrierStatus,
	GroupePropositionStatut,
	GroupeVentilation,
	NatureDemandeTransport,
	QrCreationStep,
} from "./../../../../utils/enumeration";
import { ChangeDetectorRef, Component, OnInit, ViewChild } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { DemandeStatus } from "@app/utils/enumeration";
import { EbQrGroupe } from "@app/classes/qrGroupe";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { PricingService } from "@app/services/pricing.service";
import { SearchCriteria, SearchCriteriaLite } from "@app/utils/searchCriteria";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { TypeRequestService } from "@app/services/type-request.service";
import { EbDemande } from "@app/classes/demande";
import { EbQrGroupeProposition } from "@app/classes/qrGroupeProposition";
import { StatiqueService } from "@app/services/statique.service";
import { MTEnum } from "@app/classes/mtEnum";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EcCurrency } from "@app/classes/currency";
import { ModalService } from "@app/shared/modal/modal.service";
import { IField } from "@app/classes/customField";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { DemandeCreationFormComponent } from "../creation-form/demande-creation-form.component";
import { EbParty } from "@app/classes/party";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { EbCostCenter } from "@app/classes/costCenter";
import { TranslateService } from "@ngx-translate/core";
import { QrConsolidationService } from '@app/services/qrConsolidation.service';

@Component({
	selector: "app-pricing-grouping-proposition",
	templateUrl: "./pricing-grouping-proposition.component.html",
	styleUrls: ["./pricing-grouping-proposition.component.scss"],
})
export class PricingGroupingPropositionComponent extends ConnectedUserComponent implements OnInit {
	Statique = Statique;
	GroupePropositionStatut = GroupePropositionStatut;
	GroupeVentilation = GroupeVentilation;
	QrCreationStep = QrCreationStep;

	listTypeDemandeByCompagnie: Array<EbTypeRequestDTO>;

	@ViewChild(DemandeCreationFormComponent, { static: false })
	demandeCreationForm: DemandeCreationFormComponent;

	listEbQrGroupe: Array<EbQrGroupe> = new Array<EbQrGroupe>();
	isQrGroupeLoading: boolean = false;
	isQrGroupeExist: boolean = false;
	isQrGroupePropositionExist: boolean = false;

	showModal: boolean = false;
	showModalListGroup: boolean = false;
	currentGroupe: EbQrGroupe = null;
	currentPropo: EbQrGroupeProposition = null;
	currentPropoIndex: number = null;
	currentGrpIndex: number = null;
	currentDemande: EbDemande = null;

	consolidatedPrice: number;
	modalObject: any;
	modalObjectGroupList: any;
	selectedGroupList: Array<number>;
	listVentilation: Array<any>;

	actionButtons: Array<HeaderInfos.ActionButton> = [
		{
			label: "GENERAL.REFRESH",
			icon: "fa fa-database",
			action: () => {
				this.refreshPropositions();
			},
		},
	];

	constructor(
		private pricingService: PricingService,
		private typeRequestService: TypeRequestService,
		private statiqueService: StatiqueService,
		private modalService: ModalService,
		private cd: ChangeDetectorRef,
		protected headerService: HeaderService,
		protected translate: TranslateService,
		protected qrConsolidation: QrConsolidationService

	) {
		super();
	}

	ngOnInit() {
		window["$this"] = this;
		this.loadData();

		this.headerService.registerActionButtons(this.actionButtons);

		this.modalObject = {
			type: "template-content",
			siFn: this.confirmEdit.bind(this),
			noFn: this.cancelEdit.bind(this),
		};

		this.modalObjectGroupList = {
			type: "template-content",
			siFn: this.confirmEditGroupList.bind(this),
			noFn: this.cancelEdit.bind(this),
		};

		this.getListStatiques();
	}

	getListStatiques() {
		this.statiqueService.getListGroupeVentilation().subscribe((res) => {
			this.listVentilation = res;
			if( this.listVentilation ){
				this.listVentilation.forEach(ven=>{
					if(ven.code){
						ven.libelle = this.translate.instant("GROUPING.QR.VENTILATION.VEN-"+ven.code);
					}
				})
			}
		});

	}


	refreshPropositions() {
		this.showModalListGroup = true;
	}

	confirmEditGroupList() {
		this.selectedGroupList = this.listEbQrGroupe
			.filter((it) => it.isChecked)
			.map((it) => it.ebQrGroupeNum);

		if (!this.selectedGroupList || !this.selectedGroupList.length) return;

		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		let criteria = new SearchCriteriaLite(this.userConnected.ebUserNum);
		criteria.listIdObject = this.selectedGroupList;
		this.isQrGroupeLoading = true;
		this.qrConsolidation.refreshQrGroupe(criteria).subscribe(
			(res: Array<EbQrGroupe>) => {
				if (res && res.length > 0) {
					this.setGroupData(res);
				}

				requestProcessing.afterGetResponse(event);
				this.cancelEdit();
			},
			(err) => {
				requestProcessing.afterGetResponse(event);
				this.cancelEdit();
			}
		);
	}

	loadData() {
		let criteria = new SearchCriteria();
		// criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		this.isQrGroupeLoading = true;
		this.qrConsolidation.getListQrGroupeWithDemande(criteria).subscribe((res: Array<EbQrGroupe>) => {
			this.setGroupData(res);
		});
		this.typeRequestService
			.getListTypeRequests(criteria)
			.subscribe((res: Array<EbTypeRequestDTO>) => {
				this.listTypeDemandeByCompagnie = res;
			});

	}

	getListDemandesByProp(prop: EbQrGroupeProposition, indice: number) {
		let proposition = Statique.cloneObject(prop, new EbQrGroupeProposition());
		proposition.currentPage += indice;
		proposition.listEbDemande = null;
		proposition.ebDemandeResult = null;

		this.qrConsolidation
			.getListDemandesInProposition(proposition)
			.subscribe((res: Array<EbDemande>) => {
				prop.listEbDemande = res;
				//vérification des demandes checked
				prop.listEbDemande.forEach(dem=>{
					if(prop.listSelectedDemandeNum && prop.listSelectedDemandeNum.indexOf(":"+dem.ebDemandeNum+":")>=0){
						dem.isChecked = true;
					}
				})
				this.onCheckDemande(null,prop);
				prop.currentPage += indice;
			});
	}

	setGroupData(res: Array<EbQrGroupe>) {
		this.listEbQrGroupe = res;

		this.listEbQrGroupe.forEach((it) => {

			it.listPropositions.splice(5, it.listPropositions.length);

			it.listPropositions.forEach((p: EbQrGroupeProposition, index) => {

				if (p.listEbDemande && p.listEbDemande.length) {

					p.listEbDemande.forEach((d: EbDemande) => {
						//si ebDemandeResult est null alors creation de la demande  result et initialisation des données
						if (!p.ebDemandeResult) {
							p.ebDemandeResult = new EbDemande();
							p.ebDemandeResult.ebDemandeNum = null;
							p.ebDemandeResult.refTransport = "XXXXXX";
							if (!p.ebDemandeResult.xecCurrencyInvoice) {
								p.ebDemandeResult.xecCurrencyInvoice = new EcCurrency();
							}
							p.ebDemandeResult.exEbDemandeTransporteurs = [];
							p.ebDemandeResult.listMarchandises = [];
							p.listSelectedDemandeNum = "";
						}

						//si la demande result existe
						//par deffaut les demandes sont decochés
						if (p.ebDemandeResult.ebDemandeNum && p.listSelectedDemandeNum) {
							if (p.listSelectedDemandeNum.indexOf(`:${d.ebDemandeNum}:`) >= 0) {
								d.isChecked = true;

							} else{
							 	d.isChecked = false;
							}
						 }

						if (
							p.ebDemandeResult &&
							p.ebDemandeResult.customFields &&
							!p.ebDemandeResult.listCustomsFields
						) {
							p.ebDemandeResult.listCustomsFields = JSON.parse(p.ebDemandeResult.customFields);
						}
					});

					p.ebDemandeResult = this.setDemandeCommonData(p.ebDemandeResult, p.listEbDemande);

					p.ebDemandeResult.isDemandeResult = true;

					this.onCheckDemande(null,p);
				}
			});
		});
		this.isQrGroupeLoading = false;
		this.checkIfQrGroupeVAluesExist();
	}

	checkIfQrGroupeVAluesExist() {
		this.isQrGroupePropositionExist = false;
		if (this.listEbQrGroupe.length > 0) {
			this.isQrGroupeExist = true;
		}

		for (let i = 0; i < this.listEbQrGroupe.length; i++) {
			for (let j = 0; j < this.listEbQrGroupe[i].listPropositions.length; j++) {
				if (this.listEbQrGroupe[i].listPropositions.length > 0) {
					this.isQrGroupePropositionExist = true;
					break;
				}
			}
			if (this.isQrGroupePropositionExist) break;
		}
	}

	getStatus(key: number) {
		if (!Statique.isDefined(key)) return "";
		let v = Statique.GroupePropositionStatut.find((it) => it.key == key);
		return v ? v.value : Statique.GroupePropositionStatut.find((it) => it.key == 3).value;
	}

	getDemandeStatus(xEcStatut: number) {
		if (!Statique.isDefined(xEcStatut)) return "";
		let statut = Statique.StatutDemande.find((it) => it.key == xEcStatut);
		return statut ? statut.value : Statique.StatutDemande.find((it) => it.key == 1).value;
	}

	applyCardClass(card) {
		let cls = "qrgroup-propo-card";
		if (card.isDemandeResult) {
			cls += " qr-result ";
		}
		return cls;
	}

	consolidate($event, propo: EbQrGroupeProposition) {
		let exists = !!propo.ebDemandeResult.exEbDemandeTransporteurs.filter((it) => it.isChecked)
			.length;
		if (!exists) {
			this.modalService.information("Information", "No selected quote !");
			return;
		}
		if (!propo.ebDemandeResult.listMarchandises.length) {
			this.modalService.information("Information", "List of units is empty !");
			return;
		}
		let nb = propo.listEbDemande.filter((it) => it.isChecked).length;
		if (nb < 2) {
			this.modalService.information("Information", "You should select more than one QR !");
			return;
		}

		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);

		let obj = Statique.cloneObject(propo, new EbQrGroupeProposition());
		obj.listSelectedDemandeNum = "";
		obj.listEbDemande
			.filter((it) => it.isChecked)
			.forEach((it) => {
				obj.listSelectedDemandeNum += `:${it.ebDemandeNum}:`;
			});
		obj.listEbDemande = null;

		obj.ebDemandeResult.exEbDemandeTransporteurs = obj.ebDemandeResult.exEbDemandeTransporteurs.filter(
			(it) => {
				if (it.isChecked) {
					it.exEbDemandeTransporteurNum = null;
					it.price = obj.ebDemandeResult.consolidatedPrice;
					it.status = CarrierStatus.QUOTE_SENT;
					return true;
				}
			}
		);
		obj.ebDemandeResult.listMarchandises.forEach((it) => {
			it.ebMarchandiseNum = null;
			it.ebDemande = null;
		});

		obj.ebDemandeResult.xEcNature = NatureDemandeTransport.CONSOLIDATION;
		obj.ebDemandeResult.xEcStatut = DemandeStatus.RECOMMENDATION;

		if (obj.ebDemandeResult.ebPartyOrigin != null) {
			if (
				obj.ebDemandeResult.ebPartyOrigin.xEcCountry != null &&
				obj.ebDemandeResult.ebPartyOrigin.xEcCountry.ecCountryNum == null
			)
				obj.ebDemandeResult.ebPartyOrigin.xEcCountry = null;
		}
		if (obj.ebDemandeResult.ebPartyDest != null) {
			if (
				obj.ebDemandeResult.ebPartyDest.xEcCountry != null &&
				obj.ebDemandeResult.ebPartyDest.xEcCountry.ecCountryNum == null
			)
				obj.ebDemandeResult.ebPartyDest.xEcCountry = null;
		}
		if (obj.ebDemandeResult.ebPartyNotif != null) {
			if (
				obj.ebDemandeResult.ebPartyNotif.xEcCountry != null &&
				obj.ebDemandeResult.ebPartyNotif.xEcCountry.ecCountryNum == null
			)
				obj.ebDemandeResult.ebPartyNotif.xEcCountry = null;
		}
		if (
			obj.ebDemandeResult.xEbCostCenter != null &&
			obj.ebDemandeResult.xEbCostCenter.ebCostCenterNum == null
		)
			obj.ebDemandeResult.xEbCostCenter = null;

		if (
			obj.ebDemandeResult.xEbEntrepotUnloading != null &&
			obj.ebDemandeResult.xEbEntrepotUnloading.ebEntrepotNum == null
		)
			obj.ebDemandeResult.xEbEntrepotUnloading = null;

		this.qrConsolidation.consolidatePropositionDemande(obj).subscribe(
			(res) => {
				propo.statut = GroupePropositionStatut.CONFIRMED;
				requestProcessing.afterGetResponse($event);
			},
			(err) => {
				requestProcessing.afterGetResponse($event);
			}
		);
	}
	reject($event, propo: EbQrGroupeProposition) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);

		this.qrConsolidation
			.rejectPropositionDemande(propo.ebQrGroupePropositionNum)
			.subscribe((res) => {
				propo.statut = GroupePropositionStatut.REJECTED;
				requestProcessing.afterGetResponse($event);
			});
	}
	edit($event, grpIndex: number, indexPropo: number) {
		this.currentGroupe = this.listEbQrGroupe[grpIndex];
		this.currentPropo = this.listEbQrGroupe[grpIndex].listPropositions[indexPropo];
		this.currentPropoIndex = indexPropo;
		this.currentGrpIndex = grpIndex;

		this.currentPropo.ebDemandeResult.listMarchandises = [];
		if (!this.currentPropo.ebDemandeResult.exEbDemandeTransporteurs)
			this.currentPropo.ebDemandeResult.exEbDemandeTransporteurs = [];
		this.currentPropo.listEbDemande.forEach((d) => {
			d.exEbDemandeTransporteurs &&
				d.exEbDemandeTransporteurs.forEach((tr) => {
					let qIndex = this.currentPropo.ebDemandeResult.exEbDemandeTransporteurs.findIndex(
						(it) => it.exEbDemandeTransporteurNum == tr.exEbDemandeTransporteurNum
					);
					let q = this.currentPropo.ebDemandeResult.exEbDemandeTransporteurs[qIndex];
					if (qIndex >= 0) {
						if (!q.isChecked || !Statique.isDefined(q.price)) {
							this.currentPropo.ebDemandeResult.exEbDemandeTransporteurs.splice(qIndex, 1);
							this.currentPropo.ebDemandeResult.exEbDemandeTransporteurs.push(
								Statique.cloneObject(tr, new ExEbDemandeTransporteur())
							);
						}
					} else {
						this.currentPropo.ebDemandeResult.exEbDemandeTransporteurs.push(
							Statique.cloneObject(tr, new ExEbDemandeTransporteur())
						);
					}
				});
		});

		let selectedQuote = this.onCheckDemande(null,this.currentPropo);

		if (!selectedQuote) {
			this.modalService.information("Information", "No selected quote !");
			return;
		}

		this.showModal = true;
	}

	confirmEdit() {
		let _ebDemande = this.demandeCreationForm.demande;
		if (this.currentPropo && this.currentPropo.statut != GroupePropositionStatut.CONFIRMED) {
			this.listEbQrGroupe[this.currentGrpIndex].listPropositions[
				this.currentPropoIndex
			].ebDemandeResult = _ebDemande;
			this.listEbQrGroupe[this.currentGrpIndex].listPropositions[
				this.currentPropoIndex
			].listEbDemande[0] = _ebDemande;
		}

		this.cancelEdit();
	}
	cancelEdit() {
		this.showModal = false;
		this.currentGroupe = null;
		this.currentPropo = null;
		this.currentPropoIndex = null;
		this.currentGrpIndex = null;
		this.currentDemande = null;

		this.showModalListGroup = false;
		this.selectedGroupList = null;
	}

	onCheckDemande(demande:EbDemande,propo: EbQrGroupeProposition) {

		if(demande){
			if(demande.isChecked){
				propo.listSelectedDemandeNum +=":"+demande.ebDemandeNum+":"
			}else{
				propo.listSelectedDemandeNum = propo.listSelectedDemandeNum.split(":"+demande.ebDemandeNum+":").join("")
			}
		}
		if(propo.ebDemandeResult.listMarchandises == null)
			propo.ebDemandeResult.listMarchandises = [];

		if (!propo.ebDemandeResult.exEbDemandeTransporteurs)
			propo.ebDemandeResult.exEbDemandeTransporteurs = [];

		let arr = [];
		// enlever les transporteurs de la resultante parmis ceux non existant dans les QR
		//sélectionnés (pour qu'on ne perde pas les transporteurs déjà sélectionnés)
		propo.listEbDemande
			.filter((it) => !!it.ebDemandeNum)
			.forEach((it) => (arr = arr.concat(it.exEbDemandeTransporteurs)));

		propo.ebDemandeResult.exEbDemandeTransporteurs = propo.ebDemandeResult.exEbDemandeTransporteurs.filter(
			(q) => {
				return !!arr.find((qr) => qr.exEbDemandeTransporteurNum == q.exEbDemandeTransporteurNum);
			}
		);

		arr = propo.listEbDemande
			.filter((d) => d.isChecked)
			.map((d) => {
				// ajouter les transporteurs non existants dans la QR resultante mais existants
				propo.ebDemandeResult.exEbDemandeTransporteurs = propo.ebDemandeResult.exEbDemandeTransporteurs.concat(
					d.exEbDemandeTransporteurs.filter((q) => {
						return !propo.ebDemandeResult.exEbDemandeTransporteurs.find(
							(qr) => qr.exEbDemandeTransporteurNum == q.exEbDemandeTransporteurNum
						);
					})
				);
				//ajouter les marchandises des demandes sélectionnés
				if (d.listMarchandises != null && d.listMarchandises.length > 0) {
					propo.ebDemandeResult.listMarchandises = propo.ebDemandeResult.listMarchandises.concat(
						d.listMarchandises
					);
				}

				this.currentDemande = Statique.cloneObject(propo.ebDemandeResult, new EbDemande());

				return true;
			});

			if (propo.ebDemandeResult.listMarchandises != null && propo.ebDemandeResult.listMarchandises.length > 0) {
				let totalNbrParcel = 0,totalTaxableWeight=0,totalVolume = 0,totalWeight = 0;
				let listMarchandisesFinal = [] ;
				propo.ebDemandeResult.listMarchandises.forEach(elem=>{
					let exist = listMarchandisesFinal.find(m=>m.ebMarchandiseNum == elem.ebMarchandiseNum )
					if(elem.ebDemande && elem.ebDemande.ebDemandeNum &&propo.listSelectedDemandeNum.indexOf(":"+elem.ebDemande.ebDemandeNum +":")>=0 && !exist ){
						listMarchandisesFinal.push(elem);
						totalNbrParcel += elem.numberOfUnits;
						totalTaxableWeight  += elem.weight;
						totalVolume += elem.volume;
						totalWeight += elem.weight;
					}
				});
				propo.ebDemandeResult.listMarchandises = listMarchandisesFinal;
				propo.ebDemandeResult.totalNbrParcel = totalNbrParcel;
				propo.ebDemandeResult.totalTaxableWeight = totalTaxableWeight;
				propo.ebDemandeResult.totalVolume = totalVolume;
				propo.ebDemandeResult.totalWeight = totalWeight;
			}
		return !!arr.length;
	}

	getGainDemandeQr(grp: EbQrGroupe, propo: EbQrGroupeProposition, ebDemande: EbDemande): string {
		let result = "",
			priceTFApresConsolidation = 0,
			gain = 0,
			totalNb = 0,
			curNb = 0, //totalWeigth or totalUnit
			totalTf = 0;


		propo.listEbDemande
			.filter((d) => d.isChecked)
			.forEach((d) => {
				totalTf++;
				if (grp.ventilation == GroupeVentilation.AU_PRORATA_DU_POIDS_TAXABLE) {
					if (d.totalTaxableWeight) {
						totalNb += d.totalTaxableWeight;
						if (d.ebDemandeNum == ebDemande.ebDemandeNum) curNb = d.totalTaxableWeight;
					}
				} else if (grp.ventilation == GroupeVentilation.AU_PRORATA_DES_QUANTITES) {
					if (d.totalNbrParcel) {
						totalNb += d.totalNbrParcel;
						if (d.ebDemandeNum == ebDemande.ebDemandeNum) curNb = d.totalNbrParcel;
					}
				}
			});

		let resultatPriceConsolidation = propo.ebDemandeResult.ventilationPrice;
		if(grp.ventilation == GroupeVentilation.EQUITABLEMENT_PAR_DOSSIER_DE_TRANSPORT){
			curNb = 1;
			totalNb = totalTf;
		}

		if (resultatPriceConsolidation && totalNb && curNb && grp.ventilation != GroupeVentilation.SANS_PONDERATION) {
			priceTFApresConsolidation = +((resultatPriceConsolidation * curNb) / totalNb).toFixed(2);
			gain = Math.floor((priceTFApresConsolidation / resultatPriceConsolidation) * 100);

			result = priceTFApresConsolidation + "";

			if (propo.ebDemandeResult.xecCurrencyInvoice)
				result += " " + propo.ebDemandeResult.xecCurrencyInvoice.code;

			result += " ( " + gain + "% )";

			return result;
		}

		return "-";
	}

	/*
  ********TEMPORARY FOR POC AIRBUS********
  - Catégorie Shipping Type (n° de Leg);
  - Champ paramétrable Order Number (EO/RO) ;
  - Type of request (à renommer en Service Level)
  */
	CATEGORIES = ["Shipping Type"];
	CUSTOM_FIELDS = ["Order Number (EO/RO)"];
	hasAirbusCategorie(ebDemande: EbDemande) {
		if (ebDemande.listCategories && ebDemande.listCategories.length) {
			let found = !!ebDemande.listCategories.find((it) => this.CATEGORIES.indexOf(it.libelle) >= 0);
			if (found) return true;
		}
	}
	hasAirbusCParam(ebDemande: EbDemande) {
		if (ebDemande.listCustomsFields && ebDemande.listCustomsFields.length) {
			let found = !!ebDemande.listCustomsFields.find(
				(it: IField) => this.CUSTOM_FIELDS.indexOf(it.label) >= 0
			);
			if (found) return true;
		}
	}

	setDemandeCommonData(d: EbDemande, listDemande: Array<EbDemande>) {
		let exists = true;
		let el: any = null;
		// party origin
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.ebPartyOrigin && it.ebPartyOrigin.city == el.city)) {
					el = it.ebPartyOrigin;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.ebPartyOrigin = Statique.cloneObject(el, new EbParty());
		}

		// party dest
		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.ebPartyDest && it.ebPartyDest.city == el.city)) {
					el = it.ebPartyDest;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.ebPartyDest = Statique.cloneObject(el, new EbParty());
		}

		// party notif
		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.ebPartyNotif && it.ebPartyNotif.city == el.city)) {
					el = it.ebPartyNotif;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.ebPartyNotif = Statique.cloneObject(el, new EbParty());
		}

		if (d.ebPartyOrigin) d.ebPartyOrigin.ebPartyNum = null;
		if (d.ebPartyDest) d.ebPartyDest.ebPartyNum = null;
		if (d.ebPartyNotif) d.ebPartyNotif.ebPartyNum = null;

		// mode transport
		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || it.xEcModeTransport) {
					el = it.xEcModeTransport;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.xEcModeTransport = el;
		}

		// schema
		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.xEbSchemaPsl && it.xEbSchemaPsl.ebTtSchemaPslNum == el.ebTtSchemaPslNum)) {
					el = it.xEbSchemaPsl;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.xEbSchemaPsl = Statique.cloneObject(el, new EbTtSchemaPsl());
		}

		// consolidation
		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.xEbTypeTransport && it.xEbTypeTransport == el)) {
					el = it.xEbTypeTransport;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.xEbTypeTransport = el;
		}

		// date of goods availablity
		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.dateOfGoodsAvailability && it.dateOfGoodsAvailability == el)) {
					el = it.dateOfGoodsAvailability;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.dateOfGoodsAvailability = el;
		}

		// date arrival

		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.dateOfArrival && it.dateOfArrival == el)) {
					el = it.dateOfArrival;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.dateOfArrival = el;
		}

		// costumer reference
		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.customerReference && it.customerReference == el)) {
					el = it.customerReference;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.customerReference = el;
		}

		// cost center
		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.xEbCostCenter && it.xEbCostCenter.ebCostCenterNum == el)) {
					el = it.xEbCostCenter;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.xEbCostCenter = Statique.cloneObject(el, new EbCostCenter());
		}

		// service level
		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.xEbTypeRequest && it.xEbTypeRequest == el)) {
					el = it.xEbTypeRequest;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.xEbTypeRequest = el;
		}

		// currency
		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.xecCurrencyInvoice && it.xecCurrencyInvoice.ecCurrencyNum == el)) {
					el = it.xecCurrencyInvoice;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.xecCurrencyInvoice = Statique.cloneObject(el, new EcCurrency());
		}

		// currency
		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.insurance && it.insurance == el)) {
					el = it.insurance;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.insurance = el;
		}

		// inssurance
		exists = true;
		el = null;
		exists = !listDemande.find((it) => {
			if (exists) {
				if (!el || (it.insuranceValue && it.insuranceValue == el)) {
					el = it.insuranceValue;
				} else {
					el = null;
					return true;
				}
			}
		});
		if (exists) {
			d.insuranceValue = el;
		}

		// categories ?

		return d;
	}
	
}
