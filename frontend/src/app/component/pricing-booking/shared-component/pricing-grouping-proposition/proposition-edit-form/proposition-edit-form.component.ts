import { DOCUMENT } from "@angular/common";
import { ChangeDetectorRef, Component, Inject, Input, OnInit, ViewChild } from "@angular/core";
import { EcCountry } from "@app/classes/country";
import { Statique } from "@app/utils/statique";
import { EbDemande } from "@app/classes/demande";
import { PricingService } from "@app/services/pricing.service";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { DialogService } from "@app/services/dialog.service";
import { UserService } from "@app/services/user.service";
import { EbCategorie } from "@app/classes/categorie";
import { EbUser } from "@app/classes/user";
import { ShippingInformationComponent } from "../../shipping-information/shipping-information.component";
import { GoodsInformationComponent } from "../../goods-information/goods-information.component";
import { DocumentsComponent } from "@app/shared/documents/documents.component";
import { FichierJoint } from "@app/classes/fichiersJoint";
import { CarrierChoiceComponent } from "../../carrier-choice/carrier-choice.component";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { ModalService } from "@app/shared/modal/modal.service";
import { Router } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { AccessRights, Modules } from "@app/utils/enumeration";
import { StatiqueService } from "@app/services/statique.service";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import SingletonStatique from "@app/utils/SingletonStatique";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";

@Component({
	selector: "app-proposition-edit-form",
	templateUrl: "./proposition-edit-form.component.html",
	styleUrls: ["./proposition-edit-form.component.scss"],
})
export class PropositionEditFormComponent implements OnInit {
	@Input()
	ebDemande: EbDemande;
	@Input()
	userConnected: EbUser;

	country: EcCountry;
	@ViewChild("shippingInformationComponent", { static: false })
	shippingInformationComponent: ShippingInformationComponent;
	@ViewChild("goodsInformationDiv", { static: false })
	goodsInformationComponent: GoodsInformationComponent;
	@ViewChild("carrierChoiceDiv", { static: false })
	carrierChoiceComponent: CarrierChoiceComponent;
	@ViewChild(DocumentsComponent, { static: false })
	documentsComponent: DocumentsComponent;

	Statique = Statique;
	Modules = Modules;

	filesReturnToParent: Array<Array<FichierJoint>> = new Array<Array<FichierJoint>>();
	shippingInformationFormValid: boolean = false;
	goodsPanel: boolean = false;
	goodsFormValid: boolean = false;
	quotationPanel: boolean = false;
	quotationFormValid: boolean = false;
	documentsPanel: boolean = false;
	validatePanel: boolean = false;
	btnValidate: boolean = false;
	hasContributionAccess: boolean = false;
	listTransporteur: Array<EbUser> = null;
	newEbDemandeNum: number = null;
	listTransporteurFromPlanToAdd: Array<EbUser> = [];
	showAddressWrapper: boolean = false;

	modeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();

	constructor(
		protected pricingService: PricingService,
		protected sService: SavedFormsService,
		protected dialogService: DialogService,
		protected modalService: ModalService,
		protected router: Router,
		protected userService: UserService,
		@Inject(DOCUMENT) protected document: any,
		protected translate: TranslateService,
		protected statiqueService: StatiqueService,
		protected cdRef: ChangeDetectorRef
	) {}
	ngAfterViewChecked() {
		this.cdRef.detectChanges();
	}
	ngOnInit() {
		// this.ebDemande = new EbDemande();

		// contribution access rights to module
		if (this.userConnected.listAccessRights && this.userConnected.listAccessRights.length > 0) {
			this.hasContributionAccess =
				this.userConnected.listAccessRights.find((access) => {
					return (
						access.ecModuleNum == Modules.PRICING && access.accessRight == AccessRights.CONTRIBUTION
					);
				}) != undefined;
		} else {
			this.hasContributionAccess = false;
		}

		this.pricingService.getNextEbDemandeNum().subscribe((ebDemandeNum: number) => {
			this.newEbDemandeNum = ebDemandeNum;
		});

		this.getStatiques();
	}

	async getStatiques() {
		this.modeTransport = await SingletonStatique.getListModeTransport();
	}

	protected prepareEbDemandeForSave(): EbDemande {
		let _ebDemande: EbDemande = this.ebDemande.clone();
		let i: number = 0;
		_ebDemande.ebPartyOrigin = this.shippingInformationComponent.ebPartyOrigin;
		_ebDemande.ebPartyDest = this.shippingInformationComponent.ebPartyDest;
		if (
			_ebDemande.ebPartyDest &&
			(!_ebDemande.ebPartyDest.xEbUserVisibility ||
				!_ebDemande.ebPartyDest.xEbUserVisibility.ebUserNum)
		)
			_ebDemande.ebPartyDest.xEbUserVisibility = null;
		if (
			_ebDemande.ebPartyDest &&
			(!_ebDemande.ebPartyDest.etablissementAdresse ||
				!_ebDemande.ebPartyDest.etablissementAdresse.ebEtablissementNum)
		)
			_ebDemande.ebPartyDest.etablissementAdresse = null;
		if (
			_ebDemande.ebPartyNotif &&
			(!_ebDemande.ebPartyNotif.xEbUserVisibility ||
				!_ebDemande.ebPartyNotif.xEbUserVisibility.ebUserNum)
		)
			_ebDemande.ebPartyNotif.xEbUserVisibility = null;
		if (
			_ebDemande.ebPartyNotif &&
			(!_ebDemande.ebPartyNotif.etablissementAdresse ||
				!_ebDemande.ebPartyNotif.etablissementAdresse.ebEtablissementNum)
		)
			_ebDemande.ebPartyNotif.etablissementAdresse = null;
		if (
			_ebDemande.ebPartyOrigin &&
			(!_ebDemande.ebPartyOrigin.xEbUserVisibility ||
				!_ebDemande.ebPartyOrigin.xEbUserVisibility.ebUserNum)
		)
			_ebDemande.ebPartyOrigin.xEbUserVisibility = null;
		if (
			_ebDemande.ebPartyOrigin &&
			(!_ebDemande.ebPartyOrigin.etablissementAdresse ||
				!_ebDemande.ebPartyOrigin.etablissementAdresse.ebEtablissementNum)
		)
			_ebDemande.ebPartyOrigin.etablissementAdresse = null;

		_ebDemande.listMarchandises = this.goodsInformationComponent.listMarchandise;

		if (this.documentsComponent) {
			_ebDemande.ebDemandeFichierJointNum = this.documentsComponent.listEbDemandeFichierJointNum;
		}
		this.carrierChoiceComponent.emails.forEach((email) => {
			if (email[i] != null && email[i].trim() != "") {
				if (_ebDemande.listEmailSendQuotation == "") _ebDemande.listEmailSendQuotation += email[i];
				else _ebDemande.listEmailSendQuotation += ";" + email[i];
			}
			i++;
		});

		_ebDemande.listCategories = new Array<EbCategorie>();
		_ebDemande.listCategoriesStr = "";
		_ebDemande.listLabels = "";
		if (this.shippingInformationComponent.listCategorie.length > 0) {
			i = 0;
			this.shippingInformationComponent.listCategorie.forEach((categorie) => {
				let _categorie: EbCategorie = new EbCategorie();

				if (i == 0) _ebDemande.listCategoriesStr += categorie.ebCategorieNum;
				else _ebDemande.listCategoriesStr += "," + categorie.ebCategorieNum;

				_categorie.constructorCopyLite(categorie);

				if (categorie.selectedLabel != null && categorie.selectedLabel.ebLabelNum != null) {
					_categorie.labels.push(categorie.selectedLabel);
					if (_ebDemande.listLabels && _ebDemande.listLabels.length > 0)
						_ebDemande.listLabels += ",";
					_ebDemande.listLabels += categorie.selectedLabel.ebLabelNum.toString();
				}
				i++;

				_ebDemande.listCategories.push(_categorie);
			});
		}

		_ebDemande.customFields = JSON.stringify(this.shippingInformationComponent.listFields);

		// _ebDemande = this.externalUsersComponent.getDemandeWithExternalUsers(_ebDemande);

		_ebDemande.ebDemandeNum = this.newEbDemandeNum;

		// remove duplicates
		let arr = new Array<ExEbDemandeTransporteur>();
		_ebDemande.exEbDemandeTransporteurs.forEach((it) => {
			if (!arr.find((el) => el.xTransporteur.ebUserNum == it.xTransporteur.ebUserNum)) arr.push(it);
		});
		_ebDemande.exEbDemandeTransporteurs = arr;

		return _ebDemande;
	}

	addDemande(event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		let _ebDemande = this.prepareEbDemandeForSave();

		this.pricingService.ajouterEbDemande(_ebDemande, Modules.PRICING).subscribe(
			(ebDemande: EbDemande) => {
				this.shippingInformationComponent.saveFavori(ebDemande.ebDemandeNum);
				requestProcessing.afterGetResponse(event);
				this.router.navigate(["/app/pricing/dashboard"]);
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				let title = null;
				let body = null;
				this.translate.get("MODAL.ERROR").subscribe((res: string) => {
					title = res;
				});
				// this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
				this.modalService.error(title, null, function() {});
			}
		);
	}

	containsFromPlan(trans, list) {
		for (let e of list) {
			if (
				e.selected == true &&
				e.ebUserNum == trans.ebUserNum &&
				e.isFromTransPlan &&
				e.calculatedPrice == trans.calculatedPrice &&
				e.transitTime == trans.transitTime
			) {
				return true;
			}
		}
		return false;
	}

	onListTransporteurLoadedHandler(listTransporteur) {
		this.listTransporteur = listTransporteur;
	}

	showGoodsPanel() {
		// Before doing backup operations to prevent item for change in back office
		if (this.ebDemande.ebPartyOrigin.zone && this.ebDemande.ebPartyOrigin.zone.ebZoneNum) {
			var zoneOrigin = this.findZoneByNum(this.ebDemande.ebPartyOrigin.zone.ebZoneNum);
			this.ebDemande.ebPartyOrigin.zoneNum = zoneOrigin.ebZoneNum;
			this.ebDemande.ebPartyOrigin.zoneDesignation = zoneOrigin.designation;
			this.ebDemande.ebPartyOrigin.zoneRef = zoneOrigin.ref;
		} else this.ebDemande.ebPartyOrigin.zone = null;
		if (this.ebDemande.ebPartyDest.zone && this.ebDemande.ebPartyDest.zone.ebZoneNum) {
			var zoneDest = this.findZoneByNum(this.ebDemande.ebPartyDest.zone.ebZoneNum);
			this.ebDemande.ebPartyDest.zoneNum = zoneDest.ebZoneNum;
			this.ebDemande.ebPartyDest.zoneDesignation = zoneDest.designation;
			this.ebDemande.ebPartyDest.zoneRef = zoneDest.ref;
		} else this.ebDemande.ebPartyDest.zone = null;
		if (this.ebDemande.ebPartyNotif.zone && this.ebDemande.ebPartyNotif.zone.ebZoneNum) {
			var zoneNotify = this.findZoneByNum(this.ebDemande.ebPartyNotif.zone.ebZoneNum);
			this.ebDemande.ebPartyNotif.zoneNum = zoneNotify.ebZoneNum;
			this.ebDemande.ebPartyNotif.zoneDesignation = zoneNotify.designation;
			this.ebDemande.ebPartyNotif.zoneRef = zoneNotify.ref;
		} else this.ebDemande.ebPartyNotif.zone = null;

		// Do normal stuff
		this.goodsPanel = true;
		// Commented as this view is not used anymore and the dependency has been removed
		/*
		let $this = this;
		var timeOut;
		timeOut = setTimeout(function() {
			let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(
				$this.document,
				"#goodsInformationDiv"
			);
			$this.pageScrollService.start(pageScrollInstance);
			clearTimeout(timeOut);
		}, 2);
		*/
	}

	showQuotationPanel() {
		console.log(this.goodsInformationComponent.listMarchandise);
		this.quotationPanel = true;
		// Commented as this view is not used anymore and the dependency has been removed
		/*
		let $this = this;
		var timeOut;
		timeOut = setTimeout(function() {
			let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(
				$this.document,
				"#carrierChoiceDiv"
			);
			$this.pageScrollService.start(pageScrollInstance);
			clearTimeout(timeOut);
		}, 2);
		*/
	}

	showDocumentsPanel() {
		this.documentsPanel = true;
		this.validatePanel = true;
		// Commented as this view is not used anymore and the dependency has been removed
		/*
		let $this = this;
		var timeOut;
		timeOut = setTimeout(function() {
			let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(
				$this.document,
				"#documentsDiv"
			);
			$this.pageScrollService.start(pageScrollInstance);
			clearTimeout(timeOut);
		}, 2);
		*/
	}

	isPageValid() {
		return (
			this.shippingInformationComponent &&
			this.shippingInformationComponent.isPageValid() &&
			this.goodsInformationComponent &&
			this.goodsInformationComponent.isPageValid() &&
			this.carrierChoiceComponent &&
			this.carrierChoiceComponent.isPageValid()
		);
	}

	findZoneByNum(zoneNum: number): EbPlZoneDTO {
		var rZone;
		this.shippingInformationComponent.listZone.forEach((z) => {
			if (z.ebZoneNum === zoneNum) rZone = z;
		});
		return rZone;
	}
}
