import { Component, OnInit, ChangeDetectorRef, Output, EventEmitter, Input } from "@angular/core";
import { PricingMaskFillerComponent } from "@app/component/saved-forms/pricing-mask/pricing-mask-filler.component";
import { PricingService } from "@app/services/pricing.service";
import { CompleterService, CompleterItem } from "ng2-completer";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { DialogService } from "@app/services/dialog.service";
import { DockManagementService } from "@app/services/dock-management.service";
import { UserService } from "@app/services/user.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { StatiqueService } from "@app/services/statique.service";
import { ActivatedRoute } from "@angular/router";
import { CollectionService } from "@app/services/collection.service";
import { TranslateService } from "@ngx-translate/core";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbEntrepotDTO } from "@app/classes/dock/EbEntrepotDTO";
import { EbDemande } from "@app/classes/demande";
import { EbUser } from "@app/classes/user";

@Component({
	selector: "entrepot-with-vignette",
	templateUrl: "./entrepot-with-vignette.component.html",
	styleUrls: ["./entrepot-with-vignette.component.scss"],
})
export class EntrepotWithVignetteComponent extends PricingMaskFillerComponent implements OnInit {
	searchCriteria: SearchCriteria = new SearchCriteria();
	@Input()
	ebEntrepot: EbEntrepotDTO; // peut etre les differentes EbEntrepot a envoyé depuis le composant parent

	@Input()
	showWarehouseWrapper: boolean;

	@Input()
	showAddressFields: boolean = false;

	@Output()
	onValidate = new EventEmitter<any>(); //Pour envoyer EbEntrepot au composant parent

	@Output()
	showWarehousePopup = new EventEmitter();

	@Output()
	suggestionsWarehouse: EventEmitter<Object> = new EventEmitter<Object>();

	@Output()
	removeWarehouseEmit: EventEmitter<Object> = new EventEmitter<Object>();

	constructor(
		private pricingService: PricingService,
		private completerService: CompleterService,
		protected savedFormsService: SavedFormsService,
		protected dialogService: DialogService,
		protected dockManagementService: DockManagementService,
		private userService: UserService,
		protected modalService: ModalService,
		protected cd: ChangeDetectorRef,
		protected statiqueService: StatiqueService,
		protected activatedRoute: ActivatedRoute,
		private collectionService: CollectionService,
		protected translate: TranslateService
	) {
		super(savedFormsService, dialogService, modalService, translate);
	}
	ngOnInit() {}

	/* Cette fontion est appelée lors de la selection de l'adresse , elle envoie l'adresse choisie au composant
  parent et change le bloc en mode consultation*/
	selectedEbParty(selected: CompleterItem) {
		if (selected === null) {
			this.ebEntrepot.ebEntrepotNum = null;
		} else {
			this.ebEntrepot = selected.originalObject;
			this.onValidate.emit(this.ebEntrepot);
			this.showAddressWrapper = true;
			this.showAddAddressBtn = false;
		}
	}

	/* Cette fonction verifie si le champ city et reference de EbEntrepot est renseigné et envoie dans ce cas l'entrepot
  choisie au composant parent*/
	onValideFields() {
		if (this.ebEntrepot.reference != null) this.onValidate.emit(this.ebEntrepot);
	}

	showWarehousePopupEmit(showWarehousePopup, fromOrTo) {
		this.showAddressFields = false;
		this.showWarehousePopup.emit({ showWarehousePopup, fromOrTo });
	}
	suggestionsWarehouseEmit(data, fromOrTo: string) {
		this.suggestionsWarehouse.emit({ data, fromOrTo });
	}
	//recherche ebEntrepot avec auto completion
	setSuggestionsWarehouse(value, fromOrTo) {
		if (this.ebDemande != null) {
			if (this.ebDemande.user != null) {
				this.searchCriteria.ebCompagnieNum = this.ebDemande.user.ebCompagnie.ebCompagnieNum;
				this.searchCriteria.ebEtablissementNum = this.ebDemande.user.ebEtablissement.ebEtablissementNum;
			} else {
				this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
				this.searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			}
		}

		this.searchCriteria.searchterm = value;
		this.dockManagementService.getListEntrepotTable(this.searchCriteria).subscribe((data) => {
			this.suggestionsWarehouseEmit(data, fromOrTo);
		});
	}
	removeWarehouse(fromOrTo) {
		this.showWarehouseWrapper = false;
		this.showAddAddressBtn = true;
		this.ebDemande.xEbEntrepotUnloading = new EbEntrepotDTO();

		this.removeWarehouseEmit.emit({});
	}
}
