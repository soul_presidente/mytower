import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { Modules, SavedFormIdentifier } from "@app/utils/enumeration";
import { IField } from "@app/classes/customField";
import { SearchField } from "@app/classes/searchField";
import { EbCategorie } from "@app/classes/categorie";
import { EbLabel } from "@app/classes/label";
import { AdvancedFormSearchComponent } from "@app/shared/advanced-form-search/advanced-form-search.component";
import { StatiqueService } from "@app/services/statique.service";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-pricing-booking-search",
	templateUrl: "./pricing-booking-search.component.html",
	styleUrls: ["./pricing-booking-search.component.css"],
})
export class PricingBookingSearchComponent implements OnInit {
	Statique = Statique;
	@Input()
	module: number;
	@Input()
	initialListFields: any = [];
	@Input()
	initFields?: string = "json"; // undefined | all | json
	@Output()
	onSearch = new EventEmitter<SearchCriteriaPricingBooking>();
	@Input()
	listFields: Array<IField>;
	@Input()
	listCategorie: Array<EbCategorie>;

	@ViewChild("advancedFormSearchComponentPricing", { static: false })
	advancedFormSearchComponentPricing: AdvancedFormSearchComponent;
	@ViewChild("advancedFormSearchComponentBooking", { static: false })
	advancedFormSearchComponentBooking: AdvancedFormSearchComponent;
	public listSelectedFields: Array<SearchField>;
	SavedFormIdentifier = SavedFormIdentifier;
	moduleName = Modules;
	advancedOption: SearchCriteriaPricingBooking = new SearchCriteriaPricingBooking();
	paramFields: Array<SearchField> = new Array<SearchField>();
	initParamFields: Array<SearchField> = new Array<SearchField>();
	listAllCategorie: Array<EbLabel> = new Array<EbLabel>();
	@Input()
	selectedField: any;
	@Input()
	dataInfosItem: Array<any> = [];
	@ViewChild(AdvancedFormSearchComponent, { static: false })
	advancedFormSearch: AdvancedFormSearchComponent;

	constructor(protected statiqueService: StatiqueService, private translate?: TranslateService) {}

	ngOnInit() {
		this.onInputChange(this.selectedField);
		this.initialListFields = [];
		if (this.initialListFields && typeof this.initFields !== "undefined") {
			if (this.initFields === "all")
				this.listSelectedFields = Statique.cloneObject(this.initialListFields);
			else {
				this.listSelectedFields = this.paramFields;
			}
		}
	}

	onClickInput(initParam: boolean = false) {
		if (this.module == this.moduleName.PRICING) {
			this.initParamFields = require("../../../../../assets/ressources/jsonfiles/pricing-searchfields.json");
			//cacher le field document status selon le jira MTGR-374 (sous tache)
			this.initParamFields = this.initParamFields.filter((field)=> !field.hidden );
		}
		if (this.module == this.moduleName.TRANSPORT_MANAGEMENT) {
			this.initParamFields = require("../../../../../assets/ressources/jsonfiles/booking-searchfields.json");
			//cacher le field doc status et Transport infos status selon le jira MTGR-374
			this.initParamFields = this.initParamFields.filter((field)=> !field.hidden );
		}
		this.initParamFields.forEach((f) =>
			this.translate.get(f.label).subscribe((res) => (f.label = res))
		);

		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));

		if (this.listFields && this.listFields.length > 0) {
			let arr = [];
			this.listFields.forEach((it) => {
				arr.push({
					label: it.label,
					name: it.name,
					type: "text",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
			if (initParam) {
				this.initParamFields = this.initParamFields.concat(arr);
			}
		}
		if (this.listCategorie && this.listCategorie.length > 0) {
			let arr = [];
			let i = 0;
			this.listAllCategorie = [];
			this.listCategorie.forEach((it, i) => {
				arr.push({
					label: it.libelle,
					type: "autocomplete",
					name: "categoryField" + i,
					listItems: it.labels,
					fetchAttribute: "libelle",
					fetchValue: "ebLabelNum",
					multiple: "true",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
			if (initParam) {
				this.initParamFields = this.initParamFields.concat(arr);
			}
		}

		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});
	}
	onInputChange(data: any) {
		this.onClickInput(true);
		if (data === null || data === undefined) {
			this.initParamFields = this.initParamFields.filter(
				(field) =>
					field.name === "listIncoterm" ||
					field.name === "listDestinations" ||
					field.name === "listOrigins" ||
					field.name === "originCity" ||
					field.name === "destinationCity"
			);
			this.paramFields = Array.from(new Set([].concat(this.initParamFields)));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
		} else if (data !== null && !this.paramFields.find((field) => field.name == data)) {
			let result = JSON.stringify(this.advancedFormSearch.getSearchInput());
			let parsedValues: Array<SearchField> = JSON.parse(result);
			this.paramFields.forEach((field) => {
				if (parsedValues[field.name] !== null && parsedValues[field.name] !== undefined) {
					field.default = parsedValues[field.name];
				}
			});
			this.initParamFields = this.initParamFields.filter(
				(field) => field.name === data && field.default !== null
			);
			this.paramFields = this.paramFields.concat(this.initParamFields);
			this.paramFields = Array.from(new Set(this.paramFields));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
			this.selectedField = {};
		}
	}

	search(advancedOption: SearchCriteriaPricingBooking) {
		this.onSearch.emit(advancedOption);
	}
}
