import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import {
	ChangeDetectorRef,
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ViewChild,
} from "@angular/core";
import { EbDemande } from "@app/classes/demande";
import { EbUser } from "@app/classes/user";
import {
	CarrierStatus,
	DemandeStatus,
	IDChatComponent,
	ModeTransport,
	Modules,
	NatureDemandeTransport,
	ServiceType,
	UserRole,
} from "@app/utils/enumeration";
import { PricingService } from "@app/services/pricing.service";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";
import { Statique } from "@app/utils/statique";
import { EbEtablissement } from "@app/classes/etablissement";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { EtablissementService } from "@app/services/etablissement.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EcCountry } from "@app/classes/country";
import { CompleterData, CompleterService } from "ng2-completer";
import { Cost, CostCategorie } from "@app/classes/costCategorie";
import { UserService } from "@app/services/user.service";
import { EbChat } from "@app/classes/chat";
import { ChatService } from "@app/services/chat.service";
import { EbCompagnie } from "@app/classes/compagnie";
import { StatiqueService } from "@app/services/statique.service";
import { EcCurrency } from "@app/classes/currency";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbInvoice } from "@app/classes/invoice";
import { FreightAuditService } from "@app/services/freight-audit.service";
import { AuthenticationService } from "@app/services/authentication.service";
import { GlobalService } from "@app/services/global.service";
import { EbTtCompagnyPslLightDto } from "@app/classes/ebTtCompagnyPslLightDto";
import { EbPlCostItemDTO } from "@app/classes/trpl/EbPlCostItemDTO";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { lstat } from "fs";
import { CostItemService } from "@app/services/cost-item.service";

@Component({
	selector: "app-cotation",
	templateUrl: "./cotation.component.html",
	styleUrls: ["./cotation.component.scss"],
})
export class CotationComponent implements OnInit {
	IDChatComponent = IDChatComponent;
	Statique = Statique;
	UserRole = UserRole;

	@Output()
	updateChatEmitter = new EventEmitter<EbChat>();
	@Output()
	updateCarrierChoiceEmitter = new EventEmitter<void>();
	@Output()
	feedbackQuotationEmitter = new EventEmitter<ExEbDemandeTransporteur>();

	@Input()
	ebDemande: EbDemande;
	@Input()
	userConnected: EbUser;
	@Input("module")
	ebModuleNum: number;
	@Input()
	exEbDemandeTransporteur: ExEbDemandeTransporteur;
	@Input()
	hasContributionAccess?: boolean;
	@Input()
	isForEdit: boolean;
	@Input()
	isForAdd: boolean = false;
	@Input()
	isForDetails: boolean = false;
	@Input()
	isCotationVisible: boolean = true;
	@Input()
	isCostInvoiceVisible: boolean = false;
	@Input()
	public nbColListing: string = "col-md-6 right-side";
	@Input()
	public nbColListingGlobal: string = "col-md-12";
	@Input()
	ebInvoice: EbInvoice = new EbInvoice();
	@Output()
	confirmEventHandler: EventEmitter<Object> = new EventEmitter();
	@Output()
	onQuoteEditedEmitter: EventEmitter<Object> = new EventEmitter();
	@Output()
	onCloseQuotePopupEmitter: EventEmitter<any> = new EventEmitter();

	@Input()
	isEditProcess = false;
	@Input()
	isPendingStatus = false;

	@Input()
	listPSL: Array<EbTtCompagnyPslLightDto> = new Array<EbTtCompagnyPslLightDto>();
	searchCriteria: SearchCriteria = new SearchCriteria();

	formattedPickupTime: NgbDateStruct;
	formattedDeliveryTime: NgbDateStruct;
	listDemandeTransporteur: Array<ExEbDemandeTransporteur> = new Array<ExEbDemandeTransporteur>();
	listCompagnieCurrency: Array<any> = [];

	ConstantsTranslate: Object;
	listCountry: Array<EcCountry>;
	suggetions: CompleterData;
	listPostCostCategorie: Array<CostCategorie>;
	modeTranspo: String = null;
	listCostCategorieByTransMode: Array<CostCategorie>;
	listAllCostCategorie: Array<CostCategorie>;
	listCostItemType: Array<Cost>;
	listCurrency: Array<EcCurrency>;
	listCurrenciesByPostCost = new Array<EcCurrency>();
	modeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();
	ModeTransportEnum = ModeTransport;
	readOnly: Boolean = false;
	Modules = Modules;
	statique = Statique;
	typeaheadUserEvent: EventEmitter<string> = new EventEmitter<string>();
	listUserTypeahead: Array<EbUser> = new Array<EbUser>();
	selectedUser: EbUser;
	dataInsertion: string = this.statique.controllerStatiqueListe + "/addChat";
	listChat: Array<EbChat> = new Array<EbChat>();
	ebChat: EbChat;
	listExEtablissementCurrency: Array<EbCompagnieCurrency> = new Array<EbCompagnieCurrency>();
	listExEtablissementCurrencys: Array<EbCompagnieCurrency> = new Array<EbCompagnieCurrency>();
	listEbCompagnieCurrency: Array<EbCompagnieCurrency> = new Array<EbCompagnieCurrency>();
	listCurrencyeCompagnie: Array<EcCurrency> = new Array<EcCurrency>();
	listCostCategorieInvoice: Array<CostCategorie>;
	listPlCostIemInvoice: Array<EbPlCostItemDTO>;
	detailCollapsed: boolean = true;
	code: string;
	isForAddCost: boolean = false;
	@Input()
	grid: boolean = false;

	cotationSuppAdded: boolean = false;
	compagnieNum: number;

	invoicePrice: number;
	gapPrice: number;

	constructor(
		protected chatService: ChatService,
		protected pricingService: PricingService,
		protected freightAuditService: FreightAuditService,
		protected modalService: ModalService,
		protected translate: TranslateService,
		protected etablissementService: EtablissementService,
		protected transportationPlanService: TransportationPlanService,
		protected completerService: CompleterService,
		protected userService: UserService,
		protected cd: ChangeDetectorRef,
		protected statiqueService: StatiqueService,
		protected globalService: GlobalService,
		protected costItemService: CostItemService
	) {}

	getStatiques() {
		(async () => (this.modeTransport = await SingletonStatique.getListModeTransport()))();
		(async () => (this.listCurrency = await SingletonStatique.getListEcCurrency()))();
	}

	initSearchCriteria(criteria: SearchCriteria) {
		criteria.communityCompany = true;
		criteria.ebEtablissementNum = this.ebDemande.xEbEtablissement.ebEtablissementNum;
		if (this.isChargerOrControlTower()) {
			if (this.ebDemande.xEbCompagnie != null) {
				criteria.ebCompagnieNum = this.ebDemande.xEbCompagnie.ebCompagnieNum;
			}
		}
		criteria.module = Modules.PRICING;
		if (this.exEbDemandeTransporteur.xEbCompagnie != null) {
			criteria.ebCompagnieGuestNum = this.exEbDemandeTransporteur.xEbCompagnie.ebCompagnieNum;
		}
		if (criteria.ebCompagnieGuestNum == null) {
			if (this.ebDemande.exEbDemandeTransporteurs[0] != null) {
				criteria.ebCompagnieGuestNum = this.ebDemande.exEbDemandeTransporteurs[0].xEbEtablissement.ebCompagnie.ebCompagnieNum;
				if (
					criteria.ebCompagnieNum == criteria.ebCompagnieGuestNum &&
					this.ebDemande.exEbDemandeTransporteurFinal != null
				) {
					criteria.ebCompagnieGuestNum = this.ebDemande.exEbDemandeTransporteurFinal.ebCompagnie.ebCompagnieNum;
				}
			}
		}

		criteria.currencyCibleNum = this.ebDemande.xecCurrencyInvoice.ecCurrencyNum;

		return criteria;
	}

	getListCurrenciesByPostCost() {
		let criteria: SearchCriteria = new SearchCriteria();
		criteria = this.initSearchCriteria(criteria);
		this.transportationPlanService
			.selectListCurrencyByeCompagnieCurrency(criteria)
			.subscribe((data) => {
				this.listCurrenciesByPostCost = data;
				let ciblCurrency =
					this.listCurrenciesByPostCost.length > 0
						? this.listCurrenciesByPostCost.find(
								(el) => el.ecCurrencyNum == this.ebDemande.xecCurrencyInvoice.ecCurrencyNum
						  )
						: null;
				if (ciblCurrency == null) {
					ciblCurrency = new EcCurrency();
					ciblCurrency.code = this.ebDemande.xecCurrencyInvoice.code;
					ciblCurrency.ecCurrencyNum = this.ebDemande.xecCurrencyInvoice.ecCurrencyNum;
					this.listCurrenciesByPostCost.push(ciblCurrency);
				}
			});
	}

	ngOnInit() {
		this.listPlCostIemInvoice = this.exEbDemandeTransporteur.listPlCostItem;
		this.etablissementService
			.getListExEtablissementCurrencyforQuotation(this.searchCriteria)
			.subscribe((data) => {
				this.listExEtablissementCurrency = data;
			});
		this.listPlCostIemInvoice &&
			this.listPlCostIemInvoice.forEach((it) => {
				if (it.libelle == null) {
					it.libelle = Statique.CostItemType[it.type - 1].value;
				}
			});
		this.getStatiques();
		this.getListShemaPSL();
		this.getListCurrenciesByPostCost();

		if (!this.ebDemande.xecCurrencyInvoice) this.ebDemande.xecCurrencyInvoice = new EcCurrency();

		if (
			this.exEbDemandeTransporteur.xEbCompagnieCurrency == null ||
			this.exEbDemandeTransporteur.xEbCompagnieCurrency.ecCurrency ||
			this.exEbDemandeTransporteur.xEbCompagnieCurrency.ecCurrency.ecCurrencyNum
		) {
			let searchCriteria: SearchCriteria = new SearchCriteria();

			if (
				this.userConnected.role == UserRole.PRESTATAIRE &&
				(this.userConnected.service == ServiceType.TRANSPORTEUR ||
					this.userConnected.service == ServiceType.TRANSPORTEUR_BROKER)
			)
				searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			else if (
				this.exEbDemandeTransporteur &&
				this.exEbDemandeTransporteur.xEbEtablissement &&
				this.exEbDemandeTransporteur.xEbEtablissement.ebEtablissementNum
			)
				searchCriteria.ebEtablissementNum = this.exEbDemandeTransporteur.xEbEtablissement.ebEtablissementNum;
			else searchCriteria.ebEtablissementNum = this.ebDemande.xEbEtablissement.ebEtablissementNum;

			searchCriteria.module = this.ebModuleNum;
			searchCriteria.ebCompagnieGuestNum = this.ebDemande.user.ebCompagnie.ebCompagnieNum;
			searchCriteria.currencyCibleNum = this.ebDemande.xecCurrencyInvoice.ecCurrencyNum;

			/*Ajout de l'invoice currency sur la liste des currency*/
			let ebCompagnieCurrencyInvoice = new EbCompagnieCurrency();
			ebCompagnieCurrencyInvoice.xecCurrencyCible = this.ebDemande.xecCurrencyInvoice;
			ebCompagnieCurrencyInvoice.ecCurrency = this.ebDemande.xecCurrencyInvoice;
		}

		if (typeof this.hasContributionAccess == "undefined") this.hasContributionAccess = true;

		// translation
		this.ConstantsTranslate = {
			CONFIRMATION: "",
			QUOTATION_SENT: "",
		};
		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate
				.get("PRICING_BOOKING." + key)
				.subscribe((res) => (this.ConstantsTranslate[key] = res));
		});

		this.getCountries();
		this.Modetranspo();
		this.suggetions = this.completerService.remote(
			Statique.controllerPricing + "/party?company=",
			"company",
			"company"
		);

		this.listCostCategorieByTransMode = new Array<CostCategorie>();
		if (!this.exEbDemandeTransporteur.xEcModeTransport) {
			this.exEbDemandeTransporteur.xEcModeTransport = this.ebDemande.xEcModeTransport;
		}

		if (this.ebDemande.user.ebCompagnie != null) {
			this.compagnieNum = this.ebDemande.user.ebCompagnie.ebCompagnieNum;
		}

		this.costItemService.listCostCategorieByCompagnie(this.compagnieNum, true).subscribe((data) => {
			this.listAllCostCategorie = data;
			this.listCostCategorieByTransMode = this.listEbCostByTransportMode(
				this.listAllCostCategorie,
				this.ebDemande.xEcModeTransport
			);

			if (
				!this.exEbDemandeTransporteur.listCostCategorie ||
				this.exEbDemandeTransporteur.listCostCategorie.length == 0
			) {
				this.exEbDemandeTransporteur.listCostCategorie = new Array<CostCategorie>();
				this.listCostCategorieByTransMode.forEach((it) => {
					this.exEbDemandeTransporteur.listCostCategorie.push(JSON.parse(JSON.stringify(it)));
				});
			}

			this.exEbDemandeTransporteur.listCostCategorie.forEach((it) => {
				it.isCollapsed = true;
				let totalPriceConverted: number = 0;
				it.listEbCost.forEach((el) => {
					if (el.price != null && el.priceEuro != null) {
						this.listCurrency &&
							this.listCurrency.forEach((code) => {
								if (el.xEcCurrencyNum != null) {
									if (code.ecCurrencyNum == el.xEcCurrencyNum) {
										el.codeCurrency = code.code;
									}
								}
							});
						let exChangeRateEuro = el.euroExchangeRate != null ? el.euroExchangeRate : 1;
						el.priceConverted = Statique.truncate2Decimal(el.price * exChangeRateEuro);
						totalPriceConverted = totalPriceConverted + el.priceConverted;
					}
				});

				it.totalPriceConverted = Statique.truncate2Decimal(totalPriceConverted);
			});
		});

		setTimeout(() => {
			if (this.ebInvoice) {
				if (
					(!this.ebInvoice.listCostCategorieCharger ||
						this.ebInvoice.listCostCategorieCharger.length == 0) &&
					this.exEbDemandeTransporteur.listCostCategorie &&
					this.isChargerOrControlTower()
				)
					this.listCostCategorieInvoice = this.exEbDemandeTransporteur.listCostCategorie;
				else if (
					this.isTransporteur() &&
					(!this.ebInvoice.listCostCategorieTransporteur ||
						this.ebInvoice.listCostCategorieTransporteur.length == 0) &&
					this.exEbDemandeTransporteur.listCostCategorie
				)
					this.listCostCategorieInvoice = this.exEbDemandeTransporteur.listCostCategorie;
				else {
					if (this.isTransporteur()) {
						this.listCostCategorieInvoice = this.ebInvoice.listCostCategorieTransporteur;
					} else if (this.isChargerOrControlTower()) {
						this.listCostCategorieInvoice = this.ebInvoice.listCostCategorieCharger;
					}
				}

				this.listCostCategorieInvoice &&
					this.listCostCategorieInvoice.forEach((it) => {
						it.isCollapsed = true;
						it.listEbCost.forEach((el) => {
							if (el.price != null && el.priceEuro != null) {
								if (el.xEcCurrencyNum == null)
									el.xEcCurrencyNum = this.ebDemande.xecCurrencyInvoice.ecCurrencyNum;
								if (el.xEcCurrencyNum == this.ebDemande.xecCurrencyInvoice.ecCurrencyNum) {
									el.euroExchangeRate = 1;
									el.euroExchangeRateReal = el.euroExchangeRate;
								}
								el.priceGabConverted = el.priceGab * el.euroExchangeRateReal;
								this.listCurrency &&
									this.listCurrency.forEach((code) => {
										if (code.ecCurrencyNum == el.xEcCurrencyNum) {
											el.codeCurrency = code.code;
										}
									});
							}
						});
					});
			}
		}, 500);
		/*Ajout de l'invoice currency sur la liste des currency*/
		let ebCompagnieCurrencyInvoice = new EbCompagnieCurrency();
		ebCompagnieCurrencyInvoice.xecCurrencyCible = this.ebDemande.xecCurrencyInvoice;
		ebCompagnieCurrencyInvoice.ecCurrency = this.ebDemande.xecCurrencyInvoice;
		this.etablissementService
			.getListExEtablissementCurrencyforQuotation(this.searchCriteria)
			.subscribe((data) => {
				if (data) {
					this.listExEtablissementCurrency = data;
					this.listExEtablissementCurrency.push(ebCompagnieCurrencyInvoice);
					this.listExEtablissementCurrency.forEach((it) => {
						this.listCurrencyeCompagnie.push(it.ecCurrency);
					});
				} else {
					this.listExEtablissementCurrency.push(ebCompagnieCurrencyInvoice);
				}
			});

		this.etablissementService
			.getListExEtablissementCurrencyforQuotation(this.searchCriteria)
			.subscribe((data) => {
				if (this.exEbDemandeTransporteur.isFromTransPlan == true) {
					data.forEach((it) => {
						if (this.exEbDemandeTransporteur.listPlCostItem != null) {
							this.exEbDemandeTransporteur.listPlCostItem.forEach((item) => {
								let totalPriceConverted: number = 0;
								let priceConverted: number = 0;
								if (item.currency != null) {
									if (
										item.currency.ecCurrencyNum == it.ecCurrency.ecCurrencyNum &&
										it.xecCurrencyCible.ecCurrencyNum ==
											this.ebDemande.xecCurrencyInvoice.ecCurrencyNum
									) {
										if (item.price != null && item.currency.ecCurrencyNum != null) {
											this.listCurrency &&
												this.listCurrency.forEach((code) => {
													if (code.ecCurrencyNum == item.currency.ecCurrencyNum) {
														item.xEcCurrencyNum = item.currency.ecCurrencyNum;
														item.code = item.currency.code;
														if (item.price != null) {
															let exChangeRateEuro = item.euroExchangeRate
																? item.euroExchangeRate
																: 1;
															priceConverted = item.price * exChangeRateEuro;
															item.priceConverted = priceConverted;
															totalPriceConverted = totalPriceConverted + item.priceConverted;
															this.exEbDemandeTransporteur.priceReal = totalPriceConverted;
														}
														it.totalPriceConverted = totalPriceConverted;
													}
												});
										}
									} else if (
										item.currency.ecCurrencyNum == it.ecCurrency.ecCurrencyNum &&
										it.xecCurrencyCible.ecCurrencyNum !=
											this.ebDemande.xecCurrencyInvoice.ecCurrencyNum
									) {
										if (item.price != null && item.currency.ecCurrencyNum != null) {
											this.listCurrency &&
												this.listCurrency.forEach((code) => {
													if (code.ecCurrencyNum == item.currency.ecCurrencyNum) {
														item.xEcCurrencyNum = item.currency.ecCurrencyNum;
														item.code = item.currency.code;
														if (item.price != null) {
															let exChangeRateEuro = item.euroExchangeRate
																? item.euroExchangeRate
																: 1;

															priceConverted = item.price * exChangeRateEuro;
															item.priceConverted = priceConverted;
															totalPriceConverted = totalPriceConverted + item.priceConverted;
														}
														it.totalPriceConverted = totalPriceConverted;
													}
												});
										}
									}
								}
							});
						}
					});
				}
			});
		this.getListCurrencyFromSettings();
	}
	preFills() {
		if (this.listCostCategorieByTransMode != null) {
			this.listCostCategorieInvoice &&
				this.listCostCategorieInvoice.forEach((it) => {
					it.listEbCost.forEach((el) => {
						el.priceReal = el.price;
						el.euroExchangeRateReal = el.euroExchangeRate;
						this.exEbDemandeTransporteur.listCostCategorie.forEach((lt) => {
							lt.listEbCost.forEach((lc) => {
								if (el.ebCostOrder != null && lc.ebCostOrder != null) {
									if (
										el.ebCostOrder == lc.ebCostOrder &&
										it.ebCostCategorieNum == lt.ebCostCategorieNum
									) {
										el.euroExchangeRateReal = lc.euroExchangeRate;
									}
								}
							});
						});
					});
					this.doExchangeRateConversionForCostItems(it, this.listCostCategorieInvoice);
				});
		}

		if (this.exEbDemandeTransporteur.isFromTransPlan) {
			this.listPlCostIemInvoice &&
				this.listPlCostIemInvoice.forEach((it) => {
					it.priceReal = it.price / it.euroExchangeRate;
					it.euroExchangeRateReal = it.euroExchangeRate;
				});
			this.doExchangeRateConversionForTransportPlanCostItems(this.exEbDemandeTransporteur);
		}
	}

	preFillRate() {
		this.listCostCategorieInvoice &&
			this.listCostCategorieInvoice.forEach((costCategorie) => {
				costCategorie.listEbCost.forEach((cost) => {
					cost.euroExchangeRateReal = cost.euroExchangeRate != null ? cost.euroExchangeRate : 1;
				});
				this.doExchangeRateConversionForCostItems(costCategorie, this.listCostCategorieInvoice);
			});

		//  TODO : TO TEST WITH TRANSPORT PLAN
		if (this.exEbDemandeTransporteur.isFromTransPlan) {
			this.listPlCostIemInvoice &&
				this.listPlCostIemInvoice.forEach((it) => {
					it.euroExchangeRateReal = it.euroExchangeRate != null ? it.euroExchangeRate : 1;
				});
			this.doExchangeRateConversionForTransportPlanCostItems(this.exEbDemandeTransporteur);
		}
	}

	private getListCurrencyFromSettings() {
		let criteria: SearchCriteria = new SearchCriteria();
		criteria = this.initSearchCriteria(criteria);
		criteria.ebCategorieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.etablissementService.getListCompagnieCurrencysBySetting(criteria).subscribe((data) => {
			this.listEbCompagnieCurrency = data;
		});
	}

	getListShemaPSL() {
		if (
			!Statique.isDefined(this.exEbDemandeTransporteur.listEbTtCompagniePsl) ||
			!this.exEbDemandeTransporteur.listEbTtCompagniePsl.length
		) {
			this.exEbDemandeTransporteur.listEbTtCompagniePsl = new Array<EbTtCompagnyPslLightDto>();
			for (let psl of this.ebDemande.xEbSchemaPsl.listPsl) {
				if (psl.isChecked && psl.schemaActif && psl.dateAttendue) {
					let compagniePSl: EbTtCompagnyPslLightDto = new EbTtCompagnyPslLightDto();
					compagniePSl.libelle = psl.libelle;
					compagniePSl.id = psl.ebTtCompanyPslNum;
					compagniePSl.startPsl = psl.startPsl;
					compagniePSl.schemaActif = psl.schemaActif;
					compagniePSl.isChecked = psl.isChecked;
					compagniePSl.endPsl = psl.endPsl;
					compagniePSl.codeAlpha = psl.codeAlpha;
					compagniePSl.order = psl.order;
					this.exEbDemandeTransporteur.listEbTtCompagniePsl.push(compagniePSl);
				}
			}
		}
	}

	validateCotationChoice() {
		let cat = new Array<CostCategorie>();
		this.exEbDemandeTransporteur.listCostCategorie.forEach((el) => {
			el.listEbCost.filter((cost) =>
				this.isContainsTransportMode(
					cost.listModeTransport,
					this.exEbDemandeTransporteur.xEcModeTransport
				)
			);
			if (el.listEbCost.length > 0) {
				cat.push(el);
			}
		});
		this.exEbDemandeTransporteur.listCostCategorie = cat;
		this.feedbackQuotationEmitter.emit(this.exEbDemandeTransporteur);
	}
	calculateCostCategorieTotalPrice() {
		let costCategorieTotalPrice = 0;
		let negotiatedPrice: number = Number(Number(this.exEbDemandeTransporteur.price).toFixed(2));
		if (
			this.exEbDemandeTransporteur.listCostCategorie != null &&
			this.exEbDemandeTransporteur.listCostCategorie.length > 0
		) {
			this.exEbDemandeTransporteur.listCostCategorie.forEach((costCategorie) => {
				if (costCategorie != null) costCategorieTotalPrice += costCategorie.totalPriceConverted;
			});
			if (negotiatedPrice != costCategorieTotalPrice && costCategorieTotalPrice == 0) {
				let otherCostIndex: number = this.exEbDemandeTransporteur.listCostCategorie[0].listEbCost.findIndex(
					(ebcost) => ebcost.code == "OC"
				);
				if (otherCostIndex != -1) {
					this.exEbDemandeTransporteur.listCostCategorie[0].totalPriceConverted = negotiatedPrice;
					this.exEbDemandeTransporteur.listCostCategorie[0].price = negotiatedPrice;
					this.exEbDemandeTransporteur.listCostCategorie[0].listEbCost[
						otherCostIndex
					].price = negotiatedPrice;
					this.exEbDemandeTransporteur.listCostCategorie[0].listEbCost[
						otherCostIndex
					].priceEuro = negotiatedPrice;
					this.exEbDemandeTransporteur.listCostCategorie[0].listEbCost[
						otherCostIndex
					].euroExchangeRate = 1;
				}
			}
		}
	}

	getCurrency(item: Cost) {
		if (!item || !item.xEcCurrencyNum) return null;
		return this.listCurrency.find((it) => it.ecCurrencyNum == item.xEcCurrencyNum);
	}
	getCurrencyCode(item: Cost) {
		if (!item || !item.xEcCurrencyNum) return null;
		return this.listCurrency.find((it) => it.code == item.codeCurrency);
	}

	Modetranspo() {
		if (this.ebDemande.xEcModeTransport == 1) this.modeTranspo = "Air";
		if (this.ebDemande.xEcModeTransport == 2) this.modeTranspo = "Sea";
		if (this.ebDemande.xEcModeTransport == 3) this.modeTranspo = "Road";
		if (this.ebDemande.xEcModeTransport == 4) this.modeTranspo = "Integrateur";
	}

	compareCountry(country1: EcCountry, country2: EcCountry) {
		if (country2 !== undefined && country2 !== null) {
			return country1.ecCountryNum === country2.ecCountryNum;
		}
	}

	getCountries() {
		this.pricingService.listPays().subscribe((data) => {
			this.listCountry = data;
		});
	}

	formatTime() {
		let timeRemaining = "";
	}

	get isModulePricing(): boolean {
		return this.ebModuleNum == Modules.PRICING;
	}

	get isModuleTM(): boolean {
		return this.ebModuleNum == Modules.TRANSPORT_MANAGEMENT;
	}

	get isCreationMode(): boolean {
		return this.ebDemande.ebDemandeNum == null;
	}

	calculCostItemPriceCostItem(
		cat: CostCategorie,
		listPostCostCategorie: Array<CostCategorie>,
		exEbDemandeTransporteur: ExEbDemandeTransporteur
	) {
		let categPrice: number = 0;
		let categPriceConverted: number = 0;
		cat.listEbCost.forEach((it) => {
			it.price = it.price != null ? Number(it.price.toString().replace(/,/g, ".")) : 0;
			it.euroExchangeRate = it.euroExchangeRate != null ? it.euroExchangeRate : 1;
			it.priceEuro = Statique.truncate2Decimal(it.price * it.euroExchangeRate);
			categPriceConverted += it.priceEuro;
			categPrice += it.price;
		});
		cat.price = Statique.truncate2Decimal(categPrice);
		cat.totalPriceConverted = Statique.truncate2Decimal(categPriceConverted);
		this.calculPriceTotal(listPostCostCategorie, exEbDemandeTransporteur);
	}
	doExchangeRateConversionForCostItems(
		cat: CostCategorie,
		listPostCostCategorie: Array<CostCategorie>
	) {
		let categPriceReal: number = 0;
		let categPriceEuroReal: number = 0;
		let categPriceGap: number = 0;
		cat.listEbCost.forEach((it) => {
			it.priceReal = it.priceReal != null ? it.priceReal : 0;
			it.priceReal = Statique.truncate2Decimal(Number(it.priceReal.toString().replace(/,/g, ".")));
			it.euroExchangeRateReal =
				it.euroExchangeRateReal != 0 && it.euroExchangeRateReal != null
					? it.euroExchangeRateReal
					: 1;
			it.priceEuroReal = Statique.truncate2Decimal(it.priceReal * it.euroExchangeRateReal);
			categPriceEuroReal += it.priceEuroReal;
			categPriceReal += it.priceReal;
			it.priceGab = Statique.truncate2Decimal(it.priceReal - it.price);
			it.priceGabConverted = Statique.truncate2Decimal(it.priceGab * it.euroExchangeRateReal);
			categPriceGap += it.priceGabConverted;
		});
		cat.priceReal = Statique.truncate2Decimal(categPriceReal);
		cat.priceEuroReal = Statique.truncate2Decimal(categPriceEuroReal);
		cat.priceGab = Statique.truncate2Decimal(categPriceGap);
		this.calculTotalPriceCostCategorie(listPostCostCategorie);
	}

	doExchangeRateConversionForTransportPlanCostItems(
		exEbDemandeTransporteur: ExEbDemandeTransporteur
	) {
		let priceReal: number = 0;
		let priceEuroReal: number = 0;
		let priceGab: number = 0;

		if (exEbDemandeTransporteur.isFromTransPlan) {
			exEbDemandeTransporteur.listPlCostItem.forEach((item) => {
				item.priceReal = item.priceReal != null ? item.priceReal : 0;
				item.priceReal = Statique.truncate2Decimal(
					Number(item.priceReal.toString().replace(/,/g, "."))
				);
				priceReal += item.priceReal;

				item.euroExchangeRateReal =
					item.euroExchangeRateReal != 0 && item.euroExchangeRateReal != null
						? item.euroExchangeRateReal
						: 1;

				item.priceEuroReal = Statique.truncate2Decimal(item.priceReal * item.euroExchangeRateReal);
				priceEuroReal += item.priceEuroReal;

				item.priceGab = item.priceReal - item.price / item.euroExchangeRate;
				item.priceGabConverted = item.priceGab * item.euroExchangeRateReal;
				item.priceRealConverted = item.priceReal * item.euroExchangeRateReal;
				priceGab += item.priceGabConverted;
			});
		}
		exEbDemandeTransporteur.priceReal = Statique.truncate2Decimal(priceReal);
		exEbDemandeTransporteur.priceEuroReal = Statique.truncate2Decimal(priceEuroReal);
		exEbDemandeTransporteur.priceGab = Statique.truncate2Decimal(priceGab);
		this.calculTotalPriceCostCategorieForTransportPlan(exEbDemandeTransporteur);
	}

	calculTotalPriceCostCategorieForTransportPlan(exEbDemandeTransporteur) {
		let totalPriceEuroReal: number = 0;
		let totalPriceGap: number = 0;
		let totalPriceGapConverted: number = 0;
		this.readOnly = true;

		exEbDemandeTransporteur.listPlCostItem &&
			exEbDemandeTransporteur.listPlCostItem.forEach((item) => {
				if (item.priceEuroReal != null) {
					totalPriceEuroReal += Statique.truncate2Decimal(item.priceEuroReal);
					totalPriceGap += Statique.truncate2Decimal(item.priceGab);
					totalPriceGapConverted += Statique.truncate2Decimal(
						item.priceGab * item.euroExchangeRateReal
					);
				}
			});
		if (this.isChargerOrControlTower()) {
			this.ebInvoice.applyChargeurIvoicePrice = Statique.truncate2Decimal(totalPriceEuroReal);
			this.invoicePrice = this.ebInvoice.applyChargeurIvoicePrice;
			this.ebInvoice.gapChargeur = this.exEbDemandeTransporteur.priceGab;
			this.gapPrice = this.exEbDemandeTransporteur.priceGab;
		} else if (this.isTransporteur()) {
			this.invoicePrice = Statique.truncate2Decimal(totalPriceEuroReal);
			this.ebInvoice.applyCarrierIvoicePrice = Statique.truncate2Decimal(totalPriceEuroReal);
			this.gapPrice = Statique.truncate2Decimal(totalPriceGapConverted);
			this.ebInvoice.gapCarrier = Statique.truncate2Decimal(totalPriceGapConverted);
		}
	}

	calculTotalPriceCostCategorie(listPostCostCategorie: Array<CostCategorie>) {
		let totalInvoicePrice: number = 0;
		let totalPriceGap: number = 0;
		this.readOnly = true;

		listPostCostCategorie &&
			listPostCostCategorie.forEach((costCategorie: CostCategorie) => {
				if (costCategorie.priceEuroReal != null) {
					totalInvoicePrice += Statique.truncate2Decimal(costCategorie.priceEuroReal);
					totalPriceGap += Statique.truncate2Decimal(costCategorie.priceGab);
				}
			});
		if (this.isChargerOrControlTower()) {
			this.ebInvoice.applyChargeurIvoicePrice = Statique.truncate2Decimal(totalInvoicePrice);
			this.ebInvoice.gapChargeur = Statique.truncate2Decimal(totalPriceGap);
		} else if (this.isTransporteur()) {
			this.ebInvoice.applyCarrierIvoicePrice = Statique.truncate2Decimal(totalInvoicePrice);
			this.ebInvoice.gapCarrier = Statique.truncate2Decimal(totalPriceGap);
		}
	}

	calculPriceTotal(listPostCostCategorie, exEbDemandeTransporteur) {
		let total: number = 0;
		listPostCostCategorie &&
			listPostCostCategorie.forEach((it) => {
				if (it.totalPriceConverted != null) {
					total += Statique.truncate2Decimal(it.totalPriceConverted);
				}
				exEbDemandeTransporteur.price = Statique.truncate2Decimal(total);
			});
	}

	disableField(exEbDemandeTransporteur?) {
		if (
			(this.ebModuleNum == Modules.TRANSPORT_MANAGEMENT && this.ebDemande.ebDemandeNum == null) ||
			this.isPendingStatus
		)
			return false;

		return (
			!(this.isTransporteur() || this.userConnected.role == UserRole.CONTROL_TOWER) ||
			(exEbDemandeTransporteur && exEbDemandeTransporteur.status == CarrierStatus.FINAL_CHOICE) ||
			this.ebDemande.xEcStatut >= DemandeStatus.FINAL_CHOICE ||
			!this.hasContributionAccess ||
			!this.isForAdd
		);
	}

	isTransporteur() {
		return (
			this.userConnected.service == ServiceType.TRANSPORTEUR ||
			this.userConnected.service == ServiceType.TRANSPORTEUR_BROKER
		);
	}

	addPriceToOtherCost(price: number) {
		let categorie: CostCategorie = this.exEbDemandeTransporteur.listCostCategorie[0];
		let otherCostIndex = categorie.listEbCost.findIndex((cost) => cost.code == "OC");
		if (otherCostIndex == -1) {
			let costItem = this.fillNewCostItem(categorie);
			costItem.libelle = "Other costs";
			costItem.code = "OC";
			costItem.price = price;
			categorie.listEbCost.push(costItem);
		} else {
			categorie.listEbCost[otherCostIndex].price = price;
		}
		this.calculCostItemPriceCostItem(
			categorie,
			this.exEbDemandeTransporteur.listCostCategorie,
			this.exEbDemandeTransporteur
		);
	}

	addOtherCost(categorie: CostCategorie) {
		this.isForEdit = true;
		let newCostItemNegociated = this.fillNewCostItem(categorie);
		categorie.listEbCost.push(newCostItemNegociated);
	}

	addOtherCostInvoice(categorie: CostCategorie) {
		this.isForEdit = true;
		this.cotationSuppAdded = true;
		let indexCateg = this.listCostCategorieInvoice.findIndex(
			(categ) => categ.ebCostCategorieNum == categorie.ebCostCategorieNum
		);
		if (indexCateg != -1) {
			let newCostItemNegociated = this.fillNewCostItem(categorie);
			this.exEbDemandeTransporteur.listCostCategorie[indexCateg].listEbCost.push(
				newCostItemNegociated
			);
			let newCostItemCharged = this.fillNewCostItem(this.listCostCategorieInvoice[indexCateg]);
			this.listCostCategorieInvoice[indexCateg].listEbCost.push(newCostItemCharged);
		}
	}

	fillNewCostItem(categorie: CostCategorie): Cost {
		let cost = new Cost();
		let listCostItem: Array<Cost> = categorie.listEbCost.sort((a, b) => a.ebCostNum - b.ebCostNum);
		cost.ebCostNum = listCostItem[listCostItem.length - 1].ebCostNum + 1;
		cost.ebCostOrder = listCostItem[listCostItem.length - 1].ebCostNum + 1;
		cost.price = 0;
		cost.type = 2;
		return cost;
	}

	isTrGrouped(){
		return this.ebDemande.xEcNature == NatureDemandeTransport.CONSOLIDATION;
	}

	addQuotation(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		if (this.isForAdd) {
			if (!this.ebDemande.ebDemandeNum) {
				this.onQuoteEditedEmitter.emit(this.exEbDemandeTransporteur);
				return;
			}

			this.exEbDemandeTransporteur.xEbDemande = null;

			if (
				this.exEbDemandeTransporteur.status != CarrierStatus.RECOMMENDATION &&
				this.exEbDemandeTransporteur.price &&
				this.ebDemande.xEcStatut != DemandeStatus.WPU
			)
				this.exEbDemandeTransporteur.status = CarrierStatus.QUOTE_SENT;
			this.calculateCostCategorieTotalPrice();

			this.exEbDemandeTransporteur.xEbDemande = new EbDemande();
			let o;
			for (o in this.exEbDemandeTransporteur.xEbDemande)
				this.exEbDemandeTransporteur.xEbDemande[o] = null;
			this.exEbDemandeTransporteur.xEbDemande.ebDemandeNum = this.ebDemande.ebDemandeNum;
			this.pricingService
				.ajouterQuotation(this.exEbDemandeTransporteur, this.ebModuleNum, false)
				.subscribe(
					(res) => {
						if (res.ebChat != null && this.ebModuleNum != res.ebChat.module) {
							if (res.ebChat.idChatComponent == IDChatComponent.TRANSPORT_MANAGEMENT) {
								res.ebChat.idChatComponent == IDChatComponent.PRICING;
								res.ebChat.module == Modules.PRICING;
							} else {
								res.ebChat.idChatComponent == IDChatComponent.TRANSPORT_MANAGEMENT;
								res.ebChat.module == Modules.TRANSPORT_MANAGEMENT;
							}
						}

						this.updateChatEmitter.emit(res.ebChat);
						if (
							this.ebDemande.flagRecommendation &&
							this.ebDemande.xEcStatut != DemandeStatus.RECOMMENDATION
						)
							this.ebDemande.xEcStatut = DemandeStatus.WAITING_FOR_RECOMMENDATION;
						else this.ebDemande.xEcStatut = DemandeStatus.RECOMMENDATION;

						this.feedbackQuotationEmitter.emit(res.exEbDemandeTransporteur);

						requestProcessing.afterGetResponse(event);
						window.location.reload();
					},
					(error) => {
						requestProcessing.afterGetResponse(event);
						let title = null;
						let body = null;
						this.translate.get("MODAL.ERROR").subscribe((res: string) => {
							title = res;
						});
						// this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
						this.modalService.error(title, null, () => {});
					}
				);
		} else if (this.isForEdit) {
			this.pricingService.editQuotation(this.exEbDemandeTransporteur, this.ebModuleNum).subscribe(
				(res) => {
					if (this.ebModuleNum != res.ebChat.module) {
						if (res.ebChat.idChatComponent == IDChatComponent.TRANSPORT_MANAGEMENT) {
							res.ebChat.idChatComponent == IDChatComponent.PRICING;
							res.ebChat.module == Modules.PRICING;
						} else {
							res.ebChat.idChatComponent == IDChatComponent.TRANSPORT_MANAGEMENT;
							res.ebChat.module == Modules.TRANSPORT_MANAGEMENT;
						}
					}

					this.updateChatEmitter.emit(res.ebChat);
					this.feedbackQuotationEmitter.emit(res.exEbDemandeTransporteur);
					requestProcessing.afterGetResponse(event);
					window.location.reload();
				},
				(error) => {
					requestProcessing.afterGetResponse(event);
					let title = null;
					let body = null;
					this.translate.get("MODAL.ERROR").subscribe((res: string) => {
						title = res;
					});
					this.modalService.error(title, null, () => {});
				}
			);
		}
	}

	closePopup($event) {
		this.onCloseQuotePopupEmitter.emit($event);
	}

	getComment(comment, exEbDemandeTransporteur) {
		exEbDemandeTransporteur.comment = comment;
	}

	showPsl(psl: string) {
		return this.ebDemande.configPsl != null && this.ebDemande.configPsl.indexOf(psl) >= 0;
	}

	handleChangePrice(event: number, item) {
		item.price = event;
	}

	handleChangePriceReal(event: number, item) {
		item.priceReal = event;
	}
	handleChangeEuroExchangeRate(event: number, item) {
		item.euroExchangeRateReal = event;
	}

	changeCostItemCurrency(item: Cost, cat: CostCategorie, isForTransportRequestCreation: boolean) {
		item.euroExchangeRate = 1;
		item.euroExchangeRateReal = 1;
		const costItemCurrency = this.listEbCompagnieCurrency.find(
			(it) => item.xEcCurrencyNum == it.ecCurrency.ecCurrencyNum
		);
		if (costItemCurrency) {
			item.euroExchangeRate = costItemCurrency.euroExchangeRate;
			item.euroExchangeRateReal = costItemCurrency.euroExchangeRate;
		}
		const indexCateg = isForTransportRequestCreation
			? this.exEbDemandeTransporteur.listCostCategorie.findIndex(
					(categ) => categ.ebCostCategorieNum == cat.ebCostCategorieNum
			  )
			: this.listCostCategorieInvoice.findIndex(
					(categ) => categ.ebCostCategorieNum == cat.ebCostCategorieNum
			  );
		const indexCostItem = cat.listEbCost.findIndex((cost) => cost.ebCostNum == item.ebCostNum);
		if (indexCostItem != -1 && indexCateg != -1) {
			if (!isForTransportRequestCreation) {
				this.listCostCategorieInvoice[indexCateg].listEbCost[indexCostItem].xEcCurrencyNum =
					item.xEcCurrencyNum;
				this.listCostCategorieInvoice[indexCateg].listEbCost[indexCostItem].euroExchangeRate =
					item.euroExchangeRate;
				this.listCostCategorieInvoice[indexCateg].listEbCost[indexCostItem].euroExchangeRateReal =
					item.euroExchangeRate;
				this.doExchangeRateConversionForCostItems(
					this.listCostCategorieInvoice[indexCateg],
					this.listCostCategorieInvoice
				);
			} else {
				this.exEbDemandeTransporteur.listCostCategorie[indexCateg].listEbCost[
					indexCostItem
				].xEcCurrencyNum = item.xEcCurrencyNum;
				this.exEbDemandeTransporteur.listCostCategorie[indexCateg].listEbCost[
					indexCostItem
				].euroExchangeRate = item.euroExchangeRate;
				this.exEbDemandeTransporteur.listCostCategorie[indexCateg].listEbCost[
					indexCostItem
				].euroExchangeRateReal = item.euroExchangeRate;
				this.calculCostItemPriceCostItem(
					cat,
					this.exEbDemandeTransporteur.listCostCategorie,
					this.exEbDemandeTransporteur
				);
			}
		}
	}

	ChangedDeviseByGrid(item, exEbDemandeTransporteur: ExEbDemandeTransporteur) {
		this.listExEtablissementCurrency = this.listEbCompagnieCurrency;
		this.listExEtablissementCurrency.forEach((it) => {
			if (
				item.code == it.ecCurrency.code &&
				it.xecCurrencyCible.ecCurrencyNum == this.ebDemande.xecCurrencyInvoice.ecCurrencyNum
			) {
				item.euroExchangeRate = it.euroExchangeRate;
				item.euroExchangeRateReal = it.euroExchangeRate;
			} else if (
				item.xEcCurrencyNum == it.ecCurrency.ecCurrencyNum &&
				it.xecCurrencyCible.ecCurrencyNum != this.ebDemande.xecCurrencyInvoice.ecCurrencyNum
			) {
				item.euroExchangeRate = 1;
			}
		});

		if (item.xEcCurrencyNum == this.ebDemande.xecCurrencyInvoice.ecCurrencyNum) {
			item.euroExchangeRate = 1;
		}
		this.preFillRate();
		this.doExchangeRateConversionForTransportPlanCostItems(exEbDemandeTransporteur);
	}

	ChangedDeviseForReal(item, cat: CostCategorie, listPostCostCategorie) {
		this.listExEtablissementCurrency.forEach((it) => {
			if (
				item.xEcCurrencyNumReal == it.ecCurrency.ecCurrencyNum &&
				it.xecCurrencyCible.ecCurrencyNum == this.ebDemande.xecCurrencyInvoice.ecCurrencyNum
			)
				item.euroExchangeRate = it.euroExchangeRate;
			else if (
				item.xEcCurrencyNumReal == it.ecCurrency.ecCurrencyNum &&
				it.xecCurrencyCible.ecCurrencyNum != this.ebDemande.xecCurrencyInvoice.ecCurrencyNum
			)
				item.euroExchangeRate = 1;
		});
		if (item.xEcCurrencyNumReal == this.ebDemande.xecCurrencyInvoice.ecCurrencyNum)
			item.euroExchangeRate = 1;

		this.doExchangeRateConversionForCostItems(cat, listPostCostCategorie);
	}

	ChangedDeviseInvoice(
		item,
		cat: CostCategorie,
		listPostCostCategorie,
		exEbDemandeTransporteur: ExEbDemandeTransporteur
	) {
		this.listExEtablissementCurrency.forEach((it) => {
			if (
				item.xEcCurrencyNumReal == it.ecCurrency.ecCurrencyNum &&
				it.xecCurrencyCible.ecCurrencyNum == this.ebDemande.xecCurrencyInvoice.ecCurrencyNum
			)
				item.euroExchangeRateReal = it.euroExchangeRate;
			else if (
				item.xEcCurrencyNumReal == it.ecCurrency.ecCurrencyNum &&
				it.xecCurrencyCible.ecCurrencyNum != this.ebDemande.xecCurrencyInvoice.ecCurrencyNum
			)
				item.euroExchangeRate = 1;
		});
	}
	addRealQuotation(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.ebInvoice.priceArraw = 0;

		/*
			cost objects in listCostCategorieInvoice from invoice does not contain currencies and names
			which are needed in backup for history purposes

		*/
		try {
			if (
				this.exEbDemandeTransporteur.isFromTransPlan &&
				this.listPlCostIemInvoice &&
				this.listPlCostIemInvoice.length
			) {
				this.listCostCategorieInvoice = [];
				let cc = new CostCategorie();
				cc.ebCostCategorieNum = -1;
				cc.libelle = "Price details";
				cc.listEbCost = [];
				this.listPlCostIemInvoice &&
					this.listPlCostIemInvoice.forEach((pl) => {
						let c = new Cost();
						c.ebCostNum = pl.costItemNum;
						c.price = pl.price;
						c.priceConverted = pl.priceConverted;
						c.priceEuroReal = pl.priceEuroReal;
						c.priceGab = pl.priceGab;
						c.priceGabConverted = pl.priceGabConverted;
						c.priceReal = pl.priceReal;
						c.priceRealConverted = pl.priceRealConverted;
						c.libelle = pl.libelle;
						c.type = pl.type;
						c.xEcCurrencyNum = pl.xEcCurrencyNum;
						c.xEcCurrencyNumReal = pl.xEcCurrencyNumReal;
						c.euroExchangeRate = pl.euroExchangeRate;
						c.euroExchangeRateReal = pl.euroExchangeRate;
						c.codeCurrency = pl.codeCurrency;
						c.currencyInvoice = pl.currencyInvoice;
						c.ebCompagnieNum = pl.ebCompagnieNum;
						cc.listEbCost.push(c);
					});
				this.listCostCategorieInvoice.push(cc);
			} else {
				this.listCostCategorieInvoice &&
					this.listCostCategorieInvoice.forEach((it) => {
						let trCat = this.exEbDemandeTransporteur.listCostCategorie.find(
							(el) => el.ebCostCategorieNum == it.ebCostCategorieNum
						);
						if (trCat) {
							it.listEbCost &&
								it.listEbCost.forEach((it2) => {
									let trCost = trCat.listEbCost.find((el) => el.ebCostNum == it2.ebCostNum);
									if (trCost) {
										it2.xEcCurrencyNum = trCost.xEcCurrencyNum;
										it2.libelle = trCost.libelle;
									}
								});
						}
					});
			}
		} catch (err) {
			console.error(err);
		}

		if (this.isChargerOrControlTower()) {
			this.ebInvoice.listCostCategorieCharger = this.listCostCategorieInvoice;
		} else if (this.isTransporteur) {
			this.ebInvoice.listCostCategorieTransporteur = this.listCostCategorieInvoice;
		}

		this.freightAuditService.updateListCostCategorieAndPrice(this.ebInvoice).subscribe(
			(res) => {
				this.confirmEventHandler.emit({ invoice: this.ebInvoice });
				this.feedbackQuotationEmitter.emit(null);
				requestProcessing.afterGetResponse(event);
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				let title = null;
				let body = null;
				this.translate.get("MODAL.ERROR").subscribe((res: string) => {
					title = res;
				});
				// this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
				this.modalService.error(title, null, () => {});
			}
		);
		this.exEbDemandeTransporteur.listPlCostItem = this.listPlCostIemInvoice;
		this.pricingService
			.ajouterQuotation(this.exEbDemandeTransporteur, this.ebModuleNum, false)
			.subscribe(function(res) {});
	}

	eventCollap(categorie: CostCategorie, isFromPlan: boolean = false) {
		if (isFromPlan) {
			this.detailCollapsed = !this.detailCollapsed;
		} else {
			categorie.isCollapsed = !categorie.isCollapsed;
			if (this.isCostInvoiceVisible) {
				this.listCostCategorieInvoice &&
					this.listCostCategorieInvoice.forEach((res) => {
						if (res.ebCostCategorieNum == categorie.ebCostCategorieNum)
							res.isCollapsed = categorie.isCollapsed;
					});
			}
		}
	}

	eventCollapRealCost(categorie: CostCategorie, exEbDemandeTransporteur: ExEbDemandeTransporteur) {
		categorie.isCollapsed = !categorie.isCollapsed;
		this.exEbDemandeTransporteur.listCostCategorie.forEach((res) => {
			if (res.ebCostCategorieNum == categorie.ebCostCategorieNum)
				res.isCollapsed = categorie.isCollapsed;
		});
	}
	calculCostGap() {
		if (this.userConnected && this.isChargerOrControlTower()) {
			this.ebInvoice.gapChargeur =
				this.ebInvoice.applyChargeurIvoicePrice - this.ebInvoice.preCarrierCost;
		} else if (this.isTransporteur()) {
			this.ebInvoice.gapCarrier =
				this.ebInvoice.applyCarrierIvoicePrice - this.ebInvoice.preCarrierCost;
		}
	}

	isChargerOrControlTower() {
		return (
			this.userConnected.role == UserRole.CHARGEUR ||
			this.userConnected.role == UserRole.CONTROL_TOWER
		);
	}

	getCommentInvoice(comment, ebinvvoice: EbInvoice) {
		if (this.isChargerOrControlTower()) this.ebInvoice.commentInvoiceCharger = comment;
		else if (this.isTransporteur()) this.ebInvoice.commentInvoiceCarrier = comment;
	}
	getCommentGap(comment, ebinvvoice: EbInvoice) {
		if (this.isChargerOrControlTower()) this.ebInvoice.commentGapCharger = comment;
		else if (this.isTransporteur()) this.ebInvoice.commentGapCarrier = comment;
	}

	addCoutationSupp(exEbDemandeTransporteur: ExEbDemandeTransporteur) {
		this.isForEdit = true;
		let code = this.ebDemande.xecCurrencyInvoice.code;
		this.cotationSuppAdded = true;
		this.listPlCostIemInvoice.push({
			costItemNum:
				exEbDemandeTransporteur.listPlCostItem[exEbDemandeTransporteur.listPlCostItem.length - 1]
					.costItemNum + 1,
			fixedFees: 0,
			calculationMode: null,
			trancheValues: null,
			type: 2,
			startingPsl: null,
			endPsl: null,
			comment: "",
			minFrees: null,
			maxFrees: null,
			typeUnit: null,
			price: 0,
			currency: null,
			code: null,
			priceEuro: null,
			priceReal: null,
			priceEuroReal: null,
			priceGab: null,
			totalPriceConverted: null,
			libelle: "",
			xEcCurrencyNum: null,
			euroExchangeRate: 1,
			codeCurrency: null,
			xEcCurrencyNumReal: null,
			priceGabConverted: null,
			euroExchangeRateReal: 1,
			priceConverted: null,
			priceRealConverted: null,
			currencyInvoice: null,
			ebCompagnieNum: null,
		});
		this.preFillRate();
	}

	formatInputPrice(input: any) {
		if (input != null) {
			let currentLang = this.translate.currentLang;
			if (currentLang == "fr") {
				return Number(
					input
						.toString()
						.replace(",", ".")
						.replace(/\s/g, "")
				);
			}
			return Number(input.toString().replace(",", ""));
		}
	}

	isContainsTransportMode(listModeTransport: string, modeTransport: number) {
		if (listModeTransport != null && listModeTransport.indexOf(":" + modeTransport + ":") >= 0) {
			return true;
		}
		return false;
	}

	listEbCostByTransportMode(
		listCostCategorie: Array<CostCategorie>,
		modeTransport: number
	): Array<CostCategorie> {
		let listCostByTransportMode = new Array<CostCategorie>();
		let listEbcost = new Array<Cost>();
		listCostCategorie.forEach((cat) => {
			listEbcost = cat.listEbCost.filter((cost) => {
				return this.isContainsTransportMode(cost.listModeTransport, modeTransport);
			});
			if (listEbcost.length > 0) {
				cat.listEbCost = listEbcost;
				listCostByTransportMode.push(cat);
			}
		});
		return listCostByTransportMode;
	}

	getCostItemLibelle(costNum: number) {
		let cost = new Cost();
		cost = this.listCostItemType.find((costItem) => costItem.ebCostNum === costNum);
		return cost.libelle.toString();
	}

	getListCostItemType() {
		let criteria = new SearchCriteria();
		criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		this.costItemService.listCostItems(criteria).subscribe((res) => {
			this.listCostItemType = res.data;
		});
	}
}
