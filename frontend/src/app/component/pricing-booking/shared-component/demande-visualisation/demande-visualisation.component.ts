import { ExportControlStatut } from "./../../../../utils/enumeration";
import {
	Component,
	EventEmitter,
	OnInit,
	Output,
	ViewChild,
	HostListener,
	AfterViewInit,
} from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { AuthenticationService } from "@app/services/authentication.service";
import { EbDemande } from "@app/classes/demande";
import { Statique } from "@app/utils/statique";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { PricingService } from "@app/services/pricing.service";
import { interval, Subscription } from "rxjs";
import {
	AccessRights,
	CancelStatus,
	CarrierStatus,
	DemandeStatus,
	IDChatComponent,
	Modules,
	UserRole,
	TypeDataEbDemande,
	QrCreationStep,
	NatureDemandeTransport,
	Ventilation,
	ResultEntityType,
} from "@app/utils/enumeration";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { GlobalService } from "@app/services/global.service";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { TranslateService } from "@ngx-translate/core";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbChat } from "@app/classes/chat";
import { ModalService } from "@app/shared/modal/modal.service";
import { EbEtablissement } from "@app/classes/etablissement";
import { EbCompagnie } from "@app/classes/compagnie";
import { EbUser } from "@app/classes/user";
import { EcCountry } from "@app/classes/country";
import { ChatPanelComponent } from "@app/shared/chat-panel/chat-panel.component";
import { EbCategorie } from "@app/classes/categorie";
import { DemandeCreationFormComponent } from "../creation-form/demande-creation-form.component";
import { MessageService } from "primeng/api";
import { PopupAssocierDeclarationDouaneComponent } from "../popup-associer-declaration-douane/popup-associer-declaration-douane.component";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { ModalExportDocByTemplateComponent } from "@app/shared/modal-export-doc-by-template/modal-export-doc-by-template.component";
import { DocumentsComponent } from "@app/shared/documents/documents.component";
import { EbQrGroupe } from "@app/classes/qrGroupe";
import { QrConsolidationService } from "@app/services/qrConsolidation.service";
import { EbQrGroupeProposition } from "@app/classes/qrGroupeProposition";
import { StepUnitComponent } from "../creation-form/components/step-unit/step-unit.component";
import { EbParty } from "@app/classes/party";
import { WebSocketService } from "@app/services/web-socket.service";
import { ResultEntity } from "@app/classes/ResultEntity";
import { SocketSessionData } from "@app/classes/socketSessionData";
import { BnNgIdleService } from "bn-ng-idle"; // import it to your component
import { UserService } from "@app/services/user.service";
import { StepConsolidationComponent } from "../creation-form/components/step-consolidation/step-consolidation.component";
import { environment } from "@src/environments/environment";
import { DocumentTemplatesService } from "@app/services/document-templates.service";
import { TemplateGenParamsDTO } from "@app/classes/TemplateGenParamsDTO";

@Component({
	selector: "app-demande-visualisation",
	templateUrl: "./demande-visualisation.component.html",
	styleUrls: [
		"./demande-visualisation.component.scss",
		"./header/demande-visualisation-header.component.scss",
	],
})
export class DemandeVisualisationComponent extends ConnectedUserComponent implements OnInit {
	supportMail = environment.supportMail;
	demande: EbDemande;
	demandeNum: number;
	module: number;
	loading = true;
	showInfoAboutTransport: boolean;
	hasContributionAccess: boolean;

	private timerSubscription: any;

	selectedStep: string = QrCreationStep.EXPEDITION;

	nbColTexteditor: string = " ";
	nbColListing: string = " ";

	IDChatComponent = IDChatComponent;
	Modules = Modules;
	Statique = Statique;

	modalExportPdfVisible: boolean = false;
	modalFieldMandatory: Boolean = false;
	isMandatory: boolean = false;

	selectedUser: EbUser = null;
	obj: any;

	listTypeDocuments = new Array<EbTypeDocuments>();

	@Output()
	updateCarrierChoiceEmitter = new EventEmitter<void>();

	@ViewChild("demandeCreationForm", { static: false })
	demandeCreationForm: DemandeCreationFormComponent;
	@ViewChild("chatPanelTransportComponent", { static: false })
	chatPanelTransportComponent: ChatPanelComponent;
	@ViewChild("chatPanelPricingComponent", { static: false })
	chatPanelPricingComponent: ChatPanelComponent;
	@ViewChild("popupAssocierDeclarationDouane", { static: true })
	popupAssocierDeclarationDouane: PopupAssocierDeclarationDouaneComponent;
	@ViewChild("documentsCmp", { static: false })
	documentsComponent: DocumentsComponent;

	@ViewChild(StepUnitComponent, { static: true })
	stepUnit: StepUnitComponent;

	// edit management
	readOnly: boolean = true;
	requiredReadOnly: boolean = true;
	EXPORT_CONTROL_STATUT_LICENCE_NOK = "License NOK";
	ExportControlStatut = ExportControlStatut;

	// is locked by others
	isEditAvailable: boolean = false;
	isLockedByMe: boolean = false;
	pricingEditSubsription: Subscription;
	ebUser: EbUser = null;
	blockedByLabel: string;
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		let labelTr = this.module == Modules.PRICING ? "MODULES.PRICING" : "MODULES.TM",
			path =
				this.module == Modules.PRICING
					? "/app/pricing/dashboard"
					: "/app/transport-management/dashboard",
			detailsPath =
				this.module == Modules.PRICING
					? "/app/pricing/details"
					: "/app/transport-management/details";

			let pathTrConsolidated = "/app/pricing/details/";

			let items = [
				{
					icon: "my-icon-order ",
					label: labelTr,
					path: path,
				},
				{
					label: this.demande.refTransport,
					path: detailsPath + this.demande.ebDemandeNum,
				},
			];

			if(!this.isCarrier() && this.module == Modules.PRICING && this.demande.xEbDemandeGroup)
				items.splice(1, 0, {
					label: this.translate.instant("PRICING_BOOKING.CONSOLIDATION"),
					path:	pathTrConsolidated  + this.demande.xEbDemandeGroup,
				});

		return items;
	}

	constructor(
		protected authenticationService: AuthenticationService,
		protected router: Router,
		protected route: ActivatedRoute,
		protected pricingService: PricingService,
		protected globalService: GlobalService,
		protected headerService: HeaderService,
		protected translate: TranslateService,
		private modalService: ModalService,
		protected messageService: MessageService,
		protected qrConsolidation: QrConsolidationService,
		protected wsocket: WebSocketService,
		private bnIdle: BnNgIdleService,
		protected userService: UserService,
		protected documentTemplatesService: DocumentTemplatesService
	) {
		super(authenticationService);
	}

	ngOnInit() {
		this.module = Statique.getModuleNumFromUrl(this.router.url);
		this.route.params.subscribe((params: Params) => {
			let demandeNum = +params["ebDemandeNum"];
			this.loadDemande(demandeNum);
		});

		this.showInfoAboutTransport = this.module == Modules.TRANSPORT_MANAGEMENT;

		// contribution access rights to module
		if (this.userConnected.listAccessRights && this.userConnected.listAccessRights.length > 0) {
			this.hasContributionAccess = !this.userConnected.listAccessRights.find((access) => {
				if (access.ecModuleNum == this.module && access.accessRight != AccessRights.CONTRIBUTION)
					return true;
			});
		} else {
			this.hasContributionAccess = false;
		}
		this.setReadOnly();
	}

	getMarchandises() {
		this.pricingService.getListMarchandise(this.demande.ebDemandeNum).subscribe((data) => {
			this.demande.oldMarchandises = data;
		});
	}

	addPricingWebSocketListener() {
		this.pricingEditSubsription = this.wsocket.pricingSubjectSubscriber.subscribe(
			(data: SocketSessionData) => {
				if (!data || data.recordId != this.demande.ebDemandeNum) return;

				if (
					Statique.getUserNumFromSessionId(data.sessionId) == Statique.getWsSessionIdUserNum() ||
					!data.pricingDetailsLocked
				) {
					this.isEditAvailable = true;
					if (
						data.free &&
						Statique.getUserNumFromSessionId(data.sessionId) == Statique.getWsSessionIdUserNum()
					) {
						this.isLockedByMe = false;
					} else if (data && data.pricingDetailsLocked) this.isLockedByMe = true;
					else this.isLockedByMe = false;
				} else if (data.pricingDetailsLocked) {
					if (data.sessionId != null) {
						var ebuserfromSesssion = data.sessionId.split("-");
						var ebUserNum: number;
						if (ebuserfromSesssion[0] != "") ebUserNum = Number(ebuserfromSesssion[0]);
						else ebUserNum = Number("-" + ebuserfromSesssion[1]);

						this.userService.findUserById(ebUserNum).subscribe((data) => {
							this.blockedByLabel =
								" " +
								this.translate.instant("PRICING_BOOKING.TR_BLOCKED_BY") +
								" " +
								data.nomPrenom;
						});
					}
					this.isEditAvailable = data.free;
					this.isLockedByMe = false;
				}

				this.setReadOnly();
			}
		);
		this.wsocket.sendPricingMessage(this.demande.ebDemandeNum);
	}

	lockUnlockData(lock: boolean, free: boolean = false) {
		this.wsocket.sendPricingMessage(this.demande.ebDemandeNum, lock, free);
	}

	setReadOnly() {
		this.readOnly = true;
		this.requiredReadOnly = true;
		if (this.hasContributionAccess && this.isLockedByMe) {
			this.readOnly = false;
			if (this.module == Modules.PRICING) this.requiredReadOnly = false;
		}
	}

	updateChatEmitter(ebChat: EbChat) {
		if (
			this.chatPanelPricingComponent &&
			(!ebChat.idChatComponent || ebChat.idChatComponent == IDChatComponent.PRICING)
		)
			this.chatPanelPricingComponent.insertChat(ebChat);
		else if (
			this.chatPanelTransportComponent &&
			(!ebChat.idChatComponent || ebChat.idChatComponent == IDChatComponent.TRANSPORT_MANAGEMENT)
		)
			this.chatPanelTransportComponent.insertChat(ebChat);
	}

	@HostListener("window:beforeunload", ["$event"])
	ngOnDestroy($event = null) {
		if (this.pricingEditSubsription) {
			this.wsocket.sendPricingMessage(this.demande.ebDemandeNum);
			this.pricingEditSubsription.unsubscribe();
		}
	}

	confirmPendingDemande($event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);

		let isConfirmCarrier =
			this.demandeCreationForm.isStepEnteredWorkflow(QrCreationStep.PRICING) &&
			!this.demandeCreationForm.isForGrouping &&
			!!this.demandeCreationForm.confirmableCarrier;

		let _ebDemande = this.demandeCreationForm.prepareEbDemandeForSave(isConfirmCarrier, false);

		let md = this.demandeCreationForm.isTF ? Modules.TRANSPORT_MANAGEMENT : Modules.PRICING;
		_ebDemande.module = md;

		let num = Statique.getInnerObjectStr(_ebDemande, "ebQrGroupe.ebQrGroupeNum");
		if (num) {
			_ebDemande.ebQrGroupe = new EbQrGroupe();
			_ebDemande.ebQrGroupe.ebQrGroupeNum = num;
		}

		this.pricingService.confirmPendingDemande(_ebDemande).subscribe(
			(res) => {
				this.demande = _ebDemande;
				requestProcessing.afterGetResponse($event);
				Statique.refreshComponent(this.router, null, true);
				this.demandeCreationForm.showSuccess();
			},
			(err) => {
				requestProcessing.afterGetResponse($event);
				this.demandeCreationForm.showError();
			}
		);
	}
	callback() {
		this.lockUnlockData(false);
	}
	saveUnits() {
		this.demandeCreationForm.stepUnit.goodsInformation.updateUnitInfos();
	}
	saveAllInformations($event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);

		let isConfirmCarrier =
			this.demandeCreationForm.isStepEnteredWorkflow(QrCreationStep.PRICING) &&
			!this.demandeCreationForm.isForGrouping &&
			!!this.demandeCreationForm.confirmableCarrier;

		let _ebDemande = this.demandeCreationForm.prepareEbDemandeForSave(false, false);

		let md = this.demandeCreationForm.isTF ? Modules.TRANSPORT_MANAGEMENT : Modules.PRICING;
		_ebDemande.module = md;
		if (this.selectedStep == QrCreationStep.EXPEDITION) {
			_ebDemande.typeData = TypeDataEbDemande.EXPEDITION_INFORMATION;
		} else if (this.selectedStep == QrCreationStep.REFERENCE) {
			_ebDemande.typeData = TypeDataEbDemande.REFERENCE_INFORMATION;
		} else if (this.selectedStep == QrCreationStep.UNITS) {
			_ebDemande.typeData = TypeDataEbDemande.GOODS_INFORMATION;
		}
		if (this.demande.ebQrGroupe != null) {
			let listPropos = this.demande.ebQrGroupe.listPropositions;
			_ebDemande.ebQrGroupe = new EbQrGroupe();
			_ebDemande.ebQrGroupe.listPropositions = listPropos;
			if (_ebDemande.ebQrGroupe.listPropositions[0].listEbDemande) {
				_ebDemande.ebQrGroupe.listPropositions[0].listEbDemande.forEach((value, index2) => {
					let demandeLite = new EbDemande();
					demandeLite.constructorLite(
						_ebDemande.ebQrGroupe.listPropositions[0].listEbDemande[index2]
					);
					_ebDemande.ebQrGroupe.listPropositions[0].listEbDemande[index2] = demandeLite;
				});
			}
		}
		this.pricingService.saveAllInformations(_ebDemande).subscribe(
			() => {
				requestProcessing.afterGetResponse($event);
				this.demandeCreationForm.showSuccess();
				Statique.refreshComponent(this.router, this.router.url, true);
			},
			(err) => {
				requestProcessing.afterGetResponse($event);
				this.demandeCreationForm.showError();
			}
		);
		this.demandeCreationForm.stepConsolidation.verifyCheckedListDemande();
		this.demandeCreationForm.stepConsolidation.checkEvent.emit(true);
	}

	get isPendingStatus() {
		return this.demande.xEcStatut == DemandeStatus.PND;
	}
	get isConfirmedStatus() {
		//Si le statut d'un dossier pricing est confirmed alors on a pas le droit de faire des modifs sur la section shipping information
		if (this.demande.xEcStatut >= 11 && QrCreationStep.EXPEDITION) {
			this.requiredReadOnly = true;
			return true;
		}
		return false;
	}
	calculInitialSaving(demande: EbDemande) {
		let listSelectedDemandeNumArray = demande.ebQrGroupe.listPropositions[0].listSelectedDemandeNum.split(
			":"
		);
		demande.ebQrGroupe.listPropositions[0].listEbDemande.forEach((d) => {
			if (d.isChecked) {
				if (demande.ebQrGroupe.ventilation == Ventilation.EQUITABLE) {
					d.saving = demande.saving / ((listSelectedDemandeNumArray.length - 1) / 2);
				} else if (demande.ebQrGroupe.ventilation == Ventilation.TAXABLE_WEIGHT) {
					d.saving = (demande.saving * d.totalTaxableWeight) / demande.totalTaxableWeight;
				} else if (demande.ebQrGroupe.ventilation == Ventilation.QUANTITY) {
					d.saving = (demande.saving * d.totalNbrParcel) / demande.totalNbrParcel;
				}
			}
		});
		return demande.ebQrGroupe.listPropositions[0].listEbDemande;
	}

	private getActionButtons(): Array<HeaderInfos.ActionButton> {
		return [
			{
				label: "GENERAL.EDIT",
				btnClass: "btn-tertiary",
				icon: "fa fa-pencil",
				action: ($event) => {
					this.lockUnlockData(true);
					this.bnIdle
						.startWatching(Statique.timedOutInMin * 60)
						.subscribe((isTimedOut: boolean) => {
							if (isTimedOut) {
								this.lockUnlockData(false);
							}
						});
				},
				displayCondition: () => {
					return (
						this.isEditAvailable &&
						!this.isLockedByMe &&
						(this.module == Modules.PRICING || this.module == Modules.TRANSPORT_MANAGEMENT) &&
						(this.isChargeur ||
							this.isControlTower ||
							((this.isTransporteur || this.isTransporteurBroker) &&
								(this.selectedStep == QrCreationStep.INFOS_ABOUT_TRANSPORT ||
									this.selectedStep == QrCreationStep.UNITS)))
					);
				},
				enableCondition: () => {
					return this.isEditAvailable;
				},
			},
			{
				btnClass: "btn-tertiary",
				icon: "fa fa-lock",
				action: ($event) => {},
				editLabel: () => {
					return this.blockedByLabel;
				},
				displayCondition: () => {
					return (
						!this.isEditAvailable &&
						!this.isLockedByMe &&
						(this.module == Modules.PRICING || this.module == Modules.TRANSPORT_MANAGEMENT) &&
						(this.isChargeur || this.isControlTower)
					);
				},
				enableCondition: () => {
					return false;
				},
			},
			{
				label: "GENERAL.UNLOCK",
				btnClass: "btn-tertiary",
				icon: "fa fa-unlock",
				action: ($event) => {
					this.lockUnlockData(false, true);
				},
				displayCondition: () => {
					return (
						this.isLockedByMe &&
						(this.module == Modules.PRICING || this.module == Modules.TRANSPORT_MANAGEMENT) &&
						(this.isChargeur ||
							this.isControlTower ||
							(((this.isTransporteur || this.isTransporteurBroker) &&
								this.selectedStep == QrCreationStep.INFOS_ABOUT_TRANSPORT) ||
								this.selectedStep == QrCreationStep.UNITS))
					);
				},
			},
			{
				label: "PRICING_BOOKING.CONFIRM_PENDING_TR",
				btnClass: "btn-tertiary",
				icon: "fa fa-check",
				action: ($event) => {
					this.confirmPendingDemande($event);
				},
				displayCondition: () => {
					return (
						this.isLockedByMe &&
						this.module == Modules.PRICING &&
						this.isPendingStatus &&
						(this.isChargeur || this.isControlTower)
					);
				},
				enableCondition: () => {
					return (
						this.demandeCreationForm &&
						this.demandeCreationForm.checkGlobalValidity() &&
						!this.demandeCreationForm.draftMode &&
						(this.demandeCreationForm.isPendingDraft || !this.demandeCreationForm.isForGrouping)
					);
				},
			},
			{
				label: "PRICING_BOOKING.DUPLICATE",
				icon: "fa fa-files-o",
				btnClass: "btn-tertiary",
				action: this.duplicateDemandesAction.bind(this),
				displayCondition: () => {
					return (
						!this.isPrestataire &&
						((this.module == Modules.PRICING &&
							this.userConnected.hasContribution(Modules.PRICING)) ||
							(this.module == Modules.TRANSPORT_MANAGEMENT &&
								this.userConnected.hasContribution(Modules.TRANSPORT_MANAGEMENT)))
					);
				},
			},

			{
				label: "PRICING_BOOKING.SAVE",
				btnClass: "btn-tertiary",
				icon: "fa fa-save",
				action: ($event) => {
					this.saveAllInformations($event);
				},
				displayCondition: () => {
					return (
						this.isLockedByMe &&
						this.module == Modules.PRICING &&
						this.isPendingStatus &&
						(this.isChargeur || this.isControlTower)
					);
				},
			},
			{
				label: "PRICING_BOOKING.ASK_FOR_TRANSPORT_RESPONSIBILITY",
				btnClass: "btn-tertiary",
				icon:
					this.demande &&
					this.demande.ebDemandeNum &&
					this.demande.askForTransportResponsibility &&
					!this.demande.provideTransport
						? "fa fa-paper-plane"
						: this.demande.provideTransport
						? "fa fa-check"
						: "",
				action: ($event) => {
					this.demanderPriseEnCharge($event);
				},
				displayCondition: () => {
					return (
						(this.module == Modules.TRANSPORT_MANAGEMENT ||
							(this.module == Modules.PRICING &&
								this.demande &&
								(this.demande.xEcStatut >= DemandeStatus.WPU &&
									this.demande.xEcStatut != CancelStatus.CANCELLED))) &&
						(this.isControlTower || this.isChargeur)
					);
				},
				enableCondition: () => {
					return (
						this.demande && this.demande.ebDemandeNum && !this.demande.askForTransportResponsibility
					);
				},
			},
			{
				label: "PRICING_BOOKING.PROVIDE_TRANSPORT",
				btnClass: "btn-tertiary",
				icon:
					this.demande && this.demande.ebDemandeNum && this.demande.provideTransport
						? "fa fa-check"
						: "",
				action: ($event) => {
					this.priseEnCharge($event);
				},
				displayCondition: () => {
					return (
						(this.module == Modules.TRANSPORT_MANAGEMENT ||
							(this.module == Modules.PRICING &&
								this.demande &&
								(this.demande.xEcStatut >= DemandeStatus.WPU &&
									this.demande.xEcStatut != CancelStatus.CANCELLED))) &&
						this.demande.statutStr != Statique.getStatusLibelle(DemandeStatus.NOT_AWARDED) 
						&& (this.isControlTower || this.isTransporteur || this.isTransporteurBroker)
					);
				},
				enableCondition: () => {
					return this.demande && this.demande.ebDemandeNum && !this.demande.provideTransport;
				},
			},
			{
				label: "CHANEL_PB_DELIVERY.DELETE_DELIVERY",
				icon: "fa fa-trash",
				btnClass: "btn-tertiary",
				action: (jsEvent: any) => {
					this.deleteEbDemande(jsEvent);
				},
				displayCondition: () => {
					return (
						this.isLockedByMe &&
						(this.module == Modules.TRANSPORT_MANAGEMENT ||
							(this.module == Modules.PRICING &&
								this.demande &&
								(this.demande.xEcStatut >= DemandeStatus.WPU &&
									this.demande.xEcStatut != CancelStatus.CANCELLED))) &&
						this.isControlTower &&
						this.userConnected.ebUserNum == -1
					);
				},
			},
			{
				label: "PRICING_BOOKING.CANCEL_CONSOLIDATION",
				icon: "fa fa-times",
				btnClass: "btn-tertiary",
				action: (jsEvent: any) => {
					this.cancelConsolidation(jsEvent);
				},
				displayCondition: () => {
					if (!this.demande) return false;

					// conditions sur le module, access & role de l'utilisateur
					let isQrConsolidation = this.demande.xEcNature == NatureDemandeTransport.CONSOLIDATION;
					let condModule = this.module == Modules.PRICING;
					let condAccess = this.userConnected.hasContribution(this.module) && this.isLockedByMe;
					let condRole = this.isChargeur || this.isControlTower;
					let isNotConfirmed = this.demande.xEcStatut < DemandeStatus.WPU;

					return (
						isQrConsolidation &&
						condModule &&
						condAccess &&
						condRole &&
						!this.isCancelled &&
						isNotConfirmed
					);
				},
			},
			{
				label: "GENERAL.CANCEL",
				icon: "fa fa-times",
				btnClass: "btn-tertiary",
				action: (jsEvent: any) => {
					this.cancelQuotation(jsEvent);
				},
				displayCondition: () => {
					if (!this.demande) return false;

					// conditions sur le module, access & role de l'utilisateur
					let condModule =
						this.module == Modules.PRICING || this.module == Modules.TRANSPORT_MANAGEMENT;
					let condAccess = this.userConnected.hasContribution(this.module) && this.isLockedByMe;
					let condRole = this.isChargeur || this.isControlTower;

					return condModule && condAccess && condRole && !this.isCancelled && !this.isPendingStatus;
				},
			},
			{
				label: "CUSTOM.LINK_CUSTOM_DECLARATION",
				icon: "fa fa-file-o",
				action: () => {
					this.popupAssocierDeclarationDouane.open(this.demande, this.module);
				},
				displayCondition: () => {
					return (
						this.userConnected.hasAccess(Modules.CUSTOM) &&
						!this.isCancelled &&
						!this.isPendingStatus
					);
				},
				enableCondition: () => {
					return (
						this.demande != null &&
						this.demande.listMarchandises != null &&
						this.demande.listMarchandises.length > 0
					);
				},
			},
			{
				label: "PRICING_BOOKING.ACCUSER_RECEPTION",
				icon: "fa fa-check",
				action: ($event) => {
					let requestProcessing = new RequestProcessing();
					requestProcessing.beforeSendRequest($event);
					this.pricingService.acknowledgeTdc(this.demande.ebDemandeNum).subscribe(
						(ebChat: EbChat) => {
							requestProcessing.afterGetResponse(event);
							this.demande.tdcAcknowledge = true;
							this.demande.tdcAcknowledgeDate = new Date();
							this.updateChatEmitter(ebChat);
						},
						(error) => {
							requestProcessing.afterGetResponse(event);
							console.error(error);
							this.messageService.add({
								severity: "error",
								summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
								detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
							});
						}
					);
				},
				displayCondition: () => {
					return (
						this.demande &&
						!this.demande.tdcAcknowledge &&
						this.demande.xEcStatut &&
						this.demande.xEcStatut === 1 &&
						this.userConnected.role &&
						this.userConnected.role === UserRole.CONTROL_TOWER
					);
				},
			},
			{
				label: "TRANSPORT_MANAGEMENT.ADD_QUOTATION",
				icon: "fa fa-plus-circle",
				action: () => {
					this.addEditQuotation();
				},
				displayCondition: () => {
					const status: number = this.demande.xEcStatut;

					const visibCond =
						this.module == Modules.PRICING &&
						this.userConnected.hasContribution(Modules.PRICING) &&
						(this.isTransporteur || this.isTransporteurBroker || this.isPrestataire) &&
						status < DemandeStatus.FINAL_CHOICE &&
						!this.isCancelled;

					return visibCond;
				},
			},
			//TODO MEP-ce bout de code devra être réactivé après MEP
			/*
			{
				label: "Confirm pickup",
				icon: "fa fa-plus-circle",
				action: () => {
					let moduleRoute =
						Statique.moduleNamePath[Statique.getModuleNameByModuleNum(Modules.TRACK)];
					let params = { queryParams: { filterByDemande: this.demande.ebDemandeNum } };
					params.queryParams["isUpdatePsl"] = 0;

					this.router.navigate(["/app/" + moduleRoute + "/dashboard/"], params);

					Statique.setCurrentEbDemande(this.demande);
				},
				displayCondition: () => {
					if (!this.demande) return false;

					let status: number = this.demande.xEcStatut;
					return (
						this.module == Modules.TRANSPORT_MANAGEMENT &&
						!this.isBroker &&
						this.userConnected.hasContribution(Modules.TRANSPORT_MANAGEMENT) &&
						window.location.href.indexOf("/details/") >= 0 &&
						status == DemandeStatus.WPU
					);
				},
			},
			*/
			{
				label: "PRICING_BOOKING.TRACK_TRACE",
				icon: "fa fa-refresh",
				action: () => {
					let moduleRoute =
						Statique.moduleNamePath[Statique.getModuleNameByModuleNum(Modules.TRACK)];
					let params = { queryParams: { filterByDemande: this.demande.ebDemandeNum } };
					params.queryParams["isUpdatePsl"] = 1;

					this.router.navigate(["/app/" + moduleRoute + "/dashboard/"], params);

					Statique.setCurrentEbDemande(this.demande);
				},
				displayCondition: () => {
					if (!this.demande) return false;

					let status: number = this.demande.xEcStatut;
					return (
						this.module == Modules.TRANSPORT_MANAGEMENT &&
						!this.isBroker &&
						this.userConnected.hasContribution(Modules.TRANSPORT_MANAGEMENT) &&
						window.location.href.indexOf("/details/") >= 0 &&
						status >= DemandeStatus.WPU &&
						!this.isCancelled &&
						!this.isPendingStatus
					);
				},
			},
			{
				label: "GENERAL.EXPORT",
				icon: "fa fa-file-pdf-o",
				action: () => {
					this.exportPdf();
				},
				displayCondition: () => {
					return this.canShowExportPdfButton();
				},
			},
			{
				label: "PRICING_BOOKING.SAVE",

				icon: "fa fa-save",
				action: (jsEvent) => {
					//Pour enregistrer toute les informations et aussi revaloriser quand on est en statuts inferieur à corfimed.
					this.saveAllInformations(jsEvent);
				},
				displayCondition: () => {
					const demandeStatus = this.demande.xEcStatut < DemandeStatus.FINAL_CHOICE;
					return this.isLockedByMe && !this.isCancelled && !this.isPendingStatus && demandeStatus;
				},
			},
			{
				label: "PRICING_BOOKING.SAVE",

				icon: "fa fa-save",
				action: (jsEvent) => {
					this.saveUnits();
				},
				displayCondition: () => {
					const stepCond = this.selectedStep === QrCreationStep.UNITS;
					const demandeStatus = this.demande.xEcStatut >= DemandeStatus.FINAL_CHOICE;
					return (
						this.isLockedByMe &&
						!this.isCancelled &&
						!this.isPendingStatus &&
						stepCond &&
						demandeStatus
					);
				},
			},
			{
				label: "PRICING_BOOKING.SAVE",

				icon: "fa fa-save",
				action: (jsEvent) => {
					this.saveAllInformations(jsEvent);
				},
				displayCondition: () => {
					const stepCond = this.selectedStep === QrCreationStep.EXPEDITION;
					const demandeStatus = this.demande.xEcStatut >= DemandeStatus.FINAL_CHOICE;
					return (
						this.isLockedByMe &&
						!this.isCancelled &&
						!this.isPendingStatus &&
						!this.isConfirmedStatus &&
						stepCond &&
						demandeStatus
					);
				},
			},
			{
				label: "PRICING_BOOKING.SAVE",
				icon: "fa fa-save",
				action: (jsEvent) => {
					this.editDataEbDemande(jsEvent);
					//this.saveAllInformations(jsEvent);
				},
				displayCondition: () => {
					const stepCond = this.selectedStep === QrCreationStep.REFERENCE;
					const demandeStatus = this.demande.xEcStatut >= DemandeStatus.FINAL_CHOICE;
					const visibCond =
						this.demande.ebDemandeNum &&
						(this.isChargeur || this.isControlTower) &&
						this.hasContributionAccess &&
						!this.isCancelled &&
						!this.isPendingStatus;

					return stepCond && visibCond && this.isLockedByMe && demandeStatus;
				},
			},
			{
				label: "PRICING_BOOKING.SAVE",
				icon: "fa fa-save",
				action: (jsEvent) => {
					//save pour la section transport informations
					this.confirmTransportInfo(jsEvent);
					//this.saveAllInformations(jsEvent);
				},
				displayCondition: () => {
					let stepCond = this.selectedStep === QrCreationStep.INFOS_ABOUT_TRANSPORT;

					let visibCond =
						this.demande.ebDemandeNum &&
						(this.isChargeur ||
							this.isControlTower ||
							this.isTransporteur ||
							this.isTransporteurBroker) &&
						this.hasContributionAccess &&
						!this.isCancelled &&
						!this.isPendingStatus;

					return stepCond && visibCond && this.isLockedByMe && this.isConfirmedStatus;
				},
			},
		];
	}

	duplicateDemandesAction(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		let searchCriteria = new SearchCriteriaPricingBooking();
		searchCriteria.ebDemandeNum = this.demande.ebDemandeNum;
		searchCriteria.module = this.module;
		searchCriteria.withListMarchandise = true;
		searchCriteria.ebUserNum = this.userConnected.ebUserNum;

		this.pricingService.getEbDemande(searchCriteria).subscribe((demande: EbDemande) => {
			requestProcessing.afterGetResponse(event);
			if (this.userConnected.role == UserRole.CHARGEUR) {
				demande.user = this.userConnected;
				this.isChargeur = true;
			}
			this.router.navigateByUrl("/app/pricing/creation", { state: demande });
		});
	}

	canShowExportPdfButton() {
		let ebDemande: EbDemande = null;
		if (!(ebDemande = Statique.getCurrentEbDemande())) return false;

		return this.module == Modules.PRICING || this.module == Modules.TRANSPORT_MANAGEMENT;
	}

	deleteEbDemande(jsEvent) {
		let $this = this;
		this.modalService.confirm(
			$this.translate.instant("GENERAL.CONFIRMATION"),
			$this.translate.instant("PRICING_BOOKING.DELETE_CONFIRM_MSG"),
			function() {
				this.pricingService.deleteEbDemande([this.demande.ebDemandeNum]).subscribe((res) => {
					if (res) {
						let curUrl = this.router.url;
						curUrl = curUrl.substr(0, curUrl.indexOf("/details"));
						this.router.navigateByUrl(curUrl + "/dashboard");
					}
				});
			}.bind(this)
		);
	}

	cancelConsolidation(jsEvent) {
		let $this = this;
		this.modalService.confirm(
			$this.translate.instant("GENERAL.CONFIRMATION"),
			$this.translate.instant("PRICING_BOOKING.CANCEL_CONSOLIDATION_CONFIRM_MSG"),
			function() {
				let requestProcessing = new RequestProcessing();
				requestProcessing.beforeSendRequest(jsEvent);
				$this.qrConsolidation.cancelConsolidation($this.demande.ebDemandeNum).subscribe((res) => {
					$this.demande.xEcCancelled = CancelStatus.CANCELLED;
					requestProcessing.afterGetResponse(jsEvent);
				});
			}
		);
	}

	loadDemande(demandeNum: number) {
		this.demandeNum = demandeNum;

		localStorage.removeItem("xEcStatut");
		localStorage.removeItem("canShowAddQuotation");
		localStorage.removeItem("currentEbDemande");

		if (this.demandeNum) {
			let searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();
			searchCriteriaPricingBooking.ebDemandeNum = this.demandeNum;
			// searchCriteriaPricingBooking.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			searchCriteriaPricingBooking.module = this.module;
			searchCriteriaPricingBooking.roleTransporteur = true;
			searchCriteriaPricingBooking.ebUserNum = this.userConnected.ebUserNum;
			searchCriteriaPricingBooking.setUser(this.userConnected);

			this.pricingService
				.getEbDemande(searchCriteriaPricingBooking)
				.subscribe((data: EbDemande) => {
					this.demande = data;
					// Auto redirect to pricing if not yet at module TM
					if (data) {
						this.getMarchandises();
					}

					if (
						(this.demande.xEcStatut == null || this.demande.xEcStatut < DemandeStatus.WPU) &&
						this.module === Modules.TRANSPORT_MANAGEMENT
					) {
						console.info("Auto redirect to pricing module");
						this.router.navigate(["/app/pricing/details/", this.demandeNum]);
					}

					this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());
					this.headerService.registerActionButtons(this.getActionButtons());

					Statique.setCurrentEbDemande(data);
					this.demande.listTransporteurs = [];

					if (this.demande.curentTimeRemainingInMilliSecond > 0) {
						this.timerSubscription = interval(1000).subscribe(
							function(x) {
								this.demande.curentTimeRemainingInMilliSecond -= 1000;

								if (this.demande.curentTimeRemainingInMilliSecond <= 0) {
									this.demande.curentTimeRemainingInMilliSecond = 0;
								}
							}.bind(this)
						);
					}

					this.demande.exEbDemandeTransporteur = new ExEbDemandeTransporteur();
					if (
						this.userConnected.role == UserRole.CHARGEUR &&
						this.demande.exEbDemandeTransporteurs &&
						this.demande.exEbDemandeTransporteurs.length == 1
					) {
						this.demande.exEbDemandeTransporteur.copy(this.demande.exEbDemandeTransporteurs[0]);
					} else if (
						this.userConnected.role == UserRole.CONTROL_TOWER &&
						this.demande.listDemandeQuote &&
						this.demande.listDemandeQuote.length == 1
					) {
						this.demande.exEbDemandeTransporteur.copy(this.demande.listDemandeQuote[0]);
					}

					if (this.demande.xEcStatut == DemandeStatus.PND) {
						this.requiredReadOnly = false;
						this.readOnly = false;
					}

					this.loading = false;
					this.addPricingWebSocketListener();
				});
		}
	}

	exportPdf() {
		this.modalExportPdfVisible = true;
		//this.exportShippingInformation();
	}

	// soufiane
	cancelQuotation(jsEvent) {
		let $this = this;
		if (this.demande == null || this.demande.ebDemandeNum == null) return;

		$this.modalService.confirm(
			$this.translate.instant("GENERAL.CONFIRMATION"),
			$this.translate.instant("PRICING_BOOKING.CANCEL_COTATION_CONFIRM_MSG"),
			function() {
				let requestProcessing = new RequestProcessing();
				requestProcessing.beforeSendRequest(jsEvent);

				$this.pricingService
					.cancelQuotation($this.demande.ebDemandeNum)
					.subscribe((ebChat: EbChat) => {
						if (ebChat) {
							$this.demande.xEcCancelled = CancelStatus.CANCELLED;
							Statique.setCurrentEbDemande($this.demande);
							ebChat.module = $this.module;
							ebChat.idChatComponent =
								ebChat.module == Modules.PRICING
									? IDChatComponent.PRICING
									: IDChatComponent.TRANSPORT_MANAGEMENT;
							$this.updateChatEmitter(ebChat);
						}
						requestProcessing.afterGetResponse(jsEvent);
					});
			}.bind($this)
		);
	}

	addEditQuotation(index: number = null) {
		this.selectedStep = QrCreationStep.PRICING;
		if (!Statique.isDefined(index)) {
			let i,
				n = this.demande.exEbDemandeTransporteurs.length;
			for (i = 0; i < n; i++) {
				if (
					this.demande.exEbDemandeTransporteurs[i].status == CarrierStatus.REQUEST_RECEIVED &&
					((this.userConnected.role == UserRole.PRESTATAIRE &&
						this.demande.exEbDemandeTransporteurs[i].xTransporteur.ebUserNum ==
							this.userConnected.ebUserNum) ||
						(this.selectedUser != null &&
							this.demande.exEbDemandeTransporteurs[i].xTransporteur.ebUserNum ==
								this.selectedUser.ebUserNum))
				) {
					let quote = new ExEbDemandeTransporteur();
					quote.copy(this.demande.exEbDemandeTransporteurs[i]);
					let _newDemandeQuote = (this.demandeCreationForm.stepPricing.newDemandeQuote = quote);
					_newDemandeQuote.xTransporteur.nom = (this.selectedUser || this.userConnected).nom;
					_newDemandeQuote.xTransporteur.prenom = (this.selectedUser || this.userConnected).prenom;
					_newDemandeQuote.xTransporteur.email = (this.selectedUser || this.userConnected).email;
					_newDemandeQuote.ctCommunity = (this.selectedUser || this.userConnected).isCt;
					_newDemandeQuote.xEbEtablissement = new EbEtablissement();
					_newDemandeQuote.xEbEtablissement.ebEtablissementNum = (
						this.selectedUser || this.userConnected
					).ebEtablissement.ebEtablissementNum;
					_newDemandeQuote.xEbCompagnie = new EbCompagnie();
					_newDemandeQuote.xEbCompagnie.ebCompagnieNum = (
						this.selectedUser || this.userConnected
					).ebCompagnie.ebCompagnieNum;
					break;
				}
			}
			if (!Statique.isDefined(i) || i >= n) {
				let _user: EbUser = new EbUser();
				if (this.userConnected.role == UserRole.PRESTATAIRE) _user = this.userConnected;
				// else if (this.selectedUser) _user = this.selectedUser;

				let _newDemandeQuote = (this.demandeCreationForm.stepPricing.newDemandeQuote = new ExEbDemandeTransporteur());
				_newDemandeQuote.xTransporteur = new EbUser();
				_newDemandeQuote.xTransporteur.ebUserNum = _user.ebUserNum;
				_newDemandeQuote.xTransporteur.nom = _user.nom;
				_newDemandeQuote.xTransporteur.prenom = _user.prenom;
				_newDemandeQuote.xTransporteur.email = _user.email;
				_newDemandeQuote.ctCommunity = _user.isCt;

				_newDemandeQuote.xEbEtablissement = new EbEtablissement();
				_newDemandeQuote.xEbEtablissement.ebEtablissementNum =
					_user.ebEtablissement.ebEtablissementNum;

				_newDemandeQuote.xEbCompagnie = new EbCompagnie();
				_newDemandeQuote.xEbCompagnie.ebCompagnieNum = _user.ebCompagnie.ebCompagnieNum;

				if (this.demande.ebPartyOrigin && this.demande.ebPartyOrigin.city)
					_newDemandeQuote.cityPickup = "" + this.demande.ebPartyOrigin.city;
				if (
					this.demande.ebPartyOrigin &&
					this.demande.ebPartyOrigin.xEcCountry &&
					this.demande.ebPartyOrigin.xEcCountry.ecCountryNum
				) {
					_newDemandeQuote.xEcCountryPickup = new EcCountry();
					_newDemandeQuote.xEcCountryPickup.ecCountryNum = this.demande.ebPartyOrigin.xEcCountry.ecCountryNum;
				}
				if (this.demande.ebPartyDest && this.demande.ebPartyDest.city)
					_newDemandeQuote.cityDelivery = "" + this.demande.ebPartyDest.city;
				if (
					this.demande.ebPartyDest &&
					this.demande.ebPartyDest.xEcCountry &&
					this.demande.ebPartyDest.xEcCountry.ecCountryNum
				) {
					_newDemandeQuote.xEcCountryDelivery = new EcCountry();
					_newDemandeQuote.xEcCountryDelivery.ecCountryNum = this.demande.ebPartyDest.xEcCountry.ecCountryNum;
				}
			}
		} else if (
			this.demande.exEbDemandeTransporteurs &&
			this.demande.exEbDemandeTransporteurs.length > 0
		) {
			let quote = new ExEbDemandeTransporteur();
			quote.copy(this.demande.exEbDemandeTransporteurs[index]);
			this.demandeCreationForm.stepPricing.newDemandeQuote = quote;
		}
	}

	editDataEbDemande(jsEvent) {
		if (this.demande && this.demande.ebDemandeNum) {
			let requestProcessing = new RequestProcessing();
			requestProcessing.beforeSendRequest(jsEvent);

			// on ne copie ici que les informations qui sont modifiables
			let demande = new EbDemande(true);

			// pour eviter null pointeur exception au niveau du controlRuleService.processControlRules

			demande.xEbCompagnie = this.demande.xEbCompagnie;

			demande.ebDemandeNum = this.demande.ebDemandeNum;
			demande.xEbTypeRequest = this.demande.xEbTypeRequest;
			demande.typeRequestLibelle = this.demande.typeRequestLibelle;
			demande.commentChargeur = this.demande.commentChargeur;
			demande.customerReference = this.demande.customerReference;
			demande.xEbUserObserver = this.demande.xEbUserObserver;
			if (this.demande.xEbCostCenter && this.demande.xEbCostCenter.ebCostCenterNum)
				demande.xEbCostCenter = this.demande.xEbCostCenter;
			if (this.demande.ebPartyOrigin && this.demande.ebPartyOrigin.ebPartyNum)
				demande.ebPartyOrigin = this.demande.ebPartyOrigin;
			if (this.demande.ebPartyDest && this.demande.ebPartyDest.ebPartyNum)
				demande.ebPartyDest = this.demande.ebPartyDest;
			if (this.demande.ebPartyNotif && this.demande.ebPartyNotif.ebPartyNum)
				demande.ebPartyNotif = this.demande.ebPartyNotif;
			demande.customFields = this.demande.listCustomsFields
				? JSON.stringify(this.demande.listCustomsFields)
				: null;
			demande.insurance = this.demande.insurance;
			demande.insuranceValue = this.demande.insuranceValue;
			demande.xEcTypeCustomBroker = this.demande.xEcTypeCustomBroker;

			if (this.demande.xecCurrencyInvoice && this.demande.xecCurrencyInvoice.ecCurrencyNum)
				demande.xecCurrencyInvoice = this.demande.xecCurrencyInvoice;

			demande.listCategories = new Array<EbCategorie>();
			demande.listCategoriesStr = null;
			demande.listLabels = null;

			if (this.demande.xEbUserOrigin && this.demande.xEbUserOrigin.ebUserNum) {
				demande.xEbUserOrigin = EbUser.createUser(
					null,
					this.demande.xEbUserOrigin.ebUserNum,
					null,
					this.demande.xEbUserOrigin.nom
				);
			}
			if (this.demande.xEbUserDest && this.demande.xEbUserDest.ebUserNum) {
				demande.xEbUserDest = EbUser.createUser(
					null,
					this.demande.xEbUserDest.ebUserNum,
					null,
					this.demande.xEbUserDest.nom
				);
			}
			if (
				this.demande.xEbUserOriginCustomsBroker &&
				this.demande.xEbUserOriginCustomsBroker.ebUserNum
			) {
				demande.xEbUserOriginCustomsBroker = EbUser.createUser(
					null,
					this.demande.xEbUserOriginCustomsBroker.ebUserNum,
					null,
					this.demande.xEbUserOriginCustomsBroker.nom
				);
			}
			if (
				this.demande.xEbUserDestCustomsBroker &&
				this.demande.xEbUserDestCustomsBroker.ebUserNum
			) {
				demande.xEbUserDestCustomsBroker = EbUser.createUser(
					null,
					this.demande.xEbUserDestCustomsBroker.ebUserNum,
					null,
					this.demande.xEbUserDestCustomsBroker.nom
				);
			}

			demande.listCategories = this.demande.listCategories.map((categorie) => {
				let _categorie: EbCategorie = new EbCategorie();

				_categorie.constructorCopyLite(categorie);

				if (categorie.selectedLabel != null && categorie.selectedLabel.ebLabelNum != null) {
					_categorie.labels.push(categorie.selectedLabel);
				}
				return _categorie;
			});

			demande.typeData = TypeDataEbDemande.REFERENCE_INFORMATION;
			demande.module = this.module;

			if (this.demande.xEcStatut === 1) {
				demande.tdcAcknowledge = false;
				demande.tdcAcknowledgeDate = null;
			}
			demande.refTransport = this.demande.refTransport;

			this.pricingService.updateEbDemande(demande).subscribe(
				(res) => {
					// this.toggleEditData = false;
					if (res && res.ebChat) {
						res.ebChat.idChatComponent = null;
						this.updateChatEmitter(res.ebChat);
					}
					// Force navigation to refresh the page after a save
					let url = this.router.url;
					this.router
						.navigateByUrl("/accueil", { skipLocationChange: true })
						.then(() => this.router.navigate([url]));
					requestProcessing.afterGetResponse(jsEvent);
					this.demandeCreationForm.showSuccess();
				},
				(error) => {
					let title = "";
					this.translate.get("MODAL.ERROR").subscribe((res: string) => {
						title = res;
					});
					this.modalService.error(title, null, function() {});
					requestProcessing.afterGetResponse(jsEvent);
				}
			);
		}
	}
	confirmTransportInfo(jsEvent) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(jsEvent);

		// on envoie juste les données nécessaires à la modification
		this.obj = this.demandeCreationForm.stepInfosAboutTransport.transportInfo.getTransportInformationData();
		let msg = this.translate.instant("TRANSPORT_MANAGEMENT.INFORMATION_ABOUT_TRANSPORT_UPDATED");

		this.pricingService.updateEbDemandeTransportInfos(this.obj, msg).subscribe((res) => {
			this.messageService.add({
				severity: "success",
				summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
				detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
			});
			this.demande.xEbSchemaPsl.listPsl = this.obj.xEbSchemaPsl.listPsl;
			this.demande.mawb = this.obj.mawb;
			this.demande.numAwbBol = this.obj.numAwbBol;
			this.demande.carrierComment = this.obj.carrierComment;
			requestProcessing.afterGetResponse(jsEvent);
			this.updateChatEmitter(res.ebChat);
		});
	}

	get isCancelled() {
		return this.demande.xEcCancelled && this.demande.xEcCancelled == CancelStatus.CANCELLED;
	}

	demanderPriseEnCharge(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		let action = this.userConnected.nomPrenom + " ";
		this.translate.get("TRANSPORT_MANAGEMENT.DEMANDE_PRISE_CHARGE").subscribe((res: string) => {
			action += res;
			action += " " + this.demande.refTransport;
		});
		this.pricingService
			.confirmeAskFortrResponsibility(
				this.demande.ebDemandeNum,
				this.module,
				this.demande.refTransport,
				action
			)
			.subscribe((data: EbChat) => {
				this.demande.askForTransportResponsibility = true;
				this.updateChatEmitter(data);
				requestProcessing.afterGetResponse(event);
				this.headerService.registerActionButtons(this.getActionButtons());
			});
	}

	priseEnCharge(event) {
		let requestProcessing = new RequestProcessing();
		let action = this.userConnected.nom + " " + this.userConnected.prenom + " ";
		this.translate.get("TRANSPORT_MANAGEMENT.PRISE_CHARGE").subscribe((res: string) => {
			action += res;
			action += " " + this.demande.refTransport;
		});
		requestProcessing.beforeSendRequest(event);
		this.pricingService
			.confirmeProvideTransport(
				this.demande.ebDemandeNum,
				this.module,
				this.demande.refTransport,
				action
			)
			.subscribe((data: EbChat) => {
				this.demande.provideTransport = true;
				this.demande.askForTransportResponsibility = true;
				this.updateChatEmitter(data);
				requestProcessing.afterGetResponse(event);
				this.headerService.registerActionButtons(this.getActionButtons());
			});
	}

	onCloseModalExport(event: ModalExportDocByTemplateComponent.Result) {
		this.modalExportPdfVisible = false;

		if (
			event.params &&
			event.params.attachChecked &&
			event.params.selectedTypeDoc != null &&
			event.generatedFile != null
		) {
			this.documentsComponent.uploadFile(
				event.generatedFile,
				event.params.generatedFileName,
				event.params.selectedTypeDoc
			);
		}
		this.onCloseModalFieldMandatory(event.params);
	}
	onCloseModalFieldMandatory(params: TemplateGenParamsDTO) {
		if (!this.isMandatory) {
			this.documentTemplatesService
				.chekedInformationMandatory(params)
				.subscribe((data: Boolean) => {
					this.modalFieldMandatory = data;
				});
		}
	}

	get isShowOptions(): boolean {
		return !!Statique.getInnerObjectStr(
			this,
			"demandeCreationForm.stepExpedition.showAddressPopup"
		);
	}

	isCarrier(): boolean {
		return this.userConnected.role == UserRole.PRESTATAIRE;
	}
}
