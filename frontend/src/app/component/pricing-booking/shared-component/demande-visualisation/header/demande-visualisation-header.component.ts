import { Component, Input, OnInit } from "@angular/core";
import { Statique } from "@app/utils/statique";
import {
	CancelStatus,
	CarrierStatus,
	DemandeStatus,
	Modules,
	ServiceType,
	UserRole,
} from "@app/utils/enumeration";
import { EbUser } from "@app/classes/user";
import { TranslateService } from "@ngx-translate/core";
import { EbDemande } from "@app/classes/demande";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { TypeRequestService } from "@app/services/type-request.service";

@Component({
	selector: "app-demande-visualisation-header",
	templateUrl: "./demande-visualisation-header.component.html",
	styleUrls: ["./demande-visualisation-header.component.scss"],
})
export class DemandeVisualisationHeaderComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	demande: EbDemande;
	@Input()
	module: number;
	@Input()
	userConnected: EbUser;
	@Input()
	hasContributionAccess?: boolean;

	result: any = {
		nbrOfDays: 0,
		nbrOfHours: 0,
		nbrOfMin: 0,
	};
	Modules = Modules;
	Statique = Statique;
	DemandeStatus = DemandeStatus;
	typeBroker: number = ServiceType.BROKER;
	typeTransporteurBroker: number = ServiceType.TRANSPORTEUR_BROKER;
	CancelStatus = CancelStatus;
	UserRole = UserRole;
	CarrierStatus = CarrierStatus;

	constructor(
		private translate?: TranslateService,
		private typeRequestService?: TypeRequestService
	) {
		super();
	}

	ngOnInit() {
		if (this.demande) {
			let criteria = new SearchCriteria();
			criteria.connectedUserNum = this.userConnected.ebUserNum;
			criteria.ebCompagnieNum = this.demande.xEbCompagnie.ebCompagnieNum;
			this.typeRequestService
				.getListTypeRequests(criteria)
				.subscribe((res: Array<EbTypeRequestDTO>) => {
					res.forEach((elem) => {
						if (elem.ebTypeRequestNum === this.demande.xEbTypeRequest) {
							if (elem.sensChronoPricing == 1) {
								//TIME_REMAINING
								const getTimeAgoByHours: number = this.typeRequestService.getTimeAgo(this.demande.dateCreation,true);
								if (getTimeAgoByHours >= elem.delaiChronoPricing) {
									this.result = {
										nbrOfDays: 0,
										nbrOfHours: 0,
										nbrOfMin: 0,
									};
								} else {
									this.result = this.typeRequestService.getTimeRemaining(elem.delaiChronoPricing - getTimeAgoByHours);
								}
							} else {
								//TYPE_SINCE_ISSUANCE
								this.result = this.typeRequestService.getTimeAgo(this.demande.dateCreation);
							}
						}
					});
				});
		}
		if (typeof this.hasContributionAccess == "undefined") this.hasContributionAccess = true;
	}

	// tester s'il faut activer le statut propre à In Process Waiting For Quote
	get isActiveINP(): boolean {
		return !(
			this.demande.xEcStatut >= DemandeStatus.WDCT &&
			(this.userConnected.role == UserRole.PRESTATAIRE &&
				(this.userConnected.service != this.typeBroker ||
					(this.userConnected.service == this.typeTransporteurBroker &&
						Statique.getInnerObject(this, "demande", "xEbUserOriginCustomsBroker", "ebUserNum") !=
							this.userConnected.ebUserNum &&
						Statique.getInnerObject(this, "demande", "xEbUserDestCustomsBroker", "ebUserNum") !=
							this.userConnected.ebUserNum)) &&
				this.demande.exEbDemandeTransporteurFinal &&
				((this.userConnected.superAdmin &&
					this.demande.exEbDemandeTransporteurFinal.ebCompagnie.ebCompagnieNum !=
						this.userConnected.ebCompagnie.ebCompagnieNum) ||
					(!this.userConnected.superAdmin && 
						(
							(this.userConnected.admin&&
								this.demande.exEbDemandeTransporteurFinal.ebEtablissement.ebEtablissementNum !=
							this.userConnected.ebEtablissement.ebEtablissementNum)
							||
							(!this.userConnected.admin && this.userConnected.ebUserNum != this.demande.exEbDemandeTransporteurFinal.ebUserNum
							)
						)

					)
				))
		);
	}

	// tester s'il faut activer le statut propre à Waiting for recommendation
	get isActiveWFREC(): boolean {
		return (
			!(
				this.demande.xEcStatut >= DemandeStatus.WDCT &&
				(this.userConnected.role == UserRole.PRESTATAIRE &&
					(this.userConnected.service != this.typeBroker ||
						(this.userConnected.service == this.typeTransporteurBroker &&
							Statique.getInnerObject(
								this,
								"this.demande",
								"xEbUserOriginCustomsBroker",
								"ebUserNum"
							) != this.userConnected.ebUserNum &&
							Statique.getInnerObject(
								this,
								"this.demande",
								"xEbUserDestCustomsBroker",
								"ebUserNum"
							) != this.userConnected.ebUserNum)) &&
					this.demande.exEbDemandeTransporteurFinal &&
					((this.userConnected.superAdmin &&
						this.demande.exEbDemandeTransporteurFinal.ebCompagnie.ebCompagnieNum !=
							this.userConnected.ebCompagnie.ebCompagnieNum) ||
						(!this.userConnected.superAdmin &&
							this.demande.exEbDemandeTransporteurFinal.ebEtablissement.ebEtablissementNum !=
								this.userConnected.ebEtablissement.ebEtablissementNum)))
			) && this.demande.flagRecommendation
		);
	}

	// tester s'il faut activer le statut propre à Waiting for confirmation
	get isActiveWFC(): boolean {
		return !(
			this.demande.xEcStatut >= DemandeStatus.WDCT &&
			(this.userConnected.role == UserRole.PRESTATAIRE &&
				(this.userConnected.service != this.typeBroker ||
					(this.userConnected.service == this.typeTransporteurBroker &&
						Statique.getInnerObject(this, "demande", "xEbUserOriginCustomsBroker", "ebUserNum") !=
							this.userConnected.ebUserNum &&
						Statique.getInnerObject(this, "demande", "xEbUserDestCustomsBroker", "ebUserNum") !=
							this.userConnected.ebUserNum)) &&
				this.demande.exEbDemandeTransporteurFinal &&
				((this.userConnected.superAdmin &&
					this.demande.exEbDemandeTransporteurFinal.ebCompagnie.ebCompagnieNum !=
						this.userConnected.ebCompagnie.ebCompagnieNum) ||
					(!this.userConnected.superAdmin &&
						this.demande.exEbDemandeTransporteurFinal.ebEtablissement.ebEtablissementNum !=
							this.userConnected.ebEtablissement.ebEtablissementNum)))
		);
	}

	// tester s'il faut activer le statut propre à Confirmed
	get isActiveCONFIRMED(): boolean {
		return !(
			this.demande.xEcStatut >= DemandeStatus.WDCT &&
			(this.userConnected.role == UserRole.PRESTATAIRE &&
				(this.userConnected.service != this.typeBroker ||
					(this.userConnected.service == this.typeTransporteurBroker &&
						Statique.getInnerObject(this, "demande", "xEbUserOriginCustomsBroker", "ebUserNum") !=
							this.userConnected.ebUserNum &&
						Statique.getInnerObject(this, "demande", "xEbUserDestCustomsBroker", "ebUserNum") !=
							this.userConnected.ebUserNum)) &&
				this.demande.exEbDemandeTransporteurFinal &&
				((this.userConnected.superAdmin &&
					this.demande.exEbDemandeTransporteurFinal.ebCompagnie.ebCompagnieNum !=
						this.userConnected.ebCompagnie.ebCompagnieNum) ||
					(!this.userConnected.superAdmin &&
						this.demande.exEbDemandeTransporteurFinal.ebEtablissement.ebEtablissementNum !=
							this.userConnected.ebEtablissement.ebEtablissementNum)))
		);
	}

	// tester s'il faut activer le statut propre à Not awarded
	get isActiveNAW(): boolean {
		return (
			this.demande.xEcStatut >= DemandeStatus.WDCT &&
			(this.userConnected.role == UserRole.PRESTATAIRE &&
				(this.userConnected.service != this.typeBroker ||
					(this.userConnected.service == this.typeTransporteurBroker &&
						Statique.getInnerObject(this, "demande", "xEbUserOriginCustomsBroker", "ebUserNum") !=
							this.userConnected.ebUserNum &&
						Statique.getInnerObject(this, "demande", "xEbUserDestCustomsBroker", "ebUserNum") !=
							this.userConnected.ebUserNum)) &&
				this.demande.exEbDemandeTransporteurFinal &&
				((this.userConnected.superAdmin &&
					this.demande.exEbDemandeTransporteurFinal.ebCompagnie.ebCompagnieNum !=
						this.userConnected.ebCompagnie.ebCompagnieNum) ||
					(!this.userConnected.superAdmin &&
						this.demande.exEbDemandeTransporteurFinal.ebEtablissement.ebEtablissementNum !=
							this.userConnected.ebEtablissement.ebEtablissementNum)))
		);
	}

	// tester s'il faut activer le statut propre à Cancelled
	get isCancelled(): boolean {
		return this.demande.xEcCancelled == CancelStatus.CANCELLED;
	}
}
