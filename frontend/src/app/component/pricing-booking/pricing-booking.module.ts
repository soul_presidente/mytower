import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PricingBookingRoutingModule } from "./pricing-booking-routing.module";

@NgModule({
	imports: [CommonModule, PricingBookingRoutingModule],
	declarations: [],
})
export class PricingBookingModule {}
