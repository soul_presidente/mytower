import { PricingService } from "@app/services/pricing.service";
import { EbDemande } from "@app/classes/demande";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { EbUser } from "@app/classes/user";
import { Statique } from "@app/utils/statique";
import { TranslateService } from "@ngx-translate/core";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { ChatService } from "@app/services/chat.service";
import { EbChat } from "@app/classes/chat";
import { EbCustomsInformation } from "@app/classes/customsInformation";
import { MTEnum } from "@app/classes/mtEnum";
import { Modules } from "@app/utils/enumeration";
import { EbEtablissement } from "@app/classes/etablissement";
import { EbCompagnie } from "@app/classes/compagnie";
import { StatiqueService } from "@app/services/statique.service";
import { EcCurrency } from "@app/classes/currency";
import SingletonStatique from "@app/utils/SingletonStatique";

@Component({
	selector: "app-customs-information",
	templateUrl: "./customs-information.component.html",
	styleUrls: ["./customs-information.component.css"],
})
export class CustomsInformationComponent implements OnInit {
	Modules = Modules;
	Statique = Statique;

	@Input()
	module: number;
	@Input()
	ebDemande: EbDemande;
	@Input()
	userConnected: EbUser;
	@Input()
	hasContributionAccess?: boolean;
	@Output()
	updateChatEmitter = new EventEmitter<EbChat>();

	listCurrency: Array<EcCurrency> = new Array<EcCurrency>();
	customsInformation: EbCustomsInformation = new EbCustomsInformation();
	mapStatiques: {
		customsControlStatus: Array<MTEnum>;
		customsDuty: Array<MTEnum>;
		customsVat: Array<MTEnum>;
		customsOtherTaxes: Array<MTEnum>;
	};

	ConstantsTranslate: Object;

	constructor(
		private pricingService: PricingService,
		private translate: TranslateService,
		private chatService: ChatService,
		private statiqueService: StatiqueService
	) {
		this.mapStatiques = {
			customsControlStatus: new Array<MTEnum>(),
			customsDuty: new Array<MTEnum>(),
			customsVat: new Array<MTEnum>(),
			customsOtherTaxes: new Array<MTEnum>(),
		};
	}

	ngOnInit() {
		// translation
		this.ConstantsTranslate = {};
		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate
				.get("PRICING_BOOKING." + key)
				.subscribe((res) => (this.ConstantsTranslate[key] = res));
		});

		this.pricingService.getCustomsInformation(this.ebDemande.ebDemandeNum).subscribe((res) => {
			if (res.mapStatiques) this.mapStatiques = res.mapStatiques;
			if (res.ebCustomsInformation) this.customsInformation.copy(res.ebCustomsInformation);
		});

		(async () => (this.listCurrency = await SingletonStatique.getListEcCurrency()))();
	}

	confirmCustomsInfo(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.customsInformation.xEbUser = new EbUser();
		this.customsInformation.xEbUser.ebUserNum = this.userConnected.ebUserNum;
		this.customsInformation.xEbEtablissement = new EbEtablissement();
		this.customsInformation.xEbEtablissement.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.customsInformation.xEbCompagnie = new EbCompagnie();
		this.customsInformation.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.customsInformation.xEbDemande = new EbDemande();
		this.customsInformation.xEbDemande.ebDemandeNum = this.ebDemande.ebDemandeNum;

		this.pricingService.updateCustomsInformation(this.customsInformation).subscribe((res) => {
			requestProcessing.afterGetResponse(event);

			this.updateChatEmitter.emit(res.ebChat);

			/*
            let ebChat = new EbChat();
            ebChat.userName = this.userConnected.nom + " " + this.userConnected.prenom;
            ebChat.xEbUser = this.userConnected.ebUserNum;
            ebChat.module = this.module;
            ebChat.idFiche = this.ebDemande.ebDemandeNum;
            ebChat.isHistory = true;
            ebChat.text = "Ajout/Modification du block Customs Information.";
            ebChat.idChatComponent = IDChatComponent.TRANSPORT_MANAGEMENT;

            this.chatService.addChat(Statique.controllerStatiqueListe + '/addChat', ebChat).subscribe(data => {
                this.updateChatEmitter.emit(data);
            });
            */
		});
	}
}
