import { PricingService } from "@app/services/pricing.service";
import { EbDemande } from "@app/classes/demande";
import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { EbUser } from "@app/classes/user";
import { Modules } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { TranslateService } from "@ngx-translate/core";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { ChatService } from "@app/services/chat.service";
import { EbChat } from "@app/classes/chat";
import { EbTtCompanyPsl } from "@app/classes/ttCompanyPsl";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";

@Component({
	selector: "app-transport-information",
	templateUrl: "./transport-information.component.html",
	styleUrls: ["./transport-information.component.css"],
})
export class TransportInformationComponent implements OnInit {
	Modules = Modules;

	@Input()
	module: number;
	@Input()
	ebDemande: EbDemande;
	@Input()
	userConnected: EbUser;
	@Input()
	hasContributionAccess?: boolean;
	@Input()
	isStepperForm: boolean = true;
	@Output()
	updateChatEmitter = new EventEmitter<EbChat>();
	@Output()
	onValidate = new EventEmitter<any>();

	@Input()
	readOnly: boolean = false;

	ebDemandeTransport: EbDemande = new EbDemande();
	ConstantsTranslate: Object;

	constructor(
		private pricingService: PricingService,
		private translate: TranslateService,
		private chatService: ChatService
	) {}

	ngOnInit() {
		// translation
		this.ConstantsTranslate = {};
		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate
				.get("PRICING_BOOKING." + key)
				.subscribe((res) => (this.ConstantsTranslate[key] = res));
		});

		if (this.ebDemande) {
			if (this.ebDemande.ebDemandeNum == null) {
				// It's at creation step
				this.ebDemandeTransport = this.ebDemande;
			} else {
				// It's at visualization step
				this.ebDemandeTransport.ebDemandeNum = this.ebDemande.ebDemandeNum;

				this.ebDemandeTransport.carrierUniqRefNum = this.ebDemande.carrierUniqRefNum;
				this.ebDemandeTransport.numAwbBol = this.ebDemande.numAwbBol;
				this.ebDemandeTransport.flightVessel = this.ebDemande.flightVessel;

				this.ebDemandeTransport.xEbSchemaPsl = Statique.cloneObject(
					this.ebDemande.xEbSchemaPsl,
					new EbTtSchemaPsl()
				);
				// this.ebDemandeTransport.datePickupTM = this.ebDemande.datePickupTM;
				// this.ebDemandeTransport.etd = this.ebDemande.etd;
				// this.ebDemandeTransport.eta = this.ebDemande.eta;
				// this.ebDemandeTransport.finalDelivery = this.ebDemande.finalDelivery;

				this.ebDemandeTransport.customsOffice = this.ebDemande.customsOffice;
				this.ebDemandeTransport.mawb = this.ebDemande.mawb;
			}
		}

		this.ebDemandeTransport.flagTransportInformation = true;
		this.ebDemandeTransport.xEbUserTransportInfoMaj = new EbUser();
		this.ebDemandeTransport.xEbUserTransportInfoMaj.ebUserNum = this.userConnected.ebUserNum;
		this.ebDemandeTransport.carrierComment = this.ebDemande.carrierComment;
	}

	setCarrierComment($event) {
		this.ebDemandeTransport.carrierComment = $event;
	}
	confirmTransportInfo(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		let msg = this.translate.instant("TRANSPORT_MANAGEMENT.INFORMATION_ABOUT_TRANSPORT_UPDATED");
		this.ebDemandeTransport.updateDate = new Date();
		this.pricingService
			.updateEbDemandeTransportInfos(this.ebDemandeTransport, msg)
			.subscribe((res) => {
				requestProcessing.afterGetResponse(event);

				this.updateChatEmitter.emit(res.ebChat);
			});
	}


	translatePSL(pslLibelle : string)
	{
		let psl : string;
		let formatText : string = localStorage.getItem("currentLang") == "en" ? "Estimated "+pslLibelle.toLowerCase()+" date:" : "Date prévue "+pslLibelle.toLowerCase()+" :";
		 this.translate.get("TRACK_TRACE.ESTIMATED_" + pslLibelle.toUpperCase() + "_DATE").subscribe((data:string)=>{
			psl = data.includes("_") ? formatText : data;
		});
		return psl;
	}

	getTransportInformationData(): Object {
		let obj: EbTtSchemaPsl = Statique.cloneObject(this.ebDemandeTransport.xEbSchemaPsl);
		obj &&
			obj.listPsl &&
			obj.listPsl.forEach((it: any) => {
				if (it.expectedDate) {
					it.expectedDate = new Date(it.expectedDate);
					it.expectedDate = it.expectedDate.getTime();
				}
			});
		return {
			ebDemandeNum: this.ebDemandeTransport.ebDemandeNum,
			carrierUniqRefNum: this.ebDemandeTransport.carrierUniqRefNum,
			numAwbBol: this.ebDemandeTransport.numAwbBol,
			flightVessel: this.ebDemandeTransport.flightVessel,
			customsOffice: this.ebDemandeTransport.customsOffice,
			mawb: this.ebDemandeTransport.mawb,
			xEbSchemaPsl: obj,
			carrierComment: this.ebDemandeTransport.carrierComment,
		};
	}

	nextPage(event) {
		this.onValidate.next();
	}
}
