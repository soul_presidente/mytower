import { DataTablesModule } from "angular-datatables";
import { NgModule } from "@angular/core";
import { SharedModule } from "@app/shared/module/shared.module";
import { TransportManagementRoutes } from "./transport-management.routes";
import { AdvancedSearchModule } from "@app/shared/advanced-search/advanced-search.module";
import { SharedComponentModule } from "../../pricing-booking/shared-component/shared-component.module";
import { PricingService } from "@app/services/pricing.service";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { TransportCreationComponent } from "./transport-creation/transport-creation.component";

@NgModule({
	imports: [
		SharedModule,
		AdvancedSearchModule,
		DataTablesModule,
		TransportManagementRoutes,
		SharedComponentModule,
	],
	declarations: [DashboardComponent, TransportCreationComponent],
	exports: [DashboardComponent, TransportCreationComponent],
	providers: [PricingService],
})
export class TransportManagementgModule {}
