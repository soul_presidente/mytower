import { ChangeDetectorRef, Component, Inject, OnInit, ViewChild } from "@angular/core";
import { DOCUMENT } from "@angular/common";
import { PricingService } from "@app/services/pricing.service";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { DialogService } from "@app/services/dialog.service";
import { Router } from "@angular/router";
import { UserService } from "@app/services/user.service";
import { TranslateService } from "@ngx-translate/core";
import { AccessRights, Modules, SavedFormIdentifier } from "@app/utils/enumeration";
import { CreationComponent } from "../../pricing/creation/creation.component";
import { TransportInformationComponent } from "../transport-information/transport-information.component";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbDemande } from "@app/classes/demande";
import { keyValue, Statique } from "@app/utils/statique";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { StatiqueService } from "@app/services/statique.service";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import { ExternalUsersComponent } from "../../shared-component/external-users/external-users.component";

@Component({
	selector: "app-transport-creation",
	templateUrl: "./transport-creation.component.html",
	styleUrls: ["./transport-creation.component.css"],
})
export class TransportCreationComponent extends CreationComponent implements OnInit {
	CarrierStatusCreationTM: keyValue[] = Statique.CarrierStatusCreationTM;

	@ViewChild(TransportInformationComponent, { static: false })
	transportInformationComponent: TransportInformationComponent;

	transportInfoPanel: boolean = false;
	transportInfoFormValid: boolean = false;
	modeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();
	inProcess: boolean = false;

	@ViewChild("externalUsersComponent", { static: false })
	externalUsersComponent: ExternalUsersComponent;

	constructor(
		protected pricingService: PricingService,
		protected savedFormsService: SavedFormsService,
		protected dialogService: DialogService,
		protected modalService: ModalService,
		protected router: Router,
		protected userService: UserService,
		@Inject(DOCUMENT) protected document: any,
		protected translate: TranslateService,
		protected statiqueService: StatiqueService,
		protected cdRef: ChangeDetectorRef
	) {
		super(
			pricingService,
			savedFormsService,
			dialogService,
			modalService,
			router,
			userService,
			document,
			translate,
			statiqueService,
			cdRef
		);

		this.ebCompNum = SavedFormIdentifier.PRICING_MASK;
		this.ebModuleNum = Modules.TRANSPORT_MANAGEMENT;
	}

	ngOnInit() {
		super.ngOnInit();

		super.getStatiques.bind(this);
	}

	showQuotationPanel() {
		// Prepare demande transporteurs from list transporteur
		this.generateDemandeTransporteurForTMCreation();

		super.showQuotationPanel();
	}

	showTransportInfoPanel() {
		this.transportInfoPanel = true;
		let $this = this;
		// Commented as this view is not used anymore and the dependency has been removed
		/*
		var timeOut;
		timeOut = setTimeout(function() {
			let pageScrollInstance: PageScrollInstance = PageScrollInstance.simpleInstance(
				$this.document,
				"#transportInfoDiv"
			);
			$this.pageScrollService.start(pageScrollInstance);
			clearTimeout(timeOut);
		}, 2);
		*/
	}

	createAction(event: any) {
		const $this = this;
		this.inProcess = true;

		$this.addDemande(event, function(demande) {
			$this.inProcess = false;

			$this.router.navigate(["/app/transport-management/dashboard"]);
		});
	}

	createAndTrackAction(event: any) {
		const $this = this;
		this.inProcess = true;

		$this.addDemande(event, function(demande) {
			$this.inProcess = false;

			$this.router.navigate(["/app/track-trace/dashboard"], {
				queryParams: {
					filterByDemande: demande.ebDemandeNum,
					displayAddEvent: false,
				},
			});
		});
	}

	addDemande(event: any, callback?: (EbDemande) => void) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		let _ebDemande = this.prepareEbDemandeForSave();

		this.pricingService.ajouterEbDemande(_ebDemande, Modules.TRANSPORT_MANAGEMENT).subscribe(
			(ebDemande: EbDemande) => {
				this.shippingInformationComponent.saveFavori(ebDemande.ebDemandeNum);
				requestProcessing.afterGetResponse(event);

				if (callback) callback(_ebDemande);
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				let title = null;
				this.translate.get("MODAL.ERROR").subscribe((res: string) => {
					title = res;
				});
				this.modalService.error(title, null, function() {});
			}
		);
	}

	prepareEbDemandeForSave(): EbDemande {
		let _demande = super.prepareEbDemandeForSave();

		// Prevent JSON Parse Error, Already have a POJO
		// remove duplicates
		// let arr = new Array<ExEbDemandeTransporteur>();
		_demande.exEbDemandeTransporteurs.forEach((dt) => {
			if (dt.xEbEtablissement) {
				dt.xEbEtablissement.ebCompagnie = undefined;
			}
			if (dt.xTransporteur && dt.xTransporteur.ebEtablissement) {
				dt.xTransporteur.ebEtablissement.ebCompagnie = undefined;
			}
			if (dt.xTransporteur && dt.xTransporteur.ebCompagnie) {
				dt.xTransporteur.ebCompagnie = undefined;
			}
			dt.listCostCategorie = undefined;

			/*
      if(!arr.find(el => el.xTransporteur.ebUserNum == dt.xTransporteur.ebUserNum))
        arr.push(dt);
      */
		});

		// _demande.exEbDemandeTransporteurs = arr;

		return _demande;
	}

	private generateDemandeTransporteurForTMCreation() {
		// These DemandeTransporteur are only for saving TM creation, they should be handled at save like pricing creation but with additional specific changes
		// And retrieve from mask if needed

		if (this.ebDemande.exEbDemandeTransporteurs == null)
			this.ebDemande.exEbDemandeTransporteurs = [];
		let newListDemandeTransporteurs = new Array<ExEbDemandeTransporteur>();

		this.listTransporteur.forEach((t) => {
			let foundMaskDemandeTransporteur: ExEbDemandeTransporteur = undefined;
			this.ebDemande.exEbDemandeTransporteurs.forEach((dt) => {
				if (dt.xTransporteur.ebUserNum == t.ebUserNum) foundMaskDemandeTransporteur = dt;
			});

			if (foundMaskDemandeTransporteur == undefined) {
				let dtNew = new ExEbDemandeTransporteur();
				dtNew.xTransporteur = t;
				dtNew.xEbEtablissement = t.ebEtablissement;
				dtNew.xEbEtablissement.ebCompagnie = t.ebCompagnie;
				dtNew.status = -1; // Default statut = empty

				newListDemandeTransporteurs.push(dtNew);
			} else {
				var dtNew = new ExEbDemandeTransporteur();
				dtNew.copy(foundMaskDemandeTransporteur);

				dtNew.xTransporteur = t;
				dtNew.xEbEtablissement = t.ebEtablissement;
				dtNew.xEbEtablissement.ebCompagnie = t.ebCompagnie;

				dtNew.listCostCategorie = []; // Prevent JSON parse error: Already had POJO for id

				newListDemandeTransporteurs.push(dtNew);
			}
		});

		this.ebDemande.exEbDemandeTransporteurs = newListDemandeTransporteurs;
	}
}
