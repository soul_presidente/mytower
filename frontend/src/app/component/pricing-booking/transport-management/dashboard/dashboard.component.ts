import { Component, OnInit } from "@angular/core";
import { Modules } from "@app/utils/enumeration";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { MessageService } from "primeng/api";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-dashboard",
	templateUrl: "./dashboard.component.html",
	styleUrls: ["./dashboard.component.css"],
})
export class DashboardComponent extends ConnectedUserComponent implements OnInit {
	Modules = Modules;
	searchInput: SearchCriteriaPricingBooking;
	listFields: Array<IField>;
	listCategorie: Array<EbCategorie>;

	public module: number;

	constructor(
		private messageService?: MessageService,
		protected headerService?: HeaderService,
		protected translate?: TranslateService
	) {
		super();
	}

	ngOnInit() {
		this.module = Modules.TRANSPORT_MANAGEMENT;
		this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				icon: "my-icon-trans-management",
				label: "MODULES.TM",
			},
		];
	}

	search(searchInput: SearchCriteriaPricingBooking) {
		this.searchInput = searchInput;
	}

	paramFieldsHandler(pListFields) {
		this.listFields = pListFields;
	}

	CategorieHandler(plistCategorie) {
		this.listCategorie = plistCategorie;
	}

	showSuccess(event: boolean) {
		if (event == true) {
			let detailMsg = this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL");
			let succesSummary = this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY");
			this.messageService.add({
				severity: "success",
				summary: succesSummary,
				detail: detailMsg,
			});
		}
	}
}
