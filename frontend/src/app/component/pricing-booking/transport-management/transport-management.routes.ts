import { Route, RouterModule } from "@angular/router";
import { DetailsComponent } from "../shared-component/details/details.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { TransportCreationComponent } from "./transport-creation/transport-creation.component";
import { DemandeCreationComponent } from "../pricing/creation-wf/demande-creation.component";
import { DemandeVisualisationComponent } from "../shared-component/demande-visualisation/demande-visualisation.component";

export const TRANSPORT_MANAGEMENT_ROUTES: Route[] = [
	{
		path: "dashboard",
		component: DashboardComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "creation-old",
		component: TransportCreationComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "creation",
		component: DemandeCreationComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "details-old/:ebDemandeNum",
		component: DetailsComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "details/:ebDemandeNum",
		component: DemandeVisualisationComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const TransportManagementRoutes = RouterModule.forChild(TRANSPORT_MANAGEMENT_ROUTES);
