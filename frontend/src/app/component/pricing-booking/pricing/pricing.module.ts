import { DataTablesModule } from "angular-datatables";
import { Ng2CompleterModule } from "ng2-completer";
import { PricingService } from "@app/services/pricing.service";
import { NgModule } from "@angular/core";
import { SharedModule } from "@app/shared/module/shared.module";
import { PricingRoutes } from "./pricing.routes";
import { AdvancedSearchModule } from "@app/shared/advanced-search/advanced-search.module";
import { CreationComponent } from "./creation/creation.component";
import { SharedComponentModule } from "../shared-component/shared-component.module";
import { PropositionCotationComponent } from "./proposition-cotation/proposition-cotation.component";
import { DashboardComponent } from "./dashboard/dashboard.component";

@NgModule({
	imports: [
		PricingRoutes,
		SharedModule,
		AdvancedSearchModule,
		Ng2CompleterModule,
		DataTablesModule,
		SharedComponentModule,
	],
	declarations: [
		DashboardComponent,
		//AdvancedSearchComponent,
		CreationComponent,
		PropositionCotationComponent,
		//CommentPanelComponent,
	],
	exports: [DashboardComponent],
	providers: [PricingService],
})
export class PricingModule {}
