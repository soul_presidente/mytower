import { Component, OnInit } from "@angular/core";
import { HeaderService } from "@app/services/header.service";
import { EbDemande } from "@app/classes/demande";
import { Router, ActivatedRoute } from "@angular/router";
import { map } from "rxjs/operators";
import { Statique } from "@app/utils/statique";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { ConfigPslCompanyService } from "@app/services/config-psl-company.service";

@Component({
	selector: "app-demande-creation",
	templateUrl: "./demande-creation.component.html",
	styleUrls: ["./demande-creation.component.scss"],
})
export class DemandeCreationComponent implements OnInit {
	demande: EbDemande;
	constructor(
		private configPslCompanyService : ConfigPslCompanyService,
		protected headerService: HeaderService,
		protected router: Router,
		protected route: ActivatedRoute
	) {}
	ngOnInit() {
		let duplicateDemande;
		this.route.paramMap.pipe(map(() => window.history.state)).subscribe((data) => {
			duplicateDemande = data;
		});
		this.demande = Statique.cloneObject(duplicateDemande, new EbDemande());
		this.cleaningDuplicateDemande();
	}

	cleaningDuplicateDemande() {
		if (this.demande && this.demande.ebDemandeNum) {
			this.demande.ebDemandeNum = null;
			this.demande.xEcStatut = null;
			this.demande.xEcNature = null;
			this.demande.refTransport = null;
			this.demande.listTypeDocuments = null;
			this.demande.dateCreation = null;
			this.demande.configPsl = null;
			this.demande.xEcCancelled = null;
			this.demande.exEbDemandeTransporteur = null;
			this.demande.listDemandeQuote = [];
			this.demande.sendEmails = true;
			this.demande.tdcAcknowledge = false;
			this.demande.tdcAcknowledgeDate = null;
			this.demande.askForTransportResponsibility = false;
			this.demande.provideTransport = false;
			this.demande.exEbDemandeTransporteurs = [];
			this.demande.listTypeDocuments = null;
			this.demande.numAwbBol = null;
			this.demande.flightVessel = null;
			this.demande.finalDelivery = null;
			this.demande.eta = null;
			this.demande.mawb = null;
			this.demande.carrierUniqRefNum = null;
			this.demande.customsOffice = null;
			this.demande.etd = null;
			this.demande.carrierComment = null;

			if(this.demande.xEbSchemaPsl && this.demande.xEbSchemaPsl.ebTtSchemaPslNum){
				this.configPslCompanyService
				.getSchemaPsl(this.demande.xEbSchemaPsl.ebTtSchemaPslNum)
				.subscribe((schemaPsl: EbTtSchemaPsl) => {
					this.demande.xEbSchemaPsl = schemaPsl;
				});
			}

			if (this.demande.ebPartyOrigin) {
				this.demande.ebPartyOrigin.ebPartyNum = null;
			}

			if (
				this.demande.ebPartyNotif &&
				this.demande.ebPartyNotif.ebPartyNum &&
				this.demande.ebPartyNotif.xEcCountry &&
				this.demande.ebPartyNotif.xEcCountry.ecCountryNum
			) {
				this.demande.ebPartyNotif.ebPartyNum = null;
			} else {
				this.demande.ebPartyNotif = null;
			}

			if (this.demande.ebPartyDest) {
				this.demande.ebPartyDest.ebPartyNum = null;
			}

			if (
				this.demande.exEbDemandeTransporteurs &&
				this.demande.exEbDemandeTransporteurs.length > 0
			) {
				this.demande.exEbDemandeTransporteurs.forEach((dt) => {
					if (dt.xEbDemande) {
						dt.xEbDemande = null;
						dt.exEbDemandeTransporteurNum = null;
					}
				});
			}
			if (this.demande.listMarchandises && this.demande.listMarchandises.length > 0) {
				this.demande.listMarchandises.forEach((marchandise) => {
					marchandise.ebMarchandiseNum = null;
					marchandise.xEbTracing = null;
				});
			}
		} else {
			this.demande = new EbDemande();
		}
	}
}
