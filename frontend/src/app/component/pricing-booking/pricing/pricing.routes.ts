import { PropositionCotationComponent } from "./proposition-cotation/proposition-cotation.component";
import { CreationComponent } from "./creation/creation.component";
import { Route, RouterModule } from "@angular/router";
import { DetailsComponent } from "../shared-component/details/details.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { DemandeCreationComponent } from "./creation-wf/demande-creation.component";
import { DemandeVisualisationComponent } from "../shared-component/demande-visualisation/demande-visualisation.component";
import { PricingGroupingPropositionComponent } from "../shared-component/pricing-grouping-proposition/pricing-grouping-proposition.component";
import { UserRole } from "@app/utils/enumeration";

export const PRICING_ROUTES: Route[] = [
	{
		path: "dashboard",
		component: DashboardComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "creation-old",
		component: CreationComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "creation",
		component: DemandeCreationComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "proposition-cotation",
		component: PropositionCotationComponent,
	},
	{
		path: "details-old/:ebDemandeNum",
		component: DetailsComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "details/:ebDemandeNum",
		component: DemandeVisualisationComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "proposition-groupage",
		component: PricingGroupingPropositionComponent,
		// canActivate: [UserAuthGuardService],
		// data: { chargeur: true, controlTower: true },
	},
];

export const PricingRoutes = RouterModule.forChild(PRICING_ROUTES);
