import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { Modules } from "@app/utils/enumeration";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { PricingService } from "@app/services/pricing.service";
import { Router } from "@angular/router";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbDemande } from "@app/classes/demande";
import { EbUser } from "@app/classes/user";
import { CotationComponent } from "../../shared-component/cotation/cotation.component";
import { Statique } from "@app/utils/statique";
import { GlobalService } from "@app/services/global.service";

@Component({
	selector: "app-proposition-cotation",
	templateUrl: "./proposition-cotation.component.html",
	styleUrls: ["./proposition-cotation.component.css"],
})
export class PropositionCotationComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	ebDemande: EbDemande;
	@ViewChild(CotationComponent, { static: false })
	cotationComponent: CotationComponent;
	unitInfos: any;

	constructor(
		private globalService: GlobalService,
		private router: Router,
		private pricingService: PricingService
	) {
		super();
	}

	ngOnInit() {
		let searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();

		this.router.routerState.root.queryParams.subscribe((params) => {
			searchCriteriaPricingBooking.ebDemandeNum = params["id"];

			this.getListUnit(params["id"]);

			searchCriteriaPricingBooking.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			searchCriteriaPricingBooking.module = Modules.PRICING;
			searchCriteriaPricingBooking.roleTransporteur = true;
			this.pricingService.getEbDemande(searchCriteriaPricingBooking).subscribe((data) => {
				this.ebDemande = data;
			});
		});
		this.ebDemande = new EbDemande();
	}

	getListUnit(ebDemandeNum) {
		this.globalService
			.runAction(Statique.controllerUnit + "/list-unit", ebDemandeNum)
			.subscribe((res) => {
				this.unitInfos = res;
			});
	}

	addQuotation() {
		this.cotationComponent.exEbDemandeTransporteur.xEbDemande = new EbDemande();
		this.cotationComponent.exEbDemandeTransporteur.xEbDemande.ebDemandeNum = this.ebDemande.ebDemandeNum;
		this.cotationComponent.exEbDemandeTransporteur.xTransporteur = new EbUser();
		this.cotationComponent.exEbDemandeTransporteur.xTransporteur.ebUserNum = this.userConnected.ebUserNum;
		this.cotationComponent.exEbDemandeTransporteur.deliveryTime = Statique.fromDatePicker(
			this.cotationComponent.exEbDemandeTransporteur.deliveryTime
		);
		this.cotationComponent.exEbDemandeTransporteur.pickupTime = Statique.fromDatePicker(
			this.cotationComponent.exEbDemandeTransporteur.pickupTime
		);
		this.pricingService
			.ajouterQuotation(this.cotationComponent.exEbDemandeTransporteur,Modules.PRICING,false)
			.subscribe((exEbDemandeTransporteur: ExEbDemandeTransporteur) => {});
	}
}
