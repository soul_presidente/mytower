import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ViewChild,
	OnChanges,
	SimpleChanges,
} from "@angular/core";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";
import { Statique } from "@app/utils/statique";
import { SearchCriteriaFreightAudit } from "@app/utils/SearchCriteriaFreightAudit";
import { DataTableDirective } from "angular-datatables";
import {
	ActionFreightAudit, CarrierStatus,
	GenericTableScreen,
	IDChatComponent,
	InvoiceStatus,
	ModeTransport,
	Modules,
	UserRole,
} from "@app/utils/enumeration";
import { TranslateService } from "@ngx-translate/core";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ExportService } from "@app/services/export.service";
import { FreightAuditService } from "@app/services/freight-audit.service";
import { EbInvoice } from "@app/classes/invoice";
import { FreightAuditComponent } from "../freight-audit.component";
import { ModalService } from "@app/shared/modal/modal.service";
import { EbChat } from "@app/classes/chat";
import { ChatService } from "@app/services/chat.service";
import { UploadFile, UploadOutput } from "ngx-uploader";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ExEbDemandeTransporteur } from "@app/classes/demandeTransporteur";
import { EbDemande } from "@app/classes/demande";
import { InvoiceFichierJoint } from "@app/classes/InvoiceFichierJoint";
import { PricingService } from "@app/services/pricing.service";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { EtablissementService } from "@app/services/etablissement.service";
import { EbCategorie } from "@app/classes/categorie";
import EbCustomField, { IField } from "@app/classes/customField";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import { StatiqueService } from "@app/services/statique.service";
import { FreightAuditSearchComponent } from "../../freight-audit/freight-audit-search/freight-audit-search.component";
import SingletonStatique from "@app/utils/SingletonStatique";
import { GenericTableCcpComponent } from "@app/shared/generic-table-ccp/generic-table-ccp.component";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { EbTypeTransport } from "@app/classes/EbTypeTransport";
import { EbFlag } from "@app/classes/ebFlag";
import { InvoiceWrapper } from "@app/classes/invoiceWrapper";
import { MessageService } from "primeng/api";
import { RequestProcessing } from "../../../utils/requestProcessing";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { FlagColumnGenericCell } from "@app/shared/generic-cell/commons/flag-column.generic-cell";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { PopupComponent } from "../popup/popup.component";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { EcCurrency } from "@app/classes/currency";
import { EbCompagnie } from "@app/classes/compagnie";
import { UserService } from "@app/services/user.service";
import { CompagnieService } from "@app/services/compagnie.service";
import {CostCategorie, Cost} from "@app/classes/costCategorie";

@Component({
	selector: "app-freight-audit-list",
	templateUrl: "./freight-audit-list.component.html",
	styleUrls: ["./freight-audit-list.component.scss"],
})
export class FreightAuditListComponent extends FreightAuditComponent implements OnInit, OnChanges {
	@Output()
	listFieldsEmitter = new EventEmitter<Array<IField>>();
	@Output()
	listCategorieEmitter = new EventEmitter<Array<EbCategorie>>();
	listFields: any;
	listFieldscustom: Array<IField>;
	ebCustomField: any;
	listCategorie: any;
	listserv3: Array<any> = new Array<any>();
	listCustomField: Array<EbCustomField> = new Array<EbCustomField>();
	listserv2: any;
	chargeurOuCarrier: boolean;

	applyFlag: boolean;
	res: number = 3;
	applyStatut: boolean;
	sharCom: boolean;
	applyVal: boolean = false;
	applyDate: boolean;
	applyPrice: boolean;
	applyNumber: boolean;
	ok = true;
	complete: boolean;
	applyValue: any;
	applyDateValue: any;
	datatableComponent: number;
	@Input()
	public nbColListing: string = "col-md-12 sid-border-ri padding-right-20";
	@Input()
	public nbColListingGlobal: string = "col-md-6";
	@Input()
	isCotationVisible: boolean = false;
	@Input()
	isCostInvoiceVisible: boolean = true;
	@Input()
	freightAuditSearchComponent: FreightAuditSearchComponent;
	@Input()
	popupF: boolean;
	@ViewChild("tableFreight", { static: false })
	tableF: ElementRef;
	@ViewChild("fileInput", { static: true })
	fileInput: ElementRef;

	// genericTable attributes
	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableCcpComponent;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	newInvoiceFichierJoint: InvoiceFichierJoint = null;
	statique = Statique;
	dataInsertion: string = this.statique.controllerStatiqueListe + "/addChat";
	listChat: Array<EbChat> = new Array<EbChat>();
	ebChat: EbChat;
	dataPopup: number;
	listFreightAudit: Map<number, EbInvoice> = new Map();
	comment: boolean;
	statusValue: string;
	statut: string;
	ebInvoice: EbInvoice = new EbInvoice();
	IDChatComponent = IDChatComponent;
	listInvoice: Map<number, EbInvoice> = new Map();
	listInvoiceSelected: Map<number, EbInvoice> = new Map();
	subscriptionList = [];
	diff: number;
	contributionAccess: boolean;
	disabled: string
	private elRef: ElementRef;
	protected router: Router;
	ConstantsTranslate: Object = {
		STATUS: "",
	};
	listInvoiceData: Array<EbInvoice> = new Array<EbInvoice>();
	selectedStatutRow = null;
	Modules = Modules;
	selectValue: any;
	selectValue1: string;
	selectValue2: string;
	dataUpload: any;
	nbr = 0;
	uploadF = false;
	dataUploadProgress: any;
	ebInvoiceNum: number;

	xEbDemandeNum: number;

	numberArraw: number;
	priceArraw: number;
	dateArraw: number;
	closeResult: String;
	exEbDemandeTransporteurs: ExEbDemandeTransporteur = new ExEbDemandeTransporteur();
	ebDemande: EbDemande = new EbDemande();
	selectedInvoiceMonth: number;
	selectedInvoiceYear: number;
	showPriceExEbDemandeTransporteur: ExEbDemandeTransporteur;
	selectStatut: number;
	initiateState: boolean = false;
	listDate: Array<Date> = new Array<Date>();
	dt: Date = new Date();
	str: String;
	varUser: number;
	listserv: any;
	isInitialOrder: boolean = true;
	globalSearchCriteria: SearchCriteriaFreightAudit = new SearchCriteriaFreightAudit();
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	@ViewChild("content1", { static: false })
	content1: any;

	componentObject: {
		dataUpload: any;
	};
	@Input()
	module: number;
	uploaderFacture: boolean = false;
	OpenToglePopUpAction: boolean = false;
	TogleShareAction: boolean = false;
	TogleApplyAction: boolean = false;
	invoiceWrap: InvoiceWrapper = new InvoiceWrapper();
	invoiceCopy: EbInvoice = new EbInvoice();
	typeRequestList: any;
	@Input("searchInput")
	set searchInput(searchInput: SearchCriteriaFreightAudit) {
		if (searchInput != null) {
			let tmpSearchInput: SearchCriteriaFreightAudit = new SearchCriteriaFreightAudit();

			tmpSearchInput.module = this.module;
			tmpSearchInput.ebUserNum = this.userConnected.ebUserNum;
			tmpSearchInput.searchInput = JSON.stringify(searchInput || {});

			this.globalSearchCriteria = tmpSearchInput;
		}
	}
	listTypeDocuments = new Array<EbTypeDocuments>();

	currencyList: Array<EcCurrency>;

	doRefreshDT: boolean = false;

	constructor(
		private statiqueService: StatiqueService,
		private modalService1: NgbModal,
		protected chatService: ChatService,
		protected freightAuditService: FreightAuditService,
		protected etablissementService: EtablissementService,
		protected modalService: ModalService,
		protected translate: TranslateService,
		protected messageService: MessageService,
		public generaleMethode: generaleMethodes,
		protected exportService?: ExportService,
		private freighAuditService?: FreightAuditService,
		private pricingService?: PricingService,

		protected route?: ActivatedRoute,
		protected activatedRoute?: ActivatedRoute,
		protected headerService?: HeaderService,
		private userService?: UserService,
		private compagnieService?: CompagnieService
	) {
		super(freightAuditService, etablissementService, modalService, translate);
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				icon: "my-icon-freight ",
				label: "MODULES.FA",
			},
		];
	}
	clickHandler(data) {
		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(this.module)];
	}
	// Open input memo for chargeur
	clickAddMemoChargeur(ebInvoiceNum: number, event: HTMLElement) {
		this.ebInvoice = this.ebInvoice = this.genericTable.getDataRow(ebInvoiceNum);
		this.listInvoice.set(ebInvoiceNum, this.ebInvoice);
		this.addMemoChargeur(this.listInvoice, ebInvoiceNum, event);
	}
	// Open input memo for Carrier
	clickAddMemoCarrier(ebInvoiceNum: number, event: HTMLElement) {
		this.ebInvoice = this.genericTable.getDataRow(ebInvoiceNum);
		this.listInvoice.set(ebInvoiceNum, this.ebInvoice);
		this.addMemoCarrier(this.listInvoice, ebInvoiceNum, event);
	}
	// Update price chargeur (dashboad et bdd)
	clickApplyChargeurInvoicePrice(ebInvoiceNum: number, event: HTMLElement) {
		this.ebInvoice = this.genericTable.getDataRow(ebInvoiceNum);
		const oldInvoicePrice = this.ebInvoice.applyChargeurIvoicePrice;
		this.invoiceWrap = new InvoiceWrapper();
		this.invoiceWrap.lisAction = new Array<number>();
		this.invoiceWrap.listEbInvoiceNumber = new Array<number>();
		this.invoiceWrap.ebInvoice = this.invoiceCopy;
		this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListInvoicePrice);
		this.listInvoice.set(ebInvoiceNum, this.ebInvoice);

		let ebInvoice: EbInvoice = new EbInvoice();
		ebInvoice = this.ebInvoice;
		const newInvoicePrice = event.parentNode["getElementsByTagName"]("input")[0].value;
		ebInvoice.applyChargeurIvoicePrice = newInvoicePrice;
		ebInvoice.applyCarrierIvoicePrice = ebInvoice.applyChargeurIvoicePrice;
		ebInvoice.gapChargeur = ebInvoice.applyChargeurIvoicePrice - this.ebInvoice.preCarrierCost;
		ebInvoice.gapCarrier = ebInvoice.gapChargeur;
		this.updateCostAfterApplyInvoicePrice(ebInvoice, newInvoicePrice);

		this.listInvoice.set(ebInvoiceNum, ebInvoice);

		let newList = new Array<EbInvoice>();
		this.listInvoice.forEach((it) => newList.push(it));

		this.invoiceWrap.ebInvoice = ebInvoice;
		this.invoiceWrap.ebInvoice.listCostCategorieCharger = ebInvoice.listCostCategorieCharger;
		this.invoiceWrap.listEbInvoiceNumber.push(ebInvoiceNum);

		this.freighAuditService.allAction(this.invoiceWrap).subscribe((data) => {
			try {
				let row: EbInvoice = this.genericTable.getDataRow(ebInvoiceNum);
				this.priceArraw = 0;
				row.priceArraw = this.priceArraw;
				row.applyChargeurIvoicePrice = ebInvoice.applyChargeurIvoicePrice;
				row.gapChargeur = ebInvoice.applyChargeurIvoicePrice - this.ebInvoice.preCarrierCost;
				row.listCostCategorieCharger = ebInvoice.listCostCategorieCharger;
				this.genericTable.setDataRow(ebInvoiceNum, row);
				this.messageService.add({
					severity: "success",
					summary: "Operation Success",
					detail: "Update Price",
				});
			} catch (e) {}
		});
	}
	updateCostAfterApplyInvoicePrice(invoice: EbInvoice, newInvoicePrice: number ){
		let precarriageCostCategorie = new CostCategorie();
		let otherCost = new Cost();
		let otherCostIndex: number = 0;
		let precarriageCostIndex: number = 0;
		let precarriageGap: number = 0;
		if(this.isPrestataire){
			if(invoice.listCostCategorieTransporteur != null && invoice.listCostCategorieTransporteur.length > 0 ){
				precarriageCostCategorie = invoice.listCostCategorieTransporteur[0];
			}
		}else{
			if(invoice.listCostCategorieCharger != null && invoice.listCostCategorieCharger.length > 0){
				precarriageCostCategorie = invoice.listCostCategorieCharger[0];
			}
		}
		if(precarriageCostCategorie != null && precarriageCostCategorie.listEbCost.length > 0) {
			otherCostIndex = precarriageCostCategorie.listEbCost.findIndex(cost => cost.code == "OC");
			if(otherCostIndex != -1){
				otherCost = precarriageCostCategorie.listEbCost[otherCostIndex];
				precarriageCostCategorie.priceReal =  newInvoicePrice;
				precarriageCostCategorie.priceEuroReal = newInvoicePrice;
				otherCost.euroExchangeRateReal = otherCost.euroExchangeRateReal != null ? otherCost.euroExchangeRateReal : 1;
				otherCost.euroExchangeRate = otherCost.euroExchangeRateReal;
				otherCost.priceEuro = otherCost.price;
				otherCost.priceEuroReal =  newInvoicePrice;
				otherCost.priceReal =  otherCost.priceEuroReal/otherCost.euroExchangeRateReal;
				otherCost.priceGab = otherCost.priceReal - otherCost.price;
				otherCost.priceGabConverted = otherCost.priceGab * otherCost.euroExchangeRateReal
				precarriageCostCategorie.listEbCost[otherCostIndex] = otherCost;
				precarriageCostCategorie.listEbCost.forEach( cost => {
					precarriageGap = precarriageGap + cost.priceGabConverted;
				});
				precarriageCostCategorie.priceGab = precarriageGap;
				if(this.isPrestataire){
					invoice.listCostCategorieTransporteur[precarriageCostIndex] = precarriageCostCategorie;
				}else{
					invoice.listCostCategorieCharger[precarriageCostIndex] = precarriageCostCategorie;
				}
			}
		}
	}

	// Update number charger (dashboad et bdd)
	clickApplyInvoiceNumberChargeur(ebInvoiceNum: number, event: HTMLElement) {
		this.invoiceWrap = new InvoiceWrapper();
		this.invoiceWrap.lisAction = new Array<number>();
		this.invoiceWrap.listEbInvoiceNumber = new Array<number>();
		this.invoiceWrap.ebInvoice = this.invoiceCopy;
		this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListInvoiceNumber);

		this.ebInvoice = this.genericTable.getDataRow(ebInvoiceNum);
		this.listInvoice.set(ebInvoiceNum, this.ebInvoice);

		let ebInvoice: EbInvoice = new EbInvoice();
		ebInvoice.ebInvoiceNum = ebInvoiceNum;
		ebInvoice.applyChargeurIvoiceNumber = event.parentNode["getElementsByTagName"](
			"input"
		)[0].value;
		ebInvoice.applyCarrierIvoiceNumber = ebInvoice.applyChargeurIvoiceNumber;
		this.listInvoice.set(ebInvoiceNum, ebInvoice);

		let newList = new Array<EbInvoice>();
		this.listInvoice.forEach((it) => newList.push(it));

		this.invoiceWrap.ebInvoice = ebInvoice;
		this.invoiceWrap.listEbInvoiceNumber.push(ebInvoiceNum);

		this.freighAuditService.allAction(this.invoiceWrap).subscribe((data) => {
			try {
				let row: EbInvoice = this.genericTable.getDataRow(ebInvoiceNum);
				this.numberArraw = 0;
				row.numberArraw = this.numberArraw;
				row.applyChargeurIvoiceNumber = ebInvoice.applyChargeurIvoiceNumber;
				this.genericTable.setDataRow(ebInvoiceNum, row);
				this.messageService.add({
					severity: "success",
					summary: "Operation Success",
					detail: "Update Number",
				});
			} catch (e) {}
		});
	}
	// Update number carrier (dashboad et bdd)
	clickApplyInvoiceNumberCarrier(ebInvoiceNum: number, event: HTMLElement) {
		this.invoiceWrap = new InvoiceWrapper();
		this.invoiceWrap.lisAction = new Array<number>();
		this.invoiceWrap.listEbInvoiceNumber = new Array<number>();
		this.invoiceWrap.ebInvoice = this.invoiceCopy;
		this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListInvoiceNumber);
		this.ebInvoice = this.genericTable.getDataRow(ebInvoiceNum);
		this.listInvoice.set(ebInvoiceNum, this.ebInvoice);
		let ebInvoice: EbInvoice = new EbInvoice();
		ebInvoice.ebInvoiceNum = ebInvoiceNum;
		ebInvoice.applyCarrierIvoiceNumber = event.parentNode["getElementsByTagName"]("input")[0].value;
		this.listInvoice.set(ebInvoiceNum, ebInvoice);

		let newList = new Array<EbInvoice>();
		this.listInvoice.forEach((it) => newList.push(it));

		this.invoiceWrap.ebInvoice = ebInvoice;
		this.invoiceWrap.listEbInvoiceNumber.push(ebInvoiceNum);

		this.freighAuditService.allAction(this.invoiceWrap).subscribe((data) => {
			try {
				let row: EbInvoice = this.genericTable.getDataRow(ebInvoiceNum);
				this.numberArraw = 0;
				row.applyCarrierIvoiceNumber = ebInvoice.applyCarrierIvoiceNumber;
				row.numberArraw = this.numberArraw;
				this.genericTable.setDataRow(ebInvoiceNum, row);
				this.messageService.add({
					severity: "success",
					summary: "Operation Success",
					detail: "Update Number",
				});
			} catch (e) {}
		});
	}
	// Update price Carrier (dashboad et bdd)
	clickApplyCarrierInvoicePrice(ebInvoiceNum: number, event: HTMLElement) {
		const oldInvoicePrice = this.ebInvoice.applyCarrierIvoicePrice;
		this.invoiceWrap = new InvoiceWrapper();
		this.invoiceWrap.lisAction = new Array<number>();
		this.invoiceWrap.listEbInvoiceNumber = new Array<number>();
		this.invoiceWrap.ebInvoice = this.invoiceCopy;
		this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListInvoicePrice);
		this.ebInvoice = this.genericTable.getDataRow(ebInvoiceNum);
		this.listInvoice.set(ebInvoiceNum, this.ebInvoice);

		let ebInvoice: EbInvoice = new EbInvoice();
		ebInvoice = this.ebInvoice;
		const newInvoicePrice = event.parentNode["getElementsByTagName"]("input")[0].value;
		ebInvoice.applyCarrierIvoicePrice = newInvoicePrice;
		ebInvoice.gapCarrier = ebInvoice.applyCarrierIvoicePrice - this.ebInvoice.preCarrierCost;
		this.updateCostAfterApplyInvoicePrice(ebInvoice, newInvoicePrice);
		this.listInvoice.set(ebInvoiceNum, ebInvoice);

		let newList = new Array<EbInvoice>();
		this.listInvoice.forEach((it) => newList.push(it));

		this.invoiceWrap.ebInvoice = ebInvoice;
		this.invoiceWrap.ebInvoice.listCostCategorieTransporteur = ebInvoice.listCostCategorieTransporteur;
		this.invoiceWrap.listEbInvoiceNumber.push(ebInvoiceNum);
		this.freighAuditService.allAction(this.invoiceWrap).subscribe((data) => {
			try {
				let row: EbInvoice = this.genericTable.getDataRow(ebInvoiceNum);
				row.applyCarrierIvoicePrice = ebInvoice.applyCarrierIvoicePrice;
				row.gapCarrier = ebInvoice.applyCarrierIvoicePrice - this.ebInvoice.preCarrierCost;
				row.listCostCategorieTransporteur = ebInvoice.listCostCategorieTransporteur;
				this.priceArraw = 0;
				row.priceArraw = this.priceArraw;
				this.genericTable.setDataRow(ebInvoiceNum, row);
				this.messageService.add({
					severity: "success",
					summary: "Operation Success",
					detail: "Update Price",
				});
			} catch (e) {}
		});
	}
	// update date
	changeDate(ebInvoiceNum: number, event: HTMLElement) {
		this.invoiceWrap = new InvoiceWrapper();
		this.invoiceWrap.lisAction = new Array<number>();
		this.invoiceWrap.listEbInvoiceNumber = new Array<number>();
		this.invoiceWrap.ebInvoice = this.invoiceCopy;
		this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListDate);

		let ebInvoice: EbInvoice = new EbInvoice();
		ebInvoice.ebInvoiceNum = ebInvoiceNum;

		let dt;
		dt = event.parentNode["getElementsByTagName"]("select")[0].value;

		ebInvoice.invoiceDateChargeur = new Date(dt);
		ebInvoice.invoiceDateCarrier = new Date(dt);
		this.listInvoice.set(ebInvoiceNum, ebInvoice);
		let newList = new Array<EbInvoice>();

		this.invoiceWrap.ebInvoice = ebInvoice;
		this.invoiceWrap.listEbInvoiceNumber.push(ebInvoiceNum);

		this.listInvoice.forEach((it) => newList.push(it));

		this.freighAuditService.allAction(this.invoiceWrap).subscribe(
			(data) => {
				let row: EbInvoice = this.genericTable.getDataRow(ebInvoiceNum);
				if (this.isChargeur) row.invoiceDateChargeur = new Date(dt);
				else if (this.isPrestataire) row.invoiceDateCarrier = new Date(dt);
				this.genericTable.setDataRow(ebInvoiceNum, row);
				this.messageService.add({
					severity: "success",
					summary: "Operation Success",
					detail: "Update Date",
				});
			},
			(error) => {
				let title = null;
				let body = null;
				this.translate.get("MODAL.ERROR").subscribe((res: string) => {
					title = res;
				});

				this.modalService.error(title, null, function() {});
			}
		);
	}

	handleStatutChange(el) {
		this.selectedStatutRow = {
			ebInvoice: el.dataset && el.dataset.ebinvoice ? el.dataset.ebinvoice : null,
			statut: +el.value,
		};
		let ebInvoice = new EbInvoice();
		ebInvoice.ebInvoiceNum = +this.selectedStatutRow.ebInvoice;
		ebInvoice.statut = this.selectedStatutRow.statut;
		this.freightAuditService.updateStatut(ebInvoice).subscribe((res) => {
			this.listChat.push(res["ebChat"]);
		});
		this.startPopUp(ebInvoice.ebInvoiceNum);
	}

	startPopUp(ebInvoiceNum: number) {
		this.ebInvoice = this.genericTable.getDataRow(ebInvoiceNum);
		this.ebInvoice.ebInvoiceNum = ebInvoiceNum;
		let oldStatus = this.ebInvoice.statut;

		if (this.selectedStatutRow && this.ebInvoice.ebInvoiceNum == this.selectedStatutRow.ebInvoice) {
			this.ebInvoice.statut = this.selectedStatutRow.statut;
			this.selectedStatutRow = null;
		}
	}

	async exportCsv() {
		this.exportService.startExportCsv(Modules.FREIGHT_AUDIT, false, null, this.userConnected);
	}

	getDatatableCompId() {
		if (this.module == Modules.FREIGHT_AUDIT) return GenericTableScreen.FREIGHT_AUDIT;
		return null;
	}
	setStatut() {
		let key = "FREIGHT_AUDIT.";
		if (this.ebInvoice.statut == InvoiceStatus.CONFIRMED_BY_CHARGEUR) {
			key += "CONFIRMED_BY_CHARGEUR";
		} else if (this.ebInvoice.statut == InvoiceStatus.WAITING_FOR_CARRIER_CONFIRMATION) {
			key += "WAITING_FOR_CARRIER_CONFIRMATION";
		} else if (this.ebInvoice.statut == InvoiceStatus.WAITING_FOR_CARRIER_INPUT) {
			key += "WAITING_FOR_CARRIER_INPUT";
		} else if (this.ebInvoice.statut == InvoiceStatus.WAITING_FOR_CHARGEUR_CONFIRMATION) {
			key += "WAITING_FOR_CHARGEUR_CONFIRMATION";
		}
		this.translate.get(key).subscribe((res: string) => {
			this.statut = res;
		});
	}

	changeStatutDetails(ok: boolean, event: any) {
		this.changeStatut(this.ebInvoice, ok, event).then((data: EbInvoice) => {
			this.ebInvoice = data;
			this.setStatut();
		});
	}

	commentaire() {
		this.comment = true;
		return this.comment;
	}
	ngOnInit() {
		this.route.params.subscribe((params: Params) => {
			let ebDemandeNum = params["ebDemandeNum"];
			if (ebDemandeNum) {
				this.globalSearchCriteria.ebDemandeNum = ebDemandeNum;
			}
		});
		this.contributionAccess =  this.userConnected.hasContribution(Modules.FREIGHT_AUDIT);
		this.disabled = this.contributionAccess ? '':'disabled';
		this.datatableComponent = this.getDatatableCompId();
		this.headerService.registerActionButtons(this.actionButtons);
		this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());
		this.getStatiques();
		this.freightAuditService.getListStatus().subscribe((res) => {
			this.listserv3 = res;
		});

		this.freightAuditService.getListActionType().subscribe((res) => {
			this.listserv = res;
		});

		let dt = null;
		let dtt = null;
		for (let i = -6; i < 7; i++) {
			dt = new Date();
			dt.setMonth(dt.getMonth() + i);
			dtt = Statique.invoiceMonths2[dt.getMonth()].value + "  " + dt.getFullYear();
			this.listDate.push(dtt);
		}

		this.str = "";
		let array;

		let i = -6;
		this.listDate.forEach((it) => {
			if (i < 7) {
			}
			i++;
		});

		this.selectValue = 0;
		let $this = this;

		this.componentObject = this;

		window.functions = window.functions || {};
		window.functions.handleFileUpload = this.fileInputHandle.bind(this);
		window.functions.startPopUp = this.startPopUp.bind(this);
		window.functions.clickAddMemoChargeur = this.clickAddMemoChargeur.bind(this);
		window.functions.handleStatutChange = this.handleStatutChange.bind(this);
		window.functions.clickAddMemoCarrier = this.clickAddMemoCarrier.bind(this);
		window.functions.clickApplyChargeurInvoicePrice = this.clickApplyChargeurInvoicePrice.bind(
			this
		);
		window.functions.clickApplyCarrierInvoicePrice = this.clickApplyCarrierInvoicePrice.bind(this);
		window.functions.clickApplyInvoiceNumberChargeur = this.clickApplyInvoiceNumberChargeur.bind(
			this
		);
		window.functions.clickApplyInvoiceNumberCarrier = this.clickApplyInvoiceNumberCarrier.bind(
			this
		);
		window.functions.emitClickPopUp = this.emitClickPopUp.bind(this);
		window.functions.emitClickUpload = this.emitClickUpload.bind(this);
		window.functions.popupF_true = this.popupF_true.bind(this);
		window.functions.uploadFile = this.uploadFile.bind(this);
		window.functions.showDetailsPrice = this.showDetailsPrice.bind(this);
		window.functions.handleChargeurStatutChange = this.handleChargeurStatutChange.bind(this);
		window.functions.handleCarrierStatutChange = this.handleCarrierStatutChange.bind(this);
		window.functions.changeDate = this.changeDate.bind(this);
		window.functions.gap = this.gap.bind(this);

		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate
				.get((key.indexOf(".") < 0 ? "FREIGHT_AUDIT." : "") + key)
				.subscribe((res) => (this.ConstantsTranslate[key] = res));
		});

		document.addEventListener(
			"btnUpload",
			function(e) {
				this.uploadF = true;
			}.bind(this),
			false
		);

		this.loadCurrencyList();
		this.loadTypeOfRequest();
	}

	loadCurrencyList() {
		this.statiqueService.getListEcCurrency().subscribe((currencies) => {
			this.currencyList = currencies;
		});
	}

	loadTypeOfRequest(){
		this.freightAuditService.ListTypeDemande().subscribe((typeRequestList) => {
			this.typeRequestList = typeRequestList;
		});
	}

	fileInputHandle(ebInvoiceNum: number) {
		this.dataUpload = {
			// On the backend the controller is waiting for and integer
			// when ebInvoiceNum is not defined set it to 0
			// so we don't get ArgumentException in the backend
			ebInvoiceNum: ebInvoiceNum ? ebInvoiceNum : 0,
			typeFile: this.UploadDirectory.FREIGHT_AUDIT,
			ebUserNum: this.userConnected.ebUserNum,
		};
		this.dataUploadProgress = null;
		this.fileInput.nativeElement.value = null;
		this.fileInput.nativeElement.click();
	}

	uploadFile(output: UploadOutput) {
		this.onUploadOutput(output);
		setTimeout(
			function() {
				this.startUpload();
			}.bind(this),
			500
		);

		this.genericTable.refreshData();
	}
	emitClickPopUp() {
		let event = new CustomEvent("btnPopUp", null);
		document.dispatchEvent(event);
	}

	emitClickUpload(ebInvoiceNum: number) {
		this.ebInvoiceNum = ebInvoiceNum;

		this.dataUploadProgress = null;

		let event = new CustomEvent("btnUpload", null);
		document.dispatchEvent(event);
		this.dataUpload = {
			ebInvoiceNum: this.ebInvoiceNum,

			typeFile: this.UploadDirectory.FREIGHT_AUDIT,
			ebUserNum: this.userConnected.ebUserNum,
		};
	}

	onUploadComplete(file: UploadFile) {
		setTimeout(
			function() {
				this.dataUploadProgress = null;
			}.bind(this),
			500
		);
		try {
			let ebInvoiceNum = file.response[0].fichier.ebInvoice;
			let row: EbInvoice = this.genericTable.getDataRow(ebInvoiceNum);
			row.uploadFileNbr = (row.uploadFileNbr || 0) + 1;
			this.genericTable.setDataRow(ebInvoiceNum, row);

			let newFJ = file.response[0].fichier;

			this.newInvoiceFichierJoint = newFJ;

			this.listChat.push(file.response[0]["ebChat"]);
		} catch (e) {
			console.error("can't update upload nbr");
			console.error(e);
		}
	}

	onUploadProgress(file: UploadFile) {
		if (file && file.progress && file.progress.data && file.progress.data.percentage)
			this.dataUploadProgress = file.progress.data.percentage;
		if (this.dataUploadProgress >= 100) {
		}
	}

	uploadF_false() {
		this.uploadF = false;
	}

	popupF_false() {
		this.popupF = false;
		this.comment = false;
		if (this.doRefreshDT) {
			this.genericTable.refreshData();
		}
		this.doRefreshDT = false;
	}

	popupF_true(ebInvoiceNum) {
		this.popupF = true;
		this.doRefreshDT = false;
		this.ebInvoiceNum = ebInvoiceNum;

		this.ebInvoice = this.genericTable.getDataRow(ebInvoiceNum);
	}

	showPrice: boolean = false;
	private listTypeTransportMap: Map<number, Array<EbTypeTransport>>;
	// popup cost
	showDetailsPrice(ebDemandeNum: number, envoicenum: number) {
		this.exEbDemandeTransporteurs = null;
		this.ebInvoice = this.genericTable.getDataRow(envoicenum);
		let criteria = new SearchCriteriaPricingBooking();
		criteria.ebDemandeNum = ebDemandeNum;
		this.pricingService.getEbDemande(criteria).subscribe((res: EbDemande) => {
			this.ebDemande = res;
			this.showPriceExEbDemandeTransporteur = this.ebDemande.exEbDemandeTransporteurs
				.find(quote=> quote.status == CarrierStatus.FINAL_CHOICE || quote.status == CarrierStatus.FIN_PLAN);
			this.showPrice = true;
			this.xEbDemandeNum = this.ebDemande.ebDemandeNum;
		});
	}
	// changement de statut pour chargeur
	handleChargeurStatutChange(el) {
		this.selectedStatutRow = {
			ebInvoice: el.dataset && el.dataset.ebinvoice ? el.dataset.ebinvoice : null,
			statutChargeur: +el.value,
		};

		this.invoiceWrap = new InvoiceWrapper();
		this.invoiceWrap.lisAction = new Array<number>();
		this.invoiceWrap.listEbInvoiceNumber = new Array<number>();
		this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListStatut);

		let ebInvoice = new EbInvoice();
		ebInvoice.ebInvoiceNum = +this.selectedStatutRow.ebInvoice;
		ebInvoice.statutChargeur = this.selectedStatutRow.statutChargeur;
		ebInvoice.statutCarrier = this.selectedStatutRow.statutChargeur;

		this.listInvoice.set(ebInvoice.ebInvoiceNum, ebInvoice);
		let newList = new Array<EbInvoice>();
		this.listInvoice.forEach((it) => newList.push(it));

		this.invoiceWrap.ebInvoice = ebInvoice;
		this.invoiceWrap.listEbInvoiceNumber.push(ebInvoice.ebInvoiceNum);

		this.freighAuditService.allAction(this.invoiceWrap).subscribe(
			(data) => {
				let row: EbInvoice = this.genericTable.getDataRow(el.dataset.ebinvoice);
				row.statutChargeur = ebInvoice.statutChargeur;
				this.genericTable.setDataRow(el.dataset.ebinvoice, row);

				this.messageService.add({
					severity: "success",
					summary: "Operation Success",
					detail: "Update Statut",
				});
			},
			(error) => {
				let title = null;
				let body = null;
				this.translate.get("MODAL.ERROR").subscribe((res: string) => {
					title = res;
				});

				this.modalService.error(title, null, function() {});
			}
		);

		this.listInvoice.delete(ebInvoice.ebInvoiceNum);
	}
	// changement de statut pour carrier
	handleCarrierStatutChange(el) {
		this.selectedStatutRow = {
			ebInvoice: el.dataset && el.dataset.ebinvoice ? el.dataset.ebinvoice : null,
			statutCarrier: +el.value,
		};

		this.invoiceWrap = new InvoiceWrapper();
		this.invoiceWrap.lisAction = new Array<number>();
		this.invoiceWrap.listEbInvoiceNumber = new Array<number>();
		this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListStatut);

		let ebInvoice = new EbInvoice();
		ebInvoice.ebInvoiceNum = +this.selectedStatutRow.ebInvoice;

		ebInvoice.statutCarrier = this.selectedStatutRow.statutCarrier;

		this.listInvoice.set(ebInvoice.ebInvoiceNum, ebInvoice);
		let newList = new Array<EbInvoice>();
		this.listInvoice.forEach((it) => newList.push(it));

		this.invoiceWrap.ebInvoice = ebInvoice;
		this.invoiceWrap.listEbInvoiceNumber.push(ebInvoice.ebInvoiceNum);

		this.freighAuditService.allAction(this.invoiceWrap).subscribe(
			(data) => {
				let row: EbInvoice = this.genericTable.getDataRow(el.dataset.ebinvoice);
				row.statutCarrier = ebInvoice.statutCarrier;
				this.genericTable.setDataRow(el.dataset.ebinvoice, row);
				this.messageService.add({
					severity: "success",
					summary: "Operation Success",
					detail: "Update Statut",
				});
			},
			(error) => {
				let title = null;
				let body = null;
				this.translate.get("MODAL.ERROR").subscribe((res: string) => {
					title = res;
				});

				this.modalService.error(title, null, function() {});
			}
		);

		this.listInvoice.delete(ebInvoice.ebInvoiceNum);
	}

	gap(negotiatedPrice: number, applyChargeurInvoicePrice: number) {
		this.res = applyChargeurInvoicePrice - negotiatedPrice;
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["module"] || (changes["searchInput"] && changes["searchInput"].firstChange)) {
			this.initDatatableTest();
		}
	}

	//ajouter memo chargeur dans la table et aller a update memo chargeur pour la base donne
	addMemoChargeur(listInvoice: Map<number, EbInvoice>, eb_invoice_num: number, event: HTMLElement) {
		this.invoiceWrap = new InvoiceWrapper();
		this.invoiceWrap.lisAction = new Array<number>();
		this.invoiceWrap.listEbInvoiceNumber = new Array<number>();
		this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListMemo);

		let ebInvoice: EbInvoice = new EbInvoice();
		ebInvoice.ebInvoiceNum = eb_invoice_num;
		ebInvoice.memoChageur = event.parentNode["getElementsByTagName"]("input")[0].value;
		ebInvoice.memoCarrier = ebInvoice.memoChageur;

		this.invoiceWrap.ebInvoice = ebInvoice;
		this.invoiceWrap.listEbInvoiceNumber.push(ebInvoice.ebInvoiceNum);

		listInvoice.set(eb_invoice_num, ebInvoice);

		let newList = new Array<EbInvoice>();
		listInvoice.forEach((it) => newList.push(it));
		this.freighAuditService.allAction(this.invoiceWrap).subscribe(
			(data) => {
				let row: EbInvoice = this.genericTable.getDataRow(eb_invoice_num);
				row.memoChageur = ebInvoice.memoChageur;
				this.genericTable.setDataRow(eb_invoice_num, row);
				this.messageService.add({
					severity: "success",
					summary: "Operation Success",
					detail: "Update Memo",
				});
			},
			(error) => {
				let title = null;
				let body = null;
				this.translate.get("MODAL.ERROR").subscribe((res: string) => {
					title = res;
				});

				this.modalService.error(title, null, function() {});
			}
		);
	}

	// mise a jour memo sur la table et dans la bdd
	addMemoCarrier(listInvoice: Map<number, EbInvoice>, eb_invoice_num: number, event: HTMLElement) {
		this.invoiceWrap = new InvoiceWrapper();
		this.invoiceWrap.lisAction = new Array<number>();
		this.invoiceWrap.listEbInvoiceNumber = new Array<number>();
		this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListMemo);

		let ebInvoice: EbInvoice = new EbInvoice();
		ebInvoice.ebInvoiceNum = eb_invoice_num;
		ebInvoice.memoCarrier = event.parentNode["getElementsByTagName"]("input")[0].value;

		this.invoiceWrap.ebInvoice = ebInvoice;
		this.invoiceWrap.listEbInvoiceNumber.push(ebInvoice.ebInvoiceNum);

		listInvoice.set(eb_invoice_num, ebInvoice);

		let newList = new Array<EbInvoice>();
		listInvoice.forEach((it) => newList.push(it));
		this.freighAuditService.allAction(this.invoiceWrap).subscribe(
			(data) => {
				let row: EbInvoice = this.genericTable.getDataRow(eb_invoice_num);
				row.memoCarrier = ebInvoice.memoCarrier;
				this.genericTable.setDataRow(eb_invoice_num, row);
				this.messageService.add({
					severity: "success",
					summary: "Operation Success",
					detail: "Update Memo",
				});
			},
			(error) => {
				let title = null;
				let body = null;
				this.translate.get("MODAL.ERROR").subscribe((res: string) => {
					title = res;
				});

				this.modalService.error(title, null, function() {});
			}
		);
	}

	initDatatableTest() {
		this.globalSearchCriteria.pageNumber = 1;
		this.globalSearchCriteria.module = this.module;
		this.globalSearchCriteria.ebUserNum = this.userConnected.ebUserNum;

		this.activatedRoute.queryParams.subscribe((params) => {
			this.globalSearchCriteria.idObject = +params["invoice"] || null;

			this.dataInfos.cols = this.getColumns();
		});

		this.dataInfos.dataKey = "ebInvoiceNum";
		this.dataInfos.showSubTable = false;
		this.dataInfos.showAdvancedSearchBtn = true;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = this.statique.controllerFreightAudit + "/listinvoice";
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.lineActions = [];
		this.dataInfos.extractWithAdvancedSearch = true;
		this.dataInfos.showCheckbox = true;
		this.dataInfos.showCheckboxWidth = "82px";

		this.dataInfos.showFlags = true;
		this.dataInfos.dataFlagsField = "listEbFlagDTO";
		this.dataInfos.onFlagChange = this.onFlagChange;
		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.actionColumFixed = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.blankBtnCount = 0;
		this.dataInfos.actionColumFixed = false;

		this.dataInfos.ccpTooltipRender = (row: EbInvoice): string => {
			if (row == null || row.chargeur == null || row.chargeur.ebCompagnie == null) return "";
			return row.chargeur.ebCompagnie.nom;
		};
	}

	renderDocStatut(classname: string, name: string, number: number): string {
		return `<span class='doc-status ${classname}'>${name}(${number == null ? 0 : number});</span>`;
	}

	addDays(date: Date, days: number): Date {
		date.setDate(date.getDate() + days);
		return date;
	}

	getColumns(): Array<GenericTableInfos.Col> {
		const $this = this;
		let cols: Array<GenericTableInfos.Col> = [
			{
				field: "listEbFlagDTO",
				translateCode: "PRICING_BOOKING.FLAG",
				minWidth: "125px",
				allowClick: false,
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: FlagColumnGenericCell,
				genericCellParams: new FlagColumnGenericCell.Params({
					field: "listEbFlagDTO",
					contributionAccess: this.contributionAccess,
					moduleNum : $this.module,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{
				field: "refDemande",
				translateCode: "FREIGHT_AUDIT.TRANSPORT_REF",
				headerClass: "champNonModifiable",
			},
			{
				field: "totalWeight",
				translateCode: "FREIGHT_AUDIT.TAXABLE_WEIGHT",
				headerClass: "champNonModifiable",
				width: "155px",
			},

			{
				translateCode: "FREIGHT_AUDIT.MODE_TRANSPORT",
				headerClass: "champNonModifiable",
				width: "155px",
				field: "xEcModeTransport",
				render: function(modeTransportCode, type, row) {
					if (!modeTransportCode) return "";
					return SingletonStatique.getModeTransportString(
						modeTransportCode,
						row,
						this.listTypeTransportMap
					);
				}.bind(this),
			},
			{
				field: "xEcIncotermLibelle",
				translateCode: "FREIGHT_AUDIT.INCOTERMS",
				headerClass: "champNonModifiable",
			},
			{
				translateCode: "FREIGHT_AUDIT.NUM_AWB_BOL",
				headerClass: "champNonModifiable",
				field: "numAwbBol",
			},
			{
				field: "libelleDestCountry",
				translateCode: "FREIGHT_AUDIT.DEST_COUNTRY",
				headerClass: "champNonModifiable",
			},
			{
				field: "libelleDestCity",
				translateCode: "FREIGHT_AUDIT.DEST_CITY",
				headerClass: "champNonModifiable",
			},
			{
				field: "libelleOriginCountry",
				translateCode: "FREIGHT_AUDIT.ORIGI_OF_COUNTRY",
				headerClass: "champNonModifiable",
			},
			{
				field: "libelleOriginCity",
				translateCode: "FREIGHT_AUDIT.ORIG_CITY",
				headerClass: "champNonModifiable",
			},
			{
				translateCode: "PRICING_BOOKING.TYPE_OF_REQUEST",
				headerClass: "champNonModifiable",
				field: "typeRequestLibelle",
			},
			{
				translateCode: "FREIGHT_AUDIT.NEGOTIATED_PRICE",
				field: "preCarrierCost",
				headerClass: "champNonModifiable",
				width: "165px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					let negociatedPrice = data != null ? Number.parseFloat(data).toFixed(2) : data;
					return "<span class='gap1-${ebInvoice.ebInvoiceNum}'>" + negociatedPrice + "</span>";
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.REQUESTOR",
				field: "xEbUserOwnerRequest",
				width: "180px",
				render: function(data, type, row) {
					return row.xEbUserOwnerRequest.nomPrenom;
				}.bind(this),
			},
			{
				field: "currencyLabel",
				translateCode: "FREIGHT_AUDIT.INVOICE_CURRENCY",
				width: "165px",
				headerClass: "champNonModifiable",
			},

			{
				translateCode: "FREIGHT_AUDIT.DATE_PICKUP",
				field: "datePickup",
				headerClass: "champNonModifiable",
				width: "100px",
				render: (data) =>{
					return this.statique.formatDate(data);
				},
			},
			{
				translateCode: "FREIGHT_AUDIT.DATE_DELIVER",
				field: "dateDelivery",
				headerClass: "champNonModifiable",
				width: "100px",
				render: function(data) {
					return this.statique.formatDate(data);
				}.bind(this),
			},

			{
				translateCode: "PRICING_BOOKING.STATUT_DOCUMENT",
				field: "listTypeDocuments",
				width: "160px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, d, row) {
					let libelle = "";
					let titleLibelle = "";
					let libelleWrapper = "";
					if (row.listTypeDocuments != null && row.listTypeDocuments.length > 0) {
						row.listTypeDocuments.forEach((element: EbTypeDocuments) => {
							let loopLibelle = "";
							let classname = "";
							if (element.nbrDoc == 0 || element.nbrDoc == null) {
								classname = "red";
							} else {
								classname = "blue";
							}

							loopLibelle = $this.renderDocStatut(classname, element.code, element.nbrDoc);

							titleLibelle += element.code + "(" + element.nbrDoc + "); ";
							libelle += loopLibelle;
						});
						titleLibelle = titleLibelle.substring(0, libelle.length - 2);
						libelleWrapper =
							`<div class='docs__wrapper' title='` +
							titleLibelle +
							`' onclick='functions.popupF_true(" ` +
							row.ebInvoiceNum +
							`")'  >` +
							libelle +
							`</div>`;
					}
					return libelleWrapper;
				}.bind(this),
			},

			{
				translateCode: "FREIGHT_AUDIT.DOCUMENTS_HISTORIQUES",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				field: "commentaire",
				width: "108px",
				allowClick: true,
				render: function(data, type, row) {
					return (
						"<div class='btn btn-primary btn-xs' role='button' onclick='functions.popupF_true(" +
						row.ebInvoiceNum +
						")'>Doc & His</div>"
					);
				}.bind(this),
			},

			{
				translateCode: "FREIGHT_AUDIT.DETAILS",
				field: "xEbDemandeNum",
				headerClass: "champNonModifiable",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				allowClick: true,
				width: "72px",
				render: function(data, type, row) {
					return (
						"<div class='btn btn-primary btn-xs' role='button' onclick='window.functions.showDetailsPrice(" +
						data +
						"," +
						row.ebInvoiceNum +
						")'>Details</div>"
					);
				}.bind(this),
			},
			{
				field: "exchangeRate",
				translateCode: "PRICING_BOOKING.EXCHANGE_RATE",
				headerClass: "champNonModifiable",
				width: "160px",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, d, row) {
					let exchangeRate = "";
					let currency: Array<EcCurrency>;
					let set = new Set();
					if (row.listCostCategorieTransporteur != null) {
						row.listCostCategorieTransporteur &&
							row.listCostCategorieTransporteur.forEach((transporteur) => {
								transporteur.listEbCost &&
									transporteur.listEbCost.forEach((cost) => {
										if (cost.price != null) {
											currency = this.currencyList.filter(function(c) {
												return c.ecCurrencyNum == cost.xEcCurrencyNum;
											});
											currency.forEach((c) => {
												if(cost.euroExchangeRate != null) {
													set.add(
														c.code +
														" -> " +
														row.currencyLabel +
														" : " +
														cost.euroExchangeRate +
														"<br>"
													);
												}
											});
										}
									});
							});

						if (set != null && set.size != 0) {
							set.forEach((value, valueAgain, set) => {
								exchangeRate += value;
							});
						}
						return exchangeRate;
					}
					if (row.listCostCategorieCharger != null) {
						row.listCostCategorieCharger &&
							row.listCostCategorieCharger.forEach((chargeur) => {
								chargeur.listEbCost &&
									chargeur.listEbCost.forEach((cost) => {
										if (cost.price != null) {
											currency = this.currencyList.filter(function(c) {
												return c.ecCurrencyNum == cost.xEcCurrencyNum;
											});
											currency.forEach((c) => {
												if(cost.euroExchangeRate != null) {
													set.add(
														c.code +
														" -> " +
														row.currencyLabel +
														" : " +
														cost.euroExchangeRate +
														"<br>"
													);
												}
											});
										}
									});
							});

						if (set != null && set.size != 0) {
							set.forEach((value, valueAgain, set) => {
								exchangeRate += value;
							});
						}
						return exchangeRate;
					}
					return exchangeRate;
				}.bind(this),
			},
		];

		let datatableColmunMemoChargeur = [
			{
				translateCode: "FREIGHT_AUDIT.MEMO",
				field: "memoChageur",
				headerClass: "champItalique",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type?, row?) {
					return this.freightAuditService.getHtmlForChargeurMemo(
						this.listFreightAudit,
						row.ebInvoiceNum,
						data,
						this.disabled
					);
				}.bind(this),
			},
		];

		let datatableColmunMemoCarrier = [
			{
				translateCode: "FREIGHT_AUDIT.MEMO",
				field: "memoCarrier",
				headerClass: "champItalique",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type?, row?) {
					return this.freightAuditService.getHtmlForCarrierMemo(
						this.listFreightAudit,
						row.ebInvoiceNum,
						data,
						this.disabled
					);
				}.bind(this),
			},
		];

		let datatableColmunInvoiceNumberCarrier = [
			{
				translateCode: "FREIGHT_AUDIT.INVOICE_NUMBER",
				field: "applyCarrierIvoiceNumber",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type?, row?) {
					return this.freightAuditService.getHtmlForCarrierInvoiceNumber(
						this.listFreightAudit,
						row.ebInvoiceNum,
						data,
						row.numberArraw,
						this.disabled
					);
				}.bind(this),
			},
		];

		let datatableColmunInvoiceNumberChargeur = [
			{
				translateCode: "FREIGHT_AUDIT.INVOICE_NUMBER",
				field: "applyChargeurIvoiceNumber",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type?, row?) {
					return this.freightAuditService.getHtmlForChargeurInvoiceNumber(
						this.listFreightAudit,
						row.ebInvoiceNum,
						data,
						row.numberArraw,
						this.disabled
					);
				}.bind(this),
			},
		];

		let datatableColmunInvoicePriceChargeur = [
			{
				translateCode: "FREIGHT_AUDIT.INVOICE_PRICE",
				field: "applyChargeurIvoicePrice",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type?, row?) {
					let invoicePrice = data != null ? Number.parseFloat(data).toFixed(2) : data;
					return this.freightAuditService.getHtmlForChargeurInvoicePrice(
						this.listFreightAudit,
						row.ebInvoiceNum,
						invoicePrice,
						row.priceArraw,
						row.negotiatedPrice,
						this.disabled
					);
				}.bind(this),
			},
		];

		let datatableColmunInvoicePriceCarrier = [
			{
				translateCode: "FREIGHT_AUDIT.INVOICE_PRICE",
				field: "applyCarrierIvoicePrice",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type?, row?) {
					let invoicePrice = data != null ? Number.parseFloat(data).toFixed(2) : data;
					return this.freightAuditService.getHtmlForCarrierInvoicePrice(
						this.listFreightAudit,
						row.ebInvoiceNum,
						invoicePrice,
						row.priceArraw,
						row.negotiatedPrice,
						this.disabled
					);
				}.bind(this),
			},
		];

		let datatableColmunStatutChargeur = [
			{
				translateCode: "FREIGHT_AUDIT.STATUT",
				field: "statutChargeur",
				//headerClass: "withStatut",
				width: "180px",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, tmp, ebInvoice: EbInvoice) {
					let options = "";
					Statique.InvoiceStatus.forEach((it) => {
						if (it.key == 1 || it.key == 2) {
							if (data == it.key) options += "<option selected>" + it.value + "</option>";
						}
						if (it.key == 3 || it.key == 4 || it.key == 5)
							options +=
								'<option value="' +
								it.key +
								'" ' +
								(it.key == data ? "selected" : "") +
								">" +
								it.value +
								"</option>";
					});
					if (ebInvoice.statutArraw == 1) {
						return (
							'<span class="send-arrow fa fa-sign-out"><select '+this.disabled+' class="form-control border-on-hover widthSelectStatus statut-' +
							ebInvoice.ebInvoiceNum +
							'"  data-ebinvoice="' +
							ebInvoice.ebInvoiceNum +
							'" name="statut" onchange="window.functions.handleChargeurStatutChange(this)">' +
							options +
							"</select></span>"
						);
					} else if (ebInvoice.statutArraw == 2) {
						return (
							'<span class="receive-arrow fa fa-sign-out"><select '+this.disabled+' class="form-control border-on-hover widthSelectStatus statut-' +
							ebInvoice.ebInvoiceNum +
							'"  data-ebinvoice="' +
							ebInvoice.ebInvoiceNum +
							'" name="statut" onchange="window.functions.handleChargeurStatutChange(this)">' +
							options +
							"</select></span>"
						);
					} else if (ebInvoice.statutArraw == null || ebInvoice.statutArraw == 0) {
						return (
							'<select '+this.disabled+' class="form-control border-on-hover widthSelectStatus statut-' +
							ebInvoice.ebInvoiceNum +
							'"  data-ebinvoice="' +
							ebInvoice.ebInvoiceNum +
							'" name="statut" onchange="window.functions.handleChargeurStatutChange(this)">' +
							options +
							"</select>"
						);
					}
				}.bind(this),
			},
		];

		let datatableColmunStatutCarrier: Array<GenericTableInfos.Col> = [
			{
				translateCode: "FREIGHT_AUDIT.STATUT",
				field: "statutCarrier",
				allowClick: true,
				width: "180px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, tmp, ebInvoice: EbInvoice) {
					let options = "";
					Statique.InvoiceStatus.forEach((it) => {
						if (it.key == 3 || it.key == 4 || it.key == 5) {
							if (data == it.key) options += "<option selected>" + it.value + "</option>";
						}

						if (it.key == 1 || it.key == 2)
							options +=
								'<option value="' +
								it.key +
								'" ' +
								(it.key == data ? "selected" : "") +
								">" +
								it.value +
								"</option>";
					});

					if (ebInvoice.statutArraw == 1) {
						return (
							'<span class="receive-arrow fa fa-sign-out"><select '+this.disabled+' class="form-control border-on-hover widthSelectStatus statut-' +
							ebInvoice.ebInvoiceNum +
							'"  data-ebinvoice="' +
							ebInvoice.ebInvoiceNum +
							'" name="statut" onchange="window.functions.handleCarrierStatutChange(this)">' +
							options +
							"</select></span>"
						);
					}
					if (ebInvoice.statutArraw == 2) {
						return (
							'<span class="send-arrow fa fa-sign-out"><select '+this.disabled+' class="form-control border-on-hover widthSelectStatus statut-' +
							ebInvoice.ebInvoiceNum +
							'"  data-ebinvoice="' +
							ebInvoice.ebInvoiceNum +
							'" name="statut" onchange="window.functions.handleCarrierStatutChange(this)">' +
							options +
							"</select></span>"
						);
					}
					if (ebInvoice.statutArraw == null || ebInvoice.statutArraw == 0) {
						return (
							'<select '+this.disabled+' class="form-control border-on-hover widthSelectStatus statut-' +
							ebInvoice.ebInvoiceNum +
							'"  data-ebinvoice="' +
							ebInvoice.ebInvoiceNum +
							'" name="statut" onchange="window.functions.handleCarrierStatutChange(this)">' +
							options +
							"</select>"
						);
					}
				}.bind(this),
			},
		];

		let datatableColmunGapChargeur: Array<GenericTableInfos.Col> = [
			{
				translateCode: "FREIGHT_AUDIT.GAP",
				field: "gapChargeur",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, disp, row) {
					let gap: string = "";
					// toFixed(2) used to : rounding numbers to 2 digits after comma
					if (row.gapChargeur != null)
						gap = '<span class="gap-' + row.ebInvoiceNum + '">' + data.toFixed(2) + "</span>";
					return gap;
				}.bind(this),
			},
		];

		let datatableColmunGapCarrier: Array<GenericTableInfos.Col> = [
			{
				translateCode: "FREIGHT_AUDIT.GAP",
				field: "gapCarrier",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, disp, row) {
					let gap: string = "";
					// toFixed(2) used to : rounding numbers to 2 digits after comma
					if (row.gapCarrier != null)
						gap = '<span class="gap-' + row.ebInvoiceNum + '">' + data.toFixed(2) + "</span>";
					return gap;
				}.bind(this),
			},
		];
		const userRoleField = this.userConnected.role == UserRole.CHARGEUR ? "invoiceDateChargeur" : "invoiceDateCarrier";
		let datatableColmunDateChargeur: Array<GenericTableInfos.Col> = [
			{
				translateCode: "FREIGHT_AUDIT.INVOICE_DATE",
				field: userRoleField,
				allowClick: true,
				width: "118px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data: Date, tmp, ebInvoice: EbInvoice) {
					let listDate: Array<Date> = new Array<Date>();
					let dt = null;
					for (let i = -6; i < 7; i++) {
						if (data == null) {
							dt = new Date();
						} else {
							dt = new Date(data);
						}
						dt.setMonth(dt.getMonth() + i);
						listDate.push(dt);
					}
					let str;
					let array;

					let i = -6;
					listDate.forEach((it) => {
						if (i < 7) {
							if (i == 0) {
								str +=
									'<option selected style= "background-color:green">' +
									Statique.invoiceMonths2[it.getMonth()].value +
									"  " +
									it.getFullYear() +
									"</option>";
							} else {
								str +=
									"<option>" +
									Statique.invoiceMonths2[it.getMonth()].value +
									"  " +
									it.getFullYear() +
									"</option>";
							}
						}
						i++;
					});
					if (ebInvoice.dateArraw == 1) {
						return (
							'<span class="send-arrow fa fa-sign-out"><select '+this.disabled+'  class="Date-' +
							ebInvoice.ebInvoiceNum +
							' form-control border-on-hover widthSelectDate"  onchange="window.functions.changeDate(' +
							ebInvoice.ebInvoiceNum +
							',event.target)">' +
							str +
							"</select></span>"
						);
					} else if (ebInvoice.dateArraw == 2) {
						return (
							'<span class="receive-arrow fa fa-sign-out"><select '+this.disabled+' class="Date-' +
							ebInvoice.ebInvoiceNum +
							' form-control border-on-hover widthSelectDate"  onchange="window.functions.changeDate(' +
							ebInvoice.ebInvoiceNum +
							',event.target)">' +
							str +
							"</select></span>"
						);
					} else if (ebInvoice.dateArraw == null || ebInvoice.dateArraw == 0) {
						return (
							'<select '+this.disabled+' class="Date-' +
							ebInvoice.ebInvoiceNum +
							' form-control border-on-hover widthSelectDate"  onchange="window.functions.changeDate(' +
							ebInvoice.ebInvoiceNum +
							',event.target)">' +
							str +
							"</select>"
						);
					}
				}.bind(this),
			},
		];

		let datatableColmunDateCarrier: Array<GenericTableInfos.Col> = [
			{
				translateCode: "FREIGHT_AUDIT.INVOICE_DATE",
				field: userRoleField,
				width: "118px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data: Date, tmp, ebInvoice: EbInvoice) {
					let listDate: Array<Date> = new Array<Date>();
					let dt = null;
					for (let i = -6; i < 7; i++) {
						if (data == null) {
							dt = new Date();
						} else {
							dt = new Date(data);
						}
						dt.setMonth(dt.getMonth() + i);
						listDate.push(dt);
					}
					let str;
					let array;

					let i = -6;
					listDate.forEach((it) => {
						if (i < 7) {
							if (i == 0) {
								str +=
									'<option selected style= "background-color:green">' +
									Statique.invoiceMonths2[it.getMonth()].value +
									"  " +
									it.getFullYear() +
									"</option>";
							} else {
								str +=
									"<option>" +
									Statique.invoiceMonths2[it.getMonth()].value +
									"  " +
									it.getFullYear() +
									"</option>";
							}
						}
						i++;
					});

					if (ebInvoice.dateArraw == 1) {
						return (
							'<span class="receive-arrow fa fa-sign-out"><select '+this.disabled+' class="Date-' +
							ebInvoice.ebInvoiceNum +
							' form-control border-on-hover" style="width:110px " onchange="window.functions.changeDate(' +
							ebInvoice.ebInvoiceNum +
							',event.target)">' +
							str +
							"</select></span>"
						);
					} else if (ebInvoice.dateArraw == 2) {
						return (
							'<span class="send-arrow fa fa-sign-out"><select '+this.disabled+'  class="Date-' +
							ebInvoice.ebInvoiceNum +
							' form-control border-on-hover" style="width:110px " onchange="window.functions.changeDate(' +
							ebInvoice.ebInvoiceNum +
							',event.target)">' +
							str +
							"</select></span>"
						);
					} else if (ebInvoice.dateArraw == null || ebInvoice.dateArraw == 0) {
						return (
							'<select '+this.disabled+' class="Date-' +
							ebInvoice.ebInvoiceNum +
							' form-control border-on-hover" style="width:110px " onchange="window.functions.changeDate(' +
							ebInvoice.ebInvoiceNum +
							',event.target)">' +
							str +
							"</select>"
						);
					}
				}.bind(this),
			},
		];

		let Carrier: Array<GenericTableInfos.Col> = [
			{
				translateCode: "FREIGHT_AUDIT.CARRIER",
				field: "carrier",
				headerClass: "champNonModifiable",
				render: (data) => {
					return data.nomPrenom;
				},
			},
			{
				translateCode: "FREIGHT_AUDIT.CARRIER_COMPANY",
				field: "nomCompanyCarrier",
				headerClass: "champNonModifiable",
				render: (data) => {
					return data;
				},
			},
			{
				translateCode: "FREIGHT_AUDIT.CARRIER_ESTABLISHMENT",
				field: "nomEtablissementCarrier",
				headerClass: "champNonModifiable",
				render: (data) => {
					return data;
				},
			}
		];

		let Chargeur: Array<GenericTableInfos.Col> = [
			{
				translateCode: "FREIGHT_AUDIT.CHARGEUR",
				field: "chargeur",
				headerClass: "champNonModifiable",
				render: (data) => {
					return data.compagnieNameNomUserPrenomUser;
				},
			},
		];

		if (this.userConnected.role == UserRole.CHARGEUR) {
			datatableColmunMemoChargeur.forEach((it) => {
				cols.splice(12, 0, it);
			});

			datatableColmunInvoiceNumberChargeur.forEach((it) => {
				cols.splice(14, 0, it);
			});
			datatableColmunInvoicePriceChargeur.forEach((it) => {
				cols.splice(15, 0, it);
			});

			datatableColmunGapChargeur.forEach((it) => {
				cols.splice(21, 0, it);
			});
			datatableColmunDateChargeur.forEach((it) => {
				cols.splice(22, 0, it);
			});
			Carrier.forEach((it) => {
				cols.splice(2, 0, it);
			});
			datatableColmunStatutChargeur.forEach((it) => {
				cols.splice(23, 0, it);
			});
		}

		if (this.userConnected.role == UserRole.PRESTATAIRE) {
			datatableColmunMemoCarrier.forEach((it) => {
				cols.splice(12, 0, it);
			});

			datatableColmunInvoiceNumberCarrier.forEach((it) => {
				cols.splice(14, 0, it);
			});
			datatableColmunInvoicePriceCarrier.forEach((it) => {
				cols.splice(15, 0, it);
			});

			datatableColmunGapCarrier.forEach((it) => {
				cols.splice(21, 0, it);
			});
			datatableColmunDateCarrier.forEach((it) => {
				cols.splice(22, 0, it);
			});
			Chargeur.forEach((it) => {
				cols.splice(2, 0, it);
			});
			datatableColmunStatutCarrier.forEach((it) => {
				cols.splice(23, 0, it);
			});
		}

		if (this.userConnected.role == UserRole.CONTROL_TOWER) {
			datatableColmunMemoChargeur.forEach((it) => {
				cols.splice(12, 0, it);
			});

			datatableColmunInvoiceNumberChargeur.forEach((it) => {
				cols.splice(14, 0, it);
			});
			datatableColmunInvoicePriceChargeur.forEach((it) => {
				cols.splice(15, 0, it);
			});

			datatableColmunGapChargeur.forEach((it) => {
				cols.splice(21, 0, it);
			});
			datatableColmunDateChargeur.forEach((it) => {
				cols.splice(22, 0, it);
			});
			Chargeur.forEach((it) => {
				cols.splice(2, 0, it);
			});
			Carrier.forEach((it) => {
				cols.splice(2, 0, it);
			});

			datatableColmunStatutChargeur.forEach((it) => {
				cols.splice(23, 0, it);
			});
		}

		cols.forEach((it) => {
			this.translate.get(it.translateCode).subscribe((res) => (it["header"] = res));
		});

		return cols;
	}
	onListCategorieLoaded(listCategorie: Array<EbCategorie>) {
		this.listCategorieEmitter.emit(listCategorie);
	}
	onListFieldLoaded(listField: Array<IField>) {
		this.listFieldsEmitter.emit(listField);
	}
	async getStatiques() {
		this.listTypeTransportMap = await SingletonStatique.getListTypeTransportMap();
	}

	getListInvoice(): Map<number, EbInvoice> {
		let data = this.genericTable.getSelectedRows();
		let mapData = new Map<number, EbInvoice>();

		this.listInvoice = new Map<number, EbInvoice>();
		data.forEach((it) => {
			mapData.set(it.ebInvoiceNum, it);
		});
		this.listInvoice = mapData;

		return mapData;
	}

	ModalRecupPriceAndGapCotation(data: Object) {
		if (data && data["invoice"]) this.recupInvoiceModalAddEvent(data["invoice"]);
	}

	recupInvoiceModalAddEvent(Invoice: EbInvoice) {
		this.genericTable.setDataRow(Invoice.ebInvoiceNum, Invoice);
	}
	uploadFacture() {
		this.uploaderFacture = true;
	}

	closeUploadFacture() {
		this.uploaderFacture = false;
	}

	OpenPopUpAction() {
		this.OpenToglePopUpAction = !this.OpenToglePopUpAction;
		this.invoiceCopy = new EbInvoice();
		this.getListInvoice();
		this.TogleApplyAction = false;
		this.TogleShareAction = false;
	}

	OpenBlocShareAction() {
		this.TogleShareAction = !this.TogleShareAction;
	}

	OpenBlocApplyAction() {
		this.TogleApplyAction = !this.TogleApplyAction;
		this.getListInvoice();
	}

	onFlagChange(context: any, row: EbInvoice, listFlag: Array<EbFlag>) {
		let listFlagCode = EbFlag.convertListToCommaSeparated(listFlag);
		row.listFlag = listFlagCode;
		context.freightAuditService.saveFlags(row.ebInvoiceNum, listFlagCode).subscribe();
	}

	// all action update and share a partir de la popup Action/ share
	UpdateListInvoice(event: Event) {
		this.invoiceWrap = new InvoiceWrapper();
		this.invoiceWrap.lisAction = new Array<number>();
		this.invoiceWrap.ebInvoice = this.invoiceCopy;

		let listEbInvoiceNumber: Array<number> = [];

		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.listInvoice.forEach((invoice: EbInvoice) => {
			listEbInvoiceNumber.push(invoice.ebInvoiceNum);
		});
		this.invoiceWrap.listEbInvoiceNumber = listEbInvoiceNumber;

		if (this.TogleApplyAction) {
			if ((<HTMLInputElement>document.getElementById("inputPrice")).value != "") {
				this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListInvoicePrice);
			}
			if ((<HTMLInputElement>document.getElementById("inputNumber")).value != "") {
				this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListInvoiceNumber);
			}

			if ((<HTMLInputElement>document.getElementById("inputStatus")).value != "") {
				this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListStatut);
			}

			if ((<HTMLInputElement>document.getElementById("inputDate")).value != "") {
				this.invoiceWrap.ebInvoice.invoiceDateCarrier = new Date(
					(<HTMLInputElement>document.getElementById("inputDate")).value
				);
				this.invoiceWrap.lisAction.push(ActionFreightAudit.applyListDate);
			}
		}

		if (this.TogleShareAction) {
			if ((<HTMLInputElement>document.getElementById("checkStatus")).checked) {
				this.invoiceWrap.lisAction.push(ActionFreightAudit.sharedStatus);
			}

			if ((<HTMLInputElement>document.getElementById("checkPrice")).checked) {
				this.invoiceWrap.lisAction.push(ActionFreightAudit.sharedPrice);
			}

			if ((<HTMLInputElement>document.getElementById("checkNumber")).checked) {
				this.invoiceWrap.lisAction.push(ActionFreightAudit.sharedNumber);
			}

			if ((<HTMLInputElement>document.getElementById("checkDate")).checked) {
				this.invoiceWrap.lisAction.push(ActionFreightAudit.sharedMonthAndYear);
			}
		}
		this.invoiceWrap.lisAction.sort(function(a, b) {
			return a - b;
		});

		this.freighAuditService.allAction(this.invoiceWrap).subscribe(
			(data) => {
				requestProcessing.afterGetResponse(event);
				this.OpenPopUpAction();

				this.genericTable.refreshData();
				this.messageService.add({
					severity: "success",
					summary: "Operation Success",
					detail: "success of the action",
				});
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				let title = null;
				let body = null;
				this.OpenPopUpAction();
				this.translate.get("MODAL.ERROR").subscribe((res: string) => {
					title = res;
				});

				this.modalService.error(title, null, function() {});
			}
		);
	}

	onCheckRow() {
		this.getListTransportInvoice();
	}

	getListTransportInvoice() {
		let data = this.genericTable.getSelectedRows();
		let mapData = new Map<number, EbInvoice>();

		this.listInvoiceSelected = new Map<number, EbInvoice>();
		data.forEach((it) => {
			mapData.set(it.ebInvoiceNum, it);
		});

		this.listInvoiceSelected = mapData;
	}

	actionButtons: Array<HeaderInfos.ActionButton> = [
		//TODO MEP-ce bout de code devra être réactivé après MEP
		/*
		{
			label: "QUALITY_MANAGEMENT.COMMENT",
			icon: "fa fa-plus-circle",
			action: () => {
				this.commentaire();
				let inv = new EbInvoice();
				inv.statut = this.ebInvoice.statut;
				inv.ebInvoiceNum = this.ebInvoice.ebInvoiceNum;
				this.freightAuditService.changeStatutInvoice(inv).subscribe();
			},
			enableCondition: () => {
				return this.listInvoiceSelected && this.listInvoiceSelected.size > 0;
			},
		},
		*/
		/*
		{
			label: "Upload",
			icon: "fa fa-plus-circle",
			action: () => this.uploadFacture(),
			enableCondition: () => {
				return this.listInvoiceSelected && this.listInvoiceSelected.size > 0;
			},
		},*/
		//TODO MEP-ce bout de code devra être réactivé après MEP
		/*
		{
			label: "New invoice",
			icon: "fa fa-plus-circle",
			action: () => {},
		},
		*/
		{
			label: "FREIGHT_AUDIT.APPLY_SHARE",
			icon: "fa fa-plus-circle",
			action: () => this.OpenPopUpAction(),
			enableCondition: () => {
				return this.listInvoiceSelected && this.listInvoiceSelected.size > 0 && this.contributionAccess;
			},
		},
	];

	inlineEditingHandler(key: string, value: any, model: any, context: any) {
		if (key === "selection-change") {
			context.autoSaveRow(model, value);
		}
	}

	autoSaveRow(row: EbInvoice, listFlag: Array<EbFlag>) {
		let listFlagCode = EbFlag.convertListToCommaSeparated(listFlag);
		row.listFlag = listFlagCode;
		this.freighAuditService.saveFlags(row.ebInvoiceNum, listFlagCode).subscribe();
	}
}
