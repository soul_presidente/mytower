import { Component, Input, OnChanges, OnInit } from "@angular/core";
import { EbUser } from "@app/classes/user";
import { EbChat } from "@app/classes/chat";
import { Statique } from "@app/utils/statique";
import { ChatService } from "@app/services/chat.service";
import { Modules } from "@app/utils/enumeration";
import { TranslateService } from "@ngx-translate/core";
@Component({
	selector: "app-chat-panel-freight-audit",
	templateUrl: "./chat-panel-freight-audit.component.html",
	styleUrls: ["./chat-panel-freight-audit.component.scss"],
})
export class ChatPanelFreightAuditComponent implements OnInit, OnChanges {
	statique = Statique;
	mod = Modules;

	@Input()
	public dataSource: string;
	@Input()
	public dataInsertion: string;
	@Input()
	public module: number;
	@Input()
	public idChatComponent?: number;
	@Input()
	public idFiche: number;
	@Input()
	public user: EbUser;
	@Input()
	public nbColTexteditor: string = "col-md-4";
	@Input()
	public nbColListing: string = "col-md-8";

	@Input()
	condensedView: boolean = false;

	@Input()
	contributionAccess: boolean;

	ebChat: EbChat = new EbChat();
	listChat = new Array<EbChat>();

	constructor(private chatService: ChatService, private translate: TranslateService) {}

	ngOnInit() {}

	ngOnChanges() {
		this.loadData();
	}

	loadData() {
		let params = "";
		if (this.module) params += "?module=" + this.module;
		if (this.idFiche)
			if (params != "") params += "&idFiche=" + this.idFiche;
			else params = "?idFiche=" + this.idFiche;
		if (this.idChatComponent)
			if (params != "") params += "&idChatComponent=" + this.idChatComponent;
			else params = "?idChatComponent=" + this.idChatComponent;

		this.chatService.getListChat(this.dataSource + params).subscribe((data) => {
			data.map((chat) => {
				chat.userAvatar = ChatPanelFreightAuditComponent.getFakeUserName(chat.userName);

				this.listChat.push(chat);
			});
		});
	}

	sendChat() {
		if (!this.ebChat.text || this.ebChat.text.trim().length <= 0) return;

		this.ebChat.userName = this.user.nom + " " + this.user.prenom;
		this.ebChat.xEbUser = this.user.ebUserNum;
		this.ebChat.module = this.module;
		this.ebChat.idFiche = this.idFiche;
		this.ebChat.idChatComponent = this.idChatComponent;
		this.ebChat.isHistory = false;

		this.chatService.addChat(this.dataInsertion, this.ebChat).subscribe((chat) => {
			chat.userAvatar = ChatPanelFreightAuditComponent.getFakeUserName(chat.userName);
			this.listChat.unshift(chat);
			this.ebChat = new EbChat();
		});
	}

	insertChat(chat: EbChat) {
		chat.userAvatar = ChatPanelFreightAuditComponent.getFakeUserName(chat.userName);
		this.listChat.unshift(chat);
	}

	static getFakeUserName(userName: string) {
		if (!userName.length)
			return Math.random()
				.toString()
				.replace("=", "");
		const p = userName.substr(userName.length - 2, userName.length);
		return "https://robohash.org/" + p + userName.length + ".png";
	}
}
