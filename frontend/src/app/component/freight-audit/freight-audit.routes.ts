import { Route, RouterModule } from "@angular/router";
import { FreightAuditComponent } from "./freight-audit.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";

export const FREIGHT_AUDIT_ROUTES: Route[] = [
	{
		path: "",
		component: FreightAuditComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const FreightAuditRoutes = RouterModule.forChild(FREIGHT_AUDIT_ROUTES);
