import {
	ChangeDetectorRef,
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ViewChild,
} from "@angular/core";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";

import { Modules, SavedFormIdentifier, UserRole } from "@app/utils/enumeration";
import { StatiqueService } from "@app/services/statique.service";
import { SearchCriteriaFreightAudit } from "@app/utils/SearchCriteriaFreightAudit";
import { Statique } from "@app/utils/statique";
import { FreightAuditService } from "@app/services/freight-audit.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbUser } from "@app/classes/user";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { debounceTime, distinctUntilChanged, switchMap } from "rxjs/operators";
import { AdvancedFormSearchComponent } from "@app/shared/advanced-form-search/advanced-form-search.component";
import { SearchField } from "@app/classes/searchField";
import { IField } from "@app/classes/customField";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EcCountry } from "@app/classes/country";
import { EbCategorie } from "@app/classes/categorie";
import { EbLabel } from "@app/classes/label";
import {TranslateService} from "@ngx-translate/core";

@Component({
	selector: "app-freight-audit-search",
	templateUrl: "./freight-audit-search.component.html",
	styleUrls: ["./freight-audit-search.component.css"],
})
export class FreightAuditSearchComponent extends ConnectedUserComponent implements OnInit {
	SavedFormIdentifier = SavedFormIdentifier;
	Statique = Statique;
	moduleName = Modules;
	advancedOption: SearchCriteriaFreightAudit = new SearchCriteriaFreightAudit();
	listContact: any;
	listDestCountry: any;
	listOrigineCountry: any;
	listTypeDemande: any;
	listTypeProcess: any;
	listModeTransport: any;
	listStatut: any;
	listCarrier: any;
	listChargeur: any;
	listRefDemande: any;
	listCarrierTypeahead = new EventEmitter<string>();
	listChargeurTypeahead = new EventEmitter<string>();
	listTypeFlow: any;
	listTypeFlowTypeahead = new EventEmitter<string>();
	connectedUser: EbUser = new EbUser();
	searchCriteria: SearchCriteria = new SearchCriteria();
	public name: number;
	initParamFields: Array<SearchField> = new Array<SearchField>();
	paramFields: Array<SearchField> = new Array<SearchField>();
	listAllCategorie: Array<EbLabel> = new Array<EbLabel>();
	dataInfosItem: Array<any> = [];

	@ViewChild("advancedFormSearchComponentFreight", { static: true })
	advancedFormSearchComponentFreight: AdvancedFormSearchComponent;

	@Input()
	module: number;
	@Output()
	onSearch = new EventEmitter<SearchCriteriaFreightAudit>();
	@Input()
	listFields: Array<IField>;
	@Input()
	listCategorie: Array<EbCategorie>;

	@Input()
	selectedField: any;

	constructor(
		private freightauditservice: FreightAuditService,
		private statiqueService: StatiqueService,
		private cd: ChangeDetectorRef,
		private elRef: ElementRef,
		public generaleMethode: generaleMethodes,
		private translate?: TranslateService
	) {
		super();
		this.searchCriteria.ecRoleNum = this.userConnected.role;
	}

	ngOnInit() {
		this.initParamFields = [];
		this.initParamFields = require("../../../../assets/ressources/jsonfiles/freight-audit-searchfields.json");

		this.addUserFields();

		this.onInputChange(null);

		this.getListStatique();

		this.initTypeaheadEvents();
	}

	onClickInput() {
		this.initParamFields = require("../../../../assets/ressources/jsonfiles/freight-audit-searchfields.json");

		this.addUserFields();
		this.initParamFields.forEach((f) =>
			this.translate.get(f.label).subscribe((res) => (f.label = res))
		);

		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));

		if (this.listFields && this.listFields.length > 0) {
			let arr = [];
			this.listFields.forEach((it) => {
				arr.push({
					label: it.label,
					name: it.name,
					type: "text",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
		}
		if (this.listCategorie && this.listCategorie.length > 0) {
			let arr = [];
			let i = 0;
			this.listAllCategorie = [];
			this.listCategorie.forEach((it, i) => {
				arr.push({
					label: it.libelle,
					type: "autocomplete",
					name: "categoryField" + i,
					listItems: it.labels,
					fetchAttribute: "libelle",
					fetchValue: "ebLabelNum",
					multiple: "true",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
		}

		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});
	}

	onInputChange(selectedFields) {
		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));

		this.setCFInputFields();

		this.setCategoriesInputFields();

		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});

		if (!Statique.isDefined(selectedFields)) {
			this.initParamFields = this.initParamFields.filter(
				(field) =>
					field.name === "listStatut" ||
					field.name === "listOrigineCountry" ||
					field.name === "listDestCountry"
			);
			this.paramFields = Array.from(new Set([].concat(this.initParamFields)));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
		} else if (
			selectedFields !== null &&
			!this.paramFields.find((field) => field.name == selectedFields)
		) {
			let result = JSON.stringify(this.advancedFormSearchComponentFreight.getSearchInput());
			let parsedValues: Array<SearchField> = JSON.parse(result);
			this.paramFields.forEach((field) => {
				if (parsedValues[field.name] !== null && parsedValues[field.name] !== undefined) {
					field.default = parsedValues[field.name];
				}
			});
			this.initParamFields = this.initParamFields.filter(
				(field) => field.name === selectedFields && field.default !== null
			);
			this.paramFields = this.paramFields.concat(this.initParamFields);
			this.paramFields = Array.from(new Set(this.paramFields));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
		}
	}

	setCFInputFields() {
		if (this.listFields && this.listFields.length > 0) {
			let arr = [];
			this.listFields.forEach((it) => {
				arr.push({
					label: it.label,
					name: it.name,
					type: "text",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
			this.initParamFields = this.initParamFields.concat(arr);
		}
	}

	setCategoriesInputFields() {
		if (this.listCategorie && this.listCategorie.length > 0) {
			let arr = [];
			let i = 0;
			this.listAllCategorie = [];
			this.listCategorie.forEach((it, i) => {
				arr.push({
					label: it.libelle,
					type: "autocomplete",
					name: "categoryField" + i,
					listItems: it.labels,
					fetchAttribute: "libelle",
					fetchValue: "ebLabelNum",
					multiple: "true",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
			this.initParamFields = this.initParamFields.concat(arr);
		}
	}

	ngOnChanges() {}

	async getListStatique() {
		this.listDestCountry = await SingletonStatique.getListEcCountry();
		this.listOrigineCountry = ((): Array<EcCountry> =>
			JSON.parse(JSON.stringify(this.listDestCountry)))();
		this.listModeTransport = await SingletonStatique.getListModeTransport();
		this.listTypeDemande = await SingletonStatique.getListTypeDemande();

		this.freightauditservice.ListStatut().subscribe((data) => {
			this.listStatut = data;
		});
	}

	addUserFields() {
		if (this.userConnected.role == UserRole.CHARGEUR || this.userConnected.role == UserRole.CONTROL_TOWER) {
			let arr = [];
			arr.push(
				{
					label: "FREIGHT_AUDIT.CARRIER",
					name: "listCarrier",
					type: "autocomplete",
					srcListItems: "/api/list/listCarrier",
					srcTypeahead: "/api/list/listCarrier",
					fetchAttribute: "nomPrenom",
					fetchValue: "ebUserNum",
					multiple: "true",
				},
				{
					label: "FREIGHT_AUDIT.CARRIER_ESTABLISHMENT",
					name: "listEtablissementNum",
					type: "autocomplete",
					srcListItems: "api/list/get-ebEtablissement",
					fetchAttribute: "nom",
					fetchValue: "nom",
					multiple: "true",
				},
				{
					label: "FREIGHT_AUDIT.CARRIER_COMPANY",
					name: "listCompanyName",
					type: "autocomplete",
					srcListItems: "api/list/get-all-company-name",
					fetchAttribute: "nom",
					fetchValue: "nom",
					multiple: "true",
				},
			);
			this.initParamFields = this.initParamFields.concat(arr);
		}

		if (this.userConnected.role == UserRole.PRESTATAIRE || this.userConnected.role == UserRole.CONTROL_TOWER) {
			let arr = [];
			arr.push({
				label: "FREIGHT_AUDIT.CHARGEUR",
				name: "listChargeur",
				type: "autocomplete",
				srcListItems: "/api/list/listChargeur",
				srcTypeahead: "/api/list/listChargeur",
				fetchAttribute: "compagnieNameNomUserPrenomUser",
				fetchValue: "ebUserNum",
				multiple: "true",
			});
			this.initParamFields = this.initParamFields.concat(arr);
		}
	}

	panelCollapse(panel, event) {
		this.generaleMethode.panelCollapse(panel, event);
	}
	search(advancedOption: SearchCriteriaFreightAudit) {
		this.onSearch.emit(advancedOption);
	}

	cleaSearch() {
		this.onSearch.emit(new SearchCriteriaFreightAudit());
	}

	initTypeaheadEvents() {
		this.listCarrierTypeahead
			.pipe(
				distinctUntilChanged(),
				debounceTime(200),
				switchMap((term) => this.statiqueService.getListCarrier(term))
			)
			.subscribe(
				(x) => {
					this.cd.markForCheck();
					this.listCarrier = x;
				},
				(err) => {
					this.listCarrier = [];
				}
			);

		this.listChargeurTypeahead
			.pipe(
				distinctUntilChanged(),
				debounceTime(200),
				switchMap((term) => this.statiqueService.getListChargeur(term))
			)
			.subscribe(
				(x) => {
					this.cd.markForCheck();
					this.listChargeur = x;
				},
				(err) => {
					this.listChargeur = [];
				}
			);
	}
}
