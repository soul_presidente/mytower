import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { Cost } from '@app/classes/costCategorie';
import { EbUser } from '@app/classes/user';
import { EbDemande } from '@app/classes/demande';
import { EbCompagnieCurrency } from '@app/classes/compagnieCurrency';
import { CotationComponent } from '@app/component/pricing-booking/shared-component/cotation/cotation.component';
import { EtablissementService } from '@app/services/etablissement.service';
import { PricingService } from '@app/services/pricing.service';
import { MessageService } from 'primeng/api';
import { TranslateService } from '@ngx-translate/core';
import { UserRole } from "@app/utils/enumeration";
import { DemandeStatus } from "@app/utils/enumeration";
import { Decimal } from "decimal.js";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from '@app/utils/searchCriteria';
import { EbInvoice } from '@app/classes/invoice';

@Component({
  selector: 'app-additional-cost',
  templateUrl: './additional-cost.component.html',
  styleUrls: ['./additional-cost.component.scss']
})
export class AdditionalCostComponent implements OnInit {

	listAdditionalCost: Array<Cost>;
	@Input()
	isForEdit: boolean;
	@Input()
	isForAdd: boolean = false;
	@Input() userConnected: EbUser;
	@Input() ebDemande: EbDemande;
	Statique = Statique;
	@Input("module")
	ebModuleNum: number;
	isNotTransporter: boolean = false;
	showSaveButton: boolean = false;
	activateSaveButton = true;
	selectedCurrency: EbCompagnieCurrency;
	@Input()
	ebInvoice: EbInvoice = new EbInvoice();

	@Input() listExEtablissementCurrency: Array<EbCompagnieCurrency> = new Array<EbCompagnieCurrency>();
	show : boolean = false
	i: number = 0;
	@ViewChild(CotationComponent, { static: true })
	cotationComponent: CotationComponent;
	constructor(
		protected etablissementService: EtablissementService,
		private pricingService: PricingService,
		private messageService?: MessageService,
		protected translate?: TranslateService
	) {}

	ngOnInit() {
		this.listAdditionalCost = this.ebDemande.listAdditionalCost;

		if (
			this.userConnected.role == UserRole.CHARGEUR ||
			this.userConnected.role == UserRole.CONTROL_TOWER
		) {
			this.isNotTransporter = true;
		}
		if (this.ebInvoice.ebInvoiceNum != null) {
			this.showSaveButton = true;
		}

	}

	addAutre() {
		if (this.listAdditionalCost == null || this.listAdditionalCost.length == 0) {
			this.listAdditionalCost = [];
		} else {
			this.i = this.listAdditionalCost[this.listAdditionalCost.length - 1].ebCostNum;
			this.i++;
		}

		this.listAdditionalCost.push({
			ebCostNum: this.i,
			libelle: "",
			price: null,
			priceEuro: null,
			xEcCurrencyNum: null,
			type: 2,
			euroExchangeRate: 1.0,
		});


		this.checkActivatedButton();
	}

	deleteField(i: number) {
		this.listAdditionalCost.splice(i, 1);
		if (this.listAdditionalCost.length > 0) {
			this.activateSaveButton = false;
			this.checkActivatedButton();
		} else {
			this.activateSaveButton = false;
		}
	}

	handleChangePrice(event: number, item) {
		item.price = event;
	}



	calculCost(item: Cost) {
		if(this.ebInvoice.currencyLabel !=item.currencyInvoice){
			this.show = true;
		}
		if (item.price != null) {
			if (this.listExEtablissementCurrency.length > 0) {
				if (item.euroExchangeRate != null) {
					item.priceEuro = +new Decimal(item.price).times(item.euroExchangeRate).valueOf();
				} else {
					item.priceEuro = +new Decimal(item.price).times(1).valueOf();
				}
			} else {
				item.priceEuro = +new Decimal(item.price).times(1).valueOf();
			}
		} else if (item.price == null) item.priceEuro = null;
	}

	checkActivatedButton() {
		for (let element of this.listAdditionalCost) {
			if (
				element.libelle != null &&
				element.libelle.trim() != "" &&
				element.price != null &&
				element.xEcCurrencyNum != null
			) {
				this.activateSaveButton = false;

			} else {

				this.activateSaveButton = true;
				break;
			}

		}
	}


	updateAdditionalCost() {
		this.pricingService
			.updateAdditionalCost(this.ebDemande.ebDemandeNum, this.listAdditionalCost)
			.subscribe(
				(response) => {
					this.successfulOperation();
					this.activateSaveButton = true;
				},
				(err) => {
					this.echecOperation();
				}
			);
	}

	successfulOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	echecOperation() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}

}
