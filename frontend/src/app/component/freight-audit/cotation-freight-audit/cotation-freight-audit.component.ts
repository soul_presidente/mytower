import { ChangeDetectorRef, Component, Input, OnInit } from "@angular/core";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { EbDemande } from "@app/classes/demande";
import { CotationComponent } from "@app/component/pricing-booking/shared-component/cotation/cotation.component";
import { ChatService } from "@app/services/chat.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { FreightAuditService } from "@app/services/freight-audit.service";
import { GlobalService } from "@app/services/global.service";
import { PricingService } from "@app/services/pricing.service";
import { StatiqueService } from "@app/services/statique.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { UserService } from "@app/services/user.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { Statique } from "@app/utils/statique";
import { TranslateService } from "@ngx-translate/core";
import { CompleterService } from "ng2-completer";
import {Modules} from "@app/utils/enumeration";
import {CostItemService} from "@app/services/cost-item.service";

@Component({
	selector: "app-cotation-freight-audit",
	templateUrl: "./cotation-freight-audit.component.html",
	styleUrls: ["./cotation-freight-audit.component.scss"],
})
export class CotationFreightAuditComponent extends CotationComponent implements OnInit {
	isPreFill: boolean = false;
	@Input() demande: EbDemande;

	@Input() listCurrencyDemande: Array<EbCompagnieCurrency> = new Array<EbCompagnieCurrency>();
	visualisationAccess: boolean;

	constructor(
		protected chatService: ChatService,
		protected pricingService: PricingService,
		protected freightAuditService: FreightAuditService,
		protected modalService: ModalService,
		protected translate: TranslateService,
		protected etablissementService: EtablissementService,
		protected transportationPlanService: TransportationPlanService,
		protected completerService: CompleterService,
		protected userService: UserService,
		protected cd: ChangeDetectorRef,
		protected statiqueService: StatiqueService,
		protected globalService: GlobalService,
		protected costItemService: CostItemService
	) {
		super(
			chatService,
			pricingService,
			freightAuditService,
			modalService,
			translate,
			etablissementService,
			transportationPlanService,
			completerService,
			userService,
			cd,
			statiqueService,
			globalService,
			costItemService
		);
	}

	preFill() {
		this.isPreFill = true;
	}
	ngOnInit() {
		super.ngOnInit();
		this.visualisationAccess = !this.userConnected.hasContribution(Modules.FREIGHT_AUDIT);
		this.listPlCostIemInvoice &&
			this.listPlCostIemInvoice.forEach((costItem) => {
				if(costItem.libelle == null){
					costItem.libelle = Statique.CostItemType[costItem.type - 1].value;
				}
					costItem.libelleCostItem = costItem.libelle;
			});

		this.exEbDemandeTransporteur.listCostCategorie &&
			this.exEbDemandeTransporteur.listCostCategorie.forEach((it) => {
				it.isCollapsed = true;
				let total: number = 0;
				it.listEbCost &&
					it.listEbCost.forEach(
						(el) => {

							if (el.price != null && el.priceEuro != null ) {
								if(el.xEcCurrencyNum == null){
									el.xEcCurrencyNum = this.ebDemande.xecCurrencyInvoice.ecCurrencyNum;
								}
								if(el.xEcCurrencyNum == this.ebDemande.xecCurrencyInvoice.ecCurrencyNum){
									el.euroExchangeRate = 1;
									el.euroExchangeRateReal = el.euroExchangeRate;
								}
								this.listCurrency &&
									this.listCurrency.forEach(
										(code) => {
											if (code.ecCurrencyNum == el.xEcCurrencyNum) {
												el.codeCurrency = code.code;
											}
										}
									);
									let exChangeRateEuro =	el.euroExchangeRate ? el.euroExchangeRate : 1;
									el.priceConverted = el.price * exChangeRateEuro;
									total = total + el.priceConverted;
							}
						}
					);

				it.totalPriceConverted = total;
			});
	}
	
}
