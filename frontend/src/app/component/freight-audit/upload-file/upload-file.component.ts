import { Component, Input, OnChanges, OnInit } from "@angular/core";
import { FreightAuditComponent } from "../freight-audit.component";
import { FreightAuditService } from "@app/services/freight-audit.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { InvoiceFichierJoint } from "@app/classes/InvoiceFichierJoint";
import { Statique } from "@app/utils/statique";
import { EtablissementService } from '@app/services/etablissement.service';

@Component({
	selector: "app-upload-file",
	templateUrl: "./upload-file.component.html",
	styleUrls: ["./upload-file.component.css"],
})
export class UploadFileComponent extends FreightAuditComponent implements OnInit, OnChanges {
	@Input()
	EbInvoiceNum;
	@Input()
	newInvoiceFichierJoint;

	Statique = Statique;

	deleteNbr = 0;

	ebInvoiceFichierJoint = new InvoiceFichierJoint();

	listebInvoiceFichierJoint = Array<InvoiceFichierJoint>();
	constructor(
		protected freightAuditService: FreightAuditService,
		protected etablissementService: EtablissementService,
		protected modalService: ModalService,
		protected translate: TranslateService,
		public activeModal: NgbActiveModal
	) {
		super(freightAuditService, etablissementService,  modalService, translate);
	}

	ngOnChanges() {
		if (this.newInvoiceFichierJoint)
			this.listebInvoiceFichierJoint.push(this.newInvoiceFichierJoint);
	}
	affiche(EbInvoiceNum) {
		this.freightAuditService.ListInvoiceFichierJoint(EbInvoiceNum).subscribe((data) => {
			this.listebInvoiceFichierJoint = data;
		});
	}

	delete(ebInvoiceFichierJointNum, fj, index) {
		this.freightAuditService.deleteFichierJoint(ebInvoiceFichierJointNum).subscribe((res) => {
			this.listebInvoiceFichierJoint.splice(index, 1);
		});
		this.freightAuditService.deleteNbr(this.EbInvoiceNum).subscribe((res) => {});

		let cls = `nbr-upload-${fj.numEntity}`;
		let nbr = +document.getElementsByClassName(cls)[0].textContent - 1;
		document.getElementsByClassName(cls)[0].textContent = "" + (nbr || "");
	}
	ngOnInit() {
		this.affiche(this.EbInvoiceNum);
	}

	setTag(fj) {
		console.log(fj.ebInvoiceFichierJointNum);
		this.freightAuditService.setTag(fj.ebInvoiceFichierJointNum, fj.tag).subscribe((res) => {});
	}

	isSameCompanyCheck(fj) {
		if (this.userConnected.ebCompagnie.ebCompagnieNum == fj.xEbCompagnie) return true;
		else return false;
	}
}
