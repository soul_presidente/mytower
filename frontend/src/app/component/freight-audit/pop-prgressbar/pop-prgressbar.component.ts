import { Component, Input, OnInit } from "@angular/core";
import { FreightAuditComponent } from "../freight-audit.component";
import { FreightAuditService } from "@app/services/freight-audit.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { InvoiceFichierJoint } from "@app/classes/InvoiceFichierJoint";
import { Statique } from "@app/utils/statique";
import { EtablissementService } from '@app/services/etablissement.service';

@Component({
	selector: "app-pop-prgressbar",
	templateUrl: "./pop-prgressbar.component.html",
	styleUrls: ["./pop-prgressbar.component.css"],
})
export class PopPrgressbarComponent extends FreightAuditComponent implements OnInit {
	@Input()
	EbInvoiceNum;
	@Input()
	newInvoiceFichierJoint;

	Statique = Statique;

	deleteNbr = 0;

	ebInvoiceFichierJoint = new InvoiceFichierJoint();

	listebInvoiceFichierJoint = Array<InvoiceFichierJoint>();
	constructor(
		protected freightAuditService: FreightAuditService,
		protected etablissementService: EtablissementService,
		protected modalService: ModalService,
		protected translate: TranslateService,
		public activeModal: NgbActiveModal
	) {
		super(freightAuditService, etablissementService, modalService, translate);
	}

	ngOnInit() {}
}
