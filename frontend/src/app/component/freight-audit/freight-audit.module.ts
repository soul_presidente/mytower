import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FreightAuditRoutes } from "./freight-audit.routes";
import { FreightAuditComponent } from "./freight-audit.component";
import { FreightAuditSearchComponent } from "./freight-audit-search/freight-audit-search.component";
import { FreightAuditListComponent } from "./freight-audit-list/freight-audit-list.component";
import { Ng2CompleterModule } from "ng2-completer";
import { NgSelectModule } from "@ng-select/ng-select";
import { SharedModule } from "@app/shared/module/shared.module";
import { DataTablesModule } from "angular-datatables/src/angular-datatables.module";
import { PopupComponent } from "./popup/popup.component";
import { NgxUploaderModule } from "ngx-uploader";
import { UploadFileComponent } from "./upload-file/upload-file.component";
import { PricingService } from "@app/services/pricing.service";
import { PopPrgressbarComponent } from "./pop-prgressbar/pop-prgressbar.component";
import { ChatPanelFreightAuditComponent } from "./chat-panel-freight-audit/chat-panel-freight-audit.component";
import { PopUpDetailsFreightAuditComponent } from "./pop-up-details-freight-audit/pop-up-details-freight-audit.component";
import { CotationFreightAuditComponent } from "./cotation-freight-audit/cotation-freight-audit.component";
import { AdditionalCostComponent } from "./cotation-freight-audit//additional-cost/additional-cost.component";
import { TooltipModule } from "primeng/tooltip";

@NgModule({
	imports: [
		CommonModule,
		FreightAuditRoutes,
		DataTablesModule,
		NgSelectModule,
		SharedModule,
		Ng2CompleterModule,
		NgxUploaderModule,
		TooltipModule,
	],
	declarations: [
		FreightAuditComponent,
		FreightAuditSearchComponent,
		FreightAuditListComponent,
		PopupComponent,
		UploadFileComponent,
		PopPrgressbarComponent,
		ChatPanelFreightAuditComponent,
		PopUpDetailsFreightAuditComponent,
		CotationFreightAuditComponent,
		AdditionalCostComponent,
	],
	providers: [PricingService],
})
export class FreightAuditModule {}
