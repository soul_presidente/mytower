import { Component, Input, Output, OnInit, ViewChild, EventEmitter } from "@angular/core";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { IDChatComponent, Modules } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { FreightAuditComponent } from "../freight-audit.component";
import { TranslateService } from "@ngx-translate/core";
import { FreightAuditService } from "@app/services/freight-audit.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { ChatPanelComponent } from "@app/shared/chat-panel/chat-panel.component";
import { EbChat } from "@app/classes/chat";
import { ChatPanelFreightAuditComponent } from "../chat-panel-freight-audit/chat-panel-freight-audit.component";
import { EbInvoice } from "@app/classes/invoice";
import { EtablissementService } from "@app/services/etablissement.service";
import { EbDemande } from "@app/classes/demande";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { FichierJoint } from "@app/classes/fichiersJoint";
import { Router } from "@angular/router";

@Component({
	selector: "app-popup",
	templateUrl: "./popup.component.html",
	styleUrls: ["../../../shared/modal/modal.component.scss", "./popup.component.css"],
})
export class PopupComponent extends FreightAuditComponent implements OnInit {
	IDChatComponent = IDChatComponent;
	Modules = Modules;
	statique = Statique;
	@Input()
	ebInvoiceNum: number;

	@Input() ebInvoice: EbInvoice;

	@Input() ebDemande: EbDemande;

	contributionAccess: boolean;

	nbColTexteditor: string;
	nbColListing: string;
	listTypeDocuments = new Array<EbTypeDocuments>();
	listDocuments: Array<FichierJoint>;

	@ViewChild("chatPanelFreightAuditComponent", { static: true })
	chatPanelFreightAuditComponent: ChatPanelFreightAuditComponent;

	@Output()
	refreshDT = new EventEmitter<any>();

	constructor(
		protected freightAuditService: FreightAuditService,
		protected etablissementService: EtablissementService,
		protected router: Router,
		protected modalService: ModalService,
		protected translate: TranslateService,

		public activeModal: NgbActiveModal
	) {
		super(freightAuditService, etablissementService, modalService, translate);
	}

	ngOnInit() {
		this.contributionAccess = this.userConnected.hasContribution(Modules.FREIGHT_AUDIT);
	}

	updateChatEmitter(ebChat: EbChat) {
		this.chatPanelFreightAuditComponent.insertChat(ebChat);
		this.refreshDT.emit(true);
	}
	listTypeDocument(event) {
		this.listTypeDocuments = event;
	}
}
