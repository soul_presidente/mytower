import { Component, OnInit, ViewChild } from "@angular/core";
import { FreightAuditService } from "@app/services/freight-audit.service";
import { EbInvoice } from "@app/classes/invoice";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { Statique } from "@app/utils/statique";
import { InvoiceStatus, Modules } from "@app/utils/enumeration";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { SearchCriteriaFreightAudit } from "@app/utils/SearchCriteriaFreightAudit";
import { UploadComponent } from "@app/utils/uploadComponent";
import { DomSanitizer } from "@angular/platform-browser";
import { FreightAuditSearchComponent } from "./freight-audit-search/freight-audit-search.component";
import { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { SearchCriteria } from '@app/utils/searchCriteria';
import { EtablissementService } from '@app/services/etablissement.service';
import { EbCompagnieCurrency } from '@app/classes/compagnieCurrency';

@Component({
	selector: "app-freight-audit",
	templateUrl: "./freight-audit.component.html",
	styleUrls: ["./freight-audit.component.css"],
})
export class FreightAuditComponent extends UploadComponent implements OnInit {
	Modules = Modules;
	ebInvoice: EbInvoice;
	searchInput: SearchCriteriaFreightAudit;
	listFields: Array<IField>;
	listCategorie: Array<EbCategorie>;
	@ViewChild("freightAuditSearchComponent", { static: false })
	freightAuditSearchComponent: FreightAuditSearchComponent;
	public module: number;

	listExEtablissementCurrency: Array<EbCompagnieCurrency> = new Array<EbCompagnieCurrency>();
	constructor(
		protected freightAuditService: FreightAuditService,
		protected etablissementService: EtablissementService,

		protected modalService: ModalService,
		protected translate: TranslateService,
		protected sanitizer?: DomSanitizer
	) {
		super();
	}

	ngOnInit() {
		this.module = Modules.FREIGHT_AUDIT;
		let criteria = new SearchCriteria();
		criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.etablissementService.getListCurrencyForDemande(criteria).subscribe(
			function(data) {
				if (data) {
					this.listCurrencyDemande = data;
					this.listCurrencyDemande = Statique.getUnique(this.listCurrencyDemande, "ecCurrencyNum");
				}
			}.bind(this)
		);

	}

	changeStatut(ebInvoice: EbInvoice, ok: boolean, event: any) {
		return new Promise((resolve, reject) => {
			let _ebInvoice: EbInvoice = Statique.cloneObject(ebInvoice);
			let requestProcessing = new RequestProcessing();

			requestProcessing.beforeSendRequest(event);

			if (_ebInvoice.statut == InvoiceStatus.WAITING_FOR_CARRIER_INPUT && this.isPrestataire) {
				_ebInvoice.statut = InvoiceStatus.WAITING_FOR_CHARGEUR_CONFIRMATION;
			} else if (
				_ebInvoice.statut == InvoiceStatus.WAITING_FOR_CHARGEUR_CONFIRMATION &&
				this.isChargeur
			) {
				if (ok) {
					_ebInvoice.statut = InvoiceStatus.CONFIRMED_BY_CHARGEUR;
				} else if (!ok) {
					_ebInvoice.statut = InvoiceStatus.WAITING_FOR_CARRIER_CONFIRMATION;
				}
			} else if (
				_ebInvoice.statut == InvoiceStatus.WAITING_FOR_CARRIER_CONFIRMATION &&
				this.isPrestataire
			) {
				_ebInvoice.statut = InvoiceStatus.WAITING_FOR_CHARGEUR_CONFIRMATION;
			}

			this.freightAuditService.changeStatutInvoice(_ebInvoice).subscribe(
				(data: EbInvoice) => {
					requestProcessing.afterGetResponse(event);
					ebInvoice = data;
					this.ebInvoice = data;
					resolve(ebInvoice);
					resolve(this.ebInvoice);
				},
				(error) => {
					requestProcessing.afterGetResponse(event);
					let title = null;
					let body = null;
					this.translate.get("MODAL.ERROR").subscribe((res: string) => {
						title = res;
					});
					// this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
					this.modalService.error(title, null, function() {});
				}
			);
		});
	}

	search(searchInput) {
		this.searchInput = searchInput;
	}

	paramFieldsHandler(pListFields) {
		this.listFields = pListFields;
	}
	CategorieHandler(plistCategorie) {
		this.listCategorie = plistCategorie;
	}
}
