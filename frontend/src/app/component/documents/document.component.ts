import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

import { Statique } from "@app/utils/statique";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbDocument } from "@app/classes/ebDocument";

@Component({
	selector: "app-document",
	templateUrl: "./document.component.html",
	styleUrls: ["./document.component.scss"],
})
export class DocumentComponent implements OnInit {
	Statique = Statique;

	@Input()
	typeUpload: number = 1;

	@Input()
	uploadUrl: string; //= Statique.controller + "/attach-doc";

	@Input()
	ebEntityNum: number;

	@Input()
	typeDocument: number;

	@Input()
	listDoc: Array<EbDocument> = null;
	@Output()
	listDocChange: EventEmitter<Array<EbDocument>> = new EventEmitter<Array<EbDocument>>();

	modalVisible: Boolean = false;

	ebDocument: EbDocument = null;

	loading: boolean = false;

	isPossibleToDelete = true;

	addOrEditInfosFile: Boolean = false;

	constructor() {}

	ngOnInit() {
		this.listDoc = new Array<EbDocument>();
		let doc = new EbDocument();
		doc.ebDocumentNum = 1;
		doc.fileName = "file1";
		doc.dateModif = new Date();
		doc.dateFinValidite = new Date();
		this.listDoc.push(doc);
		let doc2 = new EbDocument();
		doc2.ebDocumentNum = 2;
		doc2.fileName = "file2";
		doc2.dateModif = new Date();
		doc2.dateFinValidite = new Date();
		this.listDoc.push(doc2);
		if (this.listDoc != null && this.listDoc[0] != null && this.typeDocument == null) {
			this.typeDocument = this.listDoc[0].typeDoc;
			this.ebEntityNum = this.listDoc[0].ebEntityNum;
		} else this.refrechListDoc(null, false);
	}

	goToAdd() {
		this.ebDocument = new EbDocument();
		this.addOrEditInfosFile = true;
	}
	onBeforeUpload(event) {
		event.formData.append("ebEntityNum", this.ebEntityNum);
		event.formData.append("typeDoc", this.typeDocument);
		this.loading = true;
		console.log("before upload");
	}
	onSelect() {
		console.log("on select");
		// this.ebDocument=new EbDocument();
		// this.addOrEditInfosFile=true;
		this.updateDoc();
	}
	deleteDoc(event) {
		let list = new Array<EbDocument>();
		this.listDoc.forEach((doc) => {
			if (doc.ebDocumentNum != this.ebDocument.ebDocumentNum) list.push(doc);
		});
		this.listDoc = list;
		this.modalClose();
		// let requestProcessing = new RequestProcessing();
		// requestProcessing.beforeSendRequest(event);
		// this.docrClassifyService.deleteDocumentByEbDocument(this.doc).subscribe((data:Boolean)=>{
		//   if(data)
		//     this.refrechListDoc(event,true);
		// })
	}
	goToModalConfirm(doc: EbDocument) {
		console.log(doc);
		this.modalVisible = true;
		this.ebDocument = doc;
	}
	modalClose() {
		this.modalVisible = false;
		this.isPossibleToDelete = true;
		this.addOrEditInfosFile = false;
	}
	onClear() {
		this.modalClose();
	}
	updateDoc() {
		if (this.ebDocument != null && this.ebDocument.ebDocumentNum == null) {
			let num = this.listDoc[0].ebDocumentNum;
			this.listDoc.forEach((doc) => {
				if (doc.ebDocumentNum > num) num = doc.ebDocumentNum;
			});
			this.ebDocument.ebDocumentNum = num + 1;
			this.listDoc.push(this.ebDocument);
		} else {
			this.listDoc.forEach((doc) => {
				if (doc.ebDocumentNum == this.ebDocument.ebDocumentNum) {
					doc = this.ebDocument;
				}
			});
		}
	}
	goToEdit(doc) {
		this.ebDocument = doc;
		this.addOrEditInfosFile = true;
	}
	onUpload(event) {
		this.loading = false;
		this.addOrEditInfosFile = false;
		this.refrechListDoc(null, false);
	}
	refrechListDoc(event, isDelete: Boolean) {
		let requestProcessing = null;
		if (isDelete) requestProcessing = new RequestProcessing();
		// this.docrClassifyService.getListDocument( this.typeDocument+'', this.ebEntityNum+'').subscribe((data:Array<EbDocument>)=>{
		//   this.listDoc=data;
		//   this.listDocChange.emit(data);
		//   if(isDelete){
		//     requestProcessing.afterGetResponse(event);
		//     this.modalClose();
		//   }
		// })
	}
}
