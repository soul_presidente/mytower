import { Component, Input, OnInit } from "@angular/core";
import { EbDocument } from "@app/classes/ebDocument";

@Component({
	selector: "app-create-document",
	templateUrl: "./create-document.component.html",
	styleUrls: ["./create-document.component.scss"],
})
export class CreateDocumentComponent implements OnInit {
	@Input()
	ebDocument: EbDocument = new EbDocument();

	ngOnInit() {}
}
