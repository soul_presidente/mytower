import { Component, OnInit } from "@angular/core";
import { DocumentTemplatesService } from "@app/services/document-templates.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbTemplateDocumentsDTO } from "@app/classes/dto/ebTemplateDocumentsDTO";
import { TemplateGenParamsDTO } from "@app/classes/TemplateGenParamsDTO";

@Component({
	selector: "app-doc-template-gen-details",
	templateUrl: "./template-gen-params.html",
	styleUrls: ["./template-gen-params.scss"],
})
export class DocTemplateGenDetailsComponent implements OnInit {
	loading = false;

	templates: Array<EbTemplateDocumentsDTO> = [];

	params: TemplateGenParamsDTO = new TemplateGenParamsDTO();

	constructor(protected documentTemplateService: DocumentTemplatesService) {}

	ngOnInit() {
		this.documentTemplateService.getListDocumentTemplates(new SearchCriteria()).subscribe((res) => {
			this.templates = res;
		});
	}

	get selectedTemplateObject(): EbTemplateDocumentsDTO {
		if (this.params.selectedTemplate == null || this.templates == null) return null;

		let rValue: EbTemplateDocumentsDTO;
		this.templates.forEach((c) => {
			if (this.params.selectedTemplate === c.ebTemplateDocumentsNum) rValue = c;
		});

		return rValue;
	}

	onSelectTemplate(index?: number) {}
}
