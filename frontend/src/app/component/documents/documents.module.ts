import { CommonModule } from "@angular/common";
import { SharedModule } from "@app/shared/module/shared.module";
import { DataTablesModule } from "angular-datatables";
import { FileUploadModule } from "primeng/primeng";
import { NgModule } from "@angular/core";
import { DocumentComponent } from "./document.component";
import { ListDocumentsComponent } from "./list-documents/list-documents.component";
import { CreateDocumentComponent } from "./create-document/create-document.component";

@NgModule({
	imports: [CommonModule, SharedModule, DataTablesModule, FileUploadModule],
	declarations: [DocumentComponent, ListDocumentsComponent, CreateDocumentComponent],
	exports: [DocumentComponent],
	providers: [],
})
export class DocumentsModule {}
