import { Component, OnInit } from "@angular/core";
import { Input } from "@app/utils/Input";
import { EbUser } from "@app/classes/user";
import { Delegation } from "@app/classes/delegation";
import { UserService } from "@app/services/user.service";
import { Inscription } from "@app/classes/inscription";
import { ModalService } from "@app/shared/modal/modal.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
	selector: "app-account-settings",
	templateUrl: "./account-settings.component.html",
	styleUrls: ["./account-settings.component.scss"],
})
export class AccountSettingsComponent implements OnInit {
	newUser: EbUser = new EbUser();
	delegation: Delegation = new Delegation();

	inputsFriendsEmail: Array<Input>;
	inputsColleguesEmail: Array<Input>;

	listUser: Array<EbUser> = new Array<EbUser>();

	_password: string;
	nvPassword: string;
	confirmationPassword: string;
	closeResult: string;

	constructor(
		private userService: UserService,
		private modalService: ModalService,
		private ngbService: NgbModal
	) {
		this.inputsFriendsEmail = new Array<Input>();
		this.inputsColleguesEmail = new Array<Input>();
	}

	getUser() {
		this.userService.userInfo().subscribe((rep) => {
			this.newUser = rep;
		});
	}

	getConnectedUserInfo() {
		this.userService.getUserInfo().subscribe((rep) => {
			this.newUser = rep;
		});
	}

	updateUser() {
		this.userService.updateUser(this.newUser).subscribe((rep) => {
			this.newUser = rep;
		});
	}

	changePassword() {
		//this.newUser.ebUserNum = 1;
		this.userService
			.matchUserPassword(this.newUser.ebUserNum, this._password)
			.subscribe((result: boolean) => {
				if (result) {
					this.newUser.password = this.nvPassword;
					this.userService.updateUser(this.newUser).subscribe((rep) => {
						this.newUser = rep;
						//console.log(this.newUser);
					});
				}
			});
	}

	createBackUp(event) {
		if (event.srcElement.innerHTML === "BackUp") {
			this.delegation.userConnect = this.newUser;
			this.userService.createDelegation(this.delegation).subscribe((rep) => {
				// this.delegation = rep;
				console.log(this.delegation);
			});
			event.srcElement.innerHTML = "End BackUp";
		} else if (event.srcElement.innerHTML === "End BackUp") {
			event.srcElement.innerHTML = "BackUp";
		}
	}

	addInput(inputs: any) {
		let input: Input = new Input();
		input.id = inputs.length + 1;
		inputs.push(input);
	}

	removeInput(inputs: Array<Input>, index: number) {
		inputs.splice(-index, 1);
		//console.log(index);
	}

	inviteFriend(event: any) {
		//let requestProcessing = new RequestProcessing();
		//requestProcessing.beforeSendRequest(event);
		let inscription = new Inscription();
		inscription.ebUser = this.newUser;
		inscription.friendInvitationEmails = this.inputsFriendsEmail.map((input) => input.email);
		console.log(inscription.friendInvitationEmails);
		inscription.collegueInvitationEmails = this.inputsColleguesEmail.map((input) => input.email);
		this.userService.sendInvitationMail(inscription).subscribe((user: EbUser) => {
			//requestProcessing.afterGetResponse(event);
		});
	}

	ngOnInit() {
		this.newUser = new EbUser();
		//let searchCriteria: SearchCriteria = new SearchCriteria();
		this.getUser();
		//this.getConnectedUserInfo();
	}
}
