import {
	AfterViewInit,
	Component,
	ElementRef,
	Input,
	OnInit,
	ViewChild,
	ViewContainerRef,
} from "@angular/core";
import { EbEtablissement } from "@app/classes/etablissement";
import { Horaire } from "@app/classes/horaire";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { EbAdresse } from "@app/classes/adresse";
import { EtatHoraire, GenericTableScreen } from "@app/utils/enumeration";
import { DataTableDirective } from "angular-datatables";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { AdministrateurCompanyComponent } from "../../../../administrateur-company/administrateur-company.component";
import { StatiqueService } from "@app/services/statique.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { ModalComponent } from "@app/shared/modal/modal.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { Statique } from "@app/utils/statique";
import { EbUser } from "@app/classes/user";
import { EbCompagnie } from "@app/classes/compagnie";
import { CompagnieService } from "@app/services/compagnie.service";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { SearchCriteria } from "@app/utils/searchCriteria";
import SingletonStatique from "@app/utils/SingletonStatique";
import { Eligibility } from "@app/classes/eligibility";
import { Message, MessageService } from "primeng/api";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { EcCountry } from "@app/classes/country";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { ConnectedUserComponent } from '@app/shared/connectedUserComponent';
import { EbEntrepotDTO } from '@app/classes/dock/EbEntrepotDTO';

@Component({
	selector: "app-adresses",
	templateUrl: "./adresses.component.html",
	styleUrls: ["./adresses.component.css"],
})
export class AdressesComponent extends ConnectedUserComponent
	implements OnInit, AfterViewInit {
	GenericTableScreen = GenericTableScreen;
	horaireOuverture: Horaire = new Horaire();
	dataTableConfig: DataTableConfig = new DataTableConfig();
	adresse: EbAdresse = new EbAdresse();
	EtatHoraire = EtatHoraire;
	isAddAddress = false;
	listAddress: Array<EbAdresse> = new Array<EbAdresse>();
	listPays: any;
	@Input()
	panelClass?: string;
	@Input()
	headingClass?: string;
	@Input()
	bodyClass?: string;
	@ViewChild(DataTableDirective, { static: true })
	dtElement: DataTableDirective;
	@Input()
	ebEtablissement: EbEtablissement = new EbEtablissement();
	@Input("enableAdvancedOpeningHours")
	enableAdvancedOpeningHours: boolean = false;
	@ViewChild("openingHoursTemplate", { static: false })
	openingHoursTemplate: ElementRef;
	@ViewChild("panel", { static: true })
	panelEl: ElementRef;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	isStatiquesLoaded = false;
	criteria: SearchCriteria = new SearchCriteria();
	dataObject: EbAdresse = new EbAdresse();
	isAddObject = false;
	backupEditObject: EbAdresse;
	deleteMessage = "";
	deleteHeader = "";
	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	openingHours = null;
	listEligibility: Array<any>;
	msgs: Message[] = [];
	constructor(
		protected vcr?: ViewContainerRef,
		protected statiqueService?: StatiqueService,
		protected etablissementService?: EtablissementService,
		protected ngbService?: NgbModal,
		protected modalService?: ModalService,
		private translate?: TranslateService,
		private compagnieService?: CompagnieService,
		private messageService?: MessageService,
		protected headerService?: HeaderService
	) {
		super();
	}

	ngAfterViewInit(): void {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		if (this.userConnected.superAdmin) {
			this.compagnieService
				.getListAddressCompagnie(this.userConnected.ebCompagnie.ebCompagnieNum)
				.subscribe((res) => {
					this.listAddress = res;
					this.eligibilityToArray(this.listAddress);
					this.dataTableConfig.dtTrigger.next();
				});
		} else {
			let criteria: SearchCriteria = new SearchCriteria();
			criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			this.etablissementService.getListAdresseEtablisement(criteria).subscribe((res) => {
				this.listAddress = res;
				this.eligibilityToArray(this.listAddress);
				this.dataTableConfig.dtTrigger.next();
			});
		}

		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.etablissementService.getEtablisement(searchCriteria).subscribe((data: EbEtablissement) => {
			if (data.categories) {
				data.categories = data.categories.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}

			this.ebEtablissement.constructorCopy(data);
		});
	}

	ngOnInit(): void {
		this.statiqueService.getListEligibility().subscribe((data) => {
			this.listEligibility = data;
		});

		this.dataInfos.cols = [
			{
				field: "reference",
				header: "Reference",
				translateCode: "REPOSITORY_MANAGEMENT.ADRESSE_REFERENCE",
			},
			{
				field: "company",
				header: "Company",
				translateCode: "REPOSITORY_MANAGEMENT.ADRESSE_COMPANY",
			},
			{
				field: "street",
				header: "Address",
				translateCode: "REPOSITORY_MANAGEMENT.ADRESSE",
			},
			{
				field: "city",
				header: "city",
				translateCode: "REPOSITORY_MANAGEMENT.ADRESSE_CITY",
			},
			{
				field: "zipCode",
				header: "ZIP code",
				translateCode: "REPOSITORY_MANAGEMENT.ADRESSE_ZIP_CODE",
			},
			{
				field: "state",
				header: "Status",
				translateCode: "REPOSITORY_MANAGEMENT.STATE",
			},
			{
				field: "country",
				header: "Country",
				translateCode: "REPOSITORY_MANAGEMENT.ADRESSE_COUNTRY",
				render: function(country: EcCountry, d, row) {
					return country &&
					country.ecCountryNum &&
					country.libelle
						? country.libelle: "";
				},
			},
			{
				field: "zones",
				header: "Zone",
				sortable: false,
				translateCode: "REPOSITORY_MANAGEMENT.ADRESSE_ZONE",
				render: function(zones: EbPlZoneDTO[], d, row)
				{
					let str = "";
					zones && zones.forEach(it => {
						if(str.length > 0) str += ", ";
						str += this.getZoneDisplayString(it);
					});
					return str;
				}.bind(this)
			},
			{
				field: "airport",
				header: "Airport/Port",
				translateCode: "REPOSITORY_MANAGEMENT.ADRESSE_AIRPORT",
			},
			{
				field: "phone",
				header: "Phone",
				translateCode: "REPOSITORY_MANAGEMENT.ADRESSE_PHONE",
			},
			{
				field: "openingHoursFreeText",
				header: "Opening hours",
				translateCode: "REPOSITORY_MANAGEMENT.ADRESSE_OPENING_HOURS",
			},
			{
				field: "email",
				header: "E-mail",
				translateCode: "REPOSITORY_MANAGEMENT.ADRESSE_MAIL",
			},
			{
				field: "xEbUserVisibility",
				header: "Origin/Destination user",
				translateCode: "REPOSITORY_MANAGEMENT.VISIBILITY_RIGHTS",
				render: function(xEbUserVisibility: EbUser, d, row) {
					return xEbUserVisibility &&
						xEbUserVisibility.ebUserNum &&
						xEbUserVisibility.ebEtablissement.ebEtablissementNum
						? xEbUserVisibility.nom + " - " + xEbUserVisibility.ebEtablissement.nom
						: "";
				},
			},
			{
				field: "etablissementAdresse",
				header: "Establishment",
				sortable: false,
				translateCode: "REPOSITORY_MANAGEMENT.ETABLISSEMENT",
				render: function(ebEtablissements: EbEtablissement[], d, row)
				{
					let str = "";
					ebEtablissements && ebEtablissements.forEach(it => {
						if(str.length > 0) str += ", ";
						str += it && it.ebEtablissementNum ? it.nom : "";
					});
					return str;
				}.bind(this)
			},
			{
				field: "entrepot",
				header: "entrepot",
				translateCode: "PRICING_BOOKING.ENTREPOT",
				render: function(entrepot: EbEntrepotDTO, d, row) {
					return entrepot &&
					entrepot.ebEntrepotNum &&
					entrepot.libelle
						? entrepot.libelle: "";
				},
			},
		];

		this.criteria.ebUserNum = this.userConnected.ebUserNum;
		this.criteria.pageNumber = 1;
		this.criteria.size = 10;

		if (this.userConnected.superAdmin) {
			this.criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		} else {
			this.criteria.ebUserNum = this.userConnected.ebUserNum;
			this.criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		}

		this.dataInfos.dataKey = "ebAdresseNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerAdresse + "/list-addresses-table";
		this.dataInfos.dataType = EbAdresse;
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.actionColumFixed = false;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;

		this.loadData();
	}
	async loadData() {
		this.isStatiquesLoaded = true;
	}
	displayAdd(isUpdate?: boolean) {
		if (isUpdate) {
		} else this.dataObject = new EbAdresse();

		this.onShowAddEditDisplay(this.dataObject);

		this.isAddObject = true;
	}
	onShowAddEditDisplay(dataObject: EbAdresse) {}

	diplayEdit(dataObject: EbAdresse) {
		this.dataObject = Statique.cloneObject(dataObject, new EbAdresse());

		this.onShowAddEditDisplay(dataObject);

		this.backupEditObject = Statique.cloneObject<EbAdresse>(this.dataObject, new EbAdresse());
		this.displayAdd(true);
	}
	deleteData(adresse: EbAdresse) {
		let $this = this;
		this.deleteMessage = null;
		this.deleteHeader = null;

		this.modalService.confirm(
			$this.translate.instant("GENERAL.CONFIRMATION"),
			$this.translate.instant("ADDRESS.CONFIRM_DELETE"),
			function() {
				let dataObject = adresse;
				$this.etablissementService.deleteAdresseEtablisement(adresse).subscribe((data) => {
					$this.genericTable.refreshData();
				});
			},
			function() {},
			true
		);
	}

	editAddress(event: any) {
		this.adresse = Statique.cloneObject(event.data, new EbAdresse());
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event.event);
		let adr = new EbAdresse();
		this.adresse.ebCompagnie = new EbCompagnie();
		this.adresse.ebCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		adr.copy(this.adresse);
		adr.listEgibility = this.getEligibilityString(this.adresse.listEligibilitys);

		let etablissementsAdresse: Array<EbEtablissement> = this.adresse.etablissementAdresse;
		let etablissement: EbEtablissement = null;
		adr.etablissementAdresse = new Array<EbEtablissement>();

		let listEtabsNums: Array<Number> = new Array();
		let listZonesNums: Array<Number> = new Array();

		if (etablissementsAdresse && etablissementsAdresse.length > 0) {
			etablissementsAdresse.forEach((etab) => {
				etablissement = new EbEtablissement();
				etablissement.ebEtablissementNum = etab.ebEtablissementNum;
				etablissement.nom = etab.nom;
				etablissement.ebCompagnie = null;
				adr.etablissementAdresse.push(etablissement);
				listEtabsNums.push(etab.ebEtablissementNum);
			});

			adr.listEtablissementsNum = ":" + listEtabsNums.join(":") + ":";
		}

		if (this.adresse.zones && this.adresse.zones.length > 0) {
			adr.zones = new Array<EbPlZoneDTO>();

			this.adresse.zones.forEach((zone) => {
				adr.zones.push(zone);
				listZonesNums.push(zone.ebZoneNum);
			});

			adr.listZonesNum = ":" + listZonesNums.join(":") + ":";
		} else adr.defaultZone = null;

		if (adr.country && adr.country.ecCountryNum) {
			let country: EcCountry = adr.country;
			adr.country = new EcCountry();
			adr.country.ecCountryNum = country.ecCountryNum;
			adr.country.code = country.code;
		}
		if(adr.entrepot && adr.entrepot.ebEntrepotNum){
			let entrepot: EbEntrepotDTO = adr.entrepot;
			adr.entrepot = new EbEntrepotDTO();
			adr.entrepot.ebEntrepotNum = entrepot.ebEntrepotNum;
			adr.entrepot.libelle = entrepot.libelle;
			adr.entrepot.reference = entrepot.reference;


		}

		this.etablissementService.editAdresseEtablisement(adr).subscribe((data) => {
			requestProcessing.afterGetResponse(event.event);
			this.isAddObject = false;
			this.successfulOperation();
		});
	}

	successfulOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	async displayAddAddress(isUpdate?: boolean) {
		if (!isUpdate) {
			this.adresse = new EbAdresse();
		}
		if (
			this.adresse &&
			(!this.adresse.etablissementAdresse || this.adresse.etablissementAdresse.length == 0)
		) {
			this.adresse.etablissementAdresse = new Array<EbEtablissement>();
			let etablissement: EbEtablissement = new EbEtablissement();
			etablissement.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			this.adresse.etablissementAdresse.push(etablissement);
		}

		this.isAddAddress = true;

		if (this.listPays == null) {
			this.listPays = await SingletonStatique.getListEcCountry();
		}
	}

	backToListAddress() {
		this.isAddAddress = false;
	}

	cancel(event: any) {
		this.isAddObject = false;
	}
	addAddress(event: any) {
		this.adresse = Statique.cloneObject(event.data, new EbAdresse());
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event.event);
		this.adresse.listEgibility = this.getEligibilityString(this.adresse.listEligibilitys);
		let userId = this.adresse.xEbUserVisibility
			? this.adresse.xEbUserVisibility.ebUserNum
			: this.userConnected.ebUserNum;
		this.adresse.xEbUserVisibility = EbUser.createUser(null, userId);

		this.adresse.ebCompagnie = new EbCompagnie();
		this.adresse.ebCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.adresse.createdBy = new EbUser();
		this.adresse.createdBy.ebUserNum = this.userConnected.ebUserNum;

		let etablissementsAdresse: Array<EbEtablissement> = this.adresse.etablissementAdresse;
		let etablissement: EbEtablissement = null;
		this.adresse.etablissementAdresse = new Array<EbEtablissement>();

		let listEtabsNums: Array<Number> = new Array();
		let listZonesNums: Array<Number> = new Array();

		if (etablissementsAdresse && etablissementsAdresse.length > 0) {
			etablissementsAdresse.forEach((etab) => {
				etablissement = new EbEtablissement();
				etablissement.ebEtablissementNum = etab.ebEtablissementNum;
				etablissement.nom = etab.nom;
				etablissement.ebCompagnie = null;
				this.adresse.etablissementAdresse.push(etablissement);
				listEtabsNums.push(etab.ebEtablissementNum);
			});

			this.adresse.listEtablissementsNum = ":" + listEtabsNums.join(":") + ":";
		}

		if (this.adresse.zones && this.adresse.zones.length > 0) {
			this.adresse.zones.forEach((zone) => {
				listZonesNums.push(zone.ebZoneNum);
			});
			this.adresse.listZonesNum = ":" + listZonesNums.join(":") + ":";
		} else this.adresse.defaultZone = null;

		this.etablissementService.addAdresseEtablisement(this.adresse).subscribe(
			function(data) {
				requestProcessing.afterGetResponse(event.event);
				this.adresse.ebAdresseNum = data.ebAdresseNum;
				this.listAddress.push(this.adresse);
				this.adresse = new EbAdresse();
				this.isAddObject = false;
				this.dataTableConfig.rerender(this.dtElement);
				this.successfulOperation();
			}.bind(this)
		);
	}

	getEligibilityString(item) {
		let eligibilityStr: String = null;
		let codeEligibility: Array<any>;
		if (item && item.length > 0) {
			codeEligibility = item.map((item) => item.reference);
			eligibilityStr = codeEligibility.toString();
		}
		return eligibilityStr;
	}

	eligibilityToArray(listAddress: Array<EbAdresse>) {
		listAddress.forEach((item) => {
			let LibelleStr: String;
			let LibelleArr: Array<any> = new Array<any>();
			if (item.listEgibility != null) {
				let array = item.listEgibility.split(",");
				item.listEligibilitys = new Array<Eligibility>();
				array.forEach((x) => {
					let elig: Eligibility = this.getEligibility(x);
					this.translate
						.get("ADRESSE_ELIGIBILITY." + elig.libelle)
						.subscribe((res) => LibelleArr.push(res));
					item.listEligibilitys.push(elig);
				});
				LibelleStr = LibelleArr.toString();
				item.listEgibilityLibelle = LibelleStr;
			}
		});
	}
	getEligibility(reference) {
		let elig: Eligibility = new Eligibility();
		this.listEligibility.forEach((item) => {
			if (item.reference == reference) {
				elig.code = item.code;
				elig.reference = item.reference;
				elig.libelle = item.libelle;
			}
		});
		return elig;
	}
	diplayEditAddress(index: number) {
		this.adresse = this.listAddress[index];
		this.displayAddAddress(true);
	}

	open() {
		//const modalRef =
		return this.ngbService.open(ModalComponent);
		//modalRef.componentInstance.name = 'Test Modal';
	}

	private getZoneDisplayString(zone?: EbPlZoneDTO): string {
		if (zone) {
			if (zone.designation) {
				return zone.ref + " (" + zone.designation + ")";
			} else {
				return zone.ref;
			}
		} else {
			return "";
		}
	}

	showOpeningHours(index: number) {
		this.openingHours = this.listAddress[index].openingHours;
		setTimeout(
			function() {
				this.openDialogOpeningHours(this.openingHoursTemplate.nativeElement.innerHTML);
			}.bind(this),
			500
		);
	}

	openDialogOpeningHours(tpl) {
		this.modalService.template("Opening hours", tpl);
	}
	async moveDatatableFilter() {
		let filterEl;
		let tmpEl;
		await Statique.waitForTimemout(100);

		let collapsContainer = this.panelEl.nativeElement;

		let timer = 0;
		while (
			!(filterEl = collapsContainer.getElementsByClassName("dataTables_filter")[0]) &&
			timer < 10000
		) {
			await Statique.waitForTimemout(200);
			timer += 200;
		}

		tmpEl = collapsContainer
			? (tmpEl = collapsContainer.getElementsByClassName("search-datatable__wrapper"))
				? tmpEl[0]
				: null
			: null;
		if (tmpEl && filterEl) {
			Statique.moveDomElement(filterEl, tmpEl);
			filterEl.className += " d-inline-block";
			filterEl = filterEl.getElementsByTagName("label");
			if (filterEl && (filterEl = filterEl[0])) {
				filterEl.childNodes.forEach((it) => {
					if (it.nodeType == 3) it.data = "";
				});
				filterEl = (filterEl = filterEl.getElementsByTagName("input")) ? filterEl[0] : null;
				if (filterEl) {
					filterEl.placeholder = "Search";
				}
			}

			filterEl = (filterEl = collapsContainer.getElementsByClassName("search-datatable__wrapper"))
				? filterEl[0]
				: null;
			tmpEl = collapsContainer
				? (tmpEl = collapsContainer.getElementsByClassName("datatable-filter"))
					? tmpEl[0]
					: null
				: null;
			if (filterEl && tmpEl) {
				Statique.moveDomElement(filterEl, tmpEl);
			}
		}
	}

	ngOnDestroy() {
		let filterEl;
		if ((filterEl = this.panelEl.nativeElement.getElementsByClassName("dataTables_filter")[0]))
			filterEl.parentNode.removeChild(filterEl);
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.ADRESSES",
			},
		];
	}
}
