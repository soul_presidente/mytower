import {
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	ViewContainerRef,
	ViewChild,
} from "@angular/core";
import { StatiqueService } from "@app/services/statique.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { EbEtablissement } from "@app/classes/etablissement";
import { EtatUser, GenericTableScreen, Modules } from "@app/utils/enumeration";
import { Router } from "@angular/router";
import { EbUser } from "@app/classes/user";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { UserService } from "@app/services/user.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { AdministrateurCompanyComponent } from "./../../../../administrateur-company/administrateur-company.component";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { MessageService, SelectItem } from "primeng/api";
import { ExportService } from "@app/services/export.service";
import { SearchField } from "@app/classes/searchField";
import { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { Statique } from "@app/utils/statique";
import { EbUserProfile } from "@app/classes/userProfile";
import { UserProfileService } from "@app/services/user-profile.service";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { EbDemande } from "@app/classes/demande";

@Component({
	selector: "app-users-management",
	templateUrl: "./users-management.component.html",
	styleUrls: [
		"./users-management.component.css",
		"./../../../../administrateur-company/administrateur-company.component.css",
		"../../settings.component.scss",
	],
})
export class UsersManagementComponent extends AdministrateurCompanyComponent implements OnInit {
	@Input()
	ebEtablissement: EbEtablissement;
	@Input()
	isDetails: Boolean;
	@Output()
	changeToDetails = new EventEmitter<Boolean>();
	@Output()
	setUserToBeUpdated = new EventEmitter<EbUser>();
	@Input()
	ebUser: EbUser;
	// @ViewChild("userDetailCmp")
	// userDetail: UserDetailsComponent;
	typeParamSelected: number;
	profilSelected: EbUserProfile;

	//emun status du compte
	EtatUser = EtatUser;
	listUsers: Array<EbUser>;
	searchCriteria: SearchCriteria = new SearchCriteria();
	status: boolean;
	ConstantsTranslate: Object;

	types: SelectItem[];
	selectedType: string = "table";
	cars: any[] = [];
	Modules = Modules;
	totalRecords: number = 0;
	listPages: Array<number> = new Array<number>();

	groupes: any[];
	filteredGroupes: any[];
	seletedGroupes: any[] = [];
	filteredGroupesMultiple: any[];
	toPage: number;
	rowsPerPageOptions: number[] = [5, 10, 25, 50, 100];

	// Generique Table
	datatableComponent = GenericTableScreen;
	initParamFields: Array<SearchField> = new Array<SearchField>();
	listFields: Array<IField>;
	listCategorie: Array<EbCategorie>;
	searchInput: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	detailsInfos: GenericTableInfos = new GenericTableInfos(this);

	module: number;
	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;
	constructor(
		protected userService: UserService,
		private router: Router,
		protected modalService: ModalService,
		protected vcr?: ViewContainerRef,
		protected statiqueService?: StatiqueService,
		protected etablissementService?: EtablissementService,
		protected translate?: TranslateService,
		private exportService?: ExportService,
		private messageService?: MessageService,
		private userProfileService?: UserProfileService,
		protected headerService?: HeaderService
	) {
		super(vcr, etablissementService);
	}

	ngOnInit(): void {
		this.module = Modules.USER_MANAGEMENT;
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.headerService.registerActionButtons(this.getActionButtons());
		this.initParamFields = require("../../../../../../assets/ressources/jsonfiles/user-search.json");
		this.transaleteTexte();
		this.initializeData();

		document.addEventListener(
			"user-action-event",
			function(event) {
				event.preventDefault();
				event.stopPropagation();
				this.addNewUser();
			}.bind(this),
			false
		);

		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.etablissementService.getEtablisement(searchCriteria).subscribe((data: EbEtablissement) => {
			if (data.categories) {
				data.categories = data.categories.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}

			this.ebEtablissement.constructorCopy(data);
		});
	}

	transaleteTexte() {
		this.ConstantsTranslate = {
			CONFIRM_REMOVE_ADMIN_RIGHTS: "",
			CONFIRM_SET_AS_ADMIN: "",
			CONFIRM_DEACTIVATE_USER: "",
			CONFIRM_ACTIVATE_USER: "",
			CONFIRM_RESENT_ACTIVATION_MAIL: "",
			CONFIRM_DELETE_SELECTED_USERS: "",
			USERS_CAN_NOT_DELETED: "",
			USER_DELTED: "",
			HAS_RELATION: "",
		};
		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate
				.get("REPOSITORY_MANAGEMENT." + key)
				.subscribe((res) => (this.ConstantsTranslate[key] = res));
		});
	}

	initializeData() {
		let customizProfile: string = "Personnalisé";
		this.translate.get("REPOSITORY_MANAGEMENT.PERSONNALISE").subscribe((res: string) => {
			customizProfile = res;
		}),
			(this.dataInfos.cols = [
				{
					field: "nomPrenom",
					isCardCol: true,
					cardOrder: 1,
					translateCode: "INSCRIPTION.NOM_PRENOM",
				},
				{
					field: "ebEtablissement",
					translateCode: "GENERAL.ESTABLISHMENT",
					isCardCol: true,
					cardOrder: 2,
					render: function(data: EbEtablissement) {
						return data.nom;
					}.bind(this),
				},
				{ field: "email", isCardCol: true, cardOrder: 3, translateCode: "INSCRIPTION.EMAIL" },
				{ field: "username", translateCode: "INSCRIPTION.USERNAME" },
				{
					field: "telephone",
					isCardCol: true,
					cardOrder: 4,
					translateCode: "INSCRIPTION.TELEPHONE",
				},
				{
					field: "groupes",
					isCardCol: true,
					cardOrder: 5,
					translateCode: "INSCRIPTION.NOM_GROUPE",
				},
				{
					field: "superAdmin",
					isCardCol: true,
					cardOrder: 6,
					translateCode: "INSCRIPTION.ROLE",
					render: function(data, type, row) {
						let status: string = "";
						if (row.superAdmin) status = "Super Admin";
						else if (row.admin) status = "Administrator";
						return status;
					}.bind(this),
				},
				{
					field: "status",
					isCardCol: true,
					cardOrder: 6,
					translateCode: "INSCRIPTION.STATUS",
					render: function(data: any) {
						let status: string = "";
						if (data == 1) status = "Actif";
						else if (data == 2) status = "Desactivé";
						else status = "Non actif";
						return status;
					}.bind(this),
				},
				// { field: "telephone", isCardCol: true, cardOrder: 4, translateCode: "INSCRIPTION.TELEPHONE" },

				{
					field: "ebUserProfile",
					translateCode: "SETTINGS.PROFILE",
					isCardCol: true,
					cardOrder: 4,
					renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
					render: function(data: EbUserProfile) {
						let profileElt = "";
						let title = data.descriptif ? data.descriptif : customizProfile;
						let libelle = data.nom ? data.nom : customizProfile;
						profileElt = `<div title='` + title + `'>` + libelle + `</div>`;
						return profileElt;
					}.bind(this),
				},
			]);
		this.dataInfos.lineActions = [];

		if(this.isSuperAdmin) {
			this.dataInfos.showCheckbox = true;
			this.dataInfos.showCheckboxWidth = "150px";
		}

		this.translator(this.dataInfos.cols);
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.dataKey = "ebUserNum";
		this.dataInfos.paginator = true;

		this.dataInfos.cardView = true;
		this.dataInfos.dataLink = Statique.controllerCommunity + "/get-list-user-management";
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.showAddNewUserBtn = true;
		this.dataInfos.showCollapsibleBtn = true;

		this.dataInfos.lineActions = [];

		let editTitle: string = "Edit";
		let transfertTitle: string = "Change Establishment";

		this.translate.get("GENERAL.EDIT").subscribe((res: string) => {
			editTitle = res;
		});
		this.translate.get("GENERAL.TRANSFERT_ESTABLISHMENT").subscribe((res: string) => {
			transfertTitle = res;
		});

		this.dataInfos.lineActions.push({
			class: "btn btn-xs  font-size-10",
			icon: "fa fa-edit",
			title: editTitle,
			onClick: function(row: EbUser) {
				this.router.navigateByUrl("app/user/settings/company/users/" + row.ebUserNum);
			}.bind(this),
		});
		if (this.acls[this.ACL.Rule.Settings_Users_Change_Etablissemet_Enable]) {
			this.dataInfos.lineActions.push({
				class: "btn btn-xs  font-size-10",
				icon: "fa fa-exchange",
				title: transfertTitle,
				onClick: function(row: EbUser) {
					this.router.navigateByUrl("app/user/settings/company/etablissemnt/transfert/" + row.ebUserNum);
				}.bind(this),
			});
		}


		let delegateTitle: string = "Delete the administration right";
		this.translate.get("GENERAL.DELEGATE").subscribe((res: string) => {
			delegateTitle = res;
		}),
			this.dataInfos.lineActions.push({
				class: "btn btn-xs  font-size-10",
				icon: "fa fa-ban",
				title: delegateTitle,
				showCondition: function(row: EbUser) {
					return row.admin == true;
				},
				onClick: function(row: EbUser, $event) {
					this.delegate($event, row);
				}.bind(this),
			});

		let desactivateTitle: string = "Desactivate";
		this.translate.get("REPOSITORY_MANAGEMENT.DESACTIVER").subscribe((res: string) => {
			desactivateTitle = res;
		}),
			this.dataInfos.lineActions.push({
				class: "btn btn-xs  font-size-10",
				icon: "fa fa-power-off",
				title: desactivateTitle,
				showCondition: (row: EbUser) => row.status == EtatUser.ACTIF,
				onClick: function(row: EbUser, $event) {
					this.activateOrDesactivate($event, row);
				}.bind(this),
			});

		let activateTitle: string = "Activate";
		this.translate.get("REPOSITORY_MANAGEMENT.ACTIVATE").subscribe((res: string) => {
			activateTitle = res;
		}),
			this.dataInfos.lineActions.push({
				class: "btn btn-xs  font-size-10",
				icon: "fa fa-power-off",
				title: activateTitle,
				showCondition: (row: EbUser) =>
				row.status == EtatUser.NON_ACTIF ||
			    row.status == EtatUser.DESACTIVE,
				onClick: function(row: EbUser, $event) {
					this.activateOrDesactivate($event, row);
				}.bind(this),
			});

		let resentActivateMail: string = "Resent activation mail?";
		this.translate
			.get("REPOSITORY_MANAGEMENT.EN_ATTENTE_DE_CONFIRMATION")
			.subscribe((res: string) => {
				resentActivateMail = res;
			}),
			this.dataInfos.lineActions.push({
				class: "btn btn-xs  font-size-10 waiting",
				icon: "fa fa-paper-plane",
				title: resentActivateMail,
				showCondition: (row: EbUser) =>
					row.status == EtatUser.ACCEPTER_PAR_ADMIN ,
				onClick: function(row: EbUser, $event) {
					this.activateOrDesactivate($event, row);
				}.bind(this),
			});

		let refuseUserTitle: string = "Refuse User";
		this.translate.get("REPOSITORY_MANAGEMENT.REFUSE_USER").subscribe((res: string) => {
			refuseUserTitle = res;
		}),
			this.dataInfos.lineActions.push({
				class: "btn btn-xs  font-size-10 waiting",
				icon: "fa fa-trash",
				title: refuseUserTitle,
				showCondition: (row: EbUser) => row.status == EtatUser.NON_ACTIF,
				onClick: function(row: EbUser, $event) {
					this.refuseUser($event, row);
				}.bind(this),
			});
		this.affecteUserEtablissement();
		if (
			!this.ebEtablissement.ebCompagnie.ebCompagnieNum ||
			!this.ebEtablissement.ebEtablissementNum
		)
			return;
	}

	affecteUserEtablissement() {
		if (this.userConnected.superAdmin && this.userConnected.role == 3) {
			this.searchInput.ebCompagnieNum = null;
			this.searchInput.ebEtablissementNum = null;
			this.searchInput.roleControlTower = true;
		} else if (this.userConnected.superAdmin) {
			this.searchInput.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			this.searchInput.ebEtablissementNum = null;
			this.searchInput.superAdmin = true;
		} else if (this.userConnected.admin) {
			this.searchInput.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			this.searchInput.ebCompagnieNum = null;
			this.searchInput.admin = true;
		}
	}

	editAction(event) {
		this.router.navigate(["/app/delivery-overview/order/" + event.ebDelOrderNum]);
	}

	addNewUser() {
		this.typeParamSelected = null;
		this.profilSelected = null;
	}

	refuseUser(event, selectedUser: EbUser) {
		let that = this;
		let user: EbUser = new EbUser();
		let msg = "";
		user.constructorCopyLite(selectedUser);
		let refuseUserTitle: string = "Refuse User";
		this.translate.get("REPOSITORY_MANAGEMENT.REFUSE_USER").subscribe((res: string) => {
			refuseUserTitle = res;
		});
		let refuseUserConfirmationText: string = "Are you sure, you want to refuse this user?";
		this.translate
			.get("REPOSITORY_MANAGEMENT.REFUSE_USER_CONFIRMATION")
			.subscribe((res: string) => {
				refuseUserConfirmationText = res;
			});
		that.modalService.confirm(
			refuseUserTitle,
			refuseUserConfirmationText,
			function() {
				let requestProcessing = new RequestProcessing();
				requestProcessing.beforeSendRequest(event);
				that.userService.refuseUser(user).subscribe((data) => {
					requestProcessing.afterGetResponse(event);
					that.genericTable.refreshData();
				});
			}.bind(that)
		);
	}

	delegate(event, selectedUser: EbUser) {
		let msg = "";
		let user: EbUser = new EbUser();
		user.constructorCopyLite(selectedUser);

		if (user) msg = this.ConstantsTranslate["CONFIRM_REMOVE_ADMIN_RIGHTS"];
		else if (!user) {
			msg = this.ConstantsTranslate["CONFIRM_SET_AS_ADMIN"];
		}
		let adminDefinitionText: string = "Admin definition";
		this.translate.get("REPOSITORY_MANAGEMENT.ADMIN_DEFINTION").subscribe((res: string) => {
			adminDefinitionText = res;
		});
		this.modalService.confirm(
			adminDefinitionText,
			msg,
			function() {
				if (selectedUser.admin) selectedUser.admin = false;
				else selectedUser.admin = true;
				user.admin = selectedUser.admin;

				let requestProcessing = new RequestProcessing();
				requestProcessing.beforeSendRequest(event);
				user.ebEtablissement = new EbEtablissement();
				user.ebEtablissement.ebEtablissementNum = this.ebEtablissement.ebEtablissementNum;
				this.userService.updateAdminAndSuperAdminUser(user).subscribe((data) => {
					requestProcessing.afterGetResponse(event);
				});
			}.bind(this),
			() => {}
		);
	}

	activateOrDesactivate(event, selectedUser: EbUser) {
		let user: EbUser = new EbUser();
		let msg = "";
		user.constructorCopyLite(selectedUser);
		let log = this;

		if (user.status == EtatUser.ACTIF ) {
			msg = this.ConstantsTranslate["CONFIRM_DEACTIVATE_USER"];
		} else if (user.status == EtatUser.DESACTIVE ) {
			msg = this.ConstantsTranslate["CONFIRM_ACTIVATE_USER"];
		} else if (user.status == EtatUser.ACCEPTER_PAR_ADMIN) {
			msg = this.ConstantsTranslate["CONFIRM_RESENT_ACTIVATION_MAIL"];
		}else if (user.status == EtatUser.NON_ACTIF) {
			msg = this.ConstantsTranslate["CONFIRM_ACTIVATE_USER"];
		}
		let userStatusText: string = "User status";
		this.translate.get("REPOSITORY_MANAGEMENT.USER_STATUS").subscribe((res: string) => {
			userStatusText = res;
		});

		let uniqueAdminText: string = "Unique admin";
		this.translate.get("REPOSITORY_MANAGEMENT.UNIQUE_ADMIN").subscribe((res: string) => {
			uniqueAdminText = res;
		});

		let uniqueAdminDesactivateWarningText: string =
			"You can not deactivate your account because you are the unique admin of this establishment";
		this.translate
			.get("REPOSITORY_MANAGEMENT.UNIQUE_ADMIN_DESACTIVATE_WARNING")
			.subscribe((res: string) => {
				uniqueAdminDesactivateWarningText = res;
			});

		this.modalService.confirm(
			userStatusText,
			msg,
			function() {
				let requestProcessing = new RequestProcessing();
				requestProcessing.beforeSendRequest(event);
				user.ebEtablissement = new EbEtablissement();
				user.ebEtablissement.ebEtablissementNum = this.ebEtablissement.ebEtablissementNum;

				this.userService.updateStatusUser(user).subscribe((user: EbUser) => {
					//log.selectedUser.status = user.status;
					log.getClassCssUserCard(user.status);

					if (user.uniqueAdminOfEtablissement) {
						log.modalService.information(
							uniqueAdminText,
							uniqueAdminDesactivateWarningText,
							function() {}
						);
					}
					requestProcessing.afterGetResponse(event);
					this.genericTable.refreshData();
				});
			}.bind(this)
		);
	}

	search(event) {
		this.searchInput = event;
		this.affecteUserEtablissement();
	}

	translator(tab: any[]) {
		tab.forEach((it) => {
			this.translate.get(it.translateCode).subscribe((res) => {
				it["header"] = res;
			});
		});
	}

	getGroupName(groupID) {
		let groupName: string;
		if (this.groupes != undefined)
			this.groupes.forEach((group) => {
				if (group.ebUserGroupNum == groupID) groupName = group.name;
			});
		return groupName;
	}

	manageGroupes() {
		this.userService.getUserInfo(null).subscribe((res) => {
			let userGroupes = res.groupes;
			// Recuperation des groupes du  current User
			this.searchCriteria.listGroupeNum = [];
			if (userGroupes != null) {
				let toArray = userGroupes.split("|");
				toArray.forEach((col1) => {
					if (col1 != "") this.searchCriteria.listGroupeNum.push(parseInt(col1));
				});
			}
			// Recuperation des users en groupe
			this.loadPage(0);
			// Recuperation des groupes
			this.userService.getAllGroup(this.userConnected).subscribe((res) => {
				this.groupes = res;
				if (userGroupes != null) {
					let toArray = userGroupes.split("|");
					toArray.forEach((col1) => {
						res.forEach((col2) => {
							if (parseInt(col1) == col2.ebUserGroupNum) this.seletedGroupes.push(col2);
						});
					});
				}
			});
		});
	}

	filterGroupeMultiple(event) {
		let query = event.query;
		let filtered: any[] = [];
		for (let i = 0; i < this.groupes.length; i++) {
			if (this.groupes[i].name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
				filtered.push(this.groupes[i]);
			}
		}
		this.filteredGroupes = filtered;
	}

	onUserGroupChange() {
		// Recuperation des groupes du  current User
		this.searchCriteria.listGroupeNum = [];
		if (this.seletedGroupes.length != null) {
			this.seletedGroupes.forEach((groupe) => {
				this.searchCriteria.listGroupeNum.push(groupe.ebUserGroupNum);
			});
			this.loadPage(0);
		}
	}

	onChange(size) {
		this.searchCriteria.size = size;
		this.loadPage(0);
	}

	paginate(event) {
		this.searchCriteria.size = event.rows;
		this.loadPage(event.page);
	}

	loadPage(pageNumber: number = null) {
		if (pageNumber == null)
			pageNumber = this.listPages ? this.listPages[this.listPages.length - 1] : null;
		if (pageNumber == null) return;
		if (this.listPages && this.listPages[this.listPages.length - 1] < pageNumber) return;
		this.searchCriteria.pageNumber = pageNumber;
		this.searchCriteria.size = this.searchCriteria.size == null ? 10 : this.searchCriteria.size;

		this.userService.getListUserWithPagination(this.searchCriteria).subscribe((data) => {
			this.setDataAndPages(data);
		});
	}

	changeDisplayMode(event) {}

	searchUsers() {
		if (this.status == false) this.searchCriteria.status = EtatUser.NON_ACTIF;
		else this.searchCriteria.status = null;
		this.loadPage(0);
	}

	setDataAndPages(data: any) {
		// this.listUsers = data.records;
		this.totalRecords = data.total;
		let currentMax =
			(this.searchCriteria.pageNumber == 0 ? 1 : this.searchCriteria.pageNumber) *
			(this.searchCriteria.size >= 1 ? this.searchCriteria.size : this.totalRecords);
		this.toPage = currentMax > this.totalRecords ? this.totalRecords : currentMax;
		this.listPages = new Array<number>();

		let i,
			total = this.totalRecords / this.searchCriteria.size;
		let nbrePage = this.totalRecords % this.searchCriteria.size == 0 ? total : total + 1;
		nbrePage = this.totalRecords > this.searchCriteria.size ? nbrePage : 0;

		for (i = 1; i < nbrePage; i++) {
			this.listPages.push(i);
		}
	}

	generateXLSX() {
		let config = this.getDtConfig();
		this.exportService.startExportCsv(
			Modules.USER_MANAGEMENT,
			false,
			this.searchCriteria,
			null,
			config
		);
	}

	getDtConfig() {
		let dtConfig = [],
			index = 1;
		let cols = [
			{ field: "nom", header: "Nom" },
			{ field: "prenom", header: "Prenom" },
			{ field: "email", header: "Email" },
			{ field: "telephone", header: "Telephone" },
			{ field: "compagnie", header: "Compagnie" },
			{ field: "etablissement", header: "Telephone" },
			{ field: "role", header: "Role" },
			{ field: "status", header: "Status" },
			{ field: "isAdmin", header: "Admin" },
			{ field: "isSuperAdmin", header: "Super Admin" },
		];
		//cols.forEach(col => { dtConfig.push({ title: col.header, name: col.field, order: index++ }); });
		return dtConfig;
	}

	getClassCssUserCard(status: number) {
		if (status == EtatUser.NON_ACTIF) return "grey";
		else if (status == EtatUser.ACCEPTER_PAR_ADMIN) return "yellow";
		else if (status == EtatUser.ACTIF) return "green";
		else if (status == EtatUser.DESACTIVE) return "red";
		return "";
	}

	changeStatus(status: boolean) {
		//this.displayEdituser = status;
	}

	tasterDisplay(event: any) {
		if (event == true) {
			this.messageService.add({
				severity: "success",
				summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
				detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
			});
		}
		if (event == false) {
			this.messageService.add({
				severity: "error",
				summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
				detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE"),
			});
		}
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.USERS_MANAGEMENT",
			},
		];
	}
	deleteSelectedUsers() {
		let that = this;
		let message = that.ConstantsTranslate["CONFIRM_DELETE_SELECTED_USERS"];
		let dataFromTable = this.genericTable.getSelectedRows();
		if (dataFromTable.length > 0) {
			this.userService.checkAbilityToDeleteUers(dataFromTable).subscribe((data) => {
				if (data.length == 0) {
					this.modalService.confirm(
						that.ConstantsTranslate["DELETE_PRICING_HEAD"],
						message,
						function() {
							that.userService.deleteSelectedUsers(dataFromTable).subscribe((data) => {
								that.successfulOperation();
								// Force navigation to refresh the page after a save
								let url = that.router.url;
								that.router
									.navigateByUrl("/accueil", { skipLocationChange: true })
									.then(() => that.router.navigate([url]));
							},
							(err)=>
							{
								that.echecOperation();
							}
							);
						},
						function() {}
					);
				} else {
					let msg = "";
					data.forEach((element) => {
						msg = msg + element;
					});

					this.modalService.information(that.ConstantsTranslate["USERS_CAN_NOT_DELETED"], msg);
				}
			});
		}
	}

	successfulOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	echecOperation() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}

	getActionButtons(): Array<HeaderInfos.ActionButton> {
		return [
			{
				label: "SETTINGS.DELETE_SELECTED_USERS",
				icon: "fa fa-plus-circle",
				action: this.deleteSelectedUsers.bind(this),

				enableCondition: () => {
					return (
						this.genericTable != null &&
						this.genericTable.getSelectedRows() != null &&
						this.genericTable.getSelectedRows().length > 0
					);
				},
				displayCondition: () => {
					return this.isSuperAdmin;
				},
			},
		];
	}
	checkTheAbilityToDeleteUser(usersToCheck: Array<EbUser>): Array<EbDemande> {
		let listEbDemande: Array<EbDemande> = new Array<EbDemande>();
		this.userService.checkAbilityToDeleteUers(usersToCheck).subscribe((data) => {
			listEbDemande = data;
		});
		return listEbDemande;
	}
}
