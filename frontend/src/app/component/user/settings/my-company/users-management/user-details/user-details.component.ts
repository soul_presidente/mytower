import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
	ViewContainerRef,
	ViewChild,
} from "@angular/core";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { StatiqueService } from "@app/services/statique.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { EbEtablissement } from "@app/classes/etablissement";
import { EtatUser, ServiceType, UserRole } from "@app/utils/enumeration";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { EbUser, EbUserLite } from "@app/classes/user";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { UserService } from "@app/services/user.service";
import { ExUserModule } from "@app/classes/accessRight";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbCategorie } from "@app/classes/categorie";
import { EbLabel } from "@app/classes/label";
import { EcModule } from "@app/classes/EcModule";
import { Statique, keyValue } from "@app/utils/statique";
import { InscriptionService } from "@app/services/inscription.service";
import { CompagnieService } from "@app/services/compagnie.service";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbCompagnie } from "@app/classes/compagnie";
import { MessageService } from "primeng/api";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { AuthenticationService } from "@app/services/authentication.service";
import { AccessRightComponent } from "../../access-right/access-right.component";
import { EbUserProfile } from "@app/classes/userProfile";
import { UserProfileService } from "@app/services/user-profile.service";
import { Location } from "@angular/common";

@Component({
	selector: "app-user-details",
	templateUrl: "./user-details.component.html",
	styleUrls: [
		"./user-details.component.scss",
		"./../../../../../administrateur-company/administrateur-company.component.css",
	],
})
export class UserDetailsComponent extends ConnectedUserComponent implements OnInit {
	errorMessage: any;
	errorHeader: any;
	profilSelected: EbUserProfile;
	listProfileUser: any[] = [];
	typeParamSelected: number;
	disabledBloc: boolean = false;

	@ViewChild("accessRightsCompo", { static: false })
	accessRightsCompo: AccessRightComponent;

	ebCompagnie: EbCompagnie;
	ebEtablissement: EbEtablissement;
	listEtablissements: Array<EbEtablissement>;
	userToBeUpdated: EbUser = new EbUser();

	isEdit: boolean;

	Statique = Statique;
	UserRole = UserRole;

	mailError: string;
	userNameError: string;
	dataTableConfig: DataTableConfig = new DataTableConfig();

	broker: boolean;
	transporter: boolean;

	listCategories: Array<EbCategorie>;
	selectedCategory: EbCategorie;
	affectedCategories: Array<EbCategorie>;
	affectedCategoriesAndLabels: Array<EbCategorie>;

	selectedCategoryIndex: number;

	oldEmail: string;
	oldUserName: string;
	disableButton: boolean = false;
	requiredField: boolean;
	groupes: any[];
	filteredGroupes: any[];
	seletedGroupes: any[] = [];
	filteredGroupesMultiple: any[];
	dialogHeader: string;
	dialogContent: string;
	toastDisplay: boolean;

	showProfile: boolean = false;
	showDrtUser: boolean = false;
	disableDrtUser: boolean = false;

	selectedLabel: any[] = [];

	filteredLabels: any[] = [];
	loadingAccessRights: boolean = true;
	parametrageDroitsUser: keyValue[];
	manageCategorie: boolean = false;

	constructor(
		protected userService: UserService,
		private inscriptionService: InscriptionService,
		private compagnieService: CompagnieService,
		private router: Router,
		private route: ActivatedRoute,
		private messageService: MessageService,
		private location: Location,
		protected vcr?: ViewContainerRef,
		protected statiqueService?: StatiqueService,
		protected etablissementService?: EtablissementService,
		private translate?: TranslateService,
		protected modalService?: ModalService,
		protected headerService?: HeaderService,
		private userProfileService?: UserProfileService
	) {
		super();
	}

	ngOnInit(): void {
		this.loadingAccessRights = true;
		if (this.isSuperAdmin || this.isControlTower) this.manageCategorie = true;

		this.userToBeUpdated = new EbUser();
		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.compagnieService.getCompagnie(searchCriteria).subscribe((data: EbCompagnie) => {
			if (data.categories) {
				data.categories = data.categories.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}
			this.ebCompagnie = new EbCompagnie();
			this.ebCompagnie.constructorCopy(data);
			this.listCategories = Statique.cloneObject(this.ebCompagnie.categories);
		});

		this.etablissementService.getEtablisement(searchCriteria).subscribe((data: EbEtablissement) => {
			this.ebEtablissement = new EbEtablissement();
			this.ebEtablissement.constructorCopy(data);
			this.getUser();
		});

		this.userProfileService.getAll().subscribe((data) => {
			this.listProfileUser = data;
		});
		this.initParamAccessRightsList();
	}
	initParamAccessRightsList() {
		this.parametrageDroitsUser = Statique.parametrageDroitsUser;
		this.parametrageDroitsUser.forEach((element) => {
			this.translate.get(element.value).subscribe((res: string) => {
				element.value = res;
			});
		});
	}

	showUserCommunity() {
		this.router.navigate([
			"/app/user/settings/company/community/user/",
			this.userToBeUpdated.ebUserNum,
		]);
	}

	getUser() {
		if (this.route && this.route.snapshot.url[1].path == "creation") {
			this.isEdit = false;
			this.changeUser();
			if (!this.affectedCategories) this.affectedCategories = [];
			if (!this.affectedCategoriesAndLabels) this.affectedCategoriesAndLabels = [];
			this.headerService.registerBreadcrumbItems(
				this.getBreadcrumbItems(true),
				BreadcumbComponent.MY_COMPANY
			);
		} else {
			this.isEdit = true;

			this.route.params.subscribe((params: Params) => {
				let ebUserNum = +params["ebUserNum"];
				if (ebUserNum) {
					let searchCriteria: SearchCriteria = new SearchCriteria();
					searchCriteria.ebUserNum = ebUserNum;
					//searchCriteria.setUser(this.userConnected);
					searchCriteria.withAccessRights = true;
					searchCriteria.withEtablissement = true;
					searchCriteria.isCompagnie = true;
					this.userService.getUserDetails(searchCriteria).subscribe((data) => {
						this.userToBeUpdated = data;
						if (this.accessRightsCompo) this.accessRightsCompo.ngOnInit();

						// this.ebEtablissement = Statique.cloneObject(this.userToBeUpdated.ebEtablissement);
						this.headerService.registerBreadcrumbItems(
							this.getBreadcrumbItems(),
							BreadcumbComponent.MY_COMPANY
						);

						if (this.userConnected.ebCompagnie != null) {
							let searchCriteria: SearchCriteria = new SearchCriteria();
							searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
							this.compagnieService.getCompagnie(searchCriteria).subscribe((data) => {
								this.userConnected.ebCompagnie = data;
							});
						}

						let user = new EbUserLite(this.userToBeUpdated);
						this.userService.getAllGroup(user).subscribe((res) => {
							this.groupes = res;
							if (this.userToBeUpdated.groupes != null) {
								let toArray = this.userToBeUpdated.groupes.split("|");
								toArray.forEach((col1) => {
									res.forEach((col2) => {
										if (parseInt(col1) == col2.ebUserGroupNum) this.seletedGroupes.push(col2);
									});
								});
							}
						});

						this.oldEmail = this.userToBeUpdated.email;
						this.oldUserName = this.userToBeUpdated.username;
						this.changeUser();
						this.getParamFromProfile(data);

						if (this.userToBeUpdated.ebCompagnie && this.userToBeUpdated.ebCompagnie.ebCompagnieNum)
							this.disableDrtUser =
								this.userConnected.ebCompagnie.ebCompagnieNum !=
								this.userToBeUpdated.ebCompagnie.ebCompagnieNum;
					});
				}
			});
		}
	}

	updateUserAccessRights($event: number[]) {
		if (this.userToBeUpdated.listAccessRights.length > 0) {
			$event.forEach((it, index) => {
				let existe = false;
				this.userToBeUpdated.listAccessRights.forEach((element) => {
					if (element.ecModuleNum == index) {
						element.accessRight = $event[element.ecModuleNum];
						existe = true;
					}
				});
				if (!existe) {
					let newAccess = new ExUserModule();
					newAccess.accessRight = it;
					newAccess.ebUserNum = this.userToBeUpdated.ebUserNum;
					newAccess.ecModuleNum = index;
					this.userToBeUpdated.listAccessRights.push(newAccess);
				}
			});
		} else {
			this.initListAccessRight($event);
		}
	}

	filterLabels(event, labels, index) {
		this.filteredLabels[index] = [];
		for (let i = 0; i < labels.length; i++) {
			let label = labels[i];
			if (label.toLowerCase().indexOf(event.query.toLowerCase()) == 0) {
				this.filteredLabels[index].push(label);
			}
		}
	}

	filterGroupeMultiple(event) {
		let query = event.query;
		let filtered: any[] = [];
		for (let i = 0; i < this.groupes.length; i++) {
			if (this.groupes[i].name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
				filtered.push(this.groupes[i]);
			}
		}
		this.filteredGroupes = filtered;
	}

	compareCategory(cat1: EbCategorie, cat2: EbCategorie) {
		if (cat1 && cat2) {
			return cat1.ebCategorieNum === cat2.ebCategorieNum;
		}
	}

	editUser(event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		if (
			Statique.isNotDefined(this.userToBeUpdated.nom) ||
			Statique.isNotDefined(this.userToBeUpdated.prenom) ||
			Statique.isNotDefined(this.userToBeUpdated.email)
		)
			return;

		this.userToBeUpdated.listCategories = this.affectedCategoriesAndLabels;

		if (!this.userToBeUpdated.ebCompagnie) {
			const comp = new EbCompagnie();
			comp.ebCompagnieNum =
				typeof this.userToBeUpdated.ebEtablissement.ebCompagnie == "number"
					? this.userToBeUpdated.ebEtablissement.ebCompagnie
					: this.userToBeUpdated.ebEtablissement.ebCompagnie.ebCompagnieNum;
			this.userToBeUpdated.ebCompagnie = comp;
		} else if (typeof this.userToBeUpdated.ebCompagnie == "number") {
			const comp = new EbCompagnie();
			comp.ebCompagnieNum = this.userToBeUpdated.ebCompagnie;
			this.userToBeUpdated.ebCompagnie = comp;
		}

		if (this.seletedGroupes.length == 0) {
			this.userToBeUpdated.groupes = null;
		} else {
			let listGroupes = "|";
			this.seletedGroupes.forEach((groupe) => {
				listGroupes += groupe.ebUserGroupNum + "|";
			});
			this.userToBeUpdated.groupes = listGroupes;
		}
		//pour certain utilisateur on aura ERREUR 400 a cause de l'ebEtablissement donc je laisse que id
		let etab = new EbEtablissement();
		etab.ebEtablissementNum = this.userToBeUpdated.ebEtablissement.ebEtablissementNum;
		this.userToBeUpdated.ebEtablissement = etab;

		this.userService.updateUserDetails(this.userToBeUpdated).subscribe(
			(data) => {
				requestProcessing.afterGetResponse(event);
				this.tasterDisplay(true);

				if (this.userToBeUpdated.ebUserNum === this.userConnected.ebUserNum) {
					// update access rights in localStorage if user is editing his own access rights
					AuthenticationService.UpdateAccessRightsLocalStorage(
						this.userToBeUpdated.listAccessRights
					);

					// we need to refresh the page for the components to render with the right access rights
					window.location.replace("/app/user/settings/company/users");
				} else {
					this.router.navigateByUrl("/app/user/settings/company/users");
				}
			},
			(error) => {
				this.tasterDisplay(false);
				console.error(error);
			}
		);
	}

	initListAccessRight(accessRightsComponentOutput: number[]) {
		this.userToBeUpdated.listAccessRights = [];
		accessRightsComponentOutput.forEach((it, index) => {
			let newAccess = new ExUserModule();
			newAccess.accessRight = it;
			newAccess.ebUserNum = this.userToBeUpdated.ebUserNum;
			newAccess.ecModuleNum = index;

			this.userToBeUpdated.listAccessRights.push(newAccess);
		});
	}

	getDroitsUser(data) {
		this.userToBeUpdated.listAccessRights = data.listAccessRights;
		this.listCategories = Statique.cloneObject(this.ebCompagnie.categories);
		this.userToBeUpdated.listCategories = data.listCategories;

		// this.userToBeUpdated.telephone = data.telephone;
		if (this.userToBeUpdated.listCategories) {
			let listOrgCategorie = Statique.cloneObject(
				this.ebCompagnie.categories,
				new Array<EbCategorie>()
			);

			// categories/labels reellement affecté au user
			this.affectedCategoriesAndLabels = EbCategorie.getCopyOfListCategorie(
				this.userToBeUpdated.listCategories
			);
			let newAffectedCategoriesAndLabels = new Array<EbCategorie>();
			this.affectedCategories = listOrgCategorie.filter((it) => {
				let cat = this.affectedCategoriesAndLabels.find((ct) => {
					return ct.ebCategorieNum == it.ebCategorieNum;
				});
				if (cat) {
					let newCat: EbCategorie = (function(nc): EbCategorie {
						return JSON.parse(JSON.stringify(nc));
					})(cat);
					newCat.labels = new Array<EbLabel>();
					newAffectedCategoriesAndLabels.push(newCat);
					return true;
				}
			});

			// this.listCategories: categories restantes (contenant tous les labels)
			this.listCategories = listOrgCategorie.filter((it) => {
				let cat = this.affectedCategoriesAndLabels.find((ct) => {
					return ct.ebCategorieNum == it.ebCategorieNum;
				});
				if (!cat) return true;
			});

			// this.affectedCategories: categories affectés avec les labels restants
			this.affectedCategories.forEach((category) => {
				let cat = this.affectedCategoriesAndLabels.find((ct) => {
					return ct.ebCategorieNum == category.ebCategorieNum;
				});

				category.labels = category.labels.filter((label) => {
					let lab = cat.labels.find((lb) => {
						return lb.ebLabelNum == label.ebLabelNum;
					});
					if (!lab) return true;
					else {
						let newCat = newAffectedCategoriesAndLabels.find(
							(it) => it.ebCategorieNum == cat.ebCategorieNum
						);
						let newLabel: EbLabel = (function(nl): EbLabel {
							return JSON.parse(JSON.stringify(nl));
						})(label);
						newCat.labels.push(newLabel);
					}
				});
			});

			this.affectedCategoriesAndLabels = newAffectedCategoriesAndLabels;
		}
	}

	async changeUser() {
		if (!this.userToBeUpdated.status) this.userToBeUpdated.status = EtatUser.NON_ACTIF;
		if (this.userToBeUpdated.ebUserNum != null) {
			this.affectedCategories = new Array<EbCategorie>();
			this.affectedCategoriesAndLabels = new Array<EbCategorie>();

			this.selectedCategory = null;
			this.getDroitsUser(this.userToBeUpdated);
		}

		if (
			this.userConnected.superAdmin &&
			this.userConnected.ebCompagnie &&
			!this.userConnected.ebCompagnie.listEtablissements
		) {
			let criteriaListEtab = new SearchCriteria();
			criteriaListEtab.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			criteriaListEtab.withEtablissementDetail = true;
			this.compagnieService.getListEtablisementCompagnie(criteriaListEtab).subscribe((data) => {
				this.listEtablissements = data;
			});
		}
		this.loadingAccessRights = false;
	}

	addNewUser(event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		let that = this;
		that.userToBeUpdated.role = that.userConnected.role;

		if (!that.userConnected.superAdmin) {
			that.userToBeUpdated.ebEtablissement = new EbEtablissement();
			that.userToBeUpdated.ebEtablissement.ebEtablissementNum =
				that.userConnected.ebEtablissement.ebEtablissementNum;
			that.userConnected.ebCompagnie.listEtablissements = null;
		} else {
			if (!that.userToBeUpdated.ebEtablissement) {
				that.userToBeUpdated.ebEtablissement = new EbEtablissement();
				that.userToBeUpdated.ebEtablissement.ebEtablissementNum =
					that.userConnected.ebEtablissement.ebEtablissementNum;
			}
		}
		that.userToBeUpdated.ebCompagnie = new EbCompagnie();
		that.userToBeUpdated.ebCompagnie.ebCompagnieNum = that.userConnected.ebCompagnie.ebCompagnieNum;

		that.userToBeUpdated.ebEtablissement.ebCompagnie = new EbCompagnie();
		that.userToBeUpdated.ebEtablissement.ebCompagnie.ebCompagnieNum =
			that.userConnected.ebCompagnie.ebCompagnieNum;
		that.userConnected.ebCompagnie.listEtablissements = null;

		that.userToBeUpdated.listCategories = this.affectedCategoriesAndLabels;
		let param: any = Statique.cloneObject(that.userToBeUpdated);
		param.listAccessRights = param._listAccessRights;
		delete param._listAccessRights;
		that.inscriptionService.saveUser(param).subscribe(
			(user: EbUser) => {
				requestProcessing.afterGetResponse(event);
				this.tasterDisplay(true);
				this.router.navigateByUrl("/app/user/settings/company/users");
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				this.tasterDisplay(false);
			}
		);
	}

	checkMail(email: string) {
		this.mailError = null;
		if (this.oldEmail != email) {
			this.inscriptionService.checkEmail(email).subscribe(
				(data) => {
					if (data) {
						this.disableButton = true;
						this.mailError = null;
						this.translate.get("INSCRIPTION.EMAIL_EXISTANT").subscribe((res: string) => {
							this.mailError = res;
						});
					} else {
						this.disableButton = false;
					}
				},
				(error) => {
					console.log("Email nexiste pas!!!!!!!!!!!!!!!");
				}
			);
		}
		return this.disableButton;
	}

	checkUserName(userName: string) {
		this.userNameError = null;
		if (this.oldUserName != userName) {
			this.inscriptionService.checkUsername(userName).subscribe(
				(data) => {
					if (data) {
						this.disableButton = true;
						this.userNameError = null;
						this.translate.get("INSCRIPTION.USER_NAME_EXISTANT").subscribe((res: string) => {
							this.userNameError = res;
						});
					} else {
						this.disableButton = false;
					}
				},
				(error) => {
					console.log(error);
				}
			);
		}
	}
	tasterDisplay(event: any) {
		if (event == true) {
			this.messageService.add({
				severity: "success",
				summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
				detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
			});
		}
		if (event == false) {
			this.messageService.add({
				severity: "error",
				summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
				detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
			});
		}
	}
	get verified(): boolean {
		if (
			Statique.isDefined(this.userToBeUpdated.nom) &&
			this.userToBeUpdated.nom.length > 0 &&
			Statique.isDefined(this.userToBeUpdated.prenom) &&
			this.userToBeUpdated.prenom.length > 0 &&
			Statique.isDefined(this.userToBeUpdated.email) &&
			this.userToBeUpdated.email.length > 0 &&
			Statique.isDefined(this.userToBeUpdated.ebEtablissement) &&
			this.userToBeUpdated.email.match(Statique.patterEmail)
		) {
			this.requiredField = false;
		} else {
			this.requiredField = true;
		}
		return this.requiredField;
	}

	get verifiedPrestataire(): boolean {
		if (this.userConnected.rolePrestataire) {
			if (
				Statique.isDefined(this.userToBeUpdated.service) &&
				(this.userToBeUpdated.service == UserRole.PRESTATAIRE ||
					this.userToBeUpdated.service == ServiceType.TRANSPORTEUR ||
					this.userToBeUpdated.service == ServiceType.BROKER)
			)
				this.requiredField = false;
			else this.requiredField = true;
		}
		return this.requiredField;
	}
	private getBreadcrumbItems(
		isCreation: boolean = false,
		isEditCommunnity: boolean = false
	): Array<HeaderInfos.BreadCrumbItem> {
		let list = [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.USERS_MANAGEMENT",
				path: "/app/user/settings/company/users",
			},
			{
				label: isCreation ? "Creation" : this.userToBeUpdated.nomPrenom + "",
				path: "/app/user/settings/company/users/" + this.userToBeUpdated.ebUserNum,
			},
		];
		if (isEditCommunnity) {
			list.push({
				label: "ACL.THEME.COMMUNITY",
				path: "/app/user/settings/company/community/users/" + this.userToBeUpdated.ebUserNum,
			});
		}
		return list;
	}
	onChangeEtablissement() {
		this.ebEtablissement = this.userToBeUpdated.ebEtablissement;
		this.listCategories = this.ebCompagnie.categories;
	}

	sanitizeUsername() {
		// https://jira.adias.fr/browse/CPB-120
		// Space added to the end of username prevented the user from logging in
		this.userToBeUpdated.username = this.userToBeUpdated.username
			.trim()
			.toLowerCase()
			.replace(" ", "_");
	}
	seclectTypeParametrage(selectedValue) {
		if (this.userToBeUpdated.ebUserProfile) {
			this.userToBeUpdated.ebUserProfile.ebUserProfileNum = null;
		} else {
			this.userToBeUpdated.ebUserProfile = new EbUserProfile();
		}
		this.profilSelected = null;
		this.showDrtUser = false;
		if (selectedValue == 1 || selectedValue == 3) {
			this.showProfile = true;
			this.showDrtUser = false;
			this.disableDrtUser = true;
		} else if (selectedValue == 2) {
			this.showDrtUser = true;
			this.showProfile = false;
			this.disableDrtUser = false;
		}
	}
	getParamFromProfile(userToEdit) {
		this.showDrtUser = true;
		if (userToEdit && userToEdit.ebUserProfile && userToEdit.ebUserProfile.ebUserProfileNum) {
			this.showProfile = true;
			this.typeParamSelected = Statique.parametrageDroitsUser[0].key;
			this.disableDrtUser = true;
			this.profilSelected = userToEdit.ebUserProfile;
			let criteria = new SearchCriteria();
			criteria.ebUserProfileNum = this.profilSelected.ebUserProfileNum;
			criteria.withAccessRights = true;
			this.userProfileService.getDetailUser(criteria).subscribe((res) => {
				this.getDroitsUser(res);
			});
		} else {
			this.typeParamSelected = Statique.parametrageDroitsUser[1].key;
			this.disableDrtUser = false;
		}
	}
	selectUserProfile() {
		if (this.profilSelected && this.profilSelected.ebUserProfileNum > 0) {
			let criteria = new SearchCriteria();
			criteria.ebUserProfileNum = this.profilSelected.ebUserProfileNum;
			criteria.withAccessRights = true;
			this.userProfileService.getDetailUser(criteria).subscribe(async (res) => {
				this.getDroitsUser(res);
				this.showDrtUser = true;
				if (this.typeParamSelected == 1) {
					this.disableDrtUser = true;
					this.userToBeUpdated.ebUserProfile.ebUserProfileNum = this.profilSelected.ebUserProfileNum;
				} else {
					this.disableDrtUser = false;
				}
			});
		}
	}

	openAclSettings() {
		if (this.typeParamSelected == 1 && this.profilSelected.ebUserProfileNum != null) {
			this.router.navigate([
				"/app/user/settings/company/acl/profile/",
				this.profilSelected.ebUserProfileNum,
			]);
		} else {
			this.router.navigate([
				"/app/user/settings/company/acl/user/",
				this.userToBeUpdated.ebUserNum,
			]);
		}
	}
}
