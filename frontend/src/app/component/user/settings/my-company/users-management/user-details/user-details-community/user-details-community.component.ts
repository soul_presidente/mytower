import { Component } from "@angular/core";
import { _MyCompanyComponent } from "../../../my-company.component";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbUser } from "@app/classes/user";
import { Observable } from "rxjs";

@Component({
	selector: "app-user-details-community",
	templateUrl: "./user-details-community.component.html",
	styleUrls: ["./user-details-community.component.scss"],
})
export class MyCommunityUserDetailsComponent extends _MyCompanyComponent {
	loading: boolean = true;

	userId: number = null;

	ngOnInit() {
		this.loadParamsFromRouterUrl(() => this.loadData());
	}

	private loadParamsFromRouterUrl(callback?: () => void) {
		this.activatedRoute.params.subscribe((routeParams) => {
			this.userId = routeParams["ebUserNum"] != null ? +routeParams["ebUserNum"] : null;
			if (callback) {
				callback();
			}
		});
	}

	private getBaseBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
		];
	}

	async loadData() {
		let observableToCall: Observable<EbUser>;
		// Fire load of breadcrumb infos
		let breadcrumbItems = this.getBaseBreadcrumbItems();

		if (this.userId != null) {
			breadcrumbItems.push({
				label: "SETTINGS.USERS_MANAGEMENT",
				path: "/app/user/settings/company/users",
			});

			let criteria = new SearchCriteria();
			criteria.ebUserNum = this.userId;
			observableToCall = this.userService.getUserInfo(criteria);
			observableToCall.subscribe((user: EbUser) => {
				this.userToBeUpdated = user;
				let userBreacrumbItem: HeaderInfos.BreadCrumbItem = {
					label: user.nomPrenom,
					path: "/app/user/settings/company/users/" + this.userId,
				};
				breadcrumbItems.push(userBreacrumbItem);
				breadcrumbItems.push({
					label: "ACL.THEME.COMMUNITY",
				});
				this.headerService.registerBreadcrumbItems(breadcrumbItems, BreadcumbComponent.MY_COMPANY);
				this.loading = false;
			});
		}
	}
}
