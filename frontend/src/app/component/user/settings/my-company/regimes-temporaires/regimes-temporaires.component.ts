import { Component, OnInit, ViewChild, Output, EventEmitter } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { Modules } from "@app/utils/enumeration";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { GenericTableScreen } from "@app/utils/enumeration";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { SearchCriteria } from "@app/utils/searchCriteria";
import EbCptmRegimeTemporaireDTO from "@app/classes/cptm/EbCptmRegimeTemporaireDTO";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { AuthenticationService } from "@app/services/authentication.service";
import { ComptaMatiereService } from "@app/services/compta-matiere.service";
import { TranslateCompiler, TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
@Component({
	selector: "app-settings-regimes-temporaires",
	templateUrl: "./regimes-temporaires.component.html",
	styleUrls: ["./regimes-temporaires.component.scss"],
})
export class RegimesTemporairesComponent extends ConnectedUserComponent implements OnInit {
	Module = Modules;

	listRegime: Array<EbCptmRegimeTemporaireDTO> = new Array<EbCptmRegimeTemporaireDTO>();
	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;
	editMode: boolean = false;

	isAddEditMode = false;
	regime: EbCptmRegimeTemporaireDTO = new EbCptmRegimeTemporaireDTO();
	backupEditRegime: EbCptmRegimeTemporaireDTO;
	editingIndex: number;

	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;

	constructor(
		protected headerService: HeaderService,
		protected authenticationService: AuthenticationService,
		protected comptaMatiereService: ComptaMatiereService,
		protected translate: TranslateService,
		protected modalService: ModalService
	) {
		super(authenticationService);
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);

		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 50;

		this.dataInfos.cols = [
			{
				field: "type",
				translateCode: "CPTM.REGIME_TMP.FIELDS.TYPE",
			},
			{
				field: "description",
				translateCode: "CPTM.REGIME_TMP.FIELDS.DESCRIPTION",
			},
			{
				field: "deadlineDays",
				translateCode: "CPTM.REGIME_TMP.FIELDS.DEADLINE_DAYS",
			},
			{
				field: "alert1Days",
				translateCode: "CPTM.REGIME_TMP.FIELDS.ALERT1_DAYS",
			},
			{
				field: "alert2Days",
				translateCode: "CPTM.REGIME_TMP.FIELDS.ALERT2_DAYS",
			},
			{
				field: "alert3Days",
				translateCode: "CPTM.REGIME_TMP.FIELDS.ALERT3_DAYS",
			},
			{
				field: "leg1Name",
				translateCode: "CPTM.REGIME_TMP.FIELDS.LEG1_NAME",
			},
			{
				field: "leg1Code",
				translateCode: "CPTM.REGIME_TMP.FIELDS.LEG1_CODE",
			},
			{
				field: "leg2Name",
				translateCode: "CPTM.REGIME_TMP.FIELDS.LEG2_NAME",
			},
			{
				field: "leg2Code",
				translateCode: "CPTM.REGIME_TMP.FIELDS.LEG2_CODE",
			},
		];

		this.dataInfos.dataKey = "ebCptmRegimeTemporaireNum";
		this.dataInfos.dataType = EbCptmRegimeTemporaireDTO;
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerComptaMatiere + "/list-regime-temporaire-table";
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.REGIMES_TEMPORAIRES",
			},
		];
	}

	openAddForm(isUpdate?: boolean) {
		if (isUpdate) {
			this.editMode = true;
		} else {
			this.regime = new EbCptmRegimeTemporaireDTO();
			this.regime.alert1Days = 90;
			this.regime.alert2Days = 60;
			this.regime.alert3Days = 30;
			this.editMode = false;
		}
		this.isAddEditMode = true;
	}

	openEditForm(ebregime: EbCptmRegimeTemporaireDTO) {
		this.regime = ebregime;
		this.backupEditRegime = Statique.cloneObject<EbCptmRegimeTemporaireDTO>(
			this.regime,
			new EbCptmRegimeTemporaireDTO()
		);
		this.openAddForm(true);
	}

	openDeletePopup(regime: EbCptmRegimeTemporaireDTO) {
		let $this = this;

		this.modalService.confirm(
			this.translate.instant("CPTM.REGIME_TMP.DELETE_MESSAGE_CONFIRM_HEADER"),
			this.translate.instant("CPTM.REGIME_TMP.DELETE_MESSAGE_CONFIRM"),
			function() {
				$this.comptaMatiereService
					.deleteRegime(regime.ebCptmRegimeTemporaireNum)
					.subscribe((data) => {
						$this.genericTable.refreshData();
						$this.onDataChanged.next();
					});
			},
			function() {},
			true,
			this.translate.instant("GENERAL.CONFIRM"),
			this.translate.instant("GENERAL.CANCEL")
		);
	}

	backToListRegime() {
		if (this.editingIndex != null) {
			Statique.cloneObject<EbCptmRegimeTemporaireDTO>(
				this.backupEditRegime,
				this.listRegime[this.editingIndex]
			);
			this.editingIndex = null;
		}
		this.backupEditRegime = null;
		this.isAddEditMode = false;
	}
	editRegime(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.comptaMatiereService.updateRegime(this.regime).subscribe(
			(data) => {
				requestProcessing.afterGetResponse(event);
				this.isAddEditMode = false;
				this.editingIndex = null;
				this.backupEditRegime = null;
				this.genericTable.refreshData();
			},
			(error) => {
				console.error(error);
				requestProcessing.afterGetResponse(event);
			}
		);
	}
	addRegime(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.comptaMatiereService.updateRegime(this.regime).subscribe(
			(data: EbCptmRegimeTemporaireDTO) => {
				requestProcessing.afterGetResponse(event);
				this.isAddEditMode = false;
				this.genericTable.refreshData();
				this.onDataChanged.next();
			},
			(error) => {
				console.error(error);
				requestProcessing.afterGetResponse(event);
			}
		);
	}

	onDataLoaded(data: Array<EbCptmRegimeTemporaireDTO>) {
		this.onDataChanged.emit(data);
	}
}
