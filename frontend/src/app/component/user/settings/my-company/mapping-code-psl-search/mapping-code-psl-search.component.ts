import { Component, OnInit, OnChanges, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { IField } from '@app/classes/customField';
import { SearchCriteriaMappingCodePsl } from '@app/utils/SearchCriteriaMappingCodePsl';
import { GenericTableScreen, SavedFormIdentifier } from '@app/utils/enumeration';
import { SearchField } from '@app/classes/searchField';
import { AdvancedFormSearchComponent } from '@app/shared/advanced-form-search/advanced-form-search.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-mapping-code-psl-search',
  templateUrl: './mapping-code-psl-search.component.html',
  styleUrls: ['./mapping-code-psl-search.component.scss']
})
export class MappingCodePslSearchComponent implements OnInit, OnChanges {

  @Input()
	component: number;
	@Output()
	onSearch = new EventEmitter<any>();
	@Input()
	listFields: Array<IField>;
	SavedFormIdentifier = SavedFormIdentifier;
	GenericTableScreen = GenericTableScreen;

	paramFields: Array<SearchField> = new Array<SearchField>();
	@Input()
	initParamFields: Array<SearchField> = new Array<SearchField>();
	@Input()
	module: number;

	@ViewChild("advancedFormSearchComponentMappingCodePsl", { static: false })
	advancedFormSearchComponentMappingCodePsl: AdvancedFormSearchComponent;
	
	constructor(
		public translate: TranslateService
	) {}

	ngOnInit() {
		if (this.module == GenericTableScreen.MAPPING_CODE_PSL) {
			this.initParamFields = require("../../../../../../assets/ressources/jsonfiles/mapping-code-psl-search.json");
		}
		if (!this.paramFields || this.paramFields.length <= 0) {
			this.paramFields = [].concat(this.initParamFields);
		}
	}

	ngOnChanges() {
		this.component = GenericTableScreen.MAPPING_CODE_PSL;

		this.paramFields = [].concat(this.initParamFields);
		if (this.listFields && this.listFields.length > 0) {
			let arr = [];
			this.listFields.forEach((it) => {
				arr.push({
					label: it.label,
					name: it.name,
					type: "text",
				});
			});
			this.paramFields = this.paramFields.concat(arr);
		}
	}

	search(advancedOption: SearchCriteriaMappingCodePsl) {
		this.onSearch.emit(advancedOption);
	}

	clearSearch() {
		if (this.component == GenericTableScreen.MAPPING_CODE_PSL) {
			this.onSearch.emit(new SearchCriteriaMappingCodePsl());
		} 
	}

}
