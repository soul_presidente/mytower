import { Component, OnInit } from "@angular/core";
import { EbVehicule } from "../../../../../classes/vehicule";
import { Statique } from "../../../../../utils/statique";
import { VehiculeService } from "../../../../../services/vehicule.service";
import { ConnectedUserComponent } from "../../../../../shared/connectedUserComponent";
import { MessageService } from "primeng/api";
import { ModalService } from "../../../../../shared/modal/modal.service";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbModeTransport } from "../../../../../classes/EbModeTransport";
import { SearchCriteria } from "../../../../../utils/searchCriteria";

@Component({
	selector: "app-vehicule",
	templateUrl: "./vehicule.component.html",
	styleUrls: ["./vehicule.component.css"],
})
export class VehiculeComponent extends ConnectedUserComponent implements OnInit {
	listVehicule: Array<EbVehicule> = new Array<EbVehicule>();
	listModeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();

	listMarchandiseTypeOfUnit = Statique.MarchandiseTypeOfUnit;

	listTypeUnit = [];

	criteria: SearchCriteria = new SearchCriteria();

	statique = Statique;
	numberOnly: RegExp = /[0-9]/;
	constructor(
		private vehiculeService: VehiculeService,
		private messageService: MessageService,
		private modalService?: ModalService
	) {
		super();
	}

	getStatique() {
		let compagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		(async () => {
			this.listModeTransport = await SingletonStatique.getListModeTransport();
			this.listModeTransport = this.listModeTransport.sort((a, b) => a.order - b.order);
		})();

		this.vehiculeService.getListTypeUnit(compagnieNum).subscribe((data) => {
			this.listTypeUnit = data;
			console.log(this.listTypeUnit);
		});
	}

	ngOnInit() {
		this.getVehivule();
		this.getStatique();
	}

	add() {
		let newVehicule: EbVehicule = new EbVehicule();
		newVehicule.dateCreation = new Date();
		this.listVehicule.push(newVehicule);
	}

	saveOrUpdate() {
		console.log(this.listVehicule);
		this.listVehicule.forEach((vehicule) => {
			vehicule.xEbCompagnie = this.userConnected.ebCompagnie.ebCompagnieNum;
		});
		this.vehiculeService.save(this.listVehicule).subscribe((data) => {
			this.listVehicule = data;
			if (data) {
				this.messageService.add({
					severity: "success",
					summary: "Action reussie",
					detail: "Les vehicules ont été bien enregistré",
				});
			}
		});
	}

	getVehivule() {
		let compagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.vehiculeService.getListVehicule(compagnieNum).subscribe((data) => {
			this.listVehicule = data;
		});
	}

	delete(vehicule: EbVehicule) {
		let that = this;
		let index = that.listVehicule.indexOf(vehicule);
		that.modalService.confirm(
			"Suppression",
			"Voulez vous vraiment supprimer ce véhicule?",
			function() {
				if (vehicule.ebVehiculeNum == null) {
					that.listVehicule.splice(index, 1);
				}
				if (vehicule.ebVehiculeNum != null) {
					that.vehiculeService.deleteVehicule(vehicule.ebVehiculeNum).subscribe((data) => {
						if (data) that.listVehicule.splice(index, 1);
					});
				}
			},
			function() {}
		);
	}
}
