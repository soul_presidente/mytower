import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { EbEntrepotDTO } from "@app/classes/dock/EbEntrepotDTO";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableScreen, Modules } from "@app/utils/enumeration";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { DockManagementService } from "@app/services/dock-management.service";
import { EbEtablissement } from "@app/classes/etablissement";
import { CompagnieService } from "@app/services/compagnie.service";
import { EbUser } from "@app/classes/user";

@Component({
	selector: "app-entrepot-management",
	templateUrl: "./entrepot-management.component.html",
	styleUrls: ["./entrepot-management.component.scss"],
})
export class EntrepotManagementComponent implements OnInit {
	Statique = Statique;
	Module = Modules;
	entrepot: EbEntrepotDTO;
	listEntrepot: Array<EbEntrepotDTO> = new Array<EbEntrepotDTO>();
	listEtablissement: Array<EbEtablissement>;

	@Input("userConnected")
	userConnected: EbUser;
	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	isAddEntrepot = false;
	Entrepot: EbEntrepotDTO = new EbEntrepotDTO();
	backupEditEntrepot: EbEntrepotDTO;
	editingIndex: number;
	deleteMessage = "";
	deleteHeader = "";
	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	GenericTableScreen = GenericTableScreen;

	constructor(
		private translate?: TranslateService,
		protected modalService?: ModalService,
		protected dockManagementService?: DockManagementService,
		protected compagnieService?: CompagnieService
	) {}

	ngOnInit() {
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 50;

		this.dataInfos.cols = [
			{ field: "reference", header: "Reference", translateCode: "DOCK.ENTREPOT_REFERENCE" },
			{ field: "libelle", header: "Libelle", translateCode: "DOCK.ENTREPOT_LIBELLE" },
		];
		this.dataInfos.dataKey = "ebEntrepotNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerDock + "/list-entrepot-table";
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.dataType = EbEntrepotDTO;

		var listEtablissementCritera = new SearchCriteria();
		listEtablissementCritera.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.compagnieService
			.getListEtablisementCompagnie(listEtablissementCritera)
			.subscribe((data) => {
				this.listEtablissement = data;
			});
	}

	diplayEditEntrepot(ebEntrepot: EbEntrepotDTO) {
		this.entrepot = ebEntrepot;

		this.entrepot.etablissement = this.getEtablissementById(this.entrepot.ebEtablissementNum);
		this.backupEditEntrepot = this.entrepot;
		this.displayAddEntrepot(true);
	}

	displayAddEntrepot(isUpdate?: boolean) {
		if (isUpdate) {
		} else this.entrepot = new EbEntrepotDTO();
		this.isAddEntrepot = true;
	}

	backToListEntrepot() {
		if (this.editingIndex != null) {
			this.listEntrepot[this.editingIndex] = this.backupEditEntrepot;
			this.editingIndex = null;
		}
		this.backupEditEntrepot = null;
		this.isAddEntrepot = false;
	}
	editEntrepot() {
		this.entrepot = EbEntrepotDTO.constructorCopyDto(this.entrepot);
		this.dockManagementService.addEntrepot(this.entrepot).subscribe((data: EbEntrepotDTO) => {
			this.isAddEntrepot = false;
			this.editingIndex = null;
			this.backupEditEntrepot = null;
			this.genericTable.refreshData();
			this.onDataChanged.next();
		});
	}
	addEntrepot() {
		this.entrepot = EbEntrepotDTO.constructorCopyDto(this.entrepot);
		this.dockManagementService.addEntrepot(this.entrepot).subscribe((data: EbEntrepotDTO) => {
			this.isAddEntrepot = false;
			this.genericTable.refreshData();
			this.onDataChanged.next();
		});
	}

	deleteEntrepot(ebEntrepot: EbEntrepotDTO) {
		let $this = this;
		this.deleteMessage = null;
		this.deleteHeader = null;

		this.translate.get("DOCK.MESSAGE_DELETE_ENTREPOT").subscribe((res: string) => {
			this.deleteMessage = res;
		});

		this.translate.get("DOCK.DELETE_ENTREPOT").subscribe((res: string) => {
			this.deleteHeader = res;
		});
		this.modalService.confirm(
			this.deleteHeader,
			this.deleteMessage,
			function() {
				let Entrepot = ebEntrepot;
				$this.dockManagementService.deleteEntrepot(Entrepot.ebEntrepotNum).subscribe((data) => {
					$this.genericTable.refreshData();
					$this.onDataChanged.next();
				});
			},
			function() {},
			true
		);
	}

	changeModel(form: any) {
		setTimeout(
			function() {
				this.checkFields(form);
			}.bind(this),
			10
		);
	}

	checkFields(form: any) {
		var findOverlap = false;

		this.listEntrepot &&
			this.listEntrepot.some((aEntrepot, index, array) => {
				if (
					aEntrepot.reference &&
					this.entrepot.reference &&
					aEntrepot.ebEntrepotNum !== this.entrepot.ebEntrepotNum
				) {
					if (
						this.entrepot.reference.trim().toLowerCase() ===
						aEntrepot.reference.trim().toLowerCase()
					) {
						findOverlap = true;
						return true;
					}
				}
			});

		if (findOverlap) {
			form.controls["reference"].setErrors("overlap", true);
		} else if (this.entrepot.reference) {
			form.controls["reference"].setErrors(null);
		}
	}

	rerenderTable() {}

	onDataLoaded(data: Array<EbEntrepotDTO>) {
		this.onDataChanged.emit(data);
	}

	getEtablissementById(etabNum: number): EbEtablissement {
		let etab = new EbEtablissement();
		if (this.listEtablissement != null)
			this.listEtablissement.forEach((e) => {
				if (e.ebEtablissementNum == etabNum) {
					etab = e;
				}
			});
		return etab;
	}
}
