import { Component, Input, OnInit } from "@angular/core";
import { EbEntrepotDTO } from "@app/classes/dock/EbEntrepotDTO";
import { DockManagementService } from "@app/services/dock-management.service";
import { EbUser } from "@app/classes/user";
import { ActivatedRoute, Params } from "@angular/router";
import { Statique } from "@app/utils/statique";
import { HeaderService } from "@app/services/header.service";
import { BreadcumbComponent, HeaderInfos } from "@app/shared/header/header-infos";

@Component({
	selector: "app-settings-dock-management",
	templateUrl: "./dock-management.component.html",
	styleUrls: ["./dock-management.component.scss"],
})
export class DockManagementSettingsComponent implements OnInit {
	@Input("userConnected")
	userConnected: EbUser;
	params: Params;

	Statique = Statique;

	entrepotManagementScreen = false;

	listEntrepots: Array<EbEntrepotDTO> = new Array<EbEntrepotDTO>();
	selectedEntrepot: EbEntrepotDTO;

	constructor(
		protected dockManagementService: DockManagementService,
		protected activatedRoute: ActivatedRoute,
		protected headerService?: HeaderService
	) {}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.activatedRoute.queryParams.subscribe((params) => {
			this.params = params;
			this.refreshListEntrepot(() => {
				if (this.params["entrepotNum"]) {
					this.listEntrepots.forEach((e) => {
						let entrepotNumStr = String(e.ebEntrepotNum);
						if (entrepotNumStr === this.params["entrepotNum"]) {
							this.selectedEntrepot = e;
						}
					});
				}
			});
		});
	}

	displayEntrepotManagement() {
		this.entrepotManagementScreen = true;
	}

	closeEntrepotManagement() {
		this.selectedEntrepot = null;
		this.entrepotManagementScreen = false;
		this.refreshListEntrepot();
	}

	refreshListEntrepot(callback?: () => void) {
		this.dockManagementService.getListEntrepot().subscribe((data: Array<EbEntrepotDTO>) => {
			this.listEntrepots = data;
			if (callback) callback();
		});
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.DOCK_MANAGEMENT",
			},
		];
	}
}
