import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild,
} from "@angular/core";
import { DockManagementService } from "@app/services/dock-management.service";
import { EbUser } from "@app/classes/user";
import { EbDkOpeningPeriodDTO } from "@app/classes/dock/EbDkOpeningPeriodDTO";
import { EbDkOpeningWeekDTO } from "@app/classes/dock/EbDkOpeningWeekDTO";
import { OpeningPeriodType } from "@app/classes/dock/EnumerationDOCK";
import { Schedule } from "primeng/primeng";

@Component({
	selector: "app-opening-periods-calendar",
	templateUrl: "./opening-periods-calendar.component.html",
	styleUrls: ["./opening-periods-calendar.component.scss"],
})
export class OpeningPeriodsCalendarComponent implements OnInit, OnChanges {
	@Input("userConnected")
	userConnected: EbUser;
	@Input()
	entrepotNum: number;

	listOpeningWeek: Array<EbDkOpeningWeekDTO> = [];
	listOpeningPeriod: Array<EbDkOpeningPeriodDTO> = [];
	calendarEvents: Array<any> = [];

	viewedOpeningPeriod: EbDkOpeningPeriodDTO;
	viewedOpeningWeek: EbDkOpeningWeekDTO;
	savingViewedPeriod = false;

	@Output()
	periodUpdated: EventEmitter<number> = new EventEmitter<number>();

	@ViewChild("fcCmp", { static: true })
	fcCmp: Schedule;

	constructor(protected dockManagementService: DockManagementService) {}

	ngOnInit() {}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["entrepotNum"] && changes["entrepotNum"].currentValue != null) {
			this.reloadPeriods();
			this.reloadWeeks();
		}
	}

	reloadPeriods() {
		this.dockManagementService
			.getListOpeningPeriodByEntrepot(this.entrepotNum)
			.subscribe((result) => {
				this.listOpeningPeriod = result;
				this.generateCalendarEvents();
			});
	}

	reloadWeeks() {
		this.dockManagementService.getListOpeningWeeks(this.entrepotNum).subscribe((result) => {
			this.listOpeningWeek = result;
		});
	}

	protected generateCalendarEvents() {
		let events: Array<any> = [];
		this.listOpeningPeriod.forEach(function(period) {
			let event = {
				id: period.ebDkOpeningPeriodNum,
				start: period.startDate,
				end: period.endDate,
				title: period.label,
				color: period.typePeriod === OpeningPeriodType.EXCEPTIONAL ? "red" : "",
				allDay: true,
			};
			events.push(event);
		});
		this.calendarEvents = events;
	}

	onClickCalendarEvent(event: any) {
		this.viewedOpeningPeriod = this.findOpeningPeriod(event);
		this.loadViewedPeriodWeek();
	}

	onCalendarLapSelect(event: any) {
		console.log(event);
	}

	findOpeningPeriod(calendarEvent: any): EbDkOpeningPeriodDTO {
		const ebDkOpeningPeriodNum = calendarEvent.calEvent.id;
		let foundPeriod = this.listOpeningPeriod.find(function(item) {
			return item.ebDkOpeningPeriodNum == ebDkOpeningPeriodNum;
		});

		return foundPeriod;
	}

	loadViewedPeriodWeek() {
		this.viewedOpeningWeek = null;
		this.dockManagementService
			.getOpeningWeek(this.viewedOpeningPeriod.ebDkOpeningWeekNum)
			.subscribe((data) => {
				if (data != null) {
					this.viewedOpeningWeek = data;
				}
			});
	}

	//#region Modal UI
	saveViewedPeriod() {
		this.savingViewedPeriod = true;
		let $this = this;
		this.dockManagementService.saveOpeningPeriod(this.viewedOpeningPeriod).subscribe(
			(result) => {
				$this.reloadPeriods();
				$this.periodUpdated.emit(
					$this.viewedOpeningWeek ? $this.viewedOpeningWeek.ebDkOpeningWeekNum : null
				);
				$this.closeViewedPeriodModal();
			},
			(error) => {
				$this.savingViewedPeriod = false;
			}
		);
	}

	closeViewedPeriodModal() {
		this.viewedOpeningPeriod = null;
		this.viewedOpeningWeek = null;
		this.savingViewedPeriod = false;
	}

	get typePeriodSwitchChecked(): boolean {
		return (
			this.viewedOpeningPeriod != null &&
			this.viewedOpeningPeriod.typePeriod != null &&
			this.viewedOpeningPeriod.typePeriod === OpeningPeriodType.EXCEPTIONAL
		);
	}

	typePeriodeSwitchChange() {
		if (this.typePeriodSwitchChecked) {
			this.viewedOpeningPeriod.typePeriod = OpeningPeriodType.NORMAL;
		} else {
			this.viewedOpeningPeriod.typePeriod = OpeningPeriodType.EXCEPTIONAL;
		}
	}
	//#endregion

	protected FCHeaderConf = {
		left: "prev,next today",
		center: "title",
		right: "month,agendaWeek,agendaDay,listMonth",
	};

	public FCConf = {
		schedulerLicenseKey: "CC-Attribution-NonCommercial-NoDerivatives",
		header: this.FCHeaderConf,
		defaultView: "month",
		navLinks: true,
		selectable: true,
		editable: false,
		eventLimit: false,
		height: 420,
		timezone: "local",
	};
}
