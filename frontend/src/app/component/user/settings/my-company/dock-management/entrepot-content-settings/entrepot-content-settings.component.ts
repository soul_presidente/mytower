import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from "@angular/core";

import { EbUser } from "@app/classes/user";
import { DockManagementService } from "@app/services/dock-management.service";
import { EbDockDTO } from "@app/classes/dock/EbDockDTO";
import { OpeningPeriodsCalendarComponent } from "./opening-periods-calendar/opening-periods-calendar.component";
import { OpeningWeeksComponent } from "./opening-weeks/opening-weeks.component";

@Component({
	selector: "app-entrepot-content-settings",
	templateUrl: "./entrepot-content-settings.component.html",
	styleUrls: ["./entrepot-content-settings.component.scss"],
})
export class EntrepotContentSettingsComponent implements OnInit, OnChanges {
	@Input("userConnected")
	userConnected: EbUser;
	@Input()
	entrepotNum: number;

	@ViewChild("openingWeeksCmp", { static: true })
	openingWeeksCmp: OpeningWeeksComponent;

	@ViewChild("openingPeriodsCalendarCmp", { static: true })
	openingPeriodsCalendarCmp: OpeningPeriodsCalendarComponent;

	listDock: Array<EbDockDTO> = [];

	constructor(protected dockManagementService?: DockManagementService) {}

	ngOnInit() {}
	ngOnChanges(changes: SimpleChanges) {
		if (changes["entrepotNum"] && changes["entrepotNum"].currentValue != null) {
			this.reloadDocks();
		}
	}

	reloadDocks() {
		this.dockManagementService.getListDock(this.entrepotNum).subscribe(
			function(data) {
				this.listDock = data;
			}.bind(this)
		);
	}

	onPeriodCalendarUpdated(weekNum: number) {
		this.openingWeeksCmp.notifyPeriodOfWeekChange(weekNum);
	}

	onPeriodOpeningWeekChange() {
		this.openingPeriodsCalendarCmp.reloadPeriods();
	}

	onChangeListDock() {
		this.reloadDocks();
	}
}
