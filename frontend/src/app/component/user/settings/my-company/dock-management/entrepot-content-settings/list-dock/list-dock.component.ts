import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild,
} from "@angular/core";
import { StatiqueService } from "@app/services/statique.service";
import { EbUser } from "@app/classes/user";
import { DockManagementService } from "@app/services/dock-management.service";
import { EbDockDTO } from "@app/classes/dock/EbDockDTO";
import { GenericTableScreen } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { SearchCriteriaDOCK } from "@app/classes/dock/SearchCriteriaDOCK";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";

@Component({
	selector: "app-list-dock",
	templateUrl: "./list-dock.component.html",
	styleUrls: ["./list-dock.component.scss"],
})
export class ListDockComponent implements OnInit, OnChanges {
	@Input("userConnected")
	userConnected: EbUser;

	formDock: EbDockDTO = new EbDockDTO();
	isAddEditMode: boolean = false;

	searchCriteria = new SearchCriteriaDOCK();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	GenericTableScreen = GenericTableScreen;

	@Input()
	entrepotNum: number;
	@Output()
	listChange: EventEmitter<void> = new EventEmitter();

	constructor(
		protected translate: TranslateService,
		protected modalService: ModalService,
		protected statiqueService?: StatiqueService,
		protected dockManagementService?: DockManagementService
	) {}

	ngOnInit() {
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 10;
		this.searchCriteria.ebEntrepotNum = this.entrepotNum;

		this.dataInfos.cols = [
			{
				field: "reference",
				translateCode: "DOCK.FIELD.REFERENCE",
			},
			{
				field: "nom",
				translateCode: "DOCK.FIELD.NAME",
			},
			{
				field: "comment",
				translateCode: "DOCK.FIELD.COMMENT",
			},
			{
				field: "couleur",
				translateCode: "DOCK.FIELD.COLOR",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: (color: string) => {
					let html = "";

					html += '<div style="width: 100%; height: 25px; ';
					html += "border-radius: 4px; border-width: 2px; border-color: black; ";
					html += "background-color: " + color + ';"';
					html += "></div>";

					return html;
				},
			},
		];

		this.dataInfos.dataKey = "ebDockNum";
		this.dataInfos.dataType = EbDockDTO;
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerDock + "/get-list-dock-table";
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["entrepotNum"] && changes["entrepotNum"].currentValue) {
			this.searchCriteria.ebEntrepotNum = changes["entrepotNum"].currentValue;
			this.reloadTable();
		}
	}
	deleteDock(dock: EbDockDTO) {
		let $this = this;

		this.modalService.confirm(
			this.translate.instant("DOCK.DOCK_DELETE_MODAL_HEADER"),
			this.translate.instant("DOCK.DOCK_DELETE_MODAL_CONTENT"),
			function() {
				$this.dockManagementService.deleteDock(dock.ebDockNum).subscribe((data) => {
					$this.reloadTable();
					$this.listChange.emit();
				});
			},
			function() {},
			true
		);
	}
	displayEditDock(dock: any) {
		this.formDock.reference = dock.reference;
		this.formDock.comment = dock.comment;
		this.formDock.couleur = dock.couleur;
		this.formDock.ebDockNum = dock.ebDockNum;
		this.formDock.nom = dock.nom;
		this.formDock.ebEntrepotNum = dock.entrepot != null ? dock.entrepot.ebEntrepotNum : null;

		this.isAddEditMode = true;
	}
	displayAddDock() {
		this.formDock = new EbDockDTO();
		this.formDock.couleur = "#116fbf";
		this.isAddEditMode = true;
	}

	doSaveDock() {
		let $this = this;
		this.formDock.ebEntrepotNum = this.entrepotNum;
		this.dockManagementService.addDockPeriode(this.formDock).subscribe((newDock) => {
			$this.reloadTable();
			$this.isAddEditMode = false;
		});
	}
	backFromAddEdit() {
		this.isAddEditMode = false;
		this.formDock = null;
	}

	reloadTable() {
		this.genericTable.refreshData();
	}
}
