import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
} from "@angular/core";
import { Statique } from "@app/utils/statique";
import { EbDkOpeningDayDTO } from "@app/classes/dock/EbDkOpeningDayDTO";
import { DockManagementService } from "@app/services/dock-management.service";
import { EbDockDTO } from "@app/classes/dock/EbDockDTO";
import { Horaire } from "@app/classes/horaire";
import { EtatHoraire } from "@app/utils/enumeration";
import { Jour } from "@app/classes/jour";
import { EbRangeHourDTO } from "@app/classes/EbRangeHourDTO";

@Component({
	selector: "app-opening-day",
	templateUrl: "./opening-day.component.html",
	styleUrls: ["./opening-day.component.scss"],
})
export class OpeningDayComponent implements OnInit, OnChanges {
	Statique = Statique;
	constructor(protected dockManagementService?: DockManagementService) {}
	listDay: any;
	@Input()
	listOpeningDay: Array<EbDkOpeningDayDTO>;
	OpeningHours: Horaire = new Horaire();
	EtatHoraire = EtatHoraire;
	Jour = Jour;
	rowSpan: any;

	@Input()
	listDock: Array<EbDockDTO>;
	@Input()
	editable: boolean;
	@Output()
	changeListOpeningDay: EventEmitter<any> = new EventEmitter();
	listDockByDay: any;
	ngOnInit() {}
	ngOnChanges(changes: SimpleChanges) {
		if (this.listOpeningDay != null) {
			this.listOpeningDay = this.listOpeningDay.sort(function(a, b) {
				if (a.day.code == b.day.code) a.isRowSpan = true;
				return a.day.code - b.day.code;
			});
			var counts = {};
			this.listOpeningDay.forEach(function(x) {
				counts[x.day.name] = (counts[x.day.name] || 0) + 1;
			});
			this.rowSpan = counts;

			this.listDockByDay = {
				1: this.listDock,
				2: this.listDock,
				3: this.listDock,
				4: this.listDock,
				5: this.listDock,
				6: this.listDock,
				7: this.listDock,
			};

			if (typeof this.listDockByDay[1] !== "undefined") {
				for (var i = 1; i <= 7; i++) {
					let liste = this.listDockByDay[i];
					this.listDockByDay[i] = [];
					liste.forEach((dock, index) => {
						if (this.checkIfInListe(dock, i)) {
						} else {
							this.listDockByDay[i].push(dock);
						}
					});
				}
			}
		}
	}
	openChange(e) {
		let isChecked = e.checked;
	}
	addQuai(day) {
		let openingD = new EbDkOpeningDayDTO();
		openingD.day = day.day;
		openingD.isRowSpan = true;
		this.listOpeningDay.push(openingD);
		this.listOpeningDay = this.listOpeningDay.sort(function(a, b) {
			return a.day.code - b.day.code;
		});
		var counts = {};
		this.listOpeningDay.forEach(function(x) {
			counts[x.day.name] = (counts[x.day.name] || 0) + 1;
		});

		this.rowSpan = counts;
	}
	addRangeHour(day: EbDkOpeningDayDTO) {
		let hour: EbRangeHourDTO = new EbRangeHourDTO();
		day.hours.push(hour);
	}
	deleteRangeHour(day, hour) {
		day.hours.forEach((item, index) => {
			if (item == hour) day.hours.splice(index, 1);
		});
	}
	saveOpeningHour() {
		//let list = JSON.parse(JSON.stringify(this.listOpeningDay));
		this.dockManagementService
			.addOpeningDays(this.listOpeningDay)
			.subscribe(function(data) {}.bind(this));
	}
	displayDay(dayName) {
		if (this.rowSpan != null) return this.rowSpan[dayName];
		else return "";
	}
	deleteOpeningDay(day) {
		if (day.openingDayNum == null) {
			this.listOpeningDay.forEach((item, index) => {
				if (item == day) this.listOpeningDay.splice(index, 1);
			});
			var counts = {};
			this.listOpeningDay.forEach(function(x) {
				counts[x.day.name] = (counts[x.day.name] || 0) + 1;
			});
			this.rowSpan = counts;
		} else {
			this.dockManagementService.deleteOpeningDay(day).subscribe(
				function(data) {
					this.listOpeningDay.forEach((item, index) => {
						if (item == day) this.listOpeningDay.splice(index, 1);
					});
					var counts = {};
					this.listOpeningDay.forEach(function(x) {
						counts[x.day.name] = (counts[x.day.name] || 0) + 1;
					});
					this.rowSpan = counts;
				}.bind(this)
			);
		}
	}
	updateListeOpeningDay() {
		this.changeListOpeningDay.emit(this.listOpeningDay);
	}
	updateListeOpeningDayDock(day) {
		let liste = this.listDock;
		this.listDockByDay[day.day.code] = [];
		liste.forEach((dock, index) => {
			if (this.checkIfInListe(dock, day.day.code)) {
			} else {
				this.listDockByDay[day.day.code].push(dock);
			}
		});
		this.changeListOpeningDay.emit(this.listOpeningDay);
	}

	checkIfInListe(dock, code) {
		let check: boolean = false;
		this.listOpeningDay.forEach(function(item) {
			if (item.day.code == code) {
				item.ebdocks.forEach((Idock, index) => {
					if (Idock.ebDockNum == dock.ebDockNum) {
						check = true;
					}
				});
			}
		});
		return check;
	}
}
