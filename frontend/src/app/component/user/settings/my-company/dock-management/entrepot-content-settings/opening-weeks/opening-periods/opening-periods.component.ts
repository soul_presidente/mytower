import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { EbDkOpeningPeriodDTO } from "@app/classes/dock/EbDkOpeningPeriodDTO";
import { EbDkOpeningWeekDTO } from "@app/classes/dock/EbDkOpeningWeekDTO";
import { DockManagementService } from "@app/services/dock-management.service";
import { GenericEnum } from "@app/classes/GenericEnum";
import { StatiqueService } from "@app/services/statique.service";
import { GenericEnumDOCK } from "@app/classes/dock/EnumerationDOCK";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-opening-periods",
	templateUrl: "./opening-periods.component.html",
	styleUrls: ["./opening-periods.component.scss"],
})
export class OpeningPeriodsComponent implements OnInit {
	constructor(
		protected translate: TranslateService,
		protected statiqueService: StatiqueService,
		protected dockManagementService: DockManagementService
	) {}
	@Input()
	listOpeningPeriod: Array<EbDkOpeningPeriodDTO>;
	enableformAddPeriod: boolean;
	openingPeriod: EbDkOpeningPeriodDTO = new EbDkOpeningPeriodDTO();

	listTypePeriode: Array<GenericEnum> = [];

	@Input()
	openingWeekNum: number;

	@Output()
	periodChange = new EventEmitter<void>();

	ngOnInit() {
		this.statiqueService.getListGenericEnum(GenericEnumDOCK.OpeningPeriodType).subscribe((res) => {
			this.listTypePeriode = res;
		});
	}
	addPeriod() {
		this.enableformAddPeriod = true;
	}
	periodCancel() {
		this.enableformAddPeriod = false;
	}
	periodSave() {
		let $this = this;
		let week = new EbDkOpeningWeekDTO();
		week.ebDkOpeningWeekNum = this.openingWeekNum;
		this.dockManagementService.saveOpeningPeriod(this.openingPeriod).subscribe(
			function(data) {
				$this.reloadPeriods();
				$this.periodChange.emit();
				$this.enableformAddPeriod = false;
			}.bind(this)
		);
	}
	deleteOpeningPeriod(period: EbDkOpeningPeriodDTO) {
		this.dockManagementService
			.deleteOpeningPeriod(period.ebDkOpeningPeriodNum)
			.subscribe((nothing) => {
				this.periodChange.emit();
				this.dockManagementService.getListOpeningPeriod(this.openingWeekNum).subscribe(
					function(data) {
						this.listOpeningPeriod = data;
					}.bind(this)
				);
			});
	}
	updateOpeningPeriod(period) {
		this.openingPeriod = period;
		this.enableformAddPeriod = true;
	}

	getTypePeriodDisplay(typePeriodCode): string {
		let rValue = "";
		let foundKey = GenericEnum.findKeyByCode(typePeriodCode, this.listTypePeriode);

		if (foundKey) {
			rValue = this.translate.instant("DOCK.OPENING_PERIOD_TYPE." + foundKey);
		}

		return rValue;
	}

	reloadPeriods() {
		if (this.openingWeekNum != null) {
			this.dockManagementService.getListOpeningPeriod(this.openingWeekNum).subscribe(
				function(data2) {
					this.listOpeningPeriod = data2;
				}.bind(this)
			);
		}
	}

	notifyPeriodOfWeekChange(weekNum: number) {
		if (this.openingWeekNum != null && this.openingWeekNum === weekNum) {
			this.reloadPeriods();
		}
	}
}
