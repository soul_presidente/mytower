import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild,
} from "@angular/core";
import { DockManagementService } from "@app/services/dock-management.service";
import { EbDkOpeningWeekDTO } from "@app/classes/dock/EbDkOpeningWeekDTO";
import { EbDockDTO } from "@app/classes/dock/EbDockDTO";
import { EbDkOpeningDayDTO } from "@app/classes/dock/EbDkOpeningDayDTO";
import { JourEnum } from "@app/classes/jourEnum";
import { Statique } from "@app/utils/statique";
import { EbUser } from "@app/classes/user";
import { OpeningPeriodsComponent } from "./opening-periods/opening-periods.component";

@Component({
	selector: "app-opening-weeks",
	templateUrl: "./opening-weeks.component.html",
	styleUrls: ["./opening-weeks.component.scss"],
})
export class OpeningWeeksComponent implements OnInit, OnChanges {
	constructor(protected dockManagementService?: DockManagementService) {}
	listOpeningWeek: Array<EbDkOpeningWeekDTO>;
	@Input()
	listDock: Array<EbDockDTO>;
	listOpeningDay: Array<EbDkOpeningDayDTO>;

	selectedWeek: EbDkOpeningWeekDTO = new EbDkOpeningWeekDTO();

	listDay: any;
	@Input("userConnected")
	userConnected: EbUser;
	@Input()
	entrepotNum: number;

	@Output()
	periodChange = new EventEmitter<void>();

	@ViewChild("openingPeriodsCmp", { static: false })
	openingPeriodsCmp: OpeningPeriodsComponent;

	ngOnInit() {
		this.listDay = Statique.dowEnum;
	}
	ngOnChanges(changes: SimpleChanges) {
		this.getListWeeks();
	}
	getListWeeks() {
		this.dockManagementService.getListOpeningWeeks(this.entrepotNum).subscribe(
			function(data) {
				this.listOpeningWeek = data;
				if (this.listOpeningWeek.length <= 0) {
					this.addWeek();
					this.selectedWeek.isDefault = true;
				} else {
					var found = this.listOpeningWeek.find(function(element) {
						return element.isDefault == true;
					});
					if (found == null) found = this.listOpeningWeek[0];
					this.selectWeek(found);
				}
			}.bind(this)
		);
	}
	selectWeek(week: EbDkOpeningWeekDTO) {
		this.selectedWeek = null;
		if (week.ebDkOpeningWeekNum != null) {
			this.dockManagementService.getOpeningWeek(week.ebDkOpeningWeekNum).subscribe(
				function(data) {
					if (data != null) {
						this.selectedWeek = data;
					}
				}.bind(this)
			);
		} else {
			this.selectedWeek = week;
		}
		var found = this.listOpeningWeek.find(function(element) {
			return element.selected == true;
		});
		if (found != null) found.selected = false;
		week.selected = true;
	}
	onChangeListOpeningDay(listOpeningDay) {
		this.selectedWeek.days = listOpeningDay;
	}
	save(RefreshListWeek) {
		// if (RefreshListWeek || this.selectedWeek.ebDkOpeningWeekNum == null) {
		this.selectedWeek.ebEntrepotNum = this.entrepotNum;
		this.dockManagementService.saveOpeningWeek(this.selectedWeek).subscribe(
			function(data) {
				if (RefreshListWeek) {
					this.getListWeeks();
				} else {
					if (this.selectedWeek.ebDkOpeningWeekNum == null) {
						this.selectedWeek.ebDkOpeningWeekNum = data.ebDkOpeningWeekNum;
					}
					var found = this.listOpeningWeek.find(function(element) {
						return element.selected == true;
					});
					found.ebDkOpeningWeekNum = data.ebDkOpeningWeekNum;
					this.dockManagementService.getOpeningWeek(data.ebDkOpeningWeekNum).subscribe(
						function(data) {
							if (data != null) {
								this.selectedWeek = data;
							}
						}.bind(this)
					);
				}
			}.bind(this)
		);
		//}
	}
	addWeek() {
		this.listOpeningDay = new Array<EbDkOpeningDayDTO>();
		for (var day = 1; day <= this.listDay.length; ++day) {
			let openingD = new EbDkOpeningDayDTO();
			let jourEnum: JourEnum = new JourEnum();
			jourEnum.code = day;
			jourEnum.name = this.listDay[day - 1];
			openingD.day = jourEnum;
			this.listOpeningDay.push(openingD);
		}
		let week = new EbDkOpeningWeekDTO();
		week.label = "New week";
		week.days = this.listOpeningDay;
		var found = this.listOpeningWeek.find(function(element) {
			return element.selected == true;
		});
		if (found != null) found.selected = false;
		week.selected = true;
		this.selectedWeek = week;
		this.listOpeningWeek.push(week);
	}
	changeBaseWeek() {
		var found = this.listOpeningWeek.find(function(element) {
			return element.isDefault == true;
		});
		this.selectedWeek.isDefault = true;
		if (found != null) {
			found.isDefault = false;
			this.dockManagementService.saveOpeningWeek(found).subscribe(
				function(data) {
					this.save(true);
				}.bind(this)
			);
		} else {
			this.save(true);
		}
	}
	deleteWeek() {
		this.dockManagementService.deleteOpeningWeek(this.selectedWeek).subscribe(
			function(data) {
				this.getListWeeks();
			}.bind(this)
		);
	}

	onPeriodChange() {
		this.periodChange.emit();
	}

	notifyPeriodOfWeekChange(weekNum: number) {
		this.openingPeriodsCmp.notifyPeriodOfWeekChange(weekNum);
	}
}
