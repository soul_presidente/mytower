import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { CronjobMonitoring } from "@app/classes/cronjob/cronjobMonitoring";
import { CronjobMonitoringService } from "@app/services/cronjob-monitoring.service";
import { RequestProcessing } from '@app/utils/requestProcessing';

@Component({
	selector: "app-tracing-cronjob-monitoring",
	templateUrl: "./tracing-cronjob-monitoring.component.html",
	styleUrls: ["./tracing-cronjob-monitoring.component.scss"],
})
export class TracingCronjobMonitoringComponent implements OnInit {
	infosCronJob: CronjobMonitoring;

	constructor(
		public translate: TranslateService,
		public headerService: HeaderService,
		public cronjobMonitoringService: CronjobMonitoringService
	) {}

	ngOnInit() {
		this.infosCronJob = new CronjobMonitoring();
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.cronjobMonitoringService
			.getInfosLastExecutionCronjob()
			.subscribe((data: CronjobMonitoring) => {
				this.infosCronJob = data;
			});
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.CRONJOB_MONITORING",
			},
		];
	}

	changeStatusCronjob(status: Boolean) {
		this.cronjobMonitoringService.changeStatutCronjob(status).subscribe((data) => {
			this.infosCronJob = data;
		});
	}
	executeGenerationOfEvents(event){
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.cronjobMonitoringService.executeGenerationOfEvents().subscribe((data) => {
			if(data){
				this.infosCronJob = data;
			}
			requestProcessing.afterGetResponse(event);
		});
	}
}
