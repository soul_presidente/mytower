import { Component, OnInit } from "@angular/core";
import { DialogService } from "@app/services/dialog.service";
import { UserService } from "@app/services/user.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbEtablissement } from "@app/classes/etablissement";
import { SettingsComponent } from "../../settings.component";
import { EtatRelation, UserRole } from "@app/utils/enumeration";
import { EbUser } from "@app/classes/user";
import { EbRelation } from "@app/classes/relation";
import { AuthenticationService } from "@app/services/authentication.service";

@Component({
	selector: "app-etablissement-request",
	templateUrl: "./etablissement-request.component.html",
	styleUrls: ["../../settings.component.scss", "./etablissement-request.component.css"],
})
export class EtablissementRequestComponent extends SettingsComponent implements OnInit {
	searchCriteria: SearchCriteria = new SearchCriteria();
	listEtablissementOfContact: Array<EbRelation>;

	constructor(
		protected userService: UserService,
		protected dialogService: DialogService,
		protected authenticationService: AuthenticationService
	) {
		super(authenticationService);
	}

	ngOnInit() {
		this.searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.searchCriteria.etablissementRequest = true;
		this.searchEtablissement();
	}

	searchEtablissement() {
		this.searchCriteria.pageNumber = 0;
		this.userService.getEtablissementsContact(this.searchCriteria).subscribe((data) => {
			this.listEtablissementOfContact = data.records;
			console.log(this.listEtablissementOfContact);
		});
	}

	manageInvitation(ebRelation: EbRelation, index: number) {
		this.searchCriteria.ebEtablissementNum = ebRelation.xEbEtablissement.ebEtablissementNum;
		this.searchCriteria.userService = ebRelation.typeRelation;
		this.searchCriteria.ecRoleNum = UserRole.PRESTATAIRE;
		this.userService.getListUser(this.searchCriteria).subscribe(
			async function(data) {
				try {
					let ebUser: EbUser = await this.dialogService.selectInput(
						"Users of the etablissement",
						"Select a user",
						data,
						(it) => {
							let nomPrenom: string = "";

							if (it.nom) nomPrenom = it.nom;
							if (it.prenom && it.nom) nomPrenom += " ";
							if (it.prenom) nomPrenom += it.prenom;

							return nomPrenom;
						}
					);

					ebRelation.etat = EtatRelation.ACTIF;
					ebRelation.guest = new EbUser();
					ebRelation.guest.constructorCopyLite(ebUser);
					ebRelation.xEbEtablissementHost = new EbEtablissement();
					ebRelation.xEbEtablissementHost.constructorCopyLite(ebUser.ebEtablissement);
					ebRelation.lastModifiedBy = null;

					this.userService.updateUserContacts(ebRelation).subscribe(
						function(res) {
							this.listEtablissementOfContact.splice(index, 1);
						}.bind(this)
					);
				} catch (e) {}
			}.bind(this)
		);
	}
}
