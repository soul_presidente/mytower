import { Component, Input, OnInit, ViewContainerRef } from "@angular/core";
import { AdministrateurCompanyComponent } from "../../../../administrateur-company/administrateur-company.component";
import { EbEtablissement } from "@app/classes/etablissement";
import { EbCostCenter } from "@app/classes/costCenter";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EtablissementService } from "@app/services/etablissement.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { TranslateService } from "@ngx-translate/core";
import { MessageService } from "primeng/api";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbCompagnie } from "@app/classes/compagnie";
import { CostCenterService } from "@app/services/cost-center.service";
import { CompagnieService } from "@app/services/compagnie.service";

@Component({
	selector: "app-cost-centers",
	templateUrl: "./cost-centers.component.html",
	styleUrls: ["./cost-centers.component.css"],
})
export class CostCentersComponent extends ConnectedUserComponent implements OnInit {
	ebEtablissement: EbEtablissement = new EbEtablissement();
	ebCompagnie: EbCompagnie = new EbCompagnie();
	ebCostCenter: EbCostCenter = new EbCostCenter();
	listCostCenter: Array<EbCostCenter>;
	ebCompagnieNum: number;
	selectedIndex: number;
	deleteMessage = "";
	deleteHeader = "";

	constructor(
		protected vcr?: ViewContainerRef,
		protected etablissementService?: EtablissementService,
		protected compagnieService?: CompagnieService,
		protected costCenterService?: CostCenterService,
		protected modalService?: ModalService,
		private translate?: TranslateService,
		private messageService?: MessageService,
		protected headerService?: HeaderService
	) {
		super();
	}

	ngOnInit(): void {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);

		this.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.costCenterService
			.getListCostCenterCompagnie(this.ebCompagnieNum)
			.subscribe((res: Array<EbCostCenter>) => {
				this.listCostCenter = res;
			});

		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.etablissementService.getEtablisement(searchCriteria).subscribe((data: EbEtablissement) => {
			this.ebEtablissement.constructorCopy(data);
		});

		this.compagnieService.getCompagnie(searchCriteria).subscribe((data: EbCompagnie) => {
			if (data.categories) {
				data.categories = data.categories.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}
			this.ebCompagnie.constructorCopy(data);
		});
	}

	saveCostCenter(event: any) {
		if (
			!this.ebCostCenter ||
			!this.ebCostCenter.libelle ||
			this.ebCostCenter.libelle.length == 0 ||
			this.ebCostCenter.libelle.trim().length == 0
		)
			return;

		if (this.listCostCenter && this.listCostCenter.length > 0) {
			let existingCost = this.listCostCenter.find((it) => {
				return it.libelle == this.ebCostCenter.libelle;
			});
			if (existingCost) return;
		}

		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		if (this.ebCostCenter.ebCostCenterNum == null) {
			this.ebCostCenter.compagnie = new EbCompagnie();
			this.ebCostCenter.compagnie.ebCompagnieNum = this.ebCompagnieNum;
			this.costCenterService.addCostCenterCompagnie(this.ebCostCenter).subscribe((data) => {
				requestProcessing.afterGetResponse(event);
				this.ebCostCenter.ebCostCenterNum = data["ebCostCenterNum"];
				this.listCostCenter.push(this.ebCostCenter);
				this.ebCostCenter = new EbCostCenter();
				this.showSuccess();
			});
		} else {
			if (this.ebCostCenter.compagnie == null)
				this.ebCostCenter.compagnie = this.ebCompagnie;
			this.costCenterService.editCostCenterCompagnie(this.ebCostCenter).subscribe((data) => {
				requestProcessing.afterGetResponse(event);
				this.listCostCenter[this.selectedIndex] = data;
				this.ebCostCenter = new EbCostCenter();
				this.showSuccess();
			});
		}
	}

	deleteCostCenter(event: any, index: number) {
		this.deleteMessage = null;
		this.deleteHeader = null;
		this.translate
			.get("REPOSITORY_MANAGEMENT.MESSAGE_DELETE_COST_CENTER")
			.subscribe((res: string) => {
				this.deleteMessage = res;
			});

		this.translate.get("REPOSITORY_MANAGEMENT.DELETE_COST_CENTER").subscribe((res: string) => {
			this.deleteHeader = res;
		});
		this.modalService.confirm(
			this.deleteHeader,
			this.deleteMessage,
			function() {
				let costCenter = this.listCostCenter[index];
				this.costCenterService.deleteCostCenterCompagnie(costCenter).subscribe((data) => {
					if (data) {
						this.listCostCenter.splice(index, 1);
						this.showSuccess();
					}
				});
			}.bind(this),
			() => {}
		);
	}

	setCostCenter(index: number) {
		this.ebCostCenter = new EbCostCenter();
		this.ebCostCenter.constructorCopy(this.listCostCenter[index]);
		this.selectedIndex = index;
	}

	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE"),
		});
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.REFERENCES",
			},
		];
	}
}
