import { Component, OnInit } from "@angular/core";
import { SavedForm } from "@app/classes/savedForm";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { Statique } from "@app/utils/statique";
import { StatiqueService } from "@app/services/statique.service";
import { EbUser } from "@app/classes/user";
import { AuthenticationService } from "@app/services/authentication.service";
import { ParamsMail } from "@app/classes/paramsMail";
import { SearchField } from "@app/classes/searchField";
import { Modules } from "@app/utils/enumeration";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EtablissementService } from "@app/services/etablissement.service";
import { EbCategorie } from "@app/classes/categorie";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { EcModule } from "@app/classes/EcModule";

@Component({
	selector: "app-rule",
	templateUrl: "./rule.component.html",
	styleUrls: ["./rule.component.css"],
})
export class RuleComponent implements OnInit {
	constructor(
		private savedFormService: SavedFormsService,
		private statiqueService: StatiqueService,
		private etablissementService: EtablissementService,
		protected headerService: HeaderService
	) {}
	ebRule: SavedForm;
	enableFormAddRule: boolean = false;
	rules: any;
	listActions: any = null;
	listActionTrigger: any = null;
	Statique = Statique;
	connectedUser: EbUser = AuthenticationService.getConnectedUser();
	modalVisible: boolean = false;
	listCriteres: any;
	modalLoading: boolean = false;
	listMails: Array<ParamsMail> = new Array<ParamsMail>();
	isCritere: boolean = false;
	paramFields: Array<SearchField> = new Array<SearchField>();
	initParamFields: Array<SearchField> = new Array<SearchField>();
	moduleName = Modules;
	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.statiqueService.getListSavedSearchAction().subscribe((data) => {
			this.listActions = data;
		});
		this.statiqueService.getListSavedSearchActionTrigger().subscribe((data) => {
			this.listActionTrigger = data;
		});
		this.savedFormService.getListRule().subscribe(
			function(data) {
				this.rules = data;
			}.bind(this)
		);
		this.statiqueService.getListEmailParam().subscribe((res: Array<ParamsMail>) => {
			this.listMails = res;
		});
	}
	getLibelleActionByCode(code) {
		if (this.listActions != null && code != null) {
			let item = this.listActions.find(function(element) {
				return element.code == code;
			});
			return item.libelle;
		}
	}
	getLibelleSearchActionTriggerByCode(code) {
		if (this.listActionTrigger != null && code != null) {
			let item = this.listActionTrigger.find(function(element) {
				return element.code == code;
			});
			return item.libelle;
		}
	}
	getLibelleSendMail(code: number) {
		if (this.listMails != null && code != null) {
			let item = this.listMails.find(function(element) {
				return element.emailId == code;
			});
			return item.emailName;
		}
	}
	deleteRule(SavedForm) {
		this.savedFormService.deleteSavedForm(SavedForm).subscribe(
			function(data) {
				this.savedFormService.getListRule().subscribe(
					function(data) {
						this.rules = data;
					}.bind(this)
				);
			}.bind(this)
		);
	}
	updateRule(rule: SavedForm) {
		this.ebRule = rule;
		if (this.ebRule.ebModuleNum == this.moduleName.PRICING) {
			this.initParamFields = require("../../../../../../assets/ressources/jsonfiles/pricing-searchfields.json");
		}
		if (this.ebRule.ebModuleNum == this.moduleName.TRANSPORT_MANAGEMENT) {
			this.initParamFields = require("../../../../../../assets/ressources/jsonfiles/booking-searchfields.json");
		}

		if (!this.paramFields || this.paramFields.length <= 0) {
			this.paramFields = [].concat(this.initParamFields);
		}

		let criteria: SearchCriteria = new SearchCriteria();
		let listCustomField = [];
		let listCategorie = [];
		let listFields = [];
		this.savedFormService.getListSavedSearchValues(rule.ebSavedFormNum).subscribe(
			function(data) {
				this.listCriteres = data;
			}.bind(this)
		);
		criteria.size = null;
		criteria.ebCompagnieNum = this.connectedUser.ebCompagnie.ebCompagnieNum;
		criteria.ebEtablissementNum = this.connectedUser.ebEtablissement.ebEtablissementNum;
		criteria.ignoreContact = true;
		criteria.ebUserNum = this.connectedUser.ebUserNum;

		this.etablissementService.getCategoryCustom(criteria).subscribe((res) => {
			listCategorie = res && res.listCategorie ? res.listCategorie : new Array<EbCategorie>();
			listCustomField = res.customField;
			listCustomField.forEach((el) => {
				let listFieldsTmp = el && el.fields ? JSON.parse(el.fields) : null;
				if (listFieldsTmp) {
					listFields = listFields.concat(listFieldsTmp);
				}
			});
			if (listFields && listFields.length > 0) {
				let arr = [];
				listFields.forEach((it) => {
					arr.push({
						label: it.label,
						name: it.name,
						type: "text",
					});
				});
				this.paramFields = this.paramFields.concat(arr);
			}
			if (listCategorie && listCategorie.length > 0) {
				let arr = [];
				let i = 0;
				listCategorie.forEach((it, i) => {
					arr.push({
						label: it.libelle,
						type: "autocomplete",
						name: "categoryField" + i,
						listItems: it.labels,
						fetchAttribute: "libelle",
						fetchValue: "ebLabelNum",
						multiple: "true",
					});
				});
				this.paramFields = this.paramFields.concat(arr);
			}

			this.paramFields.forEach((it) => {
				let def = this.listCriteres.find(function(item) {
					return item.key == it.name;
				});
				if (def != null && def.value != null) {
					if (it.name.indexOf("categoryField") !== -1) {
						let arr = [];
						def.value.forEach((element) => {
							arr.push(element.ebLabelNum);
						});
						// it.default = arr;
					} else {
						it.default = def.value;
					}
				}
			});

			this.enableFormAddRule = true;
		});
	}

	ruleBack() {
		this.enableFormAddRule = false;
	}

	orderRule(rule: SavedForm, order) {
		let found = null;
		if (order == "up" && rule.ordre != 0) {
			found = this.rules.find(function(element) {
				return element.ordre < rule.ordre;
			});

			if (found != null) {
				found.ordre++;
				rule.ordre--;
			}
		}
		if (order == "down") {
			found = this.rules.find(function(element) {
				return element.ordre > rule.ordre;
			});

			if (found != null) {
				found.ordre--;
				rule.ordre++;
			}
		}
		if (found != null) {
			rule.ebUser = this.connectedUser;
			rule.compagnie = this.connectedUser.ebCompagnie;
			found.ebUser = this.connectedUser;
			found.compagnie = this.connectedUser.ebCompagnie;
			this.savedFormService.saveSavedForm(rule).subscribe(
				function(d) {
					this.savedFormService.saveSavedForm(found).subscribe(
						function(data) {
							this.rules.sort(function(a, b) {
								return a.ordre - b.ordre;
							});
						}.bind(this)
					);
				}.bind(this)
			);
		}
	}
	displayCritere(rule: SavedForm) {
		this.modalLoading = true;
		this.modalVisible = true;
		this.savedFormService.getListSavedSearchValues(rule.ebSavedFormNum).subscribe(
			function(data) {
				this.listCriteres = data;
				this.isCritere = true;
				this.modalLoading = false;
			}.bind(this)
		);
	}
	modalClose() {
		this.modalVisible = false;
	}
	applyToHistory(rule: SavedForm) {
		this.isCritere = false;
		this.modalLoading = true;
		this.modalVisible = true;
		this.savedFormService
			.applyToHistory(rule.ebSavedFormNum)
			.subscribe(function(data) {}.bind(this));
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.RULES",
			},
		];
	}
}
