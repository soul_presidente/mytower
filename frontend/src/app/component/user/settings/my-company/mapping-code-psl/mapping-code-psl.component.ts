import { Component, OnInit, ViewChild, Input, SimpleChanges, OnChanges } from "@angular/core";
import { EdiMapPsl } from "@app/classes/mappingCodePsl";
import { GenericTableScreen, TypeOfEvent } from "@app/utils/enumeration";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { Statique } from "@app/utils/statique";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { MappingCodePslService } from "@app/services/mapping-code-psl.service";
import { SearchField } from "@app/classes/searchField";
import { SearchCriteriaMappingCodePsl } from "@app/utils/SearchCriteriaMappingCodePsl";
import { MappingCodePslSearchComponent } from "../mapping-code-psl-search/mapping-code-psl-search.component";
import { Router } from "@angular/router";
import { CompagnieService } from "@app/services/compagnie.service";
import { EbCompagnie } from "@app/classes/compagnie";
import { UserService } from "@app/services/user.service";
import { EbTtCategorieDeviation } from "@app/classes/categorieDeviation";
import { MessageService } from "primeng/api";
import { EbTtCompanyPsl } from "@app/classes/ttCompanyPsl";

@Component({
	selector: "app-mapping-code-psl",
	templateUrl: "./mapping-code-psl.component.html",
	styleUrls: ["./mapping-code-psl.component.scss"],
})
export class MappingCodePslComponent extends ConnectedUserComponent implements OnInit, OnChanges {
	Statique = Statique;
	datatableComponent = GenericTableScreen;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	detailsInfos: GenericTableInfos = new GenericTableInfos(this);
	paramFields: Array<SearchField> = new Array<SearchField>();
	initParamFields: Array<SearchField> = new Array<SearchField>();
	module: number;

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;
	isStatiquesLoaded = false;
	isAddObject = false;
	dataObject: EdiMapPsl = new EdiMapPsl();
	backupEditObject: EdiMapPsl;
	editingIndex: number;
	deleteMessage = "";
	deleteHeader = "";
	searchCriteria: SearchCriteriaMappingCodePsl = new SearchCriteriaMappingCodePsl();
	listMappingCodePsl: Array<EdiMapPsl> = new Array<EdiMapPsl>();
	userConnectedCompagnie: EbCompagnie;

	@Input("searchInput")
	set searchInput(searchInput: SearchCriteriaMappingCodePsl) {
		if (searchInput != null) {
			let tmpSearchInput: SearchCriteriaMappingCodePsl = new SearchCriteriaMappingCodePsl();

			tmpSearchInput.module = this.module;
			tmpSearchInput.ebUserNum = this.userConnected.ebUserNum;
			tmpSearchInput.searchInput = JSON.stringify(searchInput || {});

			this.searchCriteria = tmpSearchInput;
		}
	}
	GenericTableScreen = GenericTableScreen;

	@ViewChild("mappingCodePslSearchComponent", { static: true })
	mappingCodePslSearchComponent: MappingCodePslSearchComponent;

	constructor(
		protected router?: Router,
		protected modalService?: ModalService,
		protected translate?: TranslateService,
		protected headerService?: HeaderService,
		protected mappingCodePslservice?: MappingCodePslService,
		protected compagnieService?: CompagnieService,
		protected userService?: UserService,
		protected messageService?: MessageService
	) {
		super();
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["module"] || (changes["searchInput"] && changes["searchInput"].firstChange))
			this.ngOnInit();
	}

	ngOnInit(): void {
		this.initParamFields = require("../../../../../../assets/ressources/jsonfiles/mapping-code-psl-search.json");

		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);

		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 5;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteria.connectedUserNum = this.userConnected.ebUserNum;

		this.dataInfos.cols = [
			{
				field: "codePslNormalise",
				header: "Code Psl Normalise",
				translateCode: "SETTINGS.CODE_PSL_NORMALISE",
			},
			{
				field: "codePslTransporteur",
				header: "Code Psl Transporteur",
				translateCode: "SETTINGS.CODE_PSL_TRANSPORTEUR",
			},
			{
				field: "typeEvent",
				header: "Event Type",
				translateCode: "SETTINGS.TYPE_OF_EVENT",
				render: function(typeEvent: TypeOfEvent, d, row) {
					return typeEvent == 1
						? "INCIDENT"
						: typeEvent == 2
						? "EVENT"
						: typeEvent == 3
						? "UNCONFORMITY"
						: "";
				},
			},
			{
				field: "xEbCompany",
				header: "Code compagnie",
				translateCode: "SETTINGS.CODE_CAMPAGNE",
				render: function(xEbCompany: EbCompagnie, d, row) {
					return xEbCompany && xEbCompany.ebCompagnieNum ? xEbCompany.nom : "";
				},
			},
			{
				field: "xEbCompanyTransporteur",
				header: "Code compagnie Transporteur",
				translateCode: "SETTINGS.CODE_CAMPAGNE_TRANSPORTEUR",
				render: function(xEbCompanyTransporteur: EbCompagnie, d, row) {
					return xEbCompanyTransporteur && xEbCompanyTransporteur.ebCompagnieNum
						? xEbCompanyTransporteur.nom
						: "";
				},
			},
			{
				field: "numCodeRaison",
				header: "Num Code Raison",
				translateCode: "SETTINGS.NUM_CODE_RAISON",
			},
			{
				field: "codePslTransporteurDescriptif",
				header: "Code Psl Transporteur Descriptif",
				translateCode: "SETTINGS.CODE_PSL_TRANSPORTEUR_DESCRIPTIF",
			},
			{
				field: "xEbCategorieDeviation",
				header: "Categorie Deviation",
				translateCode: "SETTINGS.DEVIATION_CATEGO",
				width: "150px",
				render: function(xEbCategorieDeviation: EbTtCategorieDeviation, d, row) {
					return xEbCategorieDeviation && xEbCategorieDeviation.ebTtCategorieDeviationNum
						? xEbCategorieDeviation.libelle
						: "";
				},
			},
			{
				field: "codeRaison",
				header: "Code Raison",
				translateCode: "SETTINGS.CODE_RAISON",
			},
			{
				field: "codeRaisonDescriptif",
				header: "Code Raison Descriptif",
				translateCode: "SETTINGS.CODE_RAISON_DESCRIPTIF",
			},
			{
				field: "dateCreationSys",
				header: "Date Creation Sys",
				translateCode: "SETTINGS.DATE_CREATION_SYS",
				render: function(data) {
					return data ? Statique.formatDateTimeStr(data) : "";
				}.bind(this),
			},
			{
				field: "dateMajSys",
				header: "Date Maj Sys",
				translateCode: "SETTINGS.DATE_MAJ_SYS",
				render: function(data) {
					return data ? Statique.formatDateTimeStr(data) : "";
				}.bind(this),
			},
		];

		this.dataInfos.dataLink = Statique.controllerMappingCodePsl + "/list-mapping-code-psl";
		this.dataInfos.dataKey = "ediMapPslNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.module = GenericTableScreen.MAPPING_CODE_PSL;
		this.dataInfos.dataType = EdiMapPsl;
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.actionColumFixed = false;
		this.dataInfos.showAdvancedSearchBtn = true;

		this.loadData();
	}
	async loadData() {
		this.isStatiquesLoaded = true;
	}

	displayAdd(isUpdate?: boolean) {
		if (isUpdate) {
			this.router.navigateByUrl(
				"app/user/settings/company/mapping-code-psl/" + this.dataObject.ediMapPslNum,
				{ state: { data: this.dataObject } }
			);
		} else {
			this.dataObject = new EdiMapPsl();
			this.router.navigateByUrl("app/user/settings/company/mapping-code-psl/new");
		}
		this.isAddObject = true;
	}
	displayEdit(dataObject: EdiMapPsl) {
		this.dataObject = Statique.cloneObject(dataObject, new EdiMapPsl());
		this.displayAdd(true);
	}

	deleteData(ediMapPsl: EdiMapPsl) {
		let $this = this;
		this.deleteMessage = null;
		this.deleteHeader = null;

		this.modalService.confirm(
			$this.translate.instant("GENERAL.CONFIRMATION"),
			$this.translate.instant("MAPPING_CODE_PSL.MAPPING_CODE_PSL_CONFIRM_DELETE"),
			function() {
				let dataObject = ediMapPsl;
				$this.mappingCodePslservice
					.deleteMappingCodePsl(dataObject.ediMapPslNum)
					.subscribe((data) => {
						$this.genericTable.refreshData();
					});
			},
			function() {},
			true
		);
	}

	search(searchInput: SearchCriteriaMappingCodePsl) {
		this.searchInput = searchInput;
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.MAPPING_CODE_PSL",
				path: "/app/user/settings/company/mapping-code-psl",
			},
		];
	}
}
