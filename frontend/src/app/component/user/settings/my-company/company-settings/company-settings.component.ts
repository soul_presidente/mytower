import { Component, OnInit } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbUser } from "@app/classes/user";
import { ServiceType, UserRole } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { EbEtablissement } from "@app/classes/etablissement";
import { EbCompagnie } from "@app/classes/compagnie";
import { UserService } from "@app/services/user.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthenticationService } from "@app/services/authentication.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { TypesDocumentService } from "@app/services/types-document.service";
import { ActivatedRoute } from "@angular/router";
import { SearchCriteria } from "@app/utils/searchCriteria";

@Component({
	selector: "app-company-settings",
	templateUrl: "./company-settings.component.html",
	styleUrls: ["./company-settings.component.scss"],
})
export class CompanySettingsComponent extends ConnectedUserComponent implements OnInit {
	public _newUser: EbUser = null;
	ServiceType = ServiceType;
	UserRole = UserRole;
	Statique = Statique;
	ebEtablissement: EbEtablissement = new EbEtablissement();
	ebCompagny: EbCompagnie = new EbCompagnie();

	constructor(
		protected userService?: UserService,
		protected etablissementService?: EtablissementService,
		protected ngbService?: NgbModal,
		protected authenticationService?: AuthenticationService,
		protected transportationPlanService?: TransportationPlanService,
		protected typesDocumentService?: TypesDocumentService,
		protected activatedRoute?: ActivatedRoute
	) {
		super();
	}

	ngOnInit() {
		this.getUserEtablissement();
		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.etablissementService.getEtablisement(searchCriteria).subscribe((data: EbEtablissement) => {
			if (data.categories) {
				data.categories = data.categories.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}

			this.ebEtablissement.constructorCopy(data);
			if (!this.ebEtablissement.ebCompagnie && this.ebCompagny)
				this.ebEtablissement.ebCompagnie = this.ebCompagny;
		});
	}
	getUserEtablissement() {
		this.userService.userInfo().subscribe((rep) => {
			this._newUser = rep;

			this.ebCompagny = JSON.parse(JSON.stringify(this._newUser.ebEtablissement.ebCompagnie));
			if (!this.ebEtablissement.ebCompagnie && this.ebEtablissement)
				this.ebEtablissement.ebCompagnie = JSON.parse(JSON.stringify(this.ebCompagny));
		});
	}
}
