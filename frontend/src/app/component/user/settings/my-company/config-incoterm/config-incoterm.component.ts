import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { GenericTableScreen, Modules, TypeImportance, ViewType } from "@app/utils/enumeration";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { EbCompagnie } from "@app/classes/compagnie";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { EbIncoterm } from "@app/classes/EbIncoterm";
import { ConfigIncotermService } from "@app/services/config-incoterm.service";

@Component({
	selector: "app-config-incoterm",
	templateUrl: "./config-incoterm.component.html",
	styleUrls: ["./config-incoterm.component.scss"],
})
export class ConfigIncotermComponent extends ConnectedUserComponent implements OnInit {
	Module = Modules;
	Statique = Statique;
	ViewType = ViewType;

	listIncoterms: Array<EbIncoterm> = new Array<EbIncoterm>();
	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	isStatiquesLoaded = false;
	isAddObject = false;
	dataObject: EbIncoterm = new EbIncoterm();
	backupEditObject: EbIncoterm;
	editingIndex: number;
	deleteMessage = "";
	deleteHeader = "";
	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;

	viewMode: number = 2;

	constructor(
		private translate?: TranslateService,
		protected modalService?: ModalService,
		protected configIncotermService?: ConfigIncotermService,
		protected headerService?: HeaderService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 5;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteria.connectedUserNum = this.userConnected.ebUserNum;

		this.dataInfos.cols = [
			{
				field: "libelle",
				header: "Designation",
				translateCode: "TRANSPORTATION_PLAN.ZONE_DESIGNATION",
			},
			{
				field: "code",
				header: "code",
				translateCode: "CONFIG_PSL_COMPANY.CODEALPHA",
			},
		];
		this.dataInfos.dataKey = "ebIncotermNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerIncoterm + "/list-incoterms";
		this.dataInfos.dataType = EbIncoterm;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = false;

		this.loadData();
	}

	async loadData() {
		this.isStatiquesLoaded = true;
	}

	diplayEdit(dataObject: EbIncoterm) {
		this.dataObject = Statique.cloneObject(dataObject, new EbIncoterm());

		this.onShowAddEditDisplay(dataObject);

		this.backupEditObject = Statique.cloneObject<EbIncoterm>(this.dataObject, new EbIncoterm());
		this.displayAdd(true);
	}

	onShowAddEditDisplay(dataObject: EbIncoterm) {}

	displayAdd(isUpdate?: boolean) {
		if (isUpdate) {
		} else this.dataObject = new EbIncoterm();

		this.onShowAddEditDisplay(this.dataObject);

		this.isAddObject = true;
	}

	backToListZone() {
		if (this.editingIndex != null) {
			Statique.cloneObject<EbIncoterm>(
				this.backupEditObject,
				this.listIncoterms[this.editingIndex]
			);
			this.editingIndex = null;
		}
		this.backupEditObject = null;
		this.isAddObject = false;
	}

	isFormValidated(): boolean {
		let isValid = this.dataObject.libelle && this.dataObject.libelle.length > 0;
		isValid = isValid && this.dataObject.code && this.dataObject.code.length > 0;
		return isValid;
	}

	editIncoterm() {
		if (!this.isFormValidated()) return;
		this.configIncotermService.updateIncoterm(this.dataObject).subscribe((data) => {
			this.isAddObject = false;
			this.editingIndex = null;
			this.backupEditObject = null;
			this.dataObject = null;
			this.genericTable.refreshData();
		});
	}

	addIncoterm() {
		if (!this.isFormValidated()) return;
		this.dataObject.xEbCompagnie = new EbCompagnie();
		this.dataObject.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.configIncotermService.updateIncoterm(this.dataObject).subscribe((data: EbTtSchemaPsl) => {
			this.isAddObject = false;
			this.dataObject = null;
			this.genericTable.refreshData();
		});
	}

	deleteData(incoterm: EbIncoterm) {
		let $this = this;
		this.deleteMessage = null;
		this.deleteHeader = null;

		this.modalService.confirm(
			$this.translate.instant("GENERAL.CONFIRMATION"),
			$this.translate.instant("INCOTERM.INCOTERM_CONFIRM_DELETE"),
			function() {
				let dataObject = incoterm;
				$this.configIncotermService.deleteIncoterm(dataObject.ebIncotermNum).subscribe((data) => {
					$this.genericTable.refreshData();
				});
			},
			function() {},
			true
		);
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "Incoterm",
			},
		];
	}
}
