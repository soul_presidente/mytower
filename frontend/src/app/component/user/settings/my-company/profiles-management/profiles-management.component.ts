import { Component, OnInit, ViewChild } from "@angular/core";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { TranslateService } from "@ngx-translate/core";
import { Statique } from "@app/utils/statique";
import { GenericTableScreen } from "@app/utils/enumeration";
import { EbEtablissement } from "@app/classes/etablissement";
import { EbUserProfile } from "@app/classes/userProfile";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { UserProfileService } from "@app/services/user-profile.service";
import { UserService } from "@app/services/user.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EtablissementService } from "@app/services/etablissement.service";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { MessageService } from "primeng/api";

@Component({
	selector: "app-profiles-management",
	templateUrl: "./profiles-management.component.html",
	styleUrls: ["./profiles-management.component.scss"],
})
export class ProfilesManagementComponent extends ConnectedUserComponent implements OnInit {
	// @Input()
	// ebEtablissement: EbEtablissement;

	ebEtablissement: EbEtablissement = new EbEtablissement();
	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	selectedProfile: EbUserProfile = null;
	// Generique Table
	datatableComponent = GenericTableScreen;
	title: String;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	detailsInfos: GenericTableInfos = new GenericTableInfos(this);
	displayEditProfile: boolean = false;
	isEditUser: boolean;

	constructor(
		protected translate?: TranslateService,
		private messageService?: MessageService,
		private userProfileService?: UserProfileService,
		private userService?: UserService,
		private etablissementService?: EtablissementService,
		protected headerService?: HeaderService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.title = "PROFILES";
		this.initializeData();

		document.addEventListener(
			"profile-action-event",
			function(event) {
				event.preventDefault();
				event.stopPropagation();
				this.addNewProfile();
			}.bind(this),
			false
		);

		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.etablissementService.getEtablisement(searchCriteria).subscribe((data: EbEtablissement) => {
			if (data.categories) {
				data.categories = data.categories.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}

			this.ebEtablissement.constructorCopy(data);
		});
	}

	translator(tab: any[]) {
		tab.forEach((it) => {
			this.translate.get(it.translateCode).subscribe((res) => {
				it["header"] = res;
			});
		});
	}

	addNewProfile() {
		this.displayEditProfile = true;
		this.selectedProfile = new EbUserProfile();
		this.isEditUser = false;
	}

	initializeData() {
		this.dataInfos.cols = [
			{ field: "nom", isCardCol: true, cardOrder: 1, translateCode: "SETTINGS.NAME" },
			{ field: "descriptif", isCardCol: false, cardOrder: 2, translateCode: "SETTINGS.DESCRIPTIF" },
		];

		if (this.isControlTower && this.isSuperAdmin) {
			this.dataInfos.cols.push({
				field: "ebCompagnie",
				cardOrder: 3,
				translateCode: "ORGANISATION.COMPAGNIE",
				render: function(data) {
					return data ? data.nom : "";
				}.bind(this),
			});
		}

		this.translator(this.dataInfos.cols);
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.dataKey = "ebUserProfileNum";
		this.dataInfos.paginator = true;

		// this.dataInfos.cardView = true;
		this.dataInfos.dataLink = Statique.controllerUserProfile + "/get-all";
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.showCollapsibleBtn = true;

		if (this.isSuperAdmin) {
			// add/edit only allowed for super admin users
			this.dataInfos.showAddNewProfileBtn = true;
			this.dataInfos.lineActions = [];
			this.dataInfos.lineActions.push({
				class: "btn btn-xs  font-size-10",
				icon: "fa fa-edit",
				title: "Edit",
				onClick: function(row: EbUserProfile) {
					this.editUserProfile(row);
				}.bind(this),
			});
			this.dataInfos.lineActions.push({
				class: "btn btn-xs  font-size-10",
				icon: "fa fa-trash",
				title: "Delete",
				onClick: function(row: EbUserProfile) {
					this.deleteUserProfile(row);
				}.bind(this),
			});
		}
	}

	deleteUserProfile(userProfile: EbUserProfile) {
		this.userProfileService.deleteUserProfile(userProfile.ebUserProfileNum).subscribe((res) => {
			this.displayEditProfile = false;
			if (res) {
				this.refreshData();
				this.tasterDisplay(true);
			} else {
				this.tasterDisplay(false);
			}
		});
	}
	editUserProfile(userProfile: EbUserProfile) {
		this.displayEditProfile = true;
		this.selectedProfile = userProfile;
		this.isEditUser = true;
	}
	changeStatus(status: boolean) {
		this.displayEditProfile = status;
	}
	tasterDisplay(event: any) {
		if (event == true) {
			this.messageService.add({
				severity: "success",
				summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
				detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
			});
		}
		if (event == false) {
			this.messageService.add({
				severity: "error",
				summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
				detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE"),
			});
		}
	}
	refreshData() {
		this.genericTable.refreshData();
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.PROFILES_MANAGEMENT",
			},
		];
	}
}
