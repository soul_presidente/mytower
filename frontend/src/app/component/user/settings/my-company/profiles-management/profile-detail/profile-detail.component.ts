import {
	Component,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
} from "@angular/core";
import { StatiqueService } from "@app/services/statique.service";
import { EcModule } from "@app/classes/EcModule";
import { EbEtablissement } from "@app/classes/etablissement";
import { EbCategorie } from "@app/classes/categorie";
import { Statique } from "@app/utils/statique";
import { EbLabel } from "@app/classes/label";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbUserProfile } from "@app/classes/userProfile";
import { UserProfileService } from "@app/services/user-profile.service";
import { ExUserProfileModule } from "@app/classes/accessRightProfile";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { TranslateService } from "@ngx-translate/core";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableScreen } from "@app/utils/enumeration";
import { Router } from "@angular/router";
import { EbCompagnie } from "@app/classes/compagnie";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";

@Component({
	selector: "app-profile-detail",
	templateUrl: "./profile-detail.component.html",
	styleUrls: [
		"./profile-detail.component.scss",
		"./../../../../../administrateur-company/administrateur-company.component.css",
	],
})
export class ProfileDetailComponent extends ConnectedUserComponent implements OnInit, OnChanges {
	@Output()
	EditEventChange = new EventEmitter<boolean>();
	@Output()
	refreshEvent = new EventEmitter<boolean>();
	@Output()
	toasterDisplay = new EventEmitter<boolean>();
	@Input()
	profileToBeUpdated: EbUserProfile;
	@Input()
	ebEtablissement: EbEtablissement;
	@Input()
	isEditUser: boolean;

	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	listCategories: Array<EbCategorie>;
	affectedCategories: Array<EbCategorie> = new Array<EbCategorie>();
	affectedCategoriesAndLabels: Array<EbCategorie> = new Array<EbCategorie>();

	listModule: EcModule[] = [];
	nameError: String = null;
	disableButton: boolean;
	oldName: String = null;
	title: String;
	searchCriteria: SearchCriteria;
	// Generique Table
	datatableComponent = GenericTableScreen;

	constructor(
		private userProfileService?: UserProfileService,
		protected statiqueService?: StatiqueService,
		private translate?: TranslateService,
		protected router?: Router
	) {
		super();
	}

	ngOnInit() {
		this.title = "USERS";
		this.initializeData();
	}
	initializeData() {
		this.dataInfos.cols = [
			{ field: "nom", isCardCol: true, cardOrder: 1, translateCode: "INSCRIPTION.NOM" },
			{ field: "prenom", isCardCol: false, cardOrder: 2, translateCode: "INSCRIPTION.PRENOM" },
		];
		this.translator(this.dataInfos.cols);
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.paginator = true;

		// this.dataInfos.cardView = true;
		this.dataInfos.dataLink = Statique.controllerUserProfile + "/get-users-by-profile";
		this.dataInfos.numberDatasPerPage = 10;
	}
	translator(tab: any[]) {
		tab.forEach((it) => {
			this.translate.get(it.translateCode).subscribe((res) => {
				it["header"] = res;
			});
		});
	}
	ngOnChanges(changes: SimpleChanges) {
		if (
			changes.profileToBeUpdated.previousValue == undefined ||
			(changes.profileToBeUpdated.previousValue != undefined &&
				changes.profileToBeUpdated.previousValue.ebUserProfileNum !=
					changes.profileToBeUpdated.currentValue.ebUserProfileNum)
		) {
			this.oldName = "";
			this.getListModule();
			this.listCategories = Statique.cloneObject(this.ebEtablissement.categories);
			this.listCategories.sort((a, b) => {
				if (a.ebCategorieNum > b.ebCategorieNum) return 1;
				if (a.ebCategorieNum < b.ebCategorieNum) return -1;
				return 0;
			});
			this.affectedCategories = new Array<EbCategorie>();
			this.affectedCategoriesAndLabels = new Array<EbCategorie>();
			if (this.profileToBeUpdated.ebUserProfileNum != null) {
				this.searchCriteria = new SearchCriteria();
				this.searchCriteria.withAccessRights = true;
				this.searchCriteria.ebUserProfileNum = this.profileToBeUpdated.ebUserProfileNum;
				this.userProfileService.getDetailUser(this.searchCriteria).subscribe((data) => {
					this.oldName = this.profileToBeUpdated.nom;
					this.profileToBeUpdated.listCategories = data.listCategories;
					this.profileToBeUpdated.listAccessRights = data.listAccessRights;
					this.profileToBeUpdated.listAccessRights.sort((a, b) => {
						if (a.ecModuleNum > b.ecModuleNum) return 1;
						if (a.ecModuleNum < b.ecModuleNum) return -1;
						return 0;
					});
					if (this.profileToBeUpdated.listCategories) {
						// categories/labels reellement affecté au user
						this.affectedCategoriesAndLabels = EbCategorie.getCopyOfListCategorie(
							this.profileToBeUpdated.listCategories
						);
						let newAffectedCategoriesAndLabels = new Array<EbCategorie>();
						// this.affectedCategories = new Array<EbCategorie>();
						this.affectedCategories = this.listCategories.filter((it) => {
							let cat = this.affectedCategoriesAndLabels.find((ct) => {
								return ct.ebCategorieNum == it.ebCategorieNum;
							});
							if (cat) {
								let newCat: EbCategorie = (function(nc): EbCategorie {
									return JSON.parse(JSON.stringify(nc));
								})(cat);
								newCat.labels = new Array<EbLabel>();
								newAffectedCategoriesAndLabels.push(newCat);
								return true;
							}
						});
						// this.listCategories: categories restantes (contenant tous les labels)
						this.listCategories = this.listCategories.filter((it) => {
							let cat = this.affectedCategoriesAndLabels.find((ct) => {
								return ct.ebCategorieNum == it.ebCategorieNum;
							});
							if (!cat) return true;
						});
						// this.affectedCategories: categories affectés avec les labels restants
						this.affectedCategories.forEach((category) => {
							let cat = this.affectedCategoriesAndLabels.find((ct) => {
								return ct.ebCategorieNum == category.ebCategorieNum;
							});

							category.labels = category.labels.filter((label) => {
								let lab = cat.labels.find((lb) => {
									return lb.ebLabelNum == label.ebLabelNum;
								});
								if (!lab) return true;
								else {
									let newCat = newAffectedCategoriesAndLabels.find(
										(it) => it.ebCategorieNum == cat.ebCategorieNum
									);
									let newLabel: EbLabel = (function(nl): EbLabel {
										return JSON.parse(JSON.stringify(nl));
									})(label);
									newCat.labels.push(newLabel);
								}
							});
						});

						this.affectedCategoriesAndLabels = newAffectedCategoriesAndLabels;
					}
				});
			}
		}
	}

	closePannelEditProfile(status: boolean) {
		// this.closeEditUser = status;
		this.EditEventChange.emit(status);
	}
	getListModule() {
		this.statiqueService.getListModule().subscribe((data) => {
			this.listModule = data;
			this.profileToBeUpdated.listAccessRights = new Array<ExUserProfileModule>();
			this.listModule.forEach((module) => {
				this.profileToBeUpdated.listAccessRights.push(new ExUserProfileModule(module.ecModuleNum));
			});
		});
	}
	addCategory(categorySelector) {
		if ($(categorySelector).find("option").length <= 0) return;

		// let selectedCategoryIndex = this.listCategories.findIndex(it => it.ebCategorieNum == this.selectedCategory.ebCategorieNum)
		let selectedCategoryIndex: number = +$(categorySelector).val();
		if (
			!this.listCategories ||
			!Statique.isDefined(selectedCategoryIndex) ||
			!this.listCategories[selectedCategoryIndex]
		)
			return;
		this.affectedCategories.push(this.listCategories[selectedCategoryIndex]);

		let cat = new EbCategorie();
		cat.ebCategorieNum = this.listCategories[selectedCategoryIndex].ebCategorieNum;
		cat.libelle = this.listCategories[selectedCategoryIndex].libelle;
		cat.labels = new Array<EbLabel>();
		this.affectedCategoriesAndLabels.push(cat);

		this.listCategories.splice(selectedCategoryIndex, 1);
	}
	getAffCatLabelIndex(affectedCategorieIndex) {
		if (!Statique.isDefined(affectedCategorieIndex) || affectedCategorieIndex < 0) return null;
		let index = this.affectedCategoriesAndLabels.findIndex(
			(it) => it.ebCategorieNum == this.affectedCategories[affectedCategorieIndex].ebCategorieNum
		);
		if (!Statique.isDefined(index) || index < 0) return null;
		return index;
	}
	deleteCategory(affectedCategorieIndex) {
		if (!Statique.isDefined(affectedCategorieIndex) || affectedCategorieIndex < 0) return;

		let index = this.affectedCategoriesAndLabels.findIndex(
			(it) => it.ebCategorieNum == this.affectedCategories[affectedCategorieIndex].ebCategorieNum
		);
		if (!Statique.isDefined(index) || index < 0) return;

		this.affectedCategoriesAndLabels[index].labels.forEach((it) => {
			this.affectedCategories[affectedCategorieIndex].labels.push(it);
		});
		this.listCategories.push(this.affectedCategories[affectedCategorieIndex]);
		this.affectedCategories.splice(affectedCategorieIndex, 1);
		this.affectedCategoriesAndLabels.splice(index, 1);
	}
	addLabel(labelSelector, affectedCategorieIndex) {
		let index = this.getAffCatLabelIndex(affectedCategorieIndex);

		if (
			index == null ||
			labelSelector.value < 0 ||
			!this.affectedCategories[affectedCategorieIndex].labels[labelSelector.value]
		)
			return;

		this.affectedCategoriesAndLabels[index].labels.push(
			this.affectedCategories[affectedCategorieIndex].labels[labelSelector.value]
		);
		this.affectedCategories[affectedCategorieIndex].labels.splice(labelSelector.value, 1);
	}

	deleteLabel(labelIndex, affectedCategorieIndex) {
		if (!Statique.isDefined(labelIndex) || labelIndex < 0) return;
		let index = this.getAffCatLabelIndex(affectedCategorieIndex);

		this.affectedCategories[affectedCategorieIndex].labels.push(
			this.affectedCategoriesAndLabels[index].labels[labelIndex]
		);
		this.affectedCategoriesAndLabels[index].labels.splice(index, 1);
	}
	closePannelEditUser(status: boolean) {
		this.EditEventChange.emit(status);
	}
	editUserProfile(event: any) {
		if (this.profileToBeUpdated.nom == null || this.profileToBeUpdated.nom.length == 0) {
			this.disableButton = true;
			this.nameError = "Name is required";
			return;
		}
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		let that = this;
		this.profileToBeUpdated.listCategories = this.affectedCategoriesAndLabels;
		this.profileToBeUpdated.ebCompagnie = new EbCompagnie();
		this.profileToBeUpdated.ebCompagnie.ebCompagnieNum = this.ebEtablissement.ebCompagnie.ebCompagnieNum;

		that.userProfileService.updateUserProfile(that.profileToBeUpdated).subscribe(
			(userProfile: EbUserProfile) => {
				requestProcessing.afterGetResponse(event);
				// that.listUserProfiles.push(userProfile);
				// this.genericTable.refreshData();
				this.refreshEvent.emit();
				that.closePannelEditUser(false);
				this.toasterDisplay.emit(true);
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				this.toasterDisplay.emit(false);
			}
		);
	}

	checkName(name) {
		this.nameError = null;
		if (name != null && name.length > 0 && this.oldName != name) {
			this.userProfileService.checkName(name).subscribe((data) => {
				if (data) {
					this.disableButton = true;
					this.nameError = null;
					this.translate.get("SETTINGS.PROFILE_NAME_USED").subscribe((res: string) => {
						this.nameError = res;
					});
				} else {
					this.disableButton = false;
				}
			});
		}
	}

	openAclSettings() {
		if (this.profileToBeUpdated.ebUserProfileNum != null) {
			this.router.navigate([
				"/app/user/settings/company/acl/profile/",
				this.profileToBeUpdated.ebUserProfileNum,
			]);
		}
	}
}
