import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { CompagnieService } from "@app/services/compagnie.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { EbCompagnie } from "@app/classes/compagnie";
import { DataRecoveryService } from "@app/services/data-recovery.service";
import { DataRecoveryType, UserRole } from "@app/utils/enumeration";
import { DataRecoveryParam } from "@app/classes/DataRecoveryParam";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { MessageService } from "primeng/api";
import { Statique } from "@app/utils/statique";

@Component({
	selector: "app-data-recovery",
	templateUrl: "./data-recovery.component.html",
	styleUrls: ["./data-recovery.component.css"],
})
export class DataRecoveryComponent extends ConnectedUserComponent implements OnInit {
	DataRecoveryType = DataRecoveryType;
	Statique = Statique;

	searchCriteria = new SearchCriteria();
	globalCheck: boolean = false;
	listEbCompagnie: Array<EbCompagnie> = new Array<EbCompagnie>();

	constructor(
		protected translate: TranslateService,
		protected compagnieService: CompagnieService,
		protected headerService: HeaderService,
		protected dataRecoveryService: DataRecoveryService,
		protected messageService: MessageService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);

		this.searchCriteria.pageNumber = null;
		this.searchCriteria.size = -1;
		this.searchCriteria.ecRoleNum = UserRole.CHARGEUR;

		this.compagnieService
			.getListCompagnie(this.searchCriteria)
			.subscribe((data: Array<EbCompagnie>) => {
				this.listEbCompagnie = data.filter((obj) => {
					if (obj.ebCompagnieNum) {
						obj["isChecked"] = false;
						return true;
					}
				});
			});
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.DATA_RECOVERY",
			},
		];
	}

	async companyRecoveryApply(type: number, companyId: number, event) {
		let dataForRecovery: DataRecoveryParam = { dataType: [type], compagnies: [companyId] };

		this.sendData(dataForRecovery, event);
	}

	companyRecoveryApplyByType(type: number, event) {
		let dataForRecovery: DataRecoveryParam = {
			dataType: [type],
			compagnies: this.listEbCompagnie.map((it) => it.ebCompagnieNum),
		};

		this.sendData(dataForRecovery, event);
	}

	companyRecoveryByCompanyApply(companyId: number, event) {
		let dataForRecovery: DataRecoveryParam = {
			dataType: Statique.DataRecoveryTypeList.map((it) => it.key),
			compagnies: [companyId],
		};

		this.sendData(dataForRecovery, event);
	}

	sendData(dataForRecovery: DataRecoveryParam, event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.dataRecoveryService.applyCompanyRecovery(dataForRecovery).subscribe(
			(res) => {
				requestProcessing.afterGetResponse(event);
				this.showSuccess();
			},
			(err) => {
				requestProcessing.afterGetResponse(event);
				this.showError();
			}
		);
	}

	checkUncheckAll() {
		this.listEbCompagnie.forEach((it) => (it["isChecked"] = this.globalCheck));
	}

	globalRecovery(event) {
		let listCompId: Array<number> = this.listEbCompagnie
			.filter((it) => it["isChecked"])
			.map((it) => it.ebCompagnieNum);
		let globalDataForRecovery: DataRecoveryParam = {
			compagnies: listCompId,
			dataType: Statique.DataRecoveryTypeList.map((it) => it.key),
		};

		this.sendData(globalDataForRecovery, event);
	}
	showSuccess() {
		let summary = "Success",
			detail = "Operation successfully completed!";

		this.translate.get("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY").subscribe((res: string) => {
			summary = res;
		});
		this.translate.get("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL").subscribe((res: string) => {
			detail = res;
		});
		this.messageService.add({ severity: "success", summary: summary, detail: detail });
	}

	showError() {
		let summary = "Error",
			detail = "Operation failed";

		this.translate.get("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY").subscribe((res: string) => {
			summary = res;
		});
		this.translate.get("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL").subscribe((res: string) => {
			detail = res;
		});

		this.messageService.add({
			severity: "error",
			summary: summary,
			detail: detail,
		});
	}

	get disableGlobalRecovery() {
		return this.listEbCompagnie.findIndex((it) => it["isChecked"]) == -1;
	}
}
