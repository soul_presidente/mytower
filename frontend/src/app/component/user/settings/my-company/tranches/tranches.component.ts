import { GenericTableScreen, Modules } from "@app/utils/enumeration";
import {
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	Renderer2,
	ViewChild,
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { Statique } from "@app/utils/statique";
import { EbPlTrancheDTO } from "@app/classes/trpl/EbPlTrancheDTO";
import { EbPlGrilleTransportDTO } from "@app/classes/trpl/EbPlGrilleTransportDTO";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";

@Component({
	selector: "app-tranches",
	templateUrl: "./tranches.component.html",
	styleUrls: ["./tranches.component.scss"],
})
export class TranchesComponent implements OnInit {
	Module = Modules;

	@Input()
	sharedDropDownZIndex: number;
	@Output()
	sharedDropDownZIndexEmit: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onTrancheChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	listTranche: Array<EbPlTrancheDTO>;
	listAllTranche: Array<EbPlTrancheDTO>;
	isAddTranche = false;
	tranche: EbPlTrancheDTO = new EbPlTrancheDTO();
	editingIndex: number;
	deleteMessage = "";
	deleteHeader = "";

	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	GenericTableScreen = GenericTableScreen;

	constructor(
		protected renderer: Renderer2,
		private translate?: TranslateService,
		protected modalService?: ModalService,
		protected transportationPlanService?: TransportationPlanService
	) {}

	ngOnInit() {
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 5;

		this.dataInfos.cols = [
			{
				field: "minimum",
				header: "Minimum",
				translateCode: "TRANSPORTATION_PLAN.TRANCHES_MINIMUM",
			},
			{
				field: "maximum",
				header: "Maximum",
				translateCode: "TRANSPORTATION_PLAN.TRANCHES_MAXIMUM",
			},
		];
		this.dataInfos.dataKey = "ebTrancheNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerTransportationPlan + "/list-tranche-table";
		this.dataInfos.dataType = EbPlTrancheDTO;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.transportationPlanService.getListTranche().subscribe((data) => {
			this.listAllTranche = data;
		});
	}

	diplayEditTranche(data: EbPlTrancheDTO) {
		this.tranche = Statique.cloneObject(data, new EbPlTrancheDTO());
		this.displayAddTranche(true);
	}

	displayAddTranche(isUpdate?: boolean) {
		if (isUpdate) {
		} else this.tranche = new EbPlTrancheDTO();
		this.isAddTranche = true;
	}

	backToListTranche() {
		this.isAddTranche = false;
	}
	editTranche() {
		this.transportationPlanService.saveTranche(this.tranche).subscribe((data) => {
			this.editingIndex = null;
			this.isAddTranche = false;
			this.genericTable.refreshData();
			let index = this.listAllTranche.findIndex((t) => {
				return t.ebTrancheNum == this.tranche.ebTrancheNum;
			});
			if (index > -1) this.listAllTranche[index] = this.tranche;
			this.onTrancheChanged.emit(this.listAllTranche);
		});
	}
	addTranche() {
		this.transportationPlanService.saveTranche(this.tranche).subscribe((data: EbPlTrancheDTO) => {
			this.isAddTranche = false;
			this.genericTable.refreshData();
			this.listAllTranche.push(data);
			this.onTrancheChanged.emit(this.listAllTranche);
		});
	}

	deleteTranche(data: EbPlTrancheDTO) {
		let $this = this;
		this.deleteMessage = null;
		this.deleteHeader = null;

		this.translate.get("TRANSPORTATION_PLAN.MESSAGE_DELETE_TRANCHE").subscribe((res: string) => {
			this.deleteMessage = res;
		});

		this.translate.get("TRANSPORTATION_PLAN.DELETE_TRANCHE").subscribe((res: string) => {
			this.deleteHeader = res;
		});
		this.modalService.confirm(
			this.deleteHeader,
			this.deleteMessage,
			function() {
				let tranche = data;
				$this.transportationPlanService.deleteTranche(tranche.ebTrancheNum).subscribe((data) => {
					$this.genericTable.refreshData();
					let index = $this.listAllTranche.findIndex((t) => {
						return t.ebTrancheNum == tranche.ebTrancheNum;
					});
					if (index > -1) $this.listAllTranche.splice(index, 1);
					$this.onTrancheChanged.emit($this.listAllTranche);
				});
			},
			function() {},
			true
		);
	}

	changeModel(form: any) {
		setTimeout(
			function() {
				this.checkFields(form);
			}.bind(this),
			10
		);
	}

	checkFields(form: any) {
		var findErrorMin = false;
		var findErrorMax = false;

		if (!this.tranche.minimum && this.tranche.minimum !== 0) findErrorMin = true;
		if (!this.tranche.maximum && this.tranche.maximum !== 0) findErrorMax = true;

		if (
			this.tranche.minimum &&
			this.tranche.maximum &&
			this.tranche.minimum >= this.tranche.maximum
		) {
			findErrorMin = true;
			findErrorMax = true;
		}

		this.listTranche &&
			this.listTranche.some((aTranche, index, array) => {
				if (
					aTranche.ebTrancheNum !== this.tranche.ebTrancheNum &&
					aTranche.maximum === this.tranche.maximum &&
					aTranche.minimum === this.tranche.minimum
				) {
					findErrorMin = true;
					findErrorMax = true;
					return true;
				}
			});

		if (findErrorMin) {
			form.controls["minimum"].setErrors("overlap", true);
		} else {
			form.controls["minimum"].setErrors(null);
		}

		if (findErrorMax) {
			form.controls["maximum"].setErrors("overlap", true);
		} else {
			form.controls["maximum"].setErrors(null);
		}
	}

	rerenderTable() {}

	onDataLoaded(data: Array<EbPlTrancheDTO>) {
		this.onTrancheChanged.emit(this.listAllTranche);
	}

	sharedDropDownZIndexChange(event) {
		this.sharedDropDownZIndexEmit.emit(event);
	}
}
