import {Component, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {GenericTableComponent} from "@app/shared/generic-table/generic-table.component";
import {GenericTableInfos} from "@app/shared/generic-table/generic-table-infos";
import {SearchCriteria} from "@app/utils/searchCriteria";
import {TranslateService} from "@ngx-translate/core";
import {MessageService} from "primeng/api";
import {ModalService} from "@app/shared/modal/modal.service";
import {HeaderService} from "@app/services/header.service";
import {ConnectedUserComponent} from "@app/shared/connectedUserComponent";
import {BreadcumbComponent, HeaderInfos} from "@app/shared/header/header-infos";
import {GenericTableScreen,Modules} from "@app/utils/enumeration";
import {Statique} from "@app/utils/statique";
import {RequestProcessing} from "@app/utils/requestProcessing";
import {EbCompagnie} from "@app/classes/compagnie";
import {Cost, CostCategorie} from "@app/classes/costCategorie";
import {PricingService} from "@app/services/pricing.service";
import {EbModeTransport} from "@app/classes/EbModeTransport";
import SingletonStatique from "@app/utils/SingletonStatique";
import {CostItemService} from "@app/services/cost-item.service";
import {EbTtSchemaPsl} from "@app/classes/EbTtSchemaPsl";
import {EbTypeFlux} from "@app/classes/EbTypeFlux";

@Component({
  selector: 'app-costs-items',
  templateUrl: './costs-items.component.html',
  styleUrls: ['./costs-items.component.scss']
})
export class CostsItemsComponent extends ConnectedUserComponent implements OnInit {
	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	searchCriteria: SearchCriteria = new SearchCriteria();
	isAddObject = false;
	isAddCostCategorie = false;
	dataObject: Cost = new Cost();
	backupEditObject: Cost;
	GenericTableScreen = GenericTableScreen;
	listCompanyCodes: Array<String>;
	codeDuplicated: boolean = false;
	module: number;
	selectedListCostCategorie: Array<CostCategorie> = new Array<CostCategorie>();
	listModeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();
	selectedCostCategorie: CostCategorie = new CostCategorie();
	newCostCategorie: CostCategorie = new CostCategorie();
	listCostCategorie: Array<CostCategorie> = new Array<CostCategorie>();
	selectedModesTransport: Array<number> = new Array<number>();


	constructor(
		protected translate: TranslateService,
		protected messageService: MessageService,
		protected modalService?: ModalService,
		protected headerService?: HeaderService,
		protected costItemService?: CostItemService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.module = Modules.COST_ITEM;
		this.refreshCostCategories();
		this.getListModeTransport();
	}

	ngAfterViewInit() {
		this.initDataTable();
	}

	async getListModeTransport() {
		this.listModeTransport = await SingletonStatique.getListModeTransport();
	}

	initDataTable() {
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 5;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteria.connectedUserNum = this.userConnected.ebUserNum;

		this.dataInfos.cols = [
			{
				field: "code",
				translateCode: "GENERAL.CODE",
			},
			{
				field: "libelle",
				translateCode: "GENERAL.LIBELLE",
			},
			{
				field: "actived",
				translateCode: "SETTINGS.DEFAULT_COST_ITEM",
				render: function(actived) {
					return actived
						? this.translate.instant("GENERAL.YES")
						: this.translate.instant("GENERAL.NO");
				}.bind(this),
			},
			{
				field: "listCostCategorie",
				translateCode: "SETTINGS.COST_CATEGORIE",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					let str = "<div>";
					if(data){
					data.forEach((it) => {
						if(it.libelle)
							str += `<div>${it.libelle}</div>`;
					});
					}
					str += "</div>";
					return str;
				}.bind(this),
			},
			{
				field: "listModeTransport",
				header: "Modes de transport",
				render: function(data) {
					let str = "";
					data &&
					this.listModeTransport.forEach((it) => {
						if (data.indexOf(":" + it.code + ":") >= 0) {
							if (str.length > 0) str += ", ";
							str += it.libelle;
						}
					});
					return str;
				}.bind(this),
			},
		];
		this.dataInfos.dataKey = "ebCostNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerCostItems + "/list-costs-items";
		this.dataInfos.dataType = Cost;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.showExportBtn = true;
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.COSTS_ITEMS",
			},
		];
	}

	displayAdd($event) {
		this.dataObject = new Cost();
		this.onShowAddEditDisplay(this.dataObject)
		this.dataObject.actived = true;
		this.isAddObject = true;
	}

	displayAddCostCategorieForm($event){
		this.newCostCategorie = new CostCategorie();
		this.isAddCostCategorie = true
	}

	diplayEdit(dataObject: Cost) {
		this.dataObject = Statique.cloneObject(dataObject, new Cost());
		this.onShowAddEditDisplay(this.dataObject)
		this.backupEditObject = Statique.cloneObject<Cost>(
			this.dataObject,
			new Cost()
		);
		this.isAddObject = true;
	}

	deleteData(cost: Cost) {
		let $this = this;

		this.modalService.confirm(
			$this.translate.instant("GENERAL.CONFIRMATION"),
			$this.translate.instant("SETTINGS.CONFIRM_DELETE_COST_ITEM"),
			function() {
				$this.costItemService.deleteCostItem(cost.ebCostNum).subscribe(
					(data) => {
						let desactived = data as boolean;
						if (desactived) $this.successfulOperation();
						else $this.echecOperation();
						$this.genericTable.refreshData();
					},
					(err) => $this.echecOperation()
				);
			},
			function() {},
			true
		);
	}

	addCostItem($event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);
		this.dataObject.xEbCompagnie = new EbCompagnie();
		this.dataObject.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.costItemService.addCostItem(this.dataObject).subscribe((res) => {
			this.isAddObject = false;
			this.dataObject.xEbCostCategorie = null;
			this.dataObject = null;
			this.selectedModesTransport = null;
			this.codeDuplicated = false;
			this.successfulOperation();
			requestProcessing.afterGetResponse(event);
		},
			(err) => {
				this.echecOperation();
				requestProcessing.afterGetResponse(event);
			});
	}

	editCostItem($event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);

		this.costItemService.updateCostItem(this.dataObject).subscribe((res) => {
				this.isAddObject = false;
				this.dataObject = null;
				this.selectedModesTransport = null;
				this.codeDuplicated = false;
				this.successfulOperation();
				requestProcessing.afterGetResponse(event);
			},
			(err) => {
				this.echecOperation();
				requestProcessing.afterGetResponse(event);
			});

	}

	onShowAddEditDisplay(dataObject: Cost) {
		this.selectedModesTransport = this.listModeTransport
			.filter((it) => {
				return (this.dataObject.listModeTransport || "").indexOf(":" + it.code + ":") >= 0;
			})
			.map((it) => it.code);

	}

	backToList() {
		this.isAddObject = false;
		this.isAddCostCategorie = false;
		this.dataObject = null;
	}

	isFormValidated(): boolean {

		let isValid = this.dataObject.libelle && this.dataObject.libelle.length > 0;
		isValid = isValid && this.dataObject.code && this.dataObject.code.length > 0;
		isValid = isValid && this.dataObject.listCostCategorie && this.dataObject.listCostCategorie.length > 0;
		isValid = isValid && this.dataObject.listModeTransport && this.dataObject.listModeTransport.length  > 0;
		return isValid && !this.codeDuplicated;
	}

	isAddCostCategoreFormValidated(): boolean {

		let isValid = this.newCostCategorie.libelle && this.newCostCategorie.libelle.length > 0;
		isValid = isValid && this.newCostCategorie.code && this.newCostCategorie.code.length > 0;
		return isValid && !this.codeDuplicated;
	}
c
	refreshCostCategories(){
		let compagnieNum: number
		compagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.costItemService.listCostCategorieByCompagnie(compagnieNum,false).subscribe((res) =>{
			this.listCostCategorie = res;
			console.log(res);
		});
	}

	setSelectedModesTransport() {
		this.dataObject.listModeTransport = "";
		this.selectedModesTransport &&
		this.selectedModesTransport.forEach((it) => {
			this.dataObject.listModeTransport += ":" + it + ":";
		});
	}

	addCostCategorie($event){
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);
		this.newCostCategorie.xEbCompagnie = new EbCompagnie();
		this.newCostCategorie.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.costItemService.addCostCategorie(this.newCostCategorie).subscribe((res) => {
				this.isAddCostCategorie = false;
				this.newCostCategorie = null;
				this.codeDuplicated = false;
				this.successfulOperation();
				this.refreshCostCategories();
				requestProcessing.afterGetResponse(event);
			},
			(err) => {
				this.echecOperation();
				requestProcessing.afterGetResponse(event);

			});

	}

	successfulOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	echecOperation() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}

	verifyEbCostCodeExistance() {
		if (this.dataObject && this.dataObject.code && this.dataObject.code.length > 0)
			this.costItemService
				.verifyEbCostCodeExistance(this.dataObject.code, this.userConnected.ebCompagnie.ebCompagnieNum)
				.subscribe((res) => {
					this.codeDuplicated = res as boolean;
				});
	}

}
