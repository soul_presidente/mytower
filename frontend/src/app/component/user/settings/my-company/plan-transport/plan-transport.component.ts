import { GenericTableScreen, Modules, ModeTransport } from "@app/utils/enumeration";
import {
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	Renderer2,
	ViewChild,
} from "@angular/core";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { Statique } from "@app/utils/statique";
import { EbPlTransportDTO } from "@app/classes/trpl/EbPlTransportDTO";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import SingletonStatique from "@app/utils/SingletonStatique";
import { BoolToYesNoPipe } from "@app/shared/pipes/boolToYesNo.pipe";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { EbPlGrilleTransportDTO } from "@app/classes/trpl/EbPlGrilleTransportDTO";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { TypeRequestService } from "@app/services/type-request.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbCompagnie } from "@app/classes/compagnie";
import { EbTypeGoods } from "@app/classes/EbTypeGoods";
import { TypeGoodsService } from "@app/services/type-goods.service";

@Component({
	selector: "app-plan-transport",
	templateUrl: "./plan-transport.component.html",
	styleUrls: ["./plan-transport.component.scss"],
})
export class PlanTransportComponent extends ConnectedUserComponent implements OnInit {
	Module = Modules;
	Statique = Statique;

	@Input()
	listZone: Array<EbPlZoneDTO>;
	@Input()
	listGrille: Array<EbPlGrilleTransportDTO>;
	@Input()
	sharedDropDownZIndex: number;
	@Output()
	sharedDropDownZIndexEmit: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onPlanChanged = new EventEmitter();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	listPlan: Array<EbPlTransportDTO>;
	typeDemande: any = Statique.typeDemande;
	listModeTransports: Array<any> = null;
	listTypesTransport: Array<any> = null;
	listAllTypeTransport: Array<any> = null;
	editingIndex: number;
	GenericTableScreen = GenericTableScreen;

	isAddPlan = false;
	dataTableConfig: DataTableConfig = new DataTableConfig();
	plan: EbPlTransportDTO = new EbPlTransportDTO();
	deleteMessage = "";
	deleteHeader = "";

	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	listTypeDemandeByCompagnie: Array<EbTypeRequestDTO>;
	listTypeGoods: Array<EbTypeGoods> = [];

	constructor(
		protected renderer: Renderer2,
		private boolToYesAndNo?: BoolToYesNoPipe,
		private translate?: TranslateService,
		protected modalService?: ModalService,
		protected transportationPlanService?: TransportationPlanService,
		private typeRequestService?: TypeRequestService,
		protected typeGoodsService?: TypeGoodsService
	) {
		super();
	}

	ngOnInit() {
		let criteria = new SearchCriteria();
		criteria.connectedUserNum = this.userConnected.ebUserNum;

		if (this.isControlTower) {
			this.typeGoodsService.listTypeGoodsContact().subscribe((res) => {
				this.listTypeGoods = res;
			});
			this.typeRequestService
				.listTypeRequestsContact()
				.subscribe((res: Array<EbTypeRequestDTO>) => {
					this.listTypeDemandeByCompagnie = res;
				});
		} else {
			this.typeGoodsService.listTypeGoods(this.userConnected.ebCompagnie.ebCompagnieNum)
				.subscribe((res) => {
				this.listTypeGoods = res;
			});
			this.typeRequestService
				.getListTypeRequests(criteria)
				.subscribe((res: Array<EbTypeRequestDTO>) => {
					this.listTypeDemandeByCompagnie = res;
				});
		}

		this.initPlanTransport();
	}

	async initPlanTransport() {
		this.listModeTransports = await SingletonStatique.getListModeTransport();
		this.listAllTypeTransport = await SingletonStatique.getListTypeTransport();

		this.dataInfos.cols = [
			{
				field: "reference",
				translateCode: "TRANSPORTATION_PLAN.PLAN_REFERENCE",
			},
			{
				field: "designation",
				translateCode: "TRANSPORTATION_PLAN.PLAN_DESIGNATION",
			},
			{
				field: "origine",
				translateCode: "TRANSPORTATION_PLAN.ORIGIN",
				render: (item: EbPlZoneDTO) => {
					return this.renderZoneName(item);
				},
			},
			{
				field: "destination",
				translateCode: "TRANSPORTATION_PLAN.DESTINATION",
				render: (item: EbPlZoneDTO) => {
					return this.renderZoneName(item);
				},
			},
			{
				field: "typeDemande",
				translateCode: "TRANSPORTATION_PLAN.SERVICE_LEVEL",
				//render: item => Statique.getTypeDemandeStringFromKey(item)
				render: (data) => {
					let typeDemande = "";
					if (data != null && this.listTypeDemandeByCompagnie != null) {
						this.listTypeDemandeByCompagnie.forEach((elem) => {
							if (elem.ebTypeRequestNum === data) typeDemande = this.renderTypeRequestName(elem);
						});
					}
					return typeDemande;
				},
			},
			{
				field: "modeTransport",
				translateCode: "TRANSPORTATION_PLAN.MODE_TRANSPORT",
				renderParam: this.listModeTransports,
				render: (item, listModeTransports) =>
					Statique.getModeTransportStringFromKey(item, listModeTransports),
			},
			{
				field: "typeTransport",
				translateCode: "REQUEST_OVERVIEW.CONSOLIDATION",
				renderParam: this.listAllTypeTransport,
				render: (item, listAllTypeTransport) =>
					Statique.getTypeTransportStringFromKey(item, listAllTypeTransport),
			},
			{
				field: "typeGoodsNum",
				translateCode: "TRANSPORTATION_PLAN.TYPE_OF_GOODS",
				renderParam: this.listTypeGoods,
				render: (item: number, listTypeGoods: Array<EbTypeGoods>) => {
					if (item == null || listTypeGoods == null) return "";
					let rValue = "";
					listTypeGoods.forEach((typeGoods) => {
						if (typeGoods.ebTypeGoodsNum === item) {
							rValue = this.renderTypeGoodsName(typeGoods);
						}
					});
					return rValue;
				},
			},
			{
				field: "isDangerous",
				translateCode: "TRANSPORTATION_PLAN.DANGEROUS",
				render: (item: boolean) => {
					if (item == null) return "";
					return this.translate.instant(this.boolToYesAndNo.transform(item));
				},
			},
			{
				field: "grilleTarif",
				translateCode: "TRANSPORTATION_PLAN.CARRIER",
				render: (item) => this.getGrilleTarifTransporteurDisplayString(item),
			},
			{
				field: "grilleTarif",
				translateCode: "TRANSPORTATION_PLAN.GRILLES",
				render: (item) => this.getGrilleTarifDisplayString(item),
			},
			{
				field: "transitTime",
				translateCode: "TRANSPORTATION_PLAN.PLAN_TRANSIT_TIME",
			},
			{ field: "comment", translateCode: "TRANSPORTATION_PLAN.PLAN_COMMENT" },
		];
		this.dataInfos.dataKey = "ebPlanTransportNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showCloneBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerTransportationPlan + "/list-plan-table";
		this.dataInfos.dataType = EbPlTransportDTO;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
	}

	async selectListTypeTransport(modeTransport: ModeTransport) {
		this.listTypesTransport = await SingletonStatique.getListTypeTransportByModeTransport(
			modeTransport
		);
	}
	setCloneMode(data: EbPlTransportDTO) {
		this.diplayEditPlan(data, true);
	}
	diplayEditPlan(data: EbPlTransportDTO, isclone: boolean = false) {
		this.plan = Statique.cloneObject(data, new EbPlTransportDTO());
		this.selectListTypeTransport(this.plan.modeTransport);
		if (isclone) {
			this.plan.reference = null;
			this.plan.ebPlanTransportNum = null;
		}
		this.displayAddPlan(true);
	}

	displayAddPlan(isUpdate?: boolean) {
		if (!isUpdate) {
			this.plan = new EbPlTransportDTO();
		}
		this.isAddPlan = true;
	}

	backToListPlan() {
		this.isAddPlan = false;
	}

	addPlan(isUpdate) {
		this.plan = Statique.cloneObject(this.plan, new EbPlTransportDTO());
		if (
			this.plan.grilleTarif &&
			this.plan.grilleTarif.costItems &&
			this.plan.grilleTarif.costItems.length > 0
		) {
			this.plan.grilleTarif.costItems.forEach((ci) => {
				if (ci.typeUnit) {
					ci.typeUnit.xEbCompagnie = new EbCompagnie();
					ci.typeUnit.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
				}
			});
		}
		this.transportationPlanService.updatePlan(this.plan).subscribe(
			function(data) {
				this.plan = null;
				this.editingIndex = null;
				this.genericTable.refreshData();
				this.isAddPlan = false;
				this.onPlanChanged.next();
			}.bind(this)
		);
	}

	deletePlan(data: EbPlTransportDTO) {
		let $this = this;
		this.deleteMessage = null;
		this.deleteHeader = null;

		this.translate.get("TRANSPORTATION_PLAN.MESSAGE_DELETE_PLAN").subscribe((res: string) => {
			this.deleteMessage = res;
		});

		this.translate.get("TRANSPORTATION_PLAN.HEADER_DELETE_PLAN").subscribe((res: string) => {
			this.deleteHeader = res;
		});
		this.modalService.confirm(
			this.deleteHeader,
			this.deleteMessage,
			function() {
				$this.transportationPlanService.deletePlan(data.ebPlanTransportNum).subscribe((data) => {
					if (data) {
						$this.genericTable.refreshData();
						$this.onPlanChanged.next();
					}
				});
			},
			function() {},
			true
		);
	}

	compareUser(user1: any, user2: any) {
		if (user1 && user2) {
			var user1id = user1.ebUserNum ? user1.ebUserNum : user1;
			var user2id = user2.ebUserNum ? user2.ebUserNum : user2;

			return user1id === user2id;
		}
		return false;
	}

	changeModel(form: any) {
		setTimeout(
			function() {
				this.checkFields(form);
			}.bind(this),
			10
		);
	}

	checkFields(form: any) {
		var findOverlap = false;

		this.listPlan &&
			this.listPlan.some((aPlan, index, array) => {
				if (
					aPlan.reference &&
					this.plan.reference &&
					aPlan.ebPlanTransportNum !== this.plan.ebPlanTransportNum
				) {
					if (this.plan.reference.trim().toLowerCase() === aPlan.reference.trim().toLowerCase()) {
						findOverlap = true;
						return true;
					}
				}
			});

		if (findOverlap) {
			form.controls["reference"].setErrors("overlap", true);
		} else if (this.plan.reference) {
			form.controls["reference"].setErrors(null);
		}
	}

	getGrilleTarifTransporteurDisplayString(grille?: EbPlGrilleTransportDTO): string {
		if (grille) {
			if (grille.transporteurNom || grille.transporteurPrenom)
				return (
					(grille.transporteurNom != null ? grille.transporteurNom : "") +
					" " +
					(grille.transporteurPrenom != null ? grille.transporteurPrenom : "")
				);
		}
		return "";
	}

	getGrilleTarifDisplayString(grille?: EbPlGrilleTransportDTO): string {
		if (grille) {
			if (grille.designation) {
				return grille.reference + " (" + grille.designation + ")";
			} else {
				return grille.reference;
			}
		} else {
			return "";
		}
	}

	rerenderTable() {}

	onDataLoaded(data: Array<EbPlTransportDTO>) {
		this.onPlanChanged.emit(data);
	}

	sharedDropDownZIndexChange(event) {
		this.sharedDropDownZIndexEmit.emit(event);
	}

	renderZoneName(zone: EbPlZoneDTO): string {
		if (zone == null) return "";
		if (this.isControlTower) {
			return zone.ref + " (" + zone.companyName + ")";
		}
		return Statique.getZoneDisplayString(zone);
	}

	renderTypeGoodsName(typeGoods: EbTypeGoods): string {
		if (typeGoods == null) return "";
		if (this.isControlTower) {
			return typeGoods.label + " (" + (typeGoods.xEbCompany ? typeGoods.xEbCompany.nom : "") + ")";
		}
		return typeGoods.label;
	}

	renderTypeRequestName(typeRequest: EbTypeRequestDTO) {
		if (typeRequest == null) return "";
		if (this.isControlTower) {
			return typeRequest.reference + " (" + typeRequest.xEbCompagnieNom + ")";
		}
		return typeRequest.reference;
	}
}
