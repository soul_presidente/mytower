import { Component, OnInit } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { ServiceType, UserRole } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { EbEtablissement } from "@app/classes/etablissement";
import { EbCompagnie } from "@app/classes/compagnie";
import { UserService } from "@app/services/user.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { AuthenticationService } from "@app/services/authentication.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { TypesDocumentService } from "@app/services/types-document.service";
import { ActivatedRoute } from "@angular/router";
import { EbUser } from "@app/classes/user";
import { SearchCriteria } from "@app/utils/searchCriteria";

@Component({
	selector: "app-user-settings",
	templateUrl: "./user-settings.component.html",
	styleUrls: ["./user-settings.component.scss"],
})
export class UserSettingsComponent extends ConnectedUserComponent implements OnInit {
	public _newUser: EbUser = null;
	ServiceType = ServiceType;
	UserRole = UserRole;
	Statique = Statique;
	ebEtablissement: EbEtablissement = new EbEtablissement();
	ebCompagny: EbCompagnie = new EbCompagnie();
	isDetails: Boolean = false;
	userToBeUpdated: EbUser = new EbUser();

	constructor(
		protected userService?: UserService,
		protected etablissementService?: EtablissementService,
		protected ngbService?: NgbModal,
		protected authenticationService?: AuthenticationService,
		protected transportationPlanService?: TransportationPlanService,
		protected typesDocumentService?: TypesDocumentService,
		protected activatedRoute?: ActivatedRoute
	) {
		super();
	}

	getUserEtablissement() {
		this.userService.userInfo().subscribe((rep) => {
			this._newUser = rep;

			this.ebCompagny = JSON.parse(JSON.stringify(this._newUser.ebEtablissement.ebCompagnie));
			if (!this.ebEtablissement.ebCompagnie && this.ebEtablissement)
				this.ebEtablissement.ebCompagnie = JSON.parse(JSON.stringify(this.ebCompagny));
		});
	}

	ngOnInit() {
		this.getUserEtablissement();

		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.etablissementService.getEtablisement(searchCriteria).subscribe((data: EbEtablissement) => {
			if (data.categories) {
				data.categories = data.categories.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}

			this.ebEtablissement.constructorCopy(data);
			if (!this.ebEtablissement.ebCompagnie && this.ebCompagny)
				this.ebEtablissement.ebCompagnie = this.ebCompagny;
		});
	}

	onDetails(val) {
		this.isDetails = val;
	}
	setUserTobeUpdated(val) {
		this.userToBeUpdated = val;
		console.log(this.userToBeUpdated.email);
	}
}
