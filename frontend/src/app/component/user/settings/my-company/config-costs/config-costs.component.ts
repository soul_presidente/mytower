import { Component, OnInit, Output, EventEmitter, ViewChild } from "@angular/core";
import { EbCompagnie } from "@app/classes/compagnie";
import { EbAdditionalCost } from "@app/classes/EbAdditionalCost";
import { AdditionalCostService } from "@app/services/additional-cost.service";
import { HeaderService } from "@app/services/header.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { BreadcumbComponent, HeaderInfos } from "@app/shared/header/header-infos";
import { ModalService } from "@app/shared/modal/modal.service";
import { GenericTableScreen, Modules } from "@app/utils/enumeration";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { Statique } from "@app/utils/statique";
import { TranslateService } from "@ngx-translate/core";
import { MessageService } from "primeng/api";

@Component({
	selector: "app-config-costs",
	templateUrl: "./config-costs.component.html",
	styleUrls: ["./config-costs.component.scss"],
})
export class ConfigCostsComponent extends ConnectedUserComponent implements OnInit {
	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	searchCriteria: SearchCriteria = new SearchCriteria();
	isAddObject = false;
	dataObject: EbAdditionalCost = new EbAdditionalCost();
	backupEditObject: EbAdditionalCost;
	GenericTableScreen = GenericTableScreen;
	listCompanyCodes: Array<String>;
	codeDuplicated: boolean = false;
	module: number;

	constructor(
		protected translate: TranslateService,
		protected additionalCostService: AdditionalCostService,
		protected messageService: MessageService,
		protected modalService?: ModalService,
		protected headerService?: HeaderService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.module = Modules.ADDITIONAL_COST;
	}

	ngAfterViewInit() {
		this.initDataTable();
	}

	initDataTable() {
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 5;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteria.connectedUserNum = this.userConnected.ebUserNum;

		this.dataInfos.cols = [
			{
				field: "code",
				translateCode: "GENERAL.CODE",
			},
			{
				field: "libelle",
				translateCode: "GENERAL.LIBELLE",
			},
			{
				field: "actived",
				translateCode: "BOUTON_ACTION.ACTIVE",
				render: function(actived) {
					return actived
						? this.translate.instant("GENERAL.YES")
						: this.translate.instant("GENERAL.NO");
				}.bind(this),
			},
		];
		this.dataInfos.dataKey = "ebAdditionalCostNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerAdditionalCosts + "/list-additional-costs";
		this.dataInfos.dataType = EbAdditionalCost;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.showExportBtn = true;
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.COSTS",
			},
		];
	}

	displayAdd($event) {
		this.dataObject = new EbAdditionalCost();
		this.dataObject.actived = true;
		this.isAddObject = true;
	}

	diplayEdit(dataObject: EbAdditionalCost) {
		this.dataObject = Statique.cloneObject(dataObject, new EbAdditionalCost());
		this.backupEditObject = Statique.cloneObject<EbAdditionalCost>(
			this.dataObject,
			new EbAdditionalCost()
		);
		this.isAddObject = true;
	}

	deleteData(cost: EbAdditionalCost) {
		let $this = this;

		this.modalService.confirm(
			$this.translate.instant("GENERAL.CONFIRMATION"),
			$this.translate.instant("ADDITIONAL_COST.ADDITIONAL_COST_CONFIRM_DELETE"),
			function() {
				$this.additionalCostService.desactivateAdditionalCost(cost.ebAdditionalCostNum).subscribe(
					(data) => {
						let desactived = data as boolean;
						if (desactived) $this.successfulOperation();
						else $this.echecOperation();
						$this.genericTable.refreshData();
					},
					(err) => $this.echecOperation()
				);
			},
			function() {},
			true
		);
	}

	addAdditionalCost($event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);

		this.dataObject.xEbCompagnie = new EbCompagnie();
		this.dataObject.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.additionalCostService.upsertAdditionalCost(this.dataObject).subscribe(
			(res) => {
				this.isAddObject = false;
				this.dataObject = null;
				this.codeDuplicated = false;
				this.successfulOperation();
				requestProcessing.afterGetResponse(event);
			},
			(err) => {
				this.echecOperation();
				requestProcessing.afterGetResponse(event);
			}
		);
	}

	editAdditionalCost($event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);
		this.additionalCostService.upsertAdditionalCost(this.dataObject).subscribe(
			(res) => {
				this.isAddObject = false;
				this.dataObject = null;
				this.codeDuplicated = false;
				this.successfulOperation();
				requestProcessing.afterGetResponse(event);
			},
			(err) => {
				this.echecOperation();
				requestProcessing.afterGetResponse(event);
			}
		);
	}

	backToList() {
		this.isAddObject = false;
		this.dataObject = null;
	}

	isFormValidated(): boolean {
		let isValid = this.dataObject.libelle && this.dataObject.libelle.length > 0;
		isValid = isValid && this.dataObject.code && this.dataObject.code.length > 0;
		return isValid && !this.codeDuplicated;
	}

	successfulOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	echecOperation() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}

	verifyCodeExistance() {
		if (this.dataObject && this.dataObject.code && this.dataObject.code.length > 0)
			this.additionalCostService
				.verifyCodeExistance(this.dataObject.code, this.userConnected.ebCompagnie.ebCompagnieNum)
				.subscribe((res) => {
					this.codeDuplicated = res as boolean;
				});
	}
}
