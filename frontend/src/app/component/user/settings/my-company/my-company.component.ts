import { EbUser } from "@app/classes/user";
import { EbEtablissement } from "@app/classes/etablissement";
import { UserService } from "@app/services/user.service";
import { SettingsComponent } from "../settings.component";
import { Component, OnInit, ViewChild, ViewContainerRef } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { DialogProperties } from "@app/utils/dialogProperties";
import { EtablissementService } from "@app/services/etablissement.service";
import { ModalComponent } from "@app/shared/modal/modal.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EbCompagnie } from "@app/classes/compagnie";
import { AuthenticationService } from "@app/services/authentication.service";
import { ServiceType, UserRole, Modules } from "@app/utils/enumeration";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { EbPlTrancheDTO } from "@app/classes/trpl/EbPlTrancheDTO";
import { EbPlTransportDTO } from "@app/classes/trpl/EbPlTransportDTO";
import { TypesDocumentService } from "@app/services/types-document.service";
import { TranchesComponent } from "./tranches/tranches.component";
import { PlanTransportComponent } from "./plan-transport/plan-transport.component";
import { ZoneComponent } from "./zone/zone.component";
import { GrilleTransportComponent } from "./grille-transport/grille-transport.component";
import { ParametrageMtcComponent } from "./parametrage-mtc/parametrage-mtc.component";
import { EbPlGrilleTransportDTO } from "@app/classes/trpl/EbPlGrilleTransportDTO";
import { ActivatedRoute } from "@angular/router";
import { HeaderService } from "@app/services/header.service";
import SingletonStatique from "@app/utils/SingletonStatique";

@Component({
	selector: "app-settings-my-company",
	templateUrl: "./my-company.component.html",
	styleUrls: ["./my-company.component.scss"],
})
export class _MyCompanyComponent extends SettingsComponent implements OnInit {
	public _newUser: EbUser = null;

	SingletonStatique = SingletonStatique;
	ServiceType = ServiceType;
	UserRole = UserRole;
	Statique = Statique;
	Modules = Modules;
	ebEtablissement: EbEtablissement = new EbEtablissement();
	ebCompagny: EbCompagnie = new EbCompagnie();

	dialogProperties: DialogProperties = new DialogProperties();
	isDetails: boolean = false;
	userToBeUpdated: EbUser = new EbUser();
	displayElement: boolean = true;

	public static SUBTABS = [
		"companyprofile",
		"users",
		"etablissement",
		"adresses",
		"references",
		"exchangerates",
		"documents",
		"shiptomatrix",
		"typeunit",
		"trpl",
		"dock",
		"contenant",
		"flags",
		"rules",
		"deviation",
	];
	selectedSubtab: string = _MyCompanyComponent.SUBTABS[0];

	// Transportation plan lists
	listZone: Array<EbPlZoneDTO> = new Array<EbPlZoneDTO>();
	listTranche: Array<EbPlTrancheDTO> = new Array<EbPlTrancheDTO>();
	listGrille = new Array<EbPlGrilleTransportDTO>();
	listTransporteur: Array<EbUser> = new Array<EbUser>();
	listPlan: Array<EbPlTransportDTO> = new Array<EbPlTransportDTO>();

	@ViewChild("trancheComponent", { static: false })
	tranchesComponent: TranchesComponent;
	@ViewChild(ZoneComponent, { static: false })
	zoneComponent: ZoneComponent;
	@ViewChild("planTransportComponent", { static: false })
	planTransportComponent: PlanTransportComponent;
	@ViewChild("grilleComponent", { static: false })
	grilleTransportComponent: GrilleTransportComponent;
	@ViewChild("parametrageMtcComponent", { static: false })
	parametrageMtcComponent: ParametrageMtcComponent;

	userlastAndFirstName: string;
	showMTCLinks: boolean = false;

	constructor(
		protected userService?: UserService,
		protected vcr?: ViewContainerRef,
		protected etablissementService?: EtablissementService,
		protected ngbService?: NgbModal,
		protected authenticationService?: AuthenticationService,
		protected transportationPlanService?: TransportationPlanService,
		protected typesDocumentService?: TypesDocumentService,
		public activatedRoute?: ActivatedRoute,
		protected headerService?: HeaderService
	) {
		super(authenticationService);
	}

	back() {
		this.displayElement = true;
	}

	close() {
		this.displayElement = false;
	}

	getUserEtablissement() {
		this.userService.userInfo().subscribe((rep) => {
			this._newUser = rep;

			this.ebCompagny = JSON.parse(JSON.stringify(this._newUser.ebEtablissement.ebCompagnie));
			if (!this.ebEtablissement.ebCompagnie && this.ebEtablissement)
				this.ebEtablissement.ebCompagnie = JSON.parse(JSON.stringify(this.ebCompagny));
		});
	}

	getIsMTCEnabled() {
		this.transportationPlanService.isMTCEnabled().subscribe((res) => {
			this.showMTCLinks = res as boolean;
		});
	}

	ngOnInit() {
		this.displayElement = true;
		if (this.activatedRoute) {
			this.activatedRoute.queryParams.subscribe((params) => {
				this.params = params;

				if (
					this.params["subtab"] &&
					_MyCompanyComponent.SUBTABS.filter((t) => t === this.params["subtab"])
				) {
					this.selectedSubtab = this.params["subtab"];
				}
			});
		}

		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.etablissementService.getEtablisement(searchCriteria).subscribe((data: EbEtablissement) => {
			if (data.categories) {
				data.categories = data.categories.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}

			this.ebEtablissement.constructorCopy(data);
			if (!this.ebEtablissement.ebCompagnie && this.ebCompagny)
				this.ebEtablissement.ebCompagnie = this.ebCompagny;
		});

		this.getUserEtablissement();
		this.getIsMTCEnabled();
	}

	open() {
		return this.ngbService.open(ModalComponent);
	}

	onDetails(val) {
		this.isDetails = val;
	}
	setUserTobeUpdated(val) {
		this.userToBeUpdated = val;
	}

	get isDefaultControlTower(): boolean {
		return (
			this.userConnected.role == ServiceType.CONTROL_TOWER &&
			this.userConnected.ebCompagnie.compagnieRole != UserRole.CHARGEUR
		);
	}
}
