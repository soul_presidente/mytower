import { Statique } from "./../../../../../utils/statique";
import { SearchCriteriaShipToMatrix } from "./../../../../../utils/SearchCriteriaShipToMatrix";
import { ShipToMatrixService } from "./../../../../../services/ship-to-matrix.service";
import { GenericTableScreen } from "./../../../../../utils/enumeration";
import { Component, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";

@Component({
	selector: "app-ship-to-matrix",
	templateUrl: "./ship-to-matrix.component.html",
	styleUrls: ["./ship-to-matrix.component.css"],
	providers: [ShipToMatrixService],
})
export class ShipToMatrixComponent implements OnInit {
	cols: any;
	datas: any;
	title: string = "Ship To Matrix";
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	searchInput: SearchCriteriaShipToMatrix = new SearchCriteriaShipToMatrix();
	datatableComponent = GenericTableScreen;
	Statique = Statique;

	constructor(private translate: TranslateService, protected headerService: HeaderService) {}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);

		this.dataInfos.cols = [
			{ field: "shipTo", translateCode: "SHIP_TO_MATRIX.SHIPTO" },
			{ field: "referentADV", translateCode: "SHIP_TO_MATRIX.REFERENT_ADV" },
			{ field: "ebGroupName", translateCode: "SHIP_TO_MATRIX.GROUP_NAME" },
			{ field: "etablissementName", translateCode: "SHIP_TO_MATRIX.ETABLISSEMENT_NAME" },
			{ field: "statut", translateCode: "SHIP_TO_MATRIX.STATUT" },
			{ field: "login", translateCode: "SHIP_TO_MATRIX.LOGIN" },
			{ field: "newUser", translateCode: "SHIP_TO_MATRIX.NEW_USER" },
		];
		this.translator(this.dataInfos.cols);
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.dataKey = "ebShipToMatrixNum";
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerShipToMatrix + "/list-shipto";
		this.dataInfos.numberDatasPerPage = 10;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
	}

	search(event: SearchCriteriaShipToMatrix) {
		this.searchInput = event;
	}

	translator(tab: any[]) {
		tab.forEach((it) => {
			this.translate.get(it.translateCode).subscribe((res) => {
				it["header"] = res;
			});
		});
	}
	private getBreadcrumbItems(isCreation: boolean = false): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.SHIP_TO_MATRIX",
			},
		];
	}
}
