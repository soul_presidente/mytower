import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { EbCategorie } from "@app/classes/categorie";
import { Statique } from "@app/utils/statique";
import { EbLabel } from "@app/classes/label";
import { ModalService } from "@app/shared/modal/modal.service";
import { GenericService } from "@app/services/generic.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import * as _ from "lodash";

@Component({
	selector: "app-categorie-label",
	templateUrl: "./categorie-label.component.html",
	styleUrls: ["./categorie-label.component.scss"],
})
export class CategorieLabelComponent implements OnInit {
	@Input()
	isEdit: boolean;
	@Input()
	showDrtUser: boolean = false;
	@Input()
	disableDrtUser: boolean = false;
	@Input()
	manageCategorie: boolean = false;

	@Input()
	listCategories: Array<EbCategorie>;
	@Input()
	affectedCategories: Array<EbCategorie>;
	@Input()
	affectedCategoriesAndLabels: Array<EbCategorie>;
	@Output()
	affectedCategoriesAndLabelsChange = new EventEmitter();

	sugestedCategorieLabels: Array<EbCategorie> = new Array<EbCategorie>();
	selectedLabel: EbLabel;

	constructor(
		private genericService: GenericService,
		private translate: TranslateService,
		private modalService: ModalService
	) {}

	ngOnInit() {
		this.sugestedCategorieLabels = this.affectedCategories;
	}

	filterLabels(event, index) {
		let criteria = new SearchCriteria();
		criteria.searchterm = event.query;

		criteria.ebCategorieNum = this.affectedCategories[index].ebCategorieNum;
		this.genericService
			.runAction(Statique.controllerCategorie + "/label/get-by-libelle", criteria)
			.subscribe((res) => {
				this.sugestedCategorieLabels[index].labels = _.differenceBy(res,this.affectedCategoriesAndLabels[index].labels,'libelle');
			});
	}

	// fait en jquery car des bugs ont été rencontré en travaillant avec ngModel, à revoir en utilisant (change)
	addCategory(categorySelector) {
		if ($(categorySelector).find("option").length <= 0) return;

		// let selectedCategoryIndex = this.listCategories.findIndex(it => it.ebCategorieNum == this.selectedCategory.ebCategorieNum)
		let selectedCategoryIndex: number = +$(categorySelector).val();
		this.addCategoryByIndex(selectedCategoryIndex);
	}

	addCategoryByIndex(index: number) {
		if (!this.listCategories || !Statique.isDefined(index) || !this.listCategories[index]) return;

		let newCat = Statique.cloneObject(this.listCategories[index], new EbCategorie());
		this.affectedCategories.push(newCat);

		let cat = Statique.cloneObject(newCat, new EbCategorie());
		cat.isAll = true;
		cat.labels = new Array<EbLabel>();
		this.affectedCategoriesAndLabels.push(cat);

		this.listCategories.splice(index, 1);
	}

	deleteCategory(affectedCategorieIndex) {
		if (!Statique.isDefined(affectedCategorieIndex) || affectedCategorieIndex < 0) return;

		let index = this.affectedCategoriesAndLabels.findIndex(
			(it) => it.ebCategorieNum == this.affectedCategories[affectedCategorieIndex].ebCategorieNum
		);
		if (!Statique.isDefined(index) || index < 0) return;

		this.affectedCategoriesAndLabels[index].labels.forEach((it) => {
			this.affectedCategories[affectedCategorieIndex].labels.push(it);
		});
		this.listCategories.push(this.affectedCategories[affectedCategorieIndex]);
		this.affectedCategories.splice(affectedCategorieIndex, 1);
		this.affectedCategoriesAndLabels.splice(index, 1);
	}

	addLabel(labelSelector, affectedCategorieIndex) {
		let index = this.getAffCatLabelIndex(affectedCategorieIndex);
		let indexLabel = this.getLabelIndexByLibelle(
			labelSelector.libelle,
			this.affectedCategories[affectedCategorieIndex].labels
		);

		if (
			index == null ||
			labelSelector.libelle < 0 ||
			!this.affectedCategories[affectedCategorieIndex].labels[indexLabel]
		)
			return;

		this.affectedCategoriesAndLabels[index].labels.push(
			this.affectedCategories[affectedCategorieIndex].labels[indexLabel]
		);
		this.affectedCategories[affectedCategorieIndex].labels.splice(indexLabel, 1);

		this.affectedCategories[affectedCategorieIndex].isAll = false;
		this.affectedCategoriesAndLabels[index].isAll = false;
		this.affectedCategoriesAndLabelsChange.emit(this.affectedCategoriesAndLabels);
	}

	getAffCatLabelIndex(affectedCategorieIndex) {
		if (!Statique.isDefined(affectedCategorieIndex) || affectedCategorieIndex < 0) return null;
		let index = this.affectedCategoriesAndLabels.findIndex(
			(it) => it.ebCategorieNum == this.affectedCategories[affectedCategorieIndex].ebCategorieNum
		);
		if (!Statique.isDefined(index) || index < 0) return null;
		return index;
	}

	getLabelIndexByLibelle(libelle: String, listLabel: Array<EbLabel>) {
		let index: number = -1;
		for (let i = 0; i < listLabel.length; i++) {
			if (libelle == listLabel[i].libelle) {
				index = i;
				break;
			}
		}
		return index;
	}

	deleteLabel(labelIndex, affectedCategorieIndex) {
		if (!Statique.isDefined(labelIndex) || labelIndex < 0) return;
		let index = this.getAffCatLabelIndex(affectedCategorieIndex);

		this.affectedCategories[affectedCategorieIndex].labels.push(
			this.affectedCategoriesAndLabels[index].labels[labelIndex]
		);
		this.affectedCategoriesAndLabels[index].labels.splice(labelIndex, 1);
		this.affectedCategoriesAndLabelsChange.emit(this.affectedCategoriesAndLabels);
	}

	emptyCategLabelsIfIsAllNotSelected(categorie : EbCategorie){
		if(!categorie.isAll){
			categorie.labels = [];
		}
	}
}
