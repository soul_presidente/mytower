import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableScreen, SavedFormIdentifier, ActionType } from "@app/utils/enumeration";
import { EbBoutonAction } from "@app/classes/ebBoutonAction";
import { StatiqueService } from "@app/services/statique.service";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { FlagSingleViewGenericCell } from "@app/shared/generic-cell/commons/flag-single-view.generic-cell";
import { BoutonActionService } from "@app/services/bouton-action.service";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";

@Component({
	selector: "app-bouton-action",
	templateUrl: "./bouton-action.component.html",
	styleUrls: ["./bouton-action.component.scss"],
})
export class BoutonActionComponent extends ConnectedUserComponent implements OnInit {
	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	listIcons: Array<any>;
	listIconsCode: Array<number> = new Array<number>();

	listBoutonAction: Array<any>;
	listBoutonActionNewTarget: Array<any>;
	listBoutonActionDashboardTarget: Array<any>;

	isAddMode: boolean = false;

	Statique = Statique;

	listButonAction: Array<EbBoutonAction> = new Array<EbBoutonAction>();
	ebBoutonAction: EbBoutonAction = new EbBoutonAction();
	backupEditBoutonAction: EbBoutonAction;
	editingIndex: number;
	deleteMessage = "";
	deleteHeader = "";
	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;
	listFilters: any;
	listMasques: any;

	constructor(
		protected savedFormsService: SavedFormsService,
		protected boutonActionService: BoutonActionService,
		protected statiqueService: StatiqueService,
		protected translate: TranslateService,
		protected modalService: ModalService
	) {
		super();
	}

	ngOnInit() {
		this.initStatique();

		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 10;

		this.dataInfos.cols = [
			{
				field: "type",
				header: "Type",
				translateCode: "BOUTON_ACTION.TYPE",
			},
			{
				field: "module",
				header: "Module",
				translateCode: "BOUTON_ACTION.MODULE",
			},
			{
				field: "libelle",
				header: "Libélé",
				translateCode: "BOUTON_ACTION.LIBELLE",
			},
			{
				field: "ordre",
				header: "Order",
				translateCode: "BOUTON_ACTION.ORDER",
			},
			{
				field: "color",
				header: "Color",
				translateCode: "BOUTON_ACTION.COLOR",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: (color: string) => {
					let html = "";

					html += '<div style="width: 100%; height: 25px; ';
					html += "border-radius: 4px; border-width: 2px; border-color: black; ";
					html += "background-color: " + color + ';"';
					html += "></div>";

					return html;
				},
			},
			{
				field: "icon",
				header: "Icon",
				translateCode: "BOUTON_ACTION.ICON",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: FlagSingleViewGenericCell,
				genericCellParams: new FlagSingleViewGenericCell.Params({
					fieldFlagIcon: "icon",
					fieldFlagColor: "color",
				}),
			},
			{
				field: "active",
				header: "Activated",
				translateCode: "BOUTON_ACTION.ACTIVE",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: (active: boolean) => {
					let html = "";
					if (active) html += '<strong style="color: green;">Active</strong>';
					else html += '<strong style="color: red;">Inactive</strong>';
					return html;
				},
			},
		];

		this.dataInfos.dataKey = "ebBoutonActionNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerBoutonAction + "/list-bouton-action";
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
	}

	initStatique() {
		this.statiqueService.getListFlagIcon().subscribe((res) => {
			this.listIcons = res;
			this.listIcons.forEach((i) => {
				this.listIconsCode.push(i.code);
			});
		});

		this.statiqueService.getListBoutonAction().subscribe((res) => {
			this.listBoutonAction = res;
		});

		this.statiqueService.getListBoutonActionNewTarget().subscribe((res) => {
			this.listBoutonActionNewTarget = res;
		});

		this.statiqueService.getListBoutonActionDashboardTarget().subscribe((res) => {
			this.listBoutonActionDashboardTarget = res;
		});

		let filtersParams = {
			ebCompNum: SavedFormIdentifier.SAVED_SEARCH,
			ebUserNum: this.userConnected ? this.userConnected.ebUserNum : null,
		};
		let masquesParams = {
			ebCompNum: SavedFormIdentifier.PRICING_MASK,
			ebUserNum: this.userConnected ? this.userConnected.ebUserNum : null,
		};

		this.savedFormsService
			.getSavedForms(filtersParams)
			.subscribe((res) => (this.listFilters = res));
		this.savedFormsService
			.getSavedForms(masquesParams)
			.subscribe((res) => (this.listMasques = res));
	}

	onSelected() {
		let action = "";
		if (this.ebBoutonAction.type == ActionType.FILTER) {
			action = JSON.stringify(
				this.findFilterByLiebelle(this.ebBoutonAction.module, this.listFilters)
			);
		} else if (this.ebBoutonAction.type == ActionType.MASQUE) {
			action = JSON.stringify(
				this.findFilterByLiebelle(this.ebBoutonAction.module, this.listMasques)
			);
		} else if (this.ebBoutonAction.type == ActionType.NEW) {
			action = this.findTargetByLibelle(this.ebBoutonAction.module, this.listBoutonActionNewTarget);
		} else if (this.ebBoutonAction.type == ActionType.DASHBOARD) {
			action = this.findTargetByLibelle(
				this.ebBoutonAction.module,
				this.listBoutonActionDashboardTarget
			);
		}
		this.ebBoutonAction.action = action;
	}

	findTargetByLibelle(label, list) {
		let target: any = null;
		list.forEach((elt) => {
			if (elt.libelle == label) target = elt.target;
		});
		return target;
	}

	findFilterByLiebelle(label, list) {
		let filter: any = null;
		list.forEach((elt) => {
			if (elt.formName == label) filter = elt;
		});
		return filter;
	}

	displayEditBoutonAction(ebBoutonAction: EbBoutonAction) {
		this.ebBoutonAction = ebBoutonAction;
		this.backupEditBoutonAction = EbBoutonAction.constructorCopy(this.ebBoutonAction);
		this.displayAddBoutonAction(true);
	}

	displayAddBoutonAction(isUpdate?: boolean) {
		if (isUpdate) {
		} else this.ebBoutonAction = new EbBoutonAction();
		this.isAddMode = true;
	}

	backToListBoutonAction() {
		if (this.editingIndex != null) {
			this.listButonAction[this.editingIndex].copy(this.backupEditBoutonAction);
			this.editingIndex = null;
		}
		this.backupEditBoutonAction = null;
		this.isAddMode = false;
	}
	editBoutonAction() {
		this.boutonActionService.addBoutonAction(this.ebBoutonAction).subscribe((data) => {
			this.isAddMode = false;
			this.editingIndex = null;
			this.backupEditBoutonAction = null;
			this.genericTable.refreshData();
			this.onDataChanged.next();
		});
	}
	addBoutonAction() {
		var self = this;
		if (this.ebBoutonAction.color == null) {
			this.ebBoutonAction.color = "#f01616"; //couleur rouge par defaut lorsque le user ne choisit aucune couleur
		}
		this.boutonActionService.addBoutonAction(this.ebBoutonAction).subscribe(function(data) {
			self.isAddMode = false;
			self.genericTable.refreshData();
			self.onDataChanged.next();
		});
	}

	deleteBoutonAction(ebBoutonAction: EbBoutonAction) {
		let $this = this;
		this.deleteMessage = null;
		this.deleteHeader = null;

		this.translate.get("BOUTON_ACTION.MESSAGE_DELETE_FLAG").subscribe((res: string) => {
			this.deleteMessage = res;
		});

		this.translate.get("BOUTON_ACTION.DELETE_FLAG").subscribe((res: string) => {
			this.deleteHeader = res;
		});
		this.modalService.confirm(
			this.deleteHeader,
			this.deleteMessage,
			function() {
				$this.boutonActionService
					.deleteBoutonAction(ebBoutonAction.ebBoutonActionNum)
					.subscribe((data) => {
						$this.genericTable.refreshData();
						$this.onDataChanged.next();
					});
			},
			function() {},
			true
		);
	}

	onDataLoaded(data: Array<EbBoutonAction>) {
		this.onDataChanged.emit(data);
	}
}
