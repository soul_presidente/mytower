import { Component, OnInit, EventEmitter, Input, Output, ViewChild } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { PrMtcTransporteur } from "@app/classes/PrMtcTransporteur";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { Statique } from "@app/utils/statique";
import { PrMtcTransporteurService } from "@app/services/pr-mtc-transporteur.service";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { CompagnieService } from "@app/services/compagnie.service";
import { EbCompagnie } from "@app/classes/compagnie";
import { UserRole } from "@app/utils/enumeration";
import { UserService } from "@app/services/user.service";
import { EbUser } from "@app/classes/user";
import { Router } from "@angular/router";
import { GenericTableScreen, Modules, TypeImportance, ViewType } from "@app/utils/enumeration";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";

@Component({
	selector: "app-parametrage-mtc",
	templateUrl: "./parametrage-mtc.component.html",
	styleUrls: ["./parametrage-mtc.component.scss"],
})
export class ParametrageMtcComponent extends ConnectedUserComponent implements OnInit {
	listCodeMtc: Array<PrMtcTransporteur> = new Array<PrMtcTransporteur>();
	allList: Array<PrMtcTransporteur> = new Array<PrMtcTransporteur>();
	userList: Array<EbUser> = new Array<EbUser>();
	ebCompagniesList: Object = new Array<Object>();
	selectedCompany: EbCompagnie = new EbCompagnie();

	GenericTableScreen = GenericTableScreen;

	Statique = Statique;

	backupMtcTransporteur: PrMtcTransporteur;
	editingIndex: number;

	@Input()
	sharedDropDownZIndex: number;
	@Output()
	sharedDropDownZIndexEmit: EventEmitter<any> = new EventEmitter<any>();

	prMtcTransp: PrMtcTransporteur = new PrMtcTransporteur();
	isAddMtcTransporteur = false;

	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	backupEditMtcTransp: PrMtcTransporteur;

	deleteMessage = "";
	deleteHeader = "";
	isEditMode = false;

	constructor(
		protected prMtcTransporteurService?: PrMtcTransporteurService,
		private translate?: TranslateService,
		protected compagnieService?: CompagnieService,
		protected modalService?: ModalService,
		protected userService?: UserService,
		protected router?: Router,
		protected headerService?: HeaderService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);

		this.initDataTable();

		this.getMtcParametreList();

		this.compagnieService.getListCompagnie(this.searchCriteria).subscribe((data) => {
			this.ebCompagniesList = data;
		});
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "PARAMETRAGE_MTC.CUSTOM_MTC",
			},
		];
	}

	initDataTable() {
		this.searchCriteria.pageNumber = 0;
		this.searchCriteria.size = 5;
		this.searchCriteria.ecRoleNum = UserRole.PRESTATAIRE;
		this.searchCriteria.roleTransporteur = true;
		this.searchCriteria.activated = true;

		this.dataInfos.dataKey = "prMtcTransporteurNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerPrGlobal + "/transporteur";
		this.dataInfos.dataType = PrMtcTransporteur;
		this.dataInfos.numberDatasPerPage = 5;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;

		this.dataInfos.cols = [
			{
				field: "xEbCompagnieTransporteur",
				translateCode: "PARAMETRAGE_MTC.COMPANY",
				render: (company: EbCompagnie) => {
					return this.renderTransporteurName(company);
				},
			},
			{
				field: "codeConfigurationMtc",
				header: "Code MTC",
				translateCode: "PARAMETRAGE_MTC.CODE_MTC",
			},
			{
				field: "xEbUser",
				translateCode: "PARAMETRAGE_MTC.USER",
				render: (user: EbUser) => {
					return this.renderUserName(user);
				},
			},
			{
				field: "codeConfigurationEDI",
				header: "Code edi",
				translateCode: "PARAMETRAGE_MTC.CODE_EDI",
			},
		];
		this.dataInfos.selectedCols = this.dataInfos.cols;
	}

	changeModel(form: any) {
		setTimeout(
			function() {
				this.checkFields(form);
			}.bind(this),
			10
		);
	}

	checkFields(form: any) {
		let findOverlap = false;

		this.listCodeMtc &&
			this.listCodeMtc.some((aMtcTransp, index, array) => {
				if (
					aMtcTransp.ref &&
					this.prMtcTransp.ref &&
					aMtcTransp.prMtcTransporteurNum !== this.prMtcTransp.prMtcTransporteurNum
				) {
					if (this.prMtcTransp.ref.trim().toLowerCase() === aMtcTransp.ref.trim().toLowerCase()) {
						findOverlap = true;
						return true;
					}
				}
			});

		if (findOverlap) {
			form.controls["reference"].setErrors("overlap", true);
		} else if (this.prMtcTransp.ref) {
			form.controls["reference"].setErrors(null);
		}
	}

	async displayAddMtcTransporteur(isUpdate?: boolean) {
		if (!isUpdate) {
			this.prMtcTransp = new PrMtcTransporteur();
			this.isAddMtcTransporteur = true;
		} else {
			if (
				this.prMtcTransp.xEbCompagnieTransporteur != null &&
				this.prMtcTransp.xEbCompagnieTransporteur.ebCompagnieNum != null
			) {
				let searchCriteria = new SearchCriteria();
				searchCriteria.ebCompagnieNum = this.prMtcTransp.xEbCompagnieTransporteur.ebCompagnieNum;
				this.userService.getListUser(searchCriteria).subscribe((data) => {
					this.userList = data;
					this.isAddMtcTransporteur = true;
					this.isEditMode = true;
				});
			}
		}
	}

	displayEditMtcTransporteur(prMtc: PrMtcTransporteur) {
		this.prMtcTransp = prMtc;
		this.backupEditMtcTransp = Statique.cloneObject<PrMtcTransporteur>(
			this.prMtcTransp,
			new PrMtcTransporteur()
		);
		this.displayAddMtcTransporteur(true);
	}

	deleteMtcTransporteur(prMtc: PrMtcTransporteur) {
		let $this = this;
		this.deleteMessage = null;
		this.deleteHeader = null;

		this.translate
			.get("PARAMETRAGE_MTC.MESSAGE_DELETE_MTC_TRANSPORTER")
			.subscribe((res: string) => {
				this.deleteMessage = res;
			});

		this.translate.get("PARAMETRAGE_MTC.DELETE_MTC_TRANSPORTER").subscribe((res: string) => {
			this.deleteHeader = res;
		});
		this.modalService.confirm(
			this.deleteHeader,
			this.deleteMessage,
			function() {
				let prMtcTransporteurTemp = prMtc;
				$this.prMtcTransporteurService
					.deleteMtcTransporteur(prMtcTransporteurTemp.prMtcTransporteurNum)
					.subscribe((data) => {
						$this.genericTable.refreshData();
						// let curUrl = $this.router.url;
						// $this.router.navigateByUrl(curUrl);
						// $this.getMtcParametreList();
						let index = $this.allList.findIndex((z) => {
							return z.prMtcTransporteurNum == prMtcTransporteurTemp.prMtcTransporteurNum;
						});
						if (index > -1) $this.allList.splice(index, 1);
						$this.onDataChanged.emit($this.allList);
					});
			},
			function() {},
			true
		);
	}

	addMtcTransporteur() {
		this.prMtcTransporteurService
			.updateMtcTransporteur(this.prMtcTransp)
			.subscribe((data: PrMtcTransporteur) => {
				this.isAddMtcTransporteur = false;
				this.isEditMode = false;
				this.genericTable.refreshData();
				// this.getMtcParametreList();

				let curUrl = this.router.url;
				this.router.navigateByUrl(curUrl);
				this.allList.push(data);
				this.onDataChanged.emit(this.allList);
			});
	}

	editMtcTransporteur() {
		if (!this.prMtcTransp.xEbUser || !this.prMtcTransp.xEbUser.ebUserNum) return;
		this.prMtcTransp.xEbUser = EbUser.createUser(
			null,
			this.prMtcTransp.xEbUser.ebUserNum,
			this.prMtcTransp.xEbUser.email,
			this.prMtcTransp.xEbUser.nom
		);

		this.prMtcTransporteurService.updateMtcTransporteur(this.prMtcTransp).subscribe((data) => {
			this.isAddMtcTransporteur = false;
			this.isEditMode = false;
			this.editingIndex = null;
			this.backupMtcTransporteur = null;
			this.genericTable.refreshData();
			let index = this.allList.findIndex((z) => {
				return z.prMtcTransporteurNum == this.prMtcTransp.prMtcTransporteurNum;
			});
			if (index > -1) this.allList[index] = this.prMtcTransp;
			this.onDataChanged.emit(this.allList);
		});
	}

	backToListMtcTransporteur() {
		if (this.editingIndex != null) {
			Statique.cloneObject<PrMtcTransporteur>(
				this.backupMtcTransporteur,
				this.listCodeMtc[this.editingIndex]
			);
			this.editingIndex = null;
		}
		this.backupMtcTransporteur = null;
		this.isAddMtcTransporteur = false;
		this.isEditMode = false;
		this.genericTable.refreshData();
	}

	onDataLoaded(data: Array<PrMtcTransporteur>) {
		this.onDataChanged.emit(this.allList);
	}

	sharedDropDownZIndexChange(event) {
		this.sharedDropDownZIndexEmit.emit(event);
	}

	changecompany(company: EbCompagnie) {
		this.selectedCompany = company;
		if (this.selectedCompany) {
			// this.searchCriteria.ebCompagnieNum = this.selectedCompany.ebCompagnieNum;
			this.getUserList();
		}
	}

	getUserList(): void {
		let searchCriteria = new SearchCriteria();
		searchCriteria.ebCompagnieNum = this.selectedCompany.ebCompagnieNum;
		this.userService.getListUser(searchCriteria).subscribe((data) => {
			this.userList = data;
		});
	}

	getMtcParametreList(): void {
		this.prMtcTransporteurService.ListPrMtcTransporteur(this.searchCriteria).subscribe((data) => {
			this.allList = data;
		});
	}

	renderTransporteurName(company: EbCompagnie): string {
		if (company == null) return "";
		if (this.isControlTower) {
			return company.code + " - " + company.nom;
		}
		return company.code;
	}

	renderUserName(user: EbUser): string {
		if (user == null) return "";
		if (this.isControlTower) {
			return user.prenom + " " + user.nom;
		}
		return user.username;
	}
}
