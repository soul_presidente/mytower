import {Component, Input, OnInit} from '@angular/core';
import {MtcMappingParamService} from "@app/services/mtc-mapping-param.service";
import {PrMtcMappingParam} from "@app/classes/mtc/pr-mtc-mapping-param";

@Component({
	selector: 'app-mapping-attribute',
	templateUrl: './mapping-attribute.component.html',
	styleUrls: ['./mapping-attribute.component.scss']
})
export class MappingAttributeComponent implements OnInit {
	@Input()
	configCode: string
	params: PrMtcMappingParam[] = [];
	modalLexiqueVisible = true;

	constructor(private mappingParamService: MtcMappingParamService) {
	}

	ngOnInit(): void {
		this.initDataTable();
	}

	initDataTable() {
		if (this.configCode) {
			this.mappingParamService.getListMappingParamByConfigCode(this.configCode)
				.subscribe(params =>
					this.params = [...params]
				);
		}

	}

	save() {
		this.mappingParamService.save(this.params)
			.subscribe(params => this.params = [...params])
	}
}
