import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from "@app/utils/searchCriteria";
import {
	GenericTableScreen,
	Modules,
	TypeImportance,
	ViewType,
	TrackingLevel,
} from "@app/utils/enumeration";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import SingletonStatique from "@app/utils/SingletonStatique";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbEtablissement } from "@app/classes/etablissement";
import { EtablissementService } from "@app/services/etablissement.service";
import { ConfigPslCompanyService } from "@app/services/config-psl-company.service";
import { EbTtCompanyPsl } from "@app/classes/ttCompanyPsl";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { EbCompagnie } from "@app/classes/compagnie";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { EbTypeFlux } from "@app/classes/EbTypeFlux";
import { EbIncoterm } from "@app/classes/EbIncoterm";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { ConfigTypeFluxService } from "@app/services/config-type-flux.service";
import { ConfigIncotermService } from "@app/services/config-incoterm.service";
import { TypesDocumentService } from "@app/services/types-document.service";

@Component({
	selector: "app-config-type-flux",
	templateUrl: "./config-type-flux.component.html",
	styleUrls: ["./config-type-flux.component.scss"],
})
export class ConfigTypeFluxComponent extends ConnectedUserComponent implements OnInit {
	Module = Modules;
	Statique = Statique;
	ViewType = ViewType;

	listEbTypeFlux: Array<EbTypeFlux> = new Array<EbTypeFlux>();
	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	isStatiquesLoaded = false;
	isAddObject = false;
	dataObject: EbTypeFlux = new EbTypeFlux();
	backupEditObject: EbTypeFlux;
	editingIndex: number;
	deleteMessage = "";
	deleteHeader = "";
	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;

	listTrackingLevel: { key: number; label: string }[] = [];
	listModeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();
	listIncoterms: Array<EbIncoterm> = new Array<EbIncoterm>();
	listEbTtSchemaPsl: Array<EbTtSchemaPsl> = new Array<EbTtSchemaPsl>();
	listEbTypeDocuments: Array<EbTypeDocuments> = new Array<EbTypeDocuments>();

	selectedModesTransport: Array<number> = new Array<number>();
	selectedListIncoterms: Array<EbIncoterm> = new Array<EbIncoterm>();
	selectedListEbTtSchemaPsl: Array<EbTtSchemaPsl> = new Array<EbTtSchemaPsl>();
	selectedListEbTypeDocuments: Array<EbTypeDocuments> = new Array<EbTypeDocuments>();

	searchCriteriaEtablisement: SearchCriteria = new SearchCriteria();

	viewMode: number = 2;

	selectedPsl: EbTtCompanyPsl;

	constructor(
		private translate?: TranslateService,
		protected modalService?: ModalService,
		protected etablissementService?: EtablissementService,
		protected configTypeFluxService?: ConfigTypeFluxService,
		protected configIncoterms?: ConfigIncotermService,
		protected configEbTtSchemalPsl?: ConfigPslCompanyService,
		protected typesDocumentService?: TypesDocumentService,
		protected headerService?: HeaderService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);

		this.loadData();

		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 5;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteria.connectedUserNum = this.userConnected.ebUserNum;

		this.dataInfos.cols = [
			{
				field: "designation",
				header: "Designation",
				translateCode: "TRANSPORTATION_PLAN.ZONE_DESIGNATION",
			},
			{
				field: "code",
				header: "Code",
				translateCode: "TYPE_FLUX.CODE",
			},
			{
				field: "trackingLevel",
				header: "tracking Level",
				translateCode: "TYPE_FLUX.TRACKING_LEVEL",
				render: function(data: number) {
					let str = "";
					if (data != null) {
						str = this.translate.instant("TYPE_FLUX." + TrackingLevel[data]);
					}
					return str;
				}.bind(this),
			},
			{
				field: "listModeTransport",
				header: "Modes de transport",
				render: function(data) {
					let str = "";
					data &&
						this.listModeTransport.forEach((it) => {
							if (data.indexOf(":" + it.code + ":") >= 0) {
								if (str.length > 0) str += ", ";
								str += it.libelle;
							}
						});
					return str;
				}.bind(this),
			},
			{
				field: "listEbIncoterm",
				header: "Incoterms",
				render: function(data) {
					let str = "";
					this.listIncoterms &&
						this.listIncoterms.forEach((it) => {
							if (data.indexOf(":" + it.ebIncotermNum + ":") >= 0) {
								if (str.length > 0) str += ", ";
								str += it.libelle;
							}
						});

					return str;
				}.bind(this),
			},
			// je mets ce bout de code en commentaire parce que il sera activer pour la prochaine MEP
			// MTG-2076
			/* 	{
				field: "listEbTypeDocuments",
				header: "Type de documents",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data: string) {
					let str = "<div>";
					this.listEbTypeDocuments &&
						this.listEbTypeDocuments.forEach((it) => {
							if (data && data.indexOf(":" + it.ebTypeDocumentsNum + ":") >= 0) {
								str += `<div>${it.nom}</div>`;
							}
						});
					str += "</div>";
					return str;
				}.bind(this),
			}, */
			{
				field: "listEbTtSchemaPsl",
				header: "Schema Psl",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data: string) {
					let str = "<div>";
					this.listEbTtSchemaPsl &&
						this.listEbTtSchemaPsl.forEach((it) => {
							if (data && data.indexOf(":" + it.ebTtSchemaPslNum + ":") >= 0) {
								str += `<div>${it.designation}</div>`;
							}
						});
					str += "</div>";
					return str;
				}.bind(this),
			},
		];
		this.dataInfos.dataKey = "ebTypeFluxNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerTypeFlux + "/list-type-flux";
		this.dataInfos.dataType = EbTypeFlux;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = false;
	}

	async loadData() {
		let criteria = new SearchCriteria();
		criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		criteria.includeGlobalValues = true;

		this.getListTrackingLevelByEnum();
		this.listModeTransport = await SingletonStatique.getListModeTransport();
		this.listIncoterms = await this.configIncoterms.getListIncotermsPromise(criteria);
		this.listEbTtSchemaPsl = await this.configEbTtSchemalPsl.getListSchemaPslPromise(criteria);
		this.listEbTypeDocuments = await this.typesDocumentService.getListTypeDocPromise(criteria);

		this.isStatiquesLoaded = true;
	}

	getListTrackingLevelByEnum() {
		for (var n in TrackingLevel) {
			if (typeof TrackingLevel[n] === "number") {
				this.listTrackingLevel.push({ key: <any>TrackingLevel[n], label: "TYPE_FLUX." + n });
			}
		}
	}

	diplayEdit(dataObject: EbTypeFlux) {
		this.dataObject = Statique.cloneObject(dataObject, new EbTypeFlux());
		this.onShowAddEditDisplay(dataObject);
		this.backupEditObject = Statique.cloneObject<EbTypeFlux>(this.dataObject, new EbTypeFlux());
		this.displayAdd(true);
	}

	onShowAddEditDisplay(dataObject: EbTypeFlux) {
		this.selectedModesTransport = this.listModeTransport
			.filter((it) => {
				return (this.dataObject.listModeTransport || "").indexOf(":" + it.code + ":") >= 0;
			})
			.map((it) => it.code);

		this.selectedListEbTtSchemaPsl = this.listEbTtSchemaPsl.filter((it) => {
			return (
				(this.dataObject.listEbTtSchemaPsl || "").indexOf(":" + it.ebTtSchemaPslNum + ":") >= 0
			);
		});
		this.selectedListEbTypeDocuments = this.listEbTypeDocuments.filter((it) => {
			return (
				(this.dataObject.listEbTypeDocuments || "").indexOf(":" + it.ebTypeDocumentsNum + ":") >= 0
			);
		});
		this.selectedListIncoterms = this.listIncoterms.filter((it) => {
			return (this.dataObject.listEbIncoterm || "").indexOf(":" + it.ebIncotermNum + ":") >= 0;
		});
	}

	displayAdd(isUpdate?: boolean) {
		if (isUpdate) {
		} else this.dataObject = new EbTypeFlux();
		this.onShowAddEditDisplay(this.dataObject);
		this.isAddObject = true;
	}

	isFormValidated(): boolean {
		this.setSelectedModesTransport();
		this.setSelectedListEbSchemaPsl();
		this.setSelectedListIncoterm();
		/* je mets ce bout de code en commentaire parce que il sera activer pour la prochaine MEP
		this.setSelectedListTypeDocument(); */

		let isValid = this.dataObject.designation && this.dataObject.designation.length > 0;
		isValid = isValid && this.dataObject.code && this.dataObject.code.length > 0;
		isValid = isValid && this.dataObject.trackingLevel != null;
		isValid =
			isValid && !!this.dataObject.listEbIncoterm && this.dataObject.listEbIncoterm.length > 0;
		isValid =
			isValid &&
			!!this.dataObject.listEbTtSchemaPsl &&
			this.dataObject.listEbTtSchemaPsl.length > 0;

		/* je mets ce bout de code en commentaire parce que il sera activer pour la prochaine MEP
		isValid =
			isValid &&
			!!this.dataObject.listEbTypeDocuments &&
			this.dataObject.listEbTypeDocuments.length > 0; */

		isValid =
			isValid &&
			!!this.dataObject.listModeTransport &&
			this.dataObject.listModeTransport.length > 0;
		return isValid;
	}

	editTypeFlux() {
		if (!this.isFormValidated()) return;
		this.configTypeFluxService.updateTypeFlux(this.dataObject).subscribe((data) => {
			this.isAddObject = false;
			this.editingIndex = null;
			this.backupEditObject = null;
			this.dataObject = null;
			this.selectedModesTransport = null;
			this.selectedListEbTtSchemaPsl = null;
			this.selectedListEbTypeDocuments = null;
			this.selectedListIncoterms = null;
			this.genericTable.refreshData();
		});
	}

	addtypeFlux() {
		if (!this.isFormValidated()) return;
		this.dataObject.xEbCompagnie = new EbCompagnie();
		this.dataObject.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.configTypeFluxService.updateTypeFlux(this.dataObject).subscribe((data: EbTypeFlux) => {
			this.isAddObject = false;
			this.dataObject = null;
			this.selectedModesTransport = null;
			this.selectedListEbTtSchemaPsl = null;
			this.selectedListEbTypeDocuments = null;
			this.selectedListIncoterms = null;
			this.genericTable.refreshData();
		});
	}

	setSelectedModesTransport() {
		this.dataObject.listModeTransport = "";
		this.selectedModesTransport &&
			this.selectedModesTransport.forEach((it) => {
				this.dataObject.listModeTransport += ":" + it + ":";
			});
	}

	setSelectedListIncoterm() {
		this.dataObject.listEbIncoterm = "";
		this.selectedListIncoterms &&
			this.selectedListIncoterms.forEach((it) => {
				this.dataObject.listEbIncoterm += ":" + it.ebIncotermNum + ":";
			});
	}

	setSelectedListTypeDocument() {
		this.dataObject.listEbTypeDocuments = "";
		this.selectedListEbTypeDocuments &&
			this.selectedListEbTypeDocuments.forEach((it) => {
				this.dataObject.listEbTypeDocuments += ":" + it.ebTypeDocumentsNum + ":";
			});
	}

	setSelectedListEbSchemaPsl() {
		this.dataObject.listEbTtSchemaPsl = "";
		this.selectedListEbTtSchemaPsl &&
			this.selectedListEbTtSchemaPsl.forEach((it) => {
				this.dataObject.listEbTtSchemaPsl += ":" + it.ebTtSchemaPslNum + ":";
			});
	}

	backToListZone() {
		if (this.editingIndex != null) {
			Statique.cloneObject<EbTypeFlux>(
				this.backupEditObject,
				this.listEbTypeFlux[this.editingIndex]
			);
			this.editingIndex = null;
		}
		this.backupEditObject = null;
		this.isAddObject = false;
	}

	deleteData(typeFlux: EbTypeFlux) {
		let $this = this;
		this.deleteMessage = null;
		this.deleteHeader = null;

		this.modalService.confirm(
			$this.translate.instant("GENERAL.CONFIRMATION"),
			$this.translate.instant("TYPE_FLUX.TYPE_FLUX_CONFIRM_DELETE"),
			function() {
				let dataObject = typeFlux;
				$this.configTypeFluxService.deleteTypeFlux(dataObject.ebTypeFluxNum).subscribe((data) => {
					$this.genericTable.refreshData();
				});
			},
			function() {},
			true
		);
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.TYPE_FLUX",
			},
		];
	}
}
