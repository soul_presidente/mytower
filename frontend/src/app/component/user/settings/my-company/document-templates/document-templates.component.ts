import { Component, EventEmitter, OnInit, Output, Renderer2, ViewChild } from "@angular/core";
import { GenericTableScreen, Modules } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { BoolToYesNoPipe } from "@app/shared/pipes/boolToYesNo.pipe";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { DocumentTemplatesService } from "@app/services/document-templates.service";
import { TypesDocumentService } from "@app/services/types-document.service";
import { UserService } from "@app/services/user.service";
import { EbTemplateDocumentsDTO } from "@app/classes/dto/ebTemplateDocumentsDTO";
import { EbTypeDocumentsDTO } from "@app/classes/dto/ebTypeDocumentsDTO";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { Router } from "@angular/router";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { EcModule } from "@app/classes/EcModule";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { Input } from "@app/utils/Input";
import { StatiqueService } from "@app/services/statique.service";
import { ExportService } from "@app/services/export.service";
@Component({
	selector: "app-document-templates",
	templateUrl: "./document-templates.component.html",
	styleUrls: ["./document-templates.component.scss"],
})
export class DocumentTemplatesComponent implements OnInit {
	Module = Modules;
	Statique = Statique;
	@Output()
	onDataChanged = new EventEmitter();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	listDocTemplates: Array<EbTemplateDocumentsDTO>;
	listTypeDocuments: Array<EbTypeDocumentsDTO>;
	listTypeDocumentsStr: Array<number>;
	GenericTableScreen = GenericTableScreen;

	isAddDocTemplates = false;
	dataTableConfig: DataTableConfig = new DataTableConfig();
	docTemplates: EbTemplateDocumentsDTO;
	deleteMessage = "";
	deleteHeader = "";

	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	uploadUrl: string = Statique.controllerDocumentTemplates + "/attach-doc";

	modalLexiqueVisible = false;

	module: any;
	ebTypeDocuments: EbTypeDocuments = new EbTypeDocuments();
	listTypeDoc: Array<EbTypeDocuments> = new Array<EbTypeDocuments>();
	listTypeModuletmp: Array<EcModule> = new Array<EcModule>();
	listModule: EcModule[] = [];
	fileIsUploaded: boolean = false;
	uploadHasError: boolean = false;
	fileIsUploading: boolean = false;

	constructor(
		protected renderer: Renderer2,
		private boolToYesAndNo: BoolToYesNoPipe,
		private translate: TranslateService,
		protected router: Router,
		protected modalService: ModalService,
		protected documentTemplatesService: DocumentTemplatesService,
		protected documentTypesService: TypesDocumentService,
		protected headerService: HeaderService,
		protected statiqueService: StatiqueService,
		protected exportService: ExportService
	) {}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.documentTypesService
			.getListTypeDocDTO(new SearchCriteria())
			.subscribe((data: Array<EbTypeDocumentsDTO>) => {
				this.listTypeDocuments = data;
			});
		this.initDocumentTemplates();
		this.statiqueService.getListModule().subscribe((data) => {
			this.listModule = data;
		});
	}

	getListModule() {
		this.statiqueService.getListModule().subscribe((data) => {
			this.listModule = data;

			// translate labels
			this.listModule.forEach((module) => {
				this.translate.get("MODULES." + module.libelle).subscribe((res: string) => {
					module.libelle = res;
				});
			});
		});
	}

	getModuleById(moduleNum) {
		const module = this.listModule.filter((m) => {
			return m.ecModuleNum === moduleNum;
		});
		return module.length ? module[0].libelle : "";
	}

	async initDocumentTemplates() {
		this.dataInfos.cols = [
			{
				field: "cheminDocument",
				header: "Template",
				translateCode: "DOCUMENT_TEMPLATES.TEMPLATE",
				allowClick: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: (item, type, row) => {
					return item != null
						? "<a>" +
								'<span class="fa fa-cloud-download ellipsis-item"  title="Download ' +
								row.originFileName +
								'"> ' +
								row.originFileName +
								"</span>" +
								" </a>"
						: "";
				},
			},
			{
				field: "reference",
				header: "Reference",
				translateCode: "DOCUMENT_TEMPLATES.REFERENCE",
			},
			{
				field: "libelle",
				header: "Label",
				translateCode: "DOCUMENT_TEMPLATES.LABEL",
			},
			{
				translateCode: "Module",
				field: "module",
				width: "150px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(module: Array<Number>) {
					let libelle = "";
					let libelleModule = "";
					if (module != null) {
						module.forEach((element) => {
							let libelleM = this.translate.instant("MODULES." + this.getModuleById(element));
							libelle += libelleM + " - ";
						});
						libelleModule = "<b>" + libelle.substring(0, libelle.length - 2) + "</b><br>";
					}
					return libelleModule;
				}.bind(this),
			},
			{
				field: "listEbTypeDocumentsNum",
				header: "Document types",
				translateCode: "DOCUMENT_TEMPLATES.DOCUMENT_TYPES",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: (item: Array<number>) => {
					let str = "";
					if (item) {
						item.forEach((elem) => {
							this.listTypeDocuments.forEach((el) => {
								if (el.ebTypeDocumentsNum === elem) str += "<b>" + el.nom + "</b><br>";
							});
						});
					}
					return str;
				},
			},
			{
				field: "dateMaj",
				header: "Date de mise à jour",
				translateCode: "DOCUMENT_TEMPLATES.DATE_MAJ",
				render: (item: Date) => {
					return item != null ? Statique.formatDate(item) : "";
				},
			},
		];
		this.dataInfos.dataKey = "ebTemplateDocumentsNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink =
			Statique.controllerDocumentTemplates + "/list-document-templates-table";
		this.dataInfos.dataType = EbTemplateDocumentsDTO;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showHelpBtn = true;
		this.dataInfos.showSearchBtn = true;
	}
	showHelpTemplateHandler() {
		this.modalLexiqueVisible = true;
		// this.router.navigate([Statique.controllerTemplatesLexique]);
	}

	diplayEditDocTemplates(data: EbTemplateDocumentsDTO) {
		this.docTemplates = Statique.cloneObject(data, new EbTemplateDocumentsDTO());
		this.displayAddDocTemplates(true);
	}

	displayAddDocTemplates(isUpdate?: boolean) {
		if (isUpdate) {
		} else this.docTemplates = new EbTemplateDocumentsDTO();
		this.isAddDocTemplates = true;
	}

	backToListDocTemplates() {
		this.isAddDocTemplates = false;
		this.listTypeDocumentsStr = null;
	}

	addDocTemplates(isUpdate) {
		this.docTemplates = Statique.cloneObject(this.docTemplates, new EbTemplateDocumentsDTO());
		this.documentTemplatesService.addDocumentTemplates(this.docTemplates).subscribe(
			function(data) {
				this.listTypeDocumentsStr = null;
				this.docTemplates = null;
				this.genericTable.refreshData();
				this.isAddDocTemplates = false;
				this.onDataChanged.next();
			}.bind(this)
		);
	}
	showHelpTemplate(url) {}
	downloadDoc(url) {
		this.exportService.getFileData(url);
	}

	deleteDocTemplates(data: EbTemplateDocumentsDTO) {
		let $this = this;
		this.deleteMessage = null;
		this.deleteHeader = null;

		this.translate.get("DOCUMENT_TEMPLATES.MESSAGE_DELETE").subscribe((res: string) => {
			this.deleteMessage = res;
		});

		this.translate.get("DOCUMENT_TEMPLATES.HEADER_DELETE").subscribe((res: string) => {
			this.deleteHeader = res;
		});
		this.modalService.confirm(
			this.deleteHeader,
			this.deleteMessage,
			function() {
				$this.documentTemplatesService
					.deleteDocumentTemplates(data.ebTemplateDocumentsNum)
					.subscribe((data) => {
						$this.genericTable.refreshData();
						$this.onDataChanged.next();
					});
			},
			function() {},
			true
		);
	}

	compareUser(user1: any, user2: any) {
		if (user1 && user2) {
			var user1id = user1.ebUserNum ? user1.ebUserNum : user1;
			var user2id = user2.ebUserNum ? user2.ebUserNum : user2;

			return user1id === user2id;
		}
		return false;
	}

	changeModel(form: any) {
		setTimeout(
			function() {
				this.checkFields(form);
			}.bind(this),
			10
		);
	}
	getElementById(elemNum): EbTypeDocumentsDTO {
		this.listTypeDocuments.forEach((el) => {
			if (el.ebTypeDocumentsNum === elemNum) return el;
		});
		return null;
	}

	rerenderTable() {}

	onDataLoaded(data: Array<EbTemplateDocumentsDTO>) {
		this.onDataChanged.emit(data);
	}

	onBeforeUpload(event) {
		let ebCompagnieNum: number = UserService.getConnectedUser()
			? UserService.getConnectedUser().ebCompagnie.ebCompagnieNum
			: null;
		event.formData.append("ebCompagnieNum", ebCompagnieNum);
	}
	onUpload(event) {
		this.docTemplates.cheminDocument = event.xhr.response;
		this.docTemplates.originFileName = event.files[0].name;
	}
	onFileUpload(data: { files: File }): void {
		this.fileIsUploading = true;
		let ebCompagnieNum: number = UserService.getConnectedUser()
			? UserService.getConnectedUser().ebCompagnie.ebCompagnieNum
			: null;
		const formData: FormData = new FormData();
		const file = data.files[0];
		formData.append("files", file, file.name);
		formData.append("ebCompagnieNum", ebCompagnieNum.toString());
		this.documentTypesService.uploadFile(this.uploadUrl, formData).subscribe(
			(data) => {
				if (data) {
					this.docTemplates.cheminDocument = data;
					this.docTemplates.originFileName = file.name;
					this.fileIsUploaded = true;
					this.fileIsUploading = false;
					this.uploadHasError = false;
				}
			},
			(error) => {
				this.uploadHasError = true;
				this.docTemplates.cheminDocument = null;
				this.fileIsUploading = false;
			}
		);
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.DOCUMENT_TEMPLATES",
			},
		];
	}
}
