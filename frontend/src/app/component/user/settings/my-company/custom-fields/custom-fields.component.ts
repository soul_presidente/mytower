import { _MyCompanyComponent } from "../my-company.component";
import { Component, Input, OnInit, ViewContainerRef } from "@angular/core";
import { EbEtablissement } from "@app/classes/etablissement";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EtablissementService } from "@app/services/etablissement.service";
import { UserService } from "@app/services/user.service";
import EbCustomField, { IField } from "@app/classes/customField";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { MessageService } from "primeng/api";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";

@Component({
	selector: "app-custom-fields",
	templateUrl: "./custom-fields.component.html",
	styleUrls: ["./custom-fields.component.scss"],
})
export class CustomFieldsComponent extends _MyCompanyComponent implements OnInit {
	listFields: Array<any>;
	private ebCustomField: EbCustomField;
	@Input()
	ebEtablissement: EbEtablissement;
	disableSaveButton: boolean = false;

	selectedField: any;
	modalDetailCustomFields: boolean = false;

	constructor(
		protected userService: UserService,
		protected vcr?: ViewContainerRef,
		protected etablissementService?: EtablissementService,
		protected ngbService?: NgbModal,
		protected translate?: TranslateService,
		private messageService?: MessageService,
		protected modalService?: ModalService
	) {
		super(userService, vcr, etablissementService, ngbService);
	}

	ngOnInit(): void {
		this.listFields = new Array<Object>();

		this.getCustomField();
	}

	addField() {
		let maxNum = 0;
		if (this.listFields.length > 0) maxNum = this.listFields[this.listFields.length - 1]["num"];

		this.selectedField = {
			label: "",
			num: ++maxNum,
			name: null,
			isNew: true,
		};

		this.modalDetailCustomFields = true;
	}

	removeField(field) {
		this.listFields = this.listFields.filter((it) => {
			return it["num"] != field["num"];
		});
	}
	deleteEbCustomField(field: any) {
		let $this = this;
		this.modalService.confirm(
			this.translate.instant("PRICING_BOOKING.CONFIRMATION"),
			this.translate.instant("REPOSITORY_MANAGEMENT.MESSAGE_DELETE_CUSTOM_FIELDS"),
			function() {
				$this.removeField(field);
			},
			function() {},
			true,
			this.translate.instant("GENERAL.CONFIRM"),
			this.translate.instant("GENERAL.CANCEL")
		);
	}

	save(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		let listFields = [];
		for (let elm of this.listFields) {
			if (elm["label"]) elm["label"] = elm["label"].trim();
			if (elm["value"]) elm["value"] = elm["value"].trim();
			if (elm["name"]) {
				listFields.push(elm);
			}
		}
		this.listFields = listFields;

		let json = JSON.stringify(this.listFields);

		if (!this.userConnected) {
			requestProcessing.afterGetResponse(event);
			throw new Error("User not connected !");
		}

		if (!this.ebCustomField) {
			this.ebCustomField = new EbCustomField();
		}
		this.ebCustomField.xEbCompagnie = this.ebEtablissement.ebCompagnie.ebCompagnieNum;
		this.ebCustomField.ebUser = this.userConnected;
		this.ebCustomField.fields = json;

		this.etablissementService.saveCustomField(this.ebCustomField).subscribe((res) => {
			if (!this.ebCustomField.ebCustomFieldNum)
				this.ebCustomField.ebCustomFieldNum = res.ebCustomFieldNum;
			this.modalDetailCustomFields = false;
			this.selectedField = null;
			requestProcessing.afterGetResponse(event);
			this.showSuccess();
		});
	}

	getCustomField() {
		this.etablissementService
			.getCustomFields(this.ebEtablissement.ebCompagnie.ebCompagnieNum)
			.subscribe((res) => {
				this.ebCustomField = res;
				this.listFields = JSON.parse(this.ebCustomField.fields) || new Array<Object>();
			});
	}

	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE"),
		});
	}

	disableSave(event, field) {
		// field unicity by label
		if (this.listFields.length == 1) return; // no need to check duplicated fields

		let flag: boolean = false;
		let duplicated: Object = null; // checking duplicated fields by label
		this.listFields.forEach((fd) => {
			duplicated =
				(field as IField).name != (fd as IField).name &&
				(field as IField).label == (fd as IField).label;

			if (duplicated) {
				flag = true;
				this.disableSaveButton = true;
			}
		});

		if (!flag) this.disableSaveButton = false;
	}

	openDetailCustomFields(field) {
		this.modalDetailCustomFields = true;
		this.selectedField = field;
	}
	onClosePopup(ev) {
		if (ev && ev.field) {
			if (ev.field.isNew) {
				ev.field.isNew = false;
				this.listFields.push(ev.field);
			} else {
				this.listFields.forEach((f) => {
					if (f.name == ev.field.name) {
						f = ev.field;
						f.isNew = false;
					}
				});
			}
			//save
			this.save(ev.event);
		} else {
			this.modalDetailCustomFields = false;
			this.selectedField = null;
		}
	}
}
