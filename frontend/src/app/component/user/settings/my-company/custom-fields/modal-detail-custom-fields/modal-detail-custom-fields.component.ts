import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { element } from "protractor";

@Component({
	selector: "app-modal-detail-custom-fields",
	templateUrl: "./modal-detail-custom-fields.component.html",
	styleUrls: ["./modal-detail-custom-fields.component.scss"],
})
export class ModalDetailCustomFieldsComponent implements OnInit {
	@Input()
	field: any;

	@Input()
	listFields: Array<any>;

	@Output()
	closePopup = new EventEmitter<any>();

	constructor() {}

	ngOnInit() {}

	modalSave(event) {
		let data = { field: this.field, event: event };
		this.closePopup.emit(data);
	}

	modalClose() {
		this.closePopup.emit(null);
	}
	checkIfExist(code: string): boolean {
		let exist = false;
		if (this.listFields && this.field.isNew) {
			for (let elm of this.listFields) {
				if (!elm.isNew && elm.name == code) {
					exist = true;
					break;
				}
			}
		}
		return exist;
	}
	get canSave(): boolean {
		let canSave: boolean =
			!!this.field.name && !this.checkIfExist(this.field.name) && !!this.field.label;
		return canSave;
	}
}
