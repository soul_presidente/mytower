import { Component, Input, OnInit, ViewChild, Output, EventEmitter } from "@angular/core";
import { EbUser } from "@app/classes/user";
import { EbEtablissement } from "@app/classes/etablissement";
import { UserService } from "@app/services/user.service";
import { EbCompagnie } from "@app/classes/compagnie";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { CompagnieService } from "@app/services/compagnie.service";
import { Statique } from "@app/utils/statique";
import { _MyCompanyComponent } from "../my-company.component";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EcCountry } from "@app/classes/country";
import { InscriptionService } from "@app/services/inscription.service";
import { MessageService } from "primeng/api";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { TranslateService } from "@ngx-translate/core";
import { TypesDocumentService } from "@app/services/types-document.service";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { GenericTableScreen, Modules  } from "@app/utils/enumeration";

@Component({
	selector: "app-company-profile",
	templateUrl: "./company-profile.component.html",
	styleUrls: ["./company-profile.component.css"],
})
export class CompanyProfileComponent extends _MyCompanyComponent implements OnInit {
	@Input()
	user: EbUser;

	etablissement: EbEtablissement = new EbEtablissement();
	ebCompagnie: EbCompagnie = new EbCompagnie();
	myCompagnieNum: number;
	listCountry: Array<EcCountry>;
	isAddEtablissement = false;
	images: any[];
	criteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	isStatiquesLoaded = false;
	isAddObject = false;
	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;
	dataObject: EbEtablissement = new EbEtablissement();
	backupEditObject: EbEtablissement;
	@Output()
	cancel: EventEmitter<any> = new EventEmitter<any>();

	GenericTableScreen = GenericTableScreen;
	Modules = Modules;
	@Input()
	module: number;


	constructor(
		private compagnieService: CompagnieService,
		private inscriptionService: InscriptionService,
		protected documentTypesService: TypesDocumentService,
		private messageService: MessageService,
		protected userService: UserService,
		protected headerService: HeaderService,
		protected translate?: TranslateService
	) {
		super(userService);
	}

	ngOnInit() {
		this.module = Modules.ETABLISSEMENT_INFORMATION;

		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.getCountries();
		this.getCompagnie();
		this.images = [];
		this.images.push({
			source: "assets/showcase/images/demo/galleria/galleria1.jpg",
			alt: "Description for Image 1",
			title: "Title 1",
		});
		this.images.push({
			source: "assets/showcase/images/demo/galleria/galleria2.jpg",
			alt: "Description for Image 2",
			title: "Title 2",
		});

		this.dataInfos.cols = [
			{
				field: "nic",
				header: "NIC/ID",
				translateCode: "COMPANY_PROFILE.ESTABLISHMENT.NIC_ID",
			},
			{
				field: "nom",
				header: "nom",
				translateCode: "INSCRIPTION.NOM_ETABLISSEMENT",
			},
			{
				field: "typeEtablissement",
				header: "Type",
				translateCode: "CUSTOM.TYPE",
			},
			{
				field: "ecCountry",
				header: "Country",
				translateCode: "GENERAL.COUNTRY",
				 render: function(country : EcCountry) {
				 	return Statique.isDefined(country) && country.ecCountryNum ? country.codeLibelle : "";
				 }.bind(this),
			},
			{
				field: "city",
				header: "City",
				translateCode: "ORGANISATION.CITY",
			},
			{
				field: "canShowPrice",
				header: "Can show price",
				translateCode: "COMPANY_PROFILE.ESTABLISHMENT.CAN_SHOW_PRICE",
			},
		];
		this.criteria.ebUserNum = this.userConnected.ebUserNum;
		this.criteria.pageNumber = 1;
		this.criteria.size = 10;
		if (this.userConnected.superAdmin) {
			this.criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		} else {
			this.criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		}
		this.dataInfos.dataKey = "ebEtablissementNum";
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = false;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = this.Statique.controllerEtablissement + "/list-etablissement-table";
		this.dataInfos.dataType = EbEtablissement;
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.criteria.module = this.module;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;

		this.loadData();
	}
	async loadData() {
		this.isStatiquesLoaded = true;
	}
	onShowAddEditDisplay(dataObject: EbEtablissement) {}
	diplayEdit(dataObject: EbEtablissement) {
		this.dataObject = Statique.cloneObject(dataObject, new EbEtablissement());

		this.onShowAddEditDisplay(dataObject);

		this.backupEditObject = Statique.cloneObject<EbEtablissement>(
			this.dataObject,
			new EbEtablissement()
		);
		this.displayAdd(true);
	}

	displayAdd(isUpdate?: boolean) {
		if (isUpdate) {
		} else this.dataObject = new EbEtablissement();

		this.onShowAddEditDisplay(this.dataObject);

		this.isAddObject = true;
	}
	getCompagnie() {
		if (this.userConnected.ebCompagnie.ebCompagnieNum != null) {
			this.myCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			let searchCriteria: SearchCriteria = new SearchCriteria();
			searchCriteria.ebCompagnieNum = this.myCompagnieNum;
			this.compagnieService.getCompagnie(searchCriteria).subscribe((data) => {
				this.ebCompagnie = data;
			});
		}
	}

	uploadedFiles: any[] = [];

	onFileUpload(data: { files: File }): void {
		const formData: FormData = new FormData();
		const file = data.files[0];
		formData.append("typeFile", "1");
		formData.append("files[]", file, file.name);
		formData.append("ebCompagnieNum", this.userConnected.ebCompagnie.ebCompagnieNum.toString());
		formData.append(
			"ebEtablissementNum",
			this.userConnected.ebEtablissement.ebEtablissementNum.toString()
		);

		this.documentTypesService.uploadFile("api/upload/upload-file", formData).subscribe(
			(data) => {
				if (data) {
					this.getCompagnie();
					this.messageService.add({
						severity: "success",
						summary: "Operation Success",
						detail: "Logo uploaded",
					});
				}
			},
			(error) => {
				this.messageService.add({
					severity: "error",
					summary: "Operation Failed",
					detail: "Logo uploaded failed",
				});
			}
		);
	}

	onUpload(event) {
		for (let file of event.files) {
			this.uploadedFiles.push(file);
		}
	}

	onSelect(event) {
		event.formData.append("typeFile", 1);
		event.formData.append("ebEtablissementNum", this.userConnected.ebCompagnie.ebCompagnieNum);
	}

	onBeforeSend(event) {
		event.formData.append("typeFile", 1);
		event.formData.append("ebEtablissementNum", this.userConnected.ebCompagnie.ebCompagnieNum);
	}

	addEtablissement(event: any) {
		this.etablissement = Statique.cloneObject(this.dataObject, new EbEtablissement());
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.etablissement.siret = this.ebCompagnie.siren + this.etablissement.nic;
		this.etablissement.role = this.userConnected.role;
		//this.user.ebEtablissement.ebCompagnie = new EbCompagnie()
		this.etablissement.ebCompagnie.ebCompagnieNum = this.myCompagnieNum;
		this.compagnieService.addEtablissementCompagnie(this.etablissement).subscribe(
			(data) => {
				requestProcessing.afterGetResponse(event);
				this.ebCompagnie.listEtablissements.push(data);
				this.etablissement = null;
				this.showSuccess();
				this.isAddObject = false;
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				this.showError();
			}
		);
	}

	editEtablissement(event: any) {
		this.etablissement = Statique.cloneObject(this.dataObject, new EbEtablissement());
		let requestProcessing = new RequestProcessing();
		this.etablissement.siret = this.ebCompagnie.siren + this.etablissement.nic;
		requestProcessing.beforeSendRequest(event);
		// this.etablissement.ebCompagnie = JSON.parse(JSON.stringify(this.user.ebCompagnie));
		this.compagnieService.editEtablissementCompagnie(this.etablissement).subscribe(
			(data) => {
				requestProcessing.afterGetResponse(event);
				this.etablissement = null;
				this.showSuccess();
				this.isAddObject = false;
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				this.showError();
			}
		);
	}

	getCountries() {
		this.inscriptionService.listPays().subscribe((data) => {
			this.listCountry = data as any; //TODO bind as object
		});
	}

	diplayEditEtablissement(index: number) {
		this.etablissement = this.ebCompagnie.listEtablissements[index];
		this.displayAddEtablissement(true);
	}

	displayAddEtablissement(isUpdate: boolean = false) {
		if (!isUpdate) this.etablissement = new EbEtablissement();
		this.isAddEtablissement = true;
		if (this.listCountry == null) {
			this.inscriptionService.listPays().subscribe((data) => {
				this.listCountry = data as any; //TODO bind as object
			});
		}
	}

	byId(country1: EcCountry, country2: EcCountry) {
		if (country2) {
			return country1.ecCountryNum == country2.ecCountryNum;
		}

		return false;
	}

	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE"),
		});
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.COMPANYS_PROFILE",
			},
		];
	}

	fireCancel(event: any) {
		this.cancel.next(event);
		this.isAddObject= false;
	}
}
