import { Component, OnInit, EventEmitter, Output, Input, ViewChild } from '@angular/core';
import { EdiMapPsl } from '@app/classes/mappingCodePsl';
import { EbCompagnie } from '@app/classes/compagnie';
import { EbTtCategorieDeviation } from '@app/classes/categorieDeviation';
import { TypeOfEvent, GenericTableScreen } from '@app/utils/enumeration';
import SingletonStatique from '@app/utils/SingletonStatique';
import { SearchCriteriaMappingCodePsl } from '@app/utils/SearchCriteriaMappingCodePsl';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalService } from '@app/shared/modal/modal.service';
import { TranslateService } from '@ngx-translate/core';
import { HeaderService } from '@app/services/header.service';
import { MappingCodePslService } from '@app/services/mapping-code-psl.service';
import { CompagnieService } from '@app/services/compagnie.service';
import { UserService } from '@app/services/user.service';
import { MessageService } from 'primeng/api';
import { ConnectedUserComponent } from '@app/shared/connectedUserComponent';
import { GenericTableComponent } from '@app/shared/generic-table/generic-table.component';
import { EbUser } from '@app/classes/user';
import { Statique } from '@app/utils/statique';
import { StatiqueService } from '@app/services/statique.service';
import { HeaderInfos, BreadcumbComponent } from '@app/shared/header/header-infos';
import { SearchCriteria } from '@app/utils/searchCriteria';

@Component({
  selector: 'app-mapping-code-psl-details',
  templateUrl: './mapping-code-psl-details.component.html',
  styleUrls: ['./mapping-code-psl-details.component.scss']
})
export class MappingCodePslDetailsComponent extends ConnectedUserComponent implements OnInit {

  @ViewChild("genericTable", { static: true })
  genericTable: GenericTableComponent;
  GenericTableScreen = GenericTableScreen;
	TypeOfEvent = TypeOfEvent;
	listCategorieDeviation: Array<EbTtCategorieDeviation> = new Array<EbTtCategorieDeviation>();
  listEbCompagniefiltre :  Array<EbCompagnie>;
  listCarrier: Array<EbCompagnie> = new Array<EbCompagnie>();
  listShipper: Array<EbCompagnie> = new Array<EbCompagnie>();
  searchCriteria: SearchCriteria= new SearchCriteria();
  userConnectedCompagnie: EbCompagnie;
  selectConfig: number;
  @Input("backupEditObject")
  backupEditObject: EdiMapPsl;
	editingIndex: number;
  Statique = Statique;
  @Input("listmappingCodePsl")
	listmappingCodePsl: Array<EdiMapPsl> = new Array<EdiMapPsl>();
	@Input("mappingCodePsl")
  mappingCodePsl: EdiMapPsl = new EdiMapPsl();
	@Input("userConnected")
  userConnected: EbUser;

  constructor(
      protected router?: Router,
      private route?: ActivatedRoute,
      protected modalService?: ModalService,
      protected translate?: TranslateService,
      protected headerService?: HeaderService,
      protected mappingCodePslservice?: MappingCodePslService,
      protected compagnieService?: CompagnieService,
      protected userService?: UserService,
      protected messageService?: MessageService,
      protected statiqueService?: StatiqueService,
    ) {
    super();
		this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
  }

  ngOnInit() {
    this.searchCriteria.contact = true;
    this.searchCriteria.communityCompany = false;
    this.searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
    if (this.route && this.route.snapshot.url[1].path == "new"){
      this.headerService.registerBreadcrumbItems(
        this.getBreadcrumbItems(true),
        BreadcumbComponent.MY_COMPANY
      );
    }else{
      this.headerService.registerBreadcrumbItems(
        this.getBreadcrumbItems(false),
        BreadcumbComponent.MY_COMPANY
      );
    }


			this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
      this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

      this.userService.getUserContactsCompagnie(this.searchCriteria).subscribe((data) => {
        data.forEach((t) => {
          if (t.nom != null && t.ebCompagnieNum != null && t.ebCompagnieNum != -1) {
            this.listCarrier.push(t);
          }
        });
      });

			this.compagnieService.getListCompagnieContact(this.searchCriteria).subscribe((data) => {
          data.forEach((t) => {
            if (t.nom != null && t.ebCompagnieNum != null) {
              this.listShipper.push(t);
            }
          });
			});
    
    if(Statique.getInnerObjectStr(window, "history.state.data"))
      this.mappingCodePsl = window.history.state.data as EdiMapPsl;
		
    this.getStatiques();
  }

addMappingCodePsl() {
  this.mappingCodePslservice.updateMappingCodePsl(this.mappingCodePsl).subscribe((data: EdiMapPsl) => {
    this.mappingCodePsl = null;
    this.router.navigateByUrl("app/user/settings/company/mapping-code-psl");
    this.showSuccess("SETTINGS.ADD_MAPPING_CODE_PSL_SUCCESS");
  },
  (error) => {
    this.showError("SETTINGS.ADD_MAPPING_CODE_PSL_ERROR");
  }
  );
}

editMappingCodePsl(event: any) {
  this.mappingCodePslservice.updateMappingCodePsl(this.mappingCodePsl).subscribe((data) => {
    this.editingIndex = null;
    this.backupEditObject = null;
    this.mappingCodePsl = null;
    this.router.navigateByUrl("app/user/settings/company/mapping-code-psl");
    this.showSuccess("SETTINGS.EDIT_MAPPING_CODE_PSL_SUCCESS");
  },
  (error) => {
    this.showError("SETTINGS.EDIT_MAPPING_CODE_PSL_ERROR");
  }
  );
}

backToListZone() {
  this.backupEditObject = null;
  this.router.navigateByUrl("app/user/settings/company/mapping-code-psl");
}

showSuccess(message: string) {
	this.messageService.add({
		life: 5000, //Number of time in milliseconds to wait before closing the message.
		severity: "success",
		summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
		detail: this.translate.instant(message),
	});
}
  showError(message: string) {
		this.messageService.add({
			life: 5000, 
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant(message),
		});
	}

getStatiques() {
  (async () =>
    (this.listCategorieDeviation = await SingletonStatique.getListCategorieDeviation()))();

}
private getBreadcrumbItems(isCreation: boolean = false): Array<HeaderInfos.BreadCrumbItem> {
  return [
    {
      label: "SETTINGS.MY_COMPANY",
      path: "/app/user/settings/company",
    },
    {
      label: "SETTINGS.MAPPING_CODE_PSL",
      path: "/app/user/settings/company/mapping-code-psl",
    },
    {
      label: isCreation ? "SETTINGS.ADD_MAPPING_CODE_PSL" : "SETTINGS.EDIT_MAPPING_CODE_PSL",
    },
  ];
}

}
