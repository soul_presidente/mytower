import { Component, Input, OnInit, EventEmitter, ViewChild } from "@angular/core";
import { EbEtablissement } from "@app/classes/etablissement";
import { EcCurrency } from "@app/classes/currency";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { EtablissementService } from "@app/services/etablissement.service";
import { StatiqueService } from "@app/services/statique.service";
import { Statique } from "@app/utils/statique";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { UserService } from "@app/services/user.service";
import { EbUser } from "@app/classes/user";
import { CompagnieService } from "@app/services/compagnie.service";
import { EbCompagnie } from "@app/classes/compagnie";
import SingletonStatique from "@app/utils/SingletonStatique";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableScreen, UserRole } from "@app/utils/enumeration";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";

@Component({
	selector: "app-currency-etablissement",
	templateUrl: "./currency-etablissement.component.html",
	styleUrls: ["./currency-etablissement.component.css"],
})
export class CurrencyEtablissementComponent extends ConnectedUserComponent implements OnInit {
	myDate = Date.now();
	@Input()
	ebEtablissement: EbEtablissement;
	listExEtablissementCurrency: Array<EbCompagnieCurrency> = new Array<EbCompagnieCurrency>();
	listCurrency: Array<EcCurrency> = null;
	Statique = Statique;
	isNewCurrencyEtablissement: boolean = false;
	oldListExEtablissementCurrency: Array<EbCompagnieCurrency> = new Array<EbCompagnieCurrency>();
	searchCriteria = new SearchCriteria();
	connectedUser: EbUser = new EbUser();
	listEbCompagnie: Array<EbEtablissement> = new Array<EbEtablissement>();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;
	isAddObject = false;
	backupEditObject: EbCompagnieCurrency;
	dataObject: EbCompagnieCurrency = new EbCompagnieCurrency();
	isAddGeneric: boolean = true;
	isHide: boolean;
	isStatiquesLoaded = false;
	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;
	etablissementCurrency: EbCompagnieCurrency = new EbCompagnieCurrency();
	listEbCurencys = [];

	constructor(
		protected etablissementService: EtablissementService,
		protected statiqueService: StatiqueService,
		protected modalService: ModalService,
		protected translate: TranslateService,
		protected userService: UserService,
		protected compagnieService: CompagnieService,
		protected headerService: HeaderService
	) {
		super();
		this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
	}

	ngOnInit(): void {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.initGenericTableCols();
		this.searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteria.isCompagnie = true;
		this.searchCriteria.size = 5;
		this.searchCriteria.pageNumber = 1;
		this.dataInfos.numberDatasPerPage = 5;
		this.searchCriteria.contact = true;
		this.searchCriteria.communityCompany = true;
		this.searchCriteria.excludeCt = true;
		this.searchCriteria.setUser(this.userConnected);
		this.dataInfos.dataKey = "ebCompagnieCurrencyNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = false;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink =
		Statique.controllerEtablissementCurrency + "/get-etablissementCurrencies";
		this.dataInfos.dataType = EbCompagnieCurrency;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showSearchBtn = true;
	}
	async loadData() {
		this.isStatiquesLoaded = true;
		this.genericTable.refreshData();
	}

	newCurrencyEtablissement() {
		this.listExEtablissementCurrency.push(new EbCompagnieCurrency());
		this.oldListExEtablissementCurrency.push(new EbCompagnieCurrency());
		this.isNewCurrencyEtablissement = true;
	}
	displayAdd(isUpdate?: boolean) {
		if (!isUpdate) {
			this.dataObject = new EbCompagnieCurrency();
		}

		this.onShowAddEditDisplay(this.dataObject);

		this.isAddObject = true;
		this.dataInfos.showAddBtn = false;
	}
	onShowAddEditDisplay(dataObject: EbCompagnieCurrency) {}

	diplayEdit(dataObject: EbCompagnieCurrency) {
		this.dataObject = Statique.cloneObject(dataObject, new EbCompagnieCurrency());

		this.onShowAddEditDisplay(dataObject);

		this.backupEditObject = Statique.cloneObject<EbCompagnieCurrency>(
			this.dataObject,
			new EbCompagnieCurrency()
		);
		this.displayAdd(true);
		this.isAddObject = true;
	}

	comeBack(event) {
		this.isAddObject = false;
		this.loadData();
		this.dataInfos.showAddBtn = true;
		this.genericTable.refreshData();
	}
	delete(etablissementCurrencyss: EbCompagnieCurrency) {
		let $this = this;
		let dateDebutNumber: number = new Date(etablissementCurrencyss.dateDebut).getTime();
		let title = null;
		let body = null;
		let bodyRemove = null;
		this.translate.get("CURRENCY.REMOVE_IMPOSSIBLE").subscribe((res: string) => {
			bodyRemove = res;
		});

		if (this.myDate <= dateDebutNumber) {
			$this.etablissementService
				.deleteExEtablissementCurrency(etablissementCurrencyss)
				.subscribe((data: boolean) => {
					if (data) {
						$this.genericTable.refreshData();
					} else {
						this.translate.get("MODAL.INFORMATION").subscribe((res: string) => {
							title = res;
						});
						this.translate.get("CURRENCY.CURRENCY_USED").subscribe((res: string) => {
							body = res;
						});
						this.modalService.information(title, body, function() {});
					}
				});
		} else this.modalService.information(title, bodyRemove, function() {});
	}

	initGenericTableCols() {
		if (this.userConnected.chargeur || this.userConnected.role == UserRole.CONTROL_TOWER) {
			this.dataInfos.cols.push({
				field: "ebCompagnieGuest",
				header: "Carrier",
				translateCode: "CURRENCY.CARRIER",

				render: function(data) {
					return data.nom;
				}.bind(this),
			});
		}
		if (this.userConnected.rolePrestataire || this.userConnected.role == UserRole.CONTROL_TOWER) {
			this.dataInfos.cols.push({
				field: "ebCompagnie",
				header: "Shipper",
				translateCode: "CURRENCY.CHARGER",
				render: function(data) {
					return data.nom;
				}.bind(this),
			});
		}
		this.dataInfos.cols.push(
			{
				field: "ecCurrency",
				header: "From",
				translateCode: "CURRENCY.DE",
				render: function(data) {
					return data.codeLibelle;
				}.bind(this),
			},
			{
				field: "xecCurrencyCible",
				header: "To",
				translateCode: "CURRENCY.TO",
				render: function(data) {
					return data.codeLibelle;
				}.bind(this),
			},
			{
				field: "euroExchangeRate",
				header: "Exchange Rate",
				translateCode: "CURRENCY.EXCHANGE_RATE",
			},
			{
				field: "dateDebut",
				header: " Beginning",
				translateCode: "CURRENCY.BEGINNING_DATE",
				render: function(data) {
					return this.Statique.formatDate(data);
				}.bind(this),
			},
			{
				field: "dateFin",
				header: "End",
				translateCode: "CURRENCY.END_DATE",
				render: function(data) {
					return this.Statique.formatDate(data);
				}.bind(this),
			},
		);
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.EXCHANGE_RATES",
			},
		];
	}
}
