import { GenericTableScreen, Modules } from "@app/utils/enumeration";
import {
	Component,
	EventEmitter,
	Input,
	OnInit,
	Output,
	Renderer2,
	ViewChild,
} from "@angular/core";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { Statique } from "@app/utils/statique";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { EbPlTrancheDTO } from "@app/classes/trpl/EbPlTrancheDTO";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { EcCurrency } from "@app/classes/currency";
import SingletonStatique from "@app/utils/SingletonStatique";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { EbUser } from "@app/classes/user";
import { EbPlGrilleTransportDTO } from "@app/classes/trpl/EbPlGrilleTransportDTO";
import { EbPlCostItemDTO } from "@app/classes/trpl/EbPlCostItemDTO";
import { EbPlLigneCostItemTrancheDTO } from "@app/classes/trpl/EbPlLigneCostItemTrancheDTO";
import { StatiqueService } from "@app/services/statique.service";
import { GenericEnum } from "@app/classes/GenericEnum";
import { GenericEnumTRPL } from "@app/classes/trpl/EnumerationTRPL";
import { GlobalService } from "@app/services/global.service";
import { EbTypeUnit } from "@app/classes/typeUnit";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbCompagnie } from "@app/classes/compagnie";
import { SearchComponent } from "@app/component/freight-analytics/search/search.component";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { UserService } from '@app/services/user.service';
import {CostItemService} from "@app/services/cost-item.service";
import {Cost} from "@app/classes/costCategorie";
import moment from "moment";
import {cloneDeep} from "lodash"

@Component({
	selector: "app-grille-transport",
	templateUrl: "./grille-transport.component.html",
	styleUrls: ["./grille-transport.component.scss"],
})
export class GrilleTransportComponent extends ConnectedUserComponent implements OnInit {
	Module = Modules;
	Statique = Statique;

	@Input("listTranche")
	listTranches: Array<EbPlTrancheDTO>;
	@Input()
	sharedDropDownZIndex: number;
	@Output()
	sharedDropDownZIndexEmit: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onGrilleChanged = new EventEmitter();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	listGrille: Array<EbPlGrilleTransportDTO> = new Array<EbPlGrilleTransportDTO>();
	listAllGrille: Array<EbPlGrilleTransportDTO> = new Array<EbPlGrilleTransportDTO>();
	listCurrencies = new Array<EcCurrency>();
	listCurrenciesByPostCost = new Array<EcCurrency>();

	dataTableConfig: DataTableConfig = new DataTableConfig();
	isAddEditMode = false;
	grille: EbPlGrilleTransportDTO;
	backupEditGrille: EbPlGrilleTransportDTO;

	dataInfos: GenericTableInfos = null;
	detailsInfos: GenericTableInfos = null;
	validEndDate: boolean = false;
	originGrille: EbPlGrilleTransportDTO;
	gridVersion: number = 1;

	listTransporteur: Array<EbUser> = null;
	listCalculationMode: Array<GenericEnum> = [];
	listTypeUnit: Array<EbTypeUnit>;
	searchCriteria: SearchCriteria = new SearchCriteria();
	listCostItemType = new Array<Cost>();

	GenericTableScreen = GenericTableScreen;

	constructor(
		protected transportationPlanService: TransportationPlanService,
		protected userService: UserService,
		protected statiqueService: StatiqueService,
		protected globalService: GlobalService,
		protected renderer: Renderer2,
		protected translate?: TranslateService,
		protected modalService?: ModalService,
		protected costItemService?: CostItemService
	) {
		super();
	}
	ngOnInit() {
		this.userService.getCarriers(this.searchCriteria).subscribe((data: Array<EbUser>) => {
			this.listTransporteur = data;
			if (this.listTransporteur && this.listTransporteur.length > 0) {
				this.listTransporteur.forEach((t) => {
					t.ebEtablissement = null;
					t.ebCompagnie = null;
				});
			}
			this.getListCurrencies();
			this.getListCurrenciesByPostCost();
		});

		this.statiqueService
			.getListGenericEnum(GenericEnumTRPL.GrilleTransportCalculationMode)
			.subscribe((res) => {
				this.listCalculationMode = res;
			});
		// List type Unit
		let criteria: any = {
			ebCompagnieNum: this.userConnected.ebCompagnie.ebCompagnieNum,
		};
		this.globalService
			.runAction(Statique.controllerTypeUnit + "/typeUnit-by-compagnie", criteria)
			.subscribe((res) => {
				this.listTypeUnit = res;
			});
		this.getListCostItemType();
	}

	initGenericTable() {
		this.dataInfos = null;
		this.dataInfos = new GenericTableInfos(this);
		this.dataInfos.cols = [
			{
				field: "reference",
				header: "Reference",
				translateCode: "TRANSPORTATION_PLAN.GRILLE_REFERENCE",
			},
			{
				field: "designation",
				header: "Designation",
				translateCode: "TRANSPORTATION_PLAN.GRILLE_DESIGNATION",
			},
			{
				field: "currency",
				header: "Currency",
				translateCode: "TRANSPORTATION_PLAN.GRILLE_CURRENCY",
				render: (item) => {
					return item ? item.code : "";
				},
			},
			{
				field: "transporteurNum",
				header: "Carrier",
				translateCode: "TRANSPORTATION_PLAN.CARRIER",
				render: (item) => {
					return this.getNomTransporteurById(item);
				},
			},
		];
		this.dataInfos.dataKey = "ebGrilleTransportNum";
		this.dataInfos.showUpComingVersion = true;
		this.dataInfos.showValidGrid = true;
		this.dataInfos.showPreviousGrid = true;
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showCloneBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerTransportationPlan + "/grille-list-table";
		this.dataInfos.dataType = EbPlGrilleTransportDTO;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.dataDetailsField = "costItems";

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.actionColumFixed = false;

		this.detailsInfos = null;
		this.detailsInfos = new GenericTableInfos(this);
		this.detailsInfos.paginator = false;

		let detailsCols: Array<any> = [
			{
				field: "type",
				header: "Cost item",
				translateCode: "TRANSPORTATION_PLAN.GRILLE_COST_ITEM",
				render: (item) => {
					if (item == null) return "";
					let val = Statique.findValueByKey(item, Statique.CostItemType);
					let trVal = null;
					this.translate.get(val).subscribe((res: string) => {
						trVal = res;
					});
					return trVal;
				},
			},
			{
				field: "calculationMode",
				header: "Calculation mode",
				translateCode: "TRANSPORTATION_PLAN.GRILLE_CALC_METHOD",
				render: (item: number) => {
					let val = GenericEnum.findKeyByCode(item, this.listCalculationMode);
					let trVal = this.translate.instant("TRANSPORTATION_PLAN.CALCULATION_MODE." + val);
					return trVal;
				},
			},
			{
				field: "fixedFees",
				header: "Fixed fees",
				translateCode: "TRANSPORTATION_PLAN.GRILLE_FIXED_FEES",
			},
			{
				field: "startingPsl",
				header: "Starting PSL",
				translateCode: "TRANSPORTATION_PLAN.GRILLE_STARTING_PSL",
				render: (item) => {
					if (item == null) return "";
					let val = Statique.findValueByKey(item, Statique.TtPsl);
					let trVal = null;
					this.translate.get(val).subscribe((res: string) => {
						trVal = res;
					});
					return trVal;
				},
			},
			{
				field: "endPsl",
				header: "End PSL",
				translateCode: "TRANSPORTATION_PLAN.GRILLE_END_PSL",
				render: (item) => {
					if (item == null) return "";
					let val = Statique.findValueByKey(item, Statique.TtPsl);
					let trVal = null;
					this.translate.get(val).subscribe((res: string) => {
						trVal = res;
					});
					return trVal;
				},
			},
		];

		if (this.listTranches && this.listTranches.length > 0) {
			this.listTranches.forEach((it, index) => {
				detailsCols.push({
					field: `tranche-${index}-${it.ebTrancheNum}`,
					header: "Tranche " + (it.minimum + "-" + it.maximum),
					translateCode: "Tranche " + (it.minimum + "-" + it.maximum),
					renderParam: index,
					render: (item, renderParam, globalItem: EbPlCostItemDTO) => {
						return globalItem.trancheValues && globalItem.trancheValues[renderParam]
							? globalItem.trancheValues[renderParam].value
							: "";
					},
				});
			});
		}
		this.detailsInfos.cols = detailsCols;

		this.transportationPlanService.listGrille().subscribe((data) => {
			this.listAllGrille = data;
		});
	}

	deleteGrilleTransport(data: EbPlGrilleTransportDTO) {
		let $this = this;
		var deleteMessage = null;
		var deleteHeader = null;

		this.translate.get("TRANSPORTATION_PLAN.MESSAGE_DELETE_GRILLE").subscribe((res: string) => {
			deleteMessage = res;
		});

		this.translate.get("TRANSPORTATION_PLAN.DELETE_GRILLE").subscribe((res: string) => {
			deleteHeader = res;
		});
		this.modalService.confirm(
			deleteHeader,
			deleteMessage,
			function() {
				let grille = data;
				$this.transportationPlanService
					.deleteGrilleTransport(grille.ebGrilleTransportNum)
					.subscribe((data) => {
						if (data) {
							$this.genericTable.refreshData();
							let index = $this.listAllGrille.findIndex((g) => {
								return g.ebGrilleTransportNum == grille.ebGrilleTransportNum;
							});
							if (index > -1) $this.listAllGrille.splice(index, 1);
							$this.onGrilleChanged.emit($this.listAllGrille);
						}
					});
			},
			function() {},
			true
		);
	}

	backToList() {
		this.isAddEditMode = false;
		this.grille = null;
		this.gridVersion = 1;
	}
	compareTypeUnit(typeUnit1: EbTypeUnit, typeUnit2: EbTypeUnit) {
		if (typeUnit1 && typeUnit2) {
			return typeUnit1.ebTypeUnitNum === typeUnit2.ebTypeUnitNum;
		}
	}
	newVersionGrille(isFormValid) {
		if (!this.validEndDate) {
			this.translate.get("MODAL.VERSION_GRL_ERROR1").subscribe((text: string) => {
				const title = text.split(";")[0];
				const body = text.split(";")[1];
				this.modalService.error(title, body, null);
			});
		} else if (isFormValid) {
			this.grille.origineGrlNum = this.originGrille.origineGrlNum != null ?
					 this.originGrille.origineGrlNum : this.originGrille.ebGrilleTransportNum; 
			this.editGrilleTransport(true);
		}
	}
	enableEditGrilleInline(data: EbPlGrilleTransportDTO, isClone: boolean = false) {
		let grille = data;
		grille.transporteur = this.findTransporteurById(data.transporteurNum);
		this.originGrille = grille;
		if (isClone) {
			this.grille = new EbPlGrilleTransportDTO();
			this.grille.designation = grille.designation;
			this.grille.currency = grille.currency;
			this.grille.costItems = grille.costItems;
			this.grille.transporteurNum = grille.transporteurNum;
			this.grille.transporteurNom = grille.transporteurNom;
			this.grille.transporteurPrenom = grille.transporteurPrenom;
			this.grille.transporteur = grille.transporteur;
			this.grille.costItems.forEach((costItem) => {
				costItem.costItemNum = null;
				if (costItem.trancheValues) {
					costItem.trancheValues.forEach((tranche) => (tranche.ebPlLigneCostItemTrancheNum = null));
				}
			});
			this.validEndDate = !!grille.dateFin;
		} else {
			this.grille = grille;
			this.backupEditGrille = Statique.cloneObject(this.grille, new EbPlGrilleTransportDTO());
			this.grille.isEditing = true;
		}
	}
	getGrilleData(grille: EbPlGrilleTransportDTO) {

		if(this.grille.costItems){
			this.grille.costItems.forEach((costItem) =>{
				costItem.libelle = this.getCostItemLibelle(costItem.type);
			})
		}

		return {
			ebGrilleTransportNum: grille.ebGrilleTransportNum,
			designation: grille.designation,
			reference: grille.reference,
			currency: grille.currency,
			costItems: grille.costItems,
			transporteurNum: grille.transporteurNum,
			transporteurNom: grille.transporteurNom,
			transporteurPrenom: grille.transporteurPrenom,
			transporteur: grille.transporteur,
			isEditing: grille.isEditing,

			dateDebut: moment(this.grille.dateDebut).format("YYYY-MM-DD"),
			dateFin: Statique.isDefined(this.grille.dateFin)? moment(this.grille.dateFin).format("YYYY-MM-DD") : null,
			origineGrlNum: grille.origineGrlNum,
		};
	}
	doEditGrilleInline(data: EbPlGrilleTransportDTO) {
		this.grille = Statique.cloneObject(this.grille, new EbPlGrilleTransportDTO());
		this.grille.costItems.forEach((cost) => {
			if (cost.typeUnit) cost.typeUnit.xEbCompagnie = new EbCompagnie();
		});
		let obj = this.getGrilleData(this.grille);
		this.transportationPlanService.saveGrilleTransport(obj).subscribe((data) => {
			this.grille.isEditing = false;
			this.backupEditGrille.isEditing = false;
			this.grille = null;
			this.backupEditGrille = null;
			this.genericTable.refreshData();
			let index = this.listAllGrille.findIndex((g) => {
				return g.ebGrilleTransportNum == this.grille.ebGrilleTransportNum;
			});
			if (index > -1) this.listAllGrille[index] = this.grille;
			this.onGrilleChanged.emit(this.listAllGrille);
		});
	}

	cancelEditGrilleInline(index: number) {
		this.grille = null;
		this.listGrille[index] = this.backupEditGrille;
		this.backupEditGrille = null;
	}
	setCloneMode(data: EbPlGrilleTransportDTO) {
		this.setAddEditMode(data, true);
	}

	setAddEditMode(
		data: EbPlGrilleTransportDTO,
		isClone: boolean = false,
		isUpdate: boolean = false
	) {
		if (!isUpdate) this.gridVersion = null;
		if (data) {
			let newsTranch: Array<EbPlLigneCostItemTrancheDTO> = [];
			if (
				this.listTranches &&
				this.listTranches.length > 0 &&
				data &&
				data.costItems &&
				data.costItems.length > 0 &&
				data.costItems[0].trancheValues
			) {
				this.listTranches.forEach((tv) => {
					let index = data.costItems[0].trancheValues.findIndex(
						(t) => t.ebTrancheNum == tv.ebTrancheNum
					);
					if (index < 0) {
						const line = new EbPlLigneCostItemTrancheDTO();
						line.ebTrancheNum = tv.ebTrancheNum;
						line.ebPlLigneCostItemTrancheNum = null;
						line.value = null;
						newsTranch.push(line);
					}
				});
				if (newsTranch) {
					data.costItems.forEach((cst) => {
						cst.trancheValues = cst.trancheValues.concat(newsTranch);
					});
				}
			}
			this.enableEditGrilleInline(data, isClone);
		} else {
			this.grille = new EbPlGrilleTransportDTO();
			this.originGrille = null;
		}
		this.isAddEditMode = true;
	}

	changeModel(form: any) {
		this.checkFields(form);
	}

	checkFieldsInline(index: number) {
		var findError = false;
		var findRefOverlap = false;
		var theGrille = this.listGrille[index];

		/*
    if ((!theGrille.fixedFees && theGrille.fixedFees !== 0) || !theGrille.reference) {
      findError = true;
    }*/

		this.listGrille &&
			this.listGrille.some((aGrille, index, array) => {
				if (
					aGrille.reference &&
					theGrille.reference &&
					aGrille.ebGrilleTransportNum !== theGrille.ebGrilleTransportNum
				) {
					if (theGrille.reference.trim().toLowerCase() === aGrille.reference.trim().toLowerCase()) {
						findError = true;
						findRefOverlap = true;
						return true;
					}
				}
			});
	}

	checkFields(form: any) {
		var findOverlap = false;

		this.listAllGrille &&
			this.listAllGrille.some((aGrille, index, array) => {
				if (
					aGrille.reference &&
					this.grille.reference &&
					aGrille.ebGrilleTransportNum !== this.grille.ebGrilleTransportNum
				) {
					if (
						this.grille.reference.trim().toLowerCase() === aGrille.reference.trim().toLowerCase()
					) {
						findOverlap = true;
						return true;
					}
				}
			});

		if (findOverlap) {
			form.controls["reference"].setErrors("overlap", true);
		} else if (this.grille.reference) {
			form.controls["reference"].setErrors(null);
		}
	}


	addGrilleTransport() {


		this.grille = cloneDeep(this.grille, new EbPlGrilleTransportDTO());
		this.grille.costItems.forEach((cost) => {
			if (cost.typeUnit) cost.typeUnit.xEbCompagnie = new EbCompagnie();
			if(cost.type) cost.libelle = this.getCostItemLibelle(cost.type);
		});
		let obj = this.getGrilleData(this.grille);
		this.transportationPlanService.saveGrilleTransport(obj).subscribe((data) => {
			this.grille.ebGrilleTransportNum = Number(data);
			this.listGrille.push(this.grille);
			this.isAddEditMode = false;
			this.listAllGrille.push(this.grille);
			this.onGrilleChanged.emit(this.listAllGrille);
			this.grille = null;
			this.gridVersion = 1;
		});
	}
	

	editGrilleTransport(isNewVersion: boolean = false) {
		this.grille = Statique.cloneObject(this.grille, new EbPlGrilleTransportDTO());
		this.grille.costItems.forEach((cost) => {
			if (cost.typeUnit) cost.typeUnit.xEbCompagnie = new EbCompagnie();
		});
		let obj = this.getGrilleData(this.grille);
		this.transportationPlanService.saveGrilleTransport(obj).subscribe((data) => {
			if (isNewVersion && data == -1) {
				this.translate.get("MODAL.VERSION_GRL_ERROR2").subscribe((text: string) => {
					const title = text.split(";")[0];
					const body = text.split(";")[1];
					this.modalService.error(title, body, null);
				});
			} else {
				this.isAddEditMode = false;
				let index = this.listAllGrille.findIndex((g) => {
					return g.ebGrilleTransportNum == this.grille.ebGrilleTransportNum;
				});
				if (index > -1) this.listAllGrille[index] = this.grille;
				this.onGrilleChanged.emit(this.listAllGrille);
				this.grille = null;
			}
		});
	}

	async getListCurrencies() {
		this.listCurrencies = await SingletonStatique.getListEcCurrency();
	}

	getListCurrenciesByPostCost() {
		this.searchCriteria.communityCompany = true;
		this.searchCriteria.ebEtablissementNum = this.userConnected.ebUserNum;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.transportationPlanService
			.selectListCurrencyByeCompagnieCurrency(this.searchCriteria)
			.subscribe((data) => {
				this.listCurrenciesByPostCost = data;
			});
	}

	rerenderTable() {}

	onDataLoaded(data: Array<EbPlGrilleTransportDTO>) {
		this.onGrilleChanged.emit(this.listAllGrille);
	}

	removeCostItem(grille: EbPlGrilleTransportDTO, costItem: EbPlCostItemDTO) {
		grille.costItems.forEach((item, index) => {
			if (item == costItem) grille.costItems.splice(index, 1);
		});
	}

	addCostItem(grille: EbPlGrilleTransportDTO) {
		const newCostItem = new EbPlCostItemDTO();
		newCostItem.currency = grille.currency;

		if (this.listTranches != null && this.listTranches.length > 0) {
			this.listTranches.forEach((tr) => {
				const line = new EbPlLigneCostItemTrancheDTO();
				line.ebTrancheNum = tr.ebTrancheNum;
				newCostItem.trancheValues.push(line);
			});
		}

		grille.costItems.push(newCostItem);
	}

	findTransporteurById(transporteurId: number): EbUser {
		var tr = new EbUser();
		if (this.listTransporteur && this.listTransporteur.length > 0) {
			this.listTransporteur.forEach((t) => {
				if (t.ebUserNum == transporteurId) {
					tr = t;
				}
			});
		}

		return tr;
	}

	getNomTransporteurById(trNum: number): string {
		if (trNum != null && this.listTransporteur != null) {
			let tr: EbUser = this.findTransporteurById(trNum);
			return tr != null
				? (tr.nom != null ? tr.nom : "") + " " + (tr.prenom != null ? tr.prenom : "")
				: "";
		}
		return "";
	}
	compareUser(user1: any, user2: any) {
		if (user1 && user2) {
			var user1id = user1.ebUserNum ? user1.ebUserNum : user1;
			var user2id = user2.ebUserNum ? user2.ebUserNum : user2;

			return user1id === user2id;
		}
		return false;
	}

	sharedDropDownZIndexChange(event) {
		this.sharedDropDownZIndexEmit.emit(event);
	}

	getListCostItemType(){
		let criteria = new SearchCriteria();
		criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		this.costItemService.listCostItems(criteria).subscribe((res) =>{
			this.listCostItemType = res.data;
		})
	}

	getCostItemLibelle(costNum: number){
		let cost = new Cost();
		cost =  this.listCostItemType.find(costItem => costItem.ebCostNum === costNum);
		return cost.libelle.toString();
	}
}
