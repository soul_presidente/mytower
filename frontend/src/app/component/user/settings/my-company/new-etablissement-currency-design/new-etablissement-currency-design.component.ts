import { CompanyRole } from './../../../../../utils/enumeration';
import { filter } from 'rxjs/operators';
import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { EbEtablissement } from "@app/classes/etablissement";
import { EcCurrency } from "@app/classes/currency";
import { EbCompagnieCurrency } from "@app/classes/compagnieCurrency";
import { EtablissementService } from "@app/services/etablissement.service";
import { StatiqueService } from "@app/services/statique.service";
import { Statique } from "@app/utils/statique";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { UserService } from "@app/services/user.service";
import { EbUser } from "@app/classes/user";
import { CompagnieService } from "@app/services/compagnie.service";
import { EbCompagnie } from "@app/classes/compagnie";
import SingletonStatique from "@app/utils/SingletonStatique";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { MessageService } from "primeng/api";
import { UserRole } from "@app/utils/enumeration";

@Component({
	selector: "app-new-etablissement-currency-design",
	templateUrl: "./new-etablissement-currency-design.component.html",
	styleUrls: ["./new-etablissement-currency-design.component.scss"],
})
export class NewEtablissementCurrencyDesignComponent extends ConnectedUserComponent
	implements OnInit {
	@Input()
	ebEtablissement: EbEtablissement;
	listExEtablissementCurrency: Array<EbCompagnieCurrency> = new Array<EbCompagnieCurrency>();
	listCurrency: Array<EcCurrency> = null;
	Statique = Statique;
	isNewCurrencyEtablissement: boolean = false;
	oldListExEtablissementCurrency: Array<EbCompagnieCurrency> = new Array<EbCompagnieCurrency>();
	searchCriteria: SearchCriteria = new SearchCriteria();
	connectedUser: EbUser = new EbUser();
	listEbCompagnie: Array<EbEtablissement> = new Array<EbEtablissement>();
	isAddObject = false;
	backupEditObject: EbCompagnieCurrency;
	dataObject: EbCompagnieCurrency = new EbCompagnieCurrency();
	isAddGeneric: boolean = true;
	@Output()
	validateAdd: EventEmitter<any> = new EventEmitter<any>();
	@Input()
	etablissementCurrency: EbCompagnieCurrency = new EbCompagnieCurrency();

	listEbCurencys = [];
	listEbCompagniefiltre: Array<EbCompagnie>;

	userConnectedCompagnie: EbCompagnie;
	selectConfig: number;
	listEbCompagniesChargeur: Array<EbCompagnie> = new Array<EbCompagnie>();
	listEbCompagniesPrestataire: Array<EbCompagnie> = new Array<EbCompagnie>();
	controlTowerRole: number;

	constructor(
		protected etablissementService: EtablissementService,
		protected statiqueService: StatiqueService,
		protected modalService: ModalService,
		protected translate: TranslateService,
		protected userService: UserService,
		protected compagnieService: CompagnieService,
		protected headerService: HeaderService,
		protected messageService?: MessageService
	) {
		super();
		this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
	}

	ngOnInit(): void {
		this.searchCriteria.size = 10;
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.contact = true;
		this.searchCriteria.communityCompany = false;
		this.controlTowerRole = UserRole.CONTROL_TOWER;
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		if (this.userConnected.rolePrestataire) {
			this.listEbCompagnieFiltreByRolePrestaire();
		} else if (this.userConnected.chargeur) {
			this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
			this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			this.searchCriteria.companyRole = CompanyRole.PRESTATAIRE;
			this.userService.getUserContactsCompagnie(this.searchCriteria).subscribe((data) => {
				this.listEbCompagniesPrestataire = data;
			});
		} else {
			this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
			this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			this.searchCriteria.companyRole = CompanyRole.CHARGEUR;
			this.userService.getUserContactsCompagnie(this.searchCriteria).subscribe((data) => {
				this.listEbCompagniesChargeur = data;
			});
			this.searchCriteria.companyRole = CompanyRole.PRESTATAIRE;
			this.userService.getUserContactsCompagnie(this.searchCriteria).subscribe((data) => {
				this.listEbCompagniesPrestataire = data;
			});
		}

		this.searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		(async () => (this.listCurrency = await SingletonStatique.getListEcCurrency()))();
		this.etablissementService
			.getListExEtablissementCurrency(this.searchCriteria)
			.subscribe((data: Array<EbCompagnieCurrency>) => {
				this.listExEtablissementCurrency = data;
				if (this.listExEtablissementCurrency.length == 0) {
					this.newCurrencyEtablissement();
				} else {
					this.listExEtablissementCurrency.forEach(
						(exEtablissementCurrency: EbCompagnieCurrency, index: number) => {
							if (
								exEtablissementCurrency.ebCompagnieGuest.ebCompagnieNum ==
								this.userConnected.ebCompagnie.ebCompagnieNum
							)
								exEtablissementCurrency.ebCompagnieGuest.ebCompagnieNum =
									exEtablissementCurrency.ebCompagnie.ebCompagnieNum;

							this.listExEtablissementCurrency[index] = Statique.cloneObject(
								exEtablissementCurrency,
								new EbCompagnieCurrency()
							);
							this.oldListExEtablissementCurrency[index] = Statique.cloneObject(
								exEtablissementCurrency,
								new EbCompagnieCurrency()
							);
							this.listExEtablissementCurrency[index].formatToDatePicker();
							this.oldListExEtablissementCurrency[index].formatToDatePicker();
						}
					);
				}
			});
	}

	listEbCompagnieFiltreByRolePrestaire() {
		this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteria.companyRole = CompanyRole.CHARGEUR;
		this.userService.getUserContactsCompagnie(this.searchCriteria).subscribe((data) => {
			this.listEbCompagniesChargeur = data;
		});
	}

	newCurrencyEtablissement() {
		this.listExEtablissementCurrency.push(new EbCompagnieCurrency());
		this.oldListExEtablissementCurrency.push(new EbCompagnieCurrency());
		this.isNewCurrencyEtablissement = true;
	}

	initEbCompanyCurrency(){
		if (this.isChargeur || this.isControlTower) {
			this.etablissementCurrency.ebCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			let compagnieNumGestSauvegarde = this.etablissementCurrency.ebCompagnieGuest.ebCompagnieNum;
			this.etablissementCurrency.ebCompagnieGuest = new EbCompagnie();
			this.etablissementCurrency.ebCompagnieGuest.ebCompagnieNum = compagnieNumGestSauvegarde;
		} else {
			this.etablissementCurrency.ebCompagnieGuest = new EbCompagnie();
			this.etablissementCurrency.ebCompagnieGuest.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			let compagnieNumSauvegarde = this.etablissementCurrency.ebCompagnie.ebCompagnieNum;
			this.etablissementCurrency.ebCompagnie = new EbCompagnie();
			this.etablissementCurrency.ebCompagnie.ebCompagnieNum = compagnieNumSauvegarde;
		}
		this.etablissementCurrency.formatToDate();
		this.etablissementCurrency.ebEtablissement = null;
	}

	add(btnSave) {
		this.save(btnSave);
	}

	save(btnSave) {
		let requestProcessingAdd = new RequestProcessing();
		let requestProcessingDelete = new RequestProcessing();
		requestProcessingAdd.beforeSendRequest(btnSave);
		this.initEbCompanyCurrency();
		this.etablissementService.saveExEtablissementCurrency(this.etablissementCurrency)
			.subscribe((data) => {
				requestProcessingAdd.afterGetResponse(btnSave);
				this.isNewCurrencyEtablissement = false;
				let isSuccess = data as boolean;
				if(isSuccess){
					this.validateAdd.emit();
					this.showSuccessRate();
				}else{
					this.showErrors();
				}
				let exEtablissementCurrency = new EbCompagnieCurrency();
				exEtablissementCurrency.constructorCopy(data);
				exEtablissementCurrency.formatToDatePicker();
				(err) => {
					this.showError();
				};
			});
	}

	updateEbCompanyCurrency(btnSave){
		let requestProcessingAdd = new RequestProcessing();
		let requestProcessingDelete = new RequestProcessing();
		requestProcessingAdd.beforeSendRequest(btnSave);
		this.initEbCompanyCurrency();
		this.etablissementService.updateExEtablissementCurrency(this.etablissementCurrency)
			.subscribe((data) => {
				requestProcessingAdd.afterGetResponse(btnSave);
				this.isNewCurrencyEtablissement = false;
				let isSuccess = data as boolean;
				if(isSuccess){
					this.validateAdd.emit();
					this.showSuccessRate();
				}else{
					this.showErrors();
				}
				let exEtablissementCurrency = new EbCompagnieCurrency();
				exEtablissementCurrency.constructorCopy(data);
				exEtablissementCurrency.formatToDatePicker();
				(err) => {
					this.showError();
				};
			});
	}

	changeModel(form: any, index: number, dateDebut: any, dateFin: any) {
		setTimeout(
			function() {
				this.checkInterval(form, index, dateDebut, dateFin);
			}.bind(this),
			10
		);
	}

	checkInterval(form: any, index: number, dateDebut: any, dateFin: any) {
		let findOverlap = false;
		this.copyListOld(index);
		let exEtablissementCurrency = this.listExEtablissementCurrency[index];
		if (exEtablissementCurrency.checkInterval()) {
			this.oldListExEtablissementCurrency.forEach(
				(_exEtablissementCurrency: EbCompagnieCurrency, i: number) => {
					if (index != i) {
						if (
							_exEtablissementCurrency.ecCurrency.ecCurrencyNum ===
								exEtablissementCurrency.ecCurrency.ecCurrencyNum &&
							_exEtablissementCurrency.xecCurrencyCible.ecCurrencyNum ===
								exEtablissementCurrency.xecCurrencyCible.ecCurrencyNum &&
							(_exEtablissementCurrency.ebCompagnieGuest.ebCompagnieNum ==
								exEtablissementCurrency.ebCompagnieGuest.ebCompagnieNum ||
								_exEtablissementCurrency.ebCompagnie.ebCompagnieNum ==
									exEtablissementCurrency.ebCompagnieGuest.ebCompagnieNum)
						) {
							_exEtablissementCurrency.formatToDate();
							if (
								Statique.overlapTwoDateRanges(
									exEtablissementCurrency.dateDebut,
									exEtablissementCurrency.dateFin,
									_exEtablissementCurrency.dateDebut,
									_exEtablissementCurrency.dateFin
								)
							) {
								findOverlap = true;
								return findOverlap;
							}
						}
					}
				}
			);
		} else if (
			exEtablissementCurrency._dateDebut != null &&
			exEtablissementCurrency._dateFin != null
		)
			findOverlap = true;

		if (findOverlap) {
			if (exEtablissementCurrency._dateDebut != null)
				form.controls["_dateDebut"].setErrors("dateNotValid", true);
			if (exEtablissementCurrency._dateFin != null)
				form.controls["_dateFin"].setErrors("dateNotValid", true);
		} else {
			if (exEtablissementCurrency._dateDebut != null) form.controls["_dateDebut"].setErrors(null);
			else form.controls["_dateDebut"].setErrors("dateNotValid", true);
			if (exEtablissementCurrency._dateFin != null) form.controls["_dateFin"].setErrors(null);
			else form.controls["_dateFin"].setErrors("dateNotValid", true);
		}
	}

	copyListOld(indexSelected: number) {
		this.oldListExEtablissementCurrency.forEach(
			(exEtablissementCurrency: EbCompagnieCurrency, index: number) => {
				if (indexSelected != index) {
					this.listExEtablissementCurrency[index] = Statique.cloneObject(
						exEtablissementCurrency,
						new EbCompagnieCurrency()
					);
					this.listExEtablissementCurrency[index].formatToDatePicker();
				}
			}
		);
	}

	fetchMoreCompagniesChargeur(){
		this.searchCriteria.pageNumber += 1;
		this.searchCriteria.companyRole = CompanyRole.CHARGEUR;
		this.userService.getUserContactsCompagnie(this.searchCriteria).subscribe((data: Array<EbCompagnie>) => {
			this.listEbCompagniesChargeur.push(...data);
			this.listEbCompagniesChargeur = this.listEbCompagniesChargeur.filter(
				(compagnie, i, arr) => arr.findIndex(t => t.ebCompagnieNum === compagnie.ebCompagnieNum) === i
			  );
		});
	}

	fetchMoreCompagniesPrestataire(){
		this.searchCriteria.pageNumber += 1;
		this.searchCriteria.companyRole = CompanyRole.PRESTATAIRE;
		this.userService.getUserContactsCompagnie(this.searchCriteria).subscribe((data: Array<EbCompagnie>) => {
			this.listEbCompagniesPrestataire.push(...data);
			this.listEbCompagniesPrestataire = this.listEbCompagniesPrestataire.filter(
				(compagnie, i, arr) => arr.findIndex(t => t.ebCompagnieNum === compagnie.ebCompagnieNum) === i
			  );
		});
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.EXCHANGE_RATES",
			},
		];
	}
	backToListCurrency() {
		this.validateAdd.emit();
	}
	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	showSuccessRate() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}
	showErrors() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAILS"),
		});
	}
	showConfirmation() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("CURRENCY.CONFIRMATION_UPDATE_EXCHANGE_Rate"),
			detail: this.translate.instant("CURRENCY.ERROR_SAVE"),
		});
	}
}
