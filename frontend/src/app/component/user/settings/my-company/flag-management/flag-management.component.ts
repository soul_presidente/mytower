import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { FlagService } from "@app/services/flag.service";
import { EbFlag } from "@app/classes/ebFlag";
import { StatiqueService } from "@app/services/statique.service";
import { Statique } from "@app/utils/statique";
import { GenericTableScreen } from "@app/utils/enumeration";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { TranslateService } from "../../../../../../../node_modules/@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { FlagSingleViewGenericCell } from "@app/shared/generic-cell/commons/flag-single-view.generic-cell";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { PageableSearchCriteria } from "@app/utils/PageableSearchCriteria";

@Component({
	selector: "app-flag-management",
	templateUrl: "./flag-management.component.html",
	styleUrls: ["./flag-management.component.scss"],
})
export class FlagManagementComponent implements OnInit {
	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	listIcons: Array<any>;
	listIconsCode: Array<number> = new Array<number>();

	isAddMode: boolean = false;

	Statique = Statique;

	listFlag: Array<EbFlag> = new Array<EbFlag>();
	flag: EbFlag = new EbFlag();
	backupEditFlag: EbFlag;
	editingIndex: number;
	deleteMessage = "";
	deleteHeader = "";
	flagSearchCriteria: PageableSearchCriteria = new PageableSearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;

	constructor(
		protected flagService: FlagService,
		protected statiqueService: StatiqueService,
		protected translate: TranslateService,
		protected modalService: ModalService,
		protected headerService: HeaderService
	) {}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.initStatique();

		this.flagSearchCriteria.size = 50;

		this.dataInfos.cols = [
			{
				field: "reference",
				header: "Reference",
				translateCode: "FLAG_MANAGEMENT.REF",
			},
			{
				field: "name",
				header: "Name",
				translateCode: "FLAG_MANAGEMENT.NAME",
			},
			{
				field: "color",
				header: "Color",
				translateCode: "FLAG_MANAGEMENT.COLOR",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: (color: string) => {
					let html = "";

					html += '<div style="width: 100%; height: 25px; ';
					html += "border-radius: 4px; border-width: 2px; border-color: black; ";
					html += "background-color: " + color + ';"';
					html += "></div>";

					return html;
				},
			},
			{
				field: "icon",
				header: "Icon",
				translateCode: "FLAG_MANAGEMENT.ICON",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: FlagSingleViewGenericCell,
				genericCellParams: new FlagSingleViewGenericCell.Params({
					fieldFlagIcon: "icon",
					fieldFlagColor: "color",
				}),
			},
		];

		this.dataInfos.dataKey = "ebFlagNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerFlag + "/list-flag-table";
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
	}

	initStatique() {
		this.statiqueService.getListFlagIcon().subscribe((res) => {
			this.listIcons = res;

			this.listIcons.forEach((i) => {
				this.listIconsCode.push(i.code);
			});
		});
	}

	displayEditFlag(ebFlag: EbFlag) {
		this.flag = ebFlag;
		this.backupEditFlag = EbFlag.constructorCopy(this.flag);
		this.displayAddFlag(true);
	}

	displayAddFlag(isUpdate?: boolean) {
		if (!isUpdate) {
			this.flag = new EbFlag();
			this.flag.color = "#FF0000";
		}
		this.isAddMode = true;
	}

	backToListFlag() {
		if (this.editingIndex != null) {
			this.listFlag[this.editingIndex].copy(this.backupEditFlag);
			this.editingIndex = null;
		}
		this.backupEditFlag = null;
		this.isAddMode = false;
	}
	editFlag() {
		this.flagService.addFlag(this.flag).subscribe((data) => {
			this.isAddMode = false;
			this.editingIndex = null;
			this.backupEditFlag = null;
			this.genericTable.refreshData();
			this.onDataChanged.next();
		});
	}
	addFlag() {
		var self = this;
		this.flagService.addFlag(this.flag).subscribe(function(data) {
			self.isAddMode = false;
			self.genericTable.refreshData();
			self.onDataChanged.next();
		});
	}

	deleteFlag(ebFlag: EbFlag) {
		let $this = this;
		this.deleteMessage = null;
		this.deleteHeader = null;

		this.translate.get("FLAG_MANAGEMENT.MESSAGE_DELETE_FLAG").subscribe((res: string) => {
			this.deleteMessage = res;
		});

		this.translate.get("FLAG_MANAGEMENT.DELETE_FLAG").subscribe((res: string) => {
			this.deleteHeader = res;
		});
		this.modalService.confirm(
			this.deleteHeader,
			this.deleteMessage,
			function() {
				$this.flagService.deleteFlag(ebFlag.ebFlagNum).subscribe((data) => {
					$this.genericTable.refreshData();
					$this.onDataChanged.next();
				});
			},
			function() {},
			true
		);
	}

	onDataLoaded(data: Array<EbFlag>) {
		this.onDataChanged.emit(data);
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.FLAGS",
			},
		];
	}
}
