import { Component, OnInit } from "@angular/core";
import { Statique } from "../../../../../utils/statique";
import { SearchCriteria } from "../../../../../utils/searchCriteria";
import { GenericTableScreen } from "../../../../../utils/enumeration";
import { TranslateService } from "@ngx-translate/core";
import { DeviationSettingsCheckboxComponent } from "./deviation-settings-checkbox/deviation-settings-checkbox.component";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";

@Component({
	selector: "app-deviation-settings",
	templateUrl: "./deviation-settings.component.html",
	styleUrls: ["./deviation-settings.component.css"],
})
export class DeviationSettingsComponent implements OnInit {
	cols: any;
	datas: any;
	title: string = "Deviation Settings";
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	searchInput: SearchCriteria = new SearchCriteria();
	datatableComponent = GenericTableScreen;
	Statique = Statique;

	constructor(private translateService: TranslateService) {}

	ngOnInit() {
		this.dataInfos.cols = [
			{ field: "identifiant", translateCode: "DEVIATION_MANAGEMENT.IDENTIFIANT", type: "text" },
			{ field: "libelle", translateCode: "DEVIATION_MANAGEMENT.LIBELLE", type: "text" },
			{ field: "type", translateCode: "DEVIATION_MANAGEMENT.TYPE", type: "text" },
			{ field: "sousType", translateCode: "DEVIATION_MANAGEMENT.SOUS_TYPE", type: "text" },
			{
				field: "Module",
				translateCode: "DEVIATION_MANAGEMENT.MODULE",
				type: "text",
				valueField: "xModule",
				source: "web",
				url: Statique.controllerTypeUnit + "/list-units",
			},
			{ field: "objet", translateCode: "DEVIATION_MANAGEMENT.OBJET", type: "text" },
			{ field: "champs", translateCode: "DEVIATION_MANAGEMENT.CHAMPS", type: "text" },
			{ field: "detail", translateCode: "DEVIATION_MANAGEMENT.DETAILS", type: "text" },
			{
				field: "active",
				translateCode: "DEVIATION_MANAGEMENT.ACTIVE",
				type: "boolean",
				valueField: "active",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: DeviationSettingsCheckboxComponent,
				allowClick: true,
				genericCellHandler: function(key: string, value: any) {
					console.log(key, value);
				},
			},
			{
				field: "createDeviation",
				translateCode: "DEVIATION_MANAGEMENT.CREATE",
				type: "boolean",
				valueField: "active",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: DeviationSettingsCheckboxComponent,
				allowClick: true,
				genericCellHandler: function(key: string, value: any) {
					console.log(key, value);
				},
			},
			{
				field: "sendAlert",
				translateCode: "DEVIATION_MANAGEMENT.SEND_ALERT",
				type: "boolean",
				valueField: "sendAlert",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: DeviationSettingsCheckboxComponent,
				allowClick: true,
				genericCellHandler: function(key: string, value: any) {
					console.log(key, value);
				},
			},
			{
				field: "changeStatus",
				translateCode: "DEVIATION_MANAGEMENT.CHANGE_ALERT",
				type: "boolean",
				valueField: "changeStatus",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: DeviationSettingsCheckboxComponent,
				allowClick: true,
				genericCellHandler: function(key: string, value: any) {
					console.log(key, value);
				},
			},
		];

		this.translator(this.dataInfos.cols);
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.dataKey = "ebQmDeviationSettingsNum";
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.activateCRUD = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerQualityManagement + "/list-Deviation-Setting";
		this.dataInfos.numberDatasPerPage = 10;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
	}

	translator(tab: any[]) {
		tab.forEach((it) => {
			this.translateService.get(it.translateCode).subscribe((res) => {
				it["header"] = res;
			});
		});
	}
}
