import { Component, OnInit } from "@angular/core";
import { DeviationSettingsComponent } from "../deviation-settings.component";
import { GenericCell } from "@app/shared/generic-cell/generic-cell";

@Component({
	selector: "app-deviation-settings-checkbox",
	template: '<input type="checkbox" [(ngModel)]="checkModel" (change)="onChange($event)"/>',
})
export class DeviationSettingsCheckboxComponent
	extends GenericCell<any, any, DeviationSettingsComponent>
	implements OnInit {
	checkModel: boolean = true;

	constructor() {
		super();
	}

	ngOnInit() {}

	onChange($event) {
		this.outputEvent("value-change", $event.target.checked);
	}
}
