import { OnInit, Component, ViewChild } from "@angular/core";
import { Modules, GenericTableScreen } from "@app/utils/enumeration";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { HeaderService } from "@app/services/header.service";
import { AuthenticationService } from "@app/services/authentication.service";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { Statique } from "@app/utils/statique";
import { EbTypeGoods } from "@app/classes/EbTypeGoods";
import { BreadcumbComponent, HeaderInfos } from "@app/shared/header/header-infos";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { TypeGoodsService } from "@app/services/type-goods.service";

@Component({
	selector: "app-type-goods",
	templateUrl: "./type-goods.component.html",
	styleUrls: ["./type-goods.component.scss"],
})
export class TypeGoodsComponent extends ConnectedUserComponent implements OnInit {
	Module = Modules;

	listTypeGoods: Array<EbTypeGoods> = new Array<EbTypeGoods>();

	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;
	editMode: boolean = false;

	isAddEditMode = false;
	typeGoods: EbTypeGoods = new EbTypeGoods();
	backupTypeGoods: EbTypeGoods;
	editingIndex: number;

	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;

	private listExistingCodes: Array<string> = [];

	constructor(
		protected headerService: HeaderService,
		protected authenticationService: AuthenticationService,
		protected typeGoodsService: TypeGoodsService,
		protected translate: TranslateService,
		protected modalService: ModalService
	) {
		super(authenticationService);
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);

		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 50;

		this.dataInfos.cols = [
			{
				field: "code",
				translateCode: "GENERAL.CODE",
			},
			{
				field: "label",
				translateCode: "GENERAL.LABEL",
			},
		];

		this.dataInfos.dataKey = "ebTypeGoodsNum";
		this.dataInfos.dataType = EbTypeGoods;
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerTypeGoods + "/list-type-goods-table";
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.TYPE_OF_GOODS",
			},
		];
	}

	openAddForm(isUpdate?: boolean) {
		if (isUpdate) {
			this.editMode = true;
		} else {
			this.typeGoods = new EbTypeGoods();
			this.editMode = false;
		}
		this.isAddEditMode = true;
	}

	openEditForm(ebregime: EbTypeGoods) {
		this.typeGoods = ebregime;
		this.backupTypeGoods = Statique.cloneObject<EbTypeGoods>(this.typeGoods, new EbTypeGoods());
		this.openAddForm(true);
	}

	openDeletePopup(typeGoods: EbTypeGoods) {
		let $this = this;

		this.modalService.confirm(
			this.translate.instant("GENERAL.CONFIRMATION"),
			this.translate.instant("GENERAL.CONFIRM_DELETE_ITEM"),
			function() {
				$this.typeGoodsService.deleteTypeGoods(typeGoods.ebTypeGoodsNum).subscribe((data) => {
					$this.genericTable.refreshData();
				});
			},
			function() {},
			true,
			this.translate.instant("GENERAL.CONFIRM"),
			this.translate.instant("GENERAL.CANCEL")
		);
	}

	backToList() {
		if (this.editingIndex != null) {
			Statique.cloneObject<EbTypeGoods>(
				this.backupTypeGoods,
				this.listTypeGoods[this.editingIndex]
			);
			this.editingIndex = null;
		}
		this.backupTypeGoods = null;
		this.isAddEditMode = false;
	}
	editItem(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.typeGoodsService.saveTypeGoods(this.typeGoods).subscribe(
			(data) => {
				requestProcessing.afterGetResponse(event);
				this.isAddEditMode = false;
				this.editingIndex = null;
				this.backupTypeGoods = null;
				this.genericTable.refreshData();
			},
			(error) => {
				console.error(error);
				requestProcessing.afterGetResponse(event);
			}
		);
	}
	addItem(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.typeGoodsService.saveTypeGoods(this.typeGoods).subscribe(
			(data: EbTypeGoods) => {
				requestProcessing.afterGetResponse(event);
				this.isAddEditMode = false;
				this.genericTable.refreshData();
			},
			(error) => {
				console.error(error);
				requestProcessing.afterGetResponse(event);
			}
		);
	}

	onDataLoaded() {
		this.typeGoodsService.listCodeCompany().subscribe((res) => {
			this.listExistingCodes = res;
		});
	}
}
