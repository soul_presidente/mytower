import { Component, OnInit, ViewChild } from "@angular/core";
import { ebFormField } from "@app/classes/ebFormField";
import { EbFormFieldCompagnie } from "@app/classes/ebFormFieldCompagnie";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { DouaneService } from "@app/services/douane.service";
import { CompagnieService } from "@app/services/compagnie.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { ebFormType } from "@app/classes/ebFormType";
import { flow } from "@app/classes/flow";
import { ebFormView } from "@app/classes/EbFormView";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { EbCompagnie } from "@app/classes/compagnie";
import { Panel } from "primeng/panel";
import { HeaderService } from "@app/services/header.service";
import { BreadcumbComponent, HeaderInfos } from "@app/shared/header/header-infos";

@Component({
	selector: "app-custom-setting",
	templateUrl: "./custom-setting.component.html",
	styleUrls: ["./custom-setting.component.scss"],
})
export class CustomSettingComponent extends ConnectedUserComponent implements OnInit {
	Flowbool: boolean = false;
	listype: Array<flow> = new Array<flow>();
	listype2: Array<flow> = new Array<flow>();

	index: number = 0;
	atrDouan: ebFormField = new ebFormField();
	ebFormFieldCompagnie: EbFormFieldCompagnie = new EbFormFieldCompagnie();
	listDouan: Array<ebFormField> = new Array<ebFormField>();
	listDouan1: Array<ebFormField> = new Array<ebFormField>();
	listDouan2: Array<ebFormField> = new Array<ebFormField>();
	listDouan3: Array<ebFormField> = new Array<ebFormField>();
	listFormView: Array<ebFormView> = new Array<ebFormView>();
	checklist: Array<any> = new Array<any>();
	criteria: SearchCriteria = new SearchCriteria();
	cols: any[];
	isCollapsed: boolean = false;
	iconCollapse: string = "pi pi-minus";
	isFirstCollapse: boolean = true;
	@ViewChild("purchasePanel", { static: true })
	purchasePanel: Panel;

	constructor(
		protected douaneService: DouaneService,
		private translate: TranslateService,
		private modalService: ModalService,
		protected compagnieService: CompagnieService,
		protected headerService: HeaderService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		/*Recuperation de la liste complet des formView*/
		this.douaneService.getListView().subscribe((data) => {
			this.listFormView = data;
		});
		this.ebFormFieldCompagnie.xEbCompagnie = new EbCompagnie();
		this.ebFormFieldCompagnie.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		/* list champs pour coompagnie*/
		this.douaneService.getListChampFormForCompagnie(this.criteria).subscribe((res) => {
			this.ebFormFieldCompagnie = res;
			if (this.ebFormFieldCompagnie != null) {
				if (this.ebFormFieldCompagnie.listFormField != null) {
					this.listDouan2 = this.ebFormFieldCompagnie.listFormField;
					this.douaneService.getListChampFormField().subscribe((res) => {
						this.listDouan3 = res;

						this.listconfigCompagnieWithlistGlobal(this.listDouan3, this.listDouan2);
					});
				} else {
					this.douaneService.getListChampFormField().subscribe((res) => {
						this.listDouan3 = res;

						this.listconfigCompagnieWithlistGlobal(this.listDouan3, this.listDouan3);
					});
				}
			}
		});

		this.cols = [
			{ field: "name", header: "FIELDNAME" },
			{ field: "visible", header: "VISIBILITE", width: "10.6%" },
			{ field: "xEbFormType.ebFormTypeName", header: "TYPE" },
			{ field: "ordre", header: "ORDRE", width: "9%" },
			{ field: "required", header: "REQUIRED", width: "10.6%" },
			{ field: "xEbViewForm.ebFormView", header: "VIEW" },
			{ field: "xEbViewForm.xEbFormZone.ebFormName", header: "ZONE" },
		];
	}

	initListAttribut(listDouan2: Array<ebFormField>) {
		//let i = 1;

		for (let formField_copy of listDouan2) {
			let formField: ebFormField = null;
			let FormType: ebFormType = null;
			FormType = new ebFormType();
			formField = new ebFormField();
			let attributview = new ebFormView();
			formField.xEbFormType = FormType;
			formField.ordre = formField_copy.ordre;
			(formField.motTechnique = formField_copy.motTechnique),
				(formField.translate = formField_copy.translate);
			formField.xEbViewForm = attributview;
			formField.valueAvailable = formField_copy.valueAvailable;
			formField.srcListItems = formField_copy.srcListItems;
			formField.fetchAttribute = formField_copy.fetchAttribute;
			formField.fetchValue = formField_copy.fetchValue;
			formField.required = formField_copy.required;

			formField.code = formField_copy.code;
			formField.name = formField_copy.name;
			formField.ebFormFieldNum = formField_copy.ebFormFieldNum;
			//i++;
			for (let en of this.listDouan2) {
				if (en.code == formField_copy.code) {
					formField.visible = true;

					formField.xEbViewForm = en.xEbViewForm;
					this.listDouan1.push(formField);
				}
			}
			this.listDouan.push(formField);
		}
	}

	listconfigCompagnieWithlistGlobal(
		listDouan3: Array<ebFormField>,
		listDouan2: Array<ebFormField>
	) {
		//let i = 1;

		for (let formField_copy of listDouan3) {
			let formField: ebFormField = null;
			let FormType: ebFormType = null;
			FormType = new ebFormType();
			formField = new ebFormField();
			formField.xEbFormType = FormType;
			formField.xEbFormType = formField_copy.xEbFormType;
			formField.xEbViewForm = formField_copy.xEbViewForm;
			formField.ordre = formField_copy.ordre;
			formField.code = formField_copy.code;
			formField.motTechnique = formField_copy.motTechnique;
			formField.translate = formField_copy.translate;
			formField.valueAvailable = formField_copy.valueAvailable;
			formField.srcListItems = formField_copy.srcListItems;
			formField.fetchAttribute = formField_copy.fetchAttribute;
			formField.fetchValue = formField_copy.fetchValue;
			formField.name = formField_copy.name;
			formField.required = formField_copy.required;
			formField.ebFormFieldNum = formField_copy.ebFormFieldNum;
			//i++;
			for (let en of this.listDouan2) {
				if (en.code == formField_copy.code) {
					formField.visible = true;
					formField.xEbFormType = en.xEbFormType;
					formField.ordre = en.ordre;
					formField.xEbFormType.ebFormTypeNum = en.xEbFormType.ebFormTypeNum;
					formField.xEbFormType.ebFormTypeName = en.xEbFormType.ebFormTypeName;
					formField.motTechnique = en.motTechnique;
					formField.translate = en.translate;
					formField.valueAvailable = en.valueAvailable;
					formField.xEbViewForm = en.xEbViewForm;
					formField.srcListItems = en.srcListItems;
					formField.fetchAttribute = en.fetchAttribute;
					formField.fetchValue = en.fetchValue;
					formField.required = en.required;
					this.listDouan1.push(formField);
				}
			}
			this.listDouan.push(formField);
		}
	}

	listadd(event, atr: ebFormField) {
		let i: number;

		if (event.target.checked) {
			this.listDouan1.push(atr);
		}
		if (!event.target.checked) {
			for (i = 0; this.listDouan1.length > i; i++) {
				if (this.listDouan1[i].ebFormFieldNum == atr.ebFormFieldNum) {
					this.listDouan1.splice(i, 1);
					break;
				}
			}
		}
	}

	listaddx(event, atr: ebFormField, index: number) {
		if (event.target.checked) {
			this.listDouan1.push(atr);
		}
		this.douaneService.getListType(atr.xEbFormType).subscribe((res) => {
			this.listype = res;
			this.listype1(this.listype);
		});
	}

	listaddx2(event, atr: ebFormField, index: number) {
		if (event.target.checked) {
			this.listFormView.forEach((it) => {
				if (it.ebFormViewNum == atr.xEbViewForm.ebFormViewNum) {
					atr.xEbViewForm.ebFormView = it.ebFormView;
				}
			});
			if (atr.xEbViewForm.ebFormViewNum && atr.ordre != null) this.listDouan1.push(atr);
		}
	}

	addrequired(event, atr: ebFormField, index: number) {
		if (event.target.checked) atr.required = true;
		else atr.required = false;
	}

	compareFormView(formView1: ebFormView, formView2: ebFormView) {
		if (formView2 !== undefined && formView2 !== null) {
			return formView1.ebFormViewNum === formView2.ebFormViewNum;
		}
	}

	saveListChamp(event: any) {
		let requestProcessing = new RequestProcessing();
		this.ebFormFieldCompagnie.xEbCompagnie = new EbCompagnie();
		if (this.ebFormFieldCompagnie.ebFormFieldCompagnieNum == null) {
			requestProcessing.beforeSendRequest(event);
			this.ebFormFieldCompagnie.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			this.ebFormFieldCompagnie.listFormField = this.listDouan1;
			this.douaneService.saveEbFormFieldCompagnie(this.ebFormFieldCompagnie).subscribe(
				function(res) {
					requestProcessing.afterGetResponse(event);
				}.bind(this),
				(error) => {
					requestProcessing.afterGetResponse(event);
					let title = null;
					let body = null;
					this.translate.get("MODAL.ERROR").subscribe((res: string) => {
						title = res;
					});
					// this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
					this.modalService.error(title, null, function() {});
				}
			);
		} else {
			requestProcessing.beforeSendRequest(event);
			this.ebFormFieldCompagnie.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			this.ebFormFieldCompagnie.listFormField = this.listDouan1;
			this.douaneService.updateEbFormFieldCompagnie(this.ebFormFieldCompagnie).subscribe(
				function(res) {
					requestProcessing.afterGetResponse(event);
				}.bind(this),
				(error) => {
					requestProcessing.afterGetResponse(event);
					let title = null;
					let body = null;
					this.translate.get("MODAL.ERROR").subscribe((res: string) => {
						title = res;
					});
					// this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
					this.modalService.error(title, null, function() {});
				}
			);
		}
	}

	listype1(listype: any) {
		this.listype2 = [];
		for (let entry of listype) {
			this.listype2.push(entry);
		}
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.CUSTOMS",
			},
		];
	}
	changeIcon(event: Event) {
		if (this.isCollapsed && !this.isFirstCollapse) {
			this.isCollapsed = false;
			this.iconCollapse = "pi pi-minus";
		} else if (!this.isCollapsed && !this.isFirstCollapse) {
			this.isCollapsed = true;
			this.iconCollapse = "pi pi-plus";
		}
		if (this.isFirstCollapse) {
			this.isFirstCollapse = false;
		}
	}

	togglePanel(event: Event) {
		this.purchasePanel.toggle(event);
	}
}
