import { Component, EventEmitter, OnInit, Output, Renderer2, ViewChild } from "@angular/core";
import { GenericTableScreen, Modules } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { BoolToYesNoPipe } from "@app/shared/pipes/boolToYesNo.pipe";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";

import { TypeRequestService } from "@app/services/type-request.service";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { StatiqueService } from "@app/services/statique.service";
import { GenericEnum } from "@app/classes/GenericEnum";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";

@Component({
	selector: "app-type-request",
	templateUrl: "./type-request.component.html",
	styleUrls: ["./type-request.component.scss"],
})
export class TypeRequestComponent implements OnInit {
	Module = Modules;
	Statique = Statique;
	@Output()
	onDataChanged = new EventEmitter();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	listTypeRequest: Array<EbTypeRequestDTO>;
	GenericTableScreen = GenericTableScreen;

	isAddTypeRequest = false;
	dataTableConfig: DataTableConfig = new DataTableConfig();
	typeRequest: EbTypeRequestDTO;

	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	listSenseChronoPricing: Array<GenericEnum> = [];
	listBlocageResponsePricing: Array<GenericEnum> = [];

	listActivatedRequest: Array<GenericEnum> = [];

	constructor(
		protected renderer: Renderer2,
		private boolToYesAndNo: BoolToYesNoPipe,
		private translate: TranslateService,
		protected modalService: ModalService,
		protected typeRequestService: TypeRequestService,
		protected statiqueService: StatiqueService,
		protected headerService: HeaderService
	) {}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.initListStatic();
		this.initTypeRequests();
	}

	initTypeRequests() {
		this.dataInfos.cols = [
			{
				field: "reference",
				header: "Reference",
				translateCode: "TYPE_REQUEST.REFERENCE",
			},
			{
				field: "libelle",
				header: "Label",
				translateCode: "TYPE_REQUEST.LABEL",
			},
			{
				field: "delaiChronoPricing",
				header: "Delai",
				translateCode: "TYPE_REQUEST.DELAI_CHRONO_PRICING",
			},

			{
				field: "sensChronoPricing",
				header: "Sense Chrono pricing",
				translateCode: "TYPE_REQUEST.SENSE_CHRONO_PRICING",
				render: (item) => {
					if (item == null) return "";
					return this.translate.instant(
						"TYPE_REQUEST.SENSE_CHRONO_PRICING_VALUES." +
							this.getElementKeyByCode(this.listSenseChronoPricing, item)
					);
				},
			},
			{
				field: "blocageResponsePricing",
				header: "blocage Response Pricing",
				translateCode: "TYPE_REQUEST.BLOCAGE_RESPONSE_PRICING",
				render: (item) => {
					if (item == null) return "";
					return this.translate.instant(
						"TYPE_REQUEST.BLOCAGE_RESPONSE_PRICING_VALUES." +
							this.getElementKeyByCode(this.listBlocageResponsePricing, item)
					);
				},
			},
			{
				field: "dateMaj",
				header: "Date de mise à jour",
				translateCode: "TYPE_REQUEST.DATE_MAJ",
				render: (item: Date) => {
					return item != null ? Statique.formatDate(item) : "";
				},
			},
			{
				field: "activated",
				header: "Activated",
				translateCode: "TYPE_REQUEST.ACTIVATED",
				render: (item) => {
					if (item == null) return "";
					return this.translate.instant(
						"GENERAL." + this.getElementKeyByCode(this.listActivatedRequest, item)
					);
				},
			},
		];
		this.dataInfos.dataKey = "ebTypeRequestNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerTypeRequest + "/list-type-request-table";
		this.dataInfos.dataType = EbTypeRequestDTO;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
	}

	diplayEditTypeRequest(data: EbTypeRequestDTO) {
		this.typeRequest = Statique.cloneObject(data, new EbTypeRequestDTO());
		this.displayAddTypeRequest(true);
	}

	displayAddTypeRequest(isUpdate?: boolean) {
		if (isUpdate) {
		} else this.typeRequest = new EbTypeRequestDTO();
		this.isAddTypeRequest = true;
	}

	backToListTypeRequests() {
		this.isAddTypeRequest = false;
	}

	addTypeRequest(isUpdate) {
		this.typeRequest = Statique.cloneObject(this.typeRequest, new EbTypeRequestDTO());
		this.typeRequestService.addTypeRequest(this.typeRequest).subscribe(
			function(data) {
				this.typeRequest = null;
				this.genericTable.refreshData();
				this.isAddTypeRequest = false;
				this.onDataChanged.next();
			}.bind(this)
		);
	}

	activatedTypeRequest(data: EbTypeRequestDTO) {
		let $this = this;
		let activatedMessage = null;
		let activatedHeader = null;

		if (data.activated) {
			activatedHeader = this.translate.instant("TYPE_REQUEST.HEADER_DEACTIVATE");
			activatedMessage = this.translate.instant("TYPE_REQUEST.MESSAGE_DEACTIVATE");
		} else {
			activatedHeader = this.translate.instant("TYPE_REQUEST.HEADER_ACTIVATED");
			activatedMessage = this.translate.instant("TYPE_REQUEST.MESSAGE_ACTIVATED");
		}

		this.modalService.confirm(
			activatedHeader,
			activatedMessage,
			function() {
				if (data.activated) data.activated = false;
				else data.activated = true;
				$this.typeRequestService.addTypeRequest(data).subscribe((data) => {
					$this.genericTable.refreshData();
					$this.onDataChanged.next();
				});
			},
			function() {},
			true
		);
	}

	compareUser(user1: any, user2: any) {
		if (user1 && user2) {
			var user1id = user1.ebUserNum ? user1.ebUserNum : user1;
			var user2id = user2.ebUserNum ? user2.ebUserNum : user2;

			return user1id === user2id;
		}
		return false;
	}

	changeModel(form: any) {
		setTimeout(
			function() {
				this.checkFields(form);
			}.bind(this),
			10
		);
	}
	// getElementById(elemNum):EbTypeDocumentsDTO{
	//   this.listTypeDocuments.forEach(el=>{
	//     if(el.ebTypeDocumentsNum ===  elemNum)
	//       return el;
	//   })
	//   return null;
	// }

	rerenderTable() {}

	onDataLoaded(data: Array<EbTypeRequestDTO>) {
		this.onDataChanged.emit(data);
	}

	initListStatic() {
		this.listSenseChronoPricing = Statique.getListSenseChronoPricing();
		this.listBlocageResponsePricing = Statique.getListBlocageResponsePricing();
		this.listActivatedRequest = Statique.getListActivatedRequest();
	}

	getElementKeyByCode(list: Array<any>, code: any): String {
		let element: string = null;
		list.forEach((elem) => {
			if (elem.code === code) element = elem.key;
		});
		return element;
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.TYPE_OF_REQUEST",
			},
		];
	}
}
