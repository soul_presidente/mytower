import { Component, OnInit, ViewChild } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { EbPlTrancheDTO } from "@app/classes/trpl/EbPlTrancheDTO";
import { EbPlGrilleTransportDTO } from "@app/classes/trpl/EbPlGrilleTransportDTO";
import { EbUser } from "@app/classes/user";
import { EbPlTransportDTO } from "@app/classes/trpl/EbPlTransportDTO";
import { TranchesComponent } from "../tranches/tranches.component";
import { ZoneComponent } from "../zone/zone.component";
import { PlanTransportComponent } from "../plan-transport/plan-transport.component";
import { GrilleTransportComponent } from "../grille-transport/grille-transport.component";
import { UserService } from "@app/services/user.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { AuthenticationService } from "@app/services/authentication.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { TypesDocumentService } from "@app/services/types-document.service";
import { ActivatedRoute } from "@angular/router";
import { EbCompagnie } from "@app/classes/compagnie";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";

@Component({
	selector: "app-global-transportion-plan",
	templateUrl: "./global-transportion-plan.component.html",
	styleUrls: ["./global-transportion-plan.component.scss"],
})
export class GlobalTransportionPlanComponent extends ConnectedUserComponent implements OnInit {
	// Transportation plan lists
	listZone: Array<EbPlZoneDTO> = new Array<EbPlZoneDTO>();
	listZoneForPlanTransport: Array<EbPlZoneDTO> = new Array<EbPlZoneDTO>();
	listTranche: Array<EbPlTrancheDTO> = new Array<EbPlTrancheDTO>();
	listGrille = new Array<EbPlGrilleTransportDTO>();
	listTransporteur: Array<EbUser> = new Array<EbUser>();
	listPlan: Array<EbPlTransportDTO> = new Array<EbPlTransportDTO>();
	sharedDropDownZIndex: number = 2;

	@ViewChild("trancheComponent", { static: false })
	tranchesComponent: TranchesComponent;
	@ViewChild(ZoneComponent, { static: false })
	zoneComponent: ZoneComponent;
	@ViewChild("planTransportComponent", { static: false })
	planTransportComponent: PlanTransportComponent;
	@ViewChild("grilleComponent", { static: false })
	grilleTransportComponent: GrilleTransportComponent;

	planTableVisible = false;

	constructor(
		protected userService?: UserService,
		protected etablissementService?: EtablissementService,
		protected ngbService?: NgbModal,
		protected authenticationService?: AuthenticationService,
		protected transportationPlanService?: TransportationPlanService,
		protected typesDocumentService?: TypesDocumentService,
		protected activatedRoute?: ActivatedRoute,
		protected headerService?: HeaderService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
	}

	async refreshListPlan(callback?: () => any) {
		if (this.planTransportComponent && this.planTransportComponent.genericTable) {
			this.planTransportComponent.initPlanTransport();
			this.planTransportComponent.genericTable.refreshData();

			this.listPlan.forEach((p) => {
				// Fix for the ebuser not always serialized
				if (
					p.grilleTarif != null &&
					p.grilleTarif.transporteur != null &&
					!p.grilleTarif.transporteur.ebUserNum
				) {
					p.grilleTarif.transporteur = this.findTransporteurById(p.grilleTarif.transporteur);
				}

				// Fix etablissement compagnie displayed as number instead of object
				if (
					p.grilleTarif != null &&
					p.grilleTarif.transporteur != null &&
					p.grilleTarif.transporteur.ebEtablissement &&
					p.grilleTarif.transporteur.ebEtablissement.ebCompagnie &&
					!p.grilleTarif.transporteur.ebEtablissement.ebCompagnie.ebCompagnieNum
				) {
					var ebEtablissementCompagnieNum: any =
						p.grilleTarif.transporteur.ebEtablissement.ebCompagnie;
					p.grilleTarif.transporteur.ebEtablissement.ebCompagnie = new EbCompagnie();
					p.grilleTarif.transporteur.ebEtablissement.ebCompagnie.ebCompagnieNum = ebEtablissementCompagnieNum;
				}
			});

			if (callback) callback();
		}
	}

	private async refreshListGrille(callback?: () => any) {
		if (this.grilleTransportComponent) {
			this.grilleTransportComponent.initGenericTable();
			this.grilleTransportComponent.genericTable &&
				this.grilleTransportComponent.genericTable.refreshData();

			if (callback) callback();
		}
	}

	private refreshListTransportreur(callback?: () => any) {}

	onZoneChanged(data: Array<EbPlZoneDTO>) {
		var $this = this;
		$this.listZone = data;
		$this.refreshListGrille();
		$this.refreshListPlan();

		if ($this.isControlTower) {
			this.transportationPlanService.listZoneWithCommunity().subscribe((res) => {
				$this.listZoneForPlanTransport = res;
			});
		} else {
			$this.listZoneForPlanTransport = $this.listZone;
		}
	}

	onTrancheChanged(data: Array<EbPlTrancheDTO>) {
		var $this = this;
		if (data) {
			$this.listTranche = data.sort((a, b) => a.ebTrancheNum - b.ebTrancheNum);
			$this.refreshListGrille();
		}
	}

	async onGrilleChanged(data: Array<EbPlGrilleTransportDTO>) {
		this.listGrille = data;
		await this.refreshListPlan();
		this.planTableVisible = true;
	}

	async onPlanChanged(data: Array<EbPlTransportDTO>) {
		this.listPlan = data;
		await this.getAllGrille();
	}

	getAllGrille(){
		this.transportationPlanService.listGrille().subscribe((res) => {
			this.listGrille =  res;
		});
	}
	private findTransporteurById(transporteurId: any): EbUser {
		var rValue = transporteurId;

		this.listTransporteur.forEach((t) => {
			if (t.ebUserNum === transporteurId) {
				rValue = t;
			}
		});

		return rValue;
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.TRPL",
			},
		];
	}

	sharedDropDownZIndexChange() {
		this.sharedDropDownZIndex += 1;
	}
}
