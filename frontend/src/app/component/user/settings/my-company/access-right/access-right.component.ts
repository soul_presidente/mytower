import {
	Component,
	EventEmitter,
	Output,
	Input,
	OnChanges,
	SimpleChanges,
	SimpleChange,
	OnInit,
} from "@angular/core";
import { StatiqueService } from "@app/services/statique.service";
import { EcModule } from "@app/classes/EcModule";
import { AccessRights } from "@app/utils/enumeration";
import { TranslateService } from "@ngx-translate/core";
@Component({
	selector: "app-access-right",
	templateUrl: "./access-right.component.html",
	styleUrls: ["./access-right.component.scss"],
})
export class AccessRightComponent implements OnInit, OnChanges {
	@Input()
	importedAccessRights: any[];
	@Input()
	accessRightsSpinner?: boolean;
	@Input()
	disabled: boolean;
	@Input()
	displayAdvancedSettings: boolean = false;
	@Output()
	accessRightsEmitter = new EventEmitter<number[]>();
	@Output()
	advancedSettingsClick = new EventEmitter<void>();

	listModule: EcModule[] = [];
	accessRightsTypes = AccessRights;
	modulesAndTheirAccessRights: number[] = [];
	constructor(protected statiqueService: StatiqueService, private translate: TranslateService) {}
	ngOnInit() {
		this.statiqueService.getListModule().subscribe((data) => {
			this.listModule = data;
			this.initModulesAceessRights();
			this.applyTheExistingAccessRights();
		});
	}
	ngOnChanges(changes: SimpleChanges) {
		if (
			changes.importedAccessRights &&
			changes.importedAccessRights.previousValue != changes.importedAccessRights.currentValue
		) {
			this.applyTheExistingAccessRights();
		}
	}

	initModulesAceessRights() {
		this.listModule.forEach((m) => {
			this.modulesAndTheirAccessRights[m.ecModuleNum] = this.accessRightsTypes.NON_VISIBLE;
		});
	}

	applyTheExistingAccessRights() {
		if (this.importedAccessRights && this.importedAccessRights.length > 0) {
			this.importedAccessRights.forEach((existingAccessRight) => {
				if (
					existingAccessRight &&
					(typeof existingAccessRight == "object" &&
						existingAccessRight.hasOwnProperty("accessRight") &&
						existingAccessRight.hasOwnProperty("ecModuleNum"))
				) {
					this.modulesAndTheirAccessRights[existingAccessRight.ecModuleNum] =
						existingAccessRight.accessRight;
				}
			});
		}
	}

	onAccessRightChange() {
		this.accessRightsEmitter.emit(this.modulesAndTheirAccessRights);
	}
}
