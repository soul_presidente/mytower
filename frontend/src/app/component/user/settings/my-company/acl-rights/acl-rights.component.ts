import { Component, OnInit, ViewContainerRef, HostListener } from "@angular/core";
import { _MyCompanyComponent } from "../my-company.component";
import { UserService } from "@app/services/user.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EtablissementService } from "@app/services/etablissement.service";
import { AuthenticationService } from "@app/services/authentication.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { TypesDocumentService } from "@app/services/types-document.service";
import { ActivatedRoute, Router } from "@angular/router";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { EbAclSettingDTO } from "@app/classes/dto/EbAclSettingDTO";
import { AclService } from "@app/services/acl.service";
import { Observable } from "rxjs";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbUser } from "@app/classes/user";
import { UserProfileService } from "@app/services/user-profile.service";
import { EbUserProfile } from "@app/classes/userProfile";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { MessageService } from "primeng/api";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-acl-rights",
	templateUrl: "./acl-rights.component.html",
	styleUrls: ["./acl-rights.component.scss"],
})
export class AclRightsComponent extends _MyCompanyComponent implements OnInit {
	loading: boolean = true;
	homeUrl: string = "/accueil";
	aclSettings: Array<EbAclSettingDTO> = [];
	userId: number = null;
	userProfileId: number = null;

	// Add ?experimental=true to url to set this at true
	showExperimental = false;
	canShow: boolean = false;

	// Hidden feature to show experimental ACL when click 5 times
	timesClickedHeader = 0;

	constructor(
		protected userService: UserService,
		protected vcr: ViewContainerRef,
		protected etablissementService: EtablissementService,
		protected ngbService: NgbModal,
		protected authenticationService: AuthenticationService,
		protected transportationPlanService: TransportationPlanService,
		protected typesDocumentService: TypesDocumentService,
		public activatedRoute: ActivatedRoute,
		protected headerService: HeaderService,
		protected aclService: AclService,
		protected userProfileService: UserProfileService,
		protected messageService: MessageService,
		protected translate: TranslateService,
		public router: Router
	) {
		super(
			userService,
			vcr,
			etablissementService,
			ngbService,
			authenticationService,
			transportationPlanService,
			typesDocumentService,
			activatedRoute,
			headerService
		);
	}

	ngOnInit() {
		this.loadParamsFromRouterUrl(() => this.loadUrlParams(() => this.loadData()));
	}

	private loadUrlParams(callback?: () => void) {
		this.activatedRoute.queryParamMap.subscribe((queryParams) => {
			let experimentalParam = queryParams.get("experimental");
			this.showExperimental = experimentalParam === "true";

			if (callback) {
				callback();
			}
		});
	}

	private loadParamsFromRouterUrl(callback?: () => void) {
		this.activatedRoute.params.subscribe((routeParams) => {
			this.userId = routeParams["ebUserNum"] != null ? +routeParams["ebUserNum"] : null;
			this.userProfileId =
				routeParams["ebUserProfileNum"] != null ? +routeParams["ebUserProfileNum"] : null;
			if (this.userId && (this.isSuperAdmin || this.isAdmin || this.isControlTower)) {
				this.userService.canEditUser(this.userId).subscribe((hasAccess) => {
					if (hasAccess) {
						this.canShow = true;
						if (callback) {
							callback();
						}
					} else {
						this.router.navigateByUrl(this.homeUrl);
						this.showAccessNotGranted();
					}
				});
			} else if (this.userProfileId && (this.isSuperAdmin || this.isControlTower)) {
				this.userService.canEditProfil(this.userProfileId).subscribe((hasAccess) => {
					if (hasAccess) {
						this.canShow = true;
						if (callback) {
							callback();
						}
					} else {
						this.router.navigateByUrl(this.homeUrl);
						this.showAccessNotGranted();
					}
				});
			} else {
				this.router.navigateByUrl(this.homeUrl);
				this.showAccessNotGranted();
			}
		});
	}

	private showAccessNotGranted() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("ACL.TOAST_MESSAGE.ERROR_MESSAGE"),
			detail: this.translate.instant("ACL.TOAST_MESSAGE.ACCESS_NOT_GRANTED"),
		});
	}

	private loadData() {
		this.aclSettings = [];

		// Fire loading of acl
		let observableToCall: Observable<Array<EbAclSettingDTO>>;
		if (this.userProfileId != null) {
			observableToCall = this.aclService.listSettingsForProfile(this.userProfileId);
		} else if (this.userId != null) {
			observableToCall = this.aclService.listSettingsForUser(this.userId);
		}
		observableToCall.subscribe((res) => {
			if (this.showExperimental) {
				this.aclSettings = res;
			} else {
				this.aclSettings = res.filter((r) => r.experimental !== true);
			}
			this.loading = false;
			this.onResizeAdapter();
		});

		// Fire load of breadcrumb infos
		let breadcrumbItems = this.getBaseBreadcrumbItems();

		if (this.userId != null) {
			// Retrieve user nom prenom to display on breadcrumb
			breadcrumbItems.push({
				label: "SETTINGS.USERS_MANAGEMENT",
				path: "/app/user/settings/company/users",
			});

			let criteria = new SearchCriteria();
			criteria.ebUserNum = this.userId;
			this.userService.getUserInfo(criteria).subscribe((user: EbUser) => {
				let userBreacrumbItem: HeaderInfos.BreadCrumbItem = {
					label: user.nomPrenom,
					path: "/app/user/settings/company/users/" + this.userId,
				};
				breadcrumbItems.push(userBreacrumbItem);
				breadcrumbItems.push({
					label: "ACL.SETTINGS.BREADCRUMB_TITLE",
				});
				this.headerService.registerBreadcrumbItems(breadcrumbItems, BreadcumbComponent.MY_COMPANY);
			});
		} else if (this.userProfileId != null) {
			breadcrumbItems.push({
				label: "SETTINGS.PROFILES_MANAGEMENT",
				path: "/app/user/settings/company/profiles-management",
			});

			// Retrieve profile name to display on breadcrumb
			let criteria = new SearchCriteria();
			criteria.ebUserProfileNum = this.userProfileId;
			this.userProfileService.getDetailUser(criteria).subscribe((profile: EbUserProfile) => {
				let userProfileBreacrumbItem: HeaderInfos.BreadCrumbItem = {
					label: profile.nom,
					path: "/app/user/settings/company/profiles-management/", // Curently no url available for details profile, but add it here when done
				};
				breadcrumbItems.push(userProfileBreacrumbItem);
				breadcrumbItems.push({
					label: "ACL.SETTINGS.BREADCRUMB_TITLE",
				});
				this.headerService.registerBreadcrumbItems(breadcrumbItems, BreadcumbComponent.MY_COMPANY);
			});
		}
	}

	// Called when click on title, used by hidden feature activated on multiple click on it
	public clickHeader() {
		// Hidden feature, show experimental ACL when click 5 times
		if (this.timesClickedHeader < 4) {
			this.timesClickedHeader++;
		} else if (this.timesClickedHeader == 4) {
			this.timesClickedHeader++;
			this.switchToExperimental();
		}
	}

	public switchToExperimental() {
		this.showExperimental = true;
		this.loadData();
	}

	private getBaseBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
		];
	}

	save(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.aclService
			.saveSettings(this.prepareListToSend(), this.userProfileId, this.userId)
			.subscribe(
				(res) => {
					requestProcessing.afterGetResponse(event);
					this.messageService.add({
						severity: "success",
						summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
						detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
					});
				},
				(error) => {
					requestProcessing.afterGetResponse(event);
					this.messageService.add({
						severity: "error",
						summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
						detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE"),
					});
				}
			);

		//requestProcessing.afterGetResponse(event);
	}

	prepareListToSend(): Array<EbAclSettingDTO> {
		return this.aclSettings.filter((acl) => acl.selectedValue != null);
	}

	borderColorACL(acl): string {
		if (acl.selectedValue != null && acl.selectedValue != "null") {
			return "selected-value";
		} else {
			return "";
		}
	}

	@HostListener("window:resize", ["$event"])
	onResizeAdapter() {
		let windowHeight = window.innerHeight;
		let listEltsClass: string[] = [
			".app-header",
			".head-tabs",
			".actionbar-wrapper",
			".ui-dataview-header",
			".ui-dataview-footer",
		];
		let padding = 55;
		setTimeout(() => {
			this.Statique.onResizeAdapter(listEltsClass, ".ui-dataview-content", padding, windowHeight);
		}, 200);
	}
}
