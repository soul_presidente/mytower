import { Component, OnInit, OnChanges, Input } from "@angular/core";
import { TransfertUserObject } from "@app/classes/transfertUserobject";
import { EbUser } from "@app/classes/user";
import { TransfertActionChoice } from "@app/utils/enumeration";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-affect-transports",
	templateUrl: "./affect-transports.component.html",
	styleUrls: ["./affect-transports.component.scss"],
})
export class AffectTransportsComponent implements OnInit, OnChanges {
	@Input()
	transfertUserObject: TransfertUserObject;

	@Input()
	listActionChoose: Array<any> = [];
	TransfertActionChoice: TransfertActionChoice;
	isCategoriesLoaded: boolean = false;

	@Input()
	userToBeUpdated: EbUser;

	constructor(protected translate: TranslateService) {}

	ngOnInit() {
		this.checkIfLoadingIsOver();
	}

	ngOnChanges() {
		this.checkIfLoadingIsOver();
	}

	checkIfLoadingIsOver() {
		if (this.listActionChoose.length > 0) this.isCategoriesLoaded = true;
	}

	affecteTransportAction() {
		if (this.transfertUserObject.transportFiles.action == TransfertActionChoice.CHOOSE) {
		}
	}

	getTranslateByCode(code) {
		return this.translate.instant("TRANSFERT_ESTABLISHMENT." + code);
	}
}
