import { Component, OnInit, HostListener, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { MessageService } from "primeng/api";
import { TranslateService } from "@ngx-translate/core";
import { HeaderService } from "@app/services/header.service";
import { CompagnieService } from "@app/services/compagnie.service";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { UserService } from "@app/services/user.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbUser } from "@app/classes/user";
import { EbEtablissement } from "@app/classes/etablissement";
import { Statique } from "@app/utils/statique";
import { TransfertComponent } from "@app/classes/transfertComponent";
import { TransfertUserObject } from "@app/classes/transfertUserobject";
import { EbCategorie } from "@app/classes/categorie";
import { TransfertActionChoice } from "@app/utils/enumeration";
import { EtablissementService } from "@app/services/etablissement.service";
import { EbCompagnie } from "@app/classes/compagnie";
import { GenericTableService } from "@app/services/generic-table.service";
import { Order } from "@app/classes/ebDelOrder";

@Component({
	selector: "app-transfert-etablissement",
	templateUrl: "./transfert-etablissement.component.html",
	styleUrls: ["./transfert-etablissement.component.scss"],
})
export class TransfertEtablissementComponent implements OnInit {
	userToBeUpdated: EbUser;
	userId: number = null;
	loading: boolean;
	listEtablissements: Array<EbEtablissement>;
	ebEtablissement: EbEtablissement;
	Statique = Statique;
	isFormValid: boolean = false;
	transfertComponent: TransfertComponent;
	transfertUserObject: TransfertUserObject;
	affectedCategoriesAndLabels: Array<EbCategorie> = new Array<EbCategorie>();
	selectedEtablissement: EbEtablissement;
	suggestedEtablissements: Array<EbEtablissement> = new Array<EbEtablissement>();
	aIndex: number = 0;
	listActionChoose: Array<any> = [];
	TransfertActionChoice: TransfertActionChoice;
	orders: Array<Order>;

	constructor(
		protected userService: UserService,
		public activatedRoute: ActivatedRoute,
		protected messageService: MessageService,
		protected headerService: HeaderService,
		protected compagnieService: CompagnieService,
		protected etablissementService: EtablissementService,
		protected translate: TranslateService,
		private genericTableService: GenericTableService
	) {}

	ngOnInit() {
		this.transfertComponent = new TransfertComponent();
		this.transfertUserObject = new TransfertUserObject();
		this.transfertComponent.categories = true;
		this.loadParamsFromRouterUrl();
		this.getListActionChoose();
	}

	getListActionChoose() {
		this.compagnieService.getListActionChoose().subscribe((data: any) => {
			this.listActionChoose = data;
		});
	}

	selectedAction($event) {
		this.transfertUserObject.orders.action = $event;
	}

	selectedCommunityAction($event) {
		this.transfertUserObject.community.action = $event;
	}

	private loadParamsFromRouterUrl() {
		this.activatedRoute.params.subscribe((routeParams) => {
			this.userId = routeParams["ebUserNum"] != null ? +routeParams["ebUserNum"] : null;

			if (this.userId != null) {
				this.getBaseBreadCrumbItems();
			}
		});
	}

	selectComponent(component) {
		this.transfertComponent = new TransfertComponent();
		this.transfertComponent[component] = true;
	}

	onChangeEtablissement() {
		this.transfertUserObject.newEtablissement = this.ebEtablissement.ebEtablissementNum;
		this.checkValidity();
	}

	checkValidity() {
		this.isFormValid = false;
		this.isFormValid = this.transfertUserObject.newEtablissement != null;
		if (this.transfertUserObject.orders.action == TransfertActionChoice.CHOOSE) {
			this.isFormValid = this.isFormValid && this.transfertUserObject.orders.listId.length > 0;
		}
		if (this.transfertUserObject.transportFiles.action == TransfertActionChoice.CHOOSE) {
			this.isFormValid =
				this.isFormValid && this.transfertUserObject.transportFiles.listId.length > 0;
		}
	}

	save() {
		this.etablissementService
			.transfertUserEtablissement(this.transfertUserObject)
			.subscribe((res) => {
				this.getUserCompagieListEtablissemnt();
				this.transfertUserObject.orders.action = TransfertActionChoice.NONE;
			});
	}

	private loadData() {
		this.onResizeAdapter();
		this.getUserCompagieListEtablissemnt();
	}

	private getUserCompagieListEtablissemnt() {
		let criteriaListEtab = new SearchCriteria();
		criteriaListEtab.ebCompagnieNum = this.userToBeUpdated.ebCompagnie.ebCompagnieNum;
		criteriaListEtab.withEtablissementDetail = true;
		this.compagnieService.getListEtablisementCompagnie(criteriaListEtab).subscribe((data) => {
			this.listEtablissements = data.filter(
				(res) => res.ebEtablissementNum !== this.userToBeUpdated.ebEtablissement.ebEtablissementNum
			);
		});
	}

	affecteUserData(user: EbUser) {
		this.userToBeUpdated = user;
		this.transfertUserObject.ebUserNum = user.ebUserNum;
		this.transfertUserObject.oldEtablissement = user.ebEtablissement.ebEtablissementNum;
		this.transfertUserObject.oldListCategories = user.listCategories;
		this.transfertUserObject.oldListLabels = user.listLabels;
	}

	private getBaseBreadCrumbItems() {
		let breadcrumbItems = new Array<HeaderInfos.BreadCrumbItem>();
		breadcrumbItems.push({ label: "SETTINGS.MY_COMPANY", path: "/app/user/settings/company" });
		breadcrumbItems.push({
			label: "SETTINGS.USERS_MANAGEMENT",
			path: "/app/user/settings/company/users",
		});

		let criteria = new SearchCriteria();
		criteria.ebUserNum = this.userId;
		criteria.withEtablissement = true;
		this.userService.getUserInfo(criteria).subscribe((user: EbUser) => {
			this.affecteUserData(user);
			let userBreacrumbItem: HeaderInfos.BreadCrumbItem = {
				label: this.userToBeUpdated.nomPrenom,
				path: "/app/user/settings/company/users/" + this.userId,
			};
			breadcrumbItems.push(userBreacrumbItem);
			breadcrumbItems.push({
				label: this.translate.instant("TRANSFERT_ESTABLISHMENT.TITLE"),
			});
			this.headerService.registerBreadcrumbItems(breadcrumbItems, BreadcumbComponent.MY_COMPANY);
			this.loadData();
		});
	}

	@HostListener("window:resize", ["$event"])
	onResizeAdapter() {
		let windowHeight = window.innerHeight;
		let listEltsClass: string[] = [
			".app-header",
			".head-tabs",
			".actionbar-wrapper",
			".etab-header",
			".etab-footer",
		];
		let padding = 55;
		setTimeout(() => {
			this.Statique.onResizeAdapter(listEltsClass, ".etab-content", padding, windowHeight);
		}, 200);
	}
}
