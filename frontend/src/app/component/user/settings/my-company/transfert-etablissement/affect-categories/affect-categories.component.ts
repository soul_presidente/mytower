import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from "@angular/core";
import { TransfertUserObject } from "@app/classes/transfertUserobject";
import { EbCategorie } from "@app/classes/categorie";
import { CompagnieService } from "@app/services/compagnie.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbUser } from "@app/classes/user";
import { EbCompagnie } from "@app/classes/compagnie";
import { Statique } from "@app/utils/statique";
import { EbLabel } from "@app/classes/label";

@Component({
	selector: "app-affect-categories",
	templateUrl: "./affect-categories.component.html",
	styleUrls: ["./affect-categories.component.scss"],
})
export class AffectCategoriesComponent implements OnInit, OnChanges {
	@Input()
	affectedCategoriesAndLabels: Array<EbCategorie>;
	@Output()
	affectedCategoriesAndLabelsChange = new EventEmitter();

	@Input()
	userToBeUpdated: EbUser;

	isEdit: boolean = true;
	showDrtUser: boolean = true;
	disableDrtUser: boolean = false;

	listCategories: Array<EbCategorie> = new Array<EbCategorie>();
	affectedCategories: Array<EbCategorie> = new Array<EbCategorie>();
	isCategoriesLoaded: boolean = false;

	constructor(private compagnieService: CompagnieService) {}

	ngOnInit() {
		this.getCategoriesLabels();
	}

	ngOnChanges() {
		this.getCategoriesLabels();
	}

	getCategoriesLabels() {
		let searchCriteria: SearchCriteria = new SearchCriteria();
		if (Statique.isDefined(this.userToBeUpdated)) {
			searchCriteria.ebCompagnieNum = this.userToBeUpdated.ebCompagnie.ebCompagnieNum;

			this.compagnieService.getCompagnie(searchCriteria).subscribe((data: EbCompagnie) => {
				if (data.categories) {
					data.categories = data.categories.sort(
						(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
					);
				}

				this.listCategories = Statique.cloneObject(data.categories);

				this.getAffectedCategorieLabels();
			});
		}
	}

	getAffectedCategorieLabels() {
		if (this.userToBeUpdated.listCategories) {
			let listOrgCategorie = Statique.cloneObject(this.listCategories, new Array<EbCategorie>());

			// categories/labels reellement affecté au user
			this.affectedCategoriesAndLabels = EbCategorie.getCopyOfListCategorie(
				this.userToBeUpdated.listCategories
			);
			let newAffectedCategoriesAndLabels = new Array<EbCategorie>();
			this.affectedCategories = listOrgCategorie.filter((it) => {
				let cat = this.affectedCategoriesAndLabels.find((ct) => {
					return ct.ebCategorieNum == it.ebCategorieNum;
				});
				if (cat) {
					let newCat: EbCategorie = (function(nc): EbCategorie {
						return JSON.parse(JSON.stringify(nc));
					})(cat);
					newCat.labels = new Array<EbLabel>();
					newAffectedCategoriesAndLabels.push(newCat);
					return true;
				}
			});
			// this.listCategories: categories restantes (contenant tous les labels)
			this.listCategories = listOrgCategorie.filter((it) => {
				let cat = this.affectedCategoriesAndLabels.find((ct) => {
					return ct.ebCategorieNum == it.ebCategorieNum;
				});
				if (!cat) return true;
			});
			// this.affectedCategories: categories affectés avec les labels restants
			this.affectedCategories.forEach((category) => {
				let cat = this.affectedCategoriesAndLabels.find((ct) => {
					return ct.ebCategorieNum == category.ebCategorieNum;
				});

				category.labels = category.labels.filter((label) => {
					let lab = cat.labels.find((lb) => {
						return lb.ebLabelNum == label.ebLabelNum;
					});
					if (!lab) return true;
					else {
						let newCat = newAffectedCategoriesAndLabels.find(
							(it) => it.ebCategorieNum == cat.ebCategorieNum
						);
						let newLabel: EbLabel = (function(nl): EbLabel {
							return JSON.parse(JSON.stringify(nl));
						})(label);
						newCat.labels.push(newLabel);
					}
				});
			});

			this.affectedCategoriesAndLabels = newAffectedCategoriesAndLabels;
		}
		this.isCategoriesLoaded = true;
	}

	setUserCategoriesLabels(event) {
		this.affectedCategoriesAndLabelsChange.emit(this.affectedCategoriesAndLabels);
	}
}
