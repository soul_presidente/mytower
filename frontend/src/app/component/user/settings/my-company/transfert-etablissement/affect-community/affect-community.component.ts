import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { TransfertUserObject } from "@app/classes/transfertUserobject";
import { EbUser } from "@app/classes/user";
import { TransfertActionChoice } from '@app/utils/enumeration';
import { TranslateService } from '@ngx-translate/core';

@Component({
	selector: "app-affect-community",
	templateUrl: "./affect-community.component.html",
	styleUrls: ["./affect-community.component.scss"],
})
export class AffectCommunityComponent implements OnInit {
	
	@Input()
	transfertUserObject: TransfertUserObject;

	@Input()
	listActionChoose: Array<any> = [];

	TransfertActionChoice: TransfertActionChoice;

	isCategoriesLoaded: boolean = false;

	@Input()
	userToBeUpdated: EbUser;


	@Output() action = new EventEmitter<number>();

	constructor(protected translate: TranslateService) {}

	ngOnInit() {
		this.transfertUserObject = new TransfertUserObject();
		this.checkIfLoadingIsOver();
	}

	ngOnChanges() {
		this.checkIfLoadingIsOver();
	}

	checkIfLoadingIsOver() {
		if (this.listActionChoose.length > 0) this.isCategoriesLoaded = true;
	}

	affecteCommunityAction() {

		if (this.transfertUserObject.community.action == TransfertActionChoice.ALL) {
			this.action.emit(this.transfertUserObject.community.action);
		}
	}

	getTranslateByCode(code) {
		return this.translate.instant("TRANSFERT_ESTABLISHMENT." + code);
	}
}
