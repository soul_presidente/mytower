import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { EbUser } from "@app/classes/user";
import { TransfertActionChoice } from "@app/utils/enumeration";
import { TransfertUserObject } from "@app/classes/transfertUserobject";
import { Order } from "@app/classes/ebDelOrder";

@Component({
	selector: "app-affect-orders",
	templateUrl: "./affect-orders.component.html",
	styleUrls: ["./affect-orders.component.scss"],
})
export class AffectOrdersComponent implements OnInit {
	@Input()
	transfertUserObject: TransfertUserObject;

	@Input()
	listActionChoose: Array<any> = [];

	TransfertActionChoice: TransfertActionChoice;

	isCategoriesLoaded: boolean = false;

	@Input()
	userToBeUpdated: EbUser;

	orders: Array<Order>;

	@Output() action = new EventEmitter<number>();

	constructor(protected translate: TranslateService) {}

	ngOnInit() {
		this.transfertUserObject = new TransfertUserObject();
		this.checkIfLoadingIsOver();
	}

	ngOnChanges() {
		this.checkIfLoadingIsOver();
	}

	checkIfLoadingIsOver() {
		if (this.listActionChoose.length > 0) this.isCategoriesLoaded = true;
	}

	affecteTransportAction() {
		if (this.transfertUserObject.orders.action == TransfertActionChoice.ALL) {
			this.action.emit(this.transfertUserObject.orders.action);
		}
	}

	getTranslateByCode(code) {
		return this.translate.instant("TRANSFERT_ESTABLISHMENT." + code);
	}
}
