import { Component, EventEmitter, OnInit, Input, Output, ViewChild } from "@angular/core";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { TransportationPlanService } from "@app/services/transportation-plan.service";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableScreen, Modules } from "@app/utils/enumeration";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";

@Component({
	selector: "app-trpl-zone",
	templateUrl: "./zone.component.html",
	styleUrls: ["./zone.component.scss"],
})
export class ZoneComponent implements OnInit {
	Module = Modules;

	listZone: Array<EbPlZoneDTO> = new Array<EbPlZoneDTO>();
	allList: Array<EbPlZoneDTO> = new Array<EbPlZoneDTO>();
	@Output()
	onZoneChanged = new EventEmitter<any>();
	@Input()
	sharedDropDownZIndex: number;
	@Output()
	sharedDropDownZIndexEmit: EventEmitter<any> = new EventEmitter<any>();
	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	isAddZone = false;
	zone: EbPlZoneDTO = new EbPlZoneDTO();
	backupEditZone: EbPlZoneDTO;
	editingIndex: number;
	deleteMessage = "";
	deleteHeader = "";
	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;

	constructor(
		private translate?: TranslateService,
		protected modalService?: ModalService,
		protected transportationPlanService?: TransportationPlanService
	) {}

	ngOnInit() {
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 5;

		this.dataInfos.cols = [
			{ field: "ref", header: "Reference", translateCode: "TRANSPORTATION_PLAN.ZONE_REFERENCE" },
			{
				field: "designation",
				header: "Designation",
				translateCode: "TRANSPORTATION_PLAN.ZONE_DESIGNATION",
			},
		];
		this.dataInfos.dataKey = "ebZoneNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerTransportationPlan + "/list-zone-table";
		this.dataInfos.dataType = EbPlZoneDTO;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;

		// this.loadData();
		this.transportationPlanService.listZone(null).subscribe((data) => {
			this.allList = data;
		});
	}

	diplayEditZone(ebZone: EbPlZoneDTO) {
		this.zone = ebZone;
		this.backupEditZone = Statique.cloneObject<EbPlZoneDTO>(this.zone, new EbPlZoneDTO());
		this.displayAddZone(true);
	}

	displayAddZone(isUpdate?: boolean) {
		if (isUpdate) {
		} else this.zone = new EbPlZoneDTO();
		this.isAddZone = true;
	}

	backToListZone() {
		if (this.editingIndex != null) {
			Statique.cloneObject<EbPlZoneDTO>(this.backupEditZone, this.listZone[this.editingIndex]);
			this.editingIndex = null;
		}
		this.backupEditZone = null;
		this.isAddZone = false;
	}
	editZone() {
		this.transportationPlanService.updateZone(this.zone).subscribe((data) => {
			this.isAddZone = false;
			this.editingIndex = null;
			this.backupEditZone = null;
			this.genericTable.refreshData();
			let index = this.allList.findIndex((z) => {
				return z.ebZoneNum == this.zone.ebZoneNum;
			});
			if (index > -1) this.allList[index] = this.zone;
			this.onZoneChanged.emit(this.allList);
		});
	}
	addZone() {
		this.transportationPlanService.updateZone(this.zone).subscribe((data: EbPlZoneDTO) => {
			this.isAddZone = false;
			this.genericTable.refreshData();
			this.allList.push(data);
			this.onZoneChanged.emit(this.allList);
		});
	}

	deleteZone(ebZone: EbPlZoneDTO) {
		let $this = this;
		this.deleteMessage = null;
		this.deleteHeader = null;

		this.translate.get("TRANSPORTATION_PLAN.MESSAGE_DELETE_ZONE").subscribe((res: string) => {
			this.deleteMessage = res;
		});

		this.translate.get("TRANSPORTATION_PLAN.DELETE_ZONE").subscribe((res: string) => {
			this.deleteHeader = res;
		});
		this.modalService.confirm(
			this.deleteHeader,
			this.deleteMessage,
			function() {
				let zone = ebZone;
				$this.transportationPlanService.deleteZone(zone.ebZoneNum).subscribe((data) => {
					$this.genericTable.refreshData();
					let index = $this.allList.findIndex((z) => {
						return z.ebZoneNum == zone.ebZoneNum;
					});
					if (index > -1) $this.allList.splice(index, 1);
					$this.onZoneChanged.emit($this.allList);
				});
			},
			function() {},
			true
		);
	}

	changeModel(form: any) {
		setTimeout(
			function() {
				this.checkFields(form);
			}.bind(this),
			10
		);
	}

	checkFields(form: any) {
		var findOverlap = false;

		this.listZone &&
			this.listZone.some((aZone, index, array) => {
				if (aZone.ref && this.zone.ref && aZone.ebZoneNum !== this.zone.ebZoneNum) {
					if (this.zone.ref.trim().toLowerCase() === aZone.ref.trim().toLowerCase()) {
						findOverlap = true;
						return true;
					}
				}
			});

		if (findOverlap) {
			form.controls["reference"].setErrors("overlap", true);
		} else if (this.zone.ref) {
			form.controls["reference"].setErrors(null);
		}
	}

	rerenderTable() {}

	onDataLoaded(data: Array<EbPlZoneDTO>) {
		this.onZoneChanged.emit(this.allList);
	}

	sharedDropDownZIndexChange(event) {
		this.sharedDropDownZIndexEmit.emit(event);
	}
}
