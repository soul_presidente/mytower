import { ViewType } from "./../../../../../utils/enumeration";
import { Statique } from "./../../../../../utils/statique";
import { SearchCriteria } from "./../../../../../utils/searchCriteria";
import { GenericService } from "./../../../../../services/generic.service";
import { EbLabel } from "./../../../../../classes/label";
import { EbEtablissement } from "@app/classes/etablissement";
import { Component, Input, OnInit, ViewChild, ViewContainerRef } from "@angular/core";
import { AdministrateurCompanyComponent } from "./../../../../administrateur-company/administrateur-company.component";
import { EbCategorie } from "@app/classes/categorie";
import { EtablissementService } from "@app/services/etablissement.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { Observable, of } from "rxjs";
import { filter } from "rxjs/operators";
import { LazyLoadEvent, MessageService } from "primeng/api";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { Table } from "primeng/table";
import { EbCompagnie } from "@app/classes/compagnie";

@Component({
	selector: "app-categorie",
	templateUrl: "./categorie.component.html",
	styleUrls: ["./categorie.component.css"],
	providers: [GenericService],
})
export class CategorieComponent extends AdministrateurCompanyComponent implements OnInit {
	deleteMessage = "";
	deleteHeader = "";

	ebCategorie: EbCategorie = new EbCategorie();
	@Input()
	ebEtablissement: EbEtablissement;
	@Input()
	ebCompagnie: EbCompagnie;

	loading: boolean;
	totalRecords: number[] = [];
	newLabel: EbLabel = new EbLabel();
	currentCatNum: number = null;
	labels: any[] = [];
	cardActions: any[] = [];
	@ViewChild("newLabelInput", { static: false })
	newLabelInput;
	searchTerm: string[] = [];
	saveLoading: boolean = false;

	pageOptions = [
		{ label: "5", value: 5 },
		{ label: "10", value: 10 },
		{ label: "25", value: 25 },
		{ label: "50", value: 50 },
		{ label: "100", value: 100 },
		{ label: "1000", value: 1000 },
	];

	rowsPerPageOptions = [5, 10, 25, 50, 100, 1000];

	nbRows: number[] = [];

	cols: any[];

	ViewType = ViewType;

	viewType: number = ViewType.TABLE;

	selectedPage: number[] = [];

	@ViewChild("dt", { static: false })
	dt: Table;

	constructor(
		private genericService: GenericService,
		protected vcr?: ViewContainerRef,
		protected etablissementService?: EtablissementService,
		protected modalService?: ModalService,
		protected translate?: TranslateService,
		private messageService?: MessageService
	) {
		super(vcr, etablissementService);
	}

	ngOnInit(): void {
		this.cols = [{ field: "libelle", header: "Labels" }];
		if (this.ebCompagnie && this.ebCompagnie.categories) {
			this.ebCompagnie.categories.forEach((it) => {
				it.labels = it.labels.sort((l1, l2) => l1.libelle.localeCompare(l2.libelle));
			});
			this.ebCompagnie.categories = this.ebCompagnie.categories.sort((c1, c2) =>
				c1.libelle.localeCompare(c2.libelle)
			);
		}
		this.loading = false;

		for (let i = 0; i < this.ebCompagnie.categories.length; i++) {
			this.nbRows[i] = 5;
		}

		for (let i = 0; i < this.ebCompagnie.categories.length; i++) {
			this.selectedPage[i] = 0;
		}
	}

	loadLabels(event: LazyLoadEvent, num, index) {
		this.loading = true;
		let criteria = new SearchCriteria();
		criteria.pageNumber = event.first;
		criteria.size = event.rows ? event.rows : 5;
		criteria.ebCategorieNum = num;
		criteria.searchterm = this.searchTerm[index];
		this.genericService
			.runAction(Statique.controllerCategorie + "/label/get-all", criteria)
			.subscribe((res) => {
				this.labels[index] = res.data;
				this.totalRecords[index] = res.count;
				this.loading = false;
			});
	}

	loadLabelsDropdown(event: any, num, index) {
		this.loading = true;
		let criteria = new SearchCriteria();
		criteria.pageNumber = 0;
		criteria.size = event.value;
		criteria.ebCategorieNum = num;
		this.genericService
			.runAction(Statique.controllerCategorie + "/label/get-all", criteria)
			.subscribe((res) => {
				this.labels[index] = res.data;
				this.totalRecords[index] = res.count;
				this.loading = false;
			});
	}

	searchLabel(term: string, num, index) {
		let criteria = new SearchCriteria();
		criteria.pageNumber = this.selectedPage[index];
		criteria.size = this.nbRows[index];
		criteria.searchterm = term;
		criteria.ebCategorieNum = num;
		this.searchTerm[index] = term;
		this.genericService
			.runAction(Statique.controllerCategorie + "/label/get-all", criteria)
			.subscribe((res) => {
				this.labels[index] = res.data;
				this.totalRecords[index] = res.count;
				this.dt.first = this.selectedPage[index];
				this.loading = false;
			});
	}

	saveCategorie(event) {
		if (
			!this.ebCategorie ||
			!this.ebCategorie.libelle ||
			this.ebCategorie.libelle.trim().length == 0
		)
			return;

		if (this.ebCompagnie.categories && this.ebCompagnie.categories.length > 0) {
			let existingCat = this.ebCompagnie.categories.find((it) => {
				return it.libelle == this.ebCategorie.libelle;
			});
			if (existingCat) return;
		}

		this.saveLoading = true;
		// Affectation de l'etablissement
		this.ebCategorie.etablissement = new EbEtablissement();
		this.ebCategorie.etablissement.constructorCopy(this.ebEtablissement);

		// Affectation de la categorie
		this.ebCategorie.compagnie = new EbCompagnie();
		this.ebCategorie.compagnie.constructorCopy(this.ebCompagnie);

		// Envoie requette
		this.etablissementService.addCategorie(this.ebCategorie).subscribe((data) => {
			this.ebCategorie = data;
			this.searchTerm[this.ebCompagnie.categories.length] = "";
			this.ebCompagnie.categories.push(this.ebCategorie);
			this.ebCategorie = new EbCategorie();
			this.nbRows[this.nbRows.length] = 5;
			this.selectedPage[this.selectedPage.length] = 0;
			this.labels[this.labels.length] = [];
			this.searchTerm[this.searchTerm.length] = "";
			this.showSuccess();
			this.saveLoading = false;
		});
	}

	setSelectedPage(event: any, index) {
		this.selectedPage[index] = event.first;
	}

	deleteCategorie(index: number) {
		this.deleteHeader = null;
		this.translate.get("REPOSITORY_MANAGEMENT.DELETE_CATEGORIE").subscribe((res: string) => {
			this.deleteHeader = res;
		});
		this.deleteMessage = null;
		this.translate
			.get("REPOSITORY_MANAGEMENT.MESSAGE_DELETE_CATEGORIE")
			.subscribe((res: string) => {
				this.deleteMessage = res;
			});
		this.modalService.confirm(
			this.deleteHeader,
			this.deleteMessage,
			function() {
				let categorie = this.ebCompagnie.categories[index];

				this.etablissementService.deleteCategorie(categorie).subscribe((data) => {
					if (data) {
						this.ebCompagnie.categories.splice(index, 1);
						this.showSuccess();
					}
				});
			}.bind(this),
			() => {}
		);
	}

	onLabelRemoved(categorie, label, index, catIndex) {
		this.deleteHeader = null;
		this.translate.get("REPOSITORY_MANAGEMENT.DELETE_LABEL").subscribe((res: string) => {
			this.deleteHeader = res;
		});
		this.deleteMessage = null;
		this.translate.get("REPOSITORY_MANAGEMENT.MESSAGE_DELETE_LABEL").subscribe((res: string) => {
			this.deleteMessage = res;
		});
		this.modalService.confirm(
			this.deleteHeader,
			this.deleteMessage,
			function() {
				this.etablissementService.deleteLabel(label).subscribe((data) => {
					if (data) {
						this.showSuccess();
						this.labels[catIndex].splice(index, 1);
						let page = this.selectedPage[catIndex] / this.nbRows[catIndex] + 1;
						if (this.labels[catIndex].length - 1 <= 0) {
							if (page > 1) {
								page--;
								this.selectedPage[catIndex] = (page - 1) * this.nbRows[catIndex];
							}
						}
						this.searchTerm[catIndex] =
							this.searchTerm[catIndex] == null ? "" : this.searchTerm[catIndex];
						this.searchLabel(this.searchTerm[catIndex], categorie.ebCategorieNum, catIndex);
					} else this.showError();
				});
			}.bind(this),
			() => {}
		);
	}

	onLabelAdded(categorie, index, event) {
		/*if(this.ebEtablissement.categories[index].labels && this.ebEtablissement.categories[index].labels.length > 0){
			let existingLabel = this.ebEtablissement.categories[index].labels.find(it => {
				return it.libelle == event.libelle && it.libelle != it.ebLabelNum.toString();
			});
			if(existingLabel)
				return;
		}
    */

		if (!this.newLabel.libelle || this.newLabel.libelle.length == 0) return;

		let ebLabel = new EbLabel();
		ebLabel.libelle = this.newLabel.libelle;
		ebLabel.categorie = new EbCategorie();
		ebLabel.categorie.constructorCopy(categorie);
		this.ebCategorie.etablissement = this.ebEtablissement;
		ebLabel.categorie.labels = [];

		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.etablissementService.addLabel(ebLabel).subscribe((data) => {
			requestProcessing.afterGetResponse(event);

			//this.labels[index].push(data);
			this.showSuccess();
			if (this.searchTerm[index] == null) {
				this.searchTerm[index] = "";
			}
			this.searchLabel(this.searchTerm[index], categorie.ebCategorieNum, index);
			this.newLabel.libelle = "";
		});
	}

	onAdding(categorie: EbCategorie, tag): Observable<any> {
		return of(tag).pipe(
			filter(() => {
				if (categorie.labels && categorie.labels.length > 0) {
					let existingLabel = categorie.labels.find((it) => {
						return it.libelle == tag && it.libelle != it.ebLabelNum.toString();
					});
					if (existingLabel || tag == null || tag.trim().length == 0 || /(.*[;:"].*)/.test(tag))
						return false; // ceci renvoie une erreur, c'est un bug dans ngx-chips mais on peut comme meme l'ignorer
					return true;
				}
				return true;
			})
		);
	}

	changeView() {
		this.viewType = this.viewType == ViewType.TABLE ? ViewType.CARD : ViewType.TABLE;
		console.log(this.viewType);
	}
	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE"),
		});
	}
}
