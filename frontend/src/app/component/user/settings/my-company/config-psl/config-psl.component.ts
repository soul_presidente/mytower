import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { GenericTableScreen, Modules } from "@app/utils/enumeration";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { EbTtCompanyPsl } from "../../../../../classes/ttCompanyPsl";
import { ConfigPslCompanyService } from "../../../../../services/config-psl-company.service";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";

@Component({
	selector: "app-config-psl",
	templateUrl: "./config-psl.component.html",
	styleUrls: ["./config-psl.component.scss"],
})
export class ConfigPslComponent extends ConnectedUserComponent implements OnInit {
	Module = Modules;

	listEbTtCompanyPsl: Array<EbTtCompanyPsl> = new Array<EbTtCompanyPsl>();

	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	isAddConfigPsl = false;
	statique = Statique;

	editingIndex: number;
	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;

	ebTtCompanyPsl: EbTtCompanyPsl = new EbTtCompanyPsl();
	backupEditCompanyConfig: EbTtCompanyPsl;

	constructor(
		private translate?: TranslateService,
		protected modalService?: ModalService,
		protected configPslCompanyService?: ConfigPslCompanyService,
		protected headerService?: HeaderService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 5;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteria.connectedUserNum = this.userConnected.ebUserNum;

		this.dataInfos.cols = [
			{ field: "libelle", header: "libelle", translateCode: "CONFIG_PSL_COMPANY.LIBELLEPSL" },
			{
				field: "codeAlpha",
				header: "codeAlpha",
				translateCode: "CONFIG_PSL_COMPANY.CODEALPHA",
			},
			{
				field: "dateAttendue",
				header: "dateAttendue",
				translateCode: "CONFIG_PSL_COMPANY.DATEATTENDUE",
				render: function(data) {
					if (data == true) return "Yes";
					else if (data == false) return "No";
					else return "";
				}.bind(this),
			},
			{
				field: "quantiteAttendue",
				header: "quantiteAttendue",
				translateCode: "CONFIG_PSL_COMPANY.QUANTITEATTENDUE",
				render: function(data) {
					if (data == true) return "Yes";
					else if (data == false) return "No";
					else return "";
				}.bind(this),
			},
			{
				field: "refDocumentAttendue",
				header: "refDocumentAttendue",
				translateCode: "CONFIG_PSL_COMPANY.REFDOCUMENTATTENDUE",
				render: function(data) {
					if (data == true) return "Yes";
					else if (data == false) return "No";
					else return "";
				}.bind(this),
			},
		];
		this.dataInfos.dataKey = "ebTtCompanyPslNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerCompagniePsl + "/list-track-trace-psl";
		//this.dataInfos.dataType = EbTtCompanyPsl;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
	}

	diplayEditConfigPsl(ebTtCompanyPsl: EbTtCompanyPsl) {
		this.ebTtCompanyPsl = ebTtCompanyPsl;
		this.backupEditCompanyConfig = Statique.cloneObject<EbTtCompanyPsl>(
			this.ebTtCompanyPsl,
			new EbTtCompanyPsl()
		);
		this.displayAddConfigPsl(true);
	}

	displayAddConfigPsl(isUpdate?: boolean) {
		if (isUpdate) {
		} else this.ebTtCompanyPsl = new EbTtCompanyPsl();
		this.isAddConfigPsl = true;
	}

	deleteConfigPsl(ebTtCompanyPsl: EbTtCompanyPsl) {
		let $this = this;

		this.modalService.confirm(
			$this.translate.instant("CONFIG_PSL_COMPANY.DELETE_PSL"),
			$this.translate.instant("CONFIG_PSL_COMPANY.MESSAGE_DELETE_PSL"),
			function() {
				let ttCompanyPsl = ebTtCompanyPsl;
				$this.configPslCompanyService
					.deleteEbTtCompanyPsl(ttCompanyPsl.ebTtCompanyPslNum)
					.subscribe((data) => {
						$this.genericTable.refreshData();
						$this.onDataChanged.next();
					});
			},
			function() {},
			true
		);
	}

	backToListConfigPsl() {
		if (this.editingIndex != null) {
			Statique.cloneObject<EbTtCompanyPsl>(
				this.backupEditCompanyConfig,
				this.listEbTtCompanyPsl[this.editingIndex]
			);
			this.editingIndex = null;
		}
		this.isAddConfigPsl = false;
	}

	editConfigPsl() {
		this.configPslCompanyService.updateEbTtCompanyPsl(this.ebTtCompanyPsl).subscribe((data) => {
			this.isAddConfigPsl = false;
			this.editingIndex = null;
			this.genericTable.refreshData();
			this.onDataChanged.next();
		});
	}
	addConfigPsl() {
		this.configPslCompanyService
			.updateEbTtCompanyPsl(this.ebTtCompanyPsl)
			.subscribe((data: EbTtCompanyPsl) => {
				this.isAddConfigPsl = false;
				this.genericTable.refreshData();
				this.onDataChanged.next();
			});
	}

	changeModel(form: any) {
		setTimeout(
			function() {
				this.checkFields(form);
			}.bind(this),
			10
		);
	}

	checkFields(form: any) {
		var findLibelle = false;
		var findCodeAlpha = false;
		if (!this.ebTtCompanyPsl.libelle) findLibelle = true;
		if (!this.ebTtCompanyPsl.codeAlpha) findCodeAlpha = true;

		this.listEbTtCompanyPsl &&
			this.listEbTtCompanyPsl.some((cong, index, array) => {
				if (
					cong.ebTtCompanyPslNum !== this.ebTtCompanyPsl.ebTtCompanyPslNum &&
					this.ebTtCompanyPsl.codeAlpha.trim().toLowerCase() === cong.codeAlpha.trim().toLowerCase()
				) {
					findLibelle = true;
					findCodeAlpha = true;
					return true;
				}
			});

		if (findLibelle) {
			form.controls["libelle"].setErrors("overlap", true);
		} else {
			form.controls["libelle"].setErrors(null);
		}

		if (findCodeAlpha) {
			form.controls["codeAlpha"].setErrors("overlap", true);
		} else {
			form.controls["codeAlpha"].setErrors(null);
		}
	}

	onDataLoaded(data: Array<EbTtCompanyPsl>) {
		this.listEbTtCompanyPsl = data;
		this.onDataChanged.emit(data);
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.COMPANYS_CONFIG_PSL",
			},
		];
	}
}
