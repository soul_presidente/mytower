import { Component, EventEmitter, OnInit, Output, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { Statique } from "@app/utils/statique";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { GenericTableScreen, Modules, TypeImportance, ViewType } from "@app/utils/enumeration";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import SingletonStatique from "@app/utils/SingletonStatique";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbEtablissement } from "@app/classes/etablissement";
import { EtablissementService } from "@app/services/etablissement.service";
import { ConfigPslCompanyService } from "@app/services/config-psl-company.service";
import { EbTtCompanyPsl } from "@app/classes/ttCompanyPsl";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { EbCompagnie } from "@app/classes/compagnie";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";

@Component({
	selector: "app-config-schema-psl",
	templateUrl: "./config-schema-psl.component.html",
	styleUrls: ["./config-schema-psl.component.scss"],
})
export class ConfigSchemaPslComponent extends ConnectedUserComponent implements OnInit {
	Module = Modules;
	Statique = Statique;
	ViewType = ViewType;

	listEbTtSchemaPsl: Array<EbTtSchemaPsl> = new Array<EbTtSchemaPsl>();
	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;

	isStatiquesLoaded = false;
	isAddObject = false;
	dataObject: EbTtSchemaPsl = new EbTtSchemaPsl();
	backupEditObject: EbTtSchemaPsl;
	editingIndex: number;
	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;

	listModeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();
	listEtablissement: Array<EbEtablissement> = new Array<EbEtablissement>();
	listCompanyPsl: Array<any> = new Array<any>();

	selectListPslActif: Array<any> = new Array<any>();
	selectedModesTransport: Array<number> = new Array<number>();
	selectedListEtablissement: Array<EbEtablissement> = new Array<EbEtablissement>();

	searchCriteriaEtablisement: SearchCriteria = new SearchCriteria();

	viewMode: number = ViewType.CARD;

	selectedPsl: EbTtCompanyPsl;

	constructor(
		private translate?: TranslateService,
		protected modalService?: ModalService,
		protected etablissementService?: EtablissementService,
		protected configPslCompanyService?: ConfigPslCompanyService,
		protected headerService?: HeaderService
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 5;
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteria.connectedUserNum = this.userConnected.ebUserNum;

		this.dataInfos.cols = [
			{
				field: "designation",
				header: "Designation",
				translateCode: "TRANSPORTATION_PLAN.ZONE_DESIGNATION",
			},
			{
				field: "listEtablissement",
				header: "Etablissements",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data: string) {
					let str = "<div>";

					this.listEtablissement &&
						this.listEtablissement.forEach((it) => {
							if (data && data.indexOf(":" + it.ebEtablissementNum + ":") >= 0) {
								str += `<div>${it.nom}</div>`;
							}
						});

					str += "</div>";

					return str;
				}.bind(this),
			},
			{
				field: "isEditable",
				header: "Editable",
				render: (data) => {
					return Statique.isDefined(data) ? (data ? "Yes" : "No") : "";
				},
			},
			{
				field: "listPsl",
				header: "List des PSL",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: (listPsl: Array<EbTtCompanyPsl>) => {
					let str = '<div class="psl-list">';

					if (listPsl) {
						listPsl.sort((it1, it2) => it1.order - it2.order);
						listPsl.forEach((psl) => {
							if (!psl.schemaActif) return;

							str += `
              <div class="psl-data ${
								psl.importance == TypeImportance.MAJEUR ? "psl-important" : ""
							}">
                <span>${psl.libelle}</span>
                ${psl.startPsl ? '<span class="psl-start"> [START]</span>' : ""}
                ${psl.endPsl ? '<span class="psl-end"> [END]</span>' : ""}
                ${
									psl.segmentName
										? '<span class="psl-group"> --(' + psl.segmentName + ")</span>"
										: ""
								}
              </div>`;
						});
					}
					str += "</div>";

					return str;
				},
			},
		];
		this.dataInfos.dataKey = "ebTtSchemaPslNum";
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerCompagniePsl + "/list-schema-psl";
		this.dataInfos.dataType = EbTtSchemaPsl;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = false;

		this.loadData();
	}

	async loadData() {
		let criteria = new SearchCriteria();
		criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		criteria.includeGlobalValues = true;

		this.listEtablissement = await this.etablissementService.getListEtablisementPromise(criteria);
		let etabAll = new EbEtablissement();
		etabAll.ebEtablissementNum = -1;
		etabAll.nom = "All";
		this.listEtablissement.unshift(etabAll);

		this.listCompanyPsl = await this.configPslCompanyService.getListEbTtCompanyPslTablePromise(
			criteria
		);

		this.isStatiquesLoaded = true;
	}

	diplayEdit(dataObject: EbTtSchemaPsl) {
		this.dataObject = Statique.cloneObject(dataObject, new EbTtSchemaPsl());

		this.onShowAddEditDisplay(dataObject);

		this.backupEditObject = Statique.cloneObject<EbTtSchemaPsl>(
			this.dataObject,
			new EbTtSchemaPsl()
		);
		this.displayAdd(true);
	}

	onShowAddEditDisplay(dataObject: EbTtSchemaPsl) {
		this.selectedListEtablissement = this.listEtablissement.filter((it) => {
			return (
				(this.dataObject.listEtablissement || "").indexOf(":" + it.ebEtablissementNum + ":") >= 0
			);
		});

		let dataPsl = dataObject.listPsl;
		let tmpSelectListPsl = this.listCompanyPsl.map((it) => {
			let obj = Statique.cloneObject<any>(it, new EbTtCompanyPsl());
			let data = dataPsl ? dataPsl.find((p) => p.ebTtCompanyPslNum == it.ebTtCompanyPslNum) : null;
			if (data) {
				obj.schemaActif = data.schemaActif;
				obj.segmentName = data.segmentName;
				obj.importance = data.importance;
				obj.startPsl = data.startPsl;
				obj.endPsl = data.endPsl;
				obj.order = data.order;
			}
			return obj;
		});
		tmpSelectListPsl.sort((it1, it2) => it1.order - it2.order);
		tmpSelectListPsl.forEach((it, index) => {
			if (!it.order) it.order = index + 1;
		});

		this.dataObject.listPsl = tmpSelectListPsl;
		this.selectListPslActif = tmpSelectListPsl
			.filter((it) => it.schemaActif)
			.sort((it1, it2) => it1.order - it2.order)
			.map((it, index) => {
				if (!it.order) it.order = index + 1;
				return it;
			});
	}

	displayAdd(isUpdate?: boolean) {
		if (!isUpdate) {
			this.dataObject = new EbTtSchemaPsl();
			this.dataObject.isEditable = false; // Default value to prevent issues
		}

		this.onShowAddEditDisplay(this.dataObject);

		this.isAddObject = true;
	}

	backToListZone() {
		if (this.editingIndex != null) {
			Statique.cloneObject<EbTtSchemaPsl>(
				this.backupEditObject,
				this.listEbTtSchemaPsl[this.editingIndex]
			);
			this.editingIndex = null;
		}
		this.backupEditObject = null;
		this.isAddObject = false;
	}

	isFormValidated(): boolean {
		this.setSelectedListEtablissement();
		if (!this.selectListPslActif || !this.selectListPslActif.length) return false;

		let isValid = !!this.selectListPslActif.find((it: EbTtCompanyPsl) => it.startPsl);
		isValid = isValid && !!this.selectListPslActif.find((it: EbTtCompanyPsl) => it.endPsl);
		isValid =
			isValid &&
			!!this.selectListPslActif.find(
				(it: EbTtCompanyPsl) => it.importance == TypeImportance.MAJEUR
			);

		isValid = isValid && this.dataObject.designation && this.dataObject.designation.length > 0;
		isValid =
			isValid &&
			!!this.dataObject.listEtablissement &&
			this.dataObject.listEtablissement.length > 0;

		return isValid;
	}

	editSchemaPsl() {
		if (!this.isFormValidated()) return;
		this.dataObject.listPsl = this.selectListPslActif;
		this.configPslCompanyService.updateSchemaPsl(this.dataObject).subscribe((data) => {
			this.isAddObject = false;
			this.editingIndex = null;
			this.backupEditObject = null;
			this.dataObject = null;
			this.selectedModesTransport = null;
			this.selectedListEtablissement = null;
			this.genericTable.refreshData();
		});
	}
	addSchemaPsl() {
		if (!this.isFormValidated()) return;
		this.dataObject.xEbCompagnie = new EbCompagnie();
		this.dataObject.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.configPslCompanyService
			.updateSchemaPsl(this.dataObject)
			.subscribe((data: EbTtSchemaPsl) => {
				this.isAddObject = false;
				this.dataObject = null;
				this.selectedModesTransport = null;
				this.selectedListEtablissement = null;
				this.genericTable.refreshData();
			});
	}

	setSelectedModesTransport() {
		this.dataObject.listModeTransport = "";
		this.selectedModesTransport &&
			this.selectedModesTransport.forEach((it) => {
				this.dataObject.listModeTransport += ":" + it + ":";
			});
	}
	setSelectedListEtablissement() {
		this.dataObject.listEtablissement = "";
		this.selectedListEtablissement &&
			this.selectedListEtablissement.forEach((it) => {
				this.dataObject.listEtablissement += ":" + it.ebEtablissementNum + ":";
			});
	}

	deleteData(schemaPsl: EbTtSchemaPsl) {
		let $this = this;

		this.modalService.confirm(
			$this.translate.instant("GENERAL.CONFIRMATION"),
			$this.translate.instant("CONFIG_PSL_COMPANY.SCHEMA_PSL_CONFIRM_DELETE"),
			function() {
				let dataObject = schemaPsl;
				$this.configPslCompanyService
					.deleteSchemaPsl(dataObject.ebTtSchemaPslNum)
					.subscribe((data) => {
						$this.genericTable.refreshData();
					});
			},
			function() {},
			true
		);
	}

	setOrder(psl: any, index: number, up: boolean = false) {
		this.selectListPslActif[index].order = up
			? this.selectListPslActif[index].order - 1
			: this.selectListPslActif[index].order + 1;
		if (up && this.selectListPslActif[index - 1]) {
			++this.selectListPslActif[index - 1].order;
		} else if (!up && this.selectListPslActif[index + 1]) {
			--this.selectListPslActif[index + 1].order;
		}
		this.selectListPslActif.sort((it1, it2) => it1.order - it2.order);
	}
	onStartChange(psl: any, index: number) {
		if (!this.selectListPslActif[index].startPsl) return;

		this.selectListPslActif.forEach((it, i) => {
			if (index !== i && this.selectListPslActif[i].startPsl)
				this.selectListPslActif[i].startPsl = false;
		});
	}
	onEndChange(psl: any, index: number) {
		if (!this.selectListPslActif[index].endPsl) return;

		this.selectListPslActif.forEach((it, i) => {
			if (index !== i && this.selectListPslActif[i].endPsl)
				this.selectListPslActif[i].endPsl = false;
		});
	}

	changeView() {
		this.viewMode = this.viewMode == ViewType.CARD ? ViewType.TABLE : ViewType.CARD;
		if (this.viewMode == ViewType.TABLE) {
			this.selectListPslActif.sort((it1, it2) => it1.order - it2.order);
		}
	}

	orderEndEventHandler(itemsOrder: Array<any>) {
		if (itemsOrder) {
			itemsOrder.forEach((it) => {
				this.selectListPslActif.some((el) => {
					if (+it.ebTtCompanyPslNum === +el.ebTtCompanyPslNum) {
						el.order = +it.order;
						return true;
					}
				});
			});
		}
	}

	onPslSelect($event) {
		if (!this.selectedPsl) return;

		let pslIndex = this.dataObject.listPsl.findIndex(
			(it) => this.selectedPsl.ebTtCompanyPslNum == it.ebTtCompanyPslNum
		);
		this.dataObject.listPsl[pslIndex].schemaActif = true;
		this.dataObject.listPsl[pslIndex].order = this.selectListPslActif.length;

		let psl = this.dataObject.listPsl.find(
			(it) => this.selectedPsl.ebTtCompanyPslNum == it.ebTtCompanyPslNum
		);
		this.selectListPslActif.push(psl);

		this.selectedPsl = null;
	}
	removePsl($event: EbTtCompanyPsl, index: number) {
		if (!$event) return;

		let pslIndex = this.selectListPslActif.findIndex(
			(it) => $event.ebTtCompanyPslNum == it.ebTtCompanyPslNum
		);
		this.selectListPslActif[pslIndex].schemaActif = false;
		this.selectListPslActif[pslIndex].order = null;

		this.selectListPslActif.splice(pslIndex, 1);
	}
	editPsl(event) {
		let eltParent = event.target.parentElement;
		let nextElt = event.target.parentNode.nextElementSibling;
		if (nextElt.classList.contains("on-edition")) {
			nextElt.classList.remove("on-edition");
		} else {
			nextElt.classList.add("on-edition");
		}
		if (eltParent.classList.contains("clicked")) {
			eltParent.classList.remove("clicked");
		} else {
			eltParent.classList.add("clicked");
		}
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.SCHEMAS_PSL",
			},
		];
	}
}
