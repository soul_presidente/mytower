import {
	Component,
	OnInit,
	EventEmitter,
	Output,
	Input,
	ViewChild,
	OnChanges,
	ElementRef,
	QueryList,
	ViewChildren,
} from "@angular/core";
import { EbWeekTemplatePeriodDTO } from "@app/classes/week-template/EbWeekTemplatePeriodDTO";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { EbWeekTemplateDTO } from "@app/classes/week-template/EbWeekTemplateDTO";
import { Statique } from "@app/utils/statique";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { GenericTableScreen, SavedFormIdentifier, Modules } from "@app/utils/enumeration";
import { StatiqueService } from "@app/services/statique.service";
import { HeaderService } from "@app/services/header.service";
import { AuthenticationService } from "@app/services/authentication.service";
import { ModalService } from "@app/shared/modal/modal.service";
import { WeekTemplateService } from "@app/services/WeekTemplateService";
import { PlanTransportNew } from "@app/classes/PlanTansportNew";
import { FormGroup, FormBuilder, NgForm } from "@angular/forms";
import { SearchField } from "@app/classes/searchField";
import SingletonStatique from "@app/utils/SingletonStatique";
import { FlagColumnGenericCell } from "@app/shared/generic-cell/commons/flag-column.generic-cell";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { GenericEnum } from "@app/classes/GenericEnum";
import { EbWeekTemplateDayDTO } from "@app/classes/week-template/EbWeekTemplateDayDTO";
import { EbWeekTemplateHourDTO } from "@app/classes/week-template/EbWeekTemplateHourDTO";
import { GenericEnumPlanTrans } from "@app/utils/enumeration";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { OldAutocompleteComponent } from "@app/shared/old-autocomplete/old-autocomplete.component";
import { DatetimePickerComponent } from "@app/shared/datetime-picker/datetime-picker.component";
import { AdvancedFormSearchComponent } from "@app/shared/advanced-form-search/advanced-form-search.component";
import { PlanTransportNewService } from "@app/services/planTransportNew.service";
import { TranslateService } from "@ngx-translate/core";
import { EbFlag } from "@app/classes/ebFlag";
import { EbTypeTransport } from "@app/classes/EbTypeTransport";
import { TypeRequestService } from "@app/services/type-request.service";
import { SavedForm } from "@app/classes/savedForm";
import { FlagMultiSelectComponent } from "@app/shared/flag/flag-multi-select/flag-multi-select.component";
import { EbMarchandise } from "@app/classes/marchandise";
import { element } from "protractor";
import { EbCategorie } from "@app/classes/categorie";
import { EtablissementService } from "@app/services/etablissement.service";
import { IField } from "@app/classes/customField";
import { EbLabel } from "@app/classes/label";
import { PricingService } from "@app/services/pricing.service";

@Component({
	selector: "app-plan-transport-new",
	templateUrl: "./plan-transport-new.component.html",
	styleUrls: ["./plan-transport-new.component.scss"],
})
export class PlanTransportNewComponent extends ConnectedUserComponent implements OnInit {
	Statique = Statique;
	listPlanTransport: Array<PlanTransportNew> = new Array<PlanTransportNew>();
	@Input()
	weekTemplates: Array<EbWeekTemplateDTO> = [];
	@Input()
	dataInfosItem: Array<any> = [];
	dataInfosCategorie: Array<any> = [];
	dataCategorie: Array<any> = [];
	@Input()
	selectedField: any;
	@Input()
	dataSourceType = "json";
	@Input()
	selectedWeek: any;
	@Output()
	onSearch = new EventEmitter<SearchCriteriaPricingBooking>();
	@Output()
	searchHandler = new EventEmitter<any>();
	@Output()
	confirmeEvent: EventEmitter<any> = new EventEmitter<any>();
	searchCriteria: SearchCriteria = new SearchCriteria();
	@Output()
	cancelEvent: EventEmitter<any> = new EventEmitter<any>();
	statique = Statique;
	listDay: any;
	ebWeekTemplateDTO: EbWeekTemplateDTO = new EbWeekTemplateDTO();
	public listSelectedFields: Array<SearchField>;
	isAddEditMode = false;
	planTransport: PlanTransportNew = new PlanTransportNew();
	backupPlanTransport: PlanTransportNew;
	editingIndex: number;
	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;
	@ViewChildren(OldAutocompleteComponent)
	autocompChildren: QueryList<OldAutocompleteComponent>;
	@ViewChildren(DatetimePickerComponent)
	datetimePickerChildren: QueryList<DatetimePickerComponent>;
	@ViewChild(AdvancedFormSearchComponent, { static: false })
	advancedFormSearch: AdvancedFormSearchComponent;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();
	planTransportNew: PlanTransportNew = new PlanTransportNew();
	private listTypeTransportMap: Map<number, Array<EbTypeTransport>>;

	GenericTableScreen = GenericTableScreen;
	editMode: boolean = false;
	listTypeWeek: Array<GenericEnum> = [];
	@Input()
	period: EbWeekTemplateDTO;
	public listFields: Array<SearchField>;
	paramFields: Array<SearchField> = new Array<SearchField>();
	initParamFields: Array<SearchField> = new Array<SearchField>();
	SavedFormIdentifier = SavedFormIdentifier;
	listPlanningStatus: any;
	listCountry: any;
	listCarrier: any;
	listServiceLevel: any;
	@Input()
	listCustomField: Array<IField>;
	@Input()
	listCategorie: Array<EbCategorie>;
	listCustomCategorie: Array<SearchField> = new Array<SearchField>();

	listAllCategorie: any;
	originCity: any;
	destinationCity: any;
	transportConfirmationValues: any;
	backupEditObject: PlanTransportNew;
	Modules = Modules;
	@Input()
	module: number;
	@ViewChildren(FlagMultiSelectComponent)
	flagMultiSelectChildren: QueryList<FlagMultiSelectComponent>;
	selectedMarchandise: EbMarchandise;
	marchandiseDangerousGood: Map<number, string>;
	criteria: SearchCriteria = new SearchCriteria();
	constructor(
		protected statiqueService: StatiqueService,
		protected typeRequestService: TypeRequestService,
		protected headerService: HeaderService,
		protected authenticationService: AuthenticationService,
		protected translate: TranslateService,
		protected modalService: ModalService,
		public fieldSelector: ElementRef,
		protected weekTemplateService: WeekTemplateService,
		protected planTransportNewService: PlanTransportNewService,
		protected fb: FormBuilder,
		private etablissementService: EtablissementService,
		protected pricingService: PricingService
	) {
		super();
		this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
	}
	@Input()
	initialListFields: any = [];
	parsedValue: Array<SearchField>;
	public formCtrl: FormGroup;
	private viewInit: boolean = false;
	@Input()
	initFields?: string = "json"; // undefined | all | json
	hide_opening_from_hour: boolean = true;
	getDayName(codeDay: number) {
		return this.listDay[codeDay - 1];
	}

	async getStatiques() {
		try {
			this.listCountry = await SingletonStatique.getListEcCountry();
			this.listTypeTransportMap = await SingletonStatique.getListTypeTransportMap();

			this.statiqueService.getListGenericEnum(GenericEnumPlanTrans.WeekType).subscribe((res) => {
				this.listTypeWeek = res;
			});
			this.statiqueService.getListStatutTransportManagement().subscribe((res) => {
				this.listPlanningStatus = res;
			});
			this.statiqueService.getListCarrier("").subscribe((res) => {
				this.listCarrier = res;
			});
			this.typeRequestService.getListTypeRequests(this.searchCriteria).subscribe((res) => {
				this.listServiceLevel = res;
			});
			this.statiqueService
				.getListGenericEnum(GenericEnumPlanTrans.TransportConfirmation)
				.subscribe((res) => {
					this.transportConfirmationValues = res;
				});
			this.statiqueService
				.getListGenericEnum(GenericEnumPlanTrans.TransportConfirmation)
				.subscribe((res) => {
					this.transportConfirmationValues = res;
				});
			this.marchandiseDangerousGood = await SingletonStatique.getListMarchandiseDangerousGoods();
		} catch (e) {}
	}

	addRangeHour(day: EbWeekTemplateDayDTO) {
		if (!day.hours) day.hours = new Array<EbWeekTemplateHourDTO>();

		let hour: EbWeekTemplateHourDTO = new EbWeekTemplateHourDTO();
		day.hours.push(hour);
	}

	deleteRangeHour(day, hour) {
		day.hours.forEach((item, index) => {
			if (item == hour) day.hours.splice(index, 1);
		});
	}

	ngOnInit() {
		this.module = Modules.LOAD_PLANNINNG;
		this.planTransportNew.xEbCompagnieChargeur = this.userConnected.ebCompagnie;
		this.planTransportNew.xEbCompagnieCarrier = this.userConnected.ebCompagnie;
		this.getStatiques();
		if (!this.period) {
			this.initialize();
		}
		this.weekTemplateService.getListWeekTemplate(this.searchCriteria).subscribe((res: any) => {
			this.weekTemplates = res;
		});

		this.viewInit = false;
		this.onInputChange(this.selectedField);
		this.formCtrl = this.fb.group({});
		if (this.listSelectedFields) {
			let formFields = {};
			this.listSelectedFields.forEach((it, index) => {
				let exists = this.listSelectedFields.some((sit) => {
					if (it.name === sit.name) {
						this.formCtrl.addControl(sit.name, this.fb.control(""));
						return true;
					}
				});
				if (!exists) this.listFields.push(it);
			});
		}

		this.viewInit = true;
		/**
		 * Fin Form Data Init
		 */
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 50;
		this.dataInfos.cols = this.getColumns();
		this.dataInfos.dataKey = "ebPlanTransportNum";
		this.dataInfos.dataType = PlanTransportNew;
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerPlanTransportNew + "/list-plan-transport-table";
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.onFlagChange = this.onFlagChange;
	}
	onFlagChange(context: any, row: PlanTransportNew, listFlag: Array<EbFlag>) {
		let listFlagCode = EbFlag.convertListToCommaSeparated(listFlag);
		row.input.listFlag = listFlagCode;
		context.planTransportNewService.saveFlags(row.ebPlanTransportNum, listFlagCode).subscribe();
	}
	getTypeWeekDisplay(typeWeekCode): string {
		let rValue = "";
		let foundKey = GenericEnum.findKeyByCode(typeWeekCode, this.listTypeWeek);

		if (foundKey) {
			rValue = this.translate.instant("GENERAL.WEEKTEMPLATE_TYPE." + foundKey);
		}
		return rValue;
	}

	onClickInput() {
		this.initParamFields = this.initParamFields.concat(
			require("../../../../../../assets/ressources/jsonfiles/booking-searchfields.json")
		);
		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));
		this.dataInfosItem = this.dataInfosItem.concat(this.dataInfosCategorie);
		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});
	}

	onInputChange(data: any) {
		this.initParamFields = this.initParamFields.concat(
			require("../../../../../../assets/ressources/jsonfiles/booking-searchfields.json")
		);
		this.initParamFields = this.initParamFields.concat(this.dataInfosCategorie);
		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));
		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});
		if (data === null || data === undefined) {
			this.initParamFields = this.initParamFields.filter(
				(field) =>
					field.name === "listIncoterm" ||
					field.name === "listDestinations" ||
					field.name === "listOrigins" ||
					field.name === "listOriginCity" ||
					field.name === "listDestinationCity"
			);
			this.paramFields = Array.from(new Set([].concat(this.initParamFields)));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
		} else if (data !== null && !this.paramFields.find((field) => field.name == data)) {
			let result = JSON.stringify(this.advancedFormSearch.getSearchInput());
			let parsedValues: Array<SearchField> = JSON.parse(result);
			this.paramFields.forEach((field) => {
				if (parsedValues[field.name] !== null && parsedValues[field.name] !== undefined) {
					field.default = parsedValues[field.name];
				}
			});
			this.initParamFields = this.initParamFields.filter((field) => field.name === data);
			this.paramFields = this.paramFields.concat(this.initParamFields);
			this.paramFields = Array.from(new Set(this.paramFields));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
		}
	}

	onWeekChange(week) {
		this.ebWeekTemplateDTO = this.weekTemplates.find((w) => w.ebWeektemplateNum == week);
		var weekDays = this.ebWeekTemplateDTO.days;
		if (weekDays) this.period = this.ebWeekTemplateDTO;
		this.weekTemplateService.getListDayByWeekTemplate(week).subscribe((res: any) => {
			this.ebWeekTemplateDTO.days = res;
		});
	}
	onWeekClick() {
		this.weekTemplateService.getListWeekTemplate(this.searchCriteria).subscribe((res: any) => {
			this.weekTemplates = res;
		});
	}

	inlineEditingHandler(key: string, value: any, model: any, context: any) {
		if (key === "selection-change") {
			context.autoSaveRow(model, value);
		}
	}

	autoSaveRow(row: PlanTransportNew, listFlag: Array<EbFlag>) {
		let listFlagCode = EbFlag.convertListToCommaSeparated(listFlag);
		row.input.listFlag = listFlagCode;
		this.planTransportNewService.saveFlags(row.ebPlanTransportNum, listFlagCode).subscribe();
	}
	getColumns(): any {
		const $this = this;
		let cols: any = [
			{
				field: "listEbFlagDTO",
				translateCode: "PRICING_BOOKING.FLAG",
				minWidth: "125px",
				allowClick: false,
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: FlagColumnGenericCell,
				genericCellParams: new FlagColumnGenericCell.Params({
					field: "listEbFlagDTO",
					contributionAccess: true,
					moduleNum: Modules.LOAD_PLANNINNG,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{
				translateCode: "PRICING_BOOKING.MODE_OF_TRANSPORT",
				field: "input",
				isCardCol: true,
				cardOrder: 5,
				width: "140px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type, row) {
					var result = "";
					if (data.listModeTransport && data.listModeTransport.length > 0) {
						data.listModeTransport.forEach((element) => {
							result +=
								SingletonStatique.getModeTransportString(element, row, $this.listTypeTransportMap) +
								"<br>";
						});
					}
					return result;
				}.bind(this),
			},
			{
				field: "input",
				translateCode: "TRACK_TRACE.UNIT_REFERENCE",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type, row) {
					var result = "";
					if (data.listUnitReference && data.listUnitReference.length > 0) {
						data.listUnitReference.forEach((element) => {
							result += element + "<br>";
						});
					}
					return result;
				},
			},
			{
				field: "input",
				translateCode: "PRICING_BOOKING.INCOTERMS",
				width: "100px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					var result = "";
					if (data.listIncoterm && data.listIncoterm.length > 0) {
						data.listIncoterm.forEach((element) => {
							result += element + "<br>";
						});
					}
					return result;
				},
			},
			{
				field: "input",
				translateCode: "REQUEST_OVERVIEW.CARRIER_UNIQ_REF_NUM",
				width: "130px",
				render: function(data, row, d) {
					return data.carrierUniqRefNum;
				},
			},
			{
				translateCode: "PRICING_BOOKING.QUOTATION_REFERENCE",
				field: "input",
				width: "100px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type, row) {
					var result = "";
					if (data.listTransportRef && data.listTransportRef.length > 0) {
						data.listTransportRef.forEach((element) => {
							result += element + "<br>";
						});
						return result;
					}
					return result;
				}.bind(this),
			},
			{
				field: "weekTemplate",
				translateCode: "SETTINGS.WEEK_TEMPLATE",
				width: "150px",
				isCardCol: true,
				cardOrder: 2,
				isCardLabel: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					if (data && data.days) {
						let weekDays = JSON.parse(JSON.stringify(data));
						var passingString = "";
						weekDays.days.forEach((element) => {
							if (element.hours.length > 0) {
								var formattedDay = Statique.daysEnum[element.code];
								formattedDay = $this.translate.instant("GENERAL.DAYS." + formattedDay);

								passingString += formattedDay.substring(0, 3);

								element.hours.forEach((hour) => {
									passingString += "-" + hour.startHour;
								});

								passingString += " <br>";
							}
						});
						return passingString;
					} else return "";
				},
			},
			{
				translateCode: "PRICING_BOOKING.COMPANY_ORIGIN",
				field: "input",
				render: function(data, row, d) {
					return data.companyOrigin;
				},
			},
			{
				field: "input",
				translateCode: "TRANSPORTATION_PLAN.CARRIER",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type, row) {
					var result = "";
					if (data.listCarrier && data.listCarrier.length > 0) {
						data.listCarrier.forEach((element) => {
							var carrierLabel = $this.listCarrier.find((carrier) => carrier.ebUserNum == element)
								.compagnieNameNomUserPrenomUser;
							result += carrierLabel + "<br>";
						});
					}
					return result;
				}.bind(this),
			},
			{
				field: "input",
				translateCode: "PRICING_BOOKING.ORIGIN",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type, row) {
					var result = "";
					if (data.listOrigins && data.listOrigins.length > 0) {
						data.listOrigins.forEach((element) => {
							var countryLabel = $this.listCountry.find(
								(country) => country.ecCountryNum == element
							).libelle;
							result += countryLabel + "<br>";
						});
					}
					return result;
				}.bind(this),
			},
			{
				field: "input",
				translateCode: "PRICING_BOOKING.DESTINATION",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type, row) {
					var result = "";
					if (data.listDestinations && data.listDestinations.length > 0) {
						data.listDestinations.forEach((element) => {
							var countryLabel = $this.listCountry.find(
								(country) => country.ecCountryNum == element
							).libelle;
							result += countryLabel + "<br>";
						});
					}
					return result;
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.ORIGIN_CITY",
				field: "input",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					var result = "";
					if (data.listOriginCity && data.listOriginCity.length > 0) {
						data.listOriginCity.forEach((element) => {
							var cityLabel = element;
							if (cityLabel != undefined) {
								var label = cityLabel.toLowerCase();
								result += label.charAt(0).toUpperCase() + label.slice(1) + "<br>";
							}
						});
					}
					return result;
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.DESTINATION_CITY",
				field: "input",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					var result = "";
					if (data.listDestinationCity && data.listDestinationCity.length > 0) {
						data.listDestinationCity.forEach((element) => {
							var cityLabel = element;
							if (cityLabel != undefined) {
								var label = cityLabel.toLowerCase();
								result += label.charAt(0).toUpperCase() + label.slice(1) + "<br>";
							}
						});
					}
					return result;
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.COST_CENTER",
				field: "input",
				width: "105px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					var result = "";
					if (data.listCostCenter && data.listCostCenter.length > 0) {
						data.listCostCenter.forEach((element) => {
							result += element + "<br>";
						});
					}
					return result;
				},
			},
			{
				translateCode: "CHANEL_PB_ORDER.SAP_ORDER_REF",
				field: "input",
				width: "105px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					var result = "";
					if (data.listNumOrderSAP && data.listNumOrderSAP.length > 0) {
						data.listNumOrderSAP.forEach((element) => {
							result += element + "<br>";
						});
					}
					return result;
				},
			},
			{
				translateCode: "CHANEL_PB_ORDER.EDI_ORDER_REF",
				field: "input",
				width: "105px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					var result = "";
					if (data.listNumOrderEDI && data.listNumOrderEDI.length > 0) {
						data.listNumOrderEDI.forEach((element) => {
							result += element + "<br>";
						});
					}
					return result;
				},
			},
			{
				translateCode: "CHANEL_PB_ORDER.CUSTOMER_ORDER_REF",
				field: "input",
				width: "105px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					var result = "";
					if (data.listNumOrderCustomer && data.listNumOrderCustomer.length > 0) {
						data.listNumOrderCustomer.forEach((element) => {
							result += element + "<br>";
						});
					}
					return result;
				},
			},
			{
				translateCode: "CHANEL_PB_DELIVERY.CODE_CAMPAGNE",
				field: "input",
				width: "105px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					var result = "";
					if (data.listCampaignCode && data.listCampaignCode.length > 0) {
						data.listCampaignCode.forEach((element) => {
							result += element + "<br>";
						});
					}
					return result;
				},
			},
			{
				translateCode: "CHANEL_PB_DELIVERY.NAME_CAMPAGNE",
				field: "input",
				width: "105px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					var result = "";
					if (data.listCampaignName && data.listCampaignName.length > 0) {
						data.listCampaignName.forEach((element) => {
							result += element + "<br>";
						});
					}
					return result;
				},
			},
			{
				translateCode: "GENERAL.LOAD_PLANNING.REFERENCE_DELIVERY",
				field: "input",
				width: "105px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					var result = "";
					if (data.listReferenceDelivery && data.listReferenceDelivery.length > 0) {
						data.listReferenceDelivery.forEach((element) => {
							result += element + "<br>";
						});
					}
					return result;
				},
			},
			{
				translateCode: "GENERAL.LOAD_PLANNING.REF_CUSTOMER_DELIVERY",
				field: "input",
				width: "105px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					var result = "";
					if (data.listRefCustomerDelivery && data.listRefCustomerDelivery.length > 0) {
						data.listRefCustomerDelivery.forEach((element) => {
							result += element + "<br>";
						});
					}
					return result;
				},
			},
			{
				translateCode: "GENERAL.LOAD_PLANNING.NAME_CUSTOMER_DELIVERY",
				field: "input",
				width: "105px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					var result = "";
					if (data.listNameCustomerDelivery && data.listNameCustomerDelivery.length > 0) {
						data.listNameCustomerDelivery.forEach((element) => {
							result += element + "<br>";
						});
					}
					return result;
				},
			},
			{
				translateCode: "GENERAL.LOAD_PLANNING.REQUESTED_DELIVERY_DT_FROM",
				field: "input",
				width: "80px",
				render: function(data) {
					if (data.requestedDeliveryDateFrom) return data.requestedDeliveryDateFrom;
				}.bind(this),
			},
			{
				translateCode: "GENERAL.LOAD_PLANNING.REQUESTED_DELIVERY_DT_TO",
				field: "input",
				width: "80px",
				render: function(data) {
					if (data.requestedDeliveryDateTo) return data.requestedDeliveryDateTo;
				}.bind(this),
			},
			{
				field: "input",
				translateCode: "GENERAL.LOAD_PLANNING.LOT_NUMBER",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type, row) {
					var result = "";
					if (data.listLot && data.listLot.length > 0) {
						data.listLot.forEach((element) => {
							result += element + "<br>";
						});
					}
					return result;
				},
			},
			{
				translateCode: "PRICING_BOOKING.TRANSPORT_CONFIRMATION",
				field: "input",
				isCardCol: true,
				cardOrder: 5,
				width: "140px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type, row) {
					var result = "";
					if (data.transportConfirmation && data.transportConfirmation.length > 0) {
						data.transportConfirmation.forEach((element) => {
							var transportConfirmation = $this.transportConfirmationValues.find(
								(val) => val.code == element
							).key;
							result += transportConfirmation + "<br>";
						});
					}
					return result;
				}.bind(this),
			},
			{
				translateCode: "GENERAL.LOAD_PLANNING.DATE_CREATION_FROM",
				field: "input",
				width: "80px",
				render: function(data) {
					if (data.dateCreationFrom) return data.dateCreationFrom;
				}.bind(this),
			},
			{
				translateCode: "GENERAL.LOAD_PLANNING.DATE_CREATION_TO",
				field: "input",
				width: "80px",
				render: function(data) {
					if (data.dateCreationTo) return data.dateCreationTo;
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.SERVICE_LEVEL",
				field: "input",
				width: "80px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type, row) {
					var result = "";
					if (data.listTypeDemande && data.listTypeDemande.length > 0) {
						data.listTypeDemande.forEach((element) => {
							var serviceLevel = $this.listServiceLevel.find(
								(service) => service.ebTypeRequestNum == element
							).reference;
							result += serviceLevel + "<br>";
						});
					}
					return result;
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.STATUS",
				field: "input",
				isCardCol: true,
				cardOrder: 6,
				isCardLabel: true,
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type, row) {
					var result = "";
					if (data.listStatut && data.listStatut.length > 0) {
						data.listStatut.forEach((element) => {
							var statusValue = $this.listPlanningStatus.find((status) => status.code == element)
								.libelle;
							result += statusValue + "<br>";
						});
						return result;
					}
					return result;
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.ACKNOWLEDGED_BY_CT",
				field: "input",
				width: "80px",
				render: function(data) {
					var result = "";
					if (data.acknoledgeByCt) {
						var listItems = $this.statique.yesOrNoOrEmptyWithNumberKey;
						result = listItems.find((item) => item.key == data.acknoledgeByCt).value;
						result = this.translate.instant(result);
					}
					return result;
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.ACKNOWLEDGE_DATE_BY_CT",
				field: "input",
				width: "80px",
				render: function(data) {
					return data.acknoledgeByCtDate ? data.acknoledgeByCtDate : "";
				}.bind(this),
			},
			{
				translateCode: "GENERAL.LOAD_PLANNING.PICKUP_FROM",
				field: "input",
				width: "105px",
				render: function(data, display, row) {
					return data.datePickUpFrom ? data.datePickUpFrom : "";
				}.bind(this),
			},
			{
				translateCode: "GENERAL.LOAD_PLANNING.PICKUP_TO",
				field: "input",
				width: "105px",
				render: function(data, display, row) {
					return data.datePickUpTo ? data.datePickUpTo : "";
				}.bind(this),
			},
			{
				translateCode: "TRANSPORTATION_PLAN.DANGEROUS",
				field: "input",
				width: "105px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, type, row) {
					var result = "";
					if (data.dangerousGood && data.dangerousGood.length > 0) {
						data.dangerousGood.forEach((element) => {
							result += $this.marchandiseDangerousGood[element] + "<br>";
						});
					}
					return result;
				},
			},
			{
				translateCode: "TRANSPORTATION_PLAN.GROSS_WEIGHT_MAX",
				field: "input",
				width: "80px",
				render: function(data) {
					if (data.maxWeight) return data.maxWeight;
					return "";
				},
			},
			{
				translateCode: "TRANSPORTATION_PLAN.VOLUME_MAX",
				field: "input",
				width: "80px",
				render: function(data) {
					if (data.maxVolume) return data.maxVolume;
					return "";
				},
			},
			{
				translateCode: "TRANSPORTATION_PLAN.HEIGHT_MAX",
				field: "input",
				width: "80px",
				render: function(data) {
					if (data.maxHeight) return data.maxHeight;
					return "";
				}.bind(this),
			},
			{
				translateCode: "TRANSPORTATION_PLAN.LENGTH_MAX",
				field: "input",
				width: "80px",
				render: function(data) {
					if (data.maxLength) return data.maxLength;
					return "";
				},
			},
			{
				translateCode: "TRANSPORTATION_PLAN.WIDTH_MAX",
				field: "input",
				width: "80px",
				render: function(data) {
					if (data.maxWidth) return data.maxWidth;
					return "";
				},
			},
			{
				translateCode: "UN",
				field: "input",
				width: "80px",
				render: function(data) {
					if (data.uns) return data.uns;
					return "";
				},
			},
			{
				translateCode: "Class",
				field: "input",
				width: "80px",
				render: function(data) {
					if (data.classGoods) return data.classGoods;
					return "";
				},
			},
		];

		cols.forEach((it) => {
			this.translate.get(it.translateCode).subscribe((res) => (it["header"] = res));
		});

		return cols;
	}
	onListCategorieLoaded(listCategorie: Array<EbCategorie>) {
		this.listCategorie = listCategorie;
		if (this.listCategorie && this.listCategorie.length > 0) {
			let arr = [];
			this.listCategorie.forEach((it, i) => {
				arr.push({
					label: it.libelle,
					type: "autocomplete",
					name: "category" + i,
					listItems: it.labels,
					fetchAttribute: "libelle",
					fetchValue: "ebLabelNum",
					multiple: "true",
				});
			});
			this.dataInfosCategorie = this.dataInfosCategorie.concat(arr);
		}
	}
	// methode pour instancier l'objet du formulaire et charger les données passées en param
	initFormData() {
		this.viewInit = false;

		if (this.initialListFields && typeof this.initFields !== "undefined") {
			if (this.initFields === "all")
				this.listSelectedFields = Statique.cloneObject(this.initialListFields);
			else {
				this.listSelectedFields = JSON.parse(this.initFields);
			}
		}
		this.viewInit = true;
	}

	initialize() {
		this.listDay = Statique.dowEnum;
		this.period = new EbWeekTemplateDTO();

		this.period.days = new Array<EbWeekTemplateDayDTO>();
		this.listDay.forEach((statiqueDay, index) => {
			let day = new EbWeekTemplateDayDTO();
			day.code = index + 1; // MONDAY CODE IS 1...
			this.period.days.push(day);
		});
	}

	emitConfirme() {
		this.confirmeEvent.emit(this.period);
	}

	emitCancel() {
		this.cancelEvent.emit();
	}

	openAddForm(isUpdate?: boolean) {
		if (isUpdate) {
			this.editMode = true;
		} else {
			this.planTransport = new PlanTransportNew();
			this.editMode = false;
		}
		this.isAddEditMode = true;
	}

	displayEdit(planTransportNew: PlanTransportNew) {
		this.planTransportNew = Statique.cloneObject(planTransportNew, new PlanTransportNew());
		this.backupEditObject = this.planTransportNew;
		this.ebWeekTemplateDTO = this.planTransportNew.weekTemplate;
		this.selectedWeek = this.ebWeekTemplateDTO.ebWeektemplateNum;
		this.weekTemplates.find(
			(w) => w.ebWeektemplateNum == this.selectedWeek
		).days = this.planTransportNew.weekTemplate.days;
		this.openAddForm(true);
		var formValues = JSON.stringify(this.planTransportNew.input);
		this.fillEditForm(formValues, this.planTransportNew);
	}

	async fillEditForm(formValues: any, planTransportNew: any) {
		let fields: Array<SearchField> = new Array<SearchField>();
		let parsedValues: Array<SearchField> = JSON.parse(formValues);
		let parseCatdValues: Array<SearchField> = planTransportNew;

		let initCatFields: Array<SearchField> = new Array<SearchField>();
		let categories: Array<SearchField> = JSON.parse(formValues);
		this.dataCategorie = this.dataInfosCategorie;
		this.dataCategorie.forEach((field) => {
			if (categories[field.name] !== null && categories[field.name] !== undefined) {
				field.default = planTransportNew.input.listCategorie;
			}
		});
		this.initParamFields = require("../../../../../../assets/ressources/jsonfiles/booking-searchfields.json");
		this.initParamFields = this.initParamFields.filter(
			(field) => parsedValues[field.name] !== null && parsedValues[field.name] !== undefined
		);

		Object.keys(parseCatdValues).forEach((element) => {
			if (element && element.indexOf("listCategories") >= 0) {
				if (parseCatdValues[element] != null) {
					parseCatdValues[element].forEach((key) => {
						for (var i of this.dataCategorie) {
							if (key.libelle == i.label) {
								initCatFields.push(i);
							}
						}
					});
				}
			}
		});
		fields = this.initParamFields.concat(initCatFields);
		this.listSelectedFields = this.paramFields = Array.from(new Set([].concat(fields)));
		this.listSelectedFields.forEach((field) => {
			if (field.type != "flag-multi-select" && !field.name.includes("category")) {
				field.default = parsedValues[field.name];
			} else if (field.type != "flag-multi-select" && field.name.includes("category")) {
				let labelIds: Array<number> = [];
				for (var i of field.listItems as Array<EbLabel>) {
					if (
						planTransportNew != undefined &&
						planTransportNew.input != undefined &&
						planTransportNew.input.listCategorie != undefined &&
						planTransportNew.input.listCategorie.find((el) => el == i.ebLabelNum) != undefined
					)
						labelIds.push(planTransportNew.input.listCategorie.find((el) => el == i.ebLabelNum));
					field.default = labelIds;
				}
			}
		});
	}

	resetFormFields() {
		// if a user click on the "add button" after cancelling an "edit" action, they should see the form fields reinitialized (emptied)
		if (this.listSelectedFields != undefined) {
			this.listSelectedFields.forEach((field) => {
				field.default = null;
			});
		}
		this.selectedWeek = null;
	}

	backToList() {
		if (this.editingIndex != null) {
			Statique.cloneObject<PlanTransportNew>(
				this.backupPlanTransport,
				this.listPlanTransport[this.editingIndex]
			);
			this.editingIndex = null;
		}
		this.backupPlanTransport = null;
		this.isAddEditMode = false;
		this.genericTable.refreshData();
		this.dataInfos.showAddBtn = true;
		this.resetFormFields();
	}
	openEditForm(ebregime: PlanTransportNew) {
		this.planTransport = ebregime;
		this.backupPlanTransport = Statique.cloneObject<PlanTransportNew>(
			this.planTransport,
			new PlanTransportNew()
		);
		this.openAddForm(true);
	}
	deleteLoadPlanning(planTransportNew: PlanTransportNew) {
		let $this = this;
		this.modalService.confirm(
			this.translate.instant("GENERAL.CONFIRMATION"),
			this.translate.instant("GENERAL.CONFIRM_DELETE"),
			function() {
				$this.planTransportNewService
					.deleteLoadPlanning(planTransportNew)
					.subscribe((data: boolean) => {
						$this.genericTable.refreshData();
					});
			},
			function() {},
			true,
			this.translate.instant("GENERAL.CONFIRM"),
			this.translate.instant("GENERAL.CANCEL")
		);
	}

	trackByIndex(index: number, obj: any): any {
		return index;
	}

	addItem(planTransportNew: PlanTransportNew, searchCriteria: SearchCriteria) {
		if (planTransportNew.ebPlanTransportNum !== undefined) {
			planTransportNew = new PlanTransportNew();
			planTransportNew.xEbCompagnieChargeur = this.userConnected.ebCompagnie;
			planTransportNew.xEbCompagnieCarrier = this.userConnected.ebCompagnie;
		}
		planTransportNew.weekTemplate = this.ebWeekTemplateDTO;
		let result = JSON.stringify(this.advancedFormSearch.getSearchInput());
		planTransportNew.input = JSON.parse(result);
		searchCriteria.searchInput = result;
		let categories = JSON.parse(result);
		if (planTransportNew.input.listCategorie == undefined)
			planTransportNew.input.listCategorie = [];
		Object.keys(categories).forEach((element) => {
			if (element && element.indexOf("category") >= 0) {
				planTransportNew.input.listCategorie = planTransportNew.input.listCategorie.concat(
					categories[element]
				);
			}
		});
		if (planTransportNew.listCategories == undefined) planTransportNew.listCategories = [];
		let categorie = new EbCategorie();
		planTransportNew.input.listCategorie &&
			planTransportNew.input.listCategorie.forEach((it) => {
				let cat = this.listCategorie.find((cc) => !!cc.labels.find((el) => el.ebLabelNum == it));
				if (cat) {
					let newCat = Statique.cloneObject(cat, new EbCategorie());
					newCat.labels = new Array<EbLabel>();
					newCat.labels.push(cat.labels.find((el) => el.ebLabelNum == it));
					if (categorie.libelle == newCat.libelle) categorie.labels.push(newCat.labels[0]);
					else categorie = newCat;
				}
				if (
					categorie.ebCategorieNum != null &&
					planTransportNew.listCategories.indexOf(categorie) < 0
				)
					planTransportNew.listCategories.push(categorie);
			});

		this.planTransportNewService.save(planTransportNew).subscribe((res) => {
			this.formCtrl.reset();
			this.selectedWeek = null;
			this.backToList();
		});
	}

	updateItem(planTransportNew: PlanTransportNew, searchCriteria: SearchCriteria) {
		planTransportNew.weekTemplate = this.ebWeekTemplateDTO;
		let result = JSON.stringify(this.advancedFormSearch.getSearchInput());
		planTransportNew.input = JSON.parse(result);
		searchCriteria.searchInput = result;
		let categories = JSON.parse(result);
		if (planTransportNew.input.listCategorie == undefined)
			planTransportNew.input.listCategorie = [];
		Object.keys(categories).forEach((element) => {
			if (element && element.indexOf("category") >= 0) {
				planTransportNew.input.listCategorie = planTransportNew.input.listCategorie.concat(
					categories[element]
				);
			}
		});

		if (planTransportNew.listCategories == undefined) planTransportNew.listCategories = [];
		let categorie = new EbCategorie();
		let categorieList = Array<EbCategorie>();
		if (planTransportNew.input.listCategorie.length == 0) planTransportNew.listCategories = [];
		planTransportNew.input.listCategorie &&
			planTransportNew.input.listCategorie.forEach((it) => {
				let cat = this.listCategorie.find((cc) => !!cc.labels.find((el) => el.ebLabelNum == it));
				let newCat = Statique.cloneObject(cat, new EbCategorie());
				newCat.labels = new Array<EbLabel>();
				if (cat) {
					newCat.labels.push(cat.labels.find((el) => el.ebLabelNum == it));
					if (categorie.libelle == newCat.libelle) categorie.labels.push(newCat.labels[0]);
					else categorie = newCat;
					categorieList.push(categorie);
				}
				planTransportNew.listCategories.map((element, i) => {
					if (element != null) {
						if (element.libelle == categorie.libelle)
							planTransportNew.listCategories[i] = categorie;
					}
				});
				if (
					categorie.ebCategorieNum != null &&
					planTransportNew.listCategories.indexOf(categorie) < 0
				) {
					planTransportNew.listCategories.push(categorie);
				}
			});
		planTransportNew.listCategories = planTransportNew.listCategories.filter(function(element) {
			return categorieList.includes(element);
		});
		this.planTransportNewService.save(planTransportNew).subscribe((res) => {
			this.formCtrl.reset();
			this.selectedWeek = null;
			this.backToList();
		});
	}
	search(advancedOption: SearchCriteriaPricingBooking) {
		this.onSearch.emit(advancedOption);
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.PLANNING_CHARGEMENT",
			},
		];
	}
}
