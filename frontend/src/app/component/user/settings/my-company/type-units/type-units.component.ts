import { Component, OnInit, ViewChild } from "@angular/core";

import { TranslateService } from "@ngx-translate/core";
import { Statique } from "./../../../../../utils/statique";
import { SearchCriteriaShipToMatrix } from "./../../../../../utils/SearchCriteriaShipToMatrix";
import { GenericTableScreen } from "./../../../../../utils/enumeration";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { EbTypeUnit } from "@app/classes/typeUnit";
import { ModalService } from "@app/shared/modal/modal.service";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";

@Component({
	selector: "app-type-units",
	templateUrl: "./type-units.component.html",
	styleUrls: ["./type-units.component.scss"],
})
export class TypeUnitsComponent implements OnInit {
	cols: any;
	datas: any;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	searchInput: SearchCriteriaShipToMatrix = new SearchCriteriaShipToMatrix();
	datatableComponent = GenericTableScreen;
	Statique = Statique;
	activatedMessage = "";
	activatedHeader = "";
	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	constructor(
		private http: HttpClient,
		private translate: TranslateService,
		protected headerService: HeaderService,
		protected modalService: ModalService
	) {}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.dataInfos.cols = [
			{ field: "libelle", translateCode: "TYPE_UNIT.LABEL", type: "text" },
			{
				field: "code",
				translateCode: "TYPE_UNIT.CODE",
				type: "text",
				fieldRequired: true,
				uniqueValue: true,
				urlIsUniqueValue: Statique.controllerTypeUnit + "/exist-code-typeUnit-by-compagnie",
			},
			{ field: "weight", translateCode: "TYPE_UNIT.WEIGHT", type: "text" },
			{ field: "length", translateCode: "TYPE_UNIT.LENGTH", type: "text" },
			{ field: "width", translateCode: "TYPE_UNIT.WIDTH", type: "text" },
			{ field: "height", translateCode: "TYPE_UNIT.HEIGHT", type: "text" },
			{ field: "volume", translateCode: "TYPE_UNIT.VOLUME", type: "text" },
			{
				field: "gerbable",
				translateCode: "TYPE_UNIT.GERBABLE",
				type: "boolean",
				valueField: "gerbableValue",
			},
			{
				field: "containerLabel",
				translateCode: "TYPE_UNIT.TYPE_CONTAINER",
				type: "list",
				valueField: "xEbTypeContainer",
				source: "web",
				url: Statique.controllerTypeUnit + "/list-units",
			},
			{
				field: "activated",
				translateCode: "TYPE_UNIT.ACTIVATED",
				type: "boolean",
				valueField: "activated",
			},
		];
		this.translator(this.dataInfos.cols);
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.dataKey = "ebTypeUnitNum";
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.activateCRUD = false;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showAddBtn = true;
		this.dataInfos.paginator = true;
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.dataLink = Statique.controllerTypeUnit + "/list-type-unit";
		this.dataInfos.saveLink = Statique.controllerTypeUnit + "/save-type-unit";
		this.dataInfos.deleteLink = Statique.controllerTypeUnit + "/activate-deactivate-type-unit";

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
	}
	displayUnit(data: EbTypeUnit) {
		this.genericTable.selectedActionRow = data;
		if (this.genericTable.autocomplete != null) {
			this.genericTable.autocomplete.ngOnInit();
		}
		this.genericTable.previousRow = Object.assign({}, this.genericTable.selectedActionRow);
		return (this.genericTable.displayDialog = true);
	}
	displayNewUnit() {
		this.genericTable.displayNewItem();
	}

	activatedUnit(data: EbTypeUnit) {
		let $this = this;
		this.activatedMessage = null;
		this.activatedHeader = null;

		this.translate.get("TYPE_UNIT.MESSAGE_ACTIVATED_" + data.activated).subscribe((res: string) => {
			this.activatedMessage = res;
		});
		this.translate.get("TYPE_UNIT.HEADER_ACTIVATED_" + data.activated).subscribe((res: string) => {
			this.activatedHeader = res;
		});

		this.modalService.confirm(
			this.activatedHeader,
			this.activatedMessage,
			function() {
				if (data.activated) data.activated = false;
				else data.activated = true;

				$this.activateDeactivate(data).subscribe((data) => {
					$this.genericTable.refreshData();
					//$this.onDataChanged.next();
				});
			},
			function() {},
			true
		);
	}
	activateDeactivate(ebTypeUnit: EbTypeUnit): Observable<any> {
		let params =
			"?ebTypeUnitNum=" + ebTypeUnit.ebTypeUnitNum + "&activated=" + ebTypeUnit.activated;
		return this.http.get(Statique.controllerTypeUnit + "/activate-deactivate-type-unit" + params);
	}

	translator(tab: any[]) {
		tab.forEach((it) => {
			this.translate.get(it.translateCode).subscribe((res) => {
				it["header"] = res;
			});
		});
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.TYPE_OF_UNITS",
			},
		];
	}
}
