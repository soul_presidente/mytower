import { Component, HostBinding, OnInit } from "@angular/core";
import { MessageService } from "primeng/api";
import { OverlayContainer } from "@angular/cdk/overlay";
import { UserService } from "../../../../../services/user.service";
import { ThemesConfigService } from "../../../../../services/themes-config.service";
import { EbUser } from "../../../../../classes/user";
import { EbUserThemes } from "../../../../../classes/userThemes ";
import { TranslateService } from "@ngx-translate/core";
import { Statique, Theme } from "@app/utils/statique";

@Component({
	selector: "app-themes-setting",
	templateUrl: "./themes-setting.component.html",
	styleUrls: ["./themes-setting.component.css"],
})
export class ThemesSettingComponent implements OnInit {
	ebUser: EbUser;
	newTheme: EbUserThemes;
	selectedValue: string = "default-theme";
	@HostBinding("class")
	componentCssClass;
	themesList: Theme[] = Statique.listThemes

	constructor(
		private translate: TranslateService,
		private messageService: MessageService,
		public overlayContainer: OverlayContainer,
		private themesConfigService: ThemesConfigService
	) {}

	ngOnInit() {
		this.ebUser = UserService.getConnectedUser();
		if (localStorage.getItem("activeTheme")) {
			this.selectedValue = localStorage.getItem("activeTheme");
		}
	}

	onThemeSet($event, theme) {
		this.selectedValue = theme;
		Statique.removeThemes();
		document.getElementsByTagName("body")[0].classList.add(theme)
		let event = new CustomEvent("change-theme", { detail: theme });
		document.dispatchEvent(event);
	}

	saveTheme() {
		this.newTheme = new EbUserThemes();
		this.newTheme.activeTheme = this.selectedValue;
		this.newTheme.xEbUser = this.ebUser;
		this.newTheme.xEbCompagnie = this.ebUser.ebCompagnie;
		this.themesConfigService.saveThemesConfig(this.newTheme).subscribe((res) => {
			if (res) {
				this.onPostValidateSettings();
			}
		});
	}

	onPostValidateSettings() {
		localStorage.setItem("activeTheme", this.selectedValue);
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("THEME_SETTINGS.SETTING_THEME"),
			detail: this.translate.instant("THEME_SETTINGS.SUCCES_SAVE_THEME"),
		});
	}
}
