import { Component, OnInit } from "@angular/core";

@Component({
	selector: "app-lexique-mail-control-rules",
	templateUrl: "./lexique-mail-control-rules.component.html",
	styleUrls: ["./lexique-mail-control-rules.component.scss"],
})
export class LexiqueControlRulesMailComponent implements OnInit {
	jsonArray = [
		{ cle: "${transport.reference}", type: "Text", desc: "Transport reference" },
		{ cle: "${timestamp}", type: "Text", desc: "Timestamp status" },
		{ cle: "${transport.status}", type: "Text", desc: "Transport status" },
	];

	constructor() {}

	ngOnInit() {}

	//Pour copier du texte
	copyTextToClipboard(text) {
		var txtArea = document.createElement("textarea");
		txtArea.id = "txt";
		txtArea.style.position = "fixed";
		txtArea.style.top = "0";
		txtArea.style.left = "0";
		txtArea.style.opacity = "0";
		txtArea.value = text;
		document.body.appendChild(txtArea);
		txtArea.select();

		try {
			var successful = document.execCommand("copy");
			var msg = successful ? "successful" : "unsuccessful";
			if (successful) {
				return true;
			}
		} catch (err) {
			console.log("Oops, unable to copy");
		} finally {
			document.body.removeChild(txtArea);
		}
		return false;
	}
}
