import { Component, OnInit } from "@angular/core";
import { MenuItem, TreeNode } from "primeng/api";
import {
	TemplateExportDocDictionnary,
	FieldType,
	FieldDoc,
} from "./template-export-doc-dictionnary";
import { Statique } from "@app/utils/statique";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-template-export-doc",
	templateUrl: "./template-export-doc.component.html",
	styleUrls: ["./template-export-doc.component.scss"],
})
export class TemplateExportDocComponent implements OnInit {
	Statique = Statique;

	tabItems: MenuItem[];
	tabActiveItem: MenuItem;

	pricingDoc: TreeNode[];
	orderDoc: TreeNode[];
	deliveryDoc: TreeNode[];

	constructor(protected translate: TranslateService) {}

	ngOnInit() {
		this.initDocData();
	}

	getDocTreeForSelectedTab(): TreeNode[] {
		if (this.tabActiveItem.id === "pricing") {
			return this.pricingDoc;
		} else if (this.tabActiveItem.id === "order") {
			return this.orderDoc;
		} else if (this.tabActiveItem.id === "delivery") {
			return this.deliveryDoc;
		}
	}

	// Doc Data
	private initDocData() {
		// Tabs
		this.tabItems = [
			{
				id: "pricing",
				label: this.translate.instant("MODULES.PRICING"),
				icon: "my-icon-order",
				command: () => {
					this.tabActiveItem = this.tabItems[0];
				},
			},
			{
				id: "order",
				label: this.translate.instant("MODULES.ORDER"),
				icon: "my-icon-order-management",
				command: () => {
					this.tabActiveItem = this.tabItems[1];
				},
			},
			{
				id: "delivery",
				label: this.translate.instant("MODULES.DELIVERY"),
				icon: "my-icon-delivery-management",
				command: () => {
					this.tabActiveItem = this.tabItems[2];
				},
			},
			{
				id: "howto",
				label: this.translate.instant("GENERAL.GUIDE"),
				icon: "fa fa-question",
				command: () => {
					this.tabActiveItem = this.tabItems[this.tabItems.length - 1];
				},
			},
		];
		this.tabActiveItem = this.tabItems[0];

		// Pricing
		this.pricingDoc = TemplateExportDocDictionnary.getFieldsDocForType(FieldType.PricingItemTPL);
		this.orderDoc = TemplateExportDocDictionnary.getFieldsDocForType(FieldType.OrderRootTPL);
		this.deliveryDoc = TemplateExportDocDictionnary.getFieldsDocForType(FieldType.DeliveryRootTPL);
	}
	// END Doc Data

	public getFieldCode(rowNode: TreeNode, rowData: FieldDoc): string {
		let codeHierarchy = [];

		codeHierarchy.push(rowData.code);

		let checkedNode = rowNode;
		while (checkedNode.parent) {
			checkedNode = checkedNode.parent;

			if (checkedNode.data.isList && codeHierarchy.length > 0) {
				break;
			}
			codeHierarchy.push(checkedNode.data.code);
		}
		let resultStr = codeHierarchy.reverse().join(".");
		return resultStr;
	}

	public getFieldFullCode(rowNode: TreeNode, rowData: FieldDoc): string {
		let fieldCode = this.getFieldCode(rowNode, rowData);

		if (rowData.isList) {
			return "repeatTableRow(" + fieldCode + ")";
		} else {
			return "${" + fieldCode + "}";
		}
	}
}
