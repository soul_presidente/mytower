import { TreeNode } from "primeng/api";

export class FieldDoc {
	code: string;
	type: string;
	desc: string;
	isList?: boolean;

	public constructor(init?: Partial<FieldDoc>) {
		Object.assign(this, init);
	}
}

export enum FieldType {
	PricingItemTPL,
	EbDemandeTPL,
	EbUserTPL,
	EbEtablissementTPL,
	CustomFieldsTPL,
	EbCategorieTPL,
	ExEbDemandeTransporteurTPL,
	EbPartyTPL,
	EbMarchandiseTPL,
	String,
	Double,
	Long,
	Integer,
	BigDecimal,
	Boolean,
	Date,
	OrderRootTPL,
	EbOrderTPL,
	EbOrderLineTPL,
	DeliveryRootTPL,
	EbLivraisonTPL,
	EbLivraisonLineTPL,
}

export class TemplateExportDocDictionnary {
	public static getFieldsDocForType(type: FieldType) {
		let selectedList: TreeNode[] = [];

		switch (type) {
			case FieldType.PricingItemTPL:
				selectedList = this.PricingItemTPL();
				break;
			case FieldType.EbDemandeTPL:
				selectedList = this.EbDemandeTPL();
				break;
			case FieldType.EbUserTPL:
				selectedList = this.EbUserTPL();
				break;
			case FieldType.EbEtablissementTPL:
				selectedList = this.EbEtablissementTPL();
				break;
			case FieldType.CustomFieldsTPL:
				selectedList = this.CustomFieldsTPL();
				break;
			case FieldType.EbCategorieTPL:
				selectedList = this.EbCategorieTPL();
				break;
			case FieldType.ExEbDemandeTransporteurTPL:
				selectedList = this.ExEbDemandeTransporteurTPL();
				break;
			case FieldType.EbPartyTPL:
				selectedList = this.EbPartyTPL();
				break;
			case FieldType.EbMarchandiseTPL:
				selectedList = this.EbMarchandiseTPL();
				break;
			case FieldType.OrderRootTPL:
				selectedList = this.OrderRootTPL();
				break;
			case FieldType.EbOrderTPL:
				selectedList = this.EbOrderTPL();
				break;
			case FieldType.EbOrderLineTPL:
				selectedList = this.EbOrderLineTPL();
				break;
			case FieldType.DeliveryRootTPL:
				selectedList = this.DeliveryRootTPL();
				break;
			case FieldType.EbLivraisonTPL:
				selectedList = this.EbLivraisonTPL();
				break;
			case FieldType.EbLivraisonLineTPL:
				selectedList = this.EbLivraisonLineTPL();
				break;
		}

		return selectedList.sort((a, b) => {
			return a.data.code.localeCompare(b.data.code);
		});
	}

	public static getDisplayNameForType(type: FieldType) {
		switch (type) {
			case FieldType.PricingItemTPL:
				return "Pricing / Transport Management";
			case FieldType.EbDemandeTPL:
				return "Transport Request";
			case FieldType.EbUserTPL:
				return "User";
			case FieldType.EbEtablissementTPL:
				return "Establishment";
			case FieldType.CustomFieldsTPL:
				return "Custom Field";
			case FieldType.EbCategorieTPL:
				return "Category";
			case FieldType.ExEbDemandeTransporteurTPL:
				return "Quotation";
			case FieldType.EbPartyTPL:
				return "Party";
			case FieldType.EbMarchandiseTPL:
				return "Unit (TR)";
			case FieldType.String:
				return "Text";
			case FieldType.Double:
				return "Number";
			case FieldType.Long:
				return "Number";
			case FieldType.Integer:
				return "Number";
			case FieldType.BigDecimal:
				return "Number";
			case FieldType.Boolean:
				return "Boolean";
			case FieldType.Date:
				return "Date";
			case FieldType.EbOrderTPL:
				return "Order";
			case FieldType.EbOrderLineTPL:
				return "Unit (Order)";
			case FieldType.OrderRootTPL:
				return "Order Management";
			case FieldType.EbLivraisonLineTPL:
				return "Unit (Delivery)";
			case FieldType.EbLivraisonTPL:
				return "Delivery";
			case FieldType.DeliveryRootTPL:
				return "Delivery Management";
		}

		return "";
	}

	private static PricingItemTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "dateNow",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "Actual date time",
				}),
			},
			{
				data: new FieldDoc({
					code: "connectedUser",
					type: this.getDisplayNameForType(FieldType.EbUserTPL),
					desc: "Actual connected user",
				}),
				children: this.EbUserTPL(),
			},
			{
				data: new FieldDoc({
					code: "demandePrincipal",
					type: this.getDisplayNameForType(FieldType.EbDemandeTPL),
					desc: "Transport request requested to export",
				}),
				children: this.EbDemandeTPL(),
			},
		];
	}

	private static EbDemandeTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "reference",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "Transport reference",
				}),
			},
			{
				data: new FieldDoc({
					code: "city",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "typeDemande",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "modeTransport",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "incoterm",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "libelleOriginCountry",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "libelleDestCountry",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "temperatureTransport",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "temperatureStorage",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "temperatureStorageLibelle",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "codeAnnulation",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "aboutTrAwbBol",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "aboutTrMawb",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "aboutTrFlightVessel",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "typeRequest",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "customerReference",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "insurance",
					type: this.getDisplayNameForType(FieldType.Boolean),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "insuranceValue",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "insuranceCurrencyCode",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "currencyCode",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "availabilityDate",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "dateOfArrival",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalNbrParcel",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalWeight",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalVolume",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalTaxableWeight",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "insuranceCurrency",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "costCenter",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "units",
					type: this.getDisplayNameForType(FieldType.EbMarchandiseTPL),
					desc: "",
					isList: true,
				}),
				children: this.EbMarchandiseTPL(),
			},
			{
				data: new FieldDoc({
					code: "quotations",
					type: this.getDisplayNameForType(FieldType.ExEbDemandeTransporteurTPL),
					desc: "",
					isList: true,
				}),
				children: this.ExEbDemandeTransporteurTPL(),
			},
			{
				data: new FieldDoc({
					code: "customFieldsList",
					type: this.getDisplayNameForType(FieldType.CustomFieldsTPL),
					desc: "Custom fields (list format)",
					isList: true,
				}),
				children: this.CustomFieldsTPL(),
			},
			{
				data: new FieldDoc({
					code: "customFields",
					type: "Map<Text, Text>",
					desc: "Custom fields (map format)",
				}),
			},
			{
				data: new FieldDoc({
					code: "categoriesList",
					type: this.getDisplayNameForType(FieldType.EbCategorieTPL),
					desc: "Categories (list format)",
					isList: true,
				}),
				children: this.EbCategorieTPL(),
			},
			{
				data: new FieldDoc({
					code: "categorieLabelValue",
					type: "Map<Text, Text>",
					desc: "Categories (map format)",
				}),
			},
			{
				data: new FieldDoc({
					code: "carrierName",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "commentChargeur",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "commentChargeurHtml",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "libelleTypeTransport",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "user",
					type: this.getDisplayNameForType(FieldType.EbUserTPL),
					desc: "",
				}),
				children: this.EbUserTPL(),
			},
			{
				data: new FieldDoc({
					code: "partyOrigin",
					type: this.getDisplayNameForType(FieldType.EbPartyTPL),
					desc: "",
				}),
				children: this.EbPartyTPL(),
			},
			{
				data: new FieldDoc({
					code: "partyDestination",
					type: this.getDisplayNameForType(FieldType.EbPartyTPL),
					desc: "",
				}),
				children: this.EbPartyTPL(),
			},
			{
				data: new FieldDoc({
					code: "partyNotify",
					type: this.getDisplayNameForType(FieldType.EbPartyTPL),
					desc: "",
				}),
				children: this.EbPartyTPL(),
			},
			{
				data: new FieldDoc({
					code: "etablissement",
					type: this.getDisplayNameForType(FieldType.EbEtablissementTPL),
					desc: "",
				}),
				children: this.EbEtablissementTPL(),
			},
			{
				data: new FieldDoc({
					code: "unitsReference",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "nature",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "masterObjectReference",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "dateCreation",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "nameCreator",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "nameOwner",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "carrierEstablishment",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "carrierCompany",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "carrierEmail",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "carrierPhone",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "libelleLastPsl",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "dateLastPsl",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "flagTransportInformation",
					type: this.getDisplayNameForType(FieldType.Boolean),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "pslStartDate",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "pslEndDate",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "confirmPickup",
					type: this.getDisplayNameForType(FieldType.Boolean),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "confirmDelivery",
					type: this.getDisplayNameForType(FieldType.Boolean),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "status",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "price",
					type: this.getDisplayNameForType(FieldType.BigDecimal),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalUnitPrice",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "hasDestInEurop",
					type: this.getDisplayNameForType(FieldType.Boolean),
					desc: "",
				}),
			},
		];
	}

	private static EbUserTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "nom",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "Last name",
				}),
			},
			{
				data: new FieldDoc({
					code: "prenom",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "First name",
				}),
			},
			{
				data: new FieldDoc({
					code: "email",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "Email",
				}),
			},
			{
				data: new FieldDoc({
					code: "etablissementNom",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "Name of affected establishment",
				}),
			},
			{
				data: new FieldDoc({
					code: "adresse",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "Address",
				}),
			},
			{
				data: new FieldDoc({
					code: "phone",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "Phone number",
				}),
			},
		];
	}

	private static EbEtablissementTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "ebEtablissementNum",
					type: this.getDisplayNameForType(FieldType.Integer),
					desc: "ID",
				}),
			},
			{
				data: new FieldDoc({
					code: "typeEtablissement",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "siret",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "nom",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "email",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
		];
	}

	private static CustomFieldsTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "name",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "value",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "label",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
		];
	}

	private static EbCategorieTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "libelle",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "code",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
		];
	}

	private static ExEbDemandeTransporteurTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "companyName",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "price",
					type: this.getDisplayNameForType(FieldType.BigDecimal),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "transitTime",
					type: this.getDisplayNameForType(FieldType.Integer),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "pickupTime",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "deliveryTime",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "comment",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "status",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
		];
	}

	private static EbPartyTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "company",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "adresse",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "city",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "zipCode",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "openingHours",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "airport",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "email",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "phone",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "commentaire",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "countryName",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
		];
	}

	private static EbMarchandiseTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "originCountryName",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "netWeight",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "weight",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "length",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "width",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "height",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "dg",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "dryIce",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "classGood",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "un",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "equipment",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "sensitive",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "typeMarchandise",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "unitReference",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "customerReference",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "quantityDryIce",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "numberOfUnits",
					type: this.getDisplayNameForType(FieldType.Integer),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "volume",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "dangerousGood",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "packaging",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "stackable",
					type: this.getDisplayNameForType(FieldType.Boolean),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "comment",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "nomArticle",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "lot",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "sscc",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "olpn",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "trackingNumber",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "etat",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "pood",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "typeContainer",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "handlingUnit",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "huLineNumber",
					type: this.getDisplayNameForType(FieldType.Integer),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "lotQuantite",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "typeOfUnit",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "hsCode",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "unitPrice",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "price",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
		];
	}

	private static EbOrderLineTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "itemNumber",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "itemName",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "partNumber",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "serialNumber",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "comment",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "weight",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "length",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "width",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "volume",
					type: this.getDisplayNameForType(FieldType.Double),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "serialized",
					type: this.getDisplayNameForType(FieldType.Boolean),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "quantityOrdered",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "quantityPlannable",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "quantityPlanned",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "quantityPending",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "quantityConfirmed",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "quantityShipped",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "quantityDelivered",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "quantityCanceled",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
		];
	}

	private static EbOrderTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "orderRef",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "status",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "customerOrderRef",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "ownerUser",
					type: this.getDisplayNameForType(FieldType.EbUserTPL),
					desc: "",
				}),
				children: this.EbUserTPL(),
			},
			{
				data: new FieldDoc({
					code: "partyOrigin",
					type: this.getDisplayNameForType(FieldType.EbPartyTPL),
					desc: "",
				}),
				children: this.EbPartyTPL(),
			},
			{
				data: new FieldDoc({
					code: "partyDest",
					type: this.getDisplayNameForType(FieldType.EbPartyTPL),
					desc: "",
				}),
				children: this.EbPartyTPL(),
			},
			{
				data: new FieldDoc({
					code: "partySoldTo",
					type: this.getDisplayNameForType(FieldType.EbPartyTPL),
					desc: "",
				}),
				children: this.EbPartyTPL(),
			},
			{
				data: new FieldDoc({
					code: "customerCreationDate",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "targetDate",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "complementaryInfos",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalQuantityOrdered",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalQuantityPlannable",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalQuantityPlanned",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalQuantityPending",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalQuantityConfirmed",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalQuantityShipped",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalQuantityDelivered",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "totalQuantityCanceled",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "units",
					type: this.getDisplayNameForType(FieldType.EbOrderLineTPL),
					desc: "",
					isList: true,
				}),
				children: this.EbOrderLineTPL(),
			},
		];
	}

	private static OrderRootTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "order",
					type: this.getDisplayNameForType(FieldType.EbOrderTPL),
					desc: "",
				}),
				children: this.EbOrderTPL(),
			},
			{
				data: new FieldDoc({
					code: "connectedUser",
					type: this.getDisplayNameForType(FieldType.EbUserTPL),
					desc: "",
				}),
				children: this.EbUserTPL(),
			},
			{
				data: new FieldDoc({
					code: "dateNow",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
		];
	}

	private static EbLivraisonLineTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "itemNumber",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "itemName",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "partNumber",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "serialNumber",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "quantity",
					type: this.getDisplayNameForType(FieldType.Long),
					desc: "",
				}),
			},
		];
	}

	private static EbLivraisonTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "deliveryRef",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "status",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "ownerUser",
					type: this.getDisplayNameForType(FieldType.EbUserTPL),
					desc: "",
				}),
				children: this.EbUserTPL(),
			},
			{
				data: new FieldDoc({
					code: "partyOrigin",
					type: this.getDisplayNameForType(FieldType.EbPartyTPL),
					desc: "",
				}),
				children: this.EbPartyTPL(),
			},
			{
				data: new FieldDoc({
					code: "partyDest",
					type: this.getDisplayNameForType(FieldType.EbPartyTPL),
					desc: "",
				}),
				children: this.EbPartyTPL(),
			},
			{
				data: new FieldDoc({
					code: "partySoldTo",
					type: this.getDisplayNameForType(FieldType.EbPartyTPL),
					desc: "",
				}),
				children: this.EbPartyTPL(),
			},
			{
				data: new FieldDoc({
					code: "dateOfGoodsAvailability",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "orderRef",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "customerOrderRef",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "transportRef",
					type: this.getDisplayNameForType(FieldType.String),
					desc: "",
				}),
			},
			{
				data: new FieldDoc({
					code: "units",
					type: this.getDisplayNameForType(FieldType.EbLivraisonLineTPL),
					desc: "",
					isList: true,
				}),
				children: this.EbLivraisonLineTPL(),
			},
		];
	}

	private static DeliveryRootTPL(): TreeNode[] {
		return [
			{
				data: new FieldDoc({
					code: "delivery",
					type: this.getDisplayNameForType(FieldType.EbLivraisonTPL),
					desc: "",
				}),
				children: this.EbLivraisonTPL(),
			},
			{
				data: new FieldDoc({
					code: "connectedUser",
					type: this.getDisplayNameForType(FieldType.EbUserTPL),
					desc: "",
				}),
				children: this.EbUserTPL(),
			},
			{
				data: new FieldDoc({
					code: "dateNow",
					type: this.getDisplayNameForType(FieldType.Date),
					desc: "",
				}),
			},
		];
	}
}
