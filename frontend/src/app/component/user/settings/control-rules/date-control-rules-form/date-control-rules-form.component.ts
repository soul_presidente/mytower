import {
	NatureDateEvent,
	CrOperator,
	CrOperationTypes,
	CrFieldTypes,
	CrTrGenerationOperator,
	CrRuleOperandType,
} from "@app/utils/enumeration";
import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { Statique } from "./../../../../../utils/statique";
import { EbTypeFlux } from "@app/classes/EbTypeFlux";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { EbControlRule } from "@app/classes/controlRule";
import { GenericEnum } from "@app/classes/GenericEnum";
import { EbControlRuleOperation } from "@app/classes/controlRuleOperation";
import { EbTtCategorieDeviation } from "@app/classes/categorieDeviation";
import { EbCategorie } from "@app/classes/categorie";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";

@Component({
	selector: "app-date-control-rules-form",
	templateUrl: "./date-control-rules-form.component.html",
	styleUrls: ["./date-control-rules-form.component.scss"],
})
export class DateControlRulesFormComponent implements OnInit {
	Statique = Statique;

	NatureDateEvent = NatureDateEvent;

	CrOperator = CrOperator;

	@Input()
	selectedEbTypeFlux: EbTypeFlux = null;

	@Input()
	selectedSchemaPSL: EbTtSchemaPsl = null;

	@Input()
	listTypeflux: Array<EbTypeFlux>;

	@Input()
	listSchemaPsl: Array<EbTtSchemaPsl> = new Array<EbTtSchemaPsl>();

	@Input()
	listTypeRequest: Array<EbTypeRequestDTO> = new Array<EbTypeRequestDTO>();

	@Input()
	listSchemaPslFiltered: Array<EbTtSchemaPsl> = new Array<EbTtSchemaPsl>();

	@Input()
	listOperator: Array<GenericEnum> = null;

	@Input()
	listPriority: Array<GenericEnum> = null;

	@Input()
	listPslTypeRight: Array<GenericEnum> = null;

	@Input()
	listPslTypeLeft: Array<GenericEnum> = null;

	@Input()
	listRuleOperator: Array<GenericEnum> = null;

	@Input()
	listCategorieDeviation: Array<EbTtCategorieDeviation> = new Array<EbTtCategorieDeviation>();

	@Input()
	controlRule: EbControlRule = new EbControlRule();

	@Input()
	listUnitFieldsForConditions: Array<any> = null;

	listElement: Array<any> = [];

	listPsl: Array<any> = new Array<any>();

	selectedPsl: Array<any> = new Array<any>();

	champ: any;

	modalLexiqueVisible = false;

	emailDestinataires: Array<string> = null;

	@Input()
	listConditionCrFields: Array<any> = null;
	@Input()
	listOperatorForDocRulesConditions: Array<GenericEnum> = null;

	@Input()
	listConditionGenericFields: Array<any> = null;

	@Input()
	listCategories: Array<EbCategorie>;

	@Input()
	listFields: Array<Object>;

	@Input()
	listUnitFields: Array<any>;

	@Input()
	listTypeDoc: Array<EbTypeDocuments> = new Array<EbTypeDocuments>();

	CrFieldTypes = CrFieldTypes;

	CrTrGenerationOperator = CrTrGenerationOperator;

	CrRuleOperandType = CrRuleOperandType;

	CrOperationTypes = CrOperationTypes;

	@Input()
	unitGenericFieldsForActions: Array<any> = null;

	@Input()
	unitGenericFieldsForConditions: Array<any> = null;

	@Input()
	allCrFields: Array<any> = null;

	@Output()
	emailDestinataireUpdated: EventEmitter<string> = new EventEmitter<string>();

	constructor() {}

	ngOnInit() {
		if (this.controlRule && this.controlRule.ebControlRuleNum) {
			this.prepareDateRuleForEdit(this.controlRule);
		}
	}

	selectedTypeFlux(typeFlux: EbTypeFlux) {
		this.listSchemaPslFiltered = this.listSchemaPsl.map((it) => it);
		if (this.listSchemaPsl) {
			this.listSchemaPslFiltered = this.listSchemaPsl.filter(
				(it) =>
					it &&
					(!typeFlux.listEbTtSchemaPsl ||
						!typeFlux.listEbTtSchemaPsl.length ||
						(typeFlux.listEbTtSchemaPsl &&
							typeFlux.listEbTtSchemaPsl.indexOf(":" + it.ebTtSchemaPslNum + ":") >= 0))
			);
		}

		if (Statique.isDefined(typeFlux)) {
			this.controlRule.xEbTypeFlux = new EbTypeFlux();
			this.controlRule.xEbTypeFlux.ebTypeFluxNum = typeFlux.ebTypeFluxNum;
		}
	}

	selectedSchemaPsl(schemaPsl: EbTtSchemaPsl) {
		if (schemaPsl && schemaPsl.listPsl != null) {
			this.selectedSchemaPSL = schemaPsl;
			this.listPsl = schemaPsl.listPsl;
			this.selectedPsl = this.listPsl
				.filter((it) => it.schemaActif)
				.map((it) => it.ebTtCompanyPslNum);

			this.controlRule.xEbSchemaPsl = new EbTtSchemaPsl();
			this.controlRule.xEbSchemaPsl.ebTtSchemaPslNum = schemaPsl.ebTtSchemaPslNum;
		} else {
			this.listPsl = null;
			this.selectedPsl = null;
		}
	}

	addElement(listOperations: EbControlRuleOperation[]) {
		let ruleOperation = new EbControlRuleOperation();
		ruleOperation.operationType = CrOperationTypes.IS_DATE_CONDITION;
		ruleOperation.rightOperandValues = null;
		ruleOperation.leftOperandType = null;

		listOperations.push(ruleOperation);
	}

	deleteRegle(i: number) {
		this.controlRule.listDateOperations.splice(i, 1);
	}

	getlistPslTypesLeft(pslCodeAlpha: string) {
		if (
			!Statique.isDefined(pslCodeAlpha) ||
			!Statique.isDefined(this.listPsl) ||
			this.listPsl.length == 0
		)
			return this.listPslTypeLeft.filter((it) => Number(it.code) != NatureDateEvent.CHAMP);
		let psl = this.listPsl.find((psl) => psl.codeAlpha == pslCodeAlpha);
		return Statique.isDefined(psl.champ) && psl.champ
			? this.listPslTypeLeft
			: this.listPslTypeLeft.filter((it) => Number(it.code) != NatureDateEvent.CHAMP);
	}

	getlistPslTypesRight(pslCodeAlpha: string) {
		if (
			!Statique.isDefined(pslCodeAlpha) ||
			!Statique.isDefined(this.listPsl) ||
			this.listPsl.length == 0
		)
			return this.listPslTypeRight.filter((it) => Number(it.code) != NatureDateEvent.CHAMP);
		let psl = this.listPsl.find((psl) => psl.codeAlpha == pslCodeAlpha);
		return Statique.isDefined(psl.champ) && psl.champ
			? this.listPslTypeRight
			: this.listPslTypeRight.filter((it) => Number(it.code) != NatureDateEvent.CHAMP);
	}

	get showMessageTemplate() {
		return this.controlRule.genererNotif || this.controlRule.envoyerEmail;
	}

	get showDevitionInput() {
		return this.controlRule.genererIncident;
	}

	addEmail(label) {
		return label;
	}

	prepareDateRuleForEdit(cr: EbControlRule) {
		this.selectedEbTypeFlux = this.controlRule.xEbTypeFlux;
		this.selectedSchemaPSL = this.controlRule.xEbSchemaPsl;
		this.selectedSchemaPsl(this.selectedSchemaPSL);
		this.controlRule.margin =
			Statique.getDays(this.controlRule.margin) +
			":" +
			Statique.getHours(this.controlRule.margin) +
			":" +
			Statique.getMinutes(this.controlRule.margin);
		if (this.controlRule.emailDestinataires)
			this.emailDestinataires = this.controlRule.emailDestinataires.split(";");
		if (this.controlRule.listRuleOperations) {
			// date rules
			let listGeneratedDateOperations = this.controlRule.listRuleOperations.filter(
				(op) => op.operationType == CrOperationTypes.IS_DATE_CONDITION
			);
			listGeneratedDateOperations.forEach((element, i) => {
				element.leftOperandObj = {};
				element.rightOperandObj = {};
				if (element.leftOperands && element.leftOperands.length > 0) {
					let item = element.leftOperands.split(";");
					element.leftOperandObj.typeDate = +item[0];
					if (item.length > 1) element.leftOperandObj.pslCodeAlpha = item[1];
				}

				if (element.rightOperands && element.rightOperands.length > 0) {
					let item = element.rightOperands.split(";");
					element.rightOperandObj.typeDate = +item[0];
					if (item.length > 1) element.rightOperandObj.pslCodeAlpha = item[1];
				}
			});
			this.controlRule.listDateOperations = listGeneratedDateOperations.sort(
				(it1, it2) => it1.ebControlRuleOperationNum - it2.ebControlRuleOperationNum
			);

			let listConditionOperations = this.controlRule.listRuleOperations.filter(
				(op) => op.operationType == CrOperationTypes.IS_FILE_CONDITION
			);

			if (Statique.isDefined(listConditionOperations)) {
				this.controlRule.listRuleOperations = listConditionOperations;
				this.controlRule.listRuleOperations = this.controlRule.listRuleOperations.sort(
					(it1, it2) => it1.ebControlRuleOperationNum - it2.ebControlRuleOperationNum
				);

				this.controlRule.listRuleOperations.forEach((element) => {
					element = this.setConditionElementValues(element);
				});
			}
		}
	}

	// --------------------------------------------------------------------------------------
	addCondition(listOperations: EbControlRuleOperation[]) {
		let ruleOperation = new EbControlRuleOperation();

		if (listOperations.length == 0) {
			ruleOperation.ruleOperator = null;
		} else {
			ruleOperation.ruleOperator = 1;
		}
		let listConditionCrFieldsFilter = this.listConditionCrFields.filter(
			(it) => it.code != 14 && it.code != 24
		);
		ruleOperation.operationType = CrOperationTypes.IS_FILE_CONDITION;
		ruleOperation.leftOperandValues = listConditionCrFieldsFilter;
		ruleOperation.listOfOperators = this.listOperatorForDocRulesConditions;
		ruleOperation.rightOperandValues = null;

		listOperations.push(ruleOperation);
	}

	deleteCondition(i: number) {
		this.controlRule.listRuleOperations.splice(i, 1);
	}

	resetElementConditionInputData(element: EbControlRuleOperation) {
		element.operator = null;
		element.rightOperands = null;
		element.rightOperandValues = null;
		element.ebCategorieNum = null;
		element.leftOperandType = null;

		if (
			Statique.isDefined(element.leftOperands) &&
			Statique.isDefined(this.listConditionGenericFields)
		) {
			let field = this.listConditionGenericFields.find((f) => f.code == element.leftOperands);
			element.listOfOperators = field.operators;
			element.leftOperandType = field.type;

			if (element.leftOperandType == CrFieldTypes.CAT) {
				element.listCategories = this.listCategories;
			} else if (element.leftOperandType == CrFieldTypes.CUSTOMFIELD) {
				element.listCustomFields = this.listFields;
				element.rightOperandValues = field.rightOperandValues;
			} else if (element.leftOperandType == CrFieldTypes.UNITS) {
				element.listUnits = this.listUnitFields;
			} else {
				element.rightOperandValues = field.rightOperandValues;
			}
		} else {
			element.listOfOperators = this.listOperatorForDocRulesConditions;
		}
	}

	setListLabels(element: EbControlRuleOperation) {
		element.rightOperandValues = null;
		if (
			element &&
			element.ebCategorieNum &&
			this.listCategories &&
			this.listCategories.length > 0
		) {
			let categorie = this.listCategories.find((c) => c.ebCategorieNum == element.ebCategorieNum);
			if (categorie && categorie.labels && categorie.labels.length > 0) {
				let genericField: any;
				element.rightOperandValues = new Array<any>();
				categorie.labels.forEach((l) => {
					genericField = {};
					genericField.code = l.ebLabelNum;
					genericField.key = l.libelle;
					element.rightOperandValues.push(genericField);
				});
			}
		}
	}

	loadListOperatorsForUnitField(element: EbControlRuleOperation, operationType: number) {
		element.operator = null;
		element.rightOperands = null;

		if (element && element.leftUnitFieldCode) {
			let unit = null;
			if (operationType == CrOperationTypes.IS_DATE_CONDITION)
				unit = this.unitGenericFieldsForActions.find(
					(field) => field.code == element.leftUnitFieldCode
				);

			if (operationType == CrOperationTypes.IS_FILE_CONDITION) {
				unit = this.unitGenericFieldsForConditions.find(
					(field) => field.code == element.leftUnitFieldCode
				);
				element.rightOperandValues = unit.values;
			}
			element.listOfOperators = unit.operators;
		}
	}
	isNormalField(element: EbControlRuleOperation) {
		let field = this.allCrFields.find((t) => t.code == element.leftOperands);
		return +field.type == CrFieldTypes.STRING || +field.type == CrFieldTypes.NUMBER;
	}

	checkUnitFieldType(element: EbControlRuleOperation, type: number) {
		if (this.listUnitFields && element.leftUnitFieldCode) {
			let unit = this.listUnitFields.find((unit) => unit.code == element.leftUnitFieldCode);
			if (!unit) return false;
			if (type != CrFieldTypes.LIST) return unit.type == type;
			else
				return (
					unit.type == CrFieldTypes.COUNTRY ||
					unit.type == CrFieldTypes.UNIT_TYPE ||
					unit.type == CrFieldTypes.GOOD_TYPE ||
					unit.type == CrFieldTypes.OVERSIZED_ITEM ||
					unit.type == CrFieldTypes.LARGE_ITEM ||
					unit.type == CrFieldTypes.DGR ||
					unit.type == CrFieldTypes.CLASS
				);
		}
	}

	setConditionElementValues(element: EbControlRuleOperation): EbControlRuleOperation {
		let field = this.listConditionGenericFields.find((f) => f.code == element.leftOperands);
		if (field) {
			element.leftOperandValues = this.listConditionCrFields;
			element.listOfOperators = field.operators;
			element.leftOperandType = field.type;

			if (+field.type == CrFieldTypes.CAT) {
				element.listCategories = field.listCategories;
				this.setListLabels(element);
			} else if (element.leftOperandType == CrFieldTypes.CUSTOMFIELD) {
				element.listCustomFields = field.listFields;
				element.rightOperandValues = field.rightOperandValues;
			} else if (element.leftOperandType == CrFieldTypes.UNITS) {
				element.listUnits = this.listUnitFields;
				let field = this.unitGenericFieldsForConditions.find(
					(field) => field.code == element.leftUnitFieldCode
				);
				element.listOfOperators = field.operators;
				element.rightOperandValues = field.values;
			} else {
				element.rightOperandValues = field.rightOperandValues;
			}
		}
		return element;
	}

	emitEmailDestinataire($event) {
		this.emailDestinataireUpdated.emit($event);
	}
}
