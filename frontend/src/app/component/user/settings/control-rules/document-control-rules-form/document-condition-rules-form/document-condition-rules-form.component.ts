import { Component, OnInit, Input } from "@angular/core";
import { EbControlRule } from "@app/classes/controlRule";
import { GenericEnum } from "@app/classes/GenericEnum";
import { EbControlRuleOperation } from "@app/classes/controlRuleOperation";
import { Statique } from "@app/utils/statique";
import { CrFieldTypes, CrOperationTypes } from "@app/utils/enumeration";
import { EbCategorie } from "@app/classes/categorie";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";

@Component({
	selector: "app-document-condition-rules-form",
	templateUrl: "./document-condition-rules-form.component.html",
	styleUrls: ["./document-condition-rules-form.component.scss"],
})
export class DocumentConditionRulesFormComponent implements OnInit {
	@Input()
	controlRule: EbControlRule = new EbControlRule();

	@Input()
	element: EbControlRuleOperation = new EbControlRuleOperation();

	@Input()
	listOperatorForDocRulesConditions: Array<GenericEnum> = null;

	@Input()
	listRuleOperator: Array<GenericEnum> = null;

	@Input()
	listConditionGenericFields: Array<any> = null;

	@Input()
	listCategories: Array<EbCategorie>;

	@Input()
	listFields: Array<Object>;

	@Input()
	listUnitFieldsForConditions: Array<any> = null;

	@Input()
	unitGenericFieldsForConditions: Array<any> = null;

	@Input()
	allCrFields: Array<any> = null;

	@Input()
	listUnitFields: Array<any>;

	@Input()
	index: number;

	@Input()
	listTypeDoc: Array<EbTypeDocuments> = new Array<EbTypeDocuments>();

	CrFieldTypes = CrFieldTypes;

	CrOperationTypes = CrOperationTypes;

	constructor() {}

	ngOnInit() {}

	deleteRegle(listOperations: EbControlRuleOperation[], i: number) {
		listOperations.splice(i, 1);
	}

	resetElementConditionInputData(element: EbControlRuleOperation) {
		element.operator = null;
		element.rightOperands = null;
		element.rightOperandValues = null;
		element.ebCategorieNum = null;
		element.leftOperandType = null;
		element.leftUnitFieldCode = null;

		if (
			Statique.isDefined(element.leftOperands) &&
			Statique.isDefined(this.listConditionGenericFields)
		) {
			let field = this.listConditionGenericFields.find((f) => f.code == element.leftOperands);
			element.listOfOperators = field.operators;
			element.leftOperandType = field.type;

			if (element.leftOperandType == CrFieldTypes.CAT) {
				element.listCategories = this.listCategories;
			} else if (element.leftOperandType == CrFieldTypes.CUSTOMFIELD) {
				element.listCustomFields = this.listFields;
				element.rightOperandValues = field.rightOperandValues;
			} else if (element.leftOperandType == CrFieldTypes.UNITS) {
				element.listUnits = this.listUnitFieldsForConditions;
			} else if (element.leftOperandType == CrFieldTypes.TYPE_OF_DOC) {
				element.listDocTypes = this.listTypeDoc;
			} else {
				element.rightOperandValues = field.rightOperandValues;
			}
		} else {
			element.listOfOperators = this.listOperatorForDocRulesConditions;
		}
	}

	setListLabels(element: EbControlRuleOperation) {
		element.rightOperandValues = null;
		if (
			element &&
			element.ebCategorieNum &&
			this.listCategories &&
			this.listCategories.length > 0
		) {
			let categorie = this.listCategories.find((c) => c.ebCategorieNum == element.ebCategorieNum);
			if (categorie && categorie.labels && categorie.labels.length > 0) {
				let genericField: any;
				element.rightOperandValues = new Array<any>();
				categorie.labels.forEach((l) => {
					genericField = {};
					genericField.code = l.ebLabelNum;
					genericField.key = l.libelle;
					element.rightOperandValues.push(genericField);
				});
			}
		}
	}

	loadListOperatorsForUnitField(element: EbControlRuleOperation, operationType: number) {
		element.operator = null;
		element.rightOperands = null;

		if (element && Statique.isDefined(element.leftUnitFieldCode)) {
			let unit = null;

			if (operationType == CrOperationTypes.IS_FILE_CONDITION) {
				unit = this.unitGenericFieldsForConditions.find(
					(field) => field.code == element.leftUnitFieldCode
				);
				element.rightOperandValues = unit.values;
			}
			element.listOfOperators = unit.operators;
		}
	}

	isNormalField(element: EbControlRuleOperation) {
		let field = this.allCrFields.find((t) => t.code == element.leftOperands);
		return +field.type == CrFieldTypes.STRING || +field.type == CrFieldTypes.NUMBER;
	}

	checkUnitFieldType(element: EbControlRuleOperation, type: number) {
		if (this.listUnitFields && element.leftUnitFieldCode) {
			let unit = this.listUnitFields.find((unit) => unit.code == element.leftUnitFieldCode);
			if (!unit) return false;
			if (type != CrFieldTypes.LIST) return unit.type == type;
			else
				return (
					unit.type == CrFieldTypes.COUNTRY ||
					unit.type == CrFieldTypes.UNIT_TYPE ||
					unit.type == CrFieldTypes.GOOD_TYPE ||
					unit.type == CrFieldTypes.OVERSIZED_ITEM ||
					unit.type == CrFieldTypes.LARGE_ITEM ||
					unit.type == CrFieldTypes.DGR ||
					unit.type == CrFieldTypes.CLASS
				);
		}
	}
}
