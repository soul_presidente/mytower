import { Statique } from "./../../../../../utils/statique";
import {
	CrOperationTypes,
	CrFieldTypes,
	TemplateModels,
	CrTrGenerationOperator,
	CrRuleOperandType,
} from "./../../../../../utils/enumeration";
import { Component, OnInit, Input, EventEmitter, Output } from "@angular/core";
import { EbControlRule } from "@app/classes/controlRule";
import { GenericEnum } from "@app/classes/GenericEnum";
import { EbTypeFlux } from "@app/classes/EbTypeFlux";
import { EbIncoterm } from "@app/classes/EbIncoterm";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { EbControlRuleOperation } from "@app/classes/controlRuleOperation";
import { EbCategorie } from "@app/classes/categorie";
import { TemplateGenParamsDTO } from "@app/classes/TemplateGenParamsDTO";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { EbTemplateDocumentsDTO } from "@app/classes/dto/ebTemplateDocumentsDTO";
import { EcCountry } from "@app/classes/country";
import { EbTypeGoods } from "@app/classes/EbTypeGoods";
import { ParamsMail } from "@app/classes/paramsMail";

@Component({
	selector: "app-document-control-rules-form",
	templateUrl: "./document-control-rules-form.component.html",
	styleUrls: ["./document-control-rules-form.component.scss"],
})
export class DocumentControlRulesFormComponent implements OnInit {
	@Input()
	controlRule: EbControlRule = new EbControlRule();

	@Input()
	listOperatorForDocRulesConditions: Array<GenericEnum> = null;

	@Input()
	listRuleOperator: Array<GenericEnum> = null;

	@Input()
	allCrFields: Array<any> = null;

	@Input()
	listConditionCrFields: Array<any> = null;

	@Input()
	listActionCrFields: Array<any> = null;

	@Input()
	listConditionGenericFields: Array<any> = null;

	@Input()
	listTypeflux: Array<EbTypeFlux> = new Array<EbTypeFlux>();

	@Input()
	listModeTr: Array<any> = null;

	@Input()
	listIncoterms: Array<EbIncoterm> = null;

	@Input()
	listStatus: Array<any> = null;

	@Input()
	listTypeRequests: Array<EbTypeRequestDTO> = null;

	@Input()
	listCategories: Array<EbCategorie>;

	@Input()
	listFields: Array<Object>;

	@Input()
	listOperatorForDocRulesActions: Array<GenericEnum> = null;

	@Input()
	listActionGenericFields: Array<any> = null;

	@Input()
	listTypeDoc: Array<EbTypeDocuments> = new Array<EbTypeDocuments>();

	@Input()
	listDocumentStatus: Array<GenericEnum> = new Array<GenericEnum>();

	@Input()
	docGenTemplates: Array<EbTemplateDocumentsDTO> = [];

	@Input()
	listUnitFields: Array<any>;

	@Input()
	listExportControlStatus: Array<any>;

	@Input()
	listCountries: Array<EcCountry>;

	@Input()
	listTypeUnit: Array<any>;

	@Input()
	listTypeGoods: Array<EbTypeGoods> = [];

	@Input()
	marchandiseClassGood: Array<any>;

	@Input()
	marchandiseDangerousGood: Map<number, string>;

	@Input()
	unitGenericFieldsForActions: Array<any> = null;

	@Input()
	unitGenericFieldsForConditions: Array<any> = null;

	@Input()
	listUnitFieldsForConditions: Array<any> = null;

	@Input()
	listUnitFieldsForActions: Array<any> = null;

	@Input()
	listMails: Array<ParamsMail> = new Array<ParamsMail>();

	@Output()
	typeDocsUpdated: EventEmitter<EbTypeDocuments> = new EventEmitter<EbTypeDocuments>();

	@Output()
	selectedMailsUpdated: EventEmitter<ParamsMail> = new EventEmitter<ParamsMail>();

	@Output()
	emailDestinataireUpdated: EventEmitter<string> = new EventEmitter<string>();

	selectedMails: Array<ParamsMail> = null;

	emailDestinataires: Array<string> = null;

	selectedListTypeDoc: Array<EbTypeDocuments> = new Array<EbTypeDocuments>();

	isEditTypeDoc: boolean = false;

	CrOperationTypes = CrOperationTypes;

	Statique = Statique;

	CrFieldTypes = CrFieldTypes;

	CrTrGenerationOperator = CrTrGenerationOperator;

	CrRuleOperandType = CrRuleOperandType;

	constructor() {}

	ngOnInit() {
		if (this.controlRule && this.controlRule.ebControlRuleNum) {
			this.prepareDocRuleForEdit(this.controlRule);
		}
	}

	addCondition(listOperations: EbControlRuleOperation[]) {
		let ruleOperation = new EbControlRuleOperation();

		if (listOperations.length == 0) {
			ruleOperation.ruleOperator = null;
		} else {
			ruleOperation.ruleOperator = 1;
		}

		ruleOperation.operationType = CrOperationTypes.IS_FILE_CONDITION;
		ruleOperation.leftOperandValues = this.listConditionCrFields;
		ruleOperation.listOfOperators = this.listOperatorForDocRulesConditions;
		ruleOperation.rightOperandValues = null;

		listOperations.push(ruleOperation);
	}

	deleteRegle(listOperations: EbControlRuleOperation[], i: number) {
		listOperations.splice(i, 1);
	}

	resetElementConditionInputData(element: EbControlRuleOperation) {
		element.operator = null;
		element.rightOperands = null;
		element.rightOperandValues = null;
		element.ebCategorieNum = null;
		element.leftOperandType = null;

		if (
			Statique.isDefined(element.leftOperands) &&
			Statique.isDefined(this.listConditionGenericFields)
		) {
			let field = this.listConditionGenericFields.find((f) => f.code == element.leftOperands);
			element.listOfOperators = field.operators;
			element.leftOperandType = field.type;

			if (element.leftOperandType == CrFieldTypes.CAT) {
				element.listCategories = this.listCategories;
			} else if (element.leftOperandType == CrFieldTypes.CUSTOMFIELD) {
				element.listCustomFields = this.listFields;
				element.rightOperandValues = field.rightOperandValues;
			} else if (element.leftOperandType == CrFieldTypes.UNITS) {
				element.listUnits = this.listUnitFieldsForConditions;
			} else {
				element.rightOperandValues = field.rightOperandValues;
			}
		} else {
			element.listOfOperators = this.listOperatorForDocRulesConditions;
		}
	}

	resetActionElementInputData(element: EbControlRuleOperation) {
		element.operator = null;
		element.rightOperands = null;
		element.rightOperandValues = null;
		element.ebCategorieNum = null;
		element.leftOperandType = null;

		if (
			Statique.isDefined(element.leftOperands) &&
			Statique.isDefined(this.listActionGenericFields)
		) {
			let field = this.listActionGenericFields.find((f) => f.code == element.leftOperands);
			element.listOfOperators = field.operators;
			element.leftOperandType = field.type;

			if (element.leftOperandType == CrFieldTypes.CAT) {
				element.listCategories = this.listCategories;
			} else if (element.leftOperandType == CrFieldTypes.CUSTOMFIELD) {
				element.listCustomFields = this.listFields;
				element.rightOperandValues = field.rightOperandValues;
			} else if (element.leftOperandType == CrFieldTypes.UNITS) {
				element.listUnits = this.listUnitFieldsForActions;
			} else {
				element.rightOperandValues = field.rightOperandValues;
			}
		} else {
			element.listOfOperators = this.listOperatorForDocRulesActions;
		}
	}

	setListRightValues(element: EbControlRuleOperation) {
		element.rightOperands = null;

		if (Statique.isDefined(element) && Statique.isDefined(element.operator)) {
			if (Statique.isDefined(element.leftOperandObj)) {
				if (element.leftOperandType == CrFieldTypes.UNITS && element.leftUnitFieldCode) {
					element = this.setUnitFieldRightValues(element);
				} else if (element.leftOperandType == CrFieldTypes.TYPE_OF_REQ) {
					element = this.setFieldRightValues(element, CrFieldTypes.TYPE_OF_REQ);
				}
			}
		}
	}

	setFieldRightValues(element: EbControlRuleOperation, fieldType: number): EbControlRuleOperation {
		let field = this.listActionGenericFields.find((f) => f.code == element.leftOperands);
		if (element.operator == CrTrGenerationOperator.BECOMES) {
			element.rightOperandValues = field.rightOperandValues;
		} else if (element.operator == CrTrGenerationOperator.INTERCHANGE) {
			element.rightOperandValues = this.listActionCrFields.filter((v) => v.type == fieldType);
		}
		return element;
	}

	setUnitFieldRightValues(element: EbControlRuleOperation): EbControlRuleOperation {
		if (element.operator == CrTrGenerationOperator.INTERCHANGE) {
			let field = this.listUnitFields.find((field) => field.code == element.leftUnitFieldCode);
			element.rightOperandValues = this.listUnitFields.filter((v) => v.type == field.type);
		}
		if (element.operator == CrTrGenerationOperator.BECOMES) {
			let field = this.unitGenericFieldsForActions.find(
				(field) => field.code == element.leftUnitFieldCode
			);
			element.rightOperandValues = field.values;
		}
		return element;
	}

	setListLabels(element: EbControlRuleOperation) {
		element.rightOperandValues = null;
		if (
			element &&
			element.ebCategorieNum &&
			this.listCategories &&
			this.listCategories.length > 0
		) {
			let categorie = this.listCategories.find((c) => c.ebCategorieNum == element.ebCategorieNum);
			if (categorie && categorie.labels && categorie.labels.length > 0) {
				let genericField: any;
				element.rightOperandValues = new Array<any>();
				categorie.labels.forEach((l) => {
					genericField = {};
					genericField.code = l.ebLabelNum;
					genericField.key = l.libelle;
					element.rightOperandValues.push(genericField);
				});
			}
		}
	}

	isNormalField(element: EbControlRuleOperation) {
		let field = this.allCrFields.find((t) => t.code == element.leftOperands);
		return +field.type == CrFieldTypes.STRING || +field.type == CrFieldTypes.NUMBER;
	}

	initDocGenParams() {
		this.controlRule.docGenParams = new TemplateGenParamsDTO();
		this.controlRule.docGenParams.templateModelNum = TemplateModels.PricingItem;
	}

	initListTrOperation() {
		this.controlRule.listGeneratedTrOperations = new Array<EbControlRuleOperation>();
		if (this.controlRule.generateTr) {
			this.addAction(this.controlRule.listGeneratedTrOperations);
		}
	}

	addAction(listOperations: EbControlRuleOperation[]) {
		let ruleOperation = new EbControlRuleOperation();

		ruleOperation.ruleOperator = null;
		ruleOperation.operationType = CrOperationTypes.IS_TR_GENERATION;
		ruleOperation.leftOperandValues = this.listActionCrFields;
		ruleOperation.listOfOperators = this.listOperatorForDocRulesActions;
		ruleOperation.rightOperandValues = null;
		ruleOperation.leftOperandType = null;

		listOperations.push(ruleOperation);
	}

	get showMessageTemplate() {
		return this.controlRule.genererNotif || this.controlRule.envoyerEmail;
	}

	get showTrGenerationOperations() {
		return (
			this.controlRule.listGeneratedTrOperations &&
			this.controlRule.listGeneratedTrOperations.length > 0 &&
			this.controlRule.generateTr
		);
	}

	addEmail(label) {
		return label;
	}

	prepareDocRuleForEdit(cr: EbControlRule) {
		if (cr.listRuleOperations) {
			let listConditionOperations = cr.listRuleOperations.filter(
				(op) => op.operationType == CrOperationTypes.IS_FILE_CONDITION
			);
			let listGeneratedTrOperations = cr.listRuleOperations.filter(
				(op) => op.operationType == CrOperationTypes.IS_TR_GENERATION
			);

			// preparing conditions values
			if (Statique.isDefined(listConditionOperations)) {
				cr.listRuleOperations = listConditionOperations;
				cr.listRuleOperations = cr.listRuleOperations.sort(
					(it1, it2) => it1.ebControlRuleOperationNum - it2.ebControlRuleOperationNum
				);

				cr.listRuleOperations.forEach((element) => {
					element = this.setConditionElementValues(element);
				});
			}

			// preparing actions values
			if (Statique.isDefined(listGeneratedTrOperations) && cr.generateTr) {
				cr.listGeneratedTrOperations = listGeneratedTrOperations;
				cr.listGeneratedTrOperations = cr.listGeneratedTrOperations.sort(
					(it1, it2) => it1.ebControlRuleOperationNum - it2.ebControlRuleOperationNum
				);

				cr.listGeneratedTrOperations.forEach((element) => {
					element = this.setActionElementValues(element);
				});
			}
		}

		if (this.controlRule.listTypeDocStatus && this.controlRule.listTypeDocStatus.length) {
			this.selectedListTypeDoc = this.controlRule.listTypeDocStatus.split("|").map((it) => {
				let arr = it.split(":");
				let el = this.listTypeDoc.find((p) => p.ebTypeDocumentsNum == +arr[0]);
				el.status = +arr[1] || null;
				return el;
			});
		}

		if (this.controlRule.sendEmailAlert) {
			if (this.controlRule.selectedMailNums && this.controlRule.selectedMailNums) {
				let selectedMailNums = this.controlRule.selectedMailNums.split(":").map((mailId) => {
					if (mailId != "") return Number(mailId);
				});

				this.selectedMails = this.listMails.filter((mail) =>
					selectedMailNums.includes(mail.emailId)
				);

				this.emitSelectedMailsValue(this.selectedMails);
			}

			if (this.controlRule.emailDestinataires) {
				this.emailDestinataires = this.controlRule.emailDestinataires.split(";");
				this.emitEmailDestinatire(this.emailDestinataires);
			}
		}

		if (this.controlRule.envoyerEmail || this.controlRule.genererNotif) {
			if (this.controlRule.emailDestinataires && this.controlRule.emailDestinataires.length > 0) {
				this.emailDestinataires = this.controlRule.emailDestinataires.split(";");
				this.emitEmailDestinatire(this.emailDestinataires);
			}
		}
	}

	setConditionElementValues(element: EbControlRuleOperation): EbControlRuleOperation {
		let field = this.listConditionGenericFields.find((f) => f.code == element.leftOperands);
		if (field) {
			element.leftOperandValues = this.listConditionCrFields;
			element.listOfOperators = field.operators;
			element.leftOperandType = field.type;

			if (+field.type == CrFieldTypes.CAT) {
				element.listCategories = field.listCategories;
				this.setListLabels(element);
			} else if (element.leftOperandType == CrFieldTypes.CUSTOMFIELD) {
				element.listCustomFields = field.listFields;
				element.rightOperandValues = field.rightOperandValues;
			} else if (element.leftOperandType == CrFieldTypes.UNITS) {
				element.listUnits = this.listUnitFieldsForConditions;
				let field = this.unitGenericFieldsForConditions.find(
					(field) => field.code == element.leftUnitFieldCode
				);
				element.listOfOperators = field.operators;
				element.rightOperandValues = field.values;
			} else if (element.leftOperandType == CrFieldTypes.TYPE_OF_DOC) {
				element.listDocTypes = this.listTypeDoc;
			} else {
				element.rightOperandValues = field.rightOperandValues;
			}
		}
		return element;
	}

	setActionElementValues(element: EbControlRuleOperation): EbControlRuleOperation {
		let field = this.listActionGenericFields.find((f) => f.code == element.leftOperands);
		element.leftOperandValues = this.listActionCrFields;
		element.listOfOperators = field.operators;
		element.leftOperandType = field.type;

		if (+field.type == CrFieldTypes.CAT) {
			element.listCategories = field.listCategories;
			this.setListLabels(element);
		} else if (element.leftOperandType == CrFieldTypes.CUSTOMFIELD) {
			element.listCustomFields = field.listFields;
			element.rightOperandValues = field.rightOperandValues;
		} else if (element.leftOperandType == CrFieldTypes.UNITS) {
			element.listUnits = this.listUnitFieldsForActions;
			let field = this.unitGenericFieldsForActions.find(
				(field) => field.code == element.leftUnitFieldCode
			);
			element.listOfOperators = field.operators;
			element = this.setUnitFieldRightValues(element);
		} else {
			element.rightOperandValues = field.rightOperandValues;
		}
		return element;
	}

	checkUnitFieldType(element: EbControlRuleOperation, type: number) {
		if (this.listUnitFields && element.leftUnitFieldCode) {
			let unit = this.listUnitFields.find((unit) => unit.code == element.leftUnitFieldCode);
			if (!unit) return false;
			if (type != CrFieldTypes.LIST) return unit.type == type;
			else
				return (
					unit.type == CrFieldTypes.COUNTRY ||
					unit.type == CrFieldTypes.UNIT_TYPE ||
					unit.type == CrFieldTypes.GOOD_TYPE ||
					unit.type == CrFieldTypes.OVERSIZED_ITEM ||
					unit.type == CrFieldTypes.LARGE_ITEM ||
					unit.type == CrFieldTypes.DGR ||
					unit.type == CrFieldTypes.CLASS
				);
		}
	}

	loadListOperatorsForUnitField(element: EbControlRuleOperation, operationType: number) {
		element.operator = null;
		element.rightOperands = null;

		if (element && Statique.isDefined(element.leftUnitFieldCode)) {
			let unit = null;
			if (operationType == CrOperationTypes.IS_TR_GENERATION)
				unit = this.unitGenericFieldsForActions.find(
					(field) => field.code == element.leftUnitFieldCode
				);

			if (operationType == CrOperationTypes.IS_FILE_CONDITION) {
				unit = this.unitGenericFieldsForConditions.find(
					(field) => field.code == element.leftUnitFieldCode
				);
				element.rightOperandValues = unit.values;
			}
			element.listOfOperators = unit.operators;
		}
	}

	emitSelectedMailsValue($event) {
		this.selectedMailsUpdated.emit($event);
	}

	emitEmailDestinatire($event) {
		this.emailDestinataireUpdated.emit($event);
	}

	emitSelectedTypeDocs($event) {
		this.typeDocsUpdated.emit($event);
	}
}
