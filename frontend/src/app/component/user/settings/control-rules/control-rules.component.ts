import { TemplateGenParamsDTO } from "@app/classes/TemplateGenParamsDTO";
import {
	CrFieldTypes,
	CrRuleOperandType,
	CrTrGenerationOperator,
	CrConditionOperator,
	TemplateModels,
	CrOperationTypes,
	TypeOfEvent,
} from "./../../../../utils/enumeration";
import { Component, OnInit, ViewChild, Output, EventEmitter } from "@angular/core";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { TranslateService } from "@ngx-translate/core";
import { Statique, keyValue } from "@app/utils/statique";
import { CrOperator, NatureDateEvent, CrRuleCategory } from "@app/utils/enumeration";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbCompagnie } from "@app/classes/compagnie";
import { Duration, duration } from "moment";
import { ModalService } from "@app/shared/modal/modal.service";
import { MessageService } from "primeng/api";
import { EbControlRule } from "@app/classes/controlRule";
import { ControlRuleService } from "@app/services/control-rule.service";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { EbTypeFlux } from "@app/classes/EbTypeFlux";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { ConfigTypeFluxService } from "@app/services/config-type-flux.service";
import { ConfigPslCompanyService } from "@app/services/config-psl-company.service";
import { TypeRequestService } from "@app/services/type-request.service";
import { EbTtPsl } from "@app/classes/Ttpsl";
import { EbTtCompanyPsl } from "@app/classes/ttCompanyPsl";
import { EtablissementService } from "@app/services/etablissement.service";
import { EbControlRuleOperation } from "@app/classes/controlRuleOperation";
import { TypesDocumentService } from "@app/services/types-document.service";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { StatiqueService } from "@app/services/statique.service";
import { GenericEnum } from "@app/classes/GenericEnum";
import { EbIncoterm } from "@app/classes/EbIncoterm";
import { EbCategorie } from "@app/classes/categorie";
import { EbTemplateDocumentsDTO } from "@app/classes/dto/ebTemplateDocumentsDTO";
import { EcCountry } from "@app/classes/country";
import { EbTypeGoods } from "@app/classes/EbTypeGoods";
import { KeyValuePipe } from "@angular/common";
import { EbCostCenter } from "@app/classes/costCenter";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbTtCategorieDeviation } from "@app/classes/categorieDeviation";
import { EbUser } from "@app/classes/user";
import { ParamsMail } from "@app/classes/paramsMail";

@Component({
	selector: "app-control-rules",
	templateUrl: "./control-rules.component.html",
	styleUrls: ["./control-rules.component.scss"],
	providers: [KeyValuePipe],
})
export class ControlRulesComponent extends ConnectedUserComponent implements OnInit {
	Statique = Statique;
	CrOperator = CrOperator;
	NatureDateEvent = NatureDateEvent;

	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	searchInput: SearchCriteria = new SearchCriteria();
	editMode: boolean = false;
	isAddEditMode = false;
	searchCriteria: SearchCriteria = new SearchCriteria();

	controlRule: EbControlRule = new EbControlRule();

	backupEditcontrolRule: EbControlRule;

	emailDestinataires: Array<string> = null;
	customerCodeError: string;

	listElement: Array<any> = [];

	listOperator: Array<GenericEnum> = null;
	listPriority: Array<GenericEnum> = null;
	listOperatorForDocRulesConditions: Array<GenericEnum> = null;

	listPslTypeRight: Array<GenericEnum> = null;
	listPslTypeLeft: Array<GenericEnum> = null;

	listRuleOperator: Array<GenericEnum> = null;

	listTypeflux: Array<EbTypeFlux> = new Array<EbTypeFlux>();

	listSchemaPsl: Array<EbTtSchemaPsl> = new Array<EbTtSchemaPsl>();

	listTypeRequest: Array<EbTypeRequestDTO> = new Array<EbTypeRequestDTO>();

	listSchemaPslFiltered: Array<EbTtSchemaPsl> = new Array<EbTtSchemaPsl>();

	listModeTr: Array<any> = null;

	listIncoterms: Array<EbIncoterm> = null;

	listStatus: Array<any> = null;

	listTypeRequests: Array<EbTypeRequestDTO> = null;

	listCategories: Array<EbCategorie>;

	listFields: Array<Object>;

	listRefAddresses: Array<string> = null;

	docGenTemplates: Array<EbTemplateDocumentsDTO> = [];

	listConditionGenericFields: Array<any> = null;

	allCrFields: Array<any> = null;

	listConditionCrFields: Array<any> = null;

	listActionCrFields: Array<any> = null;

	listUnitFields: Array<any> = null;

	listUnitFieldsForConditions: Array<any> = null;

	listUnitFieldsForActions: Array<any> = null;

	unitGenericFieldsForActions: Array<any> = null;

	unitGenericFieldsForConditions: Array<any> = null;

	listDocumentStatus: Array<GenericEnum> = new Array<GenericEnum>();

	listTypeDoc: Array<EbTypeDocuments> = new Array<EbTypeDocuments>();
	selectedListTypeDoc: Array<EbTypeDocuments> = new Array<EbTypeDocuments>();
	isEditTypeDoc: boolean = false;

	listActionGenericFields: Array<any> = null;

	selectedEbTypeFlux: EbTypeFlux = null;
	selectedSchemaPSL: EbTtSchemaPsl = null;
	listPsl: Array<any> = new Array<any>();
	selectedPsl: Array<any> = new Array<any>();
	champ: any;
	modalLexiqueVisible = false;

	listCrConditionsTypes: Array<GenericEnum>;
	selectedConditionType: number;

	CrRuleCategory = CrRuleCategory;

	CrFieldTypes = CrFieldTypes;
	CrRuleOperandType = CrRuleOperandType;
	CrTrGenerationOperator = CrTrGenerationOperator;
	listOperatorForDocRulesActions: Array<GenericEnum> = null;

	listRuleTrigger: Array<GenericEnum> = null;

	listTrigVal: any = {};

	genericListUnits: Array<any>;

	listExportControlStatus: Array<any>;

	listCountries: Array<EcCountry>;

	listTypeUnit: Array<any>;

	listTypeGoods: Array<EbTypeGoods> = [];

	marchandiseClassGood: Array<any>;

	marchandiseDangerousGood: Array<any>;

	listCostCenter: Array<EbCostCenter>;

	user: EbUser;

	listMails: Array<ParamsMail> = new Array<ParamsMail>();

	selectedMails: Array<ParamsMail> = null;

	listCategorieDeviation: Array<EbTtCategorieDeviation> = new Array<EbTtCategorieDeviation>();
	listCategorieDeviationFiltred: Array<EbTtCategorieDeviation> = new Array<
		EbTtCategorieDeviation
	>();

	ebControlRuleNum: number;

	constructor(
		protected controlRuleService: ControlRuleService,
		protected headerService: HeaderService,
		protected translate: TranslateService,
		private messageService: MessageService,
		protected modalService: ModalService,
		protected etablissementService: EtablissementService,
		protected configPslCompanyService: ConfigPslCompanyService,
		protected typeRequestService: TypeRequestService,
		protected typeDocument: TypesDocumentService,
		protected statiqueService: StatiqueService,
		protected keyValuePipe: KeyValuePipe
	) {
		super();
	}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.searchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		this.etablissementService.listTypeFlux(this.searchCriteria).subscribe((data) => {
			this.listTypeflux = data;
		});
		this.configPslCompanyService.getListSchemaPslTable(this.searchCriteria).subscribe((data) => {
			this.listSchemaPsl = data;
			this.listSchemaPslFiltered = data;
		});

		this.typeRequestService.getListTypeRequests(this.searchCriteria).subscribe((data) => {
			this.listTypeRequest = data;
		});

		this.controlRuleService.getListConditionCRTypeEnum().subscribe((data: GenericEnum[]) => {
			this.listCrConditionsTypes = data;
		});

		this.controlRuleService.listRuleTriggerEnum().subscribe((data) => {
			this.listRuleTrigger = data;
		});

		this.initInfo();
		this.listEnumForDateRules();
		this.listEnumForDocRules();
	}

	selectedTypeFlux(typeFlux: EbTypeFlux) {
		this.listSchemaPslFiltered = this.listSchemaPsl.map((it) => it);
		if (this.listSchemaPsl) {
			this.listSchemaPslFiltered = this.listSchemaPsl.filter(
				(it) =>
					it &&
					(!typeFlux.listEbTtSchemaPsl ||
						!typeFlux.listEbTtSchemaPsl.length ||
						(typeFlux.listEbTtSchemaPsl &&
							typeFlux.listEbTtSchemaPsl.indexOf(":" + it.ebTtSchemaPslNum + ":") >= 0))
			);
		}
	}

	selectedSchemaPsl(schemaPsl: EbTtSchemaPsl) {
		if (schemaPsl && schemaPsl.listPsl != null) {
			this.selectedSchemaPSL = schemaPsl;
			this.listPsl = schemaPsl.listPsl;
			this.selectedPsl = this.listPsl
				.filter((it) => it.schemaActif)
				.map((it) => it.ebTtCompanyPslNum);
		} else {
			this.listPsl = null;
			this.selectedPsl = null;
		}
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.CONTROL_RULES",
			},
		];
	}

	initInfo() {
		this.searchInput.withDeactive = true;
		this.searchInput.pageNumber = 1;
		this.searchInput.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		//this.searchInput.ruleCategory = CrRuleCategory.DATE;
		this.dataInfos.cols = [
			{
				field: "code",
				translateCode: "CONTROL_RULES.CODE",
			},
			{
				field: "libelle",
				translateCode: "GENERAL.LIBELLE",
			},
			{
				field: "commentaire",
				translateCode: "QUALITY_MANAGEMENT.COMMENTAIRES",
			},
		];

		this.dataInfos.dataKey = "ebControlRuleNum";
		this.dataInfos.dataType = EbControlRule;
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = false;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerControlRule + "/all";
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = false;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showHelpBtn = true;
	}

	listEnumForDateRules() {
		this.controlRuleService.listOperatorEnum().subscribe((data: GenericEnum[]) => {
			this.listOperator = data.filter((it) => it.code < 100);
		});

		this.controlRuleService.listPriorityEnum().subscribe((data) => {
			this.listPriority = data;
		});

		this.controlRuleService.listPslTypeEnum().subscribe((data) => {
			this.listPslTypeRight = data.filter((it) => it.code != NatureDateEvent.NEGOTIATED);
			this.listPslTypeLeft = data.filter((it) => it.code != NatureDateEvent.NEGOTIATED);
		});

		this.controlRuleService.listRuleOperatorEnum().subscribe((data) => {
			this.listRuleOperator = data;
		});

		(async () => (
			(this.listCategorieDeviation = await SingletonStatique.getListCategorieDeviation()),
			(this.listCategorieDeviationFiltred = this.listCategorieDeviation.filter(
				(it) => it.typeEvent != TypeOfEvent.EVENT
			))
		))();
	}

	async listEnumForDocRules() {
		let operators: Array<GenericEnum> = null;

		await this.getInputsData(); // getting all data inputs

		this.constructUnitConditionAndActionLists();

		this.prepareListMails();

		let criteria = new SearchCriteria();
		criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;
		criteria.includeGlobalValues = true;
		criteria.withDeactive = true;

		this.listTypeDoc = await this.typeDocument.getListTypeDocPromise(criteria);

		operators = await this.getlistOperatorEnumForDocRules();

		this.constructOperatorsList(operators);

		this.getListCrFields();

		this.controlRuleService.listRuleDocumentValueEnum().subscribe((data) => {
			this.listDocumentStatus = data;
		});

		this.constructUnitsGenericListValues();
	}

	constructUnitConditionAndActionLists() {
		this.listUnitFieldsForConditions = this.listUnitFields.filter(
			(field) =>
				field.operationType == CrOperationTypes.BOTH_OPERATIONS ||
				field.operationType == CrOperationTypes.IS_FILE_CONDITION
		);
		this.listUnitFieldsForActions = this.listUnitFields.filter(
			(field) =>
				field.operationType == CrOperationTypes.BOTH_OPERATIONS ||
				field.operationType == CrOperationTypes.IS_TR_GENERATION
		);
	}

	getInputsData(): Promise<any> {
		return new Promise((resolve) => {
			this.controlRuleService.getInputData().subscribe((res) => {
				this.listCategories = res.categories;
				this.listTypeflux = res.typeFlux;
				this.listIncoterms = res.incoterms;
				this.listModeTr = res.modesTr;
				this.listModeTr.sort(function(obj1, obj2) {
					return obj1.order - obj2.order;
				});
				this.listStatus = res.dmdStatut;
				this.listTypeRequests = res.requestTypes;
				this.listFields = JSON.parse(res.customField.fields) || new Array<Object>();
				this.docGenTemplates = res.docGenTemplates;
				this.listRefAddresses = res.refAdresses;
				this.listUnitFields = res.unitsFields;
				this.listExportControlStatus = res.listExportControl;
				this.listCountries = res.countries;
				this.listTypeUnit = res.listTypeUnit;
				this.listTypeGoods = res.listTypeGoods;
				this.marchandiseDangerousGood = res.marchandiseDangerousGood;
				this.marchandiseClassGood = res.marchandiseClassGood;
				this.listCostCenter = res.listCostCenter;
				this.user = res.user;
				this.listMails = res.listMails;

				resolve(res);
			});
		});
	}

	prepareListMails() {
		this.translator(this.listMails);
		this.updateListMailParam();
	}

	translator(tab: any[]) {
		tab.forEach((it) => {
			this.translate
				.get("MY_ALERT.ALERT-" + it.emailId)
				.subscribe((res) => (it["emailName"] = res));
		});
	}

	updateListMailParam() {
		if (this.user.listParamsMail == null || this.user.listParamsMail == []) {
			this.user.listParamsMail = this.listMails;
		}
		let i, j, found;
		if (this.listMails.length != this.user.listParamsMail.length) {
			for (i = 0; i < this.listMails.length; i++) {
				found = false;
				for (j = 0; j < this.user.listParamsMail.length; j++) {
					if (this.listMails[i].emailId == this.user.listParamsMail[j].emailId) {
						found = true;
						break;
					}
				}
				if (!found) this.user.listParamsMail.push(this.listMails[i]);
			}
		}

		if (this.user.listParamsMail != null && this.user.listParamsMail.length > 0) {
			for (i = 0; i < this.user.listParamsMail.length; i++) {
				for (j = 0; j < this.user.listParamsMail.length; j++) {
					if (this.listMails[i].emailId == this.user.listParamsMail[j].emailId) {
						this.user.listParamsMail[j].roles = this.listMails[i].roles;
						this.user.listParamsMail[j].services = this.listMails[i].services;
						this.user.listParamsMail[j].emailName = this.listMails[i].emailName;
						this.user.listParamsMail[j].hidden = this.listMails[i].hidden;
						break;
					}
				}
			}
		}

		this.listMails = ((): Array<ParamsMail> =>
			JSON.parse(JSON.stringify(this.user.listParamsMail)))();
	}

	getlistOperatorEnumForDocRules(): Promise<any> {
		return new Promise((resolve) => {
			this.controlRuleService.listOperatorEnum().subscribe((data) => resolve(data));
		});
	}

	constructOperatorsList(operators: Array<GenericEnum>) {
		this.listOperatorForDocRulesConditions = operators.filter(
			(it) => it.code > 100 && it.code < 200
		);
		this.listOperatorForDocRulesActions = operators.filter((it) => it.code >= 201 && it.code < 300);
	}

	getListCrFields() {
		this.controlRuleService.listRuleOperandTypeEnum().subscribe((data: any[]) => {
			this.allCrFields = data;
			this.listConditionCrFields = data.filter(
				(field) =>
					field.operationType == CrOperationTypes.BOTH_OPERATIONS ||
					field.operationType == CrOperationTypes.IS_FILE_CONDITION
			);
			this.listActionCrFields = data.filter(
				(field) =>
					field.operationType == CrOperationTypes.BOTH_OPERATIONS ||
					field.operationType == CrOperationTypes.IS_TR_GENERATION
			);
			this.initListDocRulesFields();
		});
	}

	initListDocRulesFields() {
		let fieldAction: any;
		let fieldCond: any;
		this.listActionGenericFields = new Array<any>();
		this.listConditionGenericFields = new Array<any>();

		this.allCrFields.forEach((t) => {
			fieldAction = {};
			fieldAction.code = t.code;
			fieldAction.type = t.type;
			fieldAction.key = t.key;

			fieldAction = this.setRightListValues(fieldAction, CrOperationTypes.IS_TR_GENERATION);

			fieldCond = this.cloneField(fieldAction);
			fieldCond = this.setRightListValues(fieldCond, CrOperationTypes.IS_FILE_CONDITION);

			if (
				+t.type == CrFieldTypes.CAT ||
				+t.type == CrFieldTypes.TYPE_DE_FLUX ||
				+t.type == CrFieldTypes.MODE_TR ||
				+t.type == CrFieldTypes.CUSTOMFIELD ||
				+t.type == CrFieldTypes.STATUS ||
				+t.type == CrFieldTypes.REF_ADR ||
				+t.type == CrFieldTypes.PSL_SCHEMA ||
				+t.type == CrFieldTypes.COST_CENTER ||
				+t.code == CrRuleOperandType.LEG ||
				+t.code == CrRuleOperandType.NBR_PLTR
			) {
				if (t.type != CrFieldTypes.CUSTOMFIELD) {
					fieldAction.operators = this.listOperatorForDocRulesActions.filter(
						(op) => op.code != CrTrGenerationOperator.INTERCHANGE
					);
					fieldCond.operators = this.listOperatorForDocRulesConditions.filter(
						(op) =>
							op.code != CrConditionOperator.BEGINS_WITH &&
							op.code != CrConditionOperator.AVAILABLE &&
							op.code != CrConditionOperator.NOT_AVAILABLE
					);
				} else {
					fieldAction.operators = this.listOperatorForDocRulesActions;
					fieldCond.operators = this.listOperatorForDocRulesConditions.filter(
						(op) =>
							op.code != CrConditionOperator.AVAILABLE &&
							op.code != CrConditionOperator.NOT_AVAILABLE
					);
				}
			} else {
				if (
					+t.code == CrRuleOperandType.PAYS_ORIGIN ||
					+t.code == CrRuleOperandType.DESTINATION_COUNTRY
				) {
					fieldAction.operators = this.listOperatorForDocRulesActions.filter(
						(op) => op.code != CrTrGenerationOperator.BECOMES
					);
					fieldCond.operators = this.listOperatorForDocRulesConditions.filter(
						(op) =>
							op.code != CrConditionOperator.AVAILABLE &&
							op.code != CrConditionOperator.NOT_AVAILABLE
					);
					fieldCond.rightOperandValues = null;
				} else if (t.type == CrFieldTypes.TYPE_OF_REQ) {
					fieldAction.operators = this.listOperatorForDocRulesActions;
					fieldCond.operators = this.listOperatorForDocRulesConditions.filter(
						(op) =>
							op.code != CrConditionOperator.BEGINS_WITH &&
							op.code != CrConditionOperator.AVAILABLE &&
							op.code != CrConditionOperator.NOT_AVAILABLE
					);
				} else if (t.type == CrFieldTypes.MANDATORY_FIELDS) {
					fieldAction.operators = this.listOperatorForDocRulesActions.filter(
						(op) =>
							op.code != CrTrGenerationOperator.INTERCHANGE &&
							op.code != CrTrGenerationOperator.BECOMES
					);
					fieldCond.operators = this.listOperatorForDocRulesConditions.filter(
						(op) =>
							op.code != CrConditionOperator.BEGINS_WITH &&
							op.code != CrConditionOperator.IS_IN &&
							op.code != CrConditionOperator.IS_NOT_IN
					);
				} else if (t.type == CrFieldTypes.TYPE_OF_DOC) {
					fieldCond.operators = this.listOperatorForDocRulesConditions.filter(
						(op) =>
							op.code != CrConditionOperator.BEGINS_WITH &&
							op.code != CrConditionOperator.IS_IN &&
							op.code != CrConditionOperator.IS_NOT_IN
					);
					fieldCond.rightOperandValues = null;
				} else {
					fieldAction.operators = this.listOperatorForDocRulesActions;
					fieldCond.operators = this.listOperatorForDocRulesConditions.filter(
						(op) =>
							op.code != CrConditionOperator.AVAILABLE &&
							op.code != CrConditionOperator.NOT_AVAILABLE
					);
					fieldCond.rightOperandValues = null;
				}
			}

			if (t.operationType == CrOperationTypes.BOTH_OPERATIONS) {
				this.listActionGenericFields.push(fieldAction);
				this.listConditionGenericFields.push(fieldCond);
			} else if (t.operationType == CrOperationTypes.IS_FILE_CONDITION) {
				this.listConditionGenericFields.push(fieldCond);
			} else if (t.operationType == CrOperationTypes.IS_TR_GENERATION) {
				this.listActionGenericFields.push(fieldAction);
			}
		});
	}

	cloneField(field: any): any {
		let newField: any = {};
		newField.code = field.code;
		newField.type = field.type;
		newField.key = field.key;
		newField.listCategories = field.listCategories;
		newField.listFields = field.listFields;
		return newField;
	}

	setRightListValues(field: any, operationType: number) {
		let crListField: Array<any> = null;
		if (operationType == CrOperationTypes.IS_FILE_CONDITION)
			crListField = this.listConditionCrFields;
		else if (operationType == CrOperationTypes.IS_TR_GENERATION)
			crListField = this.listActionCrFields;
		else crListField = this.allCrFields;

		let genericField: any;
		let convertedValues = new Array<any>();

		if (+field.type == CrFieldTypes.CAT) {
			if (this.listCategories && this.listCategories.length > 0) {
				field.listCategories = this.listCategories;
			}
		} else if (+field.type == CrFieldTypes.TYPE_DE_FLUX) {
			if (this.listTypeflux && this.listTypeflux.length > 0) {
				this.listTypeflux.forEach((ft) => {
					genericField = {};
					genericField.code = ft.ebTypeFluxNum;
					genericField.key = ft.designation;
					convertedValues.push(genericField);
				});
			}
		} else if (+field.type == CrFieldTypes.MODE_TR) {
			if (this.listModeTr && this.listModeTr.length > 0) {
				this.listModeTr.forEach((m) => {
					genericField = {};
					genericField.code = m.code;
					genericField.key = m.libelle;
					convertedValues.push(genericField);
				});
			}
		} else if (+field.type == CrFieldTypes.STATUS) {
			if (this.listStatus && this.listStatus.length > 0) {
				this.listStatus.forEach((status) => {
					genericField = {};
					genericField.code = status.code;
					genericField.key = status.libelle;
					convertedValues.push(genericField);
				});
			}
		} else if (+field.type == CrFieldTypes.TYPE_OF_REQ) {
			if (this.listTypeRequests && this.listTypeRequests.length > 0) {
				this.listTypeRequests.forEach((r) => {
					genericField = {};
					genericField.code = r.ebTypeRequestNum;
					genericField.key = r.libelle;
					convertedValues.push(genericField);
				});
			}
		} else if (+field.type == CrFieldTypes.REF_ADR) {
			if (this.listRefAddresses && this.listRefAddresses.length > 0) {
				this.listRefAddresses.forEach((cc) => {
					genericField = {};
					genericField.code = cc;
					genericField.key = cc;
					convertedValues.push(genericField);
				});
			}
		} else if (+field.type == CrFieldTypes.PSL_SCHEMA) {
			if (this.listSchemaPsl && this.listSchemaPsl.length > 0) {
				this.listSchemaPsl.forEach((psl) => {
					genericField = {};
					genericField.code = psl.ebTtSchemaPslNum;
					genericField.key = psl.designation;
					convertedValues.push(genericField);
				});
			}
		} else if (+field.type == CrFieldTypes.UNITS) {
			if (this.listUnitFields && this.listUnitFields) {
				this.listUnitFields.forEach((field) => {
					genericField = {};
					genericField.code = field.code;
					genericField.key = field.key;
					genericField.type = field.type; // unit field type
					convertedValues.push(genericField);
				});
			}
		} else if (+field.type == CrFieldTypes.COST_CENTER) {
			if (this.listCostCenter && this.listCostCenter.length > 0) {
				this.listCostCenter.forEach((field) => {
					genericField = {};
					genericField.code = field.ebCostCenterNum;
					genericField.key = field.libelle;
					convertedValues.push(genericField);
				});
			}
		} else if (+field.type == CrFieldTypes.CUSTOMFIELD) {
			field.listFields = this.listFields;
			convertedValues = crListField.filter(
				(v) => v.code == CrRuleOperandType.CUSTOMFIELD || v.type == CrFieldTypes.STRING
			);
		} else {
			convertedValues = crListField.filter((v) => v.type == field.type);
		}
		field.rightOperandValues = convertedValues;
		return field;
	}

	constructUnitsGenericListValues() {
		this.unitGenericFieldsForConditions = new Array<any>();
		this.unitGenericFieldsForActions = new Array<any>();

		let actionField: any;
		let condField: any;

		this.listUnitFields.forEach((unitField) => {
			let isListType: boolean = false;

			actionField = {};
			actionField.code = unitField.code;
			actionField.type = unitField.type;

			if (unitField.type == CrFieldTypes.COUNTRY) {
				actionField.values = this.listCountries.map((cc) => {
					let v = {
						code: cc.ecCountryNum,
						key: cc.libelle,
					};
					return v;
				});
				isListType = true;
			} else if (unitField.type == CrFieldTypes.UNIT_TYPE) {
				if (this.listTypeUnit && this.listTypeUnit.length > 0) {
					actionField.values = this.listTypeUnit.map((type) => {
						let v = {
							code: type.xEbTypeUnit,
							key: type.label,
						};
						return v;
					});
				}
				isListType = true;
			} else if (unitField.type == CrFieldTypes.GOOD_TYPE) {
				if (this.listTypeGoods && this.listTypeGoods.length > 0) {
					actionField.values = this.listTypeGoods.map((typeGood) => {
						let v = {
							code: typeGood.ebTypeGoodsNum,
							key: typeGood.label,
						};
						return v;
					});
				}
				isListType = true;
			} else if (unitField.type == CrFieldTypes.OVERSIZED_ITEM) {
				actionField.values = Statique.yesOrNoWithBooleanKey.map((item) => {
					let v = {
						code: item.key + "",
						key: item.value,
					};
					return v;
				});
				isListType = true;
			} else if (unitField.type == CrFieldTypes.LARGE_ITEM) {
				actionField.values = Statique.yesOrNoWithBooleanKey.map((item) => {
					let v = {
						code: item.key + "",
						key: item.value,
					};
					return v;
				});
				isListType = true;
			} else if (unitField.type == CrFieldTypes.DGR) {
				if (this.marchandiseDangerousGood && this.marchandiseDangerousGood.length > 0) {
					actionField.values = this.marchandiseDangerousGood.map((item) => {
						let v = {
							code: item.code,
							key: item.libelle,
						};
						return v;
					});
				}
				isListType = true;
			} else if (unitField.type == CrFieldTypes.CLASS) {
				if (this.marchandiseClassGood && this.marchandiseClassGood.length > 0) {
					actionField.values = this.marchandiseClassGood.map((cl) => {
						let v = {
							key: String(cl),
							code: String(cl),
						};
						return v;
					});
				}
				isListType = true;
			} else {
				actionField.values = null;
			}

			condField = {};
			condField.code = actionField.code;
			condField.type = actionField.type;
			condField.values = actionField.values;

			if (isListType) {
				actionField.operators = this.listOperatorForDocRulesActions.filter(
					(op) => op.code != CrTrGenerationOperator.INTERCHANGE
				);
				condField.operators = this.listOperatorForDocRulesConditions.filter(
					(op) =>
						op.code != CrConditionOperator.BEGINS_WITH &&
						op.code != CrConditionOperator.AVAILABLE &&
						op.code != CrConditionOperator.NOT_AVAILABLE
				);
			} else {
				if (unitField.type == CrFieldTypes.ALL) {
					actionField.operators = this.listOperatorForDocRulesActions.filter(
						(op) =>
							op.code != CrTrGenerationOperator.INTERCHANGE &&
							op.code != CrTrGenerationOperator.BECOMES
					);
				} else {
					actionField.operators = this.listOperatorForDocRulesActions;
					condField.operators = this.listOperatorForDocRulesConditions.filter(
						(op) =>
							op.code != CrConditionOperator.AVAILABLE &&
							op.code != CrConditionOperator.NOT_AVAILABLE
					);
				}
			}

			if (unitField.operationType == CrOperationTypes.BOTH_OPERATIONS) {
				this.unitGenericFieldsForActions.push(actionField);
				this.unitGenericFieldsForConditions.push(condField);
			} else if (unitField.operationType == CrOperationTypes.IS_FILE_CONDITION) {
				this.unitGenericFieldsForConditions.push(condField);
			} else if (unitField.operationType == CrOperationTypes.IS_TR_GENERATION) {
				this.unitGenericFieldsForActions.push(actionField);
			}
		});
	}

	addElement() {
		let ruleOperation = new EbControlRuleOperation();
		if (this.controlRule.listRuleOperations.length == 0) ruleOperation.ruleOperator = null;
		this.controlRule.listRuleOperations.push(ruleOperation);
	}

	deleteRegle(i: number) {
		this.controlRule.listRuleOperations.splice(i, 1);
	}

	getPriority(item: any): string {
		return this.listPriority.find((v) => v.code == item)
			? this.listPriority.find((v) => v.code == item).key
			: "";
	}

	openAddForm(isUpdate?: boolean) {
		if (isUpdate) {
			this.editMode = true;
		} else {
			this.controlRule = new EbControlRule();
			this.controlRule.activated = true;
			this.editMode = false;
		}
		this.isAddEditMode = true;
	}

	openEditForm(ebControlRule: EbControlRule) {
		this.controlRule = ebControlRule;
		this.controlRule.listRuleOperations = new Array<EbControlRuleOperation>();

		this.controlRuleService
			.getOne(ebControlRule.ebControlRuleNum)
			.subscribe((data: EbControlRule) => {
				this.ebControlRuleNum = ebControlRule.ebControlRuleNum;
				this.controlRule = data;
				this.selectedConditionType = this.controlRule.ruleCategory;

				this.listTrigVal = {};
				if (this.controlRule.triggers && this.controlRule.triggers.length) {
					let arr = this.controlRule.triggers.split(";");
					arr.forEach((it) => {
						this.listTrigVal[it] = true;
					});
				}

				this.backupEditcontrolRule = Statique.cloneObject(this.controlRule, new EbControlRule());
				this.openAddForm(true);
			});
	}

	validateOperations() {
		if (
			!this.controlRule.listDateOperations ||
			!this.controlRule.listDateOperations.length ||
			this.controlRule.listDateOperations.length == 0
		)
			return false;

		return !this.controlRule.listDateOperations.find((element, i) => {
			if (!element.leftOperandObj || !element.leftOperandObj.typeDate) {
				return true;
			}
			if (
				element.operator != CrOperator.ABSENT &&
				(!element.rightOperandObj || !element.rightOperandObj.typeDate)
			) {
				return true;
			}
			if (
				element.operator != CrOperator.ABSENT &&
				element.rightOperandObj &&
				element.rightOperandObj.typeDate
			) {
				if (
					!element.rightOperandObj.pslCodeAlpha &&
					element.rightOperandObj.typeDate != NatureDateEvent.NOW
				)
					return true;
			}
			if (element.leftOperandObj && element.leftOperandObj.typeDate) {
				if (
					!element.leftOperandObj.pslCodeAlpha &&
					element.leftOperandObj.typeDate != NatureDateEvent.NOW
				)
					return true;
			}
			if (!element.operator) {
				return true;
			}
		});
	}

	prepareDocControlRuleToSave(): EbControlRule {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		let cr = Statique.cloneObject(this.controlRule, new EbControlRule());

		if (this.selectedListTypeDoc && this.selectedListTypeDoc.length) {
			cr.listTypeDocStatus = this.selectedListTypeDoc
				.map((it) => it.ebTypeDocumentsNum + ":" + (it.status || 0))
				.reduce((acc, el) => acc + "|" + el);
		}

		cr.listRuleOperations.some((element, i) => {
			if (i == 0) element.ruleOperator = null;
			else if (i > 0 && !element.ruleOperator) element.ruleOperator = 1;

			if (element.leftOperands) element.leftOperands = String(element.leftOperands);
			if (element.rightOperands) element.rightOperands = String(element.rightOperands);
		});

		let listRuleOperations = cr.listRuleOperations;
		let listGeneratedTrOperations = cr.listGeneratedTrOperations;
		cr.listGeneratedTrOperations = null;

		if (listRuleOperations && listRuleOperations.length > 0) {
			if (listGeneratedTrOperations && listGeneratedTrOperations.length > 0)
				cr.listRuleOperations = listRuleOperations.concat(listGeneratedTrOperations);
			else cr.listRuleOperations = listRuleOperations;
		} else if (listGeneratedTrOperations && listGeneratedTrOperations.length > 0) {
			cr.listRuleOperations = listGeneratedTrOperations;
		}

		if (cr.sendEmailAlert) {
			cr.selectedMailNums = this.selectedMails
				? ":" + this.selectedMails.map((mail) => mail.emailId).join(":") + ":"
				: null;
			cr.emailDestinataires = this.emailDestinataires ? this.emailDestinataires.join(";") : null;
		}

		if (cr.envoyerEmail)
			cr.emailDestinataires = this.emailDestinataires ? this.emailDestinataires.join(";") : null;

		if (cr.genererNotif) {
			cr.emailDestinataires = this.emailDestinataires ? this.emailDestinataires.join(";") : null;
		}
		cr.ruleCategory = CrRuleCategory.DOCUMENT;

		cr = this.cleanDocRuleElements(cr);

		return cr;
	}

	prepareDateControlRuleToSave() {
		let cr = Statique.cloneObject(this.controlRule, new EbControlRule());
		cr.emailDestinataires = this.emailDestinataires ? this.emailDestinataires.join(";") : null;

		// list condition
		cr.listRuleOperations.some((element, i) => {
			if (i == 0) element.ruleOperator = null;
			else if (i > 0 && !element.ruleOperator) element.ruleOperator = 1;

			if (element.leftOperands) element.leftOperands = String(element.leftOperands);
			if (element.rightOperands) element.rightOperands = String(element.rightOperands);
		});
		let listRuleOperations = cr.listRuleOperations;

		// list date rules
		let listDateOperations = cr.listDateOperations;
		cr.listDateOperations = null;
		listDateOperations.some((element, i) => {
			if (i == 0) element.ruleOperator = null;

			let left: string = null;
			if (element.leftOperandObj && element.leftOperandObj.typeDate) {
				left = element.leftOperandObj.typeDate + "";
				if (element.leftOperandObj.typeDate != NatureDateEvent.NOW)
					left += ";" + element.leftOperandObj.pslCodeAlpha;
			}
			element.leftOperands = left;

			let right: string = null;
			if (
				element.operator != CrOperator.ABSENT &&
				element.rightOperandObj &&
				element.rightOperandObj.typeDate
			) {
				right = element.rightOperandObj.typeDate + "";
				if (element.rightOperandObj.typeDate != NatureDateEvent.NOW)
					right += ";" + element.rightOperandObj.pslCodeAlpha;
			}
			element.rightOperands = right;
		});

		if (listRuleOperations && listRuleOperations.length > 0) {
			if (listDateOperations && listDateOperations.length > 0)
				cr.listRuleOperations = listRuleOperations.concat(listDateOperations);
			else cr.listRuleOperations = listRuleOperations;
		} else if (listDateOperations && listDateOperations.length > 0) {
			cr.listRuleOperations = listDateOperations;
		}

		cr.ruleType = CrRuleCategory.DATE;
		cr.ruleCategory = CrRuleCategory.DATE;

		return cr;
	}

	saveControlRule(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		let cr: EbControlRule = null;

		if (this.selectedConditionType == CrRuleCategory.DATE) cr = this.prepareDateControlRuleToSave();
		else if (this.selectedConditionType == CrRuleCategory.DOCUMENT)
			cr = this.prepareDocControlRuleToSave();

		cr.xEbCompagnie = new EbCompagnie();
		cr.xEbCompagnie.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;

		cr.triggers = null;
		if (this.listTrigVal) {
			let o;
			for (o in this.listTrigVal) {
				if (this.listTrigVal[o]) {
					if (cr.triggers) cr.triggers += ";";
					else cr.triggers = "";

					cr.triggers += o;
				}
			}
		}
		if (!this.editMode) {
			this.controlRuleService.add(cr).subscribe(
				(data: EbControlRule) => {
					requestProcessing.afterGetResponse(event);
					this.isAddEditMode = false;
					this.backupEditcontrolRule = null;
					this.successfulOperation();
					this.genericTable.refreshData();
					this.onDataChanged.next();
				},
				(error) => {
					console.error(error);
					this.echecOperation();
					requestProcessing.afterGetResponse(event);
				}
			);
		} else if (this.ebControlRuleNum != null) {
			cr.ebControlRuleNum = this.ebControlRuleNum;
			cr.ruleType = this.controlRule.ruleType;
			this.controlRuleService.update(cr).subscribe(
				(data: EbControlRule) => {
					requestProcessing.afterGetResponse(event);
					this.isAddEditMode = false;
					this.backupEditcontrolRule = null;
					this.successfulOperation();
					this.genericTable.refreshData();
					this.onDataChanged.next();
				},
				(error) => {
					console.error(error);
					this.echecOperation();
					requestProcessing.afterGetResponse(event);
				}
			);
		}
	}

	successfulOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	echecOperation() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}

	backToListControlRules() {
		Statique.cloneObject<EbControlRule>(this.backupEditcontrolRule, this.controlRule);
		this.backupEditcontrolRule = null;
		this.isAddEditMode = false;
		this.selectedConditionType = null;
	}

	async deleteControlRule(event) {
		let controlRule: EbControlRule = event as EbControlRule;
		let $this = this;

		let confirm = await this.modalService.confirmPromise(
			$this.translate.instant("GENERAL.CONFIRMATION"),
			$this.translate.instant("CONTROL_RULES.DEACTIVATED_ITEM")
		);

		confirm &&
			$this.controlRuleService.deleteControlRule(controlRule.ebControlRuleNum).subscribe((data) => {
				if (data == true) {
					$this.successfulOperation();
					$this.genericTable.refreshData();
				} else {
					$this.messageService.add({
						severity: "error",
						summary: $this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
						detail: $this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
					});
				}
			});
	}

	addEmail(label) {
		return label;
	}

	isPslWithChamp(pslCodeAlpha: string) {
		if (
			!Statique.isDefined(pslCodeAlpha) ||
			!Statique.isDefined(this.listPsl) ||
			this.listPsl.length == 0
		)
			return false;
		let psl = this.listPsl.find((psl) => psl.codeAlpha == pslCodeAlpha);
		return Statique.isDefined(psl.champ) && psl.champ ? true : false;
	}

	getlistPslTypesLeft(pslCodeAlpha: string) {
		if (
			!Statique.isDefined(pslCodeAlpha) ||
			!Statique.isDefined(this.listPsl) ||
			this.listPsl.length == 0
		)
			return this.listPslTypeLeft.filter((it) => Number(it.code) != NatureDateEvent.CHAMP);
		let psl = this.listPsl.find((psl) => psl.codeAlpha == pslCodeAlpha);
		return Statique.isDefined(psl.champ) && psl.champ
			? this.listPslTypeLeft
			: this.listPslTypeLeft.filter((it) => Number(it.code) != NatureDateEvent.CHAMP);
	}

	getlistPslTypesRight(pslCodeAlpha: string) {
		if (
			!Statique.isDefined(pslCodeAlpha) ||
			!Statique.isDefined(this.listPsl) ||
			this.listPsl.length == 0
		)
			return this.listPslTypeRight.filter((it) => Number(it.code) != NatureDateEvent.CHAMP);
		let psl = this.listPsl.find((psl) => psl.codeAlpha == pslCodeAlpha);
		return Statique.isDefined(psl.champ) && psl.champ
			? this.listPslTypeRight
			: this.listPslTypeRight.filter((it) => Number(it.code) != NatureDateEvent.CHAMP);
	}
	showHelpTemplateHandler() {
		this.modalLexiqueVisible = true;
	}

	get isValidForm() {
		if (this.selectedConditionType && this.selectedConditionType == CrRuleCategory.DATE)
			return this.validateOperations();
		return true;
	}

	initCRValues() {
		// reset control rule data
		let code = this.controlRule.code;
		let label = this.controlRule.libelle;

		this.controlRule = new EbControlRule();
		this.controlRule.code = code;
		this.controlRule.libelle = label;
	}

	get isValideDocRule(): boolean {
		if (this.controlRule.listRuleOperations || this.controlRule.listRuleOperations.length == 0)
			return false;
	}

	cleanDocRuleElements(controlRule: EbControlRule): EbControlRule {
		controlRule.listRuleOperations.forEach((elem) => {
			delete elem.listCategories;
			delete elem.listCustomFields;
			delete elem.listCategoriesOrCF;
			delete elem.listUnits;
			delete elem.leftOperandValues;
			delete elem.listOfOperators;
			delete elem.rightOperandValues;
			delete elem.listDocTypes;
		});
		return controlRule;
	}

}
