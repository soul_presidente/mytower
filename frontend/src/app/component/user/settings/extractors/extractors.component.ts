import { Component, OnInit } from "@angular/core";
import { ExportService } from "../../../../services/export.service";
import { Extractors } from "../../../../utils/enumeration";
import { RequestProcessing } from "../../../../utils/requestProcessing";
import { StatiqueService } from "../../../../services/statique.service";

@Component({
	selector: "app-extractors",
	templateUrl: "./extractors.component.html",
	styleUrls: ["./extractors.component.css"],
})
export class ExtractorsComponent implements OnInit {
	Extractors = Extractors;
	listExtractors: Array<any>;

	constructor(private exportService: ExportService, private statiqueService: StatiqueService) {}

	ngOnInit() {
		this.statiqueService.getListExtractors().subscribe((res: Array<any>) => {
			this.listExtractors = res;
		});
	}

	extract($event, type: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest($event);

		this.exportService.extractDataAnalysis(type.code).subscribe((blob) => {
			requestProcessing.afterGetResponse($event);

			this.downloadFile(blob, type.name);
		});
	}
	downloadFile(data, module) {
		let link = document.createElement("a");
		link.href = window.URL.createObjectURL(data);
		//module-nom-d-utilisateur-connecté-JJ-MM-AAA-HH-mm
		let dt = new Date();
		link.download =
			"EXTRACT_" +
			module +
			"-" +
			dt.getDate() +
			"-" +
			(dt.getMonth() + 1) +
			"-" +
			dt.getFullYear() +
			"-" +
			dt.getHours() +
			"-" +
			dt.getMinutes() +
			".xlsx";
		link.click();
	}
}
