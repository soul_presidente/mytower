import { Component, OnInit, ViewChild } from "@angular/core";
import { AuthenticationService } from "@app/services/authentication.service";
import { ActivatedRoute, Params, Router, NavigationEnd } from "@angular/router";
import { NgbTabset } from "@ng-bootstrap/ng-bootstrap";
import { UploadComponent } from "@app/utils/uploadComponent";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";

@Component({
	selector: "app-settings",
	templateUrl: "./settings.component.html",
	styleUrls: ["./settings.component.scss"],
})
export class SettingsComponent extends UploadComponent implements OnInit {
	public static TABS = ["account", "community", "company"];
	selectedTab: string = SettingsComponent.TABS[0];

	params: Params;
	buttonClick: string;

	@ViewChild("tabset", { static: false })
	ngbTabSet: NgbTabset;
	breadcrumbsItemsConfig: Array<HeaderInfos.BreadCrumbItem> = [];

	constructor(
		protected authenticationService?: AuthenticationService,
		public activatedRoute?: ActivatedRoute,
		protected headerService?: HeaderService,
		protected router?: Router
	) {
		super();
	}

	ngOnInit() {
		this.buttonClick = "account settings";
		if (this.activatedRoute) {
			this.activatedRoute.queryParams.subscribe((params) => {
				this.params = params;
				if (this.params["tab"] && SettingsComponent.TABS.filter((t) => t === this.params["tab"])) {
					this.selectedTab = this.params["tab"];
				}
			});
		}
		this.headerService.events.breadCrumbsChange.subscribe(
			(breadcrumbsItemsConfig: Map<number, Array<HeaderInfos.BreadCrumbItem>>) => {
				this.breadcrumbsItemsConfig = breadcrumbsItemsConfig.get(BreadcumbComponent.MY_COMPANY);
			}
		);
	}

	clickFortab(event: string) {
		this.buttonClick = event;
	}
}
