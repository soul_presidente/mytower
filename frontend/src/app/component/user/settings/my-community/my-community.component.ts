import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EbRelation } from "@app/classes/relation";
import { UserService } from "@app/services/user.service";
import { EbEtablissement } from "@app/classes/etablissement";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbUser } from "@app/classes/user";
import { Component, Input, HostListener } from "@angular/core";
import { SettingsComponent } from "../settings.component";
import { EtatRelation, ServiceType, StatusDemande, UserRole } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { EtablissementService } from "@app/services/etablissement.service";
import { AuthenticationService } from "@app/services/authentication.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { TranslateService } from "@ngx-translate/core";
import { CompagnieService } from "@app/services/compagnie.service";
import { DialogService } from "@app/services/dialog.service";
import { InscriptionService } from "@app/services/inscription.service";
import { EbCompagnie } from "@app/classes/compagnie";
import { MessageService } from "primeng/api";
import { Subject } from "rxjs";
import { debounceTime, distinctUntilChanged } from "rxjs/operators";
import { EbUserTag } from "@app/classes/EbUserTag";

@Component({
	selector: "app-settings-my-community",
	templateUrl: "./my-community.component.html",
	styleUrls: ["./my-community.component.scss"],
})
export class _MyCommunityComponent extends SettingsComponent {
	@Input()
	user: EbUser;
	@Input()
	userId: number = null;
	isForConnectedUser: boolean = true;

	Statique = Statique;
	UserRole = UserRole;
	EtatRelation = EtatRelation;
	searchCriteria: SearchCriteria = new SearchCriteria();
	searchCriteriaUser: SearchCriteria = new SearchCriteria();
	ServiceType = ServiceType;
	showContact = true;
	listEbRelationWithEtablissement: Array<EbRelation>;
	listEbUser: Array<EbUser> = new Array<EbUser>();
	ListEbUserTag: Array<EbUserTag> = [];
	listContact: Array<EbUser>;
	listEbRelation: Array<EbRelation> = [];
	typeInvitation: number = null;
	isNewContactBlock: boolean;
	emailToSend: string;
	invitationMessage: string;
	isInvitationBlockEnabled: boolean;
	totalRecords: number = 0;
	listPages: Array<number> = new Array<number>();
	SendInvitationDialog: boolean = false;
	emailPattern: string;
	selectChargeurs: Array<EbUser> = new Array<EbUser>();
	userAll: EbUser = new EbUser();
	listEbCompagnie: Array<EbCompagnie> = new Array<EbCompagnie>();
	userConnectedCompagnie: EbCompagnie;
	addCarrierModal: boolean = false;

	ebRelationToAll: EbRelation = new EbRelation();
	listContactsRelation: Array<EbUser> = new Array<EbUser>();
	listSentRequestContact: Array<EbUser> = new Array<EbUser>();
	searchInputSubject: Subject<string> = new Subject();
	rowsPerPageOptions: number[] = [5, 10, 25, 50, 100, 500];
	numberDatasPerPage: number = 10;
	fromPage: number;
	toPage: number;

	listOfAllTagsFiltered: Array<String> = [];

	ConstantsTranslate: Object = {
		TRANSPORTEUR: "",
		CHARGEUR: "",
		BROKER: "",
		CONTROL_TOWER: "",
		TRANSPORTEUR_BROKER: "",
	};

	constructor(
		protected compagnieService: CompagnieService,
		protected userService: UserService,
		protected etablissementService: EtablissementService,
		protected ngbModal: NgbModal,
		protected authenticationService: AuthenticationService,
		protected dialogService: DialogService,
		protected inscriptionService: InscriptionService,
		protected translate: TranslateService,
		protected messageService: MessageService
	) {
		super(authenticationService);
		this.listEbRelationWithEtablissement = new Array<EbRelation>();
		this.searchCriteria.size = -1;
		this.searchCriteria.contact = true;
		this.isNewContactBlock = false;
		this.searchInputSubject
			.pipe(
				debounceTime(500),
				distinctUntilChanged()
			)
			.subscribe(() => {
				this.searchContact();
			});
	}

	ngOnInit() {
		this.userAll.ebUserNum = -1;
		if (this.user != null) {
			this.isForConnectedUser = false;
		} else {
			this.isForConnectedUser = true;
			this.user = this.userConnected;
		}
		this.searchCriteria.setUser(this.user);
		if (this.userId && this.userId != null) {
			this.searchCriteria.ebUserNum = this.userId;
		} else {
			this.searchCriteria.ebUserNum = this.user.ebUserNum;
		}
		if (this.user.ebCompagnie != null)
			this.searchCriteria.ebCompagnieNum = this.user.ebCompagnie.ebCompagnieNum;
		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate.get("GENERAL." + key).subscribe((res) => (this.ConstantsTranslate[key] = res));
		});
		this.emailPattern = Statique.patterEmail;
		this.searchCriteria.size = this.numberDatasPerPage;
		this.searchContact();
	}

	editRelation(etat: number, index: number, btnEditRelation: HTMLElement) {
		btnEditRelation.classList.add("disabled");
		let ebRelation: EbRelation = new EbRelation();
		ebRelation.constructorCopy(this.listContact[index].ebRelation);
		ebRelation.etat = etat;

		this.userService.updateUserContacts(ebRelation).subscribe((data: EbRelation) => {
			btnEditRelation.classList.remove("disabled");
			this.listContact[index].ebRelation.etat = data.etat;
		});
	}

	editRelationType(content, index: number) {
		let ebRelation: EbRelation = new EbRelation();
		ebRelation.constructorCopy(this.listContact[index].ebRelation);

		this.typeInvitation = null;
		this.ngbModal.open(content).result.then(
			(result) => {
				ebRelation.typeRelation = this.typeInvitation;
				this.userService.updateUserContacts(ebRelation).subscribe((data: EbRelation) => {
					this.listContact[index].ebRelation.typeRelation = data.typeRelation;
				});
			},
			(reason) => {}
		);
	}

	searchTerm(event) {
		if (this.showContact) this.searchInputSubject.next(event);
		else this.searchEtablissement();
	}

	searchTag(event) {
		this.searchCriteria.searchterm = event;
		this.searchContact();
	}

	addTagToList(event) {
		this.listOfAllTagsFiltered.push(event);
	}
	showAddPopup(index: number) {
		this.addCarrierModal = true;
		this.ebRelationToAll = new EbRelation();
		let ebRelation: EbRelation = new EbRelation();
		ebRelation.constructorCopy(this.listContact[index].ebRelation);
		this.selectChargeurs = new Array<EbUser>();
		if (this.user.superAdmin) {
			this.searchCriteriaUser.ebCompagnieNum = this.user.ebCompagnie.ebCompagnieNum;
		}

		if (this.user.uniqueAdminOfEtablissement) {
			this.searchCriteriaUser.ebEtablissementNum = this.user.ebEtablissement.ebEtablissementNum;
		}
		this.searchCriteriaUser.ebUserNum = this.user.ebUserNum;
		this.userService.getListUser(this.searchCriteriaUser).subscribe((data: Array<EbUser>) => {
			this.listEbUser = data;
		});
		this.ebRelationToAll = ebRelation;
		this.ebRelationToAll.ebRelationNum = null;
	}

	searchContact() {
		if (!Statique.isDefined(this.searchCriteria.ebUserNum)) return;
		this.searchCriteria.excludeCt = false;
		this.listContact = null;
		let isSentRequests = this.searchCriteria.sentRequests;
		this.searchCriteria.sentRequests = false;
		this.searchCriteria.pageNumber = 0;
		this.searchCriteria.withActiveUsers = true;
		this.userService.getUserContacts(this.searchCriteria).subscribe((data) => {
			this.listContactsRelation = data.records;
			this.totalRecords = data.total;
			this.listPages = new Array<number>();
			let i,
				n =
					this.totalRecords / this.searchCriteria.size +
					(this.totalRecords % this.searchCriteria.size > 0 ? 1 : 0);
			for (i = 1; i <= n; i++) {
				this.listPages.push(i);
			}
			this.listContact = this.listContactsRelation;
			this.onResizeCommunity();
			if (isSentRequests) {
				this.searchCriteria.sentRequests = true;
				if (
					!this.searchCriteria.receivedRequest &&
					!this.searchCriteria.contactable &&
					!this.searchCriteria.contact &&
					!this.searchCriteria.blackList
				) {
					this.listContact = null;
				}
				this.listSentRequestContact = new Array();
				this.userService.getUserContacts(this.searchCriteria).subscribe((data) => {
					data.records.forEach((record) => {
						let userToAdd: EbUser = new EbUser();
						if (record.ebRelation.etat == 0) {
							userToAdd.email = record.ebRelation.email;
							userToAdd.ebRelation = record.ebRelation;
							this.listSentRequestContact.push(userToAdd);
							this.onResizeCommunity();
						}
					});
					if (this.listContact != null) {
						this.listContact = this.listContact.concat(this.listSentRequestContact);
						this.onResizeCommunity();
					} else {
						this.listContact = this.listSentRequestContact;
						this.onResizeCommunity();
					}
				});
			} else if (
				!this.searchCriteria.sentRequests &&
				!this.searchCriteria.receivedRequest &&
				!this.searchCriteria.contactable &&
				!this.searchCriteria.contact &&
				!this.searchCriteria.blackList
			) {
				this.listSentRequestContact = new Array();
				this.userService.getUserContacts(this.searchCriteria).subscribe((data) => {
					data.records.forEach((record) => {
						let userToAdd: EbUser = new EbUser();
						if (record.ebRelation.etat == 0) {
							userToAdd.email = record.ebRelation.email;
							userToAdd.ebRelation = record.ebRelation;
							this.listSentRequestContact.push(userToAdd);
							this.onResizeCommunity();
						}
					});
					if (this.listContact != null) {
						this.listContact = this.listContact.concat(this.listSentRequestContact);
						this.onResizeCommunity();
					} else {
						this.listContact = this.listSentRequestContact;
						this.onResizeCommunity();
					}

					this.searchCriteria.sentRequests = false;
				});
			}
			if (this.listContact != null && this.listContact.length > 0) {
				this.ListEbUserTag = [];
				this.listContact.forEach((item) => {
					if (item.ebUserTag != null) {
						this.ListEbUserTag.push(item.ebUserTag);
					}
				});
				let listOfAllTags: Array<string> = [];

				this.ListEbUserTag.forEach((element) => {
					if (element.tags != null) {
						let listTags: Array<string> = [];

						listTags = element.tags.split(",");

						listTags.forEach((element) => {
							listOfAllTags.push(element);
						});
					}
				});

				this.listOfAllTagsFiltered = listOfAllTags.filter((elem, i, arr) => {
					if (arr.indexOf(elem) === i) {
						return elem;
					}
				});

				for (let contact of this.listContact) {
					if (contact.ebUserTag.ebUserTagNum == null) {
						let userTag: EbUserTag = new EbUserTag();
						userTag.constructorGuest(contact);
						userTag.constructorHost(this.user);
						contact.ebUserTag = new EbUserTag();
						contact.ebUserTag = userTag;
					}
				}
			}
			this.affecteFromAndToValues();
		});
	}

	searchContactOrEtablissement() {
		if (this.showContact) this.searchContact();
		else this.searchEtablissement();
	}

	searchEtablissement() {
		if (!this.searchCriteria.ebUserNum) return;
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.excludeCt = true;
		this.userService.getEtablissementsContact(this.searchCriteria).subscribe((data) => {
			this.listEbRelationWithEtablissement = data.records;
			this.totalRecords = data.total;
			this.listPages = new Array<number>();
			let i,
				n =
					this.totalRecords / this.searchCriteria.size +
					(this.totalRecords % this.searchCriteria.size > 0 ? 1 : 0);
			for (i = 1; i <= n; i++) {
				this.listPages.push(i);
			}
		});
	}

	changeContactOrEtablissement() {
		if (this.showContact) this.searchContact();
		else this.searchEtablissement();
	}

	loadPage(pageNumber: number = null) {
		if (pageNumber == null)
			pageNumber = this.listPages ? this.listPages[this.listPages.length - 1] : null;

		if (pageNumber == null) return;

		if (pageNumber <= 0) {
			pageNumber = 1;
		}

		if (pageNumber == this.searchCriteria.pageNumber) return;
		if (this.listPages && this.listPages[this.listPages.length - 1] < pageNumber) return;

		this.searchCriteria.pageNumber = pageNumber;

		if (this.showContact) {
			this.userService.getUserContacts(this.searchCriteria).subscribe((data) => {
				this.listContact = data.records;
				this.totalRecords = data.total;
				this.listPages = new Array<number>();
				let i,
					n =
						this.totalRecords / this.searchCriteria.size +
						(this.totalRecords % this.searchCriteria.size > 0 ? 1 : 0);
				for (i = 1; i <= n; i++) {
					this.listPages.push(i);
				}
			});
		} else {
			this.userService.getEtablissementsContact(this.searchCriteria).subscribe((data) => {
				this.listEbRelationWithEtablissement = data.records;
				this.totalRecords = data.total;
				this.listPages = new Array<number>();
				let i,
					n =
						this.totalRecords / this.searchCriteria.size +
						(this.totalRecords % this.searchCriteria.size > 0 ? 1 : 0);
				for (i = 1; i <= n; i++) {
					this.listPages.push(i);
				}
			});
		}
	}

	onChange(size) {
		this.searchCriteria.size = size.rows;

		if (this.showContact) {
			this.userService.getUserContacts(this.searchCriteria).subscribe((data) => {
				this.listContact = data.records;
				this.totalRecords = data.total;
				this.listPages = new Array<number>();
				let i,
					n =
						this.totalRecords / this.searchCriteria.size +
						(this.totalRecords % this.searchCriteria.size > 0 ? 1 : 0);
				for (i = 1; i <= n; i++) {
					this.listPages.push(i);
				}
			});
		} else {
			this.userService.getEtablissementsContact(this.searchCriteria).subscribe((data) => {
				this.listEbRelationWithEtablissement = data.records;
				this.totalRecords = data.total;
				this.listPages = new Array<number>();
				let i,
					n =
						this.totalRecords / this.searchCriteria.size +
						(this.totalRecords % this.searchCriteria.size > 0 ? 1 : 0);
				for (i = 1; i <= n; i++) {
					this.listPages.push(i);
				}
			});
		}
	}

	getMoreData() {
		this.searchCriteria.pageNumber += 1;
		if (this.showContact) {
			this.userService.getUserContacts(this.searchCriteria).subscribe((data) => {
				this.listContact = this.listContact.concat(data.records);
			});
		} else {
			this.userService.getEtablissementsContact(this.searchCriteria).subscribe((data) => {
				this.listEbRelationWithEtablissement = this.listEbRelationWithEtablissement.concat(
					data.records
				);
			});
		}
	}

	getClassCssContactCard(code: number) {
		let classCss = "";
		if (this.listContact.length < 4) {
			classCss = "height-auto";
		} else if (EtatRelation.BLACKLIST == code) {
			classCss += " disabled-div";
			return classCss;
		} else if (EtatRelation.ACTIF == code) {
			classCss += " green";
			return classCss;
		} else if (EtatRelation.EN_COURS == code) {
			classCss += " red";
			return classCss;
		}
		return classCss;
	}

	// -------------------------------------------------------------

	accepterRelation(accptBtn: HTMLElement, annulBtn: HTMLElement, index: number) {
		accptBtn.classList.add("disabled");
		if (annulBtn) annulBtn.classList.add("disabled");
		let ebRelation: EbRelation = new EbRelation();
		ebRelation.constructorCopy(this.listContact[index].ebRelation);
		this.userService.accepterRelation(ebRelation).subscribe((data: boolean) => {
			if (data) {
				this.listContact[index].ebRelation.etat = EtatRelation.ACTIF;
			}
		});
	}

	annulerRelation(annulBtn: HTMLElement, accptBtn: HTMLElement, index: number) {
		annulBtn.classList.add("disabled");
		if (accptBtn) accptBtn.classList.add("disabled");
		let ebRelation: EbRelation = new EbRelation();
		ebRelation.constructorCopy(this.listContact[index].ebRelation);
		this.userService.annulerRelation(ebRelation).subscribe((data: boolean) => {
			if (data && this.user.role != UserRole.PRESTATAIRE) {
				this.listContact[index].ebRelation.ebRelationNum = null;
				this.listContact[index].ebRelation.email = null;
				this.listContact[index].ebRelation.guest = null;
				this.listContact[index].ebRelation.etat = null;
				this.listContact[index].ebRelation.host = new EbUser();
				this.listContact[index].ebRelation.typeRelation = null;
			} else {
				this.listContact.splice(index, 1);
			}
		});
	}

	annulerInvitation(annulInvtBtn: HTMLElement, index: number) {
		annulInvtBtn.classList.add("disabled");
		let ebRelation: EbRelation = new EbRelation();
		ebRelation.constructorCopy(this.listContact[index].ebRelation);
		this.userService.annulerRelation(ebRelation).subscribe((data: boolean) => {
			if (data && this.user.role == UserRole.CHARGEUR) {
				this.listContact[index].ebRelation.ebRelationNum = null;
				this.listContact[index].ebRelation.email = null;
				this.listContact[index].ebRelation.etat = null;
				this.listContact[index].ebRelation.typeRelation = null;
			} else {
				this.listContact.splice(index, 1);
			}
		});
	}

	// -----------------------------------------------------

	open(content: any, index: number, btnSendInvitation: HTMLElement) {
		let user: EbUser = new EbUser();
		let etablisement: EbEtablissement = null;
		let showModel: boolean = false;
		let ebRelation = new EbRelation();
		let guest = this.listContact[index];

		btnSendInvitation.classList.add("disabled");
		ebRelation.host = new EbUser();
		ebRelation.host.ebUserNum = this.user.ebUserNum;
		ebRelation.email = guest.email;

		if (this.showContact) {
			ebRelation.guest = new EbUser();
			ebRelation.guest.ebUserNum = guest.ebUserNum;
			ebRelation.guest.role = guest.role;
			if (guest.role == UserRole.CONTROL_TOWER) ebRelation.etat = StatusDemande.ENCOURS;
			else ebRelation.etat = StatusDemande.ACCPETER;
			ebRelation.xEbEtablissementHost = new EbEtablissement();
			ebRelation.xEbEtablissementHost.ebEtablissementNum = this.user.ebEtablissement.ebEtablissementNum;
			ebRelation.xEbEtablissement = new EbEtablissement();
			ebRelation.xEbEtablissement.ebEtablissementNum = guest.ebEtablissement.ebEtablissementNum;
			if (guest.service == ServiceType.TRANSPORTEUR_BROKER && this.isChargeur) showModel = true;
			else if (guest.role != UserRole.CONTROL_TOWER) ebRelation.typeRelation = guest.service;
			else ebRelation.typeRelation = ServiceType.CONTROL_TOWER;
		} else if (this.isChargeur) {
			let selectedRel = this.listEbRelationWithEtablissement[index];
			ebRelation.xEbEtablissement = new EbEtablissement();
			ebRelation.xEbEtablissement.ebEtablissementNum =
				selectedRel.xEbEtablissementHost.ebEtablissementNum;
			ebRelation.xEbEtablissementHost = new EbEtablissement();
			ebRelation.xEbEtablissementHost.ebEtablissementNum = this.user.ebEtablissement.ebEtablissementNum;
			ebRelation.flagEtablissement = true;
			ebRelation.etat = StatusDemande.ENCOURS;
			if (selectedRel.xEbEtablissementHost.service == ServiceType.TRANSPORTEUR_BROKER)
				showModel = true;
			else ebRelation.typeRelation = selectedRel.xEbEtablissementHost.service;
		}

		if (showModel) {
			this.typeInvitation = null;
			this.ngbModal.open(content).result.then(
				(result) => {
					if (result) {
						ebRelation.typeRelation = this.typeInvitation;
						this.sendInvite(ebRelation, index, btnSendInvitation);
					} else {
						btnSendInvitation.classList.remove("disabled");
					}
				},
				(reason) => {
					btnSendInvitation.classList.remove("disabled");
				}
			);
		} else {
			this.sendInvite(ebRelation, index, btnSendInvitation);
		}
	}

	sendInvite(ebRelation: EbRelation, index: number, btnSendInvitation: HTMLElement) {
		this.userService.sendInvitaion(ebRelation).subscribe((response) => {
			if (response) {
				if (this.showContact) {
					this.listContact[index].ebRelation = response;
				} else {
					this.listEbRelationWithEtablissement[index].etat = EtatRelation.EN_COURS;
					this.listEbRelationWithEtablissement[index].xEbEtablissement.ebEtablissementNum =
						response["xEbEtablissement"].ebEtablissementNum;
					this.listEbRelationWithEtablissement[index].flagEtablissement = true;
				}
				btnSendInvitation.classList.remove("disabled");
			}
		});
	}

	newContact() {
		this.isInvitationBlockEnabled = true;
		this.SendInvitationDialog = true;
	}

	back() {
		this.invitationMessage = null;
		this.isInvitationBlockEnabled = false;
		this.addCarrierModal = false;
		this.typeInvitation = null;
		this.selectChargeurs = [];
		this.SendInvitationDialog = false;
	}

	addToChargeur(event) {
		if (this.selectChargeurs[0].ebUserNum == -1) {
			this.selectChargeurs = this.listEbUser;
		}
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.userService.addToAll(this.ebRelationToAll, this.selectChargeurs).subscribe((res) => {
			this.typeInvitation = null;
			this.selectChargeurs = [];
			requestProcessing.afterGetResponse(event);
			this.addCarrierModal = false;
			this.successfullOperation();
		});
	}

	successfullOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	sendInvitationByMail(event, email) {
		let requestProcessing = new RequestProcessing();
		if (email != null) {
			requestProcessing.beforeSendRequest(event);
			this.isInvitationBlockEnabled = false;
			this.inscriptionService.checkEmail(email).subscribe(
				(res) => {
					if (!res) {
						let ebRelation: EbRelation = new EbRelation();
						ebRelation.typeRelation =
							this.user.role == UserRole.CONTROL_TOWER ? ServiceType.CONTROL_TOWER : 3;
						ebRelation.email = this.emailToSend;
						ebRelation.host = new EbUser();
						ebRelation.host.ebUserNum = this.user.ebUserNum;
						this.userService.sendInvitationByMail(ebRelation).subscribe(
							(res) => {
								if (res["inRelation"] && res["user"] && res["inRelation"]) {
									ebRelation.copyFrom(res["ebRelation"], true);

									let exists = this.listContact.find((it, i) => {
										if (it.ebRelation.guest.ebUserNum == ebRelation.guest.ebUserNum) {
											this.listContact[i].ebRelation = ebRelation;
											return true;
										}
									});
									if (!exists) {
										let user: EbUser = new EbUser();
										user.ebRelation = ebRelation;
										user.ebUserNum = ebRelation.guest.ebUserNum;
										this.listContact.push(user);
									}
								}
								requestProcessing.afterGetResponse(event);
								this.dialogService.toasterDisplay(true);
								this.SendInvitationDialog = false;
								this.emailToSend = null;
								this.invitationMessage = "";
							},
							(error) => {
								requestProcessing.afterGetResponse(event);
								this.SendInvitationDialog = false;
								this.dialogService.toasterDisplay(false);
							}
						);
					} else {
						this.invitationMessage = this.translate.instant("COMMUNITY.EMAIL_EXISTANT");
					}
				},
				(error) => {
					requestProcessing.afterGetResponse(event);
					this.SendInvitationDialog = false;
					this.dialogService.toasterDisplay(false);
				}
			);
		}
	}

	onShowInvitModal() {
		this.emailToSend = null;
		this.invitationMessage = "";
	}
	getRoleLibelle(user: EbUser) {
		if (user.role == UserRole.CHARGEUR) {
			return this.ConstantsTranslate["CHARGEUR"];
		} else if (user.role == UserRole.CONTROL_TOWER) {
			return this.ConstantsTranslate["CONTROL_TOWER"];
		} else if (user.role == UserRole.PRESTATAIRE) {
			if (user.service == ServiceType.TRANSPORTEUR) {
				return this.ConstantsTranslate["TRANSPORTEUR"];
			} else if (user.service == ServiceType.BROKER) {
				return this.ConstantsTranslate["BROKER"];
			} else if (user.service == ServiceType.TRANSPORTEUR_BROKER) {
				if (user.ebRelation && user.ebRelation.typeRelation == ServiceType.TRANSPORTEUR) {
					return this.ConstantsTranslate["TRANSPORTEUR"];
				} else if (user.ebRelation && user.ebRelation.typeRelation == ServiceType.BROKER) {
					return this.ConstantsTranslate["BROKER"];
				} else {
					return this.ConstantsTranslate["TRANSPORTEUR_BROKER"];
				}
			} else {
				return this.ConstantsTranslate["TRANSPORTEUR_BROKER"];
			}
		}

		return "";
	}

	clearSearchField() {
		this.searchCriteria.searchterm = null;
		this.searchContact();
	}

	onPaginate(event) {
		this.searchCriteria.pageNumber = event.page + 1;
		this.searchCriteria.size = event.rows;
		this.numberDatasPerPage = this.searchCriteria.size;
		this.onChange(event);
		this.affecteFromAndToValues();
	}
	affecteFromAndToValues() {
		let searchCriteriaPageNumber = this.searchCriteria.pageNumber;
		this.fromPage = searchCriteriaPageNumber * this.numberDatasPerPage + 1;
		let currentMax = (searchCriteriaPageNumber + 1) * this.numberDatasPerPage;
		this.toPage = currentMax > this.totalRecords ? this.totalRecords : currentMax;
	}
	paginatorAdapter() {
		this.searchCriteria.pageNumber = this.searchCriteria.pageNumber - 1;
		this.affecteFromAndToValues();
	}

	@HostListener("window:resize", ["$event"])
	onResizeCommunity() {
		let windowHeight = window.innerHeight;
		let listEltsClass: string[] = [".app-header", ".community-header", ".ui-table-summary"];
		let padding = 45;
		if (this.userId != null) padding = 90;
		setTimeout(() => {
			this.Statique.onResizeAdapter(listEltsClass, "#community-wrapper", padding, windowHeight);
		}, 200);
	}
}
