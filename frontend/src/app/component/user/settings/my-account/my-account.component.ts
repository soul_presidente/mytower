import { SettingsComponent } from "../settings.component";
import { Component, OnInit } from "@angular/core";
import { Input } from "@app/utils/Input";
import { Subscription } from "rxjs";
import { EbUser } from "@app/classes/user";
import { Delegation } from "@app/classes/delegation";
import { UserService } from "@app/services/user.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { AuthenticationService } from "@app/services/authentication.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { Inscription } from "@app/classes/inscription";
import { ModalService } from "@app/shared/modal/modal.service";
import { NgbDateStruct, NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { Statique } from "@app/utils/statique";
import { TranslateService } from "@ngx-translate/core";
import { StatiqueService } from "@app/services/statique.service";
import { ParamsMail } from "@app/classes/paramsMail";
import { ServiceType, UserRole } from "@app/utils/enumeration";
import SingletonStatique from "@app/utils/SingletonStatique";
import { MessageService } from "primeng/api";
import { EcFuseauxHoraire } from "../../../../classes/fuseauHoraire";
import { Router } from "@angular/router";

@Component({
	selector: "app-settings-my-account",
	templateUrl: "./my-account.component.html",
	styleUrls: ["./my-account.component.scss"],
})
export class _MyAccountComponent extends SettingsComponent implements OnInit {
	UserRole = UserRole;
	ServiceType = ServiceType;

	newUser: EbUser = new EbUser();
	delegation: Delegation = new Delegation();

	inputsFriendsEmail: Array<Input>;
	inputsColleguesEmail: Array<Input>;

	listUser: Array<EbUser> = new Array<EbUser>();

	newParamsMail: Array<ParamsMail> = new Array<ParamsMail>();

	listMails: Array<ParamsMail> = new Array<ParamsMail>();

	configEmailSubscription: Subscription;

	_password: string;
	nvPassword: string;
	confirmationPassword: string;
	closeResult: string;
	msgPassword: string;
	defaultLang: string;
	isValidPassword: boolean;

	delegStructDateDebut: NgbDateStruct;
	delegStructDateFin: NgbDateStruct;

	Statique = Statique;

	currentDate: Date = new Date();

	errorMessage = [];

	disableSave = true;

	userOrListMailLoaded = false;

	listFuseauHoraire: Array<EcFuseauxHoraire> = new Array<EcFuseauxHoraire>();

	constructor(
		protected userService: UserService,
		protected modalService: ModalService,
		private ngbService: NgbModal,
		protected authenticationService: AuthenticationService,
		public translate: TranslateService,
		public statiqueService: StatiqueService,
		protected router: Router,
		private messageService?: MessageService
	) {
		super(authenticationService);

		this.inputsFriendsEmail = new Array<Input>();
		this.inputsColleguesEmail = new Array<Input>();
		translate.addLangs(["en", "fr"]);
	}

	ngOnInit() {
		this.initListMail();
		this.disableSave = false;
		this.newUser = new EbUser();

		this.delegStructDateDebut = Statique.formatDateToStructure(new Date());

		this.getUser();
		this.getBackupUsers();
		this.getDelegation();

		this.statiqueService.getCurrentDate().subscribe((dt) => {
			this.currentDate = Statique.getDateFromDatetime(dt);
		});

		this.msgPassword = "";
		this.loadListFuseauHoraire();
	}

	initListMail() {
		this.statiqueService.getListEmailParam().subscribe((res: Array<ParamsMail>) => {
			this.listMails = res;
			this.updateListMailParam();
		});
	}

	updateListMailParam() {
		if (!this.userOrListMailLoaded) return (this.userOrListMailLoaded = true);

		if (this.newUser.listParamsMail == null || this.newUser.listParamsMail == []) {
			this.newUser.listParamsMail = this.listMails;
		}
		let i, j, found;
		if (this.listMails.length != this.newUser.listParamsMail.length) {
			for (i = 0; i < this.listMails.length; i++) {
				found = false;
				for (j = 0; j < this.newUser.listParamsMail.length; j++) {
					if (this.listMails[i].emailId == this.newUser.listParamsMail[j].emailId) {
						found = true;
						break;
					}
				}
				if (!found) this.newUser.listParamsMail.push(this.listMails[i]);
			}
		}

		if (this.newUser.listParamsMail != null && this.newUser.listParamsMail.length > 0) {
			for (i = 0; i < this.newUser.listParamsMail.length; i++) {
				for (j = 0; j < this.newUser.listParamsMail.length; j++) {
					if (this.listMails[i].emailId == this.newUser.listParamsMail[j].emailId) {
						this.newUser.listParamsMail[j].roles = this.listMails[i].roles;
						this.newUser.listParamsMail[j].services = this.listMails[i].services;
						this.newUser.listParamsMail[j].emailName = this.listMails[i].emailName;
						this.newUser.listParamsMail[j].hidden = this.listMails[i].hidden;
						break;
					}
				}
			}
		}

		this.listMails = ((): Array<ParamsMail> =>
			JSON.parse(JSON.stringify(this.newUser.listParamsMail)))();
	}

	getUser() {
		this.userService.userInfo().subscribe((rep: EbUser) => {
			this.newUser = rep;

			this.updateListMailParam();

			if (this.newUser != null) {
				if (
					this.newUser.language == null &&
					(!window.localStorage.getItem("currentLang") ||
						window.localStorage.getItem("currentLang").length == 0 ||
						window.localStorage.getItem("currentLang") === "null")
				) {
					this.defaultLang = "en";
					console.warn(
						"la valeur de language de cet user est nulle et même sur local storage au niveau de current language"
					);
				} else {
					if (this.newUser.language) this.defaultLang = this.newUser.language;
					else this.defaultLang = window.localStorage.getItem("currentLang");
				}
			}
		});
	}

	getBackupUsers() {
		let criteria: SearchCriteria = new SearchCriteria();
		criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		criteria.excludeConnectedUser = true;
		this.userService.getListUser(criteria).subscribe((data) => {
			this.listUser = data.map((it) => {
				let user = new EbUser();
				user.ebUserNum = it.ebUserNum;
				user.nom = it.nom;
				user.prenom = it.prenom;
				user.email = it.email;
				return user;
			});
		});
	}

	getConnectedUserInfo() {
		this.userService.getUserInfo().subscribe((rep) => {
			this.newUser = rep;
			//console.log(this.newUser);
		});
	}

	updateUser(event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.userService.updateUser(this.newUser).subscribe(
			(rep) => {
				this.newUser.nom = rep.nom;
				this.newUser.prenom = rep.prenom;
				this.newUser.adresse = rep.adresse;
				this.newUser.email = rep.email;
				this.newUser.telephone = rep.telephone;

				this.userService.getToken(rep.ebUserNum).subscribe((res) => {
					localStorage.setItem("access_token", res);
					this.userConnected = AuthenticationService.getConnectedUser();
				});
				requestProcessing.afterGetResponse(event);
				this.showSuccess();
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				this.showError();
			}
		);
	}

	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE"),
		});
	}

	changePassword() {
		//this.newUser.ebUserNum = 1;
		this.userService
			.matchUserPassword(this.newUser.ebUserNum, this._password)
			.subscribe((result: boolean) => {
				if (result) {
					this.newUser.password = this.nvPassword;
					this.userService.updatePasswordUser(this.newUser).subscribe((rep) => {
						this.newUser.password = rep.password;
						this.msgPassword = this.translate.instant("MY_ACCOUNT.MESSAGE_VALID_PASSWORD");
						this.isValidPassword = true;
					});
				} else {
					this.msgPassword = this.translate.instant("MY_ACCOUNT.MESSAGE_INVALID_PASSWORD");
					this.isValidPassword = false;
				}
			});
	}

	getDelegation() {
		let criteria: SearchCriteria = new SearchCriteria();
		criteria.ebUserNum = this.userConnected.ebUserNum;

		this.userService.getDelegation(criteria).subscribe((res) => {
			if (res && res.ebDelegationNum) {
				this.delegation = res;
				this.delegation.dateDebut = new Date(res.dateDebut);
				this.delegation.dateFin = new Date(res.dateFin);
				this.delegStructDateDebut = Statique.formatDateToStructure(this.delegation.dateDebut);
				this.delegStructDateFin = Statique.formatDateToStructure(this.delegation.dateFin);
			} else {
				this.delegStructDateDebut = Statique.formatDateToStructure(this.currentDate);
			}
		});
	}

	compareUser(user1: EbUser, user2: EbUser) {
		if (user1) return user1.ebUserNum == user2.ebUserNum;

		return false;
	}

	createBackUp(event) {
		if (!this.delegStructDateDebut || !this.delegStructDateFin || !this.delegation.userBackUp)
			return;

		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.delegation.userConnect = new EbUser();
		this.delegation.userConnect.ebUserNum = this.newUser.ebUserNum;
		this.delegation.userConnect.nom = this.newUser.nom;
		this.delegation.userConnect.prenom = this.newUser.prenom;
		this.delegation.userConnect.email = this.newUser.email;
		this.delegation.dateDebut = Statique.fromDatePicker(this.delegStructDateDebut);
		this.delegation.dateFin = Statique.fromDatePicker(this.delegStructDateFin);
		this.userService.createDelegation(this.delegation).subscribe((rep) => {
			this.delegation = rep;
			requestProcessing.afterGetResponse(event);
		});
	}

	deleteBackup(event) {
		if (this.delegation && this.delegation.ebDelegationNum) {
			let requestProcessing = new RequestProcessing();
			requestProcessing.beforeSendRequest(event);
			this.userService.deleteDelegation(this.delegation.ebDelegationNum).subscribe((res) => {
				if (res) {
					this.delegation = new Delegation();
					this.delegStructDateDebut = Statique.formatDateToStructure(this.currentDate);
					this.delegStructDateFin = null;
				}
				requestProcessing.afterGetResponse(event);
			});
		}
	}

	checkDateDelegation() {
		let dtDeb = Statique.fromDatePicker(this.delegStructDateDebut);
		let dtFin = Statique.fromDatePicker(this.delegStructDateFin);
		return dtDeb <= dtFin && dtDeb >= this.currentDate && dtFin >= this.currentDate;
	}

	addInput(inputs: any) {
		let input: Input = new Input();
		input.id = inputs.length + 1;
		inputs.push(input);
	}

	removeInput(inputs: Array<Input>, index: number) {
		inputs.splice(-index, 1);
		//console.log(index);
	}

	inviteFriend(event: any) {
		//let requestProcessing = new RequestProcessing();
		//requestProcessing.beforeSendRequest(event);
		let inscription = new Inscription();
		inscription.ebUser = this.newUser;
		inscription.friendInvitationEmails = this.inputsFriendsEmail.map((input) => input.email);
		inscription.collegueInvitationEmails = this.inputsColleguesEmail.map((input) => input.email);
		this.userService.sendInvitationMail(inscription).subscribe((user: EbUser) => {
			//requestProcessing.afterGetResponse(event);
		});
	}

	changeLanguage(lang: string) {
		this.newUser.language = lang;
		const decodedUser = AuthenticationService.decodeTocken(
			window.localStorage.getItem("access_token")
		);

		decodedUser.language = lang;
		this.userService.updateUserLanguage(decodedUser).subscribe((res) => {
			if (res != null) {
				window.localStorage.setItem("currentLang", lang);
				SingletonStatique.lang = lang;
				window.localStorage.setItem("access_token", res);
				this.translate.use(lang);
				Statique.refreshComponent(this.router, this.router.url, true);
			}
		});
	}

	saveConfigEmail(event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.newUser.listParamsMail = this.listMails;
		this.userService.saveConfigEmail(this.newUser).subscribe(
			(user: EbUser) => {
				requestProcessing.afterGetResponse(event);
				this.showSuccess();
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				this.showError();
				// let title = null;
				// let body = null;
				// this.translate.get("MODAL.ERROR").subscribe((res: string) => {
				//   title = res;
				// });
				// // this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
				// this.modalService.error(title, null, function () { });
			}
		);
	}

	/* check I am concerned if colleague concerned */
	colleagueChecked(event, index, eltCreatedByMe = null) {
		if (event.target.checked) {
			this.newUser.listParamsMail[index].createdByMe = true;
			if (eltCreatedByMe) $(eltCreatedByMe).prop("checked", true);
		}
	}

	/* incheck colleague concerned if I am concerned inchecked */
	iamChecked(event, index) {
		if (!event.target.checked) {
			this.newUser.listParamsMail[index].createdByOtherUser = false;
		}
	}

	/*******************************************/
	/********* Validation des emails **********/
	onAddMail(event, index) {
		if (!this.validateEmail(event.value)) {
			this.errorMessage[index] = true;
			this.disableSave = true;
		}
	}

	checkValidity() {
		this.disableSave = false;
		this.listMails.forEach((listM) => {
			if (listM.listEmailEnCopie && listM.listEmailEnCopie.length > 0) {
				listM.listEmailEnCopie.forEach((element) => {
					if (!this.validateEmail(element)) {
						this.disableSave = true;
						return;
					}
				});
			}
		});
	}

	onRemoveMail(listM, index) {
		this.errorMessage[index] = false;
		listM.forEach((element) => {
			if (!this.validateEmail(element)) {
				this.errorMessage[index] = true;
				this.disableSave = true;
			}
		});
		this.checkValidity();
	}

	validateEmail(email) {
		var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return pattern.test(String(email).toLowerCase());
	}

	contains(str, val) {
		if (str != null && val != null) return str.indexOf(val + "") >= 0;
		return false;
	}

	async loadListFuseauHoraire() {
		this.listFuseauHoraire = await SingletonStatique.getListFuseauxHoraire();
	}

	comparefuseauHoraire(fuseauxHoraire1: EcFuseauxHoraire, fuseauxHoraire2: EcFuseauxHoraire) {
		if (fuseauxHoraire2 !== undefined && fuseauxHoraire2 !== null) {
			return fuseauxHoraire1.ecFuseauHoraireNum === fuseauxHoraire2.ecFuseauHoraireNum;
		}
	}

	compareSelectData(fuseauxHoraire1: EcFuseauxHoraire, fuseauxHoraire2: EcFuseauxHoraire) {
		if (fuseauxHoraire1 && fuseauxHoraire2) {
			return fuseauxHoraire1 === fuseauxHoraire2;
		}
	}
}
