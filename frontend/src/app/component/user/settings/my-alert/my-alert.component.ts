import { Component, OnInit } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { UserService } from "@app/services/user.service";
import { EbUser } from "@app/classes/user";
import { ParamsMail } from "@app/classes/paramsMail";
import { StatiqueService } from "@app/services/statique.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { MessageService } from "primeng/api";
import { ServiceType, UserRole } from "@app/utils/enumeration";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-my-alert",
	templateUrl: "./my-alert.component.html",
	styleUrls: ["./my-alert.component.scss"],
})
export class MyAlertComponent extends ConnectedUserComponent implements OnInit {
	newUser: EbUser = new EbUser();
	newParamsMail: Array<ParamsMail> = new Array<ParamsMail>();
	listMails: Array<ParamsMail> = new Array<ParamsMail>();
	disableSave = true;
	userOrListMailLoaded = false;
	errorMessage = [];

	UserRole = UserRole;
	ServiceType = ServiceType;
	constructor(
		protected userService: UserService,
		public statiqueService: StatiqueService,
		private messageService?: MessageService,
		private translate?: TranslateService
	) {
		super();
	}

	ngOnInit() {
		this.initListMail();
		this.disableSave = false;
		this.newUser = new EbUser();
		this.getUser();
	}

	initListMail() {
		this.statiqueService.getListEmailParam().subscribe((res: Array<ParamsMail>) => {
			this.listMails = res;
			this.translator(this.listMails);
			this.updateListMailParam();
		});
	}

	translator(tab: any[]) {
		tab.forEach((it) => {
			this.translate
				.get("MY_ALERT.ALERT-" + it.emailId)
				.subscribe((res) => (it["emailName"] = res));
		});
	}

	updateListMailParam() {
		if (!this.userOrListMailLoaded) return (this.userOrListMailLoaded = true);

		if (this.newUser.listParamsMail == null || this.newUser.listParamsMail == []) {
			this.newUser.listParamsMail = this.listMails;
		}
		let i, j, found;
		if (this.listMails.length != this.newUser.listParamsMail.length) {
			for (i = 0; i < this.listMails.length; i++) {
				found = false;
				for (j = 0; j < this.newUser.listParamsMail.length; j++) {
					if (this.listMails[i].emailId == this.newUser.listParamsMail[j].emailId) {
						found = true;
						break;
					}
				}
				if (!found) this.newUser.listParamsMail.push(this.listMails[i]);
			}
		}

		if (this.newUser.listParamsMail != null && this.newUser.listParamsMail.length > 0) {
			for (i = 0; i < this.newUser.listParamsMail.length; i++) {
				for (j = 0; j < this.newUser.listParamsMail.length; j++) {
					if (this.listMails[i].emailId == this.newUser.listParamsMail[j].emailId) {
						this.newUser.listParamsMail[j].roles = this.listMails[i].roles;
						this.newUser.listParamsMail[j].services = this.listMails[i].services;
						this.newUser.listParamsMail[j].emailName = this.listMails[i].emailName;
						this.newUser.listParamsMail[j].hidden = this.listMails[i].hidden;
						break;
					}
				}
			}
		}

		this.listMails = ((): Array<ParamsMail> =>
			JSON.parse(JSON.stringify(this.newUser.listParamsMail)))();
	}

	getUser() {
		this.userService.userInfo().subscribe((rep: EbUser) => {
			this.newUser = rep;

			this.updateListMailParam();
		});
	}

	saveConfigEmail(event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		//this.newUser.listParamsMail = this.listMails;
		this.userService.saveConfigEmail(this.newUser).subscribe(
			(user: EbUser) => {
				requestProcessing.afterGetResponse(event);
				this.showSuccess();
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				this.showError();
				// let title = null;
				// let body = null;
				// this.translate.get("MODAL.ERROR").subscribe((res: string) => {
				//   title = res;
				// });
				// // this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
				// this.modalService.error(title, null, function () { });
			}
		);
	}

	contains(str, val) {
		if (str != null && val != null) return str.indexOf(val + "") >= 0;
		return false;
	}

	onRemoveMail(listM, index) {
		this.errorMessage[index] = false;
		listM.forEach((element) => {
			if (!this.validateEmail(element)) {
				this.errorMessage[index] = true;
				this.disableSave = true;
			}
		});
		this.checkValidity();
	}

	onAddMail(event, index) {
		if (!this.validateEmail(event.value)) {
			this.errorMessage[index] = true;
			this.disableSave = true;
		}
	}

	checkValidity() {
		this.disableSave = false;
		this.listMails.forEach((listM) => {
			if (listM.listEmailEnCopie && listM.listEmailEnCopie.length > 0) {
				listM.listEmailEnCopie.forEach((element) => {
					if (!this.validateEmail(element)) {
						this.disableSave = true;
						return;
					}
				});
			}
		});
	}
	validateEmail(email) {
		var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return pattern.test(String(email).toLowerCase());
	}

	/* check I am concerned if colleague concerned */
	colleagueChecked(event, index, eltCreatedByMe = null) {
		if (event.target.checked) {
			this.newUser.listParamsMail[index].createdByMe = true;
			this.newUser.listParamsMail[index].createdByOtherUser = true;
			if (eltCreatedByMe) $(eltCreatedByMe).prop("checked", true);
		}
		else{
			this.newUser.listParamsMail[index].createdByOtherUser = false;
		}
	}

	/* incheck colleague concerned if I am concerned inchecked */
	iamChecked(event, index, eltCreatedByOthers = null) {
		if (!event.target.checked) {
			this.newUser.listParamsMail[index].createdByMe = false;
			this.newUser.listParamsMail[index].createdByOtherUser = false;
			if (eltCreatedByOthers) $(eltCreatedByOthers).prop("checked", false);
		}
		else {
			this.newUser.listParamsMail[index].createdByMe = true;
		}
	}

	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE"),
		});
	}
}
