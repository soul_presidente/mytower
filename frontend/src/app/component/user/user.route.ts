import { Route, RouterModule } from "@angular/router";
import { UserComponent } from "./user.component";
import { SettingsComponent } from "./settings/settings.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";
import { ExtractorsComponent } from "./settings/extractors/extractors.component";
import { _MyAccountComponent } from "./settings/my-account/my-account.component";
import { _MyCommunityComponent } from "./settings/my-community/my-community.component";
import { _MyCompanyComponent } from "./settings/my-company/my-company.component";
import { MyAlertComponent } from "./settings/my-alert/my-alert.component";
import { BoutonActionComponent } from "./settings/my-company/bouton-action/bouton-action.component";
import { CompanySettingsComponent } from "./settings/my-company/company-settings/company-settings.component";
import { UserSettingsComponent } from "./settings/my-company/user-settings/user-settings.component";
import { CompanyProfileComponent } from "./settings/my-company/company-profile/company-profile.component";
import { FlagManagementComponent } from "./settings/my-company/flag-management/flag-management.component";
import { RuleComponent } from "./settings/my-company/rule/rule.component";
import { CurrencyEtablissementComponent } from "./settings/my-company/currency-etablissement/currency-etablissement.component";
import { TypeDocumentComponent } from "../type-document/type-document.component";
import { DocumentTemplatesComponent } from "./settings/my-company/document-templates/document-templates.component";
import { DockManagementSettingsComponent } from "./settings/my-company/dock-management/dock-management.component";
import { ConfigQrGroupeComponent } from "@app/shared/config-qr-groupe/config-qr-groupe.component";
import { ConfigSchemaPslComponent } from "./settings/my-company/config-schema-psl/config-schema-psl.component";
import { TypeUnitsComponent } from "./settings/my-company/type-units/type-units.component";
import { ConfigPslComponent } from "./settings/my-company/config-psl/config-psl.component";
import { TypeRequestComponent } from "./settings/my-company/type-request/type-request.component";
import { ParametrageMtcComponent } from "./settings/my-company/parametrage-mtc/parametrage-mtc.component";
import { UsersManagementComponent } from "./settings/my-company/users-management/users-management.component";
import { ShipToMatrixComponent } from "./settings/my-company/ship-to-matrix/ship-to-matrix.component";
import { ProfilesManagementComponent } from "./settings/my-company/profiles-management/profiles-management.component";
import { AdressesComponent } from "./settings/my-company/adresses/adresses.component";
import { CostCentersComponent } from "./settings/my-company/cost-centers/cost-centers.component";
import { GlobalTransportionPlanComponent } from "./settings/my-company/global-transportion-plan/global-transportion-plan.component";
import { UserDetailsComponent } from "./settings/my-company/users-management/user-details/user-details.component";
import { DataRecoveryComponent } from "./settings/my-company/data-recovery/data-recovery.component";
import { UserRole } from "@app/utils/enumeration";
import { RegimesTemporairesComponent } from "./settings/my-company/regimes-temporaires/regimes-temporaires.component";
import { CustomSettingComponent } from "./settings/my-company/custom-setting/custom-setting.component";
import { TracingCronjobMonitoringComponent } from "./settings/my-company/tracing-cronjob-monitoring/tracing-cronjob-monitoring.component";
import { TypeGoodsComponent } from "./settings/my-company/type-goods/type-goods.component";
import { ConfigTypeFluxComponent } from "./settings/my-company/config-type-flux/config-type-flux.component";
import { ConfigIncotermComponent } from "./settings/my-company/config-incoterm/config-incoterm.component";
import { AclRightsComponent } from "./settings/my-company/acl-rights/acl-rights.component";
import { MyCommunityUserDetailsComponent } from "./settings/my-company/users-management/user-details/user-details-community/user-details-community.component";
import { TransfertEtablissementComponent } from "./settings/my-company/transfert-etablissement/transfert-etablissement.component";
import { WeekTemplateComponent } from "@app/shared/week-template/week-template.component";
import { PlanTransportNewComponent } from "./settings/my-company/plan-transport-new/plan-transport-new.component";
import { GlobalPlanTransportNewComponent } from "./settings/my-company/global-plan-transport-new/global-plan-transport-new.component";
import { MappingCodePslComponent } from "./settings/my-company/mapping-code-psl/mapping-code-psl.component";
import { MappingCodePslDetailsComponent } from "./settings/my-company/mapping-code-psl-details/mapping-code-psl-details.component";
import { ControlRulesComponent } from "./settings/control-rules/control-rules.component";
import { TemplateExportDocComponent } from "./settings/template-export-doc/template-export-doc.component";
import { ConfigCostsComponent } from "./settings/my-company/config-costs/config-costs.component";
import {CostsItemsComponent} from "@app/component/user/settings/my-company/costs-items/costs-items.component";

export const USER_ROUTES: Route[] = [
	{
		path: "",
		component: UserComponent,
		canActivate: [UserAuthGuardService],
	},

	{
		path: "settings",
		component: SettingsComponent,
		canActivate: [UserAuthGuardService],

		children: [
			{
				path: "account-settings",
				component: _MyAccountComponent,
				canActivate: [UserAuthGuardService],
			},
			{
				path: "company",
				component: _MyCompanyComponent,
				canActivate: [UserAuthGuardService],

				children: [
					{
						path: "company-settings",
						component: CompanySettingsComponent,
						canActivate: [UserAuthGuardService],
					},
					{
						path: "company-profil",
						component: CompanyProfileComponent,
						canActivate: [UserAuthGuardService],
					},
					{
						path: "flags",
						component: FlagManagementComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "rules",
						component: RuleComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "document",
						component: TypeDocumentComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "doc-templates",
						component: DocumentTemplatesComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "exchange-rate",
						component: CurrencyEtablissementComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "references",
						component: CostCentersComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "regimes-temporaires",
						component: RegimesTemporairesComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "custom-setting",
						component: CustomSettingComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "control-rules",
						component: ControlRulesComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "adresses",
						component: AdressesComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "transport-plan",
						component: GlobalTransportionPlanComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "load-planning",
						component: GlobalPlanTransportNewComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "dock-management",
						component: DockManagementSettingsComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "regroupement-qr",
						component: ConfigQrGroupeComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "schema-psl",
						component: ConfigSchemaPslComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "type-flux",
						component: ConfigTypeFluxComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "incoterms",
						component: ConfigIncotermComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "configuration-psl",
						component: ConfigPslComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "type-unit",
						component: TypeUnitsComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "type-goods",
						component: TypeGoodsComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "type-request",
						component: TypeRequestComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "parametrage-mtc",
						component: ParametrageMtcComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "mapping-code-psl",
						component: MappingCodePslComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "mapping-code-psl/new",
						component: MappingCodePslDetailsComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "mapping-code-psl/:ediMapPslNum",
						component: MappingCodePslDetailsComponent,
						canActivate: [UserAuthGuardService],
						data: { superAdmin: true },
					},
					{
						path: "cronjob-monitoring",
						component: TracingCronjobMonitoringComponent,
						canActivate: [UserAuthGuardService],
					},

					{
						path: "user-settings",
						component: UserSettingsComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "users",
						component: UsersManagementComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "users/creation",
						component: UserDetailsComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "users/:ebUserNum",
						component: UserDetailsComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "ship-to-matrix",
						component: ShipToMatrixComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "profiles-management",
						component: ProfilesManagementComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "data_recovery",
						component: DataRecoveryComponent,
						canActivate: [UserAuthGuardService],
						data: { role: UserRole.CONTROL_TOWER, superAdmin: true },
					},
					{
						path: "acl/user/:ebUserNum",
						component: AclRightsComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "acl/profile/:ebUserProfileNum",
						component: AclRightsComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "community/user/:ebUserNum",
						component: MyCommunityUserDetailsComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "etablissemnt/transfert/:ebUserNum",
						component: TransfertEtablissementComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "additional-costs",
						component: ConfigCostsComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
					{
						path: "costs-items",
						component: CostsItemsComponent,
						canActivate: [UserAuthGuardService],
						data: { adminOrSuperAdmin: true },
					},
				],
			},
			{
				path: "community",
				component: _MyCommunityComponent,
				canActivate: [UserAuthGuardService],
			},
			{
				path: "alerts",
				component: MyAlertComponent,
				canActivate: [UserAuthGuardService],
			},
			{
				path: "button-actions",
				component: BoutonActionComponent,
				canActivate: [UserAuthGuardService],
			},
		],
	},

	{
		path: "extractors",
		component: ExtractorsComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "settings/doc/export-template",
		component: TemplateExportDocComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const UserRoutes = RouterModule.forChild(USER_ROUTES);
