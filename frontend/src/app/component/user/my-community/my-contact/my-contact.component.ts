import { Component, OnInit } from "@angular/core";
import { EbUser } from "@app/classes/user";
import { UserService } from "@app/services/user.service";
import { Statique } from "@app/utils/statique";
import { EtatRelation, ServiceType } from "@app/utils/enumeration";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbEtablissement } from "@app/classes/etablissement";
import { MyCommunityComponent } from "../my-community.component";

@Component({
	selector: "app-my-contact",
	templateUrl: "./my-contact.component.html",
	styleUrls: ["./my-contact.component.scss"],
})
export class MyContactComponent extends MyCommunityComponent implements OnInit {
	connectedUser: EbUser = new EbUser();
	Statique = Statique;
	EtatRelation = EtatRelation;
	searchCriteria: SearchCriteria = new SearchCriteria();
	ServiceType = ServiceType;
	showContact = true;
	listEtablissementOfContact: Array<EbEtablissement>;

	constructor(protected userService: UserService) {
		super();
		this.listEtablissementOfContact = new Array<EbEtablissement>();
		this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;
	}

	ngOnInit() {
		this.userService.getUserContacts(this.searchCriteria).subscribe((data) => {
			this.connectedUser.contacts = data;
		});
	}

	editRelation(etat: number, index: number) {
		// let ebRelation: EbRelation = new EbRelation()
		// ebRelation.constructorCopy(this.connectedUser.contacts[index]);
		// ebRelation.etat = etat;
		// this.userService.updateUserContacts(ebRelation).subscribe((data: EbRelation) => {
		//   this.connectedUser.contacts[index].etat = data.etat;
		// });
	}

	searchTerm() {
		if (this.showContact) this.searchContact();
		else this.searchEtablissement();
	}

	searchContact() {
		this.searchCriteria.pageNumber = 0;
		this.userService.getUserContacts(this.searchCriteria).subscribe((data) => {
			this.connectedUser.contacts = data;
		});
	}

	searchEtablissement() {
		this.searchCriteria.pageNumber = 0;
		this.userService.getEtablissementsContact(this.searchCriteria).subscribe((data) => {
			this.listEtablissementOfContact = data;
		});
	}

	changeContactOrEtablissement() {
		if (this.showContact) this.searchContact();
		else this.searchEtablissement();
	}

	getMoreData() {
		this.searchCriteria.pageNumber += 1;
		if (this.showContact) {
			this.userService.getUserContacts(this.searchCriteria).subscribe((data) => {
				this.connectedUser.contacts = this.connectedUser.contacts.concat(data);
			});
		} else {
			this.userService.getEtablissementsContact(this.searchCriteria).subscribe((data) => {
				this.listEtablissementOfContact = this.listEtablissementOfContact.concat(data);
			});
		}
	}
}
