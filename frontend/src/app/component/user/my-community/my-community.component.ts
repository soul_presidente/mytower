import { Component, OnInit } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";

@Component({
	selector: "app-my-community",
	templateUrl: "./my-community.component.html",
	styleUrls: ["./my-community.component.scss"],
})
export class MyCommunityComponent extends ConnectedUserComponent implements OnInit {
	constructor() {
		super();
	}
	ngOnInit() {}
}
