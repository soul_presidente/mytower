import { Component, OnInit } from "@angular/core";
import { UserService } from "@app/services/user.service";
import { Statique } from "@app/utils/statique";
import { EbUser } from "@app/classes/user";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { DatePipe } from "@angular/common";
import { ServiceType } from "@app/utils/enumeration";
import { MyCommunityComponent } from "../my-community.component";

@Component({
	selector: "app-contact-request",
	templateUrl: "./contact-request.component.html",
	styleUrls: ["./contact-request.component.css"],
})
export class ContactRequestComponent extends MyCommunityComponent implements OnInit {
	user: EbUser = new EbUser();
	searchCriteria: SearchCriteria = new SearchCriteria();
	Statique = Statique;
	ServiceType = ServiceType;

	constructor(protected userService: UserService, private datePipe: DatePipe) {
		super();
	}

	// accepterRelation(index:number){
	//   let ebContactRequest: EbContactRequest = new EbContactRequest()
	//   ebContactRequest.constructorCopy(this.user.receivingResquest[index]);
	//   this.userService.accepterRelation(ebContactRequest).subscribe(
	//     (data: boolean) => {
	//       if(data)
	//       {
	//         this.user.receivingResquest.splice(index, 1);
	//       }
	//     });
	// }

	// ignorerRelation(index:number){
	//   let ebContactRequest: EbContactRequest = new EbContactRequest()
	//   ebContactRequest.constructorCopy(this.user.receivingResquest[index]);
	//   this.userService.ignorerRelation(ebContactRequest).subscribe(
	//     (data: boolean) => {
	//       if(data)
	//       {
	//         this.user.receivingResquest.splice(index, 1);
	//       }
	//     });
	// }

	// annulerRelation(index:number){
	//   let ebContactRequest: EbContactRequest = new EbContactRequest()
	//   ebContactRequest.constructorCopy(this.user.sendingRequest[index]);
	//   this.userService.annulerRelation(ebContactRequest).subscribe(
	//     (data: boolean) => {
	//       if(data)
	//       {
	//         this.user.sendingRequest.splice(index, 1);
	//       }
	//     });
	// }

	ngOnInit() {
		this.searchCriteria.ebUserNum = UserService.getConnectedUser().ebUserNum;
		//this.searchCriteria.ebUserNum = 1;
		//this.searchCriteria.status = 0;
		this.searchCriteria.demandeEnCours;
		this.searchCriteria.admin;
		this.userService.getUserReceivingRequest(this.searchCriteria).subscribe((data) => {
			this.user.receivingResquest = data;
		});

		this.userService.getUserSendingRequest(this.searchCriteria).subscribe((data) => {
			this.user.sendingRequest = data;
		});
	}
}
