import { Component, OnInit } from "@angular/core";
import { MyCommunityComponent } from "../my-community.component";
import { EbUser } from "@app/classes/user";
import { Statique } from "@app/utils/statique";
import { EtatRelation, ServiceType, StatusDemande, UserRole } from "@app/utils/enumeration";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbEtablissement } from "@app/classes/etablissement";
import { UserService } from "@app/services/user.service";
import { EtablissementService } from "@app/services/etablissement.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbContactRequest } from "@app/classes/contactRequest";

@Component({
	selector: "app-new-contact",
	templateUrl: "./new-contact.component.html",
	styleUrls: ["./new-contact.component.scss"],
})
export class NewContactComponent extends MyCommunityComponent implements OnInit {
	Statique = Statique;
	EtatRelation = EtatRelation;
	searchCriteria: SearchCriteria = new SearchCriteria();
	ServiceType = ServiceType;
	showContact = true;
	listEtablissement: Array<EbEtablissement> = null;
	listUser: Array<EbUser> = null;
	typeInvitation: number = null;

	constructor(
		protected userService: UserService,
		protected etablissementService: EtablissementService,
		protected modalService: NgbModal
	) {
		super();
		this.listEtablissement = new Array<EbEtablissement>();
		this.listUser = new Array<EbUser>();
		if (this.isChargeur) {
			this.searchCriteria.ecRoleNum = UserRole.PRESTATAIRE;
		}
		this.searchCriteria.forInvite = true;
	}

	ngOnInit(): void {}

	searchTerm() {
		if (this.showContact) this.searchContact();
		else this.searchEtablissement();
	}

	searchContact() {
		if (this.searchCriteria.searchterm != "") {
			this.searchCriteria.pageNumber = 0;
			this.userService.getListUser(this.searchCriteria).subscribe((data) => {
				this.listUser = data;
			});
		} else {
			this.listUser = new Array<EbUser>();
		}
	}

	searchEtablissement() {
		if (this.searchCriteria.searchterm != "") {
			this.searchCriteria.pageNumber = 0;
			this.etablissementService.getListEtablisement(this.searchCriteria).subscribe((data) => {
				this.listEtablissement = data;
			});
		} else {
			this.listEtablissement = new Array<EbEtablissement>();
		}
	}

	changeContactOrEtablissement() {
		if (this.showContact) this.searchContact();
		else this.searchEtablissement();
	}

	getMoreData() {
		if (this.searchCriteria.searchterm != "") {
			this.searchCriteria.pageNumber += 1;
			if (this.showContact) {
				this.userService.getListUser(this.searchCriteria).subscribe((data) => {
					this.listUser = this.listUser.concat(data);
				});
			} else {
				this.etablissementService.getListEtablisement(this.searchCriteria).subscribe((data) => {
					this.listEtablissement = this.listEtablissement.concat(data);
				});
			}
		} else {
			if (this.showContact) {
				this.listUser = new Array<EbUser>();
			} else {
				this.listEtablissement = new Array<EbEtablissement>();
			}
		}
	}

	open(content: any, index: number, event: any) {
		let requestProcessing = new RequestProcessing();
		let user: EbUser = null;
		let etablisement: EbEtablissement = null;
		let showModel: boolean = false;
		let ebContactRequest: EbContactRequest = new EbContactRequest();

		ebContactRequest.expediteur = this.userConnected;

		requestProcessing.beforeSendRequest(event);

		if (this.showContact) {
			user = this.listUser[index];
			ebContactRequest.destinataire = user;
			ebContactRequest.status = StatusDemande.ACCPETER;
			if (user.service == ServiceType.TRANSPORTEUR_BROKER && this.isChargeur) showModel = true;
			else ebContactRequest.typeRelation = user.service;
		} else {
			etablisement = this.listEtablissement[index];
			ebContactRequest.ebEtablissementDestinataire = etablisement;
			ebContactRequest.status = StatusDemande.ENCOURS;
			if (etablisement.service == ServiceType.TRANSPORTEUR_BROKER && this.isChargeur)
				showModel = true;
			else ebContactRequest.typeRelation = etablisement.service;
		}

		if (showModel) {
			this.typeInvitation = null;
			this.modalService.open(content).result.then(
				(result) => {
					if (result) {
						ebContactRequest.typeRelation = this.typeInvitation;
						this.sendInvite(ebContactRequest, index, event, requestProcessing);
					} else {
						requestProcessing.afterGetResponse(event);
					}
				},
				(reason) => {
					requestProcessing.afterGetResponse(event);
				}
			);
		} else {
			this.sendInvite(ebContactRequest, index, event, requestProcessing);
		}
	}

	sendInvite(
		ebContactRequest: EbContactRequest,
		index: number,
		event: any,
		requestProcessing: RequestProcessing
	) {
		// this.userService.sendInvitaion(ebContactRequest).subscribe((response) => {
		//   if (response) {
		//     if (this.showContact) {
		//       this.listUser.splice(index, 1)
		//     }
		//     else {
		//       this.listEtablissement.splice(index, 1)
		//     }
		//     requestProcessing.afterGetResponse(event);
		//   }
		// });
	}
}
