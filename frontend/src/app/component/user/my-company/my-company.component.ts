import { Component, OnInit } from "@angular/core";
import { EbUser } from "@app/classes/user";
import { EbEtablissement } from "@app/classes/etablissement";
import { UserService } from "@app/services/user.service";

@Component({
	selector: "app-my-company",
	templateUrl: "./my-company.component.html",
	styleUrls: ["./my-company.component.css"],
})
export class MyCompanyComponent implements OnInit {
	newUser: EbUser = new EbUser();
	newEtablissement: EbEtablissement = this.newUser.ebEtablissement;

	constructor(private userService: UserService) {}

	getUserEtablissement() {
		//this.newUser.ebUserNum = 1;
		this.userService.userInfo().subscribe((rep) => {
			this.newUser = rep;
			console.log(this.newUser);
		});
	}

	updateEtablissement() {
		//this.newUser.ebUserNum = 1;
		//this.newUser.ebEtablissement = this.newEtablissement
		this.userService.updateEtablissement(this.newUser.ebEtablissement).subscribe((rep) => {
			this.newEtablissement = rep;
			console.log(this.newEtablissement);
		});
	}

	ngOnInit() {
		this.newUser = new EbUser();
		this.newEtablissement = new EbEtablissement();
		this.getUserEtablissement();
	}
}
