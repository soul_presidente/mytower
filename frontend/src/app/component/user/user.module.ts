import { TypeDocumentComponent } from "./../type-document/type-document.component";
import { NgModule } from "@angular/core";
import { UserRoutes } from "./user.route";
import { SharedModule } from "@app/shared/module/shared.module";
import { UserComponent } from "./user.component";
import { MyCommunityComponent } from "./my-community/my-community.component";
import { MyCompanyComponent } from "./my-company/my-company.component";
import { AccountSettingsComponent } from "./account-settings/account-settings.component";
import { MyContactComponent } from "./my-community/my-contact/my-contact.component";
import { ContactRequestComponent } from "./my-community/contact-request/contact-request.component";
import { NewContactComponent } from "./my-community/new-contact/new-contact.component";
import { SettingsComponent } from "./settings/settings.component";
import { _MyAccountComponent } from "./settings/my-account/my-account.component";
import { _MyCommunityComponent } from "./settings/my-community/my-community.component";
import { _MyCompanyComponent } from "./settings/my-company/my-company.component";
import { AdressesComponent } from "./settings/my-company/adresses/adresses.component";
import { CompanyProfileComponent } from "./settings/my-company/company-profile/company-profile.component";
import { CostCentersComponent } from "./settings/my-company/cost-centers/cost-centers.component";
import { CategorieComponent } from "./settings/my-company/categorie/categorie.component";
import { DataTablesModule } from "angular-datatables";
import { CustomFieldsComponent } from "./settings/my-company/custom-fields/custom-fields.component";
import { UsersManagementComponent } from "./settings/my-company/users-management/users-management.component";
import { UserDetailsComponent } from "./settings/my-company/users-management/user-details/user-details.component";
import { EtablissementRequestComponent } from "./settings/my-company/etablissement-request/etablissement-request.component";
import { CurrencyEtablissementComponent } from "./settings/my-company/currency-etablissement/currency-etablissement.component";
import { ZoneComponent } from "./settings/my-company/zone/zone.component";
import { TranchesComponent } from "./settings/my-company/tranches/tranches.component";
import { PlanTransportComponent } from "./settings/my-company/plan-transport/plan-transport.component";
import { GrilleTransportComponent } from "./settings/my-company/grille-transport/grille-transport.component";
import { TableModule } from "primeng/table";
import { PanelModule } from "primeng/panel";
import { PaginatorModule } from "primeng/paginator";
import { BoolToYesNoPipe } from "@app/shared/pipes/boolToYesNo.pipe";
import { ExtractorsComponent } from "./settings/extractors/extractors.component";
import { PricingService } from "@app/services/pricing.service";

import { ThemesSettingComponent } from "./settings/my-company/themes-setting/themes-setting.component";
import { ToastModule } from "primeng/toast";
import { CardModule } from "primeng/card";
import { RadioButtonModule } from "primeng/radiobutton";
import { SelectButtonModule } from "primeng/selectbutton";
import { ThemesConfigService } from "@app/services/themes-config.service";
import { ExportService } from "@app/services/export.service";
import { InputSwitchModule } from "primeng/inputswitch";
import { ButtonModule } from "primeng/button";
import { DockManagementSettingsComponent } from "./settings/my-company/dock-management/dock-management.component";
import { CalendarModule } from "primeng/calendar";
import { EntrepotManagementComponent } from "./settings/my-company/dock-management/entrepot-management/entrepot-management.component";
import { AutoCompleteModule } from "primeng/autocomplete";
import { ShipToMatrixComponent } from "./settings/my-company/ship-to-matrix/ship-to-matrix.component";
import { VehiculeComponent } from "./settings/my-company/vehicule/vehicule.component";
import { VehiculeService } from "@app/services/vehicule.service";
import { TypeUnitsComponent } from "./settings/my-company/type-units/type-units.component";
import { FlagManagementComponent } from "./settings/my-company/flag-management/flag-management.component";
import { RuleComponent } from "./settings/my-company/rule/rule.component";
import { DeviationSettingsComponent } from "./settings/my-company/deviation-settings/deviation-settings.component";
import { DeviationSettingsCheckboxComponent } from "./settings/my-company/deviation-settings/deviation-settings-checkbox/deviation-settings-checkbox.component";
import {
	AccordionModule,
	FileUploadModule,
	DialogModule,
	TabMenuModule,
	TreeTableModule,
} from "primeng/primeng";
import { OpeningDayComponent } from "./settings/my-company/dock-management/entrepot-content-settings/opening-weeks/opening-day/opening-day.component";
import { EntrepotContentSettingsComponent } from "./settings/my-company/dock-management/entrepot-content-settings/entrepot-content-settings.component";
import { OpeningWeeksComponent } from "./settings/my-company/dock-management/entrepot-content-settings/opening-weeks/opening-weeks.component";
import { OpeningPeriodsComponent } from "./settings/my-company/dock-management/entrepot-content-settings/opening-weeks/opening-periods/opening-periods.component";
import { OpeningPeriodsCalendarComponent } from "./settings/my-company/dock-management/entrepot-content-settings/opening-periods-calendar/opening-periods-calendar.component";
import { ListDockComponent } from "./settings/my-company/dock-management/entrepot-content-settings/list-dock/list-dock.component";
import { ConfigPslComponent } from "./settings/my-company/config-psl/config-psl.component";
import { DocumentTemplatesComponent } from "./settings/my-company/document-templates/document-templates.component";
import { ProfilesManagementComponent } from "./settings/my-company/profiles-management/profiles-management.component";
import { ProfileDetailComponent } from "@app/component/user/settings/my-company/profiles-management/profile-detail/profile-detail.component";
import { TypeRequestComponent } from "./settings/my-company/type-request/type-request.component";
import { TabViewModule } from "primeng/tabview";
import { MyAlertComponent } from "./settings/my-alert/my-alert.component";
import { BoutonActionComponent } from "./settings/my-company/bouton-action/bouton-action.component";
import { CompanySettingsComponent } from "./settings/my-company/company-settings/company-settings.component";
import { UserSettingsComponent } from "./settings/my-company/user-settings/user-settings.component";
import { GlobalTransportionPlanComponent } from "./settings/my-company/global-transportion-plan/global-transportion-plan.component";
import { LexiqueControlRulesMailComponent } from "@app/component/user/settings/lexique/lexique-mail-control-rules/lexique-mail-control-rules.component";
import { CategorieLabelComponent } from "./settings/my-company/categorie-label/categorie-label.component";
import { TooltipModule } from "primeng/tooltip";
import { AccessRightComponent } from "./settings/my-company/access-right/access-right.component";
import { DataRecoveryComponent } from "./settings/my-company/data-recovery/data-recovery.component";
import { RegimesTemporairesComponent } from "./settings/my-company/regimes-temporaires/regimes-temporaires.component";
import { CustomSettingComponent } from "./settings/my-company/custom-setting/custom-setting.component";
import { ConfigSchemaPslComponent } from "./settings/my-company/config-schema-psl/config-schema-psl.component";
import { ConfigQrGroupeComponent } from "@app/shared/config-qr-groupe/config-qr-groupe.component";
import { TracingCronjobMonitoringComponent } from "./settings/my-company/tracing-cronjob-monitoring/tracing-cronjob-monitoring.component";
import { TypeGoodsComponent } from "./settings/my-company/type-goods/type-goods.component";
import { ConfigTypeFluxComponent } from "./settings/my-company/config-type-flux/config-type-flux.component";
import { ConfigIncotermComponent } from "./settings/my-company/config-incoterm/config-incoterm.component";
import { AclRightsComponent } from "./settings/my-company/acl-rights/acl-rights.component";
import { DataViewModule } from "primeng/dataview";
import { MyCommunityUserDetailsComponent } from "./settings/my-company/users-management/user-details/user-details-community/user-details-community.component";
import { TransfertEtablissementComponent } from "./settings/my-company/transfert-etablissement/transfert-etablissement.component";
import { AffectCategoriesComponent } from "./settings/my-company/transfert-etablissement/affect-categories/affect-categories.component";
import { AffectCommunityComponent } from "./settings/my-company/transfert-etablissement/affect-community/affect-community.component";
import { AffectOrdersComponent } from "./settings/my-company/transfert-etablissement/affect-orders/affect-orders.component";
import { AffectTransportsComponent } from "./settings/my-company/transfert-etablissement/affect-transports/affect-transports.component";
import { ParametrageMtcComponent } from "@app/component/user/settings/my-company/parametrage-mtc/parametrage-mtc.component";
import { MappingCodePslComponent } from "./settings/my-company/mapping-code-psl/mapping-code-psl.component";
import { MappingCodePslSearchComponent } from "./settings/my-company/mapping-code-psl-search/mapping-code-psl-search.component";
import { MappingCodePslDetailsComponent } from "./settings/my-company/mapping-code-psl-details/mapping-code-psl-details.component";
import { ControlRulesComponent } from "./settings/control-rules/control-rules.component";
import { DateControlRulesFormComponent } from "./settings/control-rules/date-control-rules-form/date-control-rules-form.component";
import { ControlRuleService } from "@app/services/control-rule.service";
import { TemplateExportDocComponent } from "./settings/template-export-doc/template-export-doc.component";
import { MarkdownModule } from "ngx-markdown";
import { ModalDetailCustomFieldsComponent } from "./settings/my-company/custom-fields/modal-detail-custom-fields/modal-detail-custom-fields.component";
import { ConfigCostsComponent } from './settings/my-company/config-costs/config-costs.component';
import { CostsItemsComponent } from './settings/my-company/costs-items/costs-items.component';
import {MappingAttributeComponent} from "@app/component/user/settings/my-company/parametrage-mtc/mapping-attribute/mapping-attribute.component";

@NgModule({
	imports: [
		UserRoutes,
		SharedModule,
		DataTablesModule,
		TableModule,
		PanelModule,
		PaginatorModule,
		ToastModule,
		CardModule,
		RadioButtonModule,
		SelectButtonModule,
		InputSwitchModule,
		ButtonModule,
		CalendarModule,
		AutoCompleteModule,
		AccordionModule,
		FileUploadModule,
		TabMenuModule,
		TreeTableModule,
		TabViewModule,
		TooltipModule,
		DialogModule,
		DataViewModule,
		MarkdownModule.forChild(),
	],
	declarations: [
		UserComponent,
		MyCompanyComponent,
		AccountSettingsComponent,
		MyCommunityComponent,
		MyContactComponent,
		ContactRequestComponent,
		NewContactComponent,
		SettingsComponent,
		_MyAccountComponent,
		_MyCommunityComponent,
		CompanyProfileComponent,
		_MyCompanyComponent,
		CostCentersComponent,
		AdressesComponent,
		CategorieComponent,
		CustomFieldsComponent,
		UsersManagementComponent,
		UserDetailsComponent,
		EtablissementRequestComponent,
		CurrencyEtablissementComponent,
		RegimesTemporairesComponent,
		ZoneComponent,
		TranchesComponent,
		PlanTransportComponent,
		GrilleTransportComponent,
		TypeDocumentComponent,
		ThemesSettingComponent,
		OpeningDayComponent,
		DockManagementSettingsComponent,
		EntrepotContentSettingsComponent,
		OpeningWeeksComponent,
		OpeningPeriodsComponent,
		ListDockComponent,
		EntrepotManagementComponent,
		OpeningPeriodsCalendarComponent,
		ShipToMatrixComponent,
		VehiculeComponent,
		TypeUnitsComponent,
		FlagManagementComponent,
		RuleComponent,
		DeviationSettingsComponent,
		DeviationSettingsCheckboxComponent,
		ExtractorsComponent,
		ConfigPslComponent,
		ConfigSchemaPslComponent,
		ConfigTypeFluxComponent,
		ConfigIncotermComponent,
		ConfigQrGroupeComponent,
		DocumentTemplatesComponent,
		ProfilesManagementComponent,
		ProfileDetailComponent,
		TypeRequestComponent,
		ParametrageMtcComponent,
		MyAlertComponent,
		BoutonActionComponent,
		CompanySettingsComponent,
		UserSettingsComponent,
		GlobalTransportionPlanComponent,
		LexiqueControlRulesMailComponent,
		CategorieLabelComponent,
		AccessRightComponent,
		DataRecoveryComponent,
		CustomSettingComponent,
		TracingCronjobMonitoringComponent,
		TypeGoodsComponent,
		AclRightsComponent,
		MyCommunityUserDetailsComponent,
		TransfertEtablissementComponent,
		AffectCategoriesComponent,
		AffectCommunityComponent,
		AffectOrdersComponent,
		AffectTransportsComponent,
		MappingCodePslComponent,
		MappingCodePslSearchComponent,
		MappingCodePslDetailsComponent,
		ControlRulesComponent,
		DateControlRulesFormComponent,
		TemplateExportDocComponent,
		ModalDetailCustomFieldsComponent,
		ConfigCostsComponent,
		CostsItemsComponent,
		MappingAttributeComponent
	],
	entryComponents: [DeviationSettingsCheckboxComponent],
	exports: [
		ThemesSettingComponent,
		FileUploadModule,
		AffectCategoriesComponent,
		AffectCommunityComponent,
		AffectOrdersComponent,
		AffectTransportsComponent,
	],
	providers: [
		BoolToYesNoPipe,
		ThemesConfigService,
		ExportService,
		VehiculeService,
		PricingService,
		ControlRuleService,
		BoutonActionComponent,
	],
})
export class UserModule {}
