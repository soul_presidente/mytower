import { Component, OnInit, ViewChild } from "@angular/core";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { DataTableDirective } from "angular-datatables";

@Component({
	selector: "dock-management.component",
	templateUrl: "./dock-management.component.html",
	styleUrls: ["./dock-management.component.css"],
})
export class dockManagement implements OnInit {
	dataTableConfig: DataTableConfig = new DataTableConfig();
	@ViewChild(DataTableDirective, { static: false })
	dtElement: DataTableDirective;
	ngOnInit() {}
}
