import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { dockManagementRoutes } from "./dock-management.routes";
import { dockManagement } from "./dock-management.component";
import { SharedModule } from "@app/shared/module/shared.module";
import { DataTablesModule } from "angular-datatables";

@NgModule({
	imports: [CommonModule, dockManagementRoutes, SharedModule, DataTablesModule],
	declarations: [dockManagement],
	exports: [dockManagement],
	providers: [],
})
export class dockManagementModule {}
