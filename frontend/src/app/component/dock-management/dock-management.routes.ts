import { Route, RouterModule } from "@angular/router";
import { dockManagement } from "./dock-management.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";

export const dock_management_ROUTES: Route[] = [
	{
		path: "details",
		component: dockManagement,
		canActivate: [UserAuthGuardService],
	},
];

export const dockManagementRoutes = RouterModule.forChild(dock_management_ROUTES);
