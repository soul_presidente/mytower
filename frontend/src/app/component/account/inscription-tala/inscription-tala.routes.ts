import { Route, RouterModule } from "@angular/router";
import { InscriptionTalaComponent } from "./inscription-tala.component";

export const INSCRIPTION_TALA_ROUTES: Route[] = [
	{
		path: "",
		component: InscriptionTalaComponent,
	},
];

export const InscriptionTalaRoutes = RouterModule.forChild(INSCRIPTION_TALA_ROUTES);
