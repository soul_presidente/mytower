import { ButtonModule } from "primeng/button";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../../shared/module/shared.module";
import { DialogModule } from "primeng/dialog";
import { InscriptionTalaComponent } from "./inscription-tala.component";
import { InscriptionTalaRoutes } from "./inscription-tala.routes";

@NgModule({
	imports: [CommonModule, InscriptionTalaRoutes, SharedModule, DialogModule, ButtonModule],
	declarations: [InscriptionTalaComponent],
})
export class InscriptionTalaModule {}
