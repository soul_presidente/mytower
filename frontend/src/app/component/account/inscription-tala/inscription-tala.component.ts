import { Component } from "@angular/core";
import { InscriptionComponent } from "../inscription/inscription.component";

@Component({
	selector: "app-inscription-tala",
	templateUrl: "./inscription-tala.component.html",
	styleUrls: ["./inscription-tala.component.scss", "../account.component.scss"],
})
export class InscriptionTalaComponent extends InscriptionComponent {}
