import { Routes } from "@angular/router";
import { LoginRedirectService } from "@app/component/account/common/account-redirect/login-redirect.service";
import { LoginComponent } from "@app/component/account/login/login.component";
import { LoginChanelComponent } from "@app/component/account/login-chanel/login-chanel.component";
import { ResetPasswordComponent } from "@app/component/account/login/reset-password/reset-password.component";
import { ResetPasswordChanelComponent } from "@app/component/account/login-chanel/reset-password-chanel/reset-password-chanel.component";
import { LoginTalaComponent } from "./login-tala/login-tala.component";
import { ResetPasswordTalaComponent } from "./login-tala/reset-password-tala/reset-password-tala.component";

// prettier-ignore
export const accountRoutes: Routes = [
	// redirect paths
	{ path: "login", canActivate: [LoginRedirectService], component: LoginComponent },
	{
		path: "inscription",
		loadChildren: () => import('../../../app/component/account/inscription/inscription.module').then(m => m.InscriptionModule),
	},

	// inscription paths
	{
		path: "inscription-chanel",
		loadChildren: () => import('../../../app/component/account/inscription-chanel/inscription-chanel.module').then(m => m.InscriptionChanelModule),
	},

	// inscription Tala
	{
		path: "inscription-tala",
		loadChildren: () => import('../../../app/component/account/inscription-tala/inscription-tala.module').then(m => m.InscriptionTalaModule),
	},

	// login paths
	{ path: "login-chanel", component: LoginChanelComponent },
	{ path: "login-tala", component: LoginTalaComponent },

	// reset password paths
	{ path: "reset-password", component: ResetPasswordComponent },
	{ path: "reset-password-chanel", component: ResetPasswordChanelComponent },
	{ path: "reset-password-tala", component: ResetPasswordTalaComponent },
];
