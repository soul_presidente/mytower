import { Injectable } from "@angular/core";
import {
	ActivatedRouteSnapshot,
	CanActivate,
	Router,
	RouterStateSnapshot,
} from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Statique } from "@app/utils/statique";
import { catchError, map } from "rxjs/internal/operators";

@Injectable()
export class LoginRedirectService implements CanActivate {
	constructor(private http: HttpClient, protected router: Router) {}

	canActivate(): Observable<boolean> {
		return this.getLoginPageUrl().pipe(
			map((page) => {
				// if the page is generic show it
				if (page.login === "generic") return true;
				// else show the login page from config
				this.router.navigateByUrl("/login-" + page.login);
				return false;
			}),
			catchError((err) => {
				console.log("error while fetching login page from server: ", err);
				// if we couldn't fetch login page url from server show the default one
				return of(true);
			})
		);
	}

	getLoginPageUrl(): Observable<any> {
		return this.http.post(Statique.controllerAccount + "/get-login-page-url", {});
	}
}
