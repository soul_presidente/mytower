import { InscriptionService } from "../../../services/inscription.service";
import { Component, OnInit, ViewContainerRef, Output, EventEmitter } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { AuthenticationService } from "../../../services/authentication.service";
import { UserService } from "../../../services/user.service";
import { EbUser } from "../../../classes/user";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "../../../shared/modal/modal.service";
import { Statique } from "../../../utils/statique";
import { ThemesConfigService } from "../../../services/themes-config.service";
import { SearchCriteriaTrackTrace } from "../../../utils/SearchCriteriaTrackTrace";
import { EbTracing } from "../../../classes/tracing";
import { TrackTraceService } from "../../../services/trackTrace.service";
import { RequestProcessing } from "../../../utils/requestProcessing";
import SingletonStatique from "@app/utils/SingletonStatique";

@Component({
	selector: "app-login-common",
	template: "",
})
export class LoginCommonComponent implements OnInit {
	model: any = {};
	error = "";
	successMessage = "";
	newUser: EbUser;
	Statique = Statique;
	smartTracking: boolean = false;
	smartTrackSearch: boolean = false;
	searchCriteria: SearchCriteriaTrackTrace = new SearchCriteriaTrackTrace();
	tracing: EbTracing = new EbTracing();
	listTrack: Array<EbTracing> = new Array<EbTracing>();
	isRememberMeChecked: boolean = true;

	constructor(
		private router: Router,
		private authHttp: HttpClient,
		private authenticationService: AuthenticationService,
		private inscriptionService: InscriptionService,
		private activatedRoute: ActivatedRoute,
		protected vcr?: ViewContainerRef,
		protected userService?: UserService,
		private translate?: TranslateService,
		protected modalService?: ModalService,
		private themesConfigService?: ThemesConfigService,
		private trackTraceService?: TrackTraceService
	) {}

	ngOnInit() {
		this.authenticationService.logout();
		this.Statique = Statique;
		localStorage.setItem("activeTheme", "");
		this.newUser = new EbUser();
		Statique.hideSideBar = true;

		let codeActivation = this.activatedRoute.snapshot.queryParams["codeActivation"];
		if (codeActivation) {
			let $this = this;
			this.error = null;
			this.userService.activationCompte(codeActivation).subscribe(
				(result) => {
					if (result) {
						this.successMessage = null;
						this.translate
							.get("INSCRIPTION.MY_TOWER_ACTIVATION_MESSAGE")
							.subscribe((res: string) => {
								this.successMessage = res;
							});
						this.modalService.information("Information", this.successMessage, () => {});
					} else {
						this.translate
							.get("INSCRIPTION.MY_TOWER_ACTIVATION_MESSAGE")
							.subscribe((res: string) => {
								this.successMessage = res;
							});
						this.translate.get("INSCRIPTION.UNKNOWN_ERROR").subscribe((str: string) => {
							this.error = str;
						});
					}
				},
				(error) => {
					this.translate.get("INSCRIPTION.UNKNOWN_ERROR").subscribe((str: string) => {
						this.error = str;
					});
				}
			);
		}
	}

	getCompanieTheme() {
		let ebUser = AuthenticationService.getConnectedUser();
		this.themesConfigService.getAPITheme(ebUser.ebCompagnie.ebCompagnieNum).subscribe((res) => {
			if (res) localStorage.setItem("activeTheme", res.activeTheme);
			window.location.href = window.location.origin + "/accueil";
		});
	}

	getUser() {
		this.userService.userInfo().subscribe((rep: EbUser) => {
			this.newUser = rep;
			window.localStorage.setItem("currentLang", this.newUser.language);
			this.translate.use(this.newUser.language);
		});
	}

	doLogin(e) {
		let self = this;
		if (!this.isRememberMeChecked) {
			localStorage.setItem("remember_me", "false");
		} else {
			localStorage.setItem("remember_me", "true");
		}
		e.preventDefault();
		this.authenticationService.login(this.model.username, this.model.password).subscribe(
			(result) => {
				if (result) {
					this.authenticationService.notifySideBarInitUser();
					self.getUser();
					self.getCompanieTheme();
				}
			},
			(error) => {
				this.translate.get("INSCRIPTION.AUTH_ERROR").subscribe((str: string) => {
					this.error = str;
				});
			}
		);
	}

	displaySmartTracking() {
		// Si la fonctionalité n'est plus à implementé on peut nettoyer l'existant
		// https://jira.adias.fr/browse/MTG-1055
		// this.smartTrackSearch = !this.smartTrackSearch;

		let modalTitle = "Smart Tracking";
		let modalBody =
			"This feature is not available to your company, Please contact your administrator for more information.";

		// translate labels
		this.translate.get("LOGIN.SMART_TRACKING").subscribe((title: string) => {
			modalTitle = title;
		});
		this.translate.get("LOGIN.SMART_TRACKING_MESSAGE").subscribe((body: string) => {
			modalBody = body;
		});

		this.modalService.information(modalTitle, modalBody);
	}

	getTracking(event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		if (this.searchCriteria.searchterm != null || this.searchCriteria.searchterm != "") {
			this.trackTraceService.getList(this.searchCriteria).subscribe(
				(data) => {
					this.listTrack = data;
					this.listTrack.forEach((tt) => {
						tt.listEbPslApp.sort((psl1, psl2) => psl1.ebTtPslAppNum - psl2.ebTtPslAppNum);
					});
					requestProcessing.afterGetResponse(event);
					if (this.listTrack.length == 0)
						this.modalService.information(
							"Information",
							"Aucune expédition trouvée pour cette référence",
							function() {}
						);
					else this.smartTracking = true;
				},
				(error) => {
					let url: string = "failedaccess";
					this.router.navigate([url, "41", "expedition"]);
					// this.modalService.information("Information", "Oups, An Error occured", function() {});
					requestProcessing.afterGetResponse(event);
				}
			);
		}
		if (this.searchCriteria.searchterm == null || this.searchCriteria.searchterm == "") {
			this.modalService.information(
				"Information",
				"Veuillez entrer une reférence dans le champ",
				function() {}
			);
			requestProcessing.afterGetResponse(event);
		}
	}

	displayPopUp(ebtracingNum: number) {
		this.tracing = this.listTrack.find((tt) => tt.ebTtTracingNum == ebtracingNum);
		this.tracing.listEbPslApp.sort((psl1, psl2) => psl1.ebTtPslAppNum - psl2.ebTtPslAppNum);
		this.smartTracking = true;
	}

	ngOnDestroy() {
		Statique.hideSideBar = false;
	}

	sanitizeUsername() {
		// https://jira.adias.fr/browse/CPB-120
		// Space added to the end of username prevented the user from logging in
		this.model.username = this.model.username
			.trim()
			.toLowerCase()
			.replace(" ", "_");
	}
}
