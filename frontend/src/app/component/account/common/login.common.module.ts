import { NgModule } from "@angular/core";
import { NgxUploaderModule } from "ngx-uploader";
import { LoginCommonComponent } from "@app/component/account/common/login.common.component";
import { ResetPasswordCommonComponent } from "@app/component/account/common/reset-password.common.component";
import { LoginRedirectService } from "@app/component/account/common/account-redirect/login-redirect.service";

@NgModule({
	imports: [NgxUploaderModule],
	declarations: [LoginCommonComponent, ResetPasswordCommonComponent],
	providers: [LoginRedirectService],
	exports: [LoginCommonComponent, ResetPasswordCommonComponent],
})
export class LoginCommonModule {}
