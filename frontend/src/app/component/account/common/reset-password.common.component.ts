import { Component, OnInit } from "@angular/core";
import { EbUser } from "../../../classes/user";
import { AuthenticationService } from "../../../services/authentication.service";
import { Router } from "@angular/router";
import { UserService } from "../../../services/user.service";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "../../../shared/modal/modal.service";
import { Statique } from "../../../utils/statique";
import { RequestProcessing } from "../../../utils/requestProcessing";

@Component({
	selector: "app-rest-password-common",
	template: "",
})
export class ResetPasswordCommonComponent implements OnInit {
	user = new EbUser();
	model: any = {};
	resetEmail = false;
	resetPassword = false;
	token = null;
	error = "";
	info = "";

	constructor(
		protected authenticationService: AuthenticationService,
		private router: Router,
		private userService: UserService,
		private translate: TranslateService,
		private modalService: ModalService
	) {}

	ngOnInit() {
		this.authenticationService.logout();
		this.router.routerState.root.queryParams.subscribe((params) => {
			if (params["token"]) {
				this.resetEmail = false;
				this.resetPassword = true;
				this.token = params["token"];
				//          this.user.resetToken = params['token'];
			} else {
				this.resetEmail = true;
				this.resetPassword = false;
			}
		});
		Statique.hideSideBar = true;
	}

	reset(event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.userService.resetPassword(this.model.email).subscribe((result) => {
			console.log(result);
			requestProcessing.afterGetResponse(event);

			if (result == 1) {
				this.error = null;
				this.translate.get("RESET_PASSWORD.MSG_RESETED_PASSWORD").subscribe((res: string) => {
					this.info = res;
				});
			} else if (result == -1) {
				this.info = null;
				this.translate.get("RESET_PASSWORD.MSG_EMAIL_NOT_FOUND").subscribe((res: string) => {
					this.info = res;
				});
			} else if (result == 2) {
				this.info = null;
				this.translate.get("RESET_PASSWORD.MSG_DISACTIVE").subscribe((res: string) => {
					this.info = res;
				});
			} else if (result == 3) {
				this.info = null;
				this.translate.get("RESET_PASSWORD.MSG_NON_ACTIF").subscribe((res: string) => {
					this.info = res;
				});
			}
		});
		requestProcessing.afterGetResponse(event);
	}

	changePassword(event: any) {
		//  this.user.email = this.model.email;
		let $this = this;
		if (this.model.password != this.model.password2) {
			this.info = null;
			this.error = "Pas identiques";
		} else {
			this.user.password = this.model.password;
			this.user.resetToken = this.token;
			let requestProcessing = new RequestProcessing();
			requestProcessing.beforeSendRequest(event);
			this.userService.changePassword(this.user).subscribe((result) => {
				requestProcessing.afterGetResponse(event);
				if (result) {
					this.error = null;
					this.translate.get("RESET_PASSWORD.MSG_UPDATED_PASSWSORD").subscribe((res: string) => {
						this.info = res;
					});
					this.modalService.information("information", this.info, function() {
						$this.router.navigate(["login"]);
					});
				} else {
					this.info = null;
					this.translate.get("RESET_PASSWORD.MSG_TOKEN_NOT_FOUND").subscribe((res: string) => {
						this.info = res;
					});
				}
			});
		}
	}

	ngOnDestroy() {
		Statique.hideSideBar = false;
	}
}
