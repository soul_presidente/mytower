import { Route, RouterModule } from "@angular/router";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";
import { InscriptionChanelComponent } from "./inscription-chanel.component";

export const INSCRIPTION_CHANEL_ROUTES: Route[] = [
	{
		path: "",
		component: InscriptionChanelComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const InscriptionChanelRoutes = RouterModule.forChild(INSCRIPTION_CHANEL_ROUTES);
