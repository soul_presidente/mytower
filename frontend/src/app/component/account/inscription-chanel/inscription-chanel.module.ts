import { ButtonModule } from "primeng/button";
import { InscriptionChanelRoutes } from "./inscription-chanel.routes";
import { InscriptionChanelComponent } from "./inscription-chanel.component";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SharedModule } from "../../../shared/module/shared.module";
import { DialogModule } from "primeng/dialog";

@NgModule({
	imports: [CommonModule, InscriptionChanelRoutes, SharedModule, DialogModule, ButtonModule],
	declarations: [InscriptionChanelComponent],
})
export class InscriptionChanelModule {}
