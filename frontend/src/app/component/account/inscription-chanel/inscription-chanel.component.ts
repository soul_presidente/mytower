import { Router } from "@angular/router";
import { InscriptionService } from "./../../../services/inscription.service";
import { ChanelService } from "./../../../services/chanel.service";
import { Component, OnInit } from "@angular/core";
import { Statique } from "../../../utils/statique";
import { EbUserChanel } from "../../../classes/user-chanel";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-inscription-chanel",
	templateUrl: "./inscription-chanel.component.html",
	styleUrls: ["./inscription-chanel.component.scss", "../account.component.scss"],
})
export class InscriptionChanelComponent implements OnInit {
	newUser: EbUserChanel = new EbUserChanel();
	accepte: boolean = false;
	result: boolean = false;
	display: boolean = false;
	message: string = "";
	title: string = "";
	usernameError: string = "";
	isSuccess: boolean = false;
	successMessage: string = "";

	constructor(
		private chanelService: ChanelService,
		private translate: TranslateService,
		private inscriptionService: InscriptionService,
		private router: Router
	) {}

	ngOnInit() {
		Statique.hideSideBar = true;
	}

	register() {
		this.chanelService.registerUser(this.newUser).subscribe(
			(res) => {
				if (res == "Success") {
					this.title = "Registration success";
					this.message = "User registered successfully.";
					this.isSuccess = true;
				} else {
					this.title = "Registration failed";
					this.message = "User registration failed. Check your Ship To number.";
					this.isSuccess = false;
				}
				this.display = true;
			},
			(error) => {
				this.title = "Registration failed";
				this.message = "User registration failed.";
				this.isSuccess = false;
				this.display = true;
			}
		);
	}

	affecteShiptoToUserName() {
		this.newUser.username = this.newUser.shipTo;
	}

	checkUsername(username: string) {
		this.usernameError = null;
		this.inscriptionService.checkUsername(username).subscribe(
			(data) => {
				if (data) {
					this.usernameError = null;
					this.translate.get("INSCRIPTION.USERNAME_EXISTANT").subscribe((res: string) => {
						this.usernameError = res;
					});
				}
			},
			(error) => {
				console.log("Nom d'utilisateur nexiste pas!");
				this.successMessage = null;
			}
		);
	}

	onOKRegistration() {
		if (this.isSuccess) {
			this.router.navigate(["/login"]);
		}
		this.display = false;
	}

	sanitizeUsername() {
		// https://jira.adias.fr/browse/CPB-120
		// Space added to the end of username prevented the user from logging in
		this.newUser.username = this.newUser.username
			.trim()
			.toLowerCase()
			.replace(" ", "_");
	}
}
