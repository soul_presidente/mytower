import { Component, OnInit } from "@angular/core";
import { LoginCommonComponent } from "@app/component/account/common/login.common.component";

@Component({
	selector: "app-login-chanel",
	templateUrl: "./login-chanel.component.html",
	styleUrls: ["./login-chanel.component.scss", "../account.component.scss"],
})
export class LoginChanelComponent extends LoginCommonComponent implements OnInit {}
