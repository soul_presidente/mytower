import { Component } from "@angular/core";
import { ResetPasswordCommonComponent } from "@app/component/account/common/reset-password.common.component";

@Component({
	selector: "app-reset-password-chanel",
	templateUrl: "./reset-password-chanel.component.html",
	styleUrls: ["../../account.component.scss", "./reset-password-chanel.component.scss"],
})
export class ResetPasswordChanelComponent extends ResetPasswordCommonComponent {}
