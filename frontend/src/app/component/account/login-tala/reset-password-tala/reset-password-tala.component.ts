import { Component } from "@angular/core";
import { ResetPasswordCommonComponent } from "@app/component/account/common/reset-password.common.component";

@Component({
	selector: "app-reset-password-tala",
	templateUrl: "./reset-password-tala.component.html",
	styleUrls: ["../../account.component.scss", "./reset-password-tala.component.scss"],
})
export class ResetPasswordTalaComponent extends ResetPasswordCommonComponent {}
