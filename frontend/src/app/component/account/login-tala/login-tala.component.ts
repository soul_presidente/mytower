import { Component, OnInit } from "@angular/core";
import { LoginCommonComponent } from "@app/component/account/common/login.common.component";

@Component({
	selector: "app-login-tala",
	templateUrl: "./login-tala.component.html",
	styleUrls: ["./login-tala.component.scss", "../account.component.scss"],
})
export class LoginTalaComponent extends LoginCommonComponent implements OnInit {}
