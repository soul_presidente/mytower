import { NgModule } from "@angular/core";
import { InscriptionRoutes } from "./inscription.route";
import { InscriptionComponent } from "./inscription.component";
import { NgxUploaderModule } from "ngx-uploader";
import { SharedModule } from "@app/shared/module/shared.module";

@NgModule({
	imports: [InscriptionRoutes, SharedModule, NgxUploaderModule],
	declarations: [InscriptionComponent],
	exports: [],
})
export class InscriptionModule {}
