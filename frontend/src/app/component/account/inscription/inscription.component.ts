import { RoleSociete, ServiceType, UserRole } from "@app/utils/enumeration";

import { Input } from "@app/utils/Input";
import { ActivatedRoute, Router } from "@angular/router";
import { DomSanitizer } from "@angular/platform-browser";
import { Component, ElementRef, OnInit, ViewContainerRef } from "@angular/core";
import { UploadOutput } from "ngx-uploader";
import { UploadComponent } from "@app/utils/uploadComponent";
import { EbUser } from "@app/classes/user";
import { EcCountry } from "@app/classes/country";
import { Statique } from "@app/utils/statique";
import { InscriptionService } from "@app/services/inscription.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { Inscription } from "@app/classes/inscription";
import { EbCompagnie } from "@app/classes/compagnie";
import { ModalService } from "@app/shared/modal/modal.service";
import { AuthenticationService } from "@app/services/authentication.service";
import { TranslateService } from "@ngx-translate/core";
import { UserService } from "@app/services/user.service";
import { EbEtablissement } from "@app/classes/etablissement";
import { StatiqueService } from "@app/services/statique.service";
import {EbUserDTO} from "@app/classes/dto/eb-user-dto";

@Component({
	selector: "app-inscription",
	templateUrl: "./inscription.component.html",
	styleUrls: ["./inscription.component.scss", "../account.component.scss"],
})
export class InscriptionComponent extends UploadComponent implements OnInit {
	//newUser: EbUser = new EbUser();
	newUser: EbUserDTO = new EbUserDTO()
	listCountry: Array<EcCountry> = new Array<EcCountry>();
	dataUpload: any;
	Statique: any;
	ebCompagnie: EbCompagnie = new EbCompagnie();
	ebEtablissement: EbEtablissement = new EbEtablissement();

	inputsFriendsEmail: Array<Input>;
	inputsColleguesEmail: Array<Input>;

	keys = [];
	userRoles = UserRole;

	accepte1: any;
	cacher: boolean = true;
	sirenExist: boolean = false;
	siretExist: boolean = false;
	error = "";
	successMessage = "";
	info = "";
	errorHeader = "";
	errorMessage = "";
	informationMessage = "";
	mailError = "";
	router: Router;
	canEnterStep1 = false;
	compagnieExist: boolean = false;
	compagnieExistMessage = "";
	nouvelInscription = "";
	canEnterStep3: boolean = false;

	onRoleChange() {
		this.cacher = this.newUser.role == this.userRoles.CHARGEUR ||  this.newUser.role == this.userRoles.CONTROL_TOWER;
	}

	constructor(
		protected authenticationService: AuthenticationService,
		private inscriptionService: InscriptionService,
		private myElement: ElementRef,
		protected activatedRoute: ActivatedRoute,
		protected sanitizer: DomSanitizer,
		private route: Router,
		protected vcr?: ViewContainerRef,
		protected modalService?: ModalService,
		protected translate?: TranslateService,
		private userService?: UserService,
		private statiqueService?: StatiqueService
	) {
		super(sanitizer);
		this.componentObject = this;
		this.dataUpload = { typeFile: String(this.UploadDirectory.LOGO) };
		this.inputsFriendsEmail = new Array<Input>();
		this.inputsColleguesEmail = new Array<Input>();
		this.keys = Object.keys(this.userRoles).filter(Number);
		this.router = route;
	}

	ngOnInit() {
		this.newUser = new EbUserDTO()
		this.Statique = Statique;
		this.getCountries();
		let siret = this.activatedRoute.snapshot.queryParams["siret"];
		if (siret) {
			this.getEtablissementBySiret(siret);
		}
		Statique.hideSideBar = true;
	}

	ngAfterViewChecked(): void {
		this.resolvePaddingTop();
	}

	addInput() {
		let input: Input = new Input();
		input.id = this.inputsFriendsEmail.length + 1;
		this.inputsFriendsEmail.push(input);
	}

	removeInput(index: number) {
		this.inputsFriendsEmail.splice(index, 1);
		console.log(index);
	}

	addInputCollegue() {
		let input: Input = new Input();
		input.id = this.inputsColleguesEmail.length + 1;
		this.inputsColleguesEmail.push(input);
	}

	removeInputCollegue(index: number) {
		this.inputsColleguesEmail.splice(index, 1);
	}

	// -------------------------------------Inviter les amis et collegues sur la plateforme---------------------------------------------------------
	inviteFriendAndColleagues(event: any) {
		let $this = this;
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.newUser.ebEtablissement.siret =
			this.newUser.ebEtablissement.ebCompagnie.siren + this.newUser.ebEtablissement.nic;
		let inscription = new Inscription();
		inscription.ebUser = this.newUser;
		inscription.friendInvitationEmails = this.inputsFriendsEmail.map((input) => input.email);

		inscription.collegueInvitationEmails = this.inputsColleguesEmail.map((input) => input.email);

		this.inscriptionService.inscription(inscription).subscribe(
			(user: EbUser) => {
				if (this.compagnieExist) {
					this.compagnieExistMessage = null;
					this.translate.get("INSCRIPTION.COMPAGNIE_EXIST_MESSAGE").subscribe((res: string) => {
						this.compagnieExistMessage = res;
					});
					this.modalService.information("Information", this.compagnieExistMessage, function() {
						if (user != null) {
							$this.router.navigate(["login"]);
						}
					});
				} else {
					this.informationMessage = null;
					this.translate.get("INSCRIPTION.INFORMATION_MESSAGE").subscribe((res: string) => {
						this.informationMessage = res;
					});
					this.modalService.information(
						"Information",
						this.informationMessage,
						function() {
							if (user != null) {
								$this.router.navigate(["login"]);
							}
						}.bind(this)
					);
				}

				requestProcessing.afterGetResponse(event);
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
			}
		);
	}

	// -------------------------------------Remplissage de la liste des pays-------------------------------------------------------------------------
	getCountries() {
		this.statiqueService.getListPays().subscribe((data) => {
			this.listCountry = data;
		});
	}

	// -------------------------------------Pour pre-remplir les champs de l'ecran 2  de l'inscription------------------------------------------------

	onSirenChange() {
		if (
			this.newUser.ebEtablissement.ebCompagnie != null &&
			this.newUser.ebEtablissement.ebCompagnie.siren != "" &&
			typeof this.newUser.ebEtablissement.ebCompagnie.siren != "undefined"
		) {
			this.getCompagnieBySiren(this.newUser.ebEtablissement.ebCompagnie.siren);
		}
	}

	onSiretChange() {
		if (
			this.newUser.ebEtablissement != null &&
			this.newUser.ebEtablissement.nic != "" &&
			this.newUser.ebEtablissement.nic != null &&
			typeof this.newUser.ebEtablissement.nic != "undefined"
		) {
			this.getEtablissementBySiret(
				this.newUser.ebEtablissement.ebCompagnie.siren + this.newUser.ebEtablissement.nic
			);
		}
	}

	// -------------------------------------Trouver une sociéte par SIREN------------------------------------------------

	private getCompagnieBySiren(siren: string) {
		this.sirenExist = true;
		this.inscriptionService.getCompagnieBySiren(siren).subscribe(
			(data) => {
				if (JSON.stringify(data) === "{}") {
					this.newUser.ebEtablissement = new EbEtablissement();
					this.newUser.ebEtablissement.ebCompagnie = new EbCompagnie();
					this.newUser.ebEtablissement.ebCompagnie.siren = siren;
					this.compagnieExist = false;
				} else {
					this.newUser.ebEtablissement.ebCompagnie = data as any;
					this.compagnieExist = true;
				}
			},
			(error) => {}
		);
	}

	// -------------------------------------Trouver un etablissement par son siret------------------------------------------------

	getEtablissementBySiret(siret: string) {
		this.siretExist = true;
		this.inscriptionService.getEtablissementBySiret(siret).subscribe(
			(data) => {
				if (JSON.stringify(data) === "{}") {
					let compagnie = this.newUser.ebEtablissement.ebCompagnie;
					let nic = this.newUser.ebEtablissement.nic;
					this.newUser.ebEtablissement = new EbEtablissement();
					this.newUser.ebEtablissement.ebCompagnie.constructorCopy(compagnie);
					this.newUser.ebEtablissement.nic = nic;
				} else {
					this.newUser.ebEtablissement = data;
				}
			},
			(error) => {}
		);
	}

	// -----------------------------------------------charger des images--------------------------------------------------------------------

	onUploadImage(output: UploadOutput): void {
		this.isGallery = false;
		this.onUploadOutput(output);
	}

	// -------------------------------------Afficher le pays de l'etablissement s'il existe------------------------------------------------

	byId(country1: EcCountry, country2: EcCountry) {
		if (country2) {
			return country1.ecCountryNum == country2.ecCountryNum;
		}
		return false;
	}

	// -------------------------------------Verifier si l'email de l'utilisateur existe ou pas------------------------------------------------
	checkMail(email: string) {
		this.mailError = null;
		this.inscriptionService.checkEmail(email).subscribe(
			(data) => {
				if (data) {
					this.mailError = null;
					this.translate.get("INSCRIPTION.EMAIL_EXISTANT").subscribe((res: string) => {
						this.mailError = res;
					});
				}
			},
			(error) => {
				console.log("Email nexiste pas!");
				this.successMessage = null;
			}
		);
	}

	// -----------------------------------------------Persister l'utilisateur-----------------------------------------------------
	save(event: any) {
		let $this = this;
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequestBtn(event);
		this.newUser.ebEtablissement.siret =
			this.newUser.ebEtablissement.ebCompagnie.siren + this.newUser.ebEtablissement.nic;

		if (this.newUser.serviceTransporteur && !this.newUser.serviceBroker) {
			this.newUser.service = ServiceType.TRANSPORTEUR;
		} else if (this.newUser.serviceBroker && !this.newUser.serviceTransporteur) {
			this.newUser.service = ServiceType.BROKER;
		} else if (this.newUser.serviceTransporteur && this.newUser.serviceBroker) {
			this.newUser.service = ServiceType.TRANSPORTEUR_BROKER;
		}

		this.inscriptionService.insciptionUser(this.newUser).subscribe(
			(user: EbUser) => {
				if (user.ebEtablissement != null) {
					this.dataUpload = {
						typeFile: String(this.UploadDirectory.LOGO),
						ebEtablissementNum: user.ebEtablissement.ebEtablissementNum.toString(),
					};
					this.startUpload();
				}
				if (user.ebUserNum != null) {
					this.canEnterStep1 = true;
					requestProcessing.afterGetResponseBtn(event);
				}
			},
			(error) => {
				this.canEnterStep1 = false;
				requestProcessing.afterGetResponseBtn(event);
				this.errorHeader = null;
				this.translate.get("INSCRIPTION.MODAL_ERREUR_HEADER").subscribe((res: string) => {
					this.errorHeader = res;
				});
				this.errorMessage = null;
				this.translate.get("INSCRIPTION.MODAL_ERREUR_MESSAGE").subscribe((res: string) => {
					this.errorMessage = res;
				});
				this.modalService.information(this.errorHeader, this.errorMessage, function() {
					$this.router.navigate(["login"]);
				});
			}
		);
	}


	createTranslateMessage(message : string){
		this.translate.get(message)
		.subscribe((res: string) => {
			return res;
		});
		return ""
	}

	compagnieAndUserRole() {
		if (this.compagnieExist) {
			if (this.newUser.role != this.newUser.ebEtablissement.ebCompagnie.compagnieRole) {
				this.canEnterStep3 = false;
				if (this.newUser.ebEtablissement.ebCompagnie.compagnieRole == RoleSociete.CHARGEUR) {
					if(this.newUser.role == UserRole.CONTROL_TOWER){
						this.canEnterStep3 = true;
					}
					else{
					this.modalService
					.information("Information", this.createTranslateMessage("INSCRIPTION.NOUVEL_INSCRIPTION_SHIPPER"), () => {});
					}
				} else if (
					this.newUser.ebEtablissement.ebCompagnie.compagnieRole == RoleSociete.PRESTATAIRE
				) {
					this.modalService
					.information("Information", this.createTranslateMessage("INSCRIPTION.NOUVEL_INSCRIPTION_CARRIER"), () => {});
				}
				else if(this.newUser.ebEtablissement.ebCompagnie.compagnieRole == RoleSociete.CONTROL_TOWER){
					this.modalService
					.information("Information", this.createTranslateMessage("INSCRIPTION.NOUVEL_INSCRIPTION_CONTROL_TOWER"), () => {});
				}

			} else if (this.newUser.role == this.newUser.ebEtablissement.ebCompagnie.compagnieRole) {
				this.canEnterStep3 = true;
			}
		} else if (!this.compagnieExist) {
			this.canEnterStep3 = true;
		}
	}

	retour() {
		this.canEnterStep3 = false;
	}

	resolvePaddingTop() {
		let elt = document.getElementById("corp");
		let eltClassList = elt.classList;
		if (eltClassList.contains("active-wrapper")) eltClassList.remove("active-wrapper");
	}

	ngOnDestroy() {
		Statique.hideSideBar = false;
	}
}
