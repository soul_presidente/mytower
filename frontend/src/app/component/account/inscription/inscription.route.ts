import { Route, RouterModule } from "@angular/router";
import { InscriptionComponent } from "./inscription.component";

export const INSCRIPTION_ROUTES: Route[] = [
	{
		path: "",
		component: InscriptionComponent,
	},
];

export const InscriptionRoutes = RouterModule.forChild(INSCRIPTION_ROUTES);
