import { Component } from "@angular/core";
import { ResetPasswordCommonComponent } from "@app/component/account/common/reset-password.common.component";

@Component({
	selector: "app-reset-password",
	templateUrl: "./reset-password.component.html",
	styleUrls: ["../../account.component.scss", "./reset-password.component.scss"],
})
export class ResetPasswordComponent extends ResetPasswordCommonComponent {}
