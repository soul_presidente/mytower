import { Component, OnInit } from "@angular/core";
import { LoginCommonComponent } from "@app/component/account/common/login.common.component";

@Component({
	selector: "app-login",
	templateUrl: "./login.component.html",
	styleUrls: ["./login.component.scss", "../account.component.scss"],
})
export class LoginComponent extends LoginCommonComponent implements OnInit {}
