import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { EbTracing } from "@app/classes/tracing";
import { Statique } from "@app/utils/statique";

@Component({
	selector: "app-smart-tracking",
	templateUrl: "./smart-tracking.component.html",
	styleUrls: ["./smart-tracking.component.scss"],
})
export class SmartTrackingComponent implements OnInit {
	@Input()
	tracing: EbTracing;

	@Input()
	listTracing: Array<EbTracing>;

	statique = Statique;

	@Output()
	closePopUp: EventEmitter<boolean> = new EventEmitter<boolean>();

	displaySubscribeInput: boolean = false;

	constructor() {}

	ngOnInit() {}

	close() {
		this.closePopUp.emit(false);
	}

	subscribeInput() {
		this.displaySubscribeInput = !this.displaySubscribeInput;
	}
}
