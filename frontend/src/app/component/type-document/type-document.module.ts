import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Ng2CompleterModule } from "ng2-completer";
import { NgSelectModule } from "@ng-select/ng-select";
import { SharedModule } from "../../shared/module/shared.module";
import { DataTablesModule } from "angular-datatables/src/angular-datatables.module";
import { NgxUploaderModule } from "ngx-uploader";
import { TypesDocumentService } from "../../services/types-document.service";
import { TypeDocumentRoutes } from "./type-document.routes";
import { ToastModule } from "primeng/toast";

@NgModule({
	imports: [
		CommonModule,
		DataTablesModule,
		NgSelectModule,
		SharedModule,
		Ng2CompleterModule,
		NgxUploaderModule,
		TypeDocumentRoutes,
		ToastModule,
	],
	declarations: [],
	providers: [TypesDocumentService],
})
export class TypeDocumentModule {}
