import { Component, OnInit, Output, EventEmitter, ViewChild } from "@angular/core";

import { TypesDocumentService } from "../../services/types-document.service";
import { EbTypeDocuments } from "../../classes/ebTypeDocuments";
import { Modules, GenericTableScreen } from "../../utils/enumeration";
import { SearchCriteria } from "../../utils/searchCriteria";
import { Statique } from "../../utils/statique";
import { RequestProcessing } from "../../utils/requestProcessing";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "../../shared/modal/modal.service";
import { HeaderInfos, BreadcumbComponent } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { StatiqueService } from "@app/services/statique.service";
import { EcModule } from "@app/classes/EcModule";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { AuthenticationService } from "@app/services/authentication.service";
import { MessageService } from "primeng/api";

@Component({
	selector: "app-type-document",
	templateUrl: "./type-document.component.html",
	styleUrls: ["./type-document.component.css"],
})
export class TypeDocumentComponent implements OnInit {
	Module = Modules;
	module: any;

	@Output("onDataChanged")
	onDataChanged = new EventEmitter<any>();

	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;
	editMode: boolean = false;
	isAddEditMode = false;
	document: EbTypeDocuments = new EbTypeDocuments();
	backupEditDocument: EbTypeDocuments;
	listModule: EcModule[] = [];
	searchCriteria: SearchCriteria = new SearchCriteria();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;
	modalConfirmDeleteVisibility: boolean = false;
	typeDocToDeleteNum: number;

	constructor(
		protected headerService?: HeaderService,
		protected authenticationService?: AuthenticationService,
		protected typesDocumentService?: TypesDocumentService,
		protected modalService?: ModalService,
		protected translate?: TranslateService,
		protected statiqueService?: StatiqueService,
		protected messageService?: MessageService
	) {}

	ngOnInit() {
		this.headerService.registerBreadcrumbItems(
			this.getBreadcrumbItems(),
			BreadcumbComponent.MY_COMPANY
		);
		this.statiqueService.getListModule().subscribe((data) => {
			this.listModule = data;
		});

		this.searchCriteria.withDeactive = true;
		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.size = 50;

		this.dataInfos.cols = [
			{
				field: "nom",
				translateCode: "REPOSITORY_MANAGEMENT.TYPES_DOCUMENT.NAME",
			},
			{
				field: "code",
				translateCode: "GENERAL.CODE",
			},
			{
				field: "ordre",
				translateCode: "REPOSITORY_MANAGEMENT.TYPES_DOCUMENT.ORDER",
			},
			{
				translateCode: "REPOSITORY_MANAGEMENT.TYPES_DOCUMENT.MODULES",
				field: "module",
				width: "150px",
				render: function(modulesNum: Array<number>) {
					let libelle = "";
					if (modulesNum != null) {
						modulesNum.forEach((element) => {
							libelle += this.translate.instant("MODULES." + this.getModuleById(element)) + ", ";
						});
					}
					return libelle.substring(0, libelle.length - 2);
				}.bind(this),
			},
			{
				field: "activated",
				translateCode: "GENERAL.ACTIVATED",
				render: (activated: boolean) => {
					return this.translate.instant("GENERAL." + (activated ? "YES" : "NO"));
				},
			},

			{
				field: "isexpectedDoc",
				translateCode: "GENERAL.EXPECTED",
				render: (expected: boolean) => {
					return this.translate.instant("GENERAL." + (expected ? "YES" : "NO"));
				},
			},
		];

		this.dataInfos.dataKey = "ebTypeDocumentsNum";
		this.dataInfos.dataType = EbTypeDocuments;
		this.dataInfos.showAddBtn = true;
		this.dataInfos.showEditBtn = true;
		this.dataInfos.showDeleteBtn = true;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = Statique.controllerTypeDocument + "/getListTypeDocTable";
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.selectedCols = this.dataInfos.cols;

		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "SETTINGS.MY_COMPANY",
				path: "/app/user/settings/company",
			},
			{
				label: "SETTINGS.DOCUMENT_TYPES",
			},
		];
	}
	getListModule() {
		this.statiqueService.getListModule().subscribe((data) => {
			this.listModule = data;

			// translate labels
			this.listModule.forEach((aModule) => {
				aModule.libelle = this.translate.instant("MODULES." + aModule.libelle);
			});
		});
	}

	getModuleById(moduleNum) {
		const aModule = this.listModule.filter((m) => {
			return m.ecModuleNum === moduleNum;
		});
		return aModule.length ? aModule[0].libelle : "";
	}

	openAddForm(isUpdate?: boolean) {
		if (isUpdate) {
			this.editMode = true;
		} else {
			this.document = new EbTypeDocuments();
			this.document.activated = true;
			this.document.isexpectedDoc = false;
			this.editMode = false;
		}
		this.isAddEditMode = true;
	}

	showDeleteTypeDocDialogConfirm(typeDoc: EbTypeDocuments) {
		this.typeDocToDeleteNum = typeDoc.ebTypeDocumentsNum;
		this.modalConfirmDeleteVisibility = true;
	}

	hideDeleteTypeDocDialogConfirm() {
		this.modalConfirmDeleteVisibility = false;
	}

	deleteTypeDoc(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.typesDocumentService.delete(this.typeDocToDeleteNum).subscribe(
			(data) => {
				requestProcessing.afterGetResponse(event);
				this.genericTable.refreshData();
				this.modalConfirmDeleteVisibility = false;
				this.successfullOperation();
				this.typeDocToDeleteNum = null;
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				this.modalConfirmDeleteVisibility = false;
				this.operationFailed();
				this.typeDocToDeleteNum = null;
			}
		);
	}

	openEditForm(ebdocument: EbTypeDocuments) {
		this.document = ebdocument;
		this.backupEditDocument = Statique.cloneObject<EbTypeDocuments>(
			this.document,
			new EbTypeDocuments()
		);
		this.openAddForm(true);
	}

	backToListDocuments() {
		Statique.cloneObject<EbTypeDocuments>(this.backupEditDocument, this.document);
		this.backupEditDocument = null;
		this.isAddEditMode = false;
	}
	editDocument(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.typesDocumentService.create(this.document).subscribe(
			(data) => {
				requestProcessing.afterGetResponse(event);
				this.isAddEditMode = false;
				this.backupEditDocument = null;
				this.genericTable.refreshData();
				this.successfullOperation();
			},
			(error) => {
				this.operationFailed();
				requestProcessing.afterGetResponse(event);
			}
		);
	}
	addDocument(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);

		this.typesDocumentService.create(this.document).subscribe(
			(data: EbTypeDocuments) => {
				requestProcessing.afterGetResponse(event);
				this.isAddEditMode = false;
				this.genericTable.refreshData();
				this.onDataChanged.next();
				this.successfullOperation();
			},
			(error) => {
				this.operationFailed();
				requestProcessing.afterGetResponse(event);
			}
		);
	}

	onDataLoaded(data: Array<EbTypeDocuments>) {
		this.onDataChanged.emit(data);
	}

	successfullOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	operationFailed() {
		this.messageService.add({
			severity: "error",
			life: 5000, //Number of time in milliseconds to wait before closing the message.
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL_DOC"),
		});
	}
}
