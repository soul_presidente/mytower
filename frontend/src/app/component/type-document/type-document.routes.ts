import { Route, RouterModule } from "@angular/router";
import { UserAuthGuardService } from "../../services/user-auth-guard.service";
import { TypeDocumentComponent } from "../../component/type-document/type-document.component";

export const TYPE_DOCUMENT_ROUTES: Route[] = [
	{
		path: "",
		component: TypeDocumentComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const TypeDocumentRoutes = RouterModule.forChild(TYPE_DOCUMENT_ROUTES);
