import { DeliveryOverviewModule } from "./delivery-overview.module";

describe("DeliveryOverviewModule", () => {
	let deliveryOverviewModule: DeliveryOverviewModule;

	beforeEach(() => {
		deliveryOverviewModule = new DeliveryOverviewModule();
	});

	it("should create an instance", () => {
		expect(deliveryOverviewModule).toBeTruthy();
	});
});
