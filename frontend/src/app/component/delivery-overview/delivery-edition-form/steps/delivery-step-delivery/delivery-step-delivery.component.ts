import { Component, OnInit, Input, EventEmitter, ViewChild, Output } from "@angular/core";
import { Delivery } from "@app/classes/ebDelLivraison";
import { EbUser } from "@app/classes/user";
import { EbParty } from "@app/classes/party";
import { Statique } from "@app/utils/statique";
import { AdresseForShippingDeliveryComponent } from "./adresse-for-shipping-delivery/adresse-for-shipping-delivery.component";
import { CreationFormStepBaseComponent } from "@app/shared/creation-form-step-base/creation-form-step-base.component";
import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbEtablissement } from "@app/classes/etablissement";
import { EtablissementService } from "@app/services/etablissement.service";

@Component({
	selector: "app-delivery-step-delivery",
	templateUrl: "./delivery-step-delivery.component.html",
	styleUrls: ["./delivery-step-delivery.component.scss"],
})
export class DeliveryStepDeliveryComponent extends CreationFormStepBaseComponent implements OnInit {
	@Input()
	delivery: Delivery;

	@Input()
	isModeModify: boolean = false;

	@Input()
	showAddressPopup: boolean = false;
	@Output()
	showAddressPopupChange = new EventEmitter<boolean>();

	ebPartyOrigin: EbParty = new EbParty();
	ebPartyDest: EbParty = new EbParty();
	ebPartySoldTo: EbParty = new EbParty();

	selectedUserNum: number;
	selectedUser: EbUser;
	typeaheadUserEvent: EventEmitter<string> = new EventEmitter<string>();
	listUserEtab: any = new Array<EbUser>();

	// Start : Le bazar du composant a rendre générique
	showAddressWrapperNotif: boolean;
	isAddress: boolean = true;
	fromOrToFocus: string;
	showAddressWrapperFrom: boolean;
	showAddAddressBtnFrom: boolean = false;
	showAddressWrapperTo: boolean;
	showAddAddressBtnTo: boolean;
	showAddressWrapperSale: boolean;
	showAddAddressBtnSale: boolean;
	listEtablissementPlateform: Array<EbEtablissement> = new Array<EbEtablissement>();
	suggestionsEtablissementList: Array<EbParty> = null;
	fromOrToAddressBloc: string;
	selectedIndexFrom: number = null;
	selectedIndexTo: number = null;
	selectedIndexSale: number = null;
	ebPartyAddAddress: EbParty = new EbParty();
	fromOrToAddAddress: any;
	// End : fin de ce bazar à rendre générique après release

	from: string = "From";
	to: string = "To";
	soldTo: string = "Sold to";
	@ViewChild(AdresseForShippingDeliveryComponent, { static: false })
	adresseForShipping: AdresseForShippingDeliveryComponent;
	unitInfos: any;

	@Output()
	suggestionsEtablissement: EventEmitter<Object> = new EventEmitter<Object>();

	@Output()
	hideAddressPopupEvent: EventEmitter<any> = new EventEmitter<any>();

	constructor(
		protected authenticationService: AuthenticationService,
		protected etablissementService: EtablissementService,
		protected router: Router
	) {
		super(authenticationService, router);
	}

	ngOnInit() {
		this.applyAddressWrappers();
		this.getEstablishments();
		if (this.delivery && this.delivery.ebDelLivraisonNum && this.delivery.xEbOwnerOfTheRequest) {
			this.listUserEtab = [this.delivery.xEbOwnerOfTheRequest];
			this.selectedUserNum = this.delivery.xEbOwnerOfTheRequest.ebUserNum;
		}
	}

	applyAddressWrappers() {
		if (
			Statique.getInnerObjectStr(this, "delivery.xEbPartyDestination.city") ||
			Statique.getInnerObjectStr(this, "delivery.xEbPartyDestination.xEcCountry.ecCountryNum")
		) {
			this.ebPartyDest = this.delivery.xEbPartyDestination;
		}

		if (
			Statique.getInnerObjectStr(this, "delivery.xEbPartyOrigin.city") ||
			Statique.getInnerObjectStr(this, "delivery.xEbPartyOrigin.xEcCountry.ecCountryNum")
		) {
			this.ebPartyOrigin = this.delivery.xEbPartyOrigin;
		}

		if (
			Statique.getInnerObjectStr(this, "delivery.xEbPartySale.city") ||
			Statique.getInnerObjectStr(this, "delivery.xEbPartySale.xEcCountry.ecCountryNum")
		) {
			this.ebPartySoldTo = this.delivery.xEbPartySale;
		}
	}

	checkValidity(): boolean {
		return true;
	}

	protected stepHasOptions(): boolean {
		return false;
	}

	getEstablishments() {
		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.getAll = true;
		searchCriteria.size = null;
		this.etablissementService
			.getListEtablisement(searchCriteria)
			.subscribe((data: Array<EbEtablissement>) => {
				this.listEtablissementPlateform = data;
			});
	}

	showAddressPopupHandle(event) {
		this.optionsDisplayed = false;
		this.showAddressPopup = true;
		this.isAddress = true;
		this.fromOrToFocus = event.fromOrTo;
		this.showAddressPopupChange.emit(this.showAddressPopup);
	}

	getOriginParty(data: any) {
		this.ebPartyOrigin = data;
		this.delivery.xEbPartyOrigin = this.ebPartyOrigin;
		this.showAddressWrapperFrom = true;
		this.showAddAddressBtnFrom = false;
	}

	getDestParty(data: any) {
		this.ebPartyDest = data;
		this.delivery.xEbPartyDestination = this.ebPartyDest;
		this.showAddressWrapperTo = true;
		this.showAddAddressBtnTo = false;
	}

	getSaleParty(data: any) {
		this.ebPartySoldTo = data;
		this.delivery.xEbPartySale = this.ebPartySoldTo;
		this.showAddAddressBtnSale = false;
		this.showAddressWrapperSale = true;
	}

	suggestionsEtablissementHandler(event: any) {
		this.suggestionsEtablissement.emit(event);
	}

	removeAddressOriginHandler(event: any) {
		this.selectedIndexFrom = null;
		this.ebPartyOrigin = new EbParty();
		this.delivery.xEbPartyOrigin = null;
		this.showAddressWrapperFrom = false;
		this.showAddAddressBtnFrom = true;
	}

	removeAddressDestHandler(event: any) {
		this.selectedIndexTo = null;
		this.ebPartyDest = new EbParty();
		this.delivery.xEbPartyDestination = null;
		this.showAddressWrapperTo = false;
		this.showAddAddressBtnTo = true;
	}
	removeAddressSaleHandler(event: any) {
		this.selectedIndexSale = null;
		this.ebPartySoldTo = new EbParty();
		this.delivery.xEbPartySale = null;
		this.showAddAddressBtnSale = false;
		this.showAddressWrapperSale = true;
	}

	addAddress(event) {
		let showAddressPopup, optionsDisplayed;
		if (this.optionsDisplayed == true) this.optionsDisplayed = false;
		this.showAddressPopup = true;
		this.isAddress = true;
		this.ebPartyAddAddress = event.ebParty;
		this.fromOrToAddAddress = event.fromOrTo;
		showAddressPopup = this.showAddressPopup;
		optionsDisplayed = this.optionsDisplayed;
		this.showAddressPopupChange.emit(showAddressPopup);
	}

	getPartyAdress(data: any) {
		if (this.fromOrToFocus == this.from) {
			this.ebPartyOrigin = data;
			this.delivery.xEbPartyOrigin = this.ebPartyOrigin;
			this.showAddressWrapperFrom = true;
			this.showAddAddressBtnFrom = false;
			this.getOriginParty(data);
		} else if (this.fromOrToFocus == this.to) {
			this.ebPartyDest = data;
			this.delivery.xEbPartyDestination = this.ebPartyDest;
			this.showAddressWrapperTo = true;
			this.showAddAddressBtnTo = false;
			this.getDestParty(data);
		} else if (this.fromOrToFocus == this.soldTo) {
			this.getSaleParty(data);
		}
		this.hideAddressPopupEvent.emit(true);
	}

	activateClassFrom(index: number) {
		this.selectedIndexFrom = index;
	}

	activateClassTo(index: number) {
		this.selectedIndexTo = index;
	}

	hideAddressPopup() {
		let showAddressPopup, optionsDisplayed;
		this.showAddressPopup = false;
		this.optionsDisplayed = false;
		showAddressPopup = this.showAddressPopup;
		optionsDisplayed = this.optionsDisplayed;
		this.showAddressPopupChange.emit(showAddressPopup);
	}
}
