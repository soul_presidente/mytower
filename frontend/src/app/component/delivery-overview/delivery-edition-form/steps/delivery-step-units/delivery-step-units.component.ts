import { Component, OnInit, Input, ViewChild, SimpleChanges, OnChanges } from "@angular/core";
import { SearchCriteriaDelivery } from "@app/utils/SearchCritereaDelivery";
import { Delivery } from "@app/classes/ebDelLivraison";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { GenericTableScreen } from "@app/utils/enumeration";
import { TextInputGenericCell } from "@app/shared/generic-cell/commons/text-input.generic-cell";
import { Statique } from "@app/utils/statique";
import { DeliveryLine } from "@app/classes/ebDelLivraisonLine";
import { DeliveryOverviewService } from "@app/services/delivery-overview.service";
import { ActivatedRoute } from "@angular/router";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { DeliveryPlanificationComponent } from "@app/component/delivery-overview/delivery-planification/delivery-planification.component";
import { SelectGenericCell } from "@app/shared/generic-cell/commons/select.generic-cell";
import { EcCurrency } from "@app/classes/currency";
import { StatiqueService } from "@app/services/statique.service";

@Component({
	selector: "app-delivery-step-units",
	templateUrl: "./delivery-step-units.component.html",
	styleUrls: ["./delivery-step-units.component.scss"],
})
export class DeliveryStepUnitsComponent extends ConnectedUserComponent
	implements OnInit, OnChanges {
	@Input()
	delivery: Delivery;
	deliveryLines: Array<DeliveryLine> = new Array<DeliveryLine>();

	@ViewChild("deliveryPlanificationComponent", { static: false })
	deliveryPlanificationComponent: DeliveryPlanificationComponent;

	@Input()
	isModeModify: boolean = false;

	searchCriteriaTable: SearchCriteriaDelivery = new SearchCriteriaDelivery();
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;
	genericTableReady = false;

	currencyList: Array<EcCurrency>;

	constructor(
		protected activatedRoute: ActivatedRoute,
		private deliveryOverviewService: DeliveryOverviewService,
		protected statiqueService: StatiqueService
	) {
		super();
	}

	ngOnInit() {}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["delivery"] && this.delivery) {
			if (this.delivery.ebDelLivraisonNum) {
				this.deliveryOverviewService
					.getDeliveryLineByDelivery(this.delivery.ebDelLivraisonNum)
					.subscribe((resultat) => {
						this.deliveryLines = resultat;
					});
			}

			this.initGenericTable();
		}
	}

	async initGenericTable() {
		await this.loadCurrencyList();
		this.initGenericTableCore();
		this.initGenericTableCols();
		this.genericTableReady = true;
	}

	async loadCurrencyList() {
		this.currencyList = await this.statiqueService.getListEcCurrency().toPromise();
	}

	initGenericTableCore() {
		this.searchCriteriaTable.pageNumber = 1;
		this.searchCriteriaTable.size = 5;
		this.searchCriteriaTable.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchCriteriaTable.connectedUserNum = this.userConnected.ebUserNum;
		if (this.delivery) {
			this.searchCriteriaTable.ebDelLivraisonNum = this.delivery.ebDelLivraisonNum;
		}
		this.dataInfos.dataKey = "ebDelLivraisonNum";
		this.dataInfos.dataSourceType = GenericTableInfos.DataSourceType.Url;
		this.dataInfos.dataType = DeliveryLine;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.showCollapsibleBtn = false;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
		if (this.searchCriteriaTable.ebDelLivraisonNum) {
			this.dataInfos.dataLink =
				Statique.controllerDeliveryOverView + "/get-delivery-lines-for-genericTable";
			this.dataInfos.cols = this.initGenericTableCols();
		}
	}

	addDeliveryLine() {
		let deliveryLine = new DeliveryLine();
		deliveryLine.quantity = 1;
		this.deliveryLines.push(deliveryLine);
	}

	initGenericTableCols() {
		let cols = new Array<GenericTableInfos.Col>();
		cols.push(
			{
				field: "itemNumber",
				translateCode: "ORDER.ITEM_NUMBER",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "itemNumber",
					showOnHover: true,
				}),
			},
			{
				field: "itemName",
				translateCode: "CHANEL_PB_DELIVERY.ARTICLE_NAME",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "itemName",
					showOnHover: true,
				}),
			}
		);

		cols.push({
			field: "quantity",
			translateCode: "ORDER.QUANTITY",
		});

		cols.push(
			{
				field: "partNumber",
				translateCode: "ORDER.PART_NUMBER",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "partNumber",
					showOnHover: true,
				}),
			},
			{
				field: "serialNumber",
				translateCode: "CHANEL_PB_DELIVERY.SERIAL_NUMBER",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "serialNumber",
					showOnHover: true,
				}),
			},
			{
				field: "price",
				translateCode: "CHANEL_PB_ORDER.PRICE",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "price",
					inputType: "number",
					showOnHover: true,
				}),
			},
			{
				field: "currency",
				translateCode: "CHANEL_PB_ORDER.CURRENCY",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: SelectGenericCell,
				genericCellParams: new SelectGenericCell.Params({
					field: "currency",
					itemValueField: "ecCurrencyNum",
					itemTextField: "code",
					values: this.currencyList,
					showOnHover: true,
					compareWith: Statique.compareByField,
					requiredOn: (model: DeliveryLine) => {
						return model.price != null && model.price != undefined;
					},
				}),
				
			},
			{
				field: "eccn",
				translateCode: "CHANEL_PB_ORDER.ECCN",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "eccn",
					showOnHover: true,
				}),
			}
		);

		this.dataInfos.cols = cols;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		return cols;
	}

	receiveModeModify($event) {
		this.isModeModify = $event;
	}
}
