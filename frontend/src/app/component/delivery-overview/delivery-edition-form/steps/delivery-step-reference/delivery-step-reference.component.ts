import { Component, OnInit, Input } from "@angular/core";
import { Delivery } from "@app/classes/ebDelLivraison";
import { EbUser } from "@app/classes/user";
import { Router } from "@angular/router";
import { StatusDelivery } from "@app/classes/StatusDeliveryEnum";
import { EbCategorie } from "@app/classes/categorie";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";

@Component({
	selector: "app-delivery-step-reference",
	templateUrl: "./delivery-step-reference.component.html",
	styleUrls: ["./delivery-step-reference.component.scss"],
})
export class DeliveryStepReferenceComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	delivery: Delivery;
	@Input()
	userConnected: EbUser;

	@Input()
	isModeModify: boolean = false;
	listCategories: Array<EbCategorie> = new Array<EbCategorie>();

	constructor(private router: Router) {
		super();
	}

	ngOnInit() {}

	redirectTMorPricingDetail() {
		if (this.delivery.statusDelivery == StatusDelivery.CONFIRMED) {
			this.router.navigateByUrl("app/pricing/details/" + this.delivery.xEbDemande.ebDemandeNum);
		} else if (
			this.delivery.statusDelivery == StatusDelivery.SHIPPED ||
			this.delivery.statusDelivery == StatusDelivery.DELIVERED
		) {
			this.router.navigateByUrl(
				"app/transport-management/details/" + this.delivery.xEbDemande.ebDemandeNum
			);
		}
	}

	getListCategories() {
		if (!this.delivery.listCategories) return;
		let nbre = this.delivery.listCategories.length;
		if (nbre == 1) {
			this.listCategories.push(this.delivery.listCategories[0]);
		} else if (nbre > 1) {
			this.listCategories = this.delivery.listCategories;
		}
	}
}
