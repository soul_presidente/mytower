import { Component, OnInit, Output, EventEmitter, Input } from "@angular/core";
import { DeliveryStep } from "@app/utils/enumeration";
import { Delivery } from "@app/classes/ebDelLivraison";
import { EbCategorie } from "@app/classes/categorie";
import { Order } from "@app/classes/ebDelOrder";
import { IField } from "@app/classes/customField";

@Component({
	selector: "app-delivery-overview-edition",
	templateUrl: "./delivery-overview-edition.component.html",
	styleUrls: ["./delivery-overview-edition.component.scss"],
})
export class DeliveryOverviewEditionComponent implements OnInit {
	@Output()
	onStepSelected: EventEmitter<string> = new EventEmitter<string>();

	stepsAccordionIndexes = new Map<number, string>();
	activeAccordionIndex: any;
	Step = DeliveryStep;

	@Input()
	expandAllAtStart: boolean = false;

	@Input()
	delivery: Delivery;

	listCategories: Array<EbCategorie> = new Array<EbCategorie>();
	listCustomFields: Array<IField> = new Array<IField>();

	Syntaxe2Point_Fr_En = localStorage.getItem("currentLang") == "en" ? ":" : " :";

	@Input()
	order: Order;

	constructor() {}

	ngOnInit() {
		this.stepsAccordionIndexes.set(0, DeliveryStep.DELIVERY);
		this.stepsAccordionIndexes.set(1, DeliveryStep.REFERENCE);
		this.stepsAccordionIndexes.set(2, DeliveryStep.UNITS);

		if (this.expandAllAtStart) {
			this.activeAccordionIndex = "0,1,2";
		}
		this.getListCategories();
		this.getListCustomFields();
	}

	selectStep(stepCode: string) {
		this.onStepSelected.emit(stepCode);
	}

	getListCategories() {
		if (!this.delivery.listCategories) return;
		let nbre = this.delivery.listCategories.length;
		if (nbre == 1) {
			this.listCategories.push(this.delivery.listCategories[0]);
		} else if (nbre > 1) {
			this.listCategories = this.delivery.listCategories;
		}
	}

	getListCustomFields() {
		if (!this.delivery.listCustomsFields) return;
		let nbre = this.delivery.listCustomsFields.length;
		if (nbre == 1) {
			this.listCustomFields.push(this.delivery.listCustomsFields[0]);
		} else if (nbre > 1) {
			this.listCustomFields = this.delivery.listCustomsFields;
		}
	}
}
