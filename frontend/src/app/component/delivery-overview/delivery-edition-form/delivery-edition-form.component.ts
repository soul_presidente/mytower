import { Component, OnInit, Input, ViewChild } from "@angular/core";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { AuthenticationService } from "@app/services/authentication.service";
import { Delivery } from "@app/classes/ebDelLivraison";
import { DeliveryOverviewService } from "@app/services/delivery-overview.service";
import { ActivatedRoute, Router } from "@angular/router";
import { SearchCriteriaDelivery } from "@app/utils/SearchCritereaDelivery";
import { TranslateService } from "@ngx-translate/core";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { ModalService } from "@app/shared/modal/modal.service";
import { StatusDelivery } from "@app/classes/StatusDeliveryEnum";
import { StatiqueService } from "@app/services/statique.service";
import { StatusEnum } from "@app/classes/StatusEnum";
import { Modules, IDChatComponent } from "@app/utils/enumeration";
import { MessageService } from "primeng/api";
import { EbDemande } from "@app/classes/demande";
import { DeliveryStepUnitsComponent } from "./steps/delivery-step-units/delivery-step-units.component";
import { DeliveryStepDeliveryComponent } from "./steps/delivery-step-delivery/delivery-step-delivery.component";
import { DeliveryStepReferenceComponent } from "./steps/delivery-step-reference/delivery-step-reference.component";
import { Statique } from "@app/utils/statique";
import { EbChat } from "@app/classes/chat";
import { ChatPanelComponent } from "@app/shared/chat-panel/chat-panel.component";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { DocumentsComponent } from "@app/shared/documents/documents.component";
import { ModalExportDocByTemplateComponent } from "@app/shared/modal-export-doc-by-template/modal-export-doc-by-template.component";
import { EbParty } from "@app/classes/party";

@Component({
	selector: "app-delivery-edition-form",
	templateUrl: "./delivery-edition-form.component.html",
	styleUrls: ["./delivery-edition-form.component.scss"],
})
export class DeliveryEditionFormComponent extends ConnectedUserComponent implements OnInit {
	@ViewChild("deliveryStepUnitsComponent", { static: false })
	deliveryStepUnitsComponent: DeliveryStepUnitsComponent;

	@ViewChild("deliveryStepReferenceComponent", { static: false })
	deliveryStepReferenceComponent: DeliveryStepReferenceComponent;

	@ViewChild("deliveryStepDeliveryComponent", { static: false })
	deliveryStepDeliveryComponent: DeliveryStepDeliveryComponent;

	module: number = Modules.DELIVERY_MANAGEMENT;

	showStepNavigation: boolean = true;
	delivery: Delivery;
	stepCode: string;
	isStepDelivery: boolean = true;
	isStepReference: boolean = false;
	isStepUnits: boolean = false;
	isModeModify: boolean = false;

	nbColTexteditor: string = " ";
	nbColListing: string = " ";

	IDChatComponent = IDChatComponent;
	Modules = Modules;
	Statique = Statique;

	StatusDelivery = StatusDelivery;

	listStatut: StatusEnum[];

	isConsolidating = false;
	isConfirming = false;

	@Input()
	enableCardStyle: boolean = true;

	modalExportPdfVisible: boolean = false;
	listTypeDocuments = new Array<EbTypeDocuments>();

	@ViewChild("chatPanelDeliveryComponent", { static: false })
	chatPanelDeliveryComponent: ChatPanelComponent;
	@ViewChild("documentsCmp", { static: false })
	documentsComponent: DocumentsComponent;

	showAddressPopup: boolean = false;
	optionsDisplayed: boolean = false;
	suggestionsEtablissementList: Array<EbParty> = null;
	fromOrToAddressBloc: string;
	from: string = "From";
	to: string = "To";
	selectedIndexFrom: number = null;

	constructor(
		protected headerService: HeaderService,
		protected router: Router,
		protected authenticationService: AuthenticationService,
		protected statiqueService: StatiqueService,
		private deliveryOverviewService: DeliveryOverviewService,
		private activatedRoute: ActivatedRoute,
		protected translate: TranslateService,
		protected messageService: MessageService,
		private modalService: ModalService
	) {
		super(authenticationService);
	}

	ngOnInit() {
		this.selectStep(this.stepCode);

		this.statiqueService
			.getListDeliveryStatus()
			.subscribe((res) => (this.listStatut = res.sort((s1, s2) => s1.order - s2.order)));

		let paramsid = +this.activatedRoute.snapshot.params.id;
		if (paramsid) {
			// Load data for visualization
			let criteriaGetDelivery = new SearchCriteriaDelivery();
			criteriaGetDelivery.ebDelLivraisonNum = paramsid;
			criteriaGetDelivery.connectedUserNum = this.userConnected.ebUserNum;

			this.deliveryOverviewService.getDelivery(criteriaGetDelivery).subscribe(
				(data: Delivery) => {
					this.delivery = data;
					if (this.delivery.listCategories) {
						this.delivery.listCategories.forEach(it =>{
							if (it.labels && it.labels.length) {
								it.selectedLabel = it.labels[0];
							}
						});
					}
					this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());
					this.headerService.registerActionButtons(this.actionButtons);
				},
				(error) => {
					console.log(error);
				}
			);
		} else {
			this.delivery = new Delivery();
		}
	}

	updateChatEmitter(ebChat: EbChat) {
		if (
			this.chatPanelDeliveryComponent &&
			(!ebChat.idChatComponent || ebChat.idChatComponent == IDChatComponent.DELIVERY)
		)
			this.chatPanelDeliveryComponent.insertChat(ebChat);
	}

	get isCancelled() {
		return this.delivery.statusDelivery && this.delivery.statusDelivery == StatusDelivery.CANCELED;
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		//les references ci-dessus seront géré sur un autre Jira MTALA-162 et MTALA-163
		return [
			{
				icon: "my-icon-delivery-management ",
				label: this.translate.instant("MODULES.ORDER"),
				path: "app/delivery-overview/order/dashboard",
			},
			{
				label: this.delivery.refOrder,
				path: "app/delivery-overview/order/details/" + this.delivery.xEbDelOrder.ebDelOrderNum,
			},
			{
				label: this.translate.instant("MODULES.DELIVERY"),
				path: "app/delivery-overview/delivery/dashboard",
			},
			{
				label: this.delivery.ebDelReference,
			},
		];
	}

	selectStep(stepCode: string) {
		if (stepCode == "delivery") {
			this.isStepDelivery = true;
			this.isStepReference = false;
			this.isStepUnits = false;
		} else if (stepCode == "reference") {
			this.isStepReference = true;
			this.isStepDelivery = false;

			this.isStepUnits = false;
		} else if (stepCode == "units") {
			this.isStepUnits = true;
			this.isStepDelivery = false;
			this.isStepReference = false;
		}
	}

	actionButtons: Array<HeaderInfos.ActionButton> = [
		{
			label: "GENERAL.EXPORT",
			icon: "fa fa-file-pdf-o",
			action: () => {
				this.modalExportPdfVisible = true;
			},
		},
		{
			label: "MODULES.PRICING",
			icon: "fa fa-check",
			action: ($event) => {
				this.doConsolidate($event);
			},
			displayCondition: () => {
				return (
					this.hasContribution(Modules.DELIVERY_MANAGEMENT) &&
					this.delivery.statusDelivery === StatusDelivery.CONFIRMED &&
					!this.isLinkedToTr &&
					this.acls[this.ACL.Rule.Delivery_Pricing_Enable]
				);
			},
			loadingCondition: () => {
				return this.isConsolidating;
			},
		},
		{
			label: "GENERAL.CONFIRM",
			icon: "fa fa-check",
			action: ($event) => {
				return this.confirmDelivery();
			},
			displayCondition: () => {
				return (
					this.hasContribution(Modules.DELIVERY_MANAGEMENT) &&
					this.delivery.statusDelivery === StatusDelivery.PENDING &&
					!this.isModeModify
				);
			},
			loadingCondition: () => {
				return this.isConfirming;
			},
		},
		{
			label: "GENERAL.CANCEL",
			icon: "fa fa-times",
			btnClass: "btn-tertiary",
			action: ($event) => {
				this.cancelDelivery($event);
			},
			displayCondition: () => {
				return (
					this.hasContribution(Modules.DELIVERY_MANAGEMENT) &&
					(this.delivery.statusDelivery === StatusDelivery.CONFIRMED ||
						this.delivery.statusDelivery === StatusDelivery.PENDING) &&
					!this.isModeModify
				);
			},
			enableCondition: () => {
				return (
					this.delivery != null && this.delivery.ebDelLivraisonNum != null && !this.isLinkedToTr
				);
			},
			disabledHint: this.translate.instant("CHANEL_PB_DELIVERY.MSG_ALREADY_LINKED_TO_TR"),
		},
		{
			label: "GENERAL.EDIT",
			icon: "fa fa-edit",
			btnClass: "btn-tertiary",
			action: ($event) => {
				this.isModeModify = true;
			},
			displayCondition: () => {
				return (
					this.hasContribution(Modules.DELIVERY_MANAGEMENT) &&
					!this.isModeModify
				);
			},
			enableCondition: () => {
				return this.delivery != null && this.delivery.ebDelLivraisonNum != null;
			},
		},
		{
			label: "GENERAL.SAVE",
			icon: "fa fa-save",
			btnClass: "btn-primary",
			action: ($event) => {
				this.clickSaveEdition($event);
			},
			displayCondition: () => {
				return this.isModeModify;
			},
			enableCondition: () => {
				return this.delivery != null && this.delivery.ebDelLivraisonNum != null;
			},
		},
		{
			label: "GENERAL.FREE_RELEASED",
			icon: "fa fa-unlock",
			btnClass: "btn-tertiary",
			action: ($event) => {
				this.isModeModify = false;
			},
			displayCondition: () => {
				return this.isModeModify;
			},
			enableCondition: () => {
				return this.delivery != null && this.delivery.ebDelLivraisonNum != null;
			},
		},
	];

	cancelDelivery(event) {
		let $this = this;

		if (this.delivery == null || this.delivery.ebDelLivraisonNum == null) return;

		let message = "CHANEL_PB_DELIVERY.MESSAGE_CANCEL_DELIVERY";
		let messageHead = "CHANEL_PB_DELIVERY.MESSAGE_HEAD_CANCEL_DELIVERY";
		$this.modalService.confirm(
			messageHead,
			message,
			function() {
				let requestProcessing = new RequestProcessing();
				requestProcessing.beforeSendRequest(event);

				$this.deliveryOverviewService
					.cancelDelivery($this.delivery.ebDelLivraisonNum)
					.subscribe((data) => {
						this.ngOnInit();
						requestProcessing.afterGetResponse(event);
					});
			}.bind($this)
		);
	}

	get isLinkedToTr(): boolean {
		if (this.delivery && this.delivery.refTransport) {
			return true;
		}
		return false;
	}

	get currentStatus(): StatusEnum {
		if (!this.delivery || this.delivery.statusDelivery == null || this.listStatut == null) {
			return null;
		}

		let result: StatusEnum = null;
		this.listStatut.forEach((s) => {
			if (s.code === this.delivery.statusDelivery) {
				result = s;
			}
		});

		return result;
	}

	filterStatusHeader = (status: StatusEnum): boolean => {
		if (status == null) return false;
		return status.order >= 0;
	};

	doConsolidate(event: any) {
		// Will call pre-consolidate
		this.isConsolidating = true;

		this.deliveryOverviewService
			.consolidateDeliveriesToDemande([this.delivery.ebDelLivraisonNum])
			.subscribe(
				(res: EbDemande) => {
					if (!res) return;
					this.isConsolidating = false;
					res.ebDemandeNum = 1; // just to verify conditions on demande creation (cleaningDuplicateDemande) -> will be null after
					this.router.navigateByUrl("/app/pricing/creation", {
						state: res,
					});
				},
				(error) => {
					this.isConsolidating = false;
					console.error(error);
					this.messageService.add({
						severity: "error",
						summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
						detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
					});
				}
			);
	}

	confirmDelivery() {
		this.isConfirming = true;

		this.deliveryOverviewService.confirmDelivery(this.delivery.ebDelLivraisonNum).subscribe(
			(res) => {
				this.isConfirming = false;
				this.delivery.statusDelivery = StatusDelivery.CONFIRMED;
			},
			(error) => {
				this.isConfirming = false;
				console.error(error);
				this.messageService.add({
					severity: "error",
					summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
					detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
				});
			}
		);
	}

	async clickSaveEdition(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.delivery.orderCreationDate = Statique.formatDate(this.delivery.orderCreationDate);
		this.delivery.expectedDeliveryDate = Statique.formatDate(this.delivery.expectedDeliveryDate);
		try {
			if (
				this.deliveryStepUnitsComponent &&
				this.deliveryStepUnitsComponent.deliveryPlanificationComponent
			) {
				await this.deliveryStepUnitsComponent.deliveryPlanificationComponent.planify();
			}
			await this.deliveryOverviewService.updateDelivery(this.delivery).toPromise();

			// Reload page
			Statique.refreshComponent(this.router, null, true);
		} catch (error) {
			console.error(error);
			this.messageService.add({
				severity: "error",
				summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
				detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
			});
		}

		requestProcessing.afterGetResponse(event);
	}

	onCloseModalExport(event: ModalExportDocByTemplateComponent.Result) {
		this.modalExportPdfVisible = false;

		if (
			event.params &&
			event.params.attachChecked &&
			event.params.selectedTypeDoc != null &&
			event.generatedFile != null
		) {
			this.documentsComponent.uploadFile(
				event.generatedFile,
				event.params.generatedFileName,
				event.params.selectedTypeDoc
			);
		}
	}

	hideAddressPopup() {
		this.showAddressPopup = false;
		this.optionsDisplayed = false;
	}

	getPartyAdress(data: any) {
		this.deliveryStepDeliveryComponent.getPartyAdress(data);
	}

	activateClassFrom(index: number) {
		this.deliveryStepDeliveryComponent.activateClassFrom(index);
	}

	showAddressPopupChange($event) {
		this.optionsDisplayed = false;
		this.showAddressPopup = true;
	}

	suggestionsEtablissementHandler(event: any) {
		this.suggestionsEtablissementList = event.data;
		this.fromOrToAddressBloc = event.fromOrTo;
	}
}
