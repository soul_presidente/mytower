import {
	Component,
	OnInit,
	Input,
	EventEmitter,
	Output,
	SimpleChanges,
	OnChanges,
} from "@angular/core";
import { OrderLine } from "@app/classes/ebDelOrderLine";
import { EbOrderLinePlanificationDTO } from "@app/classes/EbOrderLinePlanificationDTO";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { SearchCriteriaOrder } from "@app/utils/searchCriteriaOrder";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { Statique } from "@app/utils/statique";
import { Delivery } from "@app/classes/ebDelLivraison";
import { GenericTableScreen, OrderStatus } from "@app/utils/enumeration";
import { TextInputGenericCell } from "@app/shared/generic-cell/commons/text-input.generic-cell";
import { TreeNode, MessageService } from "primeng/api";
import { DeliveryOverviewService } from "@app/services/delivery-overview.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbOrderPlanificationDTO } from "@app/classes/EbOrderPlanificationDTO";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { DeliveryLine } from "@app/classes/ebDelLivraisonLine";

@Component({
	selector: "app-delivery-planification",
	templateUrl: "./delivery-planification.component.html",
	styleUrls: ["./delivery-planification.component.scss"],
})
export class DeliveryPlanificationComponent extends ConnectedUserComponent
	implements OnInit, OnChanges {
	dataInfosOrderLine: GenericTableInfos = new GenericTableInfos(this);
	searchInput: SearchCriteriaOrder = new SearchCriteriaOrder();
	genericTableReady = false;
	datatableComponent = GenericTableScreen;

	searchInputPlannification: SearchCriteriaOrder = new SearchCriteriaOrder();
	dataInfosPlannification: GenericTableInfos = new GenericTableInfos(this);
	numberPerPage: number = 5;
	totalOrderLineRecords: number;
	treeData: TreeNode[] = [];
	filteredListOrderLines: Array<EbOrderLinePlanificationDTO> = new Array<
		EbOrderLinePlanificationDTO
	>();

	@Input()
	delivery: Delivery;
	listToPlan: Array<EbOrderLinePlanificationDTO> = new Array<EbOrderLinePlanificationDTO>();

	addAction: boolean;

	planificationRequest = new EbOrderPlanificationDTO();

	@Input()
	deliveryLines: Array<DeliveryLine> = new Array<DeliveryLine>();

	constructor(
		private deliveryOverviewService: DeliveryOverviewService,
		private translate: TranslateService,
		private modalService?: ModalService,
		protected messageService?: MessageService
	) {
		super();
	}

	ngOnInit() {}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["deliveryLines"] && this.delivery && this.deliveryLines) {
			this.initGenericTable();
			this.loadListToPlan();
			this.initGenericTablePlannification();
		}
	}

	async loadListToPlan() {
		this.listToPlan = [];
		if (this.deliveryLines != null) {
			this.deliveryLines.forEach((element) => {
				this.deliveryOverviewService
					.getOrderLineByDeliveryLine(element.ebDelLivraisonLineNum)
					.subscribe((resultOrderline) => {
						var orderLineDTO: EbOrderLinePlanificationDTO = new EbOrderLinePlanificationDTO();
						orderLineDTO.orderLine = resultOrderline;
						orderLineDTO.ebDelOrderLineNum = resultOrderline.ebDelOrderLineNum;
						orderLineDTO.itemNum = resultOrderline.itemNumber;
						orderLineDTO.itemName = resultOrderline.itemName;
						orderLineDTO.ebDelLivraisonLineNum = element.ebDelLivraisonLineNum;
						orderLineDTO.orderLine.quantityPlannable =
							resultOrderline.quantityPlannable + element.quantity;
						orderLineDTO.quantityToPlan = element.quantity;
						this.listToPlan.push(orderLineDTO);
					});
			});
		} else {
			this.listToPlan = [];
		}
	}

	async initGenericTable() {
		this.initGenericTableCore();
		this.initGenericTableCols();
		this.genericTableReady = true;
	}

	initGenericTableCore() {
		this.searchInput.pageNumber = 1;
		this.searchInput.size = 5;
		this.searchInput.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchInput.connectedUserNum = this.userConnected.ebUserNum;
		if (this.delivery.xEbDelOrder.ebDelOrderNum)
			this.searchInput.ebDelOrderNum = this.delivery.xEbDelOrder.ebDelOrderNum;
		this.dataInfosOrderLine.dataKey = "ebDelOrderLineNum";
		this.dataInfosOrderLine.dataSourceType = GenericTableInfos.DataSourceType.Url;
		this.dataInfosOrderLine.dataType = OrderLine;
		this.dataInfosOrderLine.showDetails = false;
		this.dataInfosOrderLine.showSubTable = false;
		this.dataInfosOrderLine.paginator = true;
		this.dataInfosOrderLine.numberDatasPerPage = 5;
		this.dataInfosOrderLine.showCollapsibleBtn = false;
		this.dataInfosOrderLine.showColConfigBtn = true;
		this.dataInfosOrderLine.showSaveBtn = true;
		this.dataInfosOrderLine.showSearchBtn = true;
		if (this.searchInput.ebDelOrderNum) {
			this.dataInfosOrderLine.dataLink =
				Statique.controllerOrderOverView + "/get-order-lines-planifiable-tableau";
			this.dataInfosOrderLine.cols = this.initGenericTableCols();
		}
		this.dataInfosOrderLine.lineActions = [
			{
				icon: "fa fa-plus",
				onClick: (data: OrderLine) => {
					let elementToPlan: EbOrderLinePlanificationDTO = new EbOrderLinePlanificationDTO();
					elementToPlan.orderLine = data;
					elementToPlan.ebDelOrderLineNum = data.ebDelOrderLineNum;
					elementToPlan.itemNum = data.itemNumber;
					elementToPlan.itemName = data.itemName;
					elementToPlan.quantityToPlan = data.quantityPlannable;
					this.listToPlan.unshift(elementToPlan);
					this.initOrderLines();
				},
				enableCondition: (row: OrderLine) => {
					this.addAction = false;
					this.listToPlan.forEach((element) => {
						if (row.ebDelOrderLineNum == element.ebDelOrderLineNum) this.addAction = true;
					});
					return this.addAction;
				},
			},
		];
	}
	initGenericTableCols(): Array<GenericTableInfos.Col> {
		let cols = new Array<GenericTableInfos.Col>();
		cols.push(
			{
				field: "itemNumber",
				translateCode: "ORDER.ITEM_NUMBER",
			},
			{
				field: "itemName",
				translateCode: "CHANEL_PB_DELIVERY.ARTICLE_NAME",
			},

			{
				field: "quantityPlannable",
				translateCode: "CHANEL_PB_ORDER.PLANNABLE_QUANTITY",
			},

			{
				field: "quantityPlanned",
				translateCode: "CHANEL_PB_ORDER.PICKED_QUANTITY",
			},
			{ field: "quantityPrepared", translateCode: "CHANEL_PB_ORDER.PREPARED_QUANTITY" }
		);
		return cols;
	}

	//le tableau de planification

	async initGenericTablePlannification() {
		this.initGenericTableCorePlannification();
		this.initGenericTableColsPlannification();
	}
	initGenericTableCorePlannification() {
		this.searchInputPlannification.pageNumber = 1;
		this.searchInputPlannification.size = 5;
		this.searchInputPlannification.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchInputPlannification.connectedUserNum = this.userConnected.ebUserNum;
		if (this.delivery.xEbDelOrder.ebDelOrderNum)
			this.searchInputPlannification.ebDelOrderNum = this.delivery.xEbDelOrder.ebDelOrderNum;
		this.dataInfosPlannification.dataKey = "ebDelOrderLineNum";
		this.dataInfosPlannification.dataSourceType = GenericTableInfos.DataSourceType.Local;
		this.dataInfosPlannification.dataType = EbOrderLinePlanificationDTO;
		this.dataInfosPlannification.showDetails = false;
		this.dataInfosPlannification.showSubTable = false;
		this.dataInfosPlannification.paginator = false;
		this.dataInfosPlannification.numberDatasPerPage = 5;
		this.dataInfosPlannification.showCollapsibleBtn = false;
		this.dataInfosPlannification.showColConfigBtn = true;
		this.dataInfosPlannification.showSaveBtn = true;
		this.dataInfosPlannification.showSearchBtn = true;
		this.dataInfosPlannification.cols = this.initGenericTableColsPlannification();
		this.dataInfosPlannification.lineActions = [
			{
				icon: "fa fa-minus ",
				onClick: (data: EbOrderLinePlanificationDTO) => {
					let arrayDeletePlannification: Array<EbOrderLinePlanificationDTO> = Array<
						EbOrderLinePlanificationDTO
					>();
					this.listToPlan.forEach((element) => {
						if (element.ebDelOrderLineNum != data.ebDelOrderLineNum) {
							arrayDeletePlannification.push(element);
						} else if (element.ebDelLivraisonLineNum != null) {
							this.deliveryOverviewService
								.deleteDeliveryLineById(element.ebDelLivraisonLineNum)
								.subscribe();
						}
					});
					this.listToPlan = arrayDeletePlannification;
					this.initOrderLines();
				},
			},
		];
	}
	initGenericTableColsPlannification(): any {
		let cols = new Array<GenericTableInfos.Col>();
		cols.push(
			{
				field: "itemNum",
				translateCode: "ORDER.ITEM_NUMBER",
			},
			{
				field: "itemName",
				translateCode: "CHANEL_PB_DELIVERY.ARTICLE_NAME",
			},
			{
				field: "quantityToPlan",
				translateCode: "ORDER.QUANTITY",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "quantityToPlan",
					showOnHover: true,
					inputType: "number",
					maxOn(model: EbOrderLinePlanificationDTO) {
						return model.orderLine.quantityPlannable;
					},
					min: 1,
					keyupOn(event, model: EbOrderLinePlanificationDTO) {
						if (Number(event.target.value) >= model.orderLine.quantityPlannable)
							event.target.value = model.orderLine.quantityPlannable;
						return Number(event.target.value);
					},
				}),
			}
		);

		return cols;
	}

	getDataToPage(page: number) {
		this.listToPlan = [];
		let from = (page - 1) * this.numberPerPage,
			to = page * this.numberPerPage - 1;
		if (to >= this.filteredListOrderLines.length - 1) to = this.filteredListOrderLines.length - 1;
		for (let i = from; i <= to; i++) {
			this.listToPlan.push(this.filteredListOrderLines[i]);
		}
		if (to <= 0) this.listToPlan = this.filteredListOrderLines;
	}
	initOrderLines() {
		this.filteredListOrderLines = this.listToPlan || [];
		this.totalOrderLineRecords = this.filteredListOrderLines.length;
		this.getDataToPage(1);
	}

	planify(): Promise<Delivery> {
		if (
			this.delivery.xEbDelOrder.ebDelOrderNum != null &&
			this.delivery.ebDelLivraisonNum != null
		) {
			this.planificationRequest.ebDelOrderNum = this.delivery.xEbDelOrder.ebDelOrderNum;
			this.planificationRequest.ebDelLivraisonNum = this.delivery.ebDelLivraisonNum;
		}
		this.planificationRequest.dateOfGoodsAvailability = this.delivery.dateOfGoodsAvailability;
		this.planificationRequest.listToPlan = this.listToPlan;

		return this.deliveryOverviewService.savePlanifyOrder(this.planificationRequest).toPromise();
	}
}
