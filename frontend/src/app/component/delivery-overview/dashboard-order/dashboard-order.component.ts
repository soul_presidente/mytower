import { GenericTableScreen, Modules } from "./../../../utils/enumeration";
import {
	Component,
	OnInit,
	Output,
	EventEmitter,
	ViewChild,
	Input,
	OnChanges,
	SimpleChanges,
} from "@angular/core";
import { Statique } from "@app/utils/statique";
import { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { SearchCriteriaOrder } from "@app/utils/searchCriteriaOrder";
import { SearchField } from "@app/classes/searchField";
import { TranslateService } from "@ngx-translate/core";
import { Router } from "@angular/router";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { AuthenticationService } from "@app/services/authentication.service";
import { Order } from "@app/classes/ebDelOrder";
import { OrderLine } from "@app/classes/ebDelOrderLine";
import { DeliveryOverviewService } from "@app/services/delivery-overview.service";
import { EbOrderPlanificationDTO } from "@app/classes/EbOrderPlanificationDTO";
import { EbOrderLinePlanificationDTO } from "@app/classes/EbOrderLinePlanificationDTO";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { MessageService } from "primeng/api";
import { FlagColumnGenericCell } from "@app/shared/generic-cell/commons/flag-column.generic-cell";
import { EbFlag } from "@app/classes/ebFlag";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { EbUser } from "@app/classes/user";
import { EbParty } from "@app/classes/party";
import { StatiqueService } from "@app/services/statique.service";
import { DeliverySearchComponent } from "../delivery-search/delivery-search.component";

@Component({
	selector: "app-dashboard-order",
	templateUrl: "./dashboard-order.component.html",
	styleUrls: ["./dashboard-order.component.css"],
})
export class DashboardOrderComponent extends ConnectedUserComponent implements OnInit, OnChanges {
	cols: any[];
	selectedCols: any[];
	DASHBOARD_TABLE = Statique.dashboardTable;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	detailsInfos: GenericTableInfos = new GenericTableInfos(this);
	GenericTableScreen = GenericTableScreen;
	searchCriteria: SearchCriteriaOrder = new SearchCriteriaOrder();
	@Input()
	listFields: Array<IField>;
	initParamFields: Array<SearchField> = new Array<SearchField>();
	listCategorie: Array<EbCategorie>;
	public module: number;
	Modules = Modules;
	@Output()
	listFieldsEmitter = new EventEmitter<Array<IField>>();
	@Output()
	listCategorieEmitter = new EventEmitter<Array<EbCategorie>>();
	order: Order = new Order();
	orderLine: OrderLine = new OrderLine();
	enableDeliveryButton: boolean = false;
	listOrderStatus: any[];
	unitsCols: { field: string; translateCode: string }[];

	listOrderSelected: Array<Order>;
	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;

	orderLinePopupPlanificationValue: boolean;

	@ViewChild("deliverySearchComponent", { static: true })
	deliverySearchComponent: DeliverySearchComponent;

	@Input("searchInput")
	set searchInput(searchInput: SearchCriteriaOrder) {
		if (searchInput != null) {
			let tmpSearchInput: SearchCriteriaOrder = new SearchCriteriaOrder();

			tmpSearchInput.module = this.module;
			tmpSearchInput.ebUserNum = this.userConnected.ebUserNum;
			tmpSearchInput.searchInput = JSON.stringify(searchInput || {});

			this.searchCriteria = tmpSearchInput;
		}
	}

	constructor(
		private translate: TranslateService,
		private router: Router,
		private deliveryOverviewService: DeliveryOverviewService,
		protected authenticationService: AuthenticationService,
		protected statiqueService: StatiqueService,
		protected headerService?: HeaderService,
		protected messageService?: MessageService
	) {
		super(authenticationService);
	}

	getColumns(): any {
		let cols: any = [
			{
				field: "listEbFlagDTO",
				translateCode: "PRICING_BOOKING.FLAG",
				minWidth: "125px",
				allowClick: false,
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: FlagColumnGenericCell,
				genericCellParams: new FlagColumnGenericCell.Params({
					field: "listEbFlagDTO",
					contributionAccess: true,
					moduleNum: Modules.ORDER_MANAGEMENT,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},

			{
				field: "xEbOwnerOfTheRequest",
				translateCode: "PRICING_BOOKING.REQUESTOR",
				render: function(xEbOwnerOfTheRequest: EbUser, d, row) {
					return xEbOwnerOfTheRequest && xEbOwnerOfTheRequest.ebUserNum
						? xEbOwnerOfTheRequest.nom + " - " + xEbOwnerOfTheRequest.prenom
						: "";
				},
			},
			{
				field: "xEbPartyOrigin",
				translateCode: "PRICING_BOOKING.ADDRESS_FROM",
				render: function(xEbPartyOrigin: EbParty, d, row) {
					return xEbPartyOrigin && xEbPartyOrigin.ebPartyNum
						? xEbPartyOrigin.xEcCountry.libelle
						: "";
				},
			},
			{
				field: "xEbPartyDestination",
				translateCode: "PRICING_BOOKING.ADDRESS_TO",
				render: function(xEbPartyDestination: EbParty, d, row) {
					return xEbPartyDestination && xEbPartyDestination.ebPartyNum
						? xEbPartyDestination.xEcCountry.libelle
						: "";
				},
			},
			{
				field: "orderStatus",
				translateCode: "CHANEL_PB_ORDER.ORDER_STATUS",
				render: function(data) {
					return this.translate.instant("CHANEL_PB_ORDER." + data.toUpperCase());
				}.bind(this),
			},
			{ field: "customerCreationDate", translateCode: "ORDER.CUSTOMER_CREATION_DATE" },
			{ field: "deliveryDate", translateCode: "ORDER.DELIVERY_DATE" },
			{ field: "customerOrderReference", translateCode: "ORDER.CUSTOMER_ORDER_REFERENCE" },
			{ field: "totalQuantityPlannable", translateCode: "ORDER.TOTAL_PENDING_QUANTITY" },
			{ field: "reference", translateCode: "ORDER.ORDER_REFERENCE" },
		];
		return cols;
	}

	getDetailColumns(): any {
		let cols: any = [
			{ field: "refArticle", translateCode: "CHANEL_PB_ORDER.PRODUCT_REF" },
			{ field: "articleName", translateCode: "CHANEL_PB_ORDER.PRODUCT_NAME" },
			{ field: "originCountry", translateCode: "CHANEL_PB_ORDER.ORIGIN_COUNTRY" },
			{ field: "quantityOrdered", translateCode: "CHANEL_PB_ORDER.ORDERED_QTY" },
			{ field: "quantityPending", translateCode: "CHANEL_PB_ORDER.PENDING_QTY" },
			{ field: "quantityPlanned", translateCode: "CHANEL_PB_ORDER.PICKED_QTY" },
			{ field: "quantityPrepared", translateCode: "CHANEL_PB_ORDER.PREPARED_QUANTITY" },
			{ field: "quantityPacked", translateCode: "CHANEL_PB_ORDER.PACKED_QTY" },
			{ field: "quantityShipped", translateCode: "CHANEL_PB_ORDER.SHIPPED_QTY" },
			{ field: "quantityInvoiced", translateCode: "CHANEL_PB_ORDER.INVOICE_QTY" },
			{ field: "canceledQuantity", translateCode: "CHANEL_PB_ORDER.CANCELED_QTY" },
			{ field: "canceledReason", translateCode: "CHANEL_PB_ORDER.CANCELED_REASON" },
		];
		return cols;
	}

	getUnitsColumns(): any {
		let cols: any = [
			{ field: "refArticle", translateCode: "CHANEL_PB_ORDER.PRODUCT_REF" },
			{ field: "articleName", translateCode: "CHANEL_PB_ORDER.PRODUCT_NAME" },
			{ field: "quantityOrdered", translateCode: "CHANEL_PB_ORDER.ORDERED_QTY" },
			{ field: "quantity", translateCode: "CHANEL_PB_ORDER.QUANTITY" },
		];
		return cols;
	}

	ngOnInit() {
		this.module = Modules.ORDER_MANAGEMENT;
		this.initParamFields = require("../../../../assets/ressources/jsonfiles/order-search.json");
		this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());

		// Colonnes vibles sur le dashboard
		// Total des colonnes visibles et non visibles
		this.translator(this.dataInfos.cols);
		this.translator(this.dataInfos.selectedCols);
		this.dataInfos.cols = this.getColumns();
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.dataLink = Statique.controllerOrderOverView + "/list";
		this.dataInfos.dataKey = "ebDelOrderNum";
		this.dataInfos.dataType = OrderLine;
		this.dataInfos.lineActions = [];
		this.dataInfos.showDetails = true;
		this.dataInfos.showSubTable = false;
		this.dataInfos.enableSubTablePagination = true;
		this.dataInfos.subTableNumberDatasPerPage = 10;
		this.dataInfos.showAdvancedSearchBtn = true;
		this.dataInfos.paginator = true;
		this.dataInfos.showEditBtn = false;
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.actionDetailsLink = "app/delivery-overview/order/details/";
		this.dataInfos.contextMenu = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.blankBtnCount = 0;

		this.dataInfos.extractWithAdvancedSearch = true;

		this.dataInfos.showCheckbox = true;

		this.dataInfos.insertDetailsContextItem();
		this.dataInfos.insertDetailsNewTabContextItem();
		this.detailsInfos.cols = this.getDetailColumns();
		this.translator(this.detailsInfos.cols);
		this.detailsInfos.selectedCols = this.detailsInfos.cols;

		this.detailsInfos.dataLink = Statique.controllerOrderOverView + "/get-order-lines";
		this.detailsInfos.paginator = false;
		this.detailsInfos.numberDatasPerPage = 5;
		this.dataInfos.showFlags = true;
		this.dataInfos.dataFlagsField = "listEbFlagDTO";
		this.translator(this.dataInfos.cols);
		this.translator(this.dataInfos.selectedCols);
		this.translator(this.detailsInfos.cols);
		// show etablissement name only for super admins
		if (this.userConnected.superAdmin) {
			this.dataInfos.cols.unshift({
				field: "etablissementName",
				sortable: true,
				translateCode: "CHANEL_PB_ORDER.ETAB_NOM",
			});
		}
	}
	ngOnChanges(changes: SimpleChanges) {
		if (changes["module"] || (changes["searchInput"] && changes["searchInput"].firstChange)) {
			this.ngOnInit();
		}
	}

	onListCategorieLoaded(listCategorie: Array<EbCategorie>) {
		this.listCategorie = listCategorie;
		this.listCategorieEmitter.emit(listCategorie);
	}
	onListFieldLoaded(listField: Array<IField>) {
		this.listFields = listField;
		this.listFieldsEmitter.emit(listField);
	}

	editAction(event) {
		this.router.navigate(["/app/delivery-overview/order/creation/" + event.ebDelOrderNum]);
	}

	search(event: SearchCriteriaOrder) {
		this.searchInput = event;
	}

	translator(tab: any[]) {
		tab.forEach((it) => {
			this.translate.get(it.translateCode).subscribe((res) => {
				it["header"] = res;
			});
		});
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				icon: "my-icon-order-management ",
				label: "MODULES.ORDER",
			},
		];
	}

	inlineEditingHandler(key: string, value: any, model: any, context: any) {
		if (key === "selection-change") {
			context.autoSaveRow(model, value);
		}
	}

	autoSaveRow(row: Order, listFlag: Array<EbFlag>) {
		let listFlagCode = EbFlag.convertListToCommaSeparated(listFlag);
		row.listFlag = listFlagCode;
		this.deliveryOverviewService.saveFlags(row.ebDelOrderNum, listFlagCode).subscribe();
	}

	onCheckRow() {
		let data = this.genericTable.getSelectedRows();
		let mapData = new Array<Order>();
		this.listOrderSelected = new Array<Order>();
		data.forEach((it) => {
			mapData.push(it);
		});
		this.listOrderSelected = mapData;
	}
}
