import { GenericTableScreen, DeliveryStatus } from "./../../../utils/enumeration";
import {
	Component,
	OnInit,
	Output,
	EventEmitter,
	ViewChild,
	Input,
	SimpleChanges,
	OnChanges,
} from "@angular/core";
import { Statique } from "@app/utils/statique";
import { Modules } from "@app/utils/enumeration";
import { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { SearchField } from "@app/classes/searchField";
import { GenericTableService } from "@app/services/generic-table.service";
import { SearchCriteriaDelivery } from "@app/utils/SearchCritereaDelivery";
import { TranslateService } from "@ngx-translate/core";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { Router, ActivatedRoute } from "@angular/router";
import { CommentIconPopupGenericCell } from "@app/shared/generic-cell/commons/comment-icon-popup.generic-cell";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { DeliveryOverviewService } from "@app/services/delivery-overview.service";
import { Delivery } from "@app/classes/ebDelLivraison";
import { DeliveryLine } from "@app/classes/ebDelLivraisonLine";
import { AuthenticationService } from "@app/services/authentication.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbDemande } from "@app/classes/demande";
import { MessageService } from "primeng/api";
import { ModalService } from "@app/shared/modal/modal.service";
import { EbOrderPlanificationDTO } from "@app/classes/EbOrderPlanificationDTO";
import { StatusDelivery } from "@app/classes/StatusDeliveryEnum";
import { StatiqueService } from "@app/services/statique.service";
import { EbUser } from "@app/classes/user";
import { EbParty } from "@app/classes/party";
import { DeliverySearchComponent } from "../delivery-search/delivery-search.component";
import { FlagColumnGenericCell } from "@app/shared/generic-cell/commons/flag-column.generic-cell";
import { EbFlag } from "@app/classes/ebFlag";

@Component({
	selector: "app-dashboard-delivery",
	templateUrl: "./dashboard-delivery.component.html",
	styleUrls: ["./dashboard-delivery.component.css"],
})
export class DashboardDeliveryComponent extends ConnectedUserComponent
	implements OnInit, OnChanges {
	returnTexte: string;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	detailsInfos: GenericTableInfos = new GenericTableInfos(this);
	Modules = Modules;
	GenericTableScreen = GenericTableScreen;
	listFields: Array<IField>;
	initParamFields: Array<SearchField> = new Array<SearchField>();
	listCategorie: Array<EbCategorie>;
	@Output()
	listFieldsEmitter = new EventEmitter<Array<IField>>();
	@Output()
	listCategorieEmitter = new EventEmitter<Array<EbCategorie>>();
	@ViewChild("genericTable", { static: true })
	genericTable: GenericTableComponent;
	isLoadingShowConsolidateButton: boolean = false;
	listDeliveryForConsolidate: Array<number>;
	@Input()
	delivery: Delivery;
	public module: number;
	listDeliveryStatus: any[];
	listSelectedDelivery: Array<Delivery>;
	@ViewChild("deliverySearchComponent", { static: true })
	deliverySearchComponent: DeliverySearchComponent;
	searchCriteria: SearchCriteriaDelivery = new SearchCriteriaDelivery();
	@Input("searchInput")
	set searchInput(searchInput: SearchCriteriaDelivery) {
		if (searchInput != null) {
			let tmpSearchInput: SearchCriteriaDelivery = new SearchCriteriaDelivery();

			tmpSearchInput.module = this.module;
			tmpSearchInput.ebUserNum = this.userConnected.ebUserNum;
			tmpSearchInput.searchInput = JSON.stringify(searchInput || {});

			this.searchCriteria = tmpSearchInput;
		}
	}
	constructor(
		private translate: TranslateService,
		private genericTableService: GenericTableService,
		protected headerService: HeaderService,
		protected router: Router,
		protected activatedRoute: ActivatedRoute,
		protected statiqueService: StatiqueService,
		protected deliveryOverviewService: DeliveryOverviewService,
		protected authenticationService: AuthenticationService,
		protected messageService?: MessageService,
		private modalService?: ModalService
	) {
		super(authenticationService);
	}

	actionButtons: Array<HeaderInfos.ActionButton> = [
		{
			label: "CHANEL_PB_DELIVERY.CONSOLIDATE",
			icon: "fa fa-plus-circle",
			action: ($event) => {
				this.showConsolidateButton($event);
			},
			displayCondition: () => {
				return (
					this.hasContribution(Modules.DELIVERY_MANAGEMENT) &&
					this.acls[this.ACL.Rule.Delivery_consolidated_Enable]
				);
			},
			enableCondition: () => {
				return (
					this.listDeliveryForConsolidate &&
					this.listDeliveryForConsolidate.length > 0 &&
					this.canConsolidate
				);
			},
			loadingCondition: () => {
				return this.isLoadingShowConsolidateButton;
			},
		},
		// For the moment, cancel & edit will be managed on detail only, will be reenabled after
		/*
		{
			label: "CHANEL_PB_DELIVERY.DELETE_DELIVERY",
			btnClass: "btn-tertiary",
			icon: "fa fa-trash",
			action: ($event) => {
				this.suppressionDeliveryButton($event);
			},
			displayCondition: () => {
				return this.hasContribution(Modules.DELIVERY_MANAGEMENT);
			},
			enableCondition: () => {
				return (
					this.listSelectedDelivery && this.listSelectedDelivery.length > 0 //&& this.isPickedDelivery
				);
			},
			loadingCondition: () => {
				return false;
			},
		},
		{
			label: "CHANEL_PB_DELIVERY.MODIFY",
			icon: "fa fa-edit",
			btnClass: "btn-primary",
			action: (row: Delivery) => {
				this.updatePopupPlanification();
			},
			displayCondition: () => {
				return this.hasContribution(Modules.DELIVERY_MANAGEMENT);
			},
			enableCondition: () => {
				return this.listSelectedDelivery && this.listSelectedDelivery.length == 1;
			},
		},
		*/
	];

	async updatePopupPlanification() {
		// sera repris dans le jira 2704
	}

	async showConsolidateButton($event) {
		this.onCheckRow();
		this.isLoadingShowConsolidateButton = true;
		this.deliveryOverviewService
			.consolidateDeliveriesToDemande(this.listDeliveryForConsolidate)
			.subscribe(
				(res: EbDemande) => {
					if (!res) return;
					this.isLoadingShowConsolidateButton = false;
					res.ebDemandeNum = 1; // just to verify conditions on demande creation (cleaningDuplicateDemande) -> will be null after
					this.router.navigateByUrl("/app/pricing/creation", { state: res });
				},
				(error) => {
					this.isLoadingShowConsolidateButton = false;
					console.error(error);
					this.messageService.add({
						severity: "error",
						summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
						detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
					});
				}
			);
	}

	suppressionDeliveryButton($event) {
		this.onCheckRow();
		let that = this;
		let listForDelete: Array<number> = [];
		this.listSelectedDelivery.forEach(function(l) {
			listForDelete.push(l.ebDelLivraisonNum);
		});
		let message = "CHANEL_PB_DELIVERY.MESSAGE_DELETE_DELIVERY";
		let messageHead = "CHANEL_PB_DELIVERY.MESSAGE_HEAD_DELETE_DELIVERY";
		that.modalService.confirm(messageHead, message, function() {
			that.deliveryOverviewService.deleteDelivery(listForDelete).subscribe((data) => {
				that.genericTable.refreshData();
			});
		});
	}

	getColumns(): any {
		let cols: any = [
			{
				field: "listEbFlagDTO",
				translateCode: "PRICING_BOOKING.FLAG",
				minWidth: "125px",
				allowClick: false,
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: FlagColumnGenericCell,
				genericCellParams: new FlagColumnGenericCell.Params({
					field: "listEbFlagDTO",
					contributionAccess: true,
					moduleNum: Modules.DELIVERY_MANAGEMENT,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{ field: "ebDelReference", translateCode: "PRICING_BOOKING.DELIVERY_REFERENCE" },
			{ field: "refTransport", translateCode: "CHANEL_PB_DELIVERY.REF_TRANSPORT" },
			{ field: "refDelivery", translateCode: "CHANEL_PB_DELIVERY.REF_DELIVERY" },
			{
				field: "xEbOwnerOfTheRequest",
				translateCode: "PRICING_BOOKING.REQUESTOR",
				render: function(xEbOwnerOfTheRequest: EbUser, d, row) {
					return xEbOwnerOfTheRequest && xEbOwnerOfTheRequest.ebUserNum
						? xEbOwnerOfTheRequest.nom + " - " + xEbOwnerOfTheRequest.prenom
						: "";
				},
			},
			{
				field: "xEbPartyOrigin",
				translateCode: "PRICING_BOOKING.ADDRESS_FROM",
				render: function(xEbPartyOrigin: EbParty, d, row) {
					return xEbPartyOrigin && xEbPartyOrigin.ebPartyNum
						? xEbPartyOrigin.xEcCountry.libelle
						: "";
				},
			},
			{
				field: "xEbPartyDestination",
				translateCode: "PRICING_BOOKING.ADDRESS_TO",
				render: function(xEbPartyDestination: EbParty, d, row) {
					return xEbPartyDestination && xEbPartyDestination.ebPartyNum
						? xEbPartyDestination.xEcCountry.libelle
						: "";
				},
			},
			{
				field: "xEbPartySale",
				translateCode: "PRICING_BOOKING.ADDRESS_SOLD_TO",
				render: function(xEbPartySale: EbParty, d, row) {
					return xEbPartySale && xEbPartySale.ebPartyNum ? xEbPartySale.xEcCountry.libelle : "";
				},
			},
			{
				field: "dateCreationSys",
				translateCode: "CHANEL_PB_DETAIL_ORDER.DATE_CREATION_LIVRAISON",
				render: function(data) {
					return Statique.formatDate(data);
				},
			},
			{ field: "xEcIncotermLibelle", translateCode: "PRICING_BOOKING.INCOTERMS" },
			{
				field: "statusDelivery",
				translateCode: "CHANEL_PB_DELIVERY.DELIVERY_STATUS",
				sortable: false,
				render: function(data) {
					if (data) {
						return this.translate.instant("CHANEL_PB_DELIVERY." + StatusDelivery[data]);
					}
				}.bind(this),
			},
			{
				field: "customerOrderReference",
				translateCode: "ORDER.CUSTOMER_ORDER_REFERENCE",
				width: "150px",
				isCardCol: true,
				cardOrder: 2,
				isCardLabel: false,
			},
			{
				field: "dateOfGoodsAvailability",
				translateCode: "CHANEL_PB_DELIVERY.DATE_OF_GOODS_AVAILABILITY",
				render: function(data) {
					return Statique.formatDate(data);
				},
			},
			{
				field: "customerDeliveryDate",
				translateCode: "CHANEL_PB_DELIVERY.CUSTOMER_DELIVERY_DATE",
				render: function(data) {
					return Statique.formatDate(data);
				}
			},
			{
				field: "orderCreationDate",
				translateCode: "CHANEL_PB_DELIVERY.ORDER_CREATION_DATE"
			}
		];
		return cols;
	}
	getDetailsColumns(): any {
		let cols: any = [
			{ field: "refArticle", translateCode: "CHANEL_PB_DELIVERY.ARTICLE_REF" },
			{ field: "articleName", translateCode: "CHANEL_PB_DELIVERY.ARTICLE_NAME" },
			{ field: "originCountry", translateCode: "CHANEL_PB_DELIVERY.ORIGIN_COUNTRY" },
			{ field: "customsCode", translateCode: "CHANEL_PB_DELIVERY.CODE_DOUANE" },
			{ field: "batch", translateCode: "CHANEL_PB_DELIVERY.BATCH" },
			{ field: "quantityPlanned", translateCode: "CHANEL_PB_ORDER.PICKED_QUANTITY" },
			{ field: "quantityPrepared", translateCode: "CHANEL_PB_ORDER.PREPARED_QUANTITY" },
			{ field: "quantityPacked", translateCode: "CHANEL_PB_DELIVERY.PACKED_QTY" },
			{ field: "quantityShipped", translateCode: "CHANEL_PB_DELIVERY.SHIPPED_QTY" },
			{ field: "partNumber", translateCode: "CHANEL_PB_DELIVERY.PART_NUMBER" },
			{ field: "serialNumber", translateCode: "CHANEL_PB_DELIVERY.SERIAL_NUMBER" },
			{ field: "dateOfRequest", translateCode: "CHANEL_PB_DELIVERY.DATE_OF_REQUEST" },
			{ field: "priority", translateCode: "CHANEL_PB_DELIVERY.PRIORITY" },
			{
				field: "comment",
				translateCode: "CHANEL_PB_DELIVERY.COMMENT",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: CommentIconPopupGenericCell,
				genericCellParams: new CommentIconPopupGenericCell.Params({
					field: "comment",
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{ field: "specification", translateCode: "CHANEL_PB_DELIVERY.SPECIFICATION" },
		];
		return cols;
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["module"] || (changes["searchInput"] && changes["searchInput"].firstChange)) {
			this.ngOnInit();
		}
	}

	ngOnInit() {
		window.functions = {
			goToTransportDetails: this.goToTransportDetails.bind(this),
			goToOrderDetails: this.goToOrderDetails.bind(this),
		};
		this.headerService.registerActionButtons(this.actionButtons);
		this.initParamFields = require("../../../../assets/ressources/jsonfiles/delivery-search.json");
		this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());
		/*  todo refactor https://jira.adias.fr/browse/CPB-92
							 Intervertir les libélés des champs Edi Order n° (EDI PO#) et Customer PO N°(Customer PO#) */
		this.dataInfos.cols = this.getColumns();
		this.module = Modules.DELIVERY_MANAGEMENT;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.dataLink = Statique.controllerDeliveryOverView + "/list";
		this.dataInfos.dataKey = "ebDelLivraisonNum";
		this.dataInfos.showDetails = true;
		this.dataInfos.showSubTable = false;
		this.dataInfos.enableSubTablePagination = true;
		this.dataInfos.showAdvancedSearchBtn = true;
		this.dataInfos.subTableNumberDatasPerPage = 10;
		this.dataInfos.paginator = true;
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.contextMenu = false;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.blankBtnCount = 0;
		this.dataInfos.showCheckbox = true;
		this.dataInfos.actionDetailsLink = "app/delivery-overview/delivery/details/";
		
		this.dataInfos.extractWithAdvancedSearch = true;

		this.detailsInfos.cols = this.getDetailsColumns();
		this.detailsInfos.selectedCols = this.detailsInfos.cols;
		this.detailsInfos.dataLink = Statique.controllerDeliveryOverView + "/get-delivery-lines";
		this.detailsInfos.paginator = false;
		this.detailsInfos.numberDatasPerPage = 5;
		this.translator(this.dataInfos.cols);
		this.translator(this.detailsInfos.cols);

		this.searchCriteria.pageNumber = 1;
		this.searchCriteria.module = this.module;
		this.searchCriteria.ebUserNum = this.userConnected.ebUserNum;

		this.activatedRoute.queryParams.subscribe((params) => {
			this.searchCriteria.ebDelLivraisonNum = +params["filterByDelivery"] || null;
			if (this.searchCriteria.ebDelLivraisonNum) {
				this.dataInfos.dataLink = Statique.controllerDeliveryOverView + "/list";
			}
			this.dataInfos.cols = this.getColumns();
		});
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				icon: "my-icon-delivery-management ",
				label: this.translate.instant("MODULES.DELIVERY"),
			},
		];
	}
	inlineEditingHandler(key: string, value: any, model: any, context: any) {
		// Auto save
		if (key === "input-event" || key === "date-change") {
			if (value === "blur" || value === "select") {
				context.autoSaveRow(model);
			}
		}
		if (key === "selection-change") {
			context.autoSaveRowFlag(model, value);
		}
		if (key === "popup-action" && value === "confirm") {
			context.autoSaveRow(model);
		}
	}
	autoSaveRowFlag(row: Delivery, listFlag: Array<EbFlag>) {
		let listFlagCode = EbFlag.convertListToCommaSeparated(listFlag);
		row.listFlag = listFlagCode;
		this.deliveryOverviewService.saveFlags(row.ebDelLivraisonNum, listFlagCode).subscribe();
	}

	autoSaveRow(row: DeliveryLine) {
		this.deliveryOverviewService.dashboardDeliveryInlineUpdate(row).subscribe((result) => {
			this.genericTable.replaceRowUsingDataKey(result);
		});
	}
	remplireTableLivraison() {
		this.genericTableService.remplissageLivraison(20).subscribe((res) => {});
	}

	search(event: SearchCriteriaDelivery) {
		this.searchInput = event;
	}

	paramFieldsHandler(pListFields) {
		this.listFields = pListFields;
	}

	CategorieHandler(plistCategorie) {
		this.listCategorie = plistCategorie;
	}

	translator(tab: any[]) {
		tab.forEach((it) => {
			this.translate.get(it.translateCode).subscribe((res) => (it["header"] = res));
		});
	}

	goToTransportDetails(value: any) {
		this.router.navigate(["app/transport-management/details/" + value]);
	}

	goToOrderDetails(value: any) {
		this.router.navigate(["/app/delivery-overview/order/" + value]);
	}
	onListCategorieLoaded(listCategorie: Array<EbCategorie>) {
		this.listCategorie = listCategorie;
		this.listCategorieEmitter.emit(listCategorie);
	}
	onListFieldLoaded(listField: Array<IField>) {
		this.listFields = listField;
		this.listFieldsEmitter.emit(listField);
	}

	updateListDeliveryForConsolidate(): Array<number> {
		let data = this.genericTable.getSelectedRows();
		let ArrayData = new Array<number>();
		let ArrayDataStatus = new Array<Delivery>();
		this.listSelectedDelivery = new Array<Delivery>();
		this.listDeliveryForConsolidate = new Array<number>();
		data.forEach((it) => {
			ArrayData.push(it.ebDelLivraisonNum);
			ArrayDataStatus.push(it);
		});

		this.listDeliveryForConsolidate = ArrayData;
		this.listSelectedDelivery = ArrayDataStatus;
		return ArrayData;
	}

	onCheckRow() {
		this.updateListDeliveryForConsolidate();
	}

	get canConsolidate(): boolean {
		let result = true;

		if (this.listSelectedDelivery == null || this.listSelectedDelivery.length == 0) {
			return false;
		}

		for (let delivery of this.listSelectedDelivery) {
			if (delivery.statusDelivery !== StatusDelivery.CONFIRMED || delivery.refTransport) {
				result = false;
			}
		}

		return result;
	}
}
