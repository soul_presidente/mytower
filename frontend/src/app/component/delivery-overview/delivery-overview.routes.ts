import { Route, RouterModule } from "@angular/router";
import { DashboardOrderComponent } from "./dashboard-order/dashboard-order.component";
import { DashboardDeliveryComponent } from "./dashboard-delivery/dashboard-delivery.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";
import { OrderVisualisationComponent } from "@app/component/delivery-overview/order-visualisation/order-visualisation.component";
import { DeliveryEditionFormComponent } from "./delivery-edition-form/delivery-edition-form.component";
import { OrderCreationComponent } from "./order-creation/order-creation.component";

export const DELIVERY_OVERVIEW_ROUTES: Route[] = [
	{
		path: "order",
		redirectTo: "order/dashboard",
		canActivate: [UserAuthGuardService],
		pathMatch: "full",
	},
	{
		path: "order/dashboard",
		component: DashboardOrderComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "order/creation",
		component: OrderCreationComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "delivery/dashboard",
		component: DashboardDeliveryComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "order/details/:id",
		component: OrderVisualisationComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "order/creation/:id",
		component: OrderCreationComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "delivery/details/:id",
		component: DeliveryEditionFormComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const DeliveryOverviewRoutes = RouterModule.forChild(DELIVERY_OVERVIEW_ROUTES);
