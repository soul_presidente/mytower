import { Component, OnInit, Input } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { Order } from "@app/classes/ebDelOrder";
import { EbUser } from "@app/classes/user";
import { OrderStatus } from "@app/utils/enumeration";
import { StatiqueService } from "@app/services/statique.service";
import { StatusEnum } from "@app/classes/StatusEnum";

@Component({
	selector: "app-order-visualisation-header",
	templateUrl: "./order-visualisation-header.component.html",
	styleUrls: ["./order-visualisation-header.component.scss"],
})
export class OrderVisualisationHeaderComponent extends ConnectedUserComponent implements OnInit {
	@Input()
	order: Order;

	@Input()
	module: number;

	@Input()
	userConnected: EbUser;

	isActiveINP: boolean = true;

	isCancelled: boolean = false;

	OrderStatus = OrderStatus;

	listStatut: StatusEnum[];

	// START : Demo statuses
	okForPayment: boolean = false;
	paid: boolean = false;
	// END : Demo statuses

	constructor(private statiqueService: StatiqueService) {
		super();
	}

	ngOnInit() {
		this.statiqueService.getListOrderStatus().subscribe((res) => {
			this.listStatut = res;
		});
	}

	get currentStatus(): StatusEnum {
		if (!this.order || this.order.orderStatus == null || this.listStatut == null) {
			return null;
		}

		let result: StatusEnum = null;
		this.listStatut.forEach((s) => {
			if (s.key === this.order.orderStatus) {
				result = s;
			}
		});

		return result;
	}

	filterStatusHeader = (status: StatusEnum): boolean => {
		if (status == null) return false;
		return status.order >= 0;
	};
}
