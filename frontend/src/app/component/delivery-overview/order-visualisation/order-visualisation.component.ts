import { Component, OnInit, ViewChild } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { Order } from "@app/classes/ebDelOrder";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { SearchCriteriaDelivery } from "@app/utils/SearchCritereaDelivery";
import { SearchCriteriaOrder } from "@app/utils/searchCriteriaOrder";
import { GenericTableService } from "@app/services/generic-table.service";
import { Statique } from "@app/utils/statique";
import { OrderStep, IDChatComponent, Modules } from "@app/utils/enumeration";
import { HeaderService } from "@app/services/header.service";
import { OrderCreationFormComponent } from "@app/component/delivery-overview/order-creation-form/order-creation-form.component";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { DeliveryOverviewService } from "@app/services/delivery-overview.service";
import { EbOrderLinePlanificationDTO } from "@app/classes/EbOrderLinePlanificationDTO";
import { TranslateService } from "@ngx-translate/core";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { MessageService } from "primeng/api";
import { EbChat } from "@app/classes/chat";
import { ChatPanelComponent } from "@app/shared/chat-panel/chat-panel.component";
import { DocumentsComponent } from "@app/shared/documents/documents.component";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { ModalExportDocByTemplateComponent } from "@app/shared/modal-export-doc-by-template/modal-export-doc-by-template.component";
import { EbCategorie } from "@app/classes/categorie";
import { Delivery } from "@app/classes/ebDelLivraison";
import { EbUser } from "@app/classes/user";
import { EbCommande } from "@app/classes/commande";
import { EbCompagnie } from "@app/classes/compagnie";
import { EbEtablissement } from "@app/classes/etablissement";

@Component({
	selector: "app-order-visualisation",
	templateUrl: "./order-visualisation.component.html",
	styleUrls: [
		"./order-visualisation.component.scss",
		"./order-visualisation-header/order-visualisation-header.component.scss",
	],
})
export class OrderVisualisationComponent extends ConnectedUserComponent implements OnInit {
	order: Order;
	searchInput: SearchCriteriaDelivery = new SearchCriteriaDelivery();
	criteria: SearchCriteriaOrder = new SearchCriteriaOrder();
	orderStatus: number = 0;
	module: number = Modules.ORDER_MANAGEMENT;
	ordrStatus: number = 0;
	isEditMode: boolean = false;
	selectedStep: string = OrderStep.ORDER;
	hasContributionAccess: boolean;

	modalExportPdfVisible: boolean = false;
	listTypeDocuments = new Array<EbTypeDocuments>();

	@ViewChild("orderCreationForm", { static: false })
	orderCreationFormComponent: OrderCreationFormComponent;

	@ViewChild("chatPanelOrderComponent", { static: false })
	chatPanelOrderComponent: ChatPanelComponent;
	@ViewChild("documentsCmp", { static: false })
	documentsComponent: DocumentsComponent;

	nbColTexteditor: string = " ";
	nbColListing: string = " ";

	IDChatComponent = IDChatComponent;
	Modules = Modules;
	Statique = Statique;

	constructor(
		protected route: ActivatedRoute,
		protected router: Router,
		protected genericTableService: GenericTableService,
		protected headerService: HeaderService,
		protected translate: TranslateService,
		protected messageService: MessageService,
		protected deliveryOverviewService: DeliveryOverviewService
	) {
		super();
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				icon: "my-icon-order-management ",
				label: "GENERAL.ORDER",
				path: "app/delivery-overview/order/dashboard",
			},
			{
				label: this.order.reference,
			},
		];
	}
	ngOnInit() {
		this.route.params.subscribe((params) => {
			this.searchInput.ebDelOrderNum = !isNaN(parseInt(params["id"])) ? parseInt(params["id"]) : -1;
			this.criteria.ebDelOrderNum = this.searchInput.ebDelOrderNum;
			this.criteria.connectedUserNum = this.userConnected.ebUserNum;
			this.genericTableService
				.getDatas(Statique.controllerOrderOverView + "/list", this.criteria)
				.subscribe((res) => {
					if (res.count > 0) {
						this.order = res.data[0];
						this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());
					}
				});
		});
		this.headerService.registerActionButtons(this.actionButtons);
	}

	updateChatEmitter(ebChat: EbChat) {
		if (
			this.chatPanelOrderComponent &&
			(!ebChat.idChatComponent || ebChat.idChatComponent == IDChatComponent.ORDER_MANAGEMENT)
		)
			this.chatPanelOrderComponent.insertChat(ebChat);
	}

	get isCancelled() {
		return this.order.orderStatus && this.order.orderStatus == "CANCELED";
	}

	get isShowOptions(): boolean {
		return !!Statique.getInnerObjectStr(this, "orderCreationForm.stepOrder.showAddressPopup");
	}

	actionButtons: Array<HeaderInfos.ActionButton> = [
		{
			label: "GENERAL.EXPORT",
			icon: "fa fa-file-pdf-o",
			action: () => {
				this.modalExportPdfVisible = true;
			},
		},
		{
			label: "ORDER.PLANIFY",
			icon: "my-icon-delivery-management",
			btnClass: "btn-primary",
			action: () => {
				this.orderCreationFormComponent.orderStepDelivery.showPlanificationDetail();
			},
			displayCondition: () => {
				return (
					this.orderCreationFormComponent &&
					this.orderCreationFormComponent.orderStepDelivery &&
					!this.orderCreationFormComponent.orderStepDelivery.modePlanify &&
					this.order != null &&
					this.order.totalQuantityPlannable > 0 &&
					!this.isEditMode && this.hasContribution(Modules.ORDER_MANAGEMENT) && 
					this.hasContribution(Modules.DELIVERY_MANAGEMENT)
				);
			},
		},
		{
			label: "CHANEL_PB_DETAIL_ORDER.DELIVERIES_DETAIL",
			icon: "fa fa-arrow-left",
			btnClass: "btn-tertiary",
			action: () => {
				this.orderCreationFormComponent.orderStepDelivery.modePlanify = false;
			},
			displayCondition: () => {
				return (
					this.orderCreationFormComponent &&
					this.orderCreationFormComponent.orderStepDelivery &&
					this.orderCreationFormComponent.orderStepDelivery.modePlanify
				);
			},
		},
		{
			label: "GENERAL.EDIT",
			icon: "fa fa-pencil",
			btnClass: "btn-tertiary",
			action: () => {
				this.isEditMode = true;
			},
			displayCondition: () => {
				return !this.isEditMode && this.hasContribution(Modules.ORDER_MANAGEMENT);
			},
		},
		{
			label: "GENERAL.SAVE",
			icon: "fa fa-save",
			btnClass: "btn-primary",
			action: (event) => {
				this.saveOrder(event);
			},
			displayCondition: () => {
				return this.order && this.isEditMode;
			},
		},
		{
			label: "GENERAL.FREE_RELEASED",
			icon: "fa fa-unlock",
			btnClass: "btn-tertiary",
			action: () => {
				this.isEditMode = false;
			},
			displayCondition: () => {
				return this.isEditMode;
			},
		},
	];

	saveOrder(event) {
		// Prepare order for saving
		this.order.customerCreationDate = Statique.formatDate(this.order.customerCreationDate);
		this.order.deliveryDate = Statique.formatDate(this.order.deliveryDate);
		this.order.targetDate = Statique.formatDate(this.order.targetDate);
		this.order.orderDate = Statique.formatDate(this.order.orderDate);
		let truncate3Decimal = 3;
		let _ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		let _ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		let _ebUserNum = this.userConnected.ebUserNum;

		this.order.orderLines.forEach((orderLine) => {
			if (Statique.isDefined(orderLine.price)) {
				orderLine.price = Statique.truncate2Decimal(orderLine.price, truncate3Decimal);
			}
		});

		this.order.livraisons.forEach((delivery) => {
			let deliveryToSave: Delivery = new Delivery();
			delivery.statusDelivery = deliveryToSave.statusDelivery;
			delivery.xEbDelOrder.xEbCompagnie = new EbCompagnie();
			delivery.xEbDelOrder.xEbCompagnie.ebCompagnieNum = _ebCompagnieNum;
			delivery.xEbDelOrder.xEbEtablissement = new EbEtablissement();
			delivery.xEbDelOrder.xEbEtablissement.ebEtablissementNum = _ebEtablissementNum;
			delivery.xEbDelOrder.xEbOwnerOfTheRequest = new EbUser();
			delivery.xEbDelOrder.xEbOwnerOfTheRequest.ebUserNum = _ebUserNum;
			delivery.xEbCompagnie = new EbCompagnie();
			delivery.xEbCompagnie.ebCompagnieNum = _ebCompagnieNum;
			delivery.xEbEtablissement = new EbEtablissement();
			delivery.xEbEtablissement.ebEtablissementNum = _ebEtablissementNum;
			delivery.xEbOwnerOfTheRequest = new EbUser();
			delivery.xEbOwnerOfTheRequest.ebUserNum = _ebUserNum;
			delivery.listCategories.forEach((categorie) => {
				categorie.compagnie = new EbCompagnie();
				categorie.compagnie.ebCompagnieNum = _ebCompagnieNum;
			});
		});

		this.order.xEbOwnerOfTheRequest.ebCompagnie = new EbCompagnie();
		this.order.xEbOwnerOfTheRequest.ebCompagnie.ebCompagnieNum = _ebCompagnieNum;

		this.order.listCategories = this.order.listCategories.map((categorie) => {
			let _categorie: EbCategorie = new EbCategorie();

			_categorie.constructorCopyLite(categorie);

			if (categorie.selectedLabel != null && categorie.selectedLabel.ebLabelNum != null) {
				_categorie.labels.push(categorie.selectedLabel);
			}
			return _categorie;
		});

		// Call save
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.order.requestOwnerEmail = this.order.xEbOwnerOfTheRequest.email;

		this.deliveryOverviewService.saveOrder(this.order).subscribe(
			(res) => {
				requestProcessing.afterGetResponse(event);

				// Reload page
				Statique.refreshComponent(this.router, null, true);
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				this.messageService.add({
					severity: "error",
					summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
					detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
				});
			}
		);
	}

	onCloseModalExport(event: ModalExportDocByTemplateComponent.Result) {
		this.modalExportPdfVisible = false;

		if (
			event.params &&
			event.params.attachChecked &&
			event.params.selectedTypeDoc != null &&
			event.generatedFile != null
		) {
			this.documentsComponent.uploadFile(
				event.generatedFile,
				event.params.generatedFileName,
				event.params.selectedTypeDoc
			);
		}
	}
}
