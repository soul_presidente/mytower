import { EventEmitter, Input, OnInit, Output } from "@angular/core";
import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";
import { CreationFormStepBaseComponent } from "@app/shared/creation-form-step-base/creation-form-step-base.component";
import { Order } from "@app/classes/ebDelOrder";

export abstract class OrderCreationFormStepBaseComponent extends CreationFormStepBaseComponent
	implements OnInit {
	@Input()
	order: Order;
	@Output()
	OrderChange = new EventEmitter<Order>();

	constructor(protected authenticationService: AuthenticationService, protected router: Router) {
		super(authenticationService, router);
	}

	ngOnInit() {
		super.ngOnInit();
	}
}
