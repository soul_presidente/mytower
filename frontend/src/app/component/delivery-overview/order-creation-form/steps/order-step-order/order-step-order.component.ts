import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";
import {
	Component,
	OnInit,
	Input,
	ChangeDetectorRef,
	ViewChild,
	Output,
	EventEmitter,
	OnChanges,
	SimpleChanges,
} from "@angular/core";
import { EbCompagnie } from "@app/classes/compagnie";
import { Statique } from "@app/utils/statique";
import { YesOrNo } from "@app/utils/enumeration";
import { TranslateService } from "@ngx-translate/core";
import { EbUser } from "@app/classes/user";
import { EbEtablissement } from "@app/classes/etablissement";
import { CompleterData, CompleterService, CompleterItem } from "ng2-completer";
import { UserService } from "@app/services/user.service";
import { EbParty } from "@app/classes/party";
import { EcCountry } from "@app/classes/country";
import { EbPlZoneDTO } from "@app/classes/trpl/EbPlZoneDTO";
import { EbAdresse } from "@app/classes/adresse";
import { GlobalService } from "@app/services/global.service";
import { distinctUntilChanged, debounceTime, switchMap } from "rxjs/operators";
import { StatiqueService } from "@app/services/statique.service";
import { MessageService, SelectItem } from "primeng/api";
import { EtablissementService } from "@app/services/etablissement.service";
import { Order } from "@app/classes/ebDelOrder";
import { AdresseForShippingOrderComponent } from "./shared-component/adresse-for-shipping-order/adresse-for-shipping-order.component";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { OrderCreationFormStepBaseComponent } from "../../core/order-creation-from-step-base";
@Component({
	selector: "app-order-step-order",
	templateUrl: "./order-step-order.component.html",
	styleUrls: ["./order-step-order.component.scss"],
})
export class OrderStepOrderComponent extends OrderCreationFormStepBaseComponent
	implements OnInit, OnChanges {
	@Input()
	order: Order;
	optionNotifyParty = true;
	ebPartyOrigin: EbParty = new EbParty();
	ebPartyDest: EbParty = new EbParty();
	ebPartyNotif: EbParty = new EbParty();
	ebPartyVendor: EbParty = new EbParty();
	ebPartyIssuer: EbParty = new EbParty();
	listEtablissementPlateform: Array<EbEtablissement> = new Array<EbEtablissement>();
	from: string = "From";
	to: string = "To";
	notifyParty: string = "Sold to";
	vendorParty: string = "Vendor to";
	issuerParty: string = "Issuer to";
	selectedUserNum: number;
	selectedUser: EbUser;
	suggestionsEtablissement: CompleterData;
	typeaheadUserEvent: EventEmitter<string> = new EventEmitter<string>();
	listUserEtab: any = new Array<EbUser>();
	listEtablissement: any = new Array<EbEtablissement>();
	@Input()
	readOnly: boolean;
	@Input()
	showAddressPopup: boolean = false;
	@Output()
	showAddressPopupChange = new EventEmitter<boolean>();

	isAddress: boolean = true;
	suggestionsEtablissementList: Array<EbParty> = null;
	fromOrToAddressBloc: string;

	showAddressWrapperFrom: boolean;
	showAddressWrapperTo: boolean;
	showAddAddressBtnFrom: boolean = false;
	showAddAddressBtnTo: boolean;
	showAddressWrapperPI: boolean;
	showAddAddressBtnPI: boolean;
	showAddAddressBtnNotif: boolean;
	showAddressWrapperNotif: boolean;
	showAddAddressBtnVendor: boolean;
	showAddressWrapperVendor: boolean;
	showAddAddressBtnIssuer: boolean;
	showAddressWrapperIssuer: boolean;
	selectedIndexFrom: number = null;
	selectedIndexTo: number = null;
	fromOrToFocus: string;

	ebPartyAddAddress: EbParty = new EbParty();
	fromOrToAddAddress: any;
	adresseToSave: EbAdresse;

	@ViewChild(AdresseForShippingOrderComponent, { static: false })
	adresseForShipping: AdresseForShippingOrderComponent;

	eligibleForConsolidation: any;
	unitInfos: any;
	@Input()
	requiredReadOnly: boolean = false;

	@Output()
	reloadDataPreference = new EventEmitter<void>();

	constructor(
		protected statiqueService: StatiqueService,
		protected etablissementService: EtablissementService,
		protected completerService: CompleterService,
		protected authenticationService: AuthenticationService,
		protected router: Router,
		private userService: UserService,
		protected cd: ChangeDetectorRef,
		private globalService: GlobalService,
		private messageService?: MessageService,
		protected translate?: TranslateService
	) {
		super(authenticationService, router);
	}

	ngOnInit(): void {
		super.ngOnInit();
		this.initLabels();
		this.getEstablishments();
		this.getPointIntermediates();
		this.setSuggestionsEtablissement();
		this.applyAddressWrappers();

		this.init();
		this.initParties();
		this.cd.detectChanges();

		if (this.order && this.order.ebDelOrderNum && this.order.eligibleForConsolidation) {
			this.eligibleForConsolidation = YesOrNo.YES;
		} else {
			this.eligibleForConsolidation = YesOrNo.NO;
			if (this.order && this.order.ebDelOrderNum) {
				this.globalService
					.runAction(Statique.controllerUnit + "/list-unit", this.order.ebDelOrderNum)
					.subscribe((res) => {
						this.unitInfos = res;
					});
			}
		}
	}

	ngOnChanges(changes: SimpleChanges) {
		if (
			changes.nbSavedFormLoaded &&
			changes.nbSavedFormLoaded.previousValue < changes.nbSavedFormLoaded.currentValue
		) {
			this.initParties();
		}
		this.applyAddressWrappers();

		if (changes["order"]) {
			if (this.order && this.order.ebDelOrderNum && this.order.xEbOwnerOfTheRequest) {
				this.listUserEtab = [this.order.xEbOwnerOfTheRequest];
				this.selectedUserNum = this.order.xEbOwnerOfTheRequest.ebUserNum;
			}
		}
	}

	initParties() {
		if (this.order.ebDelOrderNum) return;

		if (this.order.xEbPartyDestination && this.order.xEbPartyDestination.city != null) {
			this.getDestParty(this.order.xEbPartyDestination);
		}
		if (this.order.xEbPartyOrigin && this.order.xEbPartyOrigin.city != null) {
			this.getOriginParty(this.order.xEbPartyOrigin);
		}
		if (this.order.xEbPartySale && this.order.xEbPartySale.city != null) {
			this.getNotifyParty(this.order.xEbPartySale);
		}
		if (this.order.xEbPartyVendor && this.order.xEbPartyVendor.city != null) {
			this.getVendorParty(this.order.xEbPartyVendor);
		}
		if (this.order.xEbPartyIssuer && this.order.xEbPartyIssuer.city != null) {
			this.getIssuerParty(this.order.xEbPartyIssuer);
		}
	}

	getEstablishments() {
		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.getAll = true;
		searchCriteria.size = null;
		this.etablissementService
			.getListEtablisement(searchCriteria)
			.subscribe((data: Array<EbEtablissement>) => {
				this.listEtablissementPlateform = data;
			});
	}

	getPointIntermediates() {
		let compagnieToFilter: number = null;
		let etablissementToFilter: number = null;
		let userNum: number = null;

		if (
			this.order.xEbOwnerOfTheRequest &&
			this.order.xEbOwnerOfTheRequest.ebCompagnie &&
			this.order.xEbOwnerOfTheRequest.ebCompagnie.ebCompagnieNum
		) {
			userNum = this.order.xEbOwnerOfTheRequest.ebUserNum;
			compagnieToFilter = this.order.xEbOwnerOfTheRequest.ebCompagnie.ebCompagnieNum;
			if (!this.order.xEbOwnerOfTheRequest.superAdmin) {
				etablissementToFilter = this.order.xEbOwnerOfTheRequest.ebEtablissement.ebEtablissementNum;
			}
		} else {
			userNum = this.userConnected.ebUserNum;
			compagnieToFilter = this.userConnected.ebCompagnie.ebCompagnieNum;
			if (!this.userConnected.superAdmin) {
				etablissementToFilter = this.userConnected.ebEtablissement.ebEtablissementNum;
			}
		}
	}

	setSuggestionsEtablissement() {
		let autocompleteUrl = `/adresseAsParty`;

		if (
			this.order &&
			this.order.xEbOwnerOfTheRequest &&
			this.order.xEbOwnerOfTheRequest.ebCompagnie
		) {
			autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
			autocompleteUrl += `ebCompagnieNum=${this.order.xEbOwnerOfTheRequest.ebCompagnie.ebCompagnieNum}`;

			if (!this.order.xEbOwnerOfTheRequest.superAdmin) {
				autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
				autocompleteUrl += `ebEtablissementNum=${this.order.xEbOwnerOfTheRequest.ebEtablissement.ebEtablissementNum}`;
			}
		}

		autocompleteUrl += autocompleteUrl.indexOf("?") > 0 ? "&" : "?";
		autocompleteUrl += `term=`;

		this.suggestionsEtablissement = this.completerService.remote(
			Statique.controllerPricing + autocompleteUrl,
			null,
			"etablissementReference"
		);
	}

	selectedPartyNotif(selected: CompleterItem) {
		if (selected === null) {
			this.order.xEbPartySale.ebPartyNum = null;
		} else {
			this.order.xEbPartySale = selected.originalObject;
		}
	}

	compareCountry(country1: EcCountry, country2: EcCountry) {
		if (country2 !== undefined && country2 !== null) {
			return country1.ecCountryNum === country2.ecCountryNum;
		}
	}

	compareZone(zone1: EbPlZoneDTO, zone2: EbPlZoneDTO) {
		if (zone1 && zone2) {
			return zone1.ebZoneNum === zone2.ebZoneNum;
		}
	}

	getOriginParty(data: any) {
		this.ebPartyOrigin = data;
		this.order.xEbPartyOrigin = this.ebPartyOrigin;
		this.showAddressWrapperFrom = true;
		this.showAddAddressBtnFrom = false;
	}

	getVendorParty(data: any) {
		this.ebPartyVendor = data;
		this.order.xEbPartyVendor = this.ebPartyVendor;
		this.showAddressWrapperVendor = true;
		this.showAddAddressBtnVendor = false;
	}

	getIssuerParty(data: any) {
		this.ebPartyIssuer = data;
		this.order.xEbPartyIssuer = this.ebPartyIssuer;
		this.showAddressWrapperIssuer = true;
		this.showAddAddressBtnIssuer = false;
	}

	getPartyAdress(data: any) {
		if (this.fromOrToFocus == this.from) {
			this.ebPartyOrigin = data;
			this.order.xEbPartyOrigin = this.ebPartyOrigin;
			if (data.xEbUserVisibility && data.xEbUserVisibility.ebUserNum)
				this.order.xEbUserOrigin = EbUser.createUser(data.xEbUserVisibility);
			this.showAddressWrapperFrom = true;
			this.showAddAddressBtnFrom = false;
			this.getOriginParty(data);
		} else if (this.fromOrToFocus == this.to) {
			this.ebPartyDest = data;
			this.order.xEbPartyDestination = this.ebPartyDest;
			if (data.xEbUserVisibility && data.xEbUserVisibility.ebUserNum)
				this.order.xEbUserDest = EbUser.createUser(data.xEbUserVisibility);
			this.showAddressWrapperTo = true;
			this.showAddAddressBtnTo = false;
			this.getDestParty(data);
		} else if (this.fromOrToFocus == this.notifyParty) {
			this.getNotifyParty(data);
		} else if (this.fromOrToFocus == this.vendorParty) {
			this.getVendorParty(data);
		} else if (this.fromOrToFocus == this.issuerParty) {
			this.getIssuerParty(data);
		}
		this.showAddressPopup = false;
		this.showAddressPopupChange.emit(this.showAddressPopup);
	}

	getDestParty(data: any) {
		this.ebPartyDest = data;
		this.order.xEbPartyDestination = this.ebPartyDest;
		this.showAddressWrapperTo = true;
		this.showAddAddressBtnTo = false;
	}

	getNotifyParty(data: any) {
		this.ebPartyNotif = data;
		this.order.xEbPartySale = this.ebPartyNotif;
		this.showAddAddressBtnNotif = false;
		this.showAddressWrapperNotif = true;
	}

	getUnloadingWarehouse(data: any) {
		this.optionsDisplayed = false;
		this.showAddressPopup = false;
		this.showAddressPopupChange.emit(false);
	}

	protected checkValidity(): boolean {
		return this.order.xEbPartyOrigin != null && this.order.xEbPartyDestination != null;
	}

	init() {
		this.initAutocomplete();

		if (this.order && this.order.xEbOwnerOfTheRequest) {
			let user = new EbUser();
			user.constructorCopy(this.order.xEbOwnerOfTheRequest);
			this.listUserEtab = new Array<EbUser>();
			this.listUserEtab.push(user);
			this.handleSelectedUserChange(user);
		} else if (this.isChargeur) {
			let user = new EbUser();
			user.constructorCopy(this.userConnected);
			this.listUserEtab = new Array<EbUser>();
			this.listUserEtab.push(user);
			this.handleSelectedUserChange(user);
		}
	}

	initAutocomplete() {
		this.typeaheadUserEvent
			.pipe(
				switchMap((term) => {
					let criteria = new SearchCriteria();
					criteria.searchterm = term;
					criteria.size = null;
					criteria.withPassword = true;
					criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
					if (!this.userConnected.superAdmin)
						criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
					return this.userService.getListUser(criteria);
				})
			)
			.subscribe(
				(data) => {
					this.cd.markForCheck();
					this.listUserEtab = data;
				},
				(err) => {
					this.listUserEtab = [];
				}
			);
	}

	protected stepHasOptions(): boolean {
		return true;
	}

	applyAddressWrappers() {
		if (
			Statique.getInnerObjectStr(this, "order.xEbPartyDestination.city") ||
			Statique.getInnerObjectStr(this, "order.xEbPartyDestination.xEcCountry.ecCountryNum")
		) {
			this.showAddressWrapperTo = true;
			this.showAddAddressBtnTo = false;
			this.ebPartyDest = this.order.xEbPartyDestination;
		} else if (!this.order.ebDelOrderNum) {
			this.showAddressWrapperTo = false;
			this.showAddAddressBtnTo = false;
		}

		if (
			Statique.getInnerObjectStr(this, "order.xEbPartyOrigin.city") ||
			Statique.getInnerObjectStr(this, "order.xEbPartyOrigin.xEcCountry.ecCountryNum")
		) {
			this.showAddressWrapperFrom = true;
			this.showAddAddressBtnFrom = false;
			this.ebPartyOrigin = this.order.xEbPartyOrigin;
		} else if (!this.order.ebDelOrderNum) {
			this.showAddressWrapperFrom = false;
			this.showAddAddressBtnFrom = false;
		}

		if (
			Statique.getInnerObjectStr(this, "order.xEbPartySale.city") ||
			Statique.getInnerObjectStr(this, "order.xEbPartySale.xEcCountry.ecCountryNum")
		) {
			this.showAddressWrapperNotif = true;
			this.showAddAddressBtnNotif = false;
			this.ebPartyNotif = this.order.xEbPartySale;
		} else if (!this.order.ebDelOrderNum) {
			this.showAddressWrapperNotif = false;
			this.showAddAddressBtnNotif = false;
		}

		if (
			Statique.getInnerObjectStr(this, "order.xEbPartyVendor.city") ||
			Statique.getInnerObjectStr(this, "order.xEbPartyVendor.xEcCountry.ecCountryNum")
		) {
			this.showAddressWrapperVendor = true;
			this.showAddAddressBtnVendor = false;
			this.ebPartyVendor = this.order.xEbPartyVendor;
		} else if (!this.order.ebDelOrderNum) {
			this.showAddressWrapperVendor = false;
			this.showAddAddressBtnVendor = false;
		}

		if (
			Statique.getInnerObjectStr(this, "order.xEbPartyIssuer.city") ||
			Statique.getInnerObjectStr(this, "order.xEbPartyIssure.xEcCountry.ecCountryNum")
		) {
			this.showAddressWrapperIssuer = true;
			this.showAddAddressBtnIssuer = false;
			this.ebPartyIssuer = this.order.xEbPartyIssuer;
		} else if (!this.order.ebDelOrderNum) {
			this.showAddressWrapperIssuer = false;
			this.showAddAddressBtnIssuer = false;
		}

		this.cd.detectChanges();
	}

	handleSelectedUserChange(user: EbUser) {
		if (!user) {
			this.order.xEbOwnerOfTheRequest = new EbUser();
			return;
		}

		this.order.xEbOwnerOfTheRequest = new EbUser();
		this.order.xEbOwnerOfTheRequest.ebUserNum = user.ebUserNum;
		this.order.xEbOwnerOfTheRequest.nom = user.nom;
		this.order.xEbOwnerOfTheRequest.prenom = user.prenom;
		this.order.xEbOwnerOfTheRequest.admin = user.admin;
		this.order.xEbOwnerOfTheRequest.superAdmin = user.superAdmin;
		this.order.xEbEtablissement = new EbEtablissement();
		this.order.xEbEtablissement.ebEtablissementNum = user.ebEtablissement.ebEtablissementNum;
		this.order.xEbEtablissement.ebCompagnie = new EbCompagnie();
		this.order.xEbEtablissement.ebCompagnie.ebCompagnieNum = user.ebCompagnie.ebCompagnieNum;
		this.order.xEbOwnerOfTheRequest = user;
		this.order.requestOwnerEmail = user.email;
		this.selectedUserNum = user.ebUserNum;
		this.setSuggestionsEtablissement();
		this.reloadDataPreference.emit();
	}
	suggestionsEtablissementHandler(event: any) {
		this.suggestionsEtablissementList = event.data.data;
		this.fromOrToAddressBloc = event.fromOrTo;
	}
	suggestionsWarehouseHandler(event: any) {
		this.fromOrToAddressBloc = event.fromOrTo;
	}

	showAddressPopupHandle(event) {
		this.optionsDisplayed = false;
		this.showAddressPopup = true;
		this.isAddress = true;
		this.fromOrToFocus = event.fromOrTo;
		this.showAddressPopupChange.emit(this.showAddressPopup);
	}
	showWarehousePopupHandle(event: any) {
		this.optionsDisplayed = false;
		this.showAddressPopup = true;
		this.isAddress = false;
		this.showAddressPopupChange.emit(this.showAddressPopup);
	}
	removeAddressOriginHandler(event: any) {
		this.selectedIndexFrom = null;
		this.ebPartyOrigin = new EbParty();
		this.order.xEbPartyOrigin = null;
		this.showAddressWrapperFrom = false;
		this.showAddAddressBtnFrom = true;
	}

	removeAddressDestHandler(event: any) {
		this.selectedIndexTo = null;
		this.ebPartyDest = new EbParty();
		this.order.xEbPartyDestination = null;
		this.showAddressWrapperTo = false;
		this.showAddAddressBtnTo = true;
	}
	removeAddressNotifHandler(event: any) {
		this.selectedIndexTo = null;
		this.ebPartyNotif = new EbParty();
		this.order.xEbPartySale = null;
		this.showAddAddressBtnNotif = false;
		this.showAddressWrapperNotif = true;
	}

	removeAddressVendorHandler(event: any) {
		this.selectedIndexTo = null;
		this.ebPartyVendor = new EbParty();
		this.order.xEbPartyVendor = null;
		this.showAddAddressBtnVendor = false;
		this.showAddressWrapperVendor = true;
	}

	removeAddressIssuerHandler(event: any) {
		this.selectedIndexTo = null;
		this.ebPartyIssuer = new EbParty();
		this.order.xEbPartyIssuer = null;
		this.showAddAddressBtnIssuer = false;
		this.showAddressWrapperIssuer = true;
	}

	hideAddressPopup() {
		let showAddressPopup, optionsDisplayed;
		this.showAddressPopup = false;
		this.optionsDisplayed = false;
		showAddressPopup = this.showAddressPopup;
		optionsDisplayed = this.optionsDisplayed;
		this.showAddressPopupChange.emit(showAddressPopup);
	}

	activateClassFrom(index: number) {
		this.selectedIndexFrom = index;
	}

	activateClassTo(index: number) {
		this.selectedIndexTo = index;
	}

	addAddress(event) {
		let showAddressPopup, optionsDisplayed;
		if (this.optionsDisplayed == true) this.optionsDisplayed = false;
		this.showAddressPopup = true;
		this.isAddress = true;
		this.ebPartyAddAddress = event.ebParty;
		this.fromOrToAddAddress = event.fromOrTo;
		showAddressPopup = this.showAddressPopup;
		optionsDisplayed = this.optionsDisplayed;
		this.showAddressPopupChange.emit(showAddressPopup);
	}

	showNotifyAddress(event) {
		if (event) {
			this.optionsDisplayed = false;
			this.showAddressPopup = true;
			this.adresseForShipping.showAddressPopupEmit(this.showAddressPopup, this.notifyParty);
			this.adresseForShipping.setSuggestionsEtablissement(" ", this.notifyParty);
		}
	}

	showVendorAddress(event) {
		if (event) {
			this.optionsDisplayed = false;
			this.showAddressPopup = true;
			this.adresseForShipping.showAddressPopupEmit(this.showAddressPopup, this.vendorParty);
			this.adresseForShipping.setSuggestionsEtablissement(" ", this.vendorParty);
		}
	}

	showIssuerAddress(event) {
		if (event) {
			this.optionsDisplayed = false;
			this.showAddressPopup = true;
			this.adresseForShipping.showAddressPopupEmit(this.showAddressPopup, this.issuerParty);
			this.adresseForShipping.setSuggestionsEtablissement(" ", this.issuerParty);
		}
	}

	successfulOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	private initLabels() {
		this.from = this.translate.instant("PRICING_BOOKING.ADDRESS_FROM");
		this.to = this.translate.instant("PRICING_BOOKING.ADDRESS_TO");
		this.notifyParty = this.translate.instant("PRICING_BOOKING.ADDRESS_SOLD_TO");
		this.vendorParty = this.translate.instant("PRICING_BOOKING.ADDRESS_VENDOR_TO");
		this.issuerParty = this.translate.instant("PRICING_BOOKING.ADDRESS_ISSUER_TO");
	}
}
