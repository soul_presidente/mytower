import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";
import {
	Component,
	OnInit,
	Input,
	ViewChild,
	ChangeDetectorRef,
	SimpleChanges,
	OnChanges,
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { OrderStep, Modules, OrderStatus, ModeOfTransport } from "@app/utils/enumeration";
import { EbCategorie } from "@app/classes/categorie";
import { IField } from "@app/classes/customField";
import { EtablissementService } from "@app/services/etablissement.service";
import { EbDemande } from "@app/classes/demande";
import { Order } from "@app/classes/ebDelOrder";
import { SearchCriteriaOrder } from "@app/utils/searchCriteriaOrder";
import { Statique } from "@app/utils/statique";
import { StatiqueService } from "@app/services/statique.service";
import { EbUser } from "@app/classes/user";
import { OrderCreationFormStepBaseComponent } from "../../core/order-creation-from-step-base";
import { EbIncoterm } from "@app/classes/EbIncoterm";
import { EbTypeFlux } from "@app/classes/EbTypeFlux";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { ConfigIncotermService } from "@app/services/config-incoterm.service";
import { ConfigPslCompanyService } from "@app/services/config-psl-company.service";
import { EbCostCenter } from "@app/classes/costCenter";

@Component({
	selector: "app-order-step-reference",
	templateUrl: "./order-step-reference.component.html",
	styleUrls: ["./order-step-reference.component.scss"],
})
export class OrderStepReferenceComponent extends OrderCreationFormStepBaseComponent
	implements OnInit {
	protected stepHasOptions(): boolean {
		return true;
	}
	searchCriteria: SearchCriteria = new SearchCriteria();
	@Input()
	selected: any;
	selectedStep: string;
	Step = OrderStep;
	@Input()
	userConnected: EbUser;
	categories: Array<EbCategorie>;
	get listCategorie() {
		return this.categories;
	}
	@Input()
	set listCategorie(listCategorie: Array<EbCategorie>) {
		if (listCategorie != null) {
			this.categories = listCategorie;
		}
	};

	_listFields: Array<IField>;
	get listFields() {
		return this._listFields;
	}
	@Input()
	set listFields(listFields: Array<IField>) {
		if (listFields != null) {
			this._listFields = listFields;
		}
	};
	toggleEditData: boolean = false;
	@Input()
	deletedCustomFields: Array<IField>;
	Modules = Modules;
	hasContributionAccess: boolean = false;
	criteria: SearchCriteriaOrder = new SearchCriteriaOrder();
	ordrStatus: number = 0;
	listOrderStatus: any[] = [];
	listModeTransport: any[];
	Statique = Statique;
	ModeOfTransport: ModeOfTransport;
	@Input()
	order: Order;

	// Categories was disabled, but to not loose all work (unfinished), we disabled them.
	enableCategories: boolean = true;
	@ViewChild("incoTermRef", { static: true })
	incoTermRef: any;

	selectedIncoterm: EbIncoterm = new EbIncoterm();
	@Input()
	requiredReadOnly: boolean = false;
	@Input()
	listIncoterms: Array<EbIncoterm> = new Array<EbIncoterm>();
	listIncotermsFiltered: Array<EbIncoterm> = new Array<EbIncoterm>();
	listTypeFluxDataLoaded = false;
	@Input()
	listTypeFlux: Array<EbTypeFlux> = new Array<EbTypeFlux>();
	selectedTypeFlux: EbTypeFlux = new EbTypeFlux();
	@Input()
	listCostCenter: Array<EbCostCenter>;

	modeTransport: number = 3;
	constructor(
		protected authenticationService: AuthenticationService,
		protected router: Router,
		protected cd: ChangeDetectorRef,
		protected etablissementService: EtablissementService,
		protected statiqueService: StatiqueService,
		protected translate?: TranslateService,
		protected configIncoterms?: ConfigIncotermService,
		protected companyPslService?: ConfigPslCompanyService
	) {
		super(authenticationService, router);
	}

	ngOnInit() {
		this.getListIncotermes();
		this.getListOrderStatus();
		this.getListModeTransport();
	}

	protected checkValidity(): boolean {
		return (
			this.order.customerOrderReference != null && this.order.customerOrderReference.length > 0
		);
	}
	getListModeTransport() {
		this.statiqueService.getListModeTransport().subscribe((res) => {
			this.listModeTransport = res;
			this.listModeTransport.sort(function(obj1, obj2) {
				return obj1.order - obj2.order;
			});
		});
	}

	getListOrderStatus() {
		this.statiqueService.getListOrderStatus().subscribe((res) => {
			if (res) {
				res.forEach((status) => {
					if (status.enabled) this.listOrderStatus.push(status);
				});
			}
			this.listOrderStatus.sort(function(obj1, obj2) {
				return obj1.order - obj2.order;
			});
		});
	}

	changeTransportMode() {
		if (this.order.meanOfTransport == ModeOfTransport.SEA) this.modeTransport = 2;
		else if (this.order.meanOfTransport == ModeOfTransport.AIR) this.modeTransport = 1;
		else this.modeTransport = 3;
	}

	selectLabel(label) {
		if (this.order.listLabels == null) this.order.listLabels = label.libelle;
		else this.order.listLabels += "," + label.libelle;
	}

	changeStatus() {
		this.ordrStatus = OrderStatus[this.order.orderStatus];
	}

	// select Incoterm when value change
	selectIncoterm(incoterm: EbIncoterm) {
		this.order.xEbIncotermNum = incoterm.ebIncotermNum;
		this.order.xEcIncotermLibelle = incoterm.libelle;
		this.cd.detectChanges();
	}

	getComment(complementaryInformations, order) {
		order.comcomplementaryInformationsment = complementaryInformations;
	}

	getListIncotermes() {
		if (this.order.xEbIncotermNum != null) {
			this.selectedIncoterm = new EbIncoterm();
			this.selectedIncoterm.ebIncotermNum = this.order.xEbIncotermNum;
			this.selectedIncoterm.libelle = this.order.xEcIncotermLibelle;
		} else {
			this.selectedIncoterm = new EbIncoterm();
		}
	}
	getCategorieFieldsAndCostCenter() {
		this.listFields = null;
		this.listCategorie = null;
		// this.listCostCenter = null;

		let criteria: SearchCriteria = new SearchCriteria();
		criteria.size = null;
		// criteria.roleBroker = true;
		// criteria.userService = ServiceType.BROKER;
		criteria.ebDelOrderNum = this.order.ebDelOrderNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;

		if (!this.order.ebDelOrderNum || this.toggleEditData) {
			if (
				this.toggleEditData &&
				this.order.xEbOwnerOfTheRequest &&
				this.order.xEbOwnerOfTheRequest.ebUserNum
			) {
				criteria.ebCompagnieNum = this.order.xEbOwnerOfTheRequest.ebCompagnie.ebCompagnieNum;
				criteria.ebEtablissementNum = this.order.xEbOwnerOfTheRequest.ebEtablissement.ebEtablissementNum;
				criteria.contact = true;
				criteria.ebUserNum = this.order.xEbOwnerOfTheRequest.ebUserNum;
			}
		} else {
			criteria.ebCompagnieNum = this.order.xEbEtablissement.ebCompagnie.ebCompagnieNum;
			criteria.ebEtablissementNum = this.order.xEbEtablissement.ebEtablissementNum;
			criteria.ebUserNum = this.order.xEbOwnerOfTheRequest.ebUserNum;
			criteria.contact = true;
		}

		if (!criteria.ebEtablissementNum || !criteria.ebCompagnieNum) {
			this.listFields = new Array<IField>();
			this.listCategorie = new Array<EbCategorie>();
			this.listCostCenter = new Array<EbCostCenter>();
			return;
		}

		this.etablissementService.getDataPreference(criteria).subscribe(async (res) => {
			this.listCostCenter =
				res && res.listCostCenter ? res.listCostCenter : new Array<EbCostCenter>();
			this.listCategorie = res && res.listCategorie ? res.listCategorie : new Array<EbCategorie>();
			this.listFields =
				res && res.customField && res.customField.fields
					? JSON.parse(res.customField.fields)
					: new Array<IField>();
			this.listFields = this.listFields || new Array<IField>();

			if (this.listCategorie && this.listCategorie.length > 0) {
				this.listCategorie.forEach((it) => {
					if (it.labels && it.labels.length > 0) {
						it.labels.sort((a, b) => {
							return a.libelle.localeCompare(b.libelle);
						});
					}
				});

				this.listCategorie = this.listCategorie.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}
		});
	}
}
