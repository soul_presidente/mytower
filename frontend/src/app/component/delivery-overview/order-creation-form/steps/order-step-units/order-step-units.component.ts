import { AuthenticationService } from "@app/services/authentication.service";
import { Router } from "@angular/router";
import {
	Component,
	OnInit,
	Input,
	Output,
	EventEmitter,
	ViewChild,
	OnChanges,
	SimpleChanges,
} from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { SearchCriteria } from "@app/utils/searchCriteria";
import {
	OrderStep,
	PageUnitOrder,
	GenericTableScreen,
	TypeContainer,
	Modules,
} from "@app/utils/enumeration";
import { DataTableConfig } from "@app/utils/dataTableConfig";
import { Statique } from "@app/utils/statique";
import { ArticleDims } from "@app/classes/articleDims";
import { OrderLine } from "@app/classes/ebDelOrderLine";
import { DeliveryLine } from "@app/classes/ebDelLivraisonLine";
import { EcCountry } from "@app/classes/country";
import { EbTypeUnit } from "@app/classes/typeUnit";
import { GenericTableService } from "@app/services/generic-table.service";
import { PricingService } from "@app/services/pricing.service";
import { EbCategorie } from "@app/classes/categorie";
import { IField, CustomField } from "@app/classes/customField";
import { EtablissementService } from "@app/services/etablissement.service";
import { Order } from "@app/classes/ebDelOrder";
import { OrderCreationFormStepBaseComponent } from "../../core/order-creation-from-step-base";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { TextInputGenericCell } from "@app/shared/generic-cell/commons/text-input.generic-cell";
import { KeyValuePipe } from "@angular/common";
import { SelectGenericCell } from "@app/shared/generic-cell/commons/select.generic-cell";
import { CountryPickerGenericCell } from "@app/shared/generic-cell/commons/country-picker.generic-cell";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { SearchCriteriaOrder } from "@app/utils/searchCriteriaOrder";
import { TreeNode } from "primeng/api";
import { EcCurrency } from "@app/classes/currency";
import { StatiqueService } from "@app/services/statique.service";
import { DeliveryOverviewService } from "@app/services/delivery-overview.service";

@Component({
	selector: "app-order-step-units",
	templateUrl: "./order-step-units.component.html",
	styleUrls: ["./order-step-units.component.scss"],
	providers: [KeyValuePipe],
})
export class OrderStepUnitsComponent extends OrderCreationFormStepBaseComponent
	implements OnInit, OnChanges {
	protected stepHasOptions(): boolean {
		return true;
	}
	@Input()
	selected: any;

	selectedStep: string;
	Step = OrderStep;
	@Input()
	order: Order = new Order();
	@Output()
	numberUnitChange: EventEmitter<Order> = new EventEmitter<Order>();

	@Input()
	deliveryLines?: Array<DeliveryLine>;

	@Input()
	listCountry: Array<EcCountry>;

	@Input()
	listTypesUnit: Array<EbTypeUnit>;

	@Input()
	page?: number;

	@Input()
	userCompagnieNum?: number;

	@Input()
	modeTransport?: number;

	PageUnitOrder = PageUnitOrder;

	@Output()
	onUnitsChange = new EventEmitter<any>();

	@Input()
	listCustomsFields = new Array<IField>();

	Statique = Statique;

	listArticleDims: Array<ArticleDims> = new Array<ArticleDims>();

	// GenericTable
	GenericTableScreen = GenericTableScreen;
	searchCriteriaTable = new SearchCriteriaOrder();

	genericTableReady = false;
	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	filteredListOrderLines: Array<OrderLine>;
	pageDatas: Array<OrderLine>;
	numberPerPage: number = 5;
	totalOrderLineRecords: number;
	@Input()
	orderLines: Array<OrderLine>;
	searchItem: any;
	treeData: TreeNode[] = [];

	extracting: boolean;
	typeContener: number = 0;

	@Input()
	listFields: Array<IField>;
	@Output()
	listFieldsEmitter = new EventEmitter<Array<IField>>();

	currencyList: Array<EcCurrency>;

	constructor(
		protected authenticationService: AuthenticationService,
		protected router: Router,
		protected genericTableService: GenericTableService,
		protected pricingService: PricingService,
		protected etablissementService: EtablissementService,
		protected keyValuePipe: KeyValuePipe,
		protected statiqueService: StatiqueService,
		protected deliveryOverviewService: DeliveryOverviewService,
		protected translate?: TranslateService
	) {
		super(authenticationService, router);
	}

	ngOnInit() {
		this.initGenericTable();
		this.getTotalOfNumberFields();
	}

	async loadCurrencyList() {
		this.currencyList = await this.statiqueService.getListEcCurrency().toPromise();
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["order"]) {
			this.getTotalOfNumberFields();
			this.evaluateVehiculeEstimation();
		}

		if (changes["selected"]) {
			if (this.selected) {
				if (
					!this.order.ebDelOrderNum &&
					(this.order.orderLines == null || this.order.orderLines.length === 0)
				) {
					this.addOrderLine();
				}

				if (this.order.ebDelOrderNum) {
					this.getTotalOfNumberFields();
					this.evaluateVehiculeEstimation();
					this.onUnitsChange.emit();
					this.initOrderLines();
				}
			}
		}
	}

	paginate(event) {
		this.numberPerPage = event.rows;
		this.getDataToPage(event.page + 1);
	}

	getDataToPage(page: number) {
		this.pageDatas = [];
		let from = (page - 1) * this.numberPerPage,
			to = page * this.numberPerPage - 1;
		if (to >= this.filteredListOrderLines.length - 1) to = this.filteredListOrderLines.length - 1;
		for (let i = from; i <= to; i++) {
			this.pageDatas.push(this.filteredListOrderLines[i]);
		}
		if (to <= 0) this.pageDatas = this.filteredListOrderLines;
	}

	evaluateVehiculeEstimation() {
		let listItems: any[] = [];
		if (this.page == PageUnitOrder.CREATE_DELIVERY) {
			this.deliveryLines.forEach((line) => {
				if (line.xEbtypeOfUnit) listItems.push(line);
			});
		} else {
			if (this.order.orderLines) {
				this.order.orderLines.forEach((line) => {
					if (line.xEbtypeOfUnit) listItems.push(line);
				});
			}
		}
		this.listArticleDims = listItems.map((it) => {
			let typeUnit = it.xEbtypeOfUnit;
			let qte = null;
			if (this.page == PageUnitOrder.CREATE_DELIVERY) qte = it.quantityAllocated;
			else qte = it.quantityOrdered;
			return ArticleDims.create(
				typeUnit.width,
				typeUnit.height,
				typeUnit.length,
				typeUnit.weight,
				qte
			);
		});
	}

	changeTypeUnit(unit) {
		if (unit) this.evaluateVehiculeEstimation();
	}
	initOrderLines() {
		this.filteredListOrderLines = this.order.orderLines || [];
		this.totalOrderLineRecords = this.filteredListOrderLines.length;
		this.getDataToPage(1);
	}

	searchElement(value: string) {
		this.deliveryOverviewService.getOrderLines(this.order.ebDelOrderNum, value).subscribe((res: Array<OrderLine>) => {
			this.pageDatas = res;
		});
	}

	changeQtyBooked() {
		this.evaluateVehiculeEstimation();
		this.getTotalOfNumberFields();
	}

	addOrderLine() {
		let orderLine = new OrderLine();
		orderLine.quantityOrdered = 1;
		orderLine.listCustomsFields = Statique.cloneListObject(this.listCustomsFields, CustomField);
		this.order.orderLines.push(orderLine);
		this.changeQtyBooked();
		this.onUnitsChange.emit();
		this.initOrderLines();
	}

	deleteOrderLine(orderLine: OrderLine) {
		let index = this.order.orderLines.indexOf(orderLine);
		if (this.order.orderLines.length == 1) {
			this.order.orderLines[index] = new OrderLine();
		} else {
			this.order.orderLines.splice(index, 1);
		}
		this.onUnitsChange.emit();
		this.changeQtyBooked();
		this.evaluateVehiculeEstimation();
		this.initOrderLines();
	}

	changeField() {
		this.getTotalOfNumberFields();
	}

	getTotalOfNumberFields() {
		let totalQtyReserver = 0;
		this.order.nbUnit = 0;

		if (this.order.orderLines) {
			this.order.orderLines.forEach((unit) => {
				if (this.page == PageUnitOrder.CREATE_ORDER) {
					unit.quantityPending = unit.quantityOrdered;
				}
				totalQtyReserver += unit.quantityOrdered ? Number(unit.quantityOrdered) : 0;
				this.order.totalQuantityOrdered = totalQtyReserver;
				this.order.nbUnit++;
			});
			this.numberUnitChange.emit(this.order);
		}
	}

	changeQtyAllocated() {
		this.changeField();
		if (this.page == PageUnitOrder.CREATE_DELIVERY) {
			this.evaluateVehiculeEstimation();
		}
	}
	async getCountries() {
		this.listCountry = await this.pricingService.listPays().toPromise();
	}

	async getTypesPallet() {
		let searchCriteria = new SearchCriteriaOrder();
		searchCriteria.ebDelOrderNum = this.order.ebDelOrderNum;
		let res: any;
		res = await this.genericTableService
			.getDatas(Statique.controllerTypeUnit + "/list-type-unit", searchCriteria)
			.toPromise();
		this.listTypesUnit = res.data;
	}

	protected checkValidity(): boolean {
		return true;
	}

	async initGenericTable() {
		await this.getCountries();
		await this.getTypesPallet();
		await this.loadCurrencyList();
		this.initGenericTableCore();
		this.initGenericTableCols();
		this.genericTableReady = true;
	}

	initGenericTableCore() {
		this.module = Modules.ORDER_LINE;
		this.searchCriteriaTable.module = this.module;
		this.dataInfos.dataKey = "ebDelOrderLineNum";
		this.dataInfos.dataSourceType = GenericTableInfos.DataSourceType.Local;
		this.dataInfos.dataType = OrderLine;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = false;
		this.dataInfos.numberDatasPerPage = 50;
		this.dataInfos.showCollapsibleBtn = false;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showExportBtn = true;

		this.searchCriteriaTable.pageNumber = 1;
		this.searchCriteriaTable.connectedUserNum = this.userConnected.ebUserNum;
		this.searchCriteriaTable.ebUserNum = this.userConnected.ebUserNum;
		this.searchCriteriaTable.ebDelOrderNum = this.order.ebDelOrderNum;

		this.dataInfos.lineActions = [
			{
				icon: "fa fa-trash",
				onClick: (model: OrderLine) => {
					this.deleteOrderLine(model);
				},
				enableCondition: (model: OrderLine) => {
					return !this.readOnly || !model.ebDelOrderLineNum;
				},
			},
		];
	}
	initGenericTableCols() {
		let cols = new Array<GenericTableInfos.Col>();
		cols.push(
			{
				field: "itemNumber",
				translateCode: "ORDER.ITEM_NUMBER",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "itemNumber",
					showOnHover: true,
					disabledOn: this.disabledOnReadOnly,
				}),
			},
			{
				field: "itemName",
				translateCode: "CHANEL_PB_DELIVERY.ARTICLE_NAME",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "itemName",
					showOnHover: true,
					disabledOn: this.disabledOnReadOnly,
				}),
			},
			{
				field: "ebTypeUnitNum",
				translateCode: "PRICING_BOOKING.TYPE_OF_UNIT",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: SelectGenericCell,
				genericCellParams: new SelectGenericCell.Params({
					field: "parentUnitNumber",
					values: this.listTypesUnit,
					itemValueField: "ebTypeUnitNum",
					itemTextField: "libelle",
					compareWith: Statique.compareTypeUnit,
					showOnHover: true,
					disabledOn: this.disabledOnReadOnly,
				}),
				genericCellHandler: (
					key: string,
					value: any,
					model: OrderLine,
					context: OrderStepUnitsComponent
				) => {
					if (key === "selection-change") {
						context.changeTypeUnit(model.xEbtypeOfUnit);
					}
				},
			},
			{
				field: "quantityOrdered",
				translateCode: "CHANEL_PB_ORDER.ORDERED_QTY",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "quantityOrdered",
					showOnHover: true,
					inputType: "number",
					disabledOn: (model: OrderLine) => {
						return (
							model.serialNumber != null && model.serialNumber.length > 0 && this.disabledOnReadOnly
						);
					},
					getTitle: (model: OrderLine) => {
						if (model.serialNumber != null && model.serialNumber.length > 0)
							return this.translate.instant("CHANEL_PB_ORDER.ALERT_SERIALIZED_ARTICLE_FOR_QT");
						return "";
					},
				}),
				genericCellHandler: (
					key: string,
					value: any,
					model: OrderLine,
					context: OrderStepUnitsComponent
				) => {
					if (key === "input-event" && value === "blur") {
						context.changeQtyBooked();
					}
				},
			}
		);
		if (this.readOnly) {
			cols.push(
				{
					field: "quantityPlannable",
					translateCode: "CHANEL_PB_ORDER.PLANNABLE_QUANTITY",
				},
				{
					field: "quantityPlanned",
					translateCode: "CHANEL_PB_ORDER.PLANNED_QUANTITY",
				},
				{
					field: "quantityPending",
					translateCode: "CHANEL_PB_ORDER.PENDING_QUANTITY",
				},
				{
					field: "quantityConfirmed",
					translateCode: "CHANEL_PB_ORDER.CONFIRMED_QUANTITY",
				},
				{
					field: "quantityShipped",
					translateCode: "CHANEL_PB_ORDER.SHIPPED_QUANTITY",
				},
				{
					field: "quantityDelivered",
					translateCode: "CHANEL_PB_ORDER.DELIVERED_QUANTITY",
				},
				{
					field: "quantityCanceled",
					translateCode: "CHANEL_PB_ORDER.CANCELED_QUANTITY",
				}
			);
		}
		cols.push(
			{
				field: "partNumber",
				translateCode: "ORDER.PART_NUMBER",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "partNumber",
					showOnHover: true,
					disabledOn: this.disabledOnReadOnly,
				}),
			},
			{
				field: "serialNumber",
				translateCode: "CHANEL_PB_DELIVERY.SERIAL_NUMBER",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "serialNumber",
					showOnHover: true,
					disabledOn: this.disabledOnReadOnly,
					keyupOn: (evt, model: OrderLine) => {
						if (model.serialNumber && model.serialNumber.length > 0) {
							model.serialized = true;
							model.quantityOrdered = 1;
						} else {
							model.serialized = null;
						}
					},
				}),
			},
			{
				field: "hsCode",
				translateCode: "CUSTOM.HSCODE",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "hsCode",
					showOnHover: true,
					disabledOn: this.disabledOnReadOnly,
				}),
			},
			{
				field: "weight",
				translateCode: "TYPE_UNIT.WEIGHT",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "weight",
					showOnHover: true,
					inputType: "number",
					disabledOn: this.disabledOnReadOnly,
				}),
			},
			{
				field: "height",
				translateCode: "TYPE_UNIT.HEIGHT",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "height",
					showOnHover: true,
					inputType: "number",
					disabledOn: this.disabledOnReadOnly,
				}),
			},
			{
				field: "width",
				translateCode: "TYPE_UNIT.WIDTH",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "width",
					showOnHover: true,
					inputType: "number",
					disabledOn: this.disabledOnReadOnly,
				}),
			},
			{
				field: "length",
				translateCode: "TYPE_UNIT.LENGTH",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "length",
					showOnHover: true,
					inputType: "number",
					disabledOn: this.disabledOnReadOnly,
				}),
			},
			{
				field: "comment",
				translateCode: "QUALITY_MANAGEMENT.COMMENT",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "comment",
					showOnHover: true,
					disabledOn: this.disabledOnReadOnly,
				}),
			},
			{
				field: "specification",
				translateCode: "CHANEL_PB_DELIVERY.SPECIFICATION",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "specification",
					showOnHover: true,
					disabledOn: this.disabledOnReadOnly,
				}),
			},
			{
				field: "serialized",
				translateCode: "CHANEL_PB_ORDER.SERIALIZED",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: SelectGenericCell,
				genericCellParams: new SelectGenericCell.Params({
					field: "serialized",
					itemValueField: "key",
					itemTextField: "value",
					values: Statique.yesOrNoWithBooleanKey,
					compareWith: Statique.compareByField,
					showOnHover: true,
					disabledOn: (model: OrderLine) => {
						return model.serialNumber != null && model.serialNumber.length > 0;
					},
					getTitle: (model: OrderLine) => {
						if (model.serialNumber != null && model.serialNumber.length > 0)
							return this.translate.instant("CHANEL_PB_ORDER.ALERT_SERIALIZED_ARTICLE_FOR_SER");
						return "";
					},
				}),
			},
			{
				field: "price",
				translateCode: "CHANEL_PB_ORDER.PRICE",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "price",
					inputType: "number",
					showOnHover: true,
					disabledOn: this.disabledOnReadOnly,
				}),
			},
			{
				field: "currency",
				translateCode: "CHANEL_PB_ORDER.CURRENCY",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: SelectGenericCell,
				genericCellParams: new SelectGenericCell.Params({
					field: "currency",
					itemValueField: "ecCurrencyNum",
					itemTextField: "code",
					values: this.currencyList,
					showOnHover: true,
					compareWith: Statique.compareByField,
					disabledOn: this.disabledOnReadOnly,
					requiredOn: (model: OrderLine) => {
						return model.price != null && model.price != undefined;
					},
				}),
				
			},
			{
				field: "eccn",
				translateCode: "CHANEL_PB_ORDER.ECCN",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "eccn",
					showOnHover: true,
					disabledOn: this.disabledOnReadOnly,
				}),
			}
		);

		// START : Demo mode fake field
		if(this.acls[this.ACL.Rule.Demo_Features_Enable]) {
			cols.push({
				field: "value",
				header: "Valeur",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "value",
					showOnHover: true,
					inputType: "number",
				}),
			});

			cols.push({
				field: "op",
				header: "Origine préférentielle",
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "op",
					showOnHover: true,
					inputType: "text",
				}),
			});
		}
		// END : Demo mode fake field

		this.dataInfos.cols = cols;
		this.dataInfos.selectedCols = this.dataInfos.cols;
	}

	onListFieldLoaded(listField: Array<IField>) {
		this.listFields = listField;
		this.listFieldsEmitter.emit(listField);
	}

	disabledOnReadOnly = function(model: any): boolean {
		return this.reanOnly;
	};
}
