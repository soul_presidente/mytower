import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ConnectedUserComponent } from "../../../../../shared/connectedUserComponent";
import { OrderStep, PageUnitOrder } from "@app/utils/enumeration";
import { Order } from "@app/classes/ebDelOrder";
import { EbCategorie } from "@app/classes/categorie";
import { IField } from "@app/classes/customField";

@Component({
	selector: "app-order-overview",
	templateUrl: "./order-overview.component.html",
	styleUrls: ["./order-overview.component.scss"],
})
export class OrderOverviewComponent extends ConnectedUserComponent implements OnInit {
	stepsAccordionIndexes = new Map<number, string>();

	Step = OrderStep;

	// Categories was disabled, but to not loose all work (unfinished), we disabled them.
	enableCategories: boolean = false;

	@Input()
	order: Order;

	activeAccordionIndex: any;

	@Input()
	hideStepArrow: boolean = false;

	@Input()
	canCollapse: boolean = true;

	@Input()
	expandAllAtStart: boolean = false;

	@Input()
	creationMode: boolean = false;

	@Input()
	page?: number;

	@Output()
	onStepSelected: EventEmitter<string> = new EventEmitter<string>();

	@Input()
	readOnly: boolean = true;

	listCategories: Array<EbCategorie> = new Array<EbCategorie>();

	customsFields: Array<IField> = new Array<IField>();

	Syntaxe2Point_Fr_En = localStorage.getItem("currentLang") == "en" ? ":" : " :";

	constructor() {
		super();
	}

	ngOnInit() {
		this.stepsAccordionIndexes.set(0, OrderStep.ORDER);
		this.stepsAccordionIndexes.set(1, OrderStep.REFERENCE);
		this.stepsAccordionIndexes.set(2, OrderStep.UNITS);
		this.stepsAccordionIndexes.set(3, OrderStep.DELIVERY);
		if (this.expandAllAtStart && this.readOnly) {
			this.activeAccordionIndex = "0,1,2,3";
		}
		this.getListCategories();
		this.getcustomsFields();
	}

	selectStep(stepCode: string) {
		this.onStepSelected.emit(stepCode);
	}

	getListCategories() {
		if (!this.order.listCategories) return;
		let nbre = this.order.listCategories.length;
		if (nbre == 1) {
			this.listCategories.push(this.order.listCategories[0]);
		} else if (nbre > 1) {
			this.listCategories = this.order.listCategories;
		}
	}

	getcustomsFields() {
		if (!this.order.listCustomsFields) return;
		this.customsFields = [...this.customsFields, ...this.order.listCustomsFields];
	}
}
