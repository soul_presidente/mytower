import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbOrderLinePlanificationDTO } from "@app/classes/EbOrderLinePlanificationDTO";
import { DeliveryOverviewService } from "@app/services/delivery-overview.service";
import { Order } from "@app/classes/ebDelOrder";
import { SearchCriteriaOrder } from "@app/utils/searchCriteriaOrder";
import { ActivatedRoute, Router } from "@angular/router";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { TextInputGenericCell } from "@app/shared/generic-cell/commons/text-input.generic-cell";
import { Statique } from "@app/utils/statique";
import { GenericTableScreen, OrderStatus, Modules } from "@app/utils/enumeration";
import { OrderLine } from "@app/classes/ebDelOrderLine";
import { TreeNode, MessageService } from "primeng/api";
import { ModalService } from "@app/shared/modal/modal.service";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { EbOrderPlanificationDTO } from "@app/classes/EbOrderPlanificationDTO";
import { TranslateService } from "@ngx-translate/core";
import { DeliveryLine } from "@app/classes/ebDelLivraisonLine";
import { OrderVisualisationComponent } from "@app/component/delivery-overview/order-visualisation/order-visualisation.component";

@Component({
	selector: "app-order-plannable",
	templateUrl: "./order-plannable.component.html",
	styleUrls: ["./order-plannable.component.scss"],
})
export class OrderPlannableComponent extends ConnectedUserComponent implements OnInit {
	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	searchInput: SearchCriteriaOrder = new SearchCriteriaOrder();
	searchInputPlannification: SearchCriteriaOrder = new SearchCriteriaOrder();
	dataInfosPlannification: GenericTableInfos = new GenericTableInfos(this);

	GenericTableScreen = GenericTableScreen;
	genericTableReady = false;
	public module: number;
	addAction: boolean;
	@Input()
	isPlanningPlanify: boolean = true;
	numberPerPage: number = 5;
	totalOrderLineRecords: number;
	treeData: TreeNode[] = [];
	@Input()
	planificationRequest = new EbOrderPlanificationDTO();
	filteredListOrderLines: Array<EbOrderLinePlanificationDTO> = new Array<
		EbOrderLinePlanificationDTO
	>();
	listToPlan: Array<EbOrderLinePlanificationDTO> = new Array<EbOrderLinePlanificationDTO>();
	@Input()
	order: Order;
	@Output() onModePlanifySelected: EventEmitter<boolean> = new EventEmitter<boolean>();
	@Output()
	numberUnitChange: EventEmitter<Order> = new EventEmitter<Order>();
	@Input()
	deliveryLine: Array<DeliveryLine> = new Array<DeliveryLine>();
	planifyOrderLienList: Array<number> = new Array<number>();
	constructor(
		private deliveryOverviewService: DeliveryOverviewService,
		protected activatedRoute: ActivatedRoute,
		protected router: Router,
		private translate: TranslateService,
		private modalService?: ModalService,
		protected messageService?: MessageService,
		private orderVisualisationComponent?: OrderVisualisationComponent
	) {
		super();
	}

	ngOnInit() {
		this.initGenericTable();
		if (
			this.planificationRequest != null &&
			this.planificationRequest.ebDelLivraisonNum != null &&
			this.deliveryLine != null &&
			this.isPlanningPlanify
		) {
			this.deliveryLine.map((element) => {
				this.deliveryOverviewService
					.getOrderLineByDeliveryLine(element.ebDelLivraisonLineNum)
					.subscribe((resultOrderline) => {
						var orderLineDTO: EbOrderLinePlanificationDTO = new EbOrderLinePlanificationDTO();
						orderLineDTO.orderLine = resultOrderline;
						orderLineDTO.ebDelOrderLineNum = resultOrderline.ebDelOrderLineNum;
						orderLineDTO.itemNum = resultOrderline.itemNumber;
						orderLineDTO.itemName = resultOrderline.itemName;
						orderLineDTO.ebDelLivraisonLineNum = element.ebDelLivraisonLineNum;
						orderLineDTO.orderLine.quantityPlannable =
							resultOrderline.quantityPlannable + element.quantity;
						orderLineDTO.quantityToPlan = element.quantity;
						this.listToPlan.push(orderLineDTO);
						this.planifyOrderLienList.push(resultOrderline.ebDelOrderLineNum);
					});
			});
		}
		this.initGenericTablePlannification();
	}

	async initGenericTable() {
		this.initGenericTableCore();
		this.initGenericTableCols();
		this.genericTableReady = true;
	}

	initGenericTableCore() {
		this.module = GenericTableScreen.ORDER_UNITS;
		this.searchInput.pageNumber = 1;
		this.searchInput.size = 5;
		this.searchInput.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchInput.connectedUserNum = this.userConnected.ebUserNum;
		this.searchInput.ebDelOrderNum = +this.activatedRoute.snapshot.params.id;
		this.dataInfos.dataKey = "ebDelOrderLineNum";
		this.dataInfos.dataSourceType = GenericTableInfos.DataSourceType.Url;
		this.dataInfos.dataType = OrderLine;
		this.dataInfos.showDetails = false;
		this.dataInfos.showSubTable = false;
		this.dataInfos.paginator = true;
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.showCollapsibleBtn = false;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showSearchBtn = true;
		if (this.searchInput.ebDelOrderNum) {
			this.dataInfos.dataLink =
				Statique.controllerOrderOverView + "/get-order-lines-planifiable-tableau";
			this.dataInfos.cols = this.initGenericTableCols();
		}
		this.dataInfos.lineActions = [
			{
				icon: "fa fa-plus",
				onClick: (data: OrderLine) => {
					let elementToPlan: EbOrderLinePlanificationDTO = new EbOrderLinePlanificationDTO();
					elementToPlan.orderLine = data;
					elementToPlan.ebDelOrderLineNum = data.ebDelOrderLineNum;
					elementToPlan.itemNum = data.itemNumber;
					elementToPlan.itemName = data.itemName;
					elementToPlan.quantityToPlan = data.quantityPlannable;
					this.listToPlan.unshift(elementToPlan);
					this.initOrderLines();
				},
				enableCondition: (row: OrderLine) => {
					this.addAction = false;
					this.listToPlan.forEach((element) => {
						if (row.ebDelOrderLineNum == element.ebDelOrderLineNum) this.addAction = true;
					});
					return !this.addAction;
				},
			},
		];
	}
	initGenericTableCols(): any {
		let cols = new Array<GenericTableInfos.Col>();
		cols.push(
			{
				field: "itemNumber",
				translateCode: "ORDER.ITEM_NUMBER",
			},
			{
				field: "itemName",
				translateCode: "CHANEL_PB_DELIVERY.ARTICLE_NAME",
			},
			{
				field: "partNumber",
				translateCode: "ORDER.PART_NUMBER",
			},
			{
				field: "quantityPlannable",
				translateCode: "CHANEL_PB_ORDER.PLANNABLE_QUANTITY",
			},
			{
				field: "serialNumber",
				translateCode: "CHANEL_PB_DELIVERY.SERIAL_NUMBER",
			},
			{
				field: "hsCode",
				translateCode: "CUSTOM.HSCODE",
			},
			{
				field: "weight",
				translateCode: "TYPE_UNIT.WEIGHT",
			},
			{
				field: "height",
				translateCode: "TYPE_UNIT.HEIGHT",
			},
			{
				field: "width",
				translateCode: "TYPE_UNIT.WIDTH",
			},
			{
				field: "comment",
				translateCode: "QUALITY_MANAGEMENT.COMMENT",
			},
			{
				field: "specification",
				translateCode: "CHANEL_PB_DELIVERY.SPECIFICATION",
			},
			{
				field: "quantityPlanned",
				translateCode: "CHANEL_PB_ORDER.PICKED_QUANTITY",
			},
			{ field: "quantityPrepared", translateCode: "CHANEL_PB_ORDER.PREPARED_QUANTITY" },
			{
				field: "quantityPacked",
				translateCode: "CHANEL_PB_ORDER.PACKED_QUANTITY",
			},
			{
				field: "quantityShipped",
				translateCode: "CHANEL_PB_ORDER.SHIPPED_QUANTITY",
			},
			{
				field: "quantityDelivered",
				translateCode: "CHANEL_PB_ORDER.DELIVERED_QUANTITY",
			},
			{
				field: "quantityOrdered",
				translateCode: "CHANEL_PB_ORDER.ORDERED_QTY",
			}
		);
		return cols;
	}

	async initGenericTablePlannification() {
		this.initGenericTableCorePlannification();
		this.initGenericTableColsPlannification();
	}
	initGenericTableCorePlannification() {
		this.module = Modules.DELIVERY_MANAGEMENT;
		this.searchInputPlannification.pageNumber = 1;
		this.searchInputPlannification.size = 5;
		this.searchInputPlannification.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.searchInputPlannification.connectedUserNum = this.userConnected.ebUserNum;
		this.searchInputPlannification.ebDelOrderNum = +this.activatedRoute.snapshot.params.id;
		this.dataInfosPlannification.dataKey = "ebDelOrderLineNum";
		this.dataInfosPlannification.dataSourceType = GenericTableInfos.DataSourceType.Local;
		this.dataInfosPlannification.dataType = EbOrderLinePlanificationDTO;
		this.dataInfosPlannification.showDetails = false;
		this.dataInfosPlannification.showSubTable = false;
		this.dataInfosPlannification.paginator = false;
		this.dataInfosPlannification.numberDatasPerPage = 5;
		this.dataInfosPlannification.showCollapsibleBtn = false;
		this.dataInfosPlannification.showColConfigBtn = true;
		this.dataInfosPlannification.showSaveBtn = true;
		this.dataInfosPlannification.showSearchBtn = true;
		this.dataInfosPlannification.cols = this.initGenericTableColsPlannification();
		this.dataInfosPlannification.selectedCols = this.dataInfosPlannification.cols;
		this.dataInfosPlannification.lineActions = [
			{
				icon: "fa fa-minus ",
				onClick: (data: EbOrderLinePlanificationDTO) => {
					let arrayDeletePlannification: Array<EbOrderLinePlanificationDTO> = Array<
						EbOrderLinePlanificationDTO
					>();
					this.listToPlan.forEach((element) => {
						if (element.ebDelOrderLineNum != data.ebDelOrderLineNum) {
							arrayDeletePlannification.push(element);
						} else if (element.ebDelLivraisonLineNum != null) {
							this.deliveryOverviewService
								.deleteDeliveryLineById(element.ebDelLivraisonLineNum)
								.subscribe();
						}
					});
					this.listToPlan = arrayDeletePlannification;
					this.initOrderLines();
				},
			},
		];
	}
	initGenericTableColsPlannification(): any {
		let cols = new Array<GenericTableInfos.Col>();
		cols.push(
			{
				field: "itemNum",
				translateCode: "ORDER.ITEM_NUMBER",
			},
			{
				field: "itemName",
				translateCode: "CHANEL_PB_DELIVERY.ARTICLE_NAME",
			},
			{
				field: "refTransport",
				translateCode: "CHANEL_PB_DELIVERY.TRANSPORT_REF",
			},
			{
				field: "quantityToPlan",
				translateCode: "ORDER.QUANTITY",
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: TextInputGenericCell,
				genericCellParams: new TextInputGenericCell.Params({
					field: "quantityToPlan",
					showOnHover: true,
					inputType: "number",
					maxOn(model: EbOrderLinePlanificationDTO) {
						return model.orderLine.quantityPlannable;
					},
					min: 1,
					keyupOn(event, model: EbOrderLinePlanificationDTO) {
						if (Number(event.target.value) >= model.orderLine.quantityPlannable)
							event.target.value = model.orderLine.quantityPlannable;
						return Number(event.target.value);
					},
				}),
			}
		);
		this.dataInfos.selectedCols = this.dataInfos.cols;

		return cols;
	}

	keyCode(event): number {
		var x: number = event.keyCode;
		return x;
	}
	paginate(event) {
		this.numberPerPage = event.rows;
		this.getDataToPage(event.page + 1);
	}
	getDataToPage(page: number) {
		this.listToPlan = [];
		let from = (page - 1) * this.numberPerPage,
			to = page * this.numberPerPage - 1;
		if (to >= this.filteredListOrderLines.length - 1) to = this.filteredListOrderLines.length - 1;
		for (let i = from; i <= to; i++) {
			this.listToPlan.push(this.filteredListOrderLines[i]);
		}
		if (to <= 0) this.listToPlan = this.filteredListOrderLines;
	}
	initOrderLines() {
		this.filteredListOrderLines = this.listToPlan || [];
		this.totalOrderLineRecords = this.filteredListOrderLines.length;
		this.treeData;
		this.getDataToPage(1);
	}
	cancelPlanify() {
		let that = this;
		let message = "CHANEL_PB_DELIVERY.MESSAGE_CANCEL_DELIVERY";
		let messageHead = "CHANEL_PB_DELIVERY.MESSAGE_HEAD_CANCEL_DELIVERY";
		this.modalService.confirm(messageHead, message, function() {
			that.listToPlan = Array<EbOrderLinePlanificationDTO>();
		});
	}
	planify() {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.planificationRequest.ebDelOrderNum = this.order.ebDelOrderNum;
		this.planificationRequest.listToPlan = this.listToPlan;
		this.deliveryOverviewService.savePlanifyOrder(this.planificationRequest).subscribe((result) => {
			requestProcessing.afterGetResponse(event);
			this.onModePlanifySelected.emit(false);
			this.numberUnitChange.emit(this.order);
			if (this.order.orderStatus == "PENDING") this.order.orderStatus = OrderStatus.NOTDLV;
			this.orderVisualisationComponent.ngOnInit();
			this.messageService.add({
				severity: "success",
				summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
				detail: this.translate.instant(
					"GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL_PLANIFICATION"
				),
			});
		});
	}
}
