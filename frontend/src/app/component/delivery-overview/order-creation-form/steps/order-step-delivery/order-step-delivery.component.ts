import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from "@angular/core";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { Delivery } from "@app/classes/ebDelLivraison";
import { SearchCriteriaDelivery } from "@app/utils/SearchCritereaDelivery";
import { EbUser } from "@app/classes/user";
import { EbParty } from "@app/classes/party";
import { Statique } from "@app/utils/statique";
import { TranslateService } from "@ngx-translate/core";
import { GenericTableScreen, Modules, OrderStep, OrderStatus } from "@app/utils/enumeration";
import { ActivatedRoute, Router } from "@angular/router";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { MessageService } from "primeng/api";
import { DeliveryOverviewService } from "@app/services/delivery-overview.service";
import { GenericTableComponent } from "@app/shared/generic-table/generic-table.component";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { Order } from "@app/classes/ebDelOrder";
import { EbOrderPlanificationDTO } from "@app/classes/EbOrderPlanificationDTO";
import { DeliveryLine } from "@app/classes/ebDelLivraisonLine";
import { OrderCreationFormStepBaseComponent } from "../../core/order-creation-from-step-base";
import { OrderStepUnitsComponent } from "../order-step-units/order-step-units.component";
import { StatusDelivery } from "@app/classes/StatusDeliveryEnum";
@Component({
	selector: "app-order-step-delivery",
	templateUrl: "./order-step-delivery.component.html",
	styleUrls: ["./order-step-delivery.component.scss"],
})
export class OrderStepDeliveryComponent implements OnInit {
	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	genericTableReady = false;

	searchInput: SearchCriteriaDelivery = new SearchCriteriaDelivery();
	GenericTableScreen = GenericTableScreen;
	deliveryToDeleteNum: number;
	modalConfirmDeleteVisibility: boolean = false;
	modePlanify: boolean = false;
	isPlanningPlanify: boolean = false;
	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableComponent;
	Modules = Modules;
	planificationRequest = new EbOrderPlanificationDTO();
	deliveryLine: Array<DeliveryLine> = new Array<DeliveryLine>();
	stepsComponents = new Map<string, OrderCreationFormStepBaseComponent>();
	@ViewChild(OrderStepUnitsComponent, { static: true })
	orderStepUnits: OrderStepUnitsComponent;
	Step = OrderStep;
	selectedStep: string;
	@Input()
	readOnly: boolean;
	@Output()
	onStepSelected: EventEmitter<string> = new EventEmitter<string>();
	stepsAccordionIndexes = new Map<number, string>();
	@Output()
	orderChange: EventEmitter<Order> = new EventEmitter<Order>();
	@Input()
	order: Order;
	@Input()
	expandAllAtStart: boolean = false;
	activeAccordionIndex: any;

	constructor(
		private translate: TranslateService,
		protected activatedRoute: ActivatedRoute,
		protected deliveryOverviewService: DeliveryOverviewService,
		protected messageService?: MessageService,
		protected headerService?: HeaderService,
		private router?: Router
	) {}

	ngOnInit() {
		this.initGenericTable();
		this.stepsAccordionIndexes.set(0, OrderStep.ORDER);
		this.stepsAccordionIndexes.set(1, OrderStep.REFERENCE);
		this.stepsAccordionIndexes.set(2, OrderStep.UNITS);
		this.stepsAccordionIndexes.set(3, OrderStep.DELIVERY);
		if (this.expandAllAtStart) {
			this.activeAccordionIndex = "0,1,2,3";
		}
	}

	numberUnitChange(order: Order) {
		this.orderChange.emit(order);
	}

	async initGenericTable() {
		this.initGenericTableCore();
		this.genericTableReady = true;
	}

	initGenericTableCore() {
		this.dataInfos.dataKey = "ebDelLivraisonNum";
		this.dataInfos.dataType = Delivery;
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showDeleteBtn = false;
		this.dataInfos.showEditBtn = false;
		this.searchInput.ebDelOrderNum = +this.activatedRoute.snapshot.params.id;
		if (this.searchInput.ebDelOrderNum) {
			this.searchInput.excludeCanceled = true;
			this.dataInfos.dataLink = Statique.controllerDeliveryOverView + "/list";
			this.dataInfos.cols = this.initGenericTableCols();
			this.dataInfos.selectedCols = this.dataInfos.cols;
		}
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.subTableNumberDatasPerPage = 10;
		this.dataInfos.paginator = true;
		this.dataInfos.numberDatasPerPage = 10;
		this.dataInfos.showDetails = true;
		this.dataInfos.actionDetailsLink = "app/delivery-overview/delivery/details/";
	}

	search(event: SearchCriteriaDelivery) {
		this.searchInput = event;
	}

	editAction(event) {
		this.router.navigate(["/app/delivery-overview/order/creation/" + event.ebDelOrderNum]);
	}
	showPlanificationDetail() {
		let params = +this.activatedRoute.snapshot.params.id;
		this.router.navigate(["app/delivery-overview/order/details/" + params]);
		this.modePlanify = true;
		this.isPlanningPlanify = false;
		this.selectStep(this.Step.DELIVERY);
	}

	initGenericTableCols(): any {
		let cols: any = [
			{
				field: "ebDelReference",
				translateCode: "PRICING_BOOKING.DELIVERY_REFERENCE",
			},

			{
				field: "xEbOwnerOfTheRequest",
				translateCode: "PRICING_BOOKING.REQUESTOR",
				render: function(xEbOwnerOfTheRequest: EbUser, d, row) {
					return xEbOwnerOfTheRequest && xEbOwnerOfTheRequest.ebUserNum
						? xEbOwnerOfTheRequest.nom + " - " + xEbOwnerOfTheRequest.prenom
						: "";
				},
			},
			{
				field: "xEbPartyOrigin",
				translateCode: "PRICING_BOOKING.ADDRESS_FROM",
				render: function(xEbPartyOrigin: EbParty, d, row) {
					return xEbPartyOrigin && xEbPartyOrigin.ebPartyNum
						? xEbPartyOrigin.xEcCountry.libelle
						: "";
				},
			},
			{
				field: "xEbPartyDestination",
				translateCode: "PRICING_BOOKING.ADDRESS_TO",
				render: function(xEbPartyDestination: EbParty, d, row) {
					return xEbPartyDestination && xEbPartyDestination.ebPartyNum
						? xEbPartyDestination.xEcCountry.libelle
						: "";
				},
			},
			{
				field: "xEbPartySale",
				translateCode: "PRICING_BOOKING.ADDRESS_SOLD_TO",
				render: function(xEbPartySale: EbParty, d, row) {
					return xEbPartySale && xEbPartySale.ebPartyNum ? xEbPartySale.xEcCountry.libelle : "";
				},
			},
			{
				field: "dateCreationSys",
				translateCode: "CHANEL_PB_DETAIL_ORDER.DATE_CREATION_LIVRAISON",
				render: function(data) {
					return Statique.formatDate(data);
				},
			},
			{ field: "xEcIncotermLibelle", translateCode: "PRICING_BOOKING.INCOTERMS" },
			{
				field: "statusDelivery",
				translateCode: "CHANEL_PB_DELIVERY.DELIVERY_STATUS",
				sortable: false,
				render: function(data) {
					if (data) {
						return this.translate.instant("CHANEL_PB_DELIVERY." + StatusDelivery[data]);
					}
				}.bind(this),
			},
			{
				field: "customerOrderReference",
				translateCode: "ORDER.CUSTOMER_ORDER_REFERENCE",
				width: "150px",
				isCardCol: true,
				cardOrder: 2,
				isCardLabel: false,
			},
			{
				field: "dateOfGoodsAvailability",
				translateCode: "CHANEL_PB_DELIVERY.DATE_OF_GOODS_AVAILABILITY",
				render: function(data) {
					return Statique.formatDate(data);
				},
			},
		];
		return cols;
	}
	showDeleteDialogConfirm(delivery: Delivery) {
		this.deliveryToDeleteNum = delivery.ebDelLivraisonNum;
		this.modalConfirmDeleteVisibility = true;
	}

	hideDeleteDialogConfirm() {
		this.modalConfirmDeleteVisibility = false;
	}

	deleteOneDelivery(event) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.deliveryOverviewService.deleteOneDelivery(this.deliveryToDeleteNum).subscribe(
			(data) => {
				requestProcessing.afterGetResponse(event);
				this.genericTable.refreshData();
				this.modalConfirmDeleteVisibility = false;
				this.successfullOperation();
				this.deliveryToDeleteNum = null;
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				this.modalConfirmDeleteVisibility = false;
				this.operationFailed();
				this.deliveryToDeleteNum = null;
			}
		);
	}

	redirectToDelivery(event) {
		this.searchInput.ebDelOrderNum = +this.activatedRoute.snapshot.params.id;
		let params = { queryParams: { filterByOrder: this.searchInput.ebDelOrderNum } };
		this.router.navigate(["app/delivery-overview/delivery/dashboard"], params);
	}

	successfullOperation() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	operationFailed() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}
	receiveModePlanify($event) {
		this.modePlanify = $event;
	}

	editDelivery(ebDelLivraison: Delivery) {
		if (ebDelLivraison.statusDelivery == StatusDelivery.PENDING) {
			this.planificationRequest = new EbOrderPlanificationDTO();
			this.planificationRequest.ebDelLivraisonNum = ebDelLivraison.ebDelLivraisonNum;
			this.planificationRequest.ebDelOrderNum = this.order.ebDelOrderNum;
			if (
				this.planificationRequest != null &&
				this.planificationRequest.ebDelLivraisonNum != null
			) {
				this.deliveryOverviewService
					.getDeliveryLineByDelivery(this.planificationRequest.ebDelLivraisonNum)
					.subscribe((result) => {
						this.deliveryLine = result;
						this.modePlanify = true;
						this.isPlanningPlanify = true;
					});
			}
		} else {
			this.messageService.add({
				severity: "error",
				summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
				detail: this.translate.instant("PRICING_BOOKING.ERROR_UPDATE_DELIVERY"),
			});
		}
	}

	selectStep(stepCode: string) {
		this.onStepSelected.emit(stepCode);
	}
}
