import {
	AfterViewInit,
	Component,
	Input,
	OnInit,
	ViewChild,
	Output,
	EventEmitter,
	SimpleChanges,
} from "@angular/core";
import { Router } from "@angular/router";
import { ActivatedRoute } from "@angular/router";
import { AuthenticationService } from "@app/services/authentication.service";
import { Modules, AccessRights, OrderStep, OrderStatus } from "@app/utils/enumeration";
import { MessageService } from "primeng/api";
import { ModalService } from "@app/shared/modal/modal.service";
import { TranslateService } from "@ngx-translate/core";
import { OrderStepUnitsComponent } from "@app/component/delivery-overview/order-creation-form/steps/order-step-units/order-step-units.component";
import { OrderStepReferenceComponent } from "@app/component/delivery-overview/order-creation-form/steps/order-step-reference/order-step-reference.component";
import { FormStep } from "@app/shared/form-step/form-step.component";
import { EtablissementService } from "@app/services/etablissement.service";
import { IField, CustomField } from "@app/classes/customField";
import { EbCostCenter } from "@app/classes/costCenter";
import { EbCategorie } from "@app/classes/categorie";
import { EbTypeRequestDTO } from "@app/classes/dto/EbTypeRequestDTO";
import { EbUser } from "@app/classes/user";
import { CollectionService } from "@app/services/collection.service";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { ConfigIncotermService } from "@app/services/config-incoterm.service";
import { EcCountry } from "@app/classes/country";
import { EbTypeUnit } from "@app/classes/typeUnit";
import { Statique } from "@app/utils/statique";
import { SearchCriteriaOrder } from "@app/utils/searchCriteriaOrder";
import { Order } from "@app/classes/ebDelOrder";
import { DeliveryOverviewService } from "@app/services/delivery-overview.service";
import { GenericTableService } from "@app/services/generic-table.service";
import { HeaderService } from "@app/services/header.service";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { OrderStepOrderComponent } from "./steps/order-step-order/order-step-order.component";
import { OrderCreationFormStepBaseComponent } from "./core/order-creation-from-step-base";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { EbTypeFlux } from "@app/classes/EbTypeFlux";
import { EbTtSchemaPsl } from "@app/classes/EbTtSchemaPsl";
import { EbIncoterm } from "@app/classes/EbIncoterm";
import { OrderStepDeliveryComponent } from "./steps/order-step-delivery/order-step-delivery.component";

@Component({
	selector: "app-order-creation-form",
	templateUrl: "./order-creation-form.component.html",
	styleUrls: ["../../../shared/modal/modal.component.scss", "./order-creation-form.component.scss"],
})
export class OrderCreationFormComponent extends FormStep implements OnInit, AfterViewInit {
	savingOrder: boolean;
	Step = OrderStep;
	stepsOrdered = new Array<string>();
	stepsComponents = new Map<string, any>();
	disabledSaveButton: boolean = false;
	shippingOriginOk: boolean = false;
	shippingDestOk: boolean = false;
	shippingSaleOk: boolean = false;
	@Input()
	readOnly: boolean = false;

	@Input()
	showStepNavigation: boolean = true;

	@Input()
	selectedStep: string = OrderStep.ORDER;

	@ViewChild("stepOrder", { static: true })
	orderStepOrder: OrderStepOrderComponent;

	@ViewChild(OrderStepReferenceComponent, { static: true })
	orderStepReference: OrderStepReferenceComponent;

	@ViewChild(OrderStepUnitsComponent, { static: true })
	orderStepUnits: OrderStepUnitsComponent;

	@ViewChild(OrderStepDeliveryComponent, { static: false })
	orderStepDelivery: OrderStepDeliveryComponent;
	displayOptions: boolean = false;
	showAddressPopup: boolean = false;
	@Input()
	order: Order = new Order();
	@Output()
	orderChange: EventEmitter<Order> = new EventEmitter<Order>();

	@Input()
	enableCardStyle: boolean = true;

	// Lists
	listCategoriesEtab: Array<EbCategorie>;
	listCategorieSelected: Array<EbCategorie>;
	nbSavedFormLoaded: number = 0;
	deletedCustomFields: Array<IField>;
	listCostCenter: Array<EbCostCenter>;
	listTypeOrderByCompagnie: Array<EbTypeRequestDTO>;
	listTypeUnit: Array<any>;
	listFields: Array<IField>;
	listCountry: Array<EcCountry> = new Array<EcCountry>();
	listTypesUnit: Array<EbTypeUnit> = new Array<EbTypeUnit>();
	listCustomsFields: Array<IField> = new Array<IField>();
	Modules = Modules;
	criteria: SearchCriteriaOrder = new SearchCriteriaOrder();
	listOrderStatus: any[] = [];
	listModeTransport: any[];
	Statique = Statique;
	modeTransport: number = 3;
	@Input()
	listTypeFlux: Array<EbTypeFlux> = new Array<EbTypeFlux>();
	@Input()
	listIncoterms: Array<EbIncoterm> = new Array<EbIncoterm>();

	listUserEtab: any = new Array<EbUser>();
	@Output()
	onStepSelected: EventEmitter<string> = new EventEmitter<string>();
	@Input()
	expandAllAtStart: boolean = false;
	activeAccordionIndex: any;
	stepsAccordionIndexes = new Map<number, string>();

	constructor(
		private deliveryOverviewService: DeliveryOverviewService,
		protected collectionService: CollectionService,
		protected savedFormsService: SavedFormsService,
		protected activatedRoute: ActivatedRoute,
		protected translate: TranslateService,
		protected authenticationService: AuthenticationService,
		protected router: Router,
		private route: ActivatedRoute,
		private genericTableService: GenericTableService,
		protected modalService: ModalService,
		protected etablissementService: EtablissementService,
		private headerService: HeaderService,
		protected messageService?: MessageService,
		protected configIncoterms?: ConfigIncotermService
	) {
		super(authenticationService);
	}

	ngOnInit() {
		this.initStepInformations();
		this.setContributionAccess();
		this.route.params.subscribe((params) => {
			if (!isNaN(parseInt(params["id"]))) {
				// Load data for visualization
				let criteriaGetOrder = new SearchCriteriaOrder();
				criteriaGetOrder.ebDelOrderNum = parseInt(params["id"]);
				criteriaGetOrder.connectedUserNum = this.userConnected.ebUserNum;
				criteriaGetOrder.includeLines = true;

				this.deliveryOverviewService.getOrder(criteriaGetOrder).subscribe((order) => {
					this.order = order;
					OrderStatus[this.order.orderStatus] = this.orderStepReference.ordrStatus;
					this.shippingOriginOk = false;
					this.shippingDestOk = false;
					this.shippingSaleOk = false;
				});
			}
		});
		this.getListTypeFluxDatas();
		// this.getDataPreference();
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["order"]) {
			if (this.order.listCategories && this.listCategoriesEtab) {
				this.order.listCategories.forEach((it) => {
					let cat: EbCategorie = this.listCategoriesEtab.find(
						(ca) => ca.ebCategorieNum == it.ebCategorieNum
					);
					if (it.labels && it.labels.length) {
						it.selectedLabel = it.labels[0];
					}
					it.labels = cat.labels;
				});
			}
		}
	}

	get isCreate(): boolean {
		if (!this.router || !this.router.url) return false;
		return this.router.url.indexOf("/creation") >= 0;
	}
	numberUnitChange(order: Order) {
		this.orderChange.emit(order);
	}
	showAddressPopupChangehandler(showAddressPopup) {
		if (showAddressPopup) this.showOverview = false;
		else this.showOverview = true;
	}
	getDataPreference(event: any = null) {
		if (this.order.listCategories) this.listCategorieSelected = this.order.listCategories;
		this.order.listCustomsFields = null;
		this.order.listCategories = null;
		let criteria: SearchCriteriaOrder = new SearchCriteriaOrder();
		criteria.size = null;
		criteria.ebDelOrderNum = this.order.ebDelOrderNum;
		criteria.connectedUserNum = this.userConnected.ebUserNum;

		if (
			this.order.xEbOwnerOfTheRequest &&
			this.order.xEbOwnerOfTheRequest.ebCompagnie &&
			this.order.xEbOwnerOfTheRequest.ebCompagnie.ebCompagnieNum
		) {
			criteria.ebCompagnieNum = this.order.xEbOwnerOfTheRequest.ebCompagnie.ebCompagnieNum;
			criteria.ebEtablissementNum = this.order.xEbOwnerOfTheRequest.ebEtablissement.ebEtablissementNum;
			criteria.ebUserNum = this.order.xEbOwnerOfTheRequest.ebUserNum;
			criteria.contact = true;
		} else {
			criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
			criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
			criteria.ebUserNum = this.order.ebDelOrderNum;
			criteria.contact = true;
		}

		if (!criteria.ebEtablissementNum || !criteria.ebCompagnieNum) {
			this.order.listCustomsFields = new Array<IField>();
			this.order.listCategories = new Array<EbCategorie>();
			this.listCostCenter = new Array<EbCostCenter>();
			return;
		}

		this.etablissementService.getDataPreference(criteria).subscribe(async (res) => {
			this.listCostCenter = this.getListCostCenterFiltered(res);
			this.listCategoriesEtab = res.listCategorie;
			if (res && res.listCategorie && res.listCategorie.length)
				this.editOrderCategoriesWithSelectedCategories(res.listCategorie);
			else this.editOrderCategoriesWithSelectedCategories(null);

			this.order.listCustomsFields =
				res && res.customField && res.customField.fields
					? JSON.parse(res.customField.fields)
					: new Array<IField>();
			this.order.listCustomsFields = this.order.listCustomsFields || new Array<IField>();
			this.listIncoterms = res.listIncoterm;
			this.listTypeOrderByCompagnie = res.listTypeRequest;

			this.listTypeUnit = res.listTypeUnit;
			if (this.order.listCategories && this.order.listCategories.length > 0) {
				this.order.listCategories.forEach((it) => {
					if (it.labels && it.labels.length > 0) {
						it.labels.sort((a, b) => {
							return a.libelle.localeCompare(b.libelle);
						});
					}
				});

				this.order.listCategories = this.order.listCategories.sort(
					(it1, it2) => it1.ebCategorieNum - it2.ebCategorieNum
				);
			}

			if (this.order.listCustomsFields && this.order.listCustomsFields.length > 0) {
				let fields: Array<IField> = this.order.customFields
					? JSON.parse(this.order.customFields)
					: new Array<IField>();
				this.order.listCustomsFields.forEach((field) => {
					let fd = fields.find((it) => it.label == field.label);
					field.value = fd && fd.value ? fd.value : field.value;
				});

				this.deletedCustomFields = new Array<IField>();
				fields.forEach((field) => {
					// adding deleted fields
					let fd = this.order.listCustomsFields.find((fld) => fld.label == field.label);
					if (!fd) this.deletedCustomFields.push(field);
				});
				// } else {
				// 	this.order.listCustomsFields = fields;
			}

			// SVP ne pas supprimer cette ligne, elle fait en sorte que les custom fields soient enregistrés dans les units
			this.listCustomsFields = Statique.cloneListObject(this.order.listCustomsFields, CustomField);
		});
	}

	//affectation de la statique liste des categories sans avoire perdre le libelle selectionné
	editOrderCategoriesWithSelectedCategories(newList: Array<EbCategorie>) {
		if (!newList || !newList.length) {
			this.order.listCategories = this.listCategorieSelected;
			if (this.order.listCategories) {
				this.order.listCategories.forEach((it) => {
					if (it.labels && it.labels.length) it.selectedLabel = it.labels[0];
				});
			} else {
				this.order.listCategories = [];
			}
		} else {
			if (this.listCategorieSelected && this.listCategorieSelected.length > 0) {
				this.listCategorieSelected.forEach((selectedCat) => {
					if (selectedCat.labels && selectedCat.labels.length == 1) {
						let found = false;
						for (let i = 0; i < newList.length; i++) {
							if (newList[i].ebCategorieNum == selectedCat.ebCategorieNum) {
								newList[i].selectedLabel = selectedCat.labels[0];
								found = true;
							}
						}
						if (!found) {
							newList.push(selectedCat);
						}
					}
				});
			}

			this.order.listCategories =
				newList && newList.length > 0 ? newList : new Array<EbCategorie>();
		}
	}
	async cloneEbOrderWithSf(parsedValues: Order, fieldsAndCategoriesOnly = false) {
		this.order = new Order();

		if (!fieldsAndCategoriesOnly) {
			this.order.cloneData(parsedValues);
			if (!this.order.xEbOwnerOfTheRequest || !this.order.xEbOwnerOfTheRequest.ebUserNum) {
				if (this.isChargeur) {
					let user = new EbUser();
					user.constructorCopy(this.userConnected);

					this.order.xEbOwnerOfTheRequest = new EbUser();
					this.order.xEbOwnerOfTheRequest.constructorCopy(user);

					this.orderStepOrder.handleSelectedUserChange(user);
				}
			}
		}
		if (this.order.listCategories && this.order.listCategories.length > 0) {
			this.order.listCategories.forEach((categorie) => {
				let cat = parsedValues.listCategories.find(
					(it) => it.ebCategorieNum == categorie.ebCategorieNum
				);
				categorie.selectedLabel =
					cat && cat.labels && cat.labels.length > 0 ? cat.labels[0] : categorie.selectedLabel;
			});
		}

		this.nbSavedFormLoaded++;

		this.getDataPreference();
	}

	get isCreation(): boolean {
		return !this.readOnly;
	}
	getListCostCenterFiltered(res) {
		let listCostCenterFiltered: Array<EbCostCenter> = new Array<EbCostCenter>();
		if (res && res.listCostCenter) {
			if (this.order.ebDelOrderNum != null) {
				listCostCenterFiltered = res.listCostCenter;
			} else {
				res.listCostCenter.forEach((item) => {
					if (!item.deleted) listCostCenterFiltered.push(item);
				});
			}
		}
		return listCostCenterFiltered;
	}
	setContributionAccess() {
		if (this.userConnected.listAccessRights && this.userConnected.listAccessRights.length > 0) {
			this.hasContributionAccess =
				this.userConnected.listAccessRights.find((access) => {
					return (
						access.ecModuleNum == Modules.ORDER_MANAGEMENT &&
						access.accessRight == AccessRights.CONTRIBUTION
					);
				}) != undefined;
		} else {
			this.hasContributionAccess = false;
		}
	}
	ngAfterViewInit() {
		this.stepsComponents.set(OrderStep.ORDER, this.orderStepOrder);
		this.stepsComponents.set(OrderStep.REFERENCE, this.orderStepReference);
		this.stepsComponents.set(OrderStep.UNITS, this.orderStepUnits);
		this.stepsComponents.set(OrderStep.DELIVERY, this.orderStepUnits);
		this.displayOptions = this.stepsComponents.get(OrderStep.ORDER).optionsDisplayed;
		if (!this.readOnly) this.showOverview = !this.displayOptions;
	}

	private initStepInformations() {
		this.stepsOrdered = [OrderStep.ORDER, OrderStep.REFERENCE, OrderStep.UNITS];
	}

	isStepVisible(stepCode: string): boolean {
		switch (stepCode) {
			case OrderStep.ORDER:
				return true;
			case OrderStep.REFERENCE:
				return true;
			case OrderStep.UNITS:
				return true;
		}
	}

	getListTypeFluxDatas() {
		let criteria = new SearchCriteriaOrder();
		if (
			this.order.xEbOwnerOfTheRequest &&
			this.order.xEbOwnerOfTheRequest.ebCompagnie &&
			this.order.xEbOwnerOfTheRequest.ebCompagnie.ebCompagnieNum
		) {
			criteria.ebCompagnieNum = this.order.xEbOwnerOfTheRequest.ebCompagnie.ebCompagnieNum;
		} else {
			criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		}

		(async function() {
			this.listIncoterms = await this.configIncoterms.getListIncotermsPromise(criteria);
		}.bind(this)());
	}
	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}

	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.DETAIL"),
		});
	}

	showCustomOrderRefError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("CHANEL_PB_ORDER.CUSTOMER_ORDER_REF_ERROR"),
		});
	}

	addOrder() {
		this.savingOrder = true;
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.order.xEbEtablissement = this.userConnected.ebEtablissement;
		this.order.xEbCompagnie = this.userConnected.ebCompagnie;
		this.order.customerCreationDate = Statique.formatDate(this.order.customerCreationDate);
		this.order.deliveryDate = Statique.formatDate(this.order.deliveryDate);
		this.order.targetDate = Statique.formatDate(this.order.targetDate);
		this.order.orderDate = Statique.formatDate(this.order.orderDate);

		this.order.listCategoriesStr = "";
		this.order.listLabels = "";
		///supp de la list des categories et mettre a sa place l'element selecter
		if (this.order.listCategories && this.order.listCategories.length > 0) {
			this.order.listCategories.forEach((elem) => {
				if (elem.selectedLabel) {
					elem.labels = [elem.selectedLabel];
				} else if (elem.labels && elem.labels.length > 1) {
					elem.labels = [];
				}
			});
		}else {
			this.order.listCategories = null;
		}

		if (this.order.ebDelOrderNum == null) {
			this.deliveryOverviewService.createOrder(this.order).subscribe(
				(res) => {
					requestProcessing.afterGetResponse(event);
					this.savingOrder = false;
					this.showSuccess();
					this.router.navigate(["app/delivery-overview/order/dashboard"]);
				},
				(error) => {
					this.savingOrder = false;
					this.showError();
					this.showCustomOrderRefError();
				}
			);
		}
		if (this.order.ebDelOrderNum != null) {
			this.deliveryOverviewService.saveOrder(this.order).subscribe(
				(res) => {
					requestProcessing.afterGetResponse(event);
					this.savingOrder = false;
					this.showSuccess();
					this.router.navigate(["app/delivery-overview/order/details/" + this.order.ebDelOrderNum]);
				},
				(error) => {
					this.savingOrder = false;
					this.showError();
					this.showCustomOrderRefError();
				}
			);
		}
	}

	loadListCategoriesAndFields() {
		let criteria = new SearchCriteria();
		criteria.ebUserNum = this.userConnected.ebUserNum;
		criteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		criteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.etablissementService.getDataPreference(criteria).subscribe((res) => {
			this.order.listCategories =
				res && res.listCategorie ? res.listCategorie : new Array<EbCategorie>();
			this.order.listCustomsFields =
				res && res.customField && res.customField.fields
					? JSON.parse(res.customField.fields)
					: new Array<IField>();

			this.listCustomsFields = Statique.cloneListObject(this.order.listCustomsFields, CustomField);
		});
	}
}
