import { GenericTableScreen } from "./../../../utils/enumeration";
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { EbCategorie } from "@app/classes/categorie";
import { IField } from "@app/classes/customField";
import { Modules, SavedFormIdentifier } from "@app/utils/enumeration";
import { SearchField } from "@app/classes/searchField";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";
import { EbLabel } from "@app/classes/label";
import { StatiqueService } from "@app/services/statique.service";
import { SearchCriteriaDelivery } from "@app/utils/SearchCritereaDelivery";
import { SearchCriteriaOrder } from "@app/utils/searchCriteriaOrder";
import { TranslateService } from "@ngx-translate/core";
import { AdvancedFormSearchComponent } from "@app/shared/advanced-form-search/advanced-form-search.component";
import { Statique } from "@app/utils/statique";

@Component({
	selector: "app-delivery-search",
	templateUrl: "./delivery-search.component.html",
	styleUrls: ["./delivery-search.component.css"],
})
export class DeliverySearchComponent implements OnInit {
	@Input()
	component: number;
	@Output()
	onSearch = new EventEmitter<any>();
	@Input()
	listFields: Array<IField>;
	@Input()
	listCategorie: Array<EbCategorie>;

	SavedFormIdentifier = SavedFormIdentifier;
	advancedOption: SearchCriteriaDelivery = new SearchCriteriaDelivery();
	Modules = Modules;

	paramFields: Array<SearchField> = new Array<SearchField>();
	@Input()
	initParamFields: Array<SearchField> = new Array<SearchField>();
	listAllCategorie: Array<EbLabel> = new Array<EbLabel>();
	activeTheme: string;
	@Input()
	module: number;
	@ViewChild("advancedFormSearchComponent", { static: false })
	advancedFormSearchComponent: AdvancedFormSearchComponent;
	@Output()
	listFieldsEmitter = new EventEmitter<Array<IField>>();

	@Input()
	selectedField: any;
	@Input()
	dataInfosItem: Array<any> = [];
	@Input()
	initialListFields: any = [];
	@Input()
	initFields?: string = "json"; // undefined | all | json
	public listSelectedFields: Array<SearchField>;
	@ViewChild(AdvancedFormSearchComponent, { static: false })
	advancedFormSearch: AdvancedFormSearchComponent;

	constructor(
		public translate: TranslateService,
		public generaleMethode: generaleMethodes,
		private statiqueService: StatiqueService
	) {}

	ngOnInit() {
		this.onInputChange(this.selectedField);
		this.initialListFields = [];
		if (this.initialListFields && typeof this.initFields !== "undefined") {
			if (this.initFields === "all")
				this.listSelectedFields = Statique.cloneObject(this.initialListFields);
			else {
				this.listSelectedFields = this.paramFields;
			}
		}
		if (!this.paramFields || this.paramFields.length <= 0) {
			this.paramFields = [].concat(this.initParamFields);
		}
	}

	onClickInput() {
		if (this.module == Modules.ORDER_MANAGEMENT) {
			this.initParamFields = require("../../../../assets/ressources/jsonfiles/order-search.json");
		}
		if (this.module == Modules.DELIVERY_MANAGEMENT) {
			this.initParamFields = require("../../../../assets/ressources/jsonfiles/delivery-search.json");
		}
		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));

		if (this.listFields && this.listFields.length > 0) {
			let arr = [];
			this.listFields.forEach((it) => {
				arr.push({
					label: it.label,
					name: it.name,
					type: "text",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
		}
		if (this.listCategorie && this.listCategorie.length > 0) {
			let arr = [];
			let i = 0;
			this.listAllCategorie = [];
			this.listCategorie.forEach((it, i) => {
				arr.push({
					label: it.libelle,
					type: "autocomplete",
					name: "categoryField" + i,
					listItems: it.labels,
					fetchAttribute: "libelle",
					fetchValue: "ebLabelNum",
					multiple: "true",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
		}

		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});
	}

	onInputChange(data: any) {
		if (this.module == Modules.ORDER_MANAGEMENT) {
			this.initParamFields = require("../../../../assets/ressources/jsonfiles/order-search.json");
		}
		if (this.module == Modules.DELIVERY_MANAGEMENT) {
			this.initParamFields = require("../../../../assets/ressources/jsonfiles/delivery-search.json");
		}

		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));

		if (this.listFields && this.listFields.length > 0) {
			let arr = [];
			this.listFields.forEach((it) => {
				arr.push({
					label: it.label,
					name: it.name,
					type: "text",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
			this.initParamFields = this.initParamFields.concat(arr);
		}
		if (this.listCategorie && this.listCategorie.length > 0) {
			let arr = [];
			let i = 0;
			this.listAllCategorie = [];
			this.listCategorie.forEach((it, i) => {
				arr.push({
					label: it.libelle,
					type: "autocomplete",
					name: "categoryField" + i,
					listItems: it.labels,
					fetchAttribute: "libelle",
					fetchValue: "ebLabelNum",
					multiple: "true",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
			this.initParamFields = this.initParamFields.concat(arr);
		}

		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});
		if (data === null || data === undefined) {
			this.initParamFields = this.initParamFields.filter(
				(field) =>
					field.name === "listPartyDestination" ||
					field.name === "listOrigins" ||
					field.name === "listOwnerOfTheRequests"
			);
			this.paramFields = Array.from(new Set([].concat(this.initParamFields)));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
		} else if (data !== null && !this.paramFields.find((field) => field.name == data)) {
			let result = JSON.stringify(this.advancedFormSearch.getSearchInput());
			let parsedValues: Array<SearchField> = JSON.parse(result);
			this.paramFields.forEach((field) => {
				if (parsedValues[field.name] !== null && parsedValues[field.name] !== undefined) {
					field.default = parsedValues[field.name];
				}
			});
			this.initParamFields = this.initParamFields.filter(
				(field) => field.name === data && field.default !== null
			);
			this.paramFields = this.paramFields.concat(this.initParamFields);
			this.paramFields = Array.from(new Set(this.paramFields));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
		}
	}

	search(advancedOption: any) {
		this.onSearch.emit(advancedOption);
	}

	clearSearch() {
		if (this.component == GenericTableScreen.DELIVERY_DASHBOARD) {
			this.onSearch.emit(new SearchCriteriaDelivery());
		} else if (this.component == GenericTableScreen.ORDER_DASHBOARD) {
			this.onSearch.emit(new SearchCriteriaOrder());
		}
	}
}
