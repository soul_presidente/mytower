import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { DeliveryOverviewRoutes } from "./delivery-overview.routes";
import { DeliveryOverviewComponent } from "./delivery-overview.component";
import { SharedModule } from "@app/shared/module/shared.module";
import { DeliverySearchComponent } from "./delivery-search/delivery-search.component";
import { DashboardOrderComponent } from "./dashboard-order/dashboard-order.component";
import { DashboardDeliveryComponent } from "./dashboard-delivery/dashboard-delivery.component";
import { PricingService } from "@app/services/pricing.service";
import { DialogModule } from "primeng/dialog";
import { DynamicDialogModule } from "primeng/dynamicdialog";
import { SpinnerModule } from "primeng/spinner";
import { DeliveryEditionFormComponent } from "./delivery-edition-form/delivery-edition-form.component";
import { DeliveryStepDeliveryComponent } from "./delivery-edition-form/steps/delivery-step-delivery/delivery-step-delivery.component";
import { DeliveryStepReferenceComponent } from "./delivery-edition-form/steps/delivery-step-reference/delivery-step-reference.component";
import { DeliveryStepUnitsComponent } from "./delivery-edition-form/steps/delivery-step-units/delivery-step-units.component";
import { AdresseForShippingDeliveryComponent } from "./delivery-edition-form/steps/delivery-step-delivery/adresse-for-shipping-delivery/adresse-for-shipping-delivery.component";
import { DeliveryOverviewEditionComponent } from "./delivery-edition-form/steps/delivery-overview-edition/delivery-overview-edition.component";
import { DeliveryPlanificationComponent } from "./delivery-planification/delivery-planification.component";

@NgModule({
	imports: [
		CommonModule,
		DeliveryOverviewRoutes,
		SharedModule,
		FormsModule,
		DialogModule,
		DynamicDialogModule,
		SpinnerModule,
	],
	declarations: [
		DeliveryOverviewComponent,
		DashboardOrderComponent,
		DashboardDeliveryComponent,
		DeliveryStepDeliveryComponent,
		DeliveryStepReferenceComponent,
		DeliveryStepUnitsComponent,
		DeliveryEditionFormComponent,
		AdresseForShippingDeliveryComponent,
		DeliveryOverviewEditionComponent,
		DeliveryPlanificationComponent,
	],
	exports: [DeliverySearchComponent],
	providers: [PricingService],
})
export class DeliveryOverviewModule {}
