import { Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { AnalyticsConfigDTO } from "@app/classes/analytics/analytics-config";
import { KibanaResponse } from "@app/classes/kibanaResponse";
import { AuthenticationService } from "@app/services/authentication.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { RoleSociete, UserRole } from "@app/utils/enumeration";

@Component({
	selector: "app-dashboard-frame",
	templateUrl: "./dashboard-frame.component.html",
	styleUrls: ["./dashboard-frame.component.scss"],
})
export class DashboardFrame extends ConnectedUserComponent implements OnChanges {
	reportUrl: string;

	@Input()
	config: AnalyticsConfigDTO;

	@Input()
	dashboardId: string;

	private static COMMON_KBN_QUERY =
		"?embed=true&_g=(refreshInterval:(pause:!t,value:0),time:(from:now-5y,mode:quick,to:now))";

	constructor(protected authenticationService: AuthenticationService) {
		super(authenticationService);
	}

	ngOnChanges(changes: SimpleChanges): void {
		if (changes["config"] && changes["config"].currentValue) {
			this.applyConfig(changes["config"].currentValue);
		}
	}

	private async applyConfig(config: AnalyticsConfigDTO) {
		this.config = config;

		if (this.config.kibanaSecured) {
			await this.buildSecuredUrl();
		} else {
			this.buildUnsecuredUrl();
		}
	}

	private async buildSecuredUrl() {
		const auth: KibanaResponse = await new Promise((resolve, reject) => {
			this.authenticationService
				.kibanaAuth()
				.subscribe((result) => resolve(result), (error) => reject(error));
		});

		let dashUrl = "/app/kibana#/dashboard/";
		dashUrl += this.dashboardId;
		dashUrl += DashboardFrame.COMMON_KBN_QUERY;
		dashUrl += `&_a=(query:(language:lucene,query:'${this.getCompanyFilter()}'))`;

		this.reportUrl =
			auth.kibana_url + `/dash?setAuthToken=${auth.access_token}&dashboard=${dashUrl}`;
	}

	private buildUnsecuredUrl() {
		let url = this.config.kibanaUrl + "/app/kibana#/dashboard/";
		url += this.dashboardId;
		url += DashboardFrame.COMMON_KBN_QUERY;
		url += `&_a=(query:(language:lucene,query:'${this.getCompanyFilter()}'))`;

		this.reportUrl = url;
	}

	private getCompanyFilter(): string {
		let filterByCompany = "";
		if (this.userConnected && this.userConnected.ebCompagnie 
			&& this.userConnected.ebCompagnie.compagnieRole != RoleSociete.CONTROL_TOWER) {
			filterByCompany = "compagnie_num:";
			if (this.userConnected && this.userConnected.ebCompagnie) {
				filterByCompany += this.userConnected.ebCompagnie.ebCompagnieNum;
			} else {
				filterByCompany += "0";
			}
		}
		return filterByCompany;
	}
}
