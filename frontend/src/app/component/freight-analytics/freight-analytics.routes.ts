import { Route, RouterModule } from "@angular/router";
import { FreightAnalyticsComponent } from "./freight-analytics.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";

export const FREIGHT_ANALYTICS_ROUTES: Route[] = [
	{
		path: "",
		component: FreightAnalyticsComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: ":tab",
		component: FreightAnalyticsComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const FreightAnalyticsRoutes = RouterModule.forChild(FREIGHT_ANALYTICS_ROUTES);
