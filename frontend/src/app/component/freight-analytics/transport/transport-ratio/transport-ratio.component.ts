import { Component, Input, OnInit } from "@angular/core";

@Component({
	selector: "app-transport-ratio",
	templateUrl: "./transport-ratio.component.html",
	styleUrls: ["./transport-ratio.component.css"],
})
export class TransportRatioComponent implements OnInit {
	@Input()
	data: any;
	@Input()
	main: any;

	constructor() {}

	ngOnInit() {}
}
