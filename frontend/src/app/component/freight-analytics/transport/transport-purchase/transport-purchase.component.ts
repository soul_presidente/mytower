import { Component, Input, OnInit } from "@angular/core";

@Component({
	selector: "app-transport-purchase",
	templateUrl: "./transport-purchase.component.html",
	styleUrls: ["./transport-purchase.component.css"],
})
export class TransportPurchaseComponent implements OnInit {
	@Input()
	data: any;

	constructor() {}

	ngOnInit() {}
}
