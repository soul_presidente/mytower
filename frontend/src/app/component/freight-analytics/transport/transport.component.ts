import { Component, OnInit, ViewChild } from "@angular/core";

import { SearchCriteriaAnalytics, TransportReport } from "@app/classes/analytics";

import { UIChart } from "primeng/primeng";
import { FreightAnalyticsService } from "@app/services/freight-analytics.service";
import { EbUser } from "@app/classes/user";
import { AuthenticationService } from "@app/services/authentication.service";
import { UserRole } from "@app/utils/enumeration";
import { KibanaResponse } from "@app/classes/kibanaResponse";

@Component({
	selector: "app-transport",
	templateUrl: "./transport.component.html",
	styleUrls: ["./transport.component.css"],
})
export class TransportComponent implements OnInit {
	token: string;
	dash;
	reportUrl: string;

	userConnected: EbUser;

	constructor(protected authenticationService: AuthenticationService) {
		this.userConnected = AuthenticationService.getConnectedUser();
	}

	ngOnInit() {
		let filterByCompany = "";
		if (this.userConnected && this.userConnected.role != UserRole.CONTROL_TOWER) {
			filterByCompany = "compagnie_num:";
			if (this.userConnected && this.userConnected.ebCompagnie) {
				filterByCompany += this.userConnected.ebCompagnie.ebCompagnieNum;
			} else {
				filterByCompany += "0";
			}
		}
		this.dash =
			"/app/kibana#/dashboard/1d824550-82bc-11e9-9a16-a5eaa2f36141?embed=true&_g=()&_a=(query:(language:lucene,query:'" +
			filterByCompany +
			"'))";

		this.authenticationService.kibanaAuth().subscribe((resp: KibanaResponse) => {
			this.token = String(resp.access_token);
			this.reportUrl = resp.kibana_url + `dash?setAuthToken=${this.token}&dashboard=` + this.dash;
			document.getElementById("iframe-kibana")["src"] = this.reportUrl;
		});
	}
}
