import { Component, Input, OnInit } from "@angular/core";

@Component({
	selector: "app-transport-activity",
	templateUrl: "./transport-activity.component.html",
	styleUrls: ["./transport-activity.component.css"],
})
export class TransportActivityComponent implements OnInit {
	@Input()
	data: any;

	constructor() {}

	ngOnInit() {}
}
