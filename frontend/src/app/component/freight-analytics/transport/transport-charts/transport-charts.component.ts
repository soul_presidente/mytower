import { Component, Input, OnInit } from "@angular/core";

@Component({
	selector: "app-transport-charts",
	templateUrl: "./transport-charts.component.html",
	styleUrls: ["./transport-charts.component.css"],
})
export class TransportChartsComponent implements OnInit {
	@Input()
	dataOrigin: any;
	@Input()
	dataDestination: any;

	options = {
		responsive: true,
		maintainAspectRatio: true,
	};

	constructor() {}

	ngOnInit() {}
}
