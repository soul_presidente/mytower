import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FreightAnalyticsRoutes } from "./freight-analytics.routes";
import { FreightAnalyticsComponent } from "./freight-analytics.component";
import {
	ChartModule,
	PanelModule,
	SharedModule,
	TabViewModule,
	ToolbarModule,
	TooltipModule,
} from "primeng/primeng";
import { TransportComponent } from "./transport/transport.component";
import { NgbTabsetModule } from "@ng-bootstrap/ng-bootstrap";
import { FreightAnalyticsService } from "@app/services/freight-analytics.service";
import { NumberSuffixesPipe } from "@app/utils/number-suffix";
import { NumberFormatPipe } from "@app/utils/number-format";
import { TransportActivityComponent } from "./transport/transport-activity/transport-activity.component";
import { TransportPurchaseComponent } from "./transport/transport-purchase/transport-purchase.component";
import { TransportRatioComponent } from "./transport/transport-ratio/transport-ratio.component";
import { TransportChartsComponent } from "./transport/transport-charts/transport-charts.component";
import { SearchComponent } from "./search/search.component";
import { SharedModule as MyTowerSharedModule } from "@app/shared/module/shared.module";
import { SafeUrl } from "@app/utils/safeUrl";
import { ProcessTimeComponent } from "./process-time/process-time.component";
import { TrendAnalysisComponent } from "./trend-analysis/trend-analysis.component";
import { DashboardFrame } from "./dashboard-frame/dashboard-frame.component";

@NgModule({
	imports: [
		CommonModule,
		FreightAnalyticsRoutes,
		TabViewModule,
		ChartModule,
		TooltipModule,
		ToolbarModule,
		SharedModule,
		MyTowerSharedModule,
		PanelModule,
		NgbTabsetModule,
	],
	declarations: [
		FreightAnalyticsComponent,
		TransportComponent,
		NumberSuffixesPipe,
		NumberFormatPipe,
		SafeUrl,
		TransportActivityComponent,
		TransportPurchaseComponent,
		TransportRatioComponent,
		TransportChartsComponent,
		SearchComponent,
		ProcessTimeComponent,
		TrendAnalysisComponent,
		DashboardFrame,
	],
	providers: [FreightAnalyticsService],
})
export class FreightAnalyticsModule {}
