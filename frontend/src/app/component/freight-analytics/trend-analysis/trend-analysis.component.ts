import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "@app/services/authentication.service";

@Component({
	selector: "app-trend-analysis",
	templateUrl: "./trend-analysis.component.html",
	styleUrls: ["./trend-analysis.component.scss"],
})
export class TrendAnalysisComponent implements OnInit {
	reportUrl =
		"https://kibana.adias.fr/app/kibana#/dashboard/8c724880-2fa5-11e9-b988-815a95cc91cb?embed=true&_g=()";

	constructor() {}

	ngOnInit() {}
}
