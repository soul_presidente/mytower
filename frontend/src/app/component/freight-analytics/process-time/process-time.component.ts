import { Component, OnInit } from "@angular/core";

@Component({
	selector: "app-process-time",
	templateUrl: "./process-time.component.html",
	styleUrls: ["./process-time.component.css"],
})
export class ProcessTimeComponent implements OnInit {
	reportUrl =
		"https://kibana.adias.fr/app/kibana#/dashboard/01830000-f898-11e8-a121-47af34bb09b2?embed=true&_g=(refreshInterval%3A(pause%3A!f%2Cvalue%3A60000)%2Ctime%3A(from%3Anow%2Fy%2Cmode%3Aquick%2Cto%3Anow%2Fy))";

	constructor() {}

	ngOnInit() {}
}
