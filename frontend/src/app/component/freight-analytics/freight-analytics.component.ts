import { Component, OnInit, ViewChild, HostListener } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Modules } from "@app/utils/enumeration";
import { SearchField } from "@app/classes/searchField";
import { NgbTabset } from "@ng-bootstrap/ng-bootstrap";
import { Statique } from "@app/utils/statique";
import {HeaderService} from "@app/services/header.service";
import {HeaderInfos} from "@app/shared/header/header-infos";
import {FreightAnalyticsService} from "@app/services/freight-analytics.service";
import {MessageService} from "primeng/components/common/messageservice";
import {TranslateService} from "@ngx-translate/core";
import {environment} from "@src/environments/environment";
import { AnalyticsConfigDTO } from "@app/classes/analytics/analytics-config";

@Component({
	selector: "app-freight-analytics",
	templateUrl: "./freight-analytics.component.html",
	styleUrls: ["./freight-analytics.component.css"],
})
export class FreightAnalyticsComponent implements OnInit {
	@ViewChild("tabset", { static: true })
	ngbTabSet: NgbTabset;
	params = null;
	url = "/app/freight-analytics";
	paramFields: Array<SearchField> = new Array<SearchField>();
	module = Modules.FREIGHT_ANALYTICS;
	Statique = Statique;

	config: AnalyticsConfigDTO;

	fetchUrl(event: any): void {
		history.replaceState(null, null, `${this.url}/${event.nextId}`);
	}

	constructor(private activeRoute: ActivatedRoute,
				protected headerService: HeaderService,
				protected freightAnalyticsService: FreightAnalyticsService,
				protected messageService: MessageService,
				protected translate: TranslateService,
	) {
		this.paramFields = require("../../../assets/ressources/jsonfiles/freight-analytics.json");
	}
	//Bouton extract data pour extraire toute les données qui se trouve dans kibana
	actionButtons: Array<HeaderInfos.ActionButton> = [
		{
			label: "GENERAL.EXTRACT_DATA",
			icon: 'fa fa-download',
			btnClass: "btn btn-primary",
			action: (jsEvent) => {
				//generation du csv sur kibana + download du csv
				this.exportKibanaData();
			},
		}
	];
	ngOnInit() {
		this.params = this.activeRoute.snapshot.params;
		this.headerService.registerActionButtons(this.actionButtons);
		this.loadConfig();
	}

	search(event: any) {}

	ngAfterViewInit() {
		if (this.params != null) {
			this.ngbTabSet.select(this.params.tab);
		}
		history.replaceState(null, null, `${this.url}/${this.ngbTabSet.activeId}`);
		this.onResize();
	}

	private loadConfig() {
		this.freightAnalyticsService.getConfig().subscribe(res => this.config = res);
	}

	@HostListener("window:resize", ["$event"])
	onResize() {
		let windowHeight = window.innerHeight;
		let listEltsClass: string[] = [".app-header", ".nav-tabs"];
		setTimeout(() => {
			this.Statique.onResizeAdapter(listEltsClass, "#iframe-kibana", 30, windowHeight);
		}, 200);
	}

	exportKibanaData() {
		this.freightAnalyticsService.getAllDataFromKibana(this.config.kibanaUrl).subscribe(res => {
			this.messageService.add({
				severity: "info",
				summary: this.translate.instant("GENERAL.EXPORT_STARTED"),
			});
			// attendre jusqu'a que kibana genere le csv (a trouver un solution meilleur)
			setTimeout(() => {
				window.open(this.config.kibanaUrl + "/" + res.path, "_blank");
				this.messageService.add({
					severity: "success",
					summary: this.translate.instant("GENERAL.EXPORT_FINISHED"),
				});
			}, 4000);

		});
	}
}
