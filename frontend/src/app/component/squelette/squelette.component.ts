import { Component, OnInit } from "@angular/core";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";
import { UploadOutput } from "ngx-uploader";
import { FileType } from "@app/utils/enumeration";
import { EbDemande } from "@app/classes/demande";
import { EbUser } from "@app/classes/user";
import { PricingService } from "@app/services/pricing.service";
import { Statique } from "@app/utils/statique";
import { DocumentUploadComponent } from "@app/shared/documents/document-upload.component";

@Component({
	selector: "app-squeltte",
	templateUrl: "./squelette.component.html",
	styleUrls: ["./squelette.component.css"],
})
export class SqueletteComponent extends DocumentUploadComponent implements OnInit {
	dataUpload: any;
	demande: EbDemande = new EbDemande();
	user: EbUser = new EbUser();
	dataSource: string;
	dataInsertion: string;
	model: string;
	currentJustify: string;

	constructor(private pricingService: PricingService, public generaleMethode: generaleMethodes) {
		super();

		this.dataUpload = { typeFile: String(this.UploadDirectory.PRICING_BOOKING) };
		this.componentObject = this;
	}

	commentModelinput: String = "";

	ngOnInit() {
		let demande = 12;
		this.dataSource = Statique.controllerStatiqueListe + "/listChat?demande=" + demande;
		this.dataInsertion = Statique.controllerStatiqueListe + "/addChat?demande=" + demande;
		this.user.nom = "amen";
		this.user.prenom = "nabil";
		this.user.ebUserNum = 1;

		this.demande.ebDemandeNum = 12;

		this.retrieveListDocuments();
	}

	ngAfterViewInit() {
		let lastOne = document.getElementsByClassName("nav-link")[
			document.getElementsByClassName("nav-link").length - 1
		];
		lastOne.classList.remove("nav-link");
	}

	panelCollapse(panel, event) {
		this.generaleMethode.panelCollapse(panel, event);
	}

	panelFullScreen(panel) {
		this.generaleMethode.panelFullScreen(panel);
	}

	panelCollapseTabs(panel) {
		var tabsBody = document.getElementsByClassName("tab-content")[0];
		this.generaleMethode.panelCollapse(tabsBody, panel);
	}

	onUploadFiles(output: UploadOutput, filetype): void {
		this.fileTypeDocumentNum = filetype;
		this.onUploadOutput(output);
	}

	retrieveListDocuments() {
		// faut envoyer l id de la demande
		// lors de laffichage
		this.pricingService.getListDocumentsByTypeDemande(0, 0, 0).subscribe((data) => {
			this.filesReturn[FileType.INTERCO_INVOICE] = data[FileType.INTERCO_INVOICE];
			this.filesReturn[FileType.COMMERCIAL_INVOICE] = data[FileType.COMMERCIAL_INVOICE];
			this.filesReturn[FileType.CUSTOMS] = data[FileType.CUSTOMS];
			this.filesReturn[FileType.TRANSPORT_DOCUMENT] = data[FileType.TRANSPORT_DOCUMENT];
			this.filesReturn[FileType.OTHER] = data[FileType.OTHER];
			this.filesReturn[FileType.POD] = data[FileType.POD];
			this.filesReturn[FileType.DELIVERY] = data[FileType.DELIVERY];
			this.filesReturn[FileType.ORDER] = data[FileType.ORDER];
		});
	}
}
