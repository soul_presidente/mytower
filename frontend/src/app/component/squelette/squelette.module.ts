import { NgModule } from "@angular/core";
import { SharedModule } from "@app/shared/module/shared.module";
import { NgxUploaderModule } from "ngx-uploader";
import { SqueletteComponent } from "./squelette.component";
import { SqueletteRoutes } from "./squelette.route";
import { PricingService } from "@app/services/pricing.service";
import { ChatService } from "@app/services/chat.service";

@NgModule({
	imports: [SharedModule, NgxUploaderModule, SqueletteRoutes],
	providers: [PricingService, ChatService],
	declarations: [SqueletteComponent],
	exports: [],
})
export class SqueletteModule {}
