import { Route, RouterModule } from "@angular/router";
import { SqueletteComponent } from "./squelette.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";

export const SQUELETTE_ROUTES: Route[] = [
	{
		path: "",
		component: SqueletteComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const SqueletteRoutes = RouterModule.forChild(SQUELETTE_ROUTES);
