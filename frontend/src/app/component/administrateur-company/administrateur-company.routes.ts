import { Route, RouterModule } from "@angular/router";
import { AdministrateurCompanyComponent } from "./administrateur-company.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";

export const ADMINISTRATION_COMPANY_ROUTES: Route[] = [
	{
		path: "",
		component: AdministrateurCompanyComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const AdministrateurCompanyRoutes = RouterModule.forChild(ADMINISTRATION_COMPANY_ROUTES);
