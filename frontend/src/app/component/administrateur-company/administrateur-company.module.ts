import { NgModule } from "@angular/core";
import { AdministrateurCompanyComponent } from "./administrateur-company.component";
import { AdministrateurCompanyRoutes } from "./administrateur-company.routes";
import { SharedModule } from "@app/shared/module/shared.module";
import { DataTablesModule } from "angular-datatables";

@NgModule({
	imports: [AdministrateurCompanyRoutes, SharedModule, DataTablesModule],
	declarations: [AdministrateurCompanyComponent],
	exports: [],
})
export class AdministrateurCompanyModule {}
