import { Component, OnInit, ViewContainerRef } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { EbEtablissement } from "@app/classes/etablissement";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { DialogProperties } from "@app/utils/dialogProperties";
import { EtablissementService } from "@app/services/etablissement.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { EbUser } from "@app/classes/user";
import { ModalComponent } from "@app/shared/modal/modal.component";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";

@Component({
	selector: "app-administrateur",
	templateUrl: "./administrateur-company.component.html",
	styleUrls: ["./administrateur-company.component.css"],
})
export class AdministrateurCompanyComponent extends ConnectedUserComponent implements OnInit {
	Statique = Statique;
	ebEtablissement: EbEtablissement = new EbEtablissement();
	dialogProperties: DialogProperties = new DialogProperties();
	isDetails: Boolean = false;
	userToBeUpdated: EbUser = new EbUser();

	constructor(
		protected vcr?: ViewContainerRef,
		protected etablissementService?: EtablissementService,
		protected ngbService?: NgbModal
	) {
		super();
	}

	ngOnInit() {
		let searchCriteria: SearchCriteria = new SearchCriteria();
		searchCriteria.ebEtablissementNum = this.userConnected.ebEtablissement.ebEtablissementNum;
		this.etablissementService.getEtablisement(searchCriteria).subscribe((data) => {
			this.ebEtablissement.constructorCopy(data);
		});
	}

	openDialog() {
		// return this.dialog.open(DialogResult, {
		//   data: this.dialogProperties
		// });
	}

	open() {
		//const modalRef =
		return this.ngbService.open(ModalComponent);
		//modalRef.componentInstance.name = 'Test Modal';
	}

	showSuccess() {
		//this.toastr.success("Operation complétée avec succès!", 'Succès');
	}

	showError() {
		//this.toastr.error("Une erreur est survenue", 'Erreur');
	}

	onDetails(val) {
		this.isDetails = val;
	}
	setUserTobeUpdated(val) {
		this.userToBeUpdated = val;
		console.log(this.userToBeUpdated.email);
	}
}
