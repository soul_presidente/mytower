import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { IDChatComponent, Modules, TypeOfEvent } from "@app/utils/enumeration";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { Statique } from "@app/utils/statique";
import { EbTracing } from "@app/classes/tracing";
import { SearchCriteriaTrackTrace } from "@app/utils/SearchCriteriaTrackTrace";
import { TrackTraceService } from "@app/services/trackTrace.service";
import { EbPslApp } from "@app/classes/pslApp";
import { StatiqueService } from "@app/services/statique.service";
import { EcCountry } from "@app/classes/country";
import { ModalService } from "@app/shared/modal/modal.service";
import { EbUser } from "@app/classes/user";
import { EbTtCategorieDeviation } from "@app/classes/categorieDeviation";
import { TTEvent } from "@app/classes/tTEvent";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { TranslateService } from "@ngx-translate/core";
import { ChatService } from "@app/services/chat.service";
import { EbChat } from "@app/classes/chat";
import { EbDemande } from "@app/classes/demande";
import { ChatPanelComponent } from "@app/shared/chat-panel/chat-panel.component";
import SingletonStatique from "@app/utils/SingletonStatique";
import { MessageService } from "primeng/api";
import { EbTtCompanyPsl } from "@app/classes/ttCompanyPsl";
import { RegroupmtPslEvent } from "@app/classes/regroupmtPslEvent";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { TypesDocumentService } from "@app/services/types-document.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { DocumentsComponent } from "@app/shared/documents/documents.component";
import { FichierJoint } from '@app/classes/fichiersJoint';
import {EbModeTransport} from "@app/classes/EbModeTransport";

@Component({
	selector: "app-track-trace-details",
	templateUrl: "./track-trace-details.component.html",
	styleUrls: ["./step-track/step-track.component.scss", "./track-trace-details.component.scss"],
})
export class TrackTraceDetailsComponent extends ConnectedUserComponent implements OnInit {
	isCollapsed = new Array<boolean>();
	Modules = Modules;
	Statique = Statique;
	modeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();
	IDChatComponent = IDChatComponent;
	ttPsl: Array<EbTtCompanyPsl> = new Array<EbTtCompanyPsl>();
	ebTracing: EbTracing = new EbTracing();
	ebTtTracingNum: number;
	module: number;
	variableModule: string;
	hasContributionAccess: boolean = false;
	listCountry: Array<EcCountry> = new Array<EcCountry>();
	listCategorieDeviation: Array<EbTtCategorieDeviation> = new Array<EbTtCategorieDeviation>();
	ebPslApp: EbPslApp = null;
	ttEvent: TTEvent = null;
	listEbTracing: Array<EbTracing> = new Array<EbTracing>();
	marginPsl: string;
	documentsToShow: Array<number> = new Array<number>();
	hasDeviation: boolean;
	nbrComment: number = 0;
	eventModalReadOnly: boolean = true;
	ebPslAppUpdate: TTEvent;
	ebPsl: EbPslApp;
	TypeOfEvent = TypeOfEvent;

	@ViewChild("modalAddEvent", { static: false })
	modalAddEvent: ElementRef;
	@ViewChild("chatPanel", { static: true })
	chatPanel: ChatPanelComponent;
	searchCriteria: SearchCriteria = new SearchCriteria();
	listTypeDoc: any;
	@ViewChild(DocumentsComponent, { static: false })
	documentsComponent: DocumentsComponent;
	nbColTexteditor: string = "col-md-4";
	nbColListing: string = "col-md-8";
	forUpdateRevisedDate: boolean = false;
	pieChart: any;
	pieChartOptions: any;
	listHistReparation: { date: string; icon: string; content: string }[];
	listPsl: Array<EbTtCompanyPsl> = new Array<EbTtCompanyPsl>();
	timelineEbPslAppList: Array<EbPslApp>;
	listDocuments: Array<FichierJoint>;

	constructor(
		private trackTraceService: TrackTraceService,
		private statiqueService: StatiqueService,
		protected headerService: HeaderService,
		private route: ActivatedRoute,
		private messageService: MessageService,
		protected modalService?: ModalService,
		protected chatService?: ChatService,
		private translate?: TranslateService,
		private router?: Router,
		private typeDocumentsService?: TypesDocumentService
	) {
		super();
		this.pieChart = {
			labels: ["In Service", "In Maintenance Repair Overall", "In Stock", "In Evaluation"],
			datasets: [
				{
					data: [78, 14, 6, 2],
					backgroundColor: ["#31C0A4", "#00C6DD", "#726ACB", "#FFB91C"],
					hoverBackgroundColor: ["#31C0A4", "#00C6DD", "#726ACB", "#FFB91C"],
				},
			],
		};
		this.pieChartOptions = {
			title: {
				display: true,
				text: "Suivi pièce",
				fontSize: 12,
			},
			legend: {
				position: "right",
				fontSize: 12,
			},
			tooltips: {
				callbacks: {
					label: function(tooltipItem, pieChart) {
						let allData = pieChart.datasets[tooltipItem.datasetIndex].data;
						let tooltipLabel = pieChart.labels[tooltipItem.index];
						let tooltipData = allData[tooltipItem.index];
						let total = 0;
						for (let i in allData) {
							total += allData[i];
						}
						let tooltipPercentage = Math.round((tooltipData / total) * 100);
						return tooltipPercentage + "%";
					},
				},
			},
		};
		this.listHistReparation = [
			{ date: "", icon: "full-icon", content: "Leg 1 (AFH-001): 12/11/2018" },
			{ date: "In Service   : 3 jours", icon: "no-icon", content: "" },
			{ date: "", icon: "full-icon", content: "Leg 2 (AFH-001) :  16/11/2018" },
			{ date: "In Maintenance Repair Overall :  12 Heure", icon: "no-icon", content: "" },
			{ date: "", icon: "full-icon", content: "Leg 3 (AFH-001) :  14/11/2018" },
			{ date: "In Stock : 10 heure", icon: "no-icon", content: "" },
			{ date: "", icon: "full-icon", content: "Leg 4 (AFH-002):  16/11/2018" },
			{ date: "In Evaluation : 8 heure", icon: "no-icon", content: "" },
			{ date: "", icon: "full-icon", content: "Leg 1 (AFH-054):  12/02/2019" },
			{ date: "In Service   : 2 jours", icon: "no-icon", content: "" },
			{ date: "", icon: "full-icon", content: "Leg 2 (AFH-054):  14/02/2019" },
			{ date: "In Maintenance Repair Overall :  10 Heure", icon: "no-icon", content: "" },
			{ date: "", icon: "full-icon", content: "Leg 3 (AFH-054):  16/02/2019" },
			{ date: "In Stock : 8 heure", icon: "no-icon", content: "" },
			{ date: "", icon: "full-icon", content: "Leg 4 (AFH-054):  16/02/2019" },
			{ date: "In Evaluation : 2 heure", icon: "no-icon", content: "" },
		];
	}

	onDocumentsLoaded(docs: any)
	{
		this.listDocuments = new Array<FichierJoint>();
		let listDoc = Object.assign([], docs);
		listDoc.forEach((arr: any[]) => {
			arr && arr.forEach(el => {
				el && this.listDocuments.push(Statique.cloneObject(el, new FichierJoint()));
			})
		})
		this.timelineEbPslAppList && this.timelineEbPslAppList.forEach(it =>
		{
			let filtered = this.listDocuments.filter(d => (it.nom == d.pslEvent));
			if(filtered && filtered.length)
				it.nbDoc = filtered.length;
		});
	}

	initTimelineEbPslAppList() {
		this.trackTraceService
			.getTracingPslAndsThiereEventsByEbTtTracingNum(this.ebTtTracingNum)
			.subscribe((dataResponse: Array<EbPslApp>) => {
				this.timelineEbPslAppList = dataResponse;
				this.timelineEbPslAppList.forEach((e) => {
					this.isCollapsed.push(true);
				});
			});
	}

	initEbTracing() {
		let searchCriteriaTrackTrace = new SearchCriteriaTrackTrace();
		searchCriteriaTrackTrace.ebTtTracingNum = this.ebTtTracingNum;
		searchCriteriaTrackTrace.ebUserNum = this.userConnected.ebUserNum;
		this.trackTraceService.getEbTracing(searchCriteriaTrackTrace).subscribe((data: EbTracing) => {
			this.ebTracing = data;

			this.searchCriteria.withDeactive = false;
			this.searchCriteria.module = Modules.TRACK;
			this.searchCriteria.ebCompagnieNum = this.ebTracing.xEbChargeurCompagnie.ebCompagnieNum;
			this.typeDocumentsService.getListTypeDoc(this.searchCriteria).subscribe((data) => {
				this.listTypeDoc = data;
			});

			this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());
		});
	}

	ngOnInit() {
		this.getStatiques();
		this.headerService.registerActionButtons(this.getActionButtons());

		this.route.params.subscribe((params: Params) => {
			this.ebTtTracingNum = +params["ebTtTracingNum"];
			this.initTimelineEbPslAppList();
			this.initEbTracing();
		});
	}

	updateChatEmitter(ebChat: EbChat) {
		if (
			this.chatPanel &&
			(!ebChat.idChatComponent || ebChat.idChatComponent == IDChatComponent.TRACK)
		)
			//this.chatPanel.listChat.unshift(ebChat);
			this.chatPanel.insertChat(ebChat);
	}

	getStatiques() {
		(async () => (this.listCountry = await SingletonStatique.getListEcCountry()))();
		(async () =>
			(this.listCategorieDeviation = await SingletonStatique.getListCategorieDeviation()))();
		(async () => (this.modeTransport = await SingletonStatique.getListModeTransport()))();

	}

	// TODO replace label values with translation key
	getActionButtons(): Array<HeaderInfos.ActionButton> {
		return [
			{
				label: "TRACK_TRACE.UPDATE",
				icon: "fa fa-plus-circle",
				action: this.popupUpdateAddClick.bind(this),
			},
			//TODO MEP-ce bout de code devra être réactivé après MEP
			/*,
			{
				label: "TRACK_TRACE.REVISED_DATE",
				icon: "fa fa-plus-circle",
				action: this.addRevisedDateEvent.bind(this),
			},*/
		];
	}

	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				label: "MODULES.TT",
				path: "/app/track-trace/dashboard",
			},
			{
				label: this.ebTracing.xEbDemande.refTransport,
				path: "/app/transport-management/details/" + this.ebTracing.xEbDemande.ebDemandeNum,
			},
			{
				label: this.ebTracing.customerReference,
			},
		];
	}

	popupUpdateAddClick() {
		this.prepareAddEventCommon();
		this.prepareAddEvent();
		this.prepareAddDeviation();
	}

	prepareAddEvent() {
		this.ebPslApp = new EbPslApp();
		if (!this.ebPslApp.dateActuelle) this.ebPslApp.dateActuelle = new Date();
		this.eventModalReadOnly = true;
	}

	prepareAddDeviation() {
		this.ttEvent = new TTEvent();
		this.ttEvent.dateEvent = new Date();
		this.ttEvent.xEbTtPslApp = new EbPslApp();
	}

	prepareAddRevisedDateEvent() {
		this.prepareAddEventCommon();
		this.ebPslApp = new EbPslApp();
		//this.ebPslApp.companyPsl = this.ebTracing.listEbPslApp[0].companyPsl;
		this.forUpdateRevisedDate = true;
		//this.ebPslApp.clone(pslapp);
		if (!this.ebPslApp.dateEstimee) this.ebPslApp.dateEstimee = new Date();

		this.eventModalReadOnly = true;
	}

	private prepareAddEventCommon() {
		if (
			this.ebTracing.xEbDemande.xEbSchemaPsl != null &&
			this.ebTracing.xEbDemande.xEbSchemaPsl.listPsl != null
		) {
			this.listPsl = this.ebTracing.xEbDemande.xEbSchemaPsl.listPsl;
		} else {
			this.messageService.add({
				severity: "warn",
				summary: this.translate.instant(
					"GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.ADDITIONALS.OBJECT_CORRUPTED"
				),
				detail: this.translate.instant("TRACK_TRACE.ERROR.NO_PSL"),
			});
			console.error("Demande object is corrupted, listPsl or xEbSchemaPsl is empty");
		}
	}

	confirmModalAddEvent(data: Object) {
		if (data && data["ebPslApp"] && !data["forUpdateRevisedDate"])
			this.confirmModalAddEventPsl(data["ebPslApp"], data["buttonHtmlEvent"]);
		else if (data && data["ebPslApp"] && data["forUpdateRevisedDate"])
			this.confirmModalAddRevisedDatePsl(data["ebPslApp"], data["buttonHtmlEvent"]);
		else if (data && data["ttEvent"])
			this.confirmModalAddEventDeviation(
				data["ttEvent"],
				data["typeOfEvent"],
				data["buttonHtmlEvent"]
			);
		else this.forUpdateRevisedDate = false;
		if (data == null) {
			this.ebPslAppUpdate = null;
			this.ebPsl = null;
		}
	}

	confirmModalAddEventPsl(ebPslApp: EbPslApp, htmlEvent: any) {
		if (this.ebPslApp == null) {
			this.ebPslApp = new EbPslApp();
		}
		this.ebPslApp.clone(ebPslApp);
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(htmlEvent);
		this.ebPslApp.validated = true;
		let ebPsl = new EbPslApp();
		ebPsl.clone(this.ebPslApp);
		ebPsl.companyPsl = this.ebPslApp.companyPsl;
		ebPsl.companyPsl["ebCompagnie"] = null;
		ebPsl.xEbUser = new EbUser();
		ebPsl.xEbUser.ebUserNum = this.userConnected.ebUserNum;
		ebPsl.xEbTrackTrace = new EbTracing();
		ebPsl.xEbTrackTrace.ebTtTracingNum = this.ebTracing.ebTtTracingNum;
		ebPsl.xEbTrackTrace.configPsl = this.ebTracing.configPsl;
		if (this.ebTracing.xEbDemande && this.ebTracing.xEbDemande.ebDemandeNum) {
			ebPsl.xEbTrackTrace.xEbDemande = new EbDemande();
			ebPsl.xEbTrackTrace.xEbDemande.ebDemandeNum = this.ebTracing.xEbDemande.ebDemandeNum;
		}
		this.trackTraceService.updateEbPslApp(ebPsl).subscribe(
			(res) => {
				this.showSuccess();
				this.initTimelineEbPslAppList();
				this.chatPanel.insertChat(res.ebChat);
				this.ebPslApp = null;
				requestProcessing.afterGetResponse(htmlEvent);
			},
			(error) => {
				requestProcessing.afterGetResponse(htmlEvent);
				this.showError();
			}
		);
	}

	confirmModalAddEventDeviation(data: TTEvent, typeOfEvent: number, htmlEvent: any) {
		let ttEvent: TTEvent = new TTEvent();
		let eventTT: RegroupmtPslEvent = new RegroupmtPslEvent();
		eventTT.coment = data.commentaire != null ? data.commentaire + "" : "" + "";
		eventTT.date = data.dateEvent;
		eventTT.nbrDoc = data.quantity;
		eventTT.type = 1;
		eventTT.validateByChamp = data.validateByChamp;
		if (Statique.isDefined(data.categorie)) eventTT.nature = data.categorie.libelle;
		eventTT.nom = data.xEbTtPslApp.companyPsl.codeAlpha;
		ttEvent.clone(data);
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(htmlEvent);
		ttEvent.xEbTtTracing = new EbTracing();
		ttEvent.xEbTtTracing.ebTtTracingNum = this.ebTracing.ebTtTracingNum;
		if (ttEvent.xEbTtPslApp) ttEvent.xEbTtPslApp.companyPsl["ebCompagnie"] = null;
		if (ttEvent.categorie && ttEvent.categorie.code == null) ttEvent.categorie = null;
		this.trackTraceService.addDeviation(ttEvent, typeOfEvent).subscribe(
			(res) => {
				this.initTimelineEbPslAppList();
				this.showSuccess();

				requestProcessing.afterGetResponse(htmlEvent);
				this.ttEvent = null;
			},
			(error) => {
				requestProcessing.afterGetResponse(htmlEvent);
				this.showError();
			}
		);
	}

	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE"),
		});
	}

	confirmModalAddRevisedDatePsl(ebPslApp: EbPslApp, htmlEvent: any) {
		this.ebPslApp.clone(ebPslApp);
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(htmlEvent);
		this.ebPslApp.validated = true;
		let ebPsl = new EbPslApp();
		ebPsl.clone(this.ebPslApp);
		ebPsl.companyPsl = this.ebPslApp.companyPsl;
		ebPsl.companyPsl["ebCompagnie"] = null;
		ebPsl.xEbUser = new EbUser();
		ebPsl.xEbUser.ebUserNum = this.userConnected.ebUserNum;
		ebPsl.xEbTrackTrace = new EbTracing();
		ebPsl.xEbTrackTrace.ebTtTracingNum = this.ebTracing.ebTtTracingNum;
		ebPsl.xEbTrackTrace.configPsl = this.ebTracing.configPsl;
		ebPsl.xEbTrackTrace.xEbDemande = new EbDemande();
		if (this.ebTracing.xEbDemande && this.ebTracing.xEbDemande.ebDemandeNum) {
			ebPsl.xEbTrackTrace.xEbDemande.ebDemandeNum = this.ebTracing.xEbDemande.ebDemandeNum;
		}

		this.trackTraceService.updateRevisedDateEbPslApp(ebPsl).subscribe(
			(res) => {
				this.initTimelineEbPslAppList();

				this.chatPanel.insertChat(res.ebChat);
				this.ebPslApp = null;
				this.forUpdateRevisedDate = false;

				requestProcessing.afterGetResponse(htmlEvent);
				this.showSuccess();
			},
			(error) => {
				requestProcessing.afterGetResponse(htmlEvent);

				this.showError();
			}
		);
	}
	isValidetedPslWithoutDev(ebPslApp: EbPslApp) {
		for (let event of ebPslApp.listEvent) {
			if (event.typeEvent == TypeOfEvent.INCIDENT || event.typeEvent == TypeOfEvent.UNCONFORMITY) {
				return false;
			}
		}
		return true;
	}

	getTimeLineStatusColor(ebPslApp: EbPslApp) {
		const hasDeviation :boolean = !this.isValidetedPslWithoutDev(ebPslApp);
		const isActual :boolean = ebPslApp.nature == 'ACTUAL_DATE';
		
		if(isActual) return hasDeviation ? 'WHITE' : 'BLUE';
		if(hasDeviation) return 'WHITE_GREY'
		return 'GREY'
	}

	refreshDocumentList(event: any) {
		this.documentsComponent.retrieveListDocuments();
	}

	refreshPSL($event, ttEvent: TTEvent, ebPslApp: EbPslApp) {
		this.ebPslAppUpdate = ttEvent;
		this.ebPsl = ebPslApp;
		this.prepareAddEventCommon();
	}
}
