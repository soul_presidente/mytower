import { Component, Input, OnInit, SimpleChanges, OnChanges } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { TypeImportance } from "@app/utils/enumeration";
import { TranslateService } from "@ngx-translate/core";
import { EbPslApp } from "@app/classes/pslApp";
@Component({
	selector: "app-step-track",
	templateUrl: "./step-track.component.html",
	styleUrls: ["./step-track.component.scss"],
})
export class StepTrackComponent implements OnInit, OnChanges {
	TypeImportance = TypeImportance;

	@Input()
	listOfPslApps?: Array<EbPslApp>;

	result: any = {
		nbrOfDays: 0,
		nbrOfHours: 0,
		nbrOfMin: 0,
	};

	constructor(private translate?: TranslateService) {}

	ngOnInit() {}
	ngOnChanges(changes: SimpleChanges) {
		let dateStart, dateEnd;

		if (this.listOfPslApps) {
			for (let psl of this.listOfPslApps) {
				if (psl.companyPsl.startPsl) {
					dateStart = psl.date;
				} else if (psl.companyPsl.endPsl) {
					dateEnd = psl.date;
				}
			}

			let now = new Date();
			if (dateStart && dateEnd && dateStart < dateEnd) {
				let getTimeAgoByHours: number = this.getTimeAgoByHours(dateStart, dateEnd);
				if (getTimeAgoByHours > 0) {
					this.getTimeRemaining(getTimeAgoByHours);
				}
			} else if (dateStart && dateStart < now) {
				let getTimeAgoByHours: number = this.getTimeAgoByHours(dateStart, now);
				if (getTimeAgoByHours > 0) {
					this.getTimeRemaining(getTimeAgoByHours);
				}
			} else {
				this.result = {
					nbrOfDays: 0,
					nbrOfHours: 0,
					nbrOfMin: 0,
				};
			}
		}
	}
	getTimeRemaining(delaiH: number) {
		let nbrTotalOfDays = delaiH / 24;
		let nbrOfDays = Math.floor(nbrTotalOfDays);
		let nbrTotalOfHours = Math.abs(nbrTotalOfDays - nbrOfDays) * 24;
		let nbrOfHours = Math.floor(nbrTotalOfHours);
		let nbrTotalOfMin = Math.abs(nbrTotalOfHours - nbrOfHours) * 60;
		let nbrOfMin = Math.ceil(nbrTotalOfMin);

		this.result = {
			nbrOfDays: nbrOfDays,
			nbrOfHours: nbrOfHours,
			nbrOfMin: nbrOfMin,
		};
	}
	getTimeAgoByHours(dateStart: Date, dateEnd: Date): number {
		let diff = Math.abs(new Date(dateStart).getTime() - new Date(dateEnd).getTime());
		return diff / (1000 * 3600);
	}
}
