import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TrackTraceRoutes } from "./track-trace.routes";
import { TrackTraceComponent } from "./track-trace.component";

import { Ng2CompleterModule } from "ng2-completer";
import { NgSelectModule } from "@ng-select/ng-select";
import { SharedModule } from "@app/shared/module/shared.module";
import { TrackTraceSearchComponent } from "./track-trace-search/track-trace-search.component";
import { TrackTraceListComponent } from "./track-trace-list/track-trace-list.component";
import { DataTablesModule } from "angular-datatables/src/angular-datatables.module";
import { TrackTraceDetailsComponent } from "./track-trace-details/track-trace-details.component";
import { TrackTraceService } from "@app/services/trackTrace.service";
import { StepTrackComponent } from "./track-trace-details/step-track/step-track.component";
import { ModalEventComponent } from "./shared-components/modal-event/modal-event.component";
import { NgxUploaderModule } from "ngx-uploader";
import { PricingService } from "@app/services/pricing.service";
import { TrackTraceDetailsNewVersionComponent } from "./track-trace-details-new-version/track-trace-details-new-version.component";
import { StepTrackNewVersionComponent } from "./track-trace-details-new-version/step-track-new-version/step-track-new-version.component";
import { ModalEventDeviationComponent } from "./track-trace-details-new-version/modal-event-deviation/modal-event-deviation.component";
import { ChartModule } from "primeng/chart";

@NgModule({
	imports: [
		CommonModule,
		TrackTraceRoutes,
		DataTablesModule,
		NgxUploaderModule,
		NgSelectModule,
		SharedModule,
		Ng2CompleterModule,
		ChartModule,
	],
	declarations: [
		TrackTraceComponent,
		TrackTraceSearchComponent,
		TrackTraceListComponent,
		TrackTraceDetailsComponent,
		StepTrackComponent,
		ModalEventComponent,
		TrackTraceDetailsNewVersionComponent,
		StepTrackNewVersionComponent,
		ModalEventDeviationComponent,
	],
	exports: [],
	providers: [TrackTraceService, PricingService],
})
export class TrackTraceModule {}
