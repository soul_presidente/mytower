import { Component, Input, OnInit } from "@angular/core";
import { EcCountry } from "@app/classes/country";
import { EbPslApp } from "@app/classes/pslApp";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { EbTtCategorieDeviation } from "@app/classes/categorieDeviation";
import { EnumPsl } from "@app/utils/enumeration";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbTtPsl } from "@app/classes/Ttpsl";

@Component({
	selector: "app-modal-event-deviation",
	templateUrl: "./modal-event-deviation.component.html",
	styleUrls: [
		"../../../../shared/modal/modal.component.scss",
		"./modal-event-deviation.component.css",
	],
})
export class ModalEventDeviationComponent implements OnInit {
	@Input()
	readOnly?: boolean;
	@Input()
	listCountry: Array<EcCountry>;
	@Input()
	set ebPslApp(pslApp: EbPslApp) {
		this.ebPslAppValue = pslApp;
	}
	ebPslAppValue: EbPslApp;
	/* listDeviationCategorie: Array<MTEnum> = new Array<MTEnum>(); */

	EnumPsl = EnumPsl;
	read: Boolean = true;
	listCategorieDeviation: Array<EbTtCategorieDeviation> = new Array<EbTtCategorieDeviation>();
	ttPsl: Array<EbTtPsl> = new Array<EbTtPsl>();

	constructor(public activeModal: NgbActiveModal) {}

	ngOnInit() {
		this.getStatiques();
	}

	getStatiques() {
		(async () => (this.listCountry = await SingletonStatique.getListEcCountry()))();
		(async () =>
			(this.listCategorieDeviation = await SingletonStatique.getListCategorieDeviation()))();
		(async () => (this.ttPsl = await SingletonStatique.getListTtPsl()))();
	}

	cancelModalAddEventDeviation() {
		this.ebPslAppValue = null;
	}
}
