import { Component, Input, OnInit } from "@angular/core";
import { Statique } from "@app/utils/statique";
import { Modules } from "@app/utils/enumeration";
import { EbUser } from "@app/classes/user";
import { EbTracing } from "@app/classes/tracing";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbPslApp } from "@app/classes/pslApp";

@Component({
	selector: "app-step-track-new-version",
	templateUrl: "./step-track-new-version.component.html",
	styleUrls: ["./step-track-new-version.component.scss"],
})
export class StepTrackNewVersionComponent implements OnInit {
	@Input()
	ebTracing: EbTracing;
	@Input()
	module: number;
	@Input()
	userConnected: EbUser;
	@Input()
	hasContributionAccess?: boolean;
	@Input()
	columnNbr: string;
	@Input()
	nbrComment: number;
	@Input()
	hasDeviation: boolean;

	ebpslapp: EbPslApp;

	Modules = Modules;
	Statique = Statique;

	listModeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();

	constructor() {}

	ngOnInit() {
		if (typeof this.hasContributionAccess == "undefined") this.hasContributionAccess = true;

		this.getStatiques();
	}

	private getStatiques() {
		(async () => {
			this.listModeTransport = await SingletonStatique.getListModeTransport();
			this.listModeTransport = this.listModeTransport.sort((a, b) => a.order - b.order);
		})();
	}

	getModeTransportIcon() {
		let result = "";
		this.listModeTransport.forEach((element) => {
			if (this.ebTracing.xEbDemande.xEcModeTransport == element.code) result = element.icon;
		});
		return result;
	}

	clickeventebpslapp(ebpslapp: EbPslApp) {
		this.ebpslapp = ebpslapp;
	}
}
