import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { AccessRights, EnumPsl, FileType, IDChatComponent, Modules } from "@app/utils/enumeration";
import { ActivatedRoute, Params, Router } from "@angular/router";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { Statique } from "@app/utils/statique";
import { EbTracing } from "@app/classes/tracing";
import { SearchCriteriaTrackTrace } from "@app/utils/SearchCriteriaTrackTrace";
import { TrackTraceService } from "@app/services/trackTrace.service";
import { EbPslApp } from "@app/classes/pslApp";
import { StatiqueService } from "@app/services/statique.service";
import { EcCountry } from "@app/classes/country";
import { ModalService } from "@app/shared/modal/modal.service";
import { EbUser } from "@app/classes/user";
import { EbTtCategorieDeviation } from "@app/classes/categorieDeviation";
import { TTEvent } from "@app/classes/tTEvent";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { TranslateService } from "@ngx-translate/core";
import { ChatService } from "@app/services/chat.service";
import { EbChat } from "@app/classes/chat";
import { EbDemande } from "@app/classes/demande";
import { EbTtPsl } from "@app/classes/Ttpsl";
import { ChatPanelComponent } from "@app/shared/chat-panel/chat-panel.component";
import SingletonStatique from "@app/utils/SingletonStatique";
import { MessageService } from "primeng/api";

@Component({
	selector: "app-track-trace-details-new-version",
	templateUrl: "./track-trace-details-new-version.component.html",
	styleUrls: [
		"./step-track-new-version/step-track-new-version.component.scss",
		"./track-trace-details-new-version.component.scss",
	],
})
export class TrackTraceDetailsNewVersionComponent extends ConnectedUserComponent implements OnInit {
	Modules = Modules;
	Statique = Statique;
	IDChatComponent = IDChatComponent;
	ttPsl: Array<EbTtPsl> = new Array<EbTtPsl>();
	ebTracing: EbTracing = new EbTracing();
	ebTtTracingNum: number;
	module: number;
	variableModule: string;
	hasContributionAccess: boolean = false;
	listCountry: Array<EcCountry> = new Array<EcCountry>();
	listCategorieDeviation: Array<EbTtCategorieDeviation> = new Array<EbTtCategorieDeviation>();
	ebPslApp: EbPslApp = null;
	ttEvent: TTEvent = null;
	listEbTracing: Array<EbTracing> = new Array<EbTracing>();
	marginPsl: string;
	documentsToShow: Array<number> = new Array<number>();
	hasDeviation: boolean;
	nbrComment: number = 0;
	eventModalReadOnly: boolean = true;

	@ViewChild("modalAddEvent", { static: false })
	modalAddEvent: ElementRef;
	@ViewChild("chatPanel", { static: false })
	chatPanel: ChatPanelComponent;
	nbColTexteditor: string = "col-md-12";
	nbColListing: string = "col-md-12";
	toggleChat: boolean = false;

	constructor(
		private trackTraceService: TrackTraceService,
		private statiqueService: StatiqueService,
		private route: ActivatedRoute,
		private messageService: MessageService,
		protected modalService?: ModalService,
		protected chatService?: ChatService,
		private translate?: TranslateService,
		private router?: Router
	) {
		super();
	}

	ngOnInit() {
		this.getStatiques();
		this.module = Modules.TRACK;

		this.route.params.subscribe((params: Params) => {
			this.ebTtTracingNum = +params["ebTtTracingNum"];
		});

		let searchCriteriaTrackTrace = new SearchCriteriaTrackTrace();
		searchCriteriaTrackTrace.ebTtTracingNum = this.ebTtTracingNum;
		searchCriteriaTrackTrace.ebUserNum = this.userConnected.ebUserNum;
		this.trackTraceService.getEbTracing(searchCriteriaTrackTrace).subscribe((data) => {
			if (data && data.ebTtTracingNum) {
				let ebTracing = new EbTracing();
				ebTracing.clone(data);
				let listPsl = new Array<EbPslApp>();
				let active = true;

				// this.ttPsl.forEach((it) => {
				// 	let ebPsl = ebTracing.listEbPslApp.find((el) => el.codePsl["code"] == it.code);
				// 	if (!ebPsl) {
				// 		ebPsl = new EbPslApp();
				// 		ebPsl.exists = false;
				// 	} else ebPsl.exists = true;
				// 	ebPsl.codePslLibelle = it.libelle;
				// 	ebPsl.xEbTrackTrace = new EbTracing();
				// 	ebPsl.xEbTrackTrace.ebTtTracingNum = ebTracing.ebTtTracingNum;
				// 	if (active && !ebPsl.validated && ebPsl.exists) {
				// 		ebPsl.active = true;
				// 		active = false;
				// 	}
				// 	if (ebPsl.exists) listPsl.push(ebPsl);
				// 	if (ebPsl.listEvent && ebPsl.listEvent.length > 0) this.hasDeviation = true;

				// 	if (ebPsl.commentaire && ebPsl.commentaire.trim().length > 0) this.nbrComment++;
				// });
				/*   Statique.TtPsl.forEach(it => {
                    let ebPsl = ebTracing.listEbPslApp.find(el => el.codePsl["code"] == it.key);
                    if (!ebPsl) {
                        ebPsl = new EbPslApp();
                        ebPsl.exists = false;
                    }
                    else ebPsl.exists = true;
                    ebPsl.codePslLibelle = it.value;
                    ebPsl.xEbTrackTrace = new EbTracing();
                    ebPsl.xEbTrackTrace.ebTtTracingNum = ebTracing.ebTtTracingNum;
                    if (active && !ebPsl.validated && ebPsl.exists) {
                        ebPsl.active = true;
                        active = false;
                    }
                    if (ebPsl.exists)
                        listPsl.push(ebPsl);
                    if (ebPsl.listEvent && ebPsl.listEvent.length > 0)
                        this.hasDeviation = true;

                    if (ebPsl.commentaire && ebPsl.commentaire.trim().length > 0)
                        this.nbrComment++;

                }); */

				ebTracing.listEbPslApp = listPsl;
				this.ebTracing = ebTracing;
				this.marginPsl =
					ebTracing.listEbPslApp.length == 1
						? "margin-37"
						: ebTracing.listEbPslApp.length == 2
						? "margin-12"
						: ebTracing.listEbPslApp.length == 3
						? "margin-5"
						: ebTracing.listEbPslApp.length == 4
						? "margin-2"
						: "";

				this.getListEbTracingByEbDemandeNum(this.ebTracing.xEbDemande.ebDemandeNum);
			} else {
				this.variableModule = "tracing ";
				let url: string = "failedaccess";
				this.router.navigate([url, this.ebTtTracingNum, this.variableModule]);
			}
		});

		// contribution access rights to module
		if (this.userConnected.listAccessRights && this.userConnected.listAccessRights.length > 0) {
			this.hasContributionAccess = !this.userConnected.listAccessRights.find((access) => {
				if (access.ecModuleNum == this.module && access.accessRight != AccessRights.CONTRIBUTION)
					return true;
			});
		} else this.hasContributionAccess = this.isControlTower;

		this.documentsToShow.push(FileType.POD);

		this.getStatiques();

		document.addEventListener(
			"open-chat",
			function(e) {
				event.preventDefault();
				event.stopPropagation();
				this.openChat();
			}.bind(this),
			false
		);
	}

	updateChatEmitter(ebChat: EbChat) {
		if (
			this.chatPanel &&
			(!ebChat.idChatComponent || ebChat.idChatComponent == IDChatComponent.PRICING)
		)
			this.chatPanel.listChat.unshift(ebChat);
	}

	getStatiques() {
		(async () => (this.listCountry = await SingletonStatique.getListEcCountry()))();
		(async () =>
			(this.listCategorieDeviation = await SingletonStatique.getListCategorieDeviation()))();
		(async () => (this.ttPsl = await SingletonStatique.getListTtPsl()))();
	}

	addEvent(pslapp) {
		this.ebPslApp = new EbPslApp();
		this.ebPslApp.clone(pslapp);
		if (!this.ebPslApp.dateActuelle) this.ebPslApp.dateActuelle = new Date();
		// if (this.ebPslApp.codePsl.code == EnumPsl.CUSTOMS) this.eventModalReadOnly = false;
		else this.eventModalReadOnly = true;
	}

	addDeviation(pslapp) {
		this.ttEvent = new TTEvent();
		this.ttEvent.dateEvent = new Date();
		this.ttEvent.xEbTtPslApp = new EbPslApp();
		this.ttEvent.xEbTtPslApp.ebTtPslAppNum = pslapp.ebTtPslAppNum;
		this.ttEvent.xEbTtPslApp.codePslLibelle = pslapp.codePsl.libelle;
	}

	confirmModalAddEvent(data: Object) {
		if (data && data["ebPslApp"]) this.confirmModalAddEventPsl(data["ebPslApp"]);
		else if (data && data["ttEvent"]) this.confirmModalAddEventDeviation(data["ttEvent"]);
	}

	confirmModalAddEventPsl(ebPslApp: EbPslApp) {
		this.ebPslApp.clone(ebPslApp);
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.ebPslApp.validated = true;
		let ebPsl = new EbPslApp();
		ebPsl.clone(this.ebPslApp);
		// ebPsl.codePsl = null;
		ebPsl.xEbUser = new EbUser();
		ebPsl.xEbUser.ebUserNum = this.userConnected.ebUserNum;
		ebPsl.xEbTrackTrace = new EbTracing();
		ebPsl.xEbTrackTrace.ebTtTracingNum = this.ebTracing.ebTtTracingNum;
		ebPsl.xEbTrackTrace.configPsl = this.ebTracing.configPsl;
		if (this.ebTracing.xEbDemande && this.ebTracing.xEbDemande.ebDemandeNum) {
			ebPsl.xEbTrackTrace.xEbDemande = new EbDemande();
			ebPsl.xEbTrackTrace.xEbDemande.ebDemandeNum = this.ebTracing.xEbDemande.ebDemandeNum;
		}

		this.trackTraceService.updateEbPslApp(ebPsl).subscribe(
			(res) => {
				this.ebTracing.listEbPslApp.some((it) => {
					if (it.ebTtPslAppNum == this.ebPslApp.ebTtPslAppNum) {
						it.clone(this.ebPslApp);
						return true;
					}
				});

				// if (
				// 	!this.ebTracing.codePslCourant ||
				// 	this.ebTracing.codePslCourant.ordre < this.ebPslApp.codePsl.ordre
				// )
				// 	this.ebTracing.codePslCourant = this.ebPslApp.codePsl;

				this.chatPanel.insertChat(res.ebChat);
				// this.historizeChat(this.ebPslApp.codePsl.libelle, this.ebPslApp.commentaire, 'evenement');
				this.ebPslApp = null;
				requestProcessing.afterGetResponse(event);
				this.showSuccess();
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				// let title = null;
				// let body = null;
				// this.ebPslApp = null;
				// this.translate.get("MODAL.ERROR").subscribe((res: string) => {
				//   title = res;
				// });
				// // this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
				// this.modalService.error(title, null, function() {});
				this.showError();
			}
		);
	}

	confirmModalAddEventDeviation(data: TTEvent) {
		let ttEvent: TTEvent = new TTEvent();
		ttEvent.clone(data);
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		ttEvent.xEbTtTracing = new EbTracing();
		ttEvent.xEbTtTracing.ebTtTracingNum = this.ebTracing.ebTtTracingNum;

		this.trackTraceService.addDeviation(ttEvent).subscribe(
			(res) => {
				let ebPslApp = res
					? this.ebTracing.listEbPslApp.find(
							(it) => it.ebTtPslAppNum == res.ebTtEvent.xEbTtPslApp.ebTtPslAppNum
					  )
					: null;
				if (res && res.ebChat && res.ebTtEvent) {
					res.ebTtEvent.xEbTtPslApp = null;
					res.ebTtEvent.xEbTtTracing = null;
					ebPslApp.listEvent.push(res.ebTtEvent);
					this.hasDeviation = true;
					// this.historizeChat(ebPslApp.codePsl.libelle, ttEvent.commentaire, 'deviation');

					this.chatPanel.insertChat(res.ebChat);

					requestProcessing.afterGetResponse(event);
				}
				this.ttEvent = null;
				this.showSuccess();
			},
			(error) => {
				// requestProcessing.afterGetResponse(event);
				// let title = null;
				// let body = null;
				// this.ttEvent = null;
				// this.translate.get("MODAL.ERROR").subscribe((res: string) => {
				//   title = res;
				// });
				// // this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
				// this.modalService.error(title, null, function() {});
				this.showError();
			}
		);
	}

	getListEbTracingByEbDemandeNum(ebDemandeNum: number) {
		this.trackTraceService.getListEbTracingByEbDemandeNum(ebDemandeNum).subscribe((res) => {
			this.listEbTracing = res;
		});
	}

	/*
    historizeChat(pslapp: string, commentaire: String, action: string) {
        let ebChat = new EbChat();
        ebChat.userName = this.userConnected.nom + " " + this.userConnected.prenom;
        ebChat.xEbUser = this.userConnected.ebUserNum;
        ebChat.module = this.module;
        ebChat.idFiche = this.ebTracing.ebTtTracingNum;
        ebChat.isHistory = true;

        if (this.module == Modules.TRACK)
            ebChat.idChatComponent = IDChatComponent.TRACK;
        else return;



        if (this.userConnected.realUserNum) {

            if (commentaire) {
                ebChat.text = "Mise a jour" + " " + action + " " + pslapp + " by <b> " + this.userConnected.realUserNom + " " + this.userConnected.realUserPrenom + '</b>';
                ebChat.text += " For <b> " + this.userConnected.nom + " " + this.userConnected.prenom + '</b>' + " avec le commentaire : " + commentaire;
            }
            else {
                ebChat.text = "Mise a jour" + " " + action + " " + pslapp + " by <b> " + this.userConnected.realUserNom + " " + this.userConnected.realUserPrenom + '</b>';
                ebChat.text += " For <b> " + this.userConnected.nom + " " + this.userConnected.prenom + '</b>';
            }
        }
        else {
            if (commentaire) {
                ebChat.text = "Mise a jour" + " " + action + " " + pslapp + " by <b> " + this.userConnected.nom + " " + this.userConnected.prenom + '</b>' + " avec le commentaire : " + commentaire;
            } else {
                ebChat.text = "Mise a jour" + " " + action + " " + pslapp + " by <b> " + this.userConnected.nom + " " + this.userConnected.prenom + '</b>';

            }
        }
        this.chatService.addChat(Statique.controllerStatiqueListe + '/addChat', ebChat).subscribe((data) => {
        });
    }*/

	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE"),
		});
	}
	openChat() {
		this.toggleChat = !this.toggleChat;
	}
}
