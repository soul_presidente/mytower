import { Component, OnInit, ViewChild } from "@angular/core";
import { Modules } from "@app/utils/enumeration";
import { SearchCriteriaTrackTrace } from "@app/utils/SearchCriteriaTrackTrace";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { TrackTraceSearchComponent } from "./track-trace-search/track-trace-search.component";
import { ActivatedRoute, Params } from "@angular/router";

@Component({
	selector: "app-track-trace",
	templateUrl: "./track-trace.component.html",
	styleUrls: ["./track-trace.component.css"],
})
export class TrackTraceComponent extends ConnectedUserComponent implements OnInit {
	@ViewChild("trackTraceSearchComponent", { static: true })
	trackTraceSearchComponent: TrackTraceSearchComponent;

	searchInput: SearchCriteriaTrackTrace;
	Modules: Modules;
	listFields: Array<IField>;
	listCategorie: Array<EbCategorie>;

	public module: number;
	ebDemandeNum: number;

	constructor(protected route: ActivatedRoute) {
		super();
	}

	ngOnInit() {
		this.module = Modules.TRACK;
		this.route.params.subscribe((params: Params) => {
			this.ebDemandeNum = params["ebDemandeNum"];
			if (this.ebDemandeNum) {
				this.searchInput = new SearchCriteriaTrackTrace();
				this.searchInput.ebDemandeNum = this.ebDemandeNum;
			}
		});
	}
	search(searchInput: SearchCriteriaTrackTrace) {
		this.searchInput = searchInput;
	}
	paramFieldsHandler(pListFields) {
		this.listFields = pListFields;
	}
	CategorieHandler(plistCategorie) {
		this.listCategorie = plistCategorie;
	}
}
