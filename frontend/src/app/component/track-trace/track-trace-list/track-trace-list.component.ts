import {
	Component,
	ElementRef,
	EventEmitter,
	Input,
	OnChanges,
	OnInit,
	Output,
	SimpleChanges,
	ViewChild,
} from "@angular/core";
import { Statique } from "@app/utils/statique";
import { StatiqueService } from "@app/services/statique.service";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { SearchCriteriaTrackTrace } from "@app/utils/SearchCriteriaTrackTrace";
import { GenericTableScreen, ModeTransport, Modules } from "@app/utils/enumeration";
import { ExportService } from "@app/services/export.service";
import { TranslateService } from "@ngx-translate/core";
import { ActivatedRoute, Router } from "@angular/router";
import { EbTracing } from "@app/classes/tracing";
import { TrackTraceService } from "@app/services/trackTrace.service";
import { EbPslApp } from "@app/classes/pslApp";
import { EbUser } from "@app/classes/user";
import { EcCountry } from "@app/classes/country";
import { EbTtCategorieDeviation } from "@app/classes/categorieDeviation";
import { TTEvent } from "@app/classes/tTEvent";
import { RequestProcessing } from "@app/utils/requestProcessing";
import { ModalService } from "@app/shared/modal/modal.service";
import { EtablissementService } from "@app/services/etablissement.service";
import EbCustomField, { IField } from "@app/classes/customField";
import { EbCategorie } from "@app/classes/categorie";
import { EbChat } from "@app/classes/chat";
import { ChatService } from "@app/services/chat.service";
import { TrackTraceSearchComponent } from "../track-trace-search/track-trace-search.component";
import { FavoriService } from "@app/services/favori.service";
import { EbFavori } from "@app/classes/favori";
import { Subscription } from "rxjs";
import { EbTtPsl } from "@app/classes/Ttpsl";
import { EbModeTransport } from "@app/classes/EbModeTransport";
import SingletonStatique from "@app/utils/SingletonStatique";
import { EbTypeTransport } from "@app/classes/EbTypeTransport";
import { GenericTableCcpComponent } from "@app/shared/generic-table-ccp/generic-table-ccp.component";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { EbFlag } from "@app/classes/ebFlag";
import { MessageService } from "primeng/api";
import { EbTtCompanyPsl } from "@app/classes/ttCompanyPsl";
import { HeaderInfos } from "@app/shared/header/header-infos";
import { HeaderService } from "@app/services/header.service";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { EbDemande } from "@app/classes/demande";
import { FlagColumnGenericCell } from "@app/shared/generic-cell/commons/flag-column.generic-cell";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { TypesDocumentService } from "@app/services/types-document.service";

declare global {
	interface Window {
		functions: any;
	}
}

@Component({
	selector: "app-track-trace-list",
	templateUrl: "./track-trace-list.component.html",
	styleUrls: ["./track-trace-list.component.scss"],
})
export class TrackTraceListComponent extends ConnectedUserComponent implements OnInit, OnChanges {
	statique = Statique;
	Modules = Modules;

	subscriptions: Array<Subscription> = new Array<Subscription>();
	listFavoris: Map<number, EbFavori> = new Map();
	ttPsl: Array<EbTtPsl> = new Array<EbTtPsl>();
	modeTransport: Array<EbModeTransport> = new Array<EbModeTransport>();
	listTracking: Map<number, EbTracing> = new Map();
	ebPslApp: EbPslApp = null;
	forUpdateRevisedDate: boolean = false;
	listDestinations: any;
	count: number;
	diff: number;
	listPsl: Array<EbTtCompanyPsl> = new Array<EbTtCompanyPsl>();
	listPslApp: Array<EbPslApp> = new Array<EbPslApp>();
	listConfigPsl: Array<String> = new Array<String>();
	listTTEvent: Array<TTEvent> = new Array<TTEvent>();
	ttEvent: TTEvent = null;
	listCountry: Array<EcCountry> = new Array<EcCountry>();
	listCategorieDeviation: Array<EbTtCategorieDeviation> = new Array<EbTtCategorieDeviation>();
	ConstantsTranslate: Object = {
		STATUS: "",
		NOT_AWARDED: "",
		DELETE_PRICING_HEAD: "",
		DELETE_PRICING_TEXT: "",
	};
	listFields: Array<IField>;
	listCategorie: Array<EbCategorie> = new Array<EbCategorie>();
	initiateState: boolean = false;
	globalSearchCriteria: SearchCriteriaTrackTrace = new SearchCriteriaTrackTrace();

	ebDemandeNumParam: number;
	isUpdatePsl: boolean;
	displayAddEvent: boolean;
	isEventsAndParamsInitialised: boolean = false;
	isFirstRefresh: boolean = true;
	@Input()
	trackTraceSearchComponent: TrackTraceSearchComponent;

	// genericTable attributes
	@ViewChild("genericTable", { static: false })
	genericTable: GenericTableCcpComponent;
	dataInfos: GenericTableInfos = new GenericTableInfos(this);

	@Output()
	listFieldsEmitter = new EventEmitter<Array<IField>>();
	@Output()
	listCategorieEmitter = new EventEmitter<Array<EbCategorie>>();
	@Input()
	module: number;
	@Input()
	ebDemandeNum: number;
	datatableComponent: number;
	@Output()
	updateChatEmitter = new EventEmitter<EbChat>();
	private listTypeTransportMap: Map<number, Array<EbTypeTransport>>;
	@Input("searchInput")
	set searchInput(searchInput: SearchCriteriaTrackTrace) {
		if (searchInput != null) {
			let tmpSearchInput: SearchCriteriaTrackTrace = new SearchCriteriaTrackTrace();

			tmpSearchInput.module = this.module;
			tmpSearchInput.ebUserNum = this.userConnected.ebUserNum;
			tmpSearchInput.searchInput = JSON.stringify(searchInput || {});

			this.globalSearchCriteria = tmpSearchInput;
		}
	}

	searchCriteria: SearchCriteria = new SearchCriteria();
	listTypeDoc: any;
	ebDemandeNumModalEvent: Number;
	ebCompagnieNumModalEvent: Number;

	constructor(
		private statiqueService: StatiqueService,
		private elRef: ElementRef,
		protected router: Router,
		protected activatedRoute: ActivatedRoute,
		protected headerService: HeaderService,
		protected exportService?: ExportService,
		private translate?: TranslateService,
		private tracktraceService?: TrackTraceService,
		protected modalService?: ModalService,
		protected chatService?: ChatService,
		protected etablissementService?: EtablissementService,
		private favoriService?: FavoriService,
		private messageService?: MessageService,
		private typeDocumentsService?: TypesDocumentService
	) {
		super();
	}

	ngOnInit() {
		this.datatableComponent = this.getDatatableCompId();
		this.headerService.registerBreadcrumbItems(this.getBreadcrumbItems());
		this.headerService.registerActionButtons(this.getActionButtons());

		this.getStatiques();
		window.functions = window.functions || {};
		window.functions.clickCheckbox = this.clickCheckbox.bind(this);
		window.functions.checkAllTrackTrace = this.checkAllTrackTrace.bind(this);
		window.functions.clickFavori = this.clickFavori.bind(this);

		Object.keys(this.ConstantsTranslate).forEach((key) => {
			this.translate
				.get((key.indexOf(".") < 0 ? "TRACK_TRACE." : "") + key)
				.subscribe((res) => (this.ConstantsTranslate[key] = res));
		});

		this.subscriptions.push(
			this.favoriService.listingFavorisObservale.subscribe((res: EbFavori) => {
				this.listFavoris.delete(res.idObject);
				document.getElementById(res.refObject).classList.remove("fa-star");
				document.getElementById(res.refObject).classList.add("fa-star-o");
			})
		);

		this.favoriService
			.getListFavoris(Statique.dataSourceFavoris + "/list-favori" + "?module=" + this.module)
			.subscribe((data: Array<EbFavori>) => {
				data.forEach((favori: EbFavori) => {
					this.listFavoris.set(favori.idObject, favori);
				});
			});
		this.searchCriteria.withDeactive = false;
		this.searchCriteria.module = Modules.TRACK;
	}
	private getBreadcrumbItems(): Array<HeaderInfos.BreadCrumbItem> {
		return [
			{
				icon: "my-icon-track",
				label: "MODULES.TT",
				path: "/app/track-trace/dashboard",
			},
		];
	}

	getActionButtons(): Array<HeaderInfos.ActionButton> {
		return [
			{
				label: "TRACK_TRACE.UPDATE",
				icon: "fa fa-plus-circle",
				action: this.popupUpdateAddClick.bind(this),
				enableCondition: () => {
					return this.listTracking && this.listTracking.size > 0;
				},
			},
			//TODO MEP-ce bout de code devra être réactivé après MEP
			/*
			{
				label: "TRACK_TRACE.REVISED_DATE",
				icon: "fa fa-plus-circle",
				action: this.handleNewRevisedDateEvent.bind(this),
				enableCondition: () => {
					return this.listTracking && this.listTracking.size > 0;
				},
			},*/
		];
	}

	getListTrackTrace(): Map<number, EbTracing> {
		let data = this.genericTable.getSelectedRows();
		let mapData = new Map<number, EbTracing>();

		this.listTracking = new Map<number, EbTracing>();
		data.forEach((it) => {
			mapData.set(it.ebTtTracingNum, it);
		});

		this.listTracking = mapData;
		if (data.length > 0 && data[0].xEbChargeur != null) {
			this.ebDemandeNumModalEvent = data[0].xEbDemande.ebDemandeNum;
			this.ebCompagnieNumModalEvent = data[0].xEbChargeur.ebCompagnie.ebCompagnieNum;

			this.listTypeDoc = [];
			let idCmp = data[0].xEbChargeur.ebCompagnie.ebCompagnieNum,
				hasSameCompagnie: boolean = Statique.isDefined(idCmp);
			mapData.forEach((it) => {
				if (it.xEbChargeur.ebCompagnie.ebCompagnieNum != idCmp) hasSameCompagnie = false;
			});
			if (hasSameCompagnie) {
				this.searchCriteria.ebCompagnieNum = idCmp;
				this.typeDocumentsService.getListTypeDoc(this.searchCriteria).subscribe((data) => {
					this.listTypeDoc = data;
				});
			}
		}
		return mapData;
	}

	popupUpdateAddClick() {
		this.getListTrackTrace();
		this.handleNewEventEvent();
		this.handleNewDeviationEvent();
	}

	handleNewEventEvent() {
		this.addNewEvent();
	}
	handleNewDeviationEvent() {
		this.addNewDeviation();
	}

	handleNewRevisedDateEvent() {
		this.getListTrackTrace();
		this.addNewRevisedDate();
	}

	clickFavori(ebDemandeNum: number, refObject: string, event: HTMLElement) {
		this.favoriService.addOrRemoveFavori(
			this.listFavoris,
			ebDemandeNum,
			refObject,
			this.module,
			this.userConnected,
			event
		);
	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes["module"] || (changes["searchInput"] && changes["searchInput"].firstChange)) {
			this.initDatatable();
		}
	}

	async getStatiques() {
		(async () => (this.listCountry = await SingletonStatique.getListEcCountry()))();
		(async () =>
			(this.listCategorieDeviation = await SingletonStatique.getListCategorieDeviation()))();
		(async () => (this.ttPsl = await SingletonStatique.getListTtPsl()))();
		this.listTypeTransportMap = await SingletonStatique.getListTypeTransportMap();
	}

	clickCheckbox(ebTtTracingNum: number, configPsl: string, event: HTMLInputElement) {
		this.statique.iconCheckOneToggle(event);
		this.tracktraceService.addOrRemoveTracing(this.listTracking, ebTtTracingNum, configPsl, event);
	}
	clear() {
		this.listPsl = new Array<EbTtCompanyPsl>();
		this.listConfigPsl = new Array<String>();
		this.listTracking = new Map();
		this.genericTable.unselectAll();
	}

	addNewEvent(isUpdatePsl: boolean = false) {
		if (this.listTracking && this.listTracking.size > 0) {
			this.setListPsl();
			this.ebPslApp = new EbPslApp();
			this.ebPslApp.dateActuelle = new Date();
		}
	}
	setListPsl() {
		this.listPsl = [];
		let listPsl = new Array<EbTtCompanyPsl>();
		if (this.listTracking)
			this.listTracking.forEach((it) => {
				if (it.listPsl)
					it.listPsl.forEach((psl) => {
						let found = !!listPsl.find((el) => el.ebTtCompanyPslNum == psl.ebTtCompanyPslNum);
						if (!found) listPsl.push(psl);
					});
			});
		listPsl.sort((it1, it2) => it1.order - it2.order);
		this.listPsl = listPsl;
	}
	addNewRevisedDate(isUpdatePsl: boolean = false) {
		if (this.listTracking && this.listTracking.size > 0) {
			this.setListPsl();
			this.ebPslApp = new EbPslApp();
			this.ebPslApp.dateEstimee = new Date();
			this.forUpdateRevisedDate = true;
			// this.clear();
		}
	}
	addNewDeviation() {
		if (this.listTracking && this.listTracking.size > 0) {
			this.setListPsl();
			this.ttEvent = new TTEvent();
			this.ttEvent.dateEvent = new Date();
		}
	}
	confirmModalAddEvent(data: Object) {
		if (data && data["ebPslApp"] && !data["forUpdateRevisedDate"])
			this.confirmModalAddEventPsl(data["ebPslApp"], data["buttonHtmlEvent"]);
		else if (data && data["ebPslApp"] && data["forUpdateRevisedDate"])
			this.confirmModalAddRevisedDatePsl(data["ebPslApp"], data["buttonHtmlEvent"]);
		else if (data && data["ttEvent"])
			this.confirmModalAddEventDeviation(
				data["ttEvent"],
				data["typeOfEvent"],
				data["buttonHtmlEvent"]
			);
		else {
			this.ttEvent = null;
			this.ebPslApp = null;
			this.forUpdateRevisedDate = false;
		}
	}

	confirmModalAddEventPsl(ebPslApp: EbPslApp, event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.listPslApp = new Array<EbPslApp>();
		this.listTracking.forEach((it1) => {
			let ebPsl = new EbPslApp();
			this.ebPslApp.validated = true;
			ebPsl.clone(this.ebPslApp);
			ebPsl.xEbUser = new EbUser();
			ebPsl.xEbUser.ebUserNum = this.userConnected.ebUserNum;
			ebPsl.companyPsl = this.ebPslApp.companyPsl;
			ebPsl.companyPsl["ebCompagnie"] = null;
			ebPsl.codePslLibelle = this.ebPslApp.companyPsl.libelle;
			ebPsl.xEbTrackTrace = new EbTracing();
			ebPsl.xEbTrackTrace.ebTtTracingNum = it1.ebTtTracingNum;
			ebPsl.xEbTrackTrace.configPsl = it1.configPsl;
			// ebPsl.xEbTrackTrace.xEbDemande = new EbDemande();
			// ebPsl.xEbTrackTrace.xEbDemande.ebDemandeNum = it1.xEbDemande.ebDemandeNum;
			this.listPslApp.push(ebPsl);
		});
		this.tracktraceService.updateEbPslAppMasse(this.listPslApp).subscribe(
			(data: Array<EbTracing>) => {
				if (!data || !data.length) {
					this.genericTable.refreshData();
				} else {
					data.forEach((it) => {
						this.genericTable.setDataRow(it["ebTtTracingNum"], it);
					});
				}
				requestProcessing.afterGetResponse(event);
				this.clear();
				this.showSuccess();
				// this.modalService.information("Success", "Data updated successfully");
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				let title = null;
				let body = null;
				this.ttEvent = null;
				this.ebPslApp = null;
				this.translate.get("MODAL.ERROR").subscribe((res: string) => {
					title = res;
				});
				// this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
				// this.modalService.error(title, null, function() {});
				this.clear();
				this.showError();
			}
		);

		this.ttEvent = null;
		this.ebPslApp = null;
	}
	confirmModalAddEventDeviation(data: TTEvent, typeOfEvent: number, event: any) {
		this.listTTEvent = new Array<TTEvent>();
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.listTracking.forEach((it1) => {
			let ttEvent: TTEvent = new TTEvent();
			ttEvent.clone(data);
			ttEvent.xEbTtPslApp = this.ttEvent.xEbTtPslApp;
			ttEvent.xEbTtPslApp.companyPsl = this.ttEvent.xEbTtPslApp.companyPsl;
			ttEvent.xEbTtPslApp.companyPsl["ebCompagnie"] = null;
			ttEvent.xEbTtPslApp.codePslLibelle = this.ttEvent.xEbTtPslApp.companyPsl.libelle;
			ttEvent.xEbTtTracing = new EbTracing();
			ttEvent.xEbTtTracing.ebTtTracingNum = it1.ebTtTracingNum;
			if (ttEvent.categorie && ttEvent.categorie.code == null) ttEvent.categorie = null;

			this.listTTEvent.push(ttEvent);
		});

		this.tracktraceService.addDeviationMasse(this.listTTEvent, typeOfEvent).subscribe(
			(data: Array<TTEvent>) => {
				requestProcessing.afterGetResponse(event);
				this.clear();
				//this.modalService.information("Success", "Data updated successfully");
				this.showSuccess();
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				let title = null;
				let body = null;
				this.ttEvent = null;
				this.ebPslApp = null;
				// this.translate.get("MODAL.ERROR").subscribe((res: string) => {
				//   title = res;
				// });
				// this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
				// this.modalService.error(title, null, function() {});
				this.showError();
				this.clear();
			}
		);

		this.ttEvent = null;
		this.ebPslApp = null;
	}

	initDatatable() {
		this.globalSearchCriteria.pageNumber = 1;
		this.globalSearchCriteria.module = this.module;
		this.globalSearchCriteria.ebUserNum = this.userConnected.ebUserNum;

		this.activatedRoute.queryParams.subscribe((params) => {
			this.globalSearchCriteria.ebDemandeNum = +params["filterByDemande"] || null;
			this.isUpdatePsl = params["isUpdatePsl"] == "1";
			this.displayAddEvent = params["displayAddEvent"] || true;

			this.dataInfos.cols = this.getColumns();
		});

		this.dataInfos.dataKey = "ebTtTracingNum";
		this.dataInfos.showSubTable = false;
		this.dataInfos.showAdvancedSearchBtn = true;
		this.dataInfos.paginator = true;
		this.dataInfos.dataLink = this.statique.controllerTrackTrace + "/listTrack";
		this.dataInfos.numberDatasPerPage = 5;
		this.dataInfos.selectedCols = this.dataInfos.cols;
		this.dataInfos.lineActions = [];
		this.dataInfos.extractWithAdvancedSearch = true;
		this.dataInfos.favorisName = (row) => row.refTransport;
		this.dataInfos.showCheckbox = true;
		this.dataInfos.cardView = true;
		this.dataInfos.contextMenu = true;

		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(this.module)];
		this.dataInfos.showDetails = true;
		this.dataInfos.actionDetailsLink = "/app/" + moduleRoute + "/details/";
		this.dataInfos.showFlags = true;
		this.dataInfos.dataFlagsField = "listEbFlagDTO";
		this.dataInfos.onFlagChange = this.onFlagChange;
		this.dataInfos.showSaveBtn = true;
		this.dataInfos.showExportBtn = true;
		this.dataInfos.showSearchBtn = true;
		this.dataInfos.showCollapsibleBtn = true;
		this.dataInfos.showColConfigBtn = true;
		this.dataInfos.actionColumFixed = false;
		this.dataInfos.blankBtnCount = 0;

		this.dataInfos.ccpTooltipRender = (row: EbTracing): string => {
			if (row == null || row.xEbChargeur == null || row.xEbChargeur.ebCompagnie == null) return "";
			return row.xEbChargeur.ebCompagnie.nom;
		};

		this.dataInfos.insertDetailsContextItem();
		this.dataInfos.insertDetailsNewTabContextItem();
	}

	renderDocStatut(classname: string, name: string, number: number): string {
		return `<p class='doc-status ${classname}'>${name} (${number == null ? 0 : number})</p>`;
	}

	getColumns(): Array<GenericTableInfos.Col> {
		const $this = this;
		let cols: Array<GenericTableInfos.Col> = [
			{
				field: "listEbFlagDTO",
				translateCode: "PRICING_BOOKING.FLAG",
				minWidth: "125px",
				allowClick: false,
				sortable: false,
				renderMode: GenericTableInfos.Col.RenderModeTable.GenericCell,
				genericCellClass: FlagColumnGenericCell,
				genericCellParams: new FlagColumnGenericCell.Params({
					field: "listEbFlagDTO",
					contributionAccess: true,
					moduleNum : $this.module,
				}),
				genericCellHandler: this.inlineEditingHandler,
			},
			{
				field: "refTransport",
				translateCode: "TRACK_TRACE.TRANSPORT_REF",
				width: "110px",
				isCardCol: true,
				isCardTitle: true,
				cardOrder: 1,
			},
			{ field: "customRef", translateCode: "PRICING_BOOKING.CUSTOMER_REFERENCE", width: "146px" },
			// {
			//   field: "partNumber",
			//   translateCode: "PRICING_BOOKING.PART_NUMBER",
			//   width: "150px"
			// },
			// {
			//   field: "ShippingType",
			//   translateCode: "PRICING_BOOKING.SHIPPING_TYPE",
			//   width: "150px"
			// },
			// {
			//   field: "orderNumber",
			//   translateCode: "PRICING_BOOKING.ORDER_NUMBER",
			//   width: "150px"
			// },

			{
				translateCode: "PRICING_BOOKING.STATUT_DOCUMENT",
				field: "listTypeDocuments",
				width: "160px",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data, d, row) {
					let libelle = "";
					let titleLibelle = "";
					let libelleWrapper = "";
					if (
						row.xEbDemande.listTypeDocuments != null &&
						row.xEbDemande.listTypeDocuments.length > 0
					) {
						row.xEbDemande.listTypeDocuments.forEach((element: EbTypeDocuments) => {
							let loopLibelle = "";
							let classname = "";
							if (element.isexpectedDoc) {
								if (element.nbrDoc == 0 || element.nbrDoc == null) {
									classname = "red";
								} else {
									classname = "green";
								}

								loopLibelle = $this.renderDocStatut(classname, element.code, element.nbrDoc);
							} else {
								if (element.nbrDoc > 0) {
									classname = "black";
									loopLibelle = $this.renderDocStatut(classname, element.code, element.nbrDoc);
								}
							}
							titleLibelle += element.code + "(" + element.nbrDoc + "); ";
							libelle += loopLibelle;
						});
						titleLibelle = titleLibelle.substring(0, libelle.length - 2);
						libelleWrapper =
							`<div class='docs__wrapper' title='` + titleLibelle + `'>` + libelle + `</div>`;
					}
					return libelleWrapper;
				}.bind(this),
			},
			{
				field: "customerReference",
				translateCode: "TRACK_TRACE.UNIT_REFERENCE",
				width: "90px",
				isCardCol: true,
				isCardLabel: true,
				cardOrder: 2,
			},
			{
				translateCode: "PRICING_BOOKING.REQUESTOR",
				field: "xEbChargeur",
				width: "180px",
				render: function(data, type, row) {
					return row.xEbChargeur.nomPrenom;
				}.bind(this),
			},
			{
				field: "packingList",
				translateCode: "TYPE_UNIT.PACKING_LIST",
				width: "90px",
				isCardCol: true,
				isCardLabel: true,
				cardOrder: 2,
			},
			{ field: "xEcIncotermLibelle", translateCode: "TRACK_TRACE.INCOTERMS", width: "110px" },
			{
				translateCode: "TRACK_TRACE.ORIGI_OF_COUNTRY",
				field: "xEcCountryOrigin",
				isCardCol: true,
				isCardLabel: true,
				render: function(data) {
					return data ? data.libelle : "";
				}.bind(this),
			},
			{ field: "libelleOriginCity", translateCode: "TRACK_TRACE.ORIG_CITY" },
			{
				translateCode: "TRACK_TRACE.DEST_COUNTRY",
				field: "xEcCountryDestination",
				isCardCol: true,
				isCardLabel: true,
				render: function(data) {
					return data ? data.libelle : "";
				}.bind(this),
			},
			{ field: "libelleDestCity", translateCode: "TRACK_TRACE.DEST_CITY" },
			// {
			//   translateCode: "TRACK_TRACE.DATE_PICKUP",
			//   field: "datePickup",
			//   render: function(data) {
			//     return this.statique.formatDate(data);
			//   }.bind(this)
			// },
			{ field: "nomEtablissementTransporteur", translateCode: "TRACK_TRACE.TRANSPORTEUR" },
			{ field: "totalWeight", translateCode: "TRACK_TRACE.GROSS_WEIGHT" },

			{
				translateCode: "TRACK_TRACE.MODE_OF_TRANSPORT",
				field: "xEcModeTransport",
				width: "90px",
				isCardCol: true,
				// cardOrder: 2,
				render: function(modeTransportCode, type, row) {
					if (!modeTransportCode) return "";
					return SingletonStatique.getModeTransportString(
						modeTransportCode,
						row,
						$this.listTypeTransportMap
					);
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.NUM_AWB_BOL",
				field: "numAwbBol",
				width: "95px",
				render: function(data, type, row) {
					return row.xEbDemande.numAwbBol;
				}.bind(this),
			},
			{
				translateCode: "REQUEST_OVERVIEW.CARRIER_UNIQ_REF_NUM",
				field: "carrierUniqRefNum",
				width: "130px",
				render: function(data, type, row) {
					return row.xEbDemande.carrierUniqRefNum;
				}.bind(this),
			},
			{
				translateCode: "TRACK_TRACE.LAST_UPDATE",
				field: "dateModification",
				width: "90px",
				render: function(data) {
					return this.statique.formatDate(data);
				}.bind(this),
			},
			{
				translateCode: "TRACK_TRACE.LATE",
				field: "late",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				render: function(data) {
					if (data == "LATE_TRACK") {
						return "<div class='progress late-column'><div class='progress-bar progress-bar-info progress-bar-danger' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>Yes</div></div>";
					} else
						return "<div class='progress'><div class='progress-bar progress-bar-info progress-bar-success' role='progressbar' aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width: 100%;'>No</div></div>";
				}.bind(this),
			},
			{
				translateCode: "TRACK_TRACE.REQUEST_DATE",
				field: "dateCreation",
				width: "80px",
				render: function(data, type, row) {
					return this.statique.formatDate(row.xEbDemande.dateCreation);
				}.bind(this),
			},
			{
				translateCode: "TRACK_TRACE.LAST_MILESTONE_UPDATED",
				field: "libellePslCourant",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				// isCardCol: true,
				// isCardLabel: true,
				// cardOrder: 3,
				render: function(data: string, display, row: EbTracing) {
					return `<div title="${data ||
						"Waiting for pickup"}" class='progress'><div class='progress-bar progress-bar-info progress-bar-striped' style="width: ${
						row.percentPslCourant
					}%; color: dimgray;" role='progressbar' aria-valuenow='${
						row.percentPslCourant
					}' aria-valuemin='0' aria-valuemax='100'>${data || "Waiting for pickup"}</div></div>`;
				}.bind(this),
			},

			{
				field: "libelleLastPsl",
				translateCode: "TRACK_TRACE.LIBELLE_LAST_PSL",
				width: "100px",
				isCardCol: true,
				cardOrder: 4,
			},
			{
				translateCode: "TRACK_TRACE.DATE_LAST_PSL",
				field: "dateLastPsl",
				width: "145px",
				isCardCol: true,
				cardOrder: 3,
				render: function(data) {
					return this.statique.formatDate(data);
				}.bind(this),
			},
			{
				translateCode: "TRACK_TRACE.Estimated_PSL_START",
				field: "datePickupTM",
				render: function(data, display, row) {
					return this.statique.formatDate(row.xEbDemande.datePickupTM);
				}.bind(this),
			},
			{
				translateCode: "TRACK_TRACE.Estimated_PSL_END",
				field: "finalDelivery",
				render: function(data, display, row) {
					return this.statique.formatDate(row.xEbDemande.finalDelivery);
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.COMPANY_ORIGIN",
				field: "companyOrigin",
			},
			{
				translateCode: "TRACK_TRACE.TRANSPORT_STATUS_PER_UNIT",
				field: "transportStatusPerUnit",
				render: function(data, type, row) {
					return data;
				}.bind(this),
			},
			{
				translateCode: "PRICING_BOOKING.TRANSPORT_CONFIRMATION",
				field: "transportConfirmation",
				render: function(data, display, row) {
					let transportConfirmation = "",
						translateCode;
					if (row.xEbDemande.askForTransportResponsibility && !row.xEbDemande.provideTransport) {
						translateCode = "PRICING_BOOKING.WAITING_FOR_CONFIRMATION";
					} else if (
						row.xEbDemande.askForTransportResponsibility &&
						row.xEbDemande.provideTransport
					) {
						translateCode = "PRICING_BOOKING.CONFIRMED";
					}

					if (translateCode) {
						this.translate.get(translateCode).subscribe((res) => (transportConfirmation = res));
					}
					return transportConfirmation;
				}.bind(this),
			},
		];

		cols.forEach((it) => {
			this.translate.get(it.translateCode).subscribe((res) => (it["header"] = res));
		});

		return cols;
	}

	clickHandler(data) {
		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(this.module)];
		this.router.navigate(["/app/" + moduleRoute + "/details", data.ebTtTracingNum]);
	}
	clickHandlerAndOpenNewWindow(data) {
		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(this.module)];
		//console.log(window.location);
		window.open(
			window.location.protocol +
				"//" +
				window.location.host +
				"/app/" +
				moduleRoute +
				"/details/" +
				data.ebTtTracingNum,
			"_blank"
		);
	}

	getDatatableCompId() {
		if (this.module == Modules.TRACK) return GenericTableScreen.TRACK;
		return null;
	}

	checkAllTrackTrace(event) {
		if (!event) return;

		let o;
		let elts = document.getElementsByClassName("checkbox-Track");

		for (o in elts) {
			if (elts[o]["click"] && (event.target || event).checked != elts[o]["checked"]) {
				elts[o]["click"]();
			}
		}
		this.statique.iconCheckAllToggle(event);
	}

	onCheckRow() {
		this.getListTrackTrace();
	}

	onListCategorieLoaded(listCategorie: Array<EbCategorie>) {
		this.listCategorieEmitter.emit(listCategorie);
	}
	onListFieldLoaded(listField: Array<IField>) {
		this.listFieldsEmitter.emit(listField);
	}
	onFlagChange(context: any, row: EbTracing, listFlag: Array<EbFlag>) {
		let listFlagCode = EbFlag.convertListToCommaSeparated(listFlag);
		row.listFlag = listFlagCode;
		context.tracktraceService.saveFlags(row.ebTtTracingNum, listFlagCode).subscribe();
	}
	showSuccess() {
		this.messageService.add({
			severity: "success",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.SUCCESS_MESSAGE.DETAIL"),
		});
	}
	showError() {
		this.messageService.add({
			severity: "error",
			summary: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE.SUMMARY"),
			detail: this.translate.instant("GENERAL.TOAST_MESSAGE.ERROR_MESSAGE"),
		});
	}

	confirmModalAddRevisedDatePsl(ebPslApp: EbPslApp, event: any) {
		let requestProcessing = new RequestProcessing();
		requestProcessing.beforeSendRequest(event);
		this.listPslApp = new Array<EbPslApp>();

		this.listTracking.forEach((it1) => {
			let ebPsl = new EbPslApp();
			this.ebPslApp.validated = true;
			ebPsl.clone(this.ebPslApp);
			ebPsl.xEbUser = new EbUser();
			ebPsl.xEbUser.ebUserNum = this.userConnected.ebUserNum;
			ebPsl.companyPsl = this.ebPslApp.companyPsl;
			ebPsl.companyPsl["ebCompagnie"] = null;
			ebPsl.codePslLibelle = this.ebPslApp.companyPsl.libelle;
			ebPsl.xEbTrackTrace = new EbTracing();
			ebPsl.xEbTrackTrace.ebTtTracingNum = it1.ebTtTracingNum;
			ebPsl.xEbTrackTrace.configPsl = it1.configPsl;
			// ebPsl.xEbTrackTrace.xEbDemande = new EbDemande();
			// ebPsl.xEbTrackTrace.xEbDemande.ebDemandeNum = it1.xEbDemande.ebDemandeNum;
			this.listPslApp.push(ebPsl);
		});
		this.tracktraceService.updateRevisedDateEbPslAppMasse(this.listPslApp).subscribe(
			(data: Array<EbTracing>) => {
				if (!data || !data.length) {
					this.genericTable.refreshData();
				} else {
					data.forEach((it) => {
						this.genericTable.setDataRow(it["ebTtTracingNum"], it);
					});
				}
				requestProcessing.afterGetResponse(event);
				this.clear();
				this.showSuccess();
				// this.modalService.information("Success", "Data updated successfully");
			},
			(error) => {
				requestProcessing.afterGetResponse(event);
				let title = null;
				let body = null;
				this.ttEvent = null;
				this.ebPslApp = null;
				this.translate.get("MODAL.ERROR").subscribe((res: string) => {
					title = res;
				});
				// this.translate.get("GENERAL.INTERNAL_SERVER_ERROR").subscribe((res: string) => { body = res; });
				// this.modalService.error(title, null, function() {});
				this.clear();
				this.showError();
			}
		);

		this.ttEvent = null;
		this.ebPslApp = null;
		this.forUpdateRevisedDate = false;
	}
	inlineEditingHandler(key: string, value: any, model: any, context: any) {
		if (key === "selection-change") {
			context.autoSaveRow(model, value);
		}
	}

	autoSaveRow(row: EbTracing, listFlag: Array<EbFlag>) {
		let listFlagCode = EbFlag.convertListToCommaSeparated(listFlag);
		row.listFlag = listFlagCode;
		this.tracktraceService.saveFlags(row.ebTtTracingNum, listFlagCode).subscribe();
	}
}
