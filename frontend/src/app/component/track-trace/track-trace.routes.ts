import { Route, RouterModule } from "@angular/router";
import { TrackTraceComponent } from "./track-trace.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";
import { TrackTraceDetailsComponent } from "./track-trace-details/track-trace-details.component";
import { TrackTraceDetailsNewVersionComponent } from "./track-trace-details-new-version/track-trace-details-new-version.component";

export const track_trace_ROUTES: Route[] = [
	{
		path: "dashboard",
		component: TrackTraceComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "dashboard/:ebDemandeNum",
		component: TrackTraceComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "details/:ebTtTracingNum",
		component: TrackTraceDetailsComponent,
		canActivate: [UserAuthGuardService],
	},
	{
		path: "detailsversion/:ebTtTracingNum",
		component: TrackTraceDetailsNewVersionComponent,
		canActivate: [UserAuthGuardService],
	},
];

export const TrackTraceRoutes = RouterModule.forChild(track_trace_ROUTES);
