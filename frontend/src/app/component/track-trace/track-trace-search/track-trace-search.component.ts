import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from "@angular/core";
import { generaleMethodes } from "@app/shared/generaleMethodes.component";

import { Modules, SavedFormIdentifier } from "@app/utils/enumeration";
import { SearchCriteriaTrackTrace } from "@app/utils/SearchCriteriaTrackTrace";
import { SearchField } from "@app/classes/searchField";
import { IField } from "@app/classes/customField";
import { EbLabel } from "@app/classes/label";
import { EbCategorie } from "@app/classes/categorie";
import { AdvancedFormSearchComponent } from "@app/shared/advanced-form-search/advanced-form-search.component";
import { Statique } from "@app/utils/statique";
import { TranslateService } from "@ngx-translate/core";

@Component({
	selector: "app-track-trace-search",
	templateUrl: "./track-trace-search.component.html",
	styleUrls: ["./track-trace-search.component.css"],
})
export class TrackTraceSearchComponent implements OnInit {
	SavedFormIdentifier = SavedFormIdentifier;
	moduleName = Modules;
	advancedOption: SearchCriteriaTrackTrace = new SearchCriteriaTrackTrace();
	paramFields: Array<SearchField> = new Array<SearchField>();
	listAllCategorie: Array<EbLabel> = new Array<EbLabel>();
	initParamFields: Array<SearchField> = new Array<SearchField>();

	@ViewChild("advancedFormSearchComponentTrack", { static: true })
	advancedFormSearchComponentTrack: AdvancedFormSearchComponent;

	@Input()
	module: number;
	@Output()
	onSearch = new EventEmitter<SearchCriteriaTrackTrace>();
	@Input()
	listFields: Array<IField>;
	@Input()
	listCategorie: Array<EbCategorie>;
	dataInfosItem: Array<any> = [];
	@Input()
	selectedField: any;

	constructor(
		public generaleMethode: generaleMethodes,
		private translateService: TranslateService
	) {}

	ngOnInit() {
		if (this.module == this.moduleName.TRACK) {
			this.initParamFields = [];
			this.initParamFields = require("../../../../assets/ressources/jsonfiles/tracing-searchfields.json");
			this.onInputChange(null);
		}
	}

	ngOnChanges() {}

	search(advancedOption: SearchCriteriaTrackTrace) {
		this.onSearch.emit(advancedOption);
	}

	onClickInput() {
		this.initParamFields = require("../../../../assets/ressources/jsonfiles/tracing-searchfields.json");
		//cacher le field document status selon le jira MTGR-374 (sous tache)
		this.initParamFields = this.initParamFields.filter((field)=> !field.hidden );
		this.initParamFields.forEach((f) =>
			this.translateService.get(f.label).subscribe((res) => (f.label = res))
		);

		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));

		if (this.listFields && this.listFields.length > 0) {
			let arr = [];
			this.listFields.forEach((it) => {
				arr.push({
					label: it.label,
					name: it.name,
					type: "text",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
		}
		if (this.listCategorie && this.listCategorie.length > 0) {
			let arr = [];
			let i = 0;
			this.listAllCategorie = [];
			this.listCategorie.forEach((it, i) => {
				arr.push({
					label: it.libelle,
					type: "autocomplete",
					name: "categoryField" + i,
					listItems: it.labels,
					fetchAttribute: "libelle",
					fetchValue: "ebLabelNum",
					multiple: "true",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
		}

		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});
	}

	onInputChange(selectedFields) {
		this.dataInfosItem = Array.from(new Set([].concat(this.initParamFields)));

		this.setCFInputFields();

		this.setCategoriesInputFields();

		this.paramFields.forEach((t) => {
			this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
		});

		if (!Statique.isDefined(selectedFields)) {
			this.initParamFields = this.initParamFields.filter(
				(field) =>
					field.name === "listStatutStr" ||
					field.name === "listOrigins" ||
					field.name === "listDestinations"
			);
			this.paramFields = Array.from(new Set([].concat(this.initParamFields)));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
		} else if (
			selectedFields !== null &&
			!this.paramFields.find((field) => field.name == selectedFields)
		) {
			let result = JSON.stringify(this.advancedFormSearchComponentTrack.getSearchInput());
			let parsedValues: Array<SearchField> = JSON.parse(result);
			this.paramFields.forEach((field) => {
				if (parsedValues[field.name] !== null && parsedValues[field.name] !== undefined) {
					field.default = parsedValues[field.name];
				}
			});
			this.initParamFields = this.initParamFields.filter(
				(field) => field.name === selectedFields && field.default !== null
			);
			this.paramFields = this.paramFields.concat(this.initParamFields);
			this.paramFields = Array.from(new Set(this.paramFields));
			this.paramFields.forEach((t) => {
				this.dataInfosItem = this.dataInfosItem.filter((el) => el !== t);
			});
		}
	}

	setCFInputFields() {
		if (this.listFields && this.listFields.length > 0) {
			let arr = [];
			this.listFields.forEach((it) => {
				arr.push({
					label: it.label,
					name: it.name,
					type: "text",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
			this.initParamFields = this.initParamFields.concat(arr);
		}
	}

	setCategoriesInputFields() {
		if (this.listCategorie && this.listCategorie.length > 0) {
			let arr = [];
			let i = 0;
			this.listAllCategorie = [];
			this.listCategorie.forEach((it, i) => {
				arr.push({
					label: it.libelle,
					type: "autocomplete",
					name: "categoryField" + i,
					listItems: it.labels,
					fetchAttribute: "libelle",
					fetchValue: "ebLabelNum",
					multiple: "true",
				});
			});
			this.dataInfosItem = this.dataInfosItem.concat(arr);
			this.initParamFields = this.initParamFields.concat(arr);
		}
	}
}
