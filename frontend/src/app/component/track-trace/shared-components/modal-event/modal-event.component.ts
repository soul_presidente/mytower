import { Component, EventEmitter, Input, OnInit, Output, SimpleChange } from "@angular/core";
import { EcCountry } from "@app/classes/country";
import { EbPslApp } from "@app/classes/pslApp";
import { NgbActiveModal } from "@ng-bootstrap/ng-bootstrap";
import { TTEvent } from "@app/classes/tTEvent";
import { EbTtCategorieDeviation } from "@app/classes/categorieDeviation";
import { Statique } from "@app/utils/statique";
import { MTEnum } from "@app/classes/mtEnum";
import { EnumPsl, TypeOfEvent } from "@app/utils/enumeration";
import SingletonStatique from "../../../../utils/SingletonStatique";
import { EcFuseauxHoraire } from "../../../../classes/fuseauHoraire";
import { EbTtCompanyPsl } from "@app/classes/ttCompanyPsl";
import { UploadInput, UploadFile } from "ngx-uploader";
import { EbTypeDocuments } from "@app/classes/ebTypeDocuments";
import { DocumentUploadComponent } from "@app/shared/documents/document-upload.component";
import { TranslateService } from "@ngx-translate/core";
import { ModalService } from "@app/shared/modal/modal.service";
import { FichierJoint } from "@app/classes/fichiersJoint";
import { PricingService } from "@app/services/pricing.service";

@Component({
	selector: "app-modal-event",
	templateUrl: "./modal-event.component.html",
	styleUrls: ["../../../../shared/modal/modal.component.scss", "./modal-event.component.css"],
})
export class ModalEventComponent extends DocumentUploadComponent implements OnInit {
	@Input()
	set forUpdateRevisedDate(dateRevisedDate: boolean) {
		this.forUpdateRevisedDateAlpha = dateRevisedDate;
	}
	@Input()
	readOnly?: boolean;
	@Input()
	listPsl?: Array<EbTtCompanyPsl>;
	@Input()
	listPslApp?: Array<EbPslApp>;
	@Input()
	typesDocument?: any;
	dataUpload: any;
	componentObject: any;
	uploadInput: EventEmitter<UploadInput> = new EventEmitter<UploadInput>();
	selectedFileType: EbTypeDocuments;
	selectedTypeDoc: EbTypeDocuments;
	@Input()
	listCountry: Array<EcCountry>;
	selectedTpDc: EbTypeDocuments;

	@Input()
	module: number;
	@Input()
	ebDemandeNum: number;

	_ebCompagnieNum: number;

	@Input() set ebCompagnieNum(value: number) {
		this._ebCompagnieNum = value;
	}

	get ebCompagnieNum() {
		return this._ebCompagnieNum;
	}
	@Output()
	documentRefresh: EventEmitter<any> = new EventEmitter();

	@Input()
	set ebPslApp(pslApp: EbPslApp) {
		this.ebPslAppValue = pslApp;
		if (pslApp && pslApp.xEcCountry && pslApp.xEcCountry.ecCountryNum)
			this.ecCountry = pslApp.xEcCountry;
		else this.ecCountry = null;
	}

	@Input()
	ebPslAppUpdate: TTEvent;
	@Input()
	ebPsl: EbPslApp;

	forUpdateRevisedDateAlpha?: boolean;
	@Input()
	listCategorieDeviation: Array<EbTtCategorieDeviation>;
	@Input()
	ttEvent: TTEvent = new TTEvent();
	@Output()
	confirmEventHandler: EventEmitter<Object> = new EventEmitter();

	ebPslAppValue: EbPslApp;
	ecCountry: EcCountry;
	Statique = Statique;

	/* listDeviationCategorie: Array<MTEnum> = new Array<MTEnum>(); */

	EnumPsl = EnumPsl;
	read: Boolean = true;

	listFuseauHoraire: Array<EcFuseauxHoraire> = new Array<EcFuseauxHoraire>();

	typeOfEvent: number;
	TypeOfEvent = TypeOfEvent;
	listCategorieDeviationFiltred: Array<EbTtCategorieDeviation>;
	ebFileDocumentToDelete: FichierJoint;

	constructor(
		public activeModal: NgbActiveModal,
		protected translate?: TranslateService,
		protected modalService?: ModalService,
		protected pricingService?: PricingService
	) {
		super();
		this.componentObject = this;
		this.dataUpload = { typeFile: String(this.UploadDirectory.PRICING_BOOKING) };
	}

	ngOnInit() {
		if (typeof this.readOnly == "undefined") this.readOnly = true;
		if (typeof this.listPsl == "undefined") this.listPsl = null;
		this.loadListFuseauHoraire()
		// this.fileTypeDocumentNum = "0";

		/*   if(this.ttEvent){
            this.listDeviationCategorie = Statique.TtDeviationCategorie.map(it => {
                let dev = new MTEnum();
                dev.code = it.key;
                dev.libelle = it.value;
                return dev;
            });
        } */
	}

	ngOnChanges(changes: SimpleChange) {
		if (changes["ebPslAppUpdate"]) {
			if (
				this.ebPslAppUpdate &&
				this.ebPslAppUpdate.xEbTtPslApp.xEcCountry &&
				this.ebPslAppUpdate.xEbTtPslApp.xEcCountry.ecCountryNum
			)
				this.ecCountry = this.ebPslAppUpdate.xEbTtPslApp.xEcCountry;
			else this.ecCountry = null;
			if (this.ebPslAppUpdate) {
				this.typeOfEvent = this.ebPslAppUpdate.typeEvent;
				if (this.typeOfEvent == TypeOfEvent.EVENT) {
					this.ebPslAppValue = new EbPslApp();
					this.ebPslAppValue.city = this.ebPsl.city;
					this.ebPslAppValue.dateActuelle = this.ebPsl.date;
					this.ebPslAppValue.commentaire = this.ebPslAppUpdate.commentaire as string;
					for (let ttEvent of this.listPsl) {
						if (ttEvent.libelle == this.ebPsl.nom) {
							this.ebPslAppValue.companyPsl = ttEvent;
							break;
						}
					}
				} else {
					this.ttEvent = new TTEvent();
					this.ttEvent.commentaire = this.ebPslAppUpdate.commentaire;
					this.ttEvent.quantity = this.ebPslAppUpdate.quantity;
					this.ttEvent.dateEvent = this.ebPslAppUpdate.dateEvent;
					for (let ttEvent of this.listPsl) {
						if (ttEvent.libelle == this.ebPsl.nom) {
							this.ttEvent.xEbTtPslApp.companyPsl = ttEvent;
							break;
						}
					}
					this.filterListCategorys();
					this.ttEvent.categorie = this.ebPslAppUpdate.categorie;
				}
			}
		}
	}

	confirmModalAddEvent(event: any) {
		if (
			this.ebPslAppValue &&
			(this.typeOfEvent == TypeOfEvent.EVENT || this.forUpdateRevisedDateAlpha)
		) {
			this.ebPslAppValue.xEcCountry = this.ecCountry;

			if (this.listPslApp) {
				this.listPslApp.forEach((elem) => {
					if (
						elem.companyPsl &&
						elem.companyPsl.ebTtCompanyPslNum === this.ebPslAppValue.companyPsl.ebTtCompanyPslNum
					) {
						this.ebPslAppValue.ebTtPslAppNum = elem.ebTtPslAppNum;
					}
				});
			}
			this.ttEvent = null;
			this.refreshDocumentList();
		}
		if (
			this.ttEvent &&
			(this.typeOfEvent == TypeOfEvent.INCIDENT || this.typeOfEvent == TypeOfEvent.UNCONFORMITY)
		) {
			if (this.listPslApp)
				this.listPslApp.forEach((elem) => {
					if (
						elem.companyPsl &&
						this.ttEvent &&
						elem.companyPsl.ebTtCompanyPslNum ===
							this.ttEvent.xEbTtPslApp.companyPsl.ebTtCompanyPslNum
					) {
						this.ttEvent.xEbTtPslApp.ebTtPslAppNum = elem.ebTtPslAppNum;
					}
				});

			this.ebPslAppValue = null;
			this.refreshDocumentList();
		}

		this.confirmEventHandler.emit({
			ebPslApp: this.ebPslAppValue,
			ttEvent: this.ttEvent,
			forUpdateRevisedDate: this.forUpdateRevisedDateAlpha,
			typeOfEvent: this.typeOfEvent,
			buttonHtmlEvent: event,
		});
		this.ebPslAppValue = null;
		this.ttEvent = null;
		this.forUpdateRevisedDateAlpha = false;
		this.typeOfEvent = null;
		this.selectedTypeDoc = null;
	}
	cancelModalAddEvent() {
		this.typeOfEvent = null;
		this.ebPslAppValue = null;
		this.ttEvent = null;
		this.forUpdateRevisedDateAlpha = false;
		this.confirmEventHandler.emit(null);
		this.selectedTypeDoc = null;
		this.pricingService.deleteDocument(this.ebFileDocumentToDelete).subscribe();
	}

	compareCountry(country1: EcCountry, country2: EcCountry) {
		if (country2 !== undefined && country2 !== null) {
			return country1.ecCountryNum === country2.ecCountryNum;
		}
	}
	compareEnums(mtEnum1: MTEnum, mtEnum2: MTEnum) {
		if (mtEnum2 !== undefined && mtEnum2 !== null) {
			return mtEnum1.code === mtEnum2.code;
		}
	}

	async loadListFuseauHoraire() {
		this.listFuseauHoraire = await SingletonStatique.getListFuseauxHoraire();
	}

	selectpsl(ebPslAppValue: EbPslApp, type: number) {
		if (this.listPslApp) {
			this.listPslApp.forEach((elem) => {
				if (
					elem.companyPsl &&
					elem.companyPsl.ebTtCompanyPslNum === ebPslAppValue.companyPsl.ebTtCompanyPslNum
				) {
					if (type == 1) this.ebPslAppValue.ebTtPslAppNum = elem.ebTtPslAppNum;
					else {
						this.ttEvent.xEbTtPslApp.ebTtPslAppNum = elem.ebTtPslAppNum;
					}
				}
			});
		}
	}

	filterListCategorys() {
		this.listCategorieDeviationFiltred = this.listCategorieDeviation.filter(
			(deviation) => deviation.typeEvent === this.typeOfEvent || !deviation.typeEvent
		);
	}
	changeSelectedDoc(event) {
		if (
			!this.filesReturn[this.fileTypeDocumentNum] ||
			this.filesReturn[this.fileTypeDocumentNum].length == 0
		) {
			this.fileTypeDocumentNum = this.selectedTypeDoc.code;

			this.documentFiles[this.fileTypeDocumentNum] = new Array<UploadFile>();
			this.filesReturn[this.fileTypeDocumentNum] = new Array<FichierJoint>();
		}
	}

	refreshDocumentList() {
		this.documentRefresh.emit({});
	}

	ebFileDocumentHandler(ebFileDoc: FichierJoint) {
		this.ebFileDocumentToDelete = ebFileDoc;
	}
}
