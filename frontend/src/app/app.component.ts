import { ChangeDetectorRef, Component, HostBinding, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { AuthenticationService } from "@app/services/authentication.service";
import { NavigationEnd, Router } from "@angular/router";
import { Modules } from "@app/utils/enumeration";
import { Statique } from "@app/utils/statique";
import { ISingletonStatique } from "@app/utils/SingletonStatique";
import { StatiqueService } from "@app/services/statique.service";
import { OverlayContainer } from "@angular/cdk/overlay";
import { TypeRequestService } from "@app/services/type-request.service";

@Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: ["./app.component.scss"],
})
export class AppComponent extends ConnectedUserComponent implements OnInit {
	title = "app";
	hideHeaderAndSideBar: boolean = false;
	module: number = null;
	Statique = Statique;
	appLoaded: boolean = false;
	showHeaderBtn: boolean;
	singletonStatique: any;
	activeActionBarre: boolean;
	@HostBinding("class")
	componentCssClass;
	LOCALSTORAGE_KEY = "WINDOW_VALIDATION";
	RECHECK_WINDOW_DELAY_MS = 100;
	initialized = false;
	isMainWindow = false;
	windowArray;
	windowId;
	isNewWindowPromotedToMain = false;

	constructor(
		public translate: TranslateService,
		public router: Router,
		protected authenticationService: AuthenticationService,
		private cdRef: ChangeDetectorRef,
		protected statiqueService: StatiqueService,
		public overlayContainer?: OverlayContainer,
		private typeRequestService?: TypeRequestService
	) {
		super();
	}

	ngOnInit() {
		this.toLoginForNotRememberMe();
		this.keepLoggedInFunction();
		if (localStorage.getItem("activeTheme")) {
			this.onSetTheme(localStorage.getItem("activeTheme"));
		} else {
			this.onSetTheme("default-theme");
		}

		let url = document.URL;
		this.showHeaderBtn = document.location.href.indexOf("/accueil") > -1 ? false : true;

		if (this.userConnected !== null) {
			this.authenticationService.refreshACLs();
		}

		this.router.events.subscribe((val) => {
			if (val instanceof NavigationEnd) {
				this.changeModule();
			}
		});
		this.translate.addLangs(["en", "fr"]);
		this.translate.setDefaultLang(
			this.userConnected && this.userConnected.language ? this.userConnected.language : "en"
		);

		//console.log(this.userConnected.language)
		this.singletonStatique = new ISingletonStatique(this.statiqueService, this.typeRequestService);
		this.appLoaded = true;

		if (this.userConnected != null) {
			this.singletonStatique.userConnected = this.userConnected;
		}

		let browserLang = this.translate.getBrowserLang();

		this.singletonStatique.lang =
			this.userConnected && this.userConnected.language ? this.userConnected.language : "en";
		this.translate.use(this.singletonStatique.lang);

		if (url.indexOf("inscription") > -1) {
			this.hideHeaderAndSideBar = true;
		}

		if (url.indexOf("inscription-chanel") > -1) {
			this.hideHeaderAndSideBar = true;
		}

		if (url.indexOf("login") > -1) {
			this.hideHeaderAndSideBar = true;
		}

		if (url.indexOf("reset-password") > -1) {
			this.hideHeaderAndSideBar = true;
		}

		/**
		 * adding auth token before every AJAX request
		 * useful for datatables
		 */
		try {
			$.ajaxSetup({
				beforeSend: function(req) {
					let authHeader = Statique.getAuthToken();
					req.setRequestHeader(authHeader.name, authHeader.value);
				},
			});
		} catch (e) {}

		document.addEventListener(
			"change-theme",
			function(e) {
				event.preventDefault();
				event.stopPropagation();
				this.onSetTheme(e.detail);
			}.bind(this),
			false
		);
	}

	changerLangue(language: string) {
		this.translate.use(language);
	}

	onSetTheme(theme) {
		this.overlayContainer.getContainerElement().classList.add(theme);
		this.componentCssClass = theme;
		Statique.removeThemes();
		document.getElementsByTagName("body")[0].classList.add(theme)
	}

	loadStatics() {
		try {
			// this.SingletonStatique.listModeTransport = JSON.parse(data.listModeTransport);
			// this.SingletonStatique.listTypeTransportAir = JSON.parse(data.listTypeTransportAir);
			// this.SingletonStatique.listTypeTransportSea = JSON.parse(data.listTypeTransportSea);
			// this.SingletonStatique.listTypeTransportIntegrateur = JSON.parse(data.listTypeTransportIntegrateur);
			// this.SingletonStatique.listTtPsl = JSON.parse(data.listTtPsl);
			// this.SingletonStatique.listEcCountry = data.listEcCountry;
			// this.SingletonStatique.listEcCurrency = data.listEcCurrency;
			// this.SingletonStatique.listCategorieDeviation = data.listCategorieDeviation;
			// this.SingletonStatique.listIncoterm = data.listIncoterm;
			// this.SingletonStatique.listEbIncotermPsl = data.listEbIncotermPsl;

			this.appLoaded = true;
		} catch (e) {}
	}

	changeModule() {
		if (this.router.url.indexOf(Statique.moduleNamePath.PRICING) > -1) {
			this.module = Modules.PRICING;
		} else if (this.router.url.indexOf(Statique.moduleNamePath.TRANSPORT_MANAGEMENT) > -1) {
			this.module = Modules.TRANSPORT_MANAGEMENT;
		} else if (this.router.url.indexOf(Statique.moduleNamePath.TRACK) > -1) {
			this.module = Modules.TRACK;
		} else if (this.router.url.indexOf(Statique.moduleNamePath.FREIGHT_AUDIT) > -1) {
			this.module = Modules.FREIGHT_AUDIT;
		} else if (this.router.url.indexOf(Statique.moduleNamePath.QUALITY_MANAGEMENT) > -1) {
			this.module = Modules.QUALITY_MANAGEMENT;
		} else if (this.router.url.indexOf(Statique.moduleNamePath.CUSTOM) > -1) {
			this.module = Modules.CUSTOM;
		} else if (this.router.url.indexOf(Statique.moduleNamePath.RECEIPT_SCHEDULING) > -1) {
			this.module = Modules.RECEIPT_SCHEDULING;
		} else if (this.router.url.indexOf(Statique.moduleNamePath.ORDER_MANAGEMENT) > -1) {
			this.module = Modules.ORDER_MANAGEMENT;
		} else if (this.router.url.indexOf(Statique.moduleNamePath.DELIVERY_MANAGEMENT) > -1) {
			this.module = Modules.DELIVERY_MANAGEMENT;
		} else this.module = null;

		if (this.singletonStatique) this.singletonStatique.module = this.module;
	}

	// cette fonction sert juste a faire l'appel a la méthode qui gére les session ouverte si l'utilisateur n'a pas coché l'option "remember me"
	keepLoggedInFunction() {
		let rememberMe = localStorage.getItem("remember_me");
		if (rememberMe == "false") {
			sessionStorage.setItem("remember_temp", "true");
			this.windowStateManager(true);
		}
	}

	windowStateManager(isNewWindowPromotedToMain) {
		let self = this;
		this.isNewWindowPromotedToMain = isNewWindowPromotedToMain;
		this.windowId = Date.now().toString();

		window.addEventListener("beforeunload", function() {
			self.removeWindow();
		});
		window.addEventListener("unload", function() {
			self.removeWindow();
		});

		this.determineWindowState();

		this.initialized = true;
	}

	determineWindowState() {
		let self = this;
		this.windowArray = localStorage.getItem(this.LOCALSTORAGE_KEY);

		if (this.windowArray === null || this.windowArray === "NaN") {
			this.windowArray = [];
		} else {
			this.windowArray = JSON.parse(this.windowArray);
		}

		if (this.initialized) {
			//Determine if this window should be promoted
			if (
				this.windowArray.length <= 1 ||
				(this.isNewWindowPromotedToMain
					? this.windowArray[this.windowArray.length - 1]
					: this.windowArray[0]) === this.windowId
			) {
				this.isMainWindow = true;
			} else {
				this.isMainWindow = false;
			}
		} else {
			if (this.windowArray.length === 0) {
				this.isMainWindow = true;
				this.windowArray[0] = this.windowId;
				localStorage.setItem(this.LOCALSTORAGE_KEY, JSON.stringify(this.windowArray));
			} else {
				this.isMainWindow = false;
				this.windowArray.push(this.windowId);
				localStorage.setItem(this.LOCALSTORAGE_KEY, JSON.stringify(this.windowArray));
			}
		}
		//Perform a recheck of the window on a delay
		setTimeout(function() {
			self.determineWindowState.call(self);
		}, self.RECHECK_WINDOW_DELAY_MS);
	}

	//Remove the window from the global count
	removeWindow() {
		let windowArray = JSON.parse(localStorage.getItem(this.LOCALSTORAGE_KEY));
		for (let i = 0, length = windowArray.length; i < length; i++) {
			if (windowArray[i] === this.windowId) {
				windowArray.splice(i, 1);
				break;
			}
		}
		//Update the local storage with the new array
		localStorage.setItem(this.LOCALSTORAGE_KEY, JSON.stringify(windowArray));
	}

	toLoginForNotRememberMe() {
		let windowsArray = JSON.parse(localStorage.getItem(this.LOCALSTORAGE_KEY));
		if (
			windowsArray &&
			windowsArray.length == 0 &&
			sessionStorage.getItem("remember_temp") == null &&
			localStorage.getItem("remember_me") == "false"
		) {
			localStorage.removeItem("access_token");
			this.router.navigate(["/login"]);
		}
	}
}
