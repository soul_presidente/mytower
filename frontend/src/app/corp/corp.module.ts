import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { CorpComponent } from "./corp.component";
import { HomeComponent } from "./home/home.component";
import { TranslateModule } from "@ngx-translate/core";
import { SharedModule } from "@app/shared/module/shared.module";
import { PricingService } from "@app/services/pricing.service";

@NgModule({
	imports: [CommonModule, RouterModule, FormsModule, TranslateModule, SharedModule],
	declarations: [CorpComponent, HomeComponent],
	providers: [PricingService],
	exports: [CorpComponent],
})
export class CorpModule {}
