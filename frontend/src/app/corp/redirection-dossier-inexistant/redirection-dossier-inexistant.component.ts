import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "@app/services/authentication.service";
import { ActivatedRoute, Params } from "@angular/router";

@Component({
	selector: "app-redirection-dossier-inexistant",
	templateUrl: "./redirection-dossier-inexistant.component.html",
	styleUrls: ["./redirection-dossier-inexistant.component.css"],
})
export class RedirectDossier implements OnInit {
	variableGeneriqueNum: number;
	nomVariableGenerique: string;

	constructor(
		protected authenticationService: AuthenticationService,
		private route: ActivatedRoute
	) {}

	ngOnInit() {
		this.route.params.subscribe((params: Params) => {
			this.variableGeneriqueNum = params["variableGeneriqueNum"];
			this.nomVariableGenerique = params["variableModule"];
		});
	}
}
