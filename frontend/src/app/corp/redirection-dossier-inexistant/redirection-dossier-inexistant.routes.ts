import { Route } from "@angular/router";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";
import { RedirectDossier } from "./redirection-dossier-inexistant.component";

export const HomeRoutes: Route[] = [
	{
		path: "failedaccess/:variableGeneriqueNum/:moduleNum",
		component: RedirectDossier,
		canActivate: [UserAuthGuardService],
	},
];
