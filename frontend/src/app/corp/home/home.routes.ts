import { Route } from "@angular/router";
import { HomeComponent } from "./home.component";
import { UserAuthGuardService } from "@app/services/user-auth-guard.service";

export const HomeRoutes: Route[] = [
	{
		path: "",
		component: HomeComponent,
		canActivate: [UserAuthGuardService],
	},
];
