import { Component, OnInit, HostListener } from "@angular/core";
import { ConnectedUserComponent } from "@app/shared/connectedUserComponent";
import { GenericTableInfos } from "@app/shared/generic-table/generic-table-infos";
import { SearchCriteriaPricingBooking } from "@app/utils/SearchCriteriaPricingBooking";
import { Statique } from "@app/utils/statique";
import { TranslateService } from "@ngx-translate/core";
import { PricingService } from "@app/services/pricing.service";
import {
	DemandeStatus,
	GenericTableScreen,
	ModeTransport,
	Modules,
	SavedFormIdentifier,
	ActionType,
} from "@app/utils/enumeration";
import { GlobalService } from "@app/services/global.service";
import { SearchCriteria } from "@app/utils/searchCriteria";
import { SavedForm } from "@app/classes/savedForm";
import { NavigationExtras, Router } from "@angular/router";
import { SavedFormsService } from "@app/services/saved-forms.service";
import { CompagnieService } from "@app/services/compagnie.service";
import { EbCompagnie } from "@app/classes/compagnie";

@Component({
	selector: "app-home",
	templateUrl: "./home.component.html",
	styleUrls: ["./home.component.scss"],
})
export class HomeComponent extends ConnectedUserComponent implements OnInit {
	ModeTransport = ModeTransport;
	Statique = Statique;
	DemandeStatus = DemandeStatus;
	datatableComponent = GenericTableScreen.FAVORIS;

	dataInfos: GenericTableInfos = new GenericTableInfos(this);
	globalSearchCriteria: SearchCriteriaPricingBooking = new SearchCriteriaPricingBooking();

	listEbDemandeTransport: Array<any>;
	listFavorisCols: Array<any>;
	listFavorisData: Array<any>;
	listBoutonAction: Array<any>;
	activeSf: number;
	cp: EbCompagnie = new EbCompagnie();

	constructor(
		private translate: TranslateService,
		protected router: Router,
		protected savedFormsService: SavedFormsService,
		private pricingService: PricingService,
		private globalService: GlobalService,
		private compagnieService: CompagnieService
	) {
		super();
	}

	ngOnInit() {
		this.activeSf = 0;
		this.globalSearchCriteria.pageNumber = 1;
		this.globalSearchCriteria.connectedUserNum = this.userConnected.ebUserNum;
		this.globalSearchCriteria.ebCompagnieNum = this.userConnected.ebCompagnie.ebCompagnieNum;
		this.globalSearchCriteria.ebUserNum = this.userConnected.ebUserNum;

		this.globalService
			.runAction(Statique.controllerPricing + "/favoris-accueil", this.globalSearchCriteria)
			.subscribe((res) => {
				this.listFavorisData = res && res.data ? res.data : new Array<any>();
				this.onResizeFavoris();
				this.onResizeFavorisWrapper();
			});

		this.compagnieService.getCompagnie(this.globalSearchCriteria).subscribe((data) => {
			this.cp = data;
		});

		this.loadListEbDemandeTransport();
		this.getListBoutonAction();
	}

	@HostListener("window:resize", ["$event"])
	onResizeFavoris() {
		let windowHeight = window.innerHeight;
		let listEltsClass: string[] = [".app-header"];
		setTimeout(() => {
			this.Statique.onResizeAdapter(listEltsClass, "#ref-container", 70, windowHeight);
		}, 200);
	}

	@HostListener("window:resize", ["$event"])
	onResizeFavorisWrapper() {
		let windowHeight = window.innerHeight;
		let listEltsClass: string[] = [".app-header"];
		setTimeout(() => {
			this.Statique.onResizeAdapter(listEltsClass, "#search-container", 42, windowHeight);
		}, 200);
	}

	triggerBoutonAction(bouton) {
		if (bouton.type == ActionType.NEW || bouton.type == ActionType.DASHBOARD) {
			this.router.navigate([bouton.action]);
		} else {
			this.sendSavedForm(JSON.parse(bouton.action));
		}
	}

	getFaIcon(code): string {
		return "fa " + Statique.getFaIconByFlagIconCode(code);
	}

	getListBoutonAction() {
		let criteria: SearchCriteria = new SearchCriteria();
		criteria.boutonAction = true;
		criteria.orderedColumn = "ordre";
		criteria.ascendant = true;
		this.globalService
			.runAction(Statique.controllerBoutonAction + "/list-bouton-action", criteria)
			.subscribe(
				(res) => {
					this.listBoutonAction = res.data;
				},
				(err) => console.error(err),
				() => {
					this.listBoutonAction.forEach((button) => {
						if (button.type == ActionType.FILTER) {
							let sf = JSON.parse(button.action);
							criteria.ebUserNum = sf.ebUser.ebUserNum;
							criteria.connectedUserNum = criteria.ebUserNum;
							criteria.orderedColumn = null;
							let formValues = JSON.parse(sf.formValues);
							let searchInput = JSON.stringify(this.getSearchInput(formValues));
							criteria.searchInput = searchInput;
							let path = "";
							if (sf.url) path = sf.url;
							else
								path = Statique.getComponentPathByCompIdAndModuleId(sf.ebCompNum, sf.ebModuleNum);

							let apiUrl = "api/" + path.split("/")[2];
							if (path.split("/")[2] === "delivery-overview" && path.split("/")[3] === "order") {
								apiUrl = Statique.controllerOrderOverView;
							}
							if (path.split("/")[2] === "track-trace") {
								apiUrl = Statique.controllerTrackTrace;
							}
							if (path.split("/")[2] === "transport-management") {
								apiUrl = Statique.controllerPricing;
							}
							if (path.split("/")[2] === "freight-audit") {
								apiUrl = Statique.controllerFreightAudit;
							}
							if (path.split("/")[2] === "quality-management") {
								apiUrl = Statique.controllerQualityManagement;
							}
							this.globalService.getSavedFormResultCount(apiUrl+ "/list-count", criteria).subscribe((res: any) => {
								button.count = res;
							});
						}
					});
				}
			);
	}

	navigateToComponentTarget(sf: SavedForm) {
		let path = null;
		if (sf.url) path = sf.url;
		else path = "app/" + Statique.getComponentPathByCompIdAndModuleId(sf.ebCompNum, sf.ebModuleNum);

		let navigationExtras: NavigationExtras = { queryParams: { sf: sf.ebSavedFormNum } };
		localStorage.setItem(
			"sf" + sf.ebSavedFormNum,
			JSON.stringify({
				ebCompNum: sf.ebCompNum,
				ebModuleNum: sf.ebModuleNum,
				ebSavedFormNum: sf.ebSavedFormNum,
				ebUserNum: sf.ebUserNum,
				formName: sf.formName,
				formValues: sf.formValues,
				url: sf.url,
			})
		);

		if (!this.router.isActive(path, true)) this.router.navigate([path], navigationExtras);
	}

	getSearchInput(listSelectedFields: Array<any>) {
		if (listSelectedFields && listSelectedFields.length > 0) {
			let result = {};

			listSelectedFields.forEach((field) => {
				result[field.name] = field.default ? field.default : field.value;
			});
			return result;
		}

		return null;
	}
	sendSavedForm(sf: SavedForm) {
		this.activeSf = sf.ebSavedFormNum;
		this.navigateToComponentTarget(sf);

		if (sf.ebCompNum == SavedFormIdentifier.SAVED_SEARCH) {
			let event = new CustomEvent("savedsearch", { detail: sf });
			document.dispatchEvent(event);
		} else {
			this.savedFormsService.emitSavedForm(sf);
		}
	}

	getFavorisColumns() {
		let cols: Array<GenericTableInfos.Col> = [
			{
				field: "xEcModule",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				cardOrder: 6,
				render(data, diplay, row) {
					if (data == Modules.PRICING) {
						return '<i class="my-icon-order"></i>';
					}
					if (data == Modules.TRANSPORT_MANAGEMENT) {
						return '<i class="my-icon-trans-management"></i>';
					}
					if (data == Modules.TRACK) {
						return '<i class="my-icon-track"></i>';
					}
					if (data == Modules.QUALITY_MANAGEMENT) {
						return '<i class="my-icon-claims "></i>';
					}
					if (data == Modules.FREIGHT_AUDIT) {
						return '<i class="my-icon-freight"></i>';
					}
					if (data == null) {
						return '<i class="my-icon-order"></i>';
					}
				},
			},
			{ field: "ref", isCardTitle: true, cardOrder: 1 },
			{
				field: "modeTransport",
				renderMode: GenericTableInfos.Col.RenderModeTable.HTML,
				cardOrder: 5,
			},
			{ field: "origin", cardOrder: 2 },
			{ field: "destination", cardOrder: 3 },
			{ field: "statutStr", cardOrder: 7, isCardLabel: true },
		];

		if (!this.userConnected.rolePrestataire) {
			cols.splice(2, 0, { field: "transporteur", translateCode: "Transporteur" });
		}
		if (!this.userConnected.chargeur) {
			cols.splice(2, 0, { field: "chargeur", translateCode: "Chargeur" });
		}

		return cols;
	}

	loadListEbDemandeTransport() {
		this.pricingService.getListDemandeTransport(this.globalSearchCriteria).subscribe((res) => {
			this.listEbDemandeTransport = res.data;
		});
	}

	goToDetails($event: any) {
		let moduleNum = $event.xEcModule;
		let moduleRoute = Statique.moduleNamePath[Statique.getModuleNameByModuleNum(moduleNum)];
		let link = "/app/" + moduleRoute + "/details/" + $event["id"];
		window.open(link, "_blank");
	}
	getModeTransportIcon(xEcModeTransport: number) {
		if (!Statique.isDefined(xEcModeTransport)) return "";
		let v = Statique.modeTransport.find((it) => it.key == xEcModeTransport);
		return v ? v.icon : "";
	}
}
