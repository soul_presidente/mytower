import { Route } from "@angular/router";
import { HomeRoutes } from "./home/home.routes";
import { CorpComponent } from "./corp.component";

export const CorpRoutes: Route[] = [
	{
		path: "",
		component: CorpComponent,
		/*canActivate: [UserAuthGuardService],*/
		children: [...HomeRoutes],
	},
];
