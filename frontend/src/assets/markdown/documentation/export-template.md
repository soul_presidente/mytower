# MyTower Template Manager documentation

## Create a template

### Main informations

#### What is a template ?

A template is a docx document created with Word 2010+. This file contains expressions and functions that will be interpreted by the template manager.\
The template manager will transform the document using functione and replace expressions with values and return a new docx file as output.\
This output can be transformed to a pdf file.

#### Model context

Template manager work with a context model. This context model is the object requested for export. It can be for example Transport request, Purchase order.\
On the MyTower documentation, there is one tab per context model object.\
Model object contains properties. Some properties can be list or contains sub-properties.

#### Expressions

The template manager will replace expression by object values.
Expressions are placed inside the document and are always enclosed between `${` and `}`.

Example : \
`${demandePrincipal.partyOrigin.zipCode}` will be replaced by the zip code at departure for a transport request.

Expressions can also contains functions. They are called incorporated functions.

Example : \
- `${stackable}` will output `true` if an item is stackable and `false` otherwise
- `${formatBool(stackable)}` will output `Yes` if an item is stackable and `No` otherwise

Expressions are Spring Expression Language (SpEL) that allow multiple advanced features. See all possibilities at the [official SpEL documentation](https://docs.spring.io/spring/docs/current/spring-framework-reference/core.html#expressions).

#### Functions

The goal of expressions is only to be replaced by a value.
Functions can do more things.
- Incorportated functions will work on the expression output
- Functions will work on the document structure

### MyTower common usages

#### Sub-properties

You can access sub-properties using a dot between parent and child property.

Example : \
`${demandePrincipal.partyOrigin.city}` : Here we want to show the value of the `city` property contained and the `partyOrigin` property contained in the `demandePrincipal` property itself contained in the transport request context model.

#### List

Follow next steps to render a list :
- Create a table with at least one row
- Locate the row you want to repeat for each items in list
- Write expressions in cells. Expressions are not contexted on the root model but on the list model.
- Select at least ont character in the row and create a Word comment
- Enter the following function in the comment `repeatTableRow(List)`

Example : \
In this example we will create a table to display 2 fields of transport request units
- Create a table with 2 rows and 2 columns
- In the first row, enter headers text.
  - First column `Unit reference`
  - Second column `Gerbable`
- In the second row, enter expressions
  - First column `${unitReference}`
  - Second column `${formatBool(stackable)}`
- With the cursur, select at least one character of the second row
- Create a Word comment
- Enter the following function in the comment `repeatTableRow(demandePrincipal.units)`

#### Incorporated functions

MyTower embeds the followings incorporated functions :
- `formatBool(boolean)` : Transform a boolean value on a user friendly Yes/No value
- `formatDate(date)` : Display a date in a common format
- `replaceByMandatoryIfEmpty(Object object)` : If object is null, return a mandatory message
- `replaceByMandatoryIfEmpty(Object object, Boolean returnEmptyIfPresent)` : If object is null, return a mandatory message. If  `returnEmptyIfPresent` is true, it will return empty string instead of the checked object himself
- `replaceByMandatoryIfEmpty(String value)` : If string is null, empty or blank, return a mandatory message
- `replaceByMandatoryIfEmpty(String value, Boolean returnEmptyIfPresent)` : If string is null, empty or blank, return a mandatory message. If  `returnEmptyIfPresent` is true, it will return empty string instead of the checked object himself

#### Advanced properties

##### Custom fields

- `${demandePrincipal.customFields.get("code")}` : Replace code with the code of the custom field

##### Categories

- `${demandePrincipal.categorieLabelValue.get("code")}` : Replace code with the name of the category to retrieve the selected label
