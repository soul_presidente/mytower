jQuery(document).ready(function() {
		
			$(".file-dropzone").on('dragover', handleDragEnter);
			$(".file-dropzone").on('dragleave', handleDragLeave);
			$(".file-dropzone").on('drop', handleDragLeave);

			function handleDragEnter(e) {

				this.classList.add('drag-over');
			}

			function handleDragLeave(e) {

				this.classList.remove('drag-over');
			}

			// "dropzoneForm" is the camel-case version of the form id
			// "dropzone-form"
			Dropzone.options.dropzoneForm = {

				
				url:UrlAttachedFileniv3,
				autoProcessQueue : true,
				uploadMultiple : false,
				maxFilesize : 256, // MB
				parallelUploads : 100,
				createImageThumbnails : false,
				maxFiles : 100,
				addRemoveLinks : true,

				// The setting up of the dropzone
				init : function() {
					var myDropzone = this;
					// reload after upload
				//	$('#myTable > tbody:last-child').append('<tr><td>testbb</td><td>..</td></tr>');
					
					this.on('success', function(file, response){
						var trplus = $ ("<div class='row'>" +
								"<span><a href='#' class='delete-file'>" +
								"<span class='fa fa-trash fa-trash-doc'></span></a>" +
								"<input type='hidden' value='"+ parseInt(response.id) + "' class='filehiddenid'/></span> <a href='../../../"+response.chemin+ "' target='_blank' class='fa fa-file-pdf-o'></a>" +
								"<span ><i>" + file.name + "</i></span>" +
										"</div>");

						
						$("#attachements").append(trplus);
						$(".dz-preview").remove();
						console.log("upload succesful!");
					});
				}
			}

			
		});