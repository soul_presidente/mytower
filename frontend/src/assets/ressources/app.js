jQuery(document).ready(
		function() {
			$(".file-dropzone").on('dragover', handleDragEnter);
			$(".file-dropzone").on('dragleave', handleDragLeave);
			$(".file-dropzone").on('drop', handleDragLeave);

			function handleDragEnter(e) {

				this.classList.add('drag-over');
			}

			function handleDragLeave(e) {

				this.classList.remove('drag-over');
			}

			// "dropzoneForm" is the camel-case version of the form id
			// "dropzone-form"
			Dropzone.options.dropzoneForm = {

				url:UrlAttachedFile,
				autoProcessQueue : true,
				uploadMultiple : false,
				maxFilesize : 256, // MB
				parallelUploads : 100,
				createImageThumbnails : false,
				maxFiles : 100,
				addRemoveLinks : true,

				// The setting up of the dropzone
				init : function() {
					var myDropzone = this;
					
					this.on('success', function(file, response){
						var trplus = $ ("<div class='row'>" +
								"<span><a href='#' class='file-delete'>" +
								"<span class='fa fa-trash fa-trash-doc'></span>&nbsp;</a>" +
								"<input type='hidden' value='"+ parseInt(response.id) + "' class='hiddeninput'/>" +
								"<a href='../../../"+response.chemin+ "' target='_blank' class='fa fa-file-pdf-o'></a>&nbsp;<span ><i>" + file.name + "</i></span>" +
				
						"</div>");

						
						$("#attachmnt").append(trplus);
						$(".dz-preview").remove();
						console.log("upload succesful!");
					});
				}
			}
		});
