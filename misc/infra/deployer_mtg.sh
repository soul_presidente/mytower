GIT_BRANCH="mtg/dev/master"
ENV="recette-client"
BUILD_FRONT="true"
GIT_PULL="true"
RUN_TESTS="true"
echo $@
for i in "$@" 
do
echo $i 
case $i in
    -e=*|--environnement=*)
    ENV="${i#*=}"
    shift # past argument=value
    ;;
    -g=*|--gitbranch=*)
    GIT_BRANCH="${i#*=}"
    shift # past argument=value
    ;;
    -i=*|--ignorefront=*)
    BUILD_FRONT="false"
    shift # past argument=value
    ;;
	  -p=*|--gitpull=*)
    GIT_PULL="${i#*=}"
    shift # past argument=value
    ;;
    --default)
    DEFAULT=YES 
    shift # past argument with no value
    ;;
    --no-test)
    RUN_TESTS="false" 
    shift # past argument with no value
    ;;
    *)
          # unknown option
    ;;
esac
done
#echo "FILE EXTENSION  = ${EXTENSION}"
#echo "SEARCH PATH     = ${SEARCHPATH}"
#echo "LIBRARY PATH    = ${LIBPATH}"

SPACE=" "


#ENV_CAPITALIZED="RecetteClient"
#remplacer - par espace
TEMP="${ENV//[-]/ }"  

#Transformer  chaque  premiere lettre en majuscule
B=( $TEMP ); 
#echo "B = $B"
ENV_CAPITALIZED="${B[@]^}"

#echo "TEMP = $TEMP"
#echo "ENV_CAPITALIZED = $ENV_CAPITALIZED"
#fin de la fonction

#supprimer les espaces
 ENV_CAPITALIZED="${ENV_CAPITALIZED//[[:blank:]]/}"
#echo "ENV_CAPITALIZED = $ENV_CAPITALIZED"
 





##########################################################################################
############### Scrip de deploiement             #########################################
##########################################################################################


echo " GIT_PULL = $GIT_PULL"
echo " BUILD_FRONT = $BUILD_FRONT"
echo " ENV = $ENV"
echo " ENV_CAPITALIZED = $ENV_CAPITALIZED"
echo " RUN_TESTS = $RUN_TESTS"

if  "$GIT_PULL" = "true"; then 
	echo "**************pull modification ************** from branch $GIT_BRANCH "
  git fetch -p; 
  git reset --hard origin/$GIT_BRANCH;
  git checkout -b $GIT_BRANCH  origin/$GIT_BRANCH; 
  git pull origin $GIT_BRANCH; 
else 
	echo " GIT_PULL  semble false "
fi 


chmod -R 777 *


#supprimer les ancienne classes Mapper genere lors des build avant migration vers src/generated pour ces mapper
rm -rf /home/workspace/git-repositories/mytower-generique/backend/src/main/java/com/adias/mytowereasy/dto/mapper/*Impl.java ;
 
if  "$RUN_TESTS" = "true"; then 
	echo "Launch build with tests"
  ./gradlew clean fullBuild test -Penvironment=$ENV -PnodeArgs='--max_old_space_size=10000' -PfrontConfiguration='embedded' --no-daemon --stacktrace;
else 
	echo "Launch build without tests"
  ./gradlew clean fullBuild -Penvironment=$ENV -PnodeArgs='--max_old_space_size=10000' -PfrontConfiguration='embedded' --no-daemon --stacktrace;
fi 


#if "$BUILD_FRONT" = "true"; then 
#	echo "builder le front"
#  ./gradlew clean front$ENV_CAPITALIZED --no-daemon;
#fi 
#supprimer  eventuellement le war genere pour l'environnement
yes | rm -f /home/adias/deploiement/mytower-$ENV.war;

#builder le back en vue de generer le war (warRecetteClient, warDev, warPreprod, warProd, warDevcentos ...)
#./gradlew clean war$ENV_CAPITALIZED --no-daemon; 
yes|cp backend/build/libs/mtg-backend-*-$ENV.war /home/adias/deploiement/mytower-$ENV.war;

