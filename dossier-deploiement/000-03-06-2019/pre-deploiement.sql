
/*
43-update-users-chanel
*/
ALTER TABLE work.eb_user ADD dtype varchar(50);
UPDATE work.eb_user SET dtype = 'EbUser'; 



/*
66.update-schemapsl
*/
ALTER TABLE work.eb_tt_schema_psl RENAME eb_incoterm_psl_num TO eb_tt_schema_psl_num;
DELETE FROM work.eb_tt_schema_psl;




/* change code type to varchar */
ALTER TABLE "work"."eb_demande_fichiers_joint" 
  ALTER COLUMN "id_categorie" TYPE varchar(255) USING "id_categorie"::varchar(255);



delete from work.eb_qm_root_cause ;


DROP TABLE IF EXISTS work.ec_zone CASCADE;
DROP TABLE IF EXISTS work.eb_zone CASCADE;
DROP TABLE IF EXISTS work.eb_tranche CASCADE;
DROP TABLE IF EXISTS work.eb_grille_transport CASCADE;
DROP TABLE IF EXISTS work.eb_plan_transport CASCADE;
DROP TABLE IF EXISTS work.ln_grille_transport__tranche CASCADE;
DROP TABLE IF EXISTS work.ln_grille_transport_tranche CASCADE;
DROP TABLE IF EXISTS work.ln_grille_transport__tranche CASCADE;
DROP TABLE IF EXISTS work.eb_pl_grille_line CASCADE;
DROP TABLE IF EXISTS work.eb_pl_ligne_grille_transport_tranche CASCADE;




