update work.eb_user u set username = u.email;
/*deja en prod : Correction du quiproquo sur le code du module Quality management */
UPDATE "work".eb_user_module SET x_ec_module = 3 WHERE x_ec_module = 6; 
update work.eb_etablissement et set role = (select role from work.eb_user u where u.x_eb_etablissement = et.eb_etablissement_num limit 1) where et.role is null;
update work.eb_etablissement et set role = case when et.service is null then 1 else 2 end where role is null;
update work.eb_compagnie et set compagnie_role = (select role from work.eb_user u where u.x_eb_compagnie = et.eb_compagnie_num limit 1) where et.compagnie_role is null;




update work.eb_demande set dg=true where eb_demande_num in (
select x_eb_demande from work.eb_marchandise where dangerous_good >1
);


INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (1,'UTC -12',-12);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (3,'UTC -10',-10);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (2,'UTC -11',-11);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (4,'UTC -9',-9);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (5,'UTC -8',-8);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (6,'UTC -7',-7);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (7,'UTC -6',-6);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (8,'UTC -5',-5);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (9,'UTC -4',-4);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (10,'UTC -3',-3);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (11,'UTC -2',-2);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (12,'UTC -1',-1);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (13,'UTC 0',0);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (14,'UTC 1',1);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (15,'UTC 2',2);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (16,'UTC 3',3);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (17,'UTC 4',4);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (18,'UTC 5',5);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (19,'UTC 6',6);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (20,'UTC 7',7);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (21,'UTC 8',8);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (22,'UTC 9',9);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (23,'UTC 10',10);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (25,'UTC 12',12);
INSERT INTO work.ec_fuseaux_horaire (ec_fuseaux_horaire_num, code, decalage)VALUES (24,'UTC 11',11);






-- insert type transports
INSERT INTO work."eb_type_transport"("eb_type_transport_num", "x_ec_mode_transport", "libelle") VALUES (1, 1, 'CONSOLIDATION');
INSERT INTO work."eb_type_transport"("eb_type_transport_num", "x_ec_mode_transport", "libelle") VALUES (2, 1, 'DIRECT');
INSERT INTO work."eb_type_transport"("eb_type_transport_num", "x_ec_mode_transport", "libelle") VALUES (3, 2, 'LCL');
INSERT INTO work."eb_type_transport"("eb_type_transport_num", "x_ec_mode_transport", "libelle") VALUES (4, 2, 'FCL');
INSERT INTO work."eb_type_transport"("eb_type_transport_num", "x_ec_mode_transport", "libelle") VALUES (5, 3, 'CONSOLIDATION');
INSERT INTO work."eb_type_transport"("eb_type_transport_num", "x_ec_mode_transport", "libelle") VALUES (6, 3, 'DEDICATED');

-- update for EbDemande

/* mode de transport air */
UPDATE "work"."eb_demande" SET "x_eb_type_transport" = 1  where "x_ec_mode_transport"=1 and "x_ec_type_transport"= 1;
UPDATE "work"."eb_demande" SET "x_eb_type_transport" = 2  where "x_ec_mode_transport"=1 and "x_ec_type_transport"= 2;

/* mode de transport sea */
UPDATE "work"."eb_demande" SET "x_eb_type_transport" = 3  where "x_ec_mode_transport"=2 and "x_ec_type_transport"= 1;
UPDATE "work"."eb_demande" SET "x_eb_type_transport" = 4  where "x_ec_mode_transport"=2 and "x_ec_type_transport"= 2;

/* mode de transport Road */
UPDATE "work"."eb_demande" SET "x_eb_type_transport" = 5  where "x_ec_mode_transport"=3 and "x_ec_type_transport"= 1;
UPDATE "work"."eb_demande" SET "x_eb_type_transport" = 6  where "x_ec_mode_transport"=3 and "x_ec_type_transport"= 2;


-- update for EbInvoice

/* mode de transport air */
UPDATE "work"."eb_invoice" SET "x_eb_type_transport" = 1  where "x_ec_mode_transport"=1 and "x_ec_type_transport"= 1;
UPDATE "work"."eb_invoice" SET "x_eb_type_transport" = 2  where "x_ec_mode_transport"=1 and "x_ec_type_transport"= 2;

/* mode de transport sea */
UPDATE "work"."eb_invoice" SET "x_eb_type_transport" = 3  where "x_ec_mode_transport"=2 and "x_ec_type_transport"= 1;
UPDATE "work"."eb_invoice" SET "x_eb_type_transport" = 4  where "x_ec_mode_transport"=2 and "x_ec_type_transport"= 2;

/* mode de transport Road */
UPDATE "work"."eb_invoice" SET "x_eb_type_transport" = 5  where "x_ec_mode_transport"=3 and "x_ec_type_transport"= 1;
UPDATE "work"."eb_invoice" SET "x_eb_type_transport" = 6  where "x_ec_mode_transport"=3 and "x_ec_type_transport"= 2;




-- remove module 6 : CLAIMS (old name of quality management )
delete from work."eb_user_module" where x_ec_module = 6 or ec_module_num=6;
delete from work."eb_user_profile_module" where x_ec_module = 6;
delete FROM work."ec_module" where ec_module_num = 6;

-- update old libeles with the one in i18n json files
update work.ec_module set
    libelle = 'PRICING' where ec_module_num = 1;

update work.ec_module set
    libelle = 'TM' where ec_module_num = 2;

update work.ec_module set
    libelle = 'QM' where ec_module_num = 3;

update work.ec_module set
    libelle = 'COMPLIANCE_MATRIX' where ec_module_num = 4;

update work.ec_module set
    libelle = 'TT' where ec_module_num = 5;

update work.ec_module set
    libelle = 'FA' where ec_module_num = 7;

update work.ec_module set
    libelle = 'ANALYTICS' where ec_module_num = 8;

update work.ec_module set
    libelle = 'CUSTOMS' where ec_module_num = 9;

update work.ec_module set
    libelle = 'ORDER' where ec_module_num = 10;

update work.ec_module set
    libelle = 'RSCH' where ec_module_num = 11;

-- insert missing module Chanel PB Delivery
insert into work.ec_module(ec_module_num, libelle) values (18, 'DELIVERY');

-- insert module compta matiere
insert into work.ec_module(ec_module_num, libelle) values (19, 'COMPTA_MATIERE');








/** update num_entity with x_eb_demande value */
 update work.eb_demande_fichiers_joint set num_entity = x_eb_demande;
 
 
 
 
 
 -- put values for the new comlumn "type_event"	
UPDATE work.eb_tt_categorie_deviation
	SET type_event=1 WHERE eb_tt_categorie_deviation_num BETWEEN 1 AND 9;
	
	
delete from work.eb_tt_categorie_deviation
where eb_tt_categorie_deviation_num BETWEEN 21 AND 24;


INSERT INTO work.eb_tt_categorie_deviation(
    eb_tt_categorie_deviation_num, code, libelle)
    VALUES (10 ,'10', 'Missing update');
INSERT INTO work.eb_tt_categorie_deviation(
    eb_tt_categorie_deviation_num, code, libelle)
    VALUES (11 ,'11', 'Delay');
INSERT INTO work.eb_tt_categorie_deviation(
    eb_tt_categorie_deviation_num, code, libelle, type_event)
    VALUES (21 ,'21', 'Missing document', 3);
	
	
	
update work.eb_demande set x_ec_incoterm_libelle = (select libelle from work.eb_incoterm where eb_incoterm_num = x_ec_incoterm) where x_ec_incoterm_libelle is null;



INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (87, '87', null, null, 'documents', 'Documents', null, null, null, 'DOCUMENTS', null, null,1, 2);
	
	
	INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (80, '80', null, null, 'leg', 'Segment', null, null, null, 'LEG', null, null,1, 2);

INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (81, '81', null, null, 'nbOfTemporaryRegime', 'Nb régimes temporaires associés', null, null, null, 'NB_OF_TEMPORARY_REGIME_LINKED', null, null, 1, 2);

INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (82, '82', null, null, 'dateEcs', 'Date Ecs', null, null, null, 'DATE_ECS', null, null, 7, 2);

INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (83, '83', null, null, 'statut', 'Statut', null, null, null, 'STATUS', null, null, 1, 2);

INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (84, '84', null, null, 'declarationNumber', 'Numéro de déclaration', null, null, null, 'DECLARATION_NUMBER', null, null, 3, 2);

INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (85, '85', null, null, 'declarationReference', 'Référence déclaration', null, null, null, 'DECLARATION_REFERENCE', null, null,1, 2);

INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (86, '86', null, null, 'xEbUserDeclarant', 'Déclarant', null, null, null, 'DECLARANT', null, null,1, 2);

update work.ec_currency set  exchange_rate_euro = 88.2612965 where code  = 'AFN' ;
update work.ec_currency set  exchange_rate_euro = 122.653151 where code  = 'ALL' ;
update work.ec_currency set  exchange_rate_euro = 534.5429113 where code  = 'AMD' ;
update work.ec_currency set  exchange_rate_euro = 2.086439173 where code  = 'ANG' ;
update work.ec_currency set  exchange_rate_euro = 367.7963977 where code  = 'AOA' ;
update work.ec_currency set  exchange_rate_euro = 49.59105078 where code  = 'ARS' ;
update work.ec_currency set  exchange_rate_euro = 1.610239983 where code  = 'AUD' ;
update work.ec_currency set  exchange_rate_euro = 2.003130748 where code  = 'AWG' ;
update work.ec_currency set  exchange_rate_euro = 1.897673525 where code  = 'AZN' ;
update work.ec_currency set  exchange_rate_euro = 1.953991578 where code  = 'BAM' ;
update work.ec_currency set  exchange_rate_euro = 2.216271206 where code  = 'BBD' ;
update work.ec_currency set  exchange_rate_euro = 94.0266624 where code  = 'BDT' ;
update work.ec_currency set  exchange_rate_euro = 1.955772386 where code  = 'BGN' ;
update work.ec_currency set  exchange_rate_euro = 0.4195750598 where code  = 'BHD' ;
update work.ec_currency set  exchange_rate_euro = 2060.172255 where code  = 'BIF' ;
update work.ec_currency set  exchange_rate_euro = 1.113005 where code  = 'BMD' ;
update work.ec_currency set  exchange_rate_euro = 1.503447154 where code  = 'BND' ;
update work.ec_currency set  exchange_rate_euro = 7.689584594 where code  = 'BOB' ;
update work.ec_currency set  exchange_rate_euro = 4.428201693 where code  = 'BRL' ;
update work.ec_currency set  exchange_rate_euro = 1.112838049 where code  = 'BSD' ;
update work.ec_currency set  exchange_rate_euro = 77.818 where code  = 'BTN' ;
update work.ec_currency set  exchange_rate_euro = 12.11617243 where code  = 'BWP' ;
update work.ec_currency set  exchange_rate_euro = 2.331912425 where code  = 'BYN' ;
update work.ec_currency set  exchange_rate_euro = 2.243038976 where code  = 'BZD' ;
update work.ec_currency set  exchange_rate_euro = 1.50273483 where code  = 'CAD' ;
update work.ec_currency set  exchange_rate_euro = 1842.579777 where code  = 'CDF' ;
update work.ec_currency set  exchange_rate_euro = 1.121675308 where code  = 'CHF' ;
update work.ec_currency set  exchange_rate_euro = 790.1166844 where code  = 'CLP' ;
update work.ec_currency set  exchange_rate_euro = 7.681849209 where code  = 'CNY' ;
update work.ec_currency set  exchange_rate_euro = 3741.700209 where code  = 'COP' ;
update work.ec_currency set  exchange_rate_euro = 654.3578996 where code  = 'CRC' ;
update work.ec_currency set  exchange_rate_euro = 1.12 where code  = 'CUC' ;
update work.ec_currency set  exchange_rate_euro = 1.11306065 where code  = 'CUP' ;
update work.ec_currency set  exchange_rate_euro = 110.6610786 where code  = 'CVE' ;
update work.ec_currency set  exchange_rate_euro = 25.83284605 where code  = 'CZK' ;
update work.ec_currency set  exchange_rate_euro = 197.8032486 where code  = 'DJF' ;
update work.ec_currency set  exchange_rate_euro = 7.467016984 where code  = 'DKK' ;
update work.ec_currency set  exchange_rate_euro = 58.4327625 where code  = 'DOP' ;
update work.ec_currency set  exchange_rate_euro = 133.3046088 where code  = 'DZD' ;
update work.ec_currency set  exchange_rate_euro = 18.6767804 where code  = 'EGP' ;
update work.ec_currency set  exchange_rate_euro = 16.696 where code  = 'ERN' ;
update work.ec_currency set  exchange_rate_euro = 32.3661854 where code  = 'ETB' ;
update work.ec_currency set  exchange_rate_euro = 2.405537706 where code  = 'FJD' ;
update work.ec_currency set  exchange_rate_euro = 0.882499 where code  = 'FKP' ;
update work.ec_currency set  exchange_rate_euro = 0.8825016645 where code  = 'GBP' ;
update work.ec_currency set  exchange_rate_euro = 3.121979025 where code  = 'GEL' ;
update work.ec_currency set  exchange_rate_euro = 1.13311 where code  = 'GGP[G]' ;
update work.ec_currency set  exchange_rate_euro = 5.893361475 where code  = 'GHS' ;
update work.ec_currency set  exchange_rate_euro = 0.8811493634 where code  = 'GIP' ;
update work.ec_currency set  exchange_rate_euro = 55.36643372 where code  = 'GMD' ;
update work.ec_currency set  exchange_rate_euro = 10267.47112 where code  = 'GNF' ;
update work.ec_currency set  exchange_rate_euro = 8.579654692 where code  = 'GTQ' ;
update work.ec_currency set  exchange_rate_euro = 232.5290046 where code  = 'GYD' ;
update work.ec_currency set  exchange_rate_euro = 8.734028486 where code  = 'HKD' ;
update work.ec_currency set  exchange_rate_euro = 27.36322792 where code  = 'HNL' ;
update work.ec_currency set  exchange_rate_euro = 7.418957428 where code  = 'HRK' ;
update work.ec_currency set  exchange_rate_euro = 101.5305421 where code  = 'HTG' ;
update work.ec_currency set  exchange_rate_euro = 324.6412984 where code  = 'HUF' ;
update work.ec_currency set  exchange_rate_euro = 16038.40205 where code  = 'IDR' ;
update work.ec_currency set  exchange_rate_euro = 4.043101963 where code  = 'ILS' ;
update work.ec_currency set  exchange_rate_euro = 0.882544 where code  = 'IMP[G]' ;
update work.ec_currency set  exchange_rate_euro = 77.63822027 where code  = 'INR' ;
update work.ec_currency set  exchange_rate_euro = 1324.47595 where code  = 'IQD' ;
update work.ec_currency set  exchange_rate_euro = 46863.07552 where code  = 'IRR' ;
update work.ec_currency set  exchange_rate_euro = 138.4912121 where code  = 'ISK' ;
update work.ec_currency set  exchange_rate_euro = 0.882544 where code  = 'JEP[G]' ;
update work.ec_currency set  exchange_rate_euro = 150.2334149 where code  = 'JMD' ;
update work.ec_currency set  exchange_rate_euro = 0.789120545 where code  = 'JOD' ;
update work.ec_currency set  exchange_rate_euro = 122.0098341 where code  = 'JPY' ;
update work.ec_currency set  exchange_rate_euro = 112.7418414 where code  = 'KES' ;
update work.ec_currency set  exchange_rate_euro = 77.74339925 where code  = 'KGS' ;
update work.ec_currency set  exchange_rate_euro = 4516.017787 where code  = 'KHR' ;
update work.ec_currency set  exchange_rate_euro = 492.5603627 where code  = 'KMF' ;
update work.ec_currency set  exchange_rate_euro = 1001.78 where code  = 'KPW' ;
update work.ec_currency set  exchange_rate_euro = 1324.186568 where code  = 'KRW' ;
update work.ec_currency set  exchange_rate_euro = 0.339021323 where code  = 'KWD' ;
update work.ec_currency set  exchange_rate_euro = 0.927372461 where code  = 'KYD' ;
update work.ec_currency set  exchange_rate_euro = 423.9992547 where code  = 'KZT' ;
update work.ec_currency set  exchange_rate_euro = 9624.710737 where code  = 'LAK' ;
update work.ec_currency set  exchange_rate_euro = 1682.474008 where code  = 'LBP' ;
update work.ec_currency set  exchange_rate_euro = 196.3229519 where code  = 'LKR' ;
update work.ec_currency set  exchange_rate_euro = 211.4152997 where code  = 'LRD' ;
update work.ec_currency set  exchange_rate_euro = 16.28326315 where code  = 'LSL' ;
update work.ec_currency set  exchange_rate_euro = 1.552641975 where code  = 'LYD' ;
update work.ec_currency set  exchange_rate_euro = 10.83276636 where code  = 'MAD' ;
update work.ec_currency set  exchange_rate_euro = 20.23888292 where code  = 'MDL' ;
update work.ec_currency set  exchange_rate_euro = 4062.46825 where code  = 'MGA' ;
update work.ec_currency set  exchange_rate_euro = 61.60315724 where code  = 'MKD' ;
update work.ec_currency set  exchange_rate_euro = 1699.725585 where code  = 'MMK' ;
update work.ec_currency set  exchange_rate_euro = 2944.71 where code  = 'MNT' ;
update work.ec_currency set  exchange_rate_euro = 8.995584661 where code  = 'MOP' ;
update work.ec_currency set  exchange_rate_euro = 408.472 where code  = 'MRO' ;
update work.ec_currency set  exchange_rate_euro = 40.8472835 where code  = 'MRU' ;
update work.ec_currency set  exchange_rate_euro = 39.65247263 where code  = 'MUR' ;
update work.ec_currency set  exchange_rate_euro = 17.19592725 where code  = 'MVR' ;
update work.ec_currency set  exchange_rate_euro = 801.5361157 where code  = 'MWK' ;
update work.ec_currency set  exchange_rate_euro = 21.3049191 where code  = 'MXN' ;
update work.ec_currency set  exchange_rate_euro = 4.659595432 where code  = 'MYR' ;
update work.ec_currency set  exchange_rate_euro = 69.2734312 where code  = 'MZN' ;
update work.ec_currency set  exchange_rate_euro = 16.28326315 where code  = 'NAD' ;
update work.ec_currency set  exchange_rate_euro = 400.6818 where code  = 'NGN' ;
update work.ec_currency set  exchange_rate_euro = 37.0408064 where code  = 'NIO' ;
update work.ec_currency set  exchange_rate_euro = 9.762957088 where code  = 'NOK' ;
update work.ec_currency set  exchange_rate_euro = 124.4172639 where code  = 'NPR' ;
update work.ec_currency set  exchange_rate_euro = 1.70974263 where code  = 'NZD' ;
update work.ec_currency set  exchange_rate_euro = 0.4284957949 where code  = 'OMR' ;
update work.ec_currency set  exchange_rate_euro = 1.112726748 where code  = 'PAB' ;
update work.ec_currency set  exchange_rate_euro = 3.73763774 where code  = 'PEN' ;
update work.ec_currency set  exchange_rate_euro = 3.760565643 where code  = 'PGK' ;
update work.ec_currency set  exchange_rate_euro = 58.05990582 where code  = 'PHP' ;
update work.ec_currency set  exchange_rate_euro = 167.8077638 where code  = 'PKR' ;
update work.ec_currency set  exchange_rate_euro = 4.28968822 where code  = 'PLN' ;
update work.ec_currency set  exchange_rate_euro = 7020.724239 where code  = 'PYG' ;
update work.ec_currency set  exchange_rate_euro = 4.052172953 where code  = 'QAR' ;
update work.ec_currency set  exchange_rate_euro = 4.752865251 where code  = 'RON' ;
update work.ec_currency set  exchange_rate_euro = 117.9340098 where code  = 'RSD' ;
update work.ec_currency set  exchange_rate_euro = 1 where code  = 'EUR' ;
update work.ec_currency set  exchange_rate_euro = 72.56124797 where code  = 'RUB' ;
update work.ec_currency set  exchange_rate_euro = 1004.487012 where code  = 'RWF' ;
update work.ec_currency set  exchange_rate_euro = 4.17376875 where code  = 'SAR' ;
update work.ec_currency set  exchange_rate_euro = 9.021628978 where code  = 'SBD' ;
update work.ec_currency set  exchange_rate_euro = 15.27877613 where code  = 'SCR' ;
update work.ec_currency set  exchange_rate_euro = 50.20097752 where code  = 'SDG' ;
update work.ec_currency set  exchange_rate_euro = 10.61014866 where code  = 'SEK' ;
update work.ec_currency set  exchange_rate_euro = 1.534833895 where code  = 'SGD' ;
update work.ec_currency set  exchange_rate_euro = 0.88249 where code  = 'SHP' ;
update work.ec_currency set  exchange_rate_euro = 9883.4844 where code  = 'SLL' ;
update work.ec_currency set  exchange_rate_euro = 648.3254125 where code  = 'SOS' ;
update work.ec_currency set  exchange_rate_euro = 8.30079129 where code  = 'SRD' ;
update work.ec_currency set  exchange_rate_euro = 24526.56 where code  = 'STD' ;
update work.ec_currency set  exchange_rate_euro = 573.225 where code  = 'SYP' ;
update work.ec_currency set  exchange_rate_euro = 16.28326315 where code  = 'SZL' ;
update work.ec_currency set  exchange_rate_euro = 35.3712989 where code  = 'THB' ;
update work.ec_currency set  exchange_rate_euro = 10.49814141 where code  = 'TJS' ;
update work.ec_currency set  exchange_rate_euro = 3.90664755 where code  = 'TMT' ;
update work.ec_currency set  exchange_rate_euro = 3.347139936 where code  = 'TND' ;
update work.ec_currency set  exchange_rate_euro = 2.557240288 where code  = 'TOP' ;
update work.ec_currency set  exchange_rate_euro = 6.544246799 where code  = 'TRY' ;
update work.ec_currency set  exchange_rate_euro = 7.536769007 where code  = 'TTD' ;
update work.ec_currency set  exchange_rate_euro = 1.61027 where code  = 'TVD[G]' ;
update work.ec_currency set  exchange_rate_euro = 35.14201987 where code  = 'TWD' ;
update work.ec_currency set  exchange_rate_euro = 2555.403829 where code  = 'TZS' ;
update work.ec_currency set  exchange_rate_euro = 29.99325874 where code  = 'UAH' ;
update work.ec_currency set  exchange_rate_euro = 4184.119696 where code  = 'UGX' ;
update work.ec_currency set  exchange_rate_euro = 39.09986565 where code  = 'UYU' ;
update work.ec_currency set  exchange_rate_euro = 9443.847425 where code  = 'UZS' ;
update work.ec_currency set  exchange_rate_euro = 11.1164 where code  = 'VEF' ;
update work.ec_currency set  exchange_rate_euro = 26064.01718 where code  = 'VND' ;
update work.ec_currency set  exchange_rate_euro = 2.96333 where code  = 'WST' ;
update work.ec_currency set  exchange_rate_euro = 655.4486445 where code  = 'XAF' ;
update work.ec_currency set  exchange_rate_euro = 3.007951662 where code  = 'XCD' ;
update work.ec_currency set  exchange_rate_euro = 654.44694 where code  = 'XOF' ;
update work.ec_currency set  exchange_rate_euro = 119.7036877 where code  = 'XPF' ;
update work.ec_currency set  exchange_rate_euro = 278.0842992 where code  = 'YER' ;
update work.ec_currency set  exchange_rate_euro = 16.38927687 where code  = 'ZAR' ;
update work.ec_currency set  exchange_rate_euro = 14.93986611 where code  = 'ZMW' ;
update work.ec_currency set  exchange_rate_euro = 4.087956064 where code  = 'AED' ;
update work.ec_currency set  exchange_rate_euro = 1.113005 where code  = 'USD' ;








