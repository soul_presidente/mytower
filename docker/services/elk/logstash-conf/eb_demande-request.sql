SELECT demande.eb_demande_num as demande_num, demande.x_ec_mode_transport as mode_transport, demande.x_ec_statut as statut,
    demande.date_creation as expedition_date_creation, demande_quote.price as demande_quote_price,

    origin_country.libelle as origin_country_libelle,
    dest_country.libelle as destination_country_libelle,

    CONCAT( origin_country.latitude,',', origin_country.longitude ) as coordinates_origin_country,
    CONCAT( dest_country.latitude,',', dest_country.longitude ) as coordinates_destination_country,
    demande.x_eb_compagnie as compagnie_num


FROM eb_demande  demande
    LEFT JOIN eb_demande_quote demande_quote ON demande_quote.x_eb_demande = demande.eb_demande_num
    LEFT JOIN eb_party origin_party ON origin_party.eb_party_num = demande.eb_party_origin
    LEFT JOIN eb_party dest_party ON dest_party.eb_party_num = demande.eb_party_dest
    LEFT JOIN ec_country origin_country ON origin_country.ec_country_num = origin_party.x_ec_country
    LEFT JOIN ec_country dest_country ON dest_country.ec_country_num = dest_party.x_ec_country
where  demande_quote.status=4;