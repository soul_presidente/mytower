# MyTower Development Docker files

This folder contains useful docker-compose files to help new (and old of course) developpers to have a full MyTower environment with one command.

You have the choice to install software on your machine or to use these docker files.

## How to start a docker-compose file

You have 2 options
- Just `cd` to the folder that contains a docker-compose.yml file and run `docker-compose up`
- Or you can from anywhere use `docker-compose up -f path/to/docker-compose.yml`

Additionals parameters and commands
- `docker-compose up -d` to run in detached mode
- `docker-compose down` to destroy all containers
- `docker-compose start` to start alrady created conainers
- `docker-compose stop` to stop containers without destroying them
- `docker-compose logs -f` display logs

Volume data is stored in project filesystem due to a docker limitation. As we can start a service from multiple compose files, names aren't same and in order to preserve data we can't store them in a standard docker volume.

## Which services available ?

# Kafka / Zookeeper

Required to run MyTower, previously we used dev server instance, but to improve start time and stability, you can use these containers instead.

# ElasticSearch / Kibana

Not stable for the moment, should be fixed

# pgAdmin 4

I really recommand to install a database manager on your system and not use this container. But in case of emergency of if you can't have an installation of pgAdmin or another tool, this will save you.

- Login : `mytower2018+pgadmindocker@gmail.com`
- Password : `postgres`
- Host connect to system host database : `host.docker.internal`

## Which development environment cominations available ?

- 0 Minimal
  - Kafka / Zookeeper
- 1 Features
  - Kafka / Zookeeper
  - ElasticSearch / Kibana (not stable for the moment)
- 2 Tools
  - Kafka / Zookeeper
  - pgAdmin 4
  - ElasticSearch / Kibana (not present for the moment, will be added here when stable)
- 3 Full (TODO)
  - Kafka / Zookeeper
  - pgAdmin 4
  - PostgreSQL
  - ElasticSearch / Kibana (not present for the moment, will be added here when stable)

## Where to find them ?

- Service one by one are in `docker/services`
- Development environment are in `docker/dev-env`
