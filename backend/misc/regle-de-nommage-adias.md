Règle de nommage des tables :

-   **tmp\_**xxx  
    préfixe pour les tables tempon. Il s’agit typiquement de tables renseignées
    via EDI en vues d’etre exploitée par des automates.

-   **eb\_**xxx  
    Table business. Il s’agit des tables métier.

-   **ec\_**xxx  
    table de référence.  
    Il faut faire la différence entre table de référence et table de
    paramétrage. Dans le contexte de mytower les data sont généralement
    paramétrées par société. Les tables accueillant les données de référence des
    sociétés sont des tables métier (eb_xxx)

-   **edi_map\_**xxx  
    table de mapping des utilisée pour mapper les donnée recu via EDI.  
    Ces tables de mapping servent à normaliser les données reçues via EDI des
    systèmes externes (transporteur, chargeur).

Règle de nommage des colonnes

-   Clé primaire = nom_table**\_num**  
    la clé primaire est composé du nom de la table suivi du suffixe « num »  
    exemple : eb_demande_num (clé primaire de la table eb_demande)

-   Clé étrangère : **x_nom_table_referencée[_suffix]**  
    les clés étrangère sont composé de 2 ou 3 partie. Ci-dessous la composition
    d’une clé étrangère :

    -   Première partie : préfixe « x_ »

    -   Deuxième partie : nom de la table

    -   Troisième partie (pas toujours obligatoire) : un suffixe précisant la
        signification du champ

Exemples de nomination de clé étrangère

**x_ec_country_destination**  
**x_ec_country_origin**  
**x_eb_demande**

Règles de nommage des index

Les index de type **btree** sont déclarés dans les entités JPA. Cependant pour
certain type d’index (**gin**, **hash**, **gist** …) qui ne sont actuellement
pas pris en compte par JPA. On les créerra manuellement via des scripts sql.

Le nom de l’index est composé de 3 parties obligatoires et 2 autres parties
optionnelles

-   Préfixe **idx\_**

-   Nom de la table

-   Nom de la colonne indexée

Il arrive qu’on crée plusieurs index sur la même colonne de la même table. Dans
ce cas on ajouter un suffixe indiquant l’algo d’indexation. Par défaut l’algo
d’indexation est btree.  
On n’a donc pas besoin d’ajouter en suffixe l’algo.  
  
On se permet également d’ajouter un suffixe pour préciser d’autres éléments sur
la nature de l’index ou les méthode d’indexation. C’est le cas où on a besoin de
créer 2 index sur le même champ pour une indexation des valeurs minuscules et
une indexation des valeurs majuscules.

Exemples

**idx_eb_demande_x_eb_party_dest** (index btree sur le champ x_eb_party_dest )

**idx_eb_demande_ref_transport_gin** (index gin sur le champ ref_transport )

**idx_eb_demande_ref_transport_gin_lower** (indexation gin du champ
ref_transport pour les valeur minuscule)
