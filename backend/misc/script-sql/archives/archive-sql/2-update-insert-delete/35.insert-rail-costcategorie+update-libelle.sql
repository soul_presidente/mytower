 -- Insert into work.eb_cost_categorie (  eb_cost_categorie_num, code,  libelle,  x_ec_mode_transport  )    values (  13, 'C13',  'Precarriage Costs',  5 );
 -- Insert into work.eb_cost_categorie (  eb_cost_categorie_num, code,  libelle,  x_ec_mode_transport  )    values (  14, 'C14',  'Freight costs',  5 );
 -- Insert into work.eb_cost_categorie (  eb_cost_categorie_num, code,  libelle,  x_ec_mode_transport  )    values (  15, 'C15',  'Costs at destination',  5 );
 -- Insert into work.eb_cost_categorie (  eb_cost_categorie_num, code,  libelle,  x_ec_mode_transport  )    values (  16, 'C16',  'Other costs',  5 );






-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  56, 'C13E56',  'Pick up',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C13' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  57, 'C13E57',  'Loading',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C13' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  58, 'C13E58',  'Export Customs formalities',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C13' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  59, 'C13E59',  'Documentation',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C13' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  60, 'C13E60',  'DGR',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C13' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  61, 'C13E61',  'Other',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C13' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  62, 'C14E62',  'Station to Station',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C14' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  63, 'C14E63',  'Surcharges',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C14' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  64, 'C14E64',  'Other',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C14' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  65, 'C15E65',  'Unloading',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C15' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  66, 'C15E66',  'Documentation',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C15' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  67, 'C15E67',  'Import Customs formalities',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C15' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  68, 'C15E68',  'Post-shipment, delivery',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C15' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  69, 'C15E69',  'Storage',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C15' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  70, 'C15E70',  'Demurrage / Detention',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C15' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  71, 'C15E71',  'Other',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C15' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  72, 'C16E72',  'Insurance',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C16' limit 1) );
-- Insert into work.eb_cost (  eb_cost_num, code,  libelle,  x_eb_cost_categorie  )    values (  73, 'C16E73',  'Other',  (select cc.eb_cost_categorie_num from work.eb_cost_categorie cc where cc.code='C16' limit 1) );







-- update work.eb_cost c set libelle = 'Pick up' where c.code = 'C5E20';
-- update work.eb_cost c set libelle = 'Export Customs formalities' where c.code = 'C5E21';
-- update work.eb_cost c set libelle = 'Documentation' where c.code = 'C5E22';
-- update work.eb_cost c set libelle = 'DGR' where c.code = 'C5E23';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C5E24';
-- update work.eb_cost c set libelle = 'Airfreight' where c.code = 'C6E25';
-- update work.eb_cost c set libelle = 'Fuel surcharge' where c.code = 'C6E26';
-- update work.eb_cost c set libelle = 'IRC surcharge' where c.code = 'C6E27';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C6E28';
-- update work.eb_cost c set libelle = 'Import Customs formalities' where c.code = 'C3E30';
-- update work.eb_cost c set libelle = 'Documentation' where c.code = 'C7E29';
-- update work.eb_cost c set libelle = 'Post-shipment, delivery' where c.code = 'C7E31';
-- update work.eb_cost c set libelle = 'Storage' where c.code = 'C7E33';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C7E34';
-- update work.eb_cost c set libelle = 'Insurance' where c.code = 'C8E35';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C8E36';
-- update work.eb_cost c set libelle = 'Rate' where c.code = 'C11E46';
-- update work.eb_cost c set libelle = 'Fuel surcharge' where c.code = 'C11E47';
-- update work.eb_cost c set libelle = 'DGR' where c.code = 'C11E48';
-- update work.eb_cost c set libelle = 'Oversize' where c.code = 'C11E49';
-- update work.eb_cost c set libelle = 'Overweight' where c.code = 'C11E50';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C11E51';
-- update work.eb_cost c set libelle = 'Import Customs formalities' where c.code = 'C12E52';
-- update work.eb_cost c set libelle = 'Export Customs formalities' where c.code = 'C12E53';
-- update work.eb_cost c set libelle = 'Insurance' where c.code = 'C12E54';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C12E55';
-- update work.eb_cost c set libelle = 'Pick up' where c.code = 'C13E56';
-- update work.eb_cost c set libelle = 'Loading' where c.code = 'C13E57';
-- update work.eb_cost c set libelle = 'Export Customs formalities' where c.code = 'C13E58';
-- update work.eb_cost c set libelle = 'Documentation' where c.code = 'C13E59';
-- update work.eb_cost c set libelle = 'DGR' where c.code = 'C13E60';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C13E61';
-- update work.eb_cost c set libelle = 'Station to Station' where c.code = 'C14E62';
-- update work.eb_cost c set libelle = 'Surcharges' where c.code = 'C14E63';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C14E64';
-- update work.eb_cost c set libelle = 'Unloading' where c.code = 'C15E65';
-- update work.eb_cost c set libelle = 'Documentation' where c.code = 'C15E66';
-- update work.eb_cost c set libelle = 'Import Customs formalities' where c.code = 'C15E67';
-- update work.eb_cost c set libelle = 'Post-shipment, delivery' where c.code = 'C15E68';
-- update work.eb_cost c set libelle = 'Storage' where c.code = 'C15E69';
-- update work.eb_cost c set libelle = 'Demurrage / Detention' where c.code = 'C15E70';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C15E71';
-- update work.eb_cost c set libelle = 'Insurance' where c.code = 'C16E72';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C16E73';
-- update work.eb_cost c set libelle = 'Import Customs formalities' where c.code = 'C10E41';
-- update work.eb_cost c set libelle = 'Export Customs formalities' where c.code = 'C10E42';
-- update work.eb_cost c set libelle = 'Insurance' where c.code = 'C10E43';
-- update work.eb_cost c set libelle = 'Other documentation' where c.code = 'C10E44';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C10E45';
-- update work.eb_cost c set libelle = 'Door to door' where c.code = 'C9E37';
-- update work.eb_cost c set libelle = 'Fuel surcharge' where c.code = 'C9E38';
-- update work.eb_cost c set libelle = 'DGR' where c.code = 'C9E39';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C9E40';
-- update work.eb_cost c set libelle = 'Pick up' where c.code = 'C1E1';
-- update work.eb_cost c set libelle = 'Loading' where c.code = 'C1E2';
-- update work.eb_cost c set libelle = 'Export Customs formalities' where c.code = 'C1E3';
-- update work.eb_cost c set libelle = 'Documentation' where c.code = 'C1E4';
-- update work.eb_cost c set libelle = 'DGR' where c.code = 'C1E5';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C1E6';
-- update work.eb_cost c set libelle = 'Port to port' where c.code = 'C2E7';
-- update work.eb_cost c set libelle = 'Surcharges' where c.code = 'C2E8';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C2E9';
-- update work.eb_cost c set libelle = 'Unloading' where c.code = 'C3E10';
-- update work.eb_cost c set libelle = 'Documentation' where c.code = 'C3E11';
-- update work.eb_cost c set libelle = 'Import Customs formalities' where c.code = 'C3E12';
-- update work.eb_cost c set libelle = 'Post-shipment, delivery' where c.code = 'C3E13';
-- update work.eb_cost c set libelle = 'Storage' where c.code = 'C3E15';
-- update work.eb_cost c set libelle = 'Demurrage / Detention' where c.code = 'C3E16';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C3E17';
-- update work.eb_cost c set libelle = 'Insurance' where c.code = 'C4E18';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C4E19';






-- update work.eb_cost c set libelle = 'Pick up' where c.code = 'C5E20';
-- update work.eb_cost c set libelle = 'Export Customs formalities' where c.code = 'C5E21';
-- update work.eb_cost c set libelle = 'Documentation' where c.code = 'C5E22';
-- update work.eb_cost c set libelle = 'DGR' where c.code = 'C5E23';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C5E24';
-- update work.eb_cost c set libelle = 'Airfreight' where c.code = 'C6E25';
-- update work.eb_cost c set libelle = 'Fuel surcharge' where c.code = 'C6E26';
-- update work.eb_cost c set libelle = 'IRC surcharge' where c.code = 'C6E27';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C6E28';
-- update work.eb_cost c set libelle = 'Import Customs formalities' where c.code = 'C3E30';
-- update work.eb_cost c set libelle = 'Documentation' where c.code = 'C7E29';
-- update work.eb_cost c set libelle = 'Post-shipment, delivery' where c.code = 'C7E31';
-- update work.eb_cost c set libelle = 'Storage' where c.code = 'C7E33';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C7E34';
-- update work.eb_cost c set libelle = 'Insurance' where c.code = 'C8E35';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C8E36';
-- update work.eb_cost c set libelle = 'Rate' where c.code = 'C11E46';
-- update work.eb_cost c set libelle = 'Fuel surcharge' where c.code = 'C11E47';
-- update work.eb_cost c set libelle = 'DGR' where c.code = 'C11E48';
-- update work.eb_cost c set libelle = 'Oversize' where c.code = 'C11E49';
-- update work.eb_cost c set libelle = 'Overweight' where c.code = 'C11E50';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C11E51';
-- update work.eb_cost c set libelle = 'Import Customs formalities' where c.code = 'C12E52';
-- update work.eb_cost c set libelle = 'Export Customs formalities' where c.code = 'C12E53';
-- update work.eb_cost c set libelle = 'Insurance' where c.code = 'C12E54';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C12E55';
-- update work.eb_cost c set libelle = 'Pick up' where c.code = 'C13E56';
-- update work.eb_cost c set libelle = 'Loading' where c.code = 'C13E57';
-- update work.eb_cost c set libelle = 'Export Customs formalities' where c.code = 'C13E58';
-- update work.eb_cost c set libelle = 'Documentation' where c.code = 'C13E59';
-- update work.eb_cost c set libelle = 'DGR' where c.code = 'C13E60';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C13E61';
-- update work.eb_cost c set libelle = 'Station to Station' where c.code = 'C14E62';
-- update work.eb_cost c set libelle = 'Surcharges' where c.code = 'C14E63';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C14E64';
-- update work.eb_cost c set libelle = 'Unloading' where c.code = 'C15E65';
-- update work.eb_cost c set libelle = 'Documentation' where c.code = 'C15E66';
-- update work.eb_cost c set libelle = 'Import Customs formalities' where c.code = 'C15E67';
-- update work.eb_cost c set libelle = 'Post-shipment, delivery' where c.code = 'C15E68';
-- update work.eb_cost c set libelle = 'Storage' where c.code = 'C15E69';
-- update work.eb_cost c set libelle = 'Demurrage / Detention' where c.code = 'C15E70';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C15E71';
-- update work.eb_cost c set libelle = 'Insurance' where c.code = 'C16E72';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C16E73';
-- update work.eb_cost c set libelle = 'Import Customs formalities' where c.code = 'C10E41';
-- update work.eb_cost c set libelle = 'Export Customs formalities' where c.code = 'C10E42';
-- update work.eb_cost c set libelle = 'Insurance' where c.code = 'C10E43';
-- update work.eb_cost c set libelle = 'Other documentation' where c.code = 'C10E44';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C10E45';
-- update work.eb_cost c set libelle = 'Door to door' where c.code = 'C9E37';
-- update work.eb_cost c set libelle = 'Fuel surcharge' where c.code = 'C9E38';
-- update work.eb_cost c set libelle = 'DGR' where c.code = 'C9E39';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C9E40';
-- update work.eb_cost c set libelle = 'Pick up' where c.code = 'C1E1';
-- update work.eb_cost c set libelle = 'Loading' where c.code = 'C1E2';
-- update work.eb_cost c set libelle = 'Export Customs formalities' where c.code = 'C1E3';
-- update work.eb_cost c set libelle = 'Documentation' where c.code = 'C1E4';
-- update work.eb_cost c set libelle = 'DGR' where c.code = 'C1E5';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C1E6';
-- update work.eb_cost c set libelle = 'Port to port' where c.code = 'C2E7';
-- update work.eb_cost c set libelle = 'Surcharges' where c.code = 'C2E8';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C2E9';
-- update work.eb_cost c set libelle = 'Unloading' where c.code = 'C3E10';
-- update work.eb_cost c set libelle = 'Documentation' where c.code = 'C3E11';
-- update work.eb_cost c set libelle = 'Import Customs formalities' where c.code = 'C3E12';
-- update work.eb_cost c set libelle = 'Post-shipment, delivery' where c.code = 'C3E13';
-- update work.eb_cost c set libelle = 'Storage' where c.code = 'C3E15';
-- update work.eb_cost c set libelle = 'Demurrage / Detention' where c.code = 'C3E16';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C3E17';
-- update work.eb_cost c set libelle = 'Insurance' where c.code = 'C4E18';
-- update work.eb_cost c set libelle = 'Other' where c.code = 'C4E19';











-- update work.eb_cost_categorie cc set libelle = 'Precarriage Costs' where cc.code = 'C5';
-- update work.eb_cost_categorie cc set libelle = 'Freight costs' where cc.code = 'C6';
-- update work.eb_cost_categorie cc set libelle = 'Costs at destination' where cc.code = 'C7';
-- update work.eb_cost_categorie cc set libelle = 'Other costs' where cc.code = 'C8';
-- update work.eb_cost_categorie cc set libelle = 'Freight costs' where cc.code = 'C11';
-- update work.eb_cost_categorie cc set libelle = 'Other costs' where cc.code = 'C12';
-- update work.eb_cost_categorie cc set libelle = 'Precarriage Costs' where cc.code = 'C13';
-- update work.eb_cost_categorie cc set libelle = 'Freight costs' where cc.code = 'C14';
-- update work.eb_cost_categorie cc set libelle = 'Costs at destination' where cc.code = 'C15';
-- update work.eb_cost_categorie cc set libelle = 'Other costs' where cc.code = 'C16';
-- update work.eb_cost_categorie cc set libelle = 'Other costs' where cc.code = 'C10';
-- update work.eb_cost_categorie cc set libelle = 'Freight costs' where cc.code = 'C9';
-- update work.eb_cost_categorie cc set libelle = 'Precarriage Costs' where cc.code = 'C1';
-- update work.eb_cost_categorie cc set libelle = 'Freight costs' where cc.code = 'C2';
-- update work.eb_cost_categorie cc set libelle = 'Costs at destination' where cc.code = 'C3';
-- update work.eb_cost_categorie cc set libelle = 'Other costs' where cc.code = 'C4';

