/* update demande code_psl_courant*/
/*
UPDATE WORK.eb_demande d 
SET code_psl_courant = subquery.code_psl_courant 
FROM
	(
SELECT DISTINCT ON
	( tt.x_eb_demande ) tt.x_eb_demande,
	pslapp.code_psl AS code_psl_courant 
FROM
	WORK.eb_tt_tracing tt
	LEFT JOIN WORK.eb_tt_psl_app pslapp ON ( pslapp.x_eb_tt_tracing = tt.eb_tt_tracing_num ) 
WHERE
	pslapp.date_actuelle IS NOT NULL 
ORDER BY
	tt.x_eb_demande DESC,
	pslapp.code_psl DESC,
	pslapp.date_actuelle DESC 
	) AS subquery 
WHERE
	d.eb_demande_num = subquery.x_eb_demande;
*/	
	
	
	
	
	
/* update tracing code_psl_courant*/
/*
UPDATE WORK.eb_tt_tracing ttr 
SET code_psl_courant = (SELECT
	pslapp.code_psl
FROM
	WORK.eb_tt_psl_app pslapp 
WHERE pslapp.x_eb_tt_tracing = ttr.eb_tt_tracing_num and pslapp.date_actuelle is not null
ORDER BY
	pslapp.code_psl desc
limit 1);

UPDATE WORK.eb_tt_tracing ttr 
SET code_psl_courant = (SELECT
	pslapp.code_psl
FROM
	WORK.eb_tt_psl_app pslapp 
WHERE pslapp.x_eb_tt_tracing = ttr.eb_tt_tracing_num 
ORDER BY
	pslapp.code_psl asc
limit 1) 
where ttr.code_psl_courant is null
*/
*/