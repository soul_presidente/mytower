/* update   invoice */
update work.eb_invoice	tt  set 
x_ec_incoterm = (select d.x_ec_incoterm from work.eb_demande d where d.eb_demande_num = tt.x_eb_demande limit 1),  
x_ec_incoterm_libelle= (select i.libelle from work.eb_incoterm  i left join work.eb_demande d on (d.x_ec_incoterm =eb_incoterm_num )   where d.eb_demande_num = tt.x_eb_demande limit 1),  
 libelle_origin_city = (select p.city from work.eb_demande d left join work.eb_party p on (p.eb_party_num = d.eb_party_origin) where d.eb_demande_num = tt.x_eb_demande limit 1), 
 libelle_dest_city = (select p.city from work.eb_demande d left join work.eb_party p on (p.eb_party_num = d.eb_party_dest) where d.eb_demande_num = tt.x_eb_demande limit 1);

 /* update   tracing */
update work.eb_tt_tracing	tt set x_ec_incoterm = (select d.x_ec_incoterm from work.eb_demande d where d.eb_demande_num = tt.x_eb_demande limit 1),
x_ec_incoterm_libelle= (select i.libelle from work.eb_incoterm  i left join work.eb_demande d on (d.x_ec_incoterm =eb_incoterm_num ) where d.eb_demande_num = tt.x_eb_demande limit 1),
libelle_origin_city=(select p.city from work.eb_demande d left join work.eb_party p on (p.eb_party_num = d.eb_party_origin) where d.eb_demande_num = tt.x_eb_demande limit 1),
 libelle_dest_city = (select p.city from work.eb_demande d left join work.eb_party p on (p.eb_party_num = d.eb_party_dest) where d.eb_demande_num = tt.x_eb_demande limit 1);
 
 
 
/* update   demande */
update work.eb_demande 	d  
set x_ec_incoterm_libelle= (select i.libelle from work.eb_incoterm  i   where d.x_ec_incoterm =eb_incoterm_num   limit 1),
libelle_origin_city=(select p.city from work.eb_party p   where p.eb_party_num = d.eb_party_origin  limit 1),
 libelle_dest_city = (select p.city from work.eb_party  p   where p.eb_party_num = d.eb_party_dest limit 1);
 
 
  
/* update   quality */