/*maj ct & labels tracing*/
--update work.eb_tt_tracing trc set x_eb_user_ct = (select d.x_eb_user_ct from work.eb_demande d where d.eb_demande_num = trc.x_eb_demande), x_eb_compagnie_ct = (select d.x_eb_compagnie_ct from work.eb_demande d where d.eb_demande_num = trc.x_eb_demande), x_eb_etablissement_ct = (select d.x_eb_etablissement_ct from work.eb_demande d where d.eb_demande_num = trc.x_eb_demande), list_labels = (select d.list_labels from work.eb_demande d where d.eb_demande_num = trc.x_eb_demande);

/*maj user events*/
--update work.eb_tt_event evt set x_eb_user = (select trc.x_eb_transporteur from work.eb_tt_tracing trc where trc.eb_tt_tracing_num = evt.x_eb_tt_tracing_eb_tt_tracing_num), x_eb_etablissement = (select trc.x_eb_etablissement_transporteur from work.eb_tt_tracing trc where trc.eb_tt_tracing_num = evt.x_eb_tt_tracing_eb_tt_tracing_num), x_eb_compagnie = (select trc.x_eb_company_transporteur from work.eb_tt_tracing trc where trc.eb_tt_tracing_num = evt.x_eb_tt_tracing_eb_tt_tracing_num);

