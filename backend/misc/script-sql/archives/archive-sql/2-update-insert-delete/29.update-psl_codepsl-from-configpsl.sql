/*

with psl as (
	(select 'PIC'::text as acode, 1::int as code
	union
	select 'DEP'::text as acode, 2::int as code
	union
	select 'ARR'::text as acode, 3::int as code
	union
	select 'CUS'::text as acode, 4::int as code
	union
	select 'DEL'::text as acode, 5::int as code)
	order by code
) 

, pslapp as (
		select 
		ttpslapp.eb_tt_psl_app_num, ttpslapp.code_psl, ttpslapp.date_creation, ttpslapp.x_eb_tt_tracing, (select acode from psl where psl.code = ttpslapp.code_psl) as acode
		from work.eb_tt_psl_app ttpslapp order  by ttpslapp.eb_tt_psl_app_num asc
	)
	
, confpsl as (	
	select date_creation, x_eb_tt_tracing, string_agg ( eb_tt_psl_app_num::text , '|' ) as pslnums, string_agg ( acode , '|' ) as splitconfig, (select config_psl from work.eb_tt_tracing tt where tt.eb_tt_tracing_num = pt.	x_eb_tt_tracing) as orgconfig from pslapp pt group by x_eb_tt_tracing, date_creation
	), concateddata as (
	
	select * from confpsl where splitconfig <> orgconfig order by date_creation desc
	
	), splitdata as (
	select date_creation, unnest(string_to_array(pslnums, '|')) as pslnum, unnest(string_to_array(splitconfig, '|')) as was, unnest(string_to_array(orgconfig, '|')) as should from concateddata
	) , converteddata as (
	select sd.pslnum::int, (select code from psl where sd.should = psl.acode) as code_psl from splitdata sd
	
)


select * from confpsl where splitconfig <> orgconfig order by date_creation desc

*/

/*
	select sd.pslnum::int, (select code from psl where sd.was = psl.acode) as code_psl from splitdata sd order by pslnum::int
*/

/*
select pslapp.eb_tt_psl_app_num, pslapp.code_psl from work.eb_tt_psl_app pslapp where pslapp.eb_tt_psl_app_num in (select sd.pslnum from converteddata sd)
*/


/*
update work.eb_tt_psl_app pslapp set code_psl = (select code_psl from converteddata sd where sd.pslnum = pslapp.eb_tt_psl_app_num) where pslapp.eb_tt_psl_app_num in (select sd.pslnum from converteddata sd);


update work.eb_tt_tracing tt set code_psl_courant = (select pslapp.code_psl from work.eb_tt_psl_app pslapp where tt.eb_tt_tracing_num = pslapp.x_eb_tt_tracing and pslapp.date_actuelle is not null order by pslapp.code_psl desc limit 1);


update work.eb_tt_psl_app pslapp set validated = true where pslapp.date_actuelle is not null;

 update work.eb_demande d set x_ec_statut =  	( 		with selecteddata as ( 			select psl.code_psl, max(psl.code_psl) as maxcodepsl , case when psl.code_psl is not null then 12 else 11 end as pslcourant from work.eb_tt_psl_app psl left join work.eb_tt_tracing tt on (tt.			eb_tt_tracing_num = psl.x_eb_tt_tracing) where psl.date_actuelle is not null and tt.x_eb_demande = d.eb_demande_num group by psl.code_psl order by psl.code_psl desc limit 1    )  		select case when (select pslcourant from selecteddata) is not null and (select code_psl from selecteddata) = (select maxcodepsl from selecteddata) then 13 when (select pslcourant from selecteddata) is not null then 12 else 11 end  )   WHERE d.x_ec_statut >= 11

 
 */
