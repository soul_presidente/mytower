WITH select_flatcustomfields AS (
	SELECT
		d.eb_demande_num,
		d.custom_fields,
		jsonb_array_elements ( d.custom_fields :: jsonb ) AS flatcustomfields 
	FROM
		WORK.eb_demande d 
	WHERE
		d.custom_fields IS NOT NULL
		
), extractquery AS (
	SELECT
		sub.eb_demande_num,
		( sub.flatcustomfields ) -> 'name' AS NAME,
		( sub.flatcustomfields ) -> 'value' AS 
	VALUE
		
	FROM
		select_flatcustomfields sub 
		
), converted AS (
		SELECT
			q.eb_demande_num,
			REPLACE ( q.NAME :: TEXT, '"', '' ) AS NAME,
			REPLACE ( q.VALUE :: TEXT, '"', '' ) AS 
		VALUE
			
		FROM
			extractquery q 
			
), customfield as (
		SELECT
				*
		FROM
		converted cv where cv.name = 'field2'
) 




, psl as (
	(select 'PIC'::text as acode, 1::int as code
	union
	select 'DEP'::text as acode, 2::int as code
	union
	select 'ARR'::text as acode, 3::int as code
	union
	select 'CUS'::text as acode, 4::int as code
	union
	select 'DEL'::text as acode, 5::int as code)
	order by code
) 

, pslapp as (
		select 
		ttpslapp.eb_tt_psl_app_num, ttpslapp.code_psl, ttpslapp.date_actuelle, ttpslapp.date_estimee, ttpslapp.date_negotiation, ttpslapp.date_creation, ttpslapp.x_eb_tt_tracing, (select acode from psl where psl.code = ttpslapp.code_psl) as acode
		from work.eb_tt_psl_app ttpslapp order  by ttpslapp.eb_tt_psl_app_num asc
	)
	
	, grouppsl as (	
	select 	
		date_creation, 
		x_eb_tt_tracing, 
		'{"x_eb_tt_tracing":' || x_eb_tt_tracing::text || ',' || string_agg ( '"' || lower(acode) || '":"' || cast(date_actuelle as TEXT)||'"' , ',' ) || '}' as date_actuelle, 
		'{"x_eb_tt_tracing":' || x_eb_tt_tracing::text || ',' || string_agg ( '"' || lower(acode) || '":"' || cast(date_estimee as TEXT)||'"' , ',' ) || '}' as date_estimee, 
		'{"x_eb_tt_tracing":' || x_eb_tt_tracing::text || ',' || string_agg ( '"' || lower(acode) || '":"' || cast(date_negotiation as TEXT)||'"' , ',' ) || '}' as date_negotiation,
		(select config_psl  from  work.eb_tt_tracing tt  where tt.eb_tt_tracing_num = pt.x_eb_tt_tracing) as orgconfig 
	from pslapp pt 
	group by x_eb_tt_tracing, date_creation
)

, splitedpsldates as (
SELECT 
	t.x_eb_tt_tracing, 
	
	da.pic as "Pickup Actual date",
	da.dep as "Departure Actual date",
	da.arr as "Arrival Actual date",
	da.cus as "Customs Actual date",
	da.del as "Delivery Actual date",
	
	de.pic as "Pickup Estimated date",
	de.dep as "Departure Estimated date",
	de.arr as "Arrival Estimated date",
	de.cus as "Customs Estimated date",
	de.del as "Delivery Estimated date",
	
	dn.pic as "Pickup Negotiated date",
	dn.dep as "Departure Negotiated date",
	dn.arr as "Arrival Negotiated date",
	dn.cus as "Customs Negotiated date",
	dn.del as "Delivery Negotiated date"
	
FROM   
	grouppsl t
  left join json_to_record(cast(t.date_actuelle AS json)) da(x_eb_tt_tracing int, PIC text, DEP text, ARR text, CUS text, DEL text) on (da.x_eb_tt_tracing = t.x_eb_tt_tracing)
	left join json_to_record(cast(t.date_estimee AS json)) de(x_eb_tt_tracing int, PIC text, DEP text, ARR text, CUS text, DEL text) on (de.x_eb_tt_tracing = t.x_eb_tt_tracing)
	left join json_to_record(cast(t.date_negotiation AS json)) dn(x_eb_tt_tracing int, PIC text, DEP text, ARR text, CUS text, DEL text) on (dn.x_eb_tt_tracing = t.x_eb_tt_tracing)
)

		
		

SELECT
	d.ref_transport,
	tt.customer_reference,
	d.units_reference,
	po.city as "Ville d'origine",
	cno.libelle as "Pays d'origine",
	pd.city as "Ville de destination",
	cnd.libelle as "Pays de destination",
	cf.value as "Vendor PO",
	CASE 
		WHEN d.x_ec_mode_transport = 1 THEN 'Air' 
		WHEN d.x_ec_mode_transport = 2 THEN 'Sea' 
		WHEN d.x_ec_mode_transport = 3 THEN 'Road' 
		WHEN d.x_ec_mode_transport = 4 THEN 'Integrator' ELSE'' || d.x_ec_mode_transport 
	END AS "mode transport",
	(select ic.libelle from work.eb_incoterm ic where ic.eb_incoterm_num = d.x_ec_incoterm limit 1) as "Incoterm",
	
	spd.*
	
FROM
	WORK.eb_tt_tracing tt
	LEFT JOIN WORK.eb_demande d ON ( d.eb_demande_num = tt.x_eb_demande )
	LEFT join work.eb_party po on (d.eb_party_origin = po.eb_party_num)
	left join work.eb_party pd on (d.eb_party_dest = pd.eb_party_num)
	left join work.ec_country cno on (cno.ec_country_num = po.x_ec_country)
	left join work.ec_country cnd on (cnd.ec_country_num = pd.x_ec_country)
	left join customfield cf on (cf.eb_demande_num = tt.x_eb_demande)
	left join splitedpsldates spd on (spd.x_eb_tt_tracing = tt.eb_tt_tracing_num);