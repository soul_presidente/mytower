SELECT
		date_creation,
		designation,
		is_deleted,
		is_editable,
		list_etablissement,
		list_mode_transport,
		list_psl,
		x_ec_incoterm_libelle,
		cc.nom

FROM
		WORK.eb_tt_schema_psl sc
		left join work.eb_compagnie cc on cc.eb_compagnie_num = sc.x_eb_compagnie;
		
		