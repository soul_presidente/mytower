
		
select 
		CASE
			WHEN st.datatable_comp_id = 1 THEN 'Pricing'
			WHEN st.datatable_comp_id = 2 THEN 'Booking'
			WHEN st.datatable_comp_id = 3 THEN 'Quality management'
			WHEN st.datatable_comp_id = 5 THEN 'Track & Trace'
			WHEN st.datatable_comp_id = 9 THEN 'Custom'
			WHEN st.datatable_comp_id = 11 THEN 'Order'
			WHEN st.datatable_comp_id = 12 THEN 'Delivery' 
			ELSE st.datatable_comp_id::text
		END "module",
		st.date_maj, 
		st.state, 
		u.email
from work.eb_datatable_state st 
left join work.eb_user u on u.eb_user_num = st.x_eb_user_num;

	