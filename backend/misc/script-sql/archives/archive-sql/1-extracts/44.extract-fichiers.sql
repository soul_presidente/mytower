SELECT
	attachment_date "Date d'upload",
	'https://tcd.mytower.fr//' || chemin AS file,
	id_categorie as "Numero categorie",
	case 
		when id_categorie = 1 then 'Supplier  invoice'
		when id_categorie = 2 then 'Commercial invoice'
		when id_categorie = 3 then 'Customs'
		when id_categorie = 4 then 'Transport document'
		when id_categorie = 5 then 'Other'
		when id_categorie = 6 then 'POD'
	end AS categorie,
	x_eb_demande as "Numero de dossier",
	case 
		when module = 1 then 'Pricing'
		when module = 2 then 'Transport management'
		when module = 3 then 'Quality management'
		when module = 4 then 'Compliance Matrix'
		when module = 5 then 'Track & Trace'
		else 'Pricing'
	end as "Module"
FROM
	WORK.eb_demande_fichiers_joint ff
ORDER BY
	x_eb_demande;