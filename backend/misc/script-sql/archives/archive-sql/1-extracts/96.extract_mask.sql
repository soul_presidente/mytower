SELECT
CASE
	
WHEN
	sf.eb_comp_num = 2 THEN
	'TRANSPORT_MANAGEMENT' 
WHEN sf.eb_comp_num = 4 THEN
'COMPLIANCE_MATRIX' 
WHEN sf.eb_comp_num = 3 THEN
'QUALITY_MANAGEMENT' 
WHEN sf.eb_comp_num = 8 THEN
'FREIGHT_ANALYTICS' 
WHEN sf.eb_comp_num = 12 THEN
'SETTINGS_ZONE' 
WHEN sf.eb_comp_num = 13 THEN
'SETTINGS_TRANCHE' 
WHEN sf.eb_comp_num = 14 THEN
'SETTINGS_GRILLE' 
WHEN sf.eb_comp_num = 15 THEN
'SETTINGS_TRANSPORT_PLAN' 
WHEN sf.eb_comp_num = 17 THEN
'SETTINGS_ENTREPOT' 
WHEN sf.eb_comp_num = 16 THEN
'USER_MANAGEMENT' 
WHEN sf.eb_comp_num = 2 THEN
'BOOKING' 
WHEN sf.eb_comp_num = 1 THEN
'PRICING' 
WHEN sf.eb_comp_num = 3 THEN
'QUALITY' 
WHEN sf.eb_comp_num = 4 THEN
'FREIGHT_AUDIT' 
WHEN sf.eb_comp_num = 5 THEN
'TRACK' 
WHEN sf.eb_comp_num = 9 THEN
'CUSTOM' 
WHEN sf.eb_comp_num = 11 THEN
'ORDER' 
WHEN sf.eb_comp_num = 12 THEN
'DELIVERY' 
WHEN sf.eb_comp_num = 18 THEN
'RECEIPT_SCHEDULING' 
WHEN sf.eb_comp_num = 19 THEN
'SHIP_TO_MATRIX' 
END AS col_1,
	M.libelle AS col_2,
	form_name AS col_3,
	form_values AS col_4,
	u.email AS col_5 
FROM
	WORK.eb_saved_form sf
	LEFT JOIN WORK.ec_module M ON M.ec_module_num = sf.eb_module_num
LEFT JOIN WORK.eb_user u ON u.eb_user_num = sf.x_eb_user