SELECT
	uh.email "email host",
	eh.nom "etablissement host",
	ug.email "email guest",
	eg.nom "etablissement guest",
	case 
		when r.type_relation = 0 then 'Transporteur'
		when r.type_relation = 1 then 'Broker'
		when r.type_relation = 2 then 'Transporteur / broker'
		when r.type_relation = 3 then 'CT'
		else NULL
	end "type relation",
	case 
		when r.etat = 0 then 'En Cours'
		when r.etat = 1 then 'Actif'
		when r.etat = 2 then 'Ignoré'
		when r.etat = 3 then 'Annulé'
		when r.etat = 4 then 'Blacklist'
	end "statut"
FROM
	WORK.eb_relation r
	LEFT JOIN WORK.eb_user uh ON r.x_host = uh.eb_user_num
	LEFT JOIN WORK.eb_etablissement eh ON r.x_eb_etablissement_host = eh.eb_etablissement_num
	LEFT JOIN WORK.eb_user ug ON r.x_guest = ug.eb_user_num
	LEFT JOIN WORK.eb_etablissement eg ON r.x_eb_etablissement = eg.eb_etablissement_num 
ORDER BY
	uh.email,
	r.etat,
	r.type_relation;