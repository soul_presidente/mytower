SELECT
	et.nom "etablissement",
	cc.libelle "categorie",
	ll.libelle "label"
FROM
	WORK.eb_label ll
	LEFT JOIN WORK.eb_categorie cc ON cc.eb_categorie_num = ll.x_eb_categorie
	LEFT JOIN WORK.eb_etablissement et ON et.eb_etablissement_num = cc.x_eb_etablissement 
ORDER BY
	et.nom,
	cc.libelle,
	ll.libelle;