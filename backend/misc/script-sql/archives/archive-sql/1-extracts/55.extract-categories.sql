SELECT
	et.nom "etablissement",
	cc.libelle
FROM
WORK
	.eb_categorie cc
	LEFT JOIN WORK.eb_etablissement et ON et.eb_etablissement_num = cc.x_eb_etablissement
order by et.nom, cc.libelle;