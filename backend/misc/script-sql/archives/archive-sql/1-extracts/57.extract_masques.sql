SELECT
	case 
		when sf.eb_comp_num = 2 then 'TRANSPORT_MANAGEMENT'
		when sf.eb_comp_num = 4 then 'COMPLIANCE_MATRIX'
		when sf.eb_comp_num = 3 then 'QUALITY_MANAGEMENT'
		when sf.eb_comp_num = 8 then 'FREIGHT_ANALYTICS'
		when sf.eb_comp_num = 12 then 'SETTINGS_ZONE'
		when sf.eb_comp_num = 13 then 'SETTINGS_TRANCHE'
		when sf.eb_comp_num = 14 then 'SETTINGS_GRILLE'
		when sf.eb_comp_num = 15 then 'SETTINGS_TRANSPORT_PLAN'
		when sf.eb_comp_num = 17 then 'SETTINGS_ENTREPOT'
		when sf.eb_comp_num = 16 then 'USER_MANAGEMENT'
		when sf.eb_comp_num = 2 then 'BOOKING'
		when sf.eb_comp_num = 1 then 'PRICING'
		when sf.eb_comp_num = 3 then 'QUALITY'
		when sf.eb_comp_num = 4 then 'FREIGHT_AUDIT'
		when sf.eb_comp_num = 5 then 'TRACK'
		when sf.eb_comp_num = 9 then 'CUSTOM'
		when sf.eb_comp_num = 11 then 'ORDER'
		when sf.eb_comp_num = 12 then 'DELIVERY'
		when sf.eb_comp_num = 18 then 'RECEIPT_SCHEDULING'
		when sf.eb_comp_num = 19 then 'SHIP_TO_MATRIX'
	end "Composant",
	m.libelle "Module",
	form_name,
	form_values,
	u.email 
FROM
	WORK.eb_saved_form sf
	LEFT JOIN WORK.ec_module M ON M.ec_module_num = sf.eb_module_num
	LEFT JOIN WORK.eb_user u ON u.eb_user_num = sf.x_eb_user