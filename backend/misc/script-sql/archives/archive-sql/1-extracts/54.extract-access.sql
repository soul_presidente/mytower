SELECT
	u.email,
	mm.libelle "module",
	case 
		when um.access_right = 2 then 'Non visible'
		when um.access_right = 1 then 'Visible'
		when um.access_right = 0 then 'Contribution'
	end "access right"
FROM
	WORK.eb_user_module um
	LEFT JOIN WORK.ec_module mm ON mm.ec_module_num = um.x_ec_module
	LEFT JOIN WORK.eb_user u ON u.eb_user_num = um.x_eb_user
order by email, module, um.access_right;