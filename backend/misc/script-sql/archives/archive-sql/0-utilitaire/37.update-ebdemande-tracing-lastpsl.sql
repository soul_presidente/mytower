/*
UPDATE work.eb_tt_tracing ttr
SET code_last_psl=subquery.code_last_psl,
    libelle_last_psl=subquery.libelle_last_psl,
    date_last_psl=subquery.date_last_psl
FROM ( SELECT DISTINCT ON 	( pslapp.x_eb_tt_tracing ) pslapp.x_eb_tt_tracing, 	pslapp.code_psl AS code_last_psl, 	( CASE 	 	WHEN pslapp.code_psl = 1 THEN 	'Pickup'  	WHEN pslapp.code_psl = 2 THEN 	'Departure'  	WHEN pslapp.code_psl = 3 THEN 	'Arrival'  	WHEN pslapp.code_psl = 4 THEN 	'Customs'  	WHEN pslapp.code_psl = 5 THEN 	'Delivery' ELSE NULL  END  	) AS libelle_last_psl, 	pslapp.date_actuelle AS date_last_psl  FROM 	WORK.eb_tt_psl_app pslapp  ORDER BY 	pslapp.x_eb_tt_tracing DESC, 	pslapp.code_psl DESC, pslapp.date_actuelle DESC) AS subquery
WHERE ttr.eb_tt_tracing_num=subquery.x_eb_tt_tracing;


UPDATE work.eb_demande d
SET code_last_psl=subquery.code_last_psl,
    libelle_last_psl=subquery.libelle_last_psl,
    date_last_psl=subquery.date_last_psl
FROM (SELECT distinct on (tt.x_eb_demande) tt.x_eb_demande, pslapp.code_psl AS code_last_psl, 	( CASE 	 	WHEN pslapp.code_psl = 1 THEN 	'Pickup'  	WHEN pslapp.code_psl = 2 THEN 	'Departure'  	WHEN pslapp.code_psl = 3 THEN 	'Arrival'  	WHEN pslapp.code_psl = 4 THEN 	'Customs'  	WHEN pslapp.code_psl = 5 THEN 	'Delivery' ELSE NULL  END  	) AS libelle_last_psl, 	pslapp.date_actuelle AS date_last_psl  FROM WORK.eb_tt_tracing tt left join work.eb_tt_psl_app pslapp on (pslapp.x_eb_tt_tracing = tt.eb_tt_tracing_num)  ORDER BY 	tt.x_eb_demande desc, 	pslapp.code_psl DESC, pslapp.date_actuelle DESC ) AS subquery
WHERE d.eb_demande_num=subquery.x_eb_demande;


with psl as ( 	(select 'PIC'::text as acode, 1::int as code 	union 	select 'DEP'::text as acode, 2::int as code 	union 	select 'ARR'::text as acode, 3::int as code 	union 	select 'CUS'::text as acode, 4::int as code 	union 	select 'DEL'::text as acode, 5::int as code) 	order by code )   , confpslarr as ( 		select eb_demande_num, string_to_array(config_psl, '|') as arr from work.eb_demande	 )  , conflastpsl as ( 	select eb_demande_num, arr[array_upper(arr, 1)] as lastpsl from confpslarr )  update work.eb_demande d  set code_last_psl=subquery.code_last_psl,     libelle_last_psl=subquery.libelle_last_psl FROM (SELECT distinct on (tt.eb_demande_num) tt.eb_demande_num, tt.lastpsl, 	  ( CASE 	 	WHEN psl.code = 1 THEN 	'Pickup'  	WHEN psl.code = 2 THEN 	'Departure'  	WHEN psl.code = 3 THEN 	'Arrival'  	WHEN psl.code = 4 THEN 	'Customs'  	WHEN psl.code = 5 THEN 	'Delivery' ELSE NULL  END  	) AS libelle_last_psl,  psl.code AS code_last_psl  FROM conflastpsl tt left join psl on psl.acode = tt.lastpsl  ) AS subquery WHERE d.eb_demande_num=subquery.eb_demande_num;  

*/