/*

update work.eb_user u set password ='$2a$10$BBOJ2Bp6m3B4Rthm8Q.Dy.n5J3u6QTjz3jD.ibXceXUoEBj3elyIS';

with orig as (select u.eb_user_num, u.nom, u.prenom, u.email, role, substr(u.email, 0, position('@' in u.email)) subemail from work.eb_user u where u.email not like '%mytower2018+%')
update work.eb_user u set email = (select 'mytower2018+'||(case when position('+' in subemail) >= 0 then substr(subemail, position('+' in subemail)+1) else subemail end)||'@gmail.com' from orig org where org.eb_user_num = u.eb_user_num) where u.email not like '%mytower2018+%';
update work.eb_user set username=email;
*/