
CREATE OR REPLACE FUNCTION work.create_indexes(tbl text, clmns text, insertIndexes boolean) 
    RETURNS void AS $$
DECLARE
    rec record;
    arr text[];
    stmt text;
BEGIN

    SELECT string_to_array(clmns, ',') INTO arr;
	
    FOR rec IN 
				
        SELECT DISTINCT 
            cc.column_name, cc.data_type, cc.table_name
		FROM 
			information_schema.columns cc
		LEFT JOIN pg_indexes ii on ii.indexdef like ('%('||cc.column_name||')%')
		WHERE
            cc.table_schema = 'work' 
            and cc.table_name = tbl
            and ii.indexname is null
            and cc.data_type like any (array['text', 'timestamp%', 'character varying', 'integer'])
            and (cc.column_name = ANY(arr))
				
    LOOP
        IF rec.data_type = 'text' OR rec.data_type LIKE 'character%' THEN

            SELECT 'CREATE INDEX idx_'||rec.table_name||'_'||rec.column_name||'_gin ON work.'||rec.table_name||' USING gin ('||rec.column_name||');' INTO stmt;
            IF insertIndexes THEN
                EXECUTE format(stmt);
            END IF;
            RAISE NOTICE '%', stmt;
            
            SELECT 'CREATE INDEX idx_'||rec.table_name||'_'||rec.column_name||'_lowergin ON work.'||rec.table_name||' USING gin (lower(('||rec.column_name||')::text));' INTO stmt;
            IF insertIndexes THEN
                EXECUTE format(stmt);
            END IF;
            RAISE NOTICE '%', stmt;
                
            SELECT 'CREATE INDEX idx_'||rec.table_name||'_'||rec.column_name||'_hash ON work.'||rec.table_name||' USING hash ('||rec.column_name||');' INTO stmt;
            IF insertIndexes THEN
                EXECUTE format(stmt);
            END IF;
            RAISE NOTICE '%', stmt;
            
            SELECT 'CREATE INDEX idx_'||rec.table_name||'_'||rec.column_name||'_lowerhash ON work.'||rec.table_name||' USING hash (lower(('||rec.column_name||')::text));' INTO stmt;
            IF insertIndexes THEN
                EXECUTE format(stmt);
            END IF;
            RAISE NOTICE '%', stmt;
        
--         ELSE
            
--             SELECT 'CREATE INDEX idx_'||rec.table_name||'_'||rec.column_name||' ON work.'||rec.table_name||' USING btree ('||rec.column_name||');' INTO stmt;
--             IF insertIndexes THEN
--                 EXECUTE format(stmt);
--             END IF;
--             RAISE NOTICE '%', stmt;
            
        END IF;
				
    END LOOP;
END;
$$ LANGUAGE plpgsql;

-- Usage: select work.create_indexes(columnsStr, autoCreateIndexes);
-- columnsStr: 'colonne1,colonne2,...colonneN'
-- autoCreateIndexes: set true to create indexes and false to show logs only