WITH extractquery AS (
SELECT
	d.eb_demande_num,
	d.list_categories,
	d.list_labels,
	jsonb_array_elements ( jsonb_array_elements ( d.list_categories ) #> '{labels}' ) #> '{ebLabelNum}' AS labels 
FROM
	WORK.eb_demande d 
WHERE
	d.list_labels is null and d.list_categories is not null
),
groupquery AS ( SELECT eb_demande_num, string_agg ( labels :: TEXT, ',' ) labels FROM extractquery GROUP BY eb_demande_num ) 
UPDATE WORK.eb_demande d
	SET list_labels = ( SELECT gq.labels FROM groupquery gq WHERE gq.eb_demande_num = d.eb_demande_num) 
WHERE
	d.list_labels is null and d.list_categories is not null ;