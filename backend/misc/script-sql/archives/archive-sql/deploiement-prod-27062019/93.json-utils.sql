drop function if exists work.update_value_by_keyvalue;
CREATE OR REPLACE FUNCTION work.update_value_by_keyvalue(pjson text, pkey text, pkeyvalue text, pkeyres text, pvalue anyelement)
returns jsonb
AS 
$BODY$
		
		SELECT 
			jsonb_agg(CASE WHEN pp->>pkey = pkeyvalue THEN (pp - pkeyres) || jsonb_build_object(pkeyres, pvalue) ELSE pp END)
		FROM jsonb_array_elements(pjson::jsonb) pp 
		
$BODY$
LANGUAGE sql;




drop function if exists work.remove_attr_from_array;
CREATE OR REPLACE FUNCTION work.remove_attr_from_array(pjson text, pkey text)
returns jsonb
AS 
$BODY$
		
		SELECT jsonb_agg(pp - pkey) val
		FROM jsonb_array_elements(pjson::jsonb) pp 
		
$BODY$
LANGUAGE sql;

