-- DROP  FUNCTION if exists work.init_mode_transport_en_quote;
-- CREATE FUNCTION work.init_mode_transport_en_quote(
--    )
--   RETURNS void
--   LANGUAGE 'plpgsql'

-- AS $BODY$
-- DECLARE

--   row_quote1 record ;
--   cur_quote1   CURSOR
--  FOR
--    select ex_eb_demande_transporteur_num from work.eb_demande_quote;
--      row_quote2 record ;
--   cur_quote2   CURSOR
--  FOR
--    select q.ex_eb_demande_transporteur_num,d.x_ec_mode_transport from work.eb_demande_quote q
--    left join work.eb_demande  d on q.x_eb_demande=d.eb_demande_num
--    where q.x_ec_mode_transport is null ;

-- BEGIN
-- --init le mode de transport par celui ds list_cost_categorie

-- OPEN cur_quote1;
-- LOOP
--  FETCH cur_quote1 INTO row_quote1 ;
--  EXIT WHEN NOT FOUND;
--  update work.eb_demande_quote set x_ec_mode_transport= cast (
--      (select distinct obj->>'xEcModeTransport' from work.eb_demande_quote q, jsonb_array_elements(q.list_cost_categorie) as obj
--       where q.ex_eb_demande_transporteur_num=row_quote1.ex_eb_demande_transporteur_num limit 1) as integer)
--        where ex_eb_demande_transporteur_num=row_quote1.ex_eb_demande_transporteur_num;
-- end loop;
--  CLOSE cur_quote1;
--  -- init le mode de transport par celui de la demande pour les quotes dont le champs list_cost_categorie  null
--   OPEN cur_quote2;
-- LOOP
--  FETCH cur_quote2 INTO row_quote2 ;
--  EXIT WHEN NOT FOUND;
--  update work.eb_demande_quote set x_ec_mode_transport=row_quote2.x_ec_mode_transport
--        where ex_eb_demande_transporteur_num=row_quote2.ex_eb_demande_transporteur_num;
-- end loop;
--  CLOSE cur_quote2;
--  end;
-- $BODY$;

-- SELECT work.init_mode_transport_en_quote();



update 
work.eb_demande_quote q set x_ec_mode_transport = (
	case 
		when (q.list_cost_categorie->0->>'xEcModeTransport')::int is not null and (q.list_cost_categorie->0->>'xEcModeTransport')::int > 0 
			then (q.list_cost_categorie->0->>'xEcModeTransport')::int 
		else (select d.x_ec_mode_transport from work.eb_demande d where d.eb_demande_num = q.x_eb_demande) 
	end
) 
where q.x_ec_mode_transport is null;