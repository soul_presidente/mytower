drop function if exists work.update_value_by_keyvalue;
CREATE OR REPLACE FUNCTION work.update_value_by_keyvalue(pjson text, pkey text, pkeyvalue text, pkeyres text, pvalue anyelement)
returns jsonb
AS 
$BODY$
		
		SELECT 
			jsonb_agg(CASE WHEN pp->>pkey = pkeyvalue THEN (pp - pkeyres) || jsonb_build_object(pkeyres, pvalue) ELSE pp END)
		FROM jsonb_array_elements(pjson::jsonb) pp 
		
$BODY$
LANGUAGE sql;




drop function if exists work.remove_attr_from_array;
CREATE OR REPLACE FUNCTION work.remove_attr_from_array(pjson text, pkey text)
returns jsonb
AS 
$BODY$
		
		SELECT jsonb_agg(pp - pkey) val
		FROM jsonb_array_elements(pjson::jsonb) pp 
		
$BODY$
LANGUAGE sql;












update work.eb_form_field set name = 'Matériel de guerre' where mot_technique = 'warGoods';
update work.eb_form_field set name = 'Date d''arrivée estimée' where mot_technique = 'dateArrivalEsstimed';
update work.eb_form_field set name = 'Licence Matériel de guerre' where mot_technique = 'warGoodLicenceReference';
update work.eb_form_field set name = 'Expéditeur' where mot_technique = 'charger';
update work.eb_form_field set name = 'Opération' where mot_technique = 'operation';
update work.eb_form_field set name = 'Coût de l''outillage' where mot_technique = 'toolingCost';
update work.eb_form_field set name = 'Texte législatif' where mot_technique = 'attroneyText';
update work.eb_form_field set name = 'Nb régimes temporaires associés' where mot_technique = 'nbOfTemporaryRegime';
update work.eb_form_field set name = 'Numéro de déclaration' where mot_technique = 'declarationNumber';
update work.eb_form_field set name = 'Référence déclaration' where mot_technique = 'declarationReference';
update work.eb_form_field set name = 'Déclarant' where mot_technique = 'xEbUserDeclarant';
update work.eb_form_field set name = 'Unité du poids brut' where mot_technique = 'grossWeightUnit';
update work.eb_form_field set name = 'Numéro de TVA' where mot_technique = 'numTva';
update work.eb_form_field set name = 'Trafic à la frontière' where mot_technique = 'traficAtBorder';
update work.eb_form_field set name = 'Ref demande dédouanement' where mot_technique = 'refDemandeDedouanement';
update work.eb_form_field set name = 'ID dédouanement' where mot_technique = 'idDedouanement';
update work.eb_form_field set name = 'Aéroport / port d''arrivée' where mot_technique = 'airportPortOfArrival';
update work.eb_form_field set name = 'Quantité' where mot_technique = 'quantity';
update work.eb_form_field set name = 'Pays de dédouanement' where mot_technique = 'xEcCountryDedouanement';
update work.eb_form_field set name = 'Quantité totale' where mot_technique = 'totalQuantity';
update work.eb_form_field set name = 'Régime' where mot_technique = 'customRegime';
update work.eb_form_field set name = 'Lieu de dédouanement' where mot_technique = 'lieuDedouanement';
update work.eb_form_field set name = 'Biens à double usage' where mot_technique = 'biensDoubleUsage';













update 
work.eb_demande_quote q set x_ec_mode_transport = (
	case 
		when (q.list_cost_categorie->0->>'xEcModeTransport')::int is not null and (q.list_cost_categorie->0->>'xEcModeTransport')::int > 0 
			then (q.list_cost_categorie->0->>'xEcModeTransport')::int 
		else (select d.x_ec_mode_transport from work.eb_demande d where d.eb_demande_num = q.x_eb_demande) 
	end
) 
where q.x_ec_mode_transport is null;








update work.eb_type_documents set module = '[1, 2]'::jsonb, list_module_str = '1|2' where code = '5';
update work.eb_demande d set list_type_documents = work.update_value_by_keyvalue(
	work.update_value_by_keyvalue(list_type_documents::text, 'code', '5', 'listModuleStr', '1|2'::text)::text, 
'code', '5', 'module', '[1, 2]'::jsonb);










update work.eb_tt_schema_psl sc set list_psl = work.remove_attr_from_array(list_psl::text, 'expectedDate');









update work.eb_demande_quote q set list_eb_tt_compagnie_psl = 
		work.update_value_by_keyvalue(
			work.update_value_by_keyvalue((list_eb_tt_compagnie_psl)::text, 
			'codeAlpha'::text, 'PIC'::text, 'expectedDate'::text, pickup_time::date)::text, 
			'codeAlpha'::text, 'DEL'::text, 'expectedDate'::text, delivery_time::date) where x_eb_demande <= 6249;










drop function if exists work.get_date_estimee;
CREATE OR REPLACE FUNCTION work.get_date_estimee(demandenum int, codealpha text)
returns timestamp
AS 
$BODY$
		
		select date_estimee from work.eb_tt_psl_app left join work.eb_tt_tracing tt on tt.eb_tt_tracing_num = x_eb_tt_tracing where tt.x_eb_demande = demandenum and date_estimee is not null and code_alpha = codealpha order by date_estimee desc limit 1;
		
$BODY$
LANGUAGE sql;



update work.eb_demande d set x_eb_schema_psl = (x_eb_schema_psl - 'listPsl') || jsonb_build_object('listPsl', 	
		work.update_value_by_keyvalue( 
			work.update_value_by_keyvalue(
				work.update_value_by_keyvalue(
					work.update_value_by_keyvalue(
						work.update_value_by_keyvalue((x_eb_schema_psl->'listPsl')::text,
							'codeAlpha'::text, 'PIC'::text, 'expectedDate'::text, work.get_date_estimee(d.eb_demande_num, 'PIC')::date)::text,
							'codeAlpha'::text, 'DEP'::text, 'expectedDate'::text, work.get_date_estimee(d.eb_demande_num, 'DEP')::date)::text,
							'codeAlpha'::text, 'ARR'::text, 'expectedDate'::text, work.get_date_estimee(d.eb_demande_num, 'ARR')::date)::text,
							'codeAlpha'::text, 'CUS'::text, 'expectedDate'::text, work.get_date_estimee(d.eb_demande_num, 'CUS')::date)::text,
							'codeAlpha'::text, 'DEL'::text, 'expectedDate'::text, work.get_date_estimee(d.eb_demande_num, 'DEL')::date
						)::jsonb)
where eb_demande_num <= 6249 and x_ec_statut >= 11;










update work.eb_saved_form sf set x_eb_compagnie = (select x_eb_compagnie from work.eb_user u where u.eb_user_num = sf.x_eb_user);

update work.eb_saved_form sf set form_values = (

	with updated_incoterm as (
		SELECT ((sf.form_values::jsonb - 'xEcIncotermLibelle') || jsonb_build_object('xEcIncotermLibelle', 
		(select ic.libelle from work.eb_incoterm ic where ic.eb_incoterm_num::text = ((sf.form_values::jsonb)->>'xEcIncoterm')))) as val
	)
	
	, updated_owner as (
			SELECT (val - 'user' || CASE WHEN val->'user'->>'ebUserNum' is not null THEN jsonb_build_object('user', val->'user') ELSE jsonb_build_object('user', 
			(
					SELECT CASE WHEN (SELECT u.role from work.eb_user u where u.eb_user_num = sf.x_eb_user) = 1 THEN ('{"ebUserNum":'||sf.x_eb_user||'}')::jsonb ELSE (select NULL::jsonb) END
			)
		) END ) as val FROM updated_incoterm
	)
	
	, updated_type_req as (
			SELECT ((val - 'xEcTypeDemande') - 'xEbTypeRequest' || jsonb_build_object('xEbTypeRequest', 
			(
					SELECT tr.eb_type_request_num FROM work.eb_type_request tr where tr.sens_chrono_pricing = (val->>'xEcTypeDemande')::int AND tr.x_eb_company = (val->'user'->'ebCompagnie'->>'ebCompagnieNum')::int
			)
		)) as val FROM updated_owner
	)
	
	
	, updated_schema as (
		SELECT (val - 'xEbSchemaPsl' || jsonb_build_object('xEbSchemaPsl', 
			(
					with schemapsl as (
						SELECT 
								pp.list_psl "listPsl", 
								config_psl "configPsl",
								designation,
								is_editable "isEditable",
								list_mode_transport "listModeTransport",
								list_psl "listPsl",
								x_ec_incoterm_libelle "xEcIncotermLibelle"
						from work.eb_tt_schema_psl pp where pp.x_ec_incoterm_libelle = val->>'xEcIncotermLibelle' and position(val->>'xEcModeTransport' in pp.list_mode_transport) > 0 AND pp.x_eb_compagnie = (val->'user'->'ebCompagnie'->>'ebCompagnieNum')::int
					)
					SELECT to_jsonb(pp) from schemapsl pp
			)
		)) as val FROM updated_type_req
	)
	
	
	, updated_quotes as (			
			SELECT (val - 'exEbDemandeTransporteurs') || (jsonb_build_object('exEbDemandeTransporteurs', '[]'::jsonb)) as val FROM updated_schema
	)
	
	
	, updated_origin as (
			SELECT (val - 'ebPartyOrigin' || jsonb_build_object('ebPartyOrigin', 
			(
					SELECT CASE WHEN val->'ebPartyOrigin'->>'xEcCountry' IS NULL OR val->'ebPartyOrigin'->'xEcCountry'->>'ecCountryNum' IS NULL THEN NULL::jsonb ELSE val->'ebPartyOrigin' END
			)
		)) as val FROM updated_quotes
	)
	
	, updated_dest as (
			SELECT (val - 'ebPartyDest' || jsonb_build_object('ebPartyDest', 
			(
					SELECT CASE WHEN val->'ebPartyDest'->>'xEcCountry' IS NULL OR val->'ebPartyDest'->'xEcCountry'->>'ecCountryNum' IS NULL THEN NULL::jsonb ELSE val->'ebPartyDest' END
			)
		)) as val FROM updated_origin
	)


 SELECT val::text from updated_dest

)
where sf.eb_comp_num = 2
and sf.x_eb_user in (select u.eb_user_num from work.eb_user u where u.role = 1 or u.role = 3)
and (sf.form_values::jsonb)?'xEcIncoterm';















WITH extractquery AS (
SELECT
	d.eb_demande_num,
	d.list_categories,
	d.list_labels,
	jsonb_array_elements ( jsonb_array_elements ( d.list_categories ) #> '{labels}' ) #> '{ebLabelNum}' AS labels 
FROM
	WORK.eb_demande d 
WHERE
	d.list_labels is null and d.list_categories is not null
),
groupquery AS ( SELECT eb_demande_num, string_agg ( labels :: TEXT, ',' ) labels FROM extractquery GROUP BY eb_demande_num ) 
UPDATE WORK.eb_demande d
	SET list_labels = ( SELECT gq.labels FROM groupquery gq WHERE gq.eb_demande_num = d.eb_demande_num) 
WHERE
	d.list_labels is null and d.list_categories is not null ;








UPDATE work.eb_demande d SET x_ec_statut = (
	WITH tracings AS (
			SELECT tt.eb_tt_tracing_num 
			FROM work.eb_tt_tracing tt 
			WHERE 
			tt.x_eb_demande = d.eb_demande_num
	)
	, pslend AS (
			SELECT * FROM work.eb_tt_psl_app pslapp 
			WHERE 
				pslapp.x_eb_tt_tracing IN (SELECT * FROM tracings)
				AND (pslapp.company_psl->>'endPsl')::boolean = true
	)
	, notvalidated AS (
			SELECT * FROM pslend
			WHERE 
				(validated = false OR validated IS NULL) 
	)
	, validated AS (
			SELECT * 
			FROM work.eb_tt_psl_app pslapp 
			WHERE 
				pslapp.x_eb_tt_tracing IN (SELECT * FROM tracings)
				AND pslapp.validated = true 
-- 					AND (pslapp.company_psl->>'startPsl')::boolean = true
			LIMIT 1
	)
	SELECT 
		CASE 
			WHEN EXISTS (SELECT * FROM tracings) AND EXISTS (SELECT * FROM pslend) AND NOT EXISTS (SELECT * FROM notvalidated) THEN 13 
			WHEN EXISTS (SELECT * FROM validated) THEN 12
			ELSE 11
		END
) WHERE d.x_ec_statut >= 11


