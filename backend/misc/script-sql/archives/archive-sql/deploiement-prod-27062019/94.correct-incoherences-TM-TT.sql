

UPDATE work.eb_demande d SET x_ec_statut = (
	WITH tracings AS (
			SELECT tt.eb_tt_tracing_num 
			FROM work.eb_tt_tracing tt 
			WHERE 
			tt.x_eb_demande = d.eb_demande_num
	)
	, pslend AS (
			SELECT * FROM work.eb_tt_psl_app pslapp 
			WHERE 
				pslapp.x_eb_tt_tracing IN (SELECT * FROM tracings)
				AND (pslapp.company_psl->>'endPsl')::boolean = true
	)
	, notvalidated AS (
			SELECT * FROM pslend
			WHERE 
				(validated = false OR validated IS NULL) 
	)
	, validated AS (
			SELECT * 
			FROM work.eb_tt_psl_app pslapp 
			WHERE 
				pslapp.x_eb_tt_tracing IN (SELECT * FROM tracings)
				AND pslapp.validated = true 
-- 					AND (pslapp.company_psl->>'startPsl')::boolean = true
			LIMIT 1
	)
	SELECT 
		CASE 
			WHEN EXISTS (SELECT * FROM tracings) AND EXISTS (SELECT * FROM pslend) AND NOT EXISTS (SELECT * FROM notvalidated) THEN 13 
			WHEN EXISTS (SELECT * FROM validated) THEN 12
			ELSE 11
		END
) WHERE d.x_ec_statut >= 11

