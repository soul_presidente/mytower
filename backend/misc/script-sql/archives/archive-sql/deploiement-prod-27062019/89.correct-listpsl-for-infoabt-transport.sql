

drop function if exists work.get_date_estimee;
CREATE OR REPLACE FUNCTION work.get_date_estimee(demandenum int, codealpha text)
returns timestamp
AS 
$BODY$
		
		select date_estimee from work.eb_tt_psl_app left join work.eb_tt_tracing tt on tt.eb_tt_tracing_num = x_eb_tt_tracing where tt.x_eb_demande = demandenum and date_estimee is not null and code_alpha = codealpha order by date_estimee desc limit 1;
		
$BODY$
LANGUAGE sql;



update work.eb_demande d set x_eb_schema_psl = (x_eb_schema_psl - 'listPsl') || jsonb_build_object('listPsl', 	
		work.update_value_by_keyvalue( 
			work.update_value_by_keyvalue(
				work.update_value_by_keyvalue(
					work.update_value_by_keyvalue(
						work.update_value_by_keyvalue((x_eb_schema_psl->'listPsl')::text,
							'codeAlpha'::text, 'PIC'::text, 'expectedDate'::text, work.get_date_estimee(d.eb_demande_num, 'PIC')::date)::text,
							'codeAlpha'::text, 'DEP'::text, 'expectedDate'::text, work.get_date_estimee(d.eb_demande_num, 'DEP')::date)::text,
							'codeAlpha'::text, 'ARR'::text, 'expectedDate'::text, work.get_date_estimee(d.eb_demande_num, 'ARR')::date)::text,
							'codeAlpha'::text, 'CUS'::text, 'expectedDate'::text, work.get_date_estimee(d.eb_demande_num, 'CUS')::date)::text,
							'codeAlpha'::text, 'DEL'::text, 'expectedDate'::text, work.get_date_estimee(d.eb_demande_num, 'DEL')::date
						)::jsonb)
where eb_demande_num <= 6249 and x_ec_statut >= 11;