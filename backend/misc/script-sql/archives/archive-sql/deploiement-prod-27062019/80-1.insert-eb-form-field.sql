INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (80, '80', null, null, 'leg', 'Segment', null, null, null, 'LEG', null, null,1, 2);

INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (81, '81', null, null, 'nbOfTemporaryRegime', 'Nb régimes temporaires associés', null, null, null, 'NB_OF_TEMPORARY_REGIME_LINKED', null, null, 1, 2);

INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (82, '82', null, null, 'dateEcs', 'Date Ecs', null, null, null, 'DATE_ECS', null, null, 7, 2);

INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (83, '83', null, null, 'statut', 'Statut', null, null, null, 'STATUS', null, null, 1, 2);

INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (84, '84', null, null, 'declarationNumber', 'Numéro de déclaration', null, null, null, 'DECLARATION_NUMBER', null, null, 3, 2);

INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (85, '85', null, null, 'declarationReference', 'Référence déclaration', null, null, null, 'DECLARATION_REFERENCE', null, null,1, 2);

INSERT INTO work.eb_form_field(
	eb_form_field_num, code, fetch_attribute, fetch_value, mot_technique, name, ordre, required, src_list_items, translate, value_available, visibilite, x_eb_form_type, x_eb_view_form)
	VALUES (86, '86', null, null, 'xEbUserDeclarant', 'Déclarant', null, null, null, 'DECLARANT', null, null,1, 2);

