/** change code type to varchar */
ALTER TABLE "work"."eb_demande_fichiers_joint" 
  ALTER COLUMN "id_categorie" TYPE varchar(255) USING "id_categorie"::varchar(255);

/** update num_entity with x_eb_demande value */
 update work.eb_demande_fichiers_joint set num_entity = x_eb_demande;