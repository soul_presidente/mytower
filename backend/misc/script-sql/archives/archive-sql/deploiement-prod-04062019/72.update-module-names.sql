/* Correction du quiproquo sur le code du module Quality management */
UPDATE "work".eb_user_module SET x_ec_module = 3 WHERE x_ec_module = 6;

-- update old libeles with the one in i18n json files
update work.ec_module set
    libelle = 'PRICING' where ec_module_num = 1;

update work.ec_module set
    libelle = 'TM' where ec_module_num = 2;

update work.ec_module set
    libelle = 'QM' where ec_module_num = 3;

update work.ec_module set
    libelle = 'COMPLIANCE_MATRIX' where ec_module_num = 4;

update work.ec_module set
    libelle = 'TT' where ec_module_num = 5;

update work.ec_module set
    libelle = 'FA' where ec_module_num = 7;

update work.ec_module set
    libelle = 'ANALYTICS' where ec_module_num = 8;

update work.ec_module set
    libelle = 'CUSTOMS' where ec_module_num = 9;

update work.ec_module set
    libelle = 'ORDER' where ec_module_num = 10;

update work.ec_module set
    libelle = 'RSCH' where ec_module_num = 11;

-- insert missing module Chanel PB Delivery
insert into work.ec_module(ec_module_num, libelle) values (18, 'DELIVERY');

-- insert module compta matiere
insert into work.ec_module(ec_module_num, libelle) values (19, 'COMPTA_MATIERE');

-- remove module 6 : CLAIMS (old name of quality management )
delete from work."eb_user_module" where x_ec_module = 6 or ec_module_num=6;
delete from work."eb_user_profile_module" where x_ec_module = 6;
delete FROM work."ec_module" where ec_module_num = 6;