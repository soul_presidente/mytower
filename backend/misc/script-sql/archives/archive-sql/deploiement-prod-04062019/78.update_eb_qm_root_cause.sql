delete from work.eb_qm_root_cause ;
ALTER TABLE work.eb_qm_root_cause ALTER COLUMN severite TYPE integer USING (severite::integer);
ALTER TABLE work.eb_qm_root_cause ALTER COLUMN statut TYPE integer USING (statut::integer);