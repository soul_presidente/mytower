-- put values for the new comlumn "type_event"	
UPDATE work.eb_tt_categorie_deviation
	SET type_event=1 WHERE eb_tt_categorie_deviation_num BETWEEN 1 AND 9;
	
	
delete from work.eb_tt_categorie_deviation
where eb_tt_categorie_deviation_num BETWEEN 21 AND 24;


INSERT INTO work.eb_tt_categorie_deviation(
    eb_tt_categorie_deviation_num, code, libelle)
    VALUES (10 ,'10', 'Missing update');
INSERT INTO work.eb_tt_categorie_deviation(
    eb_tt_categorie_deviation_num, code, libelle)
    VALUES (11 ,'11', 'Delay');
INSERT INTO work.eb_tt_categorie_deviation(
    eb_tt_categorie_deviation_num, code, libelle, type_event)
    VALUES (21 ,'21', 'Missing document', 3);
	
	
	
	
	
	
delete from work.ec_mime_type;

insert into work.ec_mime_type (mimetype,description) values ('application/msword','WORD');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.ms-excel','EXCEL');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.ms-powerpoint','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.openxmlformats-officedocument.wordprocessingml.document','WORD');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','EXCEL');
insert into work.ec_mime_type (mimetype,description) values ('application/pdf','PDF');
insert into work.ec_mime_type (mimetype,description) values ('text/html','HTML');
insert into work.ec_mime_type (mimetype,description) values ('text/plain','TEXT');
insert into work.ec_mime_type (mimetype,description) values ('image/jpeg','IMAGE');
insert into work.ec_mime_type (mimetype,description) values ('image/png','IMAGE');
insert into work.ec_mime_type  (mimetype,description) values ('image/gif','IMAGE');
insert into work.ec_mime_type (mimetype,description) values ('image/bmp','IMAGE');
insert into work.ec_mime_type (mimetype,description) values ('image/tiff','IMAGE');
insert into work.ec_mime_type (mimetype,description) values ('video/x-msvideo','VIDEO');
insert into work.ec_mime_type (mimetype,description) values ('text/csv','EXCEL');
insert into work.ec_mime_type (mimetype,description) values ('video/mpeg','IMAGE');
insert into work.ec_mime_type (mimetype,description) values ('video/webm','VIDEO');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.openxmlformats-officedocument.presentationml.presentation','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.openxmlformats-officedocument.wordprocessingml.document','WORD');
insert into work.ec_mime_type (mimetype,description) values ('image/x-icon','IMAGE');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.openxmlformats-officedocument.presentationml.presentation','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.openxmlformats-officedocument.presentationml.slideshow','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.ms-powerpoint.addin.macroEnabled.12','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.ms-powerpoint.presentation.macroEnabled.12','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.ms-powerpoint.template.macroEnabled.12','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.ms-powerpoint.slideshow.macroEnabled.12','POWERPOINT');





delete from work.eb_qm_root_cause ;
--ALTER TABLE work.eb_qm_root_cause ALTER COLUMN severite TYPE integer USING (severite::integer);
--ALTER TABLE work.eb_qm_root_cause ALTER COLUMN statut TYPE integer USING (statut::integer);





