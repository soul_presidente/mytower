ALTER TABLE work.eb_tt_schema_psl RENAME eb_incoterm_psl_num TO eb_tt_schema_psl_num;
DELETE FROM work.eb_tt_schema_psl;

update work.eb_tt_psl_app pslapp set code_alpha = (case when code_psl = 1 then 'PIC' when code_psl = 2 then 'DEP' when code_psl = 3 then 'ARR' when code_psl = 4 then 'CUS' when code_psl = 5 then 'DEL' else null end) where code_alpha is null and code_psl in (1, 2, 3, 4, 5);

update work.eb_tt_tracing set code_alpha_psl_courant = (case when code_psl_courant = 1 then 'PIC' when code_psl_courant = 2 then 'DEP' when code_psl_courant = 3 then 'ARR' when code_psl_courant = 4 then 'CUS' when code_psl_courant = 5 then 'DEL' else null end) where code_alpha_psl_courant is null and code_psl_courant in (1, 2, 3, 4, 5);