INSERT INTO work.tmp_event(
	
    tmp_event_num ,
    date_creation_sys,
    code_psl_normalise ,
    type_event ,
    unit_reference,
    x_edi_map_psl ,
    x_tmp_event_file ,
    is_treated 
	)
	
SELECT id,now(), md5(random()::text), (array[0,1, 2])[floor(random() * 3 + 1)] ,md5(random()::text) ,2,1, (array[true,false, null])[floor(random() * 3 + 1)]
  FROM generate_series(6, 9) AS id