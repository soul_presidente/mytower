SELECT
	u.email,
	mm.libelle,
	ff.ref_object,
	ff.id_object 
FROM
	WORK.eb_favori ff
	LEFT JOIN WORK.eb_demande d ON d.eb_demande_num = ff.id_object 
	AND ( ff.MODULE = 1 OR ff.MODULE = 2 )
	LEFT JOIN WORK.eb_tt_tracing tt ON tt.eb_tt_tracing_num = ff.id_object 
	AND ff.MODULE = 5
	LEFT JOIN WORK.eb_user u ON u.eb_user_num = ff.x_eb_user
	LEFT JOIN WORK.ec_module mm ON mm.ec_module_num = ff.MODULE 
WHERE
	ff.eb_favori_num IS NOT NULL
	order by u.email, mm.libelle;