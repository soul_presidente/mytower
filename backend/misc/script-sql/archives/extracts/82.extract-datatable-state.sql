SELECT
	u.email,
CASE
	
	WHEN ss.datatable_comp_id = 1 THEN
	'Pricing' 
	WHEN ss.datatable_comp_id = 2 THEN
	'Booking' 
	WHEN ss.datatable_comp_id = 3 THEN
	'Quality management' 
	WHEN ss.datatable_comp_id = 5 THEN
	'Track & Trace' 
	WHEN ss.datatable_comp_id = 9 THEN
	'Custom' 
	WHEN ss.datatable_comp_id = 1 THEN
	'Order' 
	WHEN ss.datatable_comp_id = 1 THEN
	'Delivery' 
	END,
	(
		select string_agg(sc.header, ',') 
		from json_to_recordset((ss.state)::json->'selectedCols') 
		as sc("header" TEXT, "translateCode" text, "field" TEXT)
	)
FROM
	WORK.eb_datatable_state ss
	LEFT JOIN WORK.eb_user u ON u.eb_user_num = ss.x_eb_user_num 
ORDER BY
	u.email,
ss.datatable_comp_id 