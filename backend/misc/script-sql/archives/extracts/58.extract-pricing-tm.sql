SELECT

	d.ref_transport,
	d.date_creation,
	compch.nom as "Chargeur: Société",
	etch.nom as "Chargeur: Etablissement",
	ch.email AS "Chargeur: Email",
	d.ref_transport,
	d.libelle_origin_country,
	d.libelle_dest_country,
	d.date_creation,
	d.customer_reference as "Customer Reference",
	d.units_reference as "Unit Reference",  
	dg as "DGR",  
	
	(select string_agg ( distinct class_good  , ',' )  as class  
	from work.eb_marchandise   m 
	  join work.eb_commande c on (c. eb_commande_num =  m.x_eb_commande)  
	  join work.eb_demande_commande ec on (ec. x_eb_demande =  d.eb_demande_num and ec. x_eb_commande = c. eb_commande_num )  where m.x_eb_commande = c. eb_commande_num ) ,

(select string_agg ( distinct un  , ',' )  as class  
	from work.eb_marchandise   m 
	  join work.eb_commande c on (c. eb_commande_num =  m.x_eb_commande)  
	  join work.eb_demande_commande ec on (ec. x_eb_demande =  d.eb_demande_num and ec. x_eb_commande = c. eb_commande_num )  where m.x_eb_commande = c. eb_commande_num ) ,
	  
	d.total_nbr_parcel as "Nb Parcel",  
	d.total_volume as "Volume",
	d.total_weight as "Total weight",
	d.total_taxable_weight as "Taxable weight",
	
	CASE
		when x_ec_cancelled = 1 then 'Cancelled'
		WHEN x_ec_statut = 1 THEN 'In process waiting for quote' 
		WHEN x_ec_statut = 2 THEN 'Waiting for recommendation' 
		WHEN x_ec_statut = 3 THEN 'Waiting for confirmation' 
		WHEN x_ec_statut >= 4 THEN 'Confirmed'
		WHEN x_ec_statut = 6 THEN 'Cancellation of quotation request ' 
		WHEN x_ec_statut = 11 THEN 'Waiting for pick up' 
		WHEN x_ec_statut = 12 THEN 'Transport on going'
		WHEN x_ec_statut = 13 THEN 'Delivery' 
		WHEN x_ec_statut = 14 THEN 'Waiting for transportation request'
	END AS "statut QR ",
	CASE
		when x_ec_cancelled = 1 then 'Cancelled'
		WHEN x_ec_statut = 11 THEN 'Waiting for pick up'
		WHEN x_ec_statut = 12 THEN 'Transport on going' 
		WHEN x_ec_statut = 13 THEN 'Completed'
	END AS "statut TR ",
	compq.nom as "Transporteur sollicité : Société",
	etq.nom as "Transporteur sollicité : Etablissement",
	tr.email as "Transporteur sollicité : Email",
	CASE
		WHEN q.status = 1 THEN 'Request received by FF' 
		WHEN q.status = 2 THEN 'Quote sent by FF' 
		WHEN q.status = 3 THEN 'Recommendation by CT' 
		WHEN q.status = 4 THEN 'Final choice'
		else q.status||''
	END AS "statut quotation",
	(select ic.libelle from work.eb_incoterm ic where ic.eb_incoterm_num = x_ec_incoterm limit 1) as "Incoterm",
	tr.email as "tr email",
CASE
	
	WHEN d.x_ec_mode_transport = 1 THEN
	'Air' 
	WHEN d.x_ec_mode_transport = 2 THEN
	'Sea' 
	WHEN d.x_ec_mode_transport = 3 THEN
	'Road' 
	WHEN x_ec_mode_transport = 4 THEN
	'Integrator' ELSE'' || x_ec_mode_transport 
	END AS "mode transport",
		
	q.pickup_time AS "Estimated Pickup (cotation)",
	q.delivery_time AS "Estimated Delivery (cotation)",
	q.transit_time AS "Total Transit Time (cotation)",
	q.lead_time_departure AS "Pre-carriage Leadtime (cotation)",
	q.lead_time_arrival AS "Transit Time (cotation)",
	
	d.date_of_goods_availability AS "Date of goods availability",
	d.date_pickuptm AS "Pickup (TM)",
	d.final_delivery AS "Final delivery (TM)",
	d.etd AS "ETD (TM)",
	d.eta AS "ETA (TM)"
	
FROM
	WORK.eb_demande_quote  q
	left JOIN WORK.eb_demande d ON ( q.x_eb_demande = d.eb_demande_num)
	LEFT JOIN WORK.eb_user ch ON ( ch.eb_user_num = d.x_eb_user )
	LEFT JOIN WORK.eb_user tr ON ( tr.eb_user_num = q.x_transporteur )
	LEFT JOIN WORK.eb_etablissement etq ON ( etq.eb_etablissement_num = q.x_eb_etablissement )
	LEFT JOIN WORK.eb_compagnie compq ON ( compq.eb_compagnie_num = etq.x_eb_compagnie )
	LEFT JOIN WORK.eb_etablissement etch ON ( etch.eb_etablissement_num = d.x_eb_etablissement )
	LEFT JOIN WORK.eb_compagnie compch ON ( compch.eb_compagnie_num = etch.x_eb_compagnie )
	 
ORDER BY
d.eb_demande_num DESC;
