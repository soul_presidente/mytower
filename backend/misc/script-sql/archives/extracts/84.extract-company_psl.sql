SELECT
	cc.nom,
	code_alpha,
	libelle,
	case when date_attendue = TRUE then 'Oui' else 'Non' end date_attendue,
	case when quantite_attendue = TRUE then 'Oui' else 'Non' end quantite_attendue,
	case when ref_document_attendue = TRUE then 'Oui' else 'Non' end ref_document_attendue
FROM
	WORK.eb_tt_company_psl psl
	left join work.eb_compagnie cc on cc.eb_compagnie_num = psl.x_eb_company
	order by cc.nom, code_alpha;