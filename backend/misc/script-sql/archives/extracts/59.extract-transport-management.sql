
WITH psl as (
	(select 'PIC'::text as acode, 1::int as code
	union
	select 'DEP'::text as acode, 2::int as code
	union
	select 'ARR'::text as acode, 3::int as code
	union
	select 'CUS'::text as acode, 4::int as code
	union
	select 'DEL'::text as acode, 5::int as code)
	order by code
) 

, pslapp as (
		select 
		ttpslapp.eb_tt_psl_app_num, ttpslapp.code_psl, ttpslapp.date_actuelle, ttpslapp.date_estimee, ttpslapp.date_negotiation, ttpslapp.date_creation, ttpslapp.x_eb_tt_tracing, (select acode from psl where psl.code = ttpslapp.code_psl) as acode
		from work.eb_tt_psl_app ttpslapp order  by ttpslapp.eb_tt_psl_app_num asc
	)
	
	, grouppsl as (	
	select 	
		date_creation, 
		x_eb_tt_tracing, 
		'{"x_eb_tt_tracing":' || x_eb_tt_tracing::text || ',' || string_agg ( '"' || lower(acode) || '":"' || cast(date_actuelle as TEXT)||'"' , ',' ) || '}' as date_actuelle, 
		'{"x_eb_tt_tracing":' || x_eb_tt_tracing::text || ',' || string_agg ( '"' || lower(acode) || '":"' || cast(date_estimee as TEXT)||'"' , ',' ) || '}' as date_estimee, 
		'{"x_eb_tt_tracing":' || x_eb_tt_tracing::text || ',' || string_agg ( '"' || lower(acode) || '":"' || cast(date_negotiation as TEXT)||'"' , ',' ) || '}' as date_negotiation,
		(select config_psl  from  work.eb_tt_tracing tt  where tt.eb_tt_tracing_num = pt.x_eb_tt_tracing) as orgconfig 
	from pslapp pt 
	group by x_eb_tt_tracing, date_creation
)

, splitedpsldates as (
SELECT 
	t.x_eb_tt_tracing, 
	
	da.pic as "Pickup Actual date",
	da.dep as "Departure Actual date",
	da.arr as "Arrival Actual date",
	da.cus as "Customs Actual date",
	da.del as "Delivery Actual date"
	
FROM   
	grouppsl t
  left join json_to_record(cast(t.date_actuelle AS json)) da(x_eb_tt_tracing int, PIC text, DEP text, ARR text, CUS text, DEL text) on (da.x_eb_tt_tracing = t.x_eb_tt_tracing)
	left join json_to_record(cast(t.date_estimee AS json)) de(x_eb_tt_tracing int, PIC text, DEP text, ARR text, CUS text, DEL text) on (de.x_eb_tt_tracing = t.x_eb_tt_tracing)
	left join json_to_record(cast(t.date_negotiation AS json)) dn(x_eb_tt_tracing int, PIC text, DEP text, ARR text, CUS text, DEL text) on (dn.x_eb_tt_tracing = t.x_eb_tt_tracing)
)

SELECT
	d.ref_transport,
	d.date_creation,
	compch.nom as "Chargeur: Société",
	etch.nom as "Chargeur: Etablissement",
	ch.email AS "Chargeur: Email",
	
	(select ic.libelle from work.eb_incoterm ic where ic.eb_incoterm_num = d.x_ec_incoterm limit 1) as "Incoterm",
	CASE
		WHEN d.x_ec_mode_transport = 1 THEN 'Air' 
		WHEN d.x_ec_mode_transport = 2 THEN 'Sea' 
		WHEN d.x_ec_mode_transport = 3 THEN 'Road' 
		WHEN d.x_ec_mode_transport = 4 THEN 'Integrator' 
		WHEN d.x_ec_mode_transport = 5 THEN 'Rail' 
		ELSE'' || d.x_ec_mode_transport 
	END AS "mode transport",
	
	d.libelle_last_psl,
	
	CASE
		when x_ec_cancelled = 1 then 'Cancelled'
		WHEN x_ec_statut = 1 THEN 'In process waiting for quote' 
		WHEN x_ec_statut = 2 THEN 'Waiting for recommendation' 
		WHEN x_ec_statut = 3 THEN 'Waiting for confirmation' 
		WHEN x_ec_statut >= 4 THEN 'Confirmed'
		WHEN x_ec_statut = 6 THEN 'Cancellation of quotation request ' 
		WHEN x_ec_statut = 11 THEN 'Waiting for pick up' 
		WHEN x_ec_statut = 12 THEN 'Transport on going'
		WHEN x_ec_statut = 13 THEN 'Delivery' 
		WHEN x_ec_statut = 14 THEN 'Waiting for transportation request'
	END AS "statut QR ",
	CASE
		when x_ec_cancelled = 1 then 'Cancelled'
		WHEN x_ec_statut = 11 THEN 'Waiting for pick up'
		WHEN x_ec_statut = 12 THEN 'Transport on going' 
		WHEN x_ec_statut = 13 THEN 'Completed'
	END AS "statut TR ",
	compq.nom as "Transporteur sollicité : Société",
	etq.nom as "Transporteur sollicité : Etablissement",
	tr.email as "Transporteur sollicité : Email",
	CASE
		WHEN q.status = 1 THEN 'Request received by FF' 
		WHEN q.status = 2 THEN 'Quote sent by FF' 
		WHEN q.status = 3 THEN 'Recommendation by CT' 
		WHEN q.status = 4 THEN 'Final choice'
		else q.status|| ''
	END AS "statut quotation", 
	
	trac.customer_reference as "Unit ref", 
	case 
		when trac.code_psl_courant = 1 then 'Pickup'
		when trac.code_psl_courant = 2 then 'Departure'
		when trac.code_psl_courant = 3 then 'Arrival'
		when trac.code_psl_courant = 4 then 'Customs'
		when trac.code_psl_courant = 5 then 'Delivery'
		else 'Waiting for pickup'
	end as "Status TT du Unit",
	d.date_last_psl "Date last psl",
	spd.*

FROM
	WORK.eb_demande_quote  q
	left JOIN WORK.eb_demande d ON ( q.x_eb_demande = d.eb_demande_num)
	LEFT JOIN WORK.eb_user ch ON ( ch.eb_user_num = d.x_eb_user )
	LEFT JOIN WORK.eb_user tr ON ( tr.eb_user_num = q.x_transporteur )
	LEFT JOIN WORK.eb_etablissement etq ON ( etq.eb_etablissement_num = q.x_eb_etablissement )
	LEFT JOIN WORK.eb_compagnie compq ON ( compq.eb_compagnie_num = etq.x_eb_compagnie )
	LEFT JOIN WORK.eb_etablissement etch ON ( etch.eb_etablissement_num = d.x_eb_etablissement )
	LEFT JOIN WORK.eb_compagnie compch ON ( compch.eb_compagnie_num = etch.x_eb_compagnie )
	right join work.eb_tt_tracing trac on trac.x_eb_demande = d.eb_demande_num
	left join splitedpsldates spd on (spd.x_eb_tt_tracing = trac.eb_tt_tracing_num)
	
	where q.status = 4
	 
ORDER BY
d.eb_demande_num DESC;

 