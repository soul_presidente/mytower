/*
select 

d.ref_transport as "Ref transport",

case when x_ec_mode_transport = 1 then 'Air' 
when x_ec_mode_transport = 2 then 'Sea'
when x_ec_mode_transport = 3 then 'Road'
when x_ec_mode_transport = 4 then 'Integrator'
else null
end as "Mode transport",

(select ic.libelle from work.eb_incoterm ic where ic.eb_incoterm_num = x_ec_incoterm limit 1) as "Incoterm",

case when d.x_ec_statut is null or  d.x_ec_statut = 11 then 'Waiting for pickup'
when d.x_ec_statut = 12 then 'Transport on going'
when d.x_ec_statut = 13 then 'Completed'
else null
end as "Statut"

from work.eb_demande d where d.x_ec_statut >= 11

order by d.eb_demande_num

*/