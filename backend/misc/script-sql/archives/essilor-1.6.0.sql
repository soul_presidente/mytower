-- Non executed 1.5.0 scripts on essilor env

--99
insert into work.ec_country (code,libelle,europ) values ('XK','Kosovo',true);

--99.2
DROP FUNCTION IF EXISTS work.is_userlabels_contains_demlabels;
CREATE OR REPLACE FUNCTION work.is_userlabels_contains_demlabels(catsuser text, catsdem jsonb)
RETURNS BOOLEAN
AS
$BODY$


	-- used algorithm
		-- step 1: get ids categorie to ignore
		-- step 2: remove categories to ignore from demande
		-- step 3: get all labels id of user's categories
		-- step 4: get all labels id of demande's categories
		-- compare


    -- step 1: get ids categorie to ignore
    with step1 as (
        with step11 as (
            SELECT string_agg(pp->>'ebCategorieNum', ',') val
            FROM jsonb_array_elements(catsuser::jsonb) pp
            WHERE pp?'isAll' and (pp->>'isAll')::boolean = true
        )
        select case when EXISTS (select * from step11) and (select * from step11) is not null then (select * from step11) else '' end
    )
    -- step 2: remove categories to ignore from demande
    , step2 as (

        SELECT json_agg(pp) val
        FROM jsonb_array_elements(catsdem::jsonb) pp
        WHERE NOT arraycontains(string_to_array((select * from step1), ','), string_to_array(pp->>'ebCategorieNum', ','))
    )
    -- step 3: get all labels id of user's categories
    , step3 as (
            with tmp as (
                SELECT pp val
                FROM jsonb_array_elements(catsuser::jsonb) pp
            )

						SELECT string_agg(pp->>'ebLabelNum', ',') val
						FROM tmp qq
						LEFT JOIN jsonb_array_elements(case when qq.val->>'labels' is null then '[]'::jsonb else qq.val->'labels' end) pp on true

    )
    -- step 4: get all labels id of demande's categories
    , step4 as (
            with tmp as (
                SELECT pp val
                FROM jsonb_array_elements((select * from step2)::jsonb) pp
            )

						SELECT string_agg(pp->>'ebLabelNum', ',') val
						FROM tmp qq
						LEFT JOIN jsonb_array_elements(case when qq.val->>'labels' is null then '[]'::jsonb else qq.val->'labels' end) pp on true
    )

    -- compare
    select arraycontains(
            string_to_array((select case when val is not null then val else ''::text end from step3), ','),
            string_to_array((select case when val is not null then val else ''::text end from step4), ',')
    );

$BODY$
LANGUAGE sql;

--100
update work.eb_tt_tracing set x_eb_incoterm= NULL;
update work.eb_demande set x_eb_incoterm_num = NULL;
delete from work.eb_incoterm;
insert into work.eb_incoterm (libelle  , x_eb_compagnie  )
select distinct (x_ec_incoterm_libelle) , x_eb_compagnie from work.eb_tt_schema_psl group by x_eb_compagnie , x_ec_incoterm_libelle;
update work.eb_incoterm set code = libelle ;


--100.2
UPDATE work.eb_qr_groupe SET compatibility_criteria = replace(compatibility_criteria::text, '"Are compatible"', '"ARE_COMPATIBLE"')::jsonb;
UPDATE work.eb_qr_groupe SET compatibility_criteria = replace(compatibility_criteria::text, '"Are equal"', '"ARE_EQUAL"')::jsonb;
UPDATE work.eb_qr_groupe SET compatibility_criteria = replace(compatibility_criteria::text, '"Are excluded"', '"ARE_EXCLUDED"')::jsonb;

UPDATE work.eb_qr_groupe SET compatibility_criteria = replace(compatibility_criteria::text, '"Start by"', '"START_BY"')::jsonb;
UPDATE work.eb_qr_groupe SET compatibility_criteria = replace(compatibility_criteria::text, '"Contains"', '"CONTAINS_VALUE"')::jsonb;
UPDATE work.eb_qr_groupe SET compatibility_criteria = replace(compatibility_criteria::text, '"Finish by"', '"FINISH_BY"')::jsonb;

--101
update work.eb_cost_center  c set x_eb_compagnie = (select x_eb_compagnie from work.eb_etablissement where eb_etablissement_num = c. x_eb_etablissement  ) ;
