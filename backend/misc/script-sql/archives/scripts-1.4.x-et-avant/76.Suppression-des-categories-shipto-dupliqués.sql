-- mise à jour des categories de shipto
update work.eb_label set x_eb_categorie = 57 where x_eb_categorie != 57;
-- suppression des categories shipto dupliqués
delete from work.eb_categorie where eb_categorie_num >=84 and eb_categorie_num <= 100;
