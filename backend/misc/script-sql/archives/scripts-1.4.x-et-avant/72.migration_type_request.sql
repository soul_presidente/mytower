--type  request standard
update work.eb_demande d
set 
x_eb_type_request= tr.eb_type_request_num,
type_request_libelle = tr.libelle,--||' - '||tr.reference,
tor_duration = tr.delai_chrono_pricing,
tor_type = tr.sens_chrono_pricing
from work.eb_type_request tr
where 
d.x_eb_compagnie = tr.x_eb_company and
d.x_ec_type_demande = 1 and
tr.libelle='Standard' ;

--type request urgent
update work.eb_demande d
set 
x_eb_type_request= tr.eb_type_request_num,
type_request_libelle = tr.libelle,--||' - '||tr.reference,
tor_duration = tr.delai_chrono_pricing,
tor_type = tr.sens_chrono_pricing
from work.eb_type_request tr
where 
d.x_eb_compagnie = tr.x_eb_company and
d.x_ec_type_demande = 2 and
tr.libelle='Urgent' ;