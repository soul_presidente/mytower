INSERT INTO "work"."eb_compagnie"("eb_compagnie_num", "code", "nom", "siren",  "compagnie_role", "groupe_name") 
VALUES (-2, 'EDI', 'EDI Comp', '0000000-2', NULL, 'EDI Group');

INSERT INTO "work"."eb_etablissement"("eb_etablissement_num", "adresse", "city", "code", "cout_prestation_douane", "date_creation", "email", "info", "logo", "nmbr_utilisateurs", "nom", "service", "siret", "site_web", "telephone", "x_eb_compagnie", "x_ec_country", "nic") 
VALUES (-2, 'France', 'Paris', 'EDI', NULL, '2018-01-08', 'edi@gmail.com', NULL, NULL, NULL, 'EDI Etab', NULL, '000000000000-2', NULL, '0000000000', -2, (select ec_country_num from work.ec_country where code ilike 'fr' limit 1) , '000-2');

INSERT INTO "work"."eb_user"("eb_user_num", "active_token", "admin", "adresse", "date_creation", "email", "enabled", "language", "date_activation", "list_categories", "locale", "nom", "password", "prenom", "reset_token", "role", "service", "status", "super_admin", "telephone",  "x_eb_compagnie", "x_eb_etablissement", "dtype") 
VALUES (-2, NULL, 't', NULL, '2018-03-19 17:03:49.909947', 'edi@gmail.com', 't', 'en', NULL, NULL, NULL, 'Edi', '$2a$10$E4EIVOvIPhaIq6g3IuWmIO1x0FwaFtaJR6.oL/0hwsUJZY2A/ZW3e', 'CPB', NULL, 3, NULL, 1, 't', NULL, -2, -2, 'EbUserChanel');
