/*Evol : uniformisation de la gestion des type de transport */

--INSERT INTO "work"."ec_type_transport"("ec_type_transport_num", "libelle") VALUES (1, 'Consolidation');
--INSERT INTO "work"."ec_type_transport"("ec_type_transport_num", "libelle") VALUES (2, 'Direct');
--INSERT INTO "work"."ec_type_transport"("ec_type_transport_num", "libelle") VALUES (3, 'LCL');
--INSERT INTO "work"."ec_type_transport"("ec_type_transport_num", "libelle") VALUES (4, 'FCL');
--INSERT INTO "work"."ec_type_transport"("ec_type_transport_num", "libelle") VALUES (5, 'Dedicated');


--INSERT INTO "work"."eb_shema_of_transport"("eb_shema_of_transport_num", "x_ec_mode_transport", "x_ec_type_transport") VALUES (1, 1, 1);
--INSERT INTO "work"."eb_shema_of_transport"("eb_shema_of_transport_num", "x_ec_mode_transport", "x_ec_type_transport") VALUES (2, 1, 2);
--INSERT INTO "work"."eb_shema_of_transport"("eb_shema_of_transport_num", "x_ec_mode_transport", "x_ec_type_transport") VALUES (3, 2, 3);
--INSERT INTO "work"."eb_shema_of_transport"("eb_shema_of_transport_num", "x_ec_mode_transport", "x_ec_type_transport") VALUES (4, 2, 4);
--INSERT INTO "work"."eb_shema_of_transport"("eb_shema_of_transport_num", "x_ec_mode_transport", "x_ec_type_transport") VALUES (5, 3, 1);
--INSERT INTO "work"."eb_shema_of_transport"("eb_shema_of_transport_num", "x_ec_mode_transport", "x_ec_type_transport") VALUES (6, 3, 5);


/* mode de transport air */
--UPDATE "work"."eb_demande" SET "x_ec_type_transport" = 1  where "x_ec_mode_transport"=1 and "x_ec_type_transport"= 1;
--UPDATE "work"."eb_demande" SET "x_ec_type_transport" = 2  where "x_ec_mode_transport"=1 and "x_ec_type_transport"= 2;

/* mode de transport sea */
--UPDATE "work"."eb_demande" SET "x_ec_type_transport" = 3  where "x_ec_mode_transport"=2 and "x_ec_type_transport"= 1;
--UPDATE "work"."eb_demande" SET "x_ec_type_transport" = 4  where "x_ec_mode_transport"=2 and "x_ec_type_transport"= 2;

/* mode de transport Road */
--UPDATE "work"."eb_demande" SET "x_ec_type_transport" = 3  where "x_ec_mode_transport"=3 and "x_ec_type_transport"= 1;
--UPDATE "work"."eb_demande" SET "x_ec_type_transport" = 5  where "x_ec_mode_transport"=3 and "x_ec_type_transport"= 2;
