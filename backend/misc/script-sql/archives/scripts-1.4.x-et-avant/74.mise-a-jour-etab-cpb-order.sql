-- Mise à jour des etablissements pour EbOrder
update work.eb_del_order
set x_eb_etablissement = work.eb_ship_to_matrix.x_eb_etablissement
from work.eb_ship_to_matrix
where eb_del_order.ship_to = eb_ship_to_matrix.ship_to
