/* Cr�er une sequence pour documents */
--CREATE SEQUENCE docs_sequence start 1

/* Cr�er mettre a jour la sequence avec le dernier ID present */
--SELECT setval('docs_sequence', (SELECT nextval('docs_sequence')))	

/* A deployer sur la prod */
--INSERT INTO "work"."eb_type_documents"("eb_type_documents_num", "eb_type_documents_code", "eb_type_documents_date_creation", "eb_type_documents_date_modification", "eb_type_documents_module", "eb_type_documents_nom", "eb_type_documents_ordre", "x_eb_company", "x_eb_user", "list_module_str") VALUES (nextval('docs_sequence'), 'Transport Document', NOW(), NOW(), '[1, 2, 5]', 'Transport Document', 1, NULL, NULL, '1|2|5|');

--INSERT INTO "work"."eb_type_documents"("eb_type_documents_num", "eb_type_documents_code", "eb_type_documents_date_creation", "eb_type_documents_date_modification", "eb_type_documents_module", "eb_type_documents_nom", "eb_type_documents_ordre", "x_eb_company", "x_eb_user", "list_module_str") VALUES (nextval('docs_sequence'), '', NOW(), NOW(), '[1, 2, 5]', 'Customs', 2, NULL, NULL, '1|2|5|');

--INSERT INTO "work"."eb_type_documents"("eb_type_documents_num", "eb_type_documents_code", "eb_type_documents_date_creation", "eb_type_documents_date_modification", "eb_type_documents_module", "eb_type_documents_nom", "eb_type_documents_ordre", "x_eb_company", "x_eb_user", "list_module_str") VALUES (nextval('docs_sequence'), '', NOW(), NOW(), '[1, 2, 5]', 'POD', 3, NULL, NULL, '1|2|5|');

--INSERT INTO "work"."eb_type_documents"("eb_type_documents_num", "eb_type_documents_code", "eb_type_documents_date_creation", "eb_type_documents_date_modification", "eb_type_documents_module", "eb_type_documents_nom", "eb_type_documents_ordre", "x_eb_company", "x_eb_user", "list_module_str") VALUES (nextval('docs_sequence'), '', NOW(), NOW(), '[1, 2, 5]', 'Others', 4, NULL, NULL, '1|2|5|');

--INSERT INTO "work"."eb_type_documents"("eb_type_documents_num", "eb_type_documents_code", "eb_type_documents_date_creation", "eb_type_documents_date_modification", "eb_type_documents_module", "eb_type_documents_nom", "eb_type_documents_ordre", "x_eb_company", "x_eb_user", "list_module_str") VALUES (nextval('docs_sequence'), '', NOW(), NOW(), '[1, 2, 5]', 'Commercial Invoice', 5, NULL, NULL, '1|2|5|');

--INSERT INTO "work"."eb_type_documents"("eb_type_documents_num", "eb_type_documents_code", "eb_type_documents_date_creation", "eb_type_documents_date_modification", "eb_type_documents_module", "eb_type_documents_nom", "eb_type_documents_ordre", "x_eb_company", "x_eb_user", "list_module_str") VALUES (nextval('docs_sequence'), '', NOW(), NOW(), '[1, 2, 5]', 'Supplier Invoice', 6, NULL, NULL, '1|2|5|');