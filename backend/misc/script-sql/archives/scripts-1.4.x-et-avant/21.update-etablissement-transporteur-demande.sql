UPDATE eb_demande d 
	SET x_eb_etablissement_transporteur = ( SELECT q.x_eb_etablissement FROM WORK.eb_demande_quote q WHERE q.status >= 4 AND q.x_eb_demande = d.eb_demande_num limit 1),
	x_eb_transporteur = ( SELECT q.x_transporteur FROM WORK.eb_demande_quote q WHERE q.status >= 4 AND q.x_eb_demande = d.eb_demande_num limit 1) 
WHERE
	x_eb_etablissement_transporteur IS NULL 
	OR x_eb_transporteur IS NULL 
	AND x_ec_statut >= 11;