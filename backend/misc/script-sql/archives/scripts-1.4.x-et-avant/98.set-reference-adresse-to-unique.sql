update work.eb_adresse set reference =reference ||'_' || eb_adresse_num
where  eb_adresse_num = any (
select unnest(array_agg(eb_adresse_num)) from work.eb_adresse where reference in (
       select reference
       from work.eb_adresse
       group by eb_adresse.reference, eb_adresse.x_eb_company
       having count(eb_adresse_num) > 1
   )
) and eb_adresse_num not in (
   select (array_agg(eb_adresse_num)::int[])[1] from work.eb_adresse where reference in (
   select reference
   from work.eb_adresse
   group by eb_adresse.reference, eb_adresse.x_eb_company
   having count(eb_adresse_num) > 1)  group by eb_adresse.x_eb_company,  reference order by reference
);