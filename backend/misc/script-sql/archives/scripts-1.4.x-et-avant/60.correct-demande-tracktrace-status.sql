with allpsl as (
	select distinct on (psl.x_eb_tt_tracing)
		psl.x_eb_tt_tracing, psl.code_psl, psl.date_actuelle 
	from work.eb_tt_psl_app psl
	where psl.validated = true 
	order by psl.x_eb_tt_tracing desc, psl.code_psl desc
)

update work.eb_tt_tracing ttr set code_psl_courant = (select psl.code_psl from allpsl psl where psl.x_eb_tt_tracing = ttr.eb_tt_tracing_num limit 1);





update work.eb_demande d set x_ec_statut = 
(
	with last_psl_code as (
		select psl.code_psl from work.eb_tt_psl_app psl where psl.x_eb_tt_tracing in (select eb_tt_tracing_num from work.eb_tt_tracing ttr 	where ttr.x_eb_demande = d.eb_demande_num) order by psl.code_psl desc limit 1
	), 
	count_tracing_per_demande as (
		select count(ttr.eb_tt_tracing_num) from work.eb_tt_tracing ttr 	where ttr.x_eb_demande = d.eb_demande_num
	),
	count_validated_psl as (
		select count(psl.code_psl) from work.eb_tt_psl_app psl where psl.x_eb_tt_tracing in (select eb_tt_tracing_num from work.eb_tt_tracing ttr 	where ttr.x_eb_demande = d.eb_demande_num) and psl.validated = true
	),
	count_validated_last_psl as (
		select count(psl.code_psl) from work.eb_tt_psl_app psl where psl.x_eb_tt_tracing in (select eb_tt_tracing_num from work.eb_tt_tracing ttr where ttr.x_eb_demande = d.eb_demande_num) and psl.validated = true and psl.code_psl in (select code_psl from last_psl_code)
	)
	select
		case 
			when (select * from count_validated_last_psl) = (select * from count_tracing_per_demande) then 13 
			when (select * from count_validated_psl) > 0 then 12
			else 11
		end
) where d.x_ec_statut >= 11

