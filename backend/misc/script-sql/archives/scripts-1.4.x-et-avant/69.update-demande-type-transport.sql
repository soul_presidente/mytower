-- insert type transports
INSERT INTO work."eb_type_transport"("eb_type_transport_num", "x_ec_mode_transport", "libelle") VALUES (1, 1, 'CONSOLIDATION');
INSERT INTO work."eb_type_transport"("eb_type_transport_num", "x_ec_mode_transport", "libelle") VALUES (2, 1, 'DIRECT');
INSERT INTO work."eb_type_transport"("eb_type_transport_num", "x_ec_mode_transport", "libelle") VALUES (3, 2, 'LCL');
INSERT INTO work."eb_type_transport"("eb_type_transport_num", "x_ec_mode_transport", "libelle") VALUES (4, 2, 'FCL');
INSERT INTO work."eb_type_transport"("eb_type_transport_num", "x_ec_mode_transport", "libelle") VALUES (5, 3, 'CONSOLIDATION');
INSERT INTO work."eb_type_transport"("eb_type_transport_num", "x_ec_mode_transport", "libelle") VALUES (6, 3, 'DEDICATED');

-- update for EbDemande

/* mode de transport air */
UPDATE "work"."eb_demande" SET "x_eb_type_transport" = 1  where "x_ec_mode_transport"=1 and "x_ec_type_transport"= 1;
UPDATE "work"."eb_demande" SET "x_eb_type_transport" = 2  where "x_ec_mode_transport"=1 and "x_ec_type_transport"= 2;

/* mode de transport sea */
UPDATE "work"."eb_demande" SET "x_eb_type_transport" = 3  where "x_ec_mode_transport"=2 and "x_ec_type_transport"= 1;
UPDATE "work"."eb_demande" SET "x_eb_type_transport" = 4  where "x_ec_mode_transport"=2 and "x_ec_type_transport"= 2;

/* mode de transport Road */
UPDATE "work"."eb_demande" SET "x_eb_type_transport" = 5  where "x_ec_mode_transport"=3 and "x_ec_type_transport"= 1;
UPDATE "work"."eb_demande" SET "x_eb_type_transport" = 6  where "x_ec_mode_transport"=3 and "x_ec_type_transport"= 2;


-- update for EbInvoice

/* mode de transport air */
UPDATE "work"."eb_invoice" SET "x_eb_type_transport" = 1  where "x_ec_mode_transport"=1 and "x_ec_type_transport"= 1;
UPDATE "work"."eb_invoice" SET "x_eb_type_transport" = 2  where "x_ec_mode_transport"=1 and "x_ec_type_transport"= 2;

/* mode de transport sea */
UPDATE "work"."eb_invoice" SET "x_eb_type_transport" = 3  where "x_ec_mode_transport"=2 and "x_ec_type_transport"= 1;
UPDATE "work"."eb_invoice" SET "x_eb_type_transport" = 4  where "x_ec_mode_transport"=2 and "x_ec_type_transport"= 2;

/* mode de transport Road */
UPDATE "work"."eb_invoice" SET "x_eb_type_transport" = 5  where "x_ec_mode_transport"=3 and "x_ec_type_transport"= 1;
UPDATE "work"."eb_invoice" SET "x_eb_type_transport" = 6  where "x_ec_mode_transport"=3 and "x_ec_type_transport"= 2;
