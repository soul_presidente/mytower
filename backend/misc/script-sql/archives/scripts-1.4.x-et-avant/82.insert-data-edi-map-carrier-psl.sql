INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (637, 'ABX','Direct Sales', 637);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (638, 'AEA','Direct Sales', 638);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (639, 'AGI','Direct Sales', 639);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (640, 'AIR','Direct Sales', 640);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (641, 'AIS','Direct Sales', 641);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (643, 'ALL','Direct Sales', 643);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (644, 'ARC','Direct Sales', 644);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (646, 'BAX','Direct Sales', 646);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (647, 'BGL','Direct Sales', 647);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (645, 'BOL','Export', 645);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (651, 'LDI','Direct Sales', 651);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (652, 'CAL','Direct Sales', 652);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (653, 'CEF','Direct Sales', 653);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (654, 'CIX','Direct Sales', 654);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (655, 'CLA','Direct Sales', 655);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (656, 'COM','Direct Sales', 656);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (657, 'COP','Direct Sales', 657);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (658, 'DAC','Direct Sales', 658);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (659, 'DAH','Direct Sales', 659);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (663, 'DRA','Direct Sales', 663);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (664, 'DEB','Direct Sales', 664);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (665, 'DFD','Direct Sales', 665);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (661, 'DHL','Direct Sales', 661);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (671, 'EGE','Direct Sales', 671);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (672, 'EXR','Direct Sales', 672);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (673, 'EXP','Direct Sales', 673);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (674, 'FAS','Direct Sales', 674);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (675, 'FCL','Direct Sales', 675);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (676, 'FED','Direct Sales', 676);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (677, 'TNT','Direct Sales', 677);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (678, 'FML','Direct Sales', 678);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (679, 'FCI','Direct Sales', 679);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (680, 'FHA','Direct Sales', 680);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (682, 'FMS','Direct Sales', 682);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (683, 'GAX','Direct Sales', 683);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (684, 'GEF','Direct Sales', 684);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (685, 'GOM','Direct Sales', 685);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (686, 'GOG','Direct Sales', 686);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (687, 'GRW','Direct Sales', 687);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (688, 'HAR','Direct Sales', 688);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (689, 'HEP','Direct Sales', 689);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (691, 'HES','Direct Sales', 691);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (692, 'IFC','Direct Sales', 692);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (693, 'JFH','Direct Sales', 693);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (694, 'HEJ','Direct Sales', 694);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (695, 'KUH','Direct Sales', 695);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (697, 'M&M','Direct Sales', 697);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (698, 'MAH','Direct Sales', 698);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (699, 'MEG','Direct Sales', 699);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (700, 'MZR','Direct Sales', 700);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (642, 'MOI','Direct Sales', 642);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (702, 'NTS','Direct Sales', 702);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (703, 'VAN','Direct Sales', 703);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (704, 'NIT','Direct Sales', 704);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (705, 'NPR','Direct Sales', 705);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (707, 'OTC','Direct Sales', 707);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (708, 'PAN','Direct Sales', 708);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (710, 'SFS','Direct Sales', 710);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (711, 'STP','Direct Sales', 711);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (712, 'SAG','Direct Sales', 712);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (714, 'SAM','Direct Sales', 714);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (715, 'SCH','Export', 715);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (721, 'DER','Direct Sales', 721);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (723, 'ZZZ','Direct Sales', 723);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (724, 'ZZX','Direct Sales', 724);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (725, 'SDV','Direct Sales', 725);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (727, 'FIE','Direct Sales', 727);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (728, 'SEU','Direct Sales', 728);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (729, 'SER','Direct Sales', 729);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (730, 'SOB','Direct Sales', 730);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (731, 'PIH','Direct Sales', 731);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (732, 'SOG','Direct Sales', 732);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (733, 'SON','Direct Sales', 733);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (734, 'WEB','Direct Sales', 734);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (735, 'SZY','Direct Sales', 735);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (736, 'TGD','Direct Sales', 736);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (738, 'TFM','Direct Sales', 738);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (739, 'TRR','Direct Sales', 739);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (740, 'TAO','Direct Sales', 740);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (742, 'TSA','Direct Sales', 742);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (743, 'TTR','Direct Sales', 743);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (744, 'UPS','Direct Sales', 744);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (745, 'WEG','Direct Sales', 745);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (746, 'WIL','Direct Sales', 746);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (747, 'XPF','Direct Sales', 747);								
INSERT INTO work.eb_edi_map_carrier_psl(eb_edi_map_carrier_psl_num,code_compagnie_carrier, code_schema_psl, x_eb_compagnie_carrier) VALUES (748, 'ZIE','Direct Sales', 748);								
