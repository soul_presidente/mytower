﻿INSERT INTO work.eb_compagnie(
	code, compagnie_role, nom, siren, x_eb_groupe, groupe_name)
	VALUES ('CPB', 1, 'Chanel Parfum Beauté', '478417710', null, 'Chanel');

	
INSERT INTO work.eb_etablissement(
	adresse, city, code, cout_prestation_douane, date_creation, email, info, logo, nic, nmbr_utilisateurs, nom, service, siret, site_web, telephone, x_eb_compagnie, x_ec_country, can_show_price, is_externe)
	VALUES ('CHANEL PARFUMS BEAUTE, 135 AV CHARLES DE GAULLE 92200 NEUILLY SUR SEINE', 'Paris', 'CPB', null, '07/11/2018', 'chanelpb@mytower.com', null, null, '00017', 15000, 
	'Siege', null, '47841771000017', 'www.chanel.com', '08 90 10 92 01', (select eb_compagnie_num from work.eb_compagnie where code = 'CPB' limit 1) , 2565, true, false);
	
INSERT INTO work.eb_user(
	admin, adresse, date_creation, email, enabled, language, date_activation, list_categories, locale, nom, password, prenom, reset_token, role, service, status, super_admin, telephone, x_eb_compagnie, x_eb_etablissement, access_rights, list_params_mail, list_params_mail_str, list_labels, referentadv, ship_to, username, dtype, mail_ref_adv)
	VALUES (true, 'CHANEL PARFUMS BEAUTE, 135 AV CHARLES DE GAULLE 92200 NEUILLY SUR SEINE', '07/11/2018', 'chanelpb@mytower.com', true, 'EN', '07/11/2018', null, null, 'Chanel', '$2a$10$BBOJ2Bp6m3B4Rthm8Q.Dy.n5J3u6QTjz3jD.ibXceXUoEBj3elyIS', 'Parfum Beauté', null, 3, 3, 1, true, '08 90 10 92 05',   
	(select eb_compagnie_num from work.eb_compagnie where code  = 'CPB' limit 1),  (select eb_etablissement_num  from work.eb_etablissement where code  = 'CPB' limit 1)   , null, null, null, null, null, null, null, 'EbUser', null);

/* update work.eb_del_order set  x_eb_compagnie = (select eb_compagnie_num from work.eb_compagnie where code = 'CPB' limit 1) ,  x_eb_etablissement = (select eb_etablissement_num  from work.eb_etablissement where code  = 'CPB' limit 1) ; 
update work.eb_del_livraison set  x_eb_compagnie = (select eb_compagnie_num from work.eb_compagnie where code = 'CPB' limit 1) ,  x_eb_etablissement = (select eb_etablissement_num  from work.eb_etablissement where code  = 'CPB' limit 1) ; 

INSERT INTO work.eb_categorie (libelle, code) VALUES ('SHIPTO', 'SHIPTO')*/