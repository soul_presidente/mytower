insert into work.ec_mime_type (mimetype,description) values ('application/msword','WORD');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.ms-excel','EXCEL');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.ms-powerpoint','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.openxmlformats-officedocument.wordprocessingml.document','WORD');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','EXCEL');
insert into work.ec_mime_type (mimetype,description) values ('application/pdf','PDF');
insert into work.ec_mime_type (mimetype,description) values ('text/html','HTML');
insert into work.ec_mime_type (mimetype,description) values ('text/plain','TEXT');
insert into work.ec_mime_type (mimetype,description) values ('image/jpeg','IMAGE');
insert into work.ec_mime_type (mimetype,description) values ('image/png','IMAGE');
insert into work.ec_mime_type  (mimetype,description) values ('image/gif','IMAGE');
insert into work.ec_mime_type (mimetype,description) values ('image/bmp','IMAGE');
insert into work.ec_mime_type (mimetype,description) values ('image/tiff','IMAGE');
insert into work.ec_mime_type (mimetype,description) values ('video/x-msvideo','VIDEO');
insert into work.ec_mime_type (mimetype,description) values ('text/csv','EXCEL');
insert into work.ec_mime_type (mimetype,description) values ('video/mpeg','IMAGE');
insert into work.ec_mime_type (mimetype,description) values ('video/webm','VIDEO');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.openxmlformats-officedocument.presentationml.presentation','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.openxmlformats-officedocument.wordprocessingml.document','WORD');
insert into work.ec_mime_type (mimetype,description) values ('image/x-icon','IMAGE');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.openxmlformats-officedocument.presentationml.presentation','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.openxmlformats-officedocument.presentationml.slideshow','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.ms-powerpoint.addin.macroEnabled.12','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.ms-powerpoint.presentation.macroEnabled.12','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.ms-powerpoint.template.macroEnabled.12','POWERPOINT');
insert into work.ec_mime_type (mimetype,description) values ('application/vnd.ms-powerpoint.slideshow.macroEnabled.12','POWERPOINT');
INSERT INTO work.ec_mime_type (mimetype,description) VALUES ('application/vnd.ms-outlook','OUTLOOK')