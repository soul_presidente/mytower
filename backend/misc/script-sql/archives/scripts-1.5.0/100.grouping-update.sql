UPDATE work.eb_qr_groupe SET compatibility_criteria = replace(compatibility_criteria::text, '"Are compatible"', '"ARE_COMPATIBLE"')::jsonb;
UPDATE work.eb_qr_groupe SET compatibility_criteria = replace(compatibility_criteria::text, '"Are equal"', '"ARE_EQUAL"')::jsonb;
UPDATE work.eb_qr_groupe SET compatibility_criteria = replace(compatibility_criteria::text, '"Are excluded"', '"ARE_EXCLUDED"')::jsonb;

UPDATE work.eb_qr_groupe SET compatibility_criteria = replace(compatibility_criteria::text, '"Start by"', '"START_BY"')::jsonb;
UPDATE work.eb_qr_groupe SET compatibility_criteria = replace(compatibility_criteria::text, '"Contains"', '"CONTAINS_VALUE"')::jsonb;
UPDATE work.eb_qr_groupe SET compatibility_criteria = replace(compatibility_criteria::text, '"Finish by"', '"FINISH_BY"')::jsonb;
