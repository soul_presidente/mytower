
DROP FUNCTION IF EXISTS work.is_userlabels_contains_demlabels;
CREATE OR REPLACE FUNCTION work.is_userlabels_contains_demlabels(catsuser text, catsdem jsonb)
RETURNS BOOLEAN
AS 
$BODY$
	
	
	-- used algorithm
		-- step 1: get ids categorie to ignore
		-- step 2: remove categories to ignore from demande
		-- step 3: get all labels id of user's categories
		-- step 4: get all labels id of demande's categories
		-- compare
			
			
    -- step 1: get ids categorie to ignore
    with step1 as (
        with step11 as (
            SELECT string_agg(pp->>'ebCategorieNum', ',') val
            FROM jsonb_array_elements(catsuser::jsonb) pp 
            WHERE pp?'isAll' and (pp->>'isAll')::boolean = true
        )
        select case when EXISTS (select * from step11) and (select * from step11) is not null then (select * from step11) else '' end
    )
    -- step 2: remove categories to ignore from demande
    , step2 as (
        
        SELECT json_agg(pp) val
        FROM jsonb_array_elements(catsdem::jsonb) pp 
        WHERE NOT arraycontains(string_to_array((select * from step1), ','), string_to_array(pp->>'ebCategorieNum', ','))
    )
    -- step 3: get all labels id of user's categories
    , step3 as (
            with tmp as (
                SELECT pp val
                FROM jsonb_array_elements(catsuser::jsonb) pp 
            )
                
						SELECT string_agg(pp->>'ebLabelNum', ',') val
						FROM tmp qq
						LEFT JOIN jsonb_array_elements(case when qq.val->>'labels' is null then '[]'::jsonb else qq.val->'labels' end) pp on true
                
    )
    -- step 4: get all labels id of demande's categories	
    , step4 as (
            with tmp as (
                SELECT pp val
                FROM jsonb_array_elements((select * from step2)::jsonb) pp 
            )
            
						SELECT string_agg(pp->>'ebLabelNum', ',') val
						FROM tmp qq
						LEFT JOIN jsonb_array_elements(case when qq.val->>'labels' is null then '[]'::jsonb else qq.val->'labels' end) pp on true  
    )
    
    -- compare
    select arraycontains(
            string_to_array((select case when val is not null then val else ''::text end from step3), ','), 
            string_to_array((select case when val is not null then val else ''::text end from step4), ',')
    );
			
$BODY$
LANGUAGE sql;
