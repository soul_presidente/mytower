update work.eb_adresse adresse
	set etablissement_adresse = ( 
		select json_build_array(row_to_json(t))
			from(
				select adr."x_eb_etablissement_adresse" as "ebEtablissementNum", etab."nom" as "nom"
				from work.eb_adresse adr inner join work.eb_etablissement etab
				on adr.x_eb_etablissement_adresse = etab.eb_etablissement_num
				where adr.eb_adresse_num = adresse.eb_adresse_num
			) t
		);

update work.eb_adresse 
set list_etablissements_num = ':'||x_eb_etablissement_adresse||':';