update work.eb_adresse adresse
set zones = ( 
	select json_build_array(row_to_json(t)) 
	from (
		select z."eb_zone_num" as "ebZoneNum", z."ref" as "ref", z."designation" as "designation"
		from work.eb_pl_zone z inner join work.eb_adresse adr
		on z.eb_zone_num = adr.x_eb_zone
		where adr.eb_adresse_num = adresse.eb_adresse_num
	) t 
);
							
UPDATE WORK.eb_adresse 
set x_default_zone = x_eb_zone;

update work.eb_adresse
set list_zones_num = ':'||x_eb_zone||':';