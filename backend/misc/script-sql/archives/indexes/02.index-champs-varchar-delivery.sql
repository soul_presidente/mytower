CREATE EXTENSION pg_trgm;

/* EbDelLivraison */
CREATE INDEX idx_eb_del_livraison_ship_to_hash ON work.eb_del_livraison USING hash (ship_to);
CREATE INDEX idx_eb_del_livraison_ship_to_gin ON work.eb_del_livraison USING gin (ship_to gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_ship_to_lowerhash ON work.eb_del_livraison USING hash (lower((ship_to)::text));
CREATE INDEX idx_eb_del_livraison_ship_to_lowergin ON work.eb_del_livraison USING gin (lower((ship_to)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_pickup_site_hash ON work.eb_del_livraison USING hash (pickup_site);
CREATE INDEX idx_eb_del_livraison_pickup_site_gin ON work.eb_del_livraison USING gin (pickup_site gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_pickup_site_lowerhash ON work.eb_del_livraison USING hash (lower((pickup_site)::text));
CREATE INDEX idx_eb_del_livraison_pickup_site_lowergin ON work.eb_del_livraison USING gin (lower((pickup_site)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_campaign_name_hash ON work.eb_del_livraison USING hash (campaign_name);
CREATE INDEX idx_eb_del_livraison_campaign_name_gin ON work.eb_del_livraison USING gin (campaign_name gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_campaign_name_lowerhash ON work.eb_del_livraison USING hash (lower((campaign_name)::text));
CREATE INDEX idx_eb_del_livraison_campaign_name_lowergin ON work.eb_del_livraison USING gin (lower((campaign_name)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_campaign_code_hash ON work.eb_del_livraison USING hash (campaign_code);
CREATE INDEX idx_eb_del_livraison_campaign_code_gin ON work.eb_del_livraison USING gin (campaign_code gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_campaign_code_lowerhash ON work.eb_del_livraison USING hash (lower((campaign_code)::text));
CREATE INDEX idx_eb_del_livraison_campaign_code_lowergin ON work.eb_del_livraison USING gin (lower((campaign_code)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_country_client_delivered_hash ON work.eb_del_livraison USING hash (country_client_delivered);
CREATE INDEX idx_eb_del_livraison_country_client_delivered_gin ON work.eb_del_livraison USING gin (country_client_delivered gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_country_client_delivered_lowerhash ON work.eb_del_livraison USING hash (lower((country_client_delivered)::text));
CREATE INDEX idx_eb_del_livraison_country_client_delivered_lowergin ON work.eb_del_livraison USING gin (lower((country_client_delivered)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_name_client_delivered_hash ON work.eb_del_livraison USING hash (name_client_delivered);
CREATE INDEX idx_eb_del_livraison_name_client_delivered_gin ON work.eb_del_livraison USING gin (name_client_delivered gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_name_client_delivered_lowerhash ON work.eb_del_livraison USING hash (lower((name_client_delivered)::text));
CREATE INDEX idx_eb_del_livraison_name_client_delivered_lowergin ON work.eb_del_livraison USING gin (lower((name_client_delivered)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_ref_client_delivered_hash ON work.eb_del_livraison USING hash (ref_client_delivered);
CREATE INDEX idx_eb_del_livraison_ref_client_delivered_gin ON work.eb_del_livraison USING gin (ref_client_delivered gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_ref_client_delivered_lowerhash ON work.eb_del_livraison USING hash (lower((ref_client_delivered)::text));
CREATE INDEX idx_eb_del_livraison_ref_client_delivered_lowergin ON work.eb_del_livraison USING gin (lower((ref_client_delivered)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_name_order_creator_hash ON work.eb_del_livraison USING hash (name_order_creator);
CREATE INDEX idx_eb_del_livraison_name_order_creator_gin ON work.eb_del_livraison USING gin (name_order_creator gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_name_order_creator_lowerhash ON work.eb_del_livraison USING hash (lower((name_order_creator)::text));
CREATE INDEX idx_eb_del_livraison_name_order_creator_lowergin ON work.eb_del_livraison USING gin (lower((name_order_creator)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_ref_delivery_hash ON work.eb_del_livraison USING hash (ref_delivery);
CREATE INDEX idx_eb_del_livraison_ref_delivery_gin ON work.eb_del_livraison USING gin (ref_delivery gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_ref_delivery_lowerhash ON work.eb_del_livraison USING hash (lower((ref_delivery)::text));
CREATE INDEX idx_eb_del_livraison_ref_delivery_lowergin ON work.eb_del_livraison USING gin (lower((ref_delivery)::text) gin_trgm_ops);


CREATE INDEX idx_eb_del_livraison_order_customer_name_hash ON work.eb_del_livraison USING hash (order_customer_name);
CREATE INDEX idx_eb_del_livraison_order_customer_name_gin ON work.eb_del_livraison USING gin (order_customer_name gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_order_customer_name_lowerhash ON work.eb_del_livraison USING hash (lower((order_customer_name)::text));
CREATE INDEX idx_eb_del_livraison_order_customer_name_lowergin ON work.eb_del_livraison USING gin (lower((order_customer_name)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_order_customer_code_hash ON work.eb_del_livraison USING hash (order_customer_code);
CREATE INDEX idx_eb_del_livraison_order_customer_code_gin ON work.eb_del_livraison USING gin (order_customer_code gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_order_customer_code_lowerhash ON work.eb_del_livraison USING hash (lower((order_customer_code)::text));
CREATE INDEX idx_eb_del_livraison_order_customer_code_lowergin ON work.eb_del_livraison USING gin (lower((order_customer_code)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_num_order_customer_hash ON work.eb_del_livraison USING hash (num_order_customer);
CREATE INDEX idx_eb_del_livraison_num_order_customer_gin ON work.eb_del_livraison USING gin (num_order_customer gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_num_order_customer_lowerhash ON work.eb_del_livraison USING hash (lower((num_order_customer)::text));
CREATE INDEX idx_eb_del_livraison_num_order_customer_lowergin ON work.eb_del_livraison USING gin (lower((num_order_customer)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_num_orderedi_hash ON work.eb_del_livraison USING hash (num_orderedi);
CREATE INDEX idx_eb_del_livraison_num_orderedi_gin ON work.eb_del_livraison USING gin (num_orderedi gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_num_orderedi_lowerhash ON work.eb_del_livraison USING hash (lower((num_orderedi)::text));
CREATE INDEX idx_eb_del_livraison_num_orderedi_lowergin ON work.eb_del_livraison USING gin (lower((num_orderedi)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_num_ordersap_hash ON work.eb_del_livraison USING hash (num_ordersap);
CREATE INDEX idx_eb_del_livraison_num_ordersap_gin ON work.eb_del_livraison USING gin (num_ordersap gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_num_ordersap_lowerhash ON work.eb_del_livraison USING hash (lower((num_ordersap)::text));
CREATE INDEX idx_eb_del_livraison_num_ordersap_lowergin ON work.eb_del_livraison USING gin (lower((num_ordersap)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_delivery_note_hash ON work.eb_del_livraison USING hash (delivery_note);
CREATE INDEX idx_eb_del_livraison_delivery_note_gin ON work.eb_del_livraison USING gin (delivery_note gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_delivery_note_lowerhash ON work.eb_del_livraison USING hash (lower((delivery_note)::text));
CREATE INDEX idx_eb_del_livraison_delivery_note_lowergin ON work.eb_del_livraison USING gin (lower((delivery_note)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_invoice_num_hash ON work.eb_del_livraison USING hash (invoice_num);
CREATE INDEX idx_eb_del_livraison_invoice_num_gin ON work.eb_del_livraison USING gin (invoice_num gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_invoice_num_lowerhash ON work.eb_del_livraison USING hash (lower((invoice_num)::text));
CREATE INDEX idx_eb_del_livraison_invoice_num_lowergin ON work.eb_del_livraison USING gin (lower((invoice_num)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_ref_transport_hash ON work.eb_del_livraison USING hash (ref_transport);
CREATE INDEX idx_eb_del_livraison_ref_transport_gin ON work.eb_del_livraison USING gin (ref_transport gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_ref_transport_lowerhash ON work.eb_del_livraison USING hash (lower((ref_transport)::text));
CREATE INDEX idx_eb_del_livraison_ref_transport_lowergin ON work.eb_del_livraison USING gin (lower((ref_transport)::text) gin_trgm_ops);

/* EbDelLivraisonLine */
create index idx_eb_del_livraison_line_ref_article_hash on work.eb_del_livraison_line using hash (ref_article);
create index idx_eb_del_livraison_line_ref_article_gin on work.eb_del_livraison_line using hash (ref_article gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_line_ref_article_lowerhash ON work.eb_del_livraison_line USING hash (lower((ref_article)::text));
CREATE INDEX idx_eb_del_livraison_line_ref_article_lowergin ON work.eb_del_livraison_line USING gin (lower((ref_article)::text) gin_trgm_ops);