CREATE EXTENSION pg_trgm;

CREATE INDEX idx_eb_del_order_campaign_code_hash ON work.eb_del_order USING hash (campaign_code);
CREATE INDEX idx_eb_del_order_campaign_code_gin ON work.eb_del_order USING gin (campaign_code gin_trgm_ops);
CREATE INDEX idx_eb_del_order_campaign_code_lowerhash ON work.eb_del_order USING hash (lower((campaign_code)::text));
CREATE INDEX idx_eb_del_order_campaign_code_lowergin ON work.eb_del_order USING gin (lower((campaign_code)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_order_num_order_customer_hash ON work.eb_del_order USING hash (num_order_customer);
CREATE INDEX idx_eb_del_order_num_order_customer_gin ON work.eb_del_order USING gin (num_order_customer gin_trgm_ops);
CREATE INDEX idx_eb_del_order_num_order_customer_lowerhash ON work.eb_del_order USING hash (lower((num_order_customer)::text));
CREATE INDEX idx_eb_del_order_num_order_customer_lowergin ON work.eb_del_order USING gin (lower((num_order_customer)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_order_num_orderedi_hash ON work.eb_del_order USING hash (num_orderedi);
CREATE INDEX idx_eb_del_order_num_orderedi_gin ON work.eb_del_order USING gin (num_orderedi gin_trgm_ops);
CREATE INDEX idx_eb_del_order_num_orderedi_lowerhash ON work.eb_del_order USING hash (lower((num_orderedi)::text));
CREATE INDEX idx_eb_del_order_num_orderedi_lowergin ON work.eb_del_order USING gin (lower((num_orderedi)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_order_num_ordersap_hash ON work.eb_del_order USING hash (num_ordersap);
CREATE INDEX idx_eb_del_order_num_ordersap_gin ON work.eb_del_order USING gin (num_ordersap gin_trgm_ops);
CREATE INDEX idx_eb_del_order_num_ordersap_lowerhash ON work.eb_del_order USING hash (lower((num_ordersap)::text));
CREATE INDEX idx_eb_del_order_num_ordersap_lowergin ON work.eb_del_order USING gin (lower((num_ordersap)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_order_purchase_order_type_hash ON work.eb_del_order USING hash (purchase_order_type);
CREATE INDEX idx_eb_del_order_purchase_order_type_gin ON work.eb_del_order USING gin (purchase_order_type gin_trgm_ops);
CREATE INDEX idx_eb_del_order_purchase_order_type_lowerhash ON work.eb_del_order USING hash (lower((purchase_order_type)::text));
CREATE INDEX idx_eb_del_order_purchase_order_type_lowergin ON work.eb_del_order USING gin (lower((purchase_order_type)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_order_name_client_delivered_hash ON work.eb_del_order USING hash (name_client_delivered);
CREATE INDEX idx_eb_del_order_name_client_delivered_gin ON work.eb_del_order USING gin (name_client_delivered gin_trgm_ops);
CREATE INDEX idx_eb_del_order_name_client_delivered_lowerhash ON work.eb_del_order USING hash (lower((name_client_delivered)::text));
CREATE INDEX idx_eb_del_order_name_client_delivered_lowergin ON work.eb_del_order USING gin (lower((name_client_delivered)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_order_ref_client_delivered_hash ON work.eb_del_order USING hash (ref_client_delivered);
CREATE INDEX idx_eb_del_order_ref_client_delivered_gin ON work.eb_del_order USING gin (ref_client_delivered gin_trgm_ops);
CREATE INDEX idx_eb_del_order_ref_client_delivered_lowerhash ON work.eb_del_order USING hash (lower((ref_client_delivered)::text));
CREATE INDEX idx_eb_del_order_ref_client_delivered_lowergin ON work.eb_del_order USING gin (lower((ref_client_delivered)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_order_list_labels_hash ON work.eb_del_order USING hash (list_labels);
CREATE INDEX idx_eb_del_order_list_labels_gin ON work.eb_del_order USING gin (list_labels gin_trgm_ops);
CREATE INDEX idx_eb_del_order_list_labels_lowerhash ON work.eb_del_order USING hash (lower((list_labels)::text));
CREATE INDEX idx_eb_del_order_list_labels_lowergin ON work.eb_del_order USING gin (lower((list_labels)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_order_ship_to_hash ON work.eb_del_order USING hash (ship_to);
CREATE INDEX idx_eb_del_order_ship_to_gin ON work.eb_del_order USING gin (ship_to gin_trgm_ops);
CREATE INDEX idx_eb_del_order_ship_to_lowerhash ON work.eb_del_order USING hash (lower((ship_to)::text));
CREATE INDEX idx_eb_del_order_ship_to_lowergin ON work.eb_del_order USING gin (lower((ship_to)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_order_order_customer_code_hash ON work.eb_del_order USING hash (order_customer_code);
CREATE INDEX idx_eb_del_order_order_customer_code_gin ON work.eb_del_order USING gin (order_customer_code gin_trgm_ops);
CREATE INDEX idx_eb_del_order_order_customer_code_lowerhash ON work.eb_del_order USING hash (lower((order_customer_code)::text));
CREATE INDEX idx_eb_del_order_order_customer_code_lowergin ON work.eb_del_order USING gin (lower((order_customer_code)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_order_order_customer_name_hash ON work.eb_del_order USING hash (order_customer_name);
CREATE INDEX idx_eb_del_order_order_customer_name_gin ON work.eb_del_order USING gin (order_customer_name gin_trgm_ops);
CREATE INDEX idx_eb_del_order_order_customer_name_lowerhash ON work.eb_del_order USING hash (lower((order_customer_name)::text));
CREATE INDEX idx_eb_del_order_order_customer_name_lowergin ON work.eb_del_order USING gin (lower((order_customer_name)::text) gin_trgm_ops);

/* ORDER LINE */
CREATE INDEX idx_eb_del_order_line_article_name_hash ON work.eb_del_order_line USING hash (article_name);
CREATE INDEX idx_eb_del_order_line_article_name_gin ON work.eb_del_order_line USING gin (article_name gin_trgm_ops);
CREATE INDEX idx_eb_del_order_line_article_name_lowerhash ON work.eb_del_order_line USING hash (lower((article_name)::text));
CREATE INDEX idx_eb_del_order_line_article_name_lowergin ON work.eb_del_order_line USING gin (lower((article_name)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_order_line_ref_article_hash ON work.eb_del_order_line USING hash (ref_article);
CREATE INDEX idx_eb_del_order_line_ref_article_gin ON work.eb_del_order_line USING gin (ref_article gin_trgm_ops);
CREATE INDEX idx_eb_del_order_line_ref_article_lowerhash ON work.eb_del_order_line USING hash (lower((ref_article)::text));
CREATE INDEX idx_eb_del_order_line_ref_article_lowergin ON work.eb_del_order_line USING gin (lower((ref_article)::text) gin_trgm_ops);

/* LIVRAISON LINE */
CREATE INDEX idx_eb_del_livraison_line_article_name_hash ON work.eb_del_livraison_line USING hash (article_name);
CREATE INDEX idx_eb_del_livraison_line_article_name_gin ON work.eb_del_livraison_line USING gin (article_name gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_line_article_name_lowerhash ON work.eb_del_livraison_line USING hash (lower((article_name)::text));
CREATE INDEX idx_eb_del_livraison_line_article_name_lowergin ON work.eb_del_livraison_line USING gin (lower((article_name)::text) gin_trgm_ops);

CREATE INDEX idx_eb_del_livraison_line_ref_article_hash ON work.eb_del_livraison_line USING hash (ref_article);
CREATE INDEX idx_eb_del_livraison_line_ref_article_gin ON work.eb_del_livraison_line USING gin (ref_article gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_line_ref_article_lowerhash ON work.eb_del_livraison_line USING hash (lower((ref_article)::text));
CREATE INDEX idx_eb_del_livraison_line_ref_article_lowergin ON work.eb_del_livraison_line USING gin (lower((ref_article)::text) gin_trgm_ops);
