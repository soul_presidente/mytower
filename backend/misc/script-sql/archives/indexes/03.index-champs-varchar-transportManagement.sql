CREATE EXTENSION pg_trgm;

/* EbDemande */
CREATE INDEX idx_eb_demande_carrier_uniq_ref_num_gin ON work.eb_demande USING gin (carrier_uniq_ref_num gin_trgm_ops);
CREATE INDEX idx_eb_demande_carrier_uniq_ref_num_lowergin ON work.eb_demande USING gin (lower((carrier_uniq_ref_num)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_carrier_uniq_ref_num_hash ON work.eb_demande USING hash (carrier_uniq_ref_num);
CREATE INDEX idx_eb_demande_carrier_uniq_ref_num_lowerhash ON work.eb_demande USING hash (lower((carrier_uniq_ref_num)::text));

CREATE INDEX idx_eb_demande_customer_reference_gin ON work.eb_demande USING gin (customer_reference gin_trgm_ops);
CREATE INDEX idx_eb_demande_customer_reference_lowergin ON work.eb_demande USING gin (lower((customer_reference)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_customer_reference_hash ON work.eb_demande USING hash (customer_reference);
CREATE INDEX idx_eb_demande_customer_reference_lowerhash ON work.eb_demande USING hash (lower((customer_reference)::text));

CREATE INDEX idx_eb_demande_libelle_dest_city_gin ON work.eb_demande USING gin (libelle_dest_city gin_trgm_ops);
CREATE INDEX idx_eb_demande_libelle_dest_city_lowergin ON work.eb_demande USING gin (lower((libelle_dest_city)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_libelle_dest_city_hash ON work.eb_demande USING hash (libelle_dest_city);
CREATE INDEX idx_eb_demande_libelle_dest_city_lowerhash ON work.eb_demande USING hash (lower((libelle_dest_city)::text));

CREATE INDEX idx_eb_demande_libelle_dest_country_gin ON work.eb_demande USING gin (libelle_dest_country gin_trgm_ops);
CREATE INDEX idx_eb_demande_libelle_dest_country_lowergin ON work.eb_demande USING gin (lower((libelle_dest_country)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_libelle_dest_country_hash ON work.eb_demande USING hash (libelle_dest_country);
CREATE INDEX idx_eb_demande_libelle_dest_country_lowerhash ON work.eb_demande USING hash (lower((libelle_dest_country)::text));

CREATE INDEX idx_eb_demande_libelle_last_psl_gin ON work.eb_demande USING gin (libelle_last_psl gin_trgm_ops);
CREATE INDEX idx_eb_demande_libelle_last_psl_lowergin ON work.eb_demande USING gin (lower((libelle_last_psl)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_libelle_last_psl_hash ON work.eb_demande USING hash (libelle_last_psl);
CREATE INDEX idx_eb_demande_libelle_last_psl_lowerhash ON work.eb_demande USING hash (lower((libelle_last_psl)::text));

CREATE INDEX idx_eb_demande_libelle_origin_city_gin ON work.eb_demande USING gin (libelle_origin_city gin_trgm_ops);
CREATE INDEX idx_eb_demande_libelle_origin_city_lowergin ON work.eb_demande USING gin (lower((libelle_origin_city)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_libelle_origin_city_hash ON work.eb_demande USING hash (libelle_origin_city);
CREATE INDEX idx_eb_demande_libelle_origin_city_lowerhash ON work.eb_demande USING hash (lower((libelle_origin_city)::text));

CREATE INDEX idx_eb_demande_libelle_origin_country_gin ON work.eb_demande USING gin (libelle_origin_country gin_trgm_ops);
CREATE INDEX idx_eb_demande_libelle_origin_country_lowergin ON work.eb_demande USING gin (lower((libelle_origin_country)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_libelle_origin_country_hash ON work.eb_demande USING hash (libelle_origin_country);
CREATE INDEX idx_eb_demande_libelle_origin_country_lowerhash ON work.eb_demande USING hash (lower((libelle_origin_country)::text));

CREATE INDEX idx_eb_demande_num_awb_bol_gin ON work.eb_demande USING gin (num_awb_bol gin_trgm_ops);
CREATE INDEX idx_eb_demande_num_awb_bol_lowergin ON work.eb_demande USING gin (lower((num_awb_bol)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_num_awb_bol_hash ON work.eb_demande USING hash (num_awb_bol);
CREATE INDEX idx_eb_demande_num_awb_bol_lowerhash ON work.eb_demande USING hash (lower((num_awb_bol)::text));

CREATE INDEX idx_eb_demande_order_number_gin ON work.eb_demande USING gin (order_number gin_trgm_ops);
CREATE INDEX idx_eb_demande_order_number_lowergin ON work.eb_demande USING gin (lower((order_number)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_order_number_hash ON work.eb_demande USING hash (order_number);
CREATE INDEX idx_eb_demande_order_number_lowerhash ON work.eb_demande USING hash (lower((order_number)::text));

CREATE INDEX idx_eb_demande_part_number_gin ON work.eb_demande USING gin (part_number gin_trgm_ops);
CREATE INDEX idx_eb_demande_part_number_lowergin ON work.eb_demande USING gin (lower((part_number)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_part_number_hash ON work.eb_demande USING hash (part_number);
CREATE INDEX idx_eb_demande_part_number_lowerhash ON work.eb_demande USING hash (lower((part_number)::text));

CREATE INDEX idx_eb_demande_serial_number_gin ON work.eb_demande USING gin (serial_number gin_trgm_ops);
CREATE INDEX idx_eb_demande_serial_number_lowergin ON work.eb_demande USING gin (lower((serial_number)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_serial_number_hash ON work.eb_demande USING hash (serial_number);
CREATE INDEX idx_eb_demande_serial_number_lowerhash ON work.eb_demande USING hash (lower((serial_number)::text));

CREATE INDEX idx_eb_demande_shipping_type_gin ON work.eb_demande USING gin (shipping_type gin_trgm_ops);
CREATE INDEX idx_eb_demande_shipping_type_lowergin ON work.eb_demande USING gin (lower((shipping_type)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_shipping_type_hash ON work.eb_demande USING hash (shipping_type);
CREATE INDEX idx_eb_demande_shipping_type_lowerhash ON work.eb_demande USING hash (lower((shipping_type)::text));

CREATE INDEX idx_eb_demande_type_request_libelle_gin ON work.eb_demande USING gin (type_request_libelle gin_trgm_ops);
CREATE INDEX idx_eb_demande_type_request_libelle_lowergin ON work.eb_demande USING gin (lower((type_request_libelle)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_type_request_libelle_hash ON work.eb_demande USING hash (type_request_libelle);
CREATE INDEX idx_eb_demande_type_request_libelle_lowerhash ON work.eb_demande USING hash (lower((type_request_libelle)::text));

CREATE INDEX idx_eb_demande_units_reference_gin ON work.eb_demande USING gin (units_reference gin_trgm_ops);
CREATE INDEX idx_eb_demande_units_reference_lowergin ON work.eb_demande USING gin (lower((units_reference)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_units_reference_hash ON work.eb_demande USING hash (units_reference);
CREATE INDEX idx_eb_demande_units_reference_lowerhash ON work.eb_demande USING hash (lower((units_reference)::text));

CREATE INDEX idx_eb_demande_x_ec_incoterm_libelle_gin ON work.eb_demande USING gin (x_ec_incoterm_libelle gin_trgm_ops);
CREATE INDEX idx_eb_demande_x_ec_incoterm_libelle_lowergin ON work.eb_demande USING gin (lower((x_ec_incoterm_libelle)::text) gin_trgm_ops);
CREATE INDEX idx_eb_demande_x_ec_incoterm_libelle_hash ON work.eb_demande USING hash (x_ec_incoterm_libelle);
CREATE INDEX idx_eb_demande_x_ec_incoterm_libelle_lowerhash ON work.eb_demande USING hash (lower((x_ec_incoterm_libelle)::text));

/* EbDelLivraison */
CREATE INDEX idx_eb_del_livraison_cross_dock_gin ON work.eb_del_livraison USING gin (cross_dock gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_cross_dock_lowergin ON work.eb_del_livraison USING gin (lower((cross_dock)::text) gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_cross_dock_hash ON work.eb_del_livraison USING hash (cross_dock);
CREATE INDEX idx_eb_del_livraison_cross_dock_lowerhash ON work.eb_del_livraison USING hash (lower((cross_dock)::text));

CREATE INDEX idx_eb_del_livraison_path_to_bl_gin ON work.eb_del_livraison USING gin (path_to_bl gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_path_to_bl_lowergin ON work.eb_del_livraison USING gin (lower((path_to_bl)::text) gin_trgm_ops);
CREATE INDEX idx_eb_del_livraison_path_to_bl_hash ON work.eb_del_livraison USING hash (path_to_bl);
CREATE INDEX idx_eb_del_livraison_path_to_bl_lowerhash ON work.eb_del_livraison USING hash (lower((path_to_bl)::text));

/* Ebmarchandise */
CREATE INDEX idx_eb_marchandise_unit_reference_gin ON work.eb_marchandise USING gin (unit_reference gin_trgm_ops);
CREATE INDEX idx_eb_marchandise_unit_reference_lowergin ON work.eb_marchandise USING gin (lower((unit_reference)::text) gin_trgm_ops);
CREATE INDEX idx_eb_marchandise_unit_reference_hash ON work.eb_marchandise USING hash (unit_reference);
CREATE INDEX idx_eb_marchandise_unit_reference_lowerhash ON work.eb_marchandise USING hash (lower((unit_reference)::text));

CREATE INDEX idx_eb_marchandise_lot_gin ON work.eb_marchandise USING gin (lot gin_trgm_ops);
CREATE INDEX idx_eb_marchandise_lot_lowergin ON work.eb_marchandise USING gin (lower((lot)::text) gin_trgm_ops);
CREATE INDEX idx_eb_marchandise_lot_hash ON work.eb_marchandise USING hash (lot);
CREATE INDEX idx_eb_marchandise_lot_lowerhash ON work.eb_marchandise USING hash (lower((lot)::text));

