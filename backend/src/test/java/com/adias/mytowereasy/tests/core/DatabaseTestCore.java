package com.adias.mytowereasy.tests.core;

import javax.sql.DataSource;

import org.junit.ClassRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.containers.PostgreSQLContainer;

import com.ninja_squad.dbsetup.DbSetupTracker;

import com.adias.mytowereasy.tests.container.MyTowerTestKafkaContainer;
import com.adias.mytowereasy.tests.container.MyTowerTestPostgresqlContainer;


public abstract class DatabaseTestCore extends TestCore {
    protected final static DbSetupTracker dbSetupTracker = new DbSetupTracker();

    @Autowired
    protected DataSource dataSource;

    @ClassRule
    public static PostgreSQLContainer<?> postgreSQLContainer = MyTowerTestPostgresqlContainer.getInstance();

    @ClassRule
    public static KafkaContainer kafkaContainer = MyTowerTestKafkaContainer.getInstance();
}
