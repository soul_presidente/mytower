package com.adias.mytowereasy.tests.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class DateTestUtils {
    /**
     * WARNING : Only use for unit test fake data creation
     * We catched exception in this method only for tests, don't use in
     * production
     * 
     * @param format
     *            Date format
     * @param date
     *            Date to parse
     * @return Parsed date object
     */
    public static Date parseDate(String format, String date) {

        try {
            return new SimpleDateFormat(format).parse(date);
        } catch (ParseException e) {
            System.err.println("Error parsing date for unit test");
            e.printStackTrace();
            return null;
        }

    }
}
