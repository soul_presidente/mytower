package com.adias.mytowereasy.tests.db;

import static com.adias.mytowereasy.tests.utils.DateTestUtils.parseDate;
import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;

import com.ninja_squad.dbsetup.operation.Operation;


public class UserDbOperations {
    public static final Operation INSERT_CORE_USER = sequenceOf(
        // Company MyTower Operations
        insertInto("work.eb_compagnie")
            .columns("eb_compagnie_num", "code", "compagnie_role", "nom", "siren", "date_creation_sys", "date_maj_sys")
            .values(
                -1,
                "MTO",
                "3",
                "MyTower Operations",
                "000000000",
                parseDate("yyyy-MM-dd HH:mm:ss", "2019-06-21 16:34:29"),
                parseDate("yyyy-MM-dd HH:mm:ss", "2019-06-21 16:34:29"))
            .build(),

        // Etablissement MyTower Operations
        insertInto("work.eb_etablissement")
            .columns(
                "eb_etablissement_num",
                "adresse",
                "city",
                "code",
                "date_creation",
                "email",
                "nic",
                "nom",
                "siret",
                "telephone",
                "x_eb_compagnie",
                "x_ec_country",
                "can_show_price",
                "role",
                "date_creation_sys",
                "date_maj_sys")
            .values(
                -1,
                "France",
                "Paris",
                "MTO",
                parseDate("yyyy-MM-dd", "2018-01-08"),
                "mytower2018+ct@gmail.com",
                "00000",
                "MyTower Operations",
                "00000000000000",
                "0000000000",
                -1,
                158,
                true,
                3,
                parseDate("yyyy-MM-dd HH:mm:ss", "2019-06-21 16:34:29"),
                parseDate("yyyy-MM-dd HH:mm:ss", "2019-06-21 16:34:29"))
            .build(),

        // MyTower Operations user
        insertInto("work.eb_user")
            .columns(
                "eb_user_num",
                "admin",
                "date_creation",
                "email",
                "enabled",
                "language",
                "nom",
                "password",
                "prenom",
                "role",
                "status",
                "super_admin",
                "x_eb_compagnie",
                "x_eb_etablissement",
                "username",
                "dtype")
            .values(
                -1,
                true,
                parseDate("yyyy-MM-dd HH:mm:ss", "2018-03-19 17:03:49"),
                "mytower2018+ct@gmail.com",
                true,
                "en",
                "My",
                "$2a$10$L4QMbnrHEuDdIgmfHtx/se/LoaRksgbRsPcwWMfSiDYfqCERyO6rG",
                "Tower",
                3,
                1,
                true,
                -1,
                -1,
                "mytower2018+ct@gmail.com",
                "EbUser")
            .build(),

        // MyTower Operation User Access
        insertInto("work.eb_user_module")
            .columns("eb_user_num", "ec_module_num", "access_right", "x_eb_user", "x_ec_module").values(-1, 1, 0, -1, 1)
            .values(-1, 2, 0, -1, 2).values(-1, 3, 0, -1, 3).values(-1, 4, 0, -1, 4).values(-1, 5, 0, -1, 5)
            .values(-1, 7, 0, -1, 7).values(-1, 8, 0, -1, 8).values(-1, 9, 0, -1, 9).values(-1, 18, 0, -1, 18)
            .values(-1, 19, 0, -1, 19).values(-1, 10, 0, -1, 10).build());
}
