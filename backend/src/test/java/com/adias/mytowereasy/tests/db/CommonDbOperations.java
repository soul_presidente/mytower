package com.adias.mytowereasy.tests.db;

import static com.ninja_squad.dbsetup.Operations.deleteAllFrom;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;

import com.ninja_squad.dbsetup.operation.Operation;


public class CommonDbOperations {
    /**
     * Delete all settings (user, company, ...) and operationals (pricing,
     * orders, ...)
     */
    public static final Operation DELETE_SETTINGS_AND_OPERATIONALS = deleteAllFrom(
        "work.eb_del_livraison_line",
        "work.eb_del_livraison",
        "work.eb_del_order_line",
        "work.eb_del_order",
        "work.eb_party",
        "work.eb_user_module",
        "work.eb_user",
        "work.eb_etablissement",
        "work.eb_compagnie");

    /**
     * Delete all repository data (Ec. objects)
     */
    public static final Operation DELETE_REPOSITORY = deleteAllFrom(
        "work.ec_currency",
        "work.ec_country",
        "work.ec_operational_grouping",
        "work.ec_region",
        "work.ec_module");

    /**
     * Insert repository data (Ec. objects)
     */
    public static final Operation INSERT_REPOSITORY = sequenceOf(
        RepositoryDbOperations.INSERT_CURRENCY,
        RepositoryDbOperations.INSERT_OPERATIONAL_GROUPING,
        RepositoryDbOperations.INSERT_REGION,
        RepositoryDbOperations.INSERT_COUNTRY,
        RepositoryDbOperations.INSERT_MODULE);

    /**
     * Operation that should be runned before every test
     */
    public static final Operation EACH_TESTS_OPERATION = sequenceOf(
        DELETE_SETTINGS_AND_OPERATIONALS,
        DELETE_REPOSITORY,
        INSERT_REPOSITORY,
        UserDbOperations.INSERT_CORE_USER);
}
