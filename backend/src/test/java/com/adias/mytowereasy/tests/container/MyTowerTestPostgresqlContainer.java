package com.adias.mytowereasy.tests.container;

import org.testcontainers.containers.PostgreSQLContainer;


public class MyTowerTestPostgresqlContainer extends PostgreSQLContainer<MyTowerTestPostgresqlContainer> {
    private static final String IMAGE_VERSION = "postgres:10.11";
    private static MyTowerTestPostgresqlContainer container;

    private MyTowerTestPostgresqlContainer() {
        super(IMAGE_VERSION);
    }

    public static MyTowerTestPostgresqlContainer getInstance() {

        if (container == null) {
            container = new MyTowerTestPostgresqlContainer();
        }

        return container;
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("DB_URL", container.getJdbcUrl());
        System.setProperty("DB_USERNAME", container.getUsername());
        System.setProperty("DB_PASSWORD", container.getPassword());
    }

    @Override
    public void stop() {
        // do nothing, JVM handles shut down
    }
}
