package com.adias.mytowereasy.tests.db;

import static com.ninja_squad.dbsetup.Operations.insertInto;

import com.ninja_squad.dbsetup.operation.Operation;


public class RepositoryDbOperations {
    public static Operation INSERT_MODULE = insertInto("work.ec_module")
        .columns("ec_module_num", "libelle").values(1, "PRICING").values(2, "TM").values(3, "QM")
        .values(4, "COMPLIANCE_MATRIX").values(5, "TT").values(7, "FA").values(8, "ANALYTICS").values(9, "CUSTOMS")
        .values(18, "DELIVERY").values(19, "COMPTA_MATIERE").values(10, "ORDER").build();

    public static Operation INSERT_CURRENCY = insertInto("work.ec_currency")
        .columns("ec_currency_num", "code", "libelle").values(44l, "EUR", "Euro")
        .values(148l, "USD", "United States dollar").values(87l, "MAD", "Moroccan dirham").build();

    public static Operation INSERT_OPERATIONAL_GROUPING = insertInto("work.ec_operational_grouping")
        .columns("ec_operational_grouping_num", "libelle").values(5l, "France").values(26l, "Italy").build();

    public static Operation INSERT_REGION = insertInto("work.ec_region")
        .columns("ec_region_num", "libelle").values(2l, "EMEA").values(3l, "AsiaPacific").values(4l, "North America")
        .values(5l, "Latin America").build();

    public static Operation INSERT_COUNTRY = insertInto("work.ec_country")
        .columns(
            "ec_country_num",
            "code",
            "commercial_mandatory",
            "europ",
            "libelle",
            "x_ec_operational_grouping",
            "x_ec_region",
            "latitude",
            "longitude")
        .values(158l, "FR", false, true, "France", 5l, 2l, 46.227638, 2.213749)
        .values(157l, "IT", false, true, "Italy", 26l, 2l, 41.87194, 12.56738).build();
}
