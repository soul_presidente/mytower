package com.adias.mytowereasy.tests.utils;

import io.restassured.RestAssured;
import io.restassured.config.DecoderConfig;
import io.restassured.config.EncoderConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.parsing.Parser;


public class RestAssuredUtils {
    public static void applyRestAssuredConfig(int port) {
        RestAssured.port = port;
        RestAssured.defaultParser = Parser.JSON;

        RestAssuredConfig restAssuredConfig = new RestAssuredConfig();
        restAssuredConfig.decoderConfig(new DecoderConfig("UTF-8"));
        restAssuredConfig.encoderConfig(new EncoderConfig("UTF-8", "UTF-8"));
        RestAssured.config = restAssuredConfig;
    }
}
