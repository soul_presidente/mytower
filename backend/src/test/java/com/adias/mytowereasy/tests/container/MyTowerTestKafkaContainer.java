package com.adias.mytowereasy.tests.container;

import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.utility.DockerImageName;


public class MyTowerTestKafkaContainer extends KafkaContainer {
    private static final String IMAGE_VERSION = "confluentinc/cp-kafka:5.4.3";
    private static MyTowerTestKafkaContainer container;

    private MyTowerTestKafkaContainer() {
        super(DockerImageName.parse(IMAGE_VERSION));
    }

    public static MyTowerTestKafkaContainer getInstance() {

        if (container == null) {
            container = new MyTowerTestKafkaContainer();
        }

        return container;
    }

    @Override
    public void start() {
        super.start();

        String kafkaUrl = "http://" + container.getHost() + ":" + container.getFirstMappedPort();
        System.setProperty("KAFKA_URL", kafkaUrl);
    }

    @Override
    public void stop() {
        // do nothing, JVM handles shut down
    }
}
