package com.adias.mytowereasy.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class JsonUtilsTU {
    @Test
    public void should_get_objectMapper_instance() {
        ObjectMapper mapper = JsonUtils.getObjectMapperInstance();
        assertThat(mapper).isNotNull();
    }

    @Test
    public void should_deserialize_json_list() throws JsonMappingException, JsonProcessingException {
        List<FakeObject> deserialized = JsonUtils.<FakeObject> deserializeList(VALID_JSON_LIST, FakeObject.class);

        assertThat(deserialized).isNotNull().hasSize(2);
        assertThat(deserialized.get(0))
            .hasFieldOrPropertyWithValue("label", "label1").hasFieldOrPropertyWithValue("value", "value1");
    }

    @Test(expected = JsonProcessingException.class)
    public void should_deserialize_json_list_fail_invalidJson() throws JsonMappingException, JsonProcessingException {
        JsonUtils.<FakeObject> deserializeList(INVALID_JSON_LIST, FakeObject.class);
    }

    @Test
    public void should_deserialize_json_list_fail_and_return_default() {
        List<FakeObject> deserialized = JsonUtils
            .<FakeObject> deserializeList(INVALID_JSON_LIST, FakeObject.class, new ArrayList<>());
        assertThat(deserialized).isNotNull().isEmpty();
    }

    @Test
    public void should_serialize_to_string() throws JsonProcessingException {
        FakeObject object = new FakeObject();
        object.setLabel("label1");
        object.setValue("value1");

        String serialized = JsonUtils.serializeToString(object);

        assertThat(serialized).isNotNull().isEqualTo(VALID_JSON_OBJECT);
    }

    protected final String VALID_JSON_OBJECT = "{\"label\":\"label1\",\"value\":\"value1\"}";
    protected final String VALID_JSON_LIST = "[" + "{\"label\": \"label1\", \"value\": \"value1\"},"
        + "{\"label\": \"label2\", \"value\": \"value2\"}" + "]";

    protected final String INVALID_JSON_LIST = "[{,,,},{}]";

    protected static class FakeObject {
        private String label;
        private String value;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}
