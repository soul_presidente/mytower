package com.adias.mytowereasy.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;
import com.adias.mytowereasy.airbus.model.EbQrGroupeProposition;
import com.adias.mytowereasy.airbus.repository.EbQrGroupePropositionRepository;
import com.adias.mytowereasy.airbus.repository.EbQrGroupeRepository;
import com.adias.mytowereasy.api.service.PlanTransportApiService;
import com.adias.mytowereasy.api.wso.PlanTransportWSO;
import com.adias.mytowereasy.dao.DaoPricing;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EbMarchandiseRepository;
import com.adias.mytowereasy.repository.ExEbDemandeTransporteurRepository;


@RunWith(MockitoJUnitRunner.class)
public class QrConsolidationServiceImplTest {
    @InjectMocks
    QrConsolidationServiceImpl qrConsolidationService;
    @Mock
    ExEbDemandeTransporteurRepository exEbDemandeTransporteurRepository;
    @Mock
    PlanTransportApiService planTransportApiService;
    @Mock
    EbDemandeRepository ebDemandeRepository;
    @Mock
    EbQrGroupeRepository ebQrGroupeRepository;
    @Mock
    EbQrGroupePropositionRepository ebQrGroupePropositionRepository;
    @Mock
    DaoPricing daoPricing;
    @Mock
    PricingService pricingService;
    @Mock
    ConnectedUserService connectedUserService;
    @Mock
    EbMarchandiseRepository ebMarchandiseRepository;

    EbQrGroupe ebQrGroupe;
    List<EbQrGroupeProposition> listPropo;
    EbQrGroupeProposition ebQrGroupeProposition;

    @Before
    public void setUp() {
        listPropo = new ArrayList<>();
        ebQrGroupe = new EbQrGroupe();

        ebQrGroupeProposition = new EbQrGroupeProposition();
        ebQrGroupeProposition.setListDemandeNum(":1234::12345:");
        ebQrGroupeProposition.setStatut(12);
        listPropo.add(ebQrGroupeProposition);

        ebQrGroupe.setListPropositions(listPropo);
        when(ebQrGroupeRepository.save(any())).thenReturn(null);
    }

    @After
    public void tearDown() {
        ebQrGroupe = null;
    }

    @Test
    public void shouldUpdateQrGroupeWhenPropositionsIsNull() throws Exception {
        ebQrGroupe.setListPropositions(null);
        // Test
        EbQrGroupe result = qrConsolidationService.updateQrGroupe(ebQrGroupe);
        // Assertions
        assertThat(result).isEqualTo(ebQrGroupe);
        verify(ebQrGroupeRepository, times(1)).save(ebQrGroupe);
    }

    @Test
    public void shouldUpdateQrGroupeWhenPropositionsIsNotNull() throws Exception {
        when(ebDemandeRepository.updateIsGroupingInEbDemandeByListEbDemandeNum(any(), eq(false))).thenReturn(11);
        // Test
        EbQrGroupe result = qrConsolidationService.updateQrGroupe(ebQrGroupe);
        // Assertions
        assertThat(result).isEqualTo(ebQrGroupe);
        verify(ebDemandeRepository, times(1)).updateIsGroupingInEbDemandeByListEbDemandeNum(any(), any());
        verify(ebQrGroupeRepository, times(1)).save(ebQrGroupe);
    }

    @Test
    public void shouldRetriggersWhenOnRuleUpdateAndOnRuleCration() throws Exception {
        ebQrGroupe.setTriggers("1,2,3,4");
		Set<Integer> listEbDemandeGroupedNum = new HashSet<>(Arrays.asList(1785));
        EbDemande ebDemande = new EbDemande(1234);
        ebDemande.setListMarchandises(new ArrayList<EbMarchandise>() {
            {
                add(new EbMarchandise(11));
            }
        });
        EbUser ebUser = new EbUser();
        EbCompagnie ebCompagnie = new EbCompagnie(111);
        ebCompagnie.setCode("1524F");
        ebUser.setEbCompagnie(ebCompagnie);
        ebQrGroupeProposition.setEbDemandeResult(ebDemande);

        when(ebQrGroupePropositionRepository.findEbQrGroupePropositionByebQrGroupe(any()))
            .thenReturn(listEbDemandeGroupedNum);
        doNothing().when(daoPricing).setPropositionGroupe(any(), any());
        when(connectedUserService.getCurrentUser()).thenReturn(ebUser);
        when(daoPricing.getListDemandeGroupe(any())).thenReturn(new ArrayList<EbDemande>() {
            {
                add(ebDemande);
            }
        });
        when(planTransportApiService.getNextPickUpDateOfTransportWSO(any())).thenReturn(new PlanTransportWSO());
        when(ebDemandeRepository.saveAll(any())).thenReturn(new ArrayList<EbDemande>() {
            {
                add(ebDemande);
            }
        });
        when(ebQrGroupePropositionRepository.saveAll(any())).thenReturn(new ArrayList<EbQrGroupeProposition>() {
            {
                add(ebQrGroupeProposition);
            }
        });
        when(ebDemandeRepository.updateIsGroupingInEbDemandeByListEbDemandeNum(any(), eq(true))).thenReturn(1);
        // Test
        EbQrGroupe result = qrConsolidationService.updateQrGroupe(ebQrGroupe);
        // Assertions
        assertThat(result).isEqualTo(ebQrGroupe);
        verify(ebQrGroupePropositionRepository, times(1)).findEbQrGroupePropositionByebQrGroupe(any());
        verify(daoPricing, times(1)).setPropositionGroupe(any(), any());
        verify(ebQrGroupeRepository, times(1)).save(ebQrGroupe);
        verify(daoPricing, times(1)).getListDemandeGroupe(any());
        verify(connectedUserService, times(1)).getCurrentUser();
        verify(planTransportApiService, times(1)).getNextPickUpDateOfTransportWSO(any());
        verify(ebDemandeRepository, times(1)).saveAll(any());
        verify(ebQrGroupePropositionRepository, times(1)).saveAll(any());
        verify(ebDemandeRepository, times(2)).updateIsGroupingInEbDemandeByListEbDemandeNum(any(), any());
    }
}
