package com.adias.mytowereasy.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.adias.moteur.chiffrage.library.shared.ResultEntity;
import com.adias.mytowereasy.dao.DaoPrMtcTransporteur;
import com.adias.mytowereasy.dto.PricingCarrierSearchResultDTO;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCountry;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;
import com.adias.mytowereasy.model.PrMtcTransporteur;
import com.adias.mytowereasy.model.enums.ConsolidationGroupage;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.mtc.service.MtcService;
import com.adias.mytowereasy.trpl.model.EbPlCostItem;


@RunWith(MockitoJUnitRunner.class)
public class PricingServiceImplTest {
    @InjectMocks
    PricingServiceImpl pricingServiceImpl;
    @Mock
    DaoPrMtcTransporteur daoPrMtcTransporteur;
    @Mock
    MtcService mtcService;

    private EbDemande ebDemande;
    private List<ExEbDemandeTransporteur> listTrnsp;
    private ExEbDemandeTransporteur trspt;
    private List<PrMtcTransporteur> listMtcTransporteur;
    private PrMtcTransporteur mtcTrnsp;
    private EbEtablissement ebEtablissement;
    private EbCompagnie ebCompagnie;
    private List<PricingCarrierSearchResultDTO> pricingCarrierSearchResultDTOList;
    private PricingCarrierSearchResultDTO pricingCarrierSearchResultDTO;
    private List<EbPlCostItem> listPlCostItem;
    private EbPlCostItem ebPlCostItem;
    private ResultEntity resultEntity;
    private EbUser ebUser;
    private EbParty ebParty;
    private EcCountry ecCountry;
    private EbTtSchemaPsl xEbSchemaPsl;
    private Date dateOfGoodsAvailability;
    private EcCurrency xecCurrencyInvoice;
    private List<EbMarchandise> listMarchandises;
    private EbMarchandise ebMarchandise;

    @Before
    public void setUp() throws Exception {
        generate_fake_data_for_should_give_valorization_for_a_demande();
    }

    @After
    public void tearDown() {
        ebDemande = null;
        ebMarchandise = null;
        ebParty = null;
        ebUser = null;
        ebPlCostItem = null;
        ebCompagnie = null;
        ebEtablissement = null;
        xEbSchemaPsl = null;
        listMarchandises = null;
        listPlCostItem = null;
        listMtcTransporteur = null;
        listTrnsp = null;
        pricingCarrierSearchResultDTOList = null;
    }

    @Test
    public void should_give_valorization_for_a_demande() throws Exception {
        when(daoPrMtcTransporteur.getListPrMtcTransporteur(Mockito.any())).thenReturn(listMtcTransporteur);
        when(mtcService.manageResult(resultEntity, ebDemande, ebDemande.getExEbDemandeTransporteurs().get(0)))
            .thenReturn(pricingCarrierSearchResultDTOList);
		when(mtcService.valuationFromMTC(ebDemande, mtcTrnsp.getCodeConfigurationMtc()))
            .thenReturn(resultEntity);

        EbDemande result = pricingServiceImpl.unitValuation(ebDemande);

        // assert
        assertThat(result.getValorized()).isEqualTo(true);
        assertThat(result.getxEcStatut()).isEqualTo(Enumeration.StatutDemande.WPU.getCode());
        assertThat(result.getExEbDemandeTransporteurs().get(0).getListPlCostItem()).isNotNull();
        assertThat(result.getExEbDemandeTransporteurs().get(0).getPrice()

        ).isEqualTo(pricingCarrierSearchResultDTO.getCalculatedPrice());
        assertThat(result.getExEbDemandeTransporteurs().get(0).getIsFromTransPlan()).isEqualTo(true);

        assertThat(result.getDateMaj()).isNotNull();
        assertThat(result.getxEcStatutGroupage()).isEqualTo(ConsolidationGroupage.PRS.getCode());
    }

    private void generate_fake_data_for_should_give_valorization_for_a_demande() {
        ebMarchandise = new EbMarchandise();
        ebMarchandise.setEbMarchandiseNum(889);
        listMarchandises = new ArrayList<>();
        listMarchandises.add(ebMarchandise);
        xecCurrencyInvoice = new EcCurrency();
        xecCurrencyInvoice.setEcCurrencyNum(665);
        dateOfGoodsAvailability = new Date();
        xEbSchemaPsl = new EbTtSchemaPsl();
        xEbSchemaPsl.setEbTtSchemaPslNum(3344);
        ebUser = new EbUser();
        ecCountry = new EcCountry();
        ecCountry.setEcCountryNum(5544);
        ebParty = new EbParty();
        ebParty.setEbPartyNum(2244);
        ebParty.setxEcCountry(ecCountry);
        ebUser.setEbUserNum(1133);
        ebDemande = new EbDemande();
        ebDemande.setCodeConfigurationEDI("EDI");
        ebDemande.setEbDemandeNum(111);
        trspt = new ExEbDemandeTransporteur();
        trspt.setExEbDemandeTransporteurNum(1111);
        trspt.setStatus(Enumeration.StatutCarrier.TRANSPORT_PLAN.getCode());
        ebEtablissement = new EbEtablissement();
        ebEtablissement.setEbEtablissementNum(97);
        trspt.setxEbEtablissement(ebEtablissement);
        ebCompagnie = new EbCompagnie(23);
        ebCompagnie.setCode("ESS-S");
        trspt.setxEbCompagnie(ebCompagnie);
        listTrnsp = new ArrayList<>();
        listTrnsp.add(trspt);
        ebDemande.setExEbDemandeTransporteurs(listTrnsp);
        ebDemande.setxEcStatut(Enumeration.StatutDemande.FIN.getCode());
        ebDemande.setFinalChoiceToBeCalculated(true);
        ebDemande.setxEcStatutGroupage(ConsolidationGroupage.NP.getCode());
        // demande mandatory fields
        ebDemande.setUser(ebUser);
        ebDemande.setEbPartyOrigin(ebParty);
        ebDemande.setEbPartyDest(ebParty);
        ebDemande.setxEcModeTransport(1);
        ebDemande.setxEbSchemaPsl(xEbSchemaPsl);
        ebDemande.setDateOfGoodsAvailability(dateOfGoodsAvailability);
        ebDemande.setxEbTypeRequest(1);
        ebDemande.setXecCurrencyInvoice(xecCurrencyInvoice);
        ebDemande.setListMarchandises(listMarchandises);
        // compangie
        ebCompagnie = new EbCompagnie();
        ebCompagnie.setEbCompagnieNum(60);
        // Transporteur MTC
        listMtcTransporteur = new ArrayList<>();
        mtcTrnsp = new PrMtcTransporteur();
        mtcTrnsp.setPrMtcTransporteurNum(222);
        mtcTrnsp.setxEbCompagnieTransporteur(ebCompagnie);
        mtcTrnsp.setCodeConfigurationMtc("2R");
        mtcTrnsp.setCodeConfigurationEDI("EDI");
        listMtcTransporteur.add(mtcTrnsp);
        // result Entity
        resultEntity = new ResultEntity();
        resultEntity.setMesssage("unit test");
        // listPlCostItem
        ebPlCostItem = new EbPlCostItem();
        ebPlCostItem.setCostItemNum(1221);
        listPlCostItem = new ArrayList<>();
        listPlCostItem.add(ebPlCostItem);
        // MTC Response
        pricingCarrierSearchResultDTO = new PricingCarrierSearchResultDTO();
        pricingCarrierSearchResultDTO.setCalculatedPrice(new BigDecimal(100));
        pricingCarrierSearchResultDTO.setListPlCostItem(listPlCostItem);
        pricingCarrierSearchResultDTOList = new ArrayList<>();
        pricingCarrierSearchResultDTOList.add(pricingCarrierSearchResultDTO);
    }
}
