package com.adias.mytowereasy.delivery.repository;

import static com.adias.mytowereasy.tests.db.CommonDbOperations.EACH_TESTS_OPERATION;
import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import com.adias.mytowereasy.MyTowerEasyApplication;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.tests.core.DatabaseTestCore;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyTowerEasyApplication.class, webEnvironment = RANDOM_PORT)
@ActiveProfiles("unittest")
public class EbOrderLineRepositoryTests extends DatabaseTestCore {
    @Autowired
    EbOrderLineRepository ebOrderLineRepository;

    @Before
    public void setUp() {
        Operation dbSetUp = sequenceOf(EACH_TESTS_OPERATION, insertData);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), dbSetUp);
        dbSetupTracker.launchIfNecessary(dbSetup);
    }

    @Test
    public void should_find_orderLine_by_order() {
        // Call
        List<EbOrderLine> results = ebOrderLineRepository.findByOrder(1001l);

        // Check
        assertThat(results).isNotNull().hasSize(2);
        assertThat(results).filteredOn(r -> "ref1".equals(r.getRefArticle())).hasSize(1);
        assertThat(results).filteredOn(r -> "ref2".equals(r.getRefArticle())).hasSize(1);
        assertThat(results).filteredOn(r -> "ref3".equals(r.getRefArticle())).hasSize(0);
    }

    private Operation insertData = sequenceOf(
        insertInto("work.eb_party")
            .columns("eb_party_num", "city", "x_eb_etablissement_adresse").values(1001, "Casablanca", -1)
            .values(1002, "Paris", -1).values(1003, "Nouakchott", -1).values(1004, "Casablanca", -1)
            .values(1005, "Paris", -1).values(1006, "Nouakchott", -1).build(),
        insertInto("work.eb_del_order")
            .columns(
                "eb_del_order_num",
                "num_order_customer",
                "x_eb_etablissement",
                "x_eb_compagnie",
                "x_eb_party_origin",
                "x_eb_party_destination",
                "x_eb_party_sale")
            .values(1001, "CUSTST1", -1, -1, 1001, 1002, 1003).values(1002, "CUSTST2", -1, -1, 1004, 1005, 1006)
            .build(),
        insertInto("work.eb_del_order_line")
            .columns("eb_del_order_line_num", "ref_article", "x_eb_del_order").values(1001, "ref1", 1001)
            .values(1002, "ref2", 1001).values(1003, "ref3", 1002).build());
}
