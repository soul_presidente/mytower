package com.adias.mytowereasy.delivery.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.adias.mytowereasy.delivery.repository.EbOrderLineRepository;
import com.adias.mytowereasy.delivery.service.impl.OrderLineServiceImpl;
import com.adias.mytowereasy.dto.EbOrderLineDto;
import com.adias.mytowereasy.dto.mapper.DeliveryMapper;


@RunWith(MockitoJUnitRunner.class)
public class OrderLineServiceTest {
    @Mock
    EbOrderLineRepository ebOrderLineRepository;

    @Mock
    DeliveryMapper deliveryMapper;

    @InjectMocks
    OrderLineServiceImpl orderLineService;

    @Before
    public void setUp() {
        reset(ebOrderLineRepository);
        reset(deliveryMapper);
    }

    @Test
    public void should_filter_orderLine_plannable() {
        // Prepare
        List<EbOrderLineDto> fakeDtos = new ArrayList<>();

        EbOrderLineDto plannableLine = new EbOrderLineDto();
        plannableLine.setEbDelOrderLineNum(20l);
        plannableLine.setQuantityPlannable(2l);
        fakeDtos.add(plannableLine);

        EbOrderLineDto unplannableLine = new EbOrderLineDto();
        unplannableLine.setEbDelOrderLineNum(21l);
        unplannableLine.setQuantityPlannable(0l);
        fakeDtos.add(unplannableLine);

        // Mock
        when(ebOrderLineRepository.findByOrder(any())).thenReturn(new ArrayList<>());
        when(deliveryMapper.ebOrderLinesToEbOrderLinesDto(any())).thenReturn(fakeDtos);

        // Call
        List<EbOrderLineDto> plannables = orderLineService.getOrderLinesPlanifiable(1l);

        // Check
        assertThat(plannables).isNotNull().hasSize(1);
        assertThat(plannables.get(0).getEbDelOrderLineNum()).isEqualTo(20l);
    }
}
