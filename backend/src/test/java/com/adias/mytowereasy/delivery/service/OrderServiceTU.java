package com.adias.mytowereasy.delivery.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.MessageSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.adias.mytowereasy.delivery.dao.DaoOrder;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.repository.EbLivraisonLineRepository;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.repository.EbOrderRepository;
import com.adias.mytowereasy.delivery.service.impl.OrderServiceImpl;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.repository.EbChatRepository;
import com.adias.mytowereasy.service.ConnectedUserService;


@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTU {
    @Mock
    DaoOrder daoOrder;

    @Mock
    EbOrderRepository ebOrderRepository;

    @Mock
    ConnectedUserService connectedUserService;

    @Mock
    EbLivraisonLineRepository ebLivraisonLineRepository;

    @Mock
    EbLivraisonRepository ebLivraisonRepository;

    @Mock
    EbChatRepository ebChatRepository;

		@Mock
		MessageSource						messageSource;

    @InjectMocks
    OrderServiceImpl orderService;

    @Before
    public void setUp() {
        reset(daoOrder);
        reset(connectedUserService);
        reset(ebOrderRepository);

        // Common mocks
        EbUser fakeConnectedUser = new EbUser(-1);
        fakeConnectedUser.setEbCompagnie(new EbCompagnie(-1));
        fakeConnectedUser.setEbEtablissement(new EbEtablissement(-1));
        when(connectedUserService.getCurrentUser()).thenReturn(fakeConnectedUser);
    }

    /**
     * Saved order doesn't contains etablissement and company and will use
     * connected user informations
     * 
     * @throws JsonProcessingException
     * @throws JsonMappingException
     */
    @Test
    public void
        should_save_order_case_from_form() throws JsonMappingException, JsonProcessingException {
        // Prepare
        EbOrder order = new EbOrder(5l);

        order.setOrderLines(new ArrayList<>());
        order.getOrderLines().add(new EbOrderLine());

        // Mock
        when(daoOrder.saveOrder(eq(order))).thenReturn(order);
        when(ebOrderRepository.findById(any())).thenReturn(Optional.of(order));

        // Call
        EbOrder resultOrder = orderService.updateOrder(order);

        // Check
        assertThat(resultOrder).isNotNull();
        assertThat(resultOrder.getEbDelOrderNum()).isEqualTo(order.getEbDelOrderNum());
        assertThat(resultOrder.getxEbCompagnie()).isNotNull().hasFieldOrPropertyWithValue("ebCompagnieNum", -1);
        assertThat(resultOrder.getxEbEtablissement()).isNotNull().hasFieldOrPropertyWithValue("ebEtablissementNum", -1);
    }

    /**
     * Saved order contains etablissement and company and will not use connected
     * user informations
     * 
     * @throws JsonProcessingException
     * @throws JsonMappingException
     */
    @Test
    public void
        should_save_order_case_from_edi() throws JsonMappingException, JsonProcessingException {
        // Prepare
        EbOrder order = new EbOrder(5l);
        order.setxEbCompagnie(new EbCompagnie(31));
        order.setxEbEtablissement(new EbEtablissement(32));

        order.setOrderLines(new ArrayList<>());
        order.getOrderLines().add(new EbOrderLine());

        // Mock
        when(daoOrder.saveOrder(eq(order))).thenReturn(order);
        when(ebOrderRepository.findById(any())).thenReturn(Optional.of(order));

        // Call
        EbOrder resultOrder = orderService.updateOrder(order);

        // Check
        assertThat(resultOrder).isNotNull();
        assertThat(resultOrder.getEbDelOrderNum()).isEqualTo(order.getEbDelOrderNum());
        assertThat(resultOrder.getxEbCompagnie()).isNotNull().hasFieldOrPropertyWithValue("ebCompagnieNum", 31);
        assertThat(resultOrder.getxEbEtablissement()).isNotNull().hasFieldOrPropertyWithValue("ebEtablissementNum", 32);
    }

    @Test(expected = MyTowerException.class)
    public void should_save_order_fail_due_orderLines_null()
        throws JsonMappingException,
        JsonProcessingException {
        // Prepare
        EbOrder order = new EbOrder(5l);
        order.setxEbCompagnie(new EbCompagnie(31));
        order.setxEbEtablissement(new EbEtablissement(32));

        // Call
        orderService.updateOrder(order);
    }

    @Test(expected = MyTowerException.class)
    public void should_save_order_fail_due_orderLines_empty()
        throws JsonMappingException,
        JsonProcessingException {
        // Prepare
        EbOrder order = new EbOrder(5l);
        order.setxEbCompagnie(new EbCompagnie(31));
        order.setxEbEtablissement(new EbEtablissement(32));
        order.setOrderLines(new ArrayList<>());

        // Call
        orderService.updateOrder(order);
    }

    @Test(expected = MyTowerException.class)
    public void should_calc_order_and_fail_due_order_notExist() {
        // Mock
        when(ebOrderRepository.findById(any())).thenReturn(Optional.empty());

        // Call
        orderService.calculateOrder(4598453145L, true);
    }
}
