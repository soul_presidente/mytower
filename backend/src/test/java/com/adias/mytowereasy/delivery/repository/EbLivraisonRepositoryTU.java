package com.adias.mytowereasy.delivery.repository;

import static com.adias.mytowereasy.tests.db.CommonDbOperations.EACH_TESTS_OPERATION;
import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import com.adias.mytowereasy.MyTowerEasyApplication;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.tests.core.DatabaseTestCore;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyTowerEasyApplication.class, webEnvironment = RANDOM_PORT)
@ActiveProfiles("unittest")
public class EbLivraisonRepositoryTU extends DatabaseTestCore {
    @Autowired
    EbLivraisonRepository ebLivraisonRepository;

    @Before
    public void setUp() {
        Operation dbSetUp = sequenceOf(EACH_TESTS_OPERATION, insertData);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), dbSetUp);
        dbSetupTracker.launchIfNecessary(dbSetup);
    }

    @Test
    public void should_find_by_ebDelLivraisonNum_inList() {
        // Prepare
        List<Long> requestList = Arrays.asList(10001L, 10004L, 9999999L);

        // Call
        List<EbLivraison> result = ebLivraisonRepository.findByEbDelLivraisonNumIn(requestList);

        // Check
        assertThat(result).isNotNull().hasSize(2);
        assertThat(result).filteredOn("ebDelLivraisonNum", 10001L).hasSize(1);
        assertThat(result).filteredOn("ebDelLivraisonNum", 10004L).hasSize(1);
        assertThat(result).filteredOn("ebDelLivraisonNum", 9999999L).hasSize(0);
    }

    private Operation insertData = sequenceOf(
        insertInto("work.eb_del_livraison")
            .columns("eb_del_livraison_num").values(10001).values(10002).values(10003).values(10004).build());
}
