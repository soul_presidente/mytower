package com.adias.mytowereasy.delivery.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.hamcrest.MockitoHamcrest;
import org.mockito.junit.MockitoJUnitRunner;

import com.adias.mytowereasy.dao.DaoUser;
import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.service.impl.DeliveryServiceImpl;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.AdresseService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RunWith(MockitoJUnitRunner.class)
public class DeliveryServiceTU {
    @Mock
    AdresseService adresseService;

    @Mock
    EbLivraisonRepository ebLivraisonRepository;

    @Mock
    DaoUser daoUser;

    @Mock
    OrderService orderService;

    @InjectMocks
    DeliveryServiceImpl deliveryService;

    @Before
    public void setUp() {
        reset(ebLivraisonRepository);
    }

    @Test
    public void should_preConsolidate_without_conflict() {
        // Prepare
        List<EbLivraison> fakeLivraisons = new ArrayList<>();

        EbLivraisonLine ebLivraisonLine1 = new EbLivraisonLine();
        ebLivraisonLine1.setRefArticle("ref 1");
        ebLivraisonLine1.setEbDelLivraisonLineNum(new Long(1297));

        EbLivraisonLine ebLivraisonLine2 = new EbLivraisonLine();
        ebLivraisonLine2.setRefArticle("ref 2");
        ebLivraisonLine2.setEbDelLivraisonLineNum(new Long(1298));

        EbParty commonFakeParty = new EbParty();
        commonFakeParty.setCity("City1");

        EbLivraison livraison1 = new EbLivraison();
        livraison1.setxEbPartyOrigin(commonFakeParty);
        livraison1.setxEbPartyDestination(commonFakeParty);
        livraison1.setxEbOwnerOfTheRequest(new EbUser(1));
        livraison1.setxEbDelLivraisonLine(new ArrayList<EbLivraisonLine>() {
            {
                add(ebLivraisonLine1);
            }
        });
        fakeLivraisons.add(livraison1);

        EbLivraison livraison2 = new EbLivraison();
        livraison2.setxEbPartyOrigin(commonFakeParty);
        livraison2.setxEbPartyDestination(commonFakeParty);
        livraison2.setxEbOwnerOfTheRequest(new EbUser(1));
        livraison2.setxEbDelLivraisonLine(new ArrayList<EbLivraisonLine>() {
            {
                add(ebLivraisonLine2);
            }
        });
        fakeLivraisons.add(livraison2);

        when(
            daoUser
                .findUser(
                    MockitoHamcrest.argThat(Matchers.<SearchCriteria> hasProperty("ebUserNum", Matchers.equalTo(1)))))
                        .thenReturn(new EbUser(1));
        when(adresseService.areSameAddresses(any(), any())).thenReturn(true);
        when(ebLivraisonRepository.findByEbDelLivraisonNumIn(any())).thenReturn(fakeLivraisons);

        // Call
        EbDemande demande = deliveryService.preConsolidateDelivery(new ArrayList<>());

        // Check properties
        assertThat(demande.getUser()).isNotNull().hasFieldOrPropertyWithValue("ebUserNum", 1);

        // Check EbPartyOrigin && getEbPartyDest
        assertThat(demande.getEbPartyOrigin()).isNotNull().hasFieldOrPropertyWithValue("city", "City1");
        assertThat(demande.getEbPartyDest()).isNotNull().hasFieldOrPropertyWithValue("city", "City1");

        // check list of units
        assertThat(demande.getListMarchandises()).isNotNull().hasSize(2);
        assertThat(demande.getListMarchandises().get(0)).hasFieldOrPropertyWithValue("PartNumber", "ref 1");
        assertThat(demande.getListMarchandises().get(0).getxEbLivraisonLines().get(0))
            .isNotNull().hasFieldOrPropertyWithValue("ebDelLivraisonLineNum", 1297l);

        assertThat(demande.getListMarchandises().get(1)).hasFieldOrPropertyWithValue("PartNumber", "ref 2");
        assertThat(demande.getListMarchandises().get(1).getxEbLivraisonLines().get(0))
            .isNotNull().hasFieldOrPropertyWithValue("ebDelLivraisonLineNum", 1298l);

        // Verify correct method is called (to return compagnie and
        // etablissement of user)
        verify(daoUser, times(1)).findUser(any());
    }

    @Test
    public void should_preConsolidate_with_conflict() {
        // Prepare
        List<EbLivraison> fakeLivraisons = new ArrayList<>();

        EbParty fakeParty1 = new EbParty();
        fakeParty1.setCity("City1");
        EbParty fakeParty2 = new EbParty();
        fakeParty2.setCity("City2");

        EbLivraison livraison1 = new EbLivraison();
        livraison1.setxEbPartyOrigin(fakeParty1);
        livraison1.setxEbPartyDestination(fakeParty1);
        livraison1.setxEbOwnerOfTheRequest(new EbUser(1));
        fakeLivraisons.add(livraison1);

        EbLivraison livraison2 = new EbLivraison();
        livraison2.setxEbPartyOrigin(fakeParty2);
        livraison2.setxEbPartyDestination(fakeParty2);
        livraison2.setxEbOwnerOfTheRequest(new EbUser(2));
        fakeLivraisons.add(livraison2);

        when(adresseService.areSameAddresses(any(), any())).thenReturn(false);
        when(ebLivraisonRepository.findByEbDelLivraisonNumIn(any())).thenReturn(fakeLivraisons);

        // Call
        EbDemande demande = deliveryService.preConsolidateDelivery(new ArrayList<>());

        // Check properties
        assertThat(demande.getUser()).isNull();

        // Check EbPartyOrigin && getEbPartyDest
        assertThat(demande.getEbPartyOrigin().getCity()).isNull();
        assertThat(demande.getEbPartyDest().getCity()).isNull();

        // Check list of units
        assertThat(demande.getListMarchandises()).isEmpty();

        // Verify correct method is called (to return compagnie and
        // etablissement of user)
        verify(daoUser, times(1)).findUser(any());
    }

    @Test
    public void should_confirm_delivery() {
        // Mock
        EbLivraison livraison = new EbLivraison();
        livraison.setEbDelLivraisonNum(1l);
        livraison.setStatusDelivery(StatusDelivery.PENDING);
        livraison.setxEbDelOrder(new EbOrder(1l));

        when(ebLivraisonRepository.findById(eq(1l))).thenReturn(Optional.of(livraison));

        // Call
        livraison = deliveryService.confirmDelivery(1l);

        // Check
        assertThat(livraison.getStatusDelivery()).isEqualTo(StatusDelivery.CONFIRMED);
        verify(orderService, times(1)).calculateOrder(eq(1l), eq(true));
    }

    @Test(expected = MyTowerException.class)
    public void should_refuse_confirm_delivery_due_status() {
        // Mock
        EbLivraison livraison = new EbLivraison();
        livraison.setEbDelLivraisonNum(1l);
        livraison.setStatusDelivery(StatusDelivery.DELIVERED);
        livraison.setxEbDelOrder(new EbOrder(1l));

        when(ebLivraisonRepository.findById(eq(1l))).thenReturn(Optional.of(livraison));

        // Call
        livraison = deliveryService.confirmDelivery(1l);
    }
}
