package com.adias.mytowereasy.delivery.integration;

import static com.adias.mytowereasy.tests.db.CommonDbOperations.EACH_TESTS_OPERATION;
import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import com.adias.mytowereasy.MyTowerEasyApplication;
import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.enumeration.StatusOrder;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.repository.EbLivraisonLineRepository;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.repository.EbOrderLineRepository;
import com.adias.mytowereasy.delivery.repository.EbOrderRepository;
import com.adias.mytowereasy.dto.EbLivraisonDto;
import com.adias.mytowereasy.dto.EbOrderLinePlanificationDTO;
import com.adias.mytowereasy.dto.EbOrderPlanificationDTO;
import com.adias.mytowereasy.tests.core.ApiTestCore;
import com.adias.mytowereasy.tests.utils.RestAssuredUtils;
import io.restassured.response.Response;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyTowerEasyApplication.class, webEnvironment = RANDOM_PORT)
@ActiveProfiles("unittest")
public class OrderTI extends ApiTestCore {
    private static String API_URL = "/api/order-overview/";

    @Autowired
    EbOrderRepository ebOrderRepository;

    @Autowired
    EbOrderLineRepository ebOrderLineRepository;

    @Autowired
    EbLivraisonRepository ebLivraisonRepository;

    @Autowired
    EbLivraisonLineRepository ebLivraisonLineRepository;

    @Before
    public void setUp() {
        generateCommonTestModels();
        RestAssuredUtils.applyRestAssuredConfig(port);

        Operation dbSetUp = sequenceOf(EACH_TESTS_OPERATION, insertData);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), dbSetUp);
        dbSetupTracker.launchIfNecessary(dbSetup);
    }

    @Test
    public void should_planify_order() {
        // Prepare object to send
        EbOrderPlanificationDTO requestBody = new EbOrderPlanificationDTO();
        requestBody.setEbDelOrderNum(1001l);
        EbOrderLinePlanificationDTO line = new EbOrderLinePlanificationDTO();
        line.setQuantityToPlan(6l);
        line.setEbDelOrderLineNum(1001l);
        requestBody.getListToPlan().add(line);

        // Call
        Response response = given()
            .header(HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).header(HEADER_AUTH, TOKEN_PREFIX + TOKEN_MTO)
            .body(requestBody).post(API_URL + "planify-order");

        response.then().assertThat().statusCode(200);

        Long generatedLivraisonNum = response.getBody().as(EbLivraisonDto.class).getEbDelLivraisonNum();
        assertThat(generatedLivraisonNum).isNotNull().isGreaterThan(0l);

        // Check db
        EbOrder order = ebOrderRepository.findById(1001l).get();
        assertThat(order)
            .hasFieldOrPropertyWithValue("totalQuantityOrdered", 11l)
            .hasFieldOrPropertyWithValue("totalQuantityPlannable", 5l)
            .hasFieldOrPropertyWithValue("totalQuantityPlanned", 6l)
            .hasFieldOrPropertyWithValue("totalQuantityPending", 6l)
            .hasFieldOrPropertyWithValue("orderStatus", StatusOrder.NOTDLV.getCode());

        EbOrderLine orderLine = ebOrderLineRepository.findById(1001l).get();
        assertThat(orderLine)
            .hasFieldOrPropertyWithValue("quantityOrdered", 11l).hasFieldOrPropertyWithValue("quantityPlannable", 5l)
            .hasFieldOrPropertyWithValue("quantityPending", 6l).hasFieldOrPropertyWithValue("quantityPlanned", 6l);

        EbLivraison livraison = ebLivraisonRepository.findById(generatedLivraisonNum).get();
        assertThat(livraison.getStatusDelivery()).isEqualTo(StatusDelivery.PENDING);
        assertThat(livraison.getxEbDelLivraisonLine()).isNotNull().hasSize(1);
        assertThat(livraison.getRefOrder()).isEqualTo("TST-O01001");
        assertThat(livraison.getxEbDelOrder()).isNotNull();
        assertThat(livraison.getxEbDelOrder().getEbDelOrderNum()).isEqualTo(1001l);

        EbLivraisonLine livraisonLine = livraison.getxEbDelLivraisonLine().get(0);
        assertThat(livraisonLine.getQuantity()).isEqualTo(6l);

        // Check different EbParty
        // Will be reenabled to clone entity
        assertThat(order.getxEbPartyOrigin()).isNotEqualTo(livraison.getxEbPartyOrigin());
        assertThat(order.getxEbPartyDestination()).isNotEqualTo(livraison.getxEbPartyDestination());
    }

  //@formatter:off
	private Operation insertData = sequenceOf(
		insertInto("work.eb_party")
			.columns("eb_party_num", "city", "x_eb_etablissement_adresse")
			.values(1001, "Casablanca", -1)
			.values(1002, "Paris", -1)
			.values(1003, "Nouakchott", -1)
			.build(),
		insertInto("work.eb_del_order")
			.columns(
				"eb_del_order_num",
				"order_status",
				"total_quantity_ordered",
				"x_eb_compagnie",
				"x_eb_etablissement",
				"x_eb_party_destination",
				"x_eb_party_origin",
				"x_eb_party_sale",
				"reference"
			)
			.values(1001, 0, 11, -1, -1, 1001, 1002, 1003, "TST-O01001")
			.build(),
		insertInto("work.eb_del_order_line")
			.columns(
				"eb_del_order_line_num",
				"quantity_ordered",
				"x_eb_del_order"
				)
			.values(1001, 11, 1001)
			.build()
	);
	//@formatter:on

    private void generateCommonTestModels() {
        // Add some things here when required
    }
}
