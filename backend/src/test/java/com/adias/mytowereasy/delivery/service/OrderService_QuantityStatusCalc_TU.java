package com.adias.mytowereasy.delivery.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.enumeration.StatusOrder;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.repository.EbLivraisonLineRepository;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.service.impl.OrderServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class OrderService_QuantityStatusCalc_TU {
    @Mock
    EbLivraisonRepository ebLivraisonRepository;

    @Mock
    EbLivraisonLineRepository ebLivraisonLineRepository;

    @InjectMocks
    OrderServiceImpl orderService;

    @Before
    public void setUp() throws Exception {
        orderService = spy(OrderServiceImpl.class);
        MockitoAnnotations.initMocks(this);

        reset(ebLivraisonRepository);
        reset(ebLivraisonLineRepository);
    }

    @Test
    public void should_calc_quantities_for_order() {
        EbOrder order = new EbOrder();
        order.setOrderLines(new ArrayList<>());

        EbOrderLine orderLine1 = new EbOrderLine();
        orderLine1.setEbDelOrderLineNum(1l);
        orderLine1.setQuantityOrdered(50l);
        orderLine1.setQuantityPlannable(30l);
        orderLine1.setQuantityPlanned(20l);
        orderLine1.setQuantityPending(4l);
        orderLine1.setQuantityConfirmed(5l);
        orderLine1.setQuantityShipped(6l);
        orderLine1.setQuantityDelivered(5l);
        orderLine1.setQuantityCanceled(60l);
        order.getOrderLines().add(orderLine1);

        EbOrderLine orderLine2 = new EbOrderLine();
        orderLine2.setEbDelOrderLineNum(2l);
        orderLine2.setQuantityOrdered(60l);
        orderLine2.setQuantityPlannable(36l);
        orderLine2.setQuantityPlanned(24l);
        orderLine2.setQuantityPending(3l);
        orderLine2.setQuantityConfirmed(6l);
        orderLine2.setQuantityShipped(7l);
        orderLine2.setQuantityDelivered(6l);
        orderLine2.setQuantityCanceled(20l);
        order.getOrderLines().add(orderLine2);

        Mockito.doNothing().when(orderService).calculateQuantitiesForLine(any());

        orderService.calculateQuantities(order);

        assertThat(order)
            .hasFieldOrPropertyWithValue("totalQuantityOrdered", 110l)
            .hasFieldOrPropertyWithValue("totalQuantityPlannable", 66l)
            .hasFieldOrPropertyWithValue("totalQuantityPlanned", 44l)
            .hasFieldOrPropertyWithValue("totalQuantityPending", 7l)
            .hasFieldOrPropertyWithValue("totalQuantityConfirmed", 11l)
            .hasFieldOrPropertyWithValue("totalQuantityShipped", 13l)
            .hasFieldOrPropertyWithValue("totalQuantityDelivered", 11l)
            .hasFieldOrPropertyWithValue("totalQuantityCanceled", 80l);
    }

    @Test
    public void should_calc_quantities_for_orderLine() {
        EbOrderLine orderLine = new EbOrderLine();
        orderLine.setEbDelOrderLineNum(1100l);
        orderLine.setQuantityOrdered(100l);

        // Generate one livraison per status
        EbLivraison livraisonCanceled = new EbLivraison();
        livraisonCanceled.setEbDelLivraisonNum(1l);
        livraisonCanceled.setStatusDelivery(StatusDelivery.CANCELED);
        livraisonCanceled.setxEbDelLivraisonLine(new ArrayList<>());

        EbLivraison livraisonConfirmed = new EbLivraison();
        livraisonConfirmed.setEbDelLivraisonNum(2l);
        livraisonConfirmed.setStatusDelivery(StatusDelivery.CONFIRMED);
        livraisonConfirmed.setxEbDelLivraisonLine(new ArrayList<>());

        EbLivraison livraisonDelivered = new EbLivraison();
        livraisonDelivered.setEbDelLivraisonNum(3l);
        livraisonDelivered.setStatusDelivery(StatusDelivery.DELIVERED);
        livraisonDelivered.setxEbDelLivraisonLine(new ArrayList<>());

        EbLivraison livraisonPending = new EbLivraison();
        livraisonPending.setEbDelLivraisonNum(4l);
        livraisonPending.setStatusDelivery(StatusDelivery.PENDING);
        livraisonPending.setxEbDelLivraisonLine(new ArrayList<>());

        EbLivraison livraisonShipped = new EbLivraison();
        livraisonShipped.setEbDelLivraisonNum(5l);
        livraisonShipped.setStatusDelivery(StatusDelivery.SHIPPED);
        livraisonShipped.setxEbDelLivraisonLine(new ArrayList<>());

        /////// Generate at least 2 lines per status to correctly cover specs
        List<EbLivraisonLine> allLinesForOrderLine = new ArrayList<>();

        // Status canceled (Qty 60)
        EbLivraisonLine lineCanceled1 = new EbLivraisonLine();
        lineCanceled1.setEbDelLivraisonLineNum(1l);
        lineCanceled1.setxEbDelLivraison(livraisonCanceled);
        lineCanceled1.setxEbOrderLigne(orderLine);
        lineCanceled1.setQuantity(50l);
        livraisonCanceled.getxEbDelLivraisonLine().add(lineCanceled1);
        allLinesForOrderLine.add(lineCanceled1);
        when(ebLivraisonRepository.findByLivraisonLine(eq(lineCanceled1.getEbDelLivraisonLineNum())))
            .thenReturn(Optional.of(livraisonCanceled));

        EbLivraisonLine lineCanceled2 = new EbLivraisonLine();
        lineCanceled2.setEbDelLivraisonLineNum(2l);
        lineCanceled2.setxEbDelLivraison(livraisonCanceled);
        lineCanceled2.setxEbOrderLigne(orderLine);
        lineCanceled2.setQuantity(10l);
        livraisonCanceled.getxEbDelLivraisonLine().add(lineCanceled2);
        allLinesForOrderLine.add(lineCanceled2);
        when(ebLivraisonRepository.findByLivraisonLine(eq(lineCanceled2.getEbDelLivraisonLineNum())))
            .thenReturn(Optional.of(livraisonCanceled));

        // Status confirmed (Qty 3)
        EbLivraisonLine lineConfirmed1 = new EbLivraisonLine();
        lineConfirmed1.setEbDelLivraisonLineNum(3l);
        lineConfirmed1.setxEbDelLivraison(livraisonConfirmed);
        lineConfirmed1.setxEbOrderLigne(orderLine);
        lineConfirmed1.setQuantity(2l);
        livraisonConfirmed.getxEbDelLivraisonLine().add(lineConfirmed1);
        allLinesForOrderLine.add(lineConfirmed1);
        when(ebLivraisonRepository.findByLivraisonLine(eq(lineConfirmed1.getEbDelLivraisonLineNum())))
            .thenReturn(Optional.of(livraisonConfirmed));

        EbLivraisonLine lineConfirmed2 = new EbLivraisonLine();
        lineConfirmed2.setEbDelLivraisonLineNum(4l);
        lineConfirmed2.setxEbDelLivraison(livraisonConfirmed);
        lineConfirmed2.setxEbOrderLigne(orderLine);
        lineConfirmed2.setQuantity(1l);
        livraisonConfirmed.getxEbDelLivraisonLine().add(lineConfirmed2);
        allLinesForOrderLine.add(lineConfirmed2);
        when(ebLivraisonRepository.findByLivraisonLine(eq(lineConfirmed2.getEbDelLivraisonLineNum())))
            .thenReturn(Optional.of(livraisonConfirmed));

        // Status delivered (Qty 12)
        EbLivraisonLine lineDelivered1 = new EbLivraisonLine();
        lineDelivered1.setEbDelLivraisonLineNum(5l);
        lineDelivered1.setxEbDelLivraison(livraisonDelivered);
        lineDelivered1.setxEbOrderLigne(orderLine);
        lineDelivered1.setQuantity(4l);
        livraisonDelivered.getxEbDelLivraisonLine().add(lineDelivered1);
        allLinesForOrderLine.add(lineDelivered1);
        when(ebLivraisonRepository.findByLivraisonLine(eq(lineDelivered1.getEbDelLivraisonLineNum())))
            .thenReturn(Optional.of(livraisonDelivered));

        EbLivraisonLine lineDelivered2 = new EbLivraisonLine();
        lineDelivered2.setEbDelLivraisonLineNum(6l);
        lineDelivered2.setxEbDelLivraison(livraisonDelivered);
        lineDelivered2.setxEbOrderLigne(orderLine);
        lineDelivered2.setQuantity(8l);
        livraisonDelivered.getxEbDelLivraisonLine().add(lineDelivered2);
        allLinesForOrderLine.add(lineDelivered2);
        when(ebLivraisonRepository.findByLivraisonLine(eq(lineDelivered2.getEbDelLivraisonLineNum())))
            .thenReturn(Optional.of(livraisonDelivered));

        // Status pending (Qty 15)
        EbLivraisonLine linePending1 = new EbLivraisonLine();
        linePending1.setEbDelLivraisonLineNum(7l);
        linePending1.setxEbDelLivraison(livraisonPending);
        linePending1.setxEbOrderLigne(orderLine);
        linePending1.setQuantity(5l);
        livraisonPending.getxEbDelLivraisonLine().add(linePending1);
        allLinesForOrderLine.add(linePending1);
        when(ebLivraisonRepository.findByLivraisonLine(eq(linePending1.getEbDelLivraisonLineNum())))
            .thenReturn(Optional.of(livraisonPending));

        EbLivraisonLine linePending2 = new EbLivraisonLine();
        linePending2.setEbDelLivraisonLineNum(8l);
        linePending2.setxEbDelLivraison(livraisonPending);
        linePending2.setxEbOrderLigne(orderLine);
        linePending2.setQuantity(10l);
        livraisonPending.getxEbDelLivraisonLine().add(linePending2);
        allLinesForOrderLine.add(linePending2);
        when(ebLivraisonRepository.findByLivraisonLine(eq(linePending2.getEbDelLivraisonLineNum())))
            .thenReturn(Optional.of(livraisonPending));

        // Status shipped (Qty 6)
        EbLivraisonLine lineShipped1 = new EbLivraisonLine();
        lineShipped1.setEbDelLivraisonLineNum(9l);
        lineShipped1.setxEbDelLivraison(livraisonShipped);
        lineShipped1.setxEbOrderLigne(orderLine);
        lineShipped1.setQuantity(2l);
        livraisonShipped.getxEbDelLivraisonLine().add(lineShipped1);
        allLinesForOrderLine.add(lineShipped1);
        when(ebLivraisonRepository.findByLivraisonLine(eq(lineShipped1.getEbDelLivraisonLineNum())))
            .thenReturn(Optional.of(livraisonShipped));

        EbLivraisonLine lineShipped2 = new EbLivraisonLine();
        lineShipped2.setEbDelLivraisonLineNum(10l);
        lineShipped2.setxEbDelLivraison(livraisonShipped);
        lineShipped2.setxEbOrderLigne(orderLine);
        lineShipped2.setQuantity(4l);
        livraisonShipped.getxEbDelLivraisonLine().add(lineShipped2);
        allLinesForOrderLine.add(lineShipped2);
        when(ebLivraisonRepository.findByLivraisonLine(eq(lineShipped2.getEbDelLivraisonLineNum())))
            .thenReturn(Optional.of(livraisonShipped));

        when(ebLivraisonLineRepository.findByxEbOrderLigne(eq(orderLine))).thenReturn(allLinesForOrderLine);

        // Calc
        orderService.calculateQuantitiesForLine(orderLine);

        // Check
        assertThat(orderLine)
            .hasFieldOrPropertyWithValue("quantityOrdered", 100l).hasFieldOrPropertyWithValue("quantityPlannable", 64l)
            .hasFieldOrPropertyWithValue("quantityPlanned", 36l).hasFieldOrPropertyWithValue("quantityPending", 15l)
            .hasFieldOrPropertyWithValue("quantityConfirmed", 3l).hasFieldOrPropertyWithValue("quantityShipped", 6l)
            .hasFieldOrPropertyWithValue("quantityDelivered", 12l).hasFieldOrPropertyWithValue("quantityCanceled", 60l);
    }

    @Test
    public void should_calc_status_order_pending() {
        EbOrder order = new EbOrder(5l);

        order.setTotalQuantityOrdered(6l);
        order.setTotalQuantityPlannable(6l);
        order.setTotalQuantityPlanned(0l);
        order.setTotalQuantityDelivered(0l);

        orderService.calculateStatus(order);

        assertThat(order.getOrderStatus()).isEqualTo(StatusOrder.PENDING.getCode());
    }

    @Test
    public void should_calc_status_order_notdlv() {
        EbOrder order1 = new EbOrder(5l);

        order1.setTotalQuantityOrdered(6l);
        order1.setTotalQuantityPlannable(0l);
        order1.setTotalQuantityPlanned(6l);
        order1.setTotalQuantityPending(6l);
        order1.setTotalQuantityDelivered(0l);

        orderService.calculateStatus(order1);
        assertThat(order1.getOrderStatus()).isEqualTo(StatusOrder.NOTDLV.getCode());

        EbOrder order2 = new EbOrder(5l);

        order2.setTotalQuantityOrdered(6l);
        order2.setTotalQuantityPlannable(5l);
        order2.setTotalQuantityPlanned(1l);
        order2.setTotalQuantityPending(0l);
        order2.setTotalQuantityDelivered(0l);

        orderService.calculateStatus(order2);
        assertThat(order2.getOrderStatus()).isEqualTo(StatusOrder.NOTDLV.getCode());
    }

    @Test
    public void should_calc_status_order_partialdlv() {
        EbOrder order = new EbOrder(5l);

        order.setTotalQuantityOrdered(6l);
        order.setTotalQuantityPlannable(0l);
        order.setTotalQuantityPlanned(6l);
        order.setTotalQuantityPending(5l);
        order.setTotalQuantityDelivered(1l);

        orderService.calculateStatus(order);
        assertThat(order.getOrderStatus()).isEqualTo(StatusOrder.PARTIALDLV.getCode());
    }

    @Test
    public void should_calc_status_order_tdlv() {
        EbOrder order = new EbOrder(5l);

        order.setTotalQuantityOrdered(6l);
        order.setTotalQuantityPlanned(6l);
        order.setTotalQuantityPlannable(0l);
        order.setTotalQuantityPending(0l);
        order.setTotalQuantityDelivered(6l);

        orderService.calculateStatus(order);

        assertThat(order.getOrderStatus()).isEqualTo(StatusOrder.TDLV.getCode());
    }
}
