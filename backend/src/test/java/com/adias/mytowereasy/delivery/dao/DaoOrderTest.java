package com.adias.mytowereasy.delivery.dao;

import static com.adias.mytowereasy.tests.db.CommonDbOperations.EACH_TESTS_OPERATION;
import static com.ninja_squad.dbsetup.Operations.insertInto;
import static com.ninja_squad.dbsetup.Operations.sequenceOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;
import com.ninja_squad.dbsetup.operation.Operation;

import com.adias.mytowereasy.MyTowerEasyApplication;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.repository.EbOrderRepository;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.tests.core.DatabaseTestCore;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyTowerEasyApplication.class, webEnvironment = RANDOM_PORT)
@ActiveProfiles("unittest")
public class DaoOrderTest extends DatabaseTestCore {
    @Autowired
    EbOrderRepository ebOrderRepository;

    @Autowired
    DaoOrder daoOrder;

    @Before
    public void setUp() {
        Operation dbSetUp = sequenceOf(EACH_TESTS_OPERATION, insertData);
        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), dbSetUp);
        dbSetupTracker.launchIfNecessary(dbSetup);
    }

    @Test
    public void should_create_new_order() {
        // Prepare
        EbOrder orderToSave = new EbOrder();
        orderToSave.setNumOrderCustomer("Cust1Created");

        EbParty party1 = new EbParty();
        party1.setCity("Casablanca");
        orderToSave.setxEbPartyOrigin(party1);
        EbParty party2 = new EbParty();
        party2.setCity("Paris");
        orderToSave.setxEbPartyDestination(party2);
        EbParty party3 = new EbParty();
        party3.setCity("Nouakchott");
        orderToSave.setxEbPartySale(party3);

        orderToSave.setOrderLines(new ArrayList<>());

        EbOrderLine newOrderLine1 = new EbOrderLine();
        newOrderLine1.setRefArticle("created1");

        newOrderLine1.setListCustomsFields(new ArrayList<>());
        CustomFields cf1 = new CustomFields();
        cf1.setLabel("cf1");
        cf1.setName("label1");
        cf1.setNum(1);
        cf1.setValue("valueCf1");
        newOrderLine1.getListCustomsFields().add(cf1);

        orderToSave.getOrderLines().add(newOrderLine1);

        EbOrderLine newOrderLine2 = new EbOrderLine();
        newOrderLine2.setRefArticle("created2");
        orderToSave.getOrderLines().add(newOrderLine2);

        orderToSave.setxEbCompagnie(new EbCompagnie(-1));

        orderToSave.setxEbEtablissement(new EbEtablissement(-1));

        orderToSave.setListCustomsFields(new ArrayList<>());
        orderToSave.getListCustomsFields().add(cf1);

        // Call
        EbOrder savedOrder = daoOrder.saveOrder(orderToSave);

        // Check
        assertThat(savedOrder).isNotNull();
        assertThat(savedOrder.getNumOrderCustomer()).isEqualTo("Cust1Created");
        assertThat(savedOrder.getEbDelOrderNum()).isNotNull();
        assertThat(savedOrder.getCustomFields()).isNotBlank();

        assertThat(savedOrder.getOrderLines()).isNotNull().hasSize(2);
        assertThat(savedOrder.getOrderLines().get(0).getEbDelOrderLineNum()).isNotNull();
        assertThat(savedOrder.getOrderLines().get(0).getCustomFields()).isNotBlank();
        assertThat(savedOrder.getOrderLines().get(1).getEbDelOrderLineNum()).isNotNull();

        assertThat(savedOrder.getxEbPartyOrigin()).isNotNull();
        assertThat(savedOrder.getxEbPartyOrigin().getEbPartyNum()).isNotNull();
        assertThat(savedOrder.getxEbPartyOrigin().getCity()).isEqualTo("Casablanca");

        assertThat(savedOrder.getxEbPartyDestination()).isNotNull();
        assertThat(savedOrder.getxEbPartyDestination().getEbPartyNum()).isNotNull();
        assertThat(savedOrder.getxEbPartyDestination().getCity()).isEqualTo("Paris");

        assertThat(savedOrder.getxEbPartySale()).isNotNull();
        assertThat(savedOrder.getxEbPartySale().getEbPartyNum()).isNotNull();
        assertThat(savedOrder.getxEbPartySale().getCity()).isEqualTo("Nouakchott");
        assertThat(savedOrder.getxEbCompagnie()).isNotNull().hasFieldOrPropertyWithValue("ebCompagnieNum", -1);
        assertThat(savedOrder.getxEbEtablissement()).isNotNull().hasFieldOrPropertyWithValue("ebEtablissementNum", -1);
    }

    @Test
    public void should_update_order() {
        // Prepare
        EbOrder originalOrder = ebOrderRepository.findById(1001l).get();
        originalOrder.setNumOrderCustomer("Cust1Edited");
        originalOrder.getOrderLines().remove(2);
        originalOrder.getOrderLines().remove(0);

        EbOrderLine newOrderLine1 = new EbOrderLine();
        newOrderLine1.setRefArticle("added4");
        originalOrder.getOrderLines().add(newOrderLine1);
        EbOrderLine newOrderLine2 = new EbOrderLine();
        newOrderLine2.setRefArticle("added5");
        originalOrder.getOrderLines().add(newOrderLine2);

        // Call
        EbOrder editedOrder = daoOrder.saveOrder(originalOrder);

        // Check
        assertThat(editedOrder).isNotNull();
        assertThat(editedOrder.getNumOrderCustomer()).isEqualTo("Cust1Edited");
        assertThat(editedOrder.getOrderLines()).isNotNull().hasSize(3);

        assertThat(editedOrder.getOrderLines().get(0).getRefArticle()).isEqualTo("ref2");
        assertThat(editedOrder.getOrderLines().get(0).getEbDelOrderLineNum()).isEqualTo(1002);
    }

    private Operation insertData = sequenceOf(
        insertInto("work.eb_party")
            .columns("eb_party_num", "city", "x_eb_etablissement_adresse").values(1001, "Casablanca", -1)
            .values(1002, "Paris", -1).values(1003, "Nouakchott", -1).build(),
        insertInto("work.eb_del_order")
            .columns(
                "eb_del_order_num",
                "num_order_customer",
                "x_eb_etablissement",
                "x_eb_compagnie",
                "x_eb_party_origin",
                "x_eb_party_destination",
                "x_eb_party_sale")
            .values(1001, "CUSTST1", -1, -1, 1001, 1002, 1003).build(),
        insertInto("work.eb_del_order_line")
            .columns("eb_del_order_line_num", "ref_article", "x_eb_del_order").values(1001, "ref1", 1001)
            .values(1002, "ref2", 1001).values(1003, "ref3", 1001).build());
}
