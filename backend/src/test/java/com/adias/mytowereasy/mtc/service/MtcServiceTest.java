package com.adias.mytowereasy.mtc.service;

import com.adias.moteur.chiffrage.library.calcul.Column;
import com.adias.moteur.chiffrage.library.shared.Enumeration;
import com.adias.moteur.chiffrage.library.shared.ResultEntity;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.mtc.model.PrMtcMappingParam;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(MockitoJUnitRunner.class)
public class MtcServiceTest {
    @InjectMocks
    private MtcService mtcService;

    EbDemande demande = new EbDemande();

    EbMarchandise marchandise = new EbMarchandise();
    PrMtcMappingParam param = new PrMtcMappingParam(
            1L,
            "34",
            "Airport Destination",
            "ebDemande.ebPartyDest.airport",
            Enumeration.TypeData.STRING);


    @Before
    public void SetUp() {
        EbParty dest = new EbParty();
        dest.setAirport("XYZ");
        demande.setEbPartyDest(dest);
        marchandise.setWidth(1.0);
        marchandise.setEbDemande(demande);
    }

    @Test
    public void getValuation_ShouldThrowExceptionIfTRNotValid() {
        assertThatThrownBy(() -> mtcService.getValuation(null, "34"))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void getValuation_ShouldThrowExceptionIfCodeNotValid() {
        assertThatThrownBy(() -> mtcService.getValuation(new EbMarchandise(), null))
                .isInstanceOf(IllegalArgumentException.class);
    }


    //<editor-fold desc="Mapping Tests">


    @Test
    public void mapToMtcInput_ShouldThrowExceptionIfParamNotValid() {
        assertThatThrownBy(() -> mtcService.mapToMtcInput(null, marchandise))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void mapToMtcInput_ShouldThrowExceptionIfMarchandiseNotValid1() {
        assertThatThrownBy(() -> mtcService.mapToMtcInput(new PrMtcMappingParam(), null))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void mapToMtcInput_ShouldThrowExceptionIfMarchandiseNotValid2() {
        assertThatThrownBy(() -> mtcService.mapToMtcInput(new PrMtcMappingParam(), new EbMarchandise()))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    public void mapToMtcInput_ShouldReturnRowContainingDestAirport() {
        Column column = mtcService.mapToMtcInput(param, marchandise);
        assertThat(column.getLibelle()).isEqualTo("Airport Destination");
        assertThat(column.getValue()).isEqualTo("XYZ");
    }

    @Test
    public void mapToMtcInput_ShouldReturnRowContainingNullIfMtgAttEmpty() {
        marchandise.setWidth(1.0);
        param.setMtcAttribute("Largeur produit");
        param.setMtgAttribute("");
        Column column = mtcService.mapToMtcInput(param, marchandise);
        assertThat(column.getLibelle()).isEqualTo(param.getMtcAttribute());
        assertThat(column.getValue()).isNull();
    }

    @Test
    public void mapToMtcInput_ShouldReturnSplitService() {
        demande.setTypeRequestLibelle("ESS3-ESS3");
        param.setMtcAttribute("Service");
        param.setMtgAttribute("ebDemande.typeRequestLibelle.split('-')[0]");
        Column column = mtcService.mapToMtcInput(param, marchandise);
        assertThat(column.getLibelle()).isEqualTo(param.getMtcAttribute());
        assertThat(column.getValue()).isEqualTo("ESS3");
    }
    @Test
    public void mapToMtcInput_ShouldReturnDemandeCustomFieldsValue() throws JsonProcessingException {
        CustomFields customFields = new CustomFields();
        customFields.setName("test");
        customFields.setValue("f.value");
        demande.setCustomFields(new ObjectMapper().writeValueAsString(Lists.newArrayList(customFields)));
        param.setMtcAttribute("test");
        param.setMtgAttribute("ebDemande.custom_test");
        Column column = mtcService.mapToMtcInput(param, marchandise);
        assertThat(column.getLibelle()).isEqualTo(param.getMtcAttribute());
        assertThat(column.getValue()).isEqualTo("f.value");
    }
    @Test
    public void mapToMtcInput_ShouldReturnMarchandiseCustomFieldsValue() throws JsonProcessingException {
        CustomFields customFields = new CustomFields();
        customFields.setName("test");
        customFields.setValue("f.value");
        marchandise.setCustomFields(new ObjectMapper().writeValueAsString(Lists.newArrayList(customFields)));
        param.setMtcAttribute("test");
        param.setMtgAttribute("custom_test");
        Column column = mtcService.mapToMtcInput(param, marchandise);
        assertThat(column.getLibelle()).isEqualTo(param.getMtcAttribute());
        assertThat(column.getValue()).isEqualTo("f.value");
    }

    //</editor-fold>
}
