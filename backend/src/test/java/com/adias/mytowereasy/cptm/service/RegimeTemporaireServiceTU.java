package com.adias.mytowereasy.cptm.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.adias.mytowereasy.cptm.dto.EbCptmRegimeTemporaireDTO;
import com.adias.mytowereasy.cptm.model.EbCptmRegimeTemporaire;
import com.adias.mytowereasy.cptm.repository.EbCptmRegimeTemporaireRepository;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.ConnectedUserService;


@RunWith(MockitoJUnitRunner.class)
public class RegimeTemporaireServiceTU {
    @Mock
    EbCptmRegimeTemporaireRepository ebCptmRegimeTemporaireRepository;

    @Mock
    ConnectedUserService connectedUserService;

    @InjectMocks
    RegimeTemporaireServiceImpl regimeTemporaireService;

    @Before
    public void setUp() {
        reset(ebCptmRegimeTemporaireRepository);
    }

    @Test
    public void should_save_regime_with_correctly_ordered_alerts() {
        // Mock
        when(ebCptmRegimeTemporaireRepository.save(any())).then(returnsFirstArg());
        when(connectedUserService.getCurrentUser()).thenReturn(new EbUser());

        // Prepare
        EbCptmRegimeTemporaireDTO dto = new EbCptmRegimeTemporaireDTO();
        dto.setAlert1Days(30);
        dto.setAlert2Days(20);
        dto.setAlert3Days(10);

        // Call
        EbCptmRegimeTemporaire entity = regimeTemporaireService.saveRegime(dto);

        // Check
        assertThat(entity).isNotNull();
        assertThat(entity.getAlert1Days()).isEqualTo(30);
        assertThat(entity.getAlert2Days()).isEqualTo(20);
        assertThat(entity.getAlert3Days()).isEqualTo(10);
    }

    @Test
    public void should_save_regime_with_uncorrectly_ordered_alerts() {
        // Mock
        when(ebCptmRegimeTemporaireRepository.save(any())).then(returnsFirstArg());
        when(connectedUserService.getCurrentUser()).thenReturn(new EbUser());

        // Prepare
        EbCptmRegimeTemporaireDTO dto = new EbCptmRegimeTemporaireDTO();
        dto.setAlert1Days(30);
        dto.setAlert2Days(10);
        dto.setAlert3Days(20);

        // Call
        EbCptmRegimeTemporaire entity = regimeTemporaireService.saveRegime(dto);

        // Check
        assertThat(entity).isNotNull();
        assertThat(entity.getAlert1Days()).isEqualTo(30);
        assertThat(entity.getAlert2Days()).isEqualTo(20);
        assertThat(entity.getAlert3Days()).isEqualTo(10);
    }

    @Test
    public void should_save_regime_with_uncorrectly_ordered_and_null_alerts() {
        // Mock
        when(ebCptmRegimeTemporaireRepository.save(any())).then(returnsFirstArg());
        when(connectedUserService.getCurrentUser()).thenReturn(new EbUser());

        // Prepare
        EbCptmRegimeTemporaireDTO dto = new EbCptmRegimeTemporaireDTO();
        dto.setAlert1Days(20);
        dto.setAlert2Days(null);
        dto.setAlert3Days(10);

        // Call
        EbCptmRegimeTemporaire entity = regimeTemporaireService.saveRegime(dto);

        // Check
        assertThat(entity).isNotNull();
        assertThat(entity.getAlert1Days()).isEqualTo(20);
        assertThat(entity.getAlert2Days()).isEqualTo(10);
        assertThat(entity.getAlert3Days()).isEqualTo(null);
    }

    @Test
    public void should_save_regime_with_uncorrectly_ordered_and_nulls_alerts() {
        // Mock
        when(ebCptmRegimeTemporaireRepository.save(any())).then(returnsFirstArg());
        when(connectedUserService.getCurrentUser()).thenReturn(new EbUser());

        // Prepare
        EbCptmRegimeTemporaireDTO dto = new EbCptmRegimeTemporaireDTO();
        dto.setAlert1Days(null);
        dto.setAlert2Days(null);
        dto.setAlert3Days(10);

        // Call
        EbCptmRegimeTemporaire entity = regimeTemporaireService.saveRegime(dto);

        // Check
        assertThat(entity).isNotNull();
        assertThat(entity.getAlert1Days()).isEqualTo(10);
        assertThat(entity.getAlert2Days()).isEqualTo(null);
        assertThat(entity.getAlert3Days()).isEqualTo(null);
    }

    @Test
    public void should_update_regime_and_set_null_alerts() {
        // Mock
        EbCptmRegimeTemporaire fakeEntity = new EbCptmRegimeTemporaire();
        fakeEntity.setAlert1Days(10);
        fakeEntity.setAlert2Days(10);
        fakeEntity.setAlert3Days(10);
        when(ebCptmRegimeTemporaireRepository.findById(eq(1))).thenReturn(Optional.of(fakeEntity));

        when(ebCptmRegimeTemporaireRepository.save(any())).then(returnsFirstArg());
        when(connectedUserService.getCurrentUser()).thenReturn(new EbUser());

        // Prepare
        EbCptmRegimeTemporaireDTO dto = new EbCptmRegimeTemporaireDTO();
        dto.setEbCptmRegimeTemporaireNum(1);
        dto.setAlert1Days(null);
        dto.setAlert2Days(null);
        dto.setAlert3Days(null);

        // Call
        EbCptmRegimeTemporaire entity = regimeTemporaireService.saveRegime(dto);

        // Check
        assertThat(entity).isNotNull();
        assertThat(entity.getAlert1Days()).isEqualTo(null);
        assertThat(entity.getAlert2Days()).isEqualTo(null);
        assertThat(entity.getAlert3Days()).isEqualTo(null);
    }
}
