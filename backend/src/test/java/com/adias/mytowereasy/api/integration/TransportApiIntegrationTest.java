package com.adias.mytowereasy.api.integration;

import static com.adias.mytowereasy.tests.db.CommonDbOperations.EACH_TESTS_OPERATION;
import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;

import com.adias.mytowereasy.MyTowerEasyApplication;
import com.adias.mytowereasy.api.wso.AddressWSO;
import com.adias.mytowereasy.api.wso.TransportWSO;
import com.adias.mytowereasy.api.wso.UnitWSO;
import com.adias.mytowereasy.tests.core.ApiTestCore;
import com.adias.mytowereasy.tests.utils.RestAssuredUtils;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyTowerEasyApplication.class, webEnvironment = RANDOM_PORT)
@ActiveProfiles("unittest")
public class TransportApiIntegrationTest extends ApiTestCore {
    private static String API_URL = "/api/v1/transport/";

    // Common test models
    private TransportWSO TRANSPORT_VALID;
    // END Common test models

    @Before
    public void setUp() {
        generateCommonTestModels();
        RestAssuredUtils.applyRestAssuredConfig(port);

        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), EACH_TESTS_OPERATION);
        dbSetupTracker.launchIfNecessary(dbSetup);
    }

    //// Some Exception Handler tests
    @Test
    public void should_refuse_without_token() {
        given()
            .header(HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).body(TRANSPORT_VALID).post(API_URL).then().assertThat()
            .statusCode(403).log().ifError().and().log().ifValidationFails();
    }
    //// END Some Exception Handler tests

    private void generateCommonTestModels() {
        TRANSPORT_VALID = new TransportWSO();
        TRANSPORT_VALID.setShipperUserEmail("mytower2018+unittest-1@gmail.com");
        TRANSPORT_VALID.setTransportMode("AIR");
        TRANSPORT_VALID.setInvoicingCurrencyCode("EUR");
        TRANSPORT_VALID.setServiceLevel("STD");
        TRANSPORT_VALID.setFlowTypeCode("FL1");
        TRANSPORT_VALID.setIncoterm("INC1");
        TRANSPORT_VALID.setTrackingPointSchema("CIF");
        TRANSPORT_VALID.setGoodsAvailabilityDate(new Date(System.currentTimeMillis()));

        AddressWSO partyOrigin = new AddressWSO();
        partyOrigin.setReference("RefOrigin1");
        partyOrigin.setCity("Paris");
        partyOrigin.setCountryCode("FR");
        TRANSPORT_VALID.setOriginInformation(partyOrigin);

        AddressWSO partyDest = new AddressWSO();
        partyDest.setReference("RefOrigin1");
        partyDest.setCity("Rome");
        partyDest.setCountryCode("IT");
        TRANSPORT_VALID.setDestinationInformation(partyDest);

        TRANSPORT_VALID.setUnitList(new ArrayList<>());
        UnitWSO unit1 = new UnitWSO();
        unit1.setWeight(2.0);
        unit1.setLength(50.0);
        unit1.setWidth(20.0);
        unit1.setHeigth(10.0);
        unit1.setNumberOfUnits(1);
        unit1.setDangerousGood("NO");
        unit1.setUnitType("Parcel");
        TRANSPORT_VALID.getUnitList().add(unit1);

        TRANSPORT_VALID.setCity("Paris");
    }
}
