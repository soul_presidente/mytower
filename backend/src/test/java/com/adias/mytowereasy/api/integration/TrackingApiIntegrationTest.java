package com.adias.mytowereasy.api.integration;

import static com.adias.mytowereasy.tests.db.CommonDbOperations.EACH_TESTS_OPERATION;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.ninja_squad.dbsetup.DbSetup;
import com.ninja_squad.dbsetup.destination.DataSourceDestination;

import com.adias.mytowereasy.MyTowerEasyApplication;
import com.adias.mytowereasy.api.wso.TrackingEventWSO;
import com.adias.mytowereasy.api.wso.TransportWSO;
import com.adias.mytowereasy.tests.core.ApiTestCore;
import com.adias.mytowereasy.tests.utils.RestAssuredUtils;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyTowerEasyApplication.class, webEnvironment = RANDOM_PORT)
@ActiveProfiles("unittest")
public class TrackingApiIntegrationTest extends ApiTestCore {
    private static String API_URL = "/api/v1/tracking/";

    // Common test models
    private TrackingEventWSO TRACK_EVENT_VALID;
    // END Common test models

    @Before
    public void setUp() {
        generateCommonTestModels();
        RestAssuredUtils.applyRestAssuredConfig(port);

        DbSetup dbSetup = new DbSetup(new DataSourceDestination(dataSource), EACH_TESTS_OPERATION);
        dbSetupTracker.launchIfNecessary(dbSetup);
    }

    //// Some Exception Handler tests
    @Test
    public void should_refuse_without_token() {
        given()
            .header(HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).body(TRACK_EVENT_VALID).post(API_URL).then().assertThat()
            .statusCode(403).log().ifError().and().log().ifValidationFails();
    }

    @Test
    public void should_refuse_with_invalid_data() {
        given()
            .header(HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).header(HEADER_AUTH, TOKEN_PREFIX + TOKEN_MTO)
            .body(new TransportWSO()).post(API_URL).then().assertThat().statusCode(400).and().body("size()", not(0))
            .body("get(0)", containsString("Field error")).log().ifError().and().log().ifValidationFails();
    }
    //// END Some Exception Handler tests

    private void generateCommonTestModels() {
        TRACK_EVENT_VALID = new TrackingEventWSO();
        TRACK_EVENT_VALID.setRefTransport("R0001");
        TRACK_EVENT_VALID.setCodeCountry("FR");
        TRACK_EVENT_VALID.setCodePsl("PSL1");
        TRACK_EVENT_VALID.setCarrierCode("CMP");
        TRACK_EVENT_VALID.setTypeEvent("EV1");
    }
}
