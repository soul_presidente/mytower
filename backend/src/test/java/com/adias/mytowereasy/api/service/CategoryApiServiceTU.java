package com.adias.mytowereasy.api.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.api.wso.CategoryWSO;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.repository.EbCategorieRepository;


@RunWith(MockitoJUnitRunner.class)
public class CategoryApiServiceTU {
    @Mock
    EbCategorieRepository ebCategorieRepository;

    @InjectMocks
    CategoryApiService categoryApiService;

    private List<EbCategorie> categories1;

    @Before
    public void setUp() throws JsonProcessingException {
        reset(ebCategorieRepository);
        this.generateCommonObjects();
    }

    @Test
    public void should_generate_categories_from_WSO() {
        // Prepare
        when(ebCategorieRepository.findByCompagnieEbCompagnieNum(any())).thenReturn(categories1);

        List<CategoryWSO> wsoList = new ArrayList<>();
        CategoryWSO cat = new CategoryWSO();
        cat.setCategory("libelle1");
        cat.setCategoryLabel("label2");
        wsoList.add(cat);

        // Call
        List<EbCategorie> result = categoryApiService.generateCategoriesFromWSO(wsoList, 2);

        // Check
        assertThat(result).isNotNull().hasSize(1);
        assertThat(result.get(0)).hasFieldOrPropertyWithValue("libelle", "libelle1");
        assertThat(result.get(0).getLabels()).isNotNull().hasSize(1);
        assertThat(new ArrayList<>(result.get(0).getLabels()).get(0)).hasFieldOrPropertyWithValue("libelle", "label2");
    }

    @Test(expected = MyTowerException.class)
    public void should_generate_categories_from_WSO_fail_due_label_doesntExist() {
        // Prepare
        when(ebCategorieRepository.findByCompagnieEbCompagnieNum(any())).thenReturn(categories1);

        List<CategoryWSO> wsoList = new ArrayList<>();
        CategoryWSO cat = new CategoryWSO();
        cat.setCategory("libelle1");
        cat.setCategoryLabel("labelFAKE");
        wsoList.add(cat);

        // Call
        categoryApiService.generateCategoriesFromWSO(wsoList, 2);
    }

    @Test(expected = MyTowerException.class)
    public void should_generate_categories_from_WSO_fail_due_categorie_doesntExist() {
        // Prepare
        when(ebCategorieRepository.findByCompagnieEbCompagnieNum(any())).thenReturn(categories1);

        List<CategoryWSO> wsoList = new ArrayList<>();
        CategoryWSO cat = new CategoryWSO();
        cat.setCategory("libelleFAKE");
        cat.setCategoryLabel("label2");
        wsoList.add(cat);

        // Call
        categoryApiService.generateCategoriesFromWSO(wsoList, 2);
    }

    private void generateCommonObjects() {
        categories1 = new ArrayList<>();

        // Cat 1
        EbCategorie cat = new EbCategorie();
        cat.setLibelle("libelle1");
        cat.setLabels(new HashSet<>());

        EbLabel label = new EbLabel();
        label.setLibelle("label1");
        cat.getLabels().add(label);

        label = new EbLabel();
        label.setLibelle("label2");
        cat.getLabels().add(label);

        categories1.add(cat);

        // Cat 2
        cat = new EbCategorie();
        cat.setLibelle("libelle2");
        cat.setLabels(new HashSet<>());

        label = new EbLabel();
        label.setLibelle("label3");
        cat.getLabels().add(label);

        categories1.add(cat);
    }
}
