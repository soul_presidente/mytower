package com.adias.mytowereasy.api.integration;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.adias.mytowereasy.MyTowerEasyApplication;
import com.adias.mytowereasy.tests.core.ApiTestCore;
import com.adias.mytowereasy.tests.utils.RestAssuredUtils;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyTowerEasyApplication.class, webEnvironment = RANDOM_PORT)
@ActiveProfiles("unittest")
public class CoreTI extends ApiTestCore {
    @Before
    public void setUp() {
        RestAssuredUtils.applyRestAssuredConfig(port);
    }

    @Test
    public void should_handle_mytowerexception() {
        given()
            .header(HEADER_CONTENT_TYPE, CONTENT_TYPE_JSON).header(HEADER_AUTH, TOKEN_PREFIX + TOKEN_MTO)
            .get("test/my-tower-exception").then().assertThat().statusCode(406).and()
            .body(containsString("Test exception")).log().ifError().and().log().ifValidationFails();
    }
}
