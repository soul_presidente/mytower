package com.adias.mytowereasy.api.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.service.AdresseServiceImpl;


@RunWith(MockitoJUnitRunner.class)
public class AdresseServiceTU {
    @InjectMocks
    AdresseServiceImpl adresseService;

    @Test
    public void should_compare_party_success() {
        // Prepare
        EbParty party1 = new EbParty();
        party1.setCity("city1");
        party1.setCompany("company1");
        party1.setAdresse("adresse1");
        party1.setZipCode("zipCode1");

        EbParty party2 = new EbParty();
        party2.setCity("city1");
        party2.setCompany("company1");
        party2.setAdresse("adresse1");
        party2.setZipCode("zipCode1");

        boolean result = adresseService.areSameAddresses(party1, party2);

        assertThat(result).isTrue();
    }

    @Test
    public void should_compare_party_fail() {
        EbParty party1 = new EbParty();
        party1.setCity("city1");
        party1.setCompany("company1");
        party1.setAdresse("adresse1");
        party1.setZipCode("zipCode1");

        EbParty party2 = new EbParty();
        party2.setCity("city1");
        party2.setCompany("company1");
        party2.setAdresse("adresse1");
        party2.setZipCode("zipCode1");

        // Check By City
        party2.setCity("city2");
        boolean result = adresseService.areSameAddresses(party1, party2);
        assertThat(result).isFalse();

        // Check By Company
        party2.setCity("city1");
        party2.setCompany("company2");
        result = adresseService.areSameAddresses(party1, party2);
        assertThat(result).isFalse();

        // Check By Adresse
        party2.setCompany("company1");
        party2.setAdresse("adresse2");
        result = adresseService.areSameAddresses(party1, party2);
        assertThat(result).isFalse();

        // Check By ZipCode
        party2.setAdresse("adresse1");
        party2.setZipCode("zipCode2");
        result = adresseService.areSameAddresses(party1, party2);
        assertThat(result).isFalse();
    }

    @Test
    public void should_compare_party_nullValues() {
        assertThat(adresseService.areSameAddresses(null, null)).isTrue();
        assertThat(adresseService.areSameAddresses(null, new EbParty())).isFalse();
        assertThat(adresseService.areSameAddresses(new EbParty(), null)).isFalse();
    }
}
