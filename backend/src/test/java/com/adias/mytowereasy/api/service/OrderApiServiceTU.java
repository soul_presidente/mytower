package com.adias.mytowereasy.api.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.adias.mytowereasy.api.wso.AddressWSO;
import com.adias.mytowereasy.api.wso.OrderLineWSO;
import com.adias.mytowereasy.api.wso.OrderWSO;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.model.mapper.OrderWSOMapper;
import com.adias.mytowereasy.delivery.repository.EbOrderRepository;
import com.adias.mytowereasy.delivery.service.OrderService;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbIncotermRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.repository.EcCountryRepository;
import com.adias.mytowereasy.repository.EcCurrencyRepository;


@RunWith(MockitoJUnitRunner.class)
public class OrderApiServiceTU {
    @Mock
    TransportApiService transportApiService;

    @Mock
    EcCountryRepository ecCountryRepository;

    @Mock
    EbOrderRepository ebOrderRepository;

    @Mock
    OrderService orderService;

    @Mock
    CustomFieldsApiService customFieldsApiService;

    @Mock
    CategoryApiService categoryApiService;

    @InjectMocks
    OrderApiService orderApiService;

    private EcCountry COUNTRY_FR;
    private List<CustomFields> FAKE_CUSTOM_FIELDS_1;
    private List<EbCategorie> FAKE_CATEGORIES_1;

    @Mock
    EbIncotermRepository ebIncotermRepository;

    @Mock
    EbUserRepository ebUserRepository;

    @Mock
    EcCurrencyRepository ecCurrencyRepository;

    @Mock
    OrderWSOMapper orderWSOMapper;

    @Before
    public void setUp() {
        reset(transportApiService);
        reset(ecCountryRepository);
        reset(ebOrderRepository);
        reset(orderService);
        reset(customFieldsApiService);
        this.generateCommonObjects();
        reset(ebIncotermRepository);
        reset(ebUserRepository);
        reset(ecCurrencyRepository);
    }

    @Test
    public void should_fill_EbOrder_from_wso() {
        // Prepare
        OrderWSO wso = new OrderWSO();
        wso.setTargetDate(new Date());
        wso.setCompanyId(31);
        wso.setEtablissementId(32);
        wso.setCustomFieldList(new ArrayList<>()); // To enable mock custom
                                                   // fields
        wso.setCategoryList(new ArrayList<>()); // To enable mock categories

        OrderLineWSO line1 = new OrderLineWSO();
        line1.setOriginCountryCode("FR");
        line1.setCustomFieldList(new ArrayList<>()); // To enable mock custom
                                                     // fields
        line1.setCategoryList(new ArrayList<>()); // To enable mock custom
                                                  // fields
        wso.setLines(new ArrayList<>());
        wso.getLines().add(line1);

        AddressWSO party1 = new AddressWSO();
        party1.setAddress("party1AddrTest");
        wso.setTo(party1);
        wso.setFrom(party1);
        wso.setSoldTo(party1);

        EbParty partyMocked = new EbParty();
        partyMocked.setAdresse("partyMocked");

        EbIncoterm ebIncoterm = new EbIncoterm();

        EbUser ebUser = new EbUser();

        EcCurrency ecCurrency = new EcCurrency();

        // Mock
        when(transportApiService.fillEbPartyFromAddressWSO(any(), any())).thenReturn(partyMocked);
        when(ecCountryRepository.findFirstByCode(eq("FR"))).thenReturn(COUNTRY_FR);
        when(customFieldsApiService.generateCustomFieldsFromWSO(any(), any())).thenReturn(FAKE_CUSTOM_FIELDS_1);
        when(categoryApiService.generateCategoriesFromWSO(any(), any())).thenReturn(FAKE_CATEGORIES_1);
        // when(ebIncotermRepository.findFirstByLibelleAndXEbCompagnie_ebCompagnieNum(any(),
        // any())).thenReturn(ebIncoterm);
        when(ebUserRepository.findOneByEmailIgnoreCase(any())).thenReturn(ebUser);
        // when(ecCurrencyRepository.findByCodeIgnoreCase(any())).thenReturn(ecCurrency);

        // Call
        EbOrder filledOrder = orderWSOMapper.fillEntityFromWSO(new EbOrder(), wso);

        // Check
        assertThat(filledOrder.getTargetDate()).isNotNull();
        assertThat(filledOrder).isNotNull();
        assertThat(filledOrder.getxEbPartyOrigin()).isNotNull();
        assertThat(filledOrder.getxEbPartyOrigin().getAdresse()).isEqualTo(partyMocked.getAdresse());
        assertThat(filledOrder.getxEbCompagnie()).isNotNull().hasFieldOrPropertyWithValue("ebCompagnieNum", 31);
        assertThat(filledOrder.getxEbEtablissement()).isNotNull().hasFieldOrPropertyWithValue("ebEtablissementNum", 32);
        assertThat(filledOrder.getCustomFields()).isNotBlank();
        assertThat(filledOrder.getListCustomsFields()).isNotNull().hasSize(2);
        assertThat(filledOrder.getListCategories()).isNotNull().hasSize(2);
        assertThat(filledOrder.getOrderLines()).isNotNull().hasSize(1);
        assertThat(filledOrder.getOrderLines().get(0).getxEcCountryOrigin())
            .isNotNull().hasFieldOrPropertyWithValue("code", "FR");
        assertThat(filledOrder.getOrderLines().get(0).getCustomFields()).isNotBlank();
        assertThat(filledOrder.getOrderLines().get(0).getListCustomsFields()).isNotNull().hasSize(2);
        assertThat(filledOrder.getOrderLines().get(0).getListCategories()).isNotNull().hasSize(2);
    }

    @Test
    public void should_get_OrderWSO_from_EbOrder() {
        // Prepare
        EbOrder entity = new EbOrder();
        entity.setCrossDock("cd1");
        entity.setGrossWeight(BigDecimal.valueOf(21.458d));
        entity.setNetWeight(BigDecimal.valueOf(21.458d));

        EbParty party1 = new EbParty();
        party1.setAdresse("party1AddrTest");
        entity.setxEbPartyOrigin(party1);
        entity.setxEbPartyDestination(party1);
        entity.setxEbPartySale(party1);

        EbOrderLine line1 = new EbOrderLine();
        line1.setxEcCountryOrigin(new EcCountry(15, "FR", "France"));
        line1.setRefArticle("refArt1");
        entity.setOrderLines(new ArrayList<>());
        entity.getOrderLines().add(line1);

        AddressWSO partyMocked = new AddressWSO();
        partyMocked.setAddress("partyMocked");

        // Mock
        when(transportApiService.convertEbPartyToAddresseWSO(any())).thenReturn(partyMocked);

        // Call
        OrderWSO convertedOrder = orderWSOMapper.getWSOFromEntity(entity);

        // Check
        assertThat(convertedOrder).isNotNull();
        assertThat(convertedOrder.getFrom()).isNotNull();
        assertThat(convertedOrder.getFrom().getAddress()).isEqualTo(partyMocked.getAddress());
        assertThat(convertedOrder.getLines()).isNotNull().hasSize(1);
        assertThat(convertedOrder.getLines().get(0).getOriginCountryCode()).isEqualTo("FR");
    }

    @Test(expected = MyTowerException.class)
    public void should_save_order_fail_due_orderId_not_exist() {
        // Prepare
        OrderWSO wso = new OrderWSO();
        wso.setItemId(4l);

        // Mock
        when(ebOrderRepository.findById(any())).thenReturn(Optional.empty());

        // Call
        orderApiService.saveOrder(wso);
    }

    @Test
    public void should_save_order_creation() {
        // Prepare
        OrderWSO wso = new OrderWSO();
        EbOrder orderEntity = new EbOrder();
        orderEntity.setEbDelOrderNum(4l);

        EbIncoterm ebIncoterm = new EbIncoterm();

        EbUser ebUser = new EbUser();

        EcCurrency ecCurrency = new EcCurrency();
        // Mock
        when(orderService.updateOrder(any())).thenReturn(orderEntity);

        when(ebUserRepository.findOneByEmailIgnoreCase(any())).thenReturn(ebUser);
        // when(ecCurrencyRepository.findByCodeIgnoreCase(any())).thenReturn(ecCurrency);

        // Call
        OrderWSO wsoCreated = orderApiService.saveOrder(wso);

        // Check
        assertThat(wsoCreated.getItemId()).isEqualTo(orderEntity.getEbDelOrderNum());
    }

    private void generateCommonObjects() {
        COUNTRY_FR = new EcCountry(158, "FR", "France");

        FAKE_CUSTOM_FIELDS_1 = new ArrayList<>();

        CustomFields customField1 = new CustomFields();
        customField1.setName("field1");
        customField1.setLabel("Field 1");
        customField1.setNum(1);
        customField1.setValue("Value 1");
        FAKE_CUSTOM_FIELDS_1.add(customField1);

        CustomFields customField2 = new CustomFields();
        customField2.setName("field2");
        customField2.setLabel("Field 2");
        customField2.setNum(2);
        customField2.setValue("Value 2");
        FAKE_CUSTOM_FIELDS_1.add(customField2);

        FAKE_CATEGORIES_1 = new ArrayList<>();

        EbCategorie cat = new EbCategorie();
        cat.setLibelle("cat1");
        cat.setLabels(new HashSet<>());

        EbLabel label = new EbLabel();
        label.setLibelle("label1");
        label.setEbLabelNum(1);
        cat.getLabels().add(label);

        FAKE_CATEGORIES_1.add(cat);

        cat = new EbCategorie();
        cat.setLibelle("cat2");
        cat.setLabels(new HashSet<>());

        label = new EbLabel();
        label.setLibelle("label2");
        label.setEbLabelNum(2);
        cat.getLabels().add(label);

        FAKE_CATEGORIES_1.add(cat);
    }
}
