package com.adias.mytowereasy.api.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Resource;

import com.adias.mytowereasy.api.controller.TransportApiController;
import com.adias.mytowereasy.api.wso.SearchCriteriaTransportWSO;
import com.adias.mytowereasy.api.wso.TransportWSO;
import com.adias.mytowereasy.model.EbCost;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EbMarchandiseRepository;
import com.adias.mytowereasy.repository.EcCurrencyRepository;
import com.adias.mytowereasy.repository.ExEbDemandeTransporteurRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.PricingService;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@RunWith(MockitoJUnitRunner.class)
public class TransportApiServiceTU {
    // Arrange For TransportApiControllerTest Class
    @InjectMocks
    private TransportApiService transportApiService;
    @Mock
    private PricingService pricingService;
    @Mock
    private EbMarchandiseRepository ebMarchandiseRepository;
    @Mock
    ExEbDemandeTransporteurRepository exEbDemandeTransporteurRepository;
    @Mock
    EcCurrencyRepository ecCurrencyRepository;
    @Mock
    TransportApiController transportApiController;
    @Mock
    private ConnectedUserService connectedUserService;

    @Mock
    EbDemandeRepository ebDemandeRepository;

    private SearchCriteriaTransportWSO criteria;
    private PagedResourcesAssembler assembler;
    private HateoasPageableHandlerMethodArgumentResolver hateoasPageableHandlerMethodArgumentResolver;
    private SearchCriteriaPricingBooking searchCriteriaPricingBooking;
    private List<EbDemande> listDemande;
    private EbDemande ebDemande;
    private int counter;
    List<TransportWSO> listDemandeTransport;

    private EbUser ebUser;
    private EbUser xTransporteur;
    private List<EbMarchandise> listMarchandises;
    private EbMarchandise ebMarchandise;
    private List<ExEbDemandeTransporteur> listExEbDemandeTransporteur;
    private ExEbDemandeTransporteur exEbDemandeTransporteur;
    private List<Integer> listEbDemandeNumGrouped;
    private EbCost ebCost;
    private List<EbCost> ebCostList;
    private EcCurrency ecCurrency;
    private BigDecimal value;

    @Before
    public void setUp() {
        criteria = new SearchCriteriaTransportWSO();
        criteria.setPage(null);
        criteria.setSize(null);
        criteria.setFetchConsolidationInformation(true);
        criteria.setTransportStatusList(Arrays.asList("INP"));
        hateoasPageableHandlerMethodArgumentResolver = new HateoasPageableHandlerMethodArgumentResolver();

        assembler = new PagedResourcesAssembler(hateoasPageableHandlerMethodArgumentResolver, null);
        ebCostList = new ArrayList<>();
        ebCost = new EbCost();
        value = new BigDecimal(2233.4);
        ebCost.setxEcCurrencyNum(13);
        ebCost.setPrice(value);
        ebCost.setLibelle("bbb");
        ebCostList.add(ebCost);
        ebDemande = new EbDemande();
        ebDemande.setEbDemandeNum(111);
        ebDemande.setxEcStatut(Enumeration.StatutDemande.PND.getCode());
        ebDemande.setListAdditionalCost(ebCostList);
        listDemande = new ArrayList<>();
        listDemande.add(ebDemande);
        counter = 1;
        listDemandeTransport = new ArrayList<>();
        ecCurrency = new EcCurrency();
        ecCurrency.setCode("ccc");
        ebUser = new EbUser();
        ebUser.setEbUserNum(-1);
        searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();
        ebMarchandise = new EbMarchandise();
        ebMarchandise.setEbMarchandiseNum(11);
        listMarchandises = new ArrayList<>();
        listMarchandises.add(ebMarchandise);

        listExEbDemandeTransporteur = new ArrayList<>();
        exEbDemandeTransporteur = new ExEbDemandeTransporteur();
        exEbDemandeTransporteur.setExEbDemandeTransporteurNum(11);
        xTransporteur = new EbUser();
        xTransporteur.setEmail("test@test.com");
        exEbDemandeTransporteur.setxTransporteur(xTransporteur);
        listExEbDemandeTransporteur.add(exEbDemandeTransporteur);

        listEbDemandeNumGrouped = new ArrayList<>();
        listEbDemandeNumGrouped.add(1);
    }

    /**
     * If you allocate external resources in a Before method you need to release
     * them after the test runs
     */
    @After
    public void tearDown() {
        criteria = null;
        assembler = null;
        hateoasPageableHandlerMethodArgumentResolver = null;
        searchCriteriaPricingBooking = null;
        listDemande = null;
        ebDemande = null;
        listDemandeTransport = null;
        ebUser = null;
        listMarchandises = null;
        ebMarchandise = null;
        listExEbDemandeTransporteur = null;
        exEbDemandeTransporteur = null;
        listEbDemandeNumGrouped = null;
    }

    @Test
    public void should_get_transport_request_by_searchCriteriaTransportWSO() throws Exception {
        // Arrange
        when(pricingService.getListEbDemande(any())).thenReturn(listDemande);
        when(pricingService.getCountListEbDemande(any())).thenReturn(counter);
        when(ebMarchandiseRepository.findAllByEbDemande_ebDemandeNum(any())).thenReturn(listMarchandises);
        when(exEbDemandeTransporteurRepository.findAllByXEbDemande_ebDemandeNum(any()))
            .thenReturn(listExEbDemandeTransporteur);
        when(ebDemandeRepository.selectDemandeNumInGroup(any())).thenReturn(listEbDemandeNumGrouped);
        when(connectedUserService.getCurrentUser()).thenReturn(ebUser);
        when(ecCurrencyRepository.findById(any())).thenReturn(Optional.of(ecCurrency));

        // Act
        Page<Resource<TransportWSO>> result = transportApiService.getTransportRequest(criteria);

        // Assert
        assertThat(result.getContent().get(0).getContent().getAdditionalCostList().size()).isEqualTo(1);
        assertThat(result.getContent().get(0).getContent().getAdditionalCostList().get(0).getPrice()).isEqualTo(value);
        assertThat(result.getContent().get(0).getContent().getAdditionalCostList().get(0).getLibelle())
            .isEqualTo("bbb");
		assertThat(result.getContent().get(0).getContent().getAdditionalCostList().get(0).getCurrencyCode())
            .isEqualTo("ccc");
        verify(pricingService, times(2)).getListEbDemande(any());
        verify(ebMarchandiseRepository, times(2)).findAllByEbDemande_ebDemandeNum(any());
        verify(exEbDemandeTransporteurRepository, times(2)).findAllByXEbDemande_ebDemandeNum(any());
        verify(ebDemandeRepository, times(1)).selectDemandeNumInGroup(any());
        verify(connectedUserService, times(1)).getCurrentUser();
    }
}
