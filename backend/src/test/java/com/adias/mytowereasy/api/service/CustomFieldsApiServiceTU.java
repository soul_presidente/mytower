package com.adias.mytowereasy.api.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.api.wso.CustomFieldWSO;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCustomField;
import com.adias.mytowereasy.repository.EbCustomFieldRepository;
import com.adias.mytowereasy.util.JsonUtils;


@RunWith(MockitoJUnitRunner.class)
public class CustomFieldsApiServiceTU {
    @Mock
    EbCustomFieldRepository ebCustomFieldRepository;

    @InjectMocks
    CustomFieldsApiService customFieldsApiService;

    // Common objects
    private static EbCustomField fakeEbCustomField;

    @Before
    public void setUp() throws JsonProcessingException {
        reset(ebCustomFieldRepository);
        this.generateCommonObjects();
    }

    @Test
    public void should_generate_customFields_from_WSO() {
        // Prepare
        when(ebCustomFieldRepository.findFirstByXEbCompagnie(any())).thenReturn(fakeEbCustomField);

        // Make a request with 2 of 3 available custom fields
        List<CustomFieldWSO> wsoList = new ArrayList<>();

        CustomFieldWSO wso1 = new CustomFieldWSO();
        wso1.setCode("field1");
        wso1.setValue("valueWSO1");
        wsoList.add(wso1);

        wso1 = new CustomFieldWSO();
        wso1.setCode("field2");
        wso1.setValue("valueWSO2");
        wsoList.add(wso1);

        // Call
        List<CustomFields> result = customFieldsApiService.generateCustomFieldsFromWSO(wsoList, 1);

        // Check
        assertThat(result).isNotNull().hasSize(3);
        assertThat(result.get(0))
            .hasFieldOrPropertyWithValue("name", "field1").hasFieldOrPropertyWithValue("value", "valueWSO1");
        assertThat(result.get(1))
            .hasFieldOrPropertyWithValue("name", "field2").hasFieldOrPropertyWithValue("value", "valueWSO2");
        assertThat(result.get(2))
            .hasFieldOrPropertyWithValue("name", "field3").hasFieldOrPropertyWithValue("value", null);
    }

    @Test(expected = MyTowerException.class)
    public void should_generate_customFields_from_WSO_fail_due_customField_dontExist() {
        // Prepare
        when(ebCustomFieldRepository.findFirstByXEbCompagnie(any())).thenReturn(fakeEbCustomField);

        // Make a request with 21 non existing field
        List<CustomFieldWSO> wsoList = new ArrayList<>();

        CustomFieldWSO wso1 = new CustomFieldWSO();
        wso1.setLabel("labelFAKE");
        wso1.setCode("fieldFAKE");
        wso1.setValue("valueWSO1");
        wsoList.add(wso1);

        wso1 = new CustomFieldWSO();
        wso1.setLabel("label2");
        wso1.setCode("field2");
        wso1.setValue("valueWSO2");
        wsoList.add(wso1);

        // Call
        customFieldsApiService.generateCustomFieldsFromWSO(wsoList, 1);
    }

    @Test(expected = MyTowerException.class)
    public void should_generate_customFields_from_WSO_fail_due_no_customField() {
        // Prepare
        EbCustomField cf = new EbCustomField();
        cf.setFields("[]");
        when(ebCustomFieldRepository.findFirstByXEbCompagnie(any())).thenReturn(cf);

        // Make a request with 21 non existing field
        List<CustomFieldWSO> wsoList = new ArrayList<>();

        CustomFieldWSO wso1 = new CustomFieldWSO();
        wso1.setLabel("labelFAKE");
        wso1.setCode("fieldFAKE");
        wso1.setValue("valueWSO1");
        wsoList.add(wso1);

        wso1 = new CustomFieldWSO();
        wso1.setLabel("label2");
        wso1.setCode("field2");
        wso1.setValue("valueWSO2");
        wsoList.add(wso1);

        // Call
        customFieldsApiService.generateCustomFieldsFromWSO(wsoList, 1);
    }

    private void generateCommonObjects() throws JsonProcessingException {
        fakeEbCustomField = new EbCustomField();

        List<CustomFields> customFields = new ArrayList<>();

        CustomFields cf = new CustomFields();
        cf.setLabel("label1");
        cf.setName("field1");
        cf.setNum(1);
        customFields.add(cf);

        cf = new CustomFields();
        cf.setLabel("label2");
        cf.setName("field2");
        cf.setNum(2);
        customFields.add(cf);

        cf = new CustomFields();
        cf.setLabel("label3");
        cf.setName("field3");
        cf.setNum(3);
        customFields.add(cf);

        fakeEbCustomField.setFields(JsonUtils.serializeToString(customFields));
    }
}
