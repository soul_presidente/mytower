package com.adias.mytowereasy.cronjob.tt.service;

import java.util.List;

import com.adias.mytowereasy.cronjob.tt.model.TmpEvent;
import com.adias.mytowereasy.cronjob.tt.util.CronjobMonitoring;
import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.model.tt.EbTtEvent;


public interface GenerateEventByEdiService {
    public void createTmpEvent(TmpEvent event);

    public void generateTtEventByTmpEvent();

    public EbTtEvent convertTmpEventToTDeviation(TmpEvent tmpEvent);

    public List<EbTTPslApp> convertTmpEventToTtPslApp(TmpEvent tmpEvent);

    public CronjobMonitoring getInfosLastExecutionCronjob();

    public CronjobMonitoring changeStatutCronjob(Boolean status);
}
