package com.adias.mytowereasy.cronjob.tt.model;

import java.lang.reflect.Field;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbTimeStamps;
import com.adias.mytowereasy.model.tt.EbTtCategorieDeviation;


@Table(name = "edi_map_psl")
@Entity
public class EdiMapPsl extends EbTimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ediMapPslNum;

    @Column
    private String codePslTransporteur;

    @Column
    private String codePslNormalise;

    @Column
    private Integer typeEvent; // type d'evenement

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company_psl", insertable = true)
    private EbTtCompanyPsl xEbCompanyPsl;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company_transporteur", insertable = true)
    public EbCompagnie xEbCompanyTransporteur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company", insertable = true)
    public EbCompagnie xEbCompany;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_categorie_deviation")
    public EbTtCategorieDeviation xEbCategorieDeviation;

    @Column
    private Integer numCodeRaison;

    @Column
    private String codeRaison;

    @Column
    private String categorieDeviationLibelle;

    @Column
    private Integer numCodeTransporteur;

    @Column
    private String codePslTransporteurDescriptif;

    @Column
    private String codeRaisonDescriptif;

    @QueryProjection
    public EdiMapPsl() {
        super();
    }

    @QueryProjection
    public EdiMapPsl(
        Integer ediMapPslNum,
        EbCompagnie xEbCompany,
        EbTtCompanyPsl xEbCompanyPsl,
        EbCompagnie xEbCompanyTransporteur,
        EbTtCategorieDeviation xEbCategorieDeviation,
        String codePslTransporteur,
        String codePslNormalise,
        Integer typeEvent,
        Integer numCodeRaison,
        String codeRaison,
        String categorieDeviationLibelle,
        Integer numCodeTransporteur,
        String codePslTransporteurDescriptif,
        String codeRaisonDescriptif,
        Timestamp dateCreationSys,
        Timestamp dateMajSys) {
        super();
        this.ediMapPslNum = ediMapPslNum;
        this.xEbCompany = xEbCompany;
        this.xEbCompanyTransporteur = xEbCompanyTransporteur;
        this.xEbCategorieDeviation = xEbCategorieDeviation;
        this.xEbCompanyPsl = xEbCompanyPsl;
        this.codePslTransporteur = codePslTransporteur;
        this.codePslNormalise = codePslNormalise;
        this.typeEvent = typeEvent;
        this.numCodeRaison = numCodeRaison;
        this.codeRaison = codeRaison;
        this.categorieDeviationLibelle = categorieDeviationLibelle;
        this.numCodeTransporteur = numCodeTransporteur;
        this.codePslTransporteurDescriptif = codePslTransporteurDescriptif;
        this.codeRaisonDescriptif = codeRaisonDescriptif;
        this.dateCreationSys = dateCreationSys;
        this.dateMajSys = dateMajSys;
    }

    public Integer getEdiMapPslNum() {
        return ediMapPslNum;
    }

    public void setEdiMapPslNum(Integer ediMapPslNum) {
        this.ediMapPslNum = ediMapPslNum;
    }

    public String getCodePslTransporteur() {
        return codePslTransporteur;
    }

    public void setCodePslTransporteur(String codePslTransporteur) {
        this.codePslTransporteur = codePslTransporteur;
    }

    public String getCodePslNormalise() {
        return codePslNormalise;
    }

    public void setCodePslNormalise(String codePslNormalise) {
        this.codePslNormalise = codePslNormalise;
    }

    public Integer getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(Integer typeEvent) {
        this.typeEvent = typeEvent;
    }

    public EbTtCompanyPsl getxEbCompanyPsl() {
        return xEbCompanyPsl;
    }

    public void setxEbCompanyPsl(EbTtCompanyPsl xEbCompanyPsl) {
        this.xEbCompanyPsl = xEbCompanyPsl;
    }

    public EbCompagnie getxEbCompanyTransporteur() {
        return xEbCompanyTransporteur;
    }

    public void setxEbCompanyTransporteur(EbCompagnie xEbCompanyTransporteur) {
        this.xEbCompanyTransporteur = xEbCompanyTransporteur;
    }

    public EbCompagnie getxEbCompany() {
        return xEbCompany;
    }

    public void setxEbCompany(EbCompagnie xEbCompany) {
        this.xEbCompany = xEbCompany;
    }

    public EbTtCategorieDeviation getxEbCategorieDeviation() {
        return xEbCategorieDeviation;
    }

    public void setxEbCategorieDeviation(EbTtCategorieDeviation xEbCategorieDeviation) {
        this.xEbCategorieDeviation = xEbCategorieDeviation;
    }

    public String getCodeRaison() {
        return codeRaison;
    }

    public void setCodeRaison(String codeRaison) {
        this.codeRaison = codeRaison;
    }

    public String getCategorieDeviationLibelle() {
        return categorieDeviationLibelle;
    }

    public void setCategorieDeviationLibelle(String categorieDeviationLibelle) {
        this.categorieDeviationLibelle = categorieDeviationLibelle;
    }

    public Integer getNumCodeRaison() {
        return numCodeRaison;
    }

    public void setNumCodeRaison(Integer numCodeRaison) {
        this.numCodeRaison = numCodeRaison;
    }

    public Integer getNumCodeTransporteur() {
        return numCodeTransporteur;
    }

    public void setNumCodeTransporteur(Integer numCodeTransporteur) {
        this.numCodeTransporteur = numCodeTransporteur;
    }

    public String getCodePslTransporteurDescriptif() {
        return codePslTransporteurDescriptif;
    }

    public void setCodePslTransporteurDescriptif(String codePslTransporteurDescriptif) {
        this.codePslTransporteurDescriptif = codePslTransporteurDescriptif;
    }

    public String getCodeRaisonDescriptif() {
        return codeRaisonDescriptif;
    }

    public void setCodeRaisonDescriptif(String codeRaisonDescriptif) {
        this.codeRaisonDescriptif = codeRaisonDescriptif;
    }

    public void setProperty(String property, String value) {
        Field field = null;

        try {
            Class<?> c = this.getClass();
            field = c.getDeclaredField(property);

            try {
                field.set(this, value);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }

        } catch (NoSuchFieldException e1) {
            e1.printStackTrace();
        }

    }
}
