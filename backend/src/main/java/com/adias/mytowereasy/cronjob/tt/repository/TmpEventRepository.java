package com.adias.mytowereasy.cronjob.tt.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.cronjob.tt.model.TmpEvent;


@Repository
public interface TmpEventRepository extends JpaRepository<TmpEvent, Integer> {
    @Query("SELECT e  FROM TmpEvent e  where e.isTreated is null or e.isTreated is false ")
    public Page<TmpEvent> selectTmpEventNotTreated(Pageable pageable);
}
