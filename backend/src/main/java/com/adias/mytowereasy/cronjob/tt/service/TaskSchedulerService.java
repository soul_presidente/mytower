package com.adias.mytowereasy.cronjob.tt.service;

import java.util.Date;


public interface TaskSchedulerService {
    public void activateTaskScheduler();

    public void desactivateTaskScheduler();

    public Boolean taskSchedulerIsShutdown();

    public void scheduleRunnableWithCronTrigger();

    public Date nextExecutionByCronExpression(Date lastExecution);
}
