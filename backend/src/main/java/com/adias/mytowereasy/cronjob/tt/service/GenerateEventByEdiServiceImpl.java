package com.adias.mytowereasy.cronjob.tt.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.cronjob.tt.model.TmpEvent;
import com.adias.mytowereasy.cronjob.tt.util.CronjobMonitoring;
import com.adias.mytowereasy.cronjob.tt.util.CronjobStatique;
import com.adias.mytowereasy.cronjob.tt.util.TmpInfosPsl;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCountry;
import com.adias.mytowereasy.model.Enumeration.TmpEventStatus;
import com.adias.mytowereasy.model.TtEnumeration.NatureReference;
import com.adias.mytowereasy.model.TtEnumeration.TypeEvent;
import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.model.tt.EbTtEvent;


@Service
public class GenerateEventByEdiServiceImpl extends CronjobService implements GenerateEventByEdiService {
    @Autowired
    private TaskSchedulerService taskSchedulerService;

    private Date lastExecution = null;

    private Date nextExecution = null;

    private Boolean jobExecutionFailed = false;

    @Override
    public void createTmpEvent(TmpEvent event) {
        tmpEventRepository.save(event);
    }

    @Override
    public void generateTtEventByTmpEvent() {

        try {
            lastExecution = new Date();

            EbUser ediUser = null;
            Integer page = 0;
            Boolean execute = true;

            Page<TmpEvent> listTmpEvent = null;
            List<TmpEvent> listTmpEventRejected = new ArrayList<TmpEvent>();

            while (execute) {
                listTmpEvent = tmpEventRepository
                    .selectTmpEventNotTreated(PageRequest.of(page, CronjobStatique.MAX_NUMBER_OF_EVENTS_TO_TREAT));

                page++;

                if (page >= listTmpEvent.getTotalPages()) {
                    execute = false;
                }

                List<TmpInfosPsl> listTmpInfosPsl = new ArrayList<TmpInfosPsl>();
                List<TmpInfosPsl> listTmpInfosEvent = new ArrayList<TmpInfosPsl>();

                List<EbTTPslApp> listEbPslApp = new ArrayList<EbTTPslApp>();
                List<EbTtEvent> listTTEvent = new ArrayList<EbTtEvent>();

                List<Integer> listRejectTmpEventNum = new ArrayList<Integer>();
                Set<String> listEmailCreatorPsl = new HashSet<String>();
                Set<String> listEmailCreatorEvent = new HashSet<String>();

                for (TmpEvent tmpEvent: listTmpEvent.getContent()) {

                    if ((tmpEvent.getDateActual() != null || tmpEvent.getDateEstimated() != null
                        || tmpEvent.getDateNegociated() != null)
                        && (tmpEvent.getCustomerReference() != null || tmpEvent.getRefTransport() != null
                            || tmpEvent.getUnitReference() != null)
                        && (tmpEvent.getTypeEvent() == TypeEvent.EVENT_PSL
                            || tmpEvent.getTypeEvent() == TypeEvent.EVENT_DEVIATION)) {
                        Integer numberOfEvents = 0;
                        List<EbTTPslApp> convertTmpEventToTtPslApp = null;
                        EbTtEvent convertTmpEventToTDeviation = null;

                        if (tmpEvent.getTypeEvent() == TypeEvent.EVENT_PSL) {
                            convertTmpEventToTtPslApp = convertTmpEventToTtPslApp(tmpEvent);
                        }
                        else {
                            convertTmpEventToTDeviation = convertTmpEventToTDeviation(tmpEvent);
                        }

                        Integer natureRef = null;

                        // -------------------- la logique de sélection
                        if (tmpEvent.getUnitReference() != null) {
                            natureRef = NatureReference.UNIT_REF.getCode();
                        }
                        else if (tmpEvent.getRefTransport() != null) {
                            natureRef = NatureReference.TRANSPORT_REF.getCode();
                        }
                        else {
                            natureRef = NatureReference.CUSTOMER_REF.getCode();
                        }
                        // ----------------------------------------------------

                        if (tmpEvent.getTypeEvent() == TypeEvent.EVENT_PSL) {
                            listEbPslApp.addAll(convertTmpEventToTtPslApp);
                            numberOfEvents = convertTmpEventToTtPslApp.size();
                        }
                        else {
                            listTTEvent.add(convertTmpEventToTDeviation);
                            numberOfEvents = 1;
                        }

                        TmpInfosPsl tmpInfosPsl;

                        for (int i = 0; i < numberOfEvents; i++) {
                            tmpInfosPsl = new TmpInfosPsl(
                                tmpEvent.getTmpEventNum(),
                                tmpEvent.getUnitReference(),
                                tmpEvent.getRefTransport(),
                                !StringUtils.isBlank(tmpEvent.getRefTransport()) ?
                                    null :
                                    tmpEvent.getCustomerReference(),
                                natureRef,
                                tmpEvent.getCodePslNormalise(),
                                tmpEvent.getCreatorEmail());

                            if (tmpEvent.getTypeEvent() == TypeEvent.EVENT_PSL) {
                                listTmpInfosPsl.add(tmpInfosPsl);
                            }
                            else {
                                listTmpInfosEvent.add(tmpInfosPsl);
                            }

                        }

                        if (tmpEvent.getTypeEvent() == TypeEvent.EVENT_PSL) {
                            listEmailCreatorPsl.add(tmpEvent.getCreatorEmail());
                        }
                        else {
                            listEmailCreatorEvent.add(tmpEvent.getCreatorEmail());
                        }

                    }
                    else {
                        listRejectTmpEventNum.add(tmpEvent.getTmpEventNum());
                    }

                    tmpEvent.setDateTraitement(new Date());
                    tmpEvent.setIsTreated(true);
                    tmpEvent.setStatus(TmpEventStatus.TREATED.getCode());
                }

                // -------- traitement des events PSL
                if (!listTmpInfosPsl.isEmpty()) {
                    List<TmpInfosPsl> listTmpInfosPslByCompagnie = null;
                    List<EbTTPslApp> listEbPslAppByCompagnie = null;
                    List<Integer> listRejectedEvent = null;

                    for (String emailCreator: listEmailCreatorPsl) {
                        listEbPslAppByCompagnie = new ArrayList<>();
                        listTmpInfosPslByCompagnie = new ArrayList<>();

                        for (int i = 0; i < listTmpInfosPsl.size(); i++) {

                            if (listTmpInfosPsl.get(i).getCreatorEmail().equals(emailCreator)) {
                                listTmpInfosPslByCompagnie.add(listTmpInfosPsl.get(i));
                                listEbPslAppByCompagnie.add(listEbPslApp.get(i));
                            }

                        }

                        ediUser = ebUserRepository.findByEmail(emailCreator);// recuperation
                                                                             // du
                                                                             // compte
                                                                             // User
                                                                             // EDI

                        if (ediUser != null) {
                            listRejectedEvent = trackService
                                .updateEbPslAppMasse(listEbPslAppByCompagnie, ediUser, listTmpInfosPslByCompagnie)
                                .getListReject();
                        }
                        else {
                            listRejectedEvent = listTmpInfosPslByCompagnie
                                .stream().map(TmpInfosPsl::getTmpEventNum).collect(Collectors.toList());
                        }

                        if (listRejectedEvent != null && !listRejectedEvent.isEmpty()) {
                            listRejectTmpEventNum.addAll(listRejectedEvent);
                        }

                    }

                }
                // -------- fin traitement des event PSL

                // -------- traitement des events Deviation
                if (!listTmpInfosEvent.isEmpty()) {
                    List<TmpInfosPsl> listTmpInfosEventByCompagnie = null;
                    List<EbTtEvent> listEbTtEventByCompagnie = null;
                    List<Integer> listRejectedEvent = null;

                    for (String emailCreator: listEmailCreatorEvent) {
                        listEbTtEventByCompagnie = new ArrayList<>();
                        listTmpInfosEventByCompagnie = new ArrayList<>();

                        for (int i = 0; i < listTmpInfosEvent.size(); i++) {

                            if (listTmpInfosEvent.get(i).getCreatorEmail().equals(emailCreator)) {
                                listTmpInfosEventByCompagnie.add(listTmpInfosEvent.get(i));
                                listEbTtEventByCompagnie.add(listTTEvent.get(i));
                            }

                        }

                        ediUser = ebUserRepository.findByEmail(emailCreator);// recuperation
                                                                             // du
                                                                             // compte
                                                                             // User
                                                                             // EDI

                        if (ediUser != null) {
                            listRejectedEvent = updateEbTtEvent(
                                listEbTtEventByCompagnie,
                                listTmpInfosEventByCompagnie,
                                ediUser);
                        }
                        else {
                            listRejectedEvent = listTmpInfosEventByCompagnie
                                .stream().map(TmpInfosPsl::getTmpEventNum).collect(Collectors.toList());
                        }

                        if (listRejectedEvent != null && !listRejectedEvent.isEmpty()) {
                            listRejectTmpEventNum.addAll(listRejectedEvent);
                        }

                    }

                }
                // -------- fin traitement des event DEVIATION

                if (listRejectTmpEventNum != null && !listRejectTmpEventNum.isEmpty()) {

                    for (TmpEvent tmpEvent: listTmpEvent.getContent()) {

                        if (listRejectTmpEventNum.contains(tmpEvent.getTmpEventNum())) {
                            // mettre le status a rejected
                            tmpEvent.setStatus(TmpEventStatus.REJECTED.getCode());
                            listTmpEventRejected.add(tmpEvent);
                        }

                    }

                }

                // la generation des deviations/unconformités n'est pas encore
                // traité
                // if (!listTTEvent.isEmpty())
                // {
                // trackService.addDeviationMasse(listTTEvent);
                // }
                if (listTmpEvent.getContent() != null && !listTmpEvent.getContent().isEmpty()) {
                    tmpEventRepository.saveAll(listTmpEvent.getContent());
                }

            }

            // dans le cas d'échec d'exécution alors envoie d'email
            if (listTmpEventRejected != null && !listTmpEventRejected.isEmpty()) {
                getInfosLastExecutionCronjob();

                emailGenerationEventService
                    .sendEmailEchecExecutionJob(lastExecution, nextExecution, listTmpEventRejected);
                jobExecutionFailed = true;
            }
            else {
                jobExecutionFailed = false;
            }

        } catch (Exception e) {
            jobExecutionFailed = false;
            e.printStackTrace();
        }

    }

    private List<Integer>
        updateEbTtEvent(List<EbTtEvent> listEbTtEventByCompagnie, List<TmpInfosPsl> listTmpInfosEvent, EbUser ediUser) {
        List<EbTtEvent> listEbTtEventFinal = new ArrayList<EbTtEvent>();

        List<Integer> listRejectTmpEventNum = new ArrayList<Integer>();

        List<EbTTPslApp> listPslByTransportRefAndUnitRefAndCodeAlpha = null;

        List<EbTTPslApp> listInfosEventFinal = new ArrayList<>();

        // construire la liste finale des événements traités en utilisant la
        // logique sélective

        // cas 1
        // get List tracing By unit ref and transport ref
        List<String> listUnitRef = listTmpInfosEvent
            .stream()
            .filter(
                infoPsl -> infoPsl.getNatureReference().equals(NatureReference.UNIT_REF.getCode())
                    && !StringUtils.isBlank(infoPsl.getRefTransport()))
            .map(infoPsl -> infoPsl.getUnitReference()).filter(Objects::nonNull).collect(Collectors.toList());

        List<String> listCodeAlphaPsl = listTmpInfosEvent
            .stream()
            .filter(
                infoPsl -> infoPsl.getNatureReference().equals(NatureReference.UNIT_REF.getCode())
                    && !StringUtils.isBlank(infoPsl.getRefTransport()))
            .map(infoPsl -> infoPsl.getCodeAlpha()).filter(Objects::nonNull).collect(Collectors.toList());

        List<String> listRef = listTmpInfosEvent
            .stream()
            .filter(
                infoPsl -> infoPsl.getNatureReference().equals(NatureReference.UNIT_REF.getCode())
                    && !StringUtils.isBlank(infoPsl.getRefTransport()))
            .map(infoPsl -> infoPsl.getRefTransport()).filter(Objects::nonNull).collect(Collectors.toList());

        if (listUnitRef != null && !listUnitRef.isEmpty() && listRef != null && !listRef.isEmpty()
            && listCodeAlphaPsl != null && !listCodeAlphaPsl.isEmpty()) {
            listPslByTransportRefAndUnitRefAndCodeAlpha = daoPslApp
                .getListPslAppByRefTransportAndRefUnitAndCodeAlpha(listRef, listCodeAlphaPsl, true, listUnitRef);

            if (listPslByTransportRefAndUnitRefAndCodeAlpha != null
                && !listPslByTransportRefAndUnitRefAndCodeAlpha.isEmpty()) {
                listInfosEventFinal.addAll(listPslByTransportRefAndUnitRefAndCodeAlpha);
            }

        }

        // cas 2
        // get List tracing By unit ref and Customer ref
        listUnitRef = listTmpInfosEvent
            .stream()
            .filter(
                infoPsl -> infoPsl.getNatureReference().equals(NatureReference.UNIT_REF.getCode())
                    && !StringUtils.isBlank(infoPsl.getCustomerReference()))
            .map(infoPsl -> infoPsl.getUnitReference()).filter(Objects::nonNull).collect(Collectors.toList());

        listCodeAlphaPsl = listTmpInfosEvent
            .stream()
            .filter(
                infoPsl -> infoPsl.getNatureReference().equals(NatureReference.UNIT_REF.getCode())
                    && !StringUtils.isBlank(infoPsl.getCustomerReference()))
            .map(infoPsl -> infoPsl.getCodeAlpha()).filter(Objects::nonNull).collect(Collectors.toList());

        listRef = listTmpInfosEvent
            .stream()
            .filter(
                infoPsl -> infoPsl.getNatureReference().equals(NatureReference.UNIT_REF.getCode())
                    && !StringUtils.isBlank(infoPsl.getCustomerReference()))
            .map(infoPsl -> infoPsl.getCustomerReference()).filter(Objects::nonNull).collect(Collectors.toList());

        if (listUnitRef != null && !listUnitRef.isEmpty() && listRef != null && !listRef.isEmpty()
            && listCodeAlphaPsl != null && !listCodeAlphaPsl.isEmpty()) {
            listPslByTransportRefAndUnitRefAndCodeAlpha = daoPslApp
                .getListPslAppByRefTransportAndRefUnitAndCodeAlpha(listRef, listCodeAlphaPsl, false, listUnitRef);

            if (listPslByTransportRefAndUnitRefAndCodeAlpha != null
                && !listPslByTransportRefAndUnitRefAndCodeAlpha.isEmpty()) {
                listInfosEventFinal.addAll(listPslByTransportRefAndUnitRefAndCodeAlpha);
            }

        }

        // cas 3
        // get List tracing By transport ref
        listCodeAlphaPsl = listTmpInfosEvent
            .stream()
            .filter(
                infoPsl -> infoPsl.getNatureReference().equals(NatureReference.TRANSPORT_REF.getCode())
                    && !StringUtils.isBlank(infoPsl.getRefTransport()))
            .map(infoPsl -> infoPsl.getCodeAlpha()).filter(Objects::nonNull).collect(Collectors.toList());

        listRef = listTmpInfosEvent
            .stream()
            .filter(
                infoPsl -> infoPsl.getNatureReference().equals(NatureReference.TRANSPORT_REF.getCode())
                    && !StringUtils.isBlank(infoPsl.getRefTransport()))
            .map(infoPsl -> infoPsl.getRefTransport()).filter(Objects::nonNull).collect(Collectors.toList());

        if (listRef != null && !listRef.isEmpty() && listCodeAlphaPsl != null && !listCodeAlphaPsl.isEmpty()) {
            listPslByTransportRefAndUnitRefAndCodeAlpha = daoPslApp
                .getListPslAppByRefTransportAndRefUnitAndCodeAlpha(listRef, listCodeAlphaPsl, true, null);

            if (listPslByTransportRefAndUnitRefAndCodeAlpha != null
                && !listPslByTransportRefAndUnitRefAndCodeAlpha.isEmpty()) {
                listInfosEventFinal.addAll(listPslByTransportRefAndUnitRefAndCodeAlpha);
            }

        }

        // cas 4
        // get List tracing By customer ref
        listCodeAlphaPsl = listTmpInfosEvent
            .stream()
            .filter(
                infoPsl -> infoPsl.getNatureReference().equals(NatureReference.CUSTOMER_REF.getCode())
                    && !StringUtils.isBlank(infoPsl.getCustomerReference()))
            .map(infoPsl -> infoPsl.getCodeAlpha()).filter(Objects::nonNull).collect(Collectors.toList());

        listRef = listTmpInfosEvent
            .stream()
            .filter(
                infoPsl -> infoPsl.getNatureReference().equals(NatureReference.CUSTOMER_REF.getCode())
                    && !StringUtils.isBlank(infoPsl.getCustomerReference()))
            .map(infoPsl -> infoPsl.getCustomerReference()).filter(Objects::nonNull).collect(Collectors.toList());

        if (listRef != null && !listRef.isEmpty() && listCodeAlphaPsl != null && !listCodeAlphaPsl.isEmpty()) {
            listPslByTransportRefAndUnitRefAndCodeAlpha = daoPslApp
                .getListPslAppByRefTransportAndRefUnitAndCodeAlpha(listRef, listCodeAlphaPsl, false, null);

            if (listPslByTransportRefAndUnitRefAndCodeAlpha != null
                && !listPslByTransportRefAndUnitRefAndCodeAlpha.isEmpty()) {
                listInfosEventFinal.addAll(listPslByTransportRefAndUnitRefAndCodeAlpha);
            }

        }

        for (int i = 0; i < listTmpInfosEvent.size(); i++) {
            // a partir de l'objet PSL, on construira l'oject EbTtEvent
            // (pslApp,ebTracing)
            TmpInfosPsl tmpInfosPsl = listTmpInfosEvent.get(i);

            if (listTmpInfosEvent.get(i).getNatureReference().equals(NatureReference.UNIT_REF.getCode())) {
                EbTTPslApp ebTtPslApp = listInfosEventFinal
                    .stream()
                    .filter(
                        elem -> elem.getxEbTrackTrace().getRefTransport().equals(tmpInfosPsl.getRefTransport())
                            && elem.getxEbTrackTrace().getCustomerReference().equals(tmpInfosPsl.getUnitReference())
                            && elem.getCodeAlpha().equals(tmpInfosPsl.getCodeAlpha()))
                    .findFirst().orElse(null);

                if (ebTtPslApp != null) {
                    listEbTtEventByCompagnie.get(i).setxEbTtPslApp(ebTtPslApp);
                    listEbTtEventByCompagnie.get(i).setxEbTtTracing(ebTtPslApp.getxEbTrackTrace());

                    listEbTtEventFinal.add(new EbTtEvent(listEbTtEventByCompagnie.get(i)));
                }
                else {
                    listRejectTmpEventNum.add(listTmpInfosEvent.get(i).getTmpEventNum());
                }

            }
            else if (listTmpInfosEvent.get(i).getNatureReference().equals(NatureReference.TRANSPORT_REF.getCode())) {
                List<EbTTPslApp> listEbTtPslApp = listInfosEventFinal
                    .stream()
                    .filter(
                        elem -> elem.getxEbTrackTrace().getRefTransport().equals(tmpInfosPsl.getRefTransport())
                            && elem.getCodeAlpha().equals(tmpInfosPsl.getCodeAlpha()))
                    .collect(Collectors.toList());

                if (listEbTtPslApp != null && !listEbTtPslApp.isEmpty()) {

                    for (EbTTPslApp psl: listEbTtPslApp) {
                        listEbTtEventByCompagnie.get(i).setxEbTtPslApp(psl);
                        listEbTtEventByCompagnie.get(i).setxEbTtTracing(psl.getxEbTrackTrace());

                        listEbTtEventFinal.add(new EbTtEvent(listEbTtEventByCompagnie.get(i)));
                    }

                }
                else {
                    listRejectTmpEventNum.add(listTmpInfosEvent.get(i).getTmpEventNum());
                }

            }
            else // NatureReference.CUSTOMER_REF
            {
                List<EbTTPslApp> listEbTtPslApp = listInfosEventFinal
                    .stream()
                    .filter(
                        elem -> elem.getxEbTrackTrace().getCustomRef().equals(tmpInfosPsl.getCustomerReference())
                            && elem.getCodeAlpha().equals(tmpInfosPsl.getCodeAlpha()))
                    .collect(Collectors.toList());

                if (listEbTtPslApp != null && !listEbTtPslApp.isEmpty()) {

                    for (EbTTPslApp psl: listEbTtPslApp) {
                        listEbTtEventByCompagnie.get(i).setxEbTtPslApp(psl);
                        listEbTtEventByCompagnie.get(i).setxEbTtTracing(psl.getxEbTrackTrace());

                        listEbTtEventFinal.add(new EbTtEvent(listEbTtEventByCompagnie.get(i)));
                    }

                }
                else {
                    listRejectTmpEventNum.add(listTmpInfosEvent.get(i).getTmpEventNum());
                }

            }

        }

        // ajout des deviations
        if (!listEbTtEventFinal.isEmpty()) trackService.addDeviationMasse(listEbTtEventFinal, ediUser);

        return listRejectTmpEventNum;
    }

    @Override
    public EbTtEvent convertTmpEventToTDeviation(TmpEvent tmpEvent) {
        EbTtEvent event = new EbTtEvent();

        event.setTypeEvent(tmpEvent.getTypeEvent().getCode());

        event.setCategorie(tmpEvent.getCategorieDeviation());

        Date dateEvent = tmpEvent.getDateActual();

        if (dateEvent == null) {
            dateEvent = tmpEvent.getDateEstimated() != null ?
                tmpEvent.getDateEstimated() :
                tmpEvent.getDateNegociated();
        }

        event.setDateEvent(dateEvent);
        event.setCommentaire(tmpEvent.getComment());
        event.setxEbTmptEvent(tmpEvent);
        return event;
    }

    @Override
    public List<EbTTPslApp> convertTmpEventToTtPslApp(TmpEvent tmpEvent) {
        List<EbTTPslApp> listPslApp = new ArrayList<EbTTPslApp>();
        EbTTPslApp psl = null;
        EcCountry ecCountryByCode = null;

        if (tmpEvent.getCodeCountry() != null) {
            ecCountryByCode = listStatiqueService.getEcCountryByCode(tmpEvent.getCodeCountry());
        }

        if (tmpEvent.getDateActual() != null) {
            psl = new EbTTPslApp();
            psl.setCity(tmpEvent.getCity());

            psl.setxEcCountry(ecCountryByCode);
            psl.setCompanyPsl(new EbTtCompanyPsl());
            psl.getCompanyPsl().setCodeAlpha(tmpEvent.getCodePslNormalise());

            psl.setDateActuelle(tmpEvent.getDateActual());

            psl.setCommentaire(tmpEvent.getComment());

            listPslApp.add(psl);
        }

        if (tmpEvent.getDateEstimated() != null) {
            psl = new EbTTPslApp();
            psl.setCity(tmpEvent.getCity());

            psl.setxEcCountry(ecCountryByCode);
            psl.setCompanyPsl(new EbTtCompanyPsl());
            psl.getCompanyPsl().setCodeAlpha(tmpEvent.getCodePslNormalise());

            psl.setDateEstimee(tmpEvent.getDateEstimated());

            psl.setCommentaire(tmpEvent.getComment());

            listPslApp.add(psl);
        }

        if (tmpEvent.getDateNegociated() != null) {
            psl = new EbTTPslApp();
            psl.setCity(tmpEvent.getCity());

            psl.setxEcCountry(ecCountryByCode);
            psl.setCompanyPsl(new EbTtCompanyPsl());
            psl.getCompanyPsl().setCodeAlpha(tmpEvent.getCodePslNormalise());

            psl.setDateNegotiation(tmpEvent.getDateNegociated());

            psl.setCommentaire(tmpEvent.getComment());
            listPslApp.add(psl);
        }

        return listPslApp;
    }

    @Override
    public CronjobMonitoring getInfosLastExecutionCronjob() {
        nextExecution = taskSchedulerService.nextExecutionByCronExpression(lastExecution);

        return new CronjobMonitoring(
            lastExecution,
            nextExecution,
            !taskSchedulerService.taskSchedulerIsShutdown(),
            jobExecutionFailed);
    }

    @Override
    public CronjobMonitoring changeStatutCronjob(Boolean status) {

        if (status) {
            taskSchedulerService.activateTaskScheduler();
        }
        else {
            taskSchedulerService.desactivateTaskScheduler();
        }

        return getInfosLastExecutionCronjob();
    }
}
