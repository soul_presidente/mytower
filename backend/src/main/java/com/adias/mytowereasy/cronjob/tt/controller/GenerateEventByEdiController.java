package com.adias.mytowereasy.cronjob.tt.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.cronjob.tt.service.GenerateEventByEdiService;
import com.adias.mytowereasy.cronjob.tt.util.CronjobMonitoring;


@RestController
@RequestMapping(value = "api/cronjob")
@CrossOrigin("*")
public class GenerateEventByEdiController {
    @Autowired
    GenerateEventByEdiService generateEventByEdiService;

    @RequestMapping(value = "/tracing", method = RequestMethod.GET)
    public CronjobMonitoring apiGenerateEventByEdi() {
        generateEventByEdiService.generateTtEventByTmpEvent();
        return getInfosLastExecutionCronjob();
    }

    @RequestMapping(value = "/tracing-infos-last-execution", method = RequestMethod.GET)
    public CronjobMonitoring getInfosLastExecutionCronjob() {
        return generateEventByEdiService.getInfosLastExecutionCronjob();
    }

    @RequestMapping(value = "/tracing-change-statut-cronjob", method = RequestMethod.GET)
    public CronjobMonitoring changeStatutCronjob(@RequestParam(value = "status") Boolean status) {
        return generateEventByEdiService.changeStatutCronjob(status);
    }
}
