package com.adias.mytowereasy.cronjob.tt.util;

import java.util.Date;


public class CronjobMonitoring {
    private Date lastExecutionDate;

    private Date nextExecutionDate;

    private Boolean status;

    private Boolean jobExecutionFailed;

    public CronjobMonitoring() {
        super();
    }

    public CronjobMonitoring(
        Date lastExecutionDate,
        Date nextExecutionDate,
        Boolean status,
        Boolean jobExecutionFailed) {
        super();
        this.lastExecutionDate = lastExecutionDate;
        this.nextExecutionDate = nextExecutionDate;
        this.status = status;
        this.jobExecutionFailed = jobExecutionFailed;
    }

    public Date getLastExecutionDate() {
        return lastExecutionDate;
    }

    public void setLastExecutionDate(Date lastExecutionDate) {
        this.lastExecutionDate = lastExecutionDate;
    }

    public Date getNextExecutionDate() {
        return nextExecutionDate;
    }

    public void setNextExecutionDate(Date nextExecutionDate) {
        this.nextExecutionDate = nextExecutionDate;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Boolean getJobExecutionFailed() {
        return jobExecutionFailed;
    }

    public void setJobExecutionFailed(Boolean jobExecutionFailed) {
        this.jobExecutionFailed = jobExecutionFailed;
    }
}
