package com.adias.mytowereasy.cronjob.tt.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbTimeStamps;
import com.adias.mytowereasy.model.EcCountry;
import com.adias.mytowereasy.model.TtEnumeration;
import com.adias.mytowereasy.model.tt.EbTtCategorieDeviation;


@Table(name = "tmp_event")
@Entity
public class TmpEvent extends EbTimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer tmpEventNum;

    private String codePslTransporteur;

    private String codePslNormalise;

    private Date dateActual;

    private Date dateNegociated;

    private Date dateEstimated;

    @Enumerated(EnumType.ORDINAL)
    private TtEnumeration.TypeEvent typeEvent; // type d'evenement(DEV,PSL)

    private String codeTransporteur;

    private String refTransport;

    private String unitReference;

    private String customerReference;

    private String city;

    private String CodeCountry;

    private String creatorEmail;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country", insertable = true)
    private EcCountry xEcCountry;

    private Date dateTraitement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company_transporteur", insertable = true)
    private EbCompagnie xEbCompagnieTransporteur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie")
    private EbCompagnie xEbCompagnie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_edi_map_psl")
    private EdiMapPsl xEdiMapPsl;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_tmp_event_file")
    private TmpEventFile xTmpEventFile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company_psl", insertable = true)
    private EbTtCompanyPsl xEbCompanyPsl;

    private Boolean isTreated;

    private Integer status;

    private String nomFichier;

    private String comment;

    private String libelleCategorieDeviation;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_categorie_deviation")
    private EbTtCategorieDeviation categorieDeviation;

    public TmpEvent() {
    }

    public TmpEvent(Integer tmpEventNum) {
        this.tmpEventNum = tmpEventNum;
    }

    public Integer getTmpEventNum() {
        return tmpEventNum;
    }

    public void setTmpEventNum(Integer tmpEventNum) {
        this.tmpEventNum = tmpEventNum;
    }

    public Date getDateActual() {
        return dateActual;
    }

    public void setDateActual(Date dateActual) {
        this.dateActual = dateActual;
    }

    public Date getDateNegociated() {
        return dateNegociated;
    }

    public void setDateNegociated(Date dateNegociated) {
        this.dateNegociated = dateNegociated;
    }

    public Date getDateEstimated() {
        return dateEstimated;
    }

    public void setDateEstimated(Date dateEstimated) {
        this.dateEstimated = dateEstimated;
    }

    public TtEnumeration.TypeEvent getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(TtEnumeration.TypeEvent typeEvent) {
        this.typeEvent = typeEvent;
    }

    public String getRefTransport() {
        return refTransport;
    }

    public void setRefTransport(String refTransport) {
        this.refTransport = refTransport;
    }

    public String getUnitReference() {
        return unitReference;
    }

    public void setUnitReference(String unitReference) {
        this.unitReference = unitReference;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCodeCountry() {
        return CodeCountry;
    }

    public void setCodeCountry(String codeCountry) {
        CodeCountry = codeCountry;
    }

    public EcCountry getxEcCountry() {
        return xEcCountry;
    }

    public void setxEcCountry(EcCountry xEcCountry) {
        this.xEcCountry = xEcCountry;
    }

    public Date getDateTraitement() {
        return dateTraitement;
    }

    public void setDateTraitement(Date dateTraitement) {
        this.dateTraitement = dateTraitement;
    }

    public EbCompagnie getxEbCompagnieTransporteur() {
        return xEbCompagnieTransporteur;
    }

    public void setxEbCompagnieTransporteur(EbCompagnie xEbCompagnieTransporteur) {
        this.xEbCompagnieTransporteur = xEbCompagnieTransporteur;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public EdiMapPsl getxEdiMapPsl() {
        return xEdiMapPsl;
    }

    public void setxEdiMapPsl(EdiMapPsl xEdiMapPsl) {
        this.xEdiMapPsl = xEdiMapPsl;
    }

    public TmpEventFile getxTmpEventFile() {
        return xTmpEventFile;
    }

    public void setxTmpEventFile(TmpEventFile xTmpEventFile) {
        this.xTmpEventFile = xTmpEventFile;
    }

    public EbTtCompanyPsl getxEbCompanyPsl() {
        return xEbCompanyPsl;
    }

    public void setxEbCompanyPsl(EbTtCompanyPsl xEbCompanyPsl) {
        this.xEbCompanyPsl = xEbCompanyPsl;
    }

    public Boolean getIsTreated() {
        return isTreated;
    }

    public void setIsTreated(Boolean isTreated) {
        this.isTreated = isTreated;
    }

    public String getCodePslTransporteur() {
        return codePslTransporteur;
    }

    public void setCodePslTransporteur(String codePslTransporteur) {
        this.codePslTransporteur = codePslTransporteur;
    }

    public String getCodePslNormalise() {
        return codePslNormalise;
    }

    public void setCodePslNormalise(String codePslNormalise) {
        this.codePslNormalise = codePslNormalise;
    }

    public String getCodeTransporteur() {
        return codeTransporteur;
    }

    public void setCodeTransporteur(String codeTransporteur) {
        this.codeTransporteur = codeTransporteur;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getNomFichier() {
        return nomFichier;
    }

    public void setNomFichier(String nomFichier) {
        this.nomFichier = nomFichier;
    }

    public String getCreatorEmail() {
        return creatorEmail;
    }

    public void setCreatorEmail(String creatorEmail) {
        this.creatorEmail = creatorEmail;
    }

    public String getLibelleCategorieDeviation() {
        return libelleCategorieDeviation;
    }

    public void setLibelleCategorieDeviation(String libelleCategorieDeviation) {
        this.libelleCategorieDeviation = libelleCategorieDeviation;
    }

    public EbTtCategorieDeviation getCategorieDeviation() {
        return categorieDeviation;
    }

    public void setCategorieDeviation(EbTtCategorieDeviation categorieDeviation) {
        this.categorieDeviation = categorieDeviation;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
