package com.adias.mytowereasy.cronjob.tt.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import com.adias.mytowereasy.cronjob.tt.util.CronjobStatique;
import com.adias.mytowereasy.util.email.Email;
import com.adias.mytowereasy.util.email.EmailHtmlSender;


@Service
public class EmailGenerationEventServiceImpl implements EmailGenerationEventService {
    @Value("${tracing.generate_event_by_edi.email}")
    private String executionFailureEmail;

    private EmailHtmlSender emailHtmlSender;

    @Autowired
    public EmailGenerationEventServiceImpl(EmailHtmlSender emailHtmlSender) {
        this.emailHtmlSender = emailHtmlSender;
    }

    /*
     * data[0] c'est la date d'execution
     * data[1] c'est la date de la prochaine execution
     * data[2] c'est la liste des TmpEvent Rejeté
     */
    @Override
    public void sendEmailEchecExecutionJob(Object... data) {
        if (executionFailureEmail == null) return;

        Context context = new Context();

        context.setVariable("dateExecution", CronjobStatique.convertDateToString((Date) data[0]));
        context.setVariable("dateNextExecution", CronjobStatique.convertDateToString((Date) data[1]));
        context.setVariable("listTmpEventRejected", (List) data[2]);

        String subject = "job_execution_failed.tracing.subject";

        Email email = new Email();

        email.setSubject(subject);

        email.setTo(executionFailureEmail);

        emailHtmlSender.send(email, "exception/10001-email-alert-job-execution-failure", context);
    }
}
