package com.adias.mytowereasy.cronjob.tt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.cronjob.tt.model.EdiMapPsl;


@Repository
public interface EdiMapPslRepository extends JpaRepository<EdiMapPsl, Integer> {
}
