package com.adias.mytowereasy.cronjob.tt.service;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronSequenceGenerator;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Component;


@Component
public class TaskSchedulerServiceImpl implements TaskSchedulerService {
    @Value("${tracing.generate_event_by_edi.cron}")
    private String tracingCron;

    @Value("${tracing.generate_event_by_edi.active}")
    private Boolean tracingCronActive;

    @Qualifier("ThreadPoolTaskScheduler")
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;

    @Autowired
    CronTrigger cronTrigger;

    @Autowired
    GenerateEventByEdiService generateEventByEdiService;

    @Override
    public void activateTaskScheduler() {
        taskScheduler.initialize();
    }

    @Override
    public void desactivateTaskScheduler() {
        taskScheduler.shutdown();
    }

    @Override
    public Boolean taskSchedulerIsShutdown() {
        return taskScheduler.getScheduledExecutor().isShutdown();
    }

    @PostConstruct
    @Override
    public void scheduleRunnableWithCronTrigger() {

        if (tracingCronActive) {
            taskScheduler.schedule(new Runnable() {
                @Override
                public void run() {
                    generateEventByEdiService.generateTtEventByTmpEvent();
                }
            }, cronTrigger);
        }

    }

    @Override
    public Date nextExecutionByCronExpression(Date lastExecution) {
        CronSequenceGenerator cronTrigger = new CronSequenceGenerator(tracingCron);
        return cronTrigger.next(lastExecution != null ? lastExecution : new Date());
    }
}
