package com.adias.mytowereasy.cronjob.tt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;

import com.adias.mytowereasy.cronjob.tt.repository.TmpEventRepository;
import com.adias.mytowereasy.dao.DaoPslApp;
import com.adias.mytowereasy.dao.DaoTrack;
import com.adias.mytowereasy.repository.EbPslAppRepository;
import com.adias.mytowereasy.repository.EbTtCategorieDeviationRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.service.ListStatiqueService;
import com.adias.mytowereasy.service.TrackService;


public class CronjobService {
    @Autowired
    TmpEventRepository tmpEventRepository;

    @Autowired
    TrackService trackService;

    @Autowired
    EbPslAppRepository ebPslAppRepository;

    @Autowired
    ListStatiqueService listStatiqueService;

    @Autowired
    EbUserRepository ebUserRepository;

    @Qualifier("ThreadPoolTaskScheduler")
    @Autowired
    ThreadPoolTaskScheduler taskScheduler;

    @Autowired
    CronTrigger cronTrigger;

    @Autowired
    EmailGenerationEventService emailGenerationEventService;

    @Autowired
    EbTtCategorieDeviationRepository ebTtCategorieDeviationRepository;

    @Autowired
    DaoTrack daotrack;

    @Autowired
    DaoPslApp daoPslApp;
}
