package com.adias.mytowereasy.cronjob.tt.util;

import java.util.List;

import com.adias.mytowereasy.model.tt.EbTTPslApp;


public class TmpInfosPsl {
    private Integer tmpEventNum;

    private String refTransport;

    private String unitReference;

    private String customerReference;

    private String codeAlpha;

    private List<EbTTPslApp> listPslByTrRefOrCustomerRefAndCodeAlpha;

    private String creatorEmail;

    private Integer natureReference;

    public TmpInfosPsl() {
    }

    public TmpInfosPsl(
        Integer tmpEventNum,
        String unitReference,
        String refTransport,
        String customerReference,
        Integer natureReference,
        String codeAlpha,
        String creatorEmail) {
        this.refTransport = refTransport;
        this.unitReference = unitReference;
        this.customerReference = customerReference;
        this.natureReference = natureReference;
        this.codeAlpha = codeAlpha;
        this.tmpEventNum = tmpEventNum;
        this.creatorEmail = creatorEmail;
    }

    public String getRefTransport() {
        return refTransport;
    }

    public void setRefTransport(String refTransport) {
        this.refTransport = refTransport;
    }

    public String getUnitReference() {
        return unitReference;
    }

    public void setUnitReference(String unitReference) {
        this.unitReference = unitReference;
    }

    public List<EbTTPslApp> getListPslByTrRefOrCustomerRefAndCodeAlpha() {
        return listPslByTrRefOrCustomerRefAndCodeAlpha;
    }

    public void setListPslByTrRefOrCustomerRefAndCodeAlpha(List<EbTTPslApp> listPslByTrRefOrCustomerRefAndCodeAlpha) {
        this.listPslByTrRefOrCustomerRefAndCodeAlpha = listPslByTrRefOrCustomerRefAndCodeAlpha;
    }

    public String getCodeAlpha() {
        return codeAlpha;
    }

    public void setCodeAlpha(String codeAlpha) {
        this.codeAlpha = codeAlpha;
    }

    public Integer getTmpEventNum() {
        return tmpEventNum;
    }

    public void setTmpEventNum(Integer tmpEventNum) {
        this.tmpEventNum = tmpEventNum;
    }

    public String getCreatorEmail() {
        return creatorEmail;
    }

    public void setCreatorEmail(String creatorEmail) {
        this.creatorEmail = creatorEmail;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public Integer getNatureReference() {
        return natureReference;
    }

    public void setNatureReference(Integer natureReference) {
        this.natureReference = natureReference;
    }
}
