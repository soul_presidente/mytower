package com.adias.mytowereasy.cronjob.tt.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.adias.mytowereasy.model.EbTimeStamps;


@Table(name = "tmp_event_file")
@Entity
public class TmpEventFile extends EbTimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer tmpEventFileNum;

    private String fileName;

    private Date dateIntegration;

    public TmpEventFile() {
        super();
    }

    public Integer getTmpEventFileNum() {
        return tmpEventFileNum;
    }

    public void setTmpEventFileNum(Integer tmpEventFileNum) {
        this.tmpEventFileNum = tmpEventFileNum;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Date getDateIntegration() {
        return dateIntegration;
    }

    public void setDateIntegration(Date dateIntegration) {
        this.dateIntegration = dateIntegration;
    }
}
