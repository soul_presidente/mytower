package com.adias.mytowereasy.cronjob.tt.util;

import java.text.SimpleDateFormat;
import java.util.Date;


public class CronjobStatique {
    public static final Integer MAX_NUMBER_OF_EVENTS_TO_TREAT = 1000;

    public static String convertDateToString(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm").format(date);
    }
}
