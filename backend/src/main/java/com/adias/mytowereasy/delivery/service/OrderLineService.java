package com.adias.mytowereasy.delivery.service;

import java.util.List;

import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.dto.EbOrderLineDto;


public interface OrderLineService {
    public List<EbOrderLine> getOrderLines(Long ebOrderLineNum);

    public List<EbOrderLineDto> getOrderLinesPlanifiable(Long ebDelOrderNum);

    public List<EbOrderLine> getOrderLinesForExport(SearchCriteriaOrder criteria);

    public List<EbOrderLine> getOrderLinesPlanifiableTableau(SearchCriteriaOrder criteria);

    public Long getOrderLineCountTableau(SearchCriteriaOrder criteria);

    public List<EbOrderLine> findByTerm(Long ebDelOrderNum, String searchterm);
}
