package com.adias.mytowereasy.delivery.model;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.TypeDef;

import com.adias.mytowereasy.types.JsonBinaryType;


@Table(name = "eb_del_fichiers_joint")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class EbDelFichierJoint implements java.io.Serializable {
    private static final long serialVersionUID = 5837762455423795444L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebDelFichierJointNum;

    private Integer idCategorie;

    private String chemin;

    private String fileName;

    private String originFileName;

    private String pathToBl;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_del_livraison")
    private EbLivraison xEbDelLivraison;

    @Temporal(TemporalType.TIMESTAMP)
    private Date attachmentDate;

    public Integer getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(Integer idCategorie) {
        this.idCategorie = idCategorie;
    }

    public String getChemin() {
        return chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = chemin;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOriginFileName() {
        return originFileName;
    }

    public void setOriginFileName(String originFileName) {
        this.originFileName = originFileName;
    }

    public Date getAttachmentDate() {
        return attachmentDate;
    }

    public void setAttachmentDate(Date attachmentDate) {
        this.attachmentDate = attachmentDate;
    }

    public String getPathToBl() {
        return pathToBl;
    }

    public void setPathToBl(String pathToBl) {
        this.pathToBl = pathToBl;
    }

    public EbLivraison getxEbDelLivraison() {
        return xEbDelLivraison;
    }

    public void setxEbDelLivraison(EbLivraison xEbDelLivraison) {
        this.xEbDelLivraison = xEbDelLivraison;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((attachmentDate == null) ? 0 : attachmentDate.hashCode());
        result = prime * result + ((chemin == null) ? 0 : chemin.hashCode());
        result = prime * result + ((ebDelFichierJointNum == null) ? 0 : ebDelFichierJointNum.hashCode());
        result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
        result = prime * result + ((idCategorie == null) ? 0 : idCategorie.hashCode());
        result = prime * result + ((originFileName == null) ? 0 : originFileName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbDelFichierJoint other = (EbDelFichierJoint) obj;

        if (attachmentDate == null) {
            if (other.attachmentDate != null) return false;
        }
        else if (!attachmentDate.equals(other.attachmentDate)) return false;

        if (chemin == null) {
            if (other.chemin != null) return false;
        }
        else if (!chemin.equals(other.chemin)) return false;

        if (ebDelFichierJointNum == null) {
            if (other.ebDelFichierJointNum != null) return false;
        }
        else if (!ebDelFichierJointNum.equals(other.ebDelFichierJointNum)) return false;

        if (fileName == null) {
            if (other.fileName != null) return false;
        }
        else if (!fileName.equals(other.fileName)) return false;

        if (idCategorie == null) {
            if (other.idCategorie != null) return false;
        }
        else if (!idCategorie.equals(other.idCategorie)) return false;

        if (originFileName == null) {
            if (other.originFileName != null) return false;
        }
        else if (!originFileName.equals(other.originFileName)) return false;

        return true;
    }
}
