package com.adias.mytowereasy.delivery.service;

import java.util.List;

import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;


public interface DeliveryLineService {
    public List<EbLivraisonLine> getDeliveryLines(Long ebDelLivraisonNum);

    public List<EbLivraisonLine> getDeliveryLinesForExport(SearchCriteriaDelivery criteria);

    public EbLivraisonLine inlineDeliveryDashboardUpdate(EbLivraisonLine sent);

    public List<EbLivraisonLine> getDeliveryLinesForGenericTable(SearchCriteriaDelivery criteriaDelivery);

    public Long getDeliveryLineCountForGenericTable(SearchCriteriaDelivery criteriaDelivery);
}
