package com.adias.mytowereasy.delivery.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.model.EbUser;


@Repository
public interface EbOrderRepository extends JpaRepository<EbOrder, Long> {
    @Query("select max(o.ebDelOrderNum) from EbOrder o ")
    public Long getMaxOrder();

    @Query("select max(o.ebDelOrderLineNum) from EbOrderLine o ")
    public Long getMaxOrderLine();

    @Transactional
    @Modifying
    @Query(
        value = "update work.eb_del_order set x_eb_etablissement= ?2 "
            + "where regexp_split_to_array(?3, ',') && (regexp_split_to_array(list_labels,',')) "
            + "and x_eb_etablissement = ?1",

        nativeQuery = true)
    public void
        updateOrderEtablissementAndLabels(Integer oldEtablissementNum, Integer newEtablissementNum, String listLabels);

    public List<EbOrder> findByxEbOwnerOfTheRequest(Integer ebUserNum);

    public List<EbParty> findByxEbPartyDestination(Integer ebPartyNum);

    public List<EbOrder> findAllByxEbPartyOrigin(Integer ebPartyOriginNum);

    @Query("Select o from EbOrder o where o.customerOrderReference=:customerOrderReference")
    EbOrder findByCustomerOrderReference(@Param("customerOrderReference") String customerOrderReference);

    @Query("SELECT DISTINCT u FROM EbUser u LEFT JOIN EbOrder o ON u.ebUserNum = o.xEbOwnerOfTheRequest WHERE o.xEbCompagnie.ebCompagnieNum = :compagnieNum")
    List<EbUser> getListOwnerRequest(@Param("compagnieNum") Integer compagnieNum);
}
