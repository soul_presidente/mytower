package com.adias.mytowereasy.delivery.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.delivery.dao.DaoOrderLine;
import com.adias.mytowereasy.delivery.dao.DaoVisibility;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.model.QEbOrder;
import com.adias.mytowereasy.delivery.model.QEbOrderLine;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbParty;
import com.adias.mytowereasy.model.QEcCountry;
import com.adias.mytowereasy.model.QEcCurrency;


@Service
@Transactional
public class DaoOrderLineImpl implements DaoOrderLine {
    Logger logger = LoggerFactory.getLogger(DaoOrderImpl.class);

    @PersistenceContext
    EntityManager em;

    @Autowired
    DaoVisibility daoVisibility;

    QEbOrder qEbOrder = new QEbOrder("ebOrder");
    QEbOrderLine qEbOrderLine = new QEbOrderLine("ebOrderLine");

    QEbParty qPartyDest = new QEbParty("partyDest");
    QEbParty qPartyOrigin = new QEbParty("partyOrigin");
    QEbParty qPartySale = new QEbParty("partySale");

    QEcCountry qCountryOrigin = new QEcCountry("qCountryOrigin");
    QEcCountry qCountryDest = new QEcCountry("qCountryDest");

    QEcCurrency qCurrency = new QEcCurrency("qCurrency");

    @Override
    public List<EbOrderLine> getOrderLines(Long ebOrderLineNum) {
        List<EbOrderLine> result = null;

        try {
            JPAQuery<EbOrderLine> query = new JPAQuery<EbOrderLine>(em);

            if (ebOrderLineNum == null) {
                return new ArrayList<EbOrderLine>();
            }

            query
                .select(
                    QEbOrderLine
                        .create(
                            qEbOrderLine.ebDelOrderLineNum,
                            qEbOrderLine.idOrderLine,
                            qEbOrderLine.refArticle,
                            qEbOrderLine.articleName,
                            qEbOrderLine.dateOfAvailability,
                            qEbOrderLine.canceledReason,

                            qCountryOrigin.libelle,

                            qPartyDest.reference,
                            qPartyDest.company,
                            qCountryDest.libelle,

                            qEbOrder.numOrderSAP,
                            qEbOrder.orderCustomerName,
                            qEbOrder.campaignCode,
                            qEbOrder.campaignName,
                            qEbOrder.numOrderCustomer,
                            qEbOrder.orderStatus,
                            qEbOrder.customerCreationDate,
                            qEbOrder.deliveryDate,

                            qEbOrderLine.partNumber,
                            qEbOrderLine.serialNumber,
                            qEbOrderLine.dateRequest,
                            qEbOrderLine.priority,
                            qEbOrderLine.comment,
                            qEbOrderLine.specification,
                            qEbOrderLine.listCategories,
                            qEbOrderLine.customFields,
                            qEbOrderLine.weight,
                            qEbOrderLine.length,
                            qEbOrderLine.width,
                            qEbOrderLine.height,
                            qEbOrderLine.hsCode,
                            qEbOrderLine.volume,
                            qEbOrderLine.itemName,
                            qEbOrderLine.itemNumber,
                            qEbOrderLine.ebTypeUnitNum,
                            qEbOrderLine.quantityOrdered,
                            qEbOrderLine.quantityPlannable,
                            qEbOrderLine.quantityPlanned,
                            qEbOrderLine.quantityPending,
                            qEbOrderLine.quantityConfirmed,
                            qEbOrderLine.quantityShipped,
                            qEbOrderLine.quantityDelivered,
                            qEbOrderLine.quantityCanceled,
                            qEbOrderLine.serialized,
                            qEbOrderLine.price,
                            qEbOrderLine.eccn,
                            qCurrency.ecCurrencyNum, qCurrency.libelle));
            query.from(qEbOrderLine).where(qEbOrderLine.xEbDelOrder().ebDelOrderNum.eq(ebOrderLineNum));
            query.leftJoin(qEbOrderLine.xEcCountryOrigin(), qCountryOrigin);
            query.leftJoin(qEbOrderLine.xEbDelOrder(), qEbOrder);
            query.leftJoin(qEbOrder.xEbPartyDestination(), qPartyDest);
            query.leftJoin(qPartyDest.xEcCountry(), qCountryDest);
            query.leftJoin(qEbOrderLine.currency(), qCurrency);
            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EbOrderLine> getOrderLinesForExport(SearchCriteriaOrder criteria, EbUser currentUser) {

        List<EbOrderLine> result = null;

        try {
            JPAQuery<EbOrderLine> query = new JPAQuery<EbOrderLine>(em);

            query
                .select(
                    QEbOrderLine
                        .create(
                            qEbOrderLine.ebDelOrderLineNum,
                            qEbOrderLine.idOrderLine,
                            qEbOrderLine.refArticle,
                            qEbOrderLine.articleName,
                            qEbOrderLine.dateOfAvailability,
                            qEbOrderLine.canceledReason,

                            qCountryOrigin.libelle,

                            qPartyDest.reference,
                            qPartyDest.company,
                            qCountryDest.libelle,

                            qEbOrder.numOrderSAP,
                            qEbOrder.orderCustomerName,
                            qEbOrder.campaignCode,
                            qEbOrder.campaignName,
                            qEbOrder.numOrderCustomer,
                            qEbOrder.orderStatus,
                            qEbOrder.customerCreationDate,
                            qEbOrder.deliveryDate,

                            qEbOrderLine.partNumber,
                            qEbOrderLine.serialNumber,
                            qEbOrderLine.dateRequest,
                            qEbOrderLine.priority,
                            qEbOrderLine.comment,
                            qEbOrderLine.specification,
                            qEbOrderLine.listCategories,
                            qEbOrderLine.customFields,
                            qEbOrderLine.weight,
                            qEbOrderLine.length,
                            qEbOrderLine.width,
                            qEbOrderLine.height,
                            qEbOrderLine.hsCode,
                            qEbOrderLine.volume,
                            qEbOrderLine.itemName,
                            qEbOrderLine.itemNumber,
                            qEbOrderLine.ebTypeUnitNum,
                            qEbOrderLine.quantityOrdered,
                            qEbOrderLine.quantityPlannable,
                            qEbOrderLine.quantityPlanned,
                            qEbOrderLine.quantityPending,
                            qEbOrderLine.quantityConfirmed,
                            qEbOrderLine.quantityShipped,
                            qEbOrderLine.quantityDelivered,
                            qEbOrderLine.quantityCanceled,
                            qEbOrderLine.serialized,
                                qEbOrderLine.price,
                                qEbOrderLine.eccn,
                                qCurrency.ecCurrencyNum,
                                qCurrency.libelle));

            query.from(qEbOrderLine);
            query.leftJoin(qEbOrderLine.xEcCountryOrigin(), qCountryOrigin);
            query.leftJoin(qEbOrderLine.xEbDelOrder(), qEbOrder);
            query.leftJoin(qEbOrder.xEbPartyDestination(), qPartyDest);
            query.leftJoin(qPartyDest.xEcCountry(), qCountryDest);
            query.leftJoin(qEbOrderLine.currency(), qCurrency);
            query.limit(criteria.getSize());
            query = criteria.applyCriteria(query, true);
            query = daoVisibility.addVisibilityToOrder(query, criteria, currentUser);

            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<EbOrderLine> getOrderLinesPlanifiableTableau(SearchCriteriaOrder criteria, EbUser currentUser) {
        List<EbOrderLine> resultat = new ArrayList<>();

        try {
            JPAQuery<EbOrderLine> query = new JPAQuery<>(em);
            query
                .select(
                    QEbOrderLine
                        .create(
                            qEbOrderLine.ebDelOrderLineNum,
                            qEbOrderLine.idOrderLine,
                            qEbOrderLine.refArticle,
                            qEbOrderLine.articleName,
                            qEbOrderLine.dateOfAvailability,
                            qEbOrderLine.canceledReason,

                            qCountryOrigin.libelle,

                            qPartyDest.reference,
                            qPartyDest.company,
                            qCountryDest.libelle,

                            qEbOrder.numOrderSAP,
                            qEbOrder.orderCustomerName,
                            qEbOrder.campaignCode,
                            qEbOrder.campaignName,
                            qEbOrder.numOrderCustomer,
                            qEbOrder.orderStatus,
                            qEbOrder.customerCreationDate,
                            qEbOrder.deliveryDate,

                            qEbOrderLine.partNumber,
                            qEbOrderLine.serialNumber,
                            qEbOrderLine.dateRequest,
                            qEbOrderLine.priority,
                            qEbOrderLine.comment,
                            qEbOrderLine.specification,
                            qEbOrderLine.listCategories,
                            qEbOrderLine.customFields,
                            qEbOrderLine.weight,
                            qEbOrderLine.length,
                            qEbOrderLine.width,
                            qEbOrderLine.height,
                            qEbOrderLine.hsCode,
                            qEbOrderLine.volume,
                            qEbOrderLine.itemName,
                            qEbOrderLine.itemNumber,
                            qEbOrderLine.ebTypeUnitNum,
                            qEbOrderLine.quantityOrdered,
                            qEbOrderLine.quantityPlannable,
                            qEbOrderLine.quantityPlanned,
                            qEbOrderLine.quantityPending,
                            qEbOrderLine.quantityConfirmed,
                            qEbOrderLine.quantityShipped,
                            qEbOrderLine.quantityDelivered,
                            qEbOrderLine.quantityCanceled,
                            qEbOrderLine.serialized, qEbOrderLine.price,
                                qEbOrderLine.eccn,
                                qCurrency.ecCurrencyNum,
                                qCurrency.libelle));
            query.from(qEbOrderLine);
            query.leftJoin(qEbOrderLine.xEcCountryOrigin(), qCountryOrigin);
            query.leftJoin(qEbOrderLine.xEbDelOrder(), qEbOrder);
            query.leftJoin(qEbOrder.xEbPartyDestination(), qPartyDest);
            query.leftJoin(qPartyDest.xEcCountry(), qCountryDest);
            query.leftJoin(qEbOrderLine.currency(), qCurrency);

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            String searchTerm = criteria.getSearchterm();

            if (searchTerm != null && !(searchTerm = criteria.getSearchterm().toLowerCase().trim()).isEmpty()) {
                query.where(qEbOrderLine.itemName.containsIgnoreCase(searchTerm));
            }

            // uniquement les lignes planifiables sont affichés
            query.where(qEbOrderLine.quantityPlannable.gt(0l));
            query.where(qEbOrderLine.xEbDelOrder().ebDelOrderNum.eq(criteria.getEbdelOrderNum()));
            query.orderBy(qEbOrderLine.ebDelOrderLineNum.asc());
            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public Long getOrderLineCountTableau(SearchCriteriaOrder criteria, EbUser currentUser) {
        Long result = new Long(0);

        try {
            JPAQuery<EbOrderLine> query = new JPAQuery<EbOrderLine>(em);
            query.select(qEbOrderLine);
            query.from(qEbOrderLine);
            query.leftJoin(qEbOrderLine.xEcCountryOrigin(), qCountryOrigin);
            query.leftJoin(qEbOrderLine.xEbDelOrder(), qEbOrder);
            query.leftJoin(qEbOrder.xEbPartyDestination(), qPartyDest);
            query.leftJoin(qPartyDest.xEcCountry(), qCountryDest);

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            String searchTerm = criteria.getSearchterm();

            if (searchTerm != null && !(searchTerm = criteria.getSearchterm().toLowerCase().trim()).isEmpty()) {
                query.where(qEbOrderLine.itemName.containsIgnoreCase(searchTerm));
            }

            // uniquement les lignes planifiables sont affichés
            query.where(qEbOrderLine.quantityPlannable.gt(0l));
            query.where(qEbOrderLine.xEbDelOrder().ebDelOrderNum.eq(criteria.getEbdelOrderNum()));

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<EbOrderLine> findByTerm(Long ebDelOrderNum, String searchterm) {
        List<EbOrderLine> result = null;
        BooleanBuilder where = new BooleanBuilder();
        try {
            JPAQuery<EbOrderLine> query = new JPAQuery<EbOrderLine>(em);

            query
                .select(
                    QEbOrderLine
                        .create(
                            qEbOrderLine.ebDelOrderLineNum,
                            qEbOrderLine.idOrderLine,
                            qEbOrderLine.refArticle,
                            qEbOrderLine.articleName,
                            qEbOrderLine.dateOfAvailability,
                            qEbOrderLine.canceledReason,

                            qCountryOrigin.libelle,

                            qPartyDest.reference,
                            qPartyDest.company,
                            qCountryDest.libelle,

                            qEbOrder.numOrderSAP,
                            qEbOrder.orderCustomerName,
                            qEbOrder.campaignCode,
                            qEbOrder.campaignName,
                            qEbOrder.numOrderCustomer,
                            qEbOrder.orderStatus,
                            qEbOrder.customerCreationDate,
                            qEbOrder.deliveryDate,

                            qEbOrderLine.partNumber,
                            qEbOrderLine.serialNumber,
                            qEbOrderLine.dateRequest,
                            qEbOrderLine.priority,
                            qEbOrderLine.comment,
                            qEbOrderLine.specification,
                            qEbOrderLine.listCategories,
                            qEbOrderLine.customFields,
                            qEbOrderLine.weight,
                            qEbOrderLine.length,
                            qEbOrderLine.width,
                            qEbOrderLine.height,
                            qEbOrderLine.hsCode,
                            qEbOrderLine.volume,
                            qEbOrderLine.itemName,
                            qEbOrderLine.itemNumber,
                            qEbOrderLine.ebTypeUnitNum,
                            qEbOrderLine.quantityOrdered,
                            qEbOrderLine.quantityPlannable,
                            qEbOrderLine.quantityPlanned,
                            qEbOrderLine.quantityPending,
                            qEbOrderLine.quantityConfirmed,
                            qEbOrderLine.quantityShipped,
                            qEbOrderLine.quantityDelivered,
                            qEbOrderLine.quantityCanceled,
                            qEbOrderLine.serialized,
                            qEbOrderLine.price,
                            qEbOrderLine.eccn,
                            qCurrency.ecCurrencyNum, qCurrency.libelle));
            query.from(qEbOrderLine);
            query.leftJoin(qEbOrderLine.xEcCountryOrigin(), qCountryOrigin);
            query.leftJoin(qEbOrderLine.xEbDelOrder(), qEbOrder);
            query.leftJoin(qEbOrder.xEbPartyDestination(), qPartyDest);
            query.leftJoin(qPartyDest.xEcCountry(), qCountryDest);
            query.leftJoin(qEbOrderLine.currency(), qCurrency);

            if (ebDelOrderNum != null) {
                where.and(qEbOrderLine.xEbDelOrder().ebDelOrderNum.eq(ebDelOrderNum));
            }

            String value = searchterm;

            if (!StringUtils.isEmpty(value)) {
                value = value.trim();

                where
                    .andAnyOf(
                        qEbOrderLine.serialNumber.containsIgnoreCase(value),
                        qEbOrderLine.partNumber.containsIgnoreCase(value),
                        qEbOrderLine.itemNumber.containsIgnoreCase(value));
            }

            query.where(where);
            result = query.fetch();
        } catch (Exception e) {
            logger.error("error while getting order lines by searchterm");
            e.printStackTrace();
        }

        return result;
    }
}
