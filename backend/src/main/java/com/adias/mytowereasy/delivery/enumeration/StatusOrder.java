package com.adias.mytowereasy.delivery.enumeration;

import java.util.Arrays;
import java.util.Optional;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;

import com.adias.mytowereasy.model.StatusEnum;


public enum StatusOrder implements StatusEnum {
    PENDING(0, "PENDING", 0, true),
    NOTDLV(1, "NOTDLV", 1, true),
    PARTIALDLV(2, "PARTIALDLV", 2, true),
    TDLV(3, "TDLV", 3, true);

    private Integer code;
    private String key;
    private Integer order; // Order >= 0 are normal steps of workflow, negative
                           // are specials (like cancel)
    private Boolean enabled;

    private StatusOrder(Integer code, String key, Integer order, Boolean enabled) {
        this.code = code;
        this.key = key;
        this.order = order;
        this.enabled = enabled;
    }
    
	public static Optional<Integer> getCodeByLibelle(@Nonnull String libelle) {
		return Arrays.stream(values()).filter(it -> it.getKey().toLowerCase().contains(libelle.toLowerCase()))
				.map(StatusOrder::getCode).findFirst();
    }

	public static Optional<String> getKeyByCode(@Nonnull Integer code) {
		return Arrays.stream(values()).filter(it -> it.getCode().equals(code)).map(StatusOrder::getKey).findFirst();
	}

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Integer getOrder() {
        return order;
    }

    @Override
    public Boolean getEnabled() {
        return enabled;
    }
}
