package com.adias.mytowereasy.delivery.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.delivery.dao.DaoDeliveryLine;
import com.adias.mytowereasy.delivery.dao.DaoVisibility;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.model.QEbLivraison;
import com.adias.mytowereasy.delivery.model.QEbLivraisonLine;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbParty;
import com.adias.mytowereasy.model.QEcCountry;
import com.adias.mytowereasy.model.QEcCurrency;


@Service
@Transactional
public class DaoDeliveryLineImpl implements DaoDeliveryLine {

    @PersistenceContext
    EntityManager em;

    @Autowired
    DaoVisibility daoVisibility;

    QEbLivraison qEbLivraison = QEbLivraison.ebLivraison;
    QEbLivraisonLine qEbLivraisonLine = QEbLivraisonLine.ebLivraisonLine;
    QEbLivraison qLivraisonFromLine = new QEbLivraison("qLivraisonFromLine");
    QEbParty qEbPartyDest = new QEbParty("qEbPartyDest");
    QEcCountry qEcCountryDest = new QEcCountry("qEcCountryDest");
    QEbParty qEbPartyOrigin = new QEbParty("qEbPartyOrigin");
    QEcCountry qEcCountryOrigin = new QEcCountry("qEcCountryOrigin");

    QEcCurrency qCurrency = new QEcCurrency("qCurrency");

    @SuppressWarnings("unchecked")
    @Override
    public List<EbLivraisonLine> getDeliveryLinesForExport(SearchCriteriaDelivery criteria, EbUser currentUser) {
        List<EbLivraisonLine> result = null;

        try {
            JPAQuery<EbLivraisonLine> query = new JPAQuery<EbLivraisonLine>(em);

            query
                .select(
                    QEbLivraisonLine
                        .create(
                            qEbLivraisonLine.ebDelLivraisonLineNum,
                            qEbLivraison.refDelivery,
                            qEbLivraison.numOrderSAP,
                            qEbLivraison.refTransport,
                            qEbLivraison.numOrderCustomer,

                            qEbPartyDest.reference,
                            qEbPartyDest.company,
                            qEcCountryDest.libelle,

                            qEbLivraison.orderCustomerCode,
                            qEbLivraison.orderCustomerName,
                            qEbLivraison.campaignCode,
                            qEbLivraison.campaignName,
                            qEbLivraison.orderCreationDate,
                            qEbLivraison.expectedDeliveryDate,
                            qEbLivraison.suggestedDeliveryDate,
                            qEbLivraison.deliveryCreationDate,
                            qEbLivraison.estimatedShipDate,
                            qEbLivraison.effectiveShipDate,
                            qEbLivraison.pickupSite,
                            qEbLivraison.endOfPackDate,
                            qEbLivraison.pickDate,
                            qEbLivraison.invoiceNum,
                            qEbLivraison.invoiceDate,
                            qEbLivraison.statusDelivery,
                            qEbLivraison.transportStatus,
                            qEbLivraison.numParcels,
                            qEbLivraison.totalWeight,
                            qEbLivraison.totalVolume,
                            qEbLivraison.crossDock,

                            qEbLivraisonLine.refArticle,
                            qEbLivraisonLine.articleName,
                            qEcCountryOrigin.libelle,
                            qEbLivraisonLine.customsCode,
                            qEbLivraisonLine.batch,
                            qEbLivraisonLine.quantity,
                            qEbLivraisonLine.partNumber,
                            qEbLivraisonLine.serialNumber,
                            qEbLivraisonLine.dateOfRequest,
                            qEbLivraisonLine.priority,
                            qEbLivraisonLine.comment,
                            qEbLivraisonLine.specification,
                            qEbLivraisonLine.listCategories,
                            qEbLivraisonLine.customFields,
                            qEbLivraisonLine.weight,
                            qEbLivraisonLine.width,
                            qEbLivraisonLine.length,
                            qEbLivraisonLine.hsCode,
                            qEbLivraisonLine.height,
                            qEbLivraisonLine.volume,
                            qEbLivraisonLine.itemName,
                            qEbLivraisonLine.itemNumber,
                            qEbLivraisonLine.targetDate,
                            qEbLivraisonLine.serialized

                        ));
            query.from(qEbLivraisonLine);
            query.leftJoin(qEbLivraisonLine.xEcCountryOrigin(), qEcCountryOrigin);
            query.leftJoin(qEbLivraisonLine.xEbDelLivraison(), qEbLivraison);
            query.leftJoin(qEbLivraison.xEbPartyDestination(), qEbPartyDest);
            query.leftJoin(qEbPartyDest.xEcCountry(), qEcCountryDest);
            query.limit(criteria.getSize());
            query = criteria.applyCriteria(query, true);
            query = daoVisibility.addVisibilityToDelivery(query, criteria, currentUser);

            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<EbLivraisonLine> getDeliveryLines(Long ebDelLivraisonNum, EbUser currentUser) {
        List<EbLivraisonLine> result = null;

        try {
            JPAQuery<EbLivraisonLine> query = new JPAQuery<EbLivraisonLine>(em);

            query
                .select(
                    QEbLivraisonLine
                        .create(
                            qEbLivraisonLine.ebDelLivraisonLineNum,
                            qEbLivraison.refDelivery,
                            qEbLivraison.numOrderSAP,
                            qEbLivraison.refTransport,
                            qEbLivraison.numOrderCustomer,

                            qEbPartyDest.reference,
                            qEbPartyDest.company,
                            qEcCountryDest.libelle,

                            qEbLivraison.orderCustomerCode,
                            qEbLivraison.orderCustomerName,
                            qEbLivraison.campaignCode,
                            qEbLivraison.campaignName,
                            qEbLivraison.orderCreationDate,
                            qEbLivraison.expectedDeliveryDate,
                            qEbLivraison.suggestedDeliveryDate,
                            qEbLivraison.deliveryCreationDate,
                            qEbLivraison.estimatedShipDate,
                            qEbLivraison.effectiveShipDate,
                            qEbLivraison.pickupSite,
                            qEbLivraison.endOfPackDate,
                            qEbLivraison.pickDate,
                            qEbLivraison.invoiceNum,
                            qEbLivraison.invoiceDate,
                            qEbLivraison.statusDelivery,
                            qEbLivraison.transportStatus,
                            qEbLivraison.numParcels,
                            qEbLivraison.totalWeight,
                            qEbLivraison.totalVolume,
                            qEbLivraison.crossDock,

                            qEbLivraisonLine.refArticle,
                            qEbLivraisonLine.articleName,
                            qEcCountryOrigin.libelle,
                            qEbLivraisonLine.customsCode,
                            qEbLivraisonLine.batch,
                            qEbLivraisonLine.quantity,
                            qEbLivraisonLine.partNumber,
                            qEbLivraisonLine.serialNumber,
                            qEbLivraisonLine.dateOfRequest,
                            qEbLivraisonLine.priority,
                            qEbLivraisonLine.comment,
                            qEbLivraisonLine.specification,
                            qEbLivraisonLine.listCategories,
                            qEbLivraisonLine.customFields,
                            qEbLivraisonLine.weight,
                            qEbLivraisonLine.width,
                            qEbLivraisonLine.length,
                            qEbLivraisonLine.hsCode,
                            qEbLivraisonLine.height,
                            qEbLivraisonLine.volume,
                            qEbLivraisonLine.itemName,
                            qEbLivraisonLine.itemNumber,
                            qEbLivraisonLine.targetDate,
                            qEbLivraisonLine.serialized,
                            qEbLivraisonLine.price,
                            qEbLivraisonLine.eccn,
                            qCurrency.ecCurrencyNum
                        ));
            query.from(qEbLivraisonLine);
            query.leftJoin(qEbLivraisonLine.xEcCountryOrigin(), qEcCountryOrigin);
            query.leftJoin(qEbLivraisonLine.xEbDelLivraison(), qEbLivraison);
            query.leftJoin(qEbLivraison.xEbPartyDestination(), qEbPartyDest);
            query.leftJoin(qEbPartyDest.xEcCountry(), qEcCountryDest);
            query.leftJoin(qEbLivraisonLine.currency(), qCurrency);

            result = query
                .from(qEbLivraisonLine)
                .where(
                    qEbLivraisonLine.xEbDelLivraison().ebDelLivraisonNum
                        .eq(ebDelLivraisonNum)
                        .and(qEbLivraisonLine.deleteFlag.isFalse().or(qEbLivraisonLine.deleteFlag.isNull())))
                .fetch();
            ;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<EbLivraisonLine>
        getDeliveryLinesForGenericTable(SearchCriteriaDelivery criteriaDelivery, EbUser currentUser) {
        List<EbLivraisonLine> result = null;

        try {
            JPAQuery<EbLivraisonLine> query = new JPAQuery<EbLivraisonLine>(em);

            query
                .select(
                    QEbLivraisonLine
                        .create(
                            qEbLivraisonLine.ebDelLivraisonLineNum,
                            qEbLivraison.refDelivery,
                            qEbLivraison.numOrderSAP,
                            qEbLivraison.refTransport,
                            qEbLivraison.numOrderCustomer,

                            qEbPartyDest.reference,
                            qEbPartyDest.company,
                            qEcCountryDest.libelle,

                            qEbLivraison.orderCustomerCode,
                            qEbLivraison.orderCustomerName,
                            qEbLivraison.campaignCode,
                            qEbLivraison.campaignName,
                            qEbLivraison.orderCreationDate,
                            qEbLivraison.expectedDeliveryDate,
                            qEbLivraison.suggestedDeliveryDate,
                            qEbLivraison.deliveryCreationDate,
                            qEbLivraison.estimatedShipDate,
                            qEbLivraison.effectiveShipDate,
                            qEbLivraison.pickupSite,
                            qEbLivraison.endOfPackDate,
                            qEbLivraison.pickDate,
                            qEbLivraison.invoiceNum,
                            qEbLivraison.invoiceDate,
                            qEbLivraison.statusDelivery,
                            qEbLivraison.transportStatus,
                            qEbLivraison.numParcels,
                            qEbLivraison.totalWeight,
                            qEbLivraison.totalVolume,
                            qEbLivraison.crossDock,

                            qEbLivraisonLine.refArticle,
                            qEbLivraisonLine.articleName,
                            qEcCountryOrigin.libelle,
                            qEbLivraisonLine.customsCode,
                            qEbLivraisonLine.batch,
                            qEbLivraisonLine.quantity,
                            qEbLivraisonLine.partNumber,
                            qEbLivraisonLine.serialNumber,
                            qEbLivraisonLine.dateOfRequest,
                            qEbLivraisonLine.priority,
                            qEbLivraisonLine.comment,
                            qEbLivraisonLine.specification,
                            qEbLivraisonLine.listCategories,
                            qEbLivraisonLine.customFields,
                            qEbLivraisonLine.weight,
                            qEbLivraisonLine.width,
                            qEbLivraisonLine.length,
                            qEbLivraisonLine.hsCode,
                            qEbLivraisonLine.height,
                            qEbLivraisonLine.volume,
                            qEbLivraisonLine.itemName,
                            qEbLivraisonLine.itemNumber,
                            qEbLivraisonLine.targetDate,
                            qEbLivraisonLine.serialized,
                            qEbLivraisonLine.price,
                            qEbLivraisonLine.eccn,
                            qCurrency.ecCurrencyNum
                        ));
            query.from(qEbLivraisonLine);
            query.leftJoin(qEbLivraisonLine.xEcCountryOrigin(), qEcCountryOrigin);
            query.leftJoin(qEbLivraisonLine.xEbDelLivraison(), qEbLivraison);
            query.leftJoin(qEbLivraison.xEbPartyDestination(), qEbPartyDest);
            query.leftJoin(qEbPartyDest.xEcCountry(), qEcCountryDest);
            query.leftJoin(qEbLivraisonLine.currency(), qCurrency);

            if (criteriaDelivery.getSize() != null && criteriaDelivery.getSize() >= 0) {
                query.limit(criteriaDelivery.getSize());
            }

            if (criteriaDelivery.getPageNumber() != null) {
                query
                    .offset(
                        criteriaDelivery.getPageNumber()
                            * (criteriaDelivery.getSize() != null ? criteriaDelivery.getSize() : 1));
            }

            String searchTerm = criteriaDelivery.getSearchterm();

            if (searchTerm != null && !(searchTerm = criteriaDelivery.getSearchterm().toLowerCase().trim()).isEmpty()) {
                query.where(qEbLivraisonLine.itemName.containsIgnoreCase(searchTerm));
            }

            query
                .where(
                    qEbLivraisonLine.xEbDelLivraison().ebDelLivraisonNum.eq(criteriaDelivery.getEbDelLivraisonNum()));

            result = query.fetch();
            ;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Long getDeliveryLineCountForGenericTable(SearchCriteriaDelivery criteriaDelivery, EbUser currentUser) {
        Long result = new Long(0);

        try {
            JPAQuery<EbLivraisonLine> query = new JPAQuery<EbLivraisonLine>(em);

            query.select(qEbLivraisonLine);

            if (criteriaDelivery.getSize() != null && criteriaDelivery.getSize() >= 0) {
                query.limit(criteriaDelivery.getSize());
            }

            if (criteriaDelivery.getPageNumber() != null) {
                query
                    .offset(
                        criteriaDelivery.getPageNumber()
                            * (criteriaDelivery.getSize() != null ? criteriaDelivery.getSize() : 1));
            }

            String searchTerm = criteriaDelivery.getSearchterm();

            if (searchTerm != null && !(searchTerm = criteriaDelivery.getSearchterm().toLowerCase().trim()).isEmpty()) {
                query.where(qEbLivraisonLine.itemName.containsIgnoreCase(searchTerm));
            }

            query
                .from(qEbLivraisonLine).where(
                    qEbLivraisonLine.xEbDelLivraison().ebDelLivraisonNum.eq(criteriaDelivery.getEbDelLivraisonNum()));

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
