package com.adias.mytowereasy.delivery.dao;

import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.model.EbUser;


public interface DaoVisibility {
    JPAQuery addVisibilityToOrder(JPAQuery query, SearchCriteriaOrder criteria, EbUser currentUser);

    JPAQuery addVisibilityToDelivery(JPAQuery query, SearchCriteriaDelivery criteria, EbUser currentUser);
}
