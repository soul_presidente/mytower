package com.adias.mytowereasy.delivery.dao;

import java.util.List;

import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.dto.EbOrderPlanificationDTO;
import com.adias.mytowereasy.model.EbUser;


public interface DaoOrder {
    public List<EbOrder> getListOrders(SearchCriteriaOrder criteria, EbUser currentUser);

    public EbOrder getOrder(SearchCriteriaOrder criteria);

    public Long getOrderCount(SearchCriteriaOrder criteria, EbUser currentUser);

    public List<EbOrder> getAutoCompleteList(String term, String field);

    public EbOrder saveOrder(EbOrder order);

    public EbLivraison planifyOrder(EbOrderPlanificationDTO orderPlanification);

    public EbLivraison updatePlanification(EbOrderPlanificationDTO orderPlanification);

    public Integer nextOrderNumValue();
}
