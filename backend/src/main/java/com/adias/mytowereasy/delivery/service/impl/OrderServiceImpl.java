package com.adias.mytowereasy.delivery.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.delivery.dao.DaoOrder;
import com.adias.mytowereasy.delivery.enumeration.StatusOrder;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.repository.EbLivraisonLineRepository;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.repository.EbOrderLineRepository;
import com.adias.mytowereasy.delivery.repository.EbOrderRepository;
import com.adias.mytowereasy.delivery.service.OrderService;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.dto.EbOrderPlanificationDTO;
import com.adias.mytowereasy.dto.mapper.DeliveryMapper;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbChatRepository;
import com.adias.mytowereasy.repository.EbEtablissementRepository;
import com.adias.mytowereasy.repository.EbTypeUnitRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.FlagService;
import com.adias.mytowereasy.util.Constants;
import com.adias.mytowereasy.util.JsonUtils;


@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    EbLivraisonLineRepository ebLivraisonLineRepository;

    @Autowired
    EbLivraisonRepository ebLivraisonRepository;

    @Autowired
    DaoOrder daoOrder;

    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    EbOrderRepository ebOrderRepository;

    @Autowired
    EbEtablissementRepository ebEtablissementRepository;

    @Autowired
    EbOrderLineRepository ebOrderLineRepository;

    @Autowired
    EbTypeUnitRepository ebTypeUnitRepository;

    @Autowired
    EbChatRepository ebChatRepositorty;

    @Autowired
    MessageSource messageSource;

    @Autowired
    DeliveryMapper deliveryMapper;

    @Autowired
    FlagService flagService;

    @Override
    public List<EbOrder> getListOrders(SearchCriteriaOrder criteria) {
        EbUser currentUser = connectedUserService.getCurrentUser();
        return daoOrder.getListOrders(criteria, currentUser);
    }

    @Override
    public EbOrder getOrder(SearchCriteriaOrder criteria) {
        return daoOrder.getOrder(criteria);
    }

    @Override
    public Integer getOrderCount(SearchCriteriaOrder criteria) {
        EbUser currentUser = connectedUserService.getCurrentUser();
        long count = daoOrder.getOrderCount(criteria, currentUser);
        return (int) count;
    }

    @Override
    public List<EbOrder> getAutoCompleteList(String term, String field) {
        return daoOrder.getAutoCompleteList(term, field);
    }

    @Override
    public void historizeOrderAction(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        String username,
        String usernameFor) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        EbChat chat = new EbChat(connectedUser);
        chat.setUserName(connectedUser.getNomPrenom());
        chat.setDateCreation(new Date());
        chat.setModule(module);
        chat.setIdFiche(idFiche);
        chat.setIsHistory(true);
        chat.setIdChatComponent(chatCompId);

        String lang = connectedUser != null && connectedUser.getLanguage() != null ? connectedUser.getLanguage() : "en";
        Locale lcl = new Locale(lang);
        String action1 = messageSource.getMessage("tracing.message_rd.by", null, lcl);
        String action2 = messageSource.getMessage("tracing.message_rd.for", null, lcl);
        chat.setText(action + " <b>" + action1 + " <b>" + username + "</b>");
        if (usernameFor != null) chat.setText(chat.getText() + "<b>" + action2 + "<b>" + usernameFor + "</b>");
        ebChatRepositorty.save(chat);
    }

    @Override
    public EbOrder createOrder(EbOrder order) {

        if (order.getCustomerOrderReference() == null) {
            throw new MyTowerException("Customer Order Reference is null");
        }

        SearchCriteriaOrder orderSearchCriteria = deliveryMapper.orderToSearchCriteriaOrder(order);
        EbOrder orderToCrush = getOrder(orderSearchCriteria);

        if (orderToCrush != null) {
            throw new MyTowerException("Customer Order Reference Already exists");
        }

        return this.saveOrder(order);
    }

    @Override
    public EbOrder updateOrder(EbOrder order) {

        if (order.getCustomerOrderReference() == null) {
            throw new MyTowerException("Customer Order Reference is null");
        }

        SearchCriteriaOrder orderSearchCriteria = deliveryMapper.orderToSearchCriteriaOrder(order);
        EbOrder dbOrder = getOrder(orderSearchCriteria);

        if (dbOrder == null) {
            throw new MyTowerException("order not found");
        }

        order.setEbDelOrderNum(dbOrder.getEbDelOrderNum());

        EbParty xEbPartyOrigin = dbOrder.getxEbPartyOrigin();
        EbParty xEbPartyDestination = dbOrder.getxEbPartyDestination();
        EbParty xEbPartySale = dbOrder.getxEbPartySale();
        EbParty xEbPartyVendor = dbOrder.getxEbPartyVendor();
        EbParty xEbPartyIssuer = dbOrder.getxEbPartyIssuer();


        // we do this to update EbParty if already exist && not to create new EbParty
        if (xEbPartyOrigin != null) {
            order.getxEbPartyOrigin().setEbPartyNum(xEbPartyOrigin.getEbPartyNum());
        }

        if (xEbPartyDestination != null) {
            order.getxEbPartyDestination().setEbPartyNum(xEbPartyDestination.getEbPartyNum());
        }

        if (xEbPartySale != null && order.getxEbPartySale() != null) {
            order.getxEbPartySale().setEbPartyNum(xEbPartySale.getEbPartyNum());
        }

        if (xEbPartyVendor != null && order.getxEbPartyVendor() != null) {
            order.getxEbPartyVendor().setEbPartyNum(xEbPartyVendor.getEbPartyNum());
        }

        if (xEbPartyIssuer != null && order.getxEbPartyIssuer() != null) {
            order.getxEbPartyIssuer().setEbPartyNum(xEbPartyIssuer.getEbPartyNum());
        }

        if (dbOrder.getListCustomsFields() != null) {
            String customFields = JsonUtils.serializeToString(dbOrder.getListCustomsFields(), null);
            order.setCustomFields(customFields);
        }

        ebOrderLineRepository.deleteOrderLineByOrderNum(dbOrder.getEbDelOrderNum());

        return this.saveOrder(order);
    }

    private EbOrder saveOrder(EbOrder order) {
        EbUser currentUser = connectedUserService.getCurrentUser();

        order.setxEbCompagnie(order.getxEbOwnerOfTheRequest().getEbCompagnie());
        order.setxEbEtablissement(order.getxEbOwnerOfTheRequest().getEbEtablissement());
        //set order reference
        if (StringUtils.isBlank(order.getReference())) {
            Integer orderNum = daoOrder.nextOrderNumValue();
            order.setReference(order.getxEbCompagnie().getEbCompagnieNum() + "O" + StringUtils.leftPad(orderNum.toString().toUpperCase(), 5, "0"));
        }

        if (order.getOrderLines() == null || order.getOrderLines().isEmpty()) {
            throw new MyTowerException("Order must have at least one line");
        }

        order.getOrderLines().forEach(orderLine -> {

            if (orderLine.getEbTypeUnitNum() != null) {
                EbTypeUnit typeUnite = ebTypeUnitRepository.findByEbTypeUnitNum(orderLine.getEbTypeUnitNum());
                orderLine.setxEbtypeOfUnit(typeUnite);
            }

        });

        order = daoOrder.saveOrder(order);
        if (order.getEbDelOrderNum() != null) order
            .setReference(
                generateReference(
                    order.getxEbOwnerOfTheRequest().getEbCompagnie().getCode(),
                    order.getEbDelOrderNum(),
                    Constants.refOrderPrefix));
        String lang = currentUser != null && currentUser.getLanguage() != null ? currentUser.getLanguage() : "en";
        Locale lcl = new Locale(lang);
        String action1 = messageSource.getMessage("orderHasBeenUpdated", null, lcl);
        this
            .historizeOrderAction(
                order.getEbDelOrderNum().intValue(),
                Enumeration.IDChatComponent.ORDER_MANAGEMENT.getCode(),
                Enumeration.Module.ORDER_MANAGEMENT.getCode(),
                action1,
                connectedUserService.getCurrentUser().getNomPrenom(),
                null);
        this.calculateOrder(order.getEbDelOrderNum(), true);

        return daoOrder.saveOrder(order);
    }

    @Override
    public void updateOrderEtablissement(TransfertUserObject transfertUserObjet) {
        ebOrderRepository
            .updateOrderEtablissementAndLabels(
                transfertUserObjet.getOldEtablissement(),
                transfertUserObjet.getNewEtablissement(),
                transfertUserObjet.getOldListLabels());
    }

    @Override
    public EbLivraison planifyOrder(EbOrderPlanificationDTO orderPlanification) {
        EbLivraison planifyOrder = daoOrder.planifyOrder(orderPlanification);
        // Toutes les quantités des order sont recalculés automatiquement dans
        // la méthode calculateOrder
        this.calculateOrder(orderPlanification.getEbDelOrderNum(), true);

        return planifyOrder;
    }

    @Override
    public EbOrder saveFlags(Long ebDelOrderNum, String newFlags) {
        EbOrder order = ebOrderRepository.findById(ebDelOrderNum).get();
        order.setListFlag(newFlags);
        return ebOrderRepository.save(order);
    }

    @Override
    public EbOrder calculateOrder(Long ebDelOrderNum, boolean autoPersist) {
        EbOrder order = ebOrderRepository
            .findById(ebDelOrderNum).orElseThrow(() -> new MyTowerException("Order doesn't exist"));

        order = this.calculateQuantities(order);

        if (autoPersist) {
            ebOrderRepository.save(order);
        }

        return order;
    }

    @Override
    public EbOrder calculateStatus(EbOrder order) {
        StatusOrder newStatus = null;

        if (order.getTotalQuantityPlannable() > 0 && order.getTotalQuantityPlanned() == 0) {
            newStatus = StatusOrder.PENDING;
            order.setOrderStatus(newStatus.getCode());
        }
        else if (order.getTotalQuantityPlanned() > 0 && order.getTotalQuantityDelivered() == 0) {
            order.setOrderStatus(StatusOrder.NOTDLV.getCode());
        }
        else if (order.getTotalQuantityDelivered() > 0 &&
            order.getTotalQuantityDelivered() < order.getTotalQuantityOrdered()) {
                newStatus = StatusOrder.PARTIALDLV;
                order.setOrderStatus(newStatus.getCode());
            }
        else if (order.getTotalQuantityOrdered().equals(order.getTotalQuantityDelivered())) {
            newStatus = StatusOrder.TDLV;
            order.setOrderStatus(newStatus.getCode());
        }

        return order;
    }

    @Override
    public EbOrder calculateQuantities(EbOrder order) {
        // Init totals quantities to 0 before iterate over each lines
        order.setTotalQuantityOrdered(0l);
        order.setTotalQuantityPlannable(0l);
        order.setTotalQuantityPlanned(0l);
        order.setTotalQuantityPending(0l);
        order.setTotalQuantityConfirmed(0l);
        order.setTotalQuantityShipped(0l);
        order.setTotalQuantityDelivered(0l);
        order.setTotalQuantityCanceled(0l);

        // Calc order lines before order totals
        for (EbOrderLine orderLine: order.getOrderLines()) {
            calculateQuantitiesForLine(orderLine);

            // Calc total
            order.setTotalQuantityOrdered(order.getTotalQuantityOrdered() + orderLine.getQuantityOrdered());
            order.setTotalQuantityPlannable(order.getTotalQuantityPlannable() + orderLine.getQuantityPlannable());
            order.setTotalQuantityPlanned(order.getTotalQuantityPlanned() + orderLine.getQuantityPlanned());
            order.setTotalQuantityPending(order.getTotalQuantityPending() + orderLine.getQuantityPending());
            order.setTotalQuantityConfirmed(order.getTotalQuantityConfirmed() + orderLine.getQuantityConfirmed());
            order.setTotalQuantityShipped(order.getTotalQuantityShipped() + orderLine.getQuantityShipped());
            order.setTotalQuantityDelivered(order.getTotalQuantityDelivered() + orderLine.getQuantityDelivered());
            order.setTotalQuantityCanceled(order.getTotalQuantityCanceled() + orderLine.getQuantityCanceled());
        }

        return order;
    }

    public void calculateQuantitiesForLine(EbOrderLine orderLine) {
        // Prevent null values for reference quantities and replace them with 0
        if (orderLine.getQuantityOrdered() == null) orderLine.setQuantityOrdered(0l);

        // Init calculated quantities to 0
        orderLine.setQuantityPlannable(0l);
        orderLine.setQuantityPlanned(0l);
        orderLine.setQuantityPending(0l);
        orderLine.setQuantityConfirmed(0l);
        orderLine.setQuantityShipped(0l);
        orderLine.setQuantityDelivered(0l);
        orderLine.setQuantityCanceled(0l);

        // Calc quantities from delivery lines
        List<EbLivraisonLine> ebLivraisonLines = ebLivraisonLineRepository.findByxEbOrderLigne(orderLine);

        for (EbLivraisonLine ebLivraisonLine: ebLivraisonLines) {
            EbLivraison ebLivraison = ebLivraisonRepository
                .findByLivraisonLine(ebLivraisonLine.getEbDelLivraisonLineNum()).get();

            if (ebLivraison.getStatusDelivery() != null) {
                Long deliveryLineQuantity = ebLivraisonLine.getQuantity() != null ? ebLivraisonLine.getQuantity() : 0l;

                switch (ebLivraison.getStatusDelivery()) {
                    case CANCELED:
                        orderLine.setQuantityCanceled(orderLine.getQuantityCanceled() + deliveryLineQuantity);
                        break;

                    case CONFIRMED:
                        orderLine.setQuantityConfirmed(orderLine.getQuantityConfirmed() + deliveryLineQuantity);
                        break;

                    case DELIVERED:
                        orderLine.setQuantityDelivered(orderLine.getQuantityDelivered() + deliveryLineQuantity);
                        break;

                    case PENDING:
                        orderLine.setQuantityPending(orderLine.getQuantityPending() + deliveryLineQuantity);
                        break;

                    case SHIPPED:
                        orderLine.setQuantityShipped(orderLine.getQuantityShipped() + deliveryLineQuantity);
                        break;

                    default:
                        break;
                }

            }

        }

        Long planned = orderLine.getQuantityConfirmed() +
            orderLine.getQuantityDelivered() + orderLine.getQuantityPending() + orderLine.getQuantityShipped();
        orderLine.setQuantityPlanned(planned);

        Long plannable = orderLine.getQuantityOrdered() - planned;

        if (plannable < 0) {
            plannable = 0l;
        }

        orderLine.setQuantityPlannable(plannable);
    }

    @Override
    public String generateReference(String companyCode, Long entityNum, String prefix) {
        String refEntity = StringUtils.leftPad(entityNum.toString().toUpperCase(), 5, "0");
        return companyCode.concat(prefix).concat(refEntity);
    }

}
