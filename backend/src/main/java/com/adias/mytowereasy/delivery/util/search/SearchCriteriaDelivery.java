/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.delivery.util.search;

import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.QEbLivraison;
import com.adias.mytowereasy.delivery.model.QEbLivraisonLine;
import com.adias.mytowereasy.delivery.model.QEbOrder;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.QEbParty;
import com.adias.mytowereasy.model.QEcCountry;
import com.adias.mytowereasy.model.Statiques;
import com.adias.mytowereasy.util.enums.GenericEnumException;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public class SearchCriteriaDelivery extends SearchCriteria {
    BooleanBuilder w = new BooleanBuilder();
    private QEbLivraisonLine qEbLivraisonLine = QEbLivraisonLine.ebLivraisonLine;

    private Long ebDelLivraisonNum;

    private Long ebDelOrderNum;

    private String countryDestination;

    private String numOrderCustomer;

    private String ebDelReference;

    private String customerOrderReference;

    private boolean isCount = false;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listOrderCustomerName;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listRefDelivery;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listStatusDelivery;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listNumOrderCustomer;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)

    private Map<String, Path<?>> orderMap = new HashMap<String, Path<?>>();
    private Map<String, OrderSpecifier<?>> orderMapASC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, OrderSpecifier<?>> orderMapDESC = new HashMap<String, OrderSpecifier<?>>();

    QEcCountry qEcCountryDest = new QEcCountry("qEcCountryDest");
    QEbLivraison qEbLivraison = new QEbLivraison("ebLivraison");
    QEbOrder qEbOrder = new QEbOrder("xEbOrder");

    private QEbParty qPartyDest = new QEbParty("partyDest");
    private QEbParty qPartyOrigin = new QEbParty("partyOrigin");

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dateOfGoodsAvailability;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date customerDeliveryDate;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listPartyOrigin;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Timestamp customerCreationDate;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listcustomFields;

    private Boolean excludeCanceled;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCustomerOrderReference;

    public SearchCriteriaDelivery setProperty(String property, String value, SearchCriteriaDelivery criteria) {
        Field field = null;

        try {
            Class<?> c = criteria.getClass();
            field = c.getDeclaredField(property);

            try {
                field.set(criteria, value);
                criteria.setOrderedColumn(property);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }

        } catch (NoSuchFieldException e1) {
            e1.printStackTrace();
        }

        return criteria;
    }

    public void setSearchInput(String searchInput, List<CustomFields> customsFields) throws JsonParseException, JsonMappingException, IOException {
        if (searchInput == null) searchInput = "{}";

        super.setSearchInput(searchInput, customsFields);

        this.listStatusDelivery = ((SearchCriteriaDelivery) deserialize(
            searchInput,
            SearchCriteriaDelivery.class)).listStatusDelivery;

        this.dateOfGoodsAvailability = ((SearchCriteriaDelivery) deserialize(
            searchInput,
            SearchCriteriaDelivery.class)).dateOfGoodsAvailability;

        this.listCustomerOrderReference = ((SearchCriteriaDelivery) deserialize(
            searchInput,
            SearchCriteriaDelivery.class)).listCustomerOrderReference;

        this.listRefDelivery = ((SearchCriteriaDelivery) deserialize(
            searchInput,
            SearchCriteriaDelivery.class)).listRefDelivery;

        this.customerCreationDate = ((SearchCriteriaDelivery) deserialize(
            searchInput,
            SearchCriteriaDelivery.class)).customerCreationDate;

        this.customerDeliveryDate = ((SearchCriteriaDelivery) deserialize(
            searchInput,
            SearchCriteriaDelivery.class)).customerDeliveryDate;
    }

    private void Initialize(QEbLivraison qEbLivraison) {
        orderMapASC.clear();
        orderMap.clear();
        orderMapDESC.clear();

        orderMap.put("ebDelLivraisonNum", qEbLivraison.ebDelLivraisonNum);
        orderMap.put("numOrderCustomer", qEbLivraison.numOrderCustomer);
        orderMap.put("orderCreationDate", qEbLivraison.orderCreationDate);
        orderMap.put("deliveryDate", qEbLivraison.requestedDeliveryDate);
        orderMap.put("deliveryCreationDate", qEbLivraison.pickDate);
        orderMap.put("refClientDelivered", qEbLivraison.xEbPartyDestination().reference);
        orderMap.put("nameClientDelivered", qEbLivraison.xEbPartyDestination().company);
        orderMap.put("countryClientDelivered", qEbLivraison.xEbPartyDestination().xEcCountry().libelle);
        orderMap.put("refClientCreator", qEbLivraison.xEbPartySale().reference);
        orderMap.put("nameOrderCreator", qEbLivraison.xEbPartySale().company);
        orderMap.put("orderCustomerCode", qEbLivraison.orderCustomerCode);
        orderMap.put("orderCustomerName", qEbLivraison.orderCustomerName);

        orderMapASC.put("ebDelLivraisonNum", qEbLivraison.ebDelLivraisonNum.asc());
        orderMapASC.put("numOrderCustomer", qEbLivraison.numOrderCustomer.asc());
        orderMapASC.put("orderCreationDate", qEbLivraison.orderCreationDate.asc());
        orderMapASC.put("deliveryDate", qEbLivraison.requestedDeliveryDate.asc());
        orderMapASC.put("refClientDelivered", qEbLivraison.xEbPartyDestination().reference.asc());
        orderMapASC.put("nameClientDelivered", qEbLivraison.xEbPartyDestination().company.asc());
        orderMapASC.put("countryClientDelivered", qEbLivraison.xEbPartyDestination().xEcCountry().libelle.asc());
        orderMapASC.put("refClientCreator", qEbLivraison.xEbPartySale().reference.asc());
        orderMapASC.put("nameOrderCreator", qEbLivraison.xEbPartySale().company.asc());
        orderMapASC.put("orderCustomerCode", qEbLivraison.orderCustomerCode.asc());
        orderMapASC.put("orderCustomerName", qEbLivraison.orderCustomerName.asc());

        orderMapDESC.put("ebDelLivraisonNum", qEbLivraison.ebDelLivraisonNum.desc());
        orderMapDESC.put("orderCreationDate", qEbLivraison.orderCreationDate.desc());
        orderMapDESC.put("deliveryDate", qEbLivraison.requestedDeliveryDate.desc());
        orderMapDESC.put("requestedDeliveryDate", qEbLivraison.requestedDeliveryDate.desc());
        orderMapDESC.put("deliveryCreationDate", qEbLivraison.pickDate.desc());
        orderMapDESC.put("refClientDelivered", qEbLivraison.xEbPartyDestination().reference.desc());
        orderMapDESC.put("nameClientDelivered", qEbLivraison.xEbPartyDestination().company.desc());
        orderMapDESC.put("countryClientDelivered", qEbLivraison.xEbPartyDestination().xEcCountry().libelle.desc());
        orderMapDESC.put("refClientCreator", qEbLivraison.xEbPartySale().reference.desc());
        orderMapDESC.put("nameOrderCreator", qEbLivraison.xEbPartySale().company.desc());
        orderMapDESC.put("orderCustomerCode", qEbLivraison.orderCustomerCode.desc());
        orderMapDESC.put("orderCustomerName", qEbLivraison.orderCustomerName.desc());

        orderMap.put("ebDelLivraisonNum", qEbLivraison.ebDelLivraisonNum);
        orderMapASC.put("ebDelLivraisonNum", qEbLivraison.ebDelLivraisonNum.asc());
        orderMapDESC.put("ebDelLivraisonNum", qEbLivraison.ebDelLivraisonNum.desc());
        // delivery by Owner of request
        orderMap.put("xEbOwnerOfTheRequest", qEbLivraison.xEbOwnerOfTheRequest().nom);
        orderMapASC.put("xEbOwnerOfTheRequest", qEbLivraison.xEbOwnerOfTheRequest().nom.asc());
        orderMapDESC.put("xEbOwnerOfTheRequest", qEbLivraison.xEbOwnerOfTheRequest().nom.desc());

        // delivery by origin party
        orderMap.put("xEbPartyOrigin", qEbLivraison.xEbPartyOrigin().city);
        orderMapASC.put("xEbPartyOrigin", qEbLivraison.xEbPartyOrigin().city.asc());
        orderMapDESC.put("xEbPartyOrigin", qEbLivraison.xEbPartyOrigin().city.desc());

        // delivery by destination party
        orderMap.put("xEbPartyDestination", qEbLivraison.xEbPartyDestination().city);
        orderMapASC.put("xEbPartyDestination", qEbLivraison.xEbPartyDestination().city.asc());
        orderMapDESC.put("xEbPartyDestination", qEbLivraison.xEbPartyDestination().city.desc());

        // delivery by sale party
        orderMap.put("xEbPartySale", qEbLivraison.xEbPartySale().city);
        orderMapASC.put("xEbPartySale", qEbLivraison.xEbPartySale().city.asc());
        orderMapDESC.put("xEbPartySale", qEbLivraison.xEbPartySale().city.desc());

        // delivery by incoterm
        orderMap.put("xEcIncotermLibelle", qEbLivraison.xEcIncotermLibelle);
        orderMapASC.put("xEcIncotermLibelle", qEbLivraison.xEcIncotermLibelle.asc());
        orderMapDESC.put("xEcIncotermLibelle", qEbLivraison.xEcIncotermLibelle.desc());

        // delivery by creation Date
        orderMap.put("dateCreationSys", qEbLivraison.dateCreationSys);
        orderMapASC.put("dateCreationSys", qEbLivraison.dateCreationSys.asc());
        orderMapDESC.put("dateCreationSys", qEbLivraison.dateCreationSys.desc());

        // delivery by delivery status
        orderMap.put("dateOfGoodsAvailability", qEbLivraison.dateOfGoodsAvailability);
        orderMapASC.put("dateOfGoodsAvailability", qEbLivraison.dateOfGoodsAvailability.asc());
        orderMapDESC.put("dateOfGoodsAvailability", qEbLivraison.dateOfGoodsAvailability.desc());

        // delivery by delivery status
        orderMap.put("ebDelReference", qEbLivraison.ebDelReference);
        orderMapASC.put("ebDelReference", qEbLivraison.ebDelReference.asc());
        orderMapDESC.put("ebDelReference", qEbLivraison.ebDelReference.desc());

        orderMap.put("customerOrderReference", qEbLivraison.customerOrderReference);
        orderMapASC.put("customerOrderReference", qEbLivraison.customerOrderReference.asc());
        orderMapDESC.put("customerOrderReference", qEbLivraison.customerOrderReference.desc());
    }

    public SearchCriteriaDelivery() {
        ebDelLivraisonNum = null;
        ebDelReference = null;
        countryDestination = null;
        numOrderCustomer = null;
        listStatusDelivery = null;
        listRefDelivery = null;
        listOrderCustomerName = null;
        listNumOrderCustomer = null;
        listcustomFields = null;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    public JPAQuery applyCriteria(JPAQuery query, boolean applyOrder) throws GenericEnumException {
        JPAQuery<EbLivraison> resultQuery = query;
        BooleanBuilder w = new BooleanBuilder();

        Initialize(qEbLivraison);

        if (this.getSearchterm() != null) {
            String value = this.getSearchterm();
            Date dt = null;

            if (value != null && !value.isEmpty()) {
                value = value.trim().toLowerCase();

                if (Pattern.compile("([0-9]{2}/){2}[0-9]{4}").matcher(value).matches()) {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                    try {
                        dt = formatter.parse(value);
                    } catch (Exception e) {
                    }

                }


        //@formatter:off
				w
					.andAnyOf(
						qEbLivraison.xEbOwnerOfTheRequest().nom.containsIgnoreCase(value),
						qEbLivraison.xEbOwnerOfTheRequest().prenom.containsIgnoreCase(value),
						qEbLivraison.ebDelLivraisonNum.stringValue().contains(value),
						qEbLivraison.refTransport.containsIgnoreCase(value),
						qEbLivraison.ebDelReference.containsIgnoreCase(value),
						qEbLivraison.numOrderSAP.containsIgnoreCase(value),
						qEbLivraison.numOrderEDI.containsIgnoreCase(value),
						qEbLivraison.numOrderCustomer.containsIgnoreCase(value),
						qEbLivraison.campaignCode.containsIgnoreCase(value),
						qEbLivraison.campaignName.containsIgnoreCase(value),
						qEbLivraison.pickupSite.containsIgnoreCase(value),
						qEbLivraison.xEbPartyDestination().xEcCountry().libelle.containsIgnoreCase(value),
						qEbLivraison.xEbPartyDestination().company.stringValue().containsIgnoreCase(value),
						qEbLivraison.xEbPartyDestination().reference.stringValue().containsIgnoreCase(value),
						qEbLivraison.xEbPartyOrigin().xEcCountry().libelle.containsIgnoreCase(value),
						qEbLivraison.xEbPartyOrigin().company.stringValue().containsIgnoreCase(value),
						qEbLivraison.xEbPartyOrigin().reference.stringValue().containsIgnoreCase(value),
						qEbLivraison.customerOrderReference.containsIgnoreCase(value),
						qEbLivraisonLine.serialNumber.containsIgnoreCase(value),
						qEbLivraisonLine.partNumber.containsIgnoreCase(value),
						qEbLivraisonLine.itemNumber.containsIgnoreCase(value)
					);
				//@formatter:on

                if (dt != null) {
                    w
                        .or(
                            qEbLivraison.orderCreationDate
                                .stringValue().toLowerCase()
                                .contains((new SimpleDateFormat(Statiques.DATE_FORMAT_ORDER)).format(dt)));
                    w
                        .or(
                            qEbLivraison.requestedDeliveryDate
                                .stringValue().toLowerCase()
                                .contains((new SimpleDateFormat(Statiques.DATE_FORMAT_ORDER)).format(dt)));
                    w
                        .or(
                            qEbLivraison.requestedDeliveryDate
                                .stringValue().toLowerCase()
                                .contains((new SimpleDateFormat(Statiques.DATE_FORMAT_ORDER)).format(dt)));
                    w
                        .or(
                            qEbLivraison.pickDate
                                .stringValue().toLowerCase()
                                .contains((new SimpleDateFormat(Statiques.DATE_FORMAT_ORDER)).format(dt)));
                    w
                        .or(
                            qEbLivraison.endOfPackDate
                                .stringValue().toLowerCase()
                                .contains((new SimpleDateFormat(Statiques.DATE_FORMAT_ORDER)).format(dt)));
                }
                Optional<StatusDelivery> optionalStatus = StatusDelivery.getOptionalStatusByLibelle(value);

                if (optionalStatus.isPresent()) {
                    w.or(qEbLivraison.statusDelivery.eq(optionalStatus.get()));
                }

                resultQuery.where(w);
            }

        }

        if (getEbDelLivraisonNum() != null) {
            resultQuery.where(qEbLivraison.ebDelLivraisonNum.eq(getEbDelLivraisonNum()));
        }

        if (getEbDelOrderNum() != null) {
            query.leftJoin(qEbLivraison.xEbDelOrder(), qEbOrder);
            resultQuery.where(qEbOrder.ebDelOrderNum.eq(getEbDelOrderNum()));
        }

        if (BooleanUtils.isTrue(getExcludeCanceled())) {
            query.where(qEbLivraison.statusDelivery.ne(StatusDelivery.CANCELED));
        }

        if (getListOrigins() != null && !getListOrigins().isEmpty()) {
            query.leftJoin(qEbLivraison.xEbPartyOrigin(), qPartyOrigin);
            resultQuery.where(qPartyOrigin.xEcCountry().ecCountryNum.in(getListOrigins()));
        }

        if (getListPartyDestination() != null && !getListPartyDestination().isEmpty()) {
            query.leftJoin(qEbLivraison.xEbPartyDestination(), qPartyDest);
            resultQuery.where(qPartyDest.xEcCountry().ecCountryNum.in(getListPartyDestination()));
        }

        if (getListOwnerOfTheRequests() != null && !getListOwnerOfTheRequests().isEmpty()) {
            resultQuery.where(qEbLivraison.xEbOwnerOfTheRequest().ebUserNum.in(getListOwnerOfTheRequests()));
        }

        if (getCustomerCreationDate() != null) {
            w.andAnyOf((truncTimestamp(qEbLivraison.dateCreationSys)).eq(truncDate(getCustomerCreationDate())));
            resultQuery.where(w);
        }

        if (getDateOfGoodsAvailability() != null) {
            w.andAnyOf((truncDate(qEbLivraison.dateOfGoodsAvailability)).eq(truncDate(getDateOfGoodsAvailability())));
            resultQuery.where(w);
        }

        if (this.customerDeliveryDate != null) {
            w.andAnyOf((truncDate(qEbLivraison.customerDeliveryDate)).eq(truncDate(getCustomerDeliveryDate())));
            resultQuery.where(w);
        }

        if (getListRefDelivery() != null && !getListRefDelivery().isEmpty()) {
            w.andAnyOf(qEbLivraison.ebDelReference.in(getListRefDelivery()));
            resultQuery.where(w);
        }

        if (getListCustomerOrderReference() != null && !getListCustomerOrderReference().isEmpty()) {
            w.andAnyOf(qEbLivraison.customerOrderReference.in(getListCustomerOrderReference()));
            resultQuery.where(w);
        }

        if (getListIncoterm() != null && !getListIncoterm().isEmpty()) {
            w.andAnyOf(qEbLivraison.xEcIncotermLibelle.in(getListIncoterm()));
            resultQuery.where(w);
        }

		if (getListStatusDelivery() != null && !getListStatusDelivery().isEmpty()) {
			w.andAnyOf(qEbLivraison.statusDelivery.in(getListStatusDelivery().stream()
					.map(status -> StatusDelivery.getStatusByCode(status).get()).collect(Collectors.toList())));
			resultQuery.where(w);
		}

        if (this.customerOrderReference != null) {
            w.and(qEbLivraison.customerOrderReference.eq(this.customerOrderReference));
            resultQuery.where(w);
        }

        if (this.ebCompagnieNum != null) {
            w.and(qEbLivraison.xEbCompagnie().ebCompagnieNum.eq(this.ebCompagnieNum));
            resultQuery.where(w);
        }

        this.applyAdvancedSearchForCustomFields(w, qEbLivraison.customFields);
        resultQuery.where(w);

        if (MapUtils.isNotEmpty(mapCategoryFields)) {
            BooleanBuilder boolLabels = new BooleanBuilder();
            AtomicReference<BooleanBuilder> orBoolLabels = new AtomicReference<>();

            mapCategoryFields
                .entrySet().stream().filter(e -> CollectionUtils.isNotEmpty(e.getValue())).forEach(cat -> {
                    orBoolLabels.set(new BooleanBuilder());

                    cat.getValue().stream().forEach(label -> {
                        Long ebLabelNum = Long.valueOf(label);
                        orBoolLabels
                            .get().or(
                                new BooleanBuilder()
                                    .andAnyOf(
                                        qEbLivraison.listLabels.startsWith(ebLabelNum + ","),
                                        qEbLivraison.listLabels.contains("," + ebLabelNum + ","),
                                        qEbLivraison.listLabels.endsWith("," + ebLabelNum),
                                        qEbLivraison.listLabels.eq(ebLabelNum + "")));
                    });

                    if (orBoolLabels.get().hasValue()) boolLabels.and(orBoolLabels.get());
                });

            if (boolLabels.hasValue()) {
                w.andAnyOf(qEbLivraison.listLabels.isNotNull().andAnyOf(boolLabels));
                resultQuery.where(w);
            }

        }

        if (getListCustomFieldsValues() != null && !getListCustomFieldsValues().isEmpty()) {
            Predicate[] predicats = new Predicate[getListCustomFieldsValues().size()];

            for (int i = 0; i < getListCustomFieldsValues().size(); i++) {
                predicats[i] = qEbLivraison.listCustomFieldsValueFlat.contains(getListCustomFieldsValues().get(i));
            }

            w.andAnyOf(predicats);
            resultQuery.where(w);
        }

        if (this.getListcustomFields() != null && !this.getListcustomFields().isEmpty()) {
            w.and(qEbLivraison.customFields.in(this.getListcustomFields()));
            resultQuery.where(w);
        }

        if (this.pageNumber != null && this.size != null && !isCount && !this.extractData) {
            resultQuery.limit(this.size).offset(this.getPageNumber() * (this.getSize() != null ? this.getSize() : 1));
        }

        if (!this.isCount && applyOrder) {

            if (this.orderedColumn != null) {

                if (this.ascendant) {
                    resultQuery.orderBy(this.orderMapASC.get(this.orderedColumn));
                }
                else {
                    resultQuery.orderBy(this.orderMapDESC.get(this.orderedColumn));
                }

            }
            else {
                this.orderedColumn = "ebDelLivraisonNum";
                resultQuery.orderBy(this.orderMapDESC.get(this.orderedColumn));
            }

        }

        return resultQuery;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    public JPAQuery applyCriteriaAutoComplete(JPAQuery query) {
        QEbLivraison qEbLivraison = QEbLivraison.ebLivraison;
        JPAQuery<EbLivraison> resultQuery = query;

        Initialize(qEbLivraison);

        if (this.pageNumber != null && this.size != null && !isCount) {
            resultQuery.limit(this.size).offset(this.getPageNumber() * (this.getSize() != null ? this.getSize() : 1));
        }

        if (this.orderedColumn != null) {

            switch (this.orderedColumn) {
                // todo add to a list a get the corresponding querydsl
                // expression
                case "refArticle":
                    resultQuery.groupBy(qEbLivraisonLine.ebDelLivraisonLineNum);
                    break;

                case "batch":
                    resultQuery.groupBy(qEbLivraisonLine.ebDelLivraisonLineNum);
                    break;

                case "refClientDelivered":
                    resultQuery.groupBy(qEbLivraison.xEbPartyDestination().reference);
                    break;

                case "nameClientDelivered":
                    resultQuery.groupBy(qEbLivraison.xEbPartyDestination().company);
                    break;

                case "countryDestination":
                    resultQuery.groupBy(qEcCountryDest.libelle);
                    break;

                default:
                    resultQuery.groupBy(qEbLivraison.ebDelLivraisonNum);
                    break;
            }

            resultQuery.groupBy(qEbLivraison.ebDelLivraisonNum);
            resultQuery.orderBy(this.orderMapASC.get(this.orderedColumn));
        }
        else {
            this.orderedColumn = "ebDelLivraisonNum";
            resultQuery.groupBy(qEbLivraison.ebDelLivraisonNum);
            resultQuery.orderBy(this.orderMapDESC.get(this.orderedColumn));
        }

        return resultQuery;
    }

    public Long getEbDelLivraisonNum() {
        return ebDelLivraisonNum;
    }

    public void setEbDelLivraisonNum(Long ebDelLivraisonNum) {
        this.ebDelLivraisonNum = ebDelLivraisonNum;
    }

    public String getNumOrderCustomer() {
        return numOrderCustomer;
    }

    public void setNumOrderCustomer(String numOrderCustomer) {
        this.numOrderCustomer = numOrderCustomer;
    }

    public boolean isCount() {
        return isCount;
    }

    public void setCount(boolean isCount) {
        this.isCount = isCount;
    }

    public List<Integer> getListStatusDelivery() {
        return listStatusDelivery;
    }

    public void setListStatusDelivery(List<Integer> listDeliveryStatus) {
        this.listStatusDelivery = listDeliveryStatus;
    }

    public List<String> getListNumOrderCustomer() {
        return listNumOrderCustomer;
    }

    public void setListNumOrderCustomer(List<String> listNumOrderCustomer) {
        this.listNumOrderCustomer = listNumOrderCustomer;
    }

    public List<String> getListOrderCustomerName() {
        return listOrderCustomerName;
    }

    public void setListOrderCustomerName(List<String> listOrderCustomerName) {
        this.listOrderCustomerName = listOrderCustomerName;
    }

    public List<String> getListRefDelivery() {
        return listRefDelivery;
    }

    public void setListRefDelivery(List<String> listRefDelivery) {
        this.listRefDelivery = listRefDelivery;
    }

    public String getCountryDestination() {
        return countryDestination;
    }

    public void setCountryDestination(String countryDestination) {
        this.countryDestination = countryDestination;
    }

    public Map<String, OrderSpecifier<?>> getOrderMapASC() {
        return orderMapASC;
    }

    public void setOrderMapASC(Map<String, OrderSpecifier<?>> orderMapASC) {
        this.orderMapASC = orderMapASC;
    }

    public Map<String, OrderSpecifier<?>> getOrderMapDESC() {
        return orderMapDESC;
    }

    public void setOrderMapDESC(Map<String, OrderSpecifier<?>> orderMapDESC) {
        this.orderMapDESC = orderMapDESC;
    }

    public QEbLivraison getqEbLivraison() {
        return qEbLivraison;
    }

    public void setqEbLivraison(QEbLivraison qEbLivraison) {
        this.qEbLivraison = qEbLivraison;
    }

    public String getEbDelReference() {
        return ebDelReference;
    }

    public void setEbDelReference(String ebDelReference) {
        this.ebDelReference = ebDelReference;
    }

    public Date getDateOfGoodsAvailability() {
        return dateOfGoodsAvailability;
    }

    public void setDateOfGoodsAvailability(Date dateOfGoodsAvailability) {
        this.dateOfGoodsAvailability = dateOfGoodsAvailability;
    }

    public List<Integer> getListPartyOrigin() {
        return listPartyOrigin;
    }

    public void setListPartyOrigin(List<Integer> listPartyOrigin) {
        this.listPartyOrigin = listPartyOrigin;
    }

    public QEbParty getqPartyDest() {
        return qPartyDest;
    }

    public void setqPartyDest(QEbParty qPartyDest) {
        this.qPartyDest = qPartyDest;
    }

    public QEbParty getqPartyOrigin() {
        return qPartyOrigin;
    }

    public void setqPartyOrigin(QEbParty qPartyOrigin) {
        this.qPartyOrigin = qPartyOrigin;
    }

    public Timestamp getCustomerCreationDate() {
        return customerCreationDate;
    }

    public void setCustomerCreationDate(Timestamp customerCreationDate) {
        this.customerCreationDate = customerCreationDate;
    }

    public List<String> getListcustomFields() {
        return listcustomFields;
    }

    public void setListcustomFields(List<String> listcustomFields) {
        this.listcustomFields = listcustomFields;
    }

    public Long getEbDelOrderNum() {
        return ebDelOrderNum;
    }

    public void setEbDelOrderNum(Long ebDelOrderNum) {
        this.ebDelOrderNum = ebDelOrderNum;
    }

    public Boolean getExcludeCanceled() {
        return excludeCanceled;
    }

    public void setExcludeCanceled(Boolean excludeCanceled) {
        this.excludeCanceled = excludeCanceled;
    }

    public List<String> getListCustomerOrderReference() {
        return listCustomerOrderReference;
    }

    public void setListCustomerOrderReference(List<String> listCustomerOrderReference) {
        this.listCustomerOrderReference = listCustomerOrderReference;
    }

    public String getCustomerOrderReference() {
        return customerOrderReference;
    }

    public void setCustomerOrderReference(String customerOrderReference) {
        this.customerOrderReference = customerOrderReference;
    }

    public Date getCustomerDeliveryDate() {
        return customerDeliveryDate;
    }

    public void setCustomerDeliveryDate(Date customerDeliveryDate) {
        this.customerDeliveryDate = customerDeliveryDate;
    }
}
