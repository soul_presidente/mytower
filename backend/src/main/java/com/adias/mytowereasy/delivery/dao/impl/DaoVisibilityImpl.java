package com.adias.mytowereasy.delivery.dao.impl;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.delivery.dao.DaoVisibility;
import com.adias.mytowereasy.delivery.model.QEbLivraison;
import com.adias.mytowereasy.delivery.model.QEbOrder;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbLabelRepository;
import com.adias.mytowereasy.service.ConnectedUserService;


@Service
public class DaoVisibilityImpl implements DaoVisibility {
    QEbOrder qEbOrder = new QEbOrder("ebOrder");
    QEbLivraison qEbLivraison = new QEbLivraison("ebLivraison");
    @Autowired
    private ConnectedUserService connectedUserService;

    @Autowired
    private EbLabelRepository ebLabelRepository;

    @Override
    public JPAQuery addVisibilityToOrder(JPAQuery query, SearchCriteriaOrder criteria, EbUser currentUser) {

        currentUser = connectedUserService.getCurrentUserWithCategories(criteria);

        if (currentUser != null) {
            EbCompagnie compagnie = currentUser.getEbCompagnie();
            EbEtablissement etablissement = currentUser.getEbEtablissement();

            if (compagnie != null) {
                query.where(qEbOrder.xEbCompagnie().ebCompagnieNum.eq(compagnie.getEbCompagnieNum()));
            }

            if (etablissement != null && !currentUser.isSuperAdmin()) {
                BooleanBuilder etabAndLabelCondition = new BooleanBuilder();
                etabAndLabelCondition
                    .and(qEbOrder.xEbEtablissement().ebEtablissementNum.eq(etablissement.getEbEtablissementNum()))
                    .and(addVisibilityByCategorieLabels(currentUser, qEbOrder.listLabels));

                query.where(etabAndLabelCondition);
            }

        }

        // recherche par flags
        if (criteria.getListFlag() != null && !criteria.getListFlag().trim().isEmpty()) {
            List<String> listFlagId = Arrays.asList(criteria.getListFlag().trim().split(","));
            Predicate[] predicats = new Predicate[listFlagId.size() * 4];
            int j = 0;

            for (int i = 0; i < listFlagId.size(); i++) {
                predicats[i + j] = qEbOrder.listFlag
                    .contains(listFlagId.get(i)).and(qEbOrder.listFlag.trim().length().eq(listFlagId.get(i).length()));
                predicats[i + j + 1] = qEbOrder.listFlag.endsWith("," + listFlagId.get(i));
                predicats[i + j + 2] = qEbOrder.listFlag.startsWith(listFlagId.get(i) + ",");
                predicats[i + j + 3] = qEbOrder.listFlag.contains("," + listFlagId.get(i) + ",");
                j += 3;
            }

            BooleanBuilder where = new BooleanBuilder();
            where.andAnyOf(predicats);
            query.from(qEbOrder).where(where);
        }

        return query;
    }

    @Override
    public JPAQuery addVisibilityToDelivery(JPAQuery query, SearchCriteriaDelivery criteria, EbUser currentUser) {

        if (currentUser != null) {
            EbCompagnie compagnie = currentUser.getEbCompagnie();
            EbEtablissement etablissement = currentUser.getEbEtablissement();

            if (compagnie != null) {
                query.where(qEbLivraison.xEbCompagnie().ebCompagnieNum.eq(compagnie.getEbCompagnieNum()));
            }

            if (etablissement != null && !currentUser.isSuperAdmin()) {
                query
                    .where(
                        qEbLivraison.xEbEtablissement().ebEtablissementNum.eq(etablissement.getEbEtablissementNum()));
            }

        }

        if (!currentUser.isAdmin() && !currentUser.isSuperAdmin()) {
            BooleanBuilder categBb = new BooleanBuilder();
            categBb.and(addVisibilityByCategorieLabels(currentUser, qEbLivraison.listLabels));
            query.where(categBb);
        }

        return query;
    }

    private BooleanBuilder addVisibilityByCategorieLabels(EbUser currentUser, StringExpression fieldColumn) {
        BooleanBuilder boolLabels = new BooleanBuilder();

        if (CollectionUtils.isNotEmpty(currentUser.getListCategories())) {

            for (EbCategorie categorie: currentUser.getListCategories()) {
                BooleanBuilder orBoolLabels = new BooleanBuilder();
                Set<EbLabel> categorieLabels = categorie.getLabels();

                if (categorie.getIsAll()) {
                    categorieLabels = ebLabelRepository.findLabelsByCategorieId(categorie.getEbCategorieNum());
                }

                List<String> labels = categorieLabels
                    .stream().map(it -> it.getEbLabelNum().toString()).collect(Collectors.toList());

                for (String label: labels) {
                    orBoolLabels.or(fieldColumn.contains(label));
                }

                boolLabels.and(orBoolLabels);
            }

        }

        return boolLabels;
    }

    private Set<String> addCategorieLabelToVisibility(List<EbCategorie> listCategories) {
        Set<String> labels = new HashSet<String>();
        ObjectMapper mapper = new ObjectMapper();

        List<EbCategorie> categories = mapper.convertValue(listCategories, new TypeReference<List<EbCategorie>>() {
        });

        if (categories != null) {

            for (EbCategorie categorie: categories) {

                if (categorie.getLabels() != null && categorie.getLabels().size() > 0) {

                    for (EbLabel label: categorie.getLabels()) {
                        labels.add(String.valueOf(label.getEbLabelNum()));
                    }

                }

            }

        }

        return labels;
    }
}
