package com.adias.mytowereasy.delivery.model.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.adias.mytowereasy.api.service.CategoryApiService;
import com.adias.mytowereasy.api.service.CustomFieldsApiService;
import com.adias.mytowereasy.api.service.TransportApiService;
import com.adias.mytowereasy.api.wso.AddressWSO;
import com.adias.mytowereasy.api.wso.DeliveryLineWSO;
import com.adias.mytowereasy.api.wso.DeliveryWSO;
import com.adias.mytowereasy.delivery.dao.impl.DaoDeliveryImpl;
import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.service.impl.OrderServiceImpl;
import com.adias.mytowereasy.dto.mapper.DeliveryMapper;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbIncotermRepository;
import com.adias.mytowereasy.repository.EbTypeUnitRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.repository.EcCountryRepository;
import com.adias.mytowereasy.util.Constants;
import com.adias.mytowereasy.util.JsonUtils;
import com.adias.mytowereasy.util.enums.GenericEnumUtils;


@Component
public class DeliveryWSOMapper {

    @Autowired
    EbUserRepository ebUserRepository;

    @Autowired
    TransportApiService transportApiService;

    @Autowired
    CustomFieldsApiService customFieldsApiService;

    @Autowired
    CategoryApiService categoryApiService;

    @Autowired
    EbIncotermRepository ebIncotermRepository;

    @Autowired
    EcCountryRepository ecCountryRepository;

    @Autowired
    EbTypeUnitRepository ebTypeUnitRepository;

    @Autowired
    DeliveryMapper deliveryMapper;

    @Autowired
    DaoDeliveryImpl daoDelivery;

    @Autowired
    OrderServiceImpl orderService;

    public DeliveryWSO getWSOFromEntity(EbLivraison entity) {
        DeliveryWSO wso = deliveryMapper.ebLivraisonToDeliveyWSO(entity);

        if (entity.getxEbPartyOrigin() != null) {
            AddressWSO party = transportApiService.convertEbPartyToAddresseWSO(entity.getxEbPartyOrigin());
            wso.setFrom(party);
        }

        if (entity.getxEbPartyDestination() != null) {
            AddressWSO party = transportApiService.convertEbPartyToAddresseWSO(entity.getxEbPartyDestination());
            wso.setTo(party);
        }

        if (entity.getxEbPartySale() != null) {
            AddressWSO party = transportApiService.convertEbPartyToAddresseWSO(entity.getxEbPartySale());
            wso.setSoldTo(party);
        }

        // Lines
        if (!CollectionUtils.isEmpty(entity.getxEbDelLivraisonLine())) {
            wso.setDeliveryLines(new ArrayList<>());

            for (EbLivraisonLine deliveryLine: entity.getxEbDelLivraisonLine()) {
                DeliveryLineWSO deliveryLineWSO = deliveryMapper.ebLivraisonLineToDeliveryLineWSO(deliveryLine);
                wso.getDeliveryLines().add(deliveryLineWSO);
            }

        }
        return wso;
    }

    public EbLivraison fillEntityFromWSO(EbLivraison entity, DeliveryWSO wso) {
        EbUser requestOwner = ebUserRepository.findOneByEmailIgnoreCase(wso.getRequestOwnerEmail());
        if (requestOwner
            == null) throw new MyTowerException("User with email " + wso.getRequestOwnerEmail() + " not found");
        entity.setxEbOwnerOfTheRequest(requestOwner);

        entity.setxEbCompagnie(requestOwner.getEbCompagnie());
        entity.setxEbEtablissement(requestOwner.getEbEtablissement());
        Long deliveryId = wso.getDeliveryId();
        if (Objects.isNull(deliveryId)) {
            deliveryId = daoDelivery.nextValDelivery();
        }

        entity.setEbDelLivraisonNum(deliveryId);
        entity.setDeliveryCreationDate(wso.getDeliveryCreationDate());
        entity.setRefOrder(wso.getRefOrder());
        entity.setComplementaryInformations(wso.getComplementaryInformations());
        entity.setNumOrderEDI(wso.getNumEdi());
        entity.setCustomerOrderReference(wso.getCustomerOrderReference());
        entity.setCustomerDeliveryDate(wso.getCustomerDeliveryDate());
        entity
            .setEbDelReference(
                orderService
                    .generateReference(
                        requestOwner.getEbCompagnie().getCode(),
                        deliveryId,
                        Constants.refDeliveryPrefix));

        if (wso.getFrom() == null) {
            throw new MyTowerException("Party Origin is null");
        }

        if (entity.getxEbPartyOrigin() == null) {
            entity.setxEbPartyOrigin(new EbParty());
        }

        entity
            .setxEbPartyOrigin(
                transportApiService.fillEbPartyFromAddressWSO(entity.getxEbPartyOrigin(), wso.getFrom()));

        if (wso.getTo() == null) {
            throw new MyTowerException("Party Destination in null");
        }

        if (entity.getxEbPartyDestination() == null) {
            entity.setxEbPartyDestination(new EbParty());
        }

        entity
            .setxEbPartyDestination(
                transportApiService.fillEbPartyFromAddressWSO(entity.getxEbPartyDestination(), wso.getTo()));

        if (wso.getSoldTo() != null) {

            if (entity.getxEbPartySale() == null) {
                entity.setxEbPartySale(new EbParty());
            }

            entity
                .setxEbPartySale(
                    transportApiService.fillEbPartyFromAddressWSO(entity.getxEbPartySale(), wso.getSoldTo()));
        }

        // Custom fields
        if (wso.getCustomFieldList() != null) {
            List<CustomFields> listCustomField = customFieldsApiService
                .generateCustomFieldsFromWSO(wso.getCustomFieldList(), entity.getxEbCompagnie().getEbCompagnieNum());
            entity.setCustomFields(JsonUtils.serializeToString(listCustomField, null));
            entity.setListCustomsFields(listCustomField);
        }

        // Categories
        if (wso.getCategoryList() != null) {
            List<EbCategorie> listCategorieFinal = categoryApiService
                .generateCategoriesFromWSO(wso.getCategoryList(), entity.getxEbCompagnie().getEbCompagnieNum());
            entity.setListCategories(listCategorieFinal);
        }

        // delivery Lines
        entity.setxEbDelLivraisonLine(new ArrayList<>());

        if (wso.getDeliveryLines() != null) {

            for (DeliveryLineWSO deliveryLineWSO: wso.getDeliveryLines()) {
                EbLivraisonLine deliveryLineEntity = fillLineEntityFromWSO(
                    deliveryLineWSO,
                    entity.getxEbCompagnie() != null ? entity.getxEbCompagnie().getEbCompagnieNum() : null);
                entity.getxEbDelLivraisonLine().add(deliveryLineEntity);
            }

        }

        if (wso.getIncoterm() != null) {
            EbIncoterm ebIncoterm = ebIncotermRepository
                .findFirstByLibelleAndXEbCompagnie_ebCompagnieNum(
                    wso.getIncoterm(),
                    entity.getxEbCompagnie().getEbCompagnieNum());

            if (ebIncoterm == null) throw new MyTowerException("Incoterm " + wso.getIncoterm() + " not found");
            else {
                entity.setxEbIncotermNum(ebIncoterm.getEbIncotermNum());
                entity.setxEcIncotermLibelle(ebIncoterm.getLibelle());
            }

        }

        StatusDelivery statusDelivery = (StatusDelivery) GenericEnumUtils
            .getByKey(StatusDelivery.class, wso.getDeliveryStatus())
            .orElseThrow(
                () -> new MyTowerException("Delivery Status with key " + wso.getDeliveryStatus() + " not found"));
        entity.setStatusDelivery(statusDelivery);

        return entity;
    }

    private EbLivraisonLine
        fillLineEntityFromWSO(DeliveryLineWSO wso, Integer orderCompagnieNum) {
        EbLivraisonLine entity = deliveryMapper.deliveryLineWSOToEbLivraisonLine(wso);

        if (wso.getCustomFieldList() != null) {
            List<CustomFields> listCustomField = customFieldsApiService
                .generateCustomFieldsFromWSO(wso.getCustomFieldList(), orderCompagnieNum);
            entity.setCustomFields(JsonUtils.serializeToString(listCustomField, null));
            entity.setListCustomsFields(listCustomField);
        }

        if (wso.getCategoryList() != null) {
            List<EbCategorie> listCategorieFinal = categoryApiService
                .generateCategoriesFromWSO(wso.getCategoryList(), orderCompagnieNum);
            entity.setListCategories(listCategorieFinal);
        }

        if (wso.getUnitType() != null) {
            EbTypeUnit typeUnit = ebTypeUnitRepository.findFirstByCode(wso.getUnitType());
            entity.setxEbtypeOfUnit(typeUnit);
        }

        if (wso.getOriginCountryCode() != null) {
            EcCountry country = ecCountryRepository.findFirstByCode(wso.getOriginCountryCode());
            entity.setxEcCountryOrigin(country);
        }

        return entity;
    }
}
