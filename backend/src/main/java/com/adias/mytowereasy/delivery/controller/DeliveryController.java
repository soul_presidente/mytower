package com.adias.mytowereasy.delivery.controller;

import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.controller.PricingController;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.repository.EbLivraisonLineRepository;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.service.DeliveryLineService;
import com.adias.mytowereasy.delivery.service.DeliveryService;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.dto.EbLivraisonDto;
import com.adias.mytowereasy.dto.EbLivraisonLineDto;
import com.adias.mytowereasy.dto.EbOrderDto;
import com.adias.mytowereasy.dto.mapper.DeliveryMapper;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.service.CustomFieldService;
import com.adias.mytowereasy.trpl.service.ZoneService;


@RestController
@RequestMapping(value = "api/delivery-overview")
@CrossOrigin("*")
public class DeliveryController {
    @Autowired
    EbLivraisonRepository ebLivraisonRepository;

    @Autowired
    ZoneService zoneService;

    @Autowired
    DeliveryService deliveryService;

    @Autowired
    DeliveryLineService deliveryLineService;

    @Autowired
    DeliveryMapper mapper;

    @Autowired
    PricingController pricingController;

    @Autowired
    EbLivraisonLineRepository ebLivraisonLineRepository;

    @Autowired
    CustomFieldService customFieldService;

    @RequestMapping(value = "list", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody HashMap<String, Object>
        getListDeliveries(@RequestBody SearchCriteriaDelivery criteria) throws JsonProcessingException, Exception {

        if (StringUtils.isNotBlank(criteria.getSearchInput())) {
            criteria
                .setSearchInput(criteria.getSearchInput(), customFieldService.getCustomFields(criteria.getEbUserNum()));
        }

        List<EbLivraison> resultDelivery = deliveryService.getListDeliveries(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", deliveryService.getDeliveryCount(criteria));
        result.put("data", mapper.ebLivraisonsToEbLivraisonsDto(resultDelivery));
        return result;
    }

    @PostMapping(value = "/list-count")
    public Integer
        getListCount(@RequestBody SearchCriteriaDelivery criteria) throws JsonProcessingException, Exception {

        if (StringUtils.isNotBlank(criteria.getSearchInput())) {
            criteria
                .setSearchInput(criteria.getSearchInput(), customFieldService.getCustomFields(criteria.getEbUserNum()));
        }

        return deliveryService.getDeliveryCount(criteria);
    }

    @RequestMapping(value = "get-delivery", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody EbLivraisonDto getDelivery(@RequestBody(required = false) SearchCriteriaDelivery criteria) {
        EbLivraison ebLivraison = deliveryService.getDelivery(criteria);
        EbLivraisonDto ebLivraisonDto = mapper.ebLivraisonToEbLivraisonDto(ebLivraison);
        EbOrderDto ebOrderDto = mapper.ebOrderToEbOrderDto(ebLivraison.getxEbDelOrder());
        ebOrderDto.setOrderLines(mapper.ebOrderLinesToEbOrderLinesDto(ebLivraison.getxEbDelOrder().getOrderLines()));
        ebLivraisonDto.setxEbDelOrder(ebOrderDto);
        return ebLivraisonDto;
    }

    @RequestMapping(value = "get-delivery-lines", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody List<EbLivraisonLineDto>
        getDeliveryLines(@RequestBody(required = false) Long ebDelLivraisonNum) {
        List<EbLivraisonLine> result = deliveryLineService.getDeliveryLines(ebDelLivraisonNum);

        return mapper.ebLivraisonLinesToEbLivraisonLinesDto(result);
    }

    @RequestMapping(value = "save-delivery", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody EbLivraison saveDelivery(@RequestBody EbLivraisonDto ebLivraisonDto) {
        EbLivraison delivery = new EbLivraison();
        delivery = mapper.ebDeliveryDto(ebLivraisonDto);

        delivery
            .getxEbDelLivraisonLine().removeIf(
                element -> element.getxEbOrderLigne().getIsSelected() == null
                    || element.getxEbOrderLigne().getIsSelected() == false);
        return deliveryService.saveLivraison(delivery);
    }

    @PostMapping(value = "update-delivery")
    public @ResponseBody EbLivraison updateDelivery(@RequestBody EbLivraisonDto ebLivraisonDto) {
        EbLivraison delivery = new EbLivraison();
        delivery = mapper.ebDeliveryDto(ebLivraisonDto);

        return deliveryService.updateLivraison(delivery);
    }

    @PostMapping(value = "delivery-dashboard-inline-update")
    public EbLivraisonLine inlineDashboardUpdate(@RequestBody EbLivraisonLine sent) {
        EbLivraisonLine entity = deliveryLineService.inlineDeliveryDashboardUpdate(sent);
        return entity;
    }

    @PostMapping(value = "pre-consolidate-delivery")
    public @ResponseBody EbDemande preConsolidateDelivery(@RequestBody List<Long> listDeliveryForConsolidateNum) {
        EbDemande ebDemande = deliveryService.preConsolidateDelivery(listDeliveryForConsolidateNum);
        return ebDemande;
    }

    @PostMapping(value = "delete-delivery")
    public void deleteDelivery(@RequestBody List<Long> listSelectedDelivery) {
        deliveryService.deleteDelivery(listSelectedDelivery);
    }

    @PostMapping(value = "delete-one-delivery")
    public void deleteOneDelivery(@RequestBody Long deliveryToDeleteNum) {
        deliveryService.deleteOneDelivery(deliveryToDeleteNum);
    }

    @PostMapping(value = "find-orderLine-by-ebLivraisonLineNum")
    public EbOrderLine findOrderLineByDeliveryLine(@RequestBody Long ebLivraisonLineNum) {
        if (ebLivraisonLineNum != null) return ebLivraisonLineRepository
            .findOrderLineByLivraisonLine(ebLivraisonLineNum);
        return null;
    }

    @PostMapping(value = "delete-one-delivery-line")
    public void deleteOneDeliveryLine(@RequestBody Long deliveryLineToDeleteNum) {
        deliveryService.deleteOnDeliveryLine(deliveryLineToDeleteNum);
    }

    @RequestMapping(
        value = "get-delivery-lines-for-genericTable",
        method = RequestMethod.POST,
        produces = "application/json")
    public @ResponseBody HashMap<String, Object>
        getDeliveryLinesForGenericTable(@RequestBody(required = false) SearchCriteriaDelivery criteriaDelivery) {
        List<EbLivraisonLine> resultDeliveriesLines = deliveryLineService
            .getDeliveryLinesForGenericTable(criteriaDelivery);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", deliveryLineService.getDeliveryLineCountForGenericTable(criteriaDelivery));
        result.put("data", mapper.ebLivraisonLinesToEbLivraisonLinesDto(resultDeliveriesLines));

        return result;
    }

    @RequestMapping(value = "/cancel-delivery", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody EbLivraisonDto cancelDelivery(@RequestBody Long ebDelLivraisonNum) {
        EbLivraison ebLivraison = deliveryService.cancelDelivery(ebDelLivraisonNum);
        EbLivraisonDto ebLivraisonDto = mapper.ebLivraisonToEbLivraisonDto(ebLivraison);
        return ebLivraisonDto;
    }

    @PostMapping(value = "confirm-delivery", produces = "application/json")
    public @ResponseBody EbLivraisonDto confirmDelivery(@RequestBody Long ebDelLivraisonNum) {
        EbLivraison ebLivraison = deliveryService.confirmDelivery(ebDelLivraisonNum);
        EbLivraisonDto ebLivraisonDto = mapper.ebLivraisonToEbLivraisonDto(ebLivraison);
        return ebLivraisonDto;
    }
}
