package com.adias.mytowereasy.delivery.dao;

import java.util.List;

import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.model.EbUser;


public interface DaoDeliveryLine {
    public List<EbLivraisonLine> getDeliveryLines(Long ebLivraisonLineNum, EbUser currentUser);

    public List<EbLivraisonLine> getDeliveryLinesForExport(SearchCriteriaDelivery criteria, EbUser currentUser);

    public List<EbLivraisonLine>
        getDeliveryLinesForGenericTable(SearchCriteriaDelivery criteriaDelivery, EbUser currentUser);

    public Long getDeliveryLineCountForGenericTable(SearchCriteriaDelivery criteriaDelivery, EbUser currentUser);
}
