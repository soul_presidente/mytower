package com.adias.mytowereasy.delivery.dao.impl;

import java.math.BigInteger;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.delivery.dao.DaoOrder;
import com.adias.mytowereasy.delivery.dao.DaoOrderLine;
import com.adias.mytowereasy.delivery.dao.DaoVisibility;
import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.model.*;
import com.adias.mytowereasy.delivery.repository.EbLivraisonLineRepository;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.repository.EbOrderLineRepository;
import com.adias.mytowereasy.delivery.repository.EbOrderRepository;
import com.adias.mytowereasy.delivery.service.impl.OrderServiceImpl;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.dto.EbOrderLinePlanificationDTO;
import com.adias.mytowereasy.dto.EbOrderPlanificationDTO;
import com.adias.mytowereasy.dto.mapper.DeliveryMapper;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbCategorieRepository;
import com.adias.mytowereasy.repository.EbPartyRepository;
import com.adias.mytowereasy.service.FlagService;
import com.adias.mytowereasy.util.Constants;
import com.adias.mytowereasy.util.JsonUtils;


@Component
@Transactional
public class DaoOrderImpl implements DaoOrder {
    @Autowired
    EbLivraisonLineRepository ebLivraisonLineRepository;

    @PersistenceContext
    EntityManager em;

    @Autowired
    EbOrderRepository ebOrderRepository;

    @Autowired
    EbPartyRepository ebPartyRepository;

    @Autowired
    EbOrderLineRepository ebOrderLineRepository;

    @Autowired
    EbCategorieRepository ebCategorieRepository;

    @Autowired
    DaoOrderLine daoOrderLine;

    @Autowired
    DaoVisibility daoVisibility;

    @Autowired
    FlagService flagService;

    @Autowired
    EbLivraisonRepository ebLivraisonRepository;
    @Autowired
    DeliveryMapper mapper;

    @Autowired
    DaoDeliveryImpl daoDelivery;

    @Autowired
    OrderServiceImpl orderService;

    QEbOrder qEbOrder = new QEbOrder("ebOrder");
    QEbLivraison qEbLivraison = new QEbLivraison("ebLivraison");
    QEbOrderLine qEbOrderLine = new QEbOrderLine("ebOrderLine");
    QEcCountry qEcCountry = new QEcCountry("ecCountry");

    QEbParty qPartyDest = new QEbParty("partyDest");
    QEbParty qPartyOrigin = new QEbParty("partyOrigin");
    QEbParty qPartySale = new QEbParty("partySale");
    QEbParty qPartyVendor = new QEbParty("partVendor");
    QEbParty qPartyIssuer = new QEbParty("partyIssuer");

    QEcCountry qCountryOrigin = new QEcCountry("qCountryOrigin");
    QEcCountry qCountryDest = new QEcCountry("qCountryDest");
    QEcCountry qCountrySale = new QEcCountry("qCountrySale");
    QEcCountry qCountryVendor = new QEcCountry("qCountryVendor");
    QEcCountry qCountryIssuer = new QEcCountry("qCountryIssuer");
    QEbUser qxEbOwnerOfTheRequest = new QEbUser("qxEbOwnerOfTheRequest");

    QEbEtablissement qEbEtablissement = new QEbEtablissement("ebEtablissement");

    private Map<String, Path<?>> autoCompleteFields = new HashMap<String, Path<?>>();
    private StringExpression stringOrderWithExpression = qEbOrder.customerOrderReference;
    Path<String> stringOrderAlias = ExpressionUtils.path(String.class, "stringOrderExpression");

    public DaoOrderImpl() {
        this.initialize();
    }

    private void initialize() {
        autoCompleteFields.put("crossDock", qEbOrder.crossDock);
        autoCompleteFields.put("refArticle", qEbOrderLine.refArticle);
        autoCompleteFields.put("refDelivery", qEbLivraison.refDelivery);
        autoCompleteFields.put("ebOrderNum", qEbOrder.ebDelOrderNum);
        autoCompleteFields.put("numOrderSAP", qEbOrder.numOrderSAP);
        autoCompleteFields.put("numOrderEDI", qEbOrder.numOrderEDI);
        autoCompleteFields.put("numOrderCustomer", qEbOrder.numOrderCustomer);
        autoCompleteFields.put("campaignCode", qEbOrder.campaignCode);
        autoCompleteFields.put("campaignName", qEbOrder.campaignName);
        autoCompleteFields.put("orderType", qEbOrder.orderType);
        autoCompleteFields.put("creationDate", qEbOrder.customerCreationDate);
        autoCompleteFields.put("deliveryDate", qEbOrder.deliveryDate);
        autoCompleteFields.put("orderStatus", qEbOrder.orderStatus);
        autoCompleteFields.put("orderCustomerName", qEbOrder.orderCustomerName);
        autoCompleteFields.put("orderCustomerCode", qEbOrder.orderCustomerCode);
        autoCompleteFields.put("nameClientDelivered", qEbOrder.xEbPartyDestination().company);
        autoCompleteFields.put("countryClientDelivered", qEbOrder.xEbPartyDestination().xEcCountry().libelle);
        autoCompleteFields.put("refClientDelivered", qEbOrder.xEbPartyDestination().reference);
        autoCompleteFields.put("customerOrderReference", qEbOrder.customerOrderReference);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EbOrder> getListOrders(SearchCriteriaOrder criteria, EbUser currentUser) {
        List<EbOrder> result = null;

        try {
            JPAQuery<EbOrder> query = new JPAQuery<EbOrder>(em);
            String fieldType = null;

            if (criteria != null) {
                query.distinct();

                if (criteria.getOrderedColumn() != null) {
                    String columnName = criteria.getOrderedColumn();
                    StringExpression fieldColumn = null;
                    NumberExpression indexOfCategExpr = null;

                    if (columnName != null && columnName.matches("^field[0-9]+$")) {
                        fieldType = "field";
                        fieldColumn = qEbOrder.listCustomFieldsFlat;
                        indexOfCategExpr = fieldColumn.indexOf(columnName + "\":").add(columnName.length() + 2 + 2);
                    }

                    if (fieldType != null && fieldColumn != null) {
                        StringExpression restStringExpr = fieldColumn.substring(indexOfCategExpr);
                        NumberExpression endIndexExpr = restStringExpr.indexOf("\"");
                        stringOrderWithExpression = new CaseBuilder()
                            .when(
                                new BooleanBuilder()
                                    .orAllOf(fieldColumn.isNotNull(), fieldColumn.indexOf(columnName).goe(0)))
                            .then(
                                fieldColumn
                                    .substring(
                                        indexOfCategExpr.subtract(1), // diminuer
                                                                      // 1
                                        endIndexExpr.add(indexOfCategExpr) // rajouter
                                                                           // l'expr
                                                                           // de
                                                                           // gauche
                                                                           // 'a'
                                    )).otherwise(Expressions.nullExpression());

                        stringOrderWithExpression = new CaseBuilder()
                            .when(
                                new BooleanBuilder()
                                    .orAllOf(
                                        stringOrderWithExpression.isNotNull(),
                                        stringOrderWithExpression.isNotEmpty()))
                            .then(stringOrderWithExpression).otherwise(Expressions.nullExpression());
                    }

                }

            }

            query
                .select(
                    QEbOrder
                        .create(
                            qEbOrder.ebDelOrderNum,
                            qEbOrder.numOrderSAP,
                            qEbOrder.numOrderEDI,
                            qEbOrder.orderCustomerCode,
                            qEbOrder.orderType,
                            qEbOrder.campaignCode,
                            qEbOrder.campaignName,
                            qEbOrder.numOrderCustomer,
                            qEbOrder.orderCustomerName,
                            qEbOrder.shipTo,
                            qEbOrder.purchaseOrderType,
                            qEbOrder.customerCreationDate,
                            qEbOrder.deliveryDate,
                            qEbOrder.orderStatus,
                            qEbOrder.crossDock,

                            qPartyOrigin.ebPartyNum,
                            qPartyOrigin.company,
                            qPartyOrigin.adresse,
                            qPartyOrigin.city,
                            qPartyOrigin.xEcCountry().ecCountryNum,
                            qPartyOrigin.xEcCountry().libelle,

                            qPartyDest.ebPartyNum,
                            qPartyDest.company,
                            qPartyDest.adresse,
                            qPartyDest.city,
                            qPartyDest.xEcCountry().ecCountryNum,
                            qPartyDest.xEcCountry().libelle,

                            qPartySale.ebPartyNum,
                            qPartySale.company,
                            qPartySale.adresse,
                            qPartySale.city,
                            qCountrySale.ecCountryNum,
                            qCountrySale.libelle,

                            qEbEtablissement.ebEtablissementNum,
                            qEbEtablissement.nom,
                            qEbOrder.customFields,
                            qEbOrder.listCategories,
                            qEbOrder.targetDate,
                            qEbOrder.orderDate,

                            qPartyVendor.ebPartyNum,
                            qPartyVendor.company,
                            qPartyVendor.adresse,
                            qPartyVendor.city,
                            qCountryVendor.ecCountryNum,
                            qCountryVendor.libelle,

                            qPartyIssuer.ebPartyNum,
                            qPartyIssuer.company,
                            qPartyIssuer.adresse,
                            qPartyIssuer.city,
                            qCountryIssuer.ecCountryNum,
                            qCountryIssuer.libelle,
                            qEbOrder.listFlag,
                            qEbOrder.priority,
                            qEbOrder.customerOrderReference,
                            qEbOrder.dateOfArrival,
                            qEbOrder.xEbIncotermNum,
                            qEbOrder.xEcIncotermLibelle,
                            qEbOrder.complementaryInformations,
                            stringOrderWithExpression.as("stringOrderExpression"),
                            qEbOrder.reference,
                            qEbOrder.totalQuantityOrdered,
                            qEbOrder.totalQuantityPlannable,
                            qEbOrder.totalQuantityPlanned,
                            qEbOrder.totalQuantityPending,
                            qEbOrder.totalQuantityConfirmed,
                            qEbOrder.totalQuantityShipped,
                            qEbOrder.totalQuantityDelivered,
                            qEbOrder.totalQuantityCanceled));

            if (fieldType != null) {
                if (criteria.getAscendant()) query.orderBy(new OrderSpecifier(Order.ASC, stringOrderAlias).nullsLast());
                else query.orderBy(new OrderSpecifier(Order.DESC, stringOrderAlias).nullsLast());
            }

            criteria.setQEbOrder(qEbOrder);
            criteria.setQEbLivraison(qEbLivraison);
            criteria.setQEbOrderLine(qEbOrderLine);
            criteria.setQEcCountry(qEcCountry);
            criteria.setQPartyDest(qPartyDest);
            criteria.setQPartyOrigin(qPartyOrigin);
            criteria.setQPartySale(qPartySale);
            criteria.setQCountryOrigin(qCountryOrigin);
            criteria.setQCountryDest(qCountryDest);
            criteria.setQCountrySale(qCountrySale);
            criteria.setQEbEtablissement(qEbEtablissement);
            criteria.setqPartyVendor(qPartyVendor);
            criteria.setqPartyIssuer(qPartyIssuer);

            query.from(qEbOrder);
            query = queryJoin(query, criteria);
            query = criteria.applyCriteria(query, fieldType == null);
            query = daoVisibility.addVisibilityToOrder(query, criteria, currentUser);
            // query.distinct();

            result = query.fetch();

            result.forEach(order -> {
                String listFlagIcon = order.getListFlag() != null ? order.getListFlag().trim() : null;

                if (listFlagIcon != null && !listFlagIcon.isEmpty()) {
                    order.setListEbFlagDTO(EbFlagDTO.sortByName(flagService.getListFlagDTOFromCodes(listFlagIcon)));
                }

                if (currentUser != null) {
                    order.setxEbOwnerOfTheRequest(currentUser);
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private JPAQuery<EbOrder> queryJoin(JPAQuery<EbOrder> resultQuery, SearchCriteriaOrder criteria) {
        resultQuery.leftJoin(qEbOrder.xEbPartyDestination(), qPartyDest);
        resultQuery.leftJoin(qPartyDest.xEcCountry(), qCountryDest);

        resultQuery.leftJoin(qEbOrder.xEbPartyOrigin(), qPartyOrigin);
        resultQuery.leftJoin(qPartyOrigin.xEcCountry(), qCountryOrigin);

        resultQuery.leftJoin(qEbOrder.xEbPartySale(), qPartySale);
        resultQuery.leftJoin(qPartySale.xEcCountry(), qCountrySale);

        resultQuery.leftJoin(qEbOrder.xEbPartyVendor(), qPartyVendor);
        resultQuery.leftJoin(qPartyVendor.xEcCountry(), qCountryVendor);

        resultQuery.leftJoin(qEbOrder.xEbPartyIssuer(), qPartyIssuer);
        resultQuery.leftJoin(qPartyIssuer.xEcCountry(), qCountryIssuer);

        resultQuery.leftJoin(qEbOrder.xEbEtablissement(), qEbEtablissement);

        if (criteria.getListRefDelivery() != null && !criteria.getListRefDelivery().isEmpty()) {
            resultQuery.leftJoin(qEbOrder.livraisons, qEbLivraison);
        }

        if (StringUtils.isNotBlank(criteria.getSearchterm())) {
            resultQuery.leftJoin(qEbOrder.orderLines, qEbOrderLine);
        }

        return resultQuery;
    }

    @SuppressWarnings("unchecked")
    @Override
    public EbOrder getOrder(SearchCriteriaOrder criteria) {
        EbOrder result = null;

        try {
            JPAQuery<EbOrder> query = new JPAQuery<EbOrder>(em);

            BooleanBuilder where = new BooleanBuilder();

            if (criteria != null) {

                criteria.applyAdvancedSearchForCustomFields(where, qEbOrder.customFields);

                if (criteria.getListCustomFieldsValues() != null && !criteria.getListCustomFieldsValues().isEmpty()) {
                    Predicate[] predicats = new Predicate[criteria.getListCustomFieldsValues().size()];

                    for (int i = 0; i < criteria.getListCustomFieldsValues().size(); i++) {
                        predicats[i] = qEbOrder.listCustomFieldsValueFlat
                            .contains(criteria.getListCustomFieldsValues().get(i));
                    }

                    where.andAnyOf(predicats);
                }

            }

            query.from(qEbOrder);
            query = criteria.applyCriteria(query, true);
            result = query.fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Integer nextOrderNumValue() {
        Integer result = null;

        try {
            result = ((BigInteger) em
                    .createNativeQuery("SELECT NEXTVAL('work.eb_del_order_eb_del_order_num_seq')").getSingleResult())
                    .intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    public Long getOrderCount(SearchCriteriaOrder criteria, EbUser currentUser) {
        Long result = 0L;

        try {
            JPAQuery<EbOrder> query = new JPAQuery<EbOrder>(em);
            criteria.setCount(true);

            query
                .select(
                    QEbOrder
                        .create(
                            qEbOrder.ebDelOrderNum,
                            qEbOrder.numOrderSAP,
                            qEbOrder.numOrderEDI,
                            qEbOrder.orderCustomerCode,
                            qEbOrder.orderType,
                            qEbOrder.campaignCode,
                            qEbOrder.campaignName,
                            qEbOrder.numOrderCustomer,
                            qEbOrder.orderCustomerName,
                            qEbOrder.shipTo,
                            qEbOrder.purchaseOrderType,
                            qEbOrder.customerCreationDate,
                            qEbOrder.deliveryDate,
                            qEbOrder.orderStatus,
                            qEbOrder.crossDock,

                            qPartyOrigin.ebPartyNum,
                            qPartyOrigin.company,
                            qPartyOrigin.adresse,
                            qPartyOrigin.city,
                            qCountryOrigin.ecCountryNum,
                            qCountryOrigin.libelle,

                            qPartyDest.ebPartyNum,
                            qPartyDest.company,
                            qPartyDest.adresse,
                            qPartyDest.city,
                            qCountryDest.ecCountryNum,
                            qCountryDest.libelle,

                            qPartySale.ebPartyNum,
                            qPartySale.company,
                            qPartySale.adresse,
                            qPartySale.city,
                            qCountrySale.ecCountryNum,
                            qCountrySale.libelle,

                            qEbEtablissement.ebEtablissementNum,
                            qEbEtablissement.nom,
                            qEbOrder.customFields,
                            qEbOrder.listCategories,
                            qEbOrder.targetDate,
                            qEbOrder.orderDate,

                            qPartyVendor.ebPartyNum,
                            qPartyVendor.company,
                            qPartyVendor.adresse,
                            qPartyVendor.city,
                            qCountryVendor.ecCountryNum,
                            qCountryVendor.libelle,

                            qPartyIssuer.ebPartyNum,
                            qPartyIssuer.company,
                            qPartyIssuer.adresse,
                            qPartyIssuer.city,
                            qCountryIssuer.ecCountryNum,
                            qCountryIssuer.libelle,
                            qEbOrder.listFlag,
                            qEbOrder.priority,
                            qEbOrder.customerOrderReference,
                            qEbOrder.dateOfArrival,
                            qEbOrder.xEbIncotermNum,
                            qEbOrder.xEcIncotermLibelle,
                            qEbOrder.complementaryInformations,
                            stringOrderWithExpression.as("stringOrderExpression"),
                            qEbOrder.reference,
                            qEbOrder.totalQuantityOrdered,
                            qEbOrder.totalQuantityPlannable,
                            qEbOrder.totalQuantityPlanned,
                            qEbOrder.totalQuantityPending,
                            qEbOrder.totalQuantityConfirmed,
                            qEbOrder.totalQuantityShipped,
                            qEbOrder.totalQuantityDelivered,
                            qEbOrder.totalQuantityCanceled));

            query.from(qEbOrder);
            query = queryJoin(query, criteria);
            query = criteria.applyCriteria(query, true);
            query = daoVisibility.addVisibilityToOrder(query, criteria, currentUser);
            result = query.distinct().fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbOrder> getAutoCompleteList(String term, String field) {
        List<String> result = null;
        List<EbOrder> listResult = new ArrayList<>();

        try {
            SearchCriteriaOrder criteria = new SearchCriteriaOrder();
            criteria = criteria.setProperty(field, term, criteria);
            JPAQuery query = new JPAQuery<>(em);

            query.select(autoCompleteFields.get(field));
            query.from(qEbOrder);
            query = queryJoin(query, criteria);
            query = criteria.applyCriteriaAutoComplete(query);

            query.distinct();
            query.limit(500);
            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (String value: result) {
            EbOrder liv = new EbOrder();
            liv.setProperty(field, value);
            listResult.add(liv);
        }

        return listResult;
    }

    @Override
    public EbOrder saveOrder(EbOrder order) {
        // Creation ou edition d'un order line
        EbParty xEbPartyOrigin = order.getxEbPartyOrigin();
        EbParty xEbPartyDestination = order.getxEbPartyDestination();
        EbParty xEbPartySale = null;

        EbParty xEbPartyVendor = order.getxEbPartyVendor();
        EbParty xEbPartyIssuer = order.getxEbPartyIssuer();

        // Origin & destination are mandatory so they can't be null
        xEbPartyOrigin.setZone(null);
        xEbPartyOrigin.setEtablissementAdresse(null);
        xEbPartyDestination.setZone(null);
        xEbPartyDestination.setEtablissementAdresse(null);

        ebPartyRepository.save(xEbPartyOrigin);
        ebPartyRepository.save(xEbPartyDestination);

        if (order.getxEbPartySale() != null) {
            xEbPartySale = order.getxEbPartySale();
            xEbPartySale.setZone(null);
            xEbPartySale.setEtablissementAdresse(null);
            ebPartyRepository.save(xEbPartySale);
            order.setxEbPartySale(xEbPartySale);
        }

        if (xEbPartyVendor != null) {
            xEbPartyVendor.setZone(null);
            xEbPartyVendor.setEtablissementAdresse(null);
            ebPartyRepository.save(xEbPartyVendor);
        }

        if (xEbPartyIssuer != null) {
            xEbPartyIssuer.setZone(null);
            xEbPartyIssuer.setEtablissementAdresse(null);
            ebPartyRepository.save(xEbPartyIssuer);
        }

        order.setxEbPartyOrigin(xEbPartyOrigin);
        order.setxEbPartyDestination(xEbPartyDestination);

        order.setxEbPartyVendor(xEbPartyVendor);
        order.setxEbPartyIssuer(xEbPartyIssuer);

        if (order.getEbDelOrderNum() == null && order.getCustomerCreationDate() == null) {
            order.setCustomerCreationDate(new Date());
        }

        if (order.getListCustomsFields() != null) {
            String customFields = JsonUtils.serializeToString(order.getListCustomsFields(), null);
            order.setCustomFields(customFields);
        }

        List<EbOrderLine> ordersLines = order.getOrderLines();
        order.setOrderLines(null);
        order = ebOrderRepository.save(order);

        for (EbOrderLine ebOrderLine: ordersLines) {
            ebOrderLine.setxEbDelOrder(new EbOrder(order.getEbDelOrderNum()));

            if (ebOrderLine.getListCustomsFields() != null) {
                String customFields = JsonUtils.serializeToString(ebOrderLine.getListCustomsFields(), null);
                ebOrderLine.setCustomFields(customFields);
            }

        }

        ordersLines = this.ebOrderLineRepository.saveAll(ordersLines);
        // Bind entity managed order lines to original modal to return full
        // filled model
        order.setOrderLines(ordersLines);

        return order;
    }

    @Override
    public EbLivraison planifyOrder(EbOrderPlanificationDTO orderPlanification) {

        EbLivraison ebLivraison = new EbLivraison();
        if (orderPlanification.getEbDelLivraisonNum() != null) {
            return updatePlanification(orderPlanification);
        }

        else {
            EbOrder order = ebOrderRepository.findById(orderPlanification.getEbDelOrderNum()).get();

            EbParty xEbPartyOrigin = new EbParty(order.getxEbPartyOrigin());
            xEbPartyOrigin.setEbPartyNum(null);
            xEbPartyOrigin.setxEcCountry(order.getxEbPartyOrigin().getxEcCountry());
            xEbPartyOrigin.setEtablissementAdresse(null);
            xEbPartyOrigin.setZone(order.getxEbPartyOrigin().getZone());
            ebPartyRepository.save(xEbPartyOrigin);

            // Will be reenabled to clone entity
            EbParty xEbPartyDestination = new EbParty(order.getxEbPartyDestination());
            xEbPartyDestination.setEbPartyNum(null);
            xEbPartyDestination.setxEcCountry(order.getxEbPartyDestination().getxEcCountry());
            xEbPartyDestination.setEtablissementAdresse(null);
            xEbPartyDestination.setZone(order.getxEbPartyDestination().getZone());
            ebPartyRepository.save(xEbPartyDestination);

            EbParty xEbPartySale = null;

            if (order.getxEbPartySale() != null) {
                xEbPartySale = new EbParty(order.getxEbPartySale());
                xEbPartySale.setEbPartyNum(null);
                xEbPartySale.setxEcCountry(order.getxEbPartySale().getxEcCountry());
                xEbPartySale.setEtablissementAdresse(null);
                xEbPartySale.setZone(order.getxEbPartySale().getZone());
                ebPartyRepository.save(xEbPartySale);
            }

            // Retrieve all linked order lines
            List<Long> orderLinePlanNums = new ArrayList<>();

            for (EbOrderLinePlanificationDTO ebOrderLinePlanification: orderPlanification.getListToPlan()) {

                if (ebOrderLinePlanification.getQuantityToPlan() > 0) {
                    orderLinePlanNums.add(ebOrderLinePlanification.getEbDelOrderLineNum());
                }

            }

            List<EbOrderLine> ebOrderLineList = ebOrderLineRepository.getOrderLineNums(orderLinePlanNums);

            ebLivraison.setNumOrderEDI(order.getNumOrderEDI());
            ebLivraison.setCampaignCode(order.getCampaignCode());
            ebLivraison.setCampaignName(order.getCampaignName());
            ebLivraison.setxEbEtablissement(order.getxEbEtablissement());
            ebLivraison.setxEbCompagnie(order.getxEbCompagnie());
            ebLivraison.setNumOrderCustomer(order.getNumOrderCustomer());
            ebLivraison.setNumOrderEDI(order.getNumOrderEDI());
            ebLivraison.setNumOrderSAP(order.getNumOrderSAP());
            ebLivraison.setCrossDock(order.getCrossDock());
            if (order.getCustomFields() != null) ebLivraison.setCustomFields(order.getCustomFields());
            if (order.getListCategories() != null) ebLivraison.setListCategories(order.getListCategories());
            ebLivraison.setxEbDelOrder(order);
            ebLivraison.setStatusDelivery(StatusDelivery.PENDING);
            ebLivraison.setxEbOwnerOfTheRequest(order.getxEbOwnerOfTheRequest());
            ebLivraison.setxEbIncotermNum(order.getxEbIncotermNum());
            ebLivraison.setxEcIncotermLibelle(order.getxEcIncotermLibelle());
            ebLivraison.setComplementaryInformations(order.getComplementaryInformations());
            ebLivraison.setxEbPartyOrigin(order.getxEbPartyOrigin());
            ebLivraison.getxEbPartyOrigin().setZoneRef(order.getxEbPartyOrigin().getZoneRef());
            ebLivraison.setxEbPartyDestination(order.getxEbPartyDestination());
            ebLivraison.getxEbPartyDestination().setZoneRef(order.getxEbPartyDestination().getZoneRef());
            ebLivraison.setxEbPartySale(order.getxEbPartySale());
            ebLivraison.setCustomerOrderReference(order.getCustomerOrderReference());
            ebLivraison.setOrderCreationDate(order.getCustomerCreationDate());
            ebLivraison.setRefOrder(order.getReference());
            ebLivraison.setListFlag(order.getListFlag());
            ebLivraison.setxEbCompagnie(order.getxEbCompagnie());

            ebLivraison
                .setEbDelReference(
                    orderService
                        .generateReference(
                            ebLivraison.getxEbCompagnie().getCode(),
                            daoDelivery.nextValDelivery(),
                            Constants.refDeliveryPrefix));

            ebLivraison = ebLivraisonRepository.save(ebLivraison);
            List<EbLivraisonLine> ebLivraisonLineList = new ArrayList<>();

            for (EbOrderLinePlanificationDTO ebOrderLinePlanification: orderPlanification.getListToPlan()) {

                if (ebOrderLinePlanification.getQuantityToPlan() > 0) {
                    EbOrderLine ebOrderLine = ebOrderLineList
                        .stream()
                        .filter(o -> o.getEbDelOrderLineNum().equals(ebOrderLinePlanification.getEbDelOrderLineNum()))
                        .findFirst().get();
                    EbLivraisonLine ebLivraisonLine = new EbLivraisonLine();
                    ebLivraisonLine.setxEbDelLivraison(ebLivraison);
                    ebLivraisonLine.setArticleName(ebOrderLine.getArticleName());
                    ebLivraisonLine.setIdDeliveryLine(ebOrderLine.getIdOrderLine());
                    ebLivraisonLine.setRefArticle(ebOrderLine.getRefArticle());
                    ebLivraisonLine.setPriority(ebOrderLine.getPriority());
                    ebLivraisonLine.setSerialNumber(ebOrderLine.getSerialNumber());
                    ebLivraisonLine.setSpecification(ebOrderLine.getSpecification());
                    ebLivraisonLine.setxEbOrderLigne(ebOrderLine);
                    ebLivraisonLine.setCustomFields(ebOrderLine.getCustomFields());
                    ebLivraisonLine.setListCategories(ebOrderLine.getListCategories());

                    ebLivraisonLine.setHeight(ebOrderLine.getHeight());
                    ebLivraisonLine.setWeight(ebOrderLine.getWeight());
                    ebLivraisonLine.setWidth(ebOrderLine.getWidth());
                    ebLivraisonLine.setLength(ebOrderLine.getLength());
                    ebLivraisonLine.setHsCode(ebOrderLine.getHsCode());
                    ebLivraisonLine.setVolume(ebOrderLine.getVolume());
                    ebLivraisonLine.setItemName(ebOrderLine.getItemName());
                    ebLivraisonLine.setItemNumber(ebOrderLine.getItemNumber());
                    ebLivraisonLine.setxEcCountryOrigin(ebOrderLine.getxEcCountryOrigin());
                    ebLivraisonLine.setPartNumber(ebOrderLine.getPartNumber());
                    ebLivraisonLine.setSerialNumber(ebOrderLine.getSerialNumber());
                    ebLivraisonLine.setComment(ebOrderLine.getComment());
                    ebLivraisonLine.setSpecification(ebOrderLine.getSpecification());
                    ebLivraisonLine.setTargetDate(ebOrderLine.getTargetDate());
                    ebLivraisonLine.setxEbtypeOfUnit(ebOrderLine.getxEbtypeOfUnit());
                    ebLivraisonLine.setSerialized(ebOrderLine.getSerialized());
                    ebLivraisonLine.setQuantity(ebOrderLinePlanification.getQuantityToPlan());

                    ebLivraisonLineList.add(ebLivraisonLine);
                }

            }

            ebLivraison.setxEbDelLivraisonLine(ebLivraisonLineList);

            // Genere La reference de la Livraison

            if (StringUtils.isBlank(ebLivraison.getEbDelReference()) && ebLivraison.getEbDelLivraisonNum() != null) {
                String refPrefix = "-V";

                String codeCompany = ebLivraison.getxEbCompagnie().getCode();

                String refOrder = StringUtils
                    .leftPad(ebLivraison.getEbDelLivraisonNum().toString().toUpperCase(), 5, "0");

                ebLivraison.setEbDelReference(codeCompany.concat(refPrefix).concat(refOrder));
            }

            ebLivraison = ebLivraisonRepository.save(ebLivraison);

            return ebLivraison;
        }

    }

    @Override
    public EbLivraison updatePlanification(EbOrderPlanificationDTO orderPlanification) {
        EbLivraison ebLivraison = ebLivraisonRepository.findById(orderPlanification.getEbDelLivraisonNum()).get();

        List<Long> orderLinePlanNums = new ArrayList<>();
        List<Long> ebLivraisonLinePlannedNums = new ArrayList<>();

        for (EbOrderLinePlanificationDTO ebOrderLinePlanification: orderPlanification.getListToPlan()) {

            if (ebOrderLinePlanification.getQuantityToPlan() > 0) {
                orderLinePlanNums.add(ebOrderLinePlanification.getEbDelOrderLineNum());
                if (ebOrderLinePlanification.getEbDelLivraisonLineNum() != null) ebLivraisonLinePlannedNums
                    .add(ebOrderLinePlanification.getEbDelLivraisonLineNum());
            }

        }

        List<EbOrderLine> ebOrderLineList = ebOrderLineRepository.getOrderLineNums(orderLinePlanNums);
        List<EbLivraisonLine> ebLivraisonLineListPlanned = new ArrayList<EbLivraisonLine>();
        if (ebLivraisonLinePlannedNums != null
            && ebLivraisonLinePlannedNums.size() > 0) ebLivraisonLineListPlanned = ebLivraisonLineRepository
                .getEbLivraisonLineNums(ebLivraisonLinePlannedNums);
        List<EbLivraisonLine> ebLivraisonLineList = new ArrayList<>();

        for (EbOrderLinePlanificationDTO ebOrderLinePlanification: orderPlanification.getListToPlan()) {

            if (ebOrderLinePlanification.getQuantityToPlan() > 0) {
                EbLivraisonLine ebLivraisonLine;

                if (ebOrderLinePlanification.getEbDelLivraisonLineNum() != null) {
                    ebLivraisonLine = ebLivraisonLineListPlanned
                        .stream()
                        .filter(
                            o -> o
                                .getEbDelLivraisonLineNum().equals(ebOrderLinePlanification.getEbDelLivraisonLineNum()))
                        .findFirst().get();
                    ebLivraisonLine.setQuantity(ebOrderLinePlanification.getQuantityToPlan());
                    ebLivraisonLineList.add(ebLivraisonLine);
                }
                else {
                    EbOrderLine ebOrderLine = ebOrderLineList
                        .stream()
                        .filter(o -> o.getEbDelOrderLineNum().equals(ebOrderLinePlanification.getEbDelOrderLineNum()))
                        .findFirst().get();
                    ebLivraisonLine = new EbLivraisonLine();
                    ebLivraisonLine.setxEbDelLivraison(ebLivraison);
                    ebLivraisonLine.setArticleName(ebOrderLine.getArticleName());
                    ebLivraisonLine.setIdDeliveryLine(ebOrderLine.getIdOrderLine());
                    ebLivraisonLine.setRefArticle(ebOrderLine.getRefArticle());
                    ebLivraisonLine.setPriority(ebOrderLine.getPriority());
                    ebLivraisonLine.setSerialNumber(ebOrderLine.getSerialNumber());
                    ebLivraisonLine.setSpecification(ebOrderLine.getSpecification());
                    ebLivraisonLine.setxEbOrderLigne(ebOrderLine);
                    ebLivraisonLine.setCustomFields(ebOrderLine.getCustomFields());
                    ebLivraisonLine.setListCategories(ebOrderLine.getListCategories());

                    ebLivraisonLine.setHeight(ebOrderLine.getHeight());
                    ebLivraisonLine.setWeight(ebOrderLine.getWeight());
                    ebLivraisonLine.setWidth(ebOrderLine.getWidth());
                    ebLivraisonLine.setLength(ebOrderLine.getLength());
                    ebLivraisonLine.setHsCode(ebOrderLine.getHsCode());
                    ebLivraisonLine.setVolume(ebOrderLine.getVolume());
                    ebLivraisonLine.setItemName(ebOrderLine.getItemName());
                    ebLivraisonLine.setItemNumber(ebOrderLine.getItemNumber());
                    ebLivraisonLine.setxEcCountryOrigin(ebOrderLine.getxEcCountryOrigin());
                    ebLivraisonLine.setPartNumber(ebOrderLine.getPartNumber());
                    ebLivraisonLine.setSerialNumber(ebOrderLine.getSerialNumber());
                    ebLivraisonLine.setComment(ebOrderLine.getComment());
                    ebLivraisonLine.setSpecification(ebOrderLine.getSpecification());
                    ebLivraisonLine.setTargetDate(ebOrderLine.getTargetDate());
                    ebLivraisonLine.setxEbtypeOfUnit(ebOrderLine.getxEbtypeOfUnit());
                    ebLivraisonLine.setQuantity(ebOrderLinePlanification.getQuantityToPlan());

                    ebLivraisonLineList.add(ebLivraisonLine);
                }

            }

        }

        ebLivraison.setxEbDelLivraisonLine(ebLivraisonLineList);
        ebLivraison.setDateOfGoodsAvailability(orderPlanification.getDateOfGoodsAvailability());
        ebLivraison = ebLivraisonRepository.save(ebLivraison);

        return ebLivraison;
    }

    public Long nextValOrder() {
        Long result = null;

        try {
            result = ((BigInteger) em
                .createNativeQuery("SELECT NEXTVAL('work.eb_del_order_eb_del_order_num_seq')").getSingleResult())
                    .longValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

}
