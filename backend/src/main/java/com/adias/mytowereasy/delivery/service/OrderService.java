package com.adias.mytowereasy.delivery.service;

import java.util.List;

import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.dto.EbOrderPlanificationDTO;
import com.adias.mytowereasy.model.TransfertUserObject;


public interface OrderService {
    public List<EbOrder> getListOrders(SearchCriteriaOrder criteria);

    public EbOrder getOrder(SearchCriteriaOrder criteria);

    public Integer getOrderCount(SearchCriteriaOrder criteria);

    public List<EbOrder> getAutoCompleteList(String term, String field);

    public EbOrder createOrder(EbOrder order);

    public EbOrder updateOrder(EbOrder order);

    void historizeOrderAction(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        String username,
        String usernameFor);

    public void updateOrderEtablissement(TransfertUserObject transfertUserObjet);

    public EbLivraison planifyOrder(EbOrderPlanificationDTO orderPlanification);

    public EbOrder saveFlags(Long ebDelOrderNum, String newFlags);

    /**
     * Global calculation method for order
     * 
     * @param order
     *            Order to recompute calculation
     * @param autoPersist
     * @return
     */
    public EbOrder calculateOrder(Long ebDelOrderNum, boolean autoPersist);

    /**
     * Recalculate Order status using quantities<br/>
     * <b>Quantities must be updated !</b> You can compute calculation using
     * calculateQuantities
     * method
     * 
     * @param order
     *            Order to recompute calculation
     * @return The order with calculated status
     */
    public EbOrder calculateStatus(EbOrder order);

    /**
     * Recalculate all computed quantities from operational quantities
     * 
     * @param order
     *            Order to recompute calculation
     * @return The order with calculated quantities
     */
    public EbOrder calculateQuantities(EbOrder order);

    String generateReference(String companyCode, Long entityNum, String prefix);
}
