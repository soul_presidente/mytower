package com.adias.mytowereasy.delivery.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.delivery.dao.DaoOrderLine;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.repository.EbOrderLineRepository;
import com.adias.mytowereasy.delivery.service.OrderLineService;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.dto.EbOrderLineDto;
import com.adias.mytowereasy.dto.mapper.DeliveryMapper;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.ConnectedUserService;


@Service
public class OrderLineServiceImpl implements OrderLineService {
    @Autowired
    DaoOrderLine daoOrderLine;

    @Autowired
    EbOrderLineRepository ebOrderLineRepository;

    @Autowired
    DeliveryMapper deliveryMapper;

    @Autowired
    ConnectedUserService connectedUserService;

    @Override
    public List<EbOrderLine> getOrderLines(Long ebOrderLineNum) {
        return daoOrderLine.getOrderLines(ebOrderLineNum);
    }

    @Override
    public List<EbOrderLineDto> getOrderLinesPlanifiable(Long ebDelOrderNum) {
        List<EbOrderLine> entities = ebOrderLineRepository.findByOrder(ebDelOrderNum);
        List<EbOrderLineDto> dtos = deliveryMapper.ebOrderLinesToEbOrderLinesDto(entities);

        List<EbOrderLineDto> filteredDtos = dtos
            .stream().filter(l -> l.getQuantityPlannable() > 0).collect(Collectors.toList());
        return filteredDtos;
    }

    @Override
    public List<EbOrderLine> getOrderLinesPlanifiableTableau(SearchCriteriaOrder criteria) {
        EbUser currentUser = connectedUserService.getCurrentUser();
        return daoOrderLine.getOrderLinesPlanifiableTableau(criteria, currentUser);
    }

    @Override
    public Long getOrderLineCountTableau(SearchCriteriaOrder criteria) {
        EbUser currentUser = connectedUserService.getCurrentUser();
        return daoOrderLine.getOrderLineCountTableau(criteria, currentUser);
    }

    @Override
    public List<EbOrderLine> getOrderLinesForExport(SearchCriteriaOrder criteria) {
        EbUser currentUser = connectedUserService.getCurrentUserFromDB(criteria);
        return daoOrderLine.getOrderLinesForExport(criteria, currentUser);
    }

    @Override
    public List<EbOrderLine> findByTerm(Long ebDelOrderNum, String searchterm) {
        return daoOrderLine.findByTerm(ebDelOrderNum, searchterm);
    }
}
