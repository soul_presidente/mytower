package com.adias.mytowereasy.delivery.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;


@Repository
public interface EbLivraisonLineRepository extends JpaRepository<EbLivraisonLine, Long> {
    @Modifying
    @Transactional
    void deleteByxEbDelLivraison_EbDelLivraisonNum(Long ebLivraisonNum);

    List<EbLivraisonLine> findByxEbDelLivraison_EbDelLivraisonNum(Long ebLivraisonNum);

    public List<EbLivraisonLine> findByxEbOrderLigne(EbOrderLine ebOrderLine);

    @Query("select ol from EbLivraisonLine l right join l.xEbOrderLigne ol "
        + "	on l.xEbOrderLigne.ebDelOrderLineNum = ol.ebDelOrderLineNum"
        + " where l.ebDelLivraisonLineNum is not null and   l.ebDelLivraisonLineNum  = ?1  "

    )
    public EbOrderLine findOrderLineByLivraisonLine(long ebLivraisonLineNum);

    @Query(" from EbLivraisonLine l where l.ebDelLivraisonLineNum IN (?1) ")
    public List<EbLivraisonLine> getEbLivraisonLineNums(List<Long> ebLivraisonLineNums);

    @Query("select o from EbLivraisonLine dl join dl.xEbOrderLigne ol join ol.xEbDelOrder o "
        + "on dl.xEbOrderLigne.ebDelOrderLineNum = ol.ebDelOrderLineNum and  o.ebDelOrderNum = ol.xEbDelOrder.ebDelOrderNum "
        + "where dl.ebDelLivraisonLineNum = ?1  ")
    public EbOrder getOrderByDeliveryLine(long ebDelDeliveryLineNum);
}
