package com.adias.mytowereasy.delivery.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.model.EbUser;


@Repository
public interface EbLivraisonRepository extends JpaRepository<EbLivraison, Long> {
    @Query("select max(l.ebDelLivraisonNum) from EbLivraison l") // c'est du
                                                                 // JPQL
    public Long getMaxLivraison();

    @Query("select max(line.ebDelLivraisonLineNum) from EbLivraisonLine line")
    public Long getMaxLivraisonLine();

    @Transactional
    @Modifying
    @Query(
        value = "update work.eb_del_livraison set x_eb_etablissement= ?2 "
            + "where regexp_split_to_array(?3, ',') && (regexp_split_to_array(list_labels,',')) "
            + "and x_eb_etablissement = ?1",

        nativeQuery = true)
    public int updateDeliveryEtablissementAndLabels(
        Integer oldEtablissementNum,
        Integer newEtablissementNum,
        String listLabels);

    @EntityGraph(value = "Eblivraison.ebParty.ecCountry", type = EntityGraphType.LOAD)
    public List<EbLivraison> findByEbDelLivraisonNumIn(List<Long> ebDelLivraisonNums);

    @Query("select l from EbLivraison l join l.xEbDelLivraisonLine li " + "where li.ebDelLivraisonLineNum = ?1 and "
        + "li.xEbDelLivraison.ebDelLivraisonNum = l.ebDelLivraisonNum")
    public Optional<EbLivraison> findByLivraisonLine(Long ebDelLivraisonLineNum);

    @Query("select l from EbLivraison l join l.xEbDemande d " + "where d.ebDemandeNum= ?1 and "
        + "l.xEbDemande.ebDemandeNum = d.ebDemandeNum")
    public List<EbLivraison> findByEbDemande(Integer ebDemandeNum);

    @Query("select l.ebDelLivraisonNum from EbLivraison l join l.xEbDemande d " + "where d.ebDemandeNum= ?1 and "
        + "l.xEbDemande.ebDemandeNum = d.ebDemandeNum")
    public List<Long> findByEbDemandeNum(Integer ebDemandeNum);

    public List<EbLivraison> findByxEbDelOrder(EbOrder ebOrder);

    @Query("SELECT DISTINCT u FROM EbUser u LEFT JOIN EbLivraison l ON u.ebUserNum = l.xEbOwnerOfTheRequest WHERE l.xEbCompagnie.ebCompagnieNum = :compagnieNum")
    List<EbUser> getListOwnerRequest(@Param("compagnieNum") Integer compagnieNum);
}
