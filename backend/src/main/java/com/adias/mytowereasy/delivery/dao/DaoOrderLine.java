package com.adias.mytowereasy.delivery.dao;

import java.util.List;

import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.model.EbUser;


public interface DaoOrderLine {
    public List<EbOrderLine> getOrderLines(Long ebOrderLineNum);

    public List<EbOrderLine> getOrderLinesForExport(SearchCriteriaOrder criteria, EbUser currentUser);

    public List<EbOrderLine> getOrderLinesPlanifiableTableau(SearchCriteriaOrder criteria, EbUser currentUser);

    public Long getOrderLineCountTableau(SearchCriteriaOrder criteria, EbUser currentUser);

    public List<EbOrderLine> findByTerm(Long ebDelOrderNum, String searchterm);
}
