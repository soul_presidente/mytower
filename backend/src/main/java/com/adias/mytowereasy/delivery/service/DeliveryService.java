package com.adias.mytowereasy.delivery.service;

import java.util.List;

import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.TransfertUserObject;


public interface DeliveryService {
    public List<EbLivraison> getListDeliveries(SearchCriteriaDelivery criteria);

    public EbLivraison getDelivery(SearchCriteriaDelivery criteria);

    public Integer getDeliveryCount(SearchCriteriaDelivery criteria);

    public abstract EbLivraison saveLivraison(EbLivraison ebLivraison);

    public void historizeDeliveryAction(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        String username,
        String usernameFor);

    public List<EbLivraison> getAutoCompleteList(String term, String field);

    public void updateDeliveryEtablissement(TransfertUserObject transfertUserObjet);

    public EbDemande preConsolidateDelivery(List<Long> listDeliveryForConsolidateNum);

    public void deleteDelivery(List<Long> listSelectedDelivery);

    public EbLivraison updateStatusDelivery(Long ebLivraisonListNum, StatusDelivery status);

    public void deleteOneDelivery(Long deliveryToDeleteNum);

    public void deleteOnDeliveryLine(Long deliveryLineToDeleteNum);

    public EbLivraison cancelDelivery(Long ebDelLivraisonNum);

    void calculateParentOrder(Long ebLivraisonNum, boolean autoPersist);

    EbLivraison confirmDelivery(Long ebDelLivraisonNum);

    /**
     * Securly update only fields that should be updated
     */
    EbLivraison updateLivraison(EbLivraison ebLivraison);
}
