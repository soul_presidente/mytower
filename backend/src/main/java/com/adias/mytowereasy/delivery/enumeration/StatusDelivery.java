package com.adias.mytowereasy.delivery.enumeration;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.commons.lang3.StringUtils;

import com.adias.mytowereasy.model.StatusEnum;
import com.adias.mytowereasy.util.enums.GenericEnumJPAConverter;
import com.fasterxml.jackson.annotation.JsonCreator;


public enum StatusDelivery implements StatusEnum {
    PENDING(1, "PENDING", 0, true),
    CONFIRMED(2, "CONFIRMED", 1, true),
    SHIPPED(3, "SHIPPED", 2, true),
    DELIVERED(4, "DELIVERED", 3, true),
    CANCELED(5, "CANCELED", -1, true);

    private Integer code;
    private String key;
    private Integer order; // Order >= 0 are normal steps of workflow, negative
                           // are specials (like cancel)
    private Boolean enabled;

    private StatusDelivery(Integer code, String key, Integer order, Boolean enabled) {
        this.code = code;
        this.key = key;
        this.order = order;
        this.enabled = enabled;
    }

    public static Optional<StatusDelivery> getOptionalStatusByLibelle(String libelle) {

        if (StringUtils.isNotBlank(libelle)) {
            return Arrays
                .stream(values()).filter(it -> it.getKey().toLowerCase().contains(libelle.toLowerCase())).findFirst();
        }
        return Optional.empty();
    }
      
	public static Optional<StatusDelivery> getStatusByCode(@Nonnull Integer code) {
		return Arrays.stream(values()).filter(it -> it.getCode().equals(code)).findFirst();
	}
    
    public static List<StatusDelivery> getList() {
        return Arrays.stream(values()).collect(Collectors.toList());
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Integer getOrder() {
        return order;
    }

    @Override
    public Boolean getEnabled() {
        return enabled;
    }

    @JsonCreator
    public static StatusDelivery valueOf(Integer code) {

        for (StatusDelivery statusDelivery: StatusDelivery.values()) {
            if (statusDelivery.getCode().equals(code)) return statusDelivery;
        }

        return null;
    }

    public static class Converter extends GenericEnumJPAConverter<StatusDelivery> {
        public Converter() {
            super(StatusDelivery.class);
        }
    }
}
