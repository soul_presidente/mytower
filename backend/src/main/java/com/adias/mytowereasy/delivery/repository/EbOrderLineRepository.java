package com.adias.mytowereasy.delivery.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.delivery.model.EbOrderLine;


@Repository
public interface EbOrderLineRepository extends JpaRepository<EbOrderLine, Long> {
    @Query("from EbOrderLine l where l.xEbDelOrder.ebDelOrderNum = :ebDelOrderNum")
    public List<EbOrderLine> findByOrder(@Param("ebDelOrderNum") Long ebDelOrderNum);

    @Query("from EbOrderLine l where l.ebDelOrderLineNum IN (?1)")
    public List<EbOrderLine> getOrderLineNums(List<Long> ebDelOrderNums);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbOrderLine ol WHERE ol.xEbDelOrder.ebDelOrderNum = :ebDelOrderNum")
    public Integer deleteOrderLineByOrderNum(@Param("ebDelOrderNum") Long ebDelOrderNum);
}
