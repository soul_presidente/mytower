package com.adias.mytowereasy.delivery.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.delivery.dao.DaoDeliveryLine;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.service.DeliveryLineService;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.MyTowerService;


@Service
public class DeliveryLineServiceImpl extends MyTowerService implements DeliveryLineService {
    @Autowired
    DaoDeliveryLine daoDeliveryLine;

    @Autowired
    ConnectedUserService connectedUserService;

    @Override
    public List<EbLivraisonLine> getDeliveryLines(Long ebDelLivraisonNum) {
        EbUser currentUser = connectedUserService.getCurrentUserFromDB();
        return daoDeliveryLine.getDeliveryLines(ebDelLivraisonNum, currentUser);
    }

    @Override
    public List<EbLivraisonLine> getDeliveryLinesForExport(SearchCriteriaDelivery criteria) {
        EbUser currentUser = connectedUserService.getCurrentUserFromDB(criteria);
        return daoDeliveryLine.getDeliveryLinesForExport(criteria, currentUser);
    }

    @Override
    public EbLivraisonLine inlineDeliveryDashboardUpdate(EbLivraisonLine sent) {
        EbLivraisonLine entity = ebLivraisonLineRepository.findById(sent.getEbDelLivraisonLineNum()).get();

        // Update field by field for security reason
        entity.setComment(sent.getComment());

        entity = ebLivraisonLineRepository.save(entity);

        return ebLivraisonLineRepository.findById(entity.getEbDelLivraisonLineNum()).get();
    }

    @Override
    public List<EbLivraisonLine> getDeliveryLinesForGenericTable(SearchCriteriaDelivery criteriaDelivery) {
        EbUser currentUser = connectedUserService.getCurrentUser();
        return daoDeliveryLine.getDeliveryLinesForGenericTable(criteriaDelivery, currentUser);
    }

    @Override
    public Long getDeliveryLineCountForGenericTable(SearchCriteriaDelivery criteriaDelivery) {
        EbUser currentUser = connectedUserService.getCurrentUser();
        return daoDeliveryLine.getDeliveryLineCountForGenericTable(criteriaDelivery, currentUser);
    }
}
