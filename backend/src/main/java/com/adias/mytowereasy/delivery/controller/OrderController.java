/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.delivery.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.service.DeliveryLineService;
import com.adias.mytowereasy.delivery.service.DeliveryService;
import com.adias.mytowereasy.delivery.service.OrderLineService;
import com.adias.mytowereasy.delivery.service.OrderService;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.dto.EbLivraisonDto;
import com.adias.mytowereasy.dto.EbOrderDto;
import com.adias.mytowereasy.dto.EbOrderLineDto;
import com.adias.mytowereasy.dto.EbOrderPlanificationDTO;
import com.adias.mytowereasy.dto.mapper.DeliveryMapper;
import com.adias.mytowereasy.service.CustomFieldService;
import com.adias.mytowereasy.service.WebHooksService;
import com.adias.mytowereasy.trpl.service.GrilleTransportService;
import com.adias.mytowereasy.trpl.service.PlanTransportService;
import com.adias.mytowereasy.trpl.service.TrancheService;
import com.adias.mytowereasy.trpl.service.ZoneService;


@RestController
@RequestMapping(value = "api/order-overview")
@CrossOrigin("*")
public class OrderController {
    private static final Logger log = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    OrderService orderService;

    @Autowired
    OrderLineService orderLineService;

    @Autowired
    DeliveryMapper deliveryMapper;

    @Autowired
    ZoneService zoneService;

    @Autowired
    TrancheService trancheService;

    @Autowired
    GrilleTransportService grilleService;

    @Autowired
    PlanTransportService planTransportService;

    @Autowired
    TaskExecutor taskExecutor;

    @Autowired
    WebHooksService webHooksService;

    @Autowired
    DeliveryService deliveryService;

    @Autowired
    DeliveryLineService deliveryLineService;

    @Autowired
    CustomFieldService customFieldService;

    @RequestMapping(value = "list", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object> getListOrders(@RequestBody(required = false) SearchCriteriaOrder criteria)
        throws JsonProcessingException,
        Exception

    {

        if (criteria.getSearchInput() != null && criteria.getSearchInput().length() > 0) {
            criteria
                .setSearchInput(criteria.getSearchInput(), customFieldService.getCustomFields(criteria.getEbUserNum()));
        }

        List<EbOrder> resultOrder = orderService.getListOrders(criteria);
        List<EbOrderDto> resultOrderDto = deliveryMapper.ebOrdersToEbOrdersDto(resultOrder);
        HashMap<String, Object> result = new HashMap<>();

        result.put("count", orderService.getOrderCount(criteria));
        result.put("data", resultOrderDto);
        return result;
    }

    @PostMapping(value = "/list-count")
    public Integer getListCount(@RequestBody SearchCriteriaOrder criteria) throws JsonProcessingException, Exception {

        if (StringUtils.isNotBlank(criteria.getSearchInput())) {
            criteria
                .setSearchInput(criteria.getSearchInput(), customFieldService.getCustomFields(criteria.getEbUserNum()));
        }

        return orderService.getOrderCount(criteria);
    }

    @RequestMapping(value = "get-order", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody EbOrderDto getOrder(@RequestBody(required = false) SearchCriteriaOrder criteria) {
        EbOrder ebOrder = orderService.getOrder(criteria);
        EbOrderDto orderDto = deliveryMapper.ebOrderToEbOrderDto(ebOrder);

        if (BooleanUtils.isTrue(criteria.getIncludeLines())) {
            List<EbOrderLineDto> orderLines = this.getOrderLines(criteria.getEbdelOrderNum());
            orderDto.setOrderLines(orderLines);
        }

        return orderDto;
    }

    @RequestMapping(value = "get-order-lines", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody List<EbOrderLineDto> getOrderLines(@RequestBody(required = false) Long ebOrderLineNum) {
        List<EbOrderLine> result = orderLineService.getOrderLines(ebOrderLineNum);
        List<EbOrderLineDto> dtos = deliveryMapper.ebOrderLinesToEbOrderLinesDto(result);
        return dtos;
    }

    @RequestMapping(value = "/list-order-lines", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody List<EbOrderLineDto> getOrderLinesBySearchTerm(
        @RequestParam("ebDelOrderNum") Long ebDelOrderNum,
        @RequestParam("searchterm") String searchterm) {
        return deliveryMapper
            .ebOrderLinesToEbOrderLinesDto(orderLineService.findByTerm(ebDelOrderNum, searchterm));
    }

    @GetMapping(value = "get-order-lines-planifiable", produces = "application/json")
    public @ResponseBody List<EbOrderLineDto> getOrderLinesPlanifiable(@RequestParam Long ebDelOrderNum) {
        return orderLineService.getOrderLinesPlanifiable(ebDelOrderNum);
    }

    @PostMapping(value = "get-order-lines-planifiable-tableau", produces = "application/json")
    public @ResponseBody Map<String, Object>
        getOrderLinesPlanifiableTableau(@RequestBody(required = false) SearchCriteriaOrder criteria) {
        List<EbOrderLine> resultOrderLine = orderLineService.getOrderLinesPlanifiableTableau(criteria);
        List<EbOrderLineDto> resultOrderLineDto = deliveryMapper.ebOrderLinesToEbOrderLinesDto(resultOrderLine);
        // le filtre pour recuperer les lignes planifiables
        List<EbOrderLineDto> filteredDtos = resultOrderLineDto
            .stream().filter(l -> l.getQuantityPlannable() > 0).collect(Collectors.toList());
        HashMap<String, Object> result = new HashMap<>();

        result.put("count", orderLineService.getOrderLineCountTableau(criteria));
        result.put("data", resultOrderLineDto);
        return result;
    }

    @RequestMapping(value = "get-table-state", method = RequestMethod.GET)
    public @ResponseBody String saveTableConfiguration() {
        return "c'est bon";
    }

    @RequestMapping(value = "create-order", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody EbOrder createOrder(@RequestBody EbOrderDto ebOrder) {
        EbOrder order = new EbOrder();
        order = deliveryMapper.ebOrderDtoToEbOrder(ebOrder);
        return orderService.createOrder(order);
    }

    @RequestMapping(value = "save-order", method = RequestMethod.PUT, produces = "application/json")
    public @ResponseBody EbOrder saveOrder(@RequestBody EbOrderDto ebOrder) {
        EbOrder order = new EbOrder();
        order = deliveryMapper.ebOrderDtoToEbOrder(ebOrder);
        return orderService.updateOrder(order);
    }

    @PostMapping(value = "planify-order")
    public @ResponseBody EbLivraisonDto planifyOrder(@RequestBody EbOrderPlanificationDTO ebOrderPlanification) {
        return deliveryMapper.ebLivraisonToEbLivraisonDto(orderService.planifyOrder(ebOrderPlanification));
    }

    @GetMapping("/saveFlags")
    public EbOrder saveFlag(@RequestParam Long ebDelOrderNum, @RequestParam String newFlags) {
        return orderService.saveFlags(ebDelOrderNum, newFlags);
    }
}
