/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.delivery.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.*;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.util.ObjectDeserializer;


@Entity
@Table(name = "eb_del_livraison_line", indexes = {
    @Index(name = "idx_eb_del_livraison_line_x_eb_del_livraison", columnList = "x_eb_del_livraison"),
    @Index(name = "idx_eb_del_livraison_line_x_eb_order_ligne", columnList = "x_eb_order_ligne"),
    @Index(name = "idx_eb_del_livraison_line_x_ec_country_origin", columnList = "x_ec_country_origin")
})
public class EbLivraisonLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ebDelLivraisonLineNum;

    private Long idLivraisonLine;

    private String customsCode;

    private String batch;

    private String refArticle;

    private String articleName;

    private Long quantity;

    private Date dateAvailability;

    @Column(name = "delete_flag", nullable = true)
    private Boolean deleteFlag;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country_origin")
    private EcCountry xEcCountryOrigin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_del_livraison")
    private EbLivraison xEbDelLivraison;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_order_ligne")
    private EbOrderLine xEbOrderLigne;

    private String partNumber;
    private String serialNumber;
    private Date dateOfRequest;
    private Integer priority;
    private String comment;
    private String specification;

    @Column(columnDefinition = "TEXT")
    private String customFields;

    // champs reservé pour customFields
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCategorie> listCategories;

    @Column(columnDefinition = "text")
    private String listLabels;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_marchandise")
    private EbMarchandise ebMarchandise;

    private String itemName;

    private String itemNumber;

    private Double weight;

    private Double length;

    private Double width;

    private String hsCode;

    private Integer height;

    private Double volume;

    @Temporal(TemporalType.DATE)
    private Date targetDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_type_unit")
    private EbTypeUnit xEbtypeOfUnit;

    private Boolean serialized;

    private String unitType;

    private Double price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency")
    private EcCurrency currency;

    private String eccn;

    public EbLivraisonLine() {
    }

    @QueryProjection
    public EbLivraisonLine(
        Long ebDelLivraisonLineNum,
        String refDelivery,
        String numOrderSAP,
        String refTransport,
        String numOrderCustomer,
        String refClientDelivered,
        String nameClientDelivered,
        String countryClientDelivered,
        String orderCustomerCode,
        String orderCustomerName,
        String campaignCode,
        String campaignName,
        Date orderCreationDate,
        Date expectedDeliveryDate,
        Date suggestedDeliveryDate,
        Date deliveryCreationDate,
        Date estimatedShipDate,
        Date effectiveShipDate,
        String pickupSite,
        Date endOfPackDate,
        Date pickDate,
        String invoiceNum,
        Date invoiceDate,
        StatusDelivery statusDelivery,
        Integer transportStatus,
        Long numParcels,
        Double totalWeight,
        Double totalVolume,
        String crossDock,
        String refArticle,
        String articleName,
        String countryOriginLibelle,
        String customsCode,
        String batch,
        Long quantity,
        String partNumber,
        String serialNumber,
        Date dateOfRequest,
        Integer priority,
        String comment,
        String specification,
        List<EbCategorie> listCategories,
        String customFields,
        Double weight,
        Double width,
        Double length,
        String hsCode,
        Integer height,
        Double volume,
        String itemName,
        String itemNumber,
        Date targetDate,
        Boolean serialized

    ) {
        this.xEbDelLivraison = new EbLivraison();
        this.xEbDelLivraison.setRefDelivery(refDelivery);
        this.xEbDelLivraison.setNumOrderSAP(numOrderSAP);
        this.xEbDelLivraison.setRefTransport(refTransport);
        this.xEbDelLivraison.setNumOrderCustomer(numOrderCustomer);
        this.xEbDelLivraison.setRefClientDelivered(refClientDelivered);
        this.xEbDelLivraison.setNameClientDelivered(nameClientDelivered);
        this.xEbDelLivraison.setCountryClientDelivered(countryClientDelivered);
        this.xEbDelLivraison.setOrderCustomerCode(orderCustomerCode);
        this.xEbDelLivraison.setOrderCustomerName(orderCustomerName);
        this.xEbDelLivraison.setCampaignCode(campaignCode);
        this.xEbDelLivraison.setCampaignName(campaignName);
        this.xEbDelLivraison.setOrderCreationDate(orderCreationDate);
        this.xEbDelLivraison.setExpectedDeliveryDate(expectedDeliveryDate);
        this.xEbDelLivraison.setSuggestedDeliveryDate(suggestedDeliveryDate);
        this.xEbDelLivraison.setDeliveryCreationDate(deliveryCreationDate);
        this.xEbDelLivraison.setEstimatedShipDate(estimatedShipDate);
        this.xEbDelLivraison.setEffectiveShipDate(effectiveShipDate);
        this.xEbDelLivraison.setPickupSite(pickupSite);
        this.xEbDelLivraison.setEndOfPackDate(endOfPackDate);
        this.xEbDelLivraison.setPickDate(pickDate);
        this.xEbDelLivraison.setInvoiceNum(invoiceNum);
        this.xEbDelLivraison.setInvoiceDate(invoiceDate);
        this.xEbDelLivraison.setStatusDelivery(statusDelivery);
        this.xEbDelLivraison.setTransportStatus(transportStatus);
        this.xEbDelLivraison.setNumParcels(numParcels);
        this.xEbDelLivraison.setTotalWeight(totalWeight);
        this.xEbDelLivraison.setTotalVolume(totalVolume);
        this.xEbDelLivraison.setCrossDock(crossDock);
        this.xEbDelLivraison.setCountryOriginLibelle(countryOriginLibelle);

        this.refArticle = refArticle;
        this.articleName = articleName;

        this.customsCode = customsCode;
        this.ebDelLivraisonLineNum = ebDelLivraisonLineNum;
        this.batch = batch;
        this.quantity = quantity;
        this.partNumber = partNumber;
        this.serialNumber = serialNumber;
        this.dateOfRequest = dateOfRequest;
        this.priority = priority;
        this.comment = comment;
        this.customFields = customFields;
        this.listCategories = listCategories;

        if (customFields != null) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                this.listCustomsFields = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        this.weight = weight;
        this.width = width;
        this.length = length;
        this.hsCode = hsCode;
        this.height = height;
        this.volume = volume;
        this.itemName = itemName;
        this.itemNumber = itemNumber;
        this.targetDate = targetDate;
        this.serialized = serialized;
    }

    @QueryProjection
    public EbLivraisonLine(
        Long ebDelLivraisonLineNum,
        String refDelivery,
        String numOrderSAP,
        String refTransport,
        String numOrderCustomer,
        String refClientDelivered,
        String nameClientDelivered,
        String countryClientDelivered,
        String orderCustomerCode,
        String orderCustomerName,
        String campaignCode,
        String campaignName,
        Date orderCreationDate,
        Date expectedDeliveryDate,
        Date suggestedDeliveryDate,
        Date deliveryCreationDate,
        Date estimatedShipDate,
        Date effectiveShipDate,
        String pickupSite,
        Date endOfPackDate,
        Date pickDate,
        String invoiceNum,
        Date invoiceDate,
        StatusDelivery statusDelivery,
        Integer transportStatus,
        Long numParcels,
        Double totalWeight,
        Double totalVolume,
        String crossDock,
        String refArticle,
        String articleName,
        String countryOriginLibelle,
        String customsCode,
        String batch,
        Long quantity,
        String partNumber,
        String serialNumber,
        Date dateOfRequest,
        Integer priority,
        String comment,
        String specification,
        List<EbCategorie> listCategories,
        String customFields,
        Double weight,
        Double width,
        Double length,
        String hsCode,
        Integer height,
        Double volume,
        String itemName,
        String itemNumber,
        Date targetDate,
        Boolean serialized,
        Double price,
        String eccn,
        Integer ecCurrencyNum

    ) {
        this(
            ebDelLivraisonLineNum,
            refDelivery,
            numOrderSAP,
            refTransport,
            numOrderCustomer,
            refClientDelivered,
            nameClientDelivered,
            countryClientDelivered,
            orderCustomerCode,
            orderCustomerName,
            campaignCode,
            campaignName,
            orderCreationDate,
            expectedDeliveryDate,
            suggestedDeliveryDate,
            deliveryCreationDate,
            estimatedShipDate,
            effectiveShipDate,
            pickupSite,
            endOfPackDate,
            pickDate,
            invoiceNum,
            invoiceDate,
            statusDelivery,
            transportStatus,
            numParcels,
            totalWeight,
            totalVolume,
            crossDock,
            refArticle,
            articleName,
            countryOriginLibelle,
            customsCode,
            batch,
            quantity,
            partNumber,
            serialNumber,
            dateOfRequest,
            priority,
            comment,
            specification,
            listCategories,
            customFields,
            weight,
            width,
            length,
            hsCode,
            height,
            volume,
            itemName,
            itemNumber,
            targetDate,
            serialized);
        this.price = price;
        this.eccn = eccn;

        if (ecCurrencyNum != null) {
            this.currency = new EcCurrency(ecCurrencyNum);
        }
    }

    public Long getIdLivraisonLine() {
        return idLivraisonLine;
    }

    public void setIdDeliveryLine(Long idDeliveryLine) {
        this.idLivraisonLine = idDeliveryLine;
    }

    public String getCustomsCode() {
        return customsCode;
    }

    public void setCustomsCode(String customsCode) {
        this.customsCode = customsCode;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getRefArticle() {
        return refArticle;
    }

    public void setRefArticle(String refProduct) {
        this.refArticle = refProduct;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public EcCountry getxEcCountryOrigin() {
        return xEcCountryOrigin;
    }

    public void setxEcCountryOrigin(EcCountry xEcCountryOrigin) {
        this.xEcCountryOrigin = xEcCountryOrigin;
    }

    public EbLivraison getxEbDelLivraison() {

        if (xEbDelLivraison != null && xEbDelLivraison.getxEbDelLivraisonLine() != null) {

            for (EbLivraisonLine ebLivraisonLine: xEbDelLivraison.getxEbDelLivraisonLine()) {
                ebLivraisonLine.setxEbDelLivraison(null);
            }

        }

        return xEbDelLivraison;
    }

    public void setxEbDelLivraison(EbLivraison xEbDelLivraison) {
        this.xEbDelLivraison = xEbDelLivraison;
    }

    public void setIdLivraisonLine(Long idLivraisonLine) {
        this.idLivraisonLine = idLivraisonLine;
    }

    public Date getDateAvailability() {
        return dateAvailability;
    }

    public void setDateAvailability(Date dateAvailability) {
        this.dateAvailability = dateAvailability;
    }

    public Long getEbDelLivraisonLineNum() {
        return ebDelLivraisonLineNum;
    }

    public void setEbDelLivraisonLineNum(Long ebDelLivraisonLineNum) {
        this.ebDelLivraisonLineNum = ebDelLivraisonLineNum;
    }

    public EbOrderLine getxEbOrderLigne() {
        return xEbOrderLigne;
    }

    public void setxEbOrderLigne(EbOrderLine xEbOrderLigne) {
        this.xEbOrderLigne = xEbOrderLigne;
    }

    public Boolean getDeleteFlag() {
        return deleteFlag;
    }

    public void setDeleteFlag(Boolean deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Date getDateOfRequest() {
        return dateOfRequest;
    }

    public void setDateOfRequest(Date dateOfRequest) {
        this.dateOfRequest = dateOfRequest;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public List<EbCategorie> getListCategories() {

        if (listCategories != null && listCategories.size() > 0) {

            try {
                this.listCategories = ObjectDeserializer
                    .checkJsonListObject(listCategories, new TypeToken<List<EbCategorie>>() {
                    }.getType());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {

        if (listCategories != null) {
            this.listCategories = listCategories
                .stream().map(it -> EbCategorie.getEbCategorieLite(it)).collect(Collectors.toList());

            // pour deserialiser la liste et eviter l'erreur linked hashMap
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String jsonCateg = objectMapper.writeValueAsString(listCategories);
                this.listCategories = objectMapper.readValue(jsonCateg, new TypeReference<ArrayList<EbCategorie>>() {
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (this.listCategories != null && this.listCategories.size() > 0) {
                this.listLabels = "";

                for (EbCategorie cat: this.listCategories) {

                    if (cat.getLabels() != null && cat.getLabels().size() > 0) {

                        for (EbLabel lab: cat.getLabels()) {
                            // le cas de plusieurs libelles
                            if (!this.listLabels.isEmpty()
                                && this.listLabels.charAt(this.listLabels.length() - 1) != ',') this.listLabels += ",";

                            this.listLabels += lab.getEbLabelNum();
                        }

                    }

                    if (this.listLabels.isEmpty()) {
                        this.listLabels = null;
                    }
                    else {
                        // le trie des labels est naicessaire pour la partie
                        // groupage
                        List<Integer> listLabelsArray = Arrays
                            .asList(this.listLabels.split(",")).stream().map(Integer::parseInt).sorted()
                            .collect(Collectors.toList());
                        this.listLabels = StringUtils.join(listLabelsArray, ",");
                    }

                }

            }
            else {
                this.listCategories = listCategories;
            }

        }

    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;

        if (this.customFields != null && !this.customFields.isEmpty()) {
            List<CustomFields> listCustomFieldsDeserialized = null;
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                listCustomFieldsDeserialized = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

            for (CustomFields field: listCustomFieldsDeserialized) {
                if (field.getValue() == null || field.getValue().isEmpty() || field.getName() == null
                    || field.getName().isEmpty()) continue;
            }

        }

    }

    public String getListLabels() {
        return listLabels;
    }

    public void setListLabels(String listLabels) {
        this.listLabels = listLabels;
    }

    public EbMarchandise getEbMarchandise() {
        return ebMarchandise;
    }

    public void setEbMarchandise(EbMarchandise ebMarchandise) {
        this.ebMarchandise = ebMarchandise;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public EbTypeUnit getxEbtypeOfUnit() {
        return xEbtypeOfUnit;
    }

    public void setxEbtypeOfUnit(EbTypeUnit xEbtypeOfUnit) {
        this.xEbtypeOfUnit = xEbtypeOfUnit;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Boolean getSerialized() {
        return serialized;
    }

    public void setSerialized(Boolean serialized) {
        this.serialized = serialized;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public EcCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(EcCurrency currency) {
        this.currency = currency;
    }

    public String getEccn() {
        return eccn;
    }

    public void setEccn(String eccn) {
        this.eccn = eccn;
    }
}
