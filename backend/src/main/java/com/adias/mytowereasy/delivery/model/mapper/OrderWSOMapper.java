package com.adias.mytowereasy.delivery.model.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.adias.mytowereasy.api.service.CategoryApiService;
import com.adias.mytowereasy.api.service.CustomFieldsApiService;
import com.adias.mytowereasy.api.service.TransportApiService;
import com.adias.mytowereasy.api.wso.AddressWSO;
import com.adias.mytowereasy.api.wso.OrderLineWSO;
import com.adias.mytowereasy.api.wso.OrderWSO;
import com.adias.mytowereasy.delivery.dao.impl.DaoOrderImpl;
import com.adias.mytowereasy.delivery.enumeration.StatusOrder;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.service.OrderService;
import com.adias.mytowereasy.dto.EbOrderDto;
import com.adias.mytowereasy.dto.mapper.DeliveryMapper;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.*;
import com.adias.mytowereasy.util.Constants;
import com.adias.mytowereasy.util.CustomFieldsMapper;
import com.adias.mytowereasy.util.JsonUtils;
import com.adias.mytowereasy.util.enums.GenericEnumUtils;


@Component
public class OrderWSOMapper {
    @Autowired
    EbUserRepository ebUserRepository;

    @Autowired
    TransportApiService transportApiService;

    @Autowired
    CustomFieldsApiService customFieldsApiService;

    @Autowired
    CategoryApiService categoryApiService;

    @Autowired
    EbIncotermRepository ebIncotermRepository;

    @Autowired
    EcCountryRepository ecCountryRepository;

    @Autowired
    EbTypeUnitRepository ebTypeUnitRepository;

    @Autowired
    EcCurrencyRepository ecCurrencyRepository;

    @Autowired
    DeliveryMapper deliveryMapper;

    @Autowired
    DaoOrderImpl daoOrder;

    @Autowired
    OrderService orderService;

    public OrderWSO getWSOFromEntity(EbOrder entity) {
        entity.setListCustomsFields(CustomFieldsMapper.toModel(entity.getCustomFields()));
        OrderWSO wso = deliveryMapper.ebOrderToOrderWSO(entity);

        if (entity.getxEbPartyOrigin() != null) {

            if (wso.getFrom() == null) {
                wso.setFrom(new AddressWSO());
            }

            AddressWSO party = transportApiService.convertEbPartyToAddresseWSO(entity.getxEbPartyOrigin());
            wso.setFrom(party);
        }

        if (entity.getxEbPartyDestination() != null) {

            if (wso.getTo() == null) {
                wso.setTo(new AddressWSO());
            }

            AddressWSO party = transportApiService.convertEbPartyToAddresseWSO(entity.getxEbPartyDestination());
            wso.setTo(party);
        }

        if (entity.getxEbPartySale() != null) {

            if (wso.getSoldTo() == null) {
                wso.setSoldTo(new AddressWSO());
            }

            AddressWSO party = transportApiService.convertEbPartyToAddresseWSO(entity.getxEbPartySale());
            wso.setSoldTo(party);
        }

        if (entity.getxEbPartyVendor() != null) {
            AddressWSO party = transportApiService.convertEbPartyToAddresseWSO(entity.getxEbPartyVendor());
            wso.setVendor(party);
        }

        if (entity.getxEbPartyIssuer() != null) {
            AddressWSO party = transportApiService.convertEbPartyToAddresseWSO(entity.getxEbPartyIssuer());
            wso.setIssuer(party);
        }

        // Lines
        if (entity.getOrderLines() != null) {
            wso.setLines(new ArrayList<>());

            for (EbOrderLine eLine: entity.getOrderLines()) {
                eLine.setListCustomsFields(CustomFieldsMapper.toModel(eLine.getCustomFields()));
                OrderLineWSO wLine = deliveryMapper.ebOrderLineToOrderLineWSO(eLine);
                wso.getLines().add(wLine);
            }

        }

        String orderStatus = GenericEnumUtils
            .getKeyByCode(StatusOrder.class, entity.getOrderStatus())
            .orElseThrow(() -> new MyTowerException("Order Status with key " + wso.getOrderStatus() + " not found"));

        wso.setOrderStatus(orderStatus);

        return wso;
    }

    public OrderWSO getWSOFromDto(EbOrderDto order) {
        return getWSOFromEntity(deliveryMapper.ebOrderDtoToEbOrder(order));
    }

    public EbOrder fillEntityFromWSO(EbOrder entity, OrderWSO wso) {
        EbUser requestOwner = ebUserRepository.findOneByEmailIgnoreCase(wso.getRequestOwnerEmail());
        if (requestOwner
            == null) throw new MyTowerException("User with email " + wso.getRequestOwnerEmail() + " not found");
        entity.setxEbOwnerOfTheRequest(requestOwner);

        if (wso.getEtablissementId() != null) {
            entity.setxEbEtablissement(new EbEtablissement(wso.getEtablissementId()));
        }

        if (wso.getCompanyId() != null) {
            entity.setxEbCompagnie(new EbCompagnie(wso.getCompanyId()));
        }
        else {
            entity.setxEbCompagnie(new EbCompagnie(requestOwner.getEbCompagnie().getEbCompagnieNum()));
        }

        Long orderId = wso.getItemId();
        if (Objects.isNull(orderId)) {
            orderId = daoOrder.nextValOrder();
        }

        entity.setEbDelOrderNum(orderId);
        entity
            .setReference(
                orderService
                    .generateReference(requestOwner.getEbCompagnie().getCode(), orderId, Constants.refOrderPrefix));
        entity.setCustomerCreationDate(wso.getCustomerCreationDate());

        if (wso.getFrom() == null) {
            throw new MyTowerException("Party Origin is null");
        }

        if (entity.getxEbPartyOrigin() == null) {
            entity.setxEbPartyOrigin(new EbParty());
        }

        EbParty partyOrigin = transportApiService.fillEbPartyFromAddressWSO(entity.getxEbPartyOrigin(), wso.getFrom());
        entity.setxEbPartyOrigin(partyOrigin);

        if (wso.getTo() == null) {
            throw new MyTowerException("Party Destination in null");
        }

        if (entity.getxEbPartyDestination() == null) {
            entity.setxEbPartyDestination(new EbParty());
        }

        EbParty partyDestination = transportApiService
            .fillEbPartyFromAddressWSO(entity.getxEbPartyDestination(), wso.getTo());
        entity.setxEbPartyDestination(partyDestination);

        if (wso.getSoldTo() != null) {

            if (entity.getxEbPartySale() == null) {
                entity.setxEbPartySale(new EbParty());
            }

            EbParty party = transportApiService.fillEbPartyFromAddressWSO(entity.getxEbPartySale(), wso.getSoldTo());
            entity.setxEbPartySale(party);
        }

        if (wso.getVendor() != null) {

            if (entity.getxEbPartyVendor() == null) {
                entity.setxEbPartyVendor(new EbParty());
            }

            EbParty party = transportApiService.fillEbPartyFromAddressWSO(entity.getxEbPartyVendor(), wso.getVendor());
            entity.setxEbPartyVendor(party);
        }

        if (wso.getIssuer() != null) {

            if (entity.getxEbPartyIssuer() == null) {
                entity.setxEbPartyIssuer(new EbParty());
            }

            EbParty party = transportApiService.fillEbPartyFromAddressWSO(entity.getxEbPartyIssuer(), wso.getIssuer());
            entity.setxEbPartyIssuer(party);
        }

        entity.setTargetDate(wso.getTargetDate());

        // Custom fields
        if (wso.getCustomFieldList() != null) {
            List<CustomFields> listCustomField = customFieldsApiService
                .generateCustomFieldsFromWSO(wso.getCustomFieldList(), entity.getxEbCompagnie().getEbCompagnieNum());
            entity.setCustomFields(JsonUtils.serializeToString(listCustomField, null));
            entity.setListCustomsFields(listCustomField);
        }

        // Categories
        if (wso.getCategoryList() != null) {
            List<EbCategorie> listCategorieFinal = categoryApiService
                .generateCategoriesFromWSO(wso.getCategoryList(), entity.getxEbCompagnie().getEbCompagnieNum());
            entity.setListCategories(listCategorieFinal);
        }

        // Lines
        entity.setOrderLines(new ArrayList<>());

        if (wso.getLines() != null) {

            for (OrderLineWSO lWSO: wso.getLines()) {
                EbOrderLine lEntity = fillLineEntityFromWSO(
                    new EbOrderLine(),
                    lWSO,
                    entity.getxEbCompagnie() != null ? entity.getxEbCompagnie().getEbCompagnieNum() : null);
                entity.getOrderLines().add(lEntity);
            }

        }

        entity.setPriority(wso.getPriority());
        entity.setCustomerOrderReference(wso.getCustomerOrderReference());
        entity.setComplementaryInformations(wso.getComplementaryInformations());

        if (wso.getIncoterm() != null) {
            EbIncoterm ebIncoterm = ebIncotermRepository
                .findFirstByLibelleAndXEbCompagnie_ebCompagnieNum(
                    wso.getIncoterm(),
                    entity.getxEbCompagnie().getEbCompagnieNum());

            if (ebIncoterm == null) throw new MyTowerException("Incoterm " + wso.getIncoterm() + " not found");
            else {
                entity.setxEbIncotermNum(ebIncoterm.getEbIncotermNum());
                entity.setxEcIncotermLibelle(ebIncoterm.getLibelle());
            }

        }

        Integer orderStatus = GenericEnumUtils
            .getCodeByKey(StatusOrder.class, wso.getOrderStatus())
            .orElseThrow(() -> new MyTowerException("Order Status with key " + wso.getOrderStatus() + " not found"));

        entity.setOrderStatus(orderStatus);

        return entity;
    }

    public EbOrderLine fillLineEntityFromWSO(EbOrderLine entity, OrderLineWSO wso, Integer orderCompagnieNum) {
        entity.setEbDelOrderLineNum(wso.getItemId());
        entity.setQuantityOrdered(wso.getQuantityOrdered());

        if (wso.getOriginCountryCode() != null) {
            EcCountry country = ecCountryRepository.findFirstByCode(wso.getOriginCountryCode());
            entity.setxEcCountryOrigin(country);
        }

        entity.setHeight(wso.getHeight());
        entity.setPartNumber(wso.getPartNumber());
        entity.setSerialNumber(wso.getSerialNumber());
        entity.setPriority(wso.getPriority());
        entity.setComment(wso.getComment());
        entity.setSpecification(wso.getSpecification());

        if (wso.getCustomFieldList() != null) {
            List<CustomFields> listCustomField = customFieldsApiService
                .generateCustomFieldsFromWSO(wso.getCustomFieldList(), orderCompagnieNum);
            entity.setCustomFields(JsonUtils.serializeToString(listCustomField, null));
            entity.setListCustomsFields(listCustomField);
        }

        if (wso.getCategoryList() != null) {
            List<EbCategorie> listCategorieFinal = categoryApiService
                .generateCategoriesFromWSO(wso.getCategoryList(), orderCompagnieNum);
            entity.setListCategories(listCategorieFinal);
        }

        entity.setTargetDate(wso.getTargetDate());
        entity.setWeight(wso.getWeight());
        entity.setWidth(wso.getWidth());
        entity.setHsCode(wso.getHsCode());
        entity.setLength(wso.getLength());
        entity.setItemName(wso.getItemName());
        entity.setItemNumber(wso.getItemNumber());
        entity.setPrice(wso.getPrice());
        entity.setEccn(wso.getEccn());

        if (wso.getPrice() != null && wso.getCurrencyCode() == null) {
            throw new MyTowerException("Currency Code is null");
        }
				Optional<EcCurrency> currency = null;

        if (wso.getCurrencyCode() != null) {
            currency = ecCurrencyRepository.findByCodeIgnoreCase(wso.getCurrencyCode().toLowerCase());

						if (!currency.isPresent())
							throw new MyTowerException(
								"currency with code " + wso.getCurrencyCode() + " not found"
							);
        }

					entity.setCurrency(currency.get());

        if (wso.getUnitType() != null) {
            EbTypeUnit typeUnit = ebTypeUnitRepository.findFirstByCode(wso.getUnitType());
            entity.setxEbtypeOfUnit(typeUnit);
        }

        return entity;
    }
}
