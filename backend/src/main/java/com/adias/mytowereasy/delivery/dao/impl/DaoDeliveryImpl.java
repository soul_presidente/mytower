package com.adias.mytowereasy.delivery.dao.impl;

import java.math.BigInteger;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.CaseBuilder;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.delivery.dao.DaoDelivery;
import com.adias.mytowereasy.delivery.dao.DaoVisibility;
import com.adias.mytowereasy.delivery.model.*;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.service.FlagService;


@Component
@Transactional
public class DaoDeliveryImpl implements DaoDelivery {
    @PersistenceContext
    EntityManager em;

    @Autowired
    FlagService flagService;

    @Autowired
    EbLivraisonRepository ebLivraisonRepository;

    @Autowired
    DaoVisibility daoVisibility;

    QEbLivraison qEbLivraison = QEbLivraison.ebLivraison;
    QEbLivraisonLine qEbLivraisonLine = QEbLivraisonLine.ebLivraisonLine;
    QEbLivraison qLivraisonFromLine = new QEbLivraison("qLivraisonFromLine");
    QEbParty qEbPartyDest = new QEbParty("qEbPartyDest");

    QEbParty qEbPartyOrigin = new QEbParty("qEbPartyOrigin");
    QEcCountry qEcCountryOrigin = new QEcCountry("qEcCountryOrigin");
    QEbOrder qEbOrder = new QEbOrder("qEbOrder");
    QEbUser qxEbOwnerOfTheRequest = new QEbUser("qxEbOwnerOfTheRequest");

    QEbParty qEPartySale = new QEbParty("qEPartySale");

    QEcCountry qxEcCountryOrigin = new QEcCountry("qxEcCountryOrigin");
    QEcCountry qxEcCountryDest = new QEcCountry("qxEcCountryDest");
    QEcCountry qxEcCountrySale = new QEcCountry("qxEcCountrySale");
    QEbCompagnie qEbCompagnie = QEbCompagnie.ebCompagnie;

    BooleanBuilder where = new BooleanBuilder();
    StringExpression stringOrderWithExpression = qEbLivraison.refDelivery;

    private Map<String, Expression> autoCompleteFields = new HashMap<String, Expression>();

    public DaoDeliveryImpl() {
        initialize();
    }

    private void initialize() {
        autoCompleteFields.put("crossDock", qEbLivraison.crossDock);
        autoCompleteFields.put("refTransport", qEbLivraison.refTransport);
        autoCompleteFields.put("invoiceNum", qEbLivraison.invoiceNum);
        autoCompleteFields.put("ebDelReference", qEbLivraison.ebDelReference);
        autoCompleteFields.put("refArticle", qEbLivraisonLine.refArticle);
        autoCompleteFields.put("batch", qEbLivraisonLine.batch);
        autoCompleteFields.put("ebLivraisonNum", qEbLivraison.ebDelLivraisonNum);
        autoCompleteFields.put("numOrderSAP", qEbLivraison.numOrderSAP);
        autoCompleteFields.put("numOrderEDI", qEbLivraison.numOrderEDI);
        autoCompleteFields.put("countryDestination", qxEcCountryDest.libelle);
        autoCompleteFields.put("numOrderCustomer", qEbLivraison.numOrderCustomer);
        autoCompleteFields.put("campaignCode", qEbLivraison.campaignCode);
        autoCompleteFields.put("campaignName", qEbLivraison.campaignName);
        autoCompleteFields.put("orderCreationDate", qEbLivraison.orderCreationDate);
        autoCompleteFields.put("suggestedDeliveryDate", qEbLivraison.requestedDeliveryDate);
        autoCompleteFields.put("deliveryCreationDate", qEbLivraison.pickDate);
        autoCompleteFields.put("estimatedShipDate", qEbLivraison.estimatedShipDate);
        autoCompleteFields.put("effectiveShipDate", qEbLivraison.effectiveShipDate);
        autoCompleteFields.put("pickupSite", qEbLivraison.pickupSite);
        autoCompleteFields.put("endOfPackDate", qEbLivraison.endOfPackDate);
        autoCompleteFields.put("statusDelivery", qEbLivraison.statusDelivery);
        autoCompleteFields.put("transportStatus", qEbLivraison.transportStatus);
        autoCompleteFields.put("numParcels", qEbLivraison.numParcels);
        autoCompleteFields.put("orderCustomerName", qEbLivraison.orderCustomerName);
        autoCompleteFields.put("orderCustomerCode", qEbLivraison.orderCustomerCode);
        autoCompleteFields.put("nameClientDelivered", qEbPartyDest.company);
        autoCompleteFields.put("refClientDelivered", qEbPartyDest.reference);
        autoCompleteFields.put("customerOrderReference", qEbLivraison.customerOrderReference);
    }

    private void setListFlag(List<EbLivraison> result) {
        List<String> listIcon = new ArrayList<>();

        try {

            for (EbLivraison liv: result) {

                if (liv.getListFlag() != null && !liv.getListFlag().isEmpty()) {
                    List<String> listFlag = Arrays.asList(liv.getListFlag().split(","));

                    for (String flag: listFlag) {
                        boolean found = listIcon.stream().anyMatch(it -> it.contentEquals(flag));

                        if (!found) {
                            listIcon.add(flag);
                        }

                    }

                }

            }

            List<EbFlagDTO> listFlagDTO = flagService.getListFlagDTOFromCodes(String.join(",", listIcon));
            listFlagDTO = EbFlagDTO.sortByName(listFlagDTO);

            for (EbLivraison liv: result) {

                if (liv.getListFlag() != null && !liv.getListFlag().isEmpty()) {
                    List<String> listFlag = Arrays.asList(liv.getListFlag().split(","));

                    for (String flag: listFlag) {
                        Integer flagId = Integer.valueOf(flag);
                        EbFlagDTO found = listFlagDTO
                            .stream().filter(it -> it.getEbFlagNum().equals(flagId)).findFirst().orElse(null);

                        if (found != null) {
                            if (liv.getListEbFlagDTO() == null) liv.setListEbFlagDTO(new ArrayList<>());
                            liv.getListEbFlagDTO().add(found);
                        }

                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // cette methode contient les tables des jointures utilise sur la selection
    // et count
    private JPAQuery<EbLivraison> queryJoin(JPAQuery<EbLivraison> query, SearchCriteriaDelivery criteria) {
        query.leftJoin(qEbLivraison.xEbPartyDestination(), qEbPartyDest);
        query.leftJoin(qEbPartyDest.xEcCountry(), qxEcCountryDest);

        query.leftJoin(qEbLivraison.xEbPartyOrigin(), qEbPartyOrigin);
        query.leftJoin(qEbPartyOrigin.xEcCountry(), qxEcCountryOrigin);

        query.leftJoin(qEbLivraison.xEbPartySale(), qEPartySale);
        query.leftJoin(qEPartySale.xEcCountry(), qxEcCountrySale);
        query.leftJoin(qEbLivraison.xEbOwnerOfTheRequest(), qxEbOwnerOfTheRequest);
        query.leftJoin(qEbLivraison.xEbPartyDestination(), qEbPartyDest);
        query.leftJoin(qEbPartyDest.xEcCountry(), qxEcCountryDest);
        query.innerJoin(qEbLivraison.xEbDelOrder(), qEbOrder);

        if (StringUtils.isNotBlank(criteria.getSearchterm())) {
            query.leftJoin(qEbLivraison.xEbDelLivraisonLine, qEbLivraisonLine);
        }

        return query;
    }

    @Override
    public EbLivraison saveLivraison(EbLivraison ebLivraison, EbUser ebUser) {

        if (ebUser != null) {
            ebLivraison.setxEbEtablissement(ebUser.getEbEtablissement());
            ebLivraison.setxEbCompagnie(ebUser.getEbCompagnie());
        }

        for (EbLivraisonLine ebLivraisonLine: ebLivraison.getxEbDelLivraisonLine()) {
            ebLivraisonLine.setxEbDelLivraison(ebLivraison);
        }

        EbLivraison ebLivraison2 = ebLivraisonRepository.save(ebLivraison);

        // ebLivraisonRepository
        // .updateQteAllocatedByEbDelOrderLineNum(ebLivraison2.getxEbDelOrder().getEbDelOrderNum());

        // ebLivraisonRepository
        // .updateQtePendingByEbDelOrderLineNum(ebLivraison2.getxEbDelOrder().getEbDelOrderNum());

        return ebLivraison2;
    }

    @SuppressWarnings("unchecked")
    @Override
    public EbLivraison getDelivery(SearchCriteriaDelivery criteria) {
        EbLivraison result = null;
        initialize();

        try {
            JPAQuery<EbLivraison> query = new JPAQuery<EbLivraison>(em);
            query
                .select(
                    QEbLivraison
                        .create(
                            qEbLivraison.ebDelLivraisonNum,
                            qEbLivraison.ebDelReference,
                            qEbLivraison.refDelivery,
                            qEbLivraison.numOrderSAP,
                            qEbLivraison.numOrderEDI,
                            qEbLivraison.numOrderCustomer,
                            qEbLivraison.orderCustomerCode,
                            qEbLivraison.orderCustomerName,
                            qEbLivraison.refTransport,
                            qEbLivraison.deliveryNote,
                            qEbLivraison.orderCreationDate,
                            qEbLivraison.expectedDeliveryDate,
                            qEbLivraison.campaignCode,
                            qEbLivraison.campaignName,
                            qEbLivraison.pickDate,
                            qEbLivraison.estimatedShipDate,
                            qEbLivraison.effectiveShipDate,
                            qEbLivraison.endOfPackDate,
                            qEbLivraison.pickupSite,
                            qEbLivraison.invoiceDate,
                            qEbPartyDest.reference,
                            qEbPartyDest.company,
                            qxEcCountryDest.libelle,
                            qEbLivraison.suggestedDeliveryDate,
                            qEbLivraison.deliveryCreationDate,
                            qEbLivraison.customerOrderReference,
                            qEbLivraison.transportStatus,
                            qEbLivraison.totalQuantityProducts,
                            qEbLivraison.numParcels,
                            qEbLivraison.totalWeight,
                            qEbLivraison.totalVolume,
                            qEbLivraison.invoiceNum,
                            qEbLivraison.pathToBl,
                            qEbLivraison.crossDock,
                            qEbLivraison.xEbDemande().ebDemandeNum,
                            qEbLivraison.xEbDelOrder().ebDelOrderNum,
                            qEbLivraison.customFields,
                            qEbLivraison.listCategories,
                            qEbLivraison.listLabels,
                            qEbLivraison.statusDelivery,
                            qxEbOwnerOfTheRequest.ebUserNum,
                            qxEbOwnerOfTheRequest.nom,
                            qxEbOwnerOfTheRequest.prenom,
                            qEbLivraison.xEbIncotermNum,
                            qEbLivraison.xEcIncotermLibelle,
                            qEbLivraison.complementaryInformations,

                            qEbPartyOrigin.ebPartyNum,
                            qEbPartyOrigin.company,
                            qEbPartyOrigin.adresse,
                            qEbPartyOrigin.city,
                            qxEcCountryOrigin.ecCountryNum,
                            qxEcCountryOrigin.libelle,

                            qEbPartyDest.ebPartyNum,
                            qEbPartyDest.company,
                            qEbPartyDest.adresse,
                            qEbPartyDest.city,
                            qxEcCountryDest.ecCountryNum,
                            qxEcCountryDest.libelle,

                            qEPartySale.ebPartyNum,
                            qEPartySale.company,
                            qEPartySale.adresse,
                            qEPartySale.city,
                            qxEcCountrySale.ecCountryNum,
                            qxEcCountrySale.libelle,
                            qEbLivraison.dateCreationSys,

                            stringOrderWithExpression.as("stringOrderExpression"),

                            qEbLivraison.refOrder,
                            qEbLivraison.dateOfGoodsAvailability,
                            qEbLivraison.xEbDelOrder(),
                            qEbLivraison.customerDeliveryDate));

            criteria.setqPartyDest(qEbPartyDest);
            criteria.setqPartyOrigin(qEbPartyOrigin);

            query.from(qEbLivraison);
            query = criteria.applyCriteria(query, true);

            query = queryJoin(query, criteria);
            result = query.fetchFirst();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getDeliveryCount(SearchCriteriaDelivery criteria, EbUser currentUser) {
        Long result = 0L;

        try {
            JPAQuery<EbLivraison> query = new JPAQuery<EbLivraison>(em);
            query.from(qEbLivraison);
            query = queryJoin(query, criteria);
            criteria.setCount(true);
            query = criteria.applyCriteria(query, false);
            query = daoVisibility.addVisibilityToDelivery(query, criteria, currentUser);
            result = query.distinct().fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EbLivraison> getAutoCompleteList(String term, String field, EbUser currentUser) {
        List<String> result = null;
        List<EbLivraison> listResult = new ArrayList<>();

        try {
            SearchCriteriaDelivery criteria = new SearchCriteriaDelivery();

            criteria = criteria.setProperty(field, term, criteria);
            JPAQuery query = new JPAQuery<>(em);
            query.select(autoCompleteFields.get(field));
            query.from(qEbLivraison);
            query = queryJoin(query, criteria);
            query = criteria.applyCriteriaAutoComplete(query);
            query = daoVisibility.addVisibilityToDelivery(query, criteria, currentUser);

            query.distinct();
            query.limit(500);
            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (result != null) {

            for (String value: result) {
                EbLivraison liv = new EbLivraison();
                liv.setProperty(field, value);
                listResult.add(liv);
            }

        }

        return listResult;
    }

    Path<String> stringOrderAlias = ExpressionUtils.path(String.class, "stringOrderExpression");

    @SuppressWarnings("unchecked")
    @Override
    public List<EbLivraison> getListDeliveries(SearchCriteriaDelivery criteria, EbUser currentUser) {
        List<EbLivraison> result = null;

        try {
            JPAQuery<EbLivraison> query = new JPAQuery<EbLivraison>(em);
            String fieldType = null;

            if (criteria != null) {
                query.distinct();

                if (criteria.getOrderedColumn() != null) {
                    String columnName = criteria.getOrderedColumn();
                    StringExpression fieldColumn = null;
                    NumberExpression indexOfCategExpr = null;

                    if (columnName != null && columnName.matches("^field[0-9]+$")) {
                        fieldType = "field";
                        fieldColumn = qEbLivraison.listCustomFieldsFlat;
                        indexOfCategExpr = fieldColumn.indexOf(columnName + "\":").add(columnName.length() + 2 + 2);
                    }

                    if (fieldType != null && fieldColumn != null) {
                        StringExpression restStringExpr = fieldColumn.substring(indexOfCategExpr);
                        NumberExpression endIndexExpr = restStringExpr.indexOf("\"");

                        /*
                         * quand on Ã©crie dans querydsl: substring(a, b) cela
                         * se traduit par
                         * substring(a+1, b-a) dans sql donc on doit diminuer 1
                         * de l'expr de gauche et
                         * on ajoute 'a' dans l'expr de droite
                         */
                        stringOrderWithExpression = new CaseBuilder()
                            .when(
                                new BooleanBuilder()
                                    .orAllOf(fieldColumn.isNotNull(), fieldColumn.indexOf(columnName).goe(0)))
                            .then(
                                fieldColumn
                                    .substring(
                                        indexOfCategExpr.subtract(1), // diminuer
                                                                      // 1
                                        endIndexExpr.add(indexOfCategExpr) // rajouter
                                                                           // l'expr
                                                                           // de
                                                                           // gauche
                                                                           // 'a'
                                    )).otherwise(Expressions.nullExpression());

                        stringOrderWithExpression = new CaseBuilder()
                            .when(
                                new BooleanBuilder()
                                    .orAllOf(
                                        stringOrderWithExpression.isNotNull(),
                                        stringOrderWithExpression.isNotEmpty()))
                            .then(stringOrderWithExpression).otherwise(Expressions.nullExpression());
                    }

                }

            }

            query
                .select(
                    QEbLivraison
                        .create(
                            qEbLivraison.ebDelLivraisonNum,
                            qEbLivraison.ebDelReference,
                            qEbLivraison.refDelivery,
                            qEbLivraison.numOrderSAP,
                            qEbLivraison.numOrderEDI,
                            qEbLivraison.numOrderCustomer,
                            qEbLivraison.orderCustomerCode,
                            qEbLivraison.orderCustomerName,
                            qEbLivraison.refTransport,
                            qEbLivraison.deliveryNote,
                            qEbLivraison.orderCreationDate,
                            qEbLivraison.expectedDeliveryDate,
                            qEbLivraison.campaignCode,
                            qEbLivraison.campaignName,
                            qEbLivraison.pickDate,
                            qEbLivraison.estimatedShipDate,
                            qEbLivraison.effectiveShipDate,
                            qEbLivraison.endOfPackDate,
                            qEbLivraison.pickupSite,
                            qEbLivraison.invoiceDate,
                            qEbPartyDest.reference,
                            qEbPartyDest.company,
                            qxEcCountryDest.libelle,
                            qEbLivraison.suggestedDeliveryDate,
                            qEbLivraison.deliveryCreationDate,
                            qEbLivraison.customerOrderReference,
                            qEbLivraison.transportStatus,
                            qEbLivraison.totalQuantityProducts,
                            qEbLivraison.numParcels,
                            qEbLivraison.totalWeight,
                            qEbLivraison.totalVolume,
                            qEbLivraison.invoiceNum,
                            qEbLivraison.pathToBl,
                            qEbLivraison.crossDock,
                            qEbLivraison.xEbDemande().ebDemandeNum,
                            qEbLivraison.xEbDelOrder().ebDelOrderNum,
                            qEbLivraison.customFields,
                            qEbLivraison.listCategories,
                            qEbLivraison.listLabels,
                            qEbLivraison.statusDelivery,
                            qxEbOwnerOfTheRequest.ebUserNum,
                            qxEbOwnerOfTheRequest.nom,
                            qxEbOwnerOfTheRequest.prenom,
                            qEbLivraison.xEbIncotermNum,
                            qEbLivraison.xEcIncotermLibelle,
                            qEbLivraison.complementaryInformations,

                            qEbPartyOrigin.ebPartyNum,
                            qEbPartyOrigin.company,
                            qEbPartyOrigin.adresse,
                            qEbPartyOrigin.city,
                            qxEcCountryOrigin.ecCountryNum,
                            qxEcCountryOrigin.libelle,

                            qEbPartyDest.ebPartyNum,
                            qEbPartyDest.company,
                            qEbPartyDest.adresse,
                            qEbPartyDest.city,
                            qxEcCountryDest.ecCountryNum,
                            qxEcCountryDest.libelle,

                            qEPartySale.ebPartyNum,
                            qEPartySale.company,
                            qEPartySale.adresse,
                            qEPartySale.city,
                            qxEcCountrySale.ecCountryNum,
                            qxEcCountrySale.libelle,
                            qEbLivraison.dateCreationSys,

                            stringOrderWithExpression.as("stringOrderExpression"),

                            qEbLivraison.refOrder,
                            qEbLivraison.dateOfGoodsAvailability,
                            qEbLivraison.xEbDelOrder(),
                            qEbLivraison.customerDeliveryDate));
            query.from(qEbLivraison);

            if (fieldType != null) {
                if (criteria.getAscendant()) query.orderBy(new OrderSpecifier(Order.ASC, stringOrderAlias).nullsLast());
                else query.orderBy(new OrderSpecifier(Order.DESC, stringOrderAlias).nullsLast());
            }

            query = queryJoin(query, criteria);
            query = criteria.applyCriteria(query, fieldType == null);
            query = daoVisibility.addVisibilityToDelivery(query, criteria, currentUser);
            query.distinct();
            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            setListFlag(result);
        }

        return result;
    }

    public Long nextValDelivery() {
        Long result = null;

        try {
            result = ((BigInteger) em
                .createNativeQuery("SELECT NEXTVAL('work.eb_del_livraison_eb_del_livraison_num_seq')")
                .getSingleResult()).longValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
