package com.adias.mytowereasy.delivery.dao;

import java.util.List;

import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.model.EbUser;


public interface DaoDelivery {
    public List<EbLivraison> getListDeliveries(SearchCriteriaDelivery criteria, EbUser currentUser);

    public EbLivraison getDelivery(SearchCriteriaDelivery criteria);

    public Long getDeliveryCount(SearchCriteriaDelivery criteria, EbUser currentUser);

    public abstract EbLivraison saveLivraison(EbLivraison ebLivraison, EbUser ebUser);

    public List<EbLivraison> getAutoCompleteList(String term, String field, EbUser currentUser);
}
