package com.adias.mytowereasy.delivery.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dao.DaoUser;
import com.adias.mytowereasy.delivery.dao.DaoDelivery;
import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.repository.EbLivraisonLineRepository;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.service.DeliveryService;
import com.adias.mytowereasy.delivery.service.OrderService;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.dto.mapper.DeliveryMapper;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbChatRepository;
import com.adias.mytowereasy.repository.EbPartyRepository;
import com.adias.mytowereasy.service.AdresseService;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class DeliveryServiceImpl implements DeliveryService {
    @Autowired
    DaoDelivery daoDelivery;

    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    EbLivraisonRepository ebLivraisonRepository;

    @Autowired
    EbLivraisonLineRepository ebLivraisonLineRepository;

    @Autowired
    DaoUser daoUser;

    @Autowired
    AdresseService adresseService;

    @Autowired
    OrderService orderService;

    @Autowired
    EbChatRepository ebChatRepositorty;

    @Autowired
    EbPartyRepository ebPartyRepository;

    @Autowired
    MessageSource messageSource;

    @Autowired
    DeliveryMapper deliveryMapper;

    @Override
    public List<EbLivraison> getListDeliveries(SearchCriteriaDelivery criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);
        return daoDelivery.getListDeliveries(criteria, connectedUser);
    }

    @Override
    public EbLivraison getDelivery(SearchCriteriaDelivery criteria) {
        return daoDelivery.getDelivery(criteria);
    }

    @Override
    public Integer getDeliveryCount(SearchCriteriaDelivery criteria) {
    	EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);
        long count = daoDelivery.getDeliveryCount(criteria, connectedUser);
        return (int) count;
    }

    @Override
    public EbLivraison saveLivraison(EbLivraison ebLivraison) {
        EbUser currentUser = connectedUserService.getCurrentUser();

        return daoDelivery.saveLivraison(ebLivraison, currentUser);
    }

    @Override
    public void historizeDeliveryAction(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        String username,
        String usernameFor) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        EbChat chat = new EbChat(connectedUser);
        chat.setUserName(connectedUser.getNomPrenom());
        chat.setDateCreation(new Date());
        chat.setModule(module);
        chat.setIdFiche(idFiche);
        chat.setIsHistory(true);
        chat.setIdChatComponent(chatCompId);
        String lang = connectedUser != null && connectedUser.getLanguage() != null ? connectedUser.getLanguage() : "en";
        Locale lcl = new Locale(lang);
        String action1 = messageSource.getMessage("tracing.message_rd.by", null, lcl);
        String action2 = messageSource.getMessage("tracing.message_rd.for", null, lcl);
        chat.setText(action + action1 + "  <b>" + username + "</b>");
        if (usernameFor != null) chat.setText(chat.getText() + " <b>" + action2 + " <b>" + usernameFor + "</b>");
        ebChatRepositorty.save(chat);
    }

    @Override
    public EbLivraison updateLivraison(EbLivraison ebLivraison) {
        EbLivraison livraisonEntity = ebLivraisonRepository.findById(ebLivraison.getEbDelLivraisonNum()).get();

        livraisonEntity.setDateOfGoodsAvailability(ebLivraison.getDateOfGoodsAvailability());
        ebPartyRepository.save(ebLivraison.getxEbPartyOrigin());
        livraisonEntity.setxEbPartyOrigin(ebLivraison.getxEbPartyOrigin());
        ebPartyRepository.save(ebLivraison.getxEbPartyDestination());
        livraisonEntity.setxEbPartyDestination(ebLivraison.getxEbPartyDestination());

        if (ebLivraison.getxEbPartySale() != null) {
            if (ebLivraison.getxEbPartySale().getxEcCountry() != null
                && ebLivraison.getxEbPartySale().getxEcCountry().getEcCountryNum() == null) ebLivraison
                    .getxEbPartySale().setxEcCountry(null);

            ebPartyRepository.save(ebLivraison.getxEbPartySale());
            livraisonEntity.setxEbPartySale(ebLivraison.getxEbPartySale());
        }

        EbUser connectedUser = connectedUserService.getCurrentUser();
        String lang = connectedUser != null && connectedUser.getLanguage() != null ? connectedUser.getLanguage() : "en";
        Locale lcl = new Locale(lang);
        String action = messageSource.getMessage("deliveryHasBeenUpdated", null, lcl);
        this
            .historizeDeliveryAction(
                ebLivraison.getEbDelLivraisonNum().intValue(),
                Enumeration.IDChatComponent.DELIVERY.getCode(),
                Enumeration.Module.DELIVERY_MANAGEMENT.getCode(),
                action,
                connectedUserService.getCurrentUser().getNomPrenom(),
                null);

        return ebLivraisonRepository.save(livraisonEntity);
    }

    @Override
    public List<EbLivraison> getAutoCompleteList(String term, String field) {
        EbUser currentUser = connectedUserService.getCurrentUser();
        return daoDelivery.getAutoCompleteList(term, field, currentUser);
    }

    @Override
    public void updateDeliveryEtablissement(TransfertUserObject transfertUserObjet) {
        ebLivraisonRepository
            .updateDeliveryEtablissementAndLabels(
                transfertUserObjet.getOldEtablissement(),
                transfertUserObjet.getNewEtablissement(),
                transfertUserObjet.getOldListLabels());
    }

    @Override
    public EbDemande preConsolidateDelivery(List<Long> listDeliveryForConsolidateNum) {
        EbDemande ebDemande = new EbDemande();
        ebDemande.setLivraisons(new ArrayList<EbLivraison>());
        List<EbMarchandise> listMarchandises = new ArrayList<EbMarchandise>();

        List<EbLivraison> livraisons = ebLivraisonRepository.findByEbDelLivraisonNumIn(listDeliveryForConsolidateNum);

        for (int i = 0; i < livraisons.size(); i++) {
            EbLivraison ebLivraison = livraisons.get(i);

            if (i == 0) {
                // First item, fill all values and remove them if conflict on
                // the next livraison
                ebDemande.setEbPartyOrigin(ebLivraison.getxEbPartyOrigin());
                ebDemande.getEbPartyOrigin().setZoneRef(ebLivraison.getxEbPartyOrigin().getZoneRef());
                ebDemande.setEbPartyDest(ebLivraison.getxEbPartyDestination());
                ebDemande.getEbPartyDest().setZoneRef(ebLivraison.getxEbPartyDestination().getZoneRef());
                ebDemande.setListCategories(ebLivraison.getListCategories());
                ebDemande.setCustomFields(ebLivraison.getCustomFields());
                ebDemande.setDateOfGoodsAvailability(ebLivraison.getDateOfGoodsAvailability());

                if (ebLivraison.getxEbOwnerOfTheRequest() != null) {
                    SearchCriteria criteriaUser = new SearchCriteria();
                    criteriaUser.setEbUserNum(ebLivraison.getxEbOwnerOfTheRequest().getEbUserNum());
                    EbUser owner = daoUser.findUser(criteriaUser);
                    ebDemande.setUser(owner);
                }

                // Reset party id to dissociate Livraison party and Demande
                // party
                ebDemande.getEbPartyOrigin().setEbPartyNum(null);
                ebDemande.getEbPartyDest().setEbPartyNum(null);
            }
            else {

                // Party
                if (!adresseService.areSameAddresses(ebDemande.getEbPartyOrigin(), ebLivraison.getxEbPartyOrigin())) {
                    ebDemande.setEbPartyOrigin(new EbParty());
                }

                if (!adresseService
                    .areSameAddresses(ebDemande.getEbPartyDest(), ebLivraison.getxEbPartyDestination())) {
                    ebDemande.setEbPartyDest(new EbParty());
                }

                if (ebDemande.getUser() != null) {

                    if (!ebDemande
                        .getUser().getEbUserNum().equals(ebLivraison.getxEbOwnerOfTheRequest().getEbUserNum())) {
                        ebDemande.setUser(null);
                    }

                }

                if (ebDemande.getDateOfGoodsAvailability() != null) {

                    if (!DateUtils
                        .isSameDay(ebDemande.getDateOfGoodsAvailability(), ebLivraison.getDateOfGoodsAvailability())) {
                        ebDemande.setDateOfGoodsAvailability(null);
                    }

                }

                // Champs paramétrables et catégories seront gérés plus tard
                // Je laisse le code en commentaire pour pouvoir reprendre le
                // travail à ce moment là
                /*
                 * if (ebDemande.getCustomFields() !=
                 * (ebLivraison.getCustomFields()))
                 * {
                 * ebLivraison.setCustomFields(null);
                 * }
                 * if (ebDemande.getListCategories() !=
                 * ebLivraison.getListCategories())
                 * {
                 * ebDemande.setListCategories(null);
                 * }
                 */
            }

            if (ebLivraison.getxEbDelLivraisonLine() != null) {

                for (EbLivraisonLine ebDelLivraisonLine: ebLivraison.getxEbDelLivraisonLine()) {
                    EbMarchandise ebMarchandise = new EbMarchandise();
                    if (ebDelLivraisonLine.getQuantity() != null) ebMarchandise
                        .setNumberOfUnits((int) (long) ebDelLivraisonLine.getQuantity());
                    if (ebDelLivraisonLine.getArticleName() != null) ebMarchandise
                        .setNomArticle(ebDelLivraisonLine.getArticleName());
                    if (ebDelLivraisonLine.getRefArticle() != null) ebMarchandise
                        .setPartNumber(ebDelLivraisonLine.getRefArticle());
                    if (ebDelLivraisonLine.getSerialNumber() != null) ebMarchandise
                        .setSerialNumber(ebDelLivraisonLine.getSerialNumber());
                    if (ebDelLivraisonLine.getPartNumber() != null) ebMarchandise
                        .setPartNumber(ebDelLivraisonLine.getPartNumber());
                    if (ebDelLivraisonLine.getHeight() != null) ebMarchandise
                        .setHeigth(ebDelLivraisonLine.getHeight().doubleValue());
                    if (ebDelLivraisonLine.getWeight() != null) ebMarchandise.setWeight(ebDelLivraisonLine.getWeight());
                    if (ebDelLivraisonLine.getWidth() != null) ebMarchandise.setWidth(ebDelLivraisonLine.getWidth());
                    if (ebDelLivraisonLine.getLength() != null) ebMarchandise.setLength(ebDelLivraisonLine.getLength());
                    if (ebDelLivraisonLine.getHsCode() != null) ebMarchandise.setHsCode(ebDelLivraisonLine.getHsCode());
                    if (ebDelLivraisonLine.getItemNumber() != null) ebMarchandise
                        .setUnitReference(ebDelLivraisonLine.getItemNumber().toString());
                    if (ebDelLivraisonLine.getCustomsCode() != null) ebMarchandise
                        .setCustomerReference(ebDelLivraisonLine.getCustomsCode());
                    if (ebDelLivraisonLine.getxEcCountryOrigin() != null) ebMarchandise
                        .setxEcCountryOrigin(ebDelLivraisonLine.getxEcCountryOrigin().getEcCountryNum());

                    if (ebLivraison.getCustomerOrderReference() != null) {
                        ebMarchandise.setCustomerOrderReference(ebLivraison.getCustomerOrderReference().toString());
                    }

                    if (ebDelLivraisonLine.getItemName() != null) ebMarchandise
                        .setItemName(ebDelLivraisonLine.getItemName());

                    if (ebDelLivraisonLine.getItemNumber() != null) ebMarchandise
                        .setItemNumber(ebDelLivraisonLine.getItemNumber());

                    ebMarchandise.setSerialized(ebDelLivraisonLine.getSerialized());
                    ebMarchandise.setIdEbDelLivraison(ebLivraison.getEbDelLivraisonNum());
                    ebMarchandise.setxEbLivraisonLines(new ArrayList<EbLivraisonLine>());
                    ebMarchandise.getxEbLivraisonLines().add(ebDelLivraisonLine);
                    listMarchandises.add(ebMarchandise);

                    ebMarchandise.setCustomerReference(listMarchandises.indexOf(ebMarchandise) + "");
                }

            }

        }

        ebDemande.setListDeliveryForConsolidateNum(listDeliveryForConsolidateNum);
        ebDemande.setListMarchandises(listMarchandises);
        return ebDemande;
    }

    @Override
    public void deleteDelivery(List<Long> listSelectedDelivery) {

        if (listSelectedDelivery != null) {

            for (Long ebDelLivraisonNum: listSelectedDelivery) {
                EbLivraison ebLivraison = ebLivraisonRepository.findById(ebDelLivraisonNum).orElse(null);

                if (ebLivraison != null) {
                    EbOrder order = ebLivraison.getxEbDelOrder();
                    ebLivraisonLineRepository
                        .deleteByxEbDelLivraison_EbDelLivraisonNum(ebLivraison.getEbDelLivraisonNum());

                    ebLivraisonRepository.deleteById(ebLivraison.getEbDelLivraisonNum());

                    if (order != null) {
                        orderService.calculateOrder(order.getEbDelOrderNum(), true);
                    }

                }

            }

        }

    }

    @Override
    public void calculateParentOrder(Long ebLivraisonNum, boolean autoPersist) {
        EbLivraison ebLivraison = ebLivraisonRepository.findById(ebLivraisonNum).get();

        if (ebLivraison != null) {
            EbOrder order = ebLivraison.getxEbDelOrder();
            orderService.calculateOrder(order.getEbDelOrderNum(), true);
        }

    }

    @Override
    public EbLivraison updateStatusDelivery(Long ebLivraisonNum, StatusDelivery status) {
        EbLivraison ebLivraison = ebLivraisonRepository.findById(ebLivraisonNum).get();

        if (ebLivraison != null) {
            EbOrder order = ebLivraison.getxEbDelOrder();
            ebLivraison.setStatusDelivery(status);

            ebLivraisonRepository.save(ebLivraison);
            orderService.calculateOrder(order.getEbDelOrderNum(), true);
        }

        return ebLivraison;
    }

    @Override
    public void deleteOneDelivery(Long deliveryToDeleteNum) {

        if (deliveryToDeleteNum != null) {
            EbLivraison ebLivraison = ebLivraisonRepository.findById(deliveryToDeleteNum).orElse(null);

            if (ebLivraison != null) {
                EbOrder order = ebLivraison.getxEbDelOrder();
                ebLivraisonLineRepository.deleteByxEbDelLivraison_EbDelLivraisonNum(ebLivraison.getEbDelLivraisonNum());

                ebLivraisonRepository.deleteById(ebLivraison.getEbDelLivraisonNum());

                if (order != null) {
                    orderService.calculateOrder(order.getEbDelOrderNum(), true);
                }

            }

        }

    }

    @Override
    public void deleteOnDeliveryLine(Long deliveryLineToDeleteNum) {

        if (deliveryLineToDeleteNum != null) {
            EbOrder ebOrder = ebLivraisonLineRepository.getOrderByDeliveryLine(deliveryLineToDeleteNum);
            ebLivraisonLineRepository.deleteById(deliveryLineToDeleteNum);

            if (ebOrder != null) {
                orderService.calculateOrder(ebOrder.getEbDelOrderNum(), true);
            }

        }

    }

    @Override
    public EbLivraison cancelDelivery(Long ebDelLivraisonNum) {
        EbLivraison ebLivraison = new EbLivraison();

        if (ebDelLivraisonNum != null) {
            ebLivraison = ebLivraisonRepository.findById(ebDelLivraisonNum).orElse(null);

            if (ebLivraison != null) {
                this.updateStatusDelivery(ebDelLivraisonNum, StatusDelivery.CANCELED);
            }

        }

        return ebLivraison;
    }

    @Override
    public EbLivraison confirmDelivery(Long ebDelLivraisonNum) {
        EbLivraison livraison = ebLivraisonRepository.findById(ebDelLivraisonNum).get();

        if (!StatusDelivery.PENDING.equals(livraison.getStatusDelivery())) {
            throw new MyTowerException("Can't confirm non pending delivery");
        }

        return this.updateStatusDelivery(ebDelLivraisonNum, StatusDelivery.CONFIRMED);
    }
}
