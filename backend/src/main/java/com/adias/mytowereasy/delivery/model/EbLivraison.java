/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.delivery.model;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.*;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.util.JsonUtils;


@Table(name = "eb_del_livraison", indexes = {
    @Index(name = "idx_eb_del_livraison_x_eb_etablissement", columnList = "x_eb_etablissement"),
    @Index(name = "idx_eb_del_livraison_x_eb_party_destination", columnList = "x_eb_party_destination"),
    @Index(name = "idx_eb_del_livraison_x_eb_party_origin", columnList = "x_eb_party_origin"),
    @Index(name = "idx_eb_del_livraison_x_eb_party_sale", columnList = "x_eb_party_sale"),
    @Index(name = "idx_eb_del_livraison_x_eb_del_order", columnList = "x_eb_del_order"),
    @Index(name = "idx_eb_del_livraison_x_eb_demande", columnList = "x_eb_demande"),
    @Index(name = "idx_eb_del_livraison_x_eb_compagnie", columnList = "x_eb_compagnie"),
})

@Entity
@NamedEntityGraph(name = "Eblivraison.ebParty.ecCountry", attributeNodes = {
    @NamedAttributeNode(value = "xEbPartyOrigin", subgraph = "graph.EbParty.ecCountry"),
    @NamedAttributeNode(value = "xEbPartyDestination", subgraph = "graph.EbParty.ecCountry")
}, subgraphs = @NamedSubgraph(name = "graph.EbParty.ecCountry", attributeNodes = @NamedAttributeNode("xEcCountry")))
public class EbLivraison extends EbTimeStamps {
    private static final Logger logger = LoggerFactory.getLogger(EbLivraison.class);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ebDelLivraisonNum;

    private String ebDelReference;

    private String refTransport;

    private String deliveryNote;

    private String numOrderSAP;

    private String numOrderEDI;

    private String numOrderCustomer;

    private String orderCustomerCode;

    private String orderCustomerName;

    private String refDelivery;

    private String refClientCreator;

    private String nameOrderCreator;

    private String refClientDelivered;

    private String nameClientDelivered;

    private String countryClientDelivered;

    private String campaignCode;

    private String campaignName;

    private Date orderCreationDate;

    private Date requestedDeliveryDate;

    private Date suggestedDeliveryDate;

    private Date expectedDeliveryDate;

    private Date deliveryCreationDate;

    private String customerOrderReference;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Statiques.DATE_FORMAT)
    private Date estimatedShipDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Statiques.DATE_FORMAT)
    private Date effectiveShipDate;

    private String pickupSite;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Statiques.DATE_FORMAT)
    private Date endOfPackDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Statiques.DATE_FORMAT)
    private Date pickDate;

    private String invoiceNum;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = Statiques.DATE_FORMAT)
    private Date invoiceDate;

    private Integer transportStatus;

    private Long totalQuantityProducts;

    private Long numParcels;

    private Double totalWeight;

    private Double totalVolume;

    private String pathToBl;

    private String shipTo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_party_origin")
    private EbParty xEbPartyOrigin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_party_destination")
    private EbParty xEbPartyDestination;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_party_sale")
    private EbParty xEbPartySale;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement")
    private EbEtablissement xEbEtablissement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie")
    private EbCompagnie xEbCompagnie;

    private String listLabels;

    @OneToMany(mappedBy = "xEbDelLivraison", cascade = {
        CascadeType.MERGE, CascadeType.PERSIST
    }, fetch = FetchType.LAZY)
    private List<EbLivraisonLine> xEbDelLivraisonLine;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_del_order")
    private EbOrder xEbDelOrder;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_demande")
    private EbDemande xEbDemande;

    @OneToMany(mappedBy = "xEbDelLivraison", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<EbDelFichierJoint> xEbDelFichierJoint;

    private String listFlag;

    private String crossDock;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbFlagDTO> listEbFlagDTO;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String countryOriginLibelle;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String countryDestination;

    @Column(columnDefinition = "TEXT")
    private String customFields;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCategorie> listCategories;

    // champs reservé pour customFields
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    @Convert(converter = StatusDelivery.Converter.class)
    @Column(name = "delivery_status")
    private StatusDelivery statusDelivery;

    @ManyToOne
    @JoinColumn(name = "x_eb_owner_of_the_request")
    private EbUser xEbOwnerOfTheRequest;
    private Integer xEbIncotermNum;
    private String xEcIncotermLibelle;
    @Column(columnDefinition = "TEXT")
    private String complementaryInformations;

    @Column(columnDefinition = "TEXT")
    private String listCustomFieldsFlat;

    @Column(columnDefinition = "TEXT")
    private String listCustomFieldsValueFlat;

    private String refOrder;

    private Date dateOfGoodsAvailability;

    private Date customerDeliveryDate;

    public EbLivraison() {
        // TODO Auto-generated constructor stub
    }

    public void setProperty(String property, String value) {
        Field field = null;

        try {
            Class<?> c = this.getClass();
            field = c.getDeclaredField(property);

            try {
                field.set(this, value);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }

        } catch (NoSuchFieldException e1) {
            e1.printStackTrace();
        }

    }

    @QueryProjection
    public EbLivraison(
        Long ebDelLivraisonNum,
        String ebDelReference,
        String refDelivery,
        String numOrderSAP,
        String numOrderEDI,
        String numOrderCustomer,
        String orderCustomerCode,
        String orderCustomerName,
        String refTransport,
        String deliveryNote,
        Date orderCreationDate,
        Date expectedDeliveryDate,
        String campaignCode,
        String campaignName,
        Date pickDate,
        Date estimatedShipDate,
        Date effectiveShipDate,
        Date endOfPackDate,
        String pickupSite,
        Date invoiceDate,
        String reference,
        String company,
        String libelle,
        Date suggestedDeliveryDate,
        Date deliveryCreationDate,
        String customerOrderReference,
        Integer transportStatus,
        Long totalQuantityProducts,
        Long numParcels,
        Double totalWeight,
        Double totalVolume,
        String invoiceNum,
        String pathToBl,
        String crossDock,
        Integer ebDemandeNum,
        Long ebDelOrderNum,
        String customFields,
        List<EbCategorie> listCategories,
        String listLabels,
        StatusDelivery statusDelivery,
        Integer usernum,
        String nom,
        String prenom,
        Integer xEbIncotermNum,
        String xEcIncotermLibelle,
        String complementaryInformations,
        Integer ebPartyOriginNum,
        String ebPartyOriginAdresse,
        String ebPartyOriginCompagny,
        String ebPartyOriginCity,
        Integer ecCountryOriginNum,
        String ecCountryOriginLibelle,
        Integer ebPartyDestNum,
        String ebPartyDestAdresse,
        String ebPartyDestCompagny,
        String ebPartyDestCity,
        Integer ecCountryDestNum,
        String ecCountryDestLibelle,
        Integer ebPartySaleNum,
        String ebPartySaleAdresse,
        String ebPartySaleCompagny,
        String ebPartySaleCity,
        Integer ecCountrySaleNum,
        String ecCountrySaleLibelle,
        Timestamp dateCreationSys,
        String stringOrderWithExpression,
        String refOrder,
        Date dateOfGoodsAvailability,
        EbOrder ebOrder,
        Date customerDeliveryDate) {
        this.ebDelLivraisonNum = ebDelLivraisonNum;
        this.ebDelReference = ebDelReference;
        this.refDelivery = refDelivery;
        this.numOrderSAP = numOrderSAP;
        this.refTransport = refTransport;
        this.deliveryNote = deliveryNote;
        this.numOrderCustomer = numOrderCustomer;
        this.refClientDelivered = reference;
        this.nameClientDelivered = company;
        this.countryClientDelivered = libelle;
        this.orderCustomerName = orderCustomerName;
        this.numOrderEDI = numOrderEDI;
        this.orderCustomerCode = orderCustomerCode;
        this.campaignCode = campaignCode;
        this.campaignName = campaignName;
        this.transportStatus = transportStatus;
        this.totalQuantityProducts = totalQuantityProducts;
        this.orderCreationDate = orderCreationDate;
        this.expectedDeliveryDate = expectedDeliveryDate;
        this.numParcels = numParcels;
        this.totalWeight = totalWeight;
        this.suggestedDeliveryDate = suggestedDeliveryDate;
        this.deliveryCreationDate = deliveryCreationDate;
        this.customerOrderReference = customerOrderReference;
        this.estimatedShipDate = estimatedShipDate;
        this.pickDate = pickDate;
        this.effectiveShipDate = effectiveShipDate;
        this.endOfPackDate = endOfPackDate;
        this.pickupSite = pickupSite;
        this.invoiceDate = invoiceDate;
        this.totalVolume = totalVolume;
        this.invoiceNum = invoiceNum;
        this.pathToBl = pathToBl;
        this.crossDock = crossDock;
        this.xEbDemande = new EbDemande(ebDemandeNum);
        this.xEbDelOrder = new EbOrder(ebDelOrderNum);
        this.customFields = customFields;
        this.listCategories = listCategories;
        this.statusDelivery = statusDelivery;
        this.xEbOwnerOfTheRequest = new EbUser(usernum, nom, prenom);
        this.xEbIncotermNum = xEbIncotermNum;
        this.xEcIncotermLibelle = xEcIncotermLibelle;
        this.complementaryInformations = complementaryInformations;
        this.xEbDelOrder = ebOrder;
        this.customerDeliveryDate = customerDeliveryDate;
        this.listLabels = listLabels;
        
        if (customFields != null) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                this.listCustomsFields = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (ebPartyOriginNum != null) {
            EbParty ebPartyOrigin = new EbParty();
            ebPartyOrigin.setEbPartyNum(ebPartyOriginNum);
            ebPartyOrigin.setAdresse(ebPartyOriginAdresse);
            ebPartyOrigin.setCompany(ebPartyOriginCompagny);
            ebPartyOrigin.setCity(ebPartyOriginCity);

            EcCountry ecCountryOrigin = new EcCountry();
            ecCountryOrigin.setEcCountryNum(ecCountryOriginNum);
            ecCountryOrigin.setLibelle(ecCountryOriginLibelle);
            ebPartyOrigin.setxEcCountry(ecCountryOrigin);
            this.xEbPartyOrigin = new EbParty(ebPartyOrigin);
        }

        if (ebPartyDestNum != null) {
            EbParty ebPartyDest = new EbParty();
            ebPartyDest.setEbPartyNum(ebPartyDestNum);
            ebPartyDest.setAdresse(ebPartyDestAdresse);
            ebPartyDest.setCompany(ebPartyDestCompagny);
            ebPartyDest.setCity(ebPartyDestCity);

            EcCountry ecCountryDest = new EcCountry();
            ecCountryDest.setEcCountryNum(ecCountryDestNum);
            ecCountryDest.setLibelle(ecCountryDestLibelle);
            ebPartyDest.setxEcCountry(ecCountryDest);
            this.xEbPartyDestination = new EbParty(ebPartyDest);
        }

        if (ebPartySaleNum != null) {
            EbParty ebPartySale = new EbParty();
            ebPartySale.setEbPartyNum(ebPartySaleNum);
            ebPartySale.setAdresse(ebPartySaleAdresse);
            ebPartySale.setCompany(ebPartySaleCompagny);
            ebPartySale.setCity(ebPartySaleCity);

            EcCountry ecCountrySale = new EcCountry();
            ecCountrySale.setEcCountryNum(ecCountrySaleNum);
            ecCountrySale.setLibelle(ecCountrySaleLibelle);
            ebPartySale.setxEcCountry(ecCountrySale);
            this.xEbPartySale = new EbParty(ebPartySale);
        }

        this.dateCreationSys = dateCreationSys;
        this.refOrder = refOrder;
        this.dateOfGoodsAvailability = dateOfGoodsAvailability;
    }

    public String getListFlag() {
        return listFlag;
    }

    public void setListFlag(String listFlag) {
        this.listFlag = listFlag;
    }

    public List<EbFlagDTO> getListEbFlagDTO() {
        return listEbFlagDTO;
    }

    public void setListEbFlagDTO(List<EbFlagDTO> listEbFlagDTO) {
        this.listEbFlagDTO = listEbFlagDTO;
    }

    public String getNumOrderSAP() {
        return numOrderSAP;
    }

    public void setNumOrderSAP(String numOrderSAP) {
        this.numOrderSAP = numOrderSAP;
    }

    public String getNumOrderEDI() {
        return numOrderEDI;
    }

    public void setNumOrderEDI(String numOrderEDI) {
        this.numOrderEDI = numOrderEDI;
    }

    public String getRefDelivery() {
        return refDelivery;
    }

    public void setRefDelivery(String refDelivery) {
        this.refDelivery = refDelivery;
    }

    public String getNumOrderCustomer() {
        return numOrderCustomer;
    }

    public void setNumOrderCustomer(String numOrderCustomer) {
        this.numOrderCustomer = numOrderCustomer;
    }

    public Date getPickDate() {
        return pickDate;
    }

    public void setPickDate(Date pickDate) {
        this.pickDate = pickDate;
    }

    public EbParty getxEbPartySale() {
        return xEbPartySale;
    }

    public void setxEbPartySale(EbParty xEbPartySale) {
        this.xEbPartySale = xEbPartySale;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public Date getOrderCreationDate() {
        return orderCreationDate;
    }

    public void setOrderCreationDate(Date orderCreationDate) {
        this.orderCreationDate = orderCreationDate;
    }

    public Date getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(Date requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public Date getDeliveryCreationDate() {
        return deliveryCreationDate;
    }

    public void setDeliveryCreationDate(Date deliveryCreationDate) {
        this.deliveryCreationDate = deliveryCreationDate;
    }

    public Date getEstimatedShipDate() {
        return estimatedShipDate;
    }

    public void setEstimatedShipDate(Date estimatedShipDate) {
        this.estimatedShipDate = estimatedShipDate;
    }

    public Date getEffectiveShipDate() {
        return effectiveShipDate;
    }

    public void setEffectiveShipDate(Date effectiveShipDate) {
        this.effectiveShipDate = effectiveShipDate;
    }

    public String getPickupSite() {
        return pickupSite;
    }

    public void setPickupSite(String pickupSite) {
        this.pickupSite = pickupSite;
    }

    public Date getEndOfPackDate() {
        return endOfPackDate;
    }

    public void setEndOfPackDate(Date endOfPackDate) {
        this.endOfPackDate = endOfPackDate;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Integer getTransportStatus() {
        return transportStatus;
    }

    public void setTransportStatus(Integer transportStatus) {
        this.transportStatus = transportStatus;
    }

    public Long getTotalQuantityProducts() {
        return totalQuantityProducts;
    }

    public void setTotalQuantityProducts(Long totalQuantityProducts) {
        this.totalQuantityProducts = totalQuantityProducts;
    }

    public Long getNumParcels() {
        return numParcels;
    }

    public void setNumParcels(Long numParcels) {
        this.numParcels = numParcels;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(Double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public EbParty getxEbPartyOrigin() {
        return xEbPartyOrigin;
    }

    public void setxEbPartyOrigin(EbParty xEbPartyOrigin) {
        this.xEbPartyOrigin = xEbPartyOrigin;
    }

    public EbParty getxEbPartyDestination() {
        return xEbPartyDestination;
    }

    public void setxEbPartyDestination(EbParty xEbPartyDestination) {
        this.xEbPartyDestination = xEbPartyDestination;
    }

    public List<EbLivraisonLine> getxEbDelLivraisonLine() {
        return xEbDelLivraisonLine;
    }

    public void setxEbDelLivraisonLine(List<EbLivraisonLine> xEbDelLivraisonLine) {
        this.xEbDelLivraisonLine = xEbDelLivraisonLine;
    }

    public EbOrder getxEbDelOrder() {

        if (xEbDelOrder != null) {
            xEbDelOrder.setLivraisons(null);
        }

        return xEbDelOrder;
    }

    public void setxEbDelOrder(EbOrder xEbDelOrder) {
        this.xEbDelOrder = xEbDelOrder;
    }

    public EbDemande getxEbDemande() {
        return xEbDemande;
    }

    public void setxEbDemande(EbDemande xEbDemande) {
        this.xEbDemande = xEbDemande;
    }

    public Long getEbDelLivraisonNum() {
        return ebDelLivraisonNum;
    }

    public String getPathToBl() {
        return pathToBl;
    }

    public void setPathToBl(String pathToBl) {
        this.pathToBl = pathToBl;
    }

    public List<EbDelFichierJoint> getxEbDelFichierJoint() {
        return xEbDelFichierJoint;
    }

    public void setEbDelLivraisonNum(Long ebDelLivraisonNum) {
        this.ebDelLivraisonNum = ebDelLivraisonNum;
    }

    public void setxEbDelFichierJoint(List<EbDelFichierJoint> xEbDelFichierJoint) {
        this.xEbDelFichierJoint = xEbDelFichierJoint;
    }

    public Date getSuggestedDeliveryDate() {
        return suggestedDeliveryDate;
    }

    public void setSuggestedDeliveryDate(Date suggestedDeliveryDate) {
        this.suggestedDeliveryDate = suggestedDeliveryDate;
    }

    public Date getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public String getRefClientCreator() {
        return refClientCreator;
    }

    public String getNameOrderCreator() {
        return nameOrderCreator;
    }

    public String getRefClientDelivered() {
        return refClientDelivered;
    }

    public String getNameClientDelivered() {
        return nameClientDelivered;
    }

    public String getCountryClientDelivered() {
        return countryClientDelivered;
    }

    public void setRefClientCreator(String refClientCreator) {
        this.refClientCreator = refClientCreator;
    }

    public void setNameOrderCreator(String nameOrderCreator) {
        this.nameOrderCreator = nameOrderCreator;
    }

    public void setRefClientDelivered(String refClientDelivered) {
        this.refClientDelivered = refClientDelivered;
    }

    public void setNameClientDelivered(String nameClientDelivered) {
        this.nameClientDelivered = nameClientDelivered;
    }

    public void setCountryClientDelivered(String countryClientDelivered) {
        this.countryClientDelivered = countryClientDelivered;
    }

    public EbEtablissement getxEbEtablissement() {
        return xEbEtablissement;
    }

    public void setxEbEtablissement(EbEtablissement xEbEtablissement) {
        this.xEbEtablissement = xEbEtablissement;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public String getListLabels() {
        return listLabels;
    }

    public void setListLabels(String listLabels) {
        this.listLabels = listLabels;
    }

    public String getShipTo() {
        return shipTo;
    }

    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    public String getOrderCustomerCode() {
        return orderCustomerCode;
    }

    public void setOrderCustomerCode(String orderCustomerCode) {
        this.orderCustomerCode = orderCustomerCode;
    }

    public String getOrderCustomerName() {
        return orderCustomerName;
    }

    public void setOrderCustomerName(String orderCustomerName) {
        this.orderCustomerName = orderCustomerName;
    }

    public String getRefTransport() {
        return refTransport;
    }

    public void setRefTransport(String refTransport) {
        this.refTransport = refTransport;
    }

    public String getDeliveryNote() {
        return deliveryNote;
    }

    public void setDeliveryNote(String deliveryNote) {
        this.deliveryNote = deliveryNote;
    }

    public String getCrossDock() {
        return crossDock;
    }

    public void setCrossDock(String crossDock) {
        this.crossDock = crossDock;
    }

    public void setCountryOriginLibelle(String countryOriginLibelle) {
        this.countryOriginLibelle = countryOriginLibelle;
    }

    public String getCountryOriginLibelle() {
        return countryOriginLibelle;
    }

    public String getCountryDestination() {
        return countryDestination;
    }

    public void setCountryDestination(String countryDestination) {
        this.countryDestination = countryDestination;
    }

    public List<EbCategorie> getListCategories() {

        if (listCategories != null && listCategories.size() > 0) {

            try {
                this.listCategories = JsonUtils.staticMapper
                    .convertValue(listCategories, new TypeReference<List<EbCategorie>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {

        if (CollectionUtils.isEmpty(listCategories)) throw new MyTowerException("listCategories is null");
        
        this.listCategories = listCategories
            .stream().map(it -> EbCategorie.getEbCategorieLite(it)).collect(Collectors.toList());

        // pour deserialiser la liste et eviter l'erreur linked hashMap
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            String jsonCateg = objectMapper.writeValueAsString(listCategories);
            this.listCategories = objectMapper.readValue(jsonCateg, new TypeReference<ArrayList<EbCategorie>>() {
            });
        } catch (Exception e) {
            logger.error("error deserializing listCategories", e);
        }

        this.listLabels = this.listCategories
            .stream().filter(cat -> CollectionUtils.isNotEmpty(cat.getLabels()))
            .flatMap(cat -> cat.getLabels().stream()).map(lab -> lab.getEbLabelNum().toString())
            .collect(Collectors.joining(","));

        if (StringUtils.isNotBlank(this.listLabels)) {
            // le trie des labels est naicessaire pour la partie
            // groupage
            this.listLabels = Arrays
                .asList(this.listLabels.split(",")).stream().map(Integer::parseInt).sorted().map(it -> it.toString())
                .collect(Collectors.joining(","));
        }
    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;

        if (this.customFields != null && !this.customFields.isEmpty()) {
            List<CustomFields> listCustomFieldsDeserialized = null;
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                listCustomFieldsDeserialized = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

            this.listCustomFieldsFlat = "";
            this.listCustomFieldsValueFlat = "";

            for (CustomFields field: listCustomFieldsDeserialized) {
                if (field.getValue() == null || field.getValue().isEmpty() || field.getName() == null
                    || field.getName().isEmpty()) continue;

                if (!this.listCustomFieldsFlat.isEmpty()) this.listCustomFieldsFlat += ";";

                if (!this.listCustomFieldsValueFlat.isEmpty()) this.listCustomFieldsValueFlat += ";";

                this.listCustomFieldsFlat += "\"" + field.getName() + "\":" + "\"" + field.getValue() + "\"";

                this.listCustomFieldsValueFlat += "\"" + field.getLabel() + "\":" + "\"" + field.getValue() + "\"";
            }

            if (this.listCustomFieldsFlat.isEmpty()) this.listCustomFieldsFlat = null;

            if (this.listCustomFieldsValueFlat.isEmpty()) this.listCustomFieldsValueFlat = null;
        }

    }

    public StatusDelivery getStatusDelivery() {
        return statusDelivery;
    }

    public void setStatusDelivery(StatusDelivery statusDelivery) {
        this.statusDelivery = statusDelivery;
    }

    public EbUser getxEbOwnerOfTheRequest() {
        return xEbOwnerOfTheRequest;
    }

    public void setxEbOwnerOfTheRequest(EbUser xEbOwnerOfTheRequest) {
        this.xEbOwnerOfTheRequest = xEbOwnerOfTheRequest;
    }

    public Integer getxEbIncotermNum() {
        return xEbIncotermNum;
    }

    public void setxEbIncotermNum(Integer xEbIncotermNum) {
        this.xEbIncotermNum = xEbIncotermNum;
    }

    public String getxEcIncotermLibelle() {
        return xEcIncotermLibelle;
    }

    public void setxEcIncotermLibelle(String xEcIncotermLibelle) {
        this.xEcIncotermLibelle = xEcIncotermLibelle;
    }

    public String getComplementaryInformations() {
        return complementaryInformations;
    }

    public void setComplementaryInformations(String complementaryInformations) {
        this.complementaryInformations = complementaryInformations;
    }

    public String getListCustomFieldsFlat() {
        return listCustomFieldsFlat;
    }

    public void setListCustomFieldsFlat(String listCustomFieldsFlat) {
        this.listCustomFieldsFlat = listCustomFieldsFlat;
    }

    public String getListCustomFieldsValueFlat() {
        return listCustomFieldsValueFlat;
    }

    public void setListCustomFieldsValueFlat(String listCustomFieldsValueFlat) {
        this.listCustomFieldsValueFlat = listCustomFieldsValueFlat;
    }

    public String getCustomerOrderReference() {
        return customerOrderReference;
    }

    public void setCustomerOrderReference(String customerOrderReference) {
        this.customerOrderReference = customerOrderReference;
    }

    public String getEbDelReference() {
        return ebDelReference;
    }

    public void setEbDelReference(String ebDelReference) {
        this.ebDelReference = ebDelReference;
    }

    public String getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(String refOrder) {
        this.refOrder = refOrder;
    }

    public Date getDateOfGoodsAvailability() {
        return dateOfGoodsAvailability;
    }

    public void setDateOfGoodsAvailability(Date dateOfGoodsAvailability) {
        this.dateOfGoodsAvailability = dateOfGoodsAvailability;
    }

    public Date getCustomerDeliveryDate() {
        return customerDeliveryDate;
    }

    public void setCustomerDeliveryDate(Date customerDeliveryDate) {
        this.customerDeliveryDate = customerDeliveryDate;
    }
}
