package com.adias.mytowereasy.delivery.model.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.adias.mytowereasy.api.wso.SearchCriteriaDeliveryWSO;
import com.adias.mytowereasy.api.wso.SearchCriteriaOrderWSO;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.repository.EbUserRepository;


@Component
public class SearchCriteriaMapper {

    @Autowired
    EbUserRepository ebUserRepository;

    public SearchCriteriaOrder orderSearchCriteriaDtoToModel(SearchCriteriaOrderWSO searchCriteriaOrderWSO) {
        SearchCriteriaOrder searchCriteriaOrder = new SearchCriteriaOrder();

        if (StringUtils.isEmpty(searchCriteriaOrderWSO.getCustomerOrderReference())) {
            throw new MyTowerException("Customer Order Reference is null");
        }

        searchCriteriaOrder.setCustomerOrderReference(searchCriteriaOrderWSO.getCustomerOrderReference());

        if (StringUtils.isEmpty(searchCriteriaOrderWSO.getRequestOwnerEmail())) {
            throw new MyTowerException("Request Owner Email is null");
        }

        EbUser user = getUserByEmail(searchCriteriaOrderWSO.getRequestOwnerEmail());

        searchCriteriaOrder.setEbCompagnieNum(user.getEbCompagnie().getEbCompagnieNum());

        return searchCriteriaOrder;
    }

    private EbUser getUserByEmail(String requestOwnerEmail) {
        EbUser user = ebUserRepository.findByEmail(requestOwnerEmail);

        if (user == null) {
            throw new MyTowerException(
                String.format("user with mail %s does not exist ", requestOwnerEmail));
        }

        return user;
    }

    public SearchCriteriaDelivery
        deliverySearchCriteriaDtoToModel(SearchCriteriaDeliveryWSO searchCriteriaDeliveryWSO) {
        SearchCriteriaDelivery searchCriteriaDelivery = new SearchCriteriaDelivery();

        if (StringUtils.isEmpty(searchCriteriaDeliveryWSO.getCustomerOrderReference())) {
            throw new MyTowerException("Customer Delivery Reference is null");
        }

        searchCriteriaDelivery.setCustomerOrderReference(searchCriteriaDeliveryWSO.getCustomerOrderReference());

        if (StringUtils.isEmpty(searchCriteriaDeliveryWSO.getRequestOwnerEmail())) {
            throw new MyTowerException("Request Owner Email is null");
        }

        EbUser user = getUserByEmail(searchCriteriaDeliveryWSO.getRequestOwnerEmail());
        searchCriteriaDelivery.setEbCompagnieNum(user.getEbCompagnie().getEbCompagnieNum());

        return searchCriteriaDelivery;
    }
}
