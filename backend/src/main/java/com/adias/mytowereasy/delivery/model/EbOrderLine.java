/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.delivery.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.*;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.util.ObjectDeserializer;


@Entity
@Table(name = "eb_del_order_line", indexes = {
    @Index(name = "idx_eb_del_order_line_x_eb_del_order", columnList = "x_eb_del_order"),
    @Index(name = "idx_eb_del_order_line_x_eb_type_unit", columnList = "x_eb_type_unit"),
    @Index(name = "idx_eb_del_order_line_x_ec_country_origin", columnList = "x_ec_country_origin")
})
public class EbOrderLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ebDelOrderLineNum;

    private Long idOrderLine;

    private String refArticle;

    private String articleName;

    private Date dateOfAvailability;

    private String canceledReason;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_type_Unit")
    private EbTypeUnit xEbtypeOfUnit;

    private Long ebTypeUnitNum;

    private Integer height;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country_origin")
    private EcCountry xEcCountryOrigin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_del_order")
    private EbOrder xEbDelOrder;

    // champs reservé pour customFields
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    @JsonDeserialize
    @JsonSerialize
    @Transient
    private Boolean isSelected;

    private String partNumber;
    private String serialNumber;
    private Date dateRequest;
    private Integer priority;
    private String comment;
    private String specification;

    @Column(columnDefinition = "TEXT")
    private String customFields;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCategorie> listCategories;

    @Column(columnDefinition = "text")
    private String listLabels;

    @Temporal(TemporalType.DATE)
    private Date targetDate;

    private Double weight;

    private Double length;

    private Double width;

    private String hsCode;

    private Double volume;

    private String itemName;

    private String itemNumber;

    private Long quantityOrdered; // Quantity ordered (total)
    private Long quantityPlannable; // Not already planned
    private Long quantityPlanned; // Sum of all quantity already planned (all
                                  // status)

    private Long quantityPending; // Quantities at status pending
    private Long quantityConfirmed; // Quantities at status confirmed
    private Long quantityShipped; // Quantities at status shipped (passed PSL
                                  // start)
    private Long quantityDelivered; // Quantities at status delivery (passed PSL
                                    // end)

    private Long quantityCanceled; // Quantities canceled (used for analytics
                                   // only)

    private Boolean serialized;

    private Double price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "currency")
    private EcCurrency currency;

    private String eccn;

    public EbOrderLine() {
    }

    @QueryProjection
    public EbOrderLine(
        Long ebDelOrderLineNum,
        Long idOrderLine,
        String refArticle,
        String articleName,
        Date dateOfAvailability,
        String canceledReason,
        String countryOriginLibelle,
        String refClientDelivered,
        String nameClientDelivered,
        String countryClientDelivered,
        String numOrderSAP,
        String orderCustomerName,
        String campaignCode,
        String campaignName,
        String numOrderCustomer,
        Integer orderStatus,
        Date customerCreationDate,
        Date deliveryDate,
        String partNumber,
        String serialNumber,
        Date dateRequest,
        Integer priority,
        String comment,
        String specification,
        List<EbCategorie> listCategories,
        String customFields,
        Double weight,
        Double length,
        Double width,
        Integer height,
        String hscode,
        Double volume,
        String itemName,
        String itemNumber,
        Long ebTypeUnitNum,

        Long quantityOrdered,
        Long quantityPlannable,
        Long quantityPlanned,
        Long quantityPending,
        Long quantityConfirmed,
        Long quantityShipped,
        Long quantityDelivered,
        Long quantityCanceled,
        Boolean serialized,
        Double price,
        String eccn,
        Integer currencyId,
        String currencyLibelle
        ) {
        this.ebDelOrderLineNum = ebDelOrderLineNum;
        this.idOrderLine = idOrderLine;
        this.refArticle = refArticle;
        this.articleName = articleName;
        this.dateOfAvailability = dateOfAvailability;
        this.canceledReason = canceledReason;

        List<EbLivraison> livraisons = new ArrayList<EbLivraison>();
        EbLivraison livraison = new EbLivraison();
        livraison.setRefClientDelivered(refClientDelivered);
        livraison.setNameClientDelivered(nameClientDelivered);
        livraison.setCountryClientDelivered(countryClientDelivered);
        livraisons.add(livraison);

        this.xEbDelOrder = new EbOrder();
        this.xEbDelOrder.setLivraisons(livraisons);
        this.xEbDelOrder.setCountryOriginLibelle(countryOriginLibelle);
        this.xEbDelOrder.setNumOrderSAP(numOrderSAP);
        this.xEbDelOrder.setOrderCustomerName(orderCustomerName);
        this.xEbDelOrder.setCampaignCode(campaignCode);
        this.xEbDelOrder.setCampaignName(campaignName);
        this.xEbDelOrder.setNumOrderCustomer(numOrderCustomer);
        this.xEbDelOrder.setOrderStatus(orderStatus);
        this.xEbDelOrder.setCustomerCreationDate(customerCreationDate);
        this.xEbDelOrder.setDeliveryDate(deliveryDate);

        this.partNumber = partNumber;
        this.serialNumber = serialNumber;
        this.dateRequest = dateRequest;
        this.priority = priority;
        this.comment = comment;
        this.specification = specification;
        this.listCategories = listCategories;
        this.customFields = customFields;
        this.height = height;
        this.weight = weight;
        this.width = width;
        this.length = length;
        this.hsCode = hscode;
        this.volume = volume;
        this.itemName = itemName;
        this.itemNumber = itemNumber;

        this.quantityOrdered = quantityOrdered;
        this.quantityPlannable = quantityPlannable;
        this.quantityPlanned = quantityPlanned;
        this.quantityPending = quantityPending;
        this.quantityConfirmed = quantityConfirmed;
        this.quantityShipped = quantityShipped;
        this.quantityDelivered = quantityDelivered;
        this.quantityCanceled = quantityCanceled;
        this.serialized = serialized;

        if (customFields != null) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                this.listCustomsFields = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        this.price = price;
        this.eccn = eccn;
        this.currency = new EcCurrency();
        this.currency.setEcCurrencyNum(currencyId);
        this.currency.setLibelle(currencyLibelle);

    }
    
    @QueryProjection
    public EbOrderLine(
        Long ebDelOrderLineNum,
        Long idOrderLine,
        String refArticle,
        String articleName,
        Date dateOfAvailability,
        String canceledReason,
        String countryOriginLibelle,
        String refClientDelivered,
        String nameClientDelivered,
        String countryClientDelivered,
        String numOrderSAP,
        String orderCustomerName,
        String campaignCode,
        String campaignName,
        String numOrderCustomer,
        Integer orderStatus,
        Date customerCreationDate,
        Date deliveryDate,
        String partNumber,
        String serialNumber,
        Date dateRequest,
        Integer priority,
        String comment,
        String specification,
        List<EbCategorie> listCategories,
        String customFields,
        Double weight,
        Double length,
        Double width,
        Integer height,
        String hscode,
        Double volume,
        String itemName,
        String itemNumber,
        Long ebTypeUnitNum,

        Long quantityOrdered,
        Long quantityPlannable,
        Long quantityPlanned,
        Long quantityPending,
        Long quantityConfirmed,
        Long quantityShipped,
        Long quantityDelivered,
        Long quantityCanceled,
        Boolean serialized,
        Double price,
        String eccn,
        Integer ecCurrencyNum,String currencyLibelle, String currencyCode) {
        this(
            ebDelOrderLineNum,
            idOrderLine,
            refArticle,
            articleName,
            dateOfAvailability,
            canceledReason,
            countryOriginLibelle,
            refClientDelivered,
            nameClientDelivered,
            countryClientDelivered,
            numOrderSAP,
            orderCustomerName,
            campaignCode,
            campaignName,
            numOrderCustomer,
            orderStatus,
            customerCreationDate,
            deliveryDate,
            partNumber,
            serialNumber,
            dateRequest,
            priority,
            comment,
            specification,
            listCategories,
            customFields,
            weight,
            length,
            width,
            height,
            hscode,
            volume,
            itemName,
            itemNumber,
            ebTypeUnitNum,

            quantityOrdered,
            quantityPlannable,
            quantityPlanned,
            quantityPending,
            quantityConfirmed,
            quantityShipped,
            quantityDelivered,
            quantityCanceled,
            serialized, price, eccn, ecCurrencyNum, currencyLibelle);
    }


    public Long getEbDelOrderLineNum() {
        return ebDelOrderLineNum;
    }

    public void setEbDelOrderLineNum(Long ebDelOrderLineNum) {
        this.ebDelOrderLineNum = ebDelOrderLineNum;
    }

    public String getRefArticle() {
        return refArticle;
    }

    public void setRefArticle(String refArticle) {
        this.refArticle = refArticle;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public Date getDateOfAvailability() {
        return dateOfAvailability;
    }

    public void setDateOfAvailability(Date dateOfAvailability) {
        this.dateOfAvailability = dateOfAvailability;
    }

    public EcCountry getxEcCountryOrigin() {
        return xEcCountryOrigin;
    }

    public void setxEcCountryOrigin(EcCountry xEcCountryOrigin) {
        this.xEcCountryOrigin = xEcCountryOrigin;
    }

    public EbOrder getxEbDelOrder() {
        return xEbDelOrder;
    }

    public void setxEbDelOrder(EbOrder xEbDelOrder) {
        this.xEbDelOrder = xEbDelOrder;
    }

    public Long getIdOrderLine() {
        return idOrderLine;
    }

    public void setIdOrderLine(Long idOrderLine) {
        this.idOrderLine = idOrderLine;
    }

    public EbTypeUnit getxEbtypeOfUnit() {
        return xEbtypeOfUnit;
    }

    public void setxEbtypeOfUnit(EbTypeUnit xEbtypeOfUnit) {
        this.xEbtypeOfUnit = xEbtypeOfUnit;
    }

    public Boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getCanceledReason() {
        return canceledReason;
    }

    public void setCanceledReason(String canceledReason) {
        this.canceledReason = canceledReason;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Date getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(Date dateRequest) {
        this.dateRequest = dateRequest;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public List<EbCategorie> getListCategories() {

        if (listCategories != null && listCategories.size() > 0) {

            try {
                this.listCategories = ObjectDeserializer
                    .checkJsonListObject(listCategories, new TypeToken<List<EbCategorie>>() {
                    }.getType());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {

        if (listCategories != null) {
            this.listCategories = listCategories
                .stream().map(it -> EbCategorie.getEbCategorieLite(it)).collect(Collectors.toList());

            // pour deserialiser la liste et eviter l'erreur linked hashMap
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String jsonCateg = objectMapper.writeValueAsString(listCategories);
                this.listCategories = objectMapper.readValue(jsonCateg, new TypeReference<ArrayList<EbCategorie>>() {
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (this.listCategories != null && this.listCategories.size() > 0) {
                this.listLabels = "";

                for (EbCategorie cat: this.listCategories) {

                    if (cat.getLabels() != null && cat.getLabels().size() > 0) {

                        for (EbLabel lab: cat.getLabels()) {
                            // le cas de plusieurs libelles
                            if (!this.listLabels.isEmpty()
                                && this.listLabels.charAt(this.listLabels.length() - 1) != ',') this.listLabels += ",";

                            this.listLabels += lab.getEbLabelNum();
                        }

                    }

                    if (this.listLabels.isEmpty()) {
                        this.listLabels = null;
                    }
                    else {
                        // le trie des labels est naicessaire pour la partie
                        // groupage
                        List<Integer> listLabelsArray = Arrays
                            .asList(this.listLabels.split(",")).stream().map(Integer::parseInt).sorted()
                            .collect(Collectors.toList());
                        this.listLabels = StringUtils.join(listLabelsArray, ",");
                    }

                }

            }
            else {
                this.listCategories = listCategories;
            }

        }

    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;

        if (this.customFields != null && !this.customFields.isEmpty()) {
            List<CustomFields> listCustomFieldsDeserialized = null;
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                listCustomFieldsDeserialized = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

            for (CustomFields field: listCustomFieldsDeserialized) {
                if (field.getValue() == null || field.getValue().isEmpty() || field.getName() == null
                    || field.getName().isEmpty()) continue;
            }

        }

    }

    public String getListLabels() {
        return listLabels;
    }

    public void setListLabels(String listLabels) {
        this.listLabels = listLabels;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Long getEbTypeUnitNum() {
        return ebTypeUnitNum;
    }

    public void setEbTypeUnitNum(Long ebTypeUnitNum) {
        this.ebTypeUnitNum = ebTypeUnitNum;
    }

    public Long getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(Long quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public Long getQuantityPlannable() {
        return quantityPlannable;
    }

    public void setQuantityPlannable(Long quantityPlannable) {
        this.quantityPlannable = quantityPlannable;
    }

    public Long getQuantityPlanned() {
        return quantityPlanned;
    }

    public void setQuantityPlanned(Long quantityPlanned) {
        this.quantityPlanned = quantityPlanned;
    }

    public Long getQuantityPending() {
        return quantityPending;
    }

    public void setQuantityPending(Long quantityPending) {
        this.quantityPending = quantityPending;
    }

    public Long getQuantityConfirmed() {
        return quantityConfirmed;
    }

    public void setQuantityConfirmed(Long quantityConfirmed) {
        this.quantityConfirmed = quantityConfirmed;
    }

    public Long getQuantityShipped() {
        return quantityShipped;
    }

    public void setQuantityShipped(Long quantityShipped) {
        this.quantityShipped = quantityShipped;
    }

    public Long getQuantityDelivered() {
        return quantityDelivered;
    }

    public void setQuantityDelivered(Long quantityDelivered) {
        this.quantityDelivered = quantityDelivered;
    }

    public Long getQuantityCanceled() {
        return quantityCanceled;
    }

    public void setQuantityCanceled(Long quantityCanceled) {
        this.quantityCanceled = quantityCanceled;
    }

    public Boolean getSerialized() {
        return serialized;
    }

    public void setSerialized(Boolean serialized) {
        this.serialized = serialized;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public EcCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(EcCurrency currency) {
        this.currency = currency;
    }

    public String getEccn() {
        return eccn;
    }

    public void setEccn(String eccn) {
        this.eccn = eccn;
    }
}
