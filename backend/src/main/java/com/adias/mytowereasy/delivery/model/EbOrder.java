/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.delivery.model;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.util.JsonUtils;


@Entity
@JsonIgnoreProperties({
    "nbUnit"
})
@Table(name = "eb_del_order", indexes = {
    @Index(name = "idx_eb_order_eb_party_destination", columnList = "x_eb_party_destination"),
    @Index(name = "idx_eb_order_x_eb_party_origin", columnList = "x_eb_party_origin"),
    @Index(name = "idx_eb_order_x_eb_party_sale", columnList = "x_eb_party_sale"),
    @Index(name = "idx_eb_order_x_eb_party_vendor", columnList = "x_eb_party_vendor"),
    @Index(name = "idx_eb_order_x_eb_party_issuer", columnList = "x_eb_party_issuer"),
    @Index(name = "idx_eb_order_x_eb_etablissement", columnList = "x_eb_etablissement"),
    @Index(name = "idx_eb_order_x_eb_compagnie", columnList = "x_eb_compagnie")
})
public class EbOrder extends EbTimeStamps {
    private static final Logger logger = LoggerFactory.getLogger(EbOrder.class);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ebDelOrderNum;

    private String numOrderSAP;

    private String numOrderEDI;

    private String numOrderCustomer;

    private String orderCustomerCode;

    private String orderCustomerName;

    private String orderType;

    private String campaignCode;

    private String campaignName;

    private Integer orderStatus;

    private String crossDock;

    @Temporal(TemporalType.DATE)
    private Date etd;

    @Temporal(TemporalType.DATE)
    private Date eta;

    private String meanOfTransport;

    private Integer numberOfItems;

    private String departureSite;

    @Column(name = "weight_gross")
    private BigDecimal grossWeight;

    @Column(name = "weight_net")
    private BigDecimal netWeight;

    private String deliveryCity;

    private String numberOfEdit;

    @Temporal(TemporalType.DATE)
    private Date customerCreationDate;

    @Temporal(TemporalType.DATE)
    private Date deliveryDate;

    private Long totalQuantityOrdered; // Quantity ordered (total)
    private Long totalQuantityPlannable; // Not already planned
    private Long totalQuantityPlanned; // Sum of all quantity already planned
                                       // (all status)

    private Long totalQuantityPending; // Quantities at status pending
    private Long totalQuantityConfirmed; // Quantities at status confirmed
    private Long totalQuantityShipped; // Quantities at status shipped (passed
                                       // PSL start)
    private Long totalQuantityDelivered; // Quantities at status delivery
                                         // (passed PSL end)

    private Long totalQuantityCanceled; // Quantities canceled (used for
                                        // analytics only)

    private Integer canceled;

    private String purchaseOrderType;

    private String shipTo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_party_origin")
    private EbParty xEbPartyOrigin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_party_destination")
    private EbParty xEbPartyDestination;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_party_sale")
    private EbParty xEbPartySale;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement")
    private EbEtablissement xEbEtablissement;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_compagnie")
    private EbCompagnie xEbCompagnie;

    @Column(columnDefinition = "text")
    private String listLabels;

    @OneToMany(mappedBy = "xEbDelOrder", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<EbOrderLine> orderLines;

    @OneToMany(mappedBy = "xEbDelOrder", fetch = FetchType.LAZY)
    private List<EbLivraison> livraisons;

    @ManyToOne
    @JoinColumn(name = "x_eb_owner_of_the_request")
    private EbUser xEbOwnerOfTheRequest;

    private String listFlag;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbFlagDTO> listEbFlagDTO;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String countryOriginLibelle;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String refClientDelivered;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String nameClientDelivered;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String countryClientDelivered;

    @Column(columnDefinition = "TEXT")
    private String customFields;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCategorie> listCategories;

    // champs reservé pour customFields
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    @Temporal(TemporalType.DATE)
    private Date targetDate;

    @Temporal(TemporalType.DATE)
    private Date orderDate;

    // Vendor address (informations about Vendor, supplier or customer in this
    // order. )
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_party_vendor")
    private EbParty xEbPartyVendor;

    // Issuer address (informations related to who issued the order )
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_party_issuer")
    private EbParty xEbPartyIssuer;

    private Integer priority;

    private String customerOrderReference;

    @Temporal(TemporalType.DATE)
    private Date dateOfArrival;

    private Integer xEbIncotermNum;
    private String xEcIncotermLibelle;

    @Column(columnDefinition = "TEXT")
    private String complementaryInformations;

    @Column(columnDefinition = "TEXT")
    private String listCustomFieldsFlat;

    @Column(columnDefinition = "TEXT")
    private String listCustomFieldsValueFlat;

    @NotNull
    private String reference;

    public String getCustomerOrderReference() {
        return customerOrderReference;
    }

    public void setCustomerOrderReference(String customerOrderReference) {
        this.customerOrderReference = customerOrderReference;
    }

    public EbOrder() {
    }

    public EbOrder(Long ebDelOrderNum) {
        this.ebDelOrderNum = ebDelOrderNum;
    }

    @QueryProjection
    public EbOrder(
        Long ebDelOrderNum,
        String numOrderSAPO,
        String numOrderEDI,
        String orderCustomerCode,
        String orderType,
        String campaignCode,
        String campaignName,
        String numOrderCustomer,
        String orderCustomerName,
        String shippedToCustomerRef,
        String purchaseOrderType,
        Date customerCreationDate,
        Date deliveryDate,
        Integer orderStatus,
        String crossDock,
        Integer ebPartyOriginNum,
        String ebPartyOriginAdresse,
        String ebPartyOriginCompagny,
        String ebPartyOriginCity,
        Integer ecCountryOriginNum,
        String ecCountryOriginLibelle,
        Integer ebPartyDestNum,
        String ebPartyDestAdresse,
        String ebPartyDestCompagny,
        String ebPartyDestCity,
        Integer ecCountryDestNum,
        String ecCountryDestLibelle,
        Integer ebPartySaleNum,
        String ebPartySaleAdresse,
        String ebPartySaleCompagny,
        String ebPartySaleCity,
        Integer ecCountrySaleNum,
        String ecCountrySaleLibelle,
        Integer xEbEtablissementNum,
        String xEbEtablissementNom,
        String customFields,
        List<EbCategorie> listCategories,
        Date targetDate,
        Date orderDate,
        Integer ebPartyVendorNum,
        String ebPartyVendorAdresse,
        String ebPartyVendorCompagny,
        String ebPartyVendorCity,
        Integer ecCountryVendorNum,
        String ecCountryVendorLibelle,
        Integer ebPartyIssuerNum,
        String ebPartyIssuerAdresse,
        String ebPartyIssuerCompagny,
        String ebPartyIssuerCity,
        Integer ecCountryIssuerNum,
        String ecCountryIssuerLibelle,
        String listFlag,
        Integer priority,
        String customerOrderReference,
        Date dateOfArrival,
        Integer xEbIncotermNum,
        String xEcIncotermLibelle,
        String complementaryInformations,
        String stringOrderWithExpression,
        String reference,
        Long totalQuantityOrdered,
        Long totalQuantityPlannable,
        Long totalQuantityPlanned,
        Long totalQuantityPending,
        Long totalQuantityConfirmed,
        Long totalQuantityShipped,
        Long totalQuantityDelivered,
        Long totalQuantityCanceled) {
        this.ebDelOrderNum = ebDelOrderNum;
        this.numOrderSAP = numOrderSAPO;
        this.numOrderEDI = numOrderEDI;
        this.orderCustomerCode = orderCustomerCode;
        this.orderType = orderType;
        this.campaignCode = campaignCode;
        this.campaignName = campaignName;
        this.numOrderCustomer = numOrderCustomer;
        this.orderCustomerName = orderCustomerName;
        this.shipTo = shippedToCustomerRef;
        this.purchaseOrderType = purchaseOrderType;
        this.customerCreationDate = customerCreationDate;
        this.deliveryDate = deliveryDate;
        this.orderStatus = orderStatus;
        this.crossDock = crossDock;
        this.listCategories = listCategories;
        this.customFields = customFields;
        this.targetDate = targetDate;
        this.orderDate = orderDate;
        this.listFlag = listFlag;
        this.priority = priority;
        this.customerOrderReference = customerOrderReference;
        this.dateOfArrival = dateOfArrival;
        this.xEbIncotermNum = xEbIncotermNum;
        this.xEcIncotermLibelle = xEcIncotermLibelle;
        this.complementaryInformations = complementaryInformations;

        this.totalQuantityPlannable = totalQuantityPlannable;
        this.totalQuantityPlanned = totalQuantityPlanned;

        if (customFields != null) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                this.listCustomsFields = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (ebPartyOriginNum != null) {
            EbParty ebPartyOrigin = new EbParty();
            ebPartyOrigin.setEbPartyNum(ebPartyOriginNum);
            ebPartyOrigin.setAdresse(ebPartyOriginAdresse);
            ebPartyOrigin.setCompany(ebPartyOriginCompagny);
            ebPartyOrigin.setCity(ebPartyOriginCity);

            EcCountry ecCountryOrigin = new EcCountry();
            ecCountryOrigin.setEcCountryNum(ecCountryOriginNum);
            ecCountryOrigin.setLibelle(ecCountryOriginLibelle);
            ebPartyOrigin.setxEcCountry(ecCountryOrigin);
            this.xEbPartyOrigin = new EbParty(ebPartyOrigin);
        }

        if (ebPartyDestNum != null) {
            EbParty ebPartyDest = new EbParty();
            ebPartyDest.setEbPartyNum(ebPartyDestNum);
            ebPartyDest.setAdresse(ebPartyDestAdresse);
            ebPartyDest.setCompany(ebPartyDestCompagny);
            ebPartyDest.setCity(ebPartyDestCity);

            EcCountry ecCountryDest = new EcCountry();
            ecCountryDest.setEcCountryNum(ecCountryDestNum);
            ecCountryDest.setLibelle(ecCountryDestLibelle);
            ebPartyDest.setxEcCountry(ecCountryDest);
            this.xEbPartyDestination = new EbParty(ebPartyDest);
        }

        if (ebPartySaleNum != null) {
            EbParty ebPartySale = new EbParty();
            ebPartySale.setEbPartyNum(ebPartySaleNum);
            ebPartySale.setAdresse(ebPartySaleAdresse);
            ebPartySale.setCompany(ebPartySaleCompagny);
            ebPartySale.setCity(ebPartySaleCity);

            EcCountry ecCountrySale = new EcCountry();
            ecCountrySale.setEcCountryNum(ecCountrySaleNum);
            ecCountrySale.setLibelle(ecCountrySaleLibelle);
            ebPartySale.setxEcCountry(ecCountrySale);
            this.xEbPartySale = new EbParty(ebPartySale);
        }

        if (xEbEtablissementNum != null) {
            EbEtablissement xEbEtablissement = new EbEtablissement();
            xEbEtablissement.setEbEtablissementNum(xEbEtablissementNum);
            xEbEtablissement.setNom(xEbEtablissementNom);
            this.xEbEtablissement = xEbEtablissement;
        }

        if (ebPartyVendorNum != null) {
            EbParty ebPartyVendor = new EbParty();
            ebPartyVendor.setEbPartyNum(ebPartyDestNum);
            ebPartyVendor.setAdresse(ebPartyDestAdresse);
            ebPartyVendor.setCompany(ebPartyDestCompagny);
            ebPartyVendor.setCity(ebPartyDestCity);

            EcCountry ecCountryVendor = new EcCountry();
            ecCountryVendor.setEcCountryNum(ecCountryVendorNum);
            ecCountryVendor.setLibelle(ecCountryVendorLibelle);
            ebPartyVendor.setxEcCountry(ecCountryVendor);
            this.xEbPartyVendor = new EbParty(ebPartyVendor);
        }

        if (ebPartyIssuerNum != null) {
            EbParty ebPartyIssuer = new EbParty();
            ebPartyIssuer.setEbPartyNum(ebPartyIssuerNum);
            ebPartyIssuer.setAdresse(ebPartyIssuerAdresse);
            ebPartyIssuer.setCompany(ebPartyIssuerCompagny);
            ebPartyIssuer.setCity(ebPartyIssuerCity);

            EcCountry ecCountryIssuer = new EcCountry();
            ecCountryIssuer.setEcCountryNum(ecCountryIssuerNum);
            ecCountryIssuer.setLibelle(ecCountryIssuerLibelle);
            ebPartyIssuer.setxEcCountry(ecCountryIssuer);
            this.xEbPartyIssuer = new EbParty(ebPartyIssuer);
        }

        this.reference = reference;

        this.totalQuantityOrdered = totalQuantityOrdered;
        this.totalQuantityPlannable = totalQuantityPlannable;
        this.totalQuantityPlanned = totalQuantityPlanned;
        this.totalQuantityPending = totalQuantityPending;
        this.totalQuantityConfirmed = totalQuantityConfirmed;
        this.totalQuantityShipped = totalQuantityShipped;
        this.totalQuantityDelivered = totalQuantityDelivered;
        this.totalQuantityCanceled = totalQuantityCanceled;
    }

    public void setProperty(String property, String value) {
        Field field = null;

        try {
            Class<?> c = this.getClass();
            field = c.getDeclaredField(property);

            try {
                field.set(this, value);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }

        } catch (NoSuchFieldException e1) {
            e1.printStackTrace();
        }

    }

    public String getListFlag() {
        return listFlag;
    }

    public void setListFlag(String listFlag) {
        this.listFlag = listFlag;
    }

    public List<EbFlagDTO> getListEbFlagDTO() {
        return listEbFlagDTO;
    }

    public void setListEbFlagDTO(List<EbFlagDTO> listEbFlagDTO) {
        this.listEbFlagDTO = listEbFlagDTO;
    }

    public String getNumOrderSAP() {
        return numOrderSAP;
    }

    public void setNumOrderSAP(String numOrderSAP) {
        this.numOrderSAP = numOrderSAP;
    }

    public String getNumOrderEDI() {
        return numOrderEDI;
    }

    public void setNumOrderEDI(String numOrderEDI) {
        this.numOrderEDI = numOrderEDI;
    }

    public String getNumOrderCustomer() {
        return numOrderCustomer;
    }

    public void setNumOrderCustomer(String numOrderCustomer) {
        this.numOrderCustomer = numOrderCustomer;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Date getCustomerCreationDate() {
        return customerCreationDate;
    }

    public void setCustomerCreationDate(Date customerCreationDate) {
        this.customerCreationDate = customerCreationDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public EbParty getxEbPartyOrigin() {
        return xEbPartyOrigin;
    }

    public void setxEbPartyOrigin(EbParty xEbPartyOrigin) {
        this.xEbPartyOrigin = xEbPartyOrigin;
    }

    public EbParty getxEbPartyDestination() {
        return xEbPartyDestination;
    }

    public void setxEbPartyDestination(EbParty xEbPartyDestination) {
        this.xEbPartyDestination = xEbPartyDestination;
    }

    public List<EbOrderLine> getOrderLines() {
        return orderLines;
    }

    public void setOrderLines(List<EbOrderLine> orderLines) {
        this.orderLines = orderLines;
    }

    public List<EbLivraison> getLivraisons() {
        return livraisons;
    }

    public void setLivraisons(List<EbLivraison> livraisons) {
        this.livraisons = livraisons;
    }

    public Long getEbDelOrderNum() {
        return ebDelOrderNum;
    }

    public EbParty getxEbPartySale() {
        return xEbPartySale;
    }

    public void setxEbPartySale(EbParty xEbPartySale) {
        this.xEbPartySale = xEbPartySale;
    }

    public void setEbDelOrderNum(Long ebOrderNum) {
        this.ebDelOrderNum = ebOrderNum;
    }

    public String getPurchaseOrderType() {
        return purchaseOrderType;
    }

    public void setPurchaseOrderType(String purchaseOrderType) {
        this.purchaseOrderType = purchaseOrderType;
    }

    public EbEtablissement getxEbEtablissement() {
        return xEbEtablissement;
    }

    public void setxEbEtablissement(EbEtablissement xEbEtablissement) {
        this.xEbEtablissement = xEbEtablissement;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public String getListLabels() {
        return listLabels;
    }

    public void setListLabels(String listLabels) {
        this.listLabels = listLabels;
    }

    public String getShipTo() {
        return shipTo;
    }

    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    public String getOrderCustomerCode() {
        return orderCustomerCode;
    }

    public void setOrderCustomerCode(String orderCustomerCode) {
        this.orderCustomerCode = orderCustomerCode;
    }

    public String getOrderCustomerName() {
        return orderCustomerName;
    }

    public void setOrderCustomerName(String orderCustomerName) {
        this.orderCustomerName = orderCustomerName;
    }

    public Date getEtd() {
        return etd;
    }

    public Date getEta() {
        return eta;
    }

    public String getMeanOfTransport() {
        return meanOfTransport;
    }

    public Integer getNumberOfItems() {
        return numberOfItems;
    }

    public String getDepartureSite() {
        return departureSite;
    }

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public BigDecimal getNetWeight() {
        return netWeight;
    }

    public String getDeliveryCity() {
        return deliveryCity;
    }

    public String getNumberOfEdit() {
        return numberOfEdit;
    }

    public void setEtd(Date etd) {
        this.etd = etd;
    }

    public void setEta(Date eta) {
        this.eta = eta;
    }

    public void setMeanOfTransport(String meanOfTransport) {
        this.meanOfTransport = meanOfTransport;
    }

    public void setNumberOfItems(Integer numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public void setDepartureSite(String departureSite) {
        this.departureSite = departureSite;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public void setNetWeight(BigDecimal netWeight) {
        this.netWeight = netWeight;
    }

    public void setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
    }

    public void setNumberOfEdit(String numberOfEdit) {
        this.numberOfEdit = numberOfEdit;
    }

    public Integer getCanceled() {
        return canceled;
    }

    public void setCanceled(Integer canceled) {
        this.canceled = canceled;
    }

    public String getCrossDock() {
        return crossDock;
    }

    public void setCrossDock(String crossDock) {
        this.crossDock = crossDock;
    }

    public String getCountryOriginLibelle() {
        return countryOriginLibelle;
    }

    public String getRefClientDelivered() {
        return refClientDelivered;
    }

    public String getNameClientDelivered() {
        return nameClientDelivered;
    }

    public String getCountryClientDelivered() {
        return countryClientDelivered;
    }

    public void setCountryOriginLibelle(String countryOriginLibelle) {
        this.countryOriginLibelle = countryOriginLibelle;
    }

    public void setRefClientDelivered(String refClientDelivered) {
        this.refClientDelivered = refClientDelivered;
    }

    public void setNameClientDelivered(String nameClientDelivered) {
        this.nameClientDelivered = nameClientDelivered;
    }

    public void setCountryClientDelivered(String countryClientDelivered) {
        this.countryClientDelivered = countryClientDelivered;
    }

    public List<EbCategorie> getListCategories() {

        if (listCategories != null && listCategories.size() > 0) {

            try {
                this.listCategories = JsonUtils.staticMapper
                    .convertValue(listCategories, new TypeReference<List<EbCategorie>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {

        if (CollectionUtils.isEmpty(listCategories)) throw new MyTowerException("listCategories is null");

        this.listCategories = listCategories
            .stream().map(it -> EbCategorie.getEbCategorieLite(it)).collect(Collectors.toList());

        // pour deserialiser la liste et eviter l'erreur linked hashMap
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            String jsonCateg = objectMapper.writeValueAsString(listCategories);
            this.listCategories = objectMapper.readValue(jsonCateg, new TypeReference<ArrayList<EbCategorie>>() {
            });
        } catch (Exception e) {
            logger.error("error deserializing listCategories", e);
        }

        this.listLabels = this.listCategories
            .stream().filter(cat -> CollectionUtils.isNotEmpty(cat.getLabels()))
            .flatMap(cat -> cat.getLabels().stream()).map(lab -> lab.getEbLabelNum().toString())
            .collect(Collectors.joining(","));

        if (StringUtils.isNotBlank(this.listLabels)) {
            // le trie des labels est naicessaire pour la partie
            // groupage
            this.listLabels = Arrays
                .asList(this.listLabels.split(",")).stream().map(Integer::parseInt).sorted().map(it -> it.toString())
                .collect(Collectors.joining(","));
        }

    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;

        if (this.customFields != null && !this.customFields.isEmpty()) {
            List<CustomFields> listCustomFieldsDeserialized = null;
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                listCustomFieldsDeserialized = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

            this.listCustomFieldsFlat = "";
            this.listCustomFieldsValueFlat = "";

            for (CustomFields field: listCustomFieldsDeserialized) {
                if (field.getValue() == null || field.getValue().isEmpty() || field.getName() == null
                    || field.getName().isEmpty()) continue;

                if (!this.listCustomFieldsFlat.isEmpty()) this.listCustomFieldsFlat += ";";

                if (!this.listCustomFieldsValueFlat.isEmpty()) this.listCustomFieldsValueFlat += ";";

                this.listCustomFieldsFlat += "\"" + field.getName() + "\":" + "\"" + field.getValue() + "\"";

                this.listCustomFieldsValueFlat += "\"" + field.getLabel() + "\":" + "\"" + field.getValue() + "\"";
            }

            if (this.listCustomFieldsFlat.isEmpty()) this.listCustomFieldsFlat = null;

            if (this.listCustomFieldsValueFlat.isEmpty()) this.listCustomFieldsValueFlat = null;
        }

    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public EbParty getxEbPartyVendor() {
        return xEbPartyVendor;
    }

    public void setxEbPartyVendor(EbParty xEbPartyVendor) {
        this.xEbPartyVendor = xEbPartyVendor;
    }

    public EbParty getxEbPartyIssuer() {
        return xEbPartyIssuer;
    }

    public void setxEbPartyIssuer(EbParty xEbPartyIssuer) {
        this.xEbPartyIssuer = xEbPartyIssuer;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Date getDateOfArrival() {
        return dateOfArrival;
    }

    public void setDateOfArrival(Date dateOfArrival) {
        this.dateOfArrival = dateOfArrival;
    }

    public Integer getxEbIncotermNum() {
        return xEbIncotermNum;
    }

    public void setxEbIncotermNum(Integer xEbIncotermNum) {
        this.xEbIncotermNum = xEbIncotermNum;
    }

    public String getxEcIncotermLibelle() {
        return xEcIncotermLibelle;
    }

    public void setxEcIncotermLibelle(String xEcIncotermLibelle) {
        this.xEcIncotermLibelle = xEcIncotermLibelle;
    }

    public String getComplementaryInformations() {
        return complementaryInformations;
    }

    public void setComplementaryInformations(String complementaryInformations) {
        this.complementaryInformations = complementaryInformations;
    }

    public EbUser getxEbOwnerOfTheRequest() {
        return xEbOwnerOfTheRequest;
    }

    public void setxEbOwnerOfTheRequest(EbUser xEbOwnerOfTheRequest) {
        this.xEbOwnerOfTheRequest = xEbOwnerOfTheRequest;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Long getTotalQuantityOrdered() {
        return totalQuantityOrdered;
    }

    public void setTotalQuantityOrdered(Long totalQuantityOrdered) {
        this.totalQuantityOrdered = totalQuantityOrdered;
    }

    public Long getTotalQuantityPlannable() {
        return totalQuantityPlannable;
    }

    public void setTotalQuantityPlannable(Long totalQuantityPlannable) {
        this.totalQuantityPlannable = totalQuantityPlannable;
    }

    public Long getTotalQuantityPlanned() {
        return totalQuantityPlanned;
    }

    public void setTotalQuantityPlanned(Long totalQuantityPlanned) {
        this.totalQuantityPlanned = totalQuantityPlanned;
    }

    public Long getTotalQuantityPending() {
        return totalQuantityPending;
    }

    public void setTotalQuantityPending(Long totalQuantityPending) {
        this.totalQuantityPending = totalQuantityPending;
    }

    public Long getTotalQuantityConfirmed() {
        return totalQuantityConfirmed;
    }

    public void setTotalQuantityConfirmed(Long totalQuantityConfirmed) {
        this.totalQuantityConfirmed = totalQuantityConfirmed;
    }

    public Long getTotalQuantityShipped() {
        return totalQuantityShipped;
    }

    public void setTotalQuantityShipped(Long totalQuantityShipped) {
        this.totalQuantityShipped = totalQuantityShipped;
    }

    public Long getTotalQuantityDelivered() {
        return totalQuantityDelivered;
    }

    public void setTotalQuantityDelivered(Long totalQuantityDelivered) {
        this.totalQuantityDelivered = totalQuantityDelivered;
    }

    public Long getTotalQuantityCanceled() {
        return totalQuantityCanceled;
    }

    public void setTotalQuantityCanceled(Long totalQuantityCanceled) {
        this.totalQuantityCanceled = totalQuantityCanceled;
    }

    public String getListCustomFieldsFlat() {
        return listCustomFieldsFlat;
    }

    public void setListCustomFieldsFlat(String listCustomFieldsFlat) {
        this.listCustomFieldsFlat = listCustomFieldsFlat;
    }

    public String getListCustomFieldsValueFlat() {
        return listCustomFieldsValueFlat;
    }

    public void setListCustomFieldsValueFlat(String listCustomFieldsValueFlat) {
        this.listCustomFieldsValueFlat = listCustomFieldsValueFlat;
    }
}
