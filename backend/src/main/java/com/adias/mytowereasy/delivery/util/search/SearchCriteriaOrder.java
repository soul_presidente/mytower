package com.adias.mytowereasy.delivery.util.search;

import static com.adias.mytowereasy.model.Statiques.DATE_FORMATTER;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.delivery.enumeration.StatusOrder;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.QEbLivraison;
import com.adias.mytowereasy.delivery.model.QEbOrder;
import com.adias.mytowereasy.delivery.model.QEbOrderLine;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.util.enums.GenericEnumException;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public class SearchCriteriaOrder extends SearchCriteria {
    BooleanBuilder w = new BooleanBuilder();

    private Long ebDelOrderNum;

    private String numOrderSAP;

    private String refDelivery;

    private String crossDock;

    private String refArticle;

    private String numOrderEDI;

    private String numOrderCustomer;

    private String orderCustomerCode;

    private String orderCustomerName;

    private String nameClientDelivered;

    private String countryClientDelivered;

    private String refClientDelivered;

    private String orderType;

    private String campaignCode;

    private String campaignName;

    private Integer orderStatus;

    private EbUser xEbOwnerOfTheRequest;

    private EbParty xEbPartyOrigin;

    private EbParty xEbPartyDestination;

    private String customerOrderReference;

    private String customFields;

    /**
     * Only effective for a get one
     */
    private Boolean includeLines;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date creationDateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date deliveryDateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date creationDateTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date customerCreationDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date deliveryDateTo;

    private boolean isCount = false;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCampaignCode;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCampaignName;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listNumOrderCustomer;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listOrderCustomerName;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listOrderCustomerCode;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listNameClientDelivered;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listRefDelivery;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCrossDock;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCountryClientDelivered;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listRefClientDelivered;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listNumOrderEDI;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listNumOrderSAP;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listRefArticle;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listOrderStatus;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listcustomerOrderReference;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listcustomFields;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listPartyOrigin;

    private Map<String, OrderSpecifier<?>> orderMapASC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, OrderSpecifier<?>> orderMapDESC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, Path<?>> orderMap = new HashMap<String, Path<?>>();

    private QEbOrder qEbOrder = new QEbOrder("ebOrder");
    private QEbLivraison qEbLivraison = new QEbLivraison("ebLivraison");
    private QEbOrderLine qEbOrderLine = new QEbOrderLine("ebOrderLine");
    private QEcCountry qEcCountry = new QEcCountry("ecCountry");

    private QEbParty qPartyDest = new QEbParty("partyDest");
    private QEbParty qPartyOrigin = new QEbParty("partyOrigin");
    private QEbParty qPartySale = new QEbParty("partySale");

    private QEbParty qPartyVendor = new QEbParty("partyVendor");
    private QEbParty qPartyIssuer = new QEbParty("partyIssuer");

    private QEcCountry qCountryOrigin = new QEcCountry("qCountryOrigin");
    private QEcCountry qCountryDest = new QEcCountry("qCountryDest");
    private QEcCountry qCountrySale = new QEcCountry("qCountrySale");

    private QEbEtablissement qEbEtablissement = new QEbEtablissement("ebEtablissement");

    private QEbUser qEbUser = new QEbUser("ebUser");

    public void setSearchInput(String searchInput, List<CustomFields> customsFields) throws JsonParseException, JsonMappingException, IOException {
        if (searchInput == null) searchInput = "{}";

        super.setSearchInput(searchInput, customsFields);

        this.listOrderStatus = ((SearchCriteriaOrder) deserialize(
            searchInput,
            SearchCriteriaOrder.class)).listOrderStatus;

        this.listcustomerOrderReference = ((SearchCriteriaOrder) deserialize(
            searchInput,
            SearchCriteriaOrder.class)).listcustomerOrderReference;

        this.customerCreationDate = ((SearchCriteriaOrder) deserialize(
            searchInput,
            SearchCriteriaOrder.class)).customerCreationDate;
    }

    public SearchCriteriaOrder setProperty(String property, String value, SearchCriteriaOrder criteria) {
        Field field = null;

        try {
            Class<?> c = criteria.getClass();
            field = c.getDeclaredField(property);

            try {
                field.set(criteria, value);
                criteria.setOrderedColumn(property);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }

        } catch (NoSuchFieldException e1) {
            e1.printStackTrace();
        }

        return criteria;
    }

    private void Initialize(QEbOrder qEbOrder) {
        orderMapASC.clear();
        orderMap.clear();
        orderMapDESC.clear();

        // Order by Etablissement
        orderMap.put("etablissementName", qEbOrder.xEbEtablissement().nom);
        orderMapASC.put("etablissementName", qEbOrder.xEbEtablissement().nom.asc());
        orderMapDESC.put("etablissementName", qEbOrder.xEbEtablissement().nom.desc());

        orderMap.put("ebOrderNum", qEbOrder.ebDelOrderNum);
        orderMapASC.put("ebOrderNum", qEbOrder.ebDelOrderNum.asc());
        orderMapDESC.put("ebOrderNum", qEbOrder.ebDelOrderNum.desc());

        orderMap.put("crossDock", qEbOrder.crossDock);
        orderMapASC.put("crossDock", qEbOrder.crossDock.asc());
        orderMapDESC.put("crossDock", qEbOrder.crossDock.desc());

        orderMap.put("totalQuantityOrdered", qEbOrder.totalQuantityOrdered);
        orderMapASC.put("totalQuantityOrdered", qEbOrder.totalQuantityOrdered.asc());
        orderMapDESC.put("totalQuantityOrdered", qEbOrder.totalQuantityOrdered.desc());

        orderMap.put("totalQuantityPlannable", qEbOrder.totalQuantityPlannable);
        orderMapASC.put("totalQuantityPlannable", qEbOrder.totalQuantityPlannable.asc());
        orderMapDESC.put("totalQuantityPlannable", qEbOrder.totalQuantityPlannable.desc());

        orderMap.put("totalQuantityPlanned", qEbOrder.totalQuantityPlanned);
        orderMapASC.put("totalQuantityPlanned", qEbOrder.totalQuantityPlanned.asc());
        orderMapDESC.put("totalQuantityPlanned", qEbOrder.totalQuantityPlanned.desc());

        orderMap.put("totalQuantityPending", qEbOrder.totalQuantityPending);
        orderMapASC.put("totalQuantityPending", qEbOrder.totalQuantityPending.asc());
        orderMapDESC.put("totalQuantityPending", qEbOrder.totalQuantityPending.desc());

        orderMap.put("totalQuantityConfirmed", qEbOrder.totalQuantityConfirmed);
        orderMapASC.put("totalQuantityConfirmed", qEbOrder.totalQuantityConfirmed.asc());
        orderMapDESC.put("totalQuantityConfirmed", qEbOrder.totalQuantityConfirmed.desc());

        orderMap.put("totalQuantityShipped", qEbOrder.totalQuantityShipped);
        orderMapASC.put("totalQuantityShipped", qEbOrder.totalQuantityShipped.asc());
        orderMapDESC.put("totalQuantityShipped", qEbOrder.totalQuantityShipped.desc());

        orderMap.put("totalQuantityDelivered", qEbOrder.totalQuantityDelivered);
        orderMapASC.put("totalQuantityDelivered", qEbOrder.totalQuantityDelivered.asc());
        orderMapDESC.put("totalQuantityDelivered", qEbOrder.totalQuantityDelivered.desc());

        orderMap.put("totalQuantityCanceled", qEbOrder.totalQuantityCanceled);
        orderMapASC.put("totalQuantityCanceled", qEbOrder.totalQuantityCanceled.asc());
        orderMapDESC.put("totalQuantityCanceled", qEbOrder.totalQuantityCanceled.desc());

        // order by refArticle
        orderMap.put("refArticle", qEbOrderLine.refArticle);
        orderMapASC.put("refArticle", qEbOrderLine.refArticle.asc());
        orderMapDESC.put("refArticle", qEbOrderLine.refArticle.desc());

        // order by refDelivery
        orderMap.put("refDelivery", qEbLivraison.refDelivery);
        orderMapASC.put("refDelivery", qEbLivraison.refDelivery.asc());
        orderMapDESC.put("refDelivery", qEbLivraison.refDelivery.desc());

        orderMap.put("numOrderSAP", qEbOrder.numOrderSAP);
        orderMapASC.put("numOrderSAP", qEbOrder.numOrderSAP.asc());
        orderMapDESC.put("numOrderSAP", qEbOrder.numOrderSAP.desc());

        orderMap.put("numOrderEDI", qEbOrder.numOrderEDI);
        orderMapASC.put("numOrderEDI", qEbOrder.numOrderEDI.asc());
        orderMapDESC.put("numOrderEDI", qEbOrder.numOrderEDI.desc());

        orderMap.put("numOrderCustomer", qEbOrder.numOrderCustomer);
        orderMapASC.put("numOrderCustomer", qEbOrder.numOrderCustomer.asc());
        orderMapDESC.put("numOrderCustomer", qEbOrder.numOrderCustomer.desc());
        orderMap.put("refClientCreator", qPartySale.reference);
        orderMapASC.put("refClientCreator", qPartySale.reference.asc());
        orderMapDESC.put("refClientCreator", qPartySale.reference.desc());
        orderMap.put("nameOrderCreator", qPartySale.company);
        orderMapASC.put("nameOrderCreator", qPartySale.company.asc());
        orderMapDESC.put("nameOrderCreator", qPartySale.company.desc());
        orderMap.put("refClientDelivered", qPartyDest.reference);
        orderMapASC.put("refClientDelivered", qPartyDest.reference.asc());
        orderMapDESC.put("refClientDelivered", qPartyDest.reference.desc());
        orderMap.put("nameClientDelivered", qPartyDest.company);
        orderMapASC.put("nameClientDelivered", qPartyDest.company.asc());
        orderMapDESC.put("nameClientDelivered", qPartyDest.company.desc());
        orderMap.put("countryClientDelivered", qCountryDest.libelle);
        orderMapASC.put("countryClientDelivered", qCountryDest.libelle.asc());
        orderMapDESC.put("countryClientDelivered", qCountryDest.libelle.desc());
        orderMap.put("customerCreationDate", qEbOrder.customerCreationDate);
        orderMapASC.put("customerCreationDate", qEbOrder.customerCreationDate.asc());
        orderMapDESC.put("customerCreationDate", qEbOrder.customerCreationDate.desc());
        orderMap.put("deliveryDate", qEbOrder.deliveryDate);
        orderMapASC.put("deliveryDate", qEbOrder.deliveryDate.asc());
        orderMapDESC.put("deliveryDate", qEbOrder.deliveryDate.desc());
        orderMap.put("orderStatus", qEbOrder.orderStatus);
        orderMapASC.put("orderStatus", qEbOrder.orderStatus.asc());
        orderMapDESC.put("orderStatus", qEbOrder.orderStatus.desc());
        orderMap.put("orderType", qEbOrder.orderType);
        orderMapASC.put("orderType", qEbOrder.orderType.asc());
        orderMapDESC.put("orderType", qEbOrder.orderType.desc());
        orderMap.put("purchaseOrderType", qEbOrder.purchaseOrderType);
        orderMapASC.put("purchaseOrderType", qEbOrder.purchaseOrderType.asc());
        orderMapDESC.put("purchaseOrderType", qEbOrder.purchaseOrderType.desc());
        orderMap.put("campaignCode", qEbOrder.campaignCode);
        orderMapASC.put("campaignCode", qEbOrder.campaignCode.asc());
        orderMapDESC.put("campaignCode", qEbOrder.campaignCode.desc());
        orderMap.put("campaignName", qEbOrder.campaignName);
        orderMapASC.put("campaignName", qEbOrder.campaignName.asc());
        orderMapDESC.put("campaignName", qEbOrder.campaignName.desc());
        orderMap.put("orderCustomerCode", qEbOrder.orderCustomerCode);
        orderMapASC.put("orderCustomerCode", qEbOrder.orderCustomerCode.asc());
        orderMapDESC.put("orderCustomerCode", qEbOrder.orderCustomerCode.desc());
        orderMap.put("orderCustomerName", qEbOrder.orderCustomerName);
        orderMapASC.put("orderCustomerName", qEbOrder.orderCustomerName.asc());
        orderMapDESC.put("orderCustomerName", qEbOrder.orderCustomerName.desc());

        // Order by Owner of request
        orderMap.put("xEbOwnerOfTheRequest", qEbOrder.xEbOwnerOfTheRequest().nom);
        orderMapASC.put("xEbOwnerOfTheRequest", qEbOrder.xEbOwnerOfTheRequest().nom.asc());
        orderMapDESC.put("xEbOwnerOfTheRequest", qEbOrder.xEbOwnerOfTheRequest().nom.desc());

        // Order by origin party
        orderMap.put("xEbPartyOrigin", qEbOrder.xEbPartyOrigin().xEcCountry().libelle);
        orderMapASC.put("xEbPartyOrigin", qEbOrder.xEbPartyOrigin().xEcCountry().libelle.asc());
        orderMapDESC.put("xEbPartyOrigin", qEbOrder.xEbPartyOrigin().xEcCountry().libelle.desc());

        // Order by dest party
        orderMap.put("xEbPartyDestination", qEbOrder.xEbPartyDestination().xEcCountry().libelle);
        orderMapASC.put("xEbPartyDestination", qEbOrder.xEbPartyDestination().xEcCountry().libelle.asc());
        orderMapDESC.put("xEbPartyDestination", qEbOrder.xEbPartyDestination().xEcCountry().libelle.desc());

        // Order by customerOrderReference
        orderMap.put("customerOrderReference", qEbOrder.customerOrderReference);
        orderMapASC.put("customerOrderReference", qEbOrder.customerOrderReference.asc());
        orderMapDESC.put("customerOrderReference", qEbOrder.customerOrderReference.desc());

        // Order by customerOrderReference
        orderMap.put("customFields", qEbOrder.customFields);
        orderMapASC.put("customFields", qEbOrder.customFields.asc());
        orderMapDESC.put("customFields", qEbOrder.customFields.desc());

        // order by order_reference
        orderMap.put("reference", qEbOrder.reference);
        orderMapASC.put("reference", qEbOrder.reference.asc());
        orderMapDESC.put("reference", qEbOrder.reference.desc());
    }

    public SearchCriteriaOrder() {
        this.ebDelOrderNum = null;
        numOrderSAP = null;
        numOrderEDI = null;
        numOrderCustomer = null;
        orderCustomerCode = null;
        orderCustomerName = null;
        refClientDelivered = null;
        nameClientDelivered = null;
        refDelivery = null;
        crossDock = null;
        countryClientDelivered = null;
        orderType = null;
        campaignCode = null;
        campaignName = null;
        orderStatus = null;
        listCampaignCode = null;
        listCampaignName = null;
        listOrderCustomerName = null;
        listOrderCustomerCode = null;
        listNameClientDelivered = null;
        listCountryClientDelivered = null;
        listRefDelivery = null;
        listRefClientDelivered = null;
        listNumOrderCustomer = null;
        listNumOrderEDI = null;
        listCrossDock = null;
        listNumOrderSAP = null;
        listRefArticle = null;
        listOrderStatus = null;
        creationDateFrom = null;
        deliveryDateFrom = null;
        creationDateTo = null;
        deliveryDateTo = null;
        customerOrderReference = null;
        customFields = null;
        listcustomerOrderReference = null;
        listcustomFields = null;
        listPartyOrigin = null;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    public JPAQuery applyCriteria(JPAQuery query, boolean applyorder) throws GenericEnumException {
        JPAQuery<EbOrder> resultQuery = query;
        BooleanBuilder where = new BooleanBuilder();

        Initialize(qEbOrder);

        if (this.ebDelOrderNum != null) {
            where.and(qEbOrder.ebDelOrderNum.eq(this.ebDelOrderNum));
        }

        if (this.numOrderSAP != null) {
            where.and(qEbOrder.numOrderSAP.eq(this.numOrderSAP));
        }

        if (this.refArticle != null) {
            where.and(qEbOrderLine.refArticle.eq(this.refArticle));
        }

        if (this.numOrderEDI != null) {
            where.and(qEbOrder.numOrderEDI.eq(this.numOrderEDI));
        }

        if (this.numOrderCustomer != null) {
            where.and(qEbOrder.numOrderCustomer.eq(this.numOrderCustomer));
        }

        if (this.orderCustomerName != null) {
            where.and(qEbOrder.orderCustomerName.eq(this.orderCustomerName));
        }

        if (this.nameClientDelivered != null) {
            where.and(qPartyDest.company.eq(this.nameClientDelivered));
        }

        if (this.countryClientDelivered != null) {
            where.and(qPartyDest.xEcCountry().libelle.eq(this.countryClientDelivered));
        }

        if (this.refDelivery != null) {
            query.where(qEbLivraison.refDelivery.eq(this.refDelivery));
        }

        if (this.crossDock != null) {
            where.and(qEbOrder.crossDock.eq(this.crossDock));
        }

        if (this.refClientDelivered != null) {
            where.and(qPartyDest.reference.eq(this.refClientDelivered));
        }

        if (this.orderCustomerCode != null) {
            where.and(qEbOrder.orderCustomerCode.eq(this.orderCustomerCode));
        }

        if (this.refClientDelivered != null) {
            where.and(qPartyDest.reference.eq(this.refClientDelivered));
        }

        if (this.nameClientDelivered != null) {
            where.and(qPartyDest.company.eq(this.nameClientDelivered));
        }

        if (this.countryClientDelivered != null) {
            where.and(qCountryDest.libelle.eq(this.countryClientDelivered));
        }

        if (this.orderType != null) {
            where.and(qEbOrder.orderType.eq(this.orderType));
        }

        if (this.campaignCode != null) {
            where.and(qEbOrder.campaignCode.contains(this.campaignCode));
        }

        if (this.campaignName != null) {
            where.and(qEbOrder.campaignName.contains(this.campaignName));
        }

        if (this.orderStatus != null) {
            where.and(qEbOrder.orderStatus.eq(this.orderStatus));
        }

        if (this.customerOrderReference != null) {
            where.and(qEbOrder.customerOrderReference.eq(this.customerOrderReference));
        }

        if (this.ebCompagnieNum != null) {
            where.and(qEbOrder.xEbCompagnie().ebCompagnieNum.eq(this.ebCompagnieNum));
        }

        this.applyAdvancedSearchForCustomFields(where, qEbOrder.customFields);

        if (MapUtils.isNotEmpty(mapCategoryFields)) {
            BooleanBuilder boolLabels = new BooleanBuilder();
            AtomicReference<BooleanBuilder> orBoolLabels = new AtomicReference<>();

            mapCategoryFields
                .entrySet().stream().filter(e -> CollectionUtils.isNotEmpty(e.getValue())).forEach(cat -> {
                    orBoolLabels.set(new BooleanBuilder());

                    cat.getValue().stream().forEach(label -> {
                        Long ebLabelNum = Long.valueOf(label);
                        orBoolLabels
                            .get().or(
                                new BooleanBuilder()
                                    .andAnyOf(
                                        qEbOrder.listLabels.startsWith(ebLabelNum + ","),
                                        qEbOrder.listLabels.contains("," + ebLabelNum + ","),
                                        qEbOrder.listLabels.endsWith("," + ebLabelNum),
                                        qEbOrder.listLabels.eq(ebLabelNum + "")));
                    });

                    if (orBoolLabels.get().hasValue()) boolLabels.and(orBoolLabels.get());
                });

            if (boolLabels.hasValue()) where.andAnyOf(qEbOrder.listLabels.isNotNull().andAnyOf(boolLabels));
        }

        if (getListCustomFieldsValues() != null && !getListCustomFieldsValues().isEmpty()) {
            Predicate[] predicats = new Predicate[getListCustomFieldsValues().size()];

            for (int i = 0; i < getListCustomFieldsValues().size(); i++) {
                predicats[i] = qEbOrder.listCustomFieldsValueFlat.contains(getListCustomFieldsValues().get(i));
            }

            where.andAnyOf(predicats);
        }

        if (this.getListCampaignCode() != null && !this.getListCampaignCode().isEmpty()) {
            where.and(qEbOrder.campaignCode.in(this.getListCampaignCode()));
        }

        if (this.getListCampaignName() != null && !this.getListCampaignName().isEmpty()) {
            where.and(qEbOrder.campaignName.in(this.getListCampaignName()));
        }

        if (this.getListNumOrderCustomer() != null && !this.getListNumOrderCustomer().isEmpty()) {
            where.and(qEbOrder.numOrderCustomer.in(this.getListNumOrderCustomer()));
        }

        if (this.getListOrderCustomerName() != null && !this.getListOrderCustomerName().isEmpty()) {
            where.and(qEbOrder.orderCustomerName.in(this.getListOrderCustomerName()));
        }

        if (this.getListOrderCustomerCode() != null && !this.getListOrderCustomerCode().isEmpty()) {
            where.and(qEbOrder.orderCustomerCode.in(this.getListOrderCustomerCode()));
        }

        if (this.getListRefClientDelivered() != null && !this.getListRefClientDelivered().isEmpty()) {
            where.and(qPartyDest.reference.in(this.getListRefClientDelivered()));
        }

        if (this.getListNameClientDelivered() != null && !this.getListNameClientDelivered().isEmpty()) {
            where.and(qPartyDest.company.in(this.getListNameClientDelivered()));
        }

        if (this.getListCountryClientDelivered() != null && !this.getListCountryClientDelivered().isEmpty()) {
            where.and(qCountryDest.libelle.in(this.getListCountryClientDelivered()));
        }

        if (this.getListRefDelivery() != null && !this.getListRefDelivery().isEmpty()) {
            where.and(qEbLivraison.refDelivery.in(this.getListRefDelivery()));
        }

        if (this.getListRefArticle() != null && !this.getListRefArticle().isEmpty()) {
            where.and(qEbOrderLine.refArticle.in(this.getListRefArticle()));
        }

        if (this.getListNumOrderEDI() != null && !this.getListNumOrderEDI().isEmpty()) {
            where.and(qEbOrder.numOrderEDI.in(this.getListNumOrderEDI()));
        }

        if (this.getListCrossDock() != null && !this.getListCrossDock().isEmpty()) {
            where.and(qEbOrder.crossDock.in(this.getListCrossDock()));
        }

        if (this.getListNumOrderSAP() != null && !this.getListNumOrderSAP().isEmpty()) {
            where.and(qEbOrder.numOrderSAP.in(this.getListNumOrderSAP()));
        }

        if (this.getListOrderStatus() != null && !this.getListOrderStatus().isEmpty()) {
            where.and(qEbOrder.orderStatus.in(this.getListOrderStatus()));
        }

        if (this.getListcustomerOrderReference() != null && !this.getListcustomerOrderReference().isEmpty()) {
            where.and(qEbOrder.customerOrderReference.in(this.getListcustomerOrderReference()));
        }

        if (this.getListcustomFields() != null && !this.getListcustomFields().isEmpty()) {
            where.and(qEbOrder.customFields.in(this.getListcustomFields()));
        }

        if (this.customerCreationDate != null) {
            w
                .andAnyOf(
                    qEbOrder.customerCreationDate.eq(customerCreationDate),
                    qEbOrder.customerCreationDate.in(this.getCustomerCreationDate()));
            where.and(w);
        }

        if (this.getListOrigins() != null && !this.getListOrigins().isEmpty()) {
            where.and(qPartyOrigin.xEcCountry().ecCountryNum.in(this.getListOrigins()));
        }

        if (this.getListPartyDestination() != null && !this.getListPartyDestination().isEmpty()) {
            where.and(qPartyDest.xEcCountry().ecCountryNum.in(this.getListPartyDestination()));
        }

        if (this.getListOwnerOfTheRequests() != null && !this.getListOwnerOfTheRequests().isEmpty()) {
            where.and(qEbOrder.xEbOwnerOfTheRequest().ebUserNum.in(this.getListOwnerOfTheRequests()));
        }

        if (this.creationDateFrom != null) {
            w
                .andAnyOf(
                    qEbOrder.customerCreationDate.eq(truncDate(this.creationDateFrom)),
                    qEbOrder.customerCreationDate.after(truncDate(this.creationDateFrom)));
            where.and(w);
        }

        if (this.creationDateTo != null) {
            w
                .andAnyOf(
                    qEbOrder.customerCreationDate.eq(truncDate(this.creationDateTo)),
                    qEbOrder.customerCreationDate.before(truncDate(this.creationDateTo)));
            where.and(w);
        }

        if (this.deliveryDateFrom != null) {
            w
                .andAnyOf(
                    qEbOrder.deliveryDate.eq(truncDate(this.deliveryDateFrom)),
                    qEbOrder.deliveryDate.after(truncDate(this.deliveryDateFrom)));
            where.and(w);
        }

        if (this.deliveryDateTo != null) {
            w
                .andAnyOf(
                    qEbOrder.deliveryDate.eq(truncDate(this.deliveryDateTo)),
                    qEbOrder.deliveryDate.before(truncDate(this.deliveryDateTo)));
            where.and(w);
        }

        if (this.getSearchterm() != null) {
            String value = this.getSearchterm().toLowerCase();
            Date dt = null;

            if (value != null && !value.isEmpty()) {
                value = value.trim().toLowerCase();

                if (Pattern.compile("([0-9]{2}/){2}[0-9]{4}").matcher(value).matches()) {
                    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                    try {
                        dt = formatter.parse(value);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                //@formatter:off
        				w
        					.andAnyOf(
        						qEbOrder.xEbOwnerOfTheRequest().nom.containsIgnoreCase(value),
        						qEbOrder.xEbOwnerOfTheRequest().prenom.containsIgnoreCase(value),
        						qEbOrder.reference.containsIgnoreCase(value),
        						qEbOrder.customerOrderReference.containsIgnoreCase(value),
        						qEbOrder.numOrderSAP.containsIgnoreCase(value),
        						qEbOrder.numOrderEDI.containsIgnoreCase(value),
        						qEbOrder.numOrderCustomer.containsIgnoreCase(value),
        						qEbOrder.campaignCode.containsIgnoreCase(value),
        						qEbOrder.campaignName.containsIgnoreCase(value),
        						qEbOrder.customerCreationDate.stringValue().containsIgnoreCase(value),
        						qEbOrder.deliveryDate.stringValue().containsIgnoreCase(value),
        						qCountryDest.libelle.containsIgnoreCase(value),
        						qPartyDest.company.stringValue().containsIgnoreCase(value),
        						qPartyDest.reference.stringValue().containsIgnoreCase(value),
        						qPartyOrigin.xEcCountry().libelle.containsIgnoreCase(value),
        						qPartyOrigin.company.stringValue().containsIgnoreCase(value),
        						qPartyOrigin.reference.stringValue().containsIgnoreCase(value),
        						qEbEtablissement.nom.containsIgnoreCase(value),
        						qEbOrderLine.serialNumber.containsIgnoreCase(value),
                    qEbOrderLine.partNumber.containsIgnoreCase(value),
                    qEbOrderLine.itemNumber.containsIgnoreCase(value)
        					);
        				//@formatter:on

                if (dt != null) {
                    w.or(qEbOrder.customerCreationDate.stringValue().toLowerCase().contains(DATE_FORMATTER.format(dt)));
                    w.or(qEbOrder.deliveryDate.stringValue().toLowerCase().contains(DATE_FORMATTER.format(dt)));
                }

                Optional<Integer> optionalCodeStatus = StatusOrder.getCodeByLibelle(value);
                if (optionalCodeStatus.isPresent()) {

                    w.or(qEbOrder.orderStatus.eq(optionalCodeStatus.get()));

                }

                where.and(w);
            }

        }

        if (this.pageNumber != null && this.size != null && !isCount && !this.extractData) {
            query.limit(this.size).offset(this.getPageNumber() * (this.getSize() != null ? this.getSize() : 1));
        }

        if (!this.isCount && applyorder) {

            if (this.orderedColumn != null) {

                if (this.ascendant) {
                    if (this.orderedColumn.contains("Flag")) this.orderedColumn = "ebOrderNum";
                    query.orderBy(this.orderMapASC.get(this.orderedColumn));
                }
                else {
                    if (this.orderedColumn.contains("Flag")) this.orderedColumn = "ebOrderNum";
                    query.orderBy(this.orderMapDESC.get(this.orderedColumn));
                }

            }
            else {
                this.orderedColumn = "ebOrderNum";
                query.orderBy(this.orderMapDESC.get(this.orderedColumn));
            }

        }

        query.where(where);

        return query;
    }

    public JPAQuery<EbOrder> applyCriteriaAutoComplete(JPAQuery<EbOrder> query) {
        JPAQuery<EbOrder> resultQuery = query;

        Initialize(qEbOrder);

        if (this.numOrderSAP != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qEbOrder.numOrderSAP.toLowerCase().contains(this.numOrderSAP),
                    qEbOrder.numOrderSAP.contains(this.numOrderSAP));
            resultQuery.where(w);
        }

        if (this.refArticle != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qEbOrderLine.refArticle.toLowerCase().contains(this.refArticle),
                    qEbOrderLine.refArticle.contains(this.refArticle));
            resultQuery.where(w);
        }

        if (this.numOrderEDI != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qEbOrder.numOrderEDI.toLowerCase().contains(this.numOrderEDI),
                    qEbOrder.numOrderEDI.contains(this.numOrderEDI));
            resultQuery.where(w);
        }

        if (this.crossDock != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qEbOrder.crossDock.toLowerCase().contains(this.crossDock),
                    qEbOrder.crossDock.contains(this.crossDock));
            resultQuery.where(w);
        }

        if (this.numOrderCustomer != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qEbOrder.numOrderCustomer.toLowerCase().contains(this.numOrderCustomer),
                    qEbOrder.numOrderCustomer.contains(this.numOrderCustomer));
            resultQuery.where(w);
        }

        if (this.orderCustomerName != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qEbOrder.orderCustomerName.toLowerCase().contains(this.orderCustomerName.toLowerCase()),
                    qEbOrder.orderCustomerName.contains(this.orderCustomerName));
            resultQuery.where(w);
        }

        if (this.orderCustomerCode != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qEbOrder.orderCustomerCode.toLowerCase().contains(this.orderCustomerCode),
                    qEbOrder.orderCustomerCode.contains(this.orderCustomerCode));
            resultQuery.where(w);
        }

        if (this.nameClientDelivered != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qPartyDest.company.toLowerCase().contains(this.nameClientDelivered.toLowerCase()),
                    qPartyDest.company.contains(this.nameClientDelivered));
            resultQuery.where(w);
        }

        if (this.countryClientDelivered != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qCountryDest.libelle.toLowerCase().contains(this.countryClientDelivered.toLowerCase()),
                    qCountryDest.libelle.contains(this.countryClientDelivered));
            resultQuery.where(w);
        }

        if (this.refDelivery != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qEbLivraison.refDelivery.toLowerCase().contains(this.refDelivery.toLowerCase()),
                    qEbLivraison.refDelivery.contains(this.refDelivery));
            resultQuery.where(w);
        }

        if (this.refClientDelivered != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qPartyDest.reference.toLowerCase().contains(this.refClientDelivered),
                    qPartyDest.reference.contains(this.refClientDelivered));
            resultQuery.where(w);
        }

        if (this.campaignCode != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qEbOrder.campaignCode.toLowerCase().contains(this.campaignCode),
                    qEbOrder.campaignCode.contains(this.campaignCode));
            resultQuery.where(w);
        }

        if (this.campaignName != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qEbOrder.campaignName.toLowerCase().contains(this.campaignName),
                    qEbOrder.campaignName.contains(this.campaignName));
            resultQuery.where(w);
        }

        if (this.pageNumber != null && this.size != null && !isCount) {
            resultQuery.limit(this.size).offset(this.getPageNumber() * (this.getSize() != null ? this.getSize() : 1));
        }

        if (this.orderedColumn != null) {

            switch (this.orderedColumn) {
                // todo add to a list a get the corresponding querydsl
                // expression
                case "refArticle":
                    resultQuery.groupBy(qEbOrderLine.ebDelOrderLineNum);
                    break;

                case "refDelivery":
                    resultQuery.groupBy(qEbLivraison.ebDelLivraisonNum);
                    break;

                case "refClientDelivered":
                    resultQuery.groupBy(qPartyDest.reference);
                    break;

                case "nameClientDelivered":
                    resultQuery.groupBy(qPartyDest.company);
                    break;

                default:
                    resultQuery.groupBy(qEbOrder.ebDelOrderNum);
                    break;
            }

            resultQuery.groupBy(qEbOrder.ebDelOrderNum);
            resultQuery.orderBy(this.orderMapASC.get(this.orderedColumn));
        }
        else {
            this.orderedColumn = "ebOrderNum";
            resultQuery.groupBy(qEbOrder.ebDelOrderNum);
            resultQuery.orderBy(this.orderMapASC.get(this.orderedColumn));
        }

        return resultQuery;
    }

    public Long getEbdelOrderNum() {
        return ebDelOrderNum;
    }

    public void setEbDelOrderNum(Long ebDelOrderNum) {
        this.ebDelOrderNum = ebDelOrderNum;
    }

    public String getNumOrderSAP() {
        return numOrderSAP;
    }

    public void setNumOrderSAP(String numOrderSAP) {
        this.numOrderSAP = numOrderSAP;
    }

    public String getNumOrderEDI() {
        return numOrderEDI;
    }

    public void setNumOrderEDI(String numOrderEDI) {
        this.numOrderEDI = numOrderEDI;
    }

    public String getNumOrderCustomer() {
        return numOrderCustomer;
    }

    public void setNumOrderCustomer(String numOrderCustomer) {
        this.numOrderCustomer = numOrderCustomer;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public boolean isCount() {
        return isCount;
    }

    public void setCount(boolean isCount) {
        this.isCount = isCount;
    }

    public List<String> getListCampaignCode() {
        return listCampaignCode;
    }

    public void setListCampaignCode(List<String> listCampaignCode) {
        this.listCampaignCode = listCampaignCode;
    }

    public List<String> getListCampaignName() {
        return listCampaignName;
    }

    public void setListCampaignName(List<String> listCampaignName) {
        this.listCampaignName = listCampaignName;
    }

    public List<String> getListNumOrderCustomer() {
        return listNumOrderCustomer;
    }

    public void setListNumOrderCustomer(List<String> listNumOrderCustomer) {
        this.listNumOrderCustomer = listNumOrderCustomer;
    }

    public List<String> getListNumOrderEDI() {
        return listNumOrderEDI;
    }

    public void setListNumOrderEDI(List<String> listNumOrderEDI) {
        this.listNumOrderEDI = listNumOrderEDI;
    }

    public List<String> getListNumOrderSAP() {
        return listNumOrderSAP;
    }

    public void setListNumOrderSAP(List<String> listNumOrderSAP) {
        this.listNumOrderSAP = listNumOrderSAP;
    }

    public List<Integer> getListOrderStatus() {
        return listOrderStatus;
    }

    public void setListOrderStatus(List<Integer> listOrderStatus) {
        this.listOrderStatus = listOrderStatus;
    }

    public Date getCreationDateFrom() {
        return creationDateFrom;
    }

    public Date getDeliveryDateFrom() {
        return deliveryDateFrom;
    }

    public Date getCreationDateTo() {
        return creationDateTo;
    }

    public Date getDeliveryDateTo() {
        return deliveryDateTo;
    }

    public void setCreationDateFrom(Date creationDateFrom) {
        this.creationDateFrom = creationDateFrom;
    }

    public void setDeliveryDateFrom(Date deliveryDateFrom) {
        this.deliveryDateFrom = deliveryDateFrom;
    }

    public void setCreationDateTo(Date creationDateTo) {
        this.creationDateTo = creationDateTo;
    }

    public void setDeliveryDateTo(Date deliveryDateTo) {
        this.deliveryDateTo = deliveryDateTo;
    }

    public List<String> getListRefArticle() {
        return listRefArticle;
    }

    public void setListRefArticle(List<String> listRefArticle) {
        this.listRefArticle = listRefArticle;
    }

    public String getRefArticle() {
        return refArticle;
    }

    public void setRefArticle(String refArticle) {
        this.refArticle = refArticle;
    }

    public List<String> getListOrderCustomerName() {
        return listOrderCustomerName;
    }

    public void setListOrderCustomerName(List<String> listOrderCustomerName) {
        this.listOrderCustomerName = listOrderCustomerName;
    }

    public String getOrderCustomerName() {
        return orderCustomerName;
    }

    public void setOrderCustomerName(String orderCustomerName) {
        this.orderCustomerName = orderCustomerName;
    }

    public String getOrderCustomerCode() {
        return orderCustomerCode;
    }

    public void setOrderCustomerCode(String orderCustomerCode) {
        this.orderCustomerCode = orderCustomerCode;
    }

    public List<String> getListOrderCustomerCode() {
        return listOrderCustomerCode;
    }

    public void setListOrderCustomerCode(List<String> listOrderCustomerCode) {
        this.listOrderCustomerCode = listOrderCustomerCode;
    }

    public String getNameClientDelivered() {
        return nameClientDelivered;
    }

    public void setNameClientDelivered(String nameClientDelivered) {
        this.nameClientDelivered = nameClientDelivered;
    }

    public String getRefClientDelivered() {
        return refClientDelivered;
    }

    public void setRefClientDelivered(String refClientDelivered) {
        this.refClientDelivered = refClientDelivered;
    }

    public List<String> getListNameClientDelivered() {
        return listNameClientDelivered;
    }

    public void setListNameClientDelivered(List<String> listNameClientDelivered) {
        this.listNameClientDelivered = listNameClientDelivered;
    }

    public List<String> getListRefClientDelivered() {
        return listRefClientDelivered;
    }

    public void setListRefClientDelivered(List<String> listRefClientDelivered) {
        this.listRefClientDelivered = listRefClientDelivered;
    }

    public List<String> getListCountryClientDelivered() {
        return listCountryClientDelivered;
    }

    public void setListCountryClientDelivered(List<String> listCountryClientDelivered) {
        this.listCountryClientDelivered = listCountryClientDelivered;
    }

    public String getCountryClientDelivered() {
        return countryClientDelivered;
    }

    public void setCountryClientDelivered(String countryClientDelivered) {
        this.countryClientDelivered = countryClientDelivered;
    }

    public String getRefDelivery() {
        return refDelivery;
    }

    public void setRefDelivery(String refDelivery) {
        this.refDelivery = refDelivery;
    }

    public List<String> getListRefDelivery() {
        return listRefDelivery;
    }

    public void setListRefDelivery(List<String> listRefDelivery) {
        this.listRefDelivery = listRefDelivery;
    }

    public List<String> getListCrossDock() {
        return listCrossDock;
    }

    public void setListCrossDock(List<String> listCrossDock) {
        this.listCrossDock = listCrossDock;
    }

    public void setCrossDock(String crossDock) {
        this.crossDock = crossDock;
    }

    public String getCrossDock() {
        return crossDock;
    }

    public QEbOrder getQEbOrder() {
        return qEbOrder;
    }

    public void setQEbOrder(QEbOrder qEbOrder) {
        this.qEbOrder = qEbOrder;
    }

    public QEbLivraison getQEbLivraison() {
        return qEbLivraison;
    }

    public void setQEbLivraison(QEbLivraison qEbLivraison) {
        this.qEbLivraison = qEbLivraison;
    }

    public QEbOrderLine getQEbOrderLine() {
        return qEbOrderLine;
    }

    public void setQEbOrderLine(QEbOrderLine qEbOrderLine) {
        this.qEbOrderLine = qEbOrderLine;
    }

    public QEcCountry getQEcCountry() {
        return qEcCountry;
    }

    public void setQEcCountry(QEcCountry qEcCountry) {
        this.qEcCountry = qEcCountry;
    }

    public QEbParty getQPartyDest() {
        return qPartyDest;
    }

    public void setQPartyDest(QEbParty qPartyDest) {
        this.qPartyDest = qPartyDest;
    }

    public QEbParty getQPartyOrigin() {
        return qPartyOrigin;
    }

    public void setQPartyOrigin(QEbParty qPartyOrigin) {
        this.qPartyOrigin = qPartyOrigin;
    }

    public QEbParty getQPartySale() {
        return qPartySale;
    }

    public void setQPartySale(QEbParty qPartySale) {
        this.qPartySale = qPartySale;
    }

    public QEcCountry getQCountryOrigin() {
        return qCountryOrigin;
    }

    public void setQCountryOrigin(QEcCountry qCountryOrigin) {
        this.qCountryOrigin = qCountryOrigin;
    }

    public QEcCountry getQCountryDest() {
        return qCountryDest;
    }

    public void setQCountryDest(QEcCountry qCountryDest) {
        this.qCountryDest = qCountryDest;
    }

    public QEcCountry getQCountrySale() {
        return qCountrySale;
    }

    public void setQCountrySale(QEcCountry qCountrySale) {
        this.qCountrySale = qCountrySale;
    }

    public QEbEtablissement getQEbEtablissement() {
        return qEbEtablissement;
    }

    public void setQEbEtablissement(QEbEtablissement qEbEtablissement) {
        this.qEbEtablissement = qEbEtablissement;
    }

    public QEbParty getqPartyVendor() {
        return qPartyVendor;
    }

    public void setqPartyVendor(QEbParty qPartyVendor) {
        this.qPartyVendor = qPartyVendor;
    }

    public QEbParty getqPartyIssuer() {
        return qPartyIssuer;
    }

    public void setqPartyIssuer(QEbParty qPartyIssuer) {
        this.qPartyIssuer = qPartyIssuer;
    }

    public Boolean getIncludeLines() {
        return includeLines;
    }

    public void setIncludeLines(Boolean includeLines) {
        this.includeLines = includeLines;
    }

    public EbUser getxEbOwnerOfTheRequest() {
        return xEbOwnerOfTheRequest;
    }

    public void setxEbOwnerOfTheRequest(EbUser xEbOwnerOfTheRequest) {
        this.xEbOwnerOfTheRequest = xEbOwnerOfTheRequest;
    }

    public EbParty getxEbPartyOrigin() {
        return xEbPartyOrigin;
    }

    public void setxEbPartyOrigin(EbParty xEbPartyOrigin) {
        this.xEbPartyOrigin = xEbPartyOrigin;
    }

    public EbParty getxEbPartyDestination() {
        return xEbPartyDestination;
    }

    public void setxEbPartyDestination(EbParty xEbPartyDestination) {
        this.xEbPartyDestination = xEbPartyDestination;
    }

    public String getCustomerOrderReference() {
        return customerOrderReference;
    }

    public void setCustomerOrderReference(String customerOrderReference) {
        this.customerOrderReference = customerOrderReference;
    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;
    }

    public List<String> getListcustomerOrderReference() {
        return listcustomerOrderReference;
    }

    public void setListcustomerOrderReference(List<String> listcustomerOrderReference) {
        this.listcustomerOrderReference = listcustomerOrderReference;
    }

    public List<String> getListcustomFields() {
        return listcustomFields;
    }

    public void setListcustomFields(List<String> listcustomFields) {
        this.listcustomFields = listcustomFields;
    }

    public QEbUser getqEbUser() {
        return qEbUser;
    }

    public void setqEbUser(QEbUser qEbUser) {
        this.qEbUser = qEbUser;
    }

    public Date getCustomerCreationDate() {
        return customerCreationDate;
    }

    public void setCustomerCreationDate(Date customerCreationDate) {
        this.customerCreationDate = customerCreationDate;
    }

    public List<Integer> getListPartyOrigin() {
        return listPartyOrigin;
    }

    public void setListPartyOrigin(List<Integer> listPartyOrigin) {
        this.listPartyOrigin = listPartyOrigin;
    }
}
