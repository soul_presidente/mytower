package com.adias.mytowereasy.cptm.service;

import java.util.List;

import com.adias.mytowereasy.cptm.dto.EbCptmRegimeTemporaireDTO;
import com.adias.mytowereasy.cptm.model.EbCptmRegimeTemporaire;
import com.adias.mytowereasy.cptm.model.SearchCriteriaCPTM;


public interface RegimeTemporaireService {
    public EbCptmRegimeTemporaire saveRegime(EbCptmRegimeTemporaireDTO regime);

    public Boolean deleteRegime(Integer regimeNum);

    List<EbCptmRegimeTemporaireDTO> convertEntitiesToDTOs(List<EbCptmRegimeTemporaire> dbList);

    public List<EbCptmRegimeTemporaireDTO> listRegimeTemporaire(SearchCriteriaCPTM criteria);

    public Long countListRegimeTemporaire(SearchCriteriaCPTM criteria);
}
