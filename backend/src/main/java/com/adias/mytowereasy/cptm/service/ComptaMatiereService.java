package com.adias.mytowereasy.cptm.service;

import java.util.List;

import com.adias.mytowereasy.cptm.dto.EbCptmProcedureDTO;
import com.adias.mytowereasy.cptm.dto.EbCptmRegimeSelectionDTO;
import com.adias.mytowereasy.cptm.model.CPTMEnumeration.ProcedureStatus;
import com.adias.mytowereasy.cptm.model.EbCptmProcedure;
import com.adias.mytowereasy.cptm.model.SearchCriteriaCPTM;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbEmailHistorique;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.qm.SemiAutoMail;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.util.email.Email;


public interface ComptaMatiereService {
    public List<EbCptmProcedureDTO> listProcedureDto(SearchCriteriaCPTM criteria);

    public Long countListProcedure(SearchCriteriaCPTM criteria);

    public EbCptmProcedure inlineProcedureDashboardUpdate(EbCptmProcedureDTO sent);

    public List<EbCptmRegimeSelectionDTO> listRegimesAndProceduresForSelection();

    public void generateProcedures(EbDemande ebDemande, List<EbTtTracing> ebTtTracings);

    public Integer getNextProcedureNum();

    List<?> advancedSearchAutocomplete(String term, Integer type, Boolean searchField);

    public SemiAutoMail generateSemiAutoMailTemplateForProcedure(Integer mailId, Integer procedureNum, EbUser sender);

    SemiAutoMail generateSemiAutoMailTemplateForProcedure(Integer mailId, EbCptmProcedure procedure, EbUser sender);

    public EbEmailHistorique sendSemiAutoMail(Email mailToSend, Integer procedureNum);

    public boolean areAllCompleted(List<EbCptmProcedure> procedures);

    public void emailReminderAuto();

    void sendEmailAutoReminder(EbCptmProcedure procedure);
}
