package com.adias.mytowereasy.cptm.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.adias.mytowereasy.controller.SuperControler;
import com.adias.mytowereasy.cptm.dto.EbCptmProcedureDTO;
import com.adias.mytowereasy.cptm.dto.EbCptmRegimeSelectionDTO;
import com.adias.mytowereasy.cptm.dto.EbCptmRegimeTemporaireDTO;
import com.adias.mytowereasy.cptm.model.EbCptmProcedure;
import com.adias.mytowereasy.cptm.model.SearchCriteriaCPTM;
import com.adias.mytowereasy.dto.GenericTableResponseDTO;
import com.adias.mytowereasy.model.EbEmailHistorique;
import com.adias.mytowereasy.model.qm.SemiAutoMail;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.CustomFieldService;
import com.adias.mytowereasy.util.email.Email;


@RestController
@RequestMapping(value = "api/compta-matiere")
@CrossOrigin("*")
public class ComptaMatiereController extends SuperControler {
    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    CustomFieldService customFieldService;

    ////// Regimes Temporaires

    @RequestMapping(value = "list-regime-temporaire-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object>
        listRegimeTemporaireTable(@RequestBody(required = false) SearchCriteriaCPTM criteria)

    {
        List<EbCptmRegimeTemporaireDTO> dtos = regimeTemporaireService.listRegimeTemporaire(criteria);
        Long count = regimeTemporaireService.countListRegimeTemporaire(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", dtos);
        return result;
    }

    @PostMapping(value = "save-regime-temporaire")
    public EbCptmRegimeTemporaireDTO saveRegime(@RequestBody EbCptmRegimeTemporaireDTO ebRegime) {
        return new EbCptmRegimeTemporaireDTO(regimeTemporaireService.saveRegime(ebRegime));
    }

    @DeleteMapping(value = "delete-regime-temporaire")
    public Boolean deleteRegime(@RequestParam Integer regimeNum) {
        return regimeTemporaireService.deleteRegime(regimeNum);
    }

    ////// Procedures

    @RequestMapping(value = "list-procedure-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody GenericTableResponseDTO<EbCptmProcedureDTO>
        listProceduresTable(@RequestBody(required = false) SearchCriteriaCPTM criteria)
            throws JsonParseException,
            JsonMappingException,
            IOException {

        if (criteria.getSearchInput() != null && criteria.getSearchInput().length() > 0) {
            criteria
                .setSearchInput(criteria.getSearchInput(), customFieldService.getCustomFields(criteria.getEbUserNum()));
        }

        final List<EbCptmProcedureDTO> dtos = comptaMatiereService.listProcedureDto(criteria);
        final Long count = comptaMatiereService.countListProcedure(criteria);

        return new GenericTableResponseDTO<>(dtos, count);
    }

    @PostMapping(value = "procedure-dashboard-inline-update")
    public EbCptmProcedureDTO inlineDashboardUpdate(@RequestBody EbCptmProcedureDTO sent) {
        EbCptmProcedure entity = comptaMatiereService.inlineProcedureDashboardUpdate(sent);
        return new EbCptmProcedureDTO(entity);
    }

    ////// Creation form
    @GetMapping(value = "list-regimes-procedures")
    public List<EbCptmRegimeSelectionDTO> listRegimesAndProcedures() {
        return comptaMatiereService.listRegimesAndProceduresForSelection();
    }

    ////// Generation de mail
    @GetMapping(value = "mail-generation-procedure")
    public @ResponseBody SemiAutoMail generateSemiAutoMailTemplateForProcedure(
        @RequestParam("mailId") Integer mailId,
        @RequestParam("procedureNum") Integer procedureNum) {
        return comptaMatiereService
            .generateSemiAutoMailTemplateForProcedure(mailId, procedureNum, connectedUserService.getCurrentUser());
    }

    @RequestMapping(value = "/send-semi-auto-mail", method = RequestMethod.POST)
    @ResponseBody
    public EbEmailHistorique
        sendSemiAutoMail(@RequestBody Email mailToSend, @RequestParam("procedureNum") Integer procedureNum) {
        return comptaMatiereService.sendSemiAutoMail(mailToSend, procedureNum);
    }

    @Scheduled(cron = "${mytower.cptm.cron-email-reminder:-}")
    @GetMapping(value = "email-reminder-auto")
    public @ResponseBody String emailReminderAuto() {
        comptaMatiereService.emailReminderAuto();
        return "FINISHED";
    }

    @RequestMapping(value = "/advancedSearchAutocomplete", method = RequestMethod.GET)
    public @ResponseBody List<Map<String, Object>> advancedSearchAutocomplete(
        @RequestParam(required = false, value = "term") String term,
        @RequestParam(required = false, value = "field") Integer field,
        @RequestParam(required = false, value = "searchField") Boolean searchField) {
        List<?> dbList = comptaMatiereService.advancedSearchAutocomplete(term, field, searchField);

        // Generate a json array of object containing value field, we only need
        // this at this time
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();

        for (Object dbItem: dbList) {
            Map<String, Object> rItem = new HashMap<String, Object>();
            rItem.put("value", dbItem);
            result.add(rItem);
        }

        return result;
    }
}
