package com.adias.mytowereasy.cptm.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbTimeStamps;


@Entity
@Table(name = "eb_cptm_regime_temporaire", indexes = {
    @Index(name = "idx_eb_cptm_regime_temporaire_leg1code", columnList = "leg1code"),
    @Index(name = "idx_eb_cptm_regime_temporaire_leg2code", columnList = "leg2code"),
    @Index(name = "idx_eb_cptm_regime_temporaire_type", columnList = "type"),
    @Index(name = "idx_eb_cptm_regime_temporaire_x_eb_company", columnList = "x_eb_company"),
    @Index(name = "idx_eb_cptm_regime_temporaire_deleted", columnList = "deleted"),
})
public class EbCptmRegimeTemporaire extends EbTimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebCptmRegimeTemporaireNum;

    @Column(name = "type")
    private String type;

    @Column(name = "description")
    private String description;

    @Column(name = "deadline_days")
    private Integer deadlineDays;

    @Column(name = "alert1days")
    private Integer alert1Days;

    @Column(name = "alert2days")
    private Integer alert2Days;

    @Column(name = "alert3days")
    private Integer alert3Days;

    @Column(name = "leg1name")
    private String leg1Name;

    @Column(name = "leg1code")
    private String leg1Code;

    @Column(name = "leg2name")
    private String leg2Name;

    @Column(name = "leg2code")
    private String leg2Code;

    @Column(name = "deleted")
    @ColumnDefault("false")
    private Boolean deleted;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company", insertable = true)
    private EbCompagnie ebCompagnie;

    public EbCptmRegimeTemporaire() {
    }

    public Integer getEbCptmRegimeTemporaireNum() {
        return ebCptmRegimeTemporaireNum;
    }

    public void setEbCptmRegimeTemporaireNum(Integer ebCptmRegimeTemporaireNum) {
        this.ebCptmRegimeTemporaireNum = ebCptmRegimeTemporaireNum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDeadlineDays() {
        return deadlineDays;
    }

    public void setDeadlineDays(Integer deadlineDays) {
        this.deadlineDays = deadlineDays;
    }

    public String getLeg1Name() {
        return leg1Name;
    }

    public Integer getAlert1Days() {
        return alert1Days;
    }

    public void setAlert1Days(Integer alert1Days) {
        this.alert1Days = alert1Days;
    }

    public Integer getAlert2Days() {
        return alert2Days;
    }

    public void setAlert2Days(Integer alert2Days) {
        this.alert2Days = alert2Days;
    }

    public Integer getAlert3Days() {
        return alert3Days;
    }

    public void setAlert3Days(Integer alert3Days) {
        this.alert3Days = alert3Days;
    }

    public void setLeg1Name(String leg1Name) {
        this.leg1Name = leg1Name;
    }

    public String getLeg1Code() {
        return leg1Code;
    }

    public void setLeg1Code(String leg1Code) {
        this.leg1Code = leg1Code;
    }

    public String getLeg2Name() {
        return leg2Name;
    }

    public void setLeg2Name(String leg2Name) {
        this.leg2Name = leg2Name;
    }

    public String getLeg2Code() {
        return leg2Code;
    }

    public void setLeg2Code(String leg2Code) {
        this.leg2Code = leg2Code;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }
}
