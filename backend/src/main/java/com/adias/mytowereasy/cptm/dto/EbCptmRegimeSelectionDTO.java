package com.adias.mytowereasy.cptm.dto;

import com.querydsl.core.annotations.QueryProjection;


public class EbCptmRegimeSelectionDTO {
    private String label;
    private Integer ebCptmProcedureNum;
    private Integer ebCptmRegimeTemporaireNum;
    private String procedureOperationNumber;
    private String leg1RefUnit;
    private String labelForSearch;

    public EbCptmRegimeSelectionDTO() {
    }

    @QueryProjection
    public EbCptmRegimeSelectionDTO(
        String label,
        Integer ebCptmProcedureNum,
        Integer ebCptmRegimeTemporaireNum,
        String procedureOperationNumber,
        String leg1RefUnit) {
        this.label = label;
        this.ebCptmProcedureNum = ebCptmProcedureNum;
        this.ebCptmRegimeTemporaireNum = ebCptmRegimeTemporaireNum;
        this.procedureOperationNumber = procedureOperationNumber;
        this.labelForSearch = label + " " + (procedureOperationNumber != null ? procedureOperationNumber : "") + " "
            + (leg1RefUnit != null ? leg1RefUnit : "");
        this.leg1RefUnit = leg1RefUnit != null ? leg1RefUnit : "";
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getEbCptmProcedureNum() {
        return ebCptmProcedureNum;
    }

    public void setEbCptmProcedureNum(Integer ebCptmProcedureNum) {
        this.ebCptmProcedureNum = ebCptmProcedureNum;
    }

    public Integer getEbCptmRegimeTemporaireNum() {
        return ebCptmRegimeTemporaireNum;
    }

    public void setEbCptmRegimeTemporaireNum(Integer ebCptmRegimeTemporaireNum) {
        this.ebCptmRegimeTemporaireNum = ebCptmRegimeTemporaireNum;
    }

    public String getProcedureOperationNumber() {
        return procedureOperationNumber;
    }

    public void setProcedureOperationNumber(String procedureOperationNumber) {
        this.procedureOperationNumber = procedureOperationNumber;
    }

    public String getLabelForSearch() {
        return labelForSearch;
    }

    public void setLabelForSearch(String labelForSearch) {
        this.labelForSearch = labelForSearch;
    }

    public String getLeg1RefUnit() {
        return leg1RefUnit;
    }

    public void setLeg1RefUnit(String leg1RefUnit) {
        this.leg1RefUnit = leg1RefUnit;
    }
}
