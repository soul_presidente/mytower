package com.adias.mytowereasy.cptm.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.cptm.dao.DaoCptmRegimeTemporaire;
import com.adias.mytowereasy.cptm.dto.EbCptmRegimeTemporaireDTO;
import com.adias.mytowereasy.cptm.model.EbCptmRegimeTemporaire;
import com.adias.mytowereasy.cptm.model.SearchCriteriaCPTM;
import com.adias.mytowereasy.service.MyTowerService;


@Service
public class RegimeTemporaireServiceImpl extends MyTowerService implements RegimeTemporaireService {
    @Override
    public EbCptmRegimeTemporaire saveRegime(EbCptmRegimeTemporaireDTO regime) {
        EbCptmRegimeTemporaire entity = new EbCptmRegimeTemporaire();

        if (regime.getEbCptmRegimeTemporaireNum() != null) {
            entity = ebCptmRegimeTemporaireRepository
                .findById(regime.getEbCptmRegimeTemporaireNum()).orElse(new EbCptmRegimeTemporaire());
        }

        // Order alerts to have first on 1 and latest on 3
        List<Integer> alerts = new ArrayList<>();

        if (regime.getAlert1Days() != null) {
            alerts.add(regime.getAlert1Days());
        }

        if (regime.getAlert2Days() != null) {
            alerts.add(regime.getAlert2Days());
        }

        if (regime.getAlert3Days() != null) {
            alerts.add(regime.getAlert3Days());
        }

        alerts.sort(Comparator.naturalOrder());
        Collections.reverse(alerts);

        entity.setAlert1Days(null);

        if (alerts.size() > 0) {
            entity.setAlert1Days(alerts.get(0));
        }

        entity.setAlert2Days(null);

        if (alerts.size() > 1) {
            entity.setAlert2Days(alerts.get(1));
        }

        entity.setAlert3Days(null);

        if (alerts.size() > 2) {
            entity.setAlert3Days(alerts.get(2));
        }

        entity.setDeadlineDays(regime.getDeadlineDays());
        entity.setDescription(regime.getDescription());
        entity.setLeg1Code(regime.getLeg1Code());
        entity.setLeg1Name(regime.getLeg1Name());
        entity.setLeg2Code(regime.getLeg2Code());
        entity.setLeg2Name(regime.getLeg2Name());
        entity.setType(regime.getType());

        if (entity.getEbCptmRegimeTemporaireNum() == null) {
            // New
            entity.setEbCompagnie(connectedUserService.getCurrentUser().getEbCompagnie());
            entity.setDeleted(false);
        }

        return ebCptmRegimeTemporaireRepository.save(entity);
    }

    @Autowired
    DaoCptmRegimeTemporaire regimeTemporaireDao;

    @Override
    public List<EbCptmRegimeTemporaireDTO> listRegimeTemporaire(SearchCriteriaCPTM criteria) {
        return regimeTemporaireDao.list(criteria);
    }

    @Override
    public List<EbCptmRegimeTemporaireDTO> convertEntitiesToDTOs(List<EbCptmRegimeTemporaire> dbList) {
        List<EbCptmRegimeTemporaireDTO> dtos = new ArrayList<>();

        if (dbList != null) {
            dbList.forEach(dbi -> {
                dtos.add(new EbCptmRegimeTemporaireDTO(dbi));
            });
        }

        return dtos;
    }

    @Override
    public Long countListRegimeTemporaire(SearchCriteriaCPTM criteria) {
        return regimeTemporaireDao.listCount(criteria);
    }

    @Override
    public Boolean deleteRegime(Integer regimeNum) {
        Optional<EbCptmRegimeTemporaire> entityOpt = ebCptmRegimeTemporaireRepository.findById(regimeNum);

        if (entityOpt.isPresent()) {
            EbCptmRegimeTemporaire entity = entityOpt.get();
            entity.setDeleted(true);
            ebCptmRegimeTemporaireRepository.save(entity);
            return true;
        }

        return false;
    }
}
