package com.adias.mytowereasy.cptm.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties("mytower.cptm")
public class CptmProperties {
    private String cronEmailReminder;

    public String getCronEmailReminder() {
        return cronEmailReminder;
    }

    public void setCronEmailReminder(String cronEmailReminder) {
        this.cronEmailReminder = cronEmailReminder;
    }
}
