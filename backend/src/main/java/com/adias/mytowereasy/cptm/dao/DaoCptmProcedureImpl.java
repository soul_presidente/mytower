package com.adias.mytowereasy.cptm.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.cptm.dto.EbCptmProcedureDTO;
import com.adias.mytowereasy.cptm.dto.EbCptmRegimeSelectionDTO;
import com.adias.mytowereasy.cptm.dto.QEbCptmProcedureDTO;
import com.adias.mytowereasy.cptm.dto.QEbCptmRegimeSelectionDTO;
import com.adias.mytowereasy.cptm.model.CPTMEnumeration;
import com.adias.mytowereasy.cptm.model.QEbCptmProcedure;
import com.adias.mytowereasy.cptm.model.QEbCptmRegimeTemporaire;
import com.adias.mytowereasy.cptm.model.SearchCriteriaCPTM;
import com.adias.mytowereasy.dao.BaseDao;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.YesOrNo;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.QEbDemande;
import com.adias.mytowereasy.model.QEbMarchandise;
import com.adias.mytowereasy.model.custom.QEbCustomDeclaration;
import com.adias.mytowereasy.model.tt.QEbTtTracing;
import com.adias.mytowereasy.service.ConnectedUserService;


@Component
@Transactional
public class DaoCptmProcedureImpl extends BaseDao implements DaoCptmProcedure {
    @PersistenceContext
    EntityManager em;
    @Autowired
    ConnectedUserService connectedUserService;

    QEbCptmProcedure qProcedure = QEbCptmProcedure.ebCptmProcedure;
    QEbCptmRegimeTemporaire qRegimeTemporaire = new QEbCptmRegimeTemporaire("qRegimeTemporaire");
    QEbMarchandise qLeg1Unit = new QEbMarchandise("qLeg1Unit");
    QEbMarchandise qLeg2Unit = new QEbMarchandise("qLeg2Unit");
    QEbTtTracing qLeg1Tracing = new QEbTtTracing("qLeg1Tracing");
    QEbTtTracing qLeg2Tracing = new QEbTtTracing("qLeg2Tracing");
    QEbDemande qLeg1Demande = new QEbDemande("qLeg1Demande");
    QEbDemande qLeg2Demande = new QEbDemande("qLeg2Demande");
    QEbCustomDeclaration qLeg1Declaration = new QEbCustomDeclaration("qLeg1Declaration");
    QEbCustomDeclaration qLeg2Declaration = new QEbCustomDeclaration("qLeg2Declaration");
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbCompagnie");

    @Override
    public List<EbCptmProcedureDTO> listDTO(SearchCriteriaCPTM criteria) {
        List<EbCptmProcedureDTO> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbCptmProcedureDTO> query = new JPAQuery<EbCptmProcedureDTO>(em);

            query
                .select(
                    new QEbCptmProcedureDTO(
                        qProcedure.ebCptmProcedureNum,
                        qProcedure.refProcedure,
                        qRegimeTemporaire.leg1Name,
                        qRegimeTemporaire.leg2Name,
                        qRegimeTemporaire.leg1Code,
                        qRegimeTemporaire.leg2Code,
                        qRegimeTemporaire.alert1Days,
                        qRegimeTemporaire.alert2Days,
                        qRegimeTemporaire.alert3Days,
                        qLeg1Unit.unitReference,
                        qLeg1Demande.refTransport,
                        qLeg1Demande.xEcStatut,
                        qProcedure.leg1ExpectedCustomerDeclarationDate,
                        qLeg1Declaration.dateDeclaration,
                        qProcedure.leg1Location,
                        qProcedure.leg1TransitNumber,
                        qProcedure.leg1TransitStatus,
                        qProcedure.leg1UnitIndex,
                        qLeg1Declaration.declarationNumber,
                        qLeg1Tracing.libellePslCourant,
                        qLeg2Unit.unitReference,
                        qLeg2Demande.refTransport,
                        qLeg2Demande.xEcStatut,
                        qProcedure.leg2ExpectedCustomerDeclarationDate,
                        qLeg2Declaration.dateDeclaration,
                        qProcedure.leg2Location,
                        qProcedure.leg2TransitNumber,
                        qProcedure.leg2TransitStatus,
                        qProcedure.leg2UnitIndex,
                        qLeg2Declaration.declarationNumber,
                        qLeg2Tracing.libellePslCourant,
                        qProcedure.comment,
                        qProcedure.deadline,
                        qProcedure.status,
                        qProcedure.nbAlerts,
                        qProcedure.latestAlertDate,
                        qLeg1Demande.ebDemandeNum,
                        qLeg2Demande.ebDemandeNum,
                        qLeg1Unit.cptmOperationNumber

                    ));
            query.distinct().from(qProcedure);
            defineQueryGlobals(query, where, criteria, true);
            query.where(where);

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public Long listCount(SearchCriteriaCPTM criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbCptmProcedureDTO> query = new JPAQuery<EbCptmProcedureDTO>(em);
            BooleanBuilder where = new BooleanBuilder();

            query.distinct().from(qProcedure);
            defineQueryGlobals(query, where, criteria, false);
            query.where(where);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    private void defineQueryGlobals(
        JPAQuery query,
        BooleanBuilder where,
        SearchCriteriaCPTM criteria,
        boolean enablePagination) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        // Global Where
        if (connectedUser != null && connectedUser.getEbCompagnie() != null) {
            where.and(qProcedure.ebCompagnie().ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }

        if (StringUtils.isNotBlank(criteria.getSearchterm())) {
            String searchTerm = criteria.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qProcedure.refProcedure.lower().contains(searchTerm),
                    qLeg1Unit.unitReference.lower().contains(searchTerm),
                    qLeg2Unit.unitReference.lower().contains(searchTerm),
                    qProcedure.leg1Location.lower().contains(searchTerm),
                    qProcedure.leg2Location.lower().contains(searchTerm),
                    qProcedure.leg1TransitNumber.lower().contains(searchTerm),
                    qProcedure.leg2TransitNumber.lower().contains(searchTerm),
                    qLeg1Demande.refTransport.lower().contains(searchTerm),
                    qLeg2Demande.refTransport.lower().contains(searchTerm),
                    qRegimeTemporaire.leg1Name.lower().contains(searchTerm),
                    qRegimeTemporaire.leg2Name.lower().contains(searchTerm),
                    qRegimeTemporaire.leg1Code.lower().contains(searchTerm),
                    qRegimeTemporaire.leg2Code.lower().contains(searchTerm),
                    qLeg1Declaration.declarationReference.lower().contains(searchTerm),
                    qLeg1Declaration.declarationNumber.lower().contains(searchTerm),
                    qLeg2Declaration.declarationReference.lower().contains(searchTerm),
                    qLeg2Declaration.declarationNumber.lower().contains(searchTerm),
                    qLeg1Tracing.libellePslCourant.lower().contains(searchTerm),
                    qLeg2Tracing.libellePslCourant.lower().contains(searchTerm));
        }

        // Order
        if (criteria == null || StringUtils.isBlank(criteria.getOrderedColumn())) {
            query.orderBy(qProcedure.ebCptmProcedureNum.desc());
        }
        else if (criteria.getOrderedColumn().contains("leg1ActualCustomerDeclarationDate")) {
            query
                .orderBy(
                    new OrderSpecifier(
                        criteria.getAscendant() ? Order.ASC : Order.DESC,
                        qLeg1Declaration.dateDeclaration));
        }
        else if (criteria.getOrderedColumn().contains("leg1DeclarationNumber")) {
            query
                .orderBy(
                    new OrderSpecifier(
                        criteria.getAscendant() ? Order.ASC : Order.DESC,
                        qLeg1Declaration.declarationNumber));
        }
        else if (criteria.getOrderedColumn().contains("leg2ActualCustomerDeclarationDate")) {
            query
                .orderBy(
                    new OrderSpecifier(
                        criteria.getAscendant() ? Order.ASC : Order.DESC,
                        qLeg2Declaration.dateDeclaration));
        }
        else if (criteria.getOrderedColumn().contains("leg2DeclarationNumber")) {
            query
                .orderBy(
                    new OrderSpecifier(
                        criteria.getAscendant() ? Order.ASC : Order.DESC,
                        qLeg2Declaration.declarationNumber));
        }
        else if (criteria.getOrderedColumn() != null) {
            String finalOrderColumn = getOriginQueryDSLProjectionField(
                EbCptmProcedureDTO.class,
                criteria.getOrderedColumn());
            Path<Object> fieldPath = Expressions.path(Object.class, qProcedure, finalOrderColumn);
            query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
        }
        else {
            query.orderBy(qProcedure.ebCptmProcedureNum.asc());
        }

        // Pagination
        if (enablePagination) {

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

        }

        if (criteria != null) {

            if (criteria.getRefProcedure() != null) {
                where = where.and(qProcedure.refProcedure.contains(criteria.getRefProcedure()));
            }

            if (criteria.getLeg1Location() != null) {
                where = where.and(qProcedure.leg1Location.contains(criteria.getLeg1Location()));
            }

            if (criteria.getLeg2Location() != null) {
                where = where.and(qProcedure.leg2Location.contains(criteria.getLeg2Location()));
            }

            if (criteria.getLeg1TransitNumber() != null) {
                where = where.and(qProcedure.leg1TransitNumber.contains(criteria.getLeg1TransitNumber()));
            }

            if (criteria.getLeg2TransitNumber() != null) {
                where = where.and(qProcedure.leg2TransitNumber.contains(criteria.getLeg2TransitNumber()));
            }

            if (criteria.getStatutProcedure() != null) {
                where = where.and(qProcedure.status.eq(criteria.getStatutProcedure()));
            }

            if (criteria.getDeadlineProcedure() != null) {
                where = where
                    .and(
                        Expressions
                            .comparableTemplate(Date.class, "DATE({0})", qProcedure.deadline)
                            .eq(criteria.getDeadlineProcedure()));
            }

            if (criteria.getLeg1ExpectedCustomerDeclarationDate() != null) {
                where = where
                    .and(
                        qProcedure.leg1ExpectedCustomerDeclarationDate
                            .eq(criteria.getLeg1ExpectedCustomerDeclarationDate()));
            }

            if (criteria.getLeg2ExpectedCustomerDeclarationDate() != null) {
                where = where
                    .and(
                        qProcedure.leg2ExpectedCustomerDeclarationDate
                            .eq(criteria.getLeg2ExpectedCustomerDeclarationDate()));
            }

            if (criteria.getNbAlerts() != null) {
                where = where.and(qProcedure.nbAlerts.eq(criteria.getNbAlerts()));
            }

            if (criteria.getxLeg1UnitRef() != null) {
                where = where.and(qLeg1Unit.unitReference.eq(criteria.getxLeg1UnitRef()));
            }

            if (criteria.getxLeg2UnitRef() != null) {
                where = where.and(qLeg2Unit.unitReference.eq(criteria.getxLeg2UnitRef()));
            }

            if (criteria.getxRegimeTemporaireLeg1Name() != null) {
                where = where.and(qRegimeTemporaire.leg1Name.eq(criteria.getxRegimeTemporaireLeg1Name()));
            }

            if (criteria.getxRegimeTemporaireLeg2Name() != null) {
                where = where.and(qRegimeTemporaire.leg2Name.eq(criteria.getxRegimeTemporaireLeg2Name()));
            }

            if (criteria.getxLeg1DemandeRef() != null) {
                where = where.and(qLeg1Demande.refTransport.eq(criteria.getxLeg1DemandeRef()));
            }

            if (criteria.getxLeg2DemandeRef() != null) {
                where = where.and(qLeg2Demande.refTransport.eq(criteria.getxLeg2DemandeRef()));
            }

            if (criteria.getLeg1DeclarationNumber() != null) {
                where = where.and(qLeg1Declaration.declarationNumber.eq(criteria.getLeg1DeclarationNumber()));
            }

            if (criteria.getLeg2DeclarationNumber() != null) {
                where = where.and(qLeg2Declaration.declarationNumber.eq(criteria.getLeg2DeclarationNumber()));
            }

            if (criteria.getLeg1TracingStatus() != null) {
                where = where.and(qLeg1Tracing.libellePslCourant.eq(criteria.getLeg1TracingStatus()));
            }

            if (criteria.getLeg2TracingStatus() != null) {
                where = where.and(qLeg2Tracing.libellePslCourant.eq(criteria.getLeg2TracingStatus()));
            }

            if (criteria.getLeg1ActualCustomerDeclarationDate() != null) {
                where = where.and(qLeg1Declaration.dateDeclaration.eq(criteria.getLeg1ActualCustomerDeclarationDate()));
            }

            if (criteria.getLeg2ActualCustomerDeclarationDate() != null) {
                where = where.and(qLeg2Declaration.dateDeclaration.eq(criteria.getLeg2ActualCustomerDeclarationDate()));
            }

            if (criteria.getLeg1TransitStatus() != null) {

                if (criteria.getLeg1TransitStatus() == YesOrNo.No.getCode()) {
                    where.and(qProcedure.leg1TransitStatus.isFalse().or(qProcedure.leg1TransitStatus.isNull()));
                }
                else {
                    where.and(qProcedure.leg1TransitStatus.isTrue());
                }

            }

            if (criteria.getLeg2TransitStatus() != null) {

                if (criteria.getLeg2TransitStatus() == YesOrNo.No.getCode()) {
                    where.and(qProcedure.leg2TransitStatus.isFalse().or(qProcedure.leg2TransitStatus.isNull()));
                }
                else {
                    where.and(qProcedure.leg2TransitStatus.isTrue());
                }

            }

        }

        // Join
        query.leftJoin(qProcedure.xLeg1Unit(), qLeg1Unit);
        query.leftJoin(qProcedure.xLeg2Unit(), qLeg2Unit);
        query.leftJoin(qProcedure.xLeg1Tracing(), qLeg1Tracing);
        query.leftJoin(qProcedure.xLeg2Tracing(), qLeg2Tracing);
        query.leftJoin(qProcedure.xLeg1Demande(), qLeg1Demande);
        query.leftJoin(qProcedure.xLeg2Demande(), qLeg2Demande);
        query.leftJoin(qProcedure.xRegimeTemporaire(), qRegimeTemporaire);
        query.leftJoin(qLeg1Unit.xEbCustomDeclaration(), qLeg1Declaration);
        query.leftJoin(qLeg2Unit.xEbCustomDeclaration(), qLeg2Declaration);
    }

    @Override
    public Integer nextProcedureId() {
        Integer result = null;

        try {
            result = ((BigInteger) em
                .createNativeQuery("SELECT NEXTVAL('work.eb_cptm_procedure_eb_cptm_procedure_num_seq')")
                .getSingleResult()).intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<?> advancedSearchAutocomplete(String term, Integer field, Boolean searchField) {
        List<?> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<?> query = new JPAQuery<>(em);

            if (StringUtils.isBlank(term)) {
                return resultat;
            }

            term = term.trim().toLowerCase();

            if (CPTMEnumeration.SearchCptmFields.REF_PROCEDURE.getCode().equals(field)) {
                query.select(qProcedure.refProcedure);
                query.groupBy(qProcedure.refProcedure);
                where.and(qProcedure.refProcedure.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.LOCALISATION_LEG_1.getCode().equals(field)) {
                query.select(qProcedure.leg1Location);
                query.groupBy(qProcedure.leg1Location);
                where.and(qProcedure.leg1Location.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.LOCALISATION_LEG_2.getCode().equals(field)) {
                query.select(qProcedure.leg2Location);
                query.groupBy(qProcedure.leg2Location);
                where.and(qProcedure.leg2Location.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.NUM_TRANSIT_LEG_1.getCode().equals(field)) {
                query.select(qProcedure.leg1TransitNumber);
                query.groupBy(qProcedure.leg1TransitNumber);
                where.and(qProcedure.leg1TransitNumber.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.NUM_TRANSIT_LEG_2.getCode().equals(field)) {
                query.select(qProcedure.leg2TransitNumber);
                query.groupBy(qProcedure.leg2TransitNumber);
                where.and(qProcedure.leg2TransitNumber.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.NB_ALERTS.getCode().equals(field)) {
                query.select(qProcedure.nbAlerts);
                query.groupBy(qProcedure.nbAlerts);
                where.and(qProcedure.nbAlerts.stringValue().toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.REF_UNIT_LEG_1.getCode().equals(field)) {
                query.select(qLeg1Unit.unitReference);
                query.groupBy(qLeg1Unit.unitReference);
                where.and(qLeg1Unit.unitReference.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.REF_UNIT_LEG_2.getCode().equals(field)) {
                query.select(qLeg2Unit.unitReference);
                query.groupBy(qLeg2Unit.unitReference);
                where.and(qLeg2Unit.unitReference.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.TYPE_SEGMENT_LEG_1.getCode().equals(field)) {
                query.select(qRegimeTemporaire.leg1Name);
                query.groupBy(qRegimeTemporaire.leg1Name);
                where.and(qRegimeTemporaire.leg1Name.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.TYPE_SEGMENT_LEG_2.getCode().equals(field)) {
                query.select(qRegimeTemporaire.leg2Name);
                query.groupBy(qRegimeTemporaire.leg2Name);
                where.and(qRegimeTemporaire.leg2Name.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.REF_TRANSPORT_LEG_1.getCode().equals(field)) {
                query.select(qLeg1Demande.refTransport);
                query.groupBy(qLeg1Demande.refTransport);
                where.and(qLeg1Demande.refTransport.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.REF_TRANSPORT_LEG_2.getCode().equals(field)) {
                query.select(qLeg2Demande.refTransport);
                query.groupBy(qLeg2Demande.refTransport);
                where.and(qLeg2Demande.refTransport.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.NUM_DECLARATION_LEG_1.getCode().equals(field)) {
                query.select(qLeg1Declaration.declarationNumber);
                query.groupBy(qLeg1Declaration.declarationNumber);
                where.and(qLeg1Declaration.declarationNumber.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.NUM_DECLARATION_LEG_2.getCode().equals(field)) {
                query.select(qLeg2Declaration.declarationNumber);
                query.groupBy(qLeg2Declaration.declarationNumber);
                where.and(qLeg2Declaration.declarationNumber.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.TRACKING_LEG_1.getCode().equals(field)) {
                query.select(qLeg1Tracing.libellePslCourant);
                query.groupBy(qLeg1Tracing.libellePslCourant);
                where.and(qLeg1Tracing.libellePslCourant.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.TRACKING_LEG_2.getCode().equals(field)) {
                query.select(qLeg2Tracing.libellePslCourant);
                query.groupBy(qLeg2Tracing.libellePslCourant);
                where.and(qLeg2Tracing.libellePslCourant.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.CODE_SEGMENT_LEG_1.getCode().equals(field)) {
                query.select(qRegimeTemporaire.leg1Code);
                query.groupBy(qRegimeTemporaire.leg1Code);
                where.and(qRegimeTemporaire.leg1Code.toLowerCase().contains(term.trim().toLowerCase()));
            }
            else if (CPTMEnumeration.SearchCptmFields.CODE_SEGMENT_LEG_2.getCode().equals(field)) {
                query.select(qRegimeTemporaire.leg2Code);
                query.groupBy(qRegimeTemporaire.leg2Code);
                where.and(qRegimeTemporaire.leg2Code.toLowerCase().contains(term.trim().toLowerCase()));
            }

            query.distinct().from(qProcedure);
            query.leftJoin(qProcedure.xLeg1Unit(), qLeg1Unit);
            query.leftJoin(qProcedure.xLeg2Unit(), qLeg2Unit);
            query.leftJoin(qProcedure.xLeg1Tracing(), qLeg1Tracing);
            query.leftJoin(qProcedure.xLeg2Tracing(), qLeg2Tracing);
            query.leftJoin(qProcedure.xLeg1Demande(), qLeg1Demande);
            query.leftJoin(qProcedure.xLeg2Demande(), qLeg2Demande);
            query.leftJoin(qProcedure.xRegimeTemporaire(), qRegimeTemporaire);
            query.leftJoin(qLeg1Unit.xEbCustomDeclaration(), qLeg1Declaration);
            query.leftJoin(qLeg2Unit.xEbCustomDeclaration(), qLeg2Declaration);

            query.where(where);
            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public List<EbCptmRegimeSelectionDTO> listRegimeSelectionDTO(SearchCriteriaCPTM criteria) {
        List<EbCptmRegimeSelectionDTO> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbCptmRegimeSelectionDTO> query = new JPAQuery<EbCptmRegimeSelectionDTO>(em);

            query
                .select(
                    new QEbCptmRegimeSelectionDTO(
                        qProcedure.refProcedure,
                        qProcedure.ebCptmProcedureNum,
                        qRegimeTemporaire.ebCptmRegimeTemporaireNum,
                        qLeg1Unit.cptmOperationNumber,
                        qLeg1Unit.unitReference));
            query.distinct().from(qProcedure);

            query.leftJoin(qProcedure.xRegimeTemporaire(), qRegimeTemporaire);
            query.leftJoin(qProcedure.xLeg1Unit(), qLeg1Unit);
            query.leftJoin(qProcedure.ebCompagnie(), qEbCompagnie);

            if (BooleanUtils.isTrue(criteria.getWithoutLeg2())) {
                where.and(qProcedure.xLeg2Demande().isNull());
            }

            if (criteria.getEbCompagnieNum() != null) {
                where.and(qEbCompagnie.ebCompagnieNum.eq(criteria.getEbCompagnieNum()));
            }

            query.where(where);

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }
}
