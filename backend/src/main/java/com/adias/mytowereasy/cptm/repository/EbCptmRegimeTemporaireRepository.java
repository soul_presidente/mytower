package com.adias.mytowereasy.cptm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.cptm.model.EbCptmRegimeTemporaire;
import com.adias.mytowereasy.repository.CommonRepository;


@Repository
public interface EbCptmRegimeTemporaireRepository extends CommonRepository<EbCptmRegimeTemporaire, Integer> {
    @Query("from EbCptmRegimeTemporaire r "
        + "where r.deleted = false and r.ebCompagnie.ebCompagnieNum = :ebCompanyNum " + "order by r.type")
    public List<EbCptmRegimeTemporaire> findActivesForCompany(@Param("ebCompanyNum") Integer ebCompanyNum);
}
