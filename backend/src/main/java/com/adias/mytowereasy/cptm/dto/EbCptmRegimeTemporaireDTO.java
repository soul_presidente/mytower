package com.adias.mytowereasy.cptm.dto;

import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.cptm.model.EbCptmRegimeTemporaire;


public class EbCptmRegimeTemporaireDTO

{
    private Integer ebCptmRegimeTemporaireNum;
    private String type;
    private String description;
    private Integer deadlineDays;
    private Integer alert1Days;
    private Integer alert2Days;
    private Integer alert3Days;
    private String leg1Name;
    private String leg1Code;
    private String leg2Name;
    private String leg2Code;

    public EbCptmRegimeTemporaireDTO(EbCptmRegimeTemporaire db)

    {
        this.ebCptmRegimeTemporaireNum = db.getEbCptmRegimeTemporaireNum();
        this.type = db.getType();
        this.description = db.getDescription();
        this.deadlineDays = db.getDeadlineDays();
        this.alert1Days = db.getAlert1Days();
        this.alert2Days = db.getAlert2Days();
        this.alert3Days = db.getAlert3Days();
        this.leg1Name = db.getLeg1Name();
        this.leg1Code = db.getLeg1Code();
        this.leg2Name = db.getLeg2Name();
        this.leg2Code = db.getLeg2Code();
    }

    @QueryProjection
    public EbCptmRegimeTemporaireDTO(
        Integer ebCptmRegimeTemporaireNum,
        String type,
        String description,
        Integer deadlineDays,
        Integer alert1Days,
        Integer alert2Days,
        Integer alert3Days,
        String leg1Name,
        String leg1Code,
        String leg2Name,
        String leg2Code) {
        this.ebCptmRegimeTemporaireNum = ebCptmRegimeTemporaireNum;
        this.type = type;
        this.description = description;
        this.deadlineDays = deadlineDays;
        this.alert1Days = alert1Days;
        this.alert2Days = alert2Days;
        this.alert3Days = alert3Days;
        this.leg1Name = leg1Name;
        this.leg1Code = leg1Code;
        this.leg2Name = leg2Name;
        this.leg2Code = leg2Code;
    }

    public EbCptmRegimeTemporaireDTO() {
    }

    public Integer getEbCptmRegimeTemporaireNum() {
        return ebCptmRegimeTemporaireNum;
    }

    public void setEbCptmRegimeTemporaireNum(Integer ebCptmRegimeTemporaireNum) {
        this.ebCptmRegimeTemporaireNum = ebCptmRegimeTemporaireNum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getDeadlineDays() {
        return deadlineDays;
    }

    public void setDeadlineDays(Integer deadlineDays) {
        this.deadlineDays = deadlineDays;
    }

    public Integer getAlert1Days() {
        return alert1Days;
    }

    public void setAlert1Days(Integer alert1Days) {
        this.alert1Days = alert1Days;
    }

    public Integer getAlert2Days() {
        return alert2Days;
    }

    public void setAlert2Days(Integer alert2Days) {
        this.alert2Days = alert2Days;
    }

    public Integer getAlert3Days() {
        return alert3Days;
    }

    public void setAlert3Days(Integer alert3Days) {
        this.alert3Days = alert3Days;
    }

    public String getLeg1Name() {
        return leg1Name;
    }

    public void setLeg1Name(String leg1Name) {
        this.leg1Name = leg1Name;
    }

    public String getLeg1Code() {
        return leg1Code;
    }

    public void setLeg1Code(String leg1Code) {
        this.leg1Code = leg1Code;
    }

    public String getLeg2Name() {
        return leg2Name;
    }

    public void setLeg2Name(String leg2Name) {
        this.leg2Name = leg2Name;
    }

    public String getLeg2Code() {
        return leg2Code;
    }

    public void setLeg2Code(String leg2Code) {
        this.leg2Code = leg2Code;
    }
}
