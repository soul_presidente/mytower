package com.adias.mytowereasy.cptm.dao;

import java.util.List;

import com.adias.mytowereasy.cptm.dto.EbCptmRegimeTemporaireDTO;
import com.adias.mytowereasy.cptm.model.SearchCriteriaCPTM;


public interface DaoCptmRegimeTemporaire {
    public List<EbCptmRegimeTemporaireDTO> list(SearchCriteriaCPTM criteria);

    public Long listCount(SearchCriteriaCPTM criteria);
}
