package com.adias.mytowereasy.cptm.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbTimeStamps;
import com.adias.mytowereasy.model.tt.EbTtTracing;


@Entity
@Table(name = "eb_cptm_procedure", indexes = {
    @Index(name = "idx_eb_cptm_procedure_ref_procedure", columnList = "ref_procedure"),
    @Index(name = "idx_eb_cptm_procedure_x_eb_company", columnList = "x_eb_company"),
    @Index(name = "idx_eb_cptm_procedure_x_regime_temporaire", columnList = "x_regime_temporaire"),
    @Index(name = "idx_eb_cptm_procedure_status", columnList = "status"),
    @Index(name = "idx_eb_cptm_procedure_x_leg1_demande", columnList = "x_leg1_demande"),
    @Index(name = "idx_eb_cptm_procedure_x_leg1_tracing", columnList = "x_leg1_tracing"),
    @Index(name = "idx_eb_cptm_procedure_x_leg1_unit", columnList = "x_leg1_unit"),
    @Index(name = "idx_eb_cptm_procedure_leg1unit_index", columnList = "leg1unit_index"),
    @Index(name = "idx_eb_cptm_procedure_x_leg2_demande", columnList = "x_leg2_demande"),
    @Index(name = "idx_eb_cptm_procedure_x_leg2_tracing", columnList = "x_leg2_tracing"),
    @Index(name = "idx_eb_cptm_procedure_x_leg2_unit", columnList = "x_leg2_unit"),
    @Index(name = "idx_eb_cptm_procedure_leg2unit_index", columnList = "leg2unit_index"),
})
public class EbCptmProcedure extends EbTimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebCptmProcedureNum;

    @Column(name = "ref_procedure")
    private String refProcedure;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company", insertable = true)
    private EbCompagnie ebCompagnie;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_regime_temporaire", insertable = true)
    private EbCptmRegimeTemporaire xRegimeTemporaire;

    // Procedure fields
    @Column(name = "comment")
    private String comment;

    @Column(name = "deadline")
    private Date deadline;

    @Column(name = "status")
    private Integer status;

    @Column(name = "nb_alerts")
    private Integer nbAlerts;

    @Column(name = "latest_alert_date")
    private Date latestAlertDate;

    // Leg1
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_leg1_demande", insertable = true)
    private EbDemande xLeg1Demande;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_leg1_tracing", insertable = true)
    private EbTtTracing xLeg1Tracing;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_leg1_unit", insertable = true)
    private EbMarchandise xLeg1Unit;

    @Column(name = "leg1expected_customer_declaration_date")
    private Date leg1ExpectedCustomerDeclarationDate;

    @Column(name = "leg1location")
    private String leg1Location;

    @Column(name = "leg1transit_number")
    private String leg1TransitNumber;

    @Column(name = "leg1transit_status")
    private Boolean leg1TransitStatus;

    @Column(name = "leg1unit_index")
    private Integer leg1UnitIndex;

    // Leg2
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_leg2_demande", insertable = true)
    private EbDemande xLeg2Demande;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_leg2_tracing", insertable = true)
    private EbTtTracing xLeg2Tracing;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_leg2_unit", insertable = true)
    private EbMarchandise xLeg2Unit;

    @Column(name = "leg2expected_customer_declaration_date")
    private Date leg2ExpectedCustomerDeclarationDate;

    @Column(name = "leg2location")
    private String leg2Location;

    @Column(name = "leg2transit_number")
    private String leg2TransitNumber;

    @Column(name = "leg2transit_status")
    private Boolean leg2TransitStatus;

    @Column(name = "leg2unit_index")
    private Integer leg2UnitIndex;

    public EbCptmProcedure() {
    }

    public Integer getEbCptmProcedureNum() {
        return ebCptmProcedureNum;
    }

    public void setEbCptmProcedureNum(Integer ebCptmProcedureNum) {
        this.ebCptmProcedureNum = ebCptmProcedureNum;
    }

    public String getRefProcedure() {
        return refProcedure;
    }

    public void setRefProcedure(String refProcedure) {
        this.refProcedure = refProcedure;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    public EbCptmRegimeTemporaire getxRegimeTemporaire() {
        return xRegimeTemporaire;
    }

    public void setxRegimeTemporaire(EbCptmRegimeTemporaire xRegimeTemporaire) {
        this.xRegimeTemporaire = xRegimeTemporaire;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getNbAlerts() {
        return nbAlerts;
    }

    public void setNbAlerts(Integer nbAlerts) {
        this.nbAlerts = nbAlerts;
    }

    public EbDemande getxLeg1Demande() {
        return xLeg1Demande;
    }

    public void setxLeg1Demande(EbDemande xLeg1Demande) {
        this.xLeg1Demande = xLeg1Demande;
    }

    public EbTtTracing getxLeg1Tracing() {
        return xLeg1Tracing;
    }

    public void setxLeg1Tracing(EbTtTracing xLeg1Tracing) {
        this.xLeg1Tracing = xLeg1Tracing;
    }

    public EbMarchandise getxLeg1Unit() {
        return xLeg1Unit;
    }

    public void setxLeg1Unit(EbMarchandise xLeg1Unit) {
        this.xLeg1Unit = xLeg1Unit;
    }

    public Date getLeg1ExpectedCustomerDeclarationDate() {
        return leg1ExpectedCustomerDeclarationDate;
    }

    public void setLeg1ExpectedCustomerDeclarationDate(Date leg1ExpectedCustomerDeclarationDate) {
        this.leg1ExpectedCustomerDeclarationDate = leg1ExpectedCustomerDeclarationDate;
    }

    public String getLeg1Location() {
        return leg1Location;
    }

    public void setLeg1Location(String leg1Location) {
        this.leg1Location = leg1Location;
    }

    public String getLeg1TransitNumber() {
        return leg1TransitNumber;
    }

    public void setLeg1TransitNumber(String leg1TransitNumber) {
        this.leg1TransitNumber = leg1TransitNumber;
    }

    public Boolean getLeg1TransitStatus() {
        return leg1TransitStatus;
    }

    public void setLeg1TransitStatus(Boolean leg1TransitStatus) {
        this.leg1TransitStatus = leg1TransitStatus;
    }

    public EbDemande getxLeg2Demande() {
        return xLeg2Demande;
    }

    public void setxLeg2Demande(EbDemande xLeg2Demande) {
        this.xLeg2Demande = xLeg2Demande;
    }

    public EbTtTracing getxLeg2Tracing() {
        return xLeg2Tracing;
    }

    public void setxLeg2Tracing(EbTtTracing xLeg2Tracing) {
        this.xLeg2Tracing = xLeg2Tracing;
    }

    public EbMarchandise getxLeg2Unit() {
        return xLeg2Unit;
    }

    public void setxLeg2Unit(EbMarchandise xLeg2Unit) {
        this.xLeg2Unit = xLeg2Unit;
    }

    public Date getLeg2ExpectedCustomerDeclarationDate() {
        return leg2ExpectedCustomerDeclarationDate;
    }

    public void setLeg2ExpectedCustomerDeclarationDate(Date leg2ExpectedCustomerDeclarationDate) {
        this.leg2ExpectedCustomerDeclarationDate = leg2ExpectedCustomerDeclarationDate;
    }

    public String getLeg2Location() {
        return leg2Location;
    }

    public void setLeg2Location(String leg2Location) {
        this.leg2Location = leg2Location;
    }

    public String getLeg2TransitNumber() {
        return leg2TransitNumber;
    }

    public void setLeg2TransitNumber(String leg2TransitNumber) {
        this.leg2TransitNumber = leg2TransitNumber;
    }

    public Boolean getLeg2TransitStatus() {
        return leg2TransitStatus;
    }

    public void setLeg2TransitState(Boolean leg2TransitStatus) {
        this.leg2TransitStatus = leg2TransitStatus;
    }

    public Integer getLeg1UnitIndex() {
        return leg1UnitIndex;
    }

    public void setLeg1UnitIndex(Integer leg1UnitIndex) {
        this.leg1UnitIndex = leg1UnitIndex;
    }

    public Integer getLeg2UnitIndex() {
        return leg2UnitIndex;
    }

    public void setLeg2UnitIndex(Integer leg2UnitIndex) {
        this.leg2UnitIndex = leg2UnitIndex;
    }

    public void setLeg2TransitStatus(Boolean leg2TransitStatus) {
        this.leg2TransitStatus = leg2TransitStatus;
    }

    public Date getLatestAlertDate() {
        return latestAlertDate;
    }

    public void setLatestAlertDate(Date latestAlertDate) {
        this.latestAlertDate = latestAlertDate;
    }
}
