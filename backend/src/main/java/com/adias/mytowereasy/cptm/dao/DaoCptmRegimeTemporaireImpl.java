package com.adias.mytowereasy.cptm.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.cptm.dto.EbCptmRegimeTemporaireDTO;
import com.adias.mytowereasy.cptm.dto.QEbCptmRegimeTemporaireDTO;
import com.adias.mytowereasy.cptm.model.EbCptmRegimeTemporaire;
import com.adias.mytowereasy.cptm.model.QEbCptmRegimeTemporaire;
import com.adias.mytowereasy.cptm.model.SearchCriteriaCPTM;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.ConnectedUserService;


@Component
@Transactional
public class DaoCptmRegimeTemporaireImpl implements DaoCptmRegimeTemporaire {
    @PersistenceContext
    EntityManager em;
    @Autowired
    ConnectedUserService connectedUserService;

    QEbCptmRegimeTemporaire qRegimeTemporaire = QEbCptmRegimeTemporaire.ebCptmRegimeTemporaire;

    @Override
    public List<EbCptmRegimeTemporaireDTO> list(SearchCriteriaCPTM criteria) {
        List<EbCptmRegimeTemporaireDTO> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbCptmRegimeTemporaireDTO> query = new JPAQuery<>(em);

            query
                .select(
                    new QEbCptmRegimeTemporaireDTO(
                        qRegimeTemporaire.ebCptmRegimeTemporaireNum,
                        qRegimeTemporaire.type,
                        qRegimeTemporaire.description,
                        qRegimeTemporaire.deadlineDays,
                        qRegimeTemporaire.alert1Days,
                        qRegimeTemporaire.alert2Days,
                        qRegimeTemporaire.alert3Days,
                        qRegimeTemporaire.leg1Name,
                        qRegimeTemporaire.leg1Code,
                        qRegimeTemporaire.leg2Name,
                        qRegimeTemporaire.leg2Code));
            defineQueryGlobals(query, where, criteria, true);
            query.distinct().from(qRegimeTemporaire);
            query.where(where);

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public Long listCount(SearchCriteriaCPTM criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbCptmRegimeTemporaire> query = new JPAQuery<EbCptmRegimeTemporaire>(em);
            BooleanBuilder where = new BooleanBuilder();

            defineQueryGlobals(query, where, criteria, false);
            query.from(qRegimeTemporaire);
            query.distinct().where(where);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    private void defineQueryGlobals(
        JPAQuery query,
        BooleanBuilder where,
        SearchCriteriaCPTM criteria,
        boolean enablePagination) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        // Global Where
        where.and(qRegimeTemporaire.deleted.isFalse());
        where
            .and(qRegimeTemporaire.ebCompagnie().ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));

        if (StringUtils.isNotBlank(criteria.getSearchterm())) {
            String searchTerm = criteria.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qRegimeTemporaire.type.lower().contains(searchTerm),
                    qRegimeTemporaire.description.lower().contains(searchTerm),
                    qRegimeTemporaire.leg1Name.lower().contains(searchTerm),
                    qRegimeTemporaire.leg1Code.lower().contains(searchTerm),
                    qRegimeTemporaire.leg2Name.lower().contains(searchTerm),
                    qRegimeTemporaire.leg2Code.lower().contains(searchTerm));
        }

        // Order
        if (criteria.getOrderedColumn() != null) {
            Path<Object> fieldPath = Expressions.path(Object.class, qRegimeTemporaire, criteria.getOrderedColumn());
            query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
        }
        else {
            query.orderBy(qRegimeTemporaire.ebCptmRegimeTemporaireNum.asc());
        }

        // Pagination
        if (enablePagination) {

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

        }

    }
}
