package com.adias.mytowereasy.cptm.model;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public class SearchCriteriaCPTM extends SearchCriteria {
    private Boolean leg2NotFilled;
    private String refProcedure;
    private String leg1Location;
    private String leg1TransitNumber;
    private String leg2Location;
    private String leg2TransitNumber;
    private Integer nbAlerts;
    private Integer statutProcedure;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date deadlineProcedure;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date leg1ExpectedCustomerDeclarationDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date leg2ExpectedCustomerDeclarationDate;
    private String xLeg1UnitRef;
    private String xLeg2UnitRef;
    private String xRegimeTemporaireLeg1Name;
    private String xRegimeTemporaireLeg2Name;
    private String xLeg1DemandeRef;
    private String xLeg2DemandeRef;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date leg1ActualCustomerDeclarationDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date leg2ActualCustomerDeclarationDate;
    private String leg1DeclarationNumber;
    private String leg2DeclarationNumber;
    private Integer leg1TransitStatus;
    private Integer leg2TransitStatus;
    private String leg1TracingStatus;
    private String leg2TracingStatus;
    private Boolean searchField;
    private Boolean withoutLeg2;

    public void setSearchInput(String searchInput, List<CustomFields> customsFields) throws JsonParseException, JsonMappingException, IOException {
        if (searchInput == null) searchInput = "{}";

        super.setSearchInput(searchInput, customsFields);

        this.refProcedure = ((SearchCriteriaCPTM) deserialize(this.searchInput, SearchCriteriaCPTM.class)).refProcedure;
        this.leg1Location = ((SearchCriteriaCPTM) deserialize(this.searchInput, SearchCriteriaCPTM.class)).leg1Location;
        this.leg1TransitNumber = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).leg1TransitNumber;
        this.leg2Location = ((SearchCriteriaCPTM) deserialize(this.searchInput, SearchCriteriaCPTM.class)).leg2Location;
        this.leg2TransitNumber = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).leg2TransitNumber;
        this.statutProcedure = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).statutProcedure;
        this.deadlineProcedure = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).deadlineProcedure;
        this.leg1ExpectedCustomerDeclarationDate = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).leg1ExpectedCustomerDeclarationDate;
        this.leg2ExpectedCustomerDeclarationDate = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).leg2ExpectedCustomerDeclarationDate;
        this.nbAlerts = ((SearchCriteriaCPTM) deserialize(this.searchInput, SearchCriteriaCPTM.class)).nbAlerts;
        this.xLeg1UnitRef = ((SearchCriteriaCPTM) deserialize(this.searchInput, SearchCriteriaCPTM.class)).xLeg1UnitRef;
        this.xLeg2UnitRef = ((SearchCriteriaCPTM) deserialize(this.searchInput, SearchCriteriaCPTM.class)).xLeg2UnitRef;
        this.xRegimeTemporaireLeg1Name = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).xRegimeTemporaireLeg1Name;
        this.xRegimeTemporaireLeg2Name = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).xRegimeTemporaireLeg2Name;
        this.xLeg1DemandeRef = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).xLeg1DemandeRef;
        this.xLeg2DemandeRef = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).xLeg2DemandeRef;
        this.leg1ActualCustomerDeclarationDate = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).leg1ActualCustomerDeclarationDate;
        this.leg2ActualCustomerDeclarationDate = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).leg2ActualCustomerDeclarationDate;
        this.leg1DeclarationNumber = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).leg1DeclarationNumber;
        this.leg2DeclarationNumber = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).leg2DeclarationNumber;
        this.leg1TransitStatus = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).leg1TransitStatus;
        this.leg2TransitStatus = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).leg2TransitStatus;
        this.leg1TracingStatus = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).leg1TracingStatus;
        this.leg2TracingStatus = ((SearchCriteriaCPTM) deserialize(
            this.searchInput,
            SearchCriteriaCPTM.class)).leg2TracingStatus;
    }

    public String getRefProcedure() {
        return refProcedure;
    }

    public void setRefProcedure(String refProcedure) {
        this.refProcedure = refProcedure;
    }

    public String getLeg1Location() {
        return leg1Location;
    }

    public void setLeg1Location(String leg1Location) {
        this.leg1Location = leg1Location;
    }

    public String getLeg1TransitNumber() {
        return leg1TransitNumber;
    }

    public void setLeg1TransitNumber(String leg1TransitNumber) {
        this.leg1TransitNumber = leg1TransitNumber;
    }

    public String getLeg2Location() {
        return leg2Location;
    }

    public void setLeg2Location(String leg2Location) {
        this.leg2Location = leg2Location;
    }

    public String getLeg2TransitNumber() {
        return leg2TransitNumber;
    }

    public void setLeg2TransitNumber(String leg2TransitNumber) {
        this.leg2TransitNumber = leg2TransitNumber;
    }

    public Integer getStatutProcedure() {
        return statutProcedure;
    }

    public void setStatutProcedure(Integer statutProcedure) {
        this.statutProcedure = statutProcedure;
    }

    public Date getDeadlineProcedure() {
        return deadlineProcedure;
    }

    public void setDeadlineProcedure(Date deadlineProcedure) {
        this.deadlineProcedure = deadlineProcedure;
    }

    public Date getLeg1ExpectedCustomerDeclarationDate() {
        return leg1ExpectedCustomerDeclarationDate;
    }

    public void setLeg1ExpectedCustomerDeclarationDate(Date leg1ExpectedCustomerDeclarationDate) {
        this.leg1ExpectedCustomerDeclarationDate = leg1ExpectedCustomerDeclarationDate;
    }

    public Date getLeg2ExpectedCustomerDeclarationDate() {
        return leg2ExpectedCustomerDeclarationDate;
    }

    public void setLeg2ExpectedCustomerDeclarationDate(Date leg2ExpectedCustomerDeclarationDate) {
        this.leg2ExpectedCustomerDeclarationDate = leg2ExpectedCustomerDeclarationDate;
    }

    public Integer getNbAlerts() {
        return nbAlerts;
    }

    public void setNbAlerts(Integer nbAlerts) {
        this.nbAlerts = nbAlerts;
    }

    public String getxLeg1UnitRef() {
        return xLeg1UnitRef;
    }

    public void setxLeg1UnitRef(String xLeg1UnitRef) {
        this.xLeg1UnitRef = xLeg1UnitRef;
    }

    public String getxLeg2UnitRef() {
        return xLeg2UnitRef;
    }

    public void setxLeg2UnitRef(String xLeg2UnitRef) {
        this.xLeg2UnitRef = xLeg2UnitRef;
    }

    public String getxRegimeTemporaireLeg1Name() {
        return xRegimeTemporaireLeg1Name;
    }

    public void setxRegimeTemporaireLeg1Name(String xRegimeTemporaireLeg1Name) {
        this.xRegimeTemporaireLeg1Name = xRegimeTemporaireLeg1Name;
    }

    public String getxRegimeTemporaireLeg2Name() {
        return xRegimeTemporaireLeg2Name;
    }

    public void setxRegimeTemporaireLeg2Name(String xRegimeTemporaireLeg2Name) {
        this.xRegimeTemporaireLeg2Name = xRegimeTemporaireLeg2Name;
    }

    public String getxLeg1DemandeRef() {
        return xLeg1DemandeRef;
    }

    public void setxLeg1DemandeRef(String xLeg1DemandeRef) {
        this.xLeg1DemandeRef = xLeg1DemandeRef;
    }

    public String getxLeg2DemandeRef() {
        return xLeg2DemandeRef;
    }

    public void setxLeg2DemandeRef(String xLeg2DemandeRef) {
        this.xLeg2DemandeRef = xLeg2DemandeRef;
    }

    public Date getLeg1ActualCustomerDeclarationDate() {
        return leg1ActualCustomerDeclarationDate;
    }

    public void setLeg1ActualCustomerDeclarationDate(Date leg1ActualCustomerDeclarationDate) {
        this.leg1ActualCustomerDeclarationDate = leg1ActualCustomerDeclarationDate;
    }

    public Date getLeg2ActualCustomerDeclarationDate() {
        return leg2ActualCustomerDeclarationDate;
    }

    public void setLeg2ActualCustomerDeclarationDate(Date leg2ActualCustomerDeclarationDate) {
        this.leg2ActualCustomerDeclarationDate = leg2ActualCustomerDeclarationDate;
    }

    public String getLeg1DeclarationNumber() {
        return leg1DeclarationNumber;
    }

    public void setLeg1DeclarationNumber(String leg1DeclarationNumber) {
        this.leg1DeclarationNumber = leg1DeclarationNumber;
    }

    public String getLeg2DeclarationNumber() {
        return leg2DeclarationNumber;
    }

    public void setLeg2DeclarationNumber(String leg2DeclarationNumber) {
        this.leg2DeclarationNumber = leg2DeclarationNumber;
    }

    public Integer getLeg1TransitStatus() {
        return leg1TransitStatus;
    }

    public void setLeg1TransitStatus(Integer leg1TransitStatus) {
        this.leg1TransitStatus = leg1TransitStatus;
    }

    public Integer getLeg2TransitStatus() {
        return leg2TransitStatus;
    }

    public void setLeg2TransitStatus(Integer leg2TransitStatus) {
        this.leg2TransitStatus = leg2TransitStatus;
    }

    public String getLeg1TracingStatus() {
        return leg1TracingStatus;
    }

    public void setLeg1TracingStatus(String leg1TracingStatus) {
        this.leg1TracingStatus = leg1TracingStatus;
    }

    public String getLeg2TracingStatus() {
        return leg2TracingStatus;
    }

    public void setLeg2TracingStatus(String leg2TracingStatus) {
        this.leg2TracingStatus = leg2TracingStatus;
    }

    public Boolean getSearchField() {
        return searchField;
    }

    public void setSearchField(Boolean searchField) {
        this.searchField = searchField;
    }

    public Boolean getLeg2NotFilled() {
        return leg2NotFilled;
    }

    public void setLeg2NotFilled(Boolean leg2NotFilled) {
        this.leg2NotFilled = leg2NotFilled;
    }

    public Boolean getWithoutLeg2() {
        return withoutLeg2;
    }

    public void setWithoutLeg2(Boolean withoutLeg2) {
        this.withoutLeg2 = withoutLeg2;
    }
}
