package com.adias.mytowereasy.cptm.dao;

import java.util.List;

import com.adias.mytowereasy.cptm.dto.EbCptmProcedureDTO;
import com.adias.mytowereasy.cptm.dto.EbCptmRegimeSelectionDTO;
import com.adias.mytowereasy.cptm.model.SearchCriteriaCPTM;


public interface DaoCptmProcedure {
    public List<EbCptmProcedureDTO> listDTO(SearchCriteriaCPTM criteria);

    public Long listCount(SearchCriteriaCPTM criteria);

    public Integer nextProcedureId();

    public List<?> advancedSearchAutocomplete(String term, Integer type, Boolean searchField);

    List<EbCptmRegimeSelectionDTO> listRegimeSelectionDTO(SearchCriteriaCPTM criteria);
}
