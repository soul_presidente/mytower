package com.adias.mytowereasy.cptm.dto;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.cptm.model.EbCptmProcedure;
import com.adias.mytowereasy.dto.EntityProjectionField;


public class EbCptmProcedureDTO {
    @EntityProjectionField("ebCptmProcedureNum")
    private Integer ebCptmProcedureNum;

    @EntityProjectionField("refProcedure")
    private String refProcedure;

    @EntityProjectionField("xRegimeTemporaire.leg1Name")
    private String xRegimeTemporaireLeg1Name;

    @EntityProjectionField("xRegimeTemporaire.leg1Code")
    private String xRegimeTemporaireLeg1Code;

    @EntityProjectionField("xRegimeTemporaire.leg2Name")
    private String xRegimeTemporaireLeg2Name;

    @EntityProjectionField("xRegimeTemporaire.leg2Code")
    private String xRegimeTemporaireLeg2Code;

    @EntityProjectionField("xRegimeTemporaire.alert1Days")
    private Integer xRegimeTemporaireAlert1Days;

    @EntityProjectionField("xRegimeTemporaire.alert2Days")
    private Integer xRegimeTemporaireAlert2Days;

    @EntityProjectionField("xRegimeTemporaire.alert3Days")
    private Integer xRegimeTemporaireAlert3Days;

    // Leg1
    @EntityProjectionField("xLeg1Unit.unitReference")
    private String xLeg1UnitRef;

    @EntityProjectionField("xLeg1Demande.refTransport")
    private String xLeg1DemandeRef;

    @EntityProjectionField("xLeg1Demande.xEcStatut")
    private Integer xLeg1DemandeStatus;

    @EntityProjectionField("leg1ExpectedCustomerDeclarationDate")
    private Date leg1ExpectedCustomerDeclarationDate;

    @EntityProjectionField("xLeg1Unit.xEbCustomDeclaration.dateDeclaration")
    private Date leg1ActualCustomerDeclarationDate;

    @EntityProjectionField("leg1Location")
    private String leg1Location;

    @EntityProjectionField("leg1TransitNumber")
    private String leg1TransitNumber;

    @EntityProjectionField("leg1TransitStatus")
    private Boolean leg1TransitStatus;

    @EntityProjectionField("leg1UnitIndex")
    private Integer leg1UnitIndex;

    @EntityProjectionField("xLeg1Unit.xEbCustomDeclaration.declarationNumber")
    private String leg1DeclarationNumber;

    @EntityProjectionField("xLeg1Tracing.libellePslCourant")
    private String leg1TracingStatus;

    @EntityProjectionField("xLeg1Demande.ebDemandeNum")
    private Integer xLeg1DemandeNum;

    // Leg2
    @EntityProjectionField("xLeg2Unit.unitReference")
    private String xLeg2UnitRef;

    @EntityProjectionField("xLeg2Demande.refTransport")
    private String xLeg2DemandeRef;

    @EntityProjectionField("xLeg2Demande.xEcStatut")
    private Integer xLeg2DemandeStatus;

    @EntityProjectionField("leg2ExpectedCustomerDeclarationDate")
    private Date leg2ExpectedCustomerDeclarationDate;

    @EntityProjectionField("xLeg2Unit.xEbCustomDeclaration.dateDeclaration")
    private Date leg2ActualCustomerDeclarationDate;

    @EntityProjectionField("leg2Location")
    private String leg2Location;

    @EntityProjectionField("leg2TransitNumber")
    private String leg2TransitNumber;

    @EntityProjectionField("leg2TransitStatus")
    private Boolean leg2TransitStatus;

    @EntityProjectionField("leg2UnitIndex")
    private Integer leg2UnitIndex;

    @EntityProjectionField("xLeg2Unit.xEbCustomDeclaration.declarationNumber")
    private String leg2DeclarationNumber;

    @EntityProjectionField("xLeg2Tracing.libellePslCourant")
    private String leg2TracingStatus;

    @EntityProjectionField("xLeg2Demande.ebDemandeNum")
    private Integer xLeg2DemandeNum;

    // Others procedure fields
    @EntityProjectionField("comment")
    private String comment;

    @EntityProjectionField("deadline")
    private Date deadline;

    @EntityProjectionField("status")
    private Integer status;

    @EntityProjectionField("nbAlerts")
    private Integer nbAlerts;

    @EntityProjectionField("latestAlertDate")
    private Date latestAlertDate;

    @EntityProjectionField("xLeg1Unit.cptmOperationNumber")
    private String operationNumber;

    // Calculated fields
    private Date scheduledAlert;
    private Long daysSinceImport;
    private Long daysBeforeDeadline;
    private Long daysBeforeNextAlert;
    private Integer alertNumber;

    public EbCptmProcedureDTO() {
    }

    public EbCptmProcedureDTO(EbCptmProcedure entity) {
        this(
            entity.getEbCptmProcedureNum(),
            entity.getRefProcedure(),
            entity.getxRegimeTemporaire().getLeg1Name(),
            entity.getxRegimeTemporaire().getLeg2Name(),
            entity.getxRegimeTemporaire().getLeg1Code(),
            entity.getxRegimeTemporaire().getLeg2Code(),
            entity.getxRegimeTemporaire().getAlert1Days(),
            entity.getxRegimeTemporaire().getAlert2Days(),
            entity.getxRegimeTemporaire().getAlert3Days(),
            entity.getxLeg1Unit() != null ? entity.getxLeg1Unit().getUnitReference() : null,
            entity.getxLeg1Demande() != null ? entity.getxLeg1Demande().getRefTransport() : null,
            entity.getxLeg1Demande() != null ? entity.getxLeg1Demande().getxEcStatut() : null,
            entity.getLeg1ExpectedCustomerDeclarationDate(),
            entity.getxLeg1Unit() != null && entity.getxLeg1Unit().getxEbCustomDeclaration() != null ?
                entity.getxLeg1Unit().getxEbCustomDeclaration().getDateDeclaration() :
                null,
            entity.getLeg1Location(),
            entity.getLeg1TransitNumber(),
            entity.getLeg1TransitStatus(),
            entity.getLeg1UnitIndex(),
            entity.getxLeg1Unit() != null && entity.getxLeg1Unit().getxEbCustomDeclaration() != null ?
                entity.getxLeg1Unit().getxEbCustomDeclaration().getDeclarationNumber() :
                "",
            entity.getxLeg1Tracing() != null ? entity.getxLeg1Tracing().getLibellePslCourant() : null,
            entity.getxLeg2Unit() != null ? entity.getxLeg2Unit().getUnitReference() : null,
            entity.getxLeg2Demande() != null ? entity.getxLeg2Demande().getRefTransport() : null,
            entity.getxLeg2Demande() != null ? entity.getxLeg2Demande().getxEcStatut() : null,
            entity.getLeg2ExpectedCustomerDeclarationDate(),
            entity.getxLeg2Unit() != null && entity.getxLeg2Unit().getxEbCustomDeclaration() != null ?
                entity.getxLeg2Unit().getxEbCustomDeclaration().getDateDeclaration() :
                null,
            entity.getLeg2Location(),
            entity.getLeg2TransitNumber(),
            entity.getLeg2TransitStatus(),
            entity.getLeg2UnitIndex(),
            entity.getxLeg2Unit() != null && entity.getxLeg2Unit().getxEbCustomDeclaration() != null ?
                entity.getxLeg2Unit().getxEbCustomDeclaration().getDeclarationNumber() :
                "",
            entity.getxLeg2Tracing() != null ? entity.getxLeg2Tracing().getLibellePslCourant() : null,
            entity.getComment(),
            entity.getDeadline(),
            entity.getStatus(),
            entity.getNbAlerts(),
            entity.getLatestAlertDate(),
            entity.getxLeg1Demande() != null ? entity.getxLeg1Demande().getEbDemandeNum() : null,
            entity.getxLeg2Demande() != null ? entity.getxLeg2Demande().getEbDemandeNum() : null,
            entity.getxLeg1Unit() != null ? entity.getxLeg1Unit().getCptmOperationNumber() : null);
    }

    @QueryProjection
    public EbCptmProcedureDTO(
        Integer ebCptmProcedureNum,
        String refProcedure,
        String xRegimeTemporaireLeg1Name,
        String xRegimeTemporaireLeg2Name,
        String xRegimeTemporaireLeg1Code,
        String xRegimeTemporaireLeg2Code,
        Integer xRegimeTemporaireAlert1Days,
        Integer xRegimeTemporaireAlert2Days,
        Integer xRegimeTemporaireAlert3Days,
        String xLeg1UnitRef,
        String xLeg1DemandeRef,
        Integer xLeg1DemandeStatus,
        Date leg1ExpectedCustomerDeclarationDate,
        Date leg1ActualCustomerDeclarationDate,
        String leg1Location,
        String leg1TransitNumber,
        Boolean leg1TransitStatus,
        Integer leg1UnitIndex,
        String leg1DeclarationNumber,
        String leg1TracingStatus,
        String xLeg2UnitRef,
        String xLeg2DemandeRef,
        Integer xLeg2DemandeStatus,
        Date leg2ExpectedCustomerDeclarationDate,
        Date leg2ActualCustomerDeclarationDate,
        String leg2Location,
        String leg2TransitNumber,
        Boolean leg2TransitStatus,
        Integer leg2UnitIndex,
        String leg2DeclarationNumber,
        String leg2TracingStatus,
        String comment,
        Date deadline,
        Integer status,
        Integer nbAlerts,
        Date latestAlertDate,
        Integer xLeg1DemandeNum,
        Integer xLeg2DemandeNum,
        String operationNumber) {
        this.ebCptmProcedureNum = ebCptmProcedureNum;
        this.refProcedure = refProcedure;
        this.xRegimeTemporaireLeg1Name = xRegimeTemporaireLeg1Name;
        this.xRegimeTemporaireLeg2Name = xRegimeTemporaireLeg2Name;
        this.xRegimeTemporaireLeg1Code = xRegimeTemporaireLeg1Code;
        this.xRegimeTemporaireLeg2Code = xRegimeTemporaireLeg2Code;
        this.xRegimeTemporaireAlert1Days = xRegimeTemporaireAlert1Days;
        this.xRegimeTemporaireAlert2Days = xRegimeTemporaireAlert2Days;
        this.xRegimeTemporaireAlert3Days = xRegimeTemporaireAlert3Days;
        this.xLeg1UnitRef = xLeg1UnitRef;
        this.xLeg1DemandeRef = xLeg1DemandeRef;
        this.xLeg1DemandeStatus = xLeg1DemandeStatus;
        this.leg1ExpectedCustomerDeclarationDate = leg1ExpectedCustomerDeclarationDate;
        this.leg1ActualCustomerDeclarationDate = leg1ActualCustomerDeclarationDate;
        this.leg1Location = leg1Location;
        this.leg1TransitNumber = leg1TransitNumber;
        this.leg1TransitStatus = leg1TransitStatus;
        this.leg1UnitIndex = leg1UnitIndex;
        this.leg1DeclarationNumber = leg1DeclarationNumber;
        this.leg1TracingStatus = leg1TracingStatus;
        this.xLeg2UnitRef = xLeg2UnitRef;
        this.xLeg2DemandeRef = xLeg2DemandeRef;
        this.xLeg2DemandeStatus = xLeg2DemandeStatus;
        this.leg2ExpectedCustomerDeclarationDate = leg2ExpectedCustomerDeclarationDate;
        this.leg2ActualCustomerDeclarationDate = leg2ActualCustomerDeclarationDate;
        this.leg2Location = leg2Location;
        this.leg2TransitNumber = leg2TransitNumber;
        this.leg2TransitStatus = leg2TransitStatus;
        this.leg2UnitIndex = leg2UnitIndex;
        this.leg2DeclarationNumber = leg2DeclarationNumber;
        this.leg2TracingStatus = leg2TracingStatus;
        this.comment = comment;
        this.deadline = deadline;
        this.status = status;
        this.nbAlerts = nbAlerts;
        this.xLeg1DemandeNum = xLeg1DemandeNum;
        this.xLeg2DemandeNum = xLeg2DemandeNum;
        this.operationNumber = operationNumber;

        this.calculateFields();
    }

    private void calculateFields() {

        // daysSinceImport;
        if (leg1ActualCustomerDeclarationDate != null) {
            daysSinceImport = ChronoUnit.DAYS.between(leg1ActualCustomerDeclarationDate.toInstant(), Instant.now());
        }

        // daysBeforeDeadline;
        if (deadline != null && ChronoUnit.DAYS.between(Instant.now(), deadline.toInstant()) > 0) {
            daysBeforeDeadline = ChronoUnit.DAYS.between(Instant.now(), deadline.toInstant());
        }

        // scheduledAlert;
        Integer selectedAlertDays = null;

        if (deadline != null
            && ChronoUnit.DAYS.between(Instant.now(), deadline.toInstant()) >= xRegimeTemporaireAlert1Days) {
            selectedAlertDays = xRegimeTemporaireAlert1Days;
            alertNumber = 1;
        }
        else if (deadline != null
            && ChronoUnit.DAYS.between(Instant.now(), deadline.toInstant()) >= xRegimeTemporaireAlert2Days
            && ChronoUnit.DAYS.between(Instant.now(), deadline.toInstant()) < xRegimeTemporaireAlert1Days) {
            selectedAlertDays = xRegimeTemporaireAlert2Days;
            alertNumber = 2;
        }
        else if (deadline != null
            && ChronoUnit.DAYS.between(Instant.now(), deadline.toInstant()) >= xRegimeTemporaireAlert3Days
            && ChronoUnit.DAYS.between(Instant.now(), deadline.toInstant()) < xRegimeTemporaireAlert2Days) {
            selectedAlertDays = xRegimeTemporaireAlert3Days;
            alertNumber = 3;
        }

        if (selectedAlertDays != null) {
            scheduledAlert = Date.from(deadline.toInstant().minus(selectedAlertDays, ChronoUnit.DAYS));

            daysBeforeNextAlert = ChronoUnit.DAYS.between(Instant.now(), scheduledAlert.toInstant());
        }

    }

    public Integer getEbCptmProcedureNum() {
        return ebCptmProcedureNum;
    }

    public void setEbCptmProcedureNum(Integer ebCptmProcedureNum) {
        this.ebCptmProcedureNum = ebCptmProcedureNum;
    }

    public String getRefProcedure() {
        return refProcedure;
    }

    public void setRefProcedure(String refProcedure) {
        this.refProcedure = refProcedure;
    }

    public String getxLeg1UnitRef() {
        return xLeg1UnitRef;
    }

    public void setxLeg1UnitRef(String xLeg1UnitRef) {
        this.xLeg1UnitRef = xLeg1UnitRef;
    }

    public String getxLeg1DemandeRef() {
        return xLeg1DemandeRef;
    }

    public void setxLeg1DemandeRef(String xLeg1DemandeRef) {
        this.xLeg1DemandeRef = xLeg1DemandeRef;
    }

    public Integer getxLeg1DemandeStatus() {
        return xLeg1DemandeStatus;
    }

    public void setxLeg1DemandeStatus(Integer xLeg1DemandeStatus) {
        this.xLeg1DemandeStatus = xLeg1DemandeStatus;
    }

    public Date getLeg1ExpectedCustomerDeclarationDate() {
        return leg1ExpectedCustomerDeclarationDate;
    }

    public void setLeg1ExpectedCustomerDeclarationDate(Date leg1ExpectedCustomerDeclarationDate) {
        this.leg1ExpectedCustomerDeclarationDate = leg1ExpectedCustomerDeclarationDate;
    }

    public Date getLeg1ActualCustomerDeclarationDate() {
        return leg1ActualCustomerDeclarationDate;
    }

    public void setLeg1ActualCustomerDeclarationDate(Date leg1ActualCustomerDeclarationDate) {
        this.leg1ActualCustomerDeclarationDate = leg1ActualCustomerDeclarationDate;
    }

    public String getLeg1Location() {
        return leg1Location;
    }

    public void setLeg1Location(String leg1Location) {
        this.leg1Location = leg1Location;
    }

    public String getLeg1TransitNumber() {
        return leg1TransitNumber;
    }

    public void setLeg1TransitNumber(String leg1TransitNumber) {
        this.leg1TransitNumber = leg1TransitNumber;
    }

    public String getxLeg2UnitRef() {
        return xLeg2UnitRef;
    }

    public void setxLeg2UnitRef(String xLeg2UnitRef) {
        this.xLeg2UnitRef = xLeg2UnitRef;
    }

    public String getxLeg2DemandeRef() {
        return xLeg2DemandeRef;
    }

    public void setxLeg2DemandeRef(String xLeg2DemandeRef) {
        this.xLeg2DemandeRef = xLeg2DemandeRef;
    }

    public Integer getxLeg2DemandeStatus() {
        return xLeg2DemandeStatus;
    }

    public void setxLeg2DemandeStatus(Integer xLeg2DemandeStatus) {
        this.xLeg2DemandeStatus = xLeg2DemandeStatus;
    }

    public Date getLeg2ExpectedCustomerDeclarationDate() {
        return leg2ExpectedCustomerDeclarationDate;
    }

    public void setLeg2ExpectedCustomerDeclarationDate(Date leg2ExpectedCustomerDeclarationDate) {
        this.leg2ExpectedCustomerDeclarationDate = leg2ExpectedCustomerDeclarationDate;
    }

    public Date getLeg2ActualCustomerDeclarationDate() {
        return leg2ActualCustomerDeclarationDate;
    }

    public void setLeg2ActualCustomerDeclarationDate(Date leg2ActualCustomerDeclarationDate) {
        this.leg2ActualCustomerDeclarationDate = leg2ActualCustomerDeclarationDate;
    }

    public String getLeg2Location() {
        return leg2Location;
    }

    public void setLeg2Location(String leg2Location) {
        this.leg2Location = leg2Location;
    }

    public String getLeg2TransitNumber() {
        return leg2TransitNumber;
    }

    public void setLeg2TransitNumber(String leg2TransitNumber) {
        this.leg2TransitNumber = leg2TransitNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getDeadline() {
        return deadline;
    }

    public void setDeadline(Date deadline) {
        this.deadline = deadline;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getNbAlerts() {
        return nbAlerts;
    }

    public void setNbAlerts(Integer nbAlerts) {
        this.nbAlerts = nbAlerts;
    }

    public Date getScheduledAlert() {
        return scheduledAlert;
    }

    public void setScheduledAlert(Date scheduledAlert) {
        this.scheduledAlert = scheduledAlert;
    }

    public Long getDaysSinceImport() {
        return daysSinceImport;
    }

    public void setDaysSinceImport(Long daysSinceImport) {
        this.daysSinceImport = daysSinceImport;
    }

    public Long getDaysBeforeDeadline() {
        return daysBeforeDeadline;
    }

    public void setDaysBeforeDeadline(Long daysBeforeDeadline) {
        this.daysBeforeDeadline = daysBeforeDeadline;
    }

    public String getxRegimeTemporaireLeg1Name() {
        return xRegimeTemporaireLeg1Name;
    }

    public void setxRegimeTemporaireLeg1Name(String xRegimeTemporaireLeg1Name) {
        this.xRegimeTemporaireLeg1Name = xRegimeTemporaireLeg1Name;
    }

    public String getxRegimeTemporaireLeg2Name() {
        return xRegimeTemporaireLeg2Name;
    }

    public void setxRegimeTemporaireLeg2Name(String xRegimeTemporaireLeg2Name) {
        this.xRegimeTemporaireLeg2Name = xRegimeTemporaireLeg2Name;
    }

    public Integer getxRegimeTemporaireAlert1Days() {
        return xRegimeTemporaireAlert1Days;
    }

    public void setxRegimeTemporaireAlert1Days(Integer xRegimeTemporaireAlert1Days) {
        this.xRegimeTemporaireAlert1Days = xRegimeTemporaireAlert1Days;
    }

    public Integer getxRegimeTemporaireAlert2Days() {
        return xRegimeTemporaireAlert2Days;
    }

    public void setxRegimeTemporaireAlert2Days(Integer xRegimeTemporaireAlert2Days) {
        this.xRegimeTemporaireAlert2Days = xRegimeTemporaireAlert2Days;
    }

    public Integer getxRegimeTemporaireAlert3Days() {
        return xRegimeTemporaireAlert3Days;
    }

    public void setxRegimeTemporaireAlert3Days(Integer xRegimeTemporaireAlert3Days) {
        this.xRegimeTemporaireAlert3Days = xRegimeTemporaireAlert3Days;
    }

    public Boolean getLeg1TransitStatus() {
        return leg1TransitStatus;
    }

    public void setLeg1TransitStatus(Boolean leg1TransitStatus) {
        this.leg1TransitStatus = leg1TransitStatus;
    }

    public Boolean getLeg2TransitStatus() {
        return leg2TransitStatus;
    }

    public void setLeg2TransitStatus(Boolean leg2TransitStatus) {
        this.leg2TransitStatus = leg2TransitStatus;
    }

    public Integer getLeg1UnitIndex() {
        return leg1UnitIndex;
    }

    public void setLeg1UnitIndex(Integer leg1UnitIndex) {
        this.leg1UnitIndex = leg1UnitIndex;
    }

    public Integer getLeg2UnitIndex() {
        return leg2UnitIndex;
    }

    public void setLeg2UnitIndex(Integer leg2UnitIndex) {
        this.leg2UnitIndex = leg2UnitIndex;
    }

    public String getLeg1DeclarationNumber() {
        return leg1DeclarationNumber;
    }

    public void setLeg1DeclarationNumber(String leg1DeclarationNumber) {
        this.leg1DeclarationNumber = leg1DeclarationNumber;
    }

    public String getLeg2DeclarationNumber() {
        return leg2DeclarationNumber;
    }

    public void setLeg2DeclarationNumber(String leg2DeclarationNumber) {
        this.leg2DeclarationNumber = leg2DeclarationNumber;
    }

    public String getLeg1TracingStatus() {
        return leg1TracingStatus;
    }

    public void setLeg1TracingStatus(String leg1TracingStatus) {
        this.leg1TracingStatus = leg1TracingStatus;
    }

    public String getLeg2TracingStatus() {
        return leg2TracingStatus;
    }

    public void setLeg2TracingStatus(String leg2TracingStatus) {
        this.leg2TracingStatus = leg2TracingStatus;
    }

    public Integer getxLeg1DemandeNum() {
        return xLeg1DemandeNum;
    }

    public void setxLeg1DemandeNum(Integer xLeg1DemandeNum) {
        this.xLeg1DemandeNum = xLeg1DemandeNum;
    }

    public Integer getxLeg2DemandeNum() {
        return xLeg2DemandeNum;
    }

    public void setxLeg2DemandeNum(Integer xLeg2DemandeNum) {
        this.xLeg2DemandeNum = xLeg2DemandeNum;
    }

    public Integer getAlertNumber() {
        return alertNumber;
    }

    public void setAlertNumber(Integer alertNumber) {
        this.alertNumber = alertNumber;
    }

    public Long getDaysBeforeNextAlert() {
        return daysBeforeNextAlert;
    }

    public void setDaysBeforeNextAlert(Long daysBeforeNextAlert) {
        this.daysBeforeNextAlert = daysBeforeNextAlert;
    }

    public Date getLatestAlertDate() {
        return latestAlertDate;
    }

    public void setLatestAlertDate(Date latestAlertDate) {
        this.latestAlertDate = latestAlertDate;
    }

    public String getOperationNumber() {
        return operationNumber;
    }

    public void setOperationNumber(String operationNumber) {
        this.operationNumber = operationNumber;
    }

    public String getxRegimeTemporaireLeg1Code() {
        return xRegimeTemporaireLeg1Code;
    }

    public void setxRegimeTemporaireLeg1Code(String xRegimeTemporaireLeg1Code) {
        this.xRegimeTemporaireLeg1Code = xRegimeTemporaireLeg1Code;
    }

    public String getxRegimeTemporaireLeg2Code() {
        return xRegimeTemporaireLeg2Code;
    }

    public void setxRegimeTemporaireLeg2Code(String xRegimeTemporaireLeg2Code) {
        this.xRegimeTemporaireLeg2Code = xRegimeTemporaireLeg2Code;
    }
}
