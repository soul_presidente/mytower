package com.adias.mytowereasy.cptm.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.adias.mytowereasy.util.GenericEnum;


public class CPTMEnumeration {
    public enum ProcedureStatus implements GenericEnum {
        IN_PROGRESS(0, "IN_PROGRESS"), APURE(1, "APURE"), LIQUIDE(2, "LIQUIDE"), DESTROYED(3, "DESTROYED");

        private Integer code;
        private String key;

        private ProcedureStatus(Integer code, String key) {
            this.code = code;
            this.key = key;
        }

        @Override
        public Integer getCode() {
            return this.code;
        }

        @Override
        public String getKey() {
            return this.key;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum SearchCptmFields {
        REF_PROCEDURE(1, "refProcedure"),
        REF_UNIT_LEG_1(2, "refUnitLeg1"),
        TYPE_SEGMENT_LEG_1(3, "typeSegmentLeg1"),
        REF_TRANSPORT_LEG_1(4, "refTransporttLeg1"),
        NUM_DECLARATION_LEG_1(5, "numDeclarationLeg1"),
        LOCALISATION_LEG_1(6, "localisationLeg1"),
        NUM_TRANSIT_LEG_1(7, "numTransitLeg1"),
        STATUS_TRANSIT_LEG_1(8, "statusTransitLeg1"),
        TRACKING_LEG_1(9, "trackingLeg1"),
        REF_UNIT_LEG_2(10, "refUnitLeg2"),
        TYPE_SEGMENT_LEG_2(11, "typeSegmentLeg2"),
        REF_TRANSPORT_LEG_2(12, "refTransporttLeg2"),
        NUM_DECLARATION_LEG_2(13, "numDeclarationLeg2"),
        LOCALISATION_LEG_2(14, "localisationLeg2"),
        NUM_TRANSIT_LEG_2(15, "numTransitLeg2"),
        STATUS_TRANSIT_LEG_2(16, "statusTransitLeg2"),
        TRACKING_LEG_2(17, "trackingLeg2"),
        STATUS(18, "status"),
        NB_ALERTS(19, "nbAlerts"),
        CODE_SEGMENT_LEG_1(20, "codeSegmentLeg1"),
        CODE_SEGMENT_LEG_2(21, "codeSegmentLeg2");

        private Integer code;

        private String libelle;

        private SearchCptmFields(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }
}
