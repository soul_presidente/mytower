package com.adias.mytowereasy.cptm.service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.adias.mytowereasy.cptm.dao.DaoCptmProcedure;
import com.adias.mytowereasy.cptm.dto.EbCptmProcedureDTO;
import com.adias.mytowereasy.cptm.dto.EbCptmRegimeSelectionDTO;
import com.adias.mytowereasy.cptm.model.CPTMEnumeration.ProcedureStatus;
import com.adias.mytowereasy.cptm.model.EbCptmProcedure;
import com.adias.mytowereasy.cptm.model.EbCptmRegimeTemporaire;
import com.adias.mytowereasy.cptm.model.SearchCriteriaCPTM;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbEmailHistorique;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.CustomsDeclarationStatus;
import com.adias.mytowereasy.model.custom.EbCustomDeclaration;
import com.adias.mytowereasy.model.qm.SemiAutoMail;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.repository.EbEmailHistoriqueRepository;
import com.adias.mytowereasy.service.CustomDeclarationService;
import com.adias.mytowereasy.service.ListStatiqueService;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.util.email.Email;
import com.adias.mytowereasy.util.email.EmailHtmlSender;


@Service
public class ComptaMatiereServiceImpl extends MyTowerService implements ComptaMatiereService {
    @Autowired
    DaoCptmProcedure daoProcedure;

    @Autowired
    CustomDeclarationService customDeclarationService;

    @Autowired
    EbEmailHistoriqueRepository ebEmailHistoriqueRepository;

    @Autowired
    ListStatiqueService serviceListStatique;

    @Autowired
    private EmailHtmlSender emailHtmlSender;

    @Autowired(required = false)
    private TemplateEngine templateEngine;

    @Autowired
    private MessageSource messageSource;

    @Override
    public List<EbCptmProcedureDTO> listProcedureDto(SearchCriteriaCPTM criteria) {
        return daoProcedure.listDTO(criteria);
    }

    @Override
    public Long countListProcedure(SearchCriteriaCPTM criteria) {
        return daoProcedure.listCount(criteria);
    }

    @Override
    public EbCptmProcedure inlineProcedureDashboardUpdate(EbCptmProcedureDTO sent) {
        EbCptmProcedure entity = ebCptmProcedureRepository.findById(sent.getEbCptmProcedureNum()).get();

        // Update field by field for security reason
        entity.setComment(sent.getComment());

        entity.setLeg1Location(sent.getLeg1Location());
        entity.setLeg1ExpectedCustomerDeclarationDate(sent.getLeg1ExpectedCustomerDeclarationDate());
        entity.setLeg1TransitNumber(sent.getLeg1TransitNumber());
        entity.setLeg1TransitStatus(sent.getLeg1TransitStatus());

        entity.setLeg2Location(sent.getLeg2Location());
        entity.setLeg2ExpectedCustomerDeclarationDate(sent.getLeg2ExpectedCustomerDeclarationDate());
        entity.setLeg2TransitNumber(sent.getLeg2TransitNumber());
        entity.setLeg2TransitState(sent.getLeg2TransitStatus());

        entity = ebCptmProcedureRepository.save(entity);

        // Update date declaration
        if (entity.getxLeg1Unit() != null && entity.getxLeg1Unit().getxEbCustomDeclaration() != null
            && sent.getLeg1ActualCustomerDeclarationDate() != null) {
            customDeclarationService
                .updateDateDeclaration(
                    entity.getxLeg1Unit().getxEbCustomDeclaration(),
                    sent.getLeg1ActualCustomerDeclarationDate());
        }

        if (entity.getxLeg2Unit() != null && entity.getxLeg2Unit().getxEbCustomDeclaration() != null
            && sent.getLeg2ActualCustomerDeclarationDate() != null) {
            customDeclarationService
                .updateDateDeclaration(
                    entity.getxLeg2Unit().getxEbCustomDeclaration(),
                    sent.getLeg2ActualCustomerDeclarationDate());
        }

        return ebCptmProcedureRepository.findById(entity.getEbCptmProcedureNum()).get();
    }

    @Override
    public List<EbCptmRegimeSelectionDTO> listRegimesAndProceduresForSelection() {
        final Integer connectectedUserCompanyNum = connectedUserService
            .getCurrentUser().getEbCompagnie().getEbCompagnieNum();
        final List<EbCptmRegimeSelectionDTO> rList = new ArrayList<>();
        final EbCptmRegimeSelectionDTO separator = new EbCptmRegimeSelectionDTO();
        separator.setLabel("--------");

        // Regimes
        List<EbCptmRegimeTemporaire> regimes = ebCptmRegimeTemporaireRepository
            .findActivesForCompany(connectectedUserCompanyNum);
        regimes.forEach(r -> {
            EbCptmRegimeSelectionDTO newItem = new EbCptmRegimeSelectionDTO();
            newItem.setEbCptmRegimeTemporaireNum(r.getEbCptmRegimeTemporaireNum());
            newItem.setLabel(r.getType());
            newItem.setLabelForSearch(r.getType());
            rList.add(newItem);
        });

        // Procedures
        SearchCriteriaCPTM criteriaProcedures = new SearchCriteriaCPTM();
        criteriaProcedures.setWithoutLeg2(true);
        criteriaProcedures.setEbCompagnieNum(connectectedUserCompanyNum);
        List<EbCptmRegimeSelectionDTO> procedures = daoProcedure.listRegimeSelectionDTO(criteriaProcedures);

        if (procedures.size() > 0) {
            // Separator
            rList.add(separator);
        }

        rList.addAll(procedures);

        return rList;
    }

    @Override
    public void generateProcedures(EbDemande ebDemande, List<EbTtTracing> ebTtTracings) {

        if (ebTtTracings != null && !ebTtTracings.isEmpty()) {

            for (EbTtTracing tt: ebTtTracings) {

                if (tt.getxEbMarchandise() != null) {

                    if (tt.getxEbMarchandise().getEbMarchandiseNum() != null) {
                        Optional<EbMarchandise> marchandiseOptional = ebMarchandiseRepository
                            .findById(tt.getxEbMarchandise().getEbMarchandiseNum());

                        if (marchandiseOptional.isPresent()) {
                            EbMarchandise marchandise = marchandiseOptional.get();

                            if (marchandise.getCptmProcedureNum() != null) {
                                generateLeg2ForProcedure(ebDemande, tt, marchandise);
                            }
                            else if (marchandise.getCptmRegimeTemporaireNum() != null) {
                                generateNewProcedure(ebDemande, tt, marchandise);
                            }

                        }

                    }

                }

            }

        }

    }

    private void generateNewProcedure(EbDemande ebDemande, EbTtTracing tt, EbMarchandise marchandise) {
        // Retrieve items
        Optional<EbCptmRegimeTemporaire> regime = ebCptmRegimeTemporaireRepository
            .findById(marchandise.getCptmRegimeTemporaireNum());
        if (!regime.isPresent()) return;

        final Integer quantity = marchandise.getNumberOfUnits() != null ? marchandise.getNumberOfUnits() : 1;

        for (int unitIndex = 0; unitIndex < quantity; unitIndex++) {
            // Generate procedure
            EbCptmProcedure procedure = new EbCptmProcedure();
            procedure.setEbCompagnie(connectedUserService.getCurrentUser().getEbCompagnie());
            procedure.setxRegimeTemporaire(regime.get());

            procedure.setStatus(0);
            procedure.setNbAlerts(0);

            // Leg 1
            procedure.setxLeg1Demande(ebDemande);
            procedure.setxLeg1Tracing(tt);
            procedure.setxLeg1Unit(marchandise);
            procedure.setLeg1TransitStatus(false);
            procedure.setLeg1UnitIndex(unitIndex);

            procedure = ebCptmProcedureRepository.save(procedure);

            // Generate reference
            String genRef = String.format("RTR-%d", procedure.getEbCptmProcedureNum());
            procedure.setRefProcedure(genRef);
            procedure = ebCptmProcedureRepository.save(procedure);
        }

    }

    private void generateLeg2ForProcedure(EbDemande ebDemande, EbTtTracing tt, EbMarchandise marchandise) {
        // Retrieve items
        Optional<EbCptmProcedure> procedureOpt = ebCptmProcedureRepository.findById(marchandise.getCptmProcedureNum());
        if (!procedureOpt.isPresent()) return;
        EbCptmProcedure procedure = procedureOpt.get();

        // Checks
        if (procedure.getxLeg2Demande() != null) return;

        procedure.setStatus(1);
        // Leg2
        procedure.setxLeg2Demande(ebDemande);
        procedure.setxLeg2Tracing(tt);
        procedure.setxLeg2Unit(marchandise);
        procedure.setLeg2TransitState(false);
        procedure.setLeg2UnitIndex(0);

        procedure = ebCptmProcedureRepository.save(procedure);

        // Update declaration status leg1
        if (procedure.getxLeg1Unit() != null && procedure.getxLeg1Unit().getxEbCustomDeclaration() != null) {
            EbCustomDeclaration dec = procedure.getxLeg1Unit().getxEbCustomDeclaration();
            List<EbCptmProcedure> relatedProcedure = ebCptmProcedureRepository
                .findNonFinishedByDeclaration(dec.getEbCustomDeclarationNum());

            if (relatedProcedure.size() == 0) {
                dec.setStatut(CustomsDeclarationStatus.APURE.getCode());
                ebDeclarationRepository.save(dec);
            }

        }

        // Update declaration status leg2
        if (procedure.getxLeg2Unit() != null && procedure.getxLeg2Unit().getxEbCustomDeclaration() != null) {
            EbCustomDeclaration dec = procedure.getxLeg2Unit().getxEbCustomDeclaration();
            List<EbCptmProcedure> relatedProcedure = ebCptmProcedureRepository
                .findNonFinishedByDeclaration(dec.getEbCustomDeclarationNum());

            if (relatedProcedure.size() == 0) {
                dec.setStatut(CustomsDeclarationStatus.APURE.getCode());
                ebDeclarationRepository.save(dec);
            }

        }

    }

    @Override
    public Integer getNextProcedureNum() {
        return daoProcedure.nextProcedureId();
    }

    @Override
    public SemiAutoMail generateSemiAutoMailTemplateForProcedure(Integer mailId, Integer procedureNum, EbUser sender) {
        EbCptmProcedure procedure = ebCptmProcedureRepository.findById(procedureNum).get();
        return generateSemiAutoMailTemplateForProcedure(mailId, procedure, sender);
    }

    @Override
    public SemiAutoMail
        generateSemiAutoMailTemplateForProcedure(Integer mailId, EbCptmProcedure procedure, EbUser sender) {
        Context context = new Context();
        context.setVariable("procedure", procedure);
        context.setVariable("sender", sender);

        String demandeMailLabel = "";

        if (Enumeration.MailSemiAuto.CPTM_PROLONGER_IMPORTATION_TEMPORAIRE.getCode().equals(mailId)) {
            demandeMailLabel = "Demande de prolongation";
        }

        if (Enumeration.MailSemiAuto.CPTM_LIQUIDATION_OFFICE.getCode().equals(mailId)) {
            demandeMailLabel = "Demande de liquidation d'office";
        }

        if (Enumeration.MailSemiAuto.CPTM_RELANCE.getCode().equals(mailId)) {
            demandeMailLabel = "Relance";
        }

        if (Enumeration.MailSemiAuto.CPTM_DEMANDE_ETAT_AVANCEMENT.getCode().equals(mailId)) {
            demandeMailLabel = "Demande d'état d'avancement";
        }

        context.setVariable("demandeMailLabel", demandeMailLabel);

        Enumeration.MailSemiAuto mailEnum = Enumeration.MailSemiAuto.auto(mailId);

        SemiAutoMail model = new SemiAutoMail();

        String body = templateEngine.process(mailEnum.getTemplate(), context);
        model.setText(body);

        // EmailHtmlSender force actuellement l'anglais mais on a besoin du fr
        // ici pour nexter, à traiter avec la correction du EmailHtmlSender
        model.setSubject(messageSource.getMessage(mailEnum.getSubject(), null, new Locale("fr")));

        return model;
    }

    @Override
    public EbEmailHistorique sendSemiAutoMail(Email mailToSend, Integer procedureNum) {
        // First insert to empty content template
        Context context = new Context();
        context.setVariable("content", mailToSend.getText());
        String body = templateEngine.process("45-empty-email-content", context);
        mailToSend.setText(body);

        emailHtmlSender.sendSemiAutoMail(mailToSend);
        EbEmailHistorique emailHistorique = new EbEmailHistorique();
        emailHistorique.setTto(mailToSend.getTo());
        emailHistorique.setText(mailToSend.getText());
        emailHistorique.setSubject(mailToSend.getSubject());
        emailHistorique.setObjectNum(procedureNum);
        emailHistorique.setDateEnvoi(new Date());
        emailHistorique.setTypeEmailId(mailToSend.getMailType());

        if (mailToSend.getEnCopie() != null) emailHistorique.setEnCopie(String.join(";", mailToSend.getEnCopie()));
        if (mailToSend.getEnCopieCachee() != null) emailHistorique
            .setEnCopieCachee(String.join(";", mailToSend.getEnCopieCachee()));

        ebEmailHistoriqueRepository.save(emailHistorique);

        EbCptmProcedure procedure = ebCptmProcedureRepository.findById(procedureNum).get();

        if (Enumeration.MailSemiAuto.CPTM_RELANCE.getCode().equals(mailToSend.getMailType())) {

            if (procedure.getNbAlerts() == null) {
                procedure.setNbAlerts(1);
            }
            else {
                procedure.setNbAlerts(procedure.getNbAlerts() + 1);
            }

            procedure.setLatestAlertDate(new Date());

            ebCptmProcedureRepository.save(procedure);
        }

        return emailHistorique;
    }

    @Override
    public boolean areAllCompleted(List<EbCptmProcedure> procedures) {

        for (EbCptmProcedure procedure: procedures) {
            if (procedure.getxLeg2Unit() == null) return false;
        }

        return true;
    }

    @Override
    public void emailReminderAuto() {
        SearchCriteriaCPTM criteria = new SearchCriteriaCPTM();
        criteria.setLeg2NotFilled(true);
        final List<EbCptmProcedureDTO> searchResults = daoProcedure.listDTO(criteria);
        final List<EbCptmProcedureDTO> toAlert = searchResults.stream().filter(p -> {

            if(!ProcedureStatus.IN_PROGRESS.getCode().equals(p.getStatus()))
            {
                // If another status than in progress, it's completed
                return false;
            }

            if (p.getDaysBeforeNextAlert() == null || !p.getDaysBeforeNextAlert().equals(0l)) {
                // Exclude if today is not the day of alert
                return false;
            }

            if (p.getLatestAlertDate() != null
                && ChronoUnit.DAYS.between(Instant.now(), p.getLatestAlertDate().toInstant()) == 0) {
                // Exclude if already alerted today
                return false;
            }

            return true;
        }).collect(Collectors.toList());

        for (EbCptmProcedureDTO pDTO: toAlert) {
            Integer pNum = pDTO.getEbCptmProcedureNum();
            EbCptmProcedure procedure = ebCptmProcedureRepository.findById(pNum).get();

            sendEmailAutoReminder(procedure);

            if (procedure.getNbAlerts() == null) procedure.setNbAlerts(0);
            procedure.setNbAlerts(procedure.getNbAlerts() + 1);
            procedure.setLatestAlertDate(new Date());

            ebCptmProcedureRepository.save(procedure);
        }

    }

    @Override
    public void sendEmailAutoReminder(EbCptmProcedure procedure) {
        Context params = new Context();
        Email email = new Email();

        SemiAutoMail semiAutoMail = generateSemiAutoMailTemplateForProcedure(
            Enumeration.MailSemiAuto.CPTM_RELANCE.getCode(),
            procedure,
            null);

        String subject = "cptm.mail.relance.subject";

        // Le mail est envoyé au chargeur et à ses collègues selon paramétrage
        // my alert
        Map<String, Object> res = serviceListStatique
            .listDestinataireByEmailId(
                Enumeration.EmailConfig.CPTM_REMINDER.getCode(),
                Enumeration.Module.PRICING.getCode(),
                procedure.getxLeg1Demande().getEbDemandeNum());

        if (res != null && !res.isEmpty()) {
            email.setEmailConfig(Enumeration.EmailConfig.CPTM_REMINDER);
            email.setSubject(subject);
            email.setArguments(new String[]{});
            params.setVariable("content", semiAutoMail.getText());
            emailHtmlSender
                .sendListEmail((List<String>) res.get("emails"), email, "45-empty-email-content", params, subject);
        }

    }

    @Override
    public List<?> advancedSearchAutocomplete(String term, Integer field, Boolean searchField) {
        List<?> searchProcedureList = daoProcedure.advancedSearchAutocomplete(term, field, searchField);
        return searchProcedureList;
    }
}
