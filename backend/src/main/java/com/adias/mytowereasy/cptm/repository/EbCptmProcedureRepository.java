package com.adias.mytowereasy.cptm.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.cptm.model.EbCptmProcedure;
import com.adias.mytowereasy.repository.CommonRepository;


@Repository
public interface EbCptmProcedureRepository extends CommonRepository<EbCptmProcedure, Integer> {
    @Query("SELECT p FROM EbCptmProcedure p WHERE p.xLeg1Unit.ebMarchandiseNum IN (?1) OR p.xLeg2Unit.ebMarchandiseNum IN (?1)")
    public List<EbCptmProcedure> findByMarchandiseNum(List<Integer> listEbDemandeNum);

    @Query("from EbCptmProcedure r "
        + "where (r.xLeg1Unit.xEbCustomDeclaration.ebCustomDeclarationNum = :ebCustomDeclarationNum "
        + "or r.xLeg2Unit.xEbCustomDeclaration.ebCustomDeclarationNum = :ebCustomDeclarationNum) "
        + "and r.xLeg2Demande is null " + "order by r.refProcedure asc")
    public List<EbCptmProcedure>
        findNonFinishedByDeclaration(@Param("ebCustomDeclarationNum") Integer ebCustomDeclarationNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbCptmProcedure cp WHERE cp.xLeg2Tracing.ebTtTracingNum IN (?1) OR cp.xLeg1Tracing.ebTtTracingNum IN (?1)")
    public Integer deleteByEbTracingNum(List<Integer> listEbDemandeNum);

    @Query("SELECT p FROM EbCptmProcedure p WHERE p.xLeg1Unit.xEbCustomDeclaration.ebCustomDeclarationNum = ?1")
    public List<EbCptmProcedure> findByLeg1Declaration(Integer leg1CustomDeclarationNum);
}
