package com.adias.mytowereasy.rsch.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.adias.mytowereasy.dto.PricingCarrierSearchResultDTO;
import com.adias.mytowereasy.rsch.algo.DockIntervals;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschAlgoResult;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschModeTransportOpta;
import com.adias.mytowereasy.rsch.model.EbRschUnitSchedule;


public interface ReceiptSchedulingAlgoService {
    /**
     * The main and only entry point to start the
     * RSCH Unit Schedule calculation for the current connected user
     */
    public void runCalc();

    /**
     * Retrieve from BDD all EbRschUnitSchedule
     * Available to calculation for the current connected user
     * 
     * @see EbRschUnitSchedule
     * @return List of EbRschUnitSchedule available for calculation
     */
    public List<EbRschUnitSchedule> getRschUnitScheduleAvailableToCalc();

    /**
     * Get all availabilities for all dock visible by the user<br/>
     * Take in consideration periods and default week configuration
     */
    public List<DockIntervals> getDockAvailabilities(
        Date dateToCheck,
        Set<Integer> listEntrepotNum,
        List<RschModeTransportOpta> listPropTarif);

    /**
     * Execute the calculation algorithm with OptaPlanner
     * 
     * @param listRschUS
     *            All EbRschUnitSchedule available for calculation
     * @param dockAvailabilities
     *            All dock opening availabilities
     * @return Result object containing input ibjects with filled solutions
     */
    public RschAlgoResult runOptaplanner(List<EbRschUnitSchedule> listRschUS, List<DockIntervals> dockAvailabilities
    // ,List<TransitTimeRSCHModeTransport>
    // listTransitTimeRSCHModeTransport,List<RschDateDepart> listRschDateDepart
    );

    public List<PricingCarrierSearchResultDTO> getTotalCost(EbRschUnitSchedule listEbRsch);
}
