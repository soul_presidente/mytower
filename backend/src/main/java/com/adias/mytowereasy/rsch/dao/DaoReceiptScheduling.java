package com.adias.mytowereasy.rsch.dao;

import java.util.List;

import com.adias.mytowereasy.rsch.model.EbRschUnitSchedule;
import com.adias.mytowereasy.rsch.model.SearchCriteriaRSCH;


public interface DaoReceiptScheduling {
    public List<EbRschUnitSchedule> searchRschUnitSchedule(SearchCriteriaRSCH criteria);

    public Long countListEbRschUnitSchedule(SearchCriteriaRSCH criteria);
}
