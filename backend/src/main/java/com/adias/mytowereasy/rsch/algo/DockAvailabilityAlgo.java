package com.adias.mytowereasy.rsch.algo;

import com.adias.mytowereasy.dock.model.EbDkOpeningPeriod;
import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.model.EbRangeHour;


/**
 * This class is bypass object for the dock disponibility
 * calculation during the algo process
 */
public class DockAvailabilityAlgo {
    private EbDock dock;
    private EbRangeHour rh;
    private EbDkOpeningPeriod op;
    private Boolean open;

    public DockAvailabilityAlgo() {
        super();
        // TODO Auto-generated constructor stub
    }

    public DockAvailabilityAlgo(EbDock dock, EbRangeHour rh) {
        super();
        this.dock = dock;
        this.rh = rh;
    }

    public Boolean getOpen() {
        return open;
    }

    public void setOpen(Boolean open) {
        this.open = open;
    }

    public EbDock getDock() {
        return dock;
    }

    public void setDock(EbDock dock) {
        this.dock = dock;
    }

    public EbRangeHour getRh() {
        return rh;
    }

    public void setRh(EbRangeHour rh) {
        this.rh = rh;
    }

    public DockAvailabilityAlgo(EbDock dock, EbRangeHour rh, EbDkOpeningPeriod op) {
        super();
        this.dock = dock;
        this.rh = rh;
        this.op = op;
    }

    public DockAvailabilityAlgo(EbDock dock, EbRangeHour rh, EbDkOpeningPeriod op, Boolean open) {
        super();
        this.dock = dock;
        this.rh = rh;
        this.op = op;
        this.open = open;
    }

    public EbDkOpeningPeriod getOp() {
        return op;
    }

    public void setOp(EbDkOpeningPeriod op) {
        this.op = op;
    }

    @Override
    public String toString() {
        return "\nDockWithRangeHour: (dock=" + dock.getEbDockNum() + ", rh= ["
            + StatiqueAlgo.formatDateOutput(rh.getStartHour()) + "," + StatiqueAlgo.formatDateOutput(rh.getEndHour())
            + "])";
    }
}
