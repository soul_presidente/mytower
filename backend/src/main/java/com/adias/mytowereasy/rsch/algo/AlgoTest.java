package com.adias.mytowereasy.rsch.algo;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschAlgoResult;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschDockOpenCreneauOpta;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschUnitOptaParamsRoot;
import com.adias.mytowereasy.rsch.algo.optaPlanner.persistance.AlgorithmeGenerator;
import com.adias.mytowereasy.rsch.model.EbRschUnitSchedule;


public class AlgoTest {
    public static void lanceAlgoCalculator(List<EbRschUnitSchedule> listEbRsch, List<DockIntervals> listDockInterval) {
        // Build the Solver
        SolverFactory<RschAlgoResult> solverFactory = SolverFactory
            .createFromXmlResource("rsch.algo.optaPlanner.solver/rschSolverConfig.xml");
        Solver<RschAlgoResult> solver = solverFactory.buildSolver();
        // Load a problem
        RschAlgoResult unsolvedAffectContainer = new AlgorithmeGenerator()
            .createAffectContainer(listEbRsch, listDockInterval);
        // Solve the problem
        RschAlgoResult solvedAffectContainer = solver.solve(unsolvedAffectContainer);
        // Display the result
        System.out.println("\nSolved AffectContainer \n" + toDisplayString(solvedAffectContainer));
    }

    public static String toDisplayString(RschAlgoResult affectContainer) {
        StringBuilder displayString = new StringBuilder();

        for (RschUnitOptaParamsRoot container: affectContainer.getContainerList()) {
            RschDockOpenCreneauOpta cq = container.getCreneauQuais();
            displayString
                .append("  ")
                .append(
                    container.getRschUnitSchedule().getEbRschUnitScheduleNum() + " wd:"
                        + container.getRschUnitSchedule().getWorkDuration())
                .append(" -> ").append(cq == null ? null : "dock:" + cq.getEbDockNum() + " ").append("\n");
        }

        return displayString.toString();
    }

    public static List<EbRschUnitSchedule> getListEbRsch() throws ParseException {
        List<EbRschUnitSchedule> l = new ArrayList<>();
        l
            .add(
                new EbRschUnitSchedule(
                    1,
                    new SimpleDateFormat("HH:mm").parse("02:00"),
                    new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2018-11-19 11:00")));
        l
            .add(
                new EbRschUnitSchedule(
                    2,
                    new SimpleDateFormat("HH:mm").parse("00:30"),
                    new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2018-11-19 10:00")));
        l
            .add(
                new EbRschUnitSchedule(
                    3,
                    new SimpleDateFormat("HH:mm").parse("01:30"),
                    new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2018-11-19 16:00")));
        return l;
    }

    public static List<DockIntervals> getListDockInterval() throws ParseException {
        List<DockIntervals> l = new ArrayList<>();
        List<Interval> dockInt = new ArrayList<>();
        dockInt
            .add(
                new Interval(
                    new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2018-11-19 08:00"),
                    new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2018-11-19 12:00")));
        l.add(new DockIntervals(new EbDock(1, "dock 1"), dockInt));
        dockInt = new ArrayList<>();
        dockInt
            .add(
                new Interval(
                    new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2018-11-19 10:00"),
                    new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2018-11-19 13:30")));
        dockInt
            .add(
                new Interval(
                    new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2018-11-19 15:00"),
                    new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2018-11-19 20:30")));

        l.add(new DockIntervals(new EbDock(2, "dock 2"), dockInt));

        return l;
    }

    public static void main(String[] args) throws ParseException {
        // lanceAlgoCalculator(getListEbRsch(),getListDockInterval());
        // List<RschUnitOptaParamsRoot> listContainer=new ArrayList<>();
        // listContainer.add(new RschUnitOptaParamsRoot(new
        // RschUnitScheduleOpta(1),
        // new RschDockOpenCreneauOpta(3,
        // new Interval(new SimpleDateFormat("yyyy-MM-dd
        // HH:mm").parse("2018-11-19 9:15"),
        // new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2018-11-19
        // 10:15"))),new RschModeTransportOpta()));
        // listContainer.add(new RschUnitOptaParamsRoot(new
        // RschUnitScheduleOpta(1),
        // new RschDockOpenCreneauOpta(3,
        // new Interval(new SimpleDateFormat("yyyy-MM-dd
        // HH:mm").parse("2018-11-19 09:00"),
        // new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2018-11-19
        // 09:30"))),new RschModeTransportOpta()));
        // listContainer.add(new RschUnitOptaParamsRoot(new
        // RschUnitScheduleOpta(1),
        // new RschDockOpenCreneauOpta(4,
        // new Interval(new SimpleDateFormat("yyyy-MM-dd
        // HH:mm").parse("2018-11-19 10:00"),
        // new SimpleDateFormat("yyyy-MM-dd HH:mm").parse("2018-11-19
        // 13:30"))),new RschModeTransportOpta()));
        //
        // System.out.println(StatiqueAlgo.isOverlapIntoSolution(listContainer));
    }

    public static void ecritureDansUnFichier(String txt) {

        try {
            PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("D:/data_test/myfile.txt", true)));
            out.println(txt);
            out.flush();
            out.close();
        } catch (IOException e) {
            System.out.println("erreeeeeeeeeur file");
        }

    }

    public static void deleteFile() {
        File f = new File("D:/data_test/myfile.txt");

        if (f.exists()) {
            f.delete();
        }

    }
}
