package com.adias.mytowereasy.rsch.algo.optaPlanner.domain;

public class TransitTimeRSCHModeTransport {
    private RschModeTransportOpta modetransport;

    private Integer ebRschUnitScheduleNum;

    private Double costTransport;

    private Integer transitTime;

    public TransitTimeRSCHModeTransport(
        RschModeTransportOpta modetransport,
        Integer ebRschUnitScheduleNum,
        Double costTransport,
        Integer transitTime) {
        this.modetransport = modetransport;
        this.ebRschUnitScheduleNum = ebRschUnitScheduleNum;
        this.costTransport = costTransport;
        this.transitTime = transitTime;
    }

    public TransitTimeRSCHModeTransport() {
    }

    public RschModeTransportOpta getModetransport() {
        return modetransport;
    }

    public void setModetransport(RschModeTransportOpta modetransport) {
        this.modetransport = modetransport;
    }

    public Integer getEbRschUnitScheduleNum() {
        return ebRschUnitScheduleNum;
    }

    public void setEbRschUnitScheduleNum(Integer ebRschUnitScheduleNum) {
        this.ebRschUnitScheduleNum = ebRschUnitScheduleNum;
    }

    public Double getCostTransport() {
        return costTransport;
    }

    public void setCostTransport(Double costTransport) {
        this.costTransport = costTransport;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    @Override
    public String toString() {
        return "\nTransitTimeRSCHModeTransport [modetransport=" + this.modetransport + ", ebRschUnitScheduleNum="
            + this.ebRschUnitScheduleNum + ", costTransport=" + this.costTransport + ", transitTime=" + this.transitTime
            + "]";
    }
}
