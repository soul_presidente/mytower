package com.adias.mytowereasy.rsch.algo.optaPlanner.domain;

import java.util.List;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;


/**
 * This class represent the solution after algo calc
 * Contains input properties with best solutions affected
 */
@PlanningSolution
public class RschAlgoResult {
    private List<RschUnitOptaParamsRoot> containerList;
    private List<RschDockOpenCreneauOpta> creneauQuaisList;

    private HardSoftScore score;

    public RschAlgoResult() {
        super();
    }

    public RschAlgoResult(List<RschUnitOptaParamsRoot> containerList, List<RschDockOpenCreneauOpta> creneauQuaisList) {
        super();
        this.containerList = containerList;
        this.creneauQuaisList = creneauQuaisList;
    }

    public RschAlgoResult(
        List<RschUnitOptaParamsRoot> containerList,
        List<RschDockOpenCreneauOpta> creneauQuaisList,
        List<TransitTimeRSCHModeTransport> modeTransportList) {
        super();
        this.containerList = containerList;
        this.creneauQuaisList = creneauQuaisList;
    }

    @PlanningEntityCollectionProperty
    public List<RschUnitOptaParamsRoot> getContainerList() {
        return containerList;
    }

    public void setContainerList(List<RschUnitOptaParamsRoot> containerList) {
        this.containerList = containerList;
    }

    @ValueRangeProvider(id = "creneauQuaisRange")
    @ProblemFactCollectionProperty
    public List<RschDockOpenCreneauOpta> getCreneauQuaisList() {
        return creneauQuaisList;
    }

    public void setCreneauQuaisList(List<RschDockOpenCreneauOpta> creneauQuaisList) {
        this.creneauQuaisList = creneauQuaisList;
    }

    @PlanningScore
    public HardSoftScore getScore() {
        return score;
    }

    public void setScore(HardSoftScore score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "AffectContainer [containerList=" + containerList + ", creneauQuaisList=" + creneauQuaisList + ", score="
            + score + "]";
    }
}
