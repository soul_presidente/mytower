package com.adias.mytowereasy.rsch.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.dock.repository.EbDkOpeningDayRepository;
import com.adias.mytowereasy.dto.PricingCarrierSearchResultDTO;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbRangeHour;
import com.adias.mytowereasy.model.Enumeration.JourEnum;
import com.adias.mytowereasy.repository.EbRschUnitScheduleRepository;
import com.adias.mytowereasy.rsch.algo.AlgoTest;
import com.adias.mytowereasy.rsch.algo.DockAvailabilityAlgo;
import com.adias.mytowereasy.rsch.algo.DockIntervals;
import com.adias.mytowereasy.rsch.algo.StatiqueAlgo;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschAlgoResult;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschDateArrivee;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschModeTransportOpta;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschUnitOptaParamsRoot;
import com.adias.mytowereasy.rsch.algo.optaPlanner.persistance.AlgorithmeGenerator;
import com.adias.mytowereasy.rsch.model.EbRschUnitSchedule;
import com.adias.mytowereasy.rsch.model.RSCHEnumeration.RschUnitScheduleStatus;
import com.adias.mytowereasy.rsch.model.SearchCriteriaRSCH;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.trpl.model.SearchCriteriaPlanTransport;
import com.adias.mytowereasy.trpl.service.PlanTransportService;


@Service
public class ReceiptSchedulingAlgoServiceImpl extends MyTowerService implements ReceiptSchedulingAlgoService {
    @Autowired
    EbRschUnitScheduleRepository ebRschUnitScheduleRepository;

    @Autowired
    EbDkOpeningDayRepository ebDkOpeningDayRepository;

    @Autowired
    PlanTransportService planTransportService;

    @Override
    public void runCalc() {
        AlgoTest.deleteFile();
        int MAX = 3;
        List<EbRschUnitSchedule> listRschUnitScheduleAvailableToCalc = getRschUnitScheduleAvailableToCalc();
        // annuler le calcul déjà effectué sur les conteneurs selecté
        updateStatusRschUnitScheduleTo_ToPlanify(listRschUnitScheduleAvailableToCalc);

        Set<Integer> listEntrepotNum = getListEntrepotNumIntoRSCHSelected(listRschUnitScheduleAvailableToCalc); // a
                                                                                                                // supprimer
                                                                                                                // faite
                                                                                                                // seulement
                                                                                                                // pour
                                                                                                                // le
                                                                                                                // test

        List<DockIntervals> listDockAvailabilities = new ArrayList<>();
        RschAlgoResult solution = null;
        List<RschModeTransportOpta> listTransitTimeRSCHModeTransport = new ArrayList<RschModeTransportOpta>();
        Set<RschDateArrivee> listRschDateArrivee = new HashSet<RschDateArrivee>();

        Date today = StatiqueAlgo.getDateWithoutTime(new Date());

        for (EbRschUnitSchedule ebRsch: listRschUnitScheduleAvailableToCalc) {

            if (ebRsch.getDemandeTpQuote().getDeliveryTime() != null) {
                List<PricingCarrierSearchResultDTO> carrierSearchResult = getTotalCost(ebRsch);

                if (carrierSearchResult != null && !carrierSearchResult.isEmpty()) {

                    for (PricingCarrierSearchResultDTO resultat: carrierSearchResult) {
                        Integer mt1 = null;
                        Set<Integer> listrschNum = null;

                        if (!listTransitTimeRSCHModeTransport.isEmpty()) {
                            mt1 = StatiqueAlgo
                                .getIndexOfPropTarif(
                                    listTransitTimeRSCHModeTransport,
                                    resultat.getTransitTime(),
                                    ebRsch.getDemandeTp().getxEbEntrepotUnloading().getEbEntrepotNum(),
                                    resultat.getCalculatedPrice(),
                                    resultat.getModeTransport());

                            if (mt1 != null) {
                                listrschNum = listTransitTimeRSCHModeTransport.get(mt1).getListEbRschUnitScheduleNum();
                                listrschNum.add(ebRsch.getEbRschUnitScheduleNum());
                                listTransitTimeRSCHModeTransport.get(mt1).setListEbRschUnitScheduleNum(listrschNum);
                            }

                        }

                        if (listTransitTimeRSCHModeTransport.isEmpty() || mt1 == null) {
                            listrschNum = new HashSet<Integer>();
                            listrschNum.add(ebRsch.getEbRschUnitScheduleNum());

                            RschModeTransportOpta mt = new RschModeTransportOpta(
                                StatiqueAlgo.getMtByCode(resultat.getModeTransport()),
                                resultat.getCalculatedPrice(),
                                resultat.getTransitTime(),
                                listrschNum,
                                ebRsch.getDemandeTp().getxEbEntrepotUnloading().getEbEntrepotNum());

                            listTransitTimeRSCHModeTransport.add(mt);
                        }

                        listRschDateArrivee
                            .add(
                                new RschDateArrivee(
                                    ebRsch.getDemandeTp().getxEbEntrepotUnloading().getEbEntrepotNum(),
                                    StatiqueAlgo.addDaysToDate(today, resultat.getTransitTime()),
                                    resultat.getTransitTime()));
                    }

                }

            }

        }

        listTransitTimeRSCHModeTransport = StatiqueAlgo.getFinalListPropTarif(listTransitTimeRSCHModeTransport);

        // get list dock : la recherche est faite seulement sur 0éme jour apres
        // en passe vers ++
        for (RschDateArrivee jour: listRschDateArrivee) {
            Set<Integer> listEntr = new HashSet<>();
            listEntr.add(jour.getEbEntrepotNum());

            List<RschModeTransportOpta> listPropTarif = StatiqueAlgo
                .getListPropTarifByTTAndEntrepot(
                    listTransitTimeRSCHModeTransport,
                    jour.getTransitTime(),
                    jour.getEbEntrepotNum());

            List<DockIntervals> getListDockAvailabilities = getDockAvailabilities(
                jour.getDateArrivee(),
                listEntr,
                listPropTarif);

            // add list PropTarif
            listDockAvailabilities.addAll(getListDockAvailabilities);
        }

        // a supprimer faite seulement pour le test
        if (listDockAvailabilities.isEmpty()) {
            listDockAvailabilities = getDockAvailabilities(
                StatiqueAlgo.addDaysToDate(StatiqueAlgo.getDateWithoutTime(new Date()), 0),
                listEntrepotNum,
                null);
        }

        System.err.println("containers :: " + listRschUnitScheduleAvailableToCalc);
        System.err.println("entre:: " + listEntrepotNum);
        System.err.println("listDockAvailabilities::" + listDockAvailabilities);
        System.err.println("listTransitTimeRSCHModeTransport::" + listTransitTimeRSCHModeTransport);

        if (listRschUnitScheduleAvailableToCalc != null && listDockAvailabilities != null
            && !listRschUnitScheduleAvailableToCalc.isEmpty() && !listDockAvailabilities.isEmpty()) {
            solution = runOptaplanner(listRschUnitScheduleAvailableToCalc, listDockAvailabilities);
            System.out.println(solution.getContainerList());
            AlgoTest.ecritureDansUnFichier("-_-");
            AlgoTest.ecritureDansUnFichier(solution.getContainerList() + "\n" + "score:" + solution.getScore());

            // save solution if it is correct
            if (StatiqueAlgo.verificationOfSolution(solution.getContainerList())) saveSolution(
                RschUnitScheduleStatus.CALCULATED.getCode(),
                solution.getContainerList());
            else {
                saveSolution(
                    RschUnitScheduleStatus.TO_PLANIFY.getCode(),
                    StatiqueAlgo.actualisationDesCreaneaux(solution.getContainerList()));
                System.err.println("Solution erroné");
            }

        }

    }

    public void saveSolution(Integer status, List<RschUnitOptaParamsRoot> lisContainer) {
        lisContainer.forEach(container -> {
            EbRschUnitSchedule rsch = ebRschUnitScheduleRepository
                .findById(container.getRschUnitSchedule().getEbRschUnitScheduleNum()).get();
            // rsch.setSelected(false);
            rsch.setStatus(status);

            rsch
                .setTotalCost(
                    container.getCreneauQuais() != null ?
                        container.getCreneauQuais().getModeTransportOpta() != null ?
                            container.getCreneauQuais().getModeTransportOpta().getTotalCost() :
                            null :
                        null);

            rsch
                .setDateUnloadingPa(
                    container.getCreneauQuais() != null ?
                        StatiqueAlgo.getDateWithoutTime(container.getCreneauQuais().getInterval().getStart()) :
                        null);

            rsch
                .setDateDeparturePa(
                    container.getCreneauQuais() != null ?
                        StatiqueAlgo
                            .addDaysToDate(
                                container.getCreneauQuais().getInterval().getStart(),
                                -container.getCreneauQuais().getModeTransportOpta().getTransitTime()) :
                        null);// dateArriveePa-tt
            rsch
                .setDateArrivalPa(
                    container.getCreneauQuais() != null ? container.getCreneauQuais().getInterval().getEnd() : null);

            rsch
                .setDock(
                    container.getCreneauQuais() != null ?
                        new EbDock(container.getCreneauQuais().getEbDockNum()) :
                        null);
            rsch
                .setModeTransportPa(
                    container.getCreneauQuais() != null ?
                        container.getCreneauQuais().getModeTransportOpta() != null ?
                            container.getCreneauQuais().getModeTransportOpta().getModeTransport().getCode() :
                            null :
                        null);
            // rsch.set transport ref
            rsch
                .setWorkRangeHour(
                    new EbRangeHour(
                        rsch.getWorkRangeHour() != null ? rsch.getWorkRangeHour().getRangeHoursNum() : null,
                        container.getCreneauQuais() != null ?
                            container.getCreneauQuais().getInterval().getStart() :
                            null,
                        container.getCreneauQuais() != null ?
                            container.getCreneauQuais().getInterval().getEnd() :
                            null));
            ebRschUnitScheduleRepository.save(rsch);
        });
    }

    @Override
    public List<EbRschUnitSchedule> getRschUnitScheduleAvailableToCalc() {
        SearchCriteriaRSCH criteria = new SearchCriteriaRSCH();
        criteria.setSelected(true);
        List<Integer> listStatutFilter = new ArrayList<>();
        listStatutFilter.add(RschUnitScheduleStatus.TO_PLANIFY.getCode());
        listStatutFilter.add(RschUnitScheduleStatus.CALCULATED.getCode());
        criteria.setListStatut(listStatutFilter);

        List<EbRschUnitSchedule> listRS = daoReceiptScheduling.searchRschUnitSchedule(criteria);
        return listRS;
    }

    @Override
    public List<DockIntervals> getDockAvailabilities(
        Date dateToCheck,
        Set<Integer> listEntrepotNum,
        List<RschModeTransportOpta> listPropTarif) {// getListDockCreneauHoraireOccup
                                                    // sur date 0 , 1
        List<DockAvailabilityAlgo> listInitial = ebDkOpeningDayRepository
            .findAllRH(
                JourEnum.getJourEnumByCode(StatiqueAlgo.getDayOfWeek(dateToCheck)),
                dateToCheck,
                listEntrepotNum);
        listInitial.forEach((dockRh) -> {

            try {
                Date start = setDateBy2Dates(dateToCheck, dockRh.getRh().getStartHour());

                Date end = setDateBy2Dates(dateToCheck, dockRh.getRh().getEndHour());
                dockRh.getRh().setStartHour(start);
                dockRh.getRh().setEndHour(end);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        });

        List<DockIntervals> finalListDock = StatiqueAlgo
            .getListIntervalsParDock(getListDockRHWithPriority(listInitial));

        List<DockAvailabilityAlgo> listOccupDock = ebRschUnitScheduleRepository
            .getListDockCreneauHoraireOccup(StatiqueAlgo.getDateWithoutTime(dateToCheck));

        listOccupDock.forEach((dockOccup) -> {

            try {
                Date start = setDateBy2Dates(dateToCheck, dockOccup.getRh().getStartHour());

                Date end = setDateBy2Dates(dateToCheck, dockOccup.getRh().getEndHour());
                dockOccup.getRh().setStartHour(start);
                dockOccup.getRh().setEndHour(end);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        });

        List<DockIntervals> finalListOccupDock = StatiqueAlgo.getListIntervalsParDock(listOccupDock);

        return getListDockHDFinal(finalListDock, finalListOccupDock, listPropTarif);
    }

    @Override
    public RschAlgoResult runOptaplanner(List<EbRschUnitSchedule> listRschUS, List<DockIntervals> dockAvailabilities
    // ,List<TransitTimeRSCHModeTransport>
    // listTransitTimeRSCHModeTransport,List<RschDateDepart> listRschDateDepart
    ) {
        // Build the Solver partie 1
        SolverFactory<RschAlgoResult> solverFactory = SolverFactory
            .createFromXmlResource("rsch.algo.optaPlanner.solver/rschSolverConfig.xml");
        // Load a problem
        RschAlgoResult unsolvedAffectContainer = new AlgorithmeGenerator()
            .createAffectContainer(listRschUS, dockAvailabilities);

        solverFactory
            .getSolverConfig().getTerminationConfig()
            .setSecondsSpentLimit(StatiqueAlgo.getDureeCalculAlgoBySeconds(unsolvedAffectContainer));// paramettrage
                                                                                                     // par
                                                                                                     // nombre
                                                                                                     // de
                                                                                                     // container
                                                                                                     // selected
                                                                                                     // &
                                                                                                     // creneau

        // Build the Solver partie 2
        Solver<RschAlgoResult> solver = solverFactory.buildSolver();

        // Solve the problem
        RschAlgoResult solvedAffectContainer = solver.solve(unsolvedAffectContainer);

        return solvedAffectContainer;
    }

    public Set<Integer> getListEntrepotNumIntoRSCHSelected(List<EbRschUnitSchedule> listRschSelected) {
        Set<Integer> listEntrepotNum = new HashSet<>();
        listRschSelected.forEach(rsch -> {
            if (rsch.getDemandeTp().getxEbEntrepotUnloading() != null
                && rsch.getDemandeTp().getxEbEntrepotUnloading().getEbEntrepotNum() != null) listEntrepotNum
                    .add(rsch.getDemandeTp().getxEbEntrepotUnloading().getEbEntrepotNum());
        });

        return listEntrepotNum;
    }

    public List<DockIntervals> getListDockHDFinal(
        List<DockIntervals> finalList,
        List<DockIntervals> listOccupDock,
        List<RschModeTransportOpta> listPropTarif) {
        finalList.forEach(fl -> {
            fl.setListTransitTimeRSCHModeTransport(listPropTarif);
            listOccupDock.forEach(od -> {

                if (fl.getDock().getEbDockNum().equals(od.getDock().getEbDockNum())) {
                    od.getIntervals().forEach(interval -> {
                        fl.setIntervals(StatiqueAlgo.affecteRhToInterval(fl.getIntervals(), interval));
                    });
                }

            });
        });

        return finalList;
    }

    public List<DockAvailabilityAlgo> getListDockRHWithPriority(List<DockAvailabilityAlgo> list) {
        List<DockAvailabilityAlgo> finalList = new ArrayList<>();

        List<DockAvailabilityAlgo> myList = new ArrayList<>();
        Set<Integer> priority = new HashSet<>();

        for (DockAvailabilityAlgo dockRh: list) {

            if (!myList.isEmpty()
                && myList.get(myList.size() - 1).getDock().getEbDockNum().equals(dockRh.getDock().getEbDockNum())) {
                myList.add(dockRh);
                priority
                    .add(
                        StatiqueAlgo
                            .getPriorityByTypePeriod(dockRh.getOp() != null ? dockRh.getOp().getTypePeriod() : -1));
            }
            else {

                if (!myList.isEmpty()
                    && myList.get(myList.size() - 1).getDock().getEbDockNum().equals(dockRh.getDock().getEbDockNum())) {

                    if (priority.size() >= 2) {

                        for (DockAvailabilityAlgo drh: myList) {
                            if (StatiqueAlgo
                                .getPriorityByTypePeriod(
                                    drh.getOp() != null ? drh.getOp().getTypePeriod() : -1) == Collections.max(priority)
                                && drh.getOpen()) finalList.add(drh);
                        }

                    }
                    else {
                        finalList.addAll(myList);
                    }

                    myList = new ArrayList<>();
                    priority = new HashSet<>();
                    myList.add(dockRh);
                    priority
                        .add(
                            StatiqueAlgo
                                .getPriorityByTypePeriod(dockRh.getOp() != null ? dockRh.getOp().getTypePeriod() : -1));
                }
                else {
                    myList.add(dockRh);
                    priority
                        .add(
                            StatiqueAlgo
                                .getPriorityByTypePeriod(dockRh.getOp() != null ? dockRh.getOp().getTypePeriod() : -1));
                }

            }

        }

        for (DockAvailabilityAlgo drh: myList) {
            if (StatiqueAlgo
                .getPriorityByTypePeriod(drh.getOp() != null ? drh.getOp().getTypePeriod() : -1) == Collections
                    .max(priority)
                && drh.getOpen()) finalList.add(drh);
        }

        myList = null;
        priority = null;
        return finalList;
    }

    @SuppressWarnings("deprecation")
    private Date setDateBy2Dates(Date date1, Date date2) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm")
            .parse(
                (date1.getYear() + 1900) + "-" + (date1.getMonth() + 1) + "-" + date1.getDate() + " " + date2.getHours()
                    + ":" + date2.getMinutes());
    }

    @Override
    public List<PricingCarrierSearchResultDTO> getTotalCost(EbRschUnitSchedule ebRsch) {
        SearchCriteriaPlanTransport searchCriteriaPl = new SearchCriteriaPlanTransport();
        Integer demandeTpNum = ebRsch.getDemandeTp().getEbDemandeNum();
        Integer dgMar = 0;

        if (demandeTpNum != null) {
            EbDemande ebDemandeTp = ebDemandeRepository.findById(demandeTpNum).get();

            if (ebDemandeTp != null) {
                EbDemande ebDemandeDi = ebDemandeRepository
                    .findById(ebDemandeTp.getEbDemandeInitial().getEbDemandeNum()).get();
                searchCriteriaPl.setEbZoneNumOrigin(ebDemandeTp.getEbPartyDest().getZone().getEbZoneNum());
                searchCriteriaPl.setEbZoneNumDest(ebDemandeDi.getEbPartyDest().getZone().getEbZoneNum());

                if (ebDemandeTp.getListMarchandises() != null) {

                    for (int i = 0; i < ebDemandeTp.getListMarchandises().size(); i++) {

                        if (ebDemandeTp.getListMarchandises().get(0).getDangerousGood() != null
                            && dgMar < ebDemandeTp.getListMarchandises().get(0).getDangerousGood()) {
                            dgMar = ebDemandeTp.getListMarchandises().get(0).getDangerousGood();
                        }

                    }

                    if (dgMar > 1) {
                        searchCriteriaPl.setDangerous(true);
                    }
                    else if (dgMar == 1) {
                        searchCriteriaPl.setDangerous(false);
                    }
                    else {
                        searchCriteriaPl.setDangerous(null);
                    }

                }
                else {
                    searchCriteriaPl.setDangerous(null);
                }

                // searchCriteriaPl.setDangerous(false);
                // searchCriteriaPl.setModeTransport(mode.getCode());
                searchCriteriaPl.setTypeDemande(ebDemandeTp.getxEcTypeDemande());

                searchCriteriaPl.setTotalWeight(ebDemandeTp.getTotalTaxableWeight());
                searchCriteriaPl.setTotalUnit(ebDemandeTp.getTotalNbrParcel().intValue());
                searchCriteriaPl.setTotalVolume(ebDemandeTp.getTotalVolume());

                searchCriteriaPl.setCurrencyCibleNum(ebDemandeTp.getXecCurrencyInvoice().getEcCurrencyNum());

                searchCriteriaPl.setModeTransportNonNullOnly(true);
                ;
            }

        }

        List<PricingCarrierSearchResultDTO> liste = planTransportService.searchCarrier(searchCriteriaPl);
        // PricingCarrierSearchResultDTO pricingCarrierSearchResultDTO = null;
        // if (liste.size() > 0) {
        // Double cost = liste.get(0).getCalculatedPrice();
        // pricingCarrierSearchResultDTO = liste.get(0);
        // for (int i = 0; i < liste.size(); i++) {
        // if (liste.get(i).getCalculatedPrice() < cost) {
        // cost = liste.get(i).getCalculatedPrice();
        // pricingCarrierSearchResultDTO = liste.get(i);
        // }
        // }
        // }
        return liste;
    }

    void updateStatusRschUnitScheduleTo_ToPlanify(List<EbRschUnitSchedule> listRschUnitScheduleAvailableToCalc) {
        listRschUnitScheduleAvailableToCalc.forEach(rs -> {
            ebRschUnitScheduleRepository
                .UpdateStatusRschUnitSchedule(
                    RschUnitScheduleStatus.TO_PLANIFY.getCode(),
                    rs.getEbRschUnitScheduleNum());
        });
    }
}
