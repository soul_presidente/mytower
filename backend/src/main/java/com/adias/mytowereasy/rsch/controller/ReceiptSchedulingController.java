package com.adias.mytowereasy.rsch.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.controller.SuperControler;
import com.adias.mytowereasy.rsch.dto.EbRschUnitScheduleDTO;
import com.adias.mytowereasy.rsch.model.EbRschUnitSchedule;
import com.adias.mytowereasy.rsch.model.SearchCriteriaRSCH;


@RestController
@RequestMapping(value = "api/receipt-scheduling")
@CrossOrigin("*")
public class ReceiptSchedulingController extends SuperControler {
    @RequestMapping(value = "list-rschUnit-schedule-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object>
        listRschUnitScheduleTable(@RequestBody(required = false) SearchCriteriaRSCH criteria) {
        List<EbRschUnitSchedule> entities = receiptSchedulingService.searchRschUnitSchedule(criteria);
        List<EbRschUnitScheduleDTO> dtos = receiptSchedulingService.convertEntitiesToDto(entities);
        Long count = receiptSchedulingService.countListEbRschUnitSchedule(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", dtos);
        return result;
    }

    @RequestMapping(value = "list-rschUnit-schedule", method = RequestMethod.POST, produces = "application/json")
    public List<EbRschUnitScheduleDTO>
        listRschUnitSchedule(@RequestBody(required = false) SearchCriteriaRSCH criteria) {
        List<EbRschUnitSchedule> listRschUnitSchedule = receiptSchedulingService.searchRschUnitSchedule(criteria);
        return receiptSchedulingService.convertEntitiesToDto(listRschUnitSchedule);
    }

    @PostMapping(value = "rsch-dashboard-inline-update")
    public EbRschUnitScheduleDTO inlineDashboardUpdate(@RequestBody EbRschUnitScheduleDTO sent) {
        EbRschUnitSchedule entity = receiptSchedulingService.inlineDashboardUpdate(sent);
        return new EbRschUnitScheduleDTO(entity);
    }

    @RequestMapping(value = "count-list-rschUnit-schedule", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Long getCountRschUnitSchedule(@RequestBody(required = false) SearchCriteriaRSCH criteria) {
        Long count = receiptSchedulingService.countListEbRschUnitSchedule(criteria);
        return count;
    }

    @GetMapping(value = "list-unitSchedule-unifiable")
    public List<EbRschUnitScheduleDTO> listUnifiableRschUnitSchedule() {
        List<EbRschUnitSchedule> entities = receiptSchedulingService.listUnifiableRschUnitSchedule();
        return receiptSchedulingService.convertEntitiesToDto(entities);
    }

    @PostMapping(value = "do-unplanify")
    public void doUnplanify() {
        receiptSchedulingService.doUnplanify();
    }

    @PostMapping(value = "do-calculate")
    public void doCalculateRsch() {
        receiptSchedulingService.doCalculate();
    }
}
