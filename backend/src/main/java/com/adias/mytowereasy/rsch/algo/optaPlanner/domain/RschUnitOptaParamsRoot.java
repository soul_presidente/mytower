package com.adias.mytowereasy.rsch.algo.optaPlanner.domain;

import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;


/**
 * This class contains all availabilities and Unit Schedule constraints for algo
 */
@PlanningEntity()
public class RschUnitOptaParamsRoot {
    private RschUnitScheduleOpta rschUnitSchedule;
    private RschDockOpenCreneauOpta creneauQuais;

    public RschUnitOptaParamsRoot() {
        super();
    }

    public RschUnitScheduleOpta getRschUnitSchedule() {
        return rschUnitSchedule;
    }

    public void setRschUnitSchedule(RschUnitScheduleOpta rschUnitSchedule) {
        this.rschUnitSchedule = rschUnitSchedule;
    }

    public RschUnitOptaParamsRoot(
        RschUnitScheduleOpta rschUnitSchedule,
        RschDockOpenCreneauOpta creneauQuais,
        TransitTimeRSCHModeTransport modeTransport) {
        super();
        this.rschUnitSchedule = rschUnitSchedule;
        this.creneauQuais = creneauQuais;
    }

    public RschUnitOptaParamsRoot(RschUnitScheduleOpta rschUnitSchedule, RschDockOpenCreneauOpta creneauQuais) {
        super();
        this.rschUnitSchedule = rschUnitSchedule;
        this.creneauQuais = creneauQuais;
    }

    @PlanningVariable(valueRangeProviderRefs = {
        "creneauQuaisRange"
    })
    public RschDockOpenCreneauOpta getCreneauQuais() {
        return creneauQuais;
    }

    public void setCreneauQuais(RschDockOpenCreneauOpta creneauQuais) {
        this.creneauQuais = creneauQuais;
    }

    @SuppressWarnings("deprecation")
    @Override
    public String toString() {
        return "\nContainer [rschUnitSchedule=" + rschUnitSchedule.getEbRschUnitScheduleNum() + ", dateArriveeTp="
            + rschUnitSchedule.getDateArrivalTp() + ", priority=" + rschUnitSchedule.getPriority() + " wd="
            + rschUnitSchedule.getWorkDuration().getHours() + ":" + rschUnitSchedule.getWorkDuration().getMinutes()
            + ", entrepot=" + rschUnitSchedule.getEntrepotNum() + ",\n creneauQuais=" + creneauQuais + "]\n\n";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((creneauQuais == null) ? 0 : creneauQuais.hashCode());
        result = prime * result + ((rschUnitSchedule == null) ? 0 : rschUnitSchedule.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RschUnitOptaParamsRoot other = (RschUnitOptaParamsRoot) obj;

        if (creneauQuais == null) {
            if (other.creneauQuais != null) return false;
        }
        else if (!creneauQuais.equals(other.creneauQuais)) return false;

        if (rschUnitSchedule == null) {
            if (other.rschUnitSchedule != null) return false;
        }
        else if (!rschUnitSchedule.equals(other.rschUnitSchedule)) return false;

        return true;
    }
}
