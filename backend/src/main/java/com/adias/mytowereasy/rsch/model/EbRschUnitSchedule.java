package com.adias.mytowereasy.rsch.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.dock.model.EbEntrepot;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.model.EbRangeHour;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.trpl.model.EbPlZone;


@Entity
@Table(name = "eb_rsch_unit_schedule", schema = "work")
public class EbRschUnitSchedule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebRschUnitScheduleNum;

    private Integer status;

    // Delivery informations
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_demande_tp", insertable = true)
    private EbDemande demandeTp;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_demande_pa", insertable = true)
    private EbDemande demandePa;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_tt_tracing_tp", insertable = true)
    private EbTtTracing tracingTp;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_tt_tracing_pa", insertable = true)
    private EbTtTracing tracingPa;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_demande_quote", insertable = true)
    private ExEbDemandeTransporteur demandeTpQuote;

    private Integer unitTpIndex;

    private Integer unitPaIndex;
    // End Delivery Informations

    // Algo Parameters & Solution
    private Boolean selected;

    private Integer priority;

    @Temporal(TemporalType.TIME)
    private Date workDuration;

    private Integer modeTransportPa;
    private Boolean modeTransportPaLocked;

    private Date dateDeparturePa;
    private Boolean dateDeparturePaLocked;

    private Date dateArrivalPa;
    private Boolean dateArrivalPaLocked;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_dock", insertable = true)
    private EbDock dock;
    private Boolean dockLocked;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "x_eb_range_hour", insertable = true)
    private EbRangeHour workRangeHour;
    private Boolean workRangeHourLocked;

    private Date dateUnloadingPa;
    private Boolean dateUnloadingPaLocked;

    // END Algo Parameters & Solution

    // Solution result
    private BigDecimal totalCost;
    // END Solution result

    public EbRschUnitSchedule() {
    }

    public Date getDateUnloadingPa() {
        return dateUnloadingPa;
    }

    public void setDateUnloadingPa(Date dateUnloadingPa) {
        this.dateUnloadingPa = dateUnloadingPa;
    }

    public Integer getEbRschUnitScheduleNum() {
        return ebRschUnitScheduleNum;
    }

    public void setEbRschUnitScheduleNum(Integer ebRschUnitScheduleNum) {
        this.ebRschUnitScheduleNum = ebRschUnitScheduleNum;
    }

    public EbRschUnitSchedule(Integer ebRschUnitScheduleNum, Date workDuration, Date dateArrival) {
        super();
        this.ebRschUnitScheduleNum = ebRschUnitScheduleNum;
        this.workDuration = workDuration;
        this.dateArrivalPa = dateArrival;
    }

    /**
     * QueryDSL constructor for dashboard table<br/>
     * <b>!PLEASE KEEP PARAMETERS NAME WITH SENSE!</b>
     */
    @QueryProjection
    public EbRschUnitSchedule(
        Integer ebRschUnitScheduleNum,

        Integer demandeTpNum,
        String demandeTpRefTransport,
        String demandeTpLibelleOriginCountry,
        String demandeTpLibelleDestCountry,
        Date demandeTpDateArrival,
        Integer demandeTpModeTransport,
        Double demandeTpTotalVolume,
        Double demandeTpTotalWeight,
        Integer demandeTpPartyDestNum,
        Integer demandeTpPartyDestZoneNum,
        String demandeTpPartyDestZoneRef,
        Integer demandeTpUnloadingEntrepotNum,
        String demandeTpUnloadingEntrepotReference,
        String demandeTpUnloadingEntrepotLibelle,
        Integer demandeTpXEcCurrencyInvoiceNum,
        String demandeTpXEcCurrencyInvoiceCode,
        String demandeTpXEcCurrencyInvoiceSymbol,
        Integer demandeTpQuoteNum,
        Date demandeTpQuoteDeliveryTime,
        Integer tracingTpNum,
        String tracingTpCustomerReference,
        Integer tracingTpMarchandiseNum,
        Integer demandeInitialNum,
        Integer demandeInitialPartyDestNum,
        Integer demandeInitialPartyDestZoneNum,
        String demandeInitialPartyDestZoneRef,

        Integer usUnitTpIndex,
        Integer usUnitPaIndex,
        Integer usPriority,
        Date usWorkDuration,
        BigDecimal usTotalCost,
        Integer usDockNum,
        String usDockName,
        EbRangeHour usWorkRangeHour,
        Integer usStatus,
        Boolean usSelected,
        Integer usModeTransportPa,
        Boolean usModeTransportPaLocked,
        Date usDateDeparturePa,
        Boolean usDateDeparturePaLocked,
        Date usDateArrivalPa,
        Boolean usDateArrivalPaLocked,
        Boolean usDockLocked,
        Boolean usWorkRangeHourLocked,

        Date dateUnloadingPa,
        Boolean dateUnloadingLocked,

        Integer demandePaNum,
        String demandePaRefTransport,
        Integer demandePaModeTransport,
        Integer tracingPaNum) {
        this.ebRschUnitScheduleNum = ebRschUnitScheduleNum;
        this.demandeTp = new EbDemande(demandeTpNum);
        this.demandeTp.setRefTransport(demandeTpRefTransport);
        this.demandeTp.setLibelleOriginCountry(demandeTpLibelleOriginCountry);
        this.demandeTp.setLibelleDestCountry(demandeTpLibelleDestCountry);
        this.demandeTp.setDateOfArrival(demandeTpDateArrival);
        this.demandeTp.setxEcModeTransport(demandeTpModeTransport);
        this.demandeTp.setTotalVolume(demandeTpTotalVolume);
        this.demandeTp.setTotalWeight(demandeTpTotalWeight);
        this.demandeTp
            .setXecCurrencyInvoice(
                new EcCurrency(
                    demandeTpXEcCurrencyInvoiceNum,
                    null,
                    null,
                    demandeTpXEcCurrencyInvoiceSymbol,
                    demandeTpXEcCurrencyInvoiceCode));

        if (demandeTpPartyDestNum != null) {
            EbParty partyDest = new EbParty();
            partyDest.setEbPartyNum(demandeTpPartyDestNum);
            this.demandeTp.setEbPartyDest(partyDest);

            if (demandeTpPartyDestZoneNum != null) {
                this.demandeTp.getEbPartyDest().setZone(new EbPlZone(demandeTpPartyDestZoneNum));
                this.demandeTp.getEbPartyDest().getZone().setRef(demandeTpPartyDestZoneRef);
            }

        }

        if (demandeTpUnloadingEntrepotNum != null) {
            EbEntrepot ent = new EbEntrepot();
            ent.setEbEntrepotNum(demandeTpUnloadingEntrepotNum);
            ent.setReference(demandeTpUnloadingEntrepotReference);
            ent.setLibelle(demandeTpUnloadingEntrepotLibelle);
            this.demandeTp.setxEbEntrepotUnloading(ent);
        }

        if (demandePaNum != null) {
            this.demandePa = new EbDemande(demandePaNum);
            this.demandePa.setRefTransport(demandePaRefTransport);
            this.demandePa.setxEcModeTransport(demandePaModeTransport);
        }

        if (demandeInitialNum != null) {
            EbDemande demandeInitial = new EbDemande();
            demandeInitial.setEbDemandeNum(demandeInitialNum);
            this.demandeTp.setEbDemandeInitial(demandeInitial);

            if (demandeInitialPartyDestNum != null) {
                EbParty partyDest = new EbParty();
                partyDest.setEbPartyNum(demandeInitialPartyDestNum);
                this.demandeTp.getEbDemandeInitial().setEbPartyDest(partyDest);

                if (demandeInitialPartyDestZoneNum != null) {
                    this.demandeTp
                        .getEbDemandeInitial().getEbPartyDest().setZone(new EbPlZone(demandeInitialPartyDestZoneNum));
                    this.demandeTp
                        .getEbDemandeInitial().getEbPartyDest().getZone().setRef(demandeInitialPartyDestZoneRef);
                }

            }

        }

        this.tracingTp = new EbTtTracing(tracingTpNum);
        this.tracingTp.setCustomerReference(tracingTpCustomerReference);

        if (tracingTpMarchandiseNum != null) {
            EbMarchandise ebmarch = new EbMarchandise();
            ebmarch.setEbMarchandiseNum(tracingTpMarchandiseNum);
            this.tracingTp.setxEbMarchandise(ebmarch);
        }

        this.tracingPa = new EbTtTracing(tracingPaNum);
        this.unitTpIndex = usUnitTpIndex;
        this.unitPaIndex = usUnitPaIndex;
        this.priority = usPriority;
        this.workDuration = usWorkDuration;
        this.totalCost = usTotalCost;

        if (usDockNum != null) {
            this.dock = new EbDock(usDockNum);
            this.dock.setNom(usDockName);
        }

        this.workRangeHour = usWorkRangeHour;
        this.status = usStatus;
        this.selected = usSelected;
        this.modeTransportPa = usModeTransportPa;
        this.modeTransportPaLocked = usModeTransportPaLocked;
        this.dateDeparturePa = usDateDeparturePa;
        this.dateDeparturePaLocked = usDateDeparturePaLocked;
        this.dateArrivalPa = usDateArrivalPa;
        this.dateArrivalPaLocked = usDateArrivalPaLocked;
        this.dockLocked = usDockLocked;
        this.workRangeHourLocked = usWorkRangeHourLocked;

        if (demandeTpQuoteNum != null) {
            this.demandeTpQuote = new ExEbDemandeTransporteur();
            this.demandeTpQuote.setExEbDemandeTransporteurNum(demandeTpQuoteNum);
            this.demandeTpQuote.setDeliveryTime(demandeTpQuoteDeliveryTime);
        }

        this.dateUnloadingPa = dateUnloadingPa;
        this.dateUnloadingPaLocked = dateUnloadingLocked;
    }

    public EbDemande getDemandeTp() {
        return demandeTp;
    }

    public void setDemandeTp(EbDemande demandeTp) {
        this.demandeTp = demandeTp;
    }

    public EbDemande getDemandePa() {
        return demandePa;
    }

    public void setDemandePa(EbDemande demandePa) {
        this.demandePa = demandePa;
    }

    public EbTtTracing getTracingTp() {
        return tracingTp;
    }

    public void setTracingTp(EbTtTracing tracingTp) {
        this.tracingTp = tracingTp;
    }

    public EbTtTracing getTracingPa() {
        return tracingPa;
    }

    public void setTracingPa(EbTtTracing tracingPa) {
        this.tracingPa = tracingPa;
    }

    public Integer getUnitTpIndex() {
        return unitTpIndex;
    }

    public void setUnitTpIndex(Integer unitTpIndex) {
        this.unitTpIndex = unitTpIndex;
    }

    public Integer getUnitPaIndex() {
        return unitPaIndex;
    }

    public void setUnitPaIndex(Integer unitPaIndex) {
        this.unitPaIndex = unitPaIndex;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Date getWorkDuration() {
        return workDuration;
    }

    public void setWorkDuration(Date workDuration) {
        this.workDuration = workDuration;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public EbDock getDock() {
        return dock;
    }

    public void setDock(EbDock dock) {
        this.dock = dock;
    }

    public EbRangeHour getWorkRangeHour() {
        return workRangeHour;
    }

    public void setWorkRangeHour(EbRangeHour workRangeHour) {
        this.workRangeHour = workRangeHour;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Integer getModeTransportPa() {
        return modeTransportPa;
    }

    public void setModeTransportPa(Integer modeTransportPa) {
        this.modeTransportPa = modeTransportPa;
    }

    public Boolean getModeTransportPaLocked() {
        return modeTransportPaLocked;
    }

    public void setModeTransportPaLocked(Boolean modeTransportPaLocked) {
        this.modeTransportPaLocked = modeTransportPaLocked;
    }

    public Date getDateDeparturePa() {
        return dateDeparturePa;
    }

    public void setDateDeparturePa(Date dateDeparturePa) {
        this.dateDeparturePa = dateDeparturePa;
    }

    public Boolean getDateDeparturePaLocked() {
        return dateDeparturePaLocked;
    }

    public void setDateDeparturePaLocked(Boolean dateDeparturePaLocked) {
        this.dateDeparturePaLocked = dateDeparturePaLocked;
    }

    public Date getDateArrivalPa() {
        return dateArrivalPa;
    }

    public void setDateArrivalPa(Date dateArrivalPa) {
        this.dateArrivalPa = dateArrivalPa;
    }

    public Boolean getDateArrivalPaLocked() {
        return dateArrivalPaLocked;
    }

    public void setDateArrivalPaLocked(Boolean dateArrivalPaLocked) {
        this.dateArrivalPaLocked = dateArrivalPaLocked;
    }

    public Boolean getDockLocked() {
        return dockLocked;
    }

    public void setDockLocked(Boolean dockLocked) {
        this.dockLocked = dockLocked;
    }

    public Boolean getWorkRangeHourLocked() {
        return workRangeHourLocked;
    }

    public void setWorkRangeHourLocked(Boolean workRangeHourLocked) {
        this.workRangeHourLocked = workRangeHourLocked;
    }

    public ExEbDemandeTransporteur getDemandeTpQuote() {
        return demandeTpQuote;
    }

    public void setDemandeTpQuote(ExEbDemandeTransporteur demandeTpQuote) {
        this.demandeTpQuote = demandeTpQuote;
    }

    public Boolean getDateUnloadingPaLocked() {
        return dateUnloadingPaLocked;
    }

    public void setDateUnloadingPaLocked(Boolean dateUnloadingPaLocked) {
        this.dateUnloadingPaLocked = dateUnloadingPaLocked;
    }
}
