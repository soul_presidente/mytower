package com.adias.mytowereasy.rsch.algo.optaPlanner.score;

import org.optaplanner.core.api.score.Score;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator;

import com.adias.mytowereasy.rsch.algo.AlgoTest;
import com.adias.mytowereasy.rsch.algo.Interval;
import com.adias.mytowereasy.rsch.algo.StatiqueAlgo;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschAlgoResult;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschUnitOptaParamsRoot;


public class AlgorithmeScoreCalculator implements EasyScoreCalculator<RschAlgoResult> {
    @Override
    public Score calculateScore(RschAlgoResult affectation) {
        int hardScore = 0;
        int softScore = 0;

        Integer workDuration = null;
        Integer nbrContraintValide = 0;

        for (RschUnitOptaParamsRoot container: affectation.getContainerList()) {
            workDuration = StatiqueAlgo.getTimeMinute(container.getRschUnitSchedule().getWorkDuration());

            if (container.getCreneauQuais() != null
                && container
                    .getRschUnitSchedule().getEntrepotNum().equals(container.getCreneauQuais().getEbEntrepotNum())
                && StatiqueAlgo.getDureeIntervalMinute(container.getCreneauQuais().getInterval()).equals(workDuration)
                && container.getCreneauQuais().getModeTransportOpta() != null
                && container
                    .getCreneauQuais().getModeTransportOpta().getListEbRschUnitScheduleNum()
                    .contains(container.getRschUnitSchedule().getEbRschUnitScheduleNum())
                && container
                    .getCreneauQuais().getStartDispo()
                    .compareTo(container.getCreneauQuais().getInterval().getStart()) <= 0
                && container
                    .getRschUnitSchedule().getDateArrivalTp()
                    .compareTo(container.getCreneauQuais().getInterval().getStart()) <= 0) {
                // le plus tot possible
                hardScore -= (StatiqueAlgo
                    .getDureeIntervalMinute(
                        new Interval(
                            container.getCreneauQuais().getInterval().getStart(),
                            container.getCreneauQuais().getStartDispo())));

                nbrContraintValide++;

                // ------------------------------lock
                // constraints------------------------------------

                // dock locked
                if (container.getRschUnitSchedule().getDockLocked() && !container
                    .getRschUnitSchedule().getDock().getEbDockNum()
                    .equals(container.getCreneauQuais().getEbDockNum())) {
                    softScore -= 400;
                    hardScore -= 400;
                }

                // dateDepartPa locked
                if (container.getRschUnitSchedule().getDateDeparturePaLocked() && StatiqueAlgo
                    .getDateWithoutTime(container.getRschUnitSchedule().getDateDeparturePa()).compareTo(
                        StatiqueAlgo
                            .addDaysToDate(
                                StatiqueAlgo.getDateWithoutTime(container.getCreneauQuais().getInterval().getEnd()),
                                -container.getCreneauQuais().getModeTransportOpta().getTransitTime())) != 0) {
                    softScore -= 400;
                    hardScore -= 400;
                }

                // dateArriveePa locked
                if (container.getRschUnitSchedule().getDateArrivalPaLocked() && StatiqueAlgo
                    .getDateWithoutTime(container.getRschUnitSchedule().getDateArrivalPa()).compareTo(
                        StatiqueAlgo.getDateWithoutTime(container.getCreneauQuais().getInterval().getEnd())) != 0) {
                    softScore -= 400;
                    hardScore -= 400;
                }

                // dateUnloadingPa locked
                if (container.getRschUnitSchedule().getDateUnloadingPaLocked() && StatiqueAlgo
                    .getDateWithoutTime(container.getRschUnitSchedule().getDateUnloadingPa()).compareTo(
                        StatiqueAlgo
                            .getDateWithoutTime(
                                StatiqueAlgo
                                    .getDateWithoutTime(container.getCreneauQuais().getInterval().getEnd()))) != 0) {
                    softScore -= 400;
                    hardScore -= 400;
                }

                // mode de transport locked
                if (container.getRschUnitSchedule().getModeTransportPaLocked()) {

                    if (container.getCreneauQuais().getModeTransportOpta() != null) {

                        if (!container
                            .getRschUnitSchedule().getModeTransportPa()
                            .equals(container.getCreneauQuais().getModeTransportOpta().getModeTransport().getCode())) {
                            softScore -= 400;
                            hardScore -= 400;
                        }

                    }
                    else {
                        softScore -= 600;
                        hardScore -= 600;
                    }

                }

                // range hour locked
                if (container.getRschUnitSchedule().getWorkRangeHourLocked() && !StatiqueAlgo
                    .CompartRangeHourToIntervale(
                        container.getRschUnitSchedule().getWorkRangeHour(),
                        container.getCreneauQuais().getInterval())) {
                    softScore -= 400;
                    hardScore -= 400;
                }

                // minimisation les TT && les totalCost
                hardScore -= container.getCreneauQuais().getModeTransportOpta().getTransitTime() * 100;
                hardScore -= container.getCreneauQuais().getModeTransportOpta().getTotalCost().intValue(); // j'ai
                                                                                                           // changé le
                                                                                                           // type de
                                                                                                           // totalCost
                                                                                                           // de
                                                                                                           // "double"
                                                                                                           // à
                                                                                                           // "BigDecimal"
            }

        }

        Boolean isAllAffected = StatiqueAlgo.isAllAffected(affectation.getContainerList());
        Boolean isOverlap = (isAllAffected == true ?
            StatiqueAlgo.isOverlapIntoSolution(affectation.getContainerList()) :
            false);
        Boolean isPriorityRespected = StatiqueAlgo.isPriorityRespected(affectation.getContainerList());

        // tous les affect valide / entrepot && cas de chevauchement or priority
        // not respected
        if (!nbrContraintValide.equals(affectation.getContainerList().size())) {
            hardScore -= 10000;
            softScore -= 10000;
        }

        if (isOverlap || !isPriorityRespected) {
            hardScore -= 1000;
            softScore -= 1000;
        }

        AlgoTest.ecritureDansUnFichier("\n------------------------------------------debut----------------------");
        AlgoTest
            .ecritureDansUnFichier(
                "-_- isAllAffected:" + isAllAffected + " isOverlap:" + isOverlap + " isPriorityRespected:"
                    + isPriorityRespected);
        AlgoTest
            .ecritureDansUnFichier(
                "\n-*" + nbrContraintValide + " " + affectation.getContainerList().size() + "->"
                    + affectation.getContainerList() + " score:" + HardSoftScore.valueOf(hardScore, softScore));
        AlgoTest.ecritureDansUnFichier("\n------------------------------------------fin----------------------");
        return HardSoftScore.valueOf(hardScore, softScore);
    }
}
