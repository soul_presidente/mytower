package com.adias.mytowereasy.rsch.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.rsch.dto.EbRschUnitScheduleDTO;
import com.adias.mytowereasy.rsch.model.EbRschUnitSchedule;
import com.adias.mytowereasy.rsch.model.RSCHEnumeration.RschUnitScheduleStatus;
import com.adias.mytowereasy.rsch.model.SearchCriteriaRSCH;
import com.adias.mytowereasy.service.MyTowerService;


@Service
public class ReceiptSchedulingServiceImpl extends MyTowerService implements ReceiptSchedulingService {
    @Autowired
    ReceiptSchedulingAlgoService rschAlgoService;

    @Override
    public void insertRschUnitSchedule(EbDemande ebDemande, List<EbTtTracing> ebTtTracings) {

        if (ebTtTracings != null && ebDemande != null) {

            for (EbTtTracing ebTtTracing: ebTtTracings) {
                EbMarchandise ebMarchandise = ebMarchandiseRepository
                    .findById(ebTtTracing.getxEbMarchandise().getEbMarchandiseNum()).get();
                Integer unitNumber = ebMarchandise.getNumberOfUnits();

                for (int i = 1; i <= unitNumber; i++) {
                    EbRschUnitSchedule ebRschUnitSchedule = new EbRschUnitSchedule();
                    ebRschUnitSchedule.setDemandeTp(ebDemande);
                    ebRschUnitSchedule.setTracingTp(ebTtTracing);
                    ebRschUnitSchedule.setUnitTpIndex(i);
                    ebRschUnitSchedule.setPriority(2);
                    ebRschUnitSchedule.setDateArrivalPaLocked(false);
                    ebRschUnitSchedule.setDateDeparturePaLocked(false);
                    ebRschUnitSchedule.setDockLocked(false);
                    ebRschUnitSchedule.setModeTransportPaLocked(false);
                    ebRschUnitSchedule.setWorkRangeHourLocked(false);
                    ebRschUnitSchedule.setStatus(RschUnitScheduleStatus.TO_PLANIFY.getCode());
                    ebRschUnitSchedule.setWorkRangeHour(new EbRangeHour());
                    ebRschUnitSchedule.setSelected(false);

                    if (ebDemande.getExEbDemandeTransporteurs() != null) {

                        for (ExEbDemandeTransporteur dtr: ebDemande.getExEbDemandeTransporteurs()) {

                            if (Enumeration.StatutCarrier.FIN.getCode().equals(dtr.getStatus())) {
                                ebRschUnitSchedule.setDemandeTpQuote(dtr);
                            }

                        }

                    }

                    ebRschUnitScheduleRepository.save(ebRschUnitSchedule);
                }

            }

        }

    }

    @Override
    public List<EbRschUnitSchedule> searchRschUnitSchedule(SearchCriteriaRSCH criteria) {
        List<EbRschUnitSchedule> listRS = daoReceiptScheduling.searchRschUnitSchedule(criteria);

        listRS.forEach(rs -> {

            if (rs.getWorkRangeHour() == null) {
                rs.setWorkRangeHour(new EbRangeHour());
            }

        });

        return listRS;
    }

    @Override
    public List<EbRschUnitSchedule> listUnifiableRschUnitSchedule() {
        SearchCriteriaRSCH criteria = new SearchCriteriaRSCH();
        criteria.setSelected(true);
        List<Integer> listStatutFilter = new ArrayList<>();
        listStatutFilter.add(RschUnitScheduleStatus.CALCULATED.getCode());
        criteria.setListStatut(listStatutFilter);

        List<EbRschUnitSchedule> listPlanifiables = daoReceiptScheduling.searchRschUnitSchedule(criteria);
        List<EbRschUnitSchedule> listUnifiables = new ArrayList<>();

        // Check for unifiables
        for (EbRschUnitSchedule usPlani1: listPlanifiables) {

            for (EbRschUnitSchedule usPlani2: listPlanifiables) {
                boolean meetUnifiable = checkUsUnifiables(usPlani1, usPlani2);

                if (meetUnifiable && !usPlani1.getEbRschUnitScheduleNum().equals(usPlani2.getEbRschUnitScheduleNum())) {
                    listUnifiables.add(usPlani1);
                    listUnifiables.add(usPlani2);
                }

            }

        }

        // Remove duplicates
        listUnifiables = listUnifiables.stream().distinct().collect(Collectors.toList());

        // Sort by groups
        listUnifiables.sort((EbRschUnitSchedule us1, EbRschUnitSchedule us2) -> {
            return checkUsUnifiables(us1, us2) ? 1 : -1;
        });

        return listUnifiables;
    }

    private boolean checkUsUnifiables(EbRschUnitSchedule us1, EbRschUnitSchedule us2) {
        return ObjectUtils.equals(us1.getDateDeparturePa(), us2.getDateDeparturePa())
            && ObjectUtils.equals(us1.getDateArrivalPa(), us2.getDateArrivalPa())
            && ObjectUtils
                .equals(
                    us1.getDemandeTp().getxEbEntrepotUnloading().getEbEntrepotNum(),
                    us2.getDemandeTp().getxEbEntrepotUnloading().getEbEntrepotNum())
            && ObjectUtils
                .equals(
                    us1.getDemandeTp().getEbPartyDest().getZone().getEbZoneNum(),
                    us2.getDemandeTp().getEbPartyDest().getZone().getEbZoneNum())
            && ObjectUtils
                .equals(
                    us1.getDemandeTp().getEbDemandeInitial().getEbPartyDest().getZone().getEbZoneNum(),
                    us2.getDemandeTp().getEbDemandeInitial().getEbPartyDest().getZone().getEbZoneNum());
    }

    @Override
    public void doUnplanify() {
        SearchCriteriaRSCH criteria = new SearchCriteriaRSCH();
        criteria.setSelected(true);
        List<Integer> listStatutFilter = new ArrayList<>();
        listStatutFilter.add(RschUnitScheduleStatus.PLANIFIED.getCode());
        listStatutFilter.add(RschUnitScheduleStatus.CALCULATED.getCode());
        criteria.setListStatut(listStatutFilter);

        List<EbRschUnitSchedule> listRS = daoReceiptScheduling.searchRschUnitSchedule(criteria);

        listRS.forEach(rs -> {
            ebRschUnitScheduleRepository
                .UpdateStatusRschUnitSchedule(
                    RschUnitScheduleStatus.TO_PLANIFY.getCode(),
                    rs.getEbRschUnitScheduleNum());
        });
    }

    @Override
    public void doCalculate() {
        rschAlgoService.runCalc();
    }

    @Override
    public List<EbRschUnitScheduleDTO> convertEntitiesToDto(List<EbRschUnitSchedule> entities) {
        List<EbRschUnitScheduleDTO> listDto = new ArrayList<>();
        entities.forEach(e -> listDto.add(new EbRschUnitScheduleDTO(e)));
        return listDto;
    }

    @Override
    public Long countListEbRschUnitSchedule(SearchCriteriaRSCH criteria) {
        return daoReceiptScheduling.countListEbRschUnitSchedule(criteria);
    }

    @Override
    public EbRschUnitSchedule inlineDashboardUpdate(EbRschUnitScheduleDTO sent) {
        EbRschUnitSchedule entity = ebRschUnitScheduleRepository.findById(sent.getEbRschUnitScheduleNum()).get();

        // Update field by field for security reason
        entity.setPriority(sent.getPriority());
        entity.setWorkDuration(sent.getWorkDuration());
        entity.setModeTransportPa(sent.getModeTransportPa());
        entity.setModeTransportPaLocked(sent.getModeTransportPaLocked());
        entity.setDateArrivalPa(sent.getDateArrivalPa());
        entity.setDateArrivalPaLocked(sent.getDateArrivalPaLocked());
        entity.setDateDeparturePa(sent.getDateDeparturePa());
        entity.setDateDeparturePaLocked(sent.getDateDeparturePaLocked());
        entity.setDock(sent.getDockNum() == null ? null : new EbDock(sent.getDockNum()));
        entity.setDockLocked(sent.getDockLocked());
        entity.setWorkRangeHourLocked(sent.getWorkRangeHourLocked());
        entity.setDateUnloadingPa(sent.getDateUnloadingPa());
        entity.setDateUnloadingPaLocked(sent.getDateUnloadingPaLocked());

        if (entity.getWorkRangeHour() != null) {
            entity.getWorkRangeHour().setStartHour(sent.getWorkRangeHour().getStartHour());
            entity.getWorkRangeHour().setEndHour(sent.getWorkRangeHour().getEndHour());
        }
        else {
            entity.setWorkRangeHour(new EbRangeHour());
            entity.getWorkRangeHour().setStartHour(sent.getWorkRangeHour().getStartHour());
            entity.getWorkRangeHour().setEndHour(sent.getWorkRangeHour().getEndHour());
        }

        entity.setSelected(sent.getSelected() != null ? sent.getSelected() : false);

        return ebRschUnitScheduleRepository.save(entity);
    }
}
