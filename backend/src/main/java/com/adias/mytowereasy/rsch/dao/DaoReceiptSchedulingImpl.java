package com.adias.mytowereasy.rsch.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dock.model.QEbDock;
import com.adias.mytowereasy.dock.model.QEbEntrepot;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.tt.QEbTtTracing;
import com.adias.mytowereasy.rsch.model.EbRschUnitSchedule;
import com.adias.mytowereasy.rsch.model.QEbRschUnitSchedule;
import com.adias.mytowereasy.rsch.model.SearchCriteriaRSCH;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.trpl.model.EbPlTransport;
import com.adias.mytowereasy.trpl.model.QEbPlZone;


@Component
@Transactional
public class DaoReceiptSchedulingImpl implements DaoReceiptScheduling {
    @PersistenceContext
    EntityManager em;
    @Autowired
    ConnectedUserService connectedUserService;

    QEbRschUnitSchedule qEbRschUnitSchedule = QEbRschUnitSchedule.ebRschUnitSchedule;
    QEbParty qEbPartyDestTp = new QEbParty("qEbPartyDestTp");
    QEbParty qEbPartyOriginTp = new QEbParty("qEbPartyOriginTp");
    QEbDemande qDemandeTp = new QEbDemande("qDemandeTp");
    QEbDemande qDemandePa = new QEbDemande("qDemandePa");
    QEbDemande qDemandeInitial = new QEbDemande("qDemandeInitial");
    QEbTtTracing qTracingTp = new QEbTtTracing("qTracingTp");
    QEbTtTracing qTracingPa = new QEbTtTracing("qTracingPa");
    QEbDock qEbDock = QEbDock.ebDock;
    QEbRangeHour qEbRangeHour = QEbRangeHour.ebRangeHour;
    QEbEntrepot qEbEntrepot = QEbEntrepot.ebEntrepot;
    QEbUser qBrokerTp = new QEbUser("qBrokerTp");
    QEbEtablissement qEbPartyDestTpEtab = new QEbEtablissement("qEbPartyDestTpEtab");
    QEbEtablissement qEbPartyOriginTpEtab = new QEbEtablissement("qEbPartyOriginTpEtab");
    QEbEtablissement qEbEtabChargeurTp = new QEbEtablissement("qEbEtabChargeurTp");
    QEbEtablissement qEbEtabCtTp = new QEbEtablissement("qEbEtabCtTp");
    QEbEtablissement qEbEtabTranspTp = new QEbEtablissement("qEbEtabTranspTp");
    QEbEtablissement qExEbDemandeTranspEtabTp = new QEbEtablissement("qExEbDemandeTranspEtabTp");
    QExEbDemandeTransporteur qExEbDemandeTranspTp = new QExEbDemandeTransporteur("qExEbDemandeTranspTp");
    QExEbDemandeTransporteur qDemandeQuoteTp = new QExEbDemandeTransporteur("qDemandeQuoteTp");
    QEbCompagnie qEbCompagnieChargeurTp = new QEbCompagnie("qEbCompagnieChargeurTp");
    QEbPlZone qZoneIntermediate = new QEbPlZone("qZoneIntermediate");
    QEbPlZone qZoneDest = new QEbPlZone("qZoneDest");
    QEcCurrency qEcCurrency = new QEcCurrency("qEcCurrency");

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    @Override
    public List<EbRschUnitSchedule> searchRschUnitSchedule(SearchCriteriaRSCH criteria) {
        List<EbRschUnitSchedule> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbRschUnitSchedule> query = new JPAQuery<>(em);

            query
                .select(
                    QEbRschUnitSchedule
                        .create(
                            qEbRschUnitSchedule.ebRschUnitScheduleNum,
                            qDemandeTp.ebDemandeNum,
                            qDemandeTp.refTransport,
                            qDemandeTp.libelleOriginCountry,
                            qDemandeTp.libelleDestCountry,
                            qDemandeTp.dateOfArrival,
                            qDemandeTp.xEcModeTransport,
                            qDemandeTp.totalVolume,
                            qDemandeTp.totalWeight,
                            qEbPartyDestTp.ebPartyNum,
                            qZoneIntermediate.ebZoneNum,
                            qZoneIntermediate.ref,
                            qEbEntrepot.ebEntrepotNum,
                            qEbEntrepot.reference,
                            qEbEntrepot.libelle,
                            qEcCurrency.ecCurrencyNum,
                            qEcCurrency.code,
                            qEcCurrency.symbol,
                            qDemandeQuoteTp.exEbDemandeTransporteurNum,
                            qDemandeQuoteTp.deliveryTime,
                            qTracingTp.ebTtTracingNum,
                            qTracingTp.customerReference,
                            qTracingTp.xEbMarchandise().ebMarchandiseNum,
                            qDemandeInitial.ebDemandeNum,
                            qDemandeInitial.ebPartyDest().ebPartyNum,
                            qZoneDest.ebZoneNum,
                            qZoneDest.ref,

                            qEbRschUnitSchedule.unitTpIndex,
                            qEbRschUnitSchedule.unitPaIndex,
                            qEbRschUnitSchedule.priority,
                            qEbRschUnitSchedule.workDuration,
                            qEbRschUnitSchedule.totalCost,
                            qEbDock.ebDockNum,
                            qEbDock.nom,
                            qEbRangeHour,
                            qEbRschUnitSchedule.status,
                            qEbRschUnitSchedule.selected,
                            qEbRschUnitSchedule.modeTransportPa,
                            qEbRschUnitSchedule.modeTransportPaLocked,
                            qEbRschUnitSchedule.dateDeparturePa,
                            qEbRschUnitSchedule.dateDeparturePaLocked,
                            qEbRschUnitSchedule.dateArrivalPa,
                            qEbRschUnitSchedule.dateArrivalPaLocked,
                            qEbRschUnitSchedule.dockLocked,
                            qEbRschUnitSchedule.workRangeHourLocked,

                            qEbRschUnitSchedule.dateUnloadingPa,
                            qEbRschUnitSchedule.dateUnloadingPaLocked,

                            qDemandePa.ebDemandeNum,
                            qDemandePa.refTransport,
                            qDemandePa.xEcModeTransport,
                            qTracingPa.ebTtTracingNum));
            query = getRschUnitScheduleGlobalWhere(query, where, criteria);
            query.distinct().from(qEbRschUnitSchedule);
            query = getRschUnitScheduleGlobalJoin(query, where, criteria);
            query.where(where);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions
                    .path(Object.class, qEbRschUnitSchedule, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbRschUnitSchedule.ebRschUnitScheduleNum.asc());
            }

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long countListEbRschUnitSchedule(SearchCriteriaRSCH criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbPlTransport> query = new JPAQuery<EbPlTransport>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getRschUnitScheduleGlobalWhere(query, where, criteria);
            query.from(qEbRschUnitSchedule);
            query = getRschUnitScheduleGlobalJoin(query, where, criteria);
            query.distinct().where(where);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    private JPAQuery getRschUnitScheduleGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteriaRSCH criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);

        query.leftJoin(qEbRschUnitSchedule.demandeTp(), qDemandeTp);
        query.leftJoin(qDemandeTp.ebPartyDest(), qEbPartyDestTp);
        query.leftJoin(qDemandeTp.ebPartyOrigin(), qEbPartyOriginTp);
        query.leftJoin(qDemandeTp.xEbEntrepotUnloading(), qEbEntrepot);
        query.leftJoin(qDemandeTp.ebDemandeInitial(), qDemandeInitial);
        query.leftJoin(qEbRschUnitSchedule.demandePa(), qDemandePa);
        query.leftJoin(qEbRschUnitSchedule.tracingTp(), qTracingTp);
        query.leftJoin(qEbRschUnitSchedule.tracingPa(), qTracingPa);
        query.leftJoin(qEbRschUnitSchedule.workRangeHour(), qEbRangeHour);
        query.leftJoin(qEbRschUnitSchedule.dock(), qEbDock);
        query.leftJoin(qDemandeTp.xEcBroker(), qBrokerTp);
        query.leftJoin(qEbPartyDestTp.etablissementAdresse(), qEbPartyDestTpEtab);
        query.leftJoin(qEbPartyOriginTp.etablissementAdresse(), qEbPartyOriginTpEtab);
        query.leftJoin(qTracingTp.xEbEtablissementChargeur(), qEbEtabChargeurTp);
        query.leftJoin(qTracingTp.xEbEtablissementTransporteur(), qEbEtabTranspTp);
        query.leftJoin(qTracingTp.xEbChargeurCompagnie(), qEbCompagnieChargeurTp);
        query.leftJoin(qEbRschUnitSchedule.demandeTpQuote(), qDemandeQuoteTp);
        query.leftJoin(qEbPartyDestTp.zone(), qZoneIntermediate);
        query.leftJoin(qDemandeInitial.ebPartyDest().zone(), qZoneDest);

        query.innerJoin(qDemandeTp.xecCurrencyInvoice(), qEcCurrency);

        if (connectedUser.isControlTower()) {
            query.leftJoin(qDemandeTp.xEbUserCt().ebEtablissement(), qEbEtabCtTp);
            query.leftJoin(qDemandeTp.exEbDemandeTransporteurs, qExEbDemandeTranspTp);
            query.leftJoin(qExEbDemandeTranspTp.xEbEtablissement(), qExEbDemandeTranspEtabTp);
        }

        return query;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getRschUnitScheduleGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteriaRSCH criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);

        if (criteria.getListStatut() != null && criteria.getListStatut().size() > 0) {
            where.and(qEbRschUnitSchedule.status.in(criteria.getListStatut()));
        }

        if (criteria.getMaxArrivalDateTp() != null) {
            where.and(qDemandeQuoteTp.deliveryTime.before(criteria.getMaxArrivalDateTp()));
        }

        if (criteria.getMinArrivalDateTp() != null) {
            where.and(qDemandeQuoteTp.deliveryTime.after(criteria.getMinArrivalDateTp()));
        }

        if (criteria.getMaxArrivalDatePa() != null) {
            where.and(qEbRschUnitSchedule.dateArrivalPa.before(criteria.getMaxArrivalDatePa()));
        }

        if (criteria.getMinArrivalDatePa() != null) {
            where.and(qEbRschUnitSchedule.dateArrivalPa.after(criteria.getMinArrivalDatePa()));
        }

        if (criteria.getSelected() != null) {
            where.and(qEbRschUnitSchedule.selected.eq(criteria.getSelected()));
        }

        /* User Visibility Rights */
        if (connectedUser.getService() != null
            && (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker())) {
            where
                .andAnyOf(
                    qEbEtabTranspTp.ebEtablissementNum.eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                    qBrokerTp.isNotNull().and(qBrokerTp.ebUserNum.eq(connectedUser.getEbUserNum())));
        }
        else if (connectedUser.isBroker()) {
            where.and(qBrokerTp.isNotNull().and(qBrokerTp.ebUserNum.eq(connectedUser.getEbUserNum())));
        }
        else if (connectedUser.isChargeur()) {
            where.and(qEbCompagnieChargeurTp.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));

            if (!connectedUser.isSuperAdmin()) {
                BooleanBuilder boolLabels = new BooleanBuilder();

                if (connectedUser.getListCategories() != null) {

                    for (EbCategorie cat: connectedUser.getListCategories()) {

                        if (cat.getLabels() != null) {

                            for (EbLabel label: cat.getLabels()) {
                                boolLabels
                                    .or(
                                        new BooleanBuilder()
                                            .andAnyOf(
                                                qDemandeTp.listLabels.startsWith(label.getEbLabelNum() + ","),
                                                qDemandeTp.listLabels.contains("," + label.getEbLabelNum() + ","),
                                                qDemandeTp.listLabels.endsWith("," + label.getEbLabelNum()),
                                                qDemandeTp.listLabels.eq("" + label.getEbLabelNum())));
                            }

                        }

                    }

                }

                if (!boolLabels.hasValue()) {
                    boolLabels.andAnyOf(qDemandeTp.listLabels.isNull(), qDemandeTp.listLabels.isEmpty());
                }

                /*
                 * where.andAnyOf(new BooleanBuilder().orAllOf(
                 * qEbEtabChargeurTp.ebEtablissementNum.eq(connectedUser.
                 * getEbEtablissement().getEbEtablissementNum()), new
                 * BooleanBuilder().andAnyOf(qDemandeTp.listLabels.isNull(),
                 * qDemandeTp.listLabels.isEmpty(),
                 * qDemandeTp.listLabels.isNotNull().andAnyOf(boolLabels))), new
                 * BooleanBuilder().orAllOf(qEbEtabChargeurTp.ebEtablissementNum
                 * .ne(connectedUser.getEbEtablissement().getEbEtablissementNum(
                 * )), qEbPartyDestTpEtab.ebEtablissementNum.isNotNull(),
                 * qEbPartyDestTpEtab.ebEtablissementNum.eq(connectedUser.
                 * getEbEtablissement().getEbEtablissementNum()),
                 * qEbPartyDestTpEtab.isExterne.isNotNull(),
                 * qEbPartyDestTpEtab.isExterne.isTrue()
                 * ), new
                 * BooleanBuilder().orAllOf(qEbEtabChargeurTp.ebEtablissementNum
                 * .ne(connectedUser.getEbEtablissement().getEbEtablissementNum(
                 * )), qEbPartyOriginTpEtab.ebEtablissementNum.isNotNull(),
                 * qEbPartyOriginTpEtab.ebEtablissementNum.eq(connectedUser.
                 * getEbEtablissement().getEbEtablissementNum()),
                 * qEbPartyOriginTpEtab.isExterne.isNotNull(),
                 * qEbPartyOriginTpEtab.isExterne.isTrue()
                 * ));
                 */

                where
                    .andAnyOf(
                        new BooleanBuilder()
                            .orAllOf(
                                qEbEtabChargeurTp.ebEtablissementNum
                                    .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                new BooleanBuilder()
                                    .andAnyOf(
                                        qDemandeTp.listLabels.isNull(),
                                        qDemandeTp.listLabels.isEmpty(),
                                        qDemandeTp.listLabels.isNotNull().andAnyOf(boolLabels))),
                        new BooleanBuilder()
                            .orAllOf(
                                qEbEtabChargeurTp.ebEtablissementNum
                                    .ne(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                qEbPartyDestTpEtab.ebEtablissementNum.isNotNull(),
                                qEbPartyDestTpEtab.ebEtablissementNum
                                    .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())),
                        new BooleanBuilder()
                            .orAllOf(
                                qEbEtabChargeurTp.ebEtablissementNum
                                    .ne(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                qEbPartyOriginTpEtab.ebEtablissementNum.isNotNull(),
                                qEbPartyOriginTpEtab.ebEtablissementNum
                                    .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())));
            }

        }
        else if (connectedUser.isControlTower()) {
            where
                .andAnyOf(
                    qEbEtabCtTp.ebEtablissementNum.eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                    qExEbDemandeTranspEtabTp.ebEtablissementNum
                        .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));
        }
        /* END User Visibility Rights */

        return query;
    }
}
