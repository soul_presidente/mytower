package com.adias.mytowereasy.rsch.algo;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.HashedMap;

import com.adias.mytowereasy.dock.model.DOCKEnumeration.OpeningPeriodType;
import com.adias.mytowereasy.model.EbRangeHour;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschAlgoResult;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschModeTransport;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschModeTransportOpta;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschUnitOptaParamsRoot;


public class StatiqueAlgo {
    public static Integer DUREEMINIMAL = 15;

    public static List<DockIntervals> getListIntervalsParDock(List<DockAvailabilityAlgo> list) {
        List<DockIntervals> listFinal = new ArrayList<>();
        list.forEach(drh -> {
            List<Interval> lI = null;

            if (!listFinal.isEmpty()
                && listFinal.get(listFinal.size() - 1).getDock().getEbDockNum().equals(drh.getDock().getEbDockNum())) {
                lI = listFinal.get(listFinal.size() - 1).getIntervals();
                lI.add(new Interval(drh.getRh().getStartHour(), drh.getRh().getEndHour()));
                listFinal.get(listFinal.size() - 1).setIntervals(lI);
            }
            else {
                lI = new ArrayList<>();
                lI.add(new Interval(drh.getRh().getStartHour(), drh.getRh().getEndHour()));
                listFinal.add(new DockIntervals(drh.getDock(), lI));
            }

        });
        return listFinal;
    }

    public static List<Interval> affecteRhToInterval(List<Interval> list, Interval rh) {
        List<Interval> l = new ArrayList<>();
        list.forEach(r -> {

            if (r.getStart().compareTo(rh.getStart()) <= 0 && r.getEnd().compareTo(rh.getEnd()) >= 0) {

                if (r.getStart().compareTo(rh.getStart()) != 0 && r.getEnd().compareTo(rh.getEnd()) != 0) {
                    l.add(new Interval(r.getStart(), rh.getStart()));
                    l.add(new Interval(rh.getEnd(), r.getEnd()));
                }
                else {
                    if (r.getStart().compareTo(rh.getStart()) == 0) l.add(new Interval(rh.getEnd(), r.getEnd()));
                    else if (r.getEnd().compareTo(rh.getEnd()) == 0) l.add(new Interval(r.getStart(), rh.getStart()));
                }

            }
            else l.add(r);

        });

        return l;
    }

    public static Set<Interval> divisionInterval(Interval i, Set<Date> durees) {
        Set<Interval> list = new HashSet<Interval>();
        Integer startMin = getTimeMinute(i.getStart());
        Integer endMin = getTimeMinute(i.getEnd());

        durees.forEach(d -> { // attention [..,00:00]
            Integer dureeMin = d.getHours() * 60 + d.getMinutes();

            if ((endMin - startMin) >= dureeMin) {

                for (int k = 0; k + startMin + dureeMin <= endMin; k = k + DUREEMINIMAL) {
                    int var = startMin + k;

                    while (var < endMin && var + dureeMin <= endMin) {
                        int j = var + dureeMin;

                        try {
                            list
                                .add(
                                    new Interval(
                                        getTimeHoureMinute(i.getStart(), var),
                                        getTimeHoureMinute(i.getStart(), j)));
                            var = j;
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }

                }

            }

        });

        return list;
    }

    public static Integer getTimeMinute(Date date) {
        return date.getHours() * 60 + date.getMinutes();
    }

    public static Date getTimeHoureMinute(Date date, Integer timeMin) throws ParseException {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm")
            .parse(
                (date.getYear() + 1900) + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + (timeMin / 60)
                    + ":" + (timeMin % 60));
    }

    public static Date getDateWithoutTime(Date date) {

        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm")
                .parse((date.getYear() + 1900) + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " 00:00");
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    public static Boolean CompartRangeHourToIntervale(EbRangeHour rh, Interval interval) {
        if (getTimeMinute(rh.getStartHour()).equals(getTimeMinute(interval.getStart()))
            && getTimeMinute(rh.getEndHour()).equals(getTimeMinute(interval.getEnd()))) return true;
        return false;
    }

    public static String formatDateOutput(Date date) {
        return (date.getYear() + 1900) + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " + date.getHours()
            + ":" + date.getMinutes();
    }

    public static Integer getDureeIntervalMinute(Interval i) {
        return Math.abs(getTimeMinute(i.getEnd()) - getTimeMinute(i.getStart()));
    }

    public static Integer getPriorityByTypePeriod(Integer typePeriod) {
        if (OpeningPeriodType.NORMAL.getCode().equals(typePeriod)) return 2;
        else if (OpeningPeriodType.EXCEPTIONAL.getCode().equals(typePeriod)) return 3;
        return 1;
    }

    public static Date addDaysToDate(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static List<RschModeTransport> getListMT() {
        List<RschModeTransport> MTs = new ArrayList<>();
        MTs.add(new RschModeTransport(1, "Air", 1));
        MTs.add(new RschModeTransport(2, "Sea", 2));
        MTs.add(new RschModeTransport(3, "Road", 3));
        MTs.add(new RschModeTransport(4, "Integrator", 5));
        MTs.add(new RschModeTransport(5, "Rail", 4));
        return MTs;
    }

    public static RschModeTransport getMtByCode(Integer code) {

        for (RschModeTransport mt: getListMT()) {
            if (mt.getCode().equals(code)) return mt;
        }

        return null;
    }

    public static Integer getDayOfWeek(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        Integer day = c.get(Calendar.DAY_OF_WEEK) - 1;
        if (day == 0) return 7;
        return day;
    }

    public static boolean isOverlap2Intervals(Interval i1, Interval i2) {
        if (((i1.getStart().compareTo(i2.getStart()) <= 0) && (i1.getEnd().compareTo(i2.getEnd()) >= 0))
            || ((i2.getStart().compareTo(i1.getStart()) <= 0) && (i2.getEnd().compareTo(i1.getEnd()) >= 0))
            || ((i1.getStart().compareTo(i2.getStart()) <= 0) && (i1.getStart().compareTo(i2.getEnd()) <= 0
                && i1.getEnd().compareTo(i2.getStart()) > 0 && i1.getEnd().compareTo(i2.getEnd()) < 0))
            || ((i2.getStart().compareTo(i1.getStart()) <= 0) && (i2.getStart().compareTo(i1.getEnd()) <= 0
                && i2.getEnd().compareTo(i1.getStart()) > 0 && i2.getEnd().compareTo(i1.getEnd()) < 0))) return true;
        return false;
    }

    public static boolean isOverlap(List<Interval> listInterval) {

        for (int i = 0; i < listInterval.size() - 1; i++) {

            for (int j = i + 1; j < listInterval.size(); j++) {
                if (isOverlap2Intervals(listInterval.get(i), listInterval.get(j))) return true;
            }

        }

        return false;
    }

    public static Map<Integer, List<Interval>> getIntervalByDock(List<RschUnitOptaParamsRoot> listContainer) {
        Map<Integer, List<Interval>> listCD = new HashedMap();
        listContainer.forEach(c -> {

            if (c.getCreneauQuais() != null) {
                List<Interval> listInterval = new ArrayList<>();

                if (!listCD.isEmpty() && listCD.containsKey(c.getCreneauQuais().getEbDockNum())) listInterval = listCD
                    .get(c.getCreneauQuais().getEbDockNum());

                listInterval.add(c.getCreneauQuais().getInterval());
                listCD.put(c.getCreneauQuais().getEbDockNum(), listInterval);
            }

        });

        return listCD;
    }

    public static Map<Integer, List<RschUnitOptaParamsRoot>>
        getContainerByEntrepot(List<RschUnitOptaParamsRoot> listContainer) {
        Map<Integer, List<RschUnitOptaParamsRoot>> listContainerEntrepot = new HashedMap();
        listContainer.forEach(c -> {

            if (c.getCreneauQuais() != null) {
                List<RschUnitOptaParamsRoot> listCont = new ArrayList<>();

                if (!listContainerEntrepot.isEmpty() && listContainerEntrepot
                    .containsKey(c.getCreneauQuais().getEbEntrepotNum())) listCont = listContainerEntrepot
                        .get(c.getCreneauQuais().getEbEntrepotNum());

                listCont.add(c);
                listContainerEntrepot.put(c.getCreneauQuais().getEbEntrepotNum(), listCont);
            }

        });

        return listContainerEntrepot;
    }

    public static Boolean isAllAffected(List<RschUnitOptaParamsRoot> listContainer) {

        for (int i = 0; i < listContainer.size(); i++) {
            if (listContainer.get(i).getCreneauQuais() == null) return false;
        }

        return true;
    }

    public static Boolean isOverlapIntoSolution(List<RschUnitOptaParamsRoot> listContainer) {
        Map<Integer, List<Interval>> listIntervalPardock = getIntervalByDock(listContainer);

        for (Integer key: listIntervalPardock.keySet()) {
            if (listIntervalPardock.get(key).size() > 1 && isOverlap(listIntervalPardock.get(key))) return true;
        }

        return false;
    }

    public static Boolean isPriorityRespected(List<RschUnitOptaParamsRoot> listContainer) {
        Map<Integer, List<RschUnitOptaParamsRoot>> listContainerPardock = getContainerByEntrepot(listContainer);

        for (Integer key: listContainerPardock.keySet()) {

            if (!allContainerHaveSamePriority(listContainer)
                && !isPriorityRespectedByEntrepot(listContainerPardock.get(key))) {
                return false;
            }

        }

        return true;
    }

    public static Boolean isPriorityRespectedByEntrepot(List<RschUnitOptaParamsRoot> listContainer) {
        Collections
            .sort(
                listContainer,
                (o1, o2) -> o1.getRschUnitSchedule().getPriority().compareTo(o2.getRschUnitSchedule().getPriority()));

        for (int i = 0; i < listContainer.size() - 1; i++) {

            for (int j = i + 1; j < listContainer.size(); j++) {

                if ((listContainer.get(i).getRschUnitSchedule().getPriority() > listContainer
                    .get(j).getRschUnitSchedule().getPriority()
                    && listContainer
                        .get(i).getCreneauQuais().getInterval().getStart()
                        .compareTo(listContainer.get(j).getCreneauQuais().getInterval().getStart()) < 0)
                    || (listContainer.get(i).getRschUnitSchedule().getPriority() < listContainer
                        .get(j).getRschUnitSchedule().getPriority()
                        && listContainer
                            .get(i).getCreneauQuais().getInterval().getStart()
                            .compareTo(listContainer.get(j).getCreneauQuais().getInterval().getStart()) > 0)) {
                    return false;
                }

            }

        }

        return true;
    }

    public static Boolean allContainerHaveSamePriority(List<RschUnitOptaParamsRoot> listContainer) {
        Integer priority = listContainer.get(0).getRschUnitSchedule().getPriority();

        for (int i = 1; i < listContainer.size(); i++) {

            if (listContainer.get(i).getRschUnitSchedule().getPriority() != priority) {
                return false;
            }

        }

        return true;
    }

    public static long getDureeCalculAlgoBySeconds(RschAlgoResult unsolvedAffectContainer) {
        Integer nbreContainer = unsolvedAffectContainer.getContainerList().size();
        Integer nbreCreneau = unsolvedAffectContainer.getCreneauQuaisList().size();
        System.out.println("\ndurée de calcule=" + ((nbreContainer * 10 + (nbreCreneau))) + "seconds\n\n");
        return nbreContainer * 10 + (nbreCreneau);
    }

    public static List<RschModeTransportOpta>
        getListPropTarifByTTAndEntrepot(List<RschModeTransportOpta> listProp, Integer tt, Integer entrepotNum) {
        List<RschModeTransportOpta> listFinal = new ArrayList<>();
        listProp.forEach(prop -> {

            if (prop.getTransitTime().equals(tt) && prop.getEbEntrepotNum().equals(entrepotNum)) {
                listFinal.add(prop);
            }

        });
        return listFinal;
    }

    public static Integer getIndexOfPropTarif(
        List<RschModeTransportOpta> listProp,
        Integer tt,
        Integer entrepotNum,
        BigDecimal cost,
        Integer mt) {

        for (int i = 0; i < listProp.size(); i++) {

            if (listProp.get(i).getTransitTime().equals(tt) && listProp.get(i).getEbEntrepotNum().equals(entrepotNum)
                && listProp.get(i).getTotalCost().equals(cost)
                && listProp.get(i).getModeTransport().getCode().equals(mt)) {
                return i;
            }

        }

        return null;
    }

    public static Integer getIndexOfPropTarifWithoutCost(
        List<RschModeTransportOpta> listProp,
        Integer tt,
        Integer entrepotNum,
        Integer mt,
        Set<Integer> listRschNum) {

        for (int i = 0; i < listProp.size(); i++) {

            if (listProp.get(i).getTransitTime().equals(tt) && listProp.get(i).getEbEntrepotNum().equals(entrepotNum)
                && listProp.get(i).getListEbRschUnitScheduleNum().equals(listRschNum)
                && listProp.get(i).getModeTransport().getCode().equals(mt)) {
                return i;
            }

        }

        return null;
    }

    // elimination les propositions similaires et qui ont des couts elevés
    public static List<RschModeTransportOpta> getFinalListPropTarif(List<RschModeTransportOpta> listProp) {
        List<RschModeTransportOpta> listFinal = new ArrayList<>();

        for (int i = 0; i < listProp.size(); i++) {
            Integer indexMt = null;

            if (!listFinal.isEmpty()) {
                indexMt = getIndexOfPropTarifWithoutCost(
                    listFinal,
                    listProp.get(i).getTransitTime(),
                    listProp.get(i).getEbEntrepotNum(),
                    listProp.get(i).getModeTransport().getCode(),
                    listProp.get(i).getListEbRschUnitScheduleNum());

                if (indexMt != null
                    && listFinal.get(indexMt).getTotalCost().compareTo(listProp.get(i).getTotalCost()) > 0) {
                    listFinal.get(indexMt).setTotalCost(listProp.get(i).getTotalCost());
                }

            }

            if (listFinal.isEmpty() || indexMt == null) {
                listFinal.add(listProp.get(i));
            }

        }

        return listFinal;
    }

    public static Boolean verificationOfSolution(List<RschUnitOptaParamsRoot> lisContainer) {

        for (RschUnitOptaParamsRoot container: lisContainer) {
            if (container.getCreneauQuais() == null || !container
                .getRschUnitSchedule().getEntrepotNum()
                .equals(container.getCreneauQuais().getEbEntrepotNum())) return false;
        }

        return true;
    }

    public static List<RschUnitOptaParamsRoot> actualisationDesCreaneaux(List<RschUnitOptaParamsRoot> lisContainer) {

        for (RschUnitOptaParamsRoot container: lisContainer) {
            container.setCreneauQuais(null);
        }

        return lisContainer;
    }
}
