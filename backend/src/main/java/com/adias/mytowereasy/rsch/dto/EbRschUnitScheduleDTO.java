package com.adias.mytowereasy.rsch.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.adias.mytowereasy.dto.EbRangeHourDTO;
import com.adias.mytowereasy.rsch.model.EbRschUnitSchedule;


public class EbRschUnitScheduleDTO {
    private Integer ebRschUnitScheduleNum;

    private Integer status;

    // Demande TP
    private Integer demandeTpNum;
    private String demandeTpRefTransport;
    private String demandeTpLibelleOriginCountry;
    private String demandeTpLibelleDestCountry;
    private String demandeTpEntrepotUnloadingRef;
    private Integer demandeTpEntrepotUnloadingNum;
    private Integer demandeTpModeTransport;

    private Integer demandeTpCurrencyInvoiceNum;
    private String demandeTpCurrencyInvoiceCode;
    private String demandeTpCurrencyInvoiceSymbol;

    private Integer tracingTpNum;
    private String tracingTpCustomerReference;

    private Integer demandeTpQuoteNum;
    private Date demandeTpQuoteDeliveryTime;

    private Integer unitTpIndex;
    // END Demande TP

    // Demande PA
    private Integer demandePaNum;
    private String demandePaRefTransport;

    private Integer tracingPaNum;

    private Integer unitPaIndex;
    // END Demande PA

    // Combined
    private Integer zoneIntermediateNum;
    private String zoneIntermediateRef;
    private Integer zoneDestPaNum;
    private String zoneDestPaRef;
    // END Combined

    // Algo Parameters & Solution
    private Boolean selected;

    private Integer priority;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", timezone = JsonFormat.DEFAULT_TIMEZONE)
    private Date workDuration;

    private Date dateArrivalTp;

    private Integer modeTransportPa;
    private Boolean modeTransportPaLocked;

    private Date dateDeparturePa;
    private Boolean dateDeparturePaLocked;

    private Date dateArrivalPa;
    private Boolean dateArrivalPaLocked;

    private Integer dockNum;
    private String dockName;
    private Boolean dockLocked;

    private EbRangeHourDTO workRangeHour;
    private Boolean workRangeHourLocked;

    private Date dateUnloadingPa;
    private Boolean dateUnloadingPaLocked;
    // END Algo Parameters & Solution

    // Solution result
    private BigDecimal totalCost;
    // END Solution result

    public EbRschUnitScheduleDTO() {
    }

    public Integer getDemandeTpCurrencyInvoiceNum() {
        return demandeTpCurrencyInvoiceNum;
    }

    public void setDemandeTpCurrencyInvoiceNum(Integer demandeTpCurrencyInvoiceNum) {
        this.demandeTpCurrencyInvoiceNum = demandeTpCurrencyInvoiceNum;
    }

    public String getDemandeTpCurrencyInvoiceCode() {
        return demandeTpCurrencyInvoiceCode;
    }

    public void setDemandeTpCurrencyInvoiceCode(String demandeTpCurrencyInvoiceCode) {
        this.demandeTpCurrencyInvoiceCode = demandeTpCurrencyInvoiceCode;
    }

    public String getDemandeTpCurrencyInvoiceSymbol() {
        return demandeTpCurrencyInvoiceSymbol;
    }

    public void setDemandeTpCurrencyInvoiceSymbol(String demandeTpCurrencyInvoiceSymbol) {
        this.demandeTpCurrencyInvoiceSymbol = demandeTpCurrencyInvoiceSymbol;
    }

    public Date getDateUnloadingPa() {
        return dateUnloadingPa;
    }

    public void setDateUnloadingPa(Date dateUnloadingPa) {
        this.dateUnloadingPa = dateUnloadingPa;
    }

    public Date getDateArrivalTp() {
        return dateArrivalTp;
    }

    public void setDateArrivalTp(Date dateArrivalTp) {
        this.dateArrivalTp = dateArrivalTp;
    }

    public Integer getEbRschUnitScheduleNum() {
        return ebRschUnitScheduleNum;
    }

    public void setEbRschUnitScheduleNum(Integer ebRschUnitScheduleNum) {
        this.ebRschUnitScheduleNum = ebRschUnitScheduleNum;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDemandeTpNum() {
        return demandeTpNum;
    }

    public void setDemandeTpNum(Integer demandeTpNum) {
        this.demandeTpNum = demandeTpNum;
    }

    public String getDemandeTpRefTransport() {
        return demandeTpRefTransport;
    }

    public void setDemandeTpRefTransport(String demandeTpRefTransport) {
        this.demandeTpRefTransport = demandeTpRefTransport;
    }

    public String getDemandeTpLibelleOriginCountry() {
        return demandeTpLibelleOriginCountry;
    }

    public void setDemandeTpLibelleOriginCountry(String demandeTpLibelleOriginCountry) {
        this.demandeTpLibelleOriginCountry = demandeTpLibelleOriginCountry;
    }

    public String getDemandeTpLibelleDestCountry() {
        return demandeTpLibelleDestCountry;
    }

    public void setDemandeTpLibelleDestCountry(String demandeTpLibelleDestCountry) {
        this.demandeTpLibelleDestCountry = demandeTpLibelleDestCountry;
    }

    public Integer getDemandePaNum() {
        return demandePaNum;
    }

    public void setDemandePaNum(Integer demandePaNum) {
        this.demandePaNum = demandePaNum;
    }

    public String getDemandePaRefTransport() {
        return demandePaRefTransport;
    }

    public void setDemandePaRefTransport(String demandePaRefTransport) {
        this.demandePaRefTransport = demandePaRefTransport;
    }

    public Integer getTracingTpNum() {
        return tracingTpNum;
    }

    public void setTracingTpNum(Integer tracingTpNum) {
        this.tracingTpNum = tracingTpNum;
    }

    public String getTracingTpCustomerReference() {
        return tracingTpCustomerReference;
    }

    public void setTracingTpCustomerReference(String tracingTpCustomerReference) {
        this.tracingTpCustomerReference = tracingTpCustomerReference;
    }

    public Integer getTracingPaNum() {
        return tracingPaNum;
    }

    public void setTracingPaNum(Integer tracingPaNum) {
        this.tracingPaNum = tracingPaNum;
    }

    public Integer getUnitTpIndex() {
        return unitTpIndex;
    }

    public void setUnitTpIndex(Integer unitTpIndex) {
        this.unitTpIndex = unitTpIndex;
    }

    public Integer getUnitPaIndex() {
        return unitPaIndex;
    }

    public void setUnitPaIndex(Integer unitPaIndex) {
        this.unitPaIndex = unitPaIndex;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Date getWorkDuration() {
        return workDuration;
    }

    public void setWorkDuration(Date workDuration) {
        this.workDuration = workDuration;
    }

    public Integer getModeTransportPa() {
        return modeTransportPa;
    }

    public void setModeTransportPa(Integer modeTransportPa) {
        this.modeTransportPa = modeTransportPa;
    }

    public Boolean getModeTransportPaLocked() {
        return modeTransportPaLocked;
    }

    public void setModeTransportPaLocked(Boolean modeTransportPaLocked) {
        this.modeTransportPaLocked = modeTransportPaLocked;
    }

    public Date getDateDeparturePa() {
        return dateDeparturePa;
    }

    public void setDateDeparturePa(Date dateDeparturePa) {
        this.dateDeparturePa = dateDeparturePa;
    }

    public Boolean getDateDeparturePaLocked() {
        return dateDeparturePaLocked;
    }

    public void setDateDeparturePaLocked(Boolean dateDeparturePaLocked) {
        this.dateDeparturePaLocked = dateDeparturePaLocked;
    }

    public Date getDateArrivalPa() {
        return dateArrivalPa;
    }

    public void setDateArrivalPa(Date dateArrivalPa) {
        this.dateArrivalPa = dateArrivalPa;
    }

    public Boolean getDateArrivalPaLocked() {
        return dateArrivalPaLocked;
    }

    public void setDateArrivalPaLocked(Boolean dateArrivalPaLocked) {
        this.dateArrivalPaLocked = dateArrivalPaLocked;
    }

    public Integer getDockNum() {
        return dockNum;
    }

    public void setDockNum(Integer dockNum) {
        this.dockNum = dockNum;
    }

    public Boolean getDockLocked() {
        return dockLocked;
    }

    public void setDockLocked(Boolean dockLocked) {
        this.dockLocked = dockLocked;
    }

    public EbRangeHourDTO getWorkRangeHour() {
        return workRangeHour;
    }

    public void setWorkRangeHour(EbRangeHourDTO workRangeHour) {
        this.workRangeHour = workRangeHour;
    }

    public Boolean getWorkRangeHourLocked() {
        return workRangeHourLocked;
    }

    public void setWorkRangeHourLocked(Boolean workRangeHourLocked) {
        this.workRangeHourLocked = workRangeHourLocked;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public String getDockName() {
        return dockName;
    }

    public void setDockName(String dockName) {
        this.dockName = dockName;
    }

    public String getDemandeTpEntrepotUnloadingRef() {
        return demandeTpEntrepotUnloadingRef;
    }

    public void setDemandeTpEntrepotUnloadingRef(String demandeTpEntrepotUnloadingRef) {
        this.demandeTpEntrepotUnloadingRef = demandeTpEntrepotUnloadingRef;
    }

    public Integer getDemandeTpEntrepotUnloadingNum() {
        return demandeTpEntrepotUnloadingNum;
    }

    public void setDemandeTpEntrepotUnloadingNum(Integer demandeTpEntrepotUnloadingNum) {
        this.demandeTpEntrepotUnloadingNum = demandeTpEntrepotUnloadingNum;
    }

    public Integer getDemandeTpQuoteNum() {
        return demandeTpQuoteNum;
    }

    public void setDemandeTpQuoteNum(Integer demandeTpQuoteNum) {
        this.demandeTpQuoteNum = demandeTpQuoteNum;
    }

    public Date getDemandeTpQuoteDeliveryTime() {
        return demandeTpQuoteDeliveryTime;
    }

    public void setDemandeTpQuoteDeliveryTime(Date demandeTpQuoteDeliveryTime) {
        this.demandeTpQuoteDeliveryTime = demandeTpQuoteDeliveryTime;
    }

    public Integer getDemandeTpModeTransport() {
        return demandeTpModeTransport;
    }

    public void setDemandeTpModeTransport(Integer demandeTpModeTransport) {
        this.demandeTpModeTransport = demandeTpModeTransport;
    }

    public Integer getZoneIntermediateNum() {
        return zoneIntermediateNum;
    }

    public void setZoneIntermediateNum(Integer zoneIntermediateNum) {
        this.zoneIntermediateNum = zoneIntermediateNum;
    }

    public String getZoneIntermediateRef() {
        return zoneIntermediateRef;
    }

    public void setZoneIntermediateRef(String zoneIntermediateRef) {
        this.zoneIntermediateRef = zoneIntermediateRef;
    }

    public Integer getZoneDestPaNum() {
        return zoneDestPaNum;
    }

    public void setZoneDestPaNum(Integer zoneDestPaNum) {
        this.zoneDestPaNum = zoneDestPaNum;
    }

    public String getZoneDestPaRef() {
        return zoneDestPaRef;
    }

    public void setZoneDestPaRef(String zoneDestPaRef) {
        this.zoneDestPaRef = zoneDestPaRef;
    }

    public EbRschUnitScheduleDTO(EbRschUnitSchedule entity) {
        this.ebRschUnitScheduleNum = entity.getEbRschUnitScheduleNum();
        this.status = entity.getStatus();

        if (entity.getDemandeTp() != null) {
            this.demandeTpNum = entity.getDemandeTp().getEbDemandeNum();
            this.dateArrivalTp = entity.getDemandeTp().getDateOfArrival();
            this.demandeTpRefTransport = entity.getDemandeTp().getRefTransport();
            this.demandeTpLibelleOriginCountry = entity.getDemandeTp().getLibelleOriginCountry();
            this.demandeTpLibelleDestCountry = entity.getDemandeTp().getLibelleDestCountry();
            this.demandeTpModeTransport = entity.getDemandeTp().getxEcModeTransport();
            this.demandeTpCurrencyInvoiceNum = entity.getDemandeTp().getXecCurrencyInvoice() != null ?
                entity.getDemandeTp().getXecCurrencyInvoice().getEcCurrencyNum() :
                null;
            this.demandeTpCurrencyInvoiceCode = entity.getDemandeTp().getXecCurrencyInvoice() != null ?
                entity.getDemandeTp().getXecCurrencyInvoice().getCode() :
                null;
            this.demandeTpCurrencyInvoiceSymbol = entity.getDemandeTp().getXecCurrencyInvoice() != null ?
                entity.getDemandeTp().getXecCurrencyInvoice().getSymbol() :
                null;

            if (entity.getDemandeTp().getxEbEntrepotUnloading() != null) {
                this.demandeTpEntrepotUnloadingNum = entity.getDemandeTp().getxEbEntrepotUnloading().getEbEntrepotNum();
                this.demandeTpEntrepotUnloadingRef = entity.getDemandeTp().getxEbEntrepotUnloading().getReference();
            }

        }

        if (entity.getDemandePa() != null) {
            this.demandePaNum = entity.getDemandePa().getEbDemandeNum();
            this.demandePaRefTransport = entity.getDemandePa().getRefTransport();
        }

        if (entity.getTracingTp() != null) {
            this.tracingTpNum = entity.getTracingTp().getEbTtTracingNum();
            this.tracingTpCustomerReference = entity.getTracingTp().getCustomerReference();
        }

        if (entity.getTracingPa() != null) {
            this.tracingPaNum = entity.getTracingPa().getEbTtTracingNum();
        }

        this.unitTpIndex = entity.getUnitTpIndex();
        this.unitPaIndex = entity.getUnitPaIndex();
        this.selected = entity.getSelected();
        this.priority = entity.getPriority();
        this.workDuration = entity.getWorkDuration();
        this.modeTransportPa = entity.getModeTransportPa();
        this.modeTransportPaLocked = entity.getModeTransportPaLocked();
        this.dateDeparturePa = entity.getDateDeparturePa();
        this.dateDeparturePaLocked = entity.getDateDeparturePaLocked();
        this.dateArrivalPa = entity.getDateArrivalPa();
        this.dateArrivalPaLocked = entity.getDateArrivalPaLocked();

        if (entity.getDock() != null) {
            this.dockNum = entity.getDock().getEbDockNum();
            this.dockName = entity.getDock().getNom();
        }

        this.dockLocked = entity.getDockLocked();
        this.dateUnloadingPa = entity.getDateUnloadingPa();
        this.dateUnloadingPaLocked = entity.getDateUnloadingPaLocked();
        this.workRangeHour = entity.getWorkRangeHour() != null ? new EbRangeHourDTO(entity.getWorkRangeHour()) : null;
        this.workRangeHourLocked = entity.getWorkRangeHourLocked();
        this.dateUnloadingPa = entity.getDateUnloadingPa();
        this.totalCost = entity.getTotalCost();

        if (entity.getDemandeTpQuote() != null) {
            this.demandeTpQuoteNum = entity.getDemandeTpQuote().getExEbDemandeTransporteurNum();
            this.demandeTpQuoteDeliveryTime = entity.getDemandeTpQuote().getDeliveryTime();
        }

        if (entity.getDemandeTp().getEbPartyDest() != null
            && entity.getDemandeTp().getEbPartyDest().getZone() != null) {
            this.zoneIntermediateNum = entity.getDemandeTp().getEbPartyDest().getZone().getEbZoneNum();
            this.zoneIntermediateRef = entity.getDemandeTp().getEbPartyDest().getZone().getRef();
        }

        if (entity.getDemandeTp().getEbDemandeInitial() != null
            && entity.getDemandeTp().getEbDemandeInitial().getEbPartyDest() != null
            && entity.getDemandeTp().getEbDemandeInitial().getEbPartyDest().getZone() != null) {
            this.zoneDestPaNum = entity.getDemandeTp().getEbDemandeInitial().getEbPartyDest().getZone().getEbZoneNum();
            this.zoneDestPaRef = entity.getDemandeTp().getEbDemandeInitial().getEbPartyDest().getZone().getRef();
        }

    }

    public Boolean getDateUnloadingPaLocked() {
        return dateUnloadingPaLocked;
    }

    public void setDateUnloadingPaLocked(Boolean dateUnloadingPaLocked) {
        this.dateUnloadingPaLocked = dateUnloadingPaLocked;
    }
}
