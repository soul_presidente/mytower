package com.adias.mytowereasy.rsch.algo.optaPlanner.domain;

import java.util.Date;

import com.adias.mytowereasy.rsch.algo.Interval;


/**
 * Contains a possibility for a dock
 */
public class RschDockOpenCreneauOpta {
    private Integer ebDockNum;
    private String ebDockNom;
    private Date startDispo;

    private Integer ebEntrepotNum;
    private Interval interval;

    private RschModeTransportOpta modeTransportOpta;// info MT+cost+tt+idRS

    public RschDockOpenCreneauOpta() {
        super();
    }

    public RschDockOpenCreneauOpta(
        Integer ebDockNum,
        String ebDocknom,
        Date startDispo,
        Integer ebEntrepotNum,
        Interval interval,
        RschModeTransportOpta modeTransportOpta) {
        super();
        this.ebDockNum = ebDockNum;
        this.ebDockNom = ebDocknom;
        this.startDispo = startDispo;
        this.ebEntrepotNum = ebEntrepotNum;
        this.interval = interval;
        this.modeTransportOpta = modeTransportOpta;
    }

    public RschDockOpenCreneauOpta(RschDockOpenCreneauOpta entity) {
        super();
        this.ebDockNum = entity.getEbDockNum();
        this.ebDockNom = entity.getEbDockNom();
        this.startDispo = entity.getStartDispo();
        this.ebEntrepotNum = entity.getEbEntrepotNum();
        this.interval = entity.getInterval();
        this.modeTransportOpta = entity.getModeTransportOpta();
    }

    public RschDockOpenCreneauOpta(
        Integer ebDockNum,
        String ebDocknom,
        Date startDispo,
        Integer ebEntrepotNum,
        Interval interval) {
        super();
        this.ebDockNum = ebDockNum;
        this.ebDockNom = ebDocknom;
        this.startDispo = startDispo;
        this.ebEntrepotNum = ebEntrepotNum;
        this.interval = interval;
    }

    public RschModeTransportOpta getModeTransportOpta() {
        return modeTransportOpta;
    }

    public void setModeTransportOpta(RschModeTransportOpta modeTransportOpta) {
        this.modeTransportOpta = modeTransportOpta;
    }

    public RschDockOpenCreneauOpta(Integer ebDockNum, Interval interval) {
        super();
        this.ebDockNum = ebDockNum;
        this.interval = interval;
    }

    public Integer getEbEntrepotNum() {
        return ebEntrepotNum;
    }

    public void setEbEntrepotNum(Integer ebEntrepotNum) {
        this.ebEntrepotNum = ebEntrepotNum;
    }

    public RschDockOpenCreneauOpta(
        Integer ebDockNum,
        String ebDocknom,
        Integer ebEntrepotNum,
        Interval interval,
        Date startDispo) {
        super();
        this.ebDockNum = ebDockNum;
        this.ebDockNom = ebDocknom;
        this.ebEntrepotNum = ebEntrepotNum;
        this.interval = interval;
        this.startDispo = startDispo;
    }

    public Date getStartDispo() {
        return startDispo;
    }

    public void setStartDispo(Date startDispo) {
        this.startDispo = startDispo;
    }

    public Interval getInterval() {
        return interval;
    }

    public void setInterval(Interval interval) {
        this.interval = interval;
    }

    public Integer getEbDockNum() {
        return ebDockNum;
    }

    public void setEbDockNum(Integer ebDockNum) {
        this.ebDockNum = ebDockNum;
    }

    public String getEbDockNom() {
        return ebDockNom;
    }

    public void setEbDockNom(String ebDockNom) {
        this.ebDockNom = ebDockNom;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ebDockNom == null) ? 0 : ebDockNom.hashCode());
        result = prime * result + ((ebDockNum == null) ? 0 : ebDockNum.hashCode());
        result = prime * result + ((ebEntrepotNum == null) ? 0 : ebEntrepotNum.hashCode());
        result = prime * result + ((interval == null) ? 0 : interval.hashCode());
        result = prime * result + ((modeTransportOpta == null) ? 0 : modeTransportOpta.hashCode());
        result = prime * result + ((startDispo == null) ? 0 : startDispo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RschDockOpenCreneauOpta other = (RschDockOpenCreneauOpta) obj;

        if (ebDockNom == null) {
            if (other.ebDockNom != null) return false;
        }
        else if (!ebDockNom.equals(other.ebDockNom)) return false;

        if (ebDockNum == null) {
            if (other.ebDockNum != null) return false;
        }
        else if (!ebDockNum.equals(other.ebDockNum)) return false;

        if (ebEntrepotNum == null) {
            if (other.ebEntrepotNum != null) return false;
        }
        else if (!ebEntrepotNum.equals(other.ebEntrepotNum)) return false;

        if (interval == null) {
            if (other.interval != null) return false;
        }
        else if (!interval.equals(other.interval)) return false;

        if (modeTransportOpta == null) {
            if (other.modeTransportOpta != null) return false;
        }
        else if (!modeTransportOpta.equals(other.modeTransportOpta)) return false;

        if (startDispo == null) {
            if (other.startDispo != null) return false;
        }
        else if (!startDispo.equals(other.startDispo)) return false;

        return true;
    }

    @Override
    public String toString() {
        return "\nCreneauQuais [ebDockNum=" + ebDockNum + ", ebDocknom=" + ebDockNom + ", startDispo="
            + startDispo.getHours() + ":" + startDispo.getMinutes() + ", ebEntrepotNum=" + ebEntrepotNum + ", interval="
            + interval + ", \nmodeTransportOpta=" + modeTransportOpta + "]";
    }
}
