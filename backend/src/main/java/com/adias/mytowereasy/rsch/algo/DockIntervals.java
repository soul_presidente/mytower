package com.adias.mytowereasy.rsch.algo;

import java.util.List;

import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.RschModeTransportOpta;


/**
 * For a specified dock, this class store all opened hours intervals
 */
public class DockIntervals {
    private EbDock dock;
    private List<Interval> intervals;
    private List<RschModeTransportOpta> listTransitTimeRSCHModeTransport;

    public DockIntervals(EbDock dock, List<Interval> intervals) {
        super();
        this.dock = dock;
        this.intervals = intervals;
    }

    public DockIntervals(
        EbDock dock,
        List<Interval> intervals,
        List<RschModeTransportOpta> listTransitTimeRSCHModeTransport) {
        super();
        this.dock = dock;
        this.intervals = intervals;
        this.listTransitTimeRSCHModeTransport = listTransitTimeRSCHModeTransport;
    }

    public List<RschModeTransportOpta> getListTransitTimeRSCHModeTransport() {
        return listTransitTimeRSCHModeTransport;
    }

    public void setListTransitTimeRSCHModeTransport(List<RschModeTransportOpta> listTransitTimeRSCHModeTransport) {
        this.listTransitTimeRSCHModeTransport = listTransitTimeRSCHModeTransport;
    }

    public DockIntervals() {
        super();
        // TODO Auto-generated constructor stub
    }

    public EbDock getDock() {
        return dock;
    }

    public void setDock(EbDock dock) {
        this.dock = dock;
    }

    public List<Interval> getIntervals() {
        return intervals;
    }

    public void setIntervals(List<Interval> intervals) {
        this.intervals = intervals;
    }

    @Override
    public String toString() {
        return "DockIntervals [dock=" + dock.getNom() + ", entrepot=" + dock.getEntrepot().getEbEntrepotNum()
            + ", intervals=" + intervals + ", listTransitTimeRSCHModeTransport=" + listTransitTimeRSCHModeTransport
            + "]\n";
    }
}
