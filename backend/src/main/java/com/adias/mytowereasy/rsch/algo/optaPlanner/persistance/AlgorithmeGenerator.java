package com.adias.mytowereasy.rsch.algo.optaPlanner.persistance;

import java.util.*;

import com.adias.mytowereasy.rsch.algo.AlgoTest;
import com.adias.mytowereasy.rsch.algo.DockIntervals;
import com.adias.mytowereasy.rsch.algo.StatiqueAlgo;
import com.adias.mytowereasy.rsch.algo.optaPlanner.domain.*;
import com.adias.mytowereasy.rsch.dto.EbRschUnitScheduleDTO;
import com.adias.mytowereasy.rsch.model.EbRschUnitSchedule;


public class AlgorithmeGenerator {
    public RschAlgoResult
        createAffectContainer(List<EbRschUnitSchedule> listEbRsch, List<DockIntervals> docksInterval) {
        ListContainerDurees cd = getcontainerList(listEbRsch);

        System.out.println("data input algo :");
        System.out.println(cd.getListContainer());
        AlgoTest.ecritureDansUnFichier(cd.getListContainer().toString());
        System.out.println(docksInterval);
        AlgoTest.ecritureDansUnFichier(docksInterval.toString());
        System.out.println(StatiqueAlgo.getListMT());

        List<RschDockOpenCreneauOpta> generatorCreneauQuais = generatorCreneauQuais(docksInterval, cd.getListduree());
        System.out.println(generatorCreneauQuais);

        AlgoTest.ecritureDansUnFichier(generatorCreneauQuais + "");

        System.out.println(generatorCreneauQuais.size());
        return new RschAlgoResult(cd.getListContainer(), generatorCreneauQuais);
    }

    public List<RschDockOpenCreneauOpta>
        generatorCreneauQuais(List<DockIntervals> listDockInterval, Set<DureeParEntrepot> durees) {
        List<RschDockOpenCreneauOpta> list = new ArrayList<>();

        listDockInterval.forEach(dockInterval -> {
            List<RschDockOpenCreneauOpta> listCQ = new ArrayList<>();

            dockInterval.getIntervals().forEach(interval -> {
                Set<Date> dureeParEntrepot = new HashSet<>();
                dureeParEntrepot = getDureeParEntrepot(durees, dockInterval.getDock().getEntrepot().getEbEntrepotNum());

                StatiqueAlgo.divisionInterval(interval, dureeParEntrepot).forEach(i -> {
                    listCQ
                        .add(
                            new RschDockOpenCreneauOpta(
                                dockInterval.getDock().getEbDockNum(),
                                dockInterval.getDock().getNom(),
                                dockInterval.getDock().getEntrepot().getEbEntrepotNum(),
                                i,
                                dockInterval.getIntervals().get(0).getStart()));
                });
            });

            // affectation des propTarifs
            for (RschDockOpenCreneauOpta cq: listCQ) {

                for (RschModeTransportOpta propTarif: dockInterval.getListTransitTimeRSCHModeTransport()) {
                    cq.setModeTransportOpta(propTarif);
                    list.add(new RschDockOpenCreneauOpta(cq));
                }

            }

        });
        Collections.sort(list, (o1, o2) -> o1.getInterval().getStart().compareTo(o2.getInterval().getStart()));
        return list;
    }

    public ListContainerDurees getcontainerList(List<EbRschUnitSchedule> listEbRsch) {
        ListContainerDurees cd = new ListContainerDurees();
        listEbRsch.forEach(ebRsch -> {

            if (ebRsch != null) {
                cd
                    .getListContainer().add(
                        new RschUnitOptaParamsRoot(
                            new RschUnitScheduleOpta(new EbRschUnitScheduleDTO(ebRsch)),
                            null,
                            null));// cas CQ & MT non Locked
                cd
                    .getListduree().add(
                        new DureeParEntrepot(
                            ebRsch.getWorkDuration(),
                            ebRsch.getDemandeTp().getxEbEntrepotUnloading().getEbEntrepotNum()));
            }

        });

        return cd;
    }

    public Set<Date> getDureeParEntrepot(Set<DureeParEntrepot> dureeParEntrepot, Integer entrepotNum) {
        Set<Date> duree = new HashSet<>();
        dureeParEntrepot.forEach(de -> {
            if (de.getEntrepotNum().equals(entrepotNum)) duree.add(de.getDuree());
        });

        return duree;
    }
}


class ListContainerDurees {
    private List<RschUnitOptaParamsRoot> listContainer = new ArrayList<>();
    private Set<DureeParEntrepot> listduree = new HashSet<>();

    public List<RschUnitOptaParamsRoot> getListContainer() {
        return listContainer;
    }

    public void setListContainer(List<RschUnitOptaParamsRoot> listContainer) {
        this.listContainer = listContainer;
    }

    public Set<DureeParEntrepot> getListduree() {
        return this.listduree;
    }

    public void setListduree(Set<DureeParEntrepot> listduree) {
        this.listduree = listduree;
    }

    public ListContainerDurees(List<RschUnitOptaParamsRoot> listContainer, Set<DureeParEntrepot> listduree) {
        super();
        this.listContainer = listContainer;
        this.listduree = listduree;
    }

    public ListContainerDurees() {
        super();
        this.listContainer = new ArrayList<>();
        this.listduree = new HashSet<>();
    }

    @Override
    public String toString() {
        return "ListContainerDurees [listContainer=" + listContainer + ", listduree=" + listduree + "]";
    }
}


class DureeParEntrepot {
    private Date duree;
    private Integer entrepotNum;

    public DureeParEntrepot(Date duree, Integer entrepotNum) {
        super();
        this.duree = duree;
        this.entrepotNum = entrepotNum;
    }

    public Date getDuree() {
        return duree;
    }

    public void setDuree(Date duree) {
        this.duree = duree;
    }

    public Integer getEntrepotNum() {
        return entrepotNum;
    }

    public void setEntrepotNum(Integer entrepotNum) {
        this.entrepotNum = entrepotNum;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((duree == null) ? 0 : duree.hashCode());
        result = prime * result + ((entrepotNum == null) ? 0 : entrepotNum.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        DureeParEntrepot other = (DureeParEntrepot) obj;

        if (duree == null) {
            if (other.duree != null) return false;
        }
        else if (!duree.equals(other.duree)) return false;

        if (entrepotNum == null) {
            if (other.entrepotNum != null) return false;
        }
        else if (!entrepotNum.equals(other.entrepotNum)) return false;

        return true;
    }
}
