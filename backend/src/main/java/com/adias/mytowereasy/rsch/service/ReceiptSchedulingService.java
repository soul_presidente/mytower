package com.adias.mytowereasy.rsch.service;

import java.util.List;

import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.rsch.dto.EbRschUnitScheduleDTO;
import com.adias.mytowereasy.rsch.model.EbRschUnitSchedule;
import com.adias.mytowereasy.rsch.model.SearchCriteriaRSCH;


public interface ReceiptSchedulingService {
    /**
     * Search RschUnitSchedule
     * 
     * @param criteria
     *            Additionals criterias
     * @return List of entities
     */
    public List<EbRschUnitSchedule> searchRschUnitSchedule(SearchCriteriaRSCH criteria);

    /**
     * Count RschUnitSchedule using same criterias as search method
     * 
     * @see searchRschUnitSchedule
     * @param criteria
     * @return
     */
    public Long countListEbRschUnitSchedule(SearchCriteriaRSCH criteria);

    List<EbRschUnitScheduleDTO> convertEntitiesToDto(List<EbRschUnitSchedule> entities);

    /**
     * RschUnitSchedule insert process
     * 
     * @param ebDemande
     *            The original demande (Transport Principal)
     * @param ebTtTracings
     *            TrackTrace of the initial demande
     */
    public void insertRschUnitSchedule(EbDemande ebDemande, List<EbTtTracing> ebTtTracings);

    /**
     * Update RschUnitSchedule<br>
     * Only update fields displayed in dashboard
     * 
     * @param original
     *            RschUnitScheduleDto sent by frontend
     */
    public EbRschUnitSchedule inlineDashboardUpdate(EbRschUnitScheduleDTO original);

    /**
     * Return all EbRschUnitSchedule at status calculated that contains at least
     * one value repeated on
     * than one times for the followings fields :
     * <ul>
     * <li>dateDeparturePa</li>
     * <li>dateArrivalPa</li>
     * <li>demandeTp.xEbEntrepotUnloading</li>
     * <li>demandeTp.ebPartyDest.zone</li>
     * <li>demandeTp.ebDemandeInitial.ebPartyDest.zone</li>
     * </ul>
     */
    public List<EbRschUnitSchedule> listUnifiableRschUnitSchedule();

    /**
     * Unplanify all selected
     */
    public void doUnplanify();

    /**
     * Start the algo to calculate best solution of selected elements
     * TODO : Implement this
     */
    public void doCalculate();
}
