package com.adias.mytowereasy.rsch.model;

import com.adias.mytowereasy.util.GenericEnum;


public class RSCHEnumeration {
    public enum RschArrivingBefore implements GenericEnum {
        TODAY(1, "TODAY"),
        DAY_PLUS_1(2, "DAY_PLUS_1"),
        DAY_PLUS_2(3, "DAY_PLUS_2"),
        DAY_PLUS_3(4, "DAY_PLUS_3"),
        WEEK_PLUS_1(5, "WEEK_PLUS_1"),
        WEEK_PLUS_2(6, "WEEK_PLUS_2"),
        MONTH_PLUS_1(7, "MONTH_PLUS_1"),
        MONTH_PLUS_6(8, "MONTH_PLUS_6");

        private Integer code;
        private String key;

        private RschArrivingBefore(Integer code, String key) {
            this.code = code;
            this.key = key;
        }

        @Override
        public Integer getCode() {
            return this.code;
        }

        @Override
        public String getKey() {
            return this.key;
        }
    }

    public enum RschUnitScheduleStatus implements GenericEnum {
        TO_PLANIFY(0, "TO_PLANIFY"), PLANIFIED(1, "PLANIFIED"), CALCULATED(2, "CALCULATED");

        private Integer code;
        private String key;

        private RschUnitScheduleStatus(Integer code, String key) {
            this.code = code;
            this.key = key;
        }

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }
    }
}
