package com.adias.mytowereasy.rsch.algo.optaPlanner.domain;

public class RschModeTransport {
    private Integer code;
    private String libelle;
    private Integer order;

    public RschModeTransport() {
        super();
        // TODO Auto-generated constructor stub
    }

    public RschModeTransport(Integer code) {
        this();
        this.code = code;
    }

    public RschModeTransport(Integer code, String libelle, Integer order) {
        super();
        this.code = code;
        this.libelle = libelle;
        this.order = order;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    @Override
    public String toString() {
        return "MT [code=" + code + ", libelle=" + libelle + ", order=" + order + "]";
    }
}
