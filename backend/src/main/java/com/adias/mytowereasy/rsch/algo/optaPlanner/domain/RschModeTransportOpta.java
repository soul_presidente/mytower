package com.adias.mytowereasy.rsch.algo.optaPlanner.domain;

import java.math.BigDecimal;
import java.util.Set;


public class RschModeTransportOpta {
    private RschModeTransport modeTransport;
    private BigDecimal totalCost;
    private Integer transitTime;
    private Set<Integer> listEbRschUnitScheduleNum;
    private Integer ebEntrepotNum;

    public Integer getEbEntrepotNum() {
        return ebEntrepotNum;
    }

    public void setEbEntrepotNum(Integer ebEntrepotNum) {
        this.ebEntrepotNum = ebEntrepotNum;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public RschModeTransportOpta() {
        super();
    }

    public RschModeTransport getModeTransport() {
        return modeTransport;
    }

    public void setModeTransport(RschModeTransport modeTransport) {
        this.modeTransport = modeTransport;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public Set<Integer> getListEbRschUnitScheduleNum() {
        return listEbRschUnitScheduleNum;
    }

    public void setListEbRschUnitScheduleNum(Set<Integer> listEbRschUnitScheduleNum) {
        this.listEbRschUnitScheduleNum = listEbRschUnitScheduleNum;
    }

    public RschModeTransportOpta(
        RschModeTransport modeTransport,
        BigDecimal totalCost,
        Integer transitTime,
        Set<Integer> listEbRschUnitScheduleNum) {
        super();
        this.modeTransport = modeTransport;
        this.totalCost = totalCost;
        this.transitTime = transitTime;
        this.listEbRschUnitScheduleNum = listEbRschUnitScheduleNum;
    }

    public RschModeTransportOpta(
        RschModeTransport modeTransport,
        BigDecimal totalCost,
        Integer transitTime,
        Set<Integer> listEbRschUnitScheduleNum,
        Integer ebEntrepotNum) {
        super();
        this.modeTransport = modeTransport;
        this.totalCost = totalCost;
        this.transitTime = transitTime;
        this.listEbRschUnitScheduleNum = listEbRschUnitScheduleNum;
        this.ebEntrepotNum = ebEntrepotNum;
    }

    public RschModeTransportOpta(
        RschModeTransport modeTransport,
        BigDecimal totalCost,
        Integer transitTime,
        Integer ebEntrepotNum) {
        super();
        this.modeTransport = modeTransport;
        this.totalCost = totalCost;
        this.transitTime = transitTime;
        this.ebEntrepotNum = ebEntrepotNum;
    }

    public RschModeTransportOpta(RschModeTransportOpta entity) {
        super();
        this.modeTransport = entity.getModeTransport();
        this.totalCost = entity.getTotalCost();
        this.transitTime = entity.getTransitTime();
        this.listEbRschUnitScheduleNum = entity.getListEbRschUnitScheduleNum();
        this.ebEntrepotNum = entity.getEbEntrepotNum();
    }

    @Override
    public String toString() {
        return "RschModeTransportOpta [modeTransport=" + modeTransport + ",\n totalCost=" + totalCost + ", transitTime="
            + transitTime + ", listEbRschUnitScheduleNum=" + listEbRschUnitScheduleNum + ", ebEntrepotNum="
            + ebEntrepotNum + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ebEntrepotNum == null) ? 0 : ebEntrepotNum.hashCode());
        result = prime * result + ((listEbRschUnitScheduleNum == null) ? 0 : listEbRschUnitScheduleNum.hashCode());
        result = prime * result + ((modeTransport == null) ? 0 : modeTransport.hashCode());
        result = prime * result + ((totalCost == null) ? 0 : totalCost.hashCode());
        result = prime * result + ((transitTime == null) ? 0 : transitTime.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RschModeTransportOpta other = (RschModeTransportOpta) obj;

        if (ebEntrepotNum == null) {
            if (other.ebEntrepotNum != null) return false;
        }
        else if (!ebEntrepotNum.equals(other.ebEntrepotNum)) return false;

        if (listEbRschUnitScheduleNum == null) {
            if (other.listEbRschUnitScheduleNum != null) return false;
        }
        else if (!listEbRschUnitScheduleNum.equals(other.listEbRschUnitScheduleNum)) return false;

        if (modeTransport == null) {
            if (other.modeTransport != null) return false;
        }
        else if (!modeTransport.equals(other.modeTransport)) return false;

        if (totalCost == null) {
            if (other.totalCost != null) return false;
        }
        else if (!totalCost.equals(other.totalCost)) return false;

        if (transitTime == null) {
            if (other.transitTime != null) return false;
        }
        else if (!transitTime.equals(other.transitTime)) return false;

        return true;
    }
}
