package com.adias.mytowereasy.rsch.algo.optaPlanner.domain;

import java.util.Date;

import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.model.EbRangeHour;
import com.adias.mytowereasy.rsch.dto.EbRschUnitScheduleDTO;


/***
 * Contains unit parameters for algo
 */
public class RschUnitScheduleOpta {
    private Integer ebRschUnitScheduleNum;

    private Boolean selected;

    private Integer priority;

    private Date workDuration;

    private Date dateArrivalTp;

    private Integer modeTransportPa;
    private Boolean modeTransportPaLocked;

    private Date dateDeparturePa;
    private Boolean dateDeparturePaLocked;

    private Date dateArrivalPa;
    private Boolean dateArrivalPaLocked;

    private EbDock dock;
    private Boolean dockLocked;

    private EbRangeHour workRangeHour;
    private Boolean workRangeHourLocked;

    private Integer entrepotNum;

    private Double totalCost;

    private Integer demandePaNum;

    private Integer demandeTpNum;

    private Date dateUnloadingPa;
    private Boolean dateUnloadingPaLocked;

    public RschUnitScheduleOpta() {
        super();
    }

    /*
     * les variables ...Locked doit étre initialisée par la valeur false
     */
    public RschUnitScheduleOpta(EbRschUnitScheduleDTO ebRschUnitSchedule) {
        super();
        this.ebRschUnitScheduleNum = ebRschUnitSchedule.getEbRschUnitScheduleNum();
        this.selected = ebRschUnitSchedule.getSelected();
        this.priority = ebRschUnitSchedule.getPriority();
        this.dateArrivalTp = ebRschUnitSchedule.getDemandeTpQuoteDeliveryTime();
        this.workDuration = ebRschUnitSchedule.getWorkDuration();
        this.modeTransportPa = ebRschUnitSchedule.getModeTransportPa();
        this.modeTransportPaLocked = ebRschUnitSchedule.getModeTransportPaLocked() != null ?
            ebRschUnitSchedule.getModeTransportPaLocked() :
            false;
        this.dateDeparturePa = ebRschUnitSchedule.getDateDeparturePa();
        this.dateDeparturePaLocked = ebRschUnitSchedule.getDateDeparturePaLocked() != null ?
            ebRschUnitSchedule.getDateDeparturePaLocked() :
            false;
        this.dateArrivalPa = ebRschUnitSchedule.getDateArrivalPa();
        this.dateArrivalPaLocked = ebRschUnitSchedule.getDateArrivalPaLocked() != null ?
            ebRschUnitSchedule.getDateArrivalPaLocked() :
            false;
        this.dock = new EbDock(ebRschUnitSchedule.getDockNum(), ebRschUnitSchedule.getDockName());
        this.dockLocked = ebRschUnitSchedule.getDockLocked() != null ? ebRschUnitSchedule.getDockLocked() : false;

        this.dateUnloadingPa = ebRschUnitSchedule.getDateUnloadingPa();

        if (ebRschUnitSchedule.getWorkRangeHour() != null) {
            this.workRangeHour = new EbRangeHour();
            this.workRangeHour.setStartHour(ebRschUnitSchedule.getWorkRangeHour().getStartHour());
            this.workRangeHour.setEndHour(ebRschUnitSchedule.getWorkRangeHour().getEndHour());
        }

        this.workRangeHourLocked = ebRschUnitSchedule.getWorkRangeHourLocked() != null ?
            ebRschUnitSchedule.getWorkRangeHourLocked() :
            false;
        this.entrepotNum = ebRschUnitSchedule.getDemandeTpEntrepotUnloadingNum();
        this.demandePaNum = ebRschUnitSchedule.getDemandePaNum();
        this.demandeTpNum = ebRschUnitSchedule.getDemandeTpNum();

        this.dateUnloadingPa = ebRschUnitSchedule.getDateUnloadingPa();
        this.dateUnloadingPaLocked = ebRschUnitSchedule.getDateUnloadingPaLocked() != null ?
            ebRschUnitSchedule.getDateUnloadingPaLocked() :
            false;
    }

    public Boolean getDateUnloadingPaLocked() {
        return dateUnloadingPaLocked;
    }

    public void setDateUnloadingPaLocked(Boolean dateUnloadingPaLocked) {
        this.dateUnloadingPaLocked = dateUnloadingPaLocked;
    }

    public Date getDateUnloadingPa() {
        return dateUnloadingPa;
    }

    public void setDateUnloadingPa(Date dateUnloadingPa) {
        this.dateUnloadingPa = dateUnloadingPa;
    }

    public Date getDateArrivalTp() {
        return dateArrivalTp;
    }

    public void setDateArrivalTp(Date dateArrivalTp) {
        this.dateArrivalTp = dateArrivalTp;
    }

    public Integer getDemandePaNum() {
        return demandePaNum;
    }

    public void setDemandePaNum(Integer demandePaNum) {
        this.demandePaNum = demandePaNum;
    }

    public Integer getDemandeTpNum() {
        return demandeTpNum;
    }

    public void setDemandeTpNum(Integer demandeTpNum) {
        this.demandeTpNum = demandeTpNum;
    }

    public RschUnitScheduleOpta(Integer ebRschUnitScheduleNum) {
        super();
        this.ebRschUnitScheduleNum = ebRschUnitScheduleNum;
    }

    public Integer getEntrepotNum() {
        return entrepotNum;
    }

    public void setEntrepotNum(Integer entrepotNum) {
        this.entrepotNum = entrepotNum;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public Integer getEbRschUnitScheduleNum() {
        return ebRschUnitScheduleNum;
    }

    public void setEbRschUnitScheduleNum(Integer ebRschUnitScheduleNum) {
        this.ebRschUnitScheduleNum = ebRschUnitScheduleNum;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Date getWorkDuration() {
        return workDuration;
    }

    public void setWorkDuration(Date workDuration) {
        this.workDuration = workDuration;
    }

    public Integer getModeTransportPa() {
        return modeTransportPa;
    }

    public void setModeTransportPa(Integer modeTransportPa) {
        this.modeTransportPa = modeTransportPa;
    }

    public Boolean getModeTransportPaLocked() {
        return modeTransportPaLocked;
    }

    public void setModeTransportPaLocked(Boolean modeTransportPaLocked) {
        this.modeTransportPaLocked = modeTransportPaLocked;
    }

    public Date getDateDeparturePa() {
        return dateDeparturePa;
    }

    public void setDateDeparturePa(Date dateDeparturePa) {
        this.dateDeparturePa = dateDeparturePa;
    }

    public Boolean getDateDeparturePaLocked() {
        return dateDeparturePaLocked;
    }

    public void setDateDeparturePaLocked(Boolean dateDeparturePaLocked) {
        this.dateDeparturePaLocked = dateDeparturePaLocked;
    }

    public Date getDateArrivalPa() {
        return dateArrivalPa;
    }

    public void setDateArrivalPa(Date dateArrivalPa) {
        this.dateArrivalPa = dateArrivalPa;
    }

    public Boolean getDateArrivalPaLocked() {
        return dateArrivalPaLocked;
    }

    public void setDateArrivalPaLocked(Boolean dateArrivalPaLocked) {
        this.dateArrivalPaLocked = dateArrivalPaLocked;
    }

    public EbDock getDock() {
        return dock;
    }

    public void setDock(EbDock dock) {
        this.dock = dock;
    }

    public Boolean getDockLocked() {
        return dockLocked;
    }

    public void setDockLocked(Boolean dockLocked) {
        this.dockLocked = dockLocked;
    }

    public EbRangeHour getWorkRangeHour() {
        return workRangeHour;
    }

    public void setWorkRangeHour(EbRangeHour workRangeHour) {
        this.workRangeHour = workRangeHour;
    }

    public Boolean getWorkRangeHourLocked() {
        return workRangeHourLocked;
    }

    public void setWorkRangeHourLocked(Boolean workRangeHourLocked) {
        this.workRangeHourLocked = workRangeHourLocked;
    }
}
