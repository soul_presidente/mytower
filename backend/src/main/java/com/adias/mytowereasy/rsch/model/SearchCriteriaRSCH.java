package com.adias.mytowereasy.rsch.model;

import java.util.Date;
import java.util.List;

import com.adias.mytowereasy.utils.search.SearchCriteria;


public class SearchCriteriaRSCH extends SearchCriteria {
    private List<Integer> listEbRschUnitScheduleNum;
    private Date minArrivalDateTp;
    private Date maxArrivalDateTp;
    private Date minArrivalDatePa;
    private Date maxArrivalDatePa;
    private Boolean selected;

    public List<Integer> getListEbRschUnitScheduleNum() {
        return listEbRschUnitScheduleNum;
    }

    public void setListEbRschUnitScheduleNum(List<Integer> listEbRschUnitScheduleNum) {
        this.listEbRschUnitScheduleNum = listEbRschUnitScheduleNum;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    public Date getMinArrivalDateTp() {
        return minArrivalDateTp;
    }

    public void setMinArrivalDateTp(Date minArrivalDateTp) {
        this.minArrivalDateTp = minArrivalDateTp;
    }

    public Date getMaxArrivalDateTp() {
        return maxArrivalDateTp;
    }

    public void setMaxArrivalDateTp(Date maxArrivalDateTp) {
        this.maxArrivalDateTp = maxArrivalDateTp;
    }

    public Date getMinArrivalDatePa() {
        return minArrivalDatePa;
    }

    public void setMinArrivalDatePa(Date minArrivalDatePa) {
        this.minArrivalDatePa = minArrivalDatePa;
    }

    public Date getMaxArrivalDatePa() {
        return maxArrivalDatePa;
    }

    public void setMaxArrivalDatePa(Date maxArrivalDatePa) {
        this.maxArrivalDatePa = maxArrivalDatePa;
    }
}
