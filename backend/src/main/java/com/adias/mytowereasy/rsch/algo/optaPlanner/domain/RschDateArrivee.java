package com.adias.mytowereasy.rsch.algo.optaPlanner.domain;

import java.util.Date;


public class RschDateArrivee {
    private Integer ebEntrepotNum;

    private Date dateArrivee;

    private Integer transitTime;

    private Integer shift;

    public RschDateArrivee(Integer ebEntrepotNum, Date dateArrivee) {
        super();
        this.ebEntrepotNum = ebEntrepotNum;
        this.dateArrivee = dateArrivee;
    }

    public RschDateArrivee() {
        super();
        // TODO Auto-generated constructor stub
    }

    public RschDateArrivee(Integer ebEntrepotNum, Date dateArrivee, Integer transitTime) {
        super();
        this.ebEntrepotNum = ebEntrepotNum;
        this.dateArrivee = dateArrivee;
        this.transitTime = transitTime;
        this.shift = 0;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public Integer getShift() {
        return shift;
    }

    public void setShift(Integer shift) {
        this.shift = shift;
    }

    public Date getDateArrivee() {
        return dateArrivee;
    }

    public void setDateArrivee(Date dateArrivee) {
        this.dateArrivee = dateArrivee;
    }

    public Integer getEbEntrepotNum() {
        return ebEntrepotNum;
    }

    public void setEbEntrepotNum(Integer ebEntrepotNum) {
        this.ebEntrepotNum = ebEntrepotNum;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateArrivee == null) ? 0 : dateArrivee.hashCode());
        result = prime * result + ((ebEntrepotNum == null) ? 0 : ebEntrepotNum.hashCode());
        result = prime * result + ((shift == null) ? 0 : shift.hashCode());
        result = prime * result + ((transitTime == null) ? 0 : transitTime.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        RschDateArrivee other = (RschDateArrivee) obj;

        if (dateArrivee == null) {
            if (other.dateArrivee != null) return false;
        }
        else if (!dateArrivee.equals(other.dateArrivee)) return false;

        if (ebEntrepotNum == null) {
            if (other.ebEntrepotNum != null) return false;
        }
        else if (!ebEntrepotNum.equals(other.ebEntrepotNum)) return false;

        if (shift == null) {
            if (other.shift != null) return false;
        }
        else if (!shift.equals(other.shift)) return false;

        if (transitTime == null) {
            if (other.transitTime != null) return false;
        }
        else if (!transitTime.equals(other.transitTime)) return false;

        return true;
    }

    @Override
    public String toString() {
        return "RschDateArrivee [ebEntrepotNum=" + ebEntrepotNum + ", dateArrivee=" + dateArrivee + ", transitTime="
            + transitTime + ", shift=" + shift + "]";
    }
}
