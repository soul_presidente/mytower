package com.adias.mytowereasy.utils.search;

import com.adias.mytowereasy.model.QEbTypeUnit;


public class SearchCriteriaTypeUnit extends SearchCriteria {
    QEbTypeUnit qEbTypeUnit = QEbTypeUnit.ebTypeUnit;

    private boolean isCount = false;

    private String code;

    private String libelle;

    private String typeUnitCode;

    private Long ebUnitNum;

    public String getTypeUnitCode() {
        return typeUnitCode;
    }

    public void setTypeUnitCode(String typeUnitCode) {
        this.typeUnitCode = typeUnitCode;
    }

    public boolean isCount() {
        return isCount;
    }

    public void setCount(boolean isCount) {
        this.isCount = isCount;
    }

    public String getCode() {
        return code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Long getEbUnitNum() {
        return ebUnitNum;
    }

    public void setEbUnitNum(Long ebUnitNum) {
        this.ebUnitNum = ebUnitNum;
    }
}
