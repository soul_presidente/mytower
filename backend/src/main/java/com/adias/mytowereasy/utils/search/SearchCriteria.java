/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.utils.search;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Convert;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.ConstantImpl;
import com.querydsl.core.types.Ops;
import com.querydsl.core.types.dsl.*;

import com.adias.mytowereasy.dao.impl.Dao;
import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchCriteria {

    // datatable
    protected Integer start;
    protected Map<String, String> search;
    protected List<Map<OrderCriterias, String>> order;

    // User Criteria
    protected Integer ebUserNum;
    protected Integer ebUserProfileNum;
    protected String email;
    protected String emailNomPrenom;
    protected Integer ebEtablissementNum;
    protected boolean withListAdresse;
    protected boolean withListCostCenter;
    protected boolean withListContacts;
    protected boolean withPassword;
    protected String searchterm;
    protected Integer pageNumber;
    protected Integer size;
    protected boolean roleChargeur;
    protected boolean roleBroker;
    protected boolean roleTransporteur;
    protected boolean roleControlTower;
    protected Integer status;
    protected Integer ebContactRequestNum;
    protected Integer destinataire;
    protected boolean demandeEnCours;
    protected boolean admin;
    protected boolean superAdmin;
    protected Integer ecRoleNum;
    protected Integer typeContener;
    protected boolean forInvite;
    protected boolean receivedRequest;
    protected boolean contact;
    protected boolean blackList;
    protected boolean notBlackList;
    protected boolean sentRequests;
    protected boolean contactable;
    protected boolean withListCombinaison;
    protected boolean etablissementRequest;
    protected boolean excludeConnectedUser;
    protected boolean forChargeur;
    protected boolean forControlTower;
    protected boolean ignoreContact;
    protected boolean isCompagnie;
    protected boolean communityCompany;
    protected boolean fromCommunity;
    protected boolean communityEstablishment;
    protected boolean excludeCt;
    protected Boolean getAll;
    protected boolean simpleSearch;
    protected Boolean dangerous;
    protected Integer modeTransport;
    protected Integer typeDemande;
    protected Double totalWeight;
    protected Double maxWeight;
    protected Double maxVolume;
    protected Double maxHeight;
    protected Double maxLength;
    protected Double maxWidth;
    protected Double height;
    protected Double width;
    protected Double length;
    protected Double weight;
    protected Double maxTaxableWeight;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> dangerousGood;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected String uns;
    protected Double classGood;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Double> classGoods;
    protected Integer nbPackages;
    protected String companyName;
    protected String companyCode;
    protected Integer connectedUserNum;
    protected Integer connectedControlTower;
    protected Integer idObject;
    protected List<Integer> listIdObject;
    protected boolean withListDemande;
    protected boolean includeGlobalValues;
    protected List<Integer> companyNums;

    @JsonProperty
    protected boolean ascendant;

    protected boolean extractData;

    protected String orderedColumn;

    protected String ebModuleName;
    protected Integer ebCompNum;

    protected Integer ecCountryNum;
    protected String hsCode;

    protected Integer ebDestinationCountryNum;
    protected boolean withAccessRights;
    protected String user;
    protected EbUser userReturn;

    protected Integer module;
    protected List<Integer> listModule;

    // contient true ou false : concernant si type of request est active ou
    // desctiver
    protected Boolean activated;

    protected Integer ebCompagnieNum;

    protected Integer ebCompagnieGuestNum;

    protected Integer ebCompagnieCibleNum;

    protected Boolean selectCountryDestination;

    // ebdemande num
    protected EbDemande ebDemande;

    // user service
    protected Integer userService;

    protected Integer ebDemandeNum;

    protected Long ebDelOrderNum;

    protected Integer ebCommandeNum;

    protected Integer currencyCibleNum;

    protected Integer currencyGuestNum;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Long> listGroupeNum;

    public Date dateDeb;

    public Date dateFin;

    public boolean forListMarchandise;

    protected boolean boutonAction;

    public Map<String, String> mapCustomFields;

    public Map<String, List<Integer>> mapCategoryFields;

    public List<Map<String, String>> dtConfig;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listStatut;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listStatutStr;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listTypeRegion;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listTransportRef;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listCustomerReference;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listQrGroupe;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listDestinations;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listCategorie;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listCarrier;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listModeTransport;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listOrigins;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> champListCategorie;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listCostCenter;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listIncoterm;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date datePickUpTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date datePickUpFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date dateCreationTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date dateCreationFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date expectedDateTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date expectedDateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date finalDeliveryTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date finalDeliveryFrom;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listOwnerOfTheRequests;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listPartyDestination;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listOriginCity;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listDestinationCity;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listTransportPriority;

    protected String searchInput;

    protected List<String> adresseEligibilities;

    protected List<String> listEmailsNotInPlatform;

    protected String listFlag;

    protected Integer typeDocumentNum;

    protected boolean withEtablissement;

    protected boolean withEtablissementDetail;

    protected Boolean withDeactive;

    protected boolean showGlobals;

    protected String city;
    protected String zipCode;
    protected Integer ebZoneNum;

    protected String carrierUniqRefNum;

    protected String companyOrigin;

    protected List<Integer> transportConfirmation;
    protected Integer userNum; // for acl controle verfication
    protected Integer userProfilNum; // for acl controle verfication
    protected Integer gridVersion;

    protected String numAwbBol;
    protected List<String> listCustomFieldsValues;

    private String originCity;
    private String destinationCity;

    protected Integer ruleCategory;

    private Integer currencyCostItem;

    protected List<Integer> listOfExistingTransporteur;
    protected Integer companyRole;
    protected boolean withActiveUsers;
    
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listOrderStatus;
    
    private EbUser xEbOwnerOfTheRequest;
    
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listcustomerOrderReference;
    
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listStatusDelivery;

    public Integer getCurrencyCostItem() {
        return currencyCostItem;
    }

    public void setCurrencyCostItem(Integer currencyCostItem) {
        this.currencyCostItem = currencyCostItem;
    }

    public String getOriginCity() {
        return originCity;
    }

    public void setOriginCity(String originCity) {
        this.originCity = originCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    protected boolean withListMarchandise;

    public String getListFlag() {
        return listFlag;
    }

    public void setListFlag(String listFlag) {
        this.listFlag = listFlag;
    }

    public String emailUnknownUser;

    /*
     * https://jira.adias.fr/browse/CPB-136 : Truncate the hours part of @date
     * by creating a new DateTimeExpression
     */
    protected DateTimeOperation<Date> truncDate(Date date) {
        return Expressions.dateTimeOperation(Date.class, Ops.DateTimeOps.DATE, ConstantImpl.create(date));
    }

    protected DateOperation<Timestamp> truncTimestamp(DateTimePath<Timestamp> dateCreationSys) {
        return Expressions.dateOperation(Timestamp.class, Ops.DateTimeOps.DATE, dateCreationSys);
    }

    protected DateOperation<Timestamp> truncDate(DateTimePath<Date> dateOfGoodsAvailability) {
        return Expressions.dateOperation(Timestamp.class, Ops.DateTimeOps.DATE, dateOfGoodsAvailability);
    }

    public SearchCriteria() {
        if (searchInput == null) searchInput = "{}";
    }

    public void setSearchInput(String searchInput, List<CustomFields> customsFields) throws JsonParseException, JsonMappingException, IOException {

        try {

            if (searchInput != null && !searchInput.isEmpty() && !searchInput.contentEquals("{}")) {
                Map<String, Object> tmp, newSearchInputTmp = new HashMap<String, Object>();
                Gson gson = new Gson();
                tmp = gson.fromJson(searchInput, new TypeToken<Map<String, Object>>() {
                }.getType());

                mapCustomFields = new HashMap<String, String>();
                mapCategoryFields = new HashMap<String, List<Integer>>();

                for (String key: tmp.keySet()) {

                    if (key != null && !key.isEmpty() && tmp.get(key) != null) {

                        if (CollectionUtils.isNotEmpty(customsFields) &&
                            customsFields
                            .stream().anyMatch(customField -> customField.getName().contentEquals(key))) {
                            mapCustomFields.put(key, (String) tmp.get(key));
                        }
                        else if (key.matches("^categoryField[0-9]+$") || key.matches("^listCategorie[0-9]+$")) {
                            String tmpArrStr = gson.toJson(tmp.get(key), new TypeToken<List<Integer>>() {
                            }.getType());
                            List<Integer> arr = gson.fromJson(tmpArrStr, new TypeToken<List<Integer>>() {
                            }.getType());
                            mapCategoryFields.put(key, arr);
                        }
                        else newSearchInputTmp.put(key, tmp.get(key));

                    }

                }

                this.searchInput = gson.toJson(tmp, new TypeToken<Map<String, Object>>() {
                }.getType());
                SearchCriteria criteria = (SearchCriteria) deserialize(this.searchInput, SearchCriteria.class);

                this.listTransportRef = criteria.listTransportRef;
                this.listCustomerReference = criteria.listCustomerReference;
                this.listQrGroupe = criteria.listQrGroupe;
                this.listDestinations = criteria.listDestinations;
                this.uns = criteria.uns;
                this.classGoods = criteria.classGoods;
                this.dangerousGood = criteria.dangerousGood;
                this.listCategorie = criteria.listCategorie;
                this.listCarrier = criteria.listCarrier;
                this.listStatut = criteria.listStatut;
                this.listStatutStr = criteria.listStatutStr;
                this.datePickUpFrom = criteria.datePickUpFrom;
                this.datePickUpTo = criteria.datePickUpTo;
                this.dateCreationFrom = criteria.dateCreationFrom;
                this.dateCreationTo = criteria.dateCreationTo;
                this.expectedDateFrom = criteria.expectedDateFrom;
                this.expectedDateTo = criteria.expectedDateTo;
                this.finalDeliveryFrom = criteria.finalDeliveryFrom;
                this.listOwnerOfTheRequests = criteria.listOwnerOfTheRequests;
                this.listPartyDestination = criteria.listPartyDestination;
                this.finalDeliveryTo = criteria.finalDeliveryTo;
                this.listModeTransport = criteria.listModeTransport;
                this.listOrigins = criteria.listOrigins;
                this.listCostCenter = criteria.listCostCenter;
                this.champListCategorie = criteria.champListCategorie;
                this.ebDemandeNum = criteria.ebDemandeNum;
                this.ebDelOrderNum = criteria.ebDelOrderNum;
                this.listIncoterm = criteria.listIncoterm;
                this.listFlag = criteria.listFlag;
                this.companyOrigin = criteria.companyOrigin;
                this.carrierUniqRefNum = criteria.carrierUniqRefNum;
                this.transportConfirmation = criteria.transportConfirmation;
                this.originCity = criteria.originCity;
                this.destinationCity = criteria.destinationCity;
                this.listOriginCity = criteria.listOriginCity;
                this.listDestinationCity = criteria.listDestinationCity;
                this.listTransportPriority = criteria.listTransportPriority;
                this.listOfExistingTransporteur = criteria.listOfExistingTransporteur;
                this.maxTaxableWeight = criteria.maxTaxableWeight;
            }

        } catch (Exception e) {
            throw e;
        }

    }

    public void applyAdvancedSearchForCustomFields(BooleanBuilder where, StringExpression fieldColumn) {

        if (MapUtils.isNotEmpty(mapCustomFields)) {
            String value = null;
            StringExpression customFieldExpr = null;
            BooleanBuilder customFieldValidJsonExpr = new BooleanBuilder();

            for (String key: mapCustomFields.keySet()) {
                value = mapCustomFields.get(key);

                if (StringUtils.isNotBlank(value)) {
                    customFieldValidJsonExpr
                        .and(Expressions.booleanTemplate(Dao.SCHEMA + "is_valid_json({0}) = true", fieldColumn));
                    customFieldExpr = Expressions
                        .stringTemplate(
                            Dao.SCHEMA + "get_value_by_keyvalue({0}, {1}, {2}, {3})",
                            fieldColumn,
                            "name",
                            key,
                            "value");
                }

                if (customFieldExpr != null) {
                    where
                        .and(customFieldValidJsonExpr).and(customFieldExpr.toLowerCase().contains(value.toLowerCase()));
                }

            }

        }

    }

    public List<Integer> getListOrigins() {
        return listOrigins;
    }

    public void setListOrigins(List<Integer> listOrigins) {
        this.listOrigins = listOrigins;
    }

    public boolean isContactable() {
        return contactable;
    }

    public void setContactable(boolean contactable) {
        this.contactable = contactable;
    }

    public boolean isExcludeConnectedUser() {
        return excludeConnectedUser;
    }

    public void setExcludeConnectedUser(boolean excludeConnectedUser) {
        this.excludeConnectedUser = excludeConnectedUser;
    }

    public boolean isEtablissementRequest() {
        return etablissementRequest;
    }

    public boolean getAscendant() {
        return ascendant;
    }

    public void setAscendant(boolean ascendant) {
        this.ascendant = ascendant;
    }

    public void setEtablissementRequest(boolean etablissementRequest) {
        this.etablissementRequest = etablissementRequest;
    }

    public boolean isWithListCombinaison() {
        return withListCombinaison;
    }

    public void setWithListCombinaison(boolean withListCombinaison) {
        this.withListCombinaison = withListCombinaison;
    }

    public Date getDateDeb() {
        return dateDeb;
    }

    public void setDateDeb(Date dateDeb) {
        this.dateDeb = dateDeb;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public String getEmailNomPrenom() {
        return emailNomPrenom;
    }

    public Boolean getWithDeactive() {
        return withDeactive;
    }

    public void setWithDeactive(boolean withDeactive) {
        this.withDeactive = withDeactive;
    }

    public void setEmailNomPrenom(String emailNomPrenom) {
        this.emailNomPrenom = emailNomPrenom;
    }

    public String getOrderedColumn() {
        return orderedColumn;
    }

    public void setOrderedColumn(String orderedColumn) {
        this.orderedColumn = orderedColumn;
    }

    public Integer getEcCountryNum() {
        return ecCountryNum;
    }

    public void setEcCountryNum(Integer ecCountryNum) {
        this.ecCountryNum = ecCountryNum;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Map<String, String> getSearch() {
        return search;
    }

    public void setSearch(Map<String, String> search) {
        this.search = search;
    }

    public List<Map<OrderCriterias, String>> getOrder() {
        return order;
    }

    public void setOrder(List<Map<OrderCriterias, String>> order) {
        this.order = order;
    }

    public boolean isForInvite() {
        return forInvite;
    }

    public void setForInvite(boolean forInvite) {
        this.forInvite = forInvite;
    }

    public Integer getEcRoleNum() {
        return ecRoleNum;
    }

    public void setEcRoleNum(Integer ecRoleNum) {
        this.ecRoleNum = ecRoleNum;
    }

    public Integer getEbContactRequestNum() {
        return ebContactRequestNum;
    }

    public void setEbContactRequestNum(Integer ebContactRequestNum) {
        this.ebContactRequestNum = ebContactRequestNum;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public boolean isRoleBroker() {
        return roleBroker;
    }

    public void setRoleBroker(boolean roleBroker) {
        this.roleBroker = roleBroker;
    }

    public boolean isRoleTransporteur() {
        return roleTransporteur;
    }

    public void setRoleTransporteur(boolean roleTransporteur) {
        this.roleTransporteur = roleTransporteur;
    }

    public Integer getPageNumber() {
        return pageNumber;
    }

    public void setPageNumber(Integer pageNumber) {
        this.pageNumber = pageNumber;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getSearchterm() {
        return searchterm;
    }

    public void setSearchterm(String searchterm) {
        this.searchterm = searchterm;
    }

    private String ebUserPassword;

    private Integer ebCategorieNum;

    public String getEbUserPassword() {
        return ebUserPassword;
    }

    public void setEbUserPassword(String ebUserPassword) {
        this.ebUserPassword = ebUserPassword;
    }

    public Integer getEbUserNum() {
        return ebUserNum;
    }

    public void setEbUserNum(Integer ebUserNum) {
        this.ebUserNum = ebUserNum;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getEbEtablissementNum() {
        return ebEtablissementNum;
    }

    public void setEbEtablissementNum(Integer ebEtablissementNum) {
        this.ebEtablissementNum = ebEtablissementNum;
    }

    public boolean isWithListAdresse() {
        return withListAdresse;
    }

    public void setWithListAdresse(boolean withListAdresse) {
        this.withListAdresse = withListAdresse;
    }

    public boolean isWithListCostCenter() {
        return withListCostCenter;
    }

    public void setWithListCostCenter(boolean withListCostCenter) {
        this.withListCostCenter = withListCostCenter;
    }

    public boolean isWithListContacts() {
        return withListContacts;
    }

    public void setWithListContacts(boolean withListContacts) {
        this.withListContacts = withListContacts;
    }

    public Integer getDestinataire() {
        return destinataire;
    }

    public void setDestinataire(Integer destinataire) {
        this.destinataire = destinataire;
    }

    public boolean isDemandeEnCours() {
        return demandeEnCours;
    }

    public void setDemandeEnCours(boolean demandeEnCours) {
        this.demandeEnCours = demandeEnCours;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public boolean isSuperAdmin() {
        return superAdmin;
    }

    public void setSuperAdmin(boolean superAdmin) {
        this.superAdmin = superAdmin;
    }

    public Integer getEbCategorieNum() {
        return ebCategorieNum;
    }

    public void setEbCategorieNum(Integer ebCategorieNum) {
        this.ebCategorieNum = ebCategorieNum;
    }

    public String getEbModuleName() {
        return ebModuleName;
    }

    public void setEbModuleName(String ebModuleName) {
        this.ebModuleName = ebModuleName;
    }

    public Integer getEbCompNum() {
        return ebCompNum;
    }

    public void setEbCompNum(Integer ebCompNum) {
        this.ebCompNum = ebCompNum;
    }

    public Integer getEbDestinationCountryNum() {
        return ebDestinationCountryNum;
    }

    public void setEbDestinationCountryNum(Integer ebDestinationCountryNum) {
        this.ebDestinationCountryNum = ebDestinationCountryNum;
    }

    public Boolean getSelectCountryDestination() {
        return selectCountryDestination;
    }

    public void setSelectCountryDestination(Boolean selectCountryDestination) {
        this.selectCountryDestination = selectCountryDestination;
    }

    public boolean isWithAccessRights() {
        return withAccessRights;
    }

    public void setWithAccessRights(boolean withAccessRights) {
        this.withAccessRights = withAccessRights;
    }

    public Integer getEbCompagnieNum() {
        return ebCompagnieNum;
    }

    public void setEbCompagnieNum(Integer ebCompagnieNum) {
        this.ebCompagnieNum = ebCompagnieNum;
    }

    public EbDemande getEbDemande() {
        return ebDemande;
    }

    public void setEbDemande(EbDemande ebDemande) {
        this.ebDemande = ebDemande;
    }

    public boolean isReceivedRequest() {
        return receivedRequest;
    }

    public void setReceivedRequest(boolean receivedRequest) {
        this.receivedRequest = receivedRequest;
    }

    public boolean isContact() {
        return contact;
    }

    public void setContact(boolean contact) {
        this.contact = contact;
    }

    public boolean isBlackList() {
        return blackList;
    }

    public void setBlackList(boolean blackList) {
        this.blackList = blackList;
    }

    public boolean isSentRequests() {
        return sentRequests;
    }

    public void setSentRequests(boolean sentRequests) {
        this.sentRequests = sentRequests;
    }

    public Integer getUserService() {
        return userService;
    }

    public void setUserService(Integer userService) {
        this.userService = userService;
    }

    public enum OrderCriterias {
        column("column"), dir("dir"), columnName("columnName");

        private String name;

        private OrderCriterias(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public enum ColumnCriterias {
        name("name"),
        data("data"),
        searchable("searchable"),
        orderable("orderable"),
        searchValue("searchValue"),
        searchRegex("searchRegex");

        private String nme;

        private ColumnCriterias(String name) {
            this.nme = name;
        }

        public String getName() {
            return nme;
        }
    }

    public static <T> Object
        deserialize(String str, Class<T> _Type) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Object returnObject = objectMapper.readValue(str, _Type);
        return returnObject;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) throws JsonParseException, JsonMappingException, IOException {
        this.user = user;

        if (this.user != null) {
            this.userReturn = (EbUser) deserialize(user, EbUser.class);
        }

    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public Integer getEbDemandeNum() {
        return ebDemandeNum;
    }

    public void setEbDemandeNum(Integer ebDemandeNum) {
        this.ebDemandeNum = ebDemandeNum;
    }

    public EbUser getUserReturn() {
        return userReturn;
    }

    public void setUserReturn(EbUser userReturn) {
        this.userReturn = userReturn;
    }

    public List<Integer> getListStatut() {
        return listStatut;
    }

    public void setListStatut(List<Integer> listStatut) {
        this.listStatut = listStatut;
    }

    public List<Integer> getListTypeRegion() {
        return listTypeRegion;
    }

    public void setListTypeRegion(List<Integer> listTypeRegion) {
        this.listTypeRegion = listTypeRegion;
    }

    public List<String> getListTransportRef() {
        return listTransportRef;
    }

    public void setListTransportRef(List<String> listTransportRef) {
        this.listTransportRef = listTransportRef;
    }

    public List<String> getListCustomerReference() {
        return listCustomerReference;
    }

    public void setListCustomerReference(List<String> listCustomerReference) {
        this.listCustomerReference = listCustomerReference;
    }

    public List<Integer> getListQrGroupe() {
        return listQrGroupe;
    }

    public void setListQrGroupe(List<Integer> listQrGroupe) {
        this.listQrGroupe = listQrGroupe;
    }

    public List<Integer> getListDestinations() {
        return listDestinations;
    }

    public void setListDestinations(List<Integer> listDestinations) {
        this.listDestinations = listDestinations;
    }

    public List<Integer> getListCategorie() {
        return listCategorie;
    }

    public void setListCategorie(List<Integer> listCategorie) {
        this.listCategorie = listCategorie;
    }

    public List<Integer> getListCarrier() {
        return listCarrier;
    }

    public void setListCarrier(List<Integer> listCarrier) {
        this.listCarrier = listCarrier;
    }

    public Date getDatePickUpTo() {
        return datePickUpTo;
    }

    public void setDatePickUpTo(Date datePickUpTo) {
        this.datePickUpTo = datePickUpTo;
    }

    public Date getDatePickUpFrom() {
        return datePickUpFrom;
    }

    public void setDatePickUpFrom(Date datePickUpFrom) {
        this.datePickUpFrom = datePickUpFrom;
    }

    public Date getDateCreationTo() {
        return dateCreationTo;
    }

    public void setDateCreationTo(Date dateCreationTo) {
        this.dateCreationTo = dateCreationTo;
    }

    public Date getDateCreationFrom() {
        return dateCreationFrom;
    }

    public void setDateCreationFrom(Date dateCreationFrom) {
        this.dateCreationFrom = dateCreationFrom;
    }

    public String getSearchInput() {
        return searchInput;
    }

    public boolean isNotBlackList() {
        return notBlackList;
    }

    public void setNotBlackList(boolean notBlackList) {
        this.notBlackList = notBlackList;
    }

    public List<Integer> getListModeTransport() {
        return listModeTransport;
    }

    public void setListModeTransport(List<Integer> listModeTransport) {
        this.listModeTransport = listModeTransport;
    }

    public Integer getEbCommandeNum() {
        return ebCommandeNum;
    }

    public void setEbCommandeNum(Integer ebCommandeNum) {
        this.ebCommandeNum = ebCommandeNum;
    }

    public boolean isRoleControlTower() {
        return roleControlTower;
    }

    public void setRoleControlTower(boolean roleControlTower) {
        this.roleControlTower = roleControlTower;
    }

    public boolean isWithPassword() {
        return withPassword;
    }

    public void setWithPassword(boolean withPassword) {
        this.withPassword = withPassword;
    }

    public boolean isRoleChargeur() {
        return roleChargeur;
    }

    public void setRoleChargeur(boolean roleChargeur) {
        this.roleChargeur = roleChargeur;
    }

    public List<Integer> getChampListCategorie() {
        return champListCategorie;
    }

    public void setChampListCategorie(List<Integer> champListCategorie) {
        this.champListCategorie = champListCategorie;
    }

    public boolean isForChargeur() {
        return forChargeur;
    }

    public void setForChargeur(boolean forChargeur) {
        this.forChargeur = forChargeur;
    }

    public Map<String, String> getMapCustomFields() {
        return mapCustomFields;
    }

    public void setMapCustomFields(Map<String, String> mapCustomFields) {
        this.mapCustomFields = mapCustomFields;
    }

    public boolean isIgnoreContact() {
        return ignoreContact;
    }

    public void setIgnoreContact(boolean ignoreContact) {
        this.ignoreContact = ignoreContact;
    }

    public boolean isForControlTower() {
        return forControlTower;
    }

    public void setForControlTower(boolean forControlTower) {
        this.forControlTower = forControlTower;
    }

    public Integer getEbCompagnieGuestNum() {
        return ebCompagnieGuestNum;
    }

    public void setEbCompagnieGuestNum(Integer ebCompagnieGuestNum) {
        this.ebCompagnieGuestNum = ebCompagnieGuestNum;
    }

    public Integer getCurrencyCibleNum() {
        return currencyCibleNum;
    }

    public void setCurrencyCibleNum(Integer currencyCibleNum) {
        this.currencyCibleNum = currencyCibleNum;
    }

    public boolean isCompagnie() {
        return isCompagnie;
    }

    public void setCompagnie(boolean isCompagnie) {
        this.isCompagnie = isCompagnie;
    }

    public boolean isCommunityCompany() {
        return communityCompany;
    }

    public boolean isCommunityEstablishment() {
        return communityEstablishment;
    }

    public void setCommunityCompany(boolean communityCompany) {
        this.communityCompany = communityCompany;
    }

    public void setCommunityEstablishment(boolean communityEstablishment) {
        this.communityEstablishment = communityEstablishment;
    }

    public boolean isExcludeCt() {
        return excludeCt;
    }

    public void setExcludeCt(boolean excludeCt) {
        this.excludeCt = excludeCt;
    }

    public Boolean getGetAll() {
        return getAll;
    }

    public void setGetAll(Boolean getAll) {
        this.getAll = getAll;
    }

    public boolean isForListMarchandise() {
        return forListMarchandise;
    }

    public boolean isBoutonAction() {
        return boutonAction;
    }

    public void setBoutonAction(boolean boutonAction) {
        this.boutonAction = boutonAction;
    }

    public void setForListMarchandise(boolean forListMarchandise) {
        this.forListMarchandise = forListMarchandise;
    }

    public List<Map<String, String>> getDtConfig() {
        return dtConfig;
    }

    public void setDtConfig(List<Map<String, String>> dtConfig) {
        this.dtConfig = dtConfig;
    }

    public Map<String, List<Integer>> getMapCategoryFields() {
        return mapCategoryFields;
    }

    public void setMapCategoryFields(Map<String, List<Integer>> mapCategoryFields) {
        this.mapCategoryFields = mapCategoryFields;
    }

    public Date getExpectedDateTo() {
        return expectedDateTo;
    }

    public Date getExpectedDateFrom() {
        return expectedDateFrom;
    }

    public void setExpectedDateTo(Date expectedDateTo) {
        this.expectedDateTo = expectedDateTo;
    }

    public void setExpectedDateFrom(Date expectedDateFrom) {
        this.expectedDateFrom = expectedDateFrom;
    }

    public Date getFinalDeliveryTo() {
        return finalDeliveryTo;
    }

    public Date getFinalDeliveryFrom() {
        return finalDeliveryFrom;
    }

    public void setFinalDeliveryTo(Date finalDeliveryTo) {
        this.finalDeliveryTo = finalDeliveryTo;
    }

    public void setFinalDeliveryFrom(Date finalDeliveryFrom) {
        this.finalDeliveryFrom = finalDeliveryFrom;
    }

    public boolean isSimpleSearch() {
        return simpleSearch;
    }

    public void setSimpleSearch(boolean simpleSearch) {
        this.simpleSearch = simpleSearch;
    }

    public List<String> getListCostCenter() {
        return listCostCenter;
    }

    public void setListCostCenter(List<String> listCostCenter) {
        this.listCostCenter = listCostCenter;
    }

    public boolean isExtractData() {
        return extractData;
    }

    public void setExtractData(boolean extractData) {
        this.extractData = extractData;
    }

    public Boolean getDangerous() {
        return dangerous;
    }

    public void setDangerous(Boolean dangerous) {
        this.dangerous = dangerous;
    }

    public Integer getModeTransport() {
        return modeTransport;
    }

    public Integer getTypeDemande() {
        return typeDemande;
    }

    public void setModeTransport(Integer modeTransport) {
        this.modeTransport = modeTransport;
    }

    public void setTypeDemande(Integer typeDemande) {
        this.typeDemande = typeDemande;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public Integer getNbPackages() {
        return nbPackages;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public void setNbPackages(Integer nbPackages) {
        this.nbPackages = nbPackages;
    }

    public List<String> getListIncoterm() {
        return listIncoterm;
    }

    public void setListIncoterm(List<String> listIncoterm) {
        this.listIncoterm = listIncoterm;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<String> getAdresseEligibilities() {
        return adresseEligibilities;
    }

    public void setAdresseEligibilities(List<String> adresseEligibilities) {
        this.adresseEligibilities = adresseEligibilities;
    }

    public List<Long> getListGroupeNum() {
        return listGroupeNum;
    }

    public void setListGroupeNum(List<Long> listGroupeNum) {
        this.listGroupeNum = listGroupeNum;
    }

    public boolean isFromCommunity() {
        return fromCommunity;
    }

    public void setFromCommunity(boolean fromCommunity) {
        this.fromCommunity = fromCommunity;
    }

    public Integer getConnectedUserNum() {
        return connectedUserNum;
    }

    public void setConnectedUserNum(Integer connectedUserNum) {
        this.connectedUserNum = connectedUserNum;
    }

    public Integer getConnectedControlTower() {
        return connectedControlTower;
    }

    public void setConnectedControlTower(Integer connectedControlTower) {
        this.connectedControlTower = connectedControlTower;
    }

    public List<String> getListEmailsNotInPlatform() {
        return listEmailsNotInPlatform;
    }

    public void setListEmailsNotInPlatform(List<String> listEmailsNotInPlatform) {
        this.listEmailsNotInPlatform = listEmailsNotInPlatform;
    }

    public String getEmailUnknownUser() {
        return emailUnknownUser;
    }

    public void setEmailUnknownUser(String emailUnknownUser) {
        this.emailUnknownUser = emailUnknownUser;
    }

    public Integer getIdObject() {
        return idObject;
    }

    public void setIdObject(Integer idObject) {
        this.idObject = idObject;
    }

    public List<Integer> getListModule() {
        return listModule;
    }

    public void setListModule(List<Integer> listModule) {
        this.listModule = listModule;
    }

    public Integer getEbUserProfileNum() {
        return ebUserProfileNum;
    }

    public void setEbUserProfileNum(Integer ebUserProfileNum) {
        this.ebUserProfileNum = ebUserProfileNum;
    }

    public boolean isIncludeGlobalValues() {
        return includeGlobalValues;
    }

    public void setIncludeGlobalValues(boolean includeGlobalValues) {
        this.includeGlobalValues = includeGlobalValues;
    }

    public boolean isWithEtablissement() {
        return withEtablissement;
    }

    public void setWithEtablissement(boolean withEtablissement) {
        this.withEtablissement = withEtablissement;
    }

    public boolean isWithEtablissementDetail() {
        return withEtablissementDetail;
    }

    public void setWithEtablissementDetail(boolean withEtablissementDetail) {
        this.withEtablissementDetail = withEtablissementDetail;
    }

    public Integer getTypeDocumentNum() {
        return typeDocumentNum;
    }

    public void setTypeDocumentNum(Integer typeDocumentNum) {
        this.typeDocumentNum = typeDocumentNum;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public boolean showGlobals() {
        return showGlobals;
    }

    public void setShowGlobals(boolean showGlobals) {
        this.showGlobals = showGlobals;
    }

    public List<Integer> getListIdObject() {
        return listIdObject;
    }

    public void setListIdObject(List<Integer> listIdObject) {
        this.listIdObject = listIdObject;
    }

    public List<String> getListStatutStr() {
        return listStatutStr;
    }

    public void setListStatutStr(List<String> listStatutStr) {
        this.listStatutStr = listStatutStr;
    }

    public String getCompanyOrigin() {
        return companyOrigin;
    }

    public void setCompanyOrigin(String companyOrigin) {
        this.companyOrigin = companyOrigin;
    }

    public String getCarrierUniqRefNum() {
        return carrierUniqRefNum;
    }

    public void setCarrierUniqRefNum(String carrierUniqRefNum) {
        this.carrierUniqRefNum = carrierUniqRefNum;
    }

    public Integer getTypeContener() {
        return typeContener;
    }

    public boolean isWithListDemande() {
        return withListDemande;
    }

    public boolean isShowGlobals() {
        return showGlobals;
    }

    public void setTypeContener(Integer typeContener) {
        this.typeContener = typeContener;
    }

    public void setWithListDemande(boolean withListDemande) {
        this.withListDemande = withListDemande;
    }

    public List<Integer> getTransportConfirmation() {
        return transportConfirmation;
    }

    public void setTransportConfirmation(List<Integer> transportConfirmation) {
        this.transportConfirmation = transportConfirmation;
    }

    public List<Integer> getCompanyNums() {
        return companyNums;
    }

    public void setCompanyNums(List<Integer> companyNums) {
        this.companyNums = companyNums;
    }

    public Integer getUserNum() {
        return userNum;
    }

    public void setUserNum(Integer userNum) {
        this.userNum = userNum;
    }

    public Integer getUserProfilNum() {
        return userProfilNum;
    }

    public void setUserProfilNum(Integer userProfilNum) {
        this.userProfilNum = userProfilNum;
    }

    public Integer getGridVersion() {
        return gridVersion;
    }

    public void setGridVersion(Integer gridVersion) {
        this.gridVersion = gridVersion;
    }

    public Integer getCurrencyGuestNum() {
        return currencyGuestNum;
    }

    public void setCurrencyGuestNum(Integer currencyGuestNum) {
        this.currencyGuestNum = currencyGuestNum;
    }

    public Integer getEbCompagnieCibleNum() {
        return ebCompagnieCibleNum;
    }

    public void setEbCompagnieCibleNum(Integer ebCompagnieCibleNum) {
        this.ebCompagnieCibleNum = ebCompagnieCibleNum;
    }

    public String getNumAwbBol() {
        return numAwbBol;
    }

    public void setNumAwbBol(String numAwbBol) {
        this.numAwbBol = numAwbBol;
    }

    public List<String> getListCustomFieldsValues() {
        return listCustomFieldsValues;
    }

    public void setListCustomFieldsValues(List<String> listCustomFieldsValues) {
        this.listCustomFieldsValues = listCustomFieldsValues;
    }

    public List<Integer> getListOwnerOfTheRequests() {
        return listOwnerOfTheRequests;
    }

    public void setListOwnerOfTheRequests(List<Integer> listOwnerOfTheRequests) {
        this.listOwnerOfTheRequests = listOwnerOfTheRequests;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public List<Integer> getListPartyDestination() {
        return listPartyDestination;
    }

    public void setListPartyDestination(List<Integer> listPartyDestination) {
        this.listPartyDestination = listPartyDestination;
    }

    public Double getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(Double maxWeight) {
        this.maxWeight = maxWeight;
    }

    public Double getMaxVolume() {
        return maxVolume;
    }

    public void setMaxVolume(Double maxVolume) {
        this.maxVolume = maxVolume;
    }

    public List<Integer> getDangerousGood() {
        return dangerousGood;
    }

    public void setDangerousGood(List<Integer> dangerousGood) {
        this.dangerousGood = dangerousGood;
    }

    public Double getMaxHeight() {
        return maxHeight;
    }

    public void setMaxHeight(Double maxHeight) {
        this.maxHeight = maxHeight;
    }

    public Double getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(Double maxLength) {
        this.maxLength = maxLength;
    }

    public Double getMaxWidth() {
        return maxWidth;
    }

    public void setMaxWidth(Double maxWidth) {
        this.maxWidth = maxWidth;
    }

    public Double getClassGood() {
        return classGood;
    }

    public void setClassGood(Double classGood) {
        this.classGood = classGood;
    }

    public Double getMaxTaxableWeight() {
        return maxTaxableWeight;
    }

    public void setMaxTaxableWeight(Double maxTaxableWeight) {
        this.maxTaxableWeight = maxTaxableWeight;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public List<Double> getClassGoods() {
        return classGoods;
    }

    public void setClassGoods(List<Double> classGoods) {
        this.classGoods = classGoods;
    }

    public void setWithDeactive(Boolean withDeactive) {
        this.withDeactive = withDeactive;
    }

    public String getUns() {
        return uns;
    }

    public void setUns(String uns) {
        this.uns = uns;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public List<String> getListOriginCity() {
        return listOriginCity;
    }

    public void setListOriginCity(List<String> listOriginCity) {
        this.listOriginCity = listOriginCity;
    }

    public List<String> getListDestinationCity() {
        return listDestinationCity;
    }

    public void setListDestinationCity(List<String> listDestinationCity) {
        this.listDestinationCity = listDestinationCity;
    }

    public Integer getRuleCategory() {
        return ruleCategory;
    }

    public void setRuleCategory(Integer ruleCategory) {
        this.ruleCategory = ruleCategory;
    }

    public boolean isWithListMarchandise() {
        return withListMarchandise;
    }

    public void setWithListMarchandise(boolean withListMarchandise) {
        this.withListMarchandise = withListMarchandise;
    }

    public List<Integer> getListTransportPriority() {
        return listTransportPriority;
    }

    public void setListTransportPriority(List<Integer> listTransportPriority) {
        this.listTransportPriority = listTransportPriority;
    }

    public List<Integer> getListOfExistingTransporteur() {
        return listOfExistingTransporteur;
    }

    public void setListOfExistingTransporteur(List<Integer> listOfExistingTransporteur) {
        this.listOfExistingTransporteur = listOfExistingTransporteur;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Integer getEbZoneNum() {
        return ebZoneNum;
    }

    public void setEbZoneNum(Integer ebZoneNum) {
        this.ebZoneNum = ebZoneNum;
    }

    public Long getEbDelOrderNum() {
        return ebDelOrderNum;
    }

    public void setEbDelOrderNum(Long ebDelOrderNum) {
        this.ebDelOrderNum = ebDelOrderNum;
    }

    public boolean isWithActiveUsers() {
        return withActiveUsers;
    }

    public void setWithActiveUsers(boolean withActiveUsers) {
        this.withActiveUsers = withActiveUsers;
    }

    public Integer getCompanyRole() {
        return companyRole;
    }

    public void setCompanyRole(Integer companyRole) {
        this.companyRole = companyRole;
    }

	public List<Integer> getListOrderStatus() {
		return listOrderStatus;
	}

	public void setListOrderStatus(List<Integer> listOrderStatus) {
		this.listOrderStatus = listOrderStatus;
	}

	public List<String> getListcustomerOrderReference() {
		return listcustomerOrderReference;
	}

	public void setListcustomerOrderReference(List<String> listcustomerOrderReference) {
		this.listcustomerOrderReference = listcustomerOrderReference;
	}

	public EbUser getxEbOwnerOfTheRequest() {
		return xEbOwnerOfTheRequest;
	}

	public void setxEbOwnerOfTheRequest(EbUser xEbOwnerOfTheRequest) {
		this.xEbOwnerOfTheRequest = xEbOwnerOfTheRequest;
	}

	public List<Integer> getListStatusDelivery() {
		return listStatusDelivery;
	}

	public void setListStatusDelivery(List<Integer> listStatusDelivery) {
		this.listStatusDelivery = listStatusDelivery;
	}
}
