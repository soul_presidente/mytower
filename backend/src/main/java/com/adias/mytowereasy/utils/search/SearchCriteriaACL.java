package com.adias.mytowereasy.utils.search;

public class SearchCriteriaACL extends SearchCriteria {
    private Integer ebUserProfileNum;
    private Integer ebUserNum;

    public Integer getEbUserProfileNum() {
        return ebUserProfileNum;
    }

    public void setEbUserProfileNum(Integer ebUserProfileNum) {
        this.ebUserProfileNum = ebUserProfileNum;
    }

    public Integer getEbUserNum() {
        return ebUserNum;
    }

    public void setEbUserNum(Integer ebUserNum) {
        this.ebUserNum = ebUserNum;
    }
}
