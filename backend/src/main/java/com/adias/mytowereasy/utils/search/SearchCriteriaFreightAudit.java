/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.utils.search;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.adias.mytowereasy.model.CustomFields;


public class SearchCriteriaFreightAudit extends SearchCriteria {
    private Integer ebInvoiceNum;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listOrigineCountry;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listModeTransportF;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listTypeDemande;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listStatue;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listDestCountry;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Integer statutChargeur;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Integer statutCarrier;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listNumAwbBol;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listEbUserOwnerRequest;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listChargeur;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private BigDecimal preCarrierCost;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Integer invoiceCurrency;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private String applyCarrierIvoiceNumber;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date invoiceDateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date invoiceDateTo;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private String exchangeRate;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listInfosDocsStatus;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dateDelivery;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date datePickup;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private BigDecimal gap;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private BigDecimal priceCharged;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private String memo;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listPriority;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listEtablissementNum;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listCompanyName;

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public BigDecimal getPriceCharged() {
        return priceCharged;
    }

    public void setPriceCharged(BigDecimal priceCharged) {
        this.priceCharged = priceCharged;
    }

    public BigDecimal getGap() {
        return gap;
    }

    public void setGap(BigDecimal gap) {
        this.gap = gap;
    }

    public Date getDatePickup() {
        return datePickup;
    }

    public void setDatePickup(Date datePickup) {
        this.datePickup = datePickup;
    }

    public Date getDateDelivery() {
        return dateDelivery;
    }

    public void setDateDelivery(Date dateDelivery) {
        this.dateDelivery = dateDelivery;
    }

    public List<Integer> getListInfosDocsStatus() {
        return listInfosDocsStatus;
    }

    public void setListInfosDocsStatus(List<Integer> listInfosDocsStatus) {
        this.listInfosDocsStatus = listInfosDocsStatus;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getApplyCarrierIvoiceNumber() {
        return applyCarrierIvoiceNumber;
    }

    public void setApplyCarrierIvoiceNumber(String applyCarrierIvoiceNumber) {
        this.applyCarrierIvoiceNumber = applyCarrierIvoiceNumber;
    }

    public Integer getInvoiceCurrency() {
        return invoiceCurrency;
    }

    public void setInvoiceCurrency(Integer invoiceCurrency) {
        this.invoiceCurrency = invoiceCurrency;
    }

    public BigDecimal getPreCarrierCost() {
        return preCarrierCost;
    }

    public void setPreCarrierCost(BigDecimal preCarrierCost) {
        this.preCarrierCost = preCarrierCost;
    }

    public List<String> getListNumAwbBol() {
        return listNumAwbBol;
    }

    public void setListNumAwbBol(List<String> listNumAwbBol) {
        this.listNumAwbBol = listNumAwbBol;
    }

    public Integer getStatutChargeur() {
        return statutChargeur;
    }

    public void setStatutChargeur(Integer statutChargeur) {
        this.statutChargeur = statutChargeur;
    }

    public Integer getStatutCarrier() {
        return statutCarrier;
    }

    public void setStatutCarrier(Integer statutCarrier) {
        this.statutCarrier = statutCarrier;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dateDeliveryTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dateDeliveryFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private String refDemande;

    public SearchCriteriaFreightAudit() {
        super();
    }

    public Integer getEbInvoiceNum() {
        return ebInvoiceNum;
    }

    public void setEbInvoiceNum(Integer ebInvoiceNum) {
        this.ebInvoiceNum = ebInvoiceNum;
    }

    public void setSearchInput(String searchInput, List<CustomFields> customsFields) throws JsonParseException, JsonMappingException, IOException {
        if (searchInput == null) searchInput = "{}";
        super.setSearchInput(searchInput, customsFields);

        this.listOrigineCountry = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).listOrigineCountry;
        this.listTypeDemande = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).listTypeDemande;
        this.listModeTransportF = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).listModeTransportF;
        this.dateDeliveryFrom = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).dateDeliveryFrom;
        this.dateDeliveryTo = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).dateDeliveryTo;
        this.refDemande = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).refDemande;
        this.listDestCountry = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).listDestCountry;
        this.datePickUpFrom = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).datePickUpFrom;
        this.datePickUpTo = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).datePickUpTo;
        this.listCarrier = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).listCarrier;
        this.statutChargeur = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).statutChargeur;
        this.statutCarrier = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).statutCarrier;
        this.listIncoterm = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).listIncoterm;
        this.listNumAwbBol = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).listNumAwbBol;
        this.listEbUserOwnerRequest = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).listEbUserOwnerRequest;
        this.preCarrierCost = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).preCarrierCost;
        this.invoiceCurrency = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).invoiceCurrency;
        this.applyCarrierIvoiceNumber = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).applyCarrierIvoiceNumber;
        this.invoiceDateFrom = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).invoiceDateFrom;
        this.invoiceDateTo = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).invoiceDateTo;
        this.listInfosDocsStatus = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).listInfosDocsStatus;
        this.dateDelivery = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).dateDelivery;
        this.datePickup = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).datePickup;
        this.exchangeRate = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).exchangeRate;
        this.gap = ((SearchCriteriaFreightAudit) deserialize(this.searchInput, SearchCriteriaFreightAudit.class)).gap;
        this.priceCharged = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).priceCharged;
        this.memo = ((SearchCriteriaFreightAudit) deserialize(this.searchInput, SearchCriteriaFreightAudit.class)).memo;

        this.listEtablissementNum = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).listEtablissementNum;

        this.listCompanyName = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).listCompanyName;

        this.listPriority = ((SearchCriteriaFreightAudit) deserialize(
            this.searchInput,
            SearchCriteriaFreightAudit.class)).listPriority;

        this.listChargeur = ((SearchCriteriaFreightAudit) deserialize(
            searchInput,
            SearchCriteriaFreightAudit.class)).listChargeur;
    }

    public List<String> getListDestCountry() {
        return listDestCountry;
    }

    public void setListDestCountry(List<String> listDestCountry) {
        this.listDestCountry = listDestCountry;
    }

    public List<String> getListOrigineCountry() {
        return listOrigineCountry;
    }

    public void setListOrigineCountry(List<String> listOrigineCountry) {
        this.listOrigineCountry = listOrigineCountry;
    }

    public List<Integer> getListModeTransportF() {
        return listModeTransportF;
    }

    public void setListModeTransportF(List<Integer> listModeTransportF) {
        this.listModeTransportF = listModeTransportF;
    }

    public List<String> getListTypeDemande() {
        return listTypeDemande;
    }

    public void setListTypeDemande(List<String> listTypeDemande) {
        this.listTypeDemande = listTypeDemande;
    }

    public List<Integer> getListStatue() {
        return listStatue;
    }

    public void setListStatue(List<Integer> listStatue) {
        this.listStatue = listStatue;
    }

    public Date getDateDeliveryTo() {
        return dateDeliveryTo;
    }

    public void setDateDeliveryTo(Date dateDeliveryTo) {
        this.dateDeliveryTo = dateDeliveryTo;
    }

    public Date getDateDeliveryFrom() {
        return dateDeliveryFrom;
    }

    public void setDateDeliveryFrom(Date dateDeliveryFrom) {
        this.dateDeliveryFrom = dateDeliveryFrom;
    }

    public String getRefDemande() {
        return refDemande;
    }

    public void setRefDemande(String refDemande) {
        this.refDemande = refDemande;
    }

    public List<Integer> getListEbUserOwnerRequest() {
        return listEbUserOwnerRequest;
    }

    public void setListEbUserOwnerRequest(List<Integer> listEbUserOwnerRequest) {
        this.listEbUserOwnerRequest = listEbUserOwnerRequest;
    }

    public List<String> getListEtablissementNum() {
        return listEtablissementNum;
    }

    public void setListEtablissementNum(List<String> listEtablissementNum) {
        this.listEtablissementNum = listEtablissementNum;
    }

    public List<String> getListCompanyName() {
        return listCompanyName;
    }

    public void setListCompanyName(List<String> listCompanyName) {
        this.listCompanyName = listCompanyName;
    }

    public Date getInvoiceDateFrom() {
        return invoiceDateFrom;
    }

    public void setInvoiceDateFrom(Date invoiceDateFrom) {
        this.invoiceDateFrom = invoiceDateFrom;
    }

    public Date getInvoiceDateTo() {
        return invoiceDateTo;
    }

    public void setInvoiceDateTo(Date invoiceDateTo) {
        this.invoiceDateTo = invoiceDateTo;
    }

    public List<String> getListPriority() {
        return listPriority;
    }

    public void setListPriority(List<String> listPriority) {
        this.listPriority = listPriority;
    }

    public List<Integer> getListChargeur() {
        return listChargeur;
    }

    public void setListChargeur(List<Integer> listChargeur) {
        this.listChargeur = listChargeur;
    }
}
