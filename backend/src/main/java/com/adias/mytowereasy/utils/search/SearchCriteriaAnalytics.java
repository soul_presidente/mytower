/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.utils.search;

/**
 * ********** CETTE CLASSE EST POUR UNE EVOLUTION FUTURE, A NE PAS UTILISER
 * ****************
 */
public class SearchCriteriaAnalytics {
    private String type;
    private String key;
    private String value;
    private String operation;
    private String table;

    public SearchCriteriaAnalytics(String type, String key, String operation, String value, String table) {
        this.type = type;
        this.key = key;
        this.value = value;
        this.operation = operation;
        this.table = table;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }
}
