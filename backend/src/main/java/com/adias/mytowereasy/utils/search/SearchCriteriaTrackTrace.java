/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.utils.search;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.adias.mytowereasy.model.CustomFields;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchCriteriaTrackTrace extends SearchCriteria {
    private Integer ebTtTracingNum;

    private boolean searchDemandeByMarchandiseData;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listEbTtTracingNum;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCustomerRefAndtransportRef;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listTransportStatus;

    private Integer late;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listunitsPackingList;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listNumAwbBol;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> libelleLastPsl;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date dateLastPsl;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date finalDelivery;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date datePickupTM;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listUnitReference;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listEbUserCt;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listUser;

    public Integer getEbTtTracingNum() {
        return ebTtTracingNum;
    }

    public void setEbTtTracingNum(Integer ebTtTracingNum) {
        this.ebTtTracingNum = ebTtTracingNum;
    }

    public List<String> getListCustomerRefAndtransportRef() {
        return listCustomerRefAndtransportRef;
    }

    public void setListCustomerRefAndtransportRef(List<String> listCustomerRefAndtransportRef) {
        this.listCustomerRefAndtransportRef = listCustomerRefAndtransportRef;
    }

    /*
     * public List<Integer> getListModeTransport() { return listModeTransport; }
     * public void setListModeTransport(List<Integer> listModeTransport) {
     * this.listModeTransport = listModeTransport; }
     */

    public void setSearchInput(String searchInput, List<CustomFields> customsFields) throws JsonParseException, JsonMappingException, IOException {
        if (searchInput == null) searchInput = "{}";

        super.setSearchInput(searchInput, customsFields);

        this.listCustomerRefAndtransportRef = ((SearchCriteriaTrackTrace) deserialize(
            searchInput,
            SearchCriteriaTrackTrace.class)).listCustomerRefAndtransportRef;
        this.listModeTransport = ((SearchCriteriaTrackTrace) deserialize(
            searchInput,
            SearchCriteriaTrackTrace.class)).listModeTransport;

        this.listTransportStatus = ((SearchCriteriaTrackTrace) deserialize(
            searchInput,
            SearchCriteriaTrackTrace.class)).listTransportStatus;

        this.listUnitReference = ((SearchCriteriaTrackTrace) deserialize(
            searchInput,
            SearchCriteriaTrackTrace.class)).listUnitReference;

        this.late = ((SearchCriteriaTrackTrace) deserialize(this.searchInput, SearchCriteriaTrackTrace.class)).late;

        this.listunitsPackingList = ((SearchCriteriaTrackTrace) deserialize(
            this.searchInput,
            SearchCriteriaTrackTrace.class)).listunitsPackingList;

        this.libelleLastPsl = ((SearchCriteriaTrackTrace) deserialize(
            this.searchInput,
            SearchCriteriaTrackTrace.class)).libelleLastPsl;

        this.dateLastPsl = ((SearchCriteriaTrackTrace) deserialize(
            this.searchInput,
            SearchCriteriaTrackTrace.class)).dateLastPsl;

        this.datePickupTM = ((SearchCriteriaTrackTrace) deserialize(
            this.searchInput,
            SearchCriteriaTrackTrace.class)).datePickupTM;

        this.finalDelivery = ((SearchCriteriaTrackTrace) deserialize(
            this.searchInput,
            SearchCriteriaTrackTrace.class)).finalDelivery;

        this.totalWeight = ((SearchCriteriaTrackTrace) deserialize(
            this.searchInput,
            SearchCriteriaTrackTrace.class)).totalWeight;

        this.listNumAwbBol = ((SearchCriteriaTrackTrace) deserialize(
            this.searchInput,
            SearchCriteriaTrackTrace.class)).listNumAwbBol;

        this.listEbUserCt = ((SearchCriteriaTrackTrace) deserialize(
            this.searchInput,
            SearchCriteriaTrackTrace.class)).listEbUserCt;

        this.listUser = ((SearchCriteriaTrackTrace) deserialize(
            this.searchInput,
            SearchCriteriaTrackTrace.class)).listUser;
    }

    public List<Integer> getListEbTtTracingNum() {
        return listEbTtTracingNum;
    }

    public void setListEbTtTracingNum(List<Integer> listEbTtTracingNum) {
        this.listEbTtTracingNum = listEbTtTracingNum;
    }

    public List<Integer> getListTransportStatus() {
        return listTransportStatus;
    }

    public void setListTransportStatus(List<Integer> listTransportStatus) {
        this.listTransportStatus = listTransportStatus;
    }

    public boolean isSearchDemandeByMarchandiseData() {
        return searchDemandeByMarchandiseData;
    }

    public void setSearchDemandeByMarchandiseData(boolean searchDemandeByMarchandiseData) {
        this.searchDemandeByMarchandiseData = searchDemandeByMarchandiseData;
    }

    public List<String> getListUnitReference() {
        return listUnitReference;
    }

    public void setListUnitReference(List<String> listUnitReference) {
        this.listUnitReference = listUnitReference;
    }

    public Integer getLate() {
        return late;
    }

    public void setLate(Integer late) {
        this.late = late;
    }

    public List<String> getListunitsPackingList() {
        return listunitsPackingList;
    }

    public void setListunitsPackingList(List<String> listunitsPackingList) {
        this.listunitsPackingList = listunitsPackingList;
    }

    public List<String> getLibelleLastPsl() {
        return libelleLastPsl;
    }

    public void setLibelleLastPsl(List<String> libelleLastPsl) {
        this.libelleLastPsl = libelleLastPsl;
    }

    public Date getDateLastPsl() {
        return dateLastPsl;
    }

    public void setDateLastPsl(Date dateLastPsl) {
        this.dateLastPsl = dateLastPsl;
    }

    public Date getDatePickupTM() {
        return datePickupTM;
    }

    public void setDatePickupTM(Date datePickupTM) {
        this.datePickupTM = datePickupTM;
    }

    public Date getFinalDelivery() {
        return finalDelivery;
    }

    public void setFinalDelivery(Date finalDelivery) {
        this.finalDelivery = finalDelivery;
    }

    public List<String> getListNumAwbBol() {
        return listNumAwbBol;
    }

    public void setListNumAwbBol(List<String> listNumAwbBol) {
        this.listNumAwbBol = listNumAwbBol;
    }

    public List<Integer> getListEbUserCt() {
        return listEbUserCt;
    }

    public void setListEbUserCt(List<Integer> listEbUserCt) {
        this.listEbUserCt = listEbUserCt;
    }

    public List<Integer> getListUser() {
        return listUser;
    }

    public void setListUser(List<Integer> listUser) {
        this.listUser = listUser;
    }
}
