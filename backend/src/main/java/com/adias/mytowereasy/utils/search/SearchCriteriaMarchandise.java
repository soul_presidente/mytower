package com.adias.mytowereasy.utils.search;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.QEbMarchandise;


public class SearchCriteriaMarchandise extends SearchCriteria {
    private Map<String, OrderSpecifier<?>> marchandiseMapASC = new HashMap<String, OrderSpecifier<?>>();
    QEbMarchandise qEbMarchandise = QEbMarchandise.ebMarchandise;

    private String unitReference;

    private String lot;

    private void Initialize(QEbMarchandise qEbMarchandise) {
        marchandiseMapASC.clear();
        marchandiseMapASC.put("unitReference", qEbMarchandise.unitReference.asc());
        marchandiseMapASC.put("lot", qEbMarchandise.lot.asc());
    }

    public JPAQuery<EbMarchandise> applyCriteriaAutoComplete(JPAQuery<EbMarchandise> query) {
        JPAQuery<EbMarchandise> resultQuery = query;

        Initialize(qEbMarchandise);

        if (this.unitReference != null) {
            BooleanBuilder w = new BooleanBuilder();
            w
                .andAnyOf(
                    qEbMarchandise.unitReference.toLowerCase().contains(this.unitReference),
                    qEbMarchandise.unitReference.contains(this.unitReference));
            resultQuery.where(w);
        }

        if (this.lot != null) {
            BooleanBuilder w = new BooleanBuilder();
            w.andAnyOf(qEbMarchandise.lot.toLowerCase().contains(this.lot), qEbMarchandise.lot.contains(this.lot));
            resultQuery.where(w);
        }

        if (this.orderedColumn != null) {
            resultQuery.orderBy(this.marchandiseMapASC.get(this.orderedColumn));
        }
        else {
            this.orderedColumn = "ebMarchandiseNum";
            resultQuery.orderBy(this.marchandiseMapASC.get(this.orderedColumn));
        }

        return resultQuery;
    }

    public SearchCriteriaMarchandise setProperty(String property, String value, SearchCriteriaMarchandise criteria) {
        Field field = null;

        try {
            Class<?> c = criteria.getClass();
            field = c.getDeclaredField(property);

            try {
                field.set(criteria, value);
                criteria.setOrderedColumn(property);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }

        } catch (NoSuchFieldException e1) {
            e1.printStackTrace();
        }

        return criteria;
    }

    public String getUnitReference() {
        return unitReference;
    }

    public String getLot() {
        return lot;
    }

    public void setUnitReference(String unitReference) {
        this.unitReference = unitReference;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }
}
