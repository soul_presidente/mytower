package com.adias.mytowereasy.utils.search;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;

import com.adias.mytowereasy.cronjob.tt.model.QEdiMapPsl;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCompagnie;


public class SearchCriteriaMappingCodePsl extends SearchCriteria {
    BooleanBuilder w = new BooleanBuilder();

    private Integer ediMapPslNum;
    private EbCompagnie xEbCompany;
    private EbCompagnie xEbCompanyTransporteur;
    private String codePslTransporteur;
    private String codePslNormalise;
    private Integer typeEvent;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Integer numCodeRaison;
    private String codeRaison;
    private String categorieDeviationLibelle;
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Integer numCodeTransporteur;
    private String codePslTransporteurDescriptif;
    private String codeRaisonDescriptif;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCodePslTransporteur;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCodePslNormalise;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listTypeEvent;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCodeRaison;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCategorieDeviationLibelle;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCodePslTransporteurDescriptif;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCodeRaisonDescriptif;

    private Map<String, OrderSpecifier<?>> orderMapASC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, OrderSpecifier<?>> orderMapDESC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, Path<?>> orderMap = new HashMap<String, Path<?>>();

    private QEdiMapPsl qEdiMapPsl = new QEdiMapPsl("ediMapPsl");

    private boolean isCount = false;

    public void Initialize(QEdiMapPsl qEdiMapPsl) {
        orderMapASC.clear();
        orderMap.clear();
        orderMapDESC.clear();

        orderMap.put("ediMapPslNum", qEdiMapPsl.ediMapPslNum);
        orderMapASC.put("ediMapPslNum", qEdiMapPsl.ediMapPslNum.asc());
        orderMapASC.put("ediMapPslNum", qEdiMapPsl.ediMapPslNum.desc());

        orderMap.put("codePslTransporteur", qEdiMapPsl.codePslTransporteur);
        orderMapASC.put("codePslTransporteur", qEdiMapPsl.codePslTransporteur.asc());
        orderMapASC.put("codePslTransporteur", qEdiMapPsl.codePslTransporteur.desc());

        orderMap.put("codePslNormalise", qEdiMapPsl.codePslNormalise);
        orderMapASC.put("codePslNormalise", qEdiMapPsl.codePslNormalise.asc());
        orderMapDESC.put("codePslNormalise", qEdiMapPsl.codePslNormalise.desc());

        orderMap.put("typeEvent", qEdiMapPsl.typeEvent);
        orderMapASC.put("typeEvent", qEdiMapPsl.typeEvent.asc());
        orderMapDESC.put("typeEvent", qEdiMapPsl.typeEvent.desc());

        orderMap.put("numCodeRaison", qEdiMapPsl.numCodeRaison);
        orderMapASC.put("numCodeRaison", qEdiMapPsl.numCodeRaison.asc());
        orderMapDESC.put("numCodeRaison", qEdiMapPsl.numCodeRaison.desc());

        orderMap.put("codeRaison", qEdiMapPsl.codeRaison);
        orderMapASC.put("codeRaison", qEdiMapPsl.codeRaison.asc());
        orderMapDESC.put("codeRaison", qEdiMapPsl.codeRaison.desc());

        orderMap.put("categorieDeviationLibelle", qEdiMapPsl.categorieDeviationLibelle);
        orderMapASC.put("categorieDeviationLibelle", qEdiMapPsl.categorieDeviationLibelle.asc());
        orderMapDESC.put("categorieDeviationLibelle", qEdiMapPsl.categorieDeviationLibelle.desc());

        orderMap.put("numCodeTransporteur", qEdiMapPsl.numCodeTransporteur);
        orderMapASC.put("numCodeTransporteur", qEdiMapPsl.numCodeTransporteur.asc());
        orderMapDESC.put("numCodeTransporteur", qEdiMapPsl.numCodeTransporteur.desc());

        orderMap.put("codePslTransporteurDescriptif", qEdiMapPsl.codePslTransporteurDescriptif);
        orderMapASC.put("codePslTransporteurDescriptif", qEdiMapPsl.codePslTransporteurDescriptif.asc());
        orderMapDESC.put("codePslTransporteurDescriptif", qEdiMapPsl.codePslTransporteurDescriptif.desc());

        orderMap.put("codeRaisonDescriptif", qEdiMapPsl.codeRaisonDescriptif);
        orderMapASC.put("codeRaisonDescriptif", qEdiMapPsl.codeRaisonDescriptif.asc());
        orderMapDESC.put("codeRaisonDescriptif", qEdiMapPsl.codeRaisonDescriptif.desc());
    }

    public SearchCriteriaMappingCodePsl() {
        super();
    }

    public String getCodePslTransporteur() {
        return codePslTransporteur;
    }

    public void setCodePslTransporteur(String codePslTransporteur) {
        this.codePslTransporteur = codePslTransporteur;
    }

    public String getCodePslNormalise() {
        return codePslNormalise;
    }

    public void setCodePslNormalise(String codePslNormalise) {
        this.codePslNormalise = codePslNormalise;
    }

    public Integer getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(Integer typeEvent) {
        this.typeEvent = typeEvent;
    }

    public Integer getNumCodeRaison() {
        return numCodeRaison;
    }

    public void setNumCodeRaison(Integer numCodeRaison) {
        this.numCodeRaison = numCodeRaison;
    }

    public String getCodeRaison() {
        return codeRaison;
    }

    public void setCodeRaison(String codeRaison) {
        this.codeRaison = codeRaison;
    }

    public String getCategorieDeviationLibelle() {
        return categorieDeviationLibelle;
    }

    public void setCategorieDeviationLibelle(String categorieDeviationLibelle) {
        this.categorieDeviationLibelle = categorieDeviationLibelle;
    }

    public Integer getNumCodeTransporteur() {
        return numCodeTransporteur;
    }

    public void setNumCodeTransporteur(Integer numCodeTransporteur) {
        this.numCodeTransporteur = numCodeTransporteur;
    }

    public String getCodePslTransporteurDescriptif() {
        return codePslTransporteurDescriptif;
    }

    public void setCodePslTransporteurDescriptif(String codePslTransporteurDescriptif) {
        this.codePslTransporteurDescriptif = codePslTransporteurDescriptif;
    }

    public String getCodeRaisonDescriptif() {
        return codeRaisonDescriptif;
    }

    public void setCodeRaisonDescriptif(String codeRaisonDescriptif) {
        this.codeRaisonDescriptif = codeRaisonDescriptif;
    }

    public boolean isCount() {
        return isCount;
    }

    public void setCount(boolean isCount) {
        this.isCount = isCount;
    }

    public List<String> getListCodePslTransporteur() {
        return listCodePslTransporteur;
    }

    public void setListCodePslTransporteur(List<String> listCodePslTransporteur) {
        this.listCodePslTransporteur = listCodePslTransporteur;
    }

    public List<String> getListCodePslNormalise() {
        return listCodePslNormalise;
    }

    public void setListCodePslNormalise(List<String> listCodePslNormalise) {
        this.listCodePslNormalise = listCodePslNormalise;
    }

    public List<Integer> getListTypeEvent() {
        return listTypeEvent;
    }

    public void setListTypeEvent(List<Integer> listTypeEvent) {
        this.listTypeEvent = listTypeEvent;
    }

    public List<String> getListCodeRaison() {
        return listCodeRaison;
    }

    public void setListCodeRaison(List<String> listCodeRaison) {
        this.listCodeRaison = listCodeRaison;
    }

    public List<String> getListCategorieDeviationLibelle() {
        return listCategorieDeviationLibelle;
    }

    public void setListCategorieDeviationLibelle(List<String> listCategorieDeviationLibelle) {
        this.listCategorieDeviationLibelle = listCategorieDeviationLibelle;
    }

    public List<String> getListCodePslTransporteurDescriptif() {
        return listCodePslTransporteurDescriptif;
    }

    public void setListCodePslTransporteurDescriptif(List<String> listCodePslTransporteurDescriptif) {
        this.listCodePslTransporteurDescriptif = listCodePslTransporteurDescriptif;
    }

    public List<String> getListCodeRaisonDescriptif() {
        return listCodeRaisonDescriptif;
    }

    public void setListCodeRaisonDescriptif(List<String> listCodeRaisonDescriptif) {
        this.listCodeRaisonDescriptif = listCodeRaisonDescriptif;
    }

    public Integer getEdiMapPslNum() {
        return ediMapPslNum;
    }

    public void setEdiMapPslNum(Integer ediMapPslNum) {
        this.ediMapPslNum = ediMapPslNum;
    }

    public EbCompagnie getxEbCompany() {
        return xEbCompany;
    }

    public void setxEbCompany(EbCompagnie xEbCompany) {
        this.xEbCompany = xEbCompany;
    }

    public EbCompagnie getxEbCompanyTransporteur() {
        return xEbCompanyTransporteur;
    }

    public void setxEbCompanyTransporteur(EbCompagnie xEbCompanyTransporteur) {
        this.xEbCompanyTransporteur = xEbCompanyTransporteur;
    }

    public void setSearchInput(String searchInput, List<CustomFields> customsFields) throws JsonParseException, JsonMappingException, IOException {
        if (searchInput == null) searchInput = "{}";

        super.setSearchInput(searchInput, customsFields);

        SearchCriteriaMappingCodePsl criteria = (SearchCriteriaMappingCodePsl) deserialize(
            this.searchInput,
            SearchCriteriaMappingCodePsl.class);

        this.listCodePslTransporteur = ((SearchCriteriaMappingCodePsl) deserialize(
            searchInput,
            SearchCriteriaMappingCodePsl.class)).listCodePslTransporteur;

        this.listCodePslNormalise = ((SearchCriteriaMappingCodePsl) deserialize(
            searchInput,
            SearchCriteriaMappingCodePsl.class)).listCodePslNormalise;

        this.listTypeEvent = ((SearchCriteriaMappingCodePsl) deserialize(
            searchInput,
            SearchCriteriaMappingCodePsl.class)).listTypeEvent;

        this.numCodeRaison = ((SearchCriteriaMappingCodePsl) deserialize(
            searchInput,
            SearchCriteriaMappingCodePsl.class)).numCodeRaison;

        this.listCodeRaison = ((SearchCriteriaMappingCodePsl) deserialize(
            searchInput,
            SearchCriteriaMappingCodePsl.class)).listCodeRaison;

        this.listCategorieDeviationLibelle = ((SearchCriteriaMappingCodePsl) deserialize(
            searchInput,
            SearchCriteriaMappingCodePsl.class)).listCategorieDeviationLibelle;

        this.numCodeTransporteur = ((SearchCriteriaMappingCodePsl) deserialize(
            searchInput,
            SearchCriteriaMappingCodePsl.class)).numCodeTransporteur;

        this.listCodePslTransporteurDescriptif = ((SearchCriteriaMappingCodePsl) deserialize(
            searchInput,
            SearchCriteriaMappingCodePsl.class)).listCodePslTransporteurDescriptif;

        this.listCodeRaisonDescriptif = ((SearchCriteriaMappingCodePsl) deserialize(
            searchInput,
            SearchCriteriaMappingCodePsl.class)).listCodeRaisonDescriptif;

        this.listCodePslTransporteur = criteria.listCodePslTransporteur;
        this.listCodePslNormalise = criteria.listCodePslNormalise;
        this.listTypeEvent = criteria.listTypeEvent;
        this.numCodeRaison = criteria.numCodeRaison;
        this.listCodeRaison = criteria.listCodeRaison;
        this.listCategorieDeviationLibelle = criteria.listCategorieDeviationLibelle;
        this.numCodeTransporteur = criteria.numCodeTransporteur;
        this.listCodePslTransporteurDescriptif = criteria.listCodePslTransporteurDescriptif;
        this.listCodeRaisonDescriptif = criteria.listCodeRaisonDescriptif;
    }

    public Map<String, OrderSpecifier<?>> getOrderMapASC() {
        return orderMapASC;
    }
}
