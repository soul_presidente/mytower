/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.utils.search;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.adias.mytowereasy.model.CustomFields;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SearchCriteriaPricingBooking extends SearchCriteria {
    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listTypeDemande;

    private List<Integer> listEbDemandeNum;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dateOfGoodsAvailability;
    private boolean forEmail;
    private boolean forCotation;
    private boolean flagQuoteCtRequired;
    private boolean flagEbtrackTrace;
    private boolean flagDateTraitementCPB;
    private boolean integratedVia;
    private boolean integratedMarchandiseWithoutParent;
    private boolean generateTrackTtrace;
    private Integer statutGroupage;
    private Integer xEcNature;
    private Integer xEcStatut;
    private String transporterEmail;
    private String transporterCompanyCode;
    private Boolean isValorized;
    private Boolean isGrouping;
    private Integer insurance;
    private Integer invoiceCurrency;
    private Double totalTaxableWeight;

    // EbLivraison
    private boolean searchDemandeByLivraisonData;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date requestedDeliveryDateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date requestedDeliveryDateTo;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listNumOrderSAP;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listNumOrderEDI;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listNumOrderCustomer;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listCampaignCode;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listCampaignName;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listReferenceDelivery;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listRefCustomerDelivery;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listNameCustomerDelivery;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listTypeFlux;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listUnitsReference;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listunitsPackingList;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listIncotermLibelle;

    // EbMarchandise
    private boolean searchDemandeByMarchandiseData;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listUnitReference;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listLot;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Integer transportInfostatus;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Integer customDocumentStatus;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private Integer transportDocumentStatus;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listInfosDocsStatus;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listNumAwbBol;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listCarrierUniqRefNum;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listEbCostCenter;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listEbUserCt;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listUser;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date updateDateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date updateDateTo;

    private Integer acknoledgeByCt;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date acknoledgeByCtDate;

    private Integer eligibleForConsolidation;
    private Boolean searchByWS;

    private BigDecimal price;

    private String libelleLastPsl;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date customerCreationDateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date customerCreationDateTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dateOfGoodsAvailabilityFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dateOfGoodsAvailabilityTo;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listEtablissementNum;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listCompanyName;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<String> listEmailTransporteur;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date datePickupTMFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date datePickupTMTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date waitingForQuoteDateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date waitingForQuoteDateTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date waitingForConfDateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date waitingForConfDateTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date confirmationDateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date confirmationDateTo;

    private Double totalVolume;
    private Double total_weight;
    private String xEbSchemaPslDesignation;

    private String incotermCity;

    private Double totalNbrParcel;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listValuationType;

    public SearchCriteriaPricingBooking() {
        this.forEmail = false;
        this.forCotation = false;
        this.flagQuoteCtRequired = false;
        this.flagEbtrackTrace = false;
    }

    public boolean isForEmail() {
        return forEmail;
    }

    public void setForEmail(boolean forEmail) {
        this.forEmail = forEmail;
    }

    public void setSearchInput(String searchInput, List<CustomFields> customsFields) throws JsonParseException, JsonMappingException, IOException {
        if (searchInput == null) searchInput = "{}";

        super.setSearchInput(searchInput, customsFields);

        this.listTypeDemande = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listTypeDemande;
        this.dateOfGoodsAvailability = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).dateOfGoodsAvailability;

        this.transportInfostatus = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).transportInfostatus;
        this.statutGroupage = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).statutGroupage;
        this.customDocumentStatus = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).customDocumentStatus;
        this.transportDocumentStatus = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).transportDocumentStatus;
        this.listTypeFlux = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listTypeFlux;
        this.listInfosDocsStatus = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listInfosDocsStatus;

        this.listNumOrderSAP = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listNumOrderSAP;

        this.listNumOrderEDI = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listNumOrderEDI;

        this.listNumOrderCustomer = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listNumOrderCustomer;

        this.listCampaignCode = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listCampaignCode;

        this.listCampaignName = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listCampaignName;

        this.listReferenceDelivery = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listReferenceDelivery;

        this.listRefCustomerDelivery = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listRefCustomerDelivery;

        this.listNameCustomerDelivery = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listNameCustomerDelivery;

        this.requestedDeliveryDateFrom = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).requestedDeliveryDateFrom;

        this.requestedDeliveryDateTo = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).requestedDeliveryDateTo;

        this.listUnitReference = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listUnitReference;

        this.listLot = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listLot;

        this.acknoledgeByCt = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).acknoledgeByCt;

        this.acknoledgeByCtDate = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).acknoledgeByCtDate;

        this.eligibleForConsolidation = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).eligibleForConsolidation;

        this.insurance = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).insurance;

        this.invoiceCurrency = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).invoiceCurrency;

        this.xEcNature = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).xEcNature;

        this.listUnitsReference = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listUnitsReference;

        this.listunitsPackingList = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listunitsPackingList;

        this.listIncotermLibelle = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listIncotermLibelle;

        this.listNumAwbBol = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listNumAwbBol;

        this.listCarrierUniqRefNum = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listCarrierUniqRefNum;

        this.listEbCostCenter = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listEbCostCenter;

        this.updateDateFrom = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).updateDateFrom;
        this.updateDateTo = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).updateDateTo;

        this.listUser = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listUser;

        this.listEbUserCt = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listEbUserCt;

        this.price = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).price;

        this.libelleLastPsl = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).libelleLastPsl;

        this.dateOfGoodsAvailabilityFrom = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).dateOfGoodsAvailabilityFrom;

        this.dateOfGoodsAvailabilityTo = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).dateOfGoodsAvailabilityTo;

        this.customerCreationDateFrom = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).customerCreationDateFrom;

        this.customerCreationDateTo = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).customerCreationDateTo;

        this.listEtablissementNum = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listEtablissementNum;

        this.listCompanyName = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listCompanyName;

        this.listEmailTransporteur = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listEmailTransporteur;

        this.datePickupTMFrom = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).datePickupTMFrom;

        this.datePickupTMTo = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).datePickupTMTo;

        this.waitingForQuoteDateFrom = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).waitingForQuoteDateFrom;

        this.waitingForQuoteDateTo = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).waitingForQuoteDateTo;

        this.waitingForConfDateFrom = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).waitingForConfDateFrom;

        this.waitingForConfDateTo = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).waitingForConfDateTo;

        this.confirmationDateFrom = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).confirmationDateFrom;

        this.confirmationDateTo = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).confirmationDateTo;

        this.listValuationType = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).listValuationType;

        this.incotermCity = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).incotermCity;

        this.totalNbrParcel = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).totalNbrParcel;

        this.totalVolume = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).totalVolume;

        this.total_weight = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).total_weight;

        this.xEbSchemaPslDesignation = ((SearchCriteriaPricingBooking) deserialize(
            this.searchInput,
            SearchCriteriaPricingBooking.class)).xEbSchemaPslDesignation;
    }

    public List<Integer> getListTypeDemande() {
        return listTypeDemande;
    }

    public List<Integer> getListEbDemandeNum() {
        return listEbDemandeNum;
    }

    public void setListEbDemandeNum(List<Integer> listEbDemandeNum) {
        this.listEbDemandeNum = listEbDemandeNum;
    }

    public List<Integer> getListTypeFlux() {
        return listTypeFlux;
    }

    public void setListTypeFlux(List<Integer> listTypeFlux) {
        this.listTypeFlux = listTypeFlux;
    }

    public boolean isFlagEbtrackTrace() {
        return flagEbtrackTrace;
    }

    public void setFlagEbtrackTrace(boolean flagEbtrackTrace) {
        this.flagEbtrackTrace = flagEbtrackTrace;
    }

    public void setDateOfGoodsAvailability(Date dateOfGoodsAvailability) {
        this.dateOfGoodsAvailability = dateOfGoodsAvailability;
    }

    public Integer getTransportInfostatus() {
        return transportInfostatus;
    }

    public void setTransportInfostatus(Integer transportInfostatus) {
        this.transportInfostatus = transportInfostatus;
    }

    public Integer getCustomDocumentStatus() {
        return customDocumentStatus;
    }

    public void setCustomDocumentStatus(Integer customDocumentStatus) {
        this.customDocumentStatus = customDocumentStatus;
    }

    public Integer getTransportDocumentStatus() {
        return transportDocumentStatus;
    }

    public void setTransportDocumentStatus(Integer transportDocumentStatus) {
        this.transportDocumentStatus = transportDocumentStatus;
    }

    public boolean isFlagQuoteCtRequired() {
        return flagQuoteCtRequired;
    }

    public void setFlagQuoteCtRequired(boolean flagQuoteCtRequired) {
        this.flagQuoteCtRequired = flagQuoteCtRequired;
    }

    public boolean isForCotation() {
        return forCotation;
    }

    public void setForCotation(boolean forCotation) {
        this.forCotation = forCotation;
    }

    public Date getDateOfGoodsAvailability() {
        return dateOfGoodsAvailability;
    }

    public void setListTypeDemande(List<Integer> listTypeDemande) {
        this.listTypeDemande = listTypeDemande;
    }

    public List<Integer> getListInfosDocsStatus() {
        return listInfosDocsStatus;
    }

    public void setListInfosDocsStatus(List<Integer> listInfosDocsStatus) {
        this.listInfosDocsStatus = listInfosDocsStatus;
    }

    public boolean isFlagDateTraitementCPB() {
        return flagDateTraitementCPB;
    }

    public void setFlagDateTraitementCPB(boolean flagDateTraitementCPB) {
        this.flagDateTraitementCPB = flagDateTraitementCPB;
    }

    public boolean isFlagintegratedVia() {
        return integratedVia;
    }

    public void setFlagIntegratedVia(boolean flagintegratedVia) {
        this.integratedVia = flagintegratedVia;
    }

    public boolean isIntegratedVia() {
        return integratedVia;
    }

    public boolean isGenerateTrackTtrace() {
        return generateTrackTtrace;
    }

    public void setIntegratedVia(boolean integratedVia) {
        this.integratedVia = integratedVia;
    }

    public void setGenerateTrackTtrace(boolean generateTrackTtrace) {
        this.generateTrackTtrace = generateTrackTtrace;
    }

    public Date getRequestedDeliveryDateTo() {
        return requestedDeliveryDateTo;
    }

    public void setRequestedDeliveryDateTo(Date requestedDeliveryDateTo) {
        this.requestedDeliveryDateTo = requestedDeliveryDateTo;
    }

    public Date getRequestedDeliveryDateFrom() {
        return requestedDeliveryDateFrom;
    }

    public void setRequestedDeliveryDateFrom(Date requestedDeliveryDateFrom) {
        this.requestedDeliveryDateFrom = requestedDeliveryDateFrom;
    }

    public List<String> getListNumOrderSAP() {
        return listNumOrderSAP;
    }

    public List<String> getListNumOrderEDI() {
        return listNumOrderEDI;
    }

    public List<String> getListNumOrderCustomer() {
        return listNumOrderCustomer;
    }

    public List<String> getListCampaignCode() {
        return listCampaignCode;
    }

    public List<String> getListCampaignName() {
        return listCampaignName;
    }

    public List<String> getListReferenceDelivery() {
        return listReferenceDelivery;
    }

    public List<String> getListRefCustomerDelivery() {
        return listRefCustomerDelivery;
    }

    public List<String> getListNameCustomerDelivery() {
        return listNameCustomerDelivery;
    }

    public List<String> getListUnitReference() {
        return listUnitReference;
    }

    public List<String> getListLot() {
        return listLot;
    }

    public void setListNumOrderSAP(List<String> listNumOrderSAP) {
        this.listNumOrderSAP = listNumOrderSAP;
    }

    public void setListNumOrderEDI(List<String> listNumOrderEDI) {
        this.listNumOrderEDI = listNumOrderEDI;
    }

    public void setListNumOrderCustomer(List<String> listNumOrderCustomer) {
        this.listNumOrderCustomer = listNumOrderCustomer;
    }

    public void setListCampaignCode(List<String> listCampaignCode) {
        this.listCampaignCode = listCampaignCode;
    }

    public void setListCampaignName(List<String> listCampaignName) {
        this.listCampaignName = listCampaignName;
    }

    public void setListReferenceDelivery(List<String> listReferenceDelivery) {
        this.listReferenceDelivery = listReferenceDelivery;
    }

    public void setListRefCustomerDelivery(List<String> listRefCustomerDelivery) {
        this.listRefCustomerDelivery = listRefCustomerDelivery;
    }

    public void setListNameCustomerDelivery(List<String> listNameCustomerDelivery) {
        this.listNameCustomerDelivery = listNameCustomerDelivery;
    }

    public void setListUnitReference(List<String> listUnitReference) {
        this.listUnitReference = listUnitReference;
    }

    public void setListLot(List<String> listLot) {
        this.listLot = listLot;
    }

    public boolean isSearchDemandeByLivraisonData() {
        return searchDemandeByLivraisonData;
    }

    public boolean isSearchDemandeByMarchandisenData() {
        return searchDemandeByMarchandiseData;
    }

    public void setSearchDemandeByLivraisonData(boolean searchDemandeByLivraisonData) {
        this.searchDemandeByLivraisonData = searchDemandeByLivraisonData;
    }

    public void setSearchDemandeByMarchandisenData(boolean searchDemandeByMarchandisenData) {
        this.searchDemandeByMarchandiseData = searchDemandeByMarchandisenData;
    }

    public boolean isIntegratedMarchandiseWithoutParent() {
        return integratedMarchandiseWithoutParent;
    }

    public void setIntegratedMarchandiseWithoutParent(boolean integratedMarchandiseWithoutParent) {
        this.integratedMarchandiseWithoutParent = integratedMarchandiseWithoutParent;
    }

    public Integer getAcknoledgeByCt() {
        return acknoledgeByCt;
    }

    public void setAcknoledgeByCt(Integer acknoledgeByCt) {
        this.acknoledgeByCt = acknoledgeByCt;
    }

    public Date getAcknoledgeByCtDate() {
        return acknoledgeByCtDate;
    }

    public void setAcknoledgeByCtDate(Date acknoledgeByCtDate) {
        this.acknoledgeByCtDate = acknoledgeByCtDate;
    }

    public Integer getEligibleForConsolidation() {
        return eligibleForConsolidation;
    }

    public void setEligibleForConsolidation(Integer eligibleForConsolidation) {
        this.eligibleForConsolidation = eligibleForConsolidation;
    }

    public Integer getInsurance() {
        return insurance;
    }

    public void setInsurance(Integer insurance) {
        this.insurance = insurance;
    }

    public Integer getInvoiceCurrency() {
        return invoiceCurrency;
    }

    public void setInvoiceCurrency(Integer invoiceCurrency) {
        this.invoiceCurrency = invoiceCurrency;
    }

    public Boolean getSearchByWS() {
        return searchByWS;
    }

    public void setSearchByWS(Boolean searchByWS) {
        this.searchByWS = searchByWS;
    }

    public Integer getStatutGroupage() {
        return statutGroupage;
    }

    public void setStatutGroupage(Integer statutGroupage) {
        this.statutGroupage = statutGroupage;
    }

    public String getTransporterEmail() {
        return transporterEmail;
    }

    public void setTransporterEmail(String transporterEmail) {
        this.transporterEmail = transporterEmail;
    }

    public String getTransporterCompanyCode() {
        return transporterCompanyCode;
    }

    public void setTransporterCompanyCode(String transporterCompanyCode) {
        this.transporterCompanyCode = transporterCompanyCode;
    }

    public Integer getxEcNature() {
        return xEcNature;
    }

    public void setxEcNature(Integer xEcNature) {
        this.xEcNature = xEcNature;
    }

    public Boolean getValorized() {
        return isValorized;
    }

    public void setValorized(Boolean valorized) {
        isValorized = valorized;
    }

    public Boolean getGrouping() {
        return isGrouping;
    }

    public void setGrouping(Boolean grouping) {
        isGrouping = grouping;
    }

    public Double getTotalTaxableWeight() {
        return totalTaxableWeight;
    }

    public void setTotalTaxableWeight(Double totalTaxableWeight) {
        this.totalTaxableWeight = totalTaxableWeight;
    }

    public Integer getxEcStatut() {
        return xEcStatut;
    }

    public void setxEcStatut(Integer xEcStatut) {
        this.xEcStatut = xEcStatut;
    }

    public List<String> getListUnitsReference() {
        return listUnitsReference;
    }

    public void setListUnitsReference(List<String> listUnitsReference) {
        this.listUnitsReference = listUnitsReference;
    }

    public List<String> getListunitsPackingList() {
        return listunitsPackingList;
    }

    public void setListunitsPackingList(List<String> listunitsPackingList) {
        this.listunitsPackingList = listunitsPackingList;
    }

    public List<String> getListIncotermLibelle() {
        return listIncotermLibelle;
    }

    public void setListIncotermLibelle(List<String> listIncotermLibelle) {
        this.listIncotermLibelle = listIncotermLibelle;
    }

    public List<String> getListNumAwbBol() {
        return listNumAwbBol;
    }

    public void setListNumAwbBol(List<String> listNumAwbBol) {
        this.listNumAwbBol = listNumAwbBol;
    }

    public List<String> getListCarrierUniqRefNum() {
        return listCarrierUniqRefNum;
    }

    public void setListCarrierUniqRefNum(List<String> listCarrierUniqRefNum) {
        this.listCarrierUniqRefNum = listCarrierUniqRefNum;
    }

    public List<Integer> getListEbCostCenter() {
        return listEbCostCenter;
    }

    public void setListEbCostCenter(List<Integer> listEbCostCenter) {
        this.listEbCostCenter = listEbCostCenter;
    }

    public Date getUpdateDateFrom() {
        return updateDateFrom;
    }

    public void setUpdateDateFrom(Date updateDateFrom) {
        this.updateDateFrom = updateDateFrom;
    }

    public Date getUpdateDateTo() {
        return updateDateTo;
    }

    public void setUpdateDateTo(Date updateDateTo) {
        this.updateDateTo = updateDateTo;
    }

    public List<Integer> getListEbUserCt() {
        return listEbUserCt;
    }

    public void setListEbUserCt(List<Integer> listEbUserCt) {
        this.listEbUserCt = listEbUserCt;
    }

    public List<Integer> getListUser() {
        return listUser;
    }

    public void setListUser(List<Integer> listUser) {
        this.listUser = listUser;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getLibelleLastPsl() {
        return libelleLastPsl;
    }

    public void setLibelleLastPsl(String libelleLastPsl) {
        this.libelleLastPsl = libelleLastPsl;
    }

    public Date getDateOfGoodsAvailabilityFrom() {
        return dateOfGoodsAvailabilityFrom;
    }

    public void setDateOfGoodsAvailabilityFrom(Date dateOfGoodsAvailabilityFrom) {
        this.dateOfGoodsAvailabilityFrom = dateOfGoodsAvailabilityFrom;
    }

    public Date getDateOfGoodsAvailabilityTo() {
        return dateOfGoodsAvailabilityTo;
    }

    public void setDateOfGoodsAvailabilityTo(Date dateOfGoodsAvailabilityTo) {
        this.dateOfGoodsAvailabilityTo = dateOfGoodsAvailabilityTo;
    }

    public Date getCustomerCreationDateFrom() {
        return customerCreationDateFrom;
    }

    public void setCustomerCreationDateFrom(Date customerCreationDateFrom) {
        this.customerCreationDateFrom = customerCreationDateFrom;
    }

    public Date getCustomerCreationDateTo() {
        return customerCreationDateTo;
    }

    public void setCustomerCreationDateTo(Date customerCreationDateTo) {
        this.customerCreationDateTo = customerCreationDateTo;
    }

    public List<Integer> getListEtablissementNum() {
        return listEtablissementNum;
    }

    public void setListEtablissementNum(List<Integer> listEtablissementNum) {
        this.listEtablissementNum = listEtablissementNum;
    }

    public List<String> getListCompanyName() {
        return listCompanyName;
    }

    public void setListCompanyName(List<String> listCompanyName) {
        this.listCompanyName = listCompanyName;
    }

    public List<String> getListEmailTransporteur() {
        return listEmailTransporteur;
    }

    public void setListEmailTransporteur(List<String> listEmailTransporteur) {
        this.listEmailTransporteur = listEmailTransporteur;
    }

    public Date getDatePickupTMFrom() {
        return datePickupTMFrom;
    }

    public void setDatePickupTMFrom(Date datePickupTMFrom) {
        this.datePickupTMFrom = datePickupTMFrom;
    }

    public Date getDatePickupTMTo() {
        return datePickupTMTo;
    }

    public void setDatePickupTMTo(Date datePickupTMTo) {
        this.datePickupTMTo = datePickupTMTo;
    }

    public Date getWaitingForQuoteDateFrom() {
        return waitingForQuoteDateFrom;
    }

    public void setWaitingForQuoteDateFrom(Date waitingForQuoteDateFrom) {
        this.waitingForQuoteDateFrom = waitingForQuoteDateFrom;
    }

    public Date getWaitingForQuoteDateTo() {
        return waitingForQuoteDateTo;
    }

    public void setWaitingForQuoteDateTo(Date waitingForQuoteDateTo) {
        this.waitingForQuoteDateTo = waitingForQuoteDateTo;
    }

    public Date getWaitingForConfDateFrom() {
        return waitingForConfDateFrom;
    }

    public void setWaitingForConfDateFrom(Date waitingForConfDateFrom) {
        this.waitingForConfDateFrom = waitingForConfDateFrom;
    }

    public Date getWaitingForConfDateTo() {
        return waitingForConfDateTo;
    }

    public void setWaitingForConfDateTo(Date waitingForConfDateTo) {
        this.waitingForConfDateTo = waitingForConfDateTo;
    }

    public Date getConfirmationDateFrom() {
        return confirmationDateFrom;
    }

    public void setConfirmationDateFrom(Date confirmationDateFrom) {
        this.confirmationDateFrom = confirmationDateFrom;
    }

    public Date getConfirmationDateTo() {
        return confirmationDateTo;
    }

    public void setConfirmationDateTo(Date confirmationDateTo) {
        this.confirmationDateTo = confirmationDateTo;
    }

    public Double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(Double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public String getxEbSchemaPslDesignation() {
        return xEbSchemaPslDesignation;
    }

    public void setxEbSchemaPslDesignation(String xEbSchemaPslDesignation) {
        this.xEbSchemaPslDesignation = xEbSchemaPslDesignation;
    }

    public String getIncotermCity() {
        return incotermCity;
    }

    public void setIncotermCity(String incotermCity) {
        this.incotermCity = incotermCity;
    }

    public Double getTotalNbrParcel() {
        return totalNbrParcel;
    }

    public void setTotalNbrParcel(Double totalNbrParcel) {
        this.totalNbrParcel = totalNbrParcel;
    }

    public List<Integer> getListValuationType() {
        return listValuationType;
    }

    public void setListValuationType(List<Integer> listValuationType) {
        this.listValuationType = listValuationType;
    }

    public Double getTotal_weight() {
        return total_weight;
    }

    public void setTotal_weight(Double total_weight) {
        this.total_weight = total_weight;
    }
}
