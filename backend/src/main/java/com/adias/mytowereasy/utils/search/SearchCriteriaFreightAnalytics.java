/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.utils.search;

public class SearchCriteriaFreightAnalytics extends SearchCriteria {
    private Integer month;
    private Integer year;
    private Integer carrier;
    private Integer shipper;
    private Integer transportType;
    private Integer originCountry;
    private Integer destinationCountry;
    private Integer startMonth;
    private Integer endMonth;
    private Integer startYear;
    private Integer endYear;

    public SearchCriteriaFreightAnalytics() {
        month = null;
        year = null;
        carrier = null;
        shipper = null;
        transportType = null;
        originCountry = null;
        destinationCountry = null;
        startMonth = null;
        endMonth = null;
        startYear = null;
        endYear = null;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getCarrier() {
        return carrier;
    }

    public void setCarrier(Integer carrier) {
        this.carrier = carrier;
    }

    public Integer getShipper() {
        return shipper;
    }

    public void setShipper(Integer shipper) {
        this.shipper = shipper;
    }

    public Integer getTransportType() {
        return transportType;
    }

    public void setTransportType(Integer transportType) {
        this.transportType = transportType;
    }

    public Integer getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(Integer originCountry) {
        this.originCountry = originCountry;
    }

    public Integer getDestinationCountry() {
        return destinationCountry;
    }

    public void setDestinationCountry(Integer destinationCountry) {
        this.destinationCountry = destinationCountry;
    }

    public Integer getStartMonth() {
        return startMonth;
    }

    public void setStartMonth(Integer startMonth) {
        this.startMonth = startMonth;
    }

    public Integer getEndMonth() {
        return endMonth;
    }

    public void setEndMonth(Integer endMonth) {
        this.endMonth = endMonth;
    }

    public Integer getStartYear() {
        return startYear;
    }

    public void setStartYear(Integer startYear) {
        this.startYear = startYear;
    }

    public Integer getEndYear() {
        return endYear;
    }

    public void setEndYear(Integer endYear) {
        this.endYear = endYear;
    }
}
