/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.utils.search;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.qm.EbQmIncidentLine;


public class SearchCriteriaQM extends SearchCriteria {
    private Integer eventType;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listIncidentRefs;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listCarrierRefs;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listShipToRefs;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listCarriers;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listOperationalGroupings;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listIncidentQualifs;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listIncidentCategories;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listIncidentStatus;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<String> listPsl;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listEbUserOwnerRequest;

    private List<Integer> listCriticality;

    private Boolean insuranceChoice;

    private Integer ebQmIncidentNum;

    private Integer semiAutoMailNum;

    private List<EbQmIncidentLine> listIncidentLine;

    private List<Integer> incidentLinesIdNumbers;

    private List<Integer> marchandiseIdNumbers;

    private List<Integer> listInsuranceIncident;

    private Integer ebQmRootCauseNum;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    private List<Integer> listStatusIncident;

    private List<Integer> listEventTypeStatus;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dateTo;

    private Integer ebQmDeviationNum;

    private Integer incidentStatus;
    // public void setSearchInput(String searchInput) throws JsonParseException,
    // JsonMappingException,
    // IOException {
    // this.searchInput = searchInput;
    // SearchCriteriaQM scqm = ((SearchCriteriaQM) deserialize(searchInput,
    // SearchCriteriaQM.class));
    // listCarrierRefs = scqm.listCarrierRefs;
    // listCarriers = scqm.listCarriers;
    // listIncidentCategories = scqm.listIncidentCategories;
    // listIncidentQualifs = scqm.listIncidentQualifs;incidentStatus
    // listIncidentRefs = scqm.listIncidentRefs;
    // listIncidentStatus = scqm.listIncidentStatus;
    // listOperationalGroupings = scqm.listOperationalGroupings;
    // listShipToRefs = scqm.listShipToRefs;
    // dateFrom = scqm.dateFrom;
    // dateTo = scqm.dateTo;
    // insuranceChoice = scqm.insuranceChoice;
    // listOriginCountry = scqm.listOriginCountry;
    // }

    public void setSearchInput(String searchInput, List<CustomFields> customsFields) throws JsonParseException, JsonMappingException, IOException {
        super.setSearchInput(searchInput, customsFields);
        if (searchInput == null) searchInput = "{}";

        this.listCarrierRefs = ((SearchCriteriaQM) deserialize(searchInput, SearchCriteriaQM.class)).listCarrierRefs;

        this.listIdObject = ((SearchCriteriaQM) deserialize(searchInput, SearchCriteriaQM.class)).listIdObject;

        this.listInsuranceIncident = ((SearchCriteriaQM) deserialize(
            searchInput,
            SearchCriteriaQM.class)).listInsuranceIncident;

        this.listCriticality = ((SearchCriteriaQM) deserialize(searchInput, SearchCriteriaQM.class)).listCriticality;

        this.listStatusIncident = ((SearchCriteriaQM) deserialize(
            searchInput,
            SearchCriteriaQM.class)).listStatusIncident;
        this.listEventTypeStatus = ((SearchCriteriaQM) deserialize(
            searchInput,
            SearchCriteriaQM.class)).listEventTypeStatus;
        this.listIncidentRefs = ((SearchCriteriaQM) deserialize(searchInput, SearchCriteriaQM.class)).listIncidentRefs;

        this.listIncidentCategories = ((SearchCriteriaQM) deserialize(
            searchInput,
            SearchCriteriaQM.class)).listIncidentCategories;

        this.ebQmIncidentNum = ((SearchCriteriaQM) deserialize(searchInput, SearchCriteriaQM.class)).ebQmIncidentNum;

        this.ebQmDeviationNum = ((SearchCriteriaQM) deserialize(searchInput, SearchCriteriaQM.class)).ebQmDeviationNum;

        this.listPsl = ((SearchCriteriaQM) deserialize(searchInput, SearchCriteriaQM.class)).listPsl;

        this.listEbUserOwnerRequest = ((SearchCriteriaQM) deserialize(
            searchInput,
            SearchCriteriaQM.class)).listEbUserOwnerRequest;

        this.incidentStatus = ((SearchCriteriaQM) deserialize(searchInput, SearchCriteriaQM.class)).incidentStatus;
    }

    public List<Integer> getListIncidentRefs() {
        return listIncidentRefs;
    }

    public void setListIncidentRefs(List<Integer> listIncidentRefs) {
        this.listIncidentRefs = listIncidentRefs;
    }

    public List<Integer> getListCarrierRefs() {
        return listCarrierRefs;
    }

    public void setListCarrierRefs(List<Integer> listCarrierRefs) {
        this.listCarrierRefs = listCarrierRefs;
    }

    public List<Integer> getListShipToRefs() {
        return listShipToRefs;
    }

    public void setListShipToRefs(List<Integer> listShipToRefs) {
        this.listShipToRefs = listShipToRefs;
    }

    public List<String> getListCarriers() {
        return listCarriers;
    }

    public void setListCarriers(List<String> listCarriers) {
        this.listCarriers = listCarriers;
    }

    public List<String> getListOperationalGroupings() {
        return listOperationalGroupings;
    }

    public void setListOperationalGroupings(List<String> listOperationalGroupings) {
        this.listOperationalGroupings = listOperationalGroupings;
    }

    public List<String> getListIncidentQualifs() {
        return listIncidentQualifs;
    }

    public void setListIncidentQualifs(List<String> listIncidentQualifs) {
        this.listIncidentQualifs = listIncidentQualifs;
    }

    public List<String> getListIncidentCategories() {
        return listIncidentCategories;
    }

    public void setListIncidentCategories(List<String> listIncidentCategories) {
        this.listIncidentCategories = listIncidentCategories;
    }

    public List<String> getListIncidentStatus() {
        return listIncidentStatus;
    }

    public void setListIncidentStatus(List<String> listIncidentStatus) {
        this.listIncidentStatus = listIncidentStatus;
    }

    public Boolean getInsuranceChoice() {
        return insuranceChoice;
    }

    public void setInsuranceChoice(Boolean insuranceChoice) {
        this.insuranceChoice = insuranceChoice;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Integer getEbQmIncidentNum() {
        return ebQmIncidentNum;
    }

    public void setEbQmIncidentNum(Integer ebQmIncidentNum) {
        this.ebQmIncidentNum = ebQmIncidentNum;
    }

    public Integer getSemiAutoMailNum() {
        return semiAutoMailNum;
    }

    public void setSemiAutoMailNum(Integer semiAutoMailNum) {
        this.semiAutoMailNum = semiAutoMailNum;
    }

    public List<EbQmIncidentLine> getListIncidentLine() {
        return listIncidentLine;
    }

    public void setListIncidentLine(List<EbQmIncidentLine> listIncidentLine) {
        this.listIncidentLine = listIncidentLine;
    }

    public List<Integer> getIncidentLinesIdNumbers() {
        return incidentLinesIdNumbers;
    }

    public void setIncidentLinesIdNumbers(List<Integer> incidentLinesIdNumbers) {
        this.incidentLinesIdNumbers = incidentLinesIdNumbers;
    }

    public List<Integer> getMarchandiseIdNumbers() {
        return marchandiseIdNumbers;
    }

    public void setMarchandiseIdNumbers(List<Integer> marchandiseIdNumbers) {
        this.marchandiseIdNumbers = marchandiseIdNumbers;
    }

    public List<Integer> getListInsuranceIncident() {
        return listInsuranceIncident;
    }

    public void setListInsuranceIncident(List<Integer> listInsuranceIncident) {
        this.listInsuranceIncident = listInsuranceIncident;
    }

    public List<Integer> getListStatusIncident() {
        return listStatusIncident;
    }

    public void setListStatusIncident(List<Integer> listStatusIncident) {
        this.listStatusIncident = listStatusIncident;
    }

    public List<Integer> getListCriticality() {
        return listCriticality;
    }

    public void setListCriticality(List<Integer> listCriticality) {
        this.listCriticality = listCriticality;
    }

    public Integer getEbQmRootCauseNum() {
        return ebQmRootCauseNum;
    }

    public void setEbQmRootCauseNum(Integer ebQmRootCauseNum) {
        this.ebQmRootCauseNum = ebQmRootCauseNum;
    }

    public Integer getEbQmDeviationNum() {
        return ebQmDeviationNum;
    }

    public void setEbQmDeviationNum(Integer ebQmDeviationNum) {
        this.ebQmDeviationNum = ebQmDeviationNum;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    public List<String> getListPsl() {
        return listPsl;
    }

    public void setListPsl(List<String> listPsl) {
        this.listPsl = listPsl;
    }

    public List<Integer> getListEventTypeStatus() {
        return listEventTypeStatus;
    }

    public void setListEventTypeStatus(List<Integer> listEventTypeStatus) {
        this.listEventTypeStatus = listEventTypeStatus;
    }

    public Integer getIncidentStatus() {
        return incidentStatus;
    }

    public void setIncidentStatus(Integer incidentStatus) {
        this.incidentStatus = incidentStatus;
    }

    public List<Integer> getListEbUserOwnerRequest() {
        return listEbUserOwnerRequest;
    }

    public void setListEbUserOwnerRequest(List<Integer> listEbUserOwnerRequest) {
        this.listEbUserOwnerRequest = listEbUserOwnerRequest;
    }
}
