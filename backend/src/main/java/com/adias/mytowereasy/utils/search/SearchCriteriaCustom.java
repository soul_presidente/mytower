/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.utils.search;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;


public class SearchCriteriaCustom extends SearchCriteria {
    private Integer ebCustomDeclarationNum;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listAgentBroker;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listClearanceCountry;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listclearanceId;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listvilleIncoterm;

    @JsonInclude(JsonInclude.Include.NON_DEFAULT)
    protected List<Integer> listShipper;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date requestDateFrom;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    protected Date requestDateTo;

    public List<Integer> getListAgentBroker() {
        return listAgentBroker;
    }

    public void setListAgentBroker(List<Integer> listAgentBroker) {
        this.listAgentBroker = listAgentBroker;
    }

    public List<Integer> getListClearanceCountry() {
        return listClearanceCountry;
    }

    public void setListClearanceCountry(List<Integer> listClearanceCountry) {
        this.listClearanceCountry = listClearanceCountry;
    }

    public List<Integer> getListclearanceId() {
        return listclearanceId;
    }

    public void setListclearanceId(List<Integer> listclearanceId) {
        this.listclearanceId = listclearanceId;
    }

    public List<Integer> getListvilleIncoterm() {
        return listvilleIncoterm;
    }

    public void setListvilleIncoterm(List<Integer> listvilleIncoterm) {
        this.listvilleIncoterm = listvilleIncoterm;
    }

    public List<Integer> getListShipper() {
        return listShipper;
    }

    public void setListShipper(List<Integer> listShipper) {
        this.listShipper = listShipper;
    }

    public Date getRequestDateFrom() {
        return requestDateFrom;
    }

    public void setRequestDateFrom(Date requestDateFrom) {
        this.requestDateFrom = requestDateFrom;
    }

    public Date getRequestDateTo() {
        return requestDateTo;
    }

    public void setRequestDateTo(Date requestDateTo) {
        this.requestDateTo = requestDateTo;
    }

    public Integer getEbCustomDeclarationNum() {
        return ebCustomDeclarationNum;
    }

    public void setEbCustomDeclarationNum(Integer ebCustomDeclarationNum) {
        this.ebCustomDeclarationNum = ebCustomDeclarationNum;
    }
}
