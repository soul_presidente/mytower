package com.adias.mytowereasy.utils.search;

public class PageableSearchCriteria {

    private Integer start;
    private String orderedColumn;
    private Integer size;
    private String searchterm;
    private boolean ascendant;

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public String getOrderedColumn() {
        return orderedColumn;
    }

    public void setOrderedColumn(String orderedColumn) {
        this.orderedColumn = orderedColumn;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getSearchterm() {
        return searchterm;
    }

    public void setSearchterm(String searchterm) {
        this.searchterm = searchterm;
    }

    public boolean getAscendant() {
        return ascendant;
    }

    public void setAscendant(boolean ascendant) {
        this.ascendant = ascendant;
    }
}
