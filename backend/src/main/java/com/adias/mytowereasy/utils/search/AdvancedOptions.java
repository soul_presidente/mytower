/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.utils.search;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.PricingBookingEnumeration.TypeDemande;


public class AdvancedOptions {

    // Pour l'enumerations faut construire une liste des integers
    @JsonDeserialize(as = ArrayList.class, contentAs = TypeDemande.class)
    private List<TypeDemande> listTypeDemande;

    private List<Integer> listTypeDemandeCodesOnly;

    @JsonDeserialize(as = ArrayList.class, contentAs = StatutDemande.class)
    private List<StatutDemande> listStatut;

    private List<Integer> listStatutCodesOnly;

    //
    private List<EcRegion> listTypeRegion;
    private List<EbDemande> listTransportRef;
    private List<EbCommande> listCustomerReference;
    private List<EcCountry> listDestinations;

    private List<EbUser> listCarrier;

    //
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date datePickUpTo;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date datePickUpFrom;

    public List<TypeDemande> getListTypeDemande() {
        return listTypeDemande;
    }

    public List<StatutDemande> getListStatut() {
        return listStatut;
    }

    public void setListStatut(List<StatutDemande> listStatut) {
        this.listStatut = listStatut;

        List<Integer> listStatusCodesOnly = new ArrayList<>();

        for (StatutDemande statut: listStatut) {
            listStatusCodesOnly.add(statut.getCode());
        }

        this.listStatutCodesOnly = listStatusCodesOnly;
    }

    public List<EbUser> getListCarrier() {
        return listCarrier;
    }

    public void setListCarrier(List<EbUser> listCarrier) {
        this.listCarrier = listCarrier;
    }

    public void setListTypeDemande(List<TypeDemande> listTypeDemande) {
        this.listTypeDemande = listTypeDemande;

        List<Integer> listTypeDemandeCodes = new ArrayList<>();

        for (TypeDemande typeDemande: listTypeDemande) {
            listTypeDemandeCodes.add(typeDemande.getCode());
        }

        this.listTypeDemandeCodesOnly = listTypeDemandeCodes;
    }

    public List<EbDemande> getListTransportRef() {
        return listTransportRef;
    }

    public void setListTransportRef(List<EbDemande> listTransportRef) {
        this.listTransportRef = listTransportRef;
    }

    public List<EbCommande> getListCustomerReference() {
        return listCustomerReference;
    }

    public void setListCustomerReference(List<EbCommande> listCustomerReference) {
        this.listCustomerReference = listCustomerReference;
    }

    public Date getDatePickUpTo() {
        return datePickUpTo;
    }

    public void setDatePickUpTo(Date datePickUpTo) {
        this.datePickUpTo = datePickUpTo;
    }

    public Date getDatePickUpFrom() {
        return datePickUpFrom;
    }

    public void setDatePickUpFrom(Date datePickUpFrom) {
        this.datePickUpFrom = datePickUpFrom;
    }

    public List<EcRegion> getListTypeRegion() {
        return listTypeRegion;
    }

    public void setListTypeRegion(List<EcRegion> listTypeRegion) {
        this.listTypeRegion = listTypeRegion;
    }

    public List<EcCountry> getListDestinations() {
        return listDestinations;
    }

    public void setListDestinations(List<EcCountry> listDestinations) {
        this.listDestinations = listDestinations;
    }

    public List<Integer> getListTypeDemandeCodesOnly() {
        return listTypeDemandeCodesOnly;
    }

    public List<Integer> getListStatutCodesOnly() {
        return listStatutCodesOnly;
    }
}
