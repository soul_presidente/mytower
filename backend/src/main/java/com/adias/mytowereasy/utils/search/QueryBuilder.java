/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.utils.search;

import java.lang.reflect.Field;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.dsl.*;
import com.querydsl.jpa.impl.JPAQuery;


/**
 * ********** CETTE CLASSE EST POUR UNE EVOLUTION FUTURE, A NE PAS UTILISER
 * ****************
 */
public class QueryBuilder<T> {
    private List<SearchCriteriaAnalytics> params;
    private final Class<?> type;
    private JPAQuery<T> query;

    public QueryBuilder(JPAQuery<T> query, Class<?> type) {
        params = new ArrayList<SearchCriteriaAnalytics>();
        this.type = type;
        this.query = query;
    }

    public QueryBuilder<T> with(String type, String key, String operation, String value, String table) {
        params.add(new SearchCriteriaAnalytics(type, key, operation, value, table));
        return this;
    }

    public QueryBuilder<T> with(SearchCriteriaAnalytics criteria) {
        params.add(criteria);
        return this;
    }

    public JPAQuery<T> build()
        throws ClassNotFoundException,
        NoSuchFieldException,
        SecurityException,
        InstantiationException,
        IllegalAccessException {

        for (SearchCriteriaAnalytics criteria: params) {
            Field entity = type.getDeclaredField("dwrFShipments");
            EntityPath<?> entityPath = (EntityPath<?>) entity.get(type);
            query.from(entityPath);
            System.out.println(query.toString());
            PathBuilder<?> entityPathBuilder = new PathBuilder<>(type, "dwrFShipments");
            BooleanExpression predicate = null;

            switch (criteria.getType()) {
                case "String":
                    StringPath predicateString = entityPathBuilder.getString(criteria.getKey());
                    switch (criteria.getOperation()) {
                        case ":":
                            predicate = predicateString.eq(criteria.getValue());
                        case ">":
                            predicate = predicateString.goe(criteria.getValue());
                        case "<":
                            predicate = predicateString.loe(criteria.getValue());
                    }
                    break;

                case "Integer":
                    NumberPath<Integer> predicateInteger = entityPathBuilder
                        .getNumber(criteria.getKey(), Integer.class);
                    switch (criteria.getOperation()) {
                        case ":":
                            predicate = predicateInteger.eq(Integer.parseInt(criteria.getValue()));
                        case ">":
                            predicate = predicateInteger.goe(Integer.parseInt(criteria.getValue()));
                        case "<":
                            predicate = predicateInteger.loe(Integer.parseInt(criteria.getValue()));
                    }
                    break;

                case "Float":
                    NumberPath<Float> predicateFloat = entityPathBuilder.getNumber(criteria.getKey(), Float.class);
                    switch (criteria.getOperation()) {
                        case ":":
                            predicate = predicateFloat.eq(Float.parseFloat(criteria.getValue()));
                        case ">":
                            predicate = predicateFloat.goe(Float.parseFloat(criteria.getValue()));
                        case "<":
                            predicate = predicateFloat.loe(Float.parseFloat(criteria.getValue()));
                    }
                    break;

                case "Double":
                    NumberPath<Double> predicateDouble = entityPathBuilder.getNumber(criteria.getKey(), Double.class);
                    switch (criteria.getOperation()) {
                        case ":":
                            predicate = predicateDouble.eq(Double.parseDouble(criteria.getValue()));
                        case ">":
                            predicate = predicateDouble.goe(Double.parseDouble(criteria.getValue()));
                        case "<":
                            predicate = predicateDouble.loe(Double.parseDouble(criteria.getValue()));
                    }
                    break;

                case "Date":
                    DatePath<Date> predicateDate = entityPathBuilder.getDate(criteria.getKey(), Date.class);
                    switch (criteria.getOperation()) {
                        case ":":
                            predicate = predicateDate.eq(Date.valueOf(criteria.getValue()));
                        case ">":
                            predicate = predicateDate.goe(Date.valueOf(criteria.getValue()));
                        case "<":
                            predicate = predicateDate.loe(Date.valueOf(criteria.getValue()));
                    }
                    break;

                case "DateTime":
                    DatePath<Date> predicateDateTime = entityPathBuilder.getDate(criteria.getKey(), Date.class);
                    switch (criteria.getOperation()) {
                        case ":":
                            predicate = predicateDateTime.eq(Date.valueOf(criteria.getValue()));
                        case ">":
                            predicate = predicateDateTime.goe(Date.valueOf(criteria.getValue()));
                        case "<":
                            predicate = predicateDateTime.loe(Date.valueOf(criteria.getValue()));
                    }
                    break;
            }

            if (predicate != null) {
                query.where(predicate);
            }

            System.out.println(query.toString());
        }

        return query;
    }
}
