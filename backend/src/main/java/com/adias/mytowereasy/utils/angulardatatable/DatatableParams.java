/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.utils.angulardatatable;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.json.JSONException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.SearchCriterias;
import com.adias.mytowereasy.utils.search.AdvancedOptions;
import com.adias.mytowereasy.utils.search.SearchCriteria.OrderCriterias;


public class DatatableParams {
    private Integer length;
    private Integer start;
    private Map<SearchCriterias, String> search;
    private List<Map<OrderCriterias, String>> order;
    // private List<Map<ColumnCriterias, String>> columns;
    private String advancedOptions;
    private AdvancedOptions advancedOptionsReturn;

    // private String searchCriteriaString;
    // private SearchCriteria searchCriteria;

    private String user;
    private EbUser userReturn;

    private Integer module;

    // private List<Map<OrderCriterias, String>> order;

    public Map<SearchCriterias, String> getSearch() {
        return search;
    }

    public void setSearch(Map<SearchCriterias, String> search) {
        this.search = search;
    }

    public String getAdvancedOptions() {
        return advancedOptions;
    }

    public void setAdvancedOptions(String advancedOptions)
        throws JSONException,
        JsonParseException,
        JsonMappingException,
        IOException {
        this.advancedOptionsReturn = (AdvancedOptions) deserialize(advancedOptions, AdvancedOptions.class);
        this.advancedOptions = advancedOptions;
    }

    public AdvancedOptions getAdvancedOptionsReturn() {
        return advancedOptionsReturn;
    }

    public void setAdvancedOptionsReturn(AdvancedOptions advancedOptionsReturn) {
        this.advancedOptionsReturn = advancedOptionsReturn;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public List<Map<OrderCriterias, String>> getOrder() {
        return order;
    }

    public void setOrder(List<Map<OrderCriterias, String>> order) {
        this.order = order;
    }

    /*
     * public List<Map<ColumnCriterias, String>> getColumns()
     * {
     * return columns;
     * }
     * public void setColumns(List<Map<ColumnCriterias, String>> columns)
     * {
     * this.columns = columns;
     * }
     * public String getSearchCriteriaString()
     * {
     * return searchCriteriaString;
     * }
     * public void setSearchCriteriaString(String searchCriteriaString) throws
     * JsonParseException, JsonMappingException, IOException
     * {
     * this.searchCriteria = (SearchCriteria) deserialize(searchCriteriaString,
     * SearchCriteria.class);
     * this.searchCriteriaString = searchCriteriaString;
     * }
     * public SearchCriteria getSearchCriteria()
     * {
     * return searchCriteria;
     * }
     * public void setSearchCriteria(SearchCriteria searchCriteria)
     * {
     * this.searchCriteria = searchCriteria;
     * }
     */

    public static <T> Object
        deserialize(String str, Class<T> _Type) throws JsonParseException, JsonMappingException, IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Object returnObject = objectMapper.readValue(str, _Type);
        return returnObject;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) throws JsonParseException, JsonMappingException, IOException {
        this.user = user;
        this.userReturn = (EbUser) deserialize(user, EbUser.class);
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }
}
