package com.adias.mytowereasy.kafka.model;

import java.math.BigDecimal;

import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.Enumeration.StatutCarrier;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;


public class QuotationKafkaObject {
    private Integer itemId;
    private String carrierUserEmail;

    private Integer transitTime;

    private BigDecimal price;

    private String comment;
    private String status;

    private String modeTransport;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getCarrierUserEmail() {
        return carrierUserEmail;
    }

    public void setCarrierUserEmail(String carrierUserEmail) {
        this.carrierUserEmail = carrierUserEmail;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getModeTransport() {
        return modeTransport;
    }

    public void setModeTransport(String modeTransport) {
        this.modeTransport = modeTransport;
    }

    public QuotationKafkaObject(ExEbDemandeTransporteur quotation) {
        this.itemId = quotation.getExEbDemandeTransporteurNum();
        this.carrierUserEmail = quotation.getxTransporteur().getEmail();
        this.transitTime = quotation.getTransitTime();
        this.price = quotation.getPrice();
        this.comment = quotation.getComment();
        this.status = StatutCarrier.getLibelleByCode(quotation.getStatus());
        this.modeTransport = ModeTransport.getLibelleByCode(quotation.getxEcModeTransport());
    }
}
