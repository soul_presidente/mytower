package com.adias.mytowereasy.kafka.aspects;

import com.adias.mytowereasy.SpringUtility;
import com.adias.mytowereasy.kafka.service.KafkaObjectService;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;

public enum KafkaObjectType
{

	PROCESS_GROUP_OBJECT
	{
		@Override
		public void buildAndSendObject(Object... args)
		{
			KafkaObjectService kos = SpringUtility.getBean(KafkaObjectService.class);
			Integer companyId = (Integer) args[0];
			Integer triggerCode = (Integer) args[1];
			EbDemande initialTr = (EbDemande) args[2];
			EbUser user = (EbUser) args[3];
			kos
				.buildAndSendProcessGroupToKafka(
					companyId,
					triggerCode,
					user.getEbUserNum(),
					initialTr.getEbDemandeNum()
				);
		}
	},

	TRANSPORT_REQUEST_OBJECT
	{
		@Override
		public void buildAndSendObject(Object... args)
		{
			KafkaObjectService kos = SpringUtility.getBean(KafkaObjectService.class);
			Integer trId = (Integer) args[0];
			EbUser user = (EbUser) args[1];
			kos.buildAndSendTransportRequestToKafka(trId, user, false);
		}
	},

	BOTH
	{
		@Override
		public void buildAndSendObject(Object... args)
		{
			KafkaObjectService kos = SpringUtility.getBean(KafkaObjectService.class);
			Integer companyId = (Integer) args[0];
			Integer triggerCode = (Integer) args[1];
			EbDemande initialTr = (EbDemande) args[2];
			EbUser user = (EbUser) args[3];
			kos
				.buildAndSendProcessGroupToKafka(
					companyId,
					triggerCode,
					user.getEbUserNum(),
					initialTr.getEbDemandeNum()
				);
			kos.buildAndSendTransportRequestToKafka(initialTr.getEbDemandeNum(), user, false);
		}
	};

	public abstract void buildAndSendObject(Object... args) throws Exception;
}
