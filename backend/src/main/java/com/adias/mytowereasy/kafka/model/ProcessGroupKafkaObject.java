package com.adias.mytowereasy.kafka.model;

public class ProcessGroupKafkaObject {
    private Integer companyId;
    private Integer triggerCode;
    private Integer userId;
		private Integer initialTrId;

    public ProcessGroupKafkaObject(Integer companyId, Integer triggerCode, Integer userId) {
        this.companyId = companyId;
        this.triggerCode = triggerCode;
        this.userId = userId;
    }

		public ProcessGroupKafkaObject(
			Integer companyId,
			Integer triggerCode,
			Integer userId,
			Integer initialTrId
		)
		{
			super();
			this.companyId = companyId;
			this.triggerCode = triggerCode;
			this.userId = userId;
			this.initialTrId = initialTrId;
		}

		public ProcessGroupKafkaObject()
		{
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getTriggerCode() {
        return triggerCode;
    }

    public void setTriggerCode(Integer triggerCode) {
        this.triggerCode = triggerCode;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

		public Integer getInitialTrId()
		{
			return initialTrId;
		}

		public void setInitialTrId(Integer initialTrId)
		{
			this.initialTrId = initialTrId;
		}
}

