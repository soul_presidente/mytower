package com.adias.mytowereasy.kafka.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.airbus.repository.EbQrGroupeRepository;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.kafka.config.ProcessGroupStreamConfig;
import com.adias.mytowereasy.kafka.model.ProcessGroupKafkaObject;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.service.QrConsolidationService;


@Service
@ConditionalOnProperty(prefix = "mytower", name = "enable.process.grouping", havingValue = "true")
public class ProcessGroupConsumer {

    private final QrConsolidationService qrConsolidationService;
    private final EbQrGroupeRepository ebQrGroupeRepository;
    private final EbUserRepository ebUserRepository;

		@Autowired
		public EbDemandeRepository					ebDemandeRepository;

    public ProcessGroupConsumer(QrConsolidationService qrConsolidationService, EbQrGroupeRepository ebQrGroupeRepository, EbUserRepository ebUserRepository) {
        this.qrConsolidationService = qrConsolidationService;
        this.ebQrGroupeRepository = ebQrGroupeRepository;
        this.ebUserRepository = ebUserRepository;
    }

    @StreamListener(ProcessGroupStreamConfig.PROCESS_GROUP_INPUT)
    public void consumeProcessGroup(@Payload ProcessGroupKafkaObject pgko) {

        try {
            List<Integer> groupIds = ebQrGroupeRepository.selectListEbQrGroupeNumByCompagnieNum(pgko.getCompanyId());

            EbUser connectedUser = ebUserRepository
                .findById(pgko.getUserId())
                .orElseThrow(() -> new MyTowerException(String.format("User with id %s not found", pgko.getUserId())));

            Integer idTrResult = ebDemandeRepository.findxEbDemandeGroupByInitialTrId(pgko.getInitialTrId());
            ebDemandeRepository.setInitialTrResultanteTrToNull(pgko.getInitialTrId());

            try {
                qrConsolidationService
                    .refreshQrGroupe(groupIds, pgko.getTriggerCode(), pgko.getCompanyId(), connectedUser);
            } catch (Exception e) {
                e.printStackTrace();
            }

            qrConsolidationService.sendGroupedTrToKafka(pgko.getInitialTrId(), idTrResult);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
