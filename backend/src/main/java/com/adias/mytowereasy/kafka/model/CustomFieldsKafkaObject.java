package com.adias.mytowereasy.kafka.model;

import com.adias.mytowereasy.model.CustomFields;


public class CustomFieldsKafkaObject {
    private String value;
    private String label;
    private String code;

    public CustomFieldsKafkaObject() {
    }

    public CustomFieldsKafkaObject(CustomFields customFields) {
        this.value = customFields.getValue();
        this.label = customFields.getLabel();
        this.code = customFields.getName();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
