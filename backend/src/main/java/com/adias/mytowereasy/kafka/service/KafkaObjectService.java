package com.adias.mytowereasy.kafka.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.adias.mytowereasy.airbus.model.QrGroupeObject;
import com.adias.mytowereasy.dto.EbTTPslAppAndEventsProjection;
import com.adias.mytowereasy.dto.EbTteventProjection;
import com.adias.mytowereasy.kafka.model.CallForCollectionObject;
import com.adias.mytowereasy.kafka.model.CostKafkaObject;
import com.adias.mytowereasy.kafka.model.DocumentKafkaObject;
import com.adias.mytowereasy.kafka.model.EventKafkaObject;
import com.adias.mytowereasy.kafka.model.ProcessGroupKafkaObject;
import com.adias.mytowereasy.kafka.model.TransportRequestKafkaObject;
import com.adias.mytowereasy.model.EbAdditionalCost;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbDemandeFichiersJoint;
import com.adias.mytowereasy.model.EbTypeFlux;
import com.adias.mytowereasy.model.EbTypeTransport;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.model.Enumeration.NatureDemandeTransport;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;
import com.adias.mytowereasy.repository.EbAdditionalCostRepository;
import com.adias.mytowereasy.repository.EbCompagnieRepository;
import com.adias.mytowereasy.repository.EbDemandeFichiersJointRepositorty;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.PricingService;
import com.adias.mytowereasy.service.TrackService;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Service
public class KafkaObjectService extends MyTowerService {
    private final PricingService pricingService;
    private final ProducerService producerService;
    private final TrackService trackService;

    private final ProcessGroupProducerService processGroupProducerService;

    private static final String API_INTERNAL_DOWNLOAD_FILE = "api/internal/download-file/";

		private static final Logger							LOGGER											= LoggerFactory
			.getLogger(KafkaObjectService.class);

    @Value("${mytowereasy.url.back}")
    private String urlBack;

    @Autowired
    private EbDemandeFichiersJointRepositorty ebDemandeFichiersJointRepositorty;
    @Autowired
    private EbAdditionalCostRepository ebAdditionalCostRepository;

    @Autowired
    private EbCompagnieRepository ebCompagnieRepository;

    public KafkaObjectService(
            PricingService pricingService,
            ProducerService producerService,
            TrackService trackService, ProcessGroupProducerService processGroupProducerService) {
        this.pricingService = pricingService;
        this.producerService = producerService;
        this.trackService = trackService;
        this.processGroupProducerService = processGroupProducerService;
    }

    // Cette methode permet de chercher la TR pour laquelle il faut envoyer un
    // object call for collection et construit l'objet
    public CallForCollectionObject getCallForCollectioObjectFromEbDemande(Integer ebDemandeNum) throws Exception {
        EbDemande ebDemande = getDemande(ebDemandeNum, null);

        return new CallForCollectionObject(ebDemande);
    }

    public TransportRequestKafkaObject getTransportRequestKafkaObjectWithEventFromDemande(
        Integer ebDemandeNum,
        Integer tracingNum,
        EbUser userConnected)
        throws Exception {
        TransportRequestKafkaObject transportRequestKafkaObject = new TransportRequestKafkaObject(
            getDemande(ebDemandeNum, userConnected));

        if (tracingNum != null) {
            List<EbTTPslAppAndEventsProjection> ebTTPslAppAndEventsProjectionList = trackService
                .getTracingPslAndsThiereEvents(tracingNum);
            transportRequestKafkaObject.setEvents(new ArrayList<>());

            for (EbTTPslAppAndEventsProjection ebTTPslAppAndEventsProjection: ebTTPslAppAndEventsProjectionList) {
                if (ebTTPslAppAndEventsProjection != null &&
                    ebTTPslAppAndEventsProjection.getListEvent() != null &&
                    !ebTTPslAppAndEventsProjection
                        .getListEvent()
                        .isEmpty()) for (EbTteventProjection ebTteventProjection: ebTTPslAppAndEventsProjection
                            .getListEvent()) {

                                if (ebTteventProjection != null) {
                                    EventKafkaObject eventKafkaObject = new EventKafkaObject(
                                        ebTTPslAppAndEventsProjection,
                                        ebTteventProjection,
                                        null);
                                    transportRequestKafkaObject.getEvents().add(eventKafkaObject);
                                }

                            }
            }

        }

        return transportRequestKafkaObject;
    }

    public TransportRequestKafkaObject getTransportRequestKafkaObject(Integer ebDemandeNum, EbUser user)
        throws Exception {
        EbDemande ebDemande = getDemande(ebDemandeNum, user);
        return getTransportRequestKafkaObject(ebDemande);
    }

    public TransportRequestKafkaObject getTransportRequestKafkaObject(EbDemande ebDemande) throws Exception {
        TransportRequestKafkaObject transportRequestKafkaObject = new TransportRequestKafkaObject(ebDemande);

        EbTypeFlux typeFlux = ebTypeFluxRepository.findOneByEbTypeFluxNum(ebDemande.getxEbTypeFluxNum());
        if (typeFlux != null) transportRequestKafkaObject.setFlowTypeCode(typeFlux.getCode());

        if (ebDemande.getxEbTypeTransport() != null) {
            ebDemande.setxEbTypeFluxCode(typeFlux.getCode());
            Optional<EbTypeTransport> typeTransport = ebTypeTransportRepository
                .findById(ebDemande.getxEbTypeTransport());

            if (typeTransport.isPresent()) {
                transportRequestKafkaObject.setTypeConsolidation(typeTransport.get().getLibelle());
            }

        }

        if (ebDemande.getxEbCompagnie() != null && ebDemande.getxEbCompagnie().getEbCompagnieNum() != null) {
            EbCompagnie compagnie = ebCompagnieRepository
                .findOneByEbCompagnieNum(ebDemande.getxEbCompagnie().getEbCompagnieNum());
            if (compagnie != null) transportRequestKafkaObject.setEbCompagnie(compagnie);
        }

        handleTrancing(ebDemande.getEbDemandeNum(), transportRequestKafkaObject);
        handleAdditionnelCost(ebDemande, transportRequestKafkaObject);

        // set trs intials if exist
        if (NatureDemandeTransport.CONSOLIDATION.getCode().equals(ebDemande.getxEcNature())) {

            if (CollectionUtils.isEmpty(ebDemande.getInitialTrInGroupedTransportRequest())) {
                pricingService.addListDemandeInitials(ebDemande);
            }

            transportRequestKafkaObject.setInitialTransportList(new ArrayList<>());

            for (EbDemande transportRequest: ebDemande.getInitialTrInGroupedTransportRequest()) {
                transportRequestKafkaObject
                    .getInitialTransportList().add(new TransportRequestKafkaObject(transportRequest));
            }

        }

        return transportRequestKafkaObject;
    }

    private void handleAdditionnelCost(EbDemande ebDemande, TransportRequestKafkaObject transportRequestKafkaObject) {

        if (ebDemande.getListAdditionalCost() != null && !ebDemande.getListAdditionalCost().isEmpty()) {
            List<CostKafkaObject> listCostField = new ArrayList<>();

            ebDemande.getListAdditionalCost().forEach(ebCost -> {
                CostKafkaObject costField = new CostKafkaObject(ebCost);

                if (ebCost.getxEcCurrencyNum() != null) {
                    Optional<EcCurrency> ecCurrency = ecCurrencyRepository.findById(ebCost.getxEcCurrencyNum());

                    if (ecCurrency.isPresent()) {
                        costField.setCurrency(ecCurrency.get().getCode());
                    }

                }

                if (ebCost.getLibelle() != null) {
                    EbAdditionalCost additionalCost = ebAdditionalCostRepository
                        .selectEbAdditionalCostByLibelle(ebCost.getLibelle());

                    if (additionalCost != null) {
                        costField.setCode(additionalCost.getCode());
                    }

                }

                listCostField.add(costField);
            });
            transportRequestKafkaObject.setAdditionalCostList(listCostField);
        }

    }

    private void handleTrancing(Integer ebDemandeNum, TransportRequestKafkaObject transportRequestKafkaObject) {
        List<Integer> listTracingNum = ebTrackTraceRepository.findListTracingNumByEbDemandeNum(ebDemandeNum);

        transportRequestKafkaObject.setEvents(new ArrayList<>());

        if (listTracingNum != null && !listTracingNum.isEmpty()) {

            for (Integer tracingNum: listTracingNum) {
                List<EbTTPslAppAndEventsProjection> ebTTPslAppAndEventsProjectionList = trackService
                    .getTracingPslAndsThiereEvents(tracingNum);

                Integer lastEventNum = null;
                Date lastEventDate = null;

                // chercher le dernier event crée
                for (EbTTPslAppAndEventsProjection ebTTPslAppAndEventsProjection: ebTTPslAppAndEventsProjectionList) {

                    if (lastEventNum == null &&
                        ebTTPslAppAndEventsProjection != null && ebTTPslAppAndEventsProjection.getListEvent() != null &&
                        !ebTTPslAppAndEventsProjection.getListEvent().isEmpty()) {
                        lastEventNum = ebTTPslAppAndEventsProjection.getListEvent().get(0).getEbTtEventNum();

                        lastEventDate = ebTTPslAppAndEventsProjection.getListEvent().get(0).getDateMajSys();
                    }

                    if (ebTTPslAppAndEventsProjection.getListEvent() != null &&
                        !ebTTPslAppAndEventsProjection.getListEvent().isEmpty()) {

                        for (EbTteventProjection ebTteventProjection: ebTTPslAppAndEventsProjection.getListEvent()) {

                            if (lastEventDate != null &&
                                ebTteventProjection.getDateMajSys() != null &&
                                lastEventDate.compareTo(ebTteventProjection.getDateMajSys()) < 0) {
                                lastEventNum = ebTteventProjection.getEbTtEventNum();

                                lastEventDate = ebTteventProjection.getDateMajSys();
                            }

                        }

                    }

                }

                for (EbTTPslAppAndEventsProjection ebTTPslAppAndEventsProjection: ebTTPslAppAndEventsProjectionList) {
                    if (ebTTPslAppAndEventsProjection != null &&
                        ebTTPslAppAndEventsProjection.getListEvent() != null &&
                        !ebTTPslAppAndEventsProjection
                            .getListEvent()
                            .isEmpty()) for (EbTteventProjection ebTteventProjection: ebTTPslAppAndEventsProjection
                                .getListEvent()) {

                                    if (ebTteventProjection != null) {
                                        EventKafkaObject eventKafkaObject = new EventKafkaObject(
                                            ebTTPslAppAndEventsProjection,
                                            ebTteventProjection,
                                            lastEventNum != null &&
                                                lastEventNum.equals(ebTteventProjection.getEbTtEventNum()));
                                        transportRequestKafkaObject.getEvents().add(eventKafkaObject);
                                    }

                                }
                }

            }

        }

    }

    public EbDemande getDemande(Integer ebDemandeNum, EbUser connectedUser) throws Exception {
        SearchCriteriaPricingBooking searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();
        searchCriteriaPricingBooking.setEbDemandeNum(ebDemandeNum);
        searchCriteriaPricingBooking.setWithListMarchandise(true);
        EbDemande ebDemande = pricingService.getEbDemande(searchCriteriaPricingBooking, connectedUser);
        return ebDemande;
    }

    public void
        sendTransportRequestToKafkaTopic(Integer demandeNum, String topicName, Boolean isTrackinTopic, EbUser user)
            throws Exception {
        TransportRequestKafkaObject transportRequestKafkaObject = getTransportRequestKafkaObject(demandeNum, user);

        if (transportRequestKafkaObject != null) {
            this.producerService.sendTransportRequestKafkaObject(transportRequestKafkaObject, isTrackinTopic);
        }

    }

    public String sendDocumentToKafkaTopic(Integer ebDemandeNum, EbDemandeFichiersJoint fichiersJoint) {
        List<Integer> listDemandeNum = new ArrayList<>();
        listDemandeNum.add(ebDemandeNum);
        List<EbDemandeFichiersJoint> listDemandeFichiersJoint;

        // apres upload de doc
        if (fichiersJoint != null) {
            listDemandeFichiersJoint = new ArrayList<>();
            listDemandeFichiersJoint.add(fichiersJoint);
        }
        else // apres le Call For Collection
        {
            listDemandeFichiersJoint = ebDemandeFichiersJointRepositorty.findAllByNumEntityIn(listDemandeNum);
        }

        if (listDemandeFichiersJoint != null && !listDemandeFichiersJoint.isEmpty()) {
            // get carrierCode and scenario
            EbDemande ebDemande = ebDemandeRepository.getOne(ebDemandeNum);
            List<ExEbDemandeTransporteur> exEbDemandeTransporteurs = ebDemande.getExEbDemandeTransporteurs();
            String scenario = ebDemande.getScenario();
            String selectedCarrierCode = exEbDemandeTransporteurs != null &&
                exEbDemandeTransporteurs.size() > 0 && exEbDemandeTransporteurs.get(0) != null &&
                exEbDemandeTransporteurs.get(0).getxEbEtablissement() != null &&
                exEbDemandeTransporteurs.get(0).getxEbEtablissement().getEbCompagnie() != null ?
                    exEbDemandeTransporteurs.get(0).getxEbEtablissement().getEbCompagnie().getCode() :
                    null;
            String refTransport = ebDemande.getRefTransport();

            List<Integer> listMarchandiseNum = listDemandeFichiersJoint
                .stream().filter(el -> el.getxEbMarchandise() != null).map(EbDemandeFichiersJoint::getxEbMarchandise)
                .collect(Collectors.toList());
            List<QrGroupeObject> listUnitRef = null;

            if (listMarchandiseNum != null && !listMarchandiseNum.isEmpty()) {
                listUnitRef = ebMarchandiseRepository.findUnitRefByListMarchandiseNum(listMarchandiseNum);
            }

            for (EbDemandeFichiersJoint file: listDemandeFichiersJoint) {

                if (file.getxEbMarchandise() != null && listUnitRef != null) {
                    QrGroupeObject unit = listUnitRef
                        .stream().filter(el -> el.getCode().equals(file.getxEbMarchandise())).findFirst().orElse(null);
                    file.setRefTransportAndUnit(refTransport + "," + unit.getLibelle());
                }
                else {
                    file.setRefTransportAndUnit(refTransport + ",#");
                }

            }

            List<DocumentKafkaObject> documentKafkaObjects = listEbDemandeFichiersJointToListDocumentKafka(
                listDemandeFichiersJoint);

            if (documentKafkaObjects != null && !documentKafkaObjects.isEmpty()) {
                producerService.sendDocumentObjects(documentKafkaObjects, scenario, selectedCarrierCode);
                return "Message sent successfully";
            }

        }

        return "Failed to send message";
    }

    private List<DocumentKafkaObject>
        listEbDemandeFichiersJointToListDocumentKafka(List<EbDemandeFichiersJoint> listDemandeFichiersJoint) {
        String envPath = urlBack + API_INTERNAL_DOWNLOAD_FILE;

        List<DocumentKafkaObject> documentKafkaObjects = new ArrayList<>();

        for (EbDemandeFichiersJoint file: listDemandeFichiersJoint) {
            documentKafkaObjects.add(new DocumentKafkaObject(file, envPath));
        }

        return documentKafkaObjects;
    }

    public void buildAndSendTransportRequestToKafka(Integer ebDemandeNum, EbUser user, Boolean isTrackingTopic)
		{
			try
			{
				producerService
					.sendTransportRequestKafkaObject(
						getTransportRequestKafkaObject(ebDemandeNum, user),
						isTrackingTopic
					);
			}
			catch (Exception e)
			{
				LOGGER.error("Problem de build du message kafka", e);
			}
    }

		public void buildAndSendTransportRequestToKafka(EbDemande ebDemande, Boolean isTrackingTopic)
		{
			try
			{
				producerService
					.sendTransportRequestKafkaObject(
						getTransportRequestKafkaObject(ebDemande),
						isTrackingTopic
					);
			}
			catch (Exception e)
			{
				LOGGER.error("Problem de build du message kafka", e);
			}
    }

		public void buildAndSendProcessGroupToKafka(
			Integer companyId,
			Integer triggerCode,
			Integer userId,
			Integer transportRequestId
		)
		{
			ProcessGroupKafkaObject pgko = new ProcessGroupKafkaObject(
				companyId,
				triggerCode,
				userId,
				transportRequestId
			);
        processGroupProducerService.sendProcessGroupService(pgko);
    }
}
