package com.adias.mytowereasy.kafka.model;

import com.adias.mytowereasy.model.EbDemandeFichiersJoint;


public class DocumentKafkaObject {
    private String transportReference;
    private String documentTypeCode;
    private String documentTitle; // file name sur l'interface
    private String fileURL;
    private String fileName; // le nom réel
    private String unitReference;

    public DocumentKafkaObject(EbDemandeFichiersJoint file, String envPath) {
        this.documentTypeCode = file.getIdCategorie();
        this.documentTitle = file.getOriginFileName();
        this.fileURL = envPath + file.getEbDemandeFichierJointNum();
        this.fileName = file.getFileName();

        if (file.getRefTransportAndUnit() != null) {
            String[] refTransportAndUnit = file.getRefTransportAndUnit().split(",");
            String trRef = refTransportAndUnit[0];
            String unitRef = refTransportAndUnit[1];
            this.transportReference = trRef;
            this.unitReference = !unitRef.equals("#") ? unitRef : null;
        }

    }

    public String getTransportReference() {
        return transportReference;
    }

    public void setTransportReference(String transportReference) {
        this.transportReference = transportReference;
    }

    public String getDocumentTypeCode() {
        return documentTypeCode;
    }

    public void setDocumentTypeCode(String documentTypeCode) {
        this.documentTypeCode = documentTypeCode;
    }

    public String getDocumentTitle() {
        return documentTitle;
    }

    public void setDocumentTitle(String documentTitle) {
        this.documentTitle = documentTitle;
    }

    public String getFileURL() {
        return fileURL;
    }

    public void setFileURL(String fileURL) {
        this.fileURL = fileURL;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getUnitReference() {
        return unitReference;
    }

    public void setUnitReference(String unitReference) {
        this.unitReference = unitReference;
    }
}
