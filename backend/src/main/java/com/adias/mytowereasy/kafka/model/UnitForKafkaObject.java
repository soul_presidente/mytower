package com.adias.mytowereasy.kafka.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.Enumeration;


public class UnitForKafkaObject {
    private Double weight;
    private Double volume;
    private Integer numberOfUnits;
    private Double length;
    private Double width;
    private Double height;
    private String unitReference;
    private String unitType;

    private String manifactoringCountryCode;
    private String serialNumber;
    private Boolean exportControl;

    private String hsCode;

    private String dangerousGood;
    private String un;
    private String classGood;
    private String partNumber;
    private String manufacturerCode;
    private String comment;
    private BigDecimal customsPrice;
    private Boolean stackable;
    private String parentUnitReference;
    private String initialOrderReference;
    private String binLocation;
    private String packaging;
    private List<CustomFieldsKafkaObject> customFieldsKafkaObjectList;
    private String typeOfGoodLabel;
    private String typeOfGoodCode;

    public UnitForKafkaObject() {
    }

    public UnitForKafkaObject(EbMarchandise ebMarchandise) {
        this.weight = ebMarchandise.getWeight();
        this.volume = ebMarchandise.getVolume();
        this.numberOfUnits = ebMarchandise.getNumberOfUnits();
        this.length = ebMarchandise.getLength();
        this.width = ebMarchandise.getWidth();
        this.height = ebMarchandise.getHeigth();
        this.unitReference = ebMarchandise.getUnitReference();
        this.unitType = ebMarchandise.getLabelTypeUnit();
        this.typeOfGoodLabel = ebMarchandise.getTypeGoodsLabel();
        this.typeOfGoodCode = ebMarchandise.getTypeOfGoodCode();
        this.serialNumber = ebMarchandise.getSerialNumber();
        this.exportControl = ebMarchandise.getExportControl();
        this.hsCode = ebMarchandise.getHsCode();
        this.dangerousGood = (ebMarchandise.getDangerousGood() != null) ?
            Enumeration.MarchandiseDangerousGood.getByCode(ebMarchandise.getDangerousGood()).getLibelle() :
            null;
        this.un = ebMarchandise.getUn();
        this.classGood = ebMarchandise.getClassGood();
        this.partNumber = ebMarchandise.getPartNumber();
        this.comment = ebMarchandise.getComment();
        this.stackable = ebMarchandise.getStackable();
        // this.customsPrice = ebMarchandise.getCustomsPrice();

        if (ebMarchandise.getParent() != null) {
            this.parentUnitReference = ebMarchandise.getParent().getUnitReference();
        }

        this.initialOrderReference = ebMarchandise.getOrderNumber();
        this.binLocation = ebMarchandise.getBinLocation();

        // Champs à clarifier
        this.manifactoringCountryCode = null;
        this.manufacturerCode = null;
        this.packaging = ebMarchandise.getPackaging();

        this.customFieldsKafkaObjectList = new ArrayList<>();

        if (ebMarchandise.getListCustomsFields() != null) {
            ebMarchandise.setListCustomsFields(ebMarchandise.getListCustomsFields());

            for (CustomFields customFields: ebMarchandise.getListCustomsFields()) {
                this.customFieldsKafkaObjectList.add(new CustomFieldsKafkaObject(customFields));
            }

        }

    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Integer getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(Integer numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getUnitReference() {
        return unitReference;
    }

    public void setUnitReference(String unitReference) {
        this.unitReference = unitReference;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public String getManifactoringCountryCode() {
        return manifactoringCountryCode;
    }

    public void setManifactoringCountryCode(String manifactoringCountryCode) {
        this.manifactoringCountryCode = manifactoringCountryCode;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Boolean getExportControl() {
        return exportControl;
    }

    public void setExportControl(Boolean exportControl) {
        this.exportControl = exportControl;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public String getDangerousGood() {
        return dangerousGood;
    }

    public void setDangerousGood(String dangerousGood) {
        this.dangerousGood = dangerousGood;
    }

    public String getUn() {
        return un;
    }

    public void setUn(String un) {
        this.un = un;
    }

    public String getClassGood() {
        return classGood;
    }

    public void setClassGood(String classGood) {
        this.classGood = classGood;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getManufacturerCode() {
        return manufacturerCode;
    }

    public void setManufacturerCode(String manufacturerCode) {
        this.manufacturerCode = manufacturerCode;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public BigDecimal getCustomsPrice() {
        return customsPrice;
    }

    public void setCustomsPrice(BigDecimal customsPrice) {
        this.customsPrice = customsPrice;
    }

    public String getParentUnitReference() {
        return parentUnitReference;
    }

    public void setParentUnitReference(String parentUnitReference) {
        this.parentUnitReference = parentUnitReference;
    }

    public String getInitialOrderReference() {
        return initialOrderReference;
    }

    public void setInitialOrderReference(String initialOrderReference) {
        this.initialOrderReference = initialOrderReference;
    }

    public String getBinLocation() {
        return binLocation;
    }

    public void setBinLocation(String binLocation) {
        this.binLocation = binLocation;
    }

    public Boolean getStackable() {
        return stackable;
    }

    public void setStackable(Boolean stackable) {
        this.stackable = stackable;
    }

    public String getPackaging() {
        return packaging;
    }

    public void setPackaging(String packaging) {
        this.packaging = packaging;
    }

    public String getTypeOfGoodLabel() {
        return typeOfGoodLabel;
    }

    public void setTypeOfGoodLabel(String typeOfGoodLabel) {
        this.typeOfGoodLabel = typeOfGoodLabel;
    }

    public String getTypeOfGoodCode() {
        return typeOfGoodCode;
    }

    public void setTypeOfGoodCode(String typeOfGoodCode) {
        this.typeOfGoodCode = typeOfGoodCode;
    }

}
