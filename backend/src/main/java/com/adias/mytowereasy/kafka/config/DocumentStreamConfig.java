package com.adias.mytowereasy.kafka.config;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;


public interface DocumentStreamConfig {
    String TRANSACTION_OUTPUT_TOPIC = "document-output";

    @Output(TRANSACTION_OUTPUT_TOPIC)
    MessageChannel outputMessage();
}
