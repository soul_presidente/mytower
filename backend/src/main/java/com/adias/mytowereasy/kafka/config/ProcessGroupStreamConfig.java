package com.adias.mytowereasy.kafka.config;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;


public interface ProcessGroupStreamConfig {
    String PROCESS_GROUP_INPUT = "processGroupInput";

    @Input
    SubscribableChannel processGroupInput();
    @Output
    MessageChannel processGroupOutput();
}
