package com.adias.mytowereasy.kafka.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbMarchandise;


public class CallForCollectionObject {
    private String transportRef;
    private Date callForCollectionDate;
    private String transportPriority;
    private List<UnitForKafkaObject> unitForKafkaObjectList;

    public String getTransportRef() {
        return transportRef;
    }

    public void setTransportRef(String transportRef) {
        this.transportRef = transportRef;
    }

    public Date getCallForCollectionDate() {
        return callForCollectionDate;
    }

    public void setCallForCollectionDate(Date callForCollectionDate) {
        this.callForCollectionDate = callForCollectionDate;
    }

    public String getTransportPriority() {
        return transportPriority;
    }

    public void setTransportPriority(String transportPriority) {
        this.transportPriority = transportPriority;
    }

    public List<UnitForKafkaObject> getUnitForKafkaObjectList() {
        return unitForKafkaObjectList;
    }

    public void setUnitForKafkaObjectList(List<UnitForKafkaObject> unitForKafkaObjectList) {
        this.unitForKafkaObjectList = unitForKafkaObjectList;
    }

    public CallForCollectionObject() {
    }

    public CallForCollectionObject(
        String transportRef,
        Date callForCollectionDate,
        String transportPriority,
        List<UnitForKafkaObject> unitForKafkaObjectList) {
        this.transportRef = transportRef;
        this.callForCollectionDate = callForCollectionDate;
        this.transportPriority = transportPriority;
        this.unitForKafkaObjectList = unitForKafkaObjectList;
    }

    public CallForCollectionObject(EbDemande demande) {
        this.transportRef = demande.getRefTransport();
        this.callForCollectionDate = new Date();
        // this.transportPriority = demande.getTransportPriority();

        this.unitForKafkaObjectList = new ArrayList<>();
        if (!demande.getListMarchandises().isEmpty()) for (EbMarchandise ebMarchandise: demande.getListMarchandises()) {
            this.unitForKafkaObjectList.add(new UnitForKafkaObject(ebMarchandise));
        }
    }
}
