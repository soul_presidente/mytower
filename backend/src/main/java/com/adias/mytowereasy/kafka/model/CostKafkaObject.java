package com.adias.mytowereasy.kafka.model;

import java.math.BigDecimal;

import com.adias.mytowereasy.model.EbCost;


public class CostKafkaObject {
    private String quoteReference;
    private String code;

    private String libelle;
    private BigDecimal price;
    private BigDecimal priceEuro;
    private String currency;
    private String type;

    public String getQuoteReference() {
        return quoteReference;
    }

    public void setQuoteReference(String quoteReference) {
        this.quoteReference = quoteReference;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPriceEuro() {
        return priceEuro;
    }

    public void setPriceEuro(BigDecimal priceEuro) {
        this.priceEuro = priceEuro;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public CostKafkaObject(EbCost cost) {
        this.libelle = cost.getLibelle();
        this.price = cost.getPrice();
        this.priceEuro = cost.getPriceEuro();
        this.code = cost.getCode();
    }
}
