package com.adias.mytowereasy.kafka.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.kafka.config.DocumentStreamConfig;
import com.adias.mytowereasy.kafka.config.TransportRequestStreamConfig;
import com.adias.mytowereasy.kafka.model.DocumentKafkaObject;
import com.adias.mytowereasy.kafka.model.TransportRequestKafkaObject;
import com.adias.mytowereasy.util.JsonUtils;


@Service
public class ProducerService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String KAFKA_HEADER_CARRIER = "carrier";
    private static final String KAFKA_HEADER_STATUS = "status";
		private static final String								KAFKA_HEADER_CANCEL_STATUS	= "cancel_status";
    private static final String KAFKA_HEADER_SCENARIO = "scenario";
    private static final String KAFKA_HEADER_COMPAGNIE_CODE = "compagnie_code";
    private static final String IS_VALORIZED = "is_valorized";
    private static final MimeType APPLICATION_JSON = MimeTypeUtils.APPLICATION_JSON;
    private static final String CONTENT_TYPE = MessageHeaders.CONTENT_TYPE;

    private final TransportRequestStreamConfig transportRequestStream;
    private final DocumentStreamConfig documentStreamConfig;

    @Autowired
    public ProducerService(
        TransportRequestStreamConfig transportRequestStream,
        DocumentStreamConfig documentStreamConfig) {
        this.transportRequestStream = transportRequestStream;
        this.documentStreamConfig = documentStreamConfig;
    }

    // Envoyer le message 'transport request'
    public void sendTransportRequestKafkaObject(
        TransportRequestKafkaObject transportRequestKafkaObject,
        Boolean isTrackingTopic)
        throws JsonProcessingException {
        String msgType = !isTrackingTopic ? "BOOKING" : "TRACK-TRACE";
        JsonUtils.logJson(logger, null, "Start Publish kafka " + msgType);

        Message<TransportRequestKafkaObject> message = MessageBuilder
            .withPayload(transportRequestKafkaObject)
            .setHeaderIfAbsent(KAFKA_HEADER_STATUS, transportRequestKafkaObject.getTransportStatus())
					.setHeaderIfAbsent(
						KAFKA_HEADER_CANCEL_STATUS,
						transportRequestKafkaObject.isCanceled()
					)
            .setHeaderIfAbsent(KAFKA_HEADER_CARRIER, transportRequestKafkaObject.getSelectedCarrier())
            .setHeaderIfAbsent(KAFKA_HEADER_COMPAGNIE_CODE, transportRequestKafkaObject.getEbCompagnie().getCode())
            .setHeaderIfAbsent(IS_VALORIZED, transportRequestKafkaObject.isValorized())
            .setHeader(CONTENT_TYPE, APPLICATION_JSON).build();
        JsonUtils
            .logJson(
                logger,
                transportRequestKafkaObject,
                "Publish kafka " +
							msgType + " headers (" + KAFKA_HEADER_STATUS + ":" +
							transportRequestKafkaObject.getTransportStatus() + "," + KAFKA_HEADER_CANCEL_STATUS +
							":" + transportRequestKafkaObject.isCanceled() + "," + KAFKA_HEADER_CARRIER +
							":" + transportRequestKafkaObject.getSelectedCarrierCode() + "," + IS_VALORIZED +
							":" + transportRequestKafkaObject.isValorized() + "," + KAFKA_HEADER_COMPAGNIE_CODE +
							":" + transportRequestKafkaObject.getEbCompagnie().getCode() + ")"
					);

        if (!isTrackingTopic) {
           transportRequestStream.outputMessage().send(message);
        }
        else {
            // this.transportRequestStream.outputTrackTraceMessage().send(message);
        }

        JsonUtils.logJson(logger, null, "End Publish kafka " + msgType);
    }

    // Envoyer le message 'Document' infos
    public void sendDocumentObjects(
        List<DocumentKafkaObject> documentKafkaObjects,
        String scenario,
        String SelectedCarrierCode) {
        Message<?> messageToSend = null;

        for (DocumentKafkaObject doc: documentKafkaObjects) {
            this.documentStreamConfig
                .outputMessage().send(
                    MessageBuilder
                        .withPayload(doc).setHeaderIfAbsent(KAFKA_HEADER_SCENARIO, scenario)
                        .setHeaderIfAbsent(KAFKA_HEADER_CARRIER, SelectedCarrierCode)
                        .setHeader(CONTENT_TYPE, APPLICATION_JSON).build());
        }

    }
}
