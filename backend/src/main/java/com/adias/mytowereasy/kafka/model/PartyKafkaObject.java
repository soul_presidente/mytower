package com.adias.mytowereasy.kafka.model;

import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.trpl.model.EbPlZone;

public class PartyKafkaObject
{
	public PartyKafkaObject()
	{
	}
	private Integer itemId;
	private String	reference;
	private String	zipCode;
	private String	adresse;
	private String	city;
	private String	countryCode;
	private String	company;
	private String	companyCode;
	private String	email;
	private String comment;
	private String airport;
	private EbPlZone zone;
	private String zoneReference;
	private String openingHours;
	private String phone;


	public EbPlZone getZone() {
		return zone;
	}

	public void setZone(EbPlZone zone) {
		this.zone = zone;
	}

	public String getZoneReference() {
		return zoneReference;
	}

	public void setZoneReference(String zoneReference) {
		this.zoneReference = zoneReference;
	}

	public String getOpeningHours() {
		return openingHours;
	}

	public void setOpeningHours(String openingHours) {
		this.openingHours = openingHours;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public PartyKafkaObject(EbParty ebPartyOrigin)
	{
		this.itemId = ebPartyOrigin.getEbPartyNum();
		this.reference = ebPartyOrigin.getReference();
		this.city = ebPartyOrigin.getCity();
		if (ebPartyOrigin.getxEcCountry() != null)
		{
			this.countryCode = ebPartyOrigin.getxEcCountry().getCode();
		}
		this.company = ebPartyOrigin.getCompany();
		this.adresse = ebPartyOrigin.getAdresse();
		this.zipCode = ebPartyOrigin.getZipCode();
		this.zoneReference = ebPartyOrigin.getZoneRef();
		this.openingHours = ebPartyOrigin.getOpeningHours();
		this.phone = ebPartyOrigin.getPhone();
		this.email = ebPartyOrigin.getEmail();
		this.airport = ebPartyOrigin.getAirport();
		
	}

	public String getReference()
	{
		return reference;
	}

	public void setReference(String reference)
	{
		this.reference = reference;
	}

	public String getZipCode()
	{
		return zipCode;
	}

	public void setZipCode(String zipCode)
	{
		this.zipCode = zipCode;
	}

	public String getAdresse()
	{
		return adresse;
	}

	public void setAdresse(String adresse)
	{
		this.adresse = adresse;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public String getCountryCode()
	{
		return countryCode;
	}

	public void setCountryCode(String countryCode)
	{
		this.countryCode = countryCode;
	}

	public String getCompany()
	{
		return company;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public String getCompanyCode()
	{
		return companyCode;
	}

	public void setCompanyCode(String companyCode)
	{
		this.companyCode = companyCode;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getAirport() {
		return airport;
	}

	public void setAirport(String airport) {
		this.airport = airport;
	}
}
