package com.adias.mytowereasy.kafka.config;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@EnableBinding({
        DocumentStreamConfig.class, TransportRequestStreamConfig.class, ProcessGroupStreamConfig.class
})
@Configuration
public class KafkaStreamConfig {

}
