package com.adias.mytowereasy.kafka.service;

import com.adias.mytowereasy.kafka.config.ProcessGroupStreamConfig;
import com.adias.mytowereasy.kafka.model.ProcessGroupKafkaObject;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class ProcessGroupProducerService {
    final ProcessGroupStreamConfig processGroupStreamConfig;


    public ProcessGroupProducerService(ProcessGroupStreamConfig processGroupStreamConfig) {
        this.processGroupStreamConfig = processGroupStreamConfig;
    }

    public void sendProcessGroupService(ProcessGroupKafkaObject processGroupKafkaObject) {
        processGroupStreamConfig.processGroupOutput().send(MessageBuilder.withPayload(processGroupKafkaObject).build());
    }
}
