package com.adias.mytowereasy.kafka.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.api.wso.PslWSO;
import com.adias.mytowereasy.kafka.util.KafkaUtil;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbTypeDocuments;
import com.adias.mytowereasy.model.Enumeration.NatureDemandeTransport;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;
import com.adias.mytowereasy.model.enums.ConsolidationGroupage;


@JsonIgnoreProperties({
    "ebCompagnie"
})
public class TransportRequestKafkaObject {
    private Integer itemId;
    private String shipperUserEmail;
    private String transportReference;
    private String shipperUserName;
    private String shipperEtablishment;
    private String transportMode;
    private String invoicingCurrencyCode;
    private String transportStatus;
    private String serviceLevel;
    private String flowTypeCode;
    private String incoterm;
    private String transportType;
    private String customerReference;
    private String trackingPointSchema;
    private List<String> trackingPointCodeList;
    private Date arrivalDate;
    private Date customerCreationDate;
    private String goodsAvailabilityDate;
    private Boolean insurance;
    private Double insuranceValue;
    private String insuranceCurrencyCode;
    private String costCenterCode;
    private String numAwbBol;
    private String flightVessel;
    private String customsOffice;
    private String mawb;
    private String originUserEmail;
    private String originCustomUserEmail;
    private String destUserEmail;
    private String destCustomUserEmail;
    private List<String> observerUserEmailList;
    private PartyKafkaObject originInformation;
    private PartyKafkaObject destinationInformation;
    private List<QuotationKafkaObject> quotationList;
    private PartyKafkaObject notifyInformation;
    private PartyKafkaObject intermediaryPoint;
    private List<PslWSO> estimatedDateList;
    private List<UnitForKafkaObject> unitList;
    private String city;
    private Boolean eligibleForConsolidation;
    private Boolean finalChoiceToBeCalculated;
    private String consolidationStatus;
    private String transportUpdateDate;
    private String codeConfigurationEDI;
    private Boolean grouping;
    private boolean isValorized;
    private Double totalTaxableWeight;

    private String incotermOrig;
    private String incotermOrigCity;
    private String typeConsolidation;
    private String dateOfArrival;
    private String costCenter;
    private Integer transitTime;
    private String transportPriority;
    private String comment;
    private String selectedCarrier;
    private String selectedCarrierCode;
    private String awbNumber;

    private String estimatedTimeOfArrival;
    private String estimatedTimeOfDelivery;
    private String estimatedTimeOfPickup;

    private String requiredDeliveryDate;

    private Double totalWeight;
    private Double totalVolume;

    private List<PslKafkaObject> timestamps;

    private List<EventKafkaObject> events;

    private String airportOrigin;

    private String airportDest;

    private Date confirmationDate;

    private List<EbTypeDocuments> listTypeDocuments;

    private String typeFlux;

    private List<CategorieLabelKafkaObject> categorieLabels;

    private List<TransportRequestKafkaObject> initialTransportList;

    private EbCompagnie ebCompagnie;
    private List<CostKafkaObject> additionalCostList;
    private List<CustomFieldsKafkaObject> customFields;

		private boolean													canceled;

    public List<CustomFieldsKafkaObject> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(List<CustomFieldsKafkaObject> customFields) {
        this.customFields = customFields;
    }

    public TransportRequestKafkaObject() {
    }

    public TransportRequestKafkaObject(EbDemande demande) {
        this.itemId = demande.getEbDemandeNum();
        this.transportReference = demande.getRefTransport();
        this.incotermOrig = demande.getxEcIncotermLibelle();
        this.incotermOrigCity = demande.getCity();
        this.customerReference = demande.getCustomerReference();
        this.transportMode = demande.getModeTransporteur();
        this.goodsAvailabilityDate = KafkaUtil.getDate(demande.getDateOfGoodsAvailability());
        this.transportStatus = StatutDemande.getLibelleJasonByCode(demande.getxEcStatut());
        this.serviceLevel = demande.getTypeRequestLibelle();
        this.insurance = demande.getInsurance();
        this.insuranceValue = demande.getInsuranceValue();

        if (demande.getxEbCostCenter() != null) {
            this.costCenter = demande.getxEbCostCenter().getLibelle();
            this.costCenterCode = demande.getxEbCostCenter().getLibelle();
        }

        if (demande.getSelectedCarrier() != null) {
            this.transitTime = demande.getSelectedCarrier().getTransitTime();
            EbCompagnie compCarrier = demande.getSelectedCarrier().getxEbEtablissement() != null ?
                demande.getSelectedCarrier().getxEbEtablissement().getEbCompagnie() :
                null;

            if (compCarrier != null) {
                this.selectedCarrier = compCarrier.getNom();
                this.selectedCarrierCode = compCarrier.getCode();
            }

        }

        if (demande.getSelectedCarrier() != null) this.comment = demande.getSelectedCarrier().getComment();
        this.awbNumber = demande.getNumAwbBol();

        this.totalWeight = demande.getTotalWeight();
        this.totalVolume = demande.getTotalVolume();
        this.eligibleForConsolidation = demande.getEligibleForConsolidation();
        this.finalChoiceToBeCalculated = demande.getFinalChoiceToBeCalculated();
        this.transportUpdateDate = KafkaUtil.getDate(demande.getDateMaj());
        this.totalTaxableWeight = demande.getTotalTaxableWeight();
        this.grouping = demande.getxEcStatutGroupage() != null;
		this.canceled = demande.getxEcCancelled() != null;

        // ----- set party
        if (demande.getEbPartyOrigin() != null) {
            this.originInformation = new PartyKafkaObject(demande.getEbPartyOrigin());
        }

        if (demande.getEbPartyDest() != null) {
            this.destinationInformation = new PartyKafkaObject(demande.getEbPartyDest());
        }

        if (demande.getEbPartyNotif() != null) {
            this.notifyInformation = new PartyKafkaObject(demande.getEbPartyNotif());
        }

        this.quotationList = new ArrayList<>();

        if (demande.getExEbDemandeTransporteurs() != null && !demande.getExEbDemandeTransporteurs().isEmpty()) {

            for (ExEbDemandeTransporteur dt: demande.getExEbDemandeTransporteurs()) {
                this.quotationList.add(new QuotationKafkaObject(dt));
            }

        }

        this.unitList = new ArrayList<>();

        if (demande.getListMarchandises() != null) {

            for (EbMarchandise ebMarchandise: demande.getListMarchandises()) {

                if (!NatureDemandeTransport.CONSOLIDATION.getCode().equals(demande.getxEcNature())) {
                    ebMarchandise.setListCustomsFields(demande.getListCustomsFields());
                }

                this.unitList.add(new UnitForKafkaObject(ebMarchandise));
            }

        }

        this.timestamps = new ArrayList<>();
        if (demande.getxEbSchemaPsl()
            != null) for (EbTtCompanyPsl ebTtCompanyPsl: demande.getxEbSchemaPsl().getListPsl()) {
                this.timestamps.add(new PslKafkaObject(ebTtCompanyPsl));
            }
        this.typeFlux = demande.getxEbTypeFluxCode();
        this.customFields = new ArrayList<>();

        if (demande.getListCustomsFields() != null && !demande.getListCustomsFields().isEmpty()) {

            for (CustomFields cf: demande.getListCustomsFields()) {

                if (cf.getValue() != null && !cf.getValue().isEmpty()) {
                    CustomFieldsKafkaObject customField = new CustomFieldsKafkaObject(cf);
                    this.customFields.add(customField);
                }

            }

        }
        else if (demande.getCustomFields() != null && !demande.getCustomFields().isEmpty()) {
            ObjectMapper objectMapper = new ObjectMapper();
            List<CustomFieldsKafkaObject> list = new ArrayList<>();
            List<CustomFields> liste = new ArrayList<>();

            try {
                liste = objectMapper.readValue(demande.getCustomFields(), new TypeReference<ArrayList<CustomFields>>() {
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!liste.isEmpty() && liste != null) {

                for (CustomFields cf: liste) {
                    list.add(new CustomFieldsKafkaObject(cf));
                }

            }

            this.customFields = list;
        }

        this.categorieLabels = new ArrayList<>();

        if (demande.getListCategories() != null) {

            for (EbCategorie ebCategorie: demande.getListCategories()) {
                this.categorieLabels.add(new CategorieLabelKafkaObject(ebCategorie));
            }

        }

        this.arrivalDate = demande.getDateOfArrival();

        if (demande.getUser() != null) {
            this.shipperUserEmail = demande.getUser().getEmail();
            this.shipperUserName = demande.getUser().getNomPrenom();
            this.shipperEtablishment = demande.getUser().getEbEtablissement().getNom();
        }

        if (demande.getXecCurrencyInvoice()
            != null) this.invoicingCurrencyCode = demande.getXecCurrencyInvoice().getCode();
        this.transportType = demande.getTypeRequestLibelle();

        if (demande.getxEbSchemaPsl() != null) {
            this.trackingPointSchema = demande.getxEbSchemaPsl().getDesignation();

            if (demande.getxEbSchemaPsl().getListPsl() != null && !demande.getxEbSchemaPsl().getListPsl().isEmpty()) {
                List<String> trackingPointCodeList = (demande.getxEbSchemaPsl().getListPsl() != null &&
                    demande.getxEbSchemaPsl().getListPsl().isEmpty()) ?
                        demande
                            .getxEbSchemaPsl().getListPsl().stream().filter(psl -> psl.getSchemaActif())
                            .map(it -> it.getCodeAlpha()).collect(Collectors.toList()) :
                        null;
                this.trackingPointCodeList = trackingPointCodeList;
            }

        }

        this.customerCreationDate = demande.getCustomerCreationDate();
        this.flightVessel = demande.getFlightVessel();
        this.customsOffice = demande.getCustomsOffice();
        this.mawb = demande.getMawb();

        if (demande.getxEbUserOrigin() != null) {
            this.originUserEmail = demande.getxEbUserOrigin().getEmail();
        }

        if (demande.getxEbUserOriginCustomsBroker() != null) {
            this.originCustomUserEmail = demande.getxEbUserOriginCustomsBroker().getEmail();
        }

        if (demande.getxEbUserOriginCustomsBroker() != null) {
            this.originCustomUserEmail = demande.getxEbUserOriginCustomsBroker().getEmail();
        }

        if (demande.getxEbUserDestCustomsBroker() != null) {
            this.destCustomUserEmail = demande.getxEbUserDestCustomsBroker().getEmail();
        }

        if (demande.getxEbUserObserver() != null && !demande.getxEbUserObserver().isEmpty()) {
            List<String> observerUserEmailList = demande
                .getxEbUserObserver().stream().map(us -> us.getEmail()).collect(Collectors.toList());
            this.observerUserEmailList = observerUserEmailList;
        }

        this.consolidationStatus = ConsolidationGroupage.getCodeAlphaByCode(demande.getxEcStatutGroupage());
        this.codeConfigurationEDI = demande.getCodeConfigurationEDI();
        this.goodsAvailabilityDate = KafkaUtil.getDate(demande.getDateOfGoodsAvailability());
        this.dateOfArrival = KafkaUtil.getDate(demande.getDateOfArrival());
        this.estimatedTimeOfPickup = KafkaUtil.getDate(demande.getDatePickup());
        this.isValorized = demande.getValorized() != null ? demande.getValorized() : false;
    }

    public List<TransportRequestKafkaObject> getInitialTransportList() {
        return initialTransportList;
    }

    public void setInitialTransportList(List<TransportRequestKafkaObject> initialTransportList) {
        this.initialTransportList = initialTransportList;
    }

    public String getTransportReference() {
        return transportReference;
    }

    public void setTransportReference(String transportReference) {
        this.transportReference = transportReference;
    }

    public String getShipperUserEmail() {
        return shipperUserEmail;
    }

    public void setShipperUserEmail(String shipperUserEmail) {
        this.shipperUserEmail = shipperUserEmail;
    }

    public String getIncotermOrig() {
        return incotermOrig;
    }

    public void setIncotermOrig(String incotermOrig) {
        this.incotermOrig = incotermOrig;
    }

    public String getIncotermOrigCity() {
        return incotermOrigCity;
    }

    public void setIncotermOrigCity(String incotermOrigCity) {
        this.incotermOrigCity = incotermOrigCity;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getTransportMode() {
        return transportMode;
    }

    public void setTransportMode(String transportMode) {
        this.transportMode = transportMode;
    }

    public String getTypeConsolidation() {
        return typeConsolidation;
    }

    public void setTypeConsolidation(String typeConsolidation) {
        this.typeConsolidation = typeConsolidation;
    }

    public String getDateOfArrival() {
        return dateOfArrival;
    }

    public void setDateOfArrival(String dateOfArrival) {
        this.dateOfArrival = dateOfArrival;
    }

    public String getTransportStatus() {
        return transportStatus;
    }

    public void setTransportStatus(String transportStatus) {
        this.transportStatus = transportStatus;
    }

    public String getServiceLevel() {
        return serviceLevel;
    }

    public void setServiceLevel(String serviceLevel) {
        this.serviceLevel = serviceLevel;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSelectedCarrier() {
        return selectedCarrier;
    }

    public void setSelectedCarrier(String selectedCarrier) {
        this.selectedCarrier = selectedCarrier;
    }

    public String getSelectedCarrierCode() {
        return selectedCarrierCode;
    }

    public void setSelectedCarrierCode(String selectedCarrierCode) {
        this.selectedCarrierCode = selectedCarrierCode;
    }

    public String getAwbNumber() {
        return awbNumber;
    }

    public void setAwbNumber(String awbNumber) {
        this.awbNumber = awbNumber;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(Double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public PartyKafkaObject getOriginInformation() {
        return originInformation;
    }

    public void setOriginInformation(PartyKafkaObject originInformation) {
        this.originInformation = originInformation;
    }

    public PartyKafkaObject getDestinationInformation() {
        return destinationInformation;
    }

    public void setDestinationInformation(PartyKafkaObject destinationInformation) {
        this.destinationInformation = destinationInformation;
    }

    public List<UnitForKafkaObject> getUnitList() {
        return unitList;
    }

    public void setUnitList(List<UnitForKafkaObject> unitList) {
        this.unitList = unitList;
    }

    public List<PslKafkaObject> getTimestamps() {
        return timestamps;
    }

    public void setTimestamps(List<PslKafkaObject> timestamps) {
        this.timestamps = timestamps;
    }

    public List<EventKafkaObject> getEvents() {
        return events;
    }

    public void setEvents(List<EventKafkaObject> events) {
        this.events = events;
    }

    public String getRequiredDeliveryDate() {
        return requiredDeliveryDate;
    }

    public void setRequiredDeliveryDate(String requiredDeliveryDate) {
        this.requiredDeliveryDate = requiredDeliveryDate;
    }

    public Double getInsuranceValue() {
        return insuranceValue;
    }

    public void setInsuranceValue(Double insuranceValue) {
        this.insuranceValue = insuranceValue;
    }

    public String getTypeFlux() {
        return typeFlux;
    }

    public void setTypeFlux(String typeFlux) {
        this.typeFlux = typeFlux;
    }

    public List<CategorieLabelKafkaObject> getCategorieLabels() {
        return categorieLabels;
    }

    public void setCategorieLabels(List<CategorieLabelKafkaObject> categorieLabels) {
        this.categorieLabels = categorieLabels;
    }

    public Boolean getGrouping() {
        return grouping;
    }

    public void setGrouping(Boolean grouping) {
        this.grouping = grouping;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getShipperUserName() {
        return shipperUserName;
    }

    public void setShipperUserName(String shipperUserName) {
        this.shipperUserName = shipperUserName;
    }

    public String getShipperEtablishment() {
        return shipperEtablishment;
    }

    public void setShipperEtablishment(String shipperEtablishment) {
        this.shipperEtablishment = shipperEtablishment;
    }

    public String getInvoicingCurrencyCode() {
        return invoicingCurrencyCode;
    }

    public void setInvoicingCurrencyCode(String invoicingCurrencyCode) {
        this.invoicingCurrencyCode = invoicingCurrencyCode;
    }

    public String getFlowTypeCode() {
        return flowTypeCode;
    }

    public void setFlowTypeCode(String flowTypeCode) {
        this.flowTypeCode = flowTypeCode;
    }

    public String getIncoterm() {
        return incoterm;
    }

    public void setIncoterm(String incoterm) {
        this.incoterm = incoterm;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public String getTrackingPointSchema() {
        return trackingPointSchema;
    }

    public void setTrackingPointSchema(String trackingPointSchema) {
        this.trackingPointSchema = trackingPointSchema;
    }

    public List<String> getTrackingPointCodeList() {
        return trackingPointCodeList;
    }

    public void setTrackingPointCodeList(List<String> trackingPointCodeList) {
        this.trackingPointCodeList = trackingPointCodeList;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getCustomerCreationDate() {
        return customerCreationDate;
    }

    public void setCustomerCreationDate(Date customerCreationDate) {
        this.customerCreationDate = customerCreationDate;
    }

    public String getGoodsAvailabilityDate() {
        return goodsAvailabilityDate;
    }

    public void setGoodsAvailabilityDate(String goodsAvailabilityDate) {
        this.goodsAvailabilityDate = goodsAvailabilityDate;
    }

    public Boolean getInsurance() {
        return insurance;
    }

    public void setInsurance(Boolean insurance) {
        this.insurance = insurance;
    }

    public String getInsuranceCurrencyCode() {
        return insuranceCurrencyCode;
    }

    public void setInsuranceCurrencyCode(String insuranceCurrencyCode) {
        this.insuranceCurrencyCode = insuranceCurrencyCode;
    }

    public String getCostCenterCode() {
        return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
        this.costCenterCode = costCenterCode;
    }

    public String getNumAwbBol() {
        return numAwbBol;
    }

    public void setNumAwbBol(String numAwbBol) {
        this.numAwbBol = numAwbBol;
    }

    public String getFlightVessel() {
        return flightVessel;
    }

    public void setFlightVessel(String flightVessel) {
        this.flightVessel = flightVessel;
    }

    public String getCustomsOffice() {
        return customsOffice;
    }

    public void setCustomsOffice(String customsOffice) {
        this.customsOffice = customsOffice;
    }

    public String getMawb() {
        return mawb;
    }

    public void setMawb(String mawb) {
        this.mawb = mawb;
    }

    public String getOriginUserEmail() {
        return originUserEmail;
    }

    public void setOriginUserEmail(String originUserEmail) {
        this.originUserEmail = originUserEmail;
    }

    public String getOriginCustomUserEmail() {
        return originCustomUserEmail;
    }

    public void setOriginCustomUserEmail(String originCustomUserEmail) {
        this.originCustomUserEmail = originCustomUserEmail;
    }

    public String getDestUserEmail() {
        return destUserEmail;
    }

    public void setDestUserEmail(String destUserEmail) {
        this.destUserEmail = destUserEmail;
    }

    public String getDestCustomUserEmail() {
        return destCustomUserEmail;
    }

    public void setDestCustomUserEmail(String destCustomUserEmail) {
        this.destCustomUserEmail = destCustomUserEmail;
    }

    public List<String> getObserverUserEmailList() {
        return observerUserEmailList;
    }

    public void setObserverUserEmailList(List<String> observerUserEmailList) {
        this.observerUserEmailList = observerUserEmailList;
    }

    public PartyKafkaObject getNotifyInformation() {
        return notifyInformation;
    }

    public void setNotifyInformation(PartyKafkaObject notifyInformation) {
        this.notifyInformation = notifyInformation;
    }

    public PartyKafkaObject getIntermediaryPoint() {
        return intermediaryPoint;
    }

    public void setIntermediaryPoint(PartyKafkaObject intermediaryPoint) {
        this.intermediaryPoint = intermediaryPoint;
    }

    public List<PslWSO> getEstimatedDateList() {
        return estimatedDateList;
    }

    public void setEstimatedDateList(List<PslWSO> estimatedDateList) {
        this.estimatedDateList = estimatedDateList;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Boolean getEligibleForConsolidation() {
        return eligibleForConsolidation;
    }

    public void setEligibleForConsolidation(Boolean eligibleForConsolidation) {
        this.eligibleForConsolidation = eligibleForConsolidation;
    }

    public Boolean getFinalChoiceToBeCalculated() {
        return finalChoiceToBeCalculated;
    }

    public void setFinalChoiceToBeCalculated(Boolean finalChoiceToBeCalculated) {
        this.finalChoiceToBeCalculated = finalChoiceToBeCalculated;
    }

    public String getConsolidationStatus() {
        return consolidationStatus;
    }

    public void setConsolidationStatus(String consolidationStatus) {
        this.consolidationStatus = consolidationStatus;
    }

    public String getTransportUpdateDate() {
        return transportUpdateDate;
    }

    public void setTransportUpdateDate(String transportUpdateDate) {
        this.transportUpdateDate = transportUpdateDate;
    }

    public String getCodeConfigurationEDI() {
        return codeConfigurationEDI;
    }

    public void setCodeConfigurationEDI(String codeConfigurationEDI) {
        this.codeConfigurationEDI = codeConfigurationEDI;
    }

    public Double getTotalTaxableWeight() {
        return totalTaxableWeight;
    }

    public void setTotalTaxableWeight(Double totalTaxableWeight) {
        this.totalTaxableWeight = totalTaxableWeight;
    }

    public String getTransportPriority() {
        return transportPriority;
    }

    public void setTransportPriority(String transportPriority) {
        this.transportPriority = transportPriority;
    }

    public String getEstimatedTimeOfArrival() {
        return estimatedTimeOfArrival;
    }

    public void setEstimatedTimeOfArrival(String estimatedTimeOfArrival) {
        this.estimatedTimeOfArrival = estimatedTimeOfArrival;
    }

    public String getEstimatedTimeOfDelivery() {
        return estimatedTimeOfDelivery;
    }

    public void setEstimatedTimeOfDelivery(String estimatedTimeOfDelivery) {
        this.estimatedTimeOfDelivery = estimatedTimeOfDelivery;
    }

    public String getEstimatedTimeOfPickup() {
        return estimatedTimeOfPickup;
    }

    public void setEstimatedTimeOfPickup(String estimatedTimeOfPickup) {
        this.estimatedTimeOfPickup = estimatedTimeOfPickup;
    }

    public String getAirportOrigin() {
        return airportOrigin;
    }

    public void setAirportOrigin(String airportOrigin) {
        this.airportOrigin = airportOrigin;
    }

    public String getAirportDest() {
        return airportDest;
    }

    public void setAirportDest(String airportDest) {
        this.airportDest = airportDest;
    }

    public Date getConfirmationDate() {
        return confirmationDate;
    }

    public void setConfirmationDate(Date confirmationDate) {
        this.confirmationDate = confirmationDate;
    }

    public List<EbTypeDocuments> getListTypeDocuments() {
        return listTypeDocuments;
    }

    public void setListTypeDocuments(List<EbTypeDocuments> listTypeDocuments) {
        this.listTypeDocuments = listTypeDocuments;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    public List<QuotationKafkaObject> getQuotationList() {
        return quotationList;
    }

    public void setQuotationList(List<QuotationKafkaObject> quotationList) {
        this.quotationList = quotationList;
    }

    public List<CostKafkaObject> getAdditionalCostList() {
        return additionalCostList;
    }

    public void setAdditionalCostList(List<CostKafkaObject> additionalCostList) {
        this.additionalCostList = additionalCostList;
    }

    public boolean isValorized() {
        return isValorized;
    }

    public void setValorized(boolean isValorized) {
        this.isValorized = isValorized;
    }

	public boolean isCanceled() {
		return canceled;
	}

	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}
}
