package com.adias.mytowereasy.kafka.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbLabel;


public class CategorieLabelKafkaObject {
    private String libelle;
    private List<String> listLabel;

    private String label;

    public CategorieLabelKafkaObject(EbCategorie ebCategorie) {
        this.libelle = ebCategorie.getLibelle();

        if (ebCategorie.getLabels() != null) {

            for (EbLabel eblabel: ebCategorie.getLabels()) {
                this.label = eblabel.getLibelle();
            }

        }

        this.listLabel = new ArrayList<>();
        Set<EbLabel> listCategorieLabel = ebCategorie.getLabels();

        if (!ebCategorie.getLabels().isEmpty()) {

            for (EbLabel eblabel: listCategorieLabel) {
                if (listCategorieLabel.size() == 1) this.label = eblabel.getLibelle();
                else if (listCategorieLabel.size() > 1) this.listLabel.add(eblabel.getLibelle());
            }

        }

    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public List<String> getListLabel() {
        return listLabel;
    }

    public void setListLabel(List<String> listLabel) {
        this.listLabel = listLabel;
    }
}
