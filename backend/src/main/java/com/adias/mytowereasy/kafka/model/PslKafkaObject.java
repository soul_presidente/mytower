package com.adias.mytowereasy.kafka.model;

import java.util.Date;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;


public class PslKafkaObject {
    private String code;
    private String libelle;
    private Date exeptedDateTime;

    public PslKafkaObject() {
    }

    public PslKafkaObject(EbTtCompanyPsl ebTtCompanyPsl) {
        this.code = ebTtCompanyPsl.getCodeAlpha();
        this.libelle = ebTtCompanyPsl.getLibelle();
        this.exeptedDateTime = ebTtCompanyPsl.getExpectedDate();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Date getExeptedDateTime() {
        return exeptedDateTime;
    }

    public void setExeptedDateTime(Date exeptedDateTime) {
        this.exeptedDateTime = exeptedDateTime;
    }
}
