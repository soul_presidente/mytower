package com.adias.mytowereasy.kafka.config;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;


public interface TransportRequestStreamConfig {
    String TRANSACTION_OUTPUT_TOPIC = "transport-request-output";

    @Output(TRANSACTION_OUTPUT_TOPIC)
    MessageChannel outputMessage();
}
