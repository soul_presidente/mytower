package com.adias.mytowereasy.kafka.util;

import java.text.SimpleDateFormat;
import java.util.Date;


public class KafkaUtil {
    public static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ssXXX";

    public static String getDate(Date date) {

        if (date != null) {
            String dateStr = new SimpleDateFormat(DATE_FORMAT).format(date);
            dateStr = dateStr.replaceAll("Z", "+00:00");
            return dateStr;
        }

        return null;
    }

    public static String getYesOrNo(Boolean value) {
        return value != null ? value ? "Yes" : "No" : "No";
    }
}
