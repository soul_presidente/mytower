package com.adias.mytowereasy.kafka.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Component;

import com.adias.mytowereasy.kafka.service.KafkaObjectService;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.ConnectedUserService;


@Aspect
@Component
@EnableAspectJAutoProxy
public class SendMessageToKafkaAspect {
    @Autowired
    KafkaObjectService kafkaObjectService;

    @Autowired
    ConnectedUserService connectedUserService;

    @Around(value="@annotation(sendMessageToKafka))", argNames = "proceedingJoinPoint,sendMessageToKafka")
    public Object buildAndSendProcessGroup(ProceedingJoinPoint proceedingJoinPoint, SendMessageToKafka sendMessageToKafka) throws Throwable {
        Object savedTr = proceedingJoinPoint.proceed();

        KafkaObjectType kot = sendMessageToKafka.objectToSendType();

        Integer triggerCode = sendMessageToKafka.trigger().getCode();
        Object[] arguments = proceedingJoinPoint.getArgs();

        EbUser connectedUser =  arguments[1] != null ? (EbUser) arguments[1] : connectedUserService.getCurrentUser();

				if (kot.equals(KafkaObjectType.PROCESS_GROUP_OBJECT) && arguments[0] instanceof EbDemande)
				{
					EbDemande transportRequest = (EbDemande) arguments[0];
					kot
						.buildAndSendObject(
							connectedUser.getEbCompagnie().getEbCompagnieNum(),
							triggerCode,
							transportRequest,
							connectedUser
						);
        }

        if (kot.equals(KafkaObjectType.TRANSPORT_REQUEST_OBJECT) && arguments[0] instanceof EbDemande) {
            EbDemande tr = (EbDemande) arguments[0];
            kot.buildAndSendObject(tr.getEbDemandeNum(), connectedUser);
        }

        if (kot.equals(KafkaObjectType.BOTH) && arguments[0] instanceof EbDemande) {
            EbDemande transportRequest = (EbDemande) arguments[0];
            kot.buildAndSendObject(connectedUser.getEbCompagnie().getEbCompagnieNum(), triggerCode, transportRequest.getEbDemandeNum(), connectedUser);
        }
        return savedTr;
    }
}
