package com.adias.mytowereasy.kafka.aspects;

import com.adias.mytowereasy.model.Enumeration;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SendMessageToKafka {
    Enumeration.QrGroupeTrigger trigger() default Enumeration.QrGroupeTrigger.DEFAULT;
    KafkaObjectType objectToSendType();
}
