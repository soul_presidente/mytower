package com.adias.mytowereasy.kafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adias.mytowereasy.kafka.model.TransportRequestKafkaObject;
import com.adias.mytowereasy.kafka.service.KafkaObjectService;
import com.adias.mytowereasy.kafka.service.ProducerService;
import com.adias.mytowereasy.repository.EbDemandeRepository;


@Controller
@RequestMapping(value = "api/producer")
public class KafkaProducerController {
    private final ProducerService producerService;
    private final KafkaObjectService kafkaObjectService;

    private final EbDemandeRepository ebDemandeRepository;

    @Autowired
    public KafkaProducerController(
        ProducerService producerService,
        KafkaObjectService kafkaObjectService,
        EbDemandeRepository ebDemandeRepository) {
        this.producerService = producerService;
        this.kafkaObjectService = kafkaObjectService;
        this.ebDemandeRepository = ebDemandeRepository;
    }

    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "transport-request/{demandeNum}")
    public @ResponseBody String sendTransportRequestObject(
        @PathVariable(value = "demandeNum", required = true) Integer demandeNum,
        @RequestParam(value = "topicName") String topicName)
        throws Exception {
        kafkaObjectService.sendTransportRequestToKafkaTopic(demandeNum, topicName, false, null);

        return "Message sent successfully";
    }

    @GetMapping(value = "publish/transport-request/{demandeRef}")
    public void sendTransportRequestObjectByRefTransport(@PathVariable(value = "demandeRef") String demandeRef)
        throws Exception {
        Integer demandeNum = ebDemandeRepository.findByRefTransport(demandeRef.toUpperCase());
        TransportRequestKafkaObject transportRequestKafkaObject = null;

        if (demandeNum != null) {
            transportRequestKafkaObject = this.kafkaObjectService
                .getTransportRequestKafkaObjectWithEventFromDemande(demandeNum, null, null);
        }

        if (transportRequestKafkaObject != null) {
            this.producerService.sendTransportRequestKafkaObject(transportRequestKafkaObject, false);
        }

    }
}
