package com.adias.mytowereasy.kafka.model;

import com.adias.mytowereasy.dto.EbTTPslAppAndEventsProjection;
import com.adias.mytowereasy.dto.EbTteventProjection;
import com.adias.mytowereasy.kafka.util.KafkaUtil;
import com.adias.mytowereasy.model.TtEnumeration;


public class EventKafkaObject {
    private String typeOfEvent;
    private String code;
    private String label;
    private String actualDate;
    private String countryCode;
    private String city;
    private String comment;
    private Boolean lastEvent;
    private Boolean startPsl;
    private Boolean endPsl;

    public EventKafkaObject() {
    }

    public EventKafkaObject(
        EbTTPslAppAndEventsProjection ebTTPslAppAndEventsProjection,
        EbTteventProjection ebTteventProjection,
        Boolean lastEvent) {

        if (ebTteventProjection.getTypeEvent() != null) {
            String typeEv = TtEnumeration.TypeEvent.getTypeEventByCode(ebTteventProjection.getTypeEvent()).getLibelle();

            if (typeEv != null) {
                this.typeOfEvent = typeEv.equals("Deviation") ? "INCIDENT" : typeEv;
            }

        }

        this.label = ebTTPslAppAndEventsProjection.getNom();
        this.code = ebTTPslAppAndEventsProjection.getCodeAlpha();

        this.actualDate = KafkaUtil.getDate(ebTteventProjection.getDateEvent());

        if (ebTTPslAppAndEventsProjection
            .getxEcCountry() != null) this.countryCode = ebTTPslAppAndEventsProjection.getxEcCountry().getCode();
        this.city = ebTTPslAppAndEventsProjection.getCity();
        this.comment = ebTteventProjection.getCommentaire();

        // Champ pas encore identifier
        this.lastEvent = lastEvent;
        this.startPsl = ebTTPslAppAndEventsProjection.getCompanyPsl().getStartPsl();
        this.endPsl = ebTTPslAppAndEventsProjection.getCompanyPsl().getEndPsl();
    }

    public String getTypeOfEvent() {
        return typeOfEvent;
    }

    public void setTypeOfEvent(String typeOfEvent) {
        this.typeOfEvent = typeOfEvent;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getActualDate() {
        return actualDate;
    }

    public void setActualDate(String actualDate) {
        this.actualDate = actualDate;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCoutryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public Boolean getLastEvent() {
        return lastEvent;
    }

    public void setLastEvent(Boolean lastEvent) {
        this.lastEvent = lastEvent;
    }

    public Boolean getStartPsl() {
        return startPsl;
    }

    public void setStartPsl(Boolean startPsl) {
        this.startPsl = startPsl;
    }

    public Boolean getEndPsl() {
        return endPsl;
    }

    public void setEndPsl(Boolean endPsl) {
        this.endPsl = endPsl;
    }
}
