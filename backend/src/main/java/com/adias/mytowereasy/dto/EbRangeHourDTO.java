package com.adias.mytowereasy.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.adias.mytowereasy.model.EbRangeHour;


public class EbRangeHourDTO {
    private Integer rangeHoursNum;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", timezone = JsonFormat.DEFAULT_TIMEZONE)
    private Date startHour;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm", timezone = JsonFormat.DEFAULT_TIMEZONE)
    private Date endHour;

    // Never put any relation field
    // Use reverse way to retrieve
    // Range is a dependency and don't have dependencies

    public EbRangeHourDTO() {
    }

    public EbRangeHourDTO(EbRangeHour db) {
        this.rangeHoursNum = db.getRangeHoursNum();
        this.startHour = db.getStartHour();
        this.endHour = db.getEndHour();
    }

    public Integer getRangeHoursNum() {
        return rangeHoursNum;
    }

    public void setRangeHoursNum(Integer rangeHoursNum) {
        this.rangeHoursNum = rangeHoursNum;
    }

    public Date getStartHour() {
        return startHour;
    }

    public void setStartHour(Date startHour) {
        this.startHour = startHour;
    }

    public Date getEndHour() {
        return endHour;
    }

    public void setEndHour(Date endHour) {
        this.endHour = endHour;
    }
}
