package com.adias.mytowereasy.dto;

public class EbDemandeTransportDTO {
    private Integer ebDemandeNum;

    private String refTransport;

    private Integer xEcTypeDemande;

    private Integer xEcModeTransport;

    private Integer xEcIncoterm;

    private String libelleOriginCountry;

    private String libelleDestCountry;

    private String transporteur;

    private String etabTransporteur;

    private String chargeur;

    private String etabChargeur;

    private Integer xEcStatut;

    public EbDemandeTransportDTO() {
    }

    public Integer getEbDemandeNum() {
        return ebDemandeNum;
    }

    public String getRefTransport() {
        return refTransport;
    }

    public Integer getxEcTypeDemande() {
        return xEcTypeDemande;
    }

    public Integer getxEcModeTransport() {
        return xEcModeTransport;
    }

    public Integer getxEcIncoterm() {
        return xEcIncoterm;
    }

    public String getLibelleOriginCountry() {
        return libelleOriginCountry;
    }

    public String getLibelleDestCountry() {
        return libelleDestCountry;
    }

    public String getTransporteur() {
        return transporteur;
    }

    public String getEtabTransporteur() {
        return etabTransporteur;
    }

    public String getChargeur() {
        return chargeur;
    }

    public String getEtabChargeur() {
        return etabChargeur;
    }

    public void setEbDemandeNum(Integer ebDemandeNum) {
        this.ebDemandeNum = ebDemandeNum;
    }

    public void setRefTransport(String refTransport) {
        this.refTransport = refTransport;
    }

    public void setxEcTypeDemande(Integer xEcTypeDemande) {
        this.xEcTypeDemande = xEcTypeDemande;
    }

    public void setxEcModeTransport(Integer xEcModeTransport) {
        this.xEcModeTransport = xEcModeTransport;
    }

    public void setxEcIncoterm(Integer xEcIncoterm) {
        this.xEcIncoterm = xEcIncoterm;
    }

    public void setLibelleOriginCountry(String libelleOriginCountry) {
        this.libelleOriginCountry = libelleOriginCountry;
    }

    public void setLibelleDestCountry(String libelleDestCountry) {
        this.libelleDestCountry = libelleDestCountry;
    }

    public void setTransporteur(String transporteur) {
        this.transporteur = transporteur;
    }

    public void setEtabTransporteur(String etabTransporteur) {
        this.etabTransporteur = etabTransporteur;
    }

    public void setChargeur(String chargeur) {
        this.chargeur = chargeur;
    }

    public void setEtabChargeur(String etabChargeur) {
        this.etabChargeur = etabChargeur;
    }

    public Integer getxEcStatut() {
        return xEcStatut;
    }

    public void setxEcStatut(Integer xEcStatut) {
        this.xEcStatut = xEcStatut;
    }
}
