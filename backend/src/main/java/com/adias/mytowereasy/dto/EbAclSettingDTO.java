package com.adias.mytowereasy.dto;

import java.util.List;


public class EbAclSettingDTO {
    private Integer ecAclRuleNum;
    private Integer type;
    private List<String> themes;
    private String code;
    private List<String> values;
    private String defaultValue;
    private String selectedValue;
    private Boolean experimental;

    public Integer getEcAclRuleNum() {
        return ecAclRuleNum;
    }

    public void setEcAclRuleNum(Integer ecAclRuleNum) {
        this.ecAclRuleNum = ecAclRuleNum;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<String> getThemes() {
        return themes;
    }

    public void setThemes(List<String> themes) {
        this.themes = themes;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public Boolean getExperimental() {
        return experimental;
    }

    public void setExperimental(Boolean experimental) {
        this.experimental = experimental;
    }
}
