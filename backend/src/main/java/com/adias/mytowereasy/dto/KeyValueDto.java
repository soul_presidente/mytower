package com.adias.mytowereasy.dto;

public class KeyValueDto<T> {
    public KeyValueDto() {
    }

    public KeyValueDto(String key, T value) {
        this.key = key;
        this.value = value;
    }

    private String key;
    private T value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
