package com.adias.mytowereasy.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties({
    "selectedForDeclaration"
})
public class CustomDeclarationDocumentDTO {
    private Integer ebDemandeFichierJointNum;
    private String typeDocument;
    private String filename;

    public Integer getEbDemandeFichierJointNum() {
        return ebDemandeFichierJointNum;
    }

    public void setEbDemandeFichierJointNum(Integer ebDemandeFichierJointNum) {
        this.ebDemandeFichierJointNum = ebDemandeFichierJointNum;
    }

    public String getTypeDocument() {
        return typeDocument;
    }

    public void setTypeDocument(String typeDocument) {
        this.typeDocument = typeDocument;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}
