package com.adias.mytowereasy.dto;

import com.adias.mytowereasy.model.EcCurrency;


public class EcCurrencyDTO {
    private Integer ecCurrencyNum;

    private String libelle;

    private String code;

    private String symbol;

    private String codeLibelle;

    public EcCurrencyDTO() {
    }

    public EcCurrencyDTO(EcCurrency currency) {
        this.ecCurrencyNum = currency.getEcCurrencyNum();
        this.libelle = currency.getLibelle();
        this.code = currency.getCode();
        this.symbol = currency.getSymbol();
        this.codeLibelle = currency.getCodeLibelle();
    }

    public Integer getEcCurrencyNum() {
        return ecCurrencyNum;
    }

    public void setEcCurrencyNum(Integer ecCurrencyNum) {
        this.ecCurrencyNum = ecCurrencyNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCodeLibelle() {
        return codeLibelle;
    }

    public void setCodeLibelle(String codeLibelle) {
        this.codeLibelle = codeLibelle;
    }
}
