package com.adias.mytowereasy.dto;

public class TransportReferenceInfosDTO {
    private Integer ebDemandeNum;
    private String refTransport;

    public TransportReferenceInfosDTO() {
    }

    public TransportReferenceInfosDTO(Integer ebDemandeNum, String refTransport) {
        this.ebDemandeNum = ebDemandeNum;
        this.refTransport = refTransport;
    }

    public Integer getEbDemandeNum() {
        return ebDemandeNum;
    }

    public void setEbDemandeNum(Integer ebDemandeNum) {
        this.ebDemandeNum = ebDemandeNum;
    }

    public String getRefTransport() {
        return refTransport;
    }

    public void setRefTransport(String refTransport) {
        this.refTransport = refTransport;
    }
}
