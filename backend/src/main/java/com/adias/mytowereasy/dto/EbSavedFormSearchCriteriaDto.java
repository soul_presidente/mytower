package com.adias.mytowereasy.dto;

public class EbSavedFormSearchCriteriaDto extends KeyValueDto<Object> {
    private String fieldLibelle;
    private String valueLibelle;

    public EbSavedFormSearchCriteriaDto() {
        super();
    }

    public EbSavedFormSearchCriteriaDto(String key, Object value) {
        super(key, value);
    }

    public String getFieldLibelle() {
        return fieldLibelle;
    }

    public void setFieldLibelle(String fieldLibelle) {
        this.fieldLibelle = fieldLibelle;
    }

    public String getValueLibelle() {
        return valueLibelle;
    }

    public void setValueLibelle(String valueLibelle) {
        this.valueLibelle = valueLibelle;
    }
}
