/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import org.mapstruct.*;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.type.TypeReference;

import com.adias.mytowereasy.api.wso.*;
import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.enumeration.StatusOrder;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.dto.*;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.util.GenericEnum;
import com.adias.mytowereasy.util.JsonUtils;
import com.adias.mytowereasy.util.enums.GenericEnumException;
import com.adias.mytowereasy.util.enums.GenericEnumUtils;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DeliveryMapper {
    @Mapping(source = "xEcCountry.libelle", target = "country")
    EbPartyDto EbPartyToEbPartyDto(EbParty party);

    EbParty EbPartyDtoToEbParty(EbPartyDto dto);

    @Mappings({
        @Mapping(source = "ebDelOrderNum", target = "ebDelOrderNum"),
        @Mapping(source = "xEbPartySale.reference", target = "refClientCreator"),
        @Mapping(source = "xEbPartySale.company", target = "nameOrderCreator"),
        @Mapping(source = "xEbPartyDestination.reference", target = "refClientDelivered"),
        @Mapping(source = "xEbPartyDestination.company", target = "nameClientDelivered"),
        @Mapping(source = "xEbPartyDestination.xEcCountry.libelle", target = "countryClientDelivered"),
        @Mapping(source = "xEbEtablissement.nom", target = "etablissementName"),
        @Mapping(source = "customerCreationDate", target = "customerCreationDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "deliveryDate", target = "deliveryDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "targetDate", target = "targetDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "etd", target = "etd", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "eta", target = "eta", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "orderStatus", target = "orderStatus", qualifiedByName = "orderStatusLabel"),
        @Mapping(target = "orderLines", ignore = true),
        @Mapping(source = "xEbOwnerOfTheRequest.email", target = "requestOwnerEmail")
    })
    EbOrderDto ebOrderToEbOrderDto(EbOrder order);

    @Mappings({
        @Mapping(source = "customerCreationDate", target = "customerCreationDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "deliveryDate", target = "deliveryDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "etd", target = "etd", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "eta", target = "eta", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "orderStatus", target = "orderStatus", qualifiedByName = "orderStatusId"),
        @Mapping(source = "targetDate", target = "targetDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "orderDate", target = "orderDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "requestOwnerEmail", target = "xEbOwnerOfTheRequest.email"),
        @Mapping(source = "orderLines", target = "orderLines", qualifiedByName = "orderLinesDtoToOrderLines")
    })
    EbOrder ebOrderDtoToEbOrder(EbOrderDto order, @Context DependenciesResolverContext context);

    default EbOrder ebOrderDtoToEbOrder(EbOrderDto order) {
        return ebOrderDtoToEbOrder(order, DependenciesResolverContext.instance);
    }

    @Mappings({
        @Mapping(
            source = "statusDelivery",
            target = "statusDelivery",
            qualifiedByName = "convertDeliveryStatusFromCode"),
        @Mapping(source = "orderCreationDate", target = "orderCreationDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "xEbDemande", target = "xEbDemande"),
        @Mapping(source = "expectedDeliveryDate", target = "expectedDeliveryDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(
            source = "xEbDelLivraisonLine",
            target = "xEbDelLivraisonLine",
            qualifiedByName = "ebLivraisonLineDtoToEbLivraisonLine")
    })
    EbLivraison ebDeliveryDto(EbLivraisonDto ebLivraisonDto);

    @Mappings({
        @Mapping(source = "xEcCountryOrigin.libelle", target = "originCountry"),
        @Mapping(source = "dateOfAvailability", target = "dateOfAvailability", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "currency.ecCurrencyNum", target = "currency")
    })
    EbOrderLineDto ebOrderLineToEbOrderLineDto(EbOrderLine line);

    @Mappings({
        @Mapping(source = "dateOfAvailability", target = "dateOfAvailability", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "currency", target = "currency.ecCurrencyNum")
    })
    EbOrderLine ebOrderLineDtoToEbOrderLine(EbOrderLineDto line, @Context DependenciesResolverContext context);

    @Named("orderLinesDtoToOrderLines")
    default EbOrderLine ebOrderLineDtoToEbOrderLine(EbOrderLineDto line) {
        return ebOrderLineDtoToEbOrderLine(line, DependenciesResolverContext.instance);
    }

    List<EbOrderLineDto> ebOrderLinesToEbOrderLinesDto(List<EbOrderLine> lines);

    List<EbOrderDto> ebOrdersToEbOrdersDto(List<EbOrder> orders);

    @Mappings({
        @Mapping(source = "deliveryCreationDate", target = "deliveryCreationDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "expectedDeliveryDate", target = "expectedDeliveryDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "orderCreationDate", target = "orderCreationDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "requestedDeliveryDate", target = "requestedDeliveryDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "invoiceDate", target = "invoiceDate", dateFormat = "dd/MM/yyyy"),
        @Mapping(source = "statusDelivery", target = "statusDelivery", qualifiedByName = "getCodeFromDeliveryStatus"),
        @Mapping(source = "xEbPartySale.reference", target = "refClientCreator"),
        @Mapping(source = "xEbPartySale.company", target = "nameOrderCreator"),
        @Mapping(source = "xEbDemande", target = "xEbDemande"),
        @Mapping(target = "xEbDelLivraisonLine", ignore = true),
    })
    EbLivraisonDto ebLivraisonToEbLivraisonDto(EbLivraison livraison);

    @Mappings({
        @Mapping(source = "ebDelLivraisonNum", target = "deliveryId"),
        @Mapping(source = "statusDelivery", target = "deliveryStatus", qualifiedByName = "getKeyFromDeliveryStatus"),
        @Mapping(source = "numOrderEDI", target = "numEdi"),
        @Mapping(source = "xEcIncotermLibelle", target = "incoterm"),
        @Mapping(source = "xEbOwnerOfTheRequest.email", target = "requestOwnerEmail"),
        @Mapping(source = "xEbDelOrder.ebDelOrderNum", target = "orderId"),
        @Mapping(source = "xEbDelOrder.reference", target = "refOrder"),
        @Mapping(
            source = "listCustomsFields",
            target = "customFieldList",
            qualifiedByName = "convertCustomFieldsToWSO"),
        @Mapping(source = "listCategories", target = "categoryList", qualifiedByName = "convertCategoriesToWSO")
    })
    DeliveryWSO ebLivraisonToDeliveyWSO(EbLivraison livraison);

    @Mappings({
        @Mapping(source = "ebDelOrderNum", target = "itemId"),
        @Mapping(source = "numOrderEDI", target = "numEdi"),
        @Mapping(source = "xEcIncotermLibelle", target = "incoterm"),
        @Mapping(source = "xEbOwnerOfTheRequest.email", target = "requestOwnerEmail"),
        @Mapping(
            source = "listCustomsFields",
            target = "customFieldList",
            qualifiedByName = "convertCustomFieldsToWSO"),
        @Mapping(source = "listCategories", target = "categoryList", qualifiedByName = "convertCategoriesToWSO")
    })
    OrderWSO ebOrderToOrderWSO(EbOrder order);

    @Mappings({
        @Mapping(source = "xEcCountryOrigin.libelle", target = "originCountry"),
        @Mapping(source = "articleName", target = "articleName"),
        @Mapping(source = "ebDelLivraisonLineNum", target = "ebDelLivraisonLineNum"),
        @Mapping(source = "currency.ecCurrencyNum", target = "currency")
    })
    EbLivraisonLineDto ebLivraisonLineToEbLivraisonLineDto(EbLivraisonLine line);
    
    @Mappings({
        @Mapping(source = "currency", target = "currency.ecCurrencyNum")
    })
    EbLivraisonLine
        ebLivraisonLineDtoToEbLivraisonLine(EbLivraisonLineDto dto, @Context DependenciesResolverContext context);

    @Named("ebLivraisonLineDtoToEbLivraisonLine")
    default EbLivraisonLine ebLivraisonLineDtoToEbLivraisonLine(EbLivraisonLineDto dto) {
        return ebLivraisonLineDtoToEbLivraisonLine(dto, DependenciesResolverContext.instance);
    }

    @Mappings({
        @Mapping(source = "xEcCountryOrigin.code", target = "originCountryCode"),
        @Mapping(source = "ebDelLivraisonLineNum", target = "itemId"),
        @Mapping(source = "currency.code", target = "currencyCode"),
        @Mapping(source = "quantity", target = "quantityPlanned"),
        @Mapping(
            source = "listCustomsFields",
            target = "customFieldList",
            qualifiedByName = "convertCustomFieldsToWSO"),
        @Mapping(source = "listCategories", target = "categoryList", qualifiedByName = "convertCategoriesToWSO")
    })
    DeliveryLineWSO
        ebLivraisonLineToDeliveryLineWSO(EbLivraisonLine line);

    @Mappings({
        @Mapping(source = "xEcCountryOrigin.code", target = "originCountryCode"),
        @Mapping(source = "ebDelOrderLineNum", target = "itemId"),
        @Mapping(source = "currency.code", target = "currencyCode"),
        @Mapping(
            source = "listCustomsFields",
            target = "customFieldList",
            qualifiedByName = "convertCustomFieldsToWSO"),
        @Mapping(source = "listCategories", target = "categoryList", qualifiedByName = "convertCategoriesToWSO")
    })
    OrderLineWSO ebOrderLineToOrderLineWSO(EbOrderLine line);

    @Mappings({
        @Mapping(source = "itemId", target = "ebDelLivraisonLineNum"),
        @Mapping(source = "currencyCode", target = "currency.code"),
        @Mapping(source = "quantityPlanned", target = "quantity")
    })
    EbLivraisonLine deliveryLineWSOToEbLivraisonLine(DeliveryLineWSO wso, @Context DependenciesResolverContext context);

    default EbLivraisonLine deliveryLineWSOToEbLivraisonLine(DeliveryLineWSO wso) {
        return deliveryLineWSOToEbLivraisonLine(wso, DependenciesResolverContext.instance);
    }

    List<EbLivraisonLineDto> ebLivraisonLinesToEbLivraisonLinesDto(List<EbLivraisonLine> lines);

    List<EbLivraisonDto> ebLivraisonsToEbLivraisonsDto(List<EbLivraison> livraisons);

    @Named("orderStatusLabel")
    default String orderStatusLabel(Integer code) {

        try {
            return GenericEnumUtils.getKeyByCode(StatusOrder.class, code).orElse(null);
        } catch (GenericEnumException e) {
            e.printStackTrace(System.err);
            return null;
        }

    }

    @Named("orderStatusId")
    default Integer orderStatusId(String key) {

        try {
            return GenericEnumUtils.getCodeByKey(StatusOrder.class, key).orElse(null);
        } catch (GenericEnumException e) {
            e.printStackTrace(System.err);
            return null;
        }

    }

    @Named("toDeliveryStatusFromCode")
    default GenericEnum toDeliveryStatusFromCode(Integer code) {

        try {
            return GenericEnumUtils.getByCode(StatusDelivery.class, code).orElse(null);
        } catch (GenericEnumException e) {
            e.printStackTrace(System.err);
            return null;
        }

    }

    @Named("convertDeliveryStatusFromCode")
    default StatusDelivery convertDeliveryStatusFromCode(Integer code) {
        return StatusDelivery.valueOf(code);
    }

    @Named("getCodeFromDeliveryStatus")
    default Integer getCodeFromDeliveryStatus(StatusDelivery statusDelivery) {
        if (statusDelivery == null) return null;
        return statusDelivery.getCode();
    }

    @Named("getKeyFromDeliveryStatus")
    default String getKeyFromDeliveryStatus(StatusDelivery statusDelivery) {
        if (statusDelivery == null) return null;
        return statusDelivery.getKey();
    }

    @Named("convertCustomFieldsToWSO")
    default List<CustomFieldWSO> convertCustomFieldsToWSO(List<CustomFields> customFields) {

        if (CollectionUtils.isEmpty(customFields)) {
            return null;
        }

        List<CustomFieldWSO> customFieldWSOs = new ArrayList<CustomFieldWSO>();
        customFields.forEach(customField -> {
            CustomFieldWSO customFieldWSO = new CustomFieldWSO();
            customFieldWSO.setCode(customField.getName());
            customFieldWSO.setLabel(customField.getLabel());
            customFieldWSO.setValue(customField.getValue());
            customFieldWSOs.add(customFieldWSO);
        });
        return customFieldWSOs;
    }

    @Named("convertCategoriesToWSO")
    default List<CategoryWSO> convertCategoriesToWSO(List<EbCategorie> dtos) {

        if (CollectionUtils.isEmpty(dtos)) {
            return new ArrayList<CategoryWSO>();
        }
        
        List<EbCategorie> categories = JsonUtils.staticMapper
            .convertValue(dtos, new TypeReference<List<EbCategorie>>() {
            });

        List<CategoryWSO> categoriesWSO = new ArrayList<CategoryWSO>();
        categories.forEach(category -> {
            CategoryWSO categoryWSO = new CategoryWSO();
            categoryWSO.setCategory(category.getLibelle());
            EbLabel label = !CollectionUtils.isEmpty(category.getLabels()) ?
                category.getLabels().stream().findFirst().orElse(null) :
                null;

            if (label != null) {
                categoryWSO.setCategoryLabel(label.getLibelle());
            }

            categoriesWSO.add(categoryWSO);
        });
        return categoriesWSO;
    }

    @BeanMapping(ignoreByDefault = true)
    @Mappings({
        @Mapping(source = "customerOrderReference", target = "customerOrderReference"),
        @Mapping(source = "xEbOwnerOfTheRequest.ebCompagnie.ebCompagnieNum", target = "ebCompagnieNum")
    })
    SearchCriteriaOrder orderToSearchCriteriaOrder(EbOrder order);

    @BeanMapping(ignoreByDefault = true)
    @Mappings({
        @Mapping(source = "customerOrderReference", target = "customerOrderReference"),
        @Mapping(source = "xEbOwnerOfTheRequest.ebCompagnie.ebCompagnieNum", target = "ebCompagnieNum")
    })
    SearchCriteriaDelivery deliveryToSearchCriteriaDelivery(EbLivraison delivery);
}
