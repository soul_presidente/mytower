package com.adias.mytowereasy.dto;

public class EbCompagnieDTO {
    private Integer ebCompagnieNum;
    private String code;
    private String nom;
    private String logo;
    private Integer compagnieRole;

    public EbCompagnieDTO() {
    }

    public EbCompagnieDTO(Integer ebCompagnieNum, String code, String nom, String logo, Integer compagnieRole) {
        this.ebCompagnieNum = ebCompagnieNum;
        this.code = code;
        this.nom = nom;
        this.logo = logo;
        this.setCompagnieRole(compagnieRole);
    }

    public Integer getEbCompagnieNum() {
        return ebCompagnieNum;
    }

    public String getCode() {
        return code;
    }

    public String getNom() {
        return nom;
    }

    public String getLogo() {
        return logo;
    }

    public void setEbCompagnieNum(Integer ebCompagnieNum) {
        this.ebCompagnieNum = ebCompagnieNum;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Integer getCompagnieRole() {
        return compagnieRole;
    }

    public void setCompagnieRole(Integer compagnieRole) {
        this.compagnieRole = compagnieRole;
    }
}
