package com.adias.mytowereasy.dto;

import java.util.List;

import com.adias.mytowereasy.model.EbRelation;
import com.adias.mytowereasy.model.EbUser;


public class RelationToChargeursDTO {
    List<EbUser> selectChargeurs;
    EbRelation ebRelation;

    public RelationToChargeursDTO() {
    }

    public RelationToChargeursDTO(RelationToChargeursDTO entity) {
        super();
        this.selectChargeurs = entity.getSelectChargeurs();
        this.ebRelation = entity.getEbRelation();
    }

    public EbRelation getEbRelation() {
        return ebRelation;
    }

    public void setEbRelation(EbRelation ebRelation) {
        this.ebRelation = ebRelation;
    }

    public List<EbUser> getSelectChargeurs() {
        return selectChargeurs;
    }

    public void setSelectChargeurs(List<EbUser> selectChargeurs) {
        this.selectChargeurs = selectChargeurs;
    }
}
