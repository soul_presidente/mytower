package com.adias.mytowereasy.dto;

import java.util.List;

import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbMarchandise;


public class EbMarchandiseDto {
    private Integer ebMarchandiseNum;

    private String dateCreation;

    private String storageTemperature;

    private Double weight;

    private Double length;

    private Double width;

    private Double heigth;

    private String dg;

    private String dryIce;

    private String classGood;

    private Integer un;

    private String equipment;

    private String sensitive;

    private String unitReference;

    private String customerReference;

    private String quantityDryIce;

    private Integer numberOfUnits;

    private Double volume;

    private Integer dangerousGood;

    private String packaging;

    private Boolean stackable;

    private String comment;

    private String refArticle;

    private String nomArticle;

    private String lot;

    private String sscc;

    private String olpn;

    private String tracking_number;

    private String etat;

    private String pood;

    private String typeUnitLabel;

    private EbMarchandise parent;

    private List<EbMarchandise> children;

    private String xEbTypeUnit;

    private String xEbTypeConteneur;

    private String typeContainer;

    private List<EbLivraisonLine> xEbLivraisonLines;

    private String customerOrderReference;

    private Long idEbDelLivraison;

    private Boolean serialized;

    private String customFields;

    // champs reservé pour customFields
    private List<CustomFields> listCustomsFields;

    private String itemName;

    private String itemNumber;

    public Integer getEbMarchandiseNum() {
        return ebMarchandiseNum;
    }

    public void setEbMarchandiseNum(Integer ebMarchandiseNum) {
        this.ebMarchandiseNum = ebMarchandiseNum;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getStorageTemperature() {
        return storageTemperature;
    }

    public void setStorageTemperature(String storageTemperature) {
        this.storageTemperature = storageTemperature;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeigth() {
        return heigth;
    }

    public void setHeigth(Double heigth) {
        this.heigth = heigth;
    }

    public String getDg() {
        return dg;
    }

    public void setDg(String dg) {
        this.dg = dg;
    }

    public String getDryIce() {
        return dryIce;
    }

    public void setDryIce(String dryIce) {
        this.dryIce = dryIce;
    }

    public String getClassGood() {
        return classGood;
    }

    public void setClassGood(String classGood) {
        this.classGood = classGood;
    }

    public Integer getUn() {
        return un;
    }

    public void setUn(Integer un) {
        this.un = un;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getSensitive() {
        return sensitive;
    }

    public void setSensitive(String sensitive) {
        this.sensitive = sensitive;
    }

    public String getUnitReference() {
        return unitReference;
    }

    public void setUnitReference(String unitReference) {
        this.unitReference = unitReference;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getQuantityDryIce() {
        return quantityDryIce;
    }

    public void setQuantityDryIce(String quantityDryIce) {
        this.quantityDryIce = quantityDryIce;
    }

    public Integer getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(Integer numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Integer getDangerousGood() {
        return dangerousGood;
    }

    public void setDangerousGood(Integer dangerousGood) {
        this.dangerousGood = dangerousGood;
    }

    public String getPackaging() {
        return packaging;
    }

    public void setPackaging(String packaging) {
        this.packaging = packaging;
    }

    public Boolean getStackable() {
        return stackable;
    }

    public void setStackable(Boolean stackable) {
        this.stackable = stackable;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRefArticle() {
        return refArticle;
    }

    public void setRefArticle(String refArticle) {
        this.refArticle = refArticle;
    }

    public String getNomArticle() {
        return nomArticle;
    }

    public void setNomArticle(String nomArticle) {
        this.nomArticle = nomArticle;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getSscc() {
        return sscc;
    }

    public void setSscc(String sscc) {
        this.sscc = sscc;
    }

    public String getOlpn() {
        return olpn;
    }

    public void setOlpn(String olpn) {
        this.olpn = olpn;
    }

    public String getTracking_number() {
        return tracking_number;
    }

    public void setTracking_number(String tracking_number) {
        this.tracking_number = tracking_number;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getPood() {
        return pood;
    }

    public void setPood(String pood) {
        this.pood = pood;
    }

    public String getTypeContainer() {
        return typeContainer;
    }

    public void setTypeContainer(String typeContainer) {
        this.typeContainer = typeContainer;
    }

    public EbMarchandise getParent() {
        return parent;
    }

    public void setParent(EbMarchandise parent) {
        this.parent = parent;
    }

    public List<EbMarchandise> getChildren() {
        return children;
    }

    public String getxEbTypeConteneur() {
        return xEbTypeConteneur;
    }

    public void setxEbTypeConteneur(String xEbTypeConteneur) {
        this.xEbTypeConteneur = xEbTypeConteneur;
    }

    public void setChildren(List<EbMarchandise> children) {
        this.children = children;
    }

    public String getTypeUnitLabel() {
        return typeUnitLabel;
    }

    public void setTypeUnitLabel(String typeUnitLabel) {
        this.typeUnitLabel = typeUnitLabel;
    }

    public String getxEbTypeUnit() {
        return xEbTypeUnit;
    }

    public void setxEbTypeUnit(String xEbTypeUnit) {
        this.xEbTypeUnit = xEbTypeUnit;
    }

    public List<EbLivraisonLine> getxEbLivraisonLines() {
        return xEbLivraisonLines;
    }

    public void setxEbLivraisonLines(List<EbLivraisonLine> xEbLivraisonLines) {
        this.xEbLivraisonLines = xEbLivraisonLines;
    }

    public String getCustomerOrderReference() {
        return customerOrderReference;
    }

    public void setCustomerOrderReference(String customerOrderReference) {
        this.customerOrderReference = customerOrderReference;
    }

    public Long getIdEbDelLivraison() {
        return idEbDelLivraison;
    }

    public void setIdEbDelLivraison(Long idEbDelLivraison) {
        this.idEbDelLivraison = idEbDelLivraison;
    }

    public Boolean getSerialized() {
        return serialized;
    }

    public void setSerialized(Boolean serialized) {
        this.serialized = serialized;
    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;
    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }
}
