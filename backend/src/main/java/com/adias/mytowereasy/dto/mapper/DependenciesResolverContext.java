package com.adias.mytowereasy.dto.mapper;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.mapstruct.AfterMapping;
import org.mapstruct.BeforeMapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.adias.mytowereasy.api.service.CustomFieldsApiService;
import com.adias.mytowereasy.api.wso.DeliveryLineWSO;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.dto.EbLivraisonLineDto;
import com.adias.mytowereasy.dto.EbOrderDto;
import com.adias.mytowereasy.dto.EbOrderLineDto;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.repository.EcCountryRepository;
import com.adias.mytowereasy.repository.EcCurrencyRepository;


@Component
public class DependenciesResolverContext {
    public static DependenciesResolverContext instance;

    @PostConstruct
    public void init() {
        instance = this;
    }

    @Autowired
    private EbUserRepository userRepository;
    
    @Autowired
    CustomFieldsApiService customFieldsApiService;

    @Autowired
    EcCountryRepository ecCountryRepository;

    @Autowired
    EcCurrencyRepository ecCurrencyRepository;

    @BeforeMapping
    public EbUser user(EbOrderDto dto) {

        if (StringUtils.isEmpty(dto.getRequestOwnerEmail())) {
            throw new MyTowerException("requestOwnerEmail is null");
        }

        EbUser user = userRepository.findOneByEmailIgnoreCase(dto.getRequestOwnerEmail());

        if (user == null) {
            throw new MyTowerException(String.format("user with mail %s does not exist", dto.getRequestOwnerEmail()));
        }

        return user;
    }

    @BeforeMapping
    public EcCurrency resolveDeliveryWSOCurrencyByCode(DeliveryLineWSO wso) {

        if (wso.getPrice() != null && wso.getCurrencyCode() == null) {
            throw new MyTowerException("Currency Code is null");
        }

				Optional<EcCurrency> currency = null;

        if (wso.getCurrencyCode() != null) {
            currency = ecCurrencyRepository.findByCodeIgnoreCase(wso.getCurrencyCode().toLowerCase());

						if (!currency.isPresent())
							throw new MyTowerException(
								"currency with code " + wso.getCurrencyCode() + " not found"
							);
        }

					return currency.get();
    }

    @BeforeMapping
    public EcCurrency resolveOrderDtoCurrencyById(EbOrderLineDto dto) {
        return getCurrencyById(dto.getPrice(), dto.getCurrency());
    }

    @BeforeMapping
    public EcCurrency resolveDeliveryDtoCurrencyById(EbLivraisonLineDto dto) {
        return getCurrencyById(dto.getPrice(), dto.getCurrency());
    }

    private EcCurrency getCurrencyById(Double price, Integer currencyNum) {

        if (price != null && currencyNum == null) {
            throw new MyTowerException("Currency is null");
        }

        EcCurrency currency = null;

        if (currencyNum != null) {
            currency = ecCurrencyRepository.findById(currencyNum).orElse(null);
        }

        return currency;
    }

    @AfterMapping
    public EbOrderLine resolveOrderLineCurrencyAfterMapping(@MappingTarget EbOrderLine ebOrderLine) {

        if (ebOrderLine.getCurrency() != null && ebOrderLine.getCurrency().getEcCurrencyNum() == null) {
            ebOrderLine.setCurrency(null);
        }

        return ebOrderLine;
    }
}
