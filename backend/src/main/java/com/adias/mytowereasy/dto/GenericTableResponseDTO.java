package com.adias.mytowereasy.dto;

import java.util.List;


public class GenericTableResponseDTO<T> {
    private Long count;
    private List<T> data;

    public GenericTableResponseDTO() {
        super();
    }

    public GenericTableResponseDTO(List<T> data, Long count) {
        super();
        this.count = count;
        this.data = data;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
