package com.adias.mytowereasy.dto;

import com.querydsl.core.annotations.QueryProjection;


public class EbTypeMarchandiseDto {
    private Long xEbTypeUnit;
    private String label;
    private Integer xEbTypeConteneur;

    public Integer getxEbTypeConteneur() {
        return xEbTypeConteneur;
    }

    public void setxEbTypeConteneur(Integer xEbTypeConteneur) {
        this.xEbTypeConteneur = xEbTypeConteneur;
    }

    public Long getxEbTypeUnit() {
        return xEbTypeUnit;
    }

    public void setxEbTypeUnit(Long long1) {
        this.xEbTypeUnit = long1;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public EbTypeMarchandiseDto() {
    }

    @QueryProjection
    public EbTypeMarchandiseDto(Long xEbTypeUnit, String labelTypeUnit, Integer xEbTypeConteneur) {
        this.xEbTypeUnit = xEbTypeUnit;
        this.label = labelTypeUnit;
        this.xEbTypeConteneur = xEbTypeConteneur;
    }
}
