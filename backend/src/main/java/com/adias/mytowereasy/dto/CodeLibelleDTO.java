package com.adias.mytowereasy.dto;

public class CodeLibelleDTO {
    private Integer code;
    private String libelle;

    public CodeLibelleDTO() {
    }

    public Integer getCode() {
        return code;
    }

    public CodeLibelleDTO(Integer code, String libelle) {
        super();
        this.code = code;
        this.libelle = libelle;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
