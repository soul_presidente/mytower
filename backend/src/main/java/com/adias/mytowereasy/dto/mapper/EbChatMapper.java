package com.adias.mytowereasy.dto.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import com.adias.mytowereasy.dto.ChatDTO;
import com.adias.mytowereasy.model.EbChat;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EbChatMapper {
    @Mapping(target = "chatOwner", ignore = true)
    EbChat dtoToModel(ChatDTO dto);

    ChatDTO modelToDto(EbChat model);

    List<EbChat> dtosToModels(List<ChatDTO> dtos);

    List<ChatDTO> modelsToDtos(List<EbChat> models);
}
