package com.adias.mytowereasy.dto.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import com.adias.mytowereasy.dto.EbMarchandiseDto;
import com.adias.mytowereasy.dto.EbTypeMarchandiseDto;
import com.adias.mytowereasy.dto.EbTypeUnitDto;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbTypeUnit;
import com.adias.mytowereasy.model.Enumeration.TypeContainer;
import com.adias.mytowereasy.model.TreeNode;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TransportMapper {
    @Mappings({
        @Mapping(source = "dateCreation", target = "dateCreation", dateFormat = "dd/MM/YYYY"),
        @Mapping(source = "xEbTypeConteneur", target = "typeUnitLabel", qualifiedByName = "typeUnit")
    })
    EbMarchandiseDto ebMarchandiseToEbMarchandiseDto(EbMarchandise marchandise);

    List<EbMarchandiseDto> ebMarchandisesToEbMarchandisesDto(List<EbMarchandise> marchandises);

    @Mappings({
        @Mapping(ignore = true, target = "parent")
    })
    TreeNode<EbMarchandiseDto> ebMarchandiseTreeToEbMarchandiseTreeDto(TreeNode<EbMarchandise> tree);

    @Mappings({
        @Mapping(source = "gerbable", target = "gerbable", qualifiedByName = "gerbable"),
        @Mapping(source = "gerbable", target = "gerbableValue"),
        @Mapping(source = "activated", target = "activated", qualifiedByName = "activated"),
        @Mapping(source = "xEbTypeContainer", target = "containerLabel", qualifiedByName = "typeUnit"),
        @Mapping(source = "xEbTypeContainer", target = "xEbTypeContainer")
    })
    EbTypeUnitDto ebTypeUnitToEbTypeUnitDto(EbTypeUnit typeUnit);

    @Mappings({
        @Mapping(source = "gerbableValue", target = "gerbable"), @Mapping(source = "activated", target = "activated")
    })
    EbTypeUnit ebTypeUnitDtoToEbTypeUnit(EbTypeUnitDto typeUnit);

    List<EbTypeUnitDto> ebTypeUnitsToEbTypeUnitDtos(List<EbTypeUnit> typeUnit);

    @Mappings({
        @Mapping(source = "libelle", target = "label"),
        @Mapping(source = "ebTypeUnitNum", target = "xEbTypeUnit"),
        @Mapping(source = "xEbTypeContainer", target = "xEbTypeConteneur")
    })
    EbTypeMarchandiseDto ebTypeUnitToEbTypeMarchandise(EbTypeUnit typeUnit);

    List<EbTypeMarchandiseDto> ebTypeUnitsToEbTypesMarchandise(List<EbTypeUnit> typeUnits);

    @Named("gerbable")
    default String gerbableLabel(Boolean g) {
        if (g == null) return "Non";
        return g ? "Oui" : "Non";
    }

    @Named("typeUnit")
    default String typeContainerLabel(Integer key) {
        if (key == null) return "";
        return TypeContainer.getTypeContainerByKey(key).getValue();
    }
}
