package com.adias.mytowereasy.dto;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.adias.mytowereasy.model.EbFlag;


public class EbFlagDTO {
    private Integer ebFlagNum;

    private Integer icon;

    private String color;

    private String name;

    private String reference;

    public EbFlagDTO() {
    }

    public EbFlagDTO(EbFlag ebFlag) {
        this.ebFlagNum = ebFlag.getEbFlagNum();
        this.icon = ebFlag.getIcon();
        this.color = ebFlag.getColor();
        this.name = ebFlag.getName();
        this.reference = ebFlag.getReference();
    }

    public Integer getEbFlagNum() {
        return ebFlagNum;
    }

    public void setEbFlagNum(Integer ebFlagNum) {
        this.ebFlagNum = ebFlagNum;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public static List<EbFlagDTO> sortByName(List<EbFlagDTO> listFlagDTO) {
        Collections.sort(listFlagDTO, new Comparator<EbFlagDTO>() {
            @Override
            public int compare(EbFlagDTO f1, EbFlagDTO f2) {
                if (f1.getName() == null) return -1;
                if (f2.getName() == null) return 1;
                return f1.getName().compareTo(f2.getName());
            }
        });
        return listFlagDTO;
    }
}
