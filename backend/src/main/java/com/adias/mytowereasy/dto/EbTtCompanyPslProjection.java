package com.adias.mytowereasy.dto;

public interface EbTtCompanyPslProjection {
    public Integer getEbTtCompanyPslNum();

    public Boolean getStartPsl();

    public Boolean getEndPsl();
}
