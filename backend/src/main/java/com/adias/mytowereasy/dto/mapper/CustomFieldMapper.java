package com.adias.mytowereasy.dto.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import com.adias.mytowereasy.api.wso.CustomFieldWSO;
import com.adias.mytowereasy.model.CustomFields;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CustomFieldMapper {
    @Mappings({
        @Mapping(source = "num", target = "itemId"), @Mapping(source = "name", target = "code")
    })
    CustomFieldWSO customFieldToCustomFieldWSO(CustomFields customFields);
}
