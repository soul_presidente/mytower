/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dto;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Transient;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;


public class EbLivraisonDto {
    private Long ebDelLivraisonNum;

    private String refTransport;

    private String numOrderSAP;

    private String numOrderEDI;

    private String numOrderCustomer;

    private String orderCustomerCode;

    private String orderCustomerName;

    private String refDelivery;

    private String refClientCreator;

    private String nameOrderCreator;

    private String refClientDelivered;

    private String nameClientDelivered;

    private String countryClientDelivered;

    private String campaignCode;

    private String campaignName;

    private String orderCreationDate;

    private String expectedDeliveryDate;

    private String requestedDeliveryDate;

    private String deliveryCreationDate;

    private String estimatedShipDate;

    private String effectiveShipDate;

    private String pickupSite;

    private String endOfPackDate;

    private String pickDate;

    private String invoiceNum;

    private String invoiceDate;

    private Integer statusDelivery;

    private Integer transportStatus;

    private Long totalQuantityProducts;

    private Long numParcels;

    private Double totalWeight;

    private Double totalVolume;

    private EbPartyDto xEbPartyOrigin;

    private EbPartyDto xEbPartyDestination;

    private EbPartyDto xEbPartySale;

    private EbOrderDto xEbDelOrder;

    private String pathToBl;

    private String listFlag;

    private List<EbLivraisonLine> xEbDelLivraisonLine;

    private String crossDock;

    private EbDemande xEbDemande;

    private String customerOrderReference;

    private EbEtablissementDTO ebEtablissement;

    private EbCompagnieDTO ebCompagnie;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbFlagDTO> listEbFlagDTO;

    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    private String customFields;

    private List<EbCategorie> listCategories;

    private EbUser xEbOwnerOfTheRequest;

    private Integer xEbIncotermNum;

    private String xEcIncotermLibelle;

    private String complementaryInformations;

    private Timestamp dateCreationSys;

    private String ebDelReference;

    private String refOrder;

    private Date dateOfGoodsAvailability;

    private Date customerDeliveryDate;

    public Timestamp getDateCreationSys() {
        return dateCreationSys;
    }

    public void setDateCreationSys(Timestamp dateCreationSys) {
        this.dateCreationSys = dateCreationSys;
    }

    public String getNumOrderSAP() {
        return numOrderSAP;
    }

    public void setNumOrderSAP(String numOrderSAP) {
        this.numOrderSAP = numOrderSAP;
    }

    public String getListFlag() {
        return listFlag;
    }

    public void setListFlag(String listFlag) {
        this.listFlag = listFlag;
    }

    public List<EbFlagDTO> getListEbFlagDTO() {
        return listEbFlagDTO;
    }

    public void setListEbFlagDTO(List<EbFlagDTO> listEbFlagDTO) {
        this.listEbFlagDTO = listEbFlagDTO;
    }

    public String getNumOrderEDI() {
        return numOrderEDI;
    }

    public void setNumOrderEDI(String numOrderEDI) {
        this.numOrderEDI = numOrderEDI;
    }

    public String getNumOrderCustomer() {
        return numOrderCustomer;
    }

    public void setNumOrderCustomer(String numOrderCustomer) {
        this.numOrderCustomer = numOrderCustomer;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getOrderCreationDate() {
        return orderCreationDate;
    }

    public void setOrderCreationDate(String orderCreationDate) {
        this.orderCreationDate = orderCreationDate;
    }

    public String getDeliveryCreationDate() {
        return deliveryCreationDate;
    }

    public void setDeliveryCreationDate(String deliveryCreationDate) {
        this.deliveryCreationDate = deliveryCreationDate;
    }

    public String getPickupSite() {
        return pickupSite;
    }

    public void setPickupSite(String pickupSite) {
        this.pickupSite = pickupSite;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public Integer getStatusDelivery() {
        return statusDelivery;
    }

    public void setStatusDelivery(Integer statusDelivery) {
        this.statusDelivery = statusDelivery;
    }

    public Integer getTransportStatus() {
        return transportStatus;
    }

    public void setTransportStatus(Integer transportStatus) {
        this.transportStatus = transportStatus;
    }

    public Long getTotalQuantityProducts() {
        return totalQuantityProducts;
    }

    public void setTotalQuantityProducts(Long totalQuantityProducts) {
        this.totalQuantityProducts = totalQuantityProducts;
    }

    public Long getNumParcels() {
        return numParcels;
    }

    public void setNumParcels(Long numParcels) {
        this.numParcels = numParcels;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(Double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public EbPartyDto getxEbPartyOrigin() {
        return xEbPartyOrigin;
    }

    public void setxEbPartyOrigin(EbPartyDto xEbPartyOrigin) {
        this.xEbPartyOrigin = xEbPartyOrigin;
    }

    public EbPartyDto getxEbPartyDestination() {
        return xEbPartyDestination;
    }

    public void setxEbPartyDestination(EbPartyDto xEbPartyDestination) {
        this.xEbPartyDestination = xEbPartyDestination;
    }

    public EbPartyDto getxEbPartySale() {
        return xEbPartySale;
    }

    public void setxEbPartySale(EbPartyDto xEbPartySale) {
        this.xEbPartySale = xEbPartySale;
    }

    public Long getEbDelLivraisonNum() {
        return ebDelLivraisonNum;
    }

    public String getRequestedDeliveryDate() {
        return requestedDeliveryDate;
    }

    public void setRequestedDeliveryDate(String requestedDeliveryDate) {
        this.requestedDeliveryDate = requestedDeliveryDate;
    }

    public String getPickDate() {
        return pickDate;
    }

    public void setPickDate(String pickDate) {
        this.pickDate = pickDate;
    }

    public void setEbDelLivraisonNum(Long ebDelLivraisonNum) {
        this.ebDelLivraisonNum = ebDelLivraisonNum;
    }

    public String getPathToBl() {
        return pathToBl;
    }

    public void setPathToBl(String pathToBl) {
        this.pathToBl = pathToBl;
    }

    public String getExpectedDeliveryDate() {
        return expectedDeliveryDate;
    }

    public void setExpectedDeliveryDate(String expectedDeliveryDate) {
        this.expectedDeliveryDate = expectedDeliveryDate;
    }

    public String getRefDelivery() {
        return refDelivery;
    }

    public String getRefClientCreator() {
        return refClientCreator;
    }

    public String getNameOrderCreator() {
        return nameOrderCreator;
    }

    public String getRefClientDelivered() {
        return refClientDelivered;
    }

    public String getNameClientDelivered() {
        return nameClientDelivered;
    }

    public String getCountryClientDelivered() {
        return countryClientDelivered;
    }

    public void setRefDelivery(String refDelivery) {
        this.refDelivery = refDelivery;
    }

    public void setRefClientCreator(String refClientCreator) {
        this.refClientCreator = refClientCreator;
    }

    public void setNameOrderCreator(String nameOrderCreator) {
        this.nameOrderCreator = nameOrderCreator;
    }

    public void setRefClientDelivered(String refClientDelivered) {
        this.refClientDelivered = refClientDelivered;
    }

    public void setNameClientDelivered(String nameClientDelivered) {
        this.nameClientDelivered = nameClientDelivered;
    }

    public void setCountryClientDelivered(String countryClientDelivered) {
        this.countryClientDelivered = countryClientDelivered;
    }

    public String getOrderCustomerCode() {
        return orderCustomerCode;
    }

    public void setOrderCustomerCode(String orderCustomerCode) {
        this.orderCustomerCode = orderCustomerCode;
    }

    public String getOrderCustomerName() {
        return orderCustomerName;
    }

    public void setOrderCustomerName(String orderCustomerName) {
        this.orderCustomerName = orderCustomerName;
    }

    public List<EbLivraisonLine> getxEbDelLivraisonLine() {
        return xEbDelLivraisonLine;
    }

    public void setxEbDelLivraisonLine(List<EbLivraisonLine> xEbDelLivraisonLine) {
        this.xEbDelLivraisonLine = xEbDelLivraisonLine;
    }

    public EbOrderDto getxEbDelOrder() {
        return xEbDelOrder;
    }

    public void setxEbDelOrder(EbOrderDto xEbDelOrder) {
        this.xEbDelOrder = xEbDelOrder;
    }

    public String getEstimatedShipDate() {
        return estimatedShipDate;
    }

    public void setEstimatedShipDate(String estimatedShipDate) {
        this.estimatedShipDate = estimatedShipDate;
    }

    public String getEffectiveShipDate() {
        return effectiveShipDate;
    }

    public void setEffectiveShipDate(String effectiveShipDate) {
        this.effectiveShipDate = effectiveShipDate;
    }

    public String getEndOfPackDate() {
        return endOfPackDate;
    }

    public void setEndOfPackDate(String endOfPackDate) {
        this.endOfPackDate = endOfPackDate;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getRefTransport() {
        return refTransport;
    }

    public void setRefTransport(String refTransport) {
        this.refTransport = refTransport;
    }

    public String getCrossDock() {
        return crossDock;
    }

    public void setCrossDock(String crossDock) {
        this.crossDock = crossDock;
    }

    public EbDemande getxEbDemande() {
        return xEbDemande;
    }

    public void setxEbDemande(EbDemande xEbDemande) {
        this.xEbDemande = xEbDemande;
    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;
    }

    public List<EbCategorie> getListCategories() {
        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {
        this.listCategories = listCategories;
    }

    public EbUser getxEbOwnerOfTheRequest() {
        return xEbOwnerOfTheRequest;
    }

    public void setxEbOwnerOfTheRequest(EbUser xEbOwnerOfTheRequest) {
        this.xEbOwnerOfTheRequest = xEbOwnerOfTheRequest;
    }

    public Integer getxEbIncotermNum() {
        return xEbIncotermNum;
    }

    public void setxEbIncotermNum(Integer xEbIncotermNum) {
        this.xEbIncotermNum = xEbIncotermNum;
    }

    public String getxEcIncotermLibelle() {
        return xEcIncotermLibelle;
    }

    public void setxEcIncotermLibelle(String xEcIncotermLibelle) {
        this.xEcIncotermLibelle = xEcIncotermLibelle;
    }

    public String getComplementaryInformations() {
        return complementaryInformations;
    }

    public void setComplementaryInformations(String complementaryInformations) {
        this.complementaryInformations = complementaryInformations;
    }

    public String getCustomerOrderReference() {
        return customerOrderReference;
    }

    public void setCustomerOrderReference(String customerOrderReference) {
        this.customerOrderReference = customerOrderReference;
    }

    public String getEbDelReference() {
        return ebDelReference;
    }

    public void setEbDelReference(String ebDelReference) {
        this.ebDelReference = ebDelReference;
    }

    public String getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(String refOrder) {
        this.refOrder = refOrder;
    }

    public Date getDateOfGoodsAvailability() {
        return dateOfGoodsAvailability;
    }

    public void setDateOfGoodsAvailability(Date dateOfGoodsAvailability) {
        this.dateOfGoodsAvailability = dateOfGoodsAvailability;
    }

    public EbEtablissementDTO getEbEtablissement() {
        return ebEtablissement;
    }

    public void setEbEtablissement(EbEtablissementDTO ebEtablissement) {
        this.ebEtablissement = ebEtablissement;
    }

    public EbCompagnieDTO getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnieDTO ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    public Date getCustomerDeliveryDate() {
        return customerDeliveryDate;
    }

    public void setCustomerDeliveryDate(Date customerDeliveryDate) {
        this.customerDeliveryDate = customerDeliveryDate;
    }
}
