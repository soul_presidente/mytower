package com.adias.mytowereasy.dto;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.factory.annotation.Value;

import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.model.tt.EbTtCategorieDeviation;
import com.adias.mytowereasy.model.tt.EbTtTracing;


public interface EbTteventProjection {
    @Value("#{(target.natureDate!=null && target.natureDate==1)?'ACTUAL_DATE':(target.natureDate!=null && target.natureDate==2)?'ESTIMATED_DATE':(target.natureDate!=null && target.natureDate==3)?'NEGOTIATED_DATE':''}")
    public String getTypeDate();

    public Integer getTypeEvent();

    public Integer getEbTtEventNum();

    public Date getDateEvent();

    @Value("#{target?.categorie?.libelle}")
    public String getNature();

    public String getCommentaire();

    public Boolean getValidateByChamp();

    public Date getDateCreation();

    public Timestamp getDateMajSys();

    @Value("#{target.xEbUser!=null? target.xEbUser.nom+' '+target.xEbUser.prenom:''}")
    public String getUser();

    public Integer getQuantity();

    public EbTTPslApp getxEbTtPslApp();

    public EbTtTracing getxEbTtTracing();

    public EbTtCategorieDeviation getCategorie();

    public Integer getEbQmIncidentNum();

    public Integer getEbQmDeviationNum();
}
