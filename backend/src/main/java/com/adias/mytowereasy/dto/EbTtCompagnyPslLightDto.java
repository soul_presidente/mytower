package com.adias.mytowereasy.dto;

import java.util.Date;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;


public class EbTtCompagnyPslLightDto {
    private Integer id;

    private String libelle;

    private String codeAlpha;

    private Boolean schemaActif;

    private Boolean startPsl;

    private Boolean endPsl;

    private Integer order;

    private Date expectedDate;

    private Boolean isChecked;

    public EbTtCompagnyPslLightDto() {
    }

    public EbTtCompagnyPslLightDto(EbTtCompanyPsl psl) {
        this.id = psl.getEbTtCompanyPslNum();
        this.libelle = psl.getLibelle();
        this.codeAlpha = psl.getCodeAlpha();
        this.schemaActif = psl.getSchemaActif();
        this.startPsl = psl.getStartPsl();
        this.endPsl = psl.getEndPsl();
        this.order = psl.getOrder();
        this.expectedDate = psl.getExpectedDate();
        this.isChecked = psl.getIsChecked();
    }

    public Integer getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }

    public String getCodeAlpha() {
        return codeAlpha;
    }

    public Boolean getSchemaActif() {
        return schemaActif;
    }

    public Boolean getStartPsl() {
        return startPsl;
    }

    public Boolean getEndPsl() {
        return endPsl;
    }

    public Integer getOrder() {
        return order;
    }

    public Date getExpectedDate() {
        return expectedDate;
    }

    public Boolean getIsChecked() {
        return isChecked;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setCodeAlpha(String codeAlpha) {
        this.codeAlpha = codeAlpha;
    }

    public void setSchemaActif(Boolean schemaActif) {
        this.schemaActif = schemaActif;
    }

    public void setStartPsl(Boolean startPsl) {
        this.startPsl = startPsl;
    }

    public void setEndPsl(Boolean endPsl) {
        this.endPsl = endPsl;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public void setExpectedDate(Date expectedDate) {
        this.expectedDate = expectedDate;
    }

    public void setIsChecked(Boolean isChecked) {
        this.isChecked = isChecked;
    }
}
