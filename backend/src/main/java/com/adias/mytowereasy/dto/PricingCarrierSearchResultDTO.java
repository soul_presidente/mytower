package com.adias.mytowereasy.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.trpl.dto.DetailUnitDTO;
import com.adias.mytowereasy.trpl.model.EbPlCostItem;


public class PricingCarrierSearchResultDTO {
    /**
     * The transporteur EbUser.ebUserNum
     * 
     * @see EbUser
     */
    private Integer carrierUserNum;

    private String carrierName;
    private String carrierEmail;
    private Integer carrierEtabNum;
    private String carrierEtabName;
    private Integer carrierCompanyNum;
    private String carrierCompanyName;
    private BigDecimal calculatedPrice = BigDecimal.ZERO;
    private String planRef;
    private String refGrille;
    private Integer planNum;
    private String comment;
    private Integer transitTime;
    private Boolean exchangeRateFound;
    private Integer modeTransport;
    private Integer typeDemande;
    private boolean horsGrille;

    private List<DetailUnitDTO> listUnit = new ArrayList<>();
    private List<EbPlCostItem> listPlCostItem = new ArrayList<>();

    /**
     * Dangerous flag field Null if not specified
     */
    private Boolean dangerous;

    public List<DetailUnitDTO> getListUnit() {
        return listUnit;
    }

    public void setListUnit(List<DetailUnitDTO> listUnit) {
        this.listUnit = listUnit;
    }

    public Integer getCarrierUserNum() {
        return carrierUserNum;
    }

    public void setCarrierUserNum(Integer carrierUserNum) {
        this.carrierUserNum = carrierUserNum;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public String getCarrierEmail() {
        return carrierEmail;
    }

    public void setCarrierEmail(String carrierEmail) {
        this.carrierEmail = carrierEmail;
    }

    public Integer getCarrierEtabNum() {
        return carrierEtabNum;
    }

    public void setCarrierEtabNum(Integer carrierEtabNum) {
        this.carrierEtabNum = carrierEtabNum;
    }

    public String getCarrierEtabName() {
        return carrierEtabName;
    }

    public void setCarrierEtabName(String carrierEtabName) {
        this.carrierEtabName = carrierEtabName;
    }

    public Integer getCarrierCompanyNum() {
        return carrierCompanyNum;
    }

    public void setCarrierCompanyNum(Integer carrierCompanyNum) {
        this.carrierCompanyNum = carrierCompanyNum;
    }

    public String getCarrierCompanyName() {
        return carrierCompanyName;
    }

    public void setCarrierCompanyName(String carrierCompanyName) {
        this.carrierCompanyName = carrierCompanyName;
    }

    public BigDecimal getCalculatedPrice() {
        return calculatedPrice;
    }

    public void setCalculatedPrice(BigDecimal calculatedPrice) {
        this.calculatedPrice = calculatedPrice;
    }

    public String getPlanRef() {
        return planRef;
    }

    public void setPlanRef(String planRef) {
        this.planRef = planRef;
    }

    public Integer getPlanNum() {
        return planNum;
    }

    public void setPlanNum(Integer planNum) {
        this.planNum = planNum;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public Boolean getExchangeRateFound() {
        return exchangeRateFound;
    }

    public void setExchangeRateFound(Boolean exchangeRateFound) {
        this.exchangeRateFound = exchangeRateFound;
    }

    public Boolean getDangerous() {
        return dangerous;
    }

    public void setDangerous(Boolean dangerous) {
        this.dangerous = dangerous;
    }

    public Integer getModeTransport() {
        return modeTransport;
    }

    public void setModeTransport(Integer modeTransport) {
        this.modeTransport = modeTransport;
    }

    public List<EbPlCostItem> getListPlCostItem() {
        return listPlCostItem;
    }

    public void setListPlCostItem(List<EbPlCostItem> listPlCostItem) {
        this.listPlCostItem = listPlCostItem;
    }

    public String getRefGrille() {
        return refGrille;
    }

    public void setRefGrille(String refGrille) {
        this.refGrille = refGrille;
    }

    public Integer getTypeDemande() {
        return typeDemande;
    }

    public void setTypeDemande(Integer typeDemande) {
        this.typeDemande = typeDemande;
    }

	public boolean isHorsGrille() {
		return horsGrille;
	}

	public void setHorsGrille(boolean horsGrille) {
		this.horsGrille = horsGrille;
	}
}
