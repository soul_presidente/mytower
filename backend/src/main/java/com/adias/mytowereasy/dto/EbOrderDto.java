/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dto;

import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbUser;


public class EbOrderDto {
    private Long ebDelOrderNum;

    private String numOrderSAP;

    private String numOrderEDI;

    private String numOrderCustomer;

    private String orderCustomerCode;

    private String orderCustomerName;

    private String refClientCreator;

    private String nameOrderCreator;

    private String refClientDelivered;

    private String nameClientDelivered;

    private String countryClientDelivered;

    private String etablissementName;

    private String orderType;

    private String campaignCode;

    private String campaignName;

    private String orderStatus;

    private Integer orderStatusId;

    private String customerCreationDate;

    private String deliveryDate;

    private String etd;

    private String eta;

    private String meanOfTransport;

    private Integer numberOfItems;

    private String departureSite;

    private BigDecimal grossWeight;

    private BigDecimal netWeight;

    private String deliveryCity;

    private String numberOfEdit;

    private String purchaseOrderType;

    private Long totalQuantityOrdered;
    private Long totalQuantityPlannable;
    private Long totalQuantityPlanned;

    private Long totalQuantityPending;
    private Long totalQuantityConfirmed;
    private Long totalQuantityShipped;
    private Long totalQuantityDelivered;

    private Long totalQuantityCanceled;

    private Integer canceled;

    private EbPartyDto xEbPartyOrigin;

    private EbPartyDto xEbPartyDestination;

    private EbPartyDto xEbPartySale;

    private String listLabels;

    private List<EbOrderLineDto> orderLines;

    private String shipTo;

    private String listFlag;

    private String crossDock;

    @JsonDeserialize
    @JsonSerialize
    private List<EbFlagDTO> listEbFlagDTO;

    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    private String customFields;

    private List<EbCategorie> listCategories;

    private String targetDate;

    private String orderDate;

    private EbPartyDto xEbPartyVendor;

    private EbPartyDto xEbPartyIssuer;

    private EbUser xEbOwnerOfTheRequest;

    private Integer xEbIncotermNum;

    private String xEcIncotermLibelle;

    private String complementaryInformations;

    private String customerOrderReference;

    private String reference;

    private String requestOwnerEmail;
    @JsonDeserialize
    @JsonSerialize
    private List<EbLivraison> livraisons;

    public EbUser getxEbOwnerOfTheRequest() {
        return xEbOwnerOfTheRequest;
    }

    public void setxEbOwnerOfTheRequest(EbUser xEbOwnerOfTheRequest) {
        this.xEbOwnerOfTheRequest = xEbOwnerOfTheRequest;
    }

    public String getListFlag() {
        return listFlag;
    }

    public void setListFlag(String listFlag) {
        this.listFlag = listFlag;
    }

    public List<EbFlagDTO> getListEbFlagDTO() {
        return listEbFlagDTO;
    }

    public void setListEbFlagDTO(List<EbFlagDTO> listEbFlagDTO) {
        this.listEbFlagDTO = listEbFlagDTO;
    }

    public String getNumOrderSAP() {
        return numOrderSAP;
    }

    public void setNumOrderSAP(String numOrderSAP) {
        this.numOrderSAP = numOrderSAP;
    }

    public String getNumOrderEDI() {
        return numOrderEDI;
    }

    public void setNumOrderEDI(String numOrderEDI) {
        this.numOrderEDI = numOrderEDI;
    }

    public String getNumOrderCustomer() {
        return numOrderCustomer;
    }

    public void setNumOrderCustomer(String numOrderCustomer) {
        this.numOrderCustomer = numOrderCustomer;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getCampaignCode() {
        return campaignCode;
    }

    public void setCampaignCode(String campaignCode) {
        this.campaignCode = campaignCode;
    }

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getCustomerCreationDate() {
        return customerCreationDate;
    }

    public void setCustomerCreationDate(String customerCreationDate) {
        this.customerCreationDate = customerCreationDate;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public EbPartyDto getxEbPartyOrigin() {
        return xEbPartyOrigin;
    }

    public void setxEbPartyOrigin(EbPartyDto xEbPartyOrigin) {
        this.xEbPartyOrigin = xEbPartyOrigin;
    }

    public EbPartyDto getxEbPartyDestination() {
        return xEbPartyDestination;
    }

    public void setxEbPartyDestination(EbPartyDto xEbPartyDestination) {
        this.xEbPartyDestination = xEbPartyDestination;
    }

    public EbPartyDto getxEbPartySale() {
        return xEbPartySale;
    }

    public void setxEbPartySale(EbPartyDto xEbPartySale) {
        this.xEbPartySale = xEbPartySale;
    }

    public Long getEbDelOrderNum() {
        return ebDelOrderNum;
    }

    public void setEbDelOrderNum(Long ebDelOrderNum) {
        this.ebDelOrderNum = ebDelOrderNum;
    }

    public String getPurchaseOrderType() {
        return purchaseOrderType;
    }

    public void setPurchaseOrderType(String purchaseOrderType) {
        this.purchaseOrderType = purchaseOrderType;
    }

    public String getRefClientCreator() {
        return refClientCreator;
    }

    public String getNameOrderCreator() {
        return nameOrderCreator;
    }

    public String getRefClientDelivered() {
        return refClientDelivered;
    }

    public String getNameClientDelivered() {
        return nameClientDelivered;
    }

    public String getCountryClientDelivered() {
        return countryClientDelivered;
    }

    public void setRefClientCreator(String refClientCreator) {
        this.refClientCreator = refClientCreator;
    }

    public void setNameOrderCreator(String nameOrderCreator) {
        this.nameOrderCreator = nameOrderCreator;
    }

    public void setRefClientDelivered(String refClientDelivered) {
        this.refClientDelivered = refClientDelivered;
    }

    public void setNameClientDelivered(String nameClientDelivered) {
        this.nameClientDelivered = nameClientDelivered;
    }

    public void setCountryClientDelivered(String countryClientDelivered) {
        this.countryClientDelivered = countryClientDelivered;
    }

    public String getOrderCustomerCode() {
        return orderCustomerCode;
    }

    public void setOrderCustomerCode(String orderCustomerCode) {
        this.orderCustomerCode = orderCustomerCode;
    }

    public String getOrderCustomerName() {
        return orderCustomerName;
    }

    public void setOrderCustomerName(String orderCustomerName) {
        this.orderCustomerName = orderCustomerName;
    }

    public String getListLabels() {
        return listLabels;
    }

    public List<EbOrderLineDto> getOrderLines() {
        return orderLines;
    }

    public void setListLabels(String listLabels) {
        this.listLabels = listLabels;
    }

    public void setOrderLines(List<EbOrderLineDto> orderLines) {
        this.orderLines = orderLines;
    }

    public String getEtd() {
        return etd;
    }

    public String getEta() {
        return eta;
    }

    public String getMeanOfTransport() {
        return meanOfTransport;
    }

    public Integer getNumberOfItems() {
        return numberOfItems;
    }

    public String getDepartureSite() {
        return departureSite;
    }

    public BigDecimal getGrossWeight() {
        return grossWeight;
    }

    public BigDecimal getNetWeight() {
        return netWeight;
    }

    public String getDeliveryCity() {
        return deliveryCity;
    }

    public String getNumberOfEdit() {
        return numberOfEdit;
    }

    public void setEtd(String etd) {
        this.etd = etd;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public void setMeanOfTransport(String meanOfTransport) {
        this.meanOfTransport = meanOfTransport;
    }

    public void setNumberOfItems(Integer numberOfItems) {
        this.numberOfItems = numberOfItems;
    }

    public void setDepartureSite(String departureSite) {
        this.departureSite = departureSite;
    }

    public void setGrossWeight(BigDecimal grossWeight) {
        this.grossWeight = grossWeight;
    }

    public void setNetWeight(BigDecimal netWeight) {
        this.netWeight = netWeight;
    }

    public void setDeliveryCity(String deliveryCity) {
        this.deliveryCity = deliveryCity;
    }

    public void setNumberOfEdit(String numberOfEdit) {
        this.numberOfEdit = numberOfEdit;
    }

    public String getShipTo() {
        return shipTo;
    }

    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    public Integer getCanceled() {
        return canceled;
    }

    public void setCanceled(Integer canceled) {
        this.canceled = canceled;
    }

    public String getEtablissementName() {
        return etablissementName;
    }

    public void setEtablissementName(String etablissementName) {
        this.etablissementName = etablissementName;
    }

    public String getCrossDock() {
        return crossDock;
    }

    public void setCrossDock(String crossDock) {
        this.crossDock = crossDock;
    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;
    }

    public List<EbCategorie> getListCategories() {
        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {
        this.listCategories = listCategories;
    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public String getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(String targetDate) {
        this.targetDate = targetDate;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public EbPartyDto getxEbPartyVendor() {
        return xEbPartyVendor;
    }

    public void setxEbPartyVendor(EbPartyDto xEbPartyVendor) {
        this.xEbPartyVendor = xEbPartyVendor;
    }

    public EbPartyDto getxEbPartyIssuer() {
        return xEbPartyIssuer;
    }

    public void setxEbPartyIssuer(EbPartyDto xEbPartyIssuer) {
        this.xEbPartyIssuer = xEbPartyIssuer;
    }

    public Integer getxEbIncotermNum() {
        return xEbIncotermNum;
    }

    public void setxEbIncotermNum(Integer xEbIncotermNum) {
        this.xEbIncotermNum = xEbIncotermNum;
    }

    public String getxEcIncotermLibelle() {
        return xEcIncotermLibelle;
    }

    public void setxEcIncotermLibelle(String xEcIncotermLibelle) {
        this.xEcIncotermLibelle = xEcIncotermLibelle;
    }

    public String getComplementaryInformations() {
        return complementaryInformations;
    }

    public void setComplementaryInformations(String complementaryInformations) {
        this.complementaryInformations = complementaryInformations;
    }

    public String getCustomerOrderReference() {
        return customerOrderReference;
    }

    public void setCustomerOrderReference(String customerOrderReference) {
        this.customerOrderReference = customerOrderReference;
    }

    public Integer getOrderStatusId() {
        return orderStatusId;
    }

    public void setOrderStatusId(Integer orderStatusId) {
        this.orderStatusId = orderStatusId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String ebDelOrderReference) {
        this.reference = ebDelOrderReference;
    }

    public Long getTotalQuantityOrdered() {
        return totalQuantityOrdered;
    }

    public void setTotalQuantityOrdered(Long totalQuantityOrdered) {
        this.totalQuantityOrdered = totalQuantityOrdered;
    }

    public Long getTotalQuantityPlannable() {
        return totalQuantityPlannable;
    }

    public void setTotalQuantityPlannable(Long totalQuantityPlannable) {
        this.totalQuantityPlannable = totalQuantityPlannable;
    }

    public Long getTotalQuantityPlanned() {
        return totalQuantityPlanned;
    }

    public void setTotalQuantityPlanned(Long totalQuantityPlanned) {
        this.totalQuantityPlanned = totalQuantityPlanned;
    }

    public Long getTotalQuantityPending() {
        return totalQuantityPending;
    }

    public void setTotalQuantityPending(Long totalQuantityPending) {
        this.totalQuantityPending = totalQuantityPending;
    }

    public Long getTotalQuantityConfirmed() {
        return totalQuantityConfirmed;
    }

    public void setTotalQuantityConfirmed(Long totalQuantityConfirmed) {
        this.totalQuantityConfirmed = totalQuantityConfirmed;
    }

    public Long getTotalQuantityShipped() {
        return totalQuantityShipped;
    }

    public void setTotalQuantityShipped(Long totalQuantityShipped) {
        this.totalQuantityShipped = totalQuantityShipped;
    }

    public Long getTotalQuantityDelivered() {
        return totalQuantityDelivered;
    }

    public void setTotalQuantityDelivered(Long totalQuantityDelivered) {
        this.totalQuantityDelivered = totalQuantityDelivered;
    }

    public Long getTotalQuantityCanceled() {
        return totalQuantityCanceled;
    }

    public void setTotalQuantityCanceled(Long totalQuantityCanceled) {
        this.totalQuantityCanceled = totalQuantityCanceled;
    }

    public List<EbLivraison> getLivraisons() {
        return livraisons;
    }

    public void setLivraisons(List<EbLivraison> livraisons) {
        this.livraisons = livraisons;
    }

    public String getRequestOwnerEmail() {
        return requestOwnerEmail;
    }

    public void setRequestOwnerEmail(String requestOwnerEmail) {
        this.requestOwnerEmail = requestOwnerEmail;
    }
}
