package com.adias.mytowereasy.dto.mapper;

import java.util.Set;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.adias.mytowereasy.dto.ExUserModuleDTO;
import com.adias.mytowereasy.model.ExUserModule;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EcUserModuleMapper {
    Set<ExUserModuleDTO> listUserModuleToListUserModuleDTO(Set<ExUserModule> accessRights);
}
