package com.adias.mytowereasy.dto;

import java.util.Date;


public class ChatDTO {

    private Integer ebChatNum;

    private Integer idFiche;
    private Integer idChatComponent;
    private Integer module;

    private String userName;
    private String userAvatar;

    private String text;

    private Date dateCreation;

    private Boolean isHistory;

    public Integer getEbChatNum() {
        return ebChatNum;
    }

    public void setEbChatNum(Integer ebChatNum) {
        this.ebChatNum = ebChatNum;
    }

    public Integer getIdFiche() {
        return idFiche;
    }

    public void setIdFiche(Integer idFiche) {
        this.idFiche = idFiche;
    }

    public Integer getIdChatComponent() {
        return idChatComponent;
    }

    public void setIdChatComponent(Integer idChatComponent) {
        this.idChatComponent = idChatComponent;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Boolean getIsHistory() {
        return isHistory;
    }

    public void setIsHistory(Boolean isHistory) {
        this.isHistory = isHistory;
    }
}
