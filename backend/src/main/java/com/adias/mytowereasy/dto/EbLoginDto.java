package com.adias.mytowereasy.dto;

public class EbLoginDto {
    private String username;
    private String password;
    private String emailCt;
    private String passwordCt;
    private Boolean ct;
    private String activeToken;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmailCt() {
        return emailCt;
    }

    public void setEmailCt(String emailCt) {
        this.emailCt = emailCt;
    }

    public String getPasswordCt() {
        return passwordCt;
    }

    public void setPasswordCt(String passwordCt) {
        this.passwordCt = passwordCt;
    }

    public Boolean getCt() {
        return ct;
    }

    public void setCt(Boolean ct) {
        this.ct = ct;
    }

    public String getActiveToken() {
        return activeToken;
    }

    public void setActiveToken(String activeToken) {
        this.activeToken = activeToken;
    }

    public EbLoginDto(
        String username,
        String password,
        String emailCt,
        String passwordCt,
        Boolean ct,
        String activeToken) {
        super();
        this.username = username;
        this.password = password;
        this.emailCt = emailCt;
        this.passwordCt = passwordCt;
        this.ct = ct;
        this.activeToken = activeToken;
    }

    public EbLoginDto() {
        super();
    }
}
