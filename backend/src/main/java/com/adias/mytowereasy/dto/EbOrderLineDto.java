/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dto;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EcCountry;


public class EbOrderLineDto {
    private Long ebDelOrderLineNum;

    private String refArticle;

    private String articleName;

    private String originCountry;

    private Long idOrderLine;

    private String canceledReason;

    private String dateOfAvailability;

    private EcCountry xEcCountryOrigin;

    private EbTypeUnitDto xEbtypeOfUnit;

    private Long ebTypeUnitNum;

    private Integer palletHeight;

    private Boolean isSelected;

    private String partNumber;
    private String serialNumber;
    private Date dateRequest;
    private Integer priority;
    private String comment;
    private String specification;

    private Double weight;

    private Double width;

    private String hsCode;

    private Integer height;

    private String itemName;

    private String itemNumber;

    private Double length;

    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    private String customFields;

    private List<EbCategorie> listCategories;

    private String listLabels;

    private Long quantityOrdered; // Quantity ordered (total)
    private Long quantityPlannable; // Not already planned
    private Long quantityPlanned; // Sum of all quantity already planned (all
                                  // status)

    private Long quantityPending; // Quantities at status pending
    private Long quantityConfirmed; // Quantities at status confirmed
    private Long quantityShipped; // Quantities at status shipped (passed PSL
                                  // start)
    private Long quantityDelivered; // Quantities at status delivery (passed PSL
                                    // end)

    private Long quantityCanceled; // Quantities canceled (used for analytics
                                   // only)

    private Boolean serialized;

    private Double price;

    private Integer currency;

    private String eccn;

    public String getRefArticle() {
        return refArticle;
    }

    public void setRefArticle(String refArticle) {
        this.refArticle = refArticle;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String productName) {
        this.articleName = productName;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public EbTypeUnitDto getxEbtypeOfUnit() {
        return xEbtypeOfUnit;
    }

    public Long getEbDelOrderLineNum() {
        return ebDelOrderLineNum;
    }

    public void setEbDelOrderLineNum(Long ebDelOrderLineNum) {
        this.ebDelOrderLineNum = ebDelOrderLineNum;
    }

    public String getDateOfAvailability() {
        return dateOfAvailability;
    }

    public void setDateOfAvailability(String dateOfAvailability) {
        this.dateOfAvailability = dateOfAvailability;
    }

    public EcCountry getxEcCountryOrigin() {
        return xEcCountryOrigin;
    }

    public void setxEcCountryOrigin(EcCountry xEcCountryOrigin) {
        this.xEcCountryOrigin = xEcCountryOrigin;
    }

    public Integer getPalletHeight() {
        return palletHeight;
    }

    public void setxEbtypeOfUnit(EbTypeUnitDto xEbtypeOfUnit) {
        this.xEbtypeOfUnit = xEbtypeOfUnit;
    }

    public Boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }

    public void setPalletHeight(Integer palletHeight) {
        this.palletHeight = palletHeight;
    }

    public Long getIdOrderLine() {
        return idOrderLine;
    }

    public void setIdOrderLine(Long idOrderLine) {
        this.idOrderLine = idOrderLine;
    }

    public String getCanceledReason() {
        return canceledReason;
    }

    public void setCanceledReason(String canceledReason) {
        this.canceledReason = canceledReason;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Date getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(Date dateRequest) {
        this.dateRequest = dateRequest;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;
    }

    public List<EbCategorie> getListCategories() {
        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {
        this.listCategories = listCategories;
    }

    public String getListLabels() {
        return listLabels;
    }

    public void setListLabels(String listLabels) {
        this.listLabels = listLabels;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public Long getEbTypeUnitNum() {
        return ebTypeUnitNum;
    }

    public void setEbTypeUnitNum(Long ebTypeUnitNum) {
        this.ebTypeUnitNum = ebTypeUnitNum;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Long getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(Long quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public Long getQuantityPlannable() {
        return quantityPlannable;
    }

    public void setQuantityPlannable(Long quantityPlannable) {
        this.quantityPlannable = quantityPlannable;
    }

    public Long getQuantityPlanned() {
        return quantityPlanned;
    }

    public void setQuantityPlanned(Long quantityPlanned) {
        this.quantityPlanned = quantityPlanned;
    }

    public Long getQuantityPending() {
        return quantityPending;
    }

    public void setQuantityPending(Long quantityPending) {
        this.quantityPending = quantityPending;
    }

    public Long getQuantityConfirmed() {
        return quantityConfirmed;
    }

    public void setQuantityConfirmed(Long quantityConfirmed) {
        this.quantityConfirmed = quantityConfirmed;
    }

    public Long getQuantityShipped() {
        return quantityShipped;
    }

    public void setQuantityShipped(Long quantityShipped) {
        this.quantityShipped = quantityShipped;
    }

    public Long getQuantityDelivered() {
        return quantityDelivered;
    }

    public void setQuantityDelivered(Long quantityDelivered) {
        this.quantityDelivered = quantityDelivered;
    }

    public Long getQuantityCanceled() {
        return quantityCanceled;
    }

    public void setQuantityCanceled(Long quantityCanceled) {
        this.quantityCanceled = quantityCanceled;
    }

    public Boolean getSerialized() {
        return serialized;
    }

    public void setSerialized(Boolean serialized) {
        this.serialized = serialized;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getCurrency() {
        return currency;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }

    public String getEccn() {
        return eccn;
    }

    public void setEccn(String eccn) {
        this.eccn = eccn;
    }
}
