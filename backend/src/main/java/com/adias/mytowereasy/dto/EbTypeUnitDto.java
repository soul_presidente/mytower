package com.adias.mytowereasy.dto;

public class EbTypeUnitDto {
    private Long ebTypeUnitNum;

    private String libelle;

    private Double weight;

    private Double volume;

    private Double length;

    private Double width;

    private Double height;

    private String gerbable;

    private Boolean gerbableValue;

    private String containerLabel;

    private Integer xEbTypeContainer;

    private String code;

    private Integer codeTypeUnit;

    private boolean activated;

    public boolean getActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

    public Long getEbTypeUnitNum() {
        return ebTypeUnitNum;
    }

    public void setEbTypeUnitNum(Long ebTypeUnitNum) {
        this.ebTypeUnitNum = ebTypeUnitNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getGerbable() {
        return gerbable;
    }

    public void setGerbable(String gerbable) {
        this.gerbable = gerbable;
    }

    public Boolean getGerbableValue() {
        return gerbableValue;
    }

    public void setGerbableValue(Boolean gerbableValue) {
        this.gerbableValue = gerbableValue;
    }

    public String getContainerLabel() {
        return containerLabel;
    }

    public void setContainerLabel(String containerLabel) {
        this.containerLabel = containerLabel;
    }

    public Integer getxEbTypeContainer() {
        return xEbTypeContainer;
    }

    public void setxEbTypeContainer(Integer xEbTypeContainer) {
        this.xEbTypeContainer = xEbTypeContainer;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCodeTypeUnit() {
        return codeTypeUnit;
    }

    public void setCodeTypeUnit(Integer codeTypeUnit) {
        this.codeTypeUnit = codeTypeUnit;
    }
}
