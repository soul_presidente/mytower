package com.adias.mytowereasy.dto;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;

import com.adias.mytowereasy.model.EcCountry;


public interface EbTTPslAppAndEventsProjection {
    public Integer getEbTtPslAppNum();

    public Date getDateCreation();

    public String getCommentaire();

    @Value("#{(target.dateActuelle!=null)?target.dateActuelle:(target.dateEstimee!=null)?target.dateEstimee:(target.dateNegotiation!=null)?target.dateNegotiation:null}")
    public Date getDate();

    @Value("#{(target.xEbTrackTrace?.codeAlphaPslCourant != null && target.companyPsl.codeAlpha != null && target.xEbTrackTrace?.codeAlphaPslCourant?.equals(target.companyPsl.codeAlpha))}")
    public Boolean isActual();

    @Value("#{(target.dateActuelle!=null)?'ACTUAL_DATE':(target.dateEstimee!=null)?'ESTIMATED_DATE':(target.dateNegotiation!=null)?'NEGOTIATED_DATE':''}")
    public String getNature();

    public Boolean getValidateByChamp();

    @Value("#{target?.companyPsl?.libelle}")
    public String getNom();

    @Value("#{target?.companyPsl?.order}")
    public Integer getOrder();

    public Boolean getValidated();

    @Value("#{target?.companyPsl?.importance}")
    public Integer getImportance();

    @Value("#{target?.companyPsl?.ebTtCompanyPslNum}")
    public Integer getEbTtCompanyPslNum();

    @Value("#{target?.companyPsl?.codeAlpha}")
    public String getCodeAlpha();

    public EbTtCompanyPslProjection getCompanyPsl();

    public List<EbTteventProjection> getListEvent();

    public String getCity();

    public EcCountry getxEcCountry();
}
