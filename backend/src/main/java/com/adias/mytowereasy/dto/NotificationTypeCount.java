package com.adias.mytowereasy.dto;

public class NotificationTypeCount {
    private Integer nbrAlerts;

    private Integer nbrNotifys;

    private Integer nbrMsgs;

    public Integer getNbrAlerts() {
        return nbrAlerts;
    }

    public void setNbrAlerts(Integer nbrAlerts) {
        this.nbrAlerts = nbrAlerts;
    }

    public Integer getNbrNotifys() {
        return nbrNotifys;
    }

    public void setNbrNotifys(Integer nbrNotifys) {
        this.nbrNotifys = nbrNotifys;
    }

    public Integer getNbrMsgs() {
        return nbrMsgs;
    }

    public void setNbrMsgs(Integer nbrMsgs) {
        this.nbrMsgs = nbrMsgs;
    }
}
