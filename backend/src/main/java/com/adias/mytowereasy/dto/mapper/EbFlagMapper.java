package com.adias.mytowereasy.dto.mapper;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.model.EbFlag;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EbFlagMapper {

    EbFlag ebFlagDtoToEbFlag(EbFlagDTO FlagDto);

    List<EbFlag> ebFlagDtoToEbFlags(List<EbFlagDTO> listFlagDto);
}
