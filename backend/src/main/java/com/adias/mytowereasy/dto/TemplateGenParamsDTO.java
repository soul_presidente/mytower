package com.adias.mytowereasy.dto;

import java.util.ArrayList;
import java.util.List;


public class TemplateGenParamsDTO {
    private Integer selectedTemplate;
    private Integer selectedFormat;
    private Boolean downloadChecked;
    private Boolean attachChecked;
    private Boolean emailChecked;
    private Integer selectedTypeDoc;

    private Integer templateModelNum;
    private List<String> templateModelParams;
    private String generatedFileName;
    private List<String> emailDest;

    public TemplateGenParamsDTO() {
        this.setTemplateModelParams(new ArrayList<>());
    }

    public Integer getSelectedTemplate() {
        return selectedTemplate;
    }

    public void setSelectedTemplate(Integer selectedTemplate) {
        this.selectedTemplate = selectedTemplate;
    }

    public Integer getSelectedFormat() {
        return selectedFormat;
    }

    public void setSelectedFormat(Integer selectedFormat) {
        this.selectedFormat = selectedFormat;
    }

    public Boolean getDownloadChecked() {
        return downloadChecked;
    }

    public void setDownloadChecked(Boolean downloadChecked) {
        this.downloadChecked = downloadChecked;
    }

    public Boolean getAttachChecked() {
        return attachChecked;
    }

    public void setAttachChecked(Boolean attachChecked) {
        this.attachChecked = attachChecked;
    }

    public Boolean getEmailChecked() {
        return emailChecked;
    }

    public void setEmailChecked(Boolean emailChecked) {
        this.emailChecked = emailChecked;
    }

    public Integer getSelectedTypeDoc() {
        return selectedTypeDoc;
    }

    public void setSelectedTypeDoc(Integer selectedTypeDoc) {
        this.selectedTypeDoc = selectedTypeDoc;
    }

    public Integer getTemplateModelNum() {
        return templateModelNum;
    }

    public void setTemplateModelNum(Integer templateModelNum) {
        this.templateModelNum = templateModelNum;
    }

    public List<String> getTemplateModelParams() {
        return templateModelParams;
    }

    public void setTemplateModelParams(List<String> templateModelParams) {
        this.templateModelParams = templateModelParams;
    }

    public String getGeneratedFileName() {
        return generatedFileName;
    }

    public void setGeneratedFileName(String generatedFileName) {
        this.generatedFileName = generatedFileName;
    }

    public List<String> getEmailDest() {
        return emailDest;
    }

    public void setEmailDest(List<String> emailDest) {
        this.emailDest = emailDest;
    }
}
