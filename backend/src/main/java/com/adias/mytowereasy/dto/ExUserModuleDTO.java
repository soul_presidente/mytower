package com.adias.mytowereasy.dto;

public class ExUserModuleDTO {
    private Integer ecModuleNum;
    private Integer ebUserNum;
    private Integer accessRight;

    public ExUserModuleDTO() {
    }

    public Integer getEcModuleNum() {
        return ecModuleNum;
    }

    public void setEcModuleNum(Integer ecModuleNum) {
        this.ecModuleNum = ecModuleNum;
    }

    public Integer getEbUserNum() {
        return ebUserNum;
    }

    public void setEbUserNum(Integer ebUserNum) {
        this.ebUserNum = ebUserNum;
    }

    public Integer getAccessRight() {
        return accessRight;
    }

    public void setAccessRight(Integer accessRight) {
        this.accessRight = accessRight;
    }
}
