package com.adias.mytowereasy.dto;

import java.util.Date;

import com.adias.mytowereasy.model.EbTypeRequest;


public class EbTypeRequestDTO {
    private Integer ebTypeRequestNum;

    private String reference;

    private String libelle;

    private String refLibelle; // concatenation entre reference et libelle

    private Date dateAjout;

    private Date dateMaj;

    private Integer delaiChronoPricing; // delai chrono en nombre dheure

    private Integer sensChronoPricing;

    private Boolean blocageResponsePricing;

    private Boolean activated; // permettant d'identifier si typeRequest est
                               // desactivés ou actives

    private Integer xEbUserNum;

    private Integer xEbCompagnieNum;
    private String xEbCompagnieNom;

    public EbTypeRequestDTO(EbTypeRequest entity) {
        super();
        this.ebTypeRequestNum = entity.getEbTypeRequestNum();
        this.reference = entity.getReference();
        this.libelle = entity.getLibelle();
        this.delaiChronoPricing = entity.getDelaiChronoPricing();
        this.dateAjout = entity.getDateCreationSys();
        this.dateMaj = entity.getDateMajSys();
        this.activated = entity.getActivated();
        this.sensChronoPricing = entity.getSensChronoPricing();
        this.blocageResponsePricing = entity.getBlocageResponsePricing();
        this.xEbUserNum = entity.getxEbUser() != null ? entity.getxEbUser().getEbUserNum() : null;
        this.xEbCompagnieNum = entity.getxEbCompagnie() != null ? entity.getxEbCompagnie().getEbCompagnieNum() : null;
        this.xEbCompagnieNom = entity.getxEbCompagnie() != null ? entity.getxEbCompagnie().getNom() : "";

        this.refLibelle = entity.getReference() + (entity.getLibelle() != null ? "-" + entity.getLibelle() : "");
    }

    public EbTypeRequestDTO() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Integer getEbTypeRequestNum() {
        return ebTypeRequestNum;
    }

    public void setEbTypeRequestNum(Integer ebTypeRequestNum) {
        this.ebTypeRequestNum = ebTypeRequestNum;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getDelaiChronoPricing() {
        return delaiChronoPricing;
    }

    public void setDelaiChronoPricing(Integer delaiChronoPricing) {
        this.delaiChronoPricing = delaiChronoPricing;
    }

    public Integer getSensChronoPricing() {
        return sensChronoPricing;
    }

    public void setSensChronoPricing(Integer sensChronoPricing) {
        this.sensChronoPricing = sensChronoPricing;
    }

    public Boolean getBlocageResponsePricing() {
        return blocageResponsePricing;
    }

    public void setBlocageResponsePricing(Boolean blocageResponsePricing) {
        this.blocageResponsePricing = blocageResponsePricing;
    }

    public Integer getxEbUserNum() {
        return xEbUserNum;
    }

    public void setxEbUserNum(Integer xEbUserNum) {
        this.xEbUserNum = xEbUserNum;
    }

    public Integer getxEbCompagnieNum() {
        return xEbCompagnieNum;
    }

    public void setxEbCompagnieNum(Integer xEbCompagnieNum) {
        this.xEbCompagnieNum = xEbCompagnieNum;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public String getRefLibelle() {
        return refLibelle;
    }

    public void setRefLibelle(String refLibelle) {
        refLibelle = refLibelle;
    }

    public String getxEbCompagnieNom() {
        return xEbCompagnieNom;
    }

    public void setxEbCompagnieNom(String xEbCompagnieNom) {
        this.xEbCompagnieNom = xEbCompagnieNom;
    }
}
