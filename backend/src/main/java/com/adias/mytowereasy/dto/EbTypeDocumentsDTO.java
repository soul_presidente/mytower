package com.adias.mytowereasy.dto;

import java.util.List;

import com.adias.mytowereasy.model.EbTypeDocuments;


public class EbTypeDocumentsDTO {
    private Integer ebTypeDocumentsNum;

    private String nom;

    private String code;

    private Integer ordre;

    private List<Integer> module;

    private String listModuleStr;

    private Integer xEbCompagnie;

    private Boolean activated;

    private Boolean isexpectedDoc;

    private Integer nbrDoc;

    public EbTypeDocumentsDTO() {
    }

    public EbTypeDocumentsDTO(EbTypeDocuments entity) {
        super();
        this.ebTypeDocumentsNum = entity.getEbTypeDocumentsNum();
        this.nom = entity.getNom();
        this.code = entity.getCode();
        this.ordre = entity.getOrdre();
        this.module = entity.getModule();
        this.listModuleStr = entity.getListModuleStr();
        this.xEbCompagnie = entity.getxEbCompagnie();
        this.activated = entity.getActivated();
        this.isexpectedDoc = entity.getIsexpectedDoc();
        this.nbrDoc = entity.getNbrDoc();
    }

    public Integer getEbTypeDocumentsNum() {
        return ebTypeDocumentsNum;
    }

    public void setEbTypeDocumentsNum(Integer ebTypeDocumentsNum) {
        this.ebTypeDocumentsNum = ebTypeDocumentsNum;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getOrdre() {
        return ordre;
    }

    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    public List<Integer> getModule() {
        return module;
    }

    public void setModule(List<Integer> module) {
        this.module = module;
    }

    public String getListModuleStr() {
        return listModuleStr;
    }

    public void setListModuleStr(String listModuleStr) {
        this.listModuleStr = listModuleStr;
    }

    public Integer getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(Integer xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Boolean getIsexpectedDoc() {
        return isexpectedDoc;
    }

    public void setIsexpectedDoc(Boolean isexpectedDoc) {
        this.isexpectedDoc = isexpectedDoc;
    }

    public Integer getNbrDoc() {
        return nbrDoc;
    }

    public void setNbrDoc(Integer nbrDoc) {
        this.nbrDoc = nbrDoc;
    }
}
