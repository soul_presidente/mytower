package com.adias.mytowereasy.dto;

import java.util.Date;
import java.util.List;


/**
 * Custom Declaration from an existing demande
 */
public class CustomDeclarationPricingDTO {
    private Integer ebDemandeNum;
    private String declarationNumber;
    private Date declarationDate;
    private List<Integer> unitsNum;
    private List<Integer> docsNum;

    public Integer getEbDemandeNum() {
        return ebDemandeNum;
    }

    public void setEbDemandeNum(Integer ebDemandeNum) {
        this.ebDemandeNum = ebDemandeNum;
    }

    public String getDeclarationNumber() {
        return declarationNumber;
    }

    public void setDeclarationNumber(String declarationNumber) {
        this.declarationNumber = declarationNumber;
    }

    public Date getDeclarationDate() {
        return declarationDate;
    }

    public void setDeclarationDate(Date declarationDate) {
        this.declarationDate = declarationDate;
    }

    public List<Integer> getUnitsNum() {
        return unitsNum;
    }

    public void setUnitsNum(List<Integer> unitsNum) {
        this.unitsNum = unitsNum;
    }

    public List<Integer> getDocsNum() {
        return docsNum;
    }

    public void setDocsNum(List<Integer> docsNum) {
        this.docsNum = docsNum;
    }
}
