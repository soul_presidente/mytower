package com.adias.mytowereasy.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.adias.mytowereasy.model.EbTemplateDocuments;


public class EbTemplateDocumentsDTO {
    private Integer ebTemplateDocumentsNum;

    private String libelle;

    private String reference;

    private Date dateAjout;

    private Date dateMaj;

    private List<EbTypeDocumentsDTO> typesDocument;

    private List<Integer> listEbTypeDocumentsNum;

    private String cheminDocument;

    private String originFileName;

    private Integer xEbUserNum;

    private Integer xEbCompagnieNum;

    private List<Integer> module;

    private boolean isGlobal;

    public EbTemplateDocumentsDTO(EbTemplateDocuments entity) {
        super();
        this.ebTemplateDocumentsNum = entity.getEbTemplateDocumentsNum();
        this.libelle = entity.getLibelle();
        this.reference = entity.getReference();
        this.dateAjout = entity.getDateAjout();
        this.dateMaj = entity.getDateMaj();

        if (entity.getTypesDocument() != null) {
            this.listEbTypeDocumentsNum = entity
                .getTypesDocument().stream().map(td -> td.getEbTypeDocumentsNum()).collect(Collectors.toList());

            this.typesDocument = new ArrayList<>();
            entity.getTypesDocument().forEach(td -> {
                this.typesDocument.add(new EbTypeDocumentsDTO(td));
            });
        }

        this.cheminDocument = entity.getCheminDocument();
        this.originFileName = entity.getOriginFileName();
        this.xEbUserNum = entity.getxEbUser() != null ? entity.getxEbUser().getEbUserNum() : null;
        this.xEbCompagnieNum = entity.getxEbCompagnie() != null ? entity.getxEbCompagnie().getEbCompagnieNum() : null;
        this.module = entity.getModule();
        this.isGlobal = entity.getIsGlobal();
    }

    public EbTemplateDocumentsDTO() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getOriginFileName() {
        return originFileName;
    }

    public void setOriginFileName(String originFileName) {
        this.originFileName = originFileName;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Integer getEbTemplateDocumentsNum() {
        return ebTemplateDocumentsNum;
    }

    public void setEbTemplateDocumentsNum(Integer ebTemplateDocumentsNum) {
        this.ebTemplateDocumentsNum = ebTemplateDocumentsNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public List<Integer> getListEbTypeDocumentsNum() {
        return listEbTypeDocumentsNum;
    }

    public void setListEbTypeDocumentsNum(List<Integer> listEbTypeDocumentsNum) {
        this.listEbTypeDocumentsNum = listEbTypeDocumentsNum;
    }

    public String getCheminDocument() {
        return cheminDocument;
    }

    public void setCheminDocument(String cheminDocument) {
        this.cheminDocument = cheminDocument;
    }

    public Integer getxEbUserNum() {
        return xEbUserNum;
    }

    public void setxEbUserNum(Integer xEbUserNum) {
        this.xEbUserNum = xEbUserNum;
    }

    public Integer getxEbCompagnieNum() {
        return xEbCompagnieNum;
    }

    public void setxEbCompagnieNum(Integer xEbCompagnieNum) {
        this.xEbCompagnieNum = xEbCompagnieNum;
    }

    public List<EbTypeDocumentsDTO> getTypesDocument() {
        return typesDocument;
    }

    public void setTypesDocument(List<EbTypeDocumentsDTO> typesDocument) {
        this.typesDocument = typesDocument;
    }

    public List<Integer> getModule() {
        return module;
    }

    public void setModule(List<Integer> module) {
        this.module = module;
    }

    public boolean isGlobal() {
        return isGlobal;
    }

    public void setGlobal(boolean isGlobal) {
        this.isGlobal = isGlobal;
    }
}
