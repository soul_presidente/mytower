/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dto;

import com.adias.mytowereasy.model.EcCountry;


public class EbPartyDto {
    private Integer ebPartyNum;

    private String company;

    private String adresse;

    private String country;

    private String city;

    private String zipCode;

    private String reference;

    private EcCountry xEcCountry;

    private String openingHours;

    private String airport;

    private String email;

    private String phone;

    private String commentaire;
    private Integer zoneNum;
    private String zoneRef;
    private String zoneDesignation;

    public String getCompany() {
        return company;
    }

    public void setEbPartyNum(Integer ebPartyNum) {
        this.ebPartyNum = ebPartyNum;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getEbPartyNum() {
        return ebPartyNum;
    }

    public EcCountry getxEcCountry() {
        return xEcCountry;
    }

    public void setxEcCountry(EcCountry xEcCountry) {
        this.xEcCountry = xEcCountry;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public String getAirport() {
        return airport;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public Integer getZoneNum() {
        return zoneNum;
    }

    public String getZoneRef() {
        return zoneRef;
    }

    public String getZoneDesignation() {
        return zoneDesignation;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public void setZoneNum(Integer zoneNum) {
        this.zoneNum = zoneNum;
    }

    public void setZoneRef(String zoneRef) {
        this.zoneRef = zoneRef;
    }

    public void setZoneDesignation(String zoneDesignation) {
        this.zoneDesignation = zoneDesignation;
    }
}
