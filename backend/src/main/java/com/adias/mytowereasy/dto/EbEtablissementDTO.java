package com.adias.mytowereasy.dto;

public class EbEtablissementDTO {
    private Integer ebEtablissementNum;
    private String nom;
    private String siret;
    private String logo;
    private String typeEtablissement;
    private Boolean canShowPrice;

    public Integer getEbEtablissementNum() {
        return ebEtablissementNum;
    }

    public String getNom() {
        return nom;
    }

    public String getSiret() {
        return siret;
    }

    public String getLogo() {
        return logo;
    }

    public String getTypeEtablissement() {
        return typeEtablissement;
    }

    public Boolean getCanShowPrice() {
        return canShowPrice;
    }

    public void setEbEtablissementNum(Integer ebEtablissementNum) {
        this.ebEtablissementNum = ebEtablissementNum;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public void setTypeEtablissement(String typeEtablissement) {
        this.typeEtablissement = typeEtablissement;
    }

    public void setCanShowPrice(Boolean canShowPrice) {
        this.canShowPrice = canShowPrice;
    }
}
