package com.adias.mytowereasy.dto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.adias.mytowereasy.model.EbInvoice;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.FAEnumeration;


public class SharingInformationMailParams {
    public BigDecimal cost;
    public String currency;
    public String invoiceNum;
    public String invoiceMonthDate;
    public String status;
    public String refTranport;
    public String customerRef;
    public String firstNameSender;
    public String lastNameSender;
    public String etablissementSenderName;
    public String companyChargeurName;
    public BigDecimal action;
    public Date invoiceDate;
    public Integer invoiceId;
    public Integer transportNum;
    EbUser connectedUser;

    public SharingInformationMailParams(EbInvoice invoice, BigDecimal action) {
        super();
        this.invoiceId = invoice.getEbInvoiceNum();

        this.invoiceNum = invoice.getApplyChargeurIvoiceNumber();
        this.cost = invoice.getApplyChargeurIvoicePrice();
        this.currency = invoice.getCurrencyLabel();

        this.refTranport = invoice.getRefDemande();

        if (invoice.getxEbDemande() != null) {
            this.customerRef = invoice.getxEbDemande().getCustomerReference() != null ?
                invoice.getxEbDemande().getCustomerReference() :
                " ";
        }

        if (connectedUser != null) {
            this.firstNameSender = connectedUser.getNom();
            this.lastNameSender = connectedUser.getPrenom();

            Integer etablissementSender = connectedUser.getEbEtablissement().getEbEtablissementNum();
            this.etablissementSenderName = connectedUser.getEbEtablissement().getNom();

            this.companyChargeurName = connectedUser.getEbCompagnie().getNom();

            if (invoice.getEbInvoiceNum() != null) {
                this.transportNum = invoice.getEbInvoiceNum();
            }

            if (connectedUser.getRole() == Enumeration.Role.ROLE_CHARGEUR.getCode()) {

                if (invoice.getStatutCarrier() == null && invoice.getStatutChargeur() == null) {
                    this.status = FAEnumeration.InvoiceStatus.getLibelleByCode(invoice.getStatut());
                }
                else {
                    this.status = FAEnumeration.InvoiceStatus.getLibelleByCode(invoice.getStatutChargeur());
                }

            }
            else if (connectedUser.getRole() == Enumeration.Role.ROLE_PRESTATAIRE.getCode()) {

                if (invoice.getStatutCarrier() == null && invoice.getStatutChargeur() == null) {
                    this.status = FAEnumeration.InvoiceStatus.getLibelleByCode(invoice.getStatut());
                }
                else {
                    this.status = FAEnumeration.InvoiceStatus.getLibelleByCode(invoice.getStatutCarrier());
                }

            }
            else {
                this.status = FAEnumeration.InvoiceStatus.getLibelleByCode(invoice.getStatut());
            }

            if (connectedUser.getRole() == Enumeration.Role.ROLE_CHARGEUR.getCode()) {

                if (invoice.getInvoiceDateChargeur() == null && invoice.getInvoiceDateChargeur() == null) {
                    this.invoiceDate = invoice.getDateModification();
                }
                else {
                    this.invoiceDate = invoice.getInvoiceDateChargeur();
                }

            }
            else if (connectedUser.getRole() == Enumeration.Role.ROLE_PRESTATAIRE.getCode()) {

                if (invoice.getInvoiceDateChargeur() == null && invoice.getInvoiceDateCarrier() == null) {
                    invoiceDate = invoice.getDateModification();
                }
                else {
                    invoiceDate = invoice.getInvoiceDateCarrier();
                }

            }

            if (invoiceDate != null) {
                SimpleDateFormat simpleDateFormatMonth = new SimpleDateFormat("MMMM");

                SimpleDateFormat simpleDateFormatYear = new SimpleDateFormat("YYYY");

                invoiceMonthDate = simpleDateFormatMonth.format(invoiceDate) + " "
                    + simpleDateFormatYear.format(invoiceDate);
            }

        }

    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    public String getInvoiceMonthDate() {
        return invoiceMonthDate;
    }

    public void setInvoiceMonthDate(String invoiceMonthDate) {
        this.invoiceMonthDate = invoiceMonthDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRefTranport() {
        return refTranport;
    }

    public void setRefTranport(String refTranport) {
        this.refTranport = refTranport;
    }

    public String getCustomerRef() {
        return customerRef;
    }

    public void setCustomerRef(String customerRef) {
        this.customerRef = customerRef;
    }

    public String getFirstNameSender() {
        return firstNameSender;
    }

    public void setFirstNameSender(String firstNameSender) {
        this.firstNameSender = firstNameSender;
    }

    public String getLastNameSender() {
        return lastNameSender;
    }

    public void setLastNameSender(String lastNameSender) {
        this.lastNameSender = lastNameSender;
    }

    public String getEtablissementSenderName() {
        return etablissementSenderName;
    }

    public void setEtablissementSenderName(String etablissementSenderName) {
        this.etablissementSenderName = etablissementSenderName;
    }

    public String getCompanyChargeurName() {
        return companyChargeurName;
    }

    public void setCompanyChargeurName(String companyChargeurName) {
        this.companyChargeurName = companyChargeurName;
    }

    public BigDecimal getAction() {
        return action;
    }

    public void setAction(BigDecimal action) {
        this.action = action;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Integer getTransportNum() {
        return transportNum;
    }

    public void setTransportNum(Integer transportNum) {
        this.transportNum = transportNum;
    }

    public EbUser getConnectedUser() {
        return connectedUser;
    }

    public void setConnectedUser(EbUser connectedUser) {
        this.connectedUser = connectedUser;
    }
}
