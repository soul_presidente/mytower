package com.adias.mytowereasy.dto;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.adias.mytowereasy.model.EbBoutonAction;


public class EbBoutonActionDTO {
    private Integer ebBoutonActionNum;
    private String type;
    private String module;
    private boolean active;
    private Long ordre;
    private String color;
    private Integer icon;
    private String libelle;
    private String action;

    public EbBoutonActionDTO() {
    }

    public EbBoutonActionDTO(EbBoutonAction ebBoutonAction) {
        this.color = ebBoutonAction.getColor();
        this.ebBoutonActionNum = ebBoutonAction.getEbBoutonActionNum();
        this.icon = ebBoutonAction.getIcon();
        this.type = ebBoutonAction.getType();
        this.module = ebBoutonAction.getModule();
        this.active = ebBoutonAction.isActive();
        this.ordre = ebBoutonAction.getOrdre();
        this.libelle = ebBoutonAction.getLibelle();
        this.action = ebBoutonAction.getAction();
    }

    public Integer getEbBoutonActionNum() {
        return ebBoutonActionNum;
    }

    public String getType() {
        return type;
    }

    public String getModule() {
        return module;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public boolean isActive() {
        return active;
    }

    public Long getOrdre() {
        return ordre;
    }

    public String getColor() {
        return color;
    }

    public Integer getIcon() {
        return icon;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setEbBoutonActionNum(Integer ebBoutonActionNum) {
        this.ebBoutonActionNum = ebBoutonActionNum;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setOrdre(Long ordre) {
        this.ordre = ordre;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public static List<EbBoutonActionDTO> sortByLibelle(List<EbBoutonActionDTO> listBoutonActionDTO) {
        Collections.sort(listBoutonActionDTO, new Comparator<EbBoutonActionDTO>() {
            @Override
            public int compare(EbBoutonActionDTO f1, EbBoutonActionDTO f2) {
                if (f1.getLibelle() == null) return -1;
                if (f2.getLibelle() == null) return 1;
                return f1.getLibelle().compareTo(f2.getLibelle());
            }
        });
        return listBoutonActionDTO;
    }
}
