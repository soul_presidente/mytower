package com.adias.mytowereasy.dto;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * <b>Indicate that a field is a representation of a field retrieved from the
 * database</b>
 * <br/>
 * </br>
 * Permet d'associer un champ DTO à un champ en BDD<br/>
 * Exemple : Lors du tri, indique sur quel champ la requête sql doit faire le
 * order by.<br/>
 * Le fait de faire une annotation permet de garder l'isolation des couches et
 * donc coté frontend de
 * se soucier uniquement du schéma au format DTO et de laisser la relation avec
 * le schéma entity sur
 * le backend. Le frontend ne connait donc que le DTO
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface EntityProjectionField {
    /**
     * Field in JPQL format
     */
    String value();
}
