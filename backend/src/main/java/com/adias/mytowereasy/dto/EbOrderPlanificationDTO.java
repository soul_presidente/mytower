package com.adias.mytowereasy.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class EbOrderPlanificationDTO {
    private Long ebDelOrderNum;
    private List<EbOrderLinePlanificationDTO> listToPlan;
    private Long ebDelLivraisonNum;
    private Date dateOfGoodsAvailability;

    public EbOrderPlanificationDTO() {
        this.listToPlan = new ArrayList<>();
    }

    public Long getEbDelOrderNum() {
        return ebDelOrderNum;
    }

    public void setEbDelOrderNum(Long ebDelOrderNum) {
        this.ebDelOrderNum = ebDelOrderNum;
    }

    public List<EbOrderLinePlanificationDTO> getListToPlan() {
        return listToPlan;
    }

    public void setListToPlan(List<EbOrderLinePlanificationDTO> listToPlan) {
        this.listToPlan = listToPlan;
    }

    public Long getEbDelLivraisonNum() {
        return ebDelLivraisonNum;
    }

    public void setEbDelLivraisonNum(Long ebDelLivraisonNum) {
        this.ebDelLivraisonNum = ebDelLivraisonNum;
    }

    public Date getDateOfGoodsAvailability() {
        return dateOfGoodsAvailability;
    }

    public void setDateOfGoodsAvailability(Date dateOfGoodsAvailability) {
        this.dateOfGoodsAvailability = dateOfGoodsAvailability;
    }
}
