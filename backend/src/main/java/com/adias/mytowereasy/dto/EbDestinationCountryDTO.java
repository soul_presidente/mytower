package com.adias.mytowereasy.dto;

import com.adias.mytowereasy.model.EbDestinationCountry;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EcCountry;


public class EbDestinationCountryDTO {
    private Integer ebDestinationCountryNum;
    private EcCountry country;
    private EbEtablissement etablissement;

    public EbDestinationCountryDTO(EbDestinationCountry destinationCountry) {
        this.ebDestinationCountryNum = destinationCountry.getEbDestinationCountryNum();
        this.country = destinationCountry.getCountry();
        this.etablissement = new EbEtablissement(destinationCountry.getEtablissement().getEbEtablissementNum());
        this.etablissement.setCode(destinationCountry.getEtablissement().getCode());
        this.etablissement.setNom(destinationCountry.getEtablissement().getNom());
    }

    public Integer getEbDestinationCountryNum() {
        return ebDestinationCountryNum;
    }

    public void setEbDestinationCountryNum(Integer ebDestinationCountryNum) {
        this.ebDestinationCountryNum = ebDestinationCountryNum;
    }

    public EcCountry getCountry() {
        return country;
    }

    public void setCountry(EcCountry country) {
        this.country = country;
    }

    public EbEtablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(EbEtablissement etablissement) {
        this.etablissement = etablissement;
    }
}
