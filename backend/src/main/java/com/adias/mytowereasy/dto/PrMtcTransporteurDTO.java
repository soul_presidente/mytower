package com.adias.mytowereasy.dto;

public class PrMtcTransporteurDTO {
    private Integer prMtcTransporteurNum;

    private String prCodeMTC;

    private String prCodeMTG;

    private String codeConfigurationEDI;

    public Integer getPrMtcTransporteurNum() {
        return prMtcTransporteurNum;
    }

    public void setPrMtcTransporteurNum(Integer prMtcTransporteurNum) {
        this.prMtcTransporteurNum = prMtcTransporteurNum;
    }

    public String getPrCodeMTC() {
        return prCodeMTC;
    }

    public void setPrCodeMTC(String prCodeMTC) {
        this.prCodeMTC = prCodeMTC;
    }

    public String getPrCodeMTG() {
        return prCodeMTG;
    }

    public void setPrCodeMTG(String prCodeMTG) {
        this.prCodeMTG = prCodeMTG;
    }

    public String getCodeConfigurationEDI() {
        return codeConfigurationEDI;
    }

    public void setCodeConfigurationEDI(String codeConfigurationEDI) {
        this.codeConfigurationEDI = codeConfigurationEDI;
    }
}
