package com.adias.mytowereasy.dto;

import java.util.Date;


public class ProposionRdvDto implements Comparable<ProposionRdvDto> {
    private Date date;
    private int role;
    private int statut;
    private Date dateCreation;
    private String nomUser;

    // getters && setters

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getNomUser() {
        return nomUser;
    }

    public void setNomUser(String nomUser) {
        this.nomUser = nomUser;
    }

    @Override
    public int compareTo(ProposionRdvDto o) {

        if (this.dateCreation.after(o.getDateCreation())) {
            return 1;
        }
        else if (this.dateCreation.before(o.getDateCreation())) {
            return -1;
        }
        else {
        }

        return 0;
    }
}
