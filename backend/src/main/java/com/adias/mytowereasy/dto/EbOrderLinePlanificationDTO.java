package com.adias.mytowereasy.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties({
    "orderLine"
})
public class EbOrderLinePlanificationDTO {
    private Long ebDelOrderLineNum;
    private Long quantityToPlan;
    private Long ebDelLivraisonLineNum;

    public Long getEbDelOrderLineNum() {
        return ebDelOrderLineNum;
    }

    public void setEbDelOrderLineNum(Long ebDelOrderLineNum) {
        this.ebDelOrderLineNum = ebDelOrderLineNum;
    }

    public Long getQuantityToPlan() {
        return quantityToPlan;
    }

    public void setQuantityToPlan(Long quantityToPlan) {
        this.quantityToPlan = quantityToPlan;
    }

    public Long getEbDelLivraisonLineNum() {
        return ebDelLivraisonLineNum;
    }

    public void setEbDelLivraisonLineNum(Long ebDelLivraisonLineNum) {
        this.ebDelLivraisonLineNum = ebDelLivraisonLineNum;
    }
}
