package com.adias.mytowereasy.template.model.delivery;

import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class EbLivraisonLineTPL extends BaseDocumentTPL {
    private String itemNumber;
    private String itemName;
    private Long quantity;
    private String partNumber;
    private String serialNumber;

    public EbLivraisonLineTPL() {
        // Empty constructor for template processor
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
