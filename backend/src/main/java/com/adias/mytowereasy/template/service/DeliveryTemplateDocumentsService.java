package com.adias.mytowereasy.template.service;

import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.template.model.DeliveryRootTPL;
import com.adias.mytowereasy.template.model.delivery.EbLivraisonLineTPL;
import com.adias.mytowereasy.template.model.delivery.EbLivraisonTPL;


public interface DeliveryTemplateDocumentsService {
    EbLivraisonLineTPL getEbLivraisonLineTPL(EbLivraisonLine model);

    EbLivraisonTPL getEbLivraisonTPL(EbLivraison model);

    DeliveryRootTPL generateDeliveryRootTPL(Long deliveryNum) throws Exception;
}
