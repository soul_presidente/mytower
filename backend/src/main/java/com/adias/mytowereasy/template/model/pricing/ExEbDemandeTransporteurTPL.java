package com.adias.mytowereasy.template.model.pricing;

import java.math.BigDecimal;
import java.util.Date;

import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class ExEbDemandeTransporteurTPL extends BaseDocumentTPL {
    private String companyName;
    private BigDecimal price;
    private Integer transitTime;
    private Date pickupTime;
    private Date deliveryTime;
    private String comment;
    private String status;

    public ExEbDemandeTransporteurTPL() {
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompany_name() {
        return companyName;
    }

    public void setCompany_name(String companyName) {
        this.companyName = companyName;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public Date getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(Date pickupTime) {
        this.pickupTime = pickupTime;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
