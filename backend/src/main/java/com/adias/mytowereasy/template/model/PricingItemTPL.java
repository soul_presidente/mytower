package com.adias.mytowereasy.template.model;

import java.util.Date;

import com.adias.mytowereasy.template.model.common.EbUserTPL;
import com.adias.mytowereasy.template.model.pricing.EbDemandeTPL;
import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class PricingItemTPL extends BaseDocumentTPL {
    private EbDemandeTPL demandePrincipal;
    private EbUserTPL connectedUser;
    private Date dateNow;

    public PricingItemTPL() {
        // Empty constructor for template processor
    }

    public EbDemandeTPL getDemandePrincipal() {
        return demandePrincipal;
    }

    public void setDemandePrincipal(EbDemandeTPL demandePrincipal) {
        this.demandePrincipal = demandePrincipal;
    }

    public EbUserTPL getConnectedUser() {
        return connectedUser;
    }

    public void setConnectedUser(EbUserTPL connectedUser) {
        this.connectedUser = connectedUser;
    }

    public Date getDateNow() {
        return dateNow;
    }

    public void setDateNow(Date dateNow) {
        this.dateNow = dateNow;
    }
}
