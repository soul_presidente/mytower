package com.adias.mytowereasy.template.service;

import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.template.model.common.CustomFieldsTPL;
import com.adias.mytowereasy.template.model.common.EbCategorieTPL;
import com.adias.mytowereasy.template.model.common.EbEtablissementTPL;
import com.adias.mytowereasy.template.model.common.EbPartyTPL;
import com.adias.mytowereasy.template.model.common.EbUserTPL;


public interface CommonTemplateDocumentsService {
    EbEtablissementTPL getEbEtablissementTPL(EbEtablissement etablissement);

    EbUserTPL getEbUserTPL(EbUser user);

    EbPartyTPL getEbPartyTPL(EbParty party);

    EbCategorieTPL getEbCategorieTPL(EbCategorie categorie);

    CustomFieldsTPL getCustomFieldsTPL(CustomFields customFields);
}
