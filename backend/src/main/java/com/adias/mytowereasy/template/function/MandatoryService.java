package com.adias.mytowereasy.template.function;

public interface MandatoryService {
    Object replaceByMandatoryIfEmpty(Object object);

    Object replaceByMandatoryIfEmpty(Object object, Boolean returnEmptyIfPresent);

    Object replaceByMandatoryIfEmpty(String value);

    Object replaceByMandatoryIfEmpty(String value, Boolean returnEmptyIfPresent);
}
