package com.adias.mytowereasy.template.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.delivery.enumeration.StatusOrder;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.repository.EbOrderRepository;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.template.model.OrderRootTPL;
import com.adias.mytowereasy.template.model.order.EbOrderLineTPL;
import com.adias.mytowereasy.template.model.order.EbOrderTPL;
import com.adias.mytowereasy.util.enums.GenericEnumException;
import com.adias.mytowereasy.util.enums.GenericEnumUtils;


@Service
public class OrderTemplateDocumentsServiceImpl implements OrderTemplateDocumentsService {
    @Autowired
    CommonTemplateDocumentsService commonTpl;

    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    EbOrderRepository ebOrderRepository;

    @Override
    public EbOrderLineTPL getEbOrderLineTPL(EbOrderLine model) {
        EbOrderLineTPL tpl = new EbOrderLineTPL();

        tpl.setItemNumber(model.getItemNumber() != null ? model.getItemNumber() : "");
        tpl.setItemName(model.getItemName() != null ? model.getItemName() : "");
        tpl.setPartNumber(model.getPartNumber() != null ? model.getPartNumber() : "");
        tpl.setSerialNumber(model.getSerialNumber() != null ? model.getSerialNumber() : "");
        tpl.setComment(model.getComment() != null ? model.getComment() : "");
        tpl.setWeight(model.getWeight() != null ? model.getWeight() : 0);
        tpl.setLength(model.getLength() != null ? model.getLength() : 0);
        tpl.setWidth(model.getWidth() != null ? model.getWidth() : 0);
        tpl.setVolume(model.getVolume() != null ? model.getVolume() : 0);
        tpl.setSerialized(model.getSerialized());
        tpl.setQuantityOrdered(model.getQuantityOrdered() != null ? model.getQuantityOrdered() : 0);
        tpl.setQuantityPlannable(model.getQuantityPlannable() != null ? model.getQuantityPlannable() : 0);
        tpl.setQuantityPlanned(model.getQuantityPlanned() != null ? model.getQuantityPlanned() : 0);
        tpl.setQuantityPending(model.getQuantityPending() != null ? model.getQuantityPending() : 0);
        tpl.setQuantityConfirmed(model.getQuantityConfirmed() != null ? model.getQuantityConfirmed() : 0);
        tpl.setQuantityShipped(model.getQuantityShipped() != null ? model.getQuantityShipped() : 0);
        tpl.setQuantityDelivered(model.getQuantityDelivered() != null ? model.getQuantityDelivered() : 0);
        tpl.setQuantityCanceled(model.getQuantityCanceled() != null ? model.getQuantityCanceled() : 0);

        return tpl;
    }

    @Override
    public EbOrderTPL getEbOrderTPL(EbOrder model) {
        EbOrderTPL tpl = new EbOrderTPL();

        try {
            tpl.setStatus(GenericEnumUtils.getKeyByCode(StatusOrder.class, model.getOrderStatus()).orElse(null));
        } catch (GenericEnumException e) {
            tpl.setStatus("");
            e.printStackTrace();
        }

        tpl.setOrderRef(model.getReference() != null ? model.getReference() : "");
        tpl.setCustomerOrderRef(model.getCustomerOrderReference() != null ? model.getCustomerOrderReference() : "");
        tpl.setCustomerCreationDate(model.getCustomerCreationDate());
        tpl.setTargetDate(model.getTargetDate());
        tpl
            .setComplementaryInfos(
                model.getComplementaryInformations() != null ? model.getComplementaryInformations() : "");

        tpl.setOwnerUser(commonTpl.getEbUserTPL(model.getxEbOwnerOfTheRequest()));
        tpl.setPartyOrigin(commonTpl.getEbPartyTPL(model.getxEbPartyOrigin()));
        tpl.setPartyDest(commonTpl.getEbPartyTPL(model.getxEbPartyDestination()));
        tpl.setPartySoldTo(commonTpl.getEbPartyTPL(model.getxEbPartySale()));

        tpl.setTotalQuantityOrdered(model.getTotalQuantityOrdered() != null ? model.getTotalQuantityOrdered() : 0);
        tpl
            .setTotalQuantityPlannable(
                model.getTotalQuantityPlannable() != null ? model.getTotalQuantityPlannable() : 0);
        tpl.setTotalQuantityPlanned(model.getTotalQuantityPlanned() != null ? model.getTotalQuantityPlanned() : 0);
        tpl.setTotalQuantityPending(model.getTotalQuantityPending() != null ? model.getTotalQuantityPending() : 0);
        tpl
            .setTotalQuantityConfirmed(
                model.getTotalQuantityConfirmed() != null ? model.getTotalQuantityConfirmed() : 0);
        tpl.setTotalQuantityShipped(model.getTotalQuantityShipped() != null ? model.getTotalQuantityShipped() : 0);
        tpl
            .setTotalQuantityDelivered(
                model.getTotalQuantityDelivered() != null ? model.getTotalQuantityDelivered() : 0);
        tpl.setTotalQuantityCanceled(model.getTotalQuantityCanceled() != null ? model.getTotalQuantityCanceled() : 0);

        return tpl;
    }

    @Override
    public OrderRootTPL generateOrderRootTPL(Long orderNum) throws Exception {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        EbOrder order = ebOrderRepository.findById(orderNum).get();

        // Generate Template
        OrderRootTPL tplModel = new OrderRootTPL();
        tplModel.setDateNow(new Date());
        tplModel.setOrder(getEbOrderTPL(order));
        tplModel.setConnectedUser(commonTpl.getEbUserTPL(connectedUser));

        return tplModel;
    }
}
