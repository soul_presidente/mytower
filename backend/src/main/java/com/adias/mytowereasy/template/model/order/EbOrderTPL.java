package com.adias.mytowereasy.template.model.order;

import java.util.Date;
import java.util.List;

import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.template.model.common.EbPartyTPL;
import com.adias.mytowereasy.template.model.common.EbUserTPL;
import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class EbOrderTPL extends BaseDocumentTPL {
    private String orderRef;
    private String status;
    private String customerOrderRef;

    private EbUserTPL ownerUser;
    private EbPartyTPL partyOrigin;
    private EbPartyTPL partyDest;
    private EbPartyTPL partySoldTo;
    private Date customerCreationDate;
    private Date targetDate;

    private String complementaryInfos;

    private Long totalQuantityOrdered;
    private Long totalQuantityPlannable;
    private Long totalQuantityPlanned;
    private Long totalQuantityPending;
    private Long totalQuantityConfirmed;
    private Long totalQuantityShipped;
    private Long totalQuantityDelivered;
    private Long totalQuantityCanceled;

    private List<EbOrderLine> units;

    public EbOrderTPL() {
        // Empty constructor for template processor
    }

    public String getOrderRef() {
        return orderRef;
    }

    public void setOrderRef(String orderRef) {
        this.orderRef = orderRef;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerOrderRef() {
        return customerOrderRef;
    }

    public void setCustomerOrderRef(String customerOrderRef) {
        this.customerOrderRef = customerOrderRef;
    }

    public EbUserTPL getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(EbUserTPL ownerUser) {
        this.ownerUser = ownerUser;
    }

    public EbPartyTPL getPartyOrigin() {
        return partyOrigin;
    }

    public void setPartyOrigin(EbPartyTPL partyOrigin) {
        this.partyOrigin = partyOrigin;
    }

    public EbPartyTPL getPartyDest() {
        return partyDest;
    }

    public void setPartyDest(EbPartyTPL partyDest) {
        this.partyDest = partyDest;
    }

    public EbPartyTPL getPartySoldTo() {
        return partySoldTo;
    }

    public void setPartySoldTo(EbPartyTPL partySoldTo) {
        this.partySoldTo = partySoldTo;
    }

    public Date getCustomerCreationDate() {
        return customerCreationDate;
    }

    public void setCustomerCreationDate(Date customerCreationDate) {
        this.customerCreationDate = customerCreationDate;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public String getComplementaryInfos() {
        return complementaryInfos;
    }

    public void setComplementaryInfos(String complementaryInfos) {
        this.complementaryInfos = complementaryInfos;
    }

    public Long getTotalQuantityOrdered() {
        return totalQuantityOrdered;
    }

    public void setTotalQuantityOrdered(Long totalQuantityOrdered) {
        this.totalQuantityOrdered = totalQuantityOrdered;
    }

    public Long getTotalQuantityPlannable() {
        return totalQuantityPlannable;
    }

    public void setTotalQuantityPlannable(Long totalQuantityPlannable) {
        this.totalQuantityPlannable = totalQuantityPlannable;
    }

    public Long getTotalQuantityPlanned() {
        return totalQuantityPlanned;
    }

    public void setTotalQuantityPlanned(Long totalQuantityPlanned) {
        this.totalQuantityPlanned = totalQuantityPlanned;
    }

    public Long getTotalQuantityPending() {
        return totalQuantityPending;
    }

    public void setTotalQuantityPending(Long totalQuantityPending) {
        this.totalQuantityPending = totalQuantityPending;
    }

    public Long getTotalQuantityConfirmed() {
        return totalQuantityConfirmed;
    }

    public void setTotalQuantityConfirmed(Long totalQuantityConfirmed) {
        this.totalQuantityConfirmed = totalQuantityConfirmed;
    }

    public Long getTotalQuantityShipped() {
        return totalQuantityShipped;
    }

    public void setTotalQuantityShipped(Long totalQuantityShipped) {
        this.totalQuantityShipped = totalQuantityShipped;
    }

    public Long getTotalQuantityDelivered() {
        return totalQuantityDelivered;
    }

    public void setTotalQuantityDelivered(Long totalQuantityDelivered) {
        this.totalQuantityDelivered = totalQuantityDelivered;
    }

    public Long getTotalQuantityCanceled() {
        return totalQuantityCanceled;
    }

    public void setTotalQuantityCanceled(Long totalQuantityCanceled) {
        this.totalQuantityCanceled = totalQuantityCanceled;
    }

    public List<EbOrderLine> getUnits() {
        return units;
    }

    public void setUnits(List<EbOrderLine> units) {
        this.units = units;
    }
}
