package com.adias.mytowereasy.template.service;

import java.nio.file.Path;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.adias.mytowereasy.dto.EbTemplateDocumentsDTO;
import com.adias.mytowereasy.dto.TemplateGenParamsDTO;
import com.adias.mytowereasy.model.EbTemplateDocuments;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface TemplateDocumentsService {
    public List<EbTemplateDocuments> searchTemplateDocuments(SearchCriteria criteria);

    public Long countListEbTemplateDocuments(SearchCriteria criteria);

    List<EbTemplateDocumentsDTO> convertEntitiesToDto(List<EbTemplateDocuments> entities);

    public EbTemplateDocumentsDTO saveEbTemplateDocuments(EbTemplateDocumentsDTO templateDocuments);

    public void deleteEbTemplateDocuments(Integer templateDocumentsNum);

    public String uploadFile(MultipartFile file, Integer ebCompagnieNum) throws Exception;

    public Path templateGeneration(TemplateGenParamsDTO params, EbUser connectedUser) throws Exception;
}
