package com.adias.mytowereasy.template.service;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.template.model.common.CustomFieldsTPL;
import com.adias.mytowereasy.template.model.common.EbCategorieTPL;
import com.adias.mytowereasy.template.model.common.EbEtablissementTPL;
import com.adias.mytowereasy.template.model.common.EbPartyTPL;
import com.adias.mytowereasy.template.model.common.EbUserTPL;


@Service
public class CommonTemplateDocumentsServiceImpl implements CommonTemplateDocumentsService {
    @Override
    public EbEtablissementTPL getEbEtablissementTPL(EbEtablissement etablissement) {
        EbEtablissementTPL template = new EbEtablissementTPL();

        template.setEbEtablissementNum(etablissement.getEbEtablissementNum());
        template
            .setTypeEtablissement(
                etablissement.getTypeEtablissement() != null ? etablissement.getTypeEtablissement() : "");
        template.setSiret(etablissement.getSiret() != null ? etablissement.getSiret() : "");
        template.setNom(etablissement.getNom() != null ? etablissement.getNom() : "");
        template.setEmail(etablissement.getEmail() != null ? etablissement.getEmail() : "");

        return template;
    }

    @Override
    public EbUserTPL getEbUserTPL(EbUser user) {
        EbUserTPL template = new EbUserTPL();

        template.setNom(user.getNom() != null ? user.getNom() : "");
        template.setPrenom(user.getPrenom() != null ? user.getPrenom() : "");
        template.setEmail(user.getEmail() != null ? user.getEmail() : "");
        template.setEtablissementNom(user.getEtablissementNom() != null ? user.getEtablissementNom() : "");
        template.setAdresse(user.getAdresse() != null ? user.getAdresse() : "");
        template.setPhone(user.getTelephone() != null ? user.getTelephone() : "");

        return template;
    }

    @Override
    public EbPartyTPL getEbPartyTPL(EbParty party) {
        if (party == null) return null;

        final EbPartyTPL template = new EbPartyTPL();

        template.setCompany(party.getCompany() != null ? party.getCompany() : "");
        template.setAdresse(party.getAdresse() != null ? party.getAdresse() : "");
        template.setCity(party.getCity() != null ? party.getCity() : "");
        template.setZipCode(party.getZipCode() != null ? party.getZipCode() : "");
        template.setOpeningHours(party.getOpeningHours() != null ? party.getOpeningHours() : "");
        template.setAirport(party.getAirport() != null ? party.getAirport() : "");
        template.setEmail(party.getEmail() != null ? party.getEmail() : "");
        template.setPhone(party.getPhone() != null ? party.getPhone() : "");
        template.setCommentaire(party.getCommentaire() != null ? party.getCommentaire() : "");
        template
            .setCountryName(
                party.getxEcCountry() != null && party.getxEcCountry().getLibelle() != null ?
                    party.getxEcCountry().getLibelle() :
                    "");

        return template;
    }

    @Override
    public EbCategorieTPL getEbCategorieTPL(EbCategorie categorie) {
        final EbCategorieTPL template = new EbCategorieTPL();

        template.setLibelle(categorie.getLibelle() != null ? categorie.getLibelle() : "");
        template.setCode(categorie.getCode() != null ? categorie.getCode() : "");

        return template;
    }

    @Override
    public CustomFieldsTPL getCustomFieldsTPL(CustomFields customFields) {
        final CustomFieldsTPL template = new CustomFieldsTPL();

        template.setName(customFields.getName() != null ? customFields.getName() : "");
        template.setValue(customFields.getValue() != null ? customFields.getValue() : "");
        template.setLabel(customFields.getLabel() != null ? customFields.getLabel() : "");

        return template;
    }
}
