package com.adias.mytowereasy.template.model;

import java.util.Date;

import com.adias.mytowereasy.template.model.common.EbUserTPL;
import com.adias.mytowereasy.template.model.order.EbOrderTPL;
import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class OrderRootTPL extends BaseDocumentTPL {
    private EbOrderTPL order;
    private EbUserTPL connectedUser;
    private Date dateNow;

    public OrderRootTPL() {
        // Empty constructor for template processor
    }

    public EbOrderTPL getOrder() {
        return order;
    }

    public void setOrder(EbOrderTPL order) {
        this.order = order;
    }

    public EbUserTPL getConnectedUser() {
        return connectedUser;
    }

    public void setConnectedUser(EbUserTPL connectedUser) {
        this.connectedUser = connectedUser;
    }

    public Date getDateNow() {
        return dateNow;
    }

    public void setDateNow(Date dateNow) {
        this.dateNow = dateNow;
    }
}
