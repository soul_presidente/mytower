package com.adias.mytowereasy.template.model.order;

import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class EbOrderLineTPL extends BaseDocumentTPL {
    private String itemNumber;
    private String itemName;
    private String partNumber;
    private String serialNumber;
    private String comment;

    private Double weight;
    private Double length;
    private Double width;
    private Double volume;
    private Boolean serialized;

    private Long quantityOrdered;
    private Long quantityPlannable;
    private Long quantityPlanned;
    private Long quantityPending;
    private Long quantityConfirmed;
    private Long quantityShipped;
    private Long quantityDelivered;
    private Long quantityCanceled;

    public EbOrderLineTPL() {
        // Empty constructor for template processor
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Boolean getSerialized() {
        return serialized;
    }

    public void setSerialized(Boolean serialized) {
        this.serialized = serialized;
    }

    public Long getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(Long quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public Long getQuantityPlannable() {
        return quantityPlannable;
    }

    public void setQuantityPlannable(Long quantityPlannable) {
        this.quantityPlannable = quantityPlannable;
    }

    public Long getQuantityPlanned() {
        return quantityPlanned;
    }

    public void setQuantityPlanned(Long quantityPlanned) {
        this.quantityPlanned = quantityPlanned;
    }

    public Long getQuantityPending() {
        return quantityPending;
    }

    public void setQuantityPending(Long quantityPending) {
        this.quantityPending = quantityPending;
    }

    public Long getQuantityConfirmed() {
        return quantityConfirmed;
    }

    public void setQuantityConfirmed(Long quantityConfirmed) {
        this.quantityConfirmed = quantityConfirmed;
    }

    public Long getQuantityShipped() {
        return quantityShipped;
    }

    public void setQuantityShipped(Long quantityShipped) {
        this.quantityShipped = quantityShipped;
    }

    public Long getQuantityDelivered() {
        return quantityDelivered;
    }

    public void setQuantityDelivered(Long quantityDelivered) {
        this.quantityDelivered = quantityDelivered;
    }

    public Long getQuantityCanceled() {
        return quantityCanceled;
    }

    public void setQuantityCanceled(Long quantityCanceled) {
        this.quantityCanceled = quantityCanceled;
    }
}
