package com.adias.mytowereasy.template.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import com.adias.mytowereasy.dto.TemplateGenParamsDTO;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.NatureDemandeTransport;
import com.adias.mytowereasy.model.Enumeration.StatutCarrier;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.repository.EbLabelRepository;
import com.adias.mytowereasy.repository.EbTypeTransportRepository;
import com.adias.mytowereasy.repository.ExEbDemandeTransporteurRepository;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.PricingService;
import com.adias.mytowereasy.service.email.EmailServicePricingBooking;
import com.adias.mytowereasy.template.model.PricingItemTPL;
import com.adias.mytowereasy.template.model.pricing.EbDemandeTPL;
import com.adias.mytowereasy.template.model.pricing.EbMarchandiseTPL;
import com.adias.mytowereasy.template.model.pricing.ExEbDemandeTransporteurTPL;
import com.adias.mytowereasy.util.GsonFactory;
import com.adias.mytowereasy.util.ObjectDeserializer;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Service
public class PricingTemplateDocumentsServiceImpl extends MyTowerService implements PricingTemplateDocumentsService {
    @Autowired
    EbTypeTransportRepository ebTypeTransportRepository;

    @Autowired
    ExEbDemandeTransporteurRepository demandeTransporteurRepository;

    @Autowired
    CommonTemplateDocumentsService commonTpl;

    @Autowired
    PricingService pricingService;

    @Autowired
    EmailServicePricingBooking emailServicePricingBooking;
    @Autowired
    EbLabelRepository ebLabelRepository;

    @Override
    public EbMarchandiseTPL getEbMarchandiseTPL(EbMarchandise marchandise) {
        final EbMarchandiseTPL template = new EbMarchandiseTPL();

        template.setTypeOfUnit(marchandise.getLabelTypeUnit() != null ? marchandise.getLabelTypeUnit() : "");
        template
            .setOriginCountryName(marchandise.getOriginCountryName() != null ? marchandise.getOriginCountryName() : "");
        template.setNetWeight(marchandise.getNetWeight() != null ? marchandise.getNetWeight() : 0.0);
        template.setWeight(marchandise.getWeight() != null ? marchandise.getWeight() : 0.0);
        template.setLength(marchandise.getLength() != null ? marchandise.getLength() : 0.0);
        template.setWidth(marchandise.getWidth() != null ? marchandise.getWidth() : 0.0);
        template.setHeigth(marchandise.getHeigth() != null ? marchandise.getHeigth() : 0.0);
        template.setDg(marchandise.getDg() != null ? marchandise.getDg() : "");
        template
            .setDangerousGood(
                marchandise.getDangerousGood() != null ?
                    Enumeration.MarchandiseDangerousGood
                        .getListMarchandiseDangerousGood().get(marchandise.getDangerousGood()) :
                    "");
        template.setClassGood(marchandise.getClassGood() != null ? marchandise.getClassGood() : "");
        template.setUn(marchandise.getUn() != null ? marchandise.getUn() : "0");
        template.setTypeMarchandise(marchandise.getTypeMarchandise() != null ? marchandise.getTypeMarchandise() : "");
        template.setUnitReference(marchandise.getUnitReference() != null ? marchandise.getUnitReference() : "");
        template
            .setCustomerReference(marchandise.getCustomerReference() != null ? marchandise.getCustomerReference() : "");
        template.setQuantityDryIce(marchandise.getQuantityDryIce() != null ? marchandise.getQuantityDryIce() : "");
        template.setNumberOfUnits(marchandise.getNumberOfUnits() != null ? marchandise.getNumberOfUnits() : 0);
        template.setVolume(marchandise.getVolume() != null ? marchandise.getVolume() : 0.0);
        template.setPackaging(marchandise.getPackaging() != null ? marchandise.getPackaging() : "");
        template.setStackable(marchandise.getStackable() != null ? marchandise.getStackable() : false);
        template.setComment(marchandise.getComment() != null ? marchandise.getComment() : "");
        template.setNomArticle(marchandise.getNomArticle() != null ? marchandise.getNomArticle() : "");
        template.setLot(marchandise.getLot() != null ? marchandise.getLot() : "");

        template.setSscc(marchandise.getSscc() != null ? marchandise.getSscc() : "");
        template.setOlpn(marchandise.getOlpn() != null ? marchandise.getOlpn() : "");
        template.setTrackingNumber(marchandise.getTracking_number() != null ? marchandise.getTracking_number() : "");
        template.setEtat(marchandise.getEtat() != null ? marchandise.getEtat() : "");
        template.setPood(marchandise.getPood() != null ? marchandise.getPood() : "");
        template.setTypeContainer(marchandise.getTypeContainer() != null ? marchandise.getTypeContainer() : "");
        template.setHandlingUnit(marchandise.getHandling_unit() != null ? marchandise.getHandling_unit() : "");
        template.setHuLineNumber(marchandise.getHu_line_number() != null ? marchandise.getHu_line_number() : 0);
        template.setLotQuantite(marchandise.getLot_quantite() != null ? marchandise.getLot_quantite() : "");
        template.setTypeOfUnit(marchandise.getLabelTypeUnit() != null ? marchandise.getLabelTypeUnit() : "");
        template.setHsCode(marchandise.getHsCode() != null ? marchandise.getHsCode() : "");
        template.setUnitPrice(marchandise.getPrice() != null ? marchandise.getPrice() : 0.0);
        template
            .setPrice(
                (marchandise.getPrice() != null && marchandise.getNumberOfUnits() != null) ?
                    marchandise.getPrice() * marchandise.getNumberOfUnits() :
                    0.0);

        if (template.getUnitPrice() != null && template.getNumberOfUnits() != null) {
            template.setPriceNetCalcul(template.getUnitPrice() * template.getNumberOfUnits());
        }

        return template;
    }

    public ExEbDemandeTransporteurTPL getExEbDemandeTransporteurTPL(ExEbDemandeTransporteur exEbDemandeTransporteur) {
        ExEbDemandeTransporteurTPL template = new ExEbDemandeTransporteurTPL();

        template
            .setCompanyName(
                exEbDemandeTransporteur.getCompany_name() != null ? exEbDemandeTransporteur.getCompany_name() : "");

        template
            .setPrice(
                exEbDemandeTransporteur.getPrice() != null ? exEbDemandeTransporteur.getPrice() : new BigDecimal(0.0));
        template
            .setTransitTime(
                exEbDemandeTransporteur.getTransitTime() != null ? exEbDemandeTransporteur.getTransitTime() : 0);
        template.setPickupTime(exEbDemandeTransporteur.getPickupTime());
        template.setDeliveryTime(exEbDemandeTransporteur.getDeliveryTime());
        template.setComment(exEbDemandeTransporteur.getComment() != null ? exEbDemandeTransporteur.getComment() : "");
        template
            .setStatus(
                exEbDemandeTransporteur != null ?
                    StatutCarrier.getLibelleByCode(exEbDemandeTransporteur.getStatus()) :
                    "");

        return template;
    }

    @SuppressWarnings("serial")
    @Override
    public EbDemandeTPL getEbDemandeTPL(EbDemande demande) {
        final EbDemandeTPL template = new EbDemandeTPL();

        // Retrieve Required subfields from dependencies
        EbUser user = new EbUser();
        if (null != demande.getUser()) user = userService.getEbUserByEbUserNum(demande.getUser().getEbUserNum());

        EbParty partyOrigin = new EbParty();
        EbParty partyDest = new EbParty();
        EbParty partyNotif = new EbParty();

        if (null != demande.getEbDemandeNum()) {
            partyOrigin = ebDemandeRepository.selectEbPartyOriginByEbDemandeNum(demande.getEbDemandeNum());
            partyDest = ebDemandeRepository.selectEbPartyDestByEbDemandeNum(demande.getEbDemandeNum());
            partyNotif = ebDemandeRepository.selectEbPartyNotifByEbDemandeNum(demande.getEbDemandeNum());

            List<EbMarchandise> marchandises = ebMarchandiseRepository
                .findAllByEbDemande_ebDemandeNum(demande.getEbDemandeNum());
            demande.setListMarchandises(marchandises);

            List<ExEbDemandeTransporteur> demandeTransporteur = demandeTransporteurRepository
                .findAllByXEbDemande_ebDemandeNum(demande.getEbDemandeNum());
            demande.setExEbDemandeTransporteurs(demandeTransporteur);
        }

        EbEtablissement etablissement = new EbEtablissement();
        if (null != demande.getxEbEtablissement()) etablissement = daoEtablissement
            .selectEbEtablissementByEbUser(demande.getxEbEtablissement().getEbEtablissementNum());

        Optional<EbTypeRequest> typeRequest = Optional.empty();

        if (demande.getxEbTypeTransport() != null) {
            typeRequest = ebTypeRequestRepository.findById(demande.getxEbTypeRequest());
        }

        Optional<EbTypeTransport> typeTransport = Optional.empty();

        if (demande.getxEbTypeTransport() != null) {
            typeTransport = ebTypeTransportRepository.findById(demande.getxEbTypeTransport());
        }

        // Fill template
        template.setUser(commonTpl.getEbUserTPL(user));
        template.setPartyOrigin(commonTpl.getEbPartyTPL(partyOrigin));
        template.setPartyDestination(commonTpl.getEbPartyTPL(partyDest));

        if (partyDest.getxEcCountry() != null) {
            Boolean hasDestInEurop = false;
            EcCountry country = new EcCountry();
            country = partyDest.getxEcCountry();
            hasDestInEurop = country.getEurop();
            template.setHasDestInEurop(hasDestInEurop);
        }

        if (partyNotif != null) template.setPartyNotify(commonTpl.getEbPartyTPL(partyNotif));
        else template.setPartyNotify(commonTpl.getEbPartyTPL(new EbParty()));
        template.setEtablissement(commonTpl.getEbEtablissementTPL(etablissement));

        template.setReference(demande.getRefTransport() != null ? demande.getRefTransport() : "");
        template.setCity(demande.getCity() != null ? demande.getCity() : "");
        template
            .setTypeDemande(
                demande.getxEcTypeDemande() != null ?
                    Enumeration.ModeTransport.getLibelleByCode(demande.getxEcTypeDemande()) :
                    "");
        template
            .setModeTransport(
                demande.getxEcModeTransport() != null ?
                    Enumeration.ModeTransport.getLibelleByCode(demande.getxEcModeTransport()) :
                    "");
        template.setIncoterm(demande.getxEcIncotermLibelle() != null ? demande.getxEcIncotermLibelle() : "");
        template
            .setLibelleOriginCountry(
                demande.getLibelleOriginCountry() != null ? demande.getLibelleOriginCountry() : "");
        template
            .setLibelleOriginCountry(
                demande.getLibelleOriginCountry() != null ? demande.getLibelleOriginCountry() : "");
        template.setLibelleDestCountry(demande.getLibelleDestCountry() != null ? demande.getLibelleDestCountry() : "");
        template.setTemperatureStorage(demande.getTemperatureStorage() != null ? demande.getTemperatureStorage() : "");
        template
            .setTemperatureTransport(
                demande.getTemperatureTransport() != null ? demande.getTemperatureTransport() : "");
        template.setCostCenter(demande.getxEbCostCenter() != null ? demande.getxEbCostCenter().getLibelle() : "");
        template.setCodeAnnulation(demande.getCodeAnnulation() != null ? demande.getCodeAnnulation() : "");
        template.setAboutTrAwbBol(demande.getAboutTrAwbBol() != null ? demande.getAboutTrAwbBol() : "");
        template.setAboutTrMawb(demande.getAboutTrMawb() != null ? demande.getAboutTrMawb() : "");
        template
            .setAboutTrFlightVessel(demande.getAboutTrFlightVessel() != null ? demande.getAboutTrFlightVessel() : "");
        template.setCustomerReference(demande.getCustomerReference() != null ? demande.getCustomerReference() : "");
        template.setInsurance(demande.getInsurance() != null ? demande.getInsurance() : false);
        template
            .setInsuranceValue(demande.getInsuranceValue() != null ? Double.toString(demande.getInsuranceValue()) : "");
        template
            .setInsuranceCurrencyCode(
                demande.getXecCurrencyInvoice() != null && demande.getXecCurrencyInvoice().getCode() != null ?
                    demande.getXecCurrencyInvoice().getCode() :
                    "");
        template
            .setCurrencyCode(
                demande.getXecCurrencyInvoice() != null && demande.getXecCurrencyInvoice().getCode() != null ?
                    demande.getXecCurrencyInvoice().getCode() :
                    "");
        template.setTotalNbrParcel(demande.getTotalNbrParcel() != null ? demande.getTotalNbrParcel() : 0.0);
        template.setTotalVolume(demande.getTotalVolume() != null ? demande.getTotalVolume() : 0.0);
        template.setTotalWeight(demande.getTotalWeight() != null ? demande.getTotalWeight() : 0.0);
        template.setTotalTaxableWeight(demande.getTotalTaxableWeight() != null ? demande.getTotalTaxableWeight() : 0.0);
        template.setCarrierName(demande.getNomTransporteur() != null ? demande.getNomTransporteur() : "");
        template
            .setCommentChargeur(
                demande.getCommentChargeur() != null ?
                    demande.getCommentChargeur().replaceAll("\\<.*?\\>", " ").replaceAll("  ", " ") :
                    "");
        template.setCommentChargeurHtml(demande.getCommentChargeur() != null ? demande.getCommentChargeur() : "");
        template
            .setInsuranceCurrency(
                demande.getInsurance() && demande.getXecCurrencyInsurance() != null
                    && demande.getXecCurrencyInsurance().getCodeLibelle() != null ?
                        demande.getXecCurrencyInsurance().getCodeLibelle() :
                        "");
        template
            .setTypeRequest(
                typeRequest.isPresent() && typeRequest.get().getLibelle() != null ?
                    typeRequest.get().getLibelle() :
                    "");
        template
            .setLibelleTypeTransport(
                typeTransport.isPresent() && typeTransport.get().getLibelle() != null ?
                    typeTransport.get().getLibelle() :
                    "");
        template.setUnitsReference(demande.getUnitsReference() != null ? demande.getUnitsReference() : "");
        template
            .setNature(
                demande.getxEcNature() != null ?
                    NatureDemandeTransport.fromCode(demande.getxEcNature()).getLibelle() :
                    "");
        template
            .setMasterObjectReference(
                demande.getEbDemandeMasterObject() != null
                    && demande.getEbDemandeMasterObject().getRefTransport() != null ?
                        demande.getEbDemandeMasterObject().getRefTransport() :
                        "");
        template.setDateOfArrival(demande.getDateOfArrival() != null ? demande.getDateOfArrival() : null);
        template.setAvailabilityDate(demande.getDateOfGoodsAvailability());
        template.setDateCreation(demande.getDateCreation());
        template.setPslStartDate(demande.getDatePickupTM());
        template.setPslEndDate(demande.getFinalDelivery());
        template
            .setNameCreator(
                demande.getxEbUserCt() != null && demande.getxEbUserCt().getNomPrenom() != null ?
                    demande.getxEbUserCt().getNomPrenom() :
                    "");
        template
            .setNameOwner(
                demande.getUser() != null && demande.getUser().getNomPrenom() != null ?
                    demande.getUser().getNomPrenom() :
                    "");
        template.setLibelleLastPsl(demande.getLibelleLastPsl() != null ? demande.getLibelleLastPsl() : "");
        template.setDateLastPsl(demande.getDateLastPsl());
        template
            .setFlagTransportInformation(
                demande.getFlagTransportInformation() != null ? demande.getFlagTransportInformation() : false);
        template.setConfirmPickup(demande.getConfirmPickup() != null ? demande.getConfirmPickup() : false);
        template.setConfirmDelivery(demande.getConfirmDelivery() != null ? demande.getConfirmDelivery() : false);
        template.setStatus(demande.getxEcStatut() != null ? StatutDemande.getLibelle(demande.getxEcStatut()) : "");

        if (demande.getSelectedCarrier() != null) {
            template
                .setCarrierCompany(
                    demande.getSelectedCarrier().getxEbCompagnie() != null
                        && demande.getSelectedCarrier().getxEbCompagnie().getNom() != null ?
                            demande.getSelectedCarrier().getxEbCompagnie().getNom() :
                            "");
            template
                .setCarrierEstablishment(
                    demande.getSelectedCarrier().getxEbEtablissement() != null
                        && demande.getSelectedCarrier().getxEbEtablissement().getNom() != null ?
                            demande.getSelectedCarrier().getxEbEtablissement().getNom() :
                            "");
            template
                .setCarrierEmail(
                    demande.getSelectedCarrier().getxTransporteur() != null
                        && demande.getSelectedCarrier().getxTransporteur().getEmail() != null ?
                            demande.getSelectedCarrier().getxTransporteur().getEmail() :
                            "");
            template
                .setCarrierPhone(
                    demande.getSelectedCarrier().getxTransporteur() != null
                        && demande.getSelectedCarrier().getxTransporteur().getTelephone() != null ?
                            demande.getSelectedCarrier().getxTransporteur().getTelephone() :
                            "");
        }
        else {
            template.setCarrierCompany("");
            template.setCarrierEmail("");
            template.setCarrierEstablishment("");
        }

        // Units informations
        template.setUnits(new ArrayList<>());

        if (demande.getListMarchandises() != null) {
            demande.getListMarchandises().forEach(m -> {
                template.getUnits().add(getEbMarchandiseTPL(m));
            });
        }

        Double totalUnitPrice = 0d;

        for (EbMarchandiseTPL m: template.getUnits()) {

            if (m.getPriceNetCalcul() != null) {
                totalUnitPrice += m.getPriceNetCalcul();
            }

        }

        template.setTotalUnitPrice(totalUnitPrice);

        // Quotations (ExEbDemandeTransporteurs)
        template.setQuotations(new ArrayList<>());

        if (demande.getExEbDemandeTransporteurs() != null) {
            demande.getExEbDemandeTransporteurs().forEach(l -> {
                template.getQuotations().add(getExEbDemandeTransporteurTPL(l));

                if (Enumeration.StatutCarrier.FIN.getCode().equals(l.getStatus())) {
                    template.setPrice(l.getPrice());

                    if (l.getxTransporteur() != null) {
                        template.setCarrierName(l.getxTransporteur().getNom());
                        template.setCarrierPhone(l.getxTransporteur().getTelephone());

                        if (l.getxTransporteur().getEbCompagnie() != null) {
                            template.setCarrierCompany(l.getxTransporteur().getEbCompagnie().getNom());
                        }

                    }

                }

            });
        }

        // Categories
        template.setCategoriesList(new ArrayList<>());

        if (demande.getListCategories() != null) {
            // pour deserialiser la liste et eviter l'erreur linked hashMap
            List<EbCategorie> categoriesList = ObjectDeserializer
                .checkJsonListObject(demande.getListCategories(), new TypeToken<List<EbCategorie>>() {
                }.getType());
            template.setCategorieLabelValue(new HashMap<>());

            for (EbCategorie cat: categoriesList) {

                if (cat.getLabels() != null && !cat.getLabels().isEmpty()) {

                    for (EbLabel label: cat.getLabels()) {
                        template.getCategoriesList().add(commonTpl.getEbCategorieTPL(cat));

                        // Values
                        template.getCategorieLabelValue().put(cat.getLibelle(), label.getLibelle());
                    }

                }
                else template.getCategorieLabelValue().put(cat.getLibelle(), "");

            }

        }

        // Champs paramétrables
        Gson gs = GsonFactory.newInstance();
        template.setCustomFieldsList(new ArrayList<>());

        if (demande.getListCategories() != null) {

            if (demande.getCustomFields() != null) {
                List<CustomFields> customFields = gs
                    .fromJson(demande.getCustomFields(), new TypeToken<List<CustomFields>>() {
                    }.getType());

                template.setCustomFields(new HashMap<String, String>());
                customFields.forEach(l -> {
                    template.getCustomFieldsList().add(commonTpl.getCustomFieldsTPL(l));
                    template
                        .getCustomFields()
                        .put(l.getName() != null ? l.getName() : "", l.getValue() != null ? l.getValue() : "");
                });
            }

        }

        return template;
    }

    @Override
    public boolean checkedInformationMandatory(TemplateGenParamsDTO params) throws Exception {
        boolean isMandotory = false;
        final EbTemplateDocuments ebTemplate = ebTemplateDocumentsRepository
            .findById(params.getSelectedTemplate()).get();
        String libelle = ebTemplate.getLibelle();
        SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();
        Integer ebDemandeNum = Integer.parseInt(params.getTemplateModelParams().get(0));
        EbUser connectedUser = connectedUserService.getCurrentUser();
        criteria.setEbDemandeNum(ebDemandeNum);
        EbDemande demandePrincipal = pricingService.getEbDemande(criteria, connectedUser);
        EbDemandeTPL template = getEbDemandeTPL(demandePrincipal);

        if (Enumeration.TemplateConfig.PREFACTURE.getLibelle().equalsIgnoreCase(libelle)
            || Enumeration.TemplateConfig.PROFORMA.getLibelle().equalsIgnoreCase(libelle)) {

            // envoie un email contient les information obligatoire pour genere
            // le template du essilor
            if (emailServicePricingBooking.checkedAllInformationMandotory(template)) {
                isMandotory = true;
                emailServicePricingBooking.sendEmailTemplateInformationNotCompleted(template, libelle);
            }
            else {

                if (demandePrincipal.getListCategories() != null && !demandePrincipal.getListCategories().isEmpty()) {

                    for (EbCategorie cat: demandePrincipal.getListCategories()) {

                        if (cat.getLibelle().equalsIgnoreCase("Statut Manual invoice")) {
                            EbLabel label = ebLabelRepository.findByLibelle("A traiter");
                            cat.getLabels().add(label);
                            pricingService
                                .updateEbDemande(
                                    demandePrincipal,
                                    Enumeration.TypeDataEbDemande.REFERENCE_INFORMATION.getCode(),
                                    null,connectedUser);
                        }

                    }

                }

            }

        }

        return isMandotory;
    }

    @Override
    public PricingItemTPL generatePricingItemTPL(Integer demandeNum, EbUser connectedUser) throws Exception {
        connectedUser = connectedUser == null ? connectedUserService.getCurrentUser() : connectedUser;

        // Retrieving demande object
        SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();
        criteria.setEbDemandeNum(demandeNum);
        EbDemande demandePrincipal = pricingService.getEbDemande(criteria, connectedUser);

        // Generate Template
        PricingItemTPL tplModel = new PricingItemTPL();
        tplModel.setDateNow(new Date());
        tplModel.setDemandePrincipal(getEbDemandeTPL(demandePrincipal));
        tplModel.setConnectedUser(commonTpl.getEbUserTPL(connectedUser));

        return tplModel;
    }
}
