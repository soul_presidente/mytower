package com.adias.mytowereasy.template.function;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.service.BeanUtil;
import com.adias.mytowereasy.service.ConnectedUserService;


@Service
public class MandatoryServiceImpl implements MandatoryService {
    private ConnectedUserService connectedUserService;

    private EbUser connectedUser;

    public MandatoryServiceImpl() {
        connectedUserService = BeanUtil.getBean(ConnectedUserService.class);
    }

    @Override
    public Object replaceByMandatoryIfEmpty(Object object) {
        return replaceByMandatoryIfEmpty(object, false);
    }

    @Override
    public Object replaceByMandatoryIfEmpty(Object object, Boolean returnEmptyIfPresent) {

        if (object != null) {
            return returnEmptyIfPresent ? "" : object;
        }
        else {
            return mandatoryMessage();
        }

    }

    @Override
    public Object replaceByMandatoryIfEmpty(String value) {
        return replaceByMandatoryIfEmpty(value, false);
    }

    @Override
    public Object replaceByMandatoryIfEmpty(String value, Boolean returnEmptyIfPresent) {

        if (StringUtils.isNotBlank(value)) {
            return returnEmptyIfPresent ? "" : value;
        }
        else {
            return mandatoryMessage();
        }

    }

    private String mandatoryMessage() {
        EbUser connectedUser = this.connectedUser == null ? connectedUserService.getCurrentUser() : this.connectedUser;
        String message = "this field is mandatory"; // by default make message
                                                    // in English

        String lang = connectedUser.getLanguage();

        if (Enumeration.LanguageUser.FRENCH.getKey().equals(lang)) {
            message = "ce champs est obligatoire";
        }

        return message;
    }

    public EbUser getConnectedUser() {
        return connectedUser;
    }

    public void setConnectedUser(EbUser connectedUser) {
        this.connectedUser = connectedUser;
    }
}
