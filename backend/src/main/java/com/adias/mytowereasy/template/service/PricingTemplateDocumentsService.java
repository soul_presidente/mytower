package com.adias.mytowereasy.template.service;

import com.adias.mytowereasy.dto.TemplateGenParamsDTO;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.template.model.PricingItemTPL;
import com.adias.mytowereasy.template.model.pricing.EbDemandeTPL;
import com.adias.mytowereasy.template.model.pricing.EbMarchandiseTPL;


public interface PricingTemplateDocumentsService {
    EbDemandeTPL getEbDemandeTPL(EbDemande demandele);

    PricingItemTPL generatePricingItemTPL(Integer demandeNum, EbUser connectedUser) throws Exception;

    EbMarchandiseTPL getEbMarchandiseTPL(EbMarchandise marchandise);

    boolean checkedInformationMandatory(TemplateGenParamsDTO params) throws Exception;
}
