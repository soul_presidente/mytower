package com.adias.mytowereasy.template.model;

import com.adias.mytowereasy.util.GenericEnum;


public enum TemplateModels implements GenericEnum {
    PricingItem(0, "PricingItem"), Order(1, "Order"), Delivery(2, "Delivery");

    private Integer code;
    private String key;

    TemplateModels(Integer code, String key) {
        this.code = code;
        this.key = key;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getKey() {
        return key;
    }
}
