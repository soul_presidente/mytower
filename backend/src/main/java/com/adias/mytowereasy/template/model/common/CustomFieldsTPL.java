package com.adias.mytowereasy.template.model.common;

import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class CustomFieldsTPL extends BaseDocumentTPL {
    private String name;
    private String value;
    private String label;

    public CustomFieldsTPL() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
