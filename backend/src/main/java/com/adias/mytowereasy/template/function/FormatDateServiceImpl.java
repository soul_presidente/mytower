package com.adias.mytowereasy.template.function;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.service.BeanUtil;
import com.adias.mytowereasy.service.ConnectedUserService;


@Service
public class FormatDateServiceImpl implements FormatDateService {
    private ConnectedUserService connectedUserService;

    private EbUser connectedUser;

    public FormatDateServiceImpl() {
        connectedUserService = BeanUtil.getBean(ConnectedUserService.class);
    }

    @Override
    public String formatDate(Date dateToFormat) {
        EbUser connectedUser = this.connectedUser == null ? connectedUserService.getCurrentUser() : this.connectedUser;

        String lang = connectedUser.getLanguage();

        if (Enumeration.LanguageUser.FRENCH.getKey().equals(lang)) {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");

            if (dateToFormat != null) {
                return df.format(dateToFormat);
            }
            else {
                return "";
            }

        }
        else {
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

            if (dateToFormat != null) {
                return df.format(dateToFormat);
            }
            else {
                return "";
            }

        }

    }

    public String formatDateAndTime(Date dateToFormat) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        String lang = connectedUser.getLanguage();

        if (Enumeration.LanguageUser.FRENCH.getKey().equals(lang)) {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

            if (dateToFormat != null) {
                return df.format(dateToFormat);
            }
            else {
                return "";
            }

        }
        else {
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");

            if (dateToFormat != null) {
                return df.format(dateToFormat);
            }
            else {
                return "";
            }

        }

    }

    public String formatTime(Date dateToFormat) {
        DateFormat df = new SimpleDateFormat("hh:mm:ss");

        if (dateToFormat != null) {
            return df.format(dateToFormat);
        }
        else {
            return "";
        }

    }

    public EbUser getConnectedUser() {
        return connectedUser;
    }

    public void setConnectedUser(EbUser connectedUser) {
        this.connectedUser = connectedUser;
    }
}
