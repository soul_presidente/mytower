package com.adias.mytowereasy.template.model;

import java.util.Date;

import com.adias.mytowereasy.template.model.common.EbUserTPL;
import com.adias.mytowereasy.template.model.delivery.EbLivraisonTPL;
import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class DeliveryRootTPL extends BaseDocumentTPL {
    private EbLivraisonTPL delivery;
    private EbUserTPL connectedUser;
    private Date dateNow;

    public DeliveryRootTPL() {
        // Empty constructor for template processor
    }

    public EbLivraisonTPL getDelivery() {
        return delivery;
    }

    public void setDelivery(EbLivraisonTPL delivery) {
        this.delivery = delivery;
    }

    public EbUserTPL getConnectedUser() {
        return connectedUser;
    }

    public void setConnectedUser(EbUserTPL connectedUser) {
        this.connectedUser = connectedUser;
    }

    public Date getDateNow() {
        return dateNow;
    }

    public void setDateNow(Date dateNow) {
        this.dateNow = dateNow;
    }
}
