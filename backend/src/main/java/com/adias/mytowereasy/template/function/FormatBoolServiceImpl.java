package com.adias.mytowereasy.template.function;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.service.BeanUtil;
import com.adias.mytowereasy.service.ConnectedUserService;


@Service
public class FormatBoolServiceImpl implements FormatBoolService {
    private ConnectedUserService connectedUserService;

    private EbUser connectedUser;

    public FormatBoolServiceImpl() {
        connectedUserService = BeanUtil.getBean(ConnectedUserService.class);
    }

    @Override
    public String formatBool(Boolean value) {
        if (value == null) return "";

        EbUser connectedUser = this.connectedUser == null ? connectedUserService.getCurrentUser() : this.connectedUser;

        String lang = connectedUser.getLanguage();

        if (Enumeration.LanguageUser.FRENCH.getKey().equals(lang)) {

            if (value) {
                return "Oui";
            }
            else {
                return "Non";
            }

        }
        else if (Enumeration.LanguageUser.ENGLISH.getKey().equals(lang)) {

            if (value) {
                return "Yes";
            }
            else {
                return "No";
            }

        }
        else {
            return "";
        }

    }

    public EbUser getConnectedUser() {
        return connectedUser;
    }

    public void setConnectedUser(EbUser connectedUser) {
        this.connectedUser = connectedUser;
    }
}
