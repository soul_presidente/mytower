package com.adias.mytowereasy.template.service;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import fr.mytower.lib.storage.api.StorageProvider;

import com.adias.mytowereasy.controller.FileUploadController;
import com.adias.mytowereasy.dto.EbTemplateDocumentsDTO;
import com.adias.mytowereasy.dto.TemplateGenParamsDTO;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.EbTemplateDocuments;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.UploadDirectory;
import com.adias.mytowereasy.model.Statiques;
import com.adias.mytowereasy.repository.EbTypeTransportRepository;
import com.adias.mytowereasy.repository.ExEbDemandeTransporteurRepository;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.ServiceConfiguration;
import com.adias.mytowereasy.service.storage.StorageService;
import com.adias.mytowereasy.template.model.EmptyTPL;
import com.adias.mytowereasy.template.model.TemplateModels;
import com.adias.mytowereasy.util.document.BaseDocumentTPL;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class TemplateDocumentsServiceImpl extends MyTowerService implements TemplateDocumentsService {
    @Autowired
    ServiceConfiguration serviceConfiguration;

    @Autowired
    StorageService storageService;

    @Autowired
    FileUploadController fileUploadController;

    @Autowired
    ExEbDemandeTransporteurRepository demandeTransporteurRepository;

    @Autowired
    EbTypeTransportRepository ebTypeTransportRepository;

    @Autowired
    PricingTemplateDocumentsService pricingTemplateDocumentsService;

    @Autowired
    CommonTemplateDocumentsService commonTemplateDocumentsService;

    @Autowired
    OrderTemplateDocumentsService orderTemplateDocumentsService;

    @Autowired
    DeliveryTemplateDocumentsService deliveryTemplateDocumentsService;

    @Autowired
    StorageProvider storageProvider;

    @Override
    public List<EbTemplateDocuments> searchTemplateDocuments(SearchCriteria criteria) {
        return daoTemplateDocuments.searchEbTemplateDocuments(criteria);
    }

    @Override
    public Long countListEbTemplateDocuments(SearchCriteria criteria) {
        return daoTemplateDocuments.countListEbTemplateDocuments(criteria);
    }

    @Override
    public EbTemplateDocumentsDTO saveEbTemplateDocuments(EbTemplateDocumentsDTO templateDocumentsDTO) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        return new EbTemplateDocumentsDTO(
            ebTemplateDocumentsRepository.save(new EbTemplateDocuments(templateDocumentsDTO, connectedUser)));
    }

    @Override
    public void deleteEbTemplateDocuments(Integer templateDocumentsNum) {
        ebTemplateDocumentsRepository.deleteById(templateDocumentsNum);
    }

    @Override
    public List<EbTemplateDocumentsDTO> convertEntitiesToDto(List<EbTemplateDocuments> entities) {
        List<EbTemplateDocumentsDTO> listDto = new ArrayList<>();
        entities.forEach(e -> listDto.add(new EbTemplateDocumentsDTO(e)));
        return listDto;
    }

    @Override
    public String uploadFile(MultipartFile file, Integer ebCompagnieNum) throws Exception {
        String cheminFile = null;
        boolean checkFileExtension = Statiques.checkFileExtentions(file, Arrays.asList("docx"));

        if (checkFileExtension) {

            try {
                cheminFile = (String) storageService.store(file, UploadDirectory.TEMPLATE.getCode(), ebCompagnieNum);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        else {
            throw new MyTowerException("Bad file extenxion");
        }

        return cheminFile;
    }

    @Override
    public Path templateGeneration(TemplateGenParamsDTO params, EbUser connectedUser) throws Exception {
        // Prepare document params
        final EbTemplateDocuments ebTemplate = ebTemplateDocumentsRepository
            .findById(params.getSelectedTemplate()).get();

        final Path destFile = Files.createTempFile(null, params.getSelectedFormat().intValue() == 0 ? ".pdf" : ".docx");

        try (InputStream templateFile = storageProvider.get(Paths.get(ebTemplate.getCheminDocument()))) {
            // Prepare model
            BaseDocumentTPL model = new EmptyTPL();

            if (TemplateModels.PricingItem.getCode().equals(params.getTemplateModelNum())) {
                model = pricingTemplateDocumentsService
                    .generatePricingItemTPL(Integer.parseInt(params.getTemplateModelParams().get(0)), connectedUser);
            }
            else if (TemplateModels.Order.getCode().equals(params.getTemplateModelNum())) {
                model = orderTemplateDocumentsService
                    .generateOrderRootTPL(Long.parseLong(params.getTemplateModelParams().get(0)));
            }
            else if (TemplateModels.Delivery.getCode().equals(params.getTemplateModelNum())) {
                model = deliveryTemplateDocumentsService
                    .generateDeliveryRootTPL(Long.parseLong(params.getTemplateModelParams().get(0)));
            }

            // Do generation
            if (params.getSelectedFormat().intValue() == 0) {
                docTemplateService.generateTemplatePdf(templateFile, model, destFile, connectedUser);
            }
            else {
                docTemplateService.generateTemplateDocx(templateFile, model, destFile, connectedUser);
            }
        }

        return destFile;
    }
}
