package com.adias.mytowereasy.template.service;

import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.template.model.OrderRootTPL;
import com.adias.mytowereasy.template.model.order.EbOrderLineTPL;
import com.adias.mytowereasy.template.model.order.EbOrderTPL;


public interface OrderTemplateDocumentsService {
    EbOrderLineTPL getEbOrderLineTPL(EbOrderLine model);

    EbOrderTPL getEbOrderTPL(EbOrder model);

    OrderRootTPL generateOrderRootTPL(Long orderNum) throws Exception;
}
