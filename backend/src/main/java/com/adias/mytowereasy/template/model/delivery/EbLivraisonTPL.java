package com.adias.mytowereasy.template.model.delivery;

import java.util.Date;
import java.util.List;

import com.adias.mytowereasy.template.model.common.EbPartyTPL;
import com.adias.mytowereasy.template.model.common.EbUserTPL;
import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class EbLivraisonTPL extends BaseDocumentTPL {
    private String deliveryRef;
    private String status;

    private EbUserTPL ownerUser;
    private EbPartyTPL partyOrigin;
    private EbPartyTPL partyDest;
    private EbPartyTPL partySoldTo;

    private Date dateOfGoodsAvailability;

    private String orderRef;
    private String customerOrderRef;
    private String transportRef;

    private List<EbLivraisonLineTPL> units;

    public EbLivraisonTPL() {
        // Empty constructor for template processor
    }

    public String getDeliveryRef() {
        return deliveryRef;
    }

    public void setDeliveryRef(String deliveryRef) {
        this.deliveryRef = deliveryRef;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public EbUserTPL getOwnerUser() {
        return ownerUser;
    }

    public void setOwnerUser(EbUserTPL ownerUser) {
        this.ownerUser = ownerUser;
    }

    public EbPartyTPL getPartyOrigin() {
        return partyOrigin;
    }

    public void setPartyOrigin(EbPartyTPL partyOrigin) {
        this.partyOrigin = partyOrigin;
    }

    public EbPartyTPL getPartyDest() {
        return partyDest;
    }

    public void setPartyDest(EbPartyTPL partyDest) {
        this.partyDest = partyDest;
    }

    public EbPartyTPL getPartySoldTo() {
        return partySoldTo;
    }

    public void setPartySoldTo(EbPartyTPL partySoldTo) {
        this.partySoldTo = partySoldTo;
    }

    public Date getDateOfGoodsAvailability() {
        return dateOfGoodsAvailability;
    }

    public void setDateOfGoodsAvailability(Date dateOfGoodsAvailability) {
        this.dateOfGoodsAvailability = dateOfGoodsAvailability;
    }

    public String getOrderRef() {
        return orderRef;
    }

    public void setOrderRef(String orderRef) {
        this.orderRef = orderRef;
    }

    public String getCustomerOrderRef() {
        return customerOrderRef;
    }

    public void setCustomerOrderRef(String customerOrderRef) {
        this.customerOrderRef = customerOrderRef;
    }

    public String getTransportRef() {
        return transportRef;
    }

    public void setTransportRef(String transportRef) {
        this.transportRef = transportRef;
    }

    public List<EbLivraisonLineTPL> getUnits() {
        return units;
    }

    public void setUnits(List<EbLivraisonLineTPL> units) {
        this.units = units;
    }
}
