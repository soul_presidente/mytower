package com.adias.mytowereasy.template.function;

public interface FormatBoolService {
    public String formatBool(Boolean value);
}
