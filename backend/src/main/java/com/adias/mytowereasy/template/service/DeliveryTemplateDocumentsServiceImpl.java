package com.adias.mytowereasy.template.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.template.model.DeliveryRootTPL;
import com.adias.mytowereasy.template.model.delivery.EbLivraisonLineTPL;
import com.adias.mytowereasy.template.model.delivery.EbLivraisonTPL;


@Service
public class DeliveryTemplateDocumentsServiceImpl implements DeliveryTemplateDocumentsService {
    @Autowired
    CommonTemplateDocumentsService commonTpl;

    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    EbLivraisonRepository ebLivraisonRepository;

    @Override
    public EbLivraisonLineTPL getEbLivraisonLineTPL(EbLivraisonLine model) {
        EbLivraisonLineTPL tpl = new EbLivraisonLineTPL();

        tpl.setItemNumber(model.getItemNumber() != null ? model.getItemNumber() : "");
        tpl.setItemName(model.getItemName() != null ? model.getItemName() : "");
        tpl.setQuantity(model.getQuantity() != null ? model.getQuantity() : 0);
        tpl.setPartNumber(model.getPartNumber() != null ? model.getPartNumber() : "");
        tpl.setSerialNumber(model.getSerialNumber() != null ? model.getSerialNumber() : "");

        return tpl;
    }

    @Override
    public EbLivraisonTPL getEbLivraisonTPL(EbLivraison model) {
        EbLivraisonTPL tpl = new EbLivraisonTPL();

        tpl.setDeliveryRef(model.getRefDelivery() != null ? model.getRefDelivery() : "");
        tpl.setStatus(model.getStatusDelivery().getKey());

        tpl.setOwnerUser(commonTpl.getEbUserTPL(model.getxEbOwnerOfTheRequest()));
        tpl.setPartyOrigin(commonTpl.getEbPartyTPL(model.getxEbPartyOrigin()));
        tpl.setPartyDest(commonTpl.getEbPartyTPL(model.getxEbPartyDestination()));
        tpl.setPartySoldTo(commonTpl.getEbPartyTPL(model.getxEbPartySale()));

        tpl.setDateOfGoodsAvailability(model.getDateOfGoodsAvailability());
        tpl.setOrderRef(model.getRefOrder() != null ? model.getRefOrder() : "");
        tpl.setCustomerOrderRef(model.getCustomerOrderReference() != null ? model.getCustomerOrderReference() : "");
        tpl.setTransportRef(model.getRefTransport() != null ? model.getRefTransport() : "");

        return tpl;
    }

    @Override
    public DeliveryRootTPL generateDeliveryRootTPL(Long deliveryNum) throws Exception {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        EbLivraison delivery = ebLivraisonRepository.findById(deliveryNum).get();

        // Generate Template
        DeliveryRootTPL tplModel = new DeliveryRootTPL();
        tplModel.setDateNow(new Date());
        tplModel.setDelivery(getEbLivraisonTPL(delivery));
        tplModel.setConnectedUser(commonTpl.getEbUserTPL(connectedUser));

        return tplModel;
    }
}
