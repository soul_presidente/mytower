package com.adias.mytowereasy.template.function;

import java.util.Date;


public interface FormatDateService {
    public String formatDate(Date dateToFormat);

    public String formatDateAndTime(Date dateToFormat);

    public String formatTime(Date dateToFormat);
}
