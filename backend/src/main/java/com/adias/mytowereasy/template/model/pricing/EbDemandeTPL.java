package com.adias.mytowereasy.template.model.pricing;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.adias.mytowereasy.template.model.common.CustomFieldsTPL;
import com.adias.mytowereasy.template.model.common.EbCategorieTPL;
import com.adias.mytowereasy.template.model.common.EbEtablissementTPL;
import com.adias.mytowereasy.template.model.common.EbPartyTPL;
import com.adias.mytowereasy.template.model.common.EbUserTPL;
import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class EbDemandeTPL extends BaseDocumentTPL {
    private String reference;
    private String city;
    private String typeDemande;
    private String modeTransport;
    private String incoterm;
    private String libelleOriginCountry;
    private String libelleDestCountry;
    private String temperatureTransport;
    private String temperatureStorage;
    private String temperatureStorageLibelle;
    private String codeAnnulation;
    private String aboutTrAwbBol;
    private String aboutTrMawb;
    private String aboutTrFlightVessel;
    private String typeRequest;
    private String customerReference;
    private Boolean insurance;
    private String insuranceValue;
    private String insuranceCurrencyCode;
    private String currencyCode;
    private Date availabilityDate;
    private Date dateOfArrival;

    private Double totalNbrParcel;
    private Double totalWeight;
    private Double totalVolume;
    private Double totalTaxableWeight;
    private String insuranceCurrency;
    private String costCenter;

    private List<EbMarchandiseTPL> units;
    private List<ExEbDemandeTransporteurTPL> quotations;

    private List<CustomFieldsTPL> customFieldsList;
    private Map<String, String> customFields;

    private List<EbCategorieTPL> categoriesList;
    private Map<String, String> categorieLabelValue;

    private String carrierName;
    private String commentChargeur;
    private String commentChargeurHtml;
    private String libelleTypeTransport;

    private EbUserTPL user;
    private EbPartyTPL partyOrigin;
    private EbPartyTPL partyDestination;
    private EbPartyTPL partyNotify;
    private EbEtablissementTPL etablissement;

    private String unitsReference;
    private String nature;
    private String masterObjectReference;
    private Date dateCreation;
    private String nameCreator;
    private String nameOwner;
    private String carrierEstablishment;
    private String carrierCompany;
    private String carrierEmail;
    private String carrierPhone;
    private String libelleLastPsl;
    private Date dateLastPsl;
    private Boolean flagTransportInformation;
    private Date pslStartDate;
    private Date pslEndDate;
    private Boolean confirmPickup;
    private Boolean confirmDelivery;
    private String status;
    private BigDecimal price;
    private Double totalUnitPrice;
    private Boolean hasDestInEurop;

    public EbDemandeTPL() {
        // Empty constructor for template processor
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTypeDemande() {
        return typeDemande;
    }

    public void setTypeDemande(String typeDemande) {
        this.typeDemande = typeDemande;
    }

    public String getModeTransport() {
        return modeTransport;
    }

    public void setModeTransport(String modeTransport) {
        this.modeTransport = modeTransport;
    }

    public String getIncoterm() {
        return incoterm;
    }

    public void setIncoterm(String incoterm) {
        this.incoterm = incoterm;
    }

    public String getLibelleOriginCountry() {
        return libelleOriginCountry;
    }

    public void setLibelleOriginCountry(String libelleOriginCountry) {
        this.libelleOriginCountry = libelleOriginCountry;
    }

    public String getLibelleDestCountry() {
        return libelleDestCountry;
    }

    public void setLibelleDestCountry(String libelleDestCountry) {
        this.libelleDestCountry = libelleDestCountry;
    }

    public String getTemperatureTransport() {
        return temperatureTransport;
    }

    public void setTemperatureTransport(String temperatureTransport) {
        this.temperatureTransport = temperatureTransport;
    }

    public String getTemperatureStorage() {
        return temperatureStorage;
    }

    public void setTemperatureStorage(String temperatureStorage) {
        this.temperatureStorage = temperatureStorage;
    }

    public String getCodeAnnulation() {
        return codeAnnulation;
    }

    public void setCodeAnnulation(String codeAnnulation) {
        this.codeAnnulation = codeAnnulation;
    }

    public String getAboutTrAwbBol() {
        return aboutTrAwbBol;
    }

    public void setAboutTrAwbBol(String aboutTrAwbBol) {
        this.aboutTrAwbBol = aboutTrAwbBol;
    }

    public String getAboutTrMawb() {
        return aboutTrMawb;
    }

    public void setAboutTrMawb(String aboutTrMawb) {
        this.aboutTrMawb = aboutTrMawb;
    }

    public String getAboutTrFlightVessel() {
        return aboutTrFlightVessel;
    }

    public void setAboutTrFlightVessel(String aboutTrFlightVessel) {
        this.aboutTrFlightVessel = aboutTrFlightVessel;
    }

    public List<EbMarchandiseTPL> getUnits() {
        return units;
    }

    public void setUnits(List<EbMarchandiseTPL> units) {
        this.units = units;
    }

    public String getTypeRequest() {
        return typeRequest;
    }

    public void setTypeRequest(String typeRequest) {
        this.typeRequest = typeRequest;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public Boolean getInsurance() {
        return insurance;
    }

    public void setInsurance(Boolean insurance) {
        this.insurance = insurance;
    }

    public String getInsuranceValue() {
        return insuranceValue;
    }

    public void setInsuranceValue(String insuranceValue) {
        this.insuranceValue = insuranceValue;
    }

    public Date getAvailabilityDate() {
        return availabilityDate;
    }

    public void setAvailabilityDate(Date availabilityDate) {
        this.availabilityDate = availabilityDate;
    }

    public Date getDateOfArrival() {
        return dateOfArrival;
    }

    public void setDateOfArrival(Date dateOfArrival) {
        this.dateOfArrival = dateOfArrival;
    }

    public String getInsuranceCurrencyCode() {
        return insuranceCurrencyCode;
    }

    public void setInsuranceCurrencyCode(String insuranceCurrencyCode) {
        this.insuranceCurrencyCode = insuranceCurrencyCode;
    }

    public Double getTotalNbrParcel() {
        return totalNbrParcel;
    }

    public void setTotalNbrParcel(Double totalNbrParcel) {
        this.totalNbrParcel = totalNbrParcel;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(Double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public Double getTotalTaxableWeight() {
        return totalTaxableWeight;
    }

    public void setTotalTaxableWeight(Double totalTaxableWeight) {
        this.totalTaxableWeight = totalTaxableWeight;
    }

    public List<EbCategorieTPL> getCategoriesList() {
        return categoriesList;
    }

    public void setCategoriesList(List<EbCategorieTPL> categoriesList) {
        this.categoriesList = categoriesList;
    }

    public Map<String, String> getCategorieLabelValue() {
        return categorieLabelValue;
    }

    public void setCategorieLabelValue(Map<String, String> categorieLabelValue) {
        this.categorieLabelValue = categorieLabelValue;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public List<CustomFieldsTPL> getCustomFieldsList() {
        return customFieldsList;
    }

    public void setCustomFieldsList(List<CustomFieldsTPL> customFieldsList) {
        this.customFieldsList = customFieldsList;
    }

    public Map<String, String> getCustomFields() {
        return customFields;
    }

    public void setCustomFields(Map<String, String> customFields) {
        this.customFields = customFields;
    }

    public String getCommentChargeur() {
        return commentChargeur;
    }

    public void setCommentChargeur(String commentChargeur) {
        this.commentChargeur = commentChargeur;
    }

    public String getCommentChargeurHtml() {
        return commentChargeurHtml;
    }

    public void setCommentChargeurHtml(String commentChargeurHtml) {
        this.commentChargeurHtml = commentChargeurHtml;
    }

    public String getInsuranceCurrency() {
        return insuranceCurrency;
    }

    public void setInsuranceCurrency(String insuranceCurrency) {
        this.insuranceCurrency = insuranceCurrency;
    }

    public List<ExEbDemandeTransporteurTPL> getQuotations() {
        return quotations;
    }

    public void setQuotations(List<ExEbDemandeTransporteurTPL> quotations) {
        this.quotations = quotations;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public EbUserTPL getUser() {
        return user;
    }

    public void setUser(EbUserTPL user) {
        this.user = user;
    }

    public EbPartyTPL getPartyOrigin() {
        return partyOrigin;
    }

    public void setPartyOrigin(EbPartyTPL partyOrigin) {
        this.partyOrigin = partyOrigin;
    }

    public EbPartyTPL getPartyDestination() {
        return partyDestination;
    }

    public void setPartyDestination(EbPartyTPL partyDestination) {
        this.partyDestination = partyDestination;
    }

    public EbPartyTPL getPartyNotify() {
        return partyNotify;
    }

    public void setPartyNotify(EbPartyTPL partyNotify) {
        this.partyNotify = partyNotify;
    }

    public EbEtablissementTPL getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(EbEtablissementTPL etablissement) {
        this.etablissement = etablissement;
    }

    public String getLibelleTypeTransport() {
        return libelleTypeTransport;
    }

    public void setLibelleTypeTransport(String libelleTypeTransport) {
        this.libelleTypeTransport = libelleTypeTransport;
    }

    public String getUnitsReference() {
        return unitsReference;
    }

    public void setUnitsReference(String unitsReference) {
        this.unitsReference = unitsReference;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getMasterObjectReference() {
        return masterObjectReference;
    }

    public void setMasterObjectReference(String masterObjectReference) {
        this.masterObjectReference = masterObjectReference;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getNameCreator() {
        return nameCreator;
    }

    public void setNameCreator(String nameCreator) {
        this.nameCreator = nameCreator;
    }

    public String getNameOwner() {
        return nameOwner;
    }

    public void setNameOwner(String nameOwner) {
        this.nameOwner = nameOwner;
    }

    public String getCarrierEstablishment() {
        return carrierEstablishment;
    }

    public void setCarrierEstablishment(String carrierEstablishment) {
        this.carrierEstablishment = carrierEstablishment;
    }

    public String getCarrierCompany() {
        return carrierCompany;
    }

    public void setCarrierCompany(String carrierCompany) {
        this.carrierCompany = carrierCompany;
    }

    public String getCarrierEmail() {
        return carrierEmail;
    }

    public void setCarrierEmail(String carrierEmail) {
        this.carrierEmail = carrierEmail;
    }

    public String getCarrierPhone() {
        return carrierPhone;
    }

    public void setCarrierPhone(String carrierPhone) {
        this.carrierPhone = carrierPhone;
    }

    public String getLibelleLastPsl() {
        return libelleLastPsl;
    }

    public void setLibelleLastPsl(String libelleLastPsl) {
        this.libelleLastPsl = libelleLastPsl;
    }

    public Date getDateLastPsl() {
        return dateLastPsl;
    }

    public void setDateLastPsl(Date dateLastPsl) {
        this.dateLastPsl = dateLastPsl;
    }

    public Boolean getFlagTransportInformation() {
        return flagTransportInformation;
    }

    public void setFlagTransportInformation(Boolean flagTransportInformation) {
        this.flagTransportInformation = flagTransportInformation;
    }

    public Date getPslStartDate() {
        return pslStartDate;
    }

    public void setPslStartDate(Date pslStartDate) {
        this.pslStartDate = pslStartDate;
    }

    public Date getPslEndDate() {
        return pslEndDate;
    }

    public void setPslEndDate(Date pslEndDate) {
        this.pslEndDate = pslEndDate;
    }

    public Boolean getConfirmPickup() {
        return confirmPickup;
    }

    public void setConfirmPickup(Boolean confirmPickup) {
        this.confirmPickup = confirmPickup;
    }

    public Boolean getConfirmDelivery() {
        return confirmDelivery;
    }

    public void setConfirmDelivery(Boolean confirmDelivery) {
        this.confirmDelivery = confirmDelivery;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getTemperatureStorageLibelle() {
        return temperatureStorageLibelle;
    }

    public void setTemperatureStorageLibelle(String temperatureStorageLibelle) {
        this.temperatureStorageLibelle = temperatureStorageLibelle;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Double getTotalUnitPrice() {
        return totalUnitPrice;
    }

    public void setTotalUnitPrice(Double totalUnitPrice) {
        this.totalUnitPrice = totalUnitPrice;
    }

    public Boolean getHasDestInEurop() {
        return hasDestInEurop;
    }

    public void setHasDestInEurop(Boolean hasDestInEurop) {
        this.hasDestInEurop = hasDestInEurop;
    }
}
