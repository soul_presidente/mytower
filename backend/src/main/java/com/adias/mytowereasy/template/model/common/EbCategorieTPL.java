package com.adias.mytowereasy.template.model.common;

import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class EbCategorieTPL extends BaseDocumentTPL {
    private String libelle;
    private String code;

    public EbCategorieTPL() {
        // Empty constructor for template processor
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
