package com.adias.mytowereasy.template.model.pricing;

import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class EbMarchandiseTPL extends BaseDocumentTPL {
    private String originCountryName;
    private Double netWeight;
    private Double weight;
    private Double length;
    private Double width;
    private Double heigth;
    private String dg;
    private String dryIce;
    private String classGood;
    private String un;
    private String equipment;
    private String sensitive;
    private String typeMarchandise;
    private String unitReference;
    private String customerReference;
    private String quantityDryIce;
    private Integer numberOfUnits;
    private Double volume;
    private String dangerousGood;
    private String packaging;
    private Boolean stackable;
    private String comment;
    private String nomArticle;
    private String lot;
    private String sscc;
    private String olpn;
    private String trackingNumber;
    private String etat;
    private String pood;
    private String typeContainer;
    private String handlingUnit;
    private Integer huLineNumber;
    private String lotQuantite;
    private String typeOfUnit;
    private String hsCode;
    private Double unitPrice;
    private Double price;
    // champ utilise pour le calcul de prix net (= prix unitaire * number of
    // unit)
    private Double priceNetCalcul;

    public EbMarchandiseTPL() {
        // Empty constructor for template processor
    }

    public String getOriginCountryName() {
        return originCountryName;
    }

    public void setOriginCountryName(String originCountryName) {
        this.originCountryName = originCountryName;
    }

    public Double getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Double netWeight) {
        this.netWeight = netWeight;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeigth() {
        return heigth;
    }

    public void setHeigth(Double heigth) {
        this.heigth = heigth;
    }

    public String getDg() {
        return dg;
    }

    public void setDg(String dg) {
        this.dg = dg;
    }

    public String getDryIce() {
        return dryIce;
    }

    public void setDryIce(String dryIce) {
        this.dryIce = dryIce;
    }

    public String getClassGood() {
        return classGood;
    }

    public void setClassGood(String classGood) {
        this.classGood = classGood;
    }

    public String getUn() {
        return un;
    }

    public void setUn(String un) {
        this.un = un;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getSensitive() {
        return sensitive;
    }

    public void setSensitive(String sensitive) {
        this.sensitive = sensitive;
    }

    public String getTypeMarchandise() {
        return typeMarchandise;
    }

    public void setTypeMarchandise(String typeMarchandise) {
        this.typeMarchandise = typeMarchandise;
    }

    public String getUnitReference() {
        return unitReference;
    }

    public void setUnitReference(String unitReference) {
        this.unitReference = unitReference;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getQuantityDryIce() {
        return quantityDryIce;
    }

    public void setQuantityDryIce(String quantityDryIce) {
        this.quantityDryIce = quantityDryIce;
    }

    public Integer getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(Integer numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getDangerousGood() {
        return dangerousGood;
    }

    public void setDangerousGood(String dangerousGood) {
        this.dangerousGood = dangerousGood;
    }

    public String getPackaging() {
        return packaging;
    }

    public void setPackaging(String packaging) {
        this.packaging = packaging;
    }

    public Boolean getStackable() {
        return stackable;
    }

    public void setStackable(Boolean stackable) {
        this.stackable = stackable;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getNomArticle() {
        return nomArticle;
    }

    public void setNomArticle(String nomArticle) {
        this.nomArticle = nomArticle;
    }

    public String getLot() {
        return lot;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public String getSscc() {
        return sscc;
    }

    public void setSscc(String sscc) {
        this.sscc = sscc;
    }

    public String getOlpn() {
        return olpn;
    }

    public void setOlpn(String olpn) {
        this.olpn = olpn;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public String getPood() {
        return pood;
    }

    public void setPood(String pood) {
        this.pood = pood;
    }

    public String getTypeContainer() {
        return typeContainer;
    }

    public void setTypeContainer(String typeContainer) {
        this.typeContainer = typeContainer;
    }

    public String getHandlingUnit() {
        return handlingUnit;
    }

    public void setHandlingUnit(String handlingUnit) {
        this.handlingUnit = handlingUnit;
    }

    public Integer getHuLineNumber() {
        return huLineNumber;
    }

    public void setHuLineNumber(Integer huLineNumber) {
        this.huLineNumber = huLineNumber;
    }

    public String getLotQuantite() {
        return lotQuantite;
    }

    public void setLotQuantite(String lotQuantite) {
        this.lotQuantite = lotQuantite;
    }

    public String getTypeOfUnit() {
        return typeOfUnit;
    }

    public void setTypeOfUnit(String typeOfUnit) {
        this.typeOfUnit = typeOfUnit;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPriceNetCalcul() {
        return priceNetCalcul;
    }

    public void setPriceNetCalcul(Double priceNetCalcul) {
        this.priceNetCalcul = priceNetCalcul;
    }
}
