package com.adias.mytowereasy.template.model.common;

import com.adias.mytowereasy.util.document.BaseDocumentTPL;


public class EbEtablissementTPL extends BaseDocumentTPL {
    private Integer ebEtablissementNum;
    private String typeEtablissement;
    private String siret;
    private String nom;
    private String email;

    public EbEtablissementTPL() {
        // Empty constructor for template processor
    }

    public Integer getEbEtablissementNum() {
        return ebEtablissementNum;
    }

    public void setEbEtablissementNum(Integer ebEtablissementNum) {
        this.ebEtablissementNum = ebEtablissementNum;
    }

    public String getTypeEtablissement() {
        return typeEtablissement;
    }

    public void setTypeEtablissement(String typeEtablissement) {
        this.typeEtablissement = typeEtablissement;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
