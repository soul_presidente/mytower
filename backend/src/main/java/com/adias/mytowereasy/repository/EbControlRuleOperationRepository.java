package com.adias.mytowereasy.repository;

import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbControlRuleOperation;


@Repository
public interface EbControlRuleOperationRepository extends CommonRepository<EbControlRuleOperation, Integer> {
    public Integer deleteByXEbControlRule_ebControlRuleNum(Integer ebControlRuleNum);
}
