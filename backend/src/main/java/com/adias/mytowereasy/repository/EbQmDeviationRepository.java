package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.qm.EbQmDeviation;


@Repository
public interface EbQmDeviationRepository extends JpaRepository<EbQmDeviation, Integer> {
    @Query("select count (rc) from EbQmDeviation rc")
    int countdeviation();

    @Transactional
    @Modifying
    @Query("DELETE FROM EbQmDeviation qd WHERE qd.xEbTtTracing.ebTtTracingNum IN (?1)")
    public Integer deleteByEbTracingNum(List<Integer> listTracingNum);

    @Query("SELECT qd FROM EbQmDeviation as qd  WHERE qd.deleted = false or qd.deleted is null")
    public List<EbQmDeviation> findByDeletedIsNotTrue();

    @Transactional
    @Modifying
    @Query("update EbQmDeviation qd set qd.deleted = :deleted where qd.ebQmDeviationNum = :ebQmDeviationNum")
    public int updateDeleted(@Param("ebQmDeviationNum") Integer ebQmDeviationNum, @Param("deleted") Boolean deleted);
}
