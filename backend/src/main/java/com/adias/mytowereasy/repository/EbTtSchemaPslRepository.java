/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;


public interface EbTtSchemaPslRepository extends JpaRepository<EbTtSchemaPsl, Integer> {
    public EbTtSchemaPsl findOneByEbTtSchemaPslNum(Integer ebIncotermPslNum);

    public EbTtSchemaPsl findOneByDesignation(String designation);

    public List<EbTtSchemaPsl> findByXEbCompagnieIsNull();

    public List<EbTtSchemaPsl> findByXEbCompagnieIsNullAndListPslIsNull();

    public List<EbTtSchemaPsl> findByXEbCompagnie_ebCompagnieNum(Integer ebCompagnieNum);

    public EbTtSchemaPsl
        findFirstByXEbCompagnie_ebCompagnieNumAndDesignation(Integer ebCompagnieNum, String designation);

    @Query("SELECT cmp.ebCompagnieNum FROM EbTtSchemaPsl sch LEFT JOIN sch.xEbCompagnie cmp WHERE cmp.ebCompagnieNum IN (?1)")
    public List<Integer> selectListCompagnieNumByXEbCompagnieNumIn(List<Integer> listEbCompagnieNum);

    @Query("SELECT sch FROM EbTtSchemaPsl sch WHERE sch.xEbCompagnie.ebCompagnieNum IN (?1)")
    List<EbTtSchemaPsl> findByListEbCompagnieNum(List<Integer> listEbCompagnieNum);

    @Query(
        value = "select * from work.eb_tt_schema_psl as sch cross join lateral jsonb_array_elements(sch.list_psl) as list(psl) "
            + "where x_eb_compagnie=?1 and (list.psl ->> 'ebTtCompanyPslNum') \\:\\: int =?2 ",
        nativeQuery = true)
    List<EbTtSchemaPsl> selectByEbCompagnieNumAndPslNum(Integer ebCompagnieNum, Integer ebTtCompanyPslNum);
}
