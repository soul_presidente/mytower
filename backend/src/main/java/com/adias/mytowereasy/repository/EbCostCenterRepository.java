/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbCostCenter;


@Repository
public interface EbCostCenterRepository extends JpaRepository<EbCostCenter, Integer> {
    public List<EbCostCenter> findByCompagnieEbCompagnieNum(Integer ebCompagnieNum);

    public EbCostCenter findFirstByCompagnie_ebCompagnieNumAndLibelle(Integer ebCompagnieNum, String libelle);
}
