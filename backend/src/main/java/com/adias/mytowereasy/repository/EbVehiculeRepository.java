package com.adias.mytowereasy.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.adias.mytowereasy.model.EbVehicule;


public interface EbVehiculeRepository extends JpaRepository<EbVehicule, Integer> {
    List<EbVehicule> findByXEbCompagnie(Integer compagnieNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbVehicule v WHERE v.ebVehiculeNum = ?1")
    Integer deleteVehicule(Integer vehiculeNum);
}
