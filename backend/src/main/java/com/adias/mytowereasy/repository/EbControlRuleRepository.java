package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbControlRule;


public interface EbControlRuleRepository extends CommonRepository<EbControlRule, Integer> {
    @EntityGraph(attributePaths = {
        "xEbTypeRequest", "xEbTypeFlux", "xEbSchemaPsl", "listRuleOperations"
    })
    public EbControlRule getOneByEbControlRuleNum(Integer ebControlRuleNum);

    @EntityGraph(attributePaths = {
        "xEbTypeRequest", "xEbTypeFlux", "xEbSchemaPsl", "listRuleOperations"
    })
    public List<EbControlRule> getByXEbCompagnie_ebCompagnieNumAndActivated(Integer ebCompagnieNum, Boolean activated);

    @EntityGraph(attributePaths = {
        "xEbTypeRequest", "xEbTypeFlux", "xEbSchemaPsl", "listRuleOperations"
    })
    public List<EbControlRule> getByXEbCompagnie_ebCompagnieNumAndRuleCategoryAndActivated(
        Integer ebCompagnieNum,
        Integer ruleCategory,
        Boolean activated);

    @EntityGraph(attributePaths = {
        "xEbTypeRequest", "xEbTypeFlux", "xEbSchemaPsl", "listRuleOperations"
    })
    public List<EbControlRule> getByXEbCompagnie_ebCompagnieNumAndRuleCategoryAndActivatedAndSendEmailAlert(
        Integer ebCompagnieNum,
        Integer ruleCategory,
        Boolean activated,
        Boolean sendEmailAlert);

    @EntityGraph(attributePaths = {
        "xEbTypeRequest", "xEbTypeFlux", "xEbSchemaPsl", "listRuleOperations", "xEbCompagnie"
    })
    public List<EbControlRule> findAll();

    @Transactional
    @Modifying
    @Query("update EbControlRule c set c.activated = :activated where c.ebControlRuleNum = :controlRuleNum")
    public int
        updateDeactivated(@Param("controlRuleNum") Integer controlRuleNum, @Param("activated") Boolean activated);

    public Integer deleteByEbControlRuleNum(Integer ebControlRuleNum);

    @EntityGraph(attributePaths = {
        "xEbTypeRequest", "xEbTypeFlux", "xEbSchemaPsl", "listRuleOperations"
    })
    public List<EbControlRule>
        getByXEbCompagnie_ebCompagnieNumAndRuleCategoryAndActivatedAndSendEmailAlertAndSelectedMailNumsContaining(
            Integer ebCompagnieNum,
            Integer ruleCategory,
            Boolean activated,
            Boolean sendEmailAlert,
            String emailId);

    @Query("SELECT c.ebControlRuleNum from EbControlRule c where c.xEbCompagnie.ebCompagnieNum = ?1 and c.sendEmailAlert = true and c.selectedMailNums like concat('%',?2,'%')")
    public List<Integer> selectControlRuleWithEmailId(Integer ebCompagnieNum, String emailId);
}
