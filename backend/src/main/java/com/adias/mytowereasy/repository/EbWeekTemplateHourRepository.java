package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.chanel.model.EbWeekTemplateHour;


public interface EbWeekTemplateHourRepository extends CommonRepository<EbWeekTemplateHour, Integer> {
    @Transactional
    @Modifying
    @Query("DELETE EbWeekTemplateHour b WHERE b.day.ebWeektemplateDayNum  in :daysId ")
    void deleteByEbWeekTempleDayList(@Param("daysId") List<Integer> daysId);
}
