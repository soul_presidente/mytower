package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbTypeGoods;


@Repository
public interface EbTypeGoodsRepository extends CommonRepository<EbTypeGoods, Integer> {
    @Query("select t.code from EbTypeGoods t where t.xEbCompany.ebCompagnieNum = ?1 and t.deleted != true")
    List<String> listCodeForCompany(Integer ebCompagnieNum);
}
