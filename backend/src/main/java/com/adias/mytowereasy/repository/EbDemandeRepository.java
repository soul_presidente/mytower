/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;


public interface EbDemandeRepository extends CommonRepository<EbDemande, Integer> {
    public List<EbDemande> findAllByOrderByEbDemandeNumAsc();

    @EntityGraph(attributePaths = {
        "ebPartyOrigin", "ebPartyDest", "ebPartyNotif"
    })
    public EbDemande findOneByEbDemandeNum(Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.listCategoryFlat = :listCategoryFlat, d.listCategories = :listCategories, d.listCategoriesStr = :listCategoriesStr where d.ebDemandeNum = :iddemande")
    public int updateCategoriesEbDemande(
        @Param("iddemande") Integer ebDemandeNum,
        @Param("listCategoryFlat") String listCategoryFlat,
        @Param("listCategories") List<EbCategorie> listCategories,
        @Param("listCategoriesStr") String listCategoriesStr);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEbSchemaPsl = :xEbSchemaPsl where d.ebDemandeNum = :iddemande")
    public int updateSchemaPslEbDemande(
        @Param("iddemande") Integer ebDemandeNum,
        @Param("xEbSchemaPsl") EbTtSchemaPsl xEbSchemaPsl);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.dateTraitementCPB = :dateTraitementCPB where d.ebDemandeNum = :iddemande")
    public int updateDateTraitementCPBEbDemande(
        @Param("iddemande") Integer ebDemandeNum,
        @Param("dateTraitementCPB") Date dateTraitementCPB);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEcStatut = :status, d.updateDate = now() where d.ebDemandeNum = :iddemande")
    public int updateStatusEbDemande(@Param("iddemande") Integer ebDemandeNum, @Param("status") Integer status);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.flagDocumentTransporteur = ?2 where d.ebDemandeNum = ?1")
    public int updateFlagDocumentTransporteur(Integer ebDemandeNum, Boolean flag);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.flagDocumentCustoms = ?2 where d.ebDemandeNum = ?1")
    public int updateFlagDocumentCustoms(Integer ebDemandeNum, Boolean flag);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.flagTransportInformation = ?2 where d.ebDemandeNum = ?1")
    public int updateFlagTransportInformation(Integer ebDemandeNum, Boolean flag);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.flagCustomsInformation = ?2 where d.ebDemandeNum = ?1")
    public int updateFlagCustomsInformation(Integer ebDemandeNum, Boolean flag);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.flagRequestCustomsBroker = ?2 where d.ebDemandeNum = ?1")
    public int updateFlagRequestCustomsBroker(Integer ebDemandeNum, Boolean flag);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.datePickup = ?2, d.xEcStatut = ?3 where d.ebDemandeNum = ?1")
    public int updatePickupDateAndStatus(Integer ebDemandeNum, Date datePickup, Integer status);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEcStatut = ?2, d.nomTransporteur = ?3, d.xEbTransporteur = ?4, d.xEbTransporteurEtablissementNum = ?5, d.confirmedFromTransptPlan=?6 where d.ebDemandeNum = ?1")
    public int updateStatusAndDetailsTransporteurFinal(
        Integer ebDemandeNum,
        Integer status,
        String nomTransporteur,
        Integer idTransporteur,
        Integer idTransporteurEtab,
        Boolean confirmedFromTransptPlan);

    @Query("SELECT ebPartyOrigin FROM EbDemande d WHERE d.ebDemandeNum = ?1")
    public EbParty selectEbPartyOriginByEbDemandeNum(Integer ebDemandeNum);

    @Query("SELECT ebPartyDest FROM EbDemande d WHERE d.ebDemandeNum = ?1")
    public EbParty selectEbPartyDestByEbDemandeNum(Integer ebDemandeNum);

    @Query("SELECT ebPartyNotif FROM EbDemande d WHERE d.ebDemandeNum = ?1")
    public EbParty selectEbPartyNotifByEbDemandeNum(Integer ebDemandeNum);

    @Modifying
    @Query("update EbDemande d set d.numAwbBol = ?1, d.flightVessel = ?2, d.xEbSchemaPsl = ?3, d.customsOffice = ?4, d.mawb = ?5, d.flagTransportInformation = ?6, d.xEbUserTransportInfoMaj = ?7, d.carrierUniqRefNum = ?8, d.carrierComment = ?9, d.updateDate = now() where d.ebDemandeNum = ?10")
    public int updateEbDemandeTransportInfos(
        String numAwbBol,
        String flightVessel,
        Object schemaPsl,
        String customsOffice,
        String mawb,
        Boolean flagTransportInformation,
        EbUser xEbUserTransportInfoMaj,
        String carrierUniqRefNum,
        String carrierComment,
        Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.flagEbtrackTrace = ?2 where d.ebDemandeNum = ?1")
    public int updateflagEbtrackTrace(Integer ebDemandeNum, Boolean flagEbtrackTrace);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.flagEbInvoice = ?2 where d.ebDemandeNum = ?1")
    public int updateFlagInvoice(Integer ebDemandeNum, Boolean flagEbInvoice);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.totalNbrParcel = ?2, d.totalVolume = ?3, d.totalTaxableWeight = ?4, d.totalWeight = ?5 where d.ebDemandeNum = ?1")
    void updateTotalParcelAndVolumeAndTaxableWeightAndWeight(
        Integer ebDemandeNum,
        Double totalNbrParcel,
        Double totalVolume,
        Double totalTaxableWeight,
        Double totalWeight);

    @Query("select d from EbDemande d where d.ebDemandeNum = ?1")
    EbDemande getDemande(Integer ebDemandeNum);

    @Query("SELECT new EbUser(u.ebUserNum, et.ebEtablissementNum, cmp.ebCompagnieNum) FROM EbDemande d LEFT JOIN d.xEbUserCt AS u LEFT JOIN d.xEbCompagnieCt AS cmp LEFT JOIN d.xEbEtablissementCt AS et WHERE d.ebDemandeNum = ?1")
    EbUser selectCtDataByEbDemandeNum(Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.datePickupTM = ?2, d.dateOfGoodsAvailability = ?3, d.finalDelivery = ?4, d.confirmPickup = ?5, d.confirmDelivery = ?6 where d.ebDemandeNum = ?1")
    int updateGoodsPickup(
        Integer ebDemandeNum,
        Date datePickupTM,
        Date dateOfGoodsAvailability,
        Date finalDelivery,
        Boolean confirmPickup,
        Boolean confirmDelivery);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.datePickupTM = ?2  where d.ebDemandeNum = ?1")
    public int updateDatePickup(Integer ebDemandeNum, Date datePickupTM);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.finalDelivery = ?2 where d.ebDemandeNum = ?1")
    public int updateDateDelivery(Integer ebDemandeNum, Date finalDelivery);

    @Query("SELECT new EbDemande(d.ebDemandeNum, d.listCategories) FROM EbDemande d WHERE d.ebDemandeNum = ?1")
    public EbDemande selectListCategoriesByEbDemandeNum(Integer ebDemandeNum);

    @Query("SELECT new EbDemande(d.listTypeDocuments, d.ebDemandeNum) FROM EbDemande d WHERE d.ebDemandeNum = ?1")
    public EbDemande selectListTypeDocumentsByEbDemandeNum(Integer ebDemandeNum);

    @Query("SELECT d  FROM EbDemande d  where d.xEcCancelled  is null and  d.xEcStatut >= 11  ")
    public List<EbDemande> selectAllDemandeFromTm();

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEcCancelled = ?2 where d.ebDemandeNum = ?1")
    int updateCancelStatusEbDemande(Integer ebDemandeNum, Integer xEcCancelled);

		@Transactional
		@Modifying
		@Query("update EbDemande d set d.xEcCancelled = ?2 where d.ebDemandeNum IN (?1)")
		int setCancelStatusByTrIds(List<Integer> ebDemandeNum, Integer xEcCancelled);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.listTypeDocuments = ?2 where d.ebDemandeNum = ?1")
    public int updateListTypeDocument(Integer ebDemandeNum, List<EbTypeDocuments> listtypeDoc);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEcCancelled = ?2, d.xEbDemandeGroup = ?3 where d.ebDemandeNum IN(?1)")
    public int updateCancelStatusAndGroupDemandeNumListEbDemande(
        List<Integer> listEbDemandeNum,
        Integer xEcCancelled,
        Integer xEbDemandeGroup);

    @Query("SELECT new EbCompagnie(d.xEbCompagnie.ebCompagnieNum) FROM EbDemande d WHERE d.ebDemandeNum = ?1")
    public EbCompagnie findEbCompagnieChargeurByEbDemandeNum(Integer ebTtTracingNum);

    @Query("SELECT distinct d.xEbEtablissement FROM EbDemande d WHERE d.xEbUserCt.ebUserNum = ?1")
    public List<EbEtablissement> selectEbEtablissementByEbUserCtNum(Integer ebUserCtNum);

    @Query("SELECT distinct d.xEbCompagnie FROM EbDemande d WHERE d.xEbUserCt.ebUserNum = ?1")
    public List<EbCompagnie> selectEbCompagnyByEbUserCtNum(Integer ebUserCtNum);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.dateLastPsl = ?2 where d.ebDemandeNum = ?1")
    public int updateDateLastPsl(Integer ebDemandeNum, Date dateLastPsl);

    @Transactional
    @Modifying
    @Query("update EbDemande set listPropRdvPk= ?2 ,listPropRdvDl=?3 where ebDemandeNum =?1")
    public Integer updatePropRdvEbDemande(Integer ebDemandeNum, String listPropRdvPk, String listPropRdvDl);

    @Query("SELECT d.xEbSchemaPsl FROM EbDemande d WHERE d.ebDemandeNum = ?1")
    public EbTtSchemaPsl selectSchemaPslByEbDemandeNum(Integer ebDemandeNum);

    @Query("SELECT new EbDemande(d.ebDemandeNum, d.xEcModeTransport, d.xEcIncotermLibelle, d.xEbSchemaPsl) FROM EbDemande d WHERE d.xEbCompagnie.ebCompagnieNum IN (?1)")
    public List<EbDemande> selectListDemandeSchemaPslByListDemandeNum(List<Integer> listCompagnie);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.codeAlphaPslCourant = ?2, d.libellePslCourant = ?3 where d.ebDemandeNum = ?1")
    public int updateEbDemandePslCourant(Integer ebDemandeNum, String codeAlphaPslCourant, String libellePslCourant);

    @Query("SELECT d.libelleOriginCity, d.libelleDestCity, po.zoneDesignation, pd.zoneDesignation FROM EbDemande d LEFT JOIN d.ebPartyOrigin po LEFT JOIN d.ebPartyDest pd WHERE d.xEbCompagnie.ebCompagnieNum = ?1 AND (d.libelleOriginCity is not null AND LENGTH(TRIM(d.libelleOriginCity)) > 0 OR d.libelleDestCity is not null AND LENGTH(TRIM(d.libelleDestCity)) > 0 OR d.ebPartyOrigin.zoneDesignation is not null AND LENGTH(TRIM(d.ebPartyOrigin.zoneDesignation)) > 0 OR d.ebPartyDest.zoneDesignation is not null AND LENGTH(TRIM(d.ebPartyDest.zoneDesignation)) > 0)")
    public List<Object[]> selectListGroupeFieldValuesByCompany(Integer ebCompagnieNum);

    @EntityGraph(value = "EbDemande.detail", type = EntityGraphType.LOAD)
    public List<EbDemande> findByXEbCompagnie_ebCompagnieNum(Integer ebCompagnieNum);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEbTypeRequest = ?3, d.typeRequestLibelle = ?4 , d.torDuration = ?5 , d.torType = ?6 where d.xEbCompagnie.ebCompagnieNum = ?1 and d.xEcTypeDemande = ?2 ")
    public int recoveryTypeOfRequestByEbCompanyNumAndTypeDemande(
        Integer ebCompagnieNum,
        Integer typeDemande,
        Integer ebTypeRequestNum,
        String typeRequestLibelle,
        Integer torDuration,
        Integer torType);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.codeAlphaPslCourant = ?2, d.libellePslCourant = ?3,d.xEcStatut = ?4, d.dateLastPsl = ?5 where d.ebDemandeNum = ?1")
    public int updateEbDemandePslCourantAndStatusAndDateLastPsl(
        Integer ebDemandeNum,
        String codeAlphaPslCourant,
        String libellePslCourant,
        Integer status,
        Date dateLastPsl);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.codeAlphaPslCourant = ?2, d.libellePslCourant = ?3,d.xEcStatut = ?4, d.updateDate = now() where d.ebDemandeNum = ?1")
    public int updateEbDemandePslCourantAndStatus(
        Integer ebDemandeNum,
        String codeAlphaPslCourant,
        String libellePslCourant,
        Integer status);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.askForTransportResponsibility = true where d.ebDemandeNum = ?1 ")
    public int confirmeAskFortrResponsibility(Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.provideTransport = true , d.askForTransportResponsibility = true where d.ebDemandeNum = ?1 ")
    public int confirmeProvideTransport(Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEbTypeFluxNum = :xEbTypeFluxNum, d.xEbIncotermNum = :xEbIncotermNum where d.ebDemandeNum = :demandeNum ")
    public int updateEbDemandeTypeOfFluxAndIncoterm(
        @Param("xEbTypeFluxNum") Integer xEbTypeFluxNum,
        @Param("xEbIncotermNum") Integer xEbIncotermNum,
        @Param("demandeNum") Integer demandeNum);

    @Query("SELECT d  FROM EbDemande d  where d.xEbCompagnie.ebCompagnieNum = :xEbCompanieNum")
    public List<EbDemande> selectAllDemandeByCompagnie(@Param("xEbCompanieNum") Integer xEbCompanieNum);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.nomTransporteur = ?2, d.xEbTransporteur = ?3, d.xEbTransporteurEtablissementNum = ?4 where d.ebDemandeNum = ?1")
    public int updateDetailsTransporteurFinal(
        Integer ebDemandeNum,
        String nomTransporteur,
        Integer idTransporteur,
        Integer idTransporteurEtab);

    @Query("SELECT d.listCustomFieldsFlat  FROM EbDemande d  where d.xEbCompagnie.ebCompagnieNum = ?1 and d.listCustomFieldsFlat like  '%'|| ?2 ||'%' ")
    public List<String> selectFieldsByCompagnieNumAndTerm(Integer xEbCompanieNum, String term);

    @Query("SELECT d.ebDemandeNum FROM EbDemande d WHERE d.refTransport = ?1")
    public Integer getEbDemandeNumByRefTransport(String refTransport);

    @Query("SELECT d FROM EbDemande d WHERE d.refTransport = ?1")
    public EbDemande getByRefTransport(String refTransport);

    @Query("SELECT d FROM EbDemande d WHERE d.mawb = ?1")
    public EbDemande getByMawb(String mawb);

    @Query("SELECT d FROM EbDemande d WHERE d.numAwbBol = ?1")
    public EbDemande getByNumAwbBol(String numAwbBol);

    @Query("SELECT d FROM EbDemande d WHERE d.flightVessel = ?1")
    public EbDemande getByFlightVessel(String flightVessel);

    @Query(
        value = "SELECT d.eb_demande_num FROM work.eb_demande d WHERE d.customer_reference = ?1 limit 1",
        nativeQuery = true)
    public Integer getEbDemandeNumByCustomerRef(String customerRef);

    @Query(value = "SELECT * FROM work.eb_demande d WHERE d.customer_reference = ?1 limit 1", nativeQuery = true)
    public EbDemande getByCustomerRef(String customerRef);

    @Query("SELECT COUNT(d.user)  FROM EbDemande d WHERE d.user in ?1 ")
    public Long checkIfUserExistOnEbDemande(EbUser id);

    @Transactional
    @Modifying
    @Query(
        value = "update work.eb_demande set x_eb_etablissement= ?2  " +
            "where x_ec_statut>= 11 " +
            "and regexp_split_to_array(?3, ',') && (regexp_split_to_array(list_labels,',')) " +
            "and x_eb_etablissement = ?1",
        nativeQuery = true)

    public int updateEbDemandeEtablissement(Integer oldEtablissement, Integer newEtablissement, String listLabels);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.isGrouping = :isGrouping where d.ebDemandeNum in :listNum")
    public int updateIsGroupingInEbDemandeByListEbDemandeNum(
        @Param("listNum") List<Integer> listDemandeNum,
        @Param("isGrouping") Boolean isGrouping);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.eligibleForConsolidation = :eligibleForConsolidation, d.cancelled = :cancelled where d.ebDemandeNum in :listNum")
    public int updateEligibleForConsolidation(
        @Param("listNum") List<Integer> listDemandeNum,
        @Param("eligibleForConsolidation") Boolean eligibleForConsolidation,
        @Param("cancelled") Boolean cancelled);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.isValorized = :isValorized, d.xEcStatut= :status where d.ebDemandeNum in :listNum")
    int updateIsValorizedInEbDemandeByListEbDemandeNum(
        @Param("listNum") List<Integer> listDemandeNum,
        @Param("isValorized") Boolean isValorized,
        @Param("status") Integer status);

	@Transactional
	@Modifying
	@Query("update EbDemande d set d.isValorized = :isValorized, d.xEcStatutGroupage= :status where d.ebDemandeNum in :listNum")
	int updateIsValorizedInGroupedTrByListTrNum(@Param("listNum") List<Integer> listDemandeNum,
			@Param("isValorized") Boolean isValorized, @Param("status") Integer status);

    @Query("SELECT d FROM EbDemande d  where (d.isValorized= :isValorized or d.isValorized is null) and d.xEcNature=:nature and ( d.xEcStatut BETWEEN 1 and  3 or ( d.xEcStatut >=4 and d.finalChoiceToBeCalculated=true ))")
    List<EbDemande>
        selectListEbDemandeNotValorized(@Param("isValorized") Boolean isValorized, @Param("nature") Integer nature);

    @Query("SELECT d FROM EbDemande d where (d.isValorized= :isValorized or d.isValorized is null) and d.xEcNature=:nature and d.xEcStatut=:xEcStatut")
    List<EbDemande> selectDraftEbDemandeNotValorized(
        @Param("isValorized") Boolean isValorized,
        @Param("nature") Integer nature,
        @Param("xEcStatut") Integer status);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEcStatutGroupage = :xEcStatutGroupage where d.ebDemandeNum in :listNum")
    int updatexEcStatutGroupageInEbDemandeByEbDemandeNum(
        @Param("listNum") List<Integer> listDemandeNum,
        @Param("xEcStatutGroupage") Integer xEcStatutGroupage);

    @Query("SELECT d.city FROM EbDemande d  where d.user.ebCompagnie.ebCompagnieNum= :ebCompagnieNum")
     List<String> selectListIncotermCityByUserebCompagnieNum(@Param("ebCompagnieNum") Integer ebCompagnieNum);

    @Modifying
    @Query("update EbDemande d set d.saving = :saving, d.convertedSaving = :convertedSaving, d.convertedPrice = :convertedPrice, d.xecCurrencyTrResultante = :currencyTrResultante where d.ebDemandeNum = :ebDemandeNum")
     void updateSavings(
        @Param("ebDemandeNum") Integer ebDemandeNum,
        @Param("saving") BigDecimal saving,
        @Param("convertedSaving") BigDecimal convertedSaving,
        @Param("convertedPrice") BigDecimal convertedPrice,
        @Param("currencyTrResultante") EcCurrency currencyTrResultante);

    @Query("Select d.ebDemandeNum from EbDemande d where d.refTransport = :refTransport")
     Integer findByRefTransport(@Param("refTransport") String refTransport);

    @Modifying
    @Query("update EbDemande d set d.isGrouping = true where d.ebDemandeNum = :ebDemandeNum")
     void updateGrouping(@Param("ebDemandeNum") Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("UPDATE EbDemande d set d.xEcStatut = :xEcStatut where d.ebDemandeNum in :demandeIds")
     int
        updateStatusInListDemande(@Param("xEcStatut") Integer xEcStatut, @Param("demandeIds") List<Integer> demandeIds);

    @Transactional
    @Modifying
    @Query("UPDATE EbDemande d set d.exportControlStatut = :exportControlStatut where d.ebDemandeNum in :demandeIds")
     int updateExportControlStatut(
        @Param("exportControlStatut") Integer exportControlStatut,
        @Param("demandeIds") List<Integer> demandeIds);

    @Query("SELECT new EbDemande(d.ebDemandeNum) FROM EbDemande d ")
     List<EbDemande> selectAllEbDemande();

    @Query("SELECT new EbDemande(d.ebDemandeNum) FROM EbDemande d   where d.xEbCompagnie.ebCompagnieNum = :ebCompagnieNum ")
     List<EbDemande> selectAllEbDemandeByCompany(@Param("ebCompagnieNum") Integer ebCompagnieNum);

    @Query("SELECT distinct d.numAwbBol from EbDemande d where d.numAwbBol like %:term% ")
     List<String> selectListNumAwbBolFromDemandes(@Param("term") String term);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEbDemandeGroup = ?2 where d.ebDemandeNum IN(?1)")
     int updateGroupDemandeNumListEbDemande(List<Integer> listEbDemandeNum, Integer xEbDemandeGroup);

    @Query("SELECT d.ebDemandeNum FROM EbDemande d where d.xEbDemandeGroup = :ebTrResultanteNum ")
    List<Integer> selectDemandeNumInGroup(@Param("ebTrResultanteNum") Integer ebTrResultanteNum);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEcStatut = ?2, d.xEcStatutGroupage = ?3, d.customFields = ?4 where d.ebDemandeNum = ?1")
     int updateStatusEbDemandeAndGroupageStatut(
        Integer ebDemandeNum,
        Integer xEcStatut,
        Integer groupageStatus,
        String customFields);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEcStatut = ?2, d.xEcCancelled = ?3, d.customFields = ?4, d.updateDate = ?5  where d.ebDemandeNum = ?1")
     int updateStatusEbDemandeAndCancelledAndCustomFields(
        Integer ebDemandeNum,
        Integer xEcStatut,
        Integer cancelled,
        String customFields,
        Date updateDate
        );

    @Query("SELECT d.ebDemandeNum FROM EbDemande d WHERE d.customerReference = ?1")
     Integer getListEbDemandeNumByCustomerRef(String customerRef);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEbTransporteurEtablissementNum = ?2, d.prixTransporteurFinal = ?3, d.updateDate = ?4 where d.ebDemandeNum = ?1")
    int updateTransporteurEtabPriceAndDate(
        Integer demandeNum,
        Integer xEbTransporteurEtablissementNum,
        BigDecimal prixTransporteurFinal,
        Date updateDate);

    @Query("Select d.xEbDemandeGroup from EbDemande d where d.ebDemandeNum = :ebDemandeNum")
    Integer findxEbDemandeGroupByInitialTrId(@Param("ebDemandeNum") Integer ebDemandeNum);


    @Query("SELECT d FROM EbDemande d where d.xEbDemandeGroup = :ebTrResultanteNum ")
    List<EbDemande> selectListDemandeInGroup(@Param("ebTrResultanteNum") Integer ebTrResultanteNum);

    @Query("SELECT count(d.ebDemandeNum) FROM EbDemande d where d.xEbDemandeGroup = :ebTrResultanteNum ")
    Integer countNumberOfTrInitialInResultanteTr(@Param("ebTrResultanteNum") Integer ebTrResultanteNum);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEbDemandeGroup = null  where d.ebDemandeNum = :idTr")
    void setInitialTrResultanteTrToNull(@Param("idTr") Integer idTr);

		@Transactional
		@Modifying
		@Query("update EbDemande d set d.xEbDemandeGroup = null  where d.ebDemandeNum in (?1)")
		void setListOfInitialTrResultanteTrToNull(List<Integer> ids);

    @Query("SELECT distinct ed.listFlag from EbDemande ed where ed.listFlag is not null and ed.listFlag <> '' and ed.xEbTransporteur = :ebUserNum")
    List<String> selectListFlagForCarrier(@Param("ebUserNum") Integer ebUserNum);

    @Query("SELECT distinct ed.listFlag from EbDemande ed LEFT JOIN ed.xEbUserCt AS u where ed.listFlag is not null and ed.listFlag <> '' and u.ebUserNum = :ebUserNum")
    List<String> selectListFlagForControlTower(@Param("ebUserNum") Integer ebUserNum);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.xEbDemandeGroup = :groupedTrId, d.eligibleForConsolidation = :eligibleForConsolidation  where d.ebDemandeNum = :idTr")
    void updateTrGroupAndConsolidationEligibilityInInitialTr(@Param("idTr") Integer idTr, @Param("groupedTrId") Integer groupedTrId, @Param("eligibleForConsolidation") boolean eligibleForConsolidation);

		@Query(
			"SELECT d FROM EbDemande d  where (d.isValorized is false or d.isValorized is null) and d.xEcNature=:nature and d.xEcStatut =:statut and xEcCancelled is null"
		)
	public List<EbDemande> findByNatureAndStatutAndIsNotValorisedAndNotCanceled(@Param("nature") Integer nature,
			@Param("statut") Integer statut);

	@Transactional
	@Modifying
	@Query("update EbDemande d set d.xEbDemandeGroup = null  where d.ebDemandeNum IN(?1)")
	void setxEbDemandeGroupInTrsUnitairesToNull(List<Integer> listIdsTrsUnitaires);

	@Query(
		value = "SELECT tr_result.eb_demande_num " +
			"FROM work.eb_demande AS tr_result " +
			"JOIN work.eb_demande AS tr_unitaire ON tr_result.eb_demande_num = tr_unitaire.x_eb_demande_group " +
			"WHERE tr_result.x_ec_nature =:nature AND tr_result.x_ec_statut =:statut " +
			"GROUP BY tr_result.eb_demande_num " +
			"HAVING count(tr_unitaire.x_eb_demande_group) =:totalTrs",
		nativeQuery = true
	)
	public List<Integer> getTrsGroupedWithOneTr(
		@Param("nature") Integer nature,
		@Param("statut") Integer statut,
		@Param("totalTrs") Integer totalTrs
	);

	@Query("SELECT d.ebDemandeNum from EbDemande d WHERE d.xEbDemandeGroup IN (?1)")
	List<Integer> getInitialIdsByGroupedIds(List<Integer> ids);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.tdcAcknowledge = true, d.tdcAcknowledgeDate = now() where d.ebDemandeNum = ?1")
    void updateTdcAcknowledgeAndDate(Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("update EbDemande d set d.customFields = ?2, d.listCustomFieldsFlat = ?3, d.listCustomFieldsValueFlat = ?4, d.updateDate = ?5 where d.ebDemandeNum = ?1")
    int updateCustomFields( Integer ebDemandeNum, String customFields, String listCustomFieldsFlat, String listCustomFieldsValueFlat, Date updateDate);

    boolean existsByCustomerReference(String customerReference);

    @Query(
        value = "select distinct CASE when (d.x_ec_statut >= ?2 and d.x_ec_cancelled  is null) then true else false END as is_confirmed from work.eb_demande d where d.eb_demande_num = ?1",
        nativeQuery = true)
    Boolean checkIfTrIsConfirmedAndNotCancelled(Integer id, Integer statut);
}
