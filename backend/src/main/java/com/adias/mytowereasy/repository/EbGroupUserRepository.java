package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adias.mytowereasy.model.EbUserGroup;


public interface EbGroupUserRepository extends JpaRepository<EbUserGroup, Long> {

}
