/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbCompagnieCurrency;


@Repository
public interface EbCompagnieCurrencyRepository extends CommonRepository<EbCompagnieCurrency, Integer> {
    @Query("SELECT c FROM EbCompagnieCurrency c WHERE  c.ebCompagnieCurrencyNum = ?1")
    public EbCompagnieCurrency findEbCompagnieCurrencyByEbCompagnieCurrencyNum(Integer ebCompagnieCurrencyNum);

    @Query("SELECT c FROM EbCompagnieCurrency c WHERE  c.ebCompagnie.ebCompagnieNum = ?1 and c.ebCompagnieGuest.ebCompagnieNum = ?2")
    public List<EbCompagnieCurrency> findAllByCompany(Integer ebCompagnieCurrencyNum, Integer ebCompagnieGuest);

		@Query("SELECT c FROM EbCompagnieCurrency c WHERE c.ecCurrency.code = ?1")
		EbCompagnieCurrency findEbCompagnieCurrencyByCurrencyCode(String currencyCode);

		@Query("SELECT c FROM EbCompagnieCurrency c WHERE c.ebCompagnieCurrencyNum = ?1")
		Optional<EbCompagnieCurrency> findEbCompagnieCurrencyById(
			Integer compagnieCurrencyId
		);

		@Query(
			"SELECT c FROM EbCompagnieCurrency c WHERE c.ecCurrency.code = ?1 and c.xecCurrencyCible.code = ?2 and c.ebCompagnieGuest.siren = ?3"
		)
		List<EbCompagnieCurrency> findCompagnieCurrencyWithOriginCurrencyCodeAndTargetCurrencyCodeAndCompagnieSiren(
			String originCurrencyCode,
			String targetCurrencyCode,
			String carrierCompanySiren
		);

		@Query(
			value = "SELECT eb_compagnie_currency_num FROM work.eb_compagnie_currency WHERE x_eb_compagnie = ?1 AND is_deleted = ?2",
			nativeQuery = true
		)
		public List<Integer> selectListCompagnieCurrencyByCompagnieIdAndIsDeleted(
			Integer ebCompagnieNum,
			boolean isDeleted
		);

		@Transactional
		@Modifying
		@Query(
			value = "UPDATE work.eb_compagnie_currency SET date_modification = ?1, date_fin = ?1, is_used = false, is_deleted = true WHERE eb_compagnie_currency_num IN (?2)",
			nativeQuery = true
		)
		public int deleteCompagnieCurrencyByCompagnieCurrencyIds(Date date, List<Integer> compagnieCurrencyIds);

		@Query(
			"SELECT c FROM EbCompagnieCurrency c WHERE c.ecCurrency.code = ?1 and c.xecCurrencyCible.code = ?2 and c.ebCompagnieGuest.siren = ?3 and c.isCreatedFromIHM = ?4"
		)
		Optional<EbCompagnieCurrency> findByOriginAndTargetAndCompanySirenAndIsCreatedFromIHM(
			String originCurrencyCode,
			String targetCurrencyCode,
			String carrierCompanySiren,
			boolean isCreatedFromIHM
		);
	}
