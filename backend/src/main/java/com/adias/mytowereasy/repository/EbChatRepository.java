/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adias.mytowereasy.model.EbChat;


public interface EbChatRepository extends CommonRepository<EbChat, Integer> {
    public List<EbChat> findAllByModuleAndIdFicheAndIdChatComponentOrderByDateCreationDesc(
        Integer module,
        Integer idFiche,
        Integer idChatComponent);

    @Query(
        value = "SELECT c FROM EbChat c LEFT JOIN c.chatOwner u WHERE u.role IN :role AND c.module =:module " +
            "AND c.idFiche =:idFiche AND c.idChatComponent =:idChatComponent")
    public List<EbChat> findByModuleAndIdFicheAndOwnerRole(
        @Param("role") List<Integer> role,
        @Param("module") Integer module,
        @Param("idFiche") Integer idFiche,
        @Param("idChatComponent") Integer idChatComponent);

    @Query(
        value = "SELECT c FROM EbChat c LEFT JOIN c.chatOwner u WHERE u.role IN :role AND c.module =:module " +
            "AND c.idFiche =:idFiche AND c.idChatComponent =:idChatComponent " +
            "AND c.dateCreation < :confirmedDate")
    public List<EbChat> findByModuleAndIdFicheAndOwnerRoleAndDate(
        @Param("role") List<Integer> role,
        @Param("module") Integer module,
        @Param("idFiche") Integer idFiche,
        @Param("idChatComponent") Integer idChatComponent,
        @Param("confirmedDate") Date dateDemandeConfirmed);

    @Query(
        value = "SELECT c FROM EbChat c WHERE c.chatOwner = null AND c.module =:module " +
            "AND c.idFiche =:idFiche AND c.idChatComponent =:idChatComponent")
    public List<EbChat> findByModuleAndIdFicheAndOwnerIsNull(
        @Param("module") Integer module,
        @Param("idFiche") Integer idFiche,
        @Param("idChatComponent") Integer idChatComponent);

    @Query(
        value = "SELECT c FROM EbChat c LEFT JOIN c.chatOwner u LEFT JOIN u.ebCompagnie ec " +
            "WHERE ec.ebCompagnieNum =:compagnieNum AND c.module =:module AND c.idFiche =:idFiche " +
            "AND c.idChatComponent =:idChatComponent")
    public List<EbChat> findByModuleAndIdFicheAndCompagnie(
        @Param("compagnieNum") Integer compagnieNum,
        @Param("module") Integer module,
        @Param("idFiche") Integer idFiche,
        @Param("idChatComponent") Integer idChatComponent);

    @Query(
        value = "SELECT c FROM EbChat c LEFT JOIN c.chatOwner u LEFT JOIN u.ebEtablissement et " +
            "WHERE et.ebEtablissementNum =:etablissementNum AND c.module =:module AND c.idFiche =:idFiche " +
            "AND c.idChatComponent =:idChatComponent")
    public List<EbChat> findByModuleAndIdFicheAndEtablissement(
        @Param("etablissementNum") Integer etablissementNum,
        @Param("module") Integer module,
        @Param("idFiche") Integer idFiche,
        @Param("idChatComponent") Integer idChatComponent);

    @Query(
        value = "SELECT c FROM EbChat c LEFT JOIN c.chatOwner u WHERE u.ebUserNum =:chatOwnerNum AND c.module =:module " +
            "AND c.idFiche =:idFiche AND c.idChatComponent =:idChatComponent")
    public List<EbChat> findByModuleAndIdFicheAndOwner(
        @Param("chatOwnerNum") Integer chatOwnerNum,
        @Param("module") Integer module,
        @Param("idFiche") Integer idFiche,
        @Param("idChatComponent") Integer idChatComponent);
}
