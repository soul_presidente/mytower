/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adias.mytowereasy.model.TtEnumeration;
import com.adias.mytowereasy.model.tt.EbTtEvent;


public interface EbTtEventRepository extends JpaRepository<EbTtEvent, Integer> {
    public List<EbTtEvent>
        findAllByXEbTtTracing_ebTtTracingNumAndTypeEvent(Integer ebTtTracingNum, TtEnumeration.TypeEvent typeEvent);

    @Query("select dateEvent FROM EbTtEvent evt WHERE evt.xEbTtTracing.ebTtTracingNum =?1 and  natureDate=?2")
    public List<Date> findAllByEbTtTracingAndNatureDate(Integer ebTracingNum, Integer natureDate);

    public List<EbTtEvent> findAllByXEbTtTracing_ebTtTracingNum(Integer ebTtTracingNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbTtEvent evt WHERE evt.xEbTtTracing.ebTtTracingNum IN (?1)")
    public Integer deleteListEbEventByEbTracingNumIn(List<Integer> listEbTracingNum);

    @Transactional
    @Modifying
    @Query("UPDATE EbTtEvent ev set ev.ebQmIncidentNum = :ebQmIncidentNum WHERE ev.ebTtEventNum = :ebTtEventNum")
    public int updateIncidentNumInEvent(
        @Param("ebQmIncidentNum") Integer ebQmIncidentNum,
        @Param("ebTtEventNum") Integer ebTtEventNum);
}
