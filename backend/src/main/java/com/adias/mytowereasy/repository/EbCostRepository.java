/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbCost;


public interface EbCostRepository extends JpaRepository<EbCost, Integer> {
    @Transactional
    @Modifying
    @Query("update EbCost d set d.euroExchangeRateReal = :euroExchangeRateReal where d.ebCostNum = :ebCostNum")
    public void updateEuroExchangeRateReal(
        @Param("ebCostNum") Integer ebDemandeNum,
        @Param("euroExchangeRateReal") Double euroExchangeRateReal);

    @Query("update EbCost e set e.deleted = true where e.ebCostNum = ?1")
    @Modifying
    @Transactional
    public int updateCostItemDeletedFlag(Integer ebCostNum);

    @Query("SELECT c FROM EbCost c WHERE  c.ebCostNum = ?1")
    public EbCost findEbCostByEbCostNum(Integer EbcostNum);

    public boolean existsByxEbCompagnie_ebCompagnieNumAndCodeIgnoreCase(Integer ebCompagnieNum, String code);
}
