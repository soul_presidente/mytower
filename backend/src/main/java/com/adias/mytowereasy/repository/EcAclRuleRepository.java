/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EcAclRule;


@Repository
public interface EcAclRuleRepository extends JpaRepository<EcAclRule, Integer> {
    @Query("from EcAclRule r where r.visible = true")
    public List<EcAclRule> findAllVisible();
}
