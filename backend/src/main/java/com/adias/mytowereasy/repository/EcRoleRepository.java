/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adias.mytowereasy.model.EcRole;


public interface EcRoleRepository extends JpaRepository<EcRole, Integer> {
    public List<EcRole> findByLibelleNot(String libelle);
}
