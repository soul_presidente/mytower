package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.dock.model.EbDkOpeningDay;
import com.adias.mytowereasy.model.EbRangeHour;


public interface EbRangeHoursRepository extends CommonRepository<EbRangeHour, Integer> {
    public List<EbRangeHour> findAllByOpeningDay(EbDkOpeningDay openingDay);

    @Transactional
    public void deleteByOpeningDay(EbDkOpeningDay openingDay);
}
