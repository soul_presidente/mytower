/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbCustomField;


@Repository
public interface EbCustomFieldRepository extends JpaRepository<EbCustomField, Integer> {
    public EbCustomField findFirstByXEbCompagnie(Integer ebCompagnieNum);

    @Query("SELECT new EbCustomField(c.ebCustomFieldNum, c.fields) FROM EbCustomField c where c.xEbCompagnie = :ebCompagnieNum")
    public EbCustomField getCustomFieldByXEbCompagnie(@Param("ebCompagnieNum") Integer ebCompagnieNum);
}
