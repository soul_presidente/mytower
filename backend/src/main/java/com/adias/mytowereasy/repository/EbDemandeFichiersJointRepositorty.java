/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbDemandeFichiersJoint;


public interface EbDemandeFichiersJointRepositorty extends CommonRepository<EbDemandeFichiersJoint, Integer> {
    @Transactional
    @Modifying
    @Query("DELETE EbDemandeFichiersJoint dfj WHERE dfj.ebDemandeFichierJointNum IN ?1")
    public Integer deleteFile(Integer ebFichierJointNum);

    @Transactional
    @Modifying
    @Query("UPDATE EbDemandeFichiersJoint dfj SET dfj.tag = ?2 WHERE dfj.ebDemandeFichierJointNum = ?1")
    public Integer setTag(Integer ebFichierJointNum, String tag);

    @Query("SELECT ebdfj from EbDemandeFichiersJoint ebdfj WHERE ebdfj.numEntity=?1 and ebdfj.module=?2")
    List<EbDemandeFichiersJoint> findAllByXEbQmIncidentAndModule(Integer numEntity, Integer module);

    @Query("SELECT ebdfj from EbDemandeFichiersJoint ebdfj WHERE ebdfj.numEntity=?1 and ebdfj.module=?2")
    List<EbDemandeFichiersJoint> findAllByInvoiceNumAndModule(Integer numEntity, Integer module);

    @Query("SELECT ebdfj from EbDemandeFichiersJoint ebdfj WHERE ebdfj.numEntity=?1 and ebdfj.module=?2")
    List<EbDemandeFichiersJoint> findAllByDeliveryNumAndModule(Integer numEntity, Integer module);

    @Query("SELECT max(ebDemandeFichierJointNum) FROM EbDemandeFichiersJoint")
    public Integer findMaxId();

    @Transactional
    @Modifying
    @Query("UPDATE EbDemandeFichiersJoint SET xEbDemande = ?2 WHERE ebDemandeFichierJointNum IN ?1")
    public void setDemande(List<Integer> listEbDemandeFichierJointNum, Integer ebDemandeNum);

    public EbDemandeFichiersJoint save(EbDemandeFichiersJoint FichierAEnvoyer);

    public List<EbDemandeFichiersJoint> findAllByIdCategorie(Integer type);

    public List<EbDemandeFichiersJoint> findAllByNumEntityIn(List<Integer> listEbDemandeNum);

    public List<EbDemandeFichiersJoint>
        findAllByNumEntityAndXEbEtablissemnt(Integer ebDemandeNum, Integer ebEtablissementNum);

    public List<EbDemandeFichiersJoint> findAllByNumEntityAndModule(Integer demande, Integer module);

    public List<EbDemandeFichiersJoint> findAllByNumEntityAndModuleIn(Integer demande, List<Integer> module);

    public List<EbDemandeFichiersJoint> findAllByNumEntity(Integer demande);

    @Query("SELECT f from EbDemandeFichiersJoint f where f.xEbCustomDeclaration = ?1 and f.xEbCompagnie = ?2")
    public List<EbDemandeFichiersJoint> findByDeclaration(Integer ebDeclarationNum, Integer ebCompagnieNum);
}
