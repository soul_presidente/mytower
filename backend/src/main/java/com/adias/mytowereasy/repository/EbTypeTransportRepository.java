/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adias.mytowereasy.airbus.model.QrGroupeObject;
import com.adias.mytowereasy.model.EbTypeTransport;


public interface EbTypeTransportRepository extends JpaRepository<EbTypeTransport, Integer> {
    @Query("select tt from EbTypeTransport tt where tt.libelle = :libelle and tt.xEcModeTransport = :modeTransport")
    EbTypeTransport findEbTypeTransportByLibeleAndModeTransport(
        @Param("libelle") String libeleTypeTransport,
        @Param("modeTransport") Integer modeTransport);

    @Query("select distinct tt.libelle from EbTypeTransport tt")
    List<String> findAllListLibelleTypeTransport();

    @Query("select tt.ebTypeTransportNum from EbTypeTransport tt where lower(tt.libelle) like (:libelle)")
    List<Integer> findAllTypeTransportByLibelle(@Param("libelle") String libeleTypeTransport);

    @Query("select tt from EbTypeTransport tt where tt.ebTypeTransportNum = :xEbTypeTransport")
    EbTypeTransport findEbTypeTransportByEbTypeTransportNum(@Param("xEbTypeTransport") Integer xEbTypeTransport);

    @Query("select new com.adias.mytowereasy.airbus.model.QrGroupeObject(tt.ebTypeTransportNum, tt.libelle)  from EbTypeTransport tt")
    public List<QrGroupeObject> findAllTypeTransportCodeLibelle();
}
