/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbSavedForm;


@Repository
public interface EbSavedFormRepository extends JpaRepository<EbSavedForm, Integer> {
    public List<EbSavedForm>
        findAllByEbCompNumAndEbUser_ebUserNumOrderByDateAjoutDesc(Integer ebCompNum, Integer ebUserNum);

    public List<EbSavedForm>
        findTop15ByEbCompNumAndEbUser_ebUserNumOrderByDateAjoutDesc(Integer ebCompNum, Integer ebUserNum);

    public List<EbSavedForm> findAllByEbCompNumAndCompagnieOrderByOrdreAsc(Integer ebCompNum, EbCompagnie compagnie);

    public EbSavedForm findTopByEbCompNumAndCompagnieOrderByOrdreDesc(Integer ebCompNum, EbCompagnie compagnie);

    public List<EbSavedForm> findAllByFormNameAndEbCompNumAndEbModuleNumAndEbUser_ebUserNum(
        String formName,
        Integer ebCompNum,
        Integer ebModuleNum,
        Integer ebUserNum);

    public List<EbSavedForm>
        findAllByEbCompNumAndCompagnie_ebCompagnieNumIn(Integer ebCompNum, List<Integer> listEbCompagnieNum);

    public List<EbSavedForm> findByEbUser_ebUserNum(Integer userNum);
}
