/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbDestinationCountry;


@Repository
public interface EbDestinationCountryRepository extends CommonRepository<EbDestinationCountry, Integer> {
    public List<EbDestinationCountry> findByEtablissement_ebEtablissementNum(Integer ebEtablissementNum);

    public List<EbDestinationCountry> findAllByEtablissement_EbCompagnie_EbCompagnieNum(Integer ebCompagnieNum);
}
