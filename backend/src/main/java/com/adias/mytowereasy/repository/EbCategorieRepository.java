/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbCategorie;


@Repository
public interface EbCategorieRepository extends JpaRepository<EbCategorie, Integer> {
    public List<EbCategorie> findByEtablissementEbEtablissementNum(Integer ebEtablissementNum);

    public List<EbCategorie> findByCompagnieEbCompagnieNum(Integer ebCompagnieNum);

    public EbCategorie findOneByCode(String code);

    @EntityGraph(attributePaths = "labels")
    public List<EbCategorie> findDistinctByEtablissementEbEtablissementNum(Integer ebEtablissementNum);

    @EntityGraph(attributePaths = "labels")
    public List<EbCategorie> findDistinctByEtablissementEbCompagnieEbCompagnieNum(Integer ebCompagnieNum);

    @Query(
        value = "SELECT * FROM work.ec_country  where UPPER(libelle) like CONCAT('%',UPPER(?1),'%')",
        nativeQuery = true)
    public List<EbCategorie> findAllByLibelleLike(String SearchTerm);

    @Query(
        value = "select eb_categorie_num from work.eb_categorie where x_eb_compagnie = ?1 and libelle in (?2)",
        nativeQuery = true)
    public List<Integer> getIdsByLibelle(Integer xEbCompagnie, String[] listLabelCategories);

    @Transactional
    @Modifying
    @Query(value = "delete from work.eb_categorie where x_eb_compagnie = ?1 and libelle in (?2)", nativeQuery = true)
    public void deleteAllLibelleLike(Integer xEbCompagnie, String[] listLabelCategories);
}
