/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.adias.mytowereasy.model.ExDemandeCommande;


// @Repository
public interface ExEbDemandeCommandeRepository {
    // extends CommonRepository<ExDemandeCommande, Integer> {
    public List<ExDemandeCommande> findAllByExDemandeCommandeId_xEbDemande_ebDemandeNum(Integer ebDemandeNum);

    @Query("SELECT c.ebCommandeNum FROM ExDemandeCommande dc INNER JOIN dc.exDemandeCommandeId.xEbCommande c INNER JOIN dc.exDemandeCommandeId.xEbDemande d WHERE d.ebDemandeNum = ?1")
    public Object[] selectListEbCommandeNumByEbDemandeNum(Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM ExDemandeCommande dc WHERE dc.exDemandeCommandeId.xEbDemande.ebDemandeNum = ?1")
    public Integer deleteListEbCommandeByEbDemandeNumIn(Integer ebDemandeNum);
}
