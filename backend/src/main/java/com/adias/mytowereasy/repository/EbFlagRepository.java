package com.adias.mytowereasy.repository;

import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbFlag;


@Repository
public interface EbFlagRepository extends CommonRepository<EbFlag, Integer> {
}
