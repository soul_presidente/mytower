/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbDatatableState;


public interface EbDatatableStateRepository extends CommonRepository<EbDatatableState, Integer> {
    @Transactional
    @Modifying
    @Query("update EbDatatableState dt set dt.state = ?2 where dt.ebDatatableStateNum = ?1")
    public int updateState(Integer ebDatatableStateNum, String state);

    public EbDatatableState findFirstByXEbUserNumAndDatatableCompId(Integer ebUserNum, Integer datatableCompId);
}
