/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import com.adias.mytowereasy.model.DemandeUserObservers;
import com.adias.mytowereasy.model.EbUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;


public interface DemandeObserversRepository extends JpaRepository<DemandeUserObservers,Integer>{
    Set<DemandeUserObservers> findByDemandeEbDemandeNum(Integer trId);

    @Transactional
    @Modifying
    @Query("DELETE FROM DemandeUserObservers obs WHERE obs.demande.ebDemandeNum IN (?1)")
    Integer deleteTrsObservers(List<Integer> trIds);

    @Query("select new EbUser(user.email,user.ebUserNum,user.language) from DemandeUserObservers obs left join EbUser user on obs.observer.ebUserNum = user.ebUserNum where obs.demande.ebDemandeNum in (?1)")
    List<EbUser> selectObservers(List<Integer> trIds);
}