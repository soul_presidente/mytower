package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.chanel.model.EbWeekTemplateDay;


public interface EbWeekTemplateDayRepository extends CommonRepository<EbWeekTemplateDay, Integer> {
    @EntityGraph(value = "EbWeekTemplateDay.hours", type = EntityGraphType.LOAD)
    public List<EbWeekTemplateDay> findByWeek_ebWeektemplateNum(Integer ebWeekTemplateNum);

    @Transactional
    @Modifying
    @Query("DELETE EbWeekTemplateDay b WHERE b.week.ebWeektemplateNum =:ebWeektemplateNum")
    void deleteByEbWeekTemple(@Param("ebWeektemplateNum") int ebWeektemplateNum);
}
