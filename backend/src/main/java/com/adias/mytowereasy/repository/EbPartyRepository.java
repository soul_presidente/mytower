/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.trpl.model.EbPlZone;


public interface EbPartyRepository extends CommonRepository<EbParty, Integer> {
    public EbParty findOneByCompany(String company);

    @EntityGraph(value = "EbParty.detail")
    public List<EbParty> findDistinctAllByCompanyStartingWith(String company);

    @Query("SELECT distinct new EbParty(trim(upper(b.city))) FROM EbParty b WHERE b.city is not null")
    public List<EbParty> selectDistinctAllCity();

    public List<EbParty> findAllByZone(EbPlZone ebZone);

    public List<EbParty> findAllByZoneEbZoneNum(Integer ebZoneNum);

    @Transactional
    @Modifying
    @Query("update EbParty p set p.zone = ?2 where p.ebPartyNum = ?1 ")
    public void updateEbPartyZone(Integer ebPartyNum, EbPlZone zone);

    @EntityGraph(value = "EbParty.country_zone_etablissementAdresse")
    public EbParty findByEbPartyNum(Integer ebPartyNum);
}
