/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.qm.EbQmRootCause;


@Repository
public interface EbQmRootCauseRepository extends CommonRepository<EbQmRootCause, Integer> {
    List<EbQmRootCause> findByEtablissement_ebCompagnie_ebCompagnieNum(Integer ebCompagnieNum);

    @Query("select count (rc) from EbQmRootCause rc LEFT JOIN rc.etablissement et WHERE et.ebCompagnie.ebCompagnieNum = ?1")
    int countRootCause(Integer ebCompagnieNum);

    @Query(nativeQuery = true, value = "SELECT NEXTVAL('work.eb_qm_root_cause_qm_root_cause_num_seq')")
    int getNextSequenceId();

    @Query("SELECT count(rc) FROM EbQmRootCause rc  where rc.relatedIncident = :relatedIncident")
    public Integer countListRoutCauseByRelatedIncident(@Param("relatedIncident") String relatedIncident);
}
