/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbCombinaison;
import com.adias.mytowereasy.model.EbDestinationCountry;


@Repository
public interface EbCombinaisonRepository extends CommonRepository<EbCombinaison, Integer> {
    public enum NullBehavior {
        EQUALS, IS, IGNORED
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({
        ElementType.TYPE, ElementType.METHOD, ElementType.PARAMETER
    })
    public @interface NullMeans {
        NullBehavior value() default NullBehavior.EQUALS;
    }

    Set<EbCombinaison> findByDestinationCountry(EbDestinationCountry destinationCountry);

    Set<EbCombinaison> findByDestinationCountry_ebDestinationCountryNum(Integer ebDestinationCountryNum);

    void deleteByDestinationCountry(EbDestinationCountry destinationCountry);

    List<EbCombinaison> findByDestinationCountry_ebDestinationCountryNumAndHsCodeAndOrigin_ecCountryNum(
        Integer ebDestinationCountryNum,
        @NullMeans(NullBehavior.IGNORED) String hsCode,
        @NullMeans(NullBehavior.IGNORED) Integer ecCountryNum);
}
