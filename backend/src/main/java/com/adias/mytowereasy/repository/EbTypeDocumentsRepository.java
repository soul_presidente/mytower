package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbTypeDocuments;


@Repository
public interface EbTypeDocumentsRepository extends CommonRepository<EbTypeDocuments, Integer> {
    EbTypeDocuments findFirstByCodeAndXEbCompagnie(String code, Integer ebCompagnieNum);

    @Transactional
    @Modifying
    @Query("update EbTypeDocuments d set d.nom = ?2, d.code = ?3, d.ordre = ?4 , d.activated = ?5 , d.isexpectedDoc = ?6 where d.ebTypeDocumentsNum = ?1")

    public EbTypeDocuments update(
        Integer ebTypeDocumentsNum,
        String nom,
        String code,
        Integer ordre,
        Boolean activate,
        Boolean isexpectedDoc);

    @Transactional
    @Modifying
    @Query("update EbTypeDocuments d set d.activated = ?2 where d.ebTypeDocumentsNum = ?1")
    public void updateActivated(Integer ebTypeDocumentsNum, Boolean activate);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbTypeDocuments iv WHERE iv.ebTypeDocumentsNum = ?1")
    public int deleteListEbInvoiceByEbDemandeNum(Integer ebTypeDocumentsNum);

    @Query("FROM EbTypeDocuments iv WHERE ?1 IN iv.module")
    List<EbTypeDocuments> findByModule(Integer moduleNum);

    @Query("SELECT tdoc FROM EbTypeDocuments tdoc WHERE tdoc.xEbCompagnie IN (?1)")
    List<EbTypeDocuments> findByListEbCompagnieNum(List<Integer> listEbCompagnieNum);

    List<EbTypeDocuments> findByXEbCompagnie(Integer ebCompagnieNum);

    public List<EbTypeDocuments> findAllByXEbCompagnie(Integer ebCompagnieNum);

    public EbTypeDocuments findFirstByCode(String code);

    public List<EbTypeDocuments> findByCode(String code);
}
