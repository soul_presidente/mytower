/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EcCountry;


@Repository
public interface EcCountryRepository extends JpaRepository<EcCountry, Integer> {
    public List<EcCountry> findAllByOrderByLibelleAsc();

    public EcCountry findFirstByCode(String code);

    @Query(
        value = "SELECT * FROM work.ec_country  where UPPER(libelle) like CONCAT('%',UPPER(?1),'%')",
        nativeQuery = true)
    public List<EcCountry> findAllByLibelleLike(String SearchTerm);

    public EcCountry findByLibelle(String libelle);

    public EcCountry findByEcCountryNum(Integer ecCountryNum);
}
