package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbTypeUnit;


public interface EbTypeUnitRepository extends JpaRepository<EbTypeUnit, Long> {
    @Query("select count(1) from EbTypeUnit t")
    public Long getCountTypeUnit();

    List<EbTypeUnit> findByXEbCompagnieEbCompagnieNumAndAndActivated(Integer compagnieNum, boolean activated);

    List<EbTypeUnit> findByXEbCompagnieEbCompagnieNum(Integer compagnieNum);

    EbTypeUnit findByEbTypeUnitNum(Long ebTypeUnitNum);

    public EbTypeUnit findFirstByCode(String code);

    @Transactional
    @Modifying
    @Query("update EbTypeUnit t set t.activated = :activated where t.ebTypeUnitNum = :ebTypeUnitNum ")
    public void
        activateDeactivateTypeUnit(@Param("ebTypeUnitNum") Long ebTypeUnitNum, @Param("activated") boolean activated);
}
