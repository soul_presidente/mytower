/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbEtablissement;


@Repository
public interface EbEtablissementRepository extends CommonRepository<EbEtablissement, Integer> {
    public EbEtablissement findOneBySiret(String siret);

    public boolean existsBySiret(String siret);

    public Set<EbEtablissement> findAllByEbCompagnie_ebCompagnieNum(Integer ebCompagnieNum);

    @EntityGraph(value = "EbEtablissement.detail", type = EntityGraphType.LOAD)
    public Set<EbEtablissement> findAllWithDetailsByEbCompagnie_ebCompagnieNum(Integer ebCompagnieNum);

    @EntityGraph(value = "EbEtablissement.detail", type = EntityGraphType.LOAD)
    public EbEtablissement findOneWithDetailsByEbEtablissementNum(Integer ebEtablissementNum);

    public EbEtablissement findOneByEbEtablissementNum(Integer ebEtablissementNum);

    @Modifying(clearAutomatically = true)
    @Query("update EbEtablissement etablissement set etablissement.logo =:filePath where etablissement.ebEtablissementNum =:ebEtablissementNum")
    void updateEbEtablissementLogo(
        @Param("filePath") String filePath,
        @Param("ebEtablissementNum") Integer ebEtablissementNum);

    @Transactional
    @Query("select etablissement.ebCompagnie.ebCompagnieNum from EbEtablissement etablissement where etablissement.ebEtablissementNum = :ebEtablissementNum")
    public Integer selectNumCompagnieByEtablissement(@Param("ebEtablissementNum") Integer ebEtablissementNum);

    @Query("SELECT new EbEtablissement(c.ebEtablissementNum, c.nom) FROM EbUser u LEFT JOIN u.ebEtablissement c WHERE u.ebUserNum = ?1")
    public EbEtablissement selectEtablissementByEbUserNum(Integer ebUserNum);

    @Query("SELECT new EbEtablissement(e.ebEtablissementNum, e.nom) FROM EbEtablissement e ")
    public List<EbEtablissement> selectEtablissement();

		@Query(
			value = "select case when x_eb_compagnie = ?1 then true else false end as has_id from work.eb_etablissement where eb_etablissement_num in (?2) order by has_id desc limit 1",
			nativeQuery = true
		)
		public boolean isCompanyInMyContactByListEtablissementIds(Integer idComponyGuest, List<Integer> idsEtablissement);
}
