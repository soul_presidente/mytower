/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.custom.EbCustomDeclaration;


@Repository
public interface EbCustomDeclarationRepository extends JpaRepository<EbCustomDeclaration, Integer> {
    @Transactional
    @Modifying
    @Query("DELETE FROM EbCustomDeclaration cd WHERE cd.xEbDemande.ebDemandeNum IN (?1)")
    public Integer deleteByEbDemandeNum(List<Integer> listEbDemandeNum);
}
