/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository.custom;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.custom.EbFormFieldCompagnie;
import com.adias.mytowereasy.repository.CommonRepository;


@Repository
public interface EbFormFieldCompagnieRepository extends CommonRepository<EbFormFieldCompagnie, Integer> {

    // @Query("SELECT ebFormFieldCompagnie ifj SET ifj = ?1 WHERE
    // ifj.xEbCompagnie = ?2")
    // public Integer findEbCompagnieNum(EbFormFieldCompagnie
    // ebFormFieldCompagnie, Integer
    // CompagnieNum);
    public EbFormFieldCompagnie findOneByxEbCompagnieEbCompagnieNum(Integer ebCompanyNum);

    @Transactional
    @Modifying
    @Query("update EbFormFieldCompagnie ifj SET ifj.listFormField = ?1 where ifj.ebFormFieldCompagnieNum = ?2")
    public void updateListChamp(Object listFormField, Integer formFieldCompagnieNum);
}
