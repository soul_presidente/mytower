/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import com.adias.mytowereasy.model.custom.EbFormView;


public interface EbFormViewRepository extends CommonRepository<EbFormView, Integer> {
}
