package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbEmailHistorique;


@Repository
public interface EbEmailHistoriqueRepository extends CommonRepository<EbEmailHistorique, Integer> {

    // @Query("select eb from EbEmailHistorique eb where eb.ebIncidentNum = 3")
    public List<EbEmailHistorique> findByEbIncidentNum(Integer ebIncidentNum);
}
