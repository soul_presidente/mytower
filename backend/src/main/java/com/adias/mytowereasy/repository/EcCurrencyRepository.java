/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;
import java.util.Optional;

import com.adias.mytowereasy.model.EcCurrency;


public interface EcCurrencyRepository extends CommonRepository<EcCurrency, Integer> {
    public List<EcCurrency> findAllByOrderByCodeAsc();

		Optional<EcCurrency> findByCodeIgnoreCase(String code);

    public EcCurrency findByCode(String code);
}
