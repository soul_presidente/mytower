package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adias.mytowereasy.chanel.model.EbWeekTemplatePeriod;


public interface EbWeekTemplatePeriodRepository extends JpaRepository<EbWeekTemplatePeriod, Integer> {

}
