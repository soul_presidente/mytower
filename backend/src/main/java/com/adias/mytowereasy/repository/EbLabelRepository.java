/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbLabel;


@Repository
public interface EbLabelRepository extends JpaRepository<EbLabel, Integer> {
    List<EbLabel> findByCategorieEbCategorieNum(Integer ebCategorieNum);

    @Query("select l from EbLabel l  where l.categorie.ebCategorieNum = :categorieNum AND l.libelle LIKE CONCAT('%',:libelle,'%')")
    List<EbLabel>
        findLabelsBycategorieAndLibelle(@Param("categorieNum") Integer categorieNum, @Param("libelle") String libelle);

    EbLabel findByLibelle(String libelle);

    EbLabel findByEbLabelNum(Integer ebLabelNum);

    @Transactional
    @Modifying
    @Query("update EbLabel l set l.deleted = true where l.ebLabelNum = ?1")
    void deleteLabelWithHistorisation(Integer EbLabelNum);

    @Query("select l.ebLabelNum from EbLabel l  where l.categorie.libelle LIKE CONCAT('%',:categorieLabel,'%')  AND l.libelle LIKE CONCAT('%',:label,'%')")
    List<Integer> findLabelsNumBycategorieLabelAndLabel(
        @Param("categorieLabel") String categorieLabel,
        @Param("label") String label);
    
    @Query("select label from EbLabel label  where label.categorie.ebCategorieNum = :categorieNum")
    Set<EbLabel> findLabelsByCategorieId(@Param("categorieNum") Integer categorieNum);

    @Transactional
    @Modifying
    @Query(value = "delete from work.eb_label where x_eb_categorie in (?1)", nativeQuery = true)
    void deleteLabelByCategoriesIds(List<Integer> categoriesIds);
}
