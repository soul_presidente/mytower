/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EcRegion;


@Repository
public interface EcRegionRepository extends CommonRepository<EcRegion, Integer> {
    public List<EcRegion> findAllByOrderByLibelleAsc();
}
