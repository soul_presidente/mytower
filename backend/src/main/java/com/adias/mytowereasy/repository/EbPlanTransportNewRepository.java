package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.adias.mytowereasy.model.EbPlanTransportNew;


public interface EbPlanTransportNewRepository extends CommonRepository<EbPlanTransportNew, Integer> {
    public EbPlanTransportNew findOneByEbPlanTransportNum(Integer ebPlanTransportNum);

    @Query(
        value = "SELECT * FROM work.eb_plan_transport_new  where UPPER(input ->> ?1) like CONCAT('%',UPPER(?2),'%')",
        nativeQuery = true)
    public List<EbPlanTransportNew> selectListEbDemandeByInputStringValues(String key, String value);

    @Query(
        value = "SELECT * FROM work.eb_plan_transport_new  where UPPER(input ->> ?1) similar to ?2",
        nativeQuery = true)
    public List<EbPlanTransportNew> selectListEbDemandeByInputReferenceValues(String key, String valueIds);
}
