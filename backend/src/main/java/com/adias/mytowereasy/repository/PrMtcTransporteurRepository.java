package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.PrMtcTransporteur;


@Repository
public interface PrMtcTransporteurRepository extends JpaRepository<PrMtcTransporteur, Integer> {
    @Query("SELECT prMtc FROM PrMtcTransporteur prMtc WHERE  prMtc.codeTransporteurMtc = :codeMTG")
    public PrMtcTransporteur findPrMtcTransporteurByCodeMTG(@Param("codeMTG") String codeMTG);

    @Query("SELECT prMtc FROM PrMtcTransporteur prMtc WHERE  prMtc.xEbCompagnieTransporteur.code = :codeMTC")
    public PrMtcTransporteur findPrMtcTransporteurByCodeMTC(@Param("codeMTC") String codeMTC);

    public PrMtcTransporteur findFirstByXEbCompagnieChargeur_ebCompagnieNumAndCodeConfigurationMtc(
        Integer ebCompagnieNum,
        String configCode);

    public PrMtcTransporteur findFirstByxEbUser_ebUserNum(Integer ebUserNum);
}
