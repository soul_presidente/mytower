/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbAdresse;
import com.adias.mytowereasy.trpl.model.EbPlZone;


@Repository
public interface EbAdresseRepository extends JpaRepository<EbAdresse, Integer> {
    public EbAdresse findOneByCompany(String company);

    public List<EbAdresse> findDistinctAllByCompanyStartingWith(String company);

    @Query("SELECT adr from EbAdresse adr where ?1 in adr.listZonesNum")
    public List<EbAdresse> getAllByZoneNum(String ebZoneNum); // ebZoneNum =
                                                              // :ebZoneNum:

    @Query("SELECT adr from EbAdresse adr where ?1 in adr.listEtablissementsNum")
    public List<EbAdresse> getAllByEtablissementNum(String ebEtablissementNum);

    @Transactional
    @Modifying
    @Query("UPDATE EbAdresse d set d.zones = ?1, d.listZonesNum = ?2 where d.ebAdresseNum = ?3")
    public void updateListZone(List<EbPlZone> zones, String listZonesNum, Integer ebAdresseNum);

    public List<EbAdresse> getByEbCompagnie_ebCompagnieNumAndReference(Integer ebCompanieNum, String refAdresse);

    @Query("SELECT adr.reference from EbAdresse adr where adr.ebCompagnie.ebCompagnieNum = :ebCompagnieNum and adr.reference is not null")
    public List<String> getAllAdrReferencesByCompagnieNum(@Param("ebCompagnieNum") Integer ebCompagnieNum);
}
