package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbUserNotification;


@Repository
public interface EbUserNotificationRepository extends CommonRepository<EbUserNotification, Integer> {
    public EbUserNotification findByEbUser_ebUserNum(Integer ebUserNum);

    @Transactional
    @Modifying
    @Query("update EbUserNotification un set un.nbrAlerts = un.nbrAlerts + 1 where un.ebUser.ebUserNum = :ebUserNum")
    public void incNbrAlerts(@Param("ebUserNum") Integer ebUserNum);

    @Transactional
    @Modifying
    @Query("update EbUserNotification un set un.nbrNotifys = un.nbrNotifys + 1 where un.ebUser.ebUserNum = :ebUserNum")
    public void incNbrNotifys(@Param("ebUserNum") Integer ebUserNum);

    @Transactional
    @Modifying
    @Query("update EbUserNotification un set un.nbrMsgs = un.nbrMsgs + 1 where un.ebUser.ebUserNum = :ebUserNum")
    public void incNbrMsgs(@Param("ebUserNum") Integer ebUserNum);

    @Transactional
    @Modifying
    @Query("update EbUserNotification un set un.nbrAlerts = :nbrAlerts where un.ebUser.ebUserNum = :ebUserNum")
    public void updateNbrAlerts(@Param("ebUserNum") Integer ebUserNum, @Param("nbrAlerts") Integer nbrAlerts);

    @Transactional
    @Modifying
    @Query("update EbUserNotification un set un.nbrNotifys = :nbrNotifys where un.ebUser.ebUserNum = :ebUserNum")
    public void updateNbrNotifys(@Param("ebUserNum") Integer ebUserNum, @Param("nbrNotifys") Integer nbrNotifys);

    @Transactional
    @Modifying
    @Query("update EbUserNotification un set un.nbrMsgs = :nbrMsgs where un.ebUser.ebUserNum = :ebUserNum")
    public void updateNbrMsgs(@Param("ebUserNum") Integer ebUserNum, @Param("nbrMsgs") Integer nbrMsgs);
}
