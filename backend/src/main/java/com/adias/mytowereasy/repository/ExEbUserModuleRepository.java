/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.ExUserModule;


@Repository
public interface ExEbUserModuleRepository extends CommonRepository<ExUserModule, Integer> {
    public Set<ExUserModule> findAllByEbUserNum(Integer ebUserNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM ExUserModule u WHERE u.ebUser =?1")
    public Integer deleteSelectedEbUserModule(EbUser id);
}
