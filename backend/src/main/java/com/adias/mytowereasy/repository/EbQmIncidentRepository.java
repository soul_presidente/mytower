/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.qm.EbQmIncident;
import com.adias.mytowereasy.model.qm.EbQmRootCause;


@Repository
public interface EbQmIncidentRepository extends JpaRepository<EbQmIncident, Integer> {
    @Transactional
    @Modifying
    @Query("update EbQmIncident i set i.etablissementComment = :etablissementComment, i.thirdPartyComment = :thirdPartyComment where i.ebQmIncidentNum = :ebqmincidentnum")
    public int updateIncidentComment(
        @Param("ebqmincidentnum") Integer ebQmIncidentNum,
        @Param("etablissementComment") String etablissementComment,
        @Param("thirdPartyComment") String thirdPartyComment);

    @Transactional
    @Modifying
    @Query("update EbQmIncident i set i.incidentDesc = :incidentDesc, i.addImpact = :addImpact where i.ebQmIncidentNum = :ebqmincidentnum")
    public int updateIncidentDescription(
        @Param("ebqmincidentnum") Integer ebQmIncidentNum,
        @Param("incidentDesc") String incidentDesc,
        @Param("addImpact") String addImpact);

    @Transactional
    @Modifying
    @Query("update EbQmIncident i set i.modeTransport = :modeTransport, i.originCountryLibelle = :originCountryLibelle, i.insurance = :insurance, i.goodValues = :goodValues where i.ebQmIncidentNum = :ebqmincidentnum")
    public int updateIncidentShipment(
        @Param("ebqmincidentnum") Integer ebQmIncidentNum,
        @Param("modeTransport") Integer modeTransport,
        @Param("originCountryLibelle") String originCountryLibelle,
        @Param("insurance") Integer insurance,
        @Param("goodValues") String goodValues);

    @Transactional
    @Modifying
    @Query("update EbQmIncident i set i.categoryLabel = :categoryLabel, i.rootCauses = :rootCauses, i.incidentDesc = :incidentDesc, i.addImpact = :addImpact, i.currencyLabel = :currencyLabel, i.claimAmount = :claimAmount, i.recovered = :recovered, i.balance = :balance  where i.ebQmIncidentNum = :ebqmincidentnum")
    public int updateIncidentInformation(
        @Param("categoryLabel") String categoryLabel,
        @Param("rootCauses") List<EbQmRootCause> rootCauses,
        @Param("incidentDesc") String incidentDesc,
        @Param("addImpact") String addImpact,
        @Param("currencyLabel") String currencyLabel,
        @Param("claimAmount") Double claimAmount,
        @Param("recovered") Double recovered,
        @Param("balance") Double balance);

    @Transactional
    @Modifying
    @Query("update EbQmIncident i set i.incidentStatus = :incidentstatus where i.ebQmIncidentNum = :ebqmincidentnum")
    public int updateIncidentStatus(
        @Param("ebqmincidentnum") Integer ebQmIncidentNum,
        @Param("incidentstatus") Integer incidentStatus);

    @Query("SELECT i FROM EbQmIncident i  where i.ebQmIncidentNum = :ebqmincidentnum")
    public EbQmIncident selectListDoc(@Param("ebqmincidentnum") Integer ebQmIncidentNum);

    @Query("SELECT count(i) FROM EbQmIncident i  where i.demandeTranportRef = :refTransport")
    public Integer countListIncidentByRefTransport(@Param("refTransport") String refTransport);

    @Transactional
    @Query("select qmi.xEbDemande from EbQmIncident qmi where qmi.ebQmIncidentNum = :numEntity")
    Integer findxEbDemandeByebQmIncidentNum(@Param("numEntity") Integer numEntity);

    @Transactional
    @Query("select qmi.ebQmIncidentNum from EbQmIncident qmi where qmi.xEbDemande = ?1 ")
    public List<Integer> findByxEbDemandeEbQmIncident(Integer numEntity);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbQmIncident qd WHERE qd.xEbDemande IN (?1)")
    public Integer deleteByEbDemandeNum(List<Integer> listEbDemandeNum);

    @Transactional
    @Modifying
    @Query(
        value = "update work.eb_qm_incident set x_eb_etablissement=?2 "
            + "where regexp_split_to_array(?3, ',') && (regexp_split_to_array(list_labels,',')) "
            + "and x_eb_etablissement = ?1",
        nativeQuery = true)
    public int updateIncidentEtablissement(Integer oldEtablissement, Integer newEtablissement, String listLabels);

    @Query("select count (rc) from EbQmIncident rc WHERE rc.eventType = ?1")
    public int countByEventType(int type);

    @Transactional
    @Modifying
    @Query("update EbQmIncident i set i.dateCreation = :dateCreation where i.ebQmIncidentNum = :ebQmIncidentNum")
    public int
        updateDateCreation(@Param("ebQmIncidentNum") Integer ebQmIncidentNum, @Param("dateCreation") Date dateCreation);

    @Query("SELECT distinct eqi.listFlag from EbQmIncident eqi LEFT JOIN eqi.ebDemande AS ed where eqi.listFlag is not null and eqi.listFlag <> '' and ed.xEbTransporteur = :ebUserNum")
    public List<String> selectListFlagForCarrier(@Param("ebUserNum") Integer ebUserNum);

    @Query("SELECT distinct eqi.listFlag from EbQmIncident eqi LEFT JOIN eqi.ebDemande AS ed LEFT JOIN ed.xEbUserCt AS u where eqi.listFlag is not null and eqi.listFlag <> '' and u.ebUserNum = :ebUserNum")
    public List<String> selectListFlagForControlTower(@Param("ebUserNum") Integer ebUserNum);
}
