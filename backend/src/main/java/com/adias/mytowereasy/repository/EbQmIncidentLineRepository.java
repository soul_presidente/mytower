/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.qm.EbQmIncidentLine;


@Repository
public interface EbQmIncidentLineRepository extends JpaRepository<EbQmIncidentLine, Integer> {
    @Transactional
    @Modifying
    @Query("DELETE FROM EbQmIncidentLine il WHERE il.incident.ebQmIncidentNum = ?1")
    public Integer deleteIncidentLine(Integer incidentNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbQmIncidentLine il WHERE il.incident.ebQmIncidentNum IN (?1)")
    public Integer deleteIncidentLineByIncident(List<Integer> listEbIncidentNum);
}
