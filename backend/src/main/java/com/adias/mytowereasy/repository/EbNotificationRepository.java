package com.adias.mytowereasy.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adias.mytowereasy.model.EbNotification;


public interface EbNotificationRepository extends CommonRepository<EbNotification, Long> {
    public List<EbNotification> findByTypeAndEbUser_ebUserNum(Integer type, Integer userNum);

    public List<EbNotification> findByEbUser_ebUserNumOrderByEbNotificationNumDesc(Integer userNum);

    @Query(
        value = "select * from work.eb_notification en where en.x_eb_user = :userNum and en.type = :typeNotification " +
            "and en.date_creation_sys > :since order by en.eb_notification_num desc offset :offset limit :limit",
        nativeQuery = true)
    public List<EbNotification> findByTypeAndUser(
        @Param("userNum") Integer userNum,
        @Param("typeNotification") Integer typeNotification,
        @Param("since") Timestamp since,
        @Param("offset") Integer offset,
        @Param("limit") Integer limit);
}
