/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbCostCategorie;


@Repository
public interface EbCostCategorieRepository extends JpaRepository<EbCostCategorie, Integer> {
    @EntityGraph(value = "allWithListEbCost", type = EntityGraphType.LOAD)
    // @Query("SELECT DISTINCT c FROM EbCostCategorie c")
    public List<EbCostCategorie> findAllWithListEbCostByXEcModeTransport(Integer modeTrans);

    @EntityGraph(value = "allWithListEbCost", type = EntityGraphType.LOAD)
    public List<EbCostCategorie> findAll();

    @Query("SELECT c FROM EbCostCategorie c WHERE  c.xEbCompagnie.ebCompagnieNum = ?1")
    public List<EbCostCategorie> selectEbCostCategorieByCompagnieNum(Integer ebCompganieNum);
}
