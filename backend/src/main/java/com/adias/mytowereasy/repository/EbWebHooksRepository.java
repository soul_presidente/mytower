package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbWebHooks;


@Repository
public interface EbWebHooksRepository extends JpaRepository<EbWebHooks, Integer> {

}
