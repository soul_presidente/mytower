/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.airbus.model.QrGroupeObject;
import com.adias.mytowereasy.model.EbMarchandise;


@Repository
public interface EbMarchandiseRepository extends JpaRepository<EbMarchandise, Integer> {
    @EntityGraph(value = "graph.EbEbMarchandise.xEbTracing", type = EntityGraphType.LOAD)
    public List<EbMarchandise> findAllByEbDemande_ebDemandeNum(Integer ebDemandeNum);

    @EntityGraph(value = "graph.EbMarchandise.parent")
    public List<EbMarchandise> findAllByEbDemandeEbDemandeNum(Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbMarchandise mr WHERE mr.ebDemande.ebDemandeNum IN (?1)")
    Integer deleteListEbMarchandiseByEbDemandeNumIn(List<Integer> listEbDemandeNum);

    @EntityGraph(attributePaths = {
        "ebDemande"
    })
    @Query("SELECT mr FROM EbMarchandise mr WHERE mr.ebDemande.ebDemandeNum IN (?1)")
    public List<EbMarchandise> selectListEbMarchandiseByEbDemandeNumIn(List<Integer> listEbDemandeNum);

    @Query("SELECT mr FROM EbMarchandise mr WHERE mr.ebMarchandiseNum IN (?1)")
    public List<EbMarchandise> findByIDs(List<Integer> listEbMarchandiseNum);

    @Transactional
    @Query("select m from EbMarchandise m where m.ebDemande.ebDemandeNum = :ebDemandeNum and m.xEbTypeConteneur = :xEbTypeConteneur")
    public List<EbMarchandise> selectListEbMarchandiseByEbDemandeNumAndXEbTypeConteneur(
        @Param("ebDemandeNum") Integer ebDemandeNum,
        @Param("xEbTypeConteneur") Integer xEbTypeConteneur);

    @Transactional
    @Modifying
    @Query("update EbMarchandise m set m.xEbTypeUnit = ?3, m.labelTypeUnit = ?4 where m.ebDemande.ebDemandeNum IN (select d.ebDemandeNum from EbDemande d where d.xEbCompagnie.ebCompagnieNum = ?1) and m.typeMarchandise = ?2")
    public int recoveryTypeOfUnitByEbCompanyNumAndTypeMarchandise(
        Integer ebCompagnieNum,
        String typeMarchandise,
        Long ebTypeUnitNum,
        String labelTypeUnit);

    @Query("select new com.adias.mytowereasy.airbus.model.QrGroupeObject(m.ebMarchandiseNum, m.partNumber) "
        + "from EbMarchandise m join EbDemande d on m.ebDemande.ebDemandeNum = d.ebDemandeNum "
        + "join EbCompagnie c on d.xEbCompagnie.ebCompagnieNum = c.ebCompagnieNum "
        + "where c.ebCompagnieNum = :ebCompagnieNum and m.partNumber is not null")
    public List<QrGroupeObject> getAllPartNumbersByCompagnieNum(@Param("ebCompagnieNum") Integer ebCompagnieNum);

    @Transactional
    @Modifying
    @Query("UPDATE EbMarchandise m set m.exportControlStatut = :exportControlStatut where m.ebDemande.ebDemandeNum in :demandeIds")
    public int updateExportControlStatut(
        @Param("exportControlStatut") Integer exportControlStatut,
        @Param("demandeIds") List<Integer> demandeIds);

    @Query("SELECT new com.adias.mytowereasy.airbus.model.QrGroupeObject(m.ebMarchandiseNum, m.unitReference) "
        + "FROM EbMarchandise m WHERE m.ebMarchandiseNum in :listMarchandiseNum")
    public List<QrGroupeObject>
        findUnitRefByListMarchandiseNum(@Param("listMarchandiseNum") List<Integer> listMarchandiseNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbMarchandise mr WHERE mr.ebDemande.ebDemandeNum = ?1")
    public Integer deleteListEbMarchandiseByEbDemandeNum(Integer ebDemandeNum);
}
