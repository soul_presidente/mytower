/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbCompagnie;


@Repository
public interface EbCompagnieRepository extends JpaRepository<EbCompagnie, Integer> {
    public EbCompagnie findOneByCode(String ebCompagnieCode);

    public EbCompagnie findOneBySiren(String siren);

    public boolean existsBySiren(String siren);

    @EntityGraph(value = "EbCompagnie.detail", type = EntityGraphType.LOAD)
    public EbCompagnie findOneWithDetailsByEbCompagnieNum(Integer ebCompagnieNum);

    public EbCompagnie findOneByEbCompagnieNum(Integer ebCompagnieNum);

    @Transactional
    @Query("select c.nom from EbCompagnie c where c.ebCompagnieNum = :compagnieNum")
    public String recupNomCompagnie(@Param("compagnieNum") Integer compagnieNum);

    @Transactional
    @Query("select ebCompagnie from EbEtablissement etablissement,EbCompagnie ebCompagnie where ebCompagnie.ebCompagnieNum = etablissement.ebCompagnie.ebCompagnieNum and etablissement.ebEtablissementNum = :ebEtablissementNum")
    public EbCompagnie selectNumCompagnieAndNomByEtablissement(@Param("ebEtablissementNum") Integer ebEtablissementNum);

    @Query("SELECT new EbCompagnie(c.ebCompagnieNum, c.nom, c.code) FROM EbUser u LEFT JOIN u.ebCompagnie c WHERE u.ebUserNum = ?1")
    public EbCompagnie selectCompagnieByEbUserNum(Integer ebUserNum);

    @Modifying(clearAutomatically = true)
    @Query("update EbCompagnie cp set cp.logo =:filePath where cp.ebCompagnieNum =:ebCompagnieNum")
    void updateCompagnieLogo(@Param("filePath") String filePath, @Param("ebCompagnieNum") Integer ebCompagnieNum);

    @Query("SELECT cmp.ebCompagnieNum FROM EbCompagnie cmp")
    public List<Integer> selectAllCompagnieNum();

    @Query("SELECT  new EbCompagnie (cmp.nom) FROM EbCompagnie cmp")
    public List<EbCompagnie> selectAllCompagnieName();
}
