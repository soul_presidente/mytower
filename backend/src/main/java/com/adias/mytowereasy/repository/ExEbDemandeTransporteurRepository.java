/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;


@Repository
public interface ExEbDemandeTransporteurRepository extends CommonRepository<ExEbDemandeTransporteur, Integer> {
    public List<ExEbDemandeTransporteur>
        findAllByXEbDemande_ebDemandeNumAndXTransporteur_ebUserNum(Integer ebDemandeNum, Integer ebUserNum);

    public List<ExEbDemandeTransporteur> findAllByXEbDemande_ebDemandeNum(Integer ebDemandeNum);

    public List<ExEbDemandeTransporteur>
        findAllByStatusAndXEbDemande_ebDemandeNum(Integer status, Integer ebDemandeNum);

    public List<ExEbDemandeTransporteur>
        findAllByStatusGreaterThanEqualAndXEbDemande_ebDemandeNum(Integer status, Integer ebDemandeNum);

    public List<ExEbDemandeTransporteur>
        findAllByXTransporteur_ebUserNumAndStatusGreaterThanEqualAndXEbDemande_ebDemandeNumIn(
            Integer ebUserNum,
            Integer status,
            List<Integer> listEbDemandeNum);

    @Transactional
    @Modifying
    @Query("update ExEbDemandeTransporteur dt set dt.status = ?2 where dt.exEbDemandeTransporteurNum = ?1")
    public int updateStatus(Integer exEbDemandeTransporteurNum, Integer status);

    @Transactional
    @Modifying
    @Query("update ExEbDemandeTransporteur dt set dt.price = ?2 where dt.exEbDemandeTransporteurNum = ?1")
    public int updatePrice(Integer exEbDemandeTransporteurNum, BigDecimal price);

    public Long countByxEbCompagnieCurrency_ebCompagnieCurrencyNum(Integer exEtablissementCurrencyNum);

    @Transactional
    @Modifying
    @Query("update ExEbDemandeTransporteur dt set dt.xTransporteur = ?1, dt.xEbEtablissement = ?2, dt.xEbCompagnie = ?3 where exEbDemandeTransporteurNum = ?4")
    public int updateTransporteurEtablissement(
        EbUser xTransporteur,
        EbEtablissement xEbEtablissement,
        EbCompagnie xEbCompagnie,
        Integer exEbDemandeTransporteurNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM ExEbDemandeTransporteur dt WHERE dt.xEbDemande.ebDemandeNum = ?1")
    public Integer deleteListEbDemandeQuoteByEbDemandeNum(Integer ebDemandeNum);

		@Transactional
		@Modifying
		@Query(
			"DELETE FROM ExEbDemandeTransporteur dt WHERE dt.xEbDemande.ebDemandeNum = ?1 and dt.status = ?2"
		)
		public Integer deleteListEbDemandeQuoteByEbDemandeNumAndStatus(
			Integer ebDemandeNum,
			Integer status
		);

    @Query("SELECT new ExEbDemandeTransporteur(exdt.xTransporteur.ebUserNum, exdt.xEbDemande.ebDemandeNum, exdt.status) FROM ExEbDemandeTransporteur exdt  WHERE exdt.xEbEtablissement.ebEtablissementNum = ?1 AND exdt.status >= ?2 AND exdt.xEbDemande.ebDemandeNum IN (?3)")
    public List<ExEbDemandeTransporteur> findExEbDemandeTransporteurForStatusQuote(
        Integer ebEtablissementNum,
        Integer status,
        List<Integer> listEbDemandeNum);

    @Query("SELECT new ExEbDemandeTransporteur(exdt.exEbDemandeTransporteurNum, exdt.xTransporteur.ebUserNum, exdt.xEbDemande.ebDemandeNum, exdt.status, exdt.listEbTtCompagniePsl, exdt.pickupTime, exdt.deliveryTime) FROM ExEbDemandeTransporteur exdt  WHERE exdt.xEbDemande.ebDemandeNum IN (?1)")
    public List<ExEbDemandeTransporteur> findExEbDemandeTransporteurByListEbDemandeNum(List<Integer> listEbDemandeNum);

    @Query("SELECT new ExEbDemandeTransporteur(" + "exdt.xTransporteur.ebUserNum, " + "exdt.pickupTime, "
        + "exdt.deliveryTime, " + "exdt.transitTime, " + "exdt.price, " + "exdt.status, " + "d.ebDemandeNum, "
        + "d.refTransport, " + "tr.ebUserNum, " + "tr.nom, " + "tr.prenom, " + "tr.email, " + "et.ebEtablissementNum, "
        + "et.nom, " + "cmp.ebCompagnieNum, " + "cmp.nom, " + "cmp.code " + " ) FROM ExEbDemandeTransporteur exdt "
        + " LEFT JOIN exdt.xEbDemande d " + " LEFT JOIN exdt.xTransporteur tr " + " LEFT JOIN exdt.xEbEtablissement et "
        + " LEFT JOIN exdt.xEbCompagnie cmp " + " WHERE exdt.status >= ?1 AND exdt.xEbDemande.ebDemandeNum IN (?2)")
    public List<ExEbDemandeTransporteur> findByStatusQuote(Integer status, List<Integer> listEbDemandeNum);

    @Transactional
    @Modifying
    @Query("update ExEbDemandeTransporteur dt set dt.listEbTtCompagniePsl = ?2 where exEbDemandeTransporteurNum = ?1")
    public int updateListPsl(Integer exEbDemandeTransporteurNum, String listEbTtCompagniePsl);

    @Transactional
    @Modifying
    @Query("UPDATE ExEbDemandeTransporteur dt set dt.status = ?1 where dt.xEbDemande.ebDemandeNum = ?2")
    public int cancelQuoteByDemandeNum(Integer status, Integer ebDemandeNum);

    public ExEbDemandeTransporteur findByXEbDemande_ebDemandeNumAndStatusIn(Integer ebDemandeNum, List<Integer> status);

    @Query("select dt.xEbEtablissement.nom from ExEbDemandeTransporteur dt")
    public List<String> getEbEtablissementNameTransporteur();

    @Transactional
    @Modifying
    @Query("update ExEbDemandeTransporteur d set d.comment = :comment, d.infosForCustomsClearance = :infosForCustomsClearance, d.exchangeRate = :exchangeRate where d.exEbDemandeTransporteurNum = :quoteId")
    void updateComplementaryInformation(
        @Param("quoteId") Integer quoteId,
        @Param("comment") String comment,
        @Param("infosForCustomsClearance") String infosForCustomsClearance,
        @Param("exchangeRate") String exchangeRate);

		@Query("SELECT dt.xEbDemande FROM ExEbDemandeTransporteur dt where price = 0")
		public List<EbDemande> findTrsWithZeroAsValuation();
}
