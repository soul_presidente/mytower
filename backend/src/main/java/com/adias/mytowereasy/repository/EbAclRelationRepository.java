/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbAclRelation;


@Repository
public interface EbAclRelationRepository extends JpaRepository<EbAclRelation, Integer> {
    @Query("from EbAclRelation rel where rel.profile != null and rel.profile.ebUserProfileNum = ?1")
    public List<EbAclRelation> findByProfile(Integer ebUserProfileNum);

    @Transactional
    @Modifying
    @Query("delete from EbAclRelation rel where rel.profile != null and rel.profile.ebUserProfileNum = ?1")
    public void deleteByProfile(Integer ebUserProfileNum);

    @Query("from EbAclRelation rel where rel.user != null and rel.user.ebUserNum = ?1")
    public List<EbAclRelation> findByUser(Integer ebUserNum);

    @Transactional
    @Modifying
    @Query("delete from EbAclRelation rel where rel.user != null and rel.user.ebUserNum = ?1")
    public void deleteByUser(Integer ebUserNum);
}
