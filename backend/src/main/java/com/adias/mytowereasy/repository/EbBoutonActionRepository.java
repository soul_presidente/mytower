package com.adias.mytowereasy.repository;

import com.adias.mytowereasy.model.EbBoutonAction;


public interface EbBoutonActionRepository extends CommonRepository<EbBoutonAction, Integer> {

}
