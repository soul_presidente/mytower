package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EcMimetype;


@Repository
public interface EcMimetypeRepository extends JpaRepository<EcMimetype, Integer> {
    @Query("select mimetype from EcMimetype ")
    List<String> findDescription();
}
