/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemandeFichiersJoint;
import com.adias.mytowereasy.model.EbInvoice;


@Repository
public interface EbInvoiceRepository extends CommonRepository<EbInvoice, Integer> {
    @Query("select inv from EbInvoice inv where inv.ebInvoiceNum = :ebInvoiceNum")
    public EbInvoice getEbInvoice(@Param("ebInvoiceNum") Integer ebInvoiceNum);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.statut = ?2 where d.ebInvoiceNum = ?1")
    public int updateStatus(Integer ebInvoiceNum, Integer status);

    @Query("select eb from EbDemandeFichiersJoint eb where eb.numEntity = ?1")
    public List<EbDemandeFichiersJoint> listEbInvoice_ebInvoiceNum(Integer ebInvoiceNum);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.memoChageur = ?2 where d.ebInvoiceNum = ?1")
    public int updateMemoChargeur(Integer ebInvoiceNum, String memoChageur);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.memoCarrier = ?2 where d.ebInvoiceNum = ?1")
    public int updateMemoCarrier(Integer ebInvoiceNum, String memoCarrier);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.flag = ?2 where d.ebInvoiceNum = ?1")
    public int updateFlagChargeur(Integer ebInvoiceNum, Integer flag);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.flagCarrier = ?2 where d.ebInvoiceNum = ?1")
    public int updateFlagCarrier(Integer ebInvoiceNum, Integer flagCarrier);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.uploadFileNbr = (CASE WHEN d.uploadFileNbr IS NULL THEN 0 ELSE d.uploadFileNbr END) + 1 where d.ebInvoiceNum = ?1")
    public int updateUploadFileNbr(Integer ebInvoiceNum);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.applyChargeurIvoicePrice = ?2 where d.ebInvoiceNum = ?1")
    public int applyInvoicePriceChargeur(Integer ebInvoiceNum, Double applyChargeurIvoicePrice);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.applyCarrierIvoicePrice = ?2 where d.ebInvoiceNum = ?1")
    public int applyInvoicePriceCarrier(Integer ebInvoiceNum, Double applyCarrierIvoicePrice);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.applyChargeurIvoiceNumber = ?2 where d.ebInvoiceNum = ?1")
    public int applyInvoiceNumberChargeur(Integer ebInvoiceNum, String applyChargeurIvoiceNumber);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.applyCarrierIvoiceNumber = ?2 where d.ebInvoiceNum = ?1")
    public int applyInvoiceNumberCarrier(Integer ebInvoiceNum, String applyCarrierIvoiceNumber);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.invoiceYear = ?2 where d.ebInvoiceNum = ?1")
    public int addInvoiceYearChargeur(Integer ebInvoiceNum, Integer invoiceYear);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.invoiceMonth = ?2 where d.ebInvoiceNum = ?1")
    public int addInvoiceMonthChargeur(Integer ebInvoiceNum, Integer invoiceMonth);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.invoiceYearCarrier = ?2 where d.ebInvoiceNum = ?1")
    public int addInvoiceYearCarrier(Integer ebInvoiceNum, Integer invoiceYearCarrier);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.invoiceMonthCarrier = ?2 where d.ebInvoiceNum = ?1")
    public int addInvoiceMonthCarrier(Integer ebInvoiceNum, Integer invoiceMonthCarrier);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.invoiceMonth = ?2 where d.ebInvoiceNum = ?1")
    public int applyNumber(Integer ebInvoiceNum, Integer invoiceMonth);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.uploadFileNbr = d.uploadFileNbr - 1 where d.ebInvoiceNum = ?1")
    public int updateUploadFileNbr2(Integer ebInvoiceNum);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.numberArraw = ?1 where d.ebInvoiceNum = ?2")
    public int updateNumberArraw(Integer numberArraw, Integer ebInvoiceNum);

    @Transactional
    @Query("select inv.ebInvoiceNum from EbInvoice inv where inv.xEbDemande.ebDemandeNum = :numEntity")
    Integer findEbInvoiceNumByEbdemandeNum(@Param("numEntity") Integer numEntity);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.priceArraw = ?1 where d.ebInvoiceNum = ?2")
    public int updatePriceArraw(Integer priceArraw, Integer ebInvoiceNum);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.numAwbBol = ?1 where d.ebInvoiceNum = ?2")
    public int updateNumAwbBol(String numAwbBol, Integer ebInvoiceNum);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.preCarrierCost = ?1 where d.xEbDemande.ebDemandeNum = ?2")
    public int updateNegociatedpriceByPricing(BigDecimal preCarrierCost, Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.dateArraw = ?1 where d.ebInvoiceNum = ?2")
    public int updateDateArraw(Integer dateArraw, Integer ebInvoiceNum);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.statutCarrier = ?2 where d.ebInvoiceNum = ?1")
    public int sharedStatusCarrier(Integer ebInvoiceNum, Integer statutCarrier);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.statutChargeur = ?2 where d.ebInvoiceNum = ?1")
    public int sharedStatusChargeur(Integer ebInvoiceNum, Integer statutChargeur);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.statutChargeur = ?2 where d.ebInvoiceNum = ?1")
    public int updateStatusChargeur(Integer ebInvoiceNum, Integer status);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.statutCarrier = ?2 where d.ebInvoiceNum = ?1")
    public int updateStatusCarrier(Integer ebInvoiceNum, Integer status);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.statutChargeur = ?2 where d.ebInvoiceNum = ?1")
    public int applyStatutChargeur(Integer ebInvoiceNum, Integer statutChargeur);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.statutCarrier = ?2 where d.ebInvoiceNum = ?1")
    public int applyStatutCarrier(Integer ebInvoiceNum, Integer statutCarrier);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.invoiceDateChargeur = ?2 where d.ebInvoiceNum = ?1")
    public int applyInvoiceDateChargeur(Integer ebInvoiceNum, Date invoiceDateChargeur);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.flag = ?2 where d.ebInvoiceNum = ?1")
    public int applyFlagChargeur(Integer ebInvoiceNum, Integer FlagChargeur);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.flagCarrier = ?2 where d.ebInvoiceNum = ?1")
    public int applyFlagCarrier(Integer ebInvoiceNum, Integer FlagCarrier);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.invoiceDateCarrier = ?2 where d.ebInvoiceNum = ?1")
    public int applyInvoiceDateCarrier(Integer ebInvoiceNum, Date invoiceDateCarrier);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.applyChargeurIvoiceNumber = ?2 where d.ebInvoiceNum IN (?1)")
    public int
        applyInvoiceNumbersChargeurByListInvoice(List<Integer> listEbInvoiceNum, String applyChargeurIvoiceNumber);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.applyCarrierIvoiceNumber = ?2 where d.ebInvoiceNum IN (?1)")
    public int applyInvoiceNumberCarrierByListInvoice(List<Integer> listEbInvoiceNum, String applyCarrierIvoiceNumber);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.applyChargeurIvoicePrice = ?2 where d.ebInvoiceNum IN (?1)")
    public int applyInvoicePriceChargeurByListInvoice(List<Integer> listEbInvoiceNum, Double applyChargeurIvoicePrice);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.applyCarrierIvoicePrice = ?2 where d.ebInvoiceNum IN (?1)")
    public int applyInvoicePriceCarrierByListInvoice(List<Integer> listEbInvoiceNum, Double applyCarrierIvoicePrice);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.invoiceDateChargeur = ?2 where d.ebInvoiceNum IN (?1)")
    public int addInvoiceDateChargeurByListInvoice(List<Integer> listEbInvoiceNum, Date invoiceDateChargeur);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.invoiceDateCarrier = ?2 where d.ebInvoiceNum IN (?1)")
    public int addInvoiceDateCarrierByListInvoice(List<Integer> listEbInvoiceNum, Date invoiceDateCarrier);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.invoiceDateChargeur = ?2 where d.ebInvoiceNum IN (?1)")
    public int applyInvoiceDateChargeurByListInvoice(List<Integer> listEbInvoiceNum, Date invoiceDateChargeur);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.invoiceDateCarrier = ?2 where d.ebInvoiceNum IN (?1)")
    public int applyInvoiceDateCarrierByListInvoice(List<Integer> listEbInvoiceNum, Date invoiceDateCarrier);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.statutCarrier = ?2 where d.ebInvoiceNum IN (?1)")
    public int sharedStatusCarrierByListInvoice(List<Integer> listEbInvoiceNum, Integer statutCarrier);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.statutChargeur = ?2 where d.ebInvoiceNum IN (?1)")
    public int sharedStatusChargeurByListInvoice(List<Integer> listEbInvoiceNum, Integer statutChargeur);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.statutChargeur = ?2 where d.ebInvoiceNum IN (?1)")
    public int applyStatutChargeurByListInvoice(List<Integer> listEbInvoiceNum, Integer statutChargeur);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.statutCarrier = ?2 where d.ebInvoiceNum IN (?1)")
    public int applyStatutCarrierByListInvoice(List<Integer> listEbInvoiceNum, Integer statutCarrier);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.flag = ?2 where d.ebInvoiceNum IN (?1)")
    public int applyFlagChargeurByListInvoice(List<Integer> listEbInvoiceNum, Integer FlagChargeur);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.flagCarrier = ?2 where d.ebInvoiceNum IN (?1)")
    public int applyFlagCarrierByListInvoice(List<Integer> listEbInvoiceNum, Integer FlagCarrier);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbInvoice iv WHERE iv.xEbDemande.ebDemandeNum = ?1")
    public int deleteListEbInvoiceByEbDemandeNum(Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.memoChageur = ?2 where d.ebInvoiceNum IN (?1)")
    public int updateMemoChargeurByListInvoice(List<Integer> listEbInvoiceNum, String memoChageur);

    @Transactional
    @Modifying
    @Query("update EbInvoice d set d.memoCarrier = ?2 where d.ebInvoiceNum IN (?1)")
    public int updateMemoCarrierByListInvoice(List<Integer> listEbInvoiceNum, String memoCarrier);

    @Query("SELECT new EbCompagnie(iv.xEbCompagnieChargeur.ebCompagnieNum) FROM EbInvoice iv WHERE iv.ebInvoiceNum = ?1")
    public EbCompagnie findEbCompagnieChargeurByEbInvoiceNum(Integer ebInvoiceNum);

    @Query("SELECT iv FROM EbInvoice iv WHERE iv.xEbDemande.ebDemandeNum = ?1")
    public EbInvoice findEbInvoiceByEbDemandeNum(Integer ebDemandeNum);

    @Transactional
    @Query("select inv.xEbDemandeNum from EbInvoice inv where inv.ebInvoiceNum = :numEntity")
    Integer findxEbDemandeNumByEbInvoiceNum(@Param("numEntity") Integer numEntity);

    @Transactional
    @Modifying
    @Query(
        value = "update work.eb_invoice set x_eb_etablissement_chargeur=?2 "
            + "where regexp_split_to_array(?3, ',') && (regexp_split_to_array(list_label_str,',')) "
            + "and x_eb_etablissement_chargeur = ?1",
        nativeQuery = true)
    public int updateInvoiceEtablissement(Integer oldEtablissement, Integer newEtablissement, String listLabels);

    @Query("SELECT new EbInvoice(inv.ebInvoiceNum, inv.listTypeDocuments) FROM EbInvoice inv WHERE inv.ebInvoiceNum = ?1")
    public EbInvoice getListTypeDocumentsByEbInvoiceNum(Integer ebInvoiceNum);

    @Query("SELECT distinct ei.listFlag from EbInvoice ei LEFT JOIN ei.xEbDemande AS ed where ei.listFlag is not null and ei.listFlag <> '' and ed.xEbTransporteur = :ebUserNum")
    public List<String> selectListFlagForCarrier(@Param("ebUserNum") Integer ebUserNum);

    @Query("SELECT distinct ei.listFlag from EbInvoice ei LEFT JOIN ei.xEbDemande AS ed LEFT JOIN ed.xEbUserCt AS u where ei.listFlag is not null and ei.listFlag <> '' and u.ebUserNum = :ebUserNum")
    public List<String> selectListFlagForControlTower(@Param("ebUserNum") Integer ebUserNum);
}
