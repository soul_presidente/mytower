package com.adias.mytowereasy.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.rsch.algo.DockAvailabilityAlgo;
import com.adias.mytowereasy.rsch.model.EbRschUnitSchedule;


public interface EbRschUnitScheduleRepository extends CommonRepository<EbRschUnitSchedule, Integer> {
    // TODO refactor this method or delete it after full implementation of
    // calling feature
    @Transactional
    @Modifying
    @Query("update EbRschUnitSchedule r set r.status = ?1 where r.ebRschUnitScheduleNum = ?2")
    public void UpdateStatusRschUnitSchedule(Integer status, Integer ebRschUnitScheduleNum);

    @Query("SELECT new com.adias.mytowereasy.rsch.algo.DockAvailabilityAlgo(rs.dock,rs.workRangeHour) FROM EbRschUnitSchedule  rs "
        + "where  rs.dateUnloadingPa = :dateUnloadingPa and rs.status != 0")
    public List<DockAvailabilityAlgo> getListDockCreneauHoraireOccup(@Param("dateUnloadingPa") Date dateUnloadingPa);
}
