package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adias.mytowereasy.model.EbTypeFlux;


public interface EbTypeFluxRepository extends JpaRepository<EbTypeFlux, Integer> {
    public EbTypeFlux findOneByEbTypeFluxNum(Integer ebTypeFluxNum);

    @Query("Select t from EbTypeFlux t Where t.xEbCompagnie.ebCompagnieNum = :xEbCompanieNum AND (t.isDeleted is null OR t.isDeleted is false)")
    public List<EbTypeFlux> selectByCompagnieNum(@Param("xEbCompanieNum") Integer xEbCompanieNum);

    public EbTypeFlux findByCodeAndXEbCompagnie_ebCompagnieNum(String code, Integer ebCompagnieNum);
}
