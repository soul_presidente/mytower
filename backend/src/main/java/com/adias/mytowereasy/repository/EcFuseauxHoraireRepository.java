package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EcFuseauxHoraire;


@Repository
public interface EcFuseauxHoraireRepository extends JpaRepository<EcFuseauxHoraire, Integer> {

}
