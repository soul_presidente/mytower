/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.dto.EbTTPslAppAndEventsProjection;
import com.adias.mytowereasy.model.EcCountry;
import com.adias.mytowereasy.model.tt.EbTTPslApp;


public interface EbPslAppRepository extends JpaRepository<EbTTPslApp, Integer> {
    public List<EbTTPslApp> findAllByXEbTrackTrace_ebTtTracingNum(Integer ebTtTracingNum);

    public List<EbTTPslAppAndEventsProjection>
        findByXEbTrackTrace_ebTtTracingNumOrderByOrderpsl(Integer ebTtTracingNum);

    public EbTTPslApp findByCodePslCodeAndXEbTrackTrace_ebTtTracingNum(Integer codePslCode, Integer ebTtTracingNum);

    public EbTTPslApp findFirstByxEbTrackTraceEbTtTracingNumAndCodeAlpha(Integer ebTtTracingNum, String codeAlpha);

    @Transactional
    @Modifying
    @Query("update EbTTPslApp psl set psl.dateActuelle = ?2, psl.commentaire = ?3, psl.city = ?4, psl.xEcCountry = ?5, psl.validated = ?6 where psl.ebTtPslAppNum = ?1")
    public Integer updateEbPslApp(
        Integer ebTtPslAppNum,
        Date dateActuelle,
        String commentaire,
        String city,
        EcCountry xEcCountry,
        Boolean validated);

    @Transactional
    @Modifying
    @Query("update EbTTPslApp psl set psl.dateEstimee = :dateEstimee where psl.ebTtPslAppNum = :ebTtPslAppNum")
    public Integer updateEbPslAppDateEstimate(
        @Param("ebTtPslAppNum") Integer ebTtPslAppNum,
        @Param("dateEstimee") Date dateEstimee);

    @Query("select psl.dateEstimee from EbTTPslApp psl where psl.ebTtPslAppNum = :ebTtPslAppNum")
    public Date recupDateEstimate(@Param("ebTtPslAppNum") Integer ebTtPslAppNum);

    @Query("select psl.Leadtime from EbTTPslApp psl where psl.ebTtPslAppNum = :ebTtPslAppNum")
    public Integer recupLeadtime(@Param("ebTtPslAppNum") Integer ebTtPslAppNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbTTPslApp psl WHERE psl.xEbTrackTrace.ebTtTracingNum IN (?1)")
    public Integer deleteListEbPslAppByEbTracingNumIn(List<Integer> listEbTracingNum);

    @Modifying
    @Query("UPDATE EbTTPslApp psl SET psl.dateEstimee = ?3 WHERE psl.codeAlpha = ?2 AND psl.xEbTrackTrace.ebTtTracingNum IN (?1)")
    public Integer updateEstimatedDate(List<Integer> listEbTrackTraceNum, String codeAlpha, Date date);

    @Transactional
    @Query("select psl.ebTtPslAppNum from EbTTPslApp psl where psl.xEbTrackTrace.ebTtTracingNum = :tracingNum and psl.codeAlpha = :codeAlpha")
    public Integer selectebTtPslAppNumByebTtTracingNumAndCodeAlpha(
        @Param("tracingNum") Integer ebTtTracingNum,
        @Param("codeAlpha") String codeAlpha);

    public List<EbTTPslApp> findAllByxEbTrackTraceEbTtTracingNum(Integer ebTtTracingNum);

    @Transactional
    @Query("select psl from EbTTPslApp psl where psl.treatForDeviation = false")
    public List<EbTTPslApp> findAllByTreatForDeviation();

    @Query("SELECT DISTINCT new EbTTPslApp(psl.codeAlpha, psl.libelle) FROM EbTTPslApp psl LEFT JOIN psl.xEbTrackTrace tt WHERE tt.xEbChargeurCompagnie.ebCompagnieNum = ?1")
    public List<EbTTPslApp> selectPslLibelleAndCodeAlphaByCompanyNum(Integer ebCompagnieNum);

    @Transactional
    @Modifying
    @Query(
        value = "UPDATE work.eb_tt_psl_app psl set date_actuelle = ?3, commentaire = ?4, city = ?5, x_ec_country = ?6, validated = ?7 "
            + "from  work.eb_tt_tracing as tracing "
            + " WHERE tracing.eb_tt_tracing_num = psl.x_eb_tt_tracing and tracing.ref_transport = ?1 and psl.code_alpha = ?2 ",
        nativeQuery = true)

    public Integer updateEbPslAppByTransportRefAndDateActual(
        String transportRef,
        String codeAlpha,
        Date dateActuelle,
        String commentaire,
        String city,
        Integer xEcCountryNum,
        Boolean validated);

    @Transactional
    @Modifying
    @Query(
        value = "UPDATE work.eb_tt_psl_app psl set date_estimee = ?3, commentaire = ?4, city = ?5, x_ec_country = ?6 "
            + "from  work.eb_tt_tracing as tracing "
            + " WHERE tracing.eb_tt_tracing_num = psl.x_eb_tt_tracing and tracing.ref_transport = ?1 and psl.code_alpha = ?2 ",
        nativeQuery = true)

    public Integer updateEbPslAppByTransportRefAndDateEstimated(
        String transportRef,
        String codeAlpha,
        Date dateEstimated,
        String commentaire,
        String city,
        Integer xEcCountryNum);

    @Transactional
    @Modifying
    @Query(
        value = "UPDATE work.eb_tt_psl_app psl set date_negotiation = ?3, commentaire = ?4, city = ?5, x_ec_country = ?6 "
            + "from  work.eb_tt_tracing as tracing "
            + " WHERE tracing.eb_tt_tracing_num = psl.x_eb_tt_tracing and tracing.ref_transport = ?1 and psl.code_alpha = ?2 ",
        nativeQuery = true)

    public Integer updateEbPslAppByTransportRefAndDateNegotiation(
        String transportRef,
        String codeAlpha,
        Date dateNegotiation,
        String commentaire,
        String city,
        Integer xEcCountryNum);

    @Transactional
    @Modifying
    @Query(
        value = "UPDATE work.eb_tt_psl_app psl set date_actuelle = ?3, commentaire = ?4, city = ?5, x_ec_country = ?6, validated = ?7 "
            + "from  work.eb_tt_tracing as tracing "
            + " WHERE tracing.eb_tt_tracing_num = psl.x_eb_tt_tracing and tracing.custom_ref = ?1 and psl.code_alpha = ?2 ",
        nativeQuery = true)

    public Integer updateEbPslAppByCustomerRefAndDateActual(
        String customerRef,
        String codeAlpha,
        Date dateActuelle,
        String commentaire,
        String city,
        Integer xEcCountryNum,
        Boolean validated);

    @Transactional
    @Modifying
    @Query(
        value = "UPDATE work.eb_tt_psl_app psl set date_estimee = ?3, commentaire = ?4, city = ?5, x_ec_country = ?6 "
            + "from  work.eb_tt_tracing as tracing "
            + " WHERE tracing.eb_tt_tracing_num = psl.x_eb_tt_tracing and tracing.custom_ref = ?1 and psl.code_alpha = ?2 ",
        nativeQuery = true)

    public Integer updateEbPslAppByCustomerRefAndDateEstimated(
        String customerRef,
        String codeAlpha,
        Date dateEstimated,
        String commentaire,
        String city,
        Integer xEcCountryNum);

    @Transactional
    @Modifying
    @Query(
        value = "UPDATE work.eb_tt_psl_app psl set date_negotiation = ?3, commentaire = ?4, city = ?5, x_ec_country = ?6 "
            + "from  work.eb_tt_tracing as tracing "
            + " WHERE tracing.eb_tt_tracing_num = psl.x_eb_tt_tracing and tracing.custom_ref = ?1 and psl.code_alpha = ?2 ",
        nativeQuery = true)

    public Integer updateEbPslAppByCustomerRefAndDateNegotiation(
        String customerRef,
        String codeAlpha,
        Date dateNegotiation,
        String commentaire,
        String city,
        Integer xEcCountryNum);

    @Query("select psl from EbTTPslApp psl "
        + "inner join EbTtTracing t on t.ebTtTracingNum = psl.xEbTrackTrace.ebTtTracingNum "
        + "inner join EbDemande d on d.ebDemandeNum = t.xEbDemande.ebDemandeNum where d.ebDemandeNum = ?1")
    public List<EbTTPslApp> findAllByEbDemandeNum(Integer ebDemandeNum);

		@Query(
			"SELECT DISTINCT new EbTTPslApp(psl.codeAlpha, psl.libelle) FROM EbTTPslApp psl LEFT JOIN psl.xEbTrackTrace tt WHERE tt.xEbChargeurCompagnie.ebCompagnieNum in (?1)"
		)
		public List<EbTTPslApp> selectPslLibelleAndCodeAlphaByListOfCompanyNum(
			List<Integer> ebCompagnieNum
		);
}
