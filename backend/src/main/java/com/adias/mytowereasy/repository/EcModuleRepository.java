package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adias.mytowereasy.model.EcModule;


public interface EcModuleRepository extends JpaRepository<EcModule, Integer> {
    List<EcModule> findAllByOrderByOrdreAsc();
}
