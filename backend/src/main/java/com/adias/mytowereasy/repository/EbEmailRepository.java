/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import com.adias.mytowereasy.model.EbEmail;


public interface EbEmailRepository extends CommonRepository<EbEmail, Integer> {
}
