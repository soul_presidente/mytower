/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.TtEnumeration.Late;
import com.adias.mytowereasy.model.tt.EbTtTracing;


public interface EbTrackTraceRepository extends JpaRepository<EbTtTracing, Integer> {
    @Transactional
    @Modifying
    @Query("update EbTtTracing t set t.datePickup = ?2, t.dateModification = ?3 where t.xEbDemande.ebDemandeNum = ?1")
    public int updatePickupDateAndDateModification(Integer ebDemandeNum, Date datePickup, Date dateModification);

    public List<EbTtTracing> findByXEbDemande_ebDemandeNum(Integer ebDemandenum);

    @Transactional
    @Query("select t.configPsl from EbTtTracing t where t.ebTtTracingNum = :tracingNum")
    public String SelectebTtTracingConfigPsl(@Param("tracingNum") Integer ebTtTracingNum);

    @Transactional
    @Query("select t.codeAlphaPslCourant from EbTtTracing t where t.ebTtTracingNum = ?1")
    public String selectebTtTracingCodeAlphaPslCourant(Integer ebTtTracingNum);

    @Query("SELECT new EbTtTracing(t.ebTtTracingNum, t.codeAlphaPslCourant, t.xEbDemande.ebDemandeNum, t.configPsl) FROM EbTtTracing t WHERE t.xEbDemande.ebDemandeNum IN ?1")
    public List<EbTtTracing> findByXEbDemande_ebDemandeNumIn(List<Integer> listEbDemandeNum);

    @Transactional
    @Modifying
    @Query("update EbTtTracing t set t.late = ?2 where t.ebTtTracingNum = ?1")
    public int updateLateTracing(Integer ebTtTracingNum, Late lateOrNotLate);

    @Query("SELECT new EbDemande(d.ebDemandeNum, d.refTransport, d.xEcStatut, d.codeAlphaPslCourant, d.customerReference) FROM EbTtTracing trc LEFT JOIN trc.xEbDemande AS d WHERE trc.ebTtTracingNum = ?1")
    public EbDemande selectEbDemandeByEbTracingNum(Integer ebTracingNum);

    @Query("SELECT d FROM EbTtTracing trc LEFT JOIN trc.xEbDemande AS d WHERE trc.ebTtTracingNum = :tracingId")
    public EbDemande selectEbDemandeDetailsByEbTracingNum(@Param("tracingId") Integer tracingId);

    @Query("SELECT tt.ebTtTracingNum FROM EbTtTracing tt WHERE tt.xEbDemande.ebDemandeNum = ?1")
    public Object[] selectListEbTrackTraceNumByEbDemandeNum(Integer ebDemandeNum);

    @Query("SELECT tr.ebTtTracingNum FROM EbTtTracing tr INNER JOIN tr.xEbDemande d WHERE d.ebDemandeNum = ?1")
    public Object[] selectListEbTracingNumByEbDemandeNum(Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbTtTracing tr WHERE tr.ebTtTracingNum IN (?1)")
    public Integer deleteListEbTracingByEbTracingNumIn(List<Integer> listEbTracingNum);

    @Query("SELECT new EbCompagnie(tt.xEbChargeurCompagnie.ebCompagnieNum) FROM EbTtTracing tt WHERE tt.ebTtTracingNum = ?1")
    public EbCompagnie findEbCompagnieChargeurByEbDemandeNum(Integer ebTtTracingNum);

    @Transactional
    @Modifying
    @Query("update EbTtTracing tt set tt.codeLastPsl = ?2, tt.libelleLastPsl = ?3, tt.dateLastPsl = ?4 where tt.ebTtTracingNum = ?1")
    public int updatePslEbTtTracing(Integer ebDemandeNum, Integer codeLastPsl, String libelleLastPsl, Date dateLastPsl);

    @Query("SELECT new EbTtTracing(t.ebTtTracingNum, t.codeAlphaPslCourant, t.xEbDemande.ebDemandeNum, t.configPsl) FROM EbTtTracing t WHERE t.ebTtTracingNum IN ?1")
    public List<EbTtTracing> findListEbDemandeByEbTtTracingNumIn(List<Integer> bTtTracingNumIn);

    @Query("SELECT new EbTtTracing(t.ebTtTracingNum, t.codeAlphaPslCourant, t.xEbDemande.ebDemandeNum, t.configPsl,t.customerReference,t.refTransport,t.customRef) FROM EbTtTracing t WHERE t.customerReference IN ?1")
    public List<EbTtTracing> findListEbTracingByUnitRef(List<String> listUnitRef);

    @Query("SELECT new EbTtTracing(t.ebTtTracingNum, t.codeAlphaPslCourant, t.xEbDemande.ebDemandeNum, t.configPsl,t.customerReference,t.refTransport,t.customRef) FROM EbTtTracing t WHERE t.refTransport IN ?1")
    public List<EbTtTracing> findListEbTracingByTransportRef(List<String> listTransportRef);

    public List<EbTtTracing> findByRefTransport(String refTransport);

    @Transactional
    @Modifying
    @Query("update EbTtTracing t set t.codeAlphaPslCourant = ?2, t.libellePslCourant = ?3, t.dateModification = ?4 where t.ebTtTracingNum = ?1")
    public int updatePslCourant(
        Integer ebTtTracingNum,
        String codeAlphaPslCourant,
        String libellePslCourant,
        Date dateModification);

    @EntityGraph(attributePaths = {
        "xEbDemande", "listEbPslApp"
    })
    public List<EbTtTracing> findByxEbDemande_ebDemandeNumIn(List<Integer> listDemandeNum);

    @Transactional
    @Modifying
    @Query("update EbTtTracing t set t.dateLastPsl = ?2 where t.ebTtTracingNum = ?1")
    public int updateDateLastPsl(Integer ebTtTracingNum, Date dateLastPsl);

    @Transactional
    @Modifying
    @Query("update EbTtTracing t set t.dateLastPsl = ?5, t.codeAlphaPslCourant = ?2, t.libellePslCourant = ?3, t.dateModification = ?4 where t.ebTtTracingNum = ?1")
    public int updatePslCourantAndDateLastPsl(
        Integer ebTtTracingNum,
        String codeAlphaPslCourant,
        String libellePslCourant,
        Date dateModification,
        Date dateLastPsl);

    @Query("SELECT COUNT(t.xEbUserCt)  FROM EbTtTracing t WHERE t.xEbUserCt in ?1 ")
    public Long checkIfUserExistOnEbTtTracing(EbUser id);

    @Transactional
    @Modifying
    @Query(
        value = "update work.eb_tt_tracing set x_eb_etablissement_chargeur=?2 "
            + "where regexp_split_to_array(?3, ',') && (regexp_split_to_array(list_labels,',')) "
            + "and x_eb_etablissement_chargeur = ?1",
        nativeQuery = true)
    public int updateTrackTraceEtablissement(Integer oldEtablissement, Integer newEtablissement, String listLabels);

    @Transactional
    @Modifying
    @Query("update EbTtTracing t set t.listControlRule = ?2 WHERE t.ebTtTracingNum = ?1")
    public int updateListControlRule(Integer ebTtTracingNum, String rules);

    @Query("select t.dateLastPsl from EbTtTracing t where t.xEbDemande.ebDemandeNum = :ebDemandenum")
    public List<Date> selectDateLastPslByDemandeNum(@Param("ebDemandenum") Integer ebDemandenum);

    @Query("SELECT t.ebTtTracingNum FROM EbTtTracing t where t.xEbDemande.ebDemandeNum = :tracingNum ")
    public List<Integer> findListTracingNumByEbDemandeNum(@Param("tracingNum") Integer ebDemandenum);

    @Query("SELECT distinct et.listFlag from EbTtTracing et LEFT JOIN et.xEbTransporteur AS u where et.listFlag is not null and et.listFlag <> '' and u.ebUserNum = :ebUserNum")
    public List<String> selectListFlagForCarrier(@Param("ebUserNum") Integer ebUserNum);

    @Query("SELECT distinct et.listFlag from EbTtTracing et LEFT JOIN et.xEbUserCt AS u where et.listFlag is not null and et.listFlag <> '' and u.ebUserNum = :ebUserNum")
    public List<String> selectListFlagForControlTower(@Param("ebUserNum") Integer ebUserNum);

    public boolean existsByXEbDemandeEbDemandeNum(Integer id);
}
