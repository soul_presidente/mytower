/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbFavori;


@Repository
public interface EbFavoriRepository extends CommonRepository<EbFavori, Integer> {
    List<EbFavori> findAllByModuleAndUser_ebUserNum(Integer module, Integer ebUserNum);

    List<EbFavori>
        findAllByModuleAndUserEbUserNumAndIdObjectIn(Integer module, Integer ebUserNum, List<Integer> listIdObject);

    EbFavori findOneByModuleAndIdObjectAndUser_ebUserNum(Integer module, Integer idObject, Integer ebUserNum);

    List<EbFavori> findAllByUser_ebUserNum(Integer ebUserNum);
}
