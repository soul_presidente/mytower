package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbAdditionalCost;


@Repository
public interface EbAdditionalCostRepository extends JpaRepository<EbAdditionalCost, Integer> {
    @Query("update EbAdditionalCost e set e.actived = false where e.ebAdditionalCostNum = ?1")
    @Modifying
    @Transactional
    public int desactivateAdditionalCost(Integer ebAdditionalCostNum);

    public boolean existsByxEbCompagnie_ebCompagnieNumAndCodeIgnoreCase(Integer ebCompagnieNum, String code);

    @Query("SELECT c FROM EbAdditionalCost c WHERE c.actived = true  AND c.xEbCompagnie.ebCompagnieNum = ?1")
    public List<EbAdditionalCost> selectEbAdditionalCostByebCompagnieNum(Integer ebCompagnieNum);

    @Query("SELECT additionalCost FROM EbAdditionalCost additionalCost WHERE  additionalCost.libelle = ?1")
    public EbAdditionalCost selectEbAdditionalCostByLibelle(String libelle);
}
