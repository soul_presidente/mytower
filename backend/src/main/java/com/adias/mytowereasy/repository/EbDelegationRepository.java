/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adias.mytowereasy.model.EbDelegation;


public interface EbDelegationRepository extends JpaRepository<EbDelegation, Integer> {
    public EbDelegation findFirstByUserConnect_ebUserNumOrderByEbDelegationNumDesc(Integer ebUserNum);
}
