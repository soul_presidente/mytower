package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbUserTag;


public interface EbUserTagRepository extends JpaRepository<EbUserTag, Integer> {
    @Transactional
    @Modifying
    @Query("update EbUserTag u set u.activated = false where u.guest.ebUserNum = :guest and u.xEbCompanieHost.ebCompagnieNum = :ebCompagnieNum and u.activated = true")
    public int updateActivated(@Param("ebCompagnieNum") Integer ebCompagnieNum, @Param("guest") Integer guestNum);
}
