/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.tt.EbTtCategorieDeviation;


@Repository
public interface EbTtCategorieDeviationRepository extends CommonRepository<EbTtCategorieDeviation, Integer> {
    public EbTtCategorieDeviation getFirstByCode(String code);

    @Query("SELECT c  FROM EbTtCategorieDeviation c WHERE c.typeEvent = ?1 or c.typeEvent is null")
    public List<EbTtCategorieDeviation> findEbTtCategorieDeviationByTypeOfEvent(Integer typeOfEvent);

    public EbTtCategorieDeviation findOneByTypeEventAndLibelle(Integer typeOfEvent, String libelle);
}
