/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbUser;


public interface EbUserRepository extends CommonRepository<EbUser, Integer> {
    @EntityGraph(attributePaths = {
        "ebUserProfile"
    })
    public Optional<EbUser> findUserByEbUserNum(Integer ebUserNum);

    public boolean existsByEmail(String email);

    public boolean existsByUsername(String username);

    public EbUser findOneByEbUserNum(Integer ebUserNum);

    public List<EbUser> findAll();

    public List<EbUser> findAllByAdminAndEbEtablissement(Boolean admin, Integer ebEtablissementNum);

    @EntityGraph(value = "EbUser.detail")
    public EbUser findOneByEmailIgnoreCase(String email);

    @EntityGraph(value = "EbUser.detail")
    public EbUser getOneByEbUserNum(Integer ebUserNum);

    // public List<EbUser> findByEbEtablissement(EbEtablissement
    // ebEtablissement);

    public EbUser findOneByResetToken(String resetToken);

    // public EbUser findByUsername(String username);

    public EbUser findOneByActiveToken(String activeToken);

    public EbUser findByEmail(String email);

    public EbUser findByUsername(String username);

    public List<EbUser> findByEmailContaining(String email);

    public Integer countByEbEtablissement_ebEtablissementNum(Integer ebEtablissementNum);

    @Transactional
    @Modifying
    @Query("update EbUser u set u.status = :status where u.ebUserNum = :iduser")
    public int updateStatusEbUser(@Param("iduser") Integer ebUserNum, @Param("status") Integer status);

    @Transactional
    @Modifying
    @Query("update EbUser u set u.admin = :admin, u.superAdmin = :superadmin where u.ebUserNum = :iduser")
    public int updateAdminAndSuperAdminUser(
        @Param("iduser") Integer ebUserNum,
        @Param("admin") Boolean admin,
        @Param("superadmin") Boolean superAdmin);

    @Query("SELECT email FROM EbUser WHERE role = ?1")
    public List<String> selectEmailCT(Integer role);

    @Transactional
    @Modifying
    @Query("update EbUser u set u.admin = ?2, u.superAdmin = ?3, u.status = ?4, u.nom = ?5, u.prenom = ?6, u.email = ?7, u.listCategories = ?8 where u.ebUserNum = ?1")
    public int updateUserDetails(
        Integer ebUserNum,
        Boolean admin,
        Boolean superAdmin,
        Integer status,
        String nom,
        String prenom,
        String email,
        List<EbCategorie> listCategories);

    @Query("SELECT u FROM EbUser u WHERE (u.ebCompagnie.ebCompagnieNum = :ebCompagnieNum and superAdmin = true) or (u.ebEtablissement.ebEtablissementNum = :ebEtablissementNum and (superAdmin = true or admin = true))")
    public List<EbUser> selectSuperAdmin(
        @Param("ebCompagnieNum") Integer ebCompagnieNum,
        @Param("ebEtablissementNum") Integer ebEtablissementNum);

    // cette methode concerne la séléction des admin et super admin de la
    // société selon le ebCompagnieNum ( la méthode précédente faite la
    // séléction selon ebCompagnieNum et ebEtablissementNum alors lors de la
    // création d'un user avec une etablissement inexistent le
    // ebEtablissementNum est toujours null )
    @Query("SELECT u FROM EbUser u WHERE u.ebCompagnie.ebCompagnieNum = :ebCompagnieNum and superAdmin = true")
    public List<EbUser> selectSuperAdmin(@Param("ebCompagnieNum") Integer ebCompagnieNum);

    @Query("SELECT COUNT (u) FROM EbUser u WHERE u.ebEtablissement.ebEtablissementNum = :ebEtablissementNum and admin = true")
    public Integer CountEbUserAdminOfEtablissement(@Param("ebEtablissementNum") Integer ebEtablissementNum);

    @Query("SELECT COUNT (u) FROM EbUser u WHERE u.ebEtablissement.ebEtablissementNum = :ebEtablissementNum")
    public Integer CountEbUserOfEtablissement(@Param("ebEtablissementNum") Integer ebEtablissementNum);

    @Query("SELECT email FROM EbUser u WHERE u.ebCompagnie.ebCompagnieNum = :ebCompagnieNum")
    public List<String> emailCompagnie(@Param("ebCompagnieNum") Integer ebCompagnieNum);

    @Query("SELECT email FROM EbUser u WHERE u.ebUserNum = :ebUserNum")
    public String emailUser(@Param("ebUserNum") Integer ebUserNum);

    @Query("SELECT email FROM EbUser u WHERE u.ebCompagnie.ebCompagnieNum in :ebCompagnieNums")
    public List<String> emailCompagnieList(@Param("ebCompagnieNums") List<Integer> ebCompagnieNums);

    @Transactional
    @Modifying
    @Query("update EbUser u set u.password = ?2, u.resetToken = ?3, u.status = ?4 where u.ebUserNum = ?1")
    public int updatePasswordAndResetTokenEbUser(Integer ebUserNum, String password, String resetToken, Integer status);

    @Transactional
    @Modifying
    @Query("update EbUser u set u.password = ?2 where u.ebUserNum = ?1")
    public int updatePasswordEbUser(Integer ebUserNum, String password);

    @Query("SELECT new EbUser(u.ebUserNum, u.listCategories) FROM EbUser u WHERE u.ebUserNum = ?1")
    public EbUser selectListCategoriesByEbUserNum(Integer ebUserNum);

    @Query("select u from EbUser u where u.service in(0,2)")
    public List<EbUser> findAllTransporteur();

    @Query("select u.language from EbUser u where lower(u.email) = lower(?1)")
    public String getLanguageByEmail(String email);

    @Query("select new EbUser(u.ebUserNum, u.nom, u.prenom, u.email, u.password) from EbUser u where u.ebUserNum = ?1")
    public EbUser getUserLogin(Integer ebUserNum);

    @Transactional
    @Modifying
    @Query("update EbUser u set u.language = ?2 where u.ebUserNum = ?1")
    public int updateUserLanguage(Integer ebUserNum, String language);

    @Transactional
    @Modifying
    @Query("update EbUser u set u.password = ?2 where u.ebUserNum = ?1")
    public int updateUserPassword(Integer ebUserNum, String password);

    @Transactional
    @Modifying
    @Query("update EbUser u set u.resetToken = ?2 where u.ebUserNum = ?1")
    public int updateUserResetToken(Integer ebUserNum, String resetToken);

    @Transactional
    @Modifying
    @Query("update EbUser u set u.language = ?2, u.fuseauHoraire= ?3 where u.ebUserNum = ?1")
    public int updateUserLanguageAndFuseauHoraire(Integer ebUserNum, String language, Integer fuseauHoraire);

    @Query("Select new EbUser(u.ebUserNum, u.nom,u.prenom) from EbUser u where u.ebUserProfile.ebUserProfileNum= ?1")
    public List<EbUser> getbyEbUserProfileNum(Integer EbUserProfileNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbUser u WHERE u.ebUserNum in ?1")
    public Integer deleteSelectedUsers(ArrayList<Integer> ids);

    @Query("SELECT u FROM EbUser u WHERE u.email = :userNameOrEmail OR u.username = :userNameOrEmail")
    public EbUser selectByUsernameOrEmail(@Param("userNameOrEmail") String userNameOrEmail);

    @Query("SELECT u.email from EbUser u where u.ebUserNum in :usersIds")
    public List<String> selectEmailUsersInListNums(@Param("usersIds") List<Integer> usersIds);

    @Query("SELECT u.email from EbUser u where u.email in :mails")
    public List<String> getUsersWithEmails(@Param("mails") List<String> mails);

    public List<EbUser> findByEbEtablissement_ebEtablissementNumIn(List<Integer> etablissementNums);

    @Query("SELECT new EbUser(u.ebUserNum, u.nom,u.prenom , u.email)  FROM EbUser u WHERE u.ebCompagnie.ebCompagnieNum = :ebCompagnieNum")
    public List<EbUser> selectUserByCompagnie(@Param("ebCompagnieNum") Integer ebCompagnieNum);
}
