package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adias.mytowereasy.airbus.model.QrGroupeObject;
import com.adias.mytowereasy.model.EbTypeRequest;
import com.adias.mytowereasy.model.EbUser;


public interface EbTypeRequestRepository extends CommonRepository<EbTypeRequest, Integer> {
    @Query("select new com.adias.mytowereasy.airbus.model.QrGroupeObject(tr.ebTypeRequestNum, tr.reference)  from EbTypeRequest tr where  tr.xEbCompagnie.ebCompagnieNum = :ebCompagnieNum ")
    public List<QrGroupeObject>
        findAllTypeRequestByCompagnieCodeLibelle(@Param("ebCompagnieNum") Integer ebCompagnieNum);

    public EbTypeRequest findByReferenceIgnoreCaseAndXEbCompagnie_ebCompagnieNum(String ref, Integer ebCompagnieNum);

    @Query("select tr from EbTypeRequest tr where tr.reference like '%'|| ?1 ||'%' ")
    public List<EbTypeRequest> findAllWhereReferenceLike(String term);

    @Query("select tr.ebTypeRequestNum from EbTypeRequest tr where tr.libelle in :libelle and tr.xEbCompagnie.ebCompagnieNum =:ebCompagnieNum")
    public List<Integer>
        getEbTypeRequestIds(@Param("libelle") List<String> reference, @Param("ebCompagnieNum") Integer ebCompagnieNum);

    @Query("select tr from EbTypeRequest tr where tr.xEbUser in :listUsers")
    public List<EbTypeRequest> findAllByListUsers(@Param("listUsers") List<EbUser> listUsers);
}
