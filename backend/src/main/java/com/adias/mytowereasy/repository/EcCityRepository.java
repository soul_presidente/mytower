/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adias.mytowereasy.model.EcCity;


public interface EcCityRepository extends JpaRepository<EcCity, Integer> {
    public List<EcCity> findAllByOrderByLibelleDesc();

    public List<EcCity> findByEcCountryEcCountryNum(Integer ecCountryNum);
}
