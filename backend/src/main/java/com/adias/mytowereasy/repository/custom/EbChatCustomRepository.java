/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository.custom;

import java.util.List;

import com.adias.mytowereasy.model.custom.EbChatCustom;
import com.adias.mytowereasy.repository.CommonRepository;


public interface EbChatCustomRepository extends CommonRepository<EbChatCustom, Integer> {
    public List<EbChatCustom> findAllByModuleAndIdFicheAndIdChatComponentOrderByDateCreationDesc(
        Integer module,
        Integer idFiche,
        Integer idChatComponent);
}
