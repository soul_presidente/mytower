package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.chanel.model.EbWeekTemplate;


@Repository
public interface EbWeekTemplateRepository extends JpaRepository<EbWeekTemplate, Integer> {
    List<EbWeekTemplate> findAll();
}
