package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.adias.mytowereasy.model.EbQmDeviationSettings;


public interface EbQmDeviationSettingsRepository extends JpaRepository<EbQmDeviationSettings, Integer> {
    public List<EbQmDeviationSettings> findByXEbCompagnie(Integer ebCompagnieNum);
}
