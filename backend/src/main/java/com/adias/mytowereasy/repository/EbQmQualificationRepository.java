/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.qm.EbQmQualification;


@Repository
public interface EbQmQualificationRepository extends JpaRepository<EbQmQualification, Integer> {
}
