package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adias.mytowereasy.model.EbIncoterm;


public interface EbIncotermRepository extends JpaRepository<EbIncoterm, Integer> {
    @Query("Select i from EbIncoterm i where i.libelle = :libelleIncoterm AND i.xEbCompagnie.ebCompagnieNum = :xEbCompanieNum")
    List<EbIncoterm> findOneByLibelleAndCompagnie(
        @Param("libelleIncoterm") String libelleIncoterm,
        @Param("xEbCompanieNum") Integer xEbCompanieNum);

    EbIncoterm findFirstByLibelleAndXEbCompagnie_ebCompagnieNum(String incoterm, Integer ebCompagnieNum);

    List<EbIncoterm> findByXEbCompagnie_ebCompagnieNum(Integer ebCompagnieNum);
}
