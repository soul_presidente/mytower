/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository.custom;

import com.adias.mytowereasy.model.custom.EbCustomDeclaration;
import com.adias.mytowereasy.repository.CommonRepository;


public interface EbDeclarationRepository extends CommonRepository<EbCustomDeclaration, Integer> {
}
