package com.adias.mytowereasy.repository;

import com.adias.mytowereasy.model.EbTemplateDocuments;


public interface EbTemplateDocumentsRepository extends CommonRepository<EbTemplateDocuments, Integer> {

}
