/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.repository;

import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.Predicate;

import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbRelation;
import com.adias.mytowereasy.model.EbUser;


public interface EbRelationRepository extends CommonRepository<EbRelation, Integer> {
    public List<EbRelation> findAllByHost(EbUser ebUser);

    @EntityGraph(attributePaths = {
        "guest", "host"
    })
    public List<EbRelation> findAll(Predicate predicate);

    public EbRelation findOneByGuest_ebUserNumAndHost_ebUserNum(Integer ebUserNumGuest, Integer ebUserNumHost);

    @Query("SELECT new EbRelation(d.ebRelationNum, d.host.ebUserNum, d.guest.ebUserNum) FROM EbRelation d WHERE d.etat = ?3 and (d.guest.ebUserNum = ?1 and d.host.ebUserNum IN(?2) OR d.guest.ebUserNum IN (?2) AND d.host.ebUserNum = ?1)")
    public List<EbRelation> findRelationsWithUser(Integer ebUserNum, List<Integer> listUserNum, Integer etat);

    public List<EbRelation> findAllByEmail(String email);

    public List<EbRelation> findAllByEmailAndHost_ebUserNum(String email, Integer hostEbUserNum);

    @Transactional
    @Modifying
    @Query(
        value = "update EbRelation e set e.xEbEtablissementHost=:newEtablissement, e.xEbEtablissement=:newEtablissement"
            + " where e.host.ebUserNum=:userId")
    public void updateHostCommunityEtablissement(
        @Param("userId") Integer userId,
        @Param("newEtablissement") EbEtablissement newEtablissement);

    @Transactional
    @Modifying
    @Query(value = "update EbRelation e set e.xEbEtablissement=:newEtablissement where e.guest.ebUserNum=:userId")
    public void updateGuessCommunityEtablissement(
        @Param("userId") Integer userId,
        @Param("newEtablissement") EbEtablissement newEtablissement);

		@Query(
			"select e.xEbEtablissement.ebEtablissementNum from EbRelation e where e.host.ebUserNum=:userId"
		)
		public List<Integer> getEtablissementsByHostUserId(@Param("userId") Integer userId);
}
