package com.adias.mytowereasy.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.model.EbUserThemes;


@Repository
public interface EbThemesConfigRepository extends JpaRepository<EbUserThemes, Integer> {
    @Query("select t from EbUserThemes t where t.xEbCompagnie.ebCompagnieNum = ?1")
    public EbUserThemes getCompagnieTheme(Integer ebCompagnieNum);
}
