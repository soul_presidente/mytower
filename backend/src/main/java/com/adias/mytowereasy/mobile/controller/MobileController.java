package com.adias.mytowereasy.mobile.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "mobile/")
@CrossOrigin("*")
public class MobileController {
    @RequestMapping(value = "ping", method = RequestMethod.GET, produces = "application/json")
    public String test() {
        return "pong... oh you're a mobile device ! Welcome !";
    }
}
