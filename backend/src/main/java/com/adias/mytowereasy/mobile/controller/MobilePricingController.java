package com.adias.mytowereasy.mobile.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.mobile.dto.MobilePricingListDemandeQueryDTO;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.service.PricingService;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@RestController
@RequestMapping(value = "mobile/pricing")
@CrossOrigin("*")
public class MobilePricingController {
    @Autowired
    PricingService pricingService;

    @RequestMapping(value = "listDemande", method = RequestMethod.POST, produces = "application/json")
    public List<EbDemande> listDemande(MobilePricingListDemandeQueryDTO params) throws Exception {
        SearchCriteriaPricingBooking paramsService = new SearchCriteriaPricingBooking();

        paramsService.setModule(params.getModule());
        paramsService.setStart(params.getStart());
        paramsService.setSize(params.getLength());
        paramsService.setEbUserNum(params.getEbUserNum());

        return pricingService.getListEbDemande(paramsService);
    }
}
