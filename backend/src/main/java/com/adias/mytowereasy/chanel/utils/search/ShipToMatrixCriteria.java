package com.adias.mytowereasy.chanel.utils.search;

import java.util.HashMap;
import java.util.Map;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.chanel.model.EbShipToMatrix;
import com.adias.mytowereasy.chanel.model.QEbShipToMatrix;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public class ShipToMatrixCriteria extends SearchCriteria {
    private String criteria;

    private QEbShipToMatrix qEbShipToMatrix = QEbShipToMatrix.ebShipToMatrix;

    private Map<String, OrderSpecifier<?>> orderMapASC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, OrderSpecifier<?>> orderMapDESC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, Path<?>> orderMap = new HashMap<String, Path<?>>();

    private boolean isCount = false;

    public void initialize() {
        orderMap.put("ebShipToMatrixNum", qEbShipToMatrix.shipTo);
        orderMap.put("shipTo", qEbShipToMatrix.shipTo);
        orderMap.put("referentADV", qEbShipToMatrix.referentADV);
        orderMap.put("ebGroupName", qEbShipToMatrix.ebGroupName);
        orderMap.put("etablissementName", qEbShipToMatrix.etablissementName);
        orderMap.put("statut", qEbShipToMatrix.statut);
        orderMap.put("login", qEbShipToMatrix.login);
        orderMap.put("newUser", qEbShipToMatrix.newUser);

        orderMapASC.put("ebShipToMatrixNum", qEbShipToMatrix.ebShipToMatrixNum.asc());
        orderMapASC.put("shipTo", qEbShipToMatrix.shipTo.asc());
        orderMapASC.put("referentADV", qEbShipToMatrix.referentADV.asc());
        orderMapASC.put("ebGroupName", qEbShipToMatrix.ebGroupName.asc());
        orderMapASC.put("etablissementName", qEbShipToMatrix.etablissementName.asc());
        orderMapASC.put("statut", qEbShipToMatrix.statut.asc());
        orderMapASC.put("login", qEbShipToMatrix.login.asc());
        orderMapASC.put("newUser", qEbShipToMatrix.newUser.asc());

        orderMapDESC.put("ebShipToMatrixNum", qEbShipToMatrix.ebShipToMatrixNum.desc());
        orderMapDESC.put("shipTo", qEbShipToMatrix.shipTo.desc());
        orderMapDESC.put("referentADV", qEbShipToMatrix.referentADV.desc());
        orderMapDESC.put("ebGroupName", qEbShipToMatrix.ebGroupName.desc());
        orderMapDESC.put("etablissementName", qEbShipToMatrix.etablissementName.desc());
        orderMapDESC.put("statut", qEbShipToMatrix.statut.desc());
        orderMapDESC.put("login", qEbShipToMatrix.login.desc());
        orderMapDESC.put("newUser", qEbShipToMatrix.newUser.desc());
    }

    public void clear() {
        orderMap.clear();
        orderMapASC.clear();
        orderMapDESC.clear();
    }

    public JPAQuery<EbShipToMatrix> applyCriteria(JPAQuery<EbShipToMatrix> query) {
        clear();
        initialize();

        BooleanBuilder where = new BooleanBuilder();

        if (this.searchterm != null) {
            criteria = searchterm.trim().toLowerCase();
            where
                .andAnyOf(
                    qEbShipToMatrix.etablissementName.toLowerCase().contains(criteria),
                    qEbShipToMatrix.referentADV.toLowerCase().contains(criteria),
                    qEbShipToMatrix.shipTo.toLowerCase().contains(criteria),
                    qEbShipToMatrix.ebGroupName.toLowerCase().contains(criteria));
        }

        query.where(where);

        if (this.pageNumber != null && this.size != null && !isCount) {
            query.limit(this.size).offset(this.getPageNumber() * (this.getSize() != null ? this.getSize() : 1));
        }

        if (this.orderedColumn != null && !isCount) {
            query.groupBy(qEbShipToMatrix.ebShipToMatrixNum);

            if (this.ascendant == true) {
                query.groupBy(qEbShipToMatrix.ebShipToMatrixNum);
                query.groupBy(this.orderMap.get(this.orderedColumn));
                query.orderBy(this.orderMapASC.get(this.orderedColumn));
            }
            else {
                query.groupBy(qEbShipToMatrix.ebShipToMatrixNum);
                query.groupBy(this.orderMap.get(this.orderedColumn));
                query.orderBy(this.orderMapDESC.get(this.orderedColumn));
            }

        }
        else {
            this.orderedColumn = "ebShipToMatrixNum";
            query.groupBy(qEbShipToMatrix.ebShipToMatrixNum);
            query.orderBy(this.orderMapDESC.get(this.orderedColumn));
        }

        if (this.isCount) {
            this.isCount = false;
        }

        return query;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public boolean isCount() {
        return isCount;
    }

    public void setCount(boolean isCount) {
        this.isCount = isCount;
    }
}
