package com.adias.mytowereasy.chanel.model;

import javax.persistence.Entity;

import com.adias.mytowereasy.model.EbUser;


@Entity
public class EbUserChanel extends EbUser {
    private static final long serialVersionUID = 1L;

    private String shipTo;

    private String mailRefAdv;

    public String getShipTo() {
        return shipTo;
    }

    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    public String getMailRefAdv() {
        return mailRefAdv;
    }

    public void setMailRefAdv(String mailRefAdv) {
        this.mailRefAdv = mailRefAdv;
    }
}
