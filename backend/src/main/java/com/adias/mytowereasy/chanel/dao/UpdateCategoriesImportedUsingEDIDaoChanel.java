package com.adias.mytowereasy.chanel.dao;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbLabel;


public interface UpdateCategoriesImportedUsingEDIDaoChanel {
    EbLabel getLabelForUserChanel(String shipTo, EbEtablissement etablissement, EbCategorie categorie);

    EbCategorie getCategoryShipToForCPB();

    EbCategorie getCategoryShipToOrCreateIfNotExists(EbEtablissement etablissement);
}
