package com.adias.mytowereasy.chanel.service;

import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


public interface TrackTraceServiceChanel {
    void insertTrackTraceFromEDI(SearchCriteriaPricingBooking criteria) throws Exception;
}
