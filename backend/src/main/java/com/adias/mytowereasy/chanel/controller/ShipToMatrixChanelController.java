package com.adias.mytowereasy.chanel.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.chanel.model.EbShipToMatrix;
import com.adias.mytowereasy.chanel.service.ShipToMatrixService;
import com.adias.mytowereasy.chanel.utils.search.ShipToMatrixCriteria;


@RestController
@RequestMapping(value = "/api/shiptomatrix")
@CrossOrigin("*")
public class ShipToMatrixChanelController {
    @Autowired
    ShipToMatrixService shipToMatrixService;

    @RequestMapping(value = "list-shipto", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getListShipTo(@RequestBody(required = false) ShipToMatrixCriteria criteria) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        List<EbShipToMatrix> resultOrder = shipToMatrixService.getListShipToMatrix(criteria);
        result.put("count", shipToMatrixService.getCountShipToMatrix(criteria));
        result.put("data", resultOrder);
        return result;
    }
}
