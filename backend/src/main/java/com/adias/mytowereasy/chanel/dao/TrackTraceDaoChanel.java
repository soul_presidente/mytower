package com.adias.mytowereasy.chanel.dao;

import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


public interface TrackTraceDaoChanel {
    void insertTrackTraceFromEDI(SearchCriteriaPricingBooking criteria) throws Exception;
}
