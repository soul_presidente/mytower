package com.adias.mytowereasy.chanel.service;

import java.util.List;

import com.adias.mytowereasy.chanel.model.EbShipToMatrix;
import com.adias.mytowereasy.chanel.utils.search.ShipToMatrixCriteria;


public interface ShipToMatrixService {
    public List<EbShipToMatrix> getListShipToMatrix(ShipToMatrixCriteria criteria);

    public Long getCountShipToMatrix(ShipToMatrixCriteria criteria);
}
