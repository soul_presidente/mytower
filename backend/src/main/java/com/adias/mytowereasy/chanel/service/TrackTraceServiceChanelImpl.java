package com.adias.mytowereasy.chanel.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.chanel.dao.UpdateCategoriesImportedUsingEDIDaoChanel;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.service.CategorieService;
import com.adias.mytowereasy.service.DaoPricingTrackingService;
import com.adias.mytowereasy.service.PricingService;
import com.adias.mytowereasy.service.TrackService;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Service
public class TrackTraceServiceChanelImpl implements TrackTraceServiceChanel {
    private static final Logger logger = LoggerFactory.getLogger(TrackTraceServiceChanelImpl.class);

    @Autowired
    EntityManagerFactory entityManagerFactory;

    @Autowired
    TrackService trackService;

    @Autowired
    PricingService pricingService;

    @Autowired
    DaoPricingTrackingService daoPricingTrackingService;

    @Autowired
    protected EbDemandeRepository ebDemandeRepository;

    @Autowired
    UpdateCategoriesImportedUsingEDIDaoChanel updateCategoriesDao;

    @Autowired
    EbDemandeRepository demandeRepository;

    @Autowired
    CategorieService categorieService;

    @Override
    public void insertTrackTraceFromEDI(SearchCriteriaPricingBooking criteria) throws Exception {
        List<EbDemande> listEbDemande = new ArrayList<EbDemande>();
        criteria.setFlagDateTraitementCPB(true);
        criteria.setFlagIntegratedVia(true);
        criteria.setIntegratedMarchandiseWithoutParent(true);
        criteria.setModule(Enumeration.Module.TRANSPORT_MANAGEMENT.getCode());

        listEbDemande = daoPricingTrackingService.getListEbDemande(criteria);

        // Récuperation de la categorie label Correspondant au shipTO
        EbCategorie categorie = updateCategoriesDao.getCategoryShipToForCPB();

        for (EbDemande ebDemande: listEbDemande) {
            logger.debug("DEBUT TRAITEMENT DEMAMDE : {}", ebDemande.getEbDemandeNum());

            // Affectation des categories/labels
            EbDemande demande = ebDemandeRepository.getOne(ebDemande.getEbDemandeNum());

            // Recupération de la liste des categories/labels
            List<EbCategorie> listCategories = categorieService.getListCategoriesFromListLabels(demande, categorie);

            if (!listCategories.isEmpty()) {
                demande.setListCategoriesCron(listCategories);
            }

            // Gestion des shemas PSL
            demande = trackService.handlePsl(demande);

            // Generation des TrackTrace
            if (demande.getFlagEbtrackTrace() != null && !demande.getFlagEbtrackTrace()
                && criteria.isGenerateTrackTtrace()) {
                trackService.insertEbTrackTrace(demande);
            }

            // Mise à jour de la date traitement
            if (demande.have_categories() && demande.getxEbSchemaPsl() != null && demande.getFlagEbtrackTrace() != null
                && demande.getFlagEbtrackTrace()) {
                demandeRepository.updateDateTraitementCPBEbDemande(demande.getEbDemandeNum(), new Date());
            }

        }

    }
}
