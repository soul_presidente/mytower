package com.adias.mytowereasy.chanel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adias.mytowereasy.chanel.dao.TrackTraceDaoChanel;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Controller
@ResponseBody
@RequestMapping(value = "api/generate")
@CrossOrigin("*")
public class CreateTrackTraceController {
    @Autowired
    private TrackTraceDaoChanel trackTraceDaoChanel;

    @RequestMapping(value = "tracking", method = RequestMethod.GET, produces = "application/json")
    public String insertTrackTraceFromEDI(
        @RequestParam(required = false) Integer ebDemandeNum,
        @RequestParam(required = false) boolean generateTrackTrace)
        throws Exception {
        SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();

        if (ebDemandeNum != null) {
            criteria.setEbDemandeNum(ebDemandeNum);
        }

        criteria.setGenerateTrackTtrace(generateTrackTrace);

        trackTraceDaoChanel.insertTrackTraceFromEDI(criteria);

        return "FIN TRAITEMENT : Transformation des demandes en TrackTrace avec Demande : " + ebDemandeNum
            + " et GenerateTrackTrace : " + generateTrackTrace;
    }
}
