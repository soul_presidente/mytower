package com.adias.mytowereasy.chanel.model;

import javax.persistence.*;

import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbUserGroup;


@Entity
public class EbShipToMatrix {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ebShipToMatrixNum;

    private String shipTo;

    private String referentADV;

    private String ebGroupName;

    private String statut;

    private String newUser;

    private String login;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_group")
    private EbUserGroup xEbUserGroupe;

    private String etablissementName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement")
    private EbEtablissement xEbEtablissement;

    public Long getEbShipToMatrixNum() {
        return ebShipToMatrixNum;
    }

    public void setEbShipToMatrixNum(Long ebShipToMatrixNum) {
        this.ebShipToMatrixNum = ebShipToMatrixNum;
    }

    public String getShipTo() {
        return shipTo;
    }

    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }

    public String getReferentADV() {
        return referentADV;
    }

    public void setReferentADV(String referentADV) {
        this.referentADV = referentADV;
    }

    public String getEtablissementName() {
        return etablissementName;
    }

    public void setEtablissementName(String etablissementName) {
        this.etablissementName = etablissementName;
    }

    public EbEtablissement getxEbEtablissement() {
        return xEbEtablissement;
    }

    public void setxEbEtablissement(EbEtablissement xEbEtablissement) {
        this.xEbEtablissement = xEbEtablissement;
    }

    public String getEbGroupName() {
        return ebGroupName;
    }

    public void setEbGroupName(String ebGroupName) {
        this.ebGroupName = ebGroupName;
    }

    public EbUserGroup getxEbUserGroupe() {
        return xEbUserGroupe;
    }

    public void setxEbUserGroupe(EbUserGroup xEbUserGroupe) {
        this.xEbUserGroupe = xEbUserGroupe;
    }

    public String getStatut() {
        return statut;
    }

    public String getNewUser() {
        return newUser;
    }

    public String getLogin() {
        return login;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public void setNewUser(String newUser) {
        this.newUser = newUser;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
