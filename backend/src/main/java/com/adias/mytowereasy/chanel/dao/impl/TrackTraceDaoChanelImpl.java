package com.adias.mytowereasy.chanel.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.adias.mytowereasy.chanel.dao.TrackTraceDaoChanel;
import com.adias.mytowereasy.chanel.service.TrackTraceServiceChanel;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Component
public class TrackTraceDaoChanelImpl implements TrackTraceDaoChanel {
    @Autowired
    private TrackTraceServiceChanel trackTraceServiceChanel;

    @Override
    public void insertTrackTraceFromEDI(SearchCriteriaPricingBooking criteria) throws Exception {
        trackTraceServiceChanel.insertTrackTraceFromEDI(criteria);
    }
}
