package com.adias.mytowereasy.chanel.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.chanel.dao.ShipToMatrixDao;
import com.adias.mytowereasy.chanel.model.EbShipToMatrix;
import com.adias.mytowereasy.chanel.model.EbUserChanel;
import com.adias.mytowereasy.chanel.repository.ShipToMatrixRepository;
import com.adias.mytowereasy.chanel.utils.search.ShipToMatrixCriteria;


@Service
public class ShipToMatrixServiceImpl implements ShipToMatrixService {
    @Autowired
    ShipToMatrixDao shipToDao;

    @Autowired
    ShipToMatrixRepository shipToMatrixRepository;

    @Override
    public List<EbShipToMatrix> getListShipToMatrix(ShipToMatrixCriteria criteria) {
        List<EbShipToMatrix> listEbShipToMatrix = shipToDao.getListShipToMatrix(criteria);
        List<EbUserChanel> listEbAllUserChanel = shipToDao.getListUserChanel();

        for (EbShipToMatrix ebShipToMatrix: listEbShipToMatrix) {
            List<EbUserChanel> listUserChanel = shipToDao.getListUserChanelByShipTo(ebShipToMatrix.getShipTo());

            if (listUserChanel.size() == 0) {
                ebShipToMatrix.setStatut("User non crée");
                // shipToMatrixRepository.save(ebShipToMatrix);
            }
            else {
                int nbrActif = 0, nbrInactif = 0;
                String status = "", login = "";

                for (EbUserChanel userChanel: listUserChanel) {
                    if (userChanel.getStatus() == 1) nbrActif++;
                    else if (userChanel.getStatus() == 3) nbrInactif++;
                    login += userChanel.getUsername() + " , ";
                }

                if (nbrActif != 0 && nbrInactif != 0) {
                    if (nbrActif == 1) status = nbrActif + " inscrit";
                    else status = nbrActif + " inscrits ";

                    if (nbrInactif == 1) status += " | " + nbrInactif + " demande d'inscription";
                    else status += " | " + nbrInactif + " demandes d'inscription";
                }
                else if (nbrActif != 0) {
                    if (nbrActif == 1) status = nbrActif + " inscrit";
                    else status = nbrActif + " inscrits ";
                }
                else if (nbrInactif != 0) {
                    if (nbrInactif == 1) status = nbrInactif + " demande d'inscription";
                    else status = nbrInactif + " demandes d'inscription";
                }

                ebShipToMatrix.setStatut(status);
                if (login != "") login = login.substring(0, login.length() - 2);
                ebShipToMatrix.setLogin(login);
                // shipToMatrixRepository.save(ebShipToMatrix);
            }

        }

        return listEbShipToMatrix;
    }

    @Override
    public Long getCountShipToMatrix(ShipToMatrixCriteria criteria) {
        return shipToDao.getCountShipToMatrix(criteria);
    }
}
