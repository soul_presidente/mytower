package com.adias.mytowereasy.chanel.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "eb_wt_period", schema = "work")
public class EbWeekTemplatePeriod implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebWeektemplatePeriodNum;

    private String label;

    private Date startDate;

    private Date endDate;

    private Integer typePeriod;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_week_num")
    private EbWeekTemplate week;

    public Integer getEbWeektemplatePeriodNum() {
        return ebWeektemplatePeriodNum;
    }

    public void setEbWeektemplatePeriodNum(Integer ebWeektemplatePeriodNum) {
        this.ebWeektemplatePeriodNum = ebWeektemplatePeriodNum;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getTypePeriod() {
        return typePeriod;
    }

    public void setTypePeriod(Integer typePeriod) {
        this.typePeriod = typePeriod;
    }

    public EbWeekTemplate getWeek() {
        return week;
    }

    public void setWeek(EbWeekTemplate week) {
        this.week = week;
    }
}
