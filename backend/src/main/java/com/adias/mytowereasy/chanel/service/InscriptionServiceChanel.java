package com.adias.mytowereasy.chanel.service;

import com.adias.mytowereasy.chanel.model.EbUserChanel;


public interface InscriptionServiceChanel {
    public boolean shipToExists(String shipTo);

    public boolean registerUser(EbUserChanel user);
}
