package com.adias.mytowereasy.chanel.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.chanel.dao.ShipToMatrixDao;
import com.adias.mytowereasy.chanel.model.EbShipToMatrix;
import com.adias.mytowereasy.chanel.model.EbUserChanel;
import com.adias.mytowereasy.chanel.model.QEbShipToMatrix;
import com.adias.mytowereasy.chanel.model.QEbUserChanel;
import com.adias.mytowereasy.chanel.utils.search.ShipToMatrixCriteria;


@Component
@Transactional
public class ShipToMatrixDaoImpl implements ShipToMatrixDao {
    @PersistenceContext
    EntityManager em;

    @Autowired
    PasswordEncoder passwordEncoder;

    private QEbShipToMatrix qEbShipToMatrix = QEbShipToMatrix.ebShipToMatrix;
    private QEbUserChanel qEbUserChanel = QEbUserChanel.ebUserChanel;

    @Override
    public List<EbUserChanel> getListUserChanel() {
        List<EbUserChanel> result = null;

        try {
            JPAQuery<EbUserChanel> query = new JPAQuery<EbUserChanel>(em);
            result = query.from(qEbUserChanel).fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public EbUserChanel getUserChanelByUserNum(Integer userNum) {

        EbUserChanel result = null;

        try {
            JPAQuery<EbUserChanel> query = new JPAQuery<>(em);
            query.where(qEbUserChanel.ebUserNum.eq(userNum));
            result = query.from(qEbUserChanel).fetchFirst();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public EbShipToMatrix getEbShipToMatrix(String label) {

        EbShipToMatrix result = null;

        try {
            JPAQuery<EbShipToMatrix> query = new JPAQuery<>(em);
            query.where(qEbShipToMatrix.shipTo.eq(label));
            result = query.from(qEbShipToMatrix).fetchFirst();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<EbUserChanel> getListUserChanelByShipTo(String shipto) {

        List<EbUserChanel> result = null;

        try {
            JPAQuery<EbUserChanel> query = new JPAQuery<EbUserChanel>(em);
            query.where(qEbUserChanel.shipTo.eq(shipto));
            result = query.from(qEbUserChanel).fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<EbShipToMatrix> getListShipToMatrix(ShipToMatrixCriteria criteria) {
        List<EbShipToMatrix> result = null;

        try {
            JPAQuery<EbShipToMatrix> query = new JPAQuery<EbShipToMatrix>(em);
            if (criteria != null) query = criteria.applyCriteria(query);
            result = query.from(qEbShipToMatrix).fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public EbUserChanel geUserChanelByShipTo(String shipto) {
        EbUserChanel result = null;

        try {
            JPAQuery<EbUserChanel> query = new JPAQuery<EbUserChanel>(em);
            query.where(qEbUserChanel.shipTo.eq(shipto));
            result = query.from(qEbUserChanel).fetchFirst();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Long getCountShipToMatrix(ShipToMatrixCriteria criteria) {
        Long result = null;

        try {
            JPAQuery<EbShipToMatrix> query = new JPAQuery<EbShipToMatrix>(em);
            criteria.setCount(true);
            query = criteria.applyCriteria(query);
            result = Long.valueOf(query.from(qEbShipToMatrix).fetch().size());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
