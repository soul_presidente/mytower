package com.adias.mytowereasy.chanel.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;


@Entity
@Table(name = "eb_wt_hour", schema = "work")
public class EbWeekTemplateHour implements Serializable {
    public EbWeekTemplateHour() {
        super();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebWeektemplateHourNum;
    private String startHour;
    private String endHour;
    private String cutOffTime;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_day_num")

    private EbWeekTemplateDay day;

    public Integer getEbWeektemplateHourNum() {
        return ebWeektemplateHourNum;
    }

    public void setEbWeektemplateHourNum(Integer ebWeektemplateHourNum) {
        this.ebWeektemplateHourNum = ebWeektemplateHourNum;
    }

    public EbWeekTemplateDay getDay() {
        return day;
    }

    public void setDay(EbWeekTemplateDay day) {
        this.day = day;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }

    public String getCutOffTime() {
        return cutOffTime;
    }

    public void setCutOffTime(String cutOffTime) {
        this.cutOffTime = cutOffTime;
    }
}
