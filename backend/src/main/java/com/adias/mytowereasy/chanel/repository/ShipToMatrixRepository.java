package com.adias.mytowereasy.chanel.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.chanel.model.EbShipToMatrix;


@Repository
public interface ShipToMatrixRepository extends JpaRepository<EbShipToMatrix, Long> {

}
