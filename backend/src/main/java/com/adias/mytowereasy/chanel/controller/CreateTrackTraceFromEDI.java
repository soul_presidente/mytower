package com.adias.mytowereasy.chanel.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.adias.mytowereasy.chanel.dao.TrackTraceDaoChanel;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Component
public class CreateTrackTraceFromEDI {
    private static final Logger logger = LoggerFactory.getLogger(CreateTrackTraceFromEDI.class);

    @Autowired
    private TrackTraceDaoChanel trackTraceDaoChanel;

    @Scheduled(cron = "${chanel.transport.shipto.edi.cron}")
    public void insertTrackTraceFromEDI() throws Exception {
        SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();
        logger.debug("DEBUT TRAITEMENT : Transformation des demandes en TrackTrace");
        trackTraceDaoChanel.insertTrackTraceFromEDI(criteria);
        logger.debug("FIN TRAITEMENT : Transformation des demandes en TrackTrace");
    }
}
