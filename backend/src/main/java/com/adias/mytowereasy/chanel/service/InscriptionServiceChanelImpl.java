package com.adias.mytowereasy.chanel.service;

import static com.adias.mytowereasy.model.Statiques.CODE_CPB;
import static com.adias.mytowereasy.model.Statiques.WAITING_FOR_ALLOCATION_ETABLISSEMENT_NIC;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.chanel.dao.UpdateCategoriesImportedUsingEDIDaoChanel;
import com.adias.mytowereasy.chanel.model.EbShipToMatrix;
import com.adias.mytowereasy.chanel.model.EbUserChanel;
import com.adias.mytowereasy.chanel.model.QEbShipToMatrix;
import com.adias.mytowereasy.chanel.repository.EbUserChanelRepository;
import com.adias.mytowereasy.chanel.repository.ShipToMatrixRepository;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.UserStatus;
import com.adias.mytowereasy.repository.EbCategorieRepository;
import com.adias.mytowereasy.repository.EbLabelRepository;
import com.adias.mytowereasy.service.InscriptionService;


@Service
public class InscriptionServiceChanelImpl implements InscriptionServiceChanel {
    @PersistenceContext
    EntityManager em;

    @Autowired
    private EbCategorieRepository categorieRepository;

    @Autowired
    private EbLabelRepository labelRepository;

    @Autowired
    private InscriptionService inscriptionService;

    @Autowired
    private EbUserChanelRepository userRepository;

    @Autowired
    ShipToMatrixRepository shipToMatrixRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UpdateCategoriesImportedUsingEDIDaoChanel updateCategoriesDao;

    private QEbShipToMatrix qEbShipToMatrix = QEbShipToMatrix.ebShipToMatrix;
    private QEbCategorie qEbCategorie = QEbCategorie.ebCategorie;
    private QEbEtablissement qEbEtablissement = QEbEtablissement.ebEtablissement;
    private QEbCompagnie qEbCompagnie = QEbCompagnie.ebCompagnie;
    private QEbLabel qEbLabel = QEbLabel.ebLabel;

    @Override
    public boolean shipToExists(String shipTo) {
        JPAQuery<EbShipToMatrix> query = new JPAQuery<EbShipToMatrix>(em);

        try {
            Long result = query.where(qEbShipToMatrix.shipTo.eq(shipTo)).fetchCount();
            return result > 0;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public boolean registerUser(EbUserChanel user) {
        boolean isOK = true;

        try {
            JPAQuery<EbShipToMatrix> queryShipTo = new JPAQuery<EbShipToMatrix>(em);
            EbShipToMatrix matrix = queryShipTo
                .from(qEbShipToMatrix).where(qEbShipToMatrix.shipTo.eq(user.getShipTo())).fetchFirst();
            JPAQuery<EbEtablissement> queryEtablissement = new JPAQuery<EbEtablissement>(em);
            EbEtablissement etablissement = null;

            // Recherche sur Matrix
            if (matrix == null) // CAS D'UN SHIPTO NON EXISTANT
            {
                EbShipToMatrix shipToMatrix = new EbShipToMatrix();
                etablissement = queryEtablissement
                    .from(qEbEtablissement)
                    // get etablissement with NIC=90001 and company's code is
                    // CPB
                    .where(qEbEtablissement.nic.eq(WAITING_FOR_ALLOCATION_ETABLISSEMENT_NIC))
                    .where(qEbEtablissement.ebCompagnie().code.eq(CODE_CPB)).fetchFirst();
                shipToMatrix.setShipTo(user.getShipTo());
                shipToMatrix.setNewUser("New User");
                shipToMatrix.setxEbEtablissement(etablissement);
                shipToMatrix.setStatut("1 demande d'inscription");
                shipToMatrix.setEtablissementName(etablissement.getNom());
                shipToMatrixRepository.save(shipToMatrix);
            }
            else // CAS D'UN SHIPTO EXISTANT
            {
                etablissement = queryEtablissement
                    .from(qEbEtablissement)
                    .where(qEbEtablissement.ebEtablissementNum.eq(matrix.getxEbEtablissement().getEbEtablissementNum()))
                    .fetchFirst();
                if (matrix.getxEbUserGroupe() != null) user
                    .setGroupes("|" + matrix.getxEbUserGroupe().getEbUserGroupNum().toString() + "|");
            }

            if (StringUtils.isEmpty(user.getEmail())) {
                user.setEmail("myTowerChanel+" + user.getShipTo() + "@gmail.com");
            }

            user.setEbEtablissement(etablissement);

            if (etablissement != null) {
                user.setEbCompagnie(etablissement.getEbCompagnie());
            }

            user.setSuperAdmin(false);
            user.setAdmin(false);
            user.setDateCreation(new Date());
            user.setRole(Enumeration.Role.ROLE_CHARGEUR.getCode());
            user.setStatus(UserStatus.NON_ACTIF.getCode());
            user.setEnabled(false);

            // Inscription Initiale
            // Affectation des catégories / labels
            JPAQuery<EbEtablissement> queryEtablissementSiege = new JPAQuery<EbEtablissement>(em);
            EbEtablissement etablissementSiege = queryEtablissementSiege
                .from(qEbEtablissement).where(qEbEtablissement.code.eq(CODE_CPB)).fetchFirst();

            if (etablissementSiege != null) {
                EbCategorie categorie = updateCategoriesDao.getCategoryShipToOrCreateIfNotExists(etablissementSiege);

                EbLabel label = updateCategoriesDao
                    .getLabelForUserChanel(user.getShipTo(), etablissementSiege, categorie);

                user.setListLabels(label.getEbLabelNum().toString());

                EbCategorie userCategorie = new EbCategorie();
                userCategorie.setEbCategorieNum(categorie.getEbCategorieNum());
                userCategorie.setLibelle(categorie.getLibelle());
                userCategorie.setLabels(new HashSet<EbLabel>());
                userCategorie.getLabels().add(label);
                List<EbCategorie> listCategories = new ArrayList<EbCategorie>();
                listCategories.add(userCategorie);
                user.setListCategories(listCategories);
            }

            // initialisation des module par defaut sur chanel PB : Par defaut
            // on active Transport management, Track&trace et Order management
            // les module Freight audit, quality et Freight analitycs suivront
            // une fois qu'ils seront mature.
            // Affectation des droits d'accès
            Set<ExUserModule> listExUserModule = new HashSet<ExUserModule>();

            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.ORDER_MANAGEMENT.getCode(),
                        Enumeration.AccessRight.VISUALISATION.getCode(),
                        user.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.DELIVERY_MANAGEMENT.getCode(),
                        Enumeration.AccessRight.VISUALISATION.getCode(),
                        user.getEbUserNum()));

            user.setListAccessRights(listExUserModule);
            inscriptionService.inscrire(user);
        } catch (Exception e) {
            e.printStackTrace();
            isOK = false;
        }

        return isOK;
    }
}
