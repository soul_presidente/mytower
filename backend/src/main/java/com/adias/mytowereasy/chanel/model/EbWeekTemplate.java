package com.adias.mytowereasy.chanel.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.model.EbCompagnie;


@Entity
@Table(name = "eb_wt_week", schema = "work")
public class EbWeekTemplate implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebWeektemplateNum;

    private String label;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Date dateDebut;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Date dateFin;
    @Column(name = "type")
    private Integer type;
    private Boolean isDefault;
    private Boolean deleted;
    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "week"

    )

    private List<EbWeekTemplateDay> days;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie")
    private EbCompagnie xEbCompagnie;

    public EbWeekTemplate(Integer ebWeektemplateNum) {
        super();
        this.ebWeektemplateNum = ebWeektemplateNum;
    }

    public EbWeekTemplate(
        Integer ebWeektemplateNum,
        String label,
        Date dateDebut,
        Date dateFin,
        Integer type,
        List<EbWeekTemplateDay> days,
        EbCompagnie xEbCompagnie

    ) {
        super();
        this.ebWeektemplateNum = ebWeektemplateNum;
        this.label = label;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.type = type;
        this.days = days;
        this.xEbCompagnie = xEbCompagnie;
    }

    @QueryProjection
    public EbWeekTemplate(
        Integer ebWeektemplateNum,
        String label,
        Integer type,
        Date dateDebut,
        Date dateFin,
        Boolean isDefault

    ) {
        super();
        this.ebWeektemplateNum = ebWeektemplateNum;
        this.label = label;
        this.type = type;
        // this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.isDefault = isDefault;

        if (dateDebut != null) {

            try {
                ObjectMapper objectMapper = new ObjectMapper();
                String json = objectMapper.writeValueAsString(dateDebut);
                // this.dateDebut = objectMapper.readValue(json, new
                // TypeReference<Date>() {});
                this.dateDebut = objectMapper.convertValue(dateDebut, Date.class);
            } catch (Exception e) {
                System.err.println("ConnectedUser: failed to convert object to input fields");
            }

        }

    }

    public EbWeekTemplate() {
    }

    public Integer getEbWeektemplateNum() {
        return ebWeektemplateNum;
    }

    public void setEbWeektemplateNum(Integer ebWeektemplateNum) {
        this.ebWeektemplateNum = ebWeektemplateNum;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public List<EbWeekTemplateDay> getDays() {
        return days;
    }

    public void setDays(List<EbWeekTemplateDay> days) {
        this.days = days;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
}
