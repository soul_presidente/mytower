package com.adias.mytowereasy.chanel.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;


@NamedEntityGraph(name = "EbWeekTemplateDay.hours", attributeNodes = @NamedAttributeNode("hours"))
@Entity
@Table(name = "eb_wt_day", schema = "work")
public class EbWeekTemplateDay implements Serializable {
    public EbWeekTemplateDay(Integer ebWeektemplateDayNum, boolean open) {
        super();
        this.ebWeektemplateDayNum = ebWeektemplateDayNum;
        this.open = open;
    }

    public EbWeekTemplateDay() {
        super();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebWeektemplateDayNum;

    private Integer code;

    private boolean open;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_week_num")
    private EbWeekTemplate week;

    @JsonManagedReference

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "day")

    private List<EbWeekTemplateHour> hours;

    public EbWeekTemplateDay(List<EbWeekTemplateHour> hours) {
        super();
        this.hours = hours;
    }

    public EbWeekTemplateDay(boolean open, List<EbWeekTemplateHour> hours) {
        super();
        this.open = open;
        this.hours = hours;
    }

    public EbWeekTemplateDay(boolean open) {
        super();
        this.open = open;
    }

    public EbWeekTemplateDay(Integer ebWeektemplateDayNum) {
        this.ebWeektemplateDayNum = ebWeektemplateDayNum;
    }

    public Integer getEbWeektemplateDayNum() {
        return ebWeektemplateDayNum;
    }

    public void setEbWeektemplateDayNum(Integer ebWeektemplateDayNum) {
        this.ebWeektemplateDayNum = ebWeektemplateDayNum;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public EbWeekTemplate getWeek() {
        return week;
    }

    public void setWeek(EbWeekTemplate week) {
        this.week = week;
    }

    public List<EbWeekTemplateHour> getHours() {
        return hours;
    }

    public void setHours(List<EbWeekTemplateHour> hours) {
        this.hours = hours;
    }
}
