package com.adias.mytowereasy.chanel.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.chanel.dao.ShipToMatrixDao;
import com.adias.mytowereasy.chanel.model.EbUserChanel;
import com.adias.mytowereasy.chanel.service.InscriptionServiceChanel;


@RestController
@RequestMapping(value = "/api/chanel")
@CrossOrigin("*")
public class UserChanelController {
    @Autowired
    ShipToMatrixDao shipToDao;

    @Autowired
    InscriptionServiceChanel inscription;

    @RequestMapping(value = "register", method = RequestMethod.POST, produces = "text/plain")
    public @ResponseBody String registerUser(@RequestBody(required = false) EbUserChanel user) {

        try {
            inscription.registerUser(user);
            return "Success";
        } catch (Exception e) {
            e.printStackTrace();
            return "Failed";
        }

    }
}
