package com.adias.mytowereasy.chanel.dao.impl;

import java.util.HashSet;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.chanel.dao.UpdateCategoriesImportedUsingEDIDaoChanel;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.model.QEbCategorie;
import com.adias.mytowereasy.model.QEbEtablissement;
import com.adias.mytowereasy.model.QEbLabel;
import com.adias.mytowereasy.repository.EbCategorieRepository;
import com.adias.mytowereasy.repository.EbLabelRepository;


@Component
public class UpdateCategoriesImportedUsingEDIDaoChanelImpl implements UpdateCategoriesImportedUsingEDIDaoChanel {
    @Autowired
    EbCategorieRepository ebCategorieRepository;

    private final EbLabelRepository labelRepository;
    private final EntityManager em;
    private QEbCategorie qEbCategorie = QEbCategorie.ebCategorie;
    private QEbEtablissement qEbEtablissement = QEbEtablissement.ebEtablissement;
    private QEbLabel qEbLabel = QEbLabel.ebLabel;

    private final EbCategorieRepository categorieRepository;

    @Autowired
    public UpdateCategoriesImportedUsingEDIDaoChanelImpl(
        EntityManagerFactory entityManagerFactory,
        EbLabelRepository labelRepository,
        EbCategorieRepository categorieRepository) {
        this.labelRepository = labelRepository;
        this.categorieRepository = categorieRepository;

        this.em = entityManagerFactory.createEntityManager();
    }

    @Override
    public EbLabel getLabelForUserChanel(String shipTo, EbEtablissement etablissement, EbCategorie categorie) {
        JPAQuery<EbLabel> queryLabel = new JPAQuery<EbLabel>(em);
        queryLabel.from(qEbLabel);
        queryLabel.join(qEbLabel.categorie(), qEbCategorie);
        queryLabel.join(qEbLabel.categorie().etablissement(), qEbEtablissement);
        queryLabel.where(qEbLabel.libelle.eq(shipTo));
        queryLabel
            .where(
                qEbLabel.categorie().etablissement().ebCompagnie().ebCompagnieNum
                    .eq(etablissement.getEbCompagnie().getEbCompagnieNum()));
        EbLabel label = queryLabel.fetchFirst();

        if (label == null) {
            label = new EbLabel();
            label.setLibelle(shipTo);
            label.setCategorie(categorie);
            label.setSource("web");
            labelRepository.save(label);
        }

        return label;
    }

    @Override
    public EbCategorie getCategoryShipToForCPB() {
        return ebCategorieRepository.findOneByCode("SHIPTO");
    }

    @Override
    public EbCategorie getCategoryShipToOrCreateIfNotExists(EbEtablissement etablissement) {
        JPAQuery<EbCategorie> queryCategorie = new JPAQuery<>(em);
        EbCategorie categorie = queryCategorie
            .from(qEbCategorie).join(qEbCategorie.etablissement(), qEbEtablissement)
            .where(
                qEbEtablissement.ebCompagnie().ebCompagnieNum
                    .eq(etablissement.getEbCompagnie().getEbCompagnieNum()).and(qEbCategorie.code.eq("SHIPTO")))
            .fetchFirst();

        if (categorie == null) {
            categorie = new EbCategorie();
            categorie.setLibelle("SHIPTO");
            categorie.setCode("SHIPTO");
            categorie.setSource("web");
            categorie.setEtablissement(etablissement);
            categorie.setCompagnie(etablissement.getEbCompagnie());
            categorie.setLabels(new HashSet<>());
            categorieRepository.save(categorie);
        }

        return categorie;
    }
}
