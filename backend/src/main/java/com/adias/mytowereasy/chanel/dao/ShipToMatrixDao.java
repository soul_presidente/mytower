package com.adias.mytowereasy.chanel.dao;

import java.util.List;

import com.adias.mytowereasy.chanel.model.EbShipToMatrix;
import com.adias.mytowereasy.chanel.model.EbUserChanel;
import com.adias.mytowereasy.chanel.utils.search.ShipToMatrixCriteria;


public interface ShipToMatrixDao {
    public EbUserChanel getUserChanelByUserNum(Integer userNum);

    public List<EbShipToMatrix> getListShipToMatrix(ShipToMatrixCriteria criteria);

    public List<EbUserChanel> getListUserChanel();

    public List<EbUserChanel> getListUserChanelByShipTo(String shipto);

    public Long getCountShipToMatrix(ShipToMatrixCriteria criteria);

    EbShipToMatrix getEbShipToMatrix(String label);

    EbUserChanel geUserChanelByShipTo(String shipto);
}
