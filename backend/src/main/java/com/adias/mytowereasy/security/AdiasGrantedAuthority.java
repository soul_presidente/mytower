/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.security;

import org.springframework.security.core.GrantedAuthority;


public class AdiasGrantedAuthority implements GrantedAuthority {
    String authority;
    String libelle;

    @Override
    public String getAuthority() {
        // TODO Auto-generated method stub
        return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
