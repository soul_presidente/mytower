/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.service.ServiceConfiguration;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private EbUserRepository userRepository;

    @Autowired
    private ServiceConfiguration serviceConfiguration;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http
            .authorizeRequests()

            .antMatchers("/").permitAll()

            .antMatchers(HttpMethod.POST, "/auth").permitAll()
            .antMatchers(
                "/*.js",
                "/*.css",
                "/*.woff",
                "/*.woff2",
                "/*.ttf",
                "/*.svg",
                "/*.ico",
                "/*.png",
                "/assets/**",
                "/socket/**",
                "/accueil",
                "/app/**",
                "/api/account/reset-password/**", // for sending the reset email
                "/reset-password/**", // for reseting the password form
                "/api/account/change-password-token", // for reseting the
                                                      // password request
                "/api/list/countries",
                "/api/chanel",
                "/api/kibana",
                "/api/internal/download-file/**",
                "/api/track-trace/list-track-smart",
                "/api/account/get-login-page-url",
                "/monitoring",
                "/api/list/listFilesFormats",
                "/api/inscription",
                "/api/inscription/**",
                "/test/**",
                "/api/public/**",
                "/api/analytics/**",
                "/about/dev",
                "/user-logo/**",
                "/api/refresh-cotation-price/**",
                "/login",
                "/api/v1/**")
            .permitAll()

            .antMatchers(
                "/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**",
                "/swagger-resources/configuration/**",
                "/favicon.ico")
            .permitAll()

            // pour freight analytics
            .antMatchers("/freight-audit/**")
            .access(UserPrivileges.prepareModuleRoles(UserPrivileges.getModduleFA().getCode()))

            .anyRequest().authenticated().and()
            // We filter the api/login requests
            .addFilterBefore(
                new JWTLoginFilter("/auth", authenticationManager(), userRepository, serviceConfiguration),
                UsernamePasswordAuthenticationFilter.class)
            // And filter other requests to check the presence of JWT in header
            .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
            // We filter the /mob/login requests for mobile app
            .addFilterBefore(
                new JWTLoginFilter("/mobile/login", authenticationManager(), userRepository, serviceConfiguration),
                UsernamePasswordAuthenticationFilter.class)
            // And filter other requests to check the presence of JWT in header
            .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

        // http.antMatcher("/actuator/**").httpBasic();
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);

        auth.inMemoryAuthentication().withUser("admin").password(passwordEncoder.encode("admin")).roles("USER");
    }
}
