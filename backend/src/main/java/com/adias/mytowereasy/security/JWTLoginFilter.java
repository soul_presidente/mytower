/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.security;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.google.gson.Gson;

import com.adias.mytowereasy.dto.EbLoginDto;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbUser;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.service.ServiceConfiguration;


public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {
    private static final Logger LOGGER = LoggerFactory.getLogger(JWTLoginFilter.class);

    private EbUserRepository userRepository;

    private ServiceConfiguration serviceConfiguration;

    public JWTLoginFilter(
        String url,
        AuthenticationManager authManager,
        EbUserRepository applicationUserRepository,
        ServiceConfiguration serviceConfiguration) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
        this.userRepository = applicationUserRepository;
        this.serviceConfiguration = serviceConfiguration;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
        throws AuthenticationException,
        IOException,
        ServletException {
        String username = null, password = null, tokenCt = null;

        tokenCt = req.getParameter("tokenct");

        if (req.getParameter("username") != null) {
            username = req.getParameter("username").toLowerCase();
            password = req.getParameter("password");
        }
        else {
            EbLoginDto login = null;

            try {
                BufferedReader reader = req.getReader();
                Gson gson = new Gson();
                login = gson.fromJson(reader, EbLoginDto.class);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

            username = login.getUsername().toLowerCase();
            password = login.getPassword();
        }

        LOGGER.debug("attemptAuthentication: username = " + username);

        if (tokenCt != null) {
            EbUser user = null;
            EbUser connectedUserCt = TokenAuthenticationService.getUserByToken(tokenCt);

            // chargement du CT deja connecté et veut se connecter "en tant que"
            // un autre user
            if (connectedUserCt != null && connectedUserCt.isControlTower()) {
                user = userRepository.findOne(QEbUser.ebUser.username.eq(connectedUserCt.getEmail())).orElse(null);

                // si CT deja connecté
                if (user != null && user.isControlTower() && user.isSuperAdmin()) {
                    return new UsernamePasswordAuthenticationToken(
                        username,
                        user.getPassword(),
                        Collections.emptyList());
                }

            }

        }

        UsernamePasswordAuthenticationToken user = new UsernamePasswordAuthenticationToken(
            username,
            password,
            Collections.emptyList());

        AuthenticationManager authManager = getAuthenticationManager();
        Authentication auth3 = authManager.authenticate(user);

        return auth3;
    }

    @Override
    protected void successfulAuthentication(
        HttpServletRequest req,
        HttpServletResponse res,
        FilterChain chain,
        Authentication auth)
        throws IOException,
        ServletException {
        EbUser user = userRepository.selectByUsernameOrEmail(auth.getName().toLowerCase());

        String tokenCt = req.getParameter("tokenct");
        EbUser connectedUserCt = null;

        if (tokenCt != null) connectedUserCt = TokenAuthenticationService.getUserByToken(tokenCt);

        if (connectedUserCt != null && connectedUserCt.isControlTower()
            && !connectedUserCt.getEbUserNum().equals(user.getEbUserNum())) {
            EbUser ctUser = userRepository.findOneByEmailIgnoreCase(connectedUserCt.getEmail());

            if (ctUser != null && ctUser.isControlTower()) {
                user.setRealUserNum(ctUser.getEbUserNum());
                user.setRealUserNom(ctUser.getNom());
                user.setRealUserPrenom(ctUser.getPrenom());
                user.setRealUserEmail(ctUser.getEmail());
                user.setRealUserPassword(ctUser.getPassword());
                user.setRealUserEtabNum(ctUser.getEbEtablissement().getEbEtablissementNum());
                user.setRealUserEtabNom(ctUser.getEbEtablissement().getNom());
                user.setRealUserCompNum(ctUser.getEbCompagnie().getEbCompagnieNum());
                user.setRealUserCompNom(ctUser.getEbCompagnie().getNom());
            }

        }

        if (this.serviceConfiguration != null) TokenAuthenticationService
            .addAuthentication(res, auth, user, this.serviceConfiguration.getDynamicExpirationTime());
        else TokenAuthenticationService
            .addAuthentication(res, auth, user, this.serviceConfiguration.getDynamicExpirationTime());
    }
}
