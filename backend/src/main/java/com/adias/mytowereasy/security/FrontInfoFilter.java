package com.adias.mytowereasy.security;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.adias.mytowereasy.properties.FrontProperties;


@Configuration
public class FrontInfoFilter implements Filter {
    private static final Logger LOGGER = LoggerFactory.getLogger(FrontInfoFilter.class);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws IOException,
        ServletException {

        // Check if it's Http
        if (!(response instanceof HttpServletResponse)) {
            chain.doFilter(request, response);
            return;
        }

        HttpServletResponse httpResponse = (HttpServletResponse) response;
        HttpServletRequest httpRequest = (HttpServletRequest) request;

        // Check for url (exclude all /api)
        if (httpRequest.getRequestURI().startsWith("/api/")) {
            chain.doFilter(request, response);
            return;
        }

        ServletContext servletContext = request.getServletContext();
        WebApplicationContext webApplicationContext = WebApplicationContextUtils
            .getWebApplicationContext(servletContext);
        FrontProperties frontProperties = webApplicationContext.getBean(FrontProperties.class);

        // Check for retrieving properties
        if (frontProperties == null) {
            LOGGER.error("Can't retrieve front properties");
            chain.doFilter(request, response);
            return;
        }

        Cookie cookie = null;

        if (frontProperties.getApiUrl() != null) {
            cookie = new Cookie("frt_env_apiurl", frontProperties.getApiUrl());
            cookie.setPath("/");
            httpResponse.addCookie(cookie);
        }

        if (frontProperties.getWhitelist() != null) {
            cookie = new Cookie("frt_env_whitelist", frontProperties.getWhitelist());
            cookie.setPath("/");
            httpResponse.addCookie(cookie);
        }

        if (frontProperties.getModeProd() != null) {
            cookie = new Cookie("frt_env_modeprod", String.valueOf(frontProperties.getModeProd()));
            cookie.setPath("/");
            httpResponse.addCookie(cookie);
        }

        chain.doFilter(request, response);
    }
}
