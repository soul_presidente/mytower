/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.UserStatus;
import com.adias.mytowereasy.model.ExUserModule;
import com.adias.mytowereasy.repository.EbUserRepository;


@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private EbUserRepository userRepository;

    public UserDetailsServiceImpl(EbUserRepository applicationUserRepository) {
        this.userRepository = applicationUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String userNameOrEmail) throws UsernameNotFoundException {
        EbUser user = userRepository.selectByUsernameOrEmail(userNameOrEmail);

        if (user == null || (user.getStatus() != UserStatus.ACTIF.getCode())) {
            throw new UsernameNotFoundException(userNameOrEmail);
        }

        List<GrantedAuthority> list = new ArrayList();

        for (ExUserModule exUserModule: user.getListAccessRights()) {
            String role = "ROLE_" + user.getRole() + "_" + exUserModule.getAccessRight() + "_"
                + exUserModule.getEcModuleNum();
            list.add(new SimpleGrantedAuthority(role));
        }

        return new User(user.getUsername(), user.getPassword(), list);
    }
}
