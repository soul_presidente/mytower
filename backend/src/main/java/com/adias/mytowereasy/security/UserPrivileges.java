/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.security;

import com.adias.mytowereasy.model.Enumeration.Action;
import com.adias.mytowereasy.model.Enumeration.Module;
import com.adias.mytowereasy.model.Enumeration.Role;


public class UserPrivileges {
    Role Chargeur = Role.ROLE_CHARGEUR;
    Role Prestataire = Role.ROLE_PRESTATAIRE;

    Action actionExecuteur = Action.EXECUTER;
    Action actionVisualisateur = Action.VISUALISER;

    static Module modduleFA = Module.FREIGHT_AUDIT;

    public String prepareRole(Integer role, Integer action, Integer module) {
        return "hasRole('" + role + "_" + action + "_" + module + "')";
    }

    public String prepareRoleWithotHas(Integer role, Integer action, Integer module) {
        return "ROLE_" + role + "_" + action + "_" + module;
    }

    public static String prepareModuleRoles(Integer module) {
        String result = "";

        for (int i = 0; i < Role.getListRole().size(); i++) {

            for (int j = 0; j < Action.getListAction().size(); j++) {
                result += "hasRole('ROLE_" + Role.getListRole().get(i).getCode() + "_"
                    + Action.getListAction().get(j).getCode() + "_" + module + "')";

                if (j < Action.getListAction().size() - 1) {
                    result += " or ";
                }

            }

            if (i < Role.getListRole().size() - 1) {
                result += " or ";
            }

        }

        // System.out.println(result);
        return result;
    }

    public static Module getModduleFA() {
        return modduleFA;
    }

    public void setModduleFA(Module modduleFA) {
        this.modduleFA = modduleFA;
    }

    public Role getRoleChargeur() {
        return Chargeur;
    }

    public void setRoleChargeur(Role roleChargeur) {
        this.Chargeur = roleChargeur;
    }

    public Role getRolePrestataire() {
        return Prestataire;
    }

    public void setRolePrestataire(Role rolePrestataire) {
        this.Prestataire = rolePrestataire;
    }
}
