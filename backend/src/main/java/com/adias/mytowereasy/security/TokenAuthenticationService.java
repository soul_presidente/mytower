/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.security;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.KibanaUser;
import com.adias.mytowereasy.types.JacksonUtil;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


public class TokenAuthenticationService {
    private static final Logger logger = LoggerFactory.getLogger(TokenAuthenticationService.class);

    public static final long EXPIRATIONTIME = 864_000_000; // 10 days
    static final String SECRET = "ThisIsASecret";
    static final String TOKEN_PREFIX = "Bearer";
    static final String HEADER_STRING = "Authorization";
    static final String ACCESS_TOKEN = "access_token";

    public static KibanaUser getKibanaUserFromToken(String access_token) {
        Jws jws = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(access_token.replace(TOKEN_PREFIX, ""));
        Claims tockenBody = (Claims) jws.getBody();

        // get User from token
        return JacksonUtil.OBJECT_MAPPER.convertValue(tockenBody.get("connectedUserKey"), KibanaUser.class);
    }

    public static String generateKibanaToken(String access_token, long expirationTime) {
        Jws jws = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(access_token.replace(TOKEN_PREFIX, ""));
        Claims tockenBody = (Claims) jws.getBody();

        // get User from token
        EbUser ebUser = JacksonUtil.OBJECT_MAPPER.convertValue(tockenBody.get("connectedUserKey"), EbUser.class);

        return Jwts
            .builder().claim("connectedUserKey", ebUser.toKibanaUser())
            .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
            .signWith(SignatureAlgorithm.HS256, SECRET).compact();
    }

    static void addAuthentication(HttpServletResponse res, Authentication auth, EbUser ebUser, Long expirationTime) {
        String username = auth.getName();

        List authorities = (List) auth.getAuthorities();
        String JWT = Jwts
            .builder().claim("authorities", authorities).claim("connectedUserKey", ebUser.toConnectedUser())
            .setSubject(username).setExpiration(new Date(System.currentTimeMillis() + expirationTime)) // DEFAULT
                                                                                                       // IS
                                                                                                       // TokenAuthenticationServiceRE.EXPIRATIONTIME
            .signWith(SignatureAlgorithm.HS512, SECRET).compact();

        res.setContentType("application/json");
        // Get the printwriter object from response to write the required json
        // object to the output
        // stream
        PrintWriter out;

        try {
            logger.info("Authentification reussi");

            out = res.getWriter();
            out.print(JWT);
            out.flush();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        // Assuming your json object is **jsonObject**, perform the following,
        // it will return your json
        // object

    }

    static Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);

        if (token != null) {

            try {
                // parse the token.
                Jws jws = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token.replace(TOKEN_PREFIX, ""));
                Claims tockenBody = (Claims) jws.getBody();
                String user = tockenBody.getSubject();

                ArrayList<SimpleGrantedAuthority> autorities = (ArrayList<SimpleGrantedAuthority>) tockenBody
                    .get("authorities");
                Collection<GrantedAuthority> autorities2 = new ArrayList<GrantedAuthority>();

                Object[] authorityArray = autorities.toArray();

                // Toute cette bidouille sert uniquement à convertir le
                for (int i = 0; i < autorities.size(); i++) {
                    java.util.LinkedHashMap map = (java.util.LinkedHashMap) authorityArray[i];
                    autorities2.add(new SimpleGrantedAuthority((String) map.get("authority")));
                }
                // System.out.print(autorities);
                // System.out.println(jws.getBody());

                EbUser ebUser = JacksonUtil.OBJECT_MAPPER
                    .convertValue(tockenBody.get("connectedUserKey"), EbUser.class);

                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                    ebUser,
                    null,
                    autorities2);

                return user != null ? usernamePasswordAuthenticationToken : null;
            } catch (Exception e) {
                e.printStackTrace(System.err);
                return null;
            }

        }

        return null;
    }

    public static EbUser getUserByToken(String token) {

        if (token != null) {

            try {
                // parse the token.
                Jws jws = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token.replace(TOKEN_PREFIX, ""));
                Claims tockenBody = (Claims) jws.getBody();
                String user = tockenBody.getSubject();

                ArrayList<SimpleGrantedAuthority> autorities = (ArrayList<SimpleGrantedAuthority>) tockenBody
                    .get("authorities");
                Collection<GrantedAuthority> autorities2 = new ArrayList<GrantedAuthority>();

                Object[] authorityArray = autorities.toArray();

                // Toute cette bidouille sert uniquement à convertir le
                for (int i = 0; i < autorities.size(); i++) {
                    java.util.LinkedHashMap map = (java.util.LinkedHashMap) authorityArray[i];
                    autorities2.add(new SimpleGrantedAuthority((String) map.get("authority")));
                }

                EbUser ebUser = JacksonUtil.OBJECT_MAPPER
                    .convertValue(tockenBody.get("connectedUserKey"), EbUser.class);

                return ebUser;
            } catch (Exception e) {
                return null;
            }

        }

        return null;
    }

    public static String getTokenByUser(EbUser ebUser) {
        String username = ebUser.getUsername();

        String JWT = Jwts
            .builder().claim("authorities", new ArrayList<>()).claim("connectedUserKey", ebUser.toConnectedUser())
            .setSubject(username).setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME)) // DEFAULT
                                                                                                       // IS
                                                                                                       // TokenAuthenticationServiceRE.EXPIRATIONTIME
            .signWith(SignatureAlgorithm.HS512, SECRET).compact();

        return JWT;
    }
}
