package com.adias.mytowereasy.properties;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import com.adias.mytowereasy.exception.MyTowerException;


@Configuration
@ConfigurationProperties("mytower.front")
public class FrontProperties {
    private static final String FILE_READ_URL_PREFIX = "/file/?filePath=";

    private String apiUrl;
    private String whitelist;
    private String domain;
    private Boolean modeProd;

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getWhitelist() {
        return whitelist;
    }

    public void setWhitelist(String whitelist) {
        this.whitelist = whitelist;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Boolean getModeProd() {
        return modeProd;
    }

    public void setModeProd(Boolean modeProd) {
        this.modeProd = modeProd;
    }

    public String getRealFileUrl(String relativePath) {

        try {
            return apiUrl + FILE_READ_URL_PREFIX + URLEncoder.encode(relativePath, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new MyTowerException("Unable to get real file url");
        }

    }
}
