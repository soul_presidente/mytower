package com.adias.mytowereasy.migration;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.repository.EbCategorieRepository;
import com.adias.mytowereasy.repository.EbLabelRepository;
import com.adias.mytowereasy.service.ConnectedUserService;


@Service
public class MigrationService {
    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    EbCategorieRepository ebCategorieRepository;

    @Autowired
    EbLabelRepository ebLabelRepository;

    @Value("${spring.datasource.username}")
    String dbUserName;

    @Value("${spring.datasource.password}")
    String dbPassWord;

    @Value("${spring.datasource.url}")
    String dburl;

    @Value("${spring.datasource.driver-class-name}")
    String dbDriverName;

    public void migrateOrderAndDeliveryCateg(String[] categoriesLabel) throws Throwable {
        if (ArrayUtils.isEmpty(categoriesLabel)) throw new IllegalArgumentException("categories label vides");

        EbUser user = connectedUserService.getCurrentUser();
        final Integer compagnieNum = user.getEbCompagnie().getEbCompagnieNum();
        if (compagnieNum == null) throw new MyTowerException("Compagnie num is null");

        ObjectMapper mapper = new ObjectMapper();
        Class.forName(dbDriverName);
        String queryOrder = "select eb_del_order_num, custom_fields, list_categories from work.eb_del_order";
        String selectCustomFieldsQuery = "select fields from work.eb_custom_field where x_eb_compagnie = " +
            compagnieNum;
        String updateQueryOrder = "update work.eb_del_order set custom_fields =?, list_categories =to_json(?::json), list_custom_fields_flat =?, list_custom_fields_value_flat =? where eb_del_order_num =?";

        String queryDelivery = "select eb_del_livraison_num, custom_fields, list_categories from work.eb_del_livraison";
        String updateQueryDelivery = "update work.eb_del_livraison set custom_fields =?, list_categories =to_json(?::json), list_custom_fields_flat =?, list_custom_fields_value_flat =? where eb_del_livraison_num =?";

        String queryUserIdsAndCategories = "select eb_user_num, list_categories from work.eb_user where x_eb_compagnie = " +
            compagnieNum;

        String updateUserCategoriesQuery = "update work.eb_user set list_categories =to_json(?::json) where eb_user_num = ?";

        try (Connection con = DriverManager.getConnection(dburl, dbUserName, dbPassWord)) {
            List<CustomFields> selectFields = null;

            try (PreparedStatement s = con.prepareStatement(selectCustomFieldsQuery)) {
                ResultSet result = s.executeQuery();

                while (result.next()) {
                    String custom_fields = result.getString("fields");
                    List<CustomFields> customFieldsDeserialized = mapper
                        .readValue(custom_fields, new TypeReference<ArrayList<CustomFields>>() {
                        });

                    selectFields = customFieldsDeserialized
                        .stream().filter(field -> containsLabel(field.getLabel(), categoriesLabel))
                        .collect(Collectors.toList());
                }

            }

            if (CollectionUtils.isEmpty(selectFields)) throw new MyTowerException("Custom fields not found");

            if (CollectionUtils.isNotEmpty(selectFields)) {
                // Order
                migrateEntity(
                    categoriesLabel,
                    mapper,
                    queryOrder,
                    updateQueryOrder,
                    con,
                    selectFields,
                    "eb_del_order_num");

                // delivery
                migrateEntity(
                    categoriesLabel,
                    mapper,
                    queryDelivery,
                    updateQueryDelivery,
                    con,
                    selectFields,
                    "eb_del_livraison_num");

                // delete categories in the parameters
                deleteCategoriesAndTheirsLabelsInSettings(categoriesLabel, con, compagnieNum);

                removeCategoriesFromUserSettings(
                    categoriesLabel,
                    queryUserIdsAndCategories,
                    updateUserCategoriesQuery,
                    con,
                    mapper,
                    compagnieNum);
            }

        } catch (Throwable e) {
            e.printStackTrace();
        }

    }

    private void removeCategoriesFromUserSettings(
        String[] categoriesLabel,
        String queryUserIdsAndCategories,
        String updateUserCategoriesQuery,
        Connection con,
        ObjectMapper mapper,
        Integer compagnieNum)
        throws SQLException,
        JsonProcessingException {

        try (PreparedStatement stmt0 = con.prepareStatement(queryUserIdsAndCategories)) {
            ResultSet resultSet = stmt0.executeQuery();

            while (resultSet.next()) {
                List<EbCategorie> categoriesDeserialized = null;
                long userId = resultSet.getLong("eb_user_num");
                String list_categories = resultSet.getString("list_categories");

                try {

                    if (StringUtils.isNotEmpty(list_categories)) {
                        categoriesDeserialized = mapper
                            .readValue(list_categories, new TypeReference<ArrayList<EbCategorie>>() {
                            });
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (CollectionUtils.isNotEmpty(categoriesDeserialized)) {
                    // get needed categories
                    List<EbCategorie> selectedCategories = categoriesDeserialized
                        .stream().filter(categ -> !containsLabel(categ.getLibelle(), categoriesLabel))
                        .collect(Collectors.toList());

                    String categories = mapper.writeValueAsString(selectedCategories);
                    System.out.println("updating user categories");

                    try (PreparedStatement s = con.prepareStatement(updateUserCategoriesQuery)) {
                        s.setString(1, categories);
                        s.setLong(2, userId);
                        s.executeUpdate();
                    }

                }

            }

        }

    }

    private void
        deleteCategoriesAndTheirsLabelsInSettings(String[] categoriesLabel, Connection con, Integer compagnieNum) {

        try {
            List<Integer> categorieIds = ebCategorieRepository.getIdsByLibelle(compagnieNum, categoriesLabel);

            // delete categories's labels
            if (CollectionUtils.isNotEmpty(categorieIds)) {
                ebLabelRepository.deleteLabelByCategoriesIds(categorieIds);
            }

            // delete categories
            ebCategorieRepository.deleteAllLibelleLike(compagnieNum, categoriesLabel);
        } catch (Exception e) {
            System.out.println("probleme de suppression des categories");
            e.printStackTrace();
        }

    }

    private void migrateEntity(
        String[] categoriesLabel,
        ObjectMapper mapper,
        String query,
        String updateQuery,
        Connection con,
        List<CustomFields> selectFields,
        String entityNum)
        throws SQLException,
        JsonProcessingException {

        try (PreparedStatement stmt0 = con.prepareStatement(query)) {
            ResultSet resultSet = stmt0.executeQuery();

            while (resultSet.next()) {
                long entityid = resultSet.getLong(entityNum);
                String list_categories = resultSet.getString("list_categories");
                String custom_fields = resultSet.getString("custom_fields");
                List<EbCategorie> categoriesDeserialized = null;
                List<CustomFields> customFieldsDeserialized = new ArrayList<>();

                try {
                    categoriesDeserialized = mapper
                        .readValue(list_categories, new TypeReference<ArrayList<EbCategorie>>() {
                        });
                    customFieldsDeserialized = mapper
                        .readValue(custom_fields, new TypeReference<ArrayList<CustomFields>>() {
                        });
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (CollectionUtils.isNotEmpty(categoriesDeserialized)) {
                    // get needed categories
                    List<EbCategorie> selectedCateg = categoriesDeserialized
                        .stream().filter(categ -> containsLabel(categ.getLibelle(), categoriesLabel))
                        .collect(Collectors.toList());

                    if (CollectionUtils.isNotEmpty(selectedCateg)) {

                        for (EbCategorie categ: selectedCateg) {
                            CustomFields field = new CustomFields();
                            field.setLabel(categ.getLibelle());
                            field.setName(categ.getLibelle());
                            field.setIsNew(Boolean.FALSE);

                            if (CollectionUtils.isNotEmpty(categ.getLabels())) {
                                String value = "";

                                for (EbLabel label: categ.getLabels()) {
                                    if (value.isEmpty()) value += label.getLibelle();
                                    else value += "," + label.getLibelle();
                                }

                                field.setValue(value);
                            }

                            if (CollectionUtils.isNotEmpty(selectFields)) {
                                CustomFields cf = selectFields
                                    .stream().filter(f -> f.getLabel().equalsIgnoreCase(categ.getLibelle())).findFirst()
                                    .orElse(null);

                                if (cf != null) {
                                    field.setNum(cf.getNum());
                                }

                            }

                            customFieldsDeserialized.add(field);
                        }

                        categoriesDeserialized.removeAll(selectedCateg);

                        String listLabels = "";

                        for (EbCategorie categ: categoriesDeserialized) {

                            if (CollectionUtils.isNotEmpty(categ.getLabels())) {

                                for (EbLabel label: categ.getLabels()) {
                                    if (listLabels.isEmpty()) listLabels += label.getEbLabelNum();
                                    else listLabels += "," + label.getEbLabelNum();
                                }

                            }

                        }

                        String listCustomFieldsFlat = "";
                        String listCustomFieldsValueFlat = "";

                        for (CustomFields field: customFieldsDeserialized) {
                            if (field.getValue() == null ||
                                field.getValue().isEmpty() || field.getName() == null ||
                                field.getName().isEmpty()) continue;

                            if (!listCustomFieldsFlat.isEmpty()) listCustomFieldsFlat += ";";

                            if (!listCustomFieldsValueFlat.isEmpty()) listCustomFieldsValueFlat += ";";

                            listCustomFieldsFlat += "\"" + field.getName() + "\":" + "\"" + field.getValue() + "\"";

                            listCustomFieldsValueFlat += "\"" +
                                field.getLabel() + "\":" + "\"" + field.getValue() + "\"";
                        }

                        if (listCustomFieldsFlat.isEmpty()) listCustomFieldsFlat = null;

                        if (listCustomFieldsValueFlat.isEmpty()) listCustomFieldsValueFlat = null;

                        String categories = mapper.writeValueAsString(categoriesDeserialized);
                        String customFields = mapper.writeValueAsString(customFieldsDeserialized);
                        System.out.println("list categories : " + categories);
                        System.out.println(entityNum + " " + entityid);

                        try (PreparedStatement s = con.prepareStatement(updateQuery)) {
                            s.setString(1, customFields);
                            s.setString(2, categories);
                            s.setString(3, listCustomFieldsFlat);
                            s.setString(4, listCustomFieldsValueFlat);
                            s.setLong(5, entityid);
                            s.executeUpdate();
                        }

                    }

                }

            }

        }

    }

    private boolean containsLabel(String label, String[] categoriesLabel) {
        boolean bool = false;

        for (String categLabel: categoriesLabel) {
            bool = bool || categLabel.equalsIgnoreCase(label);
        }

        return bool;
    }
}
