package com.adias.mytowereasy.migration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.api.controller.ApiController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/api/v1/migrate")
public class MigrationController extends ApiController {
    @Autowired
    MigrationService migrationService;

    @ApiOperation(value = "migrate categories to new customfields")
    @PutMapping(value = "/categories-to-customfields")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Updated"),
    })
    public @ResponseBody String migrateOrderAndDeliveryCateg(@RequestBody String[] categoriesLabel) {

        try {
            migrationService.migrateOrderAndDeliveryCateg(categoriesLabel);
            return "Successful operation";
        } catch (Throwable e) {
            e.printStackTrace();
        }

        return "An exception occurred";
    }
}
