/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.types;

import com.fasterxml.jackson.annotation.ObjectIdGenerator.IdKey;
import com.fasterxml.jackson.annotation.ObjectIdResolver;
import com.fasterxml.jackson.annotation.SimpleObjectIdResolver;


public class CustomObjectIdResolver implements ObjectIdResolver {
    private ObjectIdResolver objectIdResolver;

    public CustomObjectIdResolver() {
        clearReferences();
    }

    @Override
    public void bindItem(IdKey id, Object pojo) {
        // if (pojo instanceof EbUser)
        clearReferences();

        objectIdResolver.bindItem(id, pojo);
    }

    @Override
    public Object resolveId(IdKey id) {
        return objectIdResolver.resolveId(id);
    }

    @Override
    public boolean canUseFor(ObjectIdResolver resolverType) {
        return resolverType.getClass() == getClass();
    }

    @Override
    public ObjectIdResolver newForDeserialization(Object context) {
        return new CustomObjectIdResolver();
    }

    private void clearReferences() {
        objectIdResolver = new SimpleObjectIdResolver();
    }
}
