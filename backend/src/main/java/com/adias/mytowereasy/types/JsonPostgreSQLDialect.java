/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.types;

import java.sql.Types;

import org.hibernate.dialect.PostgreSQL9Dialect;


public class JsonPostgreSQLDialect extends PostgreSQL9Dialect {
    public JsonPostgreSQLDialect() {
        super();

        this.registerColumnType(Types.JAVA_OBJECT, "json");
    }
}
