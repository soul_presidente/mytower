package com.adias.mytowereasy.enumeration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.adias.mytowereasy.util.GenericEnum;


public enum RootCauseStatusEnum implements GenericEnum {
    TO_DO(0, "TO_DO"), IN_PROGRESS(1, "IN_PROGRESS"), WAITING_FOR(2, "WAITING_FOR"), COMPLETED(3, "COMPLETED");

    private Integer code;
    private String key;

    private RootCauseStatusEnum(Integer code, String key) {
        this.code = code;
        this.key = key;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getKey() {
        return key;
    }

    public void setKey(Integer code) {
        this.code = code;
    }

    public void setValue(String key) {
        this.key = key;
    }

    public static List<RootCauseStatusEnum> getRootCauseStatusEnum() {
        List<RootCauseStatusEnum> result = new ArrayList<>(Arrays.asList(RootCauseStatusEnum.values()));

        return result;
    }
}
