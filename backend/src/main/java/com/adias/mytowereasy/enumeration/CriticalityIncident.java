package com.adias.mytowereasy.enumeration;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.adias.mytowereasy.util.GenericEnum;


public enum CriticalityIncident implements GenericEnum {
    HIGHEST(4, "HIGHEST"), HIGH(3, "HIGH"), MEDIUM(2, "MEDIUM"), LOW(1, "LOW"), LOWEST(0, "LOWEST");

    private Integer code;
    private String key;

    private CriticalityIncident(Integer code, String key) {
        this.code = code;
        this.key = key;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getKey() {
        return key;
    }

    public void setKey(Integer code) {
        this.code = code;
    }

    public void setValue(String key) {
        this.key = key;
    }

    public static List<CriticalityIncident> getCriticalityIncidentByKey(String term) {
        CriticalityIncident[] types = CriticalityIncident.values();
        List<CriticalityIncident> criticality = new ArrayList<CriticalityIncident>();

        for (CriticalityIncident t: types) {

            if (t.getKey().toLowerCase().trim().contains(term)) {
                criticality.add(t);
            }

        }

        return criticality;
    }

    public static List<CriticalityIncident> getCriticalityIncident() {
        List<CriticalityIncident> result = new ArrayList<CriticalityIncident>();
        CriticalityIncident[] types = CriticalityIncident.values();
        result = Stream.of(types).collect(Collectors.toList());
        return result;
    }
}
