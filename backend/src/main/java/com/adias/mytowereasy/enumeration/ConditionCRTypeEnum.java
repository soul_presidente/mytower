package com.adias.mytowereasy.enumeration;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.adias.mytowereasy.util.GenericEnum;


@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ConditionCRTypeEnum implements GenericEnum {
    DATE_CONDITIONS(1, "CONTROL_RULES.DATE_CONDITIONS"), DOC_CONDITIONS(2, "CONTROL_RULES.DOC_CONDITIONS");

    private Integer code;
    private String key;

    private ConditionCRTypeEnum(Integer code, String key) {
        this.code = code;
        this.key = key;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
