package com.adias.mytowereasy.enumeration;

import java.util.Arrays;
import java.util.List;


public enum FileExtentionEnum {
    PDF(0, ".pdf"), WORD(1, ".docx");

    private Integer code;
    private String extention;

    private FileExtentionEnum(Integer code, String extention) {
        this.code = code;
        this.extention = extention;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getExtention() {
        return extention;
    }

    public void setExtention(String extention) {
        this.extention = extention;
    }

    public static String getFileExtentionByCode(Integer code) {
        FileExtentionEnum[] values = FileExtentionEnum.values();
        List<FileExtentionEnum> listFileExtentions = Arrays.asList(values);

        FileExtentionEnum fileExtentionEnum = listFileExtentions
            .stream().filter(ext -> ext.code.equals(code)).findFirst().orElse(null);
        return fileExtentionEnum.extention;
    }
}
