package com.adias.mytowereasy.enumeration;

import static java.util.Arrays.asList;

import java.util.List;
import java.util.Optional;

import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.util.GenericEnum;


public enum SupportedMimeTypes implements GenericEnum {
    DOC(0, "application/msword", "doc", asList(Applicability.ANY_FOLDER)),
    XLS(1, "application/vnd.ms-excel", "xls", asList(Applicability.ANY_FOLDER)),
    PPT(2, "application/vnd.ms-powerpoint", "ppt", asList(Applicability.ANY_FOLDER)),
    DOCX(
        3,
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "docx",
        asList(Applicability.ANY_FOLDER, Applicability.TEMPLATE)),
    XLSX(
        4,
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
        "xlsx",
        asList(Applicability.ANY_FOLDER)),
    PPTX(
        5,
        "application/vnd.openxmlformats-officedocument.presentationml.presentation",
        "pptx",
        asList(Applicability.ANY_FOLDER)),
    PDF(6, "application/pdf", "pdf", asList(Applicability.ANY_FOLDER)),
    HTML(7, "text/html", asList("html", "htm"), asList(Applicability.ANY_FOLDER)),
    TXT(8, "text/plain", "txt", asList(Applicability.ANY_FOLDER)),
    JPEG(9, "image/jpeg", asList("jpg", "jpeg"), asList(Applicability.ANY_FOLDER)),
    PNG(10, "image/png", "png", asList(Applicability.ANY_FOLDER)),
    GIF(11, "image/gif", "gif", asList(Applicability.ANY_FOLDER)),
    BMP(12, "image/bmp", "bmp", asList(Applicability.ANY_FOLDER)),
    TIFF(13, "image/tiff", asList("tif", "tiff"), asList(Applicability.ANY_FOLDER)),
    AVI(14, "video/x-msvideo", "avi", asList(Applicability.ANY_FOLDER)),
    CSV(15, "text/csv", "csv", asList(Applicability.ANY_FOLDER)),
    MPEG(16, "video/mpeg", "mpeg", asList(Applicability.ANY_FOLDER)),
    WEBM(17, "video/webm", "webm", asList(Applicability.ANY_FOLDER)),
    RTF(18, "application/rtf", "rtf", asList(Applicability.ANY_FOLDER)),
    XPS(19, "application/vnd.ms-xpsdocument", "xps", asList(Applicability.ANY_FOLDER)),
    XLSM(20, "application/vnd.ms-excel.sheet.macroEnabled.12", "xlsm", asList(Applicability.ANY_FOLDER)),
    MSG(21, "application/vnd.ms-outlook", "msg", asList(Applicability.ANY_FOLDER)),
    DOCM(22, "application/vnd.ms-word.document.macroEnabled.12", "docm", asList(Applicability.ANY_FOLDER));

    private Integer code;
    private String key;
    private List<String> extensions;
    private List<Applicability> supportedIn;

    private SupportedMimeTypes(Integer code, String key, String extension, List<Applicability> supportedIn) {
        this(code, key, asList(extension), supportedIn);
    }

    private SupportedMimeTypes(Integer code, String key, List<String> extensions, List<Applicability> supportedIn) {
        this.code = code;
        this.key = key;
        this.extensions = extensions;
        this.supportedIn = supportedIn;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getKey() {
        return key;
    }

    public String getMimeType() {
        return key;
    }

    public List<String> getExtensions() {
        return extensions;
    }

    public List<Applicability> getSupportedIn() {
        return supportedIn;
    }

    public boolean isSupported(Applicability applicability) {
        return this.supportedIn.contains(applicability);
    }

    /**
     * @param extension
     *            Extension with or without dot (eg. '.docx', 'docx')
     * 
     * @return Corresponding mimytype enum instance
     */
    public static Optional<SupportedMimeTypes> getByExtension(String extension) {
        String extensionWithoutDot = extension.replace(".", "");

        for (SupportedMimeTypes s: SupportedMimeTypes.values()) {

            if (s.extensions.stream().anyMatch(e -> e.equalsIgnoreCase(extensionWithoutDot))) {
                return Optional.of(s);
            }

        }

        return Optional.empty();
    }

    public static SupportedMimeTypes getByFileNameOrThrow(String fileName) {
        String extension = fileName.substring(fileName.lastIndexOf("."));
        return SupportedMimeTypes
            .getByExtension(extension)
            .orElseThrow(() -> new MyTowerException(String.format("Extension %s not supported", extension)));
    }

    public static enum Applicability {
        ANY_FOLDER, TEMPLATE
    }
}
