package com.adias.mytowereasy.enumeration;

import com.adias.mytowereasy.util.GenericEnum;


public enum TypeNotificationEnum implements GenericEnum {
    ALERT("alert", 1), NOTIFICATION("notification", 2), MESSAGE("Message", 3);

    private String key;
    private Integer code;

    private TypeNotificationEnum(String key, Integer code) {
        this.key = key;
        this.code = code;
    }

    @Override
    public Integer getCode() {
        return code;
    }

    @Override
    public String getKey() {
        return key;
    }
}
