package com.adias.mytowereasy.enumeration;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.adias.mytowereasy.util.GenericEnum;


@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ExportControlEnum implements GenericEnum {
    ACE(1, "PRICING_BOOKING.EXPORT_CONTROL.ACE"),
    LICENCE_OK(2, "PRICING_BOOKING.EXPORT_CONTROL.LICENCE_OK"),
    LICENCE_NOK(3, "PRICING_BOOKING.EXPORT_CONTROL.LICENCE_NOK"),
    ALD(4, "PRICING_BOOKING.EXPORT_CONTROL.ALD");

    private Integer code;
    private String key;

    private ExportControlEnum(Integer code, String key) {
        this.code = code;
        this.key = key;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static List<ExportControlEnum> getValues() {
        ExportControlEnum[] values = ExportControlEnum.values();
        return Arrays.asList(values);
    }

    public static ExportControlEnum getValueByCode(Integer code) {
        ExportControlEnum[] values = ExportControlEnum.values();
        return Arrays.asList(values).stream().filter(v -> v.getCode().equals(code)).findFirst().orElse(null);
    }
}
