package com.adias.mytowereasy.enumeration;

import java.util.Arrays;
import java.util.List;

import com.adias.mytowereasy.util.GenericEnum;


public enum ControlRuleOperationTypesEnum implements GenericEnum {
    IS_TR_GENERATION(1, "Tr generation"),
    IS_FILE_CONDITION(2, "File condition"),
    IS_DATE_CONDITION(3, "Date condition");

    private Integer code;
    private String key;

    private ControlRuleOperationTypesEnum(Integer code, String key) {
        this.code = code;
        this.key = key;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static String getLabelBrokerByCode(Integer code) {
        ControlRuleOperationTypesEnum[] values = ControlRuleOperationTypesEnum.values();

        for (ControlRuleOperationTypesEnum crRuleOpType: values) {
            if (crRuleOpType.getCode().equals(code)) return crRuleOpType.getKey();
        }

        return null;
    }

    public static List<ControlRuleOperationTypesEnum> getValues() {
        ControlRuleOperationTypesEnum[] values = ControlRuleOperationTypesEnum.values();
        return Arrays.asList(values);
    }
}
