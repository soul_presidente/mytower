package com.adias.mytowereasy.enumeration;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.adias.mytowereasy.util.GenericEnum;


@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum RootCauseCategoriesEnum implements GenericEnum {
    MAN(1, "Man"),
    MACHINE(2, "Machine"),
    METHOD(3, "Method"),
    MEASURE(4, "Measure"),
    ENV(5, "Environnement"),
    MATERIAL(6, "Material");

    private Integer code;
    private String key;

    private RootCauseCategoriesEnum(Integer code, String key) {
        this.key = key;
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public static List<RootCauseCategoriesEnum> getValues() {
        RootCauseCategoriesEnum[] categories = RootCauseCategoriesEnum.values();
        return Arrays.asList(categories);
    }
}
