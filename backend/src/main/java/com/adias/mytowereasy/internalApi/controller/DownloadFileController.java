package com.adias.mytowereasy.internalApi.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.internalApi.service.DownloadFileService;


@RestController
@RequestMapping("api/internal/download-file")
public class DownloadFileController {
    @Autowired
    private DownloadFileService downloadFileService;

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]>
        getFileBinaireById(@PathVariable(name = "id") Integer fileId) throws IOException, MyTowerException {
        return downloadFileService.getBinaryFile(fileId);
    }
}
