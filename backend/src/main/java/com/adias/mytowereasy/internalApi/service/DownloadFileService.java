package com.adias.mytowereasy.internalApi.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;

import org.apache.commons.io.IOUtils;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import fr.mytower.lib.storage.api.StorageProvider;

import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.EbDemandeFichiersJoint;
import com.adias.mytowereasy.repository.EbDemandeFichiersJointRepositorty;


@Service
public class DownloadFileService {
    private final Environment env;
    private EbDemandeFichiersJointRepositorty ebDemandeFichiersJointRepositorty;
    private StorageProvider storageProvider;

    public DownloadFileService(
        Environment env,
        EbDemandeFichiersJointRepositorty ebDemandeFichiersJointRepositorty,
        StorageProvider storageProvider) {
        this.env = env;
        this.ebDemandeFichiersJointRepositorty = ebDemandeFichiersJointRepositorty;
        this.storageProvider = storageProvider;
    }

    public ResponseEntity<byte[]> getBinaryFile(Integer fileId) throws IOException, MyTowerException {
        HttpHeaders header = new HttpHeaders();

        byte[] content = null;
        EbDemandeFichiersJoint demandeFichiersJoint = ebDemandeFichiersJointRepositorty.findById(fileId).orElse(null);

        if (demandeFichiersJoint != null) {

            try (InputStream fileStream = storageProvider.get(Paths.get(demandeFichiersJoint.getChemin()))) {

                if (fileStream != null) {
                    content = IOUtils.toByteArray(fileStream);

                    header.setContentType(MediaType.valueOf(demandeFichiersJoint.getMimeType()));
                    header
                        .set(
                            HttpHeaders.CONTENT_DISPOSITION,
                            "attachment; filename=" + demandeFichiersJoint.getOriginFileName());
                    header.setContentLength(content.length);
                }

            }

        }

        if (demandeFichiersJoint == null || content == null) {
            throw new MyTowerException("No file found");
        }

        return new ResponseEntity<byte[]>(content, header, HttpStatus.OK);
    }
}
