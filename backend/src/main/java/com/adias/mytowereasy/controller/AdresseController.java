package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbAdresse;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/adresse")
public class AdresseController extends SuperControler {
    @PostMapping(value = "/list-addresses-table")
    public Map<String, Object> getListAddressestest(@RequestBody SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);
        List<EbAdresse> ebAdresses = null;
        HashMap<String, Object> result = new HashMap<String, Object>();

        if (connectedUser.isSuperAdmin()) {
            criteria.setEbEtablissementNum(null);
        }

        ebAdresses = adresseService.getEbAdresseByCriteria(criteria);
        Long count = adresseService.getListAddressesCount(criteria);

        result.put("count", count);
        result.put("data", ebAdresses);

        return result;
    }
}
