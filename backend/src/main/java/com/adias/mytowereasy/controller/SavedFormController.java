/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.dto.EbSavedFormSearchCriteriaDto;
import com.adias.mytowereasy.model.EbSavedForm;
import com.adias.mytowereasy.service.SavedFormService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "api/savedform")
@CrossOrigin("*")
public class SavedFormController extends SuperControler {
    @Autowired
    private SavedFormService savedFormService;

    @PostMapping(value = "/getByComponentAndUser")
    @ResponseBody
    public List<EbSavedForm> getListEbSavedForm(@RequestBody SearchCriteria criterias) {
        return savedFormService.selectListEbSavedForm(criterias);
    }

    @PostMapping("/delete")
    @ResponseBody
    public boolean deleteEbSavedForm(@RequestBody EbSavedForm ebSavedForm) {
        return savedFormService.deleteEbSavedForm(ebSavedForm);
    }

    @PostMapping("/upsert")
    @ResponseBody
    public EbSavedForm upsertEbSavedForm(@RequestBody EbSavedForm ebSavedForm) {
        return savedFormService.upsertEbSavedForm(ebSavedForm);
    }

    @GetMapping("/apply-action-to-history")
    public void
        applyActionToHistory(@RequestParam(required = true) Integer ebSavedFormNum) throws Exception, IOException {
        // TODO fix this before reenable it
        // savedFormService.applyActionToHistory(ebSavedFormNum);
    }

    @GetMapping(value = "/get-list-rules")
    public List<EbSavedForm> getListRules() throws IOException {
        List<EbSavedForm> ebSavedForms = savedFormService.selectListEbSavedFormByCompanie();
        return ebSavedForms;
    }

    @GetMapping(value = "/get-list-saved-search-values")
    public List<EbSavedFormSearchCriteriaDto>
        getListSavedSearchValues(@RequestParam(required = true) Integer ebSavedFormNum)
            throws IOException,
            ClassNotFoundException {
        return savedFormService.parseSavedSearch(ebSavedFormNum, true);
    }
}
