package com.adias.mytowereasy.controller;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.wickedsource.docxstamper.api.UnresolvedExpressionException;

import com.adias.mytowereasy.dto.EbTemplateDocumentsDTO;
import com.adias.mytowereasy.dto.TemplateGenParamsDTO;
import com.adias.mytowereasy.model.EbTemplateDocuments;
import com.adias.mytowereasy.template.service.PricingTemplateDocumentsService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "api/document-templates")
@CrossOrigin("*")
public class TemplateDocumentsController extends SuperControler {
    @Autowired
    PricingTemplateDocumentsService pricingTemplateDocumentsService;

    @RequestMapping(value = "list-document-templates-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object>
        listTemplateDocumentTable(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbTemplateDocuments> entities = templateDocumentsService.searchTemplateDocuments(criteria);
        List<EbTemplateDocumentsDTO> dtos = templateDocumentsService.convertEntitiesToDto(entities);
        Long count = templateDocumentsService.countListEbTemplateDocuments(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", dtos);
        return result;
    }

    @RequestMapping(value = "list-document-templates", method = RequestMethod.POST, produces = "application/json")
    public List<EbTemplateDocumentsDTO>
        listTemplateDocumentsDTO(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbTemplateDocuments> listTemplateDocuments = templateDocumentsService.searchTemplateDocuments(criteria);
        return templateDocumentsService.convertEntitiesToDto(listTemplateDocuments);
    }

    @RequestMapping(value = "add-document-templates", method = RequestMethod.POST, produces = "application/json")
    public EbTemplateDocumentsDTO
        addTemplateDocuments(@RequestBody(required = false) EbTemplateDocumentsDTO ebTemplateDocumentsDTO) {
        return templateDocumentsService.saveEbTemplateDocuments(ebTemplateDocumentsDTO);
    }

    @RequestMapping(value = "delete-document-templates", method = RequestMethod.POST, produces = "application/json")
    public void deleteTemplateDocuments(@RequestBody(required = false) Integer docTemplatesNum) {
        templateDocumentsService.deleteEbTemplateDocuments(docTemplatesNum);
    }

    @PostMapping("/attach-doc")
    public String attachDocLevelValue(
        @RequestParam("files") MultipartFile[] files,
        @RequestParam("ebCompagnieNum") Integer ebcompagnieNum)
        throws Exception {
        return templateDocumentsService.uploadFile(files[0], ebcompagnieNum);
    }

    @PostMapping("document-generation")
    public void templateGeneration(@RequestBody TemplateGenParamsDTO params, HttpServletResponse response)
        throws Exception {

        try {
            Path genFile = templateDocumentsService.templateGeneration(params, null);
            String extension = "";

            if (params.getSelectedFormat().intValue() == 0) {
                response.setContentType("application/pdf");
                extension = "pdf";
            }
            else {
                response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
                extension = "docx";
            }

            response.setHeader("Content-disposition", "attachment; filename=" + "export" + extension);

            try (OutputStream out = response.getOutputStream(); InputStream in = Files.newInputStream(genFile)) {
                IOUtils.copy(in, out);
            }
        } catch (UnresolvedExpressionException ure) {
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.setHeader("ERROR_NAME", "INVALID_TEMPLATE");

            PrintWriter writer = response.getWriter();
            writer.append("INVALID_TEMPLATE\n\n");
            ure.printStackTrace(writer);
            writer.flush();
        }

    }

    @PostMapping("cheked-information-mandatory")
    public boolean chekedInformationMandatory(@RequestBody TemplateGenParamsDTO params) throws Exception {
        return pricingTemplateDocumentsService.checkedInformationMandatory(params);
    }
}
