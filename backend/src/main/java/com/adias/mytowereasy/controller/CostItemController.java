package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.model.EbCost;
import com.adias.mytowereasy.model.EbCostCategorie;
import com.adias.mytowereasy.service.CostItemService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/cost-item/")
public class CostItemController extends SuperControler {
    @Autowired
    CostItemService costItemService;

    @PostMapping(value = "/list-costs-items")
    public @ResponseBody Map<String, Object> getListCostsItems(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbCost> data = costItemService.getListCostsItems(criteria);
        Long count = costItemService.getCountListCostItems(criteria);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", data);

        return result;
    }

    @PostMapping(value = "/list-cost-categorie")
    public List<EbCostCategorie> getListCostCategorieByCompagnie(
        @RequestBody(required = false) Integer compagnieNum,
        @RequestParam(required = false) Boolean activedOnly) {
        return costItemService.getListCostCategorie(compagnieNum, activedOnly);
    }

    @PostMapping(value = "/add-cost-item")
    public EbCost addCostItem(@RequestBody EbCost ebCost) {
        return costItemService.addCostItem(ebCost);
    }

    @PutMapping(value = "/update-cost-item")
    public EbCost updateCostItem(@RequestBody EbCost newEbCost) {
        return costItemService.updateCostItem(newEbCost);
    }

    @PostMapping(value = "/add-cost-categorie")
    public EbCostCategorie addCostCategorie(@RequestBody EbCostCategorie ebCostCategorie) {
        return costItemService.addCostCategorie(ebCostCategorie);
    }

    @DeleteMapping(value = "/delete-cost-item")
    public boolean deleteCostItem(@RequestParam Integer ebCostNum) {
        return costItemService.deleteCostItem(ebCostNum);
    }

    @PostMapping(value = "/verify-code-existance")
    public boolean checkCodeExistance(
        @RequestParam(value = "code") String codeCost,
        @RequestParam(value = "ebCompagnieNum") Integer ebCompagnieNum) {
        return costItemService.checkCodeExistance(codeCost, ebCompagnieNum);
    }
}
