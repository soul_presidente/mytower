package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbPlanTransportNew;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "api/plan-transport-new")
@CrossOrigin("*")
public class PlanTransportNewController extends SuperControler {
    @RequestMapping(value = "list-plan-transport-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object>
        listPlanTransportTable(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbPlanTransportNew> dtos = planTransportNewService.listPlanTransport(criteria);
        Long count = planTransportNewService.countListPlanTransport(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", dtos);
        return result;
    }

    @PostMapping(value = "/delete")
    public void deletePlanTransport(@RequestBody EbPlanTransportNew ebPlanTransportNew) {
        planTransportNewService.deletePlanTransport(ebPlanTransportNew);
    }

    @PostMapping(value = "/save")
    public void save(@RequestBody EbPlanTransportNew ebPlanTransportNew) {
        planTransportNewService.save(ebPlanTransportNew);
    }

    @GetMapping("/saveFlags")
    public EbPlanTransportNew saveFlag(@RequestParam Integer ebPlanTransportNum, @RequestParam String newFlags) {
        return planTransportNewService.saveFlags(ebPlanTransportNum, newFlags);
    }
}
