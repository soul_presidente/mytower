package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.dto.EbBoutonActionDTO;
import com.adias.mytowereasy.model.EbBoutonAction;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/bouton-action")
public class BoutonActionController extends SuperControler {
    @PostMapping(value = "/add-bouton-action")
    public EbBoutonAction addBoutonAction(@RequestBody EbBoutonAction ebBoutonAction) {
        return boutonActionService.addBoutonAction(ebBoutonAction);
    }

    @PostMapping(value = "/list-bouton-action", produces = "application/json")
    public @ResponseBody Map<String, Object>
        getListBoutonAction(@RequestBody(required = false) SearchCriteria criteria) {
        if (criteria == null) criteria = new SearchCriteria();

        List<EbBoutonAction> boutonActions = boutonActionService.getListBoutonAction(criteria);
        Long count = boutonActionService.countListBoutonAction(criteria);
        HashMap<String, Object> result = new HashMap<>();
        result.put("count", count);
        result.put("data", boutonActions);
        return result;
    }

    @GetMapping(value = "/get-list-bouton-action")
    public List<EbBoutonAction> getListBoutonAction() {
        return boutonActionService.getListBoutonAction(new SearchCriteria());
    }

    @PostMapping(value = "/delete-bouton-action")
    public void deleteBoutonAction(@RequestBody Integer ebBoutonActionNum) {
        boutonActionService.deleteBoutonAction(ebBoutonActionNum);
    }

    @PostMapping(value = "/get-list-bouton-action-from-codes")
    public List<EbBoutonActionDTO> getListBoutonActionFromCodes(@RequestBody String listBoutonActionCodes) {
        return boutonActionService.getListBoutonActionFromCodes(listBoutonActionCodes);
    }
}
