/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.EmailConfig;
import com.adias.mytowereasy.model.InscriptionWrapper;
import com.adias.mytowereasy.repository.EbEtablissementRepository;
import com.adias.mytowereasy.util.EmptyJsonResponse;


@RestController
@RequestMapping("api/inscription")
public class InscriptionController extends SuperControler {
    @Autowired
    EbEtablissementRepository ebEtablissementRepository;

    @PostMapping(value = "/inscription-user")
    public EbUser inscriptionUser(@Valid @RequestBody EbUser ebUser) {
        return inscriptionService.inscriptionPublic(ebUser);
    }

    @PostMapping(value = "/save-user")
    public EbUser sinscrire(@Valid @RequestBody EbUser ebUser) {
        return inscriptionService.inscrire(ebUser);
    }

    @PostMapping(
        value = "",
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<EbUser>
        inviterCollegue(@Valid @RequestBody InscriptionWrapper inscriptionWrapper, BindingResult bindingResult) {

        if (bindingResult.hasErrors() || inscriptionWrapper == null || inscriptionWrapper.getEbUser() == null
            || inscriptionWrapper.getEbUser().getEbEtablissement() == null
            || inscriptionWrapper.getEbUser().getEbEtablissement().getEbCompagnie() == null) {
            BindingErrorsResponse bindingErrorsResponse = new BindingErrorsResponse();
            bindingErrorsResponse.addAllErrors(bindingResult);

            HttpHeaders headers = new HttpHeaders();
            headers.add("erreurs", bindingErrorsResponse.toJSON());

            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }

        // EbUser ebUser =
        // inscriptionService.inscrire(inscriptionWrapper.getEbUser());

        EbUser ebUser = inscriptionWrapper.getEbUser();

        if (ebUser != null) {
            emailService
                .sendEmailInvitationFriend(
                    ebUser,
                    inscriptionWrapper.getFriendInvitationEmails(),
                    EmailConfig.INVITE_FRIEND);

            emailService
                .sendEmailInvitationCollegue(
                    ebUser,
                    inscriptionWrapper.getCollegueInvitationEmails(),
                    EmailConfig.INVITE_COLLEGUE);

            return new ResponseEntity<>(inscriptionWrapper.getEbUser(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(value = "/emailExist")
    public boolean checkEmail(@RequestBody String email) {
        boolean emailExist = inscriptionService.checkUserByEmail(email);
        return emailExist;
    }

    @PostMapping(value = "/usernameExist")
    public boolean checkUsername(@RequestBody String email) {
        boolean usernameExist = inscriptionService.checkUserByUsername(email);
        return usernameExist;
    }

    @GetMapping(value = "/getEtablissement")
    public ResponseEntity<Object> getEbEtablissementBySiret(@RequestParam String siret) {
        EbEtablissement ebEtablissement = inscriptionService.getEbEtablissementBySiret(siret);

        if (ebEtablissement == null) {
            return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(ebEtablissement, HttpStatus.OK);
        }

    }

    @GetMapping(value = "/getCompagnie")
    @ResponseBody
    public ResponseEntity<Object> getEbCompagnieBySiren(@RequestParam String siren) {
        EbCompagnie cp = inscriptionService.getEbCompagnieBySiren(siren);

        if (cp == null) {
            return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(cp, HttpStatus.OK);
        }

    }

    @RequestMapping(value = "activation-compte/{token}", method = RequestMethod.GET)
    @ResponseBody
    public Boolean activationCompte(@PathVariable String token) {
        return userService.activateAccount(token);
    }

    // @PostMapping(value = "/activate-account", consumes =
    // MediaType.APPLICATION_JSON_UTF8_VALUE)
    // public ResponseEntity<Boolean> activateAccount(@RequestBody String token)
    // {
    //
    // if (token == null) {
    //
    // return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
    //
    // }else {
    //
    // EbUser ebUser = userService.getEbUserByActiveToken(token);
    //
    // if (ebUser != null ) {
    //
    // userService.activateAccount(token);
    //
    // return new ResponseEntity<>(true, HttpStatus.OK);
    //
    // } else {
    //
    // return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
    // }
    // }
    // }
    //
}
