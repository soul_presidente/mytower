/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController()
@RequestMapping("api/categorie")
public class CategorieController extends SuperControler {
    @PostMapping(value = "add")
    public EbCategorie addEbCategorie(@Valid @RequestBody EbCategorie ebCategorie) {
        return categorieService.addEbCategorie(ebCategorie);
    }

    @RequestMapping(value = "get-all", method = RequestMethod.POST, produces = "application/json")
    public Collection<EbCategorie> getAllEbCategorie(@RequestBody SearchCriteria criteria) {
        return categorieService.getAllEbCategorie(criteria);
    }

    @PutMapping(value = "update")
    public EbCategorie updateEbCategorie(@RequestBody EbCategorie ebCategorie) {
        return categorieService.updateEbCategorie(ebCategorie);
    }

    @PostMapping(value = "delete")
    public boolean deleteEbCategorie(@RequestBody EbCategorie ebCategorie) {
        categorieService.deleteEbCategorie(ebCategorie);

        return true;
    }

    ///////////////////////////////////////////////////////////////////////////////////

    @PostMapping(value = "label/add")
    public EbLabel addEbLabel(@Valid @RequestBody EbLabel ebLabel) {
        return categorieService.addEbLabel(ebLabel);
    }

    @PostMapping(value = "label/get-all")
    public HashMap<String, Object> getAll(@RequestBody SearchCriteria criteria) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("data", categorieService.getAllEbLabel(criteria));
        result.put("count", categorieService.getLabelCount(criteria));
        return result;
    }

    @PostMapping(value = "label/get-by-libelle")
    public List<EbLabel> getByLibelle(@RequestBody SearchCriteria criteria) {
        return categorieService.getByLibelle(criteria);
    }

    @PutMapping(value = "label/update")
    public EbLabel updateEbLabel(@RequestBody EbLabel ebLabel) {
        return categorieService.updateEbLabel(ebLabel);
    }

    @PostMapping(value = "label/delete")
    public boolean deleteEbLabel(@RequestBody EbLabel ebLabel) {
        categorieService.deleteEbLabel(ebLabel);

        return true;
    }
}
