package com.adias.mytowereasy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.model.EbEmailHistorique;
import com.adias.mytowereasy.service.EmailHistoriqueService;


@CrossOrigin("*")
@RestController
@RequestMapping("api/email-historique")
public class EmailHistoriqueController extends SuperControler {
    @Autowired
    EmailHistoriqueService emailHistoriqueService;

    @PostMapping(value = "/get-all-email")
    public List<EbEmailHistorique> getAllEmailHistorique(@RequestBody Integer ebIncidentNum) {
        return emailHistoriqueService.getAllEmailHistorique(ebIncidentNum);
    }

    /*
     * @GetMapping(value = "get-all-email")
     * public List<EbEmailHistorique> getAllEmailHistorique() {
     * return emailHistoriqueService.getAllEmailHistorique();
     * }
     */
}
