package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.model.EbFlag;
import com.adias.mytowereasy.utils.search.PageableSearchCriteria;


@RestController
@RequestMapping("api/flag")
public class FlagController extends SuperControler {
    @PostMapping(value = "/add-flag")
    public EbFlag addFlag(@RequestBody EbFlag ebFlag) {
        return flagService.addFlag(ebFlag);
    }

    @PostMapping(value = "/list-flag-table", produces = "application/json")
    public @ResponseBody Map<String, Object>
        getListFlagByCompagnieNum(@RequestBody(required = false) PageableSearchCriteria flagSearchCriteria) {

        List<EbFlag> flags = flagSearchCriteria == null ?
            flagService.getListFlagByCompagnieNum() :
            flagService.getListFlagByCompagnieNum(flagSearchCriteria);
        
        Long count = flagService.countListFlag(flagSearchCriteria);
        HashMap<String, Object> result = new HashMap<>();
        result.put("count", count);
        result.put("data", flags);
        return result;
    }

    @GetMapping(value = "/get-list-flag/{moduleNum}")
    public List<EbFlag> getListFlag(@PathVariable("moduleNum") Integer moduleNum) {
        return flagService.getListFlag(moduleNum);
    }

    @PostMapping(value = "/delete-flag")
    public void deleteFlag(@RequestBody Integer ebFlagNum) {
        flagService.deleteFlag(ebFlagNum);
    }

    @PostMapping(value = "/get-list-flag-from-codes")
    public List<EbFlagDTO> getListFlagFromCodes(@RequestBody String listFlagCodes) {
        return flagService.getListFlagDTOFromCodes(listFlagCodes);
    }
}
