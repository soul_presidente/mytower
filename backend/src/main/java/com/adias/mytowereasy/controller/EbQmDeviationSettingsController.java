package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.dao.DaoQmDeviationSettings;
import com.adias.mytowereasy.repository.EbQmDeviationSettingsRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


@RestController
@RequestMapping(value = "api/quality-management")
@CrossOrigin("*")
public class EbQmDeviationSettingsController {
    @Autowired
    private DaoQmDeviationSettings daoQmDeviationSettings;
    @Autowired
    private EbQmDeviationSettingsRepository ebQmDeviationSettingsRepository;
    @Autowired
    ConnectedUserService userService;

    @RequestMapping(value = "list-Deviation-Setting", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getListDeviationSetting(@RequestBody(required = false) SearchCriteriaQM criteria) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", daoQmDeviationSettings.getListEbQmDeviationSettingsCount(criteria));
        result.put("data", daoQmDeviationSettings.listDeviation(criteria));
        return result;
    }
}
