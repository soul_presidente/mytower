package com.adias.mytowereasy.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbUserTag;


@RestController
@RequestMapping(value = "api/user-tag")
@CrossOrigin("*")
public class UserTagController extends SuperControler {
    @PostMapping(value = "/create-user-tag")
    public EbUserTag
        addUserTag(@RequestBody EbUserTag ebUserTag, @RequestParam(required = false) Integer ebCompagnieNum) {
        return userTagService.addUserTag(ebUserTag, ebCompagnieNum);
    }
}
