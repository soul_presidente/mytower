/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class ForwardController {
    // Filter Point Entry like /socket/**
    @RequestMapping(value = {
        "/{path:[^\\.]*}", "/{path:^(?!socket).*}/**/{path:[^\\.]*}"
    })
    public String redirect() {
        // Angular frontend forwarding
        return "forward:/";
    }
}
