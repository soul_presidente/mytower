package com.adias.mytowereasy.controller;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbTypeGoods;
import com.adias.mytowereasy.repository.EbTypeGoodsRepository;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "api/type-goods")
@CrossOrigin("*")
public class TypeGoodsController extends SuperControler {
    @Autowired
    EbTypeGoodsRepository typeGoodsRepository;

    ////// Type de marchandises

    @RequestMapping(value = "list-type-goods-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object> listTypeGoodsTable(@RequestBody(required = false) SearchCriteria criteria)

    {
        List<EbTypeGoods> dtos = typeGoodsService.listTypeGoods(criteria);
        Long count = typeGoodsService.countListTypeGoods(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", dtos);
        return result;
    }

    @PostMapping(value = "save-type-goods")
    public EbTypeGoods saveTypeGoods(@RequestBody EbTypeGoods ebTypeGoods) {
        return typeGoodsService.saveTypeGoods(ebTypeGoods);
    }

    @DeleteMapping(value = "delete-type-goods")
    public Boolean deleteTypeGoods(@RequestParam Integer typeGoodsNum) {
        return typeGoodsService.deleteTypeGoods(typeGoodsNum);
    }

    @GetMapping(value = "list-type-goods/{compagnieNum}")
    public @ResponseBody List<EbTypeGoods> listTypeGoods(@PathVariable(name = "compagnieNum") Integer compagnieNum) {
        SearchCriteria criteria = new SearchCriteria();
        criteria.setCompanyNums(Arrays.asList(compagnieNum));
        return typeGoodsService.listTypeGoods(criteria);
    }

    @GetMapping(value = "list-type-goods-contact")
    public @ResponseBody List<EbTypeGoods> listTypeGoodsContacts() {
        return typeGoodsService.listWithCommunity();
    }

    @GetMapping(value = "list-code-for-company")
    public @ResponseBody List<String> listCodeForCompany(@RequestParam(required = false) Integer ebCompagnieNum) {

        if (ebCompagnieNum == null) {
            ebCompagnieNum = connectedUserService.getCurrentUser().getEbCompagnie().getEbCompagnieNum();
        }

        return typeGoodsRepository.listCodeForCompany(ebCompagnieNum);
    }
}
