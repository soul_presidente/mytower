package com.adias.mytowereasy.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbControlRule;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.ControlRuleService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "api/control-rule")
@CrossOrigin("*")
public class ControlRuleController {
    @Autowired
    ControlRuleService controlRuleService;

    @Autowired
    ConnectedUserService connectedUserService;

    @PostMapping(value = "all")
    public HashMap<String, Object> getListControlRule(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbControlRule> controlRuleList = new ArrayList<EbControlRule>();

        try {
            controlRuleList = controlRuleService.getListControlRuleDashboard(criteria);
        } catch (Exception e) {
            e.printStackTrace();
        }

        long counter = controlRuleService.listCount(criteria);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", counter);
        result.put("data", controlRuleList);
        return result;
    }

    @GetMapping
    public EbControlRule getControlRule(@RequestParam("ebControlRuleNum") Integer ebControlRuleNum) {
        return controlRuleService.getControlRule(ebControlRuleNum);
    }

    @PostMapping
    public EbControlRule addControlRule(@RequestBody EbControlRule ebControlRule) throws Exception {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        ebControlRule = controlRuleService.addControlRule(ebControlRule); // persist
                                                                          // control
                                                                          // rule

        controlRuleService.executeControlRulesWithAsyn(ebControlRule, connectedUser); // run
                                                                                      // conditions
                                                                                      // &&
                                                                                      // actions
                                                                                      // execution
                                                                                      // in
                                                                                      // background

        return ebControlRule; // returning control rule
    }

    @PutMapping
    public EbControlRule updateControlRule(@RequestBody EbControlRule ebControlRule) throws Exception {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        ebControlRule = controlRuleService.updateControlRule(ebControlRule);

        controlRuleService.executeControlRulesWithAsyn(ebControlRule, connectedUser); // run
                                                                                      // conditions
                                                                                      // &&
                                                                                      // actions
                                                                                      // execution
                                                                                      // in
                                                                                      // background

        return ebControlRule; // returning control rule
    }

    @GetMapping(value = "deactivate")
    public boolean deleteControlRule(
        @RequestParam("ebControlRuleNum") Integer ebControlRuleNum,
        @RequestParam("activated") Boolean activated) {
        return controlRuleService.deactivateControlRule(ebControlRuleNum, activated);
    }

    @GetMapping(value = "process")
    public List<Map<String, Object>> processControlRules(
        @RequestParam("ebCompagnieNum") Integer ebCompagnieNum,
        @RequestParam(value = "ebTtTracingNum", required = false) Integer ebTtTracingNum,
        @RequestParam(value = "ruleCategory", required = false) Integer ruleCategory,
        @RequestParam(value = "ebDemandeNum", required = false) Integer ebDemandeNum,
        @RequestParam(value = "isCreation", required = false) Boolean isCreation)
        throws Exception {
        return controlRuleService
            .processControlRules(
                ebCompagnieNum,
                ebTtTracingNum,
                null,
                null,
                ruleCategory,
                null,
                ebDemandeNum,
                isCreation,
                null,
                null);
    }

    @GetMapping(value = "doc-rule-inputs-data")
    public Map<String, Object> getInputsData() throws Exception {
        return this.controlRuleService.getDocRulesInputsData();
    }

    @PostMapping(value = "delete-control-rule")
    public boolean deleteControlRule(@RequestBody Integer ebControlRuleNum) {
        return this.controlRuleService.deleteControlRule(ebControlRuleNum);
    }

    @PostMapping(value = "apply-rules-with-action-exportControl")
    public boolean applyRulesWithExportControlEnabled(@RequestBody EbDemande demande) throws Exception {
        return this.controlRuleService.applyRulesWithExportControlEnabled(demande);
    }
}
