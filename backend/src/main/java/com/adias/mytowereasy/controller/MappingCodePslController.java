package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.cronjob.tt.model.EdiMapPsl;
import com.adias.mytowereasy.service.CustomFieldService;
import com.adias.mytowereasy.utils.search.SearchCriteriaMappingCodePsl;


@RestController
@RequestMapping("api/mapping-code-psl")
public class MappingCodePslController extends SuperControler {
    @Autowired
    CustomFieldService customFieldService;

    @PostMapping(value = "/list-mapping-code-psl")
    public @ResponseBody Map<String, Object>
        getListMappingCodePsl(@RequestBody(required = false) SearchCriteriaMappingCodePsl criteria)
            throws JsonProcessingException,
            Exception {

        // this helps to populate the lists defined in
        // SearchCriteriaMappingCodePsl, otherwise the lists will be empty
        if (criteria.getSearchInput() != null && criteria.getSearchInput().length() > 0) {
            criteria
                .setSearchInput(criteria.getSearchInput(), customFieldService.getCustomFields(criteria.getEbUserNum()));
        }

        List<EdiMapPsl> data = mappingCodePslService.getListMappingCodePsl(criteria);
        Long count = mappingCodePslService.getListMappingCodePslCount(criteria);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", data);

        return result;
    }

    @PostMapping(value = "update-mapping-code-psl")
    public EdiMapPsl updateMappingCodePsl(@RequestBody EdiMapPsl ediMapPsl) {
        return mappingCodePslService.updateMappingCodePsl(ediMapPsl);
    }

    @PostMapping(value = "delete-mapping-code-psl")
    public @ResponseBody Boolean deleteMappingCodePsl(@RequestBody Integer id) {
        return mappingCodePslService.deleteMappingCodePsl(id);
    }

    @RequestMapping(value = "/list-mapping-autocomplete", method = RequestMethod.GET)
    public String getListMappingCodePsl(
        @RequestParam(required = false, value = "term") String term,
        @RequestParam(required = true, value = "field") String field)
        throws JsonProcessingException {
        return mappingCodePslService.getAutoCompleteListMappingCodePsl(term, field);
    }
}
