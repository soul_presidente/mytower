/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.util.List;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.dto.EbDestinationCountryDTO;
import com.adias.mytowereasy.model.EbCombinaison;
import com.adias.mytowereasy.model.EbDestinationCountry;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/compliance-matrix")
public class ComplianceMatrixController extends SuperControler {
    @PostMapping(value = "/get-destination-country")
    public EbDestinationCountry getDestinationCountry(@RequestBody SearchCriteria criteria) {
        EbDestinationCountry ebDestinationCountry = complianceMatrixService.selectEbDestinationCountry(criteria);
        return ebDestinationCountry;
    }

    @PostMapping(value = "/add-destination-country")
    public EbDestinationCountry addDestinationCountry(@RequestBody EbDestinationCountry ebDestinationCountry) {
        return complianceMatrixService.insertEbDestinationCountry(ebDestinationCountry);
    }

    @PostMapping(value = "/list-destination-country")
    public List<EbDestinationCountryDTO> selectListEbDestinationCountry(@RequestBody SearchCriteria criteria) {
        return complianceMatrixService.selectListEbDestinationCountry(criteria);
    }

    @PostMapping(value = "/edit-destination-country")
    public EbDestinationCountry editDestinationCountry(@RequestBody EbDestinationCountry ebDestinationCountry) {
        EbDestinationCountry destinationCountry = complianceMatrixService
            .updateEbDestinationCountry(ebDestinationCountry);
        return destinationCountry;
    }

    @PostMapping(value = "/delete-destination-country")
    public Boolean deleteDestinationCountry(@RequestBody EbDestinationCountry ebDestinationCountry) {
        return complianceMatrixService.deleteEbDestinationCountry(ebDestinationCountry);
    }

    @PostMapping(value = "/list-destination-country-by-company")
    public List<EbDestinationCountryDTO> getAllDestinationCountryByCompagnie(@RequestBody SearchCriteria criteria) {
        List<EbDestinationCountryDTO> listebDestination = this.complianceMatrixService
            .selectListEbDestinationCountryByCompany(criteria);
        return listebDestination;
    }
    // -------------------------------------------------------------- //

    @PostMapping(value = "/list-combinaison")
    public List<EbCombinaison> selectListEbCombinaison(@RequestBody SearchCriteria criteria) {
        return complianceMatrixService.selectListEbCombinaison(criteria);
    }

    @PostMapping(value = "/add-combinaison")
    public EbCombinaison addCombinaison(@RequestBody EbCombinaison ebCombinaison) {
        return complianceMatrixService.insertEbCombinaison(ebCombinaison);
    }

    @PostMapping(value = "/delete-combinaison")
    public boolean deleteCombinaison(@RequestBody EbCombinaison ebCombinaison) {
        complianceMatrixService.deleteEbCombinaison(ebCombinaison);
        return true;
    }
}
