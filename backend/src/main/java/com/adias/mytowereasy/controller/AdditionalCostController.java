package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbAdditionalCost;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/additional-cost/")
public class AdditionalCostController extends SuperControler {
    @PostMapping(value = "/list-additional-costs")
    public @ResponseBody Map<String, Object>
        getListAdditionalCosts(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbAdditionalCost> data = additionalCostService.getListAdditionalCosts(criteria);
        Long count = additionalCostService.getCountListAdditionalCosts(criteria);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", data);

        return result;
    }

    @PostMapping(value = "/upsert-additional-costs")
    public EbAdditionalCost upsertAdditionalCost(@RequestBody EbAdditionalCost cost) {
        return additionalCostService.upsertAdditionalCost(cost);
    }

    @PostMapping(value = "/disable-additional-cost")
    public boolean desactivateAdditionalCost(@RequestBody Integer ebAdditionalCostNum) {
        return additionalCostService.desactivateAdditionalCost(ebAdditionalCostNum);
    }

    @PostMapping(value = "/verify-code-existance")
    public boolean checkCodeExistance(
        @RequestParam(value = "codeCost") String codeCost,
        @RequestParam(value = "ebCompagnieNum") Integer ebCompagnieNum) {
        return additionalCostService.checkCodeExistance(codeCost, ebCompagnieNum);
    }

    @PostMapping(value = "/list-additionalCostsByebcompagnieNum")
    public List<EbAdditionalCost>
        getListAdditionalCostsByebCompanieNum(@RequestBody(required = true) Integer ebCompagnieNum) {
        return additionalCostService.getEbAdditionalCostByebCompagnieNum(ebCompagnieNum);
    }
}
