/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.annotations.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.dto.RelationToChargeursDTO;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.service.UserService;
import com.adias.mytowereasy.service.email.EmailService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "api/community")
@CrossOrigin("*")
public class CommunityController extends SuperControler {
    @Autowired
    UserService userService;

    @Autowired
    EmailService emailService;

    @Autowired
    EbUserRepository ebUserRepository;

    @Autowired
    EbDemandeRepository ebDemandeRepository;
    @Type(type = "jsonb")
    @JsonFormat
    private List<?> xEbUserObserver;

    @PostMapping(value = "/accepter-relation")
    public Boolean accepterContactRequest(@RequestBody EbRelation ebRelation) {
        Boolean result = userService.accepterContactRequest(ebRelation);

        if (result) {
            EbUser guest = ebUserRepository
                .findUserByEbUserNum(ebRelation.getGuest().getEbUserNum())
                .orElseThrow(() -> new MyTowerException("Guest is null"));
            EbUser host = ebUserRepository
                .findUserByEbUserNum(ebRelation.getHost().getEbUserNum())
                .orElseThrow(() -> new MyTowerException("Host is null"));

            emailService.sendEmailInvitationAccepter(guest, host);
        }

        return result;
    }

    @PostMapping(value = "/ignorer-relation")
    public Boolean ignorerContactRequest(@RequestBody EbRelation ebRelation) {
        return userService.ignorerContactRequest(ebRelation);
    }

    @PostMapping(value = "/get-contacts")
    public Map<String, Object> getContact(@RequestBody SearchCriteria criteria) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("records", userService.selectListContact(criteria));
        result.put("total", userService.selectCountListContact(criteria));
        return result;
    }

    @PostMapping(value = "/listTransporteur")
    public String selectListCarrier(@RequestBody(required = false) SearchCriteria criteria)
        throws JsonProcessingException {
        return userService.selectListCarrier(criteria);
    }

    @PostMapping(value = "/update-contact")
    public EbRelation updateContact(@RequestBody EbRelation ebRelation) {
        return userService.updateEbRelation(ebRelation);
    }

    @PostMapping(value = "/get-etablissements-contact")
    public Map<String, Object> getEtablissementsContact(@RequestBody SearchCriteria criteria) {
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("records", userService.selectListEbEtablissementOfEbRelation(criteria));
        result.put("total", userService.selectCountListEbEtablissementOfEbRelation(criteria));
        return result;
    }

    // @PostMapping(value="/get-sending-request")
    // public List<EbContactRequest> selectListSendingRequestByUser(@RequestBody
    // SearchCriteria
    // criteria) {
    // return userService.selectListDemandesEnvoyees(criteria);
    // }
    //
    // @PostMapping(value="/get-receiving-request")
    // public List<EbContactRequest>
    // selectListReceivingRequestByUser(@RequestBody SearchCriteria
    // criteria) {
    // return userService.selectListDemandesRecues(criteria);
    // }

    @PostMapping(value = "/annuler-relation")
    public boolean annulerContactRequest(@RequestBody EbRelation ebRelation) {
        return userService.annulerContactRequest(ebRelation);
    }

    @PostMapping(value = "/get-list-user")
    public List<EbUser> getListUser(@RequestBody SearchCriteria criteria) {
        return userService.selectListEbUser(criteria);
    }


    @PostMapping(value = "/get-list-user-management")
    public Map<String, Object> getListUserWithPagination(@RequestBody SearchCriteria criteria) {
        Map<String, Object> result = new HashMap<String, Object>();

        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);
        List<EbUser> listUsers = new ArrayList<>();
        List<EbUserGroup> listGroups = new ArrayList<>();

        listUsers = userService.selectListEbUser(criteria);
        listGroups = userService.getListUserGroup(connectedUser);

        for (EbUser user: listUsers) {
            StringBuilder userGroupes = new StringBuilder();

            if (user.getGroupes() != null) {

                for (EbUserGroup group: listGroups) {

                    if (user.getGroupes().indexOf("|" + group.getEbUserGroupNum() + "|") != -1) {
                        userGroupes.append(group.getName() + ", ");
                    }

                }

            }

            user.setGroupes(userGroupes.toString());
        }

        result.put("data", listUsers);
        result.put("count", userService.selectCountListUser(criteria));
        return result;
    }

    @PostMapping(value = "/get-list-group-user")
    public List<EbUserGroup> getListUserGroup(@RequestBody EbUser ebUser) {
        return userService.getListUserGroup(ebUser);
    }

    @PostMapping(value = "/update-user-group")
    public boolean updateUserGroup(@RequestBody SearchCriteria criteria, @RequestBody Integer EbUserNum) {
        return userService.updateUserGroup(criteria, EbUserNum);
    }

    @PostMapping(value = "/send-invitaion")
    public EbRelation SendInvitaion(@RequestBody EbRelation ebRelation) {
        return userService.sendInvitation(ebRelation);
    }

    @PostMapping(value = "/add-to-another-chargeurs")
    public boolean addToAnotherChargeurs(@RequestBody RelationToChargeursDTO relationToChargeur) {

        for (EbUser chargeur: relationToChargeur.getSelectChargeurs()) {
            relationToChargeur.getEbRelation().setHost(chargeur);
            userService.sendInvitation(relationToChargeur.getEbRelation());
        }

        return true;
    }

    @PostMapping(value = "/send-invitaion-bymail")
    public Map<String, Object> SendInvitaionByMail(@RequestBody EbRelation ebRelation) {
        return userService.sendInvitationByMail(ebRelation, null, null);
    }

    @PostMapping(value = "/get-one-company-contact")
    public List<EbCompagnie> getEbCompagnieForContact(@RequestBody SearchCriteria criteria) {
        return userService.selectListOneListEbCompagnieByOneContact(criteria);
    }

    @PostMapping(value = "/delete-selected-users")
    public String deleteSelectedUsers(@RequestBody List<EbUser> users) {
        return userService.deleteSelectedUsers(users);
    }

    @PostMapping(value = "/check-ability-to-delete-users")
    public List<String> checkAbilityToDeleteUers(@RequestBody List<EbUser> users) {
        return userService.checkAbilityToDeleteUers(users);
    }

    @PostMapping(value = "/get-contacts-compagnie")
    public List<EbCompagnie> getContactCompagnie(@RequestBody SearchCriteria criteria) {
        return userService.selectListContactCompagnieOfRelation(criteria);
    }
}
