package com.adias.mytowereasy.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.dto.CodeLibelleDTO;
import com.adias.mytowereasy.model.Enumeration.IncidentStatus;


@RestController
@RequestMapping(value = "api/quality-management")
@CrossOrigin("*")
public class QualityManagementListeStatiqueController extends SuperControler {
    @RequestMapping(value = "/list-incident-status")
    public List<CodeLibelleDTO> listIncidentStatus() throws IOException {
        List list = IncidentStatus.getListStatutIncidentDTO();
        return list;
    }
}
