/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.service.CustomFieldService;
import com.adias.mytowereasy.utils.search.SearchCriteria.OrderCriterias;
import com.adias.mytowereasy.utils.search.SearchCriteriaFreightAudit;


@CrossOrigin("*")
@RestController
@RequestMapping("api/freight-audit")
public class FreightAuditController extends SuperControler {
    @Autowired
    CustomFieldService customFieldService;

    @GetMapping(value = "/list-action-type", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List getListActionType() throws IOException {
        List listStatuts = FAEnumeration.ActionType.getListActionType();

        return listStatuts;
    }

    @GetMapping(value = "/list-status", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List getListStatus() throws IOException {
        // List listStatuts = FAEnumeration.InvoiceStatus.getListStatus();
        return freightAuditService.getListStatus();
    }

    @PostMapping(value = "/listinvoice")
    public HashMap<String, Object>
        ListFreightAudit(@RequestBody SearchCriteriaFreightAudit params) throws JsonProcessingException, Exception {

        if (params.getSearchInput() != null && params.getSearchInput().length() > 0) {
            params.setSearchInput(params.getSearchInput(), customFieldService.getCustomFields(params.getEbUserNum()));
        }

        if (params.getOrderedColumn() != null) {
            Map<OrderCriterias, String> map = new HashMap<OrderCriterias, String>();
            List<Map<OrderCriterias, String>> listMap = new ArrayList<Map<OrderCriterias, String>>();
            map.put(OrderCriterias.columnName, params.getOrderedColumn());
            map.put(OrderCriterias.dir, params.getAscendant() ? "asc" : "desc");
            listMap.add(map);
            params.setOrder(listMap);
        }

        if (params.getSearchterm() != null && !params.getSearchterm().trim().isEmpty()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", params.getSearchterm());
            params.setSearch(map);
        }

        List<EbInvoice> InvoiceList = freightAuditService.getListEbInvoice(params);
        int counter = freightAuditService.getCountListEbInvoice(params);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", counter);
        result.put("data", InvoiceList);

        return result;
    }

    @PostMapping(value = "/list-count")
    public Integer
        getListCount(@RequestBody SearchCriteriaFreightAudit params) throws JsonProcessingException, Exception {

        if (StringUtils.isNotBlank(params.getSearchInput())) {
            params.setSearchInput(params.getSearchInput(), customFieldService.getCustomFields(params.getEbUserNum()));
        }

        return freightAuditService.getCountListEbInvoice(params);
    }

    @PostMapping(value = "/get-invoice")
    public EbInvoice getInvoice(@RequestBody SearchCriteriaFreightAudit criterias) {
        return freightAuditService.getListEbInvoice(criterias).get(0);
    }

    @RequestMapping(value = "/getListEbInvoiceByEbInvoiceNum", method = RequestMethod.POST)
    public List<EbDemandeFichiersJoint> getListEbInvoiceByEbInvoiceNum(@RequestBody Integer ebInvoiceNum) {
        return freightAuditService.getListEbInvoiceByEbInvoiceNum(ebInvoiceNum);
    }

    @PostMapping(value = "/generate-invoice")
    public void factureDemande(@RequestBody Integer ebDemandNum) {
        freightAuditService.generateInvoice(ebDemandNum);
    }

    @RequestMapping(value = "/generate-all-invoice")
    public int generateAllInvoices() {
        int result = freightAuditService.generateAllInvoice();
        return result;
    }

    @PostMapping(value = "/delete-ebInvoiceFichierJoint")
    public void deleteFile(@RequestBody Integer ebFichierJointNum) {
        freightAuditService.deleteFile(ebFichierJointNum);
    }

    @PostMapping(value = "/getDemande")
    public EbDemande getDemande(@RequestBody Integer ebDemandeNum) {
        return freightAuditService.getDemande(ebDemandeNum);
    }

    @PostMapping(value = "/deleteNbr")
    public Integer deleteNbr(@RequestBody Integer ebInvoiceNum) {
        return freightAuditService.deleteNbr(ebInvoiceNum);
    }

    @PostMapping(value = "/setTag")
    public Integer setTag(@RequestBody EbDemandeFichiersJoint inv) {
        return freightAuditService.setTag(inv);
    }

    @PostMapping(value = "/updateListCostCategorieAndPrice")
    public EbInvoice updateListCostCategorieAndPrice(@RequestBody EbInvoice ebInvoice) {
        return freightAuditService.updateListCostCategorieAndPrice(ebInvoice);
    }

    @PostMapping(value = "/updateListTypeDocuments")
    public EbInvoice updateListTypeDocuments(@RequestBody EbInvoice ebInvoice) {
        return freightAuditService.updateListTypeDocuments(ebInvoice);
    }

    @GetMapping("/saveFlags")
    public EbInvoice saveFlag(@RequestParam Integer ebInvoiceNum, @RequestParam String newFlags) {
        return freightAuditService.saveFlags(ebInvoiceNum, newFlags);
    }

    @PostMapping(value = "/all-action")
    public Integer actionInvoice(@RequestBody InvoiceWrapper invoiceWrap) {
        return freightAuditService.actionInvoice(invoiceWrap);
    }
}
