package com.adias.mytowereasy.controller;

import java.io.IOException;
import java.util.*;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbMarchandiseForExtraction;
import com.adias.mytowereasy.model.Enumeration.TypeContainer;
import com.adias.mytowereasy.model.ExcelToHeaderConfig;
import com.adias.mytowereasy.repository.EbMarchandiseRepository;
import com.adias.mytowereasy.service.UnitService;
import com.adias.mytowereasy.util.ConsolidateExtraction;
import com.adias.mytowereasy.util.ExcelUtils;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "/api/unit")
@CrossOrigin("*")
public class UnitController {
    @Autowired
    UnitService unitService;

    @Autowired
    EbMarchandiseRepository ebMarchandiseRepository;

    private SXSSFWorkbook workbook;
    private CellStyle boldCellStyle;
    private CellStyle cellBorderStyle;
    private CellStyle headerStyle;

    void initWorkbook() {
        workbook = new SXSSFWorkbook();

        // border color
        XSSFColor borderColor = new XSSFColor(new java.awt.Color(128, 128, 128));
        short palIndex = borderColor.getIndex();

        // Cell style
        cellBorderStyle = workbook.createCellStyle();
        cellBorderStyle.setBorderBottom(BorderStyle.THIN);
        cellBorderStyle.setBorderTop(BorderStyle.THIN);
        cellBorderStyle.setBorderRight(BorderStyle.THIN);
        cellBorderStyle.setBorderLeft(BorderStyle.THIN);

        // settings borders color
        cellBorderStyle.setBottomBorderColor(palIndex);
        cellBorderStyle.setTopBorderColor(palIndex);
        cellBorderStyle.setLeftBorderColor(palIndex);
        cellBorderStyle.setRightBorderColor(palIndex);

        Font fontArial = workbook.createFont();
        fontArial.setFontName(HSSFFont.FONT_ARIAL);
        fontArial.setFontHeightInPoints((short) 10);
        cellBorderStyle.setFont(fontArial);

        // Bold Cell style
        boldCellStyle = workbook.createCellStyle();
        boldCellStyle.setBorderBottom(BorderStyle.THIN);
        boldCellStyle.setBorderTop(BorderStyle.THIN);
        boldCellStyle.setBorderRight(BorderStyle.THIN);
        boldCellStyle.setBorderLeft(BorderStyle.THIN);

        // settings borders color
        boldCellStyle.setBottomBorderColor(palIndex);
        boldCellStyle.setTopBorderColor(palIndex);
        boldCellStyle.setLeftBorderColor(palIndex);
        boldCellStyle.setRightBorderColor(palIndex);
        boldCellStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        boldCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);

        Font fontArialBold = workbook.createFont();
        fontArialBold.setFontName(HSSFFont.FONT_ARIAL);
        fontArialBold.setFontHeightInPoints((short) 10);
        fontArialBold.setColor(IndexedColors.WHITE.getIndex());
        fontArialBold.setBold(true);
        boldCellStyle.setFont(fontArialBold);

        // Cell Colord
        headerStyle = workbook.createCellStyle();
        headerStyle.setBorderBottom(BorderStyle.THIN);
        headerStyle.setBorderTop(BorderStyle.THIN);
        headerStyle.setBorderRight(BorderStyle.THIN);
        headerStyle.setBorderLeft(BorderStyle.THIN);

        // settings borders color
        headerStyle.setBottomBorderColor(palIndex);
        headerStyle.setTopBorderColor(palIndex);
        headerStyle.setLeftBorderColor(palIndex);
        headerStyle.setRightBorderColor(palIndex);
        headerStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerStyle.setFont(fontArialBold);
        headerStyle.setAlignment(HorizontalAlignment.CENTER);
    }

    @RequestMapping(value = "list-unit", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getListTree(@RequestBody(required = false) Integer idDemande) {
        return unitService.getListTree(idDemande);
    }

    @RequestMapping(value = "list-marchandise-demande", method = RequestMethod.POST, produces = "application/json")
    public List<EbMarchandise> getListEbMarchandise(@RequestBody(required = true) Integer idDemande) {
        return ebMarchandiseRepository.findAllByEbDemande_ebDemandeNum(idDemande);
    }

    @RequestMapping(value = "export-unit", method = RequestMethod.POST, produces = "application/json")
    public void getListTree(@RequestBody(required = false) SearchCriteria criteria, HttpServletResponse response)
        throws IOException {

        try {
            List<EbMarchandise> listMarchandiseExport = null;
            this.initWorkbook();
            Map<String, Integer> cfg = new LinkedHashMap<String, Integer>();

            if (TypeContainer.ALL.getKey() == criteria.getTypeContener()) {
                listMarchandiseExport = ebMarchandiseRepository
                    .findAllByEbDemande_ebDemandeNum(criteria.getEbDemandeNum());
            }
            else {
                listMarchandiseExport = ebMarchandiseRepository
                    .selectListEbMarchandiseByEbDemandeNumAndXEbTypeConteneur(
                        criteria.getEbDemandeNum(),
                        criteria.getTypeContener());
            }

            List<Map<String, String[]>> result = unitService
                .listMapForExcelUtils(listMarchandiseExport, criteria.getTypeContener());

            SXSSFSheet sheet = workbook.createSheet("extraction");
            sheet = ExcelUtils
                .loadSheetWithData(
                    sheet,
                    result,
                    cfg,
                    null,
                    cellBorderStyle,
                    boldCellStyle,
                    null,
                    false,
                    Optional.empty(),
                    Optional.empty());

            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment; filename=data.xlsx");
            workbook.write(response.getOutputStream());
            response.getOutputStream().flush();
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
            workbook.close();
            throw e;
        }

    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "export-consolidation", method = RequestMethod.POST, produces = "application/json")
    public void exportConsolider(@RequestBody(required = false) Integer idDemande, HttpServletResponse response)
        throws IOException {

        try {
            this.initWorkbook();
            Map<String, Integer> cfg = new LinkedHashMap<String, Integer>();

            List<EbMarchandise> listMarchandise = ebMarchandiseRepository.findAllByEbDemande_ebDemandeNum(idDemande);

            Map<String, Object> dataForExtraction = ConsolidateExtraction
                .getListEbMarchandiseConsolidate(listMarchandise);

            List<EbMarchandiseForExtraction> listMarchandiseExport = (List<EbMarchandiseForExtraction>) dataForExtraction
                .get("listMarchandiseExport");

            List<Map<String, String[]>> result = unitService.listMapForExtractionConsolider(listMarchandiseExport);

            /* Configuration des tops headers */
            List<ExcelToHeaderConfig> headerCfg = new ArrayList<ExcelToHeaderConfig>();
            headerCfg.add(new ExcelToHeaderConfig(1, "", headerStyle));
            headerCfg.add(new ExcelToHeaderConfig(6, "ARTICLE", headerStyle));
            headerCfg.add(new ExcelToHeaderConfig(4, "PARCEL", headerStyle));
            headerCfg.add(new ExcelToHeaderConfig(4, "PALETTE", headerStyle));
            headerCfg.add(new ExcelToHeaderConfig(4, "CONTAINER", headerStyle));

            SXSSFSheet sheet = workbook.createSheet("extraction");
            sheet = ExcelUtils
                .loadSheetWithData(
                    sheet,
                    result,
                    cfg,
                    headerCfg,
                    cellBorderStyle,
                    boldCellStyle,
                    null,
                    true,
                    Optional.empty(),
                    Optional.empty());

            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment; filename=data.xlsx");
            workbook.write(response.getOutputStream());
            response.getOutputStream().flush();
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
            workbook.close();
            throw e;
        }

    }
}
