/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.dto.ExUserModuleDTO;
import com.adias.mytowereasy.model.EbDelegation;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbFavori;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.EmailConfig;
import com.adias.mytowereasy.model.Enumeration.Role;
import com.adias.mytowereasy.model.Enumeration.UserStatus;
import com.adias.mytowereasy.model.InscriptionWrapper;
import com.adias.mytowereasy.repository.EbFavoriRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.security.TokenAuthenticationService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "api/account")
@CrossOrigin("*")
public class AccountController extends SuperControler {
    @Value("${mytower.login-page.url}")
    private String loginPage;

    @Autowired
    EbUserRepository ebUserRepository;

    @Autowired
    EbFavoriRepository ebFavoriRepository;

    @PostMapping(value = "/get-login-page-url")
    @ResponseBody
    public HashMap<String, String> getLoginPageUrl() {
        String login = !loginPage.isEmpty() ? loginPage : "";
        HashMap<String, String> obj = new HashMap<>();
        obj.put("login", login);
        return obj;
    }

    @RequestMapping(value = "get-user-access-rights", method = RequestMethod.GET, produces = "application/json")
    public List<ExUserModuleDTO> getUserAccessRights(@RequestParam(value = "eb_user_num") Integer ebUserNum) {
        Set<ExUserModuleDTO> rights = userService.getUserAccessRights(ebUserNum);
        List<ExUserModuleDTO> result = new ArrayList<>(rights);
        return result;
    }

    @PostMapping(value = "/get-user")
    @ResponseBody
    public EbUser selectEbUser(@RequestBody SearchCriteria criterias) {
        EbUser ebUser = userService.selectEbUser(criterias);
        return ebUser;
    }

    @PostMapping(value = "/create-delegation")
    public EbDelegation createDelegation(@RequestBody EbDelegation ebDelegation) {
        return userService.createDelegation(ebDelegation);
    }

    @PostMapping(value = "/deleteDelegation")
    public Integer deleteDelegation(@RequestBody Integer ebDelegationNum) {
        return userService.deleteDelegation(ebDelegationNum);
    }

    @PostMapping(value = "/get-delegation")
    public EbDelegation getDelegation(@RequestBody SearchCriteria criteria) {
        return userService.getDelegation(criteria);
    }

    @PostMapping(value = "/updateEtablissement")
    public EbEtablissement updateEbEtablissement(@RequestBody EbEtablissement ebEtablissement) {
        return userService.updateEbEtablissement(ebEtablissement);
    }

    @PostMapping(value = "/updateUser")
    public EbUser updateEbUser(@RequestBody EbUser ebUser) {
        return userService.updateEbUser(ebUser);
    }

    @PostMapping(value = "/updateUserLanguage")
    public String updateEbUserLanguage(@RequestBody EbUser connectedUser) {
        userService.updateEbUserLanguage(connectedUser);
        String result = null;
        EbUser userFromDb = ebUserRepository.findOneByEbUserNum(connectedUser.getEbUserNum());

        if (userFromDb != null && userFromDb.getLanguage().equals(connectedUser.getLanguage())
            && connectedUser.getRealUserNum() != null) {
            userFromDb.setRealUserCompNom(connectedUser.getRealUserCompNom());
            userFromDb.setRealUserCompNum(connectedUser.getRealUserCompNum());
            userFromDb.setRealUserEmail(connectedUser.getRealUserEmail());
            userFromDb.setRealUserEtabNom(connectedUser.getRealUserEtabNom());
            userFromDb.setRealUserEtabNum(connectedUser.getRealUserEtabNum());
            userFromDb.setRealUserNom(connectedUser.getRealUserNom());
            userFromDb.setRealUserNum(connectedUser.getRealUserNum());
            userFromDb.setRealUserPassword(connectedUser.getRealUserPassword());
            userFromDb.setRealUserPrenom(connectedUser.getRealUserPrenom());
            result = TokenAuthenticationService.getTokenByUser(userFromDb);
        }
        else if (userFromDb != null && userFromDb.getLanguage().equals(connectedUser.getLanguage())) {
            result = TokenAuthenticationService.getTokenByUser(userFromDb);
        }

        return result;
    }

    @PostMapping(value = "/updateStatusUser")
    public EbUser updateStatusEbUser(@RequestBody EbUser ebUser) {
        return userService.updateStatusEbUser(ebUser);
    }

    @PostMapping(value = "/updateAdminAndSuperAdminUser")
    public Integer updateAdminUser(@RequestBody EbUser ebUser) {
        return userService.updateAdminAndSuperAdminUser(ebUser);
    }

    @PostMapping(value = "/changeUserPassword")
    public EbUser changeUserPassword(@RequestBody EbUser ebUser) {
        return userService.changeUserPassword(ebUser);
    }

    @PostMapping(value = "/updateUserDetails")
    public EbUser updateUserDetails(@RequestBody EbUser ebUser) {
        return userService.updateUserDetails(ebUser);
    }

    @PostMapping(value = "check-user-password")
    @ResponseBody
    public Boolean checkUserPassword(@RequestBody SearchCriteria criteria) {
        return userService.matchUserPassword((criteria == null) ? new SearchCriteria() : criteria);
    }

    @GetMapping(value = "/account-settings")
    public EbUser getEbUserByEbEmail(HttpServletRequest request) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        EbUser user = null;

        if (connectedUser != null) {
            user = userService.getEbUserByEmail(connectedUser.getEmail());
            user.setRoleLibelle(Role.getByCode(user.getRole()).getLibelle());
        }

        return user;
    }

    @PostMapping(value = "/reset-password", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Integer> resetPassword(@RequestBody String email) {
        EbUser ebUser = userService.getEbUserByEmail(email);

        if (ebUser != null) {

            if (ebUser.getStatus().equals(UserStatus.ACTIF.getCode())) {
                userService.resetPassword(ebUser);

                return new ResponseEntity<>(UserStatus.ACTIF.getCode(), HttpStatus.OK);
            }
            else if (ebUser.getStatus().equals(UserStatus.NON_ACTIF.getCode())) {
                return new ResponseEntity<>(UserStatus.NON_ACTIF.getCode(), HttpStatus.OK);
            }
            else if (ebUser.getStatus().equals(UserStatus.DISACTIVE.getCode())) {
                return new ResponseEntity<>(UserStatus.DISACTIVE.getCode(), HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(0, HttpStatus.OK);
            }

        }
        else {
            return new ResponseEntity<>(-1, HttpStatus.OK);
        }

    }

    @PostMapping(value = "/change-password-token", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Boolean> changePasswordWithResetToken(@RequestBody EbUser user) {

        if (user == null || user.getResetToken() == null || user.getPassword() == null) {
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }

        String token = user.getResetToken();
        EbUser ebUser = userService.getEbUserByResetToken(token);

        if (ebUser != null) {
            ebUser.setResetToken(null);

            // ebUserRepository.save(ebUser);

            ebUser.setPassword(user.getPassword());

            userService.updateEbUserPasswordAndResetToken(ebUser);

            return new ResponseEntity<>(true, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(false, HttpStatus.NO_CONTENT);
        }

    }

    @PostMapping(value = "/invitation")
    public void sendFriendInvitationMails(@RequestBody InscriptionWrapper inscriptionWrapper) {
        EbUser ebUser = new EbUser();
        emailService
            .sendEmailInvitationFriend(
                ebUser,
                inscriptionWrapper.getFriendInvitationEmails(),
                EmailConfig.INVITE_FRIEND);
        emailService
            .sendEmailInvitationCollegue(
                ebUser,
                inscriptionWrapper.getCollegueInvitationEmails(),
                EmailConfig.INVITE_COLLEGUE);
    }

    @PostMapping(value = "/update-password-user")
    @ResponseBody
    public Boolean updatePasswordEbUser(@RequestBody EbUser ebUser) {
        return userService.updatePasswordEbUser(ebUser);
    }

    @PostMapping(value = "/add-favori")
    public EbFavori addFavori(@RequestBody EbFavori ebFavori) {
        return ebFavoriRepository.save(ebFavori);
    }

    @PostMapping(value = "/delete-favori")
    public Boolean deleteFavori(@RequestBody EbFavori ebFavori) {
        ebFavoriRepository.delete(ebFavori);
        return true;
    }

    @PostMapping(value = "/delete-favori-by-id")
    public Boolean deleteFavoriById(@RequestBody Integer ebFavoriNum) {
        ebFavoriRepository.deleteById(ebFavoriNum);
        return true;
    }

    @GetMapping(value = "/list-favori")
    public List<EbFavori> listFavoris(@RequestParam(required = false, value = "module") Integer module) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        if (connectedUser != null) {
            if (module != null) return ebFavoriRepository
                .findAllByModuleAndUser_ebUserNum(module, connectedUser.getEbUserNum());

            return ebFavoriRepository.findAllByUser_ebUserNum(connectedUser.getEbUserNum());
        }

        return new ArrayList<EbFavori>();
    }

    @GetMapping(value = "/get-favori")
    public EbFavori getFavori(
        @RequestParam(required = false, value = "module") Integer module,
        @RequestParam(required = false, value = "idObject") Integer idObject) {
        return ebFavoriRepository
            .findOneByModuleAndIdObjectAndUser_ebUserNum(
                module,
                idObject,
                connectedUserService.getCurrentUser().getEbUserNum());
    }

    @PostMapping(value = "/refuse-user")
    public EbUser deleteUser(@RequestBody EbUser user) {
        userService.deleteUser(user);
        return user;
    }

    @PostMapping(value = "/save-config-params-mail")
    public EbUser configParams(@RequestBody EbUser user) {
        userService.saveConfigEmail(user);
        return user;
    }

    @GetMapping(value = "/can-edit-user/{ebUserNum}")
    public boolean canEditUser(@PathVariable("ebUserNum") Integer ebUserNum) {
        return this.userService.canEditUser(ebUserNum);
    }

    @GetMapping(value = "/can-edit-profil/{ebUserProfileNum}")
    public boolean canEditProfil(@PathVariable("ebUserProfileNum") Integer ebUserProfileNum) {
        return this.userService.canEditProfil(ebUserProfileNum);
    }

    @PostMapping(value = "find-user-ebUserNum")
    public EbUser getUserByUserNum(@RequestBody Integer ebUserNum) {
        return ebUserRepository.findOneByEbUserNum(ebUserNum);
    }

    @GetMapping(value = "get-token/{ebUserNum}")
    public String getUserToken(@PathVariable("ebUserNum") Integer ebUserNum) {
        EbUser user = ebUserRepository.findOneByEbUserNum(ebUserNum);

        if (user != null) {
            return TokenAuthenticationService.getTokenByUser(user);
        }

        return null;
    }
}
