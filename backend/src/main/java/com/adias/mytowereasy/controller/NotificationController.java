package com.adias.mytowereasy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.dto.NotificationTypeCount;
import com.adias.mytowereasy.model.EbNotification;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.NotificationService;


@RestController
@RequestMapping(value = "api/notification")
@CrossOrigin("*")
public class NotificationController {
    @Autowired
    NotificationService notificationService;
    @Autowired
    ConnectedUserService connectedUserService;

    @GetMapping(value = "/getAllNotification")
    public List<EbNotification> getAllNotification(
        @RequestParam("userNum") Integer userNum,
        @RequestParam("typeNotification") Integer typeNotification,
        @RequestParam("pageNumber") Integer pageNumber) {
        return notificationService.getAllNotification(userNum, typeNotification, pageNumber);
    }

    @GetMapping(value = "/count")
    public NotificationTypeCount count(@RequestParam("userNum") Integer userNum) {
        return notificationService.count(userNum);
    }

    @RequestMapping(value = "/deleteNotification", method = RequestMethod.POST)
    public void deleteNotification(@RequestBody Long ebNotificationNum) {
        notificationService.deleteNotification(ebNotificationNum);
    }

    @PostMapping("update-nbr-notif")
    public void updateNbrNotif(
        @RequestParam("ebUserNum") Integer ebUserNum,
        @RequestParam("notifType") Integer notifType,
        @RequestParam("nbr") Integer nbr) {
        this.notificationService.updateNotifNbrForUser(ebUserNum, notifType, nbr);
    }
}
