/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adias.mytowereasy.MTBuildConfig;
import com.adias.mytowereasy.model.AboutDev;
import com.adias.mytowereasy.service.ServiceConfiguration;


@Controller
@RequestMapping("/about")
public class AboutController {
    @Autowired
    ServiceConfiguration serviceConfiguration;

    @RequestMapping(value = "/dev-json")
    public @ResponseBody AboutDev technicalInfosModel() {
        AboutDev model = new AboutDev();

        model.setGitRevision(MTBuildConfig.GIT_REVISION);
        model.setGitMessage(MTBuildConfig.GIT_MESSAGE);
        model.setActiveProfiles(MTBuildConfig.ACTIVE_PROFILES);
        model.setBuildDate(MTBuildConfig.BUILD_INSTANT);
        model.setVersion(MTBuildConfig.VERSION);

        return model;
    }

    @RequestMapping(value = "/dev")
    public ModelAndView technicalInfos() {
        AboutDev model = technicalInfosModel();

        ModelAndView mv = new ModelAndView("web/about-dev");
        mv.addObject("about", model);

        return mv;
    }
}
