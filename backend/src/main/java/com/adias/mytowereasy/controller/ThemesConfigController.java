package com.adias.mytowereasy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.model.EbUserThemes;
import com.adias.mytowereasy.service.ThemesConfigService;


@RestController
@RequestMapping(value = "api/themes-config")
@CrossOrigin("*")
public class ThemesConfigController {
    @Autowired
    ThemesConfigService themesConfigService;

    @RequestMapping(value = "save-themes-config", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody boolean SaveThmesConfig(@RequestBody(required = true) EbUserThemes themesConfig) {
        return themesConfigService.saveThemesConfig(themesConfig);
    }

    @RequestMapping(value = "get-compagnie-theme", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody EbUserThemes getCompagnieTheme(@RequestBody(required = true) Integer ebCompagnieNum) {
        EbUserThemes ebThemesConfig = themesConfigService.getCompagnieTheme(ebCompagnieNum);
        return ebThemesConfig;
    }
}
