package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.dto.EbTypeDocumentsDTO;
import com.adias.mytowereasy.model.EbTypeDocuments;
import com.adias.mytowereasy.service.TypeDocumentService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController()
@RequestMapping("api/typeDocument")
public class TypeDocumentController {
    @Autowired
    TypeDocumentService typeDocumentService;

    @RequestMapping(value = "getListTypeDoc", method = RequestMethod.POST, produces = "application/json")
    public List<EbTypeDocuments> getListTypeDoc(@RequestBody(required = false) SearchCriteria criteria) throws Exception

    {
        return typeDocumentService.getListTypeDoc(criteria);
    }

    @RequestMapping(value = "getListTypeDocDTO", method = RequestMethod.POST, produces = "application/json")
    public List<EbTypeDocumentsDTO> getListTypeDocDTO(@RequestBody(required = false) SearchCriteria criteria)
        throws Exception

    {
        return typeDocumentService.convertEntitiesToDto(typeDocumentService.getListTypeDoc(criteria));
    }

    @RequestMapping(value = "getListTypeDocTable", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object> getListTypeDocTable(@RequestBody(required = false) SearchCriteria criteria)
        throws Exception

    {
        List<EbTypeDocuments> dtos = typeDocumentService.getListTypeDoc(criteria);
        Long count = typeDocumentService.countListTypeDocument(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", dtos);
        return result;
    }

    @PostMapping(value = "/create")
    public EbTypeDocuments create(@RequestBody EbTypeDocuments ebTypeDocuments) {
        return typeDocumentService.saveTypeDocument(ebTypeDocuments);
    }

    @PostMapping(value = "/modifier")
    public Integer modifier(@RequestBody EbTypeDocuments ebTypeDocuments) {
        typeDocumentService.updateTypeDocument(ebTypeDocuments);
        return 1;
    }

    @PostMapping(value = "/editActivated")
    public Integer editActivated(@RequestBody SearchCriteria criteria) {
        typeDocumentService.updateActivated(criteria);
        return 1;
    }

    @PostMapping(value = "/supprimer")
    public void supprimer(@RequestBody Integer ebTypeDocuments) {
        System.out.println(ebTypeDocuments);
        typeDocumentService.deleteTypeDocument(ebTypeDocuments);
    }

    @GetMapping(value = "/get-list-doc-by-module-num")
    public List<EbTypeDocuments>
        getListTypeDocByModuleNum(@RequestParam(required = false, value = "moduleNum") Integer moduleNum)
            throws Exception {
        List<EbTypeDocuments> listTypeDoc = typeDocumentService.getTypeDocsByEbTypeDocumentsModule(moduleNum, null);

        return listTypeDoc;
    }

    @GetMapping(value = "/get-list-doc-final")
    public List<EbTypeDocuments> getDemandeListTypeDocumentFinal(
        @RequestParam(required = false, value = "moduleNum") Integer moduleNum,
        @RequestParam(required = false, value = "ebDemandeNum") Integer ebDemandeNum,
        @RequestParam(required = false, value = "ebCompagnieNum") Integer ebCompagnieNum)
        throws Exception {
        List<EbTypeDocuments> listTypeDoc = typeDocumentService
            .getListTypeDocumentFinal(moduleNum, ebDemandeNum, ebCompagnieNum);

        return listTypeDoc;
    }

    @GetMapping(value = "/get-invoice-list-doc-final")
    public List<EbTypeDocuments> getInvoiceListTypeDocumentFinal(
        @RequestParam(required = false, value = "moduleNum") Integer moduleNum,
        @RequestParam(required = false, value = "ebInvoiceNum") Integer ebInvoiceNum,
        @RequestParam(required = false, value = "ebCompagnieNum") Integer ebCompagnieNum)
        throws Exception {
        List<EbTypeDocuments> listTypeDoc = typeDocumentService
            .getInvoiceListTypeDocument(moduleNum, ebCompagnieNum, ebInvoiceNum);

        return listTypeDoc;
    }
}
