/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;
import com.adias.mytowereasy.api.service.PlanTransportApiService;
import com.adias.mytowereasy.api.service.TransportApiService;
import com.adias.mytowereasy.api.wso.PlanTransportWSO;
import com.adias.mytowereasy.dto.ChatDTO;
import com.adias.mytowereasy.dto.EbDemandeTransportDTO;
import com.adias.mytowereasy.dto.mapper.EbChatMapper;
import com.adias.mytowereasy.kafka.service.ProcessGroupProducerService;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.Enumeration.Module;
import com.adias.mytowereasy.model.Enumeration.StatutCarrier;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.PricingBookingEnumeration.CustomsControlStatus;
import com.adias.mytowereasy.model.PricingBookingEnumeration.CustomsDuty;
import com.adias.mytowereasy.model.PricingBookingEnumeration.CustomsOtherTaxes;
import com.adias.mytowereasy.model.PricingBookingEnumeration.CustomsVat;
import com.adias.mytowereasy.model.enums.Modules;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.mtc.service.ValorizationService;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EbFavoriRepository;
import com.adias.mytowereasy.repository.ExEbDemandeTransporteurRepository;
import com.adias.mytowereasy.service.CustomFieldService;
import com.adias.mytowereasy.service.ListStatiqueService;
import com.adias.mytowereasy.service.email.EmailServicePricingBooking;
import com.adias.mytowereasy.util.EmptyJsonResponse;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteria.OrderCriterias;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;
import com.adias.mytowereasy.utils.search.SearchCriteriaTrackTrace;


@RestController
@RequestMapping(value = "api/pricing")
@CrossOrigin("*")
public class PricingController extends SuperControler {
    @Autowired
    EbFavoriRepository ebFavoriRepository;
    @Autowired
    TransportApiService transportApiService;
    @Autowired
    PlanTransportApiService planTransportApiService;

    @Autowired
    EbDemandeRepository ebDemandeRepository;
    @Autowired
    ListStatiqueService listStatiqueService;
    @Autowired
    EmailServicePricingBooking emailServicePricingBooking;

    @Autowired

    ExEbDemandeTransporteurRepository exEbDemandeTransporteurRepository;

    @Autowired
    ValorizationService valorizationService;

    @Autowired
    ProcessGroupProducerService processGroupProducerService;

    @Autowired
    CustomFieldService customFieldService;

    @Autowired
    private EbChatMapper chatMapper;

    @PostMapping(value = "/listInitialDemande")
    public List<EbDemande> getListInitialDemande(@RequestBody SearchCriteriaPricingBooking criterias) {
        return pricingService.getListEbDemandeInit(criterias);
    }

    @PostMapping(value = "/list")
    public HashMap<String, Object>
        getListDemande(@RequestBody SearchCriteriaPricingBooking params) throws JsonProcessingException, Exception {

        if (params.getSearchInput() != null && params.getSearchInput().length() > 0) {
            params.setSearchInput(params.getSearchInput(), customFieldService.getCustomFields(params.getEbUserNum()));
        }

        if (params.getOrderedColumn() != null) {
            Map<OrderCriterias, String> map = new HashMap<OrderCriterias, String>();
            List<Map<OrderCriterias, String>> listMap = new ArrayList<Map<OrderCriterias, String>>();
            map.put(OrderCriterias.columnName, params.getOrderedColumn());
            map.put(OrderCriterias.dir, params.getAscendant() ? "asc" : "desc");
            listMap.add(map);
            params.setOrder(listMap);
        }

        if (params.getSearchterm() != null && !params.getSearchterm().trim().isEmpty()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", params.getSearchterm());
            params.setSearch(map);
        }

        List<EbDemande> demandeList = pricingService.getListEbDemande(params);
        int counter = pricingService.getCountListEbDemande(params);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", counter);
        result.put("data", demandeList);
        return result;
    }

    @PostMapping(value = "/list-count")
    public Integer
        getListCount(@RequestBody SearchCriteriaPricingBooking params) throws JsonProcessingException, Exception {

        if (StringUtils.isNotBlank(params.getSearchInput())) {
            params.setSearchInput(params.getSearchInput(), customFieldService.getCustomFields(params.getEbUserNum()));
        }

        return pricingService.getCountListEbDemande(params);
    }

    @RequestMapping(value = "/deleteDemande", method = RequestMethod.POST)
    public Integer deleteDemande(@RequestBody Integer[] listEbDemandeNum) {
        return pricingService.deleteEbDemande(listEbDemandeNum);
    }

    @RequestMapping(value = "/updatePropRdv", method = RequestMethod.POST)
    public EbDemande updatePropRdv(@RequestBody EbDemande[] ebDemandes) {
        return pricingService.updatePropRdv(ebDemandes[0]);
    }

    /*
     * @RequestMapping(value = "/saveStateTable", method = RequestMethod.POST)
     * public @ResponseBody String saveStateTable(
     * @RequestParam(required = true, value = "state") String state,
     * @RequestParam(required = true, value = "ebUserNum") Integer ebUserNum,
     * @RequestParam(required = true, value = "datatableCompId") Integer
     * datatableCompId
     * )
     * {
     * pricingService.saveState(state, ebUserNum, datatableCompId);
     * return "done";
     * }
     */

    @PostMapping(value = "add")
    public EbDemande addEbDemande(@Valid @RequestBody EbDemande ebDemande) {
        return pricingService.addEbDemande(ebDemande);
    }

    @PostMapping(value = "save-all-informations")
		public void saveAllInformations(@Valid @RequestBody EbDemande ebDemande)
		{
			pricingService.saveInformationsBasedOnTrNature(ebDemande, ebDemande.getModule());
    }

    @SuppressWarnings("unused")
    @PostMapping(value = "confirm-pending-demande")
    public EbDemande confirmPendingDemande(@Valid @RequestBody EbDemande ebDemande) {
        PlanTransportWSO transport = planTransportApiService.convertEbDemandeToTransport(ebDemande);
        planTransportApiService.getNextPickUpDateOfTransportWSO(transport);

        ebDemande.setDateOfGoodsAvailability(transport.getNextPickUpDate());

        if (ebDemande.getxEbSchemaPsl() != null && transport.getNextPickUpDate() != null) {
            EbTtSchemaPsl ebTtSchemaPsl = ebDemande.getxEbSchemaPsl();
            ebTtSchemaPsl.getListPsl().get(0).setExpectedDate(transport.getNextPickUpDate());
        }

        if (ebDemande != null) {
            List<ExEbDemandeTransporteur> listQuote = exEbDemandeTransporteurRepository
                .findAllByStatusGreaterThanEqualAndXEbDemande_ebDemandeNum(
                    StatutCarrier.FIN.getCode(),
                    ebDemande.getEbDemandeNum());
            if (!listQuote.isEmpty()) ebDemande.set_exEbDemandeTransporteurFinal(listQuote.get(0));
            emailServicePricingBooking.sendEmailConfirmPendingTransport(ebDemande);
        }

        return pricingService.confirmPendingDemande(ebDemande);
    }

    @GetMapping(value = "/generateAssociateObjects")
    public boolean generateAssociateObjects(@RequestParam Integer ebDemandeNum) {
        return pricingService.generateAssociatedPricingObjects(ebDemandeNum);
    }

    @GetMapping(value = "/party")
    public ResponseEntity<Object> getEbPartyByCompany(@RequestParam String company) {
        List<EbParty> ebParties = pricingService.getEbPartyByCompany(company);

        if (ebParties == null) {
            return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(ebParties, HttpStatus.OK);
        }

    }

    @GetMapping(value = "/cities")
    public ResponseEntity<Object> getEbPartyByCity() {
        List<EbParty> ebParties = pricingService.getEbPartyByCity();

        if (ebParties == null) {
            return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(ebParties, HttpStatus.OK);
        }

    }

    @RequestMapping(value = "/numAwbBol", method = RequestMethod.GET)
    public List<String> searchNumAwbBol(@RequestParam(required = false, value = "term") String term) {
        return pricingService.selectListNumAwbBolFromDemandes(term);
    }

    @GetMapping(value = "/parties")
    public ResponseEntity<Object> getEbParties() {
        List<EbParty> ebParties = pricingService.getAllEbParty();

        if (ebParties == null) {
            return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(ebParties, HttpStatus.OK);
        }

    }

    @GetMapping(value = "/adresse")
    public ResponseEntity<Object> getEbAdresseyByCompany(@RequestParam String company) {
        SearchCriteria criteria = new SearchCriteria();
        criteria.setCompanyName(company);
        List<EbAdresse> ebAdresses = pricingService.getEbAdresseByCriteria(criteria);

        if (ebAdresses == null) {
            return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(ebAdresses, HttpStatus.OK);
        }

    }

    @RequestMapping(value = "/adresseAsParty", method = {
        RequestMethod.GET, RequestMethod.POST
    })
    public ResponseEntity<Object> getEbAdresseByGlobalsFieldsAsParty(
        @RequestParam String term,
        @RequestParam(required = false) Integer ebCompagnieNum,
        @RequestParam(required = false) Integer ebEtablissementNum,
        @RequestParam(required = false) String eligibilities,
        @RequestParam(required = false) Integer ebUserNum,
        @RequestParam(required = false) Integer ecCountryNum,
        @RequestParam(required = false) String city,
        @RequestParam(required = false) String zipCode,
        @RequestParam(required = false) Integer ebZoneNum,
        @RequestParam(required = false) Integer size,
        @RequestParam(required = false) Integer page) {
        HashMap<String, Object> resultat = pricingService
            .getEbAdresseByGlobalsFieldsAsEbParty(
                term,
                ebCompagnieNum,
                ebEtablissementNum,
                eligibilities,
                ebUserNum,
                ecCountryNum,
                city,
                zipCode,
                ebZoneNum,
                size,
                page);

        if (resultat == null) {
            return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(resultat, HttpStatus.OK);
        }

    }

    @GetMapping(value = "/adresses")
    public ResponseEntity<Object> getEbAdresses() {
        List<EbAdresse> ebAdresses = pricingService.getAllEbAdresse();

        if (ebAdresses == null) {
            return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(ebAdresses, HttpStatus.OK);
        }

    }

    @RequestMapping(
        value = "/listTransporteur",
        method = RequestMethod.POST,
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Object getTransporteurs(@RequestBody SearchCriteria searchCriteria) {
        return userService.selectListTransporteur(searchCriteria);
    }

    @RequestMapping(
        value = "/listTransporteurControlTower",
        method = RequestMethod.POST,
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Object listTransporteurControlTower(@RequestBody SearchCriteria searchCriteria) {
        return userService.listTransporteurControlTower(searchCriteria);
    }

    @RequestMapping(
        value = "/listBroker",
        method = RequestMethod.POST,
        consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Object getBrokers(@RequestBody SearchCriteria searchCriteria) {
        return userService.selectListBroker(searchCriteria);
    }

    @RequestMapping(value = "/listCategories", method = RequestMethod.GET)
    public Object getEbCategorieByEtablissement(@RequestParam Integer ebEtablissementNum) {
        List<EbCategorie> ebCategories = connectedUserService.getCurrentUserFromDB().getListCategories();

        if (ebCategories == null) {
            return new EmptyJsonResponse();
        }
        else {
            return new ResponseEntity<>(ebCategories, HttpStatus.OK);
        }

    }

    @RequestMapping(value = "/demande", method = RequestMethod.POST)
    public EbDemande getDemande(@RequestBody SearchCriteriaPricingBooking params) throws Exception {
			return pricingService.getEbDemande(params, connectedUserService.getCurrentUser());
    }

    @RequestMapping(value = "/listDemandeQuote", method = RequestMethod.POST)
    public Map<String, Object> getListDemandeQuote(@RequestBody SearchCriteriaPricingBooking params) throws Exception {
        return pricingService.getListDemandeQuote(params);
    }

    @RequestMapping(value = "/addQuotation", method = RequestMethod.POST)
    public Map<String, Object> addCotation(
        @RequestParam(value = "module") Integer moduleNum,
        @RequestParam(value = "isQuotationRequest") Boolean isQuotationRequest,
			@RequestBody ExEbDemandeTransporteur exEbDemandeTransporteur
		)
			throws InterruptedException,
			ExecutionException
		{
        return pricingService.addQuotation(exEbDemandeTransporteur, moduleNum, isQuotationRequest);
    }

    @RequestMapping(value = "/editQuotation", method = RequestMethod.POST)
    public Map<String, Object> editCotation(
        @RequestParam(required = true, value = "module") Integer moduleNum,
        @RequestBody ExEbDemandeTransporteur exEbDemandeTransporteur) {
        return pricingService.editQuotation(exEbDemandeTransporteur, moduleNum);
    }

    @RequestMapping(value = "/updateStatusQuotation", method = RequestMethod.POST)
    public Integer updateStatusQuotation(@RequestBody ExEbDemandeTransporteur demandeTransporteur) {
        return pricingService.updateStatusQuotation(demandeTransporteur, true, Enumeration.Module.PRICING.getCode());
    }

    @RequestMapping(value = "/confirmPickup", method = RequestMethod.POST)
    public Integer confirmPickup(@RequestBody EbDemande ebDemande) {
        return pricingService.confirmPickup(ebDemande);
    }

    @RequestMapping(value = "/cancelQuotation", method = RequestMethod.POST)
    public ChatDTO cancelQuotation(@RequestBody Integer ebDemandeNum) {
        return chatMapper.modelToDto(pricingService.cancelQuotation(ebDemandeNum));
    }

    @RequestMapping(value = "/deleteDocument", method = RequestMethod.POST)
    public Map<String, Object> deleteDocument(@RequestBody EbDemandeFichiersJoint doc) {
        return pricingService.deleteDocument(doc);
    }

    @RequestMapping(value = "/updateEbDemandeTransportInfos", method = RequestMethod.POST)
    public Map<String, Object>
        updateEbDemandeTransportInfos(@RequestBody EbDemande ebDemande, @RequestParam String action) {
        Map<String, Object> result = new HashMap<String, Object>();

        try {
            result = pricingService.updateEbDemandeTransportInfos(ebDemande, true, action);
        } catch (Exception e) {
        }

        return result;
    }

    @PostMapping(value = "/listPostCostCategorie", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<EbCostCategorie>> getCostCategorieByTranspMode(@RequestBody Integer modeTrans) {
        List<EbCostCategorie> costCategorie = pricingService.getCostCategorieByMode(modeTrans);
        return new ResponseEntity<>(costCategorie, HttpStatus.OK);
    }

    @GetMapping(value = "/listPostCostCategorie", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<EbCostCategorie>> getAllCostCategorie() {
        List<EbCostCategorie> costCategorie = pricingService.getAllCostCategorie();
        return new ResponseEntity<>(costCategorie, HttpStatus.OK);
    }

    @RequestMapping(value = "/getTaskProgress", method = RequestMethod.POST)
    public Map<String, Object> getTaskProgress(@RequestBody String ident) {
        return pricingService.getTaskProgress(ident, null, null, null);
    }

    @RequestMapping(value = "/deleteQuotation", method = RequestMethod.POST)
    public Integer deleteQuotation(@RequestBody Integer ebDemandeTransporteurNum) {
        return pricingService.deleteQuotation(ebDemandeTransporteurNum);
    }

    @PostMapping(value = "/updateGoodsPickup")
    public Integer updateGoodsPickup(@RequestBody EbDemande[] ebDemandes) {
        return pricingService.updateGoodsPickup(ebDemandes);
    }

    @RequestMapping(value = "/updateEbDemande", method = RequestMethod.POST)
    public Map<String, Object> updateEbDemande(@RequestBody EbDemande ebDemande) throws Exception {
            return pricingService.updateTransportRequestAndProcessPostUpdate(ebDemande, ebDemande.getTypeData(), ebDemande.getModule(),connectedUserService.getCurrentUser() );
    }

    @GetMapping(value = "/getNextEbDemandeNum")
    public Integer getNextEbDemandeNum() {
        return pricingService.getNextEbDemandeNum();
    }

    @PostMapping(value = "/updateCustomsInformation")
    public Map<String, Object> updateCustomsInformation(@RequestBody EbCustomsInformation ebCustomsInformation) {
        return pricingService.updateCustomsInformation(ebCustomsInformation);
    }

    @PostMapping(value = "/getCustomsInformation")
    public Map<String, Object> getCustomsInformation(@RequestBody Integer ebDemandeNum) {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> mapStatiques = new HashMap<String, Object>();

        EbCustomsInformation ebCustomsInformation = pricingService.getCustomsInformation(ebDemandeNum);
        map.put("ebCustomsInformation", ebCustomsInformation);

        mapStatiques.put("customsControlStatus", CustomsControlStatus.getValues());
        mapStatiques.put("customsDuty", CustomsDuty.getValues());
        mapStatiques.put("customsVat", CustomsVat.getValues());
        mapStatiques.put("customsOtherTaxes", CustomsOtherTaxes.getValues());
        map.put("mapStatiques", mapStatiques);

        return map;
    }

    @PostMapping(value = "/requestCustomsBroker")
    public Integer requestCustomsBroker(@RequestBody Integer ebDemandeNum) {
        return pricingService.requestCustomsBroker(ebDemandeNum);
    }

    @RequestMapping(value = "/get-demande", method = RequestMethod.POST)
    public EbDemande demande(@RequestBody SearchCriteriaPricingBooking params) throws Exception {
        return pricingService.demande(params);
    }

    @GetMapping(value = "/get-related-users-in-demande")
    public List<EbUser> getAllUsersIntervenantInDemande(@RequestParam("ebDemandeNum") Integer ebDemandeNum) {
        return this.pricingService.getAllUsersIntervenantInDemande(ebDemandeNum);
    }

    @PostMapping(value = "bulk-import")
    public Object bulkImport(@RequestBody String ident) {
        Map<String, Object> map = this.getTaskProgress(ident);
        Object result = false;
        List<Map<String, String>> errors = new ArrayList<Map<String, String>>();

        try {
            pricingService
                .bulkImportDemande(
                    (InputStream) map.get("file"),
                    (String) map.get("fileName"),
                    (Integer) map.get("ebUserNum"),
                    errors,
                    ident);
            if (!errors.isEmpty()) result = errors;
            else result = true;
        } catch (Exception e) {
            result = e.getStackTrace();
        }

        return result;
    }

    @PostMapping(value = "favoris-accueil")
    public HashMap<String, Object> getListFavorisAcceuil(@RequestBody SearchCriteriaPricingBooking searchCriteria) {
        EbUser connectedUser = connectedUserService.getCurrentUserFromDB(searchCriteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        List<Map<String, Object>> data = new ArrayList<Map<String, Object>>();
        List<EbFavori> listFavoris = ebFavoriRepository.findAllByUser_ebUserNum(searchCriteria.getConnectedUserNum());
        List<Integer> listIdDemandes = null, listIdTracing = null;
        List<EbDemande> listEbDemande = null;
        List<EbTtTracing> listEbTtTracing = null;

        try {
            if (listFavoris != null) listIdDemandes = listFavoris
                .stream().filter(it -> it.getModule() != Modules.TRACK.getValue()).map(it -> it.getIdObject())
                .collect(Collectors.toList());
            listIdTracing = listFavoris
                .stream().filter(it -> it.getModule() == Modules.TRACK.getValue()).map(it -> it.getIdObject())
                .collect(Collectors.toList());

            if (listIdDemandes != null && !listIdDemandes.isEmpty()) {
                searchCriteria.setListEbDemandeNum(listIdDemandes);
                searchCriteria.setEbUserNum(searchCriteria.getConnectedUserNum());
                searchCriteria.setSize(null);
                searchCriteria.setModule(Module.ORDRE_TRANSPORT.getCode());
                listEbDemande = pricingService.getListEbDemandeFavoris(searchCriteria);
                this.pricingService.setListDemandeQuote(searchCriteria, listEbDemande, false);

                for (EbDemande d: listEbDemande) {
                    Map<String, Object> map = new HashMap<String, Object>();

                    Map<String, Object> mapOrigin = new HashMap<String, Object>();
                    mapOrigin.put("city", (d.getLibelleOriginCity() != null ? d.getLibelleOriginCity() : ""));
                    mapOrigin.put("country", d.getLibelleOriginCountry());

                    Map<String, Object> mapDest = new HashMap<String, Object>();
                    mapDest.put("city", (d.getLibelleDestCity() != null ? d.getLibelleDestCity() : ""));
                    mapDest.put("country", d.getLibelleDestCountry());

                    EbFavori fv = listFavoris
                        .stream().filter(it -> it.getIdObject().equals(d.getEbDemandeNum())).findFirst().get();
                    String comp = "";

                    comp += d.getExEbDemandeTransporteurs() != null && !d.getExEbDemandeTransporteurs().isEmpty()
                        && d.getExEbDemandeTransporteurs().get(0).getxTransporteur() != null ?
                            d.getExEbDemandeTransporteurs().get(0).getxTransporteur().getNomPrenom() :
                            "";

                    map.put("id", d.getEbDemandeNum());
                    map.put("module", Module.getLibelleByCode(fv.getModule()));
                    map.put("xEcModule", fv.getModule());
                    map.put("flags", d.getListEbFlagDTO());
                    map.put("chargeur", d.getxEbCompagnie() != null ? d.getxEbCompagnie().getNom() : null);
                    map.put("transporteur", comp.isEmpty() ? '-' : comp);
                    map.put("ref", d.getRefTransport());
                    map.put("origin", mapOrigin);
                    map.put("destination", mapDest);
                    map.put("statutStr", d.getStatutStr());
                    map.put("dateModification", fv.getDateCreation());
                    map.put("modeTransport", ModeTransport.getLibelleByCode(d.getxEcModeTransport()));
                    map.put("xEcModeTransport", d.getxEcModeTransport());

                    String moduleRefUrl = Module.getRefUrlByCode(fv.getModule());
                    map.put("link", "/app/" + moduleRefUrl + "/details/" + d.getEbDemandeNum());

                    data.add(map);
                }

            }

            SearchCriteriaTrackTrace criterias = new SearchCriteriaTrackTrace();

            if (listIdTracing != null && !listIdTracing.isEmpty()) {
                criterias.setListEbTtTracingNum(listIdTracing);
                listEbTtTracing = tracktraceService.getListEbTrackTrace(criterias);

                for (EbTtTracing tracing: listEbTtTracing) {
                    Map<String, Object> map = new HashMap<String, Object>();

                    Map<String, Object> mapOrigin = new HashMap<String, Object>();
                    mapOrigin
                        .put("city", (tracing.getLibelleOriginCity() != null ? tracing.getLibelleOriginCity() : ""));
                    mapOrigin.put("country", tracing.getxEcCountryOrigin().getLibelle());

                    Map<String, Object> mapDest = new HashMap<String, Object>();
                    mapDest.put("city", (tracing.getLibelleDestCity() != null ? tracing.getLibelleDestCity() : ""));
                    mapDest.put("country", tracing.getxEcCountryDestination().getLibelle());

                    EbFavori fv = listFavoris
                        .stream().filter(it -> it.getIdObject().equals(tracing.getEbTtTracingNum())).findFirst().get();
                    String comp = "";

                    comp += tracing.getxEbDemande().getExEbDemandeTransporteurs() != null
                        && !tracing.getxEbDemande().getExEbDemandeTransporteurs().isEmpty()
                        && tracing.getxEbDemande().getExEbDemandeTransporteurs().get(0).getxTransporteur() != null ?
                            tracing
                                .getxEbDemande().getExEbDemandeTransporteurs().get(0).getxTransporteur()
                                .getNomPrenom() :
                            "";

                    map.put("id", tracing.getEbTtTracingNum());
                    map.put("module", Module.getLibelleByCode(fv.getModule()));
                    map.put("xEcModule", fv.getModule());
                    map.put("flags", tracing.getListEbFlagDTO());
                    map
                        .put(
                            "chargeur",
                            tracing.getxEbDemande().getxEbCompagnie() != null ?
                                tracing.getxEbDemande().getxEbCompagnie().getNom() :
                                null);
                    map.put("transporteur", comp.isEmpty() ? '-' : comp);
                    map.put("ref", tracing.getRefTransport());
                    map.put("origin", mapOrigin);
                    map.put("destination", mapDest);
                    Integer code = tracing.getxEbDemande().getxEcStatut();
                    map
                        .put(
                            "statutStr",
                            tracing.getLibellePslCourant() != null ?
                                StatutDemande.getByCode(code) + "_" + tracing.getLibellePslCourant() :
                                StatutDemande.WPU.getLibelle());

                    map.put("dateModification", fv.getDateCreation());
                    map.put("modeTransport", ModeTransport.getLibelleByCode(tracing.getxEcModeTransport()));
                    map.put("xEcModeTransport", tracing.getxEcModeTransport());

                    String moduleRefUrl = Module.getRefUrlByCode(fv.getModule());
                    map.put("link", "/app/" + moduleRefUrl + "/details/" + tracing.getEbTtTracingNum());

                    data.add(map);
                }

            }

            result.put("count", data.size());
            result.put("data", data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @PostMapping(value = "list-demande-transport")
    public HashMap<String, Object> getListDemandeTransport(@RequestBody SearchCriteriaPricingBooking searchCriteria) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        List<EbDemandeTransportDTO> data = new ArrayList<EbDemandeTransportDTO>();

        try {
            int i = 0, n = 20;

            for (i = 0; i < n; i++) {
                EbDemandeTransportDTO dto = new EbDemandeTransportDTO();

                dto.setEbDemandeNum((i + 1));
                dto.setRefTransport("EXN-COOO" + (i + 1));
                dto.setxEcTypeDemande((i + 1) % 2);
                dto.setxEcModeTransport((i + 1) % 5);
                dto.setxEcIncoterm((i + 1) % 11);
                dto.setLibelleOriginCountry(i % 4 == 0 ? "Paris" : "Londres");
                dto.setLibelleDestCountry(i % 3 == 0 ? "Londres" : "Paris");
                dto.setTransporteur("Carrier " + (i + 1) % 3);
                dto.setEtabTransporteur("CARRIER F. EXN");
                dto.setChargeur("Frédéric FILLAIRE");
                dto.setEtabChargeur("EXN.");
                dto.setxEcStatut(i % 11 == 0 ? 11 : i % 5 == 0 ? 13 : 3);

                data.add(dto);
            }

            result.put("count", n);
            result.put("data", data);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @GetMapping("/saveFlags")
    public EbDemande saveFlag(@RequestParam Integer ebDemandeNum, @RequestParam String newFlags) {
        return pricingService.saveFlags(ebDemandeNum, newFlags);
    }

    @RequestMapping(value = "/confirmeAskFtrRes", method = RequestMethod.GET)
    public ChatDTO confirmeAskFortrResponsibility(
        @RequestParam Integer ebDemandeNum,
        @RequestParam Integer moduleNum,
        @RequestParam String refTransport,
        @RequestParam String action) {
        return chatMapper
            .modelToDto(pricingService.confirmeAskFortrResponsibility(ebDemandeNum, moduleNum, refTransport, action));
    }

    @RequestMapping(value = "/confirmeProvideTransport", method = RequestMethod.GET)
    public ChatDTO confirmeProvideTransport(
        @RequestParam Integer ebDemandeNum,
        @RequestParam Integer moduleNum,
        @RequestParam String refTransport,
        @RequestParam String action) {
        return chatMapper
            .modelToDto(pricingService.confirmeProvideTransport(ebDemandeNum, moduleNum, refTransport, action));
    }

    @RequestMapping(value = "/acknowledgeTdc")
    public ChatDTO acknowledgeTdc(@RequestParam Integer ebDemandeNum) {
        return chatMapper.modelToDto(pricingService.acknowledgeTdc(ebDemandeNum));
    }

    @PostMapping(value = "update-transpot-plan")
    public ExEbDemandeTransporteur updateEbpartyZone(
        @RequestParam("ebDemandeNum") Integer ebDemandeNum,
        @RequestParam("ebPartyNum") Integer ebPartyNum,
        @RequestParam("oldZoneLabel") String oldZoneLabel,
        @RequestParam("zone") Integer ebZoneNum,
        @RequestParam("refPlan") String refPlan,
        @RequestParam("calculatedPrice") BigDecimal calculatedPrice)
        throws Exception {
        return this.pricingService
            .updateTransportPlan(ebDemandeNum, ebPartyNum, oldZoneLabel, ebZoneNum, refPlan, calculatedPrice);
    }

    @RequestMapping(value = "/fields-by-term", method = {
        RequestMethod.GET
    })
    public ResponseEntity<Object> getFieldsByCompagnieNumAndTerm(
        @RequestParam String term,
        @RequestParam(required = false) Integer ebCompagnieNum) {
        Set<String> listValue = pricingService.getFieldsByCompagnieNumAndTerm(term, ebCompagnieNum);

        if (listValue == null) {
            return new ResponseEntity<>(new EmptyJsonResponse(), HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(listValue, HttpStatus.OK);
        }

    }

    @PostMapping("/valorisation-unitaire")
    public EbDemande unitValuation(@RequestBody EbDemande demande) {
        return pricingService.unitValuation(demande);
    }

    @PostMapping(value = "save-additional-cost")
    public EbDemande saveAdditionalCost(
        @RequestBody List<EbCost> listAdditionalCost,
        @RequestParam("ebDemandeNum") Integer ebDemandeNum)
        throws Exception {
        return pricingService.updatelistAdditionalCostInEbDemandeByEbDemandeNum(ebDemandeNum, listAdditionalCost);
    }

    @PostMapping(value = "/listIncotermCity")
    public List<String> getListIncotermCity(@RequestBody Integer ebCompagnieNum) throws Exception {
        return pricingService.getListIncotermCity(ebCompagnieNum);
    }

    @PostMapping(value = "/listQrGroupe")
    public List<EbQrGroupe> getListQrGroupe(
        SearchCriteriaPricingBooking criteria,
        @RequestParam(required = false, value = "active") boolean active)
        throws JsonProcessingException,
        Exception {
        return pricingService.getListQrGroupe(criteria, active);
    }

    @RequestMapping(value = "/update-list-marchandise", method = RequestMethod.POST)
    public Map<String, Object> updateListMarchandise(
        @RequestBody List<EbMarchandise> listMarchandise,
        @RequestParam Integer ebDemandeNum,
        @RequestParam Double totalTaxableWeight,
        @RequestParam Double totalWeight,
        @RequestParam Double totalVolume)
        throws Exception {
        Map<String, Object> ebDemandeWithNewResult = pricingService
            .updateListMarchandise(listMarchandise, ebDemandeNum, totalTaxableWeight, totalWeight, totalVolume);

        return ebDemandeWithNewResult;
    }

    @PostMapping(value = "/get-list-requestor")
    public @ResponseBody Set<EbUser> getListRequestorByUser(@RequestBody SearchCriteria criteria) {
        SearchCriteriaPricingBooking criteriaPricingBooking = new SearchCriteriaPricingBooking();
        criteriaPricingBooking.setEbUserNum(criteria.getEbUserNum());
        criteriaPricingBooking.setEbCompagnieNum(criteria.getEbCompagnieNum());
        criteriaPricingBooking.setModule(Module.PRICING.getCode());
        return pricingService.getListRequestorByUser(criteriaPricingBooking);
    }
}
