package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbIncoterm;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/incoterm/")
public class IncotermController extends SuperControler {
    @PostMapping(value = "/list-incoterms")
    public @ResponseBody Map<String, Object> getListIncoterms(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbIncoterm> data = ebIncotermService.getListIncoterms(criteria);
        Long count = ebIncotermService.getListIncotermsCount(criteria);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", data);

        return result;
    }

    @PostMapping(value = "/list-incoterm")
    public @ResponseBody List<EbIncoterm> getListIncoterm(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbIncoterm> data = ebIncotermService.getListIncoterms(criteria);
        return data;
    }

    @PostMapping(value = "update-incoterm")
    public EbIncoterm updateIncoterm(@RequestBody EbIncoterm Incoterm) {
        return ebIncotermService.updateIncoterm(Incoterm);
    }

    @PostMapping(value = "delete-incoterm")
    public @ResponseBody Boolean deleteIncoterm(@RequestBody Integer id) {
        return ebIncotermService.deleteIncoterm(id);
    }
}
