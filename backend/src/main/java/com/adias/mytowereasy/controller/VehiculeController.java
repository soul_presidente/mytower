package com.adias.mytowereasy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.model.EbVehicule;
import com.adias.mytowereasy.service.VehiculeService;


@CrossOrigin("*")
@RestController
@RequestMapping("api/vehicule")
public class VehiculeController {
    @Autowired
    VehiculeService vehiculeService;

    @PostMapping(value = "/save")
    public List<EbVehicule> addVehicules(@RequestBody List<EbVehicule> listVehicule) {
        return vehiculeService.SavelistVehicule(listVehicule);
    }

    @GetMapping(value = "/get-vehicule-by-etablissment")
    public List<EbVehicule> getListVehicule(@RequestParam Integer compagnieNum) {
        return vehiculeService.getListVehicule(compagnieNum);
    }

    @GetMapping(value = "/delete")
    public Integer deleteVehicule(@RequestParam Integer vehiculeNum) {
        return vehiculeService.deleteVehicule(vehiculeNum);
    }
}
