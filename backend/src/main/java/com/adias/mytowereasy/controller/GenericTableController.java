package com.adias.mytowereasy.controller;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.model.EbDatatableState;
import com.adias.mytowereasy.service.ListStatiqueService;
import com.adias.mytowereasy.util.TurboTableConfig;


@RestController
@RequestMapping(value = "api/generic-table")
@CrossOrigin("*")
public class GenericTableController {
    @Autowired
    ListStatiqueService serviceListStatique;

    @RequestMapping(value = "/saveStateTable", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String saveStateTable(@RequestBody(required = true) TurboTableConfig config) {
        System.out.println(config);
        serviceListStatique.saveState(config.getState(), config.getEbUserNum(), config.getDatatableCompId());
        JSONObject result = new JSONObject();

        try {
            result.put("message", "Done");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result.toString();
    }

    @RequestMapping(value = "/loadStateTable", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String loadStateTable(@RequestBody(required = true) TurboTableConfig config) {
        EbDatatableState datatable = serviceListStatique.loadState(config.getEbUserNum(), config.getDatatableCompId());

        if (datatable != null) {
            return datatable.getState();
        }

        return null;
    }
}
