package com.adias.mytowereasy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.dto.EbAclRightDTO;
import com.adias.mytowereasy.dto.EbAclSettingDTO;
import com.adias.mytowereasy.service.AclService;


@RestController
@RequestMapping(value = "api/acl")
@CrossOrigin("*")
public class AclController {
    @Autowired
    AclService aclService;

    @GetMapping("list-settings-user")
    public List<EbAclSettingDTO> listSettingsForUser(@RequestParam Integer ebUserNum) {
        return aclService.getSettingsForUser(ebUserNum);
    }

    @GetMapping("list-settings-profile")
    public List<EbAclSettingDTO> listSettingsForProfile(@RequestParam Integer ebUserProfileNum) {
        return aclService.getSettingsForProfile(ebUserProfileNum);
    }

    @GetMapping("list-rights-connectedUser")
    public List<EbAclRightDTO> listRightsForConnectedUser() {
        return aclService.getACLsForConnectedUser();
    }

    @PostMapping("save-settings")
    public void saveSettings(
        @RequestBody List<EbAclSettingDTO> settings,
        @RequestParam(required = false) Integer ebUserProfileNum,
        @RequestParam(required = false) Integer ebUserNum) {
        aclService.saveSettings(settings, ebUserProfileNum, ebUserNum);
    }
}
