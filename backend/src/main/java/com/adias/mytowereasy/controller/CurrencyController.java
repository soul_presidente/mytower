package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbCompagnieCurrency;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/etablissementCurrency")
@CrossOrigin("*")
public class CurrencyController extends SuperControler {

	@PostMapping(value = "/get-etablissementCurrencies")
	public Map<String, Object> getListEtablissementCurrency(@RequestBody SearchCriteria criteria)
	{
		HashMap<String, Object> result = new HashMap<String, Object>();
		result
			.put("data", currencyService.selectListExEtablissementCurrencyByEtablissementNum(criteria));
		result.put("count", currencyService.getListEbCompagnieCurrencyCount(criteria));

		return result;
	}

    @PostMapping(value = "/save-exEtablissementCurrency")
    public Boolean saveExEtablissementCurrency(@RequestBody EbCompagnieCurrency exEtablissementCurrency) {
			return Objects.nonNull(currencyService.saveEbCompagnyCurrency(exEtablissementCurrency));
    }

    @PostMapping(value = "/update-exEtablissementCurrency")
    public Boolean updateExEtablissementCurrency(@RequestBody EbCompagnieCurrency exEtablissementCurrency) {
        return currencyService.updateEbCompanyCurrency(exEtablissementCurrency);
    }

    @PostMapping(value = "/delete-exEtablissementCurrency")
    public Boolean deleteExEtablissementCurrency(@RequestBody EbCompagnieCurrency exEtablissementCurrency) {
        return currencyService.deleteExEtablissementCurrency(exEtablissementCurrency);
    }

    @PostMapping(value = "/get-list-exEtablissementCurrency")
    public List<EbCompagnieCurrency> getListExEtablissementCurrency(@RequestBody SearchCriteria criteria) {
        return currencyService.selectListExEtablissementCurrencyByEtablissementNum(criteria);
    }

    @PostMapping(value = "/get-list-exEtablissementCurrency-forquotation")
    public List<EbCompagnieCurrency> getListExEtablissementCurrencyforQuotation(@RequestBody SearchCriteria criteria) {
        return currencyService.selectListEbCompagnieCurrencyByEbCompagnieAndEbCompagnieGuestNum(criteria);
    }

    @PostMapping(value = "/get-list-CurrencyForDemande")
    public List<EcCurrency> getListCurrencyForDemande(@RequestBody SearchCriteria criteria) {
        return currencyService.selectLisCurrencyCompagnieAndStatique(criteria);
    }

    @PostMapping(value = "/get-list-compagnie-currencys-by-setting")
    public List<EbCompagnieCurrency> getListCompanieCurrencyBySetting(@RequestBody SearchCriteria criteria) {
        return currencyService.getListCompanieCurrencyBySetting(criteria);
    }

    @PostMapping(value = "currency-by-code")
    public EcCurrency getCurrencyByCode(@RequestBody String code) {
        return currencyService.getCurrencyByCode(code);
    }

    @PostMapping(value = "/get-company-currency")
    public EbCompagnieCurrency getEbCompagnieCurrency(@RequestBody SearchCriteria criteria) {
        return currencyService.getEbCompagnieCurrency(criteria);
    }
}
