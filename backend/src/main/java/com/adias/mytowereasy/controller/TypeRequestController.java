package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.dto.EbTypeRequestDTO;
import com.adias.mytowereasy.model.EbTypeRequest;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "api/type-request")
@CrossOrigin("*")
public class TypeRequestController extends SuperControler {
    @RequestMapping(value = "list-type-request-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object> listTypeRequests(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbTypeRequest> entities = typeRequestService.searchTypeRequest(criteria);
        List<EbTypeRequestDTO> dtos = typeRequestService.convertEntitiesToDto(entities);
        Long count = typeRequestService.countListEbTypeRequest(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", dtos);
        return result;
    }

    @RequestMapping(value = "list-type-request", method = RequestMethod.POST, produces = "application/json")
    public List<EbTypeRequestDTO> listTypeRequestDTO(@RequestBody(required = false) SearchCriteria criteria) {
        criteria.setIncludeGlobalValues(true);
        List<EbTypeRequest> listTypeRequests = typeRequestService.searchTypeRequest(criteria);
        return typeRequestService.convertEntitiesToDto(listTypeRequests);
    }

    @GetMapping(value = "list-type-request-contact")
    public @ResponseBody List<EbTypeRequestDTO> listTypeRequestDTOContacts() {
        List<EbTypeRequest> listTypeRequests = typeRequestService.listWithCommunity();
        return typeRequestService.convertEntitiesToDto(listTypeRequests);
    }

    @RequestMapping(value = "add-type-request", method = RequestMethod.POST, produces = "application/json")
    public EbTypeRequestDTO addTypeRequest(@RequestBody(required = false) EbTypeRequestDTO ebTypeRequestDTO) {
        return typeRequestService.saveEbTypeRequest(ebTypeRequestDTO);
    }

    @RequestMapping(value = "delete-type-request", method = RequestMethod.POST, produces = "application/json")
    public void deleteTypeRequest(@RequestBody(required = false) Integer typeRequestNum) {
        typeRequestService.deleteEbTypeRequest(typeRequestNum);
    }

    @RequestMapping(value = "list-service-level", method = RequestMethod.POST, produces = "application/json")
    public List<EbTypeRequestDTO> listServiceLevelDTO(@RequestBody(required = false) SearchCriteria criteria) {
        criteria.setIncludeGlobalValues(true);
        List<EbTypeRequest> listTypeRequests = typeRequestService.getListServiceLevel(criteria);
        return typeRequestService.convertEntitiesToDto(listTypeRequests);
    }
}
