/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.fileupload.FileUploadService;
import com.adias.mytowereasy.service.storage.StorageFileNotFoundException;

import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/api/upload")
public class FileUploadController extends MyTowerService {
    private final FileUploadService fileUploadService;

    @Autowired
    public FileUploadController(FileUploadService fileUploadService) {
        this.fileUploadService = fileUploadService;
    }

    @PostMapping("/upload-file")
    @ResponseBody
    public Object handleFileUpload(
        HttpServletRequest request,
        @RequestParam(value = "typeFile") Integer codeTypeFile,
        @RequestParam(value = "ebCompagnieNum", required = false) Integer ebCompagnieNum,
        @RequestParam("files[]") MultipartFile... files)
        throws Exception {
        return fileUploadService.handleFileUpload(codeTypeFile, ebCompagnieNum, files);
    }

    @PostMapping("/upload-file-import")
    @ResponseBody
    public Object handleFileImportUpload(
        HttpServletRequest request,
        @RequestParam(value = "typeFile") Integer codeTypeFile,
        @RequestParam(value = "ebUserNum") Integer ebUserNum,
        @RequestParam("file") MultipartFile... files)
        throws Exception {
        return fileUploadService.handleFileImportUpload(codeTypeFile, ebUserNum, files);
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }

    /**
     * @param pslEvent
     *            The event where the document was uploaded.
     */
    @PostMapping("/upload-file-demande")
    @ResponseBody
    public List<Map<String, Object>> handleFileUploadDemande(
        HttpServletRequest request,
        @RequestParam(value = "ebEtablissementNum") Integer ebEtablissementNum,
        @RequestParam(value = "ebDemandeNum", required = false) Integer ebDemandeNum,
        @RequestParam(value = "typeFile") Integer codeTypeFile,
        @RequestParam(value = "ebUserNum") Integer ebUserNum,
        @RequestParam(value = "fileTypeDemande", required = false) String fileTypeDemande,
        @RequestParam(value = "module") Integer module,
        @RequestParam(value = "pslEvent", required = false) String pslEvent,
        @RequestParam(value = "ebQmIncidentNum", required = false) Integer ebQmIncidentNum,
        @RequestParam("file") MultipartFile... files)
        throws Exception {
        return fileUploadService
            .handleFileUploadDemande(
                fileTypeDemande,
                codeTypeFile,
                ebDemandeNum,
                ebUserNum,
                ebEtablissementNum,
                module,
                pslEvent,
                ebQmIncidentNum,
                files,
                true);
    }

    @PostMapping("/upload-file-invoice")
    @ResponseBody
    public Object handleFileUploadInvoice(
        HttpServletRequest request,
        @RequestParam(value = "ebInvoiceNum") Integer ebInvoiceNum,
        @RequestParam(value = "typeFile") Integer codeTypeFile,
        @RequestParam(value = "ebUserNum") Integer ebUserNum,
        @RequestParam(value = "fileTypeDemande", required = false) String fileTypeDemande,
        @RequestParam("file") MultipartFile... files)
        throws Exception {
        return fileUploadService.handleFileUploadInvoice(ebInvoiceNum, codeTypeFile, ebUserNum, fileTypeDemande, files);
    }

    @PostMapping("/upload-file-custom")
    @ResponseBody
    public Object handleFileUploadCustom(
        HttpServletRequest request,
        @RequestParam(value = "ebCustomDeclarationNum") Integer ebCustomDeclarationNum,
        @RequestParam(value = "codeTypeFile") Integer codeTypeFile,
        @RequestParam(value = "ebUserNum") Integer ebUserNum,
        @RequestParam(value = "typeDocCode") String typeDocCode,
        @RequestParam("file") MultipartFile... files)
        throws Exception {
        return fileUploadService
            .handleFileUploadDeclaration(ebCustomDeclarationNum, codeTypeFile, ebUserNum, typeDocCode, files);
    }
}
