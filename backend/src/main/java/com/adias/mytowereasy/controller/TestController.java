/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import com.adias.mytowereasy.dao.DaoPricing;
import com.adias.mytowereasy.model.EbInvoice;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.service.ServiceConfiguration;


// import br.com.six2six.fixturefactory.Fixture;
@RestController
@RequestMapping("/test")
public class TestController extends SuperControler {
    @Autowired
    ServiceConfiguration serviceConfiguration;

    @Autowired
    EbDemandeRepository demandeRepository;

    @Autowired
    DaoPricing daoPricing;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @GetMapping(value = "/encode/{password}")
    public String checkEmail(@PathVariable("password") String password) {
        return passwordEncoder.encode(password);
    }

    @GetMapping(value = "/userConnected")
    public String userConnected() {
        return connectedUserService.getCurrentUser().toString();
    }

    @GetMapping(value = "/getEnv")
    public String getEnv() {
        String dd = " active profile = " + serviceConfiguration.springProfilesActive + "      mytowereasyUrlBack ="
            + serviceConfiguration.mytowereasyUrlBack + "param bdd ";
        System.out.println(dd);
        return dd;
    }

    @GetMapping(value = "/my-tower-exception")
    public void testMyRowerException() throws Exception {
        throw new MyTowerException("Test exception");
    }

    @GetMapping(value = "/generate-missing-invoice/{id}")
    public void generateMissingInvoice(@PathVariable Integer id) throws Exception {
        SearchCriteriaPricingBooking scp = new SearchCriteriaPricingBooking();
        scp.setEbDemandeNum(id);
        freightAuditService.preInsertInvoice(daoPricing.getEbDemande(scp, connectedUserService.getCurrentUser()));
    }
}
