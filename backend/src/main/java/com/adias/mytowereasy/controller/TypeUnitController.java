package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.dao.DaoTypeUnit;
import com.adias.mytowereasy.dto.EbTypeMarchandiseDto;
import com.adias.mytowereasy.dto.EbTypeUnitDto;
import com.adias.mytowereasy.dto.mapper.TransportMapper;
import com.adias.mytowereasy.model.EbTypeUnit;
import com.adias.mytowereasy.model.Enumeration.TypeContainer;
import com.adias.mytowereasy.repository.EbTypeUnitRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaTypeUnit;


@RestController
@RequestMapping(value = "/api/type-unit")
@CrossOrigin("*")
public class TypeUnitController {
    @Autowired
    private DaoTypeUnit daoTypeUnit;

    @Autowired
    private EbTypeUnitRepository ebTypeUnitRepository;
    @Autowired
    private TransportMapper mapper;

    @Autowired
    ConnectedUserService userService;

    @RequestMapping(value = "list-type-unit", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getListTypeUnit(@RequestBody(required = false) SearchCriteriaTypeUnit criteria) {

        if (criteria == null) {
            criteria = new SearchCriteriaTypeUnit();
        }

        criteria.setEbCompagnieNum(userService.getCurrentUser().getEbCompagnie().getEbCompagnieNum());
        List<EbTypeUnit> resultOrder = daoTypeUnit.getListTypeUnit(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();

        criteria.setCount(true);
        result.put("count", daoTypeUnit.getListTypeUnitCount(criteria));
        result.put("data", mapper.ebTypeUnitsToEbTypeUnitDtos(resultOrder));
        return result;
    }

    @RequestMapping(value = "save-type-unit", method = RequestMethod.POST, produces = "application/json")
    public EbTypeUnitDto saveTypeUnit(@RequestBody(required = false) EbTypeUnitDto typeUnit) {
        EbTypeUnit type = new EbTypeUnit();
        type.setEbTypeUnitNum(typeUnit.getEbTypeUnitNum());
        type.setGerbable(typeUnit.getGerbableValue());
        type.setHeight(typeUnit.getHeight());
        type.setLength(typeUnit.getLength());
        type.setLibelle(typeUnit.getLibelle());
        type.setWeight(typeUnit.getWeight());
        type.setVolume(typeUnit.getVolume());
        type.setWidth(typeUnit.getWidth());
        type.setCode(typeUnit.getCode());
        type.setxEbCompagnie(userService.getCurrentUser().getEbCompagnie());
        type.setxEbTypeContainer(typeUnit.getxEbTypeContainer());
        type.setActivated(typeUnit.getActivated());
        return mapper.ebTypeUnitToEbTypeUnitDto(daoTypeUnit.saveTypeUnit(type));
    }

    @RequestMapping(value = "delete-type-unit", method = RequestMethod.POST, produces = "application/json")
    public boolean deleteTypeUnit(@RequestBody(required = false) EbTypeUnitDto typeUnit) {
        EbTypeUnit type = new EbTypeUnit();
        type.setEbTypeUnitNum(typeUnit.getEbTypeUnitNum());
        type.setGerbable(typeUnit.getGerbableValue());
        type.setHeight(typeUnit.getHeight());
        type.setLength(typeUnit.getLength());
        type.setLibelle(typeUnit.getLibelle());
        type.setWeight(typeUnit.getWeight());
        type.setVolume(typeUnit.getVolume());
        type.setWidth(typeUnit.getWidth());
        type.setxEbCompagnie(userService.getCurrentUser().getEbCompagnie());
        type.setxEbTypeContainer(typeUnit.getxEbTypeContainer());
        type.setActivated(typeUnit.getActivated());
        return daoTypeUnit.deleteTypeUnit(type);
    }

    @RequestMapping(value = "activate-deactivate-type-unit", method = RequestMethod.GET, produces = "application/json")
    public void activeDesactiveTypeUnit(
        @RequestParam(name = "ebTypeUnitNum", required = false) Long ebTypeUnitNum,
        @RequestParam(name = "activated", required = false) boolean activated) {
        daoTypeUnit.activateDeactivateTypeUnit(ebTypeUnitNum, activated);
    }

    @RequestMapping(value = "list-units", method = RequestMethod.POST, produces = "application/json")
    public List<TypeContainer> listUnit() {
        return TypeContainer.getListTypeContainer();
    }

    @RequestMapping(value = "list-type-marchandise", method = RequestMethod.POST, produces = "application/json")
    public List<EbTypeMarchandiseDto> listTypeMarchandise(@RequestBody(required = true) SearchCriteria criteria) {
        SearchCriteriaTypeUnit criteriaTypeUnit = new SearchCriteriaTypeUnit();
        criteriaTypeUnit.setSearchterm("true");
        criteriaTypeUnit.setEbCompagnieNum(criteria.getEbCompagnieNum());
        List<EbTypeMarchandiseDto> resultList = mapper
            .ebTypeUnitsToEbTypesMarchandise(daoTypeUnit.getListTypeUnit(criteriaTypeUnit));

        if (criteria.getEbDemandeNum() != null) {
            resultList = daoTypeUnit.checkListTypeMarchandise(resultList, criteria.getEbDemandeNum());
        }

        return resultList;
    }

    @GetMapping(value = "/get-typeUnit-by-compagnie")
    public List<EbTypeUnit> getListTypeUnit(@RequestParam Integer compagnieNum) {
        return ebTypeUnitRepository.findByXEbCompagnieEbCompagnieNumAndAndActivated(compagnieNum, true);
    }

    @RequestMapping(
        value = "/exist-code-typeUnit-by-compagnie",
        method = RequestMethod.POST,
        produces = "application/json")
    public Boolean checkIfExistCodeTypeUnitByCompagnie(@RequestBody(required = false) SearchCriteriaTypeUnit criteria) {
        return daoTypeUnit.checkIfExistTypeUnitByCompagnie(criteria);
    }

    @RequestMapping(value = "/typeUnit-by-compagnie", method = RequestMethod.POST)
    public List<EbTypeUnit> getListTypeUnitByCompagnie(@RequestBody SearchCriteria criteria) {
        return ebTypeUnitRepository.findByXEbCompagnieEbCompagnieNumAndAndActivated(criteria.getEbCompagnieNum(), true);
    }
}
