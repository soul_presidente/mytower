package com.adias.mytowereasy.controller;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value = "api/data-recovery")
@CrossOrigin("*")
public class DataRecoveryController extends SuperControler {
    @RequestMapping(value = "generate-data", method = RequestMethod.POST)
    public Boolean generateData(@RequestBody(required = false) Map<String, List<Integer>> data) {
        return dataRecoveryService.generateData(data);
    }
}
