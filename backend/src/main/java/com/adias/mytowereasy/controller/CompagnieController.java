/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbAdresse;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.service.CategorieService;
import com.adias.mytowereasy.service.CompagnieService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/compagnie")
public class CompagnieController extends SuperControler {
    @Autowired
    CompagnieService compagnieservice;

    @Autowired
    CategorieService categorieService;

    @PostMapping(value = "/get-compagnie")
    public EbCompagnie selectEbCompagnie(@RequestBody SearchCriteria criterias) {
        EbCompagnie comp = compagnieservice.selectEbCompagnie(criterias);

        if (comp != null && comp.getListEtablissements() != null) {
            comp.getListEtablissements().stream().forEach(it -> {
                if (it.getEbCompagnie() != null) it
                    .setEbCompagnie(new EbCompagnie(it.getEbCompagnie().getEbCompagnieNum()));
                else it.setEbCompagnie(null);
            });
        }

        return comp;
    }

    @PostMapping(value = "/get-list-etablissement-compagnie")
    public Set<EbEtablissement> selectListEbEtablissementEbCompagnie(@RequestBody SearchCriteria criterias) {
        return compagnieservice.selectListEbEtablissementEbCompagnie(criterias);
    }

    // --------------------------------------------------------------------------//

    @PostMapping(value = "update-etablissement-compagnie")
    public EbEtablissement updateEbEtablissement(@RequestBody EbEtablissement ebEtablissement) {
        return compagnieservice.updateEbEtablissement(ebEtablissement);
    }

    @PostMapping(value = "/get-address-compagnie")
    public List<EbAdresse> getListAddressCompagnie(@RequestBody Integer ebCompagnieNum) {
        List<EbAdresse> ebAdresses = pricingService.getEbAdresseByCompany(ebCompagnieNum);

        return ebAdresses != null ? ebAdresses : new ArrayList<EbAdresse>();
    }

    @PostMapping(value = "/get-list-contact-compagnie")
    public List<EbCompagnie> getListContactCompagnie(@RequestBody SearchCriteria criteria) {
        return compagnieservice.selectListCompagnie(criteria);
    }

    @PostMapping(value = "/get-list-categorie")
    public List<EbCategorie> getListCategorie(@RequestBody Integer ebCompagnieNum) {
        return categorieService.getListEbCategorieByEbCompagnie(ebCompagnieNum);
    }

    @PostMapping(value = "/get-list-compagnie")
    public List<EbCompagnie> getListCompagnie(@RequestBody SearchCriteria criteria) {
        return compagnieservice.selectListCompagnie(criteria);
    }
}
