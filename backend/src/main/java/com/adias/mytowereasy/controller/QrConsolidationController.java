package com.adias.mytowereasy.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;
import com.adias.mytowereasy.airbus.model.EbQrGroupeProposition;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.YesOrNo;
import com.adias.mytowereasy.service.QrConsolidationService;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@RestController
@RequestMapping(value = "api/qr-consolidation")
@CrossOrigin("*")
public class QrConsolidationController extends SuperControler {
    @Autowired
    QrConsolidationService qrConsolidationService;

    @PostMapping(value = "/list-config-qrgroupe")
    public @ResponseBody Map<String, Object> getListQrGroupe(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbQrGroupe> data = qrConsolidationService.getListQrGroupe(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
				result.put("count", data.size());
        result.put("data", data);
        return result;
    }

    @PostMapping(value = "update-qrgroupe")
	public EbQrGroupe updateQrGroupe(@RequestBody EbQrGroupe ebQrGroupe) throws JsonProcessingException {
		return qrConsolidationService.updateQrGroupeAndProcessGroupage(ebQrGroupe);
    }

	@PostMapping(value = "add-qrgroupe")
	public EbQrGroupe addQrGroupe(@RequestBody EbQrGroupe ebQrGroupe) {
		return qrConsolidationService.addQrGroupe(ebQrGroupe);
	}

    @PostMapping(value = "activate-qrgroupe")
    public EbQrGroupe activateQrGroupe(@RequestBody Integer ebQrGroupeNum) throws JsonProcessingException {
        return qrConsolidationService.activateQrGroupe(ebQrGroupeNum);
    }

    @PostMapping(value = "refresh-qrgroupe")
    public List<EbQrGroupe> refreshQrGroupe(@RequestBody SearchCriteriaPricingBooking criteria) {

        try {
            qrConsolidationService.refreshQrGroupe(criteria.getListIdObject(), null, null, connectedUserService.getCurrentUser());
            criteria.setListIdObject(null);
            return this.getListQrGroupeWithDemande(criteria);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @PostMapping(value = "reject-qrgroupe-proposition")
    public int updateQrGroupe(@RequestBody Integer propoNum) {
        return qrConsolidationService.rejectPropositionDemande(propoNum);
    }

    @PostMapping(value = "consolidate-qrgroupe-proposition")
    public EbDemande consolidatePropositionDemande(@RequestBody EbQrGroupeProposition propo) {
        return qrConsolidationService.consolidatePropositionDemande(propo);
    }

    @PostMapping(value = "delete-qrgroupe")
    public boolean deleteQrGroupe(@RequestBody Integer id) {
        return qrConsolidationService.deleteQrGroupe(id);
    }

    @PostMapping(value = "/list-qrgroupe-data")
    public Map<String, Object> getListQrGroupeData(@RequestBody SearchCriteria criteria) throws IOException {
        return qrConsolidationService.getListQrGroupeData(criteria);
    }

    @PostMapping(value = "/list-qrgroupe-with-demande")
    public List<EbQrGroupe> getListQrGroupeWithDemande(@RequestBody SearchCriteriaPricingBooking criteria)
        throws IOException {
        criteria.setIncludeGlobalValues(false);
        criteria.setEligibleForConsolidation(YesOrNo.Yes.getCode());
        return qrConsolidationService.getListQrGroupeWithDemande(criteria);
    }

    @PostMapping(value = "/list-demandes-proposition")
    public List<EbDemande> getListDemandeInProposition(@RequestBody EbQrGroupeProposition prop) throws IOException {
			return qrConsolidationService
				.getListDemandeInProposition(prop, connectedUserService.getCurrentUser());
    }

    @GetMapping(value = "/cancel-consolidation")
    public void cancelConsolidation(@RequestParam Integer ebDemandeNum) {
        qrConsolidationService.cancelConsolidation(ebDemandeNum);
    }

    @PostMapping(value = "/refresh-resulting-request")
    public EbDemande setCommonDataInResultingRequest(@RequestBody EbDemande ebDemande, @RequestParam(name ="checkedTrId") Integer checkedTrId, @RequestParam(name ="isChecked") boolean isChecked) throws JsonProcessingException {
        return qrConsolidationService.setDataInResultingRequest(ebDemande, checkedTrId, isChecked);
    }


    @PostMapping(value = "delete-qr-groupe-and-qr-groupe-props")
    public void deleteQrGroupeAndQrGroupeUnconfirmedPropositions(@RequestBody Integer ebQrGroupeNum) {
       qrConsolidationService.deleteQrGroupeAndQrGroupeUnconfirmedPropositions(ebQrGroupeNum);
    }
}
