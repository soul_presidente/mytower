package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.model.PrMtcTransporteur;
import com.adias.mytowereasy.service.PrMtcTransporteurService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "/api/pr-mtc")
public class PrMtcParametrageController {
    @Autowired
    private PrMtcTransporteurService prMtcTransporteurService;

    @RequestMapping(value = "transporteur", method = RequestMethod.POST, produces = "application/json")
    public Map<String, Object> getAllPrMtcTransporteur(@RequestBody SearchCriteria criteria) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        result = prMtcTransporteurService.getListPrMtcTransporteur(criteria);
        return result;
    }

    @RequestMapping(value = "list-pr-mtc-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object> getListPrMtcTable(@RequestBody(required = false) SearchCriteria criteria) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        result = prMtcTransporteurService.getListPrMtcTransporteur(criteria);
        return result;
    }

    @DeleteMapping(value = "delete-mtc-transporteur")
    public Boolean deleteMtcTransp(@RequestParam Integer mtcTranspNum) {
        return prMtcTransporteurService.deleteMtcTransporteur(mtcTranspNum);
    }

    @PostMapping(value = "update-mtc-transporteur")
    public PrMtcTransporteur updateZone(@RequestBody PrMtcTransporteur prMtcTransporteur) {
        return new PrMtcTransporteur(prMtcTransporteurService.saveMtcTransporteur(prMtcTransporteur));
    }
}
