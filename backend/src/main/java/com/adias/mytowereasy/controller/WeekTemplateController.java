package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.chanel.model.EbWeekTemplate;
import com.adias.mytowereasy.chanel.model.EbWeekTemplateDay;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "api/week-template")
@CrossOrigin("*")
public class WeekTemplateController extends SuperControler {
    @RequestMapping(value = "list-week-template-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object>
        listWeekTemplateTable(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbWeekTemplate> dtos = weekTemplateService.listWeekTemplate(criteria);
        Long count = weekTemplateService.countListWeekTemplate(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", dtos);
        return result;
    }

    @PostMapping(value = "/delete-ebWeekTemplatePeriod")
    public int deleteWeekTemplate(@RequestBody EbWeekTemplate ebWeekTemplate) {
        weekTemplateService.deleteWeekTemplate(ebWeekTemplate);
        return 0;
    }

    @PostMapping(value = "/save-week-template")
    public EbWeekTemplate saveWeekTemplate(@RequestBody EbWeekTemplate ebWeekTemplate) {
        EbWeekTemplate entity = weekTemplateService.saveWeekTemplate(ebWeekTemplate);
        return entity;
    }

    @PostMapping(value = "/list-week-templates")
    public List<EbWeekTemplate> listWeekTemplate(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbWeekTemplate> list = weekTemplateService.listWeekTemplate(criteria);
        return list;
    }

    @PutMapping(value = "update-week-template/{ebWeektemplateNum}")
    public EbWeekTemplate
        updateWeekTemplate(@PathVariable Integer ebWeektemplateNum, @RequestBody EbWeekTemplate ebWeekTemplate) {
        return weekTemplateService.saveWeekTemplate(ebWeekTemplate);
    }

    @GetMapping(value = "/list-day-by-weektemplate/{ebWeektemplateNum}")
    public List<EbWeekTemplateDay> findByWeek_ebWeektemplateNum(@PathVariable Integer ebWeektemplateNum) {
        return weekTemplateService.findByWeek_ebWeektemplateNum(ebWeektemplateNum);
    }
}
