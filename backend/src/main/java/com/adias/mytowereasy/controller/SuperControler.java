/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.adias.mytowereasy.airbus.service.EbTtCompanyPslService;
import com.adias.mytowereasy.cptm.service.ComptaMatiereService;
import com.adias.mytowereasy.cptm.service.RegimeTemporaireService;
import com.adias.mytowereasy.dock.dao.DaoDock;
import com.adias.mytowereasy.dock.service.DockService;
import com.adias.mytowereasy.rsch.service.ReceiptSchedulingService;
import com.adias.mytowereasy.service.*;
import com.adias.mytowereasy.service.category.CategoryService;
import com.adias.mytowereasy.service.email.EmailServiceImpl;
import com.adias.mytowereasy.service.storage.StorageService;
import com.adias.mytowereasy.template.service.TemplateDocumentsService;
import com.adias.mytowereasy.trpl.service.GrilleTransportService;
import com.adias.mytowereasy.trpl.service.PlanTransportService;
import com.adias.mytowereasy.trpl.service.TrancheService;
import com.adias.mytowereasy.trpl.service.ZoneService;


@Controller
public class SuperControler {
    @Autowired
    protected ApplicationContext appContext;

    @Autowired
    InscriptionService inscriptionService;

    @Autowired
    protected UserService userService;

    @Autowired
    EmailServiceImpl emailService;

    @Autowired
    WeekTemplateService weekTemplateService;

    @Autowired
    PlanTransportNewService planTransportNewService;

    @Autowired
    EtablissementService etablissementService;

    @Autowired
    CurrencyService currencyService;

    @Autowired
    CostCenterService costCenterService;

    @Autowired
    UserTagService userTagService;

    @Autowired
    protected DockService dockService;

    @Autowired
    FlagService flagService;

    @Autowired
    BoutonActionService boutonActionService;

    @Autowired
    protected TrancheService trancheService;

    @Autowired
    CategorieService categorieService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    protected ConnectedUserService connectedUserService;

    @Autowired
    protected ComplianceMatrixService complianceMatrixService;

    @Autowired
    PricingService pricingService;

    @Autowired
    QualityManagementService qualityManagementService;

    @Autowired
    FreightAuditService freightAuditService;

    @Autowired
    StorageService storageService;

    @Autowired
    protected GrilleTransportService grilleTransportService;

    @Autowired
    protected ComptaMatiereService comptaMatiereService;

    @Autowired
    CompagnieService compagnieservice;

    @Autowired
    TrackService tracktraceService;

    @Autowired
    CustomDeclarationService customService;

    @Autowired
    CustomConfigService customConfigService;

    @Autowired
    protected EbTypeFluxService ebTypeFluxService;

    @Autowired
    protected EbIncotermService ebIncotermService;

    protected Gson gson = new GsonBuilder().serializeNulls().create();

    String remoteIp(final HttpServletRequest request) throws UnknownHostException {

        if (request.getHeader("x-forwarded-for") != null) {
            return InetAddress.getByName(request.getHeader("x-forwarded-for")).getHostAddress();
        }

        System.out.println("InetAddress.getByName(request.getRemoteAddr())");
        return InetAddress.getByName(request.getRemoteAddr()).getHostAddress();
    }

    @Autowired
    RootCauseService rootCauseService;

    @Autowired
    protected ZoneService zoneService;

    @Autowired
    protected PlanTransportService planTransportService;

    @Autowired
    protected CategoryService categoryService;
    @Autowired
    protected DaoDock daoDock;
    @Autowired
    protected ReceiptSchedulingService receiptSchedulingService;

    @Autowired
    protected RegimeTemporaireService regimeTemporaireService;
    @Autowired
    protected TypeGoodsService typeGoodsService;
    @Autowired
    protected EbTtCompanyPslService ebTtCompanyPslService;

    @Autowired
    protected TemplateDocumentsService templateDocumentsService;

    @Autowired
    protected TypeRequestService typeRequestService;

    @Autowired
    protected WebHooksService webHooksService;

    @Autowired
    protected DataRecoveryService dataRecoveryService;

    @Autowired
    protected AdresseService adresseService;

    @Autowired
    protected FileDownloadService fileDownloadService;

    @Autowired
    protected MappingCodePslService mappingCodePslService;

    @Autowired
    protected AdditionalCostService additionalCostService;

    @Autowired
    QmDeviationService qmDeviationService;
}
