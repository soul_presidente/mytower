/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.FileDownloadStateAndResult;


@RestController
@RequestMapping(value = "api/export")
@CrossOrigin("*")
public class FileDownloadController extends SuperControler {
    @RequestMapping(value = "set-limite", produces = "application/json")
    public String setExportLimite(@RequestParam long size, HttpServletResponse response) throws Exception {
        fileDownloadService.setExtractionLimit(size);
        return "OK";
    }

    @RequestMapping(value = "get-limite", produces = "application/json")
    public long getExportLimite() throws Exception {
        long limit = fileDownloadService.getExtractionLimit();
        return limit;
    }

    @PostMapping(value = "/start-export")
    @ResponseBody
    public Integer startDataExport(@RequestBody Map<String, Object> criteria, HttpServletResponse response)
        throws Exception {
        fileDownloadService.exportData(criteria, response);
        return fileDownloadService.getExtractionRunningCode(criteria.get("username").toString());
    }

    @PostMapping(value = "/check-export-state")
    @ResponseBody
    public FileDownloadStateAndResult checkDataExportState(@RequestBody String code) throws Exception {
        return fileDownloadService.checkExportState(code);
    }

    @RequestMapping(
        value = "/ficheDestinationCountry/{ebDestinationCountryNum}",
        method = RequestMethod.GET,
        produces = "application/pdf")
    public void exportFacture(
        @PathVariable("ebDestinationCountryNum") Integer ebDestinationCountryNum,
        HttpServletResponse response)
        throws Exception {
        fileDownloadService.exportFacture(ebDestinationCountryNum, response);
    }

    @ResponseBody
    @GetMapping(value = "extract-data-analysis")
    public void extractPricing(@RequestParam Integer type, HttpServletResponse response) {
        fileDownloadService.extractPricing(type, response);
    }
}
