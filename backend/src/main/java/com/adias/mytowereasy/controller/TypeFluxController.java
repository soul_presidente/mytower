package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbTypeFlux;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/type-flux/")
public class TypeFluxController extends SuperControler {
    @PostMapping(value = "/list-type-flux")
    public @ResponseBody Map<String, Object> getListTypeFlux(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbTypeFlux> data = ebTypeFluxService.getListTypeFlux(criteria);
        Long count = ebTypeFluxService.getListTypeFluxCount(criteria);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", data);

        return result;
    }

    @PostMapping(value = "/list-of-type-flux")
    public @ResponseBody List<EbTypeFlux> getListOfTypeFlux(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbTypeFlux> data = ebTypeFluxService.getListTypeFlux(criteria);
        return data;
    }

    @PostMapping(value = "update-type-flux")
    public EbTypeFlux updateTypeFlux(@RequestBody EbTypeFlux typeFlux) {
        return ebTypeFluxService.updateTypeFlux(typeFlux);
    }

    @PostMapping(value = "delete-type-flux")
    public @ResponseBody Boolean deleteTypeFlux(@RequestBody Integer id) {
        return ebTypeFluxService.deleteTypeFlux(id);
    }
}
