package com.adias.mytowereasy.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.KibanaUser;
import com.adias.mytowereasy.service.KibanaAuthorisationService;
import com.adias.mytowereasy.service.ServiceConfiguration;
import com.adias.mytowereasy.util.KibanaResponse;


@RestController
@RequestMapping(value = "api/kibana")
@CrossOrigin("*")
public class KibanaAuthorisationController {
    private final KibanaAuthorisationService kibanaAuthorisationService;

    private ServiceConfiguration serviceConfiguration;

    public KibanaAuthorisationController(
        KibanaAuthorisationService kibanaAuthorisationService,
        ServiceConfiguration serviceConfiguration) {
        this.kibanaAuthorisationService = kibanaAuthorisationService;
        this.serviceConfiguration = serviceConfiguration;
    }

    @GetMapping(produces = "application/json")
    public @ResponseBody KibanaUser getUserFromToken(HttpServletRequest request) {
        return kibanaAuthorisationService.getUserFromToken(request);
    }

    @PostMapping(produces = "application/json")
    public KibanaResponse getKibanaToken(@RequestParam(value = "token") String access_token) {
        return kibanaAuthorisationService
            .getKibanaToken(access_token, this.serviceConfiguration.getDynamicExpirationTime());
    }
}
