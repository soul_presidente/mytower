package com.adias.mytowereasy.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EbInvoiceRepository;
import com.adias.mytowereasy.repository.ExEbDemandeTransporteurRepository;


@CrossOrigin("*")
@RestController
@RequestMapping("api/refresh-cotation-price")
public class RefreshCotationPrice {
    private static final Logger logger = LoggerFactory.getLogger(RefreshCotationPrice.class);
    @Autowired
    EbInvoiceRepository ebInvoiceRepository;
    @Autowired
    ExEbDemandeTransporteurRepository exEbDemandeTransporteurRepository;
    @Autowired
    EbDemandeRepository ebDemandeRepository;

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Map> refreshAllPrices(@RequestParam(required = false) Integer ebCompanyNum) throws IOException {
        List<Map> retour = new ArrayList<>();
        List<EbDemande> listEbdemande;

        if (ebCompanyNum == null) {
            listEbdemande = ebDemandeRepository.selectAllEbDemande();
        }
        else {
            listEbdemande = ebDemandeRepository.selectAllEbDemandeByCompany(ebCompanyNum);
        }

        for (EbDemande d: listEbdemande) {
            logger.info("Traitement de la demande :: {}", d.getEbDemandeNum());
            Map m;

            try {
                m = calcul(d.getEbDemandeNum());
            } catch (Exception e) {
                m = new HashMap();
                m.put(d.getEbDemandeNum(), "Une erreur est survenu lors du rafraichissement du  prix de la cotation");
                logger.info("***** erreur lors du Traitement de la demande :: {}", d.getEbDemandeNum());
            }

            retour.add(m);
        }

        return retour;
    }

    @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Map calcul(@RequestParam Integer ebDemandeNum) throws IOException {
        EbDemande demande = ebDemandeRepository.getOne(ebDemandeNum);

        List<ExEbDemandeTransporteur> listCotation = demande.getExEbDemandeTransporteurs();

        ExEbDemandeTransporteur cotationFinalChoice = null;

        for (ExEbDemandeTransporteur cotation: listCotation) {

            if (Enumeration.StatutCarrier.FIN.getCode().equals(cotation.getStatus())) {
                cotationFinalChoice = cotation;
            }

        }

        BigDecimal cotationPrice = new BigDecimal(0);
        List<EbCostCategorie> listCostCategorie = convertToList(cotationFinalChoice.getListCostCategorie());

        for (EbCostCategorie categorie: listCostCategorie) {
            List<EbCost> listCost = convertToListCost(categorie.getListEbCost());

            for (EbCost costItem: listCost) {

                if (costItem.getPrice() != null) {
                    BigDecimal priceCalcul = costItem
                        .getPrice().multiply(
                            new BigDecimal(
                                costItem.getEuroExchangeRateReal() != null ? costItem.getEuroExchangeRateReal() : 1));
                    cotationPrice = cotationPrice.add(priceCalcul);
                }

            }

        }

        cotationFinalChoice.setPrice(cotationPrice);
        exEbDemandeTransporteurRepository
            .updatePrice(cotationFinalChoice.getExEbDemandeTransporteurNum(), cotationFinalChoice.getPrice());
        ebInvoiceRepository.updateNegociatedpriceByPricing(cotationPrice, ebDemandeNum);
        Map map = new HashMap<>();
        map.put(ebDemandeNum, cotationPrice);
        return map;
    }

    private List<EbCostCategorie> convertToList(List listCostCategorie) throws JsonProcessingException {
        List<EbCostCategorie> listCostCategorieReturn;

        ObjectMapper objectMapper = new ObjectMapper();

        String json = objectMapper.writeValueAsString(listCostCategorie);
        listCostCategorieReturn = objectMapper.readValue(json, new TypeReference<ArrayList<EbCostCategorie>>() {
        });
        return listCostCategorieReturn;
    }

    private List<EbCost> convertToListCost(List listCost) throws JsonProcessingException {
        List<EbCost> listCostCategorieReturn;

        ObjectMapper objectMapper = new ObjectMapper();

        String json = objectMapper.writeValueAsString(listCost);
        listCostCategorieReturn = objectMapper.readValue(json, new TypeReference<ArrayList<EbCost>>() {
        });

        return listCostCategorieReturn;
    }
}
