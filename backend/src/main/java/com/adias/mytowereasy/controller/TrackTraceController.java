/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.dto.EbTTPslAppAndEventsProjection;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.model.tt.EbTtEvent;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.service.CustomFieldService;
import com.adias.mytowereasy.utils.search.SearchCriteria.OrderCriterias;
import com.adias.mytowereasy.utils.search.SearchCriteriaTrackTrace;


@RestController
@RequestMapping(value = "api/track-trace")
@CrossOrigin("*")
public class TrackTraceController extends SuperControler {
    @Autowired
    CustomFieldService customFieldService;

    @PostMapping(value = "/listTrack")
    public HashMap<String, Object>
        getListTracing(@RequestBody SearchCriteriaTrackTrace params) throws JsonProcessingException, Exception {

        if (params.getSearchInput() != null && params.getSearchInput().length() > 0) {
            params.setSearchInput(params.getSearchInput(), customFieldService.getCustomFields(params.getEbUserNum()));
        }

        if (params.getOrderedColumn() != null) {
            Map<OrderCriterias, String> map = new HashMap<OrderCriterias, String>();
            List<Map<OrderCriterias, String>> listMap = new ArrayList<Map<OrderCriterias, String>>();
            map.put(OrderCriterias.columnName, params.getOrderedColumn());
            map.put(OrderCriterias.dir, params.getAscendant() ? "asc" : "desc");
            listMap.add(map);
            params.setOrder(listMap);
        }

        if (params.getSearchterm() != null && !params.getSearchterm().trim().isEmpty()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", params.getSearchterm());
            params.setSearch(map);
        }

        List<EbTtTracing> TrackList = tracktraceService.getListEbTrackTrace(params);
        int counter = tracktraceService.getCountListEbTrackTrace(params);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", counter);
        result.put("data", TrackList);

        return result;
    }

    @PostMapping(value = "/list-count")
    public Integer
        getListCount(@RequestBody SearchCriteriaTrackTrace params) throws JsonProcessingException, Exception {

        if (StringUtils.isNotBlank(params.getSearchInput())) {
            params.setSearchInput(params.getSearchInput(), customFieldService.getCustomFields(params.getEbUserNum()));
        }

        return tracktraceService.getCountListEbTrackTrace(params);
    }

    @RequestMapping(value = "/getEbTracing", method = RequestMethod.POST)
    public EbTtTracing getEbTracing(@RequestBody SearchCriteriaTrackTrace params) {
        if (params.getEbTtTracingNum() != null) return tracktraceService.getEbTracing(params);
        return new EbTtTracing();
    }

    @RequestMapping(value = "/getTracingPslAndsThiereEvents", method = RequestMethod.POST)
    public List<EbTTPslAppAndEventsProjection> getTracingPslAndsThiereEvents(@RequestBody Integer ebTtTracingNum) {
        List<EbTTPslAppAndEventsProjection> result = tracktraceService.getTracingPslAndsThiereEvents(ebTtTracingNum);
        return result;
    }

    @RequestMapping(value = "/updateEbPslApp", method = RequestMethod.POST)
    public Map<String, Object> updateEbPslApp(@RequestBody EbTTPslApp ebPslApp) {
        Map<String, Object> res = tracktraceService.updateEbPslApp(ebPslApp);
        return res;
    }

    @RequestMapping(value = "/updateEbPslAppMasse", method = RequestMethod.POST)
    public List<EbTtTracing> updateEbPslAppMasse(@RequestBody List<EbTTPslApp> listPslApp) throws Exception {
        Boolean result = tracktraceService.updateEbPslAppMasse(listPslApp, null, null).getResult();
        List<EbTtTracing> listEbTtTracing = new ArrayList<EbTtTracing>();

        if (result) {
            SearchCriteriaTrackTrace criteria = new SearchCriteriaTrackTrace();
            criteria
                .setListEbTtTracingNum(
                    listPslApp
                        .stream().map(it -> it.getxEbTrackTrace().getEbTtTracingNum()).collect(Collectors.toList()));

            listEbTtTracing = tracktraceService.getListEbTrackTrace(criteria);
        }

        return listEbTtTracing;
    }

    @RequestMapping(value = "/addDeviationMasse", method = RequestMethod.POST)
    public Integer addDeviationMasse(
        @RequestBody List<EbTtEvent> listTTEvent,
        @RequestParam(required = false) Integer typeOfEvent) {
        for (EbTtEvent ev: listTTEvent) ev.setTypeEvent(typeOfEvent);

        return tracktraceService.addDeviationMasse(listTTEvent, null);
    }

    @RequestMapping(value = "/addDeviation", method = RequestMethod.POST)
    public Map<String, Object>
        addDeviation(@RequestBody EbTtEvent ebTtEvent, @RequestParam(required = false) Integer typeOfEvent) {
        ebTtEvent.setTypeEvent(typeOfEvent);

        return tracktraceService.addDeviation(ebTtEvent, null, true);
    }

    @RequestMapping(value = "/getListEbTracingByEbDemandeNum", method = RequestMethod.POST)
    public List<EbTtTracing> getListEbTracingByEbDemandeNum(@RequestBody Integer ebDemandeNum) {
        return tracktraceService.getListEbTracingByEbDemandeNum(ebDemandeNum);
    }

    @PostMapping(value = "/get-TracingSearch")
    public EbTtTracing getTrack(@RequestBody SearchCriteriaTrackTrace criterias) {
        return tracktraceService.getListEbtrack(criterias).get(0);
    }

    @RequestMapping(value = "/addTrackTrace", method = RequestMethod.POST)
    public List<EbTtTracing> addTrackTrace(@RequestBody EbDemande ebDemande) {
        return tracktraceService.insertEbTrackTrace(ebDemande);
    }

    @RequestMapping(value = "/getListEbIncotermPsl", method = RequestMethod.GET)
    public List<EbTtSchemaPsl> getListEbIncotermPsl() {
        return tracktraceService.getListEbIncotermPsl();
    }

    @GetMapping("/saveFlags")
    public EbTtTracing saveFlag(@RequestParam Integer ebTtTracingNum, @RequestParam String newFlags) {
        return tracktraceService.saveFlags(ebTtTracingNum, newFlags);
    }

    @RequestMapping(value = "/list-track-smart", method = RequestMethod.POST)
    public List<EbTtTracing> listTrack(@RequestBody SearchCriteriaTrackTrace criteria) {
        return tracktraceService.getTracing(criteria);
    }

    @RequestMapping(value = "/updateRevisedDateEbPslAppMasse", method = RequestMethod.POST)
    public List<EbTtTracing> updateRevisedDateEbPslAppMasse(@RequestBody List<EbTTPslApp> listPslApp) throws Exception {
        Integer result = tracktraceService.updateRevisedDateEbPslAppMasse(listPslApp);
        List<EbTtTracing> listEbTtTracing = new ArrayList<EbTtTracing>();

        if (result == 1) {
            SearchCriteriaTrackTrace criteria = new SearchCriteriaTrackTrace();
            criteria
                .setListEbTtTracingNum(
                    listPslApp
                        .stream().map(it -> it.getxEbTrackTrace().getEbTtTracingNum()).collect(Collectors.toList()));

            listEbTtTracing = tracktraceService.getListEbTrackTrace(criteria);
        }

        return listEbTtTracing;
    }

    @RequestMapping(value = "/updateRevisedDateEbPslApp", method = RequestMethod.POST)
    public Map<String, Object> updateRevisedDateEbPslApp(@RequestBody EbTTPslApp ebPslApp) {
        Map<String, Object> res = tracktraceService.updateRevisedDateEbPslApp(ebPslApp);

        return res;
    }
}
