/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.io.IOException;
import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbEmailHistorique;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.qm.*;
import com.adias.mytowereasy.service.CustomFieldService;
import com.adias.mytowereasy.util.EmptyJsonResponse;
import com.adias.mytowereasy.util.email.Email;
import com.adias.mytowereasy.utils.angulardatatable.DatatableResult;
import com.adias.mytowereasy.utils.search.SearchCriteria.OrderCriterias;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


@RestController
@RequestMapping(value = "api/quality-management")
@CrossOrigin("*")
public class QualityManagementController extends SuperControler {
    @Autowired
    CustomFieldService customFieldService;

    @RequestMapping(value = "/listIncident", method = RequestMethod.POST)
    public HashMap<String, Object>
        listIncident(@RequestBody SearchCriteriaQM params) throws JsonProcessingException, Exception {

        if (params.getSearchInput() != null && params.getSearchInput().length() > 0) {
            params.setSearchInput(params.getSearchInput(), customFieldService.getCustomFields(params.getEbUserNum()));
        }

        if (params.getOrderedColumn() != null) {
            Map<OrderCriterias, String> map = new HashMap<OrderCriterias, String>();
            List<Map<OrderCriterias, String>> listMap = new ArrayList<Map<OrderCriterias, String>>();
            map.put(OrderCriterias.columnName, params.getOrderedColumn());
            map.put(OrderCriterias.dir, params.getAscendant() ? "asc" : "desc");
            listMap.add(map);
            params.setOrder(listMap);
        }

        if (params.getSearchterm() != null && !params.getSearchterm().trim().isEmpty()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", params.getSearchterm());
            params.setSearch(map);
        }

        List<EbQmIncident> incidentList = qualityManagementService.getListEbIncident(params);
        int counter = qualityManagementService.getCountListEbIncident(params);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", counter);
        result.put("data", incidentList);

        return result;
    }

    @RequestMapping(value = "/list-count", method = RequestMethod.POST)
    public Integer getListCount(@RequestBody SearchCriteriaQM params) throws JsonProcessingException, Exception {

        if (StringUtils.isNotBlank(params.getSearchInput())) {
            params.setSearchInput(params.getSearchInput(), customFieldService.getCustomFields(params.getEbUserNum()));
        }

        return qualityManagementService.getCountListEbIncident(params);
    }

    @RequestMapping(value = "/saveStateTable", method = RequestMethod.POST)
    public @ResponseBody String saveStateTable(@RequestParam(required = false, value = "state") String state) {
        System.out.println("done");
        return "done";
    }

    @RequestMapping(value = "/loadStateTable", method = RequestMethod.GET)
    public @ResponseBody String loadStateTable() {
        System.out.println("done");
        return "";
    }

    @RequestMapping(value = "/listTransporteur", method = RequestMethod.GET)
    public Object getTransporteurs(@RequestParam String searchCriteria)
        throws JsonParseException,
        JsonMappingException,
        IOException {
        // ObjectMapper mapper = new ObjectMapper();

        // SearchCriteria criteria = mapper.readValue(searchCriteria,
        // SearchCriteria.class);
        DatatableResult result = new DatatableResult();

        // List<EbUser> listTransporteur =
        // userService.selectListContact(criteria).stream().map(relation ->
        // relation.getGuest()).collect(Collectors.toList());

        // result.setRecordsFiltered(listTransporteur.size());
        // result.setRecordsTotal(listTransporteur.size());
        // result.setData(listTransporteur);

        return result;
    }

    @RequestMapping(value = "/listCategories", method = RequestMethod.GET)
    public Object getEbCategorieByEtablissement(@RequestParam Integer ebEtablissementNum) {
        List<EbCategorie> ebCategories = categorieService.getAllEbCategorieWithLabels(ebEtablissementNum);

        if (ebCategories == null) {
            return new EmptyJsonResponse();
        }
        else {
            return new ResponseEntity<>(ebCategories, HttpStatus.OK);
        }

    }

    @RequestMapping(value = "/list-root-cause", method = RequestMethod.POST)
    public HashMap<String, Object>
        listRootCause(@RequestBody SearchCriteriaQM params) throws JsonProcessingException, Exception {

        if (params.getSearchInput() != null && params.getSearchInput().length() > 0) {
            params.setSearchInput(params.getSearchInput(), customFieldService.getCustomFields(params.getEbUserNum()));
        }

        if (params.getOrderedColumn() != null) {
            Map<OrderCriterias, String> map = new HashMap<OrderCriterias, String>();
            List<Map<OrderCriterias, String>> listMap = new ArrayList<Map<OrderCriterias, String>>();
            map.put(OrderCriterias.columnName, params.getOrderedColumn());
            map.put(OrderCriterias.dir, params.getAscendant() ? "asc" : "desc");
            listMap.add(map);
            params.setOrder(listMap);
        }

        if (params.getSearchterm() != null && !params.getSearchterm().trim().isEmpty()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", params.getSearchterm());
            params.setSearch(map);
        }

        List<EbQmRootCause> listRootCause = rootCauseService.listRootCause(params);
        Long counter = rootCauseService.getCountListRootCause(params);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", counter);
        result.put("data", listRootCause);

        return result;
    }

    @RequestMapping(value = "/list-incident-category", method = RequestMethod.GET)
    public List<EbQmCategory> listIncidentCategory() {
        return qualityManagementService.listCategory();
    }

    @RequestMapping(value = "/list-incident-qualification", method = RequestMethod.GET)
    public List<EbQmQualification> listQmQualification() {
        return qualityManagementService.listQmQualification();
    }

    @RequestMapping(value = "/list-incident-close-reason", method = RequestMethod.GET)
    public List<EbQmCloseReason> listQmCloseReason() {
        return qualityManagementService.listQmCloseReason();
    }

    @RequestMapping(value = "/get-incident", method = RequestMethod.POST)
    @ResponseBody
    public EbQmIncident getIncident(@RequestBody SearchCriteriaQM criteria) {
        return qualityManagementService.getEbIncident(criteria);
    }

    @RequestMapping(value = "/get-list-marchandise", method = RequestMethod.POST)
    @ResponseBody
    public List<EbMarchandise> getListMarchandise(@RequestBody SearchCriteriaQM criteria) {
        return qualityManagementService.listMarchandise(criteria);
    }

    @RequestMapping(value = "/get-semi-auto-mail", method = RequestMethod.POST)
    @ResponseBody
    public SemiAutoMail getSemeAutoMail(@RequestBody SearchCriteriaQM criteria) {
        return qualityManagementService.getSemiAutoMail(criteria);
    }

    @RequestMapping(value = "/send-semi-auto-mail", method = RequestMethod.POST)
    @ResponseBody
    public EbEmailHistorique
        sendSemiAutoMail(@RequestBody Email mailToSend, @RequestParam("incidentNum") Integer incidentNum) {
        return qualityManagementService.sendMail(mailToSend, incidentNum);
    }

    @RequestMapping(value = "/update-list-incident-line", method = RequestMethod.POST)
    @ResponseBody
    public List<EbQmIncidentLine> updateListIncidentLine(@RequestBody List<EbQmIncidentLine> listIncidentLine) {
        return qualityManagementService.updateListIncidentLine(listIncidentLine);
    }

    @PostMapping(value = "/save-incidents")
    public Map<Integer, Map<String, Object>>
        saveIncidents(
            @RequestParam(value = "isUpdate", required = true) boolean isUpdate,
            @RequestBody List<EbQmIncident> incidents) {
        return qualityManagementService.saveIncidents(incidents, isUpdate, null);
    }

    @PutMapping(value = "/updateIncident")
    public Map<Integer, Map<String, Object>> updateIncident(@RequestBody EbQmIncident incident) {
        List<EbQmIncident> incidents = Arrays.asList(incident);
        return qualityManagementService.saveIncidents(incidents, true, null);
    }

    @RequestMapping(value = "/update-incident-comment", method = RequestMethod.POST)
    @ResponseBody
    public int updateIncidentComment(@RequestBody EbQmIncident incident) {
        return qualityManagementService
            .updateIncidentComment(
                incident.getEbQmIncidentNum(),
                incident.getEtablissementComment(),
                incident.getThirdPartyComment());
    }

    @RequestMapping(value = "/update-incident-description", method = RequestMethod.POST)
    @ResponseBody
    public int updateIncidentDescription(@RequestBody EbQmIncident incident) {
        return qualityManagementService
            .updateIncidentDescription(
                incident.getEbQmIncidentNum(),
                incident.getIncidentDesc(),
                incident.getAddImpact());
    }

    @RequestMapping(value = "/update-incident-shipment", method = RequestMethod.POST)
    @ResponseBody
    public int updateIncidentShipment(@RequestBody EbQmIncident incident) {
        return qualityManagementService.updateIncidentShipment(incident);
    }

    @RequestMapping(value = "/update-incident", method = RequestMethod.POST)
    @ResponseBody
    public int updateInciden(@RequestBody EbQmIncident incident) {
        return qualityManagementService.updateIncident(incident);
    }

    @RequestMapping(value = "/listIncidentRef", method = RequestMethod.GET)
    public List<EbQmIncident> searchlistIncidentRef(@RequestParam(required = false, value = "term") String term)
        throws JsonProcessingException {
        List<EbQmIncident> incidentRef = qualityManagementService.getListIncidentRef(term);
        return incidentRef;
    }

    @GetMapping(value = "/listInsurance", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListInsurance() throws IOException {
        String listInsurance = "";
        listInsurance = Enumeration.Insurance.toJsonList();

        return listInsurance;
    }

    @GetMapping(value = "/listClassificationIncident", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListClassificationIncident() throws IOException {
        String listClassificationIncident = "";
        listClassificationIncident = Enumeration.ClassificationIncident.toJsonList();

        return listClassificationIncident;
    }

    @GetMapping(value = "/get-next-ebQmIncidentNum")
    public Integer getNextEbQmIncidentNum() {
        return qualityManagementService.getNextEbQmIncidentNum();
    }

    @GetMapping(value = "/get-list-doc")
    public EbQmIncident getListDoc(@RequestParam Integer incidentNum) {
        return qualityManagementService.getListDoc(incidentNum);
    }

    @GetMapping("/saveFlags")
    public EbQmIncident saveFlag(@RequestParam Integer ebQmIncidentNum, @RequestParam String newFlags) {
        return qualityManagementService.saveFlags(ebQmIncidentNum, newFlags);
    }

    @GetMapping(value = "/listRootCause")
    public List<EbQmRootCause> listRootCause(@RequestParam(required = false) Integer eventType) {
        SearchCriteriaQM criteria = new SearchCriteriaQM();
        EbUser userConnected = connectedUserService.getCurrentUserFromDB();

        if (userConnected != null && userConnected.getEbCompagnie() != null) {
            criteria.setEbCompNum(userConnected.getEbCompagnie().getEbCompagnieNum());
        }

        if (eventType != null) criteria.setEventType(eventType);

        return rootCauseService.listRootCause(criteria);
    }

    @RequestMapping(value = "/save-root-cause", method = RequestMethod.POST)
    @ResponseBody
    public EbQmRootCause saveRootCause(@RequestBody EbQmRootCause rootCause) {
        return qualityManagementService.saveRootCause(rootCause);
    }

    @PostMapping(value = "/list-units")
    public HashMap<String, Object> getListMarchandiseForDT(@RequestBody SearchCriteriaQM criteria) {
        List<EbMarchandise> listUnits = new ArrayList<>();
        listUnits = qualityManagementService.listMarchandise(criteria);
        long counter = qualityManagementService.countListMarchandises(criteria);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("data", listUnits);
        result.put("count", counter);
        return result;
    }

    @GetMapping(value = "update-eventType-status")
    public int updateEventTypeStatus(Integer ebQmIncidentNum, Integer iStatus) {
        return qualityManagementService.updateEventTypeStatus(ebQmIncidentNum, iStatus);
    }

    @GetMapping(value = "/transfer-deviations-to-incidents")
    public void transferDeviationsToIncidents() {
        qmDeviationService.transferDeviationsToIncidents();
    }
}
