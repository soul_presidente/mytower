package com.adias.mytowereasy.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "webhooks")
@CrossOrigin("*")
public class BusnessRelationshipController extends SuperControler {
    @RequestMapping(value = "champ/track", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ResponseEntity getTracks(
        @RequestHeader("User") String user,
        @RequestHeader("Password") String password,
        @RequestBody(required = false) String messageBody) {

        if (webHooksService.addWebHooks(user, password, messageBody)) {
            return ResponseEntity.status(HttpStatus.OK).body("success");
        }

        return ResponseEntity.status(HttpStatus.FORBIDDEN).body("bad authentication!");
    }
}
