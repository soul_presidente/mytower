package com.adias.mytowereasy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbCostCenter;
import com.adias.mytowereasy.service.CostCenterService;


@RestController()
@RequestMapping("api/costcenter")
public class CostCenterController extends SuperControler {
    @Autowired
    CostCenterService costCenterService;

    @GetMapping(value = "/get-list-cost-center")
    public List<EbCostCenter> getListCostCenterCompagnie(@RequestParam Integer ebCompagnieNum) {
        return costCenterService.getListCostCenterCompagnie(ebCompagnieNum);
    }

    @PostMapping(value = "/add-cost-center")
    public EbCostCenter addCostCenterCompagnie(@RequestBody EbCostCenter ebCostCenter) {
        return costCenterService.addEbCostCenterEbCompagnie(ebCostCenter);
    }

    @PostMapping(value = "/edit-cost-center")
    public EbCostCenter editCostCenterCompagnie(@RequestBody EbCostCenter ebCostCenter) {
        return costCenterService.updateEbCostCenterEbCompagnie(ebCostCenter);
    }

    @PostMapping(value = "/delete-cost-center")
    public Boolean deleteCostCenterCompagnie(@RequestBody EbCostCenter ebCostCenter) {
        return costCenterService.deleteEbCostCenterEbCompagnie(ebCostCenter);
    }
}
