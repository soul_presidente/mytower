/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.dto.CustomDeclarationDocumentDTO;
import com.adias.mytowereasy.dto.CustomDeclarationPricingDTO;
import com.adias.mytowereasy.model.EbDemandeFichiersJoint;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.custom.EbCustomDeclaration;
import com.adias.mytowereasy.repository.custom.EbChatCustomRepository;
import com.adias.mytowereasy.utils.search.SearchCriteriaCustom;


@RestController
@RequestMapping(value = "api/custom")
@CrossOrigin("*")
public class CustomController extends SuperControler {
    @Autowired
    EbChatCustomRepository ebChatCustomRepository;

    @RequestMapping(value = "/listDeclaration", method = RequestMethod.POST)
    @ResponseBody
    public HashMap<String, Object> tableDeclaration(@RequestBody SearchCriteriaCustom params) {
        List<EbCustomDeclaration> entities = customService.getListEbDeclaration(params);
        int count = customService.getCountListEbDeclaration(params);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", entities);
        return result;
    }

    @GetMapping(value = "/get-list-type", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListType() throws IOException {
        String listFlow = "";
        listFlow = Enumeration.Flow.getListFlow();

        return listFlow;
    }

    @PostMapping(value = "/add")
    public EbCustomDeclaration addEbDeclaration(@Valid @RequestBody EbCustomDeclaration ebDeclaration) {
        return customService.addEbDeclaration(ebDeclaration);
    }

    @RequestMapping(value = "/getEbDeclaration", method = RequestMethod.POST)
    public EbCustomDeclaration getEbDeclaration(@RequestBody SearchCriteriaCustom params) {
        /*
         * if (params.getEbCustomDeclarationNum() != null)
         */ return customService.getEbDeclaration(params);
        // return new EbCustomDeclaration();
    }

    @PostMapping(value = "/new-declaration-pricing")
    public EbCustomDeclaration newDeclarationPricing(@RequestBody CustomDeclarationPricingDTO request)
        throws Exception {
        return customService.newDeclarationPricing(request);
    }

    @PostMapping(value = "procedure-dashboard-declaration-inline-update")
    public EbCustomDeclaration inlineDashboardUpdate(@RequestBody EbCustomDeclaration sent) {
        return customService.inlineProcedureDashboardUpdate(sent);
    }

    @RequestMapping(value = "/listDocuments", method = RequestMethod.GET)
    public List<CustomDeclarationDocumentDTO> searchlistDocuments(
        @RequestParam(required = false, value = "module") Integer module,
        @RequestParam(required = false, value = "ebUserNum") Integer ebUserNum,
        @RequestParam(required = false, value = "type") Integer type,
        @RequestParam(required = false, value = "demande") Integer demande)
        throws JsonProcessingException {
        return customService.searchlistDocuments(module, ebUserNum, type, demande);
    }

    @GetMapping("/list-fichierJoints-declaration")
    public List<EbDemandeFichiersJoint> listFichierJointsDeclaration(@RequestParam Integer ebDeclarationNum) {
        return customService
            .listFichierJointsDeclaration(
                ebDeclarationNum,
                connectedUserService.getCurrentUser().getEbCompagnie().getEbCompagnieNum());
    }
}
