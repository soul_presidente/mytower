/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.airbus.service.EbTtCompanyPslService;
import com.adias.mytowereasy.cptm.service.ComptaMatiereService;
import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.enumeration.StatusOrder;
import com.adias.mytowereasy.dto.CodeLibelleDTO;
import com.adias.mytowereasy.dto.ChatDTO;
import com.adias.mytowereasy.dto.mapper.EbChatMapper;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.CREnumeration.RuleOperandType;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.NatureDemandeTransport;
import com.adias.mytowereasy.model.Enumeration.ServiceType;
import com.adias.mytowereasy.model.Enumeration.TransfertActionChoice;
import com.adias.mytowereasy.model.Enumeration.TypeContainer;
import com.adias.mytowereasy.model.tt.EbTtCategorieDeviation;
import com.adias.mytowereasy.repository.*;
import com.adias.mytowereasy.service.ListStatiqueService;
import com.adias.mytowereasy.service.TrackService;
import com.adias.mytowereasy.service.UserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "api/list")
@CrossOrigin("*")
public class ListStatiqueController extends SuperControler {
    private static final Logger LOGGER = LoggerFactory.getLogger(ListStatiqueController.class);
    @Autowired
    ListStatiqueService serviceListStatique;

    @Autowired
    UserService userService;

    @Autowired
    EbDemandeFichiersJointRepositorty ebDemandeFichiersJointRepositorty;

    @Autowired
    TrackService trackService;

    @Autowired
    EbTtCompanyPslService companyPslService;
    @Autowired
    ComptaMatiereService comptaMatiereService;

    @Autowired
    protected EbUserRepository ebUserRepository;

    @Autowired
    protected ExEbDemandeTransporteurRepository exEbDemandeTransporteurRepository;

    @Autowired
    protected EbEtablissementRepository ebEtablissementRepository;

    @Autowired
    protected EbCompagnieRepository ebCompagnieRepository;

    @Autowired
    private EbChatMapper chatMapper;

    @GetMapping(value = "list-generic-enum")
    public String listGenericEnum(@RequestParam(required = false, value = "genericEnumKey") String genericEnumKey)
        throws Exception {
        return serviceListStatique.listEnumValuesJson(genericEnumKey);
    }

    @JsonView(value = SummaryJsonView.class)
    @GetMapping(value = "/countries", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<EcCountry>> getAllCountries() {
        List<EcCountry> countries = serviceListStatique.getListEcCountry();

        return new ResponseEntity<>(countries, HttpStatus.OK);
    }

    @GetMapping(value = "/regions", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<EcRegion>> getAllRegions() {
        List<EcRegion> regions = serviceListStatique.getListEcRegion();

        return new ResponseEntity<>(regions, HttpStatus.OK);
    }

    @GetMapping(value = "/cities/{country}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<List<EcCity>> getAllCities(@PathVariable("country") Integer ecCountryNum) {
        List<EcCity> cities = serviceListStatique.getListEcCityByCountry(ecCountryNum);

        return new ResponseEntity<>(cities, HttpStatus.OK);
    }

    @GetMapping(value = "/list-type-conteneur", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<TypeContainer[]> getListtypeConteneur() {
        TypeContainer[] typeConteneurs = serviceListStatique.getListTypeConteneur();
        return new ResponseEntity<>(typeConteneurs, HttpStatus.OK);
    }

    @GetMapping(value = "/service", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<ServiceType[]> getUserService() {
        ServiceType[] userServices = serviceListStatique.getListUserService();
        return new ResponseEntity<>(userServices, HttpStatus.OK);
    }

    @GetMapping(value = "/typeprocess", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getTypes() throws IOException {
        String typeProcess = "";
        typeProcess = Enumeration.TypeProcess.getListTypeProcess();

        return typeProcess;
    }

    @GetMapping(value = "/Typedemande", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getTypeDemande() throws IOException {
        String typeProcess = "";
        typeProcess = Enumeration.TypeDemande.getListTypeDemande();

        return typeProcess;
    }

    @GetMapping(value = "/listEligibility", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListEligibility() throws IOException {
        String listEligibility = "";
        listEligibility = Enumeration.AdresseEligibility.getListEligibility();

        return listEligibility;
    }

    @RequestMapping(value = "/transportRef", method = RequestMethod.GET)
    public String searchTransportRef(
        @RequestParam(required = false, value = "module") Integer module,
        @RequestParam(required = false, value = "term") String term)
        throws JsonProcessingException {
        String typeProcess = serviceListStatique.getListTransportRef(term, module);
        return typeProcess;
    }

    @RequestMapping(value = "/customerRefAndtransportRef", method = RequestMethod.GET)
    public String searchCustomerReferenceAndTransportRef(@RequestParam(required = false, value = "term") String term)
        throws JsonProcessingException {
        String typeProcess = serviceListStatique.getListtRefCustomerAndTraspo(term);
        return typeProcess;
    }

    @RequestMapping(value = "/customerReference", method = RequestMethod.GET)
    public String searchCustomerReference(
        @RequestParam(required = false, value = "module") Integer module,
        @RequestParam(required = false, value = "term") String term)
        throws JsonProcessingException {
        String typeProcess = serviceListStatique.getListCustomerReference(term, module);
        return typeProcess;
    }

    @RequestMapping(value = "/listCarrier", method = RequestMethod.GET)
    public String searchlistCarrier(@RequestParam(required = false, value = "term") String term)
        throws JsonProcessingException {
        String typeProcess = serviceListStatique.getListCarrier(term);
        return typeProcess;
    }

    @RequestMapping(value = "/ValueTtPsl", method = RequestMethod.GET)
    public String searchValueTtPsl() throws IOException {
        String value = TtEnumeration.TtPsl.getList();
        return value;
    }

    @RequestMapping(value = "/listCostCenter", method = RequestMethod.GET)
    public String searchlistCostCenter(@RequestParam(required = false, value = "term") String term)
        throws JsonProcessingException {
        String costcenter = serviceListStatique.getListCostCenter(term);
        return costcenter;
    }

    @GetMapping(value = "/listStatutTransportManagement", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListStatutTransportManagement() throws IOException {
        String listStatuts = "";
        listStatuts = Enumeration.StatutDemande.getListStatutDemandeBooking();

        return listStatuts;
    }

    @GetMapping(value = "/listTransitStatus", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListTransitStatus() throws IOException {
        String listStatuts = "";
        listStatuts = Enumeration.TransitStatus.getListTransitStatus();

        return listStatuts;
    }

    @GetMapping(value = "/listStatutOkMissing", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListStatutOkMissing() throws IOException {
        String listStatuts = "";
        listStatuts = Enumeration.StatutOkMissing.getListStatusOkMissing();

        return listStatuts;
    }

    @GetMapping(value = "/listInfosDocsStatut", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListInfosDocsStatut() throws IOException {
        String listStatuts = "";
        listStatuts = Enumeration.InfosDocsStatus.getListInfosDocsStatus();

        return listStatuts;
    }

    @RequestMapping(value = "/listDocuments", method = RequestMethod.GET)
    public Map<String, List<EbDemandeFichiersJoint>> searchlistDocuments(
        @RequestParam(required = false, value = "module") Integer module,
        @RequestParam(required = false, value = "ebUserNum") Integer ebUserNum,
        @RequestParam(required = false, value = "type") Integer type,
        @RequestParam(required = false, value = "demande") Integer demande)
        throws JsonProcessingException {
        return serviceListStatique.searchlistDocuments(module, ebUserNum, type, demande);
    }

    @RequestMapping(value = "/listDocumentsIncident", method = RequestMethod.GET)
    public Map<String, List<EbDemandeFichiersJoint>> listDocumentsIncident(
        @RequestParam(required = false, value = "module") Integer module,
        @RequestParam(required = false, value = "ebUserNum") Integer ebUserNum,
        @RequestParam(required = false, value = "type") Integer type,
        @RequestParam(required = false, value = "ebQmIncidentNum") Integer xEbQmIncident)
        throws JsonProcessingException {
        Map<String, List<EbDemandeFichiersJoint>> map = new HashMap<>();
        List<EbDemandeFichiersJoint> listFichierJoint = new ArrayList<>();

        EbUser connectedUser = this.connectedUserService.getCurrentUser();

        if (connectedUser == null) {
            connectedUser = userService.getEbUserByEbUserNum(ebUserNum);
        }

        if (Enumeration.Module.QUALITY_MANAGEMENT.getCode().equals(module))

            listFichierJoint = ebDemandeFichiersJointRepositorty.findAllByXEbQmIncidentAndModule(xEbQmIncident, module);

        for (EbDemandeFichiersJoint f: listFichierJoint) {
            if (!map.containsKey(f.getIdCategorie())) map.put(f.getIdCategorie(), new ArrayList<>());

            map.get(f.getIdCategorie()).add(f);
        }

        return map;
    }

    @RequestMapping(value = "/listDocumentsInvoice", method = RequestMethod.GET)
    public Map<String, List<EbDemandeFichiersJoint>> listDocumentsInvoice(
        @RequestParam(required = false, value = "module") Integer module,
        @RequestParam(required = false, value = "ebUserNum") Integer ebUserNum,
        @RequestParam(required = false, value = "ebInvoiceNum") Integer ebInvoiceNum)
        throws JsonProcessingException {
        Map<String, List<EbDemandeFichiersJoint>> map = new HashMap<>();
        List<EbDemandeFichiersJoint> listFichierJoint = new ArrayList<>();

        EbUser connectedUser = this.connectedUserService.getCurrentUser();

        if (connectedUser == null) {
            connectedUser = userService.getEbUserByEbUserNum(ebUserNum);
        }

        listFichierJoint = ebDemandeFichiersJointRepositorty.findAllByInvoiceNumAndModule(ebInvoiceNum, module);

        for (EbDemandeFichiersJoint f: listFichierJoint) {
            if (!map.containsKey(f.getIdCategorie())) map.put(f.getIdCategorie(), new ArrayList<>());

            map.get(f.getIdCategorie()).add(f);
        }

        return map;
    }

    @RequestMapping(value = "/listDocumentsDelivery", method = RequestMethod.GET)
    public Map<String, List<EbDemandeFichiersJoint>> listDocumentsDelivery(
        @RequestParam(required = false, value = "module") Integer module,
        @RequestParam(required = false, value = "ebUserNum") Integer ebUserNum,
        @RequestParam(required = false, value = "ebDelLivraisonNum") Integer ebDelLivraisonNum)
        throws JsonProcessingException {
        Map<String, List<EbDemandeFichiersJoint>> map = new HashMap<>();
        List<EbDemandeFichiersJoint> listFichierJoint = new ArrayList<>();

        EbUser connectedUser = this.connectedUserService.getCurrentUser();

        if (connectedUser == null) {
            connectedUser = userService.getEbUserByEbUserNum(ebUserNum);
        }

        listFichierJoint = ebDemandeFichiersJointRepositorty.findAllByDeliveryNumAndModule(ebDelLivraisonNum, module);

        for (EbDemandeFichiersJoint f: listFichierJoint) {
            if (!map.containsKey(f.getIdCategorie())) map.put(f.getIdCategorie(), new ArrayList<>());

            map.get(f.getIdCategorie()).add(f);
        }

        return map;
    }

    @RequestMapping(value = "/listChat", method = RequestMethod.GET)
    public List<ChatDTO> searchlistChat(
        @RequestParam(required = false, value = "module") Integer module,
        @RequestParam(required = false, value = "idFiche") Integer idFiche,
        @RequestParam(required = false, value = "idChatComponent") Integer idChatComponent) {
        return chatMapper.modelsToDtos(serviceListStatique.getListChat(module, idFiche, idChatComponent));
    }

    @RequestMapping(value = "/addChat", method = RequestMethod.POST)
    public ChatDTO addChat(@RequestBody ChatDTO chat) {
        return chatMapper.modelToDto(serviceListStatique.addChat(chatMapper.dtoToModel(chat)));
    }

    @RequestMapping(value = "/getCurrentDate", method = RequestMethod.GET)
    public Date getCurrentDate() {
        return new Date();
    }

		@GetMapping(value = "/listStatutPricing", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
		public String getListStatutPricing() throws IOException
		{
			EbUser connectedUser = connectedUserService.getCurrentUser();

			String listStatuts = "";
			listStatuts = Enumeration.StatutDemande.getListStatutDemandePricing(connectedUser);

			return listStatuts;
		}

    @GetMapping(value = "/listUserStatus", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListUserStatus() throws IOException {
        return Enumeration.UserStatus.getListStatutUser();
    }

    @GetMapping(value = "/listUserGroup", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<EbUserGroup> getListUserGroup() throws IOException {
        EbUser ebUser = connectedUserService.getCurrentUser();
        return userService.getListUserGroup(ebUser);
    }

    @GetMapping(value = "/listStatutOrder", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListStatutOrder() throws Exception {
        return StatusEnum.toJson(StatusOrder.class);
    }

    @GetMapping(value = "/listStatutDelivery", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListStatutDelivery() throws Exception {
        return StatusEnum.toJson(StatusDelivery.class);
    }

    @GetMapping(value = "/listStatutQM", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListStatutQM() throws IOException {
        String listStatuts = "";
        listStatuts = Enumeration.IncidentStatus.toJsonList();

        return listStatuts;
    }

    @GetMapping(value = "/listStatutFreightAudit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListStatutFreightAudit() throws IOException {
        String listStatuts = "";
        listStatuts = FAEnumeration.InvoiceStatus.toJsonList();

        return listStatuts;
    }

    @GetMapping(value = "/listTypeDemandeFreightAudit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getlistTypeDemandeFreightAudit() throws IOException {
        String listTypeDemande = "";
        listTypeDemande = Enumeration.TypeDemande.getListTypeDemande();

        return listTypeDemande;
    }

    @RequestMapping(value = "/saveStateTable", method = RequestMethod.POST)
    public @ResponseBody String saveStateTable(
        @RequestParam(required = true, value = "state") String state,
        @RequestParam(required = true, value = "ebUserNum") Integer ebUserNum,
        @RequestParam(required = true, value = "datatableCompId") Integer datatableCompId) {
        serviceListStatique.saveState(state, ebUserNum, datatableCompId);

        return "done";
    }

    @RequestMapping(value = "/loadStateTable", method = RequestMethod.GET)
    public @ResponseBody String loadStateTable(
        @RequestParam(required = true, value = "ebUserNum") Integer ebUserNum,
        @RequestParam(required = true, value = "datatableCompId") Integer datatableCompId) {
        EbDatatableState datatable = serviceListStatique.loadState(ebUserNum, datatableCompId);

        if (datatable != null) {
            return datatable.getState();
        }

        return "";
    }

    @GetMapping(value = "/listModeTransportFreightAudit", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getlistModeTransportFreightAudit() throws IOException {
        String listModeTransport = "";
        listModeTransport = Enumeration.ModeTransport.getListModeTransport();

        return listModeTransport;
    }

    @GetMapping(value = "/listMarchandiseClassGoodAsJSON", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListMarchandiseClassGoodAsJSON() throws IOException {
        String listClassGood = "";
        listClassGood = Enumeration.getListMarchandiseClassGoodAsJSON();

        return listClassGood;
    }
    /*
     * @GetMapping(value = "/selectListContact", produces =
     * MediaType.APPLICATION_JSON_UTF8_VALUE) public List<EbUser>
     * selectListContactFreightAudit(@RequestBody SearchCriteria criteria)
     * throws
     * IOException {
     * return userService.selectListContact(criteria);
     * }
     */

    @PostMapping(value = "/listStatutTracing")
    public List<Map<String, String>> getListStatutTracing(@RequestBody SearchCriteria criteria) throws IOException {
			return trackService.getListStatutTracing(criteria);
    }

    @GetMapping(value = "/listCurrency")
    public List<EcCurrency> getListEcCurrency() {
        return serviceListStatique.getListEcCurrency();
    }

    @GetMapping(value = "/listCategorieDeviation")
    public List<EbTtCategorieDeviation>
        getListCategorieDeviation(@RequestParam(required = false, value = "typeOfEvent") Integer typeOfEvent) {
        return serviceListStatique.getListCategorieDeviation(typeOfEvent);
    }

    @RequestMapping(value = "/listAgentBroker", method = RequestMethod.GET)
    public String searchlistAgentBroker(@RequestParam(required = false, value = "term") String term)
        throws JsonProcessingException {
        String typeProcess = serviceListStatique.getListAgent(term);
        return typeProcess;
    }

    @RequestMapping(value = "/clearanceId", method = RequestMethod.GET)
    public String searchClearanceId(@RequestParam(required = false, value = "term") String term)
        throws JsonProcessingException {
        String typeProcess = serviceListStatique.getListClearanceID(term);
        return typeProcess;
    }

    @RequestMapping(value = "/listShiper", method = RequestMethod.GET)
    public String searchlistShiper(@RequestParam(required = false, value = "term") String term)
        throws JsonProcessingException {
        String typeProcess = serviceListStatique.getListCarrier(term);
        return typeProcess;
    }

    @RequestMapping(value = "/listModeTransport", method = RequestMethod.GET)
    public String getListModeTransport() throws IOException {
        String value = serviceListStatique.getListModeTransport();
        return value;
    }

    @RequestMapping(value = "/listExtractors", method = RequestMethod.GET)
    public String getListExtractors() throws IOException {
        String value = serviceListStatique.getListExtractors();
        return value;
    }

    @RequestMapping(value = "/listChargeur", method = RequestMethod.GET)
    public String searchlistChargeur(@RequestParam(required = false, value = "term") String term)
        throws JsonProcessingException {
        String typeProcess = serviceListStatique.getListChargeur(term);
        return typeProcess;
    }

    @RequestMapping(value = "/autoCompleteList", method = RequestMethod.GET)
    public String getAutoCompleteList(
        @RequestParam(required = false, value = "term") String term,
        @RequestParam(required = true, value = "field") String field,
        @RequestParam(required = true, value = "module") Integer module)
        throws JsonProcessingException {
        String list = null;

        if (Enumeration.Module.ORDER_MANAGEMENT.getCode().equals(module)) {
            list = serviceListStatique.getAutoCompleteListOrder(term, field);
        }
        else if (Enumeration.Module.DELIVERY_MANAGEMENT.getCode().equals(module)) {
            list = serviceListStatique.getAutoCompleteListDelivery(term, field);
        }
        else if (Enumeration.Module.QUALITY_MANAGEMENT.getCode().equals(module)) {
            list = serviceListStatique.getAutoCompleteListMarchandise(term, field);
        }

        return list;
    }

    @GetMapping(value = "/list-email-param")
    public List<EbParamsMail> getListEmailParam() {
        return serviceListStatique.getListEmailParam();
    }

    @RequestMapping(value = "/list-type-transports", method = RequestMethod.GET)
    public Map<Integer, List<EbTypeTransport>> listTypeTransports() throws IOException {
        return serviceListStatique.getlistAllTypeTransport();
    }

    @GetMapping(value = "/listModule")
    public List<EcModule> getListEcModule() {
        return serviceListStatique.getListEcModule();
    }

    @GetMapping(value = "/listStatusByModule")
    public List<CodeLibelleDTO> getListStatusByModule(@RequestParam("moduleNum") Integer moduleNum) throws IOException {
        return serviceListStatique.getListStatusByModule(moduleNum);
    }

    @GetMapping(value = "/listSavedSearchAction")
    public String getListSavedSearchAction() throws IOException {
        return Enumeration.SavedSearchAction.toJsonList();
    }

    @GetMapping(value = "/listSavedSearchActionTrigger")
    public String getListSavedSearchActionTrigger() throws IOException {
        return Enumeration.SavedSearchActionTrigger.toJsonList();
    }

    @GetMapping(value = "/listFlagIcon")
    public String getListFlagIcon() throws IOException {
        return Enumeration.FlagIcon.toJsonList();
    }

    @GetMapping(value = "/list-psl")
    public String getListPsl() throws IOException {
        return TtEnumeration.TtPsl.getList();
    }

    @RequestMapping(value = "/list-fuseaux-horaire", method = RequestMethod.GET)
    public List<EcFuseauxHoraire> getAllFuseauxHoraire() throws IOException {
        return serviceListStatique.getListFuseauxHoraire();
    }

    @GetMapping(value = "/listBoutonAction")
    public String getListBoutonAction() throws IOException {
        return Enumeration.BoutonAction.toJsonList();
    }

    @GetMapping(value = "/listBoutonActionNewTarget")
    public String getListBoutonActionNewTarget() throws IOException {
        return Enumeration.BoutonActionNewTarget.toJsonList();
    }

    @GetMapping(value = "/listBoutonActionDashboardTarget")
    public String getListBoutonActionDashboardTarget() throws IOException {
        return Enumeration.BoutonActionDashboardTarget.toJsonList();
    }

    @GetMapping(value = "/listGroupeVentilation")
    public String getListGroupeVentilation() throws IOException {
        return Enumeration.QrGroupeVentilation.getListQrGroupeVentilation();
    }

    @GetMapping(value = "/listMarchandiseDangerousGood")
    public Map<Integer, String> getListMarchandiseDangerousGood() {
        return Enumeration.MarchandiseDangerousGood.getListMarchandiseDangerousGood();
    }

    @GetMapping(value = "/getListMarchandiseDangerousGoodAsJson", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListMarchandiseDangerousGoodAsJson() throws IOException {
        return Enumeration.MarchandiseDangerousGood.getListMarchandiseDangerousGoodAsJson();
    }

    @GetMapping(value = "/listMarchandiseClassGood")
    public double[] getListMarchandiseClassGood() {
        return Enumeration.MarchandiseClassGood;
    }

    @GetMapping(value = "/listFilesFormats")
    public List<EcMimetype> getListFilesFormats() {
        return serviceListStatique.getListMimeType();
    }

    @GetMapping(value = "/listActionChoose")
    public List<TransfertActionChoice> getListActionChoose() throws JsonProcessingException {
        return Enumeration.TransfertActionChoice.getListAction();
    }

    @GetMapping(value = "/listOwnerRequest")
    public List<EbUser> getxEbOwnerOfTheRequest(Integer module) {
        return serviceListStatique.getxEbOwnerOfTheRequest(module);
    }

    @GetMapping(value = "/listCRFields")
    public List<RuleOperandType> getListCRFields() {
        return CREnumeration.RuleOperandType.getRuleOperandType();
    }

    @GetMapping(value = "/list-of-nature-demande-transport")
    public NatureDemandeTransport[] getListOfNatureDemandeTransport() {
        return NatureDemandeTransport.values();
    }

    @PostMapping("consolidate-doc")
    public void consolidateDoc(@RequestBody SearchCriteria criteria, HttpServletResponse response)
        throws Docx4JException,
        IOException {

        try {
            File genFile = serviceListStatique
                .consolidateDocByEntityAndModule(criteria.getListModule(), criteria.getEbDemandeNum());

            response.setContentType("application/pdf");

            if (genFile != null) {
                OutputStream out = response.getOutputStream();
                FileInputStream in = new FileInputStream(genFile);
                IOUtils.copy(in, out);
                out.close();
                in.close();
            }

        } catch (Exception e) {
            LOGGER.error("could not download the file : " + e.getMessage());
            response.setStatus(org.apache.http.HttpStatus.SC_INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping(value = "/get-list-user")
    public @ResponseBody List<EbUser> getUserByCompagnie(@RequestBody SearchCriteria criteria) {
        return ebUserRepository.selectUserByCompagnie(criteria.getEbCompagnieNum());
    }

    @GetMapping(value = "/get-ebEtablissement")
    public List<EbEtablissement> getEbEtablissement() {
        return ebEtablissementRepository.selectEtablissement();
    }

    @GetMapping(value = "/get-all-company-name")
    public List<EbCompagnie> selectAllCompagnieName() {
        return ebCompagnieRepository.selectAllCompagnieName();
    }

    @GetMapping(value = "/listValuationType", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String getListValuationType() throws IOException {
        String listModeTransport = "";
        listModeTransport = Enumeration.StatutCarrier.getListValuationType();

        return listModeTransport;
    }
}
