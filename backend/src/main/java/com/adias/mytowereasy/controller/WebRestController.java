/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.repository.EbCompagnieRepository;


@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class WebRestController {
    @Autowired
    EbCompagnieRepository ebCompagnieRepository;

    @PreAuthorize("hasRole('BROKER')")
    @GetMapping("/ebCompagnies")
    public List<EbCompagnie> getAllCompagnies() {
        return ebCompagnieRepository.findAll();
    }

    @PostMapping("/ebCompagnies")
    public EbCompagnie createCompagnie(@Valid @RequestBody EbCompagnie todo) {
        return ebCompagnieRepository.save(todo);
    }

    @GetMapping(value = "/ebCompagnies/{id}")
    public ResponseEntity<EbCompagnie> getCompagnieById(@PathVariable("id") Integer id) {
        Optional<EbCompagnie> todo = ebCompagnieRepository.findById(id);

        if (!todo.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<>(todo.get(), HttpStatus.OK);
        }

    }

    @PutMapping(value = "/ebCompagnies/{id}")
    public ResponseEntity<EbCompagnie>
        updateCompagnie(@PathVariable("id") Integer id, @Valid @RequestBody EbCompagnie ebCompagnieUpdate) {
        Optional<EbCompagnie> ebCompagnie = ebCompagnieRepository.findById(id);

        if (!ebCompagnie.isPresent()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        ebCompagnie.get().setNom(ebCompagnieUpdate.getNom());

        ebCompagnie.get().setSiren(ebCompagnieUpdate.getSiren());

        EbCompagnie updatedCompagnie = ebCompagnieRepository.save(ebCompagnie.get());

        return new ResponseEntity<>(updatedCompagnie, HttpStatus.OK);
    }

    @DeleteMapping(value = "/ebCompagnies/{id}")
    public void deleteCompagnie(@PathVariable("id") Integer id) {
        ebCompagnieRepository.deleteById(id);
    }
}
