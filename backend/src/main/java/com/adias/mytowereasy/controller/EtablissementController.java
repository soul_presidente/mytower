/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbAdresse;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbCustomField;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbTypeFlux;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.TransfertUserObject;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/etablissement")
public class EtablissementController extends SuperControler {
    @PostMapping(value = "/get-etablisement")
    public EbEtablissement getEtablisement(@RequestBody SearchCriteria criteria) {
        EbEtablissement ebEtablissement = etablissementService.selectEbEtablissement(criteria);
        return ebEtablissement != null ? ebEtablissement : new EbEtablissement();
    }

    @PostMapping(value = "/get-list-etablisement", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<EbEtablissement> getListEtablisement(@RequestBody SearchCriteria criteria) {
        return etablissementService.selectListEbEtablissement(criteria);
    }

    @PostMapping(value = "/getlist-etablisement-ct")
    public List<EbEtablissement> getListEtablisementCt(@RequestBody SearchCriteria criteria) {
        return etablissementService.selectListEbEtablissementCt(criteria);
    }

    @PostMapping(value = "/add-etablissement")
    public EbEtablissement addEtablissement(@RequestBody EbEtablissement ebEtablissement) {
        return etablissementService.addEtablissement(ebEtablissement);
    }

    // -------------------------------------------------------------- //

    // @GetMapping(value = "/get-list-adresse-etablisement")
    @PostMapping(value = "/get-list-adresse-etablisement")
    public List<EbAdresse> getListAdresseEtablisement(@RequestBody SearchCriteria criteria) {
        return etablissementService.selectListEbAdresseEbEtablissement(criteria);
    }

    @PostMapping(value = "/add-adresse-etablisement")
    public EbAdresse addAdresseEtablisement(@RequestBody EbAdresse ebAdresse) {
        return etablissementService.addEbAdresseEbEtablissement(ebAdresse);
    }

    @PostMapping(value = "/edit-adresse-etablisement")
    public EbAdresse editAdresseEtablisement(@RequestBody EbAdresse ebAdresse) {
        return etablissementService.updateEbAdresseEbEtablissement(ebAdresse);
    }

    @PostMapping(value = "/delete-adresse-etablisement")
    public Boolean deleteAdresseEtablisement(@RequestBody EbAdresse ebAdresse) {
        return etablissementService.deleteEbAdresseEbEtablissement(ebAdresse);
    }

    @PostMapping(value = "/add-customfield")
    public EbCustomField addCustomField(@RequestBody EbCustomField ebCustomField) {
        return etablissementService.addEbCustomField(ebCustomField);
    }

    @PostMapping(value = "/get-customfield")
    public EbCustomField getCustomField(@RequestBody SearchCriteria criteria) {
        EbCustomField customField = etablissementService
            .selectEbCustomFieldByEbCompagnieNum(criteria.getEbCompagnieNum());
        return customField != null ? customField : new EbCustomField();
    }

    // -------------------------------------------------------------- //

    @PostMapping(value = "/get-data-preference")
    public Map<String, Object> getDataPreference(@RequestBody SearchCriteria criteria) {
        return etablissementService.getDataPreference(criteria);
    }

    @PostMapping(value = "/list-type-flux")
    public List<EbTypeFlux> getListTypeFlux(@RequestBody SearchCriteria criteria) {
        return ebTypeFluxService.getListTypeFlux(criteria);
    }

    @PostMapping(value = "/get-listCategories")
    public List<EbCategorie> getListExEtablissementCategories(@RequestBody SearchCriteria criteria) {
        return etablissementService.selectListEbCategorieByEtablissementNum(criteria.getEbEtablissementNum());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public void handle(HttpMessageNotReadableException e) {
        e.printStackTrace();
    }

    @PostMapping(value = "/get-category-custom")
    public Map<String, Object> getCategoryCustom(@RequestBody SearchCriteria criteria) {
        EbUser userconnected = connectedUserService.getCurrentUser();

        Map<String, Object> map = categoryService.getListCategoryAndCustomField(criteria, userconnected);
        return map;
    }

    @PostMapping(value = "/transfert-user")
    public boolean transfertUser(@RequestBody TransfertUserObject transfertUserObjet) throws Exception {
        etablissementService.transfertUser(transfertUserObjet);
        return true;
    }

    @PostMapping(value = "/list-etablissement-table")
    public Map<String, Object> getListEtablissement(@RequestBody SearchCriteria criteria) {
        criteria.setShowGlobals(true);
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);
        List<EbEtablissement> ebEtablissement = null;
        HashMap<String, Object> result = new HashMap<String, Object>();

        if (connectedUser.isSuperAdmin()) {
            criteria.setEbEtablissementNum(null);
        }

        ebEtablissement = etablissementService.selectListEbEtablissement(criteria);
        Long count = etablissementService.getListEtablissementCount(criteria);

        result.put("count", count);
        result.put("data", ebEtablissement);

        return result;
    }
}
