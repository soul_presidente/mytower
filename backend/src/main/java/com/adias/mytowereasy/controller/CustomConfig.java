/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.custom.EbFormField;
import com.adias.mytowereasy.model.custom.EbFormFieldCompagnie;
import com.adias.mytowereasy.model.custom.EbFormView;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "api/custom-setting")
@CrossOrigin("*")
public class CustomConfig extends SuperControler {
    @RequestMapping(value = "/save-list-champ", method = RequestMethod.POST)
    public EbFormFieldCompagnie configParams(@RequestBody EbFormFieldCompagnie ebFormFieldCompagnie) {
        customConfigService.updateListChamp(ebFormFieldCompagnie);
        return ebFormFieldCompagnie;
    }

    @PostMapping(value = "/update-list-champ")
    public Integer configParamsM(@RequestBody EbFormFieldCompagnie ebFormFieldCompagnie) {
        customConfigService.updateListChamp(ebFormFieldCompagnie);
        return 1;
    }

    @PostMapping(value = "/get-list-champ")
    public EbFormFieldCompagnie getParams(@RequestBody SearchCriteria criteria) {
        return customConfigService.getListChamp(criteria.getEbCompagnieNum());
    }

    @GetMapping(value = "/get-list-champformfield")
    public List<EbFormField> getListChampFormFieldGlobal() {
        return customConfigService.getListChampFormFieldGlobal();
    }

    @GetMapping(value = "/getListView")
    public List<EbFormView> getListView() {
        return customConfigService.getListView();
    }
}
