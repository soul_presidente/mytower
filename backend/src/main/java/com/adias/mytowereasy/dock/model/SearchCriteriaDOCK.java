package com.adias.mytowereasy.dock.model;

import com.adias.mytowereasy.utils.search.SearchCriteria;


public class SearchCriteriaDOCK extends SearchCriteria {
    private Integer ebEntrepotNum;

    public Integer getEbEntrepotNum() {
        return ebEntrepotNum;
    }

    public void setEbEntrepotNum(Integer ebEntrepotNum) {
        this.ebEntrepotNum = ebEntrepotNum;
    }
}
