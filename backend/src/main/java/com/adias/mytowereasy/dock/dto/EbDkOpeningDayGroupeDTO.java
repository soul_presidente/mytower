package com.adias.mytowereasy.dock.dto;

import java.util.List;

import com.adias.mytowereasy.dto.EbRangeHourDTO;


public class EbDkOpeningDayGroupeDTO {
    private List<EbDockDTO> ebdocks;

    private boolean open;

    private List<EbRangeHourDTO> hours;

    public EbDkOpeningDayGroupeDTO() {
    }

    public List<EbDockDTO> getEbdocks() {
        return ebdocks;
    }

    public void setEbdocks(List<EbDockDTO> ebdocks) {
        this.ebdocks = ebdocks;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public List<EbRangeHourDTO> getHours() {
        return hours;
    }

    public void setHours(List<EbRangeHourDTO> hours) {
        this.hours = hours;
    }
}
