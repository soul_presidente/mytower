package com.adias.mytowereasy.dock.dto;

import com.adias.mytowereasy.dock.model.EbDock;


public class EbDockDTO {
    private Integer ebDockNum;

    private String nom;

    private String reference;

    private String comment;

    private String couleur;

    private Integer ebEntrepotNum;

    private Boolean deleted;

    public EbDockDTO() {
    }

    public EbDockDTO(EbDock ebDock) {
        this.ebDockNum = ebDock.getEbDockNum();

        this.nom = ebDock.getNom();

        this.reference = ebDock.getReference();

        this.comment = ebDock.getComment();

        this.couleur = ebDock.getCouleur();
        if (ebDock.getEntrepot() != null) this.ebEntrepotNum = ebDock.getEntrepot().getEbEntrepotNum();

        this.deleted = ebDock.getDeleted();
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getEbDockNum() {
        return ebDockNum;
    }

    public void setEbDockNum(Integer ebDockNum) {
        this.ebDockNum = ebDockNum;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public Integer getEbEntrepotNum() {
        return ebEntrepotNum;
    }

    public void setEbEntrepotNum(Integer ebEntrepotNum) {
        this.ebEntrepotNum = ebEntrepotNum;
    }
}
