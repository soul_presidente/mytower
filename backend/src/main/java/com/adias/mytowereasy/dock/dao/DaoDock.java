package com.adias.mytowereasy.dock.dao;

import java.util.List;

import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.dock.model.SearchCriteriaDOCK;


public interface DaoDock {
    Long countDock(SearchCriteriaDOCK criteria);

    List<EbDock> searchDock(SearchCriteriaDOCK criteria);
}
