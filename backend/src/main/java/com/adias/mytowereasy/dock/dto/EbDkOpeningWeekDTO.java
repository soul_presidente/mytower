package com.adias.mytowereasy.dock.dto;

import java.util.ArrayList;
import java.util.List;

import com.adias.mytowereasy.dock.model.EbDkOpeningDay;
import com.adias.mytowereasy.dock.model.EbDkOpeningWeek;


public class EbDkOpeningWeekDTO {
    private Integer ebDkOpeningWeekNum;

    private String label;

    private Boolean isDefault;

    private Integer ebEntrepotNum;

    private List<EbDkOpeningDayDTO> days;

    private List<EbDkOpeningPeriodDTO> periods;

    public EbDkOpeningWeekDTO() {
    }

    public EbDkOpeningWeekDTO(EbDkOpeningWeek ebDkOpeningWeek, boolean getDays) {
        this.ebDkOpeningWeekNum = ebDkOpeningWeek.getEbDkOpeningWeekNum();

        this.label = ebDkOpeningWeek.getLabel();

        this.isDefault = ebDkOpeningWeek.getIsDefault();

        if (ebDkOpeningWeek.getEntrepot() != null) {
            EbEntrepotDTO entrepot = new EbEntrepotDTO(ebDkOpeningWeek.getEntrepot(), false);

            this.ebEntrepotNum = entrepot.getEbEntrepotNum();
        }

        if (getDays) {
            List<EbDkOpeningDayDTO> days = new ArrayList<EbDkOpeningDayDTO>();

            for (EbDkOpeningDay ebDkOpeningDay: ebDkOpeningWeek.getDays()) {
                days.add(new EbDkOpeningDayDTO(ebDkOpeningDay));
            }

            this.days = days;
        }

        /*
         * List<EbDkOpeningPeriodDto> periodDtos = new
         * ArrayList<EbDkOpeningPeriodDto>();
         * for (EbDkOpeningPeriod ebDkOpeningPeriod :
         * ebDkOpeningWeek.getPeriods()) {
         * periodDtos.add(new EbDkOpeningPeriodDto(ebDkOpeningPeriod));
         * }
         * this.periods = periodDtos;
         */
    }

    public List<EbDkOpeningPeriodDTO> getPeriods() {
        return periods;
    }

    public void setPeriods(List<EbDkOpeningPeriodDTO> periods) {
        this.periods = periods;
    }

    public Integer getEbDkOpeningWeekNum() {
        return ebDkOpeningWeekNum;
    }

    public void setEbDkOpeningWeekNum(Integer ebDkOpeningWeekNum) {
        this.ebDkOpeningWeekNum = ebDkOpeningWeekNum;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Integer getEbEntrepotNum() {
        return ebEntrepotNum;
    }

    public void setEbEntrepotNum(Integer ebEntrepotNum) {
        this.ebEntrepotNum = ebEntrepotNum;
    }

    public List<EbDkOpeningDayDTO> getDays() {
        return days;
    }

    public void setDays(List<EbDkOpeningDayDTO> days) {
        this.days = days;
    }
}
