package com.adias.mytowereasy.dock.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.dock.model.EbEntrepot;
import com.adias.mytowereasy.repository.CommonRepository;


@Repository
public interface EbEntrepotRepository extends CommonRepository<EbEntrepot, Integer> {
    @Transactional
    @Modifying
    @Query("update EbEntrepot e set e.deleted = true where e.ebEntrepotNum = ?1")
    public void deleteWithHist(Integer ebEntrepotNum);

    @Query("SELECT e FROM EbEntrepot e where e.deleted = ?1 and e.etablissement.ebEtablissementNum = ?2")
    public List<EbEntrepot> findAllByEtablissement(Boolean deleted, Integer ebEtablissementNum);

    @Query("SELECT e FROM EbEntrepot e where e.deleted = ?1 and e.etablissement.ebCompagnie.ebCompagnieNum = ?2")
    public List<EbEntrepot> findAllByCompany(Boolean deleted, Integer ebCompagnieNum);

    @Query("SELECT e FROM EbEntrepot e where e.deleted = ?1 ")
    public List<EbEntrepot> findAll(Boolean deleted);

    EbEntrepot findByEbEntrepotNum(Integer ebEntrepottNum);
}
