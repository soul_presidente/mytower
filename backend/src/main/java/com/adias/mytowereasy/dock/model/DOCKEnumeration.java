package com.adias.mytowereasy.dock.model;

import com.adias.mytowereasy.util.GenericEnum;


public class DOCKEnumeration {
    public enum OpeningPeriodType implements GenericEnum {
        NORMAL(0, "NORMAL"), EXCEPTIONAL(1, "EXCEPTIONAL");

        private Integer code;
        private String key;

        private OpeningPeriodType(Integer code, String key) {
            this.code = code;
            this.key = key;
        }

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }
    }
}
