package com.adias.mytowereasy.dock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.dock.model.QEbDock;
import com.adias.mytowereasy.dock.model.QEbEntrepot;
import com.adias.mytowereasy.dock.model.SearchCriteriaDOCK;
import com.adias.mytowereasy.service.ConnectedUserService;


@Component
@Transactional
public class DaoDockImpl implements DaoDock {
    @PersistenceContext
    EntityManager em;
    @Autowired
    ConnectedUserService connectedUserService;

    QEbDock qEbDock = QEbDock.ebDock;
    QEbEntrepot qEbDockEntrepot = new QEbEntrepot("qEbDockEntrepot");

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbDock> searchDock(SearchCriteriaDOCK criteria) {
        List<EbDock> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbDock> query = new JPAQuery<>(em);

            query
                .select(
                    QEbDock
                        .create(qEbDock.ebDockNum, qEbDock.nom, qEbDock.reference, qEbDock.comment, qEbDock.couleur));
            query = getDockGlobalWhere(query, where, criteria);
            query.distinct().from(qEbDock);
            query = getDockGlobalJoin(query, where, criteria);
            query.where(where);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbDock, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbDock.reference.asc());
            }

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long countDock(SearchCriteriaDOCK criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbDock> query = new JPAQuery<EbDock>(em);
            BooleanBuilder where = new BooleanBuilder();

            query.distinct().from(qEbDock);
            query = getDockGlobalWhere(query, where, criteria);
            query.where(where);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    private JPAQuery getDockGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteriaDOCK criteria) {
        query.leftJoin(qEbDock.entrepot(), qEbDockEntrepot);
        return query;
    }

    @SuppressWarnings({
        "rawtypes"
    })
    private JPAQuery getDockGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteriaDOCK criteria) {
        where.and(qEbDock.deleted.eq(false));

        if (criteria.getEbEntrepotNum() != null) {
            where.and(qEbDock.entrepot().ebEntrepotNum.eq(criteria.getEbEntrepotNum()));
        }

        if (StringUtils.isNotBlank(criteria.getSearchterm())) {
            where
                .andAnyOf(
                    qEbDock.nom.toLowerCase().contains(criteria.getSearchterm().toLowerCase()),
                    qEbDock.reference.toLowerCase().contains(criteria.getSearchterm().toLowerCase()),
                    qEbDock.comment.toLowerCase().contains(criteria.getSearchterm().toLowerCase()));
        }

        return query;
    }
}
