package com.adias.mytowereasy.dock.repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.adias.mytowereasy.dock.model.EbDkOpeningDay;
import com.adias.mytowereasy.dock.model.EbDkOpeningWeek;
import com.adias.mytowereasy.model.Enumeration.JourEnum;
import com.adias.mytowereasy.repository.CommonRepository;
import com.adias.mytowereasy.rsch.algo.DockAvailabilityAlgo;


public interface EbDkOpeningDayRepository extends CommonRepository<EbDkOpeningDay, Integer> {
    @Query("select new com.adias.mytowereasy.rsch.algo.DockAvailabilityAlgo(dock ,rh,op,od.open) FROM ExDkOpeningHour  oh, "
        + "EbRangeHour rh,EbDock dock, EbDkOpeningDay od, EbDkOpeningWeek ow "
        + "LEFT JOIN  EbDkOpeningPeriod op on op.week.ebDkOpeningWeekNum=ow.ebDkOpeningWeekNum "
        + "where  oh.pKExDkOpeningHourId.x_opening_hours=od.openingDayNum "
        + "and oh.pKExDkOpeningHourId.x_eb_dock= dock.ebDockNum "
        + "and rh.openingDay.openingDayNum in (select od.openingDayNum from EbDkOpeningDay od where od.day= :day and open=true ) "
        + "and od.openingDayNum=rh.openingDay.openingDayNum " + "and ow.ebDkOpeningWeekNum=od.week.ebDkOpeningWeekNum  "
        + "and ((:date BETWEEN op.startDate and op.endDate)or (op.startDate is NULL and op.endDate is null)) "
        + "and dock.entrepot.ebEntrepotNum IN (:listEntrepotNum)  "
        + "Group By (dock.ebDockNum,rh.rangeHoursNum,rh.startHour,od.week.ebDkOpeningWeekNum,ow.ebDkOpeningWeekNum,op.ebDkOpeningPeriodNum,od.open)")
    public List<DockAvailabilityAlgo> findAllRH(
        @Param("day") JourEnum day,
        @Param("date") Date date,
        @Param("listEntrepotNum") Set<Integer> listEntrepotNum);

    public List<EbDkOpeningDay> findAllByWeek(EbDkOpeningWeek ebDkOpeningWeek);
}
