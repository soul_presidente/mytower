package com.adias.mytowereasy.dock.model;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;

import com.querydsl.core.annotations.QueryProjection;


@Entity
@Table(name = "eb_dock", schema = "work")
public class EbDock {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebDockNum;

    private String nom;

    @Column
    private String reference;

    @Column
    private String comment;

    @Column
    private String couleur;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_entrepot", insertable = true)
    private EbEntrepot entrepot;

    @Column
    @ColumnDefault("false")
    private Boolean deleted;

    public EbDock() {
    }

    public EbDock(Integer i) {
        this.ebDockNum = i;
    }

    public EbDock(Integer ebDockNum, String nom) {
        super();
        this.ebDockNum = ebDockNum;
        this.nom = nom;
    }

    @QueryProjection
    public EbDock(Integer ebDockNum, String nom, String reference, String comment, String couleur) {
        this.ebDockNum = ebDockNum;
        this.nom = nom;
        this.reference = reference;
        this.comment = comment;
        this.couleur = couleur;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getEbDockNum() {
        return ebDockNum;
    }

    public void setEbDockNum(Integer ebDockNum) {
        this.ebDockNum = ebDockNum;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public EbEntrepot getEntrepot() {
        return entrepot;
    }

    public void setEntrepot(EbEntrepot entrepot) {
        this.entrepot = entrepot;
    }

    @PrePersist
    void prePersist() {

        if (this.deleted == null) {
            this.deleted = false;
        }

    }
}
