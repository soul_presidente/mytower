package com.adias.mytowereasy.dock.dto;

import java.util.ArrayList;
import java.util.List;

import com.adias.mytowereasy.dock.model.EbDkOpeningDay;
import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.dto.EbRangeHourDTO;
import com.adias.mytowereasy.model.Enumeration.JourEnum;


public class EbDkOpeningDayDTO {
    private Integer openingDayNum;

    private JourEnum day;

    private List<EbDockDTO> ebdocks;

    private boolean open;

    private List<EbRangeHourDTO> hours;

    private Integer ebDkOpeningWeekNum;

    public EbDkOpeningDayDTO() {
    }

    public EbDkOpeningDayDTO(EbDkOpeningDay openingDay) {
        this.openingDayNum = openingDay.getOpeningDayNum();

        this.day = openingDay.getDay();

        List<EbDockDTO> ebdockDtos = new ArrayList<EbDockDTO>();

        for (EbDock ebDockDto: openingDay.getEbdocks()) {
            ebdockDtos.add(new EbDockDTO(ebDockDto));
        }

        this.ebdocks = ebdockDtos;

        this.open = openingDay.isOpen();

        if (openingDay.getHours() != null) {
            this.hours = new ArrayList<>();
            openingDay.getHours().forEach(h -> {
                this.hours.add(new EbRangeHourDTO(h));
            });
        }

        if (openingDay.getWeek() != null) {
            this.ebDkOpeningWeekNum = openingDay.getWeek().getEbDkOpeningWeekNum();
        }

    }

    public List<EbDockDTO> getEbdocks() {
        return ebdocks;
    }

    public void setEbdocks(List<EbDockDTO> ebdocks) {
        this.ebdocks = ebdocks;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public List<EbRangeHourDTO> getHours() {
        return hours;
    }

    public void setHours(List<EbRangeHourDTO> hours) {
        this.hours = hours;
    }

    public Integer getEbDkOpeningWeekNum() {
        return ebDkOpeningWeekNum;
    }

    public void setEbDkOpeningWeekNum(Integer ebDkOpeningWeekNum) {
        this.ebDkOpeningWeekNum = ebDkOpeningWeekNum;
    }

    public Integer getOpeningDayNum() {
        return openingDayNum;
    }

    public void setOpeningDayNum(Integer openingDayNum) {
        this.openingDayNum = openingDayNum;
    }

    public JourEnum getDay() {
        return day;
    }

    public void setDay(JourEnum day) {
        this.day = day;
    }
}
