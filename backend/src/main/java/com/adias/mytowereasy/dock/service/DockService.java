package com.adias.mytowereasy.dock.service;

import java.util.List;

import com.adias.mytowereasy.dock.dto.EbDkOpeningPeriodDTO;
import com.adias.mytowereasy.dock.dto.EbDkOpeningWeekDTO;
import com.adias.mytowereasy.dock.dto.EbDockDTO;
import com.adias.mytowereasy.dock.dto.EbEntrepotDTO;
import com.adias.mytowereasy.dock.model.EbDkOpeningDay;
import com.adias.mytowereasy.dock.model.EbDkOpeningPeriod;
import com.adias.mytowereasy.dock.model.EbDkOpeningWeek;
import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DockService {
    public List<EbEntrepotDTO> getListEntrepot(Boolean bindDocks);

    public EbEntrepotDTO addEntrepot(EbEntrepotDTO ebEntrepot);

    public void deleteEntrepot(Integer ebEntrepotNum);

    public List<EbEntrepotDTO> getListEbEntrepot(SearchCriteria criteria);

    public Long getListEbEntrepotCount(SearchCriteria criteria);

    public EbDock addDock(EbDockDTO ebDock);

    public List<EbDockDTO> getListDock(Integer ebEntrepotNum);

    public void deleteDock(Integer ebDockNum);

    public List<EbDkOpeningDay> getListOpeningDay();

    public void addOpeningDays(List<EbDkOpeningDay> listOpeningDay);

    public void deleteOpeningDay(EbDkOpeningDay openingDay);

    public List<EbDkOpeningWeekDTO> getListOpeningWeek(Integer ebEntrepotNum);

    public EbDkOpeningWeekDTO getBaseOpeningWeek(Integer ebEntrepotNum, Integer openingWeekNum);

    public List<EbDkOpeningWeekDTO> getOpeningWeeks(Integer ebEntrepotNum);

    public EbDkOpeningWeek saveOpeningWeek(EbDkOpeningWeekDTO openingWeekDto);

    public EbDkOpeningPeriod saveOpeningPeriod(EbDkOpeningPeriodDTO openingPeriod);

    public void deleteOpeningPeriod(Integer ebDkOpeningPeriodNum);

    public List<EbDkOpeningPeriod> getListOpeningPeriod(Integer ebDkOpeningWeekNum);

    public List<EbDkOpeningPeriod> getListOpeningPeriodByEntrepot(Integer ebEntrepotNum);

    public void deleteOpeningWeek(EbDkOpeningWeek ebDkOpeningWeek);

    List<EbDockDTO> convertDockEntitiesToDTOs(List<EbDock> entities);
}
