package com.adias.mytowereasy.dock.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.adias.mytowereasy.dock.model.EbDkOpeningPeriod;
import com.adias.mytowereasy.repository.CommonRepository;


public interface EbDkOpeningPeriodRepository extends CommonRepository<EbDkOpeningPeriod, Integer> {
    @Query("SELECT p FROM EbDkOpeningPeriod p where p.week.ebDkOpeningWeekNum = ?1")
    public List<EbDkOpeningPeriod> findAllByWeek(Integer ebDkOpeningWeekNum);

    @Query("SELECT p FROM EbDkOpeningPeriod p where p.week.entrepot.ebEntrepotNum = ?1 ")
    public List<EbDkOpeningPeriod> findAllByEntrepot(Integer ebEntrepotNum);
}
