package com.adias.mytowereasy.dock.dao;

import java.util.List;

import com.adias.mytowereasy.dock.model.EbEntrepot;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoEntrepot {
    public List<EbEntrepot> listEntrepots(SearchCriteria criteria);

    public Long getListEbEntrepotCount(SearchCriteria criteria);
}
