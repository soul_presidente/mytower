package com.adias.mytowereasy.dock.model;

import java.util.Date;

import javax.persistence.*;


@Entity
@Table(name = "eb_dk_opening_period", schema = "work")
public class EbDkOpeningPeriod {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebDkOpeningPeriodNum;

    private String label;

    private Date startDate;

    private Date endDate;

    private Integer typePeriod;

    // @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_opening_week", insertable = true)
    private EbDkOpeningWeek week;

    public EbDkOpeningPeriod() {
    }

    public EbDkOpeningPeriod(Integer type) {
        super();
        this.typePeriod = type;
    }

    public Integer getEbDkOpeningPeriodNum() {
        return ebDkOpeningPeriodNum;
    }

    public void setEbDkOpeningPeriodNum(Integer ebDkOpeningPeriodNum) {
        this.ebDkOpeningPeriodNum = ebDkOpeningPeriodNum;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public EbDkOpeningWeek getWeek() {
        return week;
    }

    public void setWeek(EbDkOpeningWeek week) {
        this.week = week;
    }

    public Integer getTypePeriod() {
        return typePeriod;
    }

    public void setTypePeriod(Integer typePeriod) {
        this.typePeriod = typePeriod;
    }
}
