package com.adias.mytowereasy.dock.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.dock.model.EbDkOpeningWeek;
import com.adias.mytowereasy.dock.model.EbEntrepot;
import com.adias.mytowereasy.repository.CommonRepository;


@Repository
public interface EbDkOpeningWeekRepository extends CommonRepository<EbDkOpeningWeek, Integer> {
    public List<EbDkOpeningWeek> findAllByEntrepot(EbEntrepot ebEntrepot);

    @Query("SELECT w  FROM EbDkOpeningWeek w  where w.isDefault = true and w.entrepot = ?1")
    public EbDkOpeningWeek findBaseWeekByEntrepot(EbEntrepot ebEntrepot);
}
