package com.adias.mytowereasy.dock.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import com.adias.mytowereasy.dock.dto.EbDkOpeningDayDTO;
import com.adias.mytowereasy.dock.dto.EbDockDTO;
import com.adias.mytowereasy.model.EbRangeHour;
import com.adias.mytowereasy.model.Enumeration.JourEnum;


@Entity
@Table(name = "eb_dk_opening_day", schema = "work")
public class EbDkOpeningDay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer openingDayNum;

    private JourEnum day;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "ex_dock_opening_hours", schema = "work", joinColumns = {
        @JoinColumn(name = "x_opening_hours")
    }, inverseJoinColumns = {
        @JoinColumn(name = "x_eb_dock")
    })
    private List<EbDock> ebdocks;

    private boolean open;

    @OneToMany(mappedBy = "openingDay", fetch = FetchType.LAZY)
    private List<EbRangeHour> hours;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_opening_week", insertable = true)
    private EbDkOpeningWeek week;

    public EbDkOpeningDay() {
    }

    public EbDkOpeningDay(EbDkOpeningDayDTO openingDayDto) {
        this.openingDayNum = openingDayDto.getOpeningDayNum();

        this.day = openingDayDto.getDay();

        List<EbDock> docks = new ArrayList<EbDock>();

        for (EbDockDTO ebDockDto: openingDayDto.getEbdocks()) {
            docks.add(new EbDock(ebDockDto.getEbDockNum()));
        }

        this.ebdocks = docks;

        this.open = openingDayDto.isOpen();

        if (openingDayDto.getHours() != null) {
            this.hours = new ArrayList<>();
            openingDayDto.getHours().forEach(h -> {
                this.hours.add(new EbRangeHour(h));
            });
        }

        if (openingDayDto.getEbDkOpeningWeekNum() != null) {
            EbDkOpeningWeek week = new EbDkOpeningWeek();
            week.setEbDkOpeningWeekNum(openingDayDto.getEbDkOpeningWeekNum());
            this.week = week;
        }

    }

    public EbDkOpeningWeek getWeek() {
        return week;
    }

    public void setWeek(EbDkOpeningWeek week) {
        this.week = week;
    }

    public Integer getOpeningDayNum() {
        return openingDayNum;
    }

    public void setOpeningDayNum(Integer openingDayNum) {
        this.openingDayNum = openingDayNum;
    }

    public JourEnum getDay() {
        return day;
    }

    public void setDay(JourEnum day) {
        this.day = day;
    }

    public List<EbDock> getEbdocks() {
        return ebdocks;
    }

    public void setEbdocks(List<EbDock> ebdocks) {
        this.ebdocks = ebdocks;
    }

    public boolean isOpen() {
        return open;
    }

    public boolean getOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public List<EbRangeHour> getHours() {
        return hours;
    }

    public void setHours(List<EbRangeHour> hours) {
        this.hours = hours;
    }
}
