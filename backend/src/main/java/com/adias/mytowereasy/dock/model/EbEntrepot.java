package com.adias.mytowereasy.dock.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.dock.dto.EbEntrepotDTO;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;


@Entity
@Table(name = "eb_entrepot", schema = "work")
@JsonIgnoreProperties({
    "docks", "ebEtablissementNum"
})
public class EbEntrepot {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebEntrepotNum;
    @Column
    private String reference;
    @Column
    private String libelle;
    @Column()
    @ColumnDefault("false")
    private Boolean deleted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissemnt", insertable = true)
    private EbEtablissement etablissement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", updatable = false, insertable = true)
    private EbCompagnie xEbCompagnie;

    @OneToMany(mappedBy = "entrepot", fetch = FetchType.LAZY)
    private List<EbDock> listDocks;

    public EbEntrepot() {
    }

    public EbEntrepot(EbEntrepotDTO ebEntrepotDto) {
        this.ebEntrepotNum = ebEntrepotDto.getEbEntrepotNum();

        this.reference = ebEntrepotDto.getReference();
        this.libelle = ebEntrepotDto.getLibelle();
        this.deleted = ebEntrepotDto.getDeleted();

        if (ebEntrepotDto.getEbEtablissementNum() != null) {
            this.etablissement = new EbEtablissement(ebEntrepotDto.getEbEtablissementNum());
        }

        this.etablissement = null;
        this.listDocks = null;
    }

    public EbEntrepot(Integer ebEntrepotNum) {
        this.ebEntrepotNum = ebEntrepotNum;
    }

    @QueryProjection
    public EbEntrepot(Integer ebEntrepotNum, String reference, String libelle) {
        super();
        this.ebEntrepotNum = ebEntrepotNum;
        this.reference = reference;
        this.libelle = libelle;
    }

    @QueryProjection
    public EbEntrepot(
        Integer ebEntrepotNum,
        String reference,
        String libelle,
        Boolean deleted,
        EbEtablissement etablissement
    // EbCompagnie compagnie
    ) {
        super();
        this.ebEntrepotNum = ebEntrepotNum;
        this.reference = reference;
        this.libelle = libelle;
        this.deleted = deleted;
        this.etablissement = etablissement;
        // this.xEbCompagnie = compagnie;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Integer getEbEntrepotNum() {
        return ebEntrepotNum;
    }

    public void setEbEntrepotNum(Integer ebEntrepotNum) {
        this.ebEntrepotNum = ebEntrepotNum;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public EbEtablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(EbEtablissement etablissement) {
        this.etablissement = etablissement;
    }

    public List<EbDock> getListDocks() {
        return listDocks;
    }

    public void setListDocks(List<EbDock> listDocks) {
        this.listDocks = listDocks;
    }
}
