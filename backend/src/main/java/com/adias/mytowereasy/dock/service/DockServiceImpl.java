package com.adias.mytowereasy.dock.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dock.dto.EbDkOpeningDayDTO;
import com.adias.mytowereasy.dock.dto.EbDkOpeningPeriodDTO;
import com.adias.mytowereasy.dock.dto.EbDkOpeningWeekDTO;
import com.adias.mytowereasy.dock.dto.EbDockDTO;
import com.adias.mytowereasy.dock.dto.EbEntrepotDTO;
import com.adias.mytowereasy.dock.model.EbDkOpeningDay;
import com.adias.mytowereasy.dock.model.EbDkOpeningPeriod;
import com.adias.mytowereasy.dock.model.EbDkOpeningWeek;
import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.dock.model.EbEntrepot;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbRangeHour;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class DockServiceImpl extends MyTowerService implements DockService {
    @Override
    public EbDock addDock(EbDockDTO dto) {
        EbDock ebDock;

        if (dto.getEbDockNum() != null) {
            ebDock = ebDockRepository.findById(dto.getEbDockNum()).get();
        }
        else {
            ebDock = new EbDock();
        }

        ebDock.setNom(dto.getNom());
        ebDock.setReference(dto.getReference());
        ebDock.setComment(dto.getComment());
        ebDock.setCouleur(dto.getCouleur());

        if (dto.getEbEntrepotNum() != null) {
            EbEntrepot ent = new EbEntrepot();
            ent.setEbEntrepotNum(dto.getEbEntrepotNum());
            ebDock.setEntrepot(ent);
        }

        ebDock.setDeleted(false);
        if (dto.getDeleted() != null) ebDock.setDeleted(dto.getDeleted());

        return ebDockRepository.save(ebDock);
    }

    @Override
    public List<EbDockDTO> getListDock(Integer ebEntrepotNum) {
        // List<EbDock> listDocks = ebDockRepository.findAll();
        EbEntrepot ent = new EbEntrepot();
        ent.setEbEntrepotNum(ebEntrepotNum);
        List<EbDock> listDocks = ebDockRepository.findAllByEntrepot(ent);
        List<EbDockDTO> listDockDtos = new ArrayList<EbDockDTO>();

        for (EbDock dock: listDocks) {
            listDockDtos.add(new EbDockDTO(dock));
        }

        return listDockDtos;
    }

    @Override
    public List<EbEntrepotDTO> getListEntrepot(Boolean bindDocks) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        List<EbEntrepotDTO> list = new ArrayList<>();
        List<EbEntrepot> dbList = null;

        if (connectedUser != null) if (!connectedUser.isControlTower()) {

            if (connectedUser.isSuperAdmin()) {
                dbList = ebEntrepotRepository
                    .findAllByCompany(false, connectedUser.getEbCompagnie().getEbCompagnieNum());
            }
            else {
                dbList = ebEntrepotRepository
                    .findAllByEtablissement(false, connectedUser.getEbEtablissement().getEbEtablissementNum());
            }

        }
        else {
            dbList = ebEntrepotRepository.findAll(false);
        }

        dbList.forEach(entr -> {
            list.add(new EbEntrepotDTO(entr, bindDocks));
        });
        return list;
    }

    @Override
    public EbEntrepotDTO addEntrepot(EbEntrepotDTO ebEntrepot) {
        EbEntrepot entrepot = new EbEntrepot(ebEntrepot);

        if (ebEntrepot.getEbEtablissementNum() == null) {
            entrepot
                .setEtablissement(
                    new EbEtablissement(
                        connectedUserService.getCurrentUser().getEbEtablissement().getEbEtablissementNum()));
            entrepot
                .setxEbCompagnie(
                    new EbCompagnie(connectedUserService.getCurrentUser().getEbCompagnie().getEbCompagnieNum()));
        }
        else {
            entrepot.setEtablissement(ebEtablissementRepository.getOne(ebEntrepot.getEbEtablissementNum()));
            entrepot.setxEbCompagnie(ebCompagnieRepository.getOne(ebEntrepot.getEbCompagnieNum()));
        }

        return new EbEntrepotDTO(ebEntrepotRepository.save(entrepot), false);
    }

    @Override
    public void deleteEntrepot(Integer ebEntrepotNum) {
        // ebEntrepotRepository.deleteById(ebEntrepotNum);
        ebEntrepotRepository.deleteWithHist(ebEntrepotNum);
    }

    @Override
    public void deleteDock(Integer ebDockNum) {
        // ebDockRepository.deleteById(ebDockNum);
        ebDockRepository.deleteWithHist(ebDockNum);
        ;
    }

    @Override
    public List<EbEntrepotDTO> getListEbEntrepot(SearchCriteria criteria) {
        List<EbEntrepotDTO> list = new ArrayList<>();
        doaEntrepot.listEntrepots(criteria).forEach(entr -> {
            list.add(new EbEntrepotDTO(entr, false));
        });
        return list;
    }

    @Override
    public Long getListEbEntrepotCount(SearchCriteria criteria) {
        return doaEntrepot.getListEbEntrepotCount(criteria);
    }

    @Override
    public List<EbDkOpeningDay> getListOpeningDay() {
        List<EbDkOpeningDay> openingDays = ebDkOpeningDayRepository.findAll();

        for (EbDkOpeningDay openingDay: openingDays) {
            openingDay.setHours(ebRangeHoursRepository.findAllByOpeningDay(openingDay));
        }

        return openingDays;
    }

    @Override
    public void addOpeningDays(List<EbDkOpeningDay> listOpeningDay) {

        for (EbDkOpeningDay openingDay: listOpeningDay) {
            EbDkOpeningDay day = ebDkOpeningDayRepository.save(openingDay);

            for (EbRangeHour rangeHours: openingDay.getHours()) {
                rangeHours.setOpeningDay(day);
                ebRangeHoursRepository.save(rangeHours);
            }

        }

    }

    @Override
    public void deleteOpeningDay(EbDkOpeningDay openingDay) {
        ebRangeHoursRepository.deleteByOpeningDay(openingDay);
        ebDkOpeningDayRepository.deleteById(openingDay.getOpeningDayNum());
    }

    @Override
    public List<EbDkOpeningWeekDTO> getListOpeningWeek(Integer ebEntrepotNum) {
        // List<EbDkOpeningWeek> listOpeningWeek =
        // ebDkOpeningWeekRepository.findAll();
        EbEntrepot ent = new EbEntrepot();
        ent.setEbEntrepotNum(ebEntrepotNum);
        List<EbDkOpeningWeek> listOpeningWeek = ebDkOpeningWeekRepository.findAllByEntrepot(ent);

        for (EbDkOpeningWeek openingWeek: listOpeningWeek) {
            List<EbDkOpeningDay> openingDays = ebDkOpeningDayRepository.findAllByWeek(openingWeek);
            openingWeek.setDays(openingDays);

            for (EbDkOpeningDay openingDay: openingDays) {
                openingDay.setHours(ebRangeHoursRepository.findAllByOpeningDay(openingDay));
            }

        }

        /*
         * for (EbDkOpeningWeek openingWeek : listOpeningWeek) {
         * List<EbDkOpeningPeriod> openingPeriods =
         * ebOpeningPeriodRepository.findAllByWeek(openingWeek);
         * openingWeek.setPeriods(openingPeriods);
         * }
         */
        List<EbDkOpeningWeekDTO> listOpeningWeekDtos = new ArrayList<EbDkOpeningWeekDTO>();

        for (EbDkOpeningWeek openingWeek: listOpeningWeek) {
            EbDkOpeningWeekDTO openingWeekDto = new EbDkOpeningWeekDTO(openingWeek, true);
            listOpeningWeekDtos.add(openingWeekDto);
            List<EbDkOpeningPeriod> openingPeriods = ebDkOpeningPeriodRepository
                .findAllByWeek(openingWeek.getEbDkOpeningWeekNum());

            List<EbDkOpeningPeriodDTO> ebDkOpeningPeriodDtos = new ArrayList<EbDkOpeningPeriodDTO>();

            for (EbDkOpeningPeriod ebDkOpeningPeriod: openingPeriods) {
                EbDkOpeningPeriodDTO ebDkOpeningPeriodDto = new EbDkOpeningPeriodDTO(ebDkOpeningPeriod);
                ebDkOpeningPeriodDtos.add(ebDkOpeningPeriodDto);
            }

            openingWeekDto.setPeriods(ebDkOpeningPeriodDtos);

            List<EbDkOpeningDay> openingDays = ebDkOpeningDayRepository.findAllByWeek(openingWeek);
            List<EbDkOpeningDayDTO> openingDayDtos = new ArrayList<EbDkOpeningDayDTO>();

            for (EbDkOpeningDay openingDay: openingDays) {
                openingDay.setHours(ebRangeHoursRepository.findAllByOpeningDay(openingDay));
                EbDkOpeningDayDTO openingDayDto = new EbDkOpeningDayDTO(openingDay);
                openingDayDtos.add(openingDayDto);
            }

            openingWeekDto.setDays(openingDayDtos);
        }

        return listOpeningWeekDtos;
    }

    @Override
    public EbDkOpeningWeekDTO getBaseOpeningWeek(Integer ebEntrepotNum, Integer openingWeekNum) {
        EbDkOpeningWeekDTO openingWeekDto = null;
        EbDkOpeningWeek openingWeek = null;

        if (ebEntrepotNum != null) {
            EbEntrepot ent = new EbEntrepot();
            ent.setEbEntrepotNum(ebEntrepotNum);
            openingWeek = ebDkOpeningWeekRepository.findBaseWeekByEntrepot(ent);
        }
        else {
            openingWeek = ebDkOpeningWeekRepository.findById(openingWeekNum).get();
        }

        if (openingWeek != null) {
            List<EbDkOpeningDay> openingDays = ebDkOpeningDayRepository.findAllByWeek(openingWeek);
            openingWeek.setDays(openingDays);

            for (EbDkOpeningDay openingDay: openingDays) {
                openingDay.setHours(ebRangeHoursRepository.findAllByOpeningDay(openingDay));
            }

        }

        if (openingWeek != null) {
            openingWeekDto = new EbDkOpeningWeekDTO(openingWeek, true);
            List<EbDkOpeningPeriod> openingPeriods = ebDkOpeningPeriodRepository
                .findAllByWeek(openingWeek.getEbDkOpeningWeekNum());

            List<EbDkOpeningPeriodDTO> ebDkOpeningPeriodDtos = new ArrayList<EbDkOpeningPeriodDTO>();

            for (EbDkOpeningPeriod ebDkOpeningPeriod: openingPeriods) {
                EbDkOpeningPeriodDTO ebDkOpeningPeriodDto = new EbDkOpeningPeriodDTO(ebDkOpeningPeriod);
                ebDkOpeningPeriodDtos.add(ebDkOpeningPeriodDto);
            }

            openingWeekDto.setPeriods(ebDkOpeningPeriodDtos);

            List<EbDkOpeningDay> openingDays = ebDkOpeningDayRepository.findAllByWeek(openingWeek);
            List<EbDkOpeningDayDTO> openingDayDtos = new ArrayList<EbDkOpeningDayDTO>();

            for (EbDkOpeningDay openingDay: openingDays) {
                openingDay.setHours(ebRangeHoursRepository.findAllByOpeningDay(openingDay));
                EbDkOpeningDayDTO openingDayDto = new EbDkOpeningDayDTO(openingDay);
                openingDayDtos.add(openingDayDto);
            }

            openingWeekDto.setDays(openingDayDtos);
        }

        return openingWeekDto;
    }

    @Override
    public List<EbDkOpeningWeekDTO> getOpeningWeeks(Integer ebEntrepotNum) {
        EbEntrepot ent = new EbEntrepot();
        ent.setEbEntrepotNum(ebEntrepotNum);
        List<EbDkOpeningWeek> listOpeningWeek = ebDkOpeningWeekRepository.findAllByEntrepot(ent);

        List<EbDkOpeningWeekDTO> listOpeningWeekDtos = new ArrayList<EbDkOpeningWeekDTO>();

        for (EbDkOpeningWeek openingWeek: listOpeningWeek) {
            EbDkOpeningWeekDTO openingWeekDto = new EbDkOpeningWeekDTO(openingWeek, false);
            listOpeningWeekDtos.add(openingWeekDto);
        }

        return listOpeningWeekDtos;
    }

    @Override
    public EbDkOpeningWeek saveOpeningWeek(EbDkOpeningWeekDTO openingWeekDto) {
        EbDkOpeningWeek openingWeek = new EbDkOpeningWeek(openingWeekDto);
        EbDkOpeningWeek openingW = ebDkOpeningWeekRepository.save(openingWeek);

        if (openingWeek.getDays() != null) {

            for (EbDkOpeningDay day: openingWeek.getDays()) {
                day.setWeek(openingW);
                EbDkOpeningDay ebDkOpeningDay = ebDkOpeningDayRepository.save(day);
                ebRangeHoursRepository.deleteByOpeningDay(ebDkOpeningDay);

                for (EbRangeHour rangeHours: day.getHours()) {
                    rangeHours.setOpeningDay(ebDkOpeningDay);
                    ebRangeHoursRepository.save(rangeHours);
                }

            }

        }

        return openingW;
    }

    @Override
    public EbDkOpeningPeriod saveOpeningPeriod(EbDkOpeningPeriodDTO dto) {
        EbDkOpeningPeriod period;

        if (dto.getEbDkOpeningPeriodNum() == null) {
            period = new EbDkOpeningPeriod();
        }
        else {
            period = ebDkOpeningPeriodRepository.findById(dto.getEbDkOpeningPeriodNum()).get();
        }

        period.setLabel(dto.getLabel());
        period.setStartDate(dto.getStartDate());
        period.setEndDate(dto.getEndDate());
        period.setTypePeriod(dto.getTypePeriod());

        if (dto.getEbDkOpeningWeekNum() != null) {
            EbDkOpeningWeek week = new EbDkOpeningWeek();
            week.setEbDkOpeningWeekNum(dto.getEbDkOpeningWeekNum());
            period.setWeek(week);
        }

        return ebDkOpeningPeriodRepository.save(period);
    }

    @Override
    public void deleteOpeningPeriod(Integer ebDkOpeningPeriodNum) {
        ebDkOpeningPeriodRepository.deleteById(ebDkOpeningPeriodNum);
    }

    @Override
    public List<EbDkOpeningPeriod> getListOpeningPeriod(Integer ebDkOpeningWeekNum) {
        List<EbDkOpeningPeriod> openingPeriods = ebDkOpeningPeriodRepository.findAllByWeek(ebDkOpeningWeekNum);
        return openingPeriods;
    }

    @Override
    public List<EbDkOpeningPeriod> getListOpeningPeriodByEntrepot(Integer ebEntrepotNum) {
        List<EbDkOpeningPeriod> openingPeriods = ebDkOpeningPeriodRepository.findAllByEntrepot(ebEntrepotNum);
        return openingPeriods;
    }

    @Override
    public void deleteOpeningWeek(EbDkOpeningWeek ebDkOpeningWeek) {
        ebDkOpeningWeekRepository.delete(ebDkOpeningWeek);
    }

    @Override
    public List<EbDockDTO> convertDockEntitiesToDTOs(List<EbDock> entities) {
        List<EbDockDTO> rList = new ArrayList<>();
        entities.forEach(e -> {
            rList.add(new EbDockDTO(e));
        });
        return rList;
    }
}
