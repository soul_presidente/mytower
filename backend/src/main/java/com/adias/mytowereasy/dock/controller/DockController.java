package com.adias.mytowereasy.dock.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.controller.SuperControler;
import com.adias.mytowereasy.dock.dto.*;
import com.adias.mytowereasy.dock.model.*;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/dock")
public class DockController extends SuperControler {
    @GetMapping(value = "/get-list-entrepot")
    public List<EbEntrepotDTO>
        getListEntrepot(@RequestParam(defaultValue = "false", required = false) Boolean bindDocks) {
        List<EbEntrepotDTO> ebEntrepots = dockService.getListEntrepot(bindDocks);
        return ebEntrepots;
    }

    @RequestMapping(value = "list-entrepot-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object>
        getListEntrepotTable(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbEntrepotDTO> entrepots = dockService.getListEbEntrepot(criteria);
        Long count = dockService.getListEbEntrepotCount(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", entrepots);
        return result;
    }

    @PostMapping(value = "/add-entrepot")
    public EbEntrepotDTO addEntrepot(@RequestBody EbEntrepotDTO ebEntrepot) {
        return dockService.addEntrepot(ebEntrepot);
    }

    @PostMapping(value = "/delete-entrepot")
    public void deleteEntrepot(@RequestBody Integer ebEntrepotNum) {
        dockService.deleteEntrepot(ebEntrepotNum);
    }

    @PostMapping(value = "/add-dock-periode")
    public EbDockDTO addDock(@RequestBody EbDockDTO ebDock) {
        EbDock entity = dockService.addDock(ebDock);
        return new EbDockDTO(entity);
    }

    @PostMapping(value = "/get-list-dock")
    public List<EbDockDTO> getListDock(@RequestBody Integer ebEntrepotNum) {
        List<EbDockDTO> ebDockDtos = dockService.getListDock(ebEntrepotNum);
        return ebDockDtos;
    }

    @PostMapping(value = "/get-list-dock-table")
    public @ResponseBody Map<String, Object> listDockTable(@RequestBody SearchCriteriaDOCK criteria) {
        List<EbDock> entities = daoDock.searchDock(criteria);
        List<EbDockDTO> dtos = dockService.convertDockEntitiesToDTOs(entities);
        Long count = daoDock.countDock(criteria);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", dtos);
        return result;
    }

    @DeleteMapping(value = "/delete-dock")
    public void deleteDock(@RequestParam Integer ebDockNum) {
        dockService.deleteDock(ebDockNum);
    }

    @GetMapping(value = "/get-list-openingDay")
    public List<EbDkOpeningDayDTO> getListOpeningDay() {
        List<EbDkOpeningDay> entities = dockService.getListOpeningDay();
        List<EbDkOpeningDayDTO> dtos = new ArrayList<>();

        entities.forEach(db -> {
            dtos.add(new EbDkOpeningDayDTO(db));
        });
        return dtos;
    }

    @PostMapping(value = "/add-opening-days")
    public void addOpeningDays(@RequestBody List<EbDkOpeningDay> listOpeningDay) {
        dockService.addOpeningDays(listOpeningDay);
    }

    @PostMapping(value = "/delete-opening-day")
    public void deleteOpeningDay(@RequestBody EbDkOpeningDay openingDay) {
        dockService.deleteOpeningDay(openingDay);
    }

    @PostMapping(value = "/get-list-opening-week")
    public List<EbDkOpeningWeekDTO> getListOpeningWeek(@RequestBody Integer ebEntrepotNum) {
        List<EbDkOpeningWeekDTO> ebDkOpeningWeekDtos = dockService.getListOpeningWeek(ebEntrepotNum);
        return ebDkOpeningWeekDtos;
    }

    @PostMapping(value = "/get-base-opening-week")
    public EbDkOpeningWeekDTO getBaseOpeningWeek(@RequestBody Integer ebEntrepotNum) {
        EbDkOpeningWeekDTO ebDkOpeningWeekDto = dockService.getBaseOpeningWeek(ebEntrepotNum, null);
        return ebDkOpeningWeekDto;
    }

    @PostMapping(value = "/get-opening-week")
    public EbDkOpeningWeekDTO getOpeningWeek(@RequestBody Integer openingWeekNum) {
        EbDkOpeningWeekDTO ebDkOpeningWeekDto = dockService.getBaseOpeningWeek(null, openingWeekNum);
        return ebDkOpeningWeekDto;
    }

    @PostMapping(value = "/list-opening-weeks")
    public List<EbDkOpeningWeekDTO> getOpeningWeeks(@RequestBody Integer ebEntrepotNum) {
        List<EbDkOpeningWeekDTO> ebDkOpeningWeekDtos = dockService.getOpeningWeeks(ebEntrepotNum);
        return ebDkOpeningWeekDtos;
    }

    @PostMapping(value = "/save-opening-week")
    public EbDkOpeningWeekDTO saveOpeningWeek(@RequestBody EbDkOpeningWeekDTO openingWeekDto) {
        EbDkOpeningWeek entity = dockService.saveOpeningWeek(openingWeekDto);
        return new EbDkOpeningWeekDTO(entity, true);
    }

    @PostMapping(value = "/save-opening-period")
    public EbDkOpeningPeriodDTO saveOpeningPeriod(@RequestBody EbDkOpeningPeriodDTO openingPeriod) {
        EbDkOpeningPeriod entity = dockService.saveOpeningPeriod(openingPeriod);
        return new EbDkOpeningPeriodDTO(entity);
    }

    @DeleteMapping(value = "/delete-opening-period")
    public void deleteOpeningPeriod(@RequestParam Integer ebDkOpeningPeriodNum) {
        dockService.deleteOpeningPeriod(ebDkOpeningPeriodNum);
    }

    @GetMapping(value = "/list-opening-period-by-week")
    public List<EbDkOpeningPeriodDTO> getListOpeningPeriod(@RequestParam Integer ebDkOpeningWeekNum) {
        List<EbDkOpeningPeriod> entities = dockService.getListOpeningPeriod(ebDkOpeningWeekNum);
        List<EbDkOpeningPeriodDTO> dtos = new ArrayList<>();

        entities.forEach(e -> {
            dtos.add(new EbDkOpeningPeriodDTO(e));
        });

        return dtos;
    }

    @GetMapping(value = "/list-opening-period-by-entrepot")
    public List<EbDkOpeningPeriodDTO> getListOpeningPeriodByEntrepot(@RequestParam Integer ebEntrepotNum) {
        List<EbDkOpeningPeriod> entities = dockService.getListOpeningPeriodByEntrepot(ebEntrepotNum);
        List<EbDkOpeningPeriodDTO> dtos = new ArrayList<>();

        entities.forEach(e -> {
            dtos.add(new EbDkOpeningPeriodDTO(e));
        });

        return dtos;
    }

    @PostMapping(value = "/delete-opening-week")
    public void deleteOpeningWeek(@RequestBody EbDkOpeningWeek ebDkOpeningWeek) {
        dockService.deleteOpeningWeek(ebDkOpeningWeek);
    }
}
