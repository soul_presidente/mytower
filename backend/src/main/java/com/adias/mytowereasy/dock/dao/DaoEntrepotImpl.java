package com.adias.mytowereasy.dock.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dock.model.EbEntrepot;
import com.adias.mytowereasy.dock.model.QEbEntrepot;
import com.adias.mytowereasy.dock.repository.EbDockRepository;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbUser;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoEntrepotImpl implements DaoEntrepot {
    @PersistenceContext
    EntityManager em;
    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    EbDockRepository ebDockRepository;

    QEbUser qEbUser = QEbUser.ebUser;

    QEbEntrepot qEbEntrepot = QEbEntrepot.ebEntrepot;

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbEntrepot> listEntrepots(SearchCriteria criteria) {
        List<EbEntrepot> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbEntrepot> query = new JPAQuery<>(em);

            query
                .select(
                    QEbEntrepot
                        .create(
                            qEbEntrepot.ebEntrepotNum,
                            qEbEntrepot.reference,
                            qEbEntrepot.libelle,
                            qEbEntrepot.deleted,
                            qEbEntrepot.etablissement()));

            query = getEntrepotGlobalWhere(query, where, criteria);

            query.from(qEbEntrepot);
            query.where(where);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbEntrepot, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbEntrepot.reference.asc());
            }

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getListEbEntrepotCount(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbEntrepot> query = new JPAQuery<EbEntrepot>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getEntrepotGlobalWhere(query, where, criteria);
            query.from(qEbEntrepot);
            query.distinct().where(where);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getEntrepotGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        where.and(qEbEntrepot.deleted.isFalse());

        if (connectedUser != null) {

            if (connectedUser.isControlTower() || connectedUser.isSuperAdmin()) {

                if (criteria != null && criteria.getEbCompagnieNum() != null) {
                    where.and(qEbEntrepot.xEbCompagnie().ebCompagnieNum.eq(criteria.getEbCompagnieNum()));
                }

            }
            else {

                if (criteria != null && criteria.getEbEtablissementNum() != null) {
                    where.and(qEbEntrepot.etablissement().ebEtablissementNum.eq(criteria.getEbEtablissementNum()));
                }

            }

        }

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qEbEntrepot.reference.toLowerCase().contains(term),
                    qEbEntrepot.libelle.toLowerCase().contains(term));
        }

        return query;
    }
}
