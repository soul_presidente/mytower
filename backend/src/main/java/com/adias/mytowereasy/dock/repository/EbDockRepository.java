package com.adias.mytowereasy.dock.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.dock.model.EbDock;
import com.adias.mytowereasy.dock.model.EbEntrepot;
import com.adias.mytowereasy.model.Enumeration.JourEnum;
import com.adias.mytowereasy.repository.CommonRepository;


@Repository
public interface EbDockRepository extends CommonRepository<EbDock, Integer> {
    public List<EbDock> findAllByEntrepot(EbEntrepot ebEntrepot);

    @Query("select dock FROM ExDkOpeningHour  oh, EbDock dock, EbDkOpeningDay od where  oh.pKExDkOpeningHourId.x_opening_hours=od.openingDayNum and oh.pKExDkOpeningHourId.x_eb_dock= dock.ebDockNum  and od.openingDayNum in (select od.openingDayNum from EbDkOpeningDay od where od.open=true and od.day= :day )")
    public List<EbDock> getListDockByDay(@Param("day") JourEnum day);

    @Transactional
    @Modifying
    @Query("update EbDock d set d.deleted = true where d.ebDockNum = ?1")
    public void deleteWithHist(Integer ebDockNum);
}
