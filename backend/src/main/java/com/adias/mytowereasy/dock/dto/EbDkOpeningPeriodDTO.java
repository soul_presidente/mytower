package com.adias.mytowereasy.dock.dto;

import java.util.Date;

import com.adias.mytowereasy.dock.model.EbDkOpeningPeriod;


public class EbDkOpeningPeriodDTO {
    private Integer ebDkOpeningPeriodNum;

    private String label;

    private Date startDate;

    private Date endDate;

    private Integer typePeriod;

    private Integer ebDkOpeningWeekNum;

    public EbDkOpeningPeriodDTO() {
    }

    public EbDkOpeningPeriodDTO(EbDkOpeningPeriod ebDkOpeningPeriod) {
        this.ebDkOpeningPeriodNum = ebDkOpeningPeriod.getEbDkOpeningPeriodNum();

        this.label = ebDkOpeningPeriod.getLabel();

        this.startDate = ebDkOpeningPeriod.getStartDate();

        this.endDate = ebDkOpeningPeriod.getEndDate();

        this.typePeriod = ebDkOpeningPeriod.getTypePeriod();

        if (ebDkOpeningPeriod.getWeek() != null) {
            this.ebDkOpeningWeekNum = ebDkOpeningPeriod.getWeek().getEbDkOpeningWeekNum();
        }

    }

    public Integer getEbDkOpeningPeriodNum() {
        return ebDkOpeningPeriodNum;
    }

    public void setEbDkOpeningPeriodNum(Integer ebDkOpeningPeriodNum) {
        this.ebDkOpeningPeriodNum = ebDkOpeningPeriodNum;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getTypePeriod() {
        return typePeriod;
    }

    public void setTypePeriod(Integer typePeriod) {
        this.typePeriod = typePeriod;
    }

    public Integer getEbDkOpeningWeekNum() {
        return ebDkOpeningWeekNum;
    }

    public void setEbDkOpeningWeekNum(Integer ebDkOpeningWeekNum) {
        this.ebDkOpeningWeekNum = ebDkOpeningWeekNum;
    }
}
