package com.adias.mytowereasy.dock.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.adias.mytowereasy.dock.model.EbEntrepot;


@JsonIgnoreProperties({
    "etablissement"
})
public class EbEntrepotDTO {
    private Integer ebEntrepotNum;

    private String reference;
    private String libelle;
    private Boolean deleted;
    private Integer ebEtablissementNum;
    private Integer ebCompagnieNum;
    private List<EbDockDTO> docks;

    public EbEntrepotDTO() {
    }

    public Integer getEbCompagnieNum() {
        return ebCompagnieNum;
    }

    public void setEbCompagnieNum(Integer ebCompagnieNum) {
        this.ebCompagnieNum = ebCompagnieNum;
    }

    public EbEntrepotDTO(EbEntrepot ebEntrepot, Boolean bindDocks) {
        this.ebEntrepotNum = ebEntrepot.getEbEntrepotNum();

        this.reference = ebEntrepot.getReference();
        this.libelle = ebEntrepot.getLibelle();
        this.deleted = ebEntrepot.getDeleted();

        if (ebEntrepot.getEtablissement() != null) {
            this.ebEtablissementNum = ebEntrepot.getEtablissement().getEbEtablissementNum();
        }

        if (ebEntrepot.getxEbCompagnie() != null) {
            this.ebCompagnieNum = ebEntrepot.getxEbCompagnie().getEbCompagnieNum();
        }

        if (bindDocks && ebEntrepot.getListDocks() != null) {
            docks = new ArrayList<>();
            ebEntrepot.getListDocks().forEach(d -> {
                docks.add(new EbDockDTO(d));
            });
        }

    }

    public Integer getEbEntrepotNum() {
        return ebEntrepotNum;
    }

    public void setEbEntrepotNum(Integer ebEntrepotNum) {
        this.ebEntrepotNum = ebEntrepotNum;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getEbEtablissementNum() {
        return ebEtablissementNum;
    }

    public void setEbEtablissementNum(Integer ebEtablissementNum) {
        this.ebEtablissementNum = ebEtablissementNum;
    }

    public List<EbDockDTO> getDocks() {
        return docks;
    }

    public void setDocks(List<EbDockDTO> docks) {
        this.docks = docks;
    }
}
