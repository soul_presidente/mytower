package com.adias.mytowereasy.dock.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.adias.mytowereasy.dock.dto.EbDkOpeningDayDTO;
import com.adias.mytowereasy.dock.dto.EbDkOpeningWeekDTO;


@Entity
@Table(name = "eb_dk_opening_week", schema = "work")
public class EbDkOpeningWeek {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebDkOpeningWeekNum;

    private String label;

    private Boolean isDefault;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_entrepot", insertable = true)
    private EbEntrepot entrepot;

    @OneToMany(mappedBy = "week", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private List<EbDkOpeningDay> days;

    @OneToMany(mappedBy = "week", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private List<EbDkOpeningPeriod> periods;

    public EbDkOpeningWeek() {
    }

    public EbDkOpeningWeek(EbDkOpeningWeekDTO ebDkOpeningWeekDto) {
        this.ebDkOpeningWeekNum = ebDkOpeningWeekDto.getEbDkOpeningWeekNum();

        this.label = ebDkOpeningWeekDto.getLabel();

        this.isDefault = ebDkOpeningWeekDto.getIsDefault();

        if (ebDkOpeningWeekDto.getEbEntrepotNum() != null) {
            EbEntrepot entrepot = new EbEntrepot();
            entrepot.setEbEntrepotNum(ebDkOpeningWeekDto.getEbEntrepotNum());

            this.entrepot = entrepot;
        }
        else {
            this.entrepot = null;
        }

        List<EbDkOpeningDay> days = new ArrayList<EbDkOpeningDay>();

        if (ebDkOpeningWeekDto.getDays() != null) {

            for (EbDkOpeningDayDTO ebDkOpeningDayDto: ebDkOpeningWeekDto.getDays()) {
                days.add(new EbDkOpeningDay(ebDkOpeningDayDto));
            }

            this.days = days;
        }

        /*
         * List<EbDkOpeningPeriod> periods = new ArrayList<EbDkOpeningPeriod>();
         * for (EbDkOpeningPeriodDto ebDkOpeningPeriodDto :
         * ebDkOpeningWeekDto.getPeriods()) { periods.add(new
         * EbDkOpeningPeriod(ebDkOpeningPeriodDto)); }
         */
        // this.periods = periods;
    }

    /*
     * public List<EbDkOpeningPeriod> getPeriods() { return periods; }
     * public void setPeriods(List<EbDkOpeningPeriod> periods) { this.periods =
     * periods; }
     */

    public Integer getEbDkOpeningWeekNum() {
        return ebDkOpeningWeekNum;
    }

    public void setEbDkOpeningWeekNum(Integer ebDkOpeningWeekNum) {
        this.ebDkOpeningWeekNum = ebDkOpeningWeekNum;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public EbEntrepot getEntrepot() {
        return entrepot;
    }

    public void setEntrepot(EbEntrepot entrepot) {
        this.entrepot = entrepot;
    }

    public List<EbDkOpeningDay> getDays() {
        return days;
    }

    public void setDays(List<EbDkOpeningDay> days) {
        this.days = days;
    }
}
