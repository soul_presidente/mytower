package com.adias.mytowereasy.util;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.ConnectedUserService;


public class CustomAuditorAware implements AuditorAware<String> {
    @Autowired
    protected ConnectedUserService connectedUserService;

    @Override
    public Optional<String> getCurrentAuditor() {
        // TODO Auto-generated method stub
        // Config current user
        EbUser currentUser = connectedUserService.getCurrentUser();
        String user = currentUser != null ? currentUser.getNomPrenom() : "";
        Optional<String> ostr = Optional.ofNullable(user).filter(s -> !s.isEmpty());

        return ostr;
    }
}
