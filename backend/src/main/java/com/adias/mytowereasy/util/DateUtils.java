package com.adias.mytowereasy.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


public class DateUtils {
    public static String formatUTCDateOnly(Date date) {
        if (date == null) return "";
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        long diff = TimeZone.getDefault().getRawOffset() - df.getTimeZone().getRawOffset();
        return df.format(new Date(date.getTime() - diff));
    }

    public static Date setDate_To_The_Start_Of_The_Day_Month(Date date, boolean setmonthFirstDay) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Calendar calendarLocal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendarLocal.setTime(date);
        if (setmonthFirstDay) calendarLocal.set(Calendar.DAY_OF_MONTH, 1);
        calendarLocal.set(Calendar.HOUR_OF_DAY, 0);
        calendarLocal.set(Calendar.MINUTE, 00);
        calendarLocal.set(Calendar.SECOND, 00);
        return calendarLocal.getTime();
    }

    public static Date setDate_To_The_End_Of_The_Day_And_Month(Date date, boolean setMonthLastDay) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Calendar calendarLocal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendarLocal.setTime(date);
        if (setMonthLastDay) calendarLocal.set(Calendar.DAY_OF_MONTH, calendarLocal.getActualMaximum(Calendar.DATE));
        calendarLocal.set(Calendar.HOUR_OF_DAY, 23);
        calendarLocal.set(Calendar.MINUTE, 59);
        calendarLocal.set(Calendar.SECOND, 00);
        return calendarLocal.getTime();
    }
}
