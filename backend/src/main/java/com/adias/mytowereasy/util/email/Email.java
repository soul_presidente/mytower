/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.util.email;

import java.util.List;

import com.adias.mytowereasy.model.Enumeration.EmailConfig;


public class Email {
    private String to;
    private String subject;
    private String text;
    private List<String> enCopie;
    private List<String> enCopieCachee;
    private Integer mailType;
    private String[] arguments;
    private EmailConfig emailConfig;
    private String from;

    public void setFrom(String from) {
        this.from = from;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getEnCopie() {
        return enCopie;
    }

    public void setEnCopie(List<String> enCopie) {
        this.enCopie = enCopie;
    }

    public List<String> getEnCopieCachee() {
        return enCopieCachee;
    }

    public void setEnCopieCachee(List<String> enCopieCachee) {
        this.enCopieCachee = enCopieCachee;
    }

    public Integer getMailType() {
        return mailType;
    }

    public void setMailType(Integer mailType) {
        this.mailType = mailType;
    }

    public String[] getArguments() {
        return arguments;
    }

    public void setArguments(String[] arguments) {
        this.arguments = arguments;
    }

    public EmailConfig getEmailConfig() {
        return emailConfig;
    }

    public void setEmailConfig(EmailConfig emailConfig) {
        this.emailConfig = emailConfig;
    }
}
