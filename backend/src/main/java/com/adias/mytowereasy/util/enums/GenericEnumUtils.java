package com.adias.mytowereasy.util.enums;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.adias.mytowereasy.util.GenericEnum;
import com.adias.mytowereasy.util.GenericEnumDictionnary;


public class GenericEnumUtils {
    public static Class<? extends GenericEnum> findClass(String genericEnumKey) throws GenericEnumException {
        Optional<GenericEnumDictionnary> item = Arrays
            .stream(GenericEnumDictionnary.values()).filter(it -> genericEnumKey.equals(it.getKey())).findFirst();

        return item
            .orElseThrow(
                () -> new GenericEnumException(
                    String.format("Class %s doesn't exist in GenericEnumDictionnary", genericEnumKey)))
            .getClassEnum();
    }

    public static List<GenericEnum> getAllValues(String genericEnumKey) throws GenericEnumException {
        return getAllValues(findClass(genericEnumKey));
    }

    public static List<GenericEnum> getAllValues(Class<? extends GenericEnum> genericEnumClass)
        throws GenericEnumException {

        try {
            Field valuesField;

            try {
                valuesField = genericEnumClass.getDeclaredField("$VALUES");
            } catch (NoSuchFieldException nsfe) {
                // Sometimes, the java compiler use ENUM$VALUES instead of
                // $VALUES
                valuesField = genericEnumClass.getDeclaredField("ENUM$VALUES");
            }

            valuesField.setAccessible(true);

            Object o = valuesField.get(null);
            GenericEnum[] valuesArray = (GenericEnum[]) o;
            List<GenericEnum> valuesList = Arrays.asList(valuesArray);
            return valuesList;
        } catch (NoSuchFieldException | IllegalAccessException ex) {
            throw new GenericEnumException("Error retrieving all values form GenericEnum. See cause", ex);
        }

    }

    public static Optional<GenericEnum> getByCode(String genericEnumKey, String key) throws GenericEnumException {
        return getByKey(findClass(genericEnumKey), key);
    }

    public static Optional<GenericEnum> getByCode(Class<? extends GenericEnum> genericEnumclass, Integer code)
        throws GenericEnumException {
        if (code == null) return Optional.empty();

        List<GenericEnum> values = getAllValues(genericEnumclass);

        for (GenericEnum value: values) {

            if (value.getCode().equals(code)) {
                return Optional.of(value);
            }

        }

        return Optional.empty();
    }

    public static Optional<GenericEnum> getByKey(String genericEnumKey, String key) throws GenericEnumException {
        return getByKey(findClass(genericEnumKey), key);
    }

    public static Optional<GenericEnum> getByKey(Class<? extends GenericEnum> genericEnumclass, String key)
        throws GenericEnumException {
        if (StringUtils.isBlank(key)) return Optional.empty();

        List<GenericEnum> values = getAllValues(genericEnumclass);

        for (GenericEnum value: values) {

            if (value.getKey().toLowerCase().equals(key.toLowerCase())) {
                return Optional.of(value);
            }

        }

        return Optional.empty();
    }

    public static Optional<Integer> getCodeByKey(String genericEnumKey, String key) throws GenericEnumException {
        return getCodeByKey(findClass(genericEnumKey), key);
    }

    public static Optional<Integer> getCodeByKey(Class<? extends GenericEnum> genericEnumClass, String key)
        throws GenericEnumException {
        Optional<GenericEnum> value = getByKey(genericEnumClass, key);
        if (!value.isPresent()) return Optional.empty();
        return Optional.of(value.get().getCode());
    }

    public static Optional<String> getKeyByCode(String genericEnumKey, Integer code) throws GenericEnumException {
        return getKeyByCode(findClass(genericEnumKey), code);
    }

    public static Optional<String> getKeyByCode(Class<? extends GenericEnum> genericEnumClass, Integer code)
        throws GenericEnumException {
        Optional<GenericEnum> value = getByCode(genericEnumClass, code);
        if (!value.isPresent()) return Optional.empty();
        return Optional.of(value.get().getKey());
    }
}
