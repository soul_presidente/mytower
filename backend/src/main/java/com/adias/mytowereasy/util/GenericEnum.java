package com.adias.mytowereasy.util;

/**
 * Interface to implement in each enumeration
 */
public interface GenericEnum {
    /**
     * @return Value code
     */
    Integer getCode();

    /**
     * @return Value key
     */
    String getKey();
}
