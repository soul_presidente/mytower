package com.adias.mytowereasy.util;

public class Constants {
    public static Double CentimeterToMeters = 0.01;
    public static int VALEUR_DECIMAL_ARRONDI = 4;
    public static String refOrderPrefix = "-O";
    public static String refDeliveryPrefix = "-V";
}
