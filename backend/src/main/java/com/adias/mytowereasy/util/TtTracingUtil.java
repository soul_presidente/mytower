package com.adias.mytowereasy.util;

import java.util.Collections;

import com.adias.mytowereasy.model.tt.EbTtTracing;


public class TtTracingUtil {
    // cette methode permet de calculer le pourcentage sur 100% du PSL courant
    // par rapport a la list PSL
    public static void calculPercentPslCourant(EbTtTracing tracing) {

        if (tracing.getListPsl() != null) {
            Integer nbrTotal = tracing.getListPsl().size();
            double percent = 0.0;

            if (tracing.getCodeAlphaPslCourant() != null) {
                Collections.sort(tracing.getListPsl(), (it1, it2) -> it1.getOrder().compareTo(it2.getOrder()));

                for (int i = 0; i < tracing.getListPsl().size(); i++) {

                    if (tracing.getListPsl().get(i).getCodeAlpha().equals(tracing.getCodeAlphaPslCourant())) {
                        percent = (double) ((i + 1) * 100) / nbrTotal;
                        break;
                    }

                }

            }
            else {
                tracing.setCodeAlphaPslCourant("Wating for pickup");
            }

            tracing.setPercentPslCourant(percent);
        }

    }
}
