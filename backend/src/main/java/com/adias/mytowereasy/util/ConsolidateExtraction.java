package com.adias.mytowereasy.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbMarchandiseForExtraction;
import com.adias.mytowereasy.model.Enumeration.TypeContainer;


/**
 * Generation de la liste des marchandises consolidées
 */
public class ConsolidateExtraction {
    /**
     * Construit la liste des marchandises consolidées
     * 
     * @param listEbMarchandise
     */

    @SuppressWarnings({
        "unchecked", "unused"
    })
    public static Map<String, Object> getListEbMarchandiseConsolidate(List<EbMarchandise> listEbMarchandise) {
        List<EbMarchandiseForExtraction> listEbMarchandiseForExtraction = new ArrayList<EbMarchandiseForExtraction>();

        HashMap<String, Object> extractDatas = getExtractionDatas(listEbMarchandise);
        List<EbMarchandise> listArticle = (List<EbMarchandise>) extractDatas.get("listArticle");
        List<EbMarchandise> listParcel = (List<EbMarchandise>) extractDatas.get("listParcel");
        List<EbMarchandise> listPalette = (List<EbMarchandise>) extractDatas.get("listPalette");
        List<EbMarchandise> listContainer = (List<EbMarchandise>) extractDatas.get("listContainer");
        List<Integer> listTypeContener = (List<Integer>) extractDatas.get("listTypeContener");

        if (!listTypeContener.isEmpty()) {

            if (listTypeContener.contains(TypeContainer.ARTICLE.getKey())) {

                for (EbMarchandise march: listArticle) {
                    EbMarchandiseForExtraction marchandiseToExtract = new EbMarchandiseForExtraction();
                    marchandiseToExtract.setEbMarchandiseNum(march.getEbMarchandiseNum());

                    marchandiseToExtract.setNomArticle(march.getNomArticle());
                    marchandiseToExtract.setOriginCountryName(march.getOriginCountryName());
                    marchandiseToExtract.setxEbTypeConteneur(march.getxEbTypeConteneur());

                    marchandiseToExtract.setUnitReferenceArticle(march.getUnitReference());
                    marchandiseToExtract.setLotQuantiteArticle(march.getLot_quantite());
                    marchandiseToExtract.setBatchArticle(march.getBatch());
                    marchandiseToExtract.setLotArticle(march.getLot());

                    marchandiseToExtract = affecteParentData(
                        listEbMarchandise,
                        marchandiseToExtract,
                        march.getParent());
                    listEbMarchandiseForExtraction.add(marchandiseToExtract);
                }

            }
            else if (listTypeContener.contains(TypeContainer.PARCEL.getKey())) {

                for (EbMarchandise march: listParcel) {
                    EbMarchandiseForExtraction marchandiseToExtract = new EbMarchandiseForExtraction();
                    marchandiseToExtract.setEbMarchandiseNum(march.getEbMarchandiseNum());
                    marchandiseToExtract.setxEbTypeConteneur(march.getxEbTypeConteneur());

                    marchandiseToExtract.setUnitReferenceParcel(march.getUnitReference());
                    marchandiseToExtract.setLotQuantiteParcel(march.getLot_quantite());
                    marchandiseToExtract.setBatchParcel(march.getBatch());
                    marchandiseToExtract.setLotParcel(march.getLot());

                    marchandiseToExtract = affecteParentData(
                        listEbMarchandise,
                        marchandiseToExtract,
                        march.getParent());
                    listEbMarchandiseForExtraction.add(marchandiseToExtract);
                }

            }
            else if (listTypeContener.contains(TypeContainer.PALETTE.getKey())) {

                for (EbMarchandise march: listPalette) {
                    EbMarchandiseForExtraction marchandiseToExtract = new EbMarchandiseForExtraction();
                    marchandiseToExtract.setEbMarchandiseNum(march.getEbMarchandiseNum());
                    marchandiseToExtract.setxEbTypeConteneur(march.getxEbTypeConteneur());

                    marchandiseToExtract.setUnitReferencePalette(march.getUnitReference());
                    marchandiseToExtract.setLotQuantitePalette(march.getLot_quantite());
                    marchandiseToExtract.setBatchPalette(march.getBatch());
                    marchandiseToExtract.setLotPalette(march.getLot());

                    marchandiseToExtract = affecteParentData(
                        listEbMarchandise,
                        marchandiseToExtract,
                        march.getParent());
                    listEbMarchandiseForExtraction.add(marchandiseToExtract);
                }

            }
            else if (listTypeContener.contains(TypeContainer.CONTAINER.getKey())) {

                for (EbMarchandise march: listContainer) {
                    EbMarchandiseForExtraction marchandiseToExtract = new EbMarchandiseForExtraction();
                    marchandiseToExtract.setEbMarchandiseNum(march.getEbMarchandiseNum());
                    marchandiseToExtract.setxEbTypeConteneur(march.getxEbTypeConteneur());

                    marchandiseToExtract.setUnitReferenceContainer(march.getUnitReference());
                    marchandiseToExtract.setLotQuantiteContainer(march.getLot_quantite());
                    marchandiseToExtract.setBatchContainer(march.getBatch());
                    marchandiseToExtract.setLotContainer(march.getLot());

                    marchandiseToExtract = affecteParentData(
                        listEbMarchandise,
                        marchandiseToExtract,
                        march.getParent());
                    listEbMarchandiseForExtraction.add(marchandiseToExtract);
                }

            }

        }

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("listMarchandiseExport", listEbMarchandiseForExtraction);
        result.put("listTypeContener", listTypeContener);
        return result;
    }

    private static EbMarchandiseForExtraction affecteParentData(
        List<EbMarchandise> listEbMarchandise,
        EbMarchandiseForExtraction marchandiseForExtract,
        EbMarchandise marchParent) {

        if (marchParent != null) {

            if (marchParent.getxEbTypeConteneur() == TypeContainer.PARCEL.getKey()) {
                marchandiseForExtract.setUnitReferenceParcel(marchParent.getUnitReference());
                marchandiseForExtract.setLotQuantiteParcel(marchParent.getLot_quantite());
                marchandiseForExtract.setBatchParcel(marchParent.getBatch());
                marchandiseForExtract.setLotParcel(marchParent.getLot());
            }
            else if (marchParent.getxEbTypeConteneur() == TypeContainer.PALETTE.getKey()) {
                marchandiseForExtract.setUnitReferencePalette(marchParent.getUnitReference());
                marchandiseForExtract.setLotQuantitePalette(marchParent.getLot_quantite());
                marchandiseForExtract.setBatchPalette(marchParent.getBatch());
                marchandiseForExtract.setLotPalette(marchParent.getLot());
            }
            else if (marchParent.getxEbTypeConteneur() == TypeContainer.CONTAINER.getKey()) {
                marchandiseForExtract.setUnitReferenceContainer(marchParent.getUnitReference());
                marchandiseForExtract.setLotQuantiteContainer(marchParent.getLot_quantite());
                marchandiseForExtract.setBatchContainer(marchParent.getBatch());
                marchandiseForExtract.setLotContainer(marchParent.getLot());
            }

            marchandiseForExtract = affecteParentData(
                listEbMarchandise,
                marchandiseForExtract,
                marchParent.getParent());
        }

        return marchandiseForExtract;
    }

    private static HashMap<String, Object> getExtractionDatas(List<EbMarchandise> listEbMarchandise) {
        boolean typeArticle = false, typeParcel = false, typePalette = false, typeConteneur = false;
        HashMap<String, Object> extractDatas = new HashMap<String, Object>();
        List<EbMarchandise> listArticle = new ArrayList<>();
        List<EbMarchandise> listParcel = new ArrayList<>();
        List<EbMarchandise> listPalette = new ArrayList<>();
        List<EbMarchandise> listContainer = new ArrayList<>();
        List<Integer> listTypeContener = new ArrayList<>();

        for (EbMarchandise march: listEbMarchandise) {

            if (march.getxEbTypeConteneur() == TypeContainer.ARTICLE.getKey()) {
                listArticle.add(march);

                if (!typeArticle) {
                    typeArticle = true;
                }

            }
            else if (march.getxEbTypeConteneur() == TypeContainer.PARCEL.getKey()) {
                listParcel.add(march);

                if (!typeParcel) {
                    typeParcel = true;
                }

            }
            else if (march.getxEbTypeConteneur() == TypeContainer.PALETTE.getKey()) {
                listPalette.add(march);

                if (!typePalette) {
                    typePalette = true;
                }

            }
            else if (march.getxEbTypeConteneur() == TypeContainer.CONTAINER.getKey()) {
                listContainer.add(march);

                if (!typeConteneur) {
                    typeArticle = true;
                }

            }

        }

        if (typeArticle) {
            listTypeContener.add(TypeContainer.ARTICLE.getKey());
            extractDatas.put("listArticle", listArticle);
        }

        if (typeParcel) {
            listTypeContener.add(TypeContainer.PARCEL.getKey());
            extractDatas.put("listParcel", listParcel);
        }

        if (typePalette) {
            listTypeContener.add(TypeContainer.PALETTE.getKey());
            extractDatas.put("listPalette", listPalette);
        }

        if (typeConteneur) {
            listTypeContener.add(TypeContainer.CONTAINER.getKey());
            extractDatas.put("listContainer", listContainer);
        }

        extractDatas.put("listTypeContener", listTypeContener);

        return extractDatas;
    }
}
