package com.adias.mytowereasy.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class GenericObjectUtils {
    private static final Logger logger = LoggerFactory.getLogger(GenericObjectUtils.class);

    public static Object getValueByObjectPath(Class<?> cls, Object obj, String fullPath) {
        Object val = null;
        List<String> paths = null;

        if (cls == null || fullPath == null || fullPath.isEmpty() || obj == null) return obj;

        paths = Arrays.asList(fullPath.split(":"));

        String curPath = paths.get(0);

        try {

            if (curPath.contains("call-")) {
                List<String> args = Arrays.asList(curPath.split("-"));
                List<String> splitArgs = Arrays.asList(args.get(1).split("\\."));
                List<String> classMethod = new ArrayList<>();
                for (String str: splitArgs) {
                    if (splitArgs.indexOf(str) < splitArgs.size() - 1) {
                        classMethod.add(str);
                    }
                }

                Class<?> classService = Class.forName(String.join(".", classMethod));
                if (args.size() == 2) val = GenericObjectUtils
                    .callMethod(classService, classService.newInstance(), splitArgs.get(splitArgs.size() - 1), obj);
                else if (args.size() == 3) val = GenericObjectUtils
                    .callMethod(
                        classService,
                        classService.newInstance(),
                        splitArgs.get(splitArgs.size() - 1),
                        obj,
                        args.get(2));
            }
            else if (curPath.contains("find-")) {
                List<String> args = Arrays.asList(curPath.split("-"));
                List<String> params = Arrays.asList(args.get(1).split("="));
                String term = params.size() >= 2 ? params.get(1) : params.get(0);
                String valPath = params.size() >= 2 ? params.get(0) : null;
                term = term.replaceAll("'", "");
                List<?> arr = (List<?>) obj;

                if (!arr.isEmpty()) {

                    for (Object it: arr) {
                        Object res = null;
                        if (valPath != null) res = callMethod(it.getClass(), it, "get" + valPath);
                        else res = it;

                        if (term.equalsIgnoreCase(res.toString())) {
                            val = it;
                            break;
                        }

                    }

                }

            }
            else if (curPath.equals("first-set")) {
                List<?> arr = new ArrayList<Object>((Set<?>) obj);
                if (!arr.isEmpty()) val = arr.get(0);
            }
            else if (curPath.equals("first-list")) {
                List<?> arr = (List<?>) obj;
                if (!arr.isEmpty()) val = arr.get(0);
            }
            else {
                val = GenericObjectUtils.callMethod(cls, obj, "get" + curPath);

                if (curPath.equalsIgnoreCase("TypeRequestLibelle") && val != null
                    && StringUtils.containsIgnoreCase(val.toString(), "ess")) {
                    val = val.toString().split("-")[0];
                }

            }

            List<String> subPaths = new ArrayList<>();
            for (String path: paths) {
                if (paths.indexOf(path) > 0) {
                    subPaths.add(path);
                }
            }
            val = getValueByObjectPath(val != null ? val.getClass() : null, val, String.join(":", subPaths));
        } catch (Exception e) {
            logger.error("Error when getting value for object " + cls.getName() + " by path " + curPath);
            e.printStackTrace();
        }

        return val;
    }

    public static Object callMethod(Class<?> cls, Object obj, String methodName, Object... arg) throws Exception {
        Object val = null;

        if (obj == null) return null;

        for (Method meth: cls.getMethods()) {

            if (meth.getName().contains("get") && meth.getName().equalsIgnoreCase(methodName)) {
                if (arg != null) val = meth.invoke(obj, arg);
                else val = meth.invoke(obj);
                break;
            }

        }

        return val;
    }
}
