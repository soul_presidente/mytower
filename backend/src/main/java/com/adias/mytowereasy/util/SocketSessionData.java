package com.adias.mytowereasy.util;

public class SocketSessionData {
    private boolean pricingDetailsLocked;
    private int module;
    private int recordId;
    private String sessionId;
    private boolean free;

    public SocketSessionData() {
        pricingDetailsLocked = false;
    }

    public boolean isPricingDetailsLocked() {
        return pricingDetailsLocked;
    }

    public void setPricingDetailsLocked(boolean pricingDetailsLocked) {
        this.pricingDetailsLocked = pricingDetailsLocked;
    }

    public boolean canUnload() {
        return !pricingDetailsLocked;
    }

    public int getModule() {
        return module;
    }

    public int getRecordId() {
        return recordId;
    }

    public void setModule(int module) {
        this.module = module;
    }

    public void setRecordId(int recordId) {
        this.recordId = recordId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public boolean isFree() {
        return free;
    }

    public void setFree(boolean free) {
        this.free = free;
    }
}
