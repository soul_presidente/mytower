package com.adias.mytowereasy.util;

public class ConsolidationConstants {
    public static final Integer DEFAULT_PAGE_NUMBER = 0;

    public static final Integer DEFAULT_SIZE = 100;
}
