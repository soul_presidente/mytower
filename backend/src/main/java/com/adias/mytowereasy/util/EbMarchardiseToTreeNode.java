package com.adias.mytowereasy.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.Enumeration.TypeContainer;
import com.adias.mytowereasy.model.TreeNode;


/**
 * Transformation de la liste EbDemande en une liste de TreeNode
 * EbDemande -> TreeNode
 */
public class EbMarchardiseToTreeNode {
    /**
     * Construit la hierarchie parent enfant entre container, pallete, colis et
     * article
     * 
     * @param node
     * @param listEbMarchandise
     */
    private static void buildTree(TreeNode<EbMarchandise> node, List<EbMarchandise> listEbMarchandise) {
        TreeNode<EbMarchandise> currentNode = node;

        if (currentNode.getData() == null || listEbMarchandise == null) {
            return;
        }

        List<EbMarchandise> result = listEbMarchandise
            .stream()
            .filter(
                marchandise -> marchandise.getParent() != null
                    && marchandise.getParent().getEbMarchandiseNum() == currentNode.getData().getEbMarchandiseNum())
            .collect(Collectors.toList());

        if (result.size() == 0) {
            return;
        }

        result.stream().forEach(child -> {
            if (hasChildreen(child, listEbMarchandise)) child.setHasChildren(true);
            TreeNode<EbMarchandise> childNode = currentNode.addChild(child);

            String labelContainer = "Inconnu";
            labelContainer = child.getxEbTypeConteneur() != null ?
                TypeContainer.getTypeContainerByKey(child.getxEbTypeConteneur()).getValue() :
                "";
            childNode.setLabel(labelContainer + " : " + child.getUnitReference());
            String collapsedIcon = "";
            String expandedIcon = "";
            // Conditions pour remplir les icônes.
            childNode.setCollapsedIcon(collapsedIcon);
            childNode.setExpandedIcon(expandedIcon);
            buildTree(childNode, listEbMarchandise);
        });
    }

    private static boolean hasChildreen(EbMarchandise march, List<EbMarchandise> listEbMarchandise) {
        boolean exist = false;

        for (int i = 0; i < listEbMarchandise.size(); i++) {

            if (listEbMarchandise.get(i).getParent() != null
                && march.getEbMarchandiseNum() == listEbMarchandise.get(i).getParent().getEbMarchandiseNum()) {
                exist = true;
                i = listEbMarchandise.size();
            }

        }

        return exist;
    }

    /**
     * Transformation listEbMarchandise en listTreeNode
     * 
     * @param listEbMarchandise
     * @return listTreeMarchandises
     */

    public static Map<String, Object> listMarchandiseToListTreeNode(List<EbMarchandise> listEbMarchandise) {
        List<TreeNode<EbMarchandise>> treeMarchandises = new LinkedList<TreeNode<EbMarchandise>>();
        listEbMarchandise.stream().filter(marchandise -> marchandise.getParent() == null).forEach(merch -> {
            TreeNode<EbMarchandise> node = new TreeNode<EbMarchandise>(merch);
            // EbTypeUnit type = merch.getxEbTypeUnit();

            if (hasChildreen(merch, listEbMarchandise)) merch.setHasChildren(true);

            String labelContainer = "Inconnu";
            if (merch.getxEbTypeConteneur() != null) labelContainer = TypeContainer
                .getTypeContainerByKey(merch.getxEbTypeConteneur()).getValue();

            node.setLabel(labelContainer + " : " + merch.getUnitReference());
            EbMarchardiseToTreeNode.buildTree(node, listEbMarchandise);
            treeMarchandises.add(node);
        });
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("listEbMarchandise", listEbMarchandise);
        result.put("treeMarchandises", treeMarchandises);
        return result;
    }

    public static Long countTypeMarchandise(List<EbMarchandise> listEbMarchandise, String typeContainer) {
        Long count = listEbMarchandise
            .stream().filter(marchandise -> typeContainer.equals(marchandise.getxEbTypeConteneur())).count();
        return count;
    }
}
