package com.adias.mytowereasy.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.TypeFactory;


public class JsonUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtils.class);
    public static final ObjectMapper staticMapper = new ObjectMapper();

    public static ObjectMapper getObjectMapperInstance() {
        return new ObjectMapper();
    }

    public static <T> List<T> deserializeList(String serializedList, Class<T> objectInListClass)
        throws JsonMappingException,
        JsonProcessingException {
        final TypeFactory factory = getObjectMapperInstance().getTypeFactory();
        final JavaType listOfT = factory.constructCollectionType(List.class, objectInListClass);

        return getObjectMapperInstance().readValue(serializedList, listOfT);
    }

    public static Map<String, Object> deserializeMap(String serializedMap) {

        try {
            final TypeFactory factory = getObjectMapperInstance().getTypeFactory();
            final JavaType mapOfT = factory.constructMapType(HashMap.class, String.class, Object.class);

            return getObjectMapperInstance().readValue(serializedMap, mapOfT);
        } catch (JsonProcessingException e) {
            LOGGER.warn("Can't deserialize list : {0}", e.getMessage());
            return null;
        }

    }

    public static <T> List<T>
        deserializeList(String serializedList, Class<T> objectInListClass, List<T> returnIfException)

    {

        try {
            return deserializeList(serializedList, objectInListClass);
        } catch (JsonProcessingException e) {
            LOGGER.warn("Can't deserialize list : {0}", e.getMessage());
            return returnIfException;
        }

    }

    public static String serializeToString(Object objectToSerialize) throws JsonProcessingException {
        return getObjectMapperInstance().writeValueAsString(objectToSerialize);
    }

    public static String serializeToString(Object objectToSerialize, String returnIfException) {

        try {
            return serializeToString(objectToSerialize);
        } catch (JsonProcessingException e) {
            LOGGER.warn("Can't serialize object {0}, error : {1}", objectToSerialize, e.getMessage());
            return returnIfException;
        }

    }

    public static <T> List<T> checkJsonListObject(List<T> items, Class<T> tClass) {
        if (items == null) return null;

        List<T> converted = items;

        ObjectMapper mapper = new ObjectMapper();
        CollectionType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, tClass);

        try {

            if (items.isEmpty()) {
                converted = mapper.readValue("[]", listType);
            }
            else {
                String name = items.get(0).getClass().getName();

                if (name.contentEquals("java.util.LinkedHashMap") || name.contentEquals("java.util.LinkedTreeMap")) {
                    converted = mapper.convertValue(items, listType);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return converted;
    }

    public static void logJson(Logger logger, Object obj, String msg) throws JsonProcessingException {
        logger.debug(msg);

        // get object as a json string
        if (obj != null) {
            String jsonStr = staticMapper.writeValueAsString(obj);
            // Displaying JSON String
            logger.debug(jsonStr);
        }

    }
}
