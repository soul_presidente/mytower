package com.adias.mytowereasy.util.document;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import org.docx4j.Docx4J;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.docx4j.openpackaging.packages.WordprocessingMLPackage;
import org.springframework.web.multipart.MultipartFile;
import org.wickedsource.docxstamper.DocxStamper;
import org.wickedsource.docxstamper.DocxStamperConfiguration;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.template.function.*;
import com.adias.mytowereasy.util.MultipartFileObject;


public class DocumentUtils {
    /**
     * Convert a Microsoft Word (.docx) File to PDF
     * 
     * @param docxFile
     *            .docx file input
     * @param pdfFile
     *            File pointing to the destination path where the .pdf file will
     *            be created
     * @throws Docx4JException
     * @throws IOException
     */
    public static void convertDocxToPdf(Path docxFile, Path pdfFile) throws Docx4JException, IOException {

        try (InputStream fis = Files.newInputStream(docxFile)) {
            convertDocxToPdf(fis, pdfFile);
        }

    }

    /**
     * Convert a Microsoft Word (.docx) File to PDF
     * 
     * @param docxStream
     *            .docx input stream
     * @param pdfFile
     *            File pointing to the n path where the .pdf file will be
     *            created
     * @throws Docx4JException
     * @throws IOException
     */
    public static void convertDocxToPdf(InputStream docxFile, Path pdfFile) throws Docx4JException, IOException {

        try (OutputStream pdfOutputStream = Files.newOutputStream(pdfFile)) {
            WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(docxFile);
            Docx4J.toPDF(wordMLPackage, pdfOutputStream);
        }

    }

    /**
     * Apply a model to a Microsoft Word (.docx) template file<br/>
     * <br/>
     * docx-stamper is the template engine used
     * <b><a href="https://github.com/thombergs/docx-stamper">see templating
     * documentation</a></b>
     * 
     * @param docx
     *            Stream to existing .docx file that will be used
     *            for template
     * @param model
     *            The object that will be used as POJO to the template
     * @param destStream
     *            .docx file output
     * @throws IOException
     */
    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    public static void
        applyDocxTemplate(InputStream docxFile, Object model, OutputStream destStream, EbUser connectedUser)
            throws IOException {

        try {
            FormatBoolServiceImpl formatBoolServiceImpl = new FormatBoolServiceImpl();
            formatBoolServiceImpl.setConnectedUser(connectedUser);

            FormatDateServiceImpl formatDateServiceImpl = new FormatDateServiceImpl();
            formatDateServiceImpl.setConnectedUser(connectedUser);

            MandatoryServiceImpl mandatoryServiceImpl = new MandatoryServiceImpl();
            mandatoryServiceImpl.setConnectedUser(connectedUser);

            DocxStamper stamper = new DocxStamperConfiguration()
                .exposeInterfaceToExpressionLanguage(FormatDateService.class, formatDateServiceImpl)
                .exposeInterfaceToExpressionLanguage(FormatBoolService.class, formatBoolServiceImpl)
                .exposeInterfaceToExpressionLanguage(MandatoryService.class, mandatoryServiceImpl).build();
            stamper.stamp(docxFile, model, destStream);
            destStream.close();
        } finally {
            destStream.close();
        }

    }

    /**
     * Apply a model to a Microsoft Word (.docx) template file<br/>
     * <br/>
     * docx-stamper is the template engine used
     * <b><a href="https://github.com/thombergs/docx-stamper">see templating
     * documentation</a></b>
     * 
     * @param docx
     *            Stream to existing .docx file that will be used
     *            for template
     * @param model
     *            The object that will be used as POJO to the template
     * @param destDocxFile
     *            File pointing to the destiation path where the .docx file with
     *            template applied will
     *            be created
     * @throws IOException
     */
    public static void applyDocxTemplate(InputStream docxFile, Object model, Path destDocxFile, EbUser connectedUser)
        throws IOException {

        try (OutputStream out = Files.newOutputStream(destDocxFile)) {
            applyDocxTemplate(docxFile, model, out, connectedUser);
        }

    }

    public static MultipartFile convertFileToMultipartFileObject(Path file, String filename, String mimetype)
        throws IOException {
        Path path = file.toRealPath();
        byte[] content = Files.readAllBytes(path);
        return new MultipartFileObject(filename, filename, mimetype, content);
    }

    public static MultipartFile convertFileToMultipartFileObject(Path file, String mimetype) throws IOException {
        return convertFileToMultipartFileObject(file, file.getFileName().toString(), mimetype);
    }
}
