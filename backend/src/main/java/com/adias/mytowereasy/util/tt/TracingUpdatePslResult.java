package com.adias.mytowereasy.util.tt;

import java.util.List;


public class TracingUpdatePslResult {
    private Boolean result;
    private List<Integer> listReject;

    public TracingUpdatePslResult() {
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public List<Integer> getListReject() {
        return listReject;
    }

    public void setListReject(List<Integer> listReject) {
        this.listReject = listReject;
    }
}
