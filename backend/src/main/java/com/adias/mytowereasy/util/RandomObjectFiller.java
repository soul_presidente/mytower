/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.util;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.util.Random;
import java.util.UUID;


public class RandomObjectFiller {
    private Random random = new Random();

    public <T> T createAndFill(Class<T> clazz) {
        T instance = null;
        Field f = null;

        try {
            instance = clazz.newInstance();

            for (Field field: clazz.getDeclaredFields()) {
                f = field;

                if (!java.lang.reflect.Modifier.isStatic(field.getModifiers())) {
                    field.setAccessible(true);
                    Object value = getRandomValueForField(field);
                    field.set(instance, value);
                }

            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            System.out.println(f);
            e.printStackTrace();
        }

        return instance;
    }

    private Object getRandomValueForField(Field field) throws Exception {
        Class<?> type = field.getType();

        if (type.isEnum()) {
            Object[] enumValues = type.getEnumConstants();
            return enumValues[random.nextInt(enumValues.length)];
        }
        else if (type.equals(Integer.TYPE) || type.equals(Integer.class)) {
            return random.nextInt();
        }
        else if (type.equals(Long.TYPE) || type.equals(Long.class)) {
            return random.nextLong();
        }
        else if (type.equals(Double.TYPE) || type.equals(Double.class)) {
            return random.nextDouble();
        }
        else if (type.equals(Float.TYPE) || type.equals(Float.class)) {
            return random.nextFloat();
        }
        else if (type.equals(String.class)) {
            return UUID.randomUUID().toString();
        }
        else if (type.equals(BigInteger.class)) {
            return BigInteger.valueOf(random.nextInt());
        }

        return createAndFill(type);
    }
}
