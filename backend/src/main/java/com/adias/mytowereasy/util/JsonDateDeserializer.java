package com.adias.mytowereasy.util;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;


public class JsonDateDeserializer implements JsonDeserializer<Date> {
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
        throws JsonParseException {
        Date dt = null;
        String dtStr = json.getAsJsonPrimitive().getAsString();

        try {
            dt = new Date(json.getAsJsonPrimitive().getAsLong());
        } catch (Exception e) {
        }

        if (dt == null && dtStr != null) {
            dt = tryFormat(dtStr, "MMM dd, yyyy HH:mm:ss a");
        }

        if (dt == null && dtStr != null) {
            dt = tryFormat(dtStr, "dd/MM/yyyy");
        }

        if (dt == null && dtStr != null) {
            dt = tryFormat(dtStr, "yyyy-MM-dd");
        }

        if (dt == null && dtStr != null) System.out.println("");

        return dt;
    }

    public Date tryFormat(String dtStr, String format) {
        Date dt = null;

        SimpleDateFormat formatter = new SimpleDateFormat(format, Locale.ENGLISH);

        // SimpleDateFormat formatter = new SimpleDateFormat("EEEE, MMM dd, yyyy
        // HH:mm:ss a");
        // String dateInString = "Friday, Jun 7, 2013 12:10:56 PM";
        try {
            dt = formatter.parse(dtStr);
        } catch (Exception e) {
        }

        return dt;
    }
}
