package com.adias.mytowereasy.util;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;


public class ObjectDeserializer<T> extends JsonDeserializer<T> {
    @Override
    public T deserialize(JsonParser jp, DeserializationContext deserializationContext) throws IOException {
        ObjectMapper mapper = (ObjectMapper) jp.getCodec();
        JsonNode node = mapper.readTree(jp);
        String json = mapper.writeValueAsString(node);

        if (json == null) return null;

        Type tk = null;

        if (json.contains("ebTtSchemaPslNum")) {
            tk = EbTtSchemaPsl.class;
        }

        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDateDeserializer()).create();

        EbTtSchemaPsl schema = gson.fromJson(json, tk);
        T ts = (T) schema;

        return ts;
    }

    public static <T> List<T> cloneJsonListObject(List<T> items, Type typeToken) {
        if (items == null) return null;

        if (items.isEmpty()) return (new Gson()).fromJson("[]", typeToken);

        try {
            Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDateDeserializer()).create();

            String json = gson.toJson(items);
            List<T> convJson = gson.fromJson(json, typeToken);

            return convJson;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return items;
    }

    public static <T> List<T> checkJsonListObject(List<T> items, Type typeToken) {
        if (items == null) return null;

        if (items.isEmpty()) return (new Gson()).fromJson("[]", typeToken);

        try {
            String name = items.get(0).getClass().getName();

            if (name.contentEquals("java.util.LinkedHashMap") || name.contentEquals("java.util.LinkedTreeMap")) {
                Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDateDeserializer()).create();

                String json = gson.toJson(items);
                List<T> convJson = gson.fromJson(json, typeToken);

                return convJson;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return items;
    }

    public static <T> List<T> checkJsonListObject(List<T> items, Class<T> tClass) {
        if (items == null) return null;

        List<T> converted = items;

        ObjectMapper mapper = new ObjectMapper();
        CollectionType listType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, tClass);

        try {

            if (items.isEmpty()) {
                converted = mapper.readValue("[]", listType);
            }
            else {
                String name = items.get(0).getClass().getName();

                if (name.contentEquals("java.util.LinkedHashMap") || name.contentEquals("java.util.LinkedTreeMap")) {
                    converted = mapper.convertValue(items, listType);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return converted;
    }
}
