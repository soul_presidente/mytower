/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;

import com.adias.mytowereasy.model.ExcelToHeaderConfig;


public class ExcelUtils {
    public static SXSSFSheet loadSheetWithData(
        SXSSFSheet sheet,
        List<Map<String, String[]>> data,
        Map<String, Integer> cfg,
        List<ExcelToHeaderConfig> headerCfg,
        CellStyle cellBorderStyle,
        CellStyle boldCellStyle,
        CellStyle dateCellStyle,
        boolean generateTopHeaderColumn,
        Optional<Map<String, String>> listTypeResult,
        Optional<String> optionalDateFormat) {
        if (sheet == null || data == null || data.isEmpty() || data.get(0).isEmpty()) return sheet;

        Map<String, String> typeResult = listTypeResult.isPresent() ? listTypeResult.get() : null;

        if (cfg.isEmpty() && !data.isEmpty() && !data.get(0).isEmpty()) {
            int i = 1;

            for (String key: data.get(0).keySet()) {
                cfg.put(key, i++);
            }

        }

        // sort cfg
        Map<String, Integer> oCfg = new LinkedHashMap<String, Integer>();
        List<Map.Entry<String, Integer>> tmp = new LinkedList<Map.Entry<String, Integer>>(cfg.entrySet());
        tmp
            .stream()
            .sorted((it1, it2) -> it1.getValue() > it2.getValue() ? 1 : it1.getValue() < it2.getValue() ? -1 : 0);
        for (Map.Entry<String, Integer> entry: tmp) oCfg.put(entry.getKey(), entry.getValue());

        // writing columns
        SXSSFRow row = sheet.createRow(0);

        if (generateTopHeaderColumn) {
            /* TOP COLUMNS HEADER */
            SXSSFCell cell = null;
            int compteur = 0, headStart = 0, currentEnd = 0, oldEnd = 0;

            for (ExcelToHeaderConfig config: headerCfg) {

                if (compteur > 0) {
                    headStart = oldEnd + 1;
                }

                currentEnd = headStart + config.getNumberOfRow() - 1;
                oldEnd = currentEnd;
                cell = row.createCell(headStart);
                cell.setCellStyle(config.getCellStyle());
                cell.setCellValue(config.getTitle());

                if (config.getNumberOfRow() > 1) {
                    sheet.addMergedRegion(new CellRangeAddress(0, 0, headStart, currentEnd));
                }

                compteur++;
            }

            row = sheet.createRow(1);
        }

        /* Field's names */
        int j = 0;
        SXSSFCell cell = null;

        for (String c: oCfg.keySet()) {
            cell = row.createCell(j++);
            cell.setCellStyle(boldCellStyle);
            if (data.get(0) != null &&
                data.get(0).get(c) != null && data.get(0).get(c).length > 0) cell.setCellValue(data.get(0).get(c)[0]);
        }

        DateFormat dateFormat = null;

        if (optionalDateFormat.isPresent()) {
            dateFormat = new SimpleDateFormat(optionalDateFormat.get());
        }

        if (data != null && !data.isEmpty()) {
            int i = (generateTopHeaderColumn) ? 2 : 1;

            for (Map<String, String[]> map: data) {
                row = sheet.createRow(i++);
                j = 0;

                for (String c: oCfg.keySet()) {
                    cell = row.createCell(j++);
                    cell.setCellStyle(cellBorderStyle);

                    if (map.get(c) != null && map.get(c).length > 1 && map.get(c)[1] != null) {

                        if (typeResult != null &&
                            typeResult.containsKey(c) && typeResult.get(c) == Date.class.getTypeName()) {

                            try {

                                if (!map.get(c)[1].isEmpty()) {
                                    Date date = dateFormat.parse(map.get(c)[1]);
                                    cell.setCellValue(date);
                                }

                                cell.setCellStyle(dateCellStyle);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        }
                        else {
                            if (map.get(c)[1].length() < 10000) cell.setCellValue(map.get(c)[1]);
                            else cell.setCellValue(map.get(c)[1].substring(0, 1000) + "...");
                        }

                    }

                }

            }

        }

        return sheet;
    }
}
