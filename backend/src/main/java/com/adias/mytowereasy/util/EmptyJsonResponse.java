/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.util;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@JsonSerialize
public class EmptyJsonResponse {
}
