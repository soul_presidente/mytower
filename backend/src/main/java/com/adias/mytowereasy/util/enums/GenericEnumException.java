package com.adias.mytowereasy.util.enums;

public class GenericEnumException extends RuntimeException {
    private static final long serialVersionUID = -7114177595477400419L;

    public GenericEnumException() {
        super();
    }

    public GenericEnumException(
        String message,
        Throwable cause,
        boolean enableSuppression,
        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public GenericEnumException(String message, Throwable cause) {
        super(message, cause);
    }

    public GenericEnumException(String message) {
        super(message);
    }

    public GenericEnumException(Throwable cause) {
        super(cause);
    }
}
