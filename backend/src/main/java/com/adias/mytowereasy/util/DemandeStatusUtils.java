package com.adias.mytowereasy.util;

import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.Module;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;


public class DemandeStatusUtils {
    
    public static String getStatusLibelleFromEbDemande(EbDemande ebDemande, Integer module, EbUser connectedUser) {

        if (ebDemande.getxEcStatut() != null) {
            Integer status = ebDemande.getxEcStatut();
            String statusStr = "";

            if (ebDemande.getxEcCancelled() != null &&
                ebDemande.getxEcCancelled().equals(Enumeration.CancelStatus.CANECLLED.getCode())) {
                statusStr = Enumeration.StatutDemande.CAN.getLibelle();

                if (ebDemande.getxEbDemandeGroup() != null) {
                    statusStr = "PRICING_BOOKING.CANCELLED_FOR_CONSOLIDATION";
                }

            }

            else {

                if (Module.PRICING.getCode().equals(module) ||
                    (Module.ORDRE_TRANSPORT.getCode().equals(module) &&
                        ebDemande.getxEcStatut() <= StatutDemande.FIN.getCode())) {

                    if (ebDemande.getxEcStatut() >= StatutDemande.WPU.getCode()) {
                        status = StatutDemande.FIN.getCode();
                        if (ebDemande
                            .getxEcStatut().equals(
                                StatutDemande.DLVRY.getCode())) statusStr = "PRICING_BOOKING.CONFIRMED_AND_COMPLETED";
                    }

                    if (connectedUser.isTransporteur() ||
                        connectedUser.isTransporteurBroker() &&
                            (ebDemande.getxEbUserOriginCustomsBroker() == null ||
                                !connectedUser
                                    .getEbUserNum().equals(ebDemande.getxEbUserOriginCustomsBroker().getEbUserNum())) &&
                            (ebDemande.getxEbUserDestCustomsBroker() == null ||
                                !connectedUser
                                    .getEbUserNum().equals(ebDemande.getxEbUserDestCustomsBroker().getEbUserNum()))) {

                        if (isDemandeNotAwardedToUser(ebDemande, connectedUser)) {
                            status = null;
                            statusStr = Enumeration.StatutDemande.NAW.getLibelle();
                        }
                        else if (ebDemande.getxEcStatut() < StatutDemande.FIN.getCode()) {
                            status = ebDemande.getStatusQuote() != null ?
                                ebDemande.getStatusQuote() :
                                Enumeration.StatutDemande.INP.getCode();
                        }

                    }

                }
                else {

                    if (StatutDemande.DLVRY.getCode().equals(ebDemande.getxEcStatut())) {
                        if (ebDemande.getLibelleLastPsl()
                            != null) statusStr = "Completed (" + ebDemande.getLibelleLastPsl() + ")";
                        else statusStr = "Completed";
                    }

                }

            }

            if ((statusStr == null || statusStr.isEmpty()) &&
                (status == null || status == 0)) statusStr = StatutDemande.getLibelle(ebDemande.getxEcStatut());
            else if (statusStr == null || statusStr.isEmpty()) statusStr = StatutDemande.getLibelle(status);

            return statusStr;
        }
        else return null;

    }

    public static boolean isDemandeNotAwardedToUser(EbDemande ebDemande, EbUser connectedUser) {
        return ebDemande.getxEcStatut() >= Enumeration.StatutDemande.WPU.getCode() &&
            ebDemande.getExEbDemandeTransporteurFinal() != null &&
            (isSuperAdminAndHasNotSameCompagnieAsDemandeTransporteur(ebDemande, connectedUser) ||
                isAdminAndNotSuperAdminAndHasNotSameEtablissementAsDemandeTransporteur(ebDemande, connectedUser) ||
                isNotSuperAdminAndNotAdminAndNotDemandeTransporteur(ebDemande, connectedUser));
    }

    private static boolean
        isNotSuperAdminAndNotAdminAndNotDemandeTransporteur(EbDemande ebDemande, EbUser connectedUser) {
        return !connectedUser.isSuperAdmin() &&
            !connectedUser.isAdmin() &&
            !ebDemande.getExEbDemandeTransporteurFinal().getEbUserNum().equals(connectedUser.getEbUserNum());
    }

    private static boolean isAdminAndNotSuperAdminAndHasNotSameEtablissementAsDemandeTransporteur(
        EbDemande ebDemande,
        EbUser connectedUser) {
        return !connectedUser.isSuperAdmin() &&
            connectedUser.isAdmin() &&
            !ebDemande
                .getExEbDemandeTransporteurFinal().getEbEtablissement().getEbEtablissementNum()
                .equals(connectedUser.getEbEtablissement().getEbEtablissementNum());
    }

    private static boolean
        isSuperAdminAndHasNotSameCompagnieAsDemandeTransporteur(EbDemande ebDemande, EbUser connectedUser) {
        return connectedUser.isSuperAdmin() &&
            !ebDemande
                .getExEbDemandeTransporteurFinal().getEbCompagnie().getEbCompagnieNum()
                .equals(connectedUser.getEbCompagnie().getEbCompagnieNum());
    }
}
