package com.adias.mytowereasy.util;

public class KibanaResponse {
    private String access_token;
    private String kibana_url;

    public KibanaResponse(String access_token, String kibana_url) {
        super();
        this.access_token = access_token;
        this.kibana_url = kibana_url;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getKibana_url() {
        return kibana_url;
    }

    public void setKibana_url(String kibana_url) {
        this.kibana_url = kibana_url;
    }
}
