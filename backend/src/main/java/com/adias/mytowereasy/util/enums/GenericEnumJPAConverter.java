package com.adias.mytowereasy.util.enums;

import javax.persistence.AttributeConverter;

import com.adias.mytowereasy.util.GenericEnum;


public abstract class GenericEnumJPAConverter<T extends Enum<T> & GenericEnum>
    implements AttributeConverter<T, Integer> {
    private final Class<T> clazz;

    public GenericEnumJPAConverter(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public Integer convertToDatabaseColumn(T obj) {
        return (obj != null) ? obj.getCode() : null;
    }

    @Override
    public T convertToEntityAttribute(Integer dbData) {
        T[] enums = clazz.getEnumConstants();

        for (T e: enums) {

            if (e.getCode().equals(dbData)) {
                return e;
            }

        }

        return null;
    }
}
