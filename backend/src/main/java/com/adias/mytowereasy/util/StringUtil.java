package com.adias.mytowereasy.util;

public class StringUtil {
    public static boolean isStringYes(String term) {
        return term.equalsIgnoreCase("yes") || term.equalsIgnoreCase("oui");
    }

    public static boolean isStringNo(String term) {
        return term.equalsIgnoreCase("no") || term.equalsIgnoreCase("non");
    }
}
