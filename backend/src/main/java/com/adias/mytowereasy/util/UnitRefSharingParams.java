package com.adias.mytowereasy.util;

public class UnitRefSharingParams {
    public final static int MAX_UNITS_REFS_SUPPORTED = 5;
    public final static String UNITS_REF_SUFFIX = "...";
    public final static String EMPTY_STRING = "";
    public final static String UNITS_REF_SEPARATOR = ", ";

}
