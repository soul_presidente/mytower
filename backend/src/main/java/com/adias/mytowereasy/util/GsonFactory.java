package com.adias.mytowereasy.util;

import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


/**
 * @deprecated Gson will be removed from MyTower
 * @see JsonUtils
 */
@Deprecated
public class GsonFactory {
    public static Gson newInstance() {
        return new GsonBuilder().registerTypeAdapter(Date.class, new JsonDateDeserializer()).create();
    }
}
