package com.adias.mytowereasy.util;

import com.adias.mytowereasy.api.util.MyTowerErrorMsg;


public class ImportEnMasseUtils {
    /**
     * @param message:
     *            description of the consistency error during processing.
     * @return : Un objet MessageError qui porte la description de l'erreur.
     */

    public static MyTowerErrorMsg setErrorMessage(String message) {
        MyTowerErrorMsg inner = new MyTowerErrorMsg(message);
        return inner;
    }
}
