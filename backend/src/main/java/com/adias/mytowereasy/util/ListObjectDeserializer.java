package com.adias.mytowereasy.util;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.util.StdConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.dto.EbTtCompagnyPslLightDto;


public class ListObjectDeserializer<T> extends StdConverter<String, List<T>> {
    @Override
    public List<T> convert(String json) {
        if (json == null) return null;

        Type tk = null;

        if (json.contains("ebTtCompanyPslNum")) {
            tk = new TypeToken<List<EbTtCompanyPsl>>() {
            }.getType();
        }
        else if (json.contains("id") && json.contains("schemaActif")) {
            tk = new TypeToken<List<EbTtCompagnyPslLightDto>>() {
            }.getType();
        }

        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDateDeserializer()).create();
        List<T> ts = gson.fromJson(json, tk);

        return ts;
    }
}
