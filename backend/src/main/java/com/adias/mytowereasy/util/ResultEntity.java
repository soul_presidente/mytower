package com.adias.mytowereasy.util;

import java.io.Serializable;

import com.adias.mytowereasy.model.Enumeration.ResultEntityType;


public class ResultEntity implements Serializable {
    private ResultEntityType type;

    private String messsage;

    private Object info;

    private Object value;

    public ResultEntity() {
        this.type = ResultEntityType.SUCCESS;
    }

    public String getMesssage() {
        return messsage;
    }

    public void setMesssage(String messsage) {
        this.messsage = messsage;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public ResultEntityType getType() {
        return type;
    }

    public void setType(ResultEntityType type) {
        this.type = type;
    }

    public Object getInfo() {
        return info;
    }

    public void setInfo(Object info) {
        this.info = info;
    }
}
