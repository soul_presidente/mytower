/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.util.email;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.adias.mytowereasy.model.EbEmail;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.repository.EbEmailRepository;
import com.adias.mytowereasy.service.ServiceConfiguration;


@Component
public class EmailSender {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailSender.class);

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    ServiceConfiguration config;

    @Autowired
    EbEmailRepository ebEmailRepository;

    @Autowired
    EmailSenderPoolManager pool;

    @Value("${mytower.mail.smtp.from}")
    public String smtpFromName;

    @Value("${spring.mail.properties.mail.smtp.from}")
    public String smtpFromAdress;

    /**
     * @param email
     *            : contient la liste des emails à envoyer
     * @return
     */
    public EmailStatus send(Email email) {

        try {
            MimeMessage mail = javaMailSender.createMimeMessage();
            Address dd = new InternetAddress(smtpFromAdress, smtpFromName);
            mail.setFrom(dd);
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);

            helper.setTo(StringUtils.split(email.getTo(), ";"));
            helper.setSubject(email.getSubject());
            helper.setFrom((InternetAddress) dd);
            // envoi mode html
            helper.setText(email.getText(), true);

            ajouterEmailSupportTechniqueCopie(email);

            // gestion des utilisateurs en copie
            List<String> listCopie = email.getEnCopie();

            if (email.getEnCopie() != null && !email.getEnCopie().isEmpty()) {
                helper.setCc(toStringArray(listCopie));
            }

            // gestion des emails en copie cachée
            if (email.getEnCopieCachee() != null && !email.getEnCopieCachee().isEmpty()) {
                helper.setBcc(toStringArray(email.getEnCopieCachee()));
            }

            System.out.println("getTo() : " + email.getTo());
            ExecutorService executor = pool.getExecutor();
            CompletableFuture.supplyAsync(() -> {

                try {
                    Integer status = this.asyncSend(mail, email);

                    if (status == 1) return email;
                    else throw new RuntimeException("Erreur d'envoie d'email");
                } catch (Exception ex) {
                    throw new RuntimeException("Erreur d'envoie d'email", ex);
                }

            }, executor).thenAcceptAsync(result -> {
                // email envoyé
            }).exceptionally(ex -> {
                EbEmail ebEmail = new EbEmail();
                ebEmail.setAdresseTo(email.getTo());
                ebEmail
                    .setAdresseCc(
                        email.getEnCopie() != null && !email.getEnCopie().isEmpty() ?
                            String.join(";", email.getEnCopie()) :
                            null);
                ebEmail
                    .setAdresseCci(
                        email.getEnCopieCachee() != null && !email.getEnCopieCachee().isEmpty() ?
                            String.join(";", email.getEnCopieCachee()) :
                            null);
                ebEmail.setDateCreation(new Date());
                ebEmail.setMessage(email.getText());
                ebEmail.setSubject(email.getSubject());
                ebEmail.setStackTrace(ExceptionUtils.getStackTrace(ex));
                ebEmail.setStatus(Enumeration.EmailStatus.EMAIL_NON_ENVOYE.getCode());

                ebEmailRepository.save(ebEmail);

                return null;
            });

            return new EmailStatus(email).success();
        } catch (Exception e) {
            LOGGER
                .error(
                    String
                        .format("Problem with sending email to: {}, error message: {}", email.getTo(), e.getMessage()));
            e.printStackTrace();
            return new EmailStatus(email).error(e.getMessage());
        }

    }

    public EmailStatus sendList(List<Email> listEmail) {

        try {
            List<MimeMessage> listMimeMessage = new ArrayList<MimeMessage>();
            Address dd = new InternetAddress(smtpFromAdress, smtpFromName);
            if (listEmail != null && !listEmail.isEmpty()) {
                listEmail.forEach(email -> {

                    try {
                        MimeMessage mail = javaMailSender.createMimeMessage();

                        mail.setFrom(dd);
                        MimeMessageHelper helper = new MimeMessageHelper(mail, true);

                        helper.setTo(StringUtils.split(email.getTo(), ";"));
                        helper.setSubject(email.getSubject());
                        // envoi mode html
                        helper.setText(email.getText(), true);
                        helper.setFrom((InternetAddress) dd);
                        ajouterEmailSupportTechniqueCopie(email);

                        // gestion des utilisateurs en copie
                        List<String> listCopie = email.getEnCopie();

                        email.setFrom(((InternetAddress) dd).getAddress());
                        if (email.getEnCopie() != null && !email.getEnCopie().isEmpty()) {
                            helper.setCc(toStringArray(listCopie));
                        }

                        // gestion des emails en copie cachée
                        if (email.getEnCopieCachee() != null && !email.getEnCopieCachee().isEmpty()) {
                            helper.setBcc(toStringArray(email.getEnCopieCachee()));
                        }

                        listMimeMessage.add(mail);
                    } catch (Exception ex) {
                        LOGGER
                            .info(
                                "Error occurred while sending email '{}' to: {} from : {} enCopie :{} enCopieCachee:{} ",
                                email.getSubject(),
                                email.getTo(),
                                email.getFrom(),
                                email.getEnCopie(),
                                email.getEnCopieCachee());
                        ex.printStackTrace();
                    }

                });

                ExecutorService executor = pool.getExecutor();
                CompletableFuture.supplyAsync(() -> {

                    try {
                        MimeMessage[] mimeArray = new MimeMessage[listMimeMessage.size()];

                        javaMailSender.send(listMimeMessage.toArray(mimeArray));

                        listEmail.forEach(email -> {
                            LOGGER
                                .info(
                                    "Email sent '{}' to: {} from : {} enCopie :{} enCopieCachee:{}",
                                    email.getSubject(),
                                    email.getTo(),
                                    email.getFrom(),
                                    email.getEnCopie(),
                                    email.getEnCopieCachee());
                        });

                        return 1;
                    } catch (Exception ex) {
                        throw new RuntimeException("Erreur d'envoie d'email", ex);
                    }

                }, executor).thenAcceptAsync(result -> {
                    // email envoyé
                }).exceptionally(ex -> {
                    EbEmail ebEmail = new EbEmail();
                    String to = String.join(";", listEmail.stream().map(it -> it.getTo()).collect(Collectors.toList()));
                    String cc = String
                        .join(
                            ";",
                            listEmail
                                .stream().map(it -> it.getEnCopie() != null ? String.join(";", it.getEnCopie()) : null)
                                .collect(Collectors.toList()));
                    String cci = String
                        .join(
                            ";",
                            listEmail
                                .stream()
                                .map(
                                    it -> it.getEnCopieCachee() != null ?
                                        String.join(";", it.getEnCopieCachee()) :
                                        null)
                                .collect(Collectors.toList()));
                    ebEmail.setAdresseTo(to);
                    ebEmail.setAdresseCc(cc);
                    ebEmail.setAdresseCci(cci);
                    ebEmail.setDateCreation(new Date());
                    ebEmail.setMessage(listEmail.get(0).getText());
                    ebEmail.setSubject(listEmail.get(0).getSubject());
                    ebEmail.setStackTrace(ExceptionUtils.getStackTrace(ex));
                    ebEmail.setStatus(Enumeration.EmailStatus.EMAIL_NON_ENVOYE.getCode());

                    ebEmailRepository.save(ebEmail);

                    ex.printStackTrace();
                    return null;
                });

                return new EmailStatus(listEmail.get(0)).success();
            }

        } catch (Exception e) {
            if (listEmail != null && !listEmail.isEmpty()) LOGGER
                .error(
                    String
                        .format(
                            "Problem with sending email to: {}, error message: {}",
                            String.join(";", listEmail.stream().map(it -> it.getTo()).collect(Collectors.toList())),
                            e.getMessage()));
            e.printStackTrace();
        }

        return new EmailStatus(new Email()).error("Email not sent, no email found");
    }

    public int asyncSend(MimeMessage mail, Email email) throws Exception {
        javaMailSender.send(mail);

        LOGGER
            .info(
                "Send email '{}' to: {} from: {}  enCopie :{} enCopieCachee:{} ",
                email.getSubject(),
                email.getTo(),
                email.getFrom(),
                email.getEnCopie(),
                email.getEnCopieCachee());

        return 1;
    }

    private String[] toStringArray(List<String> listString) {

        if (listString != null) {
            String[] arrays = new String[listString.size()];

            for (int i = 0; i < arrays.length; i++) {
                arrays[i] = listString.get(i);
            }

            return arrays;
        }

        return null;
    }

    private Address[] toAdressArray(List<String> listString) {

        if (listString != null) {
            Address[] arrays = new Address[listString.size()];

            for (int i = 0; i < arrays.length; i++) {

                try {
                    Address ad = new InternetAddress(listString.get(i));
                    arrays[i] = ad;
                } catch (AddressException e) {
                    e.printStackTrace();
                }

            }

            return arrays;
        }

        return null;
    }

    private void ajouterEmailSupportTechniqueCopie(Email email) {

        try {

            if (email.getEnCopieCachee() == null) {
                email.setEnCopieCachee(new ArrayList<String>());
            }

            String[] emails = config.getEmailSupportTechnique().split(";");

            if (emails.length > 0) {
                for (String m: emails) email.getEnCopieCachee().add(m);
            }

        } catch (Exception e) {
            e.printStackTrace();
            // Log.info(e.getMessage());
            // TODO: handle exception
        }

    }
}
