/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.util.email;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.adias.mytowereasy.model.Statiques;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
public class EmailHtmlSender {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailHtmlSender.class);

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private EbUserRepository ebUserRepository;

    @Value("${mytowereasy.url.front}")
    private String url;

    public EmailStatus send(Email email, String templateName, Context context) {
        // String lang = ebUserRepository.getLanguageByEmail(email.getTo());
        // TODO corriger ComptaMatiereServiceImpl > semi auto mail qui a été
        // forcé en français lors de la correction ici
        String lang = "en";

        if (email.getTo().contains(";")) {
            String[] params = email.getTo().split(";");
            lang = params[1];
            email.setTo(params[0]);
        }

        Locale lcl = new Locale(lang);
        context.setLocale(lcl);

        if (email.getEmailConfig() != null) {
            context.setVariable("mailId", email.getEmailConfig().getCode());
        }

        String body = templateEngine.process(templateName, context);

        email.setSubject(messageSource.getMessage(email.getSubject(), email.getArguments(), lcl));
        email.setText(body);

        return emailSender.send(email);
    }

    /**
     * Envoi le meme email à plusieurs destinataire
     *
     * @param listDestinataire
     * @param email
     * @param templateName
     * @param context
     * @param subject
     * @return
     */
    public EmailStatus sendListEmail(
        List<String> listDestinataire,
        Email email,
        String templateName,
        Context context,
        String subject,
        String... connectedUserMail) {
        context.setVariable("mailId", email.getEmailConfig().getCode());

        if (connectedUserMail != null && connectedUserMail.length > 0) listDestinataire.remove(connectedUserMail[0]);

        List<Email> listEmail = new ArrayList<Email>();

        for (String userMail: listDestinataire) {
            String lang = "en";

            if (userMail.contains(";")) {
                String[] params = userMail.split(";");
                lang = params[1];
                userMail = params[0];
            }

            Locale lcl = new Locale(lang);
            context.setLocale(lcl);

            Email newEmail = new Email();
            newEmail.setTo(userMail);
            String subjectMessage = messageSource.getMessage(subject, email.getArguments(), lcl);
            newEmail.setSubject(subjectMessage);

            newEmail.setEnCopie(email.getEnCopie());
            newEmail.setEnCopieCachee(email.getEnCopieCachee());

            String body = templateEngine.process(templateName, context);
            newEmail.setText(body);

            listEmail.add(newEmail);
        }

        emailSender.sendList(listEmail);

        return new EmailStatus(email);
    }

    /**
     * Envoi le meme email à plusieurs destinataire avec un corps different EX
     * (url for Unknown Users
     * )
     *
     * @param listDestinataire
     * @param email
     * @param templateName
     * @param context
     * @param subject
     * @return
     */
    public EmailStatus sendListEmail(
        List<String> listDestinataire,
        Email email,
        String templateName,
        Context context,
        String subject,
        SearchCriteria criteria,
        String... connectedUserMail) {
        context.setVariable("mailId", email.getEmailConfig().getCode());

        if (connectedUserMail != null && connectedUserMail.length > 0) listDestinataire.remove(connectedUserMail[0]);

        List<Email> listEmail = new ArrayList<Email>();

        for (String userMail: listDestinataire) {
            String lang = "en";

            if (userMail.contains(";")) {
                String[] params = userMail.split(";");
                lang = params[1];
                userMail = params[0];
            }

            Locale lcl = new Locale(lang);
            context.setLocale(lcl);

            Email newEmail = new Email();
            newEmail.setTo(userMail);

            String subjectMessage = messageSource.getMessage(subject, email.getArguments(), lcl);
            newEmail.setSubject(subjectMessage);

            newEmail.setEnCopie(email.getEnCopie());
            newEmail.setEnCopieCachee(email.getEnCopieCachee());

            String body = null;

            if (criteria.getListEmailsNotInPlatform() != null && !criteria.getListEmailsNotInPlatform().isEmpty()
                && criteria.getListEmailsNotInPlatform().contains(userMail)) {
                Context c = cloneContext(context);
                c
                    .setVariable(
                        "url",
                        url.concat("public/pricing/details/").concat(criteria.getEbDemandeNum().toString()) + "?"
                            + Statiques.UNKNOWN_PARAM + "=" + userMail);
                body = templateEngine.process(templateName, c);
            }
            else body = templateEngine.process(templateName, context);

            newEmail.setText(body);
            listEmail.add(newEmail);
        }

        emailSender.sendList(listEmail);

        return new EmailStatus(email);
    }

    /**
     * Cette methode sert à quoi exactement ? en a t-on besoin réellement ? il
     * faudrait essayer
     * d'utiliser à sa place la methode sendListEmail(List<String>
     * listDestinataire, Email email,
     * String
     * templateName, Context context, String subject, String...
     * connectedUserMail)
     * 
     * @param listMapEmail
     * @param connectedUserMail
     * @return EmailStatus
     */
    public EmailStatus sendListEmail(List<Map<String, Object>> listMapEmail, String... connectedUserMail) {
        List<Email> listEmail = new ArrayList<Email>();
        Email email = null;

        for (Map<String, Object> mapEmail: listMapEmail) {
            email = (Email) mapEmail.get("email");
            Context context = (Context) mapEmail.get("context");
            String templateName = (String) mapEmail.get("templateName");
            String subject = (String) mapEmail.get("subject");
            context.setVariable("mailId", email.getEmailConfig().getCode());

            String lang = "en";

            if (email.getTo().contains(";")) {
                String[] params = email.getTo().split(";");
                lang = params[1];
                email.setTo(params[0]);
            }

            Locale lcl = new Locale(lang);
            context.setLocale(lcl);

            Email newEmail = new Email();
            newEmail.setEmailConfig(email.getEmailConfig());
            newEmail.setText(email.getText());
            newEmail.setTo(email.getTo());
            newEmail.setSubject(messageSource.getMessage(subject, email.getArguments(), lcl));
            newEmail.setEnCopie(email.getEnCopie());
            newEmail.setEnCopieCachee(email.getEnCopieCachee());
            String body = templateEngine.process(templateName, context);
            newEmail.setText(body);

            listEmail.add(newEmail);
        }

        emailSender.sendList(listEmail);

        return new EmailStatus(email);
    }

    public EmailStatus sendSemiAutoMail(Email email) {
        return emailSender.send(email);
    }

    public Context cloneContext(Context contextParam) {
        Context context = null;

        if (contextParam != null) {
            context = new Context(contextParam.getLocale());
            context
                .setVariables(
                    contextParam
                        .getVariableNames().stream()
                        .collect(Collectors.toMap(Function.identity(), var -> contextParam.getVariable(var))));
        }

        return context;
    }
}
