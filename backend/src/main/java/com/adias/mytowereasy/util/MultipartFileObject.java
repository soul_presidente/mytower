package com.adias.mytowereasy.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;


public class MultipartFileObject implements MultipartFile {
    private byte[] fileContent;

    private String name;

    private String originalFilename;

    private String contentType;

    public MultipartFileObject(String name, String originalFilename, String contentType, byte[] content) {
        this.name = name;
        this.originalFilename = (originalFilename != null ? originalFilename : "");
        this.contentType = contentType;
        this.fileContent = (content != null ? content : new byte[0]);
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getOriginalFilename() {
        return this.originalFilename;
    }

    @Override
    public String getContentType() {
        return this.contentType;
    }

    @Override
    public boolean isEmpty() {
        return (this.fileContent.length == 0);
    }

    @Override
    public long getSize() {
        return this.fileContent.length;
    }

    @Override
    public byte[] getBytes() throws IOException {
        return this.fileContent;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(this.fileContent);
    }

    @Override
    public void transferTo(File dest) throws IOException, IllegalStateException {
        FileCopyUtils.copy(this.fileContent, dest);
    }

    public static byte[] convertInputStreamToByteArray(InputStream in) {
        byte[] buffer = new byte[1024];

        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            int len;

            // read bytes from the input stream and store them in buffer
            while ((len = in.read(buffer)) != -1) {
                // write bytes from the buffer into output stream
                os.write(buffer, 0, len);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return buffer;
    }
}
