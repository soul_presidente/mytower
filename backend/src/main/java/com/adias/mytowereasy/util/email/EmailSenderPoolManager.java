/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.util.email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Component
public class EmailSenderPoolManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailSenderPoolManager.class);

    @Value("${smtp.email.pool.connections.max}")
    private Integer maxConnections;

    private ExecutorService executor;


    public ExecutorService getExecutor() {
        return executor;
    }

    @PostConstruct
    private void Init() {
        executor = Executors.newFixedThreadPool(maxConnections);
    }
}
