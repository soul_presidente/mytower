package com.adias.mytowereasy.util;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;

import com.adias.mytowereasy.model.CustomFields;

public class CustomFieldsMapper {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomFieldsMapper.class);
    
    public static List<CustomFields> toModel(String customFields) {
        List<CustomFields> listCustomField = null;
        try {
            listCustomField = JsonUtils.staticMapper
                .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                });
        } catch (Exception e) {
            LOGGER.warn("Can't deserialize {0}, error : {1}", customFields, e.getMessage());
        }
        return listCustomField;
    }
    
    public static String toString(List<CustomFields> listCustomField) {
        String customFields = null;
        
        try {
            customFields = JsonUtils.staticMapper.writeValueAsString(listCustomField);
        } catch (Exception e) {
            LOGGER.warn("Can't serialize object {0}, error : {1}", listCustomField, e.getMessage());
        }

        return customFields;
    }
}
