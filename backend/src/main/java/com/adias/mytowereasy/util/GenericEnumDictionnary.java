package com.adias.mytowereasy.util;

import com.adias.mytowereasy.cptm.model.CPTMEnumeration;
import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.enumeration.StatusOrder;
import com.adias.mytowereasy.dock.model.DOCKEnumeration.OpeningPeriodType;
import com.adias.mytowereasy.enumeration.ConditionCRTypeEnum;
import com.adias.mytowereasy.enumeration.CriticalityIncident;
import com.adias.mytowereasy.enumeration.ExportControlEnum;
import com.adias.mytowereasy.enumeration.RootCauseCategoriesEnum;
import com.adias.mytowereasy.enumeration.RootCauseStatusEnum;
import com.adias.mytowereasy.model.CREnumeration;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.TtEnumeration;
import com.adias.mytowereasy.model.TypeRequestEnumeration.BlocageResponsePricing;
import com.adias.mytowereasy.model.TypeRequestEnumeration.SenseChronoPricing;
import com.adias.mytowereasy.model.enums.PlanTransEnumeration.WeekType;
import com.adias.mytowereasy.rsch.model.RSCHEnumeration.RschArrivingBefore;
import com.adias.mytowereasy.rsch.model.RSCHEnumeration.RschUnitScheduleStatus;
import com.adias.mytowereasy.template.model.TemplateModels;
import com.adias.mytowereasy.trpl.model.EnumerationTRPL.GrilleTransportCalculationMode;


public enum GenericEnumDictionnary {
    DOCK_OpeningPeriodType("DOCK.OpeningPeriodType", OpeningPeriodType.class),
    PLANTRANSP_WeekType("PlanTrans.WeekType", WeekType.class),

    Enum_TransportConfirmation("ENUM.TransportConfirmation", Enumeration.TransportConfirmation.class),

    RSCH_ArrivingBefore("RSCH.ArrivingBefore", RschArrivingBefore.class),
    RSCH_UnitScheduleStatus("RSCH.UnitScheduleStatus", RschUnitScheduleStatus.class),

    TRPL_GrilleTransportCalculationMode("TRPL.GrilleTransportCalculationMode", GrilleTransportCalculationMode.class),

    ORDER_Status("ORDER.Status", StatusOrder.class),
    DELIVERY_Status("DELIVERY.Status", StatusDelivery.class),

    CPTM_ProcedureStatus("CPTM.ProcedureStatus", CPTMEnumeration.ProcedureStatus.class),

    CUSTOMS_CustomsDeclarationStatus("CUSTOMS.CustomsDeclarationStatus", Enumeration.CustomsDeclarationStatus.class),

    Blocage_Response_Pricing("TYPE_REQUEST.BlocageResponsePricing", BlocageResponsePricing.class),
    Sense_Chrono_Pricing("TYPE_REQUEST.SenseChronoPricing", SenseChronoPricing.class),

    DOC_TemplateModels("DOC.TemplateModels", TemplateModels.class),

    ACL_RuleType("ACL.RuleType", Enumeration.ACLRuleType.class),

    NATUREDATEEVENT("TT.NatureDateEvent", TtEnumeration.NatureDateEvent.class),

    PRIORITY("CR.Priority", CREnumeration.Priority.class),

    OPERATOR("CR.Operator", CREnumeration.Operator.class),

    PSLTYPE("CR.PslType", CREnumeration.PslType.class),

    PSLDATES("CR.PslDates", CREnumeration.PslDates.class),

    RULEOPERATOR("CR.RuleOperator", CREnumeration.RuleOperator.class),

    RULETYPE("CR.RuleType", CREnumeration.RuleType.class),

    DocumentStatus("CR.DocumentStatus", CREnumeration.DocumentStatus.class),

    ConditionCRTypeEnum("CR.conditions_type", ConditionCRTypeEnum.class),

    CR_RuleTrigger("CR.RuleTrigger", CREnumeration.RuleTrigger.class),

    CRITICALITYINCIDENT("I.CRITICALITY", CriticalityIncident.class),
    ROOTCAUSECATEGORIES("I.RootCauseCategories", RootCauseCategoriesEnum.class),
    ROOTCAUSESTATUS("I.RootCauseStatus", RootCauseStatusEnum.class),

    EXPORT_CONTROL("EXPORT_CONTROL", ExportControlEnum.class);

    private String key;
    private Class<? extends GenericEnum> classEnum;

    private GenericEnumDictionnary(String key, Class<? extends GenericEnum> classEnum) {
        this.setKey(key);
        this.setClassEnum(classEnum);
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Class<? extends GenericEnum> getClassEnum() {
        return classEnum;
    }

    public void setClassEnum(Class<? extends GenericEnum> classEnum) {
        this.classEnum = classEnum;
    }
}
