package com.adias.mytowereasy.util;

public class TurboTableConfig {
    private Integer datatableCompId;

    private Integer ebUserNum;

    private String state;

    public Integer getDatatableCompId() {
        return datatableCompId;
    }

    public void setDatatableCompId(Integer datatableCompId) {
        this.datatableCompId = datatableCompId;
    }

    public Integer getEbUserNum() {
        return ebUserNum;
    }

    public void setEbUserNum(Integer ebUserNum) {
        this.ebUserNum = ebUserNum;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
