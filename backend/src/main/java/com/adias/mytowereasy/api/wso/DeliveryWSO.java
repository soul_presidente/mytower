package com.adias.mytowereasy.api.wso;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.adias.mytowereasy.api.util.WSConstants;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


/**
 * @author Syntiche DAKIMBOLI
 */
@ApiModel(value = "Delivery", description = "Model for the delivery")
public class DeliveryWSO {

    @ApiModelProperty(notes = "Id not required for creation")
    private Long deliveryId;

    @ApiModelProperty(notes = "EDI Number")
    private String numEdi;

    @DateTimeFormat(pattern = WSConstants.DATE_FORMAT_HEURE_EXEMPLE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = WSConstants.DATE_FORMAT_HEURE_EXEMPLE)
    @ApiModelProperty(
        notes = "Date the delivery was created on (not creation date in MyTower)",
        example = WSConstants.DATE_FORMAT_HEURE_EXEMPLE_VALUE)
    private Date deliveryCreationDate;

    @Valid
    @ApiModelProperty(notes = "Shipper address(the address of the shipper)", required = true)
    private AddressWSO from;

    @Valid
    @ApiModelProperty(notes = "Ship to address", required = true)
    private AddressWSO to;

    @Valid
    @ApiModelProperty(notes = "Sold to address (the physical address where customer shipments are sent)")
    private AddressWSO soldTo;

    @Valid
    private List<CustomFieldWSO> customFieldList;

    @Valid
    private List<CategoryWSO> categoryList;

    @ApiModelProperty(notes = "ID of company who own the order (default company of connected user)")
    private Integer companyId;

    @ApiModelProperty(notes = "ID of etablissement who own the order (default etablissement of connected user)")
    private Integer etablissementId;

    @ApiModelProperty(notes = "Order Reference")
    private String refOrder;

    @ApiModelProperty(notes = "ID of the order which the delivery is planified")
    private Integer orderId;

    @ApiModelProperty(
        notes = "delivery status, specified automatically, default pending",
        allowableValues = "PENDING,CONFIRMED,SHIPPED,DELIVERED,CANCELED")
    private String deliveryStatus;

    @Valid
    @NotNull
    @ApiModelProperty(notes = "Delivery lines", required = true)
    private List<DeliveryLineWSO> deliveryLines;

    @ApiModelProperty(notes = "Incoterm")
    private String incoterm;

    @ApiModelProperty(notes = "Complementary Information")
    private String complementaryInformations;

    @NotEmpty
    @Email
    @ApiModelProperty(notes = "The request owner's email", required = true, example = "example@mytower.com")
    private String requestOwnerEmail;

    @ApiModelProperty(notes = "Customer Order Reference", required = true)
    private String customerOrderReference;

    @DateTimeFormat(pattern = WSConstants.DATE_FORMAT_HEURE_EXEMPLE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = WSConstants.DATE_FORMAT_HEURE_EXEMPLE)
    @ApiModelProperty(notes = "Customer Delivery Date", example = WSConstants.DATE_FORMAT_HEURE_EXEMPLE_VALUE)
    private Date customerDeliveryDate;

    public Long getDeliveryId() {
        return deliveryId;
    }

    public void setDeliveryId(Long deliveryId) {
        this.deliveryId = deliveryId;
    }

    public String getNumEdi() {
        return numEdi;
    }

    public void setNumEdi(String numEdi) {
        this.numEdi = numEdi;
    }

    public Date getDeliveryCreationDate() {
        return deliveryCreationDate;
    }

    public void setDeliveryCreationDate(Date deliveryCreationDate) {
        this.deliveryCreationDate = deliveryCreationDate;
    }

    public AddressWSO getFrom() {
        return from;
    }

    public void setFrom(AddressWSO from) {
        this.from = from;
    }

    public AddressWSO getTo() {
        return to;
    }

    public void setTo(AddressWSO to) {
        this.to = to;
    }

    public AddressWSO getSoldTo() {
        return soldTo;
    }

    public void setSoldTo(AddressWSO soldTo) {
        this.soldTo = soldTo;
    }

    public List<CustomFieldWSO> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<CustomFieldWSO> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public List<CategoryWSO> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<CategoryWSO> categoryList) {
        this.categoryList = categoryList;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getEtablissementId() {
        return etablissementId;
    }

    public void setEtablissementId(Integer etablissementId) {
        this.etablissementId = etablissementId;
    }

    public String getRefOrder() {
        return refOrder;
    }

    public void setRefOrder(String refOrder) {
        this.refOrder = refOrder;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public List<DeliveryLineWSO> getDeliveryLines() {
        return deliveryLines;
    }

    public void setDeliveryLines(List<DeliveryLineWSO> deliveryLines) {
        this.deliveryLines = deliveryLines;
    }

    public String getIncoterm() {
        return incoterm;
    }

    public void setIncoterm(String incoterm) {
        this.incoterm = incoterm;
    }

    public String getComplementaryInformations() {
        return complementaryInformations;
    }

    public void setComplementaryInformations(String complementaryInformations) {
        this.complementaryInformations = complementaryInformations;
    }

    public String getRequestOwnerEmail() {
        return requestOwnerEmail;
    }

    public void setRequestOwnerEmail(String requestOwnerEmail) {
        this.requestOwnerEmail = requestOwnerEmail;
    }

    public String getCustomerOrderReference() {
        return customerOrderReference;
    }

    public void setCustomerOrderReference(String customerOrderReference) {
        this.customerOrderReference = customerOrderReference;
    }

    public Date getCustomerDeliveryDate() {
        return customerDeliveryDate;
    }

    public void setCustomerDeliveryDate(Date customerDeliveryDate) {
        this.customerDeliveryDate = customerDeliveryDate;
    }
}
