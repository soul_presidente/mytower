package com.adias.mytowereasy.api.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.api.util.MyTowerErrorMsg;
import com.adias.mytowereasy.api.wso.DeliveryWSO;
import com.adias.mytowereasy.api.wso.SearchCriteriaDeliveryWSO;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.mapper.DeliveryWSOMapper;
import com.adias.mytowereasy.delivery.model.mapper.SearchCriteriaMapper;
import com.adias.mytowereasy.delivery.repository.EbLivraisonLineRepository;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.service.DeliveryService;
import com.adias.mytowereasy.delivery.service.OrderService;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.dto.EbOrderPlanificationDTO;
import com.adias.mytowereasy.dto.mapper.DeliveryMapper;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.model.EbTypeUnit;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.repository.EbPartyRepository;
import com.adias.mytowereasy.repository.EbTypeUnitRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.util.JsonUtils;



@Service
public class DeliveryAPIService {
    final static String REF_PREFIX = "-V";

    @Autowired
    OrderService orderService;

    @Autowired
    DeliveryService deliveryService;

    @Autowired
    SearchCriteriaMapper searchCriteriaMapper;

    @Autowired
    DeliveryWSOMapper deliveryWSOMapper;

    @Autowired
    DeliveryMapper deliveryMapper;

    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    MessageSource messageSource;

    @Autowired
    EbTypeUnitRepository ebTypeUnitRepository;

    @Autowired
    EbPartyRepository ebPartyRepository;

    @Autowired
    EbLivraisonRepository ebLivraisonRepository;

    @Autowired
    EbLivraisonLineRepository ebLivraisonLineRepository;

    public DeliveryWSO getDelivery(SearchCriteriaDeliveryWSO searchCriteriaDeliveryWSO) {
        SearchCriteriaDelivery searchCriteriaDelivery = searchCriteriaMapper
            .deliverySearchCriteriaDtoToModel(searchCriteriaDeliveryWSO);
        EbLivraison delivery = deliveryService.getDelivery(searchCriteriaDelivery);

        if (delivery == null) {
            return null;
        }

        return deliveryWSOMapper.getWSOFromEntity(delivery);
    }

    public DeliveryWSO createDelivery(DeliveryWSO wso) {
        EbLivraison delivery = new EbLivraison();
        delivery = deliveryWSOMapper.fillEntityFromWSO(delivery, wso);

        if (delivery.getCustomerOrderReference() == null) {
            throw new MyTowerException("Customer Dilevery Reference is null");
        }
        if (StringUtils.isBlank(delivery.getEbDelReference())) throw new MyTowerException("Reference must not be null");

        SearchCriteriaDelivery deliverySearchCriteria = deliveryMapper.deliveryToSearchCriteriaDelivery(delivery);
        EbLivraison dbDelivery = deliveryService.getDelivery(deliverySearchCriteria);

        if (dbDelivery != null) {
            throw new MyTowerException("Customer Dilevery Reference Already exists");
        }

        SearchCriteriaOrder orderSearchCriteria = new SearchCriteriaOrder();
        orderSearchCriteria.setCustomerOrderReference(delivery.getCustomerOrderReference());
        orderSearchCriteria.setEbCompagnieNum(delivery.getxEbCompagnie().getEbCompagnieNum());
        EbOrder order = orderService.getOrder(orderSearchCriteria);
        delivery.setOrderCreationDate(order.getCustomerCreationDate());
        delivery.setxEbDelOrder(order);

        return deliveryWSOMapper.getWSOFromEntity(this.saveDelivery(delivery));
    }

    public DeliveryWSO updateDelivery(DeliveryWSO wso) {
        EbLivraison delivery = new EbLivraison();
        delivery = deliveryWSOMapper.fillEntityFromWSO(delivery, wso);

        if (delivery.getCustomerOrderReference() == null) {
            throw new MyTowerException("Customer Dilevery Reference is null");
        }

        SearchCriteriaDelivery deliverySearchCriteria = deliveryMapper.deliveryToSearchCriteriaDelivery(delivery);
        EbLivraison dbDelivery = deliveryService.getDelivery(deliverySearchCriteria);

        if (dbDelivery == null) {
            throw new MyTowerException("Dilevery not found");
        }

        delivery.setEbDelLivraisonNum(dbDelivery.getEbDelLivraisonNum());
        delivery.setxEbDelOrder(dbDelivery.getxEbDelOrder());

        if (dbDelivery.getxEbDelOrder() != null) {
            delivery.setOrderCreationDate(dbDelivery.getxEbDelOrder().getCustomerCreationDate());
        }

        // we do this to update EbParty if already exist && not to create new EbParty
        EbParty partyOrigin = dbDelivery.getxEbPartyOrigin();
        EbParty partyDestination = dbDelivery.getxEbPartyDestination();
        EbParty partySale = dbDelivery.getxEbPartySale();

        if (partyOrigin != null) {
            delivery.getxEbPartyOrigin().setEbPartyNum(partyOrigin.getEbPartyNum());
        }

        if (partyDestination != null) {
            delivery.getxEbPartyDestination().setEbPartyNum(partyDestination.getEbPartyNum());
        }

        if (partySale != null) {
            delivery.getxEbPartySale().setEbPartyNum(partySale.getEbPartyNum());
        }

        ebLivraisonLineRepository.deleteByxEbDelLivraison_EbDelLivraisonNum(delivery.getEbDelLivraisonNum());

        return deliveryWSOMapper.getWSOFromEntity(this.saveDelivery(delivery));
    }

    private EbLivraison saveDelivery(EbLivraison delivery) {

        if (delivery.getxEbDelLivraisonLine() == null || delivery.getxEbDelLivraisonLine().isEmpty()) {
            throw new MyTowerException("delivery must have at least one line");
        }

        if (delivery.getEbDelLivraisonNum() == null && delivery.getDeliveryCreationDate() == null) {
            delivery.setDeliveryCreationDate(new Date());
        }
        
        delivery.getxEbDelLivraisonLine().forEach(deliveryLine -> {

            if (deliveryLine.getUnitType() != null) {
                EbTypeUnit typeUnite = ebTypeUnitRepository.findFirstByCode(deliveryLine.getUnitType());
                deliveryLine.setxEbtypeOfUnit(typeUnite);
            }

        });

        ebPartyRepository.save(delivery.getxEbPartyOrigin());
        ebPartyRepository.save(delivery.getxEbPartyDestination());

        if (delivery.getxEbPartySale() != null) {
            ebPartyRepository.save(delivery.getxEbPartyOrigin());
        }

        List<EbLivraisonLine> deliveryLines = delivery.getxEbDelLivraisonLine();
        delivery.setxEbDelLivraisonLine(null);
        delivery = ebLivraisonRepository.save(delivery);

        for (EbLivraisonLine deliveryLine: deliveryLines) {
            deliveryLine.setxEbDelLivraison(delivery);

            if (CollectionUtils.isNotEmpty(deliveryLine.getListCustomsFields())) {
                String customFields = JsonUtils.serializeToString(deliveryLine.getListCustomsFields(), null);
                deliveryLine.setCustomFields(customFields);
            }
        }

        deliveryLines = ebLivraisonLineRepository.saveAll(deliveryLines);

        delivery.setxEbDelLivraisonLine(deliveryLines);

        EbUser connectedUser = connectedUserService.getCurrentUser();
        String action = messageSource.getMessage("deliveryHasBeenUpdated", null, connectedUser.getLocaleFromLanguage());
        deliveryService
            .historizeDeliveryAction(
                delivery.getEbDelLivraisonNum().intValue(),
                Enumeration.IDChatComponent.DELIVERY.getCode(),
                Enumeration.Module.DELIVERY_MANAGEMENT.getCode(),
                action,
                connectedUserService.getCurrentUser().getNomPrenom(),
                null);

        return delivery;
    }

    public List<DeliveryWSO> create(List<DeliveryWSO> deliveryListToSave) {
        List<DeliveryWSO> deliveryList = new ArrayList<DeliveryWSO>();
        List<MyTowerErrorMsg> deliveriesErrors = new ArrayList<>();

        for (DeliveryWSO deliveryWSO: deliveryListToSave) {
            try {
                deliveryList.add(this.createDelivery(deliveryWSO));
            } catch (MyTowerException e) {
                deliveriesErrors
                    .add(
                        new MyTowerErrorMsg(
                            "error while creating delivery with Customer order reference :" +
                                deliveryWSO.getCustomerOrderReference()));
                deliveriesErrors.addAll(e.getListErrorMessage());
            }

        }

        if (CollectionUtils.isNotEmpty(deliveriesErrors)) {
            throw new MyTowerException(deliveriesErrors);
        }

        return deliveryList;
    }

    public List<DeliveryWSO> update(List<DeliveryWSO> deliveryListToSave) {
        List<DeliveryWSO> deliveryList = new ArrayList<DeliveryWSO>();
        List<MyTowerErrorMsg> deliveriesErrors = new ArrayList<>();

        for (DeliveryWSO deliveryWSO: deliveryListToSave) {

            try {
                deliveryList.add(this.updateDelivery(deliveryWSO));
            } catch (MyTowerException e) {
                deliveriesErrors
                    .add(
                        new MyTowerErrorMsg(
                            "error while updating delivery with Customer order reference :" +
                                deliveryWSO.getCustomerOrderReference()));
                deliveriesErrors.addAll(e.getListErrorMessage());
            }

        }

        if (CollectionUtils.isNotEmpty(deliveriesErrors)) {
            throw new MyTowerException(deliveriesErrors);
        }

        return deliveryList;
    }

    public DeliveryWSO planifyOrder(EbOrderPlanificationDTO orderPlanification) throws ParseException {
        return deliveryWSOMapper.getWSOFromEntity(orderService.planifyOrder(orderPlanification));
    }
}
