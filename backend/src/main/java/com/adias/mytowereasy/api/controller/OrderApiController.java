package com.adias.mytowereasy.api.controller;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.api.service.OrderApiService;
import com.adias.mytowereasy.api.wso.OrderWSO;
import com.adias.mytowereasy.api.wso.SearchCriteriaOrderWSO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/api/v1/order")
public class OrderApiController extends ApiController {
    @Autowired
    OrderApiService orderApiService;

    @ApiOperation(value = "Search order")
    @PostMapping(value = "/search", consumes = "application/json", produces = "application/hal+json")
    public @ResponseBody Resource<OrderWSO>
        getOrder(@RequestBody SearchCriteriaOrderWSO searchCriteriaOrderWSO, HttpServletResponse response) {
        OrderWSO order = orderApiService.getOrder(searchCriteriaOrderWSO);

        if (order == null) {
            response.setStatus(HttpStatus.NO_CONTENT.value());
            return null;
        }
        Resource<OrderWSO> resourceOrder = new Resource<OrderWSO>(order);
        return resourceOrder;
    }

    @ApiOperation(value = "Create order")
    @PostMapping(consumes = "application/json", produces = "application/hal+json")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Created"),
    })
    public @ResponseBody Resource<OrderWSO> createOrder(@RequestBody OrderWSO orderToSave) {
        OrderWSO order = orderApiService.createOrder(orderToSave);
        Resource<OrderWSO> resourceWrapper = new Resource<>(order);
        return resourceWrapper;
    }

    @ApiOperation(value = "Update order")
    @PutMapping(consumes = "application/json", produces = "application/hal+json")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Updated"),
    })
    public @ResponseBody Resource<OrderWSO> saveOrder(@RequestBody OrderWSO orderToSave) {
        OrderWSO order = orderApiService.saveOrder(orderToSave);
        Resource<OrderWSO> resourceWrapper = new Resource<>(order);
        return resourceWrapper;
    }

    @ApiOperation(value = "Create orders")
    @PostMapping(value = "/createList")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Created"),
    })
    public @ResponseBody Resources<OrderWSO> create(@RequestBody List<OrderWSO> orders) {
        return new Resources<>(orderApiService.create(orders));
    }

    @ApiOperation(value = "Update orders")
    @PutMapping(value = "/updateList")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Updated"),
    })
    public @ResponseBody Resources<OrderWSO> update(@RequestBody List<OrderWSO> orders) {
        return new Resources<>(orderApiService.update(orders));
    }
}
