package com.adias.mytowereasy.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.api.service.ExchangeRateApiService;
import com.adias.mytowereasy.api.wso.ExchangeRateUnitIntegrationResponse;
import com.adias.mytowereasy.api.wso.ExchangeRateWSO;
import com.adias.mytowereasy.api.wso.SearchExchangeRateCriteriaWSO;
import com.adias.mytowereasy.model.EbCompagnieCurrency;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api/v1/currency")
public class CurrencyApiController extends ApiController
{
	@Autowired
	ExchangeRateApiService exchangeRateApiService;

	@ApiOperation(value = "add a list of exchange rate")
	@PostMapping(
		value = "/add-list",
		consumes = "application/json",
		produces = "application/hal+json"
	)
	public List<ExchangeRateUnitIntegrationResponse> addListExchangeRate(
		@ApiParam(
			value = "exchange rate cridentials",
			required = true
		) @RequestBody @Valid List<ExchangeRateWSO> exchangeRateList
	)
	{
		return exchangeRateApiService.addListExchangeRate(exchangeRateList);
	}

	@ApiOperation(value = "Update exchange rate")
	@PostMapping(
		value = "/update-exchange-rate",
		consumes = "application/json",
		produces = "application/hal+json"
	)
	public EbCompagnieCurrency updateExchangeRate(
		@ApiParam(
			value = "exchange rate cridentials",
			required = true
		) @RequestBody @Valid ExchangeRateWSO exchangeRate
	)
	{
		return exchangeRateApiService.update(exchangeRate);
	}

	@ApiOperation(value = "Search exchange rate")
	@PostMapping(
		value = "/search-exchange-rate",
		consumes = "application/json",
		produces = "application/hal+json"
	)
	public List<EbCompagnieCurrency> searchExchangeRate(
		@ApiParam(
			value = "exchange rate criteria cridentials",
			required = true
		) @RequestBody @Valid SearchExchangeRateCriteriaWSO exchangeRateCriteriaWSO
	)
	{
		return exchangeRateApiService.search(exchangeRateCriteriaWSO);
	}

	@ApiOperation(value = "delete exchange rate")
	@DeleteMapping(
		value = "/delete",
		consumes = "application/json",
		produces = "application/hal+json"
	)
	public ResponseEntity<String> deleteExchangeRate()
	{
		return exchangeRateApiService.delete();
	}

}
