package com.adias.mytowereasy.api.wso;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "PSL")
public class PslWSO {
    @ApiModelProperty(notes = "Id (Useless for creation)")
    private Integer itemId;

    private String trackingPointCode;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date date;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getTrackingPointCode() {
        return trackingPointCode;
    }

    public void setTrackingPointCode(String trackingPointCode) {
        this.trackingPointCode = trackingPointCode;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
