package com.adias.mytowereasy.api.controller;

import java.text.ParseException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.api.service.DeliveryAPIService;
import com.adias.mytowereasy.api.wso.DeliveryWSO;
import com.adias.mytowereasy.api.wso.SearchCriteriaDeliveryWSO;
import com.adias.mytowereasy.dto.EbOrderPlanificationDTO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/api/v1/delivery")
public class DeliveryApiController extends ApiController {

    @Autowired
    DeliveryAPIService deliveryApiService;

    @ApiOperation(value = "Search Delivery")
    @PostMapping(value = "/search")
    public @ResponseBody Resource<DeliveryWSO>
        getDelivery(@RequestBody SearchCriteriaDeliveryWSO searchCriteriaDeliveryWSO, HttpServletResponse response) {
        DeliveryWSO delivery = deliveryApiService.getDelivery(searchCriteriaDeliveryWSO);

        if (delivery == null) {
            response.setStatus(HttpStatus.NO_CONTENT.value());
            return null;
        }

        return new Resource<DeliveryWSO>(delivery);
    }

    @ApiOperation(value = "create delivery")
    @PostMapping()
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Created"),
    })
    public @ResponseBody Resource<DeliveryWSO> createDelivery(@RequestBody DeliveryWSO deliveryToCreate) {
        DeliveryWSO delivery = deliveryApiService.createDelivery(deliveryToCreate);
        return new Resource<>(delivery);
    }

    @ApiOperation(value = "Update delivery")
    @PutMapping()
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Updated"),
    })
    public @ResponseBody Resource<DeliveryWSO> updateDelivery(@RequestBody DeliveryWSO deliveryToSave) {
        DeliveryWSO delivery = deliveryApiService.updateDelivery(deliveryToSave);
        return new Resource<>(delivery);
    }

    @ApiOperation(value = "Create deliveries")
    @PostMapping(value = "/createList")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Created"),
    })
    public @ResponseBody Resources<DeliveryWSO>
        create(@RequestBody List<DeliveryWSO> deliveries) {
        return new Resources<DeliveryWSO>(deliveryApiService.create(deliveries));
    }

    @ApiOperation(value = "Update deliveries")
    @PutMapping(value = "/updateList")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Updated"),
    })
    public @ResponseBody Resources<DeliveryWSO>
        update(@RequestBody List<DeliveryWSO> deliveries) {
        return new Resources<DeliveryWSO>(deliveryApiService.update(deliveries));
    }

    @ApiOperation(value = "Add or update delivery from Order Planification")
    @PutMapping(value = "planify-order")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Created")
    })
    public @ResponseBody Resource<DeliveryWSO> planifyOrder(@RequestBody EbOrderPlanificationDTO orderPlanification)
        throws ParseException {
        DeliveryWSO livraison = deliveryApiService.planifyOrder(orderPlanification);
        Resource<DeliveryWSO> resourceWrapper = new Resource<>(livraison);
        return resourceWrapper;
    }



}
