package com.adias.mytowereasy.api.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@ApiResponses(value = {
    @ApiResponse(code = 200, message = "Success"),
    @ApiResponse(
        code = 400,
        message = "Some fields are not valid (not filled or not compliants with specifications) or sent model doesn't match expected"),
    @ApiResponse(code = 403, message = "Forbidden"),
    @ApiResponse(code = 404, message = "Not found"),
    @ApiResponse(code = 406, message = "Known runtime error"),
    @ApiResponse(code = 500, message = "Unmanaged error. If you encounter it, contact MyTower team")
})
public abstract class ApiController {
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ssXXX");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
    }
}
