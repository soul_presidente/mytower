package com.adias.mytowereasy.api.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.api.service.TrackingApiService;
import com.adias.mytowereasy.api.wso.TrackingEventWSO;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.ConnectedUserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/api/v1/tracking")
public class TrackingApiController extends ApiController {
    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    TrackingApiService trackingApiService;

    @ApiOperation(value = "Adding tracking events for an existing transport file")
    @PostMapping(consumes = "application/json", produces = "application/hal+json")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Created"),
    })
    public Resource<TrackingEventWSO> createTrackingEvent(
        @ApiParam(value = "Create an event for a tracking", required = true) @RequestBody @Valid TrackingEventWSO event)
        throws Exception {
        TrackingEventWSO result = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();

        if (event.getCreatorEmail() == null || event.getCreatorEmail().isEmpty()) {
            event.setCreatorEmail(connectedUser.getEmail());
        }
        else {
            event.setCreatorEmail(event.getCreatorEmail().trim());
        }

        result = trackingApiService.createTrackingEvent(event);
        Resource<TrackingEventWSO> resourceWrapper = new Resource<>(result);
        return resourceWrapper;
    }

    @ApiOperation(value = "Create Track & Trace")
    @PostMapping(value = "/create", consumes = "application/json", produces = "application/hal+json")
    public ResponseEntity<String> createTracings(
        @ApiParam(value = "Create a tracking file", required = true) @RequestBody @Valid Integer[] transportRequestIds)
        throws Exception {
        return trackingApiService.createTracings(transportRequestIds);
    }
}
