package com.adias.mytowereasy.api.wso;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "SearchCriteriaPlanTransport")
public class SearchCriteriaPlanTranportWSO {
    @ApiModelProperty(notes = "list party destination")
    private List<Integer> listPartyDestination;

    @ApiModelProperty(notes = "list transport reference")
    private List<String> listTransportRef;
    @ApiModelProperty(notes = "list customer reference")
    private List<String> listCustomerReference;

    @ApiModelProperty(notes = "Number of items in the page", value = "5")
    private Integer size;
    @ApiModelProperty(notes = "Page number", value = "0")
    private Integer page;
    @ApiModelProperty(
        notes = "Request for the list of TR by their nature, nature field is mapped to natureTransport field",
        example = "EDI")
    private String natureTransport;

    @ApiModelProperty(notes = "Request for the list of TR by the transporter email")
    private String transporterEmail;
    @ApiModelProperty(notes = "Request for the list of TR by transporter company code")
    private String transporterCompanyCode;

    @ApiModelProperty(notes = "Request for the TR valorized", value = "false")
    private boolean isValorized;

    @ApiModelProperty(notes = "Request for the TR grouping", value = "false")
    private boolean isGrouping;

    @ApiModelProperty(notes = "List of Incoterm")
    protected List<String> listIncoterm;

    @ApiModelProperty(notes = "List of origin countries")
    protected List<String> listOrigins;

    @ApiModelProperty(notes = "List of origin city")
    private List<String> listOriginCity;

    @ApiModelProperty(notes = "List of destination countries")
    protected List<String> listDestinations;

    @ApiModelProperty(notes = "List of destination city")
    protected List<String> listDestinationCity;

    @ApiModelProperty(notes = "List of mode of transport")
    protected List<String> listModeTransport;

    @ApiModelProperty(notes = "List of dangerous good")
    protected List<Integer> dangerousGood;
    @ApiModelProperty(notes = "Unit number")
    protected String uns;
    @ApiModelProperty(notes = "List of class good")
    protected List<Double> classGoods;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date transportCreationDateFrom;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date transportCreationDateTo;
    @ApiModelProperty(notes = "Information about transport field")
    private String numAwbBol;
    @Valid
    private List<CategoryWSO> categoryList;
    @Valid
    private List<CustomFieldWSO> customFieldList;
    @ApiModelProperty(notes = "Total taxable weight")
    private Double totalTaxableWeight;

    public List<CustomFieldWSO> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<CustomFieldWSO> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public List<CategoryWSO> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<CategoryWSO> categoryList) {
        this.categoryList = categoryList;
    }

    public String getNumAwbBol() {
        return numAwbBol;
    }

    public void setNumAwbBol(String numAwbBol) {
        this.numAwbBol = numAwbBol;
    }

    public Date getTransportCreationDateTo() {
        return transportCreationDateTo;
    }

    public void setTransportCreationDateTo(Date transportCreationDateTo) {
        this.transportCreationDateTo = transportCreationDateTo;
    }

    public boolean isValorized() {
        return isValorized;
    }

    public Date getTransportCreationDateFrom() {
        return transportCreationDateFrom;
    }

    public void setTransportCreationDateFrom(Date transportCreationDateFrom) {
        this.transportCreationDateFrom = transportCreationDateFrom;
    }

    public void setValorized(boolean isValorized) {
        this.isValorized = isValorized;
    }

    public String getTransporterCompanyCode() {
        return transporterCompanyCode;
    }

    public void setTransporterCompanyCode(String transporterCompanyCode) {
        this.transporterCompanyCode = transporterCompanyCode;
    }

    public String getTransporterEmail() {
        return transporterEmail;
    }

    public void setTransporterEmail(String transporterEmail) {
        this.transporterEmail = transporterEmail;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<String> getListTransportRef() {
        return listTransportRef;
    }

    public void setListTransportRef(List<String> listTransportRef) {
        this.listTransportRef = listTransportRef;
    }

    public List<String> getListCustomerReference() {
        return listCustomerReference;
    }

    public void setListCustomerReference(List<String> listCustomerReference) {
        this.listCustomerReference = listCustomerReference;
    }

    public String getNatureTransport() {
        return natureTransport;
    }

    public void setNatureTransport(String natureTransport) {
        this.natureTransport = natureTransport;
    }

    public List<Integer> getListPartyDestination() {
        return listPartyDestination;
    }

    public void setListPartyDestination(List<Integer> listPartyDestination) {
        this.listPartyDestination = listPartyDestination;
    }

    public List<String> getListIncoterm() {
        return listIncoterm;
    }

    public void setListIncoterm(List<String> listIncoterm) {
        this.listIncoterm = listIncoterm;
    }

    public List<String> getListModeTransport() {
        return listModeTransport;
    }

    public void setListModeTransport(List<String> listModeTransport) {
        this.listModeTransport = listModeTransport;
    }

    public List<String> getListOrigins() {
        return listOrigins;
    }

    public void setListOrigins(List<String> listOrigins) {
        this.listOrigins = listOrigins;
    }

    public List<String> getListDestinations() {
        return listDestinations;
    }

    public void setListDestinations(List<String> listDestinations) {
        this.listDestinations = listDestinations;
    }

    public List<String> getListOriginCity() {
        return listOriginCity;
    }

    public void setListOriginCity(List<String> listOriginCity) {
        this.listOriginCity = listOriginCity;
    }

    public List<String> getListDestinationCity() {
        return listDestinationCity;
    }

    public void setListDestinationCity(List<String> listDestinationCity) {
        this.listDestinationCity = listDestinationCity;
    }

    public List<Integer> getDangerousGood() {
        return dangerousGood;
    }

    public void setDangerousGood(List<Integer> dangerousGood) {
        this.dangerousGood = dangerousGood;
    }

    public String getUns() {
        return uns;
    }

    public void setUns(String uns) {
        this.uns = uns;
    }

    public List<Double> getClassGoods() {
        return classGoods;
    }

    public void setClassGoods(List<Double> classGoods) {
        this.classGoods = classGoods;
    }

    public boolean isGrouping() {
        return isGrouping;
    }

    public void setGrouping(boolean grouping) {
        isGrouping = grouping;
    }

    public Double getTotalTaxableWeight() {
        return totalTaxableWeight;
    }

    public void setTotalTaxableWeight(Double totalTaxableWeight) {
        this.totalTaxableWeight = totalTaxableWeight;
    }
}
