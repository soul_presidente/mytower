package com.adias.mytowereasy.api.wso;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "Order")
public class OrderWSO {
    @ApiModelProperty(notes = "Id (Useless for creation)")
    private Long itemId;

    @ApiModelProperty(notes = "EDI Number")
    private String numEdi;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @ApiModelProperty(
        notes = "Date the order was created on (not creation date in MyTower)",
        example = "2019-12-05 09:18:12-05:00")
    private Date customerCreationDate;

    @Valid
    @ApiModelProperty(notes = "Shipper address(the address of the shipper)", required = true)
    private AddressWSO from;

    @Valid
    @ApiModelProperty(notes = "Ship to address", required = true)
    private AddressWSO to;

    @Valid
    @ApiModelProperty(notes = "Sold to address (the physical address where customer shipments are sent)")
    private AddressWSO soldTo;

    @Valid
    @NotNull
    @ApiModelProperty(notes = "Order lines", required = true)
    private List<OrderLineWSO> lines;

    @Valid
    @ApiModelProperty(notes = "Vendor address")
    private AddressWSO vendor;

    @Valid
    @ApiModelProperty(notes = "Issuer address")
    private AddressWSO issuer;

    @Valid
    private List<CustomFieldWSO> customFieldList;

    @Valid
    private List<CategoryWSO> categoryList;

    @ApiModelProperty(notes = "ID of company who own the order (default company of connected user)")
    private Integer companyId;

    @ApiModelProperty(notes = "ID of etablissement who own the order (default etablissement of connected user)")
    private Integer etablissementId;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @ApiModelProperty(notes = "target date", example = "2019-12-05 09:18:12-05:00")
    private Date targetDate;

    @ApiModelProperty(notes = "Priority")
    private Integer priority;

    @ApiModelProperty(notes = "Customer Order Reference", required = true)
    private String customerOrderReference;

    @ApiModelProperty(notes = "Incoterm")
    private String incoterm;
    @ApiModelProperty(notes = "Complementary Information")
    private String complementaryInformations;

    @NotEmpty
    @Email
    @ApiModelProperty(notes = "The request owner's email", required = true, example = "example@mytower.com")
    private String requestOwnerEmail;

    @ApiModelProperty(notes = "Order Status")
    private String orderStatus;

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getNumEdi() {
        return numEdi;
    }

    public void setNumEdi(String numEdi) {
        this.numEdi = numEdi;
    }

    public Date getCustomerCreationDate() {
        return customerCreationDate;
    }

    public void setCustomerCreationDate(Date customerCreationDate) {
        this.customerCreationDate = customerCreationDate;
    }

    public AddressWSO getFrom() {
        return from;
    }

    public void setFrom(AddressWSO from) {
        this.from = from;
    }

    public AddressWSO getTo() {
        return to;
    }

    public void setTo(AddressWSO to) {
        this.to = to;
    }

    public AddressWSO getSoldTo() {
        return soldTo;
    }

    public void setSoldTo(AddressWSO soldTo) {
        this.soldTo = soldTo;
    }

    public List<OrderLineWSO> getLines() {
        return lines;
    }

    public void setLines(List<OrderLineWSO> lines) {
        this.lines = lines;
    }

    public List<CustomFieldWSO> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<CustomFieldWSO> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Integer getEtablissementId() {
        return etablissementId;
    }

    public void setEtablissementId(Integer etablissementId) {
        this.etablissementId = etablissementId;
    }

    public AddressWSO getVendor() {
        return vendor;
    }

    public void setVendor(AddressWSO vendor) {
        this.vendor = vendor;
    }

    public AddressWSO getIssuer() {
        return issuer;
    }

    public void setIssuer(AddressWSO issuer) {
        this.issuer = issuer;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getCustomerOrderReference() {
        return customerOrderReference;
    }

    public void setCustomerOrderReference(String customerOrderReference) {
        this.customerOrderReference = customerOrderReference;
    }

    public List<CategoryWSO> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<CategoryWSO> categoryList) {
        this.categoryList = categoryList;
    }

    public String getIncoterm() {
        return incoterm;
    }

    public void setIncoterm(String incoterm) {
        this.incoterm = incoterm;
    }

    public String getComplementaryInformations() {
        return complementaryInformations;
    }

    public void setComplementaryInformations(String complementaryInformations) {
        this.complementaryInformations = complementaryInformations;
    }

    public String getRequestOwnerEmail() {
        return requestOwnerEmail;
    }

    public void setRequestOwnerEmail(String requestOwnerEmail) {
        this.requestOwnerEmail = requestOwnerEmail;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }
}
