package com.adias.mytowereasy.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.api.service.PlanTransportApiService;
import com.adias.mytowereasy.api.wso.PlanTransportWSO;
import com.adias.mytowereasy.api.wso.SearchCriteriaPlanTranportWSO;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@RestController
@RequestMapping("/api/v1/plan-transport")
public class PlanTransportApiController extends ApiController {
    @Autowired
    PlanTransportApiService planTransportApiService;

    @ApiOperation("Search plan transport")
    @PostMapping(value = "/search-planTransport", consumes = "application/json", produces = "application/hal+json")
    public PagedResources<PlanTransportWSO> getPlanTransport(
        @ApiParam(
            value = "Criteria to search a Plan Transport request",
            required = true) @RequestBody @Valid SearchCriteriaPlanTranportWSO criteria,
        PagedResourcesAssembler assembler) {
        PagedResources<PlanTransportWSO> transportResources = assembler
            .toResource(
                planTransportApiService.getPlanTransportRequest(criteria),
                ControllerLinkBuilder.linkTo(TransportApiController.class).withSelfRel());

        return transportResources;
    }

    @ApiOperation("Find the next date that meets the criteria")
    @PostMapping(
        value = "/nextPickUpDate-planTransport",
        consumes = "application/json",
        produces = "application/hal+json")
    public List<Resource<PlanTransportWSO>> getNextPickUpDate(
        @ApiParam(
            value = "Criteria to search the next pick up date",
            required = true) @RequestBody @Valid SearchCriteriaPlanTranportWSO criteria)
        throws Exception {
        return planTransportApiService.getNextPickUpDatePlanTransportRequest(criteria);
    }
}
