package com.adias.mytowereasy.api.wso;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Exchange rate")
public class ExchangeRateWSO
{
	@ApiModelProperty(notes = "Currency origin", required = true)
	private String	originCurrencyCode;

	@ApiModelProperty(notes = "Currency destination", required = true)
	private String	targetCurrencyCode;

	@ApiModelProperty(notes = "Currency start validity date", example = "2019-12-05")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date		startDate;

	@ApiModelProperty(notes = "Currency end validity date", example = "2019-12-05")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date		endDate;

	@ApiModelProperty(notes = "exchange rate from currency origin to currency dest", required = true)
	private Double	rate;

	@ApiModelProperty(notes = "Carrier company siren", required = true)
	private String	carrierCompanySiren;

	@ApiModelProperty(notes = "exchange rate Id", required = true)
	private Integer	exchangeRateId;

	public String getOriginCurrencyCode()
	{
		return originCurrencyCode;
	}

	public void setOriginCurrencyCode(String originCurrencyCode)
	{
		this.originCurrencyCode = originCurrencyCode;
	}

	public String getTargetCurrencyCode()
	{
		return targetCurrencyCode;
	}

	public void setTargetCurrencyCode(String targetCurrencyCode)
	{
		this.targetCurrencyCode = targetCurrencyCode;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public Double getRate()
	{
		return rate;
	}

	public void setRate(Double rate)
	{
		this.rate = rate;
	}

	public String getCarrierCompanySiren()
	{
		return carrierCompanySiren;
	}

	public void setCarrierCompanySiren(String compaganyGuestCode)
	{
		this.carrierCompanySiren = compaganyGuestCode;
	}

	public Integer getExchangeRateId()
	{
		return exchangeRateId;
	}

	public void setExchangeRateId(Integer exchangeRateId)
	{
		this.exchangeRateId = exchangeRateId;
	}
}
