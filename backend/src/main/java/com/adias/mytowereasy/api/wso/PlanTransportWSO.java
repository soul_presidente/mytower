package com.adias.mytowereasy.api.wso;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "PlanTransport")
public class PlanTransportWSO {
    @ApiModelProperty(notes = "TR nature")
    private Integer nature;
    @ApiModelProperty(notes = "Dangerous Goods")
    private boolean dangerousGoods;
    @ApiModelProperty(notes = "UN")
    private String uns;

    @ApiModelProperty(notes = "Class Good")
    private List<Double> classGood;
    @ApiModelProperty(notes = "Dangerous Good")
    protected List<Integer> dangerousGood;
    @ApiModelProperty(notes = "For the TR that is not grouped")
    private boolean isGrouping;

    @ApiModelProperty(notes = "Id (Useless for creation)")
    private Integer itemId;

    @ApiModelProperty(notes = "Poids Taxable")
    private Double taxableWeight;

    @ApiModelProperty(notes = "Total Volume")
    private Double totalVolume;
    @ApiModelProperty(notes = "Total Weight")
    private Double totalWeight;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @ApiModelProperty(notes = "The next pick up date", example = "Sun. 12-04-2020 12:34")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date nextPickUpDate;

    @NotEmpty
    @Email
    @ApiModelProperty(notes = "The request owner's email", required = true, example = "example@mytower.com")
    private String shipperUserEmail;

    @ApiModelProperty(notes = "Reference of pricing request, generated automatically if not provided")
    private String transportReference;

    @ApiModelProperty(notes = "The request owner's first and last name")
    private String shipperUserName;

    @ApiModelProperty(notes = "The request owner's Establishment ")
    private String shipperEtablishment;

    @NotEmpty
    @ApiModelProperty(notes = "The request owner's email", required = true, example = "example@mytower.com")
    private String transportMode;

    @NotEmpty
    @Size(min = 3, max = 3)
    @ApiModelProperty(notes = "ISO Currency code", required = true, example = "EUR")
    private String invoicingCurrencyCode;

    @ApiModelProperty(notes = "Transport status, specified automatically if not provided", example = "INP")
    private String transportStatus;

    @NotEmpty
    @ApiModelProperty(notes = "Service level / type of request", required = true, example = "STD")
    private String serviceLevel;

    @NotEmpty
    @ApiModelProperty(notes = "Flow type", required = true, example = "F1")
    private String flowTypeCode;

    @NotEmpty
    @ApiModelProperty(notes = "Should be one of flow type's incoterms", required = true, example = "INC1")
    private String incoterm;

    private String transportType;

    private String customerReference;

    @NotEmpty
    @ApiModelProperty(notes = "Should be one of flow type's tracking point schema", required = true, example = "CIF")
    private String trackingPointSchema;

    @ApiModelProperty(
        notes = "Specify which tracking points should be actives, should have a list of schemas's tracking point")
    private List<String> trackingPointCodeList;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @ApiModelProperty(notes = "Arrival date", example = "2019-12-05 09:18:12-05:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date arrivalDate;

    @ApiModelProperty(notes = "Submit date", example = "2019-12-05 09:18:12-05:00")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date customerCreationDate;

    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @ApiModelProperty(notes = "Goods avalability date", required = true, example = "2019-12-05 09:18:12-05:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date goodsAvailabilityDate;

    private Boolean insurance;

    @ApiModelProperty(notes = "Should be specified if insurance is true")
    private Double insuranceValue;

    @ApiModelProperty(notes = "Should be specified if insurance value is provided")
    private String insuranceCurrencyCode;

    private String costCenterCode;

    @ApiModelProperty(notes = "Information about transport field")
    private String numAwbBol;

    @ApiModelProperty(notes = "Information about transport field")
    private String flightVessel;

    @ApiModelProperty(notes = "Information about transport field")
    private String customsOffice;

    @ApiModelProperty(notes = "Information about transport field")
    private String mawb;

    private String originUserEmail;

    private String originCustomUserEmail;

    private String destUserEmail;

    private String destCustomUserEmail;

    private List<String> observerUserEmailList;

    @Valid
    @NotNull
    @ApiModelProperty(notes = "Origin informations", required = true)
    private AddressWSO originInformation;

    @Valid
    @NotNull
    @ApiModelProperty(notes = "Destination informations", required = true)
    private AddressWSO destinationInformation;

    @Valid
    private AddressWSO notifyInformation;

    @Valid
    private AddressWSO intermediaryPoint;

    @Valid
    @ApiModelProperty(notes = "Quotation list / transport plans, should be specified if transport status is FIN")
    private List<QuotationWSO> quotationList;

    @Valid
    @ApiModelProperty(notes = "Information about transport estimated dates fields")
    private List<PslWSO> estimatedDateList;

    @Valid
    @NotEmpty
    @ApiModelProperty(notes = "Goods informations", required = true)
    private List<UnitWSO> unitList;

    @Valid
    private List<CategoryWSO> categoryList;

    @Valid
    private List<CustomFieldWSO> customFieldList;

    @NotEmpty
    @NotNull
    @ApiModelProperty(notes = "Ville", required = true)
    private String city;

    @ApiModelProperty(notes = "Eligibility for consolidation")
    private Boolean eligibleForConsolidation;

    @ApiModelProperty(
        notes = "Specify wether a Request with Status Confirmed (random price is set) should be valorized via MTC or not")
    private Boolean finalChoiceToBeCalculated;

    @ApiModelProperty(notes = "Information about grouped transport status ")
    private String consolidationStatus;

    /**
     * additionalCostList
     * it represents the additional cost added in the context of essilor.
     * A la différence des cout de transport, ces couts ne sont pas prise en
     * compte lors de la
     * valorisation, ni pour le controle de facturation.
     * Pour Essilor il s'agit des cout que Essilor refacture à ses clients. le
     * transporteur n'as donc
     * pas de visibilité sur ces coups.
     */
    @Valid
    @ApiModelProperty(notes = "Informations about the additional cost")
    private List<CostWSO> additionalCostList;

    @ApiModelProperty(notes = "Submit date", example = "2019-12-05 09:18:12-05:00")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date transportUpdateDate;
    @ApiModelProperty(notes = "Total taxable weight")
    private Double totalTaxableWeight;

    @Valid
    @ApiModelProperty(notes = "To expose the list of inital TR ")
    private List<TransportLightWSO> initialTransportList;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Date getNextPickUpDate() {
        return nextPickUpDate;
    }

    public PlanTransportWSO() {
        super();
    }

    public void setNextPickUpDate(Date nextPickUpDate) {
        this.nextPickUpDate = nextPickUpDate;
    }

    public String getShipperUserEmail() {
        return shipperUserEmail;
    }

    public void setShipperUserEmail(String shipperUserEmail) {
        this.shipperUserEmail = shipperUserEmail;
    }

    public String getTransportReference() {
        return transportReference;
    }

    public void setTransportReference(String transportReference) {
        this.transportReference = transportReference;
    }

    public String getTransportMode() {
        return transportMode;
    }

    public void setTransportMode(String transportMode) {
        this.transportMode = transportMode;
    }

    public String getInvoicingCurrencyCode() {
        return invoicingCurrencyCode;
    }

    public void setInvoicingCurrencyCode(String invoicingCurrencyCode) {
        this.invoicingCurrencyCode = invoicingCurrencyCode;
    }

    public String getTransportStatus() {
        return transportStatus;
    }

    public void setTransportStatus(String transportStatus) {
        this.transportStatus = transportStatus;
    }

    public String getServiceLevel() {
        return serviceLevel;
    }

    public void setServiceLevel(String serviceLevel) {
        this.serviceLevel = serviceLevel;
    }

    public String getFlowTypeCode() {
        return flowTypeCode;
    }

    public void setFlowTypeCode(String flowTypeCode) {
        this.flowTypeCode = flowTypeCode;
    }

    public String getIncoterm() {
        return incoterm;
    }

    public void setIncoterm(String incoterm) {
        this.incoterm = incoterm;
    }

    public String getTransportType() {
        return transportType;
    }

    public void setTransportType(String transportType) {
        this.transportType = transportType;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getTrackingPointSchema() {
        return trackingPointSchema;
    }

    public void setTrackingPointSchema(String trackingPointSchema) {
        this.trackingPointSchema = trackingPointSchema;
    }

    public List<String> getTrackingPointCodeList() {
        return trackingPointCodeList;
    }

    public void setTrackingPointCodeList(List<String> trackingPointCodeList) {
        this.trackingPointCodeList = trackingPointCodeList;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getGoodsAvailabilityDate() {
        return goodsAvailabilityDate;
    }

    public void setGoodsAvailabilityDate(Date goodsAvailabilityDate) {
        this.goodsAvailabilityDate = goodsAvailabilityDate;
    }

    public Boolean getInsurance() {
        return insurance;
    }

    public void setInsurance(Boolean insurance) {
        this.insurance = insurance;
    }

    public Double getInsuranceValue() {
        return insuranceValue;
    }

    public void setInsuranceValue(Double insuranceValue) {
        this.insuranceValue = insuranceValue;
    }

    public String getInsuranceCurrencyCode() {
        return insuranceCurrencyCode;
    }

    public void setInsuranceCurrencyCode(String insuranceCurrencyCode) {
        this.insuranceCurrencyCode = insuranceCurrencyCode;
    }

    public String getCostCenterCode() {
        return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
        this.costCenterCode = costCenterCode;
    }

    public String getNumAwbBol() {
        return numAwbBol;
    }

    public void setNumAwbBol(String numAwbBol) {
        this.numAwbBol = numAwbBol;
    }

    public String getFlightVessel() {
        return flightVessel;
    }

    public void setFlightVessel(String flightVessel) {
        this.flightVessel = flightVessel;
    }

    public String getCustomsOffice() {
        return customsOffice;
    }

    public void setCustomsOffice(String customsOffice) {
        this.customsOffice = customsOffice;
    }

    public String getMawb() {
        return mawb;
    }

    public void setMawb(String mawb) {
        this.mawb = mawb;
    }

    public String getOriginUserEmail() {
        return originUserEmail;
    }

    public void setOriginUserEmail(String originUserEmail) {
        this.originUserEmail = originUserEmail;
    }

    public String getOriginCustomUserEmail() {
        return originCustomUserEmail;
    }

    public void setOriginCustomUserEmail(String originCustomUserEmail) {
        this.originCustomUserEmail = originCustomUserEmail;
    }

    public String getDestUserEmail() {
        return destUserEmail;
    }

    public void setDestUserEmail(String destUserEmail) {
        this.destUserEmail = destUserEmail;
    }

    public String getDestCustomUserEmail() {
        return destCustomUserEmail;
    }

    public void setDestCustomUserEmail(String destCustomUserEmail) {
        this.destCustomUserEmail = destCustomUserEmail;
    }

    public AddressWSO getOriginInformation() {
        return originInformation;
    }

    public void setOriginInformation(AddressWSO originInformation) {
        this.originInformation = originInformation;
    }

    public AddressWSO getDestinationInformation() {
        return destinationInformation;
    }

    public void setDestinationInformation(AddressWSO destinationInformation) {
        this.destinationInformation = destinationInformation;
    }

    public AddressWSO getNotifyInformation() {
        return notifyInformation;
    }

    public void setNotifyInformation(AddressWSO notifyInformation) {
        this.notifyInformation = notifyInformation;
    }

    public AddressWSO getIntermediaryPoint() {
        return intermediaryPoint;
    }

    public void setIntermediaryPoint(AddressWSO intermediaryPoint) {
        this.intermediaryPoint = intermediaryPoint;
    }

    public List<QuotationWSO> getQuotationList() {
        return quotationList;
    }

    public void setQuotationList(List<QuotationWSO> quotationList) {
        this.quotationList = quotationList;
    }

    public List<PslWSO> getEstimatedDateList() {
        return estimatedDateList;
    }

    public void setEstimatedDateList(List<PslWSO> estimatedDateList) {
        this.estimatedDateList = estimatedDateList;
    }

    public List<UnitWSO> getUnitList() {
        return unitList;
    }

    public void setUnitList(List<UnitWSO> unitList) {
        this.unitList = unitList;
    }

    public List<CategoryWSO> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<CategoryWSO> categoryList) {
        this.categoryList = categoryList;
    }

    public List<CustomFieldWSO> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<CustomFieldWSO> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public List<String> getObserverUserEmailList() {
        return observerUserEmailList;
    }

    public void setObserverUserEmailList(List<String> observerUserEmailList) {
        this.observerUserEmailList = observerUserEmailList;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Boolean getEligibleForConsolidation() {
        return eligibleForConsolidation;
    }

    public void setEligibleForConsolidation(Boolean eligibleForConsolidation) {
        this.eligibleForConsolidation = eligibleForConsolidation;
    }

    public Date getCustomerCreationDate() {
        return customerCreationDate;
    }

    public void setCustomerCreationDate(Date customerCreationDate) {
        this.customerCreationDate = customerCreationDate;
    }

    public String getConsolidationStatus() {
        return consolidationStatus;
    }

    public void setConsolidationStatus(String consolidationStatus) {
        this.consolidationStatus = consolidationStatus;
    }

    public List<CostWSO> getAdditionalCostList() {
        return additionalCostList;
    }

    public void setAdditionalCostList(List<CostWSO> additionalCostList) {
        this.additionalCostList = additionalCostList;
    }

    public String getShipperUserName() {
        return shipperUserName;
    }

    public void setShipperUserName(String shipperUserName) {
        this.shipperUserName = shipperUserName;
    }

    public String getShipperEtablishment() {
        return shipperEtablishment;
    }

    public void setShipperEtablishment(String shipperEtablishment) {
        this.shipperEtablishment = shipperEtablishment;
    }

    public Date getTransportUpdateDate() {
        return transportUpdateDate;
    }

    public void setTransportUpdateDate(Date transportUpdateDate) {
        this.transportUpdateDate = transportUpdateDate;
    }

    public List<TransportLightWSO> getInitialTransportList() {
        return initialTransportList;
    }

    public void setInitialTransportList(List<TransportLightWSO> initialTransportList) {
        this.initialTransportList = initialTransportList;
    }

    public Boolean getFinalChoiceToBeCalculated() {
        return finalChoiceToBeCalculated;
    }

    public void setFinalChoiceToBeCalculated(Boolean finalChoiceToBeCalculated) {
        this.finalChoiceToBeCalculated = finalChoiceToBeCalculated;
    }

    public Double getTaxableWeight() {
        return taxableWeight;
    }

    public void setTaxableWeight(Double taxableWeight) {
        this.taxableWeight = taxableWeight;
    }

    public Double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(Double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Integer getNature() {
        return nature;
    }

    public void setNature(Integer nature) {
        this.nature = nature;
    }

    public boolean isGrouping() {
        return isGrouping;
    }

    public void setGrouping(boolean isGrouping) {
        this.isGrouping = isGrouping;
    }

    public boolean isDangerousGoods() {
        return dangerousGoods;
    }

    public void setDangerousGoods(boolean dangerousGoods) {
        this.dangerousGoods = dangerousGoods;
    }

    public List<Double> getClassGood() {
        return classGood;
    }

    public void setClassGood(List<Double> classGood) {
        this.classGood = classGood;
    }

    public String getUns() {
        return uns;
    }

    public void setUns(String uns) {
        this.uns = uns;
    }

    public List<Integer> getDangerousGood() {
        return dangerousGood;
    }

    public void setDangerousGood(List<Integer> dangerousGood) {
        this.dangerousGood = dangerousGood;
    }

    public Double getTotalTaxableWeight() {
        return totalTaxableWeight;
    }

    public void setTotalTaxableWeight(Double totalTaxableWeight) {
        this.totalTaxableWeight = totalTaxableWeight;
    }
}
