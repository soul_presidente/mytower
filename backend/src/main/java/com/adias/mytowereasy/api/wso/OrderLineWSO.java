package com.adias.mytowereasy.api.wso;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.adias.mytowereasy.api.util.WSConstants;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "OrderLine")
public class OrderLineWSO {
    @ApiModelProperty(notes = "Order line num (useless for creation)")
    private Long itemId;

    @Size(min = 2, max = 2)
    @ApiModelProperty(notes = "ISO 3166-2 Code of origin country", example = "FR")
    private String originCountryCode;

    @ApiModelProperty(notes = "Quantity booked")
    private Long quantityOrdered;

    /*
     * Ce champ peut être réactivé à l'avenir
     * @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
     * @JsonFormat(shape = JsonFormat.Shape.STRING, pattern =
     * "yyyy-MM-dd HH:mm:ssXXX")
     * @ApiModelProperty(notes = "Date of availability", example =
     * "2019-12-05 09:18:12-05:00")
     * private Date dateOfAvailability;
     */

    @ApiModelProperty(notes = "height")
    private Integer height;

    @ApiModelProperty(notes = "Part number")
    private String partNumber;

    @ApiModelProperty(notes = "Serial number")
    private String serialNumber;

    @ApiModelProperty(notes = "Priority")
    private Integer priority;

    @ApiModelProperty(notes = "Comment")
    private String comment;

    @ApiModelProperty(notes = "Specification")
    private String specification;

    @Valid
    private List<CustomFieldWSO> customFieldList;

    @Valid
    private List<CategoryWSO> categoryList;

    @DateTimeFormat(pattern = WSConstants.DATE_FORMAT_HEURE_EXEMPLE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = WSConstants.DATE_FORMAT_HEURE_EXEMPLE)
    @ApiModelProperty(notes = "target date", example = WSConstants.DATE_FORMAT_HEURE_EXEMPLE_VALUE)
    private Date targetDate;

    @ApiModelProperty(notes = "Length")
    private Double length;

    @ApiModelProperty(notes = "Weight")
    private Double weight;

    @ApiModelProperty(notes = "Width")
    private Double width;

    @ApiModelProperty(notes = "HS Code")
    private String hsCode;

    @ApiModelProperty(notes = "Item name")
    private String itemName;

    @ApiModelProperty(notes = "Item number")
    private String itemNumber;

    @NotEmpty
    @NotNull
    @ApiModelProperty(notes = "Unit type")
    private String unitType;

    @ApiModelProperty(notes = "Price")
    private Double price;

    @NotEmpty
    @Size(min = 3, max = 3)
    @ApiModelProperty(notes = "ISO Currency code", required = true, example = "EUR")
    private String CurrencyCode;

    @ApiModelProperty(notes = "Export Control Classification Number (ECCN)")
    private String eccn;

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getOriginCountryCode() {
        return originCountryCode;
    }

    public void setOriginCountryCode(String originCountryCode) {
        this.originCountryCode = originCountryCode;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public List<CustomFieldWSO> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<CustomFieldWSO> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public Long getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(Long quantityOrdered) {
        this.quantityOrdered = quantityOrdered;
    }

    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public List<CategoryWSO> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<CategoryWSO> categoryList) {
        this.categoryList = categoryList;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getCurrencyCode() {
        return CurrencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        CurrencyCode = currencyCode;
    }

    public String getEccn() {
        return eccn;
    }

    public void setEccn(String eccn) {
        this.eccn = eccn;
    }
}
