package com.adias.mytowereasy.api.wso;

import io.swagger.annotations.ApiModelProperty;

public class SearchExchangeRateCriteriaWSO
{
	@ApiModelProperty(notes = "Currency origin", required = true)
	private String	originCurrencyCode;

	@ApiModelProperty(notes = "Currency destination", required = true)
	private String	targetCurrencyCode;

	@ApiModelProperty(notes = "Carrier company siren", required = true)
	private String	carrierCompanySiren;

	public String getOriginCurrencyCode()
	{
		return originCurrencyCode;
	}

	public void setOriginCurrencyCode(String originCurrencyCode)
	{
		this.originCurrencyCode = originCurrencyCode;
	}

	public String getTargetCurrencyCode()
	{
		return targetCurrencyCode;
	}

	public void setTargetCurrencyCode(String targetCurrencyCode)
	{
		this.targetCurrencyCode = targetCurrencyCode;
	}

	public String getCarrierCompanySiren()
	{
		return carrierCompanySiren;
	}

	public void setCarrierCompanySiren(String carrierCompanySiren)
	{
		this.carrierCompanySiren = carrierCompanySiren;
	}
}
