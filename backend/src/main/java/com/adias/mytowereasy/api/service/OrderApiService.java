package com.adias.mytowereasy.api.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.api.util.MyTowerErrorMsg;
import com.adias.mytowereasy.api.wso.OrderWSO;
import com.adias.mytowereasy.api.wso.SearchCriteriaOrderWSO;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.mapper.OrderWSOMapper;
import com.adias.mytowereasy.delivery.model.mapper.SearchCriteriaMapper;
import com.adias.mytowereasy.delivery.service.OrderService;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.repository.EbCategorieRepository;
import com.adias.mytowereasy.repository.EbLabelRepository;
import com.adias.mytowereasy.service.ConnectedUserService;


@Service
public class OrderApiService {

    @Autowired
    OrderService orderService;

    @Autowired
    SearchCriteriaMapper searchCriteriaMapper;

    @Autowired
    OrderWSOMapper orderWSOMapper;

    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    EbCategorieRepository ebCategorieRepository;

    @Autowired
    EbLabelRepository ebLabelRepository;

    @Value("${spring.datasource.username}")
    String dbUserName;

    @Value("${spring.datasource.password}")
    String dbPassWord;

    @Value("${spring.datasource.url}")
    String dburl;

    @Value("${spring.datasource.driver-class-name}")
    String dbDriverName;

    public OrderWSO getOrder(SearchCriteriaOrderWSO searchCriteriaOrderWSO) {
        SearchCriteriaOrder searchCriteriaOrder = searchCriteriaMapper
            .orderSearchCriteriaDtoToModel(searchCriteriaOrderWSO);
        EbOrder order = orderService.getOrder(searchCriteriaOrder);

        if (order == null) {
            return null;
        }

        return orderWSOMapper.getWSOFromEntity(order);
    }

    public OrderWSO createOrder(OrderWSO orderToSave) {
        EbOrder order = new EbOrder();
        order = orderWSOMapper.fillEntityFromWSO(order, orderToSave);
        order = orderService.createOrder(order);
        return orderWSOMapper.getWSOFromEntity(order);
    }

    public OrderWSO saveOrder(OrderWSO orderToSave) {
        EbOrder orderEntity = new EbOrder();
        orderEntity = orderWSOMapper.fillEntityFromWSO(orderEntity, orderToSave);
        if (StringUtils.isBlank(orderEntity.getReference())) throw new MyTowerException("Reference must not be null");
        orderEntity = orderService.updateOrder(orderEntity);
        return orderWSOMapper.getWSOFromEntity(orderEntity);
    }

    public List<OrderWSO> create(List<OrderWSO> orderListToSave) {
        List<OrderWSO> orderList = new ArrayList<OrderWSO>();
        List<MyTowerErrorMsg> ordersErrors = new ArrayList<>();

        for (OrderWSO orderWSO: orderListToSave) {
            try {
                orderList.add(this.createOrder(orderWSO));
            } catch (MyTowerException e) {
                ordersErrors
                    .add(
                        new MyTowerErrorMsg(
                            "error while creating order with Customer order reference :" +
                                orderWSO.getCustomerOrderReference()));
                ordersErrors.addAll(e.getListErrorMessage());
            }
        }

        if (CollectionUtils.isNotEmpty(ordersErrors)) {
            throw new MyTowerException(ordersErrors);
        }

        return orderList;
    }

    public List<OrderWSO> update(List<OrderWSO> orderListToSave) {
        List<OrderWSO> orderList = new ArrayList<OrderWSO>();
        List<MyTowerErrorMsg> ordersErrors = new ArrayList<>();

        for (OrderWSO orderWSO: orderListToSave) {
            try {
                orderList.add(this.saveOrder(orderWSO));
            } catch (MyTowerException e) {
                ordersErrors
                    .add(
                        new MyTowerErrorMsg(
                            "error while updating order with Customer order reference :" +
                                orderWSO.getCustomerOrderReference()));

                ordersErrors.addAll(e.getListErrorMessage());
            }
        }

        if (CollectionUtils.isNotEmpty(ordersErrors)) {
            throw new MyTowerException(ordersErrors);
        }

        return orderList;
    }
}
