package com.adias.mytowereasy.api.wso;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "Unit")
public class UnitWSO {
    @ApiModelProperty(notes = "Id (Useless for creation)")
    private Integer itemId;

    @NotNull
    @ApiModelProperty(notes = "Weight", required = true)
    private Double weight;

    @NotNull
    @ApiModelProperty(notes = "Length", required = true)
    private Double length;

    @NotNull
    @ApiModelProperty(notes = "Width", required = true)
    private Double width;

    @NotNull
    @ApiModelProperty(notes = "Heigth", required = true)
    private Double heigth;

    @ApiModelProperty(notes = "Volume")
    private Double volume;

    @NotNull
    @ApiModelProperty(notes = "Number of units", required = true)
    private Integer numberOfUnits;

    @NotEmpty
    @NotNull
    @ApiModelProperty(notes = "Dangerous good", required = true)
    private String dangerousGood;

    @NotEmpty
    @NotNull
    @ApiModelProperty(notes = "Unit type", required = true)
    private String unitType;

    @ApiModelProperty(notes = "Bin Location ")
    private String binLocation;

    @ApiModelProperty(notes = "Export control ")
    private boolean exportControl;

    @ApiModelProperty(notes = "Manifactoring country code ")
    private String manifactoringCountryCode;

    private Boolean stackable;

    private String unitReference;

    private String sscc;

    private String trackingNumber;

    private String itemName;

    private String containerType;

    private String un;

    private String classGood;

    private String packaging;

    private String comment;

    @ApiModelProperty(notes = "Origin country name", example = "France")
    private String originCountry;
    private Double price;

    @ApiModelProperty(example = "BFP")
    private String typeOfGoodCode;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeigth() {
        return heigth;
    }

    public void setHeigth(Double heigth) {
        this.heigth = heigth;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Integer getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(Integer numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public String getDangerousGood() {
        return dangerousGood;
    }

    public void setDangerousGood(String dangerousGood) {
        this.dangerousGood = dangerousGood;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public Boolean getStackable() {
        return stackable;
    }

    public void setStackable(Boolean stackable) {
        this.stackable = stackable;
    }

    public String getUnitReference() {
        return unitReference;
    }

    public void setUnitReference(String unitReference) {
        this.unitReference = unitReference;
    }

    public String getSscc() {
        return sscc;
    }

    public void setSscc(String sscc) {
        this.sscc = sscc;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getContainerType() {
        return containerType;
    }

    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }

    public String getUn() {
        return un;
    }

    public void setUn(String un) {
        this.un = un;
    }

    public String getClassGood() {
        return classGood;
    }

    public void setClassGood(String classGood) {
        this.classGood = classGood;
    }

    public String getPackaging() {
        return packaging;
    }

    public void setPackaging(String packaging) {
        this.packaging = packaging;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getBinLocation() {
        return binLocation;
    }

    public void setBinLocation(String binLocation) {
        this.binLocation = binLocation;
    }

    public boolean isExportControl() {
        return exportControl;
    }

    public void setExportControl(boolean exportControl) {
        this.exportControl = exportControl;
    }

    public String getManifactoringCountryCode() {
        return manifactoringCountryCode;
    }

    public void setManifactoringCountryCode(String manifactoringCountryCode) {
        this.manifactoringCountryCode = manifactoringCountryCode;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTypeOfGoodCode() {
        return typeOfGoodCode;
    }

    public void setTypeOfGoodCode(String typeOfGoodCode) {
        this.typeOfGoodCode = typeOfGoodCode;
    }

}
