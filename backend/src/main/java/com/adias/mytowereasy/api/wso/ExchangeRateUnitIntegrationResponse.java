package com.adias.mytowereasy.api.wso;

import java.util.List;

import com.adias.mytowereasy.model.EbCompagnieCurrency;

public class ExchangeRateUnitIntegrationResponse
{
	private ExchangeRateWSO			exchangeRateWSO;
	private EbCompagnieCurrency	ebCompagnieCurrency;
	private List<String>				integrationErrorsMessages;

	public ExchangeRateUnitIntegrationResponse()
	{
	}

	public ExchangeRateUnitIntegrationResponse(
		ExchangeRateWSO exchangeRateWSO,
		EbCompagnieCurrency ebCompagnieCurrency,
		List<String> integrationErrorsMessages
	)
	{
		super();
		this.exchangeRateWSO = exchangeRateWSO;
		this.ebCompagnieCurrency = ebCompagnieCurrency;
		this.integrationErrorsMessages = integrationErrorsMessages;
	}

	public ExchangeRateWSO getExchangeRateWSO()
	{
		return exchangeRateWSO;
	}

	public void setExchangeRateWSO(ExchangeRateWSO exchangeRateWSO)
	{
		this.exchangeRateWSO = exchangeRateWSO;
	}

	public List<String> getIntegrationErrorsMessages()
	{
		return integrationErrorsMessages;
	}

	public void setIntegrationErrorsMessages(List<String> integrationErrorsMessages)
	{
		this.integrationErrorsMessages = integrationErrorsMessages;
	}

	public EbCompagnieCurrency getEbCompagnieCurrency()
	{
		return ebCompagnieCurrency;
	}

	public void setEbCompagnieCurrency(EbCompagnieCurrency ebCompagnieCurrency)
	{
		this.ebCompagnieCurrency = ebCompagnieCurrency;
	}
}
