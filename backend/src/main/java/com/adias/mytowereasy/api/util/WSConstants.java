package com.adias.mytowereasy.api.util;

public class WSConstants {
    public static final Integer DEFAULT_PAGE_NUMBER = 0;

    public static final Integer DEFAULT_SIZE = 5;

    public static final String DATE_FORMAT_HEURE_EXEMPLE = "yyyy-MM-dd HH:mm:ssXXX";

    public static final String DATE_FORMAT_HEURE_EXEMPLE_VALUE = "2019-12-05 09:18:12-05:00";
}
