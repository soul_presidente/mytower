package com.adias.mytowereasy.api.controller;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.api.service.TransportApiService;
import com.adias.mytowereasy.api.wso.PatchTransportWSO;
import com.adias.mytowereasy.api.wso.SearchCriteriaTransportWSO;
import com.adias.mytowereasy.api.wso.TransportWSO;
import com.adias.mytowereasy.service.ConnectedUserService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/api/v1/transport")
public class TransportApiController extends ApiController {
    @Autowired
    TransportApiService transportApiService;

    @Autowired
    ConnectedUserService connectedUserService;

    @ApiOperation(value = "Search transport request")
    @PostMapping(value = "/search", consumes = "application/json", produces = "application/hal+json")
    public PagedResources<TransportWSO> getTransport(
        @ApiParam(
            value = "Criteria to search a Transport request",
            required = true) @RequestBody @Valid SearchCriteriaTransportWSO criteria,
        PagedResourcesAssembler assembler,
        @PathParam(value = "list of TR grouped ids") String linkToGroupedTR)
        throws Exception {

        if (StringUtils.isNotBlank(linkToGroupedTR)) {
            criteria.setGroupedTRList(linkToGroupedTR);
        }

        PagedResources<TransportWSO> transportResources = assembler
            .toResource(
                transportApiService.getTransportRequest(criteria),
                ControllerLinkBuilder.linkTo(TransportApiController.class).withSelfRel());

        return transportResources;
    }

    @ApiOperation("Create a transport request or a Transport file")
    @PostMapping(consumes = "application/json", produces = "application/hal+json")
    @ApiResponses(value = {
        @ApiResponse(code = 201, message = "Created"),
    })
    public Resource<TransportWSO> createPricingRequest(
        @ApiParam(
            value = "Transport object to store in DB",
            required = true) @RequestBody @Valid TransportWSO transport)
        throws Exception {
        TransportWSO result = transportApiService.createPricingRequest(transport);
        Resource<TransportWSO> resourceWrapper = new Resource<>(result);
        return resourceWrapper;
    }

    @ApiOperation("Update an existing transport request or a Transport file")
    @PutMapping(consumes = "application/json", produces = "application/hal+json")
    public Resource<TransportWSO> updatePricingRequest(
        @ApiParam(value = "Transport object to update", required = true) @RequestBody @Valid TransportWSO transport)
        throws Exception {
        TransportWSO result = transportApiService.updatePricingRequest(transport);
        Resource<TransportWSO> resourceWrapper = new Resource<>(result);
        return resourceWrapper;
    }

    @ApiOperation("Partially Update an existing transport request or a Transport file")
    @PatchMapping(consumes = "application/json", produces = "application/hal+json")
    public @ResponseBody Resource<PatchTransportWSO> partiallyUpdatePricingRequest(
        @ApiParam(
            value = "Transport Informations to update",
            required = true) @RequestBody @Valid PatchTransportWSO patchTransportWSO)
        throws Exception {
        PatchTransportWSO result = transportApiService.partiallyUpdatePricingRequest(patchTransportWSO);
        Resource<PatchTransportWSO> resourceWrapper = new Resource<>(result);
        return resourceWrapper;
    }

    @ApiOperation("Update the status of an existing transport request or transport file")
    @PatchMapping(value = "/confirm", consumes = "application/json", produces = "application/hal+json")
    public @ResponseBody Resource<PatchTransportWSO> UpdateStatusPricingRequest(
        @ApiParam(
            value = "Transport Informations to update",
            required = true) @RequestBody @Valid PatchTransportWSO patchTransportWSO)
        throws Exception {
			PatchTransportWSO result = transportApiService
				.processTransportRequestConfirmation(patchTransportWSO);
        Resource<PatchTransportWSO> resourceWrapper = new Resource<>(result);
        return resourceWrapper;
    }
}
