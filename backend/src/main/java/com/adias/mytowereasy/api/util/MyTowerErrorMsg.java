package com.adias.mytowereasy.api.util;

public class MyTowerErrorMsg {
    private String error;

    public MyTowerErrorMsg() {
        super();
    }

    public MyTowerErrorMsg(String error) {
        super();
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return error;
    }
}
