package com.adias.mytowereasy.api.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.api.util.MyTowerErrorMsg;
import com.adias.mytowereasy.api.wso.CustomFieldWSO;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCustomField;
import com.adias.mytowereasy.repository.EbCustomFieldRepository;
import com.adias.mytowereasy.util.ImportEnMasseUtils;
import com.adias.mytowereasy.util.JsonUtils;


@Service
public class CustomFieldsApiService {
    @Autowired
    EbCustomFieldRepository ebCustomFieldRepository;

    public List<CustomFields> generateCustomFieldsFromWSO(List<CustomFieldWSO> wsoList, Integer ebCompagnieNum) {
        List<CustomFields> listCustomField = null;
        List<MyTowerErrorMsg> customFieldsErrors = new ArrayList<>();
        if (wsoList == null) return new ArrayList<>();

        EbCustomField ebCustomField = ebCustomFieldRepository.findFirstByXEbCompagnie(ebCompagnieNum);
        if (ebCustomField == null) customFieldsErrors
            .add(ImportEnMasseUtils.setErrorMessage("EbCustomField  not found"));

        else listCustomField = JsonUtils
            .<CustomFields> deserializeList(ebCustomField.getFields(), CustomFields.class, new ArrayList<>());

        if (listCustomField == null || listCustomField.isEmpty()) customFieldsErrors
            .add(ImportEnMasseUtils.setErrorMessage("No custom field found"));

        else for (CustomFieldWSO wsoCf: wsoList) {
            CustomFields cf = listCustomField
                .stream().filter(it -> it.getName().contentEquals(wsoCf.getCode())).findFirst().orElse(null);
            if (cf == null) customFieldsErrors
                .add(ImportEnMasseUtils.setErrorMessage("No corresponding custom field found for " + wsoCf.getLabel()));
            else cf.setValue(wsoCf.getValue());
        }

        if (!customFieldsErrors.isEmpty()) throw new MyTowerException(customFieldsErrors);

        return listCustomField;
    }
}
