package com.adias.mytowereasy.api.service;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import javax.jdo.annotations.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.airbus.repository.EbQrGroupePropositionRepository;
import com.adias.mytowereasy.airbus.repository.EbQrGroupeRepository;
import com.adias.mytowereasy.api.controller.TransportApiController;
import com.adias.mytowereasy.api.util.MyTowerErrorMsg;
import com.adias.mytowereasy.api.util.WSConstants;
import com.adias.mytowereasy.api.wso.*;
import com.adias.mytowereasy.dao.DaoPrMtcTransporteur;
import com.adias.mytowereasy.dto.EbTtCompagnyPslLightDto;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.kafka.service.KafkaObjectService;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.*;
import com.adias.mytowereasy.model.enums.ConsolidationGroupage;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EcCurrencyRepository;
import com.adias.mytowereasy.repository.ExEbDemandeTransporteurRepository;
import com.adias.mytowereasy.service.*;
import com.adias.mytowereasy.trpl.model.EbPlCostItem;
import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.util.Constants;
import com.adias.mytowereasy.util.ImportEnMasseUtils;
import com.adias.mytowereasy.util.JsonUtils;
import com.adias.mytowereasy.util.ObjectDeserializer;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Service
public class TransportApiService extends MyTowerService {
    @Autowired
    PricingService pricingService;

    @Autowired
    EbQrGroupePropositionRepository ebQrGroupePropositionRepository;

    @Autowired
    ListStatiqueService listStatiqueService;

    @Autowired
    ExEbDemandeTransporteurRepository exEbDemandeTransporteurRepository;

    @Autowired
    CustomFieldsApiService customFieldsApiService;

    @Autowired
    CategoryApiService categoryApiService;

    @Autowired
    protected EcCurrencyRepository ecCurrencyRepository;
    @Autowired
    private PlanTransportApiService planTransportApiService;

    @Autowired
    EbQrGroupeRepository ebQrGroupeRepository;

    @Autowired
    QrConsolidationService qrConsolidationService;

    @Autowired
    TrackService ebTrackService;

    @Autowired
    private KafkaObjectService kafkaObjectService;

    @Autowired
    private EbDemandeRepository ebDemandeRepository;

	@Autowired
	DaoPrMtcTransporteur daoPrMtcTransporteur;

    public PatchTransportWSO partiallyUpdatePricingRequest(PatchTransportWSO patchTransportWSO) {
        EbDemande ebdemande = null;
        Boolean cfNumFactureExist = false;
        Boolean isUpdated = false;
        EbUser connectedUser = connectedUserService.getCurrentUser();

        if (patchTransportWSO.getItemId() == null &&
                patchTransportWSO.getTransportReference() == null && patchTransportWSO.getCustomerReference() == null) {
            throw new MyTowerException("Reference of pricing request not provided.");
        }

        if (patchTransportWSO.getItemId() != null) {
            ebdemande = ebDemandeRepository.findById(patchTransportWSO.getItemId()).get();
            if (ebdemande == null) throw new MyTowerException(
                    "Item '" + patchTransportWSO.getItemId() + "' of pricing request  not found");
        } else if (patchTransportWSO.getTransportReference() != null) {
            ebdemande = ebDemandeRepository.getByRefTransport(patchTransportWSO.getTransportReference());

            if (ebdemande == null) throw new MyTowerException(
                    "Reference '" + patchTransportWSO.getTransportReference() + "' of pricing request  not found");
        } else if (patchTransportWSO.getCustomerReference() != null) {
            ebdemande = ebDemandeRepository.getByCustomerRef(patchTransportWSO.getCustomerReference());

            if (ebdemande == null) throw new MyTowerException(
                    "Customer reference '" + patchTransportWSO.getTransportReference() + "' of pricing request  not found");
        } else if (patchTransportWSO.getFlightVessel() != null) {
            ebdemande = ebDemandeRepository.getByFlightVessel(patchTransportWSO.getFlightVessel());

            if (ebdemande == null) throw new MyTowerException(
                    "Flight Vessel '" + patchTransportWSO.getFlightVessel() + "' of pricing request  not found");
        } else if (patchTransportWSO.getFlightVessel() != null) {
            ebdemande = ebDemandeRepository.getByMawb(patchTransportWSO.getMawbNumber());

            if (ebdemande == null) throw new MyTowerException(
                    "Master Air Way Bill Number '" + patchTransportWSO.getMawbNumber() + "' of pricing request  not found");
        } else if (patchTransportWSO.getAwbBolNumber() != null) {
            ebdemande = ebDemandeRepository.getByNumAwbBol(patchTransportWSO.getAwbBolNumber());

            if (ebdemande == null) throw new MyTowerException(
                    "Air Way Bill Number '" + patchTransportWSO.getAwbBolNumber() + "' of pricing request  not found");
        }

        if (patchTransportWSO.getConsolidationStatus() != null) {
            Integer consolidationStatusCode = ConsolidationGroupage
                    .getCodeByCodeAlpha(patchTransportWSO.getConsolidationStatus());

            if (consolidationStatusCode == null) throw new MyTowerException(
                    "Grouping status " + patchTransportWSO.getConsolidationStatus() + " not found");
            ebdemande.setxEcStatutGroupage(consolidationStatusCode);
        }
        // mettre à jour le statut de groupage apres reponse conex ESSILOR
        else if (patchTransportWSO.getConsolidationStatus() == null &&
                ebdemande.getxEcNature() != null &&
                ebdemande.getxEcNature().equals(NatureDemandeTransport.CONSOLIDATION.getCode())) {
            ebdemande.setxEcStatutGroupage(ConsolidationGroupage.PVC.getCode());
        }

        if (patchTransportWSO.getCustomFieldList() != null && !patchTransportWSO.getCustomFieldList().isEmpty()) {
            Gson gson = new Gson();
            List<CustomFields> listCustomField = gson
                    .fromJson(ebdemande.getCustomFields(), new TypeToken<ArrayList<CustomFields>>() {
                    }.getType());
            EbCustomField customFieldComp = ebCustomFieldRepository
                    .findFirstByXEbCompagnie(ebdemande.getxEbCompagnie().getEbCompagnieNum());
            List<CustomFields> listCustomFieldComp = gson
                    .fromJson(customFieldComp.getFields(), new TypeToken<ArrayList<CustomFields>>() {
                    }.getType());

            for (CustomFieldWSO trCf : patchTransportWSO.getCustomFieldList()) {
                boolean found = false;

                if (listCustomField != null && !listCustomField.isEmpty()) {
                    CustomFields cf = listCustomField
                            .stream().filter(it -> it.getLabel().contentEquals(trCf.getLabel())).findFirst().orElse(null);

                    if (cf != null) {
                        found = true;
                        cf.setValue(trCf.getValue());
                        String customField = new Gson().toJson(listCustomField);
                        ebdemande.setCustomFields(customField);

                        if (cf.getName().equals("field4") && cf.getValue() != null) {
                            cfNumFactureExist = true;
                        }

                    }

                }

                if (!found && listCustomFieldComp != null && !listCustomFieldComp.isEmpty()) {
                    CustomFields cfCmp = listCustomFieldComp
                            .stream().filter(it -> it.getLabel().contentEquals(trCf.getLabel())).findFirst().orElse(null);

                    if (cfCmp == null) {
                        throw new MyTowerException("No corresponding custom field found for " + trCf.getLabel());
                    } else {
                        cfCmp.setValue(trCf.getValue());
                        if (listCustomField == null) listCustomField = new ArrayList<CustomFields>();
                        listCustomField.add(cfCmp);
                        String customField = new Gson().toJson(listCustomFieldComp);
                        ebdemande.setCustomFields(customField);
                    }

                }

            }

        }

        if ((patchTransportWSO.getCustomFieldList() != null && !patchTransportWSO.getCustomFieldList().isEmpty()) ||
                patchTransportWSO.getConsolidationStatus() != null) {
            ebdemande.setUpdateDate(new Date());
            ebDemandeRepository.save(ebdemande);

            if (cfNumFactureExist && ebdemande.getxEcStatut().equals(StatutDemande.REC.getCode())) {
                ebdemande.getExEbDemandeTransporteurs().get(0).setStatus(Enumeration.StatutCarrier.FIN_PLAN.getCode());
                pricingService
                        .updateStatusQuotation(
                                ebdemande.getExEbDemandeTransporteurs().get(0),
                                false,
                                Enumeration.Module.PRICING.getCode());
            }

        }

        if (!StringUtils.isBlank(patchTransportWSO.getAwbBolNumber())) {
            isUpdated = true;
            ebdemande.setNumAwbBol(patchTransportWSO.getAwbBolNumber());
        }

        if (!StringUtils.isBlank(patchTransportWSO.getMawbNumber())) {
            isUpdated = true;
            ebdemande.setMawb(patchTransportWSO.getMawbNumber());
        }

        if (!StringUtils.isBlank(patchTransportWSO.getFlightVessel())) {
            isUpdated = true;
            ebdemande.setFlightVessel(patchTransportWSO.getFlightVessel());
        }

        if (isUpdated) {
            ebDemandeRepository.save(ebdemande);
        }

        EbDemande d = ebDemandeRepository.findById(ebdemande.getEbDemandeNum()).orElse(null);

        if (d != null) {
            listStatiqueService
                    .historizeAction(
                            ebdemande.getEbDemandeNum(),
                            Enumeration.IDChatComponent.PRICING.getCode(),
                            Enumeration.Module.PRICING.getCode(),
                            "TR updated",
                            connectedUser.getRealUserNum() != null ?
                                    connectedUser.getRealUserNomPrenom() :
                                    connectedUser.getNomPrenom(),
                            connectedUser.getRealUserNum() != null ? connectedUser.getNomPrenom() : null);
        }

        return patchTransportWSO;
    }

    public TransportWSO updatePricingRequest(TransportWSO transport) throws Exception {
        EbDemande demande = new EbDemande();
        demande.setUpdateDate(new Date());
        Integer shipperCompId = null;

        if (transport.getTransportReference() == null ||
                transport.getTransportReference().isEmpty()) throw new MyTowerException(
                "Reference of pricing request not provided.");

        Integer ebDemandeNum = ebDemandeRepository.getEbDemandeNumByRefTransport(transport.getTransportReference());

        if (ebDemandeNum == null) throw new MyTowerException(
                "Reference '" + transport.getTransportReference() + "' of pricing request  not found");

        demande.setEbDemandeNum(ebDemandeNum);
        // required fields
        EbUser requestOwner = ebUserRepository.findOneByEmailIgnoreCase(transport.getShipperUserEmail());
        if (requestOwner
                == null)
            throw new MyTowerException("User with email " + transport.getShipperUserEmail() + " not found");
        demande.setUser(requestOwner);
        shipperCompId = requestOwner.getEbCompagnie().getEbCompagnieNum();

        setExpeditionInformations(transport, demande, shipperCompId);

        setReferenceInformations(transport, demande, shipperCompId, requestOwner);

        setUnitInformations(transport, demande, shipperCompId);

        setPricingInformations(transport, demande);

        setConsolidationInformations(transport, demande);
        demande.setCodeConfigurationEDI(transport.getCodeConfigurationEDI());

        pricingService.updateTransportRequestAndProcessPostUpdate(demande, TypeDataEbDemande.ALL.getCode(), Enumeration.Module.PRICING.getCode(), requestOwner);

        transport = convertEbDemandeToTransport(demande);

        return transport;
    }

    private void setConsolidationInformations(TransportWSO transport, EbDemande demande) throws Exception {

        if (transport.getConsolidationStatus() != null) {
            Integer code = ConsolidationGroupage.getCodeByCodeAlpha(transport.getConsolidationStatus());
            if (code == null) throw new MyTowerException(
                    "Grouping status " + transport.getConsolidationStatus() + " not found");
            demande.setxEcStatutGroupage(code);
        }

    }
    public TransportWSO createPricingRequest(TransportWSO transport) throws Exception {
        PlanTransportWSO planTransportWSO = planTransportApiService.convertTransportWsoToPlanTransportWSO(transport);
        planTransportApiService.getNextPickUpDateOfTransportWSO(planTransportWSO);

        if (planTransportWSO.getNextPickUpDate() != null && transport.getEligibleForConsolidation()) {
            transport.setGoodsAvailabilityDate(planTransportWSO.getNextPickUpDate());
        }

        EbDemande transportRequest = new EbDemande();
        transportRequest.setUpdateDate(new Date());
        List<MyTowerErrorMsg> typeErrors = new ArrayList<>();
        Integer shipperCompId = null;
        EbUser requestOwner = ebUserRepository.findOneByEmailIgnoreCase(transport.getShipperUserEmail());

        if (requestOwner == null) {
            typeErrors
                    .add(
                            ImportEnMasseUtils
                                    .setErrorMessage("User with email " + transport.getShipperUserEmail() + " not found"));
        } else

            shipperCompId = requestOwner.getEbCompagnie().getEbCompagnieNum();

            transportRequest.setUser(requestOwner);

				EbUser connectedUser = requestOwner;
				if (!connectedUser.isChargeur())
				{
					typeErrors
						.add(
							ImportEnMasseUtils
								.setErrorMessage("Connected user is not a charger, check your token")
						);
				}

        if (transport.getCodeConfigurationEDI() != null) {
            transportRequest.setCodeConfigurationEDI(transport.getCodeConfigurationEDI());
        }

        try {
            setExpeditionInformations(transport, transportRequest, shipperCompId);
        } catch (MyTowerException e) {
            e.getListErrorMessage().forEach(t -> typeErrors.add(t));
        }

        try {
            setReferenceInformations(transport, transportRequest, shipperCompId, requestOwner);
        } catch (MyTowerException e) {
            e.getListErrorMessage().forEach(t -> typeErrors.add(t));
        }

        try {
            setUnitInformations(transport, transportRequest, shipperCompId);
        } catch (MyTowerException e) {
            e.getListErrorMessage().forEach(t -> typeErrors.add(t));
        }

        try {
            setPricingInformations(transport, transportRequest);
        } catch (MyTowerException e) {
            e.getListErrorMessage().forEach(t -> typeErrors.add(t));
        }


        if (CollectionUtils.isEmpty(typeErrors)) {
            final Integer moduleId =  transportRequest.getxEcStatut() < StatutDemande.FIN.getCode() ||
                    (transportRequest.getFinalChoiceToBeCalculated() && transportRequest.getxEcStatut() >= StatutDemande.FIN.getCode()) ?
                    Enumeration.Module.PRICING.getCode() :
                    Enumeration.Module.TRANSPORT_MANAGEMENT.getCode();
            EbDemande savedTr = pricingService.createEbDemande(transportRequest, moduleId, true, connectedUser);
            pricingService.sendEmailAfterCreation(savedTr, connectedUser);

            // creation of an history of the date of availability of goods.
						String userName = connectedUser.getNomPrenom();
            final int ebDemandeNum = savedTr.getEbDemandeNum();


						String lang = connectedUser.getLanguage() != null ? connectedUser.getLanguage() : "en";
            Locale lcl = new Locale(lang);
            String[] args = {
                    savedTr.getRefTransport()
            };
            String action = messageSource.getMessage("ws.action.dateAvailabilityGood", args, lcl);

            Integer module;
            Integer idFiche = ebDemandeNum;
            Integer chatCompId;

            for (int i = 0; i < 2; i++) {
                module = (i == 0) ?
                        Enumeration.Module.PRICING.getCode() :
                        Enumeration.Module.TRANSPORT_MANAGEMENT.getCode();
                chatCompId = (i == 0) ?
                        Enumeration.IDChatComponent.PRICING.getCode() :
                        Enumeration.IDChatComponent.TRANSPORT_MANAGEMENT.getCode();
                 listStatiqueService
										.historizeAction(idFiche, chatCompId, module, action, userName, null);
            }

            transport = convertEbDemandeToTransport(savedTr);
        } else {
            throw new MyTowerException(typeErrors);
        }

        return transport;
    }

	private void setUnitInformations(TransportWSO transport, EbDemande d, Integer shipperCompId) {
        List<MyTowerErrorMsg> unitInfosErrors = new ArrayList<>();

        if (transport.getUnitList() == null || transport.getUnitList().isEmpty()) unitInfosErrors
                .add(ImportEnMasseUtils.setErrorMessage("Units not provided"));

        List<EbMarchandise> units = new ArrayList<>();
        List<EbTypeUnit> listEbUnitType = ebTypeUnitRepository.findByXEbCompagnieEbCompagnieNum(shipperCompId);

        Double totalWeight = 0.0, totalNbrParcel = 0.0, totalVolume = 0.0;
        String unitsReference = "";

        if (listEbUnitType == null || listEbUnitType.isEmpty()) unitInfosErrors
                .add(ImportEnMasseUtils.setErrorMessage("List unit type is empty"));

        if (transport.getUnitList() != null) {

            for (UnitWSO unit : transport.getUnitList()) {
                EbMarchandise mr = new EbMarchandise();

                if (unit.getWeight() == null)

                    unitInfosErrors.add(ImportEnMasseUtils.setErrorMessage("Field weight not provided"));
                mr.setWeight(unit.getWeight());
                if (unit.getLength()
                        == null) unitInfosErrors.add(ImportEnMasseUtils.setErrorMessage("Field length not provided"));
                mr.setLength(unit.getLength());
                if (unit.getWidth() == null)

                    unitInfosErrors.add(ImportEnMasseUtils.setErrorMessage("Field width not provided"));
                mr.setWidth(unit.getWidth());
                if (unit.getHeigth()
                        == null) unitInfosErrors.add(ImportEnMasseUtils.setErrorMessage("Field heigth not provided"));
                mr.setHeigth(unit.getHeigth());

                totalWeight += unit.getWeight();

                if (unit.getNumberOfUnits() == null) unitInfosErrors
                        .add(ImportEnMasseUtils.setErrorMessage("Field numberOfUnits not provided"));
                mr.setNumberOfUnits(unit.getNumberOfUnits());

                totalNbrParcel += unit.getNumberOfUnits();

                if (unit.getDangerousGood() != null) {
                    MarchandiseDangerousGood dg = MarchandiseDangerousGood.getByLibelle(unit.getDangerousGood());
                    if (dg == null) unitInfosErrors
                            .add(
                                    ImportEnMasseUtils
                                            .setErrorMessage(
                                                    "Field dangerousGood " + unit.getDangerousGood() + " provided but was not found"));

                    else mr.setDangerousGood(dg.getCode());
                }

                if (unit.getUnitType() == null || unit.getUnitType().isEmpty()) {
                    unitInfosErrors
                            .add(
                                    ImportEnMasseUtils
                                            .setErrorMessage(
                                                    "Field unitType " + unit.getUnitType() + " provided but was not found"));
                }

                EbTypeUnit ebUnitType = listEbUnitType
                        .stream().filter(it -> it.getCode().toUpperCase().contentEquals(unit.getUnitType().toUpperCase()))
                        .findFirst().orElse(null);

                if (ebUnitType == null) {
                    unitInfosErrors
                            .add(ImportEnMasseUtils.setErrorMessage("Field unitType " + unit.getUnitType() + " not found"));
                } else mr.setxEbTypeUnit(ebUnitType.getEbTypeUnitNum());

                mr.setStackable(unit.getStackable());
                mr.setUnitReference(unit.getUnitReference());
                mr.setSscc(unit.getSscc());
                mr.setTracking_number(unit.getTrackingNumber());
                mr.setNomArticle(unit.getItemName());
                mr.setTypeContainer(unit.getContainerType());
                mr.setUn(unit.getUn());
                mr.setClassGood(unit.getClassGood());
                mr.setPackaging(unit.getPackaging());
                mr.setComment(unit.getComment());
                mr.setPrice(unit.getPrice());
                mr.setTypeOfGoodCode(unit.getTypeOfGoodCode());

                if (unit.getUnitReference() != null && !unit.getUnitReference().isEmpty()) {

                    if (unitsReference.length() > 1) {
                        unitsReference += ",";
                    }

                    unitsReference += unit.getUnitReference();
                }

                if (mr.getVolume() == null && unit.getVolume() == null) {
                    Double v = 1.0;
                    v *= mr.getHeigth() * Constants.CentimeterToMeters;
                    v *= mr.getWidth() * Constants.CentimeterToMeters;
                    v *= mr.getLength() * Constants.CentimeterToMeters;

                    mr.setVolume(v);
                } else if (unit.getVolume() != null) {
                    mr.setVolume(unit.getVolume());
                }

                totalVolume += mr.getVolume();

                if (StringUtils.isNotBlank(unit.getOriginCountry()) ||
                        StringUtils.isNotBlank(unit.getManifactoringCountryCode())) {
                    EcCountry countryOrigin = null;

                    if (unit.getOriginCountry() != null) {
                        countryOrigin = listStatiqueService.getEcCountryByLibelle(unit.getOriginCountry());
                    } else {
                        countryOrigin = listStatiqueService.getEcCountryByCode(unit.getManifactoringCountryCode());
                    }

                    if (countryOrigin == null) unitInfosErrors
                            .add(
                                    ImportEnMasseUtils
                                            .setErrorMessage("Field originCountry " + unit.getOriginCountry() + " not found"));
                    else {
                        mr.setOriginCountryName(countryOrigin.getLibelle());
                        mr.setOriginCountryCode(countryOrigin.getCode());
                        mr.setxEcCountryOrigin(countryOrigin.getEcCountryNum());
                    }

                }

                if (StringUtils.isNotBlank(unit.getBinLocation())) {
                    mr.setBinLocation(unit.getBinLocation());
                }

                mr.setExportControl(unit.isExportControl());

                units.add(mr);
            }

        }

        d.setTotalWeight(totalWeight);
        d.setTotalTaxableWeight(calculateTotalTaxableWeightForTR(d.getxEcModeTransport(), totalWeight, totalVolume));

        d.setTotalNbrParcel(totalNbrParcel);
        d.setTotalVolume(totalVolume);

        d.setUnitsReference(unitsReference);

        d.setListMarchandises(units);

        if (!unitInfosErrors.isEmpty()) throw new MyTowerException(unitInfosErrors);
    }

    private Double calculateTotalTaxableWeightForTR(Integer modeTransport, Double totalWeight, Double totalVolume) {
        Double TotalTaxableWeight = 0.0d;

        if (Enumeration.ModeTransport.AIR.getCode().equals(modeTransport)) {
            TotalTaxableWeight = Double.max(totalWeight, ((totalVolume / 6) * 1000));
        } else if (Enumeration.ModeTransport.SEA.getCode().equals(modeTransport)) {
            TotalTaxableWeight = Double.max(totalWeight, (totalVolume * 1000));
        } else if (Enumeration.ModeTransport.ROAD.getCode().equals(modeTransport)) {
            TotalTaxableWeight = Double.max(totalWeight, ((totalVolume / 3) * 1000));
        } else if (Enumeration.ModeTransport.INTEGRATOR.getCode().equals(modeTransport)) {
            TotalTaxableWeight = Double.max(totalWeight, ((totalVolume * 1000000) / 5000));
        }

        TotalTaxableWeight = arrondiToZeroFive(TotalTaxableWeight);
        return TotalTaxableWeight;
    }

    // calcul auparavant dans la front
    public Double arrondiToZeroFive(Double value) {

        if (value != Math.floor(value)) {
            value = Math.floor(value * 10) / 10.0;
            String s = Double.toString(value);
            String s1 = s.replace('.', ';');
            String str[] = s1.split(";");
            List<String> list = new ArrayList<String>();
            list = Arrays.asList(str);

            if (!list.isEmpty() && list.size() >= 2) {
                Double d = Double.parseDouble(list.get(1));

                if (d > 5) {
                    return Math.floor(value) + 1;
                } else {
                    return Math.floor(value) + 0.5;
                }

            } else {
                return Math.floor(value) + 0.5;
            }

        }

        return value;
    }

    private void
    setReferenceInformations(TransportWSO transport, EbDemande d, Integer shipperCompId, EbUser requestOwner)
            throws JsonProcessingException {
        List<MyTowerErrorMsg> refInformationsErrors = new ArrayList<>();

        if (transport.getCostCenterCode() != null) {
            EbCostCenter costCenter = ebCostCenterRepository
                    .findFirstByCompagnie_ebCompagnieNumAndLibelle(shipperCompId, transport.getCostCenterCode());
            if (costCenter == null) refInformationsErrors
                    .add(
                            ImportEnMasseUtils
                                    .setErrorMessage(
                                            "Cost center " + transport.getCostCenterCode() + " is provided but not found"));

            d.setxEbCostCenter(costCenter);
        }

        d.setNumAwbBol(transport.getNumAwbBol());
        d.setFlightVessel(transport.getFlightVessel());
        d.setCustomsOffice(transport.getCustomsOffice());
        d.setMawb(transport.getMawb());

        if (transport.getOriginUserEmail() != null) {
            EbUser user = ebUserRepository.findOneByEmailIgnoreCase(transport.getOriginUserEmail());
            if (user == null) refInformationsErrors
                    .add(
                            ImportEnMasseUtils
                                    .setErrorMessage("Origin user with email " + transport.getOriginUserEmail() + " not found"));

            d.setxEbUserOrigin(user);
        }

        if (transport.getOriginCustomUserEmail() != null) {
            EbUser user = ebUserRepository.findOneByEmailIgnoreCase(transport.getOriginCustomUserEmail());
            if (user == null) refInformationsErrors
                    .add(
                            ImportEnMasseUtils
                                    .setErrorMessage(
                                            "Origin custom user with email " + transport.getOriginCustomUserEmail() + " not found"));

            d.setxEbUserOriginCustomsBroker(user);
        }

        if (transport.getDestUserEmail() != null) {
            EbUser user = ebUserRepository.findOneByEmailIgnoreCase(transport.getDestUserEmail());
            if (user == null) refInformationsErrors
                    .add(
                            ImportEnMasseUtils
                                    .setErrorMessage("Destination user with email " + transport.getDestUserEmail() + " not found"));
            d.setxEbUserDest(user);
        }

        if (transport.getDestCustomUserEmail() != null) {
            EbUser user = ebUserRepository.findOneByEmailIgnoreCase(transport.getDestCustomUserEmail());
            if (user == null) refInformationsErrors
                    .add(
                            ImportEnMasseUtils
                                    .setErrorMessage(
                                            "Destination Custom user with email " + transport.getDestCustomUserEmail() + " not found"));
            d.setxEbUserDestCustomsBroker(user);
        }

        if (transport.getObserverUserEmailList() != null && !transport.getObserverUserEmailList().isEmpty()) {
            List<EbUser> listObsUser = new ArrayList<>();

            for (String obsEmail : transport.getObserverUserEmailList()) {
                EbUser user = ebUserRepository.findOneByEmailIgnoreCase(obsEmail);
                if (user == null) refInformationsErrors
                        .add(ImportEnMasseUtils.setErrorMessage("Observer user with email " + obsEmail + " not found"));

                listObsUser.add(user);
            }

            d.setxEbUserObserver(listObsUser);
        }

        if (transport.getCategoryList() != null && !transport.getCategoryList().isEmpty() && requestOwner != null) {

            try {
                List<EbCategorie> listCategorieFinal = categoryApiService
                        .generateCategoriesFromWSO(
                                transport.getCategoryList(),
                                requestOwner.getEbCompagnie().getEbCompagnieNum());
                d.setListCategories(listCategorieFinal);
            } catch (MyTowerException e) {
                refInformationsErrors.addAll(e.getListErrorMessage());
            }

        }

        if (transport.getCustomFieldList() != null && !transport.getCustomFieldList().isEmpty()) {

            try {
                List<CustomFields> listCustomField = customFieldsApiService
                        .generateCustomFieldsFromWSO(transport.getCustomFieldList(), shipperCompId);
                d.setCustomFields(JsonUtils.serializeToString(listCustomField));
                d.setListCustomsFields(listCustomField);
            } catch (MyTowerException e) {
                refInformationsErrors.addAll(e.getListErrorMessage());
            }

        }

        if (!refInformationsErrors.isEmpty()) throw new MyTowerException(refInformationsErrors);
    }

	private void setPricingInformations(TransportWSO transport, EbDemande transportRequest)
			throws JsonProcessingException {
        List<MyTowerErrorMsg> pricingInfosErrors = new ArrayList<>();
        Integer code;
				Optional<EcCurrency> ecCurrency;
		Integer status = transportRequest.getxEcStatut();

        if (transport.getQuotationList() != null && !transport.getQuotationList().isEmpty()) {
            List<EbTtCompanyPsl> listPsl = null;

			if (transportRequest.getxEbSchemaPsl() != null) {
				listPsl = transportRequest
                        .getxEbSchemaPsl().getListPsl().stream()
                        .filter(
                                it -> it.getSchemaActif() != null &&
                                        it.getSchemaActif() && it.getIsChecked() != null && it.getIsChecked() &&
                                        it.getDateAttendue() != null && it.getDateAttendue())
                        .collect(Collectors.toList());
            }

			transportRequest.setExEbDemandeTransporteurs(new ArrayList<ExEbDemandeTransporteur>());

            for (QuotationWSO qr : transport.getQuotationList()) {
				ExEbDemandeTransporteur quotation = null;
                ExEbDemandeTransporteur quotationForMtc = null;

                if (qr.getCarrierUserEmail()
                        == null)
                    pricingInfosErrors.add(ImportEnMasseUtils.setErrorMessage("Carrier email not provided"));
                EbUser user = ebUserRepository.findOneByEmailIgnoreCase(qr.getCarrierUserEmail());

                if (user == null) pricingInfosErrors
                        .add(
                                ImportEnMasseUtils
                                        .setErrorMessage("Carrier with email " + qr.getCarrierUserEmail() + " not found"));
                else {

					if (transportRequest.getFinalChoiceToBeCalculated()
							&& transportRequest.getxEcStatut() >= StatutDemande.FIN.getCode()) {
                        quotationForMtc = createQuotation(user, Enumeration.StatutCarrier.FIN_PLAN.getCode());
                    } else if (!StringUtils
                            .isNotBlank(
                                    qr.getStatus()))
						quotation = createQuotation(user, Enumeration.StatutCarrier.RRE.getCode());
                    else {
                        code = Enumeration.StatutCarrier.getCodeByCodeLibelle(qr.getStatus());
                        if (code == null) pricingInfosErrors
                                .add(ImportEnMasseUtils.setErrorMessage("Carrier status " + qr.getStatus() + " not found"));
						quotation = createQuotation(user, code);
                    }

					if (quotation != null) {

						if (quotation.getStatus() >= Enumeration.StatutCarrier.FIN.getCode()) {
							quotation.getxTransporteur().setSelected(true);
                            status = Enumeration.StatutDemande.WPU.getCode();
						} else if (quotation.getStatus() > Enumeration.StatutCarrier.RRE.getCode()
								&& !(transportRequest.getFinalChoiceToBeCalculated()
										&& transportRequest.getxEcStatut() >= StatutDemande.FIN.getCode()))
                            status = Enumeration.StatutDemande.REC
                                    .getCode();

						if (quotation.getStatus() > Enumeration.StatutCarrier.RRE.getCode()) {
                            if (qr.getTransitTime() == null) pricingInfosErrors
                                    .add(
                                            ImportEnMasseUtils
                                                    .setErrorMessage(
                                                            "Status corresponding to a sent quote but transit time not provided for carrier " +
                                                                    qr.getCarrierUserEmail()));
							quotation.setTransitTime(qr.getTransitTime());

                            if (qr.getCarrierCurrencyCode()
									== null)
								qr.setCarrierCurrencyCode(transportRequest.getXecCurrencyInvoice().getCode());

                            ecCurrency = ecCurrencyRepository
                                    .findByCodeIgnoreCase(qr.getCarrierCurrencyCode().toLowerCase());

														if (!ecCurrency.isPresent())
														{
                                pricingInfosErrors
                                        .add(
                                                ImportEnMasseUtils
                                                        .setErrorMessage(
                                                                "Carrier currency with code " +
                                                                        qr.getCarrierCurrencyCode() + " not found"));
							} else if (transportRequest.getXecCurrencyInvoice().getEcCurrencyNum().intValue()
								!= ecCurrency.get().getEcCurrencyNum().intValue()
							)
							{
                                SearchCriteria criteria = new SearchCriteria();
                                criteria.setEbCompagnieGuestNum(user.getEbCompagnie().getEbCompagnieNum());
								criteria.setEbCompagnieCibleNum(
										transportRequest.getUser().getEbCompagnie().getEbCompagnieNum());
								criteria.setCurrencyGuestNum(ecCurrency.get().getEcCurrencyNum());
								criteria.setCurrencyCibleNum(
										transportRequest.getXecCurrencyInvoice().getEcCurrencyNum());
                                List<EbCompagnieCurrency> listCompCurrency = daoCurrency
                                        .selectListEbCompagnieCurrency(criteria);
                                if (listCompCurrency == null || listCompCurrency.isEmpty()) pricingInfosErrors
                                        .add(
                                                ImportEnMasseUtils
                                                        .setErrorMessage(
                                                                "No configured currency with code " +
                                                                        qr.getCarrierCurrencyCode() + " for carrier " +
                                                                        user.getEmail()));
								quotation
                                        .setxEbCompagnieCurrency(
                                                new EbCompagnieCurrency(
                                                        listCompCurrency.get(0).getEbCompagnieCurrencyNum()));
                            }

							quotation.setComment(qr.getComment());

                            if (qr.getModeTransport() == null) qr.setModeTransport(transport.getTransportMode());
                            code = Enumeration.ModeTransport.getCodeByLibelle(qr.getModeTransport().toUpperCase());
                            if (code == null) pricingInfosErrors
                                    .add(
                                            ImportEnMasseUtils
                                                    .setErrorMessage(
                                                            "Carrier's transport mode with code " +
                                                                    qr.getModeTransport() + " not found"));
							quotation.setxEcModeTransport(code);
                            if (qr.getPrice() == null) pricingInfosErrors
                                    .add(
                                            ImportEnMasseUtils
                                                    .setErrorMessage(
                                                            "Price not provided for carrier with email " + user.getEmail()));

							quotation.setPrice(qr.getPrice());

                            if (qr.getNegotiatedDateList() != null && !qr.getNegotiatedDateList().isEmpty()) {

                                // gérer les dates estimées pour les quotes
                                if (listPsl != null && !listPsl.isEmpty()) {
									if (quotation.getListEbTtCompagniePsl() == null)
										quotation.setListEbTtCompagniePslFromListCompanyPsl(listPsl);

									if (quotation.getListEbTtCompagniePsl() != null
											&& !quotation.getListEbTtCompagniePsl().isEmpty()) {

                                        for (PslWSO psl : qr.getNegotiatedDateList()) {
                                            boolean found = false;

											for (EbTtCompagnyPslLightDto cmpPsl : quotation.getListEbTtCompagniePsl()) {

                                                if (cmpPsl
                                                        .getCodeAlpha().toUpperCase()
                                                        .contentEquals(psl.getTrackingPointCode().toUpperCase())) {
                                                    cmpPsl.setExpectedDate(psl.getDate());
                                                    found = true;
                                                    break;
                                                }

                                            }

                                            if (!found) pricingInfosErrors
                                                    .add(
                                                            ImportEnMasseUtils
                                                                    .setErrorMessage(
                                                                            "Tracking point code " +
                                                                                    psl.getTrackingPointCode() +
                                                                                    " provided in negotiatedDateList field but corresponding tracking poing not found"));
                                        }

                                    }

                                }

                            }

                        }

						transportRequest.getExEbDemandeTransporteurs().add(quotation);
                    }

					if (quotationForMtc != null)
						transportRequest.getExEbDemandeTransporteurs().add(quotationForMtc);
                }

            }
        }

		transportRequest.setxEcStatut(status);
		transportRequest.setCustomerCreationDate(transport.getCustomerCreationDate());
		transportRequest.setxEcNature(Enumeration.NatureDemandeTransport.EDI.getCode());
        if (!pricingInfosErrors.isEmpty()) throw new MyTowerException(pricingInfosErrors);
    }

	private ExEbDemandeTransporteur createQuotation(EbUser user, Integer status) {
        ExEbDemandeTransporteur quotation = new ExEbDemandeTransporteur();
        quotation.setxEbEtablissement(user.getEbEtablissement());
        quotation.setxEbCompagnie(user.getEbCompagnie());
        quotation.setxTransporteur(user);
        quotation.setStatus(status);

        return quotation;
    }

    private void setExpeditionInformations(TransportWSO transport, EbDemande d, Integer shipperCompId)
            throws Exception {
        List<MyTowerErrorMsg> expInfosErrors = new ArrayList<MyTowerErrorMsg>();
        Integer code = null;

        if (transport.getTransportMode() != null) {
            code = Enumeration.ModeTransport.getCodeByLibelle(transport.getTransportMode().toUpperCase());
        }

        if (code == null) expInfosErrors
                .add(
                        ImportEnMasseUtils
                                .setErrorMessage("Transport mode with code " + transport.getTransportMode() + " not found"));

        d.setxEcModeTransport(code);
				Optional<EcCurrency> ecCurrency = null;

        if (transport.getInvoicingCurrencyCode() != null) {
            ecCurrency = ecCurrencyRepository.findByCodeIgnoreCase(transport.getInvoicingCurrencyCode().toLowerCase());
        }

				if (!ecCurrency.isPresent())
					expInfosErrors
                .add(
                        ImportEnMasseUtils
                                .setErrorMessage(
                                        "Invoice currency with code " + transport.getInvoicingCurrencyCode() + " not found"));

				d.setXecCurrencyInvoice(ecCurrency.get());

        code = Enumeration.StatutDemande.getListCodeByLibelleJason(transport.getTransportStatus());
        if (code == null) expInfosErrors
                .add(
                        ImportEnMasseUtils
                                .setErrorMessage("Transport status with code " + transport.getTransportStatus() + " not found"));

        d.setxEcStatut(code);
        EbTypeRequest ebTypeRequest = ebTypeRequestRepository
                .findByReferenceIgnoreCaseAndXEbCompagnie_ebCompagnieNum(transport.getServiceLevel(), shipperCompId);

        if (ebTypeRequest == null)

            expInfosErrors
                    .add(
                            ImportEnMasseUtils
                                    .setErrorMessage("Service level with code " + transport.getServiceLevel() + " not found"));

        else {
            d.setxEbTypeRequest(ebTypeRequest.getEbTypeRequestNum());
            d.setTypeRequestLibelle(ebTypeRequest.getRefLibelle());
        }

        EbTypeFlux ebTypeFlux = ebTypeFluxRepository
                .findByCodeAndXEbCompagnie_ebCompagnieNum(transport.getFlowTypeCode(), shipperCompId);

        if (ebTypeFlux == null) expInfosErrors
                .add(
                        ImportEnMasseUtils
                                .setErrorMessage("Flow type with code " + transport.getFlowTypeCode() + " not found"));

        else {
            d.setxEbTypeFluxNum(ebTypeFlux.getEbTypeFluxNum());
            d.setxEbTypeFluxDesignation(ebTypeFlux.getDesignation());
        }

        EbIncoterm ebIncoterm = ebIncotermRepository
                .findFirstByLibelleAndXEbCompagnie_ebCompagnieNum(transport.getIncoterm(), shipperCompId);

        if (ebIncoterm == null) expInfosErrors
                .add(ImportEnMasseUtils.setErrorMessage("Incoterm " + transport.getIncoterm() + " not found"));

        else {
            d.setxEbIncotermNum(ebIncoterm.getEbIncotermNum());
            d.setxEcIncotermLibelle(ebIncoterm.getLibelle());
        }

        d.setCity(transport.getCity());

        if (transport.getTransportType() != null) {
            EbTypeTransport typeTransport = ebTypeTransportRepository
                    .findEbTypeTransportByLibeleAndModeTransport(
                            transport.getTransportType().toUpperCase(),
                            d.getxEcModeTransport());
            if (typeTransport == null)

                expInfosErrors
                        .add(
                                ImportEnMasseUtils
                                        .setErrorMessage(
                                                "Transport type " + transport.getTransportType() + " provided but not found"));
            else d.setxEbTypeTransport(typeTransport.getEbTypeTransportNum());
        }

        d.setCustomerReference(transport.getCustomerReference());

        EbTtSchemaPsl schema = ebTtSchemaPslRepository
                .findFirstByXEbCompagnie_ebCompagnieNumAndDesignation(shipperCompId, transport.getTrackingPointSchema());

        if (schema == null) expInfosErrors
                .add(
                        ImportEnMasseUtils
                                .setErrorMessage(
                                        "Tracking schema " + transport.getTrackingPointSchema() + " provided but not found"));

        else {
            schema.setxEbCompagnie(null);
            schema
                    .setListPsl(
                            ObjectDeserializer
                                    .checkJsonListObject(schema.getListPsl(), new TypeToken<ArrayList<EbTtCompanyPsl>>() {
                                    }.getType()));

            for (EbTtCompanyPsl psl : schema.getListPsl()) {
                psl.setIsChecked(true);
            }

            schema.getListPsl().stream().forEach(it -> it.setSchemaActif(true));
            if (schema.getListPsl() != null) schema.getListPsl().stream().forEach(it -> it.setEbCompagnie(null));
            d.setxEbSchemaPsl(schema);
        }

        if (transport.getTrackingPointCodeList() != null) {
            transport.getTrackingPointCodeList().stream().forEach(it -> {

                for (EbTtCompanyPsl p : d.getxEbSchemaPsl().getListPsl()) {

                    if (it.toUpperCase().contentEquals(p.getCodeAlpha().toUpperCase())) {
                        p.setSchemaActif(true);
                        break;
                    }

                }

                ;
            });
        }

        d.setDateOfArrival(transport.getArrivalDate());

        d.setDateOfGoodsAvailability(transport.getGoodsAvailabilityDate());

        d.setWaitingForQuote(new Date());

        d.setInsurance(transport.getInsurance());

        if (d.getInsurance() != null && d.getInsurance()) {
            if (transport.getInsuranceValue() == null) expInfosErrors
                    .add(ImportEnMasseUtils.setErrorMessage("Insurance is active but no insurance value is provided"));
            d.setInsuranceValue(transport.getInsuranceValue());

            if (transport.getInsuranceCurrencyCode() == null) expInfosErrors
                    .add(ImportEnMasseUtils.setErrorMessage("Insurance is active but no insurance currency is provided"));

            ecCurrency = ecCurrencyRepository.findByCodeIgnoreCase(transport.getInsuranceCurrencyCode().toLowerCase());
            if (ecCurrency == null) expInfosErrors
                    .add(
                            ImportEnMasseUtils
                                    .setErrorMessage(
                                            "Insurance currency with code " + transport.getInsuranceCurrencyCode() + " not found"));
						d.setXecCurrencyInsurance(ecCurrency.get());
        }

        EbParty p = null;

        try {
            p = getParty(transport.getOriginInformation(), "Origin information", shipperCompId);
            d.setEbPartyOrigin(p);
        } catch (MyTowerException e) {
            expInfosErrors.addAll(e.getListErrorMessage());
        }

        try {
            p = getParty(transport.getDestinationInformation(), "Destination information", shipperCompId);
            d.setEbPartyDest(p);
        } catch (MyTowerException e) {
            expInfosErrors.addAll(e.getListErrorMessage());
        }

        try {
            p = getParty(transport.getNotifyInformation(), "Notify information", shipperCompId);
            d.setEbPartyNotif(p);
        } catch (MyTowerException e) {
            expInfosErrors.addAll(e.getListErrorMessage());
        }

        try {
            p = getParty(transport.getIntermediaryPoint(), "Intermediary point information", shipperCompId);
            d.setPointIntermediate(p);
        } catch (MyTowerException e) {
            expInfosErrors.addAll(e.getListErrorMessage());
        }

        d
                .setEligibleForConsolidation(
                        transport.getEligibleForConsolidation() != null ? transport.getEligibleForConsolidation() : false);
        d
                .setFinalChoiceToBeCalculated(
                        transport.getFinalChoiceToBeCalculated() != null ? transport.getFinalChoiceToBeCalculated() : false);

        if (transport.getEstimatedDateList() != null && !transport.getEstimatedDateList().isEmpty()) {

            for (PslWSO psl : transport.getEstimatedDateList()) {
                if (psl.getTrackingPointCode() == null) expInfosErrors
                        .add(
                                ImportEnMasseUtils
                                        .setErrorMessage("Tracking point code not provided in estimatedDateList field"));

                boolean found = false;

                for (EbTtCompanyPsl compPsl : d.getxEbSchemaPsl().getListPsl()) {

                    if (compPsl.getCodeAlpha().toUpperCase().contentEquals(psl.getTrackingPointCode().toUpperCase()) &&
                            compPsl.getDateAttendue() != null && compPsl.getDateAttendue()) {
                        found = true;
                        compPsl.setExpectedDate(psl.getDate());
                        break;
                    }

                }

                if (!found) expInfosErrors
                        .add(
                                ImportEnMasseUtils
                                        .setErrorMessage(
                                                "Tracking point code " +
                                                        psl.getTrackingPointCode() +
                                                        " provided in estimatedDateList field but corresponding tracking poing not found"));
            }

        }

		if (!expInfosErrors.isEmpty())
			throw new MyTowerException(expInfosErrors);
    }


    private EbParty getParty(AddressWSO p, String objectName, Integer ebCompagnieNum) throws Exception {
        List<MyTowerErrorMsg> ebPartyInfosErrors = new ArrayList<>();
        EbParty ebp = null;

        if (p == null) return null;
        ebp = new EbParty();
        ebp.setReference(p.getReference());
        ebp.setAdresse(p.getAddress());
        ebp.setAirport(p.getAirport());
        ebp.setCity(p.getCity());
        ebp.setCompany(p.getCompany());
        EcCountry cc = ecCountryRepository.findFirstByCode(p.getCountryCode().toUpperCase());
        if (cc == null) ebPartyInfosErrors
                .add(ImportEnMasseUtils.setErrorMessage("Country " + p.getCountryCode() + " provided but not found "));

        ebp.setxEcCountry(cc);
        ebp.setEmail(p.getEmail());
        ebp.setOpeningHours(p.getOpeningHours());
        ebp.setPhone(p.getPhone());
        ebp.setZipCode(p.getZipCode());
        ebp.setCommentaire(p.getComment());

        if (p.getZoneReference() != null) {
            EbPlZone zone = ebZoneRepository
                    .findFirstByRefIgnoreCaseAndEbCompagnie_ebCompagnieNum(p.getZoneReference(), ebCompagnieNum);

            if (zone == null) ebPartyInfosErrors
                    .add(
                            ImportEnMasseUtils
                                    .setErrorMessage(
                                            "Zone reference " +
                                                    p.getZoneReference() + " provided but corresponding zone was not found "));
            else {
                ebp.setZoneRef(zone.getRef());
                ebp.setZoneDesignation(zone.getDesignation());
                ebp.setZoneNum(zone.getEbZoneNum());
                ebp.setZone(zone);
            }

        }

        if (!ebPartyInfosErrors.isEmpty()) throw new MyTowerException(ebPartyInfosErrors);
        return ebp;
    }

    @SuppressWarnings("rawtypes")
    public Page<Resource<TransportWSO>> getTransportRequest(SearchCriteriaTransportWSO criteria) throws Exception {
        List<MyTowerErrorMsg> typeErrors = new ArrayList<>();

        if (criteria.getPage() == null) {
            criteria.setPage(WSConstants.DEFAULT_PAGE_NUMBER);
        }

        if (criteria.getSize() == null) {
            criteria.setSize(WSConstants.DEFAULT_SIZE);
        }

        List<EbDemande> listDemande = null;
        SearchCriteriaPricingBooking searchCriteriaPricingBooking = getSearchCriteria(criteria);
        searchCriteriaPricingBooking.setGrouping(criteria.getIsGrouping());

        searchCriteriaPricingBooking.setValorized(criteria.getIsValorized());

        if (criteria.getTransportStatusList() != null && !criteria.getTransportStatusList().isEmpty()) {

            for (String status : criteria.getTransportStatusList()) {

                if (status.equals(StatutDemande.PND.getLibelleJason())) {
                    searchCriteriaPricingBooking.setxEcStatut(StatutDemande.PND.getCode());
                } else if (status.equals(StatutDemande.INP.getLibelleJason())) {
                    searchCriteriaPricingBooking.setxEcStatut(StatutDemande.INP.getCode());
                } else if (status.equals(StatutDemande.WRE.getLibelleJason())) {
                    searchCriteriaPricingBooking.setxEcStatut(StatutDemande.WRE.getCode());
                } else if (status.equals(StatutDemande.REC.getLibelleJason())) {
                    searchCriteriaPricingBooking.setxEcStatut(StatutDemande.REC.getCode());
                } else if (status.equals(StatutDemande.FIN.getLibelleJason())) {
                    searchCriteriaPricingBooking.setxEcStatut(StatutDemande.FIN.getCode());
                } else {
                    typeErrors.add(ImportEnMasseUtils.setErrorMessage("status " + status + " not found"));
                }

            }

        }

        if (typeErrors.isEmpty()) {
            searchCriteriaPricingBooking.setTotalTaxableWeight(criteria.getTotalTaxableWeight());

            if (criteria.getGroupedTRList() != null && !criteria.getGroupedTRList().isEmpty()) {
                List<Integer> listEbDemandeGroupedNum = Arrays
                        .asList(StringUtils.split(criteria.getGroupedTRList(), ":")).stream().map(s -> Integer.parseInt(s))
                        .collect(Collectors.toList());
                searchCriteriaPricingBooking.setListEbDemandeNum(listEbDemandeGroupedNum);
            }

            listDemande = pricingService.getListEbDemande(searchCriteriaPricingBooking);

            int counter = pricingService.getCountListEbDemande(searchCriteriaPricingBooking);

            List<Resource<TransportWSO>> listResourceTransport = new ArrayList<>();

            for (EbDemande demande : listDemande) {
                String listEbDemandeNumGrouped = null;
                demande.setListMarchandises(setMarchandisesList(demande.getEbDemandeNum()));
				List<ExEbDemandeTransporteur> transporteurList = new ArrayList<>(
						getListTransporteursByDemandeNum(demande.getEbDemandeNum()));
				demande.setExEbDemandeTransporteurs(transporteurList);

                TransportWSO transport = convertEbDemandeToTransport(demande);

                if (criteria.isFetchConsolidationInformation() &&
                        demande.getxEcStatut().equals(StatutDemande.PND.getCode())) {
                    List<Integer> listEbDemandeGroupedNum = ebDemandeRepository
                            .selectDemandeNumInGroup(demande.getEbDemandeNum());
                    searchCriteriaPricingBooking.setListEbDemandeNum(listEbDemandeGroupedNum);

                    SearchCriteriaPricingBooking searchCriteriaPricingBooking1 = new SearchCriteriaPricingBooking();
                    searchCriteriaPricingBooking1.setSearchByWS(true);
                    searchCriteriaPricingBooking1.setModule(searchCriteriaPricingBooking.getModule());
                    searchCriteriaPricingBooking1
                            .setConnectedUserNum(searchCriteriaPricingBooking.getConnectedUserNum());
                    searchCriteriaPricingBooking1.setListEbDemandeNum(listEbDemandeGroupedNum);

                    List<EbDemande> listDemandeInitaux = pricingService.getListEbDemande(searchCriteriaPricingBooking1);

                    List<TransportLightWSO> transportLightWSOList = new ArrayList<>();

                    if (!listDemandeInitaux.isEmpty()) {
                        TransportLightWSO transportLightWSO;

                        for (EbDemande ebDemande : listDemandeInitaux) {
                            ebDemande.setListMarchandises(setMarchandisesList(ebDemande.getEbDemandeNum()));
                            ebDemande
                                    .setExEbDemandeTransporteurs(
                                            getListTransporteursByDemandeNum(ebDemande.getEbDemandeNum()));
                            transportLightWSO = convertEbDemandeToTransportLightWSO(ebDemande);
                            transportLightWSOList.add(transportLightWSO);
                        }

                    }

                    transport.setInitialTransportList(transportLightWSOList);

                    String apiLink = ControllerLinkBuilder.linkTo(TransportApiController.class).toString();
                    Link linkToGroupedTR = new Link(apiLink + "/search?linkToGroupedTR=" + listEbDemandeNumGrouped);
                    Resource resource = new Resource(transport, linkToGroupedTR);
                    listResourceTransport.add(resource);
                } else {
                    Resource resource = new Resource(transport);
                    listResourceTransport.add(resource);
                }

            }

            Page<Resource<TransportWSO>> pages = new PageImpl<>(
                    listResourceTransport,
                    PageRequest.of(criteria.getPage(), criteria.getSize()),
                    counter);

            return pages;
        } else {
            throw new MyTowerException(typeErrors);
        }

    }

    private List<Integer> getListTransportStatusCodeByListLibelle(List<String> listStatus) {
        List<Integer> listTransportStatusCode = new ArrayList<>();

        for (String status : listStatus) {
            Integer statusCode = StatutDemande.getListCodeByLibelleJason(status);

            if (statusCode != null) {
                listTransportStatusCode.add(statusCode);
            }

        }

        return listTransportStatusCode;
    }

    private SearchCriteriaPricingBooking getSearchCriteria(SearchCriteriaTransportWSO criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        SearchCriteriaPricingBooking searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();

        if (StringUtils.isNotBlank(criteria.getNatureTransport())) {
            searchCriteriaPricingBooking
                    .setxEcNature(Enumeration.NatureDemandeTransport.getCodeByLibelle(criteria.getNatureTransport()));
        }

        if (StringUtils.isNotBlank(criteria.getTransporterEmail())) {
            searchCriteriaPricingBooking.setTransporterEmail(criteria.getTransporterEmail());
        }

        if (StringUtils.isNotBlank(criteria.getTransporterCompanyCode())) {
            searchCriteriaPricingBooking.setTransporterCompanyCode(criteria.getTransporterCompanyCode());
        }

        searchCriteriaPricingBooking.setValorized(criteria.getIsValorized());

        searchCriteriaPricingBooking.setGrouping(criteria.getIsGrouping());

        searchCriteriaPricingBooking.setListTransportRef(criteria.getListTransportRef());
        searchCriteriaPricingBooking.setListCustomerReference(criteria.getListCustomerReference());
        searchCriteriaPricingBooking.setModule(Enumeration.Module.PRICING.getCode());
        searchCriteriaPricingBooking.setConnectedUserNum(connectedUser.getEbUserNum());
        searchCriteriaPricingBooking.setSearchByWS(true);
        searchCriteriaPricingBooking.setSize(criteria.getSize());
        searchCriteriaPricingBooking.setPageNumber(criteria.getPage());

        if (criteria.getTransportStatusList() != null) {
            searchCriteriaPricingBooking
                    .setListStatut(getListTransportStatusCodeByListLibelle(criteria.getTransportStatusList()));
        }

        searchCriteriaPricingBooking.setDateCreationFrom(criteria.getTransportCreationDateFrom());
        searchCriteriaPricingBooking.setDateCreationTo(criteria.getTransportCreationDateTo());

        searchCriteriaPricingBooking.setNumAwbBol(criteria.getNumAwbBol());

        if (criteria.getCategoryList() != null && !criteria.getCategoryList().isEmpty()) {
            List<Integer> listCategoryFields = new ArrayList<>();

            for (CategoryWSO trCat : criteria.getCategoryList()) {
                List<Integer> listLabelNum = ebLabelRepository
                        .findLabelsNumBycategorieLabelAndLabel(trCat.getCategory(), trCat.getCategoryLabel());

                if (listLabelNum != null && !listLabelNum.isEmpty()) {
                    listCategoryFields.addAll(listLabelNum);
                }

            }

            if (!listCategoryFields.isEmpty()) {
                Map<String, List<Integer>> mapCategoryFields = new HashMap<>();
                mapCategoryFields.put("categoryField1", listCategoryFields);
                searchCriteriaPricingBooking.setMapCategoryFields(mapCategoryFields);
            }

        }

        if (criteria.getCustomFieldList() != null && !criteria.getCustomFieldList().isEmpty()) {
            List<String> listCustomFields = new ArrayList<>();

            for (CustomFieldWSO trCf : criteria.getCustomFieldList()) {
                listCustomFields.add("\"" + trCf.getCode() + "\":" + "\"" + trCf.getValue() + "\"");
            }

            searchCriteriaPricingBooking.setListCustomFieldsValues(listCustomFields);
        }

        if (criteria.getConsolidationStatus() != null) {
            searchCriteriaPricingBooking
                    .setStatutGroupage(ConsolidationGroupage.getCodeByCodeAlpha(criteria.getConsolidationStatus()));
        }

        return searchCriteriaPricingBooking;
    }

    private TransportWSO convertEbDemandeToTransport(EbDemande demande) {
        TransportWSO transport = new TransportWSO();
        transport.setItemId(demande.getEbDemandeNum());
        transport.setArrivalDate(demande.getDateOfArrival());
        transport.setGoodsAvailabilityDate(demande.getDateOfGoodsAvailability());
        transport.setTransportUpdateDate(demande.getDateMaj());
        transport.setCity(demande.getCity());

        if (demande.getxEbCostCenter() != null) {
            transport.setCostCenterCode(demande.getxEbCostCenter().getLibelle());
        }

        transport.setCustomerReference(demande.getCustomerReference());
        transport.setServiceLevel(demande.getTypeRequestLibelle());
        transport.setMawb(demande.getMawb());
        transport.setNumAwbBol(demande.getNumAwbBol());
        transport.setIncoterm(demande.getxEcIncotermLibelle());
        transport.setFlightVessel(demande.getFlightVessel());

        if (demande.getXecCurrencyInvoice()
                != null) transport.setInvoicingCurrencyCode(demande.getXecCurrencyInvoice().getCode());

        transport.setInsurance(demande.getInsurance());

        if (demande.getXecCurrencyInsurance()
                != null) transport.setInsuranceCurrencyCode(demande.getXecCurrencyInsurance().getCode());
        transport.setInsuranceValue(demande.getInsuranceValue());

        transport.setEligibleForConsolidation(demande.getEligibleForConsolidation());
        transport.setFinalChoiceToBeCalculated(demande.getFinalChoiceToBeCalculated());

        transport.setTransportMode(demande.getModeTransporteur());
        transport.setTransportReference(demande.getRefTransport());
        transport.setTransportStatus(StatutDemande.getLibelleJasonByCode(demande.getxEcStatut()));
        transport.setConsolidationStatus(ConsolidationGroupage.getCodeAlphaByCode(demande.getxEcStatutGroupage()));
        transport.setCustomsOffice(demande.getAboutTrCustomOffice());

        if (demande.getUser() != null) {
            transport.setShipperUserEmail(demande.getUser().getEmail());
            transport.setShipperUserName(demande.getUser().getPrenomNom());
            transport.setShipperEtablishment(demande.getUser().getEbEtablissement().getNom());
        }

        if (demande.getxEbTypeFluxNum() != null) {
            EbTypeFlux ebTypeFlux = ebTypeFluxRepository.findById(demande.getxEbTypeFluxNum()).get();
            transport.setFlowTypeCode(ebTypeFlux.getCode());
        }

        if (demande.getxEbTypeTransport() != null) {
            EbTypeTransport typeTransport = ebTypeTransportRepository.findById(demande.getxEbTypeTransport()).get();

            if (typeTransport != null) {
                transport.setTransportType(typeTransport.getLibelle());
            }

        }

        if (demande.getListCustomsFields() != null && !demande.getListCustomsFields().isEmpty()) {
            transport.setCustomFieldList(new ArrayList<CustomFieldWSO>());
            CustomFieldWSO customField = null;

            for (CustomFields cf : demande.getListCustomsFields()) {

                if (cf.getValue() != null && !cf.getValue().isEmpty()) {
                    customField = new CustomFieldWSO();
                    customField.setItemId(cf.getNum());
                    customField.setCode(cf.getName());
                    customField.setLabel(cf.getLabel());
                    customField.setValue(cf.getValue());
                    transport.getCustomFieldList().add(customField);
                }

            }

        }

        if (demande.getListCategories() != null && !demande.getListCategories().isEmpty()) {
            demande.setListCategories(demande.getListCategories());
            transport.setCategoryList(new ArrayList<CategoryWSO>());
            CategoryWSO category = null;

            for (EbCategorie c : demande.getListCategories()) {
                EbLabel ebLabel = c.getLabels() != null && !c.getLabels().isEmpty() ?
                        c.getLabels().stream().findAny().get() :
                        null;

                String label = null;
                if (ebLabel != null) label = ebLabel.getLibelle();

                category = new CategoryWSO();
                category.setItemId(c.getEbCategorieNum());
                category.setCategory(c.getLibelle());
                category.setCategoryLabel(label);
                transport.getCategoryList().add(category);
            }

        }

        if (demande.getListMarchandises() != null && !demande.getListMarchandises().isEmpty()) {
            List<UnitWSO> listUnit = convertListMarchandisesToListUnits(demande.getListMarchandises());
            transport.setUnitList(listUnit);
        }

        // ----- set party
        if (demande.getEbPartyOrigin() != null) {
            transport.setOriginInformation(convertEbPartyToAddresseWSO(demande.getEbPartyOrigin()));
        }

        if (demande.getEbPartyDest() != null) {
            transport.setDestinationInformation(convertEbPartyToAddresseWSO(demande.getEbPartyDest()));
        }

        if (demande.getEbPartyNotif() != null) {
            transport.setNotifyInformation(convertEbPartyToAddresseWSO(demande.getEbPartyNotif()));
        }

        if (demande.getxEbUserDestCustomsBroker() != null) {
            transport.setDestCustomUserEmail(demande.getxEbUserDestCustomsBroker().getEmail());
        }

        if (demande.getxEbUserDest() != null) {
            transport.setDestUserEmail(demande.getxEbUserDest().getEmail());
        }

        if (demande.getxEbUserObserver() != null && !demande.getxEbUserObserver().isEmpty()) {
            List<String> observerUserEmailList = demande
                    .getxEbUserObserver().stream().map(us -> us.getEmail()).collect(Collectors.toList());
            transport.setObserverUserEmailList(observerUserEmailList);
        }

        if (demande.getxEbUserOriginCustomsBroker() != null) {
            transport.setOriginCustomUserEmail(demande.getxEbUserOriginCustomsBroker().getEmail());
        }

        if (demande.getxEbUserOrigin() != null) {
            transport.setOriginUserEmail(demande.getxEbUserOrigin().getEmail());
        }

        if (demande.getCodeConfigurationEDI() != null) {
            transport.setCodeConfigurationEDI(demande.getCodeConfigurationEDI());
        }

        if (demande.getTotalTaxableWeight() != null) {
            transport.setTotalTaxableWeight(demande.getTotalTaxableWeight());
        }

        if (demande.getxEbSchemaPsl() != null) {
            transport.setTrackingPointSchema(demande.getxEbSchemaPsl().getDesignation());

            if (demande.getxEbSchemaPsl().getListPsl() != null && !demande.getxEbSchemaPsl().getListPsl().isEmpty()) {
                List<String> trackingPointCodeList = (demande.getxEbSchemaPsl().getListPsl() != null &&
                        demande.getxEbSchemaPsl().getListPsl().isEmpty()) ?
                        demande
                                .getxEbSchemaPsl().getListPsl().stream().filter(psl -> psl.getSchemaActif())
                                .map(it -> it.getCodeAlpha()).collect(Collectors.toList()) :
                        null;
                transport.setTrackingPointCodeList(trackingPointCodeList);
            }

        }

        if (demande.getExEbDemandeTransporteurs() != null && !demande.getExEbDemandeTransporteurs().isEmpty()) {
            transport.setQuotationList(new ArrayList<>());

            for (ExEbDemandeTransporteur dt : demande.getExEbDemandeTransporteurs()) {
                transport.getQuotationList().add(convertEbDemandeQuoteToQuotation(dt));
            }

        }

        if (demande.getListAdditionalCost() != null && !demande.getListAdditionalCost().isEmpty()) {
            List<CostWSO> listCostField = new ArrayList<>();

            demande.getListAdditionalCost().forEach(ebCost -> {
                CostWSO costField = new CostWSO(ebCost.getLibelle(), ebCost.getPrice(), ebCost.getPriceEuro());

                if (ebCost.getxEcCurrencyNum() != null) {
                    EcCurrency ecCurrency = ecCurrencyRepository.findById(ebCost.getxEcCurrencyNum()).get();
					costField.setCurrencyCode(ecCurrency.getCode());
                }

                listCostField.add(costField);
            });
            transport.setAdditionalCostList(listCostField);
        }

        return transport;
    }

    private TransportLightWSO convertEbDemandeToTransportLightWSO(EbDemande ebDemande) {
        TransportLightWSO transportLightWSO = new TransportLightWSO();
        transportLightWSO.setTotalTaxableWeight(ebDemande.getTotalTaxableWeight());
        transportLightWSO.setCustomerReference(ebDemande.getCustomerReference());
        transportLightWSO.setTransportReference(ebDemande.getRefTransport());
        transportLightWSO.setServiceLevel(ebDemande.getTypeRequestLibelle());

        if (ebDemande.getListCustomsFields() != null && !ebDemande.getListCustomsFields().isEmpty()) {
            transportLightWSO.setCustomFieldList(new ArrayList<CustomFieldWSO>());
            CustomFieldWSO customField = null;

            for (CustomFields cf : ebDemande.getListCustomsFields()) {

                if (cf.getValue() != null && !cf.getValue().isEmpty()) {
                    customField = new CustomFieldWSO();
                    customField.setItemId(cf.getNum());
                    customField.setLabel(cf.getLabel());
                    customField.setValue(cf.getValue());
                    transportLightWSO.getCustomFieldList().add(customField);
                }

            }

        }

        if (ebDemande.getListCategories() != null && !ebDemande.getListCategories().isEmpty()) {
            ebDemande.setListCategories(ebDemande.getListCategories());
            transportLightWSO.setCategoryList(new ArrayList<CategoryWSO>());
            CategoryWSO category = null;

            for (EbCategorie c : ebDemande.getListCategories()) {
                EbLabel ebLabel = c.getLabels() != null && !c.getLabels().isEmpty() ?
                        c.getLabels().stream().findAny().get() :
                        null;

                String label = null;
                if (ebLabel != null) label = ebLabel.getLibelle();

                category = new CategoryWSO();
                category.setItemId(c.getEbCategorieNum());
                category.setCategory(c.getLibelle());
                category.setCategoryLabel(label);
                transportLightWSO.getCategoryList().add(category);
            }

        }

        if (ebDemande.getExEbDemandeTransporteurs() != null && !ebDemande.getExEbDemandeTransporteurs().isEmpty()) {
            transportLightWSO.setQuotationList(new ArrayList<>());

            for (ExEbDemandeTransporteur dt : ebDemande.getExEbDemandeTransporteurs()) {
                transportLightWSO.getQuotationList().add(convertEbDemandeQuoteToQuotation(dt));
            }

        }

        transportLightWSO.setInsuranceValue(ebDemande.getInsuranceValue());

        if (ebDemande.getListAdditionalCost() != null && !ebDemande.getListAdditionalCost().isEmpty()) {
            transportLightWSO.setAdditionalCostList(new ArrayList<CostWSO>());
            CostWSO costField = null;

            for (EbCost cf : ebDemande.getListAdditionalCost()) {
                costField = new CostWSO(cf.getLibelle(), cf.getPrice(), cf.getPriceEuro());

                if (cf.getxEcCurrencyNum() != null) {
                    EcCurrency ecCurrency = ecCurrencyRepository.findById(cf.getxEcCurrencyNum()).get();
					costField.setCurrencyCode(ecCurrency.getCode());
                }

                transportLightWSO.getAdditionalCostList().add(costField);
            }

        }

        if (ebDemande.getListMarchandises() != null && !ebDemande.getListMarchandises().isEmpty()) {
            List<UnitWSO> listUnit = convertListMarchandisesToListUnits(ebDemande.getListMarchandises());
            transportLightWSO.setUnitList(listUnit);
        }

        return transportLightWSO;
    }

    private List<UnitWSO> convertListMarchandisesToListUnits(List<EbMarchandise> listmarchandise) {
        List<UnitWSO> listUnit = new ArrayList<>();
        UnitWSO unit = null;

        for (EbMarchandise mar : listmarchandise) {
            unit = new UnitWSO();

            unit.setItemId(mar.getEbMarchandiseNum());
            unit.setWeight(mar.getWeight());
            unit.setLength(mar.getLength());
            unit.setWidth(mar.getWidth());
            unit.setHeigth(mar.getHeigth());
            unit.setVolume(mar.getVolume());
            unit.setNumberOfUnits(mar.getNumberOfUnits());

            MarchandiseDangerousGood dg = MarchandiseDangerousGood.getByCode(mar.getDangerousGood());
            if (dg != null) unit.setDangerousGood(dg.getLibelle());

            if (mar.getxEbTypeUnit() != null) {
                EbTypeUnit ebTypeUnit = ebTypeUnitRepository.findById(mar.getxEbTypeUnit()).get();

                if (ebTypeUnit != null) {
                    unit.setUnitType(ebTypeUnit.getCode());
                }

            }

            unit.setStackable(mar.getStackable());
            unit.setUnitReference(mar.getUnitReference());
            unit.setSscc(mar.getSscc());
            unit.setTrackingNumber(mar.getTracking_number());
            unit.setItemName(mar.getNomArticle());
            unit.setContainerType(mar.getTypeContainer());
            unit.setUn(mar.getUn());
            unit.setClassGood(mar.getClassGood());
            unit.setPackaging(mar.getPackaging());
            unit.setComment(mar.getComment());

            if (mar.getxEcCountryOrigin() != null) {
                EcCountry ecCountry = ecCountryRepository.findById(mar.getxEcCountryOrigin()).get();
                unit.setOriginCountry(ecCountry.getLibelle());
                unit.setManifactoringCountryCode(ecCountry.getCode());
            }

            listUnit.add(unit);
        }

        return listUnit;
    }

    public AddressWSO convertEbPartyToAddresseWSO(EbParty partyOrig) {
        AddressWSO party = new AddressWSO();

        party.setItemId(partyOrig.getEbPartyNum());
        party.setReference(partyOrig.getReference());
        party.setCity(partyOrig.getCity());

        if (partyOrig.getxEcCountry() != null) {
            party.setCountryCode(partyOrig.getxEcCountry().getCode());
        }

        party.setCompany(partyOrig.getCompany());
        party.setAddress(partyOrig.getAdresse());
        party.setZipCode(partyOrig.getZipCode());
        party.setZoneReference(partyOrig.getZoneRef());
        party.setOpeningHours(partyOrig.getOpeningHours());
        party.setAirport(partyOrig.getAirport());
        party.setEmail(partyOrig.getEmail());
        party.setPhone(partyOrig.getPhone());
        party.setComment(partyOrig.getCommentaire());
        return party;
    }

    public EbParty fillEbPartyFromAddressWSO(EbParty entity, AddressWSO wso) {
        entity.setReference(wso.getReference());
        entity.setCity(wso.getCity());

        if (wso.getCountryCode() != null) {
            EcCountry country = ecCountryRepository.findFirstByCode(wso.getCountryCode());
            if (country
                    == null)
                throw new MyTowerException(String.format("Country %s doesn't exist", wso.getCountryCode()));
            entity.setxEcCountry(country);
        }

        entity.setCompany(wso.getCompany());
        entity.setAdresse(wso.getAddress());
        entity.setZipCode(wso.getZipCode());
        entity.setZoneRef(wso.getZoneReference());
        entity.setOpeningHours(wso.getOpeningHours());
        entity.setAirport(wso.getAirport());
        entity.setEmail(wso.getEmail());
        entity.setPhone(wso.getPhone());
        entity.setCommentaire(wso.getComment());
        return entity;
    }

    private QuotationWSO convertEbDemandeQuoteToQuotation(ExEbDemandeTransporteur ebDemandeTransporteur) {
        QuotationWSO quotation = new QuotationWSO();
        quotation.setItemId(ebDemandeTransporteur.getExEbDemandeTransporteurNum());
        quotation.setCarrierUserEmail(ebDemandeTransporteur.getxTransporteur().getEmail());
        quotation.setTransitTime(ebDemandeTransporteur.getTransitTime());
        quotation.setPrice(ebDemandeTransporteur.getPrice());
        quotation.setComment(ebDemandeTransporteur.getComment());
        quotation.setStatus(StatutCarrier.getLibelleByCode(ebDemandeTransporteur.getStatus()));
        quotation.setModeTransport(ModeTransport.getLibelleByCode(ebDemandeTransporteur.getxEcModeTransport()));

        if (ebDemandeTransporteur.getListPlCostItem() != null &&
                !ebDemandeTransporteur.getListPlCostItem().isEmpty() &&
                ebDemandeTransporteur.getIsFromTransPlan() != null && ebDemandeTransporteur.getIsFromTransPlan() == true) {
            quotation.setCostList(new ArrayList<CostWSO>());
            CostWSO costWSO = null;

            for (EbPlCostItem costItem : ebDemandeTransporteur.getListPlCostItem()) {
                costWSO = new CostWSO();
                costWSO.setLibelle(costItem.getLibelle());
                costWSO.setPrice(costItem.getPrice());
				if (costItem.getCurrency() != null && costItem.getCurrency().getCode() != null)
					costWSO.setCurrencyCode(costItem.getCurrency().getCode());
                quotation.getCostList().add(costWSO);
            }

        } else if (ebDemandeTransporteur.getListCostCategorie() != null &&
                !ebDemandeTransporteur.getListCostCategorie().isEmpty()) {
            quotation.setCostList(new ArrayList<CostWSO>());
            CostWSO costWSO = null;
            ObjectMapper mapper = new ObjectMapper();

            List<EbCostCategorie> listCostCategorie = mapper
                    .convertValue(
                            ebDemandeTransporteur.getListCostCategorie(),
                            mapper.getTypeFactory().constructCollectionType(List.class, EbCostCategorie.class));

            for (EbCostCategorie ebCostCategorie : listCostCategorie) {

                for (EbCost costItem : ebCostCategorie.getListEbCost()) {
                    costWSO = new CostWSO(costItem.getLibelle(), costItem.getPrice(), costItem.getPrice());
                    costWSO.setType(ebCostCategorie.getLibelle());

                    if (costItem.getxEcCurrencyNum() != null) {
                        EcCurrency ecCurrency = ecCurrencyRepository.findById(costItem.getxEcCurrencyNum()).get();
						costWSO.setCurrencyCode(ecCurrency.getCode());
                    }

                    quotation.getCostList().add(costWSO);
                }

            }

        }

        // quotation.setCarrierCurrencyCode(ebDemandeTransporteur.);
        // quotation.setNegotiatedDateList(ebDemandeTransporteur.);
        // quotation.setCostItemList(ebDemandeTransporteur.getListPlCostItem());
        return quotation;
    }

    private List<EbMarchandise> setMarchandisesList(Integer ebDemandeNum) {
        return ebMarchandiseRepository.findAllByEbDemande_ebDemandeNum(ebDemandeNum);
    }

    private List<ExEbDemandeTransporteur> getListTransporteursByDemandeNum(Integer ebDemandeNum) {
        return exEbDemandeTransporteurRepository.findAllByXEbDemande_ebDemandeNum(ebDemandeNum);
    }

    public PatchTransportWSO processTransportRequestConfirmation(PatchTransportWSO patchTransportWSO)
    {
        EbDemande transportRequest = processGivenData(patchTransportWSO);
        if(transportRequest.getxEcStatut() >= StatutDemande.FIN.getCode()){
            throw new MyTowerException(String.format("Tr with customerReference = %s is already confirmed", patchTransportWSO.getCustomerReference()));
        }
        setNumFactureInDemande(patchTransportWSO, transportRequest, false);

        final EbDemande demandeTracking;
        if(transportRequest.getxEbDemandeGroup() != null){
            demandeTracking = handleConsolidatedTrConfirmation(transportRequest, patchTransportWSO);
        }
        else {
            demandeTracking = handleNormalTrConfirmation(transportRequest, patchTransportWSO);
        }

        ExecutorService service = Executors.newCachedThreadPool();
        service.submit(() -> {
            if(demandeTracking != null){
                handleTrackingGeneration(demandeTracking);
            }
            saveHistorique(patchTransportWSO, transportRequest);
        });

        return patchTransportWSO;
    }

    @Transactional
    private EbDemande handleNormalTrConfirmation(EbDemande transportRequest, PatchTransportWSO patchTransportWSO) {
        transportRequest.setxEcStatut(StatutDemande.WPU.getCode());
        transportRequest.setUpdateDate(new Date());
        handleQuoteToBeConfirmed(transportRequest, patchTransportWSO);
        ebDemandeRepository
                .updateStatusEbDemandeAndCancelledAndCustomFields(
                        transportRequest.getEbDemandeNum(),
                        transportRequest.getxEcStatut(),
                        transportRequest.getxEcCancelled(),
                        transportRequest.getCustomFields(),
                        transportRequest.getUpdateDate()
                );

        return transportRequest;
    }

    private void handleTrackingGeneration(EbDemande transportRequest) {
        ebTrackService.insertEbTrackTrace(transportRequest);
        kafkaObjectService
                .buildAndSendTransportRequestToKafka(
                        transportRequest.getEbDemandeNum(),
                        transportRequest.getUser(),
                        false
                );
    }

    @Transactional
    private EbDemande handleConsolidatedTrConfirmation(EbDemande transportRequest, PatchTransportWSO patchTransportWSO)
    {
        EbDemande trGrouped = ebDemandeRepository
                .findOneByEbDemandeNum(transportRequest.getxEbDemandeGroup());
        if(trGrouped == null){
            throw new MyTowerException("Tr grouped not found");
        }
        if(trGrouped.getxEcStatut() >= StatutDemande.FIN.getCode()){
            throw new MyTowerException("tr grouped is already confirmed");
        }

        ebDemandeRepository.updateCustomFields(transportRequest.getEbDemandeNum(),
                transportRequest.getCustomFields(),
                transportRequest.getListCustomFieldsFlat(),
                transportRequest.getListCustomFieldsValueFlat(),
                new Date());

        setNumFactureInDemande(patchTransportWSO, trGrouped, true);
        ebDemandeRepository.updateCustomFields(trGrouped.getEbDemandeNum(),
                trGrouped.getCustomFields(),
                trGrouped.getListCustomFieldsFlat(),
                trGrouped.getListCustomFieldsValueFlat(),
                new Date());

        EbDemande trTracking = null;

        if (isAllInitialTrsUpdated(trGrouped, patchTransportWSO))
        {
            // Update tr grouped status, status group
            ebDemandeRepository
                    .updateStatusEbDemandeAndGroupageStatut(
                            trGrouped.getEbDemandeNum(),
                            StatutDemande.WPU.getCode(),
                            ConsolidationGroupage.PCO.getCode(),
                            trGrouped.getCustomFields()
                    );
            handleQuoteToBeConfirmed(trGrouped, patchTransportWSO);

            // cancel intial trs
            ebDemandeRepository.setCancelStatusByTrIds(
                    ebDemandeRepository
                            .selectDemandeNumInGroup(trGrouped.getEbDemandeNum()),
                    Enumeration.CancelStatus.CANECLLED.getCode()
            );
            trTracking = trGrouped;
        }

        return trTracking;
    }

    private boolean isAllInitialTrsUpdated(EbDemande trGrouped, PatchTransportWSO patchTransportWSO) {
        if (CollectionUtils.isNotEmpty(patchTransportWSO.getCustomFieldList())) {
            // List ids tes trs dans une proposition
            List<EbDemande> listTrsInGroup = ebDemandeRepository
                    .selectListDemandeInGroup(trGrouped.getEbDemandeNum());
            boolean bool = true;
            for (EbDemande tr : listTrsInGroup) {
                bool = bool && isConcernedCustomFieldBeingUpdated(patchTransportWSO, tr);
            }
            return bool;
        }

        return false;
    }

    private boolean isConcernedCustomFieldBeingUpdated(PatchTransportWSO patchTransportWSO, EbDemande tr) {
        List<CustomFields> listCustomField = null;
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            listCustomField = objectMapper.readValue(tr.getCustomFields(), new TypeReference<ArrayList<CustomFields>>() {
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (CollectionUtils.isNotEmpty(listCustomField)) {
            for (CustomFieldWSO customFieldWSO : patchTransportWSO.getCustomFieldList()) {
                if (hasConcernedCustomField(listCustomField, customFieldWSO)) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean hasConcernedCustomField(List<CustomFields> listCustomField, CustomFieldWSO customFieldWSO) {
        return listCustomField
                .stream().anyMatch(it -> it.getLabel().contentEquals(customFieldWSO.getLabel()) && it.getValue() != null);
    }

    private void handleQuoteToBeConfirmed(EbDemande transportRequest, PatchTransportWSO patchTransportWSO)
    {
        ExEbDemandeTransporteur demandeTransporteur = resolveDemandeTransporteur(transportRequest, patchTransportWSO.getQuoteId());
        if(demandeTransporteur == null){
            throw new IllegalArgumentException(String.format("Can't find tr carrier with quote id = %s", patchTransportWSO.getQuoteId()));
        }
        if(BooleanUtils.isFalse(demandeTransporteur.getIsFromTransPlan())) {
            demandeTransporteur.setStatus(StatutCarrier.FIN.getCode());
        }
        else{
            demandeTransporteur.setStatus(StatutCarrier.FIN_PLAN.getCode());
        }

        final BigDecimal finalTransportPrice = demandeTransporteur.getPrice();
        final Integer transporteurEtab = demandeTransporteur.getxEbEtablissement().getEbEtablissementNum();
        // Update etablissement num and final price and update date
        ebDemandeRepository
                .updateTransporteurEtabPriceAndDate(
                        transportRequest.getEbDemandeNum(),
                        transporteurEtab,
                        finalTransportPrice,
                        new Date());
        exEbDemandeTransporteurRepository.save(demandeTransporteur);
    }

		private EbDemande processGivenData(PatchTransportWSO patchTransportWSO)
		{
			EbDemande demande = null;
			
			if (
				patchTransportWSO.getItemId() == null &&
					patchTransportWSO.getTransportReference() == null &&
					patchTransportWSO.getCustomerReference() == null
			)
			{
				throw new MyTowerException("Reference of pricing request not provided.");
			}

			if (patchTransportWSO.getItemId() != null)
			{
				demande = ebDemandeRepository.findById(patchTransportWSO.getItemId()).get();
				if (demande == null)
					throw new MyTowerException(
						"Item '" + patchTransportWSO.getItemId() + "' of pricing request  not found"
					);
			}
			else if (patchTransportWSO.getTransportReference() != null)
			{
				demande = ebDemandeRepository
					.getByRefTransport(patchTransportWSO.getTransportReference());

				if (demande == null)
					throw new MyTowerException(
						"Reference '" +
							patchTransportWSO.getTransportReference() + "' of pricing request  not found"
					);
			}
			else if (patchTransportWSO.getCustomerReference() != null)
			{
				demande = ebDemandeRepository.getByCustomerRef(patchTransportWSO.getCustomerReference());

				if (demande == null)
					throw new MyTowerException(
						"Customer reference '" +
							patchTransportWSO.getTransportReference() + "' of pricing request  not found"
					);
			}
			else if (patchTransportWSO.getFlightVessel() != null)
			{
				demande = ebDemandeRepository.getByFlightVessel(patchTransportWSO.getFlightVessel());

				if (demande == null)
					throw new MyTowerException(
						"Flight Vessel '" +
							patchTransportWSO.getFlightVessel() + "' of pricing request  not found"
					);
			}
			else if (patchTransportWSO.getFlightVessel() != null)
			{
				demande = ebDemandeRepository.getByMawb(patchTransportWSO.getMawbNumber());

				if (demande == null)
					throw new MyTowerException(
						"Master Air Way Bill Number '" +
							patchTransportWSO.getMawbNumber() + "' of pricing request  not found"
					);
			}
			else if (patchTransportWSO.getAwbBolNumber() != null)
			{
				demande = ebDemandeRepository.getByNumAwbBol(patchTransportWSO.getAwbBolNumber());

				if (demande == null)
					throw new MyTowerException(
						"Air Way Bill Number '" +
							patchTransportWSO.getAwbBolNumber() + "' of pricing request  not found"
					);
			}
			
			return demande;

		}

    private ExEbDemandeTransporteur resolveDemandeTransporteur(EbDemande ebDemande, Integer quoteId) {
        ExEbDemandeTransporteur demandeTransporteur = null;
        if(CollectionUtils.isEmpty(ebDemande.getExEbDemandeTransporteurs())) {
            throw new IllegalArgumentException("transporteurs list in Tr is empty");
        }
        if(quoteId != null){
            Optional<ExEbDemandeTransporteur> quotation = ebDemande.getExEbDemandeTransporteurs().stream()
                    .filter(quote -> quote.getExEbDemandeTransporteurNum().equals(quoteId)).findFirst();
            if(quotation.isPresent()){
                demandeTransporteur = quotation.get();
            }
        }
        if(demandeTransporteur == null){
            demandeTransporteur = ebDemande.getExEbDemandeTransporteurs().get(0);
        }
        return  demandeTransporteur;
    }


    private void saveHistorique(PatchTransportWSO patchTransportWSO, EbDemande ebdemande) {
        String action = "";

        for (CustomFieldWSO field : patchTransportWSO.getCustomFieldList()) {

            if (field.getValue() != null) {
                action += field.getLabel() + " de " + ebdemande.getRefTransport() + " est mis à jour ";
            }

        }

        listStatiqueService
                .historizeAction(
                        ebdemande.getEbDemandeNum(),
                        Enumeration.IDChatComponent.PRICING.getCode(),
                        Enumeration.Module.PRICING.getCode(),
                        action,
                        null,
                        null);
    }

    private String
    setNumFactureInDemande(PatchTransportWSO patchTransportWSO, EbDemande ebDemande, Boolean isTrGroupped) {

        if (patchTransportWSO.getCustomFieldList() != null && !patchTransportWSO.getCustomFieldList().isEmpty()) {
            String customFieldValue = null;
            Gson gson = new Gson();
            List<CustomFields> listCustomField = gson
                    .fromJson(ebDemande.getCustomFields(), new TypeToken<ArrayList<CustomFields>>() {
                    }.getType());
            EbCustomField customFieldComp = ebCustomFieldRepository
                    .findFirstByXEbCompagnie(ebDemande.getxEbCompagnie().getEbCompagnieNum());
            List<CustomFields> listCustomFieldComp = gson
                    .fromJson(customFieldComp.getFields(), new TypeToken<ArrayList<CustomFields>>() {
                    }.getType());

            for (CustomFieldWSO trCf : patchTransportWSO.getCustomFieldList()) {
                boolean found = false;

                if (listCustomField != null && !listCustomField.isEmpty()) {
                    CustomFields cf = listCustomField
                            .stream().filter(it -> it.getLabel().contentEquals(trCf.getLabel())).findFirst().orElse(null);

                    if (cf != null) {
                        found = true;
                        String value = null;
                        if (isTrGroupped && cf.getValue() != null) value = cf.getValue() + ", " + trCf.getValue();
                        else value = trCf.getValue();
                        cf.setValue(value);
                        customFieldValue = value;
                        String customField = new Gson().toJson(listCustomField);
                        ebDemande.setCustomFields(customField);
                    }

                }

                if (!found && listCustomFieldComp != null && !listCustomFieldComp.isEmpty()) {
                    CustomFields cfCmp = listCustomFieldComp
                            .stream().filter(it -> it.getLabel().contentEquals(trCf.getLabel())).findFirst().orElse(null);

                    if (cfCmp == null) {
                        throw new MyTowerException("No corresponding custom field found for " + trCf.getLabel());
                    } else {
                        String value = null;
                        if (isTrGroupped && cfCmp.getValue() != null) value = cfCmp.getValue() + ", " + trCf.getValue();
                        else value = trCf.getValue();
                        cfCmp.setValue(value);
                        customFieldValue = value;
                        if (listCustomField == null) listCustomField = new ArrayList<CustomFields>();
                        listCustomField.add(cfCmp);
                        String customField = new Gson().toJson(listCustomFieldComp);
                        ebDemande.setCustomFields(customField);
                    }

                }

            }

            return customFieldValue;
        }

        return null;
    }

}
