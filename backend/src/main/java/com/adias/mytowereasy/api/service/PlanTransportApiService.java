package com.adias.mytowereasy.api.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.adias.mytowereasy.airbus.repository.EbQrGroupePropositionRepository;
import com.adias.mytowereasy.api.util.MyTowerErrorMsg;
import com.adias.mytowereasy.api.util.WSConstants;
import com.adias.mytowereasy.api.wso.AddressWSO;
import com.adias.mytowereasy.api.wso.CategoryWSO;
import com.adias.mytowereasy.api.wso.CostWSO;
import com.adias.mytowereasy.api.wso.CustomFieldWSO;
import com.adias.mytowereasy.api.wso.PlanTransportWSO;
import com.adias.mytowereasy.api.wso.QuotationWSO;
import com.adias.mytowereasy.api.wso.SearchCriteriaPlanTranportWSO;
import com.adias.mytowereasy.api.wso.TransportLightWSO;
import com.adias.mytowereasy.api.wso.TransportWSO;
import com.adias.mytowereasy.api.wso.UnitWSO;
import com.adias.mytowereasy.chanel.model.EbWeekTemplateDay;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbCost;
import com.adias.mytowereasy.model.EbCostCategorie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.model.EbPlanTransportNew;
import com.adias.mytowereasy.model.EbTypeFlux;
import com.adias.mytowereasy.model.EbTypeTransport;
import com.adias.mytowereasy.model.EbTypeUnit;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCountry;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.JourEnum;
import com.adias.mytowereasy.model.Enumeration.MarchandiseDangerousGood;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.Enumeration.NatureDemandeTransport;
import com.adias.mytowereasy.model.Enumeration.StatutCarrier;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;
import com.adias.mytowereasy.model.enums.ConsolidationGroupage;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EcCountryRepository;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.PlanTransportNewService;
import com.adias.mytowereasy.service.PricingService;
import com.adias.mytowereasy.service.QrConsolidationServiceImpl;
import com.adias.mytowereasy.trpl.model.EbPlCostItem;
import com.adias.mytowereasy.util.ImportEnMasseUtils;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Service
public class PlanTransportApiService extends MyTowerService {
    @Autowired
    PricingService pricingService;
    @Autowired
    EbDemandeRepository ebDemandeRepository;

    @Autowired
    PlanTransportNewService planTransportNewService;

    @Autowired
    EbQrGroupePropositionRepository ebQrGroupePropositionRepository;

    @Autowired
    EcCountryRepository ecCountryRepository;

    QrConsolidationServiceImpl qrConsolidationService = new QrConsolidationServiceImpl();

    @SuppressWarnings("rawtypes")
    public Page<Resource<PlanTransportWSO>> getPlanTransportRequest(SearchCriteriaPlanTranportWSO criteria) {

        try {

            if (criteria.getPage() == null) {
                criteria.setPage(WSConstants.DEFAULT_PAGE_NUMBER);
            }

            if (criteria.getSize() == null) {
                criteria.setSize(WSConstants.DEFAULT_SIZE);
            }

            List<EbDemande> listDemande = new ArrayList<>();
            SearchCriteriaPricingBooking searchCriteriaPricingBooking = getSearchCriteria(criteria);
            listDemande = pricingService.getListEbDemande(searchCriteriaPricingBooking);
            int counter = pricingService.getCountListEbDemande(searchCriteriaPricingBooking);

            List<Resource<PlanTransportWSO>> listResourceTransport = new ArrayList<>();

            for (EbDemande demande: listDemande) {
                String listEbDemandeNumGrouped = null;
                demande.setListMarchandises(setMarchandisesList(demande.getEbDemandeNum()));
                demande.setExEbDemandeTransporteurs(setExEbDemandeTransporteurList(demande.getEbDemandeNum()));

                if (demande.getxEcStatut().equals(StatutDemande.PND.getCode())
                    || demande.getxEcNature().equals(NatureDemandeTransport.CONSOLIDATION.getCode())) {
                    listEbDemandeNumGrouped = ebQrGroupePropositionRepository
                        .findEbQrGroupePropositionByEbDemandeNum(demande.getEbDemandeNum());
                }

                PlanTransportWSO planTransport = convertEbDemandeToTransport(demande);

                if (listEbDemandeNumGrouped != null) {
                    List<Integer> listEbDemandeGroupedNum = Arrays
                        .asList(StringUtils.split(listEbDemandeNumGrouped, ":")).stream().map(s -> Integer.parseInt(s))
                        .collect(Collectors.toList());
                    searchCriteriaPricingBooking.setListEbDemandeNum(listEbDemandeGroupedNum);
                    SearchCriteriaPricingBooking searchCriteriaPricingBooking1 = new SearchCriteriaPricingBooking();
                    searchCriteriaPricingBooking1.setSearchByWS(true);
                    searchCriteriaPricingBooking1.setModule(searchCriteriaPricingBooking.getModule());
                    searchCriteriaPricingBooking1
                        .setConnectedUserNum(searchCriteriaPricingBooking.getConnectedUserNum());
                    searchCriteriaPricingBooking1.setListEbDemandeNum(listEbDemandeGroupedNum);
                    List<EbDemande> listDemandeInitaux = pricingService.getListEbDemande(searchCriteriaPricingBooking1);

                    List<TransportLightWSO> transportLightWSOList = new ArrayList<>();

                    if (!listDemandeInitaux.isEmpty()) {
                        TransportLightWSO transportLightWSO;

                        for (EbDemande ebDemande: listDemandeInitaux) {
                            ebDemande.setListMarchandises(setMarchandisesList(ebDemande.getEbDemandeNum()));
                            ebDemande.setDg(false);

                            for (int i = 0; i < ebDemande.getListMarchandises().size(); i++) {
                                Integer marchandiseDg = ebDemande.getListMarchandises().get(i).getDangerousGood();

                                if (marchandiseDg != null
                                    && !MarchandiseDangerousGood.NO.getCode().equals(marchandiseDg)) {
                                    ebDemande.setDg(true);
                                    break;
                                }

                            }

                            ebDemande
                                .setExEbDemandeTransporteurs(
                                    setExEbDemandeTransporteurList(ebDemande.getEbDemandeNum()));

                            transportLightWSO = convertEbDemandeToTransportLightWSO(ebDemande);
                            transportLightWSOList.add(transportLightWSO);
                        }

                    }

                    planTransport.setDangerousGoods(false);
                    List<Double> classGoods = new ArrayList<>();
                    transportLightWSOList.forEach(t -> {
                        if (t.getClassGoods() != null) classGoods.addAll(t.getClassGoods());
                        if (t.getUns() != null) planTransport.setUns(t.getUns());
                    });

                    for (int i = 0; i < transportLightWSOList.size(); i++) {

                        if (transportLightWSOList.get(i).isDangerousGoods()) {
                            planTransport.setDangerousGoods(true);
                            break;
                        }

                    }

                    planTransport.setClassGood(classGoods);
                    planTransport.setInitialTransportList(transportLightWSOList);
                    Resource resource = new Resource(planTransport);
                    listResourceTransport.add(resource);
                }
                else {
                    if (criteria.getClassGoods() != null
                        && !criteria.getClassGoods().isEmpty()) planTransport.setClassGood(criteria.getClassGoods());
                    if (criteria.getDangerousGood() != null && !criteria.getDangerousGood().isEmpty()) planTransport
                        .setDangerousGood(criteria.getDangerousGood());
                    if (criteria.getUns() != null) planTransport.setUns(criteria.getUns());
                    Resource resource = new Resource(planTransport);
                    listResourceTransport.add(resource);
                }

            }

            Page<Resource<PlanTransportWSO>> pages = new PageImpl<>(
                listResourceTransport,
                PageRequest.of(criteria.getPage(), criteria.getSize()),
                counter);

            return pages;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    // convert TransportWSO to PlanTransportWSO.

    public PlanTransportWSO convertTransportWsoToPlanTransportWSO(TransportWSO transportWSO) {
        PlanTransportWSO planTransportWSO = new PlanTransportWSO();
        planTransportWSO.setCustomerReference(transportWSO.getCustomerReference());
        planTransportWSO.setTransportMode(transportWSO.getTransportMode());
        planTransportWSO.setOriginInformation(transportWSO.getOriginInformation());
        planTransportWSO.setDestinationInformation(transportWSO.getDestinationInformation());
        planTransportWSO.setIncoterm(transportWSO.getIncoterm());
        return planTransportWSO;
    }

    public PlanTransportWSO getNextPickUpDateOfTransportWSO(PlanTransportWSO transport) {
        Date currentDate = new Date();
        SearchCriteriaPricingBooking searchCriteriaPricingBooking;
        searchCriteriaPricingBooking = getSearchCriteriaFromTr(transport);
        List<EbPlanTransportNew> listPlanTransportNew = planTransportNewService
            .listLoadPlanIsConstraints(searchCriteriaPricingBooking, transport);
        List<EbPlanTransportNew> listPlanTransportNewValid = new ArrayList<>();

        if (listPlanTransportNew != null && listPlanTransportNew.size() > 0) {

            for (EbPlanTransportNew ptn: listPlanTransportNew) {

                if (ptn.getWeekTemplate() != null && ptn.getWeekTemplate().getDateDebut() != null
                    && ptn.getWeekTemplate().getDateFin() != null) {

                    if (ptn.getWeekTemplate().getDateDebut().getTime() < currentDate.getTime()
                        && currentDate.getTime() < ptn.getWeekTemplate().getDateFin().getTime()) {
                        listPlanTransportNewValid.add(ptn);
                    }

                }

            }

        }

        for (EbPlanTransportNew ptn: listPlanTransportNewValid) {

            if (ptn.getWeekTemplate().getDays() != null) {
                List<EbWeekTemplateDay> days = ptn
                    .getWeekTemplate().getDays().stream().filter(day -> day.isOpen()).collect(Collectors.toList());

                List<Date> datesInRange = new ArrayList<>();
                List<String> openDays = new ArrayList<>();
                Calendar calendar = new GregorianCalendar();
                calendar.setTime(ptn.getWeekTemplate().getDateDebut());

                Calendar endCalendar = new GregorianCalendar();
                endCalendar.setTime(ptn.getWeekTemplate().getDateFin());

                while (calendar.before(endCalendar)) {
                    Date result = calendar.getTime();
                    datesInRange.add(result);
                    calendar.add(Calendar.DATE, 1);
                }

                List<Date> dateTangeFromCurrentDay = datesInRange
                    .stream().filter(date -> date.compareTo(new Date()) > 0).collect(Collectors.toList());

                SimpleDateFormat simpleDayformat = new SimpleDateFormat("EEE", Locale.ENGLISH);

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                dateTangeFromCurrentDay.forEach(date -> {
                    days.forEach(day -> {

                        if (day.getHours() != null && day.getHours().size() > 0 && day.getHours().get(0) != null
                            && day.getHours().get(0).getStartHour() != null
                            && day.getHours().get(0).getStartHour() != "") {
                            JourEnum jour = JourEnum.getJourEnumByCode(day.getCode());
                            Calendar c = Calendar.getInstance();

                            if ((simpleDayformat.format(date) + ".").equalsIgnoreCase(jour.getLibelle())) {

                                try {
                                    c.setTime(simpleDateFormat.parse(simpleDateFormat.format(date)));

                                    c
                                        .add(
                                            Calendar.HOUR,
                                            Integer
                                                .parseInt(
                                                    day
                                                        .getHours().get(0).getStartHour().substring(
                                                            0,
                                                            day.getHours().get(0).getStartHour().indexOf(':'))));

                                    c
                                        .add(
                                            Calendar.MINUTE,
                                            Integer
                                                .parseInt(
                                                    day
                                                        .getHours().get(0).getStartHour().substring(
                                                            day.getHours().get(0).getStartHour().indexOf(':') + 1,
                                                            day.getHours().get(0).getStartHour().length())));

                                    if (day.getHours().get(0).getCutOffTime() != null) {
                                        c
                                            .add(
                                                Calendar.HOUR,
                                                -Integer
                                                    .parseInt(
                                                        day
                                                            .getHours().get(0).getCutOffTime().substring(
                                                                0,
                                                                day.getHours().get(0).getCutOffTime().indexOf(':'))));
                                        c
                                            .add(
                                                Calendar.MINUTE,
                                                -Integer
                                                    .parseInt(
                                                        day
                                                            .getHours().get(0).getCutOffTime().substring(
                                                                day.getHours().get(0).getCutOffTime().indexOf(':') + 1,
                                                                day.getHours().get(0).getCutOffTime().length())));
                                    }

                                    if (sdf.parse(sdf.format(c.getTime())).after(sdf.parse(sdf.format(new Date())))) {
                                        openDays
                                            .add(simpleDayformat.format(c.getTime()) + " " + sdf.format(c.getTime()));
                                    }

                                } catch (ParseException e1) {
                                    // TODO Auto-generated catch block
                                    e1.printStackTrace();
                                }

                            }

                        }

                    });
                });

                if (openDays != null && openDays.size() > 0 && openDays.get(0) != null) {
                    SimpleDateFormat df6 = new SimpleDateFormat("E dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);

                    try {
                        Date date = df6.parse(openDays.get(0));
                        transport.setNextPickUpDate(date);
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }

            }

        }

        return transport;
    }

    /**
     * This method return a SearchCriteriaPricingBooking from TR.
     */
    private SearchCriteriaPricingBooking
        getSearchCriteriaFromTrCriteria(PlanTransportWSO transport, SearchCriteriaPlanTranportWSO criteria) {
        SearchCriteriaPricingBooking searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();

        if (criteria.getListCustomerReference() == null) {

            if (transport.getIncoterm() != null && criteria.getListIncoterm() != null) {
                List<String> listIncoterm = new ArrayList<>();
                listIncoterm.add(transport.getIncoterm());
                searchCriteriaPricingBooking.setListIncoterm(listIncoterm);
            }

            if (transport.getTransportMode() != null && criteria.getListModeTransport() != null) {
                List<Integer> listModeTransport = new ArrayList<>();
                listModeTransport.add(Enumeration.ModeTransport.getCodeByLibelle(transport.getTransportMode()));
                searchCriteriaPricingBooking.setListModeTransport(listModeTransport);
            }

            if (transport.getOriginInformation() != null && transport.getOriginInformation().getCountryCode() != null
                && criteria.getListOrigins() != null) {
                EcCountry country = ecCountryRepository
                    .findFirstByCode(transport.getOriginInformation().getCountryCode());
                List<Integer> listOrigins = new ArrayList<>();
                listOrigins.add(country.getEcCountryNum());
                searchCriteriaPricingBooking.setListOrigins(listOrigins);
            }

            if (transport.getDestinationInformation() != null
                && transport.getDestinationInformation().getCountryCode() != null
                && criteria.getListDestinations() != null)

            {
                EcCountry country = ecCountryRepository
                    .findFirstByCode(transport.getDestinationInformation().getCountryCode());
                List<Integer> listDestinations = new ArrayList<>();
                listDestinations.add(country.getEcCountryNum());
                searchCriteriaPricingBooking.setListDestinations(listDestinations);
            }

            if (transport.getDestinationInformation() != null
                && transport.getDestinationInformation().getCountryCode() != null
                && criteria.getListOriginCity() != null)

            {
                List<String> listOriginCity = new ArrayList<>();
                listOriginCity.add(transport.getOriginInformation().getCity());
                searchCriteriaPricingBooking.setListOriginCity(listOriginCity);
            }

            if (transport.getDestinationInformation() != null
                && transport.getDestinationInformation().getCountryCode() != null
                && criteria.getListDestinationCity() != null)

            {
                List<String> listDestinationCity = new ArrayList<>();
                listDestinationCity.add(transport.getDestinationInformation().getCity());
                searchCriteriaPricingBooking.setListDestinationCity(listDestinationCity);
            }

        }

        else {
            searchCriteriaPricingBooking.setListCustomerReference(criteria.getListCustomerReference());

            if (transport.getIncoterm() != null) {
                List<String> listIncoterm = new ArrayList<>();
                listIncoterm.add(transport.getIncoterm());

                if (criteria.getListIncoterm() != null) {
                    criteria.getListIncoterm().forEach(t -> {

                        if (!t.equals(transport.getIncoterm())) {
                            listIncoterm.add(t);
                        }

                    });
                }

                searchCriteriaPricingBooking.setListIncoterm(listIncoterm);
            }

            if (transport.getTransportMode() != null) {
                List<Integer> listModeTransport = new ArrayList<>();
                listModeTransport.add(Enumeration.ModeTransport.getCodeByLibelle(transport.getTransportMode()));
                if (criteria.getListModeTransport() != null) criteria.getListModeTransport().forEach(t -> {
                    if (Enumeration.ModeTransport
                        .getCodeByLibelle(t)
                        .equals(t)) listModeTransport.add(Enumeration.ModeTransport.getCodeByLibelle(t));
                });
                searchCriteriaPricingBooking.setListModeTransport(listModeTransport);
            }

            if (transport.getOriginInformation() != null && transport.getOriginInformation().getCountryCode() != null) {
                EcCountry country = ecCountryRepository
                    .findFirstByCode(transport.getOriginInformation().getCountryCode());
                List<Integer> listOrigins = new ArrayList<>();
                listOrigins.add(country.getEcCountryNum());

                if (criteria.getListOrigins() != null) {
                    criteria.getListOrigins().forEach(t -> {
                        EcCountry ecCountry = new EcCountry();
                        ecCountry = ecCountryRepository
                            .findFirstByCode(transport.getOriginInformation().getCountryCode());
                        listOrigins.add(ecCountry.getEcCountryNum());
                    });
                }

                searchCriteriaPricingBooking.setListOrigins(listOrigins);
            }

            if (transport.getDestinationInformation() != null
                && transport.getDestinationInformation().getCountryCode() != null)

            {
                EcCountry country = ecCountryRepository
                    .findFirstByCode(transport.getDestinationInformation().getCountryCode());
                List<Integer> listDestinations = new ArrayList<>();
                listDestinations.add(country.getEcCountryNum());

                if (criteria.getListDestinations() != null) {
                    criteria.getListDestinations().forEach(t -> {
                        EcCountry ecCountry = new EcCountry();
                        ecCountry = ecCountryRepository
                            .findFirstByCode(transport.getDestinationInformation().getCountryCode());
                        listDestinations.add(ecCountry.getEcCountryNum());
                    });
                }

                searchCriteriaPricingBooking.setListDestinations(listDestinations);
            }

            if (transport.getOriginInformation() != null && transport.getOriginInformation().getCity() != null)

            {
                List<String> listOriginCity = new ArrayList<>();
                listOriginCity.add(transport.getOriginInformation().getCity());
                if (criteria.getListOriginCity() != null) listOriginCity.addAll(criteria.getListOriginCity());
                searchCriteriaPricingBooking.setListOriginCity(listOriginCity);
            }

            if (transport.getDestinationInformation() != null && transport.getDestinationInformation().getCity() != null
                && criteria.getListDestinationCity() != null)

            {
                List<String> listDestinationCity = new ArrayList<>();
                listDestinationCity.add(transport.getDestinationInformation().getCity());
                if (criteria.getListDestinationCity() != null) listDestinationCity
                    .addAll(criteria.getListDestinationCity());
                searchCriteriaPricingBooking.setListDestinationCity(listDestinationCity);
            }

        }

        if (criteria.getClassGoods() != null) searchCriteriaPricingBooking.setClassGoods(criteria.getClassGoods());
        if (criteria.getDangerousGood() != null) searchCriteriaPricingBooking
            .setDangerousGood(criteria.getDangerousGood());

        if (criteria.getUns() != null) searchCriteriaPricingBooking.setUns(criteria.getUns());

        return searchCriteriaPricingBooking;
    }

    /**
     * This method return a SearchCriteriaPricingBooking from TR.
     */
    private SearchCriteriaPricingBooking getSearchCriteriaFromTr(PlanTransportWSO transport) {
        SearchCriteriaPricingBooking searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();

        if (transport.getIncoterm() != null) {
            List<String> listIncoterm = new ArrayList<>();
            listIncoterm.add(transport.getIncoterm());
            searchCriteriaPricingBooking.setListIncoterm(listIncoterm);
        }

        if (transport.getTransportMode() != null) {
            List<Integer> listModeTransport = new ArrayList<>();
            listModeTransport.add(Enumeration.ModeTransport.getCodeByLibelle(transport.getTransportMode()));
            searchCriteriaPricingBooking.setListModeTransport(listModeTransport);
        }

        if (transport.getOriginInformation() != null && transport.getOriginInformation().getCountryCode() != null) {
            EcCountry country = ecCountryRepository.findFirstByCode(transport.getOriginInformation().getCountryCode());
            List<Integer> listOrigins = new ArrayList<>();
            listOrigins.add(country.getEcCountryNum());
            searchCriteriaPricingBooking.setListOrigins(listOrigins);
        }

        if (transport.getDestinationInformation() != null
            && transport.getDestinationInformation().getCountryCode() != null) {
            EcCountry country = ecCountryRepository
                .findFirstByCode(transport.getDestinationInformation().getCountryCode());
            List<Integer> listDestinations = new ArrayList<>();
            listDestinations.add(country.getEcCountryNum());
            searchCriteriaPricingBooking.setListDestinations(listDestinations);
        }

        if (transport.getDestinationInformation() != null
            && transport.getDestinationInformation().getCountryCode() != null)

        {
            List<String> listOriginCity = new ArrayList<>();
            listOriginCity
                .add(transport.getOriginInformation() != null ? transport.getOriginInformation().getCity() : null);
            searchCriteriaPricingBooking.setListOriginCity(listOriginCity);
        }

        if (transport.getDestinationInformation() != null
            && transport.getDestinationInformation().getCountryCode() != null)

        {
            List<String> listDestinationCity = new ArrayList<>();
            listDestinationCity.add(transport.getDestinationInformation().getCity());
            searchCriteriaPricingBooking.setListDestinationCity(listDestinationCity);
        }

        return searchCriteriaPricingBooking;
    }

    public List<Resource<PlanTransportWSO>>
        getNextPickUpDatePlanTransportRequest(SearchCriteriaPlanTranportWSO criteria) throws Exception {
        boolean hasLoadPlanning = false;
        List<MyTowerErrorMsg> typeErrors = new ArrayList<MyTowerErrorMsg>();
        Date currentDate = new Date();
        Page<Resource<PlanTransportWSO>> planTransportRequest = getPlanTransportRequest(criteria);
        List<EbPlanTransportNew> listPlanTransportNew = new ArrayList<EbPlanTransportNew>();
        List<Resource<PlanTransportWSO>> listPlanTransportRequest = planTransportRequest.getContent();
        SearchCriteriaPricingBooking searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();
        List<EbPlanTransportNew> listPlanTransportNewValid = new ArrayList<>();

        if (!listPlanTransportRequest.isEmpty()) {
            PlanTransportWSO planTransportWSO = listPlanTransportRequest.get(0).getContent();
            if (!planTransportRequest
                .isEmpty()) searchCriteriaPricingBooking = getSearchCriteriaFromTrCriteria(planTransportWSO, criteria);

            for (Resource<PlanTransportWSO> ptr: listPlanTransportRequest) {
                listPlanTransportNew = planTransportNewService
                    .listLoadPlanIsConstraints(searchCriteriaPricingBooking, ptr.getContent());

                if (listPlanTransportNew != null && listPlanTransportNew.size() > 0) {

                    for (EbPlanTransportNew ptn: listPlanTransportNew) {

                        if (ptn.getWeekTemplate() != null) {

                            if (ptn.getWeekTemplate().getDateDebut().getTime() < currentDate.getTime()
                                && currentDate.getTime() < ptn.getWeekTemplate().getDateFin().getTime()) {
                                listPlanTransportNewValid.add(ptn);
                            }

                        }

                    }

                }
                else {
                    typeErrors.add(ImportEnMasseUtils.setErrorMessage("The load planning is null or empty"));
                }

                if (listPlanTransportNewValid.isEmpty()) {
                    typeErrors
                        .add(
                            ImportEnMasseUtils
                                .setErrorMessage("week template is not valid or the week template is null"));
                }

                for (EbPlanTransportNew ptn: listPlanTransportNewValid) {

                    if (ptn.getWeekTemplate().getDays() != null) {
                        List<EbWeekTemplateDay> days = ptn
                            .getWeekTemplate().getDays().stream().filter(day -> day.isOpen())
                            .collect(Collectors.toList());

                        List<Date> datesInRange = new ArrayList<>();
                        List<String> openDays = new ArrayList<>();
                        Calendar calendar = new GregorianCalendar();
                        calendar.setTime(ptn.getWeekTemplate().getDateDebut());

                        Calendar endCalendar = new GregorianCalendar();
                        endCalendar.setTime(ptn.getWeekTemplate().getDateFin());

                        while (calendar.before(endCalendar)) {
                            Date result = calendar.getTime();
                            datesInRange.add(result);
                            calendar.add(Calendar.DATE, 1);
                        }

                        List<Date> dateTangeFromCurrentDay = datesInRange
                            .stream().filter(date -> date.compareTo(new Date()) > 0).collect(Collectors.toList());

                        SimpleDateFormat simpleDayformat = new SimpleDateFormat("EEE", Locale.ENGLISH);

                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                        dateTangeFromCurrentDay.forEach(date -> {
                            days.forEach(day -> {

                                if (day.getHours() != null && day.getHours().size() > 0 && day.getHours().get(0) != null
                                    && day.getHours().get(0).getStartHour() != null
                                    && day.getHours().get(0).getStartHour() != "") {
                                    JourEnum jour = JourEnum.getJourEnumByCode(day.getCode());
                                    Calendar c = Calendar.getInstance();

                                    if ((simpleDayformat.format(date) + ".").equalsIgnoreCase(jour.getLibelle())) {

                                        try {
                                            c.setTime(simpleDateFormat.parse(simpleDateFormat.format(date)));

                                            c
                                                .add(
                                                    Calendar.HOUR,
                                                    Integer
                                                        .parseInt(
                                                            day
                                                                .getHours().get(0).getStartHour().substring(
                                                                    0,
                                                                    day
                                                                        .getHours().get(0).getStartHour()
                                                                        .indexOf(':'))));

                                            c
                                                .add(
                                                    Calendar.MINUTE,
                                                    Integer
                                                        .parseInt(
                                                            day
                                                                .getHours().get(0).getStartHour().substring(
                                                                    day.getHours().get(0).getStartHour().indexOf(':')
                                                                        + 1,
                                                                    day.getHours().get(0).getStartHour().length())));

                                            if (day.getHours().get(0).getCutOffTime() != null) {
                                                c
                                                    .add(
                                                        Calendar.HOUR,
                                                        -Integer
                                                            .parseInt(
                                                                day
                                                                    .getHours().get(0).getCutOffTime().substring(
                                                                        0,
                                                                        day
                                                                            .getHours().get(0).getCutOffTime()
                                                                            .indexOf(':'))));
                                                c
                                                    .add(
                                                        Calendar.MINUTE,
                                                        -Integer
                                                            .parseInt(
                                                                day
                                                                    .getHours().get(0).getCutOffTime().substring(
                                                                        day
                                                                            .getHours().get(0).getCutOffTime()
                                                                            .indexOf(':') + 1,
                                                                        day
                                                                            .getHours().get(0).getCutOffTime()
                                                                            .length())));
                                            }

                                            if (sdf
                                                .parse(sdf.format(c.getTime()))
                                                .after(sdf.parse(sdf.format(new Date())))) {
                                                openDays
                                                    .add(
                                                        simpleDayformat.format(c.getTime()) + " "
                                                            + sdf.format(c.getTime()));
                                            }

                                        } catch (ParseException e1) {
                                            // TODO Auto-generated catch block
                                            e1.printStackTrace();
                                        }

                                    }

                                }
                                else {
                                    typeErrors.add(ImportEnMasseUtils.setErrorMessage("Hour of pick up is null"));
                                }

                            });
                        });

                        if (openDays != null && openDays.size() > 0 && openDays.get(0) != null) {
                            hasLoadPlanning = true;
                            SimpleDateFormat df6 = new SimpleDateFormat("E dd/MM/yyyy HH:mm:ss", Locale.ENGLISH);

                            try {
                                Date date = df6.parse(openDays.get(0));
                                ptr.getContent().setNextPickUpDate(date);
                            } catch (ParseException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                        }

                    }

                }

            }

        }

        List<Resource<PlanTransportWSO>> result = new ArrayList<>();

        if (!typeErrors.isEmpty() && !hasLoadPlanning) {
            List<MyTowerErrorMsg> ErrorMessages = new ArrayList<>();
            ErrorMessages
                .add(
                    ImportEnMasseUtils
                        .setErrorMessage(
                            "Some fields are not valid (not filled or not compliants with specifications) or sent model doesn’t match expected"));
            throw new MyTowerException(ErrorMessages);
        }
        else {
            result = listPlanTransportRequest
                .stream().filter(t -> t.getContent().getNextPickUpDate() != null).collect(Collectors.toList());
        }

        return result;
    }

    private SearchCriteriaPricingBooking getSearchCriteria(SearchCriteriaPlanTranportWSO criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        SearchCriteriaPricingBooking searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();

        if (StringUtils.isNotBlank(criteria.getNatureTransport())) {
            searchCriteriaPricingBooking
                .setxEcNature(Enumeration.NatureDemandeTransport.getCodeByLibelle(criteria.getNatureTransport()));
        }

        if (StringUtils.isNotBlank(criteria.getTransporterEmail())) {
            searchCriteriaPricingBooking.setTransporterEmail(criteria.getTransporterEmail());
        }

        if (StringUtils.isNotBlank(criteria.getTransporterCompanyCode())) {
            searchCriteriaPricingBooking.setTransporterCompanyCode(criteria.getTransporterCompanyCode());
        }

        if (criteria.isValorized() == true) {
            searchCriteriaPricingBooking.setValorized(criteria.isValorized());
        }

        if (criteria.getListModeTransport() != null && !criteria.getListModeTransport().isEmpty()) {
            List<Integer> listModeTransport = new ArrayList<>();
            criteria.getListModeTransport().forEach(modeTransport -> {
                listModeTransport.add(Enumeration.ModeTransport.getCodeByLibelle(modeTransport));
            });

            searchCriteriaPricingBooking.setListModeTransport(listModeTransport);
        }

        if (criteria.getListOrigins() != null && !criteria.getListOrigins().isEmpty()) {
            List<Integer> listOrigins = new ArrayList<>();

            criteria.getListOrigins().forEach(origin -> {
                String originLower = origin.toLowerCase();
                String originFormatted = originLower.substring(0, 1).toUpperCase() + originLower.substring(1);
                EcCountry country = ecCountryRepository.findByLibelle(originFormatted);
                if (country != null) listOrigins.add(country.getEcCountryNum());
            });
            searchCriteriaPricingBooking.setListOrigins(listOrigins);
        }

        if (criteria.getListDestinations() != null && !criteria.getListDestinations().isEmpty()) {
            List<Integer> listDestinations = new ArrayList<>();

            criteria.getListDestinations().forEach(destination -> {
                String destinationLower = destination.toLowerCase();
                String destinationFormatted = destinationLower.substring(0, 1).toUpperCase()
                    + destinationLower.substring(1);
                EcCountry country = ecCountryRepository.findByLibelle(destinationFormatted);
                if (country != null) listDestinations.add(country.getEcCountryNum());
            });
            searchCriteriaPricingBooking.setListDestinations(listDestinations);
        }

        searchCriteriaPricingBooking.setListPartyDestination(criteria.getListPartyDestination());
        searchCriteriaPricingBooking.setListTransportRef(criteria.getListTransportRef());
        searchCriteriaPricingBooking.setListCustomerReference(criteria.getListCustomerReference());
				searchCriteriaPricingBooking.setModule(Enumeration.Module.PRICING.getCode());
        searchCriteriaPricingBooking.setConnectedUserNum(connectedUser.getEbUserNum());
        searchCriteriaPricingBooking.setSearchByWS(true);
        searchCriteriaPricingBooking.setSize(criteria.getSize());
        searchCriteriaPricingBooking.setPageNumber(criteria.getPage());
        searchCriteriaPricingBooking.setDateCreationFrom(criteria.getTransportCreationDateFrom());
        searchCriteriaPricingBooking.setDateCreationTo(criteria.getTransportCreationDateTo());
        searchCriteriaPricingBooking.setListIncoterm(criteria.getListIncoterm());
        searchCriteriaPricingBooking.setListOriginCity(criteria.getListOriginCity());
        searchCriteriaPricingBooking.setListDestinationCity(criteria.getListDestinationCity());

        searchCriteriaPricingBooking.setNumAwbBol(criteria.getNumAwbBol());

        if (criteria.getCategoryList() != null && !criteria.getCategoryList().isEmpty()) {
            List<Integer> listCategoryFields = new ArrayList<>();

            for (CategoryWSO trCat: criteria.getCategoryList()) {
                List<Integer> listLabelNum = ebLabelRepository
                    .findLabelsNumBycategorieLabelAndLabel(trCat.getCategory(), trCat.getCategoryLabel());

                if (listLabelNum != null && !listLabelNum.isEmpty()) {
                    listCategoryFields.addAll(listLabelNum);
                }

            }

            if (!listCategoryFields.isEmpty()) {
                Map<String, List<Integer>> mapCategoryFields = new HashMap<>();
                mapCategoryFields.put("categoryField1", listCategoryFields);
                searchCriteriaPricingBooking.setMapCategoryFields(mapCategoryFields);
            }

        }

        if (criteria.getCustomFieldList() != null && !criteria.getCustomFieldList().isEmpty()) {
            List<String> listCustomFields = new ArrayList<>();

            for (CustomFieldWSO trCf: criteria.getCustomFieldList()) {
                listCustomFields.add("\"" + trCf.getLabel() + "\":" + "\"" + trCf.getValue() + "\"");
            }

            searchCriteriaPricingBooking.setListCustomFieldsValues(listCustomFields);
        }

        return searchCriteriaPricingBooking;
    }

    private List<EbMarchandise> setMarchandisesList(Integer ebDemandeNum) {
        return ebMarchandiseRepository.findAllByEbDemande_ebDemandeNum(ebDemandeNum);
    }

    private List<ExEbDemandeTransporteur> setExEbDemandeTransporteurList(Integer ebDemandeNum) {
        return exEbDemandeTransporteurRepository.findAllByXEbDemande_ebDemandeNum(ebDemandeNum);
    }

    public PlanTransportWSO convertEbDemandeToTransport(EbDemande demande) {
        PlanTransportWSO transport = new PlanTransportWSO();
        transport.setTaxableWeight((demande.getTotalTaxableWeight() == null) ? 0 : demande.getTotalTaxableWeight());
        transport.setTotalVolume((demande.getTotalVolume() == null) ? 0 : demande.getTotalVolume());
        transport.setTotalWeight((demande.getTotalVolume() == null) ? 0 : demande.getTotalVolume());
        transport.setNature(demande.getxEcNature());
        transport.setGrouping((demande.getIsGrouping() == null) ? false : demande.getIsGrouping());
        transport.setItemId(demande.getEbDemandeNum());
        transport.setArrivalDate(demande.getDateOfArrival());
        transport.setGoodsAvailabilityDate(demande.getDateOfGoodsAvailability());
        transport.setTransportUpdateDate(demande.getDateMaj());
        transport.setCity(demande.getCity());

        if (demande.getxEbCostCenter() != null) {
            transport.setCostCenterCode(demande.getxEbCostCenter().getLibelle());
        }

        transport.setCustomerReference(demande.getCustomerReference());
        transport.setServiceLevel(demande.getTypeRequestLibelle());
        transport.setMawb(demande.getMawb());
        transport.setNumAwbBol(demande.getNumAwbBol());
        transport.setIncoterm(demande.getxEcIncotermLibelle());
        transport.setFlightVessel(demande.getFlightVessel());

        if (demande.getXecCurrencyInvoice() != null) transport
            .setInvoicingCurrencyCode(demande.getXecCurrencyInvoice().getCode());

        transport.setInsurance(demande.getInsurance());

        if (demande.getXecCurrencyInsurance() != null) transport
            .setInsuranceCurrencyCode(demande.getXecCurrencyInsurance().getCode());
        transport.setInsuranceValue(demande.getInsuranceValue());

        transport.setEligibleForConsolidation(demande.getEligibleForConsolidation());
        transport.setFinalChoiceToBeCalculated(demande.getFinalChoiceToBeCalculated());

        transport.setTransportMode(demande.getModeTransporteur());
        transport.setTransportReference(demande.getRefTransport());
        transport.setTransportStatus(StatutDemande.getLibelleJasonByCode(demande.getxEcStatut()));
        transport.setConsolidationStatus(ConsolidationGroupage.getCodeAlphaByCode(demande.getxEcStatutGroupage()));
        transport.setCustomsOffice(demande.getAboutTrCustomOffice());

        if (demande.getUser() != null) {
            transport.setShipperUserEmail(demande.getUser().getEmail());
            transport.setShipperUserName(demande.getUser().getPrenomNom());
            if (demande.getUser().getEbEtablissement() != null) transport
                .setShipperEtablishment(demande.getUser().getEbEtablissement().getNom());
        }

        if (demande.getxEbTypeFluxNum() != null) {
            EbTypeFlux ebTypeFlux = ebTypeFluxRepository.findById(demande.getxEbTypeFluxNum()).get();
            transport.setFlowTypeCode(ebTypeFlux.getCode());
        }

        if (demande.getxEbTypeTransport() != null) {
            EbTypeTransport typeTransport = ebTypeTransportRepository.findById(demande.getxEbTypeTransport()).get();

            if (typeTransport != null) {
                transport.setTransportType(typeTransport.getLibelle());
            }

        }

        if (demande.getListCustomsFields() != null && !demande.getListCustomsFields().isEmpty()) {
            transport.setCustomFieldList(new ArrayList<CustomFieldWSO>());
            CustomFieldWSO customField = null;

            for (CustomFields cf: demande.getListCustomsFields()) {

                if (cf.getValue() != null && !cf.getValue().isEmpty()) {
                    customField = new CustomFieldWSO();
                    customField.setItemId(cf.getNum());
                    customField.setLabel(cf.getLabel());
                    customField.setValue(cf.getValue());
                    transport.getCustomFieldList().add(customField);
                }

            }

        }

        if (demande.getListCategories() != null && !demande.getListCategories().isEmpty()) {
            demande.setListCategories(demande.getListCategories());
            transport.setCategoryList(new ArrayList<CategoryWSO>());
            CategoryWSO category = null;

            for (EbCategorie c: demande.getListCategories()) {
                EbLabel ebLabel = c.getLabels() != null && !c.getLabels().isEmpty() ?
                    c.getLabels().stream().findAny().get() :
                    null;

                String label = null;
                if (ebLabel != null) label = ebLabel.getLibelle();

                category = new CategoryWSO();
                category.setItemId(c.getEbCategorieNum());
                category.setCategory(c.getLibelle());
                category.setCategoryLabel(label);
                transport.getCategoryList().add(category);
            }

        }

        if (demande.getListMarchandises() != null && !demande.getListMarchandises().isEmpty()) {
            List<UnitWSO> listUnit = convertListMarchandisesToListUnits(demande.getListMarchandises());
            transport.setUnitList(listUnit);
        }

        // ----- set party
        if (demande.getEbPartyOrigin() != null) {
            transport.setOriginInformation(convertEbPartyToAddresseWSO(demande.getEbPartyOrigin()));
        }

        if (demande.getEbPartyDest() != null) {
            transport.setDestinationInformation(convertEbPartyToAddresseWSO(demande.getEbPartyDest()));
        }

        if (demande.getEbPartyNotif() != null) {
            transport.setNotifyInformation(convertEbPartyToAddresseWSO(demande.getEbPartyNotif()));
        }

        if (demande.getxEbUserDestCustomsBroker() != null) {
            transport.setDestCustomUserEmail(demande.getxEbUserDestCustomsBroker().getEmail());
        }

        if (demande.getxEbUserDest() != null) {
            transport.setDestUserEmail(demande.getxEbUserDest().getEmail());
        }

        if (demande.getxEbUserObserver() != null && !demande.getxEbUserObserver().isEmpty()) {
            List<String> observerUserEmailList = demande
                .getxEbUserObserver().stream().map(us -> us.getEmail()).collect(Collectors.toList());
            transport.setObserverUserEmailList(observerUserEmailList);
        }

        if (demande.getxEbUserOriginCustomsBroker() != null) {
            transport.setOriginCustomUserEmail(demande.getxEbUserOriginCustomsBroker().getEmail());
        }

        if (demande.getxEbUserOrigin() != null) {
            transport.setOriginUserEmail(demande.getxEbUserOrigin().getEmail());
        }

        if (demande.getxEbSchemaPsl() != null) {
            transport.setTrackingPointSchema(demande.getxEbSchemaPsl().getDesignation());

            if (demande.getxEbSchemaPsl().getListPsl() != null && !demande.getxEbSchemaPsl().getListPsl().isEmpty()) {
                List<String> trackingPointCodeList = (demande.getxEbSchemaPsl().getListPsl() != null
                    && demande.getxEbSchemaPsl().getListPsl().isEmpty()) ?
                        demande
                            .getxEbSchemaPsl().getListPsl().stream().filter(psl -> psl.getSchemaActif())
                            .map(it -> it.getCodeAlpha()).collect(Collectors.toList()) :
                        null;
                transport.setTrackingPointCodeList(trackingPointCodeList);
            }

        }

        if (demande.getExEbDemandeTransporteurs() != null && !demande.getExEbDemandeTransporteurs().isEmpty()) {
            transport.setQuotationList(new ArrayList<>());

            for (ExEbDemandeTransporteur dt: demande.getExEbDemandeTransporteurs()) {
                transport.getQuotationList().add(convertEbDemandeQuoteToQuotation(dt));
            }

        }

        if (demande.getListAdditionalCost() != null && !demande.getListAdditionalCost().isEmpty()) {
            transport.setAdditionalCostList(new ArrayList<CostWSO>());
            CostWSO costField = null;

            for (EbCost cf: demande.getListAdditionalCost()) {
                costField = new CostWSO();
                costField.setLibelle(cf.getLibelle());
                costField.setPrice(cf.getPrice());
                costField.setPriceEuro(cf.getPriceEuro());

                if (cf.getxEcCurrencyNum() != null) {
                    EcCurrency ecCurrency = ecCurrencyRepository.findById(cf.getxEcCurrencyNum()).get();
                    costField.setCurrencyCode(ecCurrency.getCode());
                }

                transport.getAdditionalCostList().add(costField);
            }

        }

        return transport;
    }

    public AddressWSO convertEbPartyToAddresseWSO(EbParty partyOrig) {
        AddressWSO party = new AddressWSO();

        party.setItemId(partyOrig.getEbPartyNum());
        party.setReference(partyOrig.getReference());
        party.setCity(partyOrig.getCity());

        if (partyOrig.getxEcCountry() != null) {
            party.setCountryCode(partyOrig.getxEcCountry().getCode());
        }

        party.setCompany(partyOrig.getCompany());
        party.setAddress(partyOrig.getAdresse());
        party.setZipCode(partyOrig.getZipCode());
        party.setZoneReference(partyOrig.getZoneRef());
        party.setOpeningHours(partyOrig.getOpeningHours());
        party.setAirport(partyOrig.getAirport());
        party.setEmail(partyOrig.getEmail());
        party.setPhone(partyOrig.getPhone());
        return party;
    }

    private QuotationWSO convertEbDemandeQuoteToQuotation(ExEbDemandeTransporteur ebDemandeTransporteur) {
        QuotationWSO quotation = new QuotationWSO();
        quotation.setItemId(ebDemandeTransporteur.getExEbDemandeTransporteurNum());
        quotation.setCarrierUserEmail(ebDemandeTransporteur.getxTransporteur().getEmail());
        quotation.setTransitTime(ebDemandeTransporteur.getTransitTime());
        quotation.setPrice(ebDemandeTransporteur.getPrice());
        quotation.setComment(ebDemandeTransporteur.getComment());
        quotation.setStatus(StatutCarrier.getLibelleByCode(ebDemandeTransporteur.getStatus()));
        quotation.setModeTransport(ModeTransport.getLibelleByCode(ebDemandeTransporteur.getxEcModeTransport()));

        if (ebDemandeTransporteur.getListPlCostItem() != null && !ebDemandeTransporteur.getListPlCostItem().isEmpty()
            && ebDemandeTransporteur.getIsFromTransPlan() != null
            && ebDemandeTransporteur.getIsFromTransPlan() == true) {
            quotation.setCostList(new ArrayList<CostWSO>());
            CostWSO costWSO = null;

            for (EbPlCostItem costItem: ebDemandeTransporteur.getListPlCostItem()) {
                costWSO = new CostWSO();
                costWSO.setLibelle(costItem.getLibelle());
                costWSO.setPrice(costItem.getPrice());
                quotation.getCostList().add(costWSO);
            }

        }
        else if (ebDemandeTransporteur.getListCostCategorie() != null
            && !ebDemandeTransporteur.getListCostCategorie().isEmpty()) {
            quotation.setCostList(new ArrayList<CostWSO>());
            CostWSO costWSO = null;
            ObjectMapper mapper = new ObjectMapper();

            List<EbCostCategorie> listCostCategorie = mapper
                .convertValue(
                    ebDemandeTransporteur.getListCostCategorie(),
                    mapper.getTypeFactory().constructCollectionType(List.class, EbCostCategorie.class));

            for (EbCostCategorie ebCostCategorie: listCostCategorie) {

                for (EbCost costItem: ebCostCategorie.getListEbCost()) {
                    costWSO = new CostWSO();
                    costWSO.setType(ebCostCategorie.getLibelle());
                    costWSO.setLibelle(costItem.getLibelle());
                    costWSO.setPrice(costItem.getPrice());
                    costWSO.setPriceEuro(costItem.getPrice());

                    if (costItem.getxEcCurrencyNum() != null) {
						Optional<EcCurrency> ecCurrency = ecCurrencyRepository.findById(costItem.getxEcCurrencyNum());
						if (ecCurrency.isPresent()) {
							costWSO.setCurrencyCode(ecCurrency.get().getCode());
						}
                    }

                    quotation.getCostList().add(costWSO);
                }

            }

        }

        // quotation.setCarrierCurrencyCode(ebDemandeTransporteur.);
        // quotation.setNegotiatedDateList(ebDemandeTransporteur.);
        // quotation.setCostItemList(ebDemandeTransporteur.getListPlCostItem());
        return quotation;
    }

    private List<UnitWSO> convertListMarchandisesToListUnits(List<EbMarchandise> listmarchandise) {
        List<UnitWSO> listUnit = new ArrayList<>();
        UnitWSO unit = null;

        for (EbMarchandise mar: listmarchandise) {
            unit = new UnitWSO();

            unit.setItemId(mar.getEbMarchandiseNum());
            unit.setWeight(mar.getWeight());
            unit.setLength(mar.getLength());
            unit.setWidth(mar.getWidth());
            unit.setHeigth(mar.getHeigth());
            unit.setVolume(mar.getVolume());
            unit.setNumberOfUnits(mar.getNumberOfUnits());

            MarchandiseDangerousGood dg = MarchandiseDangerousGood.getByCode(mar.getDangerousGood());
            if (dg != null) unit.setDangerousGood(dg.getLibelle());

            if (mar.getxEbTypeUnit() != null) {
                EbTypeUnit ebTypeUnit = ebTypeUnitRepository.findById(mar.getxEbTypeUnit()).get();

                if (ebTypeUnit != null) {
                    unit.setUnitType(ebTypeUnit.getCode());
                }

            }

            unit.setStackable(mar.getStackable());
            unit.setUnitReference(mar.getUnitReference());
            unit.setSscc(mar.getSscc());
            unit.setTrackingNumber(mar.getTracking_number());
            unit.setItemName(mar.getNomArticle());
            unit.setContainerType(mar.getTypeContainer());
            unit.setUn(mar.getUn());
            unit.setClassGood(mar.getClassGood());
            unit.setPackaging(mar.getPackaging());
            unit.setComment(mar.getComment());

            if (mar.getxEcCountryOrigin() != null) {
                EcCountry ecCountry = ecCountryRepository.findById(mar.getxEcCountryOrigin()).get();
                unit.setOriginCountry(ecCountry.getLibelle());
                unit.setManifactoringCountryCode(ecCountry.getCode());
            }

            listUnit.add(unit);
        }

        return listUnit;
    }

    private TransportLightWSO convertEbDemandeToTransportLightWSO(EbDemande ebDemande) {
        TransportLightWSO transportLightWSO = new TransportLightWSO();
        transportLightWSO.setDangerousGoods(ebDemande.getDg());
        transportLightWSO.setCustomerReference(ebDemande.getCustomerReference());
        transportLightWSO.setTransportReference(ebDemande.getRefTransport());
        transportLightWSO.setServiceLevel(ebDemande.getTypeRequestLibelle());

        if (ebDemande.getListCustomsFields() != null && !ebDemande.getListCustomsFields().isEmpty()) {
            transportLightWSO.setCustomFieldList(new ArrayList<CustomFieldWSO>());
            CustomFieldWSO customField = null;

            for (CustomFields cf: ebDemande.getListCustomsFields()) {

                if (cf.getValue() != null && !cf.getValue().isEmpty()) {
                    customField = new CustomFieldWSO();
                    customField.setItemId(cf.getNum());
                    customField.setLabel(cf.getLabel());
                    customField.setValue(cf.getValue());
                    transportLightWSO.getCustomFieldList().add(customField);
                }

            }

        }

        if (ebDemande.getListCategories() != null && !ebDemande.getListCategories().isEmpty()) {
            ebDemande.setListCategories(ebDemande.getListCategories());
            transportLightWSO.setCategoryList(new ArrayList<CategoryWSO>());
            CategoryWSO category = null;

            for (EbCategorie c: ebDemande.getListCategories()) {
                EbLabel ebLabel = c.getLabels() != null && !c.getLabels().isEmpty() ?
                    c.getLabels().stream().findAny().get() :
                    null;

                String label = null;
                if (ebLabel != null) label = ebLabel.getLibelle();

                category = new CategoryWSO();
                category.setItemId(c.getEbCategorieNum());
                category.setCategory(c.getLibelle());
                category.setCategoryLabel(label);
                transportLightWSO.getCategoryList().add(category);
            }

        }

        if (ebDemande.getExEbDemandeTransporteurs() != null && !ebDemande.getExEbDemandeTransporteurs().isEmpty()) {
            transportLightWSO.setQuotationList(new ArrayList<>());

            for (ExEbDemandeTransporteur dt: ebDemande.getExEbDemandeTransporteurs()) {
                transportLightWSO.getQuotationList().add(convertEbDemandeQuoteToQuotation(dt));
            }

        }

        transportLightWSO.setInsuranceValue(ebDemande.getInsuranceValue());

        if (ebDemande.getListAdditionalCost() != null && !ebDemande.getListAdditionalCost().isEmpty()) {
            transportLightWSO.setAdditionalCostList(new ArrayList<CostWSO>());
            CostWSO costField = null;

            for (EbCost cf: ebDemande.getListAdditionalCost()) {
                costField = new CostWSO();
                costField.setLibelle(cf.getLibelle());
                costField.setPrice(cf.getPrice());
                costField.setPriceEuro(cf.getPriceEuro());

                if (cf.getxEcCurrencyNum() != null) {
                    EcCurrency ecCurrency = ecCurrencyRepository.findById(cf.getxEcCurrencyNum()).get();
                    costField.setCurrencyCode(ecCurrency.getCode());
                }

                transportLightWSO.getAdditionalCostList().add(costField);
            }

        }

        if (ebDemande.getListMarchandises() != null && !ebDemande.getListMarchandises().isEmpty()) {
            List<UnitWSO> listUnit = convertListMarchandisesToListUnits(ebDemande.getListMarchandises());
            transportLightWSO.setUnitList(listUnit);
            /**
             * Class good and un
             */
            List<Double> classGoods = new ArrayList<>();

            listUnit.forEach(t -> {
                if (t.getClassGood() != null) classGoods.add(Double.parseDouble(t.getClassGood()));
                if (t.getUn() != null) transportLightWSO.setClassGoods(classGoods);
                transportLightWSO.setUns(t.getUn());
            });
        }

        return transportLightWSO;
    }
}
