package com.adias.mytowereasy.api.wso;

import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "SearchCriteriaTransport")
public class SearchCriteriaTransportWSO {
    @ApiModelProperty(notes = "list transport reference")
    private List<String> listTransportRef;
    @ApiModelProperty(notes = "list customer reference")
    private List<String> listCustomerReference;

    @ApiModelProperty(notes = "Information about transport field")
    private String numAwbBol;

    @ApiModelProperty(notes = "Transport status", example = "INP")
    private List<String> transportStatusList;

    @Valid
    private List<CategoryWSO> categoryList;

    @Valid
    private List<CustomFieldWSO> customFieldList;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @ApiModelProperty(notes = "Transport creation date from", example = "2019-12-05 09:18:12-05:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date transportCreationDateFrom;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @ApiModelProperty(notes = "Transport creation date to", example = "2019-12-05 09:18:12-05:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date transportCreationDateTo;

    @ApiModelProperty(notes = "Number of items in the page", value = "5")
    private Integer size;
    @ApiModelProperty(notes = "Page number", value = "0")
    private Integer page;

    @ApiModelProperty(notes = "Request for informations about the TR grouped", value = "false")
    private boolean fetchConsolidationInformation;

    @ApiModelProperty(notes = "Request for status of TR grouped", example = "PRS")
    private String consolidationStatus;

    @ApiModelProperty(notes = "Request for the list of TR grouped")
    private String groupedTRList;

    @ApiModelProperty(notes = "Request for the list of TR by the transporter email")
    private String transporterEmail;

    @ApiModelProperty(notes = "Request for the list of TR by transporter company code")
    private String transporterCompanyCode;

    @ApiModelProperty(
        notes = "Request for the list of TR by their nature, nature field is mapped to natureTransport field",
        example = "EDI")
    private String natureTransport;
    @ApiModelProperty(notes = "Request for the TR valorized", value = "false")
    private Boolean isValorized;
    @ApiModelProperty(notes = "Request for the TR Grouping", value = "false")
    private Boolean isGrouping;
    @ApiModelProperty(notes = "Total taxable weight")
    private Double totalTaxableWeight;
    @ApiModelProperty(notes = "Request for the list of TR by their status", example = "WRE")
    private String xEcStatus;

    public List<String> getListTransportRef() {
        return listTransportRef;
    }

    public void setListTransportRef(List<String> listTransportRef) {
        this.listTransportRef = listTransportRef;
    }

    public String getNumAwbBol() {
        return numAwbBol;
    }

    public void setNumAwbBol(String numAwbBol) {
        this.numAwbBol = numAwbBol;
    }

    public List<String> getTransportStatusList() {
        return transportStatusList;
    }

    public void setTransportStatusList(List<String> transportStatusList) {
        this.transportStatusList = transportStatusList;
    }

    public Date getTransportCreationDateFrom() {
        return transportCreationDateFrom;
    }

    public void setTransportCreationDateFrom(Date transportCreationDateFrom) {
        this.transportCreationDateFrom = transportCreationDateFrom;
    }

    public Date getTransportCreationDateTo() {
        return transportCreationDateTo;
    }

    public void setTransportCreationDateTo(Date transportCreationDateTo) {
        this.transportCreationDateTo = transportCreationDateTo;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public List<String> getListCustomerReference() {
        return listCustomerReference;
    }

    public void setListCustomerReference(List<String> listCustomerReference) {
        this.listCustomerReference = listCustomerReference;
    }

    public List<CategoryWSO> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<CategoryWSO> categoryList) {
        this.categoryList = categoryList;
    }

    public List<CustomFieldWSO> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<CustomFieldWSO> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public boolean isFetchConsolidationInformation() {
        return fetchConsolidationInformation;
    }

    public void setFetchConsolidationInformation(boolean fetchConsolidationInformation) {
        this.fetchConsolidationInformation = fetchConsolidationInformation;
    }

    public String getConsolidationStatus() {
        return consolidationStatus;
    }

    public void setConsolidationStatus(String consolidationStatus) {
        this.consolidationStatus = consolidationStatus;
    }

    public String getGroupedTRList() {
        return groupedTRList;
    }

    public void setGroupedTRList(String groupedTRList) {
        this.groupedTRList = groupedTRList;
    }

    public String getTransporterEmail() {
        return transporterEmail;
    }

    public void setTransporterEmail(String transporterEmail) {
        this.transporterEmail = transporterEmail;
    }

    public String getTransporterCompanyCode() {
        return transporterCompanyCode;
    }

    public void setTransporterCompanyCode(String transporterCompanyCode) {
        this.transporterCompanyCode = transporterCompanyCode;
    }

    public String getNatureTransport() {
        return natureTransport;
    }

    public void setNatureTransport(String natureTransport) {
        this.natureTransport = natureTransport;
    }

    public Boolean getIsValorized() {
        return isValorized;
    }

    public void setIsValorized(Boolean isValorized) {
        this.isValorized = isValorized;
    }

    public Boolean getIsGrouping() {
        return isGrouping;
    }

    public void setIsGrouping(Boolean isGrouping) {
        this.isGrouping = isGrouping;
    }

    public Double getTotalTaxableWeight() {
        return totalTaxableWeight;
    }

    public void setTotalTaxableWeight(Double totalTaxableWeight) {
        this.totalTaxableWeight = totalTaxableWeight;
    }

    public String getxEcStatus() {
        return xEcStatus;
    }

    public void setxEcStatus(String xEcStatus) {
        this.xEcStatus = xEcStatus;
    }
}
