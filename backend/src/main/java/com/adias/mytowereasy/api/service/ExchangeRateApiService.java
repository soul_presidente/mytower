package com.adias.mytowereasy.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.api.wso.ExchangeRateUnitIntegrationResponse;
import com.adias.mytowereasy.api.wso.ExchangeRateWSO;
import com.adias.mytowereasy.api.wso.SearchExchangeRateCriteriaWSO;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbCompagnieCurrency;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.model.Enumeration.RoleSociete;
import com.adias.mytowereasy.service.CurrencyService;
import com.adias.mytowereasy.service.MyTowerService;

@Service
public class ExchangeRateApiService extends MyTowerService
{

	@Autowired
	CurrencyService currencyService;

	public List<ExchangeRateUnitIntegrationResponse> addListExchangeRate(
		List<ExchangeRateWSO> exchangeRateList
	)
	{
		List<ExchangeRateUnitIntegrationResponse> exchangeRateIntegrationResponseList = new ArrayList<ExchangeRateUnitIntegrationResponse>();
		EbUser user = connectedUserService.getCurrentUser();
		exchangeRateList
			.parallelStream()
			.forEach(
				exchangeRate -> {
					exchangeRateIntegrationResponseList.add(checkListExchangeRateByUnit(exchangeRate, user));
				}
			);

		List<ExchangeRateUnitIntegrationResponse> integrationErrorsMessages = exchangeRateIntegrationResponseList
			.stream()
			.filter(
				exchangeRateIntegrationResponse -> CollectionUtils
					.isNotEmpty(exchangeRateIntegrationResponse.getIntegrationErrorsMessages())
			)
			.collect(Collectors.toList());

		if (CollectionUtils.isEmpty(integrationErrorsMessages))
		{
			List<EbCompagnieCurrency> listCurrency = exchangeRateIntegrationResponseList
				.stream()
				.map(exchangeRateIntegrationResponse -> exchangeRateIntegrationResponse.getEbCompagnieCurrency())
				.collect(Collectors.toList());
			ebCompagnieCurrencyRepository.saveAll(listCurrency);
		}

		integrationErrorsMessages.forEach(errorsMessages -> errorsMessages.setEbCompagnieCurrency(null));

		return integrationErrorsMessages;
	}

	private ExchangeRateUnitIntegrationResponse checkListExchangeRateByUnit(
		ExchangeRateWSO exchangeRate,
		EbUser user
	)
	{
		List<String> errorsMessagesIntegrationList = new ArrayList<String>();
		checkIfNecessaryDataAreProvided(exchangeRate, errorsMessagesIntegrationList);

		EbCompagnie company = checkCompanyTransporteurValidity(
			exchangeRate,
			user,
			errorsMessagesIntegrationList
		);

		EcCurrency originCurrency = getCurrency(
			exchangeRate.getOriginCurrencyCode(),
			errorsMessagesIntegrationList
		);
		EcCurrency targetCurrency = getCurrency(
			exchangeRate.getTargetCurrencyCode(),
			errorsMessagesIntegrationList
		);

		EbCompagnieCurrency newCompagnieCurrency = null;
		if (originCurrency != null && targetCurrency != null && company != null)
		{
			
			Optional<EbCompagnieCurrency> oldCompagnieCurrency = ebCompagnieCurrencyRepository
				.findByOriginAndTargetAndCompanySirenAndIsCreatedFromIHM(
					originCurrency.getCode(),
					targetCurrency.getCode(),
					company.getSiren(),
					false
				);

			if (oldCompagnieCurrency.isPresent())
			{
				exchangeRate.setExchangeRateId(oldCompagnieCurrency.get().getEbCompagnieCurrencyNum());
			}

			newCompagnieCurrency = new EbCompagnieCurrency(
				exchangeRate,
				user,
				company,
				originCurrency,
				targetCurrency
			);
		}

		return new ExchangeRateUnitIntegrationResponse(
			exchangeRate,
			newCompagnieCurrency,
			errorsMessagesIntegrationList
		);
	}

	private EcCurrency getCurrency(
		String currencyCode,
		List<String> errorsMessagesIntegrationList
	)
	{
		Optional<EcCurrency> currency = ecCurrencyRepository
			.findByCodeIgnoreCase(currencyCode);

		if (!currency.isPresent())
		{
			errorsMessagesIntegrationList
				.add(
					String.format("Currency with code %s not found", currencyCode)
				);
			return null;
		}
		return currency.get();
	}

	private EbCompagnie checkCompanyTransporteurValidity(
		ExchangeRateWSO exchangeRate,
		EbUser user,
		List<String> errorsMessagesIntegrationList
	)
	{
		List<Integer> listIdEtablissementGuest = ebRelationRepository
			.getEtablissementsByHostUserId(user.getEbUserNum());

		EbCompagnie company = new EbCompagnie();

		if (CollectionUtils.isNotEmpty(listIdEtablissementGuest))
		{
			company = ebCompagnieRepository.findOneBySiren(exchangeRate.getCarrierCompanySiren());

			if (
				company == null ||
					!RoleSociete.ROLE_PRESTATAIRE.getCode().equals(company.getCompagnieRole())
			)
			{
				errorsMessagesIntegrationList.add(String.format("CarrierCompanySiren is not valid"));
			}
			else {
			boolean isCompanyInMyContact = userService
				.isCompanyInMyContactByListEtablissementIds(
					company.getEbCompagnieNum(),
					listIdEtablissementGuest
				);
			if (!isCompanyInMyContact)
			{
				errorsMessagesIntegrationList
					.add(
						String.format("Company with siren %s not found", exchangeRate.getCarrierCompanySiren())
					);
			}
			}
		}
		else
		{
			errorsMessagesIntegrationList
				.add(String.format("Connected user has no carrier in his community"));
		}
		return company;
	}

	private void checkIfNecessaryDataAreProvided(
		ExchangeRateWSO exchangeRate,
		List<String> errorsMessagesIntegrationList
	)
	{
		if (exchangeRate.getStartDate() == null)
		{
			errorsMessagesIntegrationList.add("StartDate not provided");
		}
		if (exchangeRate.getRate() == null)
		{
			errorsMessagesIntegrationList.add("Rate not provided");
		}
		if (StringUtils.isBlank(exchangeRate.getCarrierCompanySiren()))
		{
			errorsMessagesIntegrationList.add("CarrierCompanySiren not provided");
		}
		if (StringUtils.isBlank(exchangeRate.getOriginCurrencyCode()))
		{
			errorsMessagesIntegrationList.add("OriginCurrencyCode not provided");
		}
		if (StringUtils.isBlank(exchangeRate.getTargetCurrencyCode()))
		{
			errorsMessagesIntegrationList.add("TargetCurrencyCode not provided");
		}
	}

	public EbCompagnieCurrency update(@Valid ExchangeRateWSO exchangeRate)
	{
		Optional<EbCompagnieCurrency> dbCompagnieCurrency = ebCompagnieCurrencyRepository
			.findEbCompagnieCurrencyById(exchangeRate.getExchangeRateId());

		if (!dbCompagnieCurrency.isPresent())
		{
			throw new MyTowerException(
				String.format("exchangeRateId with code %s not found", exchangeRate.getExchangeRateId())
			);
		}
		if (!dbCompagnieCurrency.get().getIsUsed())
		{
			dbCompagnieCurrency.get().setEuroExchangeRate(exchangeRate.getRate());
		}
		dbCompagnieCurrency.get().setDateModification(new Date());
		dbCompagnieCurrency.get().setDateFin(exchangeRate.getEndDate());
		currencyService.updateEbCompanyCurrency(dbCompagnieCurrency.get());

		return dbCompagnieCurrency.get();
	}

	public List<EbCompagnieCurrency> search(
		@Valid SearchExchangeRateCriteriaWSO exchangeRateCriteriaWSO
	)
	{
		checkSearchDataValidity(exchangeRateCriteriaWSO);
		List<EbCompagnieCurrency> currencyList = ebCompagnieCurrencyRepository
			.findCompagnieCurrencyWithOriginCurrencyCodeAndTargetCurrencyCodeAndCompagnieSiren(
				exchangeRateCriteriaWSO.getOriginCurrencyCode(),
				exchangeRateCriteriaWSO.getTargetCurrencyCode(),
				exchangeRateCriteriaWSO.getCarrierCompanySiren()
			);

		if (CollectionUtils.isEmpty(currencyList))
		{
			throw new MyTowerException(
				String
					.format(
						"Exchange with originCurrencyCode %s, targetCurrencyCode %s and carrierCompanySiren %s not found",
						exchangeRateCriteriaWSO.getOriginCurrencyCode(),
						exchangeRateCriteriaWSO.getTargetCurrencyCode(),
						exchangeRateCriteriaWSO.getCarrierCompanySiren()
					)
			);
		}

		return currencyList;
	}

	private void checkSearchDataValidity(@Valid SearchExchangeRateCriteriaWSO exchangeRate)
	{
		if (StringUtils.isBlank(exchangeRate.getCarrierCompanySiren()))
		{
			throw new MyTowerException(String.format("CarrierCompanySiren not provided"));
		}
		if (StringUtils.isBlank(exchangeRate.getOriginCurrencyCode()))
		{
			throw new MyTowerException(String.format("OriginCurrencyCode not provided"));
		}
		if (StringUtils.isBlank(exchangeRate.getTargetCurrencyCode()))
		{
			throw new MyTowerException(String.format("TargetCurrencyCode not provided"));
		}
	}

	public ResponseEntity<String> delete()
	{
		EbUser user = connectedUserService.getCurrentUser();

		List<Integer> companyCurrencieIds = ebCompagnieCurrencyRepository
			.selectListCompagnieCurrencyByCompagnieIdAndIsDeleted(
				user.getEbCompagnie().getEbCompagnieNum(),
				false
			);
		
		if (CollectionUtils.isNotEmpty(companyCurrencieIds))
		{
			ebCompagnieCurrencyRepository
			.deleteCompagnieCurrencyByCompagnieCurrencyIds(
				new Date(),
				companyCurrencieIds
			);
		}
		return ResponseEntity.ok(new String("Exchange rate settings deleted successfully"));
	}

}
