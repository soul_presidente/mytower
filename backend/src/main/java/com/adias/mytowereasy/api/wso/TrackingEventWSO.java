package com.adias.mytowereasy.api.wso;

import java.util.Date;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "TrackingEvent")
public class TrackingEventWSO {
    @ApiModelProperty(notes = "Id (Useless for creation)")
    private Integer itemId;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @ApiModelProperty(notes = "The event Actual date", example = "2019-12-05 09:18:12-05:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date dateActual;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @ApiModelProperty(notes = "The event Negociated date", example = "2019-12-05 09:18:12-05:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date dateNegociated;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @ApiModelProperty(notes = "The event Estimated date", example = "2019-12-05 09:18:12-05:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private Date dateEstimated;

    @ApiModelProperty(notes = "Reference of pricing request")
    private String refTransport;

    @ApiModelProperty(notes = "Reference of unit")
    private String unitReference;

    @ApiModelProperty(notes = "Customer reference of pricing request")
    private String customerReference;

    @ApiModelProperty(notes = "City")
    private String city;

    @NotBlank
    @ApiModelProperty(notes = "Country code", required = true)
    private String codeCountry;

    @NotBlank
    @ApiModelProperty(notes = "PSL Code", required = true)
    private String codePsl;

    @Email
    @ApiModelProperty(notes = "Creator email")
    private String creatorEmail;

    @ApiModelProperty(notes = "Comment")
    private String comment;

    @ApiModelProperty(notes = "Integration file name")
    private String fileName;

    @NotBlank
    @ApiModelProperty(notes = "Type of Event (Event/Deviation)", example = "Event", required = true)
    private String typeEvent;

    @NotBlank
    @ApiModelProperty(notes = "Company carrier code", required = true)
    private String carrierCode;

    @ApiModelProperty(notes = "Deviation label category", example = "Information flow issues")
    private String categoryLabel;

    public TrackingEventWSO() {
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public Date getDateActual() {
        return dateActual;
    }

    public void setDateActual(Date dateActual) {
        this.dateActual = dateActual;
    }

    public Date getDateNegociated() {
        return dateNegociated;
    }

    public void setDateNegociated(Date dateNegociated) {
        this.dateNegociated = dateNegociated;
    }

    public Date getDateEstimated() {
        return dateEstimated;
    }

    public void setDateEstimated(Date dateEstimated) {
        this.dateEstimated = dateEstimated;
    }

    public String getRefTransport() {
        return refTransport;
    }

    public void setRefTransport(String refTransport) {
        this.refTransport = refTransport;
    }

    public String getUnitReference() {
        return unitReference;
    }

    public void setUnitReference(String unitReference) {
        this.unitReference = unitReference;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCodeCountry() {
        return codeCountry;
    }

    public void setCodeCountry(String codeCountry) {
        this.codeCountry = codeCountry;
    }

    public String getCodePsl() {
        return codePsl;
    }

    public void setCodePsl(String codePsl) {
        this.codePsl = codePsl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(String typeEvent) {
        this.typeEvent = typeEvent;
    }

    public String getCreatorEmail() {
        return creatorEmail;
    }

    public void setCreatorEmail(String creatorEmail) {
        this.creatorEmail = creatorEmail;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getCarrierCode() {
        return carrierCode;
    }

    public void setCarrierCode(String carrierCode) {
        this.carrierCode = carrierCode;
    }

    public String getCategoryLabel() {
        return categoryLabel;
    }

    public void setCategoryLabel(String categoryLabel) {
        this.categoryLabel = categoryLabel;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
