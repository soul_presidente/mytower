package com.adias.mytowereasy.api.wso;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "PlanTransportLight")
public class planTransportLightWSO {
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ssXXX")
    @ApiModelProperty(notes = "The next pick up date", example = "2019-12-05 09:18:12-05:00")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ssXXX")
    private String nextPickUpDate;
}
