package com.adias.mytowereasy.api.wso;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "Quotation")
public class QuotationWSO {
    private String quoteReference;

    @ApiModelProperty(notes = "Id (Useless for creation)")
    private Integer itemId;

    @NotEmpty
    @Email
    @ApiModelProperty(notes = "Carrier's email", required = true)
    private String carrierUserEmail;

    private Integer transitTime;

    private BigDecimal price;

    private String comment;

    @ApiModelProperty(notes = "Specified automatically if not provided")
    private String status;

    private String modeTransport;

    private String carrierCurrencyCode;

    @Valid
    private List<PslWSO> negotiatedDateList;

    @Valid
    private List<CostWSO> costList;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getCarrierUserEmail() {
        return carrierUserEmail;
    }

    public void setCarrierUserEmail(String carrierUserEmail) {
        this.carrierUserEmail = carrierUserEmail;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getModeTransport() {
        return modeTransport;
    }

    public void setModeTransport(String modeTransport) {
        this.modeTransport = modeTransport;
    }

    public String getCarrierCurrencyCode() {
        return carrierCurrencyCode;
    }

    public void setCarrierCurrencyCode(String carrierCurrencyCode) {
        this.carrierCurrencyCode = carrierCurrencyCode;
    }

    public List<PslWSO> getNegotiatedDateList() {
        return negotiatedDateList;
    }

    public void setNegotiatedDateList(List<PslWSO> negotiatedDateList) {
        this.negotiatedDateList = negotiatedDateList;
    }

    public List<CostWSO> getCostList() {
        return costList;
    }

    public void setCostList(List<CostWSO> costList) {
        this.costList = costList;
    }

    public String getQuoteReference() {
        return quoteReference;
    }

    public void setQuoteReference(String quoteReference) {
        this.quoteReference = quoteReference;
    }
}
