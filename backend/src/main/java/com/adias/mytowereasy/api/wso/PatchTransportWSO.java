package com.adias.mytowereasy.api.wso;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "PatchTransport")
public class PatchTransportWSO {
    @ApiModelProperty(notes = "Id (Useless for creation)")
    private Integer itemId;

    @ApiModelProperty(notes = "Reference of pricing request, generated automatically if not provided")
    private String transportReference;

    @ApiModelProperty(notes = "Information about grouped transport status ")
    private String consolidationStatus;
    @ApiModelProperty(notes = "Customer reference of pricing request")
    private String customerReference;

    @ApiModelProperty(notes = "Air Way Bill BOL Number")
    private String awbBolNumber;

    @ApiModelProperty(notes = "Flight Vessel")
    private String flightVessel;

    @ApiModelProperty(notes = "Master Air Way Bill Number")
    private String mawbNumber;

    @ApiModelProperty(notes = "Customer field List of pricing request")
    private List<CustomFieldWSO> customFieldList;

	// added to facilitate the confirmation of grouped Tr
	@ApiModelProperty(notes = "List Customer reference of pricing requests")
	private List<String> listCustomerReference;

    @ApiModelProperty(notes = "id de la vignette")
    private Integer quoteId;

	public List<String> getListCustomerReference() {
		return listCustomerReference;
	}

	public void setListCustomerReference(List<String> listCustomerReference) {
		this.listCustomerReference = listCustomerReference;
	}

	public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getTransportReference() {
        return transportReference;
    }

    public void setTransportReference(String transportReference) {
        this.transportReference = transportReference;
    }

    public String getConsolidationStatus() {
        return consolidationStatus;
    }

    public void setConsolidationStatus(String consolidationStatus) {
        this.consolidationStatus = consolidationStatus;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public List<CustomFieldWSO> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<CustomFieldWSO> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public String getAwbBolNumber() {
        return awbBolNumber;
    }

    public void setAwbBolNumber(String awbBolNumber) {
        this.awbBolNumber = awbBolNumber;
    }

    public String getFlightVessel() {
        return flightVessel;
    }

    public void setFlightVessel(String flightVessel) {
        this.flightVessel = flightVessel;
    }

    public String getMawbNumber() {
        return mawbNumber;
    }

    public void setMawbNumber(String mawbNumber) {
        this.mawbNumber = mawbNumber;
    }

    public Integer getQuoteId() {
        return quoteId;
    }

    public void setQuoteId(Integer quoteId) {
        this.quoteId = quoteId;
    }
}
