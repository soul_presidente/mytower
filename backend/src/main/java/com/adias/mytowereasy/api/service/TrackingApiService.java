package com.adias.mytowereasy.api.service;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.BooleanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.adias.mytowereasy.api.util.MyTowerErrorMsg;
import com.adias.mytowereasy.api.wso.TrackingEventWSO;
import com.adias.mytowereasy.cronjob.tt.model.TmpEvent;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCountry;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.TtEnumeration.TypeEvent;
import com.adias.mytowereasy.model.tt.EbTtCategorieDeviation;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.TrackService;
import com.adias.mytowereasy.service.TrackServiceImpl;
import com.adias.mytowereasy.util.ImportEnMasseUtils;


@Service
public class TrackingApiService extends MyTowerService {
    @Autowired
    TrackService tracktraceService;

    private static final Logger logger = LoggerFactory.getLogger(TrackServiceImpl.class);

    public TrackingEventWSO createTrackingEvent(TrackingEventWSO event) throws Exception {
        List<MyTowerErrorMsg> listErrors = new ArrayList<>();

        TmpEvent tmpEvent = new TmpEvent();

        if (event.getDateActual() == null && event.getDateEstimated() == null && event.getDateNegociated() == null) {
            listErrors.add(ImportEnMasseUtils.setErrorMessage("Date not found"));
        }
        else {
            tmpEvent.setDateActual(event.getDateActual());

            tmpEvent.setDateEstimated(event.getDateEstimated());

            tmpEvent.setDateNegociated(event.getDateNegociated());
        }

        if (StringUtils.isEmpty(event.getRefTransport()) && StringUtils.isEmpty(event.getCustomerReference())) {
            listErrors
                .add(ImportEnMasseUtils.setErrorMessage("Both refTransport and customerReference can't be empty"));
        }
        else {

            if (org.apache.commons.lang.StringUtils.isNotBlank(event.getRefTransport())) {
                Integer demandeNum = ebDemandeRepository.getEbDemandeNumByRefTransport(event.getRefTransport());

                if (demandeNum == null) {
                    listErrors
                        .add(
                            ImportEnMasseUtils
                                .setErrorMessage("Tr with refTransport = " + event.getRefTransport() + " not found"));
                }
                else {
                    tmpEvent.setRefTransport(event.getRefTransport());
                }

            }

            if (org.apache.commons.lang.StringUtils.isNotBlank(event.getCustomerReference())) {

                if (!ebDemandeRepository.existsByCustomerReference(event.getCustomerReference())) {
                    listErrors
                        .add(
                            ImportEnMasseUtils
                                .setErrorMessage(
                                    "Tr with customerReference = " + event.getCustomerReference() + " not found"));
                }
                else {
                    tmpEvent.setCustomerReference(event.getCustomerReference());
                }

            }

        }

        if (org.apache.commons.lang.StringUtils.isNotBlank(event.getUnitReference())) {
            tmpEvent.setUnitReference(event.getUnitReference());
        }

        tmpEvent.setCity(event.getCity());

        EcCountry ecCountry = null;

        tmpEvent.setCodeCountry(event.getCodeCountry());

        if (tmpEvent.getCodeCountry() != null) {
            ecCountry = serviceListStatique.getEcCountryByCode(tmpEvent.getCodeCountry());
        }

        if (ecCountry == null) {
            listErrors.add(ImportEnMasseUtils.setErrorMessage("Country " + tmpEvent.getCodeCountry() + " not found"));
        }

        tmpEvent.setxEcCountry(ecCountry);

        tmpEvent.setCodePslNormalise(event.getCodePsl());

        EbCompagnie carrierCompagnie = getCarrierCompagnie(event);

        if (carrierCompagnie == null) {
            listErrors
                .add(ImportEnMasseUtils.setErrorMessage("carrier code : '" + event.getCarrierCode() + "' not found"));
        }

        tmpEvent.setxEbCompagnieTransporteur(carrierCompagnie);

        tmpEvent.setComment(event.getComment());

        tmpEvent.setCreatorEmail(event.getCreatorEmail());

        EbUser creator = ebUserRepository.findByEmail(event.getCreatorEmail());

        if (creator == null) {
            listErrors
                .add(ImportEnMasseUtils.setErrorMessage("creator email :  " + event.getCreatorEmail() + " not found"));
        }
        else {
            tmpEvent.setxEbCompagnie(creator.getEbCompagnie());
        }

        if (event.getFileName() != null) {
            tmpEvent.setNomFichier(event.getFileName());
        }

        if (event.getTypeEvent().toUpperCase().equals("DEVIATION")) {
            tmpEvent.setTypeEvent(TypeEvent.EVENT_DEVIATION);

            if (!StringUtils.isEmpty(event.getCategoryLabel())) {
                EbTtCategorieDeviation categorieDeviation = ebTtCategorieDeviationRepository
                    .findOneByTypeEventAndLibelle(TypeEvent.EVENT_DEVIATION.getCode(), event.getCategoryLabel());

                if (categorieDeviation == null) {
                    listErrors
                        .add(
                            ImportEnMasseUtils
                                .setErrorMessage("Category '" + event.getCategoryLabel() + "' not found"));
                }
                else {
                    tmpEvent.setCategorieDeviation(categorieDeviation);
                    tmpEvent.setLibelleCategorieDeviation(categorieDeviation.getLibelle());
                }

            }

        }
        else if (event.getTypeEvent().toUpperCase().equals("EVENT")) {
            tmpEvent.setTypeEvent(TypeEvent.EVENT_PSL);
        }
        else {
            listErrors.add(ImportEnMasseUtils.setErrorMessage("Type event not found"));
        }

        if (!listErrors.isEmpty()) throw new MyTowerException(listErrors);

        generateEventByEdiService.createTmpEvent(tmpEvent);
        event.setItemId(tmpEvent.getTmpEventNum());
        return event;
    }

    private EbCompagnie getCarrierCompagnie(TrackingEventWSO event) {
        return daoCompagnie.getCarrierCompagnie(event);
    }

    @Transactional
    public ResponseEntity<String> createTracings(Integer[] transportRequestIds) throws Exception {
        if (ArrayUtils.isEmpty(transportRequestIds)) throw new IllegalArgumentException("tr ids empty");

        List<MyTowerErrorMsg> errors = new ArrayList<>();
        List<Integer> trsWithFalseIds = new ArrayList<>();
        List<Integer> trsNotConfirmed = new ArrayList<>();
        List<Integer> trsWithTracing = new ArrayList<>();

        for (Integer id: transportRequestIds) {

            if (!ebDemandeRepository.existsById(id)) {
                trsWithFalseIds.add(id);
            }

            if (BooleanUtils
                .isFalse(ebDemandeRepository.checkIfTrIsConfirmedAndNotCancelled(id, StatutDemande.WPU.getCode()))) {
                trsNotConfirmed.add(id);
            }

            if (ebTrackTraceRepository.existsByXEbDemandeEbDemandeNum(id)) {
                trsWithTracing.add(id);
            }

        }

        if (CollectionUtils.isNotEmpty(trsWithFalseIds)) errors
            .add(ImportEnMasseUtils.setErrorMessage("No tr found for these ids " + trsWithFalseIds + ""));

        if (CollectionUtils.isNotEmpty(trsNotConfirmed)) errors
            .add(ImportEnMasseUtils.setErrorMessage("Trs with ids " + trsNotConfirmed + " are not yet confirmed"));

        if (CollectionUtils.isNotEmpty(trsWithTracing)) errors
            .add(ImportEnMasseUtils.setErrorMessage("Trs with ids in " + trsWithTracing + " already have tracing"));

        if (CollectionUtils.isNotEmpty(errors)) throw new MyTowerException(errors);

        for (Integer id: transportRequestIds) {
            EbTtSchemaPsl schemaPsl = ebDemandeRepository.selectSchemaPslByEbDemandeNum(id);

            EbDemande tr = ebDemandeRepository.findOneByEbDemandeNum(id);

            if (schemaPsl != null) {
                schemaPsl
                    .getListPsl().stream().filter(it -> it.getIsChecked() == null).forEach(it -> it.setIsChecked(true));
                ebDemandeRepository.updateSchemaPslEbDemande(id, schemaPsl);
            }

            try {
                tracktraceService.insertEbTrackTrace(tr);
            } catch (Exception e) {
                logger.debug("An problem occured in the insertion of object TrackTrace");
                e.printStackTrace();
            }

        }

        return ResponseEntity.ok(new String("Trackings created successfully"));
    }
}
