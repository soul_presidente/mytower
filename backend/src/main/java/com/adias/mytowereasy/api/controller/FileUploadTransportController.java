package com.adias.mytowereasy.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import com.adias.mytowereasy.api.service.FileUploadTransportApiService;

import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("/api/v1/upload-file")
public class FileUploadTransportController extends ApiController {
    @Autowired
    FileUploadTransportApiService fileUploadTransportApiService;

    @ApiOperation("Upload file by http url for transport request or unit with replacement of the old document")
    @PutMapping
    public @ResponseBody
    ResponseEntity<FileUploadTransportApiService.UploadResponse> putFile(@RequestParam(value = "file") MultipartFile file,
                                                                         @RequestParam(value = "transportReference") String transportReference,
                                                                         @RequestParam(value = "documentTypeCode") String documentTypeCode,
                                                                         @RequestParam(value = "pslEvent", required = false) String pslEvent
    )
            throws Exception {
        return fileUploadTransportApiService.uploadFile(file, transportReference, documentTypeCode, pslEvent);
    }


}
