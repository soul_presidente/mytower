package com.adias.mytowereasy.api.wso;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel
public class SearchCriteriaDeliveryWSO {

    @ApiModelProperty(notes = "Customer Delivery Reference")
    private String customerOrderReference;

    @ApiModelProperty(notes = "Request Owner Email")
    private String requestOwnerEmail;

    public String getCustomerOrderReference() {
        return customerOrderReference;
    }

    public void setCustomerOrderReference(String customerOrderReference) {
        this.customerOrderReference = customerOrderReference;
    }

    public String getRequestOwnerEmail() {
        return requestOwnerEmail;
    }

    public void setRequestOwnerEmail(String requestOwnerEmail) {
        this.requestOwnerEmail = requestOwnerEmail;
    }
}
