package com.adias.mytowereasy.api.service;

import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbTypeDocuments;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.Module;
import com.adias.mytowereasy.model.Enumeration.UploadDirectory;
import com.adias.mytowereasy.repository.EbDemandeFichiersJointRepositorty;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EbTypeDocumentsRepository;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.fileupload.FileUploadService;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;


@Service
public class FileUploadTransportApiService extends MyTowerService {
    @Autowired
    EbDemandeRepository ebDemandeRepository;

    @Autowired
    EbDemandeFichiersJointRepositorty ebDemandeFichiersJointRepositorty;

    @Autowired
    FileUploadService fileUploadService;

    @Autowired
    EbTypeDocumentsRepository ebTypeDocumentsRepository;

    @Transactional
    public ResponseEntity<UploadResponse> uploadFile(MultipartFile multipartFile, String transportReference, String documentTypeCode, String pslEvent) throws Exception {


        if (multipartFile == null) {
            return new ResponseEntity<>(new UploadResponse("File is mandatory"), HttpStatus.BAD_REQUEST);
        }

        EbDemande ebDemande = ebDemandeRepository.getByRefTransport(transportReference);

        if (ebDemande == null) {
            return new ResponseEntity<>(new UploadResponse(
                "Reference '" + transportReference + "' of pricing request  not found"),
                HttpStatus.BAD_REQUEST);
        }

        EbUser connectedUser = connectedUserService.getCurrentUser();
        String userEmail = connectedUser.getEmail();

        EbUser creatorUser = ebUserRepository.findOneByEmailIgnoreCase(userEmail);

        if (ebDemande.getUser() == null) {
            return new ResponseEntity<>(new UploadResponse(
                "User with email : " + creatorUser.getEmail() + " does not have access to this transport request"),
                HttpStatus.UNAUTHORIZED);
        }

        Integer uploadDirectoryCode = UploadDirectory.PRICING_BOOKING.getCode();
        Integer module = Module.TRANSPORT_MANAGEMENT.getCode();


        EbTypeDocuments typeDoc = ebTypeDocumentsRepository.findFirstByCode(documentTypeCode);

        if (typeDoc == null) return new ResponseEntity<>(new UploadResponse(
            "Document type '" + documentTypeCode + "' not found"),
            HttpStatus.BAD_REQUEST);

        if (!typeDoc.getListModuleStr().contains(module.toString())) {
            return new ResponseEntity<>(new UploadResponse(
                "Document type '" + documentTypeCode + "' not visible in "
                    + Module.TRANSPORT_MANAGEMENT.getLibelle()),
                HttpStatus.NOT_ACCEPTABLE);
        }

        MultipartFile[] files = {
                multipartFile
        };

        List<Map<String, Object>> result = fileUploadService
                .handleFileUploadDemande(
                        documentTypeCode,
                        uploadDirectoryCode,
                        ebDemande.getEbDemandeNum(),
                        creatorUser.getEbUserNum(),
                        creatorUser.getEbEtablissement().getEbEtablissementNum(),
                        module,
                        pslEvent,
                        null,
                        files,
                        false);

        if(CollectionUtils.isEmpty(result)) {
            return new ResponseEntity<>(new UploadResponse("An Unknown error occured"),
                    HttpStatus.CONFLICT);
        }

        return ResponseEntity.ok(new UploadResponse("file uploaded"));
    }

    public static class UploadResponse {
        public String message;
        UploadResponse(String message) {
            this.message = message;
        }
    }

}
