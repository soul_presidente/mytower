package com.adias.mytowereasy.api.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.api.util.MyTowerErrorMsg;
import com.adias.mytowereasy.api.wso.CategoryWSO;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.repository.EbCategorieRepository;
import com.adias.mytowereasy.util.ImportEnMasseUtils;


@Service
public class CategoryApiService {
    @Autowired
    EbCategorieRepository ebCategorieRepository;

    public List<EbCategorie> generateCategoriesFromWSO(List<CategoryWSO> wsoList, Integer ebCompagnieNum) {
        List<MyTowerErrorMsg> categoriesErrors = new ArrayList<>();
        List<EbCategorie> listCategorie = ebCategorieRepository.findByCompagnieEbCompagnieNum(ebCompagnieNum);

        List<EbCategorie> listCategorieFinal = new ArrayList<>();

        if (listCategorie == null
            || listCategorie.isEmpty()) categoriesErrors.add(ImportEnMasseUtils.setErrorMessage("No category found"));

        for (CategoryWSO trCat: wsoList) {
            EbCategorie cat = listCategorie
                .stream().filter(it -> it.getLibelle().contentEquals(trCat.getCategory())).findFirst().orElse(null);

            if (cat == null) categoriesErrors
                .add(ImportEnMasseUtils.setErrorMessage("No corresponding category found for " + trCat.getCategory()));
            else {
                Set<EbLabel> labels = cat.getLabels();
                cat.setLabels(null);

                if (trCat.getCategoryLabel() != null && !trCat.getCategoryLabel().isEmpty() && labels != null
                    && !labels.contains(null)) {
                    EbLabel label = labels
                        .stream().filter(it -> it.getLibelle().contentEquals(trCat.getCategoryLabel())).findFirst()
                        .orElse(null);
                    if (label == null) categoriesErrors
                        .add(
                            ImportEnMasseUtils
                                .setErrorMessage("No corresponding label found for " + trCat.getCategoryLabel()));

                    cat.setLabels(new HashSet<>());
                    cat.getLabels().add(label);
                    listCategorieFinal.add(cat);
                }

            }

        }

        if (!categoriesErrors.isEmpty()) throw new MyTowerException(categoriesErrors);

        return listCategorieFinal;
    }
}
