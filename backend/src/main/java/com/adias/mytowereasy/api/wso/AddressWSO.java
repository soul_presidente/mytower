package com.adias.mytowereasy.api.wso;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "Address")
public class AddressWSO {
    @ApiModelProperty(notes = "Id (Useless for creation)")
    private Integer itemId;

    @NotEmpty
    @NotNull
    @ApiModelProperty(notes = "Reference", required = true)
    private String reference;

    @NotEmpty
    @NotNull
    @ApiModelProperty(notes = "City", required = true)
    private String city;

    @NotEmpty
    @NotNull
    @Size(min = 2, max = 2)
    @ApiModelProperty(notes = "Country ISO code", required = true)
    private String countryCode;

    private String company;

    private String address;

    private String zipCode;

    private String zoneReference;

    private String openingHours;

    private String airport;

    @Email
    private String email;

    private String phone;
    private String comment;

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getZoneReference() {
        return zoneReference;
    }

    public void setZoneReference(String zoneReference) {
        this.zoneReference = zoneReference;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
