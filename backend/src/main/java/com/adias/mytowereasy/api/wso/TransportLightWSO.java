package com.adias.mytowereasy.api.wso;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "TransportLight")
public class TransportLightWSO {
    @ApiModelProperty(notes = "Customer Reference")
    private String customerReference;
    @ApiModelProperty(notes = "Dangerous Goods")
    private boolean dangerousGoods;
    @ApiModelProperty(notes = "Class Goods")
    private List<Double> classGoods;

    @ApiModelProperty(notes = "UN")
    private String uns;

    @ApiModelProperty(notes = "Reference of pricing request, generated automatically if not provided")
    private String transportReference;
    @NotEmpty
    @ApiModelProperty(notes = "Service level / type of request", required = true, example = "STD")
    private String serviceLevel;
    @Valid
    private List<CategoryWSO> categoryList;

    @Valid
    private List<CustomFieldWSO> customFieldList;

    @Valid
    @ApiModelProperty(notes = "Quotation list ")
    private List<QuotationWSO> quotationList;

    @ApiModelProperty(notes = "The insurance value")
    private Double insuranceValue;

    @Valid
    @ApiModelProperty(notes = "Informations about the additional cost")
    private List<CostWSO> additionalCostList;

    @Valid
    @NotEmpty
    @ApiModelProperty(notes = "Goods informations", required = true)
    private List<UnitWSO> unitList;
    @ApiModelProperty(notes = "Total taxable weight")
    private Double totalTaxableWeight;

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getTransportReference() {
        return transportReference;
    }

    public void setTransportReference(String transportReference) {
        this.transportReference = transportReference;
    }

    public List<QuotationWSO> getQuotationList() {
        return quotationList;
    }

    public void setQuotationList(List<QuotationWSO> quotationList) {
        this.quotationList = quotationList;
    }

    public Double getInsuranceValue() {
        return insuranceValue;
    }

    public void setInsuranceValue(Double insuranceValue) {
        this.insuranceValue = insuranceValue;
    }

    public List<CostWSO> getAdditionalCostList() {
        return additionalCostList;
    }

    public void setAdditionalCostList(List<CostWSO> additionalCostList) {
        this.additionalCostList = additionalCostList;
    }

    public List<UnitWSO> getUnitList() {
        return unitList;
    }

    public void setUnitList(List<UnitWSO> unitList) {
        this.unitList = unitList;
    }

    public String getServiceLevel() {
        return serviceLevel;
    }

    public void setServiceLevel(String serviceLevel) {
        this.serviceLevel = serviceLevel;
    }

    public List<CategoryWSO> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<CategoryWSO> categoryList) {
        this.categoryList = categoryList;
    }

    public List<CustomFieldWSO> getCustomFieldList() {
        return customFieldList;
    }

    public void setCustomFieldList(List<CustomFieldWSO> customFieldList) {
        this.customFieldList = customFieldList;
    }

    public boolean isDangerousGoods() {
        return dangerousGoods;
    }

    public void setDangerousGoods(boolean dangerousGoods) {
        this.dangerousGoods = dangerousGoods;
    }

    public List<Double> getClassGoods() {
        return classGoods;
    }

    public void setClassGoods(List<Double> classGoods) {
        this.classGoods = classGoods;
    }

    public String getUns() {
        return uns;
    }

    public void setUns(String uns) {
        this.uns = uns;
    }

    public Double getTotalTaxableWeight() {
        return totalTaxableWeight;
    }

    public void setTotalTaxableWeight(Double totalTaxableWeight) {
        this.totalTaxableWeight = totalTaxableWeight;
    }
}
