package com.adias.mytowereasy.api.util;

import java.util.List;


/**
 * A FunctionError object represents a consistency error when processing a
 * transportWSO object,
 * this object also contains the different messages of error encountered and the
 * ID of the
 * transportWSO.
 */
public class MytowerTransportErrorMsg {
    private int itemId;
    private String transportReference;
    private String customerReference;
    private List<MyTowerErrorMsg> errorMsgs;

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String getTransportReference() {
        return transportReference;
    }

    public void setTransportReference(String transportReference) {
        this.transportReference = transportReference;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public List<MyTowerErrorMsg> getErrorMsgs() {
        return errorMsgs;
    }

    public void setErrorMsgs(List<MyTowerErrorMsg> errorMsgs) {
        this.errorMsgs = errorMsgs;
    }
}
