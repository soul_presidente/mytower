package com.adias.mytowereasy.api.wso;

import javax.validation.constraints.NotEmpty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "CustomField")
public class CustomFieldWSO {
    @ApiModelProperty(notes = "Id (Useless for creation)")
    private Integer itemId;

    @NotEmpty
    @ApiModelProperty(notes = "Enter custom field code")
    private String code;

    private String label;

    private String value;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
