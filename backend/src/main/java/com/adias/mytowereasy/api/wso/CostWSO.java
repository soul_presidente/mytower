package com.adias.mytowereasy.api.wso;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


@ApiModel(value = "Cost")
public class CostWSO {
    private String quoteReference;
    private String code;

    @ApiModelProperty(notes = "Libelle of the cost")
    private String libelle;
    @ApiModelProperty(notes = "The price")
    private BigDecimal price;
    @ApiModelProperty(notes = "The price in Euro")
    private BigDecimal priceEuro;
	@ApiModelProperty(notes = "The currency code")
    private String currencyCode;

    @ApiModelProperty(notes = "This field grouped a list of costWSO that has the same type")
    private String type;

    public CostWSO(String libelle, BigDecimal price, BigDecimal priceEuro) {
        super();
        this.libelle = libelle;
        this.price = price;
        this.priceEuro = priceEuro;
    }

    public CostWSO() {
        super();
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPriceEuro() {
        return priceEuro;
    }

    public void setPriceEuro(BigDecimal priceEuro) {
        this.priceEuro = priceEuro;
    }

	public String getCurrencyCode() {
        return currencyCode;
    }

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getQuoteReference() {
        return quoteReference;
    }

    public void setQuoteReference(String quoteReference) {
        this.quoteReference = quoteReference;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
