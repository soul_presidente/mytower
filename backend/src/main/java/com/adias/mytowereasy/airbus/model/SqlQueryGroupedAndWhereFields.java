package com.adias.mytowereasy.airbus.model;

public class SqlQueryGroupedAndWhereFields {
    private String groupedFields;
    private String whereFields;
    private String joinFields;
    private String excludeFields;
    private String sqlQueryWithCategorie;

    public SqlQueryGroupedAndWhereFields(
        String groupedFields,
        String whereFields,
        String joinFields,
        String excludeFields,
        String sqlQueryWithCategorie) {
        this.groupedFields = groupedFields;
        this.whereFields = whereFields;
        this.sqlQueryWithCategorie = sqlQueryWithCategorie;
        this.joinFields = joinFields;
        this.excludeFields = excludeFields;
    }

    public String getGroupedFields() {
        return groupedFields;
    }

    public void setGroupedFields(String groupedFields) {
        this.groupedFields = groupedFields;
    }

    public String getWhereFields() {
        return whereFields;
    }

    public void setWhereFields(String whereFields) {
        this.whereFields = whereFields;
    }

    public String getSqlQueryWithCategorie() {
        return sqlQueryWithCategorie;
    }

    public void setSqlQueryWithCategorie(String sqlQueryWithCategorie) {
        this.sqlQueryWithCategorie = sqlQueryWithCategorie;
    }

    public String getJoinFields() {
        return joinFields;
    }

    public void setJoinFields(String joinFields) {
        this.joinFields = joinFields;
    }

    public String getExcludeFields() {
        return excludeFields;
    }

    public void setExcludeFields(String excludeFields) {
        this.excludeFields = excludeFields;
    }
}
