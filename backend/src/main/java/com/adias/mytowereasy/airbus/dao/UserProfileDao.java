package com.adias.mytowereasy.airbus.dao;

import java.util.List;

import com.adias.mytowereasy.airbus.model.EbUserProfile;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface UserProfileDao {
    public EbUserProfile selectEbUserProfile(SearchCriteria criterias);

    public List<EbUserProfile> getUserProfils(SearchCriteria criterias);
}
