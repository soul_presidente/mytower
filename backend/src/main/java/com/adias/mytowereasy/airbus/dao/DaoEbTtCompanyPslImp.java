package com.adias.mytowereasy.airbus.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.airbus.model.QEbTtCompanyPsl;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.model.tt.QEbTtSchemaPsl;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoEbTtCompanyPslImp implements DaoEbTtCompanyPsl {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbTtCompanyPsl qEbTtCompanyPsl = QEbTtCompanyPsl.ebTtCompanyPsl;
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbZoneCompagnie");

    QEbTtSchemaPsl qEbTtSchemaPsl = QEbTtSchemaPsl.ebTtSchemaPsl;

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbTtCompanyPsl> listTtCompanyPsl(SearchCriteria criteria) {
        List<EbTtCompanyPsl> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbTtCompanyPsl> query = new JPAQuery<>(em);

            query
                .select(
                    QEbTtCompanyPsl
                        .create(
                            qEbTtCompanyPsl.ebTtCompanyPslNum,
                            qEbTtCompanyPsl.libelle,
                            qEbTtCompanyPsl.dateAttendue,
                            qEbTtCompanyPsl.quantiteAttendue,
                            qEbTtCompanyPsl.refDocumentAttendue,
                            qEbCompagnie.ebCompagnieNum,
                            qEbCompagnie.nom,
                            qEbCompagnie.code,
                            qEbTtCompanyPsl.codeAlpha));

            query = getZoneGlobalWhere(query, where, criteria);

            query.from(qEbTtCompanyPsl);
            query.where(where);

            query = getZoneGlobalJoin(query, where, criteria);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbTtCompanyPsl, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbTtCompanyPsl.libelle.asc());
            }

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long countListTtCompanyPsl(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbTtCompanyPsl> query = new JPAQuery<EbTtCompanyPsl>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getZoneGlobalWhere(query, where, criteria);

            query.from(qEbTtCompanyPsl);
            query.where(where);

            query = getZoneGlobalJoin(query, where, criteria);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getZoneGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        if (criteria.isIncludeGlobalValues() || connectedUser.isControlTower()) {
            where
                .andAnyOf(
                    qEbCompagnie.ebCompagnieNum.isNull(),
                    qEbCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }
        else {
            where.and(qEbCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qEbTtCompanyPsl.libelle.toLowerCase().contains(term),
                    qEbTtCompanyPsl.codeAlpha.toLowerCase().contains(term));
        }

        return query;
    }

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    private JPAQuery getZoneGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        query.leftJoin(qEbTtCompanyPsl.ebCompagnie(), qEbCompagnie);
        return query;
    }

    @Override
    public List<EbTtSchemaPsl> getListSchemaPsl(SearchCriteria criteria) {
        List<EbTtSchemaPsl> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbTtSchemaPsl> query = new JPAQuery<>(em);

            query.select(qEbTtSchemaPsl);

            query = getSchemaPslGlobalWhere(query, where, criteria);

            // EntityGraph graphs = em.createEntityGraph(EbTtSchemaPsl.class);
            // graphs.addSubgraph("xEcIncoterm");
            // query.setHint("javax.persistence.loadgraph", graphs);

            query.from(qEbTtSchemaPsl);
            query.where(where);

            query = getSchemaPslGlobalJoin(query, where, criteria);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbTtSchemaPsl, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbTtSchemaPsl.designation.asc());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public Long getListSchemaPslCount(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbTtSchemaPsl> query = new JPAQuery<EbTtSchemaPsl>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getSchemaPslGlobalWhere(query, where, criteria);

            query.from(qEbTtSchemaPsl);
            query.where(where);

            query = getSchemaPslGlobalJoin(query, where, criteria);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private JPAQuery getSchemaPslGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);

        if (criteria.getEbEtablissementNum() != null) {
            where
                .andAnyOf(
                    qEbTtSchemaPsl.listEtablissement.contains(":" + criteria.getEbEtablissementNum() + ":"),
                    qEbTtSchemaPsl.listEtablissement.contains(":-1:"));
        }

        if (!criteria.isIncludeGlobalValues() || connectedUser.isControlTower()) {
            where.and(qEbCompagnie.ebCompagnieNum.eq(criteria.getEbCompagnieNum()));
        }
        else {
            Integer ebCompagnieNum = criteria.getEbCompagnieNum() != null ?
                criteria.getEbCompagnieNum() :
                connectedUser.getEbCompagnie().getEbCompagnieNum();
            where.andAnyOf(qEbCompagnie.ebCompagnieNum.isNull(), qEbCompagnie.ebCompagnieNum.eq(ebCompagnieNum));
        }

        where.and(qEbTtSchemaPsl.isDeleted.isNull().or(qEbTtSchemaPsl.isDeleted.isFalse()));

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qEbTtSchemaPsl.designation.toLowerCase().contains(term),
                    qEbTtCompanyPsl.codeAlpha.toLowerCase().contains(term));
        }

        return query;
    }

    private JPAQuery getSchemaPslGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        query.leftJoin(qEbTtSchemaPsl.xEbCompagnie(), qEbCompagnie);

        return query;
    }
}
