package com.adias.mytowereasy.airbus.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.airbus.dao.UserProfileDao;
import com.adias.mytowereasy.airbus.model.EbUserProfile;
import com.adias.mytowereasy.airbus.model.ExUserProfileModule;
import com.adias.mytowereasy.airbus.repository.EbUserProfileRepository;
import com.adias.mytowereasy.airbus.repository.ExUserProfileModuleRepository;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class UserProfileServiceImp implements UserProfileService {
    @Autowired
    private EbUserProfileRepository ebUserProfileRepository;
    @Autowired
    private ExUserProfileModuleRepository exUserProfileModuleRepository;
    @Autowired
    private UserProfileDao userProfileDao;
    @Autowired
    private ConnectedUserService connectedUserService;

    @Override
    public List<EbUserProfile> getAll() {
        SearchCriteria criterias = new SearchCriteria();
        EbUser connectedUser = this.connectedUserService.getCurrentUser();

        if (connectedUser != null && !connectedUser.isControlTower()) {
            criterias.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
        }

        return this.userProfileDao.getUserProfils(criterias);
    }

    @Override
    public EbUserProfile updateUserDetails(EbUserProfile ebUserProfile) {

        if (ebUserProfile.getListCategories() != null) {
            String labels = "";

            for (EbCategorie cat: ebUserProfile.getListCategories()) {

                if (cat.getLabels() != null) {

                    for (EbLabel lab: cat.getLabels()) {
                        if (lab.getEbLabelNum() != null) labels += (!labels.isEmpty() ? "," : "") + lab.getEbLabelNum();
                    }

                }

            }

            if (!labels.isEmpty()) ebUserProfile.setListLabels(labels);
        }

        if (ebUserProfile.getEbUserProfileNum() != null) {
            // deleting old access rights
            Set<ExUserProfileModule> oldListAccessRights = exUserProfileModuleRepository
                .findAllByEbUserProfileNum(ebUserProfile.getEbUserProfileNum());
            exUserProfileModuleRepository.deleteAll(oldListAccessRights);
        }

        // saving new access rights
        Set<ExUserProfileModule> listExUserProfileModule = ebUserProfile.getListAccessRights();
        ebUserProfile.setListAccessRights(null);
        ebUserProfileRepository.save(ebUserProfile);

        for (ExUserProfileModule exUserProfileModule: listExUserProfileModule) {
            exUserProfileModule.setEbUserProfile(ebUserProfile);
            exUserProfileModule.setModule(null);
            exUserProfileModule.setEbUserProfileNum(ebUserProfile.getEbUserProfileNum());
            // exUserProfileModule.setEbUserProfileNum(exUserProfileModule.getEbUserProfileNum());
            exUserProfileModule.setEcModuleNum(exUserProfileModule.getEcModuleNum());
        }

        exUserProfileModuleRepository.saveAll(listExUserProfileModule);

        return ebUserProfile;
    }

    @Override
    public EbUserProfile getDetailUserProfile(SearchCriteria criterias) {
        return userProfileDao.selectEbUserProfile(criterias);
    }

    @Override
    public boolean checkUserProfileByNom(String nom) {
        return ebUserProfileRepository.existsByNom(nom);
    }

    @Override
    public boolean deleteUserProfile(Integer id) {
        // you should check if user profile is already affected

        ebUserProfileRepository.deleteById(id);
        return true;
    }
}
