package com.adias.mytowereasy.airbus.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;


@Repository
public interface EbQrGroupeRepository extends JpaRepository<EbQrGroupe, Integer> {
    @Query("SELECT g.ebQrGroupeNum  FROM EbQrGroupe g WHERE g.xEbCompagnie.ebCompagnieNum = ?1 and  ( g.isDeleted is null or g.isDeleted is false )")
    public List<Integer> selectListEbQrGroupeNumByCompagnieNum(Integer compagnieNum);
}
