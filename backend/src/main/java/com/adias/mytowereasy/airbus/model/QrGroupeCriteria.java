package com.adias.mytowereasy.airbus.model;

import java.util.List;


public class QrGroupeCriteria // liste de fonctions
{
    private Integer id;

    private Integer code;

    private String libelle;

    private QrGroupeObject field;

    private List<QrGroupeValue> groupValues;

    private Boolean isAnd;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public QrGroupeObject getField() {
        return field;
    }

    public void setField(QrGroupeObject field) {
        this.field = field;
    }

    public List<QrGroupeValue> getGroupValues() {
        return groupValues;
    }

    public void setGroupValues(List<QrGroupeValue> groupValues) {
        this.groupValues = groupValues;
    }

    public Boolean getIsAnd() {
        return isAnd;
    }

    public void setIsAnd(Boolean isAnd) {
        this.isAnd = isAnd;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((field == null) ? 0 : field.hashCode());
		result = prime * result + ((groupValues == null) ? 0 : groupValues.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((isAnd == null) ? 0 : isAnd.hashCode());
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QrGroupeCriteria other = (QrGroupeCriteria) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (field == null) {
			if (other.field != null)
				return false;
		} else if (!field.equals(other.field))
			return false;
		if (groupValues == null) {
			if (other.groupValues != null)
				return false;
		} else if (!groupValues.equals(other.groupValues))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (isAnd == null) {
			if (other.isAnd != null)
				return false;
		} else if (!isAnd.equals(other.isAnd))
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		return true;
	}

}
