package com.adias.mytowereasy.airbus.service;

import java.util.List;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface EbTtCompanyPslService {
    List<EbTtCompanyPsl> listTtCompanyPsl(SearchCriteria criteria);

    Long countListTtCompanyPsl(SearchCriteria criteria);

    public EbTtCompanyPsl saveEbTtCompanyPsl(EbTtCompanyPsl ebTtCompanyPsl);

    public Boolean deleteEbTtCompanyPsl(Integer ebTtCompanyPslNum);

    List<EbTtSchemaPsl> getListSchemaPsl(SearchCriteria criteria);

    Long getListSchemaPslCount(SearchCriteria criteria);

    EbTtSchemaPsl updateSchemaPsl(EbTtSchemaPsl ebTtSchemaPsl);

    Boolean deleteSchemaPsl(Integer id);

    void generatePslSchemaPslByCompanyNum(List<Integer> listEbCompagnieNum) throws Exception;

    EbTtSchemaPsl getSchemaPsl(Integer ebTtCompanyPslNum);
}
