package com.adias.mytowereasy.airbus.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

import com.adias.mytowereasy.model.EcModule;


@Entity
@IdClass(ExUserProfileModuleId.class)
@Table(name = "eb_user_profile_module", schema = "work")
public class ExUserProfileModule implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Integer ecModuleNum;
    @Id
    private Integer ebUserProfileNum;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_module")
    @PrimaryKeyJoinColumn(name = "x_ec_module", referencedColumnName = "ec_module_num")
    private EcModule module;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "x_eb_user_profile")
    @PrimaryKeyJoinColumn(name = "x_eb_user_profile", referencedColumnName = "eb_user_profile_num")
    private EbUserProfile ebUserProfile;

    @Column(name = "access_right")
    private Integer accessRight;

    public ExUserProfileModule() {
    }

    public EcModule getModule() {
        return module;
    }

    public void setModule(EcModule module) {
        this.module = module;
    }

    public EbUserProfile getEbUserProfile() {
        return ebUserProfile;
    }

    public void setEbUserProfile(EbUserProfile ebUserProfile) {
        this.ebUserProfile = ebUserProfile;
    }

    public Integer getAccessRight() {
        return accessRight;
    }

    public void setAccessRight(Integer accessRight) {
        this.accessRight = accessRight;
    }

    public Integer getEcModuleNum() {
        return ecModuleNum;
    }

    public void setEcModuleNum(Integer ecModuleNum) {
        this.ecModuleNum = ecModuleNum;
    }

    public Integer getEbUserProfileNum() {
        return ebUserProfileNum;
    }

    public void setEbUserProfileNum(Integer ebUserProfileNum) {
        this.ebUserProfileNum = ebUserProfileNum;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }
}
