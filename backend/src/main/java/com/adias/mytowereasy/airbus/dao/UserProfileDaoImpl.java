package com.adias.mytowereasy.airbus.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.airbus.model.EbUserProfile;
import com.adias.mytowereasy.airbus.model.ExUserProfileModule;
import com.adias.mytowereasy.airbus.model.QEbUserProfile;
import com.adias.mytowereasy.airbus.repository.EbUserProfileRepository;
import com.adias.mytowereasy.airbus.repository.ExUserProfileModuleRepository;
import com.adias.mytowereasy.model.EcModule;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.QEbEtablissement;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Repository
@Transactional
public class UserProfileDaoImpl implements UserProfileDao {
    QEbUserProfile qEbUserProfile = QEbUserProfile.ebUserProfile;

    QEbEtablissement qEbEtablissement = QEbEtablissement.ebEtablissement;

    QEbCompagnie qEbCompagne = QEbCompagnie.ebCompagnie;

    @Autowired
    private EbUserProfileRepository ebUserProfileRepository;
    @Autowired
    private ExUserProfileModuleRepository exUserProfileModuleRepository;

    @PersistenceContext
    EntityManager em;

    @Override
    public EbUserProfile selectEbUserProfile(SearchCriteria criterias) {
        if (criterias == null) criterias = new SearchCriteria();
        BooleanBuilder where = new BooleanBuilder();

        if (criterias.getEbUserProfileNum() != null) {
            where.and(qEbUserProfile.ebUserProfileNum.eq(criterias.getEbUserProfileNum()));
        }

        Optional<EbUserProfile> ebUserProfile = ebUserProfileRepository.findOne(where);

        if (ebUserProfile.isPresent()) {

            if (criterias.isWithAccessRights()) {
                Set<ExUserProfileModule> listExUserProfileModule = this.exUserProfileModuleRepository
                    .findAllByEbUserProfileNum(ebUserProfile.get().getEbUserProfileNum());
                listExUserProfileModule.forEach(ex -> {
                    ex.setModule(new EcModule(ex.getEcModuleNum()));
                });
                ebUserProfile.get().setListAccessRights(listExUserProfileModule);
            }

            ebUserProfile.get().getListCategories();
        }

        return ebUserProfile.orElse(null);
    }

    @Override
    public List<EbUserProfile> getUserProfils(SearchCriteria criterias) {
        List<EbUserProfile> profils = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbUserProfile> query = new JPAQuery<EbUserProfile>(em);

            query
                .select(
                    QEbUserProfile
                        .create(
                            qEbUserProfile.ebUserProfileNum,
                            qEbUserProfile.nom,
                            qEbUserProfile.descriptif,
                            qEbUserProfile.listLabels,
                            qEbCompagne.ebCompagnieNum,
                            qEbCompagne.nom));
            query.from(qEbUserProfile);
            query.leftJoin(qEbUserProfile.ebCompagnie(), qEbCompagne);

            if (criterias.getEbCompagnieNum() != null) {
                where.and(qEbCompagne.ebCompagnieNum.eq(criterias.getEbCompagnieNum()));
            }

            if (criterias.getUserProfilNum() != null) // for access controle
                                                      // verification, to check
                                                      // if the connectedUser
                                                      // can show this user
                                                      // profile
                where.and(qEbUserProfile.ebUserProfileNum.eq(criterias.getUserProfilNum()));

            query.where(where);

            query.distinct();
            profils = (List<EbUserProfile>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return profils;
    }
}
