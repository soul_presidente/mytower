package com.adias.mytowereasy.airbus.dao;

import java.util.List;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoEbTtCompanyPsl {
    List<EbTtCompanyPsl> listTtCompanyPsl(SearchCriteria criteria);

    Long countListTtCompanyPsl(SearchCriteria criteria);

    List<EbTtSchemaPsl> getListSchemaPsl(SearchCriteria criteria);

    Long getListSchemaPslCount(SearchCriteria criteria);
}
