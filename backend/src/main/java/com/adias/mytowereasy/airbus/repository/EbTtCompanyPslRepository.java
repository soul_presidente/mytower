package com.adias.mytowereasy.airbus.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;


@Repository
public interface EbTtCompanyPslRepository extends JpaRepository<EbTtCompanyPsl, Integer> {
    List<EbTtCompanyPsl> findByEbCompagnieIsNull();

    @Query("SELECT new EbTtCompanyPsl(psl.ebTtCompanyPslNum, psl.codeAlpha, psl.libelle, psl.dateAttendue, psl.quantiteAttendue, psl.refDocumentAttendue, psl.ebCompagnie.ebCompagnieNum) FROM EbTtCompanyPsl psl WHERE psl.ebCompagnie.ebCompagnieNum IN (?1)")
    List<EbTtCompanyPsl> findByListEbCompagnieNum(List<Integer> listEbCompagnieNum);
}
