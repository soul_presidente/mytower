package com.adias.mytowereasy.airbus.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.controller.SuperControler;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/config-psl/")
public class EbTtCompanyPslController extends SuperControler {
    @RequestMapping(value = "list-track-trace-psl", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object>
        getListEbTtCompanyPslTable(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbTtCompanyPsl> ebTtCompanyPsl = ebTtCompanyPslService.listTtCompanyPsl(criteria);
        Long count = ebTtCompanyPslService.countListTtCompanyPsl(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", ebTtCompanyPsl);
        return result;
    }

    @PostMapping(value = "update-track-trace-psl")
    public EbTtCompanyPsl updateEbTtCompanyPsl(@RequestBody EbTtCompanyPsl ebTtCompanyPsl) {
        return ebTtCompanyPslService.saveEbTtCompanyPsl(ebTtCompanyPsl);
    }

    @DeleteMapping(value = "delete-track-trace-psl")
    public Boolean deleteEbTtCompanyPsl(@RequestParam Integer ebTtCompanyPslNum) {
        return ebTtCompanyPslService.deleteEbTtCompanyPsl(ebTtCompanyPslNum);
    }

    @PostMapping(value = "/list-schema-psl")
    public @ResponseBody Map<String, Object> getListSchemaPsl(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbTtSchemaPsl> data = ebTtCompanyPslService.getListSchemaPsl(criteria);
        Long count = ebTtCompanyPslService.getListSchemaPslCount(criteria);

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", data);

        return result;
    }

    @PostMapping(value = "update-schemapsl")
    public EbTtSchemaPsl updateSchemaPsl(@RequestBody EbTtSchemaPsl ebPlanTransport) {
        return ebTtCompanyPslService.updateSchemaPsl(ebPlanTransport);
    }

    @PostMapping(value = "delete-schemapsl")
    public @ResponseBody Boolean deleteSchemaPsl(@RequestBody Integer id) {
        return ebTtCompanyPslService.deleteSchemaPsl(id);
    }

    @PostMapping(value = "/search-schema-psl")
    public @ResponseBody List<EbTtSchemaPsl>
        searchListSchemaPsl(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbTtSchemaPsl> data = ebTtCompanyPslService.getListSchemaPsl(criteria);

        return data;
    }

    @PostMapping(value = "/search-list-psl")
    public @ResponseBody List<EbTtCompanyPsl> searchListPsl(@RequestBody() SearchCriteria criteria) {
        return ebTtCompanyPslService.listTtCompanyPsl(criteria);
    }
    
	@GetMapping
	public EbTtSchemaPsl getSchemaPsl(@RequestParam Integer ebTtCompanyPslNum) {
		return ebTtCompanyPslService.getSchemaPsl(ebTtCompanyPslNum);
	}
}
