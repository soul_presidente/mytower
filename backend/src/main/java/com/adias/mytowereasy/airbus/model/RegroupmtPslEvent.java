package com.adias.mytowereasy.airbus.model;

import java.util.Date;


public class RegroupmtPslEvent {
    private Integer ebTtPslNum;
    private String nom;
    private Date date;
    private String nature;
    private String coment;
    private int nbrDoc;
    private boolean isCourant = false;
    private int type;
    private Boolean validateByChamp;
    private Boolean validatePsl;

    // getters && setters

    public String getNom() {
        return nom;
    }

    public boolean isCourant() {
        return isCourant;
    }

    public void setCourant(boolean isCourant) {
        this.isCourant = isCourant;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Integer getEbTtPslNum() {
        return ebTtPslNum;
    }

    public void setEbTtPslNum(Integer ebTtPslNum) {
        this.ebTtPslNum = ebTtPslNum;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public String getComent() {
        return coment;
    }

    public void setComent(String coment) {
        this.coment = coment;
    }

    public int getNbrDoc() {
        return nbrDoc;
    }

    public void setNbrDoc(int nbrDoc) {
        this.nbrDoc = nbrDoc;
    }

    public Boolean isValidateByChamp() {
        return validateByChamp;
    }

    public void setValidateByChamp(Boolean validateByChamp) {
        this.validateByChamp = validateByChamp;
    }

    public Boolean getValidatePsl() {
        return validatePsl;
    }

    public void setValidatePsl(Boolean validatePsl) {
        this.validatePsl = validatePsl;
    }

    public Boolean getValidateByChamp() {
        return validateByChamp;
    }
}
