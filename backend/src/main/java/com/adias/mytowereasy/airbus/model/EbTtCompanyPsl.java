package com.adias.mytowereasy.airbus.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbTimeStamps;


@Entity
@Table(name = "eb_tt_company_psl", schema = "work", indexes = {
    @Index(name = "idx_eb_tt_company_psl_x_eb_company", columnList = "x_eb_company")
})
public class EbTtCompanyPsl extends EbTimeStamps implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebTtCompanyPslNum;

    private String libelle;

    private Boolean dateAttendue;

    private Boolean quantiteAttendue;

    private Boolean refDocumentAttendue;

    private String codeAlpha;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company", updatable = false)
    private EbCompagnie ebCompagnie;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Boolean schemaActif;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String segmentName;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer importance;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Boolean startPsl;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Boolean endPsl;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer order;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    // j'ai mis cette ligne en commentaire parceque sera causer un bug au niveau
    // de IAT a chaque refresh de la page les dates changes automatiquement
    // @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date expectedDate;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Long expectedDateTime;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Boolean isChecked;

    public EbTtCompanyPsl() {
    }

    public EbTtCompanyPsl(
        Integer ebTtCompanyPslNum,
        String codeAlpha,
        String libelle,
        Boolean dateAttendue,
        Boolean quantiteAttendue,
        Boolean refDocumentAttendue,
        Integer ebCompagnieNum) {
        this.ebTtCompanyPslNum = ebTtCompanyPslNum;
        this.codeAlpha = codeAlpha;
        this.libelle = libelle;
        this.dateAttendue = dateAttendue;
        this.quantiteAttendue = quantiteAttendue;
        this.refDocumentAttendue = refDocumentAttendue;
        this.ebCompagnie = new EbCompagnie();
        this.ebCompagnie.setEbCompagnieNum(ebCompagnieNum);
    }

    public EbTtCompanyPsl(EbTtCompanyPsl companyPsl) {
        this.ebTtCompanyPslNum = companyPsl.getEbTtCompanyPslNum();
        this.libelle = companyPsl.getLibelle();
        this.codeAlpha = companyPsl.getCodeAlpha();
        this.dateAttendue = companyPsl.getDateAttendue();
        this.quantiteAttendue = companyPsl.getQuantiteAttendue();
        this.refDocumentAttendue = companyPsl.getRefDocumentAttendue();
        this.setDateCreationSys(companyPsl.getDateCreationSys());
        if (companyPsl.getEbCompagnie() != null
            && companyPsl.getEbCompagnie().getEbCompagnieNum() != null) this.ebCompagnie = new EbCompagnie(
                companyPsl.getEbCompagnie().getEbCompagnieNum());
    }

    public EbTtCompanyPsl(
        Integer ebTtCompanyPslNum,
        String libelle,
        String codeAlpha,
        Boolean dateAttendue,
        Boolean quantiteAttendue,
        Boolean refDocumentAttendue,
        Timestamp date,
        EbCompagnie ebCompagnie) {
        this.ebTtCompanyPslNum = ebTtCompanyPslNum;
        this.libelle = libelle;
        this.codeAlpha = codeAlpha;
        this.dateAttendue = dateAttendue;
        this.quantiteAttendue = quantiteAttendue;
        this.refDocumentAttendue = refDocumentAttendue;
        this.setDateCreationSys(date);
        if (ebCompagnie != null && ebCompagnie
            .getEbCompagnieNum() != null) this.ebCompagnie = new EbCompagnie(ebCompagnie.getEbCompagnieNum());
    }

    @QueryProjection
    public EbTtCompanyPsl(
        Integer ebTtCompanyPslNum,
        String libelleEbTtCompanyPsl,
        Boolean dateEbTtCompanyPsl,
        Boolean quantite,
        Boolean refDocument,
        Integer ebCompangieNum,
        String ebCompagnieName,
        String ebCompagnieCode,
        // Timestamp dateCreation,
        // Timestamp dateMaj,
        String codeAlpha) {
        this.ebTtCompanyPslNum = ebTtCompanyPslNum;
        this.libelle = libelleEbTtCompanyPsl;
        this.dateAttendue = dateEbTtCompanyPsl;
        this.quantiteAttendue = quantite;
        this.refDocumentAttendue = refDocument;
        this.ebCompagnie = new EbCompagnie(ebCompangieNum, ebCompagnieName, ebCompagnieCode);
        // this.setDateCreationSys(dateCreation);
        // this.setDateMajSys(dateMaj);
        this.codeAlpha = codeAlpha;
    }

    public Integer getEbTtCompanyPslNum() {
        return ebTtCompanyPslNum;
    }

    public void setEbTtCompanyPslNum(Integer ebTtCompanyPslNum) {
        this.ebTtCompanyPslNum = ebTtCompanyPslNum;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean getDateAttendue() {
        return dateAttendue;
    }

    public void setDateAttendue(Boolean dateAttendue) {
        this.dateAttendue = dateAttendue;
    }

    public Boolean getQuantiteAttendue() {
        return quantiteAttendue;
    }

    public void setQuantiteAttendue(Boolean quantiteAttendue) {
        this.quantiteAttendue = quantiteAttendue;
    }

    public Boolean getRefDocumentAttendue() {
        return refDocumentAttendue;
    }

    public void setRefDocumentAttendue(Boolean refDocumentAttendue) {
        this.refDocumentAttendue = refDocumentAttendue;
    }

    public String getCodeAlpha() {
        return codeAlpha;
    }

    public void setCodeAlpha(String codeAlpha) {
        this.codeAlpha = codeAlpha;
    }

    public Boolean getSchemaActif() {
        return schemaActif;
    }

    public String getSegmentName() {
        return segmentName;
    }

    public Boolean getStartPsl() {
        return startPsl;
    }

    public Boolean getEndPsl() {
        return endPsl;
    }

    public Integer getOrder() {
        return order;
    }

    public void setSchemaActif(Boolean schemaActif) {
        this.schemaActif = schemaActif;
    }

    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    public void setStartPsl(Boolean startPsl) {
        this.startPsl = startPsl;
    }

    public void setEndPsl(Boolean endPsl) {
        this.endPsl = endPsl;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public Date getExpectedDate() {
        return expectedDate;
    }

    public void setExpectedDate(Date expectedDate) {
        this.expectedDate = expectedDate;
    }

    public Integer getImportance() {
        return importance;
    }

    public void setImportance(Integer importance) {
        this.importance = importance;
    }

    public Boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Boolean isChecked) {
        this.isChecked = isChecked;
    }

    public Long getExpectedDateTime() {
        return expectedDateTime;
    }

    public void setExpectedDateTime(Long expectedDateTime) {
        this.expectedDateTime = expectedDateTime;
    }
}
