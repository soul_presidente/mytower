package com.adias.mytowereasy.airbus.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.TtEnumeration.PslImportance;
import com.adias.mytowereasy.model.TtEnumeration.TtPsl;
import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.util.JsonDateDeserializer;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class EbTtCompanyPslServiceImp extends MyTowerService implements EbTtCompanyPslService {
    @Override
    public List<EbTtCompanyPsl> listTtCompanyPsl(SearchCriteria criteria) {
        List<EbTtCompanyPsl> listPsl = daoEbTtCompanyPsl.listTtCompanyPsl(criteria);
        listPsl.stream().filter(it -> Objects.isNull(it.getIsChecked())).forEach(it -> it.setIsChecked(true));
        return listPsl;
    }

    @Override
    public Long countListTtCompanyPsl(SearchCriteria criteria) {
        return daoEbTtCompanyPsl.countListTtCompanyPsl(criteria);
    }

    @Override
    public EbTtCompanyPsl saveEbTtCompanyPsl(EbTtCompanyPsl ebTtCompanyPsl) {
        if (ebTtCompanyPsl.getEbTtCompanyPslNum() == null) ebTtCompanyPsl
            .setEbCompagnie(connectedUserService.getCurrentUser().getEbCompagnie());

        if (ebTtCompanyPsl.getEbTtCompanyPslNum() != null) {
            // Mise à jour des schemas psl qui utilisent ce psl
            List<EbTtSchemaPsl> listSchema = ebTtSchemaPslRepository
                .selectByEbCompagnieNumAndPslNum(
                    ebTtCompanyPsl.getEbCompagnie().getEbCompagnieNum(),
                    ebTtCompanyPsl.getEbTtCompanyPslNum());
            listSchema.forEach(sch -> sch.setListPsl(sch.getListPsl().stream().map(psl -> {

                if (psl.getEbTtCompanyPslNum().intValue() == ebTtCompanyPsl.getEbTtCompanyPslNum().intValue()) {
                    psl.setLibelle(ebTtCompanyPsl.getLibelle());
                    psl.setCodeAlpha(ebTtCompanyPsl.getCodeAlpha());
                    psl.setDateAttendue(ebTtCompanyPsl.getDateAttendue());
                    psl.setQuantiteAttendue(ebTtCompanyPsl.getQuantiteAttendue());
                    psl.setRefDocumentAttendue(ebTtCompanyPsl.getRefDocumentAttendue());
                }

                return psl;
            }).collect(Collectors.toList())));
            ebTtSchemaPslRepository.saveAll(listSchema);
        }

        EbTtCompanyPsl psl = ebTtCompanyPslRepository.save(ebTtCompanyPsl);

        return psl;
    }

    @Override
    public Boolean deleteEbTtCompanyPsl(Integer ebTtCompanyPslNum) {
        ebTtCompanyPslRepository.deleteById(ebTtCompanyPslNum);
        return true;
    }

    @Override
    public List<EbTtSchemaPsl> getListSchemaPsl(SearchCriteria criteria) {
        return daoEbTtCompanyPsl.getListSchemaPsl(criteria);
    }

    @Override
    public Long getListSchemaPslCount(SearchCriteria criteria) {
        return daoEbTtCompanyPsl.getListSchemaPslCount(criteria);
    }

    @Override
    public EbTtSchemaPsl updateSchemaPsl(EbTtSchemaPsl ebTtSchemaPsl) {
        return ebTtSchemaPslRepository.save(ebTtSchemaPsl);
    }

    @Override
    public Boolean deleteSchemaPsl(Integer id) {
        EbTtSchemaPsl schema = ebTtSchemaPslRepository.getOne(id);
        schema.setIsDeleted(true);
        ebTtSchemaPslRepository.save(schema);
        return true;
    }

    @Override
    public void generatePslSchemaPslByCompanyNum(List<Integer> listEbCompagnieNum) throws Exception {
        // generer les psl standards
        List<EbTtCompanyPsl> listCompanyPsl = generateCompanyPsl(listEbCompagnieNum);

        // chercher les psl existants pour les compagnies dans la liste
        List<EbTtSchemaPsl> listExistingSchema = ebTtSchemaPslRepository.findByListEbCompagnieNum(listEbCompagnieNum);
        List<EbTtSchemaPsl> listStdSchema = new ArrayList();

        // creer les objets propres aux schemas de psl standards
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEP|ARR", ":1:2:", "CFR", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEP|ARR", ":1:2:", "CIF", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEP|ARR", ":2:1:", "CIP", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEL", ":3:", "CPT", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEP|ARR", ":2:1:", "CPT", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEL", ":4:3:", "DAP", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEP|ARR|DEL", ":2:1:", "DAP", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEP|ARR", ":2:1:", "DAT", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|CUS|DEL", ":4:3:", "DDP", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEP|ARR|CUS|DEL", ":1:2:", "DDP", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC", ":1:2:3:4:5:", "EXW (Export)", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEP|ARR|DEL", ":1:2:", "EXW (Import CPT/CFR)", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEL", ":3:4:", "EXW (Import DAP)", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEP|ARR|DEL", ":2:5:1:", "EXW (Import DAP)", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC", ":1:2:", "FAS", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC", ":1:", "FCA", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEL", ":3:", "FCA", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEP", ":2:1:", "FOB (Export)", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEP|ARR|DEL", ":2:1:", "FOB (Import DAP)", null));
        listStdSchema.add(new EbTtSchemaPsl("PIC|DEP|ARR|CUS|DEL", ":2:", "FOB (Import DDP)", null));

        try {

            for (Integer compagnieNum: listEbCompagnieNum) {
                // filter la liste des psl standards déjà existants
                List<EbTtSchemaPsl> listExistingCompagnieSchemaPsl = listExistingSchema
                    .stream().filter(it -> it.getxEbCompagnie().getEbCompagnieNum().equals(compagnieNum))
                    .collect(Collectors.toList());

                List<EbTtSchemaPsl> listSchema = new ArrayList();

                for (EbTtSchemaPsl stdSchema: listStdSchema) {
                    // vérifier si le schema standard n'est pas encore créé
                    Boolean exists = false;

                    for (EbTtSchemaPsl compSchema: listExistingCompagnieSchemaPsl) {

                        if (stdSchema.getListModeTransport() != null
                            && stdSchema.getListModeTransport().contentEquals(compSchema.getListModeTransport())
                            && stdSchema.getxEcIncotermLibelle() != null
                            && stdSchema.getxEcIncotermLibelle().contentEquals(compSchema.getxEcIncotermLibelle())) {
                            exists = true;
                        }

                    }

                    // créer le schema s'il n'existe pas encore en base
                    if (!exists) {
                        EbTtSchemaPsl newSchema = new EbTtSchemaPsl(stdSchema);
                        newSchema.setxEbCompagnie(new EbCompagnie(compagnieNum));
                        newSchema.setListEtablissement(":-1:");
                        newSchema.setIsEditable(true);
                        newSchema.setEbTtSchemaPslNum(null);
                        newSchema.setListPsl(new ArrayList<EbTtCompanyPsl>());

                        // se baser sur le champs configPsl pour générer la
                        // configuration du schema psl
                        String[] listConfig = newSchema.getConfigPsl().split("[|]");
                        int i = 1;

                        for (String cfg: listConfig) {
                            EbTtCompanyPsl psl = listCompanyPsl
                                .stream()
                                .filter(
                                    it -> it.getCodeAlpha().equals(cfg)
                                        && it.getEbCompagnie().getEbCompagnieNum().equals(compagnieNum))
                                .findFirst().orElse(null);

                            if (psl != null) {
                                EbTtCompanyPsl cpsl = new EbTtCompanyPsl(psl);
                                cpsl.setSchemaActif(true);
                                cpsl.setIsChecked(true);
                                cpsl.setOrder(i);
                                cpsl.setEbCompagnie(null);
                                cpsl.setImportance(PslImportance.MAJEUR.getCode());
                                if (i == 1) cpsl.setStartPsl(true);
                                i++;

                                newSchema.getListPsl().add(cpsl);
                            }

                        }

                        if (!newSchema.getListPsl().isEmpty()) newSchema
                            .getListPsl().get(newSchema.getListPsl().size() - 1).setEndPsl(true);

                        listSchema.add(newSchema);
                    }

                }

                if (!listSchema.isEmpty()) ebTtSchemaPslRepository.saveAll(listSchema);
                listSchema.addAll(listExistingCompagnieSchemaPsl);

                int i = 0;
                List<Integer> listDemandeNum = new ArrayList<Integer>();
                List<EbDemande> listEbDemande = ebDemandeRepository.findByXEbCompagnie_ebCompagnieNum(compagnieNum);

                // reprise de données pour les quotes
                listDemandeNum = new ArrayList<Integer>();

                for (EbDemande d: listEbDemande) {
                    // chercher le schema adapté à la demande selon l'incoterme
                    // et le mode de transport
                    EbTtSchemaPsl schema = listSchema.stream().filter(it -> {
                        boolean binc = (it.getxEcIncotermLibelle() != null && d.getxEcIncotermLibelle() != null) ?
                            it.getxEcIncotermLibelle().contentEquals(d.getxEcIncotermLibelle()) :
                            false;
                        boolean btr = it.getListModeTransport().indexOf(":" + d.getxEcModeTransport() + ":") >= 0;

                        return binc && btr;
                    }).findFirst().orElse(null);

                    // mise à jour des champs de la demande propres au psl pour
                    // l'adapter à la nouvelle config du schema psl
                    if (schema != null) {
                        EbTtSchemaPsl newSchema = new EbTtSchemaPsl(schema);
                        newSchema.setxEbCompagnie(null);
                        d.setxEbSchemaPsl(newSchema);

                        EbTtCompanyPsl psl = d
                            .getxEbSchemaPsl().getListPsl().stream()
                            .filter(it -> it.getEndPsl() != null && it.getEndPsl()).findFirst().orElse(null);

                        if (psl != null) {
                            d.setCodeAlphaLastPsl(psl.getCodeAlpha());
                            d.setCodeLastPsl(psl.getEbTtCompanyPslNum());
                            d.setLibelleLastPsl(psl.getLibelle());
                        }

                        if (d.getxEcStatut() >= StatutDemande.WPU.getCode()) {
                            ObjectMapper mapper = new ObjectMapper();
                            String json = mapper.writeValueAsString(d.getxEbSchemaPsl().getListPsl());
                            Gson gson = new GsonBuilder()
                                .registerTypeAdapter(Date.class, new JsonDateDeserializer()).create();
                            List<EbTtCompanyPsl> listPsl = gson.fromJson(json, new TypeToken<List<EbTtCompanyPsl>>() {
                            }.getType());
                            d.getxEbSchemaPsl().setListPsl(listPsl);
                            newSchema.setListPsl(listPsl);

                            if (d.getDatePickupTM() != null) {
                                psl = d
                                    .getxEbSchemaPsl().getListPsl().stream()
                                    .filter(
                                        it -> it.getCodeAlpha().contentEquals(TtPsl.PICKUP.getCodeAlpha())
                                            && it.getIsChecked() != null && it.getIsChecked())
                                    .findFirst().orElse(null);
                                if (psl != null) psl.setExpectedDate(d.getDatePickupTM());
                            }

                            if (d.getFinalDelivery() != null) {
                                psl = d
                                    .getxEbSchemaPsl().getListPsl().stream()
                                    .filter(
                                        it -> it.getCodeAlpha().contentEquals(TtPsl.DELIVERY.getCodeAlpha())
                                            && it.getIsChecked() != null && it.getIsChecked())
                                    .findFirst().orElse(null);
                                if (psl != null) psl.setExpectedDate(d.getFinalDelivery());
                            }

                            if (d.getEtd() != null) {
                                psl = d
                                    .getxEbSchemaPsl().getListPsl().stream()
                                    .filter(
                                        it -> it.getCodeAlpha().contentEquals(TtPsl.DEPARTURE.getCodeAlpha())
                                            && it.getIsChecked() != null && it.getIsChecked())
                                    .findFirst().orElse(null);
                                if (psl != null) psl.setExpectedDate(d.getEtd());
                            }

                            if (d.getEta() != null) {
                                psl = d
                                    .getxEbSchemaPsl().getListPsl().stream()
                                    .filter(
                                        it -> it.getCodeAlpha().contentEquals(TtPsl.ARRIVAL.getCodeAlpha())
                                            && it.getIsChecked() != null && it.getIsChecked())
                                    .findFirst().orElse(null);
                                if (psl != null) psl.setExpectedDate(d.getEta());
                            }

                        }

                        if (d.getxEcStatut() >= StatutDemande.WPU.getCode()) listDemandeNum.add(d.getEbDemandeNum());
                    }
                    else {
                        System.err
                            .println(
                                "SCHEMA NOT FOUND / DEMANDE: " + d.getEbDemandeNum() + " - MODE TRANSPORT: "
                                    + d.getxEcModeTransport() + " - INCOTERM: " + d.getxEcIncotermLibelle());

                        continue;
                    }

                }

                // reprise de données pour les tracings/psl

                // mise à jour des champs du Tracing propres au psl pour
                // l'adapter à la nouvelle config du schema psl
                if (!listDemandeNum.isEmpty()) {
                    List<EbTtTracing> listTracing = ebTrackTraceRepository
                        .findByxEbDemande_ebDemandeNumIn(listDemandeNum);
                    List<EbTTPslApp> listPslApp = new ArrayList<EbTTPslApp>();

                    for (EbTtTracing tt: listTracing) {
                        EbDemande d = listEbDemande
                            .stream().filter(it -> it.getEbDemandeNum().equals(tt.getxEbDemande().getEbDemandeNum()))
                            .findFirst().orElse(null);
                        List<EbTtCompanyPsl> listPsl = d.getxEbSchemaPsl().getListPsl();

                        tt.setxEcIncotermLibelle(d.getxEcIncotermLibelle());

                        if (d != null) {
                            tt.setxEcIncotermLibelle(d.getxEcIncotermLibelle());
                            EbTtCompanyPsl psl = null;

                            if (tt.getCodeAlphaPslCourant() != null) {
                                psl = listPsl
                                    .stream().filter(it -> it.getCodeAlpha().contentEquals(tt.getCodeAlphaPslCourant()))
                                    .findFirst().orElse(null);

                                if (psl != null) {
                                    tt.setLibellePslCourant(psl.getLibelle());
                                }

                            }

                            tt.setCodeAlphaLastPsl(d.getCodeAlphaLastPsl());
                            tt.setCodeLastPsl(d.getCodeLastPsl());
                            tt.setLibelleLastPsl(d.getLibelleLastPsl());

                            for (EbTTPslApp pslApp: tt.getListEbPslApp()) {
                                psl = listPsl
                                    .stream().filter(it -> it.getCodeAlpha().contentEquals(pslApp.getCodeAlpha()))
                                    .findFirst().orElse(null);

                                pslApp.setxEbTrackTrace(new EbTtTracing(tt.getEbTtTracingNum()));

                                if (psl != null) {
                                    pslApp.setCompanyPsl(psl);
                                    pslApp.setOrderpsl(psl.getOrder());
                                    pslApp.setLibelle(psl.getLibelle());
                                    listPslApp.add(pslApp);
                                }

                                // MAJ du TrackTrace par la 'date end PSL'
                                if (pslApp.getCompanyPsl() != null && pslApp.getCompanyPsl().getEndPsl() != null
                                    && pslApp.getCompanyPsl().getEndPsl()) {
                                    tt.setDateLastPsl(pslApp.getDateActuelle());
                                }

                            }

                            tt.setListEbPslApp(null);
                        }

                    }

                    ebTrackTraceRepository.saveAll(listTracing);
                    ebPslAppRepository.saveAll(listPslApp);
                }

                ebDemandeRepository.saveAll(listEbDemande);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    private List<EbTtCompanyPsl> generateCompanyPsl(List<Integer> listEbCompagnieNum) {
        List<EbTtCompanyPsl> listExistingPsl = ebTtCompanyPslRepository.findByListEbCompagnieNum(listEbCompagnieNum);
        List<EbTtCompanyPsl> listStandardCompanyPsl = new ArrayList<EbTtCompanyPsl>();
        listStandardCompanyPsl.add(new EbTtCompanyPsl(null, "Pickup", "PIC", true, false, false, null, null));
        listStandardCompanyPsl.add(new EbTtCompanyPsl(null, "Departure", "DEP", true, false, false, null, null));
        listStandardCompanyPsl.add(new EbTtCompanyPsl(null, "Arrival", "ARR", true, false, false, null, null));
        listStandardCompanyPsl.add(new EbTtCompanyPsl(null, "Custom", "CUS", false, false, false, null, null));
        listStandardCompanyPsl.add(new EbTtCompanyPsl(null, "Delivery", "DEL", true, false, false, null, null));

        List<EbTtCompanyPsl> listCompanyPsl = new ArrayList<EbTtCompanyPsl>();

        for (Integer compagnieNum: listEbCompagnieNum) {
            List<EbTtCompanyPsl> listExistingCompagniePsl = listExistingPsl
                .stream().filter(it -> it.getEbCompagnie().getEbCompagnieNum().equals(compagnieNum))
                .collect(Collectors.toList());

            for (EbTtCompanyPsl stdPsl: listStandardCompanyPsl) {
                Boolean exists = false;

                for (EbTtCompanyPsl compPsl: listExistingCompagniePsl) {

                    if (stdPsl.getCodeAlpha().contentEquals(compPsl.getCodeAlpha())) {
                        exists = true;
                    }

                }

                if (!exists) {
                    EbTtCompanyPsl newPsl = new EbTtCompanyPsl(stdPsl);
                    newPsl.setEbCompagnie(new EbCompagnie(compagnieNum));
                    listCompanyPsl.add(newPsl);
                }

            }

        }

        if (!listCompanyPsl.isEmpty()) ebTtCompanyPslRepository.saveAll(listCompanyPsl);

        listCompanyPsl.addAll(listExistingPsl);

        return listCompanyPsl;
    }

	@Override
	public EbTtSchemaPsl getSchemaPsl(Integer ebTtCompanyPslNum) {
		return ebTtSchemaPslRepository.findById(ebTtCompanyPslNum).orElseThrow(() -> new IllegalArgumentException(
				String.format("Schema PSL with id %s not found", ebTtCompanyPslNum)));
	}
}
