package com.adias.mytowereasy.airbus.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbCompagnie;


@Entity
@Table(name = "eb_user_profile", schema = "work")
public class EbUserProfile implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebUserProfileNum;

    @Column(unique = true)
    private String nom;

    @Column(columnDefinition = "TEXT")
    private String descriptif;

    @Column
    private String listLabels;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCategorie> listCategories;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "ebUserProfile")
    private Set<ExUserProfileModule> listAccessRights;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie")
    private EbCompagnie ebCompagnie;

    public EbUserProfile() {
    }

    public EbUserProfile(Integer ebUserProfileNum) {
        this.ebUserProfileNum = ebUserProfileNum;
    }

    public EbUserProfile(Integer ebUserProfileNum, String nom, String descriptif) {
        this.nom = nom;
        this.ebUserProfileNum = ebUserProfileNum;
        this.descriptif = descriptif;
    }

    @QueryProjection
    public EbUserProfile(
        Integer ebUserProfileNum,
        String nom,
        String descriptif,
        String listLabels,
        Integer ebCompagnieNum,
        String ebCompagnieNom) {
        this.ebUserProfileNum = ebUserProfileNum;
        this.nom = nom;
        this.descriptif = descriptif;
        this.listLabels = listLabels;

        if (ebCompagnieNum != null) {
            this.ebCompagnie = new EbCompagnie();
            this.ebCompagnie.setEbCompagnieNum(ebCompagnieNum);
            this.ebCompagnie.setNom(ebCompagnieNom);
        }

    }

    public String getListLabels() {
        return listLabels;
    }

    public void setListLabels(String listLabels) {
        this.listLabels = listLabels;
    }

    public Integer getEbUserProfileNum() {
        return ebUserProfileNum;
    }

    public void setEbUserProfileNum(Integer ebUserProfileNum) {
        this.ebUserProfileNum = ebUserProfileNum;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDescriptif() {
        return descriptif;
    }

    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }

    public List<EbCategorie> getListCategories() {
        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {
        this.listCategories = listCategories;
    }

    public Set<ExUserProfileModule> getListAccessRights() {
        return listAccessRights;
    }

    public void setListAccessRights(Set<ExUserProfileModule> listAccessRights) {
        this.listAccessRights = listAccessRights;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebcompagnie) {
        this.ebCompagnie = ebcompagnie;
    }
}
