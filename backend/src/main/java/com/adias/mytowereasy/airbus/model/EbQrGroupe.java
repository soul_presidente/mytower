package com.adias.mytowereasy.airbus.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.util.ObjectDeserializer;


@NamedEntityGraphs({
    @NamedEntityGraph(
        name = "graph.EbQrGroupe.listPropositions",
        attributeNodes = @NamedAttributeNode("listPropositions")),
})
@JsonIgnoreProperties({
    "isChecked",
})
@Entity
@Table(name = "eb_qr_groupe", uniqueConstraints = {
    @UniqueConstraint(columnNames = {
        "libelle", "x_eb_compagnie"
    })
})
@DynamicUpdate
public class EbQrGroupe {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebQrGroupeNum;

    private String libelle;

    private Integer grpOrder;

    private Boolean isDeleted;
    private Boolean isActive = true;

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", updatable = false)
    private EbCompagnie xEbCompagnie;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<QrGroupeCriteria> compatibilityCriteria;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<QrGroupeCriteria> exclusionCriteria;

    private String triggers;
    private Integer ventilation;
    private Integer output;

    @Column(columnDefinition = "TEXT")
    private String description;

    @JsonManagedReference
		@OneToMany(fetch = FetchType.LAZY, mappedBy = "ebQrGroupe")
		@JsonSerialize
    private List<EbQrGroupeProposition> listPropositions;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Boolean updatePropositions;

    public EbQrGroupe() {
    }

    public EbQrGroupe(Integer ebQrGroupeNum) {
        this.ebQrGroupeNum = ebQrGroupeNum;
    }

    public EbQrGroupe(EbQrGroupe ebQrGroupe, EbQrGroupeProposition propo) {
        this.ebQrGroupeNum = ebQrGroupe.getEbQrGroupeNum();
        this.libelle = ebQrGroupe.getLibelle();
        this.grpOrder = ebQrGroupe.getGrpOrder();
        this.isDeleted = ebQrGroupe.getIsDeleted();
        this.xEbCompagnie = ebQrGroupe.getxEbCompagnie();
        this.compatibilityCriteria = ebQrGroupe.getCompatibilityCriteria();
        this.exclusionCriteria = ebQrGroupe.getExclusionCriteria();
        this.triggers = ebQrGroupe.getTriggers();
        this.ventilation = ebQrGroupe.getVentilation();
        this.output = ebQrGroupe.getOutput();
        this.description = ebQrGroupe.getDescription();
        this.listPropositions = new ArrayList<>();
        this.listPropositions.add(propo);
    }

		@QueryProjection
		public EbQrGroupe(
			Integer ebQrGroupeNum,
			String libelle,
			Integer grpOrder,
			Boolean isDeleted,
			Boolean isActive,
			EbCompagnie xEbCompagnie,
			List<QrGroupeCriteria> compatibilityCriteria,
			List<QrGroupeCriteria> exclusionCriteria,
			String triggers,
			Integer ventilation,
			Integer output,
			String description
		)
		{
			this.ebQrGroupeNum = ebQrGroupeNum;
			this.libelle = libelle;
			this.grpOrder = grpOrder;
			this.isDeleted = isDeleted;
			this.isActive = isActive;
			this.xEbCompagnie = xEbCompagnie;
			this.compatibilityCriteria = compatibilityCriteria;
			this.exclusionCriteria = exclusionCriteria;
			this.triggers = triggers;
			this.ventilation = ventilation;
			this.output = output;
			this.description = description;
		}

    public Integer getEbQrGroupeNum() {
        return ebQrGroupeNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setEbQrGroupeNum(Integer ebQrGroupeNum) {
        this.ebQrGroupeNum = ebQrGroupeNum;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getTriggers() {
        return triggers;
    }

    public void setTriggers(String triggers) {
        this.triggers = triggers;
    }

    public Integer getVentilation() {
        return ventilation;
    }

    public Integer getOutput() {
        return output;
    }

    public void setVentilation(Integer ventilation) {
        this.ventilation = ventilation;
    }

    public void setOutput(Integer output) {
        this.output = output;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<QrGroupeCriteria> getCompatibilityCriteria() {
        compatibilityCriteria = ObjectDeserializer
            .checkJsonListObject(compatibilityCriteria, new TypeToken<List<QrGroupeCriteria>>() {
            }.getType());
        return compatibilityCriteria;
    }

    public void setCompatibilityCriteria(List<QrGroupeCriteria> compatibilityCriteria) {
        this.compatibilityCriteria = ObjectDeserializer
            .checkJsonListObject(compatibilityCriteria, new TypeToken<List<QrGroupeCriteria>>() {
            }.getType());
    }

    public List<QrGroupeCriteria> getExclusionCriteria() {
        compatibilityCriteria = ObjectDeserializer
            .checkJsonListObject(compatibilityCriteria, new TypeToken<List<QrGroupeCriteria>>() {
            }.getType());
        return exclusionCriteria;
    }

    public void setExclusionCriteria(List<QrGroupeCriteria> exclusionCriteria) {
        this.compatibilityCriteria = ObjectDeserializer
            .checkJsonListObject(compatibilityCriteria, new TypeToken<List<QrGroupeCriteria>>() {
            }.getType());
    }

    public List<EbQrGroupeProposition> getListPropositions() {
        listPropositions = ObjectDeserializer
            .checkJsonListObject(listPropositions, new TypeToken<List<QrGroupeCriteria>>() {
            }.getType());
        return listPropositions;
    }

    public void setListPropositions(List<EbQrGroupeProposition> listPropositions) {
        if (listPropositions != null
            && listPropositions.isEmpty()) this.listPropositions = new ArrayList<EbQrGroupeProposition>();
        else if (listPropositions != null && !listPropositions.isEmpty()) {
            this.listPropositions = ObjectDeserializer
                .checkJsonListObject(listPropositions, new TypeToken<List<EbQrGroupeProposition>>() {
                }.getType());
        }
        else {
            this.listPropositions = listPropositions;
        }
    }

    public Integer getGrpOrder() {
        return grpOrder;
    }

    public void setGrpOrder(Integer grpOrder) {
        this.grpOrder = grpOrder;
    }

    public Boolean getUpdatePropositions() {
        return updatePropositions;
    }

    public void setUpdatePropositions(Boolean updatePropositions) {
        this.updatePropositions = updatePropositions;
    }
}
