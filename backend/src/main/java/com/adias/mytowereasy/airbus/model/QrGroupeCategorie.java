package com.adias.mytowereasy.airbus.model;

import java.util.ArrayList;
import java.util.List;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbLabel;


public class QrGroupeCategorie {
    private QrGroupeObject value;
    private List<QrGroupeObject> labels;

    public QrGroupeCategorie() {
    }

    public QrGroupeCategorie(EbCategorie categorie) {
        this.value = new QrGroupeObject(categorie.getEbCategorieNum(), categorie.getLibelle());

        if (categorie.getLabels() != null && !categorie.getLabels().isEmpty()) {
            this.labels = new ArrayList<QrGroupeObject>();

            for (EbLabel label: categorie.getLabels()) {
                this.labels.add(new QrGroupeObject(label.getEbLabelNum(), label.getLibelle()));
            }

        }

    }

    public QrGroupeObject getValue() {
        return value;
    }

    public void setValue(QrGroupeObject value) {
        this.value = value;
    }

    public List<QrGroupeObject> getLabels() {
        return labels;
    }

    public void setLabels(List<QrGroupeObject> labels) {
        this.labels = labels;
    }
}
