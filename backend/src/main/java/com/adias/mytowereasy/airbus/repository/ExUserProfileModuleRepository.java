package com.adias.mytowereasy.airbus.repository;

import java.util.Set;

import com.adias.mytowereasy.airbus.model.ExUserProfileModule;
import com.adias.mytowereasy.repository.CommonRepository;


public interface ExUserProfileModuleRepository extends CommonRepository<ExUserProfileModule, Integer> {
    public Set<ExUserProfileModule> findAllByEbUserProfileNum(Integer ebUserProfileNum);
}
