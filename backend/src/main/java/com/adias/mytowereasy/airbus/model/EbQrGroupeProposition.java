package com.adias.mytowereasy.airbus.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.adias.mytowereasy.model.EbDemande;


@Entity
@Table(name = "eb_qr_groupe_proposition")
public class EbQrGroupeProposition {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebQrGroupePropositionNum;

    private Integer statut;

    private String listDemandeNum; // :a::b::c::d::e: ...

    private String listSelectedDemandeNum; // :a::b::c::d::e: ...

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_demande", nullable = false, unique = true)
    private EbDemande ebDemandeResult;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbDemande> listEbDemande;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_qr_groupe", insertable = true, updatable = false)
    private EbQrGroupe ebQrGroupe;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer nombreDemandes;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer nombrePages;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer currentPage;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer nombreDemandesPerPage;

    public EbQrGroupeProposition(Integer statut, String listDemandeNum, EbQrGroupe ebQrGroupe) {
        super();
        this.statut = statut;
        this.listDemandeNum = listDemandeNum;
        this.ebQrGroupe = ebQrGroupe;
    }

    public EbQrGroupeProposition() {
        super();
    }

    public Integer getStatut() {
        return statut;
    }

    public void setStatut(Integer statut) {
        this.statut = statut;
    }

    public String getListDemandeNum() {
        return listDemandeNum;
    }

    public void setListDemandeNum(String listDemandeNum) {
        this.listDemandeNum = listDemandeNum;
    }

    public List<EbDemande> getListEbDemande() {
        return listEbDemande;
    }

    public void setListEbDemande(List<EbDemande> listEbDemande) {
        this.listEbDemande = listEbDemande;
    }

    public EbDemande getEbDemandeResult() {
        return ebDemandeResult;
    }

    public void setEbDemandeResult(EbDemande ebDemandeResult) {
        this.ebDemandeResult = ebDemandeResult;
    }

    public String getListSelectedDemandeNum() {
        return listSelectedDemandeNum;
    }

    public void setListSelectedDemandeNum(String listSelectedDemandeNum) {
        this.listSelectedDemandeNum = listSelectedDemandeNum;
    }

    public Integer getEbQrGroupePropositionNum() {
        return ebQrGroupePropositionNum;
    }

    public void setEbQrGroupePropositionNum(Integer ebQrGroupePropositionNum) {
        this.ebQrGroupePropositionNum = ebQrGroupePropositionNum;
    }

    public EbQrGroupe getEbQrGroupe() {
        return ebQrGroupe;
    }

    public void setEbQrGroupe(EbQrGroupe ebQrGroupe) {
        this.ebQrGroupe = ebQrGroupe;
    }

    public Integer getNombreDemandes() {
        return nombreDemandes;
    }

    public void setNombreDemandes(Integer nombreDemandes) {
        this.nombreDemandes = nombreDemandes;
    }

    public Integer getNombrePages() {
        return nombrePages;
    }

    public void setNombrePages(Integer nombrePages) {
        this.nombrePages = nombrePages;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getNombreDemandesPerPage() {
        return nombreDemandesPerPage;
    }

    public void setNombreDemandesPerPage(Integer nombreDemandesPerPage) {
        this.nombreDemandesPerPage = nombreDemandesPerPage;
    }
}
