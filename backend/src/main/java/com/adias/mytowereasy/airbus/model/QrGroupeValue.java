package com.adias.mytowereasy.airbus.model;

public class QrGroupeValue {
    private QrGroupeObject value;

    private QrGroupeObject categorieValue;

    private QrGroupeObject sousFunction;

    public QrGroupeObject getSousFunction() {
        return sousFunction;
    }

    public void setSousFunction(QrGroupeObject sousFunction) {
        this.sousFunction = sousFunction;
    }

    public QrGroupeObject getValue() {
        return value;
    }

    public void setValue(QrGroupeObject value) {
        this.value = value;
    }

    public QrGroupeObject getCategorieValue() {
        return categorieValue;
    }

    public void setCategorieValue(QrGroupeObject categorieValue) {
        this.categorieValue = categorieValue;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((categorieValue == null) ? 0 : categorieValue.hashCode());
		result = prime * result + ((sousFunction == null) ? 0 : sousFunction.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QrGroupeValue other = (QrGroupeValue) obj;
		if (categorieValue == null) {
			if (other.categorieValue != null)
				return false;
		} else if (!categorieValue.equals(other.categorieValue))
			return false;
		if (sousFunction == null) {
			if (other.sousFunction != null)
				return false;
		} else if (!sousFunction.equals(other.sousFunction))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
