package com.adias.mytowereasy.airbus.repository;

import com.adias.mytowereasy.airbus.model.EbUserProfile;
import com.adias.mytowereasy.repository.CommonRepository;


public interface EbUserProfileRepository extends CommonRepository<EbUserProfile, Integer> {
    public boolean existsByNom(String nom);
}
