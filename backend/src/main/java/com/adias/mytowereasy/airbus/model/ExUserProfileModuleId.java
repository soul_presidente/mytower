package com.adias.mytowereasy.airbus.model;

import java.io.Serializable;

import javax.persistence.Embeddable;


@Embeddable
public class ExUserProfileModuleId implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer ecModuleNum;

    private Integer ebUserProfileNum;
}
