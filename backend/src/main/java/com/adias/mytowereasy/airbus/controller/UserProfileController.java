package com.adias.mytowereasy.airbus.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.airbus.model.EbUserProfile;
import com.adias.mytowereasy.airbus.service.UserProfileService;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.UserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/user-profile/")
public class UserProfileController {
    @Autowired
    private UserProfileService userProfileService;

    @Autowired
    private UserService userService;

    @PostMapping(value = "get-all")
    public Map<String, Object> getListUserWithPagination(@RequestBody SearchCriteria criteria) {
        Map<String, Object> result = new HashMap<String, Object>();

        List<EbUserProfile> list = userProfileService.getAll();
        result.put("data", list);
        result.put("count", list.size());
        return result;
    }

    @GetMapping(value = "get-all-profile")
    public List<EbUserProfile> getListUserProfile() {
        return userProfileService.getAll();
    }

    @PostMapping(value = "update-user-profile")
    public EbUserProfile updateUserGroup(@RequestBody EbUserProfile ebUserProfile) {
        return userProfileService.updateUserDetails(ebUserProfile);
    }

    @PostMapping(value = "/get-user-profile")
    @ResponseBody
    public EbUserProfile getDetailUserProfile(@RequestBody SearchCriteria criterias) {
        return userProfileService.getDetailUserProfile(criterias);
    }

    @PostMapping(value = "/nom-exist")
    public boolean checkEmail(@RequestBody String nom) {
        return userProfileService.checkUserProfileByNom(nom);
    }

    @PostMapping(value = "/delete-user-profile")
    public boolean deleteUserProfile(@RequestBody Integer id) {
        return userProfileService.deleteUserProfile(id);
    }

    @PostMapping(value = "get-users-by-profile")
    public Map<String, Object> getListUser(@RequestBody SearchCriteria criteria) {
        Map<String, Object> result = new HashMap<String, Object>();
        List<EbUser> list = userService.getByProfile(criteria.getEbUserProfileNum());
        result.put("data", list);
        result.put("count", list.size());
        return result;
    }
}
