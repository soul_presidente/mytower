package com.adias.mytowereasy.airbus.service;

import java.util.List;

import com.adias.mytowereasy.airbus.model.EbUserProfile;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface UserProfileService {
    public EbUserProfile updateUserDetails(EbUserProfile ebUserProfile);

    public EbUserProfile getDetailUserProfile(SearchCriteria criterias);

    public boolean checkUserProfileByNom(String nom);

    public boolean deleteUserProfile(Integer id);

    public List<EbUserProfile> getAll();
}
