package com.adias.mytowereasy.airbus.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;
import com.adias.mytowereasy.airbus.model.EbQrGroupeProposition;
import com.adias.mytowereasy.repository.CommonRepository;


@Repository
public interface EbQrGroupePropositionRepository extends CommonRepository<EbQrGroupeProposition, Integer> {
    @Transactional
    @Modifying
    @Query("DELETE FROM EbQrGroupeProposition propo WHERE propo.ebQrGroupePropositionNum IN (?1)")
    public Integer deleteListObject(List<Integer> listNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbQrGroupeProposition propo WHERE propo.listDemandeNum  =:listDemandeNum")
    void deleteTrinitial(@Param("listDemandeNum") String listDemandeNum);

    @Transactional
    @Modifying
    @Query("DELETE FROM EbQrGroupeProposition propo WHERE propo.statut = 3 AND propo.ebQrGroupe.ebQrGroupeNum IN (?1)")
    public Integer deleteWaitingPropositionByGroup(List<Integer> listEbQrGroupNum);

    @Query("SELECT propo.listDemandeNum FROM EbQrGroupeProposition propo WHERE  propo.ebDemandeResult.ebDemandeNum = :xEbDemandeNum")
    public String findEbQrGroupePropositionByEbDemandeNum(@Param("xEbDemandeNum") Integer xEbDemandeNum);

    public EbQrGroupeProposition findFirstByListDemandeNumContains(String xEbDemandeNum);

    @Query("SELECT propo.ebDemandeResult.ebDemandeNum FROM EbQrGroupeProposition propo WHERE propo.statut = 3 AND propo.ebQrGroupe.ebQrGroupeNum IN (?1)")
	Set<Integer> findEbQrGroupePropositionByebQrGroupe(List<Integer> listEbQrGroupe);

    @Query("SELECT new EbQrGroupe(gr,p)  FROM EbQrGroupeProposition p  "
        + "INNER JOIN EbQrGroupe gr ON gr.ebQrGroupeNum = p.ebQrGroupe.ebQrGroupeNum "
        + "where p.ebQrGroupePropositionNum = (SELECT MAX(prop.ebQrGroupePropositionNum) FROM EbQrGroupeProposition prop where prop.ebDemandeResult.ebDemandeNum = :xEbDemandeNum)")
     EbQrGroupe getEbQrGroupeWithPropositionByEbDemandeResult(@Param("xEbDemandeNum") Integer xEbDemandeNum);

     EbQrGroupeProposition getOneByEbDemandeResult_ebDemandeNum(Integer ebDemandeNum);

    @Transactional
    @Modifying
    @Query("update EbQrGroupeProposition prop set prop.listSelectedDemandeNum = :listSelectedDemandeNum, prop.listDemandeNum = :listDemandeNum where prop.ebQrGroupePropositionNum  = :ebQrGroupePropositionNum")
     int updateListSelectedDemandeNumByEbQrGroupePropositionNum(
        @Param("listSelectedDemandeNum") String listSelectedDemandeNum,
        @Param("ebQrGroupePropositionNum") Integer ebQrGroupePropositionNum,
        @Param("listDemandeNum") String listDemandeNum);

    @Query("SELECT propo.ebDemandeResult.ebDemandeNum FROM EbQrGroupeProposition propo INNER JOIN EbQrGroupe gr ON gr.ebQrGroupeNum = propo.ebQrGroupe.ebQrGroupeNum  WHERE  propo.ebQrGroupe.ebQrGroupeNum = :xEbQrGroupeNum")
    public List<Integer> getListEbDemandeByEbQrGroupeNum(@Param("xEbQrGroupeNum") Integer xEbQrGroupeNum);

		@Transactional
		@Modifying
		@Query(
			"update EbQrGroupeProposition prop set prop.ebDemandeResult.ebDemandeNum = :ebDemandeNum where prop.ebQrGroupePropositionNum  = :ebQrGroupePropositionNum"
		)
		public void updateX_EbDemandeResult(
			@Param("ebDemandeNum") Integer ebDemandeNum,
			@Param("ebQrGroupePropositionNum") Integer ebQrGroupePropositionNum
		);

		@Transactional
		@Modifying
		@Query(
			value = "DELETE FROM work.eb_qr_groupe_proposition where x_eb_demande = ?1",
			nativeQuery = true
		)
		void deleteByXEbDemande(Integer demandeNum);
		
		@Transactional
		@Modifying
		@Query(value = "DELETE FROM work.eb_qr_groupe_proposition where eb_qr_groupe_proposition_num IN (?1)", nativeQuery = true)
		public void deletePropositionsByIds(List<Integer> propositions);

}
