/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.exception;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.adias.mytowereasy.api.util.MyTowerErrorMsg;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.email.EmailService;
import com.adias.mytowereasy.service.storage.StorageException;


@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @Autowired
    EmailService emailService;

    @Autowired
    ConnectedUserService connectedUserService;

    @ExceptionHandler(MyTowerException.class)
    @ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
    public @ResponseBody List<MyTowerErrorMsg> handleMyTowerException(MyTowerException ex) {
        return ex.getListErrorMessage();
    }

    @ExceptionHandler({
        AccessDeniedException.class
    })
    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    public @ResponseBody String springSecurityAccessDenied(AccessDeniedException ex) {
        return "Token not found or not authorized to access this ressource";
    }

    /**
     * Handle invalid model and generate an human readable list of errors
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        MethodArgumentNotValidException ex,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request) {
        List<String> errorList = ex
            .getBindingResult().getFieldErrors().stream().map(fieldError -> fieldError.toString())
            .collect(Collectors.toList());

        return handleExceptionInternal(ex, errorList, headers, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = {
        Exception.class, StorageException.class, IllegalArgumentException.class,
    })
    public final ResponseEntity<String> handleAllUnmanagedExceptions(Exception ex, WebRequest request)
        throws Exception {

        if (ex != null) {

            if (ex instanceof ResponseStatusException) {
                throw ex;
            }

        }

        Date dateCreation = new Date();
        ErrorDetails errorDetails = new ErrorDetails();
        HttpServletRequest currentRequest = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
            .getRequest();
        EbUser connectedUser = connectedUserService.getCurrentUser();

        if (ex.getStackTrace() != null && ex.getStackTrace().length > 0) {
            boolean found = false;

            for (StackTraceElement stk: ex.getStackTrace()) {

                if (stk.getClassName().contains("com.adias")) {
                    errorDetails.setMethodName(stk.getMethodName());
                    errorDetails.setClassName(stk.getClassName());
                    errorDetails.setLineNumber(stk.getLineNumber());
                    found = true;
                    break;
                }

            }

            if (!found) {
                errorDetails.setMethodName(ex.getStackTrace()[0].getMethodName());
                errorDetails.setClassName(ex.getStackTrace()[0].getClassName());
                errorDetails.setLineNumber(ex.getStackTrace()[0].getLineNumber());
            }

        }

        if (connectedUser != null) {
            errorDetails.setEbUserNom(connectedUser.getNom());
            errorDetails.setEbUserPrenom(connectedUser.getPrenom());
            errorDetails.setEbUserNum(connectedUser.getEbUserNum());
            errorDetails.setEbUserRole(connectedUser.getRole());
            errorDetails.setEbUserService(connectedUser.getService());
            errorDetails.setEbUserEmail(connectedUser.getEmail());
        }

        errorDetails.setDescription(request.getDescription(false));
        errorDetails.setDetails(ex.getMessage());
        errorDetails.setRealError(ExceptionUtils.getRootCauseMessage(ex));
        errorDetails.setDateCreation(dateCreation);
        errorDetails.setReferer(request.getHeader("referer"));
        errorDetails.setClientIp(getClientIpAddr(currentRequest));
        errorDetails.setBrowser(getClientBrowser(request));
        errorDetails.setAgent(getUserAgent(request));
        errorDetails.setClientOs(getClientOS(request));
        errorDetails.setRequestUrl(getFullURL(currentRequest));
        errorDetails.setStack(ExceptionUtils.getStackTrace(ex));

        String action = errorDetails.getDescription() != null ?
            errorDetails.getDescription() :
            errorDetails.getRequestUrl();

        if (action != null) {
            action = action.substring(action.indexOf("api/") + 4, action.length());
            errorDetails.setAction(getActionFromUri(action));
        }

        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> mapError = oMapper.convertValue(errorDetails, Map.class);
        String value;

        for (String key: mapError.keySet()) {

            if (key != null && mapError.get(key) != null) {
                if (mapError.get(key) instanceof Date) value = ((Date) mapError.get(key)).toString();
                else if (mapError.get(key) instanceof Integer) value = "" + (Integer) mapError.get(key);
                else if (mapError.get(key) instanceof Long) value = "" + (Long) mapError.get(key);
                else value = (String) mapError.get(key);

                System.err.println(key + ": " + value);
            }

        }

        try {
            emailService.sendEmailException(errorDetails);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String errorMessage = "Internal server error" + ex.getMessage();
        return new ResponseEntity<>(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static String getActionFromUri(String uri) {
        String action = "";

        // account controller
        if (uri.contains("/create-delegation")) action = "Creation de delegation";
        else if (uri.contains("/deleteDelegation")) action = "Suppression de delegation";
        else if (uri.contains("/updateEtablissement")) action = "MAJ d'un etablissement";
        else if (uri.contains("/updateUser") || uri.contains("/updateUserDetails")) action = "MAJ d'un user";
        else if (uri.contains("/changeUserPassword")) action = "MAJ du mot de passe";
        else if (uri.contains("/account-settings")) action = "Chargement des infos sur l'user (account settings)";
        else if (uri
            .contains("/change-password-token")) action = "Changement du mot de passe et reinitialisation du token";
        else if (uri.contains("/invitation")) action = "Invitation des amis et des collegues";
        else if (uri.contains("/add-favori")) action = "Ajout du favori";
        else if (uri.contains("/delete-favori")) action = "Suppression du favori";
        else if (uri.contains("/list-favori")) action = "Chargement de la liste des favoris";
        else if (uri.contains("/save-config-params-mail")) action = "Sauvegarde de la config des parametres du mail";

        // community controller
        else if (uri.contains("/accepter-relation")) action = "Accepter une relation";
        else if (uri.contains("/ignorer-relation")) action = "Ignorer une relation";
        else if (uri.contains("/get-contacts")) action = "/Chargement des contacts";
        else if (uri.contains("/update-contact")) action = "MAJ d'une relation";
        else if (uri.contains("/get-etablissements-contact")) action = "Chargement des etablissements des contacts";
        else if (uri.contains("/get-sending-request")) action = "Chargement des demandes de MER envoyes";
        else if (uri.contains("/get-receiving-request")) action = "Chargement des demandes de MER recus";
        else if (uri.contains("/annuler-relation")) action = "Annuler une relation";
        else if (uri.contains("/get-list-user")) action = "Chargement de liste des users";
        else if (uri.contains("/send-invitaion")) action = "Envoie d'une invitation";
        else if (uri.contains("/send-invitaion-bymail")) action = "Envoie d'une invitation par email";

        // pricing
        else if (uri.contains("listDemande")) action = "Chargement des dossiers pricing";
        else if (uri.matches(".+/add$")) action = "Add";
        else if (uri.contains("party")) action = "Chargement d'un party";
        else if (uri.contains("parties")) action = "Chargement des parties";
        else if (uri.contains("adresse")) action = "Chargement d'une adresse";
        else if (uri
            .contains("adresseAsParty")) action = "Chargement d'une adresse en tant que party (auto-completion)";
        else if (uri.contains("adresses")) action = "Chargement des adresses";
        else if (uri.contains("listTransporteur")) action = "Chargement de la liste des transporteurs";
        else if (uri
            .contains("listTransporteurControlTower")) action = "Chargement de la liste des transporteurs et des CTs";
        else if (uri.contains("listBroker")) action = "Chargement de la liste des brokers";
        else if (uri.contains("listCostCenters")) action = "Chargement de liste des cost-centers";
        else if (uri.contains("listCategories")) action = "Chargement de la liste des categories";
        else if (uri.contains("demande")) action = "Chargement des details d'un dossier Pricing";
        else if (uri.contains("listDemandeQuote")) action = "Chargement de la liste des demandes de cotation";
        else if (uri.contains("addQuotation")) action = "Ajout d'une cotation";
        else if (uri.contains("updateStatusQuotation")) action = "Confirmation/Recommandation d'une cotation";
        else if (uri.contains("confirmPickup")) action = "Confirmation du pickup";
        else if (uri.contains("cancelQuotation")) action = "Annulation d'une cotation";
        else if (uri.contains("deleteDocument")) action = "Suppression d'un document";
        else if (uri.contains("updateEbDemandeTransportInfos")) action = "MAJ des informations de transport";
        else if (uri.contains("listPostCostCategorie")) action = "Chargement de liste des postes de cout";
        else if (uri.contains("deleteQuotation")) action = "Suppression d'une cotation";

        // file download
        else if (uri.contains("/csv")) action = "Extraction des donnees en CSV/Excel";
        else if (uri.contains("/ficheDestinationCountry")) action = "Extraction des donnees en PDF";

        // file upload
        else if (uri.contains("/upload-file")) action = "Upload d'un fichier";
        else if (uri.contains("/upload-file-import")) action = "Upload d'un fichier d'import en masse";
        else if (uri.contains("/upload-file-invoice")) action = "Upload d'un fichier invoice";

        // etablissement
        else if (uri.contains("/get-etablisement")) action = "Chargement d'un etablissement";
        else if (uri.contains("/get-list-etablisement")) action = "Chargement de la liste des etablisements";
        else if (uri.contains("/getlist-etablisement-ct")) action = "Chargement de la liste des etablissement CT";
        else if (uri.contains("/add-etablissement")) action = "add-etablissement";
        else if (uri
            .contains("/get-list-adresse-etablisement")) action = "Chargement de liste des adresses d'un etablisement";
        else if (uri.contains("/add-adresse-etablisement")) action = "Ajout d'une adresse";
        else if (uri.contains("/edit-adresse-etablisement")) action = "Modification d'une adresse";
        else if (uri.contains("/delete-adresse-etablisement")) action = "Suppression d'une adresse";
        else if (uri.contains("/get-list-cost-center-etablisement")) action = "Chargement de la liste des cost-center";
        else if (uri.contains("/add-cost-center-etablisement")) action = "Ajout d'un cost-center";
        else if (uri.contains("/edit-cost-center-etablisement")) action = "MAJ d'un cost-center";
        else if (uri.contains("/delete-cost-center-etablisement")) action = "Suppression d'un cost-center";
        else if (uri.contains("/add-customfield")) action = "Ajout d'un champs parametre";
        else if (uri.contains("/get-customfield")) action = "Chargement des champs parametres";
        else if (uri.contains("/save-exEtablissementCurrency")) action = "Sauvegarde d'une devise (currency)";
        else if (uri.contains("/delete-exEtablissementCurrency")) action = "suppression d'une devise (currency)";
        else if (uri
            .contains("/get-list-exEtablissementCurrency")) action = "Chargement de liste des devises (currency)";
        else if (uri
            .contains(
                "/get-data-preference")) action = "Chargement des donnes de preference (categories, champs parametres, brokers, transporteurs)";
        else if (uri.contains("/get-listCategories")) action = "Chargement de liste des categories";
        else action = uri.toString();

        return action;
    }

    public static String getFullURL(HttpServletRequest request) {
        final StringBuffer requestURL = request.getRequestURL();
        final String queryString = request.getQueryString();

        final String result = queryString == null ?
            requestURL.toString() :
            requestURL.append('?').append(queryString).toString();

        return result;
    }

    public static String getUserAgent(WebRequest request) {
        return request.getHeader("User-Agent");
    }

    public static String getClientBrowser(WebRequest request) {
        final String browserDetails = request.getHeader("User-Agent");
        final String user = browserDetails.toLowerCase();

        String browser = "";

        // ===============Browser===========================
        if (user.contains("msie")) {
            String substring = browserDetails.substring(browserDetails.indexOf("MSIE")).split(";")[0];
            browser = substring.split(" ")[0].replace("MSIE", "IE") + "-" + substring.split(" ")[1];
        }
        else if (user.contains("safari") && user.contains("version")) {
            browser = (browserDetails.substring(browserDetails.indexOf("Safari")).split(" ")[0]).split("/")[0] + "-"
                + (browserDetails.substring(browserDetails.indexOf("Version")).split(" ")[0]).split("/")[1];
        }
        else if (user.contains("opr") || user.contains("opera")) {
            if (user
                .contains("opera")) browser = (browserDetails.substring(browserDetails.indexOf("Opera")).split(" ")[0])
                    .split("/")[0] + "-"
                    + (browserDetails.substring(browserDetails.indexOf("Version")).split(" ")[0]).split("/")[1];
            else if (user
                .contains("opr")) browser = ((browserDetails.substring(browserDetails.indexOf("OPR")).split(" ")[0])
                    .replace("/", "-")).replace("OPR", "Opera");
        }
        else if (user.contains("chrome")) {
            browser = (browserDetails.substring(browserDetails.indexOf("Chrome")).split(" ")[0]).replace("/", "-");
        }
        else if ((user.indexOf("mozilla/7.0") > -1) || (user.indexOf("netscape6") != -1)
            || (user.indexOf("mozilla/4.7") != -1) || (user.indexOf("mozilla/4.78") != -1)
            || (user.indexOf("mozilla/4.08") != -1) || (user.indexOf("mozilla/3") != -1)) {
            // browser=(userAgent.substring(userAgent.indexOf("MSIE")).split("
            // ")[0]).replace("/", "-");
            browser = "Netscape-?";
        }
        else if (user.contains("firefox")) {
            browser = (browserDetails.substring(browserDetails.indexOf("Firefox")).split(" ")[0]).replace("/", "-");
        }
        else if (user.contains("rv")) {
            browser = "IE";
        }
        else {
            browser = "UnKnown, More-Info: " + browserDetails;
        }

        return browser;
    }

    public static String getClientOS(WebRequest request) {
        final String browserDetails = request.getHeader("User-Agent");

        // =================OS=======================
        final String lowerCaseBrowser = browserDetails.toLowerCase();

        if (lowerCaseBrowser.contains("windows")) {
            return "Windows";
        }
        else if (lowerCaseBrowser.contains("mac")) {
            return "Mac";
        }
        else if (lowerCaseBrowser.contains("x11")) {
            return "Unix";
        }
        else if (lowerCaseBrowser.contains("android")) {
            return "Android";
        }
        else if (lowerCaseBrowser.contains("iphone")) {
            return "IPhone";
        }
        else {
            return "UnKnown, More-Info: " + browserDetails;
        }

    }

    public static String getClientIpAddr(HttpServletRequest request) {
        String ip = request.getHeader("X-Forwarded-For");

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_CLIENT_IP");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
            ip = request.getRemoteUser();
        }

        return ip;
    }
}
