package com.adias.mytowereasy.exception;

import java.util.ArrayList;
import java.util.List;

import com.adias.mytowereasy.api.util.MyTowerErrorMsg;


public class MyTowerException extends RuntimeException {
    private static final long serialVersionUID = 4L;
    private List<MyTowerErrorMsg> listErrorMessage = new ArrayList<MyTowerErrorMsg>();

    public MyTowerException(String message) {
        super(message);

        listErrorMessage = new ArrayList<>();
        listErrorMessage.add(new MyTowerErrorMsg(message));
    }

    public MyTowerException(List<MyTowerErrorMsg> listErrorMessage) {
        super();
        this.listErrorMessage = listErrorMessage;
    }

    public List<MyTowerErrorMsg> getListErrorMessage() {
        return listErrorMessage;
    }

    public void setListErrorMessage(List<MyTowerErrorMsg> listErrorMessage) {
        this.listErrorMessage = listErrorMessage;
    }
}
