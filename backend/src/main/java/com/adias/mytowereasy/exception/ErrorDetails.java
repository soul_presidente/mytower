/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.exception;

import java.util.Date;


public class ErrorDetails {
    private Date dateCreation;

    private String browser;

    private String className;

    private String methodName;

    private String details;

    private String description;

    private String realError;

    private String requestUrl;

    private String referer;

    private String agent;

    private String clientIp;

    private String clientOs;

    private String action;

    private String stack;

    private Integer ebUserNum;
    private String ebUserNom;
    private String ebUserPrenom;
    private Integer ebUserRole;
    private Integer ebUserService;
    private String ebUserEmail;

    private Integer lineNumber;

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRealError() {
        return realError;
    }

    public void setRealError(String realError) {
        this.realError = realError;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getAgent() {
        return agent;
    }

    public void setAgent(String agent) {
        this.agent = agent;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getClientOs() {
        return clientOs;
    }

    public void setClientOs(String clientOs) {
        this.clientOs = clientOs;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Integer getEbUserNum() {
        return ebUserNum;
    }

    public String getEbUserNom() {
        return ebUserNom;
    }

    public String getEbUserPrenom() {
        return ebUserPrenom;
    }

    public void setEbUserNum(Integer ebUserNum) {
        this.ebUserNum = ebUserNum;
    }

    public void setEbUserNom(String ebUserNom) {
        this.ebUserNom = ebUserNom;
    }

    public void setEbUserPrenom(String ebUserPrenom) {
        this.ebUserPrenom = ebUserPrenom;
    }

    public Integer getEbUserRole() {
        return ebUserRole;
    }

    public void setEbUserRole(Integer ebUserRole) {
        this.ebUserRole = ebUserRole;
    }

    public Integer getEbUserService() {
        return ebUserService;
    }

    public void setEbUserService(Integer ebUserService) {
        this.ebUserService = ebUserService;
    }

    public String getEbUserEmail() {
        return ebUserEmail;
    }

    public void setEbUserEmail(String ebUserEmail) {
        this.ebUserEmail = ebUserEmail;
    }

    public String getStack() {
        return stack;
    }

    public void setStack(String stack) {
        this.stack = stack;
    }
}
