/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import javax.persistence.*;

import com.querydsl.core.annotations.QueryProjection;


@Table(name = "eb_customs_information")
@Entity
public class EbCustomsInformation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebCustomsInformationNum;

    private String declarationNumber;

    private Integer control;

    private Integer customsDuty;
    private Double costCustomsDuty;
    private EcCurrency xEcCurrencyCustomsDuty;

    private Integer Vat;
    private Double costVat;
    private EcCurrency xEcCurrencyVat;

    private Integer OtherTaxes;
    private Double costOtherTaxes;
    private EcCurrency xEcCurrencyOtherTaxes;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user")
    private EbUser xEbUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement")
    private EbEtablissement xEbEtablissement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie")
    private EbCompagnie xEbCompagnie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_demande")
    private EbDemande xEbDemande;

    public EbCustomsInformation() {
    }

    @QueryProjection
    public EbCustomsInformation(
        Integer ebCustomsInformationNum,
        String declarationNumber,
        Integer control,
        Integer customsDuty,
        Double costCustomsDuty,
        EcCurrency xEcCurrencyCustomsDuty,
        Integer Vat,
        Double costVat,
        EcCurrency xEcCurrencyVat,
        Integer OtherTaxes,
        Double costOtherTaxes,
        EcCurrency xEcCurrencyOtherTaxes,
        Integer ebUserNum,
        String nom,
        String prenom,
        Integer ebCompagnieNum,
        String nomCompagnie,
        String code,
        Integer ebEtablissementNum,
        String nomEtablissement,
        Integer ebDemandeNum) {
        this.ebCustomsInformationNum = ebCustomsInformationNum;

        this.declarationNumber = declarationNumber;
        this.control = control;
        this.customsDuty = customsDuty;
        this.costCustomsDuty = costCustomsDuty;
        this.xEcCurrencyCustomsDuty = xEcCurrencyCustomsDuty;
        this.Vat = Vat;
        this.costVat = costVat;
        this.xEcCurrencyVat = xEcCurrencyVat;
        this.OtherTaxes = OtherTaxes;
        this.costOtherTaxes = costOtherTaxes;
        this.xEcCurrencyOtherTaxes = xEcCurrencyOtherTaxes;

        this.xEbUser = new EbUser(ebUserNum, nom, prenom);
        this.xEbCompagnie = new EbCompagnie(ebCompagnieNum, nom, code);
        this.xEbEtablissement = new EbEtablissement(ebEtablissementNum, nom);

        this.xEbDemande = new EbDemande(ebDemandeNum);
    }

    public Integer getEbCustomsInformationNum() {
        return ebCustomsInformationNum;
    }

    public void setEbCustomsInformationNum(Integer ebCustomsInformationNum) {
        this.ebCustomsInformationNum = ebCustomsInformationNum;
    }

    public String getDeclarationNumber() {
        return declarationNumber;
    }

    public void setDeclarationNumber(String declarationNumber) {
        this.declarationNumber = declarationNumber;
    }

    public Integer getControl() {
        return control;
    }

    public void setControl(Integer control) {
        this.control = control;
    }

    public Integer getCustomsDuty() {
        return customsDuty;
    }

    public void setCustomsDuty(Integer customsDuty) {
        this.customsDuty = customsDuty;
    }

    public Double getCostCustomsDuty() {
        return costCustomsDuty;
    }

    public void setCostCustomsDuty(Double costCustomsDuty) {
        this.costCustomsDuty = costCustomsDuty;
    }

    public EcCurrency getxEcCurrencyCustomsDuty() {
        return xEcCurrencyCustomsDuty;
    }

    public void setxEcCurrencyCustomsDuty(EcCurrency xEcCurrencyCustomsDuty) {
        this.xEcCurrencyCustomsDuty = xEcCurrencyCustomsDuty;
    }

    public Integer getVat() {
        return Vat;
    }

    public void setVat(Integer vat) {
        Vat = vat;
    }

    public Double getCostVat() {
        return costVat;
    }

    public void setCostVat(Double costVat) {
        this.costVat = costVat;
    }

    public EcCurrency getxEcCurrencyVat() {
        return xEcCurrencyVat;
    }

    public void setxEcCurrencyVat(EcCurrency xEcCurrencyVat) {
        this.xEcCurrencyVat = xEcCurrencyVat;
    }

    public Integer getOtherTaxes() {
        return OtherTaxes;
    }

    public void setOtherTaxes(Integer otherTaxes) {
        OtherTaxes = otherTaxes;
    }

    public Double getCostOtherTaxes() {
        return costOtherTaxes;
    }

    public void setCostOtherTaxes(Double costOtherTaxes) {
        this.costOtherTaxes = costOtherTaxes;
    }

    public EcCurrency getxEcCurrencyOtherTaxes() {
        return xEcCurrencyOtherTaxes;
    }

    public void setxEcCurrencyOtherTaxes(EcCurrency xEcCurrencyOtherTaxes) {
        this.xEcCurrencyOtherTaxes = xEcCurrencyOtherTaxes;
    }

    public EbUser getxEbUser() {
        return xEbUser;
    }

    public void setxEbUser(EbUser xEbUser) {
        this.xEbUser = xEbUser;
    }

    public EbEtablissement getxEbEtablissement() {
        return xEbEtablissement;
    }

    public void setxEbEtablissement(EbEtablissement xEbEtablissement) {
        this.xEbEtablissement = xEbEtablissement;
    }

    public EbDemande getxEbDemande() {
        return xEbDemande;
    }

    public void setxEbDemande(EbDemande xEbDemande) {
        this.xEbDemande = xEbDemande;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }
}
