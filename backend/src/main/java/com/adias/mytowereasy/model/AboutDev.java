package com.adias.mytowereasy.model;

import java.time.Instant;

import com.fasterxml.jackson.annotation.JsonFormat;


public class AboutDev {
    private String gitRevision;
    private String gitMessage;
    private String activeProfiles;
    private String version;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd@HH:mm:ss.SSSZ")
    private Instant buildDate;

    public String getGitRevision() {
        return gitRevision;
    }

    public void setGitRevision(String gitRevision) {
        this.gitRevision = gitRevision;
    }

    public String getGitMessage() {
        return gitMessage;
    }

    public void setGitMessage(String gitMessage) {
        this.gitMessage = gitMessage;
    }

    public String getActiveProfiles() {
        return activeProfiles;
    }

    public void setActiveProfiles(String activeProfiles) {
        this.activeProfiles = activeProfiles;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Instant getBuildDate() {
        return buildDate;
    }

    public void setBuildDate(Instant buildDate) {
        this.buildDate = buildDate;
    }
}
