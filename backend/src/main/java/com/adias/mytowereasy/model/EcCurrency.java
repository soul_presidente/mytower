/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@JsonIgnoreProperties({
    "hibernateLazyInitializer", "handler"
})
public class EcCurrency implements Serializable {
    public EcCurrency(String codeLibelle) {
        super();
        this.codeLibelle = codeLibelle;
    }

    private static final long serialVersionUID = 9220745822871291682L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true, updatable = false)
    private Integer ecCurrencyNum;

    @Column(updatable = false)
    private String libelle;

    @Column(updatable = false)
    private String code;

    private String symbol;

    private String codeLibelle;

    @Column(insertable = false, updatable = false)
    @ColumnDefault("now()")
    private Date dateCreation;

    @Column(updatable = false)
    private BigDecimal exchangeRateEuro;

    public EcCurrency() {
    }

    public EcCurrency(Integer ecCurrencyNum, String libelle, String codeLibelle, String symbol, String code) {
        super();
        this.ecCurrencyNum = ecCurrencyNum;
        this.libelle = libelle;
        this.codeLibelle = codeLibelle;
        this.symbol = symbol;
        this.code = code;
    }

    public EcCurrency(Integer ecCurrencyNum) {
        this.ecCurrencyNum = ecCurrencyNum;
    }

    public EcCurrency(Integer ecCurrencyNum, String libelle, String code, String codeLibelle) {
        super();
        this.ecCurrencyNum = ecCurrencyNum;
        this.libelle = libelle;
        this.code = code;
        this.codeLibelle = codeLibelle;
    }

    public EcCurrency(Integer ecCurrencyNum, String code) {
        this.ecCurrencyNum = ecCurrencyNum;
        this.code = code;
    }

    public EcCurrency(Integer ecCurrencyNum, String libelle, String code) {
        super();
        this.ecCurrencyNum = ecCurrencyNum;
        this.libelle = libelle;
        this.code = code;
    }

    public Integer getEcCurrencyNum() {
        return ecCurrencyNum;
    }

    public void setEcCurrencyNum(Integer ecCurrencyNum) {
        this.ecCurrencyNum = ecCurrencyNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getCodeLibelle() {
        String str = "";

        if (this.code != null) str += this.code;
        if (this.libelle != null && this.code != null) str += " - ";
        if (this.libelle != null) str += this.libelle;

        this.codeLibelle = str;

        return this.codeLibelle;
    }

    public void setCodeLibelle(String codeLibelle) {
        String str = "";

        if (this.code != null) str += this.code;
        if (this.libelle != null && this.code != null) str += " - ";
        if (this.libelle != null) str += this.libelle;

        this.codeLibelle = str;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        result = prime * result + ((ecCurrencyNum == null) ? 0 : ecCurrencyNum.hashCode());
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        result = prime * result + ((symbol == null) ? 0 : symbol.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EcCurrency other = (EcCurrency) obj;

        if (code == null) {
            if (other.code != null) return false;
        }
        else if (!code.equals(other.code)) return false;

        if (dateCreation == null) {
            if (other.dateCreation != null) return false;
        }
        else if (!dateCreation.equals(other.dateCreation)) return false;

        if (ecCurrencyNum == null) {
            if (other.ecCurrencyNum != null) return false;
        }
        else if (!ecCurrencyNum.equals(other.ecCurrencyNum)) return false;

        if (libelle == null) {
            if (other.libelle != null) return false;
        }
        else if (!libelle.equals(other.libelle)) return false;

        if (symbol == null) {
            if (other.symbol != null) return false;
        }
        else if (!symbol.equals(other.symbol)) return false;

        return true;
    }

    public BigDecimal getExchangeRateEuro() {
        return exchangeRateEuro;
    }

    public void setExchangeRateEuro(BigDecimal exchangeRateEuro) {
        this.exchangeRateEuro = exchangeRateEuro;
    }
}
