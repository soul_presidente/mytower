package com.adias.mytowereasy.model;

import javax.persistence.*;

import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.dto.EbFlagDTO;


@Entity
@Table(name = "eb_flag", schema = "work")
public class EbFlag {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebFlagNum;

    private String name;

    private String reference;

    private Integer icon;

    private String color;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", insertable = true)
    private EbCompagnie compagnie;

    public EbFlag(EbFlagDTO flagDto) {
        this.color = flagDto.getColor();
        this.ebFlagNum = flagDto.getEbFlagNum();
        this.icon = flagDto.getIcon();
    }

    public EbFlag() {
    }

    @QueryProjection
    public EbFlag(Integer ebFlagNum, String name, String reference, Integer icon, String color) {
        this.ebFlagNum = ebFlagNum;
        this.name = name;
        this.reference = reference;
        this.icon = icon;
        this.color = color;
    }

    public Integer getEbFlagNum() {
        return ebFlagNum;
    }

    public void setEbFlagNum(Integer ebFlagNum) {
        this.ebFlagNum = ebFlagNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Integer getIcon() {
        return icon;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public EbCompagnie getCompagnie() {
        return compagnie;
    }

    public void setCompagnie(EbCompagnie compagnie) {
        this.compagnie = compagnie;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ebFlagNum == null) ? 0 : ebFlagNum.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbFlag other = (EbFlag) obj;

        if (ebFlagNum == null) {
            if (other.ebFlagNum != null) return false;
        }
        else if (!ebFlagNum.equals(other.ebFlagNum)) return false;

        return true;
    }
}
