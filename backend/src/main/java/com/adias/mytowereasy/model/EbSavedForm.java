/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.dto.EbFlagDTO;


@Entity
@Table(name = "eb_saved_form", schema = "work")
public class EbSavedForm {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebSavedFormNum;

    private String formName;

    @Column(columnDefinition = "TEXT")
    private String formValues;

    // identifiant du module (id des modules pricing, booking, trake, claims,
    // freight)
    private Integer ebModuleNum;

    // identifiant du component (id du formulaire de recherche, id du masque,
    // etc.)
    private Integer ebCompNum;

    private String url;

    @ManyToOne
    @JoinColumn(name = "x_eb_user", insertable = true)
    private EbUser ebUser;

    @Column(insertable = false, updatable = true)
    @ColumnDefault("now()")
    private Date dateAjout;

    private Integer action;
    private String actionTrigger;

    @Column(columnDefinition = "TEXT")
    private String actionTargets;

    private String actionParameters;
    private Integer ordre;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", insertable = true)
    private EbCompagnie compagnie;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer ebUserNum;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbFlagDTO> flagDtos;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String statusLibelle;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<Integer> listCodeActionTrigger;

    public EbSavedForm() {
        // Empty constructor
    }

		@QueryProjection
		public EbSavedForm(
			Integer ebSavedFormNum,
			String formName,
			String formValues,
			Integer ebModuleNum,
			Integer ebCompNum,
			Date dateAjout,
			Integer action,
			String actionTrigger,
			String actionTargets,
			String actionParameters,
			Integer ordre,
			Integer ebUserNum,
			Integer compagnieNum,
			String url
		)
		{
			this.ebSavedFormNum = ebSavedFormNum;
			this.formName = formName;
			this.formValues = formValues;
			this.ebModuleNum = ebModuleNum;
			this.ebCompNum = ebCompNum;
			this.dateAjout = dateAjout;
			this.action = action;
			this.actionTrigger = actionTrigger;
			this.actionTargets = actionTargets;
			this.actionParameters = actionParameters;
			this.ordre = ordre;
			this.url = url;

			if (ebUserNum != null)
			{
				this.ebUser = new EbUser(ebUserNum);
			}

			if (compagnieNum != null)
			{
				this.compagnie = new EbCompagnie(compagnieNum);
			}

		}

    public Integer getOrdre() {
        return ordre;
    }

    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    public List<Integer> getListCodeActionTrigger() {
        return listCodeActionTrigger;
    }

    public void setListCodeActionTrigger(List<Integer> listCodeActionTrigger) {
        this.listCodeActionTrigger = listCodeActionTrigger;
    }

    public EbCompagnie getCompagnie() {
        return compagnie;
    }

    public void setCompagnie(EbCompagnie compagnie) {
        this.compagnie = compagnie;
    }

    public Integer getEbSavedFormNum() {
        return ebSavedFormNum;
    }

    public void setEbSavedFormNum(Integer ebSavedFormNum) {
        this.ebSavedFormNum = ebSavedFormNum;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Integer getEbModuleNum() {
        return ebModuleNum;
    }

    public void setEbModuleNum(Integer ebModuleNum) {
        this.ebModuleNum = ebModuleNum;
    }

    public EbUser getEbUser() {
        return ebUser;
    }

    public void setEbUser(EbUser ebUser) {
        this.ebUser = ebUser;
    }

    public String getFormValues() {
        return formValues;
    }

    public void setFormValues(String formValues) {
        this.formValues = formValues;
    }

    public Integer getEbCompNum() {
        return ebCompNum;
    }

    public void setEbCompNum(Integer ebCompNum) {
        this.ebCompNum = ebCompNum;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getEbUserNum() {
        return this.ebUser != null ? this.ebUser.getEbUserNum() : null;
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public String getActionTrigger() {
        return actionTrigger;
    }

    public void setActionTrigger(String actionTrigger) {
        this.actionTrigger = actionTrigger;
    }

    public String getActionTargets() {
        return actionTargets;
    }

    public void setActionTargets(String actionTargets) {
        this.actionTargets = actionTargets;
    }

    public String getActionParameters() {
        return actionParameters;
    }

    public void setActionParameters(String actionParameters) {
        this.actionParameters = actionParameters;
    }

    public List<EbFlagDTO> getFlagDtos() {
        return flagDtos;
    }

    public void setFlagDtos(List<EbFlagDTO> flagDtos) {
        this.flagDtos = flagDtos;
    }

    public String getStatusLibelle() {
        return statusLibelle;
    }

    public void setStatusLibelle(String statusLibelle) {
        this.statusLibelle = statusLibelle;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateAjout == null) ? 0 : dateAjout.hashCode());
        result = prime * result + ((ebCompNum == null) ? 0 : ebCompNum.hashCode());
        result = prime * result + ((ebModuleNum == null) ? 0 : ebModuleNum.hashCode());
        result = prime * result + ((ebSavedFormNum == null) ? 0 : ebSavedFormNum.hashCode());
        result = prime * result + ((ebUser == null) ? 0 : ebUser.hashCode());
        result = prime * result + ((formName == null) ? 0 : formName.hashCode());
        result = prime * result + ((formValues == null) ? 0 : formValues.hashCode());
        result = prime * result + ((action == null) ? 0 : action.hashCode());
        result = prime * result + ((actionTrigger == null) ? 0 : actionTrigger.hashCode());
        result = prime * result + ((actionTargets == null) ? 0 : actionTargets.hashCode());
        result = prime * result + ((actionParameters == null) ? 0 : actionParameters.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbSavedForm other = (EbSavedForm) obj;

        if (dateAjout == null) {
            if (other.dateAjout != null) return false;
        }
        else if (!dateAjout.equals(other.dateAjout)) return false;

        if (ebCompNum == null) {
            if (other.ebCompNum != null) return false;
        }
        else if (!ebCompNum.equals(other.ebCompNum)) return false;

        if (ebModuleNum == null) {
            if (other.ebModuleNum != null) return false;
        }
        else if (!ebModuleNum.equals(other.ebModuleNum)) return false;

        if (ebSavedFormNum == null) {
            if (other.ebSavedFormNum != null) return false;
        }
        else if (!ebSavedFormNum.equals(other.ebSavedFormNum)) return false;

        if (ebUser == null) {
            if (other.ebUser != null) return false;
        }
        else if (!ebUser.equals(other.ebUser)) return false;

        if (formName == null) {
            if (other.formName != null) return false;
        }
        else if (!formName.equals(other.formName)) return false;

        if (formValues == null) {
            if (other.formValues != null) return false;
        }
        else if (!formValues.equals(other.formValues)) return false;

        if (action == null) {
            if (other.action != null) return false;
        }
        else if (!action.equals(other.action)) return false;

        if (formValues == null) {
            if (other.actionTargets != null) return false;
        }
        else if (!actionTargets.equals(other.actionTargets)) return false;

        if (formValues == null) {
            if (other.actionTrigger != null) return false;
        }
        else if (!actionTrigger.equals(other.actionTrigger)) return false;

        if (formValues == null) {
            if (other.actionParameters != null) return false;
        }
        else if (!actionParameters.equals(other.actionParameters)) return false;

        return true;
    }
}
