/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.qm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.persistence.*;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.tt.EbTtCategorieDeviation;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.types.JsonBinaryType;


@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Entity
@JsonIgnoreProperties({
    "hibernateLazyInitializer", "handler"
})
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "ebQmIncidentNum",
    scope = EbQmIncident.class)
@Table(name = "eb_qm_incident", indexes = {
    @Index(name = "idx_eb_qm_incident_x_eb_tt_tracing", columnList = "x_eb_tt_tracing"),
    @Index(name = "idx_eb_qm_incident_x_eb_demande", columnList = "eb_demande"),
    @Index(name = "idx_eb_qm_incident_x_eb_etab_carrier", columnList = "x_eb_etab_carrier"),
    @Index(name = "idx_eb_qm_incident_x_eb_tt_categorie_deviation", columnList = "x_eb_tt_categorie_deviation"),
    @Index(name = "idx_eb_qm_incident_x_eb_compagnie", columnList = "x_eb_compagnie"),
    @Index(name = "idx_eb_qm_incident_x_ec_currency", columnList = "x_ec_currency"),
    @Index(name = "idx_eb_qm_incident_x_eb_user_last_contrib", columnList = "x_eb_user_last_contrib"),
    @Index(name = "idx_eb_qm_incident_x_ec_country_dest", columnList = "x_ec_country_dest"),
    @Index(name = "idx_eb_qm_incident_x_eb_qm_qualif", columnList = "x_eb_qm_qualif"),
    @Index(name = "idx_eb_qm_incident_x_eb_user_issuer", columnList = "x_eb_user_issuer"),
    @Index(name = "idx_eb_qm_incident_x_eb_user_other", columnList = "x_eb_user_other"),
    @Index(name = "idx_eb_qm_incident_x_eb_etablissement", columnList = "x_eb_etablissement"),
    @Index(name = "idx_eb_qm_incident_x_eb_user_request_owner", columnList = "x_eb_user_request_owner"),
    @Index(name = "idx_eb_qm_incident_x_eb_compagnie_request_owner", columnList = "x_eb_compagnie_request_owner"),
    @Index(
        name = "idx_eb_qm_incident_x_eb_etablissement_request_owner",
        columnList = "x_eb_etablissement_request_owner"),

})
@EntityListeners(AuditingEntityListener.class)
public class EbQmIncident implements java.io.Serializable {
    private static final long serialVersionUID = 1L;
    // private static final long serialVersionUID = -6549604130845892218L;

    // @GeneratedValue supprimé car la generation de l'id se fait manuelement via next val
    @Id
    private Integer ebQmIncidentNum;

    private Integer eventType;

    private Integer eventTypeStatus;

    private String unitRef;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_tt_tracing")
    private EbTtTracing xEbTtTracing;

    @Column(columnDefinition = "TEXT")
    private String incidentDesc;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date dateCreation;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date dateMaj;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCloture;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateIncident;

    @Column
    private Integer insurance;

    @Column(columnDefinition = "TEXT")
    private String insuranceComment;

    @Column
    private Double claimAmount;

    @Column
    private Double recovered;

    @Column
    private Double balance;

    @Column
    private String addImpact;

    @Column
    private String incidentRef;

    @Column(columnDefinition = "TEXT")
    private String etablissementComment;

    @Column(columnDefinition = "TEXT")
    private String thirdPartyComment;

    @Column
    private String goodValues;

    @Column
    private String icomReference;

    @Column
    private Double totalPieces;

    @Column
    private Double totalKg;

    @Column
    private Short cancelled;

    /**
     * FIXME
     * Investigate why this column exists ?
     * Probable duplicate with {@link EbQmIncident#ebDemande}
     */
    @Column
    @Deprecated
    private Integer xEbDemande;

    @Enumerated(EnumType.ORDINAL)
    private Enumeration.Incoterms incoterm;

    @Column(updatable = false)
    private Integer modeTransport;

    private Integer incidentStatus;

    @Enumerated(EnumType.ORDINAL)
    private Enumeration.FlowType flowType;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eb_demande")
    private EbDemande ebDemande;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab_carrier")
    private EbEtablissement carrier;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country_origin")
    private EcCountry originCountry;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country_dest")
    private EcCountry destinationCountry;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_currency")
    private EcCurrency currency;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_tt_categorie_deviation")
    private EbTtCategorieDeviation category;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_qm_qualif")
    private EbQmQualification qualification;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_validator")
    private EbUser userValidator;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_cancel_reason")
    private EbQmCancellationReason cancellationReason;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_incident_previous")
    private EbQmIncident previousIncident;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_last_contrib")
    private EbUser lastContributer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_other")
    private EbUser otherUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_issuer")
    private EbUser issuer;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "incident", cascade = CascadeType.ALL)
    private List<EbQmIncidentLine> incidentLines = new ArrayList<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement")
    private EbEtablissement xEbEtablissement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie")
    private EbCompagnie xEbCompagnie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_request_owner")
    private EbUser xEbUserOwnerRequest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie_request_owner")
    private EbCompagnie xEbCompagnieOwnerRequest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement_request_owner")
    private EbEtablissement xEbEtablissementOwnerRequest;

    @Column(columnDefinition = "TEXT")
    private String listCategoriesStr;

    private String listLabels;

    @Column(columnDefinition = "TEXT")
    private String listCategoryFlat;

    @Column(columnDefinition = "TEXT")
    private String customFields;

    @Column(columnDefinition = "TEXT")
    private String listCustomFieldsFlat;

    @Column(columnDefinition = "TEXT")
    private String listRootCauses;

    // champs reservé pour customFields
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    @Column(columnDefinition = "jsonb")
    @JsonFormat
    @Type(type = "jsonb")
    private List<EbQmRootCause> rootCauses;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCategorie> listCategories;

    private String rootCausesString;

    private String originCountryLibelle;

    private String destinationCountryLibelle;

    private String currencyLabel;

    private String lastContributerNomPremon;

    private String ownerNomPrenom;

    private Integer ownerNum;

    private String issuerNomPrenom;

    private String carrierNom;

    private String categoryLabel;

    private String qualificationLabel;

    private String demandeTranportRef;
    private Integer daysDelay;
    private Integer hoursDelay;
    private Integer unitDamaged;
    private Double unitDamagedAmount;
    private Integer unitLost;
    private Integer amondOfunitLost;
    private Double recoveredInsurance;
    private Integer rootCauseStatut;
    private String waitingForText;
    private Double impactedTransportAmount;

    private String partNumber;

    private String serialNumber;

    private String shippingType;

    private String orderNumber;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<OtherCost> othersCost;
    private Double impactedTransportPercentage;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbTypeDocuments> listTypeDocuments;

    private String listFlag;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbFlagDTO> listEbFlagDTO;

    private String responsable;

    private Integer severity;
    private String actions;
    private Integer criticality;
    private Integer rootCauseCategory;
    private String psl;
    private Integer typeRequestNum;
    private String typeRequestLibelle;

    @QueryProjection
    public EbQmIncident(EbQmIncident ebQmIncident) {
        this.ownerNomPrenom = ebQmIncident.ownerNomPrenom;
    }

    @QueryProjection
    public EbQmIncident(
        Integer ebQmIncidentNum,
        String incidentRef,
        Integer insurance,
        Integer xEcModeTransport,
        String partNumber,
        String serialNumber,
        String shippingType,
        String orderNumber,
        Date dateCreation,
        Date dateMaj,
        Date dateCloture,
        Date dateIncident,
        Integer xEcStatut,
        String originCountryLibelle,
        String destinationCountryLibelle,
        String currencyLabel,
        String lastContributerNomPremon,
        String ownerNomPrenom,
        String issuerNomPrenom,
        String carrierNom,
        String categoryLabel,
        String qualificationLabel,
        Double claimAmount,
        Double recovered,
        Double balance,
        String demandeTranportRef,
        Enumeration.FlowType flowType,
        String thirdPartyComment,
        String etablissementComment,
        String incidentDesc,
        String goodValues,
        String demandeTransportRef,
        String qualifLabel,
        Integer ebEtablissementnum,
        String addImpact,
        Integer xEbDemande,
        List<EbQmRootCause> rootCauses,
        Integer daysDelay,
        Integer hoursDelay,
        Integer unitDamaged,
        Integer unitLost,
        Integer amondOfunitLost,
        String customFields,
        List<EbCategorie> listCategories,
        String stringOrderWithExpression,
        Integer numberOrderWithExpression,
        String waitingForText,
        Integer rootCauseStatut,
        Double unitDamagedAmount,
        Double impactedTransportAmount,
        List<OtherCost> othersCost,
        Integer compagnieNum,
        String nomCompagnie,
        Integer ecCurrencyNum,
        String libelle,
        String codeLibelle,
        Integer ebTtCategorieDeviationNum,
        String label,
        Double recoveredInsurance,
        Double impactedTransportPercentage,
        String symbol,
        String code,
        String listFlag,
        Integer severity,
        String actions,
        String insuranceComment,
        Integer criticality,
        String responsable,
        Integer rootCauseCategory,
        String listRootCauses,
        String psl,
        String typeRequestLibelle,
        Integer typeRequestNum,
        Integer eventType,
        Integer eventTypeStatus) {
        this.partNumber = partNumber;
        this.serialNumber = serialNumber;
        this.orderNumber = orderNumber;
        this.shippingType = shippingType;
        this.daysDelay = daysDelay;
        this.hoursDelay = hoursDelay;
        this.unitDamaged = unitDamaged;
        this.unitLost = unitLost;
        this.amondOfunitLost = amondOfunitLost;
        this.ebQmIncidentNum = ebQmIncidentNum;
        this.incidentRef = incidentRef;
        this.insurance = insurance;
        this.modeTransport = xEcModeTransport;
        this.dateCreation = dateCreation;
        this.dateMaj = dateMaj;
        this.dateCloture = dateCloture;
        this.dateIncident = dateIncident;
        this.incidentStatus = xEcStatut;

        this.destinationCountryLibelle = destinationCountryLibelle;
        this.originCountryLibelle = originCountryLibelle;
        this.currencyLabel = currencyLabel;
        this.carrierNom = carrierNom;

        this.lastContributerNomPremon = lastContributerNomPremon;
        this.ownerNomPrenom = ownerNomPrenom;
        this.issuerNomPrenom = issuerNomPrenom;
        this.categoryLabel = categoryLabel;

        this.qualificationLabel = qualificationLabel;
        this.claimAmount = claimAmount;
        this.recovered = recovered;
        this.balance = balance;
        this.demandeTranportRef = demandeTranportRef;
        this.flowType = flowType;
        this.thirdPartyComment = thirdPartyComment;
        this.etablissementComment = etablissementComment;
        this.incidentDesc = incidentDesc;
        this.goodValues = goodValues;
        this.carrier = new EbEtablissement();
        this.carrier.setEbEtablissementNum(ebEtablissementnum);
        this.addImpact = addImpact;
        this.xEbDemande = xEbDemande;
        this.rootCauses = rootCauses;
        this.xEbCompagnie = new EbCompagnie(compagnieNum, nomCompagnie, null);
        // this.xEbUserOwnerRequest = new EbUser(numOwnerRequest,
        // nomOwnerRequest, prenomOwnerRequest);

        this.customFields = customFields;

        Gson gson = new Gson();

        if (customFields != null) {
            this.listCustomsFields = gson.fromJson(customFields, new TypeToken<ArrayList<CustomFields>>() {
            }.getType());
        }

        if (listCategories != null && listCategories.size() > 0) {
            String jsonCateg = gson.toJson(listCategories);
            this.listCategories = gson.fromJson(jsonCateg, new TypeToken<ArrayList<EbCategorie>>() {
            }.getType());
        }

        // this.listCategories = listCategories;
        this.waitingForText = waitingForText;
        this.rootCauseStatut = rootCauseStatut;
        this.unitDamagedAmount = unitDamagedAmount;
        this.impactedTransportAmount = impactedTransportAmount;
        this.othersCost = othersCost;

        this.currency = new EcCurrency(ecCurrencyNum, libelle, codeLibelle, symbol, code);
        if (ebTtCategorieDeviationNum != null
            && label != null) this.category = new EbTtCategorieDeviation(ebTtCategorieDeviationNum, label);
        else this.category = null;
        this.recoveredInsurance = recoveredInsurance;
        this.impactedTransportPercentage = impactedTransportPercentage;
        this.listFlag = listFlag;
        this.severity = severity;
        this.actions = actions;
        this.insuranceComment = insuranceComment;
        this.criticality = criticality;
        this.responsable = responsable;
        this.rootCauseCategory = rootCauseCategory;
        this.listRootCauses = listRootCauses;
        this.psl = psl;
        this.typeRequestLibelle = typeRequestLibelle;
        this.typeRequestNum = typeRequestNum;
        this.eventTypeStatus = eventTypeStatus;
    }

    public EbQmIncident(EbQmDeviation ebQmDeviation, EbDemande demande, Integer deviationCount) {

        if (Objects.nonNull(ebQmDeviation)) {
            this.carrierNom = ebQmDeviation.getCarrierName();
            this.demandeTranportRef = ebQmDeviation.getRefTransport();
            this.destinationCountryLibelle = ebQmDeviation.getDestinationCountryLibelle();
            this.originCountryLibelle = ebQmDeviation.getOriginCountryLibelle();
            this.qualificationLabel = ebQmDeviation.getTypeDeviation();
            this.carrier = ebQmDeviation.getxEbEtablissement();
            this.destinationCountry = ebQmDeviation.getDestinationCountry();
            this.originCountry = ebQmDeviation.getOriginCountry();
            this.xEbDemande = demande.getEbDemandeNum();
            this.ebDemande = demande;
            this.incidentRef = incidentRefRepresentation(deviationCount, ebQmDeviation.getRefTransport());
            this.issuer = ebQmDeviation.getxEbUser();
            this.xEbCompagnie = ebQmDeviation.getxEbCompagnie();
            this.xEbEtablissement = ebQmDeviation.getxEbEtablissement();
            this.eventType = TtEnumeration.TypeEvent.UNCONFORMITY.getCode();
            this.unitRef = ebQmDeviation.getUnitRef();
            this.xEbTtTracing = ebQmDeviation.getxEbTtTracing();
            this.dateCreation = ebQmDeviation.getDeviationDate();
            // this.xEbUserOwnerRequest = ebQmDeviation.getxEbUser();
        }

    }

    @QueryProjection
    public EbQmIncident(Integer ebQmIncidentNum, String incidentRef) {
        this.ebQmIncidentNum = ebQmIncidentNum;
        this.incidentRef = incidentRef;
    }

    public EbQmIncident() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String incidentRefRepresentation(Integer deviationCount, String refTransport) {
        return "DEV" + (deviationCount + 1) + "-" + refTransport;
    }

    public EbQmIncident(Integer ebQmIncidentNum) {
        this.ebQmIncidentNum = ebQmIncidentNum;
    }

    public Integer getEbQmIncidentNum() {
        return ebQmIncidentNum;
    }

    public void setEbQmIncidentNum(Integer ebQmIncidentNum) {
        this.ebQmIncidentNum = ebQmIncidentNum;
    }

    public String getIncidentDesc() {
        return incidentDesc;
    }

    public void setIncidentDesc(String incidentDesc) {
        this.incidentDesc = incidentDesc;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Date getDateCloture() {
        return dateCloture;
    }

    public void setDateCloture(Date dateCloture) {
        this.dateCloture = dateCloture;
    }

    public Date getDateIncident() {
        return dateIncident;
    }

    public void setDateIncident(Date dateIncident) {
        this.dateIncident = dateIncident;
    }

    public Integer getInsurance() {
        return insurance;
    }

    public void setInsurance(Integer insurance) {
        this.insurance = insurance;
    }

    public String getInsuranceComment() {
        return insuranceComment;
    }

    public void setInsuranceComment(String insuranceComment) {
        this.insuranceComment = insuranceComment;
    }

    public Double getClaimAmount() {
        return claimAmount;
    }

    public void setClaimAmount(Double claimAmount) {
        this.claimAmount = claimAmount;
    }

    public Double getRecovered() {
        return recovered;
    }

    public void setRecovered(Double recovered) {
        this.recovered = recovered;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getAddImpact() {
        return addImpact;
    }

    public void setAddImpact(String addImpact) {
        this.addImpact = addImpact;
    }

    public String getEtablissementComment() {
        return etablissementComment;
    }

    public void setEtablissementComment(String etablissementComment) {
        this.etablissementComment = etablissementComment;
    }

    public String getThirdPartyComment() {
        return thirdPartyComment;
    }

    public void setThirdPartyComment(String thirdPartyComment) {
        this.thirdPartyComment = thirdPartyComment;
    }

    public String getGoodValues() {
        return goodValues;
    }

    public void setGoodValues(String goodValues) {
        this.goodValues = goodValues;
    }

    public String getIcomReference() {
        return icomReference;
    }

    public void setIcomReference(String icomReference) {
        this.icomReference = icomReference;
    }

    public Double getTotalPieces() {
        return totalPieces;
    }

    public void setTotalPieces(Double totalPieces) {
        this.totalPieces = totalPieces;
    }

    public Double getTotalKg() {
        return totalKg;
    }

    public void setTotalKg(Double totalKg) {
        this.totalKg = totalKg;
    }

    public Short getCancelled() {
        return cancelled;
    }

    public void setCancelled(Short cancelled) {
        this.cancelled = cancelled;
    }

    public Enumeration.Incoterms getIncoterm() {
        return incoterm;
    }

    public void setIncoterm(Enumeration.Incoterms incoterm) {
        this.incoterm = incoterm;
    }

    public Integer getIncidentStatus() {
        return incidentStatus;
    }

    public void setIncidentStatus(Integer incidentStatus) {
        this.incidentStatus = incidentStatus;
    }

    public Enumeration.FlowType getFlowType() {
        return flowType;
    }

    public void setFlowType(Enumeration.FlowType flowType) {
        this.flowType = flowType;
    }

    public EbDemande getEbDemande() {
        return ebDemande;
    }

    public void setEbDemande(EbDemande ebDemande) {
        this.ebDemande = ebDemande;
        this.xEbDemande = ebDemande.getEbDemandeNum();
    }

    public EbEtablissement getCarrier() {
        return carrier;
    }

    public void setCarrier(EbEtablissement carrier) {
        this.carrier = carrier;
    }

    public EcCountry getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(EcCountry originCountry) {
        this.originCountry = originCountry;
    }

    public EcCountry getDestinationCountry() {
        return destinationCountry;
    }

    public void setDestinationCountry(EcCountry destinationCountry) {
        this.destinationCountry = destinationCountry;
    }

    public EcCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(EcCurrency currency) {
        this.currency = currency;
    }

    public EbTtCategorieDeviation getCategory() {
        return category;
    }

    public void setCategory(EbTtCategorieDeviation category) {
        this.category = category;
    }

    public EbQmQualification getQualification() {
        return qualification;
    }

    public void setQualification(EbQmQualification qualification) {
        this.qualification = qualification;
    }

    public EbUser getUserValidator() {
        return userValidator;
    }

    public void setUserValidator(EbUser userValidator) {
        this.userValidator = userValidator;
    }

    public EbQmCancellationReason getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(EbQmCancellationReason cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public EbQmIncident getPreviousIncident() {
        return previousIncident;
    }

    public void setPreviousIncident(EbQmIncident previousIncident) {
        this.previousIncident = previousIncident;
    }

    public EbUser getLastContributer() {
        return lastContributer;
    }

    public void setLastContributer(EbUser lastContributer) {
        this.lastContributer = lastContributer;
    }

    public EbUser getOtherUser() {
        return otherUser;
    }

    public void setOtherUser(EbUser otherUser) {
        this.otherUser = otherUser;
    }

    public EbUser getIssuer() {
        return issuer;
    }

    public void setIssuer(EbUser issuer) {
        this.issuer = issuer;
    }

    public List<EbQmIncidentLine> getIncidentLines() {
        return incidentLines;
    }

    public void setIncidentLines(List<EbQmIncidentLine> incidentLines) {
        this.incidentLines = incidentLines;
    }

    public String getIncidentRef() {
        return incidentRef;
    }

    public void setIncidentRef(String incidentRef) {
        this.incidentRef = incidentRef;
    }

    public String getOriginCountryLibelle() {
        return originCountryLibelle;
    }

    public void setOriginCountryLibelle(String originCountryLibelle) {
        this.originCountryLibelle = originCountryLibelle;
    }

    public String getDestinationCountryLibelle() {
        return destinationCountryLibelle;
    }

    public void setDestinationCountryLibelle(String destinationCountryLibelle) {
        this.destinationCountryLibelle = destinationCountryLibelle;
    }

    public String getCurrencyLabel() {
        return currencyLabel;
    }

    public void setCurrencyLabel(String currencyLabel) {
        this.currencyLabel = currencyLabel;
    }

    public String getLastContributerNomPremon() {
        return lastContributerNomPremon;
    }

    public void setLastContributerNomPremon(String lastContributerNomPremon) {
        this.lastContributerNomPremon = lastContributerNomPremon;
    }

    public String getIssuerNomPrenom() {
        return issuerNomPrenom;
    }

    public void setIssuerNomPrenom(String issuerNomPrenom) {
        this.issuerNomPrenom = issuerNomPrenom;
    }

    public String getCarrierNom() {
        return carrierNom;
    }

    public void setCarrierNom(String carrierNom) {
        this.carrierNom = carrierNom;
    }

    public String getCategoryLabel() {
        return categoryLabel;
    }

    public void setCategoryLabel(String categoryLabel) {
        this.categoryLabel = categoryLabel;
    }

    public String getQualificationLabel() {
        return qualificationLabel;
    }

    public void setQualificationLabel(String qualificationLabel) {
        this.qualificationLabel = qualificationLabel;
    }

    public String getDemandeTranportRef() {
        return demandeTranportRef;
    }

    public void setDemandeTranportRef(String demandeTranportRef) {
        this.demandeTranportRef = demandeTranportRef;
    }

    public EbEtablissement getxEbEtablissement() {
        return xEbEtablissement;
    }

    public void setxEbEtablissement(EbEtablissement xEbEtablissement) {
        this.xEbEtablissement = xEbEtablissement;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public String getListCategoriesStr() {
        return listCategoriesStr;
    }

    public void setListCategoriesStr(String listCategoriesStr) {
        this.listCategoriesStr = listCategoriesStr;
    }

    public String getListLabels() {
        return listLabels;
    }

    public void setListLabels(String listLabels) {
        this.listLabels = listLabels;
    }

    public Integer getModeTransport() {
        return modeTransport;
    }

    public void setModeTransport(Integer modeTransport) {
        this.modeTransport = modeTransport;
    }

    public String getListCategoryFlat() {
        return listCategoryFlat;
    }

    public void setListCategoryFlat(String listCategoryFlat) {
        this.listCategoryFlat = listCategoryFlat;
    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;

        if (this.customFields != null && !this.customFields.isEmpty()) {
            Gson gson = new Gson();
            List<CustomFields> listCustomFields = gson.fromJson(customFields, new TypeToken<ArrayList<CustomFields>>() {
            }.getType());

            this.listCustomFieldsFlat = "";

            for (CustomFields field: listCustomFields) {
                if (field.getValue() == null || field.getValue().isEmpty() || field.getName() == null
                    || field.getName().isEmpty()) continue;

                if (!this.listCustomFieldsFlat.isEmpty()) this.listCustomFieldsFlat += ";";

                this.listCustomFieldsFlat += field.getName() + ":" + "\"" + field.getValue() + "\"";
            }

            if (this.listCustomFieldsFlat.isEmpty()) this.listCustomFieldsFlat = null;
        }

    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;

        if (this.customFields != null && !this.customFields.isEmpty()) {
            Gson gson = new Gson();
            List<CustomFields> listCustomFields = gson.fromJson(customFields, new TypeToken<ArrayList<CustomFields>>() {
            }.getType());

            this.listCustomFieldsFlat = "";

            for (CustomFields field: listCustomFields) {
                if (field.getValue() == null || field.getValue().isEmpty() || field.getName() == null
                    || field.getName().isEmpty()) continue;

                if (!this.listCustomFieldsFlat.isEmpty()) this.listCustomFieldsFlat += ";";

                this.listCustomFieldsFlat += "\"" + field.getName() + "\":" + "\"" + field.getValue() + "\"";
            }

            if (this.listCustomFieldsFlat.isEmpty()) this.listCustomFieldsFlat = null;
        }

    }

    public String getListCustomFieldsFlat() {
        return listCustomFieldsFlat;
    }

    public void setListCustomFieldsFlat(String listCustomFieldsFlat) {
        this.listCustomFieldsFlat = listCustomFieldsFlat;
    }

    public Integer getxEbDemande() {
        return xEbDemande;
    }

    public void setxEbDemande(Integer xEbDemande) {
        this.xEbDemande = xEbDemande;
    }

    public String getRootCausesString() {
        return rootCausesString;
    }

    public void setRootCausesString(String rootCausesString) {
        this.rootCausesString = rootCausesString;
    }

    public List<EbQmRootCause> getRootCauses() {
        return rootCauses;
    }

    public void setRootCauses(List<EbQmRootCause> rootCauses) {
        this.rootCauses = rootCauses;
    }

    public List<EbCategorie> getListCategories() {
        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {
        this.listCategories = listCategories;

        if (this.listCategories != null && this.listCategories.size() > 0) {
            this.listCategoryFlat = "";

            for (EbCategorie cat: this.listCategories) {

                if (cat.getLabels() != null && cat.getLabels().size() > 0) {
                    if (!this.listCategoryFlat.isEmpty()) this.listCategoryFlat += ";";

                    this.listCategoryFlat += cat.getEbCategorieNum() + ":" + "\""
                        + cat.getLabels().stream().findFirst().get().getLibelle() + "\"";
                }

            }

            if (this.listCategoryFlat.isEmpty()) this.listCategoryFlat = null;
        }

    }

    public Integer getDaysDelay() {
        return daysDelay;
    }

    public void setDaysDelay(Integer daysDelay) {
        this.daysDelay = daysDelay;
    }

    public Integer getHoursDelay() {
        return hoursDelay;
    }

    public void setHoursDelay(Integer hoursDelay) {
        this.hoursDelay = hoursDelay;
    }

    public Integer getUnitDamaged() {
        return unitDamaged;
    }

    public void setUnitDamaged(Integer unitDamaged) {
        this.unitDamaged = unitDamaged;
    }

    public Double getUnitDamagedAmount() {
        return unitDamagedAmount;
    }

    public void setUnitDamagedAmount(Double unitDamagedAmount) {
        this.unitDamagedAmount = unitDamagedAmount;
    }

    public Double getImpactedTransportAmount() {
        return impactedTransportAmount;
    }

    public void setImpactedTransportAmount(Double impactedTransportAmount) {
        this.impactedTransportAmount = impactedTransportAmount;
    }

    public Integer getUnitLost() {
        return unitLost;
    }

    public void setUnitLost(Integer unitLost) {
        this.unitLost = unitLost;
    }

    public Integer getAmondOfunitLost() {
        return amondOfunitLost;
    }

    public void setAmondOfunitLost(Integer amondOfunitLost) {
        this.amondOfunitLost = amondOfunitLost;
    }

    public Double getRecoveredInsurance() {
        return recoveredInsurance;
    }

    public void setRecoveredInsurance(Double recoveredInsurance) {
        this.recoveredInsurance = recoveredInsurance;
    }

    public Integer getRootCauseStatut() {
        return rootCauseStatut;
    }

    public void setRootCauseStatut(Integer rootCauseStatut) {
        this.rootCauseStatut = rootCauseStatut;
    }

    public String getWaitingForText() {
        return waitingForText;
    }

    public void setWaitingForText(String waitingForText) {
        this.waitingForText = waitingForText;
    }

    public List<OtherCost> getOthersCost() {
        return othersCost;
    }

    public void setOthersCost(List<OtherCost> othersCost) {
        this.othersCost = othersCost;
    }

    public Double getImpactedTransportPercentage() {
        return impactedTransportPercentage;
    }

    public void setImpactedTransportPercentage(Double impactedTransportPercentage) {
        this.impactedTransportPercentage = impactedTransportPercentage;
    }

    public List<EbTypeDocuments> getListTypeDocuments() {
        return listTypeDocuments;
    }

    public void setListTypeDocuments(List<EbTypeDocuments> listTypeDocuments) {
        this.listTypeDocuments = listTypeDocuments;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getListFlag() {
        return listFlag;
    }

    public void setListFlag(String listFlag) {
        this.listFlag = listFlag;
    }

    public List<EbFlagDTO> getListEbFlagDTO() {
        return listEbFlagDTO;
    }

    public void setListEbFlagDTO(List<EbFlagDTO> listEbFlagDTO) {
        this.listEbFlagDTO = listEbFlagDTO;
    }

    public Integer getSeverity() {
        return severity;
    }

    public void setSeverity(Integer severity) {
        this.severity = severity;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    public Integer getCriticality() {
        return criticality;
    }

    public void setCriticality(Integer criticality) {
        this.criticality = criticality;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public Integer getRootCauseCategory() {
        return rootCauseCategory;
    }

    public void setRootCauseCategory(Integer rootCauseCategory) {
        this.rootCauseCategory = rootCauseCategory;
    }

    public String getListRootCauses() {
        return listRootCauses;
    }

    public void setListRootCauses(String listRootCauses) {
        this.listRootCauses = listRootCauses;
    }

    public String getUnitRef() {
        return unitRef;
    }

    public EbTtTracing getxEbTtTracing() {
        return xEbTtTracing;
    }

    public void setUnitRef(String unitRef) {
        this.unitRef = unitRef;
    }

    public void setxEbTtTracing(EbTtTracing xEbTtTracing) {
        this.xEbTtTracing = xEbTtTracing;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    public String getPsl() {
        return psl;
    }

    public void setPsl(String psl) {
        this.psl = psl;
    }

    public String getTypeRequestLibelle() {
        return typeRequestLibelle;
    }

    public void setTypeRequestLibelle(String typeRequestLibelle) {
        this.typeRequestLibelle = typeRequestLibelle;
    }

    public Integer getTypeRequestNum() {
        return typeRequestNum;
    }

    public void setTypeRequestNum(Integer typeRequestNum) {
        this.typeRequestNum = typeRequestNum;
    }

    public Integer getEventTypeStatus() {
        return eventTypeStatus;
    }

    public void setEventTypeStatus(Integer eventTypeStatus) {
        this.eventTypeStatus = eventTypeStatus;
    }

    public String getOwnerNomPrenom() {
        return ownerNomPrenom;
    }

    public void setOwnerNomPrenom(String ownerNomPrenom) {
        this.ownerNomPrenom = ownerNomPrenom;
    }

    public Integer getOwnerNum() {
        return ownerNum;
    }

    public void setOwnerNum(Integer ownerNum) {
        this.ownerNum = ownerNum;
    }

    @PrePersist
    public void updateDateIncident() {

        if (dateIncident == null) {
            dateIncident = new Date();
        }

    }
}
