package com.adias.mytowereasy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.querydsl.core.annotations.QueryProjection;


@Entity
@Table(name = "ec_acl_rule", schema = "work", indexes = {
    @Index(name = "idx_ec_acl_rule_code", columnList = "code"),
    @Index(name = "idx_ec_acl_rule_visible", columnList = "visible"),
    @Index(name = "idx_ec_acl_rule_visible_short_view", columnList = "visible_short_view"),
    @Index(name = "idx_ec_acl_rule_experimental", columnList = "experimental")
})
public class EcAclRule {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ecAclRuleNum;

    @Column(nullable = false)
    private Integer type;

    private String themes;

    @Column(nullable = false, unique = true)
    private String code;

    private String values;

    @Column(nullable = false)
    private String defaultValue;

    @Column(nullable = false)
    @ColumnDefault("true")
    private Boolean visible;

    @Column(name = "visible_short_view", nullable = false)
    @ColumnDefault("false")
    private Boolean visibleShortView;

    @Column(nullable = false)
    @ColumnDefault("false")
    private Boolean experimental;

    public EcAclRule() {
        // empty constructor
    }

    public EcAclRule(Integer ecAclRuleNum) {
        this.ecAclRuleNum = ecAclRuleNum;
    }

    /**
     * Projection used to retrieve only required field for session
     */
    @QueryProjection
    public EcAclRule(Integer ecAclRuleNum, Integer type, String code, String defaultValue) {
        this.ecAclRuleNum = ecAclRuleNum;
        this.type = type;
        this.code = code;
        this.defaultValue = defaultValue;
    }

    public Integer getEcAclRuleNum() {
        return ecAclRuleNum;
    }

    public void setEcAclRuleNum(Integer ecAclRuleNum) {
        this.ecAclRuleNum = ecAclRuleNum;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getThemes() {
        return themes;
    }

    public void setThemes(String themes) {
        this.themes = themes;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Boolean getVisibleShortView() {
        return visibleShortView;
    }

    public void setVisibleShortView(Boolean visibleShortView) {
        this.visibleShortView = visibleShortView;
    }

    public Boolean getExperimental() {
        return experimental;
    }

    public void setExperimental(Boolean experimental) {
        this.experimental = experimental;
    }
}
