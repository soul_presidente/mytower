package com.adias.mytowereasy.model;

import java.util.Date;

import javax.persistence.*;


@Entity
@Table(name = "eb_user_themes", schema = "work")
public class EbUserThemes {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebUserThemesNum;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_companie", insertable = true, updatable = false)
    private EbCompagnie xEbCompagnie;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user", insertable = true, updatable = false)
    private EbUser xEbUser;

    @Column(columnDefinition = "TEXT")
    private String activeTheme;

    @Temporal(TemporalType.DATE)
    private Date dateMaj;

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public EbUser getxEbUser() {
        return xEbUser;
    }

    public String getActiveTheme() {
        return activeTheme;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public void setxEbUser(EbUser xEbUser) {
        this.xEbUser = xEbUser;
    }

    public void setActiveTheme(String activeTheme) {
        this.activeTheme = activeTheme;
    }

    public Integer getEbUserThemesNum() {
        return ebUserThemesNum;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setEbUserThemesNum(Integer ebUserThemesNum) {
        this.ebUserThemesNum = ebUserThemesNum;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }
}
