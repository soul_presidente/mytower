package com.adias.mytowereasy.model;

import org.apache.poi.ss.usermodel.CellStyle;


public class ExcelToHeaderConfig {
    Integer numberOfRow;
    String title;
    CellStyle cellStyle;

    public ExcelToHeaderConfig(Integer numberOfRow, String title, CellStyle cellStyle) {
        this.numberOfRow = numberOfRow;
        this.title = title;
        this.cellStyle = cellStyle;
    }

    public Integer getNumberOfRow() {
        return numberOfRow;
    }

    public String getTitle() {
        return title;
    }

    public void setNumberOfRow(Integer numberOfRow) {
        this.numberOfRow = numberOfRow;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CellStyle getCellStyle() {
        return cellStyle;
    }

    public void setCellStyle(CellStyle cellStyle) {
        this.cellStyle = cellStyle;
    }
}
