/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.analytics;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "dwr_d_month")
public class DwrDMonth {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dwr_d_month")
    private Integer dwrDMonthNum;

    @Column(name = "month")
    private Integer month;

    @Column(name = "year")
    private Integer year;

    @OneToMany(mappedBy = "dateInitiated", cascade = CascadeType.ALL)
    private List<DwrFShipments> shipmentsInitiated = new ArrayList<DwrFShipments>();

    @OneToMany(mappedBy = "datePickup", cascade = CascadeType.ALL)
    private List<DwrFShipments> shipmentsPickedUp = new ArrayList<DwrFShipments>();

    @OneToMany(mappedBy = "dateDelivered", cascade = CascadeType.ALL)
    private List<DwrFShipments> shipmentsDelivered = new ArrayList<DwrFShipments>();

    @OneToMany(mappedBy = "datePickup", cascade = CascadeType.ALL)
    private List<DwrFShipmentsIncident> shipmentsIncidentPickedUp = new ArrayList<DwrFShipmentsIncident>();

    @OneToMany(mappedBy = "dateDelivered", cascade = CascadeType.ALL)
    private List<DwrFShipmentsIncident> shipmentsIncidentDelivered = new ArrayList<DwrFShipmentsIncident>();

    @OneToMany(mappedBy = "dateInitiated", cascade = CascadeType.ALL)
    private List<DwrFShipmentsIncident> shipmentsIncidentInitiated = new ArrayList<DwrFShipmentsIncident>();

    public Integer getID() {
        return this.dwrDMonthNum;
    }

    public Integer getMonth() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.month;
    }

    public void setMonth(Integer value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.month = value;
    }

    public Integer getYear() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.year;
    }

    public void setYear(Integer value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.year = value;
    }

    public List<DwrFShipments> getShipmentsInitiated() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.shipmentsInitiated;
    }

    public void setShipmentsInitiated(List<DwrFShipments> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.shipmentsInitiated = value;
    }

    public List<DwrFShipmentsIncident> getShipmentsIncidentInitiated() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.shipmentsIncidentInitiated;
    }

    public void setShipmentsIncidentInitiated(List<DwrFShipmentsIncident> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.shipmentsIncidentInitiated = value;
    }

    public List<DwrFShipments> getShipmentsPickedUp() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.shipmentsPickedUp;
    }

    public void setShipmentsPickedUp(List<DwrFShipments> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.shipmentsPickedUp = value;
    }

    public List<DwrFShipments> getShipmentsDelivered() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.shipmentsDelivered;
    }

    public void setShipmentsDelivered(List<DwrFShipments> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.shipmentsDelivered = value;
    }

    public List<DwrFShipmentsIncident> getShipmentsIncidentPickedUp() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.shipmentsIncidentPickedUp;
    }

    public void setShipmentsIncidentPickedUp(List<DwrFShipmentsIncident> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.shipmentsIncidentPickedUp = value;
    }

    public void setShipmentsIncidentDelivered(List<DwrFShipmentsIncident> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.shipmentsIncidentDelivered = value;
    }

    public List<DwrFShipmentsIncident> getShipmentsIncidentDelivered() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.shipmentsIncidentDelivered;
    }
}
