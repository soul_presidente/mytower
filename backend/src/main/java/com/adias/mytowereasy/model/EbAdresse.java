/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.dock.dto.EbEntrepotDTO;
import com.adias.mytowereasy.dock.model.EbEntrepot;
import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.types.JsonBinaryType;


@Entity
@Table(name = "eb_adresse", schema = "work")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class EbAdresse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebAdresseNum;

    private String reference;

    private String city;

    private String zipCode;

    private String street;

    private String airport;

    private Integer type;

    private String email;

    private String phone;

    private String company;
    private String state;
    @Transient
    @JsonSerialize
    @JsonDeserialize
    private Boolean isUpdateUserVisibility;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbPlZone> zones;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_default_zone")
    private EbPlZone defaultZone;

    // visibilité des etablissements (origin/destination)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_visibility", insertable = true)
    private EbUser xEbUserVisibility;

    // visibilité des adresses dans le formulaire du shipping information
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbEtablissement> etablissementAdresse;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company", insertable = true)
    private EbCompagnie ebCompagnie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user", insertable = true)
    private EbUser createdBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country", insertable = true)
    private EcCountry country;

    // @Type(type = "jsonb")
    // @Column(columnDefinition = "text")
    // private Horaire openingHours;

    @Column(insertable = false, updatable = false)
    @ColumnDefault("now()")
    private Date dateAjout;

    private String openingHoursFreeText;

    @Column(columnDefinition = "TEXT")
    private String commentaire;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "x_eb_entrepot")
    private EbEntrepot entrepot;

    @Column(columnDefinition = "TEXT")
    private String listEgibility;

    @Column
    private String listEtablissementsNum; // to simplify etablissements querying

    @Column
    private String listZonesNum; // to simplify zones querying

    public String getListEgibility() {
        return listEgibility;
    }

    public void setListEgibility(String listEgibility) {
        this.listEgibility = listEgibility;
    }

    public EbEntrepot getEntrepot() {
        return entrepot;
    }

    public void setEntrepot(EbEntrepot entrepot) {
        this.entrepot = entrepot;
    }

    public EbAdresse() {
    }

    @QueryProjection
    public EbAdresse(
        String airport,
        String city,
        String commentaire,
        String company,
        Date dateAjout,
        Integer ebAdresseNum,
        String email,
        String openingHoursFreeText,
        String phone,
        String reference,
        String street,
        Integer type,
        String zipCode,
        String listEgibility,
        List<EbEtablissement> etablissementAdresse,
        Integer countryNum,
        String countryCode,
        String countryCodeLibelle,
        String countryLibelle,
        Integer ebCompagnieNum,
        String ebCompagnieNom,
        String ebCompagnieCode,
        Integer xEbUserVisibilityNum,
        String xEbUserVisibilityNom,
        Integer xEbUserVisibilityEtablissementNum,
        String xEbUserVisibilityEtablissementNom,
        List<EbPlZone> zones,
        Integer ebDefaultZoneNum,
        String defaultZoneRef,
        String defaultZoneDesignation,
        String state,
        String listEtablissementsNum,
        String listZonesNum,
        Integer ebEntrepotNum,
        String libelle) {
        this.airport = airport;
        this.city = city;
        this.commentaire = commentaire;
        this.company = company;
        this.dateAjout = dateAjout;
        this.ebAdresseNum = ebAdresseNum;

        this.email = email;
        this.openingHoursFreeText = openingHoursFreeText;
        this.phone = phone;
        this.reference = reference;

        this.street = street;
        this.type = type;
        this.zipCode = zipCode;
        this.listEgibility = listEgibility;
        this.country = new EcCountry();
        this.country.setEcCountryNum(countryNum);
        this.country.setCode(countryCode);
        this.country.setCodeLibelle(countryCodeLibelle);
        this.country.setLibelle(countryLibelle);
        this.ebCompagnie = new EbCompagnie(ebCompagnieNum, ebCompagnieNom, ebCompagnieCode);
        this.xEbUserVisibility = new EbUser();
        this.xEbUserVisibility.setEbUserNum(xEbUserVisibilityNum);
        this.xEbUserVisibility.setNom(xEbUserVisibilityNom);
        this.xEbUserVisibility
            .setEbEtablissement(
                new EbEtablissement(xEbUserVisibilityEtablissementNum, xEbUserVisibilityEtablissementNom));
        this.etablissementAdresse = this.serializeListEtablissements(etablissementAdresse);
        this.zones = this.serializeListZones(zones);

        if (ebDefaultZoneNum != null) {
            this.defaultZone = new EbPlZone();
            this.defaultZone.setEbZoneNum(ebDefaultZoneNum);
            this.defaultZone.setRef(defaultZoneRef);
            this.defaultZone.setDesignation(defaultZoneDesignation);
        }

        this.state = state;
        this.listEtablissementsNum = listEtablissementsNum;
        this.listZonesNum = listZonesNum;

        if (ebEntrepotNum != null) {
            EbEntrepotDTO entrepotdto = new EbEntrepotDTO();
            entrepotdto.setEbEntrepotNum(ebEntrepotNum);
            entrepotdto.setLibelle(libelle);
            this.entrepot = new EbEntrepot(entrepotdto);
        }

    }

    public EbAdresse(Object zones) {
        this.zones = this.serializeListZones(zones);
    }

    @QueryProjection
    public EbAdresse(
        String airport,
        String city,
        String commentaire,
        String company,
        Date dateAjout,
        Integer ebAdresseNum,
        String email,
        String openingHoursFreeText,
        String phone,
        String reference,
        String street,
        Integer type,
        String zipCode,
        String listEgibility,
        Integer ebCompagnieNum,
        String ebCompagnieNom,
        String ebCompagnieCode,
        String state) {
        this.airport = airport;
        this.city = city;
        this.commentaire = commentaire;
        this.company = company;
        this.dateAjout = dateAjout;
        this.ebAdresseNum = ebAdresseNum;

        this.email = email;
        this.openingHoursFreeText = openingHoursFreeText;
        this.phone = phone;
        this.reference = reference;

        this.street = street;
        this.type = type;
        this.zipCode = zipCode;
        this.listEgibility = listEgibility;
        this.ebCompagnie = new EbCompagnie(ebCompagnieNum, ebCompagnieNom, ebCompagnieCode);
        this.state = state;
    }

    @QueryProjection
    public EbAdresse(Integer ebAdresseNum, List<EbPlZone> zones, String listZonesNum) {
        this.ebAdresseNum = ebAdresseNum;
        this.zones = this.serializeListZones(zones);
        this.listZonesNum = listZonesNum;
    }

    public Integer getEbAdresseNum() {
        return ebAdresseNum;
    }

    public void setEbAdresseNum(Integer ebAdresseNum) {
        this.ebAdresseNum = ebAdresseNum;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public EcCountry getCountry() {
        return country;
    }

    public void setCountry(EcCountry country) {
        this.country = country;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    public EbUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(EbUser createdBy) {
        this.createdBy = createdBy;
    }

    public String getOpeningHoursFreeText() {
        return openingHoursFreeText;
    }

    public void setOpeningHoursFreeText(String openingHoursFreeText) {
        this.openingHoursFreeText = openingHoursFreeText;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public List<EbPlZone> getZones() {
        return zones;
    }

    public void setZones(List<EbPlZone> zone) {
        this.zones = zone;
    }

    public List<EbEtablissement> getEtablissementAdresse() {
        return etablissementAdresse;
    }

    public void setEtablissementAdresse(List<EbEtablissement> etablissementAdresse) {
        this.etablissementAdresse = etablissementAdresse;
    }

    public EbUser getxEbUserVisibility() {
        return xEbUserVisibility;
    }

    public void setxEbUserVisibility(EbUser xEbUserVisibility) {
        this.xEbUserVisibility = xEbUserVisibility;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((airport == null) ? 0 : airport.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result + ((dateAjout == null) ? 0 : dateAjout.hashCode());
        result = prime * result + ((ebAdresseNum == null) ? 0 : ebAdresseNum.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((openingHoursFreeText == null) ? 0 : openingHoursFreeText.hashCode());
        result = prime * result + ((phone == null) ? 0 : phone.hashCode());
        result = prime * result + ((street == null) ? 0 : street.hashCode());
        result = prime * result + ((reference == null) ? 0 : reference.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
        result = prime * result + ((company == null) ? 0 : company.hashCode());
        result = prime * result + ((commentaire == null) ? 0 : commentaire.hashCode());
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;

        if (getClass() != obj.getClass()) return false;
        EbAdresse other = (EbAdresse) obj;

        if (airport == null) {
            if (other.airport != null) return false;
        }
        else if (!airport.equals(other.airport)) return false;

        if (city == null) {
            if (other.city != null) return false;
        }
        else if (!city.equals(other.city)) return false;

        if (country == null) {
            if (other.country != null) return false;
        }
        else if (!country.equals(other.country)) return false;

        if (dateAjout == null) {
            if (other.dateAjout != null) return false;
        }
        else if (!dateAjout.equals(other.dateAjout)) return false;

        if (ebAdresseNum == null) {
            if (other.ebAdresseNum != null) return false;
        }
        else if (!ebAdresseNum.equals(other.ebAdresseNum)) return false;

        if (email == null) {
            if (other.email != null) return false;
        }
        else if (!email.equals(other.email)) return false;

        if (openingHoursFreeText == null) {
            if (other.openingHoursFreeText != null) return false;
        }
        else if (!openingHoursFreeText.equals(other.openingHoursFreeText)) return false;

        if (phone == null) {
            if (other.phone != null) return false;
        }
        else if (!phone.equals(other.phone)) return false;

        if (street == null) {
            if (other.street != null) return false;
        }
        else if (!street.equals(other.street)) return false;

        if (reference == null) {
            if (other.reference != null) return false;
        }
        else if (!reference.equals(other.reference)) return false;

        if (type == null) {
            if (other.type != null) return false;
        }
        else if (!type.equals(other.type)) return false;

        if (zipCode == null) {
            if (other.zipCode != null) return false;
        }
        else if (!zipCode.equals(other.zipCode)) return false;

        if (state == null) {
            if (other.state != null) return false;
        }
        else if (!state.equals(other.state)) return false;

        if (company == null) {
            if (other.company != null) return false;
        }
        else if (!company.equals(other.company)) return false;

        if (commentaire == null) {
            if (other.commentaire != null) return false;
        }
        else if (!commentaire.equals(other.commentaire)) return false;

        return true;
    }

    public Boolean getIsUpdateUserVisibility() {
        return isUpdateUserVisibility;
    }

    public void setIsUpdateUserVisibility(Boolean isUpdateUserVisibility) {
        this.isUpdateUserVisibility = isUpdateUserVisibility;
    }

    public String getListEtablissementsNum() {
        return listEtablissementsNum;
    }

    public void setListEtablissementsNum(String listEtablissementsNum) {
        this.listEtablissementsNum = listEtablissementsNum;
    }

    private List<EbEtablissement> serializeListEtablissements(Object listEtabs) {

        try {
            Gson gson = new Gson();
            String listAsJson = gson.toJson(listEtabs);
            return gson.fromJson(listAsJson, new TypeToken<ArrayList<EbEtablissement>>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private List<EbPlZone> serializeListZones(Object listZones) {

        try {
            Gson gson = new Gson();
            String listAsJson = gson.toJson(listZones);
            return gson.fromJson(listAsJson, new TypeToken<ArrayList<EbPlZone>>() {
            }.getType());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public EbPlZone getDefaultZone() {
        return defaultZone;
    }

    public void setDefaultZone(EbPlZone defaultZone) {
        this.defaultZone = defaultZone;
    }

    public String getListZonesNum() {
        return listZonesNum;
    }

    public void setListZonesNum(String listZonesNum) {
        this.listZonesNum = listZonesNum;
    }
}
