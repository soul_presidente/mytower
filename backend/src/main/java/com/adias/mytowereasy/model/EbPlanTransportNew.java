package com.adias.mytowereasy.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.*;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.chanel.model.EbWeekTemplate;
import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.types.JsonBinaryType;
import com.adias.mytowereasy.util.ObjectDeserializer;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


/**
 * Planning de Chargement
 */
@Table(name = "eb_plan_transport_new", indexes = {
    @Index(name = "idx_eb_plan_transport_new_x_eb_compagnie_chargeur", columnList = "x_eb_compagnie_chargeur"),
    @Index(name = "idx_eb_plan_transport_new_x_eb_compagnie_carrier", columnList = "x_eb_compagnie_carrier"),
    @Index(name = "idx_eb_plan_transport_new_input", columnList = "input")
})
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class EbPlanTransportNew {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebPlanTransportNum;
    private String reference;
    private String label;
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonDeserialize
    private SearchCriteriaPricingBooking input;
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonDeserialize
    private EbWeekTemplate weekTemplate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie_chargeur")
    private EbCompagnie xEbCompagnieChargeur;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie_carrier")
    private EbCompagnie xEbCompagnieCarrier;
    private Boolean deleted;
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private EbWeekTemplate week;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbFlagDTO> listEbFlagDTO;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCategorie> listCategories;

    @Column(columnDefinition = "TEXT")
    private String listCategoryFlat;

    @Column(columnDefinition = "TEXT")
    private String listCategoriesStr;

    private String listLabels;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    public EbPlanTransportNew(
        Integer ebPlanTransportNum,
        String reference,
        String label,
        SearchCriteriaPricingBooking input,
        EbWeekTemplate weekTemplate,
        EbCompagnie xEbCompagnieChargeur,
        EbCompagnie xEbCompagnieCarrier,
        Boolean deleted) {
        super();
        this.ebPlanTransportNum = ebPlanTransportNum;
        this.reference = reference;
        this.label = label;
        this.input = input;
        this.weekTemplate = weekTemplate;
        this.xEbCompagnieChargeur = xEbCompagnieChargeur;
        this.xEbCompagnieCarrier = xEbCompagnieCarrier;
        this.deleted = deleted;
    }

    public EbPlanTransportNew() {
    }

    @QueryProjection
    public EbPlanTransportNew(
        Integer ebPlanTransportNum,
        String reference,
        String label,
        Integer xEbCompagnieNum,
        String xEbCompagnieCode,
        String xEbCompagnieNom,
        Integer xEbCompagnieNumChargeur,
        String xEbCompagnieCodeChargeur,
        String xEbCompagnieNomChargeur,
        SearchCriteriaPricingBooking input,
        EbWeekTemplate weekTemplate,
        List<EbCategorie> listCategories) {
        this.ebPlanTransportNum = ebPlanTransportNum;
        this.reference = reference;
        this.label = label;
        this.xEbCompagnieCarrier = new EbCompagnie(xEbCompagnieNum, xEbCompagnieNom, xEbCompagnieCode);
        this.xEbCompagnieChargeur = new EbCompagnie(
            xEbCompagnieNumChargeur,
            xEbCompagnieNomChargeur,
            xEbCompagnieCodeChargeur);

        if (input != null) {

            try {
                ObjectMapper objectMapper = new ObjectMapper();
                String json = objectMapper.writeValueAsString(input);
                this.input = objectMapper.readValue(json, new TypeReference<SearchCriteriaPricingBooking>() {
                });
            } catch (Exception e) {
                System.err.println("ConnectedUser: failed to convert object to input fields");
            }

        }

        if (weekTemplate != null) {

            try {
                ObjectMapper objectMapper = new ObjectMapper();
                String json = objectMapper.writeValueAsString(weekTemplate);
                this.weekTemplate = objectMapper.readValue(json, new TypeReference<EbWeekTemplate>() {
                });
            } catch (Exception e) {
                System.err.println("ConnectedUser: failed to convert object to input fields");
            }

        }

        this.listCategories = listCategories;
    }

    public List<EbFlagDTO> getListEbFlagDTO() {
        return listEbFlagDTO;
    }

    public void setListEbFlagDTO(List<EbFlagDTO> listEbFlagDTO) {
        this.listEbFlagDTO = listEbFlagDTO;
    }

    public EbWeekTemplate getWeekTemplate() {
        return weekTemplate;
    }

    public void setWeekTemplate(EbWeekTemplate weekTemplate) {
        this.weekTemplate = weekTemplate;
    }

    public Integer getEbPlanTransportNum() {
        return ebPlanTransportNum;
    }

    public void setEbPlanTransportNum(Integer ebPlanTransportNum) {
        this.ebPlanTransportNum = ebPlanTransportNum;
    }

    public SearchCriteriaPricingBooking getInput() {
        return input;
    }

    public void setInput(SearchCriteriaPricingBooking input) {
        this.input = input;
    }

    public EbCompagnie getxEbCompagnieChargeur() {
        return xEbCompagnieChargeur;
    }

    public void setxEbCompagnieChargeur(EbCompagnie xEbCompagnieChargeur) {
        this.xEbCompagnieChargeur = xEbCompagnieChargeur;
    }

    public EbCompagnie getxEbCompagnieCarrier() {
        return xEbCompagnieCarrier;
    }

    public void setxEbCompagnieCarrier(EbCompagnie xEbCompagnieCarrier) {
        this.xEbCompagnieCarrier = xEbCompagnieCarrier;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getListLabels() {
        return listLabels;
    }

    public void setListLabels(String listLabels) {
        this.listLabels = listLabels;
    }

    public String getListCategoriesStr() {
        return listCategoriesStr;
    }

    public void setListCategoriesStr(String listCategoriesStr) {
        this.listCategoriesStr = listCategoriesStr;
    }

    public String getListCategoryFlat() {
        return listCategoryFlat;
    }

    public void setListCategoryFlat(String listCategoryFlat) {
        this.listCategoryFlat = listCategoryFlat;
    }

    public List<EbCategorie> getListCategories() {

        if (listCategories != null && listCategories.size() > 0) {

            try {
                this.listCategories = ObjectDeserializer
                    .checkJsonListObject(listCategories, new TypeToken<List<EbCategorie>>() {
                    }.getType());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {

        if (listCategories != null) {
            this.listCategories = listCategories
                .stream().map(it -> EbCategorie.getEbCategorieLite(it)).collect(Collectors.toList());

            // pour deserialiser la liste et eviter l'erreur linked hashMap
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String jsonCateg = objectMapper.writeValueAsString(listCategories);
                this.listCategories = objectMapper.readValue(jsonCateg, new TypeReference<ArrayList<EbCategorie>>() {
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (this.listCategories != null && this.listCategories.size() > 0) {
                this.listCategoryFlat = "";
                this.listLabels = "";
                this.listCategoriesStr = "";

                for (EbCategorie cat: this.listCategories) {
                    if (!this.listCategoriesStr.isEmpty()) this.listCategoriesStr += ",";
                    this.listCategoriesStr += cat.getEbCategorieNum();

                    if (cat.getLabels() != null && cat.getLabels().size() > 0) {

                        if (!this.listCategoryFlat.isEmpty()) {
                            this.listCategoryFlat += ";";
                            this.listLabels += ",";
                        }

                        for (EbLabel lab: cat.getLabels()) {
                            // le cas de plusieurs libelles
                            if (!this.listLabels.isEmpty()
                                && this.listLabels.charAt(this.listLabels.length() - 1) != ',') this.listLabels += ",";

                            this.listLabels += lab.getEbLabelNum();
                        }

                        this.listCategoryFlat += cat.getEbCategorieNum() + ":" + "\""
                            + cat.getLabels().stream().findFirst().get().getLibelle() + "\"";
                    }

                }

                if (this.listCategoryFlat.isEmpty()) this.listCategoryFlat = null;
                if (this.listCategoriesStr.isEmpty()) this.listCategoriesStr = null;

                if (this.listLabels.isEmpty()) {
                    this.listLabels = null;
                }
                else {
                    // le trie des labels est naicessaire pour la partie
                    // groupage
                    List<Integer> listLabelsArray = Arrays
                        .asList(this.listLabels.split(",")).stream().map(Integer::parseInt).sorted()
                        .collect(Collectors.toList());
                    this.listLabels = StringUtils.join(listLabelsArray, ",");
                }

            }

        }
        else {
            this.listCategories = listCategories;
        }

    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public EbWeekTemplate getWeek() {
        return week;
    }

    public void setWeek(EbWeekTemplate week) {
        this.week = week;
    }
}
