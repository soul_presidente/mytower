/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.qm;

import javax.persistence.*;

import com.adias.mytowereasy.model.EbEtablissement;


/** EcQmCategory entity. @author Mohamed */
@Entity
// @Table(indexes = { @Index(name = "ecQmCategorieNumIdx", columnList =
// "qmCategorieNum"),
// @Index(name = "ecQmCategoryEtablissementIdx", columnList = "x_eb_etab") })
@Table(name = "eb_qm_category", schema = "work")
public class EbQmCategory implements java.io.Serializable {
    private static final long serialVersionUID = -585217246406649061L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer qmCategorieNum;

    @Column
    private String label;

    @Column
    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab")
    private EbEtablissement etablissement;

    public EbQmCategory() {
    }

    public EbQmCategory(Integer qmCategorieNum, String label) {
        super();
        this.qmCategorieNum = qmCategorieNum;
        this.label = label;
    }

    public Integer getQmCategorieNum() {
        return qmCategorieNum;
    }

    public void setQmCategorieNum(Integer qmCategorieNum) {
        this.qmCategorieNum = qmCategorieNum;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public EbEtablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(EbEtablissement etablissement) {
        this.etablissement = etablissement;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((qmCategorieNum == null) ? 0 : qmCategorieNum.hashCode());
        result = prime * result + ((etablissement == null) ? 0 : etablissement.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbQmCategory other = (EbQmCategory) obj;

        if (code == null) {
            if (other.code != null) return false;
        }
        else if (!code.equals(other.code)) return false;

        if (qmCategorieNum == null) {
            if (other.qmCategorieNum != null) return false;
        }
        else if (!qmCategorieNum.equals(other.qmCategorieNum)) return false;

        if (etablissement == null) {
            if (other.etablissement != null) return false;
        }
        else if (!etablissement.equals(other.etablissement)) return false;

        if (label == null) {
            if (other.label != null) return false;
        }
        else if (!label.equals(other.label)) return false;

        return true;
    }
}
