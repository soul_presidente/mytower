/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;

import com.adias.mytowereasy.util.GenericEnum;


public class TtEnumeration {
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum TtPsl {
        WPU(0, "Waiting for pickup", 0, "WPU"),
        PICKUP(1, "Pickup", 1, "PIC"),
        DEPARTURE(2, "Departure", 2, "DEP"),
        ARRIVAL(3, "Arrival", 3, "ARR"),
        CUSTOMS(4, "Customs", 4, "CUS"),
        DELIVERY(5, "Delivery", 5, "DEL"),
        UNLOADING(6, "Unloading", 6, "UNL");

        private Integer code;
        private String libelle;
        private Integer ordre;
        private String codeAlpha;

        private TtPsl(int code, String libelle, Integer ordre, String codeAlpha) {
            this.code = code;
            this.libelle = libelle;
            this.ordre = ordre;
            this.codeAlpha = codeAlpha;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public String getCodeAlpha() {
            return codeAlpha;
        }

        public int getOrdre() {
            return ordre;
        }

        public static TtPsl getPslByLibelle(String libelle) {
            TtPsl val = null;

            for (TtPsl item: TtPsl.values()) {

                if (item.libelle.toLowerCase().contains(libelle.toLowerCase())) {
                    val = item;
                    break;
                }

            }

            return val;
        }

        public static TtPsl getPslByCodeAlpha(String codeAlpha) {
            TtPsl val = null;

            for (TtPsl item: TtPsl.values()) {

                if (item.codeAlpha.toLowerCase().contains(codeAlpha.toLowerCase())) {
                    val = item;
                    break;
                }

            }

            return val;
        }

        public static TtPsl getPslByCode(Integer code) {
            TtPsl val = null;

            for (TtPsl item: TtPsl.values()) {

                if (item.code.equals(code)) {
                    val = item;
                    break;
                }

            }

            return val;
        }

        public static Integer getCodeByLibelle(String libelle) {

            if (libelle != null) {
                Integer ttPsl = null;
                String value = libelle.toLowerCase();

                for (TtPsl item: TtPsl.values()) {
                    if (item.libelle.toLowerCase().contains(value)) ttPsl = item.code;
                }

                return ttPsl;
            }

            return null;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                String value = null;

                for (TtPsl item: TtPsl.values()) {
                    if (item.code == code) value = item.libelle;
                }

                return value;
            }

            return null;
        }

        public static List<TtPsl> getListByLibelle(String libelle) {

            if (libelle != null && !libelle.isEmpty()) {
                List<TtPsl> listCode = new ArrayList<TtPsl>();
                String value = libelle.toLowerCase();

                for (TtPsl item: TtPsl.values()) {
                    if (item.libelle.toLowerCase().contains(value)) listCode.add(item);
                }

                return listCode;
            }

            return null;
        }

        public static String getList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (TtPsl item: TtPsl.values()) {
                arrayNode.add(mapper.readTree(mapper.writeValueAsString(item)));
            }

            return arrayNode.toString();
        }

        public static List<TtPsl> getListPsl() throws IOException {
            List<TtPsl> listPsl = Arrays.asList(TtPsl.values());
            return listPsl;
        }
    }

    public enum NatureDateEvent implements GenericEnum {
        DATE_ACTUAL(1, "Actual"),
        DATE_ESTIMATED(2, "Estimated"),
        DATE_NEGOTIATED(3, "Negotiated"),
        NOW(4, "NOW"),
        DATE_CHAMP(5, "CHAMP");
        ;

        private Integer code;
        private String libelle;

        private NatureDateEvent(int code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static NatureDateEvent getNatureDateEventByCode(Integer code) {
            NatureDateEvent val = null;

            for (NatureDateEvent item: NatureDateEvent.values()) {

                if (item.code == code) {
                    val = item;
                    break;
                }

            }

            return val;
        }

        @Override
        public String getKey() {
            return libelle;
        }
    }

    public enum TypeEvent {
        EVENT_DEVIATION(1, "Deviation"), EVENT_PSL(2, "PSL"), UNCONFORMITY(3, "UNCONFORMITY");

        private Integer code;
        private String libelle;

        private TypeEvent(int code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static TypeEvent getTypeEventByCode(Integer code) {
            TypeEvent val = null;

            for (TypeEvent item: TypeEvent.values()) {

                if (item.code == code) {
                    val = item;
                    break;
                }

            }

            return val;
        }
    }

    public enum TtDeviationCategorie {
        MISSING_OR_DAMAGED_GOODS(1, "Missing or damaged goods"),
        INFORMATION_FLOW_ISSUES(2, "Information flow issues"),
        OPERATIONAL_ISSUES(3, "Operational issues"),
        OTHER_ISSUES(4, "Other issues"),
        CUSTOMS_ISSUE(5, "Customs issue"),
        GOODS_AVAILIBILITY_ISSUES(6, "Goods availibility issues"),
        HAZARDOUS_GOODS_ISSUES(7, "Hazardous goods issues"),
        FORCE_MAJEURE(8, "Force majeure"),
        COLD_CHAIN_ISSUES(9, "Cold chain issues");

        private Integer code;
        private String libelle;

        private TtDeviationCategorie(int code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public int getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static TtDeviationCategorie getDeviationCategorieByLibelle(String libelle) {
            TtDeviationCategorie val = null;

            for (TtDeviationCategorie item: TtDeviationCategorie.values()) {

                if (item.libelle.toLowerCase().contains(libelle.toLowerCase())) {
                    val = item;
                    break;
                }

            }

            return val;
        }
    }

    public enum Late {
        LATE_TRACK(1, "Yes"), NOT_LATE(2, "No");

        private Integer code;
        private String libelle;

        private Late(int code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public int getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum PslImportance {
        MAJEUR(1, "Majeur"), MINEUR(2, "Mineur");

        private Integer code;
        private String libelle;

        private PslImportance(int code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public int getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum NatureReference implements GenericEnum {
        UNIT_REF("Unit ref", 0), TRANSPORT_REF("Transport ref", 1), CUSTOMER_REF("Customer ref", 2);

        private Integer code;
        private String key;

        private NatureReference(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }
    }
}
