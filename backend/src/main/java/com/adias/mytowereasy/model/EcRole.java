/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import javax.persistence.*;


@Entity
@Table(name = "ec_role", schema = "work")
public class EcRole implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ec_role_num")
    private Integer ecRoleNum;

    @Column
    private String libelle;

    public EcRole() {
    }

    public EcRole(Integer ecRoleNum) {
        this.ecRoleNum = ecRoleNum;
    }

    public Integer getEcRoleNum() {
        return ecRoleNum;
    }

    public void setEcRoleNum(Integer ecRoleNum) {
        this.ecRoleNum = ecRoleNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ecRoleNum == null) ? 0 : ecRoleNum.hashCode());
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EcRole other = (EcRole) obj;

        if (ecRoleNum == null) {
            if (other.ecRoleNum != null) return false;
        }
        else if (!ecRoleNum.equals(other.ecRoleNum)) return false;

        if (libelle == null) {
            if (other.libelle != null) return false;
        }
        else if (!libelle.equals(other.libelle)) return false;

        return true;
    }
}
