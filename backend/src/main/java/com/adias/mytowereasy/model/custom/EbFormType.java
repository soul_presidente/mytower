/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.custom;

import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;


@Table(name = "eb_form_type")
@Entity
public class EbFormType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebFormTypeNum;

    private String ebFormTypeName;
    /*
     * List des valeurs possible dans le cas des champs liste déroulante
     * par exemple pour le champ Flow : la liste va contenir les valeurs import
     * et export
     */
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private List<Object> listDropDownListValues;
    /*
     * va contenir les request mapping exepmle celui du country
     */
    private String dataLink;

    public EbFormType() {
    }

    public EbFormType(Integer xEbFormType, String xEbFormTypeName) {
        this.ebFormTypeName = xEbFormTypeName;
        this.ebFormTypeNum = xEbFormType;
    }

    public Integer getEbFormTypeNum() {
        return ebFormTypeNum;
    }

    public void setEbFormTypeNum(Integer ebFormTypeNum) {
        this.ebFormTypeNum = ebFormTypeNum;
    }

    public String getEbFormTypeName() {
        return ebFormTypeName;
    }

    public void setEbFormTypeName(String ebFormTypeName) {
        this.ebFormTypeName = ebFormTypeName;
    }

    public List<Object> getListDropDownListValues() {
        return listDropDownListValues;
    }

    public void setListDropDownListValues(List<Object> listDropDownListValues) {
        this.listDropDownListValues = listDropDownListValues;
    }

    public String getDataLink() {
        return dataLink;
    }

    public void setDataLink(String dataLink) {
        this.dataLink = dataLink;
    }
}
