package com.adias.mytowereasy.model;

import javax.persistence.*;

import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.dto.EbBoutonActionDTO;

import java.io.Serializable;


@Entity
@Table(name = "eb_bouton_action", schema = "work")
public class EbBoutonAction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer ebBoutonActionNum;

    private String type;
    private String module;
    private boolean active;
    private Long ordre;
    private String color;
    private Integer icon;
    private String libelle;

    @Column(columnDefinition = "TEXT")
    private String action;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user", insertable = true)
    private EbUser user;

    public EbBoutonAction() {
    }

    public EbBoutonAction(EbBoutonActionDTO ebBoutonActionDTO) {
        this.color = ebBoutonActionDTO.getColor();
        this.ebBoutonActionNum = ebBoutonActionDTO.getEbBoutonActionNum();
        this.icon = ebBoutonActionDTO.getIcon();
    }

    @QueryProjection
    public EbBoutonAction(
        Integer ebBoutonActionNum,
        String type,
        String module,
        boolean active,
        String libelle,
        Integer icon,
        Long ordre,
        String color,
        String action) {
        this.ebBoutonActionNum = ebBoutonActionNum;
        this.type = type;
        this.module = module;
        this.active = active;
        this.libelle = libelle;
        this.icon = icon;
        this.ordre = ordre;
        this.color = color;
        this.action = action;
    }

    public Integer getEbBoutonActionNum() {
        return ebBoutonActionNum;
    }

    public void setEbBoutonActionNum(Integer ebBoutonActionNum) {
        this.ebBoutonActionNum = ebBoutonActionNum;
    }

    public String getType() {
        return type;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getModule() {
        return module;
    }

    public boolean isActive() {
        return active;
    }

    public Long getOrdre() {
        return ordre;
    }

    public String getColor() {
        return color;
    }

    public Integer getIcon() {
        return icon;
    }

    public String getLibelle() {
        return libelle;
    }

    public EbUser getUser() {
        return user;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void setOrdre(Long ordre) {
        this.ordre = ordre;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setIcon(Integer icon) {
        this.icon = icon;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setUser(EbUser user) {
        this.user = user;
    }
}
