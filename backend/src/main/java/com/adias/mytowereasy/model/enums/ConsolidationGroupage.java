package com.adias.mytowereasy.model.enums;

public enum ConsolidationGroupage {
    NP("NP", "New proposition", 1),
    PRS("PRS", "Proposal ready for sending to the client for validation", 2),
    PPV("PPV", "Proposal pending validation", 3),
    PVC("PVC", "Proposal validated by the customer via EDI", 4),
    PCO("PCO", "Proposition confirmed by the operator", 5);

    private Integer code;
    private String libelle;
    private String codeAlpha;

    public String getCodeAlpha() {
        return codeAlpha;
    }

    public void setCodeAlpha(String CodeAlpha) {
        this.codeAlpha = CodeAlpha;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    // Constructeur
    private ConsolidationGroupage(String libelleJason, String libelle, Integer code) {
        this.codeAlpha = libelleJason;
        this.code = code;
        this.libelle = libelle;
    }

    public static Integer getCodeByCodeAlpha(String codeAlpha) {

        if (codeAlpha != null && !codeAlpha.isEmpty()) {
            Integer status = null;
            String value = codeAlpha.toLowerCase();

            for (ConsolidationGroupage item: ConsolidationGroupage.values()) {
                if (item.codeAlpha.toLowerCase().contains(value)) status = item.code;
            }

            return status;
        }

        return null;
    }

    public static Integer getCodeByLibelle(String libelle) {

        if (libelle != null && !libelle.isEmpty()) {
            Integer status = null;
            String value = libelle.toLowerCase();

            for (ConsolidationGroupage item: ConsolidationGroupage.values()) {
                if (item.libelle.toLowerCase().contains(value)) status = item.code;
            }

            return status;
        }

        return null;
    }

    public static String getByCode(Integer code) {

        if (code != null) {
            ConsolidationGroupage consolidationGroupage = null;

            for (ConsolidationGroupage item: ConsolidationGroupage.values()) {
                if (item.code.equals(code)) consolidationGroupage = item;
            }

            return consolidationGroupage != null ? consolidationGroupage.getLibelle() : "";
        }

        return null;
    }

    public static String getCodeAlphaByCode(Integer code) {

        if (code != null) {
            ConsolidationGroupage consolidationGroupage = null;

            for (ConsolidationGroupage item: ConsolidationGroupage.values()) {
                if (item.code.equals(code)) consolidationGroupage = item;
            }

            return consolidationGroupage != null ? consolidationGroupage.getCodeAlpha() : "";
        }

        return null;
    }
}
