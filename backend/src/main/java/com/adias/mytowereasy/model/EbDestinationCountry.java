/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicUpdate;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.querydsl.core.annotations.QueryProjection;


@Entity
@Table(name = "eb_cm_destination_country")
@DynamicUpdate(value = true)
@JsonIgnoreProperties({
    "hibernateLazyInitializer", "handler"
})
public class EbDestinationCountry implements Serializable {
    /** */
    private static final long serialVersionUID = -5686384398545391217L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebDestinationCountryNum;

    @Column(columnDefinition = "TEXT")
    private String generalComments;

    @Column(columnDefinition = "TEXT")
    private String processLeadTimes;

    @Column(columnDefinition = "TEXT")
    private String preferredPartners;

    @Column(columnDefinition = "TEXT")
    private String documentationRequired;

    @Column(insertable = true, updatable = true)
    @ColumnDefault("now()")
    private Date lastDateModified;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_user_modified", insertable = true)
    private EbUser lastUserModified;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_user", insertable = true, updatable = false)
    private EbUser createdBy;

    @Column(insertable = true, updatable = false)
    @ColumnDefault("now()")
    private Date dateAjout;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country", insertable = true, updatable = false)
    private EcCountry country;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "destinationCountry")
    private Set<EbCombinaison> combinaisons;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement", insertable = true, updatable = false)
    private EbEtablissement etablissement;

    private String idImportLine;

    @Transient
    @JsonIgnore
    private String createdByOrModifiedBy;

    @Transient
    @JsonIgnore
    private Date dateCreationOrModification;

    @Transient
    @JsonIgnore
    private String logoEtablissement;

    @QueryProjection
    public EbDestinationCountry(Integer ebDestinationCountryNum, EcCountry country) {
        this.ebDestinationCountryNum = ebDestinationCountryNum;
        this.country = country;
    }

    @QueryProjection
    public EbDestinationCountry(Integer ebDestinationCountryNum, EcCountry country, EbEtablissement etablissement) {
        this.ebDestinationCountryNum = ebDestinationCountryNum;
        this.country = country;
        this.etablissement = etablissement;
    }

    @QueryProjection
    public EbDestinationCountry(
        Integer ebDestinationCountryNum,
        String generalComments,
        String processLeadTimes,
        String preferredPartners,
        String documentationRequired,
        Date lastDateModified,
        String lastUserModifiedName,
        String createdByName,
        Date dateAjout,
        String countryLibelle) {
        this.ebDestinationCountryNum = ebDestinationCountryNum;
        this.generalComments = generalComments;
        this.processLeadTimes = processLeadTimes;
        this.preferredPartners = preferredPartners;
        this.documentationRequired = documentationRequired;
        this.lastDateModified = lastDateModified;
        this.dateAjout = dateAjout;

        if (lastUserModifiedName != null) {
            this.lastUserModified = new EbUser();
            this.lastUserModified.setNom(lastUserModifiedName);
        }

        if (createdByName != null) {
            this.createdBy = new EbUser();
            this.createdBy.setNom(createdByName);
        }

        if (countryLibelle != null) {
            this.country = new EcCountry();
            this.country.setLibelle(countryLibelle);
        }

    }

    public EbDestinationCountry() {
    }

    public Date getLastDateModified() {
        return lastDateModified;
    }

    public void setLastDateModified(Date lastDateModified) {
        this.lastDateModified = lastDateModified;
    }

    public EbUser getLastUserModified() {
        return lastUserModified;
    }

    public void setLastUserModified(EbUser lastUserModified) {
        this.lastUserModified = lastUserModified;
    }

    public EbUser getCreatedBy() {
        if (createdBy != null) return new EbUser(createdBy);
        else return createdBy;
    }

    public void setCreatedBy(EbUser createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getEbDestinationCountryNum() {
        return ebDestinationCountryNum;
    }

    public void setEbDestinationCountryNum(Integer ebDestinationCountryNum) {
        this.ebDestinationCountryNum = ebDestinationCountryNum;
    }

    public String getGeneralComments() {
        return generalComments;
    }

    public void setGeneralComments(String generalComments) {
        this.generalComments = generalComments;
    }

    public String getProcessLeadTimes() {
        return processLeadTimes;
    }

    public void setProcessLeadTimes(String processLeadTimes) {
        this.processLeadTimes = processLeadTimes;
    }

    public String getPreferredPartners() {
        return preferredPartners;
    }

    public void setPreferredPartners(String preferredPartners) {
        this.preferredPartners = preferredPartners;
    }

    public String getDocumentationRequired() {
        return documentationRequired;
    }

    public void setDocumentationRequired(String documentationRequired) {
        this.documentationRequired = documentationRequired;
    }

    public EbEtablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(EbEtablissement etablissement) {
        this.etablissement = etablissement;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public EcCountry getCountry() {
        return country;
    }

    public void setCountry(EcCountry country) {
        this.country = country;
    }

    public Set<EbCombinaison> getCombinaisons() {
        return combinaisons;
    }

    public void setCombinaisons(Set<EbCombinaison> combinaisons) {
        this.combinaisons = combinaisons;
    }

    public String getCreatedByOrModifiedBy() {
        if (this.lastUserModified != null) return this.lastUserModified.getNom();
        else return this.createdBy.getNom();
    }

    public Date getDateCreationOrModification() {
        if (this.getLastDateModified() != null) return this.getLastDateModified();
        else return this.dateAjout;
    }

    public String getLogoEtablissement() {
        return logoEtablissement;
    }

    public void setCreatedByOrModifiedBy(String createdByOrModifiedBy) {
        this.createdByOrModifiedBy = createdByOrModifiedBy;
    }

    public void setDateCreationOrModification(Date dateCreationOrModification) {
        this.dateCreationOrModification = dateCreationOrModification;
    }

    public void setLogoEtablissement(String logoEtablissement) {
        this.logoEtablissement = logoEtablissement;
    }

    public String getIdImportLine() {
        return idImportLine;
    }

    public void setIdImportLine(String idImportLine) {
        this.idImportLine = idImportLine;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((combinaisons == null) ? 0 : combinaisons.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        result = prime * result + ((createdBy == null) ? 0 : createdBy.hashCode());
        result = prime * result + ((createdByOrModifiedBy == null) ? 0 : createdByOrModifiedBy.hashCode());
        result = prime * result + ((dateAjout == null) ? 0 : dateAjout.hashCode());
        result = prime * result + ((dateCreationOrModification == null) ? 0 : dateCreationOrModification.hashCode());
        result = prime * result + ((documentationRequired == null) ? 0 : documentationRequired.hashCode());
        result = prime * result + ((ebDestinationCountryNum == null) ? 0 : ebDestinationCountryNum.hashCode());
        result = prime * result + ((etablissement == null) ? 0 : etablissement.hashCode());
        result = prime * result + ((generalComments == null) ? 0 : generalComments.hashCode());
        result = prime * result + ((lastDateModified == null) ? 0 : lastDateModified.hashCode());
        result = prime * result + ((lastUserModified == null) ? 0 : lastUserModified.hashCode());
        result = prime * result + ((logoEtablissement == null) ? 0 : logoEtablissement.hashCode());
        result = prime * result + ((preferredPartners == null) ? 0 : preferredPartners.hashCode());
        result = prime * result + ((processLeadTimes == null) ? 0 : processLeadTimes.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbDestinationCountry other = (EbDestinationCountry) obj;

        if (combinaisons == null) {
            if (other.combinaisons != null) return false;
        }
        else if (!combinaisons.equals(other.combinaisons)) return false;

        if (country == null) {
            if (other.country != null) return false;
        }
        else if (!country.equals(other.country)) return false;

        if (createdBy == null) {
            if (other.createdBy != null) return false;
        }
        else if (!createdBy.equals(other.createdBy)) return false;

        if (createdByOrModifiedBy == null) {
            if (other.createdByOrModifiedBy != null) return false;
        }
        else if (!createdByOrModifiedBy.equals(other.createdByOrModifiedBy)) return false;

        if (dateAjout == null) {
            if (other.dateAjout != null) return false;
        }
        else if (!dateAjout.equals(other.dateAjout)) return false;

        if (dateCreationOrModification == null) {
            if (other.dateCreationOrModification != null) return false;
        }
        else if (!dateCreationOrModification.equals(other.dateCreationOrModification)) return false;

        if (documentationRequired == null) {
            if (other.documentationRequired != null) return false;
        }
        else if (!documentationRequired.equals(other.documentationRequired)) return false;

        if (ebDestinationCountryNum == null) {
            if (other.ebDestinationCountryNum != null) return false;
        }
        else if (!ebDestinationCountryNum.equals(other.ebDestinationCountryNum)) return false;

        if (etablissement == null) {
            if (other.etablissement != null) return false;
        }
        else if (!etablissement.equals(other.etablissement)) return false;

        if (generalComments == null) {
            if (other.generalComments != null) return false;
        }
        else if (!generalComments.equals(other.generalComments)) return false;

        if (lastDateModified == null) {
            if (other.lastDateModified != null) return false;
        }
        else if (!lastDateModified.equals(other.lastDateModified)) return false;

        if (lastUserModified == null) {
            if (other.lastUserModified != null) return false;
        }
        else if (!lastUserModified.equals(other.lastUserModified)) return false;

        if (logoEtablissement == null) {
            if (other.logoEtablissement != null) return false;
        }
        else if (!logoEtablissement.equals(other.logoEtablissement)) return false;

        if (preferredPartners == null) {
            if (other.preferredPartners != null) return false;
        }
        else if (!preferredPartners.equals(other.preferredPartners)) return false;

        if (processLeadTimes == null) {
            if (other.processLeadTimes != null) return false;
        }
        else if (!processLeadTimes.equals(other.processLeadTimes)) return false;

        return true;
    }
}
