/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;


public class PricingBookingEnumeration {
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum TypeDemande {
        STANDARD(1, "Standard", 48 * 3600), URGENT(2, "Urgent", 24 * 3600);

        private Integer code;
        private String libelle;
        private Integer timeRemaining;

        private TypeDemande(Integer code, String libelle, Integer timeRemaining) {
            this.code = code;
            this.libelle = libelle;
            this.timeRemaining = timeRemaining;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public Integer getTimeRemaining() {
            return timeRemaining;
        }

        // récuppération de la liste format JSON
        public static String getListTypeDemande() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            JsonNode standard = mapper.readTree(mapper.writeValueAsString(TypeDemande.STANDARD));
            JsonNode urgent = mapper.readTree(mapper.writeValueAsString(TypeDemande.URGENT));

            arrayNode.add(standard);
            arrayNode.add(urgent);

            return arrayNode.toString();
        }

        @JsonCreator
        public static TypeDemande fromNode(JsonNode node) {
            if (!node.has("libelle")) return null;

            String name = node.get("libelle").asText().toUpperCase();

            return TypeDemande.valueOf(name);
        }
    }

    public enum Incoterms {
        DDP(1, "DDP"),
        DAP(2, "DAP"),
        DAT(3, "DAT"),
        CIP(4, "CIP"),
        CPT(5, "CPT"),
        CIF(6, "CIF"),
        CFR(7, "CFR"),
        FOB(8, "FOB"),
        FAS(9, "FAS"),
        FCA(10, "FCA"),
        EXW(11, "EXW");

        private Integer code;
        private String libelle;

        private Incoterms(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum DemandeStatus {
        WAITING_FOR_QUOTE(1, "In process, waiting for quote"),
        WAITING_FOR_RECOMENDATION(2, "In process, waiting for recomendation"),
        WAITING_FOR_CONFIRMATION(3, "Waiting for confirmation"),
        CONFIRMED(4, "Confirmed");

        private Integer code;
        private String libelle;

        private DemandeStatus(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getLibelle(int code) {
            String libelle = null;

            for (DemandeStatus st: DemandeStatus.values()) {

                if (st.getCode().equals(code)) {
                    libelle = st.getLibelle();
                    break;
                }

            }

            return libelle;
        }
    }

    public enum MarchandiseTypeOfUnit {
        CARDBOARD_BOX(1, "Cardboard box", 3),
        PALLET(2, "Pallet", 2),
        CRATE(3, "Crate", 3),
        CASE(4, "Case", 3),
        BUNDLE(5, "Bundle", 3),
        BARREL(6, "Barrel", 3),
        CONTAINER_20_X_8_6_DRY(7, "Container 20' x 8'6' Dry", 1),
        CONTAINER_20_X_9_6_DRY_HIGH_CUBE(8, "Container 20' x 9'6' Dry High Cube", 1),
        CONTAINER_20_X_8_6_REEFER(9, "Container 20' x 8'6' Reefer", 1),
        CONTAINER_20_X_9_6_REEFER_HIGH_CUBE(10, "Container 20' x 9'6' Reefer High Cube", 1),
        CONTAINER_20_OPEN_TOP(11, "Container 20' Open Top", 1),
        CONTAINER_20_HARD_TOP(12, "Container 20' Hard Top", 1),
        CONTAINER_20_FLAT_RACK(13, "Container 20' Flat Rack", 1),
        CONTAINER_40_X8_6_DRY(14, "Container 40'x8'6' Dry", 1),
        CONTAINER_40_X9_6_DRY_HIGH_CUBE(15, "Container 40'x9'6' Dry High Cube", 1),
        CONTAINER_40_X8_6_REEFER(16, "Container 40'x8'6' Reefer", 1),
        CONTAINER_40_X9_6_REEFER_HIGH_CUBE(17, "Container 40'x9'6' Reefer High Cube", 1),
        CONTAINER_40_OPEN_TOP(18, "Container 40' Open Top", 1),
        CONTAINER_40_HARD_TOP(19, "Container 40' Hard Top", 1),
        CONTAINER_40_FLAT_RACK(20, "Container 40' Flat Rack", 1),
        CONTAINER_45_X9_6_DRY_HIGH_CUBE(21, "Container 45'x9'6' Dry High Cube", 1),
        CONTAINER_45_X9_6_REEFER_HIGH_CUBE(22, "Container 45'x9'6' Reefer High Cube", 1),
        SUSPENDU(23, "Suspendu", 3),
        COLIS(24, "Colis", 3),
        COMPLETE_TRUCK(25, "Complete truck", 1),
        CAMION_COMPLET(26, "Camion complet", 1);

        private Integer code;
        private String libelle;
        private Integer typeConteneur;

        private MarchandiseTypeOfUnit(Integer code, String libelle, Integer typeConteneur) {
            this.code = code;
            this.libelle = libelle;
            this.typeConteneur = typeConteneur;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public Integer getTypeConteneur() {
            return typeConteneur;
        }

        public static Integer getCodeByLibelle(String libelle) {

            if (libelle != null && !libelle.isEmpty()) {
                Integer status = null;
                String value = libelle.toLowerCase();

                for (MarchandiseTypeOfUnit item: MarchandiseTypeOfUnit.values()) {
                    if (item.libelle.toLowerCase().contains(value)) status = item.code;
                }

                return status;
            }

            return null;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                String status = null;

                for (MarchandiseTypeOfUnit item: MarchandiseTypeOfUnit.values()) {
                    if (item.code.equals(code)) status = item.libelle;
                }

                return status;
            }

            return null;
        }

        public static List<MarchandiseTypeOfUnit> getListMarchandiseTypeOfUnit() {
            List<MarchandiseTypeOfUnit> result = new ArrayList<MarchandiseTypeOfUnit>();
            MarchandiseTypeOfUnit[] types = MarchandiseTypeOfUnit.values();

            for (MarchandiseTypeOfUnit t: types) {
                result.add(t);
            }

            return result;
        }
    }

    public enum MarchandiseDangerousGood {
        NO(1, "NO"), OK_PAX(2, "OK PAX"), CAO(3, "CAO"), OK(4, "OK");

        private Integer code;
        private String libelle;

        private MarchandiseDangerousGood(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static Integer getCodeByLibelle(String libelle) {

            if (libelle != null && !libelle.isEmpty()) {
                Integer status = null;
                String value = libelle.toLowerCase();

                for (MarchandiseDangerousGood item: MarchandiseDangerousGood.values()) {
                    if (item.libelle.toLowerCase().contains(value)) status = item.code;
                }

                return status;
            }

            return null;
        }
    }

    public enum CustomsControlStatus {
        NONE(1, "None"), DOCUMENTARY(2, "Documentary"), PHYSICAL(3, "Physical");

        private Integer code;
        private String libelle;

        CustomsControlStatus(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static List<Map<String, Object>> getValues() {
            List<Map<String, Object>> arr = new ArrayList<Map<String, Object>>();

            for (CustomsControlStatus item: CustomsControlStatus.values()) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("code", item.getCode());
                map.put("libelle", item.getLibelle());
                arr.add(map);
            }

            return arr;
        }
    }

    public enum CustomsDuty {
        PAID_BY_THE_BROKER(1, "Paid by the broker"), DEFERMENT_ACCOUNT(2, "Deferment Account");

        private Integer code;
        private String libelle;

        CustomsDuty(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static List<Map<String, Object>> getValues() {
            List<Map<String, Object>> arr = new ArrayList<Map<String, Object>>();

            for (CustomsDuty item: CustomsDuty.values()) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("code", item.getCode());
                map.put("libelle", item.getLibelle());
                arr.add(map);
            }

            return arr;
        }
    }

    public enum CustomsVat {
        PAID_BY_THE_BROKER(1, "Paid by the broker"),
        DEFERMENT_ACCOUNT(2, "Deferment Account"),
        REVERSED_CHARGE(3, "Reversed Charge");

        private Integer code;
        private String libelle;

        CustomsVat(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static List<Map<String, Object>> getValues() {
            List<Map<String, Object>> arr = new ArrayList<Map<String, Object>>();

            for (CustomsVat item: CustomsVat.values()) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("code", item.getCode());
                map.put("libelle", item.getLibelle());
                arr.add(map);
            }

            return arr;
        }
    }

    public enum CustomsOtherTaxes {
        PAID_BY_THE_BROKER(1, "Paid by the broker"), DEFERMENT_ACCOUNT(2, "Deferment Account");

        private Integer code;
        private String libelle;

        CustomsOtherTaxes(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static List<Map<String, Object>> getValues() {
            List<Map<String, Object>> arr = new ArrayList<Map<String, Object>>();

            for (CustomsOtherTaxes item: CustomsOtherTaxes.values()) {
                Map<String, Object> map = new HashMap<String, Object>();
                map.put("code", item.getCode());
                map.put("libelle", item.getLibelle());
                arr.add(map);
            }

            return arr;
        }
    }

    // public enum TypeCustomBroker {
    //
    // M,y(1, "In process, waiting for quote"), WAITING_FOR_RECOMENDATION(2, "In
    // process, waiting for recomendation")
    // , WAITING_FOR_CONFIRMATION(3, "Waiting for confirmation"), CONFIRMED(4,
    // "Confirmed");
    // //other n'existe plus
    //
    // private Integer code;
    // private String libelle;
    //
    // private TypeCustomBroker(Integer code, String libelle) {
    // this.code = code;
    // this.libelle = libelle;
    // }
    //
    // public Integer getCode() {
    // return code;
    // }
    //
    // public String getLibelle() {
    // return libelle;
    // }
    //
    // }
}
