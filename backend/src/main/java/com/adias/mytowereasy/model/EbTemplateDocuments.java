package com.adias.mytowereasy.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.dto.EbTemplateDocumentsDTO;


@Entity
@Table(name = "eb_template_documents")
public class EbTemplateDocuments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebTemplateDocumentsNum;

    private String libelle;

    private String reference;

    @Column(insertable = false, updatable = false)
    @ColumnDefault("now()")
    private Date dateAjout;

    private Date dateMaj;

    @ManyToMany
    @JoinTable(
        name = "ex_documents_template_type",
        joinColumns = @JoinColumn(name = "eb_template_documents_num"),
        inverseJoinColumns = @JoinColumn(name = "eb_type_documents_num"))
    private List<EbTypeDocuments> typesDocument;

    private String cheminDocument;

    private String originFileName;

    @ManyToOne
    @JoinColumn(name = "x_eb_user")
    private EbUser xEbUser;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_company", insertable = true)
    private EbCompagnie xEbCompagnie;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<Integer> module;

    @ColumnDefault("false")
    private Boolean isGlobal;

    // Projection details Templates Document.
    @QueryProjection
    public EbTemplateDocuments(
        Integer ebTemplateDocumentsNum,
        String libelle,
        String reference,
        Date dateAjout,
        Date dateMaj,
        List<EbTypeDocuments> typesDocument,
        String originFileName,
        String cheminDocument,
        EbUser xEbUser,
        EbCompagnie xEbCompagnie,
        List<Integer> module,
        boolean isGlobal) {
        super();
        this.ebTemplateDocumentsNum = ebTemplateDocumentsNum;
        this.libelle = libelle;
        this.reference = reference;
        this.dateAjout = dateAjout;
        this.dateMaj = dateMaj;
        this.typesDocument = typesDocument;
        this.originFileName = originFileName;
        this.cheminDocument = cheminDocument;
        this.xEbUser = xEbUser;
        this.xEbCompagnie = xEbCompagnie;
        this.module = module;
        this.isGlobal = isGlobal;
    }

    public EbTemplateDocuments(EbTemplateDocumentsDTO dto, EbUser userConnected) {
        super();
        this.ebTemplateDocumentsNum = dto.getEbTemplateDocumentsNum();
        this.dateMaj = new Date();
        this.libelle = dto.getLibelle();
        this.reference = dto.getReference();

        if (dto.getListEbTypeDocumentsNum() != null) {
            this.typesDocument = new ArrayList<>();
            dto.getListEbTypeDocumentsNum().forEach(tdn -> {
                EbTypeDocuments tDoc = new EbTypeDocuments();
                tDoc.setEbTypeDocumentsNum(tdn);
                this.typesDocument.add(tDoc);
            });
        }

        this.originFileName = dto.getOriginFileName();
        this.cheminDocument = dto.getCheminDocument();
        this.xEbUser = userConnected;
        this.xEbCompagnie = userConnected != null ? userConnected.getEbCompagnie() : null;
        this.module = dto.getModule();
        this.isGlobal = dto.isGlobal();
    }

    public EbTemplateDocuments() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public String getOriginFileName() {
        return originFileName;
    }

    public void setOriginFileName(String originFileName) {
        this.originFileName = originFileName;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getEbTemplateDocumentsNum() {
        return ebTemplateDocumentsNum;
    }

    public void setEbTemplateDocumentsNum(Integer ebTemplateDocumentsNum) {
        this.ebTemplateDocumentsNum = ebTemplateDocumentsNum;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public List<EbTypeDocuments> getTypesDocument() {
        return typesDocument;
    }

    public void setTypesDocument(List<EbTypeDocuments> typesDocument) {
        this.typesDocument = typesDocument;
    }

    public String getCheminDocument() {
        return cheminDocument;
    }

    public void setCheminDocument(String cheminDocument) {
        this.cheminDocument = cheminDocument;
    }

    public EbUser getxEbUser() {
        return xEbUser;
    }

    public void setxEbUser(EbUser xEbUser) {
        this.xEbUser = xEbUser;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public List<Integer> getModule() {
        return module;
    }

    public void setModule(List<Integer> module) {
        this.module = module;
    }

    public Boolean getIsGlobal() {
        return isGlobal;
    }

    public void setIsGlobal(Boolean isGlobal) {
        this.isGlobal = isGlobal;
    }
}
