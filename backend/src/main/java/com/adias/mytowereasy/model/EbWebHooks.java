package com.adias.mytowereasy.model;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.adias.mytowereasy.types.JsonBinaryType;


@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class EbWebHooks {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebWebHooksNum;

    @Column(insertable = false, updatable = false)
    @ColumnDefault("now()")
    private Date dateAjout;

    // private Date dateMaj;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private String messageBody;

    @ManyToOne
    @JoinColumn(name = "x_eb_user")
    private EbUser xEbUser;

    public EbWebHooks() {
        super();
    }

    public EbWebHooks(String messageBody, EbUser xEbUser) {
        super();
        this.messageBody = messageBody;
        this.xEbUser = xEbUser;
    }

    public EbUser getxEbUser() {
        return xEbUser;
    }

    public void setxEbUser(EbUser xEbUser) {
        this.xEbUser = xEbUser;
    }

    public Integer getEbWebHooksNum() {
        return ebWebHooksNum;
    }

    public void setEbWebHooksNum(Integer ebWebHooksNum) {
        this.ebWebHooksNum = ebWebHooksNum;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }
}
