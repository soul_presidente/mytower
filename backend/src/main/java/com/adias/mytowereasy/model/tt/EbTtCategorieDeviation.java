/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.tt;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.adias.mytowereasy.model.TtEnumeration;


@Entity
public class EbTtCategorieDeviation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer ebTtCategorieDeviationNum;

    String libelle;
    String code;

    Integer typeEvent; // contient la valeur 1 pour les incidents et la valeur 3
                       // pour les unconfomitys

    public EbTtCategorieDeviation() {
        super();
    }

    public EbTtCategorieDeviation(TtEnumeration.TtDeviationCategorie devCategory) {
        super();
        this.libelle = devCategory.getLibelle();
        this.code = devCategory.getCode() + "";
    }

    public EbTtCategorieDeviation(Integer ebTtCategorieDeviationNum, String libelle) {
        super();
        this.ebTtCategorieDeviationNum = ebTtCategorieDeviationNum;
        this.libelle = libelle;
    }

    public Integer getEbTtCategorieDeviationNum() {
        return ebTtCategorieDeviationNum;
    }

    public void setEbTtCategorieDeviationNum(Integer ebTtCategorieDeviationNum) {
        this.ebTtCategorieDeviationNum = ebTtCategorieDeviationNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(Integer typeEvent) {
        this.typeEvent = typeEvent;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ebTtCategorieDeviationNum == null) ? 0 : ebTtCategorieDeviationNum.hashCode());
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbTtCategorieDeviation other = (EbTtCategorieDeviation) obj;

        if (ebTtCategorieDeviationNum == null) {
            if (other.ebTtCategorieDeviationNum != null) return false;
        }
        else if (!ebTtCategorieDeviationNum.equals(other.ebTtCategorieDeviationNum)) return false;

        if (code == null) {
            if (other.code != null) return false;
        }
        else if (!code.equals(other.code)) return false;

        if (libelle == null) {
            if (other.libelle != null) return false;
        }
        else if (!libelle.equals(other.libelle)) return false;

        return true;
    }
}
