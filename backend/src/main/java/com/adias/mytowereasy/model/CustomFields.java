/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

public class CustomFields implements java.io.Serializable {
    private static final long serialVersionUID = 3752989330099769808L;

    private String name;
    private String value;
    private String label;
    private Integer num;
    private Boolean isdefault;
    private Boolean isNew;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Boolean getIsdefault() {
        return isdefault;
    }

    public void setIsdefault(Boolean isdefault) {
        this.isdefault = isdefault;
    }

    public Boolean getIsNew() {
        return isNew;
    }

    public void setIsNew(Boolean isNew) {
        this.isNew = isNew;
    }

		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + ((isNew == null) ? 0 : isNew.hashCode());
			result = prime * result + ((isdefault == null) ? 0 : isdefault.hashCode());
			result = prime * result + ((label == null) ? 0 : label.hashCode());
			result = prime * result + ((name == null) ? 0 : name.hashCode());
			result = prime * result + ((num == null) ? 0 : num.hashCode());
			result = prime * result + ((value == null) ? 0 : value.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			CustomFields other = (CustomFields) obj;
			if (isNew == null)
			{
				if (other.isNew != null)
					return false;
			}
			else if (!isNew.equals(other.isNew))
				return false;
			if (isdefault == null)
			{
				if (other.isdefault != null)
					return false;
			}
			else if (!isdefault.equals(other.isdefault))
				return false;
			if (label == null)
			{
				if (other.label != null)
					return false;
			}
			else if (!label.equals(other.label))
				return false;
			if (name == null)
			{
				if (other.name != null)
					return false;
			}
			else if (!name.equals(other.name))
				return false;
			if (num == null)
			{
				if (other.num != null)
					return false;
			}
			else if (!num.equals(other.num))
				return false;
			if (value == null)
			{
				if (other.value != null)
					return false;
			}
			else if (!value.equals(other.value))
				return false;
			return true;
		}
}
