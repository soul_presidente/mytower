/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import com.adias.mytowereasy.model.Enumeration.JourEnum;


public class Jour {
    JourEnum jourEnum;
    String heure1;
    String heure2;
    String heure3;
    String heure4;
    Boolean ish3h4actif;
    Boolean ouvert;

    public JourEnum getJourEnum() {
        return jourEnum;
    }

    public void setJourEnum(JourEnum jourEnum) {
        this.jourEnum = jourEnum;
    }

    public String getHeure1() {
        return heure1;
    }

    public void setHeure1(String heure1) {
        this.heure1 = heure1;
    }

    public String getHeure2() {
        return heure2;
    }

    public void setHeure2(String heure2) {
        this.heure2 = heure2;
    }

    public String getHeure3() {
        return heure3;
    }

    public void setHeure3(String heure3) {
        this.heure3 = heure3;
    }

    public String getHeure4() {
        return heure4;
    }

    public void setHeure4(String heure4) {
        this.heure4 = heure4;
    }

    public Boolean getOuvert() {
        return ouvert;
    }

    public void setOuvert(Boolean ouvert) {
        this.ouvert = ouvert;
    }

    public Boolean getIsh3h4actif() {
        return ish3h4actif;
    }

    public void setIsh3h4actif(Boolean ish3h4actif) {
        this.ish3h4actif = ish3h4actif;
    }
}
