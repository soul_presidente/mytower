/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.tt;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCountry;


@JsonIgnoreProperties({
    "nbDoc"
})
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "ebTtPslAppNum",
    scope = EbTTPslApp.class)
@Entity
@Table(name = "eb_tt_psl_app")
public class EbTTPslApp {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebTtPslAppNum;

    @Column(insertable = true, updatable = false)
    @ColumnDefault("now()")
    private Date dateCreation;

    private Date dateActuelle;
    private Date dateNegotiation;
    private Date dateEstimee;
    private Date dateChamps;

    private Integer Leadtime;

    @Column
    private Boolean validateByChamp;

    @OneToOne
    @JoinColumn(name = "x_ec_country")
    private EcCountry xEcCountry;

    private String city;

    private String commentaire;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user")
    private EbUser xEbUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(insertable = true, updatable = false, name = "x_eb_tt_tracing")
    private EbTtTracing xEbTrackTrace;

    private Integer codePslCode;

    private Boolean validated;

    // créer le champ x_eb_tt_psl_app sur l'entité eb_tt_event
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "xEbTtPslApp")
    @OrderBy(value = "dateCreation DESC NULLS LAST")
    private List<EbTtEvent> listEvent;

    private Boolean treatForDeviation;

    @Column
    private Integer fuseauHoraire;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private EbTtCompanyPsl companyPsl;

    private Integer orderpsl;

    private String codeAlpha;

    private String libelle;

    private Integer quantity;

    public EbTTPslApp() {
    }

    public EbTTPslApp(String codeAlpha, String libelle) {
        this.codeAlpha = codeAlpha;
        this.libelle = libelle;
    }

    @QueryProjection
    public EbTTPslApp(
        Integer ebTtPslAppNum,
        EbTtCompanyPsl companyPsl,
        Integer tracingNum,
        String codeAlpha,
        String codeAlphaPslCourant,
        Integer demandeNum,
        String configPsl,
        String unitRef,
        String transportRef,
        String customerRef

    ) {
        this.ebTtPslAppNum = ebTtPslAppNum;
        this.companyPsl = companyPsl;
        this.codeAlpha = codeAlpha;
        this.xEbTrackTrace = new EbTtTracing(
            tracingNum,
            codeAlphaPslCourant,
            demandeNum,
            configPsl,
            unitRef,
            transportRef,
            customerRef);
    }

    @QueryProjection
    public EbTTPslApp(
        Integer ebTtPslAppNum,

        String codeAlpha,
        String libelle,
        Date dateActuelle,
        Date dateEstimee,
        Date dateNegotiation,
        Date dateChamps,
        Boolean validated,

        Integer ebDemandeNum,
        Integer xEcStatut,
        Integer ebTtTracingNum,
        String refTransport,
        String listControlRule,
        String codeAlphaPslCourant,
        String codeAlphaLastPsl,
        String nomEtablissementChargeur,
        String nomEtablissementTransporteur) {
        this.ebTtPslAppNum = ebTtPslAppNum;

        this.codeAlpha = codeAlpha;
        this.dateActuelle = dateActuelle;
        this.dateEstimee = dateEstimee;
        this.dateNegotiation = dateNegotiation;
        this.dateChamps = dateChamps;
        this.validated = validated;

        this.xEbTrackTrace = new EbTtTracing();
        this.xEbTrackTrace.setEbTtTracingNum(ebTtTracingNum);
        this.xEbTrackTrace.setListControlRule(listControlRule);
        this.xEbTrackTrace.setCodeAlphaLastPsl(codeAlphaLastPsl);
        this.xEbTrackTrace.setCodeAlphaPslCourant(codeAlphaPslCourant);
        this.xEbTrackTrace.setNomEtablissementChargeur(nomEtablissementChargeur);
        this.xEbTrackTrace.setNomEtablissementTransporteur(nomEtablissementTransporteur);
        this.xEbTrackTrace.setRefTransport(refTransport);
        this.xEbTrackTrace.setxEbDemande(new EbDemande(ebDemandeNum));
        this.xEbTrackTrace.getxEbDemande().setxEcStatut(xEcStatut);
    }

    public Integer getOrderpsl() {
        return orderpsl;
    }

    public void setOrderpsl(Integer orderpsl) {
        this.orderpsl = orderpsl;
    }

    public EbTTPslApp(Integer ebPslAppNum) {
        this.ebTtPslAppNum = ebPslAppNum;
    }

    public Integer getCodePslCode() {
        return codePslCode;
    }

    public void setCodePslCode(Integer codePslCode) {
        this.codePslCode = codePslCode;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateActuelle() {
        return dateActuelle;
    }

    public void setDateActuelle(Date dateActuelle) {
        this.dateActuelle = dateActuelle;
    }

    public Date getDateNegotiation() {
        return dateNegotiation;
    }

    public void setDateNegotiation(Date dateNegotiation) {
        this.dateNegotiation = dateNegotiation;
    }

    public Date getDateEstimee() {
        return dateEstimee;
    }

    public void setDateEstimee(Date dateEstimee) {
        this.dateEstimee = dateEstimee;
    }

    public EcCountry getxEcCountry() {
        return xEcCountry;
    }

    public void setxEcCountry(EcCountry xEcCountry) {
        this.xEcCountry = xEcCountry;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public EbUser getxEbUser() {
        return xEbUser;
    }

    public void setxEbUser(EbUser xEbUser) {
        this.xEbUser = xEbUser;
    }

    public EbTtTracing getxEbTrackTrace() {
        return xEbTrackTrace;
    }

    public void setxEbTrackTrace(EbTtTracing xEbTrackTrace) {
        this.xEbTrackTrace = xEbTrackTrace;
    }

    public Boolean getValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public Integer getEbTtPslAppNum() {
        return ebTtPslAppNum;
    }

    public void setEbTtPslAppNum(Integer ebTtPslAppNum) {
        this.ebTtPslAppNum = ebTtPslAppNum;
    }

    public List<EbTtEvent> getListEvent() {
        return listEvent;
    }

    public void setListEvent(List<EbTtEvent> listEvent) {
        this.listEvent = listEvent;
    }

    public Integer getLeadtime() {
        return Leadtime;
    }

    public void setLeadtime(Integer leadtime) {
        Leadtime = leadtime;
    }

    public Boolean isTreatForDeviation() {
        return treatForDeviation;
    }

    public void setTreatForDeviation(Boolean treatForDeviation) {
        this.treatForDeviation = treatForDeviation;
    }

    public Integer getFuseauHoraire() {
        return fuseauHoraire;
    }

    public void setFuseauHoraire(Integer fuseauHoraire) {
        this.fuseauHoraire = fuseauHoraire;
    }

    public EbTtCompanyPsl getCompanyPsl() {
        return companyPsl;
    }

    public void setCompanyPsl(EbTtCompanyPsl companyPsl) {
        this.companyPsl = companyPsl;
    }

    public Boolean getValidateByChamp() {
        return validateByChamp;
    }

    public void setValidateByChamp(Boolean validateByChamp) {
        this.validateByChamp = validateByChamp;
    }

    public Boolean getTreatForDeviation() {
        return treatForDeviation;
    }

    public String getCodeAlpha() {
        return codeAlpha;
    }

    public void setCodeAlpha(String codeAlpha) {
        this.codeAlpha = codeAlpha;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Date getDateChamps() {
        return dateChamps;
    }

    public void setDateChamps(Date dateChamps) {
        this.dateChamps = dateChamps;
    }
}
