/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.qm;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import com.adias.mytowereasy.model.EbEtablissement;


/** EcQmQualification entity. @author Mohamed */
@Entity
@JsonIgnoreProperties({
    "hibernateLazyInitializer", "handler"
})
// @Table(indexes = { @Index(name = "ecQmQualificationNumIdx", columnList =
// "qmQualificationNum"),
// @Index(name = "ecQmQualificationEtablissementIdx", columnList = "x_eb_etab")
// })
@Table(name = "eb_qm_qualification", schema = "work")
public class EbQmQualification implements java.io.Serializable {
    private static final long serialVersionUID = 7748905743817618608L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer qmQualificationNum;

    @Column
    private String label;

    @Column
    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab")
    private EbEtablissement etablissement;

    public Integer getQmQualificationNum() {
        return qmQualificationNum;
    }

    public void setQmQualificationNum(Integer qmQualificationNum) {
        this.qmQualificationNum = qmQualificationNum;
    }

    public EbQmQualification(Integer qmQualificationNum) {
        super();
        this.qmQualificationNum = qmQualificationNum;
    }

    public EbQmQualification() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public EbEtablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(EbEtablissement etablissement) {
        this.etablissement = etablissement;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((qmQualificationNum == null) ? 0 : qmQualificationNum.hashCode());
        result = prime * result + ((etablissement == null) ? 0 : etablissement.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbQmQualification other = (EbQmQualification) obj;

        if (code == null) {
            if (other.code != null) return false;
        }
        else if (!code.equals(other.code)) return false;

        if (qmQualificationNum == null) {
            if (other.qmQualificationNum != null) return false;
        }
        else if (!qmQualificationNum.equals(other.qmQualificationNum)) return false;

        if (etablissement == null) {
            if (other.etablissement != null) return false;
        }
        else if (!etablissement.equals(other.etablissement)) return false;

        if (label == null) {
            if (other.label != null) return false;
        }
        else if (!label.equals(other.label)) return false;

        return true;
    }
}
