/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.persistence.*;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;
import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.dock.model.EbEntrepot;
import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.dto.ProposionRdvDto;
import com.adias.mytowereasy.dto.TransportReferenceInfosDTO;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.Enumeration.TypeDemande;
import com.adias.mytowereasy.model.enums.Modules;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.types.JsonBinaryType;
import com.adias.mytowereasy.util.DemandeStatusUtils;
import com.adias.mytowereasy.util.ObjectDeserializer;


@NamedEntityGraph(name = "EbDemande.detail", attributeNodes = @NamedAttributeNode("xEbCompagnie"))
@Table(name = "eb_demande", indexes = {
    @Index(name = "idx_eb_demande_eb_party_origin", columnList = "eb_party_origin"),
    @Index(name = "idx_eb_demande_eb_party_notif", columnList = "eb_party_notif"),
    @Index(name = "idx_eb_demande_eb_party_dest", columnList = "eb_party_dest"),
    @Index(name = "idx_eb_demande_x_eb_demande_initial", columnList = "x_eb_demande_initial"),
    @Index(name = "idx_eb_demande_x_eb_demande_master_object", columnList = "x_eb_demande_master_object"),
    @Index(name = "idx_eb_demande_x_eb_entrepot_unloading", columnList = "x_eb_entrepot_unloading"),
    @Index(name = "idx_eb_demande_x_eb_etablissement_transporteur", columnList = "x_eb_etablissement_transporteur"),
    @Index(name = "idx_eb_demande_x_eb_user", columnList = "x_eb_user"),
    @Index(name = "idx_eb_demande_x_ec_broker", columnList = "x_ec_broker"),
    @Index(name = "idx_eb_demande_x_eb_etablissement_broker", columnList = "x_eb_etablissement_broker"),
    @Index(name = "idx_eb_demande_x_eb_compagnie_broker", columnList = "x_eb_compagnie_broker"),
    @Index(name = "idx_eb_demande_x_eb_costCenter", columnList = "x_eb_costCenter"),
    @Index(name = "idx_eb_demande_x_eb_user_transport_info_maj", columnList = "x_eb_user_transport_info_maj"),
    @Index(name = "idx_eb_demande_x_eb_user_ct", columnList = "x_eb_user_ct"),
    @Index(name = "idx_eb_demande_x_eb_compagnie_ct", columnList = "x_eb_compagnie_ct"),
    @Index(name = "idx_eb_demande_x_eb_etablissement_ct", columnList = "x_eb_etablissement_ct"),
    @Index(name = "idx_eb_demande_x_eb_etablissement", columnList = "x_eb_etablissement"),
    @Index(name = "idx_eb_demande_x_eb_compagnie", columnList = "x_eb_compagnie"),
    @Index(name = "idx_eb_demande_x_eb_user_ct_required", columnList = "x_eb_user_ct_required"),
    @Index(name = "idx_eb_demande_x_ec_currency_invoice", columnList = "x_ec_currency_invoice"),
    @Index(name = "idx_eb_demande_x_ec_currency_insurance", columnList = "x_ec_currency_insurance"),
    @Index(name = "idx_eb_demande_point_intermediate", columnList = "point_intermediate"),
    @Index(name = "idx_eb_demande_x_eb_user_origin", columnList = "x_eb_user_origin"),
    @Index(name = "idx_eb_demande_x_eb_user_dest", columnList = "x_eb_user_dest"),
    @Index(name = "idx_eb_demande_x_eb_user_origin_customs_broker", columnList = "x_eb_user_origin_customs_broker"),
    @Index(name = "idx_eb_demande_x_eb_user_dest_customs_broker", columnList = "x_eb_user_dest_customs_broker"),
    @Index(name = "idx_eb_demande_x_eb_etab_origin", columnList = "x_eb_etab_origin"),
    @Index(name = "idx_eb_demande_x_eb_etab_dest", columnList = "x_eb_etab_dest"),
    @Index(name = "idx_eb_demande_x_eb_etab_origin_customs_broker", columnList = "x_eb_etab_origin_customs_broker"),
    @Index(name = "idx_eb_demande_x_eb_etab_dest_customs_broker", columnList = "x_eb_etab_dest_customs_broker")
})
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id", scope = EbDemande.class)
public class EbDemande implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(nullable = false, unique = true)
    private Integer ebDemandeNum;

    private BigDecimal saving;

    private BigDecimal convertedSaving; // The converted saving of the "initial
                                        // TR" converted to the "TR
                                        // résultante"'s currency

    private BigDecimal convertedPrice; // The converted price of the "initial
                                       // TR" converted to the "TR résultante"'s
                                       // currency

    @ManyToOne
    @JoinColumn(name = "x_ec_currency_tr_resultante")
    private EcCurrency xecCurrencyTrResultante; // The currency of the "TR
                                                // resultante" which we have
                                                // used to convert the converted
                                                // saving and price

    private Integer nbReminderSent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eb_party_origin")
    // @JsonManagedReference
    private EbParty ebPartyOrigin;

    // @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eb_party_notif")
    private EbParty ebPartyNotif;

    // @JsonManagedReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "eb_party_dest")
    private EbParty ebPartyDest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_demande_initial")
    private EbDemande ebDemandeInitial;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_demande_master_object")
    private EbDemande ebDemandeMasterObject;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_entrepot_unloading")
    private EbEntrepot xEbEntrepotUnloading;

    @Column(insertable = false, updatable = false)
    @ColumnDefault("now()")
    private Date dateCreation;

    @ColumnDefault("now()")
    private Date updateDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReadyForPickup; // TODO renamne this field

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    @Temporal(TemporalType.TIMESTAMP)
    private Date datePickup;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEnvoiQuotation; // TODO a supprimer potentielement

    @Temporal(TemporalType.TIMESTAMP)
    private Date customerCreationDate;

    private Boolean insurance;

    private Double insuranceValue;

    private String city; // incoterm city

    @Column(columnDefinition = "TEXT")
    private String commentMytower;

    @Column(columnDefinition = "TEXT")
    private String commentChargeur;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfGoodsAvailability;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfArrival;

    @OneToMany(mappedBy = "xEbDemande")
    private List<ExEbDemandeTransporteur> exEbDemandeTransporteurs;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private EbUser exEbDemandeTransporteurFinal;

    @Transient
    private ExEbDemandeTransporteur _exEbDemandeTransporteurFinal;

    @Transient
    @JsonDeserialize
    private List<EbUser> listTransporteurs;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<ExEbDemandeTransporteur> listDemandeQuote;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    String statsMarchandises;

    Integer valuationType;

    @Temporal(TemporalType.TIMESTAMP)
    private Date waitingForQuote;

    @Temporal(TemporalType.TIMESTAMP)
    private Date waitingForConf;

    @Temporal(TemporalType.TIMESTAMP)
    private Date confirmed;

    private Integer xEbTypeTransport;

    private BigDecimal prixTransporteurFinal;

    private Integer xEcProductFamily;

    @Column(columnDefinition = "TEXT")
    private String listCategoriesStr;

    private String listLabels;

    private Integer nbrDoc;

    private String nomTransporteur;

    @Column(name = "x_eb_transporteur")
    private Integer xEbTransporteur;

    @Column(name = "x_eb_etablissement_transporteur")
    private Integer xEbTransporteurEtablissementNum;

		@Column(columnDefinition = "TEXT")
    private String unitsReference;
    private String unitsPackingList;

    private Boolean confirmedFromTransptPlan;

    /*
     * C'est le temps restant avant l'ecoulement definitive du compte Ã  rebours
     * (il
     * est en milliseconde) Exemple si ce champ contient 100 cela voudra dire
     * que
     * dans 100 milliseconde le timeremainig sera expirÃ©
     */
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Long curentTimeRemainingInMilliSecond;

    /*
     * C'est le time remaining en minute issu de la base de donnÃ©e: Soit de la
     * table ec_type_demande soit de la table eb_demande si le type de la
     * demande
     * est Other.
     */
    @Transient
    private Integer timeRemaining;

    @Transient
    @JsonDeserialize
    private List<Integer> ebDemandeFichierJointNum;

    @Transient
    @JsonIgnore
    private ExEbDemandeTransporteur selectedCarrier;

    @Transient
    private Double timeRemainingFf;

    private Boolean dg;

    private Boolean equipement;

    private Boolean sensitive;

    private Boolean dryIce;

    private Double totalNbrParcel;

    @Column
    private Double totalWeight;

    private Double totalVolume;

    private Integer xEcStatut;

    private Integer xEcStatutGroupage;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String statutStr;

    private Integer xEcMasterObjectType;

    // @ColumnDefault("f")
    private Boolean flagDocumentTransporteur;

    // @ColumnDefault("f")
    private Boolean flagDocumentCustoms;

    // @ColumnDefault("f")
    private Boolean flagTransportInformation;

    private Boolean flagCustomsInformation;

    private Boolean flagRequestCustomsBroker;

    // @ColumnDefault("f")
    private Boolean flagRecommendation;

    private Integer xEcTypeDemande;

    private Integer xEbTypeRequest;

    private String typeRequestLibelle;

    private Integer torDuration; // time since issuance or time remaining
    private Integer torType; // type of request duration

    private Integer xEcModeTransport;

    private String libelleOriginCountry;

    private String libelleDestCountry;

    @ManyToOne
    @JoinColumn(name = "x_eb_user")
    private EbUser user; // Owner of the request

    private String refTransport;

    private String temperatureTransport;

    private String temperatureStorage;

    private String codeAnnulation;

    private String aboutTrAwbBol;

    private String aboutTrMawb;

    private String aboutTrFlightVessel;

    private String aboutTrCustomOffice;

    @Temporal(TemporalType.TIMESTAMP)
    private Date aboutTrEta;

    @Temporal(TemporalType.TIMESTAMP)
    private Date aboutTrEtd;

    @Temporal(TemporalType.TIMESTAMP)
    private Date aboutTrPickUp;

    @Temporal(TemporalType.TIMESTAMP)
    private Date aboutTrFinalDelivery;

    private Integer xEcCancelled;
    @Column(columnDefinition = "TEXT")
    private String customerReference;
    private String serialNumber;

    private String shippingType;
    @Column(columnDefinition = "TEXT")
    private String partNumber;
    @Column(columnDefinition = "TEXT")
    private String orderNumber;

    private Boolean preAlertInfoAboutTransportSent;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDelivery;

    private Integer xEcTypeCustomBroker;

    private Double coutPrestationDouane;

    @ManyToOne
    @JoinColumn(name = "x_ec_broker", columnDefinition = "int", nullable = true)
    private EbUser xEcBroker;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement_broker")
    private EbEtablissement xEbEtablissementBroker;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie_broker")
    private EbCompagnie xEbCompagnieBroker;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCategorie> listCategories;

    /**
     * it represents the additional cost added in the context of essilor.
     * A la différence des cout de transport, ces couts ne sont pas prise en
     * compte lors de la
     * valorisation, ni pour le controle de facturation.
     * Pour Essilor il s'agit des cout que Essilor refacture à ses clients. le
     * transporteur n'as donc
     * pas de visibilité sur ces coups.
     */
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCost> listAdditionalCost;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb", nullable = false)
    @JsonFormat
    private List<EbTypeDocuments> listTypeDocuments;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    @JsonDeserialize
    private List<ProposionRdvDto> listPropRdvPk;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<ProposionRdvDto> listPropRdvDl;

    @ManyToOne
    @JoinColumn(name = "x_eb_costCenter")
    private EbCostCenter xEbCostCenter;

    private Double totalTaxableWeight;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbMarchandise> listMarchandises;

    private String listEmailSendQuotation;

    @Column(columnDefinition = "TEXT")
    private String customFields;

    private String pathImport;

    private String configPsl;

    private String configPslNum;

    /* infos transport */
    private String numAwbBol;
    private String flightVessel;
    private Date datePickupTM;
    private Date etd;
    private Date finalDelivery;
    private Date eta;
    private String customsOffice;
    private String mawb;
    private String carrierUniqRefNum;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_transport_info_maj")
    private EbUser xEbUserTransportInfoMaj;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_ct")
    private EbUser xEbUserCt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement_ct")
    private EbEtablissement xEbEtablissementCt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie_ct")
    private EbCompagnie xEbCompagnieCt;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private EbUser xEbUserImport;
    private Boolean flagConnectedAsCt;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbTtTracing> listEbTracing;
    private Boolean flagEbtrackTrace;
    private Boolean flagEbInvoice;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement")
    private EbEtablissement xEbEtablissement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", updatable = false, insertable = true)
    private EbCompagnie xEbCompagnie;

    // champs reservé pour customFields
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Boolean sendQuotation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_ct_required")
    private EbUser xEbUserCtRequired;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer statusQuote;

    @ManyToOne
    @JoinColumn(name = "x_ec_currency_invoice")
    private EcCurrency xecCurrencyInvoice;

    @ManyToOne
    @JoinColumn(name = "x_ec_currency_insurance")
    private EcCurrency xecCurrencyInsurance;

    private Boolean cancelled;

    @Column(columnDefinition = "TEXT")
    private String ebDemandeHistory; // contiendra une List<Map<String, Object>>
    // des champs modifiés en plus de
    // la date de modification

    private Boolean confirmPickup;
    private Boolean confirmDelivery;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer module;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer typeData;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Boolean sendEmails;

    private Integer moduleCreator;

    @OneToMany(mappedBy = "xEbDemande", fetch = FetchType.LAZY)
    private List<EbLivraison> livraisons;

    @Column(updatable = false)
    private String libelleOriginCity;

    @Column(updatable = false)
    private String libelleDestCity;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String companyOrigin;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String companyDest;

    @Column(columnDefinition = "TEXT")
    private String listCategoryFlat;

    @Column(columnDefinition = "TEXT")
    private String listCustomFieldsFlat; // exp : "fieldName" :"value";..

    @Column(columnDefinition = "TEXT")
    private String listCustomFieldsValueFlat; // exp: "fieldLabel" :"value";..

    private Integer xEcNature;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "point_intermediate")
    private EbParty pointIntermediate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_origin")
    private EbUser xEbUserOrigin;


    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbUser> xEbUserObserver = new ArrayList<>();

   @OneToMany(cascade = CascadeType.ALL)
   private Set<DemandeUserObservers> observers = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_dest")
    private EbUser xEbUserDest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_origin_customs_broker")
    private EbUser xEbUserOriginCustomsBroker;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_dest_customs_broker")
    private EbUser xEbUserDestCustomsBroker;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab_origin")
    private EbEtablissement xEbEtabOrigin;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab_dest")
    private EbEtablissement xEbEtabDest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab_origin_customs_broker")
    private EbEtablissement xEbEtabOriginCustomsBroker;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab_dest_customs_broker")
    private EbEtablissement xEbEtabDestCustomsBroker;

    private String listFlag;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbFlagDTO> listEbFlagDTO;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<TreeNode<EbMarchandise>> treeEbMarchandise;

    /****************** DEBUT ATTRIBUTS GESTION DES PSL *****************/

    private Integer xEbTypeFluxNum;

    private String xEbTypeFluxDesignation;

    private String xEbTypeFluxCode;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonDeserialize(using = ObjectDeserializer.class)
    private EbTtSchemaPsl xEbSchemaPsl;

    private Integer xEbIncotermNum;

    private String xEcIncotermLibelle;

    private String codeAlphaPslCourant;
    private String libellePslCourant;

    private Integer codeLastPsl;
    private String codeAlphaLastPsl;
    private String libelleLastPsl;

    private Date dateLastPsl;
    private Date dateTraitementCPB;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    List<Long> listDeliveryForConsolidateNum;

    @Column(updatable = false, insertable = true)
    private String integratedVia; // à virer par la suite

    /****************** FIN ATTRIBUTS GESTION DES PSL *****************/

    private Boolean flagConsolidated; // si la demande est un regroupement
                                      // d'autres demandes (demande resultante)

    private Boolean isValorized;

    private Integer xEbDemandeGroup; // num de la demande resultante dont cette
                                     // demande fait partie

    private String carrierComment;

    private Boolean tdcAcknowledge;
    private Date tdcAcknowledgeDate;

    private Boolean eligibleForConsolidation;

    private Boolean isGrouping;

    private Integer trackingLevel;

		private String													consolidationRuleLibelle;

    @Column
    @ColumnDefault("false")
    private Boolean isDraft;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private EbQrGroupe ebQrGroupe;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<TransportReferenceInfosDTO> consolidationListTransportReferenceInfos;
    @Column
    @ColumnDefault("false")
    private Boolean finalChoiceToBeCalculated;

    private String scenario;

    private String codeConfigurationEDI;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbMarchandise> oldMarchandises;

    @Transient
    @JsonSerialize
    private List<EbDemande> initialTrInGroupedTransportRequest;

    public List<EbDemande> getInitialTrInGroupedTransportRequest() {
        return initialTrInGroupedTransportRequest;
    }

    public void setInitialTrInGroupedTransportRequest(List<EbDemande> initialTrInGroupedTransportRequest) {
        this.initialTrInGroupedTransportRequest = initialTrInGroupedTransportRequest;
    }

    public String getCodeConfigurationEDI() {
        return codeConfigurationEDI;
    }

    public void setCodeConfigurationEDI(String codeConfigurationEDI) {
        this.codeConfigurationEDI = codeConfigurationEDI;
    }

    public BigDecimal getSaving() {
        return saving;
    }

    public Boolean getFinalChoiceToBeCalculated() {
        return finalChoiceToBeCalculated;
    }

    public void setFinalChoiceToBeCalculated(Boolean finalChoiceToBeCalculated) {
        this.finalChoiceToBeCalculated = finalChoiceToBeCalculated;
    }

    public void setSaving(BigDecimal saving) {
        this.saving = saving;
    }

    public String getCarrierComment() {
        return carrierComment;
    }

    public void setCarrierComment(String carrierComment) {
        this.carrierComment = carrierComment;
    }

    private Boolean askForTransportResponsibility;

    private Boolean provideTransport;

    private Integer exportControlStatut;

    public List<TreeNode<EbMarchandise>> getTreeEbMarchandise() {
        return treeEbMarchandise;
    }

    public void setTreeEbMarchandise(List<TreeNode<EbMarchandise>> treeEbMarchandise) {
        this.treeEbMarchandise = treeEbMarchandise;
    }

    public List<EbFlagDTO> getListEbFlagDTO() {
        return listEbFlagDTO;
    }

    public void setListEbFlagDTO(List<EbFlagDTO> listEbFlag) {
        this.listEbFlagDTO = listEbFlag;
    }

    public EbParty getPointIntermediate() {
        return pointIntermediate;
    }

    public void setPointIntermediate(EbParty pointIntermediate) {
        this.pointIntermediate = pointIntermediate;
    }

    public EbDemande() {
    }

    public EbDemande(Integer ebDemandeNum, EbParty origin, EbParty dest) {
        this.ebDemandeNum = ebDemandeNum;
        this.ebPartyOrigin = origin;
        this.ebPartyDest = dest;
        this.libelleOriginCity = origin.getCity();
        this.libelleDestCity = dest.getCity();
    }

    public EbDemande(Integer ebDemandeNum) {
        this.ebDemandeNum = ebDemandeNum;
    }

    public EbDemande(
        Integer ebDemandeNum,
        String refTransport,
        Integer xEcStatut,
        String codeAlphaPslCourant,
        String customerReference) {
        this.ebDemandeNum = ebDemandeNum;
        this.refTransport = refTransport;
        this.xEcStatut = xEcStatut;
        this.codeAlphaPslCourant = codeAlphaPslCourant;
        this.customerReference = customerReference;
    }

    public EbDemande(Integer ebDemandeNum, Object listCategories) {
        this.ebDemandeNum = ebDemandeNum;

        if (listCategories != null) {

            try {
                ObjectMapper objectMapper = new ObjectMapper();

                String json = objectMapper.writeValueAsString(listCategories);
                this.listCategories = objectMapper.readValue(json, new TypeReference<ArrayList<EbCategorie>>() {
                });
            } catch (Exception e) {
                System.err.println("ConnectedUser: failed to convert object to listCategories.");
            }

        }

    }

    public EbDemande(Integer ebDemandeNum, Integer xEcModeTransport, String xEcIncotermLibelle, Object xEbSchemaPsl) {
        this.ebDemandeNum = ebDemandeNum;
        this.xEcModeTransport = xEcModeTransport;
        this.xEcIncotermLibelle = xEcIncotermLibelle;

        if (xEbSchemaPsl != null) {

            try {
                ObjectMapper objectMapper = new ObjectMapper();

                String json = objectMapper.writeValueAsString(xEbSchemaPsl);
                this.xEbSchemaPsl = objectMapper.readValue(json, EbTtSchemaPsl.class);

                json = objectMapper.writeValueAsString(this.xEbSchemaPsl.getListPsl());
                List<EbTtCompanyPsl> listPsl = objectMapper
                    .readValue(json, new TypeReference<ArrayList<EbTtCompanyPsl>>() {
                    });
                this.xEbSchemaPsl.setListPsl(listPsl);
            } catch (Exception e) {
                System.out.println("EbDemande: Cannot convert json to schema / list Psl");
            }

        }

    }

    // l'objectif de ce constructeur et de recuperer list type document selon
    // format
    // List<EbTypeDocument>
    public EbDemande(Object listTypeDocument, Integer ebDemandeNum) {
        this.ebDemandeNum = ebDemandeNum;

        if (listTypeDocument != null) {

            try {
                ObjectMapper objectMapper = new ObjectMapper();

                this.listTypeDocuments = objectMapper
                    .convertValue(listTypeDocument, new TypeReference<ArrayList<EbTypeDocuments>>() {
                    });
            } catch (Exception e) {
                System.err.println("ConnectedUser: failed to convert object to listTypeDocument.");
            }

        }

    }

    @QueryProjection
    public EbDemande(Integer ebDemandeNum, String refTransport) {
        this.ebDemandeNum = ebDemandeNum;
        this.refTransport = refTransport;
    }

    @QueryProjection
    public EbDemande(Integer ebDemandeNum, String refTransport, String customerReference) {
        this.ebDemandeNum = ebDemandeNum;
        this.refTransport = refTransport;
        this.customerReference = customerReference;
    }

    // Mode transport
    @QueryProjection
    public EbDemande(Integer ebDemandeNum, Integer xEcModeTransport) {
        this.ebDemandeNum = ebDemandeNum;
        this.xEcModeTransport = xEcModeTransport;
    }

    // projection for favoris in vignette home page
    @QueryProjection
    public EbDemande(
        Integer ebDemandeNum,
        String listFlag,
        EbCompagnie xEbCompagnie,
        String refTransport,
        Integer xEcModeTransport,
        String libelleOriginCity,
        String libelleOriginCountry,
        String libelleDestCity,
        String libelleDestCountry,
        EbUser connectedUser,
        Integer module,
        Integer xEcStatut,
        Integer numberOrderWithExpression) {
        this.ebDemandeNum = ebDemandeNum;
        this.listFlag = listFlag;
        this.xEbCompagnie = xEbCompagnie;
        this.refTransport = refTransport;
        this.xEcStatut = xEcStatut;
        this.xEcModeTransport = xEcModeTransport;
        this.libelleOriginCity = libelleOriginCity;
        this.libelleOriginCountry = libelleOriginCountry;
        this.libelleDestCity = libelleDestCity;
        this.libelleDestCountry = libelleDestCountry;
        this.statutStr = DemandeStatusUtils.getStatusLibelleFromEbDemande(this, module, connectedUser);
    }

    @QueryProjection
    public EbDemande(
        Integer ebDemandeNum,
        String refTransport,
        String costCenterLibelle,
        String customerReference,
        Double totalWeight,
        Integer xEcModeTransport,
        Date dateCreation,
        Integer xEcStatut,
        Integer xEcTypeDemande,
        String libelleDestCountry,
        String libelleOriginCountry,
        Boolean flagRecommendation,
        Integer xEbTypeTransport,
        Integer xEcTypeCustomBroker,
        Integer xEbTransporteur,
        String customFields,
        Integer usernum,
        Integer compagnieNum,
        List<EbCategorie> listCategories,
        String unitsReference,
        Integer statusQuote,
        Integer currencynum,
        String unitsPackingList) {
        this.ebDemandeNum = ebDemandeNum;
        this.refTransport = refTransport;
        this.customerReference = customerReference;
        this.libelleOriginCountry = libelleOriginCountry;
        this.libelleDestCountry = libelleDestCountry;
        this.totalWeight = totalWeight;
        this.xEcModeTransport = xEcModeTransport;
        this.dateCreation = dateCreation;
        this.xEcTypeDemande = xEcTypeDemande;
        this.xEcStatut = xEcStatut;
        this.flagRecommendation = flagRecommendation;
        this.xEbTypeTransport = xEbTypeTransport;
        this.xEcTypeCustomBroker = xEcTypeCustomBroker;
        this.xEbTransporteur = xEbTransporteur;
        this.customFields = customFields;
        this.user = new EbUser(usernum);
        this.statusQuote = statusQuote;
        this.user.setEbCompagnie(new EbCompagnie(compagnieNum));
        this.xecCurrencyInvoice = new EcCurrency(currencynum);
        this.xecCurrencyInsurance = new EcCurrency(currencynum);

        if (customFields != null) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                this.listCustomsFields = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (costCenterLibelle != null) {
            this.xEbCostCenter = new EbCostCenter();
            xEbCostCenter.setLibelle(costCenterLibelle);
        }

        this.listCategories = listCategories;

        this.getDateRemaing();

        this.unitsReference = unitsReference;
        this.unitsPackingList = unitsPackingList;
    }

    // dashboard Pricing test
    @QueryProjection
    public EbDemande(
        Integer ebDemandeNum,
        String refTransport,
        String costCenterLibelle,
        String customerReference,
        String partNumber,
        String serialNumber,
        String shippingType,
        String orderNumber,
        Double totalWeight,
        Double totalVolume,
        Double totalNbrParcel,
        Integer xEcModeTransport,
        Date dateCreation,
        Date updateDate,
        Integer xEcStatut,
        Integer xEcTypeDemande,
        Integer xEbTypeRequest,
        String typeRequestLibelle,
        String libelleDestCountry,
        String libelleOriginCountry,
        String city,
        Boolean flagRecommendation,
        Integer xEbTypeTransport,
        Integer xEcTypeCustomBroker,
        Integer xEbTransporteur,
        Integer xEbTransporteurEtablissementNum,
        String customFields,
        Integer usernum,
        String nomChargeur,
        String prenomChargeur,
        String emailChargeur,
        Integer compagnieNum,
        EbTtSchemaPsl xEbSchemaPsl,
        Date waitingForQuote,
        Date waitingForConf,
        Date confirmed,
        List<EbCategorie> listCategories,
        String unitsReference,
        Integer currencynum,
        Integer xEcCancelled,
        Boolean confirmedFromTransptPlan,
        String companyOrigin,
        String companyDest,
        String orderWithExpression,
        Integer numberOrderWithExpression,
        String libelleOriginCity,
        String libelleDestCity,
        String xEcIncotermLibelle,
        String numAwbBol,
        String compagnieNom,
        Integer xEcNature,
        Integer ebDemandeMasterObjectNum,
        String ebDemandeMasterObjectRefTransport,
        String listFlag,
        EbUser connectedUser,
        Integer module,
        Boolean flagConsolidated,
        Integer xEbDemandeGroup,
        Integer numCt,
        String nomCt,
        String prenomCt,
        List<EbTypeDocuments> listTypeDocuments,
        Boolean askForTransportResponsibility,
        Boolean provideTransport,
        Boolean tdcAcknowledge,
        Date tdcAcknowledgeDate,
        Boolean eligibleForConsolidation,
        Date customerCreationDate,
        String carrierUniqRefNum,
        String unitsPackingList,
        Integer typeOfRequestType,
        Integer typeOfRequestDuration,
			Integer valuationType,
			String consolidationRuleLibelle
		)
		{
        this.carrierUniqRefNum = carrierUniqRefNum;
        this.ebDemandeNum = ebDemandeNum;
        this.refTransport = refTransport;
        this.customerReference = customerReference;
        this.partNumber = partNumber;
        this.serialNumber = serialNumber;
        this.shippingType = shippingType;
        this.orderNumber = orderNumber;
        this.libelleOriginCountry = libelleOriginCountry;
        this.libelleDestCountry = libelleDestCountry;
        this.city = city;
        this.totalWeight = totalWeight;
        this.totalVolume = totalVolume;
        this.totalNbrParcel = totalNbrParcel;
        this.xEcModeTransport = xEcModeTransport;
        this.dateCreation = dateCreation;
        this.updateDate = updateDate;
        this.xEcTypeDemande = xEcTypeDemande;
        this.xEbTypeRequest = xEbTypeRequest;
        this.typeRequestLibelle = typeRequestLibelle;
        this.xEcStatut = xEcStatut;
        this.flagRecommendation = flagRecommendation;
        this.xEbTypeTransport = xEbTypeTransport;
        this.xEcTypeCustomBroker = xEcTypeCustomBroker;
        this.xEbTransporteur = xEbTransporteur;
        this.xEbTransporteurEtablissementNum = xEbTransporteurEtablissementNum;
        this.customFields = customFields;
        this.user = new EbUser(usernum, nomChargeur, prenomChargeur, emailChargeur, null, null);
        this.user.setEbCompagnie(new EbCompagnie(compagnieNum, compagnieNom, null));
        this.xecCurrencyInvoice = new EcCurrency(currencynum);
        this.xecCurrencyInsurance = new EcCurrency(currencynum);
        this.xEcCancelled = xEcCancelled;
        this.confirmedFromTransptPlan = confirmedFromTransptPlan;
        this.companyOrigin = companyOrigin;
        this.companyDest = companyDest;
        this.libelleOriginCity = libelleOriginCity;
        this.libelleDestCity = libelleDestCity;
        this.xEcIncotermLibelle = xEcIncotermLibelle;
        this.numAwbBol = numAwbBol;
        this.xEcNature = xEcNature;
        this.torDuration = typeOfRequestDuration;
        this.torType = typeOfRequestType;
        this.xEbSchemaPsl = xEbSchemaPsl;
        this.waitingForConf = waitingForConf;
        this.waitingForQuote = waitingForQuote;
        this.confirmed = confirmed;
        this.valuationType = valuationType;
				this.consolidationRuleLibelle = consolidationRuleLibelle;

        if (ebDemandeMasterObjectNum != null) {
            EbDemande mutiSegment = new EbDemande();
            mutiSegment.setEbDemandeNum(ebDemandeMasterObjectNum);
            mutiSegment.setRefTransport(ebDemandeMasterObjectRefTransport);
            this.ebDemandeMasterObject = mutiSegment;
        }
        else {
            this.ebDemandeMasterObject = null;
        }

        if (customFields != null && !customFields.isEmpty()) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                this.listCustomsFields = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (costCenterLibelle != null) {
            this.xEbCostCenter = new EbCostCenter();
            xEbCostCenter.setLibelle(costCenterLibelle);
        }

        if (listCategories != null && listCategories.size() > 0) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String jsonCateg = objectMapper.writeValueAsString(listCategories);
                this.listCategories = objectMapper.readValue(jsonCateg, new TypeReference<ArrayList<EbCategorie>>() {
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        this.getDateRemaing();

        this.unitsReference = unitsReference;
        this.listFlag = listFlag;

        this.flagConsolidated = flagConsolidated;
        this.xEbDemandeGroup = xEbDemandeGroup;

        ObjectMapper objectMapper = new ObjectMapper();

        // pour deserialiser la liste et eviter l'erreur lincked hashMap
        if (listTypeDocuments != null) {
            listTypeDocuments = Arrays.asList(objectMapper.convertValue(listTypeDocuments, EbTypeDocuments[].class));
            this.listTypeDocuments = listTypeDocuments
                .stream().filter(it -> it.getModule() != null && it.getModule().contains(Modules.PRICING.getValue()))
                .collect(Collectors.toList());
        }
        else {
            listTypeDocuments = null;
        }

        this.xEbUserCt = new EbUser(numCt, nomCt, prenomCt);

        this.statutStr = DemandeStatusUtils.getStatusLibelleFromEbDemande(this, module, connectedUser);

        this.askForTransportResponsibility = askForTransportResponsibility;
        this.provideTransport = provideTransport;
        this.tdcAcknowledge = tdcAcknowledge;
        this.tdcAcknowledgeDate = tdcAcknowledgeDate;
        this.eligibleForConsolidation = eligibleForConsolidation;
        this.customerCreationDate = customerCreationDate;
        this.unitsPackingList = unitsPackingList;
    }

    public Integer getxEbTypeRequest() {
        return xEbTypeRequest;
    }

    public void setxEbTypeRequest(Integer xEbTypeRequest) {
        this.xEbTypeRequest = xEbTypeRequest;
    }

    public EbUser getxEbUserOrigin() {
        return xEbUserOrigin;
    }

    public void setxEbUserOrigin(EbUser xEbUserOrigin) {
        this.xEbUserOrigin = xEbUserOrigin;
    }

    public EbUser getxEbUserDest() {
        return xEbUserDest;
    }

    public void setxEbUserDest(EbUser xEbUserDest) {
        this.xEbUserDest = xEbUserDest;
    }

    public List<EbUser> getxEbUserObserver() {return xEbUserObserver;}

    public void setxEbUserObserver(List<EbUser> xEbUserObserver) {
        this.xEbUserObserver = xEbUserObserver;
    }

    public EbUser getxEbUserOriginCustomsBroker() {
        return xEbUserOriginCustomsBroker;
    }

    public void setxEbUserOriginCustomsBroker(EbUser xEbUserOriginCustomsBroker) {
        this.xEbUserOriginCustomsBroker = xEbUserOriginCustomsBroker;
    }

    public EbUser getxEbUserDestCustomsBroker() {
        return xEbUserDestCustomsBroker;
    }

    public void setxEbUserDestCustomsBroker(EbUser xEbUserDestCustomsBroker) {
        this.xEbUserDestCustomsBroker = xEbUserDestCustomsBroker;
    }

    // queryprojection dashboard for TM
    @QueryProjection
    public EbDemande(
        Integer ebDemandeNum,
        String refTransport,
        String costCenterLibelle,
        String customerReference,
        String partNumber,
        String serialNumber,
        String shippingType,
        String orderNumber,
        Double totalWeight,
        Integer xEcModeTransport,
        Date dateCreation,
        Date updateDate,
        Integer xEcStatut,
        Integer xEcTypeDemande,
        Integer xEbTypeRequest,
        String typeRequestLibelle,
        String libelleDestCountry,
        String libelleOriginCountry,
        Integer xEbTypeTransport,
        Integer xEcTypeCustomBroker,
        Integer nbrDoc,
        Boolean flagRecommendation,
        Boolean flagDocumentCustoms,
        Boolean flagDocumentTransporteur,
        Boolean flagTransportInformation,
        Boolean flagCustomsInformation,
        Boolean flagRequestCustomsBroker,
        Integer xEbTransporteur,
        String customFields,
        Integer usernum,
        String userNom,
        String userPrenom,
        String userEmail,
        Integer numCompagnieChargeur,
        String nomCompagnieChargeur,
        String codeCompagnieChargeur,
        List<EbCategorie> listCategories,
        Date datePickupTM,
        String unitsReference,
        Integer currencynum,
        Boolean confirmPickup,
        Boolean confirmDelivery,
        Date dateOfGoodsAvailability,
        Date finalDelivery,
        String companyOrigin,
        String companyDest,
        String orderWithExpression,
        Integer numberOrderWithExpression,
        Integer xEcCancelled,
        String libelleOriginCity,
        String libelleDestCity,
        Integer xEbTypeFluxNum,
        String xEbTypeFluxDesignation,
        String xEbTypeFluxCode,
        Integer xEbIncotermNum,
        String xEcIncotermLibelle,
        String numAwbBol,
        String carrierUniqRefNum,
        String libelleLastPsl,
        String codeAlphaLastPsl,
        Date dateLastPsl,
        String codeAlphaPslCourant,
        Integer xEcNature,
        Integer ebDemandeMasterObjectNum,
        String ebDemandeMasterObjectRefTransport,
        String listFlag,
        EbUser connectedUser,
        Integer module,
        List<ProposionRdvDto> listPropRdvPk,
        List<ProposionRdvDto> listPropRdvDl,
        Integer numCt,
        String nomCt,
        String prenomCt,
        EbTtSchemaPsl xEbSchemaPsl,
        List<EbTypeDocuments> listTypeDocuments,
        Boolean askForTransportResponsibility,
        Boolean provideTransport,
        Boolean tdcAcknowledge,
        Date tdcAcknowledgeDate,
        Date customerCreationDate,
        String unitsPackingList,
        BigDecimal prixTransporteurFinal

    ) {
        this.ebDemandeNum = ebDemandeNum;
        this.refTransport = refTransport;
        this.partNumber = partNumber;
        this.serialNumber = serialNumber;
        this.shippingType = shippingType;
        this.orderNumber = orderNumber;
        this.customerReference = customerReference;
        this.libelleOriginCountry = libelleOriginCountry;
        this.libelleDestCountry = libelleDestCountry;
        this.totalWeight = totalWeight;
        this.xEcModeTransport = xEcModeTransport;
        this.dateCreation = dateCreation;
        this.updateDate = updateDate;
        this.xEcTypeDemande = xEcTypeDemande;
        this.xEbTypeRequest = xEbTypeRequest;
        this.typeRequestLibelle = typeRequestLibelle;

        this.xEcStatut = xEcStatut;
        this.xEbTypeTransport = xEbTypeTransport;
        this.xEcTypeCustomBroker = xEcTypeCustomBroker;
        this.flagRecommendation = flagRecommendation;
        this.nbrDoc = nbrDoc;
        this.flagDocumentCustoms = flagDocumentCustoms;
        this.flagDocumentTransporteur = flagDocumentTransporteur;
        this.flagTransportInformation = flagTransportInformation;
        this.flagRequestCustomsBroker = flagRequestCustomsBroker;
        this.flagCustomsInformation = flagCustomsInformation;
        this.xEbTransporteur = xEbTransporteur;
        this.customFields = customFields;
        this.user = new EbUser(usernum, userNom, userPrenom, userEmail, null, null);
        this.user.setEbCompagnie(new EbCompagnie(numCompagnieChargeur, nomCompagnieChargeur, codeCompagnieChargeur));
        this.xEbUserCt = new EbUser(numCt, nomCt, prenomCt);
        this.datePickupTM = datePickupTM;
        this.unitsReference = unitsReference;
        this.unitsPackingList = unitsPackingList;
        this.listCategories = listCategories;
        this.xecCurrencyInvoice = new EcCurrency(currencynum);
        this.xecCurrencyInsurance = new EcCurrency(currencynum);
        this.dateOfGoodsAvailability = dateOfGoodsAvailability;
        this.finalDelivery = finalDelivery;
        this.confirmDelivery = confirmDelivery;
        this.confirmPickup = confirmPickup;
        this.xEcCancelled = xEcCancelled;
        this.companyOrigin = companyOrigin;
        this.companyDest = companyDest;
        this.libelleOriginCity = libelleOriginCity;
        this.libelleDestCity = libelleDestCity;

        /* Type de flux, schema Psl, et incoterm */
        this.xEbTypeFluxNum = xEbTypeFluxNum;
        this.xEbTypeFluxCode = xEbTypeFluxCode;
        this.xEbTypeFluxDesignation = xEbTypeFluxDesignation;
        this.xEbSchemaPsl = xEbSchemaPsl;
        this.xEbIncotermNum = xEbIncotermNum;
        this.xEcIncotermLibelle = xEcIncotermLibelle;
        /* Fin Type de flux, schema Psl, et incoterm */

        this.numAwbBol = numAwbBol;
        this.carrierUniqRefNum = carrierUniqRefNum;
        this.xEcNature = xEcNature;

        this.libelleLastPsl = libelleLastPsl;
        this.codeAlphaLastPsl = codeAlphaLastPsl;

        this.dateLastPsl = dateLastPsl;
        this.codeAlphaPslCourant = codeAlphaPslCourant;

        if (ebDemandeMasterObjectNum != null) {
            EbDemande mutiSegment = new EbDemande();
            mutiSegment.setEbDemandeNum(ebDemandeMasterObjectNum);
            mutiSegment.setRefTransport(ebDemandeMasterObjectRefTransport);
            this.ebDemandeMasterObject = mutiSegment;
        }
        else {
            this.ebDemandeMasterObject = null;
        }

        if (customFields != null) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                this.listCustomsFields = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (listCategories != null && listCategories.size() > 0) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String jsonCateg = objectMapper.writeValueAsString(listCategories);
                this.listCategories = objectMapper.readValue(jsonCateg, new TypeReference<ArrayList<EbCategorie>>() {
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (costCenterLibelle != null)

        {
            this.xEbCostCenter = new EbCostCenter();
            xEbCostCenter.setLibelle(costCenterLibelle);
        }

        this.exEbDemandeTransporteurFinal = new EbUser(xEbTransporteur, nomTransporteur, null, null);

        this.getDateRemaing();

        this.listFlag = listFlag;

        this.statutStr = DemandeStatusUtils.getStatusLibelleFromEbDemande(this, module, connectedUser);
        this.listPropRdvDl = listPropRdvDl;
        this.listPropRdvPk = listPropRdvPk;

        /*
         * if (listTypeDocuments != null) this.listTypeDocuments =
         * listTypeDocuments;
         * else this.listTypeDocuments = null;
         */
        ObjectMapper objectMapper = new ObjectMapper();

        // pour deserialiser la liste et eviter l'erreur lincked hashMap
        if (listTypeDocuments != null) {
            listTypeDocuments = Arrays.asList(objectMapper.convertValue(listTypeDocuments, EbTypeDocuments[].class));

            this.listTypeDocuments = listTypeDocuments
                .stream()
                .filter(
                    it -> it.getModule() != null && it.getModule().contains(Modules.TRANSPORT_MANAGEMENT.getValue()))
                .collect(Collectors.toList());
        }
        else {
            listTypeDocuments = null;
        }

        this.askForTransportResponsibility = askForTransportResponsibility;
        this.provideTransport = provideTransport;
        this.tdcAcknowledge = tdcAcknowledge;
        this.tdcAcknowledgeDate = tdcAcknowledgeDate;
        this.customerCreationDate = customerCreationDate;
        this.prixTransporteurFinal = prixTransporteurFinal;
    }

    // Projection details demande.
    @QueryProjection
    public EbDemande(
        Integer ebDemandeNum,
        String refTransport,
        Integer costCenterNum,
        String costCenterLibelle,
        String customerReference,

        Double totalWeight,
        Integer xEcModeTransport,
        Date dateCreation,
        Integer xEcStatut,
        Integer xEcTypeDemande,
        Integer xEbTypeRequest,
        String typeRequestLibelle,

        String libelleDestCountry,
        String libelleOriginCountry,
        Date dateOfGoodsAvailability,
        Date dateOfArrival,

        Date datePickup,
        EbParty ebPartyOrigin,
        EbParty ebPartyDest,
        EbParty ebPartyNotif,
        Integer xEbTypeTransport,

        Boolean flagRecommendation,
        String commentChargeur,
        Double totalVolume,

        Double totalTaxableWeight,
        Boolean insurance,
        Double insuranceValue,
        String city,

        List<EbCategorie> listCategories,
        List<EbTypeDocuments> listTypeDocuments,
        Double totalNbrParcel,
        Integer xEcBrokerOrigin,
        String xEcBrokerOriginEmail,
        Integer xEcBrokerOriginEtablissementNum,
        Integer xEcBrokerDest,
        String xEcBrokerDestEmail,
        Integer xEcBrokerDestEtablissementNum,
        Integer xEcTypeCustomBroker,
        Integer xEbTransporteur,

        Integer xEbTransporteurEtablissementNum,
        Boolean flagDocumentCustoms,
        Boolean flagDocumentTransporteur,

        Boolean flagTransportInformation,
        Boolean flagCustomsInformation,
        Boolean flagRequestCustomsBroker,

        String customFields,
        String configPsl,
        String numAwbBol,
        String carrierUniqRefNum,
        String flightVessel,
        Date datePickupTM,
        Date etd,

        Date finalDelivery,
        Date eta,
        String customsOffice,
        String mawb,
        Boolean flagEbtrackTrace,

        Boolean flagEbInvoice,
        EbUser chargeur,

        Integer chargeurEtabNum,
        String chargeurEtabNom,
        Integer chargeurCompNum,
        String chargeurCompNom,
        String chargeurCompCode,
        Integer currencynum,

        String currencyCode,
        String ebDemandeHistory,
        Integer moduleCreator,
        String listLabels,

        Integer xEcCancelled,
        Integer xEcNature,
        Integer pointIntermediateNum,
        String reference,
        String company,

        Integer ebDemandeMasterObjectNum,
        String ebDemandeMasterObjectRefTransport,
        Integer ebDemandeInitialNum,
        String ebDemandeInitialRefTransport,

        Integer userOriginEbUserNum,
        String userOriginNom,
        Integer userOriginEtabNum,
        String userOriginEtabNom,

        Integer userDestEbUserNum,
        String userDestNom,
        Integer userDestEtabNum,
        String userDestEtabNom,

        Integer userOriginCBEbUserNum,
        String userOriginCBNom,
        Integer userOriginCBEtabNum,
        String userOriginCBEtabNom,

        Integer userDestCBEbUserNum,
        String userDestCBNom,
        Integer userDestCBEtabNum,
        String userDestCBEtabNom,
        String listFlag,

        Integer xEbTypeFluxNum,
        String xEbTypeFluxCode,
        String xEbTypeFluxDesignation,
        Integer xEbIncotermNum,
        String xEcIncotermLibelle,
        EbTtSchemaPsl schemaPsl,
        EbEntrepot entrepot,
        Integer xEbDemandeGroup,
        String carrierComment,
        Boolean askForTransportResponsibility,
        Boolean provideTransport,
        Boolean tdcAcknowledge,
        Date tdcAcknowledgeDate,
        Boolean eligibleForConsolidation,
        Date customerCreationDate,
        Integer statutGroupage,
        List<EbCost> listAdditionalCost,
        Date dateMaj,
        Boolean finalChoiceToBeCalculated,
        Integer exportControlStatut,
			Boolean isValorized,
		String	codeConfigurationEDI
		)
		{
        this.dateMaj = dateMaj;
        this.listAdditionalCost = listAdditionalCost;
        this.xEcStatutGroupage = statutGroupage;
        this.ebDemandeNum = ebDemandeNum;
        this.refTransport = refTransport;
        this.customerReference = customerReference;
        this.libelleOriginCountry = libelleOriginCountry;
        this.libelleDestCountry = libelleDestCountry;
        this.dateOfGoodsAvailability = dateOfGoodsAvailability;
        this.dateOfArrival = dateOfArrival;
        this.datePickup = datePickup;
        this.totalWeight = totalWeight;
        this.xEcModeTransport = xEcModeTransport;
        this.dateCreation = dateCreation;
        this.xEcTypeDemande = xEcTypeDemande;
        this.xEbTypeRequest = xEbTypeRequest;
        this.typeRequestLibelle = typeRequestLibelle;
        this.xEcStatut = xEcStatut;
        this.ebPartyOrigin = ebPartyOrigin != null ? new EbParty(ebPartyOrigin) : null;
        this.ebPartyDest = ebPartyDest != null ? new EbParty(ebPartyDest) : null;
        this.ebPartyNotif = ebPartyNotif != null ? new EbParty(ebPartyNotif) : null;
        this.xEbTypeTransport = xEbTypeTransport;
        this.flagRecommendation = flagRecommendation;
        this.commentChargeur = commentChargeur;
        this.totalVolume = totalVolume;
        this.totalTaxableWeight = totalTaxableWeight;
        this.insurance = insurance;
        this.insuranceValue = insuranceValue;
        this.city = city;
        this.listCategories = listCategories;
        this.listTypeDocuments = listTypeDocuments;
        this.totalNbrParcel = totalNbrParcel;

        if (Objects.nonNull(xEcBrokerOrigin)) {
            this.xEbUserOriginCustomsBroker = new EbUser(xEcBrokerOrigin);
            this.xEbUserOriginCustomsBroker.setEmail(xEcBrokerOriginEmail);
            this.xEbUserOriginCustomsBroker.setEbEtablissement(new EbEtablissement(xEcBrokerOriginEtablissementNum));
        }

        if (Objects.nonNull(xEcBrokerDest)) {
            this.xEbUserDestCustomsBroker = new EbUser(xEcBrokerDest);
            this.xEbUserDestCustomsBroker.setEmail(xEcBrokerDestEmail);
            this.xEbUserDestCustomsBroker.setEbEtablissement(new EbEtablissement(xEcBrokerDestEtablissementNum));
        }
        this.xEcTypeCustomBroker = xEcTypeCustomBroker;
        this.xEbTransporteur = xEbTransporteur;
        this.xEbTransporteurEtablissementNum = xEbTransporteurEtablissementNum;
        this.flagDocumentCustoms = flagDocumentCustoms;
        this.flagDocumentTransporteur = flagDocumentTransporteur;
        this.flagTransportInformation = flagTransportInformation;
        this.flagCustomsInformation = flagCustomsInformation;
        this.flagRequestCustomsBroker = flagRequestCustomsBroker;
        this.customFields = customFields;
        this.configPsl = configPsl;
        this.numAwbBol = numAwbBol;
        this.carrierUniqRefNum = carrierUniqRefNum;
        this.flightVessel = flightVessel;
        this.datePickupTM = datePickupTM;
        this.etd = etd;
        this.finalDelivery = finalDelivery;
        this.eta = eta;
        this.customsOffice = customsOffice;
        this.mawb = mawb;
        this.flagEbtrackTrace = flagEbtrackTrace;
        this.flagEbInvoice = flagEbInvoice;
        this.user = new EbUser(chargeur);
        this.user.setEbEtablissement(new EbEtablissement(chargeurEtabNum, chargeurEtabNom));
        this.user.setEbCompagnie(new EbCompagnie(chargeurCompNum, chargeurCompNom, chargeurCompCode));
        this.xEbEtablissement = new EbEtablissement(chargeurEtabNum, chargeurEtabNom);
        this.xEbEtablissement.setEbCompagnie(new EbCompagnie(chargeurCompNum, chargeurCompNom, chargeurCompCode));
        this.xEbCompagnie = new EbCompagnie(chargeurCompNum, chargeurCompNom, chargeurCompCode);
        this.xecCurrencyInvoice = new EcCurrency(currencynum);
        this.xecCurrencyInvoice.setCode(currencyCode);
        this.xecCurrencyInsurance = new EcCurrency(currencynum);
        this.xecCurrencyInsurance.setCode(currencyCode);
        this.ebDemandeHistory = ebDemandeHistory;
        this.moduleCreator = moduleCreator;
        this.listLabels = listLabels;
        this.xEcCancelled = xEcCancelled;
        this.xEcNature = xEcNature;
        this.carrierComment = carrierComment;
        this.exportControlStatut = exportControlStatut;
        this.codeConfigurationEDI = codeConfigurationEDI;

        if (pointIntermediateNum != null) {
            this.pointIntermediate = new EbParty();
            this.pointIntermediate.setEbPartyNum(pointIntermediateNum);
            this.pointIntermediate.setReference(reference);
            this.pointIntermediate.setCompany(company);
        }

        if (ebDemandeMasterObjectNum != null) {
            EbDemande mutiSegment = new EbDemande();
            mutiSegment.setEbDemandeNum(ebDemandeMasterObjectNum);
            mutiSegment.setRefTransport(ebDemandeMasterObjectRefTransport);
            this.ebDemandeMasterObject = mutiSegment;
        }

        if (ebDemandeInitialNum != null) {
            EbDemande demandeInitial = new EbDemande();
            demandeInitial.setEbDemandeNum(ebDemandeInitialNum);
            demandeInitial.setRefTransport(ebDemandeInitialRefTransport);
            this.ebDemandeInitial = demandeInitial;
        }

        if (costCenterLibelle != null) {
            this.xEbCostCenter = new EbCostCenter();
            xEbCostCenter.setLibelle(costCenterLibelle);
            xEbCostCenter.setEbCostCenterNum(costCenterNum);
        }

        if (userOriginEbUserNum != null) {
            this.xEbUserOrigin = new EbUser(userOriginEbUserNum);
            this.xEbUserOrigin.setNom(userOriginNom);
            this.xEbUserOrigin.setEbEtablissement(new EbEtablissement(userOriginEtabNum, userOriginEtabNom));
        }

        if (userDestEbUserNum != null) {
            this.xEbUserDest = new EbUser(userDestEbUserNum);
            this.xEbUserDest.setNom(userDestNom);
            this.xEbUserDest.setEbEtablissement(new EbEtablissement(userDestEtabNum, userDestEtabNom));
        }

        if (userOriginCBEbUserNum != null) {
            this.xEbUserOriginCustomsBroker = new EbUser(userOriginCBEbUserNum);
            this.xEbUserOriginCustomsBroker.setNom(userOriginCBNom);
            this.xEbUserOriginCustomsBroker
                .setEbEtablissement(new EbEtablissement(userOriginCBEtabNum, userOriginCBEtabNom));
        }

        if (userDestCBEbUserNum != null) {
            this.xEbUserDestCustomsBroker = new EbUser(userDestCBEbUserNum);
            this.xEbUserDestCustomsBroker.setNom(userDestCBNom);
            this.xEbUserDestCustomsBroker.setEbEtablissement(new EbEtablissement(userDestCBEtabNum, userDestCBEtabNom));
        }

        this.getDateRemaing();
        this.listFlag = listFlag;

        /* Type de flux, schema Psl, et incoterm */
        this.xEbTypeFluxNum = xEbTypeFluxNum;
        this.xEbTypeFluxCode = xEbTypeFluxCode;
        this.xEbTypeFluxDesignation = xEbTypeFluxDesignation;
        this.xEbIncotermNum = xEbIncotermNum;
        this.xEbSchemaPsl = schemaPsl;
		this.xEcIncotermLibelle = xEcIncotermLibelle;
        /* Fin Type de flux, schema Psl, et incoterm */

        this.xEbEntrepotUnloading = entrepot;

        this.xEbDemandeGroup = xEbDemandeGroup;

        this.askForTransportResponsibility = askForTransportResponsibility;
        this.provideTransport = provideTransport;

        this.tdcAcknowledge = tdcAcknowledge;
        this.tdcAcknowledgeDate = tdcAcknowledgeDate;
        this.eligibleForConsolidation = eligibleForConsolidation;
        this.customerCreationDate = customerCreationDate;
        this.finalChoiceToBeCalculated = finalChoiceToBeCalculated;
        this.isValorized = isValorized;

        if (customFields != null) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                this.listCustomsFields = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    @QueryProjection
    public EbDemande(
        Integer ebDemandeNum,
        String refTransport,
        String customerReference,
        Integer xEcModeTransport,
        EbParty ebPartyOrigin,
        String libelleOriginCountry,
        EbParty ebPartyDest,
        String libelleDestCountry,
        Integer xEcStatut,
        Double totalVolume,
        Double totalTaxableWeight,
        Double totalWeight,
        Double totalNbrParcel,
        boolean isGrouping,
        Integer currencynum

    ) {
        this.ebDemandeNum = ebDemandeNum;
        this.refTransport = refTransport;
        this.customerReference = customerReference;
        this.ebPartyOrigin = ebPartyOrigin != null ? new EbParty(ebPartyOrigin) : null;
        this.ebPartyDest = ebPartyDest != null ? new EbParty(ebPartyDest) : null;
        this.xEcModeTransport = xEcModeTransport;
        this.xEcStatut = xEcStatut;
        this.totalVolume = totalVolume;
        this.totalTaxableWeight = totalTaxableWeight;
        this.totalWeight = totalWeight;
        this.totalNbrParcel = totalNbrParcel;
        this.libelleOriginCountry = libelleOriginCountry;
        this.libelleDestCountry = libelleDestCountry;
        this.isGrouping = isGrouping;
        this.xecCurrencyInvoice = new EcCurrency(currencynum);
    }

    // Projection details groupe.
    @QueryProjection
    public EbDemande(
        Integer ebDemandeNum,
        String refTransport,
        Integer costCenterNum,
        String costCenterLibelle,
        String customerReference,

        Double totalWeight,
        Integer xEcModeTransport,
        Date dateCreation,
        Integer xEcStatut,
        Integer xEcTypeDemande,
        Integer xEbTypeRequest,

        String libelleDestCountry,
        String libelleOriginCountry,
        Date dateOfGoodsAvailability,
        Date dateOfArrival,

        Date datePickup,
        EbParty ebPartyOrigin,
        EcCountry ecCountryOrigin,
        EbParty ebPartyDest,
        EcCountry ecCountryDest,
        EbParty ebPartyNotif,
        EcCountry ecCountryNotif,
        Integer xEbTypeTransport,

        Boolean flagRecommendation,
        String commentChargeur,
        Double totalVolume,

        Double totalTaxableWeight,
        Boolean insurance,
        Double insuranceValue,
        String city,

        List<EbCategorie> listCategories,
        Double totalNbrParcel,
        Integer xEcBrokerOrigin,
        String xEcBrokerOriginEmail,
        Integer xEcBrokerOriginEtablissementNum,
        Integer xEcBrokerDest,
        String xEcBrokerDestEmail,
        Integer xEcBrokerDestEtablissementNum,
        Integer xEcTypeCustomBroker,
        Integer xEbTransporteur,

        Integer xEbTransporteurEtablissementNum,
        Boolean flagDocumentCustoms,
        Boolean flagDocumentTransporteur,

        Boolean flagTransportInformation,
        Boolean flagCustomsInformation,
        Boolean flagRequestCustomsBroker,

        String customFields,
        String configPsl,
        String numAwbBol,
        String carrierUniqRefNum,
        String flightVessel,
        Date datePickupTM,
        Date etd,

        Date finalDelivery,
        Date eta,
        String customsOffice,
        String mawb,
        Boolean flagEbtrackTrace,

        Boolean flagEbInvoice,
        EbUser chargeur,

        Integer chargeurEtabNum,
        String chargeurEtabNom,
        Integer chargeurCompNum,
        String chargeurCompNom,
        String chargeurCompCode,
        Integer currencynum,

        String currencyCode,
        String ebDemandeHistory,
        Integer moduleCreator,
        String listLabels,

        Integer xEcCancelled,
        Integer xEcNature,
        Integer pointIntermediateNum,
        String reference,
        String company,

        Integer ebDemandeMasterObjectNum,
        String ebDemandeMasterObjectRefTransport,
        Integer ebDemandeInitialNum,
        String ebDemandeInitialRefTransport,

        Integer userOriginEbUserNum,
        String userOriginNom,
        Integer userOriginEtabNum,
        String userOriginEtabNom,

        Integer userDestEbUserNum,
        String userDestNom,
        Integer userDestEtabNum,
        String userDestEtabNom,

        Integer userOriginCBEbUserNum,
        String userOriginCBNom,
        Integer userOriginCBEtabNum,
        String userOriginCBEtabNom,

        Integer userDestCBEbUserNum,
        String userDestCBNom,
        Integer userDestCBEtabNum,
        String userDestCBEtabNom,
        String listFlag,

        EbTtSchemaPsl schemaPsl,
        Boolean dg,
        Boolean eligibleForConsolidation,
        EbPlZone zoneOrigin,
        EbPlZone zoneDest,
        Integer xEbTypeFluxNum,
        String xEbTypeFluxCode,
        String xEbTypeFluxDesignation,
        String typeRequestLibelle,
        Integer xEbIncotermNum,
        String unitsRef,
        String unitsPackingList

    ) {
        this.ebDemandeNum = ebDemandeNum;
        this.refTransport = refTransport;
        this.customerReference = customerReference;
        this.libelleOriginCountry = libelleOriginCountry;
        this.libelleDestCountry = libelleDestCountry;
        this.dateOfGoodsAvailability = dateOfGoodsAvailability;
        this.dateOfArrival = dateOfArrival;
        this.datePickup = datePickup;
        this.totalWeight = totalWeight;
        this.xEcModeTransport = xEcModeTransport;
        this.dateCreation = dateCreation;
        this.xEcTypeDemande = xEcTypeDemande;
        this.xEbTypeRequest = xEbTypeRequest;
        this.typeRequestLibelle = typeRequestLibelle;
        this.xEcStatut = xEcStatut;

        if (ebPartyOrigin != null) {
            ebPartyOrigin.setxEcCountry(ecCountryOrigin);
            this.ebPartyOrigin = new EbParty(ebPartyOrigin);
        }

        if (ebPartyDest != null) {
            ebPartyDest.setxEcCountry(ecCountryDest);
            this.ebPartyDest = new EbParty(ebPartyDest);
        }

        if (ebPartyNotif != null) {
            ebPartyNotif.setxEcCountry(ecCountryNotif);
            this.ebPartyNotif = new EbParty(ebPartyNotif);
        }

        this.xEbTypeTransport = xEbTypeTransport;
        this.flagRecommendation = flagRecommendation;
        this.commentChargeur = commentChargeur;
        this.totalVolume = totalVolume;
        this.totalTaxableWeight = totalTaxableWeight;
        this.insurance = insurance;
        this.insuranceValue = insuranceValue;
        this.city = city;

        if (listCategories != null) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String jsonCateg = objectMapper.writeValueAsString(listCategories);
                this.listCategories = objectMapper.readValue(jsonCateg, new TypeReference<ArrayList<EbCategorie>>() {
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        this.totalNbrParcel = totalNbrParcel;
        this.xEbUserOriginCustomsBroker = new EbUser(xEcBrokerOrigin);
        this.xEbUserOriginCustomsBroker.setEmail(xEcBrokerOriginEmail);
        this.xEbUserOriginCustomsBroker.setEbEtablissement(new EbEtablissement(xEcBrokerOriginEtablissementNum));
        this.xEbUserDestCustomsBroker = new EbUser(xEcBrokerDest);
        this.xEbUserDestCustomsBroker.setEmail(xEcBrokerDestEmail);
        this.xEbUserDestCustomsBroker.setEbEtablissement(new EbEtablissement(xEcBrokerDestEtablissementNum));
        this.xEcTypeCustomBroker = xEcTypeCustomBroker;
        this.xEbTransporteur = xEbTransporteur;
        this.xEbTransporteurEtablissementNum = xEbTransporteurEtablissementNum;
        this.flagDocumentCustoms = flagDocumentCustoms;
        this.flagDocumentTransporteur = flagDocumentTransporteur;
        this.flagTransportInformation = flagTransportInformation;
        this.flagCustomsInformation = flagCustomsInformation;
        this.flagRequestCustomsBroker = flagRequestCustomsBroker;
        this.customFields = customFields;
        this.configPsl = configPsl;
        this.numAwbBol = numAwbBol;
        this.carrierUniqRefNum = carrierUniqRefNum;
        this.flightVessel = flightVessel;
        this.datePickupTM = datePickupTM;
        this.etd = etd;
        this.finalDelivery = finalDelivery;
        this.eta = eta;
        this.customsOffice = customsOffice;
        this.mawb = mawb;
        this.flagEbtrackTrace = flagEbtrackTrace;
        this.flagEbInvoice = flagEbInvoice;
        this.user = new EbUser(chargeur);
        this.user.setEbEtablissement(new EbEtablissement(chargeurEtabNum, chargeurEtabNom));
        this.user.setEbCompagnie(new EbCompagnie(chargeurCompNum, chargeurCompNom, chargeurCompCode));
        this.xEbEtablissement = new EbEtablissement(chargeurEtabNum, chargeurEtabNom);
        this.xEbEtablissement.setEbCompagnie(new EbCompagnie(chargeurCompNum, chargeurCompNom, chargeurCompCode));
        this.xEbCompagnie = new EbCompagnie(chargeurCompNum, chargeurCompNom, chargeurCompCode);
        this.xecCurrencyInvoice = new EcCurrency(currencynum);
        this.xecCurrencyInvoice.setCode(currencyCode);
        this.xecCurrencyInsurance = new EcCurrency(currencynum);
        this.xecCurrencyInsurance.setCode(currencyCode);
        this.ebDemandeHistory = ebDemandeHistory;
        this.moduleCreator = moduleCreator;
        this.listLabels = listLabels;
        this.xEcCancelled = xEcCancelled;
        this.xEcNature = xEcNature;

        if (pointIntermediateNum != null) {
            this.pointIntermediate = new EbParty();
            this.pointIntermediate.setEbPartyNum(pointIntermediateNum);
            this.pointIntermediate.setReference(reference);
            this.pointIntermediate.setCompany(company);
        }

        if (ebDemandeMasterObjectNum != null) {
            EbDemande mutiSegment = new EbDemande();
            mutiSegment.setEbDemandeNum(ebDemandeMasterObjectNum);
            mutiSegment.setRefTransport(ebDemandeMasterObjectRefTransport);
            this.ebDemandeMasterObject = mutiSegment;
        }

        if (ebDemandeInitialNum != null) {
            EbDemande demandeInitial = new EbDemande();
            demandeInitial.setEbDemandeNum(ebDemandeInitialNum);
            demandeInitial.setRefTransport(ebDemandeInitialRefTransport);
            this.ebDemandeInitial = demandeInitial;
        }

        if (costCenterLibelle != null) {
            this.xEbCostCenter = new EbCostCenter(costCenterNum, costCenterLibelle);
        }

        if (userOriginEbUserNum != null) {
            this.xEbUserOrigin = new EbUser(userOriginEbUserNum);
            this.xEbUserOrigin.setNom(userOriginNom);
            this.xEbUserOrigin.setEbEtablissement(new EbEtablissement(userOriginEtabNum, userOriginEtabNom));
        }

        if (userDestEbUserNum != null) {
            this.xEbUserDest = new EbUser(userDestEbUserNum);
            this.xEbUserDest.setNom(userDestNom);
            this.xEbUserDest.setEbEtablissement(new EbEtablissement(userDestEtabNum, userDestEtabNom));
        }

        if (userOriginCBEbUserNum != null) {
            this.xEbUserOriginCustomsBroker = new EbUser(userOriginCBEbUserNum);
            this.xEbUserOriginCustomsBroker.setNom(userOriginCBNom);
            this.xEbUserOriginCustomsBroker
                .setEbEtablissement(new EbEtablissement(userOriginCBEtabNum, userOriginCBEtabNom));
        }

        if (userDestCBEbUserNum != null) {
            this.xEbUserDestCustomsBroker = new EbUser(userDestCBEbUserNum);
            this.xEbUserDestCustomsBroker.setNom(userDestCBNom);
            this.xEbUserDestCustomsBroker.setEbEtablissement(new EbEtablissement(userDestCBEtabNum, userDestCBEtabNom));
        }

        this.getDateRemaing();
        this.listFlag = listFlag;

        this.xEbSchemaPsl = schemaPsl;
        if (this.xEbSchemaPsl != null) this.xEcIncotermLibelle = xEbSchemaPsl.getxEcIncotermLibelle();

        if (zoneOrigin != null && zoneOrigin.getEbZoneNum() != null && this.ebPartyOrigin != null) {
            this.ebPartyOrigin.setZoneNum(zoneOrigin.getEbZoneNum());
            this.ebPartyOrigin.setZoneDesignation(zoneOrigin.getDesignation());
        }

        if (zoneDest != null && zoneDest.getEbZoneNum() != null && this.ebPartyDest != null) {
            this.ebPartyDest.setZoneNum(zoneDest.getEbZoneNum());
            this.ebPartyDest.setZoneDesignation(zoneDest.getDesignation());
        }

        this.dg = dg;
        this.eligibleForConsolidation = eligibleForConsolidation;
        this.xEbTypeFluxNum = xEbTypeFluxNum;
        this.xEbTypeFluxCode = xEbTypeFluxCode;
        this.xEbTypeFluxDesignation = xEbTypeFluxDesignation;
        this.xEbIncotermNum = xEbIncotermNum;
        this.unitsReference = unitsRef;
        this.unitsPackingList = unitsPackingList;

        if (customFields != null) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                this.listCustomsFields = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public EbDemande(EbDemande d) {
        this.ebDemandeNum = d.getEbDemandeNum();
        this.refTransport = d.getRefTransport();
        this.customerReference = d.getCustomerReference();
        this.libelleOriginCountry = d.getLibelleOriginCity();
        this.libelleDestCountry = d.getLibelleDestCity();
        this.dateOfGoodsAvailability = d.getDateOfGoodsAvailability();
        this.datePickup = d.getDatePickup();
        this.totalWeight = d.getTotalWeight();
        this.xEcModeTransport = d.getxEcModeTransport();
        this.dateCreation = d.getDateCreation();
        this.xEcTypeDemande = d.getxEcTypeDemande();
        this.xEbTypeRequest = d.getxEbTypeRequest();
        this.xEcStatut = d.getxEcStatut();
        this.ebPartyOrigin = d.getEbPartyOrigin() != null ? new EbParty(d.getEbPartyOrigin()) : null;
        this.ebPartyDest = d.getEbPartyDest() != null ? new EbParty(d.getEbPartyDest()) : null;
        this.ebPartyNotif = d.getEbPartyNotif() != null ? new EbParty(d.getEbPartyNotif()) : null;
        this.xEbTypeTransport = d.getxEbTypeTransport();
        this.flagRecommendation = d.getFlagRecommendation();
        this.commentChargeur = d.getCommentChargeur();
        this.totalVolume = d.getTotalVolume();
        this.totalTaxableWeight = d.getTotalTaxableWeight();
        this.insurance = d.getInsurance();
        this.insuranceValue = d.getInsuranceValue();
        this.city = d.getCity();
        this.listCategories = d.getListCategories();
        this.totalNbrParcel = d.getTotalNbrParcel();
        this.customFields = d.getCustomFields();
        this.numAwbBol = d.getNumAwbBol();
        this.xEcIncotermLibelle = d.getxEcIncotermLibelle();
        this.xEbSchemaPsl = d.getxEbSchemaPsl();
        this.xEcIncotermLibelle = d.getxEcIncotermLibelle();
        this.flightVessel = d.getFlightVessel();
        this.partNumber = d.getPartNumber();
        this.orderNumber = d.getOrderNumber();
        this.serialNumber = d.getSerialNumber();
        this.user = new EbUser(d.getUser());
        this.user.setEbEtablissement(d.getUser().getEbEtablissement());
        this.user.setEbCompagnie(d.getxEbCompagnie());

        if (d.getXecCurrencyInvoice() != null && d.getXecCurrencyInvoice().getEcCurrencyNum() != null) {
            this.setXecCurrencyInvoice(new EcCurrency());
            this.getXecCurrencyInvoice().setCode(d.getXecCurrencyInvoice().getCode());
            this.getXecCurrencyInvoice().setCodeLibelle(d.getXecCurrencyInvoice().getCodeLibelle());
            this.getXecCurrencyInvoice().setLibelle(d.getXecCurrencyInvoice().getLibelle());
            this.getXecCurrencyInvoice().setEcCurrencyNum(d.getXecCurrencyInvoice().getEcCurrencyNum());
            this.getXecCurrencyInvoice().setSymbol(d.getXecCurrencyInvoice().getSymbol());
        }

        if (d.getXecCurrencyInsurance() != null && d.getXecCurrencyInsurance().getEcCurrencyNum() != null) {
            this.setXecCurrencyInsurance(new EcCurrency());
            this.getXecCurrencyInsurance().setCode(d.getXecCurrencyInsurance().getCode());
            this.getXecCurrencyInsurance().setCodeLibelle(d.getXecCurrencyInsurance().getCodeLibelle());
            this.getXecCurrencyInsurance().setLibelle(d.getXecCurrencyInsurance().getLibelle());
            this.getXecCurrencyInsurance().setEcCurrencyNum(d.getXecCurrencyInsurance().getEcCurrencyNum());
            this.getXecCurrencyInsurance().setSymbol(d.getXecCurrencyInsurance().getSymbol());
        }

        if (d.getExEbDemandeTransporteurs() != null) {
            this.exEbDemandeTransporteurs = new ArrayList<ExEbDemandeTransporteur>();
            for (ExEbDemandeTransporteur tr: d
                .getExEbDemandeTransporteurs()) this.exEbDemandeTransporteurs.add(new ExEbDemandeTransporteur(tr));
        }

        if (d.getListMarchandises() != null) {
            this.listMarchandises = new ArrayList<EbMarchandise>();
            for (EbMarchandise mr: d.getListMarchandises()) this.listMarchandises.add(new EbMarchandise(mr));
        }

    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;

        if (this.customFields != null && !this.customFields.isEmpty()) {
            List<CustomFields> listCustomFieldsDeserialized = null;
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                listCustomFieldsDeserialized = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

            this.listCustomFieldsFlat = "";
            this.listCustomFieldsValueFlat = "";

            for (CustomFields field: listCustomFieldsDeserialized) {
                if (field.getValue() == null ||
                    field.getValue().isEmpty() || field.getName() == null || field.getName().isEmpty()) continue;

                if (!this.listCustomFieldsFlat.isEmpty()) this.listCustomFieldsFlat += ";";

                if (!this.listCustomFieldsValueFlat.isEmpty()) this.listCustomFieldsValueFlat += ";";

                this.listCustomFieldsFlat += "\"" + field.getName() + "\":" + "\"" + field.getValue() + "\"";

                this.listCustomFieldsValueFlat += "\"" + field.getLabel() + "\":" + "\"" + field.getValue() + "\"";
            }

            if (this.listCustomFieldsFlat.isEmpty()) this.listCustomFieldsFlat = null;

            if (this.listCustomFieldsValueFlat.isEmpty()) this.listCustomFieldsValueFlat = null;
        }

    }

    // Groupage QR
    @QueryProjection
    public EbDemande(
        Integer ebDemandeNum,
        String refTransport,
        String zoneOriginDesignation,
        String zoneDestDesignation,
        String cityOrigin,
        String cityDest,
        Integer xEcTypeDemande,
        Integer xEcTypeCustomBroker,
        String listFlag,
        List<EbCategorie> listCategories,
        Integer xEcModeTransport) {
        this.ebDemandeNum = ebDemandeNum;
        this.refTransport = refTransport;

        this.ebPartyOrigin = new EbParty();
        this.ebPartyOrigin.setZoneDesignation(zoneOriginDesignation);
        this.ebPartyOrigin.setCity(cityOrigin);

        this.ebPartyDest = new EbParty();
        this.ebPartyDest.setZoneDesignation(zoneDestDesignation);
        this.ebPartyDest.setCity(cityDest);

        this.xEcTypeDemande = xEcTypeDemande;
        this.xEcTypeCustomBroker = xEcTypeCustomBroker;
        this.listFlag = listFlag;
        this.listCategories = listCategories;

        this.xEcModeTransport = xEcModeTransport;
    }

    // Control rules
    @QueryProjection
    public EbDemande(
        Integer ebDemandeNum,
        String customFields,
        List<EbCategorie> listCategories,
        List<EbTypeDocuments> listTypeDocuments,
        Integer xEbTypeFluxNum,
        String xEbTypeFluxDesignation,
        String typeRequestLibelle,
        Integer xEbTypeRequest,
        Integer xEcModeTransport,
        Integer xEcStatut,
        Boolean flagRecommendation,
        Integer xEcNature,
        Date dateOfGoodsAvailability,
        EbTtSchemaPsl ebSchemaPsl,
        String listCategoriesStr,
        String scenario,
        Integer ebPartyOriginNum,
        Integer ebPartyDestNum,
        Integer ebCompagnieNum,
        Integer ebEtablissementNum,
        Integer ecCurrencyNum,
        Integer ebUserNum,
        Integer ebUserCtNum,
        String refTransport) {
        this.ebDemandeNum = ebDemandeNum;
        this.refTransport = refTransport;
        this.customFields = customFields;
        this.xEbTypeFluxNum = xEbTypeFluxNum;
        this.xEbTypeFluxDesignation = xEbTypeFluxDesignation;
        this.typeRequestLibelle = typeRequestLibelle;
        this.xEbTypeRequest = xEbTypeRequest;
        this.xEcModeTransport = xEcModeTransport;
        this.xEcStatut = xEcStatut;
        this.flagRecommendation = flagRecommendation;
        this.xEcNature = xEcNature;
        this.dateOfGoodsAvailability = dateOfGoodsAvailability;
        this.listCategoriesStr = listCategoriesStr;
        if (ecCurrencyNum != null) this.xecCurrencyInvoice = new EcCurrency(ecCurrencyNum);
        this.xEbSchemaPsl = ebSchemaPsl;
        this.scenario = scenario;

        if (listCategories != null) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String jsonCateg = objectMapper.writeValueAsString(listCategories);
                this.listCategories = objectMapper.readValue(jsonCateg, new TypeReference<ArrayList<EbCategorie>>() {
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (listTypeDocuments != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            listTypeDocuments = Arrays.asList(objectMapper.convertValue(listTypeDocuments, EbTypeDocuments[].class));
            this.listTypeDocuments = listTypeDocuments;
        }

        if (customFields != null && !customFields.isEmpty()) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                this.listCustomsFields = objectMapper
                    .readValue(customFields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (ebSchemaPsl != null) {
        }

        if (ebPartyOriginNum != null) {
            this.ebPartyOrigin = new EbParty(ebPartyOriginNum);
        }

        if (ebPartyDestNum != null) {
            this.ebPartyDest = new EbParty(ebPartyDestNum);
        }

        if (ebCompagnieNum != null) {
            this.xEbCompagnie = new EbCompagnie(ebCompagnieNum);
        }

        if (ebEtablissementNum != null) {
            this.xEbEtablissement = new EbEtablissement(ebEtablissementNum);
        }

        if (ebUserNum != null) {
            this.user = new EbUser(ebUserNum);
        }

        if (ebUserCtNum != null) {
            this.xEbUserCt = new EbUser(ebUserCtNum);
        }

    }

    public EbUser getExEbDemandeTransporteurFinal() {
        return exEbDemandeTransporteurFinal;
    }

    public void setExEbDemandeTransporteurFinal(EbUser exEbDemandeTransporteurFinal) {
        this.exEbDemandeTransporteurFinal = exEbDemandeTransporteurFinal;
    }

    public List<EbLivraison> getLivraisons() {
        return livraisons;
    }

    public void setLivraisons(List<EbLivraison> livraisons) {
        this.livraisons = livraisons;
    }

    public List<Integer> getEbDemandeFichierJointNum() {
        return ebDemandeFichierJointNum;
    }

    public void setEbDemandeFichierJointNum(List<Integer> ebDemandeFichierJointNum) {
        this.ebDemandeFichierJointNum = ebDemandeFichierJointNum;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Boolean getInsurance() {
        return insurance != null ? insurance : false;
    }

    public Boolean isFlagRecommendation() {
        return flagRecommendation;
    }

    public void setFlagRecommendation(Boolean flagRecommendation) {
        this.flagRecommendation = flagRecommendation;
    }

    public Boolean getDg() {
        return dg;
    }

    public Boolean getEquipement() {
        return equipement;
    }

    public Boolean getSensitive() {
        return sensitive;
    }

    public Boolean getDryIce() {
        return dryIce;
    }

    public List<ExEbDemandeTransporteur> getExEbDemandeTransporteurs() {
        return exEbDemandeTransporteurs;
    }

    public void setExEbDemandeTransporteurs(List<ExEbDemandeTransporteur> exEbDemandeTransporteurs) {
        this.exEbDemandeTransporteurs = exEbDemandeTransporteurs;
    }

    public Boolean getPreAlertInfoAboutTransportSent() {
        return preAlertInfoAboutTransportSent;
    }

    public void setLibelleOriginCountry(String libelleOriginCountry) {
        this.libelleOriginCountry = libelleOriginCountry;
    }

    public void setLibelleDestCountry(String libelleDestCountry) {
        this.libelleDestCountry = libelleDestCountry;
    }

    public Integer getEbDemandeNum() {
        return ebDemandeNum;
    }

    public void setEbDemandeNum(Integer ebDemandeNum) {
        this.ebDemandeNum = ebDemandeNum;
    }

    public Integer getNbReminderSent() {
        return nbReminderSent;
    }

    public void setNbReminderSent(Integer nbReminderSent) {
        this.nbReminderSent = nbReminderSent;
    }

    public EbParty getEbPartyOrigin() {
        return ebPartyOrigin;
    }

    public void setEbPartyOrigin(EbParty ebPartyOrigin) {
        this.ebPartyOrigin = ebPartyOrigin;
    }

    public EbParty getEbPartyNotif() {
        return ebPartyNotif;
    }

    public void setEbPartyNotif(EbParty ebPartyNotif) {
        this.ebPartyNotif = ebPartyNotif;
    }

    public EbParty getEbPartyDest() {
        return ebPartyDest;
    }

    public void setEbPartyDest(EbParty ebPartyDest) {
        this.ebPartyDest = ebPartyDest;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
        this.getDateRemaing();
    }

    public Date getDateReadyForPickup() {
        return dateReadyForPickup;
    }

    public void setDateReadyForPickup(Date dateReadyForPickup) {
        this.dateReadyForPickup = dateReadyForPickup;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Date getDatePickup() {
        return datePickup;
    }

    public void setDatePickup(Date datePickup) {
        this.datePickup = datePickup;
    }

    public Date getDateEnvoiQuotation() {
        return dateEnvoiQuotation;
    }

    public void setDateEnvoiQuotation(Date dateEnvoiQuotation) {
        this.dateEnvoiQuotation = dateEnvoiQuotation;
    }

    public Boolean isInsurance() {
        return insurance;
    }

    public void setInsurance(Boolean insurance) {
        this.insurance = insurance;
    }

    public Double getInsuranceValue() {
        return insuranceValue;
    }

    public void setInsuranceValue(Double insuranceValue) {
        this.insuranceValue = insuranceValue;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCommentMytower() {
        return commentMytower;
    }

    public void setCommentMytower(String commentMytower) {
        this.commentMytower = commentMytower;
    }

    public String getCommentChargeur() {
        return commentChargeur;
    }

    public void setCommentChargeur(String commentChargeur) {
        this.commentChargeur = commentChargeur;
    }

    public Date getDateOfGoodsAvailability() {
        return dateOfGoodsAvailability;
    }

    public void setDateOfGoodsAvailability(Date dateOfGoodsAvailability) {
        this.dateOfGoodsAvailability = dateOfGoodsAvailability;
    }

    public Date getDateOfArrival() {
        return dateOfArrival;
    }

    public void setDateOfArrival(Date dateOfArrival) {
        this.dateOfArrival = dateOfArrival;
    }

    public Integer getxEbTypeTransport() {
        return xEbTypeTransport;
    }

    public void setxEbTypeTransport(Integer xEbTypeTransport) {
        this.xEbTypeTransport = xEbTypeTransport;
    }

    public Integer getxEcProductFamily() {
        return xEcProductFamily;
    }

    public void setxEcProductFamily(Integer xEcProductFamily) {
        this.xEcProductFamily = xEcProductFamily;
    }

    public Integer getNbrDoc() {
        return nbrDoc;
    }

    public void setNbrDoc(Integer nbrDoc) {
        this.nbrDoc = nbrDoc;
    }

    public Long getCurentTimeRemainingInMilliSecond() {
        this.getDateRemaing();
        return curentTimeRemainingInMilliSecond;
    }

    public void setCurentTimeRemainingInMilliSecond(Long curentTimeRemainingInMilliSecond) {
        this.curentTimeRemainingInMilliSecond = curentTimeRemainingInMilliSecond;
    }

    public Integer getTimeRemaining() {
        return timeRemaining;
    }

    public void setTimeRemaining(Integer timeRemaining) {
        this.timeRemaining = timeRemaining;
    }

    public Double getTimeRemainingFf() {
        return timeRemainingFf;
    }

    public void setTimeRemainingFf(Double timeRemainingFf) {
        this.timeRemainingFf = timeRemainingFf;
    }

    public Boolean isDg() {
        return dg;
    }

    public void setDg(Boolean dg) {
        this.dg = dg;
    }

    public Boolean isEquipement() {
        return equipement;
    }

    public void setEquipement(Boolean equipement) {
        this.equipement = equipement;
    }

    public String getStatsMarchandises() {
        return statsMarchandises;
    }

    public void setStatsMarchandises(String statsMarchandises) {
        this.statsMarchandises = statsMarchandises;
    }

    public Boolean isSensitive() {
        return sensitive;
    }

    public void setSensitive(Boolean sensitive) {
        this.sensitive = sensitive;
    }

    public Boolean isDryIce() {
        return dryIce;
    }

    public void setDryIce(Boolean dryIce) {
        this.dryIce = dryIce;
    }

    public Double getTotalNbrParcel() {
        return totalNbrParcel;
    }

    public void setTotalNbrParcel(Double totalNbrParcel) {
        this.totalNbrParcel = totalNbrParcel;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(Double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public Integer getxEcStatut() {
        return xEcStatut;
    }

    public void setxEcStatut(Integer xEcStatut) {
        this.xEcStatut = xEcStatut;
    }

    public Integer getxEcTypeDemande() {
        return xEcTypeDemande;
    }

    public void setxEcTypeDemande(Integer xEcTypeDemande) {
        this.xEcTypeDemande = xEcTypeDemande;
    }

    public Integer getxEcModeTransport() {
        return xEcModeTransport;
    }

    public void setxEcModeTransport(Integer xEcModeTransport) {
        this.xEcModeTransport = xEcModeTransport;
    }

    public String getLibelleOriginCountry() {
        return libelleOriginCountry;
    }

    public String getLibelleDestCountry() {
        return libelleDestCountry;
    }

    public EbUser getUser() {
        return user;
    }

    public void setUser(EbUser user) {
        this.user = user;
    }

    public String getRefTransport() {
        return refTransport;
    }

    public void setRefTransport(String refTransport) {
        this.refTransport = refTransport;
    }

    public String getTemperatureTransport() {
        return temperatureTransport;
    }

    public void setTemperatureTransport(String temperatureTransport) {
        this.temperatureTransport = temperatureTransport;
    }

    public String getTemperatureStorage() {
        return temperatureStorage;
    }

    public void setTemperatureStorage(String temperatureStorage) {
        this.temperatureStorage = temperatureStorage;
    }

    public String getCodeAnnulation() {
        return codeAnnulation;
    }

    public void setCodeAnnulation(String codeAnnulation) {
        this.codeAnnulation = codeAnnulation;
    }

    public String getAboutTrAwbBol() {
        return aboutTrAwbBol;
    }

    public void setAboutTrAwbBol(String aboutTrAwbBol) {
        this.aboutTrAwbBol = aboutTrAwbBol;
    }

    public String getAboutTrMawb() {
        return aboutTrMawb;
    }

    public void setAboutTrMawb(String aboutTrMawb) {
        this.aboutTrMawb = aboutTrMawb;
    }

    public String getAboutTrFlightVessel() {
        return aboutTrFlightVessel;
    }

    public void setAboutTrFlightVessel(String aboutTrFlightVessel) {
        this.aboutTrFlightVessel = aboutTrFlightVessel;
    }

    public String getAboutTrCustomOffice() {
        return aboutTrCustomOffice;
    }

    public void setAboutTrCustomOffice(String aboutTrCustomOffice) {
        this.aboutTrCustomOffice = aboutTrCustomOffice;
    }

    public Date getAboutTrEta() {
        return aboutTrEta;
    }

    public void setAboutTrEta(Date aboutTrEta) {
        this.aboutTrEta = aboutTrEta;
    }

    public Date getAboutTrEtd() {
        return aboutTrEtd;
    }

    public void setAboutTrEtd(Date aboutTrEtd) {
        this.aboutTrEtd = aboutTrEtd;
    }

    public Date getAboutTrPickUp() {
        return aboutTrPickUp;
    }

    public void setAboutTrPickUp(Date aboutTrPickUp) {
        this.aboutTrPickUp = aboutTrPickUp;
    }

    public Date getAboutTrFinalDelivery() {
        return aboutTrFinalDelivery;
    }

    public void setAboutTrFinalDelivery(Date aboutTrFinalDelivery) {
        this.aboutTrFinalDelivery = aboutTrFinalDelivery;
    }

    public Integer getxEcCancelled() {
        return xEcCancelled;
    }

    public void setxEcCancelled(Integer xEcCancelled) {
        this.xEcCancelled = xEcCancelled;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public Boolean isPreAlertInfoAboutTransportSent() {
        return preAlertInfoAboutTransportSent;
    }

    public void setPreAlertInfoAboutTransportSent(Boolean preAlertInfoAboutTransportSent) {
        this.preAlertInfoAboutTransportSent = preAlertInfoAboutTransportSent;
    }

    public Date getDateDelivery() {
        return dateDelivery;
    }

    public void setDateDelivery(Date dateDelivery) {
        this.dateDelivery = dateDelivery;
    }

    public Integer getxEcTypeCustomBroker() {
        return xEcTypeCustomBroker;
    }

    public void setxEcTypeCustomBroker(Integer xEcTypeCustomBroker) {
        this.xEcTypeCustomBroker = xEcTypeCustomBroker;
    }

    public Double getCoutPrestationDouane() {
        return coutPrestationDouane;
    }

    public void setCoutPrestationDouane(Double coutPrestationDouane) {
        this.coutPrestationDouane = coutPrestationDouane;
    }

    public EbUser getxEcBroker() {
        return xEcBroker;
    }

    public void setxEcBroker(EbUser xEcBroker) {
        this.xEcBroker = xEcBroker;
    }

    public EbCostCenter getxEbCostCenter() {
        return xEbCostCenter;
    }

    public void setxEbCostCenter(EbCostCenter xEbCostCenter) {
        this.xEbCostCenter = xEbCostCenter;
    }

    private void getDateRemaing() {

        if (this.xEcTypeDemande != null && this.dateCreation != null) {
            // le time remaining est toujours stockÃƒÂ©e en minute
            // une fois qu'on le recupere on l'ajoute ÃƒÂ  la date de
            // crÃƒÂ©ation de la demande afin d'initialiser le chrono ÃƒÂ  cette
            // date

            // Date exacte de la fin du timeremaing
            Date dateTimeRemaining;
            if (Enumeration.TypeDemande.URGENT.getCode()
                == this.xEcTypeDemande) timeRemaining = Enumeration.TypeDemande.URGENT.getTimeRemaining();
            else timeRemaining = Enumeration.TypeDemande.STANDARD.getTimeRemaining();

            // dateCreation, xEcTypeDemandeNum, timeRemaining
            Calendar c = Calendar.getInstance();
            c.setTime(this.dateCreation);
            c.add(Calendar.MINUTE, this.timeRemaining.intValue());
            dateTimeRemaining = new Date(c.getTimeInMillis());

            Date serverSideNow = new Date(System.currentTimeMillis());

            this.curentTimeRemainingInMilliSecond = dateTimeRemaining.getTime() - serverSideNow.getTime();
        }

    }

    public Boolean getFlagRecommendation() {
        return flagRecommendation;
    }

    public String getNomTransporteur() {
        return nomTransporteur;
    }

    public void setNomTransporteur(String nomTransporteur) {
        this.nomTransporteur = nomTransporteur;
    }

    public Integer getxEbTransporteur() {
        return xEbTransporteur;
    }

    public void setxEbTransporteur(Integer xEbTransporteur) {
        this.xEbTransporteur = xEbTransporteur;
    }

    public List<EbUser> getListTransporteurs() {
        return listTransporteurs;
    }

    public void setListTransporteurs(List<EbUser> listTransporteurs) {
        this.listTransporteurs = listTransporteurs;
    }

    public Double getTotalTaxableWeight() {
        return totalTaxableWeight;
    }

    public void setTotalTaxableWeight(Double totalTaxableWeight) {
        this.totalTaxableWeight = totalTaxableWeight;
    }

    public List<EbMarchandise> getListMarchandises() {
        return listMarchandises;
    }

    public void setListMarchandises(List<EbMarchandise> listMarchandises) {
        this.listMarchandises = listMarchandises;
    }

    public String getListLabels() {
        return listLabels;
    }

    public void setListLabels(String listLabels) {
        this.listLabels = listLabels;
    }

    public String getListCategoriesStr() {
        return listCategoriesStr;
    }

    public void setListCategoriesStr(String listCategoriesStr) {
        this.listCategoriesStr = listCategoriesStr;
    }

    public List<EbCategorie> getListCategories() {

        if (listCategories != null && listCategories.size() > 0) {

            try {
                this.listCategories = ObjectDeserializer
                    .checkJsonListObject(listCategories, new TypeToken<List<EbCategorie>>() {
                    }.getType());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return listCategories;
    }

    public List<EbCost> getListAdditionalCost() {

        if (listAdditionalCost != null && listAdditionalCost.size() > 0) {

            try {
                this.listAdditionalCost = ObjectDeserializer
                    .checkJsonListObject(listAdditionalCost, new TypeToken<List<EbCost>>() {
                    }.getType());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        return listAdditionalCost;
    }

    public void setListAdditionalCost(List<EbCost> listAdditionalCost) {
        this.listAdditionalCost = listAdditionalCost;
    }

    public void setListCategoriesCron(List<EbCategorie> listCategories) {
        ObjectMapper objectMapper = new ObjectMapper();
        // pour deserialiser la liste et eviter l'erreur lincked hashMap
        this.listCategories = Arrays.asList(objectMapper.convertValue(listCategories, EbCategorie[].class));

        if (this.listCategories != null && this.listCategories.size() > 0) {
            this.listCategoryFlat = "";
            this.listCategoriesStr = "";

            for (EbCategorie cat: this.listCategories) {
                if (!this.listCategoriesStr.isEmpty()) this.listCategoriesStr += ",";
                this.listCategoriesStr += cat.getEbCategorieNum();

                if (cat.getLabels() != null && cat.getLabels().size() > 0) {

                    if (!this.listCategoryFlat.isEmpty()) {
                        this.listCategoryFlat += ";";
                    }

                    if (this.listLabels != null && !this.listLabels.isEmpty()) {
                        this.listLabels += ",";
                    }

                    this.listLabels += cat.getLabels().stream().findFirst().get().getEbLabelNum();
                    this.listCategoryFlat += cat.getEbCategorieNum() +
                        ":" + "\"" + cat.getLabels().stream().findFirst().get().getLibelle() + "\"";
                }

            }

            if (this.listCategoryFlat.isEmpty()) this.listCategoryFlat = null;
            if (this.listCategoriesStr.isEmpty()) this.listCategoriesStr = null;
            if (this.listLabels.isEmpty()) this.listLabels = null;
        }

    }

    public void setListCategories(List<EbCategorie> listCategories) {

        if (listCategories != null) {
            this.listCategories = listCategories
                .stream().map(it -> EbCategorie.getEbCategorieLite(it)).collect(Collectors.toList());

            // pour deserialiser la liste et eviter l'erreur linked hashMap
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String jsonCateg = objectMapper.writeValueAsString(listCategories);
                this.listCategories = objectMapper.readValue(jsonCateg, new TypeReference<ArrayList<EbCategorie>>() {
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (this.listCategories != null && this.listCategories.size() > 0) {
                this.listCategoryFlat = "";
                this.listLabels = "";
                this.listCategoriesStr = "";

                for (EbCategorie cat: this.listCategories) {
                    if (!this.listCategoriesStr.isEmpty()) this.listCategoriesStr += ",";
                    this.listCategoriesStr += cat.getEbCategorieNum();

                    if (cat.getLabels() != null && cat.getLabels().size() > 0) {

                        if (!this.listCategoryFlat.isEmpty()) {
                            this.listCategoryFlat += ";";
                            this.listLabels += ",";
                        }

                        for (EbLabel lab: cat.getLabels()) {
                            // le cas de plusieurs libelles
                            if (!this.listLabels.isEmpty() &&
                                this.listLabels.charAt(this.listLabels.length() - 1) != ',') this.listLabels += ",";

                            this.listLabels += lab.getEbLabelNum();
                        }

                        this.listCategoryFlat += cat.getEbCategorieNum() +
                            ":" + "\"" + cat.getLabels().stream().findFirst().get().getLibelle() + "\"";
                    }

                }

                if (this.listCategoryFlat.isEmpty()) this.listCategoryFlat = null;
                if (this.listCategoriesStr.isEmpty()) this.listCategoriesStr = null;

                if (this.listLabels.isEmpty()) {
                    this.listLabels = null;
                }
                else {
                    // le trie des labels est naicessaire pour la partie
                    // groupage
                    List<Integer> listLabelsArray = Arrays
                        .asList(this.listLabels.split(",")).stream().map(Integer::parseInt).sorted()
                        .collect(Collectors.toList());
                    this.listLabels = StringUtils.join(listLabelsArray, ",");
                }

            }

        }
        else {
            this.listCategories = listCategories;
        }

    }

    @JsonIgnore
    public String getModeTransporteur() {
        return ModeTransport.getLibelleByCode(this.xEcModeTransport);
    }

    @JsonIgnore
    public Long getCurentTimeRemainingInHours() {
        return TimeUnit.MILLISECONDS.toHours(this.getCurentTimeRemainingInMilliSecond());
    }

    @JsonIgnore
    public String getLibelleTypeOfRequest() {
        return TypeDemande.getLibelleByCode(this.xEcTypeDemande);
    }

    public String getListEmailSendQuotation() {
        return listEmailSendQuotation;
    }

    public void setListEmailSendQuotation(String listEmailSendQuotation) {
        this.listEmailSendQuotation = listEmailSendQuotation;
    }

    public Boolean getFlagDocumentTransporteur() {
        return flagDocumentTransporteur;
    }

    public void setFlagDocumentTransporteur(Boolean flagDocumentTransporteur) {
        this.flagDocumentTransporteur = flagDocumentTransporteur;
    }

    public Boolean getFlagDocumentCustoms() {
        return flagDocumentCustoms;
    }

    public void setFlagDocumentCustoms(Boolean flagDocumentCustoms) {
        this.flagDocumentCustoms = flagDocumentCustoms;
    }

    public ExEbDemandeTransporteur get_exEbDemandeTransporteurFinal() {
        return _exEbDemandeTransporteurFinal;
    }

    public void set_exEbDemandeTransporteurFinal(ExEbDemandeTransporteur _exEbDemandeTransporteurFinal) {
        this._exEbDemandeTransporteurFinal = _exEbDemandeTransporteurFinal;
    }

    public String getPathImport() {
        return pathImport;
    }

    public void setPathImport(String pathImport) {
        this.pathImport = pathImport;
    }

    public String getConfigPsl() {
        return configPsl;
    }

    public void setConfigPsl(String configPsl) {
        this.configPsl = configPsl;
    }

    public String getNumAwbBol() {
        return numAwbBol;
    }

    public void setNumAwbBol(String numAwbBol) {
        this.numAwbBol = numAwbBol;
    }

    public String getFlightVessel() {
        return flightVessel;
    }

    public void setFlightVessel(String flightVessel) {
        this.flightVessel = flightVessel;
    }

    public Date getDatePickupTM() {
        return datePickupTM;
    }

    public void setDatePickupTM(Date datePickupTM) {
        this.datePickupTM = datePickupTM;
    }

    public Date getEtd() {
        return etd;
    }

    public void setEtd(Date etd) {
        this.etd = etd;
    }

    public Date getFinalDelivery() {
        return finalDelivery;
    }

    public void setFinalDelivery(Date finalDelivery) {
        this.finalDelivery = finalDelivery;
    }

    public Date getEta() {
        return eta;
    }

    public void setEta(Date eta) {
        this.eta = eta;
    }

    public String getCustomsOffice() {
        return customsOffice;
    }

    public void setCustomsOffice(String customsOffice) {
        this.customsOffice = customsOffice;
    }

    public String getMawb() {
        return mawb;
    }

    public void setMawb(String mawb) {
        this.mawb = mawb;
    }

    public Boolean getFlagTransportInformation() {
        return flagTransportInformation;
    }

    public void setFlagTransportInformation(Boolean flagTransportInformation) {
        this.flagTransportInformation = flagTransportInformation;
    }

    public List<EbTtTracing> getListEbTracing() {
        return listEbTracing;
    }

    public void setListEbTracing(List<EbTtTracing> listEbTracing) {
        this.listEbTracing = listEbTracing;
    }

    public List<ExEbDemandeTransporteur> getListDemandeQuote() {
        return listDemandeQuote;
    }

    public void setListDemandeQuote(List<ExEbDemandeTransporteur> listDemandeQuote) {
        this.listDemandeQuote = listDemandeQuote;
    }

    public Boolean getFlagEbtrackTrace() {
        return flagEbtrackTrace;
    }

    public void setFlagEbtrackTrace(Boolean flagEbtrackTrace) {
        this.flagEbtrackTrace = flagEbtrackTrace;
    }

    public EbEtablissement getxEbEtablissement() {
        return xEbEtablissement;
    }

    public void setxEbEtablissement(EbEtablissement xEbEtablissement) {
        this.xEbEtablissement = xEbEtablissement;
    }

    public EbUser getxEbUserCt() {
        return xEbUserCt;
    }

    public void setxEbUserCt(EbUser xEbUserCt) {
        this.xEbUserCt = xEbUserCt;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((aboutTrAwbBol == null) ? 0 : aboutTrAwbBol.hashCode());
        result = prime * result + ((aboutTrCustomOffice == null) ? 0 : aboutTrCustomOffice.hashCode());
        result = prime * result + ((aboutTrEta == null) ? 0 : aboutTrEta.hashCode());
        result = prime * result + ((aboutTrEtd == null) ? 0 : aboutTrEtd.hashCode());
        result = prime * result + ((aboutTrFinalDelivery == null) ? 0 : aboutTrFinalDelivery.hashCode());
        result = prime * result + ((aboutTrFlightVessel == null) ? 0 : aboutTrFlightVessel.hashCode());
        result = prime * result + ((aboutTrMawb == null) ? 0 : aboutTrMawb.hashCode());
        result = prime * result + ((aboutTrPickUp == null) ? 0 : aboutTrPickUp.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((codeAnnulation == null) ? 0 : codeAnnulation.hashCode());
        result = prime * result + ((commentChargeur == null) ? 0 : commentChargeur.hashCode());
        result = prime * result + ((commentMytower == null) ? 0 : commentMytower.hashCode());
        result = prime * result + ((configPsl == null) ? 0 : configPsl.hashCode());
        result = prime * result + ((coutPrestationDouane == null) ? 0 : coutPrestationDouane.hashCode());
        result = prime * result +
            ((curentTimeRemainingInMilliSecond == null) ? 0 : curentTimeRemainingInMilliSecond.hashCode());
        result = prime * result + ((customFields == null) ? 0 : customFields.hashCode());
        result = prime * result + ((customerReference == null) ? 0 : customerReference.hashCode());
        result = prime * result + ((customsOffice == null) ? 0 : customsOffice.hashCode());
        result = prime * result + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        result = prime * result + ((dateDelivery == null) ? 0 : dateDelivery.hashCode());
        result = prime * result + ((dateEnvoiQuotation == null) ? 0 : dateEnvoiQuotation.hashCode());
        result = prime * result + ((dateMaj == null) ? 0 : dateMaj.hashCode());
        result = prime * result + ((dateOfArrival == null) ? 0 : dateOfArrival.hashCode());
        result = prime * result + ((dateOfGoodsAvailability == null) ? 0 : dateOfGoodsAvailability.hashCode());
        result = prime * result + ((datePickup == null) ? 0 : datePickup.hashCode());
        result = prime * result + ((datePickupTM == null) ? 0 : datePickupTM.hashCode());
        result = prime * result + ((dateReadyForPickup == null) ? 0 : dateReadyForPickup.hashCode());
        result = prime * result + ((dg == null) ? 0 : dg.hashCode());
        result = prime * result + ((dryIce == null) ? 0 : dryIce.hashCode());
        result = prime * result + ((ebDemandeFichierJointNum == null) ? 0 : ebDemandeFichierJointNum.hashCode());
        result = prime * result + ((ebDemandeNum == null) ? 0 : ebDemandeNum.hashCode());
        result = prime * result + ((ebPartyDest == null) ? 0 : ebPartyDest.hashCode());
        result = prime * result + ((ebPartyNotif == null) ? 0 : ebPartyNotif.hashCode());
        result = prime * result + ((ebPartyOrigin == null) ? 0 : ebPartyOrigin.hashCode());
        result = prime * result + ((equipement == null) ? 0 : equipement.hashCode());
        result = prime * result + ((eta == null) ? 0 : eta.hashCode());
        result = prime * result + ((etd == null) ? 0 : etd.hashCode());
        result = prime * result + ((finalDelivery == null) ? 0 : finalDelivery.hashCode());
        result = prime * result + ((flagDocumentCustoms == null) ? 0 : flagDocumentCustoms.hashCode());
        result = prime * result + ((flagDocumentTransporteur == null) ? 0 : flagDocumentTransporteur.hashCode());
        result = prime * result + ((flagRecommendation == null) ? 0 : flagRecommendation.hashCode());
        result = prime * result + ((flagTransportInformation == null) ? 0 : flagTransportInformation.hashCode());
        result = prime * result + ((flightVessel == null) ? 0 : flightVessel.hashCode());
        result = prime * result + ((insurance == null) ? 0 : insurance.hashCode());
        result = prime * result + ((insuranceValue == null) ? 0 : insuranceValue.hashCode());
        result = prime * result + ((libelleDestCountry == null) ? 0 : libelleDestCountry.hashCode());
        result = prime * result + ((libelleOriginCountry == null) ? 0 : libelleOriginCountry.hashCode());
        result = prime * result + ((listCategories == null) ? 0 : listCategories.hashCode());
        result = prime * result + ((listCategoriesStr == null) ? 0 : listCategoriesStr.hashCode());
        result = prime * result + ((listEbTracing == null) ? 0 : listEbTracing.hashCode());
        result = prime * result + ((listEmailSendQuotation == null) ? 0 : listEmailSendQuotation.hashCode());
        result = prime * result + ((listLabels == null) ? 0 : listLabels.hashCode());
        result = prime * result + ((listMarchandises == null) ? 0 : listMarchandises.hashCode());
        result = prime * result + ((listTransporteurs == null) ? 0 : listTransporteurs.hashCode());
        result = prime * result + ((mawb == null) ? 0 : mawb.hashCode());
        result = prime * result + ((nbReminderSent == null) ? 0 : nbReminderSent.hashCode());
        result = prime * result + ((nbrDoc == null) ? 0 : nbrDoc.hashCode());
        result = prime * result + ((nomTransporteur == null) ? 0 : nomTransporteur.hashCode());
        result = prime * result + ((numAwbBol == null) ? 0 : numAwbBol.hashCode());
        result = prime * result + ((carrierUniqRefNum == null) ? 0 : carrierUniqRefNum.hashCode());
        result = prime * result + ((pathImport == null) ? 0 : pathImport.hashCode());
        result = prime * result +
            ((preAlertInfoAboutTransportSent == null) ? 0 : preAlertInfoAboutTransportSent.hashCode());
        result = prime * result + ((refTransport == null) ? 0 : refTransport.hashCode());
        result = prime * result + ((sensitive == null) ? 0 : sensitive.hashCode());
        result = prime * result + ((temperatureStorage == null) ? 0 : temperatureStorage.hashCode());
        result = prime * result + ((temperatureTransport == null) ? 0 : temperatureTransport.hashCode());
        result = prime * result + ((timeRemaining == null) ? 0 : timeRemaining.hashCode());
        result = prime * result + ((timeRemainingFf == null) ? 0 : timeRemainingFf.hashCode());
        result = prime * result + ((totalNbrParcel == null) ? 0 : totalNbrParcel.hashCode());
        result = prime * result + ((totalTaxableWeight == null) ? 0 : totalTaxableWeight.hashCode());
        result = prime * result + ((totalVolume == null) ? 0 : totalVolume.hashCode());
        result = prime * result + ((totalWeight == null) ? 0 : totalWeight.hashCode());
        result = prime * result + ((user == null) ? 0 : user.hashCode());
        result = prime * result + ((xEbCostCenter == null) ? 0 : xEbCostCenter.hashCode());
        result = prime * result + ((xEbTransporteur == null) ? 0 : xEbTransporteur.hashCode());
        result = prime * result + ((xEcBroker == null) ? 0 : xEcBroker.hashCode());
        result = prime * result + ((xEcCancelled == null) ? 0 : xEcCancelled.hashCode());
        result = prime * result + ((xEcModeTransport == null) ? 0 : xEcModeTransport.hashCode());
        result = prime * result + ((xEcProductFamily == null) ? 0 : xEcProductFamily.hashCode());
        result = prime * result + ((xEcStatut == null) ? 0 : xEcStatut.hashCode());
        result = prime * result + ((xEcMasterObjectType == null) ? 0 : xEcMasterObjectType.hashCode());
        result = prime * result + ((xEcTypeCustomBroker == null) ? 0 : xEcTypeCustomBroker.hashCode());
        result = prime * result + ((xEcTypeDemande == null) ? 0 : xEcTypeDemande.hashCode());
        result = prime * result + ((xEbTypeTransport == null) ? 0 : xEbTypeTransport.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbDemande other = (EbDemande) obj;

        if (aboutTrAwbBol == null) {
            if (other.aboutTrAwbBol != null) return false;
        }
        else if (!aboutTrAwbBol.equals(other.aboutTrAwbBol)) return false;

        if (aboutTrCustomOffice == null) {
            if (other.aboutTrCustomOffice != null) return false;
        }
        else if (!aboutTrCustomOffice.equals(other.aboutTrCustomOffice)) return false;

        if (aboutTrEta == null) {
            if (other.aboutTrEta != null) return false;
        }
        else if (!aboutTrEta.equals(other.aboutTrEta)) return false;

        if (aboutTrEtd == null) {
            if (other.aboutTrEtd != null) return false;
        }
        else if (!aboutTrEtd.equals(other.aboutTrEtd)) return false;

        if (aboutTrFinalDelivery == null) {
            if (other.aboutTrFinalDelivery != null) return false;
        }
        else if (!aboutTrFinalDelivery.equals(other.aboutTrFinalDelivery)) return false;

        if (aboutTrFlightVessel == null) {
            if (other.aboutTrFlightVessel != null) return false;
        }
        else if (!aboutTrFlightVessel.equals(other.aboutTrFlightVessel)) return false;

        if (aboutTrMawb == null) {
            if (other.aboutTrMawb != null) return false;
        }
        else if (!aboutTrMawb.equals(other.aboutTrMawb)) return false;

        if (aboutTrPickUp == null) {
            if (other.aboutTrPickUp != null) return false;
        }
        else if (!aboutTrPickUp.equals(other.aboutTrPickUp)) return false;

        if (city == null) {
            if (other.city != null) return false;
        }
        else if (!city.equals(other.city)) return false;

        if (codeAnnulation == null) {
            if (other.codeAnnulation != null) return false;
        }
        else if (!codeAnnulation.equals(other.codeAnnulation)) return false;

        if (commentChargeur == null) {
            if (other.commentChargeur != null) return false;
        }
        else if (!commentChargeur.equals(other.commentChargeur)) return false;

        if (commentMytower == null) {
            if (other.commentMytower != null) return false;
        }
        else if (!commentMytower.equals(other.commentMytower)) return false;

        if (configPsl == null) {
            if (other.configPsl != null) return false;
        }
        else if (!configPsl.equals(other.configPsl)) return false;

        if (coutPrestationDouane == null) {
            if (other.coutPrestationDouane != null) return false;
        }
        else if (!coutPrestationDouane.equals(other.coutPrestationDouane)) return false;

        if (curentTimeRemainingInMilliSecond == null) {
            if (other.curentTimeRemainingInMilliSecond != null) return false;
        }
        else if (!curentTimeRemainingInMilliSecond.equals(other.curentTimeRemainingInMilliSecond)) return false;

        if (customFields == null) {
            if (other.customFields != null) return false;
        }
        else if (!customFields.equals(other.customFields)) return false;

        if (customerReference == null) {
            if (other.customerReference != null) return false;
        }
        else if (!customerReference.equals(other.customerReference)) return false;

        if (customsOffice == null) {
            if (other.customsOffice != null) return false;
        }
        else if (!customsOffice.equals(other.customsOffice)) return false;

        if (dateCreation == null) {
            if (other.dateCreation != null) return false;
        }
        else if (!dateCreation.equals(other.dateCreation)) return false;

        if (dateDelivery == null) {
            if (other.dateDelivery != null) return false;
        }
        else if (!dateDelivery.equals(other.dateDelivery)) return false;

        if (dateEnvoiQuotation == null) {
            if (other.dateEnvoiQuotation != null) return false;
        }
        else if (!dateEnvoiQuotation.equals(other.dateEnvoiQuotation)) return false;

        if (dateMaj == null) {
            if (other.dateMaj != null) return false;
        }
        else if (!dateMaj.equals(other.dateMaj)) return false;

        if (dateOfArrival == null) {
            if (other.dateOfArrival != null) return false;
        }
        else if (!dateOfArrival.equals(other.dateOfArrival)) return false;

        if (dateOfGoodsAvailability == null) {
            if (other.dateOfGoodsAvailability != null) return false;
        }
        else if (!dateOfGoodsAvailability.equals(other.dateOfGoodsAvailability)) return false;

        if (datePickup == null) {
            if (other.datePickup != null) return false;
        }
        else if (!datePickup.equals(other.datePickup)) return false;

        if (datePickupTM == null) {
            if (other.datePickupTM != null) return false;
        }
        else if (!datePickupTM.equals(other.datePickupTM)) return false;

        if (dateReadyForPickup == null) {
            if (other.dateReadyForPickup != null) return false;
        }
        else if (!dateReadyForPickup.equals(other.dateReadyForPickup)) return false;

        if (dg == null) {
            if (other.dg != null) return false;
        }
        else if (!dg.equals(other.dg)) return false;

        if (dryIce == null) {
            if (other.dryIce != null) return false;
        }
        else if (!dryIce.equals(other.dryIce)) return false;

        if (ebDemandeFichierJointNum == null) {
            if (other.ebDemandeFichierJointNum != null) return false;
        }
        else if (!ebDemandeFichierJointNum.equals(other.ebDemandeFichierJointNum)) return false;

        if (ebDemandeNum == null) {
            if (other.ebDemandeNum != null) return false;
        }
        else if (!ebDemandeNum.equals(other.ebDemandeNum)) return false;

        if (ebPartyDest == null) {
            if (other.ebPartyDest != null) return false;
        }
        else if (!ebPartyDest.equals(other.ebPartyDest)) return false;

        if (ebPartyNotif == null) {
            if (other.ebPartyNotif != null) return false;
        }
        else if (!ebPartyNotif.equals(other.ebPartyNotif)) return false;

        if (ebPartyOrigin == null) {
            if (other.ebPartyOrigin != null) return false;
        }
        else if (!ebPartyOrigin.equals(other.ebPartyOrigin)) return false;

        if (equipement == null) {
            if (other.equipement != null) return false;
        }
        else if (!equipement.equals(other.equipement)) return false;

        if (eta == null) {
            if (other.eta != null) return false;
        }
        else if (!eta.equals(other.eta)) return false;

        if (etd == null) {
            if (other.etd != null) return false;
        }
        else if (!etd.equals(other.etd)) return false;

        if (finalDelivery == null) {
            if (other.finalDelivery != null) return false;
        }
        else if (!finalDelivery.equals(other.finalDelivery)) return false;

        if (flagDocumentCustoms == null) {
            if (other.flagDocumentCustoms != null) return false;
        }
        else if (!flagDocumentCustoms.equals(other.flagDocumentCustoms)) return false;

        if (flagDocumentTransporteur == null) {
            if (other.flagDocumentTransporteur != null) return false;
        }
        else if (!flagDocumentTransporteur.equals(other.flagDocumentTransporteur)) return false;

        if (flagRecommendation == null) {
            if (other.flagRecommendation != null) return false;
        }
        else if (!flagRecommendation.equals(other.flagRecommendation)) return false;

        if (flagTransportInformation == null) {
            if (other.flagTransportInformation != null) return false;
        }
        else if (!flagTransportInformation.equals(other.flagTransportInformation)) return false;

        if (flightVessel == null) {
            if (other.flightVessel != null) return false;
        }
        else if (!flightVessel.equals(other.flightVessel)) return false;

        if (insurance == null) {
            if (other.insurance != null) return false;
        }
        else if (!insurance.equals(other.insurance)) return false;

        if (insuranceValue == null) {
            if (other.insuranceValue != null) return false;
        }
        else if (!insuranceValue.equals(other.insuranceValue)) return false;

        if (libelleDestCountry == null) {
            if (other.libelleDestCountry != null) return false;
        }
        else if (!libelleDestCountry.equals(other.libelleDestCountry)) return false;

        if (libelleOriginCountry == null) {
            if (other.libelleOriginCountry != null) return false;
        }
        else if (!libelleOriginCountry.equals(other.libelleOriginCountry)) return false;

        if (listCategories == null) {
            if (other.listCategories != null) return false;
        }
        else if (!listCategories.equals(other.listCategories)) return false;

        if (listCategoriesStr == null) {
            if (other.listCategoriesStr != null) return false;
        }
        else if (!listCategoriesStr.equals(other.listCategoriesStr)) return false;

        if (listEbTracing == null) {
            if (other.listEbTracing != null) return false;
        }
        else if (!listEbTracing.equals(other.listEbTracing)) return false;

        if (listEmailSendQuotation == null) {
            if (other.listEmailSendQuotation != null) return false;
        }
        else if (!listEmailSendQuotation.equals(other.listEmailSendQuotation)) return false;

        if (listLabels == null) {
            if (other.listLabels != null) return false;
        }
        else if (!listLabels.equals(other.listLabels)) return false;

        if (listMarchandises == null) {
            if (other.listMarchandises != null) return false;
        }
        else if (!listMarchandises.equals(other.listMarchandises)) return false;

        if (listTransporteurs == null) {
            if (other.listTransporteurs != null) return false;
        }
        else if (!listTransporteurs.equals(other.listTransporteurs)) return false;

        if (mawb == null) {
            if (other.mawb != null) return false;
        }
        else if (!mawb.equals(other.mawb)) return false;

        if (nbReminderSent == null) {
            if (other.nbReminderSent != null) return false;
        }
        else if (!nbReminderSent.equals(other.nbReminderSent)) return false;

        if (nbrDoc == null) {
            if (other.nbrDoc != null) return false;
        }
        else if (!nbrDoc.equals(other.nbrDoc)) return false;

        if (nomTransporteur == null) {
            if (other.nomTransporteur != null) return false;
        }
        else if (!nomTransporteur.equals(other.nomTransporteur)) return false;

        if (numAwbBol == null) {
            if (other.numAwbBol != null) return false;
        }
        else if (!numAwbBol.equals(other.numAwbBol)) return false;

        if (carrierUniqRefNum == null) {
            if (other.carrierUniqRefNum != null) return false;
        }

        else if (!carrierUniqRefNum.equals(other.carrierUniqRefNum)) return false;

        if (pathImport == null) {
            if (other.pathImport != null) return false;
        }
        else if (!pathImport.equals(other.pathImport)) return false;

        if (preAlertInfoAboutTransportSent == null) {
            if (other.preAlertInfoAboutTransportSent != null) return false;
        }
        else if (!preAlertInfoAboutTransportSent.equals(other.preAlertInfoAboutTransportSent)) return false;

        if (refTransport == null) {
            if (other.refTransport != null) return false;
        }
        else if (!refTransport.equals(other.refTransport)) return false;

        if (sensitive == null) {
            if (other.sensitive != null) return false;
        }
        else if (!sensitive.equals(other.sensitive)) return false;

        if (temperatureStorage == null) {
            if (other.temperatureStorage != null) return false;
        }
        else if (!temperatureStorage.equals(other.temperatureStorage)) return false;

        if (temperatureTransport == null) {
            if (other.temperatureTransport != null) return false;
        }
        else if (!temperatureTransport.equals(other.temperatureTransport)) return false;

        if (timeRemaining == null) {
            if (other.timeRemaining != null) return false;
        }
        else if (!timeRemaining.equals(other.timeRemaining)) return false;

        if (timeRemainingFf == null) {
            if (other.timeRemainingFf != null) return false;
        }
        else if (!timeRemainingFf.equals(other.timeRemainingFf)) return false;

        if (totalNbrParcel == null) {
            if (other.totalNbrParcel != null) return false;
        }
        else if (!totalNbrParcel.equals(other.totalNbrParcel)) return false;

        if (totalTaxableWeight == null) {
            if (other.totalTaxableWeight != null) return false;
        }
        else if (!totalTaxableWeight.equals(other.totalTaxableWeight)) return false;

        if (totalVolume == null) {
            if (other.totalVolume != null) return false;
        }
        else if (!totalVolume.equals(other.totalVolume)) return false;

        if (totalWeight == null) {
            if (other.totalWeight != null) return false;
        }
        else if (!totalWeight.equals(other.totalWeight)) return false;

        if (user == null) {
            if (other.user != null) return false;
        }
        else if (!user.equals(other.user)) return false;

        if (xEbCostCenter == null) {
            if (other.xEbCostCenter != null) return false;
        }
        else if (!xEbCostCenter.equals(other.xEbCostCenter)) return false;

        if (xEbTransporteur == null) {
            if (other.xEbTransporteur != null) return false;
        }
        else if (!xEbTransporteur.equals(other.xEbTransporteur)) return false;

        if (xEcBroker == null) {
            if (other.xEcBroker != null) return false;
        }
        else if (!xEcBroker.equals(other.xEcBroker)) return false;

        if (xEcCancelled == null) {
            if (other.xEcCancelled != null) return false;
        }
        else if (!xEcCancelled.equals(other.xEcCancelled)) return false;

        if (xEcModeTransport == null) {
            if (other.xEcModeTransport != null) return false;
        }
        else if (!xEcModeTransport.equals(other.xEcModeTransport)) return false;

        if (xEcProductFamily == null) {
            if (other.xEcProductFamily != null) return false;
        }
        else if (!xEcProductFamily.equals(other.xEcProductFamily)) return false;

        if (xEcStatut == null) {
            if (other.xEcStatut != null) return false;
        }
        else if (!xEcStatut.equals(other.xEcStatut)) return false;

        if (xEcMasterObjectType == null) {
            if (other.xEcMasterObjectType != null) return false;
        }
        else if (!xEcMasterObjectType.equals(other.xEcMasterObjectType)) return false;

        if (xEcTypeCustomBroker == null) {
            if (other.xEcTypeCustomBroker != null) return false;
        }
        else if (!xEcTypeCustomBroker.equals(other.xEcTypeCustomBroker)) return false;

        if (xEcTypeDemande == null) {
            if (other.xEcTypeDemande != null) return false;
        }
        else if (!xEcTypeDemande.equals(other.xEcTypeDemande)) return false;

        if (xEbTypeTransport == null) {
            if (other.xEbTypeTransport != null) return false;
        }
        else if (!xEbTypeTransport.equals(other.xEbTypeTransport)) return false;

        return true;
    }

    public Boolean getFlagEbInvoice() {
        return flagEbInvoice;
    }

    public void setFlagEbInvoice(Boolean flagEbInvoice) {
        this.flagEbInvoice = flagEbInvoice;
    }

    public String getUnitsReference() {
        return unitsReference;
    }

    public void setUnitsReference(String unitsReference) {
        this.unitsReference = unitsReference;
    }

    public Boolean getSendQuotation() {
        return sendQuotation;
    }

    public void setSendQuotation(Boolean sendQuotation) {
        this.sendQuotation = sendQuotation;
    }

    public EbUser getxEbUserTransportInfoMaj() {
        return xEbUserTransportInfoMaj;
    }

    public void setxEbUserTransportInfoMaj(EbUser xEbUserTransportInfoMaj) {
        this.xEbUserTransportInfoMaj = xEbUserTransportInfoMaj;
    }

    public EbUser getxEbUserCtRequired() {
        return xEbUserCtRequired;
    }

    public void setxEbUserCtRequired(EbUser xEbUserCtRequired) {
        this.xEbUserCtRequired = xEbUserCtRequired;
    }

    public Integer getStatusQuote() {
        return statusQuote;
    }

    public void setStatusQuote(Integer statusQuote) {
        this.statusQuote = statusQuote;
    }

    public Integer getxEbTransporteurEtablissementNum() {
        return xEbTransporteurEtablissementNum;
    }

    public void setxEbTransporteurEtablissementNum(Integer xEbTransporteurEtablissementNum) {
        this.xEbTransporteurEtablissementNum = xEbTransporteurEtablissementNum;
    }

    public Boolean getCancelled() {
        return cancelled;
    }

    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public EcCurrency getXecCurrencyInvoice() {
        return xecCurrencyInvoice;
    }

    public void setXecCurrencyInvoice(EcCurrency xecCurrencyInvoice) {
        this.xecCurrencyInvoice = xecCurrencyInvoice;
    }

    public EcCurrency getXecCurrencyInsurance() {
        return xecCurrencyInsurance;
    }

    public void setXecCurrencyInsurance(EcCurrency xecCurrencyInsurance) {
        this.xecCurrencyInsurance = xecCurrencyInsurance;
    }

    public EbEtablissement getxEbEtablissementCt() {
        return xEbEtablissementCt;
    }

    public EbCompagnie getxEbCompagnieCt() {
        return xEbCompagnieCt;
    }

    public void setxEbEtablissementCt(EbEtablissement xEbEtablissementCt) {
        this.xEbEtablissementCt = xEbEtablissementCt;
    }

    public void setxEbCompagnieCt(EbCompagnie xEbCompagnieCt) {
        this.xEbCompagnieCt = xEbCompagnieCt;
    }

    public EbEtablissement getxEbEtablissementBroker() {
        return xEbEtablissementBroker;
    }

    public EbCompagnie getxEbCompagnieBroker() {
        return xEbCompagnieBroker;
    }

    public void setxEbEtablissementBroker(EbEtablissement xEbEtablissementBroker) {
        this.xEbEtablissementBroker = xEbEtablissementBroker;
    }

    public void setxEbCompagnieBroker(EbCompagnie xEbCompagnieBroker) {
        this.xEbCompagnieBroker = xEbCompagnieBroker;
    }

    public EbUser getxEbUserImport() {
        return xEbUserImport;
    }

    public void setxEbUserImport(EbUser xEbUserImport) {
        this.xEbUserImport = xEbUserImport;
    }

    public Boolean getFlagConnectedAsCt() {
        return flagConnectedAsCt;
    }

    public void setFlagConnectedAsCt(Boolean flagConnectedAsCt) {
        this.flagConnectedAsCt = flagConnectedAsCt;
    }

    public Boolean getConfirmedFromTransptPlan() {
        return confirmedFromTransptPlan;
    }

    public void setConfirmedFromTransptPlan(Boolean confirmedFromTransptPlan) {
        this.confirmedFromTransptPlan = confirmedFromTransptPlan;
    }

    public List<EbTypeDocuments> getListTypeDocuments() {

        try {

            if (listTypeDocuments != null) {
                ObjectMapper objectMapper = new ObjectMapper();
                // pour deserialiser la liste et eviter l'erreur lincked hashMap
                listTypeDocuments = Arrays
                    .asList(objectMapper.convertValue(listTypeDocuments, EbTypeDocuments[].class));
            }
            else {
                listTypeDocuments = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTypeDocuments;
    }

    public void setListTypeDocuments(List<EbTypeDocuments> listTypeDocuments) {
        this.listTypeDocuments = listTypeDocuments;
    }

    public Boolean getConfirmPickup() {
        return confirmPickup;
    }

    public Boolean getConfirmDelivery() {
        return confirmDelivery;
    }

    public void setConfirmPickup(Boolean confirmPickup) {
        this.confirmPickup = confirmPickup;
    }

    public void setConfirmDelivery(Boolean confirmDelivery) {
        this.confirmDelivery = confirmDelivery;
    }

    public String getCompanyOrigin() {
        return companyOrigin;
    }

    public void setCompanyOrigin(String companyOrigin) {
        this.companyOrigin = companyOrigin;
    }

    public String getCompanyDest() {
        return companyDest;
    }

    public void setCompanyDest(String companyDest) {
        this.companyDest = companyDest;
    }

    public String getListCategoryFlat() {
        return listCategoryFlat;
    }

    public void setListCategoryFlat(String listCategoryFlat) {
        this.listCategoryFlat = listCategoryFlat;
    }

    public String getListCustomFieldsFlat() {
        return listCustomFieldsFlat;
    }

    public void setListCustomFieldsFlat(String listCustomFieldsFlat) {
        this.listCustomFieldsFlat = listCustomFieldsFlat;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public Integer getTypeData() {
        return typeData;
    }

    public void setTypeData(Integer typeData) {
        this.typeData = typeData;
    }

    public String getEbDemandeHistory() {
        return ebDemandeHistory;
    }

    public void setEbDemandeHistory(String ebDemandeHistory) {
        this.ebDemandeHistory = ebDemandeHistory;
    }

    public Boolean getFlagCustomsInformation() {
        return flagCustomsInformation;
    }

    public void setFlagCustomsInformation(Boolean flagCustomsInformation) {
        this.flagCustomsInformation = flagCustomsInformation;
    }

    public Boolean getSendEmails() {
        return sendEmails;
    }

    public void setSendEmails(Boolean sendEmails) {
        this.sendEmails = sendEmails;
    }

    public Boolean getFlagRequestCustomsBroker() {
        return flagRequestCustomsBroker;
    }

    public void setFlagRequestCustomsBroker(Boolean flagRequestCustomsBroker) {
        this.flagRequestCustomsBroker = flagRequestCustomsBroker;
    }

    public Integer getModuleCreator() {
        return moduleCreator;
    }

    public void setModuleCreator(Integer moduleCreator) {
        this.moduleCreator = moduleCreator;
    }

    public String getLibelleOriginCity() {
        return libelleOriginCity;
    }

    public void setLibelleOriginCity(String libelleOriginCity) {
        this.libelleOriginCity = libelleOriginCity;
    }

    public String getLibelleDestCity() {
        return libelleDestCity;
    }

    public void setLibelleDestCity(String libelleDestCity) {
        this.libelleDestCity = libelleDestCity;
    }

    public String getLibelleLastPsl() {
        return libelleLastPsl;
    }

    public Integer getCodeLastPsl() {
        return codeLastPsl;
    }

    public Date getDateLastPsl() {
        return dateLastPsl;
    }

    public void setLibelleLastPsl(String libelleLastPsl) {
        this.libelleLastPsl = libelleLastPsl;
    }

    public void setCodeLastPsl(Integer codeLastPsl) {
        this.codeLastPsl = codeLastPsl;
    }

    public void setDateLastPsl(Date dateLastPsl) {
        this.dateLastPsl = dateLastPsl;
    }

    public Integer getxEcNature() {
        return xEcNature;
    }

    public void setxEcNature(Integer xEcNature) {
        this.xEcNature = xEcNature;
    }

    public EbDemande getEbDemandeInitial() {
        return ebDemandeInitial;
    }

    public void setEbDemandeInitial(EbDemande ebDemandeInitial) {
        this.ebDemandeInitial = ebDemandeInitial;
    }

    public EbDemande getEbDemandeMasterObject() {
        return ebDemandeMasterObject;
    }

    public void setEbDemandeMasterObject(EbDemande ebDemandeMasterObject) {
        this.ebDemandeMasterObject = ebDemandeMasterObject;
    }

    public Integer getxEcMasterObjectType() {
        return xEcMasterObjectType;
    }

    public void setxEcMasterObjectType(Integer xEcMasterObjectType) {
        this.xEcMasterObjectType = xEcMasterObjectType;
    }

    public String getListFlag() {
        return listFlag;
    }

    public void setListFlag(String listFlag) {
        this.listFlag = listFlag;
    }

    public EbEtablissement getxEbEtabOrigin() {
        return xEbEtabOrigin;
    }

    public void setxEbEtabOrigin(EbEtablissement xEbEtabOrigin) {
        this.xEbEtabOrigin = xEbEtabOrigin;
    }

    public EbEtablissement getxEbEtabDest() {
        return xEbEtabDest;
    }

    public void setxEbEtabDest(EbEtablissement xEbEtabDest) {
        this.xEbEtabDest = xEbEtabDest;
    }

    public EbEtablissement getxEbEtabOriginCustomsBroker() {
        return xEbEtabOriginCustomsBroker;
    }

    public void setxEbEtabOriginCustomsBroker(EbEtablissement xEbEtabOriginCustomsBroker) {
        this.xEbEtabOriginCustomsBroker = xEbEtabOriginCustomsBroker;
    }

    public EbEtablissement getxEbEtabDestCustomsBroker() {
        return xEbEtabDestCustomsBroker;
    }

    public void setxEbEtabDestCustomsBroker(EbEtablissement xEbEtabDestCustomsBroker) {
        this.xEbEtabDestCustomsBroker = xEbEtabDestCustomsBroker;
    }

    public String getStatutStr() {
        return statutStr;
    }

    public void setStatutStr(String statutStr) {
        this.statutStr = statutStr;
    }

    public List<ProposionRdvDto> getListPropRdvPk() {
        return listPropRdvPk;
    }

    public void setListPropRdvPk(List<ProposionRdvDto> listPropRdvPk) {
        this.listPropRdvPk = listPropRdvPk;
    }

    public List<ProposionRdvDto> getListPropRdvDl() {
        return listPropRdvDl;
    }

    public void setListPropRdvDl(List<ProposionRdvDto> listPropRdvDl) {
        this.listPropRdvDl = listPropRdvDl;
    }

    public String getConfigPslNum() {
        return configPslNum;
    }

    public void setConfigPslNum(String configPslNum) {
        this.configPslNum = configPslNum;
    }

    public EbEntrepot getxEbEntrepotUnloading() {
        return xEbEntrepotUnloading;
    }

    public void setxEbEntrepotUnloading(EbEntrepot xEbEntrepotUnloading) {
        this.xEbEntrepotUnloading = xEbEntrepotUnloading;
    }

    public EbTtSchemaPsl getxEbSchemaPsl() {
        return xEbSchemaPsl;
    }

    public void setxEbSchemaPsl(EbTtSchemaPsl xEbSchemaPsl) {
        this.xEbSchemaPsl = xEbSchemaPsl;

        if (this.xEbSchemaPsl != null &&
            this.xEbSchemaPsl.getListPsl() != null && !this.xEbSchemaPsl.getListPsl().isEmpty()) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                String json = objectMapper.writeValueAsString(this.xEbSchemaPsl.getListPsl());
                this.xEbSchemaPsl
                    .setListPsl(objectMapper.readValue(json, new TypeReference<ArrayList<EbTtCompanyPsl>>() {
                    }));
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            this.configPsl = "";

            List<EbTtCompanyPsl> listPsl = this.xEbSchemaPsl.getListPsl();
            EbTtSchemaPsl.sortListPsl(listPsl);

            for (EbTtCompanyPsl psl: listPsl) {

                if (psl.getIsChecked() != null && psl.getIsChecked()) {
                    if (!this.configPsl.isEmpty()) this.configPsl += "|";
                    this.configPsl += psl.getCodeAlpha();
                }

            }

        }

    }

    public String getxEcIncotermLibelle() {
        return xEcIncotermLibelle;
    }

    public void setxEcIncotermLibelle(String xEcIncotermLibelle) {
        this.xEcIncotermLibelle = xEcIncotermLibelle;
    }

    public String getCodeAlphaPslCourant() {
        return codeAlphaPslCourant;
    }

    public String getLibellePslCourant() {
        return libellePslCourant;
    }

    public String getCodeAlphaLastPsl() {
        return codeAlphaLastPsl;
    }

    public void setCodeAlphaPslCourant(String codeAlphaPslCourant) {
        this.codeAlphaPslCourant = codeAlphaPslCourant;
    }

    public void setLibellePslCourant(String libellePslCourant) {
        this.libellePslCourant = libellePslCourant;
    }

    public void setCodeAlphaLastPsl(String codeAlphaLastPsl) {
        this.codeAlphaLastPsl = codeAlphaLastPsl;
    }

    public Boolean getFlagConsolidated() {
        return flagConsolidated;
    }

    public void setFlagConsolidated(Boolean flagConsolidated) {
        this.flagConsolidated = flagConsolidated;
    }

    public Boolean getValorized() {
        return isValorized;
    }

    public void setValorized(Boolean valorized) {
		this.isValorized = valorized;
    }

    public Integer getxEbDemandeGroup() {
        return xEbDemandeGroup;
    }

    public void setxEbDemandeGroup(Integer xEbDemandeGroup) {
        this.xEbDemandeGroup = xEbDemandeGroup;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTypeRequestLibelle() {
        return typeRequestLibelle;
    }

    public void setTypeRequestLibelle(String typeRequestLibelle) {
        this.typeRequestLibelle = typeRequestLibelle;
    }

    public boolean have_categories() {
        boolean result = false;
        List<EbCategorie> cats = this.getListCategories();
        return cats != null;
    }

    public Integer getTorDuration() {
        return torDuration;
    }

    public void setTorDuration(Integer torDuration) {
        this.torDuration = torDuration;
    }

    public Integer getTorType() {
        return torType;
    }

    public void setTorType(Integer torType) {
        this.torType = torType;
    }

    public Date getDateTraitementCPB() {
        return dateTraitementCPB;
    }

    public void setDateTraitementCPB(Date dateTraitementCPB) {
        this.dateTraitementCPB = dateTraitementCPB;
    }

    public String getIntegratedVia() {
        return integratedVia;
    }

    public void setIntegratedVia(String integratedVia) {
        this.integratedVia = integratedVia;
    }

    public String getCarrierUniqRefNum() {
        return carrierUniqRefNum;
    }

    public void setCarrierUniqRefNum(String carrierUniqRefNum) {
        this.carrierUniqRefNum = carrierUniqRefNum;
    }

    public ExEbDemandeTransporteur getSelectedCarrier() {

        if (this.exEbDemandeTransporteurs != null && this.exEbDemandeTransporteurs.size() > 0) {
            return this.exEbDemandeTransporteurs.get(0);
        }

        return null;
    }

    public Boolean getAskForTransportResponsibility() {
        return askForTransportResponsibility;
    }

    public void setAskForTransportResponsibility(Boolean askForTransportResponsibility) {
        this.askForTransportResponsibility = askForTransportResponsibility;
    }

    public Boolean getProvideTransport() {
        return provideTransport;
    }

    public void setProvideTransport(Boolean provideTransport) {
        this.provideTransport = provideTransport;
    }

    public Boolean getTdcAcknowledge() {
        return tdcAcknowledge;
    }

    public void setTdcAcknowledge(Boolean tdcAcknowledge) {
        this.tdcAcknowledge = tdcAcknowledge;
    }

    public Date getTdcAcknowledgeDate() {
        return tdcAcknowledgeDate;
    }

    public void setTdcAcknowledgeDate(Date tdcAcknowledgeDate) {
        this.tdcAcknowledgeDate = tdcAcknowledgeDate;
    }

    public Boolean getEligibleForConsolidation() {
        return eligibleForConsolidation;
    }

    public void setEligibleForConsolidation(Boolean eligibleForConsolidation) {
        this.eligibleForConsolidation = eligibleForConsolidation;
    }

    public Integer getTrackingLevel() {
        return trackingLevel;
    }

    public void setTrackingLevel(Integer trackingLevel) {
        this.trackingLevel = trackingLevel;
    }

    public Integer getxEbTypeFluxNum() {
        return xEbTypeFluxNum;
    }

    public Integer getxEbIncotermNum() {
        return xEbIncotermNum;
    }

    public String getxEbTypeFluxDesignation() {
        return xEbTypeFluxDesignation;
    }

    public void setxEbTypeFluxDesignation(String xEbTypeFluxDesignation) {
        this.xEbTypeFluxDesignation = xEbTypeFluxDesignation;
    }

    public void setSelectedCarrier(ExEbDemandeTransporteur selectedCarrier) {
        this.selectedCarrier = selectedCarrier;
    }

    public void setxEbTypeFluxNum(Integer xEbTypeFluxNum) {
        this.xEbTypeFluxNum = xEbTypeFluxNum;
    }

    public void setxEbIncotermNum(Integer xEbIncotermNum) {
        this.xEbIncotermNum = xEbIncotermNum;
    }

    public Boolean getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(Boolean isDraft) {
        this.isDraft = isDraft;
    }

    public String getListCustomFieldsValueFlat() {
        return listCustomFieldsValueFlat;
    }

    public void setListCustomFieldsValueFlat(String listCustomFieldsValueFlat) {
        this.listCustomFieldsValueFlat = listCustomFieldsValueFlat;
    }

    public Date getCustomerCreationDate() {
        return this.customerCreationDate;
    }

    public void setCustomerCreationDate(Date customerCreationDate) {
        this.customerCreationDate = customerCreationDate;
    }

    public Boolean getIsGrouping() {
        return isGrouping;
    }

    public void setIsGrouping(Boolean isGrouping) {
        this.isGrouping = isGrouping;
    }

    public EbQrGroupe getEbQrGroupe() {
        return ebQrGroupe;
    }

    public void setEbQrGroupe(EbQrGroupe ebQrGroupe) {
        this.ebQrGroupe = ebQrGroupe;
    }

    public Integer getxEcStatutGroupage() {
        return xEcStatutGroupage;
    }

    public void setxEcStatutGroupage(Integer xEcStatutGroupage) {
        this.xEcStatutGroupage = xEcStatutGroupage;
    }

    public List<TransportReferenceInfosDTO> getConsolidationListTransportReferenceInfos() {
        return consolidationListTransportReferenceInfos;
    }

    public void setConsolidationListTransportReferenceInfos(
        List<TransportReferenceInfosDTO> consolidationListTransportReferenceInfos) {
        this.consolidationListTransportReferenceInfos = consolidationListTransportReferenceInfos;
    }

    public List<Long> getListDeliveryForConsolidateNum() {
        return listDeliveryForConsolidateNum;
    }

    public void setListDeliveryForConsolidateNum(List<Long> listDeliveryForConsolidateNum) {
        this.listDeliveryForConsolidateNum = listDeliveryForConsolidateNum;
    }

    public BigDecimal getConvertedSaving() {
        return convertedSaving;
    }

    public void setConvertedSaving(BigDecimal convertedSaving) {
        this.convertedSaving = convertedSaving;
    }

    public BigDecimal getConvertedPrice() {
        return convertedPrice;
    }

    public void setConvertedPrice(BigDecimal convertedPrice) {
        this.convertedPrice = convertedPrice;
    }

    public EcCurrency getXecCurrencyTrResultante() {
        return xecCurrencyTrResultante;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void setXecCurrencyTrResultante(EcCurrency xecCurrencyTrInitiale) {
        this.xecCurrencyTrResultante = xecCurrencyTrInitiale;
    }

    public String getScenario() {
        return scenario;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public void setCustomFields(List<CustomFields> listCustomFields) {

        try {
            this.listCustomsFields = listCustomFields;
            ObjectMapper mapper = new ObjectMapper();
            String customFieldsAsJson = mapper.writeValueAsString(listCustomFields);
            this.setCustomFields(customFieldsAsJson);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Integer getExportControlStatut() {
        return exportControlStatut;
    }

    public void setExportControlStatut(Integer exportControlStatut) {
        this.exportControlStatut = exportControlStatut;
    }

    public String getUnitsPackingList() {
        return unitsPackingList;
    }

    public void setUnitsPackingList(String unitsPackingList) {
        this.unitsPackingList = unitsPackingList;
    }

    public List<EbMarchandise> getOldMarchandises() {
        return oldMarchandises;
    }

    public void setOldMarchandises(List<EbMarchandise> oldMarchandises) {
        this.oldMarchandises = oldMarchandises;
    }

    public BigDecimal getPrixTransporteurFinal() {
        return prixTransporteurFinal;
    }

    public void setPrixTransporteurFinal(BigDecimal prixTransporteurFinal) {
        this.prixTransporteurFinal = prixTransporteurFinal;
    }

    public String getxEbTypeFluxCode() {
        return xEbTypeFluxCode;
    }

    public void setxEbTypeFluxCode(String xEbTypeFluxCode) {
        this.xEbTypeFluxCode = xEbTypeFluxCode;
    }

    public Integer getValuationType() {
        return valuationType;
    }

    public void setValuationType(Integer valuationType) {
        this.valuationType = valuationType;
    }

    public Date getWaitingForQuote() {
        return waitingForQuote;
    }

    public void setWaitingForQuote(Date waitingForQuote) {
        this.waitingForQuote = waitingForQuote;
    }

    public Date getWaitingForConf() {
        return waitingForConf;
    }

    public void setWaitingForConf(Date waitingForConf) {
        this.waitingForConf = waitingForConf;
    }

    public Date getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Date confirmed) {
        this.confirmed = confirmed;
    }

		public Boolean getIsValorized()
		{
			return isValorized;
		}

		public void setIsValorized(Boolean isValorized)
		{
			this.isValorized = isValorized;
		}

		public String getConsolidationRuleLibelle()
		{
			return consolidationRuleLibelle;
		}

		public void setConsolidationRuleLibelle(String consolidationRuleLibelle)
		{
			this.consolidationRuleLibelle = consolidationRuleLibelle;
		}
}
