package com.adias.mytowereasy.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;


@Entity
@Table(name = "eb_user_tag", schema = "work")
public class EbUserTag extends EbTimeStamps {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebUserTagNum;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_host", insertable = true, updatable = false, nullable = true)
    private EbUser host;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "x_guest", nullable = true)
    private EbUser guest;

    private String tags;
    private String comment;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_etablissement_host", insertable = true, updatable = false)
    private EbEtablissement xEbEtablissementHost;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_etablissement_guest", insertable = true, updatable = false, nullable = true)
    private EbEtablissement xEbEtablissementGuest;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_compagnie_host", insertable = true, updatable = false)
    private EbCompagnie xEbCompanieHost;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_compagnie_guest", insertable = true, updatable = false, nullable = true)
    private EbCompagnie xEbCompagnieGuest;

    @ColumnDefault("true")
    private Boolean activated;

    public EbUserTag() {
    }

    public EbUserTag(
        Integer ebUserTagNum,
        EbUser host,
        EbUser guest,
        String tags,
        String comment,
        EbEtablissement xEbEtablissementHost,
        EbEtablissement xEbEtablissementGuest,
        EbCompagnie xEbCompanieHost,
        EbCompagnie xEbCompagnieGuest,
        Boolean activated) {
        super();
        this.ebUserTagNum = ebUserTagNum;
        this.host = host;
        this.guest = guest;
        this.tags = tags;
        this.comment = comment;
        this.xEbEtablissementHost = xEbEtablissementHost;
        this.xEbEtablissementGuest = xEbEtablissementGuest;
        this.xEbCompanieHost = xEbCompanieHost;
        this.xEbCompagnieGuest = xEbCompagnieGuest;
        this.activated = activated;
    }

    public Integer getEbUserTagNum() {
        return ebUserTagNum;
    }

    public void setEbUserTagNum(Integer ebUserTagNum) {
        this.ebUserTagNum = ebUserTagNum;
    }

    public EbUser getHost() {
        return host;
    }

    public void setHost(EbUser host) {
        this.host = host;
    }

    public EbUser getGuest() {
        return guest;
    }

    public void setGuest(EbUser guest) {
        this.guest = guest;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public EbEtablissement getxEbEtablissementHost() {
        return xEbEtablissementHost;
    }

    public void setxEbEtablissementHost(EbEtablissement xEbEtablissementHost) {
        this.xEbEtablissementHost = xEbEtablissementHost;
    }

    public EbEtablissement getxEbEtablissementGuest() {
        return xEbEtablissementGuest;
    }

    public void setxEbEtablissementGuest(EbEtablissement xEbEtablissementGuest) {
        this.xEbEtablissementGuest = xEbEtablissementGuest;
    }

    public EbCompagnie getxEbCompanieHost() {
        return xEbCompanieHost;
    }

    public void setxEbCompanieHost(EbCompagnie xEbCompanieHost) {
        this.xEbCompanieHost = xEbCompanieHost;
    }

    public EbCompagnie getxEbCompagnieGuest() {
        return xEbCompagnieGuest;
    }

    public void setxEbCompagnieGuest(EbCompagnie xEbCompagnieGuest) {
        this.xEbCompagnieGuest = xEbCompagnieGuest;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((ebUserTagNum == null) ? 0 : ebUserTagNum.hashCode());
        result = prime * result + ((guest == null) ? 0 : guest.hashCode());
        result = prime * result + ((host == null) ? 0 : host.hashCode());
        result = prime * result + ((tags == null) ? 0 : tags.hashCode());
        result = prime * result + ((xEbCompagnieGuest == null) ? 0 : xEbCompagnieGuest.hashCode());
        result = prime * result + ((xEbCompanieHost == null) ? 0 : xEbCompanieHost.hashCode());
        result = prime * result + ((xEbEtablissementGuest == null) ? 0 : xEbEtablissementGuest.hashCode());
        result = prime * result + ((xEbEtablissementHost == null) ? 0 : xEbEtablissementHost.hashCode());

        result = prime * result + ((activated == null) ? 0 : activated.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbUserTag other = (EbUserTag) obj;

        if (comment == null) {
            if (other.comment != null) return false;
        }
        else if (!comment.equals(other.comment)) return false;

        if (ebUserTagNum == null) {
            if (other.ebUserTagNum != null) return false;
        }
        else if (!ebUserTagNum.equals(other.ebUserTagNum)) return false;

        if (guest == null) {
            if (other.guest != null) return false;
        }
        else if (!guest.equals(other.guest)) return false;

        if (host == null) {
            if (other.host != null) return false;
        }
        else if (!host.equals(other.host)) return false;

        if (tags == null) {
            if (other.tags != null) return false;
        }
        else if (!tags.equals(other.tags)) return false;

        if (xEbCompagnieGuest == null) {
            if (other.xEbCompagnieGuest != null) return false;
        }
        else if (!xEbCompagnieGuest.equals(other.xEbCompagnieGuest)) return false;

        if (xEbCompanieHost == null) {
            if (other.xEbCompanieHost != null) return false;
        }
        else if (!xEbCompanieHost.equals(other.xEbCompanieHost)) return false;

        if (xEbEtablissementGuest == null) {
            if (other.xEbEtablissementGuest != null) return false;
        }
        else if (!xEbEtablissementGuest.equals(other.xEbEtablissementGuest)) return false;

        if (xEbEtablissementHost == null) {
            if (other.xEbEtablissementHost != null) return false;
        }
        else if (!xEbEtablissementHost.equals(other.xEbEtablissementHost)) return false;

        if (activated == null) {
            if (other.activated != null) return false;
        }
        else if (!activated.equals(other.activated)) return false;

        return true;
    }
}
