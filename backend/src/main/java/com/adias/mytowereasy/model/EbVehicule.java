package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;


@Entity
@EntityListeners(AuditingEntityListener.class)
public class EbVehicule implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebVehiculeNum;

    // poids
    private Double weight;
    private String libelle;

    // largeur
    private Double width;

    // longueur
    private Double lengh;

    // hauteur
    private Double height;

    @CreatedDate
    private Date dateCreation;

    @LastModifiedDate
    private Date maj;

    private Integer xEbEtablissement;

    private Integer xEbCompagnie;

    private Integer xModeTransport;

    private Double tare;

    private Integer xParticularite;

    private Integer xType;

    public Integer getEbVehiculeNum() {
        return ebVehiculeNum;
    }

    public void setEbVehiculeNum(Integer ebVehiculeNum) {
        this.ebVehiculeNum = ebVehiculeNum;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getLengh() {
        return lengh;
    }

    public void setLengh(Double lengh) {
        this.lengh = lengh;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getMaj() {
        return maj;
    }

    public void setMaj(Date maj) {
        this.maj = maj;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getxEbEtablissement() {
        return xEbEtablissement;
    }

    public void setxEbEtablissement(Integer xEbEtablissement) {
        this.xEbEtablissement = xEbEtablissement;
    }

    public Integer getxModeTransport() {
        return xModeTransport;
    }

    public void setxModeTransport(Integer xModeTransport) {
        this.xModeTransport = xModeTransport;
    }

    public Integer getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(Integer xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Double getTare() {
        return tare;
    }

    public void setTare(Double tare) {
        this.tare = tare;
    }

    public Integer getxParticularite() {
        return xParticularite;
    }

    public void setxParticularite(Integer xParticularite) {
        this.xParticularite = xParticularite;
    }

    public Integer getxType() {
        return xType;
    }

    public void setxType(Integer xType) {
        this.xType = xType;
    }

    public EbVehicule() {
        super();
        // TODO Auto-generated constructor stub
    }
}
