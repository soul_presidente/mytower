/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.qm;

import java.util.Date;

import javax.persistence.*;

import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.model.Enumeration;


/** EbQmIncidentResponsible entity. @author Mohamed */
@Entity
// @Table(indexes = { @Index(name = "ebQmIncidentResponsibleNumIdx", columnList
// =
// "ebQmIncidentResponsibleNum"),
// @Index(name = "ebQmIncidentResponsibleIncidentIdx", columnList =
// "x_eb_incident"),
// @Index(name = "ebQmIncidentResponsibleResponsibleIdx", columnList =
// "x_eb_etab_resp"),
// @Index(name = "ebQmIncidentResponsibleCarrierIdx", columnList =
// "x_eb_etab_carrier"),
// @Index(name = "ebQmIncidentResponsibleCurrencyIdx", columnList =
// "x_ec_currency"),
// @Index(name = "ebQmIncidentResponsibleCurrency2Idx", columnList =
// "x_ec_currency2"),
// @Index(name = "ebQmIncidentResponsibleUserOtherIdx", columnList =
// "x_eb_user_other") })
@Table(name = "eb_qm_incident_responsible", schema = "work")
public class EbQmIncidentResponsible implements java.io.Serializable {
    private static final long serialVersionUID = 3938565630051110629L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebQmIncidentResponsibleNum;

    @Column
    private Integer pourcentIncident;

    @Column(columnDefinition = "TEXT")
    private String commentAlis;

    @Column(columnDefinition = "TEXT")
    private String commentEtablissement;

    @Column
    private Double montantIncident;

    @Column(columnDefinition = "TEXT")
    private String commentThirdParty;

    @Column
    private Double claimAmount = new Double(0);

    @Column
    private Double recovered = new Double(0);

    @Column
    private Double balance = new Double(0);

    @Column
    private Double claimAmount2 = new Double(0);

    @Column
    private Double recovered2 = new Double(0);

    @Column
    private Double balance2 = new Double(0);

    @Column(columnDefinition = "TEXT")
    private String commentAssurance;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateMaj;

    @Enumerated(EnumType.ORDINAL)
    private Enumeration.IncidentResponsibleType typeResp;

    @Enumerated(EnumType.ORDINAL)
    private Enumeration.IncidentStatus statut;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_incident")
    private EbQmIncident incident;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab_carrier")
    private EbEtablissement carrier;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab_resp")
    private EbEtablissement responsible;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_currency")
    private EcCurrency currency;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_currency2")
    private EcCurrency currency2;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_other")
    private EbUser userOther;

    public Integer getEbQmIncidentResponsibleNum() {
        return ebQmIncidentResponsibleNum;
    }

    public void setEbQmIncidentResponsibleNum(Integer ebQmIncidentResponsibleNum) {
        this.ebQmIncidentResponsibleNum = ebQmIncidentResponsibleNum;
    }

    public Integer getPourcentIncident() {
        return pourcentIncident;
    }

    public void setPourcentIncident(Integer pourcentIncident) {
        this.pourcentIncident = pourcentIncident;
    }

    public String getCommentAlis() {
        return commentAlis;
    }

    public void setCommentAlis(String commentAlis) {
        this.commentAlis = commentAlis;
    }

    public String getCommentEtablissement() {
        return commentEtablissement;
    }

    public void setCommentEtablissement(String commentEtablissement) {
        this.commentEtablissement = commentEtablissement;
    }

    public Double getMontantIncident() {
        return montantIncident;
    }

    public void setMontantIncident(Double montantIncident) {
        this.montantIncident = montantIncident;
    }

    public String getCommentThirdParty() {
        return commentThirdParty;
    }

    public void setCommentThirdParty(String commentThirdParty) {
        this.commentThirdParty = commentThirdParty;
    }

    public Double getClaimAmount() {
        return claimAmount;
    }

    public void setClaimAmount(Double claimAmount) {
        this.claimAmount = claimAmount;
    }

    public Double getRecovered() {
        return recovered;
    }

    public void setRecovered(Double recovered) {
        this.recovered = recovered;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getClaimAmount2() {
        return claimAmount2;
    }

    public void setClaimAmount2(Double claimAmount2) {
        this.claimAmount2 = claimAmount2;
    }

    public Double getRecovered2() {
        return recovered2;
    }

    public void setRecovered2(Double recovered2) {
        this.recovered2 = recovered2;
    }

    public Double getBalance2() {
        return balance2;
    }

    public void setBalance2(Double balance2) {
        this.balance2 = balance2;
    }

    public String getCommentAssurance() {
        return commentAssurance;
    }

    public void setCommentAssurance(String commentAssurance) {
        this.commentAssurance = commentAssurance;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateMaj() {
        return dateMaj;
    }

    public void setDateMaj(Date dateMaj) {
        this.dateMaj = dateMaj;
    }

    public Enumeration.IncidentResponsibleType getTypeResp() {
        return typeResp;
    }

    public void setTypeResp(Enumeration.IncidentResponsibleType typeResp) {
        this.typeResp = typeResp;
    }

    public Enumeration.IncidentStatus getStatut() {
        return statut;
    }

    public void setStatut(Enumeration.IncidentStatus statut) {
        this.statut = statut;
    }

    public EbQmIncident getIncident() {
        return incident;
    }

    public void setIncident(EbQmIncident incident) {
        this.incident = incident;
    }

    public EbEtablissement getCarrier() {
        return carrier;
    }

    public void setCarrier(EbEtablissement carrier) {
        this.carrier = carrier;
    }

    public EbEtablissement getResponsible() {
        return responsible;
    }

    public void setResponsible(EbEtablissement responsible) {
        this.responsible = responsible;
    }

    public EcCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(EcCurrency currency) {
        this.currency = currency;
    }

    public EcCurrency getCurrency2() {
        return currency2;
    }

    public void setCurrency2(EcCurrency currency2) {
        this.currency2 = currency2;
    }

    public EbUser getUserOther() {
        return userOther;
    }

    public void setUserOther(EbUser userOther) {
        this.userOther = userOther;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((balance == null) ? 0 : balance.hashCode());
        result = prime * result + ((balance2 == null) ? 0 : balance2.hashCode());
        result = prime * result + ((carrier == null) ? 0 : carrier.hashCode());
        result = prime * result + ((claimAmount == null) ? 0 : claimAmount.hashCode());
        result = prime * result + ((claimAmount2 == null) ? 0 : claimAmount2.hashCode());
        result = prime * result + ((commentAlis == null) ? 0 : commentAlis.hashCode());
        result = prime * result + ((commentAssurance == null) ? 0 : commentAssurance.hashCode());
        result = prime * result + ((commentEtablissement == null) ? 0 : commentEtablissement.hashCode());
        result = prime * result + ((commentThirdParty == null) ? 0 : commentThirdParty.hashCode());
        result = prime * result + ((currency == null) ? 0 : currency.hashCode());
        result = prime * result + ((currency2 == null) ? 0 : currency2.hashCode());
        result = prime * result + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        result = prime * result + ((dateMaj == null) ? 0 : dateMaj.hashCode());
        result = prime * result + ((ebQmIncidentResponsibleNum == null) ? 0 : ebQmIncidentResponsibleNum.hashCode());
        result = prime * result + ((incident == null) ? 0 : incident.hashCode());
        result = prime * result + ((montantIncident == null) ? 0 : montantIncident.hashCode());
        result = prime * result + ((pourcentIncident == null) ? 0 : pourcentIncident.hashCode());
        result = prime * result + ((recovered == null) ? 0 : recovered.hashCode());
        result = prime * result + ((recovered2 == null) ? 0 : recovered2.hashCode());
        result = prime * result + ((responsible == null) ? 0 : responsible.hashCode());
        result = prime * result + ((statut == null) ? 0 : statut.hashCode());
        result = prime * result + ((typeResp == null) ? 0 : typeResp.hashCode());
        result = prime * result + ((userOther == null) ? 0 : userOther.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbQmIncidentResponsible other = (EbQmIncidentResponsible) obj;

        if (balance == null) {
            if (other.balance != null) return false;
        }
        else if (!balance.equals(other.balance)) return false;

        if (balance2 == null) {
            if (other.balance2 != null) return false;
        }
        else if (!balance2.equals(other.balance2)) return false;

        if (carrier == null) {
            if (other.carrier != null) return false;
        }
        else if (!carrier.equals(other.carrier)) return false;

        if (claimAmount == null) {
            if (other.claimAmount != null) return false;
        }
        else if (!claimAmount.equals(other.claimAmount)) return false;

        if (claimAmount2 == null) {
            if (other.claimAmount2 != null) return false;
        }
        else if (!claimAmount2.equals(other.claimAmount2)) return false;

        if (commentAlis == null) {
            if (other.commentAlis != null) return false;
        }
        else if (!commentAlis.equals(other.commentAlis)) return false;

        if (commentAssurance == null) {
            if (other.commentAssurance != null) return false;
        }
        else if (!commentAssurance.equals(other.commentAssurance)) return false;

        if (commentEtablissement == null) {
            if (other.commentEtablissement != null) return false;
        }
        else if (!commentEtablissement.equals(other.commentEtablissement)) return false;

        if (commentThirdParty == null) {
            if (other.commentThirdParty != null) return false;
        }
        else if (!commentThirdParty.equals(other.commentThirdParty)) return false;

        if (currency == null) {
            if (other.currency != null) return false;
        }
        else if (!currency.equals(other.currency)) return false;

        if (currency2 == null) {
            if (other.currency2 != null) return false;
        }
        else if (!currency2.equals(other.currency2)) return false;

        if (dateCreation == null) {
            if (other.dateCreation != null) return false;
        }
        else if (!dateCreation.equals(other.dateCreation)) return false;

        if (dateMaj == null) {
            if (other.dateMaj != null) return false;
        }
        else if (!dateMaj.equals(other.dateMaj)) return false;

        if (ebQmIncidentResponsibleNum == null) {
            if (other.ebQmIncidentResponsibleNum != null) return false;
        }
        else if (!ebQmIncidentResponsibleNum.equals(other.ebQmIncidentResponsibleNum)) return false;

        if (incident == null) {
            if (other.incident != null) return false;
        }
        else if (!incident.equals(other.incident)) return false;

        if (montantIncident == null) {
            if (other.montantIncident != null) return false;
        }
        else if (!montantIncident.equals(other.montantIncident)) return false;

        if (pourcentIncident == null) {
            if (other.pourcentIncident != null) return false;
        }
        else if (!pourcentIncident.equals(other.pourcentIncident)) return false;

        if (recovered == null) {
            if (other.recovered != null) return false;
        }
        else if (!recovered.equals(other.recovered)) return false;

        if (recovered2 == null) {
            if (other.recovered2 != null) return false;
        }
        else if (!recovered2.equals(other.recovered2)) return false;

        if (responsible == null) {
            if (other.responsible != null) return false;
        }
        else if (!responsible.equals(other.responsible)) return false;

        if (statut != other.statut) return false;
        if (typeResp != other.typeResp) return false;

        if (userOther == null) {
            if (other.userOther != null) return false;
        }
        else if (!userOther.equals(other.userOther)) return false;

        return true;
    }
}
