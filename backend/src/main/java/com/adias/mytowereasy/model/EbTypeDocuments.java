package com.adias.mytowereasy.model;

import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.adias.mytowereasy.types.CustomObjectIdResolver;


@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "ebTypeDocumentsNum",
    resolver = CustomObjectIdResolver.class,
    scope = EbTypeDocuments.class)
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table
@EntityListeners(AuditingEntityListener.class)
public class EbTypeDocuments extends EbTimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "eb_type_documents_num", nullable = false, unique = true)
    private Integer ebTypeDocumentsNum;

    private String nom;

    private String code;

    private Integer ordre;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<Integer> module;

    private String listModuleStr;

    private Integer xEbCompagnie;

    @Column(columnDefinition = "boolean default true")
    private Boolean activated; // permettant d'identifier si typeDocument est
                               // desactivés ou actives

    @Column(columnDefinition = "boolean default true")
    private Boolean isexpectedDoc;

    @Column(columnDefinition = "boolean default false")
    private Boolean required;

    @Transient
    @JsonSerialize
    private Integer nbrDoc;

    @Transient
    private Integer status;

    public EbTypeDocuments() {
    }

    public EbTypeDocuments(
        String nom,
        String code,
        Integer ordre,
        List<Integer> module,
        String listModuleStr,
        EbUser ebTypeDocumentsebUser,
        Integer xEbCompagnie,
        Boolean activated,
        Boolean isexpectedDoc,
        Integer nbrDoc) {
        super();
        this.ebTypeDocumentsNum = null;
        this.nom = nom;
        this.code = code;
        this.ordre = ordre;
        this.module = module;
        this.xEbCompagnie = xEbCompagnie;
        this.activated = activated;
        this.isexpectedDoc = isexpectedDoc;
        this.nbrDoc = nbrDoc;
        this.listModuleStr = listModuleStr;
    }

    public EbTypeDocuments(EbTypeDocuments typeDoc) {
        super();
        this.ebTypeDocumentsNum = typeDoc.getEbTypeDocumentsNum();
        this.nom = typeDoc.getNom();
        this.code = typeDoc.getCode();
        this.ordre = typeDoc.getOrdre();
        this.module = typeDoc.getModule();
        this.listModuleStr = typeDoc.getListModuleStr();
        this.xEbCompagnie = typeDoc.getxEbCompagnie();
        this.activated = typeDoc.getActivated();
        this.isexpectedDoc = typeDoc.getIsexpectedDoc();
        this.nbrDoc = typeDoc.getNbrDoc();
    }

    public Integer getEbTypeDocumentsNum() {
        return ebTypeDocumentsNum;
    }

    public void setEbTypeDocumentsNum(Integer ebTypeDocumentsNum) {
        this.ebTypeDocumentsNum = ebTypeDocumentsNum;
    }

    public Integer getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(Integer xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public String getListModuleStr() {
        return listModuleStr;
    }

    public void setListModuleStr(String listModuleStr) {
        this.listModuleStr = listModuleStr;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getOrdre() {
        return ordre;
    }

    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    public List<Integer> getModule() {
        return module;
    }

    public void setModule(List<Integer> module) {
        this.module = module;
    }

    public Boolean getIsexpectedDoc() {
        return isexpectedDoc;
    }

    public void setIsexpectedDoc(Boolean isexpectedDoc) {
        this.isexpectedDoc = isexpectedDoc;
    }

    public Integer getNbrDoc() {
        return nbrDoc;
    }

    public void setNbrDoc(Integer nbrDoc) {
        this.nbrDoc = nbrDoc;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }
}
