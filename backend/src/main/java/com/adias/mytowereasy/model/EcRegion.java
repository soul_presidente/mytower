/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "ec_region", schema = "work")
@JsonIgnoreProperties({
    "hibernateLazyInitializer", "handler"
})
public class EcRegion implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ec_region_num")
    private Integer ecRegionNum;

    @Column(name = "code")
    private String code;

    @Column(name = "libelle")
    private String libelle;

    public Integer getEcRegionNum() {
        return ecRegionNum;
    }

    public void setEcRegionNum(Integer ecRegionNum) {
        this.ecRegionNum = ecRegionNum;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((ecRegionNum == null) ? 0 : ecRegionNum.hashCode());
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EcRegion other = (EcRegion) obj;

        if (code == null) {
            if (other.code != null) return false;
        }
        else if (!code.equals(other.code)) return false;

        if (ecRegionNum == null) {
            if (other.ecRegionNum != null) return false;
        }
        else if (!ecRegionNum.equals(other.ecRegionNum)) return false;

        if (libelle == null) {
            if (other.libelle != null) return false;
        }
        else if (!libelle.equals(other.libelle)) return false;

        return true;
    }
}
