/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.tt;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.TtEnumeration;
import com.adias.mytowereasy.model.TtEnumeration.TtPsl;
import com.adias.mytowereasy.util.ListObjectDeserializer;
import com.adias.mytowereasy.util.ObjectDeserializer;


@NamedEntityGraphs({
    @NamedEntityGraph(name = "graph.EbTtSchemaPsl.xEbCompagnie", attributeNodes = @NamedAttributeNode("xEbCompagnie")),
})
@Entity
@Table(name = "eb_tt_schema_psl", indexes = {
    @Index(name = "idx_eb_tt_schema_psl_x_eb_compagnie", columnList = "x_eb_compagnie")
})
@JsonDeserialize(using = ObjectDeserializer.class)
public class EbTtSchemaPsl implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebTtSchemaPslNum;

    private String configPsl;
    // chaine de code psl separes par espace/tabulation ou autre separateur: PIC
    // DEP
    // ARR CUS DEL

    private Integer leadtime; // jours
    private Integer xEcModeTransport;

    private String designation;

    private String listEtablissement;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", updatable = false)
    private EbCompagnie xEbCompagnie;

    private String listModeTransport;

    private Boolean isEditable;

    private Boolean isDeleted;

    @Column(insertable = true, updatable = false)
    @ColumnDefault("now()")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dateCreation;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonDeserialize(converter = ListObjectDeserializer.class)
    private List<EbTtCompanyPsl> listPsl;

    private String xEcIncotermLibelle;

    private String code;

    public EbTtSchemaPsl() {
    }

    public EbTtSchemaPsl(EbTtSchemaPsl schema) {
        this.ebTtSchemaPslNum = schema.getEbTtSchemaPslNum();
        this.configPsl = schema.getConfigPsl();
        this.leadtime = schema.getLeadtime();
        this.designation = schema.getDesignation();
        this.xEcModeTransport = schema.getxEcModeTransport();
        this.listModeTransport = schema.getListModeTransport();
        this.xEcIncotermLibelle = schema.getxEcIncotermLibelle();
        this.listEtablissement = schema.getListEtablissement();
        this.isEditable = schema.getIsEditable();
        this.isDeleted = schema.getIsDeleted();
        this.xEbCompagnie = schema.getxEbCompagnie();
        this.listPsl = ObjectDeserializer
            .cloneJsonListObject(schema.getListPsl(), new TypeToken<List<EbTtCompanyPsl>>() {
            }.getType());
    }

    public EbTtSchemaPsl(
        String configPsl,
        String listModeTransport,
        String xEcIncotermLibelle,
        EbCompagnie xEbCompagnie) {
        this.configPsl = configPsl;
        this.listModeTransport = listModeTransport;
        this.xEcIncotermLibelle = xEcIncotermLibelle;
        this.designation = xEcIncotermLibelle;
        this.xEbCompagnie = xEbCompagnie;
    }

    public EbTtSchemaPsl(Integer ebTtSchemaPslNum) {
        this.ebTtSchemaPslNum = ebTtSchemaPslNum;
    }

    @QueryProjection
    public EbTtSchemaPsl(Integer ebTtSchemaPslNum, String configPsl, Integer xEcModeTransport) {
        this.ebTtSchemaPslNum = ebTtSchemaPslNum;
        this.configPsl = configPsl;
        this.xEcModeTransport = xEcModeTransport;
    }

    // geter and setter
    public String getConfigPsl() {
        return configPsl;
    }

    public void setConfigPsl(String configPsl) {
        this.configPsl = configPsl;
    }

    public Integer getLeadtime() {
        return leadtime;
    }

    public void setLeadtime(Integer leadtime) {
        this.leadtime = leadtime;
    }

    public Integer getxEcModeTransport() {
        return xEcModeTransport;
    }

    public void setxEcModeTransport(Integer xEcModeTransport) {
        this.xEcModeTransport = xEcModeTransport;
    }

    // Method
    public static List<TtEnumeration.TtPsl> recupPSL(String pslConfig) {
        String[] arrIncoterms = pslConfig != null ? pslConfig.split("[|]") : null;

        List<TtPsl> listIncoterms = new ArrayList<TtPsl>();

        if (arrIncoterms != null && arrIncoterms.length > 0) {

            for (String it: arrIncoterms) {
                TtPsl inco = TtPsl.getPslByCodeAlpha(it);
                if (inco != null) listIncoterms.add(inco);
            }

            if (!listIncoterms.isEmpty()) {
                listIncoterms.sort((it1, it2) -> {
                    if (it1.getOrdre() > it2.getOrdre()) return 1;
                    if (it1.getOrdre() < it2.getOrdre()) return -1;
                    return 0;
                });
            }

        }

        return listIncoterms;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Integer getEbTtSchemaPslNum() {
        return ebTtSchemaPslNum;
    }

    public void setEbTtSchemaPslNum(Integer ebTtSchemaPslNum) {
        this.ebTtSchemaPslNum = ebTtSchemaPslNum;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getListModeTransport() {
        return listModeTransport;
    }

    public void setListModeTransport(String listModeTransport) {
        this.listModeTransport = listModeTransport;
    }

    public Boolean getIsEditable() {
        return isEditable;
    }

    public void setIsEditable(Boolean isEditable) {
        this.isEditable = isEditable;
    }

    public String getListEtablissement() {
        return listEtablissement;
    }

    public void setListEtablissement(String listEtablissement) {
        this.listEtablissement = listEtablissement;
    }

    public String getxEcIncotermLibelle() {
        return xEcIncotermLibelle;
    }

    public void setxEcIncotermLibelle(String xEcIncotermLibelle) {
        this.xEcIncotermLibelle = xEcIncotermLibelle;
    }

    public List<EbTtCompanyPsl> getListPsl() {
        listPsl = ObjectDeserializer.checkJsonListObject(listPsl, new TypeToken<List<EbTtCompanyPsl>>() {
        }.getType());
        return listPsl;
    }

    public void setListPsl(List<EbTtCompanyPsl> listPsl) {
        this.listPsl = listPsl;
    }

    public static void sortListPsl(List<EbTtCompanyPsl> listPsl) {
        listPsl.sort((it1, it2) -> {
            if (it1.getOrder() == null) it1.setOrder(listPsl.size() - 1);
            if (it2.getOrder() == null) it2.setOrder(listPsl.size() - 1);
            return it1.getOrder() - it2.getOrder();
        });
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
