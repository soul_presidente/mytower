package com.adias.mytowereasy.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.querydsl.core.annotations.QueryProjection;


@Entity
@Table(name = "eb_type_goods")
public class EbTypeGoods {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebTypeGoodsNum;
    private String code;
    private String label;
    @ColumnDefault("false")
    private Boolean deleted;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company", insertable = true)
    private EbCompagnie xEbCompany;

    public EbTypeGoods() {
    }

    @QueryProjection
    public EbTypeGoods(Integer ebTypeGoodsNum) {
        this.ebTypeGoodsNum = ebTypeGoodsNum;
    }

    @QueryProjection
    public EbTypeGoods(
        Integer ebTypeGoodsNum,
        String code,
        String label,
        Integer ebCompagnieNum,
        String ebCompagnieNom) {
        this.ebTypeGoodsNum = ebTypeGoodsNum;
        this.code = code;
        this.label = label;

        if (ebCompagnieNum != null) {
            this.xEbCompany = new EbCompagnie();
            this.xEbCompany.setEbCompagnieNum(ebCompagnieNum);
            this.xEbCompany.setNom(ebCompagnieNom);
        }

    }

    public Integer getEbTypeGoodsNum() {
        return ebTypeGoodsNum;
    }

    public void setEbTypeGoodsNum(Integer ebTypeGoodsNum) {
        this.ebTypeGoodsNum = ebTypeGoodsNum;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @JsonProperty
    public EbCompagnie getxEbCompany() {
        return xEbCompany;
    }

    @JsonIgnore
    public void setxEbCompany(EbCompagnie xEbCompany) {
        this.xEbCompany = xEbCompany;
    }
}
