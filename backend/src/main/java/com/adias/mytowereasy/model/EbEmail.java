/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@Entity
@Table(name = "eb_email", schema = "work")
public class EbEmail {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebEmailNum;

    @Column(columnDefinition = "TEXT")
    private String adresseFrom;

    @Column(columnDefinition = "TEXT")
    private String adresseTo;

    @Column(columnDefinition = "TEXT")
    private String adresseCc;

    @Column(columnDefinition = "TEXT")
    private String adresseCci;

    @Column(columnDefinition = "TEXT")
    private String subject;

    @Column(columnDefinition = "TEXT")
    private String message;

    private Integer status;

    @Column(columnDefinition = "TEXT")
    private String stackTrace;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    public Integer getEbEmailNum() {
        return ebEmailNum;
    }

    public void setEbEmailNum(Integer ebEmailNum) {
        this.ebEmailNum = ebEmailNum;
    }

    public String getAdresseFrom() {
        return adresseFrom;
    }

    public String getAdresseTo() {
        return adresseTo;
    }

    public String getAdresseCc() {
        return adresseCc;
    }

    public String getAdresseCci() {
        return adresseCci;
    }

    public String getSubject() {
        return subject;
    }

    public String getMessage() {
        return message;
    }

    public Integer getStatus() {
        return status;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setAdresseFrom(String adresseFrom) {
        this.adresseFrom = adresseFrom;
    }

    public void setAdresseTo(String adresseTo) {
        this.adresseTo = adresseTo;
    }

    public void setAdresseCc(String adresseCc) {
        this.adresseCc = adresseCc;
    }

    public void setAdresseCci(String adresseCci) {
        this.adresseCci = adresseCci;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public void setStackTrace(String stackTrace) {
        this.stackTrace = stackTrace;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((adresseCc == null) ? 0 : adresseCc.hashCode());
        result = prime * result + ((adresseCci == null) ? 0 : adresseCci.hashCode());
        result = prime * result + ((adresseFrom == null) ? 0 : adresseFrom.hashCode());
        result = prime * result + ((adresseTo == null) ? 0 : adresseTo.hashCode());
        result = prime * result + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        result = prime * result + ((message == null) ? 0 : message.hashCode());
        result = prime * result + ((stackTrace == null) ? 0 : stackTrace.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((subject == null) ? 0 : subject.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbEmail other = (EbEmail) obj;

        if (adresseCc == null) {
            if (other.adresseCc != null) return false;
        }
        else if (!adresseCc.equals(other.adresseCc)) return false;

        if (adresseCci == null) {
            if (other.adresseCci != null) return false;
        }
        else if (!adresseCci.equals(other.adresseCci)) return false;

        if (adresseFrom == null) {
            if (other.adresseFrom != null) return false;
        }
        else if (!adresseFrom.equals(other.adresseFrom)) return false;

        if (adresseTo == null) {
            if (other.adresseTo != null) return false;
        }
        else if (!adresseTo.equals(other.adresseTo)) return false;

        if (dateCreation == null) {
            if (other.dateCreation != null) return false;
        }
        else if (!dateCreation.equals(other.dateCreation)) return false;

        if (message == null) {
            if (other.message != null) return false;
        }
        else if (!message.equals(other.message)) return false;

        if (stackTrace == null) {
            if (other.stackTrace != null) return false;
        }
        else if (!stackTrace.equals(other.stackTrace)) return false;

        if (status == null) {
            if (other.status != null) return false;
        }
        else if (!status.equals(other.status)) return false;

        if (subject == null) {
            if (other.subject != null) return false;
        }
        else if (!subject.equals(other.subject)) return false;

        return true;
    }
}
