package com.adias.mytowereasy.model;

import java.util.List;


public class TransfertAction {
    private Integer action;
    private List<Integer> listId;

    public TransfertAction() {
    }

    public Integer getAction() {
        return action;
    }

    public void setAction(Integer action) {
        this.action = action;
    }

    public List<Integer> getListId() {
        return listId;
    }

    public void setListId(List<Integer> listId) {
        this.listId = listId;
    }
}
