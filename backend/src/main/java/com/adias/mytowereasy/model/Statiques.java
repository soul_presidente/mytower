/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;


@Component
public class Statiques {
    public static final String DATE_FORMAT = "dd-MM-yyyy";
    public static final String DATE_FORMAT1 = "dd/MM/yyyy";
    public static final String DATE_FORMAT_ORDER = "yyyy-MM-dd";
    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(DATE_FORMAT_ORDER);
    public static final SimpleDateFormat DATE_FORMATTER1 = new SimpleDateFormat(DATE_FORMAT1);
    public static final String SCHEMA = "work.";
    public static final String SHIPTO_CATEGORIE = "SHIPTO";
    public static final String CODE_CPB = "CPB";
    public static final String WAITING_FOR_ALLOCATION_ETABLISSEMENT_NIC = "90001";
    public static final Integer UNKNOWN_USER_NUM = -2;
    public static final String UNKNOWN_PARAM = "unknownUser";

    public static String toBase36(Integer value) {
        return Long.toString(value, 36);
    }

    public static <T> T clone(Object obj, Class<T> _class) {
        ObjectMapper mapper = new ObjectMapper();
        T result = null;

        try {
            String jsonSource = mapper.writeValueAsString(obj);
            result = mapper.readValue(jsonSource, _class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String convertSubDateToEngFormat(String value) {

        // dd/mm/yyyy => yyyy-mm-dd
        if (value == null || value.isEmpty() || !Pattern.compile("[0-9]*/?[0-9]*/?[0-9]*").matcher(value).matches()) {

            if (value != null && !value.isEmpty()) {

                try {
                    Date dt = Statiques.DATE_FORMATTER1.parse(value);
                    if (dt != null) return Statiques.DATE_FORMATTER1.format(dt);
                } catch (Exception e) {
                }

            }
            else return null;

        }

        String str = "";
        String[] parts = value.split("/");

        int i;

        for (i = parts.length - 1; i >= 0; i--) {
            if (str.length() > 0) str += "-";

            if (i < 2 || parts[i].length() == 4) {
                str += parts[i];
            }

        }

        return str;
    }

    public static boolean checkFileExtentions(MultipartFile file, List<String> extensionList) {
        String fileExtension = new String();

        if (!file.isEmpty()) {
            fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
        }

        return extensionList.contains(fileExtension);
    }

    public static boolean checkFileMimeType(MultipartFile file, List<String> mimeList) {
        String fileContentType = new String();

        if (!file.isEmpty()) {
            fileContentType = file.getContentType();
        }

        return mimeList.contains(fileContentType);
    }
}
