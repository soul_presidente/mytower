package com.adias.mytowereasy.model;

import javax.persistence.*;

@Entity
@Table(indexes = {
        @Index(name = "idx_demande_user_observers_demande", columnList = "tr_id"),
        @Index(name = "idx_demande_user_observers_observer", columnList = "user_id"),
})
public class DemandeUserObservers {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tr_id")
    private EbDemande demande;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private EbUser observer;

    private String observerName;
    @Column(nullable = false)
    private String observerEmail;
    private String observerPrenom;

    public DemandeUserObservers(EbDemande ebDemande, EbUser user) {
        this.demande = new EbDemande(ebDemande.getEbDemandeNum());
        this.observer = new EbUser(user.getEbUserNum());
        this.observerEmail = user.getEmail();
        this.observerName = user.getNom();
        this.observerPrenom = user.getPrenom();
    }

    public DemandeUserObservers() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public EbDemande getDemande() {
        return demande;
    }

    public void setDemande(EbDemande demande) {
        this.demande = demande;
    }

    public EbUser getObserver() {
        return observer;
    }

    public void setObserver(EbUser observer) {
        this.observer = observer;
    }

    public String getObserverName() {
        return observerName;
    }

    public void setObserverName(String observerName) {
        this.observerName = observerName;
    }

    public String getObserverEmail() {
        return observerEmail;
    }

    public void setObserverEmail(String observerEmail) {
        this.observerEmail = observerEmail;
    }

    public String getObserverPrenom() {
        return observerPrenom;
    }

    public void setObserverPrenom(String observerPrenom) {
        this.observerPrenom = observerPrenom;
    }
}
