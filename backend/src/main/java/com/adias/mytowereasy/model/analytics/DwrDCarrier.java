/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.analytics;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "dwr_d_carrier")
public class DwrDCarrier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dwr_d_carrier_num")
    private Integer dwrDCarrierNum;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "carrier", cascade = CascadeType.ALL)
    private List<DwrFShipments> shipments = new ArrayList<DwrFShipments>();

    @OneToMany(mappedBy = "carrier", cascade = CascadeType.ALL)
    private List<DwrFShipmentsIncident> shipmentsIncident = new ArrayList<DwrFShipmentsIncident>();

    public Integer getID() {
        return this.dwrDCarrierNum;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public List<DwrFShipments> getShipments() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.shipments;
    }

    public void setShipments(List<DwrFShipments> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.shipments = value;
    }

    public List<DwrFShipmentsIncident> getShipmentsIncident() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.shipmentsIncident;
    }

    public void setShipmentsIncident(List<DwrFShipmentsIncident> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.shipmentsIncident = value;
    }
}
