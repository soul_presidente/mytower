/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.custom;

import javax.persistence.*;


@Table(name = "eb_form_view")
@Entity
public class EbFormView {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebFormViewNum;

    private String ebFormView;

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "xEbFormZone", insertable = true, updatable = false)
    private EbFormZone xEbFormZone;

    public EbFormView(Integer xEbViewForm, String xEbViewFormName) {
        this.ebFormViewNum = xEbViewForm;
        this.ebFormView = xEbViewFormName;
        this.xEbFormZone = new EbFormZone();
    }

    public EbFormView() {
    }

    public Integer getEbFormViewNum() {
        return ebFormViewNum;
    }

    public void setEbFormViewNum(Integer ebFormViewNum) {
        this.ebFormViewNum = ebFormViewNum;
    }

    public String getEbFormView() {
        return ebFormView;
    }

    public void setEbFormView(String ebFormView) {
        this.ebFormView = ebFormView;
    }

    public EbFormZone getxEbFormZone() {
        return xEbFormZone;
    }

    public void setxEbFormZone(EbFormZone xEbFormZone) {
        this.xEbFormZone = xEbFormZone;
    }
}
