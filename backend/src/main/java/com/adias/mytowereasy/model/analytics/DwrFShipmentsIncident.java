/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.analytics;

import javax.persistence.*;


@Entity
@Table(name = "dwr_f_shipments_incident")
public class DwrFShipmentsIncident {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dwr_f_shipments_incident_num")
    private long dwrFShipmentsIncident;

    @Column(name = "nb_expeditions")
    private int nbExpeditions;

    @Column(name = "total_cost")
    private float totalCost;

    @Column(name = "total_weight")
    private float totalWeight;

    @ManyToOne
    @JoinColumn(name = "incident_id")
    private DwrDIncidentCategory incident;

    @ManyToOne
    @JoinColumn(name = "date_initiated_id")
    private DwrDMonth dateInitiated;

    @ManyToOne
    @JoinColumn(name = "date_pickup_id")
    private DwrDMonth datePickup;

    @ManyToOne
    @JoinColumn(name = "date_delivered_id")
    private DwrDMonth dateDelivered;

    @ManyToOne
    @JoinColumn(name = "carrier_id")
    private DwrDCarrier carrier;

    @ManyToOne
    @JoinColumn(name = "charger_id")
    private DwrDCharger charger;

    public long getID() {
        return this.dwrFShipmentsIncident;
    }

    public int getNbExpeditions() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.nbExpeditions;
    }

    public void setNbExpeditions(int value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.nbExpeditions = value;
    }

    public float getTotalCost() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.totalCost;
    }

    public void setTotalCost(float value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.totalCost = value;
    }

    public float getTotalWeight() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.totalWeight;
    }

    public void setTotalWeight(float value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.totalWeight = value;
    }

    public DwrDIncidentCategory getIncident() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.incident;
    }

    public void setIncident(DwrDIncidentCategory value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.incident = value;
    }

    public DwrDMonth getDateInitiated() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.dateInitiated;
    }

    public void setDateInitiated(DwrDMonth value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.dateInitiated = value;
    }

    public DwrDMonth getDatePickup() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.datePickup;
    }

    public void setDatePickup(DwrDMonth value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.datePickup = value;
    }

    public DwrDMonth getDateDelivered() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.dateDelivered;
    }

    public void setDateDelivered(DwrDMonth value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.dateDelivered = value;
    }

    public DwrDCharger getCharger() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.charger;
    }

    public void setCharger(DwrDCharger value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.charger = value;
    }

    public DwrDCarrier getCarrier() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.carrier;
    }

    public void setCarrier(DwrDCarrier value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.carrier = value;
    }
}
