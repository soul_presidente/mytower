/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.dto.EbTtCompagnyPslLightDto;
import com.adias.mytowereasy.trpl.model.EbPlCostItem;
import com.adias.mytowereasy.types.JsonBinaryType;
import com.adias.mytowereasy.util.JsonDateDeserializer;
import com.adias.mytowereasy.util.ObjectDeserializer;


@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Table(name = "eb_demande_quote", schema = "work")
public class ExEbDemandeTransporteur implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer exEbDemandeTransporteurNum;

    @Temporal(TemporalType.TIMESTAMP)
    private Date pickupTime;

    @Temporal(TemporalType.TIMESTAMP)

    private Date deliveryTime;

    private Integer transitTime;

    private BigDecimal price;

    private Double priceReal;

    @Column
    private Boolean exchangeRateFound;

    @Column
    private Boolean isChecked;

    @Column(columnDefinition = "TEXT")
    private String comment;

    private Integer status;

    private Integer leadTimeDeparture;

    @Column
    private Boolean isFromTransPlan;
    @Column
    private String refPlan;
    private String refGrille;
    private Integer xEcModeTransport;

	@Column(columnDefinition = "boolean default false")
	private boolean horsGrille;

    public Integer getxEcModeTransport() {
        return xEcModeTransport;
    }

    public void setxEcModeTransport(Integer xEcModeTransport) {
        this.xEcModeTransport = xEcModeTransport;
    }

    @ManyToOne
    private EcCountry xEcCountryDeparture;
    private String cityDeparture;

    private Integer leadTimeArrival;
    @ManyToOne
    private EcCountry xEcCountryArrival;
    private String cityArrival;

    private Integer leadTimePickup;
    @ManyToOne
    private EcCountry xEcCountryPickup;
    private String cityPickup;

    private Integer leadTimeCustoms;
    @ManyToOne
    private EcCountry xEcCountryCustoms;
    private String cityCustoms;

    private Integer leadTimeDelivery;
    @ManyToOne
    private EcCountry xEcCountryDelivery;
    private String cityDelivery;

    @Transient
    private String company_name;

    private String emailTransporteur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_ct")
    private EbUser xEbUserCt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_confirm_by")
    private EbUser xEbUserConfirmBy;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_demande", updatable = false)
    private EbDemande xEbDemande;

    @ManyToOne
    @JoinColumn(name = "x_transporteur", nullable = false, updatable = false)
    private EbUser xTransporteur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement", updatable = false, nullable = false)
    private EbEtablissement xEbEtablissement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", updatable = false, nullable = false)
    private EbCompagnie xEbCompagnie;

    @ManyToOne
    @JoinColumn(name = "x_eb_compagnie_currency")
    private EbCompagnieCurrency xEbCompagnieCurrency;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCostCategorie> listCostCategorie;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbPlCostItem> listPlCostItem;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String strListCostCategorie;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbTtCompagnyPslLightDto> listEbTtCompagniePsl;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Boolean ctCommunity;

    @Column(columnDefinition = "TEXT")
    private String exchangeRate;

    @Column(columnDefinition = "TEXT")
    private String infosForCustomsClearance;

    public ExEbDemandeTransporteur() {
    }

    public ExEbDemandeTransporteur(
        Integer exdtXTransporteurEbUserNum,
        Date exdtPickupTime,
        Date exdtDeliveryTime,
        Integer exdtTransitTime,
        BigDecimal exdtPrice,
        Integer exdtStatus,
        Integer dEbDemandeNum,
        String refTransport,
        Integer trEbUserNum,
        String trNom,
        String trPrenom,
        String trEmail,
        Integer etEbEtablissementNum,
        String etNom,
        Integer cmpEbCompagnieNum,
        String cmpNom,
        String cmpCode) {
        this.exEbDemandeTransporteurNum = exdtXTransporteurEbUserNum;
        this.pickupTime = exdtPickupTime;
        this.deliveryTime = exdtDeliveryTime;
        this.transitTime = exdtTransitTime;
        this.price = exdtPrice;
        this.status = exdtStatus;
        this.setxEbDemande(new EbDemande(dEbDemandeNum, refTransport));
        this.setxTransporteur(new EbUser(trEbUserNum, trNom, trPrenom, trEmail, null));
        this.setxEbEtablissement(new EbEtablissement(etEbEtablissementNum, etNom));
        this.setxEbCompagnie(new EbCompagnie(cmpEbCompagnieNum, cmpNom, cmpCode));
    }

    public ExEbDemandeTransporteur(Integer ebUserNum, Integer ebDemandeNum, Integer status) {
        this.xEbDemande = new EbDemande(ebDemandeNum);
        this.xTransporteur = new EbUser(ebUserNum);
        this.status = status;
    }

    public ExEbDemandeTransporteur(
        Integer exEbDemandeTransporteurNum,
        Integer ebUserNum,
        Integer ebDemandeNum,
        Integer status,
        Object listEbTtCompagniePsl,
        Date pickupTime,
        Date deliveryTime) {
        this.exEbDemandeTransporteurNum = exEbDemandeTransporteurNum;
        this.xTransporteur = new EbUser(ebUserNum);
        this.xEbDemande = new EbDemande(ebDemandeNum);
        this.status = status;
        this.pickupTime = pickupTime;
        this.deliveryTime = deliveryTime;

        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDateDeserializer()).create();

        String json = gson.toJson(listEbTtCompagniePsl);
        this.listEbTtCompagniePsl = gson.fromJson(json, new TypeToken<List<EbTtCompagnyPslLightDto>>() {
        }.getType());
    }

    public ExEbDemandeTransporteur(EbDemande ebDemande, EbUser transporteur) {
        this.xEbDemande = ebDemande;
        this.xTransporteur = transporteur;
    }

    public ExEbDemandeTransporteur(ExEbDemandeTransporteur exEbDemandeTransporteur) {
        this.exEbDemandeTransporteurNum = exEbDemandeTransporteur.getExEbDemandeTransporteurNum();
        this.pickupTime = exEbDemandeTransporteur.getPickupTime();
        this.deliveryTime = exEbDemandeTransporteur.getDeliveryTime();
        this.transitTime = exEbDemandeTransporteur.getTransitTime();
        this.price = exEbDemandeTransporteur.getPrice();
        this.comment = exEbDemandeTransporteur.getComment();
        this.status = exEbDemandeTransporteur.getStatus();
        this.leadTimeDeparture = exEbDemandeTransporteur.getLeadTimeDeparture();
        this.xEcCountryDeparture = exEbDemandeTransporteur.getxEcCountryDeparture();
        this.cityDeparture = exEbDemandeTransporteur.getCityDeparture();
        this.leadTimeArrival = exEbDemandeTransporteur.getLeadTimeArrival();
        this.xEcCountryArrival = exEbDemandeTransporteur.getxEcCountryArrival();
        this.cityArrival = exEbDemandeTransporteur.getCityArrival();
        this.leadTimePickup = exEbDemandeTransporteur.getLeadTimePickup();
        this.xEcCountryPickup = exEbDemandeTransporteur.getxEcCountryPickup();
        this.cityPickup = exEbDemandeTransporteur.getCityPickup();
        this.leadTimeCustoms = exEbDemandeTransporteur.getLeadTimeCustoms();
        this.xEcCountryCustoms = exEbDemandeTransporteur.getxEcCountryCustoms();
        this.cityCustoms = exEbDemandeTransporteur.getCityCustoms();
        this.leadTimeDelivery = exEbDemandeTransporteur.getLeadTimeDelivery();
        this.xEcCountryDelivery = exEbDemandeTransporteur.getxEcCountryDelivery();
        this.cityDelivery = exEbDemandeTransporteur.getCityDelivery();
        this.company_name = exEbDemandeTransporteur.getCompany_name();
        this.xEbEtablissement = new EbEtablissement(
            exEbDemandeTransporteur.getxEbEtablissement().getEbEtablissementNum());
        this.xEbCompagnieCurrency = exEbDemandeTransporteur.getxEbCompagnieCurrency();

        if (exEbDemandeTransporteur.getxEbDemande() != null) {
            this.xEbDemande = new EbDemande(exEbDemandeTransporteur.getxEbDemande().getEbDemandeNum());
        }

        this.xTransporteur = new EbUser(exEbDemandeTransporteur.getxTransporteur());

        this.exchangeRate = exEbDemandeTransporteur.exchangeRate;
        this.infosForCustomsClearance = exEbDemandeTransporteur.infosForCustomsClearance;
    }

    @QueryProjection
    public ExEbDemandeTransporteur(String nom, String prenom) {
        this.xTransporteur = new EbUser();

        if (nom != null) {
            this.xTransporteur.setNom(nom);
        }

        if (prenom != null) {
            this.xTransporteur.setPrenom(prenom);
        }

    }

    @QueryProjection
    public ExEbDemandeTransporteur(
        Integer exEbDemandeTransporteurNum,
        Integer numTransporteur,
        String nomTransporteur,
        String prenomTransporteur,
        String emailTransporteur,
        Integer transitTime,
        BigDecimal price,
        String codeCompany,
        Integer statut,
        Integer ebEtablissementNum,
        Integer ebCompanyNum,
        String nomEtablissement,
        String nomCompany,
        Date pickupTime,
        Date deliveryTime,
        Integer exEtablissementCurrencyNum,
        String ecCurrencyCode,
        List<EbCostCategorie> listCostCategorie,
        Boolean isFromTransPlan,
        String refPlan,
        Boolean exchangeRateFound,
        Integer ebDemandeNum,
        Integer roleTransporteur,
        String exchangeRate,
        String infosForCustomsClearance,
        List<EbTtCompagnyPslLightDto> listEbTtCompagniePsl,
        List<EbPlCostItem> listPlCostItem,
        EbCompagnie compagnie,
			Boolean isChecked, boolean horsGrille) {
        this.exEbDemandeTransporteurNum = exEbDemandeTransporteurNum;

        this.xEbEtablissement = new EbEtablissement(ebEtablissementNum, nomEtablissement);
        this.xEbEtablissement.setEbCompagnie(new EbCompagnie(ebCompanyNum, nomCompany, codeCompany));
        this.xEbCompagnieCurrency = new EbCompagnieCurrency(exEtablissementCurrencyNum);
        this.xEbCompagnieCurrency.setEcCurrency(new EcCurrency(null, ecCurrencyCode));

        this.xTransporteur = new EbUser();
        this.xTransporteur.setEbUserNum(numTransporteur);
        this.xTransporteur.setNom(nomTransporteur);
        this.xTransporteur.setPrenom(prenomTransporteur);
        this.xTransporteur.setEmail(emailTransporteur);
        this.xTransporteur.setRole(roleTransporteur);

        this.transitTime = transitTime;
        this.price = price;
        this.status = statut;
        this.pickupTime = pickupTime;
        this.deliveryTime = deliveryTime;
        this.listCostCategorie = listCostCategorie;
        this.emailTransporteur = emailTransporteur;

        this.xEbDemande = new EbDemande(ebDemandeNum);

        this.exchangeRate = exchangeRate;
        this.infosForCustomsClearance = infosForCustomsClearance;
        this.listEbTtCompagniePsl = listEbTtCompagniePsl;
        this.listPlCostItem = listPlCostItem;
        this.xEbCompagnie = compagnie;
        this.isChecked = isChecked;
		this.horsGrille = horsGrille;
    }

    // for cotation
    @QueryProjection
    public ExEbDemandeTransporteur(
        Integer numTransporteur,
        String nomTransporteur,
        String prenomTransporteur,
        String emailTransporteur,
        Integer transitTime,
        BigDecimal price,
        String codeCompany,
        Integer statut,
        Integer ebEtablissementNum,
        Integer ebCompanyNum,
        String nomEtablissement,
        String nomCompany,
        Integer exEbDemandeTransporteurNum,
        Date pickupTime,
        Date deliveryTime,
        Integer leadTimeDeparture,
        String cityDeparture,
        EcCountry xEcCountryDeparture,
        Integer leadTimeDelivery,
        String cityDelivery,
        EcCountry xEcCountryDelivery,
        Integer leadTimePickup,
        String cityPickup,
        EcCountry xEcCountryPickup,
        Integer leadTimeCustoms,
        String cityCustoms,
        EcCountry xEcCountryCustoms,
        Integer leadTimeArrival,
        String cityArrival,
        EcCountry xEcCountryArrival,
        List<EbCostCategorie> listCostCategorie,
        String comment,
        Integer xEbUserCtNum,
        Boolean isFromTransPlan,
        String refPlan,
        String refGrille,
        Boolean exchangeRateFound,
        Integer ebDemandeNum,
        Integer roleTransporteur,
        Integer statusTransporteur,
        String exchangeRate,
        String infosForCustomsClearance,
        List<EbTtCompagnyPslLightDto> listEbTtCompagniePsl,
        Integer xEcModeTransport,
        List<EbPlCostItem> listPlCostItem,
        EbCompagnie compagnie,
			Boolean isChecked, boolean horsGrille) {
        this.exEbDemandeTransporteurNum = exEbDemandeTransporteurNum;

        this.xEbEtablissement = new EbEtablissement(ebEtablissementNum, nomEtablissement);
        this.xEbEtablissement.setEbCompagnie(new EbCompagnie(ebCompanyNum, nomCompany, codeCompany));

        this.xTransporteur = new EbUser();
        this.xTransporteur.setEbUserNum(numTransporteur);
        this.xTransporteur.setNom(nomTransporteur);
        this.xTransporteur.setPrenom(prenomTransporteur);
        this.xTransporteur.setEmail(emailTransporteur);
        this.xTransporteur.setRole(roleTransporteur);
        this.xTransporteur.setStatus(statusTransporteur);

        this.transitTime = transitTime;
        this.price = price;
        this.status = statut;
        this.pickupTime = pickupTime;
        this.deliveryTime = deliveryTime;

        this.cityDeparture = cityDeparture;
        this.leadTimeDeparture = leadTimeDeparture;
        this.xEcCountryDeparture = xEcCountryDeparture;

        this.cityArrival = cityArrival;
        this.leadTimeArrival = leadTimeArrival;
        this.xEcCountryArrival = xEcCountryArrival;

        this.leadTimePickup = leadTimePickup;
        this.cityPickup = cityPickup;
        this.xEcCountryPickup = xEcCountryPickup;

        this.cityCustoms = cityCustoms;
        this.leadTimeCustoms = leadTimeCustoms;
        this.xEcCountryCustoms = xEcCountryCustoms;

        this.leadTimeDelivery = leadTimeDelivery;
        this.cityDelivery = cityDelivery;
        this.xEcCountryDelivery = xEcCountryDelivery;

        this.listCostCategorie = listCostCategorie;
        this.comment = comment;

        this.xEbUserCt = new EbUser(xEbUserCtNum);
        this.isFromTransPlan = isFromTransPlan;
        this.refPlan = refPlan;
        this.refGrille = refGrille;
        this.exchangeRateFound = exchangeRateFound;
        this.emailTransporteur = emailTransporteur;

        this.xEbDemande = new EbDemande(ebDemandeNum);

        this.exchangeRate = exchangeRate;
        this.infosForCustomsClearance = infosForCustomsClearance;

        this.listEbTtCompagniePsl = listEbTtCompagniePsl;
        this.xEcModeTransport = xEcModeTransport;
        this.listPlCostItem = listPlCostItem;
        this.xEbCompagnie = compagnie;
        this.isChecked = isChecked;
		this.horsGrille = horsGrille;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public Date getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(Date pickupTime) {
        this.pickupTime = pickupTime;
    }

    public Date getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Date deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(Boolean isChecked) {
        this.isChecked = isChecked;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public EbEtablissement getxEbEtablissement() {
        return xEbEtablissement;
    }

    public void setxEbEtablissement(EbEtablissement xEbEtablissement) {
        this.xEbEtablissement = xEbEtablissement;
    }

    public EcCountry getxEcCountryDeparture() {
        return xEcCountryDeparture;
    }

    public void setxEcCountryDeparture(EcCountry xEcCountryDeparture) {
        this.xEcCountryDeparture = xEcCountryDeparture;
    }

    public String getCityDeparture() {
        return cityDeparture;
    }

    public void setCityDeparture(String cityDeparture) {
        this.cityDeparture = cityDeparture;
    }

    public EcCountry getxEcCountryArrival() {
        return xEcCountryArrival;
    }

    public void setxEcCountryArrival(EcCountry xEcCountryArrival) {
        this.xEcCountryArrival = xEcCountryArrival;
    }

    public String getCityArrival() {
        return cityArrival;
    }

    public void setCityArrival(String cityArrival) {
        this.cityArrival = cityArrival;
    }
    // NE JAMAIS UTILISER CE TRUC !!
    // @PrePersist
    // void prePersist()
    // {
    // this.status = StatutCarrier.RRE.getCode();
    // }

    public EcCountry getxEcCountryPickup() {
        return xEcCountryPickup;
    }

    public void setxEcCountryPickup(EcCountry xEcCountryPickup) {
        this.xEcCountryPickup = xEcCountryPickup;
    }

    public String getCityPickup() {
        return cityPickup;
    }

    public void setCityPickup(String cityPickup) {
        this.cityPickup = cityPickup;
    }

    public EcCountry getxEcCountryCustoms() {
        return xEcCountryCustoms;
    }

    public void setxEcCountryCustoms(EcCountry xEcCountryCustoms) {
        this.xEcCountryCustoms = xEcCountryCustoms;
    }

    public String getCityCustoms() {
        return cityCustoms;
    }

    public void setCityCustoms(String cityCustoms) {
        this.cityCustoms = cityCustoms;
    }

    public EcCountry getxEcCountryDelivery() {
        return xEcCountryDelivery;
    }

    public void setxEcCountryDelivery(EcCountry xEcCountryDelivery) {
        this.xEcCountryDelivery = xEcCountryDelivery;
    }

    public String getCityDelivery() {
        return cityDelivery;
    }

    public void setCityDelivery(String cityDelivery) {
        this.cityDelivery = cityDelivery;
    }

    public Integer getExEbDemandeTransporteurNum() {
        return exEbDemandeTransporteurNum;
    }

    public void setExEbDemandeTransporteurNum(Integer exEbDemandeTransporteurNum) {
        this.exEbDemandeTransporteurNum = exEbDemandeTransporteurNum;
    }

    public Integer getLeadTimeDeparture() {
        return leadTimeDeparture;
    }

    public void setLeadTimeDeparture(Integer leadTimeDeparture) {
        this.leadTimeDeparture = leadTimeDeparture;
    }

    public Integer getLeadTimeArrival() {
        return leadTimeArrival;
    }

    public void setLeadTimeArrival(Integer leadTimeArrival) {
        this.leadTimeArrival = leadTimeArrival;
    }

    public Integer getLeadTimeCustoms() {
        return leadTimeCustoms;
    }

    public void setLeadTimeCustoms(Integer leadTimeCustoms) {
        this.leadTimeCustoms = leadTimeCustoms;
    }

    public Integer getLeadTimePickup() {
        return leadTimePickup;
    }

    public void setLeadTimePickup(Integer leadTimePickup) {
        this.leadTimePickup = leadTimePickup;
    }

    public Integer getLeadTimeDelivery() {
        return leadTimeDelivery;
    }

    public void setLeadTimeDelivery(Integer leadTimeDelivery) {
        this.leadTimeDelivery = leadTimeDelivery;
    }

    public List<EbCostCategorie> getListCostCategorie() {
        return listCostCategorie;
    }

    public void setListCostCategorie(List<EbCostCategorie> listCostCategorie) {
        this.listCostCategorie = listCostCategorie;
    }

    public String getEmailTransporteur() {
        return emailTransporteur;
    }

    public void setEmailTransporteur(String emailTransporteur) {
        this.emailTransporteur = emailTransporteur;
    }

    public EbUser getxTransporteur() {
        return xTransporteur;
    }

    public void setxTransporteur(EbUser xTransporteur) {
        this.xTransporteur = xTransporteur;
    }

    public EbUser getxEbUserCt() {
        return xEbUserCt;
    }

    public void setxEbUserCt(EbUser xEbUserCt) {
        this.xEbUserCt = xEbUserCt;
    }

    public String getStrListCostCategorie() {
        return strListCostCategorie;
    }

    public void setStrListCostCategorie(String strListCostCategorie) {
        this.strListCostCategorie = strListCostCategorie;
    }

    public EbUser getxEbUserConfirmBy() {
        return xEbUserConfirmBy;
    }

    public void setxEbUserConfirmBy(EbUser xEbUserConfirmBy) {
        this.xEbUserConfirmBy = xEbUserConfirmBy;
    }

    public EbDemande getxEbDemande() {
        return xEbDemande;
    }

    public void setxEbDemande(EbDemande xEbDemande) {
        this.xEbDemande = xEbDemande;
    }

    public Boolean getCtCommunity() {
        return ctCommunity;
    }

    public void setCtCommunity(Boolean ctCommunity) {
        this.ctCommunity = ctCommunity;
    }

    public EbCompagnieCurrency getxEbCompagnieCurrency() {
        return xEbCompagnieCurrency;
    }

    public void setxEbCompagnieCurrency(EbCompagnieCurrency xEbCompagnieCurrency) {
        this.xEbCompagnieCurrency = xEbCompagnieCurrency;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Boolean getIsFromTransPlan() {
        return isFromTransPlan;
    }

    public void setIsFromTransPlan(Boolean isFromTransPlan) {
        this.isFromTransPlan = isFromTransPlan;
    }

    public String getRefPlan() {
        return refPlan;
    }

    public void setRefPlan(String refPlan) {
        this.refPlan = refPlan;
    }

    public Boolean getExchangeRateFound() {
        return exchangeRateFound;
    }

    public void setExchangeRateFound(Boolean exchangeRateFound) {
        this.exchangeRateFound = exchangeRateFound;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cityArrival == null) ? 0 : cityArrival.hashCode());
        result = prime * result + ((cityCustoms == null) ? 0 : cityCustoms.hashCode());
        result = prime * result + ((cityDelivery == null) ? 0 : cityDelivery.hashCode());
        result = prime * result + ((cityDeparture == null) ? 0 : cityDeparture.hashCode());
        result = prime * result + ((cityPickup == null) ? 0 : cityPickup.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((company_name == null) ? 0 : company_name.hashCode());
        result = prime * result + ((deliveryTime == null) ? 0 : deliveryTime.hashCode());
        result = prime * result + ((exEbDemandeTransporteurNum == null) ? 0 : exEbDemandeTransporteurNum.hashCode());
        result = prime * result + ((leadTimeArrival == null) ? 0 : leadTimeArrival.hashCode());
        result = prime * result + ((leadTimeCustoms == null) ? 0 : leadTimeCustoms.hashCode());
        result = prime * result + ((leadTimeDelivery == null) ? 0 : leadTimeDelivery.hashCode());
        result = prime * result + ((leadTimeDeparture == null) ? 0 : leadTimeDeparture.hashCode());
        result = prime * result + ((leadTimePickup == null) ? 0 : leadTimePickup.hashCode());
        result = prime * result + ((listCostCategorie == null) ? 0 : listCostCategorie.hashCode());
        result = prime * result + ((pickupTime == null) ? 0 : pickupTime.hashCode());
        result = prime * result + ((price == null) ? 0 : price.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + ((transitTime == null) ? 0 : transitTime.hashCode());
        result = prime * result + ((xEbDemande == null) ? 0 : xEbDemande.hashCode());
        result = prime * result + ((xEbEtablissement == null) ? 0 : xEbEtablissement.hashCode());
        result = prime * result + ((xEcCountryArrival == null) ? 0 : xEcCountryArrival.hashCode());
        result = prime * result + ((xEcCountryCustoms == null) ? 0 : xEcCountryCustoms.hashCode());
        result = prime * result + ((xEcCountryDelivery == null) ? 0 : xEcCountryDelivery.hashCode());
        result = prime * result + ((xEcCountryDeparture == null) ? 0 : xEcCountryDeparture.hashCode());
        result = prime * result + ((xEcCountryPickup == null) ? 0 : xEcCountryPickup.hashCode());
        result = prime * result + ((xEbCompagnieCurrency == null) ? 0 : xEbCompagnieCurrency.hashCode());
        result = prime * result + ((xTransporteur == null) ? 0 : xTransporteur.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        ExEbDemandeTransporteur other = (ExEbDemandeTransporteur) obj;

        if (cityArrival == null) {
            if (other.cityArrival != null) return false;
        }
        else if (!cityArrival.equals(other.cityArrival)) return false;

        if (cityCustoms == null) {
            if (other.cityCustoms != null) return false;
        }
        else if (!cityCustoms.equals(other.cityCustoms)) return false;

        if (cityDelivery == null) {
            if (other.cityDelivery != null) return false;
        }
        else if (!cityDelivery.equals(other.cityDelivery)) return false;

        if (cityDeparture == null) {
            if (other.cityDeparture != null) return false;
        }
        else if (!cityDeparture.equals(other.cityDeparture)) return false;

        if (cityPickup == null) {
            if (other.cityPickup != null) return false;
        }
        else if (!cityPickup.equals(other.cityPickup)) return false;

        if (comment == null) {
            if (other.comment != null) return false;
        }
        else if (!comment.equals(other.comment)) return false;

        if (company_name == null) {
            if (other.company_name != null) return false;
        }
        else if (!company_name.equals(other.company_name)) return false;

        if (deliveryTime == null) {
            if (other.deliveryTime != null) return false;
        }
        else if (!deliveryTime.equals(other.deliveryTime)) return false;

        if (exEbDemandeTransporteurNum == null) {
            if (other.exEbDemandeTransporteurNum != null) return false;
        }
        else if (!exEbDemandeTransporteurNum.equals(other.exEbDemandeTransporteurNum)) return false;

        if (leadTimeArrival == null) {
            if (other.leadTimeArrival != null) return false;
        }
        else if (!leadTimeArrival.equals(other.leadTimeArrival)) return false;

        if (leadTimeCustoms == null) {
            if (other.leadTimeCustoms != null) return false;
        }
        else if (!leadTimeCustoms.equals(other.leadTimeCustoms)) return false;

        if (leadTimeDelivery == null) {
            if (other.leadTimeDelivery != null) return false;
        }
        else if (!leadTimeDelivery.equals(other.leadTimeDelivery)) return false;

        if (leadTimeDeparture == null) {
            if (other.leadTimeDeparture != null) return false;
        }
        else if (!leadTimeDeparture.equals(other.leadTimeDeparture)) return false;

        if (leadTimePickup == null) {
            if (other.leadTimePickup != null) return false;
        }
        else if (!leadTimePickup.equals(other.leadTimePickup)) return false;

        if (listCostCategorie == null) {
            if (other.listCostCategorie != null) return false;
        }
        else if (!listCostCategorie.equals(other.listCostCategorie)) return false;

        if (pickupTime == null) {
            if (other.pickupTime != null) return false;
        }
        else if (!pickupTime.equals(other.pickupTime)) return false;

        if (price == null) {
            if (other.price != null) return false;
        }
        else if (!price.equals(other.price)) return false;

        if (status == null) {
            if (other.status != null) return false;
        }
        else if (!status.equals(other.status)) return false;

        if (transitTime == null) {
            if (other.transitTime != null) return false;
        }
        else if (!transitTime.equals(other.transitTime)) return false;

        if (xEbDemande == null) {
            if (other.xEbDemande != null) return false;
        }
        else if (!xEbDemande.equals(other.xEbDemande)) return false;

        if (xEbEtablissement == null) {
            if (other.xEbEtablissement != null) return false;
        }
        else if (!xEbEtablissement.equals(other.xEbEtablissement)) return false;

        if (xEcCountryArrival == null) {
            if (other.xEcCountryArrival != null) return false;
        }
        else if (!xEcCountryArrival.equals(other.xEcCountryArrival)) return false;

        if (xEcCountryCustoms == null) {
            if (other.xEcCountryCustoms != null) return false;
        }
        else if (!xEcCountryCustoms.equals(other.xEcCountryCustoms)) return false;

        if (xEcCountryDelivery == null) {
            if (other.xEcCountryDelivery != null) return false;
        }
        else if (!xEcCountryDelivery.equals(other.xEcCountryDelivery)) return false;

        if (xEcCountryDeparture == null) {
            if (other.xEcCountryDeparture != null) return false;
        }
        else if (!xEcCountryDeparture.equals(other.xEcCountryDeparture)) return false;

        if (xEcCountryPickup == null) {
            if (other.xEcCountryPickup != null) return false;
        }
        else if (!xEcCountryPickup.equals(other.xEcCountryPickup)) return false;

        if (xEbCompagnieCurrency == null) {
            if (other.xEbCompagnieCurrency != null) return false;
        }
        else if (!xEbCompagnieCurrency.equals(other.xEbCompagnieCurrency)) return false;

        if (xTransporteur == null) {
            if (other.xTransporteur != null) return false;
        }
        else if (!xTransporteur.equals(other.xTransporteur)) return false;

        return true;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getInfosForCustomsClearance() {
        return infosForCustomsClearance;
    }

    public void setInfosForCustomsClearance(String infosForCustomsClearance) {
        this.infosForCustomsClearance = infosForCustomsClearance;
    }

    public Double getPriceReal() {
        return priceReal;
    }

    public void setPriceReal(Double priceReal) {
        this.priceReal = priceReal;
    }

    public List<EbPlCostItem> getListPlCostItem() {

        try {

            if (listPlCostItem != null) {
                ObjectMapper objectMapper = new ObjectMapper();
                // pour deserialiser la liste et eviter l'erreur lincked hashMap
                listPlCostItem = Arrays.asList(objectMapper.convertValue(listPlCostItem, EbPlCostItem[].class));
            }
            else {
                listPlCostItem = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listPlCostItem;
    }

    public void setListPlCostItem(List<EbPlCostItem> listPlCostItem) {
        this.listPlCostItem = listPlCostItem;
    }

    public String getRefGrille() {
        return refGrille;
    }

    public void setRefGrille(String refGrille) {
        this.refGrille = refGrille;
    }

    public List<EbTtCompagnyPslLightDto> getListEbTtCompagniePsl() {

        try {

            if (listEbTtCompagniePsl != null) {
                ObjectMapper objectMapper = new ObjectMapper();
                // pour deserialiser la liste et eviter l'erreur lincked hashMap
                listEbTtCompagniePsl = Arrays
                    .asList(objectMapper.convertValue(listEbTtCompagniePsl, EbTtCompagnyPslLightDto[].class));
            }
            else {
                listEbTtCompagniePsl = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listEbTtCompagniePsl;
    }

    public void setListEbTtCompagniePsl(List<EbTtCompagnyPslLightDto> listEbTtCompagniePsl) {
        this.listEbTtCompagniePsl = listEbTtCompagniePsl;
    }

    public void setListEbTtCompagniePslFromListCompanyPsl(List<EbTtCompanyPsl> listPsl) {
        this.listEbTtCompagniePsl = listPsl.stream().map(it -> {
            EbTtCompagnyPslLightDto dto = new EbTtCompagnyPslLightDto(it);
            dto.setExpectedDate(null);

            return dto;
        }).collect(Collectors.toList());
    }

    public void setExpectedDateByCodePsl(String codeAlpha, Boolean start, Boolean end, Date date) {

        if (this.listEbTtCompagniePsl != null) {
            this.listEbTtCompagniePsl = ObjectDeserializer
                .checkJsonListObject(this.listEbTtCompagniePsl, new TypeToken<List<EbTtCompagnyPslLightDto>>() {
                }.getType());

            for (EbTtCompagnyPslLightDto psl: this.listEbTtCompagniePsl) {
                boolean bca = codeAlpha != null && psl.getCodeAlpha().contentEquals(codeAlpha);
                boolean bcs = start != null && start && psl.getStartPsl() != null && psl.getStartPsl();
                boolean bce = end != null && end && psl.getEndPsl() != null && psl.getEndPsl();

                if (bca || bcs || bce) {
                    psl.setExpectedDate(date);
                }

            }

        }

    }

	public boolean isHorsGrille() {
		return horsGrille;
	}

	public void setHorsGrille(boolean horsGrille) {
		this.horsGrille = horsGrille;
	}

}
