/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.analytics;

import javax.persistence.*;


@Entity
@Table(name = "dwr_f_shipments")
public class DwrFShipments {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dwr_f_shipments")
    private Long dwrFShipmentsNum;

    @Column(name = "total_cost")
    private Double totalCost;

    @Column(name = "cost_delivered")
    private Double costDelivered;

    @Column(name = "cost_others")
    private Double costOthers;

    @Column(name = "total_weight")
    private Double totalWeight;

    @Column(name = "weight_delivered")
    private Double weightDelivered;

    @Column(name = "weight_others")
    private Double weightOthers;

    @Column(name = "nb_expeditions")
    private Long nbExpeditions;

    @Column(name = "nb_delivered")
    private Long nbDelivered;

    @Column(name = "nb_others")
    private Long nbOthers;

    @Column(name = "nb_valid_receipts")
    private Long nbValidReceipts;

    @Column(name = "nb_negotiated_receipts")
    private Long nbNegociatedReceipts;

    @Column(name = "p_request_on_time")
    private Long pRequestOnTime;

    @Column(name = "p_request_late")
    private Long pRequestLate;

    @Column(name = "p_request_met_costs")
    private Long pRequestMetCosts;

    @Column(name = "p_price_negociated_receipt")
    private Long pPriceNegociatedReceipt;

    @Column(name = "avg_nb_offers_per_request")
    private Long avgNbOffersPerRequest;

    @Column(name = "avg_response_time")
    private Long avgResponseTime;

    @Column(name = "avg_cost_per_request")
    private Double avgCostPerRequest;

    @Column(name = "transport_type")
    private Integer transportType;

    @ManyToOne
    @JoinColumn(name = "origin_country_id")
    private DwrDCountry originCountry;

    @ManyToOne
    @JoinColumn(name = "destination_country_id")
    private DwrDCountry destinationCountry;

    @ManyToOne
    @JoinColumn(name = "date_initiated_id")
    private DwrDMonth dateInitiated;

    @ManyToOne
    @JoinColumn(name = "date_pickup_id")
    private DwrDMonth datePickup;

    @ManyToOne
    @JoinColumn(name = "date_delivered_id")
    private DwrDMonth dateDelivered;

    @ManyToOne
    @JoinColumn(name = "carrier_id")
    private DwrDCarrier carrier;

    @ManyToOne
    @JoinColumn(name = "charger_id")
    private DwrDCharger charger;

    public Long getID() {
        return this.dwrFShipmentsNum;
    }

    DwrDCountry getDestinationCountry() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.destinationCountry;
    }

    void setDestinationCountry(DwrDCountry value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.destinationCountry = value;
    }

    DwrDCountry getOriginCountry() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.originCountry;
    }

    void setOriginCountry(DwrDCountry value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.originCountry = value;
    }

    Double getTotalCost() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.totalCost;
    }

    void setTotalCost(Double value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.totalCost = value;
    }

    Double getCostDelivered() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.costDelivered;
    }

    void setCostDelivered(Double value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.costDelivered = value;
    }

    Double getCostOthers() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.costOthers;
    }

    void setCostOthers(Double value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.costOthers = value;
    }

    Double getTotalWeight() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.totalWeight;
    }

    void setTotalWeight(Double value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.totalWeight = value;
    }

    Double getWeightDelivered() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.weightDelivered;
    }

    void setWeightDelivered(Double value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.weightDelivered = value;
    }

    Double getWeightOthers() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.weightOthers;
    }

    void setWeightOthers(Double value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.weightOthers = value;
    }

    Long getNbExpeditions() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.nbExpeditions;
    }

    void setNbExpeditions(Long value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.nbExpeditions = value;
    }

    Long getNbDelivered() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.nbDelivered;
    }

    void setNbDelivered(Long value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.nbDelivered = value;
    }

    Long getNbOthers() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.nbOthers;
    }

    void setNbOthers(Long value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.nbOthers = value;
    }

    Long getNbValidReceipts() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.nbValidReceipts;
    }

    void setNbValidReceipts(Long value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.nbValidReceipts = value;
    }

    Long getNbNegociatedReceipts() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.nbNegociatedReceipts;
    }

    void setNbNegociatedReceipts(Long value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.nbNegociatedReceipts = value;
    }

    Long getPRequestOnTime() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.pRequestOnTime;
    }

    void setPRequestOnTime(Long value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.pRequestOnTime = value;
    }

    Long getPRequestLate() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.pRequestLate;
    }

    void setPRequestLate(Long value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.pRequestLate = value;
    }

    Long getPRequestMetCosts() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.pRequestMetCosts;
    }

    void setPRequestMetCosts(Long value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.pRequestMetCosts = value;
    }

    Long getPPriceNegociatedReceipt() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.pPriceNegociatedReceipt;
    }

    void setPPriceNegociatedReceipt(Long value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.pPriceNegociatedReceipt = value;
    }

    Long getAvgNbOffersPerRequest() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.avgNbOffersPerRequest;
    }

    void setAvgNbOffersPerRequest(Long value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.avgNbOffersPerRequest = value;
    }

    Long getAvgResponseTime() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.avgResponseTime;
    }

    void setAvgResponseTime(Long value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.avgResponseTime = value;
    }

    Double getAvgCostPerRequest() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.avgCostPerRequest;
    }

    void setAvgCostPerRequest(Double value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.avgCostPerRequest = value;
    }

    DwrDMonth getDateInitiated() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.dateInitiated;
    }

    void setDateInitiated(DwrDMonth value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.dateInitiated = value;
    }

    DwrDMonth getDateDelivered() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.dateDelivered;
    }

    void setDateDelivered(DwrDMonth value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.dateDelivered = value;
    }

    DwrDMonth getDatePickup() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.datePickup;
    }

    void setDatePickup(DwrDMonth value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.datePickup = value;
    }

    DwrDCarrier getCarrier() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.carrier;
    }

    void setCarrier(DwrDCarrier value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.carrier = value;
    }

    Integer getTransportType() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.transportType;
    }

    void setTransportType(Integer value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.transportType = value;
    }

    DwrDCharger getCharger() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.charger;
    }

    void setCharger(DwrDCharger value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.charger = value;
    }
}
