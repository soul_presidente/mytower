package com.adias.mytowereasy.model;

import javax.persistence.*;


@Entity
public class EdiMapCarrier {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ediMapCarrierPslNum;

    private String codeMtg;

    private String nomTransporteur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company", insertable = true)
    private EbCompagnie xEbCompagnie;

    public Integer getEdiMapCarrierPslNum() {
        return ediMapCarrierPslNum;
    }

    public String getCodeMtg() {
        return codeMtg;
    }

    public String getNomTransporteur() {
        return nomTransporteur;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setEdiMapCarrierPslNum(Integer ediMapCarrierPslNum) {
        this.ediMapCarrierPslNum = ediMapCarrierPslNum;
    }

    public void setCodeMtg(String codeMtg) {
        this.codeMtg = codeMtg;
    }

    public void setNomTransporteur(String nomTransporteur) {
        this.nomTransporteur = nomTransporteur;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }
}
