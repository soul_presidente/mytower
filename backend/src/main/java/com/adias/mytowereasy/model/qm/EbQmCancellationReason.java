/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.qm;

import javax.persistence.*;

import com.adias.mytowereasy.model.EbEtablissement;


/** @author admin */
@Entity
@Table(name = "eb_qm_cancellation_reason")
public class EbQmCancellationReason implements java.io.Serializable {
    private static final long serialVersionUID = -3800826220554152680L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer cancellationReasonNum;

    @Column
    private String label;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab")
    private EbEtablissement etablissement;

    public Integer getCancellationReasonNum() {
        return cancellationReasonNum;
    }

    public void setCancellationReasonNum(Integer cancellationReasonNum) {
        this.cancellationReasonNum = cancellationReasonNum;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cancellationReasonNum == null) ? 0 : cancellationReasonNum.hashCode());
        result = prime * result + ((etablissement == null) ? 0 : etablissement.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbQmCancellationReason other = (EbQmCancellationReason) obj;

        if (cancellationReasonNum == null) {
            if (other.cancellationReasonNum != null) return false;
        }
        else if (!cancellationReasonNum.equals(other.cancellationReasonNum)) return false;

        if (etablissement == null) {
            if (other.etablissement != null) return false;
        }
        else if (!etablissement.equals(other.etablissement)) return false;

        if (label == null) {
            if (other.label != null) return false;
        }
        else if (!label.equals(other.label)) return false;

        return true;
    }
}
