package com.adias.mytowereasy.model;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import com.adias.mytowereasy.util.GenericEnum;
import com.adias.mytowereasy.util.enums.GenericEnumException;
import com.adias.mytowereasy.util.enums.GenericEnumUtils;


public interface StatusEnum extends GenericEnum {
    /**
     * @return Value code
     */
    Integer getOrder();

    /**
     * @return Value key
     */
    Boolean getEnabled();

    static String toJson(Class<? extends StatusEnum> clazz) {
        List<GenericEnum> values;

        try {
            values = GenericEnumUtils.getAllValues(clazz);
        } catch (GenericEnumException e) {
            e.printStackTrace();
            return null;
        }

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode arrayNode = mapper.createArrayNode();

        for (GenericEnum item: values) {
            StatusEnum itemCasted = (StatusEnum) item;
            ObjectNode jsonObj = mapper.createObjectNode();
            jsonObj.set("code", mapper.valueToTree(item.getCode()));
            jsonObj.set("key", mapper.valueToTree(item.getKey()));
            jsonObj.set("order", mapper.valueToTree(itemCasted.getOrder()));
            jsonObj.set("enabled", mapper.valueToTree(itemCasted.getEnabled()));

            arrayNode.add(jsonObj);
        }

        return arrayNode.toString();
    }
}
