/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.types.CustomObjectIdResolver;


@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "ebCompagnieNum",
    resolver = CustomObjectIdResolver.class,
    scope = EbCompagnie.class)
@NamedEntityGraph(name = "EbCompagnie.detail", attributeNodes = {
    @NamedAttributeNode(value = "listEtablissements")
})
@SqlResultSetMapping(name = "CompagnieContactMapping", classes = {
    @ConstructorResult(targetClass = EbCompagnie.class, columns = {
        @ColumnResult(name = "eb_relation_num", type = Integer.class),
        @ColumnResult(name = "type_relation", type = Integer.class),
        @ColumnResult(name = "etat", type = Integer.class),
        @ColumnResult(name = "rel_email"),
        @ColumnResult(name = "x_eb_etablissement", type = Integer.class),
        @ColumnResult(name = "x_eb_etablissement_host", type = Integer.class),
        @ColumnResult(name = "flag_etablissement", type = Boolean.class),
        @ColumnResult(name = "eb_compagnie_num", type = Integer.class),
        @ColumnResult(name = "comp_nom"),
        @ColumnResult(name = "compagnie_role", type = Integer.class),
        @ColumnResult(name = "comp_siren"),
        @ColumnResult(name = "comp_code"),
        @ColumnResult(name = "x_host", type = Integer.class),
        @ColumnResult(name = "x_guest", type = Integer.class),
        @ColumnResult(name = "guest_nom"),
        @ColumnResult(name = "guest_prenom"),
        @ColumnResult(name = "guest_email")
    })
})
@Entity
@JsonIgnoreProperties(ignoreUnknown = true, value = {
    "hibernateLazyInitializer", "handler", "ebGroupe"
})
@Table(name = "eb_compagnie", schema = "work")
public class EbCompagnie extends EbTimeStamps implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "eb_compagnie_num", nullable = false, unique = true)
    private Integer ebCompagnieNum;

    @Column(name = "siren", updatable = false, insertable = true)
    private String siren;

    @Column(name = "nom", updatable = false, insertable = true)
    private String nom;

    // @ManyToOne(cascade = CascadeType.ALL)
    // @JoinColumn(name = "x_eb_groupe", nullable = true)
    // private EbGroupe ebGroupe;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "ebCompagnie")
    private Set<EbEtablissement> listEtablissements;

    @Column(updatable = false, insertable = true)
    private String code;

    @Column(updatable = false, insertable = true)
    private Integer compagnieRole;

    @Column(updatable = false, insertable = true)
    private String groupeName;

    private String logo;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private EbRelation ebRelation;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "compagnie")
    private Set<EbCategorie> categories;

    public EbCompagnie() {
    }

    public EbCompagnie(
        Integer ebRelationNum,
        Integer typeRelation,
        Integer etat,
        String relEmail,
        Integer xEbEtablissement,
        Integer xEbEtablissementHost,
        Boolean flagEtablissement,
        Integer ebCompagnieNum,
        String compNom,
        Integer compRole,
        String compSiren,
        String compCode,
        Integer xHost,
        Integer xGuest,
        String guestNom,
        String guestPrenom,
        String guestEmail) {
        this.ebRelation = new EbRelation();
        this.ebRelation.setEbRelationNum(ebRelationNum);
        this.ebRelation.setTypeRelation(typeRelation);
        this.ebRelation.setEtat(etat);
        this.ebRelation.setEmail(relEmail);
        this.ebRelation.setFlagEtablissement(flagEtablissement);
        this.ebRelation.setxEbEtablissement(new EbEtablissement(xEbEtablissement));
        this.ebRelation.setxEbEtablissementHost(new EbEtablissement(xEbEtablissementHost));
        this.ebRelation.setHost(new EbUser(xHost));
        this.ebRelation.setGuest(new EbUser(xGuest, guestNom, guestPrenom, guestEmail, null, null));

        this.ebCompagnieNum = ebCompagnieNum;
        this.nom = compNom;
        this.siren = compSiren;
        this.code = compCode;
        this.compagnieRole = compRole;
    }

    public EbCompagnie(Integer ebCompagnieNum) {
        this.ebCompagnieNum = ebCompagnieNum;
    }

    public EbCompagnie(String nom) {
        this.nom = nom;
    }

    public EbCompagnie(Integer ebCompagnieNum, String code) {
        this.ebCompagnieNum = ebCompagnieNum;
        this.nom = code;
    }

    public EbCompagnie(Integer ebCompagnieNum, String nom, Integer compRole, String compSiren) {
        this.ebCompagnieNum = ebCompagnieNum;
        this.nom = nom;
        this.compagnieRole = compRole;
        this.siren = compSiren;
    }

    @QueryProjection
    public EbCompagnie(Integer ebCompagnieNum, String nom, String code) {
        this.ebCompagnieNum = ebCompagnieNum;
        this.nom = nom;
        this.code = code;
    }

    @QueryProjection
    public EbCompagnie(
        Integer ebCompagnieNum,
        String nom,
        String code,
        String siren,
        Integer compagnieRole,
        String groupName,
        String logo) {
        this.ebCompagnieNum = ebCompagnieNum;
        this.nom = nom;
        this.code = code;
        this.siren = siren;
        this.compagnieRole = compagnieRole;
        this.groupeName = groupName;
        this.logo = logo;
    }

    public EbCompagnie(Integer ebCompagnieNum, String nom, String logo, Integer x) {
        this.ebCompagnieNum = ebCompagnieNum;
        this.nom = nom;
        this.logo = logo;
    }

    public EbCompagnie(EbCompagnie compagnie) {

        if (compagnie != null) {
            this.ebCompagnieNum = compagnie.ebCompagnieNum;
            this.nom = compagnie.nom;
            this.siren = compagnie.siren;
            this.code = compagnie.code;
        }

    }

    public Integer getEbCompagnieNum() {
        return ebCompagnieNum;
    }

    public void setEbCompagnieNum(Integer ebCompagnieNum) {
        this.ebCompagnieNum = ebCompagnieNum;
    }

    public String getSiren() {
        return siren;
    }

    public void setSiren(String siren) {
        this.siren = siren;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    // public EbGroupe getEbGroupe()
    // {
    // return ebGroupe;
    // }
    //
    // public void setEbGroupe(EbGroupe ebGroupe)
    // {
    // this.ebGroupe = ebGroupe;
    // }

    public Set<EbEtablissement> getListEtablissements() {
        return listEtablissements;
    }

    public void setListEtablissements(Set<EbEtablissement> listEtablissements) {
        this.listEtablissements = listEtablissements;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ebCompagnieNum == null) ? 0 : ebCompagnieNum.hashCode());
        // result = prime * result + ((ebGroupe == null) ? 0 :
        // ebGroupe.hashCode());
        // result = prime * result + ((listEtablissements == null) ? 0 :
        // listEtablissements.hashCode());
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        result = prime * result + ((siren == null) ? 0 : siren.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbCompagnie other = (EbCompagnie) obj;

        if (ebCompagnieNum == null) {
            if (other.ebCompagnieNum != null) return false;
        }
        else if (!ebCompagnieNum.equals(other.ebCompagnieNum)) return false;

        // if (ebGroupe == null)
        // {
        // if (other.ebGroupe != null)
        // return false;
        // }
        // else if (!ebGroupe.equals(other.ebGroupe))
        // return false;
        /*
         * if (listEtablissements == null)
         * {
         * if (other.listEtablissements != null)
         * return false;
         * }
         * else if (!listEtablissements.equals(other.listEtablissements))
         * return false;
         */
        if (nom == null) {
            if (other.nom != null) return false;
        }
        else if (!nom.equals(other.nom)) return false;

        if (siren == null) {
            if (other.siren != null) return false;
        }
        else if (!siren.equals(other.siren)) return false;

        return true;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getCompagnieRole() {
        return compagnieRole;
    }

    public void setCompagnieRole(Integer compagnieRole) {
        this.compagnieRole = compagnieRole;
    }

    public String getGroupeName() {
        return groupeName;
    }

    public void setGroupeName(String groupeName) {
        this.groupeName = groupeName;
    }

    public EbRelation getEbRelation() {
        return ebRelation;
    }

    public void setEbRelation(EbRelation ebRelation) {
        this.ebRelation = ebRelation;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Set<EbCategorie> getCategories() {
        return categories;
    }

    public void setCategories(Set<EbCategorie> categories) {
        this.categories = categories;
    }
}
