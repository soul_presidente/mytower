/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;


public class FAEnumeration {
    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum InvoiceStatus {
        WAITING_FOR_CONFIRMATION(1, "Waiting for confirmation", new ArrayList<Integer>(Arrays.asList(2))),
        WAITING_FOR_PAYMENT(2, "Waiting for Payment", new ArrayList<Integer>(Arrays.asList(2))),
        WAITING_FOR_INFORMATION(3, "Waiting for information", new ArrayList<Integer>(Arrays.asList(1, 3))),
        APPROVED(4, "Approved", new ArrayList<Integer>(Arrays.asList(1, 3))),
        PAYMENT_SENT(5, "Payment sent", new ArrayList<Integer>(Arrays.asList(1, 3)));

        private Integer code;
        private String libelle;
        private List<Integer> userRole;

        private InvoiceStatus(Integer code, String libelle, List<Integer> userRole) {
            this.code = code;
            this.libelle = libelle;
            this.userRole = userRole;
            // this.userRole.toArray()
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public List<Integer> getUserRole() {
            return userRole;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                String invoiceStatus = null;

                for (InvoiceStatus item: InvoiceStatus.values()) {
                    if (item.code.equals(code)) invoiceStatus = item.libelle;
                }

                return invoiceStatus;
            }

            return null;
        }

        // récuppération de la liste format JSON
        public static List<InvoiceStatus> getListStatus() {
            List<InvoiceStatus> listStatus = new ArrayList();
            InvoiceStatus[] values = InvoiceStatus.values();

            for (InvoiceStatus at: values) {
                listStatus.add(at);
            }

            // Arrays.asList();
            return listStatus;
        }

        public static String toJsonList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (InvoiceStatus item: InvoiceStatus.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }

        public static Integer getCodeByLibelle(String libelle) {

            if (libelle != null) {
                String value = libelle.toLowerCase();
                Integer incoterm = null;

                for (InvoiceStatus item: InvoiceStatus.values()) {
                    if (item.libelle.toLowerCase().contains(value)) incoterm = item.code;
                }

                return incoterm;
            }

            return null;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum ActionType {
        sharedStatus("sharedStatus", "Share Invoice Status", true),
        sharedPrice("sharedPrice", "Share Invoice Price", true),
        sharedNumber("sharedNumber", "Share Invoice Number", true),
        sharedMonthAndYear("sharedMonthAndYear", "Share Invoice Date", true),
        applyListDate("applyListDate", "Apply Date", true),
        applyListInvoicePrice("applyListInvoicePrice", "Apply Price", true),
        applyListInvoiceNumber("applyListInvoiceNumber", "Apply Number", true),
        applyListStatut("applyListStatut", "Apply status", true),
        applyListFlag("applyListFlag", "Apply flag", true),
        // changeNumber("changeNumber" , "changeNumber", false),
        // changePrice("changePrice" , "changePrice", false),
        applyListMemo("applyListMemo", "apply Memo", false);

        // changeDate("changeDate" , "changeDate", false);
        private String code;
        private String libelle;
        private boolean displayInListAction = false;

        private ActionType(String code, String libelle, boolean displayInListAction) {
            this.code = code;
            this.libelle = libelle;
            this.displayInListAction = displayInListAction;
        }

        public String getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public boolean getDisplayInListAction() {
            return displayInListAction;
        }

        // récuppération de la liste format JSON
        public static List<ActionType> getListActionType() {
            List<ActionType> listActionType = new ArrayList();
            ActionType[] values = ActionType.values();

            for (ActionType at: values) {

                if (at.getDisplayInListAction() == true) {
                    listActionType.add(getByCode(at.getCode()));
                }

            }

            // Arrays.asList();
            return listActionType;
        }

        public static ActionType getByCode(String code) {

            if (code != null) {
                ActionType ac = null;

                for (ActionType item: ActionType.values()) {
                    if (item.code.equals(code)) ac = item;
                }

                return ac;
            }

            return null;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum Flag {
        BLANK(0, " "), VALIDATE(1, "V"), NOT_VALIDATE(2, "X"), QUESTION(3, "?"), EXCLAMATION(4, "!");

        private Integer code;
        private String libelle;

        private Flag(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static Integer getCodeByLibelle(String libelle) {

            if (libelle != null) {
                Integer code = null;

                for (Flag item: Flag.values()) {
                    if (item.libelle.equals(libelle)) code = item.code;
                }

                return code;
            }

            return null;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                String libelle = null;

                for (Flag item: Flag.values()) {
                    if (item.code.equals(code)) libelle = item.libelle;
                }

                return libelle;
            }

            return null;
        }
    }
    /*
     * @JsonFormat(shape = JsonFormat.Shape.OBJECT)
     * public static enum MonthEnum {
     * ALL(0, "All"), MONDAY(DayOfWeek.MONDAY.getValue(), "Mon."),
     * TUESDAY(DayOfWeek.TUESDAY.getValue(),
     * "Tue."), WEDNESDAY(DayOfWeek.WEDNESDAY.getValue(), "Wed."),
     * THURSDAY(DayOfWeek.THURSDAY.getValue(),
     * "Thu."), FRIDAY(DayOfWeek.FRIDAY.getValue(), "Fri."),
     * SATURDAY(DayOfWeek.SATURDAY.getValue(),
     * "Sat."), SUNDAY(DayOfWeek.SUNDAY.getValue(), "Sun.");
     * private String libelle;
     * private Integer code;
     * public Integer getCode() {
     * return code;
     * }
     * public void setCode(Integer code) {
     * this.code = code;
     * }
     * public String getLibelle() {
     * return libelle;
     * }
     * public void setLibelle(String libelle) {
     * this.libelle = libelle;
     * }
     * public static JourEnum getJourEnumByCode(Integer code) {
     * JourEnum jourEnum = null;
     * for (JourEnum item : JourEnum.values()) {
     * if (item.code.equals(code))
     * jourEnum = item;
     * }
     * return jourEnum;
     * }
     */

    /*
     * public enum dateMonth {
     * SEPTEMBRE(9, "sept");
     * private Integer code;
     * private String libelle;
     * private dateMonth(Integer code, String libelle) {
     * this.code = code;
     * this.libelle = libelle;
     * }
     * public Integer getCode() {
     * return code;
     * }
     * public String getLibelle() {
     * return libelle;
     * }
     * public static Integer getCodeByLibelle(String libelle) {
     * if (libelle != null) {
     * String value = libelle.toLowerCase();
     * Integer incoterm = null;
     * for (dateMonth item : dateMonth.values()) {
     * if (item.libelle.toLowerCase().contains(value)) incoterm = item.code;
     * }
     * return incoterm;
     * }
     * return null;
     * }
     * }
     */
}
