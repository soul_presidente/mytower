/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.tt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.airbus.model.RegroupmtPslEvent;
import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbCostCenter;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.model.EbTypeDocuments;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCountry;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.model.Enumeration.TransitStatus;
import com.adias.mytowereasy.model.TtEnumeration;
import com.adias.mytowereasy.model.TtEnumeration.Late;
import com.adias.mytowereasy.model.enums.Modules;
import com.adias.mytowereasy.util.JsonDateDeserializer;
import com.adias.mytowereasy.util.TtTracingUtil;


// @NamedEntityGraphs({
// @NamedEntityGraph(name = "graph.EbTtTracing.xEbDemande", attributeNodes =
// @NamedAttributeNode("xEbDemande")),
// @NamedEntityGraph(name = "graph.EbTtTracing.listEbPslApp", attributeNodes =
// @NamedAttributeNode("listEbPslApp")),
// })
@Table(name = "eb_tt_tracing", indexes = {
    // @Index(name = "idx_eb_tt_tracing_total_weight", columnList =
    // "total_weight"),
    @Index(name = "idx_eb_tt_tracing_late", columnList = "late"),
    // @Index(name = "idx_eb_tt_tracing_date_last_psl", columnList =
    // "date_last_psl"),
    // @Index(name = "idx_eb_tt_tracing_date_modification", columnList =
    // "date_modification"),
    // @Index(name = "idx_eb_tt_tracing_date_pickup", columnList =
    // "date_pickup"),

    @Index(name = "idx_eb_tt_tracing_x_eb_demande", columnList = "x_eb_demande"),
    @Index(name = "idx_eb_tt_tracing_x_eb_chargeur", columnList = "x_eb_chargeur"),
    @Index(name = "idx_eb_tt_tracing_x_eb_company_chargeur", columnList = "x_eb_company_chargeur"),
    @Index(name = "idx_eb_tt_tracing_x_eb_etablissement_chargeur", columnList = "x_eb_etablissement_chargeur"),
    @Index(name = "idx_eb_tt_tracing_x_ec_country_origin", columnList = "x_ec_country_origin"),
    @Index(name = "idx_eb_tt_tracing_x_ec_country_destination", columnList = "x_ec_country_destination"),
    @Index(name = "idx_eb_tt_tracing_x_eb_user_ct", columnList = "x_eb_user_ct"),
})
@Entity
public class EbTtTracing {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebTtTracingNum;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_marchandise")
    private EbMarchandise xEbMarchandise;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "xEbTrackTrace", cascade = CascadeType.ALL)
    private List<EbTTPslApp> listEbPslApp;

    @Column(insertable = true, updatable = false)
    @ColumnDefault("now()")
    private Date dateCreation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_demande", insertable = true, updatable = false)
    private EbDemande xEbDemande;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country_origin", insertable = true, updatable = false)
    private EcCountry xEcCountryOrigin;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country_destination", insertable = true, updatable = false)
    private EcCountry xEcCountryDestination;

    private Date datePickup;

    @Column(insertable = true, updatable = true)
    @ColumnDefault("now()")
    private Date dateModification;

    private Double totalWeight;

    private String refTransport;

    private String customerReference; // c'est le "unit ref"
    private String packingList;

    private String partNumber;

    private String serialNumber;

    private String shippingType;

    private String orderNumber;
    @Column(columnDefinition = "TEXT")
    private String customRef; // c'est le "customer ref"

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_transporteur", insertable = true, updatable = false)
    private EbUser xEbTransporteur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement_transporteur", insertable = true, updatable = false)
    private EbEtablissement xEbEtablissementTransporteur;

    private String nomEtablissementTransporteur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company_transporteur", insertable = true, updatable = false)
    private EbCompagnie xEbCompagnieTransporteur;

    // créer le champ x_eb_tt_tracing sur l'entité eb_tt_event
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE, mappedBy = "xEbTtTracing")
    private List<EbTtEvent> listEvent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_chargeur", insertable = true, updatable = false)
    private EbUser xEbChargeur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement_chargeur", insertable = true, updatable = false)
    private EbEtablissement xEbEtablissementChargeur;

    private String nomEtablissementChargeur;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company_chargeur", insertable = true, updatable = false)
    private EbCompagnie xEbChargeurCompagnie;

    @Enumerated(EnumType.ORDINAL)
    private TtEnumeration.Late late;

    private String configPsl; // chaine de code psl separes par
                              // espace/tabulation ou autre separateur: PIC DEP
    // ARR CUS DEL

    private Integer leadtime; // jours

    @Transient
    @JsonDeserialize
    private String customerRefTransportRef;

    @Column(updatable = false)
    private Integer xEcModeTransport;

    private String originPlace;

    private String destinationPlace;

    // champs reservé pour customFields
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbCategorie> listCategories;

    @Column(columnDefinition = "TEXT")
    private String listLabels;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_ct")
    private EbUser xEbUserCt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement_ct")
    private EbEtablissement xEbEtablissementCt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie_ct")
    private EbCompagnie xEbCompagnieCt;

    @Column(updatable = false)
    private String libelleOriginCity;

    @Column(updatable = false)
    private String libelleDestCity;

    @Column(updatable = false)
    private String xEcIncotermLibelle;

    private String listFlag;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbFlagDTO> listEbFlagDTO;

    /****************** DEBUT ATTRIBUTS GESTION DES PSL *****************/

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private EbTtSchemaPsl xEbSchemaPsl;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbTtCompanyPsl> listPsl;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<RegroupmtPslEvent> listPslEvent;

    private String codeAlphaPslCourant;
    private String libellePslCourant;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Double percentPslCourant;

    private String codeAlphaLastPsl;
    private String libelleLastPsl;
    private Date dateLastPsl;

    private Integer codeLastPsl;

    /****************** FIN ATTRIBUTS GESTION DES PSL *****************/

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String transportStatusPerUnit;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String companyOrigin;

    private String listControlRule;

    public String getListFlag() {
        return listFlag;
    }

    public void setListFlag(String listFlag) {
        this.listFlag = listFlag;
    }

    public List<EbFlagDTO> getListEbFlagDTO() {
        return listEbFlagDTO;
    }

    public void setListEbFlagDTO(List<EbFlagDTO> listEbFlagDTO) {
        this.listEbFlagDTO = listEbFlagDTO;
    }

    /* Constructor */
    public EbTtTracing() {
    }

    public EbTtTracing(Integer ebTtTracingNum, String codeAlphaPslCourant, Integer ebDemandeNum, String configPsl) {
        this.ebTtTracingNum = ebTtTracingNum;
        this.codeAlphaPslCourant = codeAlphaPslCourant;
        this.xEbDemande = new EbDemande(ebDemandeNum);
        this.configPsl = configPsl;
    }

    // utiliser dans la generation des events par unit ref
    @QueryProjection
    public EbTtTracing(
        Integer ebTtTracingNum,
        String codeAlphaPslCourant,
        Integer ebDemandeNum,
        String configPsl,
        String unitRef,
        String refTransport,
        String customerRef) {
        this.ebTtTracingNum = ebTtTracingNum;
        this.codeAlphaPslCourant = codeAlphaPslCourant;
        this.xEbDemande = new EbDemande(ebDemandeNum);
        this.configPsl = configPsl;
        this.customerReference = unitRef;
        this.refTransport = refTransport;
        this.customRef = customerRef;
    }

    public EbTtTracing(Integer ebTtTracingNum) {
        this.ebTtTracingNum = ebTtTracingNum;
    }

    // dashboard tt
    @QueryProjection
    public EbTtTracing(
        Integer ebTtTracingNum,
        String refTransport,
        String customerReference,
        String partNumber,
        String serialNumber,
        String shippingType,
        String orderNumber,
        String libelleOriginCountry,
        String libelleDestiCountry,
        Date datePickup,
        String nomEtablissementTransporteur,
        Double totalWeight,
        Date dateModification,
        Late late,
        String configPsl,
        Integer xEcModeTransport,
        Integer ebDemandeNum,
        String customFields,
        List<EbCategorie> listCategories,
        Integer usernum,
        Integer compagnieNum,
        String customRef,
        Date dateCreation,
        String numAwbBol,
        String carrierUniqRefNum,
        String stringOrderWithExpression,
        Integer numberOrderWithExpression,
        String libelleOriginCity,
        String libelleDestCity,
        String xEcIncotermLibelle,
        String nomCompagnie,
        String libellePslCourant,
        String libelleLastPsl,
        String codeAlphaLastPsl,
        Date dateLastPsl,
        String listFlag,
        EbTtSchemaPsl schemaPsl,
        String codeAlphaPslCourant,
        Date datePickupTM,
        Date finalDelivery,
        String nomCh,
        String prenomCh,
        Integer numCt,
        String nomCt,
        String prenomCt,
        List<EbTypeDocuments> listTypeDocuments,
        String companyOrigin,
        Integer xEcStatus,
        Boolean askForTransportResponsibility,
        Boolean provideTransport,
        Date dateLastPslEbDemande,
        String packingList) {
        this.ebTtTracingNum = ebTtTracingNum;
        this.refTransport = refTransport;
        this.customerReference = customerReference;
        this.partNumber = partNumber;
        this.orderNumber = orderNumber;
        this.serialNumber = serialNumber;
        this.shippingType = shippingType;
        this.xEbDemande = new EbDemande();
        // this.xEbDemande.setCustomerReference(customerReference);

        this.xEcCountryOrigin = new EcCountry();
        this.xEcCountryOrigin.setLibelle(libelleOriginCountry);

        this.xEcCountryDestination = new EcCountry();
        this.xEcCountryDestination.setLibelle(libelleDestiCountry);

        this.datePickup = datePickup;
        this.nomEtablissementTransporteur = nomEtablissementTransporteur;
        this.totalWeight = totalWeight;
        this.dateModification = dateModification;
        this.late = late;
        this.configPsl = configPsl;
        this.xEcModeTransport = xEcModeTransport;
        this.xEbChargeur = new EbUser(usernum);
        this.xEbChargeur = new EbUser(usernum, nomCh, prenomCh);
        this.xEbUserCt = new EbUser(numCt, nomCt, prenomCt);
        this.xEbChargeur.setEbCompagnie(new EbCompagnie(compagnieNum, nomCompagnie, null));
        this.customRef = customRef;
        this.packingList = packingList;
        this.xEbDemande.setEbDemandeNum(ebDemandeNum);
        this.xEbDemande.setCustomerReference(customRef);
        this.xEbDemande.setCustomFields(customFields);
        this.xEbDemande.setDateCreation(dateCreation);
        this.xEbDemande.setxEcStatut(xEcStatus);
        ObjectMapper objectMapper = new ObjectMapper();

        // pour deserialiser la liste et eviter l'erreur lincked hashMap
        if (listTypeDocuments != null) {
            listTypeDocuments = Arrays.asList(objectMapper.convertValue(listTypeDocuments, EbTypeDocuments[].class));

            listTypeDocuments = listTypeDocuments
                .stream().filter(it -> it.getModule() != null && it.getModule().contains(Modules.TRACK.getValue()))
                .collect(Collectors.toList());
            this.xEbDemande.setListTypeDocuments(listTypeDocuments);
        }
        else {
            listTypeDocuments = null;
        }

        // this.xEbDemande.setListTypeDocuments(listTypeDocuments);
        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDateDeserializer()).create();

        if (customFields != null) {
            this.listCustomsFields = gson.fromJson(customFields, new TypeToken<ArrayList<CustomFields>>() {
            }.getType());
        }

        this.xEbDemande.setListCustomsFields(this.listCustomsFields);

        if (listCategories != null && listCategories.size() > 0) {
            String jsonCateg = gson.toJson(listCategories);
            this.listCategories = gson.fromJson(jsonCateg, new TypeToken<ArrayList<EbCategorie>>() {
            }.getType());
        }

        this.xEbDemande.setListCategories(this.listCategories);

        this.xEbMarchandise = new EbMarchandise();
        this.xEbMarchandise.setEbMarchandiseNum(xEbMarchandise.getEbMarchandiseNum());

        this.xEbDemande.setNumAwbBol(numAwbBol);
        this.xEbDemande.setCarrierUniqRefNum(carrierUniqRefNum);
        this.libelleOriginCity = libelleOriginCity;
        this.libelleDestCity = libelleDestCity;
        this.xEcIncotermLibelle = xEcIncotermLibelle;

        this.libellePslCourant = libellePslCourant;
        this.libelleLastPsl = libelleLastPsl;
        this.dateLastPsl = dateLastPsl;
        this.listFlag = listFlag;

        this.xEbSchemaPsl = schemaPsl;
        this.listPsl = schemaPsl != null ? schemaPsl.getListPsl() : null;
        this.codeAlphaPslCourant = codeAlphaPslCourant;
        this.xEbDemande.setDatePickupTM(datePickupTM);
        this.xEbDemande.setFinalDelivery(finalDelivery);

        this.companyOrigin = companyOrigin;

        this.transportStatusPerUnit = (codeAlphaPslCourant != null ?
            (codeAlphaLastPsl != null && codeAlphaPslCourant.contentEquals(codeAlphaLastPsl) ?
                TransitStatus.COMPLETED.getLibelle() :
                TransitStatus.ONGOING.getLibelle()) :
            TransitStatus.WAITINGFORPICKUP.getLibelle());

        this.xEbDemande.setAskForTransportResponsibility(askForTransportResponsibility);
        this.xEbDemande.setProvideTransport(provideTransport);

        TtTracingUtil.calculPercentPslCourant(this);
    }

    @QueryProjection
    public EbTtTracing(Integer ebTtTracingNum, String customerReference, String refTranspor) {
        this.ebTtTracingNum = ebTtTracingNum;
        this.customerReference = customerReference;
        this.refTransport = refTranspor;
    }

    // detail tt
    @QueryProjection
    public EbTtTracing(
        Integer ebTtTracingNum,
        String configPsl,
        String refTransport,
        String customerReference,
        Integer originCountryNum,
        String libelleOriginCountry,
        Integer destiCountryNum,
        String libelleDestiCountry,
        Date datePickup,
        String nomEtablissementTransporteur,
        Double totalWeight,
        Date dateModification,
        Late late,
        // List<EbTTPslApp> listEbPslApp,
        Integer leadtime,
        // List<EbTtEvent> listEvent,
        String nomEtablissementChargeur,
        String xEbChargeurCompagnieNom,
        Integer xEbChargeurCompagnieNum,
        String xEbChargeurNom,
        String xEbChargeurPrenom,
        Integer xEbChargeurNum,
        Integer xEbMarchandiseNum,
        String xEbTransporteurNom,
        String xEbTransporteurPrenom,
        Integer xEbTransporteurNum,
        String xEbCompagnieTransporteurNom,
        Integer xEbCompagnieTransporteurNum,
        Integer ebDemandeNum,
        Date dateCreation,
        Integer xEcStatut,
        Integer xEcTypeDemande,
        Integer xEbTypeTransport,
        Integer xEcModeTransport,
        String originPlace,
        String destinationPlace,
        String customRef,
        Integer xEbEtablissementTransporteur,
        String numAwbBol,
        String listFlag,
        EbTtSchemaPsl schemaPsl,

        Double totalNbrParcel,
        Double totalVolume,
        Double totalWeightDem,
        String numAwbBolDem,
        String mawb,
        Date etd,
        Date eta,
        String xEcIncotermLibelle,
        String libelleOriginCountryDem,
        String libelleDestCountry,
        List<EbCategorie> listCategories,
        EcCurrency xecCurrencyInvoice,
        EbCostCenter xEbCostCenter,
        String codeAlphaPslCourant,

        Date dateOfGoodsAvailability,
        String typeRequestLibelle,
        Integer xEbTypeRequest,
        Integer ebuserNumOwner,
        Integer ebcompagnieNum,
        String cityOrigin,
        String cityDest,
        String refTransportDemande

    ) {
        this.ebTtTracingNum = ebTtTracingNum;
        this.configPsl = configPsl;

        this.refTransport = refTransport;
        this.customerReference = customerReference;
        this.xEbDemande = new EbDemande();
        this.xEbDemande.setCustomerReference(customerReference);

        this.xEcCountryOrigin = new EcCountry(originCountryNum);
        this.xEcCountryOrigin.setLibelle(libelleOriginCountry);

        this.xEcCountryDestination = new EcCountry(destiCountryNum);
        this.xEcCountryDestination.setLibelle(libelleDestiCountry);

        this.datePickup = datePickup;
        this.nomEtablissementTransporteur = nomEtablissementTransporteur;
        this.totalWeight = totalWeight;
        this.dateModification = dateModification;
        this.late = late;
        this.dateCreation = dateCreation;

        this.leadtime = leadtime;
        this.nomEtablissementChargeur = nomEtablissementChargeur;
        this.xEbChargeurCompagnie = new EbCompagnie(xEbChargeurCompagnieNum, xEbChargeurCompagnieNom);
        this.xEbChargeur = new EbUser(xEbChargeurNum, xEbChargeurNom, xEbChargeurPrenom);
        this.xEbMarchandise = new EbMarchandise(xEbMarchandiseNum, customerReference);
        this.xEbTransporteur = new EbUser(xEbTransporteurNum, xEbTransporteurNom, xEbTransporteurPrenom);
        this.xEbCompagnieTransporteur = new EbCompagnie(xEbCompagnieTransporteurNum, xEbCompagnieTransporteurNom);

        this.xEbDemande = new EbDemande(ebDemandeNum, refTransport);
        this.customRef = customRef;
        this.xEbDemande.setCustomerReference(customRef);
        this.xEbDemande.setxEcTypeDemande(xEcTypeDemande);
        this.xEbDemande.setxEbTypeTransport(xEbTypeTransport);
        this.xEbDemande.setxEcModeTransport(xEcModeTransport);
        this.xEbDemande.setxEbTransporteurEtablissementNum(xEbEtablissementTransporteur);
        this.xEbDemande.setRefTransport(refTransportDemande);
        this.originPlace = originPlace;
        this.destinationPlace = destinationPlace;
        this.xEbDemande.setNumAwbBol(numAwbBol);
        this.listFlag = listFlag;

        this.xEbSchemaPsl = schemaPsl;

        this.xEbDemande.setTotalNbrParcel(totalNbrParcel);
        this.xEbDemande.setTotalVolume(totalVolume);

        this.xEbDemande.setTotalWeight(totalWeightDem);

        this.xEbDemande.setNumAwbBol(numAwbBolDem);
        this.xEbDemande.setMawb(mawb);

        this.xEbDemande.setEta(eta);
        this.xEbDemande.setEtd(etd);
        this.xEbDemande.setxEcIncotermLibelle(xEcIncotermLibelle);
        this.xEbDemande.setxEbCostCenter(xEbCostCenter);

        this.xEbDemande.setLibelleOriginCountry(libelleOriginCountryDem);
        this.xEbDemande.setLibelleDestCountry(libelleDestCountry);
        ObjectMapper mapper = new ObjectMapper();
        List<EbCategorie> listCategoriesMapped = mapper
            .convertValue(
                listCategories,
                mapper.getTypeFactory().constructCollectionType(List.class, EbCategorie.class));
        this.xEbDemande.setListCategories(listCategoriesMapped);
        this.xEbDemande.setXecCurrencyInvoice(xecCurrencyInvoice);
        this.xEbDemande.setxEbSchemaPsl(xEbSchemaPsl);
        this.codeAlphaPslCourant = codeAlphaPslCourant;

        this.xEbDemande.setDateOfGoodsAvailability(dateOfGoodsAvailability);
        this.xEbDemande.setTypeRequestLibelle(typeRequestLibelle);
        this.xEbDemande.setxEbTypeRequest(xEbTypeRequest);
        this.xEbDemande.setUser(new EbUser(ebuserNumOwner));
        this.xEbDemande.setxEbCompagnie(new EbCompagnie(ebcompagnieNum));

        this.xEbDemande.setEbPartyDest(new EbParty());
        this.xEbDemande.getEbPartyDest().setCity(cityDest);
        this.xEbDemande.setEbPartyOrigin(new EbParty());
        this.xEbDemande.getEbPartyOrigin().setCity(cityOrigin);
    }

    public EbTtTracing(EbDemande ebDemande, EbMarchandise ebMarchandise) {
        super();

        /* INFORMATIONS RECUS DU TRANSPORT */
        this.setCustomRef(ebDemande.getCustomerReference());
        this.datePickup = ebDemande.getDatePickup();

        if (ebDemande.get_exEbDemandeTransporteurFinal() != null) {
            this.xEbTransporteur = new EbUser(ebDemande.get_exEbDemandeTransporteurFinal().getxTransporteur());

            this.leadtime = ebDemande.get_exEbDemandeTransporteurFinal().getTransitTime();
        }

        if (ebDemande.get_exEbDemandeTransporteurFinal() != null
            && ebDemande.get_exEbDemandeTransporteurFinal().getxEbEtablissement() != null) {
            this.xEbEtablissementTransporteur = new EbEtablissement(
                ebDemande.get_exEbDemandeTransporteurFinal().getxEbEtablissement().getEbEtablissementNum());

            this.nomEtablissementTransporteur = ebDemande
                .get_exEbDemandeTransporteurFinal().getxEbEtablissement().getNom();
        }

        if (ebDemande.get_exEbDemandeTransporteurFinal() != null && ebDemande
            .get_exEbDemandeTransporteurFinal()
            .getxEbCompagnie() != null) this.xEbCompagnieTransporteur = new EbCompagnie(
                ebDemande.get_exEbDemandeTransporteurFinal().getxEbCompagnie().getEbCompagnieNum());
        else {

            if (ebDemande.get_exEbDemandeTransporteurFinal() != null
                && ebDemande.get_exEbDemandeTransporteurFinal().getxEbEtablissement() != null) {
                this.xEbCompagnieTransporteur = new EbCompagnie(
                    ebDemande
                        .get_exEbDemandeTransporteurFinal().getxEbEtablissement().getEbCompagnie().getEbCompagnieNum());
            }

        }

        if (ebDemande.getUser() != null && ebDemande.getUser().getEbEtablissement() != null) {
            this.nomEtablissementChargeur = ebDemande.getUser().getEbEtablissement().getNom();
            this.xEbEtablissementChargeur = new EbEtablissement(
                ebDemande.getUser().getEbEtablissement().getEbEtablissementNum());
        }

        this.refTransport = ebDemande.getRefTransport();
        this.xEbChargeur = new EbUser(ebDemande.getUser().getEbUserNum());
        if (ebDemande.getUser().getEbCompagnie() != null) this.xEbChargeurCompagnie = new EbCompagnie(
            ebDemande.getUser().getEbCompagnie().getEbCompagnieNum());
        else this.xEbChargeurCompagnie = new EbCompagnie(
            ebDemande.getUser().getEbEtablissement().getEbCompagnie().getEbCompagnieNum());

        this.xEcModeTransport = ebDemande.getxEcModeTransport();
        this.xEcCountryOrigin = new EcCountry(ebDemande.getEbPartyOrigin().getxEcCountry().getEcCountryNum());
        this.xEcCountryDestination = new EcCountry(ebDemande.getEbPartyDest().getxEcCountry().getEcCountryNum());

        this.xEcIncotermLibelle = ebDemande.getxEcIncotermLibelle();
        this.xEbDemande = new EbDemande(ebDemande.getEbDemandeNum());

        this.listLabels = ebDemande.getListLabels();

        if (ebDemande.getxEbUserCt() != null && ebDemande.getxEbUserCt().getEbUserNum() != null) {
            this.xEbUserCt = new EbUser(ebDemande.getxEbUserCt().getEbUserNum());
            this.xEbCompagnieCt = new EbCompagnie(ebDemande.getxEbCompagnieCt().getEbCompagnieNum());
            this.xEbEtablissementCt = new EbEtablissement(ebDemande.getxEbEtablissementCt().getEbEtablissementNum());
        }

        this.libelleOriginCity = ebDemande.getEbPartyOrigin().getCity();
        this.libelleDestCity = ebDemande.getEbPartyDest().getCity();
        this.xEcIncotermLibelle = ebDemande.getxEcIncotermLibelle();

        this.xEbSchemaPsl = ebDemande.getxEbSchemaPsl();
        this.codeAlphaLastPsl = ebDemande.getCodeAlphaLastPsl();
        this.codeLastPsl = ebDemande.getCodeLastPsl();
        this.libelleLastPsl = ebDemande.getLibelleLastPsl();

        /* INFORMATIONS RECUS DE LA MARCHANDISE */
        if (ebMarchandise != null) {
            this.setPackingList(ebMarchandise.getPackingList());
            this.customerReference = ebMarchandise.getUnitReference();
            this.partNumber = ebMarchandise.getPartNumber() + "";
            this.orderNumber = ebMarchandise.getOrderNumber() + "";
            this.shippingType = ebMarchandise.getShippingType();
            this.totalWeight = ebMarchandise.getWeight();

            if (ebMarchandise.getEbMarchandiseNum() != null) {
                this.xEbMarchandise = new EbMarchandise(
                    ebMarchandise.getEbMarchandiseNum(),
                    ebMarchandise.getCustomerReference());
            }

            // utile pour l'import en masse
            if (ebMarchandise
                .getCodeAlphaPslCourant() != null) this.codeAlphaPslCourant = ebMarchandise.getCodeAlphaPslCourant();

            if (ebMarchandise.getIdUnite() != null) this.xEbMarchandise.setIdUnite(ebMarchandise.getIdUnite());
        }

    }

    /* getters and setters */

    public String getLibelleOriginCity() {
        return libelleOriginCity;
    }

    public void setLibelleOriginCity(String libelleOriginCity) {
        this.libelleOriginCity = libelleOriginCity;
    }

    public String getLibelleDestCity() {
        return libelleDestCity;
    }

    public void setLibelleDestCity(String libelleDestCity) {
        this.libelleDestCity = libelleDestCity;
    }

    public Integer getEbTtTracingNum() {
        return ebTtTracingNum;
    }

    public void setEbTtTracingNum(Integer ebTtTracingNum) {
        this.ebTtTracingNum = ebTtTracingNum;
    }

    public List<EbTTPslApp> getListEbPslApp() {
        return listEbPslApp;
    }

    public void setListEbPslApp(List<EbTTPslApp> listEbPslApp) {
        this.listEbPslApp = listEbPslApp;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public EbDemande getxEbDemande() {
        return xEbDemande;
    }

    public void setxEbDemande(EbDemande xEbDemande) {
        this.xEbDemande = xEbDemande;
    }

    public EcCountry getxEcCountryOrigin() {
        return xEcCountryOrigin;
    }

    public void setxEcCountryOrigin(EcCountry xEcCountryOrigin) {
        this.xEcCountryOrigin = xEcCountryOrigin;
    }

    public EcCountry getxEcCountryDestination() {
        return xEcCountryDestination;
    }

    public void setxEcCountryDestination(EcCountry xEcCountryDestination) {
        this.xEcCountryDestination = xEcCountryDestination;
    }

    public Date getDatePickup() {
        return datePickup;
    }

    public void setDatePickup(Date datePickup) {
        this.datePickup = datePickup;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public String getRefTransport() {
        return refTransport;
    }

    public void setRefTransport(String refTransport) {
        this.refTransport = refTransport;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public EbUser getxEbTransporteur() {
        return xEbTransporteur;
    }

    public void setxEbTransporteur(EbUser xEbTransporteur) {
        this.xEbTransporteur = xEbTransporteur;
    }

    public EbEtablissement getxEbEtablissementTransporteur() {
        return xEbEtablissementTransporteur;
    }

    public void setxEbEtablissementTransporteur(EbEtablissement xEbEtablissementTransporteur) {
        this.xEbEtablissementTransporteur = xEbEtablissementTransporteur;
    }

    public String getNomEtablissementTransporteur() {
        return nomEtablissementTransporteur;
    }

    public void setNomEtablissementTransporteur(String nomEtablissementTransporteur) {
        this.nomEtablissementTransporteur = nomEtablissementTransporteur;
    }

    public EbCompagnie getxEbCompagnieTransporteur() {
        return xEbCompagnieTransporteur;
    }

    public void setxEbCompagnieTransporteur(EbCompagnie xEbCompagnieTransporteur) {
        this.xEbCompagnieTransporteur = xEbCompagnieTransporteur;
    }

    public EbUser getxEbChargeur() {
        return xEbChargeur;
    }

    public void setxEbChargeur(EbUser xEbChargeur) {
        this.xEbChargeur = xEbChargeur;
    }

    public EbEtablissement getxEbEtablissementChargeur() {
        return xEbEtablissementChargeur;
    }

    public void setxEbEtablissementChargeur(EbEtablissement xEbEtablissementChargeur) {
        this.xEbEtablissementChargeur = xEbEtablissementChargeur;
    }

    public String getNomEtablissementChargeur() {
        return nomEtablissementChargeur;
    }

    public void setNomEtablissementChargeur(String nomEtablissementChargeur) {
        this.nomEtablissementChargeur = nomEtablissementChargeur;
    }

    public EbCompagnie getxEbChargeurCompagnie() {
        return xEbChargeurCompagnie;
    }

    public void setxEbChargeurCompagnie(EbCompagnie xEbChargeurCompagnie) {
        this.xEbChargeurCompagnie = xEbChargeurCompagnie;
    }

    public TtEnumeration.Late getLate() {
        return late;
    }

    public void setLate(TtEnumeration.Late late) {
        this.late = late;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getConfigPsl() {
        return configPsl;
    }

    public void setConfigPsl(String configPsl) {
        this.configPsl = configPsl;
    }

    public List<EbTtEvent> getListEvent() {
        return listEvent;
    }

    public void setListEvent(List<EbTtEvent> listEvent) {
        this.listEvent = listEvent;
    }

    public String getCustomerRefTransportRef() {
        return this.customerRefTransportRef;
    }

    public void setCustomerRefTransportRef(String customerRefTransportRef) {
        this.customerRefTransportRef = customerRefTransportRef;
    }

    public Integer getLeadtime() {
        return leadtime;
    }

    public void setLeadtime(Integer leadtime) {
        this.leadtime = leadtime;
    }

    public String getOriginPlace() {
        return originPlace;
    }

    public void setOriginPlace(String originPlace) {
        this.originPlace = originPlace;
    }

    public String getDestinationPlace() {
        return destinationPlace;
    }

    public void setDestinationPlace(String destinationPlace) {
        this.destinationPlace = destinationPlace;
    }

    public Integer getxEcModeTransport() {
        return xEcModeTransport;
    }

    public void setxEcModeTransport(Integer xEcModeTransport) {
        this.xEcModeTransport = xEcModeTransport;
    }

    public String getCustomRef() {
        return customRef;
    }

    public void setCustomRef(String customRef) {
        this.customRef = customRef;
    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public String getListLabels() {
        return listLabels;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public void setListLabels(String listLabels) {
        this.listLabels = listLabels;
    }

    public EbUser getxEbUserCt() {
        return xEbUserCt;
    }

    public EbEtablissement getxEbEtablissementCt() {
        return xEbEtablissementCt;
    }

    public EbCompagnie getxEbCompagnieCt() {
        return xEbCompagnieCt;
    }

    public void setxEbUserCt(EbUser xEbUserCt) {
        this.xEbUserCt = xEbUserCt;
    }

    public void setxEbEtablissementCt(EbEtablissement xEbEtablissementCt) {
        this.xEbEtablissementCt = xEbEtablissementCt;
    }

    public void setxEbCompagnieCt(EbCompagnie xEbCompagnieCt) {
        this.xEbCompagnieCt = xEbCompagnieCt;
    }

    public EbMarchandise getxEbMarchandise() {
        return xEbMarchandise;
    }

    public void setxEbMarchandise(EbMarchandise xEbMarchandise) {
        this.xEbMarchandise = xEbMarchandise;
    }

    public String getLibelleLastPsl() {
        return libelleLastPsl;
    }

    public Integer getCodeLastPsl() {
        return codeLastPsl;
    }

    public Date getDateLastPsl() {
        return dateLastPsl;
    }

    public void setLibelleLastPsl(String libelleLastPsl) {
        this.libelleLastPsl = libelleLastPsl;
    }

    public void setCodeLastPsl(Integer codeLastPsl) {
        this.codeLastPsl = codeLastPsl;
    }

    public void setDateLastPsl(Date dateLastPsl) {
        this.dateLastPsl = dateLastPsl;
    }

    public List<EbCategorie> getListCategories() {
        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {
        this.listCategories = listCategories;
    }

    public String getTransportStatusPerUnit() {
        return transportStatusPerUnit;
    }

    public void setTransportStatusPerUnit(String transportStatusPerUnit) {
        this.transportStatusPerUnit = transportStatusPerUnit;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((customerReference == null) ? 0 : customerReference.hashCode());
        result = prime * result + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        result = prime * result + ((dateModification == null) ? 0 : dateModification.hashCode());
        result = prime * result + ((datePickup == null) ? 0 : datePickup.hashCode());
        result = prime * result + ((ebTtTracingNum == null) ? 0 : ebTtTracingNum.hashCode());
        result = prime * result + ((late == null) ? 0 : late.hashCode());
        result = prime * result + ((listEbPslApp == null) ? 0 : listEbPslApp.hashCode());
        result = prime * result + ((nomEtablissementChargeur == null) ? 0 : nomEtablissementChargeur.hashCode());
        result = prime * result
            + ((nomEtablissementTransporteur == null) ? 0 : nomEtablissementTransporteur.hashCode());
        result = prime * result + ((refTransport == null) ? 0 : refTransport.hashCode());
        result = prime * result + ((totalWeight == null) ? 0 : totalWeight.hashCode());
        result = prime * result + ((xEbChargeur == null) ? 0 : xEbChargeur.hashCode());
        result = prime * result + ((xEbChargeurCompagnie == null) ? 0 : xEbChargeurCompagnie.hashCode());
        result = prime * result + ((xEbCompagnieTransporteur == null) ? 0 : xEbCompagnieTransporteur.hashCode());
        result = prime * result + ((xEbDemande == null) ? 0 : xEbDemande.hashCode());
        result = prime * result + ((xEbEtablissementChargeur == null) ? 0 : xEbEtablissementChargeur.hashCode());
        result = prime * result
            + ((xEbEtablissementTransporteur == null) ? 0 : xEbEtablissementTransporteur.hashCode());
        result = prime * result + ((xEbTransporteur == null) ? 0 : xEbTransporteur.hashCode());
        result = prime * result + ((xEcCountryDestination == null) ? 0 : xEcCountryDestination.hashCode());
        result = prime * result + ((xEcCountryOrigin == null) ? 0 : xEcCountryOrigin.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbTtTracing other = (EbTtTracing) obj;

        if (customerReference == null) {
            if (other.customerReference != null) return false;
        }
        else if (!customerReference.equals(other.customerReference)) return false;

        if (dateCreation == null) {
            if (other.dateCreation != null) return false;
        }
        else if (!dateCreation.equals(other.dateCreation)) return false;

        if (dateModification == null) {
            if (other.dateModification != null) return false;
        }
        else if (!dateModification.equals(other.dateModification)) return false;

        if (datePickup == null) {
            if (other.datePickup != null) return false;
        }
        else if (!datePickup.equals(other.datePickup)) return false;

        if (ebTtTracingNum == null) {
            if (other.ebTtTracingNum != null) return false;
        }
        else if (!ebTtTracingNum.equals(other.ebTtTracingNum)) return false;

        if (late != other.late) return false;

        if (listEbPslApp == null) {
            if (other.listEbPslApp != null) return false;
        }
        else if (!listEbPslApp.equals(other.listEbPslApp)) return false;

        if (nomEtablissementChargeur == null) {
            if (other.nomEtablissementChargeur != null) return false;
        }
        else if (!nomEtablissementChargeur.equals(other.nomEtablissementChargeur)) return false;

        if (nomEtablissementTransporteur == null) {
            if (other.nomEtablissementTransporteur != null) return false;
        }
        else if (!nomEtablissementTransporteur.equals(other.nomEtablissementTransporteur)) return false;

        if (refTransport == null) {
            if (other.refTransport != null) return false;
        }
        else if (!refTransport.equals(other.refTransport)) return false;

        if (totalWeight == null) {
            if (other.totalWeight != null) return false;
        }
        else if (!totalWeight.equals(other.totalWeight)) return false;

        if (xEbChargeur == null) {
            if (other.xEbChargeur != null) return false;
        }
        else if (!xEbChargeur.equals(other.xEbChargeur)) return false;

        if (xEbChargeurCompagnie == null) {
            if (other.xEbChargeurCompagnie != null) return false;
        }
        else if (!xEbChargeurCompagnie.equals(other.xEbChargeurCompagnie)) return false;

        if (xEbCompagnieTransporteur == null) {
            if (other.xEbCompagnieTransporteur != null) return false;
        }
        else if (!xEbCompagnieTransporteur.equals(other.xEbCompagnieTransporteur)) return false;

        if (xEbDemande == null) {
            if (other.xEbDemande != null) return false;
        }
        else if (!xEbDemande.equals(other.xEbDemande)) return false;

        if (xEbEtablissementChargeur == null) {
            if (other.xEbEtablissementChargeur != null) return false;
        }
        else if (!xEbEtablissementChargeur.equals(other.xEbEtablissementChargeur)) return false;

        if (xEbEtablissementTransporteur == null) {
            if (other.xEbEtablissementTransporteur != null) return false;
        }
        else if (!xEbEtablissementTransporteur.equals(other.xEbEtablissementTransporteur)) return false;

        if (xEbTransporteur == null) {
            if (other.xEbTransporteur != null) return false;
        }
        else if (!xEbTransporteur.equals(other.xEbTransporteur)) return false;

        if (xEcCountryDestination == null) {
            if (other.xEcCountryDestination != null) return false;
        }
        else if (!xEcCountryDestination.equals(other.xEcCountryDestination)) return false;

        if (xEcCountryOrigin == null) {
            if (other.xEcCountryOrigin != null) return false;
        }
        else if (!xEcCountryOrigin.equals(other.xEcCountryOrigin)) return false;

        return true;
    }

    public EbTtSchemaPsl getxEbSchemaPsl() {
        return xEbSchemaPsl;
    }

    public void setxEbSchemaPsl(EbTtSchemaPsl xEbSchemaPsl) {
        this.xEbSchemaPsl = xEbSchemaPsl;
    }

    public String getCodeAlphaPslCourant() {
        return codeAlphaPslCourant;
    }

    public String getLibellePslCourant() {
        return libellePslCourant;
    }

    public String getCodeAlphaLastPsl() {
        return codeAlphaLastPsl;
    }

    public void setCodeAlphaPslCourant(String codeAlphaPslCourant) {
        this.codeAlphaPslCourant = codeAlphaPslCourant;
    }

    public void setLibellePslCourant(String libellePslCourant) {
        this.libellePslCourant = libellePslCourant;
    }

    public void setCodeAlphaLastPsl(String codeAlphaLastPsl) {
        this.codeAlphaLastPsl = codeAlphaLastPsl;
    }

    public List<RegroupmtPslEvent> getListPslEvent() {
        return listPslEvent;
    }

    public void setListPslEvent(List<RegroupmtPslEvent> listPslEvent) {
        this.listPslEvent = listPslEvent;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getxEcIncotermLibelle() {
        return xEcIncotermLibelle;
    }

    public void setxEcIncotermLibelle(String xEcIncotermLibelle) {
        this.xEcIncotermLibelle = xEcIncotermLibelle;
    }

    public List<EbTtCompanyPsl> getListPsl() {
        return listPsl;
    }

    public void setListPsl(List<EbTtCompanyPsl> listPsl) {
        this.listPsl = listPsl;
    }

    public Double getPercentPslCourant() {
        return percentPslCourant;
    }

    public void setPercentPslCourant(Double percentPslCourant) {
        this.percentPslCourant = percentPslCourant;
    }

    public String getCompanyOrigin() {
        return companyOrigin;
    }

    public void setCompanyOrigin(String companyOrigin) {
        this.companyOrigin = companyOrigin;
    }

    public String getListControlRule() {
        return listControlRule;
    }

    public void setListControlRule(String listControlRule) {
        this.listControlRule = listControlRule;
    }

    public String getPackingList() {
        return packingList;
    }

    public void setPackingList(String packingList) {
        this.packingList = packingList;
    }
}
