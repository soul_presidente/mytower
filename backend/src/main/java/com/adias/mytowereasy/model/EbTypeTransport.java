package com.adias.mytowereasy.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;


@Entity
@Table(name = "eb_type_transport")
public class EbTypeTransport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebTypeTransportNum;

    private Integer xEcModeTransport;

    private String libelle;

    private Date creationDate;
    private Date lastUpdateDate;

    @PrePersist
    protected void onCreate() {
        creationDate = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        lastUpdateDate = new Date();
    }

    public Integer getxEcModeTransport() {
        return xEcModeTransport;
    }

    public void setxEcModeTransport(Integer xEcModeTransport) {
        this.xEcModeTransport = xEcModeTransport;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getEbTypeTransportNum() {
        return ebTypeTransportNum;
    }

    public void setEbTypeTransportNum(Integer ebTypeTransportNum) {
        this.ebTypeTransportNum = ebTypeTransportNum;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }
}
