package com.adias.mytowereasy.model;

import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;

import com.adias.mytowereasy.dock.model.EbDkOpeningDay;
import com.adias.mytowereasy.dto.EbRangeHourDTO;


@Entity
@Table(name = "eb_range_hours", schema = "work")
/**
 * Generic class for storing range hour data
 */
public class EbRangeHour {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer rangeHoursNum;

    @Temporal(TemporalType.TIME)
    private Date startHour;

    @Temporal(TemporalType.TIME)
    private Date endHour;

    // Used only for bdd model generation
    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_opening_day", insertable = true)
    private EbDkOpeningDay openingDay;

    public EbRangeHour() {
    }

    public EbRangeHour(Integer rangeHoursNum, Date startHour, Date endHour) {
        super();
        this.rangeHoursNum = rangeHoursNum;
        this.startHour = startHour;
        this.endHour = endHour;
    }

    public EbRangeHour(Date startHour, Date endHour) {
        this();
        this.startHour = startHour;
        this.endHour = endHour;
    }

    public EbRangeHour(EbRangeHourDTO dto) {
        this();
        this.setRangeHoursNum(dto.getRangeHoursNum());
        this.startHour = dto.getStartHour();
        this.endHour = dto.getEndHour();
    }

    public Integer getRangeHoursNum() {
        return rangeHoursNum;
    }

    public void setRangeHoursNum(Integer rangeHoursNum) {
        this.rangeHoursNum = rangeHoursNum;
    }

    public Date getStartHour() {
        return startHour;
    }

    public void setStartHour(Date startHour) {
        this.startHour = startHour;
    }

    public Date getEndHour() {
        return endHour;
    }

    public void setEndHour(Date endHour) {
        this.endHour = endHour;
    }

    public EbDkOpeningDay getOpeningDay() {
        return openingDay;
    }

    public void setOpeningDay(EbDkOpeningDay openingDay) {
        this.openingDay = openingDay;
    }
}
