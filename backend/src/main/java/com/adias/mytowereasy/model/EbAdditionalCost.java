package com.adias.mytowereasy.model;

import javax.persistence.*;


@Entity
@Table(name = "eb_additional_cost", schema = "work")
public class EbAdditionalCost {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebAdditionalCostNum;

    private String code;

    private String libelle;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", updatable = false)
    private EbCompagnie xEbCompagnie;

    private Boolean actived;

    public Integer getEbAdditionalCostNum() {
        return ebAdditionalCostNum;
    }

    public void setEbAdditionalCostNum(Integer ebAdditionalCostNum) {
        this.ebAdditionalCostNum = ebAdditionalCostNum;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Boolean getActived() {
        return actived;
    }

    public void setActived(Boolean active) {
        this.actived = active;
    }
}
