/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.qm;

import java.util.Date;

import javax.persistence.*;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;


/** EbQmTrackingStatus entity. @author Mohamed */
@Entity
// @Table(indexes = { @Index(name = "ebQmTrackingStatusNumIdx", columnList =
// "ebQmTrackingStatusNum"),
// @Index(name = "ebQmTrackingStatusIncidentIdx", columnList = "x_eb_incident"),
// @Index(name = "ebQmTrackingStatusUserIdx", columnList = "x_eb_user") })
@Table(name = "eb_qm_tracking_status", schema = "work")
public class EbQmTrackingStatus implements java.io.Serializable {
    private static final long serialVersionUID = -5047981751299718160L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebQmTrackingStatusNum;

    @Column
    private String ipAddress;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateAction;

    @Enumerated(EnumType.ORDINAL)
    private Enumeration.IncidentStatus oldStatut;

    @Enumerated(EnumType.ORDINAL)
    private Enumeration.IncidentStatus newStatut;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user")
    private EbUser user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_incident")
    private EbQmIncident incident;

    public Integer getEbQmTrackingStatusNum() {
        return ebQmTrackingStatusNum;
    }

    public void setEbQmTrackingStatusNum(Integer ebQmTrackingStatusNum) {
        this.ebQmTrackingStatusNum = ebQmTrackingStatusNum;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Date getDateAction() {
        return dateAction;
    }

    public void setDateAction(Date dateAction) {
        this.dateAction = dateAction;
    }

    public Enumeration.IncidentStatus getOldStatut() {
        return oldStatut;
    }

    public void setOldStatut(Enumeration.IncidentStatus oldStatut) {
        this.oldStatut = oldStatut;
    }

    public Enumeration.IncidentStatus getNewStatut() {
        return newStatut;
    }

    public void setNewStatut(Enumeration.IncidentStatus newStatut) {
        this.newStatut = newStatut;
    }

    public EbUser getUser() {
        return user;
    }

    public void setUser(EbUser user) {
        this.user = user;
    }

    public EbQmIncident getIncident() {
        return incident;
    }

    public void setIncident(EbQmIncident incident) {
        this.incident = incident;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateAction == null) ? 0 : dateAction.hashCode());
        result = prime * result + ((ebQmTrackingStatusNum == null) ? 0 : ebQmTrackingStatusNum.hashCode());
        result = prime * result + ((incident == null) ? 0 : incident.hashCode());
        result = prime * result + ((ipAddress == null) ? 0 : ipAddress.hashCode());
        result = prime * result + ((newStatut == null) ? 0 : newStatut.hashCode());
        result = prime * result + ((oldStatut == null) ? 0 : oldStatut.hashCode());
        result = prime * result + ((user == null) ? 0 : user.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbQmTrackingStatus other = (EbQmTrackingStatus) obj;

        if (dateAction == null) {
            if (other.dateAction != null) return false;
        }
        else if (!dateAction.equals(other.dateAction)) return false;

        if (ebQmTrackingStatusNum == null) {
            if (other.ebQmTrackingStatusNum != null) return false;
        }
        else if (!ebQmTrackingStatusNum.equals(other.ebQmTrackingStatusNum)) return false;

        if (incident == null) {
            if (other.incident != null) return false;
        }
        else if (!incident.equals(other.incident)) return false;

        if (ipAddress == null) {
            if (other.ipAddress != null) return false;
        }
        else if (!ipAddress.equals(other.ipAddress)) return false;

        if (newStatut != other.newStatut) return false;
        if (oldStatut != other.oldStatut) return false;

        if (user == null) {
            if (other.user != null) return false;
        }
        else if (!user.equals(other.user)) return false;

        return true;
    }
}
