/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@Entity
@Table(name = "eb_custom_field", schema = "work")
public class EbCustomField implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = 8172729099388561441L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebCustomFieldNum;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user", insertable = true, updatable = false)
    private EbUser ebUser;

    private Integer xEbCompagnie;

    private String fields;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String nomCompagnie;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    public EbCustomField() {
    }

    public EbCustomField(Integer ebCustomFieldNum, String fields) {
        this.ebCustomFieldNum = ebCustomFieldNum;
        this.fields = fields;

        if (this.fields != null && !this.fields.isEmpty()) {
            ObjectMapper objectMapper = new ObjectMapper();

            try {
                this.listCustomsFields = objectMapper
                    .readValue(this.fields, new TypeReference<ArrayList<CustomFields>>() {
                    });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public Integer getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(Integer xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Integer getEbCustomFieldNum() {
        return ebCustomFieldNum;
    }

    public void setEbCustomFieldNum(Integer ebCustomFieldNum) {
        this.ebCustomFieldNum = ebCustomFieldNum;
    }

    public EbUser getEbUser() {
        return ebUser;
    }

    public void setEbUser(EbUser ebUser) {
        this.ebUser = ebUser;
    }

    public String getFields() {
        return fields;
    }

    public void setFields(String fields) {
        this.fields = fields;
    }

    public String getNomCompagnie() {
        return nomCompagnie;
    }

    public void setNomCompagnie(String nomCompagnie) {
        this.nomCompagnie = nomCompagnie;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ebCustomFieldNum == null) ? 0 : ebCustomFieldNum.hashCode());
        result = prime * result + ((ebUser == null) ? 0 : ebUser.hashCode());
        result = prime * result + ((fields == null) ? 0 : fields.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbCustomField other = (EbCustomField) obj;

        if (ebCustomFieldNum == null) {
            if (other.ebCustomFieldNum != null) return false;
        }
        else if (!ebCustomFieldNum.equals(other.ebCustomFieldNum)) return false;

        if (ebUser == null) {
            if (other.ebUser != null) return false;
        }
        else if (!ebUser.equals(other.ebUser)) return false;

        if (fields == null) {
            if (other.fields != null) return false;
        }
        else if (!fields.equals(other.fields)) return false;

        return true;
    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }
}
