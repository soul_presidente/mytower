package com.adias.mytowereasy.model;

public class UnitChildren {
    private Integer palette;
    private Integer colis;
    private Integer article;

    public Integer getPalette() {
        return palette;
    }

    public void setPalette(Integer palette) {
        this.palette = palette;
    }

    public Integer getColis() {
        return colis;
    }

    public void setColis(Integer colis) {
        this.colis = colis;
    }

    public Integer getArticle() {
        return article;
    }

    public void setArticle(Integer article) {
        this.article = article;
    }
}
