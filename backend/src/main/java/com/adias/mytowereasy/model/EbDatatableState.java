/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import javax.persistence.*;


@Entity
@Table(name = "eb_datatable_state", schema = "work")
public class EbDatatableState {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebDatatableStateNum;

    @Column(columnDefinition = "TEXT")
    private String state;

    private Integer xEbUserNum;

    public Integer datatableCompId;

    public Integer getxEbUserNum() {
        return xEbUserNum;
    }

    public void setxEbUserNum(Integer xEbUserNum) {
        this.xEbUserNum = xEbUserNum;
    }

    public Integer getEbDatatableStateNum() {
        return ebDatatableStateNum;
    }

    public void setEbDatatableStateNum(Integer ebDatatableStateNum) {
        this.ebDatatableStateNum = ebDatatableStateNum;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getDatatableCompId() {
        return datatableCompId;
    }

    public void setDatatableCompId(Integer datatableCompId) {
        this.datatableCompId = datatableCompId;
    }
}
