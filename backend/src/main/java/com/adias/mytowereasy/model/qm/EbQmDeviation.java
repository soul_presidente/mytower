package com.adias.mytowereasy.model.qm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCountry;
import com.adias.mytowereasy.model.tt.EbTtTracing;


@Entity
@Table(name = "eb_qm_deviation", schema = "work")
public class EbQmDeviation implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebQmDeviationNum;
    private String typeDeviation;
    private String refDeviation;
    private String refTransport;
    private String unitRef;

    @Column(insertable = true, updatable = false)
    @ColumnDefault("now()")
    private Date deviationDate;

    @ManyToOne(cascade = {
        CascadeType.MERGE, CascadeType.REFRESH
    }, fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab_carrier")
    private EbEtablissement carrier;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country_origin")
    private EcCountry originCountry;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country_dest")
    private EcCountry destinationCountry;

    private String originCountryLibelle;
    private String destinationCountryLibelle;
    private String carrierName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_tt_tracing")
    private EbTtTracing xEbTtTracing;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user")
    private EbUser xEbUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement")
    private EbEtablissement xEbEtablissement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie")
    private EbCompagnie xEbCompagnie;

    @Column(name = "deleted", nullable = false, columnDefinition = "boolean default false")
    private Boolean deleted = false;

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Integer getEbQmDeviationNum() {
        return ebQmDeviationNum;
    }

    public void setEbQmDeviationNum(Integer ebQmDeviationNum) {
        this.ebQmDeviationNum = ebQmDeviationNum;
    }

    public String getTypeDeviation() {
        return typeDeviation;
    }

    public void setTypeDeviation(String typeDeviation) {
        this.typeDeviation = typeDeviation;
    }

    public String getRefDeviation() {
        return refDeviation;
    }

    public void setRefDeviation(String refDeviation) {
        this.refDeviation = refDeviation;
    }

    public String getRefTransport() {
        return refTransport;
    }

    public void setRefTransport(String refTransport) {
        this.refTransport = refTransport;
    }

    public String getUnitRef() {
        return unitRef;
    }

    public void setUnitRef(String unitRef) {
        this.unitRef = unitRef;
    }

    public Date getDeviationDate() {
        return deviationDate;
    }

    public void setDeviationDate(Date deviationDate) {
        this.deviationDate = deviationDate;
    }

    public EbEtablissement getCarrier() {
        return carrier;
    }

    public void setCarrier(EbEtablissement carrier) {
        this.carrier = carrier;
    }

    public EcCountry getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(EcCountry originCountry) {
        this.originCountry = originCountry;
    }

    public EcCountry getDestinationCountry() {
        return destinationCountry;
    }

    public void setDestinationCountry(EcCountry destinationCountry) {
        this.destinationCountry = destinationCountry;
    }

    public String getOriginCountryLibelle() {
        return originCountryLibelle;
    }

    public void setOriginCountryLibelle(String originCountryLibelle) {
        this.originCountryLibelle = originCountryLibelle;
    }

    public String getDestinationCountryLibelle() {
        return destinationCountryLibelle;
    }

    public void setDestinationCountryLibelle(String destinationCountryLibelle) {
        this.destinationCountryLibelle = destinationCountryLibelle;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public EbQmDeviation() {
        // TODO Auto-generated constructor stub
    }

    public EbUser getxEbUser() {
        return xEbUser;
    }

    public void setxEbUser(EbUser xEbUser) {
        this.xEbUser = xEbUser;
    }

    public EbEtablissement getxEbEtablissement() {
        return xEbEtablissement;
    }

    public void setxEbEtablissement(EbEtablissement xEbEtablissement) {
        this.xEbEtablissement = xEbEtablissement;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public EbTtTracing getxEbTtTracing() {
        return xEbTtTracing;
    }

    public void setxEbTtTracing(EbTtTracing xEbTtTracing) {
        this.xEbTtTracing = xEbTtTracing;
    }
}
