package com.adias.mytowereasy.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.adias.mytowereasy.delivery.model.EbOrderLine;


@Entity
@Table(indexes = {
    @Index(name = "idx_eb_type_unit_x_eb_compagnie", columnList = "x_eb_compagnie"),
})
public class EbTypeUnit extends EbTimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ebTypeUnitNum;

    private String libelle;

    private Double weight;

    private Double volume;

    private Double length;

    private Double width;

    private Double height;

    private Boolean gerbable;

    private Integer xEbTypeContainer;

    @Column(columnDefinition = "boolean default true")
    private boolean activated;

    @OneToMany(mappedBy = "xEbtypeOfUnit", fetch = FetchType.LAZY)
    private List<EbOrderLine> orderline;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie")
    private EbCompagnie xEbCompagnie;

    private String code;

    public Long getEbTypeUnitNum() {
        return ebTypeUnitNum;
    }

    public void setEbTypeUnitNum(Long ebTypeUnitNum) {
        this.ebTypeUnitNum = ebTypeUnitNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Boolean getGerbable() {
        return gerbable;
    }

    public void setGerbable(Boolean gerbable) {
        this.gerbable = gerbable;
    }

    public Integer getxEbTypeContainer() {
        return xEbTypeContainer;
    }

    public void setxEbTypeContainer(Integer xEbTypeContainer) {
        this.xEbTypeContainer = xEbTypeContainer;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean getActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }
}
