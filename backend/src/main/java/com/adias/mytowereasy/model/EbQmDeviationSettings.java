package com.adias.mytowereasy.model;

import javax.persistence.*;


@Entity
public class EbQmDeviationSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebQmDeviationSettingsNum;

    private String identifiant;
    private String libelle;
    private String type;
    private String sousType;
    private Integer xModule;
    private String objet;
    private String champs;
    private String detail;
    private String description;
    private Boolean active;
    private Boolean createDeviation;
    private Boolean sendAlert;
    private Boolean changeStatus;
    private Integer xEbCompagnie;

    public Integer getEbQmDeviationSettingsNum() {
        return ebQmDeviationSettingsNum;
    }

    public void setEbQmDeviationSettingsNum(Integer ebQmDeviationSettingsNum) {
        ebQmDeviationSettingsNum = ebQmDeviationSettingsNum;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSousType() {
        return sousType;
    }

    public void setSousType(String sousType) {
        this.sousType = sousType;
    }

    public Integer getxModule() {
        return xModule;
    }

    public void setxModule(Integer xModule) {
        this.xModule = xModule;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getChamps() {
        return champs;
    }

    public void setChamps(String champs) {
        this.champs = champs;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getCreateDeviation() {
        return createDeviation;
    }

    public void setCreateDeviation(Boolean createDeviation) {
        this.createDeviation = createDeviation;
    }

    public Boolean getSendAlert() {
        return sendAlert;
    }

    public void setSendAlert(Boolean sendAlert) {
        this.sendAlert = sendAlert;
    }

    public Boolean getChangeStatus() {
        return changeStatus;
    }

    public void setChangeStatus(Boolean changeStatus) {
        this.changeStatus = changeStatus;
    }

    public Integer getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(Integer xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }
}
