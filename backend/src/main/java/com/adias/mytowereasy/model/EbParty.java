/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.trpl.model.EbPlZone;


@Entity
@Table(indexes = {
    @Index(name = "idx_eb_party_x_ec_country", columnList = "x_ec_country"),
})
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedEntityGraphs({
    @NamedEntityGraph(name = "EbParty.detail", attributeNodes = {
        @NamedAttributeNode(value = "xEcCountry"), @NamedAttributeNode(value = "zone")
    }), @NamedEntityGraph(name = "EbParty.country_zone_etablissementAdresse", attributeNodes = {
        @NamedAttributeNode(value = "xEcCountry"),
        @NamedAttributeNode(value = "zone"),
        @NamedAttributeNode(value = "etablissementAdresse")
    })
})
public class EbParty implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Integer ebPartyNum;

    private String company;

    private String adresse;

    private String city;

    private String zipCode;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country", insertable = true)
    private EcCountry xEcCountry;

    // Don't set cascade all here, produce issues with point intermediate at
    // creation
    @ManyToOne(fetch = FetchType.LAZY, cascade = {
        CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH
    })
    @JoinColumn(name = "x_eb_zone", insertable = true)
    private EbPlZone zone;

    private String openingHours;

    private String airport;

    private String email;

    private String phone;

    @Column(columnDefinition = "TEXT")
    private String commentaire;

    @OneToMany(mappedBy = "xEbPartySale", fetch = FetchType.LAZY)
    private List<EbOrder> ordersSale;

    @OneToMany(mappedBy = "xEbPartyOrigin", fetch = FetchType.LAZY)
    private List<EbOrder> ordersOrigin;

    @OneToMany(mappedBy = "xEbPartyDestination", fetch = FetchType.LAZY)
    private List<EbOrder> ordersDestination;

    @OneToMany(mappedBy = "xEbPartySale", fetch = FetchType.LAZY)
    private List<EbLivraison> livraisonsSale;

    @OneToMany(mappedBy = "xEbPartyOrigin", fetch = FetchType.LAZY)
    private List<EbLivraison> livraisonsOrigin;

    @OneToMany(mappedBy = "xEbPartyDestination", fetch = FetchType.LAZY)
    private List<EbLivraison> livraisonsDestination;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "x_eb_etablissement_adresse", insertable = true)
    private EbEtablissement etablissementAdresse;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private EbUser xEbUserVisibility;

    private String reference;

    // Zone backup
    private Integer zoneNum;
    private String zoneRef;
    private String zoneDesignation;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer ebAdresseNum;

    @OneToMany(mappedBy = "xEbPartyVendor", fetch = FetchType.LAZY)
    private List<EbOrder> ordersVendor;

    @OneToMany(mappedBy = "xEbPartyIssuer", fetch = FetchType.LAZY)
    private List<EbOrder> ordersIssuer;

    public EbParty() {
    }

    public EbParty(Integer ebPartyNum) {
        this.ebPartyNum = ebPartyNum;
    }

    public EbParty(Integer ebPartyNum, String city) {
        this.ebPartyNum = ebPartyNum;
        this.city = city;
    }

    public EbParty(EbParty ebParty) {

        if (ebParty != null) {
            this.ebPartyNum = ebParty.getEbPartyNum();
            this.company = ebParty.getCompany();
            this.adresse = ebParty.getAdresse();
            this.city = ebParty.getCity();
            this.zipCode = ebParty.getZipCode();
            this.xEcCountry = new EcCountry();

            if (ebParty.getxEcCountry() != null && ebParty.getxEcCountry().getEcCountryNum() != null) {
                this.xEcCountry.setEcCountryNum(ebParty.getxEcCountry().getEcCountryNum());
                this.xEcCountry.setLibelle(ebParty.getxEcCountry().getLibelle());
                this.xEcCountry.setCode(ebParty.getxEcCountry().getCode());
            }

            if (ebParty.getZone() != null && ebParty.getZone().getEbZoneNum() != null) {
                this.zone = new EbPlZone();
                this.zone.setEbZoneNum(ebParty.getZone().getEbZoneNum());
                this.zone.setDesignation(ebParty.getZone().getDesignation());
                this.zone.setRef(ebParty.getZone().getRef());
                if (ebParty.getZone().getEbCompagnie() != null
                    && ebParty.getZone().getEbCompagnie().getEbCompagnieNum() != null) this.zone
                        .setEbCompagnie(new EbCompagnie(ebParty.getZone().getEbCompagnie().getEbCompagnieNum()));
            }

            this.openingHours = ebParty.getOpeningHours();
            this.airport = ebParty.getAirport();
            this.email = ebParty.getEmail();
            this.phone = ebParty.getPhone();
            this.commentaire = ebParty.getCommentaire();

            if (ebParty.getEtablissementAdresse() != null
                && ebParty.getEtablissementAdresse().getEbEtablissementNum() != null) {
                this.etablissementAdresse = new EbEtablissement(
                    ebParty.getEtablissementAdresse().getEbEtablissementNum(),
                    ebParty.getEtablissementAdresse().getNom());
            }

            this.reference = ebParty.getReference();
            this.zoneNum = ebParty.getZoneNum();
            this.zoneRef = ebParty.getZoneRef();
            this.zoneDesignation = ebParty.getZoneDesignation();
        }

    }

    public EbParty(String city) {
        super();
        this.city = city;
    }

    public Integer getEbPartyNum() {
        return ebPartyNum;
    }

    public void setEbPartyNum(Integer ebPartyNum) {
        this.ebPartyNum = ebPartyNum;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public EcCountry getxEcCountry() {
        return xEcCountry;
    }

    public void setxEcCountry(EcCountry xEcCountry) {
        this.xEcCountry = xEcCountry;
    }

    public String getOpeningHours() {
        return openingHours;
    }

    public void setOpeningHours(String openingHours) {
        this.openingHours = openingHours;
    }

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public String getCompanyReference() {
        return this.company + " (" + this.reference + ")";
    }

    public String getEtablissementReference() {

        if (this.getCompany() != null) {
            return this.getCompany() + " (" + this.reference + ")";
        }
        else {
            return "?" + " (" + this.reference + ")";
        }

    }

    public EbPlZone getZone() {
        return zone;
    }

    public void setZone(EbPlZone zone) {
        this.zone = zone;
    }

    public Integer getZoneNum() {
        return zoneNum;
    }

    public void setZoneNum(Integer zoneNum) {
        this.zoneNum = zoneNum;
    }

    public String getZoneRef() {
        return zoneRef;
    }

    public void setZoneRef(String zoneRef) {
        this.zoneRef = zoneRef;
    }

    public String getZoneDesignation() {
        return zoneDesignation;
    }

    public void setZoneDesignation(String zoneDesignation) {
        this.zoneDesignation = zoneDesignation;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((adresse == null) ? 0 : adresse.hashCode());
        result = prime * result + ((airport == null) ? 0 : airport.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((company == null) ? 0 : company.hashCode());
        result = prime * result + ((ebPartyNum == null) ? 0 : ebPartyNum.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((openingHours == null) ? 0 : openingHours.hashCode());
        result = prime * result + ((phone == null) ? 0 : phone.hashCode());
        result = prime * result + ((xEcCountry == null) ? 0 : xEcCountry.hashCode());
        result = prime * result + ((zipCode == null) ? 0 : zipCode.hashCode());
        result = prime * result + ((commentaire == null) ? 0 : commentaire.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbParty other = (EbParty) obj;

        if (adresse == null) {
            if (other.adresse != null) return false;
        }
        else if (!adresse.equals(other.adresse)) return false;

        if (airport == null) {
            if (other.airport != null) return false;
        }
        else if (!airport.equals(other.airport)) return false;

        if (city == null) {
            if (other.city != null) return false;
        }
        else if (!city.equals(other.city)) return false;

        if (company == null) {
            if (other.company != null) return false;
        }
        else if (!company.equals(other.company)) return false;

        if (ebPartyNum == null) {
            if (other.ebPartyNum != null) return false;
        }
        else if (!ebPartyNum.equals(other.ebPartyNum)) return false;

        if (email == null) {
            if (other.email != null) return false;
        }
        else if (!email.equals(other.email)) return false;

        if (openingHours == null) {
            if (other.openingHours != null) return false;
        }
        else if (!openingHours.equals(other.openingHours)) return false;

        if (phone == null) {
            if (other.phone != null) return false;
        }
        else if (!phone.equals(other.phone)) return false;

        if (xEcCountry == null) {
            if (other.xEcCountry != null) return false;
        }
        else if (!xEcCountry.equals(other.xEcCountry)) return false;

        if (zipCode == null) {
            if (other.zipCode != null) return false;
        }
        else if (!zipCode.equals(other.zipCode)) return false;

        if (commentaire == null) {
            if (other.commentaire != null) return false;
        }
        else if (!commentaire.equals(other.commentaire)) return false;

        return true;
    }

    public static EbParty fromEbAdresse(EbAdresse ebAdresse, EbEtablissement etablissement) {
        EbParty party = new EbParty();
        party.setAdresse(ebAdresse.getStreet());
        party.setAirport(ebAdresse.getAirport());
        party.setCity(ebAdresse.getCity());
        party.setCompany(ebAdresse.getCompany());
        party.setEmail(ebAdresse.getEmail());

        if (ebAdresse.getxEbUserVisibility() != null) {
            party.xEbUserVisibility = new EbUser();
            party.xEbUserVisibility.setEbUserNum(ebAdresse.getxEbUserVisibility().getEbUserNum());
            party.xEbUserVisibility.setNom(ebAdresse.getxEbUserVisibility().getNom());
            party.xEbUserVisibility
                .setEbEtablissement(
                    new EbEtablissement(
                        ebAdresse.getxEbUserVisibility().getEbEtablissement().getEbEtablissementNum(),
                        ebAdresse.getxEbUserVisibility().getEbEtablissement().getNom()));
        }

        if (etablissement != null) party.setEtablissementAdresse(etablissement);

        party.setPhone(ebAdresse.getPhone());
        party.setxEcCountry(ebAdresse.getCountry());
        party.setZipCode(ebAdresse.getZipCode());
        party.setReference(ebAdresse.getReference());
        party.setOpeningHours(ebAdresse.getOpeningHoursFreeText());
        party.setCommentaire(ebAdresse.getCommentaire());
        party.setZone(ebAdresse.getDefaultZone());
        party.setEbAdresseNum(ebAdresse.getEbAdresseNum());

        return party;
    }

    public EbEtablissement getEtablissementAdresse() {
        return etablissementAdresse;
    }

    public void setEtablissementAdresse(EbEtablissement etablissementAdresse) {
        this.etablissementAdresse = etablissementAdresse;
    }

    public List<EbOrder> getOrdersSale() {
        return ordersSale;
    }

    public List<EbOrder> getOrdersOrigin() {
        return ordersOrigin;
    }

    public List<EbOrder> getOrdersDestination() {
        return ordersDestination;
    }

    public List<EbLivraison> getLivraisonsSale() {
        return livraisonsSale;
    }

    public List<EbLivraison> getLivraisonsOrigin() {
        return livraisonsOrigin;
    }

    public List<EbLivraison> getLivraisonsDestination() {
        return livraisonsDestination;
    }

    public void setOrdersSale(List<EbOrder> ordersSale) {
        this.ordersSale = ordersSale;
    }

    public void setOrdersOrigin(List<EbOrder> ordersOrigin) {
        this.ordersOrigin = ordersOrigin;
    }

    public void setOrdersDestination(List<EbOrder> ordersDestination) {
        this.ordersDestination = ordersDestination;
    }

    public void setLivraisonsSale(List<EbLivraison> livraisonsSale) {
        this.livraisonsSale = livraisonsSale;
    }

    public void setLivraisonsOrigin(List<EbLivraison> livraisonsOrigin) {
        this.livraisonsOrigin = livraisonsOrigin;
    }

    public void setLivraisonsDestination(List<EbLivraison> livraisonsDestination) {
        this.livraisonsDestination = livraisonsDestination;
    }

    public EbUser getxEbUserVisibility() {
        return xEbUserVisibility;
    }

    public void setxEbUserVisibility(EbUser xEbUserVisibility) {
        this.xEbUserVisibility = xEbUserVisibility;
    }

    public Integer getEbAdresseNum() {
        return ebAdresseNum;
    }

    public void setEbAdresseNum(Integer ebAdresseNum) {
        this.ebAdresseNum = ebAdresseNum;
    }

    public List<EbOrder> getOrdersVendor() {
        return ordersVendor;
    }

    public void setOrdersVendor(List<EbOrder> ordersVendor) {
        this.ordersVendor = ordersVendor;
    }

    public List<EbOrder> getOrdersIssuer() {
        return ordersIssuer;
    }

    public void setOrdersIssuer(List<EbOrder> ordersIssuer) {
        this.ordersIssuer = ordersIssuer;
    }
}
