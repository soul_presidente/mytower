/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.analytics;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "dwr_d_country")
public class DwrDCountry {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dwr_d_country_num")
    private Integer dwrDCountryNum;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "destinationCountry", cascade = CascadeType.ALL)
    private List<DwrFShipments> destShipments = new ArrayList<DwrFShipments>();

    @OneToMany(mappedBy = "originCountry", cascade = CascadeType.ALL)
    private List<DwrFShipments> originShipments = new ArrayList<DwrFShipments>();

    public Integer getID() {
        return this.dwrDCountryNum;
    }

    String getName() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.name;
    }

    void setName(String value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.name = value;
    }

    List<DwrFShipments> getOriginShipments() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.originShipments;
    }

    void setOriginShipments(List<DwrFShipments> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.originShipments = value;
    }

    List<DwrFShipments> getDestShipments() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.destShipments;
    }

    void setDestShipments(List<DwrFShipments> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.destShipments = value;
    }
}
