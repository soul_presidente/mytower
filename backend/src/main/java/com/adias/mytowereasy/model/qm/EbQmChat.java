/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.qm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;


@Entity
@Table(name = "eb_qm_chat")
public class EbQmChat implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebQmChatNum;

    private Integer idFiche;
    private Integer idChatComponent;
    private Integer module;

    private String userName;
    private String userAvatar;

    @Column(columnDefinition = "TEXT")
    private String text;

    @ColumnDefault("now()")
    private Date dateCreation;

    private Boolean isHistory;

    public Integer getEbQmChatNum() {
        return ebQmChatNum;
    }

    public void setEbQmChatNum(Integer ebQmChatNum) {
        this.ebQmChatNum = ebQmChatNum;
    }

    public Integer getIdFiche() {
        return idFiche;
    }

    public void setIdFiche(Integer idFiche) {
        this.idFiche = idFiche;
    }

    public Integer getIdChatComponent() {
        return idChatComponent;
    }

    public void setIdChatComponent(Integer idChatComponent) {
        this.idChatComponent = idChatComponent;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Boolean getIsHistory() {
        return isHistory;
    }

    public void setIsHistory(Boolean isHistory) {
        this.isHistory = isHistory;
    }

    public EbQmChat() {
        super();
        // TODO Auto-generated constructor stub
    }
}
