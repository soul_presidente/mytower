package com.adias.mytowereasy.model;

import java.util.List;

import javax.persistence.*;


@Entity
@Table
public class EbUserGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ebUserGroupNum;

    private String name;

    @ManyToOne(cascade = {
        CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH
    }, fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie")
    private EbCompagnie ebCompagnie;

    @OneToMany(mappedBy = "group")
    private List<EbLabel> labels;

    public Long getEbUserGroupNum() {
        return ebUserGroupNum;
    }

    public void setEbUserGroupNum(Long ebUserGroupNum) {
        this.ebUserGroupNum = ebUserGroupNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EbLabel> getLabels() {
        return labels;
    }

    public void setLabels(List<EbLabel> labels) {
        this.labels = labels;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }
}
