/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.qm;

import java.sql.Timestamp;

import javax.persistence.*;

import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbTimeStamps;


/** EcQmRootCause entity. @author Mohamed */
@Entity
// @Table(indexes = { @Index(name = "ecQmRootCauseNumIdx", columnList =
// "qmRootCauseNum"),
// @Index(name = "ecQmRootCauseEtablissementIdx", columnList = "x_eb_etab") })
@Table(name = "eb_qm_root_cause", schema = "work")
public class EbQmRootCause extends EbTimeStamps implements java.io.Serializable {
    private static final long serialVersionUID = 1039761640894749808L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer qmRootCauseNum;

    @Column
    private String label;

    private String name;
    private String ref;
    private String groupe;
    private String relatedIncident;
    private String listIncidentNum;
    private Integer eventType;
    private String perimetreImpacte;
    private String userImpacter;
    private Integer severite;
    private String relatedActions;
    private String createur;
    private String montantCumuleCIL;
    private Integer statut;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab")
    private EbEtablissement etablissement;
    private String responsable;
    private Integer rootCauseCategory;

    private String actions;

    private String description;

    @QueryProjection
    public EbQmRootCause(
        Integer qmRootCauseNum,
        String label,

        String name,
        String ref,
        String groupe,
        String relatedIncident,
        String perimetreImpacte,
        String userImpacter,
        Integer severite,
        String relatedActions,
        String createur,
        String montantCumuleCIL,
        Integer statut,
        String responsable,
        String description,
        Timestamp dateCreationSys,
        Integer eventType,
        String listIncidentNum,
        String actions,
        Integer rootCauseCategory) {
        this.qmRootCauseNum = qmRootCauseNum;
        this.ref = ref;
        this.label = label;
        this.name = name;
        this.groupe = groupe;
        this.relatedIncident = relatedIncident;
        this.relatedActions = relatedActions;
        this.perimetreImpacte = perimetreImpacte;
        this.userImpacter = userImpacter;
        this.severite = severite;
        this.createur = createur;
        this.montantCumuleCIL = montantCumuleCIL;
        this.statut = statut;
        this.responsable = responsable;
        this.description = description;
        this.dateCreationSys = dateCreationSys;
        this.eventType = eventType;
        this.listIncidentNum = listIncidentNum;
        this.actions = actions;
        this.rootCauseCategory = rootCauseCategory;
    }

    public EbQmRootCause() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Integer getQmRootCauseNum() {
        return qmRootCauseNum;
    }

    public void setQmRootCauseNum(Integer qmRootCauseNum) {
        this.qmRootCauseNum = qmRootCauseNum;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public EbEtablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(EbEtablissement etablissement) {
        this.etablissement = etablissement;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getGroupe() {
        return groupe;
    }

    public void setGroupe(String groupe) {
        this.groupe = groupe;
    }

    public String getRelatedIncident() {
        return relatedIncident;
    }

    public void setRelatedIncident(String relatedIncident) {
        this.relatedIncident = relatedIncident;
    }

    public String getPerimetreImpacte() {
        return perimetreImpacte;
    }

    public void setPerimetreImpacte(String perimetreImpacte) {
        this.perimetreImpacte = perimetreImpacte;
    }

    public String getUserImpacter() {
        return userImpacter;
    }

    public void setUserImpacter(String userImpacter) {
        this.userImpacter = userImpacter;
    }

    public Integer getSeverite() {
        return severite;
    }

    public void setSeverite(Integer severite) {
        this.severite = severite;
    }

    public String getRelatedActions() {
        return relatedActions;
    }

    public void setRelatedActions(String relatedActions) {
        this.relatedActions = relatedActions;
    }

    public String getCreateur() {
        return createur;
    }

    public void setCreateur(String createur) {
        this.createur = createur;
    }

    public String getMontantCumuleCIL() {
        return montantCumuleCIL;
    }

    public void setMontantCumuleCIL(String montantCumuleCIL) {
        this.montantCumuleCIL = montantCumuleCIL;
    }

    public Integer getStatut() {
        return statut;
    }

    public void setStatut(Integer statut) {
        this.statut = statut;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public Integer getRootCauseCategory() {
        return rootCauseCategory;
    }

    public void setRootCauseCategory(Integer rootCauseCategory) {
        this.rootCauseCategory = rootCauseCategory;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getListIncidentNum() {
        return listIncidentNum;
    }

    public void setListIncidentNum(String listIncidentNum) {
        this.listIncidentNum = listIncidentNum;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }
}
