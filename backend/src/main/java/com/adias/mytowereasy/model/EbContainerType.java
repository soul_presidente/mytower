package com.adias.mytowereasy.model;

import javax.persistence.*;


@Entity
@Table
public class EbContainerType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ebContainerTypeNum;

    private String libelle;

    public Long getEbContainerTypeNum() {
        return ebContainerTypeNum;
    }

    public void setEbContainerTypeNum(Long ebContainerTypeNum) {
        this.ebContainerTypeNum = ebContainerTypeNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
