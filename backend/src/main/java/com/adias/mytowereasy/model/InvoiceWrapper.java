/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.ArrayList;
import java.util.List;


public class InvoiceWrapper {
    private List<Integer> listEbInvoiceNumber = new ArrayList<Integer>();
    private String selectValue;
    private EbInvoice ebInvoice;
    private List<Integer> lisAction = new ArrayList<Integer>();

    public EbInvoice getEbInvoice() {
        return ebInvoice;
    }

    public void setEbInvoice(EbInvoice ebInvoice) {
        this.ebInvoice = ebInvoice;
    }

    public List<Integer> getlistEbInvoiceNumber() {
        return listEbInvoiceNumber;
    }

    public void setListIvoice(List<Integer> listEbInvoiceNumber) {
        this.listEbInvoiceNumber = listEbInvoiceNumber;
    }

    public String getSelectValue() {
        return selectValue;
    }

    public void setSelectValue(String selectValue) {
        this.selectValue = selectValue;
    }

    public List<Integer> getLisAction() {
        return lisAction;
    }
}
