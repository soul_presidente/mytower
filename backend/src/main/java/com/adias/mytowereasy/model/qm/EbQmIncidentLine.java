/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.qm;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.model.EbEtablissement;


@Entity
@Table(name = "eb_qm_incident_line", schema = "work")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "ebQmIncidentLineNum",
    scope = EbQmIncidentLine.class)
@JsonIgnoreProperties(ignoreUnknown = true)

public class EbQmIncidentLine implements Serializable {
    /** */
    private static final long serialVersionUID = -1340408221866826057L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebQmIncidentLineNum;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_incident")
    private EbQmIncident incident;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab_carrier")
    private EbEtablissement carrier;

    private String unitReference;

    private Integer ebMarchandiseNum;

    private String typeMarchandise;

    private String carrierName;

    private Integer numberOfUnits;

    private Integer numberOfUnitsImpacted;

    private Integer weightImpacted;

    private Double weight;

    @Temporal(TemporalType.TIMESTAMP)
    private Date atd;

    @Temporal(TemporalType.TIMESTAMP)
    private Date eta;

    @Temporal(TemporalType.TIMESTAMP)
    private Date delayDepartureDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date ata;

    @Temporal(TemporalType.TIMESTAMP)
    private Date etd;

    @Temporal(TemporalType.TIMESTAMP)
    private Date delayArrivalDate;

    private Integer dangerousGood;
    private String comment;

    public EbQmIncidentLine() {
        super();
        // TODO Auto-generated constructor stub
    }

    @QueryProjection
    public EbQmIncidentLine(
        Integer ebQmIncidentLineNum,
        String unitReference,
        Integer numberOfUnits,
        Double weight,
        String carrierName,
        Date atd,
        Date eta,
        Date ata,
        Date etd,
        String typeMarchandise,
        Integer ebMarchandiseNum,
        Integer dangerousGood,
        String comment,
        Integer weightImpacted,
        Integer numberOfUnitsImpacted,
        Date delayDepartureDate,
        Date deplayArrivalDate) {
        this.ebQmIncidentLineNum = ebQmIncidentLineNum;
        this.setUnitReference(unitReference);
        this.setWeight(weight);
        this.carrierName = carrierName;
        this.incident = null;
        this.numberOfUnits = numberOfUnits;
        this.atd = atd;
        this.eta = eta;
        this.ata = ata;
        this.etd = etd;
        this.setTypeMarchandise(typeMarchandise);
        this.setEbMarchandiseNum(ebMarchandiseNum);
        this.dangerousGood = dangerousGood;
        this.comment = comment;
        this.weightImpacted = weightImpacted;
        this.numberOfUnitsImpacted = numberOfUnitsImpacted;
        this.delayDepartureDate = delayDepartureDate;
        this.delayArrivalDate = deplayArrivalDate;
    }

    public Integer getEbQmIncidentLineNum() {
        return ebQmIncidentLineNum;
    }

    public void setEbQmIncidentLineNum(Integer ebQmIncidentLineNum) {
        this.ebQmIncidentLineNum = ebQmIncidentLineNum;
    }

    public EbQmIncident getIncident() {
        return incident;
    }

    public void setIncident(EbQmIncident incident) {
        this.incident = incident;
    }

    public EbEtablissement getCarrier() {
        return carrier;
    }

    public void setCarrier(EbEtablissement carrier) {
        this.carrier = carrier;
    }

    public String getUnitReference() {
        return unitReference;
    }

    public void setUnitReference(String unitReference) {
        this.unitReference = unitReference;
    }

    public String getCarrierName() {
        return carrierName;
    }

    public void setCarrierName(String carrierName) {
        this.carrierName = carrierName;
    }

    public Integer getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(Integer numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getEbMarchandiseNum() {
        return ebMarchandiseNum;
    }

    public void setEbMarchandiseNum(Integer ebMarchandiseNum) {
        this.ebMarchandiseNum = ebMarchandiseNum;
    }

    public String getTypeMarchandise() {
        return typeMarchandise;
    }

    public void setTypeMarchandise(String typeMarchandise) {
        this.typeMarchandise = typeMarchandise;
    }

    public Date getAtd() {
        return atd;
    }

    public void setAtd(Date atd) {
        this.atd = atd;
    }

    public Date getEta() {
        return eta;
    }

    public void setEta(Date eta) {
        this.eta = eta;
    }

    public Date getAta() {
        return ata;
    }

    public void setAta(Date ata) {
        this.ata = ata;
    }

    public Date getEtd() {
        return etd;
    }

    public void setEtd(Date etd) {
        this.etd = etd;
    }

    public Integer getDangerousGood() {
        return dangerousGood;
    }

    public void setDangerousGood(Integer dangerousGood) {
        this.dangerousGood = dangerousGood;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getNumberOfUnitsImpacted() {
        return numberOfUnitsImpacted;
    }

    public void setNumberOfUnitsImpacted(Integer numberOfUnitsImpacted) {
        this.numberOfUnitsImpacted = numberOfUnitsImpacted;
    }

    public Integer getWeightImpacted() {
        return weightImpacted;
    }

    public void setWeightImpacted(Integer weightImpacted) {
        this.weightImpacted = weightImpacted;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((carrier == null) ? 0 : carrier.hashCode());
        result = prime * result + ((ebQmIncidentLineNum == null) ? 0 : ebQmIncidentLineNum.hashCode());
        result = prime * result + ((incident == null) ? 0 : incident.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbQmIncidentLine other = (EbQmIncidentLine) obj;

        if (carrier == null) {
            if (other.carrier != null) return false;
        }
        else if (!carrier.equals(other.carrier)) return false;

        if (ebQmIncidentLineNum == null) {
            if (other.ebQmIncidentLineNum != null) return false;
        }
        else if (!ebQmIncidentLineNum.equals(other.ebQmIncidentLineNum)) return false;

        if (incident == null) {
            if (other.incident != null) return false;
        }
        else if (!incident.equals(other.incident)) return false;

        return true;
    }

    public Date getDelayDepartureDate() {
        return delayDepartureDate;
    }

    public void setDelayDepartureDate(Date delayDepartureDate) {
        this.delayDepartureDate = delayDepartureDate;
    }

    public Date getDelayArrivalDate() {
        return delayArrivalDate;
    }

    public void setDelayArrivalDate(Date deplayArrivalDate) {
        this.delayArrivalDate = deplayArrivalDate;
    }
}
