/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.qm;

import java.util.List;


public class SemiAutoMail {
    private String to;
    private String subject;
    private String text;
    private List<String> enCopie;
    private List<String> enCopieCachee;

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<String> getEnCopie() {
        return enCopie;
    }

    public void setEnCopie(List<String> enCopie) {
        this.enCopie = enCopie;
    }

    public List<String> getEnCopieCachee() {
        return enCopieCachee;
    }

    public void setEnCopieCachee(List<String> enCopieCachee) {
        this.enCopieCachee = enCopieCachee;
    }
}
