package com.adias.mytowereasy.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table
public class EbDemandeLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ebDemandeLineNum;

    private String refTransport;

    private String numTransport;

    private String refDelivery;

    private String adresse;

    private String shipTo;

    private String zipCode;

    private String city;

    private String country;

    private Long transportLine;

    public Long getEbDemandeLineNum() {
        return ebDemandeLineNum;
    }

    public void setEbDemandeLineNum(Long ebDemandeLineNum) {
        this.ebDemandeLineNum = ebDemandeLineNum;
    }

    public String getRefTransport() {
        return refTransport;
    }

    public void setRefTransport(String refTransport) {
        this.refTransport = refTransport;
    }

    public String getNumTransport() {
        return numTransport;
    }

    public void setNumTransport(String numTransport) {
        this.numTransport = numTransport;
    }

    public String getRefDelivery() {
        return refDelivery;
    }

    public void setRefDelivery(String refDelivery) {
        this.refDelivery = refDelivery;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Long getTransportLine() {
        return transportLine;
    }

    public void setTransportLine(Long transportLine) {
        this.transportLine = transportLine;
    }

    public String getShipTo() {
        return shipTo;
    }

    public void setShipTo(String shipTo) {
        this.shipTo = shipTo;
    }
}
