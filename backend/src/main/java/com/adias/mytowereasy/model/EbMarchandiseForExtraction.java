package com.adias.mytowereasy.model;

public class EbMarchandiseForExtraction {
    private Integer ebMarchandiseNum;

    private String nomArticle;
    private String originCountryName;
    private Integer xEbTypeConteneur;

    private String unitReferenceArticle;
    private String lotArticle;
    private String batchArticle;
    private String lotQuantiteArticle;

    private String unitReferenceParcel;
    private String lotParcel;
    private String batchParcel;
    private String lotQuantiteParcel;

    private String unitReferencePalette;
    private String lotPalette;
    private String batchPalette;
    private String lotQuantitePalette;

    private String unitReferenceContainer;
    private String lotContainer;
    private String batchContainer;
    private String lotQuantiteContainer;

    public Integer getEbMarchandiseNum() {
        return ebMarchandiseNum;
    }

    public String getNomArticle() {
        return nomArticle;
    }

    public String getOriginCountryName() {
        return originCountryName;
    }

    public String getUnitReferenceArticle() {
        return unitReferenceArticle;
    }

    public String getLotArticle() {
        return lotArticle;
    }

    public String getBatchArticle() {
        return batchArticle;
    }

    public String getUnitReferenceParcel() {
        return unitReferenceParcel;
    }

    public String getLotParcel() {
        return lotParcel;
    }

    public String getBatchParcel() {
        return batchParcel;
    }

    public String getUnitReferencePalette() {
        return unitReferencePalette;
    }

    public String getLotPalette() {
        return lotPalette;
    }

    public String getBatchPalette() {
        return batchPalette;
    }

    public String getUnitReferenceContainer() {
        return unitReferenceContainer;
    }

    public String getLotContainer() {
        return lotContainer;
    }

    public String getBatchContainer() {
        return batchContainer;
    }

    public void setEbMarchandiseNum(Integer ebMarchandiseNum) {
        this.ebMarchandiseNum = ebMarchandiseNum;
    }

    public void setNomArticle(String nomArticle) {
        this.nomArticle = nomArticle;
    }

    public void setOriginCountryName(String originCountryName) {
        this.originCountryName = originCountryName;
    }

    public void setUnitReferenceArticle(String unitReferenceArticle) {
        this.unitReferenceArticle = unitReferenceArticle;
    }

    public void setLotArticle(String lotArticle) {
        this.lotArticle = lotArticle;
    }

    public void setBatchArticle(String batchArticle) {
        this.batchArticle = batchArticle;
    }

    public void setUnitReferenceParcel(String unitReferenceParcel) {
        this.unitReferenceParcel = unitReferenceParcel;
    }

    public void setLotParcel(String lotParcel) {
        this.lotParcel = lotParcel;
    }

    public void setBatchParcel(String batchParcel) {
        this.batchParcel = batchParcel;
    }

    public void setUnitReferencePalette(String unitReferencePalette) {
        this.unitReferencePalette = unitReferencePalette;
    }

    public void setLotPalette(String lotPalette) {
        this.lotPalette = lotPalette;
    }

    public void setBatchPalette(String batchPalette) {
        this.batchPalette = batchPalette;
    }

    public void setUnitReferenceContainer(String unitReferenceContainer) {
        this.unitReferenceContainer = unitReferenceContainer;
    }

    public void setLotContainer(String lotContainer) {
        this.lotContainer = lotContainer;
    }

    public void setBatchContainer(String batchContainer) {
        this.batchContainer = batchContainer;
    }

    public Integer getxEbTypeConteneur() {
        return xEbTypeConteneur;
    }

    public String getLotQuantiteArticle() {
        return lotQuantiteArticle;
    }

    public String getLotQuantiteParcel() {
        return lotQuantiteParcel;
    }

    public String getLotQuantitePalette() {
        return lotQuantitePalette;
    }

    public String getLotQuantiteContainer() {
        return lotQuantiteContainer;
    }

    public void setLotQuantiteArticle(String lotQuantiteArticle) {
        this.lotQuantiteArticle = lotQuantiteArticle;
    }

    public void setLotQuantiteParcel(String lotQuantiteParcel) {
        this.lotQuantiteParcel = lotQuantiteParcel;
    }

    public void setLotQuantitePalette(String lotQuantitePalette) {
        this.lotQuantitePalette = lotQuantitePalette;
    }

    public void setLotQuantiteContainer(String lotQuantiteContainer) {
        this.lotQuantiteContainer = lotQuantiteContainer;
    }

    public void setxEbTypeConteneur(Integer xEbTypeConteneur) {
        this.xEbTypeConteneur = xEbTypeConteneur;
    }
}
