package com.adias.mytowereasy.model;

import javax.persistence.*;


@Entity
@Table(name = "Ec_fuseaux_horaire")
public class EcFuseauxHoraire {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ec_fuseaux_horaire_num")
    private Integer ecFuseauHoraireNum;
    private Integer decalage;
    private String code;
    private String libelle;

    public Integer getEcFuseauHoraireNum() {
        return ecFuseauHoraireNum;
    }

    public void setEcFuseauHoraireNum(Integer ecFuseauHoraireNum) {
        this.ecFuseauHoraireNum = ecFuseauHoraireNum;
    }

    public Integer getDecalage() {
        return decalage;
    }

    public void setDecalage(Integer decalage) {
        this.decalage = decalage;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
