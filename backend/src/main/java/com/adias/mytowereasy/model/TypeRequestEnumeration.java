package com.adias.mytowereasy.model;

import com.adias.mytowereasy.util.GenericEnum;


public class TypeRequestEnumeration {
    public enum BlocageResponsePricing implements GenericEnum {
        YES(1, "YES"), NO(2, "NO");

        private Integer code;
        private String key;

        private BlocageResponsePricing(Integer code, String key) {
            this.code = code;
            this.key = key;
        }

        @Override
        public Integer getCode() {
            return this.code;
        }

        @Override
        public String getKey() {
            return this.key;
        }
    }

    public enum SenseChronoPricing implements GenericEnum {
        TIME_REMAINING(1, "TIME_REMAINING"), TYPE_SINCE_ISSUANCE(2, "TYPE_SINCE_ISSUANCE");

        private Integer code;
        private String key;

        private SenseChronoPricing(Integer code, String key) {
            this.code = code;
            this.key = key;
        }

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }
    }
}
