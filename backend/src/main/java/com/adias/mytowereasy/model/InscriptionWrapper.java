/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.List;

import javax.validation.Valid;


public class InscriptionWrapper {
    @Valid
    private EbUser ebUser;

    private List<String> friendInvitationEmails;

    private List<String> collegueInvitationEmails;

    public EbUser getEbUser() {
        return ebUser;
    }

    public void setEbUser(EbUser ebUser) {
        this.ebUser = ebUser;
    }

    public List<String> getCollegueInvitationEmails() {
        return collegueInvitationEmails;
    }

    public void setCollegueInvitationEmails(List<String> collegueInvitationEmails) {
        this.collegueInvitationEmails = collegueInvitationEmails;
    }

    public List<String> getFriendInvitationEmails() {
        return friendInvitationEmails;
    }

    public void setFriendInvitationEmails(List<String> friendInvitationEmails) {
        this.friendInvitationEmails = friendInvitationEmails;
    }
}
