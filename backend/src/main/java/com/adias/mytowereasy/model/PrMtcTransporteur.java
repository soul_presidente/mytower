package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.mtc.model.PrMtcFieldDTO;


@Entity
@Table(name = "pr_mtc_Transporteur", schema = "work")
public class PrMtcTransporteur implements Serializable
{
	private static final long		serialVersionUID	= 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer							prMtcTransporteurNum;

    // Code MTG field
    private String codeTransporteurMtc;

    private String codeConfigurationMtc;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_company_chargeur", insertable = true, updatable = false)
    private EbCompagnie xEbCompagnieChargeur;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_company_transporteur", insertable = true)
    private EbCompagnie xEbCompagnieTransporteur;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_user", insertable = true)
    private EbUser xEbUser;

    private String codeConfigurationEDI;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<PrMtcFieldDTO> attributes;

    public Integer getPrMtcTransporteurNum() {
        return prMtcTransporteurNum;
    }

    public void setPrMtcTransporteurNum(Integer prMtcTransporteurNum) {
        this.prMtcTransporteurNum = prMtcTransporteurNum;
    }

    public String getCodeTransporteurMtc() {
        return codeTransporteurMtc;
    }

    public void setCodeTransporteurMtc(String codeTransporteurMtc) {
        this.codeTransporteurMtc = codeTransporteurMtc;
    }

    public EbCompagnie getxEbCompagnieChargeur() {
        return xEbCompagnieChargeur;
    }

    public void setxEbCompagnieChargeur(EbCompagnie xEbCompagnieChargeur) {
        this.xEbCompagnieChargeur = xEbCompagnieChargeur;
    }

    public EbCompagnie getxEbCompagnieTransporteur() {
        return xEbCompagnieTransporteur;
    }

    public void setxEbCompagnieTransporteur(EbCompagnie xEbCompagnieTransporteur) {
        this.xEbCompagnieTransporteur = xEbCompagnieTransporteur;
    }

    public EbUser getxEbUser() {
        return xEbUser;
    }

    public void setxEbUser(EbUser xEbUser) {
        this.xEbUser = xEbUser;
    }

    @QueryProjection
    public PrMtcTransporteur(
        Integer prMtcTransporteurNum,
        Integer xEbCompagnieNum,
        String xEbCompagnieCode,
        String xEbCompagnieNom,
        String codeTransporteurMtc,
        Integer xEbUserNum,
        String xEbUserNom,
        String xEbUserPrenom,
        String xEbUserEmail,
        String xEbUserName,
        String codeConfigurationMtc,
        Integer xEbEtablissementNum,
        String xEbEtablissementCode,
        String xEbEtablissementNom,
        String codeConfigurationEDI) {
        this.prMtcTransporteurNum = prMtcTransporteurNum;
        this.codeConfigurationMtc = codeConfigurationMtc;
        this.xEbCompagnieTransporteur = new EbCompagnie(xEbCompagnieNum, xEbCompagnieNom, xEbCompagnieCode);
        EbEtablissement carrierEtab = new EbEtablissement(
            xEbEtablissementNum,
            xEbEtablissementCode,
            xEbEtablissementNom);
        this.codeTransporteurMtc = codeTransporteurMtc;
        this.xEbUser = new EbUser(xEbUserNum, xEbUserNom, xEbUserPrenom, xEbUserEmail);
        this.xEbUser.setUsername(xEbUserName);
        this.xEbUser.setEbEtablissement(carrierEtab);
        this.codeConfigurationEDI = codeConfigurationEDI;
    }

    public PrMtcTransporteur(PrMtcTransporteur db) {
        this.xEbCompagnieTransporteur = db.xEbCompagnieTransporteur;
        this.xEbCompagnieChargeur = db.xEbCompagnieChargeur;
        this.codeTransporteurMtc = db.codeTransporteurMtc;
        this.xEbUser = db.xEbUser;
        this.codeConfigurationEDI = db.codeConfigurationEDI;
    }

    public PrMtcTransporteur(Integer prMtcTransporteurNum) {
        this.prMtcTransporteurNum = prMtcTransporteurNum;
    }

    public PrMtcTransporteur() {
    }

    public String getCodeConfigurationEDI() {
        return codeConfigurationEDI;
    }

    public void setCodeConfigurationEDI(String codeConfigurationEDI) {
        this.codeConfigurationEDI = codeConfigurationEDI;
    }

    public String getCodeConfigurationMtc() {
        return codeConfigurationMtc;
    }

    public void setCodeConfigurationMtc(String codeConfigurationMtc) {
        this.codeConfigurationMtc = codeConfigurationMtc;
    }

    public List<PrMtcFieldDTO> getAttributes() {
        return attributes;
    }

    public void setAttributes(List<PrMtcFieldDTO> attributes) {
        this.attributes = attributes;
    }
}
