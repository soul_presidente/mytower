/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.tt;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import com.adias.mytowereasy.cronjob.tt.model.TmpEvent;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbTimeStamps;
import com.adias.mytowereasy.model.EbUser;


@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "ebTtEventNum",
    scope = EbTtEvent.class)
@Entity
@Table(name = "eb_tt_event")
public class EbTtEvent extends EbTimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebTtEventNum;

    @Column(insertable = true, updatable = false)
    @ColumnDefault("now()")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateCreation;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEvent;

    private Integer typeEvent; // type d'evenement

    private Integer natureDate; // TtEnumeration.natureDateEvent

    @ManyToOne
    @JoinColumn(name = "x_eb_tt_categorie_deviation")
    private EbTtCategorieDeviation categorie;

    @Column(columnDefinition = "TEXT")
    String commentaire;

    private Integer quantity;
    @Column
    private Boolean validateByChamp;

    @ManyToOne
    @JoinColumn(insertable = true, updatable = false)
    private EbTTPslApp xEbTtPslApp;

    @ManyToOne
    @JoinColumn(insertable = true, updatable = false)
    private EbTtTracing xEbTtTracing;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user")
    private EbUser xEbUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement")
    private EbEtablissement xEbEtablissement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie")
    private EbCompagnie xEbCompagnie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_tmp_event")
    private TmpEvent xEbTmptEvent;

    private Integer ebQmIncidentNum;

    private Integer ebQmDeviationNum;

    public EbTtEvent(EbTtEvent ebTtEvent) {
        this.ebTtEventNum = ebTtEvent.getEbTtEventNum();
        this.dateCreation = ebTtEvent.getDateCreation();
        this.dateEvent = ebTtEvent.getDateEvent();
        this.typeEvent = ebTtEvent.getTypeEvent();
        this.natureDate = ebTtEvent.getNatureDate();
        this.categorie = ebTtEvent.getCategorie();
        this.commentaire = ebTtEvent.getCommentaire();
        this.quantity = ebTtEvent.getQuantity();
        this.validateByChamp = ebTtEvent.getValidateByChamp();

        if (ebTtEvent.getxEbTtPslApp() != null) {
            this.xEbTtPslApp = new EbTTPslApp();
            this.xEbTtPslApp.setEbTtPslAppNum(ebTtEvent.getxEbTtPslApp().getEbTtPslAppNum());
        }

        if (ebTtEvent.getxEbTtTracing() != null) {
            this.xEbTtTracing = new EbTtTracing(ebTtEvent.getxEbTtTracing().getEbTtTracingNum());
        }

        this.xEbTmptEvent = ebTtEvent.getxEbTmptEvent();
    }

    public EbTtEvent() {
    }

    public Integer getEbTtEventNum() {
        return ebTtEventNum;
    }

    public void setEbTtEventNum(Integer ebTtEventNum) {
        this.ebTtEventNum = ebTtEventNum;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public EbTtCategorieDeviation getCategorie() {
        return categorie;
    }

    public void setCategorie(EbTtCategorieDeviation categorie) {
        this.categorie = categorie;
    }

    public Date getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(Date dateEvent) {
        this.dateEvent = dateEvent;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Integer getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(Integer typeEvent) {
        this.typeEvent = typeEvent;
    }

    public EbTTPslApp getxEbTtPslApp() {
        return xEbTtPslApp;
    }

    public void setxEbTtPslApp(EbTTPslApp xEbTtPslApp) {
        this.xEbTtPslApp = xEbTtPslApp;
    }

    public EbTtTracing getxEbTtTracing() {
        return xEbTtTracing;
    }

    public void setxEbTtTracing(EbTtTracing xEbTtTracing) {
        this.xEbTtTracing = xEbTtTracing;
    }

    public EbUser getxEbUser() {
        return xEbUser;
    }

    public EbEtablissement getxEbEtablissement() {
        return xEbEtablissement;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbUser(EbUser xEbUser) {
        this.xEbUser = xEbUser;
    }

    public void setxEbEtablissement(EbEtablissement xEbEtablissement) {
        this.xEbEtablissement = xEbEtablissement;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getValidateByChamp() {
        return validateByChamp;
    }

    public void setValidateByChamp(Boolean validateByChamp) {
        this.validateByChamp = validateByChamp;
    }

    public TmpEvent getxEbTmptEvent() {
        return xEbTmptEvent;
    }

    public void setxEbTmptEvent(TmpEvent xEbTmptEvent) {
        this.xEbTmptEvent = xEbTmptEvent;
    }

    public Integer getNatureDate() {
        return natureDate;
    }

    public void setNatureDate(Integer natureDate) {
        this.natureDate = natureDate;
    }

    public Integer getEbQmIncidentNum() {
        return ebQmIncidentNum;
    }

    public void setEbQmIncidentNum(Integer ebQmIncidentNum) {
        this.ebQmIncidentNum = ebQmIncidentNum;
    }

    public Integer getEbQmDeviationNum() {
        return ebQmDeviationNum;
    }

    public void setEbQmDeviationNum(Integer ebQmDeviationNum) {
        this.ebQmDeviationNum = ebQmDeviationNum;
    }
}
