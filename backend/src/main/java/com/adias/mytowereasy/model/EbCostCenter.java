/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.querydsl.core.annotations.QueryProjection;


@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "eb_cost_center", schema = "work", indexes = {
    @Index(name = "idx_eb_cost_center_x_eb_compagnie", columnList = "x_eb_compagnie")
})
public class EbCostCenter implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = -4199656464864847878L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebCostCenterNum;

    private String libelle;

    private Boolean deleted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", insertable = true)
    private EbCompagnie compagnie;

    @QueryProjection
    public EbCostCenter(Integer ebCostCenterNum, String libelle) {
        this.ebCostCenterNum = ebCostCenterNum;
        this.libelle = libelle;
    }

    public EbCostCenter() {
    }

    public EbCostCenter(Integer ebCostCenterNum) {
        this.ebCostCenterNum = ebCostCenterNum;
    }

    public Integer getEbCostCenterNum() {
        return ebCostCenterNum;
    }

    public void setEbCostCenterNum(Integer ebCostCenterNum) {
        this.ebCostCenterNum = ebCostCenterNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public EbCompagnie getCompagnie() {
        if (compagnie != null) compagnie = new EbCompagnie(compagnie.getEbCompagnieNum());
        return compagnie;
    }

    public void setCompagnie(EbCompagnie compagnie) {
        this.compagnie = compagnie;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ebCostCenterNum == null) ? 0 : ebCostCenterNum.hashCode());
        result = prime * result + ((compagnie == null) ? 0 : compagnie.hashCode());
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbCostCenter other = (EbCostCenter) obj;

        if (ebCostCenterNum == null) {
            if (other.ebCostCenterNum != null) return false;
        }
        else if (!ebCostCenterNum.equals(other.ebCostCenterNum)) return false;

        if (compagnie == null) {
            if (other.compagnie != null) return false;
        }
        else if (!compagnie.equals(other.compagnie)) return false;

        if (libelle == null) {
            if (other.libelle != null) return false;
        }
        else if (!libelle.equals(other.libelle)) return false;

        return true;
    }
}
