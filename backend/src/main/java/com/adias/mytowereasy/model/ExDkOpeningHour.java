package com.adias.mytowereasy.model;

import java.io.Serializable;

import javax.persistence.*;


@Entity
@Table(name = "ex_dock_opening_hours")
public class ExDkOpeningHour {
    @EmbeddedId
    private PKExDkOpeningHour pKExDkOpeningHourId;

    public ExDkOpeningHour() {
        super();
    }

    public PKExDkOpeningHour getPKExDkOpeningHourId() {
        return pKExDkOpeningHourId;
    }

    public void setPKExDkOpeningHourId(PKExDkOpeningHour pKExDkOpeningHourId) {
        this.pKExDkOpeningHourId = pKExDkOpeningHourId;
    }
}


@Embeddable
class PKExDkOpeningHour implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "x_opening_hours")
    private Integer x_opening_hours;

    @Column(name = "x_eb_dock")
    private Integer x_eb_dock;

    public PKExDkOpeningHour() {
        super();
    }

    public Integer getX_opening_hours() {
        return x_opening_hours;
    }

    public void setX_opening_hours(Integer x_opening_hours) {
        this.x_opening_hours = x_opening_hours;
    }

    public Integer getX_eb_dock() {
        return x_eb_dock;
    }

    public void setX_eb_dock(Integer x_eb_dock) {
        this.x_eb_dock = x_eb_dock;
    }
}
