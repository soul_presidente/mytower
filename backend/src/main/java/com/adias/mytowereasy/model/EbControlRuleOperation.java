package com.adias.mytowereasy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties({
    "leftOperandObj", "rightOperandObj",
})
@Entity
@Table(name = "eb_control_rule_operation", schema = "work")
public class EbControlRuleOperation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebControlRuleOperationNum;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_control_rule", nullable = false, updatable = false)
    @JsonBackReference
    private EbControlRule xEbControlRule;

    // AND, OR
    private Integer ruleOperator;

    // operator of the operation
    private Integer operator;

    // operands separated by ';', these operands should be strictly ordered. if
    // a field is absent, the operand should be absent too (a;b;;d)
    @Column(columnDefinition = "text")
    private String leftOperands; // actual;codealpha

    // operands separated by ';'
    @Column(columnDefinition = "text")
    private String rightOperands;

    private Boolean isOperation;

    private Integer operationType;

    private Integer ebCategorieNum;

    private Integer ebLeftCustomFieldNum;

    private Integer ebRightCustomFieldNum;

    private Integer leftUnitFieldCode;

    private Integer selectedTypeDocNum;

    public Integer getEbControlRuleOperationNum() {
        return ebControlRuleOperationNum;
    }

    public void setEbControlRuleOperationNum(Integer ebControlRuleOperationNum) {
        this.ebControlRuleOperationNum = ebControlRuleOperationNum;
    }

    public EbControlRule getxEbControlRule() {
        return xEbControlRule;
    }

    public void setxEbControlRule(EbControlRule xEbControlRule) {
        this.xEbControlRule = xEbControlRule;
    }

    public Integer getRuleOperator() {
        return ruleOperator;
    }

    public void setRuleOperator(Integer ruleOperator) {
        this.ruleOperator = ruleOperator;
    }

    public Integer getOperator() {
        return operator;
    }

    public void setOperator(Integer operator) {
        this.operator = operator;
    }

    public String getLeftOperands() {
        return leftOperands;
    }

    public void setLeftOperands(String leftOperands) {
        this.leftOperands = leftOperands;
    }

    public String getRightOperands() {
        return rightOperands;
    }

    public void setRightOperands(String rightOperands) {
        this.rightOperands = rightOperands;
    }

    public Boolean getIsOperation() {
        return isOperation;
    }

    public void setIsOperation(Boolean isOperation) {
        this.isOperation = isOperation;
    }

    public Integer getOperationType() {
        return operationType;
    }

    public void setOperationType(Integer operationType) {
        this.operationType = operationType;
    }

    public Integer getEbCategorieNum() {
        return ebCategorieNum;
    }

    public void setEbCategorieNum(Integer ebCategorieNum) {
        this.ebCategorieNum = ebCategorieNum;
    }

    public Integer getEbLeftCustomFieldNum() {
        return ebLeftCustomFieldNum;
    }

    public void setEbLeftCustomFieldNum(Integer ebLeftCustomFieldNum) {
        this.ebLeftCustomFieldNum = ebLeftCustomFieldNum;
    }

    public Integer getEbRightCustomFieldNum() {
        return ebRightCustomFieldNum;
    }

    public void setEbRightCustomFieldNum(Integer ebRightCustomFieldNum) {
        this.ebRightCustomFieldNum = ebRightCustomFieldNum;
    }

    public Integer getLeftUnitFieldCode() {
        return leftUnitFieldCode;
    }

    public void setLeftUnitFieldCode(Integer unitFieldCode) {
        this.leftUnitFieldCode = unitFieldCode;
    }

    public Integer getSelectedTypeDocNum() {
        return selectedTypeDocNum;
    }

    public void setSelectedTypeDocNum(Integer selectedTypeDocNum) {
        this.selectedTypeDocNum = selectedTypeDocNum;
    }
}
