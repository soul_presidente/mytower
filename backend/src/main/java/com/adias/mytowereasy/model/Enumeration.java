/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.IOException;
import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import com.adias.mytowereasy.SpringUtility;
import com.adias.mytowereasy.dto.CodeLibelleDTO;
import com.adias.mytowereasy.service.CommonFlagSevice;
import com.adias.mytowereasy.service.FreightAuditServiceImpl;
import com.adias.mytowereasy.service.PricingServiceImpl;
import com.adias.mytowereasy.service.QualityManagementServiceImpl;
import com.adias.mytowereasy.service.TrackServiceImpl;
import com.adias.mytowereasy.util.GenericEnum;
import com.adias.mytowereasy.util.enums.GenericEnumException;
import com.adias.mytowereasy.util.enums.GenericEnumUtils;


public class Enumeration {
    public static final String RTN_LIB = "RTN";
    public static final String CRI_LIB = "CRI";

    private static final Logger LOGGER = LoggerFactory.getLogger(Enumeration.class);

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum CarrierStatus implements GenericEnum {
        REQUEST_RECEIVED("REQUEST_RECEIVED", 1),
        QUOTE_SENT("QUOTE_SENT", 2),
        RECOMMENDATION("RECOMMENDATION", 3),
        FINAL_CHOICE("FINAL_CHOICE", 4),
        NOT_AWARDED("NOT_AWARDED", 5),
        TRANSPORT_PLAN("TRANSPORT_PLAN", 6),
        FIN_PLAN("FIN_PLAN", 7);

        private Integer code;
        private String label;

        CarrierStatus(String label, Integer code) {
            this.code = code;
            this.label = label;
        }

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return label;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum Action {
        VISUALISER("Visualize", 1), EXECUTER("Executive", 2);

        private Integer code;
        private String libelle;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        // Constructeur
        private Action(String libelle, Integer code) {
            this.code = code;
            this.libelle = libelle;
        }

        public static List<Action> getListAction() {
            List<Action> listAction = new ArrayList<Action>();
            listAction.add(Action.VISUALISER);
            listAction.add(Action.EXECUTER);
            return listAction;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum Role {
        ROLE_CHARGEUR(1, "Chargeur"),
        ROLE_PRESTATAIRE(2, "Prestataire"),
        ROLE_CONTROL_TOWER(3, "Control Tower"),
        ROLE_UNKNOWN(4, "Unknown");

        private Integer code;
        private String libelle;

        public static String getLibelleByCode(Integer code) {

            for (Role item: Role.values()) {

                if (item.code != null && item.code.equals(code)) {
                    return item.libelle;
                }

            }

            return "";
        }

        public static Role getByCode(Integer code) {

            if (code != null) {
                Role roleUser = null;

                for (Role item: Role.values()) {
                    if (item.code.equals(code)) roleUser = item;
                }

                return roleUser;
            }

            return null;
        }

        private Role(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static List<Role> getListRole() {
            List<Role> listRole = new ArrayList<Role>();

            listRole.add(Role.ROLE_CHARGEUR);
            listRole.add(Role.ROLE_PRESTATAIRE);
            listRole.add(Role.ROLE_CONTROL_TOWER);
            return listRole;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum TypeMarchandise {
        CARDBOARD_BOX(1, "Cardboard box"),
        SUSPENDU(25, "Suspendu"),
        COLIS(26, "Colis"),
        PALLET(2, "Pallet"),
        CRATE(3, "Crate"),
        CASE(4, "Case"),
        BUNDLE(5, "Bundle"),
        BARREL(6, "Barrel"),
        CONTAINER_DRY_20(7, "Container 20' x 8'6' Dry"),
        CONTAINER_DRY_HIGH_20(8, "Container 20' x 9'6' Dry High Cube"),
        CONTAINER_REEFER_20(9, "Container 20' x 8'6' Reefer"),
        CONTAINER_REEFER_HIGH_20(10, "Container 20' x 9'6' Reefer High Cube"),
        OPEN_TOP_20(11, "Container 20' Open Top"),
        CONTAINER_HARD_TOP_20(12, "Container 20' Hard Top"),
        CONTAINER_FLAT_20(13, "Container 20' Flat Rack"),
        CONTAINER_DRY_40(14, "Container 40'x8'6' Dry"),
        CONTAINER_DRY_HIGH_40(15, "Container 40'x9'6' Dry High Cube"),
        CONTAINER_REEFER_40(16, "Container 40'x8'6' Reefer"),
        CONTAINER_REEFER_HIGH_40(17, "Container 40'x9'6' Reefer High Cube"),
        OPEN_TOP_40(18, "Container 40' Open Top"),
        CONTAINER_HARD_TOP_40(19, "Container 40' Hard Top"),
        CONTAINER_FLAT_40(20, "Container 40' Flat Rack"),
        CONTAINER_DRY_HIGH_45(21, "Container 45'x9'6' Dry High Cube"),
        CONTAINER_REEFER_HIGH_45(22, "Container 45'x9'6' Reefer High Cube"),
        COMPLETE_TRUCK(23, "Complete truck"),
        CAMION_COMPLET(24, "Camion complet");

        private Integer key;
        private String label;

        private TypeMarchandise(Integer key, String label) {
            this.key = key;
            this.label = label;
        }

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public static TypeMarchandise getTypeMarchandiseByKey(Integer key) {
            TypeMarchandise[] types = TypeMarchandise.values();

            for (TypeMarchandise t: types) {

                if (t.getKey() == key) {
                    return t;
                }

            }

            return null;
        }

        public static Integer getTypeMarchandiseCodeByLabel(String label) {
            TypeMarchandise[] types = TypeMarchandise.values();

            for (TypeMarchandise t: types) {

                if (t.getLabel() == label) {
                    return t.getKey();
                }

            }

            return null;
        }

        public static List<TypeMarchandise> getListTypeMarchandise() {
            List<TypeMarchandise> result = new ArrayList<TypeMarchandise>();
            TypeMarchandise[] types = TypeMarchandise.values();

            for (TypeMarchandise t: types) {
                result.add(t);
            }

            return result;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum TypeContainer {
        ARTICLE(4, "Article"), PARCEL(3, "Parcel"), PALETTE(2, "Palette"), CONTAINER(1, "Container"), ALL(0, "All");

        private Integer key;
        private String value;

        private TypeContainer(Integer key, String value) {
            this.key = key;
            this.value = value;
        }

        public Integer getKey() {
            return key;
        }

        public void setKey(Integer key) {
            this.key = key;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public static TypeContainer getTypeContainerByKey(Integer key) {
            TypeContainer[] types = TypeContainer.values();

            for (TypeContainer t: types) {

                if (t.getKey() == key) {
                    return t;
                }

            }

            return null;
        }

        public static List<TypeContainer> getListTypeContainer() {
            List<TypeContainer> result = new ArrayList<TypeContainer>();
            TypeContainer[] types = TypeContainer.values();

            for (TypeContainer t: types) {
                result.add(t);
            }

            return result;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum ServiceType {
        SERVICE_TRANSPORTEUR(0, "Transporteur"),
        SERVICE_BROKER(1, "Broker"),
        SERVICE_BROKER_TRANSPORTEUR(2, "Transporteur / Broker"),
        SERVICE_CONTROL_TOWER(3, "Contrôl Tower"),
        UNKNOWN(4, "Unknown");

        private Integer code;

        private String libelle;

        private ServiceType(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum EmailConfig {
        ACTIVATE_ACCOUNT(1, "email-activate-account"),
        NOUVEAU_COMPTE_SOCIETE(2, "email-nouveau-compte-societe"),
        INVITE_FRIEND(3, "3-email-invitation-friend"),
        INVITE_COLLEGUE(4, "4-email-invitation-collegue"),
        BACKUP(5, "email-backup"),
        RESET_PASSWORD(6, "email-reset-password"),
        UPDATED_PASSWORD(7, "email-update-password"),
        EMAIL_INVITATION(8, "email-invitation"),
        EMAIL_INVITATION_ACCEPTER(9, "email-invitation-accepter"),
        COMPTE_ACTIVER_PAR_ADMIN(10, "email-compte-activer-par-admin"),
        EMAIL_ADD_USER_BY_ADMIN(11, "email-add-user-by-admin"),
        REFUS_USER_PAR_ADMIN(100, "refus-user-par-admin"),

        QUOTATION_REQUEST(12, "email-quotation-request"),
        QUOTATION_REQUEST_CHARGEUR(13, "email-quotation-request-chargeur"),
        REPONSE_FROM_CHARGEUR(14, "email-reponse-from-chargeur"),
        REPONSE_TRANSPORTEUR(15, "email-reponse-transporteur"),
        QUOTATION_RECOMMENDATION(16, "email-quotation-recommendation"),
        QUOTATION_FROM_FORWARDER(17, "email-quotation-from-forwarder"),
        QUOTATION_FORWARDER_SELECTED(20, "email-quotation-forwarder-selected"),
        QUOTATION_FORWARDER_NOT_SELECTED(21, "email-quotation-forwarder-not-selected"),

        NOUVEAU_COMMENTAIRE(22, "email-ajout-nouveau-commentaire"),
        QUOTATION_RENSEIGNEMENT_INFORMATIONS_TRANSPORT(24, "email-renseignement-informations-transport"),

        CHARGE_DOC_TM_TT(25, "email-charge-doc-tm-tt"),
        SUPPRESSION_DOCUMENT(26, "email-suppression-document"),

        DECLARATION_DUNE_DEVIATION(30, "email-declaration-dune-deviation"),
        MISE_A_JR_UN_PSL(33, "email-mise-a-jr-un-psl"),
        QUOTATION_ALRTE_CT(34, "email-quotation-alerte-ct"),

        TRANSPORT_FILE_CREATION(37, "email-transport-file-creation"),

        PRICING_DATA_CHANGED(38, "email-pricing-data-changed"),

        QUOTATION_CANCELLED(39, "email-quotation-cancelled"),
        INVITE_USER_NOT_REGISTRED(40, "email-invitation-user-not-registred"),
        INFORMATION_APPLY(42, "Information-apply"),
        INFORMATION_SHARING(43, "Information-sharing"),
        QUOTATION_CKNOWLEDGE_TDC(60, "email-quotation-cknowledge-tdc"),
        CPTM_REMINDER(61, "email-cptm-reminder"),
        DEMANDE_CONFIRMATION_DE_PRISE_EN_CHARGE_DU_TRANSPORT(
            62,
            "email-demande-confirmation-de-prise-en-charge-du-transport"),
        PRICING_CONFIRMATION_PRISE_EN_CHARGE(63, "email-pricing-confirmation-prise-en-charge"),
        CREATION_NOUVELLE_SOCIETE_SUR_MYTOWER(1001, "email-creation-nouvelle-societe-sur-myTower"),
        INFO_MANDATORY_PREFACTURE(64, "email-info-mandatory-prefacture"),
        INFO_MANDATORY_PROFORMA(65, "email-info-mandatory-proforma");

        private Integer code;

        private String template;

        public Integer getCode() {
            return code;
        }

        public String getTemplate() {
            return template;
        }

        private EmailConfig(Integer code, String template) {
            this.code = code;
            this.template = template;
        }

        public String getTemplateName() {
            return code + "-" + template;
        }
    }

    public enum TemplateConfig {
        PREFACTURE(1, "Préfacture"), PROFORMA(2, "Proforma");

        private Integer code;

        private String libelle;

        public Integer getCode() {
            return code;
        }

        private TemplateConfig(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }
    }

    public enum EmailStatus {
        EMAIL_NON_ENVOYE(0, "Email non envoyé"), EMAIL_ENVOYE(1, "Email envoyé");

        private Integer code;
        private String libelle;

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        private EmailStatus(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }
    }

    public static Map<Integer, String> uploadPrefixes = new HashMap<Integer, String>() {
        {
            put(1, "logo_file_");
            put(2, "demande_file_");
            put(3, "import_file_");
            put(4, "freight_audit_file_");
            put(5, "chanelpb_file_");
            put(6, "incident_file_");
            put(7, "template_file_");
            put(8, "douane_declaration_");
        }
    };

    public enum UploadDirectory {
        LOGO(1, "user-logo"),
        PRICING_BOOKING(2, "pricing-booking"),
        BULK_IMPORT(3, "bulk-import"),
        FREIGHT_AUDIT(4, "freight-audit"),
        CHANEL_PB(5, "chanel-pb"),
        QUALITY_MANAGEMENT(6, "quality_management"),
        TEMPLATE(7, "template"),
        CUSTOM(8, "custom"),
        EXPORT(9, "exports");

        private Integer code;
        private String directory;

        private UploadDirectory(Integer code, String directory) {
            this.code = code;
            this.directory = directory;
        }

        public Integer getCode() {
            return code;
        }

        public String getDirectory() {
            return directory;
        }

        public static String[] valuesAsString() {
            return Arrays.stream(values()).map(dir -> dir.directory).toArray(String[]::new);
        }

        public static UploadDirectory getUploadDirectoryByCode(Integer code) {

            for (UploadDirectory item: UploadDirectory.values()) {

                if (item.code == code) {
                    return item;
                }

            }

            return null;
        }
    }

    public enum UserStatus {
        ACTIF(1, "Actif"),
        DISACTIVE(2, "Désactivé"),
        NON_ACTIF(3, "Non Activé"),
        ACCEPTER_PAR_ADMIN(4, "Accepter par admin");

        private Integer code;
        private String status;

        public static String getLibelleByCode(Integer code) {

            for (UserStatus item: UserStatus.values()) {

                if (item.code != null && item.code.equals(code)) {
                    return item.status;
                }

            }

            return "";
        }

        public static String getListStatutUser() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (UserStatus item: UserStatus.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.status);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }

        private UserStatus(Integer code, String status) {
            this.code = code;
            this.status = status;
        }

        public Integer getCode() {
            return code;
        }

        public String getStatus() {
            return status;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum EtatHoraire {
        ALL_DAYS(0, "All Days"), CUSTOMISE(1, "Customise");

        private String libelle;
        private Integer code;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        // Constructeur
        private EtatHoraire(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        @JsonCreator
        public static EtatHoraire fromNode(JsonNode node) {
            if (!node.has("name")) return null;

            String name = node.get("name").asText();

            return EtatHoraire.valueOf(name);
        }

        @JsonProperty
        public String getName() {
            return name();
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum JourEnum {
        ALL(0, "All"),
        MONDAY(DayOfWeek.MONDAY.getValue(), "Mon."),
        TUESDAY(DayOfWeek.TUESDAY.getValue(), "Tue."),
        WEDNESDAY(DayOfWeek.WEDNESDAY.getValue(), "Wed."),
        THURSDAY(DayOfWeek.THURSDAY.getValue(), "Thu."),
        FRIDAY(DayOfWeek.FRIDAY.getValue(), "Fri."),
        SATURDAY(DayOfWeek.SATURDAY.getValue(), "Sat."),
        SUNDAY(DayOfWeek.SUNDAY.getValue(), "Sun.");

        private String libelle;
        private Integer code;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        public static JourEnum getJourEnumByCode(Integer code) {
            JourEnum jourEnum = null;

            for (JourEnum item: JourEnum.values()) {
                if (item.code.equals(code)) jourEnum = item;
            }

            return jourEnum;
        }

        // Constructeur
        private JourEnum(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        @JsonCreator
        public static JourEnum fromNode(JsonNode node) {
            if (!node.has("name")) return null;

            String name = node.get("name").asText();

            return JourEnum.valueOf(name);
        }

        @JsonProperty
        public String getName() {
            return name();
        }
    }

    public enum EtatRelation {
        EN_COURS(0, "En Cours"),
        ACTIF(1, "Actif"),
        IGNORER(2, "Ignorer"),
        ANNULER(3, "Annuler"),
        BLACKLIST(4, "Blacklist");

        private Integer code;
        private String libelle;

        private EtatRelation(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum EmailInvitation {
        INVITATION_ACCEPTER(0, "Invitation Accepter", "9-email-invitation-accepter"),
        INVITATION(1, "Invitation", "8-email-invitation");

        private Integer code;
        private String subject;
        private String template;

        public Integer getCode() {
            return code;
        }

        public String getSubject() {
            return subject;
        }

        public String getTemplate() {
            return template;
        }

        private EmailInvitation(Integer code, String subject, String template) {
            this.code = code;
            this.subject = subject;
            this.template = template;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum TypeDemande {
        STANDARD(1, "Standard", 48 * 60), URGENT(2, "Urgent", 24 * 60);

        private Integer code;
        private String libelle;
        private Integer timeRemaining;

        private TypeDemande(Integer code, String libelle, Integer timeRemaining) {
            this.code = code;
            this.libelle = libelle;
            this.timeRemaining = timeRemaining;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public Integer getTimeRemaining() {
            return timeRemaining;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                String typeDemande = null;

                for (TypeDemande item: TypeDemande.values()) {
                    if (item.code.equals(code)) typeDemande = item.libelle;
                }

                return typeDemande;
            }

            return null;
        }

        public static Integer getCodeByLibelle(String libelle) {

            if (libelle != null) {
                Integer typeDemande = null;
                String value = libelle.toLowerCase();

                for (TypeDemande item: TypeDemande.values()) {
                    if (item.libelle.toLowerCase().contains(value)) typeDemande = item.code;
                }

                return typeDemande;
            }

            return null;
        }

        public static List<Integer> getCodesByLibelle(String libelle) {

            if (libelle != null) {
                List<Integer> listTypeDemande = new ArrayList<Integer>();
                String value = libelle.toLowerCase();

                for (TypeDemande item: TypeDemande.values()) {
                    if (item.libelle.toLowerCase().contains(value)) listTypeDemande.add(item.code);
                }

                return listTypeDemande;
            }

            return null;
        }

        // récuppération de la liste format JSON
        public static String getListTypeDemande() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            JsonNode standard = mapper.readTree(mapper.writeValueAsString(TypeDemande.STANDARD));
            JsonNode urgent = mapper.readTree(mapper.writeValueAsString(TypeDemande.URGENT));

            arrayNode.add(standard);
            arrayNode.add(urgent);

            return arrayNode.toString();
        }

        @JsonCreator
        public static TypeDemande fromNode(JsonNode node) {
            if (!node.has("libelle")) return null;

            String name = node.get("libelle").asText().toUpperCase();

            return TypeDemande.valueOf(name);
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum ModeTransport {
        AIR(1, "Air", 1, "fa fa-plane"),
        SEA(2, "Sea", 2, "fa fa-ship"),
        ROAD(3, "Road", 3, "fa fa-road"),
        INTEGRATOR(4, "Integrator", 5, "fa fa-arrows"),
        RAIL(5, "Rail", 4, "fa fa-train");

        private Integer code;
        private String libelle;
        private Integer order;
        private String icon;

        private ModeTransport(Integer code, String libelle, Integer order, String icon) {
            this.code = code;
            this.libelle = libelle;
            this.order = order;
            this.icon = icon;
        }

        public static List<ModeTransport> getValues() {
            ModeTransport[] values = ModeTransport.values();
            return Arrays.asList(values);
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                String modeTransport = null;

                for (ModeTransport item: ModeTransport.values()) {
                    if (item.code.equals(code)) modeTransport = item.libelle;
                }

                return modeTransport;
            }

            return null;
        }

        public static Integer getCodeByLibelle(String libelle) {

            if (libelle != null) {
                Integer modeTransport = null;
                String value = libelle.toLowerCase();

                for (ModeTransport item: ModeTransport.values()) {
                    if (item.libelle.toLowerCase().contains(value)) modeTransport = item.code;
                }

                return modeTransport;
            }

            return null;
        }

        public static List<Integer> getListCodeByLibelle(String libelle) {

            if (libelle != null && !libelle.isEmpty()) {
                List<Integer> listCode = new ArrayList<Integer>();

                for (ModeTransport item: ModeTransport.values()) {
                    if (item.libelle.toLowerCase().contains(libelle.toLowerCase())) listCode.add(item.code);
                }

                return listCode;
            }

            return null;
        }

        public Integer getCode() {
            return code;
        }

        public Integer getOrder() {
            return order;
        }

        public String getLibelle() {
            return libelle;
        }

        public String getIcon() {
            return icon;
        }

        public static String getListModeTransport() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (ModeTransport item: ModeTransport.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);
                jsonObj.put("order", item.order);
                jsonObj.put("icon", item.icon);
                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }

        public static String getListModeTransportCodeLibelle() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (ModeTransport item: ModeTransport.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);
                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }

        public static List<String> getListModeTransportLibelle() {
            List<String> res = new ArrayList<String>();

            for (ModeTransport item: ModeTransport.values()) {
                res.add(item.getLibelle());
            }

            return res;
        }

        public static List<Integer> getListModeTransportCode() {
            List<Integer> res = new ArrayList<Integer>();

            for (ModeTransport item: ModeTransport.values()) {
                res.add(item.getCode());
            }

            return res;
        }
    }

    public enum Incoterms {
        DDP(1, "DDP"),
        DAP(2, "DAP"),
        DAT(3, "DAT"),
        CIP(4, "CIP"),
        CPT(5, "CPT"),
        CIF(6, "CIF"),
        CFR(7, "CFR"),
        FOB(8, "FOB"),
        FAS(9, "FAS"),
        FCA(10, "FCA"),
        EXW(11, "EXW");

        private Integer code;
        private String libelle;

        private Incoterms(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static Incoterms getIncoterm(String libelle) {
            Incoterms val = null;
            String value = libelle.toLowerCase();

            for (Incoterms item: Incoterms.values()) {

                if (item.libelle.toLowerCase().contains(value)) {
                    val = item;
                    break;
                }

            }

            return val;
        }

        public static Integer getCodeByLibelle(String libelle) {

            if (libelle != null) {
                String value = libelle.toLowerCase();
                Integer incoterm = null;

                for (Incoterms item: Incoterms.values()) {
                    if (item.libelle.toLowerCase().contains(value)) incoterm = item.code;
                }

                return incoterm;
            }

            return null;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum TypeProcess {
        SPOT("Spot", 1), ADHOC("Adhoc", 2), ROUTINE("Routine", 3);

        private Integer code;
        private String libelle;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        // Constructeur
        private TypeProcess(String libelle, Integer code) {
            this.code = code;
            this.libelle = libelle;
        }

        // récuppération de la liste format JSON
        public static String getListTypeProcess() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            JsonNode spot = mapper.readTree(mapper.writeValueAsString(TypeProcess.SPOT));
            JsonNode adhoc = mapper.readTree(mapper.writeValueAsString(TypeProcess.ADHOC));
            JsonNode routine = mapper.readTree(mapper.writeValueAsString(TypeProcess.ROUTINE));

            arrayNode.add(spot);
            arrayNode.add(adhoc);
            arrayNode.add(routine);

            return arrayNode.toString();
        }

        @JsonCreator
        public static TypeProcess fromNode(JsonNode node) {
            if (!node.has("libelle")) return null;

            String name = node.get("libelle").asText().toUpperCase();

            return TypeProcess.valueOf(name);
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum StatutOkMissing {
        MISSING(0, "Missing"), OK(1, "Ok");

        private Integer code;
        private String libelle;

        private StatutOkMissing(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getListStatusOkMissing() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            JsonNode node1 = mapper.readTree(mapper.writeValueAsString(StatutOkMissing.MISSING));
            JsonNode node2 = mapper.readTree(mapper.writeValueAsString(StatutOkMissing.OK));

            arrayNode.add(node1);
            arrayNode.add(node2);

            return arrayNode.toString();
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum InfosDocsStatus {
        CUSTOM_DOCUMENT_MISSING(0, "Custom document Missing"),
        CUSTOM_DOCUMENT_OK(1, "Custom document Ok"),

        /*
         * désactivé pour répondre aux critères de ce jira
         * https://jira.adias.fr/browse/MTGR-392
         * TRANSPORT_INFO_MISSING(2, "Transport Info Missing"),
         * TRANSPORT_INFO_OK(3, "Transport Info Ok"),
         */

        TRANSPORT_DOCUMENT_MISSING(4, "Transport document Missing"),
        TRANSPORT_DOCUMENT_OK(5, "Transport document Ok");

        private Integer code;
        private String libelle;

        private InfosDocsStatus(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getListInfosDocsStatus() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            JsonNode node1 = mapper.readTree(mapper.writeValueAsString(InfosDocsStatus.CUSTOM_DOCUMENT_MISSING));
            JsonNode node2 = mapper.readTree(mapper.writeValueAsString(InfosDocsStatus.CUSTOM_DOCUMENT_OK));
            JsonNode node3 = mapper.readTree(mapper.writeValueAsString(InfosDocsStatus.TRANSPORT_DOCUMENT_MISSING));
            JsonNode node4 = mapper.readTree(mapper.writeValueAsString(InfosDocsStatus.TRANSPORT_DOCUMENT_OK));
            /*
             * désactivé pour répondre aux critères de ce jira
             * https://jira.adias.fr/browse/MTGR-392
             * JsonNode node5 = mapper
             * .readTree(mapper.writeValueAsString(InfosDocsStatus.
             * TRANSPORT_INFO_MISSING));
             * JsonNode node6 = mapper
             * .readTree(mapper.writeValueAsString(InfosDocsStatus.
             * TRANSPORT_INFO_OK));
             * JsonNode node15 =
             * mapper.readTree(mapper.writeValueAsString(StatutDemande.WPU));
             * JsonNode node16 =
             * mapper.readTree(mapper.writeValueAsString(StatutDemande.TO));
             * JsonNode node17 =
             * mapper.readTree(mapper.writeValueAsString(StatutDemande.DLVRY));
             * arrayNode.add(node15);
             * arrayNode.add(node16);
             * arrayNode.add(node17);
             */

            arrayNode.add(node1);
            arrayNode.add(node2);
            arrayNode.add(node3);
            arrayNode.add(node4);
            /*
             * arrayNode.add(node5);
             * arrayNode.add(node6);
             */

            return arrayNode.toString();
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum TransitStatus {
        WAITINGFORPICKUP(0, "Waiting for pickup"), ONGOING(1, "Ongoing"), COMPLETED(2, "Completed");

        private Integer code;
        private String libelle;

        private TransitStatus(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getListTransitStatus() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            JsonNode node1 = mapper.readTree(mapper.writeValueAsString(TransitStatus.WAITINGFORPICKUP));
            JsonNode node2 = mapper.readTree(mapper.writeValueAsString(TransitStatus.ONGOING));
            JsonNode node3 = mapper.readTree(mapper.writeValueAsString(TransitStatus.COMPLETED));

            arrayNode.add(node1);
            arrayNode.add(node2);
            arrayNode.add(node3);

            return arrayNode.toString();
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum StatutDemande {
        PND("PND", "PENDING", -1),
        INP("INP", "IN_PROCESS_WAITING_FOR_QUOTE", 1),
        WRE("WRE", "WAITING_FOR_RECOMMENDATION", 2),
        REC("REC", "WAITING_FOR_CONFIRMATION", 3),
        FIN("FIN", "CONFIRMED", 4),

        NAW("NAW", "NOT_AWARDED", 5),
        CAN("CAN", "CANCELLATION_OF_QUOTATION_REQUEST", 6),
        ARC("ARC", "ARCHIVED", 7),

        // PCA("PCA", "Prebooking cancelled ", 8),
        NARC("NARC", "ACTIVE", 9),

        // WCC("WCC", "Waiting for cancellation confirmation", 9),

        WDCT("WDCT", "WAITING_DOCUMENT_TRANSPORT_CUSTOMS", 10),
        WDC("WDC", "WAITING_DOCUMENT_CUSTOMS", 101),
        WDT("WDT", "WAITING_DOCUMENT_TRANSPORT", 102),
        WPU("WPU", "WAITING_PICKUP", 11),
        TO("TO", "TRANSPORT_ONGOING", 12),
        DLVRY("DLVRY", "COMPLETED", 13),

        WTR("WTR", "Waiting for transportation request", 14),

        RPL("RPL", "REPLACED", 201),
        CSL("CSL", "CANCELLED_FOR_CONSOLIDATION", 202);

        private Integer code;
        private String libelle;
        private String libelleJason;

        public String getLibelleJason() {
            return libelleJason;
        }

        public void setLibelleJason(String libelleJason) {
            this.libelleJason = libelleJason;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        // Constructeur
        private StatutDemande(String libelleJason, String libelle, Integer code) {
            this.libelleJason = libelleJason;
            this.code = code;
            this.libelle = libelle;
        }

        public static String toJsonList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            ObjectNode jsonObj = mapper.createObjectNode();
            jsonObj.put("code", StatutDemande.INP.code);
            jsonObj.put("libelle", StatutDemande.INP.libelle);
            arrayNode.add(jsonObj);

            ObjectNode jsonObj2 = mapper.createObjectNode();
            jsonObj2.put("code", StatutDemande.REC.code);
            jsonObj2.put("libelle", StatutDemande.REC.libelle);
            arrayNode.add(jsonObj2);
            return arrayNode.toString();
        }

        public static Integer getCodeByLibelle(String libelle) {

            if (libelle != null && !libelle.isEmpty()) {
                Integer status = null;
                String value = libelle.toLowerCase();

                for (StatutDemande item: StatutDemande.values()) {
                    if (item.libelle.toLowerCase().contains(value)) status = item.code;
                }

                return status;
            }

            return null;
        }

        public static String getByCode(Integer code) {

            if (code != null) {
                StatutDemande statusDemande = null;

                for (StatutDemande item: StatutDemande.values()) {
                    if (item.code.equals(code)) statusDemande = item;
                }

                return statusDemande != null ? statusDemande.getLibelle() : "";
            }

            return null;
        }

        public static String getLibelleJasonByCode(Integer code) {

            if (code != null) {
                StatutDemande statusDemande = null;

                for (StatutDemande item: StatutDemande.values()) {
                    if (item.code.equals(code)) statusDemande = item;
                }

                return statusDemande != null ? statusDemande.getLibelleJason() : "";
            }

            return null;
        }

        public static Integer getListCodeByLibelleJason(String libelle) {

            if (libelle != null && !libelle.isEmpty()) {
                String libelleStr = libelle.toLowerCase();
                Integer code = null;

                for (StatutDemande item: StatutDemande.values()) {
                    if (item.libelleJason.toLowerCase().contains(libelleStr)) code = item.code;
                }

                return code;
            }

            return null;
        }

        public static List<Integer> getListCodeByLibelle(String libelle) {

            if (libelle != null && !libelle.isEmpty()) {
                List<Integer> listCode = new ArrayList<Integer>();

                for (StatutDemande item: StatutDemande.values()) {
                    if (item.libelle.toLowerCase().contains(libelle)) listCode.add(item.code);
                }

                return listCode;
            }

            return null;
        }

        // récuppération de la liste format JSON
				public static String getListStatutDemandePricing(EbUser connectedUser) throws IOException
				{
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

						if (!connectedUser.isPrestataire())
						{
            JsonNode node1 = mapper.readTree(mapper.writeValueAsString(StatutDemande.PND));
							arrayNode.add(node1);
						}
            JsonNode node2 = mapper.readTree(mapper.writeValueAsString(StatutDemande.INP));
            JsonNode node4 = mapper.readTree(mapper.writeValueAsString(StatutDemande.REC));
            JsonNode node5 = mapper.readTree(mapper.writeValueAsString(StatutDemande.FIN));
            JsonNode node7 = mapper.readTree(mapper.writeValueAsString(StatutDemande.CAN));
            JsonNode node8 = mapper.readTree(mapper.writeValueAsString(StatutDemande.ARC));
            JsonNode node9 = mapper.readTree(mapper.writeValueAsString(StatutDemande.NARC));

            arrayNode.add(node2);
            arrayNode.add(node4);
            arrayNode.add(node5);
            arrayNode.add(node7);
            arrayNode.add(node8);
            arrayNode.add(node9);

            return arrayNode.toString();
        }

        public static String getListStatutDemandeBooking() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            JsonNode node15 = mapper.readTree(mapper.writeValueAsString(StatutDemande.WPU));
            JsonNode node16 = mapper.readTree(mapper.writeValueAsString(StatutDemande.TO));
            JsonNode node17 = mapper.readTree(mapper.writeValueAsString(StatutDemande.DLVRY));
            JsonNode node18 = mapper.readTree(mapper.writeValueAsString(StatutDemande.CAN));
            JsonNode node19 = mapper.readTree(mapper.writeValueAsString(StatutDemande.NARC));
            JsonNode node110 = mapper.readTree(mapper.writeValueAsString(StatutDemande.ARC));

            arrayNode.add(node15);
            arrayNode.add(node16);
            arrayNode.add(node17);
            arrayNode.add(node18);
            arrayNode.add(node19);
            arrayNode.add(node110);

            return arrayNode.toString();
        }

        public static List<CodeLibelleDTO> getListStatutDemandeBookingDTO() throws IOException {
            List<CodeLibelleDTO> list = new ArrayList<CodeLibelleDTO>();

            for (StatutDemande item: StatutDemande.values()) {
                list.add(new CodeLibelleDTO(item.code, item.libelle));
            }

            return list;
        }

        @JsonCreator
        public static StatutDemande fromNode(JsonNode node) {
            if (!node.has("libelleJason")) return null;

            String name = node.get("libelleJason").asText().toUpperCase();

            return StatutDemande.valueOf(name);
        }

        public static String getLibelle(int code) {
            String libelle = null;

            for (StatutDemande st: StatutDemande.values()) {

                if (st.getCode().equals(code)) {
                    libelle = st.getLibelle();
                    break;
                }

            }

            return libelle;
        }

        // global function to get Status
        public static List<StatutDemande> getListStatus() {
            List<StatutDemande> listStatuts = new ArrayList<>();
            listStatuts.add(StatutDemande.PND);
            listStatuts.add(StatutDemande.CSL);
            listStatuts.add(StatutDemande.TO);
            listStatuts.add(StatutDemande.DLVRY);
            listStatuts.add(StatutDemande.WPU);
            listStatuts.add(StatutDemande.CAN);
            listStatuts.add(StatutDemande.REC);
            listStatuts.add(StatutDemande.INP);
            listStatuts.add(StatutDemande.FIN);

            return listStatuts;
        }
    }

    public enum SearchCriterias {
        value("value"), regex("regex");

        private String name;

        private SearchCriterias(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    public enum Module implements GenericEnum {
        PRICING(1, "Pricing", "pricing", PricingServiceImpl.class),
        TRANSPORT_MANAGEMENT(2, "Transport management", "transport-management", PricingServiceImpl.class),
        QUALITY_MANAGEMENT(3, "Quality management", "quality-management", QualityManagementServiceImpl.class),
        COMPLIANCE_MATRIX(4, "Compliance Matrix", "compliance_matrix"),
        TRACK(5, "Track & Trace", "track-trace", TrackServiceImpl.class),
        FREIGHT_AUDIT(7, "Freight Audit", "freight-audit", FreightAuditServiceImpl.class),
        FREIGHT_ANALYTICS(8, "Freight Analytics", "freight-analytics"),
        CUSTOM(9, "Custom", "custom"),
        ORDER_MANAGEMENT(10, "Order Management", "order-management"),
        USER_MANAGEMENT(16, "User Management", "user-management"),
        RECEIPT_SCHEDULING(11, "Receipt Scheduling", "receipt-scheduling"),
        ORDRE_TRANSPORT(12, "Ordre de transport", "ordre-transport"),
        DELIVERY_MANAGEMENT(18, "Delivery Management", "livraison"),
        COMPTA_MATIERE(19, "Material Accounting", "material-accounting"),
        ETABLISSEMENT_INFORMATION(20, "Etablissement Information", "etablissement information"),
        ORDER_LINE(21, "Articles informations", "articles-information"),
        LOAD_PLANNING(22, "LOAD PLANNING", "load_planning"),
        ADDITIONAL_COSTS(23, "ADDITIONAL COSTS", "additional-cost"),
        COSTS_ITEMS(24, "COSTS_ITEMS", "costs-items");

        private Integer code;
        private String libelle;
        private String refUrl;
        private Class<? extends CommonFlagSevice> flagServiceBean;

        public List<String> getListFlagForCarrier(Integer userNum) {
            if (getFlagServiceBean() == null) return new ArrayList<>();
            CommonFlagSevice service = (CommonFlagSevice) SpringUtility.getBean(getFlagServiceBean());
            return service.getListFlagForCarrier(userNum);
        }

        public List<String> getListFlagForControlTower(Integer userNum) {
            if (getFlagServiceBean() == null) return new ArrayList<>();
            CommonFlagSevice service = (CommonFlagSevice) SpringUtility.getBean(getFlagServiceBean());
            return service.getListFlagForControlTower(userNum);
        }

        private Module(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
            this.refUrl = null;
        }

        private Module(Integer code, String libelle, String refUrl) {
            this.code = code;
            this.libelle = libelle;
            this.refUrl = refUrl;
        }

        private Module(Integer code, String libelle, String refUrl, Class<? extends CommonFlagSevice> flagServiceBean) {
            this.code = code;
            this.libelle = libelle;
            this.refUrl = refUrl;
            this.flagServiceBean = flagServiceBean;
        }

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return null;
        }

        public String getLibelle() {
            return libelle;
        }

        public Class<?> getFlagServiceBean() {
            return flagServiceBean;
        }

        public String getRefUrl() {
            return refUrl;
        }

        public static String getLibelleByCode(Integer code) {
            Module m = null;

            if (code != null) {

                for (Module item: Module.values()) {

                    if (item.code.equals(code)) {
                        m = item;
                        break;
                    }

                }

            }

            return m != null ? m.getLibelle() : null;
        }

        public static Integer getCodeByLibelle(String libelle) {
            Module m = null;

            if (libelle != null) {

                for (Module item: Module.values()) {

                    if (item.libelle.equals(libelle)) {
                        m = item;
                        break;
                    }

                }

            }

            return m != null ? m.getCode() : null;
        }

        public static String getRefUrlByCode(Integer code) {
            Module m = null;

            if (code != null) {

                for (Module item: Module.values()) {

                    if (item.code.equals(code)) {
                        m = item;
                        break;
                    }

                }

            }

            return m != null ? m.getRefUrl() : null;
        }

        public static GenericEnum getByCode(Integer code) {
            try {
                return GenericEnumUtils
                    .getByCode(Module.class, code).orElseThrow(
                        () -> new IllegalArgumentException(String.format("Module with code %d does not exist", code)));
            } catch (GenericEnumException e) {
                LOGGER.error("GenericEnum Error", e);
                return null;
            }
        }
    }

    public enum StatutCarrier {
        RRE(1, "RRE", "Request received by FF"), // FF = Freight Forwarder
        QSE(2, "QSE", "Quote sent by FF"),
        REC(3, "REC", "Recommendation by CT"),
        FIN(4, "FIN", "Final choice"),
        NOT_AWARDED(5, "NOT_AWARDED", "Not Awarded"),
        TRANSPORT_PLAN(6, "TRANSPORT_PLAN", "Transport plan"),
        FIN_PLAN(7, "FIN_PLAN", "Final choice(Transport plan)");

        private Integer code;
        private String libelle;
        private String codeLibelle;

        private StatutCarrier(Integer code, String codeLibelle, String libelle) {
            this.code = code;
            this.libelle = libelle;
            this.codeLibelle = codeLibelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public String getCodeLibelle() {
            return codeLibelle;
        }

        public static Integer getCodeByCodeLibelle(String libelle) {

            if (libelle != null) {
                String value = libelle.toLowerCase();
                Integer code = null;

                for (StatutCarrier item: StatutCarrier.values()) {
                    if (item.codeLibelle.toLowerCase().equals(value)) code = item.code;
                }

                return code;
            }

            return null;
        }

        public static Integer getCodeByLibelle(String libelle) {

            if (libelle != null) {
                String value = libelle.toLowerCase();
                Integer originCustomsBroker = null;

                for (StatutCarrier item: StatutCarrier.values()) {
                    if (item.libelle.toLowerCase().equals(value)) originCustomsBroker = item.code;
                }

                return originCustomsBroker;
            }

            return null;
        }

        public static String getLibelleByCode(Integer code) {
            StatutCarrier s = null;

            if (code != null) {

                for (StatutCarrier item: StatutCarrier.values()) {

                    if (item.code.equals(code)) {
                        s = item;
                        break;
                    }

                }

            }

            return s != null ? s.getLibelle() : null;
        }

        public static String getListValuationType() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            ObjectNode jsonObj1 = mapper.createObjectNode();
            jsonObj1.put("code", StatutCarrier.FIN.code);
            jsonObj1.put("libelle", "Quote");
            arrayNode.add(jsonObj1);

            ObjectNode jsonObj2 = mapper.createObjectNode();
            jsonObj2.put("code", StatutCarrier.FIN_PLAN.code);
            jsonObj2.put("libelle", "Transport Plan");
            arrayNode.add(jsonObj2);

            return arrayNode.toString();
        }
    }

    public enum IncidentResponsibleType {
        ALIS(0, "ALIS"),
        CUSTOM_BROKER(1, "Custom Broker"),
        CARRIER(2, "Carrier"),
        INSURRANCE(3, "Insurance"),
        OTHER(4, "Other");

        private Integer code;
        private String libelle;

        private IncidentResponsibleType(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum IncidentStatus {
        INTENT_TO_CLAIM(1, "Intent to claim"), CLAIM(2, "Claim"), RECOVERED(3, "Recovered"), CLOSED(4, "Closed");

        private Integer code;
        private String libelle;

        private IncidentStatus(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String toJsonList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (IncidentStatus item: IncidentStatus.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }

        public static String getListStatutIncident() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            JsonNode node2 = mapper.readTree(mapper.writeValueAsString(IncidentStatus.INTENT_TO_CLAIM));
            JsonNode node3 = mapper.readTree(mapper.writeValueAsString(IncidentStatus.CLAIM));
            JsonNode node4 = mapper.readTree(mapper.writeValueAsString(IncidentStatus.RECOVERED));
            JsonNode node5 = mapper.readTree(mapper.writeValueAsString(IncidentStatus.CLOSED));

            arrayNode.add(node2);
            arrayNode.add(node3);
            arrayNode.add(node4);
            arrayNode.add(node5);

            return arrayNode.toString();
        }

        public static List<CodeLibelleDTO> getListStatutIncidentDTO() throws IOException {
            List<CodeLibelleDTO> list = new ArrayList<CodeLibelleDTO>();

            for (IncidentStatus item: IncidentStatus.values()) {
                list.add(new CodeLibelleDTO(item.code, item.libelle));
            }

            return list;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {

                for (IncidentStatus item: IncidentStatus.values()) {
                    if (item.code.equals(code)) return item.libelle;
                }

            }

            return null;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum FlowType {
        INBOUND(0, "Inbound"), OUTBOUND(1, "Outbound");

        private Integer code;
        private String libelle;

        private FlowType(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum FileType {
        INTERCO_INVOICE(1, "INTERCO_INVOICE", "Supplier invoice"),
        COMMERCIAL_INVOICE(2, "COMMERCIAL_INVOICE", "Commercial invoice"),
        CUSTOMS(3, "CUSTOMS", "Customs"),
        TRANSPORT_DOCUMENT(4, "TRANSPORT_DOCUMENT", "Transport document"),
        OTHER(5, "OTHER", "Other"),
        POD(6, "POD", "POD"),
        DELIVERY(7, "DELIVERY", "Delivery"),
        ORDER(8, "ORDER", "Order");

        private Integer code;
        private String libelle;
        private String cammelLibelle;

        private FileType(Integer code, String libelle, String cammelLibelle) {
            this.code = code;
            this.libelle = libelle;
            this.cammelLibelle = cammelLibelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public String getCammelLibelle() {
            return cammelLibelle;
        }

        public static FileType getFileTypeByCode(Integer code) {
            FileType res = null;

            for (FileType ft: FileType.values()) {

                if (ft.getCode().equals(code)) {
                    res = ft;
                    break;
                }

            }

            return res;
        }
    }

    public enum SavedFormComponent {
        SAVED_SEARCH(1, "SAVED SEARCH"),
        PRICING_MASK(2, "PRICING MASK"),
        FAVORIS(3, "FAVORIS"),
        HISTORY(4, "HISTORY"),
        SAVED_SEARCH_ACTION(6, "SAVED_SEARCH_ACTION");

        private Integer code;
        private String libelle;

        private SavedFormComponent(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum SavedSearchAction {
        CHANGE_STATUS(1, "CHANGE_STATUS"), ADD_FLAG(2, "ADD_FLAG"), SEND_ALERT_TO(3, "SEND_ALERT_TO");

        private Integer code;
        private String libelle;

        private SavedSearchAction(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String toJsonList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (SavedSearchAction item: SavedSearchAction.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum SavedSearchActionTrigger {
        ON_CREATE(1, "ON_CREATE"), ON_UPDATE(2, "ON_UPDATE"), DAILY(3, "DAILY");

        private Integer code;
        private String libelle;

        private SavedSearchActionTrigger(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String toJsonList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (SavedSearchActionTrigger item: SavedSearchActionTrigger.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum DatatableComponent {
        PRICING(1, "Pricing"),
        BOOKING(2, "Booking"),
        QUALITY(3, "Quality management"),
        TRACK(5, "Track & Trace"),
        CUSTOM(9, "Custom"),
        ORDER(11, "Order"),
        DELIVERY(12, "Delivery");

        private Integer code;
        private String libelle;

        private DatatableComponent(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum NatureDemandeTransport {
        NORMAL(0, "NORMAL", "A"),
        TRANSPORT_FILE(1, "TRANSPORT_FILE", "C"),
        MASS_IMPORT(2, "MASS_IMPORT", "B"),
        MASTER_OBJECT(3, "MASTER_OBJECT", "J"),
        INITIAL(4, "INITIAL", "K"),
        TRANSPORT_PRINCIPAL(5, "TRANSPORT_PRINCIPAL", "L"),
        POST_ACHEMINEMENT(6, "POST_ACHEMINEMENT", "M"),
        CONSOLIDATION(7, "CONSOLIDATION", "S"),
        EDI(8, "EDI", "E");// EDI : 'nature' of the demande added by the EDI
                           // through web service

        private Integer code;
        private String libelle;
        private String letter;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        public String getLetter() {
            return letter;
        }

        public void setLetter(String letter) {
            this.letter = letter;
        }

        // Constructeur
        private NatureDemandeTransport(Integer code, String libelle, String letter) {
            this.code = code;
            this.libelle = libelle;
            this.letter = letter;
        }

        public static Integer getCodeByLibelle(String libelleStr) {

            if (libelleStr != null) {
                String libelle = libelleStr.toLowerCase();
                Integer originCustomsBroker = null;

                for (NatureDemandeTransport item: NatureDemandeTransport.values()) {
                    if (item.libelle.toLowerCase().contains(libelle)) originCustomsBroker = item.code;
                }

                return originCustomsBroker;
            }

            return null;
        }

        // récuppération de la liste format JSON
        public static String getListNatureDemandeTransport() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            arrayNode.add(mapper.readTree(mapper.writeValueAsString(NatureDemandeTransport.NORMAL)));
            arrayNode.add(mapper.readTree(mapper.writeValueAsString(NatureDemandeTransport.TRANSPORT_FILE)));
            arrayNode.add(mapper.readTree(mapper.writeValueAsString(NatureDemandeTransport.MASS_IMPORT)));
            arrayNode.add(mapper.readTree(mapper.writeValueAsString(NatureDemandeTransport.MASTER_OBJECT)));
            arrayNode.add(mapper.readTree(mapper.writeValueAsString(NatureDemandeTransport.INITIAL)));
            arrayNode.add(mapper.readTree(mapper.writeValueAsString(NatureDemandeTransport.TRANSPORT_PRINCIPAL)));
            arrayNode.add(mapper.readTree(mapper.writeValueAsString(NatureDemandeTransport.POST_ACHEMINEMENT)));

            return arrayNode.toString();
        }

        @JsonCreator
        public static NatureDemandeTransport fromNode(JsonNode node) {
            if (!node.has("libelle")) return null;

            String name = node.get("libelle").asText().toUpperCase();

            return NatureDemandeTransport.valueOf(name);
        }

        public static NatureDemandeTransport fromCode(Integer code) {
            return NatureDemandeTransport.values()[code];
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum MasterObjectType {
        MULTI_SEGMENT(0, "MULTI_SEGMENT"), MULTI_SEGMENT_DOCK(1, "MULTI_SEGMENT_DOCK");

        private Integer code;
        private String libelle;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        // Constructeur
        private MasterObjectType(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        // récuppération de la liste format JSON
        public static String getListNatureDemandeTransport() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            arrayNode.add(mapper.readTree(mapper.writeValueAsString(MasterObjectType.MULTI_SEGMENT)));
            arrayNode.add(mapper.readTree(mapper.writeValueAsString(MasterObjectType.MULTI_SEGMENT_DOCK)));

            return arrayNode.toString();
        }

        @JsonCreator
        public static NatureDemandeTransport fromNode(JsonNode node) {
            if (!node.has("libelle")) return null;

            String name = node.get("libelle").asText().toUpperCase();

            return NatureDemandeTransport.valueOf(name);
        }

        public static NatureDemandeTransport fromCode(Integer code) {
            return NatureDemandeTransport.values()[code];
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum RoleSociete {
        ROLE_CHARGEUR(1, "Chargeur"), ROLE_PRESTATAIRE(2, "Prestataire"), ROLE_CONTROL_TOWER(3, "Control Tower");

        private Integer code;

        private String libelle;

        // public static Role getByCode(Integer code) {
        // if (code != null) {
        // Role roleUser = null;
        // for (Role item : Role.values()) {
        // if (item.code.equals(code))
        // roleUser = item;
        // }
        // return roleUser;
        // }
        //
        // return null;
        // }

        private RoleSociete(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum AccessRight {
        CONTRIBUTION(0, "Contribution"), VISUALISATION(1, "Visualisation"), NON_VISIBLE(2, "Non visible");

        private Integer code;
        private String libelle;

        private AccessRight(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum OriginCustomsBroker {
        MYTOWER_OPERATION(1, "MyTower operation"), CARRIER(2, "Carrier"), NON_APPLICABLE(3, "Not applicable");

        private Integer code;
        private String libelle;

        private OriginCustomsBroker(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static Integer getCodeByLibelle(String libelle) {

            if (libelle != null) {
                Integer originCustomsBroker = null;

                for (OriginCustomsBroker item: OriginCustomsBroker.values()) {
                    if (item.libelle.toLowerCase().contains(libelle)) originCustomsBroker = item.code;
                }

                return originCustomsBroker;
            }

            return null;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                String originCustomsBroker = null;

                for (OriginCustomsBroker item: OriginCustomsBroker.values()) {
                    if (item.code.equals(code)) originCustomsBroker = item.libelle;
                }

                return originCustomsBroker;
            }

            return null;
        }

        public static List<String> getListOriginCustomsBrokerLibelle() {
            List<String> res = new ArrayList<String>();

            for (OriginCustomsBroker item: OriginCustomsBroker.values()) {
                res.add(item.getLibelle());
            }

            return res;
        }
    }

    public enum PostsCout {
        /**
         * ***********SEA***********
         */
        POSITIONNEMENT_ENLEVEMENT(1, "Positionnement / Enlèvement", 1, 2),
        EMBARQUEMENT(2, "Embarquement", 1, 2),
        INTERVENTION_DOUANE_SEA_FOB(3, "Intervention & Douane", 1, 2),
        FRAIS_DE_DOSSIERS_DOCUMENTATION_SEA_FOB(4, "Frais de dossiers & Documentation", 1, 2),
        DGR_SEA_FOB(5, "DGR", 1, 2),
        AUTRE_SEA_FOB(6, "Autre", 1, 2),

        PORT_PORT(7, "Port / Port", 2, 2),
        SURCHARGES(8, "Surcharges", 2, 2),
        AUTRE_SEA_FRET(9, "Autre", 2, 2),

        DEBARQUEMENT(10, "Débarquement", 3, 2),
        FRAIS_DOCUMENTATION_DOSSIER_SEA_DEST(11, "Frais de documentation & Dossier", 3, 2),
        INTERVENTION_DOUANE_SEA_DEST(12, "Intervention & Douane", 3, 2),
        POST_ACHEMINEMENT_LIVRAISON_SEA_DEST(13, "Post acheminement, Livraison", 3, 2),
        FRANCHISE_EN_JOURS_SEA_DEST(14, "Franchise en jours", 3, 2),
        FRAIS_DE_STATIONNEMENT_STORAGE_SEA_DEST(15, "Frais de stationnement (Storage)", 3, 2),
        FRAIS_DE_SURRESTARIES_DETENTION_POUR_LE_FCL(16, "Frais de Surrestaries / Detention (Pour le FCL)", 3, 2),
        AUTRE_SEA_DEST(17, "Autre", 3, 2),

        ASSURANCE_SEA(18, "Assurance", 4, 2),
        AUTRE_SEA_DIVERS(19, "Autre", 4, 2),

        /**
         * ***********AIR***********
         */
        ENLEVEMENT(20, "Enlèvement", 1, 1),
        INTERVENTION_DOUANE_AIR_FOB(21, "Intervention & Douane", 1, 1),
        FRAIS_DOCUMENTATION_DOSSIER_AIR_FOB(22, "Frais de documentation & Dossier", 1, 1),
        DGR_AIR_FOB(23, "DGR", 1, 1),
        AUTRE_AIR_FOB(24, "Autre", 1, 1),

        FRET_AERIEN(25, "Fret aérien", 2, 1),
        SURCHARGES_FUEL(26, "Surcharges fuel", 2, 1),
        SURCHARGES_IRC(27, "Surcharges IRC", 2, 1),
        AUTRE_AIR_FRET(28, "Autre", 2, 1),

        FRAIS_DOCUMENTATION_DOSSIER_AIR_DEST(29, "Frais de documentation & Dossier", 3, 1),
        INTERVENTION_DOUANE_AIR_DEST(30, "Intervention & Douane", 3, 1),
        POST_ACHEMINEMENT_LIVRAISON_AIR_DEST(31, "Post acheminement, Livraison", 3, 1),
        FRANCHISE_EN_JOURS_AIR_DEST(32, "Franchise en jours", 3, 1),
        FRAIS_DE_STATIONNEMENT_STORAGE_AIR_DEST(33, "Frais de stationnement (Storage),", 3, 1),
        AUTRE_AIR_DEST(34, "Autre", 3, 1),

        ASSURANCE_AIR(35, "Assurance", 4, 1),
        AUTRE_AIR_DIVERS(36, "Autre", 4, 2),

        /**
         * ***********ROAD***********
         */
        TARIF_DOOR_TO_DOOR(37, "Tarif door to door", 2, 3),
        SURCHARGES_FUEL_ROAD_FRET(38, "Surcharges  fuel", 2, 3),
        DGR_ROAD_DIVERS(39, "DGR", 2, 3),
        AUTRE_ROAD_FRET(50, "Autre", 2, 3),

        INTERVENTION_DOUANE_ROAD_DIVERS(40, "Intervention & Douane", 4, 3),
        ASSURANCE_ROAD(41, "Assurance", 4, 3),
        FRAIS_DE_DOSSIERS(42, "Frais de dossiers", 4, 3),
        AUTRE_ROAD_DIVERS(43, "Autre", 4, 3),

        /**
         * ***********INTERGRATEUR***********
         */
        TARIF(44, "Tarif", 2, 4),
        SURCHARGES_FUEL_INTEGRATOR(45, "Surcharges fuel", 2, 4),
        DGR(46, "DGR", 2, 4),
        OVERSIZE(47, "Oversize", 2, 4),
        OVERWEIGHT(48, "Overweight", 2, 4),
        AUTRE_INTEGRATOR_FRET(49, "Autre", 2, 4),

        INTERVENTION_DOUANE_INTEGRATOR(50, "Intervention & Douane", 8, 4),
        ASSURANCE_INTEGRATOR(51, "Assurance", 2, 4),
        AUTRE_INTEGRATOR_DIVERS(52, "Autre", 2, 4);

        private Integer code;
        private String libelle;
        private Integer categorie;
        private Integer modeTransport;

        private PostsCout(Integer code, String libelle, Integer categorie, Integer modeTransport) {
            this.code = code;
            this.libelle = libelle;
            this.categorie = categorie;
            this.modeTransport = modeTransport;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public Integer getCategorie() {
            return categorie;
        }

        public Integer getModeTransport() {
            return modeTransport;
        }
    }

    public enum PostsCoutCategorie {
        MISE_FOB(1, "Mise à FOB"), FRET(2, "Fret"), FRAIS_DESTINATION(3, "Frais à destination"), DIVERS(4, "Divers");

        private Integer code;
        private String libelle;

        private PostsCoutCategorie(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum IDChatComponent {
        FREIGHT_AUDIT(1, "Freight Audit"),
        TRANSPORT_MANAGEMENT(2, "Transport Management"),
        TRACK(3, "Track & Trace"),
        PRICING(4, "Pricing"),
        CUSTOM(5, "Custom"),
        QUALITY_MANAGEMENT(6, "qm"),
        ORDER_MANAGEMENT(7, "Order Management"),
        DELIVERY(8, "Delivery Management");

        private Integer code;
        private String libelle;

        private IDChatComponent(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static Integer getCodeByLibelle(String libelle) {
            IDChatComponent m = null;

            if (libelle != null) {

                for (IDChatComponent item: IDChatComponent.values()) {

                    if (item.libelle.equals(libelle)) {
                        m = item;
                        break;
                    }

                }

            }

            return m != null ? m.getCode() : null;
        }
    }

    public enum MailSemiAuto {
        QM_INTENT_TO_CLAIM(37, "MyTower- Intent to claim - Incident ref", "quality/37-email-intent-to-claim"),
        QM_INCIDENT_QUALIFICATION_CLAIM(
            38,
            "MyTower- Incident qualification - Incident ref",
            "quality/38-incident-qualification-claim"),
        QM_INCIDENT_QUALIFICATION_STATEMENT(
            39,
            "MyTower- Incident qualification - Incident ref",
            "quality/39-incident-qualification-statement"),
        QM_CONFIRMATION_PAYMENT(41, "MyTower- Compensation receipt - Incident ref", "quality/41-confirmation-payment"),
        QM_QUALIFICATION_MODIF_TO_CLAIM(
            42,
            "MyTower- Incident qualification modification - Incident ref",
            "quality/42-qualif-modification-statement-to-claim"),
        QM_QUALIFICATION_MODIF_TO_STATEMENT(
            43,
            "MyTower- Incident qualification modification - Incident ref",
            "quality/43-qualif-modification-claim-to-statement"),

        CPTM_PROLONGER_IMPORTATION_TEMPORAIRE(
            44,
            "cptm.mail.prolonger_import_temp.subject",
            "cptm/44-46-47-48-email-compta-matiere"),
        CPTM_LIQUIDATION_OFFICE(46, "cptm.mail.liquidation_office.subject", "cptm/44-46-47-48-email-compta-matiere"),
        CPTM_RELANCE(47, "cptm.mail.relance.subject", "cptm/44-46-47-48-email-compta-matiere"),
        CPTM_DEMANDE_ETAT_AVANCEMENT(
            48,
            "cptm.mail.demande_etat_evancement.subject",
            "cptm/44-46-47-48-email-compta-matiere"),
        QM_FINANCE_REMINDER(49, "MyTower- Compensation receipt - Incident ref", "quality/40-finance-reminder");

        private Integer code;
        private String subject;
        private String template;

        public String getTemplate() {
            return template;
        }

        public void setTemplate(String template) {
            this.template = template;
        }

        public Integer getCode() {
            return code;
        }

        public String getSubject() {
            return subject;
        }

        private MailSemiAuto(Integer code, String subject, String template) {
            this.code = code;
            this.subject = subject;
            this.template = template;
        }

        public static MailSemiAuto auto(Integer code) {
            MailSemiAuto semiAuto = null;

            if (code != null) {

                for (MailSemiAuto auto: MailSemiAuto.values()) {

                    if (auto.code.equals(code)) {
                        semiAuto = auto;
                    }

                }

                return semiAuto;
            }

            return null;
        }
    }

    public enum TypeDataEbDemande {
        EXPEDITION_INFORMATION(1, "Expedition Informations"),
        REFERENCE_INFORMATION(2, "Reference Informations"),
        GOODS_INFORMATION(3, "Goods information"),
        EXTERNAL_USERS(4, "External users"),
        ALL(5, "Update all data");

        private Integer code;
        private String libelle;

        private TypeDataEbDemande(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum CancelStatus {
        CANECLLED(1, "Cancelled");

        private Integer code;
        private String libelle;

        private CancelStatus(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum Flow {
        IMPORT(1, "import"), EXPORT(2, "export");

        private Integer code;
        private String libelle;

        private Flow(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getListFlow() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            JsonNode node2 = mapper.readTree(mapper.writeValueAsString(Flow.IMPORT));
            JsonNode node3 = mapper.readTree(mapper.writeValueAsString(Flow.EXPORT));

            arrayNode.add(node2);
            arrayNode.add(node3);

            return arrayNode.toString();
        }
    }

    public enum VersionGrid {
        PREVIOUS(0, "Previous"), VALID(1, "Valid"), UPCOMING(2, "Upcoming");

        private Integer code;
        private String libelle;

        private VersionGrid(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum Insurance {
        Yes(0, "Yes"), No(1, "No");

        private Integer code;
        private String libelle;

        private Insurance(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                Insurance in = null;

                for (Insurance item: Insurance.values()) {
                    if (item.code.equals(code)) in = item;
                }

                return in.getLibelle();
            }

            return null;
        }

        public static String toJsonList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (Insurance item: Insurance.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum ClassificationIncident {
        Incident(0, "Incident"), Claim(1, "Claim"), Statement(2, "Statement");

        private Integer code;
        private String libelle;

        private ClassificationIncident(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String toJsonList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (ClassificationIncident item: ClassificationIncident.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum EmailType {
        Intent_to_claim(37, "Intent to claim"),
        Incident_qualification_claim(38, "Incident qualification: claim"),
        Incident_qualification_statement(39, "Incident qualification: statement"),
        Finance_reminder_waiting_for_compensation(40, "Finance reminder : waiting for compensation"),
        Comfirmation_compensation_payment(41, "Comfirmation compensation payment"),
        Qualification_modification_statement_claim(42, "Qualification modification statement -> claim"),
        Qualification_modification_claim_statement(43, "Qualification modification claim -> statement");

        private Integer code;
        private String libelle;

        private EmailType(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getByCode(Integer code) {

            if (code != null) {
                EmailType ac = null;

                for (EmailType item: EmailType.values()) {
                    if (item.code.equals(code)) ac = item;
                }

                return ac.getLibelle();
            }

            return null;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum AdresseEligibility {
        INTERMEDIATE_POINT(0, "INTERMEDIATE_POINT", "INP");

        private Integer code;
        private String libelle;
        private String reference;

        public String getReference() {
            return reference;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        // Constructeur
        private AdresseEligibility(Integer code, String libelle, String reference) {
            this.code = code;
            this.libelle = libelle;
            this.reference = reference;
        }

        // récuppération de la liste format JSON
        public static String getListEligibility() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            arrayNode.add(mapper.readTree(mapper.writeValueAsString(AdresseEligibility.INTERMEDIATE_POINT)));

            return arrayNode.toString();
        }

        @JsonCreator
        public static AdresseEligibility fromNode(JsonNode node) {
            if (!node.has("libelle")) return null;

            String name = node.get("libelle").asText().toUpperCase();

            return AdresseEligibility.valueOf(name);
        }

        public static AdresseEligibility fromCode(Integer code) {
            return AdresseEligibility.values()[code];
        }
    }

    public enum BoutonAction {
        // MTG-1384 MEP > Eléments à cacher suite MEP
        // FILTERS(2, "Filters"), MASQUES(1, "Masques"),
        NEW(1, "New"), DASHBOARD(2, "Dashboard"), FILTER(3, "Filter");

        private Integer code;
        private String libelle;

        private BoutonAction(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String toJsonList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (BoutonAction item: BoutonAction.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum BoutonActionNewTarget {
        PRICING_REQUEST(1, "Pricing request", "/app/pricing/creation"),
        PURCHASE_ORDER(2, "Purshase order", "app/delivery-overview/order/creation"),
        // Targets no longer available, see CDT-170
        // TRANSPORT_FILE(3, "Transport file",
        // "app/transport-management/creation"),
        // INVOICE(4, "Invoice", "app/freight-audit"),
        // CLAIM(5, "Claim", "/app/quality-management/create"),
        INCIDENT(6, "Incident", "/app/quality-management/create"),
        // ROOT_CAUSE(7, "Root cause", "/app/quality-management/dashboard"),
        // CORRECTIVE_ACTION(8, "Corrective action",
        // "/app/quality-management/dashboard"),
        // RULE(9, "Rule", "/app"),
        USER(10, "User", "/app/user/settings/company/users/creation");
        // ESTABLISHEMENT(11, "Establishement", "/app/user/settings"),
        // ADRESS(11, "Adress", "/app/user/settings"),
        // DELIVERY(12, "Transport file by delivery",
        // "/app/delivery-overview/delivery/create-tf");

        private Integer code;
        private String libelle;
        private String target;

        private BoutonActionNewTarget(Integer code, String libelle, String target) {
            this.code = code;
            this.libelle = libelle;
            this.target = target;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public String getTarget() {
            return target;
        }

        public static String toJsonList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (BoutonActionNewTarget item: BoutonActionNewTarget.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);
                jsonObj.put("target", item.target);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum BoutonActionDashboardTarget {
        PRICING(1, "Pricing", "app/pricing/dashboard"),
        TRANSPORT_MANAGEMENT(2, "Transport Management", "/app/transport-management/dashboard"),
        TRACK_TRACE(3, "Track Trace", "app/track-trace/dashboard"),
        QUALITY_CLAIMS(4, "Quality claims", "app/quality-management/dashboard"),
        // dashboard no longer available see CDT-170
        // QUALITY_DEVIATIONS(5, "Quality deviations",
        // "app/quality-management/dashboard"),
        // QUALITY_ROOT_CAUSES(6, "Quality root causes analyses",
        // "app/quality-management/dashboard"),
        // QUALITY_CORRECTIVE(
        // 7,
        // "Quality corrective & preventive actions",
        // "app/quality-management/dashboard"
        // ),
        FREIGHT_AUDIT(8, "Freight audit", "app/freight-audit"),
        FREIGHT_ANALITICS(9, "Freight analytics", "app/freight-analytics"),
        CUSTOMS(10, "Customs", "app/custom/dashboard"),
        ORDER(11, "Order", "app/delivery-overview/order/dashboard"),
        DELIVERY(13, "Delivery", "app/delivery-overview/delivery/dashboard");

        private Integer code;
        private String libelle;
        private String target;

        private BoutonActionDashboardTarget(Integer code, String libelle, String target) {
            this.code = code;
            this.libelle = libelle;
            this.target = target;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public String getTarget() {
            return target;
        }

        public static String toJsonList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (BoutonActionDashboardTarget item: BoutonActionDashboardTarget.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);
                jsonObj.put("target", item.target);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum FlagIcon {
        ROAD(1, "ROAD"),
        SHIP(2, "SHIP"),
        PLANE(3, "PLANE"),
        TRAIN(4, "TRAIN"),
        CAR(5, "CAR"),
        COG(6, "COG"),
        SQUARE(7, "SQUARE"),
        CIRCLE(8, "CIRCLE"),
        TRUCK(9, "TRUCK"),
        ADDRESS_CARD(10, "ADDRESS_CARD"),
        EXCLAMATION_TRIANGLE(11, "EXCLAMATION_TRIANGLE"),
        ORDER(12, "ORDER"),
        DELIVERY(13, "DELIVERY"),
        PRICING(14, "PRICING"),
        TRANSPORT(15, "TRANSPORT"),
        TRACK_TRACE(16, "TRACK_TRACE"),
        QUALITY(17, "QUALITY"),
        FREIGTH_AUDIT(18, "FREIGTH_AUDIT"),
        FREIGTH_ANALYTICS(19, "FREIGTH_ANALYTICS"),
        CUSTOMS(20, "CUSTOMS"),
        RECEIPT_SCHEDULING(21, "RECEIPT_SCHEDULING"),
        ADD(22, "ADD"),
        SEARCH(23, "SEARCH");

        private Integer code;
        private String libelle;

        private FlagIcon(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String toJsonList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (FlagIcon item: FlagIcon.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum Extractors {
        PRICING(1, "Pricing", "pricing"),
        TRANSPORT_MANAGEMENT(2, "Transport management", "tm"),
        INCOERENCE_STATUT_TM_TT(3, "Incohérence du statut entre TM & TT", "incoherence-tm-tt"),
        INCOERENCE_STATUT_TT_PSL(4, "Incohérence du statut entre TT & PSL", "incoherence-tt-psl"),
        SAVINGS(5, "Savings", "savings"),
        GLOBAL_DATA(6, "Global data", "global-data");

        private Integer code;

        private String libelle;

        private String name;

        private Extractors(Integer code, String libelle, String name) {
            this.code = code;
            this.libelle = libelle;
            this.name = name;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public String getName() {
            return name;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                Extractors ac = null;

                for (Extractors item: Extractors.values()) {
                    if (item.code.equals(code)) ac = item;
                }

                return ac.getLibelle();
            }

            return null;
        }

        public static String getListExtractors() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (Extractors item: Extractors.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);
                jsonObj.put("name", item.name);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum ActionFreightAudit {
        APPLYLISTINVOICEPRICE(1, "En applyListInvoicePrice"),
        APPLYLISTINVOICENUMBER(2, "applyListInvoiceNumber"),
        APPLYLISTSTATUT(3, "applyListStatut"),
        APPLYLISTDATE(4, "applyListDate"),
        APPLYLISTMEMO(5, "applyListMemo"),
        SHAREDPRICE(6, "sharedPrice"),
        SHAREDNUMBER(7, "sharedNumber"),
        SHAREDSTATUS(8, "sharedStatus"),
        SHAREDMONTHANDYEAR(9, "sharedMonthAndYear");

        private Integer code;
        private String libelle;

        private ActionFreightAudit(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum PriseRDVStatus {
        INISTIAL("initial", 0), ANNULEE("annulée", 1), VALIDEE("validée", 2);

        private Integer code;
        private String libelle;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        // Constructeur
        private PriseRDVStatus(String libelle, Integer code) {
            this.code = code;
            this.libelle = libelle;
        }

        public static List<PriseRDVStatus> getListAction() {
            List<PriseRDVStatus> priseRDV = new ArrayList<PriseRDVStatus>();
            priseRDV.add(PriseRDVStatus.INISTIAL);
            priseRDV.add(PriseRDVStatus.ANNULEE);
            priseRDV.add(PriseRDVStatus.VALIDEE);
            return priseRDV;
        }
    }

    public enum QrGroupeVentilation {
        PAR_TF_AU_PRORATA_DU_POIDS_TAXABLE(1, "Par dossier au prorata du poids chargeable"),
        PAR_TF_AU_PRORATA_DES_QUANTITES(2, "Par dossier au prorata des quantités"),
        A_LA_TF(3, "Équitablement par dossier de transport"),
        SANS_PONDERATION(4, "Sans pondération");

        private Integer code;

        private String libelle;

        private QrGroupeVentilation(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                QrGroupeVentilation ac = null;

                for (QrGroupeVentilation item: QrGroupeVentilation.values()) {
                    if (item.code.equals(code)) ac = item;
                }

                return ac.getLibelle();
            }

            return null;
        }

        public static String getListQrGroupeVentilation() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (QrGroupeVentilation item: QrGroupeVentilation.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum QrGroupeTrigger {
        DEFAULT(0, "default"),
        ON_RULE_UPDATE(1, "On rule update"),
        ON_RULE_CREATION(2, "On rule creation"),
        ON_TRANSPORT_FILE_UPDATE(3, "On transport file update"),
        ON_TRANSPORT_FILE_CREATION(4, "On transport file creation"),
        ON_MANUAL_DEMANDE(5, "On manual demande");

        private Integer code;

        private String libelle;

        QrGroupeTrigger(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                QrGroupeTrigger ac = null;

                for (QrGroupeTrigger item: QrGroupeTrigger.values()) {
                    if (item.code.equals(code)) ac = item;
                }

                return ac.getLibelle();
            }

            return null;
        }

        public static String getListGroupeTrigger() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (QrGroupeTrigger item: QrGroupeTrigger.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum QrGroupeOutput {
        CONSOLIDATION_AUTOMATIQUE(1, "Consolidation automatique"),
        PROPOSITION_DE_CONSOLIDATION(2, "Proposition de consolidation");

        private Integer code;

        private String libelle;

        private QrGroupeOutput(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                QrGroupeOutput ac = null;

                for (QrGroupeOutput item: QrGroupeOutput.values()) {
                    if (item.code.equals(code)) ac = item;
                }

                return ac.getLibelle();
            }

            return null;
        }

        public static String getListGroupeOutput() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (QrGroupeOutput item: QrGroupeOutput.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum GroupeField {
        MODE_DE_TRANSPORT(1, "MODE_DE_TRANSPORT"),
        ZONE_ORIGINE(2, "ZONE_ORIGINE"),
        ZONE_DESTINATION(3, "ZONE_DESTINATION"),
        ORIGINE(4, "ORIGINE"),
        DESTINATION(5, "DESTINATION"),
        NIVEAU_DE_SERVICE(6, "NIVEAU_DE_SERVICE"),
        TYPE_DE_TRANSPORT(7, "TYPE_DE_TRANSPORT"),
        // CUSTOMS_PROCEDURE(8, "Customs procedure"),//c'est pas encore traité
        DGR_TYPE(9, "DGR_TYPE"),
        // FLAG(10, "Flag"),
        CATEGORIES(11, "CATEGORIES"),
        // COMPANY_ORIGIN(12, "Origin's Company"),
        // COMPANY_DESTINATION(13, "Destination's Company"),
        INCOTERM(14, "INCOTERM"),
        VILLE_INCOTERM(15, "VILLE_INCOTERM"),
        DATE_AVAILABILITY(16, "DATE_AVAILABILITY"),
        DATE_ARRIVAL(17, "DATE_ARRIVAL"),
        ELIGIBILITE(18, "ELIGIBILITE"),
        TRANSPORTEUR(19, "TRANSPORTEUR"),
        ZONE_ORIGINE_GT(20, "ZONE_ORIGINE_GT"),
        ZONE_DESTINATION_GT(21, "ZONE_ORIGINE_GT"),
        CUSTOM_FIELDS(22, "CUSTOM_FIELDS"),
        PART_NUMBER(23, "PART_NUMBER"),
        MAPPING_CODE_PSL(51, "MAPPING_CODE_PSL"),
        STATUS(52, "STATUS");

        private Integer code;

        private String libelle;

        private GroupeField(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                GroupeField ac = null;

                for (GroupeField item: GroupeField.values()) {
                    if (item.code.equals(code)) ac = item;
                }

                return ac.getLibelle();
            }

            return null;
        }

        public static String getListGroupeField() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (GroupeField item: GroupeField.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum GroupeFunction {
        ARE_COMPATIBLE(1, "ARE_COMPATIBLE"), ARE_EQUAL(2, "ARE_EQUAL"), ARE_EXCLUDED(3, "ARE_EXCLUDED"),
        // GAP_IN_HOURS_MAX(3, "Gap in hours max"),
        // ARE_DIFFERENT(4, "Are different"),
        ;

        private Integer code;

        private String libelle;

        private GroupeFunction(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                GroupeFunction ac = null;

                for (GroupeFunction item: GroupeFunction.values()) {
                    if (item.code.equals(code)) ac = item;
                }

                return ac.getLibelle();
            }

            return null;
        }

        public static String getListGroupeFunction() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (GroupeFunction item: GroupeFunction.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum GroupeSousFunction {
        START_BY(1, "START_BY"), FINISH_BY(2, "FINISH_BY"), CONTAINS_VALUE(3, "CONTAINS_VALUE");

        private Integer code;

        private String libelle;

        private GroupeSousFunction(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                GroupeSousFunction ac = null;

                for (GroupeSousFunction item: GroupeSousFunction.values()) {
                    if (item.code.equals(code)) ac = item;
                }

                return ac.getLibelle();
            }

            return null;
        }

        public static String getListGroupeSousFunction() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (GroupeSousFunction item: GroupeSousFunction.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum GroupePropositionStatut {
        CONFIRMED(1, "Confirmed"), REJECTED(2, "Rejected"), WAITING_FOR_CONFIRMATION(3, "Waiting for confirmation");

        private Integer code;

        private String libelle;

        private GroupePropositionStatut(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                GroupeFunction ac = null;

                for (GroupeFunction item: GroupeFunction.values()) {
                    if (item.code.equals(code)) ac = item;
                }

                return ac.getLibelle();
            }

            return null;
        }

        public static String getListGroupePropositionStatut() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (GroupePropositionStatut item: GroupePropositionStatut.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }
    }

    public enum GroupeTypeTraitemant implements GenericEnum {
        BY_LABEL("By Label", 0),
        BY_CODE("By Code", 1),
        BY_CATEGORIE("By Categorie", 2),
        BY_DATE("By Date", 3),
        BY_BOOL("By Bool", 4),
        BY_CUSTOM_FIELDS("By Custom fields", 5);

        private Integer code;
        private String key;

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        // Constructeur
        private GroupeTypeTraitemant(String key, Integer code) {
            this.code = code;
            this.key = key;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum MarchandiseDangerousGood {
        NO(1, "NO"), OKPAX(2, "OK PAX"), CAO(3, "CAO"), YES(4, "YES");

        private Integer code;
        private String libelle;

        private MarchandiseDangerousGood(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        public static Map<Integer, String> getListMarchandiseDangerousGood() {
            Map<Integer, String> listMerchandises = new HashMap<>();

            for (MarchandiseDangerousGood mdg: MarchandiseDangerousGood.values()) {
                listMerchandises.put(mdg.code, mdg.libelle);
            }

            return listMerchandises;
        }

        public static String getListMarchandiseDangerousGoodAsJson() throws JsonProcessingException, IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (MarchandiseDangerousGood mdg: MarchandiseDangerousGood.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", mdg.code);
                jsonObj.put("libelle", mdg.libelle);
                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }

        public static MarchandiseDangerousGood getByLibelle(String pLibelle) {

            if (pLibelle != null) {
                String libelle = pLibelle.toLowerCase();

                MarchandiseDangerousGood res = null;

                for (MarchandiseDangerousGood item: MarchandiseDangerousGood.values()) {
                    if (item.libelle.toLowerCase().equals(libelle)) res = item;
                }

                return res;
            }

            return null;
        }

        public static MarchandiseDangerousGood getByCode(Integer pCode) {

            if (pCode != null) {
                Integer code = pCode;

                MarchandiseDangerousGood res = null;

                for (MarchandiseDangerousGood item: MarchandiseDangerousGood.values()) {
                    if (item.code.equals(code)) res = item;
                }

                return res;
            }

            return null;
        }

        public static String toJsonList() throws IOException {
            ObjectMapper mapper = new ObjectMapper();
            ArrayNode arrayNode = mapper.createArrayNode();

            for (MarchandiseDangerousGood item: MarchandiseDangerousGood.values()) {
                ObjectNode jsonObj = mapper.createObjectNode();
                jsonObj.put("code", item.code);
                jsonObj.put("libelle", item.libelle);

                arrayNode.add(jsonObj);
            }

            return arrayNode.toString();
        }

        public static List<MarchandiseDangerousGood> getValues() {
            return Arrays.asList(MarchandiseDangerousGood.values());
        }
    }

    public static double[] MarchandiseClassGood = new double[]{
        1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 2, 2.1, 2.2, 2.3, 3, 4, 4.1, 4.2, 4.3, 5, 5.1, 5.2, 6, 6.1, 6.2, 7, 8, 9
    };

    public static String getListMarchandiseClassGoodAsJSON() throws JsonProcessingException, IOException {
        ObjectMapper mapper = new ObjectMapper();
        ArrayNode arrayNode = mapper.createArrayNode();

        for (double item: MarchandiseClassGood) {
            ObjectNode jsonObj = mapper.createObjectNode();
            jsonObj.put("id", item);
            jsonObj.put("value", item);
            arrayNode.add(jsonObj);
        }

        return arrayNode.toString();
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum DataRecoveryType {
        PSL_SCHEMA(1, "Psl schema"),
        TYPE_REQUEST(2, "Type request"),
        TYPE_UNIT(3, "Type unit"),
        DEMANDE_QUOTE(4, "Demande Quote"),
        TYPE_DOCUMENT(5, "Type document"),
        MASQUE(6, "Masques"),
        TYPE_FLUX(7, "Type Flux");

        private Integer code;
        private String libelle;

        private DataRecoveryType(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getLibelleByCode(Integer code) {

            if (code != null) {
                String dataRecoveryType = null;

                for (DataRecoveryType item: DataRecoveryType.values()) {
                    if (item.code.equals(code)) dataRecoveryType = item.libelle;
                }

                return dataRecoveryType;
            }

            return null;
        }

        public static Integer getCodeByLibelle(String libelle) {

            if (libelle != null) {
                Integer dataRecoveryType = null;
                String value = libelle.toLowerCase();

                for (DataRecoveryType item: DataRecoveryType.values()) {
                    if (item.libelle.toLowerCase().contains(value)) dataRecoveryType = item.code;
                }

                return dataRecoveryType;
            }

            return null;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum DataRecoveryFormat {
        DATA_TYPE("dataType"), COMPAGNIES("compagnies");

        private String libelle;

        private DataRecoveryFormat(String libelle) {
            this.libelle = libelle;
        }

        public String getLibelle() {
            return libelle;
        }
    }

    public enum CustomsDeclarationStatus implements GenericEnum {
        IN_PROGRESS(0, "IN_PROGRESS"), APURE(1, "APURE");

        private Integer code;
        private String key;

        private CustomsDeclarationStatus(Integer code, String key) {
            this.code = code;
            this.key = key;
        }

        @Override
        public Integer getCode() {
            return this.code;
        }

        @Override
        public String getKey() {
            return this.key;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum TmpEventStatus {
        NOT_TREATED(0, "Not treated"), TREATED(1, "Treated"), REJECTED(2, "Rejected");

        private Integer code;
        private String key;

        private TmpEventStatus(Integer code, String key) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return this.code;
        }

        public String getKey() {
            return this.key;
        }
    }

    public static enum TransportConfirmation implements GenericEnum {
        WAITING_FOR_CONFIRMATION("Waiting for confirmation", 0), CONFIRMED("Confirmed", 1);

        private Integer code;
        private String key;

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        // Constructeur
        private TransportConfirmation(String key, Integer code) {
            this.code = code;
            this.key = key;
        }
    }

    public enum YesOrNo implements GenericEnum {
        Yes("Yes", 1), No("No", 0);

        private Integer code;
        private String key;

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        private YesOrNo(String key, Integer code) {
            this.code = code;
            this.key = key;
        }
    }

    public enum TrackingLevel implements GenericEnum {
        TRANSPORT("TRANSPORT", 0),
        CONTAINER("CONTAINER", 1),
        PALETTE("PALETTE", 2),
        COLIS("COLIS", 3),
        ARTICLE("ARTICLE", 4),
        UNITS("UNITS", 5),
        UNIT_WITHOUT_PARENT("UNIT_WITHOUT_PARENT", 6);

        private Integer code;
        private String key;

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        private TrackingLevel(String key, Integer code) {
            this.code = code;
            this.key = key;
        }
    }

    public enum TypeFluxCPB {
        FLUX_EXPORT("Export", 1), VENTE_DIRECT("Direct Sales", 2);

        private Integer code;
        private String libelle;

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static String getLibelleByCode(Integer code) {

            for (TypeFluxCPB item: TypeFluxCPB.values()) {

                if (item.code != null && item.code.equals(code)) {
                    return item.libelle;
                }

            }

            return "";
        }

        private TypeFluxCPB(String libelle, Integer code) {
            this.code = code;
            this.libelle = libelle;
        }
    }

    public enum ACLRuleType implements GenericEnum {
        BOOLEAN("BOOLEAN", 0), LIST("LIST", 1);

        private Integer code;
        private String key;

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        // Constructeur
        private ACLRuleType(String key, Integer code) {
            this.code = code;
            this.key = key;
        }
    }

    public enum FileDownLoadStatus implements GenericEnum {
        DEBUT("Debut", 0),
        ENCOURS("En cours", 1),
        FIN("Fin", 2),
        ERROR("Error", 3),
        LIMITE_DEPASSE("Limite depassé", 4);

        private Integer code;
        private String key;

        private FileDownLoadStatus(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }
    }

    public enum LanguageUser implements GenericEnum {
        FRENCH("fr", 0), ENGLISH("en", 1);

        private Integer code;
        private String key;

        private LanguageUser(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum TransfertActionChoice {
        NONE("ACTION_NONE", 1), ALL("ACTION_ALL", 2), CHOOSE("ACTION_CHOOSE", 3);

        private Integer code;
        private String libelle;

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(String libelle) {
            this.libelle = libelle;
        }

        // Constructeur
        private TransfertActionChoice(String libelle, Integer code) {
            this.code = code;
            this.libelle = libelle;
        }

        public static List<TransfertActionChoice> getListAction() throws JsonProcessingException {
            List<TransfertActionChoice> listActionChoose = new ArrayList<TransfertActionChoice>();
            listActionChoose.add(TransfertActionChoice.NONE);
            listActionChoose.add(TransfertActionChoice.ALL);
            listActionChoose.add(TransfertActionChoice.CHOOSE);
            return listActionChoose;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public static enum ResultEntityType implements GenericEnum {
        SUCCESS(1, "Success"), ERROR(2, "Error");

        Integer code;
        String key;

        private ResultEntityType(Integer code, String key) {
            this.code = code;
            this.key = key;
        }

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum TransortPriorityEnum {
        CRI(1, "CRI"), AOG(2, "AOG"), RTN(3, "RTN"), SCH(4, "SCH"), RPL(5, "RPL"), AOD(6, "AOD"), MOD(7, "MOD");

        private Integer code;
        private String libelle;

        private TransortPriorityEnum(Integer code, String libelle) {
            this.code = code;
            this.libelle = libelle;
        }

        public Integer getCode() {
            return code;
        }

        public String getLibelle() {
            return libelle;
        }

        public static Integer getCodeByLibelle(String libelleStr) {

            if (libelleStr != null) {
                if (libelleStr.equalsIgnoreCase(RTN_LIB)) libelleStr = RTN.libelle;
                else if (libelleStr.equalsIgnoreCase(CRI_LIB)) libelleStr = CRI.libelle;

                String libelle = libelleStr.toLowerCase();
                Integer transportPrId = null;

                for (TransortPriorityEnum item: TransortPriorityEnum.values()) {
                    if (libelle.contains(item.libelle.toLowerCase())) transportPrId = item.code;
                }

                return transportPrId;
            }

            return null;
        }

        public static String getByCode(Integer code) {

            if (code != null) {

                for (TransortPriorityEnum item: TransortPriorityEnum.values()) {
                    if (item.code.equals(code)) return item.libelle;
                }

            }

            return null;
        }

        public static List<TransortPriorityEnum> getListTransportPriority() {
            List<TransortPriorityEnum> listTransportPriority = new ArrayList<>();
            listTransportPriority.add(TransortPriorityEnum.CRI);
            listTransportPriority.add(TransortPriorityEnum.AOG);
            listTransportPriority.add(TransortPriorityEnum.RTN);
            listTransportPriority.add(TransortPriorityEnum.SCH);
            listTransportPriority.add(TransortPriorityEnum.RPL);
            listTransportPriority.add(TransortPriorityEnum.AOD);
            listTransportPriority.add(TransortPriorityEnum.MOD);

            return listTransportPriority;
        }
    }
}
