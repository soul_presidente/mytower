/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "ec_mime_type", schema = "work")
public class EcMimetype implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ec_mime_type_num")
    private Integer ecMimetypeNum;

    @Column(name = "mimetype")
    private String mimetype;

    @Column(name = "description")
    private String description;

    public Integer getEcMimetypeNum() {
        return ecMimetypeNum;
    }

    public void setEcMimetypeNum(Integer ecMimetypeNum) {
        this.ecMimetypeNum = ecMimetypeNum;
    }

    public String getMimetype() {
        return mimetype;
    }

    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
