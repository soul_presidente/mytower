/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.trpl.model.EbPlCostItem;
import com.adias.mytowereasy.types.CustomObjectIdResolver;
import com.adias.mytowereasy.types.JsonBinaryType;
import com.adias.mytowereasy.util.JsonDateDeserializer;
import com.adias.mytowereasy.util.JsonUtils;


@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "ebInvoiceNum",
    resolver = CustomObjectIdResolver.class,
    scope = EbInvoice.class)
@Entity
@JsonIgnoreProperties({
    "hibernateLazyInitializer", "handler"
})
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Table
public class EbInvoice {
    @Id
    @Column(nullable = false, unique = true)
    private Integer ebInvoiceNum;

    @Column(updatable = false)
    private String ref;

    private Integer statut;

    @Column(updatable = false)
    private Integer xEcTypeDemande;

    @Column(updatable = false)
    private String typeRequestLibelle;

    @Column(updatable = false)
    private Integer xEcModeTransport;

    @Column(updatable = false)
    private Integer xEbTypeTransport;

    @Column(updatable = false)
    private String refDemande;

    @Column(updatable = false)
    private Boolean insurance;

    @Column(updatable = false)
    private Double insuranceValue;

    @Column(updatable = false)
    private String libelleOriginCountry;

    @Column(updatable = false)
    private String libelleOriginCity;

    @Column(updatable = false)
    private String libelleDestCountry;

    @Column(updatable = false)
    private String libelleDestCity;

    @Column(updatable = false)
    private Double totalNbrParcel;

    @Column(updatable = false)
    private Double totalVolume;

    @Column(updatable = false)
    private Double totalWeight;

    @Column(updatable = false)
    private Double totalTaxableWeight;

    @Column(updatable = false)
    private BigDecimal preCarrierCost;

    private Double finalCarrierCost;

    private Double gap;

    @Column(updatable = false)
    private Date datePickup;

    @Column(updatable = false)
    private Integer transitTime;

    @Column(updatable = false)
    private Date dateDelivery;

    @Column(updatable = false)
    private Integer xEbDemandeNum;

    @Column(updatable = false)
    private Date dateRequest;

    @Column(updatable = false)
    private String listLabelStr;

    @Column(updatable = false)
    private String listCategorieStr;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_carrier", insertable = true, updatable = false)
    private EbUser carrier;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_etablissement_carrier", insertable = true, updatable = false)
    private EbEtablissement xEbEtablissementCarrier;

    @Column(updatable = false)
    private String nomEtablissementCarrier;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_company_carrier", insertable = true, updatable = false)
    private EbCompagnie xEbCompagnieCarrier;

    @Column(updatable = false)
    private String nomCompanyCarrier;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_chargeur", insertable = true, updatable = false)
    private EbUser chargeur;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_etablissement_chargeur", insertable = true, updatable = false)
    private EbEtablissement xEbEtablissementChargeur;

    @Column(updatable = false)
    private String nomEtablissementChargeur;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_company_chargeur", insertable = true, updatable = false)
    private EbCompagnie xEbCompagnieChargeur;

    @Column(updatable = false)
    private String nomCompanyChargeur;

    @Column(updatable = false)
    private Date dateAjout;

    private Date dateModification;

    @Column(updatable = false)
    private String memo;

    @Column(updatable = false)
    private Integer flag;

    private Integer xEcStatut;

    private Double price;

    private String memoCarrier;

    private String memoChageur;

    private Integer flagCarrier;

    private BigDecimal applyChargeurIvoicePrice;

    private BigDecimal applyCarrierIvoicePrice;

    private String applyChargeurIvoiceNumber;

    private String applyCarrierIvoiceNumber;

    private Integer uploadFileNbr;

    private Integer invoiceMonth;

    private Integer invoiceYear;

    private Integer invoiceMonthCarrier;

    private Integer invoiceYearCarrier;

    private Integer numberArraw;

    private Integer priceArraw;

    private Integer dateArraw;

    private Integer statutChargeur;

    private Integer statutCarrier;

    private Date invoiceDateChargeur;

    private Date invoiceDateCarrier;

    private BigDecimal gapChargeur;

    private BigDecimal gapCarrier;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_demande", insertable = true, updatable = false)
    private EbDemande xEbDemande;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_ro")
    private EbUser xEbUserOwnerRequest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie_ro")
    private EbCompagnie xEbCompagnieOwnerRequest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement_ro")
    private EbEtablissement xEbEtablissementOwnerRequest;

    private String numAwbBol;

    // champs reservé pour customFields
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbCategorie> listCategories;

    @Column(updatable = false)
    private String xEcIncotermLibelle;

    private Integer xEbTypeRequest;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCostCategorie> listCostCategorieCharger;

    private String listFlag;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbFlagDTO> listEbFlagDTO;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCostCategorie> listCostCategorieTransporteur;

    @ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_currency")
    private EcCurrency currency;

    private String currencyLabel;

    private String commentInvoiceCarrier;

    private String commentInvoiceCharger;

    private String commentGapCarrier;

    private String commentGapCharger;

    private Integer statutArraw;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbTypeDocuments> listTypeDocuments;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<EbPlCostItem> listPlCostItem;

    public List<EbPlCostItem> getListPlCostItem() {
        return listPlCostItem;
    }

    public void setListPlCostItem(List<EbPlCostItem> listPlCostItem) {
        this.listPlCostItem = listPlCostItem;
    }

    public List<EbTypeDocuments> getListTypeDocuments() {
        return listTypeDocuments;
    }

    public void setListTypeDocuments(List<EbTypeDocuments> listTypeDocuments) {
        this.listTypeDocuments = listTypeDocuments;
    }

    public EbInvoice() {
    }

    public EbInvoice(Integer ebInvoiceNum, Object listTypeDocuments) {
        this.ebInvoiceNum = ebInvoiceNum;

        if (listTypeDocuments != null) {

            try {
                ObjectMapper objectMapper = new ObjectMapper();

                this.listTypeDocuments = objectMapper
                    .convertValue(listTypeDocuments, new TypeReference<ArrayList<EbTypeDocuments>>() {
                    });
            } catch (Exception e) {
                System.err.println("ConnectedUser: failed to convert object to listTypeDocument in EbInvoice.");
            }

        }

    }

    public String getListFlag() {
        return listFlag;
    }

    public void setListFlag(String listFlag) {
        this.listFlag = listFlag;
    }

    public List<EbFlagDTO> getListEbFlagDTO() {
        return listEbFlagDTO;
    }

    public void setListEbFlagDTO(List<EbFlagDTO> listEbFlagDTO) {
        this.listEbFlagDTO = listEbFlagDTO;
    }

    public EbInvoice(EbDemande ebDemande) {
        super();
        this.xEbDemande = new EbDemande(ebDemande.getEbDemandeNum());

        if (ebDemande.getUser() != null
            && ebDemande.getUser().getEbUserNum() != null) this.xEbUserOwnerRequest = new EbUser(
                ebDemande.getUser().getEbUserNum());

        if (ebDemande.getxEbUserCt() != null && ebDemande.getxEbUserCt().getEbUserNum() != null) {
            this.xEbCompagnieOwnerRequest = ebDemande.getxEbCompagnie();
            this.xEbEtablissementOwnerRequest = ebDemande.getxEbEtablissement();
        }

        this.xEbTypeRequest = ebDemande.getxEbTypeRequest();
        this.typeRequestLibelle = ebDemande.getTypeRequestLibelle();
        this.xEcModeTransport = ebDemande.getxEcModeTransport();
        this.xEbTypeTransport = ebDemande.getxEbTypeTransport();
        this.refDemande = ebDemande.getRefTransport();
        this.insurance = ebDemande.getInsurance();
        this.insuranceValue = ebDemande.getInsuranceValue();
        this.libelleOriginCountry = ebDemande.getEbPartyOrigin().getxEcCountry().getLibelle();
        this.libelleOriginCity = ebDemande.getEbPartyOrigin().getCity();
        this.libelleDestCountry = ebDemande.getEbPartyDest().getxEcCountry().getLibelle();
        this.libelleDestCity = ebDemande.getEbPartyDest().getCity();
        this.totalNbrParcel = ebDemande.getTotalNbrParcel();
        this.totalVolume = ebDemande.getTotalVolume();
        this.totalWeight = ebDemande.getTotalWeight();
        this.totalTaxableWeight = ebDemande.getTotalTaxableWeight();
        this.preCarrierCost = ebDemande.get_exEbDemandeTransporteurFinal().getPrice();
        this.datePickup = ebDemande.get_exEbDemandeTransporteurFinal().getPickupTime();
        this.transitTime = ebDemande.get_exEbDemandeTransporteurFinal().getTransitTime();
        this.dateDelivery = ebDemande.get_exEbDemandeTransporteurFinal().getDeliveryTime();
        this.xEbDemandeNum = ebDemande.getEbDemandeNum();
        this.dateRequest = ebDemande.getDateCreation();
        this.listLabelStr = ebDemande.getListLabels();
        this.listCategorieStr = ebDemande.getListCategoriesStr();
        this.carrier = new EbUser(ebDemande.get_exEbDemandeTransporteurFinal().getxTransporteur().getEbUserNum());
        this.xEbEtablissementCarrier = new EbEtablissement(
            ebDemande.get_exEbDemandeTransporteurFinal().getxEbEtablissement().getEbEtablissementNum());
        this.nomEtablissementCarrier = ebDemande.get_exEbDemandeTransporteurFinal().getxEbEtablissement().getNom();
        this.chargeur = new EbUser(ebDemande.getUser().getEbUserNum());
        this.xEbEtablissementChargeur = new EbEtablissement(
            ebDemande.getUser().getEbEtablissement().getEbEtablissementNum());
        this.nomEtablissementChargeur = ebDemande.getUser().getEbEtablissement().getNom();

        if (ebDemande.get_exEbDemandeTransporteurFinal().getxEbCompagnie() != null) {
            this.xEbCompagnieCarrier = new EbCompagnie(
                ebDemande.get_exEbDemandeTransporteurFinal().getxEbCompagnie().getEbCompagnieNum());

            if (ebDemande.get_exEbDemandeTransporteurFinal().getxEbCompagnie().getNom() != null) {
                this.nomCompanyCarrier = ebDemande.get_exEbDemandeTransporteurFinal().getxEbCompagnie().getNom();
            }
            else {
                this.nomCompanyCarrier = ebDemande.get_exEbDemandeTransporteurFinal().getxEbEtablissement().getNom();
            }

        }
        else {
            this.xEbCompagnieCarrier = new EbCompagnie(
                ebDemande
                    .get_exEbDemandeTransporteurFinal().getxEbEtablissement().getEbCompagnie().getEbCompagnieNum());

            if (ebDemande.get_exEbDemandeTransporteurFinal().getxEbEtablissement().getEbCompagnie().getNom() != null) {
                this.nomCompanyCarrier = ebDemande
                    .get_exEbDemandeTransporteurFinal().getxEbEtablissement().getEbCompagnie().getNom();
            }
            else {
                this.nomCompanyCarrier = ebDemande.get_exEbDemandeTransporteurFinal().getxEbEtablissement().getNom();
            }

        }

        if (ebDemande.getUser().getEbCompagnie() != null) {
            this.xEbCompagnieChargeur = new EbCompagnie(ebDemande.getUser().getEbCompagnie().getEbCompagnieNum());
            this.nomCompanyChargeur = ebDemande.getUser().getEbCompagnie().getNom();
        }

        this.xEcIncotermLibelle = ebDemande.getxEcIncotermLibelle();
        this.listCostCategorieCharger = ebDemande.get_exEbDemandeTransporteurFinal().getListCostCategorie();
        this.listCostCategorieTransporteur = ebDemande.get_exEbDemandeTransporteurFinal().getListCostCategorie();
        this.currency = new EcCurrency(ebDemande.getXecCurrencyInvoice().getEcCurrencyNum());
        this.numAwbBol = ebDemande.getNumAwbBol();
    }

    @QueryProjection
    public EbInvoice(
        Integer ebInvoiceNum,
        String ref,
        Integer statut,
        Integer xEcTypeDemande,
        Integer xEcModeTransport,
        Integer xEbTypeTransport,
        String typeRequestLibelle,
        String refDemande,
        Boolean insurance,
        Double insuranceValue,
        String libelleOriginCountry,
        String libelleOriginCity,
        String libelleDestCountry,
        String libelleDestCity,
        Double totalNbrParcel,
        Double totalVolume,
        Double totalWeight,
        Double totalTaxableWeight,
        BigDecimal preCarrierCost,
        Double finalCarrierCost,
        Date datePickup,
        Integer transitTime,
        Date dateDelivery,
        Integer xEbDemandeNum,
        Date dateRequest,
        String listLabelStr,
        String listCategorieStr,
        String nomEtablissementCarrier,
        String nomCompanyCarrier,
        String nomEtablissementChargeur,
        String nomCompanyChargeur,
        Date dateAjout,
        Date dateModification,
        String memoCarrier,
        String memoChageur,
        Integer flagCarrier,
        BigDecimal applyChargeurIvoicePrice,
        BigDecimal applyCarrierIvoicePrice,
        String applyChargeurIvoiceNumber,
        String applyCarrierIvoiceNumber,
        Integer uploadFileNbr,
        Integer invoiceMonth,
        Integer invoiceYear,
        Integer invoiceMonthCarrier,
        Integer invoiceYearCarrier,
        Integer flag,
        Integer carrierEbUserNum,
        String carrierNom,
        String carrierPrenom,
        Integer chargeurEbUserNum,
        String chargeurNom,
        String chargeurPrenom,
        Integer numberArraw,
        Integer priceArraw,
        Integer dateArraw,
        Integer statutChargeur,
        Integer statutCarrier,
        Date invoiceDateChargeur,
        Date invoiceDateCarrier,
        BigDecimal gapChargeur,
        BigDecimal gapCarrier,
        Integer ebDemandeNum,
        String customFields,
        List<EbCategorie> listCategories,
        String xEcIncotermLibelle,
        Integer xEbTypeRequest,
        String numAwbBol,
        EbTtSchemaPsl xEbSchemaPsl,
        Integer compagnieNum,
        String nomCompagnie,
        List<EbCostCategorie> listCostCategorieCharger,
        List<EbCostCategorie> listCostCategorieTransporteur,
        String listFlag,
        String currencyLabel,
        String commentInvoiceCarrier,
        String commentInvoiceCharger,
        String commentGapCarrier,
        String commentGapCharger,
        Integer statutArraw,
        Object listTypeDoc,
        String numAwbBolInvoice,
        List<EbPlCostItem> listPlCostItem,
        Integer numOwnerRequest,
        String nomOwnerRequest,
        String prenomOwnerRequest) {
        this.ebInvoiceNum = ebInvoiceNum;
        this.ref = ref;
        this.statut = statut;
        this.xEcTypeDemande = xEcTypeDemande;
        this.xEcModeTransport = xEcModeTransport;
        this.xEbTypeTransport = xEbTypeTransport;
        this.xEcIncotermLibelle = xEcIncotermLibelle;
        this.xEbTypeRequest = xEbTypeRequest;
        this.typeRequestLibelle = typeRequestLibelle;
        this.refDemande = refDemande;
        this.insurance = insurance;
        this.insuranceValue = insuranceValue;
        this.libelleOriginCountry = libelleOriginCountry;
        this.libelleOriginCity = libelleOriginCity;
        this.libelleDestCountry = libelleDestCountry;
        this.libelleDestCity = libelleDestCity;
        this.totalNbrParcel = totalNbrParcel;
        this.totalVolume = totalVolume;
        this.totalWeight = totalWeight;
        this.totalTaxableWeight = totalTaxableWeight;
        this.preCarrierCost = preCarrierCost;
        this.finalCarrierCost = finalCarrierCost;
        this.datePickup = datePickup;
        this.transitTime = transitTime;
        this.dateDelivery = dateDelivery;
        this.xEbDemandeNum = xEbDemandeNum;
        this.dateRequest = dateRequest;
        this.listLabelStr = listLabelStr;
        this.listCategorieStr = listCategorieStr;
        this.nomEtablissementCarrier = nomEtablissementCarrier;
        this.nomCompanyCarrier = nomCompanyCarrier;
        this.nomEtablissementChargeur = nomEtablissementChargeur;
        this.nomCompanyChargeur = nomCompanyChargeur;
        this.dateAjout = dateAjout;
        this.dateModification = dateModification;
        this.memoCarrier = memoCarrier;
        this.memoChageur = memoChageur;
        this.flagCarrier = flagCarrier;
        this.uploadFileNbr = uploadFileNbr;
        this.applyChargeurIvoicePrice = applyChargeurIvoicePrice;
        this.applyCarrierIvoicePrice = applyCarrierIvoicePrice;
        this.applyChargeurIvoiceNumber = applyChargeurIvoiceNumber;
        this.applyCarrierIvoiceNumber = applyCarrierIvoiceNumber;
        this.invoiceMonth = invoiceMonth;
        this.invoiceYear = invoiceYear;
        this.invoiceMonthCarrier = invoiceMonthCarrier;
        this.invoiceYearCarrier = invoiceYearCarrier;
        this.flag = flag;
        this.carrier = new EbUser(carrierEbUserNum, carrierNom, carrierPrenom);
        this.chargeur = new EbUser(chargeurEbUserNum, chargeurNom, chargeurPrenom);
        this.chargeur.setEbCompagnie(new EbCompagnie(compagnieNum, nomCompagnie, null));
        this.numberArraw = numberArraw;
        this.priceArraw = priceArraw;
        this.dateArraw = dateArraw;
        this.statutChargeur = statutChargeur;
        this.statutCarrier = statutCarrier;
        this.invoiceDateChargeur = invoiceDateChargeur;
        this.invoiceDateCarrier = invoiceDateCarrier;
        this.gapChargeur = gapChargeur;
        this.gapCarrier = gapCarrier;
        this.xEbDemande = new EbDemande(ebDemandeNum);
        this.xEbDemande.setCustomFields(customFields);
        this.listPlCostItem = listPlCostItem;
        this.xEbUserOwnerRequest = new EbUser(numOwnerRequest, nomOwnerRequest, prenomOwnerRequest);
        ObjectMapper objectMapper = JsonUtils.getObjectMapperInstance();

        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new JsonDateDeserializer()).create();

        if (customFields != null) {
            this.listCustomsFields = gson.fromJson(customFields, new TypeToken<ArrayList<CustomFields>>() {
            }.getType());
        }

        if (listCategories != null && listCategories.size() > 0) {
            String jsonCateg = gson.toJson(listCategories);
            this.listCategories = gson.fromJson(jsonCateg, new TypeToken<ArrayList<EbCategorie>>() {
            }.getType());
        }

        this.xEbDemande.setListCategories(this.listCategories);

        this.xEbDemande.setNumAwbBol(numAwbBol);
        this.listCostCategorieCharger = listCostCategorieCharger;
        this.listCostCategorieTransporteur = listCostCategorieTransporteur;
        this.listFlag = listFlag;
        this.currencyLabel = currencyLabel;
        this.commentInvoiceCarrier = commentInvoiceCarrier;
        this.commentInvoiceCharger = commentInvoiceCharger;
        this.commentGapCarrier = commentGapCarrier;
        this.commentGapCharger = commentGapCharger;
        this.statutArraw = statutArraw;

        // Date Pickup && Delivery
        if (xEbSchemaPsl != null) {
            List<EbTtCompanyPsl> listPsl = xEbSchemaPsl.getListPsl();

            if (listPsl != null && !listPsl.isEmpty()) {

                for (EbTtCompanyPsl psl: listPsl) {

                    // TODO psl.getExpectedDate() is to be make sure of
                    if (psl.getStartPsl() != null && psl.getStartPsl() && psl.getExpectedDate() != null) {
                        this.datePickup = psl.getExpectedDate();
                    }
                    else if (psl.getEndPsl() != null && psl.getEndPsl() && psl.getExpectedDate() != null) {
                        this.dateDelivery = psl.getExpectedDate();
                    }

                }

            }

        }

        if (listTypeDoc != null) {

            try {
                // ObjectMapper objectMapper = new ObjectMapper();
                String json = objectMapper.writeValueAsString(listTypeDoc);
                this.listTypeDocuments = objectMapper.readValue(json, new TypeReference<ArrayList<EbTypeDocuments>>() {
                });
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        this.numAwbBol = numAwbBolInvoice;
    }

    public Integer getEbInvoiceNum() {
        return ebInvoiceNum;
    }

    public void setEbInvoiceNum(Integer ebInvoiceNum) {
        this.ebInvoiceNum = ebInvoiceNum;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Integer getStatut() {
        return statut;
    }

    public void setStatut(Integer statut) {
        this.statut = statut;
    }

    public Integer getxEcTypeDemande() {
        return xEcTypeDemande;
    }

    public void setxEcTypeDemande(Integer xEcTypeDemande) {
        this.xEcTypeDemande = xEcTypeDemande;
    }

    public Integer getxEcModeTransport() {
        return xEcModeTransport;
    }

    public void setxEcModeTransport(Integer xEcModeTransport) {
        this.xEcModeTransport = xEcModeTransport;
    }

    public Integer getxEbTypeTransport() {
        return xEbTypeTransport;
    }

    public void setxEbTypeTransport(Integer xEbTypeTransport) {
        this.xEbTypeTransport = xEbTypeTransport;
    }

    public String getRefDemande() {
        return refDemande;
    }

    public void setRefDemande(String refDemande) {
        this.refDemande = refDemande;
    }

    public Boolean getInsurance() {
        return insurance;
    }

    public void setInsurance(Boolean insurance) {
        this.insurance = insurance;
    }

    public Double getInsuranceValue() {
        return insuranceValue;
    }

    public void setInsuranceValue(Double insuranceValue) {
        this.insuranceValue = insuranceValue;
    }

    public String getLibelleOriginCountry() {
        return libelleOriginCountry;
    }

    public void setLibelleOriginCountry(String libelleOriginCountry) {
        this.libelleOriginCountry = libelleOriginCountry;
    }

    public String getLibelleOriginCity() {
        return libelleOriginCity;
    }

    public void setLibelleOriginCity(String libelleOriginCity) {
        this.libelleOriginCity = libelleOriginCity;
    }

    public String getLibelleDestCountry() {
        return libelleDestCountry;
    }

    public void setLibelleDestCountry(String libelleDestCountry) {
        this.libelleDestCountry = libelleDestCountry;
    }

    public String getLibelleDestCity() {
        return libelleDestCity;
    }

    public void setLibelleDestCity(String libelleDestCity) {
        this.libelleDestCity = libelleDestCity;
    }

    public Double getTotalNbrParcel() {
        return totalNbrParcel;
    }

    public void setTotalNbrParcel(Double totalNbrParcel) {
        this.totalNbrParcel = totalNbrParcel;
    }

    public Double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(Double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Double getTotalTaxableWeight() {
        return totalTaxableWeight;
    }

    public void setTotalTaxableWeight(Double totalTaxableWeight) {
        this.totalTaxableWeight = totalTaxableWeight;
    }

    public BigDecimal getPreCarrierCost() {
        return preCarrierCost;
    }

    public void setPreCarrierCost(BigDecimal preCarrierCost) {
        this.preCarrierCost = preCarrierCost;
    }

    public Double getFinalCarrierCost() {
        return finalCarrierCost;
    }

    public void setFinalCarrierCost(Double finalCarrierCost) {
        this.finalCarrierCost = finalCarrierCost;
    }

    public Date getDatePickup() {
        return datePickup;
    }

    public void setDatePickup(Date datePickup) {
        this.datePickup = datePickup;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public Date getDateDelivery() {
        return dateDelivery;
    }

    public void setDateDelivery(Date dateDelivery) {
        this.dateDelivery = dateDelivery;
    }

    public EbUser getCarrier() {
        return carrier;
    }

    public void setCarrier(EbUser carrier) {
        this.carrier = carrier;
    }

    public EbUser getChargeur() {
        return chargeur;
    }

    public void setChageur(EbUser chargeur) {
        this.chargeur = chargeur;
    }

    public Integer getxEbDemandeNum() {
        return xEbDemandeNum;
    }

    public void setxEbDemandeNum(Integer xEbDemandeNum) {
        this.xEbDemandeNum = xEbDemandeNum;
    }

    public String getListLabelStr() {
        return listLabelStr;
    }

    public void setListLabelStr(String listLabelStr) {
        this.listLabelStr = listLabelStr;
    }

    public String getListCategorieStr() {
        return listCategorieStr;
    }

    public void setListCategorieStr(String listCategorieStr) {
        this.listCategorieStr = listCategorieStr;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public EbEtablissement getxEbEtablissementCarrier() {
        if (xEbEtablissementCarrier != null) xEbEtablissementCarrier.setEbCompagnie(null);
        return xEbEtablissementCarrier;
    }

    public void setxEbEtablissementCarrier(EbEtablissement xEbEtablissementCarrier) {
        this.xEbEtablissementCarrier = xEbEtablissementCarrier;
    }

    public String getNomEtablissementCarrier() {
        return nomEtablissementCarrier;
    }

    public void setNomEtablissementCarrier(String nomEtablissementCarrier) {
        this.nomEtablissementCarrier = nomEtablissementCarrier;
    }

    public EbCompagnie getxEbCompagnieCarrier() {
        return xEbCompagnieCarrier;
    }

    public void setxEbCompagnieCarrier(EbCompagnie xEbCompagnieCarrier) {
        this.xEbCompagnieCarrier = xEbCompagnieCarrier;
    }

    public String getNomCompanyCarrier() {
        return nomCompanyCarrier;
    }

    public void setNomCompanyCarrier(String nomCompanyCarrier) {
        this.nomCompanyCarrier = nomCompanyCarrier;
    }

    public EbEtablissement getxEbEtablissementChargeur() {
        if (xEbEtablissementChargeur != null) xEbEtablissementChargeur.setEbCompagnie(null);
        return xEbEtablissementChargeur;
    }

    public void setxEbEtablissementChargeur(EbEtablissement xEbEtablissementChargeur) {
        this.xEbEtablissementChargeur = xEbEtablissementChargeur;
    }

    public String getNomEtablissementChargeur() {
        return nomEtablissementChargeur;
    }

    public void setNomEtablissementChargeur(String nomEtablissementChargeur) {
        this.nomEtablissementChargeur = nomEtablissementChargeur;
    }

    public EbCompagnie getxEbCompagnieChargeur() {
        return xEbCompagnieChargeur;
    }

    public void setxEbCompagnieChargeur(EbCompagnie xEbCompagnieChargeur) {
        this.xEbCompagnieChargeur = xEbCompagnieChargeur;
    }

    public String getNomCompanyChargeur() {
        return nomCompanyChargeur;
    }

    public void setNomCompanyChargeur(String nomCompanyChargeur) {
        this.nomCompanyChargeur = nomCompanyChargeur;
    }

    public Date getDateRequest() {
        return dateRequest;
    }

    public void setDateRequest(Date dateRequest) {
        this.dateRequest = dateRequest;
    }

    public Double getGap() {
        return gap;
    }

    public void setGap(Double gap) {
        this.gap = gap;
    }

    @PrePersist
    void prePersist() {
        this.dateAjout = new Date();
    }

    @PreUpdate
    void preUpdate() {
        this.dateModification = new Date();

        if (this.finalCarrierCost != null && this.preCarrierCost != null) {
            // this.gap = this.preCarrierCost - this.finalCarrierCost;
        }

    }

    public String getMemoCarrier() {
        return memoCarrier;
    }

    public void setMemoCarrier(String memoCarrier) {
        this.memoCarrier = memoCarrier;
    }

    public String getMemoChageur() {
        return memoChageur;
    }

    public void setMemoChageur(String memoChageur) {
        this.memoChageur = memoChageur;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getxEcStatut() {
        return xEcStatut;
    }

    public void setxEcStatut(Integer xEcStatut) {
        this.xEcStatut = xEcStatut;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getFlagCarrier() {
        return flagCarrier;
    }

    public void setFlagCarrier(Integer flagCarrier) {
        this.flagCarrier = flagCarrier;
    }

    public Integer getUploadFileNbr() {
        return this.uploadFileNbr;
    }

    public void setUploadFileNbr(Integer uploadFileNbr) {
        this.uploadFileNbr = uploadFileNbr;
    }

    public BigDecimal getApplyChargeurIvoicePrice() {
        return applyChargeurIvoicePrice;
    }

    public void setApplyChargeurIvoicePrice(BigDecimal applyChargeurIvoicePrice) {
        this.applyChargeurIvoicePrice = applyChargeurIvoicePrice;
    }

    public BigDecimal getApplyCarrierIvoicePrice() {
        return applyCarrierIvoicePrice;
    }

    public void setApplyCarrierIvoicePrice(BigDecimal applyCarrierIvoicePrice) {
        this.applyCarrierIvoicePrice = applyCarrierIvoicePrice;
    }

    public String getApplyChargeurIvoiceNumber() {
        return applyChargeurIvoiceNumber;
    }

    public void setApplyChargeurIvoiceNumber(String applyChargeurIvoiceNumber) {
        this.applyChargeurIvoiceNumber = applyChargeurIvoiceNumber;
    }

    public String getApplyCarrierIvoiceNumber() {
        return applyCarrierIvoiceNumber;
    }

    public void setApplyCarrierIvoiceNumber(String applyCarrierIvoiceNumber) {
        this.applyCarrierIvoiceNumber = applyCarrierIvoiceNumber;
    }

    public Integer getInvoiceMonth() {
        return invoiceMonth;
    }

    public void setInvoiceMonth(Integer invoiceMonth) {
        this.invoiceMonth = invoiceMonth;
    }

    public Integer getInvoiceYear() {
        return invoiceYear;
    }

    public void setInvoiceYear(Integer invoiceYear) {
        this.invoiceYear = invoiceYear;
    }

    public Integer getInvoiceMonthCarrier() {
        return invoiceMonthCarrier;
    }

    public void setInvoiceMonthCarrier(Integer invoiceMonthCarrier) {
        this.invoiceMonthCarrier = invoiceMonthCarrier;
    }

    public Integer getInvoiceYearCarrier() {
        return invoiceYearCarrier;
    }

    public void setInvoiceYearCarrier(Integer invoiceYearCarrier) {
        this.invoiceYearCarrier = invoiceYearCarrier;
    }

    public void setChargeur(EbUser chargeur) {
        this.chargeur = chargeur;
    }

    public Integer getNumberArraw() {
        return numberArraw;
    }

    public void setNumberArraw(Integer numberArraw) {
        this.numberArraw = numberArraw;
    }

    public Integer getPriceArraw() {
        return priceArraw;
    }

    public void setPriceArraw(Integer priceArraw) {
        this.priceArraw = priceArraw;
    }

    public Integer getDateArraw() {
        return dateArraw;
    }

    public void setDateArraw(Integer dateArraw) {
        this.dateArraw = dateArraw;
    }

    public EbDemande getxEbDemande() {
        return xEbDemande;
    }

    public void setxEbDemande(EbDemande xEbDemande) {
        this.xEbDemande = xEbDemande;
    }

    public Integer getStatutChargeur() {
        return statutChargeur;
    }

    public void setStatutChargeur(Integer statutChargeur) {
        this.statutChargeur = statutChargeur;
    }

    public Integer getStatutCarrier() {
        return statutCarrier;
    }

    public void setStatutCarrier(Integer statutCarrier) {
        this.statutCarrier = statutCarrier;
    }

    public Date getInvoiceDateChargeur() {
        return invoiceDateChargeur;
    }

    public void setInvoiceDateChargeur(Date invoiceDateChargeur) {
        this.invoiceDateChargeur = invoiceDateChargeur;
    }

    public Date getInvoiceDateCarrier() {
        return invoiceDateCarrier;
    }

    public void setInvoiceDateCarrier(Date invoiceDateCarrier) {
        this.invoiceDateCarrier = invoiceDateCarrier;
    }

    public BigDecimal getGapChargeur() {
        return gapChargeur;
    }

    public void setGapChargeur(BigDecimal gapChargeur) {
        this.gapChargeur = gapChargeur;
    }

    public BigDecimal getGapCarrier() {
        return gapCarrier;
    }

    public void setGapCarrier(BigDecimal gapCarrier) {
        this.gapCarrier = gapCarrier;
    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public List<EbCategorie> getListCategories() {
        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {
        this.listCategories = listCategories;
    }

    public List<EbCostCategorie> getListCostCategorieCharger() {
        return listCostCategorieCharger;
    }

    public void setListCostCategorieCharger(List<EbCostCategorie> listCostCategorieCharger) {
        this.listCostCategorieCharger = listCostCategorieCharger;
    }

    public List<EbCostCategorie> getListCostCategorieTransporteur() {
        return listCostCategorieTransporteur;
    }

    public void setListCostCategorieTransporteur(List<EbCostCategorie> listCostCategorieTransporteur) {
        this.listCostCategorieTransporteur = listCostCategorieTransporteur;
    }

    public EcCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(EcCurrency currency) {
        this.currency = currency;
    }

    public String getCurrencyLabel() {
        return currencyLabel;
    }

    public void setCurrencyLabel(String currencyLabel) {
        this.currencyLabel = currencyLabel;
    }

    public String getCommentInvoiceCarrier() {
        return commentInvoiceCarrier;
    }

    public void setCommentInvoiceCarrier(String commentInvoiceCarrier) {
        this.commentInvoiceCarrier = commentInvoiceCarrier;
    }

    public String getCommentInvoiceCharger() {
        return commentInvoiceCharger;
    }

    public void setCommentInvoiceCharger(String commentInvoiceCharger) {
        this.commentInvoiceCharger = commentInvoiceCharger;
    }

    public String getCommentGapCarrier() {
        return commentGapCarrier;
    }

    public void setCommentGapCarrier(String commentGapCarrier) {
        this.commentGapCarrier = commentGapCarrier;
    }

    public String getCommentGapCharger() {
        return commentGapCharger;
    }

    public void setCommentGapCharger(String commentGapCharger) {
        this.commentGapCharger = commentGapCharger;
    }

    public Integer getStatutArraw() {
        return statutArraw;
    }

    public void setStatutArraw(Integer statutArraw) {
        this.statutArraw = statutArraw;
    }

    public String getxEcIncotermLibelle() {
        return xEcIncotermLibelle;
    }

    public void setxEcIncotermLibelle(String xEcIncotermLibelle) {
        this.xEcIncotermLibelle = xEcIncotermLibelle;
    }

    public String getTypeRequestLibelle() {
        return typeRequestLibelle;
    }

    public void setTypeRequestLibelle(String typeRequestLibelle) {
        this.typeRequestLibelle = typeRequestLibelle;
    }

    public String getNumAwbBol() {
        return numAwbBol;
    }

    public void setNumAwbBol(String numAwbBol) {
        this.numAwbBol = numAwbBol;
    }

    public EbUser getxEbUserOwnerRequest() {
        return xEbUserOwnerRequest;
    }

    public void setxEbUserOwnerRequest(EbUser xEbUserOwnerRequest) {
        this.xEbUserOwnerRequest = xEbUserOwnerRequest;
    }

    public EbCompagnie getxEbCompagnieOwnerRequest() {
        return xEbCompagnieOwnerRequest;
    }

    public void setxEbCompagnieOwnerRequest(EbCompagnie xEbCompagnieOwnerRequest) {
        this.xEbCompagnieOwnerRequest = xEbCompagnieOwnerRequest;
    }

    public EbEtablissement getxEbEtablissementOwnerRequest() {
        return xEbEtablissementOwnerRequest;
    }

    public void setxEbEtablissementOwnerRequest(EbEtablissement xEbEtablissementOwnerRequest) {
        this.xEbEtablissementOwnerRequest = xEbEtablissementOwnerRequest;
    }

    public Integer getxEbTypeRequest() {
        return xEbTypeRequest;
    }

    public void setxEbTypeRequest(Integer xEbTypeRequest) {
        this.xEbTypeRequest = xEbTypeRequest;
    }
}
