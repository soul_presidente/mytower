package com.adias.mytowereasy.model;

import java.util.List;
import java.util.Map;


/**
 * Cette classe permet de contruire un object qui contient :
 * state: L'etat de l'export à un instant donnée
 * fileUrl: L'url du fichier à exporter
 * listResult: Les données de la base de données à exporter ainsi que les
 * colonnes
 * listTypeResult: les clès des colonnes qui ont le type de données Date
 */
public class FileDownloadStateAndResult {
    private Integer state;
    private String fileUrl;
    private List<Map<String, String[]>> listResult;
    private Map<String, String> listTypeResult;

    public FileDownloadStateAndResult(Integer state, String fileUrl, List<Map<String, String[]>> listResult) {
        this.state = state;
        this.fileUrl = fileUrl;
        this.listResult = listResult;
    }

    public FileDownloadStateAndResult() {
    }

    public List<Map<String, String[]>> getListResult() {
        return listResult;
    }

    public void setListResult(List<Map<String, String[]>> listResult) {
        this.listResult = listResult;
    }

    public Integer getState() {
        return state;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Map<String, String> getListTypeResult() {
        return listTypeResult;
    }

    public void setListTypeResult(Map<String, String> listTypeResult) {
        this.listTypeResult = listTypeResult;
    }
}
