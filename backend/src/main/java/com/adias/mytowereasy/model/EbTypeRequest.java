package com.adias.mytowereasy.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.dto.EbTypeRequestDTO;


@Table(name = "eb_type_request", indexes = {
    @Index(name = "idx_eb_type_request_x_eb_user", columnList = "x_eb_user"),
    @Index(name = "idx_eb_type_request_x_eb_company", columnList = "x_eb_company")
})
@Entity
public class EbTypeRequest extends EbTimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebTypeRequestNum;

    private String reference;

    private String libelle;

    private Integer delaiChronoPricing; // par nombre d'heures

    private Integer sensChronoPricing;

    private Boolean blocageResponsePricing;

    @ColumnDefault(value = "true")
    private Boolean activated; // permettant d'identifier si typeRequest est
                               // desactivés ou actives

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user")
    private EbUser xEbUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company", insertable = true)
    private EbCompagnie xEbCompagnie;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String refLibelle; // concatenation entre reference et libelle

    public EbTypeRequest(
        String reference,
        String libelle,
        Integer delaiChronoPricing,
        Integer sensChronoPricing,
        Boolean blocageResponsePricing,
        EbUser xEbUser,
        EbCompagnie xEbCompagnie) {
        super();
        this.reference = reference;
        this.libelle = libelle;
        this.delaiChronoPricing = delaiChronoPricing;
        this.sensChronoPricing = sensChronoPricing;
        this.blocageResponsePricing = blocageResponsePricing;
        this.xEbUser = xEbUser;
        this.xEbCompagnie = xEbCompagnie;
        this.activated = true;
    }

    // Projection details type of request
    @QueryProjection
    public EbTypeRequest(
        Integer ebTypeRequestNum,
        String reference,
        String libelle,
        Timestamp dateAjout,
        Timestamp dateMaj,
        Integer delaiChronoPricing,
        Integer sensChronoPricing,
        Boolean blocageResponsePricing,
        Boolean activated,
        Integer ebUserNum,
        String emailUser,
        EbCompagnie xEbCompagnie) {
        super();
        this.ebTypeRequestNum = ebTypeRequestNum;
        this.reference = reference;
        this.libelle = libelle;
        this.setDateCreationSys(dateAjout);
        this.setDateMajSys(dateMaj);
        this.delaiChronoPricing = delaiChronoPricing;
        this.sensChronoPricing = sensChronoPricing;
        this.blocageResponsePricing = blocageResponsePricing;
        this.activated = activated;
        this.xEbUser = new EbUser(ebUserNum);
        this.xEbUser.setEmail(emailUser);
        this.xEbCompagnie = xEbCompagnie;
    }

    public EbTypeRequest(EbTypeRequestDTO entity, EbUser userConnected) {
        super();
        this.ebTypeRequestNum = entity.getEbTypeRequestNum();
        this.reference = entity.getReference();
        this.libelle = entity.getLibelle();
        this.delaiChronoPricing = entity.getDelaiChronoPricing();
        this.sensChronoPricing = entity.getSensChronoPricing();
        this.activated = entity.getActivated();
        this.blocageResponsePricing = entity.getBlocageResponsePricing();
        this.xEbUser = userConnected;
        this.xEbCompagnie = userConnected != null ? userConnected.getEbCompagnie() : null;
    }

    public EbTypeRequest() {
        super();
    }

    public Integer getEbTypeRequestNum() {
        return ebTypeRequestNum;
    }

    public EbUser getxEbUser() {
        return xEbUser;
    }

    public void setxEbUser(EbUser xEbUser) {
        this.xEbUser = xEbUser;
    }

    public void setEbTypeRequestNum(Integer ebTypeRequestNum) {
        this.ebTypeRequestNum = ebTypeRequestNum;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Integer getDelaiChronoPricing() {
        return delaiChronoPricing;
    }

    public void setDelaiChronoPricing(Integer delaiChronoPricing) {
        this.delaiChronoPricing = delaiChronoPricing;
    }

    public Integer getSensChronoPricing() {
        return sensChronoPricing;
    }

    public void setSensChronoPricing(Integer sensChronoPricing) {
        this.sensChronoPricing = sensChronoPricing;
    }

    public Boolean getBlocageResponsePricing() {
        return blocageResponsePricing;
    }

    public void setBlocageResponsePricing(Boolean blocageResponsePricing) {
        this.blocageResponsePricing = blocageResponsePricing;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public String getRefLibelle() {
        this.refLibelle = this.getReference() + (this.getLibelle() != null ? "-" + this.getLibelle() : "");

        return refLibelle;
    }

    public void setRefLibelle(String refLibelle) {
        this.refLibelle = refLibelle;

        this.refLibelle = this.getReference() + (this.getLibelle() != null ? "-" + this.getLibelle() : "");
    }
}
