/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import javax.persistence.*;


@Entity
@Table(name = "ec_city", schema = "work")
public class EcCity implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ec_city_num")
    private Integer ecCityNum;

    @Column(name = "code")
    private String code;

    @Column(name = "libelle")
    private String libelle;

    @ManyToOne()
    @JoinColumn(name = "x_ec_country")
    private EcCountry ecCountry;

    public Integer getEcCityNum() {
        return ecCityNum;
    }

    public void setEcCityNum(Integer ecCityNum) {
        this.ecCityNum = ecCityNum;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public EcCountry getEcCountry() {
        return ecCountry;
    }

    public void setEcCountry(EcCountry ecCountry) {
        this.ecCountry = ecCountry;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((ecCityNum == null) ? 0 : ecCityNum.hashCode());
        result = prime * result + ((ecCountry == null) ? 0 : ecCountry.hashCode());
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EcCity other = (EcCity) obj;

        if (code == null) {
            if (other.code != null) return false;
        }
        else if (!code.equals(other.code)) return false;

        if (ecCityNum == null) {
            if (other.ecCityNum != null) return false;
        }
        else if (!ecCityNum.equals(other.ecCityNum)) return false;

        if (ecCountry == null) {
            if (other.ecCountry != null) return false;
        }
        else if (!ecCountry.equals(other.ecCountry)) return false;

        if (libelle == null) {
            if (other.libelle != null) return false;
        }
        else if (!libelle.equals(other.libelle)) return false;

        return true;
    }
}
