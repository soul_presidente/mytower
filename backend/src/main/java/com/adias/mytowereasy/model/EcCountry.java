/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;


@Entity
@Table(name = "ec_country", schema = "work")
@JsonIgnoreProperties({
    "hibernateLazyInitializer", "handler"
})
public class EcCountry implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @JsonView(value = SummaryJsonView.class)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ec_country_num")
    private Integer ecCountryNum;

    @JsonView(value = SummaryJsonView.class)
    @Column(name = "code")
    private String code;

    @JsonView(value = SummaryJsonView.class)
    @Column(name = "libelle")
    private String libelle;

    @Column(name = "europ")
    private Boolean europ;

    @Column(name = "commercial_mandatory")
    private Boolean commercialMandatory;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_operational_grouping")
    private EcOperationalGrouping ecOperationalGrouping;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_region")
    private EcRegion ecRegion;

    @JsonView(value = SummaryJsonView.class)
    @Column(name = "libelle_code")
    private String libelleCode;
    @JsonView(value = SummaryJsonView.class)
    @Column(name = "code_libelle")
    private String codeLibelle;

    private Double latitude;

    private Double longitude;

    public EcCountry(Integer ecCountryNum, String code, String libelle) {
        this.ecCountryNum = ecCountryNum;
        this.code = code;
        this.libelle = libelle;
    }

    public String getCodeLibelle() {
        String str = "";

        if (this.code != null) str += this.code;
        if (this.libelle != null && this.code != null) str += " - ";
        if (this.libelle != null) str += this.libelle;

        this.codeLibelle = str;

        return this.codeLibelle;
    }

    public void setCodeLibelle(String codeLibelle) {
        String str = "";

        if (this.code != null) str += this.code;
        if (this.libelle != null && this.code != null) str += " - ";
        if (this.libelle != null) str += this.libelle;

        this.codeLibelle = str;
    }

    public String getLibelleCode() {
        String str = "";

        if (this.libelle != null) str += this.libelle;
        if (this.libelle != null && this.code != null) str += " - ";
        if (this.code != null) str += this.code;

        this.libelleCode = str;

        return this.libelleCode;
    }

    public void setLibelleCode(String libelleCode) {
        String str = "";

        if (this.libelle != null) str += this.libelle;
        if (this.libelle != null && this.code != null) str += " - ";
        if (this.code != null) str += this.code;

        this.libelleCode = str;
    }

    public Integer getEcCountryNum() {
        return ecCountryNum;
    }

    public void setEcCountryNum(Integer ecCountryNum) {
        this.ecCountryNum = ecCountryNum;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean getEurop() {
        return europ;
    }

    public void setEurop(Boolean europ) {
        this.europ = europ;
    }

    public Boolean getCommercialMandatory() {
        return commercialMandatory;
    }

    public void setCommercialMandatory(Boolean commercialMandatory) {
        this.commercialMandatory = commercialMandatory;
    }

    public EcOperationalGrouping getEcOperationalGrouping() {
        return ecOperationalGrouping;
    }

    public void setEcOperationalGrouping(EcOperationalGrouping ecOperationalGrouping) {
        this.ecOperationalGrouping = ecOperationalGrouping;
    }

    public EcRegion getEcRegion() {
        return ecRegion;
    }

    public void setEcRegion(EcRegion ecRegion) {
        this.ecRegion = ecRegion;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((commercialMandatory == null) ? 0 : commercialMandatory.hashCode());
        result = prime * result + ((ecCountryNum == null) ? 0 : ecCountryNum.hashCode());
        result = prime * result + ((ecOperationalGrouping == null) ? 0 : ecOperationalGrouping.hashCode());
        result = prime * result + ((ecRegion == null) ? 0 : ecRegion.hashCode());
        result = prime * result + ((europ == null) ? 0 : europ.hashCode());
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        result = prime * result + ((codeLibelle == null) ? 0 : codeLibelle.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EcCountry other = (EcCountry) obj;

        if (code == null) {
            if (other.code != null) return false;
        }
        else if (!code.equals(other.code)) return false;

        if (commercialMandatory == null) {
            if (other.commercialMandatory != null) return false;
        }
        else if (!commercialMandatory.equals(other.commercialMandatory)) return false;

        if (ecCountryNum == null) {
            if (other.ecCountryNum != null) return false;
        }
        else if (!ecCountryNum.equals(other.ecCountryNum)) return false;

        if (ecOperationalGrouping == null) {
            if (other.ecOperationalGrouping != null) return false;
        }
        else if (!ecOperationalGrouping.equals(other.ecOperationalGrouping)) return false;

        if (ecRegion == null) {
            if (other.ecRegion != null) return false;
        }
        else if (!ecRegion.equals(other.ecRegion)) return false;

        if (europ == null) {
            if (other.europ != null) return false;
        }
        else if (!europ.equals(other.europ)) return false;

        if (libelle == null) {
            if (other.libelle != null) return false;
        }
        else if (!libelle.equals(other.libelle)) return false;

        if (codeLibelle == null) {
            if (other.codeLibelle != null) return false;
        }
        else if (!codeLibelle.equals(other.codeLibelle)) return false;

        return true;
    }

    public EcCountry() {
        super();
    }

    public EcCountry(Integer ecCountryNum) {
        this.ecCountryNum = ecCountryNum;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
