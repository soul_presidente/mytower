/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

import javax.persistence.*;
import javax.validation.constraints.Size;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.querydsl.core.annotations.PropertyType;
import com.querydsl.core.annotations.QueryProjection;
import com.querydsl.core.annotations.QueryType;

import com.adias.mytowereasy.airbus.model.EbUserProfile;
import com.adias.mytowereasy.connectedUser.ConnectedUser;
import com.adias.mytowereasy.model.Enumeration.Role;
import com.adias.mytowereasy.model.Enumeration.ServiceType;
import com.adias.mytowereasy.types.CustomObjectIdResolver;


@SqlResultSetMappings({
    @SqlResultSetMapping(name = "ContactMapping", classes = {
        @ConstructorResult(targetClass = EbUser.class, columns = {
            @ColumnResult(name = "eb_user_num", type = Integer.class),
            @ColumnResult(name = "nom"),
            @ColumnResult(name = "prenom"),
            @ColumnResult(name = "email"),
            @ColumnResult(name = "enabled", type = Boolean.class),
            @ColumnResult(name = "status", type = Integer.class),
            @ColumnResult(name = "service", type = Integer.class),
            @ColumnResult(name = "role", type = Integer.class),
            @ColumnResult(name = "telephone"),
            @ColumnResult(name = "eb_relation_num", type = Integer.class),
            @ColumnResult(name = "type_relation", type = Integer.class),
            @ColumnResult(name = "etat", type = Integer.class),
            @ColumnResult(name = "rel_email"),
            @ColumnResult(name = "x_eb_etablissement", type = Integer.class),
            @ColumnResult(name = "x_eb_etablissement_host", type = Integer.class),
            @ColumnResult(name = "x_host", type = Integer.class),
            @ColumnResult(name = "x_guest", type = Integer.class),
            @ColumnResult(name = "eb_etablissement_num", type = Integer.class),
            @ColumnResult(name = "etab_nom"),
            @ColumnResult(name = "etab_email"),
            @ColumnResult(name = "etab_service"),
            @ColumnResult(name = "etab_telephone"),
            @ColumnResult(name = "eb_compagnie_num", type = Integer.class),
            @ColumnResult(name = "comp_nom"),
            @ColumnResult(name = "comp_role", type = Integer.class),
            @ColumnResult(name = "comp_siren", type = String.class),
            @ColumnResult(name = "tags", type = String.class),
            @ColumnResult(name = "comment", type = String.class),
            @ColumnResult(name = "activated"),
            @ColumnResult(name = "eb_user_tag_num"),
            @ColumnResult(name = "tag_guest"),
            @ColumnResult(name = "tag_host"),
            @ColumnResult(name = "tag_compagnie_guest"),
            @ColumnResult(name = "tag_compagnie_host"),
            @ColumnResult(name = "tag_etablissement_guest"),
            @ColumnResult(name = "tag_etablissement_host")
        })
    }), @SqlResultSetMapping(name = "UserMailDemandeMapping", classes = {
        @ConstructorResult(targetClass = EbUser.class, columns = {
            @ColumnResult(name = "eb_user_num", type = Integer.class),
            @ColumnResult(name = "nom", type = String.class),
            @ColumnResult(name = "prenom", type = String.class),
            @ColumnResult(name = "email", type = String.class),
            @ColumnResult(name = "ROLE", type = Integer.class),
            @ColumnResult(name = "language", type = String.class),
            @ColumnResult(name = "eb_etablissement_num", type = Integer.class),
            @ColumnResult(name = "etablissement_nom", type = String.class),
            @ColumnResult(name = "eb_compagnie_num", type = Integer.class),
            @ColumnResult(name = "compagnie_nom", type = String.class),
            @ColumnResult(name = "code", type = String.class),
            @ColumnResult(name = "list_params_mail", type = String.class),
            @ColumnResult(name = "list_params_mail_str", type = String.class),
            @ColumnResult(name = "created_by_me", type = Boolean.class)
        })
    })
})
@Entity
@Table(name = "eb_user", schema = "work", indexes = {
    @Index(name = "idx_eb_user_x_eb_etablissement", columnList = "x_eb_etablissement"),
    @Index(name = "idx_eb_user_x_eb_compagnie", columnList = "x_eb_compagnie"),
    @Index(name = "idx_eb_user_x_eb_user_profile", columnList = "x_eb_user_profile")
})
@JsonIgnoreProperties(ignoreUnknown = true)

@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "ebUserNum",
    resolver = CustomObjectIdResolver.class,
    scope = EbUser.class)
@NamedEntityGraph(name = "EbUser.detail", attributeNodes = {
    @NamedAttributeNode(value = "ebEtablissement", subgraph = "graph.etablissement.compagnie")
},
    subgraphs = @NamedSubgraph(
        name = "graph.etablissement.compagnie",
        attributeNodes = @NamedAttributeNode("ebCompagnie")))
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "dtype")
public class EbUser implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebUserNum;

    @Column(unique = true)
    private String username;

    @Column(name = "PASSWORD", length = 250, updatable = false)
    @Size(min = 8)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JsonDeserialize
    private String password;

    private String nom;

    @Column
    private String prenom;

    private String email;

    @Column(name = "ENABLED")
    private Boolean enabled;

    @Column(name = "date_activation")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastPasswordResetDate;

    @Column
    private String adresse;

    private String telephone;

    private String locale;

    private boolean admin;

    @Column(length = 255)
    private String resetToken;

    // Transporteur / Broker
    private Integer service;

    // chargeur/ prestataire
    private Integer role;

    @Column(length = 255)
    private String activeToken;

    @Column
    private Integer status;

    @Column(insertable = false, updatable = false)
    @ColumnDefault("now()")

    private Date dateCreation;

    // @ManyToMany(fetch = FetchType.EAGER)
    // @JoinTable(name = "ex_user_role", schema = "work", joinColumns = {
    // @JoinColumn(name = "x_eb_user") }, inverseJoinColumns = {
    // @JoinColumn(name =
    // "x_ec_role") })
    // private List<EcRole> roles;
    //
    // @ManyToOne(cascade = CascadeType.ALL)
    // @JoinColumn(name = "x_ec_role")
    // private EcRole xEcRoleUser;

    // @ManyToOne(cascade = CascadeType.PERSIST)

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement")
    private EbEtablissement ebEtablissement;

    private String etablissementNom;

    private String groupes;

    @OneToOne(mappedBy = "userBackUp", fetch = FetchType.LAZY)
    @JoinColumn(name = "user_backup")
    private EbDelegation delegation;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "host")
    private Set<EbRelation> contactsHost;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "guest")
    private Set<EbRelation> contactsGuests;

    @OneToMany(cascade = {
        CascadeType.MERGE, CascadeType.REMOVE
    }, fetch = FetchType.LAZY, mappedBy = "ebUser")
    // @JsonManagedReference
    private Set<ExUserModule> listAccessRights;


    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbCategorie> listCategories;

    private String listLabels;

    @Column
    private boolean superAdmin;

    @Transient
    @JsonSerialize
    @JsonDeserialize
    private Boolean isFromTransPlan;
    @Transient
    @JsonSerialize
    @JsonDeserialize
    private String refPlan;
    @Transient
    @JsonSerialize
    @JsonDeserialize
    private Boolean selected;
    @Transient
    @JsonSerialize
    @JsonDeserialize
    private String coment;
    @Transient
    @JsonSerialize
    @JsonDeserialize
    private BigDecimal calculatedPrice;
    @Transient
    @JsonSerialize
    @JsonDeserialize
    private Integer transitTime;

    @Transient
    @JsonSerialize
    @JsonDeserialize
    private Boolean exchangeRateFound;

    @Transient
    @JsonSerialize
    @QueryType(PropertyType.SIMPLE)
    private EbRelation ebRelation;

    @Transient
    @JsonSerialize
    @QueryType(PropertyType.SIMPLE)
    private EbUserTag ebUserTag;

    @ManyToOne(cascade = {
        CascadeType.MERGE, CascadeType.REFRESH
    }, fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", updatable = false)
    private EbCompagnie ebCompagnie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_profile")
    private EbUserProfile ebUserProfile;

    @ColumnDefault(value = "'en'")
    private String language;

    @Transient
    @JsonSerialize
    private String roleLibelle;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    @JsonManagedReference
    private Set<EbFavori> favoris;

    @Transient
    private Integer count;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private Map<String, Boolean> accessRights;

    @Transient
    @JsonSerialize
    private boolean uniqueAdminOfEtablissement;

    @Transient
    @JsonSerialize
    private Boolean createdByMe;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String realUserPassword;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer realUserNum;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String realUserNom;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String realUserPrenom;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String realUserEmail;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer realUserEtabNum;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String realUserEtabNom;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer realUserCompNum;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String realUserCompNom;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String connectedUserAuthToken;
    @Transient
    private Boolean isUnknownUser;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbParamsMail> listParamsMail;

    @Column(columnDefinition = "TEXT")
    private String listParamsMailStr;

    @Column
    private Integer fuseauHoraire;

    private Boolean superCt;

    public EbUser() {
    }

    public EbUser(Integer ebUserNum, Object listCategories) {
        this.ebUserNum = ebUserNum;

        if (listCategories != null) {

            try {
                Gson gson = new Gson();
                String json = gson.toJson(listCategories);
                this.listCategories = gson.fromJson(json, new TypeToken<ArrayList<EbCategorie>>() {
                }.getType());
            } catch (Exception e) {
                System.err.println("ConnectedUser: failed to convert object to listCategories.");
            }

        }

    }

    public EbUser(EbUser user) {

        if (user != null) {
            this.ebUserNum = user.getEbUserNum();
            if (user.getNom() != null) this.nom = user.getNom().toString();
            if (user.getPrenom() != null) this.prenom = user.getPrenom().toString();
            if (user.getEmail() != null) this.email = user.getEmail().toString();
        }

    }

    public EbUser(Integer ebUserNum) {
        this.ebUserNum = ebUserNum;
    }

    public EbUser(Integer ebUserNum, Integer ebEtablissementNum, Integer ebCompagnieNum) {
        this.ebUserNum = ebUserNum;
        this.ebEtablissement = new EbEtablissement(ebEtablissementNum);
        this.ebCompagnie = new EbCompagnie(ebCompagnieNum);
    }

    public EbUser(Integer ebUserNum, String nom, String prenom) {
        this.ebUserNum = ebUserNum;
        this.nom = nom;
        this.prenom = prenom;
    }

    public EbUser(String email, Integer ebUserNum, String language) {
        this.ebUserNum = ebUserNum;
        this.email = email;
        this.language = language;
    }

    public EbUser(Integer ebUserNum, String nom, String prenom, String email) {
        this.ebUserNum = ebUserNum;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
    }

    public EbUser(Integer ebUserNum, String nom, String prenom, String email, String password) {
        this.ebUserNum = ebUserNum;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.password = password;
    }

    public EbUser(
        Integer ebUserNum,
        String nom,
        String prenom,
        String email,
        Integer service,
        Integer role,
        String telephone) {
        this.ebUserNum = ebUserNum;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.service = service;
        this.telephone = telephone;
    }

    public EbUser(Integer ebUserNum, String nom, String prenom, String email, Integer service, String telephone) {
        this.ebUserNum = ebUserNum;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.service = service;
        this.telephone = telephone;
    }

    public EbUser(
        Integer ebUserNum,
        List<EbParamsMail> listParamsMail,
        Integer ebEtablissementNum,
        Integer ebCompagnieNum) {
        this.ebUserNum = ebUserNum;
        this.listParamsMail = listParamsMail;

        if (ebEtablissementNum != null) {
            this.ebEtablissement = new EbEtablissement(ebEtablissementNum);
        }

        if (ebCompagnieNum != null) {
            this.ebCompagnie = new EbCompagnie(ebCompagnieNum);
        }

    }

    // ContactMapping
    public EbUser(
        Integer ebUserNum,
        String nom,
        String prenom,
        String email,
        Boolean enabled,
        Integer status,
        Integer service,
        Integer role,
        String telephone,
        Integer ebRelationNum,
        Integer typeRelation,
        Integer etat,
        String relEmail,
        Integer xEbEtabGuestNum,
        Integer xEbEtabHostNum,
        Integer xEbHostNum,
        Integer xEbGuestNum,
        Integer ebEtabNum,
        String ebEtabNom,
        String ebEtabEmail,
        Integer ebEtabService,
        String ebEtabTelephone,
        Integer compNum,
        String compNom,
        Integer compRole,
        String compSiren,
        String tags,
        String comment,
        Boolean activated,
        Integer ebUserTagNum,

        Integer xTagGuestNum,
        Integer xTagHostNum,

        Integer xTagCompGuestNum,
        Integer xTagCompHostNum,

        Integer xTagEtabGuestNum,
        Integer xTagEtabHostNum) {
        EbRelation rel = new EbRelation();

        EbUserTag usertag = new EbUserTag();

        this.ebUserNum = ebUserNum;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.service = service;
        this.role = role;
        this.telephone = telephone;
        this.enabled = enabled;
        this.status = status;

        rel.setEbRelationNum(ebRelationNum);
        rel.setTypeRelation(typeRelation);
        rel.setEtat(etat);
        rel.setEmail(relEmail);
        rel.setxEbEtablissement(new EbEtablissement(xEbEtabGuestNum));
        rel.setxEbEtablissementHost(new EbEtablissement(xEbEtabHostNum));
        rel.setHost(new EbUser(xEbHostNum));
        rel.setGuest(new EbUser(xEbGuestNum));
        this.setEbRelation(rel);

        usertag.setTags(tags);
        usertag.setActivated(activated);
        usertag.setEbUserTagNum(ebUserTagNum);
        usertag.setComment(comment);
        usertag.setxEbEtablissementGuest(new EbEtablissement(xTagEtabGuestNum));
        usertag.setxEbEtablissementHost(new EbEtablissement(xTagEtabHostNum));
        usertag.setHost(new EbUser(xTagHostNum));
        usertag.setGuest(new EbUser(xTagGuestNum));
        usertag.setxEbCompanieHost(new EbCompagnie(xTagCompHostNum));
        usertag.setxEbCompagnieGuest(new EbCompagnie(xTagCompGuestNum));
        this.setEbUserTag(usertag);

        EbEtablissement etab = new EbEtablissement();
        etab.setEbEtablissementNum(ebEtabNum);
        etab.setNom(ebEtabNom);
        etab.setEmail(ebEtabEmail);
        etab.setService(ebEtabService);
        etab.setTelephone(ebEtabTelephone);
        this.setEbEtablissement(etab);

        EbCompagnie comp = new EbCompagnie();
        comp.setEbCompagnieNum(compNum);
        comp.setNom(compNom);
        comp.setCompagnieRole(compRole);
        comp.setSiren(compSiren);
        this.setEbCompagnie(comp);
    }

    @QueryProjection
    public EbUser(
        Integer ebUserNum,
        String nom,
        String prenom,
        String email,
        Integer service,
        Integer etablissementNum,
        String etablissementNom,
        Boolean etablissementCanShowPrice) {
        this.ebUserNum = ebUserNum;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.service = service;
        this.ebEtablissement = new EbEtablissement(etablissementNum, etablissementNom, etablissementCanShowPrice);
    }

    @QueryProjection
    public EbUser(
        Integer ebUserNum,
        String nom,
        String prenom,
        String email,
        Integer service,
        Integer etablissementNum,
        String etablissementNom,
        EbRelation ebRelation) {
        this.ebUserNum = ebUserNum;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.service = service;
        this.ebEtablissement = new EbEtablissement(etablissementNum, etablissementNom);
        this.ebRelation = ebRelation;
    }

    @QueryProjection
    public EbUser(
        Integer ebUserNum,
        String nom,
        String prenom,
        String username,
        String email,
        Integer service,
        Integer status,
        Boolean admin,
        Boolean superAdmin,
        Integer role,
        String groupes,
        String password,
        Integer etablissementNum,
        String etablissementNom,
        Boolean etablissementCanShowPrice,
        Integer ebCompagnieNum,
        String nomCompagnie,
        String codeCompagnie,
        Integer ebUserProfileNum,
        String nomProfil,
        String descriptifProfil,
        String telephone) {
        this.telephone = telephone;
        this.ebUserNum = ebUserNum;
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.email = email;
        this.service = service;
        this.status = status;
        this.admin = admin;
        this.superAdmin = superAdmin;
        this.role = role;
        this.groupes = groupes;
        this.password = password;
        this.ebEtablissement = new EbEtablissement(etablissementNum, etablissementNom, etablissementCanShowPrice);
        this.ebEtablissement.setEbCompagnie(new EbCompagnie(ebCompagnieNum, nomCompagnie, codeCompagnie));
        this.ebCompagnie = new EbCompagnie(ebCompagnieNum, nomCompagnie, codeCompagnie);
        this.ebUserProfile = new EbUserProfile(ebUserProfileNum, nomProfil, descriptifProfil);
    }

    @QueryProjection
    public EbUser(
        Integer ebUserNum,
        String nom,
        String prenom,
        String username,
        String email,
        Integer service,
        Integer status,
        Boolean admin,
        Boolean superAdmin,
        Integer role,
        String groupes,
        Integer etablissementNum,
        String etablissementNom,
        Boolean etablissementCanShowPrice,
        Integer ebCompagnieNum,
        String nomCompagnie,
        String codeCompagnie) {
        this.ebUserNum = ebUserNum;
        this.nom = nom;
        this.prenom = prenom;
        this.username = username;
        this.email = email;
        this.service = service;
        this.status = status;
        this.admin = admin;
        this.superAdmin = superAdmin;
        this.role = role;
        this.groupes = groupes;
        this.setEtablissementNom(etablissementNom);
        this.ebEtablissement = new EbEtablissement(etablissementNum, etablissementNom, etablissementCanShowPrice);
        this.ebEtablissement.setEbCompagnie(new EbCompagnie(ebCompagnieNum, nomCompagnie, codeCompagnie));
        this.ebCompagnie = new EbCompagnie(ebCompagnieNum, nomCompagnie, codeCompagnie);
    }

    // Param mail projection
    @QueryProjection
    public EbUser(
        Integer ebUserNum,
        String nom,
        String prenom,
        String email,
        Integer role,
        String language,
        Integer etablissementNum,
        String etablissementNom,
        Integer ebCompagnieNum,
        String nomCompagnie,
        String codeCompagnie,
        String listParamsMail,
        String listParamsMailStr,
        Boolean createdByMe

    ) {
        this.ebUserNum = ebUserNum;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        // this.service = service;
        // this.status = status;
        // this.admin = admin;
        // this.superAdmin = superAdmin;
        this.role = role;
        this.ebEtablissement = new EbEtablissement(etablissementNum, etablissementNom);
        this.ebEtablissement.setEbCompagnie(new EbCompagnie(ebCompagnieNum, nomCompagnie, codeCompagnie));
        this.ebCompagnie = new EbCompagnie(ebCompagnieNum, nomCompagnie, codeCompagnie);
        Gson gson = new Gson();
        this.listParamsMailStr = listParamsMailStr;
        this.listParamsMail = gson.fromJson(listParamsMail, new TypeToken<ArrayList<EbParamsMail>>() {
        }.getType());
        this.createdByMe = createdByMe;
        this.language = language;
    }

    public BigDecimal getCalculatedPrice() {
        return calculatedPrice;
    }

    public void setCalculatedPrice(BigDecimal calculatedPrice) {
        this.calculatedPrice = calculatedPrice;
    }

    public Boolean getIsFromTransPlan() {
        return isFromTransPlan;
    }

    public void setIsFromTransPlan(Boolean isFromTransPlan) {
        this.isFromTransPlan = isFromTransPlan;
    }

    public String getRefPlan() {
        return refPlan;
    }

    public void setRefPlan(String refPlan) {
        this.refPlan = refPlan;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public Integer getEbUserNum() {
        return ebUserNum;
    }

    public void setEbUserNum(Integer ebUserNum) {
        this.ebUserNum = ebUserNum;
    }

    public String getNomPrenom() {
        String nomPrenom = "";
        if (this.nom != null) nomPrenom += this.nom;

        if (this.nom != null && this.prenom != null) nomPrenom += " ";

        if (this.prenom != null) nomPrenom += this.prenom;

        return nomPrenom;
    }

    public String getCompagnieNameNomUserPrenomUser() {
        String compagnieNameNomUserPrenomUser = "";
        if (this.ebCompagnie != null
            && this.ebCompagnie.getNom() != null) compagnieNameNomUserPrenomUser += this.ebCompagnie.getNom();

        if (this.nom != null && this.prenom != null) compagnieNameNomUserPrenomUser += " -  ";

        if (this.nom != null) compagnieNameNomUserPrenomUser += this.nom;

        if (this.nom != null && this.prenom != null) compagnieNameNomUserPrenomUser += "  ";

        if (this.prenom != null) compagnieNameNomUserPrenomUser += this.nom;

        return compagnieNameNomUserPrenomUser;
    }

    public String getRealUserNomPrenom() {
        String nomPrenom = "";
        if (this.realUserNom != null) nomPrenom += this.realUserNom;

        if (this.realUserNom != null && this.realUserPrenom != null) nomPrenom += " ";

        if (this.realUserPrenom != null) nomPrenom += this.realUserPrenom;

        return nomPrenom;
    }

    public String getPrenomNom() {
        String prenomNom = "";

        if (this.prenom != null) prenomNom += this.prenom;

        if (this.nom != null && this.prenom != null) prenomNom += " ";

        if (this.nom != null) prenomNom += this.nom;

        return prenomNom;
    }

    public String getRealUserPrenomNom() {
        String prenomNom = "";

        if (this.realUserPrenom != null) prenomNom += this.realUserPrenom;

        if (this.realUserNom != null && this.realUserPrenom != null) prenomNom += " ";

        if (this.realUserNom != null) prenomNom += this.realUserNom;

        return prenomNom;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public EbDelegation getDelegation() {
        return delegation;
    }

    public void setDelegation(EbDelegation delegation) {
        this.delegation = delegation;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public EbEtablissement getEbEtablissement() {
        return ebEtablissement;
    }

    public void setEbEtablissement(EbEtablissement ebEtablissement) {
        this.ebEtablissement = ebEtablissement;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public Integer getService() {
        return service;
    }

    public void setService(Integer service) {
        this.service = service;
    }

    public String getResetToken() {
        return resetToken;
    }

    public void setResetToken(String resetToken) {
        this.resetToken = resetToken;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getActiveToken() {
        return activeToken;
    }

    public void setActiveToken(String activeToken) {
        this.activeToken = activeToken;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Date getLastPasswordResetDate() {
        return lastPasswordResetDate;
    }

    public void setLastPasswordResetDate(Date lastPasswordResetDate) {
        this.lastPasswordResetDate = lastPasswordResetDate;
    }

    public Set<EbRelation> getContactsHost() {
        return contactsHost;
    }

    public void setContactsHost(Set<EbRelation> contactsHost) {
        this.contactsHost = contactsHost;
    }

    public Set<EbRelation> getContactsGuests() {
        return contactsGuests;
    }

    public void setContactsGuests(Set<EbRelation> contactsGuests) {
        this.contactsGuests = contactsGuests;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public KibanaUser toKibanaUser() {
        return new KibanaUser(this.ebUserNum, this.username);
    }

    public ConnectedUser toConnectedUser() {
        return new ConnectedUser(this);
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    @JsonIgnore
    public Boolean isChargeur() {
        return (this.role != null && this.role.equals(Role.ROLE_CHARGEUR.getCode()));
    }

    @JsonIgnore
    public Boolean isControlTower() {
        return (this.role != null && this.role.equals(Role.ROLE_CONTROL_TOWER.getCode()));
    }

    @JsonIgnore
    public Boolean isDefaultControlTower() {
        return isControlTower() && Role.ROLE_CONTROL_TOWER.getCode().equals(this.getEbCompagnie().getCompagnieRole());
    }

    @JsonIgnore
    public Boolean isPrestataire() {
        return (this.role != null && this.role.equals(Role.ROLE_PRESTATAIRE.getCode()));
    }

    @JsonIgnore
    public Boolean isBroker() {
        return isPrestataire() && (this.service != null && this.service.equals(ServiceType.SERVICE_BROKER.getCode()));
    }

    @JsonIgnore
    public Boolean isTransporteur() {
        return isPrestataire()
            && (this.service != null && this.service.equals(ServiceType.SERVICE_TRANSPORTEUR.getCode()));
    }

    @JsonIgnore
    public Boolean isTransporteurBroker() {
        return isPrestataire()
            && (this.service != null && this.service.equals(ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode()));
    }

    public Set<ExUserModule> getListAccessRights() {
        return listAccessRights;
    }

    public void setListAccessRights(Set<ExUserModule> listAccessRights) {

        if (listAccessRights != null) {

            for (ExUserModule access: listAccessRights) {
                access.setEbUser(this);
            }

        }

        this.listAccessRights = listAccessRights;
    }

    public EbRelation getEbRelation() {
        return ebRelation;
    }

    public void setEbRelation(EbRelation ebRelation) {
        this.ebRelation = ebRelation;
    }

    public boolean isSuperAdmin() {
        return superAdmin;
    }

    public void setSuperAdmin(boolean superAdmin) {
        this.superAdmin = superAdmin;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;

        if (role != null) {
            roleLibelle = Role.getByCode(role).getLibelle();
        }

    }

    public List<EbCategorie> getListCategories() {
        return listCategories;
    }

    public void setListCategories(List<EbCategorie> listCategories) {
        this.listCategories = listCategories;
    }

    public Boolean getSuperAdmin() {
        return superAdmin;
    }

    public void setSuperAdmin(Boolean superAdmin) {
        this.superAdmin = superAdmin;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getRoleLibelle() {
        return roleLibelle;
    }

    public void setRoleLibelle(String roleLibelle) {

        if (roleLibelle == null) {

            if (role != null) {
                roleLibelle = Role.getByCode(role).getLibelle();
            }

        }

        this.roleLibelle = roleLibelle;
    }

    public boolean isUniqueAdminOfEtablissement() {
        return uniqueAdminOfEtablissement;
    }

    public void setUniqueAdminOfEtablissement(boolean uniqueAdminOfEtablissement) {
        this.uniqueAdminOfEtablissement = uniqueAdminOfEtablissement;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getRealUserNum() {
        return realUserNum;
    }

    public void setRealUserNum(Integer realUserNum) {
        this.realUserNum = realUserNum;
    }

    public String getRealUserNom() {
        return realUserNom;
    }

    public void setRealUserNom(String realUserNom) {
        this.realUserNom = realUserNom;
    }

    public String getRealUserPrenom() {
        return realUserPrenom;
    }

    public void setRealUserPrenom(String realUserPrenom) {
        this.realUserPrenom = realUserPrenom;
    }

    public String getRealUserEmail() {
        return realUserEmail;
    }

    public void setRealUserEmail(String realUserEmail) {
        this.realUserEmail = realUserEmail;
    }

    public Integer getRealUserEtabNum() {
        return realUserEtabNum;
    }

    public void setRealUserEtabNum(Integer realUserEtabNum) {
        this.realUserEtabNum = realUserEtabNum;
    }

    public String getRealUserEtabNom() {
        return realUserEtabNom;
    }

    public void setRealUserEtabNom(String realUserEtabNom) {
        this.realUserEtabNom = realUserEtabNom;
    }

    public Integer getRealUserCompNum() {
        return realUserCompNum;
    }

    public void setRealUserCompNum(Integer realUserCompNum) {
        this.realUserCompNum = realUserCompNum;
    }

    public String getRealUserCompNom() {
        return realUserCompNom;
    }

    public void setRealUserCompNom(String realUserCompNom) {
        this.realUserCompNom = realUserCompNom;
    }

    public String getRealUserPassword() {
        return realUserPassword;
    }

    public void setRealUserPassword(String realUserPassword) {
        this.realUserPassword = realUserPassword;
    }

    public Map<String, Boolean> getAccessRights() {
        return accessRights;
    }

    public void setAccessRights(Map<String, Boolean> accessRights) {
        this.accessRights = accessRights;
    }

    public String getComent() {
        return coment;
    }

    public void setComent(String coment) {
        this.coment = coment;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((activeToken == null) ? 0 : activeToken.hashCode());
        result = prime * result + (admin ? 1231 : 1237);
        result = prime * result + ((adresse == null) ? 0 : adresse.hashCode());
        // result = prime * result + ((contactsGuests == null) ? 0 :
        // contactsGuests.hashCode());
        // result = prime * result + ((contactsHost == null) ? 0 :
        // contactsHost.hashCode());
        result = prime * result + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        // result = prime * result + ((delegation == null) ? 0 :
        // delegation.hashCode());
        // result = prime * result + ((ebCompagnie == null) ? 0 :
        // ebCompagnie.hashCode());
        // result = prime * result + ((ebEtablissement == null) ? 0 :
        // ebEtablissement.hashCode());
        // result = prime * result + ((ebRelation == null) ? 0 :
        // ebRelation.hashCode());
        result = prime * result + ((ebUserNum == null) ? 0 : ebUserNum.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((enabled == null) ? 0 : enabled.hashCode());
        result = prime * result + ((language == null) ? 0 : language.hashCode());
        result = prime * result + ((lastPasswordResetDate == null) ? 0 : lastPasswordResetDate.hashCode());
        // result = prime * result + ((listAccessRights == null) ? 0 :
        // listAccessRights.hashCode());
        // result = prime * result + ((listCategories == null) ? 0 :
        // listCategories.hashCode());
        result = prime * result + ((locale == null) ? 0 : locale.hashCode());
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        result = prime * result + ((password == null) ? 0 : password.hashCode());
        result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
        result = prime * result + ((resetToken == null) ? 0 : resetToken.hashCode());
        result = prime * result + ((role == null) ? 0 : role.hashCode());
        result = prime * result + ((roleLibelle == null) ? 0 : roleLibelle.hashCode());
        result = prime * result + ((service == null) ? 0 : service.hashCode());
        result = prime * result + ((status == null) ? 0 : status.hashCode());
        result = prime * result + (superAdmin ? 1231 : 1237);
        result = prime * result + ((telephone == null) ? 0 : telephone.hashCode());

        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbUser other = (EbUser) obj;

        if (activeToken == null) {
            if (other.activeToken != null) return false;
        }
        else if (!activeToken.equals(other.activeToken)) return false;

        if (admin != other.admin) return false;

        if (adresse == null) {
            if (other.adresse != null) return false;
        }
        else if (!adresse.equals(other.adresse)) return false;

        if (contactsGuests == null) {
            if (other.contactsGuests != null) return false;
        }
        else if (!contactsGuests.equals(other.contactsGuests)) return false;

        if (contactsHost == null) {
            if (other.contactsHost != null) return false;
        }
        else if (!contactsHost.equals(other.contactsHost)) return false;

        if (dateCreation == null) {
            if (other.dateCreation != null) return false;
        }
        else if (!dateCreation.equals(other.dateCreation)) return false;

        if (delegation == null) {
            if (other.delegation != null) return false;
        }
        else if (!delegation.equals(other.delegation)) return false;

        if (ebCompagnie == null) {
            if (other.ebCompagnie != null) return false;
        }
        else if (!ebCompagnie.equals(other.ebCompagnie)) return false;

        if (ebEtablissement == null) {
            if (other.ebEtablissement != null) return false;
        }
        else if (!ebEtablissement.equals(other.ebEtablissement)) return false;

        if (ebRelation == null) {
            if (other.ebRelation != null) return false;
        }
        else if (!ebRelation.equals(other.ebRelation)) return false;

        if (ebUserNum == null) {
            if (other.ebUserNum != null) return false;
        }
        else if (!ebUserNum.equals(other.ebUserNum)) return false;

        if (email == null) {
            if (other.email != null) return false;
        }
        else if (!email.equals(other.email)) return false;

        if (enabled == null) {
            if (other.enabled != null) return false;
        }
        else if (!enabled.equals(other.enabled)) return false;

        if (language == null) {
            if (other.language != null) return false;
        }
        else if (!language.equals(other.language)) return false;

        if (lastPasswordResetDate == null) {
            if (other.lastPasswordResetDate != null) return false;
        }
        else if (!lastPasswordResetDate.equals(other.lastPasswordResetDate)) return false;

        if (listAccessRights == null) {
            if (other.listAccessRights != null) return false;
        }
        else if (!listAccessRights.equals(other.listAccessRights)) return false;

        if (listCategories == null) {
            if (other.listCategories != null) return false;
        }
        else if (!listCategories.equals(other.listCategories)) return false;

        if (locale == null) {
            if (other.locale != null) return false;
        }
        else if (!locale.equals(other.locale)) return false;

        if (nom == null) {
            if (other.nom != null) return false;
        }
        else if (!nom.equals(other.nom)) return false;

        if (password == null) {
            if (other.password != null) return false;
        }
        else if (!password.equals(other.password)) return false;

        if (prenom == null) {
            if (other.prenom != null) return false;
        }
        else if (!prenom.equals(other.prenom)) return false;

        if (resetToken == null) {
            if (other.resetToken != null) return false;
        }
        else if (!resetToken.equals(other.resetToken)) return false;

        if (role == null) {
            if (other.role != null) return false;
        }
        else if (!role.equals(other.role)) return false;

        if (roleLibelle == null) {
            if (other.roleLibelle != null) return false;
        }
        else if (!roleLibelle.equals(other.roleLibelle)) return false;

        if (service == null) {
            if (other.service != null) return false;
        }
        else if (!service.equals(other.service)) return false;

        if (status == null) {
            if (other.status != null) return false;
        }
        else if (!status.equals(other.status)) return false;

        if (superAdmin != other.superAdmin) return false;

        if (telephone == null) {
            if (other.telephone != null) return false;
        }
        else if (!telephone.equals(other.telephone)) return false;

        return true;
    }

    public List<EbParamsMail> getListParamsMail() {
        return listParamsMail;
    }

    public void setListParamsMail(List<EbParamsMail> listParamsMail) {
        this.listParamsMail = listParamsMail;
    }

    public String getListParamsMailStr() {
        return listParamsMailStr;
    }

    public void setListParamsMailStr(String listParamsMailStr) {
        this.listParamsMailStr = listParamsMailStr;
    }

    public String getListLabels() {
        return listLabels;
    }

    public void setListLabels(String listLabels) {
        this.listLabels = listLabels;
    }

    public Boolean getCreatedByMe() {
        return createdByMe;
    }

    public void setCreatedByMe(Boolean createdByMe) {
        this.createdByMe = createdByMe;
    }

    public Boolean getExchangeRateFound() {
        return exchangeRateFound;
    }

    public void setExchangeRateFound(Boolean exchangeRateFound) {
        this.exchangeRateFound = exchangeRateFound;
    }

    private static final Logger log = LoggerFactory.getLogger(EbUser.class);

    public String getConnectedUserAuthToken() {
        return connectedUserAuthToken;
    }

    public void setConnectedUserAuthToken(String connectedUserAuthToken) {
        this.connectedUserAuthToken = connectedUserAuthToken;
    }

    public String getConnectedUsername() {
        return this.getRealUserNum() != null ? this.getRealUserNomPrenom() : this.getNomPrenom();
    }

    public String getGroupes() {
        return groupes;
    }

    public void setGroupes(String groupes) {
        this.groupes = groupes;
    }

    public String getDelegatedUsername() {
        return this.getRealUserNum() != null ? this.getNomPrenom() : null;
    }

    public Boolean getIsUnknownUser() {
        return isUnknownUser;
    }

    public void setIsUnknownUser(Boolean isUnknownUser) {
        this.isUnknownUser = isUnknownUser;
    }

    public String getEtablissementNom() {
        return etablissementNom;
    }

    public void setEtablissementNom(String etablissementNom) {
        this.etablissementNom = etablissementNom;
    }

    public String getLangageUser() {
        if (this.getLanguage() != null) return this.getLanguage();
        return "en";
    }

    public Integer getFuseauHoraire() {
        return fuseauHoraire;
    }

    public void setFuseauHoraire(Integer fuseauHoraire) {
        this.fuseauHoraire = fuseauHoraire;
    }

    public EbUserProfile getEbUserProfile() {
        return ebUserProfile;
    }

    public void setEbUserProfile(EbUserProfile ebUserProfile) {
        this.ebUserProfile = ebUserProfile;
    }

    public Boolean getSuperCt() {
        return superCt;
    }

    public Boolean isSuperCt() {
        return superCt != null && superCt;
    }

    public void setSuperCt(Boolean superCt) {
        this.superCt = superCt;
    }

    public EbUserTag getEbUserTag() {
        return ebUserTag;
    }

    public void setEbUserTag(EbUserTag ebUserTag) {
        this.ebUserTag = ebUserTag;
    }

    public Locale getLocaleFromLanguage() {
        return new Locale(getLangageUser());
    }

    public String getDateFormatFromLocale() {
        return Locale.ENGLISH.equals(getLocaleFromLanguage()) ? "MM-dd-yyyy HH:mm:ss" : "dd-MM-yyyy HH:mm:ss";
    }
}
