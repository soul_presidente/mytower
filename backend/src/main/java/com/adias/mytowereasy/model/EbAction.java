/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import javax.persistence.*;


@Entity
@Table(name = "eb_action", schema = "work")
public class EbAction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebActionNum;

    private String label;

    private String code;

    private Integer module;

    public Integer getEbActionNum() {
        return ebActionNum;
    }

    public void setEbActionNum(Integer ebActionNum) {
        this.ebActionNum = ebActionNum;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((ebActionNum == null) ? 0 : ebActionNum.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        result = prime * result + ((module == null) ? 0 : module.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbAction other = (EbAction) obj;

        if (code == null) {
            if (other.code != null) return false;
        }
        else if (!code.equals(other.code)) return false;

        if (ebActionNum == null) {
            if (other.ebActionNum != null) return false;
        }
        else if (!ebActionNum.equals(other.ebActionNum)) return false;

        if (label == null) {
            if (other.label != null) return false;
        }
        else if (!label.equals(other.label)) return false;

        if (module == null) {
            if (other.module != null) return false;
        }
        else if (!module.equals(other.module)) return false;

        return true;
    }
}
