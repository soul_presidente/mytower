package com.adias.mytowereasy.model;

import java.time.Duration;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.dto.TemplateGenParamsDTO;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.types.JsonBinaryType;


@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@NamedEntityGraph(name = "EbControlRule.detail", attributeNodes = @NamedAttributeNode("listRuleOperations"))
@JsonIgnoreProperties({
    "listRuleConditions"
})
@Table(name = "eb_control_rule", schema = "work")
public class EbControlRule extends EbTimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebControlRuleNum;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", nullable = false, updatable = false)
    private EbCompagnie xEbCompagnie;

    @OneToMany(mappedBy = "xEbControlRule", fetch = FetchType.LAZY)
    @JsonManagedReference
    private List<EbControlRuleOperation> listRuleOperations;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_type_request")
    private EbTypeRequest xEbTypeRequest;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_type_flux")
    private EbTypeFlux xEbTypeFlux;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_schema_psl")
    private EbTtSchemaPsl xEbSchemaPsl;

    private String code;

    private String libelle;

    private Boolean changeDocumentStatus;

    private Boolean genererIncident;

    private Boolean genererNotif;

    private Boolean envoyerEmail;

    private Boolean generateTr;

    private Boolean sendCotation;

    private Boolean exportControl;

    private Boolean generateDocument;

    private Boolean changeDemandesStatus;

    private Boolean sendEmailAlert;

    private String emailObjet;

    @Column(columnDefinition = "TEXT")
    private String emailMessage;

    @Column(columnDefinition = "TEXT")
    private String listTypeDocStatus;

    private String emailDestinataires;

    private Duration margin;

    private Integer priority;

    // incohérence, absence, ..
    private Integer ruleType;

    @ColumnDefault("true")
    private Boolean activated;

    // document ou date
    private Integer ruleCategory;

    private String triggers;

    private String commentaire;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private TemplateGenParamsDTO docGenParams;

    private Integer destStatus;

    private String selectedMailNums;

    public EbControlRule() {
    }

    @QueryProjection
    public EbControlRule(
        Integer ebControlRuleNum,
        Integer xEbCompagnie,
        String designationSchema,
        String codeTypeFlux,
        String referenceRequest,
        String libelleRequest,
        String code,
        String libelle,
        Boolean genererIncident,
        Boolean genererNotif,
        Boolean generateTr,
        Boolean changeDocumentStatus,
        Boolean envoyerEmail,
        String emailObjet,
        String emailMessage,
        String emailDestinataires,
        Duration margin,
        Integer priority,
        Integer ruleType,
        Integer ruleCategory,
        String listTypeDocStatus,
        Boolean activated,
        String commentaire,
        Boolean exportControl,
        Integer destStatus,
        Boolean changeDemandesStatus) {
        this.ebControlRuleNum = ebControlRuleNum;
        EbCompagnie compagnie = new EbCompagnie();
        compagnie.setEbCompagnieNum(xEbCompagnie);
        this.xEbCompagnie = compagnie;
        EbTypeRequest typeRequest = new EbTypeRequest();
        typeRequest.setLibelle(libelleRequest);
        typeRequest.setReference(referenceRequest);
        xEbTypeRequest = typeRequest;
        EbTypeFlux typeflux = new EbTypeFlux();
        typeflux.setCode(codeTypeFlux);
        xEbTypeFlux = typeflux;
        EbTtSchemaPsl schemaPSL = new EbTtSchemaPsl();
        schemaPSL.setCode(designationSchema);

        xEbSchemaPsl = schemaPSL;
        this.code = code;
        this.libelle = libelle;
        this.genererIncident = genererIncident;
        this.genererNotif = genererNotif;
        this.generateTr = generateTr;
        this.changeDocumentStatus = changeDocumentStatus;
        this.envoyerEmail = envoyerEmail;
        this.emailObjet = emailObjet;
        this.emailMessage = emailMessage;
        this.emailDestinataires = emailDestinataires;
        this.margin = margin;
        this.priority = priority;
        this.ruleType = ruleType;
        this.activated = activated;
        this.commentaire = commentaire;
        this.ruleCategory = ruleCategory;
        this.listTypeDocStatus = listTypeDocStatus;
        this.exportControl = exportControl;
        this.destStatus = destStatus;
        this.changeDemandesStatus = changeDemandesStatus;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public List<EbControlRuleOperation> getListRuleOperations() {
        return listRuleOperations;
    }

    public void setListRuleOperations(List<EbControlRuleOperation> listRuleOperations) {
        this.listRuleOperations = listRuleOperations;
    }

    public EbTypeRequest getxEbTypeRequest() {
        return xEbTypeRequest;
    }

    public void setxEbTypeRequest(EbTypeRequest xEbTypeRequest) {

        if (xEbTypeRequest != null) {
            this.xEbTypeRequest = new EbTypeRequest();
            this.xEbTypeRequest.setEbTypeRequestNum(xEbTypeRequest.getEbTypeRequestNum());
            this.xEbTypeRequest.setLibelle(xEbTypeRequest.getLibelle());
        }
        else this.xEbTypeRequest = null;

    }

    public EbTypeFlux getxEbTypeFlux() {
        return xEbTypeFlux;
    }

    public void setxEbTypeFlux(EbTypeFlux xEbTypeFlux) {

        if (xEbTypeFlux != null) {
            this.xEbTypeFlux = new EbTypeFlux();
            this.xEbTypeFlux.setEbTypeFluxNum(xEbTypeFlux.getEbTypeFluxNum());
            this.xEbTypeFlux.setCode(xEbTypeFlux.getCode());
            this.xEbTypeFlux.setDesignation(xEbTypeFlux.getDesignation());
        }
        else this.xEbTypeFlux = null;

    }

    public EbTtSchemaPsl getxEbSchemaPsl() {
        return xEbSchemaPsl;
    }

    public void setxEbSchemaPsl(EbTtSchemaPsl xEbSchemaPsl) {

        if (xEbSchemaPsl != null) {
            this.xEbSchemaPsl = new EbTtSchemaPsl();
            this.xEbSchemaPsl.setEbTtSchemaPslNum(xEbSchemaPsl.getEbTtSchemaPslNum());
            this.xEbSchemaPsl.setCode(xEbSchemaPsl.getCode());
        }
        else this.xEbSchemaPsl = null;

    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Boolean getGenererIncident() {
        return genererIncident;
    }

    public void setGenererIncident(Boolean genererIncident) {
        this.genererIncident = genererIncident;
    }

    public Boolean getGenererNotif() {
        return genererNotif;
    }

    public void setGenererNotif(Boolean genererNotif) {
        this.genererNotif = genererNotif;
    }

    public Boolean getEnvoyerEmail() {
        return envoyerEmail;
    }

    public void setEnvoyerEmail(Boolean envoyerEmail) {
        this.envoyerEmail = envoyerEmail;
    }

    public String getEmailObjet() {
        return emailObjet;
    }

    public void setEmailObjet(String emailObjet) {
        this.emailObjet = emailObjet;
    }

    public String getEmailMessage() {
        return emailMessage;
    }

    public void setEmailMessage(String emailMessage) {
        this.emailMessage = emailMessage;
    }

    public String getEmailDestinataires() {
        return emailDestinataires;
    }

    public void setEmailDestinataires(String emailDestinataires) {
        this.emailDestinataires = emailDestinataires;
    }

    public Integer getEbControlRuleNum() {
        return ebControlRuleNum;
    }

    public void setEbControlRuleNum(Integer ebControlRuleNum) {
        this.ebControlRuleNum = ebControlRuleNum;
    }

    public Duration getMargin() {
        return margin;
    }

    public Integer getRuleType() {
        return ruleType;
    }

    public void setRuleType(Integer ruleType) {
        this.ruleType = ruleType;
    }

    public void setMargin(Duration margin) {
        this.margin = margin;
    }

    // used when request is coming from the front app, type conversion is needed
    public void setMargin(String margin) {
        if (margin == null) return;

        String[] times = margin.split(":");
        Duration duration = Duration.ofDays(Integer.parseInt(times[0]));
        duration = duration.plusHours(Integer.parseInt(times[1]));
        duration = duration.plusMinutes(Integer.parseInt(times[2]));
        this.margin = duration;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Boolean getChangeDocumentStatus() {
        return changeDocumentStatus;
    }

    public void setChangeDocumentStatus(Boolean changeDocumentStatus) {
        this.changeDocumentStatus = changeDocumentStatus;
    }

    public String getListTypeDocStatus() {
        return listTypeDocStatus;
    }

    public void setListTypeDocStatus(String listTypeDocStatus) {
        this.listTypeDocStatus = listTypeDocStatus;
    }

    public Integer getRuleCategory() {
        return ruleCategory;
    }

    public void setRuleCategory(Integer ruleCategory) {
        this.ruleCategory = ruleCategory;
    }

    public String getTriggers() {
        return triggers;
    }

    public void setTriggers(String triggers) {
        this.triggers = triggers;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Boolean getGenerateTr() {
        return generateTr;
    }

    public void setGenerateTr(Boolean generateTr) {
        this.generateTr = generateTr;
    }

    public Boolean getSendCotation() {
        return sendCotation;
    }

    public void setSendCotation(Boolean sendCotation) {
        this.sendCotation = sendCotation;
    }

    public Boolean getExportControl() {
        return exportControl;
    }

    public void setExportControl(Boolean exportControl) {
        this.exportControl = exportControl;
    }

    public Boolean getGenerateDocument() {
        return generateDocument;
    }

    public void setGenerateDocument(Boolean generateDocument) {
        this.generateDocument = generateDocument;
    }

    public TemplateGenParamsDTO getDocGenParams() {
        return docGenParams;
    }

    public void setDocGenParams(Object docGenParams) {

        if (docGenParams != null) {

            try {
                ObjectMapper objectMapper = new ObjectMapper();

                String json = objectMapper.writeValueAsString(docGenParams);
                this.docGenParams = objectMapper.readValue(json, new TypeReference<TemplateGenParamsDTO>() {
                });
            } catch (Exception e) {
                System.err.println("ControlRule: failed to convert object to TemplateGenParamsDTO.");
            }

        }

    }

    public Integer getDestStatus() {
        return destStatus;
    }

    public void setDestStatus(Integer destStatus) {
        this.destStatus = destStatus;
    }

    public Boolean getChangeDemandesStatus() {
        return changeDemandesStatus;
    }

    public void setChangeDemandesStatus(Boolean changeStatus) {
        this.changeDemandesStatus = changeStatus;
    }

    public Boolean getSendEmailAlert() {
        return sendEmailAlert;
    }

    public void setSendEmailAlert(Boolean sendEmailAlert) {
        this.sendEmailAlert = sendEmailAlert;
    }

    public String getSelectedMailNums() {
        return selectedMailNums;
    }

    public void setSelectedMailNums(String selectedMailNums) {
        this.selectedMailNums = selectedMailNums;
    }
}
