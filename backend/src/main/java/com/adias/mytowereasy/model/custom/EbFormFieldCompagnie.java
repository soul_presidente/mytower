/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.custom;

import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.adias.mytowereasy.model.EbCompagnie;


@Table(name = "eb_form_field_compagnie")
@Entity
public class EbFormFieldCompagnie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebFormFieldCompagnieNum;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company", insertable = true, updatable = false)
    private EbCompagnie xEbCompagnie;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbFormField> listFormField;

    public Integer getEbFormFieldCompagnieNum() {
        return ebFormFieldCompagnieNum;
    }

    public void setEbFormFieldCompagnieNum(Integer ebFormFieldCompagnieNum) {
        this.ebFormFieldCompagnieNum = ebFormFieldCompagnieNum;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public List<EbFormField> getListFormField() {
        return listFormField;
    }

    public void setListFormField(List<EbFormField> listFormField) {
        this.listFormField = listFormField;
    }
}
