package com.adias.mytowereasy.model;

import java.util.List;


public class TransfertUserObject {
    private Integer ebUserNum;
    private Integer oldEtablissement;
    private List<EbCategorie> oldListCategories;
    private String oldListLabels;

    private Integer newEtablissement;
    private List<EbCategorie> newListCategories;
    private String newListLabels;

    private TransfertAction orders;
    private TransfertAction transportFiles;
    private TransfertAction community;

    public TransfertUserObject() {
    }

    public Integer getEbUserNum() {
        return ebUserNum;
    }

    public Integer getOldEtablissement() {
        return oldEtablissement;
    }

    public List<EbCategorie> getOldListCategories() {
        return oldListCategories;
    }

    public String getOldListLabels() {
        return oldListLabels;
    }

    public Integer getNewEtablissement() {
        return newEtablissement;
    }

    public List<EbCategorie> getNewListCategories() {
        return newListCategories;
    }

    public String getNewListLabels() {
        return newListLabels;
    }

    public TransfertAction getOrders() {
        return orders;
    }

    public TransfertAction getTransportFiles() {
        return transportFiles;
    }

    public void setEbUserNum(Integer ebUserNum) {
        this.ebUserNum = ebUserNum;
    }

    public void setOldEtablissement(Integer oldEtablissement) {
        this.oldEtablissement = oldEtablissement;
    }

    public void setOldListCategories(List<EbCategorie> oldListCategories) {
        this.oldListCategories = oldListCategories;
    }

    public void setOldListLabels(String oldListLabels) {
        this.oldListLabels = oldListLabels;
    }

    public void setNewEtablissement(Integer newEtablissement) {
        this.newEtablissement = newEtablissement;
    }

    public void setNewListCategories(List<EbCategorie> newListCategories) {
        this.newListCategories = newListCategories;
    }

    public void setNewListLabels(String newListLabels) {
        this.newListLabels = newListLabels;
    }

    public void setOrders(TransfertAction orders) {
        this.orders = orders;
    }

    public void setTransportFiles(TransfertAction transportFiles) {
        this.transportFiles = transportFiles;
    }

    public TransfertAction getCommunity() {
        return community;
    }

    public void setCommunity(TransfertAction community) {
        this.community = community;
    }
}
