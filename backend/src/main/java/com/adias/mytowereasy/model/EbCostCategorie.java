/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.adias.mytowereasy.types.CustomObjectIdResolver;


@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "ebCostCategorieNum",
    resolver = CustomObjectIdResolver.class,
    scope = EbCostCategorie.class)
@NamedEntityGraph(name = "allWithListEbCost", attributeNodes = {
    @NamedAttributeNode(value = "listEbCost")
})
@Entity
@Table(name = "eb_cost_categorie", schema = "work")
public class EbCostCategorie implements Serializable {
    private static final long serialVersionUID = 175454985628454009L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebCostCategorieNum;

    private String libelle;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "listCostCategorie", cascade = CascadeType.ALL)
    @JsonIgnoreProperties("listCostCategorie")
    private List<EbCost> listEbCost;

    private Integer xEcModeTransport;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private BigDecimal price;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private BigDecimal priceEuro;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Date dateCreation;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Double priceReal;
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Double priceEuroReal;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Date dateCreationReal;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Double priceGab;

    private String code;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", updatable = false)
    private EbCompagnie xEbCompagnie;

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Integer getEbCostCategorieNum() {
        return ebCostCategorieNum;
    }

    public void setEbCostCategorieNum(Integer ebCostCategorieNum) {
        this.ebCostCategorieNum = ebCostCategorieNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getxEcModeTransport() {
        return xEcModeTransport;
    }

    public void setxEcModeTransport(Integer xEcModeTransport) {
        this.xEcModeTransport = xEcModeTransport;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPriceEuro() {
        return priceEuro;
    }

    public void setPriceEuro(BigDecimal priceEuro) {
        this.priceEuro = priceEuro;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public List<EbCost> getListEbCost() {
        return listEbCost;
    }

    public void setListEbCost(List<EbCost> listEbCost) {
        this.listEbCost = listEbCost;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static String getCategorieCode(String fullCode) {
        String[] codes = fullCode.split("E");
        return codes != null && codes.length > 1 ? codes[0] : null;
    }

    public Double getPriceReal() {
        return priceReal;
    }

    public void setPriceReal(Double priceReal) {
        this.priceReal = priceReal;
    }

    public Double getPriceEuroReal() {
        return priceEuroReal;
    }

    public void setPriceEuroReal(Double priceEuroReal) {
        this.priceEuroReal = priceEuroReal;
    }

    public Date getDateCreationReal() {
        return dateCreationReal;
    }

    public void setDateCreationReal(Date dateCreationReal) {
        this.dateCreationReal = dateCreationReal;
    }

    public Double getPriceGab() {
        return priceGab;
    }

    public void setPriceGab(Double priceGab) {
        this.priceGab = priceGab;
    }
}
