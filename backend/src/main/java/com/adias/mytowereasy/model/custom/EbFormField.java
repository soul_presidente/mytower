/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.custom;

import javax.persistence.*;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.querydsl.core.annotations.QueryProjection;


@Table(name = "eb_form_field")
@Entity
public class EbFormField {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebFormFieldNum;

    private String name;
    private String code;
    private Boolean visibilite;
    private Integer ordre;
    private String motTechnique;
    private String translate;
    private String valueAvailable;
    private String srcListItems;
    private String fetchAttribute;
    private String fetchValue;
    private Boolean required;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String valeurUnit;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "xEbFormType", insertable = true, updatable = false)
    private EbFormType xEbFormType;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "xEbViewForm", insertable = true, updatable = false)
    private EbFormView xEbViewForm;

    public EbFormField() {
    }

    @QueryProjection
    public EbFormField(
        Integer ebFormFieldNum,
        String name,
        String code,
        Boolean visibilite,
        Integer ordre,
        Integer xEbFormType,
        String xEbFormTypeName,
        String motTechnique,
        String translate,
        String valueAvailable,
        Integer xEbViewForm,
        String xEbViewFormName,
        Integer EbFormZone,
        String EbFormZoneName,
        String srcListItems,
        String fetchAttribute,
        String fetchValue) {
        this.ebFormFieldNum = ebFormFieldNum;
        this.name = name;
        this.code = code;
        this.visibilite = visibilite;
        this.ordre = ordre;
        this.motTechnique = motTechnique;
        this.translate = translate;
        this.valueAvailable = valueAvailable;
        this.srcListItems = srcListItems;
        this.fetchAttribute = fetchAttribute;
        this.fetchValue = fetchValue;
        EbFormZone formZone = new EbFormZone();
        formZone.setEbFormZoneNum(EbFormZone);
        formZone.setEbFormName(EbFormZoneName);
        this.xEbFormType = new EbFormType(xEbFormType, xEbFormTypeName);
        this.xEbViewForm = new EbFormView(xEbViewForm, xEbViewFormName);
        this.xEbViewForm.setxEbFormZone(formZone);
    }

    public Integer getEbFormFieldNum() {
        return ebFormFieldNum;
    }

    public void setEbFormFieldNum(Integer ebFormFieldNum) {
        this.ebFormFieldNum = ebFormFieldNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public EbFormType getxEbFormType() {
        return xEbFormType;
    }

    public void setxEbFormType(EbFormType xEbFormType) {
        this.xEbFormType = xEbFormType;
    }

    public EbFormView getxEbViewForm() {
        return xEbViewForm;
    }

    public void setxEbViewForm(EbFormView xEbViewForm) {
        this.xEbViewForm = xEbViewForm;
    }

    public Integer getOrdre() {
        return ordre;
    }

    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    public String getMotTechnique() {
        return motTechnique;
    }

    public void setMotTechnique(String motTechnique) {
        this.motTechnique = motTechnique;
    }

    public String getTranslate() {
        return translate;
    }

    public void setTranslate(String translate) {
        this.translate = translate;
    }

    public String getValueAvailable() {
        return valueAvailable;
    }

    public void setValueAvailable(String valueAvailable) {
        this.valueAvailable = valueAvailable;
    }

    public String getValeurUnit() {
        return valeurUnit;
    }

    public void setValeurUnit(String valeurUnit) {
        this.valeurUnit = valeurUnit;
    }

    public String getSrcListItems() {
        return srcListItems;
    }

    public void setSrcListItems(String srcListItems) {
        this.srcListItems = srcListItems;
    }

    public String getFetchAttribute() {
        return fetchAttribute;
    }

    public void setFetchAttribute(String fetchAttribute) {
        this.fetchAttribute = fetchAttribute;
    }

    public String getFetchValue() {
        return fetchValue;
    }

    public void setFetchValue(String fetchValue) {
        this.fetchValue = fetchValue;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }
}
