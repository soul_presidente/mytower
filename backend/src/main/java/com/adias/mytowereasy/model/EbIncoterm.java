package com.adias.mytowereasy.model;

import javax.persistence.*;


@Entity
@Table(name = "eb_incoterm", schema = "work")
public class EbIncoterm {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebIncotermNum;

    private String code;

    private String libelle;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", updatable = false)
    private EbCompagnie xEbCompagnie;

    public Integer getEbIncotermNum() {
        return ebIncotermNum;
    }

    public String getCode() {
        return code;
    }

    public String getLibelle() {
        return libelle;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setEbIncotermNum(Integer ebIncotermNum) {
        this.ebIncotermNum = ebIncotermNum;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }
}
