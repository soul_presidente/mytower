/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.querydsl.core.annotations.QueryProjection;


// @Entity
// @Table(name = "eb_demande_commande")
public class ExDemandeCommande implements Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @EmbeddedId
    private ExDemandeCommandeId exDemandeCommandeId;

    private Date dateCreation;

    public ExDemandeCommande() {
        super();
        this.exDemandeCommandeId = new ExDemandeCommandeId();
    }

    // @Embeddable
    public static class ExDemandeCommandeId implements Serializable {
        /** */
        private static final long serialVersionUID = 1L;

        @ManyToOne
        @JoinColumn(name = "x_eb_demande")
        private EbDemande xEbDemande;

        @ManyToOne
        @JoinColumn(name = "x_eb_commande")
        private EbCommande xEbCommande;

        public EbDemande getxEbDemande() {
            return xEbDemande;
        }

        public void setxEbDemande(EbDemande xEbDemande) {
            this.xEbDemande = xEbDemande;
        }

        public EbCommande getxEbCommande() {
            return xEbCommande;
        }

        public void setxEbCommande(EbCommande xEbCommande) {
            this.xEbCommande = xEbCommande;
        }

        public ExDemandeCommandeId() {
        }

        public ExDemandeCommandeId(EbDemande xEbDemande, EbCommande xEbCommande) {
            super();
            this.xEbDemande = xEbDemande;
            this.xEbCommande = xEbCommande;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((xEbCommande == null) ? 0 : xEbCommande.hashCode());
            result = prime * result + ((xEbDemande == null) ? 0 : xEbDemande.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) return true;
            if (obj == null) return false;
            if (getClass() != obj.getClass()) return false;
            ExDemandeCommandeId other = (ExDemandeCommandeId) obj;

            if (xEbCommande == null) {
                if (other.xEbCommande != null) return false;
            }
            else if (!xEbCommande.equals(other.xEbCommande)) return false;

            if (xEbDemande == null) {
                if (other.xEbDemande != null) return false;
            }
            else if (!xEbDemande.equals(other.xEbDemande)) return false;

            return true;
        }
    }

    @QueryProjection
    public ExDemandeCommande(
        Integer ebCommandeNum,
        String customerReference,
        EcCountry xEcCountry,
        Date dateDeparture,
        Date deleveryDate,
        Double totalWeight,
        Double totalUnits) {
        this.exDemandeCommandeId = new ExDemandeCommandeId();
        this.exDemandeCommandeId.xEbCommande = new EbCommande();
        /*
         * this.exDemandeCommandeId.xEbCommande.setCustomerReference(
         * customerReference);
         * this.exDemandeCommandeId.xEbCommande.setEbCommandeNum(ebCommandeNum);
         * this.exDemandeCommandeId.xEbCommande.setxEcCountry(xEcCountry);
         * this.exDemandeCommandeId.xEbCommande.setDateDeparture(dateDeparture);
         * this.exDemandeCommandeId.xEbCommande.setDeliveryDate(deleveryDate);
         * this.exDemandeCommandeId.xEbCommande.setTotalWeight(totalWeight);
         * this.exDemandeCommandeId.xEbCommande.setTotalUnits(totalUnits);
         */
    }

    public ExDemandeCommandeId getExDemandeCommandeId() {
        return exDemandeCommandeId;
    }

    public void setExDemandeCommandeId(ExDemandeCommandeId exDemandeCommandeId) {
        this.exDemandeCommandeId = exDemandeCommandeId;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public EbDemande getXEbDemande() {
        return exDemandeCommandeId.xEbDemande;
    }

    public EbCommande getXEbCommande() {
        return exDemandeCommandeId.xEbCommande;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        result = prime * result + ((exDemandeCommandeId == null) ? 0 : exDemandeCommandeId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        ExDemandeCommande other = (ExDemandeCommande) obj;

        if (dateCreation == null) {
            if (other.dateCreation != null) return false;
        }
        else if (!dateCreation.equals(other.dateCreation)) return false;

        if (exDemandeCommandeId == null) {
            if (other.exDemandeCommandeId != null) return false;
        }
        else if (!exDemandeCommandeId.equals(other.exDemandeCommandeId)) return false;

        return true;
    }
}
