/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.TypeDef;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.adias.mytowereasy.types.JsonBinaryType;


@Table(name = "eb_demande_fichiers_joint")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@JsonIgnoreProperties({
    "xEbCustomDeclaration"
})
public class EbDemandeFichiersJoint implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = 5836762455423795446L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebDemandeFichierJointNum;

    @ManyToOne
    @JoinColumn(name = "x_eb_user")
    private EbUser xEbUser;

    private Integer xEbCompagnie;
    private Integer xEbDemande;
    private Integer xEbCustomDeclaration;
    private Integer numEntity;

    private Integer xEbEtablissemnt;

    private String idCategorie;
    private String chemin;
    private String fileName;
    private String originFileName;

    @Column(name = "mimetype")
    private String mimeType;

    private String tag;
    private String pslEvent;
    private Integer module;

    @Temporal(TemporalType.TIMESTAMP)
    private Date attachmentDate;

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public Integer getEbDemandeFichierJointNum() {
        return ebDemandeFichierJointNum;
    }

    public void setEbDemandeFichierJointNum(Integer ebDemandeFichierJointNum) {
        this.ebDemandeFichierJointNum = ebDemandeFichierJointNum;
    }

    public Integer getxEbEtablissemnt() {
        return xEbEtablissemnt;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public void setxEbEtablissemnt(Integer xEbEtablissemnt) {
        this.xEbEtablissemnt = xEbEtablissemnt;
    }

    public String getIdCategorie() {
        return idCategorie;
    }

    public void setIdCategorie(String idCategorie) {
        this.idCategorie = idCategorie;
    }

    public String getChemin() {
        return chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = chemin;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getOriginFileName() {
        return originFileName;
    }

    public void setOriginFileName(String originFileName) {
        this.originFileName = originFileName;
    }

    public Date getAttachmentDate() {
        return attachmentDate;
    }

    public void setAttachmentDate(Date attachmentDate) {
        this.attachmentDate = attachmentDate;
    }

    public EbUser getxEbUser() {
        return xEbUser;
    }

    public void setxEbUser(EbUser xEbUser) {
        this.xEbUser = xEbUser;
    }

    public Integer getNumEntity() {
        return numEntity;
    }

    public void setNumEntity(Integer numEntity) {
        this.numEntity = numEntity;
    }

    public Integer getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(Integer xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public String getPslEvent() {
        return pslEvent;
    }

    public void setPslEvent(String pslEvent) {
        this.pslEvent = pslEvent;
    }

    private Integer xEbMarchandise;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String refTransportAndUnit;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((attachmentDate == null) ? 0 : attachmentDate.hashCode());
        result = prime * result + ((chemin == null) ? 0 : chemin.hashCode());
        result = prime * result + ((ebDemandeFichierJointNum == null) ? 0 : ebDemandeFichierJointNum.hashCode());
        result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
        result = prime * result + ((idCategorie == null) ? 0 : idCategorie.hashCode());
        result = prime * result + ((originFileName == null) ? 0 : originFileName.hashCode());
        result = prime * result + ((numEntity == null) ? 0 : numEntity.hashCode());
        result = prime * result + ((xEbUser == null) ? 0 : xEbUser.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbDemandeFichiersJoint other = (EbDemandeFichiersJoint) obj;

        if (attachmentDate == null) {
            if (other.attachmentDate != null) return false;
        }
        else if (!attachmentDate.equals(other.attachmentDate)) return false;

        if (chemin == null) {
            if (other.chemin != null) return false;
        }
        else if (!chemin.equals(other.chemin)) return false;

        if (ebDemandeFichierJointNum == null) {
            if (other.ebDemandeFichierJointNum != null) return false;
        }
        else if (!ebDemandeFichierJointNum.equals(other.ebDemandeFichierJointNum)) return false;

        if (fileName == null) {
            if (other.fileName != null) return false;
        }
        else if (!fileName.equals(other.fileName)) return false;

        if (idCategorie == null) {
            if (other.idCategorie != null) return false;
        }
        else if (!idCategorie.equals(other.idCategorie)) return false;

        if (originFileName == null) {
            if (other.originFileName != null) return false;
        }
        else if (!originFileName.equals(other.originFileName)) return false;

        if (numEntity == null) {
            if (other.numEntity != null) return false;
        }
        else if (!numEntity.equals(other.numEntity)) return false;

        if (xEbUser == null) {
            if (other.xEbUser != null) return false;
        }
        else if (!xEbUser.equals(other.xEbUser)) return false;

        return true;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getxEbDemande() {
        return xEbDemande;
    }

    public void setxEbDemande(Integer xEbDemande) {
        this.xEbDemande = xEbDemande;
    }

    public Integer getxEbCustomDeclaration() {
        return xEbCustomDeclaration;
    }

    public void setxEbCustomDeclaration(Integer xEbCustomDeclaration) {
        this.xEbCustomDeclaration = xEbCustomDeclaration;
    }

    public Integer getxEbMarchandise() {
        return xEbMarchandise;
    }

    public void setxEbMarchandise(Integer xEbMarchandise) {
        this.xEbMarchandise = xEbMarchandise;
    }

    public String getRefTransportAndUnit() {
        return refTransportAndUnit;
    }

    public void setRefTransportAndUnit(String refTransportAndUnit) {
        this.refTransportAndUnit = refTransportAndUnit;
    }
}
