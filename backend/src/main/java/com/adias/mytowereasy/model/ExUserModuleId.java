/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;

import javax.persistence.Embeddable;


@Embeddable
public class ExUserModuleId implements Serializable {
    /** */
    private static final long serialVersionUID = -5151623672236886745L;

    private Integer ecModuleNum;

    private Integer ebUserNum;

    // public int hashCode() {
    // return (int)(ecModuleNum + ebUserNum);
    // }

    // public boolean equals(Object object) {
    // if (object instanceof ExUserModuleId) {
    // ExUserModuleId otherId = (ExUserModuleId) object;
    // return (otherId.ebUserNum == this.ebUserNum) && (otherId.ecModuleNum ==
    // this.ecModuleNum);
    // }
    // return false;
    // }
}
