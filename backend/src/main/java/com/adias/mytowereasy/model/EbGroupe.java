/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;

import javax.persistence.*;


@Entity
@Table(name = "eb_groupe", schema = "work")
public class EbGroupe implements Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebGroupeNum;

    @Column(nullable = false)
    private String nom;

    public Integer getEbGroupeNum() {
        return ebGroupeNum;
    }

    public void setEbGroupeNum(Integer ebGroupeNum) {
        this.ebGroupeNum = ebGroupeNum;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ebGroupeNum == null) ? 0 : ebGroupeNum.hashCode());
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbGroupe other = (EbGroupe) obj;

        if (ebGroupeNum == null) {
            if (other.ebGroupeNum != null) return false;
        }
        else if (!ebGroupeNum.equals(other.ebGroupeNum)) return false;

        if (nom == null) {
            if (other.nom != null) return false;
        }
        else if (!nom.equals(other.nom)) return false;

        return true;
    }
}
