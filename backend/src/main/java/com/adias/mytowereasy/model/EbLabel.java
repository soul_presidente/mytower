/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "eb_label", indexes = {
    @Index(name = "idx_eb_label_x_eb_categorie", columnList = "x_eb_categorie"),
    @Index(name = "idx_eb_label_x_eb_group_user", columnList = "x_eb_group_user")
})
public class EbLabel implements Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebLabelNum;

    private String libelle;

    private String source;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_categorie", insertable = true, updatable = false)
    private EbCategorie categorie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_group_user")
    private EbUserGroup group;

    @Column
    @ColumnDefault("false")
    private Boolean deleted;

    public Integer getEbLabelNum() {
        return ebLabelNum;
    }

    public void setEbLabelNum(Integer ebLabelNum) {
        this.ebLabelNum = ebLabelNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public EbCategorie getCategorie() {
        return categorie;
    }

    public void setCategorie(EbCategorie categorie) {
        this.categorie = categorie;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((categorie == null) ? 0 : categorie.hashCode());
        result = prime * result + ((ebLabelNum == null) ? 0 : ebLabelNum.hashCode());
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbLabel other = (EbLabel) obj;

        if (categorie == null) {
            if (other.categorie != null) return false;
        }
        else if (!categorie.equals(other.categorie)) return false;

        if (ebLabelNum == null) {
            if (other.ebLabelNum != null) return false;
        }
        else if (!ebLabelNum.equals(other.ebLabelNum)) return false;

        if (libelle == null) {
            if (other.libelle != null) return false;
        }
        else if (!libelle.equals(other.libelle)) return false;

        return true;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public EbUserGroup getGroup() {
        return group;
    }

    public void setGroup(EbUserGroup group) {
        this.group = group;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}
