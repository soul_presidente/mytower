/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import javax.persistence.*;


@Entity
@Table(name = "ec_module", schema = "work")
public class EcModule implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ec_module_num")
    private Integer ecModuleNum;

    @Column
    private String libelle;

    @Column
    private Integer ordre;

    public EcModule() {
    }

    public EcModule(Integer ecModuleNum) {
        this.ecModuleNum = ecModuleNum;
    }

    public Integer getEcModuleNum() {
        return ecModuleNum;
    }

    public void setEcModuleNum(Integer ecModuleNum) {
        this.ecModuleNum = ecModuleNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Integer getOrdre() {
        return ordre;
    }

    public void setOrdre(Integer ordre) {
        this.ordre = ordre;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ecModuleNum == null) ? 0 : ecModuleNum.hashCode());
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EcModule other = (EcModule) obj;

        if (ecModuleNum == null) {
            if (other.ecModuleNum != null) return false;
        }
        else if (!ecModuleNum.equals(other.ecModuleNum)) return false;

        if (libelle == null) {
            if (other.libelle != null) return false;
        }
        else if (!libelle.equals(other.libelle)) return false;

        return true;
    }
}
