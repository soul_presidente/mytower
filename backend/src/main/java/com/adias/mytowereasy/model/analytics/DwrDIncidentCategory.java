/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.analytics;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "dwr_d_incident_category")
public class DwrDIncidentCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dwr_d_incident_category_num")
    private Integer dwrDIncidentCategoryNum;

    @Column(name = "label")
    private String label;

    @Column(name = "code")
    private String code;

    @OneToMany(mappedBy = "incident", cascade = CascadeType.ALL)
    private List<DwrFShipmentsIncident> shipments = new ArrayList<DwrFShipmentsIncident>();

    public Integer getID() {
        return this.dwrDIncidentCategoryNum;
    }

    public String getLabel() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.label;
    }

    public void setLabel(String value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.label = value;
    }

    public String getCode() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.code;
    }

    public void setCode(String value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.code = value;
    }

    public List<DwrFShipmentsIncident> getShipments() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.shipments;
    }

    public void setShipments(List<DwrFShipmentsIncident> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.shipments = value;
    }
}
