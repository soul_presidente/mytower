package com.adias.mytowereasy.model.enums;

import com.adias.mytowereasy.util.GenericEnum;


public class PlanTransEnumeration {
    public enum WeekType implements GenericEnum {
        NORMAL(0, "NORMAL"), SPECIAL(1, "SPECIAL");

        private Integer code;
        private String key;

        private WeekType(Integer code, String key) {
            this.code = code;
            this.key = key;
        }

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }
    }
}
