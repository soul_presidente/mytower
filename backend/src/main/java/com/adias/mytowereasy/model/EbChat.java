/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.TypeDef;

import com.adias.mytowereasy.types.JsonBinaryType;


@Table(name = "eb_chat")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class EbChat implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = -1021460914257092635L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebChatNum;

    private Integer idFiche;
    private Integer idChatComponent;
    private Integer module;

    private String userName;
    private String userAvatar;

    @Column(columnDefinition = "TEXT")
    private String text;

    @ColumnDefault("now()")
    private Date dateCreation;

    private Boolean isHistory;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.DETACH)
    @JoinColumn(name = "x_chat_owner", updatable = false)
    private EbUser chatOwner;

    public EbChat() {
    }

    public EbChat(
        Integer ebChatNum,
        Integer idFiche,
        Integer idChatComponent,
        Integer module,
        String userName,
        String userAvatar,
        String text,
        Date dateCreation,
        Boolean isHistory,
        EbUser chatOwner) {
        this.ebChatNum = ebChatNum;
        this.idFiche = idFiche;
        this.idChatComponent = idChatComponent;
        this.module = module;
        this.userName = userName;
        this.userAvatar = userAvatar;
        this.text = text;
        this.dateCreation = dateCreation;
        this.isHistory = isHistory;
        this.chatOwner = chatOwner;
    }

    public EbChat(EbUser chatOwner) {
        this.chatOwner = new EbUser(chatOwner.getEbUserNum());
    }

    public Integer getIdChatComponent() {
        return idChatComponent;
    }

    public void setIdChatComponent(Integer idChatComponent) {
        this.idChatComponent = idChatComponent;
    }

    public Integer getEbChatNum() {
        return ebChatNum;
    }

    public void setEbChatNum(Integer ebChatNum) {
        this.ebChatNum = ebChatNum;
    }

    public Integer getIdFiche() {
        return idFiche;
    }

    public void setIdFiche(Integer idFiche) {
        this.idFiche = idFiche;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Boolean getIsHistory() {
        return isHistory;
    }

    public void setIsHistory(Boolean isHistory) {
        this.isHistory = isHistory;
    }

    public EbUser getChatOwner() {
        return chatOwner;
    }

    public void setChatOwner(EbUser chatOwner) {
        this.chatOwner = chatOwner;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        result = prime * result + ((ebChatNum == null) ? 0 : ebChatNum.hashCode());
        result = prime * result + ((idChatComponent == null) ? 0 : idChatComponent.hashCode());
        result = prime * result + ((idFiche == null) ? 0 : idFiche.hashCode());
        result = prime * result + ((isHistory == null) ? 0 : isHistory.hashCode());
        result = prime * result + ((module == null) ? 0 : module.hashCode());
        result = prime * result + ((text == null) ? 0 : text.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbChat other = (EbChat) obj;

        if (dateCreation == null) {
            if (other.dateCreation != null) return false;
        }
        else if (!dateCreation.equals(other.dateCreation)) return false;

        if (ebChatNum == null) {
            if (other.ebChatNum != null) return false;
        }
        else if (!ebChatNum.equals(other.ebChatNum)) return false;

        if (idChatComponent == null) {
            if (other.idChatComponent != null) return false;
        }
        else if (!idChatComponent.equals(other.idChatComponent)) return false;

        if (idFiche == null) {
            if (other.idFiche != null) return false;
        }
        else if (!idFiche.equals(other.idFiche)) return false;

        if (isHistory == null) {
            if (other.isHistory != null) return false;
        }
        else if (!isHistory.equals(other.isHistory)) return false;

        if (module == null) {
            if (other.module != null) return false;
        }
        else if (!module.equals(other.module)) return false;

        if (text == null) {
            if (other.text != null) return false;
        }
        else if (!text.equals(other.text)) return false;

        return true;
    }
}
