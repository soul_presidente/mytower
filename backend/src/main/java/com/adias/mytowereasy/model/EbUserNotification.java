package com.adias.mytowereasy.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ColumnDefault;


@Entity
@Table(name = "eb_user_notification")
public class EbUserNotification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebUserNotificationNum;

    @ColumnDefault("0")
    private Integer nbrAlerts;

    @ColumnDefault("0")
    private Integer nbrNotifys;

    @ColumnDefault("0")
    private Integer nbrMsgs;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user")
    private EbUser ebUser;

    public Integer getEbUserNotificationNum() {
        return ebUserNotificationNum;
    }

    public void setEbUserNotificationNum(Integer ebUserNotificationNum) {
        this.ebUserNotificationNum = ebUserNotificationNum;
    }

    public Integer getNbrAlerts() {
        return nbrAlerts;
    }

    public void setNbrAlerts(Integer nbrAlerts) {
        this.nbrAlerts = nbrAlerts;
    }

    public Integer getNbrNotifys() {
        return nbrNotifys;
    }

    public void setNbrNotifys(Integer nbrNotifys) {
        this.nbrNotifys = nbrNotifys;
    }

    public Integer getNbrMsgs() {
        return nbrMsgs;
    }

    public void setNbrMsgs(Integer nbrMsgs) {
        this.nbrMsgs = nbrMsgs;
    }

    public EbUser getEbUser() {
        return ebUser;
    }

    public void setEbUser(EbUser ebUser) {
        this.ebUser = ebUser;
    }
}
