package com.adias.mytowereasy.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.airbus.model.EbUserProfile;


@Entity
@Table(name = "eb_acl_relation", schema = "work", indexes = {
    @Index(name = "idx_eb_acl_relation_rule", columnList = "x_ec_acl_rule"),
    @Index(name = "idx_eb_acl_relation_profile", columnList = "x_eb_user_profile"),
    @Index(name = "idx_eb_acl_relation_user", columnList = "x_eb_user")
})
public class EbAclRelation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebAclRelationNum;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_acl_rule")
    private EcAclRule rule;

    private String value;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_profile")
    private EbUserProfile profile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user")
    private EbUser user;

    public EbAclRelation() {
        // empty constructor
    }

    /**
     * Projection used to retrieve only required field for session
     */
    @QueryProjection
    public EbAclRelation(Integer ebAclRelationNum, String value, Integer ecAclRuleNum, String ruleCode) {
        this.ebAclRelationNum = ebAclRelationNum;
        this.value = value;

        if (ecAclRuleNum != null) {
            this.rule = new EcAclRule();
            this.rule.setEcAclRuleNum(ecAclRuleNum);
            this.rule.setCode(ruleCode);
        }

    }

    public Integer getEbAclRelationNum() {
        return ebAclRelationNum;
    }

    public void setEbAclRelationNum(Integer ebAclRelationNum) {
        this.ebAclRelationNum = ebAclRelationNum;
    }

    public EcAclRule getRule() {
        return rule;
    }

    public void setRule(EcAclRule rule) {
        this.rule = rule;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public EbUserProfile getProfile() {
        return profile;
    }

    public void setProfile(EbUserProfile profile) {
        this.profile = profile;
    }

    public EbUser getUser() {
        return user;
    }

    public void setUser(EbUser user) {
        this.user = user;
    }
}
