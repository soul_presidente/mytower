/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.custom;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.cptm.model.EbCptmRegimeTemporaire;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbTimeStamps;
import com.adias.mytowereasy.model.EbTypeDocuments;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCountry;
import com.adias.mytowereasy.model.EcCurrency;


@Entity
@Table(name = "eb_custom_declaration")
public class EbCustomDeclaration extends EbTimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebCustomDeclarationNum;

    private String refDemandeDedouanement;

    private String idDedouanement;

    private Date demandeDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_demandeur", insertable = true, updatable = false)
    private EbUser demandeur;

    private Integer operation;

    private String projet;

    private String codeBAU;

    private String eori;

    private String typeDeFlux;

    private String sitePlant;

    private String destinataire;

    private String villeIncoterm;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_charger", insertable = true, updatable = false)
    private EbUser charger;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissementcharger", insertable = true, updatable = false)
    private EbEtablissement etablissementCharger;

    private String nomFournisseur;

    private Integer xEcModeTransport;

    private String immatriculation;

    private Date dateArrivalEsstimed;

    private String airportPortOfArrival;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country_provenances", insertable = true, updatable = false)
    private EcCountry xEcCountryProvenances;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_agent_broker", insertable = true, updatable = false)
    private EbUser agentBroker;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissementbroker", insertable = true, updatable = false)
    private EbEtablissement etablissementbroker;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_country_dedouanement", insertable = true, updatable = false)
    private EcCountry xEcCountryDedouanement;

    private String lieuDedouanement;

    private String biensDoubleUsage;

    private String licenceDoubleUsage;

    private String transit;

    private String bureauDeTransit;

    private String warGoods;

    private String warGoodLicenceReference;

    private String customOffice;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_carrier", insertable = true, updatable = false)
    private EbUser carrier;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissementcarrier", insertable = true, updatable = false)
    private EbEtablissement etablissementCarrier;

    private String nomTransporteur;

    private String telephoneTransporteur;

    private String nomContactTransporteur;

    private String emailTransporteur;

    private Integer freightPart;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_freight_part_currency")
    private EcCurrency freightPartCurrency;

    private Integer insuranceCost;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_insurance_cost_currency")
    private EcCurrency insuranceCostCurrency;

    private Double invoiceAmount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_invoice_currency")
    private EcCurrency invoiceCurrency;

    private Double toolingCost;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_tooling_cost_currency")
    private EcCurrency toolingCostCurrency;

    private Double totalFreightCost;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_total_freight_cost_currency")
    private EcCurrency totalFreightCostCurrency;

    private Integer numTva;

    private String traficAtBorder;

    private String importerOfRecord;

    private String delay;

    private String comment;

    private Double grossWeight;

    private String grossWeightUnit;

    private Integer numberOfPack;

    private Integer numberOfLines;

    private Integer totalQuantity;

    private Integer valueAmountTotal;

    private String business;

    private String liquidation;

    private String euCode;

    private String hsCode;

    private String partNumber;

    private String procedure;

    private String quantity;

    private Integer valueAmount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_ec_value_currency")
    private EcCurrency valueCurrency;

    private Double exchangeRate;

    private Date dateUpload;

    private String typeDoc;

    private String numberOrReference1;

    private String numberOrReference2;

    private String attroneyText;

    private String cancellationReason;

    private Date dateCancellation;

    private String customRegime;
    private String leg;
    private Integer nbOfTemporaryRegime;
    private Date dateEcs;
    private Date dateDeclaration;
    private Integer statut;
    private String declarationNumber;
    private String declarationReference;

    private Integer attachedFilesCount;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbTypeDocuments> listTypeDocuments;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_form_field_compagnie", insertable = true, updatable = false)
    private EbFormFieldCompagnie xEbFormFieldCompagnie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_declarant")
    private EbUser xEbUserDeclarant;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user_ct")
    private EbUser xEbUserCt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement_ct")
    private EbEtablissement xEbEtablissementCt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie_ct")
    private EbCompagnie xEbCompagnieCt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_demande", insertable = true, updatable = false)
    private EbDemande xEbDemande;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_cptm_regime_temporaire", insertable = true, updatable = false)
    private EbCptmRegimeTemporaire xEbCptmRegimeTemporaire;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<List<EbFormField>> listUnit;

    @JsonIgnore
    private String listEbDemandeFichiersJointNum;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company", insertable = true)
    private EbCompagnie ebCompagnie;

    public EbCustomDeclaration() {
    }

    @QueryProjection
    public EbCustomDeclaration(
        Integer ebCustomDeclarationNum,
        String declarationReference,
        String ref,
        String idDedouanement,
        Date demandeDate,
        Integer demandeur,
        String nomDemandeur,
        String prenomDemandeur,
        Integer operation,
        String projet,
        String codeBAU,
        String EORI,
        String typeDeFlux,
        String sitePlant,
        String destinataire,
        String villeIncoterm,
        String NomFournisseur,
        Integer xEcModeTransport,
        String immatriculation,
        Date dateArrivalEsstimed,
        String airportPortOfArrival,
        Integer xEcCountryProvenances,
        String libelleCountryProvenances,
        Integer agentBroker,
        String agentNom,
        Integer xEcCountryDedouanement,
        String libelleCountryDedouanement,
        String lieuDedouanement,
        String biensDoubleUsage,
        String licenceDoubleUsage,
        String Transit,
        String bureauDeTransit,
        String warGoods,
        String warGoodLicenceReference,
        String customOffice,
        String nomTransporteur,
        String telephoneTransporteur,
        String nomContactTransporteur,
        String emailTransporteur,
        Integer freightPart,
        Integer freightPartCurrency,
        String libellefreightPartCurrency,
        Integer insuranceCost,
        Integer insuranceCostCurrency,
        String libelleInsuranceCostCurrency,
        Double invoiceAmount,
        Integer invoiceCurrency,
        String libelleInvoiceCurrency,
        Double toolingCost,
        Integer toolingCostCurrency,
        String libelleToolingCostCurrency,
        Double totalFreightCost,
        Integer totalFreightCostCurrency,
        String libelleTotalFreightCostCurrency,
        Integer numTva,
        String traficAtBorder,
        String importerOfRecord,
        String delay,
        String comment,
        Double grossWeight,
        String grossWeightUnit,
        Integer numberOfPack,
        Integer numberOfLines,
        Integer totalQuantity,
        Integer valueAmountTotal,
        String business,
        String hsCode,
        String partNumber,
        String procedure,
        String quantity,
        Integer valueAmount,
        Integer valueCurrency,
        String libelleValueCurrency,
        Double exchangeRate,
        Date dateUpload,
        String typeDoc,
        String numberOrReference1,
        String numberOrReference2,
        String attroneyText,
        String cancellationReason,
        Date dateCancellation,
        String customRegime,
        Integer chargerNum,
        String ChargerNom,
        String ChargerPrenomm,
        Integer etablissementCharger,
        String etablissementChargerNom,
        Integer carrierNum,
        String carrierNom,
        String carrierPrenomm,
        Integer etablissementcarrier,
        String etablissementcarrierNom,
        Integer etablissementBroker,
        String etablissementBrokerNom,
        List<List<EbFormField>> listUnit,
        String leg,
        Integer nbOfTemporaryRegime,
        Date dateEcs,
        Date dateDeclaration,
        Integer statut,
        String declarationNumber,
        Integer declarantUserNum,
        String declarantNom,
        String declarantPrenom,
        Integer attachedFilesCount,
        Integer demandeNum,
        String demandeRef,
        Timestamp dateCreationSys) {
        this.declarationReference = declarationReference;
        this.leg = leg;
        this.nbOfTemporaryRegime = nbOfTemporaryRegime;
        this.dateEcs = dateEcs;
        this.dateDeclaration = dateDeclaration;
        this.statut = statut;
        this.declarationNumber = declarationNumber;
        this.ebCustomDeclarationNum = ebCustomDeclarationNum;
        this.refDemandeDedouanement = ref;
        this.idDedouanement = idDedouanement;
        this.demandeDate = demandeDate;
        this.demandeur = new EbUser(demandeur, nomDemandeur, prenomDemandeur);
        this.operation = operation;
        this.projet = projet;
        this.codeBAU = codeBAU;
        this.eori = EORI;
        this.typeDeFlux = typeDeFlux;
        this.sitePlant = sitePlant;
        this.destinataire = destinataire;
        this.villeIncoterm = villeIncoterm;
        this.nomFournisseur = NomFournisseur;
        this.xEcModeTransport = xEcModeTransport;
        this.immatriculation = immatriculation;
        this.dateArrivalEsstimed = dateArrivalEsstimed;
        this.airportPortOfArrival = airportPortOfArrival;
        this.xEcCountryProvenances = new EcCountry(xEcCountryProvenances);
        this.xEcCountryProvenances.setLibelle(libelleCountryProvenances);
        this.agentBroker = new EbUser(agentBroker);
        this.agentBroker.setNom(agentNom);
        this.xEcCountryDedouanement = new EcCountry(xEcCountryDedouanement);
        this.xEcCountryDedouanement.setLibelle(libelleCountryDedouanement);
        this.lieuDedouanement = lieuDedouanement;
        this.biensDoubleUsage = biensDoubleUsage;
        this.licenceDoubleUsage = licenceDoubleUsage;
        this.transit = Transit;
        this.bureauDeTransit = bureauDeTransit;
        this.warGoods = warGoods;
        this.warGoodLicenceReference = warGoodLicenceReference;
        this.customOffice = customOffice;
        this.nomTransporteur = nomTransporteur;
        this.telephoneTransporteur = telephoneTransporteur;
        this.nomContactTransporteur = nomContactTransporteur;
        this.emailTransporteur = emailTransporteur;
        this.freightPart = freightPart;
        this.freightPartCurrency = new EcCurrency(freightPartCurrency);
        this.freightPartCurrency.setCode(libellefreightPartCurrency);
        this.insuranceCost = insuranceCost;
        this.insuranceCostCurrency = new EcCurrency(insuranceCostCurrency, libelleInsuranceCostCurrency);
        this.invoiceAmount = invoiceAmount;
        this.invoiceCurrency = new EcCurrency(invoiceCurrency, libelleInvoiceCurrency);
        this.toolingCost = toolingCost;
        this.toolingCostCurrency = new EcCurrency(toolingCostCurrency, libelleToolingCostCurrency);
        this.totalFreightCost = totalFreightCost;
        this.totalFreightCostCurrency = new EcCurrency(totalFreightCostCurrency, libelleTotalFreightCostCurrency);
        this.numTva = numTva;
        this.traficAtBorder = traficAtBorder;
        this.importerOfRecord = importerOfRecord;
        this.delay = delay;
        this.comment = comment;
        this.grossWeight = grossWeight;
        this.grossWeightUnit = grossWeightUnit;
        this.numberOfPack = numberOfPack;
        this.numberOfLines = numberOfLines;
        this.totalQuantity = totalQuantity;
        this.valueAmountTotal = valueAmountTotal;
        this.business = business;
        this.hsCode = hsCode;
        this.partNumber = partNumber;
        this.procedure = procedure;
        this.quantity = quantity;
        this.valueAmount = valueAmount;
        this.valueCurrency = new EcCurrency(valueCurrency, libelleValueCurrency);
        this.exchangeRate = exchangeRate;
        this.dateUpload = dateUpload;
        this.typeDoc = typeDoc;
        this.numberOrReference1 = numberOrReference1;
        this.numberOrReference2 = numberOrReference2;
        this.attroneyText = attroneyText;
        this.cancellationReason = cancellationReason;
        this.dateCancellation = dateCancellation;
        this.customRegime = customRegime;
        this.charger = new EbUser(chargerNum, ChargerNom, ChargerPrenomm);
        this.etablissementCharger = new EbEtablissement(etablissementCharger, etablissementChargerNom);
        this.carrier = new EbUser(carrierNum, carrierNom, carrierPrenomm);
        this.etablissementCarrier = new EbEtablissement(etablissementcarrier, etablissementcarrierNom);

        this.etablissementbroker = new EbEtablissement(etablissementBroker, etablissementcarrierNom);
        this.listUnit = listUnit;
        this.attachedFilesCount = attachedFilesCount;
        this.dateCreationSys = dateCreationSys;

        if (declarantUserNum != null) {
            this.xEbUserDeclarant = new EbUser(declarantUserNum);
            this.xEbUserDeclarant.setNom(declarantNom);
            this.xEbUserDeclarant.setPrenom(declarantPrenom);
        }

        if (demandeNum != null) {
            this.xEbDemande = new EbDemande();
            this.xEbDemande.setEbDemandeNum(demandeNum);
            this.xEbDemande.setRefTransport(demandeRef);
        }

    }

    @QueryProjection
    public EbCustomDeclaration(Integer ebCustomDeclarationNum, String idDedouanement, String villeIncoterm) {
        this.ebCustomDeclarationNum = ebCustomDeclarationNum;
        this.idDedouanement = idDedouanement;
        this.villeIncoterm = villeIncoterm;
    }

    public String getRef() {
        return refDemandeDedouanement;
    }

    public void setRef(String ref) {
        this.refDemandeDedouanement = ref;
    }

    public EbUser getDemandeur() {
        return demandeur;
    }

    public void setDemandeur(EbUser demandeur) {
        this.demandeur = demandeur;
    }

    public Integer getOperation() {
        return operation;
    }

    public void setOperation(Integer operation) {
        this.operation = operation;
    }

    public String getProjet() {
        return projet;
    }

    public void setProjet(String projet) {
        this.projet = projet;
    }

    public String getCodeBAU() {
        return codeBAU;
    }

    public void setCodeBAU(String codeBAU) {
        this.codeBAU = codeBAU;
    }

    public String getTypeDeFlux() {
        return typeDeFlux;
    }

    public void setTypeDeFlux(String typeDeFlux) {
        this.typeDeFlux = typeDeFlux;
    }

    public String getSitePlant() {
        return sitePlant;
    }

    public void setSitePlant(String sitePlant) {
        this.sitePlant = sitePlant;
    }

    public EbFormFieldCompagnie getxEbFormFieldCompagnie() {
        return xEbFormFieldCompagnie;
    }

    public void setxEbFormFieldCompagnie(EbFormFieldCompagnie xEbFormFieldCompagnie) {
        this.xEbFormFieldCompagnie = xEbFormFieldCompagnie;
    }

    public String getRefDemandeDedouanement() {
        return refDemandeDedouanement;
    }

    public void setRefDemandeDedouanement(String refDemandeDedouanement) {
        this.refDemandeDedouanement = refDemandeDedouanement;
    }

    public String getIdDedouanement() {
        return idDedouanement;
    }

    public void setIdDedouanement(String idDedouanement) {
        this.idDedouanement = idDedouanement;
    }

    public String getEori() {
        return eori;
    }

    public void setEori(String eori) {
        this.eori = eori;
    }

    public String getDestinataire() {
        return destinataire;
    }

    public void setDestinataire(String destinataire) {
        this.destinataire = destinataire;
    }

    public String getVilleIncoterm() {
        return villeIncoterm;
    }

    public void setVilleIncoterm(String villeIncoterm) {
        this.villeIncoterm = villeIncoterm;
    }

    public Integer getxEcModeTransport() {
        return xEcModeTransport;
    }

    public void setxEcModeTransport(Integer xEcModeTransport) {
        this.xEcModeTransport = xEcModeTransport;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public void setImmatriculation(String immatriculation) {
        this.immatriculation = immatriculation;
    }

    public Date getDateArrivalEsstimed() {
        return dateArrivalEsstimed;
    }

    public void setDateArrivalEsstimed(Date dateArrivalEsstimed) {
        this.dateArrivalEsstimed = dateArrivalEsstimed;
    }

    public String getAirportPortOfArrival() {
        return airportPortOfArrival;
    }

    public void setAirportPortOfArrival(String airportPortOfArrival) {
        this.airportPortOfArrival = airportPortOfArrival;
    }

    public EcCountry getxEcCountryProvenances() {
        return xEcCountryProvenances;
    }

    public void setxEcCountryProvenances(EcCountry xEcCountryProvenances) {
        this.xEcCountryProvenances = xEcCountryProvenances;
    }

    public EbUser getAgentBroker() {
        return agentBroker;
    }

    public void setAgentBroker(EbUser agentBroker) {
        this.agentBroker = agentBroker;
    }

    public EcCountry getxEcCountryDedouanement() {
        return xEcCountryDedouanement;
    }

    public void setxEcCountryDedouanement(EcCountry xEcCountryDedouanement) {
        this.xEcCountryDedouanement = xEcCountryDedouanement;
    }

    public String getLieuDedouanement() {
        return lieuDedouanement;
    }

    public void setLieuDedouanement(String lieuDedouanement) {
        this.lieuDedouanement = lieuDedouanement;
    }

    public String getBiensDoubleUsage() {
        return biensDoubleUsage;
    }

    public void setBiensDoubleUsage(String biensDoubleUsage) {
        this.biensDoubleUsage = biensDoubleUsage;
    }

    public String getLicenceDoubleUsage() {
        return licenceDoubleUsage;
    }

    public void setLicenceDoubleUsage(String licenceDoubleUsage) {
        this.licenceDoubleUsage = licenceDoubleUsage;
    }

    public String getTransit() {
        return transit;
    }

    public void setTransit(String transit) {
        this.transit = transit;
    }

    public String getBureauDeTransit() {
        return bureauDeTransit;
    }

    public void setBureauDeTransit(String bureauDeTransit) {
        this.bureauDeTransit = bureauDeTransit;
    }

    public String getWarGoods() {
        return warGoods;
    }

    public void setWarGoods(String warGoods) {
        this.warGoods = warGoods;
    }

    public String getWarGoodLicenceReference() {
        return warGoodLicenceReference;
    }

    public void setWarGoodLicenceReference(String warGoodLicenceReference) {
        this.warGoodLicenceReference = warGoodLicenceReference;
    }

    public String getCustomOffice() {
        return customOffice;
    }

    public void setCustomOffice(String customOffice) {
        this.customOffice = customOffice;
    }

    public String getNomTransporteur() {
        return nomTransporteur;
    }

    public void setNomTransporteur(String nomTransporteur) {
        this.nomTransporteur = nomTransporteur;
    }

    public String getTelephoneTransporteur() {
        return telephoneTransporteur;
    }

    public void setTelephoneTransporteur(String telephoneTransporteur) {
        this.telephoneTransporteur = telephoneTransporteur;
    }

    public String getNomContactTransporteur() {
        return nomContactTransporteur;
    }

    public void setNomContactTransporteur(String nomContactTransporteur) {
        this.nomContactTransporteur = nomContactTransporteur;
    }

    public String getEmailTransporteur() {
        return emailTransporteur;
    }

    public void setEmailTransporteur(String emailTransporteur) {
        this.emailTransporteur = emailTransporteur;
    }

    public Integer getFreightPart() {
        return freightPart;
    }

    public void setFreightPart(Integer freightPart) {
        this.freightPart = freightPart;
    }

    public EcCurrency getFreightPartCurrency() {
        return freightPartCurrency;
    }

    public void setFreightPartCurrency(EcCurrency freightPartCurrency) {
        this.freightPartCurrency = freightPartCurrency;
    }

    public Integer getInsuranceCost() {
        return insuranceCost;
    }

    public void setInsuranceCost(Integer insuranceCost) {
        this.insuranceCost = insuranceCost;
    }

    public EcCurrency getInsuranceCostCurrency() {
        return insuranceCostCurrency;
    }

    public void setInsuranceCostCurrency(EcCurrency insuranceCostCurrency) {
        this.insuranceCostCurrency = insuranceCostCurrency;
    }

    public Double getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(Double invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public EcCurrency getInvoiceCurrency() {
        return invoiceCurrency;
    }

    public void setInvoiceCurrency(EcCurrency invoiceCurrency) {
        this.invoiceCurrency = invoiceCurrency;
    }

    public Double getToolingCost() {
        return toolingCost;
    }

    public void setToolingCost(Double toolingCost) {
        this.toolingCost = toolingCost;
    }

    public EcCurrency getToolingCostCurrency() {
        return toolingCostCurrency;
    }

    public void setToolingCostCurrency(EcCurrency toolingCostCurrency) {
        this.toolingCostCurrency = toolingCostCurrency;
    }

    public Double getTotalFreightCost() {
        return totalFreightCost;
    }

    public void setTotalFreightCost(Double totalFreightCost) {
        this.totalFreightCost = totalFreightCost;
    }

    public EcCurrency getTotalFreightCurrency() {
        return totalFreightCostCurrency;
    }

    public void setTotalFreightCurrency(EcCurrency totalFreightCurrency) {
        this.totalFreightCostCurrency = totalFreightCurrency;
    }

    public Integer getNumTva() {
        return numTva;
    }

    public void setNumTva(Integer numTva) {
        this.numTva = numTva;
    }

    public String getTraficAtBorder() {
        return traficAtBorder;
    }

    public void setTraficAtBorder(String traficAtBorder) {
        this.traficAtBorder = traficAtBorder;
    }

    public String getImporterOfRecord() {
        return importerOfRecord;
    }

    public void setImporterOfRecord(String importerOfRecord) {
        this.importerOfRecord = importerOfRecord;
    }

    public String getDelay() {
        return delay;
    }

    public void setDelay(String delay) {
        this.delay = delay;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(Double grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String getGrossWeightUnit() {
        return grossWeightUnit;
    }

    public void setGrossWeightUnit(String grossWeightUnit) {
        this.grossWeightUnit = grossWeightUnit;
    }

    public Integer getNumberOfPack() {
        return numberOfPack;
    }

    public void setNumberOfPack(Integer numberOfPack) {
        this.numberOfPack = numberOfPack;
    }

    public Integer getNumberOfLines() {
        return numberOfLines;
    }

    public void setNumberOfLines(Integer numberOfLines) {
        this.numberOfLines = numberOfLines;
    }

    public Integer getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(Integer totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public Integer getValueAmountTotal() {
        return valueAmountTotal;
    }

    public void setValueAmountTotal(Integer valueAmountTotal) {
        this.valueAmountTotal = valueAmountTotal;
    }

    public String getBusiness() {
        return business;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getProcedure() {
        return procedure;
    }

    public void setProcedure(String procedure) {
        this.procedure = procedure;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Integer getValueAmount() {
        return valueAmount;
    }

    public void setValueAmount(Integer valueAmount) {
        this.valueAmount = valueAmount;
    }

    public EcCurrency getValueCurrency() {
        return valueCurrency;
    }

    public void setValueCurrency(EcCurrency valueCurrency) {
        this.valueCurrency = valueCurrency;
    }

    public Double getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(Double exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public Date getDateUpload() {
        return dateUpload;
    }

    public void setDateUpload(Date dateUpload) {
        this.dateUpload = dateUpload;
    }

    public String getTypeDoc() {
        return typeDoc;
    }

    public void setTypeDoc(String typeDoc) {
        this.typeDoc = typeDoc;
    }

    public String getNumberOrReference1() {
        return numberOrReference1;
    }

    public void setNumberOrReference1(String numberOrReference1) {
        this.numberOrReference1 = numberOrReference1;
    }

    public String getNumberOrReference2() {
        return numberOrReference2;
    }

    public void setNumberOrReference2(String numberOrReference2) {
        this.numberOrReference2 = numberOrReference2;
    }

    public String getAttroneyText() {
        return attroneyText;
    }

    public void setAttroneyText(String attroneyText) {
        this.attroneyText = attroneyText;
    }

    public String getCancellationReason() {
        return cancellationReason;
    }

    public void setCancellationReason(String cancellationReason) {
        this.cancellationReason = cancellationReason;
    }

    public Date getDateCancellation() {
        return dateCancellation;
    }

    public void setDateCancellation(Date dateCancellation) {
        this.dateCancellation = dateCancellation;
    }

    public String getCustomRegime() {
        return customRegime;
    }

    public void setCustomRegime(String customRegime) {
        this.customRegime = customRegime;
    }

    public Date getDateDemande() {
        return demandeDate;
    }

    public void setDateDemande(Date dateDemande) {
        this.demandeDate = dateDemande;
    }

    public EcCurrency getTotalFreightCostCurrency() {
        return totalFreightCostCurrency;
    }

    public void setTotalFreightCostCurrency(EcCurrency totalFreightCostCurrency) {
        this.totalFreightCostCurrency = totalFreightCostCurrency;
    }

    public Date getDemandeDate() {
        return demandeDate;
    }

    public void setDemandeDate(Date demandeDate) {
        this.demandeDate = demandeDate;
    }

    public EbUser getCharger() {
        return charger;
    }

    public void setCharger(EbUser charger) {
        this.charger = charger;
    }

    public EbEtablissement getEtablissementCharger() {
        return etablissementCharger;
    }

    public void setEtablissementCharger(EbEtablissement etablissementCharger) {
        this.etablissementCharger = etablissementCharger;
    }

    public EbUser getCarrier() {
        return carrier;
    }

    public void setCarrier(EbUser carrier) {
        this.carrier = carrier;
    }

    public EbEtablissement getEtablissementCarrier() {
        return etablissementCarrier;
    }

    public void setEtablissementCarrier(EbEtablissement etablissementCarrier) {
        this.etablissementCarrier = etablissementCarrier;
    }

    public String getLiquidation() {
        return liquidation;
    }

    public void setLiquidation(String liquidation) {
        this.liquidation = liquidation;
    }

    public String getEuCode() {
        return euCode;
    }

    public void setEuCode(String euCode) {
        this.euCode = euCode;
    }

    public EbEtablissement getEtablissementbroker() {
        return etablissementbroker;
    }

    public void setEtablissementbroker(EbEtablissement etablissementbroker) {
        this.etablissementbroker = etablissementbroker;
    }

    public EbUser getxEbUserCt() {
        return xEbUserCt;
    }

    public void setxEbUserCt(EbUser xEbUserCt) {
        this.xEbUserCt = xEbUserCt;
    }

    public EbEtablissement getxEbEtablissementCt() {
        return xEbEtablissementCt;
    }

    public void setxEbEtablissementCt(EbEtablissement xEbEtablissementCt) {
        this.xEbEtablissementCt = xEbEtablissementCt;
    }

    public EbCompagnie getxEbCompagnieCt() {
        return xEbCompagnieCt;
    }

    public void setxEbCompagnieCt(EbCompagnie xEbCompagnieCt) {
        this.xEbCompagnieCt = xEbCompagnieCt;
    }

    public String getNomFournisseur() {
        return nomFournisseur;
    }

    public void setNomFournisseur(String nomFournisseur) {
        this.nomFournisseur = nomFournisseur;
    }

    public EbDemande getxEbDemande() {
        return xEbDemande;
    }

    public void setxEbDemande(EbDemande xEbDemande) {
        this.xEbDemande = xEbDemande;
    }

    public List<List<EbFormField>> getListUnit() {
        return listUnit;
    }

    public void setListUnit(List<List<EbFormField>> listUnit) {
        this.listUnit = listUnit;
    }

    public Integer getEbCustomDeclarationNum() {
        return ebCustomDeclarationNum;
    }

    public void setEbCustomDeclarationNum(Integer ebCustomDeclarationNum) {
        this.ebCustomDeclarationNum = ebCustomDeclarationNum;
    }

    public String getLeg() {
        return leg;
    }

    public void setLeg(String leg) {
        this.leg = leg;
    }

    public Integer getNbOfTemporaryRegime() {
        return nbOfTemporaryRegime;
    }

    public void setNbOfTemporaryRegime(Integer nbOfTemporaryRegime) {
        this.nbOfTemporaryRegime = nbOfTemporaryRegime;
    }

    public Date getDateEcs() {
        return dateEcs;
    }

    public void setDateEcs(Date dateEcs) {
        this.dateEcs = dateEcs;
    }

    public Date getDateDeclaration() {
        return dateDeclaration;
    }

    public void setDateDeclaration(Date dateDeclaration) {
        this.dateDeclaration = dateDeclaration;
    }

    public Integer getStatut() {
        return statut;
    }

    public void setStatut(Integer statut) {
        this.statut = statut;
    }

    public String getDeclarationNumber() {
        return declarationNumber;
    }

    public void setDeclarationNumber(String declarationNumber) {
        this.declarationNumber = declarationNumber;
    }

    public String getDeclarationReference() {
        return declarationReference;
    }

    public void setDeclarationReference(String declarationReference) {
        this.declarationReference = declarationReference;
    }

    public EbUser getxEbUserDeclarant() {
        return xEbUserDeclarant;
    }

    public void setxEbUserDeclarant(EbUser xEbUserDeclarant) {
        this.xEbUserDeclarant = xEbUserDeclarant;
    }

    public EbCptmRegimeTemporaire getxEbCptmRegimeTemporaire() {
        return xEbCptmRegimeTemporaire;
    }

    public void setxEbCptmRegimeTemporaire(EbCptmRegimeTemporaire xEbCptmRegimeTemporaire) {
        this.xEbCptmRegimeTemporaire = xEbCptmRegimeTemporaire;
    }

    public String getListEbDemandeFichiersJointNum() {
        return listEbDemandeFichiersJointNum;
    }

    public void setListEbDemandeFichiersJointNum(String listEbDemandeFichiersJointNum) {
        this.listEbDemandeFichiersJointNum = listEbDemandeFichiersJointNum;
    }

    public List<EbTypeDocuments> getListTypeDocuments() {
        return listTypeDocuments;
    }

    public void setListTypeDocuments(List<EbTypeDocuments> listTypeDocuments) {
        this.listTypeDocuments = listTypeDocuments;
    }

    public Integer getAttachedFilesCount() {
        return attachedFilesCount;
    }

    public void setAttachedFilesCount(Integer attachedFilesCount) {
        this.attachedFilesCount = attachedFilesCount;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }
}
