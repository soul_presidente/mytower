package com.adias.mytowereasy.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "eb_type_flux", schema = "work")
public class EbTypeFlux extends EbTimeStamps implements Serializable {

	private static final long	serialVersionUID	= 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer						ebTypeFluxNum;

    private String designation;

    private String code;

    private String listModeTransport;

    private String listEbTtSchemaPsl;

    private String listEbTypeDocuments;

    private String listEbIncoterm;

    private Boolean isDeleted;

    private Integer trackingLevel;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", updatable = false)
    private EbCompagnie xEbCompagnie;

    public Integer getEbTypeFluxNum() {
        return ebTypeFluxNum;
    }

    public String getDesignation() {
        return designation;
    }

    public String getListModeTransport() {
        return listModeTransport;
    }

    public String getListEbTtSchemaPsl() {
        return listEbTtSchemaPsl;
    }

    public String getListEbTypeDocuments() {
        return listEbTypeDocuments;
    }

    public String getListEbIncoterm() {
        return listEbIncoterm;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setEbTypeFluxNum(Integer ebTypeFluxNum) {
        this.ebTypeFluxNum = ebTypeFluxNum;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public void setListModeTransport(String listModeTransport) {
        this.listModeTransport = listModeTransport;
    }

    public void setListEbTtSchemaPsl(String listEbTtSchemaPsl) {
        this.listEbTtSchemaPsl = listEbTtSchemaPsl;
    }

    public void setListEbTypeDocuments(String listEbTypeDocuments) {
        this.listEbTypeDocuments = listEbTypeDocuments;
    }

    public void setListEbIncoterm(String listEbIncoterm) {
        this.listEbIncoterm = listEbIncoterm;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }

    public Boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getTrackingLevel() {
        return trackingLevel;
    }

    public void setTrackingLevel(Integer trackingLevel) {
        this.trackingLevel = trackingLevel;
    }
}
