/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedSubgraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.dock.model.EbEntrepot;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.types.CustomObjectIdResolver;


@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "ebEtablissementNum",
    resolver = CustomObjectIdResolver.class,
    scope = EbEtablissement.class)
@NamedEntityGraph(name = "EbEtablissement.detail", attributeNodes = {
    @NamedAttributeNode(value = "categories", subgraph = "graph.categories.labels")
}, subgraphs = @NamedSubgraph(name = "graph.categories.labels", attributeNodes = @NamedAttributeNode("labels")))
@Entity
@JsonIgnoreProperties(value = {
    "hibernateLazyInitializer", "handler"
}, ignoreUnknown = true)
@Table(name = "eb_etablissement", schema = "work", indexes = {
    @Index(name = "idx_eb_etablissement_x_ec_country", columnList = "x_ec_country"),
    @Index(name = "idx_eb_etablissement_x_eb_compagnie", columnList = "x_eb_compagnie"),
    @Index(name = "idx_eb_etablissement_x_eb_schema_psl", columnList = "x_eb_schema_psl")
})
public class EbEtablissement extends EbTimeStamps implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "eb_etablissement_num", nullable = false, unique = true)
    private Integer ebEtablissementNum;

    @Column(name = "type_etablissement")
    private String typeEtablissement;

    @Column(name = "siret", unique = true, nullable = false)
    private String siret;

    @Column(name = "nom")
    private String nom;

    @Column(name = "logo")
    private String logo;

    @Column(name = "email")
    private String email;

    @Column(name = "date_creation")
    @Temporal(TemporalType.DATE)
    private Date dateCreation;

    @Column(name = "telephone")
    private String telephone;

    @Column(name = "nmbr_utilisateurs")
    private Integer nmbrUtilisateurs;

    @Column(name = "adresse")
    private String adresse;

    @Column(name = "info")
    private String info;

    @Column(name = "code")
    private String code;

    @Column
    @ColumnDefault("true")
    private Boolean canShowPrice;

    @ManyToOne(cascade = {
        CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH
    })
    @JoinColumn(name = "x_eb_compagnie", updatable = false)
    private EbCompagnie ebCompagnie;

    @Column(name = "cout_prestation_douane")
    private Double coutPrestationDouane;

    private String city;

    // @JsonView(value = SummaryJsonView.class)
    @ManyToOne()
    @JoinColumn(name = "x_ec_country")
    private EcCountry ecCountry;

    @Column(name = "site_web")
    private String siteWeb;

    // Transporteur / Broker
    private Integer service;

    private Integer role;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "etablissement")
    private Set<EbCategorie> categories;

    @OneToMany(mappedBy = "ebEtablissement", fetch = FetchType.LAZY)
    private Set<EbCompagnieCurrency> exEtablissementCurrency;

    private String nic;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "etablissement")
    private Set<EbEntrepot> listEntrepots;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "xEbEtablissementHost")
    private Set<EbRelation> etablissementsHost;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "xEbEtablissement")
    private Set<EbRelation> etablissementsGuests;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_schema_psl", insertable = true, updatable = false)
    private EbTtSchemaPsl schemaPsl;

    public Set<EbEntrepot> getListEntrepots() {
        return listEntrepots;
    }

    public void setListEntrepots(Set<EbEntrepot> listEntrepots) {
        this.listEntrepots = listEntrepots;
    }

    public EbEtablissement() {
    }

    public EbEtablissement(
        Integer ebEtablissementNum,
        String nom,
        String adresse,
        String telephone,
        String email,
        Integer service) {
        this.ebEtablissementNum = ebEtablissementNum;
        this.nom = nom;
        this.adresse = adresse;
        this.telephone = telephone;
        this.email = email;
        this.service = service;
    }

    public EbEtablissement(Integer ebEtablissementNum) {
        this.ebEtablissementNum = ebEtablissementNum;
    }

    public EbEtablissement(Integer ebEtablissementNum, Boolean canShowPrice) {
        this.ebEtablissementNum = ebEtablissementNum;
        this.canShowPrice = canShowPrice;
    }

    public EbEtablissement(Integer ebEtablissementNum, String nom, Boolean canShowPrice) {
        this.ebEtablissementNum = ebEtablissementNum;
        this.nom = nom;
        this.canShowPrice = canShowPrice;
    }

    public EbEtablissement(Integer ebEtablissementNum, String nom) {
        this.ebEtablissementNum = ebEtablissementNum;
        this.nom = nom;
    }

    public EbEtablissement(Integer ebEtablissementNum, String code, String nom) {
        this.ebEtablissementNum = ebEtablissementNum;
        this.code = code;
        this.nom = nom;
    }

    @QueryProjection
    public EbEtablissement(
        Integer ebEtablissementNum,
        String nom,
        String adresse,
        String telephone,
        String email,
        Integer service,
        Integer role,
        String siteWeb,
        String typeEtablissement,
        Integer ebCompagnieNum,
        String nomCompagnie,
        String code) {
        this.ebEtablissementNum = ebEtablissementNum;
        this.nom = nom;
        this.adresse = adresse;
        this.telephone = telephone;
        this.email = email;
        this.service = service;
        this.role = role;
        this.siteWeb = siteWeb;
        this.typeEtablissement = typeEtablissement;

        this.ebCompagnie = new EbCompagnie();
        this.ebCompagnie.setEbCompagnieNum(ebCompagnieNum);
        this.ebCompagnie.setNom(nomCompagnie);
        this.ebCompagnie.setCode(code);
    }

    @QueryProjection
    public EbEtablissement(
        Integer ebEtablissementNum,
        String nom,
        String adresse,
        String telephone,
        String email,
        Integer service,
        Integer role,
        Integer ebCompagnieNum,
        String nomCompagnie,
        String code,
        Boolean canShowPrice,
        String city,
        String nic,
        String siteWeb,
        String typeEtablissement,
        String codeLibelle,
        Boolean commercialMandatory,
        Double latitude,
        Double longitude,
        String libelle,
        Integer ecCountryNum,
        Integer ecRegionNum,
        Integer ecOperationalGroupingNum

    ) {
        this.ebEtablissementNum = ebEtablissementNum;
        this.nom = nom;
        this.adresse = adresse;
        this.telephone = telephone;
        this.email = email;
        this.service = service;
        this.role = role;
        this.ebCompagnie = new EbCompagnie();
        this.ebCompagnie.setEbCompagnieNum(ebCompagnieNum);
        this.ebCompagnie.setNom(nomCompagnie);
        this.ebCompagnie.setCode(code);
        this.canShowPrice = canShowPrice;
        this.city = city;
        this.nic = nic;
        this.siteWeb = siteWeb;
        this.typeEtablissement = typeEtablissement;
        this.ecCountry = new EcCountry();
        this.ecCountry.setCode(code);
        this.ecCountry.setCodeLibelle(codeLibelle);
        this.ecCountry.setCommercialMandatory(commercialMandatory);
        this.ecCountry.setLatitude(latitude);
        this.ecCountry.setLongitude(longitude);
        this.ecCountry.setLibelle(libelle);
        this.ecCountry.setEcCountryNum(ecCountryNum);
        EcRegion reg = new EcRegion();
        reg.setEcRegionNum(ecRegionNum);
        this.ecCountry.setEcRegion(reg);
        EcOperationalGrouping ecOperationalGrouping = new EcOperationalGrouping();
        ecOperationalGrouping.setEcOperationalGroupingNum(ecOperationalGroupingNum);
        this.ecCountry.setEcOperationalGrouping(ecOperationalGrouping);
    }

    public Integer getEbEtablissementNum() {
        return ebEtablissementNum;
    }

    public void setEbEtablissementNum(Integer ebEtablissementNum) {
        this.ebEtablissementNum = ebEtablissementNum;
    }

    public String getSiret() {
        return siret;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Integer getNmbrUtilisateurs() {
        return nmbrUtilisateurs;
    }

    public void setNmbrUtilisateurs(Integer nmbrUtilisateurs) {
        this.nmbrUtilisateurs = nmbrUtilisateurs;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    // public EbCompagnie getEbCompagnie(){
    //
    // EbCompagnie compagnie = new EbCompagnie();
    // compagnie.setEbCompagnieNum(this.ebCompagnie.getEbCompagnieNum());
    // compagnie.setNom(this.ebCompagnie.getNom());
    // compagnie.setSiren(this.ebCompagnie.getSiren());
    //
    // return compagnie;
    // }

    public EbCompagnie getEbCompagnie() {
        return this.ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    // public void setEbCompagnie(EbCompagnie a) {
    // setEbCompagnie(a, true);
    // }
    //
    // void setEbCompagnie(EbCompagnie ebCompagnie, boolean add) {
    // this.ebCompagnie = ebCompagnie;
    // if (ebCompagnie != null && add) {
    // ebCompagnie.addB(this, false);
    // }
    // }

    public Double getCoutPrestationDouane() {
        return coutPrestationDouane;
    }

    public void setCoutPrestationDouane(Double coutPrestationDouane) {
        this.coutPrestationDouane = coutPrestationDouane;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public EcCountry getEcCountry() {
        return ecCountry;
    }

    public void setEcCountry(EcCountry ecCountry) {
        this.ecCountry = ecCountry;
    }

    public String getSiteWeb() {
        return siteWeb;
    }

    public void setSiteWeb(String siteWeb) {
        this.siteWeb = siteWeb;
    }

    public Integer getService() {
        return service;
    }

    public void setService(Integer service) {
        this.service = service;
    }

    public Set<EbCategorie> getCategories() {
        return categories;
    }

    public void setCategories(Set<EbCategorie> categories) {
        this.categories = categories;
    }

    public Boolean getCanShowPrice() {
        return canShowPrice;
    }

    public void setCanShowPrice(Boolean canShowPrice) {
        this.canShowPrice = canShowPrice;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((adresse == null) ? 0 : adresse.hashCode());
        // result = prime * result + ((categories == null) ? 0 :
        // categories.hashCode());
        result = prime * result + ((city == null) ? 0 : city.hashCode());
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((coutPrestationDouane == null) ? 0 : coutPrestationDouane.hashCode());
        result = prime * result + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        result = prime * result + ((ebCompagnie == null) ? 0 : ebCompagnie.hashCode());
        result = prime * result + ((ebEtablissementNum == null) ? 0 : ebEtablissementNum.hashCode());
        result = prime * result + ((ecCountry == null) ? 0 : ecCountry.hashCode());
        result = prime * result + ((email == null) ? 0 : email.hashCode());
        result = prime * result + ((info == null) ? 0 : info.hashCode());
        // result = prime * result + ((listAdresses == null) ? 0 :
        // listAdresses.hashCode());
        result = prime * result + ((logo == null) ? 0 : logo.hashCode());
        result = prime * result + ((nmbrUtilisateurs == null) ? 0 : nmbrUtilisateurs.hashCode());
        result = prime * result + ((nom == null) ? 0 : nom.hashCode());
        result = prime * result + ((service == null) ? 0 : service.hashCode());
        result = prime * result + ((siret == null) ? 0 : siret.hashCode());
        result = prime * result + ((siteWeb == null) ? 0 : siteWeb.hashCode());
        result = prime * result + ((telephone == null) ? 0 : telephone.hashCode());
        result = prime * result + ((canShowPrice == null) ? 0 : canShowPrice.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbEtablissement other = (EbEtablissement) obj;

        if (adresse == null) {
            if (other.adresse != null) return false;
        }
        else if (!adresse.equals(other.adresse)) return false;

        if (categories == null) {
            if (other.categories != null) return false;
        }
        else if (!categories.equals(other.categories)) return false;

        if (city == null) {
            if (other.city != null) return false;
        }
        else if (!city.equals(other.city)) return false;

        if (code == null) {
            if (other.code != null) return false;
        }
        else if (!code.equals(other.code)) return false;

        if (coutPrestationDouane == null) {
            if (other.coutPrestationDouane != null) return false;
        }
        else if (!coutPrestationDouane.equals(other.coutPrestationDouane)) return false;

        if (dateCreation == null) {
            if (other.dateCreation != null) return false;
        }
        else if (!dateCreation.equals(other.dateCreation)) return false;

        if (ebCompagnie == null) {
            if (other.ebCompagnie != null) return false;
        }
        else if (!ebCompagnie.equals(other.ebCompagnie)) return false;

        if (ebEtablissementNum == null) {
            if (other.ebEtablissementNum != null) return false;
        }
        else if (!ebEtablissementNum.equals(other.ebEtablissementNum)) return false;

        if (ecCountry == null) {
            if (other.ecCountry != null) return false;
        }
        else if (!ecCountry.equals(other.ecCountry)) return false;

        if (email == null) {
            if (other.email != null) return false;
        }
        else if (!email.equals(other.email)) return false;

        if (info == null) {
            if (other.info != null) return false;
        }
        else if (!info.equals(other.info)) return false;

        if (logo == null) {
            if (other.logo != null) return false;
        }
        else if (!logo.equals(other.logo)) return false;

        if (nmbrUtilisateurs == null) {
            if (other.nmbrUtilisateurs != null) return false;
        }
        else if (!nmbrUtilisateurs.equals(other.nmbrUtilisateurs)) return false;

        if (nom == null) {
            if (other.nom != null) return false;
        }
        else if (!nom.equals(other.nom)) return false;

        if (service == null) {
            if (other.service != null) return false;
        }
        else if (!service.equals(other.service)) return false;

        if (siret == null) {
            if (other.siret != null) return false;
        }
        else if (!siret.equals(other.siret)) return false;

        if (siteWeb == null) {
            if (other.siteWeb != null) return false;
        }
        else if (!siteWeb.equals(other.siteWeb)) return false;

        if (telephone == null) {
            if (other.telephone != null) return false;
        }
        else if (!telephone.equals(other.telephone)) return false;

        if (canShowPrice == null) {
            if (other.canShowPrice != null) return false;
        }
        else if (!canShowPrice.equals(other.canShowPrice)) return false;

        return true;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getTypeEtablissement() {
        return typeEtablissement;
    }

    public void setTypeEtablissement(String typeEtablissement) {
        this.typeEtablissement = typeEtablissement;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Set<EbRelation> getEtablissementsHost() {
        return etablissementsHost;
    }

    public void setEtablissementsHost(Set<EbRelation> etablissementsHost) {
        this.etablissementsHost = etablissementsHost;
    }

    public Set<EbRelation> getEtablissementsGuests() {
        return etablissementsGuests;
    }

    public void setEtablissementsGuests(Set<EbRelation> etablissementsGuests) {
        this.etablissementsGuests = etablissementsGuests;
    }

    public EbTtSchemaPsl getSchemaPsl() {
        return schemaPsl;
    }

    public void setSchemaPsl(EbTtSchemaPsl schemaPsl) {
        this.schemaPsl = schemaPsl;
    }
}
