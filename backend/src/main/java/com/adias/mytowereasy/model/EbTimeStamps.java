package com.adias.mytowereasy.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;


@MappedSuperclass
public class EbTimeStamps {
    @Column(name = "date_creation_sys", updatable = false)
    @CreationTimestamp
    protected Timestamp dateCreationSys;

    @Column(name = "date_maj_sys")
    @UpdateTimestamp
    protected Timestamp dateMajSys;

    public Timestamp getDateCreationSys() {
        return dateCreationSys;
    }

    public void setDateCreationSys(Timestamp dateCreationSys) {
        this.dateCreationSys = dateCreationSys;
    }

    public Timestamp getDateMajSys() {
        return dateMajSys;
    }

    public void setDateMajSys(Timestamp dateMajSys) {
        this.dateMajSys = dateMajSys;
    }
}
