package com.adias.mytowereasy.model;

import java.util.List;

public class ComputeTransportRequestUnitMetrics {
	private double totalNbrParcel = 0;
	private double totalTaxableWeight = 0;
	private double totalVolume = 0;
	private double totalWeight = 0;
	private List<EbMarchandise> units;

	public ComputeTransportRequestUnitMetrics(List<EbMarchandise> units, EbDemande demande) {
		calculateVolumeWeightAndParcelCount(units);
		demande.setTotalWeight(totalWeight);
		demande.setTotalVolume(totalVolume);
		demande.setTotalNbrParcel(totalNbrParcel);
		demande.setTotalTaxableWeight(calculateTaxableWeight(demande));
	}

	private double calculateTaxableWeight(EbDemande demande) {
		double weight = 0.0;

		if (Enumeration.ModeTransport.AIR.getCode().equals(demande.getxEcModeTransport())) {
			// Air
			weight = (demande.getTotalVolume() / 6) * 1000;
		} else if (Enumeration.ModeTransport.SEA.getCode().equals(demande.getxEcModeTransport())) {
			// Sea
			weight = demande.getTotalVolume() * 1000;
		} else if (Enumeration.ModeTransport.ROAD.getCode().equals(demande.getxEcModeTransport())) {
			// Road
			weight = (demande.getTotalVolume() / 3) * 1000;
		} else if (Enumeration.ModeTransport.INTEGRATOR.getCode().equals(demande.getxEcModeTransport())) {
			// Integrator
			weight = (demande.getTotalVolume() * 1000000) / 5000;
		}

		return handleTotalWeight(weight, demande.getTotalWeight());
	}

	private double handleTotalWeight(double weight, Double demandeTotalWeight) {
		return demandeTotalWeight != null ? (weight > demandeTotalWeight ? weight : demandeTotalWeight) : weight;
	}

	private void calculateVolumeWeightAndParcelCount(List<EbMarchandise> units) {
		units.forEach(mar -> {

			if (mar.getNumberOfUnits() != null) {
				totalNbrParcel += mar.getNumberOfUnits();
			}

			if (mar.getWeight() != null) {
				totalWeight += mar.getWeight() * mar.getNumberOfUnits();
			}

			if (mar.getVolume() != null) {
				totalVolume += mar.getVolume() * mar.getNumberOfUnits();
			}
		});
	}

	public double getTotalNbrParcel() {
		return totalNbrParcel;
	}

	public void setTotalNbrParcel(double totalNbrParcel) {
		this.totalNbrParcel = totalNbrParcel;
	}

	public double getTotalTaxableWeight() {
		return totalTaxableWeight;
	}

	public void setTotalTaxableWeight(double totalTaxableWeight) {
		this.totalTaxableWeight = totalTaxableWeight;
	}

	public double getTotalVolume() {
		return totalVolume;
	}

	public void setTotalVolume(double totalVolume) {
		this.totalVolume = totalVolume;
	}

	public double getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(double totalWeight) {
		this.totalWeight = totalWeight;
	}

	public List<EbMarchandise> getUnits() {
		return units;
	}

	public void setUnits(List<EbMarchandise> units) {
		this.units = units;
	}
}
