/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


@Entity
@Table(name = "eb_cm_combinaison")
public class EbCombinaison implements Serializable {
    /** */
    private static final long serialVersionUID = 8966316346693507663L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebCombinaisonNum;

    @Column(columnDefinition = "TEXT")
    private String hsCode;

    @OneToOne
    @JoinColumn(name = "x_ec_country", insertable = true)
    private EcCountry origin;

    private Double taxes;

    @Column(columnDefinition = "TEXT")
    private String duties;

    @Column(columnDefinition = "TEXT")
    private String otherTaxes;

    @Column(columnDefinition = "TEXT")
    private String requiredDocument;

    private Boolean importLicense;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_destination_country", insertable = true, updatable = false)
    @JsonIgnore
    @JsonDeserialize
    private EbDestinationCountry destinationCountry;

    @Column(updatable = false, insertable = false)
    @ColumnDefault("now()")
    private Date dateAjout;

    public EbCombinaison() {
    }

    public Integer getEbCombinaisonNum() {
        return ebCombinaisonNum;
    }

    public void setEbCombinaisonNum(Integer ebCombinaisonNum) {
        this.ebCombinaisonNum = ebCombinaisonNum;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public EcCountry getOrigin() {
        return origin;
    }

    public void setOrigin(EcCountry origin) {
        this.origin = origin;
    }

    public Double getTaxes() {
        return taxes;
    }

    public void setTaxes(Double taxes) {
        this.taxes = taxes;
    }

    public String getDuties() {
        return duties;
    }

    public void setDuties(String duties) {
        this.duties = duties;
    }

    public String getOtherTaxes() {
        return otherTaxes;
    }

    public void setOtherTaxes(String otherTaxes) {
        this.otherTaxes = otherTaxes;
    }

    public String getRequiredDocument() {
        return requiredDocument;
    }

    public void setRequiredDocument(String requiredDocument) {
        this.requiredDocument = requiredDocument;
    }

    public Boolean getImportLicense() {
        return importLicense;
    }

    public void setImportLicense(Boolean importLicense) {
        this.importLicense = importLicense;
    }

    public EbDestinationCountry getDestinationCountry() {
        return destinationCountry;
    }

    public void setDestinationCountry(EbDestinationCountry destinationCountry) {
        this.destinationCountry = destinationCountry;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateAjout == null) ? 0 : dateAjout.hashCode());
        // result = prime * result + ((destinationCountry == null) ? 0 :
        // destinationCountry.hashCode());
        result = prime * result + ((duties == null) ? 0 : duties.hashCode());
        result = prime * result + ((ebCombinaisonNum == null) ? 0 : ebCombinaisonNum.hashCode());
        result = prime * result + ((hsCode == null) ? 0 : hsCode.hashCode());
        result = prime * result + ((importLicense == null) ? 0 : importLicense.hashCode());
        result = prime * result + ((origin == null) ? 0 : origin.hashCode());
        result = prime * result + ((otherTaxes == null) ? 0 : otherTaxes.hashCode());
        result = prime * result + ((requiredDocument == null) ? 0 : requiredDocument.hashCode());
        result = prime * result + ((taxes == null) ? 0 : taxes.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbCombinaison other = (EbCombinaison) obj;

        if (dateAjout == null) {
            if (other.dateAjout != null) return false;
        }
        else if (!dateAjout.equals(other.dateAjout)) return false;

        if (destinationCountry == null) {
            if (other.destinationCountry != null) return false;
        }
        else if (!destinationCountry.equals(other.destinationCountry)) return false;

        if (duties == null) {
            if (other.duties != null) return false;
        }
        else if (!duties.equals(other.duties)) return false;

        if (ebCombinaisonNum == null) {
            if (other.ebCombinaisonNum != null) return false;
        }
        else if (!ebCombinaisonNum.equals(other.ebCombinaisonNum)) return false;

        if (hsCode == null) {
            if (other.hsCode != null) return false;
        }
        else if (!hsCode.equals(other.hsCode)) return false;

        if (importLicense == null) {
            if (other.importLicense != null) return false;
        }
        else if (!importLicense.equals(other.importLicense)) return false;

        if (origin == null) {
            if (other.origin != null) return false;
        }
        else if (!origin.equals(other.origin)) return false;

        if (otherTaxes == null) {
            if (other.otherTaxes != null) return false;
        }
        else if (!otherTaxes.equals(other.otherTaxes)) return false;

        if (requiredDocument == null) {
            if (other.requiredDocument != null) return false;
        }
        else if (!requiredDocument.equals(other.requiredDocument)) return false;

        if (taxes == null) {
            if (other.taxes != null) return false;
        }
        else if (!taxes.equals(other.taxes)) return false;

        return true;
    }
}
