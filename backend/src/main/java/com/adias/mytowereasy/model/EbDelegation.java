/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;


@Entity
@Table(name = "eb_delegation", schema = "work")
public class EbDelegation implements Serializable {
    /** */
    private static final long serialVersionUID = 5483584914887945270L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebDelegationNum;

    @OneToOne
    @JoinColumn(name = "user_connect")
    // @JsonIgnore
    private EbUser userConnect;

    @OneToOne
    @JoinColumn(name = "user_backUp")
    // @JsonIgnore
    private EbUser userBackUp;

    private Date dateDebut;
    private Date dateFin;

    public EbDelegation() {
        super();

        this.userBackUp = new EbUser();
        this.userConnect = new EbUser();
    }

    public EbUser getUserConnect() {
        return new EbUser(this.userConnect);
    }

    public void setUserConnect(EbUser userConnect) {
        this.userConnect = userConnect;
    }

    public Integer getEbDelegationNum() {
        return ebDelegationNum;
    }

    public void setEbDelegationNum(Integer ebDelegationNum) {
        this.ebDelegationNum = ebDelegationNum;
    }

    public EbUser getUserBackUp() {
        return new EbUser(this.userBackUp);
    }

    public void setUserBackUp(EbUser userBackUp) {
        this.userBackUp = userBackUp;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateDebut == null) ? 0 : dateDebut.hashCode());
        result = prime * result + ((dateFin == null) ? 0 : dateFin.hashCode());
        result = prime * result + ((ebDelegationNum == null) ? 0 : ebDelegationNum.hashCode());
        result = prime * result + ((userBackUp == null) ? 0 : userBackUp.hashCode());
        result = prime * result + ((userConnect == null) ? 0 : userConnect.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbDelegation other = (EbDelegation) obj;

        if (dateDebut == null) {
            if (other.dateDebut != null) return false;
        }
        else if (!dateDebut.equals(other.dateDebut)) return false;

        if (dateFin == null) {
            if (other.dateFin != null) return false;
        }
        else if (!dateFin.equals(other.dateFin)) return false;

        if (ebDelegationNum == null) {
            if (other.ebDelegationNum != null) return false;
        }
        else if (!ebDelegationNum.equals(other.ebDelegationNum)) return false;

        if (userBackUp == null) {
            if (other.userBackUp != null) return false;
        }
        else if (!userBackUp.equals(other.userBackUp)) return false;

        if (userConnect == null) {
            if (other.userConnect != null) return false;
        }
        else if (!userConnect.equals(other.userConnect)) return false;

        return true;
    }
}
