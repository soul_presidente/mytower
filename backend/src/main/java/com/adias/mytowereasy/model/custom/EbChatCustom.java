/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.custom;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.TypeDef;

import com.adias.mytowereasy.types.JsonBinaryType;


@Table(name = "eb_chat_custom")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class EbChatCustom implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = -1021460914257092635L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebChatCustomNum;

    private Integer idFiche;
    private Integer idChatComponent;
    private Integer module;

    private String userName;
    private String userAvatar;

    @Column(columnDefinition = "TEXT")
    private String text;

    @ColumnDefault("now()")
    private Date dateCreation;

    private Boolean isHistory;

    public Integer getIdChatComponent() {
        return idChatComponent;
    }

    public void setIdChatComponent(Integer idChatComponent) {
        this.idChatComponent = idChatComponent;
    }

    public Integer getEbChatCustomNum() {
        return ebChatCustomNum;
    }

    public void setEbChatCustomNum(Integer ebChatCustomNum) {
        this.ebChatCustomNum = ebChatCustomNum;
    }

    public Integer getIdFiche() {
        return idFiche;
    }

    public void setIdFiche(Integer idFiche) {
        this.idFiche = idFiche;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Boolean getIsHistory() {
        return isHistory;
    }

    public void setIsHistory(Boolean isHistory) {
        this.isHistory = isHistory;
    }
}
