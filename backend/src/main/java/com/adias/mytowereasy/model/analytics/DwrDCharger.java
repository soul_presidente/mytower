/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.analytics;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


@Entity
@Table(name = "dwr_d_charger")
public class DwrDCharger {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dwr_d_charger")
    private Integer dwrDChargerNum;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "charger", cascade = CascadeType.ALL)
    private List<DwrFShipments> shipments = new ArrayList<DwrFShipments>();

    @OneToMany(mappedBy = "charger", cascade = CascadeType.ALL)
    private List<DwrFShipmentsIncident> shipmentsIncident = new ArrayList<DwrFShipmentsIncident>();

    public Integer getID() {
        return this.dwrDChargerNum;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public List<DwrFShipments> getShipments() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.shipments;
    }

    public void setShipments(List<DwrFShipments> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.shipments = value;
    }

    public List<DwrFShipmentsIncident> getShipmentsIncident() {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        return this.shipmentsIncident;
    }

    public void setShipmentsIncident(List<DwrFShipmentsIncident> value) {
        // Automatically generated method. Please delete this comment before
        // entering specific code.
        this.shipmentsIncident = value;
    }
}
