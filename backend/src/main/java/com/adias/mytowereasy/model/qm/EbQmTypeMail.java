/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.qm;

import javax.persistence.*;

import com.adias.mytowereasy.model.EbEtablissement;


/** EcQmTypeMail entity. @author Mohamed */
@Entity
// @Table(indexes = { @Index(name = "ecQmTypeMailNumIdx", columnList =
// "qmTypeMailNum"),
// @Index(name = "ecQmTypeMailEtablissementIdx", columnList = "x_eb_etab") })
@Table(name = "eb_qm_type_mail", schema = "work")
public class EbQmTypeMail implements java.io.Serializable {
    private static final long serialVersionUID = -7403482451722936156L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer qmTypeMailNum;

    @Column
    private String label;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etab")
    private EbEtablissement etablissement;

    public Integer getQmTypeMailNum() {
        return qmTypeMailNum;
    }

    public void setQmTypeMailNum(Integer qmTypeMailNum) {
        this.qmTypeMailNum = qmTypeMailNum;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public EbEtablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(EbEtablissement etablissement) {
        this.etablissement = etablissement;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((qmTypeMailNum == null) ? 0 : qmTypeMailNum.hashCode());
        result = prime * result + ((etablissement == null) ? 0 : etablissement.hashCode());
        result = prime * result + ((label == null) ? 0 : label.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbQmTypeMail other = (EbQmTypeMail) obj;

        if (qmTypeMailNum == null) {
            if (other.qmTypeMailNum != null) return false;
        }
        else if (!qmTypeMailNum.equals(other.qmTypeMailNum)) return false;

        if (etablissement == null) {
            if (other.etablissement != null) return false;
        }
        else if (!etablissement.equals(other.etablissement)) return false;

        if (label == null) {
            if (other.label != null) return false;
        }
        else if (!label.equals(other.label)) return false;

        return true;
    }
}
