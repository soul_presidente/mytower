/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.types.CustomObjectIdResolver;


@SqlResultSetMapping(name = "EtablissementContactMapping", classes = {
    @ConstructorResult(targetClass = EbRelation.class, columns = {
        @ColumnResult(name = "eb_relation_num", type = Integer.class),
        @ColumnResult(name = "type_relation", type = Integer.class),
        @ColumnResult(name = "etat", type = Integer.class),
        @ColumnResult(name = "rel_email"),
        @ColumnResult(name = "x_eb_etablissement", type = Integer.class),
        @ColumnResult(name = "x_eb_etablissement_host", type = Integer.class),
        @ColumnResult(name = "flag_etablissement", type = Boolean.class),
        @ColumnResult(name = "eb_etablissement_num", type = Integer.class),
        @ColumnResult(name = "etab_nom"),
        @ColumnResult(name = "etab_email"),
        @ColumnResult(name = "etab_service", type = Integer.class),
        @ColumnResult(name = "etab_telephone"),
        @ColumnResult(name = "etab_logo"),
        @ColumnResult(name = "x_host", type = Integer.class),
        @ColumnResult(name = "x_guest", type = Integer.class),
        @ColumnResult(name = "host_nom"),
        @ColumnResult(name = "host_prenom"),
        @ColumnResult(name = "host_email"),
        @ColumnResult(name = "etab_host_eb_etablissement_num", type = Integer.class),
        @ColumnResult(name = "etab_host_nom"),
        @ColumnResult(name = "eb_compagnie_num"),
        @ColumnResult(name = "comp_nom"),
        @ColumnResult(name = "is_establishment_request", type = Boolean.class)
    })

})
@Entity
@Table(name = "eb_relation", indexes = {
    @Index(name = "idx_eb_relation_x_host", columnList = "x_host"),
    @Index(name = "idx_eb_relation_x_guest", columnList = "x_guest"),
    @Index(name = "idx_eb_relation_x_eb_etablissement_host", columnList = "x_eb_etablissement_host"),
    @Index(name = "idx_eb_relation_x_eb_etablissement", columnList = "x_eb_etablissement_host"),
    @Index(name = "idx_eb_relation_x_last_modified_by", columnList = "x_last_modified_by")
})
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "ebRelationNum",
    resolver = CustomObjectIdResolver.class,
    scope = EbRelation.class)
public class EbRelation implements Serializable {
    /** */
    private static final long serialVersionUID = 1115172577441322667L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebRelationNum;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_host", insertable = true, updatable = false)
    private EbUser host;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "x_guest", nullable = true)
    private EbUser guest;

    // typeRelation is the UserService (relation between the host & the geust)
    private Integer typeRelation;

    private Integer etat;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_etablissement_host", insertable = true, updatable = false)
    private EbEtablissement xEbEtablissementHost;

    @OneToOne(cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_etablissement", insertable = true, updatable = false)
    private EbEtablissement xEbEtablissement;

    private Boolean flagEtablissement;

    private String email;

    @Column(updatable = false)
    @ColumnDefault("now()")
    private Date dateAjout;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_last_modified_by", insertable = true, updatable = true)
    private EbUser lastModifiedBy;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private EbCompagnie ebCompagnie;

    public EbRelation() {
    }

    public EbRelation(
        Integer ebRelationNum,
        Integer typeRelation,
        Integer etat,
        String relEmail,
        Integer xEbEtablissement,
        Integer xEbEtablissementHost,
        Boolean flagEtablissement,
        Integer ebEtablissementNum,
        String etabNom,
        String etabEmail,
        Integer etabService,
        String etabTelephone,
        String etabLogo,
        Integer xHost,
        Integer xGuest,
        String hostNom,
        String hostPrenom,
        String hostEmail,
        Integer etabHostEbEtablissementNum,
        String etabHostNom,
        Integer compHostNum,
        String compHostNom,
        Boolean isEstablishmentRequest) {
        this.ebRelationNum = ebRelationNum;
        this.typeRelation = typeRelation;
        this.etat = etat;
        this.email = relEmail;
        this.flagEtablissement = flagEtablissement;
        this.xEbEtablissement = new EbEtablissement(xEbEtablissement);
        this.xEbEtablissementHost = new EbEtablissement(xEbEtablissementHost);
        this.setHost(new EbUser(xHost, hostNom, hostPrenom, hostEmail, null, null));
        this.setGuest(new EbUser(xGuest));

        EbEtablissement etab = new EbEtablissement();
        etab.setEbEtablissementNum(ebEtablissementNum);
        etab.setNom(etabNom);
        etab.setEmail(etabEmail);
        etab.setService(etabService);
        etab.setTelephone(etabTelephone);
        etab.setLogo(etabLogo);

        /*
         * EbCompagnie ebcomp = new EbCompagnie();
         * ebcomp.setEbCompagnieNum(numCompagnie);
         * ebcomp.setNom(nomCompagne);
         */

        if (isEstablishmentRequest != null && isEstablishmentRequest) {
            this.setxEbEtablissement(etab);
            this.setxEbEtablissementHost(new EbEtablissement(etabHostEbEtablissementNum, etabHostNom));
        }
        else {
            this.setxEbEtablissementHost(etab);
        }

        this.xEbEtablissementHost.setEbCompagnie(new EbCompagnie(compHostNum, compHostNom, null));
    }

    @QueryProjection
    public EbRelation(
        Integer ebRelationNum,
        Integer typeRelation,
        Integer etat,
        Date dateAjout,
        Integer hostNum,
        String hostNom,
        String hostPrenom,
        Integer hostService,
        String hostEmail,
        Integer guestNum,
        String guestNom,
        String guestPrenom,
        Integer guestService,
        String guestEmail) {
        this.ebRelationNum = ebRelationNum;
        this.typeRelation = typeRelation;
        this.etat = etat;
        this.dateAjout = dateAjout;
        this.host = new EbUser(hostNum, hostNom, hostPrenom, hostEmail, hostService, null);
        this.guest = new EbUser(guestNum, guestNom, guestPrenom, guestEmail, guestService, null);
    }

    @QueryProjection
    public EbRelation(
        Integer ebRelationNum,
        Integer typeRelation,
        Integer etat,
        Date dateAjout,
        String email,
        Integer hostNum,
        String hostNom,
        String hostPrenom,
        Integer hostService,
        String hostEmail,
        Integer guestNum,
        String guestNom,
        String guestPrenom,
        Integer guestService,
        String guestEmail,
        String telephone,
        Integer xEbEtablissementHostNum,
        String xEbEtablissementHostNom,
        Integer xEbEtablissementGuestNum,
        String xEbEtablissementGuestNom,
        Integer lastModifiedByEbUserNum) {
        this.ebRelationNum = ebRelationNum;
        this.typeRelation = typeRelation;
        this.etat = etat;
        this.dateAjout = dateAjout;
        this.email = email;
        this.lastModifiedBy = new EbUser(lastModifiedByEbUserNum, null, null, null, null, null);
        this.host = new EbUser(hostNum, hostNom, hostPrenom, hostEmail, hostService, null);
        this.guest = new EbUser(guestNum, guestNom, guestPrenom, guestEmail, guestService, telephone);
        this.host.setEbEtablissement(new EbEtablissement(xEbEtablissementHostNum, xEbEtablissementHostNom));
        this.guest.setEbEtablissement(new EbEtablissement(xEbEtablissementGuestNum, xEbEtablissementGuestNom));
        this.xEbEtablissementHost = new EbEtablissement(xEbEtablissementHostNum, xEbEtablissementHostNom);
    }

    @QueryProjection
    public EbRelation(
        Integer ebRelationNum,
        Integer typeRelation,
        Integer etat,
        Date dateAjout,
        String email,
        Integer xEbEtaHostNum,
        String xEbEtabHostNom,
        String xEbEtabHostAdresse,
        String xEbEtabHostTelephone,
        String xEbEtabHostEmail,
        Integer xEbEtabHostService,
        Integer xEbEtablissementGuestNum,
        String xEbEtablissementGuestNom,
        Boolean flagEtablissement) {
        this.ebRelationNum = ebRelationNum;
        this.typeRelation = typeRelation;
        this.etat = etat;
        this.dateAjout = dateAjout;
        this.email = email;
        this.xEbEtablissementHost = new EbEtablissement(
            xEbEtaHostNum,
            xEbEtabHostNom,
            xEbEtabHostAdresse,
            xEbEtabHostTelephone,
            xEbEtabHostEmail,
            xEbEtabHostService);
        this.xEbEtablissement = new EbEtablissement(xEbEtablissementGuestNum, xEbEtablissementGuestNom);
        this.flagEtablissement = flagEtablissement;
    }

    @QueryProjection
    public EbRelation(
        Integer ebRelationNum,
        Integer typeRelation,
        Integer etat,
        Date dateAjout,
        String email,
        Integer hostNum,
        String hostNom,
        String hostPrenom,
        Integer hostService,
        String hostEmail,
        String hostTel,
        Integer guestNum,
        String guestNom,
        String guestPrenom,
        Integer guestService,
        String guestEmail,
        String guestTel,
        Integer xEbEtablissementHostNum,
        String xEbEtablissementHostNom,
        Integer xEbEtablissementGuestNum,
        String xEbEtablissementGuestNom,
        Integer lastModifiedByEbUserNum) {
        this.ebRelationNum = ebRelationNum;
        this.typeRelation = typeRelation;
        this.etat = etat;
        this.dateAjout = dateAjout;
        this.email = email;
        this.lastModifiedBy = new EbUser(lastModifiedByEbUserNum, null, null, null, null, null);
        this.host = new EbUser(hostNum, hostNom, hostPrenom, hostEmail, hostService, hostTel);
        this.guest = new EbUser(guestNum, guestNom, guestPrenom, guestEmail, guestService, guestTel);
        this.host.setEbEtablissement(new EbEtablissement(xEbEtablissementHostNum, xEbEtablissementHostNom));
        this.guest.setEbEtablissement(new EbEtablissement(xEbEtablissementGuestNum, xEbEtablissementGuestNom));
        this.xEbEtablissementHost = new EbEtablissement(xEbEtablissementHostNum, xEbEtablissementHostNom);
    }

    public EbRelation(Integer ebRelationNum, Integer host, Integer guest) {
        this.ebRelationNum = ebRelationNum;
        this.host = new EbUser(host);
        this.guest = new EbUser(guest);
    }

    public EbUser getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(EbUser lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getEbRelationNum() {
        return ebRelationNum;
    }

    public void setEbRelationNum(Integer ebRelationNum) {
        this.ebRelationNum = ebRelationNum;
    }

    public EbUser getHost() {
        return host;
    }

    public void setHost(EbUser host) {
        this.host = host;
    }

    public EbUser getGuest() {
        return guest;
    }

    public void setGuest(EbUser guest) {
        this.guest = guest;
    }

    public Integer getTypeRelation() {
        return typeRelation;
    }

    public void setTypeRelation(Integer typeRelation) {
        this.typeRelation = typeRelation;
    }

    public Integer getEtat() {
        return etat;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public Date getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(Date dateAjout) {
        this.dateAjout = dateAjout;
    }

    public EbEtablissement getxEbEtablissementHost() {
        return xEbEtablissementHost;
    }

    public void setxEbEtablissementHost(EbEtablissement xEbEtablissementHost) {
        this.xEbEtablissementHost = xEbEtablissementHost;
    }

    public EbEtablissement getxEbEtablissement() {
        return xEbEtablissement;
    }

    public void setxEbEtablissement(EbEtablissement xEbEtablissement) {
        this.xEbEtablissement = xEbEtablissement;
    }

    public Boolean getFlagEtablissement() {
        return flagEtablissement;
    }

    public void setFlagEtablissement(Boolean flagEtablissement) {
        this.flagEtablissement = flagEtablissement;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    @PrePersist
    void preInsert() {
        if (guest != null && guest.getEbEtablissement() != null) this.xEbEtablissementHost = guest.getEbEtablissement();
        this.dateAjout = new Date();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateAjout == null) ? 0 : dateAjout.hashCode());
        result = prime * result + ((ebRelationNum == null) ? 0 : ebRelationNum.hashCode());
        result = prime * result + ((etat == null) ? 0 : etat.hashCode());
        result = prime * result + ((guest == null) ? 0 : guest.hashCode());
        result = prime * result + ((host == null) ? 0 : host.hashCode());
        result = prime * result + ((typeRelation == null) ? 0 : typeRelation.hashCode());
        result = prime * result + ((xEbEtablissement == null) ? 0 : xEbEtablissement.hashCode());
        result = prime * result + ((xEbEtablissementHost == null) ? 0 : xEbEtablissementHost.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbRelation other = (EbRelation) obj;

        if (dateAjout == null) {
            if (other.dateAjout != null) return false;
        }
        else if (!dateAjout.equals(other.dateAjout)) return false;

        if (ebRelationNum == null) {
            if (other.ebRelationNum != null) return false;
        }
        else if (!ebRelationNum.equals(other.ebRelationNum)) return false;

        if (etat == null) {
            if (other.etat != null) return false;
        }
        else if (!etat.equals(other.etat)) return false;

        if (guest == null) {
            if (other.guest != null) return false;
        }
        else if (!guest.equals(other.guest)) return false;

        if (host == null) {
            if (other.host != null) return false;
        }
        else if (!host.equals(other.host)) return false;

        if (typeRelation == null) {
            if (other.typeRelation != null) return false;
        }
        else if (!typeRelation.equals(other.typeRelation)) return false;

        if (xEbEtablissement == null) {
            if (other.xEbEtablissement != null) return false;
        }
        else if (!xEbEtablissement.equals(other.xEbEtablissement)) return false;

        if (xEbEtablissementHost == null) {
            if (other.xEbEtablissementHost != null) return false;
        }
        else if (!xEbEtablissementHost.equals(other.xEbEtablissementHost)) return false;

        return true;
    }
}
