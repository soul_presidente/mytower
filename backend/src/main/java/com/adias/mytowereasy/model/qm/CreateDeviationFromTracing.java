package com.adias.mytowereasy.model.qm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.adias.mytowereasy.service.TrackService;


@Configuration
@EnableScheduling
public class CreateDeviationFromTracing {
    @Autowired
    TrackService trackService;

    // todo disabled cron job here BUG: @Scheduled(fixedDelay = 100_000_000)
    public void addDeviation() throws Exception {
        trackService.insertDeviation();
    }
}
