package com.adias.mytowereasy.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

import com.adias.mytowereasy.util.GenericEnum;


public class CREnumeration {
    public enum Operator implements GenericEnum {
        GREATER_THAN("GREATER_THAN", 1),
        EQUALS("EQUALS", 2),
        ABSENT("ABSENT", 3),
        DIFFERENT("DIFFERENT", 4),

        IS_IN("CONTROL_RULES.IS_IN", 101),
        IS_NOT_IN("CONTROL_RULES.IS_NOT_IN", 102),
        BEGINS_WITH("CONTROL_RULES.BEGINS_WITH", 103),
        AVAILABLE("CONTROL_RULES.AVAILABLE", 104),
        NOT_AVAILABLE("CONTROL_RULES.NOT_AVAILABLE", 105),

        INTERCHANGE("CONTROL_RULES.INTERCHANGE", 201),
        BECOMES("CONTROL_RULES.BECOMES", 202),
        IS_DELETED("CONTROL_RULES.IS_DELETED", 203),
        IS_CONSERVED("CONTROL_RULES.IS_CONSERVED", 204);

        private Integer code;
        private String key;

        private Operator(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        public static Operator getOperatorEnumByKey(Integer key) {
            Operator[] types = Operator.values();

            for (Operator operator: types) {
                if (operator.getCode() == key) return operator;
            }

            return null;
        }

        public static List<Operator> getOperator() {
            Operator[] operator = Operator.values();
            return Arrays.asList(operator);
        }
    }

    public enum PslType implements GenericEnum {
        ACTUAL("Actual", 1), ESTIMATED("Estimated", 2), NOW("NOW", 3);

        private Integer code;
        private String key;

        private PslType(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        public static PslType getPslTypeEnumByKey(Integer key) {
            PslType[] types = PslType.values();

            for (PslType pslType: types) {
                if (pslType.getCode() == key) return pslType;
            }

            return null;
        }

        public static List<PslType> getPslType() {
            PslType[] types = PslType.values();
            return Arrays.asList(types);
        }
    }

    public enum RuleOperator implements GenericEnum {
        OR("Or", 1), AND("And", 2);

        private Integer code;
        private String key;

        private RuleOperator(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        public static RuleOperator getRuleOperatorEnumByKey(Integer key) {
            RuleOperator[] ruleOperators = RuleOperator.values();

            for (RuleOperator ruleOperator: ruleOperators) {
                if (ruleOperator.getCode() == key) return ruleOperator;
            }

            return null;
        }

        public static List<RuleOperator> getRuleOperator() {
            RuleOperator[] ruleOperator = RuleOperator.values();
            return Arrays.asList(ruleOperator);
        }
    }

    public enum RuleType implements GenericEnum {
        INCOHERENCE("Incoherence", 1), ABSENCE("Absence", 2), RETARD("Retard", 3);

        private Integer code;
        private String key;

        private RuleType(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        public static List<Integer> getRuleTypeEnumByKey(String key) {

            if (key != null && !key.isEmpty()) {
                List<Integer> listCode = new ArrayList<>();
                RuleType[] ruleTypes = RuleType.values();

                for (RuleType ruleType: ruleTypes) {
                    if (ruleType.getKey().toLowerCase().trim().contains(key)) listCode.add(ruleType.getCode());
                }

                return listCode;
            }

            return null;
        }

        public static List<RuleType> getRuleType() {
            RuleType[] ruleTypes = RuleType.values();
            return Arrays.asList(ruleTypes);
        }
    }

    public enum Priority implements GenericEnum {
        LOW("Low", 1), MEDIUM("Medium", 2), HIGH("High", 3);

        private Integer code;
        private String key;

        private Priority(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        public static List<Integer> getPriorityEnumByKey(String key) {

            if (key != null && !key.isEmpty()) {
                List<Integer> listCode = new ArrayList<Integer>();
                Priority[] prioritys = Priority.values();

                for (Priority priority: prioritys) {
                    if (priority.getKey().toLowerCase().trim().contains(key)) listCode.add(priority.getCode());
                }

                return listCode;
            }

            return null;
        }

        public static List<Priority> getPriority() {
            Priority[] priority = Priority.values();
            return Arrays.asList(priority);
        }
    }

    public enum RuleCategory implements GenericEnum {
        DATE("Date", 1), DOCUMENT("Document", 2);

        private Integer code;
        private String key;

        private RuleCategory(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        public static List<Integer> getRuleCategoryEnumByKey(String key) {

            if (key != null && !key.isEmpty()) {
                List<Integer> listCode = new ArrayList<Integer>();
                RuleCategory[] prioritys = RuleCategory.values();

                for (RuleCategory priority: prioritys) {
                    if (priority.getKey().toLowerCase().trim().contains(key)) listCode.add(priority.getCode());
                }

                return listCode;
            }

            return null;
        }

        public static List<RuleCategory> getPriority() {
            RuleCategory[] priority = RuleCategory.values();
            return Arrays.asList(priority);
        }
    }

    public enum DocumentStatus implements GenericEnum {
        NO("No", 1),
        EXPECTED("Expected", 2),
        REQUIRED("Required", 3),
        DEACTIVATED("Deactivated", 4),
        ACTIVATED("Activated", 5);

        private Integer code;
        private String key;

        private DocumentStatus(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum RuleOperandType {
        PAYS_ORIGIN("PRICING_BOOKING.PAYS_ORIGIN", 3, 3, 0, "ebPartyOrigin"),
        DESTINATION_COUNTRY("PRICING_BOOKING.PAYS_DEST", 4, 3, 0, "ebPartyDest"),
        MODE_DE_TRANSPORT("PRICING_BOOKING.MODE_OF_TRANSPORT", 5, 7, 0, "xEcModeTransport"),
        STATUT_DOSSIER("PRICING_BOOKING.STATUS", 6, 9, 0, "xEcStatut"),
        TYPE_OF_REQUEST("PRICING_BOOKING.TYPE_OF_REQUEST", 8, 10, 0, "xEbTypeRequest"),
        CATEGORY("Categories", 11, 4, 0, "listCategoriesStr"),
        CUSTOM_FIELD("SETTINGS.CUSTOM_FIELDS", 12, 5, 0, "customFields"),
        FLOW_TYPE("TYPE_FLUX.TYPE_OF_FLUX", 14, 6, 0, "xEbTypeFluxNum"),
        LEG("Leg", 20, 2, 0, "scenario"),
        REF_ADR_ORG("REPOSITORY_MANAGEMENT.REF_ORG", 22, 12, 2, "reference"),
        REF_ADR_DEST("REPOSITORY_MANAGEMENT.REF_DEST", 23, 12, 2, "reference"),
        PSL_SCHEMA("PRICING_BOOKING.SCHEMA_PSL", 24, 13, 0, "xEbSchemaPsl"),
        UNITS("PRICING_BOOKING.UNITS", 25, 14, 0, "listMarchandises"),
        MANDATORY_FIELDS("PRICING_BOOKING.MANDATORY_FIELDS", 27, 24, 0, null),
        DATE_OF_GOOD_AVAILABILITY(
            "PRICING_BOOKING.DATE_GOODS_AVAILABILITY", // name to display in
                                                       // list
            28, // code field
            25, // code type field
            3, // hidden
            "dateOfGoodsAvailability" // name of field in EbDemande class
        ),
        INVOICE_CURRENCY("PRICING_BOOKING.INVOICE_CURRENCY", 29, 14, 3, "xecCurrencyInvoice"),
        COST_CENTER("PRICING_BOOKING.COST_CENTER", 30, 26, 0, "xEbCostCenter"),
        TYPE_OF_DOC("TRACK_TRACE.DOCUMENT_TYPE", 31, 27, 2, "listTypeDocuments");

        private Integer code;
        private String key;
        private Integer type; // CrFieldTypes below
        private Integer operationType; // is 0 -> both, 1 -> action, 2 ->
                                       // condition, 3->hiddenn
        private String fieldName; // name of field in it's own class

        private RuleOperandType(String key, Integer code, Integer type, Integer operationType, String fieldName) {
            this.code = code;
            this.key = key;
            this.type = type;
            this.operationType = operationType;
            this.fieldName = fieldName;
        }

        public Integer getCode() {
            return code;
        }

        public String getKey() {
            return key;
        }

        public Integer getType() {
            return type;
        }

        public Integer getOperationType() {
            return operationType;
        }

        public String getFieldName() {
            return fieldName;
        }

        public static RuleOperandType getRuleOperandTypeEnumByKey(Integer key) {
            RuleOperandType[] ruleOperandTypes = RuleOperandType.values();

            for (RuleOperandType ruleOperandType: ruleOperandTypes) {
                if (ruleOperandType.getCode() == key) return ruleOperandType;
            }

            return null;
        }

        public static List<RuleOperandType> getRuleOperandType() {
            RuleOperandType[] ruleOperandType = RuleOperandType.values();
            return Arrays.asList(ruleOperandType);
        }

        public static List<RuleOperandType> getEbDemandeFields() {
            List<RuleOperandType> trFields = new ArrayList<>();
            trFields.add(RuleOperandType.MODE_DE_TRANSPORT);
            trFields.add(RuleOperandType.LEG);
            trFields.add(RuleOperandType.FLOW_TYPE);
            trFields.add(RuleOperandType.PSL_SCHEMA);
            trFields.add(RuleOperandType.DATE_OF_GOOD_AVAILABILITY);
            trFields.add(RuleOperandType.STATUT_DOSSIER);
            trFields.add(RuleOperandType.TYPE_OF_REQUEST);
            trFields.add(RuleOperandType.INVOICE_CURRENCY);
            return trFields;
        }

        public static List<RuleOperandType> getEbPartyFields() {
            List<RuleOperandType> partyFields = new ArrayList<>();
            partyFields.add(RuleOperandType.PAYS_ORIGIN);
            partyFields.add(RuleOperandType.DESTINATION_COUNTRY);
            partyFields.add(RuleOperandType.REF_ADR_ORG);
            partyFields.add(RuleOperandType.REF_ADR_DEST);
            return partyFields;
        }

        public static List<RuleOperandType> getEbDemandeRequiredFields() {
            List<RuleOperandType> requiredFields = new ArrayList<>();
            requiredFields.add(RuleOperandType.PAYS_ORIGIN);
            requiredFields.add(RuleOperandType.DESTINATION_COUNTRY);
            requiredFields.add(RuleOperandType.MODE_DE_TRANSPORT);
            requiredFields.add(RuleOperandType.LEG);
            requiredFields.add(RuleOperandType.FLOW_TYPE);
            requiredFields.add(RuleOperandType.PSL_SCHEMA);
            requiredFields.add(RuleOperandType.DATE_OF_GOOD_AVAILABILITY);
            requiredFields.add(RuleOperandType.INVOICE_CURRENCY);
            return requiredFields;
        }

        public static List<RuleOperandType> getAllRequiredFields() {
            List<RuleOperandType> requiredFields = new ArrayList<>();
            requiredFields.add(RuleOperandType.PAYS_ORIGIN);
            requiredFields.add(RuleOperandType.DESTINATION_COUNTRY);
            requiredFields.add(RuleOperandType.MODE_DE_TRANSPORT);
            requiredFields.add(RuleOperandType.LEG);
            requiredFields.add(RuleOperandType.FLOW_TYPE);
            requiredFields.add(RuleOperandType.PSL_SCHEMA);
            requiredFields.add(RuleOperandType.DATE_OF_GOOD_AVAILABILITY);
            requiredFields.add(RuleOperandType.CATEGORY);
            requiredFields.add(RuleOperandType.CUSTOM_FIELD);
            requiredFields.add(RuleOperandType.INVOICE_CURRENCY);
            requiredFields.add(RuleOperandType.REF_ADR_ORG);
            requiredFields.add(RuleOperandType.REF_ADR_DEST);
            return requiredFields;
        }

        public static boolean isTrField(Integer code) {
            List<RuleOperandType> trFields = RuleOperandType.getEbDemandeFields();
            return trFields.stream().filter(field -> field.code.equals(code)).findFirst().isPresent();
        }

        public static boolean isPartyField(Integer code) {
            List<RuleOperandType> partyFields = RuleOperandType.getEbPartyFields();
            return partyFields.stream().filter(field -> field.code.equals(code)).findFirst().isPresent();
        }
    }

    public enum RuleTrigger implements GenericEnum {
        NOW("CONTROL_RULES.NOW", 1),
        AT_TR_FILE_CREATION("CONTROL_RULES.AT_TR_FILE_CREATION", 2),
        AT_TR_FILE_UPDATE("CONTROL_RULES.AT_TR_FILE_UPDATE", 3);

        private Integer code;
        private String key;

        private RuleTrigger(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        public static RuleTrigger getRuleTriggerEnumByKey(Integer key) {
            RuleTrigger[] ruleTriggers = RuleTrigger.values();

            for (RuleTrigger ruleTrigger: ruleTriggers) {
                if (ruleTrigger.getCode() == key) return ruleTrigger;
            }

            return null;
        }

        public static List<RuleTrigger> getRuleTrigger() {
            RuleTrigger[] ruleTrigger = RuleTrigger.values();
            return Arrays.asList(ruleTrigger);
        }
    }

    public enum CrFieldTypes {
        STRING("String", 1),
        NUMBER("Number", 2),
        EBPARTY("Address", 3),
        CATEGORIE("Categorie", 4),
        CUSTOMFIELD("CustomField", 5),
        TYPEDEFLUX("Flow type", 6),
        MODE_TR("Mode Transport", 7),
        INCOTERM("Incoterm", 8),
        STATUS("Status Demande", 9),
        TYPE_OF_REQ("Type of request", 10),
        COMPANY_CODE("Code company", 11),
        REF_ADDRESS("Address Reference", 12),
        PSL_SCHEMA("Timestamps scheme", 13),
        UNITS("Units", 14),
        LIST("List", 15),
        EXPORT_CONTROL("EXPORT_CONTROL", 16),
        COUNTRY("COUNTRY", 17),
        UNIT_TYPE("UNIT_TYPE", 18),
        GOOD_TYPE("GOOD_TYPE", 19),
        OVERSIZED_ITEM("OVERSIZED_ITEM", 20),
        LARGE_ITEM("LARGE_ITEM", 21),
        DGR("DGR", 22),
        CLASS("CLASS", 23),
        MANDATORY_FIELDS("MANDATORY_FIELDS", 24),
        DATE("DATE", 25),
        COST_CENTER("COST_CENTER", 26),
        TYPE_OF_DOC("TYPE_OF_DOC", 27),

        UNKOWN("Unkown", 6);

        private Integer code;
        private String key;

        private CrFieldTypes(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return code;
        }

        public String getKey() {
            return key;
        }
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum CrUnitFields implements GenericEnum {
  // @formatter:off
		ALL("GENERAL.ALL", 0, 0, 1, null, false),
		HS_CODE("CUSTOM.HSCODE", 1, 1, 0, "hsCode", true),
		PART_NUMBER("PRICING_BOOKING.PART_NUMBER", 2, 1, 0, "partNumber", true),
		SERIAL_NUMBER("PRICING_BOOKING.SERIAL_NUMBER", 3, 1, 0, "serialNumber", true),
		ARTICLE_REF("SETTINGS.ARTICLE_REF", 4, 1, 0, "unitReference", true),
		PRICE("PRICING_BOOKING.PRICE", 7, 2, 0, "price", false), 
		BIN_LOCALTION("PRICING_BOOKING.BIN_LOCALTION", 8, 1, 0, "binLocation", false),
		EXPORT_REF("PRICING_BOOKING.EXPORT_REF", 9, 1, 0, "exportRef", false),
		ARTICE_NAME("PRICING_BOOKING.ARTICE_NAME", 10, 1, 0, "nomArticle", false),
		ORIGIN_COUNTRY("TRACK_TRACE.ORIGIN_COUNTRY", 11, 17, 0, "xEcCountryOrigin", false),
		UNIT_TYPE("PRICING_BOOKING.TYPE_OF_UNIT", 12, 18, 0, "xEbTypeUnit", false),
		GOOD_TYPE("PRICING_BOOKING.TYPE_OF_GOODS", 13, 19, 0, "typeGoodsLabel", false),
		DGR("PRICING_BOOKING.DOUNGEROUS_GOOD", 16, 22, 0, "dangerousGood", false),
		UN("PRICING_BOOKING.UN", 17, 1, 0, "un", false),
		CLASS("PRICING_BOOKING.CLASS", 18, 23, 0, "classGood", false),
		PACKAGING("PRICING_BOOKING.PACKAGING", 19, 1, 0, "packaging", false),
		VOLUME("PRICING_BOOKING.VOLUME_UNIT", 20, 2, 0, "volume", false),
		WEIGHT("PRICING_BOOKING.WEIGHT_UNIT", 21, 2, 0, "weight", false),
		LENGHT("PRICING_BOOKING.LENGHT", 22, 2, 0, "length", false),
		WIDTH("PRICING_BOOKING.WIDTH", 23, 2, 0, "width", false),
		HEIGTH("PRICING_BOOKING.HEIGTH", 24, 2, 0, "heigth", false),;
	// @formatter:on
        private Integer code;
        private String key;
        private Integer type;
        private Integer operationType; // is 0->both, 1 -> action, 2 ->
                                       // condition, 3->hidden
        private String fieldName; // exact same name of the field in
                                  // EbMarchandie class
        private boolean isDefault;

        private CrUnitFields(
            String key,
            Integer code,
            Integer type,
            Integer operationType,
            String fieldName,
            boolean isDefault) {
            this.code = code;
            this.key = key;
            this.type = type;
            this.operationType = operationType;
            this.fieldName = fieldName;
            this.isDefault = isDefault;
        }

        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        public Integer getType() {
            return type;
        }

        public String getFieldName() {
            return fieldName;
        }

        public boolean isDefault() {
            return isDefault;
        }

        public Integer getOperationType() {
            return operationType;
        }

        public static List<CrUnitFields> getValues() {
            return Arrays.asList(CrUnitFields.values());
        }

        public static CrUnitFields getUnitInfoByCode(Integer unitCode) {
            return Arrays
                .asList(CrUnitFields.values()).stream().filter(unit -> unit.code == unitCode).findFirst().orElse(null);
        }

        public static List<CrUnitFields> getUnitFields() {
            List<CrUnitFields> unitFields = new ArrayList<>();
            unitFields.add(ARTICE_NAME);
            unitFields.add(HS_CODE);
            unitFields.add(PART_NUMBER);
            unitFields.add(SERIAL_NUMBER);
            unitFields.add(ARTICLE_REF);
            unitFields.add(PRICE);
            unitFields.add(BIN_LOCALTION);
            unitFields.add(EXPORT_REF);
            unitFields.add(ORIGIN_COUNTRY);
            unitFields.add(UNIT_TYPE);
            unitFields.add(GOOD_TYPE);
            unitFields.add(DGR);
            unitFields.add(UN);
            unitFields.add(CLASS);
            unitFields.add(PACKAGING);
            unitFields.add(VOLUME);
            unitFields.add(WEIGHT);
            unitFields.add(WIDTH);
            unitFields.add(LENGHT);
            unitFields.add(HEIGTH);
            return unitFields;
        }
    }

    public enum PslDates implements GenericEnum {
        LDD("TR_CALCULATED_LDD", 1),
        EDD("TR_CALCULATED_EDD", 2),
        RDD("TR_CALCULATED_RDD", 3),
        SDD("TR_CALCULATED_SDD", 4),
        CURT_TR("TR_CALCULATED_CURT_TR", 5),
        CURT_SLA("TR_CALCULATED_CURT_SLA", 6),
        ROOCED("TR_CALCULATED_ROOCED", 7),
        ROOCID("TR_CALCULATED_ROOCID", 8),
        RCFC("TR_CALCULATED_RCFC", 9);

        private Integer code;
        private String key;

        private PslDates(String key, Integer code) {
            this.code = code;
            this.key = key;
        }

        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }

        public static PslDates getPslDatesEnumByKey(Integer key) {
            PslDates[] dates = PslDates.values();

            for (PslDates date: dates) {
                if (date.getCode() == key) return date;
            }

            return null;
        }

        public static List<PslDates> getPslDates() {
            PslDates[] dates = PslDates.values();
            return Arrays.asList(dates);
        }
    }
}
