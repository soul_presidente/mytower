package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.adias.mytowereasy.model.qm.EbQmIncident;


@Entity
@Table(name = "eb_email_historique", schema = "work")
public class EbEmailHistorique implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer module;

    private Date dateEnvoi;

    private String typeEmail;

    private Integer typeEmailId;

    private String tto;

    @Column(columnDefinition = "TEXT")
    private String subject;

    @Column(columnDefinition = "TEXT")
    private String text;

    @Column(columnDefinition = "TEXT")
    private String enCopie;

    @Lob
    private String enCopieCachee;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "x_eb_incident", insertable = true, updatable = false)
    private EbQmIncident ebIncident;

    @Column(updatable = false)
    private Integer ebIncidentNum; // QM Specific

    @Column(updatable = false)
    private Integer objectNum; // Generic

    public EbEmailHistorique() {
    }

    public EbEmailHistorique(
        Integer id,
        Integer module,
        Date dateEnvoi,
        String typeEmail,
        String tto,
        String subject,
        String text,
        String enCopie,
        String enCopieCachee,
        EbQmIncident ebIncident,
        Integer ebIncidentNum) {
        this.id = id;
        this.module = module;
        this.dateEnvoi = dateEnvoi;
        this.typeEmail = typeEmail;
        this.tto = tto;
        this.subject = subject;
        this.text = text;
        this.enCopie = enCopie;
        this.enCopieCachee = enCopieCachee;
        this.ebIncident = ebIncident;
        this.ebIncidentNum = ebIncidentNum;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public Date getDateEnvoi() {
        return dateEnvoi;
    }

    public void setDateEnvoi(Date dateEnvoi) {
        this.dateEnvoi = dateEnvoi;
    }

    public String getTypeEmail() {
        return typeEmail;
    }

    public void setTypeEmail(String typeEmail) {
        this.typeEmail = typeEmail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTto() {
        return tto;
    }

    public void setTto(String tto) {
        this.tto = tto;
    }

    public String getEnCopie() {
        return enCopie;
    }

    public void setEnCopie(String enCopie) {
        this.enCopie = enCopie;
    }

    public String getEnCopieCachee() {
        return enCopieCachee;
    }

    public void setEnCopieCachee(String enCopieCachee) {
        this.enCopieCachee = enCopieCachee;
    }

    public EbQmIncident getEbIncident() {
        return ebIncident;
    }

    public void setEbIncident(EbQmIncident ebIncident) {
        this.ebIncident = ebIncident;
    }

    public Integer getEbIncidentNum() {
        return ebIncidentNum;
    }

    public void setEbIncidentNum(Integer ebIncidentNum) {
        this.ebIncidentNum = ebIncidentNum;
    }

    public Integer getObjectNum() {
        return objectNum;
    }

    public void setObjectNum(Integer objectNum) {
        this.objectNum = objectNum;
    }

    public Integer getTypeEmailId() {
        return typeEmailId;
    }

    public void setTypeEmailId(Integer typeEmailId) {
        this.typeEmailId = typeEmailId;
    }
}
