/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.api.wso.ExchangeRateWSO;


@Entity
@Table
public class EbCompagnieCurrency implements Serializable {
    private static final long serialVersionUID = -6383562667397163281L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "eb_compagnie_currency_num")
    private Integer ebCompagnieCurrencyNum;

    private Double euroExchangeRate;

    private Date dateDebut;

    private Date dateFin;

    @Column(updatable = false)
    private Date dateCreation;

    private Date dateModification;

    @ManyToOne
    @JoinColumn(name = "x_ec_Currency")
    private EcCurrency ecCurrency;

    // etablissement du chargeur
    @ManyToOne
    @JoinColumn(name = "x_eb_etablissement", updatable = false)
    private EbEtablissement ebEtablissement;

    // compagnie du chargeur
    @ManyToOne
    @JoinColumn(name = "x_eb_compagnie", nullable = false)
    private EbCompagnie ebCompagnie;

    @Transient
    @JsonSerialize
    @JsonDeserialize
    private Boolean isUsed;

    // compagnie du transporteur
    @ManyToOne
    @JoinColumn(name = "x_eb_compagnie_guest", nullable = false)
    private EbCompagnie ebCompagnieGuest;

    // invoice currency
    @ManyToOne
    @JoinColumn(name = "x_ec_currency_cible")
    private EcCurrency xecCurrencyCible;

    @Column(name = "order_sort")
    private Integer orderSort;

		@Column(name = "is_deleted", columnDefinition = "boolean default false")
		private boolean					isDeleted					= false;

		//		IHM : Human–computer interaction
		@Column(name = "is_created_from_IHM", columnDefinition = "boolean default true")
		private boolean					isCreatedFromIHM	= true;

    public EbCompagnieCurrency() {
    }

    public EbCompagnieCurrency(Integer exEtablissementCurrencyNum) {
        this.ebCompagnieCurrencyNum = exEtablissementCurrencyNum;
    }

    @QueryProjection
    public EbCompagnieCurrency(
        Integer exEtablissementCurrencyNum,
        Double euroExchangeRate,
        Date dateDebut,
        Date dateFin,
        Date dateCreation,
        Integer ecCurrencyNum,
        String libelle,
        String code,
        String codeLibelle,
        Integer ebCompagnieNum,
        String nomCompagnie,
        String codeCompagnie,
        Integer orderSort,
        Integer xecCurrencyCibleNum,
        String xecCurrencyCibleLibelle,
        String xecCurrencyCibleCode,
        Integer ebCompagnieGuestNum,
        String nomCompagnieGuest,
        String codeEbcompanieguest,
        Boolean isUsed) {
        this.ebCompagnieCurrencyNum = exEtablissementCurrencyNum;
        this.euroExchangeRate = euroExchangeRate;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.dateCreation = dateCreation;
        this.ecCurrency = new EcCurrency(ecCurrencyNum, libelle, code, codeLibelle);
        this.orderSort = orderSort;

        this.xecCurrencyCible = new EcCurrency(xecCurrencyCibleNum, xecCurrencyCibleLibelle, xecCurrencyCibleCode);
        this.ebCompagnie = new EbCompagnie(ebCompagnieNum, nomCompagnie, codeCompagnie);
        this.ebCompagnieGuest = new EbCompagnie(ebCompagnieGuestNum, nomCompagnieGuest, codeEbcompanieguest);
        this.isUsed = isUsed;
    }

    @QueryProjection
    public EbCompagnieCurrency(
        Integer exEtablissementCurrencyNum,
        Double euroExchangeRate,
        Date dateDebut,
        Date dateFin,
        Date dateCreation,
        Integer ecCurrencyNum,
        String libelle,
        String code,
        String codelibelle,
        Integer ebCompagnieNum,
        String nomCompagnie,
        String codeCompagnie,
        Integer orderSort,
        Integer xecCurrencyCibleNum,
        String xecCurrencyCibleLibelle,
        String xecCurrencyCibleCode,
        String xecCurrencyCibleCodeLibelle,
        Integer ebCompagnieGuestNum,
        String nomCompagnieGuest,
        String codeEbcompanieguest,
			Boolean isUsed
		)
		{
        this.ebCompagnieCurrencyNum = exEtablissementCurrencyNum;
        this.euroExchangeRate = euroExchangeRate;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.dateCreation = dateCreation;
        this.ecCurrency = new EcCurrency(ecCurrencyNum, libelle, code, codelibelle);
        this.orderSort = orderSort;
        this.ebCompagnie = new EbCompagnie(ebCompagnieNum, nomCompagnie, codeCompagnie);
        this.xecCurrencyCible = new EcCurrency(
            xecCurrencyCibleNum,
            xecCurrencyCibleLibelle,
            xecCurrencyCibleCode,
            xecCurrencyCibleCodeLibelle);
        this.ebCompagnieGuest = new EbCompagnie(ebCompagnieGuestNum, nomCompagnieGuest, codeEbcompanieguest);
        this.isUsed = isUsed;
    }

    @QueryProjection
    public EbCompagnieCurrency(

        Integer ebCompagnieGuestNum,
        String nom,
        String codeEbcompanieguest,
        Integer exEtablissementCurrencyNum,
        Double euroExchangeRate,
        Date dateDebut,
        Date dateFin,
        Date dateCreation,
        Integer ecCurrencyNum,
        Integer ebCompagnieNum,
        Integer xecCurrencybleNum) {
        this.ebCompagnieGuest = new EbCompagnie(ebCompagnieGuestNum, nom, codeEbcompanieguest);
        this.ebCompagnieCurrencyNum = exEtablissementCurrencyNum;
        this.euroExchangeRate = euroExchangeRate;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.dateCreation = dateCreation;
        this.ecCurrency = new EcCurrency(ecCurrencyNum);
        this.ebCompagnie = new EbCompagnie(ebCompagnieNum);
    }

		public EbCompagnieCurrency(
			ExchangeRateWSO exchangeRate,
			EbUser user,
			EbCompagnie company,
			EcCurrency fromCurrency,
			EcCurrency toCurrency
		)
		{
			this.ebCompagnieCurrencyNum = exchangeRate.getExchangeRateId();
			this.dateCreation = new Date();
			this.dateDebut = exchangeRate.getStartDate();
			this.dateFin = exchangeRate.getEndDate();
			this.ebCompagnie = new EbCompagnie(user.getEbCompagnie().getEbCompagnieNum());
			this.ebCompagnieGuest = company;
			this.euroExchangeRate = exchangeRate.getRate();
			this.xecCurrencyCible = toCurrency;
			this.ecCurrency = fromCurrency;
			this.isUsed = false;
			this.isCreatedFromIHM = false;
		}

		public static long getSerialversionuid()
		{
        return serialVersionUID;
    }

    public Double getEuroExchangeRate() {
        return euroExchangeRate;
    }

    public void setEuroExchangeRate(Double euroExchangeRate) {
        this.euroExchangeRate = euroExchangeRate;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public EcCurrency getEcCurrency() {
        return ecCurrency;
    }

    public void setEcCurrency(EcCurrency ecCurrency) {
        this.ecCurrency = ecCurrency;
    }

    public EbEtablissement getEbEtablissement() {
        return ebEtablissement;
    }

    public void setEbEtablissement(EbEtablissement ebEtablissement) {
        this.ebEtablissement = ebEtablissement;
    }

    public Boolean getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(Boolean isUsed) {
        this.isUsed = isUsed;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    public EbCompagnie getEbCompagnieGuest() {
        return ebCompagnieGuest;
    }

    public void setEbCompagnieGuest(EbCompagnie ebCompagnieGuest) {
        this.ebCompagnieGuest = ebCompagnieGuest;
    }

    public Integer getEbCompagnieCurrencyNum() {
        return ebCompagnieCurrencyNum;
    }

    public void setEbCompagnieCurrencyNum(Integer ebCompagnieCurrencyNum) {
        this.ebCompagnieCurrencyNum = ebCompagnieCurrencyNum;
    }

    public EcCurrency getXecCurrencyCible() {
        return xecCurrencyCible;
    }

    public void setXecCurrencyCible(EcCurrency xecCurrencyCible) {
        this.xecCurrencyCible = xecCurrencyCible;
    }

    public Integer getOrderSort() {
        return orderSort;
    }

    public void setOrderSort(Integer orderSort) {
        this.orderSort = orderSort;
    }

		public boolean isDeleted()
		{
			return isDeleted;
		}

		public void setDeleted(boolean isDeleted)
		{
			this.isDeleted = isDeleted;
		}

		public boolean isCreatedFromIHM()
		{
			return isCreatedFromIHM;
		}

		public void setCreatedFromIHM(boolean isCreatedFromIHM)
		{
			this.isCreatedFromIHM = isCreatedFromIHM;
		}

		@PreUpdate
    void preUpdate() {
        this.dateModification = new Date();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        result = prime * result + ((dateDebut == null) ? 0 : dateDebut.hashCode());
        result = prime * result + ((dateFin == null) ? 0 : dateFin.hashCode());
        result = prime * result + ((dateModification == null) ? 0 : dateModification.hashCode());
        result = prime * result + ((ebCompagnie == null) ? 0 : ebCompagnie.hashCode());
        result = prime * result + ((ebCompagnieCurrencyNum == null) ? 0 : ebCompagnieCurrencyNum.hashCode());
        result = prime * result + ((ebCompagnieGuest == null) ? 0 : ebCompagnieGuest.hashCode());
        result = prime * result + ((ebEtablissement == null) ? 0 : ebEtablissement.hashCode());
        result = prime * result + ((ecCurrency == null) ? 0 : ecCurrency.hashCode());
        result = prime * result + ((euroExchangeRate == null) ? 0 : euroExchangeRate.hashCode());
        result = prime * result + ((isUsed == null) ? 0 : isUsed.hashCode());
        result = prime * result + ((orderSort == null) ? 0 : orderSort.hashCode());
        result = prime * result + ((xecCurrencyCible == null) ? 0 : xecCurrencyCible.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbCompagnieCurrency other = (EbCompagnieCurrency) obj;

        if (dateCreation == null) {
            if (other.dateCreation != null) return false;
        }
        else if (!dateCreation.equals(other.dateCreation)) return false;

        if (dateDebut == null) {
            if (other.dateDebut != null) return false;
        }
        else if (!dateDebut.equals(other.dateDebut)) return false;

        if (dateFin == null) {
            if (other.dateFin != null) return false;
        }
        else if (!dateFin.equals(other.dateFin)) return false;

        if (dateModification == null) {
            if (other.dateModification != null) return false;
        }
        else if (!dateModification.equals(other.dateModification)) return false;

        if (ebCompagnie == null) {
            if (other.ebCompagnie != null) return false;
        }
        else if (!ebCompagnie.equals(other.ebCompagnie)) return false;

        if (ebCompagnieCurrencyNum == null) {
            if (other.ebCompagnieCurrencyNum != null) return false;
        }
        else if (!ebCompagnieCurrencyNum.equals(other.ebCompagnieCurrencyNum)) return false;

        if (ebCompagnieGuest == null) {
            if (other.ebCompagnieGuest != null) return false;
        }
        else if (!ebCompagnieGuest.equals(other.ebCompagnieGuest)) return false;

        if (ebEtablissement == null) {
            if (other.ebEtablissement != null) return false;
        }
        else if (!ebEtablissement.equals(other.ebEtablissement)) return false;

        if (ecCurrency == null) {
            if (other.ecCurrency != null) return false;
        }
        else if (!ecCurrency.equals(other.ecCurrency)) return false;

        if (euroExchangeRate == null) {
            if (other.euroExchangeRate != null) return false;
        }
        else if (!euroExchangeRate.equals(other.euroExchangeRate)) return false;

        if (isUsed == null) {
            if (other.isUsed != null) return false;
        }
        else if (!isUsed.equals(other.isUsed)) return false;

        if (orderSort == null) {
            if (other.orderSort != null) return false;
        }
        else if (!orderSort.equals(other.orderSort)) return false;

        if (xecCurrencyCible == null) {
            if (other.xecCurrencyCible != null) return false;
        }
        else if (!xecCurrencyCible.equals(other.xecCurrencyCible)) return false;

        return true;
    }
}
