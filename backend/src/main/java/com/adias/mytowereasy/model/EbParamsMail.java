/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.List;


public class EbParamsMail {
    private Integer ebParamEmailNum;

    private String emailName;

    private Boolean createdByMe;

    private Boolean createdByOtherUser;

    private List<String> listEmailEnCopie;

    private Integer emailId;

    private Integer xEcModule;

    private String services;

    private String roles;

    private boolean hidden;

    public Integer getEbParamEmailNum() {
        return ebParamEmailNum;
    }

    public void setEbParamEmailNum(Integer ebParamEmailNum) {
        this.ebParamEmailNum = ebParamEmailNum;
    }

    public String getEmailName() {
        return emailName;
    }

    public void setEmailName(String emailName) {
        this.emailName = emailName;
    }

    public Boolean getCreatedByMe() {
        return createdByMe;
    }

    public void setCreatedByMe(Boolean createdByMe) {
        this.createdByMe = createdByMe;
    }

    public Boolean getCreatedByOtherUser() {
        return createdByOtherUser;
    }

    public void setCreatedByOtherUser(Boolean createdByOtherUser) {
        this.createdByOtherUser = createdByOtherUser;
    }

    public Integer getEmailId() {
        return emailId;
    }

    public void setEmailId(Integer emailId) {
        this.emailId = emailId;
    }

    public Integer getxEcModule() {
        return xEcModule;
    }

    public void setxEcModule(Integer xEcModule) {
        this.xEcModule = xEcModule;
    }

    public List<String> getListEmailEnCopie() {
        return listEmailEnCopie;
    }

    public void setListEmailEnCopie(List<String> listEmailEnCopie) {
        this.listEmailEnCopie = listEmailEnCopie;
    }

    public String getServices() {
        return services;
    }

    public void setServices(String services) {
        this.services = services;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }
}
