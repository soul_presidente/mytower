/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model.custom;

import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonFormat;


@Table(name = "eb_form_zone")
@Entity
public class EbFormZone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebFormZoneNum;

    private String ebFormName;

    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    @JsonFormat
    private List<EbFormView> listFormView;

    public Integer getEbFormZoneNum() {
        return ebFormZoneNum;
    }

    public void setEbFormZoneNum(Integer ebFormZoneNum) {
        this.ebFormZoneNum = ebFormZoneNum;
    }

    public String getEbFormName() {
        return ebFormName;
    }

    public void setEbFormName(String ebFormName) {
        this.ebFormName = ebFormName;
    }

    public List<EbFormView> getListFormView() {
        return listFormView;
    }

    public void setListFormView(List<EbFormView> listFormView) {
        this.listFormView = listFormView;
    }
}
