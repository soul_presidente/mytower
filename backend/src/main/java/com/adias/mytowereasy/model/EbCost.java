/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.querydsl.core.annotations.QueryProjection;


@Entity
@Table(name = "eb_cost")
@JsonIgnoreProperties(ignoreUnknown = true)

public class EbCost implements Serializable {
    private static final long serialVersionUID = 4389891831441585544L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebCostNum;

    private String libelle;

    private BigDecimal euroExchangeRate;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private BigDecimal price;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private BigDecimal priceEuro;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer xEcCurrencyNum;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Date dateCreation;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Double priceReal;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Double priceEuroReal;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Integer xEcCurrencyNumReal;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Date dateCreationReal;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Boolean flagOtherReal;

    private Double priceGab;

    private String code;

    private Double euroExchangeRateReal;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Boolean flagOther;

    @ManyToMany
    private List<EbCostCategorie> listCostCategorie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", updatable = false)
    private EbCompagnie xEbCompagnie;

    private String listModeTransport;

    private String libelleCostItem;

    private Boolean actived;

    private Boolean deleted;

    public EbCost() {
    }

    @QueryProjection
    public EbCost(Integer ebCostNum, String code, String libelle, Boolean actived) {
        this.ebCostNum = ebCostNum;
        this.code = code;
        this.libelle = libelle;
        this.actived = actived;
    }

    public String getListModeTransport() {
        return listModeTransport;
    }

    public void setListModeTransport(String listModeTransport) {
        this.listModeTransport = listModeTransport;
    }

    public List<EbCostCategorie> getListCostCategorie() {
        return listCostCategorie;
    }

    public void setListCostCategorie(List<EbCostCategorie> listCostCategorie) {
        this.listCostCategorie = listCostCategorie;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getActived() {
        return actived;
    }

    public void setActived(Boolean actived) {
        this.actived = actived;
    }

    public BigDecimal getEuroExchangeRate() {
        return euroExchangeRate;
    }

    public void setEuroExchangeRate(BigDecimal euroExchangeRate) {
        this.euroExchangeRate = euroExchangeRate;
    }

    public Boolean getFlagOther() {
        return flagOther;
    }

    public void setFlagOther(Boolean flagOther) {
        this.flagOther = flagOther;
    }

    public Integer getEbCostNum() {
        return ebCostNum;
    }

    public void setEbCostNum(Integer ebCostNum) {
        this.ebCostNum = ebCostNum;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPriceEuro() {
        return priceEuro;
    }

    public void setPriceEuro(BigDecimal priceEuro) {
        this.priceEuro = priceEuro;
    }

    public Integer getxEcCurrencyNum() {
        return xEcCurrencyNum;
    }

    public void setxEcCurrencyNum(Integer xEcCurrencyNum) {
        this.xEcCurrencyNum = xEcCurrencyNum;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getPriceReal() {
        return priceReal;
    }

    public void setPriceReal(Double priceReal) {
        this.priceReal = priceReal;
    }

    public Double getPriceEuroReal() {
        return priceEuroReal;
    }

    public void setPriceEuroReal(Double priceEuroReal) {
        this.priceEuroReal = priceEuroReal;
    }

    public Integer getxEcCurrencyNumReal() {
        return xEcCurrencyNumReal;
    }

    public void setxEcCurrencyNumReal(Integer xEcCurrencyNumReal) {
        this.xEcCurrencyNumReal = xEcCurrencyNumReal;
    }

    public Date getDateCreationReal() {
        return dateCreationReal;
    }

    public void setDateCreationReal(Date dateCreationReal) {
        this.dateCreationReal = dateCreationReal;
    }

    public Boolean getFlagOtherReal() {
        return flagOtherReal;
    }

    public void setFlagOtherReal(Boolean flagOtherReal) {
        this.flagOtherReal = flagOtherReal;
    }

    public Double getPriceGab() {
        return priceGab;
    }

    public void setPriceGab(Double priceGab) {
        this.priceGab = priceGab;
    }

    public Double getEuroExchangeRateReal() {
        return euroExchangeRateReal;
    }

    public void setEuroExchangeRateReal(Double euroExchangeRateReal) {
        this.euroExchangeRateReal = euroExchangeRateReal;
    }

    public String getLibelleCostItem() {
        return libelleCostItem;
    }

    public void setLibelleCostItem(String libelleCostItem) {
        this.libelleCostItem = libelleCostItem;
    }

    public EbCompagnie getxEbCompagnie() {
        return xEbCompagnie;
    }

    public void setxEbCompagnie(EbCompagnie xEbCompagnie) {
        this.xEbCompagnie = xEbCompagnie;
    }
}
