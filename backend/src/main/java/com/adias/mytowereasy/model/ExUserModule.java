/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@Entity
@Table(name = "eb_user_module", schema = "work")
@IdClass(ExUserModuleId.class)
@JsonIgnoreProperties({
    "xEcModule"
})
public class ExUserModule implements java.io.Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    private Integer ecModuleNum;
    @Id
    private Integer ebUserNum;

    @ManyToOne
    @JoinColumn(name = "x_ec_module")
    @PrimaryKeyJoinColumn(name = "x_ec_module", referencedColumnName = "ec_module_num")
    private EcModule module;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "x_eb_user")
    @PrimaryKeyJoinColumn(name = "x_eb_user", referencedColumnName = "eb_user_num")
    private EbUser ebUser;

    @Column(name = "access_right")
    private Integer accessRight;

    public ExUserModule() {
    }

    public ExUserModule(Integer module, Integer access, Integer ebUserNum) {
        this.ecModuleNum = module;
        this.module = new EcModule(module);
        this.accessRight = access;
        this.ebUserNum = ebUserNum;
        this.ebUser = new EbUser(ebUserNum);
    }

    public EbUser getEbUser() {
        return ebUser;
    }

    public void setEbUser(EbUser ebUser) {
        this.ebUser = ebUser;
    }

    public EcModule getModule() {
        return module;
    }

    public void setModule(EcModule module) {
        this.module = module;
    }

    public Integer getAccessRight() {
        return accessRight;
    }

    public void setAccessRight(Integer accessRight) {
        this.accessRight = accessRight;
    }

    public Integer getEcModuleNum() {
        return ecModuleNum;
    }

    public void setEcModuleNum(Integer ecModuleNum) {
        this.ecModuleNum = ecModuleNum;
    }

    public Integer getEbUserNum() {
        return ebUserNum;
    }

    public void setEbUserNum(Integer ebUserNum) {
        this.ebUserNum = ebUserNum;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((accessRight == null) ? 0 : accessRight.hashCode());
        // result = prime * result + ((ebUser == null) ? 0 : ebUser.hashCode());
        result = prime * result + ((ebUserNum == null) ? 0 : ebUserNum.hashCode());
        result = prime * result + ((ecModuleNum == null) ? 0 : ecModuleNum.hashCode());
        result = prime * result + ((module == null) ? 0 : module.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        ExUserModule other = (ExUserModule) obj;

        if (accessRight == null) {
            if (other.accessRight != null) return false;
        }
        else if (!accessRight.equals(other.accessRight)) return false;

        if (ebUser == null) {
            if (other.ebUser != null) return false;
        }
        else if (!ebUser.equals(other.ebUser)) return false;

        if (ebUserNum == null) {
            if (other.ebUserNum != null) return false;
        }
        else if (!ebUserNum.equals(other.ebUserNum)) return false;

        if (ecModuleNum == null) {
            if (other.ecModuleNum != null) return false;
        }
        else if (!ecModuleNum.equals(other.ecModuleNum)) return false;

        if (module == null) {
            if (other.module != null) return false;
        }
        else if (!module.equals(other.module)) return false;

        return true;
    }
}
