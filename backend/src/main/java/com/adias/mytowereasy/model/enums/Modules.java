package com.adias.mytowereasy.model.enums;

import java.util.HashMap;
import java.util.Map;


public enum Modules {
    PRICING(1),
    TRANSPORT_MANAGEMENT(2),
    COMPLIANCE_MATRIX(4),
    TRACK(5),
    QUALITY_MANAGEMENT(6),
    FREIGHT_AUDIT(7),
    FREIGHT_ANALYTICS(8),
    CUSTOM(9);

    private int moduleNum;

    private static Map<Integer, Modules> map = new HashMap<Integer, Modules>();

    static {

        for (Modules legEnum: Modules.values()) {
            map.put(legEnum.moduleNum, legEnum);
        }

    }

    private Modules(final int module) {
        moduleNum = module;
    }

    public static Modules valueOf(int moduleNum) {
        return map.get(moduleNum);
    }

    public int getValue() {
        return moduleNum;
    }
}
