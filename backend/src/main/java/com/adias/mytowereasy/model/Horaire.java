/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.Set;


public class Horaire {
    Set<Jour> jours;
    Integer etatHoraireNum;
    Jour horaireAllDays;

    public Set<Jour> getJours() {
        return jours;
    }

    public void setJours(Set<Jour> jours) {
        this.jours = jours;
    }

    public Integer getEtatHoraireNum() {
        return etatHoraireNum;
    }

    public void setEtatHoraireNum(Integer etatHoraireNum) {
        this.etatHoraireNum = etatHoraireNum;
    }

    public Jour getHoraireAllDays() {
        return horaireAllDays;
    }

    public void setHoraireAllDays(Jour horaireAllDays) {
        this.horaireAllDays = horaireAllDays;
    }
}
