/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.lang.reflect.Field;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.NamedEntityGraphs;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.model.custom.EbCustomDeclaration;
import com.adias.mytowereasy.model.tt.EbTtTracing;


@JsonIgnoreProperties({
    "selectedRegimeDouanier", "selectedForDeclaration", "selectedTypeGoods"
})
@NamedEntityGraphs({
    @NamedEntityGraph(name = "graph.EbEbMarchandise.xEbTracing", attributeNodes = @NamedAttributeNode("xEbTracing")),
    @NamedEntityGraph(name = "graph.EbMarchandise.parent", attributeNodes = @NamedAttributeNode("parent")),
    // @NamedEntityGraph(name = "graph.EbEbMarchandise.ebDemande", attributeNodes =
    // @NamedAttributeNode("ebDemande"))
})
@Table(name = "eb_marchandise")
@Entity
public class EbMarchandise extends EbTimeStamps implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebMarchandiseNum;

    private String storageTemperature;

    private String batch;

    private Date batchProductionDate;

    private Date batchExpirationDate;

    private String partNumber;

    private String shippingType;

    private String orderNumber;

    private String hsCode;

    private Double price;

    private String originCountryName;

    private String originCountryCode;

    private Integer xEcCountryOrigin;

    private Double netWeight;

    private Double weight;

    private Double length;

    private Double width;

    private Double heigth;

    private String dg;

    private String dryIce;

    private String classGood;

    private String un;

    private String equipment;

    private String sensitive;

    private String unitReference;
    private String packingList;

    private String customerReference;

    private String quantityDryIce;

    private Integer numberOfUnits;

    private Double volume;

    private String binLocation;

    private Integer dangerousGood;

    private String packaging;

    private Boolean stackable;

    private String comment;

    private String nomArticle;

    private String lot;

    private String sscc;

    private String olpn;

    private String tracking_number;

    private String etat;

    private String pood;

    private String typeMarchandise;

    private Long xEbTypeUnit;

    private Integer xEbTypeConteneur;

    private String typeContainer;

    private String handling_unit;

    private Integer hu_line_number;

    private String lot_quantite;

    private String labelTypeUnit;

    private Integer cptmProcedureNum;

    private Integer cptmRegimeTemporaireNum;

    private String cptmRegimeDouanierLabel;

    private String refDelivery;

    private Integer typeGoodsNum;
    private String typeGoodsLabel;
    private String cptmOperationNumber;

    private String serialNumber;

    private Boolean exportControl;

    private String exportRef;

    private Integer refArticle;

    private String customerOrderReference;

    private Long idEbDelLivraison;

    private Integer exportControlStatut;

    private Duration exportControlDelay;

    private Boolean oversizedItem;

    private Boolean largeItem;

    private String typeOfGoodCode;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private boolean hasChildren;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_parent")
    private EbMarchandise parent;

    @JsonManagedReference
    @OneToMany(mappedBy = "parent", fetch = FetchType.LAZY)
    private List<EbMarchandise> children;

    @OneToOne(mappedBy = "xEbMarchandise", fetch = FetchType.LAZY, optional = false)
    private EbTtTracing xEbTracing;

    @Transient
    private List<EbMarchandise> listParent = new ArrayList<EbMarchandise>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_custom_declaration", insertable = true)
    private EbCustomDeclaration xEbCustomDeclaration;

    // demande & ref de la demande initiale dans le cadre de la consolidation
    private Integer xEbDemandeInitial;
    private String refTransportInitial;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_livraison_line")
    private List<EbLivraisonLine> xEbLivraisonLines;

    private Boolean serialized;

    @Column(columnDefinition = "TEXT")
    private String customFields;

    // champs reservé pour customFields
    @Transient
    @JsonDeserialize
    @JsonSerialize
    private List<CustomFields> listCustomsFields;

    private String itemName;

    private String itemNumber;

    private Double taxableWeight;

    @QueryProjection
    public EbMarchandise(Long xEbTypeUnit, String labelTypeUnit, Integer xEbTypeConteneur) {
        this.xEbTypeUnit = xEbTypeUnit;
        this.labelTypeUnit = labelTypeUnit;
        this.xEbTypeConteneur = xEbTypeConteneur;
    }

    public EbMarchandise(Integer ebMarchandiseNum) {
        this.ebMarchandiseNum = ebMarchandiseNum;
    }

    public String getLot() {
        return lot;
    }

    public String getSscc() {
        return sscc;
    }

    public String getOlpn() {
        return olpn;
    }

    public String getTracking_number() {
        return tracking_number;
    }

    public String getEtat() {
        return etat;
    }

    public String getPood() {
        return pood;
    }

    public String getTypeContainer() {
        return typeContainer;
    }

    public void setTypeContainer(String typeContainer) {
        this.typeContainer = typeContainer;
    }

    public String getNomArticle() {
        return nomArticle;
    }

    public void setNomArticle(String nomArticle) {
        this.nomArticle = nomArticle;
    }

    public void setLot(String lot) {
        this.lot = lot;
    }

    public void setSscc(String sscc) {
        this.sscc = sscc;
    }

    public void setOlpn(String olpn) {
        this.olpn = olpn;
    }

    public void setTracking_number(String tracking_number) {
        this.tracking_number = tracking_number;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }

    public void setPood(String pood) {
        this.pood = pood;
    }

    public EbMarchandise getParent() {
        return parent;
    }

    public void setParent(EbMarchandise parent) {
        this.parent = parent;
    }

    public List<EbMarchandise> getChildren() {
        return children;
    }

    public void setChildren(List<EbMarchandise> children) {
        this.children = children;
    }

    public String getLabelTypeUnit() {
        return labelTypeUnit;
    }

    public void setLabelTypeUnit(String labelTypeUnit) {
        this.labelTypeUnit = labelTypeUnit;
    }

    public Double getTaxableWeight() {
        return taxableWeight;
    }

    public void setTaxableWeight(Double taxableWeight) {
        this.taxableWeight = taxableWeight;
    }

    @Transient
    private String codeAlphaPslCourant;

    @Transient
    private String idUnite;

    @Column(insertable = false, updatable = false)
    @ColumnDefault("now()")
    private Date dateCreation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_demande", nullable = false, updatable = false)
    private EbDemande ebDemande;

    @QueryProjection
    public EbMarchandise() {
    }

    public EbMarchandise(EbMarchandise mr) {
        this.ebMarchandiseNum = mr.getEbMarchandiseNum();
        this.classGood = mr.getClassGood();
        this.comment = mr.getComment();
        this.dangerousGood = mr.getDangerousGood();
        this.dg = mr.getDg();
        this.dryIce = mr.getDryIce();
        this.equipment = mr.getEquipment();
        this.heigth = mr.getHeigth();
        this.length = mr.getLength();
        this.numberOfUnits = mr.getNumberOfUnits();
        this.packaging = mr.getPackaging();
        this.quantityDryIce = mr.getQuantityDryIce();
        this.sensitive = mr.getSensitive();
        this.storageTemperature = mr.getStorageTemperature();
        this.un = mr.getUn();
        this.volume = mr.getVolume();
        this.width = mr.getWidth();
        this.weight = mr.getWeight();
        this.unitReference = mr.getUnitReference();
        this.packingList = mr.getPackingList();
        this.serialNumber = mr.getSerialNumber();
        this.customerReference = mr.getCustomerReference();
        this.partNumber = mr.getPartNumber();
        this.orderNumber = mr.getOrderNumber();
        this.shippingType = mr.getShippingType();
        this.cptmOperationNumber = mr.getCptmOperationNumber();
        this.price = mr.getPrice();
        this.binLocation = mr.getBinLocation();
        this.exportControl = mr.getExportControl();
        this.exportRef = mr.getExportRef();
        this.refArticle = mr.getRefArticle();
        this.customerOrderReference = mr.customerOrderReference;
        this.idEbDelLivraison = mr.idEbDelLivraison;
				this.xEbTypeUnit = mr.getxEbTypeUnit();
				this.stackable = mr.getStackable();
				this.cptmRegimeDouanierLabel = mr.getCptmRegimeDouanierLabel();

        if (mr.getEbDemande() != null) {
            this.ebDemande = new EbDemande();
            this.ebDemande.setEbDemandeNum(mr.getEbDemande().getEbDemandeNum());
            this.ebDemande.setRefTransport(mr.getEbDemande().getRefTransport());
            this.ebDemande.setLibelleOriginCountry(mr.getEbDemande().getLibelleOriginCountry());
            this.ebDemande.setLibelleDestCountry(mr.getEbDemande().getLibelleDestCountry());
            this.ebDemande.setCustomerReference(mr.getEbDemande().getCustomerReference());

            if (this.ebDemande.getXecCurrencyInvoice() != null) {
                this.ebDemande
                    .setXecCurrencyInvoice(
                        new EcCurrency(
                            mr.getEbDemande().getXecCurrencyInvoice().getEcCurrencyNum(),
                            mr.getEbDemande().getXecCurrencyInvoice().getLibelle(),
                            mr.getEbDemande().getXecCurrencyInvoice().getCodeLibelle(),
                            mr.getEbDemande().getXecCurrencyInvoice().getSymbol(),
                            mr.getEbDemande().getXecCurrencyInvoice().getCode()));
            }

        }

    }

    public EbMarchandise(Integer ebMarnchandiseNum, String customerReference) {
        this.ebMarchandiseNum = ebMarnchandiseNum;
        this.customerReference = customerReference;
    }

    @QueryProjection
    public EbMarchandise(
        Integer ebMarchandiseNum,
        String classGood,
        String comment,
        Integer dangerousGood,
        String dg,
        String dryIce,
        String equipement,
        Double heigth,
        Double length,
        Integer numberOfUnits,
        String packaging,
        String quantityDryIce,
        String sensitive,
        String storageTemperature,
        Long xEbTypeUnit,
        String un,
        Double volume,
        Double width,
        String customerReference,
        EbDemande demande,
        Double weight,
        String unitReference,
        Integer ecCurrencyNum,
        String libelle,
        String codeLibelle,
        String symbol,
        String code,
        String cptmOperationNumber,
        String partNumber,
        String serialNumber,
        Double price,
        String binLocation,
        Boolean exportControl,
        String exportRef,
        Boolean oversizedItem,
        Boolean largeItem,
        Duration exportControlDelay,
        String packingList,
        String typeOfGoodCode) {
        this.ebMarchandiseNum = ebMarchandiseNum;
        this.classGood = classGood;
        this.comment = comment;
        this.partNumber = partNumber;
        this.serialNumber = serialNumber;
        this.dangerousGood = dangerousGood;
        this.dg = dg;
        this.dryIce = dryIce;
        this.equipment = equipement;
        this.heigth = heigth;
        this.length = length;
        this.numberOfUnits = numberOfUnits;
        this.packaging = packaging;
        this.quantityDryIce = quantityDryIce;
        this.sensitive = sensitive;
        this.storageTemperature = storageTemperature;
        this.un = un;
        this.volume = volume;
        this.width = width;
        this.weight = weight;
        this.unitReference = unitReference;
        this.packingList = packingList;
        this.typeOfGoodCode = typeOfGoodCode;
        this.customerReference = customerReference;
        this.price = price;
        this.binLocation = binLocation;

        this.exportRef = exportRef;
        this.exportControl = exportControl;
        this.oversizedItem = oversizedItem;
        this.largeItem = largeItem;

        this.exportControlDelay = exportControlDelay;
        this.ebDemande = new EbDemande();

        if (demande != null) {
            this.ebDemande.setNumAwbBol(demande.getNumAwbBol());
            this.ebDemande.setEbDemandeNum(demande.getEbDemandeNum());
            this.ebDemande.setRefTransport(demande.getRefTransport());
            this.ebDemande.setLibelleOriginCountry(demande.getLibelleOriginCountry());
            this.ebDemande.setLibelleDestCountry(demande.getLibelleDestCountry());
            this.ebDemande.setCustomerReference(demande.getCustomerReference());
            this.ebDemande.setXecCurrencyInvoice(new EcCurrency(ecCurrencyNum, libelle, codeLibelle, symbol, code));
        }

        this.cptmOperationNumber = cptmOperationNumber;
    }

    public Integer getEbMarchandiseNum() {
        return ebMarchandiseNum;
    }

    public void setEbMarchandiseNum(Integer ebMarchandiseNum) {
        this.ebMarchandiseNum = ebMarchandiseNum;
    }

    public String getStorageTemperature() {
        return storageTemperature;
    }

    public void setStorageTemperature(String storageTemperature) {
        this.storageTemperature = storageTemperature;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getHeigth() {
        return heigth;
    }

    public void setHeigth(Double heigth) {
        this.heigth = heigth;
    }

    public String getDg() {
        return dg;
    }

    public void setDg(String dg) {
        this.dg = dg;
    }

    public String getDryIce() {
        return dryIce;
    }

    public void setDryIce(String dryIce) {
        this.dryIce = dryIce;
    }

    public String getClassGood() {
        return classGood;
    }

    public void setClassGood(String classGood) {
        this.classGood = classGood;
    }

    public String getUn() {
        return un;
    }

    public void setUn(String un) {
        this.un = un;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getSensitive() {
        return sensitive;
    }

    public void setSensitive(String sensitive) {
        this.sensitive = sensitive;
    }

    public void setUnitReference(String unitReference) {
        this.unitReference = unitReference;
    }

    public String getQuantityDryIce() {
        return quantityDryIce;
    }

    public void setQuantityDryIce(String quantityDryIce) {
        this.quantityDryIce = quantityDryIce;
    }

    public Integer getNumberOfUnits() {
        return numberOfUnits;
    }

    public void setNumberOfUnits(Integer numberOfUnits) {
        this.numberOfUnits = numberOfUnits;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Integer getDangerousGood() {
        return dangerousGood;
    }

    public void setDangerousGood(Integer dangerousGood) {
        this.dangerousGood = dangerousGood;
    }

    public String getPackaging() {
        return packaging;
    }

    public void setPackaging(String packaging) {
        this.packaging = packaging;
    }

    public Boolean getStackable() {
        return stackable;
    }

    public void setStackable(Boolean stackable) {
        this.stackable = stackable;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public EbDemande getEbDemande() {
        return ebDemande;
    }

    public void setEbDemande(EbDemande ebDemande) {
        this.ebDemande = ebDemande;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public String getIdUnite() {
        return idUnite;
    }

    public void setIdUnite(String idUnite) {
        this.idUnite = idUnite;
    }

    public String getUnitReference() {
        return unitReference;
    }

    @PrePersist
    void prePersist() {
        this.dateCreation = new Date();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((classGood == null) ? 0 : classGood.hashCode());
        result = prime * result + ((comment == null) ? 0 : comment.hashCode());
        result = prime * result + ((unitReference == null) ? 0 : unitReference.hashCode());
        result = prime * result + ((dangerousGood == null) ? 0 : dangerousGood.hashCode());
        result = prime * result + ((dg == null) ? 0 : dg.hashCode());
        result = prime * result + ((dryIce == null) ? 0 : dryIce.hashCode());
        result = prime * result + ((ebMarchandiseNum == null) ? 0 : ebMarchandiseNum.hashCode());
        result = prime * result + ((equipment == null) ? 0 : equipment.hashCode());
        result = prime * result + ((heigth == null) ? 0 : heigth.hashCode());
        result = prime * result + ((length == null) ? 0 : length.hashCode());
        result = prime * result + ((numberOfUnits == null) ? 0 : numberOfUnits.hashCode());
        result = prime * result + ((packaging == null) ? 0 : packaging.hashCode());
        result = prime * result + ((quantityDryIce == null) ? 0 : quantityDryIce.hashCode());
        result = prime * result + ((sensitive == null) ? 0 : sensitive.hashCode());
        result = prime * result + ((stackable == null) ? 0 : stackable.hashCode());
        result = prime * result + ((storageTemperature == null) ? 0 : storageTemperature.hashCode());
        result = prime * result + ((un == null) ? 0 : un.hashCode());
        result = prime * result + ((volume == null) ? 0 : volume.hashCode());
        result = prime * result + ((weight == null) ? 0 : weight.hashCode());
        result = prime * result + ((width == null) ? 0 : width.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbMarchandise other = (EbMarchandise) obj;

        if (classGood == null) {
            if (other.classGood != null) return false;
        }
        else if (!classGood.equals(other.classGood)) return false;

        if (comment == null) {
            if (other.comment != null) return false;
        }
        else if (!comment.equals(other.comment)) return false;

        if (unitReference == null) {
            if (other.unitReference != null) return false;
        }
        else if (!unitReference.equals(other.unitReference)) return false;

        if (dangerousGood == null) {
            if (other.dangerousGood != null) return false;
        }
        else if (!dangerousGood.equals(other.dangerousGood)) return false;

        if (dg == null) {
            if (other.dg != null) return false;
        }
        else if (!dg.equals(other.dg)) return false;

        if (dryIce == null) {
            if (other.dryIce != null) return false;
        }
        else if (!dryIce.equals(other.dryIce)) return false;

        if (ebMarchandiseNum == null) {
            if (other.ebMarchandiseNum != null) return false;
        }
        else if (!ebMarchandiseNum.equals(other.ebMarchandiseNum)) return false;

        if (equipment == null) {
            if (other.equipment != null) return false;
        }
        else if (!equipment.equals(other.equipment)) return false;

        if (heigth == null) {
            if (other.heigth != null) return false;
        }
        else if (!heigth.equals(other.heigth)) return false;

        if (length == null) {
            if (other.length != null) return false;
        }
        else if (!length.equals(other.length)) return false;

        if (numberOfUnits == null) {
            if (other.numberOfUnits != null) return false;
        }
        else if (!numberOfUnits.equals(other.numberOfUnits)) return false;

        if (packaging == null) {
            if (other.packaging != null) return false;
        }
        else if (!packaging.equals(other.packaging)) return false;

        if (quantityDryIce == null) {
            if (other.quantityDryIce != null) return false;
        }
        else if (!quantityDryIce.equals(other.quantityDryIce)) return false;

        if (sensitive == null) {
            if (other.sensitive != null) return false;
        }
        else if (!sensitive.equals(other.sensitive)) return false;

        if (stackable == null) {
            if (other.stackable != null) return false;
        }
        else if (!stackable.equals(other.stackable)) return false;

        if (storageTemperature == null) {
            if (other.storageTemperature != null) return false;
        }
        else if (!storageTemperature.equals(other.storageTemperature)) return false;

        if (un == null) {
            if (other.un != null) return false;
        }
        else if (!un.equals(other.un)) return false;

        if (volume == null) {
            if (other.volume != null) return false;
        }
        else if (!volume.equals(other.volume)) return false;

        if (weight == null) {
            if (other.weight != null) return false;
        }
        else if (!weight.equals(other.weight)) return false;

        if (width == null) {
            if (other.width != null) return false;
        }
        else if (!width.equals(other.width)) return false;

        return true;
    }

    public void setProperty(String property, String value) {
        Field field = null;

        try {
            Class<?> c = this.getClass();
            field = c.getDeclaredField(property);

            try {
                field.set(this, value);
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }

        } catch (NoSuchFieldException e1) {
            e1.printStackTrace();
        }

    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Long getxEbTypeUnit() {
        return xEbTypeUnit;
    }

    public void setxEbTypeUnit(Long xEbTypeUnit) {
        this.xEbTypeUnit = xEbTypeUnit;
    }

    public String getHandling_unit() {
        return handling_unit;
    }

    public Integer getHu_line_number() {
        return hu_line_number;
    }

    public String getLot_quantite() {
        return lot_quantite;
    }

    public void setHandling_unit(String handling_unit) {
        this.handling_unit = handling_unit;
    }

    public void setHu_line_number(Integer hu_line_number) {
        this.hu_line_number = hu_line_number;
    }

    public void setLot_quantite(String lot_quantite) {
        this.lot_quantite = lot_quantite;
    }

    public Integer getxEbTypeConteneur() {
        return xEbTypeConteneur;
    }

    public void setxEbTypeConteneur(Integer xEbTypeConteneur) {
        this.xEbTypeConteneur = xEbTypeConteneur;
    }

    public EbTtTracing getxEbTracing() {
        return xEbTracing;
    }

    public void setxEbTracing(EbTtTracing xEbTracing) {
        this.xEbTracing = xEbTracing;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public Date getBatchProductionDate() {
        return batchProductionDate;
    }

    public void setBatchProductionDate(Date batchProductionDate) {
        this.batchProductionDate = batchProductionDate;
    }

    public Date getBatchExpirationDate() {
        return batchExpirationDate;
    }

    public void setBatchExpirationDate(Date batchExpirationDate) {
        this.batchExpirationDate = batchExpirationDate;
    }

    public String getPartNumber() {
        return partNumber;
    }

    public void setPartNumber(String partNumber) {
        this.partNumber = partNumber;
    }

    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    public String getOriginCountryName() {
        return originCountryName;
    }

    public void setOriginCountryName(String originCountryName) {
        this.originCountryName = originCountryName;
    }

    public String getOriginCountryCode() {
        return originCountryCode;
    }

    public void setOriginCountryCode(String originCountryCode) {
        this.originCountryCode = originCountryCode;
    }

    public Double getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Double netWeight) {
        this.netWeight = netWeight;
    }

    public List<EbMarchandise> getListParent() {
        return listParent;
    }

    public void setListParent(List<EbMarchandise> listParent) {
        this.listParent = listParent;
    }

    public boolean isHasChildren() {
        return hasChildren;
    }

    public void setHasChildren(boolean hasChildren) {
        this.hasChildren = hasChildren;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getTypeMarchandise() {
        return typeMarchandise;
    }

    public void setTypeMarchandise(String typeMarchandise) {
        this.typeMarchandise = typeMarchandise;
    }

    public Integer getCptmProcedureNum() {
        return cptmProcedureNum;
    }

    public void setCptmProcedureNum(Integer cptmProcedureNum) {
        this.cptmProcedureNum = cptmProcedureNum;
    }

    public Integer getCptmRegimeTemporaireNum() {
        return cptmRegimeTemporaireNum;
    }

    public void setCptmRegimeTemporaireNum(Integer cptmRegimeTemporaireNum) {
        this.cptmRegimeTemporaireNum = cptmRegimeTemporaireNum;
    }

    public String getCptmRegimeDouanierLabel() {
        return cptmRegimeDouanierLabel;
    }

    public void setCptmRegimeDouanierLabel(String cptmRegimeDouanierLabel) {
        this.cptmRegimeDouanierLabel = cptmRegimeDouanierLabel;
    }

    public EbCustomDeclaration getxEbCustomDeclaration() {
        return xEbCustomDeclaration;
    }

    public void setxEbCustomDeclaration(EbCustomDeclaration xEbCustomDeclaration) {
        this.xEbCustomDeclaration = xEbCustomDeclaration;
    }

    public String getCodeAlphaPslCourant() {
        return codeAlphaPslCourant;
    }

    public void setCodeAlphaPslCourant(String codeAlphaPslCourant) {
        this.codeAlphaPslCourant = codeAlphaPslCourant;
    }

    public String getRefDelivery() {
        return refDelivery;
    }

    public void setRefDelivery(String refDelivery) {
        this.refDelivery = refDelivery;
    }

    public Integer getTypeGoodsNum() {
        return typeGoodsNum;
    }

    public void setTypeGoodsNum(Integer typeGoodsNum) {
        this.typeGoodsNum = typeGoodsNum;
    }

    public String getTypeGoodsLabel() {
        return typeGoodsLabel;
    }

    public void setTypeGoodsLabel(String typeGoodsLabel) {
        this.typeGoodsLabel = typeGoodsLabel;
    }

    public String getCptmOperationNumber() {
        return cptmOperationNumber;
    }

    public void setCptmOperationNumber(String cptmOperationNumber) {
        this.cptmOperationNumber = cptmOperationNumber;
    }

    public Integer getxEcCountryOrigin() {
        return xEcCountryOrigin;
    }

    public void setxEcCountryOrigin(Integer xEcCountryOrigin) {
        this.xEcCountryOrigin = xEcCountryOrigin;
    }

    public Integer getxEbDemandeInitial() {
        return xEbDemandeInitial;
    }

    public void setxEbDemandeInitial(Integer xEbDemandeInitial) {
        this.xEbDemandeInitial = xEbDemandeInitial;
    }

    public String getRefTransportInitial() {
        return refTransportInitial;
    }

    public void setRefTransportInitial(String refTransportInitial) {
        this.refTransportInitial = refTransportInitial;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getBinLocation() {
        return binLocation;
    }

    public void setBinLocation(String binLocation) {
        this.binLocation = binLocation;
    }

    public Boolean getExportControl() {
        return exportControl;
    }

    public void setExportControl(Boolean exportControl) {
        this.exportControl = exportControl;
    }

    public String getExportRef() {
        return exportRef;
    }

    public Duration getExportControlDelay() {
        return exportControlDelay;
    }

    public void setExportControlDelay(Duration exportControlDelay) {
        this.exportControlDelay = exportControlDelay;
    }

    public Boolean getOversizedItem() {
        return oversizedItem;
    }

    public void setOversizedItem(Boolean oversizedItem) {
        this.oversizedItem = oversizedItem;
    }

    public Boolean getLargeItem() {
        return largeItem;
    }

    public void setLargeItem(Boolean largeItem) {
        this.largeItem = largeItem;
    }

    public void setExportRef(String exportRef) {
        this.exportRef = exportRef;
    }

    public List<EbLivraisonLine> getxEbLivraisonLines() {
        return xEbLivraisonLines;
    }

    public void setxEbLivraisonLines(List<EbLivraisonLine> xEbLivraisonLines) {
        this.xEbLivraisonLines = xEbLivraisonLines;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Integer getRefArticle() {
        return refArticle;
    }

    public void setRefArticle(Integer refArticle) {
        this.refArticle = refArticle;
    }

    public String getCustomerOrderReference() {
        return customerOrderReference;
    }

    public void setCustomerOrderReference(String customerOrderReference) {
        this.customerOrderReference = customerOrderReference;
    }

    public Long getIdEbDelLivraison() {
        return idEbDelLivraison;
    }

    public void setIdEbDelLivraison(Long idEbDelLivraison) {
        this.idEbDelLivraison = idEbDelLivraison;
    }

    public Boolean getSerialized() {
        return serialized;
    }

    public void setSerialized(Boolean serialized) {
        this.serialized = serialized;
    }

    public Integer getExportControlStatut() {
        return exportControlStatut;
    }

    public void setExportControlStatut(Integer exportControlStatus) {
        this.exportControlStatut = exportControlStatus;
    }

    public String getCustomFields() {
        return customFields;
    }

    public void setCustomFields(String customFields) {
        this.customFields = customFields;
    }

    public List<CustomFields> getListCustomsFields() {
        return listCustomsFields;
    }

    public void setListCustomsFields(List<CustomFields> listCustomsFields) {
        this.listCustomsFields = listCustomsFields;
    }

    public String getPackingList() {
        return packingList;
    }

    public void setPackingList(String packingList) {
        this.packingList = packingList;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNumber() {
        return itemNumber;
    }

    public void setItemNumber(String itemNumber) {
        this.itemNumber = itemNumber;
    }

    public String getTypeOfGoodCode() {
        return typeOfGoodCode;
    }

    public void setTypeOfGoodCode(String typeOfGoodCode) {
        this.typeOfGoodCode = typeOfGoodCode;
    }

}
