/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.querydsl.core.annotations.QueryProjection;


// @Entity
// @Table
@JsonIdentityInfo(
    generator = ObjectIdGenerators.PropertyGenerator.class,
    property = "ebCommandeNum",
    scope = EbCommande.class)
public class EbCommande implements java.io.Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebCommandeNum;

    private Double nbrParcel;

    private Double totalWeight;

    private Double totalVolume;

    private Double taxableWeight;

    private Double totalUnits;

    private String customerReference;

    private Double nbDocument;

    @ManyToOne
    @JoinColumn(name = "x_eb_party_dest")
    private EbParty xEbPartyDest;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateDeparture;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateRealPickup;

    private String codeRealTransporteur;

    private String countryDest;

    @OneToMany(mappedBy = "exDemandeCommandeId.xEbCommande")
    private Set<ExDemandeCommande> exDemandeCommandes;

    @Temporal(TemporalType.TIMESTAMP)
    private Date deliveryDate;

    private String referencePo;

    @Transient
    private List<EbMarchandise> listMarchandise = new ArrayList<>();

    @ManyToOne()
    @JoinColumn(name = "x_ec_country")
    private EcCountry xEcCountry;

    private String codeConsignee;

    private Date dateCreation;

    @Transient
    private TtEnumeration.TtPsl pslCourant;

    @Transient
    private String idUnite;

    public EbCommande() {
        super();
    }

    @QueryProjection
    public EbCommande(Integer ebCommandeNum, String customerReference) {
        this.ebCommandeNum = ebCommandeNum;
        this.customerReference = customerReference;
    }

    @QueryProjection
    public EbCommande(
        Integer ebCommandeNum,
        String customerReference,
        EcCountry xEcCountry,
        Date ATD,
        Date ATA,
        Double totalWeight,
        Double totalUnits) {
        this.ebCommandeNum = ebCommandeNum;
        this.customerReference = customerReference;
        this.xEcCountry = xEcCountry;
        this.dateDeparture = ATD;
        this.deliveryDate = ATA;
        this.totalWeight = totalWeight;
        this.totalUnits = totalUnits;
    }

    public Integer getEbCommandeNum() {
        return ebCommandeNum;
    }

    public void setEbCommandeNum(Integer ebCommandeNum) {
        this.ebCommandeNum = ebCommandeNum;
    }

    public Double getNbrParcel() {
        return nbrParcel;
    }

    public void setNbrParcel(Double nbrParcel) {
        this.nbrParcel = nbrParcel;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(Double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public Double getTaxableWeight() {
        return taxableWeight;
    }

    public void setTaxableWeight(Double taxableWeight) {
        this.taxableWeight = taxableWeight;
    }

    public String getCustomerReference() {
        return customerReference;
    }

    public void setCustomerReference(String customerReference) {
        this.customerReference = customerReference;
    }

    public Double getNbDocument() {
        return nbDocument;
    }

    public void setNbDocument(Double nbDocument) {
        this.nbDocument = nbDocument;
    }

    public EbParty getxEbPartyDest() {
        return xEbPartyDest;
    }

    public void setxEbPartyDest(EbParty xEbPartyDest) {
        this.xEbPartyDest = xEbPartyDest;
    }

    public Date getDateDeparture() {
        return dateDeparture;
    }

    public void setDateDeparture(Date dateDeparture) {
        this.dateDeparture = dateDeparture;
    }

    public Date getDateRealPickup() {
        return dateRealPickup;
    }

    public void setDateRealPickup(Date dateRealPickup) {
        this.dateRealPickup = dateRealPickup;
    }

    public String getCodeRealTransporteur() {
        return codeRealTransporteur;
    }

    public void setCodeRealTransporteur(String codeRealTransporteur) {
        this.codeRealTransporteur = codeRealTransporteur;
    }

    public String getCountryDest() {
        return countryDest;
    }

    public void setCountryDest(String countryDest) {
        this.countryDest = countryDest;
    }

    public Set<ExDemandeCommande> getExDemandeCommandes() {
        return exDemandeCommandes;
    }

    public void setExDemandeCommandes(Set<ExDemandeCommande> exDemandeCommandes) {
        this.exDemandeCommandes = exDemandeCommandes;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getReferencePo() {
        return referencePo;
    }

    public void setReferencePo(String referencePo) {
        this.referencePo = referencePo;
    }

    public List<EbMarchandise> getListMarchandise() {
        return listMarchandise;
    }

    public void setListMarchandise(List<EbMarchandise> listMarchandise) {
        this.listMarchandise = listMarchandise;
    }

    public EcCountry getxEcCountry() {
        return xEcCountry;
    }

    public void setxEcCountry(EcCountry xEcCountry) {
        this.xEcCountry = xEcCountry;
    }

    public String getCodeConsignee() {
        return codeConsignee;
    }

    public void setCodeConsignee(String codeConsignee) {
        this.codeConsignee = codeConsignee;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

    public Double getTotalUnits() {
        return totalUnits;
    }

    public void setTotalUnits(Double totalUnits) {
        this.totalUnits = totalUnits;
    }

    public TtEnumeration.TtPsl getPslCourant() {
        return pslCourant;
    }

    public void setPslCourant(TtEnumeration.TtPsl pslCourant) {
        this.pslCourant = pslCourant;
    }

    public String getIdUnite() {
        return idUnite;
    }

    public void setIdUnite(String idUnite) {
        this.idUnite = idUnite;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((codeConsignee == null) ? 0 : codeConsignee.hashCode());
        result = prime * result + ((codeRealTransporteur == null) ? 0 : codeRealTransporteur.hashCode());
        result = prime * result + ((countryDest == null) ? 0 : countryDest.hashCode());
        result = prime * result + ((customerReference == null) ? 0 : customerReference.hashCode());
        result = prime * result + ((dateCreation == null) ? 0 : dateCreation.hashCode());
        result = prime * result + ((dateDeparture == null) ? 0 : dateDeparture.hashCode());
        result = prime * result + ((dateRealPickup == null) ? 0 : dateRealPickup.hashCode());
        result = prime * result + ((deliveryDate == null) ? 0 : deliveryDate.hashCode());
        result = prime * result + ((ebCommandeNum == null) ? 0 : ebCommandeNum.hashCode());
        result = prime * result + ((exDemandeCommandes == null) ? 0 : exDemandeCommandes.hashCode());
        // result = prime * result + ((listMarchandise == null) ? 0 :
        // listMarchandise.hashCode());
        result = prime * result + ((nbDocument == null) ? 0 : nbDocument.hashCode());
        result = prime * result + ((nbrParcel == null) ? 0 : nbrParcel.hashCode());
        result = prime * result + ((referencePo == null) ? 0 : referencePo.hashCode());
        result = prime * result + ((taxableWeight == null) ? 0 : taxableWeight.hashCode());
        result = prime * result + ((totalVolume == null) ? 0 : totalVolume.hashCode());
        result = prime * result + ((totalWeight == null) ? 0 : totalWeight.hashCode());
        result = prime * result + ((xEbPartyDest == null) ? 0 : xEbPartyDest.hashCode());
        result = prime * result + ((xEcCountry == null) ? 0 : xEcCountry.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbCommande other = (EbCommande) obj;

        if (codeConsignee == null) {
            if (other.codeConsignee != null) return false;
        }
        else if (!codeConsignee.equals(other.codeConsignee)) return false;

        if (codeRealTransporteur == null) {
            if (other.codeRealTransporteur != null) return false;
        }
        else if (!codeRealTransporteur.equals(other.codeRealTransporteur)) return false;

        if (countryDest == null) {
            if (other.countryDest != null) return false;
        }
        else if (!countryDest.equals(other.countryDest)) return false;

        if (customerReference == null) {
            if (other.customerReference != null) return false;
        }
        else if (!customerReference.equals(other.customerReference)) return false;

        if (dateCreation == null) {
            if (other.dateCreation != null) return false;
        }
        else if (!dateCreation.equals(other.dateCreation)) return false;

        if (dateDeparture == null) {
            if (other.dateDeparture != null) return false;
        }
        else if (!dateDeparture.equals(other.dateDeparture)) return false;

        if (dateRealPickup == null) {
            if (other.dateRealPickup != null) return false;
        }
        else if (!dateRealPickup.equals(other.dateRealPickup)) return false;

        if (deliveryDate == null) {
            if (other.deliveryDate != null) return false;
        }
        else if (!deliveryDate.equals(other.deliveryDate)) return false;

        if (ebCommandeNum == null) {
            if (other.ebCommandeNum != null) return false;
        }
        else if (!ebCommandeNum.equals(other.ebCommandeNum)) return false;

        if (exDemandeCommandes == null) {
            if (other.exDemandeCommandes != null) return false;
        }
        else if (!exDemandeCommandes.equals(other.exDemandeCommandes)) return false;

        if (listMarchandise == null) {
            if (other.listMarchandise != null) return false;
        }
        else if (!listMarchandise.equals(other.listMarchandise)) return false;

        if (nbDocument == null) {
            if (other.nbDocument != null) return false;
        }
        else if (!nbDocument.equals(other.nbDocument)) return false;

        if (nbrParcel == null) {
            if (other.nbrParcel != null) return false;
        }
        else if (!nbrParcel.equals(other.nbrParcel)) return false;

        if (referencePo == null) {
            if (other.referencePo != null) return false;
        }
        else if (!referencePo.equals(other.referencePo)) return false;

        if (taxableWeight == null) {
            if (other.taxableWeight != null) return false;
        }
        else if (!taxableWeight.equals(other.taxableWeight)) return false;

        if (totalVolume == null) {
            if (other.totalVolume != null) return false;
        }
        else if (!totalVolume.equals(other.totalVolume)) return false;

        if (totalWeight == null) {
            if (other.totalWeight != null) return false;
        }
        else if (!totalWeight.equals(other.totalWeight)) return false;

        if (xEbPartyDest == null) {
            if (other.xEbPartyDest != null) return false;
        }
        else if (!xEbPartyDest.equals(other.xEbPartyDest)) return false;

        if (xEcCountry == null) {
            if (other.xEcCountry != null) return false;
        }
        else if (!xEcCountry.equals(other.xEcCountry)) return false;

        return true;
    }
}
