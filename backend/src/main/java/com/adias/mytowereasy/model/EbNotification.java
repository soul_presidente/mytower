package com.adias.mytowereasy.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.querydsl.core.annotations.QueryProjection;


@Entity
@Table(name = "eb_notification", indexes = {
    @Index(name = "idx_eb_notification_eb_user", columnList = "x_eb_user"),
    @Index(name = "idx_eb_notification_eb_compagnie", columnList = "x_eb_compagnie")
})
public class EbNotification extends EbTimeStamps {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long ebNotificationNum;
    private String header;
    private String body;
    private Integer type;
    private Integer module;
    private Integer numEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_user")
    private EbUser ebUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie")
    private EbCompagnie ebCompagnie;

    public EbNotification() {
    }

    @QueryProjection
    public EbNotification(Long ebNotificationNum, String header, String body, Integer type) {
        super();
        this.ebNotificationNum = ebNotificationNum;
        this.header = header;
        this.body = body;
        this.type = type;
    }

    public Long getEbNotificationNum() {
        return ebNotificationNum;
    }

    public void setEbNotificationNum(Long ebNotificationNum) {
        this.ebNotificationNum = ebNotificationNum;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public EbUser getEbUser() {
        return ebUser;
    }

    public void setEbUser(EbUser ebUser) {
        this.ebUser = ebUser;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    public Integer getModule() {
        return module;
    }

    public void setModule(Integer module) {
        this.module = module;
    }

    public Integer getNumEntity() {
        return numEntity;
    }

    public void setNumEntity(Integer numEntity) {
        this.numEntity = numEntity;
    }
}
