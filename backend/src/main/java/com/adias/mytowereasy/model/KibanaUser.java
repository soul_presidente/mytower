package com.adias.mytowereasy.model;

public class KibanaUser {
    private int id;
    private String username;

    public KibanaUser() {
    }

    public KibanaUser(int id, String username) {
        this.id = id;
        this.username = username;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
