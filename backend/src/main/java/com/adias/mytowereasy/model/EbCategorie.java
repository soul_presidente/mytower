/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedAttributeNode;
import javax.persistence.NamedEntityGraph;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;


@NamedEntityGraph(name = "EbCategorie.detail", attributeNodes = {
    @NamedAttributeNode(value = "labels")
})
@Entity
@Table(name = "eb_categorie", indexes = {
    @Index(name = "idx_eb_categorie_x_eb_etablissement", columnList = "x_eb_etablissement")
})
@JsonIgnoreProperties(ignoreUnknown = true)
public class EbCategorie implements Serializable {
    /** */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebCategorieNum;

    private String libelle;

    private String code;

    private String source;

    private Boolean deleted;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_etablissement", nullable = false, insertable = true, updatable = false)
    private EbEtablissement etablissement;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_compagnie", nullable = true, insertable = true, updatable = false)
    private EbCompagnie compagnie;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private String nomCompagnie;

    @JsonManagedReference
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "categorie")
    private Set<EbLabel> labels;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Boolean isAll;

    public Integer getEbCategorieNum() {
        return ebCategorieNum;
    }

    public void setEbCategorieNum(Integer ebCategorieNums) {
        this.ebCategorieNum = ebCategorieNums;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public EbEtablissement getEtablissement() {
        return etablissement;
    }

    public void setEtablissement(EbEtablissement etablissement) {
        this.etablissement = etablissement;
    }

    public Set<EbLabel> getLabels() {
        return labels;
    }

    public void setLabels(Set<EbLabel> labels) {
        this.labels = labels;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((ebCategorieNum == null) ? 0 : ebCategorieNum.hashCode());
        result = prime * result + ((etablissement == null) ? 0 : etablissement.hashCode());
        // result = prime * result + ((labels == null) ? 0 : labels.hashCode());
        result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
        return result;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbCategorie other = (EbCategorie) obj;

        if (ebCategorieNum == null) {
            if (other.ebCategorieNum != null) return false;
        }
        else if (!ebCategorieNum.equals(other.ebCategorieNum)) return false;

        if (etablissement == null) {
            if (other.etablissement != null) return false;
        }
        else if (!etablissement.equals(other.etablissement)) return false;

        if (labels == null) {
            if (other.labels != null) return false;
        }
        else if (!labels.equals(other.labels)) return false;

        if (libelle == null) {
            if (other.libelle != null) return false;
        }
        else if (!libelle.equals(other.libelle)) return false;

        return true;
    }

    public String getNomCompagnie() {

        if (this.etablissement != null && this.etablissement.getEbCompagnie() != null) {
            this.nomCompagnie = this.etablissement.getEbCompagnie().getNom();
        }

        return nomCompagnie;
    }

    public void setNomCompagnie(String nomCompagnie) {
        this.nomCompagnie = nomCompagnie;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Boolean getIsAll() {
        return isAll;
    }

    public void setIsAll(Boolean isAll) {
        this.isAll = isAll;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public EbCompagnie getCompagnie() {
        return compagnie;
    }

    public void setCompagnie(EbCompagnie compagnie) {
        this.compagnie = compagnie;
    }

    public static EbCategorie getEbCategorieLite(EbCategorie ebCategorie) {
        ebCategorie.setCompagnie(null);
        ebCategorie.setEtablissement(null);

        if (ebCategorie.getLabels() != null) {
            ebCategorie.getLabels().stream().forEach(it -> it.setCategorie(null));
        }

        return ebCategorie;
    }
}
