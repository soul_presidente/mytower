package com.adias.mytowereasy.service.filedownload;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.chanel.model.EbWeekTemplateDay;
import com.adias.mytowereasy.dao.DaoPlanTransportNew;
import com.adias.mytowereasy.dao.DaoTypeRequest;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbFlag;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.model.EbPlanTransportNew;
import com.adias.mytowereasy.model.EbTypeRequest;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCountry;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.model.Enumeration.JourEnum;
import com.adias.mytowereasy.model.Enumeration.MarchandiseDangerousGood;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.Enumeration.TransportConfirmation;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.repository.EcCountryRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.ListStatiqueService;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.category.CategoryService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class FiledownloadLoadPlanningServiceImpl extends MyTowerService implements FiledownloadLoadPlanningService {
    @Autowired
    DaoPlanTransportNew daoPlanTransportNew;
    @Autowired
    ConnectedUserService connectedUserService;
    @Autowired
    EcCountryRepository ecCountryRepository;
    @Autowired
    ListStatiqueService serviceListStatique;

    @Autowired
    DaoTypeRequest daoTypeRequest;

    @Autowired
    CategoryService categoryService;

    @Override
    public FileDownloadStateAndResult listFileDownloadLoadPlanning(
        SearchCriteria plantransportCriteria,
        EbUser connectedUser,
        List<Map<String, String>> dtConfig,
        List<EbCategorie> listCategorie,
        Map<String, String> mapCustomField,
        Map<String, List<Integer>> mapCategoryFields)
        throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();

        plantransportCriteria.setConnectedUserNum(connectedUser.getEbUserNum());
        plantransportCriteria.setEbUserNum(connectedUser.getEbUserNum());

        List<EbPlanTransportNew> listPlanTransportNew = daoPlanTransportNew.list(plantransportCriteria);

        if (!mapCategoryFields.isEmpty()) plantransportCriteria.setMapCategoryFields(mapCategoryFields);

        Map<String, Object> listcatAndCustom = categoryService
            .getListCategoryAndCustomField(plantransportCriteria, connectedUser);

        listCategorie = (List<EbCategorie>) listcatAndCustom.get("listCategorie");

        for (EbPlanTransportNew planTransportNew: listPlanTransportNew) {
            Map<String, String[]> map = new LinkedHashMap<String, String[]>();
            if (isPropertyInDtConfig(dtConfig, "reference")) map.put("reference", new String[]{
                "input", planTransportNew.getReference() != null ? planTransportNew.getReference().toString() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "label")) map.put("label", new String[]{
                "label", planTransportNew.getLabel() != null ? planTransportNew.getLabel().toString() : ""
            });

            if (isPropertyInDtConfig(dtConfig, "listEbFlagDTO")) {
                String listFlagsLibelle = "";
                List<Integer> listFlagsId = new ArrayList<>();

                if (planTransportNew.getInput().getListFlag() != null
                    && planTransportNew.getInput().getListFlag().length() > 0) {
                    listFlagsId = Arrays
                        .asList(planTransportNew.getInput().getListFlag().split(",")).stream().map(Integer::valueOf)
                        .collect(Collectors.toList());
                    List<EbFlag> listEbFlag = ebFlagRepository.findAllById(listFlagsId);

                    for (EbFlag ebFlag: listEbFlag) {
                        listFlagsLibelle += ebFlag.getName() + ", ";
                    }

                    if (listFlagsLibelle
                        .length() > 0) listFlagsLibelle = listFlagsLibelle.substring(0, listFlagsLibelle.length() - 2);
                }

                map.put("Flag", new String[]{
                    "Flag", listFlagsLibelle
                });
            }

            if (isPropertyInDtConfig(dtConfig, "weekTemplate")) {
                List<String> weekTemplates = new ArrayList<String>();
                StringBuffer weekTemplatebuffer = new StringBuffer();
                String weekTemplate = "";

                if (!planTransportNew.getWeekTemplate().getDays().isEmpty()) {

                    for (int i = 0; i < planTransportNew.getWeekTemplate().getDays().size(); i++) {
                        EbWeekTemplateDay day = planTransportNew.getWeekTemplate().getDays().get(i);

                        if (!day.getHours().isEmpty()) {

                            if (day.getCode() != null) {
                                JourEnum jour = JourEnum.getJourEnumByCode(day.getCode());
                                String jourFormatted = jour.getLibelle();
                                weekTemplates.add(jourFormatted.substring(0, 3));
                                String time = day.getHours().get(0).getStartHour();
                                if (time != null) weekTemplates.add(time);

                                if (weekTemplates.size() > 1) {

                                    for (int j = 1; j < day.getHours().size();) {
                                        time = day.getHours().get(j).getStartHour();
                                        if (time != null) weekTemplates.add(time);
                                        break;
                                    }

                                }

                            }

                        }

                    }

                }

                if (!weekTemplates.isEmpty()) {
                    weekTemplatebuffer.append(weekTemplates.get(0));

                    if (weekTemplates.size() > 1) {

                        for (int i = 1; i < weekTemplates.size(); i++) {
                            weekTemplatebuffer.append("-" + weekTemplates.get(i));
                        }

                    }

                }

                if (weekTemplatebuffer.length() > 0) {
                    weekTemplate = weekTemplatebuffer.toString();
                }

                map.put("Week Templates", new String[]{
                    "Week Templates", (weekTemplate + "  ")

                });
            }

            if (isPropertyInDtConfig(dtConfig, "Mode of transport")
                || isPropertyInDtConfig(dtConfig, "Mode de transport")) {
                String modeOfTransport = "";
                Integer modeOfTransportNum = 0;
                List<String> modeOfTransports = new ArrayList<String>();
                StringBuffer modeOfTransportBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListModeTransport() != null) {

                    for (ModeTransport enumValue: ModeTransport.values()) {
                        List<Integer> listModeOfTransport = new ArrayList<Integer>();
                        listModeOfTransport.addAll(planTransportNew.getInput().getListModeTransport());

                        for (int i = 0; i < listModeOfTransport.size(); i++) {
                            modeOfTransportNum = listModeOfTransport.get(i);
                            if (enumValue
                                .getCode()
                                .equals(modeOfTransportNum)) modeOfTransports.add(enumValue.getLibelle() + " ");
                        }

                    }

                }

                if (!modeOfTransports.isEmpty()) {
                    modeOfTransportBuffer.append(modeOfTransports.get(0));

                    if (modeOfTransports.size() > 1) {

                        for (int i = 1; i < modeOfTransports.size(); i++) {
                            modeOfTransportBuffer.append(" - " + modeOfTransports.get(i));
                        }

                    }

                }

                if (modeOfTransportBuffer.length() > 0) {
                    modeOfTransport = modeOfTransportBuffer.toString();
                }

                map.put("Mode of transport", new String[]{
                    "Mode of transport", modeOfTransport
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Customer reference")
                || isPropertyInDtConfig(dtConfig, "Référence client")) {
                String customerReference = "";
                StringBuffer customerReferenceBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListCustomerReference() != null) {
                    List<String> listCustomerReference = new ArrayList<String>();
                    listCustomerReference.addAll(planTransportNew.getInput().getListCustomerReference());

                    if (!listCustomerReference.isEmpty()) {
                        customerReferenceBuffer.append(listCustomerReference.get(0));

                        if (listCustomerReference.size() > 1) {

                            for (int i = 1; i < listCustomerReference.size(); i++) {
                                customerReferenceBuffer.append(" - " + listCustomerReference.get(i));
                            }

                        }

                    }

                    if (customerReferenceBuffer.length() > 0) {
                        customerReference = customerReferenceBuffer.toString();
                    }

                }

                map.put("Customer reference", new String[]{
                    "Customer reference", customerReference
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Unit reference") || isPropertyInDtConfig(dtConfig, "Référence Unité")) {
                String unitReference = "";
                StringBuffer unitReferenceBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListUnitReference() != null) {
                    List<String> listUnitReference = new ArrayList<String>();
                    listUnitReference.addAll(planTransportNew.getInput().getListUnitReference());

                    if (!listUnitReference.isEmpty()) {
                        unitReferenceBuffer.append(listUnitReference.get(0));

                        if (listUnitReference.size() > 1) {

                            for (int i = 1; i < listUnitReference.size(); i++) {
                                unitReferenceBuffer.append(" - " + listUnitReference.get(i));
                            }

                        }

                    }

                    if (unitReferenceBuffer.length() > 0) {
                        unitReference = unitReferenceBuffer.toString();
                    }

                }

                map.put("Unit reference", new String[]{
                    "Unit reference", unitReference
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Incoterm")) {
                String incoterm = "";
                StringBuffer incotermBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListIncoterm() != null) {
                    List<String> listincoterm = new ArrayList<String>();
                    listincoterm.addAll(planTransportNew.getInput().getListIncoterm());

                    if (!listincoterm.isEmpty()) {
                        incotermBuffer.append(listincoterm.get(0));

                        if (listincoterm.size() > 1) {

                            for (int i = 1; i < listincoterm.size(); i++) {
                                incotermBuffer.append(" - " + listincoterm.get(i));
                            }

                        }

                    }

                    if (incotermBuffer.length() > 0) {
                        incoterm = incotermBuffer.toString();
                    }

                }

                map.put("Incoterm", new String[]{
                    "Incoterm", incoterm
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Carrier's unique Ref Number")
                || isPropertyInDtConfig(dtConfig, "Référence Transporteur Unique")) {
                map.put("Carrier's unique Ref Number", new String[]{
                    "Carrier's unique Ref Number",
                    planTransportNew.getInput().getCarrierUniqRefNum() != null ?
                        planTransportNew.getInput().getCarrierUniqRefNum().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Quotation reference")
                || isPropertyInDtConfig(dtConfig, "Référence Cotation")) {
                String quotationReference = "";
                StringBuffer quotationReferenceBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListTransportRef() != null) {
                    List<String> listquotationReference = new ArrayList<String>();
                    listquotationReference.addAll(planTransportNew.getInput().getListTransportRef());

                    if (!listquotationReference.isEmpty()) {
                        quotationReferenceBuffer.append(listquotationReference.get(0));

                        if (listquotationReference.size() > 1) {

                            for (int i = 1; i < listquotationReference.size(); i++) {
                                quotationReferenceBuffer.append(" - " + listquotationReference.get(i));
                            }

                        }

                    }

                    if (quotationReferenceBuffer.length() > 0) {
                        quotationReference = quotationReferenceBuffer.toString();
                    }

                }

                map.put("Quotation reference", new String[]{
                    "Quotation reference", quotationReference
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Origin company name")
                || isPropertyInDtConfig(dtConfig, "Compagnie d'origine")) {
                map.put("Origin company name", new String[]{
                    "Origin company name",
                    planTransportNew.getInput().getCompanyOrigin() != null ?
                        planTransportNew.getInput().getCompanyOrigin().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Carrier") || isPropertyInDtConfig(dtConfig, "Transporteur")) {
                String carrier = "";
                StringBuffer carrierBuffer = new StringBuffer();
                List<String> listCarrier = new ArrayList<String>();
                Integer ebUserNum = 0;

                if (planTransportNew.getInput().getListCarrier() != null) {
                    List<Integer> listCarrierNum = new ArrayList<Integer>();
                    listCarrierNum.addAll(planTransportNew.getInput().getListCarrier());

                    for (int i = 0; i < listCarrierNum.size(); i++) {
                        ebUserNum = listCarrierNum.get(i);
                        EbUser user = ebUserRepository.findOneByEbUserNum(ebUserNum);

                        if (user.getEbCompagnie().getEbCompagnieNum() != null) {
                            EbCompagnie compagnie = ebCompagnieRepository
                                .findOneByEbCompagnieNum(user.getEbCompagnie().getEbCompagnieNum());
                            listCarrier.add(compagnie.getNom());
                        }

                    }

                    if (!listCarrier.isEmpty()) {
                        carrierBuffer.append(listCarrier.get(0));

                        if (listCarrier.size() > 1) {

                            for (int i = 1; i < listCarrier.size(); i++) {
                                carrierBuffer.append(" - " + listCarrier.get(i));
                            }

                        }

                    }

                    if (carrierBuffer.length() > 0) {
                        carrier = carrierBuffer.toString();
                    }

                }

                map.put("Carrier", new String[]{
                    "Carrier", carrier
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Origin") || isPropertyInDtConfig(dtConfig, "Origine")) {
                String origin = "";
                StringBuffer originBuffer = new StringBuffer();
                Integer ecCountryNum = 0;
                List<String> listOrigins = new ArrayList<String>();

                if (planTransportNew.getInput().getListOrigins() != null) {
                    List<Integer> listOrigin = new ArrayList<Integer>();
                    listOrigin.addAll(planTransportNew.getInput().getListOrigins());

                    for (int i = 0; i < listOrigin.size(); i++) {
                        ecCountryNum = listOrigin.get(i);
                        EcCountry country = ecCountryRepository.getOne(ecCountryNum);
                        listOrigins.add(country.getLibelle());
                    }

                    if (!listOrigins.isEmpty()) {
                        originBuffer.append(listOrigins.get(0));

                        if (listOrigins.size() > 1) {

                            for (int i = 1; i < listOrigins.size(); i++) {
                                originBuffer.append(" - " + listOrigins.get(i));
                            }

                        }

                    }

                    if (originBuffer.length() > 0) {
                        origin = originBuffer.toString();
                    }

                }

                map.put("Origin", new String[]{
                    "Origin", origin
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Destination")) {
                String destination = "";
                Integer ecCountryNumero = 0;
                List<String> listdestinations = new ArrayList<String>();
                StringBuffer destinationBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListDestinations() != null) {
                    List<Integer> listdestination = new ArrayList<Integer>();
                    listdestination.addAll(planTransportNew.getInput().getListDestinations());

                    for (int i = 0; i < listdestination.size(); i++) {
                        ecCountryNumero = listdestination.get(i);
                        EcCountry country = ecCountryRepository.getOne(ecCountryNumero);
                        listdestinations.add(country.getLibelle());
                    }

                    if (!listdestinations.isEmpty()) {
                        destinationBuffer.append(listdestinations.get(0));

                        if (listdestinations.size() > 1) {

                            for (int i = 1; i < listdestinations.size(); i++) {
                                destinationBuffer.append(" - " + listdestinations.get(i));
                            }

                        }

                    }

                    if (destinationBuffer.length() > 0) {
                        destination = destinationBuffer.toString();
                    }

                }

                map.put("Destination", new String[]{
                    "Destination", destination
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Destination city")
                || isPropertyInDtConfig(dtConfig, "Ville d'arrivée")) {
                String destinationCity = "";
                String destinationCityFormatted = "";
                List<String> listdestinationCities = new ArrayList<String>();
                StringBuffer destinationBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListDestinationCity() != null) {
                    List<String> listdestinationCity = new ArrayList<String>();
                    listdestinationCity.addAll(planTransportNew.getInput().getListDestinationCity());

                    for (int i = 0; i < listdestinationCity.size(); i++) {
                        destinationCity = listdestinationCity.get(i).toString().toLowerCase();
                        destinationCityFormatted = destinationCity.substring(0, 1).toUpperCase()
                            + destinationCity.substring(1);
                        listdestinationCities.add(destinationCityFormatted);
                    }

                    if (!listdestinationCities.isEmpty()) {
                        destinationBuffer.append(listdestinationCities.get(0));

                        if (listdestinationCities.size() > 1) {

                            for (int i = 1; i < listdestinationCities.size(); i++) {
                                destinationBuffer.append(" - " + listdestinationCities.get(i));
                            }

                        }

                    }

                    if (destinationBuffer.length() > 0) {
                        destinationCity = destinationBuffer.toString();
                    }

                }

                map.put("Destination City", new String[]{
                    "Destination City", destinationCity
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Origin city") || isPropertyInDtConfig(dtConfig, "Ville de départ")) {
                String originCity = "";
                String originCityFormatted = "";
                List<String> listOrigins = new ArrayList<String>();
                StringBuffer originBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListOriginCity() != null) {
                    List<String> listOriginCity = new ArrayList<String>();
                    listOriginCity.addAll(planTransportNew.getInput().getListOriginCity());

                    for (int i = 0; i < listOriginCity.size(); i++) {
                        originCity = listOriginCity.get(i).toString().toLowerCase();
                        originCityFormatted = originCity.substring(0, 1).toUpperCase() + originCity.substring(1);
                        listOrigins.add(originCityFormatted);
                    }

                    if (!listOrigins.isEmpty()) {
                        originBuffer.append(listOrigins.get(0));

                        if (listOrigins.size() > 1) {

                            for (int i = 1; i < listOrigins.size(); i++) {
                                originBuffer.append(" - " + listOrigins.get(i));
                            }

                        }

                    }

                    if (originBuffer.length() > 0) {
                        originCity = originBuffer.toString();
                    }

                }

                map.put("Origin City", new String[]{
                    "Origin City", originCity
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Cost center") || isPropertyInDtConfig(dtConfig, "Centre de couts")) {
                String costCenter = "";
                StringBuffer costCenterBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListCostCenter() != null) {
                    List<String> listCostCenter = new ArrayList<String>();
                    listCostCenter.addAll(planTransportNew.getInput().getListCostCenter());

                    if (!listCostCenter.isEmpty()) {
                        costCenterBuffer.append(listCostCenter.get(0));

                        if (listCostCenter.size() > 1) {

                            for (int i = 1; i < listCostCenter.size(); i++) {
                                costCenterBuffer.append(" - " + listCostCenter.get(i));
                            }

                        }

                    }

                    if (costCenterBuffer.length() > 0) {
                        costCenter = costCenterBuffer.toString();
                    }

                }

                map.put("Cost Center", new String[]{
                    "Cost Center", costCenter
                });
            }

            if (isPropertyInDtConfig(dtConfig, "SAP Order#") || isPropertyInDtConfig(dtConfig, "Cde SAP")) {
                String sapOrder = "";
                StringBuffer sapOrderBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListNumOrderSAP() != null) {
                    List<String> listsapOrder = new ArrayList<String>();
                    listsapOrder.addAll(planTransportNew.getInput().getListNumOrderSAP());

                    if (!listsapOrder.isEmpty()) {
                        sapOrderBuffer.append(listsapOrder.get(0));

                        if (listsapOrder.size() > 1) {

                            for (int i = 1; i < listsapOrder.size(); i++) {
                                sapOrderBuffer.append(" - " + listsapOrder.get(i));
                            }

                        }

                    }

                    if (sapOrderBuffer.length() > 0) {
                        sapOrder = sapOrderBuffer.toString();
                    }

                }

                map.put("SAP Order#", new String[]{
                    "SAP Order#", sapOrder
                });
            }

            if (isPropertyInDtConfig(dtConfig, "EDI Order#") || isPropertyInDtConfig(dtConfig, "Cde EDI")) {
                String ediOrder = "";
                StringBuffer ediOrderBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListNumOrderEDI() != null) {
                    List<String> listediOrder = new ArrayList<String>();
                    listediOrder.addAll(planTransportNew.getInput().getListNumOrderEDI());

                    if (!listediOrder.isEmpty()) {
                        ediOrderBuffer.append(listediOrder.get(0));

                        if (listediOrder.size() > 1) {

                            for (int i = 1; i < listediOrder.size(); i++) {
                                ediOrderBuffer.append(" - " + listediOrder.get(i));
                            }

                        }

                    }

                    if (ediOrderBuffer.length() > 0) {
                        ediOrder = ediOrderBuffer.toString();
                    }

                }

                map.put("EDI Order#", new String[]{
                    "EDI Order#", ediOrder
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Customer PO#") || isPropertyInDtConfig(dtConfig, "Cde achat client")) {
                String customerPo = "";
                StringBuffer customerPoBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListNumOrderCustomer() != null) {
                    List<String> listcustomerPo = new ArrayList<String>();
                    listcustomerPo.addAll(planTransportNew.getInput().getListNumOrderCustomer());

                    if (!listcustomerPo.isEmpty()) {
                        customerPoBuffer.append(listcustomerPo.get(0));

                        if (listcustomerPo.size() > 1) {

                            for (int i = 1; i < listcustomerPo.size(); i++) {
                                customerPoBuffer.append(" - " + listcustomerPo.get(i));
                            }

                        }

                    }

                    if (customerPoBuffer.length() > 0) {
                        customerPo = customerPoBuffer.toString();
                    }

                }

                map.put("Customer PO#", new String[]{
                    "Customer PO#", customerPo
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Campaign Code") || isPropertyInDtConfig(dtConfig, "Code campagne")) {
                String campaignCode = "";
                StringBuffer campaignCodeBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListCampaignCode() != null) {
                    List<String> listcampaignCode = new ArrayList<String>();
                    listcampaignCode.addAll(planTransportNew.getInput().getListCampaignCode());

                    if (!listcampaignCode.isEmpty()) {
                        campaignCodeBuffer.append(listcampaignCode.get(0));

                        if (listcampaignCode.size() > 1) {

                            for (int i = 1; i < listcampaignCode.size(); i++) {
                                campaignCodeBuffer.append(" - " + listcampaignCode.get(i));
                            }

                        }

                    }

                    if (campaignCodeBuffer.length() > 0) {
                        campaignCode = campaignCodeBuffer.toString();
                    }

                }

                map.put("Campaign Code", new String[]{
                    "Campaign Code", campaignCode
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Campaign Name") || isPropertyInDtConfig(dtConfig, "Nom campagne")) {
                String campaignName = "";
                StringBuffer campaignNameBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListCampaignName() != null) {
                    List<String> listcampaignName = new ArrayList<String>();
                    listcampaignName.addAll(planTransportNew.getInput().getListCampaignName());

                    if (!listcampaignName.isEmpty()) {
                        campaignNameBuffer.append(listcampaignName.get(0));

                        if (listcampaignName.size() > 1) {

                            for (int i = 1; i < listcampaignName.size(); i++) {
                                campaignNameBuffer.append(" - " + listcampaignName.get(i));
                            }

                        }

                    }

                    if (campaignNameBuffer.length() > 0) {
                        campaignName = campaignNameBuffer.toString();
                    }

                }

                map.put("Campaign Name", new String[]{
                    "Campaign Name", campaignName
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Reference Delivery")
                || isPropertyInDtConfig(dtConfig, "Livraison Référence")) {
                String referenceDelivery = "";
                StringBuffer referenceDeliveryBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListReferenceDelivery() != null) {
                    List<String> listreferenceDelivery = new ArrayList<String>();
                    listreferenceDelivery.addAll(planTransportNew.getInput().getListReferenceDelivery());

                    if (!listreferenceDelivery.isEmpty()) {
                        referenceDeliveryBuffer.append(listreferenceDelivery.get(0));

                        if (listreferenceDelivery.size() > 1) {

                            for (int i = 1; i < listreferenceDelivery.size(); i++) {
                                referenceDeliveryBuffer.append(" - " + listreferenceDelivery.get(i));
                            }

                        }

                    }

                    if (referenceDeliveryBuffer.length() > 0) {
                        referenceDelivery = referenceDeliveryBuffer.toString();
                    }

                }

                map.put("Reference Delivery", new String[]{
                    "Reference Delivery", referenceDelivery
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Ref Customer Delivery")
                || isPropertyInDtConfig(dtConfig, "Réf Livraison Client")) {
                String refCustomerDelivery = "";
                StringBuffer refCustomerDeliveryBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListRefCustomerDelivery() != null) {
                    List<String> listrefCustomerDelivery = new ArrayList<String>();
                    listrefCustomerDelivery.addAll(planTransportNew.getInput().getListRefCustomerDelivery());

                    if (!listrefCustomerDelivery.isEmpty()) {
                        refCustomerDeliveryBuffer.append(listrefCustomerDelivery.get(0));

                        if (listrefCustomerDelivery.size() > 1) {

                            for (int i = 1; i < listrefCustomerDelivery.size(); i++) {
                                refCustomerDeliveryBuffer.append(" - " + listrefCustomerDelivery.get(i));
                            }

                        }

                    }

                    if (refCustomerDeliveryBuffer.length() > 0) {
                        refCustomerDelivery = refCustomerDeliveryBuffer.toString();
                    }

                }

                map.put("Ref Customer Delivery", new String[]{
                    "Ref Customer Delivery", refCustomerDelivery
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Name Customer Delivery")
                || isPropertyInDtConfig(dtConfig, "Nom Livraison Client")) {
                String nameCustomerDelivery = "";
                StringBuffer nameCustomerDeliveryBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListNameCustomerDelivery() != null) {
                    List<String> listnameCustomerDelivery = new ArrayList<String>();
                    listnameCustomerDelivery.addAll(planTransportNew.getInput().getListNameCustomerDelivery());

                    if (!listnameCustomerDelivery.isEmpty()) {
                        nameCustomerDeliveryBuffer.append(listnameCustomerDelivery.get(0));

                        if (listnameCustomerDelivery.size() > 1) {

                            for (int i = 1; i < listnameCustomerDelivery.size(); i++) {
                                nameCustomerDeliveryBuffer.append(" - " + listnameCustomerDelivery.get(i));
                            }

                        }

                    }

                    if (nameCustomerDeliveryBuffer.length() > 0) {
                        nameCustomerDelivery = nameCustomerDeliveryBuffer.toString();
                    }

                }

                map.put("Name Customer Delivery", new String[]{
                    "Name Customer Delivery", nameCustomerDelivery
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Requested Delivery Date from")
                || isPropertyInDtConfig(dtConfig, "Date Livraison Demandée de")) {
                map.put("Requested Delivery Date from", new String[]{
                    "Requested Delivery Date from",
                    planTransportNew.getInput().getRequestedDeliveryDateFrom() != null ?
                        planTransportNew.getInput().getRequestedDeliveryDateFrom().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Requested Delivery Date to")
                || isPropertyInDtConfig(dtConfig, "Date Livraison Demandée à")) {
                map.put("Requested Delivery Date to", new String[]{
                    "Requested Delivery Date to",
                    planTransportNew.getInput().getRequestedDeliveryDateTo() != null ?
                        planTransportNew.getInput().getRequestedDeliveryDateTo().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Lot #")) {
                String lot = "";
                StringBuffer lotBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListLot() != null) {
                    List<String> listLot = new ArrayList<String>();
                    listLot.addAll(planTransportNew.getInput().getListLot());

                    if (!listLot.isEmpty()) {
                        lotBuffer.append(listLot.get(0));

                        if (listLot.size() > 1) {

                            for (int i = 1; i < listLot.size(); i++) {
                                lotBuffer.append(" - " + listLot.get(i));
                            }

                        }

                    }

                    if (lotBuffer.length() > 0) {
                        lot = lotBuffer.toString();
                    }

                }

                map.put("Lot #", new String[]{
                    "Lot #", lot
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Transport confirmation")
                || isPropertyInDtConfig(dtConfig, "Prise en charge")) {
                String transportConfirmation = "";
                Integer transportConfirmationNum = 0;
                List<String> listtransportConfirmation = new ArrayList<String>();
                StringBuffer transportConfirmationBuffer = new StringBuffer();

                if (planTransportNew.getInput().getTransportConfirmation() != null) {
                    List<Integer> listTransportConfirmation = new ArrayList<Integer>();
                    listTransportConfirmation.addAll(planTransportNew.getInput().getTransportConfirmation());

                    for (int i = 0; i < listTransportConfirmation.size(); i++) {
                        transportConfirmationNum = listTransportConfirmation.get(i);

                        for (TransportConfirmation enumValue: TransportConfirmation.values()) {
                            if (enumValue
                                .getCode().equals(transportConfirmationNum)) transportConfirmation = enumValue.getKey();
                        }

                        listtransportConfirmation.add(transportConfirmation);
                    }

                    if (!listtransportConfirmation.isEmpty()) {
                        transportConfirmationBuffer.append(listtransportConfirmation.get(0));

                        if (listtransportConfirmation.size() > 1) {

                            for (int i = 1; i < listtransportConfirmation.size(); i++) {
                                transportConfirmationBuffer.append(" - " + listtransportConfirmation.get(i));
                            }

                        }

                    }

                    if (transportConfirmationBuffer.length() > 0) {
                        transportConfirmation = transportConfirmationBuffer.toString();
                    }

                }

                map.put("Transport confirmation", new String[]{
                    "Transport confirmation", transportConfirmation
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Creation date from")
                || isPropertyInDtConfig(dtConfig, "Date de création de")) {
                map.put("Creation date from", new String[]{
                    "Creation date from",
                    planTransportNew.getInput().getDateCreationFrom() != null ?
                        planTransportNew.getInput().getDateCreationFrom().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Creation date to")
                || isPropertyInDtConfig(dtConfig, "Date de création à")) {
                map.put("Creation date to", new String[]{
                    "Creation date to",
                    planTransportNew.getInput().getDateCreationTo() != null ?
                        planTransportNew.getInput().getDateCreationTo().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Service Level")
                || isPropertyInDtConfig(dtConfig, "Niveau de service")) {
                String serviceLevel = "";
                Integer listTypeDemandeNum = 0;
                List<String> listserviceLevel = new ArrayList<String>();
                StringBuffer serviceLevelBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListTypeDemande() != null) {
                    List<Integer> listTypeDemande = new ArrayList<Integer>();
                    listTypeDemande.addAll(planTransportNew.getInput().getListTypeDemande());

                    for (int i = 0; i < listTypeDemande.size(); i++) {
                        listTypeDemandeNum = listTypeDemande.get(i);
                        List<EbTypeRequest> listTypeRequest = daoTypeRequest.searchEbTypeRequest(plantransportCriteria);

                        for (EbTypeRequest ebTypeRequest: listTypeRequest) {
                            if (ebTypeRequest
                                .getEbTypeRequestNum()
                                .equals(listTypeDemandeNum)) serviceLevel = ebTypeRequest.getReference();
                        }

                        listserviceLevel.add(serviceLevel);
                    }

                    if (!listserviceLevel.isEmpty()) {
                        serviceLevelBuffer.append(listserviceLevel.get(0));

                        if (listserviceLevel.size() > 1) {

                            for (int i = 1; i < listserviceLevel.size(); i++) {
                                serviceLevelBuffer.append(" - " + listserviceLevel.get(i));
                            }

                        }

                    }

                    if (serviceLevelBuffer.length() > 0) {
                        serviceLevel = serviceLevelBuffer.toString();
                    }

                }

                map.put("Service Level", new String[]{
                    "Service Level", serviceLevel
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Status") || isPropertyInDtConfig(dtConfig, "Statut")) {
                String status = "";
                Integer statusNum = 0;
                List<String> liststatus = new ArrayList<String>();
                StringBuffer statusBuffer = new StringBuffer();

                if (planTransportNew.getInput().getListStatut() != null) {
                    List<Integer> listStatus = new ArrayList<Integer>();
                    listStatus.addAll(planTransportNew.getInput().getListStatut());

                    for (int i = 0; i < listStatus.size(); i++) {
                        statusNum = listStatus.get(i);

                        for (StatutDemande enumValue: StatutDemande.values()) {
                            if (enumValue.getCode().equals(statusNum)) status = enumValue.getLibelle();
                        }

                        liststatus.add(status);
                    }

                    if (!liststatus.isEmpty()) {
                        statusBuffer.append(liststatus.get(0));

                        if (liststatus.size() > 1) {

                            for (int i = 1; i < liststatus.size(); i++) {
                                statusBuffer.append(" - " + liststatus.get(i));
                            }

                        }

                    }

                    if (statusBuffer.length() > 0) {
                        status = statusBuffer.toString();
                    }

                }

                map.put("status", new String[]{
                    "Status", status
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Acknowledged by CT")
                || isPropertyInDtConfig(dtConfig, "Accusé réception par TDC")) {
                map.put("Acknowledged by CT", new String[]{
                    "Acknowledged by CT",
                    planTransportNew.getInput().getAcknoledgeByCt() != null ?
                        planTransportNew.getInput().getAcknoledgeByCt().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Acknowledge by CT date")
                || isPropertyInDtConfig(dtConfig, "Date d'accusé de réception par TDC")) {
                map.put("Acknowledge by CT date", new String[]{
                    "Acknowledge by CT date",
                    planTransportNew.getInput().getAcknoledgeByCtDate() != null ?
                        planTransportNew.getInput().getAcknoledgeByCtDate().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Pickup from")) {
                map.put("Pickup from", new String[]{
                    "Pickup from",
                    planTransportNew.getInput().getDatePickUpFrom() != null ?
                        planTransportNew.getInput().getDatePickUpFrom().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Pickup to")) {
                map.put("Pickup to", new String[]{
                    "Pickup to",
                    planTransportNew.getInput().getDatePickUpTo() != null ?
                        planTransportNew.getInput().getDatePickUpTo().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Dangerous") || isPropertyInDtConfig(dtConfig, "Dangereux")) {
                String dangerous = "";
                Integer dangerousNum = 0;
                List<String> listdangerous = new ArrayList<String>();
                StringBuffer dangerousBuffer = new StringBuffer();

                if (planTransportNew.getInput().getDangerousGood() != null) {
                    List<Integer> listDangerous = new ArrayList<Integer>();
                    listDangerous.addAll(planTransportNew.getInput().getDangerousGood());

                    for (int i = 0; i < listDangerous.size(); i++) {
                        dangerousNum = listDangerous.get(i);

                        for (MarchandiseDangerousGood enumValue: MarchandiseDangerousGood.values()) {
                            if (enumValue.getCode().equals(dangerousNum)) dangerous = enumValue.getLibelle();
                        }

                        listdangerous.add(dangerous);
                    }

                    if (!listdangerous.isEmpty()) {
                        dangerousBuffer.append(listdangerous.get(0));

                        if (listdangerous.size() > 1) {

                            for (int i = 1; i < listdangerous.size(); i++) {
                                dangerousBuffer.append(" - " + listdangerous.get(i));
                            }

                        }

                    }

                    if (dangerousBuffer.length() > 0) {
                        dangerous = dangerousBuffer.toString();
                    }

                }

                map.put("Dangerous", new String[]{
                    "Dangerous", dangerous
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Gross Weight Max (kg)")
                || isPropertyInDtConfig(dtConfig, "Poids Brut Max")) {
                map.put("Gross Weight Max (kg)", new String[]{
                    "Gross Weight Max (kg)",
                    planTransportNew.getInput().getMaxWeight() != null ?
                        planTransportNew.getInput().getMaxWeight().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Volume Max (m3)") || isPropertyInDtConfig(dtConfig, "Volume Max")) {
                map.put("Volume Max (m3)", new String[]{
                    "Volume Max (m3)",
                    planTransportNew.getInput().getMaxVolume() != null ?
                        planTransportNew.getInput().getMaxVolume().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Height Max (cm)") || isPropertyInDtConfig(dtConfig, "Hauteur Max")) {
                map.put("Height Max (cm)", new String[]{
                    "Height Max (cm)",
                    planTransportNew.getInput().getMaxHeight() != null ?
                        planTransportNew.getInput().getMaxHeight().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Length Max (cm)") || isPropertyInDtConfig(dtConfig, "Longueur Max")) {
                map.put("Length Max (cm)", new String[]{
                    "Length Max (cm)",
                    planTransportNew.getInput().getMaxLength() != null ?
                        planTransportNew.getInput().getMaxLength().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Width Max (cm)") || isPropertyInDtConfig(dtConfig, "Largeur Max")) {
                map.put("Width Max (cm)", new String[]{
                    "Width Max (cm)",
                    planTransportNew.getInput().getMaxWidth() != null ?
                        planTransportNew.getInput().getMaxWidth().toString() :
                        ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "UN") || isPropertyInDtConfig(dtConfig, "UN")) {
                map.put("UN", new String[]{
                    "UN",
                    planTransportNew.getInput().getUns() != null ? planTransportNew.getInput().getUns().toString() : ""
                });
            }

            if (isPropertyInDtConfig(dtConfig, "Class") || isPropertyInDtConfig(dtConfig, "Classe")) {
                map.put("Class", new String[]{
                    "Class",
                    planTransportNew.getInput().getClassGood() != null ?
                        planTransportNew.getInput().getClassGood().toString() :
                        ""
                });
            }

            if (listCategorie != null && listCategorie.size() > 0) {

                for (EbCategorie cat: listCategorie) {
                    if (isPropertyInDtConfig(dtConfig, "category" + cat.getEbCategorieNum())) map
                        .put("category" + cat.getEbCategorieNum(), new String[]{
                            cat.getLibelle(), null
                        });
                }

            }

            if (listCategorie != null && planTransportNew.getListCategories() != null) {
                String label = "";

                for (EbCategorie categorie: listCategorie) {

                    if (isPropertyInDtConfig(dtConfig, "category" + categorie.getEbCategorieNum())) {

                        for (EbCategorie demCategorie: planTransportNew.getListCategories()) {
                            List<String> listLabels = new ArrayList<String>();
                            StringBuffer categorieBuffer = new StringBuffer();

                            if (demCategorie.getLabels() != null
                                && demCategorie.getEbCategorieNum().equals(categorie.getEbCategorieNum())) {

                                for (EbLabel ebLabel: demCategorie.getLabels()) {
                                    listLabels.add(ebLabel.getLibelle());
                                    // break;
                                }

                                if (!listLabels.isEmpty()) {
                                    categorieBuffer.append(listLabels.get(0));

                                    if (listLabels.size() > 1) {

                                        for (int i = 1; i < listLabels.size(); i++) {
                                            categorieBuffer.append(" - " + listLabels.get(i));
                                        }

                                    }

                                }

                                if (categorieBuffer.length() > 0) {
                                    label = categorieBuffer.toString();
                                }

                                map.put("category" + categorie.getEbCategorieNum(), new String[]{
                                    categorie.getLibelle(), label
                                });
                                break;
                            }

                        }

                    }

                }

            }

            listresult.add(map);
        }

        resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
        resultAndState.setListResult(listresult);
        return resultAndState;
    }
}
