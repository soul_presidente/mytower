package com.adias.mytowereasy.service.filedownload;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.delivery.enumeration.StatusOrder;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.model.EbOrderLine;
import com.adias.mytowereasy.delivery.service.OrderLineService;
import com.adias.mytowereasy.delivery.service.OrderService;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.FileDownloadServiceImpl;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.category.CategoryService;
import com.adias.mytowereasy.util.enums.GenericEnumUtils;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class FiledownloadOrderManagementServiceImp extends MyTowerService
    implements FiledownloadOrderManagementService {
    @Autowired
    private OrderLineService orderLineService;

    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    OrderService orderService;

    @Override
    public FileDownloadStateAndResult listFileDownLoadOrderManagement(
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        SearchCriteria criteria,
        List<EbCategorie> listCategorie,
        Map<String, String> mapCustomField)
        throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();
        Locale locale = connectedUser.getLocaleFromLanguage();

        ObjectMapper objectMapper = new ObjectMapper();
        SearchCriteriaOrder criteriaOrder = objectMapper.convertValue(criteria, SearchCriteriaOrder.class);

        List<EbOrder> orders = orderService.getListOrders(criteriaOrder);
        Map<String, Object> listcatAndCustom = categoryService
            .getListCategoryAndCustomField(criteriaOrder, connectedUser);
        List<EbCustomField> fields = new ArrayList<EbCustomField>();
        fields = (List<EbCustomField>) listcatAndCustom.get("customField");
        listCategorie = (List<EbCategorie>) listcatAndCustom.get("listCategorie");

        if (orders.size() > FileDownloadServiceImpl.EXTRACTION_LIMITE) {
            resultAndState.setState(FileDownLoadStatus.LIMITE_DEPASSE.getCode());
            resultAndState.setListResult(listresult);
        }
        else {

            for (EbOrder order: orders) {
                Map<String, String[]> map = new LinkedHashMap<String, String[]>();
                if (isPropertyInDtConfig(dtConfig, "listEbFlagDTO")) {
                    String listFlagsLibelle = "";

                    if (order.getListEbFlagDTO() != null && order.getListEbFlagDTO().size() > 0) {

                        for (EbFlagDTO ebFlagDTO: order.getListEbFlagDTO()) {
                            listFlagsLibelle += ebFlagDTO.getName() + ", ";
                        }

                    }

                    if (listFlagsLibelle.length()
                        > 0) listFlagsLibelle = listFlagsLibelle.substring(0, listFlagsLibelle.length() - 2);
                    map.put("listEbFlagDTO", new String[]{
                        messageSource.getMessage("pricing_booking.flag", null, locale), listFlagsLibelle
                    });
                }


                if (isPropertyInDtConfig(dtConfig, "xEbPartyOrigin")) map.put("xEbPartyOrigin", new String[]{
                    messageSource.getMessage("pricing_booking.address_from", null, locale),
                    order.getxEbPartyOrigin().getxEcCountry().getLibelle()
                });

                if (isPropertyInDtConfig(dtConfig, "xEbPartyDestination")) map.put("xEbPartyDestination", new String[]{
                    messageSource.getMessage("pricing_booking.address_to", null, locale),
                    order.getxEbPartyDestination().getxEcCountry().getLibelle()
                });

                if (connectedUser.isSuperAdmin()) {

                    if (isPropertyInDtConfig(dtConfig, "xEbEtablissement")) map.put("xEbEtablissement", new String[]{
                        messageSource.getMessage("pricing_booking.etablissement", null, locale),
                        order.getxEbEtablissement().getNom()
                    });
                }

                if (isPropertyInDtConfig(
                    dtConfig,
                    "customerOrderReference")) map.put("customerOrderReference", new String[]{
                        messageSource.getMessage("order.customer_order_reference", null, locale),
                        order.getCustomerOrderReference()
                });
                if (isPropertyInDtConfig(dtConfig, "reference")) map.put("reference", new String[]{
                    messageSource.getMessage("order.reference", null, locale), order.getReference()
                });
                
			map.put("orderStatus", new String[] { "Status",
					messageSource.getMessage(StatusOrder.getKeyByCode(order.getOrderStatus()).get(), null, locale) });

                map.put("creationDate", new String[]{
                    "Date Creation",
                    order.getCustomerCreationDate() != null ?
                        order.getCustomerCreationDate().toString() :
                        ""
                });
                map.put("deliveryDate", new String[]{
                    "Due Date",
                    order.getDeliveryDate() != null ?
                        order.getDeliveryDate().toString() :
                        ""
                });

                if (listCategorie != null && order.getListCategories() != null) {

                    for (EbCategorie categorie: listCategorie) {

                        if (isPropertyInDtConfig(dtConfig, "category" + categorie.getEbCategorieNum())) {

                            for (EbCategorie orderCategorie: order.getListCategories()) {

                                if (orderCategorie.getLabels() != null &&
                                    orderCategorie.getEbCategorieNum().equals(categorie.getEbCategorieNum())) {
                                    String label = null;

                                    for (EbLabel ebLabel: orderCategorie.getLabels()) {
                                        label = ebLabel.getLibelle();
                                    }

                                    map.put("category" + categorie.getEbCategorieNum(), new String[]{
                                        categorie.getLibelle(), label
                                    });
                                }

                            }

                        }

                    }

                }

                if (fields != null && fields.size() > 0) {

                    for (EbCustomField customFields: fields) {
                        List<CustomFields> field = new ArrayList<CustomFields>();

                        field = gson.fromJson(customFields.getFields(), new TypeToken<ArrayList<CustomFields>>() {
                        }.getType());

                        if (field != null && field.size() > 0) {

                            for (CustomFields fie: field) {
                                if (isPropertyInDtConfig(dtConfig, fie.getName())) map.put(fie.getName(), new String[]{
                                    fie.getLabel(), null
                                });
                            }

                        }

                        if (field != null &&
                            order.getListCustomsFields() != null &&
                            (order
                                .getxEbOwnerOfTheRequest().getEbCompagnie().getEbCompagnieNum()
                                .equals(customFields.getxEbCompagnie()))) {

                            for (CustomFields fie: field) {

                                if (isPropertyInDtConfig(dtConfig, fie.getName())) {

                                    for (CustomFields orderField: order.getListCustomsFields()) {

                                        if (orderField.getName().contentEquals(fie.getName())) {
                                            map.put(fie.getName(), new String[]{
                                                fie.getLabel(), orderField.getValue()
                                            });
                                            break;
                                        }

                                    }

                                }

                            }

                        }
                    }

                }
                listresult.add(map);
            }

            resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
            resultAndState.setListResult(listresult);
        }

        return resultAndState;
    }

    @Override
    public FileDownloadStateAndResult listFileDownLoadOrderLineManagement(
        SearchCriteria orderCriteria,
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        Map<String, String> mapCustomField)
        throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();

        List<EbCustomField> fields = new ArrayList<EbCustomField>();

        if (!mapCustomField.isEmpty()) orderCriteria.setMapCustomFields(mapCustomField);

        ObjectMapper objectMapper = new ObjectMapper();
        SearchCriteriaOrder criteriaOrder = objectMapper.convertValue(orderCriteria, SearchCriteriaOrder.class);

        List<EbOrderLine> listorderLine = orderLineService.getOrderLinesForExport(criteriaOrder);
        /* Gestion des champs parametrables */
        Map<String, Object> listcatAndCustom = categoryService
            .getListCategoryAndCustomField(orderCriteria, connectedUser);
        fields = (List<EbCustomField>) listcatAndCustom.get("customField");

        if (listorderLine.size() > FileDownloadServiceImpl.EXTRACTION_LIMITE) {
            resultAndState.setState(FileDownLoadStatus.LIMITE_DEPASSE.getCode());
            resultAndState.setListResult(listresult);
        }
        else {

            for (EbOrderLine orderLine: listorderLine) {
                Map<String, String[]> map = new LinkedHashMap<String, String[]>();

                if (isPropertyInDtConfig(dtConfig, "itemNumber")) map.put("itemNumber", new String[]{
                    "Item Num", orderLine.getItemNumber() != null ? orderLine.getItemNumber().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "itemName")) map.put("itemName", new String[]{
                    "Item Name", orderLine.getItemName() != null ? orderLine.getItemName() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "ebTypeUnitNum")) map.put("ebTypeUnitNum", new String[]{
                    "Type Of Units", orderLine.getEbTypeUnitNum() != null ? orderLine.getEbTypeUnitNum().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "partNumber")) map.put("partNumber", new String[]{
                    "Part Number", orderLine.getPartNumber() != null ? orderLine.getPartNumber() : ""
                });
                /* COLUMNS OF ORDER */

                if (isPropertyInDtConfig(dtConfig, "weight")) map.put("weight", new String[]{
                    "weight Qty", orderLine.getWeight() != null ? orderLine.getWeight().toString() : ""
                });
                if (isPropertyInDtConfig(dtConfig, "width")) map.put("width", new String[]{
                    "width Qty", orderLine.getWidth() != null ? orderLine.getWidth().toString() : ""
                });
                if (isPropertyInDtConfig(dtConfig, "length")) map.put("length", new String[]{
                    "length Qty", orderLine.getLength() != null ? orderLine.getLength().toString() : ""
                });
                if (isPropertyInDtConfig(dtConfig, "height")) map.put("height", new String[]{
                    "height Qty", orderLine.getHeight() != null ? orderLine.getHeight().toString() : ""
                });

                // COLUMNS OF DELIVERY
                if (isPropertyInDtConfig(dtConfig, "quantityOrdered")) map.put("quantityOrdered", new String[]{
                    "Ordered Qty",
                    orderLine.getQuantityOrdered() != null ? orderLine.getQuantityOrdered().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "quantityPlannable")) map.put("quantityPlannable", new String[]{
                    "Plannable Qty",
                    orderLine.getQuantityPlannable() != null ? orderLine.getQuantityPlannable().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "quantityPlanned")) map.put("quantityPlanned", new String[]{
                    "Planned Qty",
                    orderLine.getQuantityPlanned() != null ? orderLine.getQuantityPlanned().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "quantityPending")) map.put("quantityPending", new String[]{
                    "Pending Qty",
                    orderLine.getQuantityPending() != null ? orderLine.getQuantityPending().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "quantityConfirmed")) map.put("quantityConfirmed", new String[]{
                    "Confirmed Qty",
                    orderLine.getQuantityConfirmed() != null ? orderLine.getQuantityConfirmed().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "quantityShipped")) map.put("quantityShipped", new String[]{
                    "Shipped Qty",
                    orderLine.getQuantityShipped() != null ? orderLine.getQuantityShipped().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "quantityDelivered")) map.put("quantityDelivered", new String[]{
                    "Delivered Qty",
                    orderLine.getQuantityDelivered() != null ? orderLine.getQuantityDelivered().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "quantityCanceled")) map.put("quantityCanceled", new String[]{
                    "Canceled Qty",
                    orderLine.getQuantityCanceled() != null ? orderLine.getQuantityCanceled().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "serialNumber")) map.put("serialNumber", new String[]{
                    "Serial Number", orderLine.getSerialNumber() != null ? orderLine.getSerialNumber().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "hsCode")) map.put("hsCode", new String[]{
                    "HS Code", orderLine.getHsCode() != null ? orderLine.getHsCode().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "comment")) map.put("comment", new String[]{
                    "Comment", orderLine.getComment() != null ? orderLine.getComment().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "specification")) map.put("specification", new String[]{
                    "Specification", orderLine.getSpecification() != null ? orderLine.getSpecification().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "price")) map.put("price", new String[]{
                        "Price", orderLine.getPrice() != null ? orderLine.getPrice().toString() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "eccn")) map.put("eccn", new String[]{
                        "Eccn", orderLine.getEccn() != null ? orderLine.getEccn() : ""
                });

                if (isPropertyInDtConfig(dtConfig, "currency")){
                    String currencyLibelle = "";
                    if (orderLine.getCurrency() != null){
                        currencyLibelle = orderLine.getCurrency().getLibelle();
                    }
                    map.put("currency", new String[]{"Currency", currencyLibelle});
                }

                if (fields != null && fields.size() > 0) {

                    for (EbCustomField customFields: fields) {
                        List<CustomFields> field = new ArrayList<CustomFields>();

                        field = gson.fromJson(customFields.getFields(), new TypeToken<ArrayList<CustomFields>>() {
                        }.getType());

                        if (field != null && field.size() > 0) {

                            for (CustomFields fie: field) {
                                if (isPropertyInDtConfig(dtConfig, fie.getName())) map.put(fie.getName(), new String[]{
                                    fie.getLabel(), null
                                });
                            }

                        }

                        if (field != null && orderLine.getListCustomsFields() != null) {

                            for (CustomFields fie: field) {

                                if (isPropertyInDtConfig(dtConfig, fie.getName())) {

                                    for (CustomFields demField: orderLine.getListCustomsFields()) {

                                        if (demField.getName().contentEquals(fie.getName())) {
                                            map.put(fie.getName(), new String[]{
                                                fie.getLabel(), demField.getValue()
                                            });
                                            break;
                                        }

                                    }

                                }

                            }

                        }

                    }

                }

                listresult.add(map);
            }

            resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
            resultAndState.setListResult(listresult);
        }

        return resultAndState;
    }
}
