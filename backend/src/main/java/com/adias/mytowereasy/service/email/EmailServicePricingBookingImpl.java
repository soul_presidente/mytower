/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service.email;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import com.adias.mytowereasy.dao.DaoUser;
import com.adias.mytowereasy.dto.SharingInformationMailParams;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.repository.*;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.ControlRuleService;
import com.adias.mytowereasy.service.ListStatiqueService;
import com.adias.mytowereasy.service.NotificationService;
import com.adias.mytowereasy.template.model.pricing.EbDemandeTPL;
import com.adias.mytowereasy.template.model.pricing.EbMarchandiseTPL;
import com.adias.mytowereasy.util.email.Email;
import com.adias.mytowereasy.util.email.EmailHtmlSender;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class EmailServicePricingBookingImpl implements EmailServicePricingBooking {
    @Autowired
    ListStatiqueService serviceListStatique;

    private EmailHtmlSender emailHtmlSender;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    EbCompagnieRepository ebCompagnieRepository;

    @Autowired
    EbUserRepository ebUserRepository;

    @Autowired
    EbEtablissementRepository ebEtablissementRepository;

    @Autowired
    ExEbDemandeTransporteurRepository exEbDemandeTransporteurRepository;

    @Autowired
    EbDemandeRepository ebDemandeRepository;

    @Autowired
    EbInvoiceRepository ebInvoiceRepository;

    @Autowired
    DaoUser daoUser;

    @Autowired
    @Lazy
    ControlRuleService controlRuleService;

    @Autowired
    private ConnectedUserService connectedUserService;

    @Autowired
    private NotificationService notificationService;

    @Value("${mytowereasy.test.emails}")
    private String testEmails;

    private static final Logger log = LoggerFactory.getLogger(EmailServicePricingBookingImpl.class);

    @Value("${mytowereasy.url.front}")
    private String url;

    @Autowired
    public EmailServicePricingBookingImpl(EmailHtmlSender emailHtmlSender) {
        this.emailHtmlSender = emailHtmlSender;
    }

		@Override
		public void sendEmailQuotationRequest(EbDemande ebDemande, EbUser transporteur)
		{
			try
			{
				Map<String, Object> params = new HashMap<String, Object>();
				Email email = new Email();

				String refTran = ebDemande.getRefTransport();
				String nomUser = ebDemande.getUser().getNom();
				String prenomUser = ebDemande.getUser().getPrenom();
				String typeRequest = ebDemande.getTypeRequestLibelle();

				String[] args = {
					refTran, nomUser, prenomUser, typeRequest
				};

				String subject = "demandealertetransporteur.subject";
				email.setArguments(args);
				email.setSubject(subject);

				params
					.put(
						"url",
						url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString())
					);
				params.put("ebdemande", ebDemande);

				String incoterm = ebDemande.getxEbSchemaPsl() != null ?
					ebDemande.getxEbSchemaPsl().getxEcIncotermLibelle() :
					"";
				params.put("inco", incoterm);
				params.put("transporteur", transporteur);

				Optional<EbCompagnie> cp = ebCompagnieRepository
					.findById(ebDemande.getUser().getEbCompagnie().getEbCompagnieNum());

				String cpName = cp.get().getNom();
				Boolean isDanger = ebDemande.getDg();

				params.put("cpName", cpName);
				params.put("isDanger", isDanger);

				if (
					ebDemande.getExEbDemandeTransporteurs() != null &&
						!ebDemande.getExEbDemandeTransporteurs().isEmpty()
				)
				{
					Integer compagnieNum = null;
					EbUser connectedUser = connectedUserService.getCurrentUser();
					if (connectedUser != null)
						compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

					Map<String, Object> res = serviceListStatique
						.getEmailUsersWithMailParamCheck(
							compagnieNum,
							Enumeration.EmailConfig.QUOTATION_REQUEST.getCode(),
							Enumeration.Module.PRICING.getCode(),
							ebDemande.getEbDemandeNum(),
							false,
							params
						);

					email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_REQUEST);

					notificationService
						.generateAlert(
							email,
							(List<Integer>) res.get("ids"),
							ebDemande.getEbDemandeNum(),
							Enumeration.Module.PRICING.getCode()
						);

					Context context = new Context();
					context.setVariables(params);

					emailHtmlSender
						.sendListEmail(
							(List<String>) res.get("emails"),
							email,
							"quotation/12-email-quotation-request",
							context,
							subject
						);
				}

				if (
					ebDemande.getListEmailSendQuotation() != null &&
						!ebDemande.getListEmailSendQuotation().isEmpty()
				)
				{
					email = new Email();
					String[] emails = ebDemande.getListEmailSendQuotation().split(";");
					List<String> emailList = new ArrayList(Arrays.asList(emails));
					email.setTo(emailList.get(0).toString());
					emailList.remove(0);
					email.setEnCopieCachee(emailList);
					email.setSubject(subject);
					Context context = new Context();
					context.setVariables(params);
					context.setVariable("transporteur", null);
					emailHtmlSender.send(email, "quotation/12-email-quotation-request", context);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		@Override
		public void sendEmailQuotationForwarder(
			EbUser user,
			EbDemande ebDemande,
			List<ExEbDemandeTransporteur> listExEbDemandeTransporteur
		)
		{
			try
			{
				Context params = new Context();
				Email email = new Email();

				String refTran = ebDemande.getRefTransport();

				String[] args = {
					refTran
				};

				String subject = "repdemandeavecrecommendation.subject";
				email.setArguments(args);
				email.setSubject(subject);

				EbUser connectedOne = ebUserRepository.getOne(user.getEbUserNum());
				String societeName = connectedOne.getEbCompagnie().getNom();
				String EtabName = connectedOne.getEbEtablissement().getNom();

				for (ExEbDemandeTransporteur dt : listExEbDemandeTransporteur)
				{
					Integer num = dt.getxEbDemande().getEbDemandeNum();
					Optional<EbDemande> demande = ebDemandeRepository.findById(num);
					if (demande.isPresent())
						dt.setxEbDemande(demande.get());
				}

				params
					.setVariable(
						"url",
						url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString())
					);
				params.setVariable("ebdemande", ebDemande);
				params.setVariable("listExEbDemandeTransporteur", listExEbDemandeTransporteur);
				params.setVariable("societeName", societeName);
				params.setVariable("EtabName", EtabName);

				Boolean isDanger = ebDemande.getDg();

				params.setVariable("inco", ebDemande.getxEcIncotermLibelle());
				params.setVariable("isDanger", isDanger);

				if (ebDemande.getxEbUserCt() != null)
				{
					Map<String, Object> res = serviceListStatique
						.getEmailUsers(
							connectedOne.getEbCompagnie().getEbCompagnieNum(),
							Enumeration.EmailConfig.QUOTATION_FROM_FORWARDER.getCode(),
							Enumeration.Module.PRICING.getCode(),
							ebDemande.getEbDemandeNum()
						);

					notificationService
						.generateAlert(
							email,
							(List<Integer>) res.get("ids"),
							ebDemande.getEbDemandeNum(),
							Enumeration.Module.PRICING.getCode()
						);
					email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_FROM_FORWARDER);
					emailHtmlSender
						.sendListEmail(
							(List<String>) res.get("emails"),
							email,
							"quotation/17-email-quotation-from-forwarder",
							params,
							subject
						);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

    @Override
    public void sendEmailQuotationRecommendation(EbDemande ebDemande) {
        Context params = new Context();
        Email email = new Email();
        String refTran = ebDemande.getRefTransport();

        String[] args = {
            refTran
        };

        String subject = "demandealertetransrecommend.subject";
        email.setArguments(args);
        email.setSubject(subject);

        params.setVariable("url", url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString()));
        params.setVariable("ebdemande", ebDemande);

        /*
         * EbIncoterm inco =
         * serviceListStatique.getIncotermById(ebDemande.getxEcIncoterm());
         * String incoterm = inco.getLibelle();
         */

        params.setVariable("inco", ebDemande.getxEcIncotermLibelle());

        Integer compagnieNum = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();
        if (connectedUser != null) compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                compagnieNum,
                Enumeration.EmailConfig.QUOTATION_RECOMMENDATION.getCode(),
                Enumeration.Module.PRICING.getCode(),
                ebDemande.getEbDemandeNum());

        email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_RECOMMENDATION);
        notificationService
            .generateAlert(
                email,
                (List<Integer>) res.get("ids"),
                ebDemande.getEbDemandeNum(),
                Enumeration.Module.PRICING.getCode());

        emailHtmlSender
            .sendListEmail(
                (List<String>) res.get("emails"),
                email,
                "quotation/16-email-quotation-recommendation",
                params,
                subject);
        // for(String userMail : mails) {
        // email.setTo(userMail);
        // email.setSubject(subject);
        // emailHtmlSender.send(email,
        // "quotation/16-email-quotation-recommendation", params);
        // }

        // email.setTo("");
        // email.setEnCopieCachee(mails);
        // email.setSubject(subject);
        // emailHtmlSender.send(email,
        // "quotation/16-email-quotation-recommendation", params);
    }

    @Override
    public List<String> sendEmailQuotationForwarderSelected(
        EbUser connectedUser,
        EbDemande ebDemande,
        ExEbDemandeTransporteur exEbDemandeTransporteur) {
        Context params = new Context();
        Email email = new Email();

        String refTran = ebDemande.getRefTransport();
        EbEtablissement etab;

        if (exEbDemandeTransporteur.getxEbEtablissement().getNom() != null) {
            etab = exEbDemandeTransporteur.getxEbEtablissement();
        }
        else {
            etab = ebEtablissementRepository
                .getOne(exEbDemandeTransporteur.getxEbEtablissement().getEbEtablissementNum());
        }

        String etabNom = etab.getNom();

        String[] args = {
            refTran, etabNom
        };

        String subject;

        if (exEbDemandeTransporteur.getIsFromTransPlan() != null && exEbDemandeTransporteur.getIsFromTransPlan()) {
            subject = "finalchoicemailfromtransportplan.subject";
            email.setArguments(args);
        }
        else {
            subject = "finalchoicemail.subject";
            email.setArguments(args);
        }

        email.setSubject(subject);
        params.setVariable("url", url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString()));
        params.setVariable("ebdemande", ebDemande);
        params.setVariable("exEbDemandeTransporteur", exEbDemandeTransporteur);
        params.setVariable("nomUser", connectedUser.getNom());
        params.setVariable("prenomUser", connectedUser.getPrenom());
        params.setVariable("etabNom", etabNom);

        params.setVariable("inco", ebDemande.getxEcIncotermLibelle());

        String modeTrans = ebDemande.getModeTransporteur();
        params.setVariable("modeTrans", modeTrans);

        Map<String, Object> mapParams = new HashMap<String, Object>();
        mapParams.put("transporteur", exEbDemandeTransporteur.getxTransporteur());

        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                connectedUser.getEbCompagnie().getEbCompagnieNum(),
                Enumeration.EmailConfig.QUOTATION_FORWARDER_SELECTED.getCode(),
                Enumeration.Module.PRICING.getCode(),
                ebDemande.getEbDemandeNum(),
                mapParams);

        email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_FORWARDER_SELECTED);

        notificationService
            .generateAlert(
                email,
                (List<Integer>) res.get("ids"),
                ebDemande.getEbDemandeNum(),
                Enumeration.Module.PRICING.getCode());

        emailHtmlSender
            .sendListEmail(
                (List<String>) res.get("emails"),
                email,
                "quotation/20-email-quotation-forwarder-selected",
                params,
                subject);

        return (List<String>) res.get("emails");
    }

    @Override
    public void
        sendEmailQuotationForwarderNotSelected(EbDemande ebDemande, ExEbDemandeTransporteur exEbDemandeTransporteur) {
        // log.info("**********sendEmailQuotationForwarderNotSelected");

        Context params = new Context();
        Email email = new Email();

        params.setVariable("ebdemande", ebDemande);

        String refTrans = ebDemande.getRefTransport();

        String[] args = {
            refTrans
        };

        params.setVariable("url", url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString()));

        String subject = "transnotselected.subject";
        email.setArguments(args);
        email.setSubject(subject);

        // email = new Email();
        Map<String, Object> mapParams = new HashMap<String, Object>();
        mapParams.put("transporteur", exEbDemandeTransporteur.getxTransporteur());

        Integer compagnieNum = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();
        if (connectedUser != null) compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                compagnieNum,
                Enumeration.EmailConfig.QUOTATION_FORWARDER_NOT_SELECTED.getCode(),
                Enumeration.Module.PRICING.getCode(),
                ebDemande.getEbDemandeNum(),
                mapParams);

        email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_FORWARDER_NOT_SELECTED);

        /*
         * log.info("**********serviceListStatique.listDestinataireByEmailId");
         * log.info("** liste des emails avant netoyage ");
         * log.info("{}",mails);
         * //on ne doit envoyer cet email qu'au prestataire non selectionné.
         * if(listEmailForwarderSelected != null)
         * mails.removeIf(element ->
         * listEmailForwarderSelected.contains(element));
         * log.info("{}",mails);
         * log.info("** liste des emails après netoyage ");
         */
        notificationService
            .generateAlert(
                email,
                (List<Integer>) res.get("ids"),
                ebDemande.getEbDemandeNum(),
                Enumeration.Module.PRICING.getCode());

        emailHtmlSender
            .sendListEmail(
                (List<String>) res.get("emails"),
                email,
                "quotation/21-email-quotation-forwarder-not-selected",
                params,
                subject);
        // for(String userMail : mails) {
        // email.setTo(userMail);
        // email.setSubject(subject);
        // emailHtmlSender.send(email,
        // "quotation/21-email-quotation-forwarder-not-selected", params);
        // }
        // email.setTo("");
        // email.setEnCopieCachee(mails);
        // emailHtmlSender.send(email,
        // "quotation/21-email-quotation-forwarder-not-selected", params);
        /*
         * for (ExEbDemandeTransporteur exEbDemandeTransporteur :
         * listExEbDemandeTransporteur)
         * {
         * email.setTo(exEbDemandeTransporteur.getxTransporteur().getEmail());
         * email.setSubject(subject);
         * params.setVariable("exEbDemandeTransporteur",
         * exEbDemandeTransporteur);
         * emailHtmlSender.send(email,
         * "quotation/21-email-quotation-forwarder-not-selected", params);
         * }
         */
    }

    @Override
    public void sendEmailQuotationResquestChargeur(EbDemande ebDemande, List<EbUser> transporteurs) {

        try {
            Context params = new Context();

            Email email = new Email();

            String refTrans = ebDemande.getRefTransport();
            String custRef = ebDemande.getCustomerReference();

            if (custRef != null && !custRef.isEmpty()) {
                custRef += " /";
            }
            else {
                custRef = "";
            }

            String[] args = {
                refTrans, custRef
            };

            String subject = "demandealertechargeur.subject";
            email.setArguments(args);
            email.setSubject(subject);
            String incoterm = ebDemande.getxEcIncotermLibelle() != null ? ebDemande.getxEcIncotermLibelle() : "";
            Boolean isDanger = ebDemande.getDg();

            params.setVariable("isDanger", isDanger);
            params.setVariable("ebdemande", ebDemande);
            params.setVariable("refTrans", refTrans);
            params.setVariable("listEbDemandeTransporteurs", transporteurs);

            params
                .setVariable("url", url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString()));
            params.setVariable("inco", incoterm);

            Integer compagnieNum = null;
            EbUser connectedUser = connectedUserService.getCurrentUser();
            if (connectedUser != null) compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

            Map<String, Object> res = serviceListStatique
                .getEmailUsers(
                    compagnieNum,
                    Enumeration.EmailConfig.QUOTATION_REQUEST_CHARGEUR.getCode(),
                    Enumeration.Module.PRICING.getCode(),
                    ebDemande.getEbDemandeNum());

            email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_REQUEST_CHARGEUR);

            notificationService
                .generateAlert(
                    email,
                    (List<Integer>) res.get("ids"),
                    ebDemande.getEbDemandeNum(),
                    Enumeration.Module.PRICING.getCode());

            params.setVariable("demandeId", ebDemande.getEbDemandeNum());
            emailHtmlSender
                .sendListEmail(
                    (List<String>) res.get("emails"),
                    email,
                    "quotation/13-email-quotation-request-chargeur",
                    params,
                    subject);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void sendEmailPricingCancelled(EbDemande ebDemande) {
        Context params = new Context();

        Email email = new Email();

        String refTrans = ebDemande.getRefTransport();

        String[] args = {
            refTrans
        };

        String subject = "demandealertecancel.subject";
        email.setArguments(args);
        email.setSubject(subject);

        params.setVariable("ebdemande", ebDemande);
        params.setVariable("inco", ebDemande.getxEcIncotermLibelle());
        Integer module = ebDemande.getxEcStatut() >= StatutDemande.FIN.getCode() ?
            Enumeration.Module.TRANSPORT_MANAGEMENT.getCode() :
            Enumeration.Module.PRICING.getCode();

        String link = module.equals(Enumeration.Module.PRICING.getCode()) ? "pricing" : "transport-management";
        params
            .setVariable("url", url.concat("app/" + link + "/details/").concat(ebDemande.getEbDemandeNum().toString()));

        Integer compagnieNum = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();
        if (connectedUser != null) compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                compagnieNum,
                Enumeration.EmailConfig.QUOTATION_CANCELLED.getCode(),
                module,
                ebDemande.getEbDemandeNum());

        notificationService.generateAlert(email, (List<Integer>) res.get("ids"), ebDemande.getEbDemandeNum(), module);

        email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_CANCELLED);

        emailHtmlSender
            .sendListEmail(
                (List<String>) res.get("emails"),
                email,
                "quotation/39-email-quotation-cancelled",
                params,
                subject);
    }

		@Override
		public void sendMailQuotationResponseTransporteur(
			EbUser user,
			EbDemande ebDemande,
			List<ExEbDemandeTransporteur> listExEbDemandeTransporteur
		)
		{
			try
			{
				Context params = new Context();
				Email email = new Email();

				String refTran = ebDemande.getRefTransport();

				String etabChargeurName = ebDemande.getxEbEtablissement().getNom();

				String CpName = ebDemande.getxEbCompagnie().getNom();

				String[] args = {
					refTran
				};

				String subject = "demandealertetransreponse.subject";
				email.setArguments(args);
				email.setSubject(subject);

				params
					.setVariable(
						"url",
						url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString())
					);
				params.setVariable("ebdemande", ebDemande);
				params.setVariable("listExEbDemandeTransporteur", listExEbDemandeTransporteur);
				params.setVariable("etabChargeurName", etabChargeurName);
				params.setVariable("CpName", CpName);

				params.setVariable("inco", ebDemande.getxEcIncotermLibelle());
				Optional<ExEbDemandeTransporteur> exEbDemandeTransporteur = listExEbDemandeTransporteur
					.stream()
					.filter(
						transporteur -> transporteur
							.getxTransporteur()
							.getEbUserNum()
							.equals(user.getEbUserNum())
					)
					.findAny();

				Map<String, Object> mapParams = new HashMap<String, Object>();

				if (exEbDemandeTransporteur.isPresent())
				{
					mapParams.put("transporteur", exEbDemandeTransporteur.get().getxTransporteur());
				}

				Integer compagnieNum = null;
				EbUser connectedUser = connectedUserService.getCurrentUser();
				if (connectedUser != null)
					compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

				Map<String, Object> res = serviceListStatique
					.getEmailUsers(
						compagnieNum,
						Enumeration.EmailConfig.REPONSE_TRANSPORTEUR.getCode(),
						Enumeration.Module.PRICING.getCode(),
						ebDemande.getEbDemandeNum(),
						mapParams
					);

				email.setEmailConfig(Enumeration.EmailConfig.REPONSE_TRANSPORTEUR);

				notificationService
					.generateAlert(
						email,
						(List<Integer>) res.get("ids"),
						ebDemande.getEbDemandeNum(),
						Enumeration.Module.PRICING.getCode()
					);
				emailHtmlSender
					.sendListEmail(
						(List<String>) res.get("emails"),
						email,
						"quotation/15-reponse-tranporteur",
						params,
						subject
					);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		@Override
		public void sendMailQuotationAlerteCt(
			EbUser user,
			EbDemande ebDemande,
			ExEbDemandeTransporteur exEbDemandeTransporteur
		)
		{
			try
			{
				Context params = new Context();
				Email email = new Email();

				String refTran = ebDemande.getRefTransport();
				String customerRef = ebDemande.getCustomerReference();

				String[] args = {
					refTran, customerRef
				};

				Optional<EbDemande> demande = ebDemandeRepository.findById(ebDemande.getEbDemandeNum());
				if (demande.isPresent())
					exEbDemandeTransporteur.setxEbDemande(demande.get());

				String subject = "demandealertect.subject";
				email.setArguments(args);
				email.setSubject(subject);

				EbUser transporteur = ebUserRepository
					.getOneByEbUserNum(exEbDemandeTransporteur.getxTransporteur().getEbUserNum());

				params
					.setVariable(
						"url",
						url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString())
					);
				params.setVariable("ebdemande", ebDemande);
				params.setVariable("transporteur", transporteur);
				params.setVariable("exEbDemandeTransporteur", exEbDemandeTransporteur);

				params.setVariable("inco", ebDemande.getxEcIncotermLibelle());

				Integer compagnieNum = null;
				EbUser connectedUser = connectedUserService.getCurrentUser();
				if (connectedUser != null)
					compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

				Map<String, Object> res = serviceListStatique
					.getEmailUsers(
						compagnieNum,
						Enumeration.EmailConfig.QUOTATION_ALRTE_CT.getCode(),
						Enumeration.Module.PRICING.getCode(),
						ebDemande.getEbDemandeNum()
					);

				email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_ALRTE_CT);

				notificationService
					.generateAlert(
						email,
						(List<Integer>) res.get("ids"),
						ebDemande.getEbDemandeNum(),
						Enumeration.Module.PRICING.getCode()
					);

				emailHtmlSender
					.sendListEmail(
						(List<String>) res.get("emails"),
						email,
						"quotation/34-email-quotation-alerte-ct",
						params,
						subject
					);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		@Override
		public void sendMailQuotationResponseChargeur(
			EbUser user,
			EbDemande ebDemande,
			List<ExEbDemandeTransporteur> listExEbDemandeTransporteur
		)
		{
			try
			{
				Context params = new Context();
				Email email = new Email();

				String refTran = ebDemande.getRefTransport();

				SearchCriteria criterias = new SearchCriteria();
				criterias.setEbUserNum(user.getEbUserNum());

				EbUser userC = daoUser.findUser(criterias);
				String etabName = userC.getEbEtablissement().getNom();
				String cpName = userC.getEbCompagnie().getNom();

				List<ExEbDemandeTransporteur> listDemandeQuote = new ArrayList<ExEbDemandeTransporteur>();

				for (ExEbDemandeTransporteur dt : listExEbDemandeTransporteur)
				{
					ExEbDemandeTransporteur tr = new ExEbDemandeTransporteur();
					tr.setPrice(dt.getPrice());
					tr.setPickupTime(dt.getPickupTime());
					tr.setDeliveryTime(dt.getDeliveryTime());
					tr.setxTransporteur(userC);
					tr.setListEbTtCompagniePsl(dt.getListEbTtCompagniePsl());
					listDemandeQuote.add(tr);
				}

				String[] args = {
					refTran, cpName
				};

				String subject = "demandealertetransreponsechargeur.subject";
				email.setArguments(args);
				email.setSubject(subject);

				params
					.setVariable(
						"url",
						url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString())
					);
				params.setVariable("ebdemande", ebDemande);
				params.setVariable("listExEbDemandeTransporteur", listDemandeQuote);

				params.setVariable("inco", ebDemande.getxEcIncotermLibelle());

				params.setVariable("etabName", etabName);
				params.setVariable("cpName", cpName);

				Integer compagnieNum = null;
				EbUser connectedUser = connectedUserService.getCurrentUser();
				if (connectedUser != null)
					compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();
				Map<String, Object> res = serviceListStatique
					.getEmailUsers(
						compagnieNum,
						Enumeration.EmailConfig.REPONSE_FROM_CHARGEUR.getCode(),
						Enumeration.Module.PRICING.getCode(),
						ebDemande.getEbDemandeNum()
					);

				notificationService
					.generateAlert(
						email,
						(List<Integer>) res.get("ids"),
						ebDemande.getEbDemandeNum(),
						Enumeration.Module.PRICING.getCode()
					);

				email.setEmailConfig(Enumeration.EmailConfig.REPONSE_FROM_CHARGEUR);

				emailHtmlSender
					.sendListEmail(
						(List<String>) res.get("emails"),
						email,
						"quotation/14-reponse-from-chargeur",
						params,
						subject
					);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

    @Override
    public void sendEmailTranpsportInformation(EbDemande ebDemande, EbUser user) {
        // TODO Auto-generated method stub
        Context params = new Context();
        Email email = new Email();

        String refTran = ebDemande.getRefTransport();

        String[] args = {
            refTran
        };

        String subject = "renseigninfostrans.subject";
        email.setArguments(args);
        email.setSubject(subject);

        params
            .setVariable(
                "url",
                url.concat("app/transport-management/details/").concat(ebDemande.getEbDemandeNum().toString()));
        params.setVariable("ebdemande", ebDemande);

        Optional<EbCompagnie> cp = ebCompagnieRepository.findById(user.getEbCompagnie().getEbCompagnieNum());

        String nomCompUser = cp.get().getNom();

        params.setVariable("nomUser", user.getNom());
        params.setVariable("prenomUser", user.getPrenom());
        params.setVariable("nomCompUser", nomCompUser);
        params.setVariable("nomEtabUser", user.getEbEtablissement().getNom());

        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                user.getEbCompagnie().getEbCompagnieNum(),
                Enumeration.EmailConfig.QUOTATION_RENSEIGNEMENT_INFORMATIONS_TRANSPORT.getCode(),
                Enumeration.Module.TRANSPORT_MANAGEMENT.getCode(),
                ebDemande.getEbDemandeNum());

        email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_RENSEIGNEMENT_INFORMATIONS_TRANSPORT);

        notificationService
            .generateAlert(
                email,
                (List<Integer>) res.get("ids"),
                ebDemande.getEbDemandeNum(),
                Enumeration.Module.TRANSPORT_MANAGEMENT.getCode());
        emailHtmlSender
            .sendListEmail(
                (List<String>) res.get("emails"),
                email,
                "quotation/24-email-renseignement-informations-transport",
                params,
                subject);
    }

    @Override
    public void sendEmailQuotationRequestViaSendQuotation(
        EbDemande ebDemande,
        ExEbDemandeTransporteur exEbDemandeTransporteur) {
        Context params = new Context();
        Email email = new Email();

        String refTran = ebDemande.getRefTransport();
        String nomUser = ebDemande.getUser().getNom();
        String prenomUser = ebDemande.getUser().getPrenom();
        String typeRequest = ebDemande.getTypeRequestLibelle();

        String[] args = {
            refTran, nomUser, prenomUser, typeRequest
        };

        String subject = "demandealertetransporteur.subject";
        email.setArguments(args);

        params.setVariable("url", url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString()));
        params.setVariable("ebdemande", ebDemande);

        /*
         * EbIncoterm inco =
         * serviceListStatique.getIncotermById(ebDemande.getxEcIncoterm());
         * String incoterm = inco.getLibelle();
         */
        params.setVariable("inco", ebDemande.getxEcIncotermLibelle());

        Optional<EbCompagnie> cp = ebCompagnieRepository
            .findById(ebDemande.getUser().getEbCompagnie().getEbCompagnieNum());

        String cpName = cp.get().getNom();

        params.setVariable("cpName", cpName);

        if (ebDemande.getListTransporteurs() != null && !ebDemande.getListTransporteurs().isEmpty()) {
            List<Map<String, Object>> listMapEmail = new ArrayList<Map<String, Object>>();

            for (EbUser transporteur: ebDemande.getListTransporteurs()) {
                email = new Email();
                email.setTo(transporteur.getEmail());
                email.setSubject(subject);
                email.setEmailConfig(Enumeration.EmailConfig.EMAIL_INVITATION);

                Context newParams = new Context();
                for (String varName: params
                    .getVariableNames()) newParams.setVariable(varName, params.getVariable(varName));
                newParams.setVariable("transporteur", transporteur);

                Map<String, Object> mapEmail = new HashMap<String, Object>();
                mapEmail.put("context", params);
                mapEmail.put("templateName", "8-email-invitation");
                mapEmail.put("email", email);
                mapEmail.put("subject", subject);

                listMapEmail.add(mapEmail);
            }

            emailHtmlSender.sendListEmail(listMapEmail);
        }

        if (exEbDemandeTransporteur.getxTransporteur().getEbUserNum() != null) {
            EbUser transporteur = ebUserRepository.getOne(exEbDemandeTransporteur.getxTransporteur().getEbUserNum());
            email = new Email();
            email.setTo(transporteur.getEmail());
            email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_REQUEST);
            email.setSubject(subject);
            params.setVariable("transporteur", transporteur);
            emailHtmlSender.send(email, "quotation/12-email-quotation-request", params);
        }

        if (ebDemande.getListEmailSendQuotation() != null && !ebDemande.getListEmailSendQuotation().isEmpty()) {
            email = new Email();
            String[] emails = ebDemande.getListEmailSendQuotation().split(";");
            List<String> emailList = new ArrayList(Arrays.asList(emails));
            email.setTo(emailList.get(0).toString());
            emailList.remove(0);
            email.setEnCopieCachee(emailList);
            email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_REQUEST);
            email.setSubject(subject);
            params.setVariable("transporteur", null);
            emailHtmlSender.send(email, "quotation/12-email-quotation-request", params);
        }

    }

    @Override
    public void sendEmailCreationTransportFile(EbDemande ebDemande) {
        Context params = new Context();
        Email email = new Email();

        String refTran = ebDemande.getRefTransport();
        String customerRef = ebDemande.getCustomerReference()!= null ? ebDemande.getCustomerReference() : "";
        EbUser userDemande = ebDemande.getUser();
        String firstNameSender = userDemande.getNom();
        String lastNameSender = userDemande.getPrenom();

        ExEbDemandeTransporteur demandeTransporteurFinal = ebDemande.get_exEbDemandeTransporteurFinal();

        EbEtablissement etablissementSender = ebEtablissementRepository
            .getOne(userDemande.getEbEtablissement().getEbEtablissementNum());
        String etablissementSenderName = etablissementSender.getNom();

        EbUser userTransporteur = ebDemande.getExEbDemandeTransporteurFinal();
        Optional<EbEtablissement> etablissementTransporteur = ebEtablissementRepository
            .findById(userTransporteur.getEbEtablissement().getEbEtablissementNum());

        // EbIncoterm inco =
        // serviceListStatique.getIncotermById(ebDemande.getxEcIncoterm());
        String incoterm = ebDemande.getxEcIncotermLibelle();

        String modeTransportLibelle = Enumeration.ModeTransport.getLibelleByCode(ebDemande.getxEcModeTransport());

        EbParty partyOrigin = ebDemande.getEbPartyOrigin();
        EbParty partyDest = ebDemande.getEbPartyDest();

        String[] args = {
            refTran, // Ref transport
            customerRef, // Customer ref
            firstNameSender, // First name sender
            lastNameSender, // Last name sender
            etablissementSenderName // Etablissement
        };

        String subject = "transportfile.creation.email.title";
        email.setArguments(args);
        email.setSubject(subject);
        params.setVariable("ebDemande", ebDemande);
        params.setVariable("userDemande", userDemande);
        params.setVariable("etablissementSender", etablissementSender);
        params.setVariable("userTransporteur", userTransporteur);
        params.setVariable("etablissementTransporteur", etablissementTransporteur.get());
        params.setVariable("demandeTransporteurFinal", demandeTransporteurFinal);
        params.setVariable("partyOrigin", partyOrigin);
        params.setVariable("partyOriginCountry", partyOrigin.getxEcCountry().getLibelle());

        params.setVariable("partyDest", partyDest);
        params.setVariable("refTransport", refTran);

        params
            .setVariable(
                "url",
                url.concat("app/transport-management/details/").concat(ebDemande.getEbDemandeNum().toString()));
        params.setVariable("incoterm", incoterm);
        params.setVariable("modeTransportLibelle", modeTransportLibelle);

        // Destinataire
        Map<String, Object> destinataireParams = new HashMap<>();
        destinataireParams.put("transporteur", userTransporteur);

        Integer compagnieNum = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();
        if (connectedUser != null) compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                compagnieNum,
                Enumeration.EmailConfig.TRANSPORT_FILE_CREATION.getCode(),
                Enumeration.Module.TRANSPORT_MANAGEMENT.getCode(),
                ebDemande.getEbDemandeNum(),
                destinataireParams);

        email.setEmailConfig(Enumeration.EmailConfig.TRANSPORT_FILE_CREATION);

        notificationService
            .generateAlert(
                email,
                (List<Integer>) res.get("ids"),
                ebDemande.getEbDemandeNum(),
                Enumeration.Module.TRANSPORT_MANAGEMENT.getCode());
        emailHtmlSender
            .sendListEmail(
                (List<String>) res.get("emails"),
                email,
                "quotation/37-email-transport-file-creation",
                params,
                subject);
    }

    @Override
    public void sendEmailSharingInformation(EbInvoice invoice, List<Integer> actions) {
        Context params = new Context();
        Email email = new Email();

        String invoiceNum = null;
        BigDecimal invoiceCost = null;
        Date invoiceDate = null;
        String invoiceCurrency = null;
        String invoiceStatus = null;
        String customerRef = "";

        EbUser connectedUser = connectedUserService.getCurrentUser();

        Integer invoiceId = invoice.getEbInvoiceNum();

        invoiceNum = invoice.getApplyChargeurIvoiceNumber();
        invoiceCost = invoice.getApplyChargeurIvoicePrice();
        invoiceCurrency = invoice.getCurrencyLabel();

        String refTran = invoice.getRefDemande();

        if (invoice.getxEbDemande() != null && invoice.getxEbDemande().getCustomerReference() != null) {
            customerRef = invoice.getxEbDemande().getCustomerReference();
        }

        String firstNameSender = connectedUser.getNom();
        String lastNameSender = connectedUser.getPrenom();

        EbEtablissement etablissementSender = ebEtablissementRepository
            .getOne(connectedUser.getEbEtablissement().getEbEtablissementNum());
        String etablissementSenderName = etablissementSender.getNom();

        String companyChargeurName = connectedUser.getEbCompagnie().getNom();

        Integer transportNum = ebInvoiceRepository.findxEbDemandeNumByEbInvoiceNum(invoiceId);

        String invoiceMonthDate = null;

        if (connectedUser.getRole() == Enumeration.Role.ROLE_CHARGEUR.getCode()) {

            if (invoice.getStatutCarrier() == null && invoice.getStatutChargeur() == null) {
                invoiceStatus = FAEnumeration.InvoiceStatus.getLibelleByCode(invoice.getStatut());
            }
            else {
                invoiceStatus = FAEnumeration.InvoiceStatus.getLibelleByCode(invoice.getStatutChargeur());
            }

        }
        else if (connectedUser.getRole() == Enumeration.Role.ROLE_PRESTATAIRE.getCode()) {

            if (invoice.getStatutCarrier() == null && invoice.getStatutChargeur() == null) {
                invoiceStatus = FAEnumeration.InvoiceStatus.getLibelleByCode(invoice.getStatut());
            }
            else {
                invoiceStatus = FAEnumeration.InvoiceStatus.getLibelleByCode(invoice.getStatutCarrier());
            }

        }
        else {
            invoiceStatus = FAEnumeration.InvoiceStatus.getLibelleByCode(invoice.getStatut());
        }

        if (connectedUser.getRole() == Enumeration.Role.ROLE_CHARGEUR.getCode()) {

            if (invoice.getInvoiceDateChargeur() == null && invoice.getInvoiceDateChargeur() == null) {
                invoiceDate = invoice.getDateModification();
            }
            else {
                invoiceDate = invoice.getInvoiceDateChargeur();
            }

        }
        else if (connectedUser.getRole() == Enumeration.Role.ROLE_PRESTATAIRE.getCode()) {

            if (invoice.getInvoiceDateChargeur() == null && invoice.getInvoiceDateCarrier() == null) {
                invoiceDate = invoice.getDateModification();
            }
            else {
                invoiceDate = invoice.getInvoiceDateCarrier();
            }

        }

        if (invoiceDate != null) {
            SimpleDateFormat simpleDateFormatMonth = new SimpleDateFormat("MMMM");

            SimpleDateFormat simpleDateFormatYear = new SimpleDateFormat("YYYY");

            invoiceMonthDate = simpleDateFormatMonth.format(invoiceDate) + " "
                + simpleDateFormatYear.format(invoiceDate);
        }

        String[] args = {
            refTran, // Ref transport
            customerRef
        };

        String subject = "freight.audit.mail.sharing.information.subject";
        email.setSubject(subject);
        email.setArguments(args);

        params.setVariable("invoiceCost", invoiceCost);
        params.setVariable("invoiceCurrency", invoiceCurrency);
        params.setVariable("invoiceNum", invoiceNum);
        params.setVariable("invoiceMonthDate", invoiceMonthDate);
        params.setVariable("invoiceStatus", invoiceStatus);
        params.setVariable("refTran", refTran);
        params.setVariable("customerRef", customerRef);
        params.setVariable("firstNameSender", firstNameSender);
        params.setVariable("lastNameSender", lastNameSender);
        params.setVariable("etablissementSenderName", etablissementSenderName);
        params.setVariable("companyChargeurName", companyChargeurName);
        params.setVariable("actions", actions);
        params.setVariable("libelleDestCountry", invoice.getLibelleDestCountry());

        params.setVariable("url", url.concat("app/transport-management/details/").concat(transportNum.toString()));

        // Destinataire
        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                connectedUser.getEbCompagnie().getEbCompagnieNum(),
                Enumeration.EmailConfig.INFORMATION_SHARING.getCode(),
                Enumeration.Module.FREIGHT_AUDIT.getCode(),
                transportNum);

        email.setEmailConfig(Enumeration.EmailConfig.INFORMATION_SHARING);

        notificationService
            .generateAlert(
                email,
                (List<Integer>) res.get("ids"),
                transportNum,
                Enumeration.Module.FREIGHT_AUDIT.getCode());

        emailHtmlSender
            .sendListEmail((List<String>) res.get("emails"), email, "43-sharing-information", params, subject);
    }

    @Override
    public void sendEmailApplyingInformation(EbInvoice invoice, List<Integer> actions) {
        Context params = new Context();
        Email email = new Email();
        String customerRef = "";

        Integer invoiceId = invoice.getEbInvoiceNum();
        String invoiceNum = invoice.getApplyChargeurIvoiceNumber();
        BigDecimal invoiceCost = invoice.getApplyChargeurIvoicePrice();
        String invoiceCurrency = invoice.getCurrencyLabel();
        Date invoiceDate = invoice.getInvoiceDateChargeur();
        String invoiceStatus = FAEnumeration.InvoiceStatus.getLibelleByCode(invoice.getStatutChargeur());

        EbUser connectedUser = connectedUserService.getCurrentUser();

        String refTran = invoice.getRefDemande();

        if (invoice.getxEbDemande() != null && invoice.getxEbDemande().getCustomerReference() != null) {
            customerRef = invoice.getxEbDemande().getCustomerReference();
        }

        String firstNameSender = connectedUser.getNom();
        String lastNameSender = connectedUser.getPrenom();

        EbEtablissement etablissementSender = ebEtablissementRepository
            .getOne(connectedUser.getEbEtablissement().getEbEtablissementNum());
        String etablissementSenderName = etablissementSender.getNom();

        String companyChargeurName = connectedUser.getEbCompagnie().getNom();

        Integer transportNum = ebInvoiceRepository.findxEbDemandeNumByEbInvoiceNum(invoiceId);

        String invoiceMonthDate = null;

        if (invoiceDate != null) {
            SimpleDateFormat simpleDateFormatMonth = new SimpleDateFormat("MMMM");

            SimpleDateFormat simpleDateFormatYear = new SimpleDateFormat("YYYY");

            invoiceMonthDate = simpleDateFormatMonth.format(invoiceDate) + " "
                + simpleDateFormatYear.format(invoiceDate);
        }

        String[] args = {
            refTran, // Ref transport
            customerRef
        };

        String subject = "freight.audit.mail.applying.information.subject";
        email.setSubject(subject);
        email.setArguments(args);

        params.setVariable("invoiceCost", invoiceCost);
        params.setVariable("invoiceCurrency", invoiceCurrency);
        params.setVariable("invoiceNum", invoiceNum);
        params.setVariable("invoiceMonthDate", invoiceMonthDate);
        params.setVariable("invoiceStatus", invoiceStatus);
        params.setVariable("refTran", refTran);
        params.setVariable("customerRef", customerRef);
        params.setVariable("firstNameSender", firstNameSender);
        params.setVariable("lastNameSender", lastNameSender);
        params.setVariable("etablissementSenderName", etablissementSenderName);
        params.setVariable("companyChargeurName", companyChargeurName);
        params.setVariable("actions", actions);
        params.setVariable("libelleDestCountry", invoice.getLibelleDestCountry());

        params.setVariable("url", url.concat("app/transport-management/details/").concat(transportNum.toString()));

        // Destinataire
        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                connectedUser.getEbCompagnie().getEbCompagnieNum(),
                Enumeration.EmailConfig.INFORMATION_APPLY.getCode(),
                Enumeration.Module.FREIGHT_AUDIT.getCode(),
                transportNum);

        email.setEmailConfig(Enumeration.EmailConfig.INFORMATION_APPLY);

        notificationService
            .generateAlert(
                email,
                (List<Integer>) res.get("ids"),
                transportNum,
                Enumeration.Module.FREIGHT_AUDIT.getCode());

        emailHtmlSender
            .sendListEmail((List<String>) res.get("emails"), email, "42-applying-information", params, subject);
    }

    @Override
    public void sendEmailTemplateInformationNotCompleted(EbDemandeTPL ebDemandeTPL, String libelle) {
        Context params = new Context();

        Email email = new Email();
        Integer compagnieNum = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();
        if (connectedUser != null) compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        Integer module = Enumeration.Module.PRICING.getCode();
        Integer ebDemandeNum = ebDemandeRepository.getEbDemandeNumByRefTransport(ebDemandeTPL.getReference());

        // refrence
        String refTransport = ebDemandeTPL.getReference();
        params.setVariable("refTransport", refTransport);
        // refrence
        String customerReference = ebDemandeTPL.getCustomerReference();
        params.setVariable("customerReference", customerReference);
        // OrigincustomerReference
        String companyOrigin = ebDemandeTPL.getPartyOrigin().getCompany();
        params.setVariable("companyOrigin", companyOrigin);
        String adresseOrigin = ebDemandeTPL.getPartyOrigin().getAdresse();
        params.setVariable("adresseOrigin", adresseOrigin);
        String cityOrigin = ebDemandeTPL.getPartyOrigin().getCity();
        params.setVariable("cityOrigin", cityOrigin);
        String CountryOriginName = ebDemandeTPL.getPartyOrigin().getCountryName();
        params.setVariable("CountryOriginName", CountryOriginName);
        String zipCodeOrigin = ebDemandeTPL.getPartyOrigin().getZipCode();
        params.setVariable("zipCodeOrigin", zipCodeOrigin);
        // Destination
        String companyDestination = ebDemandeTPL.getPartyDestination().getCompany();
        params.setVariable("companyDestination", companyDestination);
        String adresseDestination = ebDemandeTPL.getPartyOrigin().getAdresse();
        params.setVariable("adresseDestination", adresseDestination);
        String cityDestination = ebDemandeTPL.getPartyOrigin().getCity();
        params.setVariable("cityDestination", cityDestination);
        String CountryDestinationName = ebDemandeTPL.getPartyOrigin().getCountryName();
        params.setVariable("CountryDestinationName", CountryDestinationName);
        String zipCodeDestination = ebDemandeTPL.getPartyOrigin().getZipCode();
        params.setVariable("zipCodeDestination", zipCodeDestination);
        // mode de transport
        String modeOfTransport = ebDemandeTPL.getCategorieLabelValue().get("Type de Transport");
        params.setVariable("modeOfTransport", modeOfTransport);
        // currencyCode
        String currencyCode = ebDemandeTPL.getCurrencyCode();
        params.setVariable("currencyCode", currencyCode);
        // demandeur
        String demandeur = ebDemandeTPL.getCustomFields().get("field10");
        params.setVariable("demandeur", demandeur);
        String telDemandeur = ebDemandeTPL.getCustomFields().get("field14");
        params.setVariable("telDemandeur", telDemandeur);
        // destinataire
        String destinataire = ebDemandeTPL.getCustomFields().get("field12");
        params.setVariable("destinataire", destinataire);
        String telDestinataire = ebDemandeTPL.getCustomFields().get("field15");
        params.setVariable("telDestinataire", telDestinataire);
        // motif
        String motif = ebDemandeTPL.getCategorieLabelValue().get("Motif");
        params.setVariable("motif", motif);
        // costCenter
        String CostCenter = ebDemandeTPL.getCostCenter();
        params.setVariable("CostCenter", CostCenter);
        // units
        List<EbMarchandiseTPL> units = ebDemandeTPL.getUnits();
        params.setVariable("units", units);

        if (ebDemandeNum != null) {
            params.setVariable("EbDemandeNum", ebDemandeNum);
        }

        String[] args = {
            refTransport, customerReference
        };
        String link = module.equals(Enumeration.Module.PRICING.getCode()) ? "pricing" : "transport-management";
        params.setVariable("url", url.concat("app/" + link + "/details/").concat(ebDemandeNum.toString()));

        email.setArguments(args);
        String support = "myrower2018+ct@gmail.com";
        List<String> enCopie = new ArrayList<>();
        enCopie.add(support);
        email.setEnCopie(enCopie);

        Integer moduleCode = module.equals(Enumeration.Module.PRICING.getCode()) ?
            Enumeration.Module.PRICING.getCode() :
            Enumeration.Module.TRANSPORT_MANAGEMENT.getCode();

        // prefacture
        if (Enumeration.TemplateConfig.PREFACTURE.getLibelle().equalsIgnoreCase(libelle)) {
            String subject = "InfoMandatoryPrefacture.subject";
            Map<String, Object> res = serviceListStatique
                .getEmailUsers(
                    compagnieNum,
                    Enumeration.EmailConfig.INFO_MANDATORY_PREFACTURE.getCode(),
                    module,
                    ebDemandeNum);
            email.setEmailConfig(Enumeration.EmailConfig.INFO_MANDATORY_PROFORMA);
            email.setSubject(subject);

            notificationService.generateAlert(email, (List<Integer>) res.get("ids"), ebDemandeNum, moduleCode);

            emailHtmlSender
                .sendListEmail(
                    (List<String>) res.get("emails"),
                    email,
                    "factureEssilor/" + Enumeration.EmailConfig.INFO_MANDATORY_PROFORMA.getCode() + "-"
                        + Enumeration.EmailConfig.INFO_MANDATORY_PROFORMA.getTemplate(),
                    params,
                    subject);
        }

        if (Enumeration.TemplateConfig.PROFORMA.getLibelle().equalsIgnoreCase(libelle)) {
            String subject = "InfoMandatoryProforma.subject";
            Map<String, Object> res = serviceListStatique
                .getEmailUsers(
                    compagnieNum,
                    Enumeration.EmailConfig.INFO_MANDATORY_PROFORMA.getCode(),
                    module,
                    ebDemandeNum);
            email.setEmailConfig(Enumeration.EmailConfig.INFO_MANDATORY_PREFACTURE);
            email.setSubject(subject);
            notificationService.generateAlert(email, (List<Integer>) res.get("ids"), ebDemandeNum, moduleCode);

            emailHtmlSender
                .sendListEmail(
                    (List<String>) res.get("emails"),
                    email,
                    "factureEssilor/" + Enumeration.EmailConfig.INFO_MANDATORY_PREFACTURE.getCode() + "-"
                        + Enumeration.EmailConfig.INFO_MANDATORY_PREFACTURE.getTemplate(),
                    params,
                    subject);
        }

    }

    @Override
    public boolean checkedAllInformationMandotory(EbDemandeTPL ebDemandeTPL) {
        boolean isMandatory = false;
        if (ebDemandeTPL.getCustomFields().get("field10") == null) return isMandatory = true;
        else if (ebDemandeTPL.getCustomFields().get("field14") == null) return isMandatory = true;
        else if (ebDemandeTPL.getCustomFields().get("field11") == null) return isMandatory = true;
        else if (ebDemandeTPL.getCustomFields().get("field12") == null) return isMandatory = true;
        else if (ebDemandeTPL.getCustomFields().get("field15") == null) return isMandatory = true;
        else if (ebDemandeTPL.getCostCenter() == null) return isMandatory = true;
        else if (ebDemandeTPL.getPartyOrigin().getCity() == null) return isMandatory = true;
        else if (ebDemandeTPL.getPartyDestination().getCity() == null) return isMandatory = true;
        else if (ebDemandeTPL.getPartyDestination().getCompany() == null) return isMandatory = true;
        else if (ebDemandeTPL.getPartyDestination().getZipCode() == null) return isMandatory = true;
        else if (ebDemandeTPL.getPartyDestination().getAdresse() == null) return isMandatory = true;
        else if (ebDemandeTPL.getPartyDestination().getCountryName() == null) return isMandatory = true;
        else if (ebDemandeTPL.getCategorieLabelValue().get("Motif") == null) return isMandatory = true;
        else if (ebDemandeTPL.getCurrencyCode() == null) return isMandatory = true;
        else if (ebDemandeTPL.getUnits() != null && !ebDemandeTPL.getUnits().isEmpty()) {

            for (EbMarchandiseTPL unit: ebDemandeTPL.getUnits()) {
                if (unit.getTypeOfUnit() == null || unit.getTypeOfUnit() == "") return isMandatory = true;
                else if (unit.getLength() == null || unit.getTypeOfUnit().equals(0.0)) return isMandatory = true;
                else if (unit.getWeight() == null || unit.getWeight().equals(0.0)) return isMandatory = true;
                else if (unit.getWidth() == null || unit.getWidth().equals(0.0)) return isMandatory = true;
                else if (unit.getHeigth() == null || unit.getHeigth().equals(0.0)) return isMandatory = true;
                else if (unit.getNomArticle() == null || unit.getNomArticle() == "") return isMandatory = true;
                else if (unit.getComment() == null || unit.getComment() == "") return isMandatory = true;
                else if (unit.getOriginCountryName() == null
                    || unit.getOriginCountryName() == "") return isMandatory = true;
                else if (unit.getNumberOfUnits() == null
                    || unit.getNumberOfUnits().equals(1)) return isMandatory = true;
                else if (unit.getUnitPrice() == null || unit.getUnitPrice().equals(0.0)) return isMandatory = true;
            }

        }
        return isMandatory;
    }

    @Override
    public void sendEmailPricingDataChanged(EbDemande ebDemande, String actions) {
        Context params = new Context();
        Email email = new Email();
        EbUser connectedUser = connectedUserService.getCurrentUser();

        String refTran = ebDemande.getRefTransport();
        String customerRef = ebDemande.getCustomerReference();
        EbUser userDemande = ebDemande.getUser();
        String firstNameSender = connectedUser.getNom();
        String lastNameSender = connectedUser.getPrenom();

        customerRef = customerRef != null ? customerRef : " - ";

        EbUser userTransporteur = ebDemande.getExEbDemandeTransporteurFinal();

        String[] args = {
            refTran, // Ref transport
            customerRef, // Customer ref
            firstNameSender, // First name sender
            lastNameSender // Last name sender
        };

        String subject = "pricing.datachanged.email.title";
        email.setArguments(args);

        String action = actions.replace("Shipping information updated: ", "");

        Date dt = new Date();
        String strDate = new SimpleDateFormat("dd/MM/yyyy à HH:mm:ss").format(dt);
        String strDateSolo = strDate.substring(0, 10);
        String strTimeSolo = strDate.substring(13);
        params.setVariable("ebDemande", ebDemande);
        params.setVariable("userDemande", userDemande);
        params.setVariable("refTransport", refTran);
        params.setVariable("actions", action);
        params.setVariable("date", strDateSolo);
        params.setVariable("time", strTimeSolo);

        params
            .setVariable(
                "url",
                url.concat("app/transport-management/details/").concat(ebDemande.getEbDemandeNum().toString()));

        Integer module = Enumeration.Module.PRICING.getCode();
        if (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.WPU
            .getCode()) module = Enumeration.Module.TRANSPORT_MANAGEMENT.getCode();
        // Destinataire
        Map<String, Object> destinataireParams = new HashMap<>();
        destinataireParams.put("transporteur", userDemande);

        Integer compagnieNum = null;
        if (connectedUser != null) compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                compagnieNum,
                Enumeration.EmailConfig.PRICING_DATA_CHANGED.getCode(),
                module,
                ebDemande.getEbDemandeNum(),
                destinataireParams);

        if (res != null && !res.isEmpty()) {
            email.setEmailConfig(Enumeration.EmailConfig.PRICING_DATA_CHANGED);
            email.setSubject(subject);
            email.setArguments(args);
            params.setVariable("refTran", refTran);
            params.setVariable("demandeId", ebDemande.getEbDemandeNum());
            params.setVariable("demandeId", ebDemande.getEbDemandeNum());
            notificationService
                .generateAlert(email, (List<Integer>) res.get("ids"), ebDemande.getEbDemandeNum(), module);
            emailHtmlSender
                .sendListEmail(
                    (List<String>) res.get("emails"),
                    email,
                    "quotation/" + Enumeration.EmailConfig.PRICING_DATA_CHANGED.getCode() + "-"
                        + Enumeration.EmailConfig.PRICING_DATA_CHANGED.getTemplate(),
                    params,
                    subject);
        }

    }

    @Override
    public void sendEmailQuotationaCknowledgeTdc(EbDemande ebDemande) {
        Context params = new Context();
        Email email = new Email();

        String refTran = ebDemande.getRefTransport();
        String urlVisualisation = url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString());

        String[] args = {
            refTran, urlVisualisation
        };

        String subject = "demandecknowledgetdc.subject";
        email.setArguments(args);

        Integer compagnieNum = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();
        if (connectedUser != null) compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        // Le mail est envoyé au chargeur et à ses collègues selon paramétrage
        // (si alert est activé)
        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                compagnieNum,
                Enumeration.EmailConfig.QUOTATION_CKNOWLEDGE_TDC.getCode(),
                Enumeration.Module.PRICING.getCode(),
                ebDemande.getEbDemandeNum());

        if (res != null && !res.isEmpty()) {
            email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_CKNOWLEDGE_TDC);
            email.setSubject(subject);
            email.setArguments(args);
            params.setVariable("refTran", refTran);
            params.setVariable("url", urlVisualisation);
            params.setVariable("demandeId", ebDemande.getEbDemandeNum());
            params.setVariable("ebdemande", ebDemande);
            notificationService
                .generateAlert(
                    email,
                    (List<Integer>) res.get("ids"),
                    ebDemande.getEbDemandeNum(),
                    Enumeration.Module.PRICING.getCode());

            emailHtmlSender
                .sendListEmail(
                    (List<String>) res.get("emails"),
                    email,
                    "quotation/60-email-quotation-cknowledge-tdc",
                    params,
                    subject);
        }

    }

    @Override
    public void sendEmailConfirmPendingTransport(EbDemande ebDemande) {
        Context params = new Context();
        Email email = new Email();
        Boolean psl = true;
        Boolean isDanger = ebDemande.getDg();
        String refTransport = ebDemande.getRefTransport();
        String customRef = ebDemande.getCustomerReference();
        String companyChargeur = ebDemande.getxEbCompagnie().getNom();

        if (customRef != null && !customRef.isEmpty()) {
            customRef += " /";
        }
        else {
            customRef = "";
        }

        String[] args = {
            refTransport, customRef, companyChargeur
        };

        String subject = "demandeconfirmation.subject";
        email.setArguments(args);
        email.setSubject(subject);
        Integer compagnieNum = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();
        if (connectedUser != null) compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        // Le mail est envoyé au chargeur et à ses collègues selon paramétrage
        // (si alert est activé)
        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                compagnieNum,
                Enumeration.EmailConfig.QUOTATION_CKNOWLEDGE_TDC.getCode(),
                Enumeration.Module.PRICING.getCode(),
                ebDemande.getEbDemandeNum());
        email.setEmailConfig(Enumeration.EmailConfig.DEMANDE_CONFIRMATION_DE_PRISE_EN_CHARGE_DU_TRANSPORT);
        notificationService
            .generateAlert(
                email,
                (List<Integer>) res.get("ids"),
                ebDemande.getEbDemandeNum(),
                Enumeration.Module.PRICING.getCode());

        if (res != null && !res.isEmpty()) {
            email.setEmailConfig(Enumeration.EmailConfig.QUOTATION_RENSEIGNEMENT_INFORMATIONS_TRANSPORT);
            email.setSubject(subject);
            email.setArguments(args);
            params.setVariable("ebdemande", ebDemande);
            params.setVariable("companychargeur", companyChargeur);
            params.setVariable("isDanger", isDanger);
            params.setVariable("psl", psl);
            params
                .setVariable(
                    "url",
                    url.concat("app/transport-management/details/").concat(ebDemande.getEbDemandeNum().toString()));
            params.setVariable("demandeId", ebDemande.getEbDemandeNum());
            emailHtmlSender
                .sendListEmail(
                    (List<String>) res.get("emails"),
                    email,
                    "quotation/24-email-renseignement-informations-transport",
                    params,
                    subject);
        }

    }

    @Override
    public void sendEmailDemandeConfirmationOfTransportSupport(EbDemande ebDemande) {
        Context params = new Context();
        Email email = new Email();
        Boolean psl = true;

        Boolean isDanger = ebDemande.getDg();
        String refTransport = ebDemande.getRefTransport();
        String customRef = ebDemande.getCustomerReference();
        String companyChargeur = ebDemande.getxEbCompagnie().getNom();

        if (customRef != null && !customRef.isEmpty()) {
            customRef += " /";
        }
        else {
            customRef = "";
        }

        String[] args = {
            refTransport, customRef, companyChargeur
        };

        String subject = "demandeconfirmation.subject";
        email.setArguments(args);
        email.setSubject(subject);
        Integer compagnieNum = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();
        if (connectedUser != null) compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        // Le mail est envoyé au chargeur et à ses collègues selon paramétrage
        // (si alert est activé)
        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                compagnieNum,
                Enumeration.EmailConfig.QUOTATION_CKNOWLEDGE_TDC.getCode(),
                Enumeration.Module.PRICING.getCode(),
                ebDemande.getEbDemandeNum());

        notificationService
            .generateAlert(
                email,
                (List<Integer>) res.get("ids"),
                ebDemande.getEbDemandeNum(),
                Enumeration.Module.PRICING.getCode());

        if (res != null && !res.isEmpty()) {
            email.setEmailConfig(Enumeration.EmailConfig.DEMANDE_CONFIRMATION_DE_PRISE_EN_CHARGE_DU_TRANSPORT);
            email.setSubject(subject);
            email.setArguments(args);
            params.setVariable("ebdemande", ebDemande);
            params.setVariable("companychargeur", companyChargeur);
            params.setVariable("isDanger", isDanger);
            params.setVariable("psl", psl);
            params
                .setVariable(
                    "url",
                    url.concat("app/transport-management/details/").concat(ebDemande.getEbDemandeNum().toString()));
            params.setVariable("demandeId", ebDemande.getEbDemandeNum());
            emailHtmlSender
                .sendListEmail(
                    (List<String>) res.get("emails"),
                    email,
                    "quotation/62-email-demande-confirmation-de-prise-en-charge-du-transport",
                    params,
                    subject);
        }

    }

    public void sendEmailPricingConfirmationPriseEnCharge(EbDemande ebDemande) {
        Context params = new Context();
        Email email = new Email();
        Boolean psl = true;
        Boolean confirmOfSupport = true;
        Boolean isDanger = ebDemande.getDg();
        String refTransport = ebDemande.getRefTransport();
        String customRef = ebDemande.getCustomerReference();
        String companyChargeur = ebDemande.getxEbCompagnie().getNom();
        String companyCarrier = ebDemande.getSelectedCarrier().getxEbCompagnie().getNom();

        if (customRef != null && !customRef.isEmpty()) {
            customRef += " /";
        }
        else {
            customRef = "";
        }

        String[] args = {
            refTransport, customRef, companyCarrier
        };

        String subject = "pricing.mail.confirmation.subject";
        email.setArguments(args);
        email.setSubject(subject);
        Integer compagnieNum = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();
        if (connectedUser != null) compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();
        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                compagnieNum,
                Enumeration.EmailConfig.PRICING_CONFIRMATION_PRISE_EN_CHARGE.getCode(),
                Enumeration.Module.PRICING.getCode(),
                ebDemande.getEbDemandeNum());

        notificationService
            .generateAlert(
                email,
                (List<Integer>) res.get("ids"),
                ebDemande.getEbDemandeNum(),
                Enumeration.Module.PRICING.getCode());

        if (res != null && !res.isEmpty()) {
            email.setEmailConfig(Enumeration.EmailConfig.PRICING_CONFIRMATION_PRISE_EN_CHARGE);
            email.setSubject(subject);
            email.setArguments(args);
            params.setVariable("ebdemande", ebDemande);
            params.setVariable("companychargeur", companyChargeur);
            params.setVariable("companyCarrier", companyCarrier);
            params.setVariable("isDanger", isDanger);
            params.setVariable("psl", psl);
            params.setVariable("confirmOfSupport", confirmOfSupport);
            params
                .setVariable(
                    "url",
                    url.concat("app/transport-management/details/").concat(ebDemande.getEbDemandeNum().toString()));
            emailHtmlSender
                .sendListEmail(
                    (List<String>) res.get("emails"),
                    email,
                    "quotation/63-email-pricing-confirmation-prise-en-charge",
                    params,
                    subject);
        }

    }

    @Override
    public void sendEmailSharingOneInformation(EbInvoice invoice, BigDecimal action) {
        Context params = new Context();
        Email email = new Email();

        EbUser connectedUser = connectedUserService.getCurrentUser();
        SharingInformationMailParams sharingInformation = new SharingInformationMailParams(invoice, action);
        Integer transportNum = ebInvoiceRepository.findxEbDemandeNumByEbInvoiceNum(sharingInformation.invoiceId);

        if (sharingInformation != null) {
            String[] args = {
                sharingInformation.refTranport, sharingInformation.customerRef
            };

            String subject = "freight.audit.mail.sharing.information.subject";
            email.setSubject(subject);
            email.setArguments(args);

            params.setVariable("invoiceCost", sharingInformation.cost);
            params.setVariable("invoiceCurrency", sharingInformation.currency);
            params.setVariable("invoiceNum", sharingInformation.invoiceNum);
            params.setVariable("invoiceMonthDate", sharingInformation.invoiceMonthDate);
            params.setVariable("invoiceStatus", sharingInformation.status);
            params.setVariable("refTran", sharingInformation.refTranport);
            params.setVariable("customerRef", sharingInformation.customerRef);
            params.setVariable("firstNameSender", sharingInformation.firstNameSender);
            params.setVariable("lastNameSender", sharingInformation.lastNameSender);
            params.setVariable("etablissementSenderName", sharingInformation.etablissementSenderName);
            params.setVariable("companyChargeurName", sharingInformation.companyChargeurName);
            params.setVariable("actions", sharingInformation.action);
            params.setVariable("libelleDestCountry", invoice.getLibelleDestCountry());
            params.setVariable("url", url.concat("app/transport-management/details/").concat(transportNum.toString()));

            // Destinataire
            Map<String, Object> res = serviceListStatique
                .getEmailUsers(
                    connectedUser.getEbCompagnie().getEbCompagnieNum(),
                    Enumeration.EmailConfig.INFORMATION_SHARING.getCode(),
                    Enumeration.Module.FREIGHT_AUDIT.getCode(),
                    transportNum);

            email.setEmailConfig(Enumeration.EmailConfig.INFORMATION_SHARING);

            notificationService
                .generateAlert(
                    email,
                    (List<Integer>) res.get("ids"),
                    transportNum,
                    Enumeration.Module.FREIGHT_AUDIT.getCode());

            emailHtmlSender
                .sendListEmail((List<String>) res.get("emails"), email, "43-sharing-information", params, subject);
        }

    }

    @Override
    public void sendEmailPricingFileUpdated(EbDemande ebDemande, EbUser connectedUser) {
        ebDemande = ebDemandeRepository.getOne(ebDemande.getEbDemandeNum());
        Context params = new Context();

        Email email = new Email();

        String refTran = ebDemande.getRefTransport();
        String customerRef = ebDemande.getCustomerReference();
        EbUser userDemande = ebDemande.getUser();
        String firstNameSender = connectedUser.getNom();
        String lastNameSender = connectedUser.getPrenom();
        String incoterm = ebDemande.getxEcIncotermLibelle()!=null ? ebDemande.getxEcIncotermLibelle(): "";

        customerRef = customerRef != null ? customerRef : " - ";

        String[] args = {
            refTran, // Ref transport
            customerRef, // Customer ref
            firstNameSender, // First name sender
            lastNameSender // Last name sender
        };

        String subject = "pricing.datachanged.email.title";
        email.setArguments(args);
        email.setSubject(subject);

        params.setVariable("ebdemande", ebDemande);
        Date dt = new Date();
        String strDate = new SimpleDateFormat("dd/MM/yyyy à HH:mm:ss").format(dt);
        String strDateSolo = strDate.substring(0, 10);
        String strTimeSolo = strDate.substring(13);
        params.setVariable("userDemande", userDemande);
        params.setVariable("refTransport", refTran);
        params.setVariable("date", strDateSolo);
        params.setVariable("time", strTimeSolo);
        params.setVariable("inco", incoterm);

        params
            .setVariable(
                "url",
                url.concat("app/transport-management/details/").concat(ebDemande.getEbDemandeNum().toString()));

        Integer module = Enumeration.Module.PRICING.getCode();
        if (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.WPU
            .getCode()) module = Enumeration.Module.TRANSPORT_MANAGEMENT.getCode();
        Integer compagnieNum = null;
        if (connectedUser != null) compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                compagnieNum,
                Enumeration.EmailConfig.QUOTATION_CANCELLED.getCode(),
                module,
                ebDemande.getEbDemandeNum());

        email.setEmailConfig(Enumeration.EmailConfig.PRICING_DATA_CHANGED);
        email.setSubject(subject);
        email.setArguments(args);
        params.setVariable("refTran", refTran);
        params.setVariable("demandeId", ebDemande.getEbDemandeNum());
        notificationService.generateAlert(email, (List<Integer>) res.get("ids"), ebDemande.getEbDemandeNum(), module);
        emailHtmlSender
            .sendListEmail(
                (List<String>) res.get("emails"),
                email,
                "quotation/" + Enumeration.EmailConfig.PRICING_DATA_CHANGED.getCode() + "-"
                    + Enumeration.EmailConfig.PRICING_DATA_CHANGED.getTemplate(),
                params,
                subject);
    }
}
