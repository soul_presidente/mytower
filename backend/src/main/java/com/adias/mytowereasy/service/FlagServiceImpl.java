package com.adias.mytowereasy.service;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.dto.mapper.EbFlagMapper;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbFlag;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.Module;
import com.adias.mytowereasy.utils.search.PageableSearchCriteria;


@Service
public class FlagServiceImpl extends MyTowerService implements FlagService {
    @Autowired
    private EbFlagMapper flagMapper;

    @Override
    public EbFlag addFlag(EbFlag ebFlag) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        EbCompagnie compagnie = connectedUser.getEbCompagnie();
        ebFlag.setCompagnie(compagnie);
        return ebFlagRepository.save(ebFlag);
    }

    @Override
    public List<EbFlag> getListFlagByCompagnieNum(PageableSearchCriteria flagSearchCriteria) {
        return daoFlag.getFlagsByCriteria(flagSearchCriteria);
    }

    @Override
    public List<EbFlag> getListFlagByCompagnieNum() {
        PageableSearchCriteria flagSearchCriteria = new PageableSearchCriteria();
        return getListFlagByCompagnieNum(flagSearchCriteria);
    }

    @Override
    public List<EbFlag> getListFlag(Integer moduleNum) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        List<EbFlag> result = getListFlagByCompagnieNum();

        if (CollectionUtils.isEmpty(result)) {
            result = new ArrayList<EbFlag>();
        }

        List<String> listFlagCodes = new ArrayList<>();

        Module module = (Module) Module.getByCode(moduleNum);

        if (connectedUser.isPrestataire()) {
            listFlagCodes = module.getListFlagForCarrier(connectedUser.getEbUserNum());
        }
        else if (connectedUser.isDefaultControlTower()) {
            listFlagCodes = module.getListFlagForControlTower(connectedUser.getEbUserNum());
        }

        result.addAll(getListFlagFromCodes(listFlagCodes));

        return result.stream().distinct().collect(Collectors.toList());
    }

    private List<EbFlag> getListFlagFromCodes(List<String> listFlagCodes) {
        return listFlagCodes
            .stream().filter(it -> !StringUtils.isEmpty(it)).map(flagCodes -> this.getListFlagDTOFromCodes(flagCodes))
            .map(listFlagDto -> flagMapper.ebFlagDtoToEbFlags(listFlagDto)).flatMap(Collection::stream)
            .collect(Collectors.toList());
    }

    @Override
    public Long countListFlag(PageableSearchCriteria flagSearchCriteria) {
        return daoFlag.countListFlag(flagSearchCriteria);
    }

    @Override
    public void deleteFlag(Integer ebFlagNum) {
        ebFlagRepository.deleteById(ebFlagNum);
    }

    @Override
    public List<EbFlagDTO> getListFlagDTOFromCodes(String listFlagCodes) {

        if (listFlagCodes != null) {
            List<EbFlagDTO> listEbFlag = new ArrayList<>();
            List<String> listFlagId = Arrays.asList(listFlagCodes.split(","));

            for (int i = 0; i < listFlagId.size(); i++) {

                try {
                    Optional<EbFlag> flag = ebFlagRepository.findById(Integer.parseInt(listFlagId.get(i)));

                    if (flag.isPresent()) {
                        listEbFlag.add(new EbFlagDTO(flag.get()));
                    }

                } catch (NumberFormatException e) {
                    System.err.println("Number format exception on flag id : " + listFlagId.get(i));
                }

            }

            return listEbFlag;
        }

        return null;
    }
}
