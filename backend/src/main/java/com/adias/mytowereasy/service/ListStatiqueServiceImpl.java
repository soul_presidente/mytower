/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import com.adias.mytowereasy.repository.*;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.drools.core.util.IoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.querydsl.core.BooleanBuilder;

import com.adias.mytowereasy.cptm.dao.DaoCptmProcedure;
import com.adias.mytowereasy.dao.DaoMarchandise;
import com.adias.mytowereasy.dao.DaoPricing;
import com.adias.mytowereasy.dao.DaoTrack;
import com.adias.mytowereasy.dao.DaoUser;
import com.adias.mytowereasy.dao.impl.DaoEmailImpl;
import com.adias.mytowereasy.delivery.dao.DaoOrder;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbOrder;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.repository.EbOrderRepository;
import com.adias.mytowereasy.delivery.service.DeliveryService;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.dto.CodeLibelleDTO;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.CREnumeration.RuleCategory;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.Module;
import com.adias.mytowereasy.model.Enumeration.ServiceType;
import com.adias.mytowereasy.model.Enumeration.TypeContainer;
import com.adias.mytowereasy.model.custom.EbChatCustom;
import com.adias.mytowereasy.model.custom.EbCustomDeclaration;
import com.adias.mytowereasy.model.tt.EbTtCategorieDeviation;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.repository.*;
import com.adias.mytowereasy.repository.custom.EbChatCustomRepository;
import com.adias.mytowereasy.rsch.model.SearchCriteriaRSCH;
import com.adias.mytowereasy.service.chat.ChatService;
import com.adias.mytowereasy.service.email.EmailDocumentService;
import com.adias.mytowereasy.service.email.EmailService;
import com.adias.mytowereasy.service.email.EmailServicePricingBooking;
import com.adias.mytowereasy.service.storage.StorageService;
import com.adias.mytowereasy.util.GenericEnum;
import com.adias.mytowereasy.util.document.DocumentUtils;
import com.adias.mytowereasy.util.enums.GenericEnumException;
import com.adias.mytowereasy.util.enums.GenericEnumUtils;
import com.adias.mytowereasy.utils.search.*;

import fr.mytower.lib.storage.api.StorageProvider;


@Service
public class ListStatiqueServiceImpl extends MyTowerService implements ListStatiqueService {
    private static final String DOCX = "docx";

    private static final String PDF = "pdf";

    private static final Logger LOGGER = LoggerFactory.getLogger(ListStatiqueServiceImpl.class);

    private static final List<Integer> chargerMailIds = Arrays.asList(13,14,20,22,24,25,26,30,33,37,38,39,42,64,65,43,60,63,61,62);

    @Autowired
    private EcCountryRepository ecCountryRepository;

    @Autowired
    private EbTypeTransportRepository ebTypeTransportRepository;

    @Autowired
    private EcCityRepository ecCityRepository;

    @Autowired
    private EcRegionRepository ecRegionRepository;

    @Autowired
    private DaoPricing daoPricing;

    @Autowired
    private DaoUser daoUser;

    @Autowired
    private DaoTrack daoTrack;

    @Autowired
    private DaoOrder daoOrder;

    @Autowired
    private DaoMarchandise daoMarchandise;

    @Autowired
    private DeliveryService deliveryService;

    @Autowired
    private ConnectedUserService connectedUserService;

    @Autowired
    private EbChatRepository chatRepository;

    @Autowired
    EmailService emailService;

    @Autowired
    EmailServicePricingBooking emailServicePricingBooking;

    @Autowired
    EmailDocumentService emailDocumentService;

    @Autowired
    EcModuleRepository ecModuleRepository;

    private List<EcCountry> listCountry;

    @Autowired
    private EbChatCustomRepository ebChatCustomRepository;

    @Autowired
    private DaoCptmProcedure daoCptmProcedure;

    private List<EcFuseauxHoraire> listEcFuseauxHoraire;

    @Autowired
    @Lazy
    ControlRuleService controlRuleService;

    private List<EcMimetype> listMimeType;

    private List<EbOrder> listOwnerOfRequest;

    private List<EbParty> listPartyOrigin;

    @Autowired
    ServiceConfiguration serviceConfiguration;
    @Autowired
    StorageService storageService;

    @Autowired
    EbOrderRepository ebOrderRepository;

    @Autowired
    EbLivraisonRepository ebLivraisonRepository;

    @Autowired
    StorageProvider storageProvider;

    @Autowired
    DemandeObserversRepository demandeObserversRepository;
    @Autowired
    ChatService chatService;

    @Override
    public List<EcCity> getListEcCityByCountry(Integer ecCountryNum) {
        return ecCityRepository.findByEcCountryEcCountryNum(ecCountryNum);
    }

    @Override
    public EcCountry getEcCountryByCode(String code) {

        for (EcCountry pays: getListEcCountry()) {

            if (pays.getCode().equals(code)) {
                return pays;
            }

        }

        return null;
    }

    @Override
    public EcCountry getEcCountryByLibelle(String libelle) {

        for (EcCountry pays: getListEcCountry()) {

            if (pays.getLibelle().equals(libelle)) {
                return pays;
            }

        }

        return null;
    }

    @Override
    public List<EcRegion> getListEcRegion() {
        return ecRegionRepository.findAllByOrderByLibelleAsc();
    }

    @Override
    public List<EcCountry> getListEcCountry() {

        if (this.listCountry == null || this.listCountry.isEmpty()) {
            this.listCountry = ecCountryRepository.findAllByOrderByLibelleAsc();
        }

        return this.listCountry;
    }

    // @Override
    // public List<EcRole> getListRoleNotAdmin() {
    //
    // return
    // ecRoleRepository.findByLibelleNot(Enumeration.Role.ROLE_ADMIN.getLibelle());
    // }

    @Override
    public TypeContainer[] getListTypeConteneur() {
        return Enumeration.TypeContainer.values();
    }

    @Override
    public ServiceType[] getListUserService() {
        return Enumeration.ServiceType.values();
    }

    @Override
    public List<EcCity> getListEcCity() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getListTransportRef(String term, Integer module) throws JsonProcessingException {
        List<EbDemande> listEbDmeande = daoPricing.getListTransportRef(term, module);

        return serializationJson(listEbDmeande);
    }

    @Override
    public String getListCustomerReference(String term, Integer module) throws JsonProcessingException {
        List<EbDemande> listEbDemande = daoPricing.getListCustomerReference(term, module);

        return serializationJson(listEbDemande);
    }

    @Override
    public String getListtRefCustomerAndTraspo(String term) throws JsonProcessingException {
        List<EbTtTracing> listResult = new ArrayList<EbTtTracing>();
        List<EbTtTracing> listEbTrackCustomerRef = daoTrack.getListCustomerReference(term);
        List<EbTtTracing> listEbTrackRefTransport = daoTrack.getListRefTransport(term);

        if (!listEbTrackRefTransport.isEmpty()) {

            for (EbTtTracing ebtracing: listEbTrackRefTransport) {
                EbTtTracing tracing = new EbTtTracing();
                List<EbTtTracing> filteredListTr = null;

                if (ebtracing.getRefTransport() != null) {
                    filteredListTr = listResult
                        .stream()
                        .filter(it -> it.getCustomerRefTransportRef().contentEquals(ebtracing.getRefTransport()))
                        .collect(Collectors.toList());

                    if (filteredListTr.isEmpty()) {
                        tracing.setEbTtTracingNum(ebtracing.getEbTtTracingNum());
                        tracing.setCustomerRefTransportRef(ebtracing.getRefTransport());
                        listResult.add(tracing);
                    }

                }

            }

        }

        if (!listEbTrackCustomerRef.isEmpty()) {

            for (EbTtTracing ebtracing: listEbTrackCustomerRef) {
                EbTtTracing tracing = new EbTtTracing();
                List<EbTtTracing> filteredListTr = null;

                if (ebtracing.getCustomerReference() != null) {
                    filteredListTr = listResult
                        .stream()
                        .filter(it -> it.getCustomerRefTransportRef().contentEquals(ebtracing.getCustomerReference()))
                        .collect(Collectors.toList());

                    if (filteredListTr.isEmpty()) {
                        tracing = new EbTtTracing();
                        tracing.setEbTtTracingNum(ebtracing.getEbTtTracingNum());
                        tracing.setCustomerRefTransportRef(ebtracing.getCustomerReference());
                        listResult.add(tracing);
                    }

                }

            }

        }

        return serializationJson(listResult);
    }

    @Override
    public String getListCarrier(String term) throws JsonProcessingException {
        SearchCriteria criterias = new SearchCriteria();
        // TODO Auto-generated method stub

        criterias.setSearchterm(term);
        criterias.setUserService(Enumeration.ServiceType.SERVICE_TRANSPORTEUR.getCode());
        criterias.setEcRoleNum(Enumeration.Role.ROLE_PRESTATAIRE.getCode());
        // List<EbUser> listEbTransporteur =
        // daoUser.selectListEbUser(criterias);
        List<EbUser> listEbTransporteur = daoPricing.getListCarrier(term);

        return serializationJson(listEbTransporteur);
    }

    public String getListChargeur(String term) throws JsonProcessingException {
        SearchCriteria criterias = new SearchCriteria();
        // TODO Auto-generated method stub

        criterias.setSearchterm(term);
        // List<EbUser> listEbTransporteur =
        // daoUser.selectListEbUser(criterias);
        List<EbUser> listEbTransporteur = daoPricing.getListChargeur(term);

        return serializationJson(listEbTransporteur);
    }

    @Override
    public String getListCostCenter(String term) throws JsonProcessingException {
        List<EbCostCenter> listEbCommande = daoPricing.getListCostcenter(term);

        return serializationJson(listEbCommande);
    }

    @Override
    public EbDatatableState saveState(String state, Integer ebUserNum, Integer datatableCompId) {
        EbDatatableState datatable;
        System.out.println("ebUserNum = " + ebUserNum + " datatableCompId= " + datatableCompId);
        boolean isPresent = ebDatatableStateRepository
            .findOne(
                new BooleanBuilder()
                    .orAllOf(
                        QEbDatatableState.ebDatatableState.xEbUserNum.eq(ebUserNum),
                        QEbDatatableState.ebDatatableState.datatableCompId.eq(datatableCompId)))
            .isPresent();

        if (isPresent) {
            datatable = ebDatatableStateRepository
                .findOne(
                    new BooleanBuilder()
                        .orAllOf(
                            QEbDatatableState.ebDatatableState.xEbUserNum.eq(ebUserNum),
                            QEbDatatableState.ebDatatableState.datatableCompId.eq(datatableCompId)))
                .get();
        }
        else {
            datatable = new EbDatatableState();
        }

        if (datatable.getEbDatatableStateNum() == null) {
            datatable.setxEbUserNum(ebUserNum);
            datatable.setState(state);
            datatable.setDatatableCompId(datatableCompId);
        }

        if (datatable.getEbDatatableStateNum() != null) ebDatatableStateRepository
            .updateState(datatable.getEbDatatableStateNum(), state);
        else ebDatatableStateRepository.save(datatable);

        return datatable;
    }

    @Override
    public EbDatatableState loadState(Integer ebUserNum, Integer module) {
        return ebDatatableStateRepository.findFirstByXEbUserNumAndDatatableCompId(ebUserNum, module);
    }

    @Override
    public String getModeTrasport(String term) throws JsonProcessingException {
        List<EbDemande> listEbDmeande = daoPricing.getListModeTransport(term);

        return serializationJson(listEbDmeande);
    }

    @Override
    public List<EcCurrency> getListEcCurrency() {
        return ecCurrencyRepository.findAll();
    }

    @Override
    public List<EbTtCategorieDeviation> getListCategorieDeviation(Integer typeOfEvent) {

        if (typeOfEvent != null) {
            return ebTtCategorieDeviationRepository.findEbTtCategorieDeviationByTypeOfEvent(typeOfEvent);
        }

        return ebTtCategorieDeviationRepository.findAll();
    }

    @Override
    public EbChat historizeAction(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        String username,
        String usernameFor,
        Object... data) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        String lang = connectedUser != null && connectedUser.getLanguage() != null ? connectedUser.getLanguage() : "en";
        Locale lcl = new Locale(lang);

        EbChat chat = new EbChat(connectedUser);

        chat
            .setText(
                action + (username != null ?
                    " " + messageSource.getMessage("tracing.message_rd.by", null, lcl) + " <b>" + username + "</b>" :
                    ""));
        if (usernameFor != null) chat
            .setText(
                chat.getText() + " " + messageSource.getMessage("tracing.message_rd.for", null, lcl) + " <b>"
                    + usernameFor + "</b>");

        if (username == null) {
            username = connectedUser.getNom() + " " + connectedUser.getPrenom();
        }

        chat.setDateCreation(new Date());
        chat.setModule(module);
        chat.setIdFiche(idFiche);
        chat.setIsHistory(true);
        chat.setIdChatComponent(chatCompId);
        chat.setUserName(username);
        chatRepository.save(chat);

        return chat;
    }

    @Override
    public EbChat addChat(EbChat chat) {
        EbUser userConnected = connectedUserService.getCurrentUser();
        chat.setDateCreation(new Date());
        if (chat.getIsHistory() == null) chat.setIsHistory(true);

        if (chat.getModule() != Module.CUSTOM.getCode()) {
            chat.setUserName(userConnected.getNom() + " " + userConnected.getPrenom());
            chat.setChatOwner(new EbUser(userConnected.getEbUserNum()));
            chatRepository.save(chat);
        }
        else if (chat.getModule() == Module.CUSTOM.getCode()) {
            EbChatCustom chatCustom = new EbChatCustom();
            chatCustom.setDateCreation(chat.getDateCreation());
            chatCustom.setIdChatComponent(chat.getIdChatComponent());
            chatCustom.setIsHistory(chat.getIsHistory());
            chatCustom.setIdFiche(chat.getIdFiche());
            chatCustom.setUserAvatar(chat.getUserAvatar());
            chatCustom.setUserName(chat.getUserName());
            chatCustom.setText(chat.getText());
            chatCustom.setModule(chat.getModule());
            ebChatCustomRepository.save(chatCustom);
        }

        if ((Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(chat.getModule())
            || Enumeration.Module.PRICING.getCode().equals(chat.getModule())
            || Enumeration.Module.TRACK.getCode().equals(chat.getModule())) && !chat.getIsHistory()) {
            String username = chat.getUserName();
            EbChat commentaire = chat;
            EbDemande ebDemande = null;
            if (Enumeration.Module.TRACK.getCode().equals(chat.getModule())) ebDemande = ebTrackTraceRepository
                .selectEbDemandeDetailsByEbTracingNum(chat.getIdFiche());
            else ebDemande = ebDemandeRepository.findById(chat.getIdFiche()).get();

            try {
                emailService.sendMailAddComment(username, commentaire, ebDemande);
            } catch (Exception e) {
                LOGGER.error("Error sending MailAddComment ", e);
            }
        }

        return chat;
    }

    @Override
    public Map<String, Object>
        listDestinataireByEmailId(Integer mailId, Integer xEcModule, Integer idEntity, Object... params) {
        List<EbUser> resultat = new ArrayList<EbUser>();
        List<String> emails = new ArrayList<>();
        List<Integer> ids = new ArrayList<>();
        Map<String, Object> map = params != null && params.length > 0 ? (Map<String, Object>) params[0] : null;

        Map<String, Object> res = new HashMap<String, Object>();
        resultat = getUsersByMailId(mailId, xEcModule, idEntity, map);

        filterUsersByMailParameters(mailId, resultat, emails, ids);

        ///// Si le mail est un mail chargeur
        if (chargerMailIds.contains(mailId)) {
            findAndAddObservers(idEntity, ids, emails);
        }


        System.out.println("*******************************");
        System.out.println("Emails selected from listDestinataireByEmailId() before async send:");
        for (String email: emails) System.out.println(">>" + email);
        System.out.println("*******************************");

        res.put("emails", emails);
        res.put("ids", ids);
        return res;
    }

    private void findAndAddObservers(Integer idEntity, List<Integer> ids, List<String> emails) {
        //find observers
        List<EbUser> trObservers = demandeObserversRepository.selectObservers(Collections.singletonList(idEntity));
        if(!trObservers.isEmpty()){
            for (EbUser observer : trObservers){
                String lang = observer.getLanguage() != null && !observer.getLanguage().trim().isEmpty() ?
                        ";" + observer.getLanguage() :
                        ";fr";
                emails.add(observer.getEmail() + lang);
                ids.add(observer.getEbUserNum());
            }
        }
    }

    private List<EbUser>
        filterUsersByMailParameters(Integer mailId, List<EbUser> resultat, List<String> emails, List<Integer> ids) {
        List<EbUser> listUser = new ArrayList<>();

        for (EbUser user: resultat) {

            if (user.getListParamsMail() != null) {

                for (EbParamsMail param: user.getListParamsMail()) {

                    if (mailId.equals(param.getEmailId())
                        && Enumeration.EmailConfig.DEMANDE_CONFIRMATION_DE_PRISE_EN_CHARGE_DU_TRANSPORT
                            .getCode().equals(mailId)) {

                        // if(connectedUser.g)
                        if (user.getEmail() != null && !user.getEmail().trim().isEmpty()) {
                            String lang = user.getLanguage() != null && !user.getLanguage().trim().isEmpty() ?
                                ";" + user.getLanguage() :
                                ";fr";
                            emails.add(user.getEmail() + lang);
                            ids.add(user.getEbUserNum());
                            listUser.add(user);
                        }

                        break;
                    }
                    else {

                        if (mailId.equals(param.getEmailId())) {

                            if (user.getCreatedByMe() && param.getCreatedByMe()
                                || !user.getCreatedByMe() && param.getCreatedByOtherUser()) {

                                if (user.getEmail() != null && !user.getEmail().trim().isEmpty()) {
                                    String lang = user.getLanguage() != null && !user.getLanguage().trim().isEmpty() ?
                                        ";" + user.getLanguage() :
                                        ";fr";
                                    emails.add(user.getEmail() + lang);
                                    ids.add(user.getEbUserNum());
                                    listUser.add(user);
                                }

                            }

                            if (param.getListEmailEnCopie() != null) {
                                List<String> mails = param.getListEmailEnCopie();

                                for (String mail: mails) {
                                    emails.add(mail);
                                    ids.add(user.getEbUserNum());
                                    EbUser extUser = new EbUser();
                                    extUser.setEmail(mail);
                                    listUser.add(extUser);
                                }

                            }

                            break;
                        }

                    }

                }

            }

        }

        return listUser;
    }

    private List<EbUser>
        getUsersByMailId(Integer mailId, Integer xEcModule, Integer idEntity, Map<String, Object> map) {
        return getUsersByMailId(mailId, xEcModule, idEntity, false, map);
    }

    private List<EbUser> getUsersByMailId(
        Integer mailId,
        Integer xEcModule,
        Integer idEntity,
        boolean ignoreMailParamCheck,
        Map<String, Object> map) {
        List<EbUser> resultat;
        EbUser transporteur = null, connectedUser = connectedUserService.getCurrentUser();
        Integer ebTtEventNum = null, ebTtTracingNum = null, ebTtEventUserNum = null, ebUserNum = null;

        if (map != null) {

            // on vérifie si le transporteur est bien récupéré avec la compagnie
            // et l'établissement
            if (map.get("transporteur") != null) {
                transporteur = (EbUser) map.get("transporteur");

                if (transporteur.getEbUserNum() != null) {

                    if (transporteur.getEbCompagnie() == null || transporteur.getEbEtablissement() == null
                        || transporteur.getEbCompagnie().getEbCompagnieNum() == null
                        || transporteur.getEbEtablissement().getEbEtablissementNum() == null) {
                        if (transporteur
                            .getEbUserNum()
                            .equals(connectedUser.getEbUserNum())) transporteur = new EbUser(
                                connectedUser.getEbUserNum(),
                                connectedUser.getEbEtablissement().getEbEtablissementNum(),
                                connectedUser.getEbCompagnie().getEbCompagnieNum());
                        else transporteur = ebUserRepository.findOneByEbUserNum(transporteur.getEbUserNum());
                    }

                }
                else {
                    transporteur = null;
                    System.err
                        .println("'Transporteur' in map is not null, but no 'compagnie' or 'etablissement' is given");
                }

            }

            if (map.get("ebTtEventNum") != null) ebTtEventNum = (Integer) map.get("ebTtEventNum");
            if (map.get("ebTtTracingNum") != null) ebTtTracingNum = (Integer) map.get("ebTtTracingNum");
            if (map.get("ebTtEventUserNum") != null) ebTtEventUserNum = (Integer) map.get("ebTtEventUserNum");
        }

        if (Enumeration.EmailConfig.QUOTATION_RENSEIGNEMENT_INFORMATIONS_TRANSPORT.getCode().equals(mailId)) {
            transporteur = new EbUser(connectedUser.getEbUserNum());
        }

        resultat = daoUser
            .listDestinataireByEmailId(
                idEntity,
                mailId,
                xEcModule,
                transporteur,
                ebTtEventNum,
                ebTtTracingNum,
                ebTtEventUserNum,
                ignoreMailParamCheck);
        return resultat;
    }

    @Override
    public String getListAgent(String term) throws JsonProcessingException {
        SearchCriteria criterias = new SearchCriteria();
        // TODO Auto-generated method stub

        criterias.setSearchterm(term);
        criterias.setUserService(Enumeration.ServiceType.SERVICE_BROKER.getCode());
        criterias.setEcRoleNum(Enumeration.Role.ROLE_PRESTATAIRE.getCode());
        List<EbUser> listEbTransporteur = daoUser.selectListEbUser(criterias);
        criterias.setUserService(Enumeration.ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        List<EbUser> listEbTransporteur1 = daoUser.selectListEbUser(criterias);
        listEbTransporteur.addAll(listEbTransporteur1);
        return serializationJson(listEbTransporteur);
    }

    @Override
    public String getListClearanceID(String term) throws JsonProcessingException {
        List<EbCustomDeclaration> listEbDeclaration = daoDeclaration.getListClearanceId(term);
        return serializationJson(listEbDeclaration);
    }

    @Override
    public String getListShiper(String term) throws JsonProcessingException {
        SearchCriteria criterias = new SearchCriteria();
        // TODO Auto-generated method stub

        criterias.setSearchterm(term);
        criterias.setEcRoleNum(Enumeration.Role.ROLE_CHARGEUR.getCode());
        List<EbUser> listEbCharger = daoUser.selectListEbUser(criterias);

        return serializationJson(listEbCharger);
    }

    @Override
    public String getValueTtPsl(Integer key) {
        String value = TtEnumeration.TtPsl.getLibelleByCode(key);
        return value;
    }

    @Override
    public String getListModeTransport() throws JsonProcessingException, IOException {
        String value = Enumeration.ModeTransport.getListModeTransport();
        return value;
    }

    @Override
    public String getListExtractors() throws JsonProcessingException, IOException {
        String value = Enumeration.Extractors.getListExtractors();
        return value;
    }

    @Override
    public String getAutoCompleteListDelivery(String term, String field) throws JsonProcessingException {
        List<EbLivraison> listLivraison = deliveryService.getAutoCompleteList(term, field);
        return serializationJson(listLivraison);
    }

    @Override
    public String getAutoCompleteListOrder(String term, String field) throws JsonProcessingException {
        List<EbOrder> listOrder = daoOrder.getAutoCompleteList(term, field);
        return serializationJson(listOrder);
    }

    @Override
    public String getAutoCompleteListMarchandise(String term, String field) throws JsonProcessingException {
        List<EbMarchandise> listMarchandise = daoMarchandise.getAutoCompleteList(term, field);
        return serializationJson(listMarchandise);
    }

    @Override
    public List<EbParamsMail> getListEmailParam() {
        return DaoEmailImpl.listEmail;
    }

    @Override
    public Map<Integer, List<EbTypeTransport>> getlistAllTypeTransport() {
        Map<Integer, List<EbTypeTransport>> result = new HashMap<>();
        List<EbTypeTransport> typeTransportList = ebTypeTransportRepository.findAll();

        for (Enumeration.ModeTransport modeTransport: Enumeration.ModeTransport.values()) {
            List<EbTypeTransport> transportsList = new ArrayList<>();

            for (EbTypeTransport ebTypeTransport: typeTransportList) {

                if (modeTransport.getCode().equals(ebTypeTransport.getxEcModeTransport())) {
                    transportsList.add(ebTypeTransport);
                }

            }

            if (transportsList.size() > 0) result.put(modeTransport.getCode(), transportsList);
        }

        return result;
    }

    @Override
    public List<EcModule> getListEcModule() {
        return ecModuleRepository.findAllByOrderByOrdreAsc();
    }

    @Override
    public List<CodeLibelleDTO> getListStatusByModule(Integer moduleNum) throws IOException {
        List<CodeLibelleDTO> liste = new ArrayList<CodeLibelleDTO>();

        if (moduleNum.equals(Module.PRICING.getCode())) {
            return Enumeration.StatutDemande.getListStatutDemandeBookingDTO();
        }
        else if (moduleNum.equals(Module.TRANSPORT_MANAGEMENT.getCode())) {
            return Enumeration.StatutDemande.getListStatutDemandeBookingDTO();
        }
        else if (moduleNum.equals(Module.QUALITY_MANAGEMENT.getCode())) {
            return Enumeration.IncidentStatus.getListStatutIncidentDTO();
        }
        else if (moduleNum.equals(Module.RECEIPT_SCHEDULING.getCode())) {
            // TODO update with generic enum
            // return
            // RSCHEnumeration.RschUnitScheduleStatus.getLisRschUnitScheduleStatusDTO();
        }

        return liste;
    }

    @Override
    public Class<? extends SearchCriteria> getSearchCriteriaClassByModule(Integer ebModuleNum) {

        if (Module.PRICING.getCode().equals(ebModuleNum) || Module.TRANSPORT_MANAGEMENT.getCode().equals(ebModuleNum)) {
            return SearchCriteriaPricingBooking.class;
        }
        else if (Module.TRACK.getCode().equals(ebModuleNum)) {
            return SearchCriteriaTrackTrace.class;
        }
        else if (Module.QUALITY_MANAGEMENT.getCode().equals(ebModuleNum)) {
            return SearchCriteriaQM.class;
        }
        else if (Module.FREIGHT_AUDIT.getCode().equals(ebModuleNum)) {
            return SearchCriteriaFreightAudit.class;
        }
        else if (Module.FREIGHT_ANALYTICS.getCode().equals(ebModuleNum)) {
            return SearchCriteriaFreightAnalytics.class;
        }
        else if (Module.RECEIPT_SCHEDULING.getCode().equals(ebModuleNum)) {
            return SearchCriteriaRSCH.class;
        }
        else if (Module.ORDER_MANAGEMENT.getCode().equals(ebModuleNum)) {
            return SearchCriteriaOrder.class;
        }
        else {
            return SearchCriteria.class;
        }

    }

    @Override
    public List<GenericEnum> listEnumValues(String genericEnumKey) throws GenericEnumException {
        return GenericEnumUtils.getAllValues(genericEnumKey);
    }

    @Override
    public String listEnumValuesJson(String genericEnumKey) throws GenericEnumException {
        List<GenericEnum> values = listEnumValues(genericEnumKey);

        ObjectMapper mapper = new ObjectMapper();
        ArrayNode arrayNode = mapper.createArrayNode();

        for (GenericEnum item: values) {
            ObjectNode jsonObj = mapper.createObjectNode();
            jsonObj.set("code", mapper.valueToTree(item.getCode()));
            jsonObj.set("key", mapper.valueToTree(item.getKey()));

            arrayNode.add(jsonObj);
        }

        return arrayNode.toString();
    }

    @Override
    public List<EcFuseauxHoraire> getListFuseauxHoraire() {

        if (this.listEcFuseauxHoraire == null || this.listEcFuseauxHoraire.isEmpty()) {
            this.listEcFuseauxHoraire = ecFuseauxHoraireRepository.findAll();
        }

        return this.listEcFuseauxHoraire;
    }

    @Override
    public List<EcMimetype> getListMimeType() {

        if (this.listMimeType == null || this.listMimeType.isEmpty()) {
            this.listMimeType = ecMimeTypeRepository.findAll();
        }

        return this.listMimeType;
    }

    @Override
    public Map<String, List<EbDemandeFichiersJoint>>
        searchlistDocuments(Integer module, Integer ebUserNum, Integer type, Integer demande)
            throws JsonProcessingException {
        Map<String, List<EbDemandeFichiersJoint>> map = new HashMap<>();
        List<EbDemandeFichiersJoint> listFichierJoint;

        List<Integer> listModule = new ArrayList<Integer>();
        List<EcModule> listEcModule = ecModuleRepository.findAll();

        for (EcModule ecModule: listEcModule) {
            listModule.add(ecModule.getEcModuleNum());
        }

        listFichierJoint = ebDemandeFichiersJointRepositorty.findAllByNumEntityAndModuleIn(demande, listModule);

        for (EbDemandeFichiersJoint f: listFichierJoint) {
            if (!map.containsKey(f.getIdCategorie())) map.put(f.getIdCategorie(), new ArrayList<>());

            map.get(f.getIdCategorie()).add(f);
        }

        return map;
    }

    @Override
    public List<EbUser> getxEbOwnerOfTheRequest(Integer module) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        List<EbUser> listUser = null;

        if (Module.DELIVERY_MANAGEMENT.getCode().equals(module)) {
            listUser = ebLivraisonRepository.getListOwnerRequest(connectedUser.getEbCompagnie().getEbCompagnieNum());
        }
        else {
            listUser = ebOrderRepository.getListOwnerRequest(connectedUser.getEbCompagnie().getEbCompagnieNum());
        }
        return listUser;
    }

    @Override
    public Map<String, Object>
        getEmailUsers(Integer compagnieNum, Integer mailId, Integer xEcModule, Integer ebDemandeNum, Object... params) {
        List<String> finalEmails = null;

        if (compagnieNum == null) {
            EbUser connectedUser = connectedUserService.getCurrentUser();
            compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();
        }

        Map<String, Object> res = this.listDestinataireByEmailId(mailId, xEcModule, ebDemandeNum, params);

        // getting control rule with the action send email alert and whose
        // conditions are applicable on current demande
        List<EbControlRule> rules = this.controlRuleService
            .getRulesWithEmailId(compagnieNum, RuleCategory.DOCUMENT.getCode(), mailId);
        LOGGER.debug("** getRulesWithEmailId.size :" + (rules != null ? rules.size() : 0));

        if (rules != null && !rules.isEmpty()) {
            EbControlRule rule = this.controlRuleService // get first rule whose
                                                         // conditions are
                                                         // applicable on
                                                         // current demande
                .executeRulesWithSendAlertActionOnDemande(mailId, RuleCategory.DOCUMENT.getCode(), ebDemandeNum, rules);

            if (rule != null) {
                finalEmails = this.getFinalEmails(rule, (List<String>) res.get("emails"));
                res.put("emails", finalEmails);
            }
            else // no rule was verified
            return res;

        }
        else return res;

        return res;
    }

    private String getEmail(List<String> mails, String userMail) {

        for (String mail: mails) {
            if (mail.startsWith(userMail)) return mail;
        }

        return null;
    }

    private List<String> getFinalEmails(EbControlRule rule, List<String> mails) {
        List<String> allEmailsDest = null, nonPlatformUsers = null, platformUsers = null, finalEmails = null;

        allEmailsDest = Arrays
            .asList(rule.getEmailDestinataires().split(";")).stream().filter(dest -> dest != null && !dest.isEmpty())
            .collect(Collectors.toList()); // get all destinataires in control
                                           // rule

        platformUsers = this.ebUserRepository.getUsersWithEmails(allEmailsDest); // extract
                                                                                 // users
                                                                                 // registred
                                                                                 // in
                                                                                 // MyTower
                                                                                 // from
                                                                                 // destinataires
                                                                                 // list

        if (platformUsers != null && !platformUsers.isEmpty()) {
            finalEmails = new ArrayList<>();

            if (mails != null && !mails.isEmpty()) for (String userMail: platformUsers) { // intersection
                                                                                          // between
                                                                                          // platform_users
                                                                                          // and
                                                                                          // mails,
                                                                                          // result
                                                                                          // ->
                                                                                          // users
                                                                                          // (in
                                                                                          // rule
                                                                                          // and
                                                                                          // mytower)
                                                                                          // with
                                                                                          // checkbox
                                                                                          // alerte
                                                                                          // enabled
                String mailFound = this.getEmail(mails, userMail);
                if (mailFound != null) finalEmails.add(mailFound);
            }

            nonPlatformUsers = new ArrayList<>();

            for (String userMail: allEmailsDest) // add non plateforme users
            {
                if (!platformUsers.contains(userMail)) nonPlatformUsers.add(userMail);
            }

            finalEmails.addAll(nonPlatformUsers); // users with enabled alerte
                                                  // && non platform users
        }
        else {
            finalEmails = allEmailsDest; // all users are not in the platform
            finalEmails.addAll(mails);
        }

        return finalEmails;
    }

    @Override
		public Map<String, Object> listDestinataireUserByEmailId(
			Integer mailId,
			Integer xEcModule,
			Integer idEntity,
			boolean ignoreMailParamCheck,
			Map<String, Object> params
		)
		{
        List<EbUser> resultat = new ArrayList<EbUser>();
        List<String> emails = new ArrayList<>();
        List<Integer> ids = new ArrayList<>();
        Map<String, Object> res = new HashMap<String, Object>();

				resultat = getUsersByMailId(mailId, xEcModule, idEntity, ignoreMailParamCheck, params);

        if (!ignoreMailParamCheck) resultat = filterUsersByMailParameters(mailId, resultat, emails, ids);

        System.out.println("*******************************");
        System.out.println("Emails selected from listDestinataireByEmailId() before async send:");
        for (String email: emails) System.out.println(">>" + email);
        System.out.println("*******************************");
        res.put("emails", emails);
        res.put("ids", ids);
        res.put("users", resultat);
        return res;
    }

		@Override
		public Map<String, Object> getEmailUsersWithMailParamCheck(
			Integer compagnieNum,
			Integer mailId,
			Integer xEcModule,
			Integer ebDemandeNum,
			boolean ignoreMailParamCheck,
			Map<String, Object> params
		)
		{
        List<String> finalEmails = null;

        if (compagnieNum == null) {
            EbUser connectedUser = connectedUserService.getCurrentUser();
            compagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();
        }

        Map<String, Object> res = this
            .listDestinataireUserByEmailId(mailId, xEcModule, ebDemandeNum, ignoreMailParamCheck, params);
        List<EbUser> users = (List<EbUser>) res.get("users");

        List<String> usersMails = new ArrayList<>();

        if (users != null && !users.isEmpty()) {
            usersMails = users.stream().map(u -> u.getEmail()).collect(Collectors.toList());
        }

        res.put("emails", usersMails);

        // getting control rule with the action send email alert and whose
        // conditions are applicable on current demande
        List<EbControlRule> rules = this.controlRuleService
            .getRulesWithEmailId(compagnieNum, RuleCategory.DOCUMENT.getCode(), mailId);

        if (rules != null && !rules.isEmpty()) {
            EbControlRule rule = this.controlRuleService // get first rule whose
                                                         // conditions are
                                                         // applicable on
                                                         // current demande
                .executeRulesWithSendAlertActionOnDemande(mailId, RuleCategory.DOCUMENT.getCode(), ebDemandeNum, rules);

            if (rule != null) {
                finalEmails = this.getFinalEmails(rule, (List<String>) res.get("emails"));
                res.put("emails", finalEmails);
            }
            else // no rule was verified
            return res;

        }
        else return res;

        return res;
    }

    public List<EbUser>
        listUserByEmailId(Integer mailId, Integer xEcModule, Integer idEntity, boolean checkAlert, Object... params) {
        List<EbUser> resultat = new ArrayList<EbUser>();
        List<String> emails = new ArrayList<>();
        Map<String, Object> map = params != null && params.length > 0 ? (Map<String, Object>) params[0] : null;

        resultat = getUsersByMailId(mailId, xEcModule, idEntity, checkAlert, map);

        return resultat;
    }

    @Override
    public EbChat historizeAction(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        EbUser creatorUser,
        String username,
        String usernameFor,
        Object... data) {
        EbChat chat = this.createEbChat(idFiche, chatCompId, module, action, creatorUser, username, usernameFor, data);
        chatRepository.save(chat);

        return chat;
    }

    private EbChat createEbChat(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        EbUser creatorUser,
        String username,
        String usernameFor,
        Object... data) {
        EbUser connectedUser = creatorUser != null ? creatorUser : connectedUserService.getCurrentUser();
        String lang = connectedUser != null && connectedUser.getLanguage() != null ? connectedUser.getLanguage() : "en";
        Locale lcl = new Locale(lang);

        EbChat chat = new EbChat(creatorUser);

        chat
            .setText(
                action + (username != null ?
                    " " + messageSource.getMessage("tracing.message_rd.by", null, lcl) + " <b>" + username + "</b>" :
                    ""));
        if (usernameFor != null) chat
            .setText(
                chat.getText() + " " + messageSource.getMessage("tracing.message_rd.for", null, lcl) + " <b>"
                    + usernameFor + "</b>");

        if (username == null) {
            username = connectedUser.getNom() + " " + connectedUser.getPrenom();
        }

        chat.setDateCreation(new Date());
        chat.setModule(module);
        chat.setIdFiche(idFiche);
        chat.setIsHistory(true);
        chat.setIdChatComponent(chatCompId);
        chat.setUserName(username);
        return chat;
    }

    @Override
    public File
        consolidateDocByEntityAndModule(List<Integer> modules, Integer entityNum) throws Docx4JException, IOException {
        String genFilePath = serviceConfiguration.getTempFolderPath() + File.separator
            + String.format("generateFile-%d", System.currentTimeMillis()) + ".pdf";
        final File genFile = new File(genFilePath);
        PDFMergerUtility ut = new PDFMergerUtility();

        List<EbDemandeFichiersJoint> listFichierJoint = ebDemandeFichiersJointRepositorty
            .findAllByNumEntityAndModuleIn(entityNum, modules);
        boolean foundFile = false;

        for (EbDemandeFichiersJoint fj: listFichierJoint) {
            String[] splitFj = fj.getOriginFileName().split("\\.");

            if (splitFj.length > 1) {

                try (InputStream fjInputStream = storageProvider.get(Paths.get(fj.getChemin()))) {

                    if (splitFj[1].equalsIgnoreCase(PDF)) {
                        Path tempFile = Files.createTempFile(null, ".".concat(PDF));
                        try (OutputStream fileOutputStream = Files.newOutputStream(tempFile)) {
                            IoUtils.copy(fjInputStream, fileOutputStream);
                        }
                        ut.addSource(tempFile.toFile());
                        foundFile = true;
                    }
                    else if (splitFj[1].equals(DOCX)) {
                        Path tempPdfFile = Files.createTempFile(null, ".".concat(PDF));
                        DocumentUtils.convertDocxToPdf(fjInputStream, tempPdfFile);
                        ut.addSource(tempPdfFile.toFile());
                        foundFile = true;
                    }
                }

            }

        }

        ut.setDestinationFileName(genFilePath);
        ut.mergeDocuments(MemoryUsageSetting.setupMainMemoryOnly());

        if (!foundFile) {
            return null;
        }

        return genFile;
    }

    @Override
    public List<EbChat> getListChat(Integer module, Integer idFiche, Integer idChatComponent) {
        return chatService.getListChat(module, idFiche, idChatComponent);
    }
}
