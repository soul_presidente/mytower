/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service.email;

import java.math.BigDecimal;
import java.util.List;

import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbInvoice;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;
import com.adias.mytowereasy.template.model.pricing.EbDemandeTPL;


public interface EmailServicePricingBooking {
	void sendEmailQuotationRequest(EbDemande ebDemande, EbUser transporteur);

    void sendEmailQuotationForwarder(
        EbUser user,
        EbDemande ebDemande,
        List<ExEbDemandeTransporteur> listExEbDemandeTransporteur);

    void sendEmailQuotationRecommendation(EbDemande ebDemande);

    List<String> sendEmailQuotationForwarderSelected(
        EbUser connectedUser,
        EbDemande ebDemande,
        ExEbDemandeTransporteur exEbDemandeTransporteur);

    void sendMailQuotationResponseTransporteur(
        EbUser user,
        EbDemande ebDemande,
        List<ExEbDemandeTransporteur> listExEbDemandeTransporteur);

    void sendMailQuotationResponseChargeur(
        EbUser user,
        EbDemande ebDemande,
        List<ExEbDemandeTransporteur> listExEbDemandeTransporteur);

    void sendEmailTranpsportInformation(EbDemande ebDemande, EbUser user);

    void sendEmailQuotationRequestViaSendQuotation(
        EbDemande ebDemande,
        ExEbDemandeTransporteur exEbDemandeTransporteur);

    void sendEmailQuotationForwarderNotSelected(EbDemande ebDemande, ExEbDemandeTransporteur exEbDemandeTransporteur);

    void sendEmailQuotationResquestChargeur(EbDemande ebDemande, List<EbUser> transpoteurs);

    void sendEmailCreationTransportFile(EbDemande ebDemande);

    void sendEmailPricingDataChanged(EbDemande ebDemande, String actions);

    void sendMailQuotationAlerteCt(EbUser user, EbDemande ebDemande, ExEbDemandeTransporteur exEbDemandeTransporteur);

    void sendEmailPricingCancelled(EbDemande ebDemande);

    void sendEmailQuotationaCknowledgeTdc(EbDemande ebDemande);

    void sendEmailDemandeConfirmationOfTransportSupport(EbDemande ebDemande);

    void sendEmailPricingConfirmationPriseEnCharge(EbDemande ebDemande);

    void sendEmailConfirmPendingTransport(EbDemande ebDemande);

    void sendEmailSharingInformation(EbInvoice invoice, List<Integer> actions);

    void sendEmailSharingOneInformation(EbInvoice invoice, BigDecimal action);

    void sendEmailApplyingInformation(EbInvoice invoice, List<Integer> actions);

    void sendEmailTemplateInformationNotCompleted(EbDemandeTPL ebDemandeTPL, String libelle);

    boolean checkedAllInformationMandotory(EbDemandeTPL ebDemandeTPL);

    void sendEmailPricingFileUpdated(EbDemande ebDemande, EbUser connectedUser);
}
