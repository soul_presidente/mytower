/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.dto.CustomDeclarationDocumentDTO;
import com.adias.mytowereasy.dto.CustomDeclarationPricingDTO;
import com.adias.mytowereasy.model.EbDemandeFichiersJoint;
import com.adias.mytowereasy.model.custom.EbCustomDeclaration;
import com.adias.mytowereasy.utils.search.SearchCriteriaCustom;


public interface CustomDeclarationService {
    public int getCountListEbDeclaration(SearchCriteriaCustom criteria);

    public List<EbCustomDeclaration> getListEbDeclaration(SearchCriteriaCustom criteria);

    EbCustomDeclaration addEbDeclaration(EbCustomDeclaration ebDeclaration);

    EbCustomDeclaration updateDateDeclaration(EbCustomDeclaration declaration, Date newDateDeclaration);

    EbCustomDeclaration updateDateDeclaration(Integer declarationNum, Date newDateDeclaration);

    void historizeActioncUSTOM(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        String username,
        String usernameFor);

    public EbCustomDeclaration inlineProcedureDashboardUpdate(EbCustomDeclaration sent);

    EbCustomDeclaration getEbDeclaration(SearchCriteriaCustom criteria);

    EbCustomDeclaration newDeclarationPricing(CustomDeclarationPricingDTO request) throws Exception;

    List<CustomDeclarationDocumentDTO>
        searchlistDocuments(Integer module, Integer ebUserNum, Integer type, Integer demande)
            throws JsonProcessingException;

    List<EbDemandeFichiersJoint> listFichierJointsDeclaration(Integer ebDeclarationNum, Integer ebCompagnieNum);
}
