/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service.storage;

import org.springframework.web.multipart.MultipartFile;


public interface StorageService {
    Object store(MultipartFile file, Integer codeTypeFile, Object... params) throws Exception;
}
