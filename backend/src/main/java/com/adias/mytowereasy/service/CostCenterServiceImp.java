package com.adias.mytowereasy.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbCostCenter;


@Service
public class CostCenterServiceImp extends MyTowerService implements CostCenterService {
    @Override
    public List<EbCostCenter> getListCostCenterCompagnie(Integer ebCompagnieNum) {
        return daoCostCenter.findActiveCostCenters(ebCompagnieNum);
    }

    @Override
    public EbCostCenter addEbCostCenterEbCompagnie(EbCostCenter ebCostCenter) {
        return ebCostCenterRepository.save(ebCostCenter);
    }

    @Override
    public EbCostCenter updateEbCostCenterEbCompagnie(EbCostCenter ebCostCenter) {
        return ebCostCenterRepository.save(ebCostCenter);
    }

    @Override
    public Boolean deleteEbCostCenterEbCompagnie(EbCostCenter ebCostCenter) {
        ebCostCenter.setDeleted(true);
        ebCostCenterRepository.save(ebCostCenter);
        return true;
    }
}
