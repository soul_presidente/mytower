package com.adias.mytowereasy.service;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbUserTag;


@Service
public class UserTagServiceImp extends MyTowerService implements UserTagService {
    @Override
    public EbUserTag addUserTag(EbUserTag ebUsertag, Integer ebCompagnieNum) {
        ebUserTagRepository.updateActivated(ebCompagnieNum, ebUsertag.getGuest().getEbUserNum());
        return ebUserTagRepository.save(ebUsertag);
    }
}
