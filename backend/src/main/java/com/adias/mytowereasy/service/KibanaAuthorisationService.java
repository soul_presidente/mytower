package com.adias.mytowereasy.service;

import javax.servlet.http.HttpServletRequest;

import com.adias.mytowereasy.model.KibanaUser;
import com.adias.mytowereasy.util.KibanaResponse;


public interface KibanaAuthorisationService {
    KibanaUser getUserFromToken(HttpServletRequest request);

    KibanaResponse getKibanaToken(String access_token, long expirationTime);
}
