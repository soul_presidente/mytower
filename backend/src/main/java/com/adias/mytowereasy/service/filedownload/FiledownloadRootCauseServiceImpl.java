package com.adias.mytowereasy.service.filedownload;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbCustomField;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.model.qm.EbQmRootCause;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.QualityManagementService;
import com.adias.mytowereasy.service.RootCauseService;
import com.adias.mytowereasy.service.category.CategoryService;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


@Service
public class FiledownloadRootCauseServiceImpl extends MyTowerService implements FiledownloadRootCauseService {
    @Autowired
    ConnectedUserService connectedUserService;
    @Autowired
    QualityManagementService qualityManagementService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    RootCauseService rootCauseService;

    @Override
    public FileDownloadStateAndResult listFileDownLoadRootCause(
        Map<String, Object> mapCriteria,
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        List<EbCategorie> listCategorie,
        EbCustomField customField,
        String strCriteria,
        Map<String, String> mapCustomField,
        Map<String, List<Integer>> mapCategoryFields)
        throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();

        List<EbCustomField> fields = new ArrayList<EbCustomField>();
        SearchCriteriaQM searchCriteria = new SearchCriteriaQM();

        strCriteria = gson.toJson(mapCriteria);
        searchCriteria = gson.fromJson(strCriteria, SearchCriteriaQM.class);

        Map<String, Object> listcatAndCustom = categoryService
            .getListCategoryAndCustomField(searchCriteria, connectedUser);

        fields = (List<EbCustomField>) listcatAndCustom.get("customField");
        listCategorie = (List<EbCategorie>) listcatAndCustom.get("listCategorie");

        if (!mapCustomField.isEmpty()) searchCriteria.setMapCustomFields(mapCustomField);

        if (!mapCategoryFields.isEmpty()) searchCriteria.setMapCategoryFields(mapCategoryFields);
        List<EbQmRootCause> listRoootCause = rootCauseService.listRootCause(searchCriteria);

        if (searchCriteria.getEbCompagnieNum() == null) {
            searchCriteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
        }

        if (searchCriteria.getEbEtablissementNum() == null) {
            searchCriteria.setEbEtablissementNum(connectedUser.getEbEtablissement().getEbEtablissementNum());
        }

        for (EbQmRootCause rootCause: listRoootCause) {
            Map<String, String[]> map = new LinkedHashMap<String, String[]>();

            map.put("ref", new String[]{
                "Reference", rootCause.getRef()
            });

            map.put("label", new String[]{
                "Label", rootCause.getLabel()
            });

            map.put("name", new String[]{
                "Name", rootCause.getName()
            });

            map.put("groupe", new String[]{
                "Groupe", rootCause.getGroupe()
            });

            map.put("relatedIncident", new String[]{
                "Related Incident", rootCause.getRelatedIncident()
            });

            map.put("createur", new String[]{
                "Createur", rootCause.getCreateur()
            });

            if (listCategorie != null && !listCategorie.isEmpty()) {

                for (EbCategorie cat: listCategorie) {
                    if (isPropertyInDtConfig(dtConfig, "category" + cat.getEbCategorieNum())) map
                        .put("category" + cat.getEbCategorieNum(), new String[]{
                            cat.getLibelle(), null
                        });
                }

            }

            listresult.add(map);
        }

        resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
        resultAndState.setListResult(listresult);
        return resultAndState;
    }
}
