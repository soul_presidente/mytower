package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.EbEmailHistorique;


public interface EmailHistoriqueService {

    // List<EbEmailHistorique> getAllEmailHistorique1();
    List<EbEmailHistorique> getAllEmailHistorique(Integer ebIncidentNum);
}
