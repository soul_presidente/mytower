/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service.email;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.adias.mytowereasy.exception.ErrorDetails;
import com.adias.mytowereasy.model.EbChat;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.EmailConfig;
import com.adias.mytowereasy.model.Enumeration.EmailInvitation;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.ListStatiqueService;
import com.adias.mytowereasy.service.NotificationService;
import com.adias.mytowereasy.util.email.Email;
import com.adias.mytowereasy.util.email.EmailHtmlSender;


@Service
public class EmailServiceImpl implements EmailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);

    @Autowired
    ListStatiqueService serviceListStatique;

    @Autowired
    private ConnectedUserService connectedUserService;
    @Autowired
    private EmailHtmlSender emailHtmlSender;

    @Autowired
    private NotificationService notificationService;

    @Value("${mytowereasy.url.front}")
    private String url;

    @Value("${mytowereasy.url.back}")
    private String urlBack;

    @Value("${mytower.login-page.url}")
    private String nomPlateform;

    @Value("${mytowereasy.exception.emails}")
    private String exceptionEmails;

    @Value("${mytower.team.mail}")
    private String mytowerTeamMail;

    @Value("${ct.admin.mails}")
    public String ctAdminsMails;

    @Autowired
    public EmailServiceImpl(EmailHtmlSender emailHtmlSender) {
        this.emailHtmlSender = emailHtmlSender;
    }

    public void sendEmailInvitationFriend(EbUser user, List<String> emails, EmailConfig emailParams) {

        if (emails != null && emailParams != null) {
            Context context = new Context();
            context.setVariable("nom", (user != null) ? user.getNom() : "");
            context.setVariable("url", url);

            Email email;
            List<Map<String, Object>> listMapEmail = new ArrayList<Map<String, Object>>();
            Map<String, Object> mapEmail;

            for (String to: emails) {
                email = new Email();

                String subject = "invitefriend.subject";
                email.setSubject(subject);
                email.setEmailConfig(EmailConfig.INVITE_FRIEND);

                email.setTo(to);

                mapEmail = new HashMap<String, Object>();
                mapEmail.put("context", context);
                mapEmail.put("templateName", emailParams.getTemplate());
                mapEmail.put("email", email);
                mapEmail.put("subject", email.getSubject());

                listMapEmail.add(mapEmail);
            }

            emailHtmlSender.sendListEmail(listMapEmail);
        }

    }

    @Override
    public void sendEmailResetPassword(EbUser user) {

        if (user != null) {
            Context context = new Context();

            context.setVariable("url", url);

            context.setVariable("urlBack", urlBack);

            if (!nomPlateform.equals("generic")) {
                context.setVariable("nomPlateform", "-" + nomPlateform);
            }
            else {
                context.setVariable("nomPlateform", "");
            }

            ;

            context.setVariable("nom", user.getNom());

            context.setVariable("resetToken", user.getResetToken());

            Email email = new Email();

            String nom = user.getNom();

            String[] args = {
                ""
            };

            String subject = "resetpassword.subject";
            email.setArguments(args);

            email.setSubject(subject);
            email.setEmailConfig(EmailConfig.RESET_PASSWORD);

            email.setTo(user.getEmail());

            emailHtmlSender.send(email, "6-email-reset-password", context);
        }

    }

    @Override
    public void sendEmailUpdatedPassword(EbUser user) {

        if (user != null) {
            Context context = new Context();

            context.setVariable("nom", user.getNom());

            Email email = new Email();

            String[] args = {
                ""
            };

            String subject = "resetupdatepassword.subject";
            email.setArguments(args);

            email.setSubject(subject);
            email.setEmailConfig(EmailConfig.UPDATED_PASSWORD);

            email.setTo(user.getEmail());

            emailHtmlSender.send(email, "7-email-update-password", context);
        }

    }

    @Override
    public void sendEmailActivePassword(EbUser user) {

        if (user != null) {
            Context context = new Context();

            context.setVariable("url", url);

            context.setVariable("nom", user.getNom());

            context.setVariable("activeToken", user.getActiveToken());

            Email email = new Email();

            String[] args = {
                ""
            };
            String subject = "activercompte.subject";
            email.setArguments(args);
            email.setEmailConfig(EmailConfig.ACTIVATE_ACCOUNT);

            email.setTo(user.getEmail());

            email.setSubject(subject);

            emailHtmlSender.send(email, "1-email-activate-account", context);
        }

    }

    @Override
    public void sendEmailInvitationCollegue(EbUser user, List<String> emails, EmailConfig emailParams) {

        if (user != null && emails != null && emailParams != null) {
            Context context = new Context();

            context.setVariable("nom", user.getNom());

            context.setVariable("url", url);

            context.setVariable("siret", user.getEbEtablissement().getSiret());
            Email email;
            List<Map<String, Object>> listMapEmail = new ArrayList<Map<String, Object>>();
            Map<String, Object> mapEmail;

            for (String to: emails) {
                email = new Email();

                String[] args = {};
                String subject = "invitefriend.subject";
                email.setSubject(subject);
                email.setEmailConfig(EmailConfig.INVITE_COLLEGUE);

                email.setTo(to);
                mapEmail = new HashMap<String, Object>();
                mapEmail.put("email", email);

                mapEmail.put("context", context);
                mapEmail.put("templateName", emailParams.getTemplate());

                mapEmail.put("subject", email.getSubject());

                listMapEmail.add(mapEmail);
            }

            emailHtmlSender.sendListEmail(listMapEmail);
        }

    }

    @Override
    public void sendEmailInvitationAccepter(EbUser destinataire, EbUser expediteur) {
        Context params = new Context();

        params.setVariable("nomDestinataire", destinataire.getNom());

        params.setVariable("nomExpediteur", expediteur.getNom());

        Email email = new Email();
        email.setTo(expediteur.getEmail());
        email.setEmailConfig(EmailConfig.EMAIL_INVITATION_ACCEPTER);
        email.setSubject("invitation.accepted");

        emailHtmlSender.send(email, EmailInvitation.INVITATION_ACCEPTER.getTemplate(), params);
    }

    @Override
    public void sendEmailCompteActiverParAdminSocieter(EbUser user) {

        if (user != null) {
            Context context = new Context();

            context.setVariable("url", url);

            context.setVariable("nom", user.getNom());

            context.setVariable("activeToken", user.getActiveToken());

            Email email = new Email();

            String[] args = {
                ""
            };
            String subject = "nvcomptesocieteadminaccept.subject";
            email.setArguments(args);

            email.setSubject(subject);
            email.setEmailConfig(EmailConfig.COMPTE_ACTIVER_PAR_ADMIN);

            email.setTo(user.getEmail());

            emailHtmlSender.send(email, "10-email-compte-activer-par-admin", context);
        }

    }

    @Override
    public void sendEmailNouveauCompteSociete(EbUser nouvelUtilisateur, List<EbUser> adminsUser) {

        if (nouvelUtilisateur != null) {
            Context context = new Context();

            context.setVariable("url", url);

            context.setVariable("nomNouveauUtilisateur", nouvelUtilisateur.getNom());

            context.setVariable("prenomNouveauUtilisateur", nouvelUtilisateur.getPrenom());

            context.setVariable("nomEstablishement", nouvelUtilisateur.getEbEtablissement().getNom());

            Email email = new Email();

            String nomNvUser = nouvelUtilisateur.getNom();
            String prenomNvUser = nouvelUtilisateur.getPrenom();

            String[] args = {
                prenomNvUser, nomNvUser
            };

            String subject = "nvcomptesociete.subject";
            email.setArguments(args);

            email.setEmailConfig(EmailConfig.NOUVEAU_COMPTE_SOCIETE);

            List<String> list = new ArrayList<String>();

            for (EbUser u: adminsUser) {
                String lang = u.getLanguage() != null && !u.getLanguage().trim().isEmpty() ?
                    ";" + u.getLanguage() :
                    ";fr";
                list.add(u.getEmail() + lang);
            }

            ;
            emailHtmlSender.sendListEmail(list, email, "2-email-nouveau-compte-societe", context, subject);
        }

    }

    @Override
    public void sendEmailInviterMyCommunity(List<EbUser> destinataires, EbUser expediteur, String link) {
        String nomExpiditeur = expediteur.getNom();
        String prenomExpiditeur = expediteur.getPrenom();
        String[] args = {
            prenomExpiditeur, nomExpiditeur
        };
        String subject = "invitationautreutilisateur.subject";

        List<Map<String, Object>> listMapEmail = new ArrayList<Map<String, Object>>();

        for (EbUser destinataire: destinataires) {
            Email email = new Email();
            email.setTo(destinataire.getEmail());
            email.setEmailConfig(EmailConfig.EMAIL_INVITATION);

            Context params = new Context();
            params.setVariable("url", url);
            params.setVariable("prenomExpediteur", prenomExpiditeur);
            params.setVariable("nomExpediteur", nomExpiditeur);
            params.setVariable("nomDestinataire", destinataire.getNom());

            Map<String, Object> mapEmail = new HashMap<String, Object>();
            mapEmail.put("context", params);
            mapEmail.put("templateName", "8-email-invitation");
            mapEmail.put("email", email);
            mapEmail.put("subject", subject);

            listMapEmail.add(mapEmail);
        }

        emailHtmlSender.sendListEmail(listMapEmail);
    }

    @Override
    public void sendEmailBackup(EbUser destinataire, EbUser expediteur) {
        Context params = new Context();

        params.setVariable("nomDestinataire", destinataire.getNom());
        params.setVariable("nomExpediteur", expediteur.getNom());

        String subject = "userbackup.subject";

        Email email = new Email();
        email.setTo(expediteur.getEmail());
        email.setSubject(subject);
        email.setEmailConfig(EmailConfig.BACKUP);
        emailHtmlSender.send(email, EmailConfig.BACKUP.getTemplateName(), params);
    }

    @Override
    public void sendEmailForCreatePassword(EbUser user) {

        // TODO Auto-generated method stub
        if (user != null) {
            Context context = new Context();

            context.setVariable("url", url);

            context.setVariable("nom", user.getNom());

            context.setVariable("resetToken", user.getResetToken());

            String[] args = {
                ""
            };
            String subject = "nvuserbyadmin.subject";

            Email email = new Email();
            email.setArguments(args);
            email.setSubject(subject);
            email.setEmailConfig(EmailConfig.EMAIL_ADD_USER_BY_ADMIN);

            email.setTo(user.getEmail());

            emailHtmlSender.send(email, "11-email-add-user-by-admin", context);
        }

    }

    private Object concatUrlWithEntityNum(String urlDetailModule, String entityNum) {
        return url.concat(urlDetailModule).concat(entityNum);
    }

    private void generateModuleUrl(EbChat commentaire, EbDemande ebDemande, Context params) {
        String urlPricing = "app/pricing/details/";
        String urlTransFile = "app/transport-management/details/";
        String urlTT = "app/track-trace/details/";

        if (Enumeration.Module.PRICING.getCode().equals(commentaire.getModule())) {
            params.setVariable("url", concatUrlWithEntityNum(urlPricing, ebDemande.getEbDemandeNum().toString()));
        }
        else if ((Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(commentaire.getModule()))) {
            params.setVariable("url", concatUrlWithEntityNum(urlTransFile, ebDemande.getEbDemandeNum().toString()));
        }
        else if (Enumeration.Module.TRACK.getCode().equals(commentaire.getModule())) params
            .setVariable("url", concatUrlWithEntityNum(urlTT, commentaire.getIdFiche().toString()));

    }

    @Override
    public void sendMailAddComment(String username, EbChat commentaire, EbDemande ebDemande) {
        // TODO Auto-generated method stub
        Context params = new Context();
        Email email = new Email();
        String refTran = ebDemande.getRefTransport();
        String[] args = {
            refTran, username
        };
        String subject = "ajoutcommentaire.subject";
        email.setArguments(args);
        email.setSubject(subject);

        /*
         * Test sur le type de la demande pour le url de redirection vers la
         * demande
         */
        Integer typeDemande = ebDemande.getxEcTypeDemande();

        Integer numEntity = ebDemande.getEbDemandeNum();

        generateModuleUrl(commentaire, ebDemande, params);

        params.setVariable("ebdemande", ebDemande);
        params.setVariable("username", username);
        params.setVariable("commentaire", commentaire);
        params.setVariable("commentaireDate", commentaire.getDateCreation());
        params.setVariable("commentaireText", commentaire.getText());

        EbUser connectedUser = connectedUserService.getCurrentUser();
        Map<String, Object> mapParams = new HashMap<String, Object>();
        mapParams.put("ebUserNum", connectedUser.getEbUserNum());

        if (commentaire.getModule().equals(Enumeration.Module.TRACK.getCode())) {
            mapParams.put("ebTtTracingNum", commentaire.getIdFiche());
            numEntity = commentaire.getIdFiche();
        }

        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                connectedUser.getEbCompagnie().getEbCompagnieNum(),
                Enumeration.EmailConfig.NOUVEAU_COMMENTAIRE.getCode(),
                commentaire.getModule(),
                ebDemande.getEbDemandeNum(),
                mapParams);

        email.setEmailConfig(Enumeration.EmailConfig.NOUVEAU_COMMENTAIRE);

		List<Integer> usersIds = (List<Integer>) res.get("ids");
		List<String> listDestinataireEmails = (List<String>) res.get("emails");

		if (Enumeration.StatutDemande.PND.getCode().equals(ebDemande.getxEcStatut())) {
			List<Integer> transporteursIds = ebDemande.getExEbDemandeTransporteurs().stream()
					.map(ExEbDemandeTransporteur::getxTransporteur).map(EbUser::getEbUserNum)
					.collect(Collectors.toList());

			usersIds = usersIds.stream().filter(userId -> !transporteursIds.contains(userId))
					.collect(Collectors.toList());

			List<String> transporteurEmails = ebDemande.getExEbDemandeTransporteurs().stream()
					.map(ExEbDemandeTransporteur::getxTransporteur)
					.map(user -> user.getEmail() + ";" + user.getLanguage()).collect(Collectors.toList());

			listDestinataireEmails = listDestinataireEmails.stream().filter(mail -> !transporteurEmails.contains(mail))
					.collect(Collectors.toList());
		}

		notificationService.generateAlert(email, usersIds, numEntity, commentaire.getModule());

        notificationService
            .generateMessages(ebDemande, commentaire, commentaire.getModule(), usersIds);

        String connectedUserMail = connectedUser.getEmail();
        params.setVariable("demandeId", ebDemande.getEbDemandeNum());

        LOGGER.info("before sending emails");
        LOGGER
            .info(
                "destinateur : " +
                    connectedUser.getNomPrenom() + " - idUserConnected : " + connectedUser.getEbUserNum() +
                    " - commentaire :  " + commentaire.getText());
        LOGGER
            .info(
                "timestamp : " +
                    LocalDate.now() + " - idDemande : " + ebDemande.getEbDemandeNum() + " - idChat : " +
                    commentaire.getEbChatNum());
        
		emailHtmlSender
            .sendListEmail(
                listDestinataireEmails,
                email,
                "commentaire/22-email-comment-dossier",
                params,
                subject,
                connectedUserMail);
        LOGGER.info("after sending emails");
        LOGGER
            .info(
                "destinateur : " +
                    connectedUser.getNomPrenom() + " - idUserConnected : " + connectedUser.getEbUserNum() +
                    " - commentaire :  " + commentaire.getText());
        LOGGER
            .info(
                "timestamp : " +
                    LocalDate.now() + " - idDemande : " + ebDemande.getEbDemandeNum() + " - idChat : " +
                    commentaire.getEbChatNum());
    }

    @Override
    public void sendEmailCompteRefuserParAdminSocieter(EbUser nouvelUtilisateur, EbUser adminSocieter) {

        // TODO Auto-generated method stub
        if (nouvelUtilisateur != null) {
            Context context = new Context();

            context.setVariable("compagnieName", adminSocieter.getEbCompagnie().getNom());

            context.setVariable("etablissementName", adminSocieter.getEbEtablissement().getNom());

            String[] args = {
                ""
            };

            String subject = "refususer.subject";

            Email email = new Email();
            email.setEmailConfig(EmailConfig.REFUS_USER_PAR_ADMIN);
            email.setArguments(args);

            email.setSubject(subject);

            email.setTo(nouvelUtilisateur.getEmail());

            emailHtmlSender.send(email, "100-refus-user-par-admin", context);
        }

    }

    @Override
    public void sendEmailException(ErrorDetails errorDetails) {
        if (exceptionEmails == null || exceptionEmails.isEmpty() || errorDetails == null) return;

        Context context = new Context();

        ObjectMapper oMapper = new ObjectMapper();
        Map<String, Object> mapError = oMapper.convertValue(errorDetails, Map.class);
        String value = null;

        for (String key: mapError.keySet()) {

            if (key != null && mapError.get(key) != null) {

                if (key.contentEquals("dateCreation")) {
                    DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                    value = df.format(mapError.get(key));
                }
                else if (mapError.get(key) instanceof Date) value = ((Date) mapError.get(key)).toString();
                else if (mapError.get(key) instanceof Integer) value = "" + (Integer) mapError.get(key);
                else if (mapError.get(key) instanceof Long) value = "" + (Long) mapError.get(key);
                else value = (String) mapError.get(key);

                context.setVariable(key, value);
            }

        }

        String[] args = {
            errorDetails.getAction()
        };
        String subject = "exception.subject";
        Email email = new Email();
        email.setArguments(args);
        email.setSubject(subject);

        String[] listEmails = exceptionEmails.split(";");

        for (String addrEmail: listEmails) {

            if (addrEmail != null && !addrEmail.isEmpty()) {
                email.setTo(addrEmail);
                emailHtmlSender.send(email, "exception/10000-email-alert-error", context);
            }

        }

    }

    @Override
    public void sendEmailCreationNouvelleSociete(String lang) {
        Email email = new Email();

        String subject = "creationNouvelleSociete.subject";

        email.setEmailConfig(EmailConfig.CREATION_NOUVELLE_SOCIETE_SUR_MYTOWER);

        Context context = new Context();
        context.setVariable("url", url);

        String language = "en";

        if (StringUtils.isNotBlank(lang)) {
            language = lang;
        }

        final String mapLang = language;
        List<String> ctAdminEmails = new ArrayList<>(Arrays.asList(ctAdminsMails.split(";")))
            .stream().map(mail -> mail + ";" + mapLang).collect(Collectors.toList());

        emailHtmlSender
            .sendListEmail(
                ctAdminEmails,
                email,
                "1001-email-creation-nouvelle-societe-sur-myTower",
                context,
                subject,
                null);
    }

    @Override
    public void sendEmailInviteUtiliserMyTower(EbUser user, String emailToSend) {

        if (user != null && emailToSend != null) {
            String compagnieName = "";

            if (user.getEbCompagnie() != null) {
                compagnieName = user.getEbCompagnie().getNom();
            }

            String userFullName = user.getNomPrenom();
            Context context = new Context();
            List<String> mailsCopieCachee = new ArrayList<>();
            mailsCopieCachee.add(mytowerTeamMail);
            context.setVariable("compagnieName", compagnieName);
            context.setVariable("userFullName", userFullName);
            context.setVariable("mytowerTeamMail", mytowerTeamMail);

            String[] args = {
                compagnieName
            };
            String subject = "inviteusemytower.subject";

            Email email = new Email();
            email.setArguments(args);
            email.setEmailConfig(EmailConfig.INVITE_USER_NOT_REGISTRED);
            email.setArguments(args);

            email.setSubject(subject);

            email.setTo(emailToSend);

            email.setEnCopieCachee(mailsCopieCachee);

            emailHtmlSender.send(email, EmailConfig.INVITE_USER_NOT_REGISTRED.getTemplateName(), context);
        }

    }
}
