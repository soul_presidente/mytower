package com.adias.mytowereasy.service.filedownload;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.TrackService;
import com.adias.mytowereasy.service.category.CategoryService;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaTrackTrace;
import org.springframework.util.StringUtils;


@Service
public class FiledownloadTrackServiceImp extends MyTowerService implements FiledownloadTrackService {
    @Autowired
    TrackService tracktraceService;
    @Autowired
    CategoryService categoryService;

    @Autowired
    MessageSource messageSource;

    private static final String PSL_APP = "pslApp_";
    private static final String ESTIMATED = "_estimated";
    private static final String NEGOTIATED = "_negotiated";
    private static final String ACTUAL = "_actual";

    @Override
    public FileDownloadStateAndResult listFileDownLoadTracing(
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        List<EbCategorie> listCategorie,
        SearchCriteria searchCriteria,
        Map<String, String> mapCustomField,
        Map<String, List<Integer>> mapCategoryFields)
        throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();
        Map<String, String> listTypeResult = new LinkedHashMap<String, String>();

        Map<String, Object> listcatAndCustom = categoryService
            .getListCategoryAndCustomField(searchCriteria, connectedUser);

        List<EbCustomField> fields = new ArrayList<EbCustomField>();

        fields = (List<EbCustomField>) listcatAndCustom.get("customField");
        listCategorie = (List<EbCategorie>) listcatAndCustom.get("listCategorie");

        if (!mapCustomField.isEmpty()) searchCriteria.setMapCustomFields(mapCustomField);

        if (!mapCategoryFields.isEmpty()) searchCriteria.setMapCategoryFields(mapCategoryFields);

        ObjectMapper objectMapper = new ObjectMapper();
        SearchCriteriaTrackTrace criteriaTrackTrace = objectMapper
            .convertValue(searchCriteria, SearchCriteriaTrackTrace.class);

        List<EbTtTracing> listEbtTracing = tracktraceService.getListEbtrack(criteriaTrackTrace);

        List<EbFavori> listEbFavori = new ArrayList<EbFavori>();

        Locale locale = connectedUser.getLocaleFromLanguage();

        DateFormat dateFormat = new SimpleDateFormat(connectedUser.getDateFormatFromLocale());

        if (listEbtTracing != null && listEbtTracing.size() > 0 && isPropertyInDtConfig(dtConfig, "btn-favori")) {
            List<Integer> listEbtTracingNum = listEbtTracing
                .stream().map(it -> it.getEbTtTracingNum()).collect(Collectors.toList());
            listEbFavori = ebFavoriRepository
                .findAllByModuleAndUserEbUserNumAndIdObjectIn(
                    searchCriteria.getModule(),
                    connectedUser.getEbUserNum(),
                    listEbtTracingNum);
        }

        for (EbTtTracing ebTtTracing: listEbtTracing) {
            Map<String, String[]> map = new LinkedHashMap<String, String[]>();

            List<EbTypeDocuments> listTypeDocument = ebTtTracing.getxEbDemande().getListTypeDocuments();
            if (isPropertyInDtConfig(dtConfig, "libelleLastPsl")) map.put("libelleLastPsl", new String[]{
                messageSource.getMessage("track_trace.libelle_last_psl", null, locale), ebTtTracing.getLibelleLastPsl()
            });

            if (isPropertyInDtConfig(dtConfig, "dateLastPsl")) {
                map.put("dateLastPsl", new String[]{
                    messageSource.getMessage("track_trace.date_last_psl", null, locale),
                    ebTtTracing.getDateLastPsl() != null ? dateFormat.format(ebTtTracing.getDateLastPsl()) : ""
                });
                listTypeResult.put("dateLastPsl", Date.class.getTypeName());
            }

            if (isPropertyInDtConfig(dtConfig, "datePickupTM")) {
                map.put("datePickupTM", new String[]{
                    messageSource.getMessage("track_trace.estimated_psl_start", null, locale),
                    ebTtTracing.getxEbDemande().getDatePickupTM() != null ?
                        dateFormat.format(ebTtTracing.getxEbDemande().getDatePickupTM()) :
                        ""
                });
                listTypeResult.put("datePickupTM", Date.class.getTypeName());
            }

            if (isPropertyInDtConfig(dtConfig, "finalDelivery")) map.put("finalDelivery", new String[]{
                messageSource.getMessage("track_trace.estimated_psl_end", null, locale),
                ebTtTracing.getxEbDemande().getFinalDelivery() != null ?
                    dateFormat.format(ebTtTracing.getxEbDemande().getFinalDelivery()) :
                    ""
            });

            if (isPropertyInDtConfig(dtConfig, "refTransport")) map.put("refTransport", new String[]{
                messageSource.getMessage("track_trace.transport_ref", null, locale), ebTtTracing.getRefTransport()
            });
            if (isPropertyInDtConfig(dtConfig, "customRef")) map.put("customRef", new String[]{
                messageSource.getMessage("pricing_booking.customer_reference", null, locale),
                ebTtTracing.getxEbDemande().getCustomerReference()
            });

            if (isPropertyInDtConfig(dtConfig, "listTypeDocuments")) {

                if (!CollectionUtils.isEmpty(listTypeDocument)) {
                    String codeDoc = getCodeDoc(listTypeDocument);

                    map.put("listTypeDocuments", new String[]{
                        messageSource.getMessage("pricing_booking.statut_document", null, locale), codeDoc
                    });
                }
                else {
                    map.put("listTypeDocuments", new String[]{
                        messageSource.getMessage("pricing_booking.statut_document", null, locale), ""
                    });
                }

            }

            if (isPropertyInDtConfig(dtConfig, "customerReference")) if (ebTtTracing.getxEbDemande()
                != null) map.put("customerReference", new String[]{
                    messageSource.getMessage("track_trace.unit_reference", null, locale),
                    ebTtTracing.getCustomerReference()
            });
            else map.put("customerReference", new String[]{
                messageSource.getMessage("track_trace.unit_reference", null, locale), null
            });
            if (isPropertyInDtConfig(dtConfig, "packingList")) map.put("packingList", new String[]{
                messageSource.getMessage("type_unit.packing_list", null, locale), ebTtTracing.getPackingList()
            });
            if (isPropertyInDtConfig(dtConfig, "xEcIncotermLibelle")) map.put("xEcIncotermLibelle", new String[]{
                messageSource.getMessage("track_trace.incoterms", null, locale), ebTtTracing.getxEcIncotermLibelle()
            });

            if (isPropertyInDtConfig(dtConfig, "numAwbBol")) map.put("numAwbBol", new String[]{
                messageSource.getMessage("pricing_booking.num_awb_bol", null, locale),
                ebTtTracing.getxEbDemande().getNumAwbBol()
            });

            if (isPropertyInDtConfig(dtConfig, "xEcCountryOrigin")) map.put("xEcCountryOrigin", new String[]{
                messageSource.getMessage("track_trace.origi_of_country", null, locale),
                ebTtTracing.getxEcCountryOrigin().getLibelle()
            });
            if (isPropertyInDtConfig(dtConfig, "companyOrigin")) map.put("companyOrigin", new String[]{
                messageSource.getMessage("pricing_booking.company_origin", null, locale), ebTtTracing.getCompanyOrigin()
            });

            if (isPropertyInDtConfig(dtConfig, "libelleOriginCity")) map.put("libelleOriginCity", new String[]{
                messageSource.getMessage("track_trace.orig_city", null, locale), ebTtTracing.getLibelleOriginCity()
            });

            if (isPropertyInDtConfig(dtConfig, "xEcCountryDestination")) map.put("xEcCountryDestination", new String[]{
                messageSource.getMessage("track_trace.dest_country", null, locale),
                ebTtTracing.getxEcCountryDestination().getLibelle()
            });

            if (isPropertyInDtConfig(dtConfig, "libelleDestCity")) map.put("libelleDestCity", new String[]{
                messageSource.getMessage("track_trace.dest_city", null, locale), ebTtTracing.getLibelleDestCity()
            });

            if (isPropertyInDtConfig(dtConfig, "xEcModeTransport")) map.put("xEcModeTransport", new String[]{
                messageSource.getMessage("track_trace.mode_of_transport", null, locale),
                ebTtTracing.getxEcModeTransport() != null ?
                    ModeTransport.getLibelleByCode(ebTtTracing.getxEcModeTransport()) :
                    null
            });

            if (isPropertyInDtConfig(dtConfig, "datePickup")) {
                map.put("datePickup", new String[]{
                    messageSource.getMessage("track_trace.estimated_psl_start", null, locale),
                    ebTtTracing.getDatePickup() != null ? dateFormat.format(ebTtTracing.getDatePickup()) : null
                });
                listTypeResult.put("datePickup", Date.class.getTypeName());
            }

            if (isPropertyInDtConfig(
                dtConfig,
                "nomEtablissementTransporteur")) map.put("nomEtablissementTransporteur", new String[]{
                    messageSource.getMessage("track_trace.transporteur", null, locale),
                    ebTtTracing.getNomEtablissementTransporteur() != null ?
                        ebTtTracing.getNomEtablissementTransporteur() :
                        null
            });
            if (isPropertyInDtConfig(dtConfig, "totalWeight")) map.put("totalWeight", new String[]{
                messageSource.getMessage("track_trace.gross_weight", null, locale),
                ebTtTracing.getTotalWeight() != null ? ebTtTracing.getTotalWeight().toString() : null
            });

            if (isPropertyInDtConfig(dtConfig, "dateModification")) {
                map.put("dateModification", new String[]{
                    messageSource.getMessage("track_trace.last_update", null, locale),
                    ebTtTracing.getDateModification() != null ?
                        dateFormat.format(ebTtTracing.getDateModification()) :
                        null
                });
                listTypeResult.put("dateModification", Date.class.getName());
            }

            if (isPropertyInDtConfig(dtConfig, "late")) {

                if (ebTtTracing.getLate() != null) {
                    map.put("late", new String[]{
                        messageSource.getMessage("track_trace.late", null, locale), ebTtTracing.getLate().getLibelle()
                    });
                }
                else map.put("late", new String[]{
                    messageSource.getMessage("track_trace.late", null, locale), "No"
                });

            }

            if (isPropertyInDtConfig(dtConfig, "libellePslCourant")) map.put("libellePslCourant", new String[]{
                messageSource.getMessage("track_trace.last_milestone_updated", null, locale),
                ebTtTracing.getLibellePslCourant() != null ?
                    ebTtTracing.getLibellePslCourant() :
                    StatutDemande.WPU.getLibelle()
            });
            if (isPropertyInDtConfig(dtConfig, "xEbUserCt")) map.put("xEbUserCt", new String[]{
                "Name of requestor", ebTtTracing.getxEbUserCt().getNomPrenom()
            });
            if (isPropertyInDtConfig(dtConfig, "xEbChargeur")) map.put("xEbChargeur", new String[]{
                messageSource.getMessage("pricing_booking.requestor", null, locale),
                ebTtTracing.getxEbChargeur().getNomPrenom()
            });
            if (isPropertyInDtConfig(
                dtConfig,
                "transportStatusPerUnit")) map.put("transportStatusPerUnit", new String[]{
                    messageSource.getMessage("track_trace.transport_status_per_unit", null, locale),
                    ebTtTracing.getTransportStatusPerUnit()
            });

            if (isPropertyInDtConfig(dtConfig, "listEbFlagDTO")) {
                String listFlagsLibelle = "";
                List<Integer> listFlagsId = new ArrayList<>();

                if (ebTtTracing.getListFlag() != null && ebTtTracing.getListFlag().length() > 0) {
                    listFlagsId = Arrays
                        .asList(ebTtTracing.getListFlag().split(",")).stream().map(Integer::valueOf)
                        .collect(Collectors.toList());
                    List<EbFlag> listEbFlag = ebFlagRepository.findAllById(listFlagsId);

                    for (EbFlag ebFlag: listEbFlag) {
                        listFlagsLibelle += ebFlag.getName() + ", ";
                    }

                    if (listFlagsLibelle.length()
                        > 0) listFlagsLibelle = listFlagsLibelle.substring(0, listFlagsLibelle.length() - 2);
                }

                map.put("listEbFlagDTO", new String[]{
                    messageSource.getMessage("pricing_booking.flag", null, locale), listFlagsLibelle
                });
            }

            if (isPropertyInDtConfig(dtConfig, "dateCreation")) {
                map.put("dateCreation", new String[]{
                    messageSource.getMessage("track_trace.request_date", null, locale),
                    ebTtTracing.getxEbDemande().getDateCreation() != null ?
                        dateFormat.format(ebTtTracing.getxEbDemande().getDateCreation()) :
                        null
                });
                listTypeResult.put("dateCreation", Date.class.getTypeName());
            }

            if (listCategorie != null && listCategorie.size() > 0) {

                for (EbCategorie cat: listCategorie) {
                    if (isPropertyInDtConfig(dtConfig, "category" + cat.getEbCategorieNum())) map
                        .put("category" + cat.getEbCategorieNum(), new String[]{
                            cat.getLibelle(), null
                        });
                }

            }

            if (fields != null && fields.size() > 0) {

                for (EbCustomField customFields: fields) {
                    List<CustomFields> field = new ArrayList<CustomFields>();

                    field = gson.fromJson(customFields.getFields(), new TypeToken<ArrayList<CustomFields>>() {
                    }.getType());

                    if (field != null && field.size() > 0) {

                        for (CustomFields fie: field) {
                            if (isPropertyInDtConfig(dtConfig, fie.getName())) map.put(fie.getName(), new String[]{
                                fie.getLabel(), null
                            });
                        }

                    }

                    if (field != null &&
                        ebTtTracing.getListCustomsFields() != null &&
                        (ebTtTracing
                            .getxEbChargeur().getEbCompagnie().getEbCompagnieNum()
                            .equals(customFields.getxEbCompagnie()))) {

                        for (CustomFields fie: field) {

                            if (isPropertyInDtConfig(dtConfig, fie.getName())) {

                                for (CustomFields demField: ebTtTracing.getListCustomsFields()) {

                                    if (demField.getName().contentEquals(fie.getName())) {
                                        map.put(fie.getName(), new String[]{
                                            fie.getLabel(), demField.getValue()
                                        });
                                        break;
                                    }

                                }

                            }

                        }

                    }

                }

            }

            if (listCategorie != null && ebTtTracing.getxEbDemande().getListCategories() != null) {

                for (EbCategorie categorie: listCategorie) {

                    if (isPropertyInDtConfig(dtConfig, "category" + categorie.getEbCategorieNum())) {

                        for (EbCategorie demCategorie: ebTtTracing.getxEbDemande().getListCategories()) {

                            if (demCategorie.getLabels() != null &&
                                demCategorie.getEbCategorieNum().equals(categorie.getEbCategorieNum())) {
                                String label = null;

                                for (EbLabel ebLabel: demCategorie.getLabels()) {
                                    label = ebLabel.getLibelle();
                                    break;
                                }

                                map.put("category" + categorie.getEbCategorieNum(), new String[]{
                                    categorie.getLibelle(), label
                                });
                                break;
                            }

                        }

                    }

                }

            }

            List<EbTTPslApp> listEbPslApp = ebPslAppRepository
                .findAllByxEbTrackTraceEbTtTracingNum(ebTtTracing.getEbTtTracingNum());

            Integer orderPsl = 100;

            for (EbTTPslApp ebPslApp: listEbPslApp) {
               String pslLibelle =  ebPslApp.getCompanyPsl().getLibelle();
               pslLibelle = pslLibelle.substring(0, 1).toUpperCase() + pslLibelle.substring(1).toLowerCase();
                map.put(PSL_APP + ebPslApp.getCodeAlpha() + NEGOTIATED, new String[]{
                        pslLibelle + " Negotiated Date",
                    ebPslApp.getDateNegotiation() != null ? dateFormat.format(ebPslApp.getDateNegotiation()) : ""
                });
                listTypeResult.put(PSL_APP + ebPslApp.getCodeAlpha() + NEGOTIATED, Date.class.getTypeName());

                map.put(PSL_APP + ebPslApp.getCodeAlpha() + ESTIMATED, new String[]{
                        pslLibelle + " Estimated Date",
                    ebPslApp.getDateEstimee() != null ? dateFormat.format(ebPslApp.getDateEstimee()) : ""
                });
                listTypeResult.put(PSL_APP + ebPslApp.getCodeAlpha() + ESTIMATED, Date.class.getTypeName());

                map.put(PSL_APP + ebPslApp.getCodeAlpha() + ACTUAL, new String[]{
                        pslLibelle + " Actual Date",
                    ebPslApp.getDateActuelle() != null ? dateFormat.format(ebPslApp.getDateActuelle()) : ""
                });
                listTypeResult.put(PSL_APP + ebPslApp.getCodeAlpha() + ACTUAL, Date.class.getTypeName());

                if (!CollectionUtils.isEmpty(listresult)) {
                    // ajouter tous les PSL dans la premiere Map de listResult afin d'avoir toutes les collones dans
                    // l'extraction
                    addMissingPslToListResult(listresult, ebPslApp);
                }

                if (!isPropertyInDtConfig(dtConfig, PSL_APP + ebPslApp.getCodeAlpha() + NEGOTIATED)) {
                    Map<String, String> mapPsl = new HashMap<String, String>();
                    mapPsl.put("title", ebPslApp.getCompanyPsl().getLibelle() + " Negotiated Date");
                    mapPsl.put("name", PSL_APP + ebPslApp.getCodeAlpha() + NEGOTIATED);
                    mapPsl.put("order", "" + (orderPsl++));
                    dtConfig.add(mapPsl);
                }

                if (!isPropertyInDtConfig(dtConfig, PSL_APP + ebPslApp.getCodeAlpha() + ESTIMATED)) {
                    Map<String, String> mapPsl = new HashMap<String, String>();
                    mapPsl.put("title", ebPslApp.getCompanyPsl().getLibelle() + " Estimated Date");
                    mapPsl.put("name", PSL_APP + ebPslApp.getCodeAlpha() + ESTIMATED);
                    mapPsl.put("order", "" + (orderPsl++));
                    dtConfig.add(mapPsl);
                }

                if (!isPropertyInDtConfig(dtConfig, PSL_APP + ebPslApp.getCodeAlpha() + ACTUAL)) {
                    Map<String, String> mapPsl = new HashMap<String, String>();
                    mapPsl.put("title", ebPslApp.getCompanyPsl().getLibelle() + " Actual Date");
                    mapPsl.put("name", PSL_APP + ebPslApp.getCodeAlpha() + ACTUAL);
                    mapPsl.put("order", "" + (orderPsl++));
                    dtConfig.add(mapPsl);
                }

            }

            if (listEbFavori.size() > 0 && isPropertyInDtConfig(dtConfig, "btn-favori")) {
                Optional<EbFavori> opt = listEbFavori
                    .stream().filter(it -> it.getIdObject().equals(ebTtTracing.getEbTtTracingNum())).findFirst();
                if (opt.isPresent()) map.put("btn-favori", new String[]{
                    "Favori", "Oui"
                });
                else map.put("btn-favori", new String[]{
                    "Favori", "Non"
                });
            }
            else if (isPropertyInDtConfig(dtConfig, "btn-favori")) {
                map.put("btn-favori", new String[]{
                    "Favori", "Non"
                });
            }

            if (isPropertyInDtConfig(dtConfig, "carrierUniqRefNum")) map.put("carrierUniqRefNum", new String[]{
                messageSource.getMessage("request_overview.carrier_uniq_ref_num", null, locale),
                ebTtTracing.getxEbDemande().getCarrierUniqRefNum()
            });

            if (isPropertyInDtConfig(dtConfig, "transportConfirmation")) {
                String transportConfirmation = "";

                if (ebTtTracing.getxEbDemande().getAskForTransportResponsibility() != null &&
                    ebTtTracing.getxEbDemande().getAskForTransportResponsibility() &&
                    ebTtTracing.getxEbDemande().getProvideTransport() != null &&
                    ebTtTracing.getxEbDemande().getProvideTransport()) {
                    transportConfirmation = "Confirmed";
                }
                else if (ebTtTracing.getxEbDemande().getAskForTransportResponsibility() != null &&
                    ebTtTracing.getxEbDemande().getAskForTransportResponsibility()) {
                        transportConfirmation = "Waiting for confirmation";
                    }

                map.put("transportConfirmation", new String[]{
                    messageSource.getMessage("pricing_booking.transport_confirmation", null, locale),
                    transportConfirmation
                });
            }

            // adding data
            listresult.add(map);
        }

        resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
        resultAndState.setListResult(listresult);
        resultAndState.setListTypeResult(listTypeResult);
        return resultAndState;
    }

    private void addMissingPslToListResult(List<Map<String, String[]>> listresult, EbTTPslApp ebPslApp) {
        if (!listresult.get(0).containsKey(PSL_APP + ebPslApp.getCodeAlpha() + NEGOTIATED)) {
            listresult.get(0).put(PSL_APP + ebPslApp.getCodeAlpha() + NEGOTIATED, new String[]{
                ebPslApp.getCompanyPsl().getLibelle() +
                    " " + TtEnumeration.NatureDateEvent.DATE_NEGOTIATED.getLibelle(),
                ""

            });
        }
        if (!listresult.get(0).containsKey(PSL_APP + ebPslApp.getCodeAlpha() + ESTIMATED)) {
            listresult.get(0).put(PSL_APP + ebPslApp.getCodeAlpha() + ESTIMATED, new String[]{
                ebPslApp.getCompanyPsl().getLibelle() + " " + TtEnumeration.NatureDateEvent.DATE_ESTIMATED.getLibelle(),
                ""
            });
        }
        if (!listresult.get(0).containsKey(PSL_APP + ebPslApp.getCodeAlpha() + ACTUAL)) {
            listresult.get(0).put(PSL_APP + ebPslApp.getCodeAlpha() + ACTUAL, new String[]{
                ebPslApp.getCompanyPsl().getLibelle() + " " + TtEnumeration.NatureDateEvent.DATE_ACTUAL.getLibelle(), ""
            });
        }
    }

    private String getCodeDoc(List<EbTypeDocuments> listTypeDocument) {
        String codeDoc = "";

        for (EbTypeDocuments cat: listTypeDocument) {
            codeDoc += cat.getNom() + "(" + (cat.getNbrDoc() != null ? cat.getNbrDoc() : 0) + ")";
            codeDoc += " \r\n";
        }

        return codeDoc;
    }
}
