/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.List;
import java.util.Set;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface CompagnieService {
    public Set<EbEtablissement> selectListEbEtablissementEbCompagnie(SearchCriteria criterias);

    public EbCompagnie selectEbCompagnie(SearchCriteria criterias);

    public EbEtablissement updateEbEtablissement(EbEtablissement ebEtablissement);

    List<EbCompagnie> selectListContactCompagnie(SearchCriteria criteria);

    List<EbCompagnie> selectListCompagnie(SearchCriteria criteria);

    List<Integer> findContactsCompagniesNums(Integer ebUserNum);

    List<Integer> findContactsCompagniesNums(Integer ebUserNum, Boolean addConnectedUserCompany);
}
