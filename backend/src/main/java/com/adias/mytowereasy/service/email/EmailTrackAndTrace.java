/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service.email;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.model.tt.EbTtEvent;


public interface EmailTrackAndTrace {
    void sendEmailDeviationTT(EbTtEvent deviation, EbUser user, EbTtEvent ttEvent);

    void sendEmailUpdatePsl(EbTTPslApp ebPslApp, EbUser user, EbTtEvent ttEvent, String libellePsl);
}
