package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.CustomFields;


public interface CustomFieldService {

    public List<CustomFields> getCustomFields(Integer ebUserNum);
}
