package com.adias.mytowereasy.service.filedownload;

import java.util.List;
import java.util.Map;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface FiledownloadPricingService {
    public FileDownloadStateAndResult listFileDownLoadPricing(
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        List<EbCategorie> listCategorie,
        SearchCriteria searchCriteria)
        throws Exception;

    List<Map<String, String[]>> extractPricing();

    List<Map<String, String[]>> extractTM();

    List<Map<String, String[]>> extractIncoherenceStatutTTPSL();

    List<Map<String, String[]>> extractIncoherenceStatutTMTT();

    List<Map<String, String[]>> extractSavings();

    List<Map<String, String[]>> extractGlobalData(String key);
}
