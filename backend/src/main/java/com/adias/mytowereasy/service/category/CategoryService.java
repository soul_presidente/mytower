package com.adias.mytowereasy.service.category;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class CategoryService extends MyTowerService {
    public Map<String, Object> getListCategoryAndCustomField(SearchCriteria searchCriteria, EbUser connectedUser) {
        if (connectedUser == null) connectedUser = connectedUserService.getAvailableCurrentUser(searchCriteria);

        List<EbCategorie> listCategorie = new ArrayList<EbCategorie>();
        Map<String, Object> result = new HashMap<String, Object>();
        List<EbCustomField> fields = new ArrayList<EbCustomField>();
        EbCustomField customField = new EbCustomField();
        boolean isNotDefaultControlTower = connectedUser.isControlTower() &&
            Enumeration.Role.ROLE_CHARGEUR.getCode().equals(connectedUser.getEbCompagnie().getCompagnieRole());

        if (connectedUser.isChargeur() || isNotDefaultControlTower) {

            if (searchCriteria.getEbCompagnieNum() == null) {
                searchCriteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
            }

            if (searchCriteria.getEbEtablissementNum() == null) {
                searchCriteria.setEbEtablissementNum(connectedUser.getEbEtablissement().getEbEtablissementNum());
            }

            listCategorie = ebCategorieRepository
                .findDistinctByEtablissementEbCompagnieEbCompagnieNum(searchCriteria.getEbCompagnieNum());
            customField = ebCustomFieldRepository.findFirstByXEbCompagnie(searchCriteria.getEbCompagnieNum());

            if (customField != null && customField.getFields() != null) {
                fields.add(customField);
            }

        }

        if (connectedUser.isControlTower()) {
            List<EbEtablissement> listetablissement = new ArrayList<EbEtablissement>();
            List<EbCompagnie> listebcompagnie = new ArrayList<EbCompagnie>();
            List<EbCategorie> listCategorieCopy = new ArrayList<EbCategorie>();
            listetablissement = ebDemandeRepository.selectEbEtablissementByEbUserCtNum(connectedUser.getEbUserNum());
            listebcompagnie = ebDemandeRepository.selectEbCompagnyByEbUserCtNum(connectedUser.getEbUserNum());

            for (EbEtablissement etablissement: listetablissement) {
                listCategorieCopy = new ArrayList<EbCategorie>();
                listCategorieCopy = ebCategorieRepository
                    .findDistinctByEtablissementEbEtablissementNum(etablissement.getEbEtablissementNum());
                if (listCategorie == null) listCategorie = listCategorieCopy;

                else listCategorie.addAll(listCategorieCopy);
            }

            for (EbCompagnie compagnie: listebcompagnie) {
                customField = new EbCustomField();
                if (compagnie.getEbCompagnieNum() != null) customField = ebCustomFieldRepository
                    .findFirstByXEbCompagnie(compagnie.getEbCompagnieNum());

                if (compagnie != null && compagnie.getNom() != null) {
                    String compagnieName = compagnie.getNom();

                    if (customField != null) {
                        customField.setNomCompagnie(compagnieName);
                        fields.add(customField);
                    }

                }

            }

        }

        if (!fields.isEmpty()) result.put("customField", fields);

        if (!listCategorie.isEmpty()) result.put("listCategorie", listCategorie);

        return result;
    }
}
