/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import com.adias.mytowereasy.airbus.dao.DaoEbTtCompanyPsl;
import com.adias.mytowereasy.airbus.repository.EbQrGroupePropositionRepository;
import com.adias.mytowereasy.airbus.repository.EbQrGroupeRepository;
import com.adias.mytowereasy.airbus.repository.EbTtCompanyPslRepository;
import com.adias.mytowereasy.cptm.repository.EbCptmProcedureRepository;
import com.adias.mytowereasy.cptm.repository.EbCptmRegimeTemporaireRepository;
import com.adias.mytowereasy.cptm.service.ComptaMatiereService;
import com.adias.mytowereasy.cronjob.tt.repository.EdiMapPslRepository;
import com.adias.mytowereasy.cronjob.tt.service.GenerateEventByEdiService;
import com.adias.mytowereasy.dao.*;
import com.adias.mytowereasy.dao.custom.DaoCustom;
import com.adias.mytowereasy.dao.custom.DaoDeclaration;
import com.adias.mytowereasy.delivery.repository.EbLivraisonLineRepository;
import com.adias.mytowereasy.dock.dao.DaoDock;
import com.adias.mytowereasy.dock.dao.DaoEntrepot;
import com.adias.mytowereasy.dock.repository.*;
import com.adias.mytowereasy.repository.*;
import com.adias.mytowereasy.repository.custom.EbDeclarationRepository;
import com.adias.mytowereasy.repository.custom.EbFormFieldCompagnieRepository;
import com.adias.mytowereasy.rsch.dao.DaoReceiptScheduling;
import com.adias.mytowereasy.rsch.service.ReceiptSchedulingService;
import com.adias.mytowereasy.service.email.EmailDocumentService;
import com.adias.mytowereasy.service.email.EmailService;
import com.adias.mytowereasy.service.email.EmailServicePricingBooking;
import com.adias.mytowereasy.service.email.EmailTrackAndTrace;
import com.adias.mytowereasy.service.filedownload.*;
import com.adias.mytowereasy.service.fileupload.FileUploadService;
import com.adias.mytowereasy.template.service.TemplateDocumentsService;
import com.adias.mytowereasy.trpl.dao.DaoPlGrilleTransport;
import com.adias.mytowereasy.trpl.dao.DaoPlTranche;
import com.adias.mytowereasy.trpl.dao.DaoPlZone;
import com.adias.mytowereasy.trpl.dao.DaoPlanTransport;
import com.adias.mytowereasy.trpl.repository.*;


public class MyTowerService {
    @Autowired
    InscriptionService inscriptionService;
    @Autowired
    protected EbUserRepository ebUserRepository;

    @Autowired
    protected EbCompagnieRepository ebCompagnieRepository;

    @Autowired
    protected EbEtablissementRepository ebEtablissementRepository;

    @Autowired
    protected EbWeekTemplateDayRepository ebWeekTemplateDayRepository;

    @Autowired
    protected EbWeekTemplateHourRepository ebWeekTemplateHourRepository;

    @Autowired
    protected EbAdresseRepository ebAdresseRepository;

    @Autowired
    protected EbCustomFieldRepository ebCustomFieldRepository;

    @Autowired
    EbUserTagRepository ebUserTagRepository;

    @Autowired
    protected EbCostCenterRepository ebCostCenterRepository;

    @Autowired
    EbDelegationRepository ebDelegationRepository;

    @Autowired
    protected EbRelationRepository ebRelationRepository;

    @Autowired
    EbSavedFormRepository ebSavedFormRepository;

    @Autowired
    ExEbUserModuleRepository exEbUserModuleRepository;

    @Autowired
    EbQmIncidentLineRepository ebQmIncidentLineRepository;

    @Autowired
    protected EbDkOpeningWeekRepository ebDkOpeningWeekRepository;

    @Autowired
    protected EbDkOpeningPeriodRepository ebDkOpeningPeriodRepository;

    @Autowired
    protected EbDockRepository ebDockRepository;

    @Autowired
    protected EbFlagRepository ebFlagRepository;

    @Autowired
    EbBoutonActionRepository ebBoutonActionRepository;

    @Autowired
    protected EbEntrepotRepository ebEntrepotRepository;

    @Autowired
    protected ReceiptSchedulingService receiptSchedulingService;

    @Autowired
    protected ComptaMatiereService comptaMatiereService;

    @Autowired
    protected EbRangeHoursRepository ebRangeHoursRepository;

    @Autowired
    protected EbPlTrancheRepository ebTrancheRepository;

    @Autowired
    protected EbPlanTransportRepository ebPlanTransportRepository;
    @Autowired
    EbPlanTransportNewRepository ebPlanTransportNewRepository;

    @Autowired
    protected EbPlZoneRepository ebZoneRepository;

    @Autowired
    protected EbRschUnitScheduleRepository ebRschUnitScheduleRepository;

    @Autowired
    protected EbTtCompanyPslRepository ebTtCompanyPslRepository;

    @Autowired
    EcFuseauxHoraireRepository ecFuseauxHoraireRepository;

    @Autowired
    EcMimetypeRepository ecMimeTypeRepository;
    @Autowired
    protected EbCptmRegimeTemporaireRepository ebCptmRegimeTemporaireRepository;
    @Autowired
    protected EbLivraisonLineRepository ebLivraisonLineRepository;

    @Autowired
    protected EbCptmProcedureRepository ebCptmProcedureRepository;

    @Autowired
    protected EbTypeGoodsRepository ebTypeGoodsRepository;

    @Autowired
    protected EbControlRuleRepository ebControlRuleRepository;

    @Autowired
    protected EbControlRuleOperationRepository ebControlRuleOperationRepository;

    @Autowired
    protected TypeGoodsService typeGoodsService;

    @Autowired
    DaoUser daoUser;

    @Autowired
    DaoGroupUser daoGroupUser;

    @Autowired
    DaoTypeDocuments daoTypeDocuments;

    @Autowired
    protected DaoEtablissement daoEtablissement;

    @Autowired
    protected DaoCurrency daoCurrency;

    @Autowired
    protected DaoPricing daoPricing;

    @Autowired
    protected DaoDock daoDock;

    @Autowired
    protected DaoEntrepot doaEntrepot;

    @Autowired
    DaoFlag daoFlag;

    @Autowired
    BoutonActionDao daoBoutonAction;

    @Autowired
    DaoSavedForm daoSavedForm;

    @Autowired
    Environment environment;

    @Autowired
    EmailService emailService;

    @Autowired
    EmailServicePricingBooking emailServicePricingBooking;

    @Autowired
    EmailDocumentService emailDocumentService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    protected EbCategorieRepository ebCategorieRepository;

    @Autowired
    protected EbLabelRepository ebLabelRepository;

    @Autowired
    EbDestinationCountryRepository ebDestinationCountryRepository;

    @Autowired
    DaoComplianceMatrix daoComplianceMatrix;

    @Autowired
    protected DaoCompagnie daoCompagnie;

    @Autowired
    EbCombinaisonRepository ebCombinaisonRepository;

    @Autowired
    protected EbPartyRepository ebPartyRepository;

    @Autowired
    protected EbDemandeRepository ebDemandeRepository;

    @Autowired
    protected UnitService unitService;

    @Autowired
    EbQmIncidentRepository ebIncidentRepository;

    @Autowired
    protected ExEbDemandeTransporteurRepository exEbDemandeTransporteurRepository;

    @Autowired
    protected EbPlGrilleTransportRepository ebPlGrilleTransportRepository;

    @Autowired
    protected EbPlCostItemRepository ebPlCostItemRepository;

    @Autowired
    protected EbPlLigneCostItemTrancheRepository ebPlLigneCostItemTrancheRepository;

    @Autowired
    protected EbDkOpeningDayRepository ebDkOpeningDayRepository;

    @Autowired
    protected EbTtSchemaPslRepository ebTtSchemaPslRepository;

    @Autowired
    protected EbMarchandiseRepository ebMarchandiseRepository;

    @Autowired
    EbDemandeFichiersJointRepositorty ebDemandeFichiersJointRepositorty;

    @Autowired
    EbDatatableStateRepository ebDatatableStateRepository;

    @Autowired
    protected DaoFileDownloader daoFileDownloader;

    @Autowired
    EbInvoiceRepository ebInvoiceRepository;

    @Autowired
    protected ConnectedUserService connectedUserService;

    @Autowired
    protected UserService userService;

    @Autowired
    DaoInvoice daoInvoice;

    @Autowired
    protected EbTrackTraceRepository ebTrackTraceRepository;

    @Autowired
    protected EbPslAppRepository ebPslAppRepository;

    @Autowired
    TrackService ebTrackService;

    @Autowired
    DaoTrack daotrack;

    @Autowired
    DaoSchemaPsl daoShemaPsl;

    @Autowired
    DaoQualityManagement daoQualityManagement;

    @Autowired
    DaoCustom daoCustom;

    @Autowired
    DaoDeclaration daoDeclaration;

    @Autowired
    protected DaoPlanTransport daoPlanTransport;

    @Autowired
    protected DaoPlZone daoPlZone;

    @Autowired
    protected DaoPlTranche daoPlTranche;

    @Autowired
    protected DaoPlGrilleTransport daoPlGrilleTransport;

    @Autowired
    protected DaoReceiptScheduling daoReceiptScheduling;

    @Autowired
    protected DaoEbTtCompanyPsl daoEbTtCompanyPsl;

    @Autowired
    protected EbCompagnieCurrencyRepository ebCompagnieCurrencyRepository;

    @Autowired
    protected EcCurrencyRepository ecCurrencyRepository;

    @Autowired
    protected EbTtCategorieDeviationRepository ebTtCategorieDeviationRepository;

    @Autowired
    protected EcCountryRepository ecCountryRepository;

    @Autowired
    EbTtEventRepository ebTtEventRepository;

    @Autowired
    EbCostCategorieRepository ebCostCategorieRepository;

    @Autowired
    EbCostRepository ebCostRepository;

    @Autowired
    EbQmRootCauseRepository ebQmRootCauseRepository;

    @Autowired
    EbQmIncidentRepository ebQmIncidentRepository;

    @Autowired
    EbQmQualificationRepository ebQmQualificationRepository;

    @Autowired
    EbQmClosingReasonRepository ebQmClosingReasonRepository;
    @Autowired
    EbWeekTemplateRepository ebWeekTemplateRepository;

    @Autowired
    EmailTrackAndTrace emailTrackAndTrace;

    @Autowired
    EbEmailRepository ebEmailRepository;

    @Autowired
    EbCustomsInformationRepository ebCustomsInformationRepository;

    @Autowired
    protected EbFormFieldCompagnieRepository ebDouaneRepository;

    @Autowired
    protected EbDeclarationRepository ebDeclarationRepository;

    @Autowired
    EbFormFieldRepository ebFormFieldRepository;

    @Autowired
    EbFormViewRepository ebFormViewRepository;

    @Autowired
    EbEmailHistoriqueRepository ebEmailHistoriqueRepository;

    @Autowired
    EbTypeDocumentsRepository ebTypeDocumentsRepository;

    @Autowired
    protected EbFavoriRepository ebFavoriRepository;

    @Autowired
    EbChatRepository ebChatRepository;

    @Autowired
    EbQmDeviationRepository ebQmDeviationRepository;

    @Autowired
    EbCustomDeclarationRepository ebCustomDeclarationRepository;

    @Autowired
    protected EbTemplateDocumentsRepository ebTemplateDocumentsRepository;

    @Autowired
    protected DocTemplateService docTemplateService;

    @Autowired
    protected DaoTemplateDocuments daoTemplateDocuments;

    @Autowired
    protected EbTypeRequestRepository ebTypeRequestRepository;

    @Autowired
    DaoTypeRequest daoTypeRequest;

    @Autowired
    EbQrGroupeRepository ebQrGroupeRepository;

    @Autowired
    EbQrGroupePropositionRepository ebQrGroupePropositionRepository;

    @Autowired
    protected EbTypeTransportRepository ebTypeTransportRepository;

    @Autowired
    EbWebHooksRepository ebWebHooksRepository;

    @Autowired
    QmDeviationService ebQmDeviationService;

    @Autowired
    QualityManagementService qualityManagementService;

    @Autowired
    protected MessageSource messageSource;

    @Autowired
    DaoCostCenter daoCostCenter;

    @Autowired
    protected EbTypeFluxRepository ebTypeFluxRepository;

    @Autowired
    protected DaoTypeFlux daoEbTypeFlux;

    @Autowired
    protected EbTypeFluxService ebTypeFluxService;

    @Autowired
    protected EbIncotermRepository ebIncotermRepository;

    @Autowired
    protected EdiMapPslRepository ediMapPslRepository;

    @Autowired
    protected EbTypeUnitRepository ebTypeUnitRepository;

    @Autowired
    protected DaoIncoterm daoIncoterm;

    @Autowired
    protected DaoMappingCodePsl daoMappingCodePsl;

    @Autowired
    protected ServiceConfiguration config;

    @Autowired
    protected ComplianceMatrixService complianceMatrixService;
    @Autowired
    protected FiledownloadPricingService filedonwloadpricing;
    @Autowired
    protected FiledownloadAdditionalCostService filedownloadAdditionalCostService;
    @Autowired
    protected FiledownloadTrackService filedownloadTrackService;
    @Autowired
    protected FiledownloadFreightAuditService filedownloadFreightAuditService;
    @Autowired
    protected FiledownloadOrderManagementService filedownloadOrderManagementService;
    @Autowired
    protected FiledownloadDeliveryManagementService filedownloadDeliveryManagementService;
    @Autowired
    protected FiledownloadQualityManagementService filedownloadQualityManagementService;
    @Autowired
    protected FiledownloadCustomDeclarationService filedownloadCustomDeclarationService;
    @Autowired
    protected FiledownloadComplianceMatrixService filedownloadComplianceMatrixService;
    @Autowired
    protected FiledownloadUserManagementService filedownloadUserManagementService;
    @Autowired
    protected FiledownloadComptaMatiereService filedownloadComptaMatiereService;
    @Autowired
    protected FiledownlaodEtablissementService filedownlaodEtablissementService;
    @Autowired
    protected FiledownloadRootCauseService filedownloadRootCauseService;
    @Autowired
    CostItemService costItemService;

    @Autowired
    protected ListStatiqueService serviceListStatique;

    @Autowired
    PricingService pricingService;

    @Autowired
    protected DaoCategorie daoCategorie;

    @Autowired
    protected GenerateEventByEdiService generateEventByEdiService;

    @Autowired

    QrConsolidationService qrConsolidationService;

    @Autowired

    DaoAdresse daoAdresse;

    @Autowired
    protected FiledownloadLoadPlanningService filedownloadLoadPlanningService;

    @Autowired
    protected DaoControlRule daoControlRule;

    @Autowired
    protected TemplateDocumentsService templateDocumentsService;

    @Autowired
    protected FileUploadService fileUploadService;

    @Autowired
    TypeDocumentService typeDocumentService;

    @Autowired
    protected DaoAdditionalCost daoAdditionalCost;

    @Autowired
    protected EbAdditionalCostRepository ebAdditionalCostRepository;

    protected Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();

    protected boolean isPropertyInDtConfig(List<Map<String, String>> dtConfig, String property) {
        boolean exists = false;

        if (dtConfig != null && dtConfig.size() > 0) {

            for (Map<String, String> config: dtConfig) {

                if (config.get("name") != null && !config.get("name").isEmpty()
                    && config.get("name").equals(property)) {
                    exists = true;
                    break;
                }

                if (config.get("title") != null && !config.get("title").isEmpty()
                    && config.get("title").equals(property)) {
                    exists = true;
                    break;
                }

            }

        }
        else return true;

        return exists;
    }

    protected String serializationJson(Object obj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(Include.NON_NULL);
        return mapper.writeValueAsString(obj);
    }
}
