package com.adias.mytowereasy.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.KibanaUser;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.security.TokenAuthenticationService;
import com.adias.mytowereasy.util.KibanaResponse;


@Service
public class KibanaAuthorisationServiceImpl implements KibanaAuthorisationService {
    @Value("${mytowereasy.kibana.username}")
    private String kibanaUsername;
    @Value("${mytowereasy.kibana.password}")
    private String kibanaPwd;
    @Value("${mytower.front.kibana-url}")
    private String kibana_url;
    @Autowired
    EbUserRepository userRepository;

    @Override
    public KibanaUser getUserFromToken(HttpServletRequest request) {
        String token = request.getParameter("kibana_token");
        String userid = request.getParameter("userid");
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        KibanaUser user = null;

        System.err
            .println(
                "kibanaUsername:" + kibanaUsername + " kibanaPwd:" + kibanaPwd + " token:" + token + " userid:" + userid
                    + " username:" + username + " password:" + password);

        // token auth
        if (token != null && !token.isEmpty()) {
            user = TokenAuthenticationService.getKibanaUserFromToken(token);
        }

        // session id validation
        if (userid != null && !userid.isEmpty()) {
            EbUser ebUser = userRepository.findOneByEbUserNum(Integer.parseInt(userid));

            if (ebUser != null) {
                user = ebUser.toKibanaUser();
            }

        }

        // username/password auth
        if (username != null && password != null && !username.isEmpty() && !password.isEmpty()) {

            if (username.trim().equals(kibanaUsername) && password.trim().equals(kibanaPwd)) {
                user = new KibanaUser(-1, username);
            }

        }

        return user != null ? user : new KibanaUser();
    }

    @Override
    public KibanaResponse getKibanaToken(String access_token, long expirationTime) {
        System.err.println("kibana_url: " + kibana_url);
        return new KibanaResponse(
            TokenAuthenticationService.generateKibanaToken(access_token, expirationTime),
            kibana_url);
    }
}
