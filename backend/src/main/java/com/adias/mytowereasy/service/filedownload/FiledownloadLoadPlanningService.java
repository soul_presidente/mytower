package com.adias.mytowereasy.service.filedownload;

import java.util.List;
import java.util.Map;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface FiledownloadLoadPlanningService {
    public FileDownloadStateAndResult listFileDownloadLoadPlanning(
        SearchCriteria criteria,
        EbUser connectedUser,
        List<Map<String, String>> dtConfig,
        List<EbCategorie> listCategorie,
        Map<String, String> mapCustomField,
        Map<String, List<Integer>> mapCategoryFields)
        throws Exception;
}
