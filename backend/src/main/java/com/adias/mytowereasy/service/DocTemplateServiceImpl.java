package com.adias.mytowereasy.service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.docx4j.Docx4jProperties;
import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.util.document.DocumentUtils;


@Service
public class DocTemplateServiceImpl implements DocTemplateService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DocTemplateServiceImpl.class);

    @Autowired
    ServiceConfiguration serviceConfiguration;

    /**
     * Apply a model to a Microsoft Word (.docx) template file and convert to
     * pdf<br/>
     * <br/>
     * docx-stamper is the template engine used
     * <b><a href="https://github.com/thombergs/docx-stamper">see templating
     * documentation</a></b>
     * 
     * @param templateFile
     *            Stream to existing .docx file that will be used
     *            for template
     * @param model
     *            The object that will be used as POJO to the template
     * @param pdfFile
     *            File pointing to the path where the .pdf file will be created
     * @throws IOException
     * @throws Docx4JException
     */
    @Override
    public void generateTemplatePdf(InputStream templateFile, Object model, Path pdfFile, EbUser connectedUser)
        throws IOException,
        Docx4JException {
        Path tempFile = Files.createTempFile(null, ".pdf");

        try {
            applyDocx4jProperties();
            DocumentUtils.applyDocxTemplate(templateFile, model, tempFile, connectedUser);
            DocumentUtils.convertDocxToPdf(tempFile, pdfFile);
        } finally {
            Files.deleteIfExists(tempFile);
        }

    }

    /**
     * Apply a model to a Microsoft Word (.docx) template file<br/>
     * <br/>
     * docx-stamper is the template engine used
     * <b><a href="https://github.com/thombergs/docx-stamper">see templating
     * documentation</a></b>
     * 
     * @param templateFile
     *            Stream to existing .docx file that will be used
     *            for template
     * @param model
     *            The object that will be used as POJO to the template
     * @param docxFile
     *            File pointing to the path where the .docx file will be created
     * @throws IOException
     */
    @Override
    public void generateTemplateDocx(InputStream templateFile, Object model, Path docxFile, EbUser connectedUser)
        throws IOException {
        applyDocx4jProperties();
        DocumentUtils.applyDocxTemplate(templateFile, model, docxFile, connectedUser);
    }

    /**
     * Set the tmpFontDir variable to docx4j
     * Required on some environment (like nexter)
     */
    private void applyDocx4jProperties() {
        final String tmpFolderPath = serviceConfiguration.getTempFolderPath();

        if (StringUtils.isBlank(tmpFolderPath)) {
            LOGGER.error("No temp folder path found ! Template export can crash");
        }

        final String newPath = FilenameUtils.concat(tmpFolderPath, "docx4jTemp");

        File newPathFile = new File(newPath);
        newPathFile.mkdirs();
        Docx4jProperties
            .setProperty("docx4j.openpackaging.parts.WordprocessingML.ObfuscatedFontPart.tmpFontDir", newPath);

        LOGGER.info("Template export will manage his temporary files on the following path\n-> {}", newPath);
    }
}
