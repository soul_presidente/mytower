package com.adias.mytowereasy.service.filedownload;

import java.util.List;
import java.util.Map;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface FiledownloadFreightAuditService {
    public FileDownloadStateAndResult listFileDownLoadFreightAudit(
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        List<EbCategorie> listCategorie,
        SearchCriteria searchCriteria,
        Map<String, String> mapCustomFields,
        Map<String, List<Integer>> mapCategoryFields)
        throws Exception;
}
