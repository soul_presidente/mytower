package com.adias.mytowereasy.service.filedownload;

import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface FiledownloadUserManagementService {
    public FileDownloadStateAndResult listFileDownLoadUserManagement(SearchCriteria searchCriteria) throws Exception;
}
