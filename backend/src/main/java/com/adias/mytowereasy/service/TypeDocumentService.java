package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.dto.EbTypeDocumentsDTO;
import com.adias.mytowereasy.model.EbTypeDocuments;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface TypeDocumentService {
    public EbTypeDocuments saveTypeDocument(EbTypeDocuments ebTypeDocuments);

    public EbTypeDocuments updateTypeDocument(EbTypeDocuments ebTypeDocuments);

    public void updateActivated(SearchCriteria criteria);

    void deleteTypeDocument(Integer ebTypeDocuments);

    public Long countListTypeDocument(SearchCriteria criteria);

    List<EbTypeDocuments> getListTypeDoc(SearchCriteria criteria) throws Exception;

    List<EbTypeDocuments> getTypeDocsByEbTypeDocumentsModule(Integer moduleNum, EbUser user) throws Exception;

    List<EbTypeDocuments> getListTypeDocumentFinal(Integer moduleNum, Integer ebDemandeNum, Integer ebCompagnieNum)
        throws Exception;

    List<EbTypeDocumentsDTO> convertEntitiesToDto(List<EbTypeDocuments> entities);

    void generateTypeOfDocumentByCompanyNum(List<Integer> listEbCompagnieNum) throws Exception;

    List<EbTypeDocuments> getInvoiceListTypeDocument(Integer module, Integer ebCompagnieNum, Integer ebInvoiceNum)
        throws Exception;
}
