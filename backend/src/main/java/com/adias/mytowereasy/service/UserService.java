/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import com.adias.mytowereasy.dto.ExUserModuleDTO;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDelegation;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbRelation;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EbUserGroup;
import com.adias.mytowereasy.model.TransfertUserObject;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface UserService {
    Set<ExUserModuleDTO> getUserAccessRights(Integer ebUserNum);

    public EbUser getEbUserByEmail(String email);

    public EbUser selectEbUser(SearchCriteria criterias);

    public EbUser getEbUserByEbUserNum(Integer ebUserNum);

    public List<EbUser> getAllUser();

    public List<EbUser> getByProfile(Integer ebUserProfileNum);

    public EbUser getEbUserByResetToken(String resetToken);

    public void resetPassword(EbUser ebUser);

    public EbUser updateEbUser(EbUser ebUser);

    public EbUser updateStatusEbUser(EbUser ebUser);

    public Integer updateAdminAndSuperAdminUser(EbUser ebUser);

    public EbUser changeUserPassword(EbUser ebUser);

    public EbUser updateUserDetails(EbUser ebUser);

    public boolean matchUserPassword(SearchCriteria criteria);

    public EbEtablissement updateEbEtablissement(EbEtablissement ebEtablissement);

    public void updateEbUserPasswordAndResetToken(EbUser ebUser);

    public EbUser getEbUserByActiveToken(String resetToken);

    public Boolean activateAccount(String token);

    public EbDelegation createDelegation(EbDelegation ebDelegation);

    public Integer deleteDelegation(Integer ebDelegationNum);

    public EbDelegation getDelegation(SearchCriteria criteria);

    public List<EbUser> selectListContact(SearchCriteria criteria);

    public List<EbUser> selectListTransporteur(SearchCriteria criteria);

    public List<EbUser> selectListBroker(SearchCriteria criteria);

    public EbRelation updateEbRelation(EbRelation ebRelation);

    public List<EbRelation> selectListEbEtablissementOfEbRelation(SearchCriteria criterias);

    public Boolean accepterContactRequest(EbRelation ebRelation);

    public Boolean ignorerContactRequest(EbRelation ebRelation);

    public Boolean annulerContactRequest(EbRelation ebRelation);

    public List<EbUser> selectListEbUser(SearchCriteria criterias);

    public long selectCountListUser(SearchCriteria criteria);

    public EbRelation sendInvitation(EbRelation ebRelation);

    public Boolean updatePasswordEbUser(EbUser ebUser);

    public BigInteger selectCountListContact(SearchCriteria criteria);

    public BigInteger selectCountListEbEtablissementOfEbRelation(SearchCriteria criterias);

    Map sendInvitationByMail(EbRelation ebRelation, Integer serviceToIgnore, Integer ebUserNum);

    List<EbUser> listTransporteurControlTower(SearchCriteria criteria);

    public EbUser deleteUser(EbUser user);

    public List<EbCompagnie> selectListOneListEbCompagnieByOneContact(SearchCriteria criteria);

    public EbUser saveConfigEmail(EbUser user);

    public List<EbUser> getAllTransporteur();

    Boolean setRelationsByEmail(EbUser ebUser);

    Object updateEbUserLanguage(EbUser ebUser);

    public List<EbUserGroup> getListUserGroup(EbUser ebUser);

    public Boolean updateUserGroup(SearchCriteria criteria, Integer ebUserNum);

    public boolean canEditUser(Integer ebUserNum);

    public boolean canEditProfil(Integer ebUserProfileNum);

    public String deleteSelectedUsers(List<EbUser> users);

    public List<String> checkAbilityToDeleteUers(List<EbUser> users);

    public void updateEtablissement(TransfertUserObject transfertUserObjet);

    public String selectListCarrier(SearchCriteria criteria) throws JsonProcessingException;

    public void updateMaskEtablissement(TransfertUserObject transfertUserObjet)
        throws JsonMappingException,
        JsonProcessingException;

    List<EbCompagnie> selectListContactCompagnie(SearchCriteria criteria);

    List<EbCompagnie> selectListContactCompagnieOfRelation(SearchCriteria criteria);

		boolean isCompanyInMyContactByListEtablissementIds(Integer idComponyGuest, List<Integer> idsEtablissement);
}
