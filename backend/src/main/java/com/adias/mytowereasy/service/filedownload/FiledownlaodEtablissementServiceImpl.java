package com.adias.mytowereasy.service.filedownload;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dao.DaoEtablissement;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class FiledownlaodEtablissementServiceImpl extends MyTowerService implements FiledownlaodEtablissementService {
    @Autowired
    DaoEtablissement daoEtablissement;

    @Override
    public FileDownloadStateAndResult listFileDownLoadEtablissementInformation(
        SearchCriteria etablissementCriteria,
        List<Map<String, String>> dtConfig)
        throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();

        etablissementCriteria.setShowGlobals(true);
        etablissementCriteria.setSize(null);

        List<EbEtablissement> listEbEtablissements = daoEtablissement.selectListEbEtablissement(etablissementCriteria);

        for (EbEtablissement etablissement: listEbEtablissements) {
            Map<String, String[]> map = new LinkedHashMap<String, String[]>();
            if (isPropertyInDtConfig(dtConfig, "nic")) map.put("nic", new String[]{
                "NIC/ID", etablissement.getNic() != null ? etablissement.getNic().toString() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "nom")) map.put("nom", new String[]{
                "Etablissement", etablissement.getNom() != null ? etablissement.getNom().toString() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "typeEtablissement")) map.put("typeEtablissement", new String[]{
                "TypeEtablissement",
                etablissement.getTypeEtablissement() != null ? etablissement.getTypeEtablissement().toString() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "ecCountry")) map.put("ecCountry", new String[]{
                "Country",
                etablissement.getEcCountry() != null ? etablissement.getEcCountry().getCodeLibelle().toString() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "city")) map.put("city", new String[]{
                "City", etablissement.getCity() != null ? etablissement.getCity().toString() : ""
            });

            if (isPropertyInDtConfig(dtConfig, "canShowPrice")) map.put("canShowPrice", new String[]{
                "Can Show Price",
                etablissement.getCanShowPrice() != null ? etablissement.getCanShowPrice().toString() : ""
            });
            listresult.add(map);
        }

        resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
        resultAndState.setListResult(listresult);
        return resultAndState;
    }
}
