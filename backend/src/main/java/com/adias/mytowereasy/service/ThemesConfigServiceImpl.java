package com.adias.mytowereasy.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbUserThemes;
import com.adias.mytowereasy.repository.EbThemesConfigRepository;


@Service
public class ThemesConfigServiceImpl implements ThemesConfigService {
    @Autowired
    EbThemesConfigRepository ebThemesConfigRepository;

    @Override
    public boolean saveThemesConfig(EbUserThemes themesConfig) {
        EbUserThemes newTheme = getCompagnieTheme(themesConfig.getxEbCompagnie().getEbCompagnieNum());
        if (newTheme == null) newTheme = new EbUserThemes();
        newTheme.setActiveTheme(themesConfig.getActiveTheme());
        newTheme.setDateMaj(new Date());
        newTheme.setxEbUser(themesConfig.getxEbUser());
        newTheme.setxEbCompagnie(themesConfig.getxEbCompagnie());
        ebThemesConfigRepository.save(newTheme);
        return true;
    }

    @Override
    public EbUserThemes getCompagnieTheme(Integer ebCompagnieNum) {
        return ebThemesConfigRepository.getCompagnieTheme(ebCompagnieNum);
    }
}
