package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbCompagnieCurrency;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.repository.EcCurrencyRepository;
import com.adias.mytowereasy.util.DateUtils;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class CurrencyServiceImpl extends MyTowerService implements CurrencyService {
    @Autowired
    CurrencyService currencyService;

    @Autowired
    EcCurrencyRepository ecCurrencyRepository;

		public EbCompagnieCurrency saveEbCompagnyCurrency(EbCompagnieCurrency ebCompagnieCurrency)
		{
			return validCurrency(ebCompagnieCurrency) ?
				ebCompagnieCurrencyRepository.save(ebCompagnieCurrency) :
				null;
    }

		public boolean validCurrency(EbCompagnieCurrency ebCompagnieCurrency)
		{
			Date today = DateUtils.setDate_To_The_Start_Of_The_Day_Month(new Date(), false);
			Date dateDebut = ebCompagnieCurrency.getDateDebut();
			Date dateFin = ebCompagnieCurrency.getDateFin();
			ebCompagnieCurrency.setIsUsed(false);
			if (hasValidDatesRage(ebCompagnieCurrency, today, dateDebut, dateFin))
			{
				List<EbCompagnieCurrency> listEbCompanyCurrency = daoCurrency
					.getListNonComplaiantCompagniecurrency(ebCompagnieCurrency);

				return listEbCompanyCurrency == null || listEbCompanyCurrency.isEmpty();
			}

			return false;
		}

		private boolean hasValidDatesRage(
			EbCompagnieCurrency ebCompagnieCurrency,
			Date today,
			Date dateDebut,
			Date dateFin
		)
		{
			return ebCompagnieCurrency.getEbCompagnie() != null &&
				((dateFin != null ? dateDebut.before(dateFin) : true) &&
					(today.before(DateUtils.setDate_To_The_End_Of_The_Day_And_Month(dateDebut, false))) &&
					(ebCompagnieCurrency.getEcCurrency().getEcCurrencyNum() != ebCompagnieCurrency
						.getXecCurrencyCible()
						.getEcCurrencyNum()));
		}

    public Boolean updateEbCompanyCurrency(EbCompagnieCurrency ebCompagnieCurrency) {
        Date today = new Date();
        EbCompagnieCurrency oldEbCompagnieCurrency = ebCompagnieCurrencyRepository
            .findEbCompagnieCurrencyByEbCompagnieCurrencyNum(ebCompagnieCurrency.getEbCompagnieCurrencyNum());
        Boolean isOldEbCompanyCurrencyValid = verifyEbCompagnieCurrencyValidty(
            ebCompagnieCurrency.getEbCompagnieCurrencyNum());
        Date oldDateDebut = oldEbCompagnieCurrency.getDateDebut();
        Date newDateDebut = ebCompagnieCurrency.getDateDebut();
        Date newDateFin = ebCompagnieCurrency.getDateFin();

        if (isExchangeRateCanBeUpdated(ebCompagnieCurrency, oldEbCompagnieCurrency, isOldEbCompanyCurrencyValid)) {
            ebCompagnieCurrencyRepository.save(ebCompagnieCurrency);
            return true;
        }

        if (today.before(oldDateDebut) && today.before(newDateDebut) && newDateDebut.before(newDateFin)) {
            ebCompagnieCurrencyRepository.save(ebCompagnieCurrency);
            return true;
        }

        return false;
    }

    public Boolean isExchangeRateCanBeUpdated(
        EbCompagnieCurrency ebCompagnieCurrency,
        EbCompagnieCurrency oldEbCompanyCurrency,
        Boolean isExchangeRateValid) {
        Date newDateDebut = ebCompagnieCurrency.getDateDebut();
        Date newDateFin = ebCompagnieCurrency.getDateFin();
        Date oldDateDebut = oldEbCompanyCurrency.getDateDebut();
        Date today = new Date();
        long newDateFinInMills = getDatePart(newDateFin);
        long todayInMills = getDatePart(today);

        if (ebCompagnieCurrency.getEbCompagnie() != null && oldEbCompanyCurrency != null && isExchangeRateValid
            && (newDateDebut.getTime() == oldDateDebut.getTime())
            && (today.before(newDateFin) || newDateFinInMills == todayInMills) && (ebCompagnieCurrency
                .getEcCurrency().getEcCurrencyNum() != ebCompagnieCurrency.getXecCurrencyCible().getEcCurrencyNum())) {
            Boolean isTheSameCompGuest = oldEbCompanyCurrency
                .getEbCompagnieGuest().getEbCompagnieNum()
                .equals(ebCompagnieCurrency.getEbCompagnieGuest().getEbCompagnieNum());
            Boolean isTheSameEcCurrency = oldEbCompanyCurrency
                .getEcCurrency().getEcCurrencyNum().equals(ebCompagnieCurrency.getEcCurrency().getEcCurrencyNum());
            Boolean isTheSameCurrencyCible = oldEbCompanyCurrency
                .getXecCurrencyCible().getEcCurrencyNum()
                .equals(ebCompagnieCurrency.getXecCurrencyCible().getEcCurrencyNum());
            Boolean isTheSameExchangeRate = oldEbCompanyCurrency
                .getEuroExchangeRate().equals(ebCompagnieCurrency.getEuroExchangeRate());

            if (isTheSameCompGuest && isTheSameCurrencyCible && isTheSameEcCurrency && isTheSameExchangeRate) {
                return true;
            }

        }

        return false;
    }

    public long getDatePart(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTimeInMillis();
    }

		public Boolean verifyEbCompagnieCurrencyValidty(Integer ebCompanyCurrencyNum)
		{
			Date today = new Date();
			EbCompagnieCurrency ebCompagnieCurrency = ebCompagnieCurrencyRepository
				.findEbCompagnieCurrencyByEbCompagnieCurrencyNum(ebCompanyCurrencyNum);

			if (ebCompagnieCurrency != null)
			{

				if (
					(ebCompagnieCurrency.getDateFin() != null ?
						ebCompagnieCurrency.getDateDebut().before(ebCompagnieCurrency.getDateFin()) :
						true) &&
						today
							.before(
								DateUtils
									.setDate_To_The_End_Of_The_Day_And_Month(
										ebCompagnieCurrency.getDateDebut(),
										false
									)
							)
				)
				{
					return true;
				}

			}

			return false;
		}

    @Override
    public Boolean deleteExEtablissementCurrency(EbCompagnieCurrency compagnieCurrency) {

        if (exEbDemandeTransporteurRepository
            .countByxEbCompagnieCurrency_ebCompagnieCurrencyNum(compagnieCurrency.getEbCompagnieCurrencyNum())
            .equals(new Long(0))) {
            ebCompagnieCurrencyRepository.delete(compagnieCurrency);
            return true;
        }
        else return false;

    }

    @Override
    public List<EbCompagnieCurrency> selectListExEtablissementCurrencyByEtablissementNum(SearchCriteria criteria) {
        return daoCurrency.selectListEbCompagnieCurrency(criteria);
    }

    @Override
    public Long getListEbCompagnieCurrencyCount(SearchCriteria criteria) {
        return daoCurrency.getListEbCompagnieCurrencyCount(criteria);
    }

    @Override
    public List<EbCompagnieCurrency>
        selectListEbCompagnieCurrencyByEbCompagnieAndEbCompagnieGuestNum(SearchCriteria criteria) {
        Integer numcompagnie = ebEtablissementRepository
            .selectNumCompagnieByEtablissement(criteria.getEbEtablissementNum());
        criteria.setEbCompagnieNum(numcompagnie);
        return daoCurrency.selectListEbCompagnieCurrencyAndEbCompagnieGuest(criteria);
    }

    @Override
    public List<EcCurrency> selectLisCurrencyCompagnieAndStatique(SearchCriteria criteria) {
        List<EbCompagnieCurrency> ListCompagnieCurrency = daoCurrency.selectListEbCompagnieCurrency(criteria);

        List<EcCurrency> listCurrencyStatique = ecCurrencyRepository.findAllByOrderByCodeAsc();

        List<EcCurrency> listEccurencyfinal = new ArrayList<EcCurrency>();

        if (ListCompagnieCurrency.size() > 0) {

            for (EbCompagnieCurrency compagnieCurrency: ListCompagnieCurrency) {

                for (EcCurrency curstatique: listCurrencyStatique) {

                    if (compagnieCurrency
                        .getXecCurrencyCible().getEcCurrencyNum().equals(curstatique.getEcCurrencyNum())) {
                        listEccurencyfinal.add(curstatique);
                        break;
                    }

                }

            }

            for (EcCurrency curstatique: listCurrencyStatique) {

                if (!listEccurencyfinal.contains(curstatique)) {
                    listEccurencyfinal.add(curstatique);
                }

            }

        }
        else {
            listEccurencyfinal = listCurrencyStatique;
        }

        return listEccurencyfinal;
    }

    @Override
    public List<EbCompagnieCurrency> getListCompanieCurrencyBySetting(SearchCriteria criteria) {
        return currencyService.selectListExEtablissementCurrencyByEtablissementNum(criteria);
    }

    @Override
    public EcCurrency getCurrencyByCode(String code) {
        return ecCurrencyRepository.findByCode(code);
    }

    @Override
    public EbCompagnieCurrency getEbCompagnieCurrency(SearchCriteria criteria) {
        List<EbCompagnieCurrency> list = daoCurrency.selectListEbCompagnieCurrency(criteria);
        EbCompagnieCurrency ebCompagnieCurrency = new EbCompagnieCurrency();

        for (EbCompagnieCurrency cc: list) {

            if (doExchangeRateMatchCondition(criteria, cc)) {
                ebCompagnieCurrency = cc;
                break;
            }

        }

        return ebCompagnieCurrency;
    }

    private boolean doExchangeRateMatchCondition(SearchCriteria criteria, EbCompagnieCurrency cc) {
        return cc.getEcCurrency().getEcCurrencyNum().equals(criteria.getCurrencyCostItem())
            && cc.getXecCurrencyCible().getEcCurrencyNum().equals(criteria.getCurrencyCibleNum())
            && cc.getEbCompagnie().getEbCompagnieNum().equals(criteria.getEbCompagnieNum())
            && cc.getEbCompagnieGuest().getEbCompagnieNum().equals(criteria.getEbCompagnieGuestNum());
    }
}
