/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.repository.EbCategorieRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class ConnectedUserService {
    private static final Logger log = LoggerFactory.getLogger(ConnectedUserService.class);

    @Autowired
    EbUserRepository ebUserRepository;

    @Autowired
    EbCategorieRepository ebCategorieRepository;

    public EbUser getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object principal = auth != null ? auth.getPrincipal() : null;
        EbUser user = null;

        if (principal instanceof String) {
            log.error("Connected user needed ! Please add AuthToken into the current request");
        }
        else if (principal != null) {
            user = (EbUser) principal;
        }
        else {
        }

        return user;
    }

    public EbUser getCurrentUserFromDB() {
        EbUser currentUserFromDB = null;
        EbUser currentUser = getCurrentUser();

        if (currentUser != null) {
            currentUserFromDB = ebUserRepository.findOneByEbUserNum(currentUser.getEbUserNum());
        }

        return currentUserFromDB;
    }

    public EbUser getCurrentUserFromDBWithCategories(Integer... pEbUserNum) {
        EbUser connectedUser = null;
        EbUser currentUser = getCurrentUser();
        Integer ebUserNum = currentUser != null ?
            currentUser.getEbUserNum() :
            pEbUserNum != null && pEbUserNum.length > 0 ? pEbUserNum[0] : null;

        if (ebUserNum != null) {
            connectedUser = ebUserRepository.findOneByEbUserNum(ebUserNum);

            if (connectedUser != null && connectedUser.getEbUserNum() != null) {
                List<EbCategorie> listCategories = ebUserRepository
                    .selectListCategoriesByEbUserNum(connectedUser.getEbUserNum()).getListCategories();
                connectedUser.setListCategories(listCategories);
            }

        }

        return connectedUser;
    }

    public EbUser getCurrentUserFromDB(SearchCriteria criteria) {
        Integer ebUserNum = criteria.getConnectedUserNum();
        if (ebUserNum == null) ebUserNum = criteria.getEbUserNum();
        return ebUserRepository.findOneByEbUserNum(ebUserNum);
    }

    public EbUser getCurrentUserFromCriteria(SearchCriteria criteria) {
        Gson gson = new Gson();

        EbUser connectedUser = getCurrentUser();

        if (connectedUser == null && criteria != null || criteria != null && criteria.getConnectedUserNum() != null
            && !criteria.getConnectedUserNum().equals(connectedUser.getEbUserNum())) {
            if (criteria.getUserReturn() != null) connectedUser = criteria.getUserReturn();
            if (connectedUser == null
                && criteria.getUser() != null) connectedUser = gson.fromJson(criteria.getUser(), EbUser.class);

            if (connectedUser == null || criteria != null && criteria.getConnectedUserNum() != null
                && !criteria.getConnectedUserNum().equals(connectedUser.getEbUserNum())) {
                connectedUser = getCurrentUserFromDB(criteria);
                criteria.setUserReturn(connectedUser);
            }

        }

        return connectedUser;
    }

    public EbUser getAvailableCurrentUser(SearchCriteria criteria) {
        EbUser connectedUser = getCurrentUser();

        if (connectedUser == null || criteria != null && criteria.getConnectedUserNum() != null
            && !criteria
                .getConnectedUserNum().equals(connectedUser.getEbUserNum())) connectedUser = getCurrentUserFromCriteria(
                    criteria);

        return connectedUser;
    }

    public EbUser getCurrentUserWithCategories(SearchCriteria criteria) {
        EbUser connectedUser = getAvailableCurrentUser(criteria);

        if (connectedUser != null && connectedUser.getEbUserNum() != null) {
            List<EbCategorie> listCategories = ebUserRepository
                .selectListCategoriesByEbUserNum(connectedUser.getEbUserNum()).getListCategories();

            connectedUser.setListCategories(listCategories);
        }

        return connectedUser;
    }

    public List<EbUser> getAllUsersByIds(List<Integer> usersIds) {
        return ebUserRepository.findAllById(usersIds);
    }
}
