package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.dto.EbTypeRequestDTO;
import com.adias.mytowereasy.model.EbTypeRequest;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface TypeRequestService {
    public List<EbTypeRequest> searchTypeRequest(SearchCriteria criteria);

    public Long countListEbTypeRequest(SearchCriteria criteria);

    List<EbTypeRequestDTO> convertEntitiesToDto(List<EbTypeRequest> entities);

    public EbTypeRequestDTO saveEbTypeRequest(EbTypeRequestDTO typeRequest);

    public void deleteEbTypeRequest(Integer typeRequestNum);

    public void generateTypeOfRequestByCompanyNum(List<Integer> listEbCompagnieNum) throws Exception;

    public List<EbTypeRequest> listWithCommunity();

    public List<EbTypeRequest> searchTypeRequestByReference(String term);

    List<EbTypeRequest> getListServiceLevel(SearchCriteria criteria);
}
