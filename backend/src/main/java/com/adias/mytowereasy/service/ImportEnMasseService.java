package com.adias.mytowereasy.service;

import java.io.IOException;
import java.util.*;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.common.model.ImportResult;
import com.adias.common.model.ImportSheetConfig;
import com.adias.common.model.ImportWorkBookConfig;
import com.adias.common.utils.ImportMasseUtil;
import com.adias.mytowereasy.api.service.TransportApiService;
import com.adias.mytowereasy.api.util.MytowerTransportErrorMsg;
import com.adias.mytowereasy.api.wso.*;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.util.JsonUtils;


@Service
public class ImportEnMasseService {
    @Autowired
    TransportApiService transportService;

    /**
     * Returns a list of errors (an error object contains the type of error, the
     * name of the Sheet
     * where we encounter this error, the row number and the column).
     */

    public ImportResult getResultOfSheetTransformation(XSSFSheet workbook, ImportSheetConfig sheetConfig)
        throws Exception {
        return ImportMasseUtil.transformSheetToObject(workbook, sheetConfig);
    }

    public List<ImportResult> readAllSheetAndTransformToObject(XSSFWorkbook workbook, ImportWorkBookConfig bookConfig)
        throws Exception {
        return ImportMasseUtil.readAllSheetAndTransformToObject(workbook);
    }

    class TransportObjectHelper {
        List<UnitWSO> listUnit = new ArrayList<>();
        List<QuotationWSO> listQuotation = new ArrayList<>();
        List<CategoryWSO> listCategory = new ArrayList<>();
        List<CustomFieldWSO> listCustomField = new ArrayList<>();
    }

    /**
     * retrieve the list of "TransportWSO" objects from all the sheets.
     * this liste is represent a validated objects in the technical sense.
     * 
     * @param `List<ImportResult>`
     *            :
     *            for each sheet, an ImportResult object contains a list of
     *            objects, a list of errors
     *            and an appropriate status.
     * 
     * @throws JsonProcessingException
     */
    public List<TransportWSO>
        getListOfTransportWSO(List<ImportResult> listImportResult) throws JsonProcessingException, IOException {
        List<TransportWSO> allTransportWso = new ArrayList<>();

        Map<Integer, TransportObjectHelper> mapTransportWSOs = new HashMap<>();
        List<TransportWSO> transportWSOs = Arrays
            .asList(
                JsonUtils
                    .getObjectMapperInstance().readValue(
                        JsonUtils.serializeToString(listImportResult.get(0).getListObject()),
                        TransportWSO[].class));

        List<UnitWSO> unitWSOs = Arrays
            .asList(
                JsonUtils
                    .getObjectMapperInstance()
                    .readValue(JsonUtils.serializeToString(listImportResult.get(1).getListObject()), UnitWSO[].class));

        List<QuotationWSO> quotationWso = Arrays
            .asList(
                JsonUtils
                    .getObjectMapperInstance().readValue(
                        JsonUtils.serializeToString(listImportResult.get(2).getListObject()),
                        QuotationWSO[].class));

        List<CostWSO> costWso = Arrays
            .asList(
                JsonUtils
                    .getObjectMapperInstance()
                    .readValue(JsonUtils.serializeToString(listImportResult.get(3).getListObject()), CostWSO[].class));

        List<CategoryWSO> categoryWso = Arrays
            .asList(
                JsonUtils
                    .getObjectMapperInstance().readValue(
                        JsonUtils.serializeToString(listImportResult.get(4).getListObject()),
                        CategoryWSO[].class));

        List<CustomFieldWSO> customFieldWso = Arrays
            .asList(
                JsonUtils
                    .getObjectMapperInstance().readValue(
                        JsonUtils.serializeToString(listImportResult.get(5).getListObject()),
                        CustomFieldWSO[].class));

        /**
         * Set the transport 'itemId' as the value keys in HashMap
         * 'mapTransportWSOs'.
         */

        transportWSOs.forEach(t -> mapTransportWSOs.put(t.getItemId(), null));

        /**
         * The methods called to create "TransportObjectHelper" objects and
         * set its attributes and assign them as 'mapTransportWSOs' values.
         * 
         * @param `mapTransportWSOs`:
         *            HashMap which takes the transport ID "itemId" as key and
         *            an 'TransportObjectHelper'
         *            as value
         */
        setListUnitWso(mapTransportWSOs, unitWSOs);
        setListQuotationWSO(mapTransportWSOs, quotationWso, costWso);
        setListCategoryWso(mapTransportWSOs, categoryWso);
        setListCustomFieldWso(mapTransportWSOs, customFieldWso);

        /**
         * Loop through the list of transport objects
         * and assigns the missing attributes to each object.
         */
        transportWSOs.forEach(transportWso -> {
            TransportObjectHelper trObjListes = mapTransportWSOs.get(transportWso.getItemId());
            transportWso.setUnitList(trObjListes.listUnit);
            transportWso.setQuotationList(trObjListes.listQuotation);
            transportWso.setCategoryList(trObjListes.listCategory);
            transportWso.setCustomFieldList(trObjListes.listCustomField);
            allTransportWso.add(transportWso);
        });

        return allTransportWso;
    }

    /**
     * retrieve the list of "UnitWSO" corresponding to a given "TransportWSO".
     * 
     * @param `List<Object>'
     *            :
     *            contains all "UnitWSO" objects for the "Commandes" sheet.
     */

    private void setListUnitWso(Map<Integer, TransportObjectHelper> mapListTransportObjects, List<UnitWSO> listUnit) {
        listUnit.forEach(unitWso -> {
            Integer transportId = unitWso.getItemId();
            TransportObjectHelper listTrObjects = mapListTransportObjects.get(transportId);
            if (listTrObjects == null) listTrObjects = new TransportObjectHelper();
            listTrObjects.listUnit.add(unitWso);
            mapListTransportObjects.put(transportId, listTrObjects);
        });
    }

    /**
     * retrieve the list of "QuotationWSO" corresponding to a given
     * "TransportWSO".
     * 
     * @param `List<Object>
     *            objects'
     *            :
     *            contains all "QuotationWSO" objects for the "Transporteurs a
     *            interroger" sheet.
     * @param `List<Object>
     *            objects1':
     *            contains all "CostWSO" objects for the "Post couts" sheet.
     */
    private void setListQuotationWSO(
        Map<Integer, TransportObjectHelper> mapListTransportObjects,
        List<QuotationWSO> listQuotation,
        List<CostWSO> ListCostWso) {
        listQuotation.forEach(quotationWso -> {
            Integer transportId = quotationWso.getItemId();
            TransportObjectHelper listTrObjects = mapListTransportObjects.get(transportId);
            if (listTrObjects == null) listTrObjects = new TransportObjectHelper();
            quotationWso.setCostList(setListCost(quotationWso, ListCostWso));
            listTrObjects.listQuotation.add(quotationWso);
            mapListTransportObjects.put(transportId, listTrObjects);
        });
    }

    /**
     * retrieve the list of "CategoryWSO" corresponding to a given
     * "TransportWSO".
     * 
     * @param `List<Object>'
     *            :
     *            contains all "CategoryWSO" objects for the "Categories" sheet.
     */
    private void setListCategoryWso(
        Map<Integer, TransportObjectHelper> mapListTransportObjects,
        List<CategoryWSO> listCategory) {
        listCategory.forEach(categoryWso -> {
            Integer transportId = categoryWso.getItemId();
            TransportObjectHelper listTrObjects = mapListTransportObjects.get(transportId);
            if (listTrObjects == null) listTrObjects = new TransportObjectHelper();

            listTrObjects.listCategory.add(categoryWso);
            mapListTransportObjects.put(transportId, listTrObjects);
        });
    }

    /**
     * retrieve the list of "CustomFieldWSO" corresponding to a given
     * "TransportWSO".
     * 
     * @param `List<Object>'
     *            :
     *            contains all "CustomFieldWSO" objects for the "Categories"
     *            sheet.
     */
    private void setListCustomFieldWso(
        Map<Integer, TransportObjectHelper> mapListTransportObjects,
        List<CustomFieldWSO> listCustomField) {
        listCustomField.forEach(customFieldWso -> {
            Integer transportId = customFieldWso.getItemId();
            TransportObjectHelper listTrObjects = mapListTransportObjects.get(transportId);
            if (listTrObjects == null) listTrObjects = new TransportObjectHelper();
            listTrObjects.listCustomField.add(customFieldWso);
            mapListTransportObjects.put(transportId, listTrObjects);
        });
    }

    /**
     * retrieve the list of "CostWSO" corresponding to a given "QuotationWSO".
     * 
     * @param `List<Object>'
     *            :
     *            contains all "CostWSO" objects for the "Post couts" sheet.
     */
    private List<CostWSO> setListCost(QuotationWSO quotationWSO, List<CostWSO> costWSOList) {
        List<CostWSO> costWSOs = new ArrayList<>();
        costWSOList.forEach(t -> {
            if (t.getQuoteReference().equals(quotationWSO.getQuoteReference())) costWSOs.add(t);
        });
        return costWSOs;
    }

    /**
     * Returns a list of functional errors.
     * Persist the items of the "list<TransportWSO>".
     * 
     * @param `List<TransportWSO>'
     *            :
     *            List of 'TransportWSO' retrieved from an Excel workbook.
     * 
     * @throws Exception
     */

    public List<MytowerTransportErrorMsg> getListFunctionalErrors(List<TransportWSO> transportWsos) throws Exception

    {
        List<MytowerTransportErrorMsg> functionalErrors = new ArrayList<>();

        for (int i = 0; i < transportWsos.size(); i++) {

            try {
                transportService.createPricingRequest(transportWsos.get(i));
            } catch (MyTowerException e) {
                MytowerTransportErrorMsg error = new MytowerTransportErrorMsg();
                error.setItemId(transportWsos.get(i).getItemId());
                error.setCustomerReference(transportWsos.get(i).getCustomerReference());
                error.setTransportReference(transportWsos.get(i).getTransportReference());
                error.setErrorMsgs(e.getListErrorMessage());
                functionalErrors.add(error);
            }

        }

        return functionalErrors;
    }
}
