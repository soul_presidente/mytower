package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.adias.mytowereasy.model.EbControlRule;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.util.email.Email;
import com.adias.mytowereasy.util.email.EmailHtmlSender;
import com.adias.mytowereasy.util.email.EmailSender;


@Service
public class EmailControlRuleService {
    @Value("${mytowereasy.url.front}")
    private String url;

    private EmailHtmlSender emailHtmlSender;

    @Autowired
    private EmailSender emailSender;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    public EmailControlRuleService(EmailHtmlSender emailHtmlSender) {
        this.emailHtmlSender = emailHtmlSender;
    }

    public void sendEmailControlRule(EbControlRule rule, EbTTPslApp ttPsl, EbDemande demande) {
        Email email = new Email();
        Context params = new Context();
        String emailBody = rule.getEmailMessage();
        String refTransport = null, statusPsl = null, statusTransport = null;

        if (ttPsl != null) {
            refTransport = ttPsl.getxEbTrackTrace().getRefTransport();
            statusPsl = ttPsl.getxEbTrackTrace().getCodeAlphaPslCourant();
            statusTransport = StatutDemande.getLibelle(ttPsl.getxEbTrackTrace().getxEbDemande().getxEcStatut());

            emailBody = emailBody.replace("${timestamp}", statusPsl);
        }

        if (demande != null) {
            refTransport = demande.getRefTransport();
            statusTransport = StatutDemande.getLibelle(demande.getxEcStatut());
        }

        emailBody = emailBody.replace("\n", "<br>");
        emailBody = emailBody.replace("${transport.reference}", refTransport);
        emailBody = emailBody.replace("${transport.status}", statusTransport);

        params.setVariable("emailBody", emailBody);
        emailBody = templateEngine.process("email-template-no-body.html", params);
        email.setSubject(rule.getEmailObjet());
        List<String> emails = Arrays.asList(rule.getEmailDestinataires().split(";"));
        email.setTo(emails.get(0));
        List<String> listCc = new ArrayList<>();

        if (emails.size() > 1) {
            int i = 1, n = emails.size();

            for (i = 1; i < n; i++) {
                listCc.add(emails.get(i));
            }

            email.setEnCopieCachee(listCc);
        }

        email.setText(emailBody);

        emailSender.send(email);
    }
}
