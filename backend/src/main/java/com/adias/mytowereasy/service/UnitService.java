package com.adias.mytowereasy.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbMarchandiseForExtraction;


@Service
public interface UnitService {
    Map<String, Object> getListTree(Integer idDemande);

    List<Map<String, String[]>>
        listMapForExcelUtils(List<EbMarchandise> listMarchandiseAExporter, Integer typeContener);

    List<Map<String, String[]>>
        listMapForExtractionConsolider(List<EbMarchandiseForExtraction> listMarchandiseAExporter);

    public Integer getNextMarchandiseNum();

    List<EbMarchandise> getListMarchandises(Integer idDemande);
}
