package com.adias.mytowereasy.service;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.airbus.service.EbTtCompanyPslService;
import com.adias.mytowereasy.model.Enumeration.DataRecoveryFormat;
import com.adias.mytowereasy.model.Enumeration.DataRecoveryType;


@Service
public class DataRecoveryServiceImpl extends MyTowerService implements DataRecoveryService {
    @Autowired
    protected TypeRequestService typeRequestService;

    @Autowired
    protected EbTtCompanyPslService ebTtCompanyPslService;

    @Autowired
    protected TypeUnitService typeUnitService;

    @Autowired
    protected TypeDocumentService typeDocumentService;

    @Autowired
    protected PricingService pricingService;

    @Autowired
    protected SavedFormService savedFormService;

    @Autowired
    protected EbTypeFluxService ebTypeFluxService;

    @Transactional
    @Override
    public Boolean generateData(Map<String, List<Integer>> data) {
        List<Integer> dataTypes = data.get(DataRecoveryFormat.DATA_TYPE.getLibelle());
        List<Integer> compagnies = data.get(DataRecoveryFormat.COMPAGNIES.getLibelle());

        try {

            for (Integer dt: dataTypes) {

                if (dt.equals(DataRecoveryType.TYPE_REQUEST.getCode())) {
                    typeRequestService.generateTypeOfRequestByCompanyNum(compagnies);
                }
                else if (dt.equals(DataRecoveryType.PSL_SCHEMA.getCode())) {
                    ebTtCompanyPslService.generatePslSchemaPslByCompanyNum(compagnies);
                }
                else if (dt.equals(DataRecoveryType.TYPE_DOCUMENT.getCode())) {
                    typeDocumentService.generateTypeOfDocumentByCompanyNum(compagnies);
                }
                else if (dt.equals(DataRecoveryType.DEMANDE_QUOTE.getCode())) {
                    pricingService.updateQuotesWithPslByCompanyNum(compagnies);
                }
                else if (dt.equals(DataRecoveryType.TYPE_UNIT.getCode())) {
                    typeUnitService.generateTypeOfUnitServiceByCompanyNum(compagnies);
                }
                else if (dt.equals(DataRecoveryType.TYPE_FLUX.getCode())) {
                    ebTypeFluxService.generateTypeOfFluxServiceByCompanyNum(compagnies);
                }

            }

            System.out.println("Recovery completed successfully.");

            return true;
        } catch (Exception ex) {
            ex.printStackTrace();

            return false;
        }

    }
}
