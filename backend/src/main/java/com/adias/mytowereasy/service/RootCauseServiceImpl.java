/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dao.DaoRootCause;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.qm.EbQmRootCause;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


@Service
public class RootCauseServiceImpl extends MyTowerService implements RootCauseService {
    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    DaoRootCause daoRootCause;

    @Override
    public List<EbQmRootCause> listRootCause(SearchCriteriaQM params) {

        if (params != null) {
            EbUser connectedUser = connectedUserService.getAvailableCurrentUser(params);
            params.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
            return daoRootCause.getListEbQmRootCause(params);
        }
        else return ebQmRootCauseRepository.findAll();

    }

    @Override
    public boolean saveRootCauses(EbQmRootCause rootCause) {
        ebQmRootCauseRepository.save(rootCause);
        return true;
    }

    @Override
    public Long getCountListRootCause(SearchCriteriaQM criteria) throws Exception {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);
        criteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
        // TODO Auto-generated method stub
        return daoRootCause.listCount(criteria);
    }
}
