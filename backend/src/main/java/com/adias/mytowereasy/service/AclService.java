package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.dto.EbAclRightDTO;
import com.adias.mytowereasy.dto.EbAclSettingDTO;


public interface AclService {
    List<EbAclSettingDTO> getSettingsForProfile(Integer ebUserProfileNum);

    List<EbAclSettingDTO> getSettingsForUser(Integer ebUserNum);

    void saveSettings(List<EbAclSettingDTO> settings, Integer ebUserProfileNum, Integer ebUserNum);

    List<EbAclRightDTO> getACLsForConnectedUser();
}
