package com.adias.mytowereasy.service;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.cronjob.tt.model.EdiMapPsl;
import com.adias.mytowereasy.utils.search.SearchCriteriaMappingCodePsl;


public interface MappingCodePslService {
    List<EdiMapPsl> getListMappingCodePsl(SearchCriteriaMappingCodePsl criteria);

    Long getListMappingCodePslCount(SearchCriteriaMappingCodePsl criteria);

    EdiMapPsl updateMappingCodePsl(EdiMapPsl mapping);

    Boolean deleteMappingCodePsl(Integer Id);

    String getAutoCompleteListMappingCodePsl(String term, String field) throws JsonProcessingException;
}
