package com.adias.mytowereasy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dao.DaoPricingTracking;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Service
public class DaoPricingTrackingServiceImpl implements DaoPricingTrackingService {
    @Autowired
    DaoPricingTracking daoPricingTracking;

    @Override
    public List<EbDemande> getListEbDemande(SearchCriteriaPricingBooking criteria) throws Exception {
        return daoPricingTracking.getListEbDemande(criteria);
    }
}
