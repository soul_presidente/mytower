/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.adias.mytowereasy.cronjob.tt.util.TmpInfosPsl;
import com.adias.mytowereasy.dto.EbTTPslAppAndEventsProjection;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.model.tt.EbTtEvent;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.util.tt.TracingUpdatePslResult;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaTrackTrace;


public interface TrackService {
    public int getCountListEbTrackTrace(SearchCriteriaTrackTrace criteria) throws Exception;

    public List<EbTtTracing> insertEbTrackTrace(EbDemande ebDemande);

    public List<EbTtTracing> getListEbTrackTrace(SearchCriteriaTrackTrace criteria) throws Exception;

    public EbTtTracing getEbTracing(SearchCriteriaTrackTrace criteria);

    public Map<String, Object> updateEbPslApp(EbTTPslApp ebPslApp);

    /*
     * TracingResult a deux variables
     * pour la MAJ d'un psl a partir de l'app nous utilisons result de type
     * Boolean(true-->succes, false-->erreur)
     * mais pour la generation des events par edi nous utilisons listReject,
     * c'est la liste des events rejeté
     */
    public TracingUpdatePslResult
        updateEbPslAppMasse(List<EbTTPslApp> listEbPslApp, EbUser user, List<TmpInfosPsl> listlistTmpInfosPsl);

    public Integer addDeviationMasse(List<EbTtEvent> listTTEvent, EbUser userConnected);

    public List<EbTtTracing> getListEbTracingByEbDemandeNum(Integer ebDemandeNum);

    public List<EbTtTracing> getListEbtrack(SearchCriteriaTrackTrace criterias);

    public List<EbTtSchemaPsl> getListEbIncotermPsl();

    public EbTtTracing saveFlags(Integer ebTtTracingNum, String newFlags);

    public void insertDeviation();

    List<EbTtTracing> getTracing(SearchCriteriaTrackTrace criteria);

    public Integer updateRevisedDateEbPslAppMasse(List<EbTTPslApp> listEbPslApp);

    Map<String, Object> updateRevisedDateEbPslApp(EbTTPslApp ebPslApp);

    public List<EbTTPslAppAndEventsProjection> getTracingPslAndsThiereEvents(Integer ebTtTracingNum);

    List<EbTTPslApp> getListTracingStatus(SearchCriteria criteria);

    /**
     * Affectation des informations liées aux PSL à la demande </br>
     * </br>
     * - Mise à jour des dates estimées des PSL Pickup, Arrival, Departure,
     * Delivery </br>
     * - Affectation du libellé du dernier Psl à la demande </br>
     * - Afffectation du schema PSl à la demande </br>
     * 
     * @param demande
     * @return demande
     */
    EbDemande handlePsl(EbDemande demande);

    Map<String, Object> addDeviation(EbTtEvent ebTtEvent, EbUser connectedUser, boolean lunchDR);

		public List<Map<String, String>> getListStatutTracing(SearchCriteria criteria)
			throws IOException;

		public List<EbTTPslApp> getListTracingStatusByListOfCompanyNum(List<Integer> listCompanyNums);
}
