/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service.email;

import java.util.List;

import com.adias.mytowereasy.exception.ErrorDetails;
import com.adias.mytowereasy.model.EbChat;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.EmailConfig;


public interface EmailService {
    public void sendEmailInvitationCollegue(EbUser user, List<String> emails, EmailConfig emailParams);

    public void sendEmailInvitationFriend(EbUser user, List<String> emails, EmailConfig emailParams);

    public void sendEmailResetPassword(EbUser user);

    public void sendEmailUpdatedPassword(EbUser user);

    public void sendEmailActivePassword(EbUser user);

    public void sendEmailInvitationAccepter(EbUser destinataire, EbUser expediteur);

    public void sendEmailCompteActiverParAdminSocieter(EbUser user);

    public void sendEmailNouveauCompteSociete(EbUser nouvelUtilisateur, List<EbUser> adminsUser);

    public void sendEmailInviterMyCommunity(List<EbUser> destinataires, EbUser adminSocieter, String link);

    public void sendEmailInviteUtiliserMyTower(EbUser user, String emailToSend);

    public void sendEmailBackup(EbUser destinataire, EbUser expediteur);

    public void sendEmailForCreatePassword(EbUser user);

    public void sendMailAddComment(String username, EbChat commentaire, EbDemande ebDemande);

    public void sendEmailCompteRefuserParAdminSocieter(EbUser nouvelUtilisateur, EbUser adminSocieter);

    void sendEmailException(ErrorDetails errorDetails);

    public void sendEmailCreationNouvelleSociete(String lang);
}
