/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.cptm.model.EbCptmProcedure;
import com.adias.mytowereasy.cptm.model.EbCptmRegimeTemporaire;
import com.adias.mytowereasy.dto.CustomDeclarationDocumentDTO;
import com.adias.mytowereasy.dto.CustomDeclarationPricingDTO;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbDemandeFichiersJoint;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbTypeDocuments;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.custom.EbChatCustom;
import com.adias.mytowereasy.model.custom.EbCustomDeclaration;
import com.adias.mytowereasy.model.enums.Modules;
import com.adias.mytowereasy.repository.custom.EbChatCustomRepository;
import com.adias.mytowereasy.utils.search.SearchCriteriaCustom;


@Service
public class CustomDeclarationServiceImpl extends MyTowerService implements CustomDeclarationService {
    @Autowired
    ListStatiqueService listStatiqueService;

    @Autowired
    private EbChatCustomRepository ebChatRepositorty;

    @Autowired
    TypeDocumentService typeDocumentService;

    @Override
    public int getCountListEbDeclaration(SearchCriteriaCustom criteria) {
        long count = daoDeclaration.getCountListEbDeclaration(criteria);
        return (int) count;
    }

    @Override
    public List<EbCustomDeclaration> getListEbDeclaration(SearchCriteriaCustom criteria) {
        List<EbCustomDeclaration> listDeclaration = daoDeclaration.getListEbDeclaration(criteria);
        return listDeclaration;
    }

    @Override
    public void historizeActioncUSTOM(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        String username,
        String usernameFor) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        EbChatCustom chat = new EbChatCustom();
        chat.setUserName(connectedUser.getNomPrenom());
        chat.setDateCreation(new Date());
        chat.setModule(module);
        chat.setIdFiche(idFiche);
        chat.setIsHistory(true);
        chat.setIdChatComponent(chatCompId);
        chat.setText(action + " by <b>" + username + "</b>");
        if (usernameFor != null) chat.setText(chat.getText() + " for <b>" + usernameFor + "</b>");
        ebChatRepositorty.save(chat);
    }

    @Override
    public EbCustomDeclaration addEbDeclaration(EbCustomDeclaration ebDeclaration) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        ebDeclaration = ebDeclarationRepository.save(ebDeclaration);

        ebDeclaration.setDeclarationReference("DEC-" + String.valueOf(ebDeclaration.getEbCustomDeclarationNum()));
        ebDeclaration.setxEbUserDeclarant(connectedUser);
        ebDeclaration.setEbCompagnie(connectedUser.getEbCompagnie());

        ebDeclaration = ebDeclarationRepository.save(ebDeclaration);

        String historisationUserName = null;

        if (connectedUser.isControlTower() || connectedUser.getRealUserNum() != null) {

            if (ebDeclaration.getCharger() != null) {
                historisationUserName = ebDeclaration.getCharger().getNomPrenom();
            }
            else if (ebDeclaration.getxEbUserDeclarant() != null) {
                historisationUserName = ebDeclaration.getxEbUserDeclarant().getNomPrenom();
            }

        }

        // Historisation
        historizeActioncUSTOM(
            ebDeclaration.getEbCustomDeclarationNum(),
            Enumeration.IDChatComponent.CUSTOM.getCode(),
            Enumeration.Module.CUSTOM.getCode(),
            "Declaration created",
            connectedUser.getRealUserNum() != null ?
                connectedUser.getRealUserNomPrenom() :
                connectedUser.getNomPrenom(),
            historisationUserName);

        return ebDeclaration;
    }

    @Override
    public EbCustomDeclaration getEbDeclaration(SearchCriteriaCustom criteria) {
        EbCustomDeclaration ebDeclaration = daoDeclaration.getEbDeclaration(criteria);
        return ebDeclaration;
    }

    @Override
    public EbCustomDeclaration newDeclarationPricing(CustomDeclarationPricingDTO request) throws Exception {
        EbCustomDeclaration dec = new EbCustomDeclaration();

        // Retrieving items
        EbDemande demande = ebDemandeRepository.findById(request.getEbDemandeNum()).get();
        List<EbMarchandise> marchandises = ebMarchandiseRepository.findByIDs(request.getUnitsNum());

        List<EbCptmProcedure> procedures = ebCptmProcedureRepository.findByMarchandiseNum(request.getUnitsNum());

        EbCptmRegimeTemporaire regime = null;

        for (EbCptmProcedure procedure: procedures) {

            if (regime == null && procedure.getxRegimeTemporaire() != null) {
                regime = procedure.getxRegimeTemporaire();
            }

        }

        EbUser connectedUser = connectedUserService.getCurrentUser();

        // Fill declaration object
        dec.setDeclarationNumber(request.getDeclarationNumber());
        dec.setxEbUserDeclarant(connectedUser);
        dec.setStatut(comptaMatiereService.areAllCompleted(procedures) ? 1 : 0);
        dec.setNbOfTemporaryRegime(marchandises.size());

        if (regime != null) {
            dec.setCustomRegime(regime.getType());
            dec.setxEbCptmRegimeTemporaire(regime);
        }

        dec.setxEbDemande(demande);

        if (request.getDocsNum() != null && request.getDocsNum().size() > 0) {
            List<String> stringDocNumList = new ArrayList<String>(request.getDocsNum().size());

            for (Integer myInt: request.getDocsNum()) {
                stringDocNumList.add(String.valueOf(myInt));
            }

            String splittedString = String.join(",", stringDocNumList);
            dec.setListEbDemandeFichiersJointNum(splittedString);
        }

        // Fill related objets
        List<EbTypeDocuments> typeDocs = typeDocumentService
            .getTypeDocsByEbTypeDocumentsModule(Modules.CUSTOM.getValue(), connectedUser);
        dec.setListTypeDocuments(typeDocs);

        // Save declaration object
        dec = addEbDeclaration(dec);

        for (EbMarchandise marchandise: marchandises) {
            marchandise.setxEbCustomDeclaration(dec);
            ebMarchandiseRepository.save(marchandise);
        }

        // Save Date Declaration using the generic method
        dec = updateDateDeclaration(dec, request.getDeclarationDate());
        return dec;
    }

    public EbCustomDeclaration inlineProcedureDashboardUpdate(EbCustomDeclaration sent) {
        EbCustomDeclaration entity = ebDeclarationRepository.findById(sent.getEbCustomDeclarationNum()).get();

        entity.setComment(sent.getComment());

        return ebDeclarationRepository.save(entity);
    }

    @Override
    public List<CustomDeclarationDocumentDTO>
        searchlistDocuments(Integer module, Integer ebUserNum, Integer type, Integer demande)
            throws JsonProcessingException {
        Map<String, List<EbDemandeFichiersJoint>> originalResult = listStatiqueService
            .searchlistDocuments(module, ebUserNum, type, demande);
        List<CustomDeclarationDocumentDTO> result = new ArrayList<>();

        originalResult.values().forEach(val -> {
            val.forEach(origDoc -> {
                CustomDeclarationDocumentDTO doc = new CustomDeclarationDocumentDTO();
                doc.setEbDemandeFichierJointNum(origDoc.getEbDemandeFichierJointNum());
                doc.setFilename(origDoc.getFileName());
                doc.setTypeDocument(origDoc.getIdCategorie());

                result.add(doc);
            });
        });

        return result;
    }

    @Override
    public List<EbDemandeFichiersJoint> listFichierJointsDeclaration(Integer ebDeclarationNum, Integer ebCompagnieNum) {
        return ebDemandeFichiersJointRepositorty.findByDeclaration(ebDeclarationNum, ebCompagnieNum);
    }

    @Override
    public EbCustomDeclaration updateDateDeclaration(EbCustomDeclaration declaration, Date newDateDeclaration) {
        if (newDateDeclaration == null) return declaration;

        // Save Declaration Object
        declaration.setDateDeclaration(newDateDeclaration);
        declaration = ebDeclarationRepository.save(declaration);

        // Update Compta Matière deadline
        List<EbCptmProcedure> procedures = ebCptmProcedureRepository
            .findByLeg1Declaration(declaration.getEbCustomDeclarationNum());

        for (EbCptmProcedure procedure: procedures) {
            Optional<EbCptmRegimeTemporaire> regime = ebCptmRegimeTemporaireRepository
                .findById(procedure.getxLeg1Unit().getCptmRegimeTemporaireNum());

            if (regime.isPresent()) {
                Calendar c = Calendar.getInstance();
                c.setTime(newDateDeclaration);
                c.add(Calendar.DAY_OF_MONTH, regime.get().getDeadlineDays());
                procedure.setDeadline(c.getTime());
                ebCptmProcedureRepository.save(procedure);
            }

        }

        return declaration;
    }

    @Override
    public EbCustomDeclaration updateDateDeclaration(Integer declarationNum, Date newDateDeclaration) {
        EbCustomDeclaration entity = ebDeclarationRepository.findById(declarationNum).get();
        return updateDateDeclaration(entity, newDateDeclaration);
    }
}
