/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service.email;

import java.util.List;

import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;


public interface EmailDocumentService {
    public void sendMailUploadFile(EbUser user, EbDemande ebDemande, Integer module, Integer ebQmIncidentNum);

    void sendEmailDeleteDocument(
        EbUser user,
        List<String> emailToSend,
        EbDemande ebDemande,
        Integer numEntity,
        Integer module);
}
