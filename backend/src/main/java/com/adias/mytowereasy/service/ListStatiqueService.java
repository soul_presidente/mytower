/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.docx4j.openpackaging.exceptions.Docx4JException;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.dto.CodeLibelleDTO;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.ServiceType;
import com.adias.mytowereasy.model.Enumeration.TypeContainer;
import com.adias.mytowereasy.model.tt.EbTtCategorieDeviation;
import com.adias.mytowereasy.util.GenericEnum;
import com.adias.mytowereasy.util.enums.GenericEnumException;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public interface ListStatiqueService {
    public List<EcCity> getListEcCityByCountry(Integer ecCountryNum);

    public List<EcCountry> getListEcCountry();

    public EcCountry getEcCountryByCode(String code);

    public EcCountry getEcCountryByLibelle(String libelle);

    public List<EcMimetype> getListMimeType();

    public ServiceType[] getListUserService();

    public TypeContainer[] getListTypeConteneur();

    public List<EcCity> getListEcCity();

    List<EcRegion> getListEcRegion();

    public String getListCarrier(String term) throws JsonProcessingException;

    public String getListCostCenter(String term) throws JsonProcessingException;

    public EbDatatableState loadState(Integer ebUserNum, Integer module);

    public String getListtRefCustomerAndTraspo(String term) throws JsonProcessingException;

    public String getModeTrasport(String term) throws JsonProcessingException;

    public List<EcCurrency> getListEcCurrency();

    public List<EbTtCategorieDeviation> getListCategorieDeviation(Integer typeOfEvent);

    EbChat historizeAction(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        String username,
        String usernameFor,
        Object... criteria);

    EbChat addChat(EbChat chat);

    Map<String, Object>
        listDestinataireByEmailId(Integer mailId, Integer xEcModule, Integer idEntity, Object... params);

    EbDatatableState saveState(String state, Integer ebUserNum, Integer datatableCompId);

    String getListTransportRef(String term, Integer module) throws JsonProcessingException;

    String getListCustomerReference(String term, Integer module) throws JsonProcessingException;

    public String getListAgent(String term) throws JsonProcessingException;

    public String getListClearanceID(String term) throws JsonProcessingException;

    public String getListShiper(String term) throws JsonProcessingException;

    public String getValueTtPsl(Integer key);

    public String getListModeTransport() throws JsonProcessingException, IOException;

    public String getListChargeur(String term) throws JsonProcessingException;

    String getAutoCompleteListOrder(String term, String field) throws JsonProcessingException;

    String getAutoCompleteListDelivery(String term, String field) throws JsonProcessingException;

    String getAutoCompleteListMarchandise(String term, String field) throws JsonProcessingException;

    List<EbParamsMail> getListEmailParam();

    public Map<Integer, List<EbTypeTransport>> getlistAllTypeTransport() throws IOException;

    List<EcModule> getListEcModule();

    public List<CodeLibelleDTO> getListStatusByModule(Integer moduleNum) throws IOException;

    Class<? extends SearchCriteria> getSearchCriteriaClassByModule(Integer ebModuleNum);

    String getListExtractors() throws JsonProcessingException, IOException;

    List<GenericEnum> listEnumValues(String genericEnumKey) throws GenericEnumException;

    String listEnumValuesJson(String genericEnumKey) throws GenericEnumException;

    public List<EcFuseauxHoraire> getListFuseauxHoraire();

    Map<String, List<EbDemandeFichiersJoint>>
        searchlistDocuments(Integer module, Integer ebUserNum, Integer type, Integer demande)
            throws JsonProcessingException;

    public List<EbUser> getxEbOwnerOfTheRequest(Integer ebUserNum);

    Map<String, Object>
        getEmailUsers(Integer compagnieNum, Integer mailId, Integer xEcModule, Integer ebDemandeNum, Object... params);

		public Map<String, Object> getEmailUsersWithMailParamCheck(
			Integer compagnieNum,
			Integer mailId,
			Integer xEcModule,
			Integer ebDemandeNum,
			boolean ignoreMailParamCheck,
			Map<String, Object> params
		);

		Map<String, Object> listDestinataireUserByEmailId(
			Integer mailId,
			Integer xEcModule,
			Integer idEntity,
			boolean ignoreMailParamCheck,
			Map<String, Object> params
		);

    public List<EbUser>
        listUserByEmailId(Integer mailId, Integer xEcModule, Integer idEntity, boolean checkAlert, Object... params);

    public EbChat historizeAction(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        EbUser creatorUser,
        String username,
        String usernameFor,
        Object[] data);

    public File
        consolidateDocByEntityAndModule(List<Integer> modules, Integer entityNum) throws Docx4JException, IOException;

    public List<EbChat> getListChat(Integer module, Integer idFiche, Integer idChatComponent);
}
