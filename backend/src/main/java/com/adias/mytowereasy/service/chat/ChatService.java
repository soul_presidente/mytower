package com.adias.mytowereasy.service.chat;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbChat;
import com.adias.mytowereasy.repository.EbChatRepository;
import com.adias.mytowereasy.repository.custom.EbChatCustomRepository;
import com.adias.mytowereasy.service.MyTowerService;


@Service
public class ChatService extends MyTowerService {
    @Autowired
    EbChatCustomRepository ebChatCustomRepository;

    @Autowired
    EbChatRepository chatRepository;

    @Autowired
    ChatFetcher chatFetcher;

    public List<EbChat> getListChat(Integer module, Integer idFiche, Integer idChatComponent) {
        if (idFiche == null || module == null) {
            throw new IllegalStateException("idFiche And module mustn't be null");
        }
        return chatFetcher.getListChat(module, idFiche, idChatComponent);
    }
}
