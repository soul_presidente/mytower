package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.EbVehicule;


public interface VehiculeService {
    public List<EbVehicule> SavelistVehicule(List<EbVehicule> listVehicule);

    public List<EbVehicule> getListVehicule(Integer compagnieNum);

    public Integer deleteVehicule(Integer vehiculeNum);
}
