package com.adias.mytowereasy.service;

import com.adias.mytowereasy.model.EbUserThemes;


public interface ThemesConfigService {
    boolean saveThemesConfig(EbUserThemes themesConfig);

    EbUserThemes getCompagnieTheme(Integer ebCompagnieNum);
}
