package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.chanel.model.EbWeekTemplate;
import com.adias.mytowereasy.chanel.model.EbWeekTemplateDay;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface WeekTemplateService {
    public List<EbWeekTemplate> listWeekTemplate(SearchCriteria criteria);

    public Long countListWeekTemplate(SearchCriteria criteria);

    public int deleteWeekTemplate(EbWeekTemplate ebWeekTemplate);

    public EbWeekTemplate saveWeekTemplate(EbWeekTemplate ebWeekTemplate);

    public List<EbWeekTemplate> listWeekTemplate();

    List<EbWeekTemplateDay> findByWeek_ebWeektemplateNum(Integer ebWeekTemplateNum);
}
