package com.adias.mytowereasy.service;

import com.adias.mytowereasy.model.EbUserTag;


public interface UserTagService {
    public EbUserTag addUserTag(EbUserTag ebUsertag, Integer ebCompagnieNum);
}
