package com.adias.mytowereasy.service;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;

import org.docx4j.openpackaging.exceptions.Docx4JException;

import com.adias.mytowereasy.model.EbUser;


public interface DocTemplateService {
    void generateTemplatePdf(InputStream templateFile, Object model, Path pdfFile, EbUser connectedUser)
        throws IOException,
        Docx4JException;

    void generateTemplateDocx(InputStream templateFile, Object model, Path docxFile, EbUser connectedUser)
        throws IOException;
}
