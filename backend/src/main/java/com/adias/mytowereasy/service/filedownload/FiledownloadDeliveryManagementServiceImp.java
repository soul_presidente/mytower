package com.adias.mytowereasy.service.filedownload;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.service.DeliveryLineService;
import com.adias.mytowereasy.delivery.service.DeliveryService;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.FileDownloadServiceImpl;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.category.CategoryService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class FiledownloadDeliveryManagementServiceImp extends MyTowerService
    implements FiledownloadDeliveryManagementService {
    @Autowired
    private DeliveryLineService deliveryLineService;

    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    DeliveryService deliveryService;

    @Autowired
    CategoryService categoryService;

    @Override
    public FileDownloadStateAndResult listFileDownLoadDeliveryManagement(
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        SearchCriteria criteria,
        List<EbCategorie> listCategorie,
        Map<String, String> mapCustomField)
        throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();
        Locale locale = connectedUser.getLocaleFromLanguage();
        DateFormat dateFormat = new SimpleDateFormat(connectedUser.getDateFormatFromLocale());

        ObjectMapper objectMapper = new ObjectMapper();
        SearchCriteriaDelivery criteriaDelivery = objectMapper
            .convertValue(criteria, SearchCriteriaDelivery.class);

        List<EbLivraison> deliveries = deliveryService.getListDeliveries(criteriaDelivery);
        Map<String, Object> listcatAndCustom = categoryService
            .getListCategoryAndCustomField(criteriaDelivery, connectedUser);
        List<EbCustomField> fields = new ArrayList<EbCustomField>();
        fields = (List<EbCustomField>) listcatAndCustom.get("customField");
        listCategorie = (List<EbCategorie>) listcatAndCustom.get("listCategorie");

        if (deliveries.size() > FileDownloadServiceImpl.EXTRACTION_LIMITE) {
            resultAndState.setState(FileDownLoadStatus.LIMITE_DEPASSE.getCode());
            resultAndState.setListResult(listresult);
        }
        else {

            /* DELIVERY'S COLUMNS */
            for (EbLivraison delivery: deliveries) {
                Map<String, String[]> map = new LinkedHashMap<String, String[]>();
                
                if (isPropertyInDtConfig(dtConfig, "listEbFlagDTO")) {
                    String listFlagsLibelle = "";

                    if (delivery.getListEbFlagDTO() != null && delivery.getListEbFlagDTO().size() > 0) {

                        listFlagsLibelle = delivery
                            .getListEbFlagDTO().stream().map(d -> d.getName()).collect(Collectors.joining(", "));

                    }

                    if (listFlagsLibelle.length()
                        > 0) listFlagsLibelle = listFlagsLibelle.substring(0, listFlagsLibelle.length() - 2);
                    map.put("listEbFlagDTO", new String[]{
                        messageSource.getMessage("pricing_booking.flag", null, locale), listFlagsLibelle
                    });
                }

                if (isPropertyInDtConfig(dtConfig, "xEbPartyOrigin")) map.put("xEbPartyOrigin", new String[]{
                    messageSource.getMessage("pricing_booking.address_from", null, locale),
                    delivery.getxEbPartyOrigin().getxEcCountry().getLibelle()
                });
                
                if (isPropertyInDtConfig(dtConfig, "xEbPartyDestination")) map.put("xEbPartyDestination", new String[]{
                    messageSource.getMessage("pricing_booking.address_to", null, locale),
                    delivery.getxEbPartyDestination().getxEcCountry().getLibelle()
                });
                
                
                if (isPropertyInDtConfig(dtConfig, "ebDelReference")) map.put("ebDelReference", new String[]{
                    messageSource.getMessage("delivery.reference", null, locale), delivery.getEbDelReference()
                });
                
                
                if (isPropertyInDtConfig(dtConfig, "xEbPartySale")) map.put("xEbPartySale", new String[]{
                    messageSource.getMessage("delivery.address_sold_to", null, locale),
                    delivery.getxEbPartySale() != null && delivery.getxEbPartySale().getxEcCountry() != null ?
                        delivery.getxEbPartySale().getxEcCountry().getLibelle() :
                        null
                });
                
                
                if (isPropertyInDtConfig(dtConfig, "dateCreationSys")) map.put("dateCreationSys", new String[]{
                    messageSource.getMessage("pricing_booking.request_date", null, locale),
                    delivery.getDateCreationSys() != null ? dateFormat.format(delivery.getDateCreationSys()) : ""
                });
                
                
                if (isPropertyInDtConfig(dtConfig, "xEcIncotermLibelle")) map.put("xEcIncotermLibelle", new String[]{
                    messageSource.getMessage("track_trace.incoterms", null, locale), delivery.getxEcIncotermLibelle()
                });

                map.put("refDelivery", new String[]{
                    "Delivery#", delivery.getRefDelivery() != null ? delivery.getRefDelivery() : ""
                });
                map.put("refTransport", new String[]{
                    "Transport#", delivery.getRefTransport() != null ? delivery.getRefTransport() : ""
                });
                
				if (isPropertyInDtConfig(dtConfig, "statusDelivery"))
					map.put("statusDelivery",
							new String[] { messageSource.getMessage("delivery.delivery_status", null, locale),
									messageSource.getMessage(
											"chanel_pb_delivery." + delivery.getStatusDelivery().getKey(), null,
											locale) });

                map.put("orderCreationDate", new String[]{
                    messageSource.getMessage("chanel_pb_delivery.order_creation_date", null, locale),
                    delivery.getOrderCreationDate() != null ? dateFormat.format(delivery.getOrderCreationDate()) : ""
                });
                
                if (isPropertyInDtConfig(
                    dtConfig,
                    "customerOrderReference")) map.put("customerOrderReference", new String[]{
                        messageSource.getMessage("order.customer_order_reference", null, locale),
                        delivery.getxEbDelOrder().getCustomerOrderReference()
                });
                
                
                if (isPropertyInDtConfig(
                    dtConfig,
                    "dateOfGoodsAvailability")) map.put("dateOfGoodsAvailability", new String[]{
                        messageSource.getMessage("pricing_booking.address_from", null, locale),
                        delivery.getDateOfGoodsAvailability() != null ?
                            dateFormat.format(delivery.getDateOfGoodsAvailability()) :
                            ""
                });
                
                /* CATEGORIES & LABELS */
                if (listCategorie != null && delivery.getListCategories() != null) {

                    for (EbCategorie categorie: listCategorie) {

                        if (isPropertyInDtConfig(dtConfig, "category" + categorie.getEbCategorieNum())) {
                            for (EbCategorie deliveryCategorie: delivery.getListCategories()) {

                                if (deliveryCategorie.getLabels() != null &&
                                    deliveryCategorie.getEbCategorieNum().equals(categorie.getEbCategorieNum())) {
                                    String label = null;

                                    for (EbLabel ebLabel: deliveryCategorie.getLabels()) {
                                        label = ebLabel.getLibelle();
                                    }

                                    map.put("category" + categorie.getEbCategorieNum(), new String[]{
                                        categorie.getLibelle(), label
                                    });
                                }

                            }

                        }

                    }

                }
                
                /* CUSTOM FIELD'S COLUMNS */
                if (fields != null && fields.size() > 0) {

                    for (EbCustomField customFields: fields) {
                        List<CustomFields> field = new ArrayList<CustomFields>();

                        field = gson.fromJson(customFields.getFields(), new TypeToken<ArrayList<CustomFields>>() {
                        }.getType());

                        if (field != null && field.size() > 0) {

                            for (CustomFields fie: field) {
                                if (isPropertyInDtConfig(dtConfig, fie.getName())) map.put(fie.getName(), new String[]{
                                    fie.getLabel(), null
                                });
                            }

                        }
                        if (delivery.getxEbCompagnie() == null) {
                            delivery.setxEbCompagnie(connectedUser.getEbCompagnie());
                        if (field != null &&
                            delivery.getListCustomsFields() != null &&
                            (delivery.getxEbCompagnie().getEbCompagnieNum()
                                .equals(customFields.getxEbCompagnie()))) {

                            for (CustomFields fie: field) {

                                if (isPropertyInDtConfig(dtConfig, fie.getName())) {

                                    for (CustomFields deliveryField: delivery.getListCustomsFields()) {

                                        if (deliveryField.getName().contentEquals(fie.getName())) {
                                            map.put(fie.getName(), new String[]{
                                                fie.getLabel(), deliveryField.getValue()
                                            });
                                            break;
                                        }

                                    }

                                }

                            }

                            }
                        }

                    }

                }

                listresult.add(map);
            }

            resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
            resultAndState.setListResult(listresult);
        }

        return resultAndState;
    }
}
