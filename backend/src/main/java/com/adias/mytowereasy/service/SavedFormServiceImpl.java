/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.adias.mytowereasy.dao.DaoSavedForm;
import com.adias.mytowereasy.dto.CodeLibelleDTO;
import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.dto.EbSavedFormSearchCriteriaDto;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.Enumeration.Module;
import com.adias.mytowereasy.model.Enumeration.SavedFormComponent;
import com.adias.mytowereasy.model.Enumeration.SavedSearchAction;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.repository.EbEtablissementRepository;
import com.adias.mytowereasy.repository.EbTypeUnitRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;
import com.adias.mytowereasy.utils.search.SearchCriteriaTrackTrace;


@Service
public class SavedFormServiceImpl extends MyTowerService implements SavedFormService {
    static final Integer SIZE = 15;

    @Autowired
    ListStatiqueService serviceListStatique;

    @Autowired
    EbTypeUnitRepository ebTypeUnitRepository;

    @Autowired
    EbEtablissementRepository ebEtablissementRepository;

    @Autowired
    EbUserRepository ebUserRepository;

    @Autowired
    DaoSavedForm daoSavedForm;

    @Override
    public boolean deleteEbSavedForm(EbSavedForm ebSavedForm) {
        ebSavedFormRepository.delete(ebSavedForm);
        return true;
    }

    @Override
    public EbSavedForm upsertEbSavedForm(EbSavedForm ebSavedForm) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        if (ebSavedForm.getEbCompNum().equals(Enumeration.SavedFormComponent.HISTORY.getCode())) {
            Integer ebDemandeNum = NumberUtils.toInt(ebSavedForm.getFormName());

            if (ebDemandeNum != null) {
                Optional<EbDemande> demande = ebDemandeRepository.findById(ebDemandeNum);

                if (demande.isPresent()) {
                    ebSavedForm.setFormName(demande.get().getRefTransport());
                }

            }

        }

        if (ebSavedForm.getEbSavedFormNum() == null) {
            List<EbSavedForm> listSavedForm = ebSavedFormRepository
                .findAllByFormNameAndEbCompNumAndEbModuleNumAndEbUser_ebUserNum(
                    ebSavedForm.getFormName(),
                    ebSavedForm.getEbCompNum(),
                    ebSavedForm.getEbModuleNum(),
                    ebSavedForm.getEbUserNum());

            if (listSavedForm != null && listSavedForm.size() > 0) {
                ebSavedForm = listSavedForm.get(0);
                ebSavedForm.setDateAjout(new Date());
            }

        }

        if (ebSavedForm.getCompagnie() == null) {
            ebSavedForm.setCompagnie(connectedUser.getEbCompagnie());
        }

        if (ebSavedForm.getOrdre() == null) {
            EbSavedForm Sform = ebSavedFormRepository
                .findTopByEbCompNumAndCompagnieOrderByOrdreDesc(
                    ebSavedForm.getEbCompNum(),
                    connectedUser.getEbCompagnie());
            if (Sform != null && Sform.getOrdre() != null) ebSavedForm.setOrdre(Sform.getOrdre() + 1);
            else ebSavedForm.setOrdre(0);
        }

        return ebSavedFormRepository.save(ebSavedForm);
    }

    @Override
    public List<EbSavedForm> selectListEbSavedForm(SearchCriteria criterias) {

        if (criterias.getEbCompNum() == SavedFormComponent.HISTORY.getCode()) { // 4 pour le component du history
            criterias.setSize(SIZE);
        }
        return daoSavedForm.listRules(criterias);
    }

    @Override
    public List<EbSavedForm> selectListEbSavedFormByCompanie() throws IOException {
        List<EbSavedForm> ebSavedForms = new ArrayList<EbSavedForm>();
        EbUser connectedUser = connectedUserService.getCurrentUser();
        ebSavedForms = ebSavedFormRepository
            .findAllByEbCompNumAndCompagnieOrderByOrdreAsc(
                SavedFormComponent.SAVED_SEARCH_ACTION.getCode(),
                connectedUser.getEbCompagnie());

        for (EbSavedForm ebSavedForm: ebSavedForms) {

            if (ebSavedForm.getActionTrigger() != null) {
                List<Integer> codeActionTrigger = new ArrayList<Integer>();
                String actionTrigger[] = ebSavedForm.getActionTrigger().split(",");

                for (int i = 0; i < actionTrigger.length; i++) {
                    System.err.println(actionTrigger[i]);
                    codeActionTrigger.add(Integer.parseInt(actionTrigger[i]));
                }

                ebSavedForm.setListCodeActionTrigger(codeActionTrigger);
            }

            if (ebSavedForm.getAction() == SavedSearchAction.CHANGE_STATUS.getCode()) {
                List<CodeLibelleDTO> listStatus = serviceListStatique
                    .getListStatusByModule(ebSavedForm.getEbModuleNum());

                for (CodeLibelleDTO Status: listStatus) {

                    if (Status.getCode().toString().equals(ebSavedForm.getActionTargets())) {
                        ebSavedForm.setStatusLibelle(Status.getLibelle());
                    }

                }

            }

            if (ebSavedForm.getAction() == SavedSearchAction.ADD_FLAG.getCode()) {
                String param[] = ebSavedForm.getActionTargets().split(",");
                List<EbFlagDTO> listFlagDTOs = new ArrayList<EbFlagDTO>();

                for (int i = 0; i < param.length; i++) {

                    if (param[i].length() > 0) {
                        Optional<EbFlag> flag = ebFlagRepository.findById(Integer.parseInt(param[i]));

                        if (flag.isPresent()) {
                            EbFlagDTO ebFlagDTO = new EbFlagDTO(flag.get());
                            listFlagDTOs.add(ebFlagDTO);
                        }

                    }

                }

                ebSavedForm.setFlagDtos(listFlagDTOs);
            }

        }

        return ebSavedForms;
    }

    @Override
    public void applyActionToHistory(EbSavedForm ebSavedForm) throws Exception {
        SearchCriteria criterias = parseSavedSearchToSearchCriteria(ebSavedForm.getEbSavedFormNum());
        Integer ebModuleNum = ebSavedForm.getEbModuleNum();

        if (Module.PRICING.getCode().equals(ebModuleNum) || Module.TRANSPORT_MANAGEMENT.getCode().equals(ebModuleNum)) {
            SearchCriteriaPricingBooking criteria = (SearchCriteriaPricingBooking) criterias;
            EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);

            List<EbDemande> demandes = daoPricing.getListEbDemande(criteria, connectedUser);
            demandes.stream().forEach(d -> {
                if (SavedSearchAction.ADD_FLAG
                    .getCode().equals(ebSavedForm.getAction())) d.setListFlag(ebSavedForm.getActionTargets());
            });
            ebDemandeRepository.saveAll(demandes);
        }
        else if (Module.TRACK.getCode().equals(ebModuleNum)) {
            SearchCriteriaTrackTrace criteria = (SearchCriteriaTrackTrace) criterias;
            EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);

            List<EbTtTracing> tracings = daotrack.getListEbTrackTrace(criteria, connectedUser);

            tracings.stream().forEach(t -> {
                if (SavedSearchAction.ADD_FLAG
                    .getCode().equals(ebSavedForm.getAction())) t.setListFlag(ebSavedForm.getActionTargets());
            });
            ebTrackTraceRepository.saveAll(tracings);
        }
        else if (Module.QUALITY_MANAGEMENT.getCode().equals(ebModuleNum)) {
            // TODO
        }
        else if (Module.FREIGHT_AUDIT.getCode().equals(ebModuleNum)) {
            // TODO
            /*
             * List<EbInvoice> invoices =
             * daoInvoice.getListEbInvoice((SearchCriteriaFreightAudit)
             * criteria);
             * invoices.stream().forEach(i ->
             * i.setListFlag(ebSavedForm.getActionTargets()));
             * ebInvoiceRepository.saveAll(invoices);
             */
        }
        else if (Module.FREIGHT_ANALYTICS.getCode().equals(ebModuleNum)) {
            // TODO
        }
        else if (Module.RECEIPT_SCHEDULING.getCode().equals(ebModuleNum)) {
            // TODO
        }
        else if (Module.ORDER_MANAGEMENT.getCode().equals(ebModuleNum)) {
            // TODO
        }

    }

    @Override
    public void applyActionToHistory(Integer ebSavedFormNum) throws Exception {
        EbSavedForm savedForm = ebSavedFormRepository.findById(ebSavedFormNum).get();
        this.applyActionToHistory(savedForm);
    }

    @Override
    public List<EbSavedFormSearchCriteriaDto>
        parseSavedSearch(Integer ebSavedFormNum, Boolean bindValueLabels) throws IOException, ClassNotFoundException {
        EbSavedForm savedForm = ebSavedFormRepository.findById(ebSavedFormNum).get();
        return this.parseSavedSearch(savedForm, bindValueLabels);
    }

    @Override
    public List<EbSavedFormSearchCriteriaDto>
        parseSavedSearch(EbSavedForm savedForm, Boolean bindValueLabels) throws IOException, ClassNotFoundException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode rootTree = mapper.readTree(savedForm.getFormValues());
        List<EbSavedFormSearchCriteriaDto> rList = new ArrayList<>();

        int i = 0;

        while (rootTree.get(i) != null) {
            JsonNode jsonItem = rootTree.get(i);

            if ((jsonItem.get("default") != null && !jsonItem.get("default").isNull()
                || jsonItem.get("listItems") != null && !jsonItem.get("listItems").isNull())
                && jsonItem.get("name") != null && !jsonItem.get("name").isNull()) {
                // Preparing values
                String field = jsonItem.get("name").asText();
                JsonNode valueNode;
                String sourceField;

                if (jsonItem.get("listItems") != null && !jsonItem.get("listItems").isNull()) {
                    valueNode = jsonItem.get("listItems");
                    sourceField = "listItems";
                }
                else {
                    valueNode = jsonItem.get("default");
                    sourceField = "default";
                }

                final Object valueObject = mapper.convertValue(valueNode, Object.class);
                EbSavedFormSearchCriteriaDto rItem = new EbSavedFormSearchCriteriaDto(field, valueObject);

                // Check field validity
                Boolean isValid = true;

                if (valueObject instanceof String && ((String) valueObject).length() == 0) {
                    isValid = false;
                }

                // Do binding
                if (isValid) {
                    // Field Label
                    String fieldLabel;

                    if (jsonItem.get("label") != null && !jsonItem.get("label").isNull()) {
                        fieldLabel = jsonItem.get("label").asText();
                    }
                    else {
                        fieldLabel = field;
                    }

                    rItem.setFieldLibelle(fieldLabel);

                    // Value label
                    String valueLabel = valueObject.toString();

                    if (bindValueLabels) {

                        // List Statut
                        if ("listStatut".equals(field)) {
                            List<CodeLibelleDTO> listStatusForModule = serviceListStatique
                                .getListStatusByModule(savedForm.getEbModuleNum());
                            String[] listStatusCode = valueNode.toString().replace("[", "").replace("]", "").split(",");
                            valueLabel = "";

                            for (int li = 0; li < listStatusCode.length; li++) {
                                final String currentStatusCode = listStatusCode[li];
                                Optional<CodeLibelleDTO> foundStatus = listStatusForModule
                                    .stream().filter(s -> s.getCode().toString().equals(currentStatusCode)).findFirst();

                                if (foundStatus.isPresent()) {
                                    if (valueLabel.length() > 0) valueLabel += ", ";
                                    valueLabel += foundStatus.get().getLibelle();
                                }

                            }

                        }

                        // List Mode Transport
                        else if ("listModeTransport".equals(field)) {
                            List<Enumeration.ModeTransport> allModeTransport = Arrays.<Enumeration
                                .ModeTransport> asList(Enumeration.ModeTransport.values());
                            String[] listModeTransport = valueNode
                                .toString().replace("[", "").replace("]", "").split(",");
                            valueLabel = "";

                            for (int li = 0; li < listModeTransport.length; li++) {
                                final String currentCode = String.valueOf(listModeTransport[li]);
                                Optional<ModeTransport> found = allModeTransport
                                    .stream().filter(s -> s.getCode().toString().equals(currentCode)).findFirst();

                                if (found.isPresent()) {
                                    if (valueLabel.length() > 0) valueLabel += ", ";
                                    valueLabel += found.get().getLibelle();
                                }

                            }

                        }
                        // List Type of request
                        else if ("listTypeDemande".equals(field)) {
                            List<Enumeration.TypeDemande> allTypeDemande = Arrays.<Enumeration
                                .TypeDemande> asList(Enumeration.TypeDemande.values());
                            String[] listTypeDemande = valueNode
                                .toString().replace("[", "").replace("]", "").split(",");
                            valueLabel = "";

                            for (int li = 0; li < listTypeDemande.length; li++) {
                                final String currentCode = String.valueOf(listTypeDemande[li]);
                                Optional<Enumeration.TypeDemande> found = allTypeDemande
                                    .stream().filter(s -> s.getCode().toString().equals(currentCode)).findFirst();

                                if (found.isPresent()) {
                                    if (valueLabel.length() > 0) valueLabel += ", ";
                                    valueLabel += found.get().getLibelle();
                                }

                            }

                        }
                        // List Destination && origin
                        else if ("listDestinations".equals(field) || "listOrigins".equals(field)) {
                            valueLabel = "";
                            String[] list = valueNode.toString().replace("[", "").replace("]", "").split(",");

                            for (int li = 0; li < list.length; li++) {
                                EcCountry country = ecCountryRepository.findById(Integer.parseInt(list[li])).get();

                                if (country != null) {
                                    if (valueLabel.length() > 0) valueLabel += ", ";
                                    valueLabel += country.getLibelle();
                                }

                            }

                        }
                        // List Incoterm
                        // else if ("listIncoterm".equals(field)) {
                        // valueLabel = "";
                        // String[] list = valueNode.toString().replace("[",
                        // "").replace("]", "").split(",");
                        // for (int li = 0; li < list.length; li++) {
                        // EbIncoterm incoterm =
                        // ebIncotermRepository.findById(Integer.parseInt(list[li])).get();
                        // if (incoterm != null) {
                        // if (valueLabel.length() > 0)
                        // valueLabel += ", ";
                        // valueLabel += incoterm.getLibelle();
                        // }
                        // }
                        // }
                        // Any field with source from listItems
                        else if ("listItems".equals(sourceField)) {
                            valueLabel = "";
                            int listItemIndex = 0;

                            while (valueNode.get(listItemIndex) != null) {
                                String subValueText = valueNode.get(listItemIndex).get("libelle").asText();
                                if (valueLabel.length() > 0) valueLabel += ", ";
                                valueLabel += subValueText;
                                listItemIndex++;
                            }

                        }
                        // Any field of kind list retrieved from field default
                        else if (field.contains("list")) {
                            valueLabel = valueObject.toString().replace("[", "").replace("]", "");
                        }

                        rItem.setValueLibelle(valueLabel.trim());
                    }

                    rList.add(rItem);
                }

            }

            i++;
        }

        return rList;
    }

    @Override
    public SearchCriteria parseSavedSearchToSearchCriteria(Integer ebSavedFormNum)
        throws IOException,
        ClassNotFoundException,
        InstantiationException,
        IllegalAccessException {
        EbSavedForm savedForm = ebSavedFormRepository.findById(ebSavedFormNum).get();
        return this.parseSavedSearchToSearchCriteria(savedForm);
    }

    @Override
    public SearchCriteria parseSavedSearchToSearchCriteria(EbSavedForm ebSavedForm)
        throws IOException,
        ClassNotFoundException,
        InstantiationException,
        IllegalAccessException {
        List<EbSavedFormSearchCriteriaDto> parsedSearch = this.parseSavedSearch(ebSavedForm, false);

        // Preparing SearchCriteria object
        SearchCriteria criteria = serviceListStatique
            .getSearchCriteriaClassByModule(ebSavedForm.getEbModuleNum()).newInstance();

        // Injecting data
        Class<? extends SearchCriteria> criteriaClass = criteria.getClass();

        for (EbSavedFormSearchCriteriaDto sc: parsedSearch) {

            try {
                Field destField = criteriaClass.getDeclaredField(sc.getKey());
                destField.setAccessible(true);
                destField.set(criteria, sc.getValue());
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }

        return criteria;
    }
}
