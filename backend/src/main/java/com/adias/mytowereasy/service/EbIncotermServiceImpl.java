package com.adias.mytowereasy.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbIncoterm;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class EbIncotermServiceImpl extends MyTowerService implements EbIncotermService {
    @Override
    public List<EbIncoterm> getListIncoterms(SearchCriteria criteria) {
        return daoIncoterm.getListIncoterms(criteria);
    }

    @Override
    public Long getListIncotermsCount(SearchCriteria criteria) {
        return daoIncoterm.getListIncotermsCount(criteria);
    }

    @Override
    public EbIncoterm updateIncoterm(EbIncoterm incoterm) {
        return ebIncotermRepository.save(incoterm);
    }

    @Override
    public Boolean deleteIncoterm(Integer Id) {
        EbIncoterm incoterm = ebIncotermRepository.getOne(Id);
        ebIncotermRepository.delete(incoterm);
        return true;
    }
}
