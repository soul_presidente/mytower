/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.airbus.service.EbTtCompanyPslService;
import com.adias.mytowereasy.cronjob.tt.model.TmpEvent;
import com.adias.mytowereasy.cronjob.tt.util.TmpInfosPsl;
import com.adias.mytowereasy.dao.DaoPslApp;
import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.service.DeliveryService;
import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.dto.EbTTPslAppAndEventsProjection;
import com.adias.mytowereasy.dto.EbTtCompagnyPslLightDto;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.kafka.model.TransportRequestKafkaObject;
import com.adias.mytowereasy.kafka.service.KafkaObjectService;
import com.adias.mytowereasy.kafka.service.ProducerService;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.CREnumeration.RuleCategory;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.*;
import com.adias.mytowereasy.model.TtEnumeration.NatureDateEvent;
import com.adias.mytowereasy.model.TtEnumeration.NatureReference;
import com.adias.mytowereasy.model.TtEnumeration.TtPsl;
import com.adias.mytowereasy.model.TtEnumeration.TypeEvent;
import com.adias.mytowereasy.model.qm.EbQmDeviation;
import com.adias.mytowereasy.model.qm.EbQmIncident;
import com.adias.mytowereasy.model.qm.EbQmIncidentLine;
import com.adias.mytowereasy.model.tt.*;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.util.UnitRefSharingParams;
import com.adias.mytowereasy.util.tt.TracingUpdatePslResult;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;
import com.adias.mytowereasy.utils.search.SearchCriteriaTrackTrace;


@Service
public class TrackServiceImpl extends MyTowerService implements TrackService, CommonFlagSevice {
    private static final Logger logger = LoggerFactory.getLogger(TrackServiceImpl.class);
    private final List<EbTTPslApp> DEFAULT_PSL_LIST = Arrays.asList(new EbTTPslApp("WPU", "Waiting for pickup"));

    @Autowired
    ListStatiqueService listStatiqueService;

    @Autowired
    FlagService flagService;

    @Autowired
    DaoPslApp daoPslApp;

    @Autowired
    EbDemandeRepository demandeRepository;

    @Autowired
    EbLivraisonRepository ebLivraisonRepository;

    @Autowired
    DeliveryService deliveryService;

    @Autowired
    KafkaObjectService kafkaObjectService;

    @Autowired
    ProducerService producerService;

    @Autowired
    private ControlRuleService controlRuleService;

    @Autowired
    EbTtCompanyPslService companyPslService;

    @Autowired
    CompagnieService compagnieService;

    private Integer tmpEventRejectNum = null;

    @SuppressWarnings("unchecked")
    @Transactional
    @Override
    public List<EbTtTracing> insertEbTrackTrace(EbDemande ebDemande) {
        EbTypeFlux typeOfFlux = null;
        Integer trackingLevel = null;
        List<EbTtTracing> listEbTrack = new ArrayList<EbTtTracing>();

        try {

            if (ebDemande.get_exEbDemandeTransporteurFinal() == null ||
                ebDemande.get_exEbDemandeTransporteurFinal().getxEbCompagnie() == null ||
                ebDemande.get_exEbDemandeTransporteurFinal().getxEbCompagnie().getEbCompagnieNum() == null) {
                SearchCriteriaPricingBooking transportCriteria = new SearchCriteriaPricingBooking();
                transportCriteria.setStatus(Enumeration.StatutDemande.WPU.getCode());
                transportCriteria.setEbDemandeNum(ebDemande.getEbDemandeNum());
                transportCriteria.setForCotation(true);
                List<ExEbDemandeTransporteur> listDemandeTrans = daoPricing.getListExEbTransporteur(transportCriteria);

                if (listDemandeTrans != null && listDemandeTrans.size() > 0) {
                    ebDemande.set_exEbDemandeTransporteurFinal(listDemandeTrans.get(0));
                }

            }

            List<EbTtCompanyPsl> listPsl = null;
            EbTtSchemaPsl schemaPsl = ebDemandeRepository.selectSchemaPslByEbDemandeNum(ebDemande.getEbDemandeNum());

            if (ebDemande.getxEbTypeFluxNum() != null) {
                typeOfFlux = ebTypeFluxRepository.findOneByEbTypeFluxNum(ebDemande.getxEbTypeFluxNum());
                trackingLevel = typeOfFlux.getTrackingLevel();

                if (schemaPsl == null && typeOfFlux != null) {
                    String[] listSchemaPslNum = typeOfFlux.getListEbTtSchemaPsl().split(":");

                    Integer schemaPslNum = (listSchemaPslNum.length > 1) ?
                        NumberUtils.toInt(listSchemaPslNum[1]) :
                        null;

                    if (schemaPslNum != null) {
                        schemaPsl = ebTtSchemaPslRepository.findOneByEbTtSchemaPslNum(schemaPslNum);
                        schemaPsl.setxEbCompagnie(null);
                        schemaPsl = new EbTtSchemaPsl(schemaPsl);
                    }

                }

            }

            if (schemaPsl != null) {
                listPsl = schemaPsl
                    .getListPsl().stream().filter(it -> it.getIsChecked() != null && it.getIsChecked())
                    .collect(Collectors.toList());
            }

            if (((ebDemande.getFlagEbtrackTrace() == null) || !ebDemande.getFlagEbtrackTrace()) &&
                listPsl != null && !listPsl.isEmpty()) {
                SearchCriteriaPricingBooking transportCriteria = new SearchCriteriaPricingBooking();
                transportCriteria.setForCotation(true);

                if (ebDemande.get_exEbDemandeTransporteurFinal() != null) {
                    transportCriteria
                        .setEbUserNum(ebDemande.get_exEbDemandeTransporteurFinal().getxTransporteur().getEbUserNum());
                }
                else {
                    transportCriteria.setEbUserNum(ebDemande.getxEbTransporteur());
                }

                transportCriteria.setEbDemandeNum(ebDemande.getEbDemandeNum());
                // - afficher la cotation du transporteur
                List<ExEbDemandeTransporteur> listDemandeTrans = daoPricing.getListExEbTransporteur(transportCriteria);

                if (ebDemande.getxEbUserCt() == null ||
                    ebDemande.getxEbUserCt().getEbUserNum() == null || ebDemande.getxEbEtablissementCt() == null ||
                    ebDemande.getxEbEtablissementCt().getEbEtablissementNum() == null ||
                    ebDemande.getxEbCompagnieCt() == null ||
                    ebDemande.getxEbCompagnieCt().getEbCompagnieNum() == null) {
                    EbUser userCt = ebDemandeRepository.selectCtDataByEbDemandeNum(ebDemande.getEbDemandeNum());

                    if (userCt != null && userCt.getEbUserNum() != null) {
                        ebDemande.setxEbUserCt(new EbUser(userCt.getEbUserNum()));
                        ebDemande
                            .setxEbEtablissementCt(
                                new EbEtablissement(userCt.getEbEtablissement().getEbEtablissementNum()));
                        ebDemande.setxEbCompagnieCt(new EbCompagnie(userCt.getEbCompagnie().getEbCompagnieNum()));
                    }

                }

                /* CREATION DU OU DES TRACING(S) LIE A LA DEMANDE */
                List<EbTtEvent> listEvents = new ArrayList<EbTtEvent>();

                if (trackingLevel == TrackingLevel.TRANSPORT.getCode()) {
                    /* Niveau de tracking par Transport */
                    createTracingByTrackingLevel(ebDemande, listEbTrack, listPsl, listEvents, null);
                }
                else {
                    /* Niveau de tracking par marchandise */
                    // Récuperation de la liste des marchandises
                    List<EbMarchandise> listMarchandise = ebDemande.getListMarchandises();
                    if (listMarchandise == null ||
                        listMarchandise.size() <= 0) listMarchandise = ebMarchandiseRepository
                            .findAllByEbDemande_ebDemandeNum(ebDemande.getEbDemandeNum());

                    if (listMarchandise.isEmpty()) {
                        listMarchandise.add(new EbMarchandise());
                    }

                    int nbTracingCreated = 0;

                    for (EbMarchandise mr: listMarchandise) {

                        if (isMarchandiseEligibleForTrackingLevel(mr, trackingLevel)) {
                            nbTracingCreated++;
                            createTracingByTrackingLevel(ebDemande, listEbTrack, listPsl, listEvents, mr);
                        }

                    }

                    if (nbTracingCreated == 0) {
                        // trackingLevel = TrackingLevel.TRANSPORT
                        createTracingByTrackingLevel(ebDemande, listEbTrack, listPsl, listEvents, null);
                    }

                }

                ebTtEventRepository.saveAll(listEvents);
                ebDemandeRepository.updateflagEbtrackTrace(ebDemande.getEbDemandeNum(), true);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listEbTrack;
    }

    private void createTracingByTrackingLevel(
        EbDemande ebDemande,
        List<EbTtTracing> listEbTrack,
        List<EbTtCompanyPsl> listPsl,
        List<EbTtEvent> listEvents,
        EbMarchandise marchandise) {
        Map<String, Object> tracingData = generateTracingForEbDemande(ebDemande, marchandise, listPsl);
        EbTtTracing ebTrack = (EbTtTracing) tracingData.get("ebTrack");
        List<EbTtEvent> listEventsForCurrentTracing = (List<EbTtEvent>) tracingData.get("listEvents");
        ebTrack = ebTrackTraceRepository.save(ebTrack);

        listEbTrack.add(ebTrack);
        listEvents.addAll(listEventsForCurrentTracing);
    }

    @Override
    public EbDemande handlePsl(EbDemande demande) {
        EbTtSchemaPsl schemaPsl = null;
        EbTypeFlux typeOfFlux = null;

        if (demande.getxEbTypeFluxNum() != null) {
            typeOfFlux = ebTypeFluxRepository.findOneByEbTypeFluxNum(demande.getxEbTypeFluxNum());

            if (typeOfFlux != null) {
                String[] listSchemaPslNum = typeOfFlux.getListEbTtSchemaPsl().split(":");

                Integer schemaPslNum = (listSchemaPslNum.length > 1) ? NumberUtils.toInt(listSchemaPslNum[1]) : null;

                if (schemaPslNum != null) {
                    EbTtSchemaPsl schema = ebTtSchemaPslRepository.findOneByEbTtSchemaPslNum(schemaPslNum);
                    schemaPsl = new EbTtSchemaPsl(schema);
                    schemaPsl.setxEbCompagnie(null);
                }

            }

        }

        if (schemaPsl != null) {
            List<EbTtCompanyPsl> listPSL = new ArrayList<EbTtCompanyPsl>();

            List<EbTtCompanyPsl> linkedListSchemaPsl = schemaPsl.getListPsl();
            ObjectMapper mapper = new ObjectMapper();
            List<EbTtCompanyPsl> listSchemaPsl = mapper
                .convertValue(linkedListSchemaPsl, new TypeReference<List<EbTtCompanyPsl>>() {
                });

            for (EbTtCompanyPsl psl: listSchemaPsl) {
                psl.setIsChecked(true);

                if (psl != null && psl.getEndPsl() != null && psl.getEndPsl()) {
                    demande.setCodeAlphaLastPsl(psl.getCodeAlpha());
                    demande.setCodeLastPsl(psl.getEbTtCompanyPslNum());
                    demande.setLibelleLastPsl(psl.getLibelle());
                }

                if (demande.getxEcStatut() >= StatutDemande.WPU.getCode()) {

                    if (demande.getDatePickupTM() != null) {
                        if (psl.getCodeAlpha().contentEquals(TtPsl.PICKUP.getCodeAlpha()) &&
                            psl.getIsChecked() != null &&
                            psl.getIsChecked()) psl.setExpectedDate(demande.getDatePickupTM());
                    }

                    if (demande.getFinalDelivery() != null) {
                        if (psl.getCodeAlpha().contentEquals(TtPsl.DELIVERY.getCodeAlpha()) &&
                            psl.getIsChecked() != null &&
                            psl.getIsChecked()) psl.setExpectedDate(demande.getFinalDelivery());
                    }

                    if (demande.getEtd() != null) {
                        if (psl.getCodeAlpha().contentEquals(TtPsl.DEPARTURE.getCodeAlpha()) &&
                            psl.getIsChecked() != null && psl.getIsChecked()) psl.setExpectedDate(demande.getEtd());
                    }

                    if (demande.getEta() != null) {
                        if (psl.getCodeAlpha().contentEquals(TtPsl.ARRIVAL.getCodeAlpha()) &&
                            psl.getIsChecked() != null && psl.getIsChecked()) psl.setExpectedDate(demande.getEta());
                    }

                }

                listPSL.add(psl);
            }

            schemaPsl.setListPsl(listPSL);
            schemaPsl = new EbTtSchemaPsl(schemaPsl);
            demande.setxEbSchemaPsl(schemaPsl);
        }

        demandeRepository.save(demande);
        return demande;
    }

    Map<String, Object>
        generateTracingForEbDemande(EbDemande ebDemande, EbMarchandise mr, List<EbTtCompanyPsl> listPsl) {
        List<EbTtEvent> listEvents = new ArrayList<EbTtEvent>();
        EbTtTracing ebTrack = new EbTtTracing(ebDemande, mr);

        EbParty origin = ebDemandeRepository.selectEbPartyOriginByEbDemandeNum(ebDemande.getEbDemandeNum());
        EbParty dest = ebDemandeRepository.selectEbPartyDestByEbDemandeNum(ebDemande.getEbDemandeNum());

        EbUser connectedUser = connectedUserService.getCurrentUser();
        Date dateCreation = new Date();

        ebTrack.setOriginPlace(origin.getCompany());
        ebTrack.setDestinationPlace(dest.getCompany());
        ebTrack.setDateCreation(dateCreation);

        List<EbTTPslApp> listPslApp = new ArrayList<EbTTPslApp>();

        for (EbTtCompanyPsl psl: listPsl) {
            EbTTPslApp ebPslapp = new EbTTPslApp();
            ebPslapp.setCompanyPsl(psl);
            ebPslapp.setxEbTrackTrace(ebTrack);
            ebPslapp.setxEbUser(new EbUser(ebDemande.getUser()));
            ebPslapp.setDateCreation(dateCreation);
            ebPslapp.setCodePslCode(psl.getEbTtCompanyPslNum());
            ebPslapp.setOrderpsl(psl.getOrder());
            ebPslapp.setCodeAlpha(psl.getCodeAlpha());
            ebPslapp.setLibelle(psl.getLibelle());

            List<EbTtCompagnyPslLightDto> listCompPsl = null;

            if (ebDemande.get_exEbDemandeTransporteurFinal() != null) {
                listCompPsl = ebDemande.get_exEbDemandeTransporteurFinal().getListEbTtCompagniePsl();
            }

            EbTtCompagnyPslLightDto compPslDto = null;

            if (listCompPsl != null) {
                compPslDto = listCompPsl
                    .stream().filter(it -> it.getCodeAlpha().contentEquals(psl.getCodeAlpha())).findFirst()
                    .orElse(null);
            }

            if (compPslDto != null) {
                ebPslapp.setDateNegotiation(compPslDto.getExpectedDate());

                // generer event pour negociated date
                if (ebPslapp.getDateNegotiation() != null) {
                    EbTtEvent ttEvent = new EbTtEvent();
                    ttEvent.setDateCreation(new Date());
                    ttEvent.setxEbTtPslApp(ebPslapp);
                    ttEvent.setDateEvent(ebPslapp.getDateNegotiation());
                    ttEvent.setxEbTtTracing(ebTrack);
                    ttEvent.setNatureDate(TtEnumeration.NatureDateEvent.DATE_NEGOTIATED.getCode());
                    ttEvent.setxEbUser(new EbUser(connectedUser.getEbUserNum()));
                    ttEvent
                        .setxEbEtablissement(
                            new EbEtablissement(connectedUser.getEbEtablissement().getEbEtablissementNum()));
                    ttEvent.setxEbCompagnie(new EbCompagnie(connectedUser.getEbCompagnie().getEbCompagnieNum()));
                    listEvents.add(ttEvent);
                }

            }

            listPslApp.add(ebPslapp);
        }

        ebTrack.setxEbSchemaPsl(ebDemande.getxEbSchemaPsl());
        ebTrack.setListEbPslApp(listPslApp);
        ebTrack.setDateCreation(new Date());

        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("ebTrack", ebTrack);
        result.put("listEvents", listEvents);
        return result;
    }

    Boolean isMarchandiseEligibleForTrackingLevel(EbMarchandise mr, Integer trackingLevel) {
        boolean isMarchandiseEligible = false;

        if (trackingLevel == null) {
            trackingLevel = TrackingLevel.UNITS.getCode();
        }

        if (trackingLevel == TrackingLevel.UNITS.getCode()) {
            isMarchandiseEligible = true;
        }
        else if (trackingLevel == TrackingLevel.UNIT_WITHOUT_PARENT.getCode() && mr.getParent() == null) {
            isMarchandiseEligible = true;
        }
        else if (trackingLevel == TrackingLevel.ARTICLE.getCode() &&
            mr.getxEbTypeConteneur() == TypeContainer.ARTICLE.getKey()) {
                isMarchandiseEligible = true;
            }
        else if (trackingLevel == TrackingLevel.COLIS.getCode() &&
            mr.getxEbTypeConteneur() == TypeContainer.PARCEL.getKey()) {
                isMarchandiseEligible = true;
            }
        else if (trackingLevel == TrackingLevel.PALETTE.getCode() &&
            mr.getxEbTypeConteneur() == TypeContainer.PALETTE.getKey()) {
                isMarchandiseEligible = true;
            }
        else if (trackingLevel == TrackingLevel.CONTAINER.getCode() &&
            mr.getxEbTypeConteneur() == TypeContainer.CONTAINER.getKey()) {
                isMarchandiseEligible = true;
            }

        return isMarchandiseEligible;
    }

    @Override
    public int getCountListEbTrackTrace(SearchCriteriaTrackTrace criteria) throws Exception {
        long count = daotrack.getCountListEbTrackTrace(criteria);

        return (int) count;
    }

    @Override
    public List<EbTtTracing> getListEbTrackTrace(SearchCriteriaTrackTrace criteria) throws Exception {
        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);
        List<ExEbDemandeTransporteur> listTranspoFinal = null;
        List<EbTtTracing> listTrack = daotrack.getListEbTrackTrace(criteria, connectedUser);
        List<Integer> listEbDemandeNum = listTrack
            .stream().map(it -> it.getxEbDemande().getEbDemandeNum()).collect(Collectors.toList());

        if (listEbDemandeNum != null && listEbDemandeNum.size() > 0) {
            listTranspoFinal = exEbDemandeTransporteurRepository
                .findByStatusQuote(Enumeration.StatutCarrier.FIN.getCode(), listEbDemandeNum);
        }

        for (EbTtTracing d: listTrack) {
            ExEbDemandeTransporteur tr = null;

            if (listTranspoFinal != null && listTranspoFinal.size() > 0) {
                tr = listTranspoFinal
                    .stream()
                    .filter(it -> it.getxEbDemande().getEbDemandeNum().equals(d.getxEbDemande().getEbDemandeNum()))
                    .findFirst().orElse(null);
            }

            if (tr != null) {
                d.getxEbDemande().setExEbDemandeTransporteurs(new ArrayList<ExEbDemandeTransporteur>());
                d.getxEbDemande().getExEbDemandeTransporteurs().add(tr);
            }

            String listFlagIcon = d.getListFlag() != null ? d.getListFlag().trim() : null;
            if (listFlagIcon != null && !listFlagIcon.isEmpty()) d
                .setListEbFlagDTO(EbFlagDTO.sortByName(flagService.getListFlagDTOFromCodes(listFlagIcon)));

            List<EbMarchandise> listMarchandise = ebMarchandiseRepository
                .findAllByEbDemande_ebDemandeNum(d.getxEbDemande().getEbDemandeNum());

            List<EbMarchandise> unitList = listMarchandise.size() > UnitRefSharingParams.MAX_UNITS_REFS_SUPPORTED ?
                listMarchandise.subList(0, UnitRefSharingParams.MAX_UNITS_REFS_SUPPORTED) :
                listMarchandise;

            String suffix = unitList.size() > UnitRefSharingParams.MAX_UNITS_REFS_SUPPORTED ?
                UnitRefSharingParams.UNITS_REF_SUFFIX :
                UnitRefSharingParams.EMPTY_STRING;
            String defaultUnitsRefs = unitList
                .stream().filter(u -> u.getUnitReference() != null).map(EbMarchandise::getUnitReference).collect(
                    Collectors
                        .joining(UnitRefSharingParams.UNITS_REF_SEPARATOR, UnitRefSharingParams.EMPTY_STRING, suffix));

            for (EbMarchandise ebMarchandise: unitList) {
                EbDemande ebDemande = ebMarchandise.getEbDemande();

                if (ebDemande != null) {
                    EbTypeFlux typeOfFlux = ebTypeFluxRepository.findOneByEbTypeFluxNum(ebDemande.getxEbTypeFluxNum());

                    if (TrackingLevel.TRANSPORT
                        .getCode().equals(typeOfFlux.getTrackingLevel())) d.setCustomerReference(defaultUnitsRefs);
                }

            }

        }

        return listTrack;
    }

    @Override
    public EbTtTracing getEbTracing(SearchCriteriaTrackTrace criteria) {
        EbTtTracing ebTracing = daotrack.getEbTrackTrace(criteria);

        if (ebTracing != null && ebTracing.getEbTtTracingNum() != null) {
            String listFlagIcon = ebTracing.getListFlag() != null ? ebTracing.getListFlag().trim() : null;
            ebTracing
                .setListEbFlagDTO(
                    listFlagIcon != null && !listFlagIcon.isEmpty() ?
                        EbFlagDTO.sortByName(flagService.getListFlagDTOFromCodes(listFlagIcon)) :
                        null);
        }

        return ebTracing;
    }

    /*
     * cette methode permet l'ajout des events et la MAJ du PSL soit par l'app
     * ou bien automatiquement a partir des données creer par EDI (par UR /
     * TrRef)
     */
    @Transactional
    private EbTtEvent updateEbPslAppAndAddEvent(
        EbTTPslApp ebPslApp,
        Boolean ignoreIfNull,
        EbDemande demande,
        StringBuilder libelle,
        EbUser user,
        TmpInfosPsl tmpInfosPsl) {
        List<EbTtEvent> listEvents = null;
        List<EbTtCompanyPsl> listPsl = null;

        if (user == null) {
            user = connectedUserService.getCurrentUser();
        }

        try {

            // MAJ du psl
            if (ebPslApp.getxEcCountry() != null && ebPslApp.getxEcCountry().getEcCountryNum() == null) {
                ebPslApp.setxEcCountry(null);
            }

            EbTTPslApp oldPslApp = null;
            Integer nombreEvents = 1;
            Boolean isGenerateEventByTrRefOrCustomerRef = tmpInfosPsl != null &&
                (tmpInfosPsl.getNatureReference().equals(NatureReference.TRANSPORT_REF.getCode()) ||
                    tmpInfosPsl.getNatureReference().equals(NatureReference.CUSTOMER_REF.getCode()));

            Integer natureEventDate = null;

            if (ebPslApp.getDateActuelle() != null) {
                natureEventDate = NatureDateEvent.DATE_ACTUAL.getCode();
            }
            else if (ebPslApp.getDateNegotiation() != null) {
                natureEventDate = NatureDateEvent.DATE_NEGOTIATED.getCode();
            }
            else {
                natureEventDate = NatureDateEvent.DATE_ESTIMATED.getCode();
            }

            // dans le cas d'un psl ajouté par l'app ou bien psl generer par EDI
            // avec unit ref
            if (tmpInfosPsl == null || tmpInfosPsl.getNatureReference().equals(NatureReference.UNIT_REF.getCode())) {
                // load old psl and set country & city if ignoreIfNull is true
                oldPslApp = ebPslAppRepository
                    .findFirstByxEbTrackTraceEbTtTracingNumAndCodeAlpha(
                        ebPslApp.getxEbTrackTrace().getEbTtTracingNum(),
                        ebPslApp.getCompanyPsl().getCodeAlpha());

                if (oldPslApp == null) return null;

                if (ebPslApp.getxEcCountry() != null) oldPslApp.setxEcCountry(ebPslApp.getxEcCountry());
                if (ebPslApp.getCity() != null) oldPslApp.setCity(ebPslApp.getCity());
                if (ebPslApp.getCommentaire() != null) oldPslApp.setCommentaire(ebPslApp.getCommentaire());

                if (ebPslApp.getDateActuelle() != null) {
                    oldPslApp.setDateActuelle(ebPslApp.getDateActuelle());
                    oldPslApp.setValidated(true);
                }

                if (ebPslApp.getDateEstimee() != null) {
                    oldPslApp.setDateEstimee(ebPslApp.getDateEstimee());
                }

                if (ebPslApp.getDateNegotiation() != null) {
                    oldPslApp.setDateNegotiation(ebPslApp.getDateNegotiation());
                }

                if (ebPslApp.getFuseauHoraire() != null) oldPslApp.setFuseauHoraire(ebPslApp.getFuseauHoraire());

                if (oldPslApp.getxEbTrackTrace() == null || ebPslApp.getxEbTrackTrace().getEbTtTracingNum() == null) {
                    ebPslApp.setxEbTrackTrace(new EbTtTracing());
                    ebPslApp.getxEbTrackTrace().setEbTtTracingNum(ebPslApp.getxEbTrackTrace().getEbTtTracingNum());
                }

                oldPslApp.setQuantity(ebPslApp.getQuantity());

                oldPslApp.setValidateByChamp(ebPslApp.getValidateByChamp());

                ebPslAppRepository.save(oldPslApp);
            }
            else {

                if (tmpInfosPsl.getNatureReference().equals(NatureReference.TRANSPORT_REF.getCode())) {

                    if (ebPslApp.getDateActuelle() != null) {
                        ebPslAppRepository
                            .updateEbPslAppByTransportRefAndDateActual(
                                tmpInfosPsl.getRefTransport(),
                                ebPslApp.getCompanyPsl().getCodeAlpha(),
                                ebPslApp.getDateActuelle(),
                                ebPslApp.getCommentaire(),
                                ebPslApp.getCity(),
                                ebPslApp.getxEcCountry().getEcCountryNum(),
                                true // càd que validated = true
                            );
                    }

                    if (ebPslApp.getDateEstimee() != null) {
                        ebPslAppRepository
                            .updateEbPslAppByTransportRefAndDateEstimated(
                                tmpInfosPsl.getRefTransport(),
                                ebPslApp.getCompanyPsl().getCodeAlpha(),
                                ebPslApp.getDateEstimee(),
                                ebPslApp.getCommentaire(),
                                ebPslApp.getCity(),
                                ebPslApp.getxEcCountry().getEcCountryNum());
                    }

                    if (ebPslApp.getDateNegotiation() != null) {
                        ebPslAppRepository
                            .updateEbPslAppByTransportRefAndDateNegotiation(
                                tmpInfosPsl.getRefTransport(),
                                ebPslApp.getCompanyPsl().getCodeAlpha(),
                                ebPslApp.getDateNegotiation(),
                                ebPslApp.getCommentaire(),
                                ebPslApp.getCity(),
                                ebPslApp.getxEcCountry().getEcCountryNum());
                    }

                }
                else if (tmpInfosPsl.getNatureReference().equals(NatureReference.CUSTOMER_REF.getCode())) {

                    if (ebPslApp.getDateActuelle() != null)

                    {
                        ebPslAppRepository
                            .updateEbPslAppByCustomerRefAndDateActual(
                                tmpInfosPsl.getCustomerReference(),
                                ebPslApp.getCompanyPsl().getCodeAlpha(),
                                ebPslApp.getDateActuelle(),
                                ebPslApp.getCommentaire(),
                                ebPslApp.getCity(),
                                ebPslApp.getxEcCountry().getEcCountryNum(),
                                true // càd que validated = true
                        );
                    }

                    if (ebPslApp.getDateEstimee() != null) {
                        ebPslAppRepository
                            .updateEbPslAppByCustomerRefAndDateEstimated(
                                tmpInfosPsl.getCustomerReference(),
                                ebPslApp.getCompanyPsl().getCodeAlpha(),
                                ebPslApp.getDateEstimee(),
                                ebPslApp.getCommentaire(),
                                ebPslApp.getCity(),
                                ebPslApp.getxEcCountry().getEcCountryNum());
                    }

                    if (ebPslApp.getDateNegotiation() != null) {
                        ebPslAppRepository
                            .updateEbPslAppByCustomerRefAndDateNegotiation(
                                tmpInfosPsl.getCustomerReference(),
                                ebPslApp.getCompanyPsl().getCodeAlpha(),
                                ebPslApp.getDateNegotiation(),
                                ebPslApp.getCommentaire(),
                                ebPslApp.getCity(),
                                ebPslApp.getxEcCountry().getEcCountryNum());
                    }

                }

                nombreEvents = tmpInfosPsl.getListPslByTrRefOrCustomerRefAndCodeAlpha().size();
            }

            if (oldPslApp != null) {
                ebPslApp = oldPslApp;
            }

            listEvents = new ArrayList();

            for (int i = 0; i < nombreEvents; i++) {
                // adding event
                EbTtEvent ttEvent = new EbTtEvent();

                // ca peut servir d'avoir le tracing sur le tmpevent mais
                // generalement un event mettra à jour un ou plusieurs tracing
                if (tmpInfosPsl != null) {
                    ttEvent.setxEbTmptEvent(new TmpEvent(tmpInfosPsl.getTmpEventNum()));
                }

                ttEvent.setQuantity(ebPslApp.getQuantity());
                ttEvent.setCommentaire(ebPslApp.getCommentaire());
                ttEvent.setDateCreation(new Date());
                Date dateEvent = null;

                if (natureEventDate.equals(NatureDateEvent.DATE_ACTUAL.getCode())) {
                    dateEvent = ebPslApp.getDateActuelle();
                }
                else if (natureEventDate.equals(NatureDateEvent.DATE_NEGOTIATED.getCode())) {
                    dateEvent = ebPslApp.getDateNegotiation();
                }
                else {
                    dateEvent = ebPslApp.getDateEstimee();
                }

                ttEvent.setDateEvent(dateEvent);
                ttEvent.setTypeEvent(TtEnumeration.TypeEvent.EVENT_PSL.getCode());

                ttEvent.setNatureDate(natureEventDate);

                ttEvent
                    .setxEbTtPslApp(
                        new EbTTPslApp(
                            isGenerateEventByTrRefOrCustomerRef ?
                                tmpInfosPsl.getListPslByTrRefOrCustomerRefAndCodeAlpha().get(i).getEbTtPslAppNum() :
                                ebPslApp.getEbTtPslAppNum()));

                ttEvent
                    .setxEbTtTracing(
                        new EbTtTracing(
                            isGenerateEventByTrRefOrCustomerRef ?
                                tmpInfosPsl
                                    .getListPslByTrRefOrCustomerRefAndCodeAlpha().get(i).getxEbTrackTrace()
                                    .getEbTtTracingNum() :
                                ebPslApp.getxEbTrackTrace().getEbTtTracingNum()));

                ttEvent.setxEbUser(new EbUser(user.getEbUserNum()));
                ttEvent.setxEbEtablissement(new EbEtablissement(user.getEbEtablissement().getEbEtablissementNum()));
                ttEvent.setxEbCompagnie(new EbCompagnie(user.getEbCompagnie().getEbCompagnieNum()));

                ttEvent.setValidateByChamp(ebPslApp.getValidateByChamp());

                listEvents.add(ttEvent);
            }

            ebTtEventRepository.saveAll(listEvents);

            // on met la TM en Transport ongoing/completed
            EbDemande ebDemande = demande != null ?
                demande :
                ebTrackTraceRepository
                    .selectEbDemandeDetailsByEbTracingNum(
                        isGenerateEventByTrRefOrCustomerRef ?
                            tmpInfosPsl
                                .getListPslByTrRefOrCustomerRefAndCodeAlpha().get(0).getxEbTrackTrace()
                                .getEbTtTracingNum() :
                            ebPslApp.getxEbTrackTrace().getEbTtTracingNum());

            String codeAlphaPslCourantTt = isGenerateEventByTrRefOrCustomerRef ?
                tmpInfosPsl
                    .getListPslByTrRefOrCustomerRefAndCodeAlpha().get(0).getxEbTrackTrace().getCodeAlphaPslCourant() :
                ebPslApp.getxEbTrackTrace().getCodeAlphaPslCourant();

            EbTtCompanyPsl pslTt = null;

            if (ebDemande.getxEbSchemaPsl() == null) {
                ebDemande
                    .setxEbSchemaPsl(ebDemandeRepository.selectSchemaPslByEbDemandeNum(ebDemande.getEbDemandeNum()));
            }

            if (ebDemande.getxEbSchemaPsl() != null) {
                ObjectMapper objectMapper = new ObjectMapper();
                // pour deserialiser la liste et eviter l'erreur lincked hashMap
                listPsl = Arrays
                    .asList(
                        objectMapper.convertValue(ebDemande.getxEbSchemaPsl().getListPsl(), EbTtCompanyPsl[].class));

                if (codeAlphaPslCourantTt != null) pslTt = listPsl
                    .stream().filter(it -> it.getCodeAlpha().contentEquals(codeAlphaPslCourantTt)).findFirst()
                    .orElse(null);
            }

            for (int i = 0; i < nombreEvents; i++) // par defaut nombreEvents=1
            {
                Boolean updateDateLastPslInDemande = false;

                if (isGenerateEventByTrRefOrCustomerRef) {
                    ebPslApp
                        .setxEbTrackTrace(
                            tmpInfosPsl.getListPslByTrRefOrCustomerRefAndCodeAlpha().get(i).getxEbTrackTrace());
                    ebPslApp
                        .setCompanyPsl(tmpInfosPsl.getListPslByTrRefOrCustomerRefAndCodeAlpha().get(i).getCompanyPsl());
                }

                // MAJ du tracing

                if ((pslTt == null || pslTt != null && pslTt.getOrder() < ebPslApp.getCompanyPsl().getOrder()) &&
                    (ebPslApp.getCompanyPsl().getEndPsl() != null && ebPslApp.getCompanyPsl().getEndPsl()) &&
                    ebPslApp.getDateActuelle() != null) {
                    updateDateLastPslInDemande = true;

                    ebTrackTraceRepository
                        .updatePslCourantAndDateLastPsl(
                            ebPslApp.getxEbTrackTrace().getEbTtTracingNum(),
                            ebPslApp.getCompanyPsl().getCodeAlpha(),
                            ebPslApp.getCompanyPsl().getLibelle(),
                            new Date(),
                            ebPslApp.getDateActuelle());
                }
                else {

                    // MAJ du TrackTrace par le dernier psl en cours
                    if (pslTt == null || pslTt != null && pslTt.getOrder() < ebPslApp.getCompanyPsl().getOrder()) {

                        if (ebPslApp.getDateActuelle() != null) {
                            ebTrackTraceRepository
                                .updatePslCourant(
                                    ebPslApp.getxEbTrackTrace().getEbTtTracingNum(),
                                    ebPslApp.getCompanyPsl().getCodeAlpha(),
                                    ebPslApp.getCompanyPsl().getLibelle(),
                                    new Date());
                        }

                    }

                    // MAJ du TrackTrace par la 'date end PSL' si le psl modifié
                    // est le 'end'
                    if (ebPslApp.getCompanyPsl().getEndPsl() != null &&
                        ebPslApp.getCompanyPsl().getEndPsl() && ebPslApp.getDateActuelle() != null) {
                        updateDateLastPslInDemande = true;
                        ebTrackTraceRepository
                            .updateDateLastPsl(
                                ebPslApp.getxEbTrackTrace().getEbTtTracingNum(),
                                ebPslApp.getDateActuelle());
                    }

                }

                // MAJ de la demande
                if (updateDateLastPslInDemande && ebPslApp.getDateActuelle() != null) {
                    ebDemandeRepository.updateDateLastPsl(ebDemande.getEbDemandeNum(), ebPslApp.getDateActuelle());
                }

            } // end for

            if (ebPslApp.getDateActuelle() != null && ebDemande.getEbDemandeNum() != null) {

                try {
                    controlRuleService
                        .processControlRules(
                            user.getEbCompagnie().getEbCompagnieNum(),
                            ebPslApp.getxEbTrackTrace().getEbTtTracingNum(),
                            ebPslApp.getEbTtPslAppNum(),
                            natureEventDate,
                            RuleCategory.DATE.getCode(),
                            null,
                            ebDemande.getEbDemandeNum(),
                            null,
                            null,
                            user);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            // MAJ de la demande
            if (ebDemande.getxEcStatut() == null || ebDemande.getxEcStatut() < StatutDemande.DLVRY.getCode()) {
                Integer xEcStatut = ebDemande.getxEcStatut();
                Integer orderPslStart = 0;
                Integer orderPslEnd = 0;
                boolean statutChanged = false;

                if (listPsl != null) {

                    for (EbTtCompanyPsl companyPSL: listPsl) {

                        if (companyPSL.getStartPsl() != null && companyPSL.getStartPsl()) {
                            orderPslStart = companyPSL.getOrder();
                        }

                        if (companyPSL.getEndPsl() != null && companyPSL.getEndPsl()) {
                            orderPslEnd = companyPSL.getOrder();
                        }

                    }

                }

                if (((ebPslApp.getCompanyPsl().getEndPsl() != null &&
                    ebPslApp.getCompanyPsl().getEndPsl() && checkIfAllTracingHaveStatusCompeted(
                        ebDemande.getEbDemandeNum(),
                        ebPslApp.getxEbTrackTrace().getEbTtTracingNum())) ||
                    (ebPslApp.getCompanyPsl().getStartPsl() != null &&
                        ebPslApp.getCompanyPsl().getStartPsl() &&
                        checkIfAllTracingHaveStatusCompeted(ebDemande.getEbDemandeNum(), null)))

                ) xEcStatut = StatutDemande.DLVRY.getCode();
                else if ((ebPslApp.getCompanyPsl().getStartPsl() != null && ebPslApp.getCompanyPsl().getStartPsl()) ||
                    (ebPslApp.getCompanyPsl().getOrder() > orderPslStart &&
                        ebPslApp.getCompanyPsl().getOrder() < orderPslEnd)) xEcStatut = StatutDemande.TO.getCode();

                if (demande != null) {
                    demande.setxEcStatut(xEcStatut);
                    statutChanged = true;
                }

                if ((pslTt == null || ebPslApp.getCompanyPsl().getOrder() > pslTt.getOrder())) {
                    ebDemande.setUpdateDate(new Date());
                    ebDemandeRepository
                        .updateEbDemandePslCourantAndStatus(
                            ebDemande.getEbDemandeNum(),
                            ebPslApp.getCompanyPsl().getCodeAlpha(),
                            ebPslApp.getCompanyPsl().getLibelle(),
                            xEcStatut);
                }
                else {
                    ebDemande.setUpdateDate(new Date());
                    ebDemandeRepository.updateStatusEbDemande(ebDemande.getEbDemandeNum(), xEcStatut);
                }

                // update status delivery when tr is on_going
                if (ebDemande.getEbDemandeNum() != null) {
                    List<EbLivraison> ListDeliveryFromConsolidate = ebLivraisonRepository
                        .findByEbDemande(ebDemande.getEbDemandeNum());

                    if (ListDeliveryFromConsolidate != null) {

                        try {

                            if (xEcStatut == StatutDemande.TO.getCode()) {

                                for (EbLivraison ebLivraison: ListDeliveryFromConsolidate) {
                                    ebLivraison = deliveryService
                                        .updateStatusDelivery(
                                            ebLivraison.getEbDelLivraisonNum(),
                                            StatusDelivery.SHIPPED);
                                }

                            }
                            else if (xEcStatut == StatutDemande.DLVRY.getCode()) {

                                for (EbLivraison ebLivraison: ListDeliveryFromConsolidate) {
                                    ebLivraison = deliveryService
                                        .updateStatusDelivery(
                                            ebLivraison.getEbDelLivraisonNum(),
                                            StatusDelivery.DELIVERED);
                                }

                            }

                        } catch (MyTowerException e) {
                            e.printStackTrace();
                        }

                    }

                    if (xEcStatut.equals(StatutDemande.DLVRY.getCode())) // run
                                                                         // automatic
                                                                         // actions
                                                                         // if
                                                                         // Tr
                                                                         // is
                                                                         // delivered
                    {
                        this.controlRuleService.processDocumentRules(ebDemande.getEbDemandeNum(), true, null);
                    }

                    if (statutChanged && ebPslApp.getDateActuelle() != null) {

                        // send kafka messages
                        try {
                            // Publier un message Booking sur un Topic
                            // Track-trace
                            kafkaObjectService
                                .sendTransportRequestToKafkaTopic(demande.getEbDemandeNum(), null, true, user);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {
                            controlRuleService
                                .processControlRules(
                                    user.getEbCompagnie().getEbCompagnieNum(),
                                    ebPslApp.getxEbTrackTrace().getEbTtTracingNum(),
                                    ebPslApp.getEbTtPslAppNum(),
                                    natureEventDate,
                                    RuleCategory.DATE.getCode(),
                                    null,
                                    ebDemande.getEbDemandeNum(),
                                    null,
                                    null,
                                    user);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

            }

            // update psl start/end date

            if (ebPslApp.getCompanyPsl() != null &&
                ebPslApp.getCompanyPsl().getStartPsl() != null && ebPslApp.getCompanyPsl().getStartPsl() &&
                ebPslApp.getDateActuelle() != null) {
                ebDemandeRepository.updateDatePickup(ebDemande.getEbDemandeNum(), ebPslApp.getDateActuelle());
            }
            else if (ebPslApp.getCompanyPsl() != null &&
                ebPslApp.getCompanyPsl().getEndPsl() != null && ebPslApp.getCompanyPsl().getEndPsl() &&
                ebPslApp.getDateActuelle() != null) {
                    ebDemandeRepository.updateDateDelivery(ebDemande.getEbDemandeNum(), ebPslApp.getDateActuelle());
                }

        } catch (Exception e) {
            logger.debug("updateEbPslAppAndAddEvent(): une erreur est détectée dans les données à intégrer");
            e.printStackTrace();

            if (tmpInfosPsl == null) {
                throw e;
            }
            else {
                tmpEventRejectNum = tmpInfosPsl.getTmpEventNum();
            }

        }

        return listEvents != null && !listEvents.isEmpty() ? listEvents.get(0) : null;
    }

    public Boolean checkIfAllTracingHaveStatusCompeted(Integer ebDemandeNum, Integer ebTracingToUpdate) {
        List<Date> dateLastPslList = ebTrackTraceRepository.selectDateLastPslByDemandeNum(ebDemandeNum);

        for (Date tracingDateLastPsl: dateLastPslList) {

            if (ebTracingToUpdate == null || tracingDateLastPsl == null) {
                return false;
            }

        }

        return true;
    }

    @Override
    public Map<String, Object> updateEbPslApp(EbTTPslApp ebPslApp) {
        Map<String, Object> result = new HashMap<String, Object>();
        EbUser user = connectedUserService.getCurrentUser();
        StringBuilder libellePsl = new StringBuilder();

        EbTtEvent ttEvent = updateEbPslAppAndAddEvent(ebPslApp, false, null, libellePsl, user, null);

        if (ttEvent != null) {
            emailTrackAndTrace.sendEmailUpdatePsl(ebPslApp, user, ttEvent, libellePsl.toString());

            String username = user.getRealUserNum() != null ? user.getRealUserNomPrenom() : user.getNomPrenom();
            String usernameFor = user.getRealUserNum() != null ? user.getNomPrenom() : null;
            String action = "Mise à jour de l'évènement " + libellePsl;
            Integer module = Enumeration.Module.TRACK.getCode();
            Integer chatCompId = Enumeration.IDChatComponent.TRACK.getCode();
            Integer idFiche = ebPslApp.getxEbTrackTrace().getEbTtTracingNum();
            EbChat ebChat = listStatiqueService
                .historizeAction(idFiche, chatCompId, module, action, username, usernameFor);

            result.put("ebChat", ebChat);
        }

        return result;
    }

    @Override
    public TracingUpdatePslResult
        updateEbPslAppMasse(List<EbTTPslApp> listEbPslApp, EbUser user, List<TmpInfosPsl> listTmpInfosPsl) {
        TracingUpdatePslResult result = new TracingUpdatePslResult();
        List<Integer> listRejectTmpEventNum = new ArrayList<Integer>();

        if (listTmpInfosPsl == null) {
            result.setResult(false);
        }

        if (user == null) {
            user = connectedUserService.getCurrentUser();
        }

        EbTtEvent ttEvent = null;
        boolean isSameEbDemande = true;
        Integer ebDemandeNum = null;

        if (listEbPslApp != null) {
            // pour chaque tracing juste la recuperation de (ebTtTracingNum,
            // codeAlphaPslCourant, ebDemandeNum, configPsl)
            List<EbTtTracing> listTracing = null;
            List<EbTTPslApp> listPslByTransportRefAndCodeAlpha = null;

            if (listTmpInfosPsl == null) {
                listTracing = ebTrackTraceRepository
                    .findListEbDemandeByEbTtTracingNumIn(
                        listEbPslApp
                            .stream().map(it -> it.getxEbTrackTrace().getEbTtTracingNum())
                            .collect(Collectors.toList()));
            }
            else {
                listTracing = new ArrayList<>();
                List<EbTtTracing> listTracingByRefTransportAndUnitRef = null;

                // get List tracing By unit ref and transport ref
                List<String> listUnitRef = listTmpInfosPsl
                    .stream()
                    .filter(
                        infoPsl -> infoPsl.getNatureReference().equals(NatureReference.UNIT_REF.getCode()) &&
                            !StringUtils.isBlank(infoPsl.getRefTransport()))
                    .map(infoPsl -> infoPsl.getUnitReference()).filter(Objects::nonNull).distinct()
                    .collect(Collectors.toList());

                List<String> listRef = listTmpInfosPsl
                    .stream()
                    .filter(
                        infoPsl -> infoPsl.getNatureReference().equals(NatureReference.UNIT_REF.getCode()) &&
                            !StringUtils.isBlank(infoPsl.getRefTransport()))
                    .map(infoPsl -> infoPsl.getRefTransport()).filter(Objects::nonNull).distinct()
                    .collect(Collectors.toList());

                if (listUnitRef != null && !listUnitRef.isEmpty() && listRef != null && !listRef.isEmpty()) {
                    listTracingByRefTransportAndUnitRef = daotrack
                        .getListTracingByRefTransportAndUnitRef(listRef, listUnitRef, true, null);

                    if (listTracingByRefTransportAndUnitRef != null && !listTracingByRefTransportAndUnitRef.isEmpty()) {
                        listTracing.addAll(listTracingByRefTransportAndUnitRef);
                    }

                }

                // get List tracing By unit ref and Customer ref
                listUnitRef = listTmpInfosPsl
                    .stream()
                    .filter(
                        infoPsl -> infoPsl.getNatureReference().equals(NatureReference.UNIT_REF.getCode()) &&
                            !StringUtils.isBlank(infoPsl.getCustomerReference()))
                    .map(infoPsl -> infoPsl.getUnitReference()).filter(Objects::nonNull).distinct()
                    .collect(Collectors.toList());

                listRef = listTmpInfosPsl
                    .stream()
                    .filter(
                        infoPsl -> infoPsl.getNatureReference().equals(NatureReference.UNIT_REF.getCode()) &&
                            !StringUtils.isBlank(infoPsl.getCustomerReference()))
                    .map(infoPsl -> infoPsl.getCustomerReference()).filter(Objects::nonNull).distinct()
                    .collect(Collectors.toList());

                if (listUnitRef != null && !listUnitRef.isEmpty() && listRef != null && !listRef.isEmpty()) {
                    listTracingByRefTransportAndUnitRef = daotrack
                        .getListTracingByRefTransportAndUnitRef(listRef, listUnitRef, false, null);

                    if (listTracingByRefTransportAndUnitRef != null && !listTracingByRefTransportAndUnitRef.isEmpty()) {
                        listTracing.addAll(listTracingByRefTransportAndUnitRef);
                    }

                }

                // get by Ref transport
                listRef = new ArrayList<String>();
                List<String> listCodeAlpha = new ArrayList<String>();

                for (TmpInfosPsl tmp: listTmpInfosPsl) {

                    if (tmp.getRefTransport() != null &&
                        tmp.getNatureReference().equals(NatureReference.TRANSPORT_REF.getCode())) {
                        listCodeAlpha.add(tmp.getCodeAlpha());
                        listRef.add(tmp.getRefTransport());
                    }

                }

                if (listRef != null && !listRef.isEmpty()) {
                    listPslByTransportRefAndCodeAlpha = daoPslApp
                        .getListPslAppByRefAndCodeAlpha(listRef, listCodeAlpha, true);
                }

                // get by customerRef
                listRef = new ArrayList<String>();
                listCodeAlpha = new ArrayList<String>();

                for (TmpInfosPsl tmp: listTmpInfosPsl) {

                    if (tmp.getCustomerReference() != null &&
                        tmp.getNatureReference().equals(NatureReference.CUSTOMER_REF.getCode())) {
                        listCodeAlpha.add(tmp.getCodeAlpha());
                        listRef.add(tmp.getCustomerReference());
                    }

                }

                if (listRef != null && !listRef.isEmpty()) {
                    listPslByTransportRefAndCodeAlpha = daoPslApp
                        .getListPslAppByRefAndCodeAlpha(listRef, listCodeAlpha, false);
                }

            }

            for (int i = 0; i < listEbPslApp.size(); i++) {
                final int index = i;

                Optional<EbTtTracing> tt = null;

                if (listTmpInfosPsl == null) {
                    tt = listTracing
                        .stream()
                        .filter(
                            it -> it
                                .getEbTtTracingNum()
                                .equals(listEbPslApp.get(index).getxEbTrackTrace().getEbTtTracingNum()))
                        .findFirst();
                }
                else {
                    // filtrage par unit ref dans le cas de generation par
                    // EDI

                    if (listTmpInfosPsl.get(index).getNatureReference().equals(NatureReference.UNIT_REF.getCode())) {
                        tt = listTracing
                            .stream()
                            .filter(
                                it -> it.getCustomerReference().equals(listTmpInfosPsl.get(index).getUnitReference()))
                            .findFirst();

                        if (tt != null && tt.isPresent()) listEbPslApp.get(index).setxEbTrackTrace(tt.get());
                        else {
                            listRejectTmpEventNum.add(listTmpInfosPsl.get(index).getTmpEventNum());
                            continue;
                        }

                    }
                    else // filtrage par ref transp dans le cas de generation
                         // par EDI
                    {

                        if (listPslByTransportRefAndCodeAlpha != null && !listPslByTransportRefAndCodeAlpha.isEmpty()) {

                            if (listTmpInfosPsl
                                .get(index).getNatureReference().equals(NatureReference.TRANSPORT_REF.getCode())) {
                                listTmpInfosPsl
                                    .get(index).setListPslByTrRefOrCustomerRefAndCodeAlpha(
                                        listPslByTransportRefAndCodeAlpha
                                            .stream()
                                            .filter(
                                                it -> it
                                                    .getxEbTrackTrace().getRefTransport()
                                                    .equals(listTmpInfosPsl.get(index).getRefTransport()))
                                            .filter(
                                                it -> it
                                                    .getCodeAlpha().equals(listTmpInfosPsl.get(index).getCodeAlpha()))
                                            .collect(Collectors.toList()));
                            }
                            else {
                                listTmpInfosPsl
                                    .get(index).setListPslByTrRefOrCustomerRefAndCodeAlpha(
                                        listPslByTransportRefAndCodeAlpha
                                            .stream()
                                            .filter(
                                                it -> it
                                                    .getxEbTrackTrace().getCustomRef()
                                                    .equals(listTmpInfosPsl.get(index).getCustomerReference()))
                                            .filter(
                                                it -> it
                                                    .getCodeAlpha().equals(listTmpInfosPsl.get(index).getCodeAlpha()))
                                            .collect(Collectors.toList()));
                            }

                        }

                    }

                }

                if (listTmpInfosPsl == null || listTmpInfosPsl.get(i).getUnitReference() != null)// a
                                                                                                 // verifier
                {

                    if (tt != null && tt.isPresent()) {
                        listEbPslApp
                            .get(i).getxEbTrackTrace()
                            .setxEbDemande(new EbDemande(tt.get().getxEbDemande().getEbDemandeNum()));

                        listEbPslApp
                            .get(i).getxEbTrackTrace().setCodeAlphaPslCourant(tt.get().getCodeAlphaPslCourant());
                    }

                    if (ebDemandeNum == null) ebDemandeNum = listEbPslApp
                        .get(i).getxEbTrackTrace().getxEbDemande().getEbDemandeNum();
                    else if (!ebDemandeNum
                        .equals(listEbPslApp.get(i).getxEbTrackTrace().getxEbDemande().getEbDemandeNum())) {
                            isSameEbDemande = false;
                        }
                }

            } // end for

        }

        try {
            EbDemande ebDemande = null;
            StringBuilder libelle = new StringBuilder();

            for (int i = 0; i < listEbPslApp.size(); i++) {

                try {
                    Boolean isGenerateEventByTransportRefOrCustomerRef = false;

                    if (listTmpInfosPsl != null &&
                        !listRejectTmpEventNum
                            .contains(
                                listTmpInfosPsl
                                    .get(i)
                                    .getTmpEventNum())) isGenerateEventByTransportRefOrCustomerRef = listTmpInfosPsl
                                        != null &&
                                        (listTmpInfosPsl
                                            .get(i).getNatureReference()
                                            .equals(NatureReference.CUSTOMER_REF.getCode()) ||
                                            listTmpInfosPsl
                                                .get(i).getNatureReference()
                                                .equals(NatureReference.TRANSPORT_REF.getCode()));

                    EbTtEvent newTtEvent = null;

                    if (!isGenerateEventByTransportRefOrCustomerRef ||
                        (isGenerateEventByTransportRefOrCustomerRef &&
                            (listTmpInfosPsl.get(i).getListPslByTrRefOrCustomerRefAndCodeAlpha() != null &&
                                !listTmpInfosPsl.get(i).getListPslByTrRefOrCustomerRefAndCodeAlpha().isEmpty()))

                    ) {

                        if (ebDemande == null && isSameEbDemande) {
                            // pour chaque ebDemande juste la recuperation de
                            // (ebDemandeNum, refTransport, xEcStatut,
                            // codeAlphaPslCourant, customerReference)
                            ebDemande = ebTrackTraceRepository
                                .selectEbDemandeByEbTracingNum(
                                    isGenerateEventByTransportRefOrCustomerRef ?
                                        listTmpInfosPsl
                                            .get(i).getListPslByTrRefOrCustomerRefAndCodeAlpha().get(0)
                                            .getxEbTrackTrace().getEbTtTracingNum() :
                                        listEbPslApp.get(i).getxEbTrackTrace().getEbTtTracingNum());
                        }

                        newTtEvent = updateEbPslAppAndAddEvent(
                            listEbPslApp.get(i),
                            true,
                            ebDemande,
                            libelle,
                            user,
                            listTmpInfosPsl != null ? listTmpInfosPsl.get(i) : null);

                        if (tmpEventRejectNum != null) {
                            listRejectTmpEventNum.add(tmpEventRejectNum);
                            tmpEventRejectNum = null;
                        }

                    }
                    else {

                        // s'il n'y a pas de tracing donc c'est un rejet
                        if (listTmpInfosPsl != null && listTmpInfosPsl.get(i) != null) {
                            listRejectTmpEventNum.add(listTmpInfosPsl.get(i).getTmpEventNum());
                        }

                        logger.debug("pas de tracing dans la DB. c'est un rejet (index= " + i + ")");
                    }

                    if (ttEvent == null) ttEvent = newTtEvent;
                }

                catch (Exception e) {
                    e.printStackTrace();
                    logger.debug("updateEbPslAppMasse() : une erreur est detectée dans les données !!!!!");
                }

            }

            // ancien test : if (listEbPslApp.size() > 0 && ttEvent != null &&
            // listTmpInfosPsl != null); listTmpInfosPsl != null à été supprimé
            // du test pour permettre l'envoie des mails lors de la mis a jour
            // psl via edi..
            if (listEbPslApp.size() > 0 && ttEvent != null) {
                result.setResult(true);
                emailTrackAndTrace.sendEmailUpdatePsl(listEbPslApp.get(0), user, ttEvent, libelle.toString());

                String username = user.getRealUserNum() != null ? user.getRealUserNomPrenom() : user.getNomPrenom();
                String usernameFor = user.getRealUserNum() != null ? user.getNomPrenom() : null;
                String action = "Mise à jour en masse de l'évènement " + libelle;
                Integer module = Enumeration.Module.TRACK.getCode();
                Integer chatCompId = Enumeration.IDChatComponent.TRACK.getCode();

                for (EbTTPslApp pslApp: listEbPslApp) {
                    Integer idFiche = pslApp.getxEbTrackTrace().getEbTtTracingNum();
                    listStatiqueService
                        .historizeAction(idFiche, chatCompId, module, action, username, usernameFor, user);
                }

            }

            if (listTmpInfosPsl != null) {
                result.setListReject(listRejectTmpEventNum);
            }

        } catch (Exception e) {
            e.printStackTrace();

            if (listTmpInfosPsl == null) {
                throw e;
            }

        }

        // Envoyer la demande et les evenement lier sur un topic kafka/
        /*
         * Optional<EbTtTracing> tracing = ebTrackTraceRepository
         * .findById(ttEvent.getxEbTtTracing().getEbTtTracingNum());
         */
        /*
         * for (int i = 0; i < listEbPslApp.size(); i++)
         * {
         * TransportRequestKafkaObject transportRequestKafkaObject;
         * try
         * {
         * transportRequestKafkaObject = kafkaObjectService
         * .getTransportRequestKafkaObjectWithEventFromDemande(listEbPslApp.get(
         * i).getxEbTrackTrace().getxEbDemande().getEbDemandeNum(),
         * listEbPslApp.get(i).getxEbTrackTrace().getEbTtTracingNum(), null);
         * producerService.sendTransportRequestKafkaObject(
         * transportRequestKafkaObject);
         * }
         * catch (Exception e)
         * {
         * e.printStackTrace();
         * }
         * }
         */
        return result;
    }

    @Transactional
    @Override
    public Integer addDeviationMasse(List<EbTtEvent> listTTEvent, EbUser userConnected) {
        Integer result = 0;
        EbUser user = userConnected != null ? userConnected : connectedUserService.getCurrentUser();
        List<EbTtEvent> listEvent = new ArrayList<EbTtEvent>();

        for (EbTtEvent ebTtEvent: listTTEvent) {

            try {
                Integer numEbPslApp = ebTtEvent.getxEbTtPslApp().getEbTtPslAppNum() != null ?
                    ebTtEvent.getxEbTtPslApp().getEbTtPslAppNum() :
                    ebPslAppRepository
                        .selectebTtPslAppNumByebTtTracingNumAndCodeAlpha(
                            ebTtEvent.getxEbTtTracing().getEbTtTracingNum(),
                            ebTtEvent.getxEbTtPslApp().getCompanyPsl().getCodeAlpha());

                // adding event

                ebTtEvent.getxEbTtPslApp().setEbTtPslAppNum(numEbPslApp);
                Integer tracingNum = ebTtEvent.getxEbTtTracing().getEbTtTracingNum();
                ebTtEvent.setxEbTtTracing(new EbTtTracing(tracingNum));
                EbDemande demande = ebTrackTraceRepository.selectEbDemandeByEbTracingNum(tracingNum);
                ebTtEvent.getxEbTtTracing().setRefTransport(demande.getRefTransport());
                ebTtEvent.setxEbUser(new EbUser(user.getEbUserNum()));
                ebTtEvent.setxEbEtablissement(new EbEtablissement(user.getEbEtablissement().getEbEtablissementNum()));
                ebTtEvent.setxEbCompagnie(new EbCompagnie(user.getEbCompagnie().getEbCompagnieNum()));

                ebTtEventRepository.save(ebTtEvent);
                listEvent.add(ebTtEvent);

                result = 1;

                if (listTTEvent != null &&
                    listTTEvent.size() > 0) emailTrackAndTrace.sendEmailDeviationTT(ebTtEvent, user, ebTtEvent);
            } catch (Exception e) {
                e.printStackTrace();
                throw e;
            }

        }

        if (listTTEvent.get(0).getTypeEvent()
            == TypeEvent.EVENT_DEVIATION.getCode()) createListEbQmIncidentByEvents(listEvent, userConnected);
        else if (listTTEvent.get(0).getTypeEvent()
            == TypeEvent.UNCONFORMITY.getCode()) createListEbQmDeviationByEvents(listEvent);

        if (listTTEvent != null && listTTEvent.size() > 0) {
            String username = user.getRealUserNum() != null ? user.getRealUserNomPrenom() : user.getNomPrenom();
            String usernameFor = user.getRealUserNum() != null ? user.getNomPrenom() : null;
            String action = "Mise à jour en masse de déviation " +
                TtEnumeration.TtPsl.getLibelleByCode(listTTEvent.get(0).getxEbTtPslApp().getCodePslCode());
            Integer module = Enumeration.Module.TRACK.getCode();
            Integer chatCompId = Enumeration.IDChatComponent.TRACK.getCode();
            Integer idFiche = listTTEvent.get(0).getxEbTtTracing().getEbTtTracingNum();
            listStatiqueService
                .historizeAction(
                    idFiche,
                    chatCompId,
                    module,
                    action,
                    user.getNom() + " " + user.getPrenom(),
                    username,
                    usernameFor);
        }

        return result;
    }

    @Override
    public Map<String, Object> addDeviation(EbTtEvent ebTtEvent, EbUser connectedUser, boolean lunchDR) {
        EbUser user = connectedUser == null ? connectedUserService.getCurrentUser() : connectedUser;
        Map<String, Object> result = new HashMap<String, Object>();
        List<EbTtEvent> listEvent = new ArrayList<EbTtEvent>();

        ebTtEvent.setxEbUser(ebUserRepository.getOne(user.getEbUserNum()));
        ebTtEvent.setxEbEtablissement(new EbEtablissement(user.getEbEtablissement().getEbEtablissementNum()));
        ebTtEvent.setxEbCompagnie(new EbCompagnie(user.getEbCompagnie().getEbCompagnieNum()));

        EbTTPslApp psl = ebPslAppRepository.findById(ebTtEvent.getxEbTtPslApp().getEbTtPslAppNum()).get();
        ebTtEvent.setxEbTtPslApp(psl);
        Optional<EbTtTracing> tracing = ebTrackTraceRepository
            .findById(ebTtEvent.getxEbTtTracing().getEbTtTracingNum());
        Integer compagnieTransNum = tracing.get().getxEbTransporteur().getEbCompagnie().getEbCompagnieNum();
        Integer compagnieChargeurNum = tracing.get().getxEbChargeur().getEbCompagnie().getEbCompagnieNum();
        Integer ctNum = tracing.get().getxEbDemande().getEbDemandeNum();

        // List<Integer> ebCompagnieNums = new ArrayList<>();
        // ebCompagnieNums.add(compagnieChargeurNum);
        // ebCompagnieNums.add(compagnieTransNum);

        // List<String> mails =
        // ebUserRepository.emailCompagnieList(ebCompagnieNums);
        // List<String> emailsTransporteurs =
        // ebUserRepository.emailCompagnie(compagnieTransNum);
        // List<String> emailsChargeur =
        // ebUserRepository.emailCompagnie(compagnieChargeurNum);
        // List<String> emails = new ArrayList<String>(emailsTransporteurs);
        // emails.addAll(emailsChargeur);
        // emails.remove(user.getEmail());

        ebTtEventRepository.save(ebTtEvent);

        listEvent.add(ebTtEvent);

        List<EbTTPslApp> listPsl = ebPslAppRepository
            .findAllByxEbTrackTraceEbTtTracingNum(tracing.get().getEbTtTracingNum());

        Integer orderPslStart = 0;

        for (EbTTPslApp ttpsl: listPsl) {

            if (ttpsl.getCompanyPsl().getStartPsl() != null && ttpsl.getCompanyPsl().getStartPsl()) {
                orderPslStart = ttpsl.getCompanyPsl().getOrder();
            }

        }

        if (ebTtEvent.getTypeEvent()
            == TypeEvent.EVENT_DEVIATION.getCode()) createListEbQmIncidentByEvents(listEvent, connectedUser);
        else if (ebTtEvent.getTypeEvent()
            == TypeEvent.UNCONFORMITY.getCode()) createListEbQmDeviationByEvents(listEvent);

        result.put("ebTtEvent", ebTtEvent);

        // on met la TM en Transport ongoing
        if (tracing.get().getxEbDemande().getxEcStatut() == null ||
            tracing.get().getxEbDemande().getxEcStatut() < StatutDemande.DLVRY.getCode()) {
            Integer xEcStatut = tracing.get().getxEbDemande().getxEcStatut();

            if (ebTtEvent.getxEbTtPslApp().getCompanyPsl().getOrder()
                >= orderPslStart) xEcStatut = StatutDemande.TO.getCode();
            tracing.get().getxEbDemande().setUpdateDate(new Date());
            ebDemandeRepository.updateStatusEbDemande(tracing.get().getxEbDemande().getEbDemandeNum(), xEcStatut);

            if (lunchDR) this.controlRuleService
                .processDocumentRules(tracing.get().getxEbDemande().getEbDemandeNum(), true, connectedUser);
        }

        try {
            ebTtEvent.setxEbTtTracing(tracing.get());
            emailTrackAndTrace.sendEmailDeviationTT(ebTtEvent, user, ebTtEvent);

            EbTTPslApp ebPslApp = ebPslAppRepository.findById(ebTtEvent.getxEbTtPslApp().getEbTtPslAppNum()).get();
            String username = user.getRealUserNum() != null ? user.getRealUserNomPrenom() : user.getNomPrenom();
            String usernameFor = user.getRealUserNum() != null ? user.getNomPrenom() : null;
            String action = "Mise à jour du déviation " +
                (ebPslApp.getCompanyPsl() != null ? ebPslApp.getCompanyPsl().getLibelle() : "");
            Integer module = Enumeration.Module.TRACK.getCode();
            Integer chatCompId = Enumeration.IDChatComponent.TRACK.getCode();
            Integer idFiche = tracing.get().getEbTtTracingNum();
            EbChat ebChat = listStatiqueService
                .historizeAction(
                    idFiche,
                    chatCompId,
                    module,
                    action,
                    user.getNom() + " " + user.getPrenom(),
                    username,
                    usernameFor);

            result.put("ebChat", ebChat);

            // Envoyer la demande et les evenement lier sur un topic kafka/
            TransportRequestKafkaObject transportRequestKafkaObject = kafkaObjectService
                .getTransportRequestKafkaObjectWithEventFromDemande(
                    tracing.get().getxEbDemande().getEbDemandeNum(),
                    ebTtEvent.getxEbTtTracing().getEbTtTracingNum(),
                    user);

            producerService.sendTransportRequestKafkaObject(transportRequestKafkaObject, false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public void createListEbQmIncidentByEvents(List<EbTtEvent> eventList, EbUser connectedUser) {
        EbUser user = connectedUser != null ? connectedUser : connectedUserService.getCurrentUser();

        EbEtablissement etablissement = null;
        List<EbQmIncident> listIncidents = new ArrayList<>();

        try {

            for (EbTtEvent ebTtEvent: eventList) {
                EbQmIncident incident = new EbQmIncident();

                incident.setEventType(TypeEvent.EVENT_DEVIATION.getCode());

                Optional<EbTtTracing> tracing = ebTrackTraceRepository
                    .findById(ebTtEvent.getxEbTtTracing().getEbTtTracingNum());

                incident.setxEbCompagnie(user.getEbCompagnie());

                incident.setOriginCountry(tracing.get().getxEcCountryOrigin());
                incident.setDestinationCountry(tracing.get().getxEcCountryDestination());

                incident.setModeTransport(tracing.get().getxEcModeTransport());

                incident
                    .setCarrierNom(
                        tracing.get().getxEbTransporteur() != null ?
                            tracing.get().getxEbTransporteur().getPrenomNom() :
                            "");
                incident.setIssuer(user);
                incident.setIssuerNomPrenom(user.getPrenomNom());
                incident
                    .setCategoryLabel(
                        eventList.get(0).getCategorie() != null ? eventList.get(0).getCategorie().getLibelle() : null);

                incident.setCategory(eventList.get(0).getCategorie());
                etablissement = tracing.get().getxEbDemande().getxEbEtablissement();

                Integer nbreIncident = ebQmIncidentRepository
                    .countListIncidentByRefTransport(tracing.get().getxEbDemande().getRefTransport());
                if (nbreIncident == null) nbreIncident = 0;

                incident
                    .setIncidentRef("QM" + (nbreIncident + 1) + "-" + tracing.get().getxEbDemande().getRefTransport());

                incident.setEbDemande(tracing.get().getxEbDemande());
                incident.setxEbDemande(tracing.get().getxEbDemande().getEbDemandeNum());
                incident.setIncidentDesc(ebTtEvent.getCommentaire());
                incident.setPsl(ebTtEvent.getxEbTtPslApp().getLibelle());
                incident.setDateIncident(ebTtEvent.getDateEvent());

                if (tracing.isPresent() && tracing.get().getxEbDemande() != null) {
                    incident.setTypeRequestLibelle(tracing.get().getxEbDemande().getTypeRequestLibelle());
                }

                if (tracing.get().getxEbDemande().getTypeRequestLibelle() != null) {
                    incident
                        .setTypeRequestNum(
                            TransortPriorityEnum
                                .getCodeByLibelle(tracing.get().getxEbDemande().getTypeRequestLibelle()));
                }

                if (ebTtEvent.getxEbTtPslApp().getCompanyPsl() != null) {
                    incident.setIncidentLines(this.getIncidentLines(tracing.get(), ebTtEvent, incident));

                    if (incident.getIncidentLines() == null || incident.getIncidentLines().isEmpty()) {
                        EbQmIncidentLine il = new EbQmIncidentLine();
                        il.setIncident(incident);
                        il.setCarrier(etablissement);
                        il.setCarrierName(etablissement.getNom());

                        if (tracing.get().getxEbMarchandise() != null) {
                            il.setEbMarchandiseNum(tracing.get().getxEbMarchandise().getEbMarchandiseNum());
                        }

                        incident.setIncidentLines(new ArrayList<EbQmIncidentLine>(Arrays.asList(il)));
                    }

                }

                listIncidents.add(incident);
            }

            Map<Integer, Map<String, Object>> results = qualityManagementService
                .saveIncidents(listIncidents, false, connectedUser);

            if (eventList != null && !eventList.isEmpty() && eventList.size() == 1) {

                if (results != null && !results.isEmpty()) {
                    List<Integer> incidentsNums = new ArrayList<Integer>(results.keySet());

                    if (incidentsNums.size() == 1) {
                        this.ebTtEventRepository
                            .updateIncidentNumInEvent(incidentsNums.get(0), eventList.get(0).getEbTtEventNum());
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void createListEbQmDeviationByEvents(List<EbTtEvent> eventList) {
        EbEtablissement etablissement = null;

        for (EbTtEvent ebTtEvent: eventList) {
            EbQmIncident deviation = new EbQmIncident();

            EbTtTracing tracing = ebTrackTraceRepository
                .findById(ebTtEvent.getxEbTtTracing().getEbTtTracingNum()).orElse(null);

            if (tracing != null) {
                deviation.setEbQmIncidentNum(daoQualityManagement.nextValQuality());
                deviation.setDemandeTranportRef(tracing.getRefTransport());
                deviation.setUnitRef(tracing.getCustomerReference());
                deviation.setOriginCountry(tracing.getxEcCountryOrigin());
                deviation.setDestinationCountry(tracing.getxEcCountryDestination());
                deviation.setxEbTtTracing(ebTrackTraceRepository.getOne(tracing.getEbTtTracingNum()));

                deviation.setOriginCountryLibelle(tracing.getxEcCountryOrigin().getLibelle());
                deviation.setOriginCountry(ecCountryRepository.getOne(tracing.getxEcCountryOrigin().getEcCountryNum()));

                deviation.setDestinationCountryLibelle(tracing.getxEcCountryDestination().getLibelle());
                deviation
                    .setDestinationCountry(
                        ecCountryRepository.getOne(tracing.getxEcCountryDestination().getEcCountryNum()));

                deviation.setModeTransport(tracing.getxEcModeTransport());
                deviation
                    .setInsurance(
                        tracing.getxEbDemande().getInsurance() != null && tracing.getxEbDemande().getInsurance() ?
                            Insurance.Yes.getCode() :
                            Insurance.No.getCode());

                deviation
                    .setCategory(
                        ebTtCategorieDeviationRepository
                            .getOne(ebTtEvent.getCategorie().getEbTtCategorieDeviationNum()));

                deviation.setEbDemande(tracing.getxEbDemande());
                deviation.setxEbDemande(tracing.getxEbDemande().getEbDemandeNum());
                deviation.setEbDemande(ebDemandeRepository.getOne(tracing.getxEbDemande().getEbDemandeNum()));

                deviation
                    .setCarrier(
                        ebEtablissementRepository
                            .getOne(tracing.getxEbEtablissementTransporteur().getEbEtablissementNum()));
                deviation.setCarrierNom(tracing.getxEbEtablissementTransporteur().getNom());

                deviation.setIssuer(ebTtEvent.getxEbUser());
                deviation.setIssuerNomPrenom(ebTtEvent.getxEbUser().getNomPrenom());
                deviation.setPsl(ebTtEvent.getxEbTtPslApp().getLibelle());
                deviation.setTypeRequestLibelle(tracing.getxEbDemande().getTypeRequestLibelle());

                if (tracing.getxEbDemande().getTypeRequestLibelle() != null) {
                    deviation
                        .setTypeRequestNum(
                            TransortPriorityEnum.getCodeByLibelle(tracing.getxEbDemande().getTypeRequestLibelle()));
                }

                if (tracing.getxEbDemande() != null) etablissement = tracing.getxEbDemande().getxEbEtablissement();

                if (ebTtEvent.getxEbTtPslApp().getCompanyPsl() != null) {
                    deviation.setIncidentLines(this.getIncidentLines(tracing, ebTtEvent, deviation));

                    if (deviation.getIncidentLines() == null || deviation.getIncidentLines().isEmpty()) {
                        EbQmIncidentLine il = new EbQmIncidentLine();
                        il.setIncident(deviation);

                        if (etablissement != null) {
                            il.setCarrier(etablissement);
                            il.setCarrierName(etablissement.getNom());
                        }

                        if (tracing.getxEbMarchandise() != null) {
                            il.setEbMarchandiseNum(tracing.getxEbMarchandise().getEbMarchandiseNum());
                        }

                        deviation.setIncidentLines(new ArrayList<EbQmIncidentLine>(Arrays.asList(il)));
                    }

                }

                ebQmDeviationService.addEbQmDeviation(deviation);

                // ebTtEvent =
                // this.ebTtEventRepository.getOne(ebTtEvent.getEbTtEventNum());
                ebTtEvent.setEbQmIncidentNum(deviation.getEbQmIncidentNum());
            }

        }

        if (eventList != null && !eventList.isEmpty()) this.ebTtEventRepository.saveAll(eventList);
    }

    private List<EbQmIncidentLine> getIncidentLines(EbTtTracing tracing, EbTtEvent ebTtEvent, EbQmIncident incident) {
        List<EbQmIncidentLine> incidentLines = new ArrayList<>();
        EbTTPslAppAndEventsProjection actualEvent = null;

        EbTtSchemaPsl schemaPsl = this.ebDemandeRepository
            .selectSchemaPslByEbDemandeNum(tracing.getxEbDemande().getEbDemandeNum());

        List<EbTtCategorieDeviation> listCategorieDeviation = serviceListStatique.getListCategorieDeviation(null);

        EbTtCategorieDeviation catDelay = listCategorieDeviation
            .stream().filter(cat -> cat.getLibelle().equals("Delay")).findFirst().orElse(null);

        EbTtCompanyPsl pslEvent = schemaPsl
            .getListPsl().stream()
            .filter(
                psl -> psl.getLibelle() != null &&
                    psl.getLibelle().equals(ebTtEvent.getxEbTtPslApp().getCompanyPsl().getLibelle()))
            .findFirst().orElse(null);

        List<EbMarchandise> units = this.ebMarchandiseRepository
            .findAllByEbDemande_ebDemandeNum(tracing.getxEbDemande().getEbDemandeNum());

        List<EbTTPslAppAndEventsProjection> result = ebPslAppRepository
            .findByXEbTrackTrace_ebTtTracingNumOrderByOrderpsl(tracing.getEbTtTracingNum());

        if (result != null && !result.isEmpty()) {
            actualEvent = result.stream().filter(ev -> ev.isActual()).findFirst().orElse(null);
        }

        for (EbMarchandise ebMarchandise: units) {
            EbQmIncidentLine incidentLine = new EbQmIncidentLine();

            incidentLine.setUnitReference(ebMarchandise.getUnitReference());
            incidentLine.setEbMarchandiseNum(ebMarchandise.getEbMarchandiseNum());
            incidentLine.setTypeMarchandise(ebMarchandise.getTypeMarchandise());
            incidentLine.setNumberOfUnits(ebMarchandise.getNumberOfUnits());
            incidentLine.setWeight(ebMarchandise.getWeight());

            incidentLine.setDangerousGood(ebMarchandise.getDangerousGood());
            incidentLine.setComment(ebMarchandise.getComment());
            incidentLine.setIncident(new EbQmIncident(incident.getEbQmIncidentNum()));

            if (pslEvent != null && pslEvent.getEbTtCompanyPslNum() != null) {

                if (ebTtEvent.getCategorie() != null &&
                    ebTtEvent.getCategorie().getLibelle().equals(catDelay.getLibelle())) {

                    if (pslEvent.getLibelle().equals("Departure")) // those are
                                                                   // the only
                                                                   // psls that
                                                                   // exist in
                                                                   // incident
                                                                   // lines tab
                                                                   // (statically)
                    {
                        incidentLine.setDelayDepartureDate(ebTtEvent.getDateEvent());
                        incidentLine.setEta(pslEvent.getExpectedDate());
                        incidentLine.setEtd(actualEvent != null ? actualEvent.getDate() : null);
                    }
                    else if (pslEvent.getLibelle().equals("Arrival")) {
                        incidentLine.setDelayArrivalDate(ebTtEvent.getDateEvent());
                        incidentLine.setAtd(pslEvent.getExpectedDate());
                        incidentLine.setAta(actualEvent != null ? actualEvent.getDate() : null);
                    }

                }

            }

            incidentLines.add(incidentLine);
        }

        return incidentLines;
    }

    @Override
    public List<EbTtTracing> getListEbTracingByEbDemandeNum(Integer ebDemandeNum) {
        return ebTrackTraceRepository.findByXEbDemande_ebDemandeNum(ebDemandeNum);
    }

    @Override
    public List<EbTtTracing> getListEbtrack(SearchCriteriaTrackTrace criterias) {
        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criterias);
        return daotrack.getListEbTrackTrace(criterias, connectedUser);
    }

    @Override
    public List<EbTtSchemaPsl> getListEbIncotermPsl() {
        List<EbTtSchemaPsl> listIncotermPsl = daoShemaPsl.getListSchemaPsl();
        return listIncotermPsl;
    }

    @Override
    public EbTtTracing saveFlags(Integer ebTtTracingNum, String newFlags) {
        EbTtTracing tt = ebTrackTraceRepository.getOne(ebTtTracingNum);
        tt.setListFlag(newFlags);
        return ebTrackTraceRepository.save(tt);
    }

    @Override
    public List<EbTtTracing> getTracing(SearchCriteriaTrackTrace criteria) {
        // TODO Auto-generated method stub
        List<EbTtTracing> listTracing = daotrack.getListTracing(criteria);

        for (EbTtTracing ebTracing: listTracing) {

            if (ebTracing != null && ebTracing.getEbTtTracingNum() != null) {
                List<EbTTPslApp> listPslApp = ebPslAppRepository
                    .findAllByXEbTrackTrace_ebTtTracingNum(ebTracing.getEbTtTracingNum());

                ebTracing.setListEbPslApp(listPslApp);

                if (ebTracing.getListEbPslApp() != null && !ebTracing.getListEbPslApp().isEmpty()) {
                    ebTracing.getListEbPslApp().forEach(it -> {
                        if (it.getxEbTrackTrace()
                            != null) it.setxEbTrackTrace(new EbTtTracing(it.getxEbTrackTrace().getEbTtTracingNum()));
                        if (it.getxEbUser() != null) it.setxEbUser(new EbUser(it.getxEbUser().getEbUserNum()));
                        if (it.getxEcCountry() != null) it
                            .setxEcCountry(
                                new EcCountry(
                                    it.getxEcCountry().getEcCountryNum(),
                                    it.getxEcCountry().getCode(),
                                    it.getxEcCountry().getLibelle()));
                    });
                }

            }

        }

        return listTracing;
    }

    @Override
    public void insertDeviation() {
        List<EbTTPslApp> listPslApp = ebPslAppRepository.findAllByTreatForDeviation();

        for (EbTTPslApp pslApp: listPslApp) {
            if (pslApp.getDateEstimee() != null) if (pslApp.getDateEstimee().before(new Date())) {
                EbQmDeviation dev = new EbQmDeviation();
                EbTtTracing tracing = pslApp.getxEbTrackTrace();
                dev.setRefTransport(tracing.getRefTransport());
                dev.setOriginCountry(tracing.getxEcCountryOrigin());
                dev.setDestinationCountry(tracing.getxEcCountryDestination());
                dev.setOriginCountryLibelle(tracing.getLibelleOriginCity());
                dev.setDestinationCountryLibelle(tracing.getLibelleDestCity());
                dev.setDeviationDate(new Date());
                dev.setRefDeviation("DEV-" + "N");
                // ebTrackTraceRepository.findById(pslApp.getx)
                ebQmDeviationRepository.save(dev);
                pslApp.setTreatForDeviation(true);
            }
        }

    }

    @Override
    public Integer updateRevisedDateEbPslAppMasse(List<EbTTPslApp> listEbPslApp) {
        Integer result = 0;
        EbUser user = connectedUserService.getCurrentUser();
        boolean isSameEbDemande = true;
        Integer ebDemandeNum = null;

        if (listEbPslApp != null) {
            List<EbTtTracing> listTracing = ebTrackTraceRepository
                .findListEbDemandeByEbTtTracingNumIn(
                    listEbPslApp
                        .stream().map(it -> it.getxEbTrackTrace().getEbTtTracingNum()).collect(Collectors.toList()));

            for (EbTTPslApp pslApp: listEbPslApp) {
                Optional<EbTtTracing> tt = listTracing
                    .stream().filter(it -> it.getEbTtTracingNum().equals(pslApp.getxEbTrackTrace().getEbTtTracingNum()))
                    .findFirst();

                if (tt.isPresent()) pslApp
                    .getxEbTrackTrace().setxEbDemande(new EbDemande(tt.get().getxEbDemande().getEbDemandeNum()));

                if (ebDemandeNum == null) ebDemandeNum = pslApp.getxEbTrackTrace().getxEbDemande().getEbDemandeNum();
                else if (!ebDemandeNum.equals(pslApp.getxEbTrackTrace().getxEbDemande().getEbDemandeNum())) {
                    isSameEbDemande = false;
                }
            }

        }

        try {
            EbDemande ebDemande = null;
            StringBuilder libelle = new StringBuilder();
            String username = user.getRealUserNum() != null ? user.getRealUserNomPrenom() : user.getNomPrenom();

            String usernameFor = user.getRealUserNum() != null ? user.getNomPrenom() : null;
            Integer module = Enumeration.Module.TRACK.getCode();
            Integer chatCompId = Enumeration.IDChatComponent.TRACK.getCode();

            for (EbTTPslApp ebPslApp: listEbPslApp) {
                Integer idFiche = ebPslApp.getxEbTrackTrace().getEbTtTracingNum();
                ebDemande = null;

                if (idFiche != null) {
                    ebDemande = ebTrackTraceRepository.selectEbDemandeByEbTracingNum(idFiche);
                }

                String action = generateMessageForHistEditRDChat(ebPslApp, user);

                updateForRevisedDateEbPslApp(ebPslApp, libelle);

                // envoie de mail
                if (ebDemande != null) {
                    emailServicePricingBooking.sendEmailTranpsportInformation(ebDemande, user);
                }

                listStatiqueService.historizeAction(idFiche, chatCompId, module, action, username, usernameFor);
            }

            result = 1;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return result;
    }

    @Transactional
    private void updateForRevisedDateEbPslApp(EbTTPslApp ebPslApp, StringBuilder libelle) {
        // ebPslApp.setCodePsl(TtPsl.getPslByCode(ebPslApp.getCodePslCode()));
        EbTTPslApp oldPslApp = ebPslAppRepository
            .findFirstByxEbTrackTraceEbTtTracingNumAndCodeAlpha(
                ebPslApp.getxEbTrackTrace().getEbTtTracingNum(),
                ebPslApp.getCompanyPsl().getCodeAlpha());

        if (ebPslApp.getCommentaire() != null) oldPslApp.setCommentaire(ebPslApp.getCommentaire());

        if (ebPslApp.getDateEstimee() != null) oldPslApp.setDateEstimee(ebPslApp.getDateEstimee());

        if (ebPslApp.getFuseauHoraire() != null) oldPslApp.setFuseauHoraire(ebPslApp.getFuseauHoraire());

        if (oldPslApp.getxEbTrackTrace() == null || ebPslApp.getxEbTrackTrace().getEbTtTracingNum() == null) {
            ebPslApp.setxEbTrackTrace(new EbTtTracing());
            ebPslApp.getxEbTrackTrace().setEbTtTracingNum(ebPslApp.getxEbTrackTrace().getEbTtTracingNum());
        }

        libelle.append(ebPslApp.getCompanyPsl().getLibelle());

        ebPslAppRepository.save(oldPslApp);
    }

    @Override
    public Map<String, Object> updateRevisedDateEbPslApp(EbTTPslApp ebPslApp) {
        Map<String, Object> result = new HashMap<String, Object>();
        EbUser user = connectedUserService.getCurrentUser();
        StringBuilder libellePsl = new StringBuilder();

        // historisation
        String username = user.getRealUserNum() != null ? user.getRealUserNomPrenom() : user.getNomPrenom();
        String usernameFor = user.getRealUserNum() != null ? user.getNomPrenom() : null;

        Integer module = Enumeration.Module.TRACK.getCode();
        Integer chatCompId = Enumeration.IDChatComponent.TRACK.getCode();
        Integer idFiche = ebPslApp.getxEbTrackTrace().getEbTtTracingNum();

        String action = generateMessageForHistEditRDChat(ebPslApp, user);

        // mise à jour de la date estimée
        updateForRevisedDateEbPslApp(ebPslApp, libellePsl);

        // chargement de la demande et mise à jour des données du transport
        // information
        EbDemande demande = ebDemandeRepository
            .findById(ebPslApp.getxEbTrackTrace().getxEbDemande().getEbDemandeNum()).orElse(null);

        if (demande != null) {

            for (EbTtCompanyPsl psl: demande.getxEbSchemaPsl().getListPsl()) {

                if (psl.getCodeAlpha().contentEquals(ebPslApp.getCompanyPsl().getCodeAlpha())) {
                    psl.setExpectedDate(ebPslApp.getDateEstimee());
                    ebDemandeRepository.save(demande);
                    break;
                }

            }

        }

        // envoie de mail
        emailServicePricingBooking.sendEmailTranpsportInformation(demande, user);

        EbChat ebChat = listStatiqueService.historizeAction(idFiche, chatCompId, module, action, username, usernameFor);

        result.put("ebChat", ebChat);

        return result;
    }

    public String generateMessageForHistEditRDChat(EbTTPslApp ebPslApp, EbUser connectedUser) {
        EbTTPslApp oldPslApp = null;

        // chargement de l'ancienne date estimée pour l'historiser dans le chat
        if (ebPslApp.getEbTtPslAppNum() == null) {
            oldPslApp = ebPslAppRepository
                .findFirstByxEbTrackTraceEbTtTracingNumAndCodeAlpha(
                    ebPslApp.getxEbTrackTrace().getEbTtTracingNum(),
                    ebPslApp.getCompanyPsl().getCodeAlpha());
        }
        else {
            oldPslApp = ebPslAppRepository.findById(ebPslApp.getEbTtPslAppNum()).orElse(null);
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

        String oldEstimatedDate = null;

        if (oldPslApp != null && oldPslApp.getDateEstimee() != null) {
            oldEstimatedDate = formatter.format(oldPslApp.getDateEstimee());
        }

        String lang = connectedUser != null && connectedUser.getLanguage() != null ? connectedUser.getLanguage() : "en";
        Locale lcl = new Locale(lang);
        String[] params = {
            ebPslApp.getCompanyPsl().getLibelle()
        };

        String message = messageSource.getMessage("tracing.message_rd.update_ed", params, lcl);

        if (ebPslApp.getDateEstimee() != null && oldEstimatedDate != null) {
            params = new String[]{
                oldEstimatedDate, formatter.format(ebPslApp.getDateEstimee())
            };
            message += messageSource.getMessage("tracing.message_rd.value_with_old_date", params, lcl);
        }

        else if (ebPslApp.getDateEstimee() != null) {
            message += formatter.format(ebPslApp.getDateEstimee()) + "<br>";
        }

        if (ebPslApp.getCommentaire() != null) {
            params = new String[]{
                ebPslApp.getCommentaire()
            };
            message += messageSource.getMessage("tracing.message_rd.comment", params, lcl);
        }

        return message;
    }

    @Override
    public List<EbTTPslAppAndEventsProjection> getTracingPslAndsThiereEvents(Integer ebTtTracingNum) {
        Comparator<EbTTPslAppAndEventsProjection> EbTTPslAppAndEventsProjectionComparator = (o1, o2) -> o1.getOrder() -
            (o2.getOrder());
        List<EbTTPslAppAndEventsProjection> result = ebPslAppRepository
            .findByXEbTrackTrace_ebTtTracingNumOrderByOrderpsl(ebTtTracingNum);
        if (result != null) result.sort(EbTTPslAppAndEventsProjectionComparator);

        return result;
    }

    @Override
    public List<EbTTPslApp> getListTracingStatus(SearchCriteria criteria) {
        return ebPslAppRepository.selectPslLibelleAndCodeAlphaByCompanyNum(criteria.getEbCompagnieNum());
    }

		@Override
		public List<EbTTPslApp> getListTracingStatusByListOfCompanyNum(List<Integer> listCompanyNums)
		{
			return ebPslAppRepository
				.selectPslLibelleAndCodeAlphaByListOfCompanyNum(listCompanyNums);
		}

		public List<Map<String, String>> getListStatutTracing(SearchCriteria criteria)
			throws IOException
		{
			List<EbTTPslApp> listPslApp = new ArrayList<EbTTPslApp>();
			List<Map<String, String>> listStatut = new ArrayList<Map<String, String>>();

			EbUser connectedUser = connectedUserService.getCurrentUser();

			if (isDefaultControlTowerOrCarrier(connectedUser))
			{
				List<Integer> listCompanyNums = compagnieService
					.findContactsCompagniesNums(connectedUser.getEbUserNum(), true);
				listCompanyNums = listCompanyNums.stream().distinct().collect(Collectors.toList());
				listPslApp = getListTracingStatusByListOfCompanyNum(listCompanyNums);
			}
			else
			{
				listPslApp = getListTracingStatus(criteria);
			}
			List<EbTtCompanyPsl> listCompanyPsl = companyPslService.listTtCompanyPsl(criteria);

        if (listPslApp != null) {
            listPslApp.addAll(DEFAULT_PSL_LIST);
            listStatut = listPslApp.stream().map(it -> {
                Map<String, String> map = new HashMap<String, String>();
                map.put("codeAlpha", it.getCodeAlpha());
                map.put("libelle", it.getLibelle());
                return map;
            }).collect(Collectors.toList());
        }

			if (listCompanyPsl != null)
			{
				for (EbTtCompanyPsl psl : listCompanyPsl)
				{
					boolean exists = listStatut
						.stream()
						.filter(it -> it.get("codeAlpha").contentEquals(psl.getCodeAlpha()))
						.findFirst()
						.isPresent();

					if (!exists)
					{
						Map<String, String> map = new HashMap<String, String>();
						map.put("codeAlpha", psl.getCodeAlpha());
						map.put("libelle", psl.getLibelle());
						listStatut.add(map);
					}
				}
			}

			if (listStatut != null && !listStatut.isEmpty())
			{
				listStatut
					.sort(
						(it1, it2) -> {
							return org.apache.commons.lang3.StringUtils
								.compare(it1.get("libelle"), it2.get("libelle"));
						}
					);
			}
			return listStatut;
		}

		private boolean isDefaultControlTowerOrCarrier(EbUser connectedUser)
		{
			return (connectedUser.isControlTower() &&
				!Arrays
					.asList(Role.ROLE_CHARGEUR.getCode(), Role.ROLE_PRESTATAIRE.getCode())
					.contains(connectedUser.getEbCompagnie().getCompagnieRole())) ||
				connectedUser.isPrestataire();
		}

    @Override
    public List<String> getListFlagForCarrier(Integer ebUserNum) {
        return ebTrackTraceRepository.selectListFlagForCarrier(ebUserNum);
    }

    @Override
    public List<String> getListFlagForControlTower(Integer ebUserNum) {
        return ebTrackTraceRepository.selectListFlagForControlTower(ebUserNum);
    }
}
