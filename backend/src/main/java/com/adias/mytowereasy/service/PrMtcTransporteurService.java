package com.adias.mytowereasy.service;

import java.util.HashMap;

import com.adias.mytowereasy.model.PrMtcTransporteur;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface PrMtcTransporteurService {
    public HashMap<String, Object> getListPrMtcTransporteur(SearchCriteria criteria);

    public Boolean deleteMtcTransporteur(Integer mtcTranspNum);

    public PrMtcTransporteur saveMtcTransporteur(PrMtcTransporteur prMtcTransporteur);
}
