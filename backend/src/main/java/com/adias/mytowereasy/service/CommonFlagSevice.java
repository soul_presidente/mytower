package com.adias.mytowereasy.service;

import java.util.List;


public interface CommonFlagSevice {
    List<String> getListFlagForCarrier(Integer ebUserNum);

    List<String> getListFlagForControlTower(Integer ebUserNum);
}
