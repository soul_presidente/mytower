package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.EbWebHooks;


public interface WebHooksService {
    public Boolean addWebHooks(String user, String pwd, String msg);

    public List<EbWebHooks> listWebHooks();
}
