package com.adias.mytowereasy.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.TtEnumeration;
import com.adias.mytowereasy.model.TtEnumeration.TypeEvent;
import com.adias.mytowereasy.model.qm.EbQmDeviation;
import com.adias.mytowereasy.model.qm.EbQmIncident;
import com.adias.mytowereasy.model.qm.EbQmIncidentLine;


@Service
public class QmDeviationServiceImpl extends MyTowerService implements QmDeviationService {
    @Autowired
    ConnectedUserService connectedUserService;

    @Override
    public void addEbQmDeviation(EbQmIncident deviation) {
        EbDemande demande = ebDemandeRepository
            .findOneByEbDemandeNum(deviation.getxEbTtTracing().getxEbDemande().getEbDemandeNum());

        EbUser connectedUser = connectedUserService.getCurrentUser();

        if (demande != null) {
            if (deviation.getOriginCountryLibelle() == null) deviation
                .setOriginCountryLibelle(demande.getLibelleOriginCountry());
            if (deviation.getDestinationCountryLibelle() == null) deviation
                .setDestinationCountryLibelle(demande.getLibelleDestCountry());

            if (deviation.getCarrier() == null) deviation.setCarrier(demande.getxEbEtablissementBroker());
            if (deviation.getCarrierNom() == null) deviation.setCarrierNom(demande.getNomTransporteur());

            EbUser issuer = deviation.getIssuer();

            if (issuer == null || issuer.getEbUserNum() == null) {
                if (connectedUser != null && connectedUser.getEbUserNum() != null) issuer = connectedUser;
                else issuer = demande.getUser();

                deviation.setIssuer(ebUserRepository.getOne(issuer.getEbUserNum()));
                deviation.setIssuerNomPrenom(issuer.getNomPrenom());
            }

            deviation.setxEbEtablissement(demande.getxEbEtablissement());
            deviation.setxEbCompagnie(demande.getxEbCompagnie());

            Integer nbreDeviation = ebQmIncidentRepository.countByEventType(TypeEvent.UNCONFORMITY.getCode());

            deviation.setIncidentRef("DEV" + (nbreDeviation + 1) + "-" + demande.getRefTransport());
            deviation.setEbDemande(ebDemandeRepository.getOne(demande.getEbDemandeNum()));
            deviation.setEventType(TypeEvent.UNCONFORMITY.getCode());

            // Owner of the request
            deviation.setOwnerNomPrenom(demande.getUser().getNomPrenom());
            deviation.setOwnerNum(demande.getUser().getEbUserNum());

            List<EbQmIncidentLine> incidentLines = deviation.getIncidentLines();
            if (incidentLines != null && !incidentLines.isEmpty()) deviation.setIncidentLines(null);

            EbQmIncident savedDeviation = ebQmIncidentRepository.save(deviation);
            incidentLines
                .stream().forEach(
                    ebQmIncidentLine -> ebQmIncidentLine
                        .getIncident().setEbQmIncidentNum(savedDeviation.getEbQmIncidentNum()));
            this.ebQmIncidentLineRepository.saveAll(incidentLines);
        }

    }

    @Override
    public void transferDeviationsToIncidents() {
        List<EbQmDeviation> listDeviation = ebQmDeviationRepository.findByDeletedIsNotTrue();

        if (listDeviation != null && !listDeviation.isEmpty()) {

            for (EbQmDeviation deviation: listDeviation) {

                if (deviation.getRefTransport() != null && !deviation.isDeleted()) {
                    EbDemande demande = ebDemandeRepository.getByRefTransport(deviation.getRefTransport());
                    Integer nbreDeviation = ebQmIncidentRepository
                        .countByEventType(TtEnumeration.TypeEvent.UNCONFORMITY.getCode());

                    if (Objects.nonNull(demande.getEbDemandeNum()) && Objects.nonNull(nbreDeviation)) {
                        EbQmIncident incident = new EbQmIncident(deviation, demande, nbreDeviation);
                        EbQmIncident savedIncident = ebQmIncidentRepository.save(incident);
                        ebQmIncidentRepository
                            .updateDateCreation(savedIncident.getEbQmIncidentNum(), deviation.getDeviationDate());
                        ebQmDeviationRepository.updateDeleted(deviation.getEbQmDeviationNum(), true);
                    }

                }

            }

        }

    }
}
