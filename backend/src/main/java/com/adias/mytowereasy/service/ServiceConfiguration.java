/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.security.TokenAuthenticationService;


@Service
public class ServiceConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceConfiguration.class);

    @Value("${mytowereasy.temp-file.location}")
    public String tempFileLocation;

    @Value("${mytower.mail.smtp.from}")
    public String smtpFromName;

    @Value("${spring.mail.properties.mail.smtp.from}")
    public String smtpFromAdress;

    public String emailSupportTechnique;

    @Value("${spring.profiles.active}")
    public String springProfilesActive;

    @Value("${mytowereasy.url.back}")
    public String mytowereasyUrlBack;

    @Value("${adias.batchsize:1000}")
    public int batchsize = 1000;

    private long dynamicExpirationTime;

    public String getTempFolderPath() {
        String tmpFolderPath = this.tempFileLocation;

        if (StringUtils.isBlank(tmpFolderPath)) {
            // If not setted in properties, use JVM default temp
            tmpFolderPath = System.getProperty("java.io.tmpdir");
        }

        if (StringUtils.isBlank(tmpFolderPath)) {
            LOGGER.error("No temp folder path found ! Some features will not work properly");
        }

        return tmpFolderPath;
    }

    public long getDynamicExpirationTime() {
        return dynamicExpirationTime;
    }

    @Value("${mytower.auth.token-lifetime:" + TokenAuthenticationService.EXPIRATIONTIME + "}")
    public void setDynamicExpirationTime(long dynamicExpirationTime) {
        this.dynamicExpirationTime = dynamicExpirationTime;
    }

    public String getEmailSupportTechnique() {
        return emailSupportTechnique;
    }

    @Value("${adias.support.email}")
    public void setEmailSupportTechnique(String emailSupportTechnique) {
        this.emailSupportTechnique = emailSupportTechnique;
    }
}
