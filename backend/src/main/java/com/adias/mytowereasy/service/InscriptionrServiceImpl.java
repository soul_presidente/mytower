/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.*;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.LanguageUser;
import com.adias.mytowereasy.model.Enumeration.UserStatus;
import com.adias.mytowereasy.service.email.EmailService;


@Service
public class InscriptionrServiceImpl extends MyTowerService implements InscriptionService {
    @Autowired
    EmailService emailService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    UserService userService;

    private static final Logger LOGGER = LoggerFactory.getLogger(InscriptionrServiceImpl.class);

    @Override
    public EbUser inscriptionPublic(EbUser ebUser) {
        // gestion des Access rights
        Set<ExUserModule> listExUserModule = new HashSet<ExUserModule>();

        if (ebUser.isTransporteur()) {
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.PRICING.getCode(),
                        Enumeration.AccessRight.CONTRIBUTION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.TRANSPORT_MANAGEMENT.getCode(),
                        Enumeration.AccessRight.CONTRIBUTION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.TRACK.getCode(),
                        Enumeration.AccessRight.CONTRIBUTION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.QUALITY_MANAGEMENT.getCode(),
                        Enumeration.AccessRight.VISUALISATION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.FREIGHT_AUDIT.getCode(),
                        Enumeration.AccessRight.VISUALISATION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.FREIGHT_ANALYTICS.getCode(),
                        Enumeration.AccessRight.VISUALISATION.getCode(),
                        ebUser.getEbUserNum()));
        }
        else if (ebUser.isBroker()) {
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.TRANSPORT_MANAGEMENT.getCode(),
                        Enumeration.AccessRight.CONTRIBUTION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.QUALITY_MANAGEMENT.getCode(),
                        Enumeration.AccessRight.VISUALISATION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.FREIGHT_AUDIT.getCode(),
                        Enumeration.AccessRight.VISUALISATION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.PRICING.getCode(),
                        Enumeration.AccessRight.NON_VISIBLE.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.TRACK.getCode(),
                        Enumeration.AccessRight.NON_VISIBLE.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.FREIGHT_ANALYTICS.getCode(),
                        Enumeration.AccessRight.NON_VISIBLE.getCode(),
                        ebUser.getEbUserNum()));
        }
        else if (ebUser.isChargeur()) {
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.PRICING.getCode(),
                        Enumeration.AccessRight.CONTRIBUTION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.TRANSPORT_MANAGEMENT.getCode(),
                        Enumeration.AccessRight.CONTRIBUTION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.TRACK.getCode(),
                        Enumeration.AccessRight.VISUALISATION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.QUALITY_MANAGEMENT.getCode(),
                        Enumeration.AccessRight.VISUALISATION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.FREIGHT_AUDIT.getCode(),
                        Enumeration.AccessRight.VISUALISATION.getCode(),
                        ebUser.getEbUserNum()));
            listExUserModule
                .add(
                    new ExUserModule(
                        Enumeration.Module.FREIGHT_ANALYTICS.getCode(),
                        Enumeration.AccessRight.VISUALISATION.getCode(),
                        ebUser.getEbUserNum()));
        }

        ebUser.setListAccessRights(listExUserModule);
        return inscrire(ebUser);
    }

    @Transactional
    public EbUser inscrire(EbUser ebUser) {
        String activeToken = Base64
            .getEncoder().encodeToString((ebUser.getEmail() + System.currentTimeMillis()).getBytes());
        ebUser.setActiveToken(activeToken);
        ebUser.setEmail(ebUser.getEmail().toLowerCase());

        if (StringUtils.isEmpty(ebUser.getUsername())) {
            ebUser.setUsername(ebUser.getEmail());
        }

        Integer ebCompagnieNum = ebUser.getEbEtablissement().getEbCompagnie().getEbCompagnieNum();
        Integer ebEtablissementNum = ebUser.getEbEtablissement().getEbEtablissementNum();
        boolean sendEmailNouveauCompteSociete = false;
        boolean sendEmailActivePassword = false;

        List<EbUser> adminsUser = new ArrayList<EbUser>();

        if (ebCompagnieNum != null) {

            if (ebEtablissementNum == null) {
                adminsUser = ebUserRepository.selectSuperAdmin(ebCompagnieNum);
            }
            else {
                adminsUser = ebUserRepository.selectSuperAdmin(ebCompagnieNum, ebEtablissementNum);
            }

            // adminUser = adminsUser.get(0);

            sendEmailNouveauCompteSociete = true;

            EbCompagnie compagnie = ebCompagnieRepository.findOneByEbCompagnieNum(ebCompagnieNum);
            ebUser.setEbCompagnie(compagnie);

            if (ebUser.getEbEtablissement().getEbEtablissementNum() != null) {
                EbEtablissement etab = null;
                etab = ebEtablissementRepository
                    .findOneByEbEtablissementNum(ebUser.getEbEtablissement().getEbEtablissementNum());
                ebUser.setEbEtablissement(etab);

                if (ebUser.getPassword() == null && ebUser.getStatus() == UserStatus.ACTIF.getCode()) {
                    String resetToken = Base64
                        .getEncoder().encodeToString((ebUser.getEmail() + System.currentTimeMillis()).getBytes());
                    ebUser.setResetToken(resetToken);
                    ebUser.setActiveToken(null);

                    emailService.sendEmailForCreatePassword(ebUser);
                    sendEmailNouveauCompteSociete = false;
                }

            }
            else {
                ebUser.getEbEtablissement().setDateCreation(new Date());
                ebUser.getEbEtablissement().setEbCompagnie(compagnie);
                ebUser.getEbEtablissement().setRole(ebUser.getRole());
            }

        }
        else {
            ebUser.setEbCompagnie(ebUser.getEbEtablissement().getEbCompagnie());
            ebUser.getEbEtablissement().getEbCompagnie().setCompagnieRole(ebUser.getRole());
            ebUser.setSuperAdmin(true);
            ebUser.setAdmin(true);
            ebUser.getEbEtablissement().setDateCreation(new Date());
            sendEmailActivePassword = true;
        }

        if (ebUser.getPassword() != null) {
            ebUser.setPassword(passwordEncoder.encode(ebUser.getPassword()));
            ebUser.setStatus(UserStatus.NON_ACTIF.getCode());
        }

        // régler le Pb:save the transient instance before flushing
        if (ebUser.getEbUserProfile() == null || ebUser.getEbUserProfile().getEbUserProfileNum() == null

        ) {
            ebUser.setEbUserProfile(null);
        }

        ebUser.setEnabled(true);

        // https://jira.adias.fr/browse/CPB-127
        // username must be saved in database in lowercase so it's not case
        // sensitive
        ebUser.setUsername(ebUser.getUsername().toLowerCase());
        ebUser.setLanguage(LanguageUser.ENGLISH.getKey());
        ebCompagnieRepository.save(ebUser.getEbCompagnie());
        ebEtablissementRepository.save(ebUser.getEbEtablissement());
        ebUserRepository.save(ebUser);

        // Saving Access right
        if (ebUser.getListAccessRights() != null && !ebUser.getListAccessRights().isEmpty()) {
            Set<ExUserModule> listAccessRights = ebUser.getListAccessRights();

            // pour éviter l'enregistrement en cascade des access rights
            ebUser.setListAccessRights(null);
            // cette methode devrait persister l'utilisateur et la liste des
            // modules par defaut.

            for (ExUserModule access: listAccessRights) {
                access.setEbUser(ebUser);
                access.setEbUserNum(ebUser.getEbUserNum());
                access.setModule(new EcModule(access.getEcModuleNum()));
            }

            exEbUserModuleRepository.saveAll(listAccessRights);
        }

        if (ebUser.getPassword() == null) {
            // TODO normalement cette methode n'est pas necessaire car la
            // methode save devrait persister la liste des Module par defaut
            // Cette ligne poses des soucis dans la création des users.
            // exEbUserModuleRepository.saveAll(ebUser.getListAccessRights());

            // TODO : je ne comprend pas
            userService.setRelationsByEmail(ebUser);
        }
        else {
            String lang = ebUserRepository.getLanguageByEmail("mytower2018+ct@gmail.com");
            emailService.sendEmailCreationNouvelleSociete(lang);
            sendEmailActivePassword = false;
        }

        if (sendEmailNouveauCompteSociete) {
            // Ne pas envoyer d'email quand il y'a pas d'admin établissement
            if (adminsUser.size() > 0) emailService.sendEmailNouveauCompteSociete(ebUser, adminsUser);
        }
        else if (sendEmailActivePassword) {
            String lang = ebUserRepository.getLanguageByEmail("mytower2018+ct@gmail.com");
            emailService.sendEmailCreationNouvelleSociete(lang);
        }

        return ebUser;
    }

    @Override
    public boolean checkUserByEmail(String email) {
        return ebUserRepository.existsByEmail(email);
    }

    @Override
    public boolean checkUserByUsername(String username) {
        return ebUserRepository.existsByUsername(username);
    }

    @Override
    public EbEtablissement getEbEtablissementBySiret(String siret) {
        return ebEtablissementRepository.findOneBySiret(siret);
    }

    @Override
    public EbCompagnie getEbCompagnieBySiren(String siren) {
        return ebCompagnieRepository.findOneBySiren(siren);
    }
}
