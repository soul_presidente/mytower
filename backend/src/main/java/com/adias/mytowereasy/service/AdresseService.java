package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.EbAdresse;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface AdresseService {
    public List<EbPlZone> getListZonesByCompanyNumAndAdresseRef(Integer ebCompanieNum, String refAdresse);

    Long getListAddressesCount(SearchCriteria criteria);

    List<EbAdresse> getEbAdresseByCriteria(SearchCriteria criteria);

    boolean areSameAddresses(EbParty party1, EbParty party2);
}
