/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.qm.EbQmRootCause;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


public interface RootCauseService {
    boolean saveRootCauses(EbQmRootCause rootCause);

    public Long getCountListRootCause(SearchCriteriaQM criteria) throws Exception;

    List<EbQmRootCause> listRootCause(SearchCriteriaQM params);
}
