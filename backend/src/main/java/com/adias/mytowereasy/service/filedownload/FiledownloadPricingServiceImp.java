package com.adias.mytowereasy.service.filedownload;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.Enumeration.Module;
import com.adias.mytowereasy.model.Enumeration.StatutCarrier;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.PricingService;
import com.adias.mytowereasy.service.category.CategoryService;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Service
public class FiledownloadPricingServiceImp extends MyTowerService implements FiledownloadPricingService {
    private static String QUOTE = "Quote";
    private static String TRANSPORT_PLAN = "Transport Plan";
    @Autowired
    PricingService pricingService;
    @Autowired
    CategoryService categoryService;

    @Autowired
    MessageSource messageSource;

    @Override
    public FileDownloadStateAndResult listFileDownLoadPricing(
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        List<EbCategorie> listCategorie,
        SearchCriteria searchCriteria)
        throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();
        Map<String, String> listTypeResult = new LinkedHashMap<String, String>();

        List<EbCustomField> fields = new ArrayList<EbCustomField>();

        ObjectMapper objectMapper = new ObjectMapper();
        SearchCriteriaPricingBooking pricingBooking = objectMapper
            .convertValue(searchCriteria, SearchCriteriaPricingBooking.class);

        List<EbDemande> listEbDemande = pricingService.getListEbDemande(pricingBooking);

        /* Gestion des categories, champs parametrables, favories */
        Map<String, Object> listcatAndCustom = categoryService
            .getListCategoryAndCustomField(searchCriteria, connectedUser);

        fields = (List<EbCustomField>) listcatAndCustom.get("customField");
        listCategorie = (List<EbCategorie>) listcatAndCustom.get("listCategorie");
        List<EbFavori> listEbFavori = new ArrayList<EbFavori>();

        Locale locale = connectedUser.getLocaleFromLanguage();

        DateFormat dateFormat = new SimpleDateFormat(connectedUser.getDateFormatFromLocale());

        if (listEbDemande != null && listEbDemande.size() > 0 && isPropertyInDtConfig(dtConfig, "btn-favori")) {
            List<Integer> listEbDemandeNum = listEbDemande
                .stream().map(it -> it.getEbDemandeNum()).collect(Collectors.toList());
            listEbFavori = ebFavoriRepository
                .findAllByModuleAndUserEbUserNumAndIdObjectIn(
                    searchCriteria.getModule(),
                    connectedUser.getEbUserNum(),
                    listEbDemandeNum);
        }

        for (EbDemande ebDemande: listEbDemande) {
            Map<String, String[]> map = new LinkedHashMap<String, String[]>();
            List<EbTypeDocuments> listTypeDocument = null;
            listTypeDocument = ebDemande.getListTypeDocuments();

            if (ebDemande.getUser() != null) {
                map.put("nomPrenomChargeur", new String[]{
                    "Nom/Prenom chargeur", ebDemande.getUser().getNomPrenom()
                });
                map.put("emailChargeur", new String[]{
                    "Email chargeur", ebDemande.getUser().getEmail()
                });
            }
            else {
                map.put("nomPrenomChargeur", new String[]{
                    "Prenom chargeur", null
                });
                map.put("emailChargeur", new String[]{
                    "Email chargeur", null
                });
            }

            if (isPropertyInDtConfig(dtConfig, "refTransport")) {
                String header = null;

                if (searchCriteria.getModule().equals(Module.PRICING.getCode())) {
                    header = messageSource.getMessage("pricing_booking.pricing_reference", null, locale);
                }
                else if (searchCriteria.getModule().equals(Module.TRANSPORT_MANAGEMENT.getCode())) {
                    header = messageSource.getMessage("pricing_booking.quotation_reference", null, locale);
                }

                map.put("refTransport", new String[]{
                    header, ebDemande.getRefTransport()
                });
            }

            if (searchCriteria.getModule().equals(Module.PRICING.getCode()) &&
                isPropertyInDtConfig(dtConfig, "curentTimeRemainingInMilliSecond")) {
                addTimeSinceCreationToMap(locale, ebDemande, map);
            }

            if (isPropertyInDtConfig(dtConfig, "customerReference")) map.put("customerReference", new String[]{
                messageSource.getMessage("track_trace.customer_reference", null, locale),
                ebDemande.getCustomerReference()
            });
            if (isPropertyInDtConfig(dtConfig, "unitsReference")) map.put("unitsReference", new String[]{
                messageSource.getMessage("track_trace.unit_reference", null, locale), ebDemande.getUnitsReference()
            });
            if (isPropertyInDtConfig(dtConfig, "unitsPackingList")) map.put("unitsPackingList", new String[]{
                messageSource.getMessage("type_unit.packing_list", null, locale), ebDemande.getUnitsPackingList()
            });
            if (isPropertyInDtConfig(dtConfig, "xEcIncotermLibelle")) map.put("xEcIncotermLibelle", new String[]{
                messageSource.getMessage("pricing_booking.incoterms", null, locale), ebDemande.getxEcIncotermLibelle()
            });

            if (isPropertyInDtConfig(dtConfig, "numAwbBol")) map.put("numAwbBol", new String[]{
                messageSource.getMessage("pricing_booking.num_awb_bol", null, locale), ebDemande.getNumAwbBol()
            });
            if (isPropertyInDtConfig(dtConfig, "libelleOriginCountry")) map.put("libelleOriginCountry", new String[]{
                messageSource.getMessage("pricing_booking.origin", null, locale), ebDemande.getLibelleOriginCountry()
            });
            if (isPropertyInDtConfig(dtConfig, "typeRequestLibelle")) map.put("typeRequestLibelle", new String[]{
                messageSource.getMessage("pricing_booking.service_level", null, locale),
                ebDemande.getTypeRequestLibelle()
            });

            if (isPropertyInDtConfig(dtConfig, "user")) {
                String fullName = "";

                if (ebDemande.getUser() != null) {
                    fullName = ebDemande.getUser().getNomPrenom();
                }

                map.put("user", new String[]{
                    messageSource.getMessage("pricing_booking.requestor", null, locale),
                    fullName != null ? fullName : ""
                });
            }

            if (listTypeDocument != null && listTypeDocument.size() > 0) {
                String codeDoc = "";

                for (EbTypeDocuments cat: listTypeDocument) {

                    if (isPropertyInDtConfig(dtConfig, "listTypeDocuments")) {
                        codeDoc += cat.getNom() + "(" + (cat.getNbrDoc() != null ? cat.getNbrDoc() : 0) + ")";
                        codeDoc += " \r\n";
                    }

                }

                map.put("listTypeDocuments", new String[]{
                    messageSource.getMessage("pricing_booking.statut_document", null, locale), codeDoc
                });
            }
            else {
                map.put("listTypeDocuments", new String[]{
                    messageSource.getMessage("pricing_booking.statut_document", null, locale), ""
                });
            }

            if (isPropertyInDtConfig(
                dtConfig,
                "strCurentTimeRemaining")) map.put("strCurentTimeRemaining", new String[]{
                    messageSource.getMessage("pricing_booking.time_remaining", null, locale),
                    ebDemande.getTimeRemaining() != null ? ebDemande.getTimeRemaining().toString() : null
            });

            if (isPropertyInDtConfig(dtConfig, "eligibleForConsolidation")) {
                String value = "No";

                if (ebDemande.getEligibleForConsolidation() != null && ebDemande.getEligibleForConsolidation()) {
                    value = "Yes";
                }

                map.put("eligibleForConsolidation", new String[]{
                    messageSource.getMessage("pricing_booking.options.eligible_for_consolidation", null, locale), value
                });
            }

            if (isPropertyInDtConfig(dtConfig, "xEcNature")) map.put("xEcNature", new String[]{
                messageSource.getMessage("pricing_booking.nature", null, locale),
                ebDemande.getxEcNature() != null ? ebDemande.getxEcNature().toString() : null
            });
            if (isPropertyInDtConfig(dtConfig, "libelleOriginCity")) map.put("libelleOriginCity", new String[]{
                messageSource.getMessage("pricing_booking.orig_city", null, locale), ebDemande.getLibelleOriginCity()
            });
            if (isPropertyInDtConfig(dtConfig, "companyOrigin")) map.put("companyOrigin", new String[]{
                messageSource.getMessage("pricing_booking.company_origin", null, locale), ebDemande.getCompanyOrigin()
            });
            if (isPropertyInDtConfig(dtConfig, "ebDemandeMasterObject")) map.put("ebDemandeMasterObject", new String[]{
                messageSource.getMessage("pricing_booking.master_object", null, locale),
                ebDemande.getEbDemandeMasterObject() != null ?
                    ebDemande.getEbDemandeMasterObject().getRefTransport() :
                    null
            });
            if (isPropertyInDtConfig(dtConfig, "libelleDestCountry")) map.put("libelleDestCountry", new String[]{
                messageSource.getMessage("pricing_booking.destination", null, locale), ebDemande.getLibelleDestCountry()
            });
            if (isPropertyInDtConfig(dtConfig, "libelleDestCity")) map.put("libelleDestCity", new String[]{
                messageSource.getMessage("pricing_booking.dest_city", null, locale), ebDemande.getLibelleDestCity()
            });

            if (isPropertyInDtConfig(dtConfig, "totalWeight")) map.put("totalWeight", new String[]{
                messageSource.getMessage("pricing_booking.total_weight", null, locale),
                ebDemande.getTotalWeight() != null ? ebDemande.getTotalWeight().toString() : null
            });
            if (isPropertyInDtConfig(dtConfig, "xEcModeTransport")) map.put("xEcModeTransport", new String[]{
                messageSource.getMessage("pricing_booking.mode_of_transport", null, locale),
                ebDemande.getxEcModeTransport() != null ?
                    ModeTransport.getLibelleByCode(ebDemande.getxEcModeTransport()) :
                    null
            });

            if (isPropertyInDtConfig(dtConfig, "xEbCostCenter")) {
                if (ebDemande.getxEbCostCenter() != null) map.put("xEbCostCenter", new String[]{
                    messageSource.getMessage("pricing_booking.cost_center", null, locale),
                    ebDemande.getxEbCostCenter().getLibelle()
                });
                else map.put("xEbCostCenter", new String[]{
                    messageSource.getMessage("pricing_booking.cost_center", null, locale), null
                });
            }

            if (isPropertyInDtConfig(dtConfig, "dateOfGoodsAvailability")) {
                map.put("dateOfGoodsAvailability", new String[]{
                    messageSource.getMessage("pricing_booking.date_of_goods_availability", null, locale),
                    ebDemande.getDateOfGoodsAvailability() != null ?
                        dateFormat.format(ebDemande.getDateOfGoodsAvailability()) :
                        ""
                });
                listTypeResult.put("dateOfGoodsAvailability", Date.class.getTypeName());
            }

            if (isPropertyInDtConfig(dtConfig, "customerCreationDate")) {
                map.put("customerCreationDate", new String[]{
                    messageSource.getMessage("pricing_booking.customer_creation_date", null, locale),
                    ebDemande.getCustomerCreationDate() != null ?
                        dateFormat.format(ebDemande.getCustomerCreationDate()) :
                        ""
                });
                listTypeResult.put("customerCreationDate", Date.class.getTypeName());
            }

            if (isPropertyInDtConfig(dtConfig, "finalDelivery") &&
                ebDemande.getExEbDemandeTransporteurs() != null && ebDemande.getExEbDemandeTransporteurs().size() > 0) {
                map.put("finalDelivery", new String[]{
                    messageSource.getMessage("pricing_booking.psl_end", null, locale),
                    ebDemande.getExEbDemandeTransporteurs().get(0).getDeliveryTime() != null ?
                        ebDemande.getExEbDemandeTransporteurs().get(0).getDeliveryTime().toString() :
                        ""
                });
            }
            else map.put("finalDelivery", new String[]{
                messageSource.getMessage("pricing_booking.psl_end", null, locale), ""
            });

            if (isPropertyInDtConfig(dtConfig, "confirmPickup")) map.put("confirmPickup", new String[]{
                messageSource.getMessage("pricing_booking.pickup_confirmation", null, locale),
                ebDemande.getConfirmPickup() != null && ebDemande.getConfirmPickup() ? "OK" : "KO"
            });
            if (isPropertyInDtConfig(dtConfig, "confirmDelivery")) map.put("confirmDelivery", new String[]{
                messageSource.getMessage("pricing_booking.delivery_confirmation", null, locale),
                ebDemande.getConfirmDelivery() != null && ebDemande.getConfirmDelivery() ? "OK" : "KO"
            });

            if (isPropertyInDtConfig(dtConfig, "dateCreation")) {
                map.put("dateCreation", new String[]{
                    messageSource.getMessage("pricing_booking.request_date", null, locale),
                    ebDemande.getDateCreation() != null ? dateFormat.format(ebDemande.getDateCreation()) : null
                });
                listTypeResult.put("dateCreation", Date.class.getTypeName());
            }

            if (isPropertyInDtConfig(dtConfig, "updateDate")) {
                map.put("updateDate", new String[]{
                    messageSource.getMessage("pricing_booking.date_update", null, locale),
                    ebDemande.getUpdateDate() != null ? dateFormat.format(ebDemande.getUpdateDate()) : null
                });
                listTypeResult.put("updateDate", Date.class.getTypeName());
            }

            if (isPropertyInDtConfig(dtConfig, "xEbUserCt")) map.put("xEbUserCt", new String[]{
                "Name of requestor", ebDemande.getxEbUserCt().getNomPrenom()
            });
            if (isPropertyInDtConfig(dtConfig, "xEbChargeur")) map.put("xEbChargeur", new String[]{
                "Name of owner", ebDemande.getUser().getNomPrenom()
            });
            if (isPropertyInDtConfig(dtConfig, "xEcTypeDemande")) map.put("xEcTypeDemande", new String[]{
                messageSource.getMessage("pricing_booking.type_of_request", null, locale),
                ebDemande.getxEcTypeDemande() != null ?
                    Enumeration.TypeDemande.getLibelleByCode(ebDemande.getxEcTypeDemande()) :
                    null
            });
            if (isPropertyInDtConfig(dtConfig, "statutStr")) map.put("statutStr", new String[]{
                messageSource.getMessage("pricing_booking.status", null, locale), ebDemande.getStatutStr()
            });

            if (isPropertyInDtConfig(dtConfig, "insurance")) {
                String value = "No";

                if (ebDemande.getInsurance()) {
                    value = "Yes";
                }

                map.put("insurance", new String[]{
                    messageSource.getMessage("pricing_booking.insurance", null, locale), value
                });
            }

            if (isPropertyInDtConfig(dtConfig, "xecCurrencyInvoice")) map.put("xecCurrencyInvoice", new String[]{
                messageSource.getMessage("pricing_booking.invoice_currency", null, locale),
                ebDemande.getXecCurrencyInvoice().getCode()
            });

            if (isPropertyInDtConfig(dtConfig, "totalVolume")) map.put("totalVolume", new String[]{
                messageSource.getMessage("pricing_booking.total_volume", null, locale),
                ebDemande.getTotalVolume() != null ? ebDemande.getTotalVolume().toString() : ""
            });

            if (isPropertyInDtConfig(dtConfig, "xEbSchemaPsl")) map.put("xEbSchemaPsl", new String[]{
                messageSource.getMessage("pricing_booking.schema_psl", null, locale),
                ebDemande.getxEbSchemaPsl() != null ? ebDemande.getxEbSchemaPsl().getDesignation() : ""
            });

            if (isPropertyInDtConfig(dtConfig, "city")) map.put("city", new String[]{
                messageSource.getMessage("pricing_booking.villeincoterm", null, locale), ebDemande.getCity()
            });

            if (isPropertyInDtConfig(dtConfig, "totalNbrParcel")) map.put("totalNbrParcel", new String[]{
                messageSource.getMessage("pricing_booking.number_of_units", null, locale),
                ebDemande.getTotalNbrParcel() != null ? ebDemande.getTotalNbrParcel().toString() : ""
            });

            if (isPropertyInDtConfig(dtConfig, "waitingForQuote")) {
                map.put("waitingForQuote", new String[]{
                    messageSource.getMessage("pricing_booking.waiting_for_quote_date", null, locale),
                    ebDemande.getWaitingForQuote() != null ? dateFormat.format(ebDemande.getWaitingForQuote()) : ""
                });
                listTypeResult.put("waitingForQuote", Date.class.getTypeName());
            }

            if (isPropertyInDtConfig(dtConfig, "waitingForConf")) {
                map.put("waitingForConf", new String[]{
                    messageSource.getMessage("pricing_booking.waiting_for_confirmation_date", null, locale),
                    ebDemande.getWaitingForConf() != null ? dateFormat.format(ebDemande.getWaitingForConf()) : ""
                });
                listTypeResult.put("waitingForConf", Date.class.getTypeName());
            }

            if (isPropertyInDtConfig(dtConfig, "confirmed")) {
                map.put("confirmed", new String[]{
                    messageSource.getMessage("pricing_booking.confirmation_date", null, locale),
                    ebDemande.getConfirmed() != null ? dateFormat.format(ebDemande.getConfirmed()) : ""
                });
                listTypeResult.put("confirmed", Date.class.getTypeName());
            }

            if (isPropertyInDtConfig(dtConfig, "valuationType")) map.put("valuationType", new String[]{
                messageSource.getMessage("pricing_booking.valuation_type", null, locale),
                ebDemande.getValuationType() != null ?
                    (StatutCarrier.FIN.getCode().equals(ebDemande.getValuationType()) ? QUOTE : TRANSPORT_PLAN) :
                    null
            });

            if (isPropertyInDtConfig(dtConfig, "listEbFlagDTO")) {
                String listFlagsLibelle = "";

                if (ebDemande.getListEbFlagDTO() != null && ebDemande.getListEbFlagDTO().size() > 0) {

                    for (EbFlagDTO ebFlagDTO: ebDemande.getListEbFlagDTO()) {
                        listFlagsLibelle += ebFlagDTO.getName() + ", ";
                    }

                }

                if (listFlagsLibelle.length()
                    > 0) listFlagsLibelle = listFlagsLibelle.substring(0, listFlagsLibelle.length() - 2);
                map.put("listEbFlagDTO", new String[]{
                    messageSource.getMessage("pricing_booking.flag", null, locale), listFlagsLibelle
                });
            }

            if (searchCriteria.getModule().equals(Module.TRANSPORT_MANAGEMENT.getCode())) {
                if (isPropertyInDtConfig(dtConfig, "libelleLastPsl")) map.put("libelleLastPsl", new String[]{
                    messageSource.getMessage("track_trace.libelle_last_psl", null, locale),
                    ebDemande.getLibelleLastPsl()
                });

                if (isPropertyInDtConfig(dtConfig, "dateLastPsl")) {
                    map.put("dateLastPsl", new String[]{
                        messageSource.getMessage("track_trace.date_last_psl", null, locale),
                        ebDemande.getDateLastPsl() != null ? dateFormat.format(ebDemande.getDateLastPsl()) : ""
                    });
                    listTypeResult.put("dateLastPsl", Date.class.getTypeName());
                }

                if (ebDemande.getExEbDemandeTransporteurs() != null &&
                    ebDemande.getExEbDemandeTransporteurs().size() > 0) {

                    if (isPropertyInDtConfig(dtConfig, "prixTransporteurFinal")) {
                        if (ebDemande.getPrixTransporteurFinal() != null) map.put("prixTransporteurFinal", new String[]{
                            messageSource.getMessage("pricing_booking.price", null, locale),
                            ebDemande.getPrixTransporteurFinal().toString()
                        });
                        else map.put("prixTransporteurFinal", new String[]{
                            messageSource.getMessage("pricing_booking.price", null, locale), null
                        });
                    }

                    if (isPropertyInDtConfig(dtConfig, "etablissementTransporteur")) {
                        if (ebDemande.getExEbDemandeTransporteurs().get(0).getxEbEtablissement()
                            != null) map.put("etablissementTransporteur", new String[]{
                                messageSource.getMessage("pricing_booking.etablissement_transporteur", null, locale),
                                ebDemande.getExEbDemandeTransporteurs().get(0).getxEbEtablissement().getNom()
                        });
                        else map.put("etablissementTransporteur", new String[]{
                            messageSource.getMessage("pricing_booking.etablissement_transporteur", null, locale), null
                        });
                    }

                    if (isPropertyInDtConfig(dtConfig, "companyTransporteur")) {
                        if (ebDemande.getExEbDemandeTransporteurs().get(0).getxEbCompagnie()
                            != null) map.put("companyTransporteur", new String[]{
                                messageSource.getMessage("pricing_booking.company_transporteur", null, locale),
                                ebDemande.getExEbDemandeTransporteurs().get(0).getxEbCompagnie().getNom()
                        });
                        else map.put("companyTransporteur", new String[]{
                            messageSource.getMessage("pricing_booking.company_transporteur", null, locale), null
                        });
                    }

                    if (isPropertyInDtConfig(dtConfig, "datePickup")) {
                        if (ebDemande.getExEbDemandeTransporteurs().get(0).getPickupTime()
                            != null) map.put("datePickup", new String[]{
                                messageSource.getMessage("pricing_booking.date_pickup_negotiated", null, locale),
                                dateFormat.format(ebDemande.getExEbDemandeTransporteurs().get(0).getPickupTime())
                        });
                        else map.put("datePickup", new String[]{
                            messageSource.getMessage("pricing_booking.date_pickup_negotiated", null, locale), null
                        });
                        listTypeResult.put("datePickup", Date.class.getTypeName());
                    }

                    if (isPropertyInDtConfig(dtConfig, "dateDelivery")) {
                        if (ebDemande.getExEbDemandeTransporteurs().get(0).getDeliveryTime()
                            != null) map.put("dateDelivery", new String[]{
                                messageSource.getMessage("pricing_booking.date_delivery_negotiated", null, locale),
                                dateFormat.format(ebDemande.getExEbDemandeTransporteurs().get(0).getDeliveryTime())
                        });
                        else map.put("dateDelivery", new String[]{
                            messageSource.getMessage("pricing_booking.date_delivery_negotiated", null, locale), null
                        });
                        listTypeResult.put("dateDelivery", Date.class.getTypeName());
                    }

                    if (isPropertyInDtConfig(dtConfig, "flagDocumentTransporteur")) {
                        String docs = "";
                        if (ebDemande.getFlagDocumentTransporteur() != null &&
                            ebDemande.getFlagDocumentTransporteur()) docs += "OK";
                        else docs += "MISSING";

                        map.put("flagDocumentTransporteur", new String[]{
                            "Transport Docs", docs
                        });
                    }

                    if (isPropertyInDtConfig(dtConfig, "flagDocumentCustoms")) {
                        String docs = "";
                        if (ebDemande.getFlagDocumentCustoms() != null &&
                            ebDemande.getFlagDocumentCustoms()) docs += "OK";
                        else if (!Enumeration.OriginCustomsBroker.NON_APPLICABLE
                            .getCode().equals(ebDemande.getxEcTypeCustomBroker())) docs += "MISSING";

                        map.put("flagDocumentCustoms", new String[]{
                            messageSource.getMessage("pricing_booking.doc_customs_status", null, locale), docs
                        });
                    }

                    if (isPropertyInDtConfig(dtConfig, "flagTransportInformation")) {
                        String docs = "";
                        if (ebDemande.getFlagTransportInformation() != null &&
                            ebDemande.getFlagTransportInformation()) docs += "OK";
                        else docs += "MISSING";

                        map.put("flagTransportInformation", new String[]{
                            messageSource.getMessage("pricing_booking.transport_infos_status", null, locale), docs
                        });
                    }

                    if (isPropertyInDtConfig(dtConfig, "datePickupTM")) {
                        map.put("datePickupTM", new String[]{
                            messageSource.getMessage("pricing_booking.psl_start", null, locale),
                            ebDemande.getDatePickupTM() != null ? dateFormat.format(ebDemande.getDatePickupTM()) : null
                        });
                        listTypeResult.put("datePickupTM", Date.class.getTypeName());
                    }

                    if (isPropertyInDtConfig(dtConfig, "carriers")) {
                        map.put("carriers", new String[]{
                            "Carriers", ebDemande.getExEbDemandeTransporteurFinal().getNomPrenom()
                        });
                    }

                    if (isPropertyInDtConfig(dtConfig, "documents")) {
                        map.put("documents", new String[]{
                            "Documents", null
                        });
                    }

                }
                else {
                    if (isPropertyInDtConfig(dtConfig, "price")) map.put("price", new String[]{
                        "Price", null
                    });
                    if (isPropertyInDtConfig(
                        dtConfig,
                        "etablissementTransporteur")) map.put("etablissementTransporteur", new String[]{
                            messageSource.getMessage("pricing_booking.etablissement_transporteur", null, locale), null
                    });
                    if (isPropertyInDtConfig(
                        dtConfig,
                        "companyTransporteur")) map.put("companyTransporteur", new String[]{
                            messageSource.getMessage("pricing_booking.company_transporteur", null, locale), null
                    });

                    if (isPropertyInDtConfig(dtConfig, "datePickup")) {
                        map.put("datePickup", new String[]{
                            messageSource.getMessage("pricing_booking.date_pickup_negotiated", null, locale), null
                        });
                        listTypeResult.put("datePickup", Date.class.getTypeName());
                    }

                    if (isPropertyInDtConfig(dtConfig, "dateDelivery")) {
                        map.put("dateDelivery", new String[]{
                            messageSource.getMessage("pricing_booking.date_delivery_negotiated", null, locale), null
                        });
                        listTypeResult.put("dateDelivery", Date.class.getTypeName());
                    }

                }

                if (ebDemande.getExEbDemandeTransporteurFinal() != null) {
                    if (isPropertyInDtConfig(
                        dtConfig,
                        "nomPrenomTransporteur")) map.put("nomPrenomTransporteur", new String[]{
                            "Nom/Prenom transporteur", ebDemande.getExEbDemandeTransporteurFinal().getNomPrenom()
                    });
                    if (isPropertyInDtConfig(dtConfig, "emailTransporteur")) map.put("emailTransporteur", new String[]{
                        messageSource.getMessage("pricing_booking.email_transporteur", null, locale),
                        ebDemande.getExEbDemandeTransporteurFinal().getEmail()
                    });
                }
                else {
                    if (isPropertyInDtConfig(
                        dtConfig,
                        "nomPrenomTransporteur")) map.put("nomPrenomTransporteur", new String[]{
                            "Prenom transporteur", null
                    });
                    if (isPropertyInDtConfig(dtConfig, "emailTransporteur")) map.put("emailTransporteur", new String[]{
                        messageSource.getMessage("pricing_booking.email_transporteur", null, locale), null
                    });
                }

            }

            if (listEbFavori.size() > 0 && isPropertyInDtConfig(dtConfig, "btn-favori")) {
                Optional<EbFavori> opt = listEbFavori
                    .stream().filter(it -> it.getIdObject().equals(ebDemande.getEbDemandeNum())).findFirst();
                if (opt.isPresent()) map.put("btn-favori", new String[]{
                    "Favori", "Oui"
                });
                else map.put("btn-favori", new String[]{
                    "Favori", "Non"
                });
            }
            else if (isPropertyInDtConfig(dtConfig, "btn-favori")) {
                map.put("btn-favori", new String[]{
                    "Favori", "Non"
                });
            }

            if (listCategorie != null && listCategorie.size() > 0) {

                for (EbCategorie cat: listCategorie) {
                    if (isPropertyInDtConfig(dtConfig, "category" + cat.getEbCategorieNum())) map
                        .put("category" + cat.getEbCategorieNum(), new String[]{
                            cat.getLibelle(), null
                        });
                }

            }

            if (listCategorie != null && listCategorie.size() > 0) {

                for (EbCategorie cat: listCategorie) {
                    if (isPropertyInDtConfig(dtConfig, "category" + cat.getEbCategorieNum())) map
                        .put("category" + cat.getEbCategorieNum(), new String[]{
                            cat.getLibelle(), null
                        });
                }

            }

            if (fields != null && fields.size() > 0) {

                for (EbCustomField customFields: fields) {
                    List<CustomFields> field = new ArrayList<CustomFields>();

                    field = gson.fromJson(customFields.getFields(), new TypeToken<ArrayList<CustomFields>>() {
                    }.getType());

                    if (field != null && field.size() > 0) {

                        for (CustomFields fie: field) {
                            if (isPropertyInDtConfig(dtConfig, fie.getName())) map.put(fie.getName(), new String[]{
                                fie.getLabel(), null
                            });
                        }

                    }

                    if (field != null &&
                        ebDemande.getListCustomsFields() != null && (ebDemande
                            .getUser().getEbCompagnie().getEbCompagnieNum().equals(customFields.getxEbCompagnie()))) {

                        for (CustomFields fie: field) {

                            if (isPropertyInDtConfig(dtConfig, fie.getName())) {

                                for (CustomFields demField: ebDemande.getListCustomsFields()) {

                                    if (demField.getName().contentEquals(fie.getName())) {
                                        map.put(fie.getName(), new String[]{
                                            fie.getLabel(), demField.getValue()
                                        });
                                        break;
                                    }

                                }

                            }

                        }

                    }

                }

            }

            if (listCategorie != null && ebDemande.getListCategories() != null) {

                for (EbCategorie categorie: listCategorie) {

                    if (isPropertyInDtConfig(dtConfig, "category" + categorie.getEbCategorieNum())) {

                        for (EbCategorie demCategorie: ebDemande.getListCategories()) {

                            if (demCategorie.getLabels() != null &&
                                demCategorie.getEbCategorieNum().equals(categorie.getEbCategorieNum())) {
                                String label = null;

                                for (EbLabel ebLabel: demCategorie.getLabels()) {
                                    label = ebLabel.getLibelle();
                                    break;
                                }

                                map.put("category" + categorie.getEbCategorieNum(), new String[]{
                                    categorie.getLibelle(), label
                                });
                                break;
                            }

                        }

                    }

                }

            }

            if (isPropertyInDtConfig(dtConfig, "carrierUniqRefNum")) map.put("carrierUniqRefNum", new String[]{
                messageSource.getMessage("request_overview.carrier_uniq_ref_num", null, locale),
                ebDemande.getCarrierUniqRefNum()
            });

            if (isPropertyInDtConfig(dtConfig, "transportConfirmation")) {
                String transportConfirmation = "";

                if (ebDemande.getAskForTransportResponsibility() != null &&
                    ebDemande.getAskForTransportResponsibility() && ebDemande.getProvideTransport() != null &&
                    ebDemande.getProvideTransport()) {
                    transportConfirmation = "Confirmed";
                }
                else if (ebDemande.getAskForTransportResponsibility() != null &&
                    ebDemande.getAskForTransportResponsibility()) {
                        transportConfirmation = "Waiting for confirmation";
                    }

                map.put("transportConfirmation", new String[]{
                    messageSource.getMessage("pricing_booking.transport_confirmation", null, locale),
                    transportConfirmation
                });
            }

            if (isPropertyInDtConfig(dtConfig, "tdcAcknowledgeDate")) {
                map.put("tdcAcknowledgeDate", new String[]{
                    messageSource.getMessage("pricing_booking.acknowledge_date_by_ct", null, locale),
                    ebDemande.getTdcAcknowledgeDate() != null ?
                        dateFormat.format(ebDemande.getTdcAcknowledgeDate()) :
                        null
                });
                listTypeResult.put("tdcAcknowledgeDate", Date.class.getTypeName());
            }

            // adding data
            listresult.add(map);
        }

        resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
        resultAndState.setListResult(listresult);
        resultAndState.setListTypeResult(listTypeResult);
        return resultAndState;
    }

    private void addTimeSinceCreationToMap(Locale locale, EbDemande ebDemande, Map<String, String[]> map) {

        if (ebDemande.getCurentTimeRemainingInMilliSecond() != null) {
            long diff = ebDemande.getCurentTimeRemainingInMilliSecond();
            long diffSeconds = diff / 1000 % 60;
            long diffMinutes = diff / (60 * 1000) % 60;
            long diffHours = diff / (60 * 60 * 1000) % 24;
            long diffDays = diff / (24 * 60 * 60 * 1000);

            map.put("curentTimeRemainingInMilliSecond", new String[]{
                messageSource.getMessage("pricing_booking.time_remaining", null, locale),
                diffDays + "d " + diffHours + "h " + diffMinutes + "m " + diffSeconds + "s"
            });
        }
        else map.put("curentTimeRemainingInMilliSecond", new String[]{
            messageSource.getMessage("pricing_booking.time_remaining", null, locale), null
        });

    }

    @Override
    public List<Map<String, String[]>> extractPricing() {
        return this.daoFileDownloader.extractGDPricing();
    }

    @Override
    public List<Map<String, String[]>> extractTM() {
        return daoFileDownloader.extractTM();
    }

    @Override
    public List<Map<String, String[]>> extractIncoherenceStatutTTPSL() {
        return daoFileDownloader.extractIncoherenceStatutTTPSL();
    }

    @Override
    public List<Map<String, String[]>> extractIncoherenceStatutTMTT() {
        return daoFileDownloader.extractIncoherenceStatutTMTT();
    }

    @Override
    public List<Map<String, String[]>> extractSavings() {
        return daoFileDownloader.extractSavings();
    }

    @Override
    public List<Map<String, String[]>> extractGlobalData(String key) {
        List<Map<String, String[]>> res = new ArrayList();

        if (key.contentEquals("users")) res = daoFileDownloader.extractGDUsers();
        else if (key.contentEquals("pricing")) res = daoFileDownloader.extractGDPricing();
        else if (key.contentEquals("transport_management")) res = daoFileDownloader.extractTM();
        else if (key.contentEquals("access_right")) res = daoFileDownloader.extractGDAccessRight();
        else if (key.contentEquals("adress")) res = daoFileDownloader.extractGDAdress();
        else if (key.contentEquals("categories")) res = daoFileDownloader.extractGDCategories();
        else if (key.contentEquals("labels")) res = daoFileDownloader.extractGDLabels();
        else if (key.contentEquals("champs_param")) res = daoFileDownloader.extractGDChampsParam();
        else if (key.contentEquals("currencies")) res = daoFileDownloader.extractGDCurrencies();
        else if (key.contentEquals("documents")) res = daoFileDownloader.extractGDDocuments();
        else if (key.contentEquals("incoterms")) res = daoFileDownloader.extractGDIncoterms();
        else if (key.contentEquals("community")) res = daoFileDownloader.extractGDCommunity();
        else if (key.contentEquals("masques")) res = daoFileDownloader.extractGDMasques();
        else if (key.contentEquals("dates_psl")) res = daoFileDownloader.extractGDDatesPsl();

        return res;
    }
}
