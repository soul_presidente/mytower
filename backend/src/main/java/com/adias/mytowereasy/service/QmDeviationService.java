package com.adias.mytowereasy.service;

import com.adias.mytowereasy.model.qm.EbQmIncident;


public interface QmDeviationService {
    void addEbQmDeviation(EbQmIncident deviation);

    void transferDeviationsToIncidents();
}
