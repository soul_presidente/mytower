package com.adias.mytowereasy.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbCostCenter;


@Service
public interface CostCenterService {
    List<EbCostCenter> getListCostCenterCompagnie(Integer ebCompagnieNum);

    public EbCostCenter addEbCostCenterEbCompagnie(EbCostCenter ebCostCenter);

    public EbCostCenter updateEbCostCenterEbCompagnie(EbCostCenter ebCostCenter);

    public Boolean deleteEbCostCenterEbCompagnie(EbCostCenter ebCostCenter);
}
