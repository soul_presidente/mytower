/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.exception.DataException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.jpa.repository.JpaContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;
import com.adias.mytowereasy.airbus.model.EbQrGroupeProposition;
import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.api.service.PlanTransportApiService;
import com.adias.mytowereasy.api.wso.PlanTransportWSO;
import com.adias.mytowereasy.dao.DaoAdresse;
import com.adias.mytowereasy.dao.DaoPrMtcTransporteur;
import com.adias.mytowereasy.dao.DaoTypeUnit;
import com.adias.mytowereasy.delivery.enumeration.StatusDelivery;
import com.adias.mytowereasy.delivery.model.EbLivraison;
import com.adias.mytowereasy.delivery.model.EbLivraisonLine;
import com.adias.mytowereasy.delivery.repository.EbLivraisonLineRepository;
import com.adias.mytowereasy.delivery.repository.EbLivraisonRepository;
import com.adias.mytowereasy.delivery.service.DeliveryService;
import com.adias.mytowereasy.delivery.service.OrderService;
import com.adias.mytowereasy.dto.*;
import com.adias.mytowereasy.dto.mapper.TransportMapper;
import com.adias.mytowereasy.enumeration.ExportControlEnum;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.kafka.service.KafkaObjectService;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.*;
import com.adias.mytowereasy.model.enums.ConsolidationGroupage;
import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.model.tt.EbTtCategorieDeviation;
import com.adias.mytowereasy.model.tt.EbTtEvent;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.mtc.service.MtcService;
import com.adias.mytowereasy.repository.*;
import com.adias.mytowereasy.trpl.model.EbPlCostItem;
import com.adias.mytowereasy.trpl.model.EbPlTransport;
import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.util.Constants;
import com.adias.mytowereasy.util.DemandeStatusUtils;
import com.adias.mytowereasy.util.JsonUtils;
import com.adias.mytowereasy.util.UnitRefSharingParams;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;
import com.adias.mytowereasy.utils.search.SearchCriteriaTypeUnit;

@Service
public class PricingServiceImpl extends MyTowerService implements PricingService, CommonFlagSevice {
    Logger logger = LoggerFactory.getLogger(PricingServiceImpl.class);
    private static final int DEFAULT_PAGE = 0;

    private static final int DEFAULT_SIZE = 10;

    private static final Logger LOGGER = LoggerFactory.getLogger(PricingService.class);

    private Map<String, Map<String, Object>> progressBulkImport = null;
    @Autowired
    EbCompagnieRepository compagnieRepository;

    @PersistenceContext
    EntityManager em;

    @Autowired
    FreightAuditService freightAuditService;

    @Autowired
    UserService userService;

    @Autowired
    EbTypeTransportRepository ebTypeTransportRepository;

    @Autowired
    EbChatRepository ebChatRepositorty;

    @Autowired
    ListStatiqueService listStatiqueService;

    @Autowired
    TypeDocumentService typeDocumentService;

    @Autowired
    private JpaContext jpaContext;

    @Autowired
    FlagService flagService;

    @Autowired
    EbTrackTraceRepository ebTrackTraceRepository;

    @Autowired
    EbTypeUnitRepository ebTypeUnitRepository;

    @Autowired
    MessageSource messageSource;

    @Autowired
    DaoAdresse daoAdresse;
    @Autowired
    MtcService mtcService;
    @Autowired
    PrMtcTransporteurRepository prMtcTransporteurRepository;

    @Autowired
    OrderService orderService;

    @Autowired
    EbLivraisonRepository ebLivraisonRepository;

    @Autowired
    EbLivraisonLineRepository ebLivraisonLineRepository;

    @Autowired
    DeliveryService deliveryService;
    @Autowired
    DaoPrMtcTransporteur daoPrMtcTransporteur;

    @Autowired
    PlanTransportApiService planTransportApiService;

    @Autowired
    private ControlRuleService controlRuleService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    EcCurrencyRepository ecCurrencyRepository;

    @Autowired
    private TransportMapper mapper;

    @Autowired
    DaoTypeUnit daoTypeUnit;

    @Autowired
    KafkaObjectService kafkaObjectService;

    @Autowired
    PricingAnnexMethodsService pricingAnnexMethodsService;

    @Autowired
    PricingCreateAndUpdateService pricingCreateAndUpdateService;

    @Autowired
    DemandeObserversRepository demandeObserversRepository;

		@Transactional
		@Override
		public EbDemande saveAllInformations(EbDemande ebDemande, Integer ebModuleNum)
		{
			try
			{
				EbUser connectedUser = connectedUserService.getCurrentUser();
				SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();
				criteria.setEbDemandeNum(ebDemande.getEbDemandeNum());
				EbDemande oldEbDemande = daoPricing.getEbDemande(criteria, null);
				oldEbDemande.setOldMarchandises(ebDemande.getOldMarchandises());
				StringBuilder sb = new StringBuilder();

				// MISE A JOUR DE LA DEMANDE
        ebDemande = pricingCreateAndUpdateService.updateEbDemandeObjects(ebDemande, ebModuleNum, true);

				// HISTORISATION DES CHANGEMENTS
				historizeModifToEbchat(
					ebDemande,
					ebDemande.getTypeData(),
					ebModuleNum,
					oldEbDemande,
					connectedUser,
					sb
				);
			}
			catch (Exception e)
			{
          throw new MyTowerException(e.getMessage());
			}

			return ebDemande;
		}

		public void saveInformationsBasedOnTrNature(EbDemande ebDemande, Integer ebModuleNum)
		{
			if (isGroupedTr(ebDemande))
			{
				updatelistAdditionalCostInEbDemandeByEbDemandeNum(
					ebDemande.getEbDemandeNum(),
					ebDemande.getListAdditionalCost()
				);
			}
			else
				saveAllInformations(ebDemande, ebModuleNum);
		}

		private boolean isGroupedTr(EbDemande ebDemande)
		{
			return ebDemande.getxEcNature().equals(NatureDemandeTransport.CONSOLIDATION.getCode()) &&
				ebDemande.getxEcStatut().equals(Enumeration.StatutDemande.PND.getCode());
		}

		public void addListDemandeInitials(EbDemande ebDemande)
		{
        List<Integer> listDemandeNumInitials = ebDemande
                .getConsolidationListTransportReferenceInfos().stream().map(TransportReferenceInfosDTO::getEbDemandeNum)
                .collect(Collectors.toList());
        List<EbDemande> listDemandeInitials = new ArrayList<EbDemande>();
        listDemandeNumInitials.forEach(demandeNum -> {
            SearchCriteriaPricingBooking searchCriteriaPricingBooking = new SearchCriteriaPricingBooking();
            searchCriteriaPricingBooking.setEbDemandeNum(demandeNum);
            searchCriteriaPricingBooking.setWithListMarchandise(true);
            EbDemande demandeInitials = null;

            try {
                demandeInitials = getEbDemande(searchCriteriaPricingBooking, ebDemande.getUser());
            } catch (Exception e) {
                LOGGER.error("Error while adding listDemandeInitials ", e);
            }

            if (demandeInitials != null) {
                listDemandeInitials.add(demandeInitials);
            }

        });

        ebDemande.setInitialTrInGroupedTransportRequest(listDemandeInitials);
    }

    private Map<String, Object> historizeModifToEbchat(
            EbDemande ebDemande,
            Integer typeData,
            Integer module,
            EbDemande oldEbDemande,
            EbUser connectedUser,
            StringBuilder sb)
            throws JsonProcessingException {
        Map<String, Object> result = new HashMap<String, Object>();
        Map<String, Object> mapHistory = new HashMap<String, Object>();
        TransportMapper transportMapper = null;
        String libelleTypeData = null;
        ObjectMapper mapper = new ObjectMapper();
        String action = "";
        List<EbMarchandise> oldMarchandise = oldEbDemande.getOldMarchandises();

        if ((TypeDataEbDemande.EXPEDITION_INFORMATION.getCode().equals(typeData) ||
                TypeDataEbDemande.ALL.getCode().equals(typeData)) &&
                !(oldEbDemande.getxEcStatut() != null &&
                        oldEbDemande.getxEcStatut() >= Enumeration.StatutDemande.WPU.getCode())) {
            libelleTypeData = TypeDataEbDemande.EXPEDITION_INFORMATION.getLibelle();

            if (ebDemande.getUser() != null &&
                    (oldEbDemande.getUser() == null ||
                            !ebDemande.getUser().getEbUserNum().equals(oldEbDemande.getUser().getEbUserNum()))) {
                mapHistory.put("User", oldEbDemande.getUser());
                action += "User : " + ebDemande.getUser() + " <strike> " + oldEbDemande.getUser() + " </strike> <br> ";
            }

            if (ebDemande.getEbPartyOrigin() != null &&
                    ebDemande.getEbPartyOrigin().getEbPartyNum() != null &&
                    (!ebDemande.getEbPartyOrigin().getCity().equals(oldEbDemande.getEbPartyOrigin().getCity()) ||
                            !ebDemande
                                    .getEbPartyOrigin().getxEcCountry().getLibelle()
                                    .equals(oldEbDemande.getEbPartyOrigin().getxEcCountry().getLibelle()))) {
                mapHistory.put("ebPartyOrigin", ebDemande.getEbPartyOrigin());
                action += "Origin : " +
                        ebDemande.getEbPartyOrigin().getCity() + "," +
                        ebDemande.getEbPartyOrigin().getxEcCountry().getLibelle() + " <strike> " +
                        oldEbDemande.getEbPartyOrigin().getCity() + "," +
                        oldEbDemande.getEbPartyOrigin().getxEcCountry().getLibelle() + " </strike> <br> ";
            }

            if (ebDemande.getEbPartyDest() != null &&
                    ebDemande.getEbPartyDest().getEbPartyNum() != null &&
                    (!ebDemande.getEbPartyDest().getCity().equals(oldEbDemande.getEbPartyDest().getCity()) ||
                            !ebDemande
                                    .getEbPartyDest().getxEcCountry().getLibelle()
                                    .equals(oldEbDemande.getEbPartyDest().getxEcCountry().getLibelle()))) {
                mapHistory.put("ebPartyDest", ebDemande.getEbPartyDest());
                action += "Destination : " +
                        ebDemande.getEbPartyDest().getCity() + "," +
                        ebDemande.getEbPartyDest().getxEcCountry().getLibelle() + " <strike> " +
                        oldEbDemande.getEbPartyDest().getCity() + "," +
                        oldEbDemande.getEbPartyDest().getxEcCountry().getLibelle() + " </strike> <br> ";
            }

            if (ebDemande.getEbPartyNotif() != null) {
                mapHistory.put("ebPartyNotif", oldEbDemande.getEbPartyNotif());
                action += "Party Notif : " +
                        ebDemande.getEbPartyNotif() + " <strike> " + oldEbDemande.getEbPartyNotif() + " </strike> <br> ";
            }

            if (ebDemande.getEligibleForConsolidation() != null &&
                    !ebDemande.getEligibleForConsolidation().equals(oldEbDemande.getEligibleForConsolidation())) {
                mapHistory.put("EligibleForConsolidation", oldEbDemande.getEligibleForConsolidation());
                action += "Eligible for consolidation : " +
                        ebDemande.getEligibleForConsolidation() + " <strike> " +
                        oldEbDemande.getEligibleForConsolidation() + " </strike> <br> ";
            }

            if (ebDemande.getDateOfGoodsAvailability() != null &&
                    !ebDemande.getDateOfGoodsAvailability().equals(oldEbDemande.getDateOfGoodsAvailability())) {
                mapHistory.put("DateOfGoodsAvailability", oldEbDemande.getDateOfGoodsAvailability());
                action += "Date of goods availability : " +
                        ebDemande.getDateOfGoodsAvailability() + " <strike> " + oldEbDemande.getDateOfGoodsAvailability() +
                        " </strike> <br> ";
            }

            if (ebDemande.getDateOfArrival() != null &&
                    !ebDemande.getDateOfArrival().equals(oldEbDemande.getDateOfArrival())) {
                mapHistory.put("DateOfArrival", oldEbDemande.getDateOfArrival());
                action += "Date of arrival : " +
                        ebDemande.getDateOfArrival() + " <strike> " + oldEbDemande.getDateOfArrival() + " </strike> <br> ";
            }

            if (ebDemande.getxEcModeTransport() != null &&
                    !ebDemande.getxEcModeTransport().equals(oldEbDemande.getxEcModeTransport())) {
                mapHistory.put("xEcModeTransport", oldEbDemande.getxEcModeTransport());
                action += "Mode transport : " +
                        ebDemande.getModeTransporteur() + " <strike> " + oldEbDemande.getModeTransporteur() +
                        " </strike> <br> ";
            }

            if (ebDemande.getxEbSchemaPsl() != null &&
                    (oldEbDemande.getxEbSchemaPsl() == null ||
                            !ebDemande
                                    .getxEbSchemaPsl().getEbTtSchemaPslNum()
                                    .equals(oldEbDemande.getxEbSchemaPsl().getEbTtSchemaPslNum()))) {
                mapHistory.put("xEbSchemaPsl", oldEbDemande.getxEbSchemaPsl());
                action += "Schema Psl : " +
                        ebDemande.getxEbSchemaPsl().getDesignation() + " <strike> " +
                        oldEbDemande.getxEbSchemaPsl().getDesignation() + " </strike> <br> ";
            }

            if (ebDemande.getxEbIncotermNum() != null &&
                    !ebDemande.getxEbIncotermNum().equals(oldEbDemande.getxEbIncotermNum())) {
                mapHistory.put("xEbIncotermNum", oldEbDemande.getxEbIncotermNum());
                action += "Incoterm : " +
                        ebDemande.getxEcIncotermLibelle() + " <strike> " + oldEbDemande.getxEcIncotermLibelle() +
                        " </strike> <br> ";
            }

            if (ebDemande.getCity() != null && !ebDemande.getCity().equals(oldEbDemande.getCity())) {
                mapHistory.put("city", oldEbDemande.getCity());
                action += "City : " + ebDemande.getCity() + " <strike> " + oldEbDemande.getCity() + " </strike> <br> ";
            }

            if (ebDemande.getxEbTypeTransport() != null &&
                    !ebDemande.getxEbTypeTransport().equals(oldEbDemande.getxEbTypeTransport())) {
                mapHistory.put("xEbTypeTransport", oldEbDemande.getxEbTypeTransport());
                action += "Type transport : " +
                        ebDemande.getxEbTypeTransport() + " <strike> " + oldEbDemande.getxEbTypeTransport() +
                        " </strike> <br> ";
            }

            if (ebDemande.getxEbTypeFluxNum() != null &&
                    !ebDemande.getxEbTypeFluxNum().equals(oldEbDemande.getxEbTypeFluxNum())) {
                mapHistory.put("xEbTypeFluxNum", oldEbDemande.getxEbTypeFluxNum());
                action += "Type flux : " +
                        ebDemande.getxEbTypeFluxNum() + " <strike> " + oldEbDemande.getxEbTypeFluxNum() +
                        " </strike> <br> ";
            }

        }

        if (TypeDataEbDemande.REFERENCE_INFORMATION.getCode().equals(typeData) ||
                TypeDataEbDemande.ALL.getCode().equals(typeData)) {
            libelleTypeData = TypeDataEbDemande.REFERENCE_INFORMATION.getLibelle();

            if (ebDemande.getCustomerReference() != null &&
                    !ebDemande.getCustomerReference().equals(oldEbDemande.getCustomerReference())) {
                mapHistory.put("customerReference", oldEbDemande.getCustomerReference());
                action += "Customer reference : " +
                        ebDemande.getCustomerReference() + " <strike> " + oldEbDemande.getCustomerReference() +
                        " </strike> <br> ";
            }

            if (ebDemande.getxEbCostCenter() != null &&
                    (oldEbDemande.getxEbCostCenter() == null ||
                            !ebDemande
                                    .getxEbCostCenter().getEbCostCenterNum()
                                    .equals(oldEbDemande.getxEbCostCenter().getEbCostCenterNum()))

            ) {
                mapHistory.put("xEbCostCenter", oldEbDemande.getxEbCostCenter());
                action += "Cost center :" +
                        (ebDemande.getxEbCostCenter() != null ? ebDemande.getxEbCostCenter().getLibelle() : "-") +
                        " <strike> " +
                        (oldEbDemande.getxEbCostCenter() != null ? oldEbDemande.getxEbCostCenter().getLibelle() : "-") +
                        " </strike> <br> ";
            }

            {
                boolean exists = false;
                boolean modified = false;

                EbDemande d = ebDemandeRepository.selectListCategoriesByEbDemandeNum(oldEbDemande.getEbDemandeNum());
                List<EbCategorie> listOldCategories = d.getListCategories();

                if (ebDemande.getListCategories() != null && !ebDemande.getListCategories().isEmpty()) {

                    for (EbCategorie cat : ebDemande.getListCategories()) {
                        exists = false;

                        if (listOldCategories != null && !listOldCategories.isEmpty()) {

                            for (EbCategorie oldCat : listOldCategories) {

                                if (cat.getEbCategorieNum().equals(oldCat.getEbCategorieNum())) {
                                    exists = true;

                                    List<EbLabel> listLabel = new ArrayList<EbLabel>();
                                    if (cat.getLabels() != null) listLabel.addAll(cat.getLabels());
                                    List<EbLabel> listOldLabel = new ArrayList<EbLabel>();
                                    if (oldCat.getLabels() != null) listOldLabel.addAll(oldCat.getLabels());

                                    if (!listOldLabel.isEmpty() &&
                                            (listLabel.isEmpty() ||
                                                    !listLabel
                                                            .get(0).getEbLabelNum().equals(listOldLabel.get(0).getEbLabelNum()))) {
                                        action += oldCat.getLibelle() +
                                                " : " + (listLabel.isEmpty() ? "-" : listLabel.get(0).getLibelle()) +
                                                " <strike> " + listOldLabel.get(0).getLibelle() + "</strike> <br>";
                                        modified = true;
                                    }

                                    if (!listLabel.isEmpty() && listOldLabel.isEmpty()) {
                                        action += oldCat.getLibelle() +
                                                " : " + listLabel.get(0).getLibelle() + "-  " + "</strike> <br>";
                                        modified = true;
                                    }

                                }

                            }

                        }

                        if (!exists) {
                            List<EbLabel> listLabel = new ArrayList<EbLabel>();
                            if (cat.getLabels() != null) listLabel.addAll(cat.getLabels());

                            if (!listLabel.isEmpty()) {
                                modified = true;
                                action += cat.getLibelle() +
                                        " :  " + listLabel.get(0).getLibelle() + " - " + "</strike> <br>";
                            }

                        }

                    }

                }

                if (modified ||
                        ebDemande.getListCategories() != null &&
                                listOldCategories.size() != ebDemande.getListCategories().size()) {
                    mapHistory.put("listCategories", oldEbDemande.getListCategories());

                    if (!modified) {
                        action += "Categories/Labels modified <br>";
                    }

                }

            }

            if (ebDemande.getxEbTypeRequest() != null &&
                    !ebDemande.getxEbTypeRequest().equals(oldEbDemande.getxEbTypeRequest())) {
                mapHistory.put("TypeRequest", oldEbDemande.getxEbTypeRequest());
                mapHistory.put("LibeleTypeRequest", oldEbDemande.getLibelleTypeOfRequest());

                action += "Service Level : " +
                        ebDemande.getTypeRequestLibelle() + " <strike> " + oldEbDemande.getTypeRequestLibelle() +
                        " </strike> <br> ";
            }

            if (ebDemande.getCustomFields() != null &&
                    !ebDemande.getCustomFields().equals(oldEbDemande.getCustomFields())) {
                List<CustomFields> listCustomsFields = gson
                        .fromJson(ebDemande.getCustomFields(), new TypeToken<ArrayList<CustomFields>>() {
                        }.getType());
                List<CustomFields> oldListCustomsFields = gson
                        .fromJson(oldEbDemande.getCustomFields(), new TypeToken<ArrayList<CustomFields>>() {
                        }.getType());
                if (oldListCustomsFields == null) oldListCustomsFields = new ArrayList<CustomFields>();
                Boolean modified = false;
                Boolean exists = false;

                if (listCustomsFields.size() == oldListCustomsFields.size()) {

                    for (CustomFields field : listCustomsFields) {

                        for (CustomFields oldField : oldListCustomsFields) {

                            if (field.getName().equals(oldField.getName())) {
                                if (!modified) exists = true;

                                if (field.getValue() != null && !field.getValue().equals(oldField.getValue())) {
                                    modified = true;
                                    action += field.getLabel() +
                                            " : " + field.getValue() + " <strike> " +
                                            (oldField.getValue() != null ? oldField.getValue() : "-") + " </strike> <br> ";
                                    // break;
                                }

                            }

                        }

                        // if(!exists) break;
                        // if(modified) break;
                    }

                }

                if (modified || !exists) {
                    mapHistory.put("customFields", oldEbDemande.getCustomFields());
                }

            }

            if (ebDemande.getCommentChargeur() != null &&
                    !ebDemande.getCommentChargeur().equals(oldEbDemande.getCommentChargeur())) {
                mapHistory.put("commentChargeur", oldEbDemande.getCommentChargeur());
                action += "Comment : " +
                        ebDemande.getCommentChargeur() + " <strike> " + oldEbDemande.getCommentChargeur() +
                        " </strike> <br> ";
            }

            if (ebDemande.getInsurance() != null && !ebDemande.getInsurance().equals(oldEbDemande.getInsurance())) {
                mapHistory.put("insurance", oldEbDemande.getInsurance());
                action += "Insurance : " +
                        ebDemande.getInsurance() + " <strike> " + oldEbDemande.getInsurance() + " </strike> <br> ";
            }

            if (ebDemande.getInsuranceValue() != null &&
                    !ebDemande.getInsuranceValue().equals(oldEbDemande.getInsuranceValue())) {
                mapHistory.put("insuranceValue", oldEbDemande.getInsuranceValue());
                action += "Insurance : " +
                        ebDemande.getInsuranceValue() + " <strike> " + oldEbDemande.getInsuranceValue() +
                        " </strike> <br> ";
            }

            if (ebDemande.getxEcTypeCustomBroker() != null &&
                    !ebDemande.getxEcTypeCustomBroker().equals(oldEbDemande.getxEcTypeCustomBroker())) {
                mapHistory.put("xEcTypeCustomBroker", oldEbDemande.getxEcTypeCustomBroker());
                action += "Type Customs Broker : " +
                        (ebDemande.getxEcTypeCustomBroker() != null ?
                                Enumeration.OriginCustomsBroker.getLibelleByCode(ebDemande.getxEcTypeCustomBroker()) :
                                " - ") +
                        " <strike> " +
                        (oldEbDemande.getxEcTypeCustomBroker() != null ?
                                Enumeration.OriginCustomsBroker.getLibelleByCode(oldEbDemande.getxEcTypeCustomBroker()) :
                                " - ") +
                        " </strike> <br> ";
            }

            if (ebDemande.getXecCurrencyInvoice() != null &&
                    ebDemande.getXecCurrencyInvoice().getEcCurrencyNum() != null &&
                    (oldEbDemande.getXecCurrencyInvoice() == null ||
                            !ebDemande
                                    .getXecCurrencyInvoice().getEcCurrencyNum()
                                    .equals(oldEbDemande.getXecCurrencyInvoice().getEcCurrencyNum()))) {
                mapHistory.put("xecCurrencyInvoice", oldEbDemande.getXecCurrencyInvoice());
                action += "Currency : " +
                        (ebDemande.getXecCurrencyInvoice() != null ? ebDemande.getXecCurrencyInvoice().getCode() : " - ") +
                        " <strike> " +
                        (oldEbDemande.getXecCurrencyInvoice() != null ?
                                oldEbDemande.getXecCurrencyInvoice().getCode() :
                                " - ") +
                        " </strike> <br> ";
            }

            if (ebDemande.getxEbUserOrigin() != null &&
                    (oldEbDemande.getxEbUserOrigin() == null ||
                            !ebDemande
                                    .getxEbUserOrigin().getEbUserNum().equals(oldEbDemande.getxEbUserOrigin().getEbUserNum())) ||
                    ebDemande.getxEbUserOrigin() == null &&
                            oldEbDemande.getxEbUserOrigin() != null && oldEbDemande.getxEbUserOrigin().getEbUserNum() != null) {
                mapHistory.put("xEbUserOrigin", oldEbDemande.getxEbUserOrigin());
                action += "User origin : " +
                        (ebDemande.getxEbUserOrigin() != null ? ebDemande.getxEbUserOrigin().getNom() : "-") +
                        " <strike> " +
                        (oldEbDemande.getxEbUserOrigin() != null ? oldEbDemande.getxEbUserOrigin().getNom() : "-") +
                        " </strike>  <br> ";

                if (oldEbDemande.getxEbUserOrigin() != null && oldEbDemande.getxEbUserOrigin().getEbUserNum() != null) {
                    // add desaffectation alerte to old user
                    this.notificationService
                            .generateNotificationForUser(
                                    ebDemande,
                                    oldEbDemande.getxEbUserOrigin().getEbUserNum(),
                                    module,
                                    false);
                }

                if (ebDemande.getxEbUserOrigin() != null && ebDemande.getxEbUserOrigin().getEbUserNum() != null) {
                    // add affectation alerte to old user
                    this.notificationService
                            .generateNotificationForUser(
                                    ebDemande,
                                    ebDemande.getxEbUserOrigin().getEbUserNum(),
                                    module,
                                    true);
                }

            }

            if ((ebDemande.getTdcAcknowledge() == null || !ebDemande.getTdcAcknowledge()) &&
                    ebDemande.getTdcAcknowledgeDate() == null && oldEbDemande.getTdcAcknowledgeDate() != null) {
                mapHistory.put("tdcAcknowledge", oldEbDemande.getTdcAcknowledge());
                mapHistory.put("tdcAcknowledgeDate", oldEbDemande.getTdcAcknowledgeDate());
            }

            if (ebDemande.getxEbUserDest() != null &&
                    (oldEbDemande.getxEbUserDest() == null ||
                            !ebDemande.getxEbUserDest().getEbUserNum().equals(oldEbDemande.getxEbUserDest().getEbUserNum())) ||
                    ebDemande.getxEbUserDest() == null &&
                            oldEbDemande.getxEbUserDest() != null && oldEbDemande.getxEbUserDest().getEbUserNum() != null) {
                mapHistory.put("xEbUserDest", oldEbDemande.getxEbUserDest());
                action += "User destination : " +
                        (ebDemande.getxEbUserDest() != null ? ebDemande.getxEbUserDest().getNom() : "-") + " <strike> " +
                        (oldEbDemande.getxEbUserDest() != null ? oldEbDemande.getxEbUserDest().getNom() : "-") +
                        " </strike>  <br> ";

                if (oldEbDemande.getxEbUserDest() != null && oldEbDemande.getxEbUserDest().getEbUserNum() != null) { // add
                    // desaffectation
                    // alerte
                    // to
                    // old
                    // user
                    this.notificationService
                            .generateNotificationForUser(
                                    ebDemande,
                                    oldEbDemande.getxEbUserDest().getEbUserNum(),
                                    module,
                                    false);
                }

                if (ebDemande.getxEbUserDest() != null && ebDemande.getxEbUserDest().getEbUserNum() != null) {
                    // add affectation alert to new user
                    this.notificationService
                            .generateNotificationForUser(
                                    ebDemande,
                                    ebDemande.getxEbUserDest().getEbUserNum(),
                                    module,
                                    true);
                }

            }

            if (ebDemande.getxEbUserOriginCustomsBroker() != null &&
                    (oldEbDemande.getxEbUserOriginCustomsBroker() == null ||
                            !ebDemande
                                    .getxEbUserOriginCustomsBroker().getEbUserNum()
                                    .equals(oldEbDemande.getxEbUserOriginCustomsBroker().getEbUserNum())) ||
                    ebDemande.getxEbUserOriginCustomsBroker() == null &&
                            oldEbDemande.getxEbUserOriginCustomsBroker() != null &&
                            oldEbDemande.getxEbUserOriginCustomsBroker().getEbUserNum() != null) {
                mapHistory.put("xEbUserOriginCustomsBroker", oldEbDemande.getxEbUserOriginCustomsBroker());
                action += "User origin customs broker : " +
                        (ebDemande.getxEbUserOriginCustomsBroker() != null ?
                                ebDemande.getxEbUserOriginCustomsBroker().getNom() :
                                "-") +
                        " <strike> " +
                        (oldEbDemande.getxEbUserOriginCustomsBroker() != null ?
                                oldEbDemande.getxEbUserOriginCustomsBroker().getNom() :
                                "-") +
                        " </strike>  <br> ";

                if (oldEbDemande.getxEbUserOriginCustomsBroker() != null &&
                        oldEbDemande.getxEbUserOriginCustomsBroker().getEbUserNum() != null) { // add desaffectation
                    // alerte to old user
                    this.notificationService
                            .generateNotificationForUser(
                                    ebDemande,
                                    oldEbDemande.getxEbUserOriginCustomsBroker().getEbUserNum(),
                                    module,
                                    false);
                }

            }

            if (ebDemande.getxEbUserObserver() != null &&
                    !ebDemande.getxEbUserObserver().equals(oldEbDemande.getxEbUserObserver())

            ) {
                mapHistory.put("xEbUserObserver", oldEbDemande.getxEbUserObserver());

                if ((ebDemande.getxEbUserObserver() == null || ebDemande.getxEbUserObserver().isEmpty()) && // desaffectation
                        // case
                        (oldEbDemande.getxEbUserObserver() != null && !oldEbDemande.getxEbUserObserver().isEmpty())) {
                    oldEbDemande.getxEbUserObserver().forEach(user -> {
                        this.notificationService
                                .generateNotificationForUser(ebDemande, user.getEbUserNum(), module, false);
                    });
                } else if (oldEbDemande.getxEbUserObserver() == null || oldEbDemande.getxEbUserObserver().isEmpty() // affectation
                    // case
                ) {
                    ebDemande.getxEbUserObserver().forEach(user -> {
                        this.notificationService
                                .generateNotificationForUser(ebDemande, user.getEbUserNum(), module, true);
                    });
                } else // mixed case
                {
                    List<Integer> newUsers = ebDemande
                            .getxEbUserObserver().stream().map(user -> user.getEbUserNum()).collect(Collectors.toList());

                    List<Integer> oldUsers = oldEbDemande
                            .getxEbUserObserver().stream().map(user -> user.getEbUserNum()).collect(Collectors.toList());

                    List<Integer> newAffectedUsers = newUsers
                            .stream().filter(userNum -> !oldUsers.contains(userNum)).collect(Collectors.toList());

                    List<Integer> desaffectedUsers = oldUsers
                            .stream().filter(userNum -> !newUsers.contains(userNum)).collect(Collectors.toList());

                    newAffectedUsers.forEach(userNum -> {
                        this.notificationService.generateNotificationForUser(ebDemande, userNum, module, true);
                    });

                    desaffectedUsers.forEach(userNum -> {
                        this.notificationService.generateNotificationForUser(ebDemande, userNum, module, false);
                    });
                }

            }

            if (ebDemande.getxEbUserDestCustomsBroker() != null &&
                    (oldEbDemande.getxEbUserDestCustomsBroker() == null ||
                            !ebDemande
                                    .getxEbUserDestCustomsBroker().getEbUserNum()
                                    .equals(oldEbDemande.getxEbUserDestCustomsBroker().getEbUserNum())) ||
                    ebDemande.getxEbUserDestCustomsBroker() == null &&
                            oldEbDemande.getxEbUserDestCustomsBroker() != null &&
                            oldEbDemande.getxEbUserDestCustomsBroker().getEbUserNum() != null) {
                mapHistory.put("xEbUserDestCustomsBroker", oldEbDemande.getxEbUserDestCustomsBroker());
                action += "User destination customs broker : " +
                        (ebDemande.getxEbUserDestCustomsBroker() != null ?
                                ebDemande.getxEbUserDestCustomsBroker().getNom() :
                                "-") +
                        " <strike> " +
                        (oldEbDemande.getxEbUserDestCustomsBroker() != null ?
                                oldEbDemande.getxEbUserDestCustomsBroker().getNom() :
                                "-") +
                        " </strike>  <br> ";

                if (oldEbDemande.getxEbUserDestCustomsBroker() != null &&
                        oldEbDemande.getxEbUserDestCustomsBroker().getEbUserNum() != null) { // add desaffectation alerte
                    // to old user
                    this.notificationService
                            .generateNotificationForUser(
                                    ebDemande,
                                    oldEbDemande.getxEbUserDestCustomsBroker().getEbUserNum(),
                                    module,
                                    false);
                }

            }

            if (mapHistory.size() > 0) {
                mapHistory.put("ebDemandeNum", ebDemande.getEbDemandeNum());
                mapHistory.put("dateHistory", new Date());
                mapHistory
                        .put(
                                "connectedUser",
                                new EbUser(
                                        connectedUser.getEbUserNum(),
                                        connectedUser.getNom(),
                                        connectedUser.getPrenom(),
                                        connectedUser.getEmail(),
                                        connectedUser.getService(),
                                        connectedUser.getRole(),
                                        connectedUser.getTelephone()));

                List<Map<String, Object>> demandeHistory = new ArrayList<Map<String, Object>>();

                if (oldEbDemande.getEbDemandeHistory() != null) {
                    demandeHistory = gson
                            .fromJson(oldEbDemande.getEbDemandeHistory(), new TypeToken<ArrayList<Map<String, Object>>>() {
                            }.getType());
                }

                demandeHistory.add(mapHistory);
                String newHistory = mapper.writeValueAsString(demandeHistory);
                oldEbDemande.setEbDemandeHistory(newHistory);
            }

        }

        if ((TypeDataEbDemande.GOODS_INFORMATION.getCode().equals(typeData) ||
                TypeDataEbDemande.ALL.getCode().equals(typeData)) &&
                !(oldEbDemande.getxEcStatut() != null &&
                        oldEbDemande.getxEcStatut() >= Enumeration.StatutDemande.WPU.getCode())) {

            if (ebDemande.getListMarchandises() != null && ebDemande.getListMarchandises().size() > 0) {
                List<EbMarchandise> listMarchandise = ebDemande.getListMarchandises();
                libelleTypeData = TypeDataEbDemande.GOODS_INFORMATION.getLibelle();

                if (oldMarchandise != null &&
                        !oldMarchandise.isEmpty()) for (EbMarchandise oldEbMarchandise : oldMarchandise) {

                    for (EbMarchandise newEbMarchandise : listMarchandise) {
                        boolean areEqualUnits = false;
                        if (oldEbMarchandise.getCustomerReference() != null &&
                                newEbMarchandise.getCustomerReference() != null) areEqualUnits = oldEbMarchandise
                                .getCustomerReference().equals(newEbMarchandise.getCustomerReference());
                        else if (oldEbMarchandise.getEbMarchandiseNum() != null &&
                                newEbMarchandise.getEbMarchandiseNum() != null) areEqualUnits = oldEbMarchandise
                                .getEbMarchandiseNum().equals(newEbMarchandise.getEbMarchandiseNum());

                        if (areEqualUnits) {

                            if ((newEbMarchandise.getxEbTypeUnit() != null &&
                                    oldEbMarchandise.getxEbTypeUnit() == null) ||
                                    (newEbMarchandise.getxEbTypeUnit() != null &&
                                            !oldEbMarchandise.getxEbTypeUnit().equals(newEbMarchandise.getxEbTypeUnit()))) {
                                // chercher les type of unit
                                SearchCriteriaTypeUnit criteriaTypeUnit = new SearchCriteriaTypeUnit();
                                List<EbTypeMarchandiseDto> resultList = transportMapper
                                        .ebTypeUnitsToEbTypesMarchandise(daoTypeUnit.getListTypeUnit(criteriaTypeUnit));

                                if (action
                                        .indexOf(

                                                "Part Number"

                                        ) < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                String newUnitType = "";

                                if (newEbMarchandise.getxEbTypeUnit() != null) {
                                    EbTypeMarchandiseDto newUnitTypeMarchandise = resultList
                                            .stream()
                                            .filter(
                                                    typeUnit -> typeUnit
                                                            .getxEbTypeUnit().equals(newEbMarchandise.getxEbTypeUnit()))
                                            .findFirst().orElse(null);

                                    if (newUnitTypeMarchandise != null) {
                                        newUnitType = newUnitTypeMarchandise.getLabel();
                                    }

                                }

                                String oldUnitType = "";

                                if (oldEbMarchandise.getxEbTypeUnit() != null) {
                                    EbTypeMarchandiseDto oldUnitTypeMarchandise = resultList
                                            .stream()
                                            .filter(
                                                    typeUnit -> typeUnit.getxEbTypeUnit()
                                                            == oldEbMarchandise.getxEbTypeUnit())
                                            .findFirst().orElse(null);

                                    if (oldUnitTypeMarchandise != null) {
                                        oldUnitType = oldUnitTypeMarchandise.getLabel();
                                    }

                                }

                                action += "<table class='width-100-percent margin-top-5'><tr>" +
                                        "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                        "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                        oldEbMarchandise.getPartNumber() + "</div>" +
                                        "<div class='padding-4'>Unit Reference : " +
                                        oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                            }

                            if ((newEbMarchandise.getUnitReference() != null &&
                                    oldEbMarchandise.getUnitReference() == null) ||
                                    (newEbMarchandise.getUnitReference() != null &&
                                            !oldEbMarchandise
                                                    .getUnitReference().equals(newEbMarchandise.getUnitReference()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Unit Reference :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newEbMarchandise.getUnitReference() + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldEbMarchandise.getUnitReference() + "</strike>" + "</td>" +
                                        "</tr>";
                            }

                            if ((newEbMarchandise.getPartNumber() != null &&
                                    oldEbMarchandise.getPartNumber() == null) ||
                                    (newEbMarchandise.getPartNumber() != null &&
                                            !oldEbMarchandise.getPartNumber().equals(newEbMarchandise.getPartNumber()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Part Number :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newEbMarchandise.getPartNumber() + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldEbMarchandise.getPartNumber() + "</strike>" + "</td>" +
                                        "</tr>";
                            }

                            if ((newEbMarchandise.getSerialNumber() != null &&
                                    oldEbMarchandise.getSerialNumber() == null) ||
                                    (newEbMarchandise.getSerialNumber() != null &&
                                            !oldEbMarchandise
                                                    .getSerialNumber().equals(newEbMarchandise.getSerialNumber()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Serial Number :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newEbMarchandise.getSerialNumber() + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldEbMarchandise.getSerialNumber() + "</strike>" + "</td>" +
                                        "</tr>";
                            }

                            if ((newEbMarchandise.getPackingList() != null &&
                                    oldEbMarchandise.getPackingList() == null) ||
                                    (newEbMarchandise.getPackingList() != null &&
                                            !oldEbMarchandise.getPackingList().equals(newEbMarchandise.getPackingList()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Packing List :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newEbMarchandise.getPackingList() + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldEbMarchandise.getPackingList() + "</strike>" + "</td>" +
                                        "</tr>";
                            }

                            if ((newEbMarchandise.getWeight() != null && oldEbMarchandise.getWeight() == null) ||
                                    (newEbMarchandise.getWeight() != null &&
                                            !oldEbMarchandise.getWeight().equals(newEbMarchandise.getWeight()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                Double newWeight = newEbMarchandise.getWeight() != null ?
                                        newEbMarchandise.getWeight() :
                                        0;
                                Double oldWeight = oldEbMarchandise.getWeight() != null ?
                                        oldEbMarchandise.getWeight() :
                                        0;
                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Weight :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        +newWeight + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldWeight + "</strike>" + "</td>" + "</tr>";
                            }

                            if ((newEbMarchandise.getWidth() != null && oldEbMarchandise.getWidth() == null) ||
                                    (newEbMarchandise.getWidth() != null &&
                                            !oldEbMarchandise.getWidth().equals(newEbMarchandise.getWidth()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                Double newWidth = newEbMarchandise.getWidth() != null ?
                                        newEbMarchandise.getWidth() :
                                        0;
                                Double oldWidth = oldEbMarchandise.getWidth() != null ?
                                        oldEbMarchandise.getWidth() :
                                        0;
                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Width :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newWidth + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldWidth + "</strike>" + "</td>" + "</tr>";
                            }

                            if ((newEbMarchandise.getHeigth() != null && oldEbMarchandise.getHeigth() == null) ||
                                    (newEbMarchandise.getHeigth() != null &&
                                            !oldEbMarchandise.getHeigth().equals(newEbMarchandise.getHeigth()))) {

                                if (action
                                        .indexOf(
                                                "<table class='width-100-percent margin-top-5'><tr>" +
                                                        "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                                        "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                                        oldEbMarchandise.getPartNumber() + "</div>" +
                                                        "<div class='padding-4'>Unit Reference : " +
                                                        oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>")
                                        < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                Double newHeigth = newEbMarchandise.getHeigth() != null ?
                                        newEbMarchandise.getHeigth() :
                                        0;
                                Double oldHeigth = oldEbMarchandise.getHeigth() != null ?
                                        oldEbMarchandise.getHeigth() :
                                        0;
                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Height :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newHeigth + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldHeigth + "</strike>" + "</td>" + "</tr>";
                            }

                            if ((newEbMarchandise.getLength() != null && oldEbMarchandise.getLength() == null) ||
                                    (newEbMarchandise.getLength() != null &&
                                            !oldEbMarchandise.getLength().equals(newEbMarchandise.getLength()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                Double newLength = newEbMarchandise.getLength() != null ?
                                        newEbMarchandise.getLength() :
                                        0;
                                Double oldLength = oldEbMarchandise.getLength() != null ?
                                        oldEbMarchandise.getLength() :
                                        0;
                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Length :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newLength + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldLength + "</strike>" + "</td>" + "</tr>";
                            }

                            // Comment field
                            if ((newEbMarchandise.getComment() != null && oldEbMarchandise.getComment() == null) ||
                                    (newEbMarchandise.getComment() != null &&
                                            !oldEbMarchandise.getComment().equals(newEbMarchandise.getComment()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                String newComment = newEbMarchandise.getComment() != null ?
                                        newEbMarchandise.getComment() :
                                        "";
                                String oldComment = oldEbMarchandise.getComment() != null ?
                                        oldEbMarchandise.getComment() :
                                        "";
                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Length :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newComment + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldComment + "</strike>" + "</td>" + "</tr>";
                            }

                            // Item Name field
                            if ((newEbMarchandise.getItemName() != null &&
                                    oldEbMarchandise.getItemName() == null) ||
                                    (newEbMarchandise.getItemName() != null &&
                                            !oldEbMarchandise.getItemName().equals(newEbMarchandise.getItemName()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                String newName = newEbMarchandise.getItemName() != null ?
                                        newEbMarchandise.getItemName() :
                                        "";
                                String oldName = oldEbMarchandise.getItemName() != null ?
                                        oldEbMarchandise.getItemName() :
                                        "";
                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Length :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newName + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldName + "</strike>" + "</td>" + "</tr>";
                            }

                            // Origin Country field
                            if ((newEbMarchandise.getOriginCountryName() != null &&
                                    oldEbMarchandise.getOriginCountryName() == null) ||
                                    (newEbMarchandise.getOriginCountryName() != null &&
                                            !oldEbMarchandise
                                                    .getOriginCountryName().equals(newEbMarchandise.getOriginCountryName()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                String newName = newEbMarchandise.getOriginCountryName() != null ?
                                        newEbMarchandise.getOriginCountryName() :
                                        "";
                                String oldName = oldEbMarchandise.getOriginCountryName() != null ?
                                        oldEbMarchandise.getOriginCountryName() :
                                        "";
                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Length :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newName + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldName + "</strike>" + "</td>" + "</tr>";
                            }

                            // Hs Code field
                            if ((newEbMarchandise.getHsCode() != null && oldEbMarchandise.getHsCode() == null) ||
                                    (newEbMarchandise.getHsCode() != null &&
                                            !oldEbMarchandise.getHsCode().equals(newEbMarchandise.getHsCode()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                String newCode = newEbMarchandise.getHsCode() != null ?
                                        newEbMarchandise.getHsCode() :
                                        "";
                                String oldCode = oldEbMarchandise.getHsCode() != null ?
                                        oldEbMarchandise.getHsCode() :
                                        "";
                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Length :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newCode + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldCode + "</strike>" + "</td>" + "</tr>";
                            }

                            // Price field
                            if ((newEbMarchandise.getPrice() != null && oldEbMarchandise.getPrice() == null) ||
                                    (newEbMarchandise.getPrice() != null &&
                                            !oldEbMarchandise.getPrice().equals(newEbMarchandise.getPrice()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                Double newPrice = newEbMarchandise.getPrice() != null ?
                                        newEbMarchandise.getPrice() :
                                        0;
                                Double oldPrice = oldEbMarchandise.getPrice() != null ?
                                        oldEbMarchandise.getPrice() :
                                        0;
                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Length :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newPrice + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldPrice + "</strike>" + "</td>" + "</tr>";
                            }

                            if ((newEbMarchandise.getVolume() != null && oldEbMarchandise.getVolume() == null) ||
                                    (newEbMarchandise.getVolume() != null &&
                                            !oldEbMarchandise.getVolume().equals(newEbMarchandise.getVolume()))) {

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                Double newVolume = newEbMarchandise.getVolume() != null ?
                                        newEbMarchandise.getVolume() :
                                        0;
                                Double oldVolume = oldEbMarchandise.getVolume() != null ?
                                        oldEbMarchandise.getVolume() :
                                        0;
                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Volume :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        pricingAnnexMethodsService
                                            .roundDecimals(newVolume, Constants.VALEUR_DECIMAL_ARRONDI) +
                                        "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " +
                                        pricingAnnexMethodsService
                                            .roundDecimals(oldVolume, Constants.VALEUR_DECIMAL_ARRONDI) +
                                        "</strike>" + "</td>" + "</tr>";
                            }

                            if ((newEbMarchandise.getExportControlStatut() != null &&
                                    oldEbMarchandise.getExportControlStatut() == null) ||
                                    (newEbMarchandise.getExportControlStatut() != null &&
                                            !oldEbMarchandise
                                                    .getExportControlStatut()
                                                    .equals(newEbMarchandise.getExportControlStatut()))) {
                                String oldExportControlStatut = oldEbMarchandise.getExportControlStatut() != null ?
                                        this
                                                .getExportControlStatutTranslatedByCode(
                                                        oldEbMarchandise.getExportControlStatut(),
                                                        "en") :
                                        null;

                                if (action.indexOf("Part Number") < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            newEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                String newExportControlStatut = newEbMarchandise.getExportControlStatut() != null ?
                                        this
                                                .getExportControlStatutTranslatedByCode(
                                                        newEbMarchandise.getExportControlStatut(),
                                                        "en") :
                                        null;

                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "Export control statut :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        newExportControlStatut + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + oldExportControlStatut + "</strike>" + "</td>" + "</tr>";
                            }

                            List<EbMarchandise> listChild = new ArrayList<EbMarchandise>();

                            if (newEbMarchandise.getChildren() != null &&
                                    !newEbMarchandise.getChildren().isEmpty()) {
                                newEbMarchandise.getChildren().stream().forEach(child -> {
                                    child.setParent(newEbMarchandise);
                                    listChild.add(child);
                                });
                            }

                            if (!listChild.isEmpty()) {

                                for (EbMarchandise unit : oldMarchandise) {
                                    EbMarchandise children = listChild
                                            .stream()
                                            .filter(
                                                    child -> child
                                                            .getCustomerReference()
                                                            .equalsIgnoreCase(unit.getCustomerReference()))
                                            .findFirst().orElse(null);

                                    if (children != null) {

                                        if (action
                                                .indexOf(
                                                        "<table class='width-100-percent margin-top-5'><tr>" +
                                                                "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                                                "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                                                oldEbMarchandise.getPartNumber() + "</div>" +
                                                                "<div class='padding-4'>Unit Reference : " +
                                                                oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>")
                                                < 0) {
                                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                                    "<div class='padding-4'>Unit Reference : " +
                                                    oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                        }

                                        action += "<tr>" +
                                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                                "fils :" + "</td>" +
                                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                                children.getParent().getUnitReference() + "</td>" +
                                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                                "<strike> " + unit.getUnitReference() + "</strike>" + "</td>" + "</tr>";
                                    } else {
                                    }

                                }

                            }

                            if (action
                                    .indexOf(
                                            "<table class='width-100-percent margin-top-5'><tr>" +
                                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                                    "<div class='padding-4'>Unit Reference : " +
                                                    oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>")
                                    >= 0) {
                                action += "</table>";
                            }

                            break;
                        }

                    }

                }

                // relation fils parent

                // mise à jour des nouvelles valeurs des marchandises enregistré
                // dans la demande
                Double totalVolume = ebDemande.getTotalVolume();
                Double totalWeight = ebDemande.getTotalWeight();
                Double totalTaxableWeight = ebDemande.getTotalTaxableWeight();

                if (totalVolume != null && oldEbDemande.getTotalVolume() != totalVolume) {

                    if (action.indexOf("units information updated :") < 0) {
                        action += "units information updated : ";
                    }

                    totalVolume = pricingAnnexMethodsService
                        .roundDecimals(totalVolume, Constants.VALEUR_DECIMAL_ARRONDI);

                    action += "total volume : " +
                        totalVolume + " <strike> " +
                        pricingAnnexMethodsService
                            .roundDecimals(oldEbDemande.getTotalVolume(), Constants.VALEUR_DECIMAL_ARRONDI) +
                        " </strike> <br> ";
                }

                if (totalWeight != null && oldEbDemande.getTotalWeight() != totalWeight) {

                    if (action.indexOf("units information updated :") < 0) {
                        action += "units information updated : ";
                    }

                    action += "total weight : " +
                            totalWeight + " <strike> " + oldEbDemande.getTotalWeight() + " </strike> <br> ";
                }

                if (totalTaxableWeight != null && oldEbDemande.getTotalTaxableWeight() != totalTaxableWeight) {

                    if (action.indexOf("units information updated :") < 0) {
                        action += "units information updated : ";
                    }

                    action += "total taxable weight : " +
                            totalTaxableWeight + " <strike> " + oldEbDemande.getTotalTaxableWeight() + " </strike> <br> ";
                }

            }

        }

        if (TypeDataEbDemande.EXTERNAL_USERS.getCode().equals(typeData) ||
                TypeDataEbDemande.ALL.getCode().equals(typeData)) {
            libelleTypeData = TypeDataEbDemande.EXTERNAL_USERS.getLibelle();

            if (ebDemande.getxEbUserOrigin() != null &&
                    (oldEbDemande.getxEbUserOrigin() == null ||
                            !ebDemande
                                    .getxEbUserOrigin().getEbUserNum().equals(oldEbDemande.getxEbUserOrigin().getEbUserNum())) ||
                    ebDemande.getxEbUserOrigin() == null && oldEbDemande.getxEbUserOrigin() != null) {
                mapHistory.put("xEbUserOrigin", oldEbDemande.getxEbUserOrigin());
                action += "User origin : " +
                        (ebDemande.getxEbUserOrigin() != null ? ebDemande.getxEbUserOrigin().getNom() : "-") +
                        " <strike> " +
                        (oldEbDemande.getxEbUserOrigin() != null ? oldEbDemande.getxEbUserOrigin().getNom() : "-") +
                        " </strike>";
            }

            if (ebDemande.getxEbUserDest() != null &&
                    (oldEbDemande.getxEbUserDest() == null ||
                            !ebDemande.getxEbUserDest().getEbUserNum().equals(oldEbDemande.getxEbUserDest().getEbUserNum())) ||
                    ebDemande.getxEbUserDest() == null && oldEbDemande.getxEbUserDest() != null) {
                mapHistory.put("xEbUserDest", oldEbDemande.getxEbUserDest());
                action += "User destination : " +
                        (ebDemande.getxEbUserDest() != null ? ebDemande.getxEbUserDest().getNom() : "-") + " <strike> " +
                        (oldEbDemande.getxEbUserDest() != null ? oldEbDemande.getxEbUserDest().getNom() : "-") +
                        " </strike>";
            }

            if (ebDemande.getxEbUserOriginCustomsBroker() != null &&
                    (oldEbDemande.getxEbUserOriginCustomsBroker() == null ||
                            !ebDemande
                                    .getxEbUserOriginCustomsBroker().getEbUserNum()
                                    .equals(oldEbDemande.getxEbUserOriginCustomsBroker().getEbUserNum())) ||
                    ebDemande.getxEbUserOriginCustomsBroker() == null &&
                            oldEbDemande.getxEbUserOriginCustomsBroker() != null) {
                mapHistory.put("xEbUserOriginCustomsBroker", oldEbDemande.getxEbUserOriginCustomsBroker());
                action += "User origin customs broker : " +
                        (ebDemande.getxEbUserOriginCustomsBroker() != null ?
                                ebDemande.getxEbUserOriginCustomsBroker().getNom() :
                                "-") +
                        " <strike> " +
                        (oldEbDemande.getxEbUserOriginCustomsBroker() != null ?
                                oldEbDemande.getxEbUserOriginCustomsBroker().getNom() :
                                "-") +
                        " </strike>";
            }

            if (

                    ebDemande.getxEbUserObserver() != null &&
                            !ebDemande.getxEbUserObserver().equals(oldEbDemande.getxEbUserObserver())

            ) {
                mapHistory.put("xEbUserObserver", oldEbDemande.getxEbUserObserver());
            }

            if (ebDemande.getxEbUserDestCustomsBroker() != null &&
                    (oldEbDemande.getxEbUserDestCustomsBroker() == null ||
                            !ebDemande
                                    .getxEbUserDestCustomsBroker().getEbUserNum()
                                    .equals(oldEbDemande.getxEbUserDestCustomsBroker().getEbUserNum())) ||
                    ebDemande.getxEbUserDestCustomsBroker() == null && oldEbDemande.getxEbUserDestCustomsBroker() != null) {
                mapHistory.put("xEbUserDestCustomsBroker", oldEbDemande.getxEbUserDestCustomsBroker());
                action += "User destination customs broker : " +
                        (ebDemande.getxEbUserDestCustomsBroker() != null ?
                                ebDemande.getxEbUserDestCustomsBroker().getNom() :
                                "-") +
                        " <strike> " +
                        (oldEbDemande.getxEbUserDestCustomsBroker() != null ?
                                oldEbDemande.getxEbUserDestCustomsBroker().getNom() :
                                "-") +
                        " </strike>";
            }

        }

        if (libelleTypeData != null) {

            if (action.length() > 0) {
                action = libelleTypeData + " updated: <br>" + action + " ";
            } else action = libelleTypeData + " updated ";

            listStatiqueService
                    .historizeAction(
                            ebDemande.getEbDemandeNum(),
                            Enumeration.IDChatComponent.PRICING.getCode(),
                            module,
                            action,
                            connectedUser.getRealUserNum() != null ?
                                    connectedUser.getRealUserNomPrenom() :
                                    connectedUser.getNomPrenom(),
                            connectedUser.getRealUserNum() != null ? connectedUser.getNomPrenom() : null);
            EbChat ebChat = listStatiqueService
                    .historizeAction(
                            ebDemande.getEbDemandeNum(),
                            Enumeration.IDChatComponent.TRANSPORT_MANAGEMENT.getCode(),
                            module,
                            action,
                            connectedUser.getRealUserNum() != null ?
                                    connectedUser.getRealUserNomPrenom() :
                                    connectedUser.getNomPrenom(),
                            connectedUser.getRealUserNum() != null ? connectedUser.getNomPrenom() : null);

            result.put("ebChat", ebChat);
            result.put("dataHistory", mapHistory);

            sb.append(action);
        }

        return result;
    }

    @Override
    public EbDemande confirmPendingDemande(EbDemande ebDemande) {
        ebDemande = confirmPendingDemandeWithTransactional(ebDemande);
        this.controlRuleService.processDocumentRules(ebDemande.getEbDemandeNum(), true, null);
        return ebDemande;
    }

    @Transactional
    public EbDemande confirmPendingDemandeWithTransactional(EbDemande ebDemande) {
        // sauvegarder la demande et générer les différents objets à créer pour
        // un dossier confirmé (génération des tracings, invoices, etc.)
        ebDemande.setModule(Enumeration.Module.TRANSPORT_MANAGEMENT.getCode());
        ebDemande.setxEcStatut(StatutDemande.WPU.getCode());
        ebDemande.setxEcStatutGroupage(ConsolidationGroupage.PCO.getCode());
        ebDemande.setConfirmedFromTransptPlan(true);

        //
        ExEbDemandeTransporteur quote = ebDemande
                .getExEbDemandeTransporteurs().stream()
                .filter(q -> q.getPrice() != null && q.getPrice().compareTo(BigDecimal.ZERO) > 0).findFirst().orElse(null);

        if (quote != null) {
            quote.setStatus(StatutCarrier.FIN_PLAN.getCode());
            ebDemande.getExEbDemandeTransporteurs().add(ebDemande.getExEbDemandeTransporteurs().indexOf(quote), quote);
        }

        ebDemande = pricingCreateAndUpdateService.updateEbDemandeObjects(ebDemande, ebDemande.getModule(), false);

        EbQrGroupeProposition propo = ebQrGroupePropositionRepository
                .getOneByEbDemandeResult_ebDemandeNum(ebDemande.getEbDemandeNum());
        propo.setStatut(GroupePropositionStatut.CONFIRMED.getCode());
        ebQrGroupePropositionRepository.save(propo);

        List<Integer> listDemandeNum = Arrays
                .asList(propo.getListDemandeNum().split(":")).stream().filter(it -> it != null && !it.isEmpty()).map(it -> {
                    it.replace(":", "");
                    return Integer.valueOf(it);
                }).collect(Collectors.toList());
        if (listDemandeNum != null && !listDemandeNum.isEmpty()) ebDemandeRepository
                .updateCancelStatusAndGroupDemandeNumListEbDemande(
                        listDemandeNum,
                        CancelStatus.CANECLLED.getCode(),
                        ebDemande.getEbDemandeNum());
        return ebDemande;
    }

    @Override
    public EbDemande addEbDemande(EbDemande ebDemande) {
        EbUser currentUser = connectedUserService.getCurrentUser();
        this.createEbDemande(ebDemande, ebDemande.getModule(), true, currentUser);
        sendEmailAfterCreation(ebDemande, currentUser);

        // update status delivery when tr is created
        if (ebDemande.getListDeliveryForConsolidateNum() != null) {

            try {
                for (Long ebLivraisonNum : ebDemande.getListDeliveryForConsolidateNum()) {
                    EbLivraison eblivraison = ebLivraisonRepository.findById(ebLivraisonNum).get();
                    eblivraison.setxEbDemande(ebDemande);

                    if (ebDemande.getRefTransport() != null) {
                        eblivraison.setRefTransport(ebDemande.getRefTransport());
                    }

                    ebLivraisonRepository.save(eblivraison);
                    deliveryService.calculateParentOrder(ebLivraisonNum, true);
                }

            } catch (MyTowerException e) {
                e.printStackTrace();
            }

        }
        return ebDemande;
    }

    @Transactional
    @Override
    public EbDemande createEbDemande(EbDemande ebDemande, Integer ebModuleNum, boolean activateTasksAfterCreation, EbUser connectedUser) {
        // The demande was created from form, create the demande with Pricing or
        // TM
        // logic
        // First common behavior
        NatureDemandeTransport nature = NatureDemandeTransport.NORMAL;

        if (ebDemande.getxEcNature() != null) nature = NatureDemandeTransport.fromCode(ebDemande.getxEcNature());

        if (ebDemande.getIsGrouping() == null) {
            ebDemande.setIsGrouping(false);
        }

        resolveCostItemsCurrency(ebDemande);

        // cette boucle sert a setter la mention dg de la demande a "true" si on
        // a au
        // moins une marchandise dangereuse dans la demande
        if (ebDemande.getListMarchandises() != null) {
            ebDemande.setDg(false);

            for (int i = 0; i < ebDemande.getListMarchandises().size(); i++) {
                Integer marchandiseDg = ebDemande.getListMarchandises().get(i).getDangerousGood();

                if (marchandiseDg != null && !MarchandiseDangerousGood.NO.getCode().equals(marchandiseDg)) {
                    ebDemande.setDg(true);
                    break;
                }

            }

            ebDemande.setExportControlStatut(this.getExportControlStatusFromUnits(ebDemande.getListMarchandises()));
        }

        EbDemande savedDemande = pricingCreateAndUpdateService
            .createEbDemandeNormalOrTF(ebDemande, nature, ebModuleNum, connectedUser);

        if (Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(ebModuleNum)) {
            nature = NatureDemandeTransport.TRANSPORT_FILE;
        }

        // Specific behavior for TM or Pricing
        if (nature == NatureDemandeTransport.NORMAL ||
                nature == NatureDemandeTransport.CONSOLIDATION || nature == NatureDemandeTransport.EDI) {
            savedDemande = postCreateEbDemandeNormal(savedDemande, activateTasksAfterCreation, connectedUser);
        } else if (nature == NatureDemandeTransport.TRANSPORT_FILE) {
            savedDemande = postCreateEbDemandeTF(savedDemande, activateTasksAfterCreation, connectedUser);
        }

        if (activateTasksAfterCreation) savedDemande = pricingAnnexMethodsService.afterDemandeNormalOrTrCreated(savedDemande, connectedUser);

        return savedDemande;
    }

    private void resolveCostItemsCurrency(EbDemande demande) {
        if (demande.getExEbDemandeTransporteurs() != null && !demande.getExEbDemandeTransporteurs().isEmpty()) {
            demande.getExEbDemandeTransporteurs().forEach(demandeTransporteur -> {
                if (demandeTransporteur.getListPlCostItem() != null &&
                        !demandeTransporteur.getListPlCostItem().isEmpty()) {
                    demandeTransporteur.getListPlCostItem().forEach(costItem -> {

                        if (costItem.getCurrency() == null || costItem.getCurrency().getEcCurrencyNum() == null) {
                            costItem.setCurrency(demande.getXecCurrencyInvoice());
                        }

                        if (costItem.getEuroExchangeRate() == null) {
                            Double defaultExchangeRate = 1d;
                            costItem.setEuroExchangeRate(defaultExchangeRate);
                        }

                    });

                }
            });

        }
    }

    public EbEtablissement findSolicitedTDCEtab(List<ExEbDemandeTransporteur> exEbDemandeTransporteurs) {
        Integer tdcEtabNum = null;

        if (exEbDemandeTransporteurs != null && !exEbDemandeTransporteurs.isEmpty()) {
            Optional<ExEbDemandeTransporteur> exEbDemandeTransporteurForTDC = exEbDemandeTransporteurs
                    .stream().filter(exEbDemandeTransporteur -> {
                        if (exEbDemandeTransporteur.getxEbCompagnie()
                                != null)
                            return exEbDemandeTransporteur.getxEbCompagnie().getEbCompagnieNum().equals(-1);
                        else return false;
                    }).findFirst();

            if (exEbDemandeTransporteurForTDC.isPresent()) {
                tdcEtabNum = exEbDemandeTransporteurForTDC.get().getxEbEtablissement().getEbEtablissementNum();
            }

        }

        return tdcEtabNum != null ? new EbEtablissement(tdcEtabNum) : null;
    }


    private EbDemande postCreateEbDemandeNormal(EbDemande ebDemande, boolean activateTasksAfterCreation, EbUser connectedUser) {
        if (ebDemande.getListTransporteurs() != null && !ebDemande.getListTransporteurs().isEmpty()) {
            // Generate ExEbDemandeTransport
            ebDemande.setExEbDemandeTransporteurs(ebDemande.getListTransporteurs().stream().map(transporteur -> {
                ExEbDemandeTransporteur exDemandeTransporteur = new ExEbDemandeTransporteur(ebDemande, transporteur);
                exDemandeTransporteur
                        .setxEbEtablissement(
                                new EbEtablissement(transporteur.getEbEtablissement().getEbEtablissementNum()));

                if (transporteur.getSelected() != null && transporteur.getSelected()) {

                    exDemandeTransporteur.setStatus(Enumeration.StatutCarrier.FIN_PLAN.getCode());
                    ebDemande.setxEcStatut(Enumeration.StatutDemande.WPU.getCode());
                    ebDemande.setValuationType(exDemandeTransporteur.getStatus());
                    ebDemande.setConfirmed(new Date());
                    ebDemandeRepository
                            .updateStatusAndDetailsTransporteurFinal(
                                    ebDemande.getEbDemandeNum(),
                                    Enumeration.StatutDemande.FIN.getCode(),
                                    transporteur.getNom(),
                                    transporteur.getEbUserNum(),
                                    transporteur.getEbEtablissement().getEbEtablissementNum(),
                                    true);

                    ebDemande.set_exEbDemandeTransporteurFinal(exDemandeTransporteur);
                } else if (exDemandeTransporteur.getxTransporteur().getIsFromTransPlan() != null &&
                        exDemandeTransporteur.getxTransporteur().getIsFromTransPlan()) {
                    exDemandeTransporteur.setStatus(Enumeration.StatutCarrier.TRANSPORT_PLAN.getCode());
                } else {
                    exDemandeTransporteur.setStatus(Enumeration.StatutCarrier.RRE.getCode());
                }

                if (transporteur.getIsFromTransPlan() != null && transporteur.getIsFromTransPlan()) {
                    exDemandeTransporteur.setPrice(transporteur.getCalculatedPrice());
                    exDemandeTransporteur.setTransitTime(transporteur.getTransitTime());
                    exDemandeTransporteur.setRefPlan(transporteur.getRefPlan());
                    exDemandeTransporteur.setComment(transporteur.getComent());
                }

                exDemandeTransporteur.setExchangeRateFound(transporteur.getExchangeRateFound());
                exDemandeTransporteur.setIsFromTransPlan(transporteur.getIsFromTransPlan());
                if (transporteur.getEbCompagnie() != null) exDemandeTransporteur
                        .setxEbCompagnie(new EbCompagnie(transporteur.getEbCompagnie().getEbCompagnieNum()));
                exDemandeTransporteur.setCityPickup(ebDemande.getEbPartyOrigin().getCity());
                exDemandeTransporteur.setxEcCountryPickup(ebDemande.getEbPartyOrigin().getxEcCountry());
                exDemandeTransporteur.setCityDelivery(ebDemande.getEbPartyDest().getCity());
                exDemandeTransporteur.setxEcCountryDelivery(ebDemande.getEbPartyDest().getxEcCountry());

                exDemandeTransporteur.setxEbUserCt(ebDemande.getxEbUserCt());

                return exDemandeTransporteur;
            }).collect(Collectors.toList()));
        } else if (ebDemande.getExEbDemandeTransporteurs() != null &&
                !ebDemande.getExEbDemandeTransporteurs().isEmpty()) {

			if ((ebDemande.getxEcNature().equals(NatureDemandeTransport.CONSOLIDATION.getCode())
					&& ebDemande.getxEcStatut().equals(StatutDemande.PND.getCode()))
					|| (ebDemande.getxEcNature().equals(NatureDemandeTransport.EDI.getCode()))) {

				try {
					unitValuation(ebDemande);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}

            ebDemande.getExEbDemandeTransporteurs().stream().forEach(it -> {
                it
                        .setCityPickup(
                                ebDemande.getEbPartyOrigin() != null ? ebDemande.getEbPartyOrigin().getCity() : null);
                // TODO ce code écrase l'établissement des vignettes
                // transporteurs
                // il a été écrit dans le cadre du Jira CDT-322 : Le
                // développeur ne se rappelle plus du pourquoi de la chose
                // je le commente car il a causé des dégats sur la Release.
                /*
                 * it
                 * .setxEbEtablissement(
                 * ebDemande.getxEbEtablissement() != null ?
                 * ebDemande.getxEbEtablissement() :
                 * new EbEtablissement(connectedUser.getEbEtablissement().
                 * getEbEtablissementNum())
                 * );
                 */
                it
                        .setxEcCountryPickup(
                                ebDemande.getEbPartyOrigin() != null ? ebDemande.getEbPartyOrigin().getxEcCountry() : null);
                it
                        .setCityDelivery(
                                ebDemande.getEbPartyDest() != null ? ebDemande.getEbPartyDest().getCity() : null);
                it
                        .setxEcCountryDelivery(
                                ebDemande.getEbPartyDest() != null && ebDemande.getEbPartyDest() != null ?
                                        ebDemande.getEbPartyDest().getxEcCountry() :
                                        null);
                it.setxEbUserCt(ebDemande.getxEbUserCt());
                it.setxEbDemande(ebDemande);
                it
                        .setxTransporteur(
                                it.getxTransporteur() != null ?
                                        it.getxTransporteur() :
                                        new EbUser(ebDemande.getUser().getEbUserNum()));

                if (it.getxTransporteur() != null &&
                        it.getxTransporteur().getSelected() != null && it.getxTransporteur().getSelected()) {
                    it
                            .getxTransporteur()
                            .setEbEtablissement(new EbEtablissement(it.getxEbEtablissement().getEbEtablissementNum()));
                }

                if (it.getStatus() != null && it.getStatus() >= Enumeration.StatutCarrier.FIN.getCode()) {
                    ebDemande.set_exEbDemandeTransporteurFinal(it);
                }

            });
        }

        if (ebDemande.getExEbDemandeTransporteurs() != null && !ebDemande.getExEbDemandeTransporteurs().isEmpty()) {
            exEbDemandeTransporteurRepository.saveAll(ebDemande.getExEbDemandeTransporteurs());
        }

        /**
         * loop listEmailSendQuotation email dans plateform &
         * transporteur/TransporteurBroker => MER + send quotation email dans
         * plateform
         * & !transporteur/transporteurBroker => ignorer email pas dans
         * plateform =>
         * envoie MER + send quotation si role valable
         */
        if (ebDemande.getListEmailSendQuotation() != null && !ebDemande.getListEmailSendQuotation().trim().isEmpty()) {
            String[] listEmails = ebDemande.getListEmailSendQuotation().split(";");
            List<ExEbDemandeTransporteur> listDemandeTransporteurEmail = new ArrayList<ExEbDemandeTransporteur>();

            for (String email : listEmails) {
                if (email == null || email.trim().length() <= 0) continue;

                EbRelation ebRelation = new EbRelation();
                ebRelation.setEmail(email);
                ebRelation.setTypeRelation(Enumeration.ServiceType.SERVICE_TRANSPORTEUR.getCode());
                ebRelation.setHost(new EbUser(ebDemande.getUser().getEbUserNum()));
                Map<String, Object> map = userService
                        .sendInvitationByMail(
                                ebRelation,
                                Enumeration.ServiceType.SERVICE_TRANSPORTEUR.getCode(),
                                ebDemande.getUser().getEbUserNum());
                Boolean inRelation = (Boolean) map.get("inRelation");
                EbUser transporteur = (EbUser) map.get("user");

                ExEbDemandeTransporteur exDemandeTransporteur = new ExEbDemandeTransporteur(ebDemande, transporteur);

                if (inRelation != null &&
                        inRelation && transporteur != null &&
                        transporteur.getEbUserNum() != null) exDemandeTransporteur
                        .setxEbEtablissement(
                                new EbEtablissement(transporteur.getEbEtablissement().getEbEtablissementNum()));
                else {
                    transporteur.setEbUserNum(Statiques.UNKNOWN_USER_NUM);
                    transporteur.setEbEtablissement(new EbEtablissement((Statiques.UNKNOWN_USER_NUM)));
                    exDemandeTransporteur.setEmailTransporteur(email);
                    exDemandeTransporteur.setxTransporteur(transporteur);
                    exDemandeTransporteur.setxEbEtablissement(transporteur.getEbEtablissement());
                }

                exDemandeTransporteur.setStatus(Enumeration.StatutCarrier.RRE.getCode());
                exDemandeTransporteur.setCityPickup(ebDemande.getEbPartyOrigin().getCity());
                exDemandeTransporteur.setxEcCountryPickup(ebDemande.getEbPartyOrigin().getxEcCountry());
                exDemandeTransporteur.setCityDelivery(ebDemande.getEbPartyDest().getCity());
                exDemandeTransporteur.setxEcCountryDelivery(ebDemande.getEbPartyDest().getxEcCountry());

                exDemandeTransporteur.setxEbUserCt(ebDemande.getxEbUserCt());

                listDemandeTransporteurEmail.add(exDemandeTransporteur);
            }

            if (listDemandeTransporteurEmail != null && listDemandeTransporteurEmail.size() > 0) {
                exEbDemandeTransporteurRepository.saveAll(listDemandeTransporteurEmail);
            }

        }

        // Others datas
        ebDemande.setModuleCreator(Enumeration.Module.PRICING.getCode());

        // set libelle type request
        if (ebDemande.getxEbTypeRequest() != null) {
            EbTypeRequest typeRequest = ebTypeRequestRepository.getOne(ebDemande.getxEbTypeRequest());
            ebDemande.setTypeRequestLibelle(typeRequest.getReference() + "-" + typeRequest.getLibelle());
            ebDemande.setTorDuration(typeRequest.getDelaiChronoPricing());
            ebDemande.setTorType(typeRequest.getSensChronoPricing());
        }

                ebDemandeRepository.save(ebDemande);

				if (ebDemande.getxEcStatut() >= StatutDemande.WPU.getCode())
				{
					this
						.updateStatusQuotation(
							ebDemande.get_exEbDemandeTransporteurFinal(),
							false,
							Enumeration.Module.TRANSPORT_MANAGEMENT.getCode()
						);
				}

        return ebDemande;
    }

    public void sendEmailAfterCreation(EbDemande ebDemande, EbUser connectedUser) {
        ExecutorService service = Executors.newCachedThreadPool();
        service.submit(() -> {
            String lang = connectedUser != null && connectedUser.getLanguage() != null ?
                connectedUser.getLanguage() :
                "en";
            Locale lcl = new Locale(lang);

            // History
            listStatiqueService
                .historizeAction(
                    ebDemande.getEbDemandeNum(),
                    Enumeration.IDChatComponent.PRICING.getCode(),
                    Enumeration.Module.PRICING.getCode(),
                    messageSource.getMessage("tracing.message_rd.quotation_created", null, lcl),
                    connectedUser.getRealUserNum() != null ?
                        connectedUser.getRealUserNomPrenom() :
                        connectedUser.getNomPrenom(),
                    connectedUser.isControlTower() || connectedUser.getRealUserNum() != null ?
                        ebDemande.getUser().getNomPrenom() :
                        null);

            // Send email
            if (ebDemande.isFlagRecommendation() != null && ebDemande.isFlagRecommendation()) {
                emailServicePricingBooking.sendEmailQuotationRecommendation(ebDemande);
            }

            if (!isNatureConsolidationOrEDI(ebDemande)) {
                List<EbUser> transporteurs = ebDemande
                    .getExEbDemandeTransporteurs().stream().map(ExEbDemandeTransporteur::getxTransporteur)
                    .collect(Collectors.toList());

                transporteurs
                    .parallelStream().forEach(
                        transporteur -> emailServicePricingBooking.sendEmailQuotationRequest(ebDemande, transporteur));

                emailServicePricingBooking.sendEmailQuotationResquestChargeur(ebDemande, transporteurs);
            }

            if (NatureDemandeTransport.CONSOLIDATION.getCode().equals(ebDemande.getxEcNature()) &&
                ebDemande.getxEcStatut() >= StatutDemande.WPU.getCode()) {
                ExEbDemandeTransporteur quote = ebDemande
                    .getExEbDemandeTransporteurs().stream().filter(q -> q.getPrice().compareTo(BigDecimal.ZERO) > 0)
                    .findFirst().orElse(null);

                if (quote != null) {
                    emailServicePricingBooking.sendEmailQuotationForwarderSelected(connectedUser, ebDemande, quote);
                }

            }

        });
    }

		private boolean isNatureConsolidationOrEDI(EbDemande ebDemande)
		{
			return ebDemande.getxEcNature().equals(NatureDemandeTransport.EDI.getCode()) ||
				ebDemande.getxEcNature().equals(NatureDemandeTransport.CONSOLIDATION.getCode());
		}

		private EbDemande postCreateEbDemandeTF(EbDemande ebDemande, boolean activateTasksAfterCreation, EbUser connectedUser)
		{

		    // Generate ExEbDemandeTransport
        List<ExEbDemandeTransporteur> exEbDemandeTransporteurs = new ArrayList<>();
        ExEbDemandeTransporteur finalDemandeTransporteur = null;
        EbUser finalTransporteur = null;

        for (ExEbDemandeTransporteur dt : ebDemande.getExEbDemandeTransporteurs()) {

            if (dt.getStatus() != null && dt.getStatus() > -1) {
                EbEtablissement ebEtablissement = ebEtablissementRepository
                        .getOne(dt.getxEbEtablissement().getEbEtablissementNum());

                Optional<EbUser> transporteur = ebUserRepository.findById(dt.getxTransporteur().getEbUserNum());
                ExEbDemandeTransporteur exDemandeTransporteur = new ExEbDemandeTransporteur(
                        ebDemande,
                        dt.getxTransporteur());

                exDemandeTransporteur.setxEbDemande(ebDemande);

                exDemandeTransporteur.setStatus(dt.getStatus());
                exDemandeTransporteur.setxEbEtablissement(ebEtablissement);
                exDemandeTransporteur
                        .setxEbCompagnie(new EbCompagnie(transporteur.get().getEbCompagnie().getEbCompagnieNum(),
                            transporteur.get().getEbCompagnie().getNom()));

                exDemandeTransporteur.setCityPickup(ebDemande.getEbPartyOrigin().getCity());
                exDemandeTransporteur.setxEcCountryPickup(ebDemande.getEbPartyOrigin().getxEcCountry());
                exDemandeTransporteur.setCityDelivery(ebDemande.getEbPartyDest().getCity());
                exDemandeTransporteur.setxEcCountryDelivery(ebDemande.getEbPartyDest().getxEcCountry());

                exDemandeTransporteur.setxEbUserCt(ebDemande.getxEbUserCt());

                exDemandeTransporteur.setTransitTime(dt.getTransitTime());
                exDemandeTransporteur.setRefPlan(dt.getRefPlan());
                exDemandeTransporteur.setComment(dt.getComment());
                exDemandeTransporteur.setIsFromTransPlan(dt.getIsFromTransPlan());
                exDemandeTransporteur.setPrice(dt.getPrice());
				exDemandeTransporteur.setHorsGrille(dt.isHorsGrille());

                // Save cotation fields for some status
                if (StatutCarrier.QSE.getCode().equals(exDemandeTransporteur.getStatus()) ||
                        StatutCarrier.FIN.getCode().equals(exDemandeTransporteur.getStatus()) ||
                        StatutCarrier.FIN_PLAN.getCode().equals(exDemandeTransporteur.getStatus())) {
                    exDemandeTransporteur.setPickupTime(dt.getPickupTime());
                    exDemandeTransporteur.setTransitTime(dt.getTransitTime());
                    exDemandeTransporteur.setDeliveryTime(dt.getDeliveryTime());
                    exDemandeTransporteur.setPrice(dt.getPrice());
                    exDemandeTransporteur.setComment(dt.getComment());
                    exDemandeTransporteur.setExchangeRate(dt.getExchangeRate());
                    exDemandeTransporteur.setInfosForCustomsClearance(dt.getInfosForCustomsClearance());
                    exDemandeTransporteur.setListCostCategorie(dt.getListCostCategorie());
                    // exDemandeTransporteur.setListCustomsFields(dt.getListCustomsFields());
                    exDemandeTransporteur.setListPlCostItem(dt.getListPlCostItem());
                    exDemandeTransporteur.setListEbTtCompagniePsl(dt.getListEbTtCompagniePsl());
                    exDemandeTransporteur.setxEcModeTransport(dt.getxEcModeTransport());
                }

                exEbDemandeTransporteurs.add(exDemandeTransporteur);

                if (dt.getxTransporteur().getSelected() != null && dt.getxTransporteur().getSelected()) {
                    dt
                            .getxTransporteur().setEbEtablissement(
                            ebEtablissementRepository.getOne(dt.getxEbEtablissement().getEbEtablissementNum()));
                }

                if (Enumeration.StatutCarrier.FIN.getCode().equals(exDemandeTransporteur.getStatus()) ||
                        Enumeration.StatutCarrier.FIN_PLAN.getCode().equals(exDemandeTransporteur.getStatus())) {
                    finalDemandeTransporteur = exDemandeTransporteur;
                    finalTransporteur = transporteur.get();
                }

            }

        }

        exEbDemandeTransporteurRepository.saveAll(exEbDemandeTransporteurs);

        // Others datas
        ebDemande.setModuleCreator(Enumeration.Module.TRANSPORT_MANAGEMENT.getCode());

        // set libelle type request
        if (ebDemande.getxEbTypeRequest() != null) {
            EbTypeRequest typeRequest = ebTypeRequestRepository.findById(ebDemande.getxEbTypeRequest()).get();
            ebDemande.setTypeRequestLibelle(typeRequest.getReference() + "-" + typeRequest.getLibelle());
            ebDemande.setTorDuration(typeRequest.getDelaiChronoPricing());
            ebDemande.setTorType(typeRequest.getSensChronoPricing());
        }

        if (ebDemande.getxEcStatut() == null || ebDemande.getxEcStatut() < Enumeration.StatutDemande.WPU.getCode()) {
            ebDemande.setxEcStatut(Enumeration.StatutDemande.WPU.getCode());
            ebDemande.setConfirmed(new Date());
        }

        ebDemandeRepository.save(ebDemande);

        if (activateTasksAfterCreation) {
            String lang = connectedUser != null && connectedUser.getLanguage() != null ?
                    connectedUser.getLanguage() :
                    "en";
            Locale lcl = new Locale(lang);

            // History
            listStatiqueService
                    .historizeAction(
                            ebDemande.getEbDemandeNum(),
                            Enumeration.IDChatComponent.TRANSPORT_MANAGEMENT.getCode(),
                            Enumeration.Module.TRANSPORT_MANAGEMENT.getCode(),
                            messageSource.getMessage("tracing.message_rd.transport_file", null, lcl),
                            connectedUser.getRealUserNum() != null ?
                                    connectedUser.getRealUserNomPrenom() :
                                    connectedUser.getNomPrenom(),
                            connectedUser.isControlTower() || connectedUser.getRealUserNum() != null ?
                                    ebDemande.getUser().getNomPrenom() :
                                    null);

            // Send email
            if (ebDemande.getSendEmails() != null && ebDemande.getSendEmails()) {
                ebDemande.set_exEbDemandeTransporteurFinal(finalDemandeTransporteur);
                ebDemande.setExEbDemandeTransporteurFinal(finalTransporteur);

                if (ebDemande.getConfirmedFromTransptPlan() != null && ebDemande.getConfirmedFromTransptPlan()) {
                    emailServicePricingBooking
                            .sendEmailQuotationForwarderSelected(
                                    connectedUser,
                                    ebDemande,
                                    ebDemande.get_exEbDemandeTransporteurFinal());
                } else {
                    emailServicePricingBooking.sendEmailCreationTransportFile(ebDemande);
                }

            }

        }

        // Confirm final choice
        // ebDemande.setxEcStatut(Enumeration.StatutDemande.WPU.getCode());

        if ((finalDemandeTransporteur.getxEbDemande() == null ||
                finalDemandeTransporteur.getxEbDemande().getEbDemandeNum() == null) &&
                ebDemande.getEbDemandeNum() != null) finalDemandeTransporteur
                .setxEbDemande(ebDemandeRepository.getOne(ebDemande.getEbDemandeNum()));
        this.updateStatusQuotation(finalDemandeTransporteur, false, Enumeration.Module.TRANSPORT_MANAGEMENT.getCode());

        return ebDemande;
    }


    @Override
    public Map<String, Object> getTaskProgress(String ident, InputStream file, String fileName, Integer ebUserNum) {

        if (ident != null &&
                !ident.contentEquals("-") && progressBulkImport != null && progressBulkImport.get(ident) != null) {

            if (progressBulkImport.get(ident).get("finished") != null &&
                    progressBulkImport.get(ident).get("finished").equals(progressBulkImport.get(ident).get("total"))) {
                Map<String, Object> res = progressBulkImport.get(ident);
                progressBulkImport.remove("ident");
                return res;
            } else return progressBulkImport.get(ident);

        }

        if (ident != null && ident.contentEquals("-")) {
            if (progressBulkImport == null) progressBulkImport = new HashMap<String, Map<String, Object>>();
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("ident", UUID.randomUUID().toString());
            map.put("ebUserNum", ebUserNum);
            map.put("file", file);
            map.put("fileName", fileName);
            progressBulkImport.put((String) map.get("ident"), map);
            return map;
        }

        return new HashMap<String, Object>();
    }

    @Override
    public void bulkImportDemande(
            InputStream file,
            String fileName,
            Integer ebUserNum,
            List<Map<String, String>> errors,
            String ident)
            throws Exception {

        try {
            if (true) return;

            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFSheet sheetDemande = workbook.getSheetAt(0);
            List<CompletableFuture<Boolean>> listCompletableFuture = new ArrayList<CompletableFuture<Boolean>>();
            Gson gson = new Gson();
            Date dateCreation = new Date();
            int i = 0, n = sheetDemande.getLastRowNum() + 1;

            EbUser connectedUser = ebUserRepository.getOneByEbUserNum(ebUserNum);
            List<EcCurrency> listEcCurrency = ecCurrencyRepository.findAll();

            // List<EbCostCategorie> listEbCostCategorie =
            // ebCostCategorieRepository.findAllWithListEbCost();
            // listEbCostCategorie.stream().forEach(cat ->
            // cat.getListEbCost().stream().forEach(it -> {
            // it.setxEbCostCategorie(null);
            // }));
            // final String jsonListEbCostCategorie =
            // gson.toJson(listEbCostCategorie,
            // new TypeToken<ArrayList<EbCostCategorie>>() {
            // }.getType());

            if (progressBulkImport == null) progressBulkImport = new HashMap<String, Map<String, Object>>();

            Map<String, Object> res = new HashMap<String, Object>();
            res.put("finished", 0);
            res.put("total", n - 2);
            progressBulkImport.put(ident, res);

            for (i = 2; i < n; i++) {
                final Row row = sheetDemande.getRow(i);

                // if (row != null) {
                // listCompletableFuture.add(
                // CompletableFuture.supplyAsync(() ->
                // this.processRowDemande(connectedUser,
                // fileName, ident,
                // row, workbook, listEcCurrency, jsonListEbCostCategorie,
                // dateCreation,
                // errors)));
                // }
            }

            CompletableFuture<Void> allFutures = CompletableFuture
                    .allOf(listCompletableFuture.toArray(new CompletableFuture[listCompletableFuture.size()]));
            allFutures.get();

            workbook.close();
        } catch (Exception e) {
            e.printStackTrace();

            throw e;
        }

    }

    private String getCellValue(Cell cell) {
        String strCellValue = null;

        if (cell == null) return null;

        if (cell.getCellTypeEnum() == CellType.STRING) strCellValue = cell.toString();
        else if (cell.getCellTypeEnum() == CellType.NUMERIC) {

            if (DateUtil.isCellDateFormatted(cell)) {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                strCellValue = dateFormat.format(cell.getDateCellValue());
            } else {
                Double value = cell.getNumericCellValue();
                if (value != Math.floor(value)) strCellValue = new String(value.toString());
                strCellValue = new String(value.longValue() + "");
            }

        } else if (cell.getCellTypeEnum() == CellType.BOOLEAN) {
            strCellValue = new String(new Boolean(cell.getBooleanCellValue()).toString());
        } else if (cell.getCellTypeEnum() == CellType.BLANK) {
            strCellValue = null;
        } else if (cell.getCellTypeEnum() == CellType.FORMULA) {

            if (cell.getCachedFormulaResultTypeEnum() == CellType.NUMERIC) {
                Double value = cell.getNumericCellValue();
                if (value != Math.floor(value)) strCellValue = new String(value.toString());
                strCellValue = new String(value.longValue() + "");
            } else if (cell.getCachedFormulaResultTypeEnum() == CellType.STRING) {
                strCellValue = cell.getRichStringCellValue().toString();
            }

        } else strCellValue = null;

        return strCellValue;
    }

    @Transactional
    boolean processRowDemande(
            EbUser connectedUser,
            String pathImport,
            String ident,
            Row rowDemande,
            XSSFWorkbook workbook,
            List<EcCurrency> listEcCurrency,
            String jsonListEbCostCategorie,
            Date dateCreation,
            List<Map<String, String>> errors) {
        boolean isCompleted = true;
        String identifiant = null;
        String cellValue = null;
        Gson gson = new Gson();
        List<EbTtTracing> listEbTrack = null;
        XSSFSheet sheetCommande = workbook.getSheetAt(1);
        XSSFSheet sheetTransporteur = workbook.getSheetAt(2);
        XSSFSheet sheetPostCout = workbook.getSheetAt(3);
        XSSFSheet sheetCategories = workbook.getSheetAt(4);
        XSSFSheet sheetCustomFields = workbook.getSheetAt(5);
        XSSFSheet sheetTracing = workbook.getSheetAt(6);
        Row row = null;

        try {
            int indexCell = 0;
            EbDemande ebDemande = new EbDemande();
            List<EbMarchandise> listEbMarchandise = new ArrayList<EbMarchandise>();

            if (rowDemande.getCell(indexCell) != null &&
                    (identifiant = this.getCellValue(rowDemande.getCell(indexCell))) != null &&
                    !(identifiant = identifiant.trim()).isEmpty()) {
                ebDemande.setEbDemandeNum(daoPricing.nextValEbDemande());
                ebDemande.setDateCreation(dateCreation);
                ebDemande.setxEbUserCt(new EbUser(connectedUser.getEbUserNum()));
                ebDemande.setxEbCompagnieCt(new EbCompagnie(connectedUser.getEbCompagnie().getEbCompagnieNum()));
                ebDemande
                        .setxEbEtablissementCt(
                                new EbEtablissement(connectedUser.getEbEtablissement().getEbEtablissementNum()));
                ebDemande.setPathImport(pathImport);
                ebDemande.setxEbUserImport(connectedUser);

                // get data demande
                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));
                if (cellValue == null || (cellValue = cellValue.trim()).isEmpty()) throw new IOException(
                        "Email du chargeur non renseigné dans la cellule (" +
                                rowDemande.getRowNum() + ", " + indexCell + ")");
                EbUser chargeur = ebUserRepository.findOneByEmailIgnoreCase(cellValue);

                if (chargeur != null) {
                    ebDemande.setUser(chargeur);
                    ebDemande
                            .setxEbEtablissement(
                                    new EbEtablissement(chargeur.getEbEtablissement().getEbEtablissementNum()));
                    ebDemande.setxEbCompagnie(new EbCompagnie(chargeur.getEbCompagnie().getEbCompagnieNum()));
                } else throw new IOException(
                        "Email du chargeur non disponible dans la plateforme dans la cellule (" +
                                rowDemande.getRowNum() + ", " + indexCell + ")");

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    ebDemande.setRefTransport(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    ebDemande.setCustomerReference(cellValue);
                }

                EbCostCenter costCenter = null;
                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    costCenter = ebCostCenterRepository
                            .findFirstByCompagnie_ebCompagnieNumAndLibelle(
                                    chargeur.getEbCompagnie().getEbCompagnieNum(),
                                    cellValue);
                    if (costCenter != null) ebDemande.setxEbCostCenter(costCenter);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));
                Integer code = null;

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    code = Enumeration.TypeDemande.getCodeByLibelle(cellValue);
                    if (code != null) ebDemande.setxEcTypeDemande(code);
                }

                if (code == null) throw new IOException(
                        "Type de demande invalide dans la cellule (" + rowDemande.getRowNum() + ", " + indexCell + ")");

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));
                Date dt = null;

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {

                    if (Pattern.compile("([0-9]{2}/){2}[0-9]{4}").matcher(cellValue).matches()) {
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                        try {
                            dt = formatter.parse(cellValue);
                            ebDemande.setDateOfGoodsAvailability(dt);
                        } catch (Exception e) {
                        }

                    }

                }

                if (dt == null) throw new IOException(
                        "Date of goods availability invalide dans la cellule (" +
                                rowDemande.getRowNum() + ", " + indexCell + ")");

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));
                dt = null;

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {

                    if (Pattern.compile("([0-9]{2}/){2}[0-9]{4}").matcher(cellValue).matches()) {
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                        try {
                            dt = formatter.parse(cellValue);
                            ebDemande.setDateOfArrival(dt);
                        } catch (Exception e) {
                        }

                    }

                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));
                code = null;

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    code = Enumeration.ModeTransport.getCodeByLibelle(cellValue);
                    if (code != null) ebDemande.setxEcModeTransport(code);
                }

                if (code == null) throw new IOException(
                        "Mode de transport invalide dans la cellule (" + rowDemande.getRowNum() + ", " + indexCell + ")");

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    EbTypeTransport ebTypeTransport = ebTypeTransportRepository
                            .findEbTypeTransportByLibeleAndModeTransport(cellValue.toUpperCase(), code);

                    if (ebTypeTransport != null) {
                        code = ebTypeTransport.getEbTypeTransportNum();
                        ebDemande.setxEbTypeTransport(code);
                    }

                }

                EbParty party = new EbParty();
                EcCountry country;
                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setCompany(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setAdresse(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setCity(cellValue);
                } else throw new IOException(
                        "Champs city non renseigné dans la cellule (" + rowDemande.getRowNum() + ", " + indexCell + ")");

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setZipCode(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));
                country = null;

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    country = ecCountryRepository.findFirstByCode(cellValue);

                    if (country != null) {
                        party.setxEcCountry(country);
                        ebDemande.setLibelleOriginCountry(country.getLibelle());
                    }

                }

                if (country == null) throw new IOException(
                        "Champs country vide ou invalide dans la cellule (" +
                                rowDemande.getRowNum() + ", " + indexCell + ")");
                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setOpeningHours(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setAirport(cellValue);
                }

                ebPartyRepository.save(party);
                ebDemande.setEbPartyOrigin(party);

                party = new EbParty();
                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setCompany(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setAdresse(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setCity(cellValue);
                } else throw new IOException(
                        "Champs city non renseigné dans la cellule (" + rowDemande.getRowNum() + ", " + indexCell + ")");

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setZipCode(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));
                country = null;

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    country = ecCountryRepository.findFirstByCode(cellValue);

                    if (country != null) {
                        party.setxEcCountry(country);
                        ebDemande.setLibelleDestCountry(country.getLibelle());
                    }

                }

                if (country == null) throw new IOException(
                        "Champs country vide ou invalide dans la cellule (" +
                                rowDemande.getRowNum() + ", " + indexCell + ")");
                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setOpeningHours(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setAirport(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setPhone(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setEmail(cellValue);
                }

                ebPartyRepository.save(party);
                ebDemande.setEbPartyDest(party);

                boolean emptyData = true;
                party = new EbParty();
                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setCompany(cellValue);
                    emptyData = false;
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setAdresse(cellValue);
                    emptyData = false;
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setCity(cellValue);
                    emptyData = false;
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setZipCode(cellValue);
                    emptyData = false;
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));
                country = null;

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    country = ecCountryRepository.findFirstByCode(cellValue);

                    if (country != null) {
                        party.setxEcCountry(country);
                        emptyData = false;
                    }

                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setOpeningHours(cellValue);
                    emptyData = false;
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setAirport(cellValue);
                    emptyData = false;
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setPhone(cellValue);
                    emptyData = false;
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    party.setEmail(cellValue);
                    emptyData = false;
                }

                ebPartyRepository.save(party);
                ebDemande.setEbPartyNotif(party);

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    ebDemande
                            .setInsurance(
                                    cellValue.toLowerCase().contains("yes") ?
                                            true :
                                            cellValue.toLowerCase().contains("oui") ? true : false);
                } else throw new IOException(
                        "Champs insurance vide ou invalide dans la cellule (" +
                                rowDemande.getRowNum() + ", " + indexCell + ")");

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    ebDemande.setInsuranceValue(Double.valueOf(cellValue));
                } else if (ebDemande.getInsurance()) throw new IOException(
                        "Champs value de insurance vide ou invalide alors que insurance est active dans la cellule (" +
                                rowDemande.getRowNum() + ", " + indexCell + ")");

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));
                code = null;

                // ************************** MUST SUPPORT THE NEW WAY TO GET
                // PSL CONFIG
                // *********************
                if (ebDemande.getxEcModeTransport() != null &&
                        cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    code = Enumeration.Incoterms.getCodeByLibelle(cellValue);

                    if (code != null) {
                        // TODO: TO CHANGE
                        // ebDemande.setxEcIncoterm(code);
                        // String psl =
                        // ebIncotermPslRepository.selectyXEcIncoterm_ebIncotermNumAndModeTransport(
                        // ebDemande.getxEcIncoterm().intValue(),
                        // ebDemande.getxEcModeTransport());
                        ebDemande.setFlagEbtrackTrace(false);
                        // if (psl != null)
                        // ebDemande.setConfigPsl(psl);
                    }

                }

                if (code == null) throw new IOException(
                        "Incoterm vide ou invalide dans la cellule (" + rowDemande.getRowNum() + ", " + indexCell + ")");

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    ebDemande.setCity(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));
                code = null;

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {

                    for (EcCurrency curr : listEcCurrency) {

                        if (cellValue.toLowerCase().contentEquals(curr.getCode().toLowerCase())) {
                            ebDemande.setXecCurrencyInvoice(curr);
                            break;
                        }

                    }

                }

                if (ebDemande.getXecCurrencyInvoice() == null) throw new IOException(
                        "Invoice currency vide ou invalide dans la cellule (" +
                                rowDemande.getRowNum() + ", " + indexCell + ")");

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));
                code = null;

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    code = Enumeration.OriginCustomsBroker.getCodeByLibelle(cellValue);

                    if (code != null) {
                        ebDemande.setxEcTypeCustomBroker(code);
                    } else {
                        EbUser broker = ebUserRepository.findOneByEmailIgnoreCase(cellValue);

                        if (broker != null) {
                            ebDemande.setxEbUserOriginCustomsBroker(new EbUser(broker.getEbUserNum()));
                            ebDemande
                                    .setxEbCompagnieBroker(new EbCompagnie(broker.getEbCompagnie().getEbCompagnieNum()));
                            ebDemande
                                    .setxEbEtablissementBroker(
                                            new EbEtablissement(broker.getEbEtablissement().getEbEtablissementNum()));
                        }

                    }

                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    ebDemande.setCommentChargeur(cellValue);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    ebDemande
                            .setFlagRecommendation(
                                    cellValue.toLowerCase().contains("yes") ?
                                            true :
                                            cellValue.toLowerCase().contains("oui") ? true : false);
                }

                cellValue = this.getCellValue(rowDemande.getCell(++indexCell));
                code = null;

                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                    code = Enumeration.StatutDemande.getCodeByLibelle(cellValue);
                    if (code != null && code < Enumeration.StatutDemande.FIN.getCode()) ebDemande.setxEcStatut(code);
                    else if (code != null && code.equals(Enumeration.StatutDemande.FIN.getCode())) {
                        ebDemande.setxEcStatut(Enumeration.StatutDemande.WPU.getCode());
                        ebDemande.setConfirmed(new Date());
                    } else {
                        ebDemande.setxEcStatut(Enumeration.StatutDemande.INP.getCode());
                        ebDemande.setWaitingForQuote(new Date());
                    }
                }

                // get categories data
                Integer i, n = sheetCategories.getLastRowNum();
                List<EbCategorie> listCategories = ebCategorieRepository
                        .findDistinctByEtablissementEbEtablissementNum(
                                chargeur.getEbEtablissement().getEbEtablissementNum());
                List<EbCategorie> affectedCategories = new ArrayList<EbCategorie>();
                String listCategoriesStr = "";
                String listLabelsStr = "";

                for (EbCategorie category : listCategories) {
                    EbCategorie affCat = new EbCategorie();
                    affCat.setEbCategorieNum(category.getEbCategorieNum());
                    affCat.setLibelle(category.getLibelle());
                    affectedCategories.add(affCat);
                    if (!listCategoriesStr.isEmpty()) listCategoriesStr += ",";
                    listCategoriesStr += affCat.getEbCategorieNum();
                }

                for (i = 1; i <= n; i++) {
                    row = sheetCategories.getRow(i);
                    if (row == null) break;
                    cellValue = this.getCellValue(row.getCell(0));

                    if (cellValue == null ||
                            (cellValue = cellValue.trim()).isEmpty() || !cellValue.equals(identifiant)) {
                        continue;
                    }

                    String cellValueCategory = getCellValue(row.getCell(1));
                    String cellValueLabel = getCellValue(row.getCell(2));

                    if (cellValueCategory != null &&
                            !(cellValueCategory = cellValueCategory.trim().toLowerCase()).isEmpty() &&
                            cellValueLabel != null && !(cellValueLabel = cellValueLabel.trim().toLowerCase()).isEmpty()) {
                        int catIndex = 0;

                        for (EbCategorie cat : listCategories) {

                            if (cat.getLibelle().toLowerCase().equals(cellValueCategory)) {
                                break;
                            }

                            catIndex++;
                        }

                        if (catIndex < listCategories.size()) {

                            for (EbLabel label : listCategories.get(catIndex).getLabels()) {

                                if (label.getLibelle().toLowerCase().equals(cellValueLabel)) {
                                    EbLabel ebLabel = new EbLabel();
                                    ebLabel.setEbLabelNum(label.getEbLabelNum());
                                    ebLabel.setLibelle(label.getLibelle());
                                    affectedCategories.get(catIndex).setLabels(new HashSet<EbLabel>());
                                    affectedCategories.get(catIndex).getLabels().add(ebLabel);
                                    if (!listLabelsStr.isEmpty()) listLabelsStr += ",";
                                    listLabelsStr += label.getEbLabelNum();
                                    break;
                                }

                            }

                        }

                    }

                }

                ebDemande.setListCategories(affectedCategories);
                ebDemande.setListCategoriesStr(listCategoriesStr);
                ebDemande.setListLabels(listLabelsStr);

                // get data custom fields
                EbCustomField customFields = ebCustomFieldRepository
                        .findFirstByXEbCompagnie(chargeur.getEbCompagnie().getEbCompagnieNum());

                if (customFields != null) {
                    List<Map<String, Object>> fields = gson
                            .fromJson(customFields.getFields(), new TypeToken<ArrayList<Map<String, Object>>>() {
                            }.getType());

                    n = sheetCustomFields.getLastRowNum();

                    for (i = 1; i <= n; i++) {
                        row = sheetCustomFields.getRow(i);
                        if (row == null) break;
                        cellValue = this.getCellValue(row.getCell(0));

                        if (cellValue == null ||
                                (cellValue = cellValue.trim()).isEmpty() || !cellValue.equals(identifiant)) {
                            continue;
                        }

                        String label = getCellValue(row.getCell(1));
                        String val = getCellValue(row.getCell(2));

                        if (label != null &&
                                !(label = label.trim()).isEmpty() && val != null && !(val = val.trim()).isEmpty()) {

                            for (Map<String, Object> field : fields) {

                                if (field.get("label") != null &&
                                        ((String) field.get("label")).toLowerCase().equals(label.toLowerCase())) {
                                    field.put("value", val);
                                    break;
                                }

                            }

                        }

                    }

                    ebDemande.setCustomFields(gson.toJson(fields));
                }

                // get data commande
                String custRef = "";
                n = sheetCommande.getLastRowNum();

                for (i = 1; i <= n; i++) {
                    row = sheetCommande.getRow(i);
                    indexCell = 0;
                    if (row != null) cellValue = this.getCellValue(row.getCell(indexCell));

                    if (row == null && listEbMarchandise.isEmpty()) throw new IOException(
                            "Aucune commande trouvé dans la cellule (" + rowDemande.getRowNum() + ", " + indexCell + ")");

                    if (cellValue == null ||
                            (cellValue = cellValue.trim()).isEmpty() || !cellValue.contentEquals(identifiant)) continue;

                    EbMarchandise ebMarchandise = new EbMarchandise();

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        custRef = cellValue;
                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        ebMarchandise.setNumberOfUnits(Double.valueOf(cellValue).intValue());
                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));
                    code = null;

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        code = PricingBookingEnumeration.MarchandiseTypeOfUnit.getCodeByLibelle(cellValue);
                        if (code != null) ebMarchandise.setxEbTypeUnit(NumberUtils.toLong(code + ""));
                    }
                    // if(code == null)
                    // throw new IOException("Type de l'unite de la marchandise
                    // invalide dans la
                    // cellule (" + rowDemande.getRowNum() + ", " + indexCell +
                    // ")");

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        ebMarchandise.setWeight(Double.valueOf(cellValue));
                    }
                    // else
                    // throw new IOException("Poid de la marchandise invalide
                    // dans la cellule (" +
                    // rowDemande.getRowNum() + ", " + indexCell + ")");

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        ebMarchandise.setLength(Double.valueOf(cellValue));
                    }
                    // else
                    // throw new IOException("Longueur de la marchandise
                    // invalide dans la cellule ("
                    // + rowDemande.getRowNum() + ", " + indexCell + ")");

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        ebMarchandise.setWidth(Double.valueOf(cellValue));
                    }
                    // else
                    // throw new IOException("largeur de la marchandise invalide
                    // dans la cellule ("
                    // + rowDemande.getRowNum() + ", " + indexCell + ")");

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        ebMarchandise.setHeigth(Double.valueOf(cellValue));
                    }
                    // else
                    // throw new IOException("Hauteur de la marchandise invalide
                    // dans la cellule ("
                    // + rowDemande.getRowNum() + ", " + indexCell + ")");

                    Double volum = 0.0;
                    if (ebMarchandise.getHeigth() != null &&
                            ebMarchandise.getLength() != null &&
                            ebMarchandise.getWidth() != null) volum = ebMarchandise.getHeigth() *
                            ebMarchandise.getLength() * ebMarchandise.getWidth();
                    ebMarchandise.setVolume(volum / 1000000);

                    cellValue = this.getCellValue(row.getCell(++indexCell));
                    code = null;

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        code = PricingBookingEnumeration.MarchandiseDangerousGood.getCodeByLibelle(cellValue);
                        if (code != null) ebMarchandise.setDangerousGood(code);
                    }
                    // if(code == null)
                    // throw new IOException("Dangerous good invalide dans la
                    // cellule (" +
                    // rowDemande.getRowNum() + ", " + indexCell + ")");

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null &&
                            !(cellValue = cellValue.trim()).isEmpty() &&
                            !ebMarchandise
                                    .getDangerousGood()
                                    .equals(PricingBookingEnumeration.MarchandiseDangerousGood.NO.getCode())) {
                        ebMarchandise.setUn(cellValue);
                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null &&
                            !(cellValue = cellValue.trim()).isEmpty() &&
                            !ebMarchandise
                                    .getDangerousGood()
                                    .equals(PricingBookingEnumeration.MarchandiseDangerousGood.NO.getCode())) {
                        ebMarchandise.setClassGood(cellValue);
                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        ebMarchandise.setPackaging(cellValue);
                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        ebMarchandise
                                .setStackable(
                                        cellValue.toLowerCase().contains("yes") ?
                                                true :
                                                cellValue.toLowerCase().contains("oui") ? true : false);
                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        ebMarchandise.setComment(cellValue);
                    }

                    /*
                     * final String fCustRef = custRef; Optional<EbCommande> opt
                     * =
                     * listEbCommande.stream().filter(it ->
                     * it.getCustomerReference().equals(fCustRef)).findFirst();
                     * if (opt.isPresent())
                     * { opt.get().getListMarchandise().add(ebMarchandise); }
                     * else {
                     */
                    ebMarchandise.setCustomerReference(custRef);

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        ebMarchandise.setIdUnite(cellValue);
                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        ebMarchandise.setCodeAlphaPslCourant(cellValue);
                    }

                    ebMarchandise.setEbDemande(new EbDemande(ebDemande.getEbDemandeNum()));
                    listEbMarchandise.add(ebMarchandise);
                    // }
                }

                Double ttWeight = 0.0;
                Double ttVolume = 0.0;
                Double ttUnits = 0.0;

                Double weight = 0.0;

                Double volume = 0.0;
                Double units = 0.0;

                for (EbMarchandise ebMarchandise : listEbMarchandise) {
                    if (ebMarchandise.getWeight() != null) weight += ebMarchandise.getWeight();
                    volume += ebMarchandise.getVolume();
                    if (ebMarchandise.getNumberOfUnits() != null) units += ebMarchandise.getNumberOfUnits();
                }

                ttWeight += weight;
                ttVolume += volume;
                ttUnits += units;

                ebDemande.setTotalVolume(ttVolume);
                ebDemande.setTotalWeight(ttWeight);
                ebDemande.setTotalNbrParcel(ttUnits);
                ebDemande
                        .setTotalTaxableWeight(
                                Math.max(ebDemande.getTotalWeight(), (ebDemande.getTotalVolume() / 3) * 1000));

                if (ebDemande.getRefTransport() == null) {
                    String refTransport = StringUtils
                            .leftPad(ebDemande.getEbDemandeNum().toString().toUpperCase(), 5, "0");
                    ebDemande.setRefTransport(chargeur.getEbCompagnie().getCode().concat("-").concat(refTransport));
                }

                ebDemandeRepository.save(ebDemande);
                ebMarchandiseRepository.saveAll(listEbMarchandise);

                // get data transporteur
                Boolean finalChoiceExists = null;
                EbUser transporteur = null;
                List<ExEbDemandeTransporteur> listDemandeTransporteur = new ArrayList<ExEbDemandeTransporteur>();
                n = sheetTransporteur.getLastRowNum();
                List<Map<String, Object>> quoteRefs = new ArrayList<Map<String, Object>>();
                String currentQuoteRef = null;

                for (i = 4; i <= n; i++) {
                    currentQuoteRef = null;
                    row = sheetTransporteur.getRow(i);
                    indexCell = 0;

                    if (row != null) cellValue = this.getCellValue(row.getCell(indexCell));

                    if (row == null && listDemandeTransporteur.isEmpty()) throw new IOException(
                            "Aucun transporteur trouvé dans la cellule (" +
                                    rowDemande.getRowNum() + ", " + indexCell + ")");
                    if (cellValue == null ||
                            (cellValue = cellValue.trim()).isEmpty() || !cellValue.contentEquals(identifiant)) continue;

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        currentQuoteRef = cellValue;
                    }

                    ExEbDemandeTransporteur exDemTranspo = new ExEbDemandeTransporteur();
                    exDemTranspo.setxEbDemande(new EbDemande(ebDemande.getEbDemandeNum()));
                    exDemTranspo.setxEbUserCt(new EbUser(connectedUser.getEbUserNum()));

                    transporteur = null;
                    cellValue = this.getCellValue(row.getCell(++indexCell));
                    if (cellValue == null || (cellValue = cellValue.trim()).isEmpty()) throw new IOException(
                            "Email du transporteur non renseigné dans la cellule (" +
                                    rowDemande.getRowNum() + ", " + indexCell + ")");
                    transporteur = ebUserRepository.findOneByEmailIgnoreCase(cellValue);
                    if (transporteur == null ||
                            !transporteur.isTransporteur() && !transporteur.isTransporteurBroker())
                        throw new IOException(
                                "Email du transporteur non disponible dans la plateforme dans la cellule (" +
                                        rowDemande.getRowNum() + ", " + indexCell + ")");
                    exDemTranspo
                            .setxTransporteur(
                                    new EbUser(transporteur.getEbUserNum(), transporteur.getNom(), transporteur.getPrenom()));
                    exDemTranspo
                            .setxEbEtablissement(
                                    new EbEtablissement(transporteur.getEbEtablissement().getEbEtablissementNum()));
                    exDemTranspo.setxEbCompagnie(new EbCompagnie(transporteur.getEbCompagnie().getEbCompagnieNum()));

                    dt = null;
                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {

                        if (Pattern.compile("([0-9]{2}/){2}[0-9]{4}").matcher(cellValue).matches()) {
                            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                            try {
                                dt = formatter.parse(cellValue);
                                exDemTranspo.setPickupTime(dt);
                            } catch (Exception e) {
                            }

                        }

                    }
                    /*
                     * if (dt == null) throw new IOException(
                     * "Date pickup invalide dans la cellule (" +
                     * rowDemande.getRowNum() + ", " +
                     * indexCell + ")");
                     */

                    dt = null;
                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {

                        if (Pattern.compile("([0-9]{2}/){2}[0-9]{4}").matcher(cellValue).matches()) {
                            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                            try {
                                dt = formatter.parse(cellValue);
                                exDemTranspo.setDeliveryTime(dt);
                            } catch (Exception e) {
                            }

                        }

                    }
                    // if(dt == null)
                    // throw new IOException("Date delivery invalide dans la
                    // cellule (" +
                    // rowDemande.getRowNum() + ", " + indexCell + ")");

                    /*
                     * cellValue = this.getCellValue(row.getCell(++indexCell));
                     * if(cellValue != null
                     * && !(cellValue = cellValue.trim()).isEmpty()) {
                     * exDemTranspo.setTransitTime(Double.valueOf(cellValue).
                     * intValue()); }
                     */

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        exDemTranspo.setPrice(new BigDecimal(cellValue));
                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        exDemTranspo.setComment(cellValue);
                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));
                    code = null;

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        code = Enumeration.StatutCarrier.getCodeByLibelle(cellValue);

                        if (code != null) {
                            /*
                             * // gestion des statuts
                             * if(ebDemande.getxEcStatut().equals(Enumeration.
                             * StatutDemande.INP.getCode()))
                             * {
                             * exDemTranspo.setStatus(Enumeration.StatutCarrier.
                             * RRE.getCode()); }else
                             * if(ebDemande.getxEcStatut().equals(Enumeration.
                             * StatutDemande.WRE.getCode())
                             * && code > Enumeration.StatutCarrier.RRE.getCode()
                             * && code < Enumeration.g) {
                             * exDemTranspo.setStatus(Enumeration.StatutCarrier.
                             * REC.getCode()); }
                             */

                            exDemTranspo.setStatus(code);

                            if (code.equals(Enumeration.StatutCarrier.FIN.getCode()) && finalChoiceExists == null) {
                                finalChoiceExists = true;
                            } else if (code.equals(Enumeration.StatutCarrier.FIN.getCode())) {
                                exDemTranspo.setStatus(Enumeration.StatutCarrier.QSE.getCode());
                            }

                        }

                    }

                    if (code == null) exDemTranspo.setStatus(Enumeration.StatutCarrier.RRE.getCode());

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        exDemTranspo.setLeadTimePickup(Double.valueOf(cellValue).intValue());
                    }
                    // else
                    // throw new IOException("Leadtime pickup invalide dans la
                    // cellule (" +
                    // rowDemande.getRowNum() + ", " + indexCell + ")");

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        exDemTranspo.setCityPickup(cellValue);
                    } else exDemTranspo.setCityPickup(ebDemande.getEbPartyOrigin().getCity());

                    cellValue = this.getCellValue(row.getCell(++indexCell));
                    country = null;

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        country = ecCountryRepository.findFirstByCode(cellValue);

                        if (country != null) {
                            exDemTranspo.setxEcCountryPickup(country);
                        }

                    } else exDemTranspo.setxEcCountryPickup(ebDemande.getEbPartyOrigin().getxEcCountry());

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        exDemTranspo.setLeadTimeDeparture(Double.valueOf(cellValue).intValue());
                    }
                    // else
                    // throw new IOException("Leadtime departure invalide dans
                    // la cellule (" +
                    // rowDemande.getRowNum() + ", " + indexCell + ")");

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        exDemTranspo.setCityDeparture(cellValue);
                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));
                    country = null;

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        country = ecCountryRepository.findFirstByCode(cellValue);

                        if (country != null) {
                            exDemTranspo.setxEcCountryDeparture(country);
                        }

                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        exDemTranspo.setLeadTimeArrival(Double.valueOf(cellValue).intValue());
                    }
                    // else
                    // throw new IOException("Leadtime arrival invalide dans la
                    // cellule (" +
                    // rowDemande.getRowNum() + ", " + indexCell + ")");

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        exDemTranspo.setCityArrival(cellValue);
                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));
                    country = null;

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        country = ecCountryRepository.findFirstByCode(cellValue);

                        if (country != null) {
                            exDemTranspo.setxEcCountryArrival(country);
                        }

                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        exDemTranspo.setLeadTimeCustoms(Double.valueOf(cellValue).intValue());
                    }
                    // else
                    // throw new IOException("Leadtime customs invalide dans la
                    // cellule (" +
                    // rowDemande.getRowNum() + ", " + indexCell + ")");

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        exDemTranspo.setCityCustoms(cellValue);
                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));
                    country = null;

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        country = ecCountryRepository.findFirstByCode(cellValue);

                        if (country != null) {
                            exDemTranspo.setxEcCountryCustoms(country);
                        }

                    }

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        exDemTranspo.setLeadTimeDelivery(Double.valueOf(cellValue).intValue());
                    }
                    // else
                    // throw new IOException("Leadtime delivery invalide dans la
                    // cellule (" +
                    // rowDemande.getRowNum() + ", " + indexCell + ")");

                    cellValue = this.getCellValue(row.getCell(++indexCell));

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        exDemTranspo.setCityDelivery(cellValue);
                    } else exDemTranspo.setCityDelivery(ebDemande.getEbPartyDest().getCity());

                    cellValue = this.getCellValue(row.getCell(++indexCell));
                    country = null;

                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                        country = ecCountryRepository.findFirstByCode(cellValue);

                        if (country != null) {
                            exDemTranspo.setxEcCountryDelivery(country);
                        }

                    } else exDemTranspo.setxEcCountryDelivery(ebDemande.getEbPartyDest().getxEcCountry());

                    if (Enumeration.StatutCarrier.FIN.getCode().equals(exDemTranspo.getStatus())) {
                        ebDemande.set_exEbDemandeTransporteurFinal(exDemTranspo);
                        ebDemande.setxEbTransporteur(transporteur.getEbUserNum());
                        ebDemande
                                .setxEbTransporteurEtablissementNum(
                                        transporteur.getEbEtablissement().getEbEtablissementNum());
                        finalChoiceExists = false;
                    }

                    SearchCriteria criteria = new SearchCriteria();
                    criteria.setModule(Enumeration.Module.PRICING.getCode());
                    criteria.setEbCompagnieNum(transporteur.getEbCompagnie().getEbCompagnieNum());
                    List<EbCompagnieCurrency> listCompCurrency = daoCurrency.selectListEbCompagnieCurrency(criteria);
                    if (listCompCurrency != null &&
                            !listCompCurrency.isEmpty()) exDemTranspo.setxEbCompagnieCurrency(listCompCurrency.get(0));
                    // listDemandeTransporteur.add(exDemTranspo);

                    Map<String, Object> map = new HashMap<String, Object>();
                    map.put("exEbDemandeTransporteur", exDemTranspo);
                    map.put("listCompCurrency", listCompCurrency);
                    map.put("quoteRef", currentQuoteRef);
                    quoteRefs.add(map);
                }

                List<EbCostCategorie> listEbCostCategorie = null;
                EcCurrency curEuro = null;

                for (EcCurrency cur : listEcCurrency) {

                    if (cur.getCode().trim().contentEquals("EUR")) {
                        curEuro = cur;
                        break;
                    }

                }

                // get data transporteur
                for (Map<String, Object> mapQr : quoteRefs) {
                    n = sheetPostCout.getLastRowNum();
                    BigDecimal totalPrice = new BigDecimal("0.0");
                    ExEbDemandeTransporteur exEbDemandeTransporteur = (ExEbDemandeTransporteur) mapQr
                            .get("exEbDemandeTransporteur");
                    List<EbCompagnieCurrency> listCompCurrency = (List<EbCompagnieCurrency>) mapQr
                            .get("listCompCurrency");

                    listEbCostCategorie = gson
                            .fromJson(jsonListEbCostCategorie, new TypeToken<ArrayList<EbCostCategorie>>() {
                            }.getType());

                    for (EbCostCategorie costCategorie : listEbCostCategorie) {
                        costCategorie.setPrice(new BigDecimal(0.0));
                        costCategorie.setPriceEuro(new BigDecimal(0.0));
                        if (costCategorie.getDateCreation() == null) costCategorie.setDateCreation(dateCreation);

                        costCategorie.getListEbCost().removeIf(it -> it.getFlagOther() != null);

                        for (EbCost cost : costCategorie.getListEbCost()) {
                            cost.setPrice(new BigDecimal(0.0));
                            cost.setPriceEuro(new BigDecimal(0.0));
                            cost.setxEcCurrencyNum(null);
                            if (cost.getDateCreation() == null) cost.setDateCreation(dateCreation);
                        }

                    }

                    for (i = 1; i <= n; i++) {
                        EbCostCategorie ebCostCategorie = null;
                        EbCost ebCost = null;
                        row = sheetPostCout.getRow(i);
                        indexCell = 0;

                        if (row != null) cellValue = this.getCellValue(row.getCell(indexCell));
                        if (!(cellValue != null &&
                                !(cellValue = cellValue.trim()).isEmpty() &&
                                mapQr.get("quoteRef").toString().contentEquals(cellValue))) continue;

                        cellValue = this.getCellValue(row.getCell(++indexCell));

                        if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                            final String categorieCode = EbCostCategorie.getCategorieCode(cellValue);
                            final String costCode = cellValue;
                            Optional<EbCostCategorie> opt1 = listEbCostCategorie
                                    .stream().filter(it -> it.getCode().contentEquals(categorieCode)).findFirst();
                            ebCostCategorie = opt1.isPresent() ? opt1.get() : null;
                            Optional<EbCost> opt2 = ebCostCategorie
                                    .getListEbCost().stream().filter(it -> it.getCode().contentEquals(costCode))
                                    .findFirst();
                            ebCost = opt2.isPresent() ? opt2.get() : null;

                            if (ebCostCategorie != null &&
                                    (ebCost == null || ebCost.getLibelle().contentEquals("Autre"))) {
                                ebCost = new EbCost();
                                ebCost.setCode(costCode);
                                ebCost.setDateCreation(dateCreation);
                                ebCost.setFlagOther(true);
                                ebCost.setPrice(new BigDecimal("0.0"));
                                ebCost.setPriceEuro(new BigDecimal("0.0"));
                                ebCost.getListCostCategorie().add(ebCostCategorie);
                                ebCostCategorie.getListEbCost().add(ebCost);
                            }

                        }

                        if (ebCostCategorie == null) continue;

                        cellValue = this.getCellValue(row.getCell(++indexCell));

                        if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                            EcCurrency ecCurrency = null;

                            for (EcCurrency cur : listEcCurrency) {

                                if (cur.getCode().toLowerCase().trim().contentEquals(cellValue.toLowerCase())) {
                                    ecCurrency = cur;
                                    break;
                                }

                            }

                            if (ecCurrency == null) ecCurrency = curEuro;

                            if (ecCurrency != null) ebCost.setxEcCurrencyNum(ecCurrency.getEcCurrencyNum());
                        }

                        cellValue = this.getCellValue(row.getCell(++indexCell));

                        if (cellValue != null &&
                                !(cellValue = cellValue.trim()).isEmpty() && ebCost.getFlagOther() != null) {
                            ebCost.setLibelle(cellValue);
                        }

                        cellValue = this.getCellValue(row.getCell(++indexCell));

                        if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                            ebCost.setPrice(new BigDecimal(cellValue));
                            ebCostCategorie.setPrice(ebCostCategorie.getPrice().add(ebCost.getPrice()));
                        }

                        if (ebCost.getxEcCurrencyNum() != null) {

                            for (EbCompagnieCurrency compCur : listCompCurrency) {

                                if (ebCost.getxEcCurrencyNum().equals(compCur.getEcCurrency().getEcCurrencyNum()) &&
                                        compCur
                                                .getXecCurrencyCible().getEcCurrencyNum()
                                                .equals(ebDemande.getXecCurrencyInvoice().getEcCurrencyNum())) {
                                    ebCost
                                            .setPriceEuro(
                                                    ebCost
                                                            .getPrice()
                                                            .multiply(BigDecimal.valueOf(compCur.getEuroExchangeRate())));
                                    break;
                                }

                            }

                            ebCostCategorie.setPriceEuro(ebCostCategorie.getPriceEuro().add(ebCost.getPriceEuro()));
                            totalPrice = totalPrice.add(ebCost.getPriceEuro());
                        }

                    }

                    exEbDemandeTransporteur.setListCostCategorie(listEbCostCategorie);
                    if (totalPrice.compareTo(new BigDecimal("0.0")) > 0) exEbDemandeTransporteur.setPrice(totalPrice);
                    listDemandeTransporteur.add(exEbDemandeTransporteur);
                }

                if (listDemandeTransporteur.size() > 0) {
                    exEbDemandeTransporteurRepository.saveAll(listDemandeTransporteur);
                }

                if (ebDemande.get_exEbDemandeTransporteurFinal() != null) {
                    ebDemande.setListMarchandises(listEbMarchandise);
                    EbInvoice ebInvoice = new EbInvoice(ebDemande);
                    freightAuditService.insertEbInvoice(ebInvoice);

                    // ajout des track & trace
                    listEbTrack = ebTrackService.insertEbTrackTrace(ebDemande);

                    // ajout des evenements et MAJ du statut du tracing
                    if (!listEbTrack.isEmpty() && sheetTracing != null) {
                        n = sheetTracing.getLastRowNum();
                        List<EbTtEvent> listTtEvent = new ArrayList<EbTtEvent>();

                        for (i = 1; i <= n; i++) {
                            row = sheetTracing.getRow(i);
                            indexCell = 0;
                            if (row != null) cellValue = this.getCellValue(row.getCell(indexCell));

                            if (cellValue == null || (cellValue = cellValue.trim()).isEmpty()) continue;

                            for (EbTtTracing tracing : listEbTrack) {

                                if (tracing.getListEbPslApp() != null &&
                                        !tracing.getListEbPslApp().isEmpty() &&
                                        tracing.getxEbMarchandise().getIdUnite().equals(cellValue)) {
                                    cellValue = this.getCellValue(row.getCell(++indexCell));

                                    if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {

                                        for (EbTTPslApp pslApp : tracing.getListEbPslApp()) {

                                            if (cellValue.equals(pslApp.getCodeAlpha())) {
                                                // adding event
                                                EbTtEvent ttEvent = new EbTtEvent();
                                                ttEvent.setDateCreation(dateCreation);
                                                ttEvent.setTypeEvent(TtEnumeration.TypeEvent.EVENT_PSL.getCode());
                                                ttEvent.setxEbTtPslApp(new EbTTPslApp(pslApp.getEbTtPslAppNum()));
                                                ttEvent.setxEbTtTracing(new EbTtTracing(tracing.getEbTtTracingNum()));

                                                cellValue = this.getCellValue(row.getCell(++indexCell));

                                                if (cellValue != null &&
                                                        !(cellValue = cellValue.trim()).isEmpty() &&
                                                        cellValue.toLowerCase().contentEquals("deviation")) {
                                                    ttEvent
                                                            .setTypeEvent(
                                                                    TtEnumeration.TypeEvent.EVENT_DEVIATION.getCode());
                                                }

                                                cellValue = this.getCellValue(row.getCell(++indexCell));

                                                if (cellValue != null &&
                                                        !(cellValue = cellValue.trim()).isEmpty() &&
                                                        ttEvent
                                                                .getTypeEvent()
                                                                .equals(TtEnumeration.TypeEvent.EVENT_DEVIATION.getCode())) {
                                                    TtEnumeration.TtDeviationCategorie devCategory = TtEnumeration.TtDeviationCategorie
                                                            .getDeviationCategorieByLibelle(cellValue);
                                                    EbTtCategorieDeviation ttDevCategory = ebTtCategorieDeviationRepository
                                                            .getFirstByCode(devCategory.getCode() + "");
                                                    if (ttDevCategory != null) ttEvent.setCategorie(ttDevCategory);
                                                }

                                                if (cellValue != null &&
                                                        ttEvent
                                                                .getTypeEvent()
                                                                .equals(TtEnumeration.TypeEvent.EVENT_DEVIATION.getCode()) &&
                                                        ttEvent.getCategorie() == null) throw new IOException(
                                                        "Categorie de la déviation est vide ou incohérente dans la cellule (" +
                                                                rowDemande.getRowNum() + ", " + indexCell + ")");

                                                cellValue = this.getCellValue(row.getCell(++indexCell));

                                                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {

                                                    if (Pattern
                                                            .compile("([0-9]{2}/){2}[0-9]{4}").matcher(cellValue)
                                                            .matches()) {
                                                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                                                        try {
                                                            dt = formatter.parse(cellValue);
                                                            ttEvent.setDateEvent(dt);
                                                            if (ttEvent
                                                                    .getTypeEvent().equals(
                                                                            TtEnumeration.TypeEvent.EVENT_PSL
                                                                                    .getCode()))
                                                                pslApp.setDateActuelle(dt);
                                                        } catch (Exception e) {
                                                        }

                                                    }

                                                }

                                                cellValue = this.getCellValue(row.getCell(++indexCell));

                                                if (cellValue != null && !(cellValue = cellValue.trim()).isEmpty()) {
                                                    ttEvent.setCommentaire(cellValue);
                                                    if (ttEvent
                                                            .getTypeEvent().equals(
                                                                    TtEnumeration.TypeEvent.EVENT_PSL
                                                                            .getCode()))
                                                        pslApp.setCommentaire(cellValue);
                                                }

                                                if (ttEvent
                                                        .getTypeEvent()
                                                        .equals(TtEnumeration.TypeEvent.EVENT_PSL.getCode())) {
                                                    cellValue = this.getCellValue(row.getCell(++indexCell));

                                                    if (cellValue != null &&
                                                            !(cellValue = cellValue.trim()).isEmpty()) {
                                                        country = ecCountryRepository.findFirstByCode(cellValue);

                                                        if (country != null) {
                                                            pslApp.setxEcCountry(country);
                                                        }

                                                    }

                                                    cellValue = this.getCellValue(row.getCell(++indexCell));

                                                    if (cellValue != null &&
                                                            !(cellValue = cellValue.trim()).isEmpty()) {
                                                        pslApp.setCity(cellValue);
                                                    }

                                                }

                                                listTtEvent.add(ttEvent);
                                                break;
                                            }

                                        }

                                    }

                                    break;
                                }

                            }

                        }

                        if (!listTtEvent.isEmpty()) ebTtEventRepository.saveAll(listTtEvent);

                        if (listEbTrack != null && !listEbTrack.isEmpty()) {
                            List<EbTTPslApp> listPsl = new ArrayList<EbTTPslApp>();
                            for (EbTtTracing tracing : listEbTrack)
                                if (tracing.getListEbPslApp()
                                        != null) listPsl.addAll(tracing.getListEbPslApp());
                            ebPslAppRepository.saveAll(listPsl);
                        }

                    }

                }

            }

        } catch (

                IOException e) {
            isCompleted = false;
            Map<String, String> map = new HashMap<String, String>();

            if (identifiant == null || identifiant.trim().isEmpty()) {
                map.put("ident", "-");
                map.put("error", "Indentifiant vide");
            } else {
                map.put("ident", identifiant);
                if (!e.getMessage().trim().isEmpty()) map.put("error", e.getMessage());
                else map.put("error", "Erreur inconnu dans la demande avec cet identifiant");
            }

            errors.add(map);

            e.printStackTrace();
        } catch (DataIntegrityViolationException e) {

            if (e.getRootCause() instanceof SQLException) {
                DataException dex = (DataException) e.getCause();

                isCompleted = false;
                Map<String, String> map = new HashMap<String, String>();

                if (identifiant != null && !identifiant.trim().isEmpty()) {
                    map.put("ident", identifiant);
                    map.put("error", "Erreur DB - " + dex.getSQLException().getMessage());
                }

                errors.add(map);

                e.printStackTrace();
            }

        } catch (Exception e) {
            isCompleted = false;
            Map<String, String> map = new HashMap<String, String>();

            if (identifiant != null && !identifiant.trim().isEmpty()) {
                map.put("ident", identifiant);
                map.put("error", "Erreur à la ligne " + e.getStackTrace()[0].getLineNumber() + " du code.");
            }

            errors.add(map);

            e.printStackTrace();
        }

        progressBulkImport.get(ident).put("finished", (Integer) progressBulkImport.get(ident).get("finished") + 1);

        return isCompleted;
    }

    @Override
    public HashMap<String, Object> getEbAdresseByGlobalsFieldsAsEbParty(
            String term,
            Integer ebCompagnieNum,
            Integer ebEtablissementNum,
            String eligibilities,
            Integer ebUserNum,
            Integer ecCountryNum,
            String city,
            String zipCode,
            Integer ebZoneNum,
            Integer size,
            Integer page) {
        HashMap<String, Object> result = new HashMap<String, Object>();
        List<EbParty> parties = new ArrayList<>();

        if (ebUserNum != null) {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setSearchterm(term);
            criteria.setEbCompagnieNum(ebCompagnieNum);
            criteria.setEbEtablissementNum(ebEtablissementNum);
            criteria.setEcCountryNum(ecCountryNum);
            criteria.setCity(city);
            criteria.setZipCode(zipCode);
            criteria.setEbZoneNum(ebZoneNum);

            if (size == null) {
                size = DEFAULT_SIZE;
            }

            if (page == null) {
                page = DEFAULT_PAGE;
            }

            criteria.setSize(size);
            criteria.setPageNumber(page);

            List<EbAdresse> adresses = daoAdresse.getListEbAdresse(criteria);
            Long total = daoAdresse.getListAddressesCount(criteria);

            if (eligibilities != null) {
                criteria.setAdresseEligibilities(Arrays.asList(eligibilities.split(",")));
            }

            criteria.setEbUserNum(ebUserNum);
            EbEtablissement userEtab = null;
            EbUser connectedUser = this.daoUser.findUser(criteria);

            if (connectedUser != null) userEtab = connectedUser.getEbEtablissement();

            for (EbAdresse addr : adresses)
                parties
                        .add(
                                EbParty
                                        .fromEbAdresse(addr, new EbEtablissement(userEtab.getEbEtablissementNum(), userEtab.getNom())));

            result.put("count", total);
            result.put("data", parties);
        }

        return result;
    }

    @Override
    public List<EbAdresse> getEbAdresseByCompany(Integer ebCompagnieNum) {
        SearchCriteria criteria = new SearchCriteria();
        criteria.setEbCompagnieNum(ebCompagnieNum);

        List<EbAdresse> adresses = daoAdresse.getListEbAdresse(criteria);

        return adresses;
    }

    @Override
    public List<EbAdresse> getEbAdresseByCriteria(SearchCriteria criteria) {
        return daoAdresse.getListEbAdresse(criteria);
    }

    @Override
    public List<EbAdresse> getAllEbAdresse() {
        return ebAdresseRepository.findAll();
    }

    @Override
    public List<EbParty> getEbPartyByCompany(String company) {
        return ebPartyRepository.findDistinctAllByCompanyStartingWith(company);
    }

    @Override
    public List<EbParty> getEbPartyByCity() {
        return ebPartyRepository.selectDistinctAllCity();
    }

    @Override
    public List<EbParty> getAllEbParty() {
        return ebPartyRepository.findAll();
    }

    @Override
    public List<EbDemande> getAllEbDemande(SearchCriteriaPricingBooking criteria) {
        // TODO Auto-generated method stub
        return ebDemandeRepository.findAllByOrderByEbDemandeNumAsc();
    }

    @Override
    public List<EbDemande> getListEbDemande(SearchCriteriaPricingBooking criteria) throws Exception {
        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);
        List<EbDemande> listDemande = daoPricing.getListEbDemande(criteria, connectedUser);
        List<Integer> listEbDemandeNum = listDemande
                .stream().map(it -> it.getEbDemandeNum()).collect(Collectors.toList());

        if (connectedUser != null &&
                (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker()) &&
                Enumeration.Module.PRICING.getCode().equals(criteria.getModule()) && !listEbDemandeNum.isEmpty()) {
            List<ExEbDemandeTransporteur> listDemTransporteur = exEbDemandeTransporteurRepository
                    .findExEbDemandeTransporteurForStatusQuote(
                            connectedUser.getEbEtablissement().getEbEtablissementNum(),
                            Enumeration.StatutCarrier.QSE.getCode(),
                            listEbDemandeNum);

            if (!listDemTransporteur.isEmpty()) {

                for (EbDemande dem : listDemande) {
                    boolean found = false;
                    Integer currentStatus = null;

                    for (ExEbDemandeTransporteur demTranspo : listDemTransporteur) {

                        if (demTranspo.getxEbDemande().getEbDemandeNum().equals(dem.getEbDemandeNum()) &&
                                demTranspo.getxTransporteur().getEbUserNum().equals(connectedUser.getEbUserNum())) {

                            if (dem.getStatusQuote() == null ||
                                    currentStatus == null ||
                                    currentStatus != null && currentStatus < demTranspo.getStatus()) {
                                found = true;
                                if (Enumeration.StatutCarrier.QSE
                                        .getCode()
                                        .equals(demTranspo.getStatus()))
                                    dem.setStatusQuote(StatutDemande.REC.getCode());
                                else if (Enumeration.StatutCarrier.FIN.getCode().equals(demTranspo.getStatus())) dem
                                        .setStatusQuote(Enumeration.StatutDemande.FIN.getCode());
                                else if (Enumeration.StatutCarrier.FIN_PLAN
                                        .getCode().equals(demTranspo.getStatus())) dem
                                        .setStatusQuote(Enumeration.StatutDemande.FIN.getCode());
                                else if (Enumeration.StatutCarrier.TRANSPORT_PLAN
                                        .getCode().equals(demTranspo.getStatus())) dem
                                        .setStatusQuote(Enumeration.StatutDemande.INP.getCode());

                                currentStatus = demTranspo.getStatus();
                            } else if (demTranspo.getStatus().equals(Enumeration.StatutCarrier.FIN.getCode())) {
                                dem.setStatusQuote(Enumeration.StatutDemande.FIN.getCode());
                                break;
                            }

                        }

                    }

                    if (!found && dem.getxEcStatut() >= Enumeration.StatutDemande.WPU.getCode()) dem
                            .setStatusQuote(Enumeration.StatutDemande.NAW.getCode());
                    else if (!found &&
                            dem.getxEcCancelled() != null) dem.setStatusQuote(Enumeration.StatutDemande.CAN.getCode());
                    else if (!found) dem.setStatusQuote(Enumeration.StatutDemande.INP.getCode());



                    String statutStr = getStatusStr(connectedUser, dem);
                    dem.setStatutStr(statutStr);
                }

            } else {

                for (EbDemande dem : listDemande) {
                    dem.setStatusQuote(Enumeration.StatutDemande.INP.getCode());
                }

            }

        } else if (Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(criteria.getModule()) &&
                !listEbDemandeNum.isEmpty()) {
            List<ExEbDemandeTransporteur> listTranspoFinal = exEbDemandeTransporteurRepository
                    .findByStatusQuote(Enumeration.StatutCarrier.FIN.getCode(), listEbDemandeNum);

            for (EbDemande d : listDemande) {
                ExEbDemandeTransporteur tr = listTranspoFinal
                        .stream().filter(it -> it.getxEbDemande().getEbDemandeNum().equals(d.getEbDemandeNum()))
                        .findFirst().orElse(null);

                if (tr != null) {
                    ExEbDemandeTransporteur demTranspo = new ExEbDemandeTransporteur();
                    demTranspo
                            .setxEbEtablissement(
                                    new EbEtablissement(
                                            tr.getxEbEtablissement().getEbEtablissementNum(),
                                            tr.getxEbEtablissement().getNom()));
                    demTranspo
                            .setxEbCompagnie(
                                    new EbCompagnie(
                                            tr.getxEbCompagnie().getEbCompagnieNum(),
                                            tr.getxEbCompagnie().getNom(),
                                            tr.getxEbCompagnie().getCode()));
                    demTranspo.setExEbDemandeTransporteurNum(tr.getExEbDemandeTransporteurNum());
                    demTranspo.setPickupTime(tr.getPickupTime());
                    demTranspo.setDeliveryTime(tr.getDeliveryTime());
                    demTranspo.setPrice(tr.getPrice());
                    demTranspo
                            .setxTransporteur(
                                    new EbUser(
                                            tr.getxTransporteur().getEbUserNum(),
                                            tr.getxTransporteur().getNom(),
                                            tr.getxTransporteur().getPrenom(),
                                            tr.getxTransporteur().getEmail(),
                                            null,
                                            null));
                    d.setExEbDemandeTransporteurs(new ArrayList<ExEbDemandeTransporteur>());
                    d.setValuationType(tr.getStatus());
                    d.getExEbDemandeTransporteurs().add(demTranspo);
                }

            }

        }

        listDemande.forEach(dem -> {
            String listFlagIcon = dem.getListFlag() != null ? dem.getListFlag().trim() : null;

            if (listFlagIcon != null && !listFlagIcon.isEmpty()) {
                dem.setListEbFlagDTO(EbFlagDTO.sortByName(flagService.getListFlagDTOFromCodes(listFlagIcon)));
            }

        });

        for (EbDemande dem : listDemande) {

            if (dem.getXecCurrencyInvoice() != null && dem.getXecCurrencyInvoice().getEcCurrencyNum() != null) {
                Optional<EcCurrency> ecCurrency = Optional.empty();
                ecCurrency = ecCurrencyRepository.findById(dem.getXecCurrencyInvoice().getEcCurrencyNum());

                if (ecCurrency.isPresent()) {
                    dem.getXecCurrencyInvoice().setCode(ecCurrency.get().getCode());
                }

                DemandeStatusUtils.getStatusLibelleFromEbDemande(dem, criteria.getModule(), connectedUser);
            }

        }

        return listDemande;
    }

    @Override
    public int getCountListEbDemande(SearchCriteriaPricingBooking criteria) throws Exception {
        // TODO Auto-generated method stub

        long count = daoPricing.getCountListEbDemande(criteria);

        return (int) count;
    }

    @SuppressWarnings("unchecked")
    @Override
    public EbDemande getEbDemande(SearchCriteriaPricingBooking criteria, EbUser userConnected) throws Exception {
			EbDemande ebDemande = daoPricing.getEbDemande(criteria, userConnected);

        if (ebDemande != null) {
            PlanTransportWSO transport = planTransportApiService.convertEbDemandeToTransport(ebDemande);
            transport = planTransportApiService.getNextPickUpDateOfTransportWSO(transport);
            if (transport.getNextPickUpDate()
                    != null) ebDemande.setDateOfGoodsAvailability(transport.getNextPickUpDate());
            Map<String, Object> result = loadListDemandeQuote(criteria, ebDemande, userConnected);
            List<ExEbDemandeTransporteur> demandeTransporteurs = (ArrayList<ExEbDemandeTransporteur>) result
                    .get("exEbDemandeTransporteurs");
            ebDemande.setExEbDemandeTransporteurs(demandeTransporteurs);
            ebDemande.setListDemandeQuote((ArrayList<ExEbDemandeTransporteur>) result.get("listDemandeQuote"));

            ExEbDemandeTransporteur demandeTransporteur = demandeTransporteurs
                    .stream()
                    .filter(
                            demandeTransport -> demandeTransport.getStatus().equals(CarrierStatus.FINAL_CHOICE.getCode()) ||
                                    demandeTransport.getStatus().equals(CarrierStatus.FIN_PLAN.getCode()))
                    .findFirst().orElse(null);
            ebDemande.setSelectedCarrier(demandeTransporteur);

            String listFlagIcon = ebDemande.getListFlag() != null ? ebDemande.getListFlag().trim() : null;
            ebDemande
                .setListEbFlagDTO(
                    listFlagIcon != null && !listFlagIcon.isEmpty() ?
                        EbFlagDTO.sortByName(flagService.getListFlagDTOFromCodes(listFlagIcon)) :
                        null);

            if (criteria.isWithListMarchandise()) {
                List<EbMarchandise> listMarchandises = unitService.getListMarchandises(ebDemande.getEbDemandeNum());

                ebDemande.setListMarchandises(listMarchandises);
            }

            if (NatureDemandeTransport.CONSOLIDATION.getCode().equals(ebDemande.getxEcNature())) {
                EbQrGroupe ebQrGroupe = ebQrGroupePropositionRepository
                        .getEbQrGroupeWithPropositionByEbDemandeResult(ebDemande.getEbDemandeNum());

                if (ebQrGroupe != null) {
                    List<EbDemande> listDemandeInitials = qrConsolidationService
											.getListDemandeInProposition(
												ebQrGroupe.getListPropositions().get(0),
												userConnected
											);

                    List<TransportReferenceInfosDTO> listTransportReferenceInfos = new ArrayList<>();

                    listDemandeInitials.forEach(dem -> {
                        listTransportReferenceInfos
                                .add(new TransportReferenceInfosDTO(dem.getEbDemandeNum(), dem.getRefTransport()));
                        if (dem.getListMarchandises() != null) dem.getListMarchandises().forEach(m -> {
                            m.setEbDemande(null);
                        });
                    });

                    ebDemande.setConsolidationListTransportReferenceInfos(listTransportReferenceInfos);

                    ebQrGroupe.getListPropositions().get(0).setListEbDemande(listDemandeInitials);
                    ebDemande.setEbQrGroupe(ebQrGroupe);
                    ebDemande.setInitialTrInGroupedTransportRequest(listDemandeInitials);
                }

            }

            String statutStr = getStatusStr(userConnected, ebDemande);
            ebDemande.setStatutStr(statutStr);

        } else {
            ebDemande = new EbDemande();
            ebDemande.setExEbDemandeTransporteurs(new ArrayList<>());
            ebDemande.setListDemandeQuote(new ArrayList<>());
        }

        getObservers(ebDemande);

        return ebDemande;
    }

    private String getStatusStr(EbUser userConnected, EbDemande ebDemande) {
        boolean isStatusConfirmedAndTransporteurNotNull = ebDemande.getxEcStatut()
            >= Enumeration.StatutDemande.WPU.getCode() &&
            ebDemande.getxEbTransporteur() != null;

        if (isStatusConfirmedAndTransporteurNotNull) {
            EbUser transporteurFinal = ebUserRepository.getOneByEbUserNum(ebDemande.getxEbTransporteur());

            if (transporteurFinal != null) {
                ebDemande
                    .setExEbDemandeTransporteurFinal(
                        new EbUser(
                            transporteurFinal.getEbUserNum(),
                            transporteurFinal.getEbEtablissement().getEbEtablissementNum(),
                            transporteurFinal.getEbCompagnie().getEbCompagnieNum()));
            }

        }

        String statutStr = DemandeStatusUtils
            .getStatusLibelleFromEbDemande(ebDemande, Enumeration.Module.PRICING.getCode(), userConnected);
        return statutStr;
    }

    private void getObservers(EbDemande ebDemande) {
		    Set<DemandeUserObservers> observers = demandeObserversRepository.findByDemandeEbDemandeNum(ebDemande.getEbDemandeNum());

		    for (DemandeUserObservers obs : observers) {
		        EbUser user = new EbUser();
		        user.setEbUserNum(obs.getObserver().getEbUserNum());
		        user.setEmail(obs.getObserverEmail());
		        ebDemande.getxEbUserObserver().add(user);
            }
    }

    @Override
    public void
    setListDemandeQuote(SearchCriteriaPricingBooking criteria, List<EbDemande> listEbDemande, boolean onlyOneTr) {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);

        if (listEbDemande != null && listEbDemande.size() > 0) {
            List<Integer> listEbDemandeNum = listEbDemande
                    .stream().map(EbDemande::getEbDemandeNum).collect(Collectors.toList());

            if (connectedUser.getRole().equals(Enumeration.Role.ROLE_PRESTATAIRE.getCode()) ||
                    connectedUser.getRole().equals(Enumeration.Role.ROLE_UNKNOWN.getCode())) {
                SearchCriteriaPricingBooking transportCriteria = new SearchCriteriaPricingBooking();
                transportCriteria.setEmailUnknownUser(criteria.getEmailUnknownUser());
                transportCriteria.setForCotation(true);
                transportCriteria.setListEbDemandeNum(listEbDemandeNum);
                transportCriteria.setEbEtablissementNum(connectedUser.getEbEtablissement().getEbEtablissementNum());
                // transport plan
                transportCriteria.setGrouping(true);
                transportCriteria.setEcRoleNum(Enumeration.Role.ROLE_PRESTATAIRE.getCode());
                List<ExEbDemandeTransporteur> listDemandeTrans = daoPricing.getListExEbTransporteur(transportCriteria);
                if (listDemandeTrans == null) listDemandeTrans = new ArrayList<ExEbDemandeTransporteur>();

                for (EbDemande ebDemande : listEbDemande) {
                    List<ExEbDemandeTransporteur> newListTrans = new ArrayList<ExEbDemandeTransporteur>();

                    if (listDemandeTrans.size() > 0 &&
                            connectedUser != null &&
                            (connectedUser.isTransporteur() ||
                                    connectedUser.isTransporteurBroker() || connectedUser.isPrestataire()) &&
                            Enumeration.Module.PRICING.getCode().equals(criteria.getModule())) {
                        boolean found = false;

                        for (ExEbDemandeTransporteur demTranspo : listDemandeTrans) {
                            if (demTranspo.getxEbDemande() == null ||
                                    !demTranspo
                                            .getxEbDemande().getEbDemandeNum().equals(ebDemande.getEbDemandeNum()))
                                continue;

                            if ((newListTrans.isEmpty() && demTranspo.getxTransporteur().isPrestataire()) ||
                                    !onlyOneTr) {
                                newListTrans.add(demTranspo);
                            }

                            if (connectedUser.getEbUserNum().equals(demTranspo.getxTransporteur().getEbUserNum())) {
                                found = true;

                                if (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.WPU.getCode()) {

                                    if (connectedUser
                                            .getEbEtablissement().getEbEtablissementNum()
                                            .equals(ebDemande.getxEbTransporteurEtablissementNum())) {
                                        ebDemande.setStatusQuote(ebDemande.getxEcStatut());
                                    } else if (connectedUser.isSuperAdmin() &&
                                            connectedUser
                                                    .getEbCompagnie().getEbCompagnieNum().equals(
                                                    ebDemande
                                                            .get_exEbDemandeTransporteurFinal().getxEbCompagnie()
                                                            .getEbCompagnieNum())) {
                                        ebDemande.setStatusQuote(ebDemande.getxEcStatut());
                                    } else {
                                        ebDemande.setStatusQuote(Enumeration.StatutDemande.NAW.getCode());
                                    }

                                } else {
                                    if (demTranspo.getStatus() == null ||
                                            Enumeration.StatutCarrier.RRE
                                                    .getCode().equals(demTranspo.getStatus())) ebDemande
                                            .setStatusQuote(Enumeration.StatutDemande.INP.getCode());
                                }

                            }

                        }

                        if (!found) {

                            if (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.WPU.getCode()) {

                                if (connectedUser
                                        .getEbEtablissement().getEbEtablissementNum()
                                        .equals(ebDemande.getxEbTransporteurEtablissementNum())) {
                                    ebDemande.setStatusQuote(ebDemande.getxEcStatut());
                                } else if (connectedUser.isSuperAdmin() &&
                                        connectedUser
                                                .getEbCompagnie().getEbCompagnieNum().equals(
                                                ebDemande
                                                        .get_exEbDemandeTransporteurFinal().getxEbCompagnie()
                                                        .getEbCompagnieNum())) {
                                    ebDemande.setStatusQuote(ebDemande.getxEcStatut());
                                } else {
                                    ebDemande.setStatusQuote(Enumeration.StatutDemande.NAW.getCode());
                                }

                            } else {
                                ebDemande.setStatusQuote(Enumeration.StatutDemande.INP.getCode());
                            }

                        }

                    }

                    ebDemande.setListDemandeQuote(newListTrans);
                    ebDemande.setExEbDemandeTransporteurs(newListTrans);
                }

            } else if (connectedUser.getRole().equals(Enumeration.Role.ROLE_CONTROL_TOWER.getCode())) {
                // - afficher la liste des transporteurs choisis par le
                // chargeur
                SearchCriteriaPricingBooking transportCriteria = new SearchCriteriaPricingBooking();

                if (criteria.getModule() != null &&
                        (Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(criteria.getModule()) ||
                                Enumeration.Module.PRICING.getCode().equals(criteria.getModule()))) {
                    transportCriteria.setStatus(Enumeration.StatutCarrier.FIN.getCode());
                }

                transportCriteria.setListEbDemandeNum(listEbDemandeNum);
                transportCriteria.setForCotation(true);
                transportCriteria.setGrouping(true);
                List<ExEbDemandeTransporteur> listDemandeTrans = daoPricing.getListExEbTransporteur(transportCriteria);
                if (listDemandeTrans == null) listDemandeTrans = new ArrayList<ExEbDemandeTransporteur>();

                for (EbDemande ebDemande : listEbDemande) {
                    List<ExEbDemandeTransporteur> newListTrans = new ArrayList<ExEbDemandeTransporteur>();

                    for (ExEbDemandeTransporteur demTranspo : listDemandeTrans) {
                        if (demTranspo.getxEbDemande() == null ||
                                !demTranspo.getxEbDemande().getEbDemandeNum().equals(ebDemande.getEbDemandeNum()))
                            continue;

                        if ((newListTrans.isEmpty() && demTranspo.getxTransporteur().isPrestataire()) || !onlyOneTr) {
                            newListTrans.add(demTranspo);
                        }

                    }

                    ebDemande.setExEbDemandeTransporteurs(newListTrans);
                }

                transportCriteria = new SearchCriteriaPricingBooking();
                transportCriteria.setForCotation(true);
                transportCriteria.setEbEtablissementNum(connectedUser.getEbEtablissement().getEbEtablissementNum());
                transportCriteria.setListEbDemandeNum(listEbDemandeNum);
                // - afficher la cotation du transporteur
                listDemandeTrans = daoPricing.getListExEbTransporteur(transportCriteria);

                if (listDemandeTrans == null) listDemandeTrans = new ArrayList<ExEbDemandeTransporteur>();

                for (EbDemande ebDemande : listEbDemande) {
                    List<ExEbDemandeTransporteur> newListTrans = new ArrayList<ExEbDemandeTransporteur>();

                    for (ExEbDemandeTransporteur demTranspo : listDemandeTrans) {
                        if (demTranspo.getxEbDemande() == null ||
                                !demTranspo.getxEbDemande().getEbDemandeNum().equals(ebDemande.getEbDemandeNum()))
                            continue;

                        if ((newListTrans.isEmpty() && demTranspo.getxTransporteur().isPrestataire()) || !onlyOneTr) {
                            newListTrans.add(demTranspo);
                        }

                    }

                    ebDemande.setListDemandeQuote(newListTrans);
                }

            } else if (connectedUser.getRole().equals(Enumeration.Role.ROLE_CHARGEUR.getCode())) {
                SearchCriteriaPricingBooking transportCriteria = new SearchCriteriaPricingBooking();

                if (criteria.getModule() != null &&
                        (Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(criteria.getModule()) ||
                                Enumeration.Module.PRICING.getCode().equals(criteria.getModule()))) {
                    transportCriteria.setStatus(Enumeration.StatutCarrier.FIN.getCode());
                }

                transportCriteria.setListEbDemandeNum(listEbDemandeNum);
                transportCriteria.setForCotation(true);
                transportCriteria.setGrouping(true);

                List<ExEbDemandeTransporteur> listDemandeTrans = daoPricing.getListExEbTransporteur(transportCriteria);

                for (EbDemande ebDemande : listEbDemande) {
                    List<ExEbDemandeTransporteur> newListTrans = new ArrayList<ExEbDemandeTransporteur>();

                    for (ExEbDemandeTransporteur demTranspo : listDemandeTrans) {
                        if (demTranspo.getxEbDemande() == null ||
                                !demTranspo.getxEbDemande().getEbDemandeNum().equals(ebDemande.getEbDemandeNum()))
                            continue;

                        if ((newListTrans.isEmpty() && demTranspo.getxTransporteur().isPrestataire()) || !onlyOneTr) {
                            newListTrans.add(demTranspo);
                        }

                    }

                    ebDemande.setExEbDemandeTransporteurs(newListTrans);
                    if (newListTrans.size() > 0) ebDemande.setSelectedCarrier(newListTrans.get(0));
                }

            }

        }

        // mettre null les demandes dans les quotes pour ne plus avoir de
        // probleme
        // d'identifiants répétés dans le json
        if (listEbDemande != null) {

            for (EbDemande d : listEbDemande) {

                if (d.getExEbDemandeTransporteurs() != null) {

                    for (ExEbDemandeTransporteur q : d.getExEbDemandeTransporteurs()) {
                        q.setxEbDemande(null);
                    }

                }

            }

        }

    }

    private Map<String, Object>
    loadListDemandeQuote(SearchCriteriaPricingBooking criteria, EbDemande ebDemande, EbUser user) {
        EbUser connectedUser = user != null ? user : connectedUserService.getAvailableCurrentUser(criteria);
        Map<String, Object> result = new HashMap<String, Object>();

        /*
         * Integer status; if (ebDemande.getxEcStatut() <
         * Enumeration.StatutDemande.WPU.getCode()) status =
         * Enumeration.StatutCarrier.QSE.getCode(); else status =
         * Enumeration.StatutCarrier.FIN.getCode();
         */
        if (ebDemande != null) {

            if (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.WPU.getCode() &&
                    ebDemande.getxEbTransporteur() != null) {
                EbUser transporteurFinal = ebUserRepository.getOneByEbUserNum(ebDemande.getxEbTransporteur());

                if (transporteurFinal != null) {
                    ebDemande
                            .setExEbDemandeTransporteurFinal(
                                    new EbUser(
                                            transporteurFinal.getEbUserNum(),
                                            transporteurFinal.getEbEtablissement().getEbEtablissementNum(),
                                            transporteurFinal.getEbCompagnie().getEbCompagnieNum()));
                }

            }

            if (connectedUser.getRole().equals(Enumeration.Role.ROLE_PRESTATAIRE.getCode()) ||
                    connectedUser.getRole().equals(Enumeration.Role.ROLE_UNKNOWN.getCode())) {
                SearchCriteriaPricingBooking transportCriteria = new SearchCriteriaPricingBooking();
                transportCriteria.setEmailUnknownUser(criteria.getEmailUnknownUser());
                transportCriteria.setForCotation(true);
                transportCriteria.setEbDemandeNum(ebDemande.getEbDemandeNum());

                if (connectedUser.isSuperAdmin()) {
                    transportCriteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
                } else {
                    transportCriteria.setEbEtablissementNum(connectedUser.getEbEtablissement().getEbEtablissementNum());
                }

                // transport plan
                transportCriteria.setEcRoleNum(Enumeration.Role.ROLE_PRESTATAIRE.getCode());

                List<ExEbDemandeTransporteur> listDemandeTrans = daoPricing.getListExEbTransporteur(transportCriteria);
                if (listDemandeTrans == null) listDemandeTrans = new ArrayList<ExEbDemandeTransporteur>();
                ExEbDemandeTransporteur finalQuote = null;

                for (ExEbDemandeTransporteur dt : listDemandeTrans) {

                    if (Enumeration.StatutCarrier.FIN.getCode().equals(dt.getStatus()) ||
                            Enumeration.StatutCarrier.FIN_PLAN.getCode().equals(dt.getStatus())) {
                        finalQuote = dt;
                        break;
                    }

                }

                if (listDemandeTrans.size() > 0 &&
                        connectedUser != null && (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker()) &&
                        Enumeration.Module.PRICING.getCode().equals(criteria.getModule())) {
                    boolean found = false;

                    for (ExEbDemandeTransporteur demTranspo : listDemandeTrans) {

                        if (connectedUser.getEbUserNum().equals(demTranspo.getxTransporteur().getEbUserNum())) {
                            found = true;

                            if (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.WPU.getCode()) {

                                if (connectedUser
                                        .getEbEtablissement().getEbEtablissementNum()
                                        .equals(ebDemande.getxEbTransporteurEtablissementNum())) {
                                    ebDemande.setStatusQuote(ebDemande.getxEcStatut());
                                } else {

                                    if (finalQuote != null &&
                                            connectedUser.isSuperAdmin() &&
                                            connectedUser
                                                    .getEbCompagnie().getEbCompagnieNum()
                                                    .equals(finalQuote.getxEbCompagnie().getEbCompagnieNum())) {
                                        ebDemande.setStatusQuote(ebDemande.getxEcStatut());
                                    } else {
                                        ebDemande.setStatusQuote(Enumeration.StatutDemande.NAW.getCode());
                                    }

                                }

                            } else {
                                if (demTranspo.getStatus() == null ||
                                        Enumeration.StatutCarrier.RRE.getCode().equals(demTranspo.getStatus()))
                                    ebDemande
                                            .setStatusQuote(Enumeration.StatutDemande.INP.getCode());
                                else ebDemande.setStatusQuote(ebDemande.getxEcStatut());
                            }

                        }

                    }

                    if (!found) {

                        if (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.WPU.getCode()) {

                            if (connectedUser
                                    .getEbEtablissement().getEbEtablissementNum()
                                    .equals(ebDemande.getxEbTransporteurEtablissementNum())) {
                                ebDemande.setStatusQuote(ebDemande.getxEcStatut());
                            } else if (finalQuote != null &&
                                    connectedUser.isSuperAdmin() &&
                                    connectedUser
                                            .getEbCompagnie().getEbCompagnieNum()
                                            .equals(finalQuote.getxEbCompagnie().getEbCompagnieNum())) {
                                ebDemande.setStatusQuote(ebDemande.getxEcStatut());
                            } else {
                                ebDemande.setStatusQuote(Enumeration.StatutDemande.NAW.getCode());
                            }

                        } else {
                            ebDemande.setStatusQuote(Enumeration.StatutDemande.INP.getCode());
                        }

                    }

                }

                ebDemande.setExEbDemandeTransporteurs(listDemandeTrans);
            } else if (connectedUser.getRole().equals(Enumeration.Role.ROLE_CONTROL_TOWER.getCode())) {
                // - afficher la liste des transporteurs choisis par le
                // chargeur
                SearchCriteriaPricingBooking transportCriteria = new SearchCriteriaPricingBooking();

                if (criteria.getModule() != null &&
                        Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(criteria.getModule()) &&
                        (ebDemande.getModuleCreator() == null ||
                                !Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(ebDemande.getModuleCreator()))) {
                    transportCriteria.setStatus(Enumeration.StatutCarrier.FIN.getCode());
                }

                transportCriteria.setEbDemandeNum(ebDemande.getEbDemandeNum());
                transportCriteria.setForCotation(true);
                List<ExEbDemandeTransporteur> listDemandeTrans = daoPricing.getListExEbTransporteur(transportCriteria);
                if (listDemandeTrans == null) listDemandeTrans = new ArrayList<ExEbDemandeTransporteur>();
                ebDemande.setExEbDemandeTransporteurs(listDemandeTrans);

                transportCriteria = new SearchCriteriaPricingBooking();
                transportCriteria.setForCotation(true);
                transportCriteria.setEbEtablissementNum(connectedUser.getEbEtablissement().getEbEtablissementNum());
                transportCriteria.setEbDemandeNum(ebDemande.getEbDemandeNum());
                // - afficher la cotation du transporteur
                listDemandeTrans = daoPricing.getListExEbTransporteur(transportCriteria);

                if (listDemandeTrans == null) listDemandeTrans = new ArrayList<ExEbDemandeTransporteur>();
                ebDemande.setListDemandeQuote(listDemandeTrans);
            } else if (connectedUser.getRole().equals(Enumeration.Role.ROLE_CHARGEUR.getCode())) {
                SearchCriteriaPricingBooking transportCriteria = new SearchCriteriaPricingBooking();

                if (criteria.getModule() != null &&
                        Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(criteria.getModule()) &&
                        (ebDemande.getModuleCreator() == null ||
                                !Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(ebDemande.getModuleCreator()) ||
                                Enumeration.Module.PRICING.getCode().equals(criteria.getModule()))) {
                    transportCriteria.setStatus(Enumeration.StatutCarrier.FIN.getCode());
                }

                transportCriteria.setEbDemandeNum(ebDemande.getEbDemandeNum());
                transportCriteria.setForCotation(true);

                List<ExEbDemandeTransporteur> listDemandeTrans = daoPricing.getListExEbTransporteur(transportCriteria);
                if (listDemandeTrans == null) listDemandeTrans = new ArrayList<ExEbDemandeTransporteur>();
                ebDemande.setExEbDemandeTransporteurs(listDemandeTrans);
            }

        } else {
            ebDemande = new EbDemande();
            ebDemande.setExEbDemandeTransporteurs(new ArrayList<ExEbDemandeTransporteur>());
            ebDemande.setListDemandeQuote(new ArrayList<ExEbDemandeTransporteur>());
        }

        result.put("exEbDemandeTransporteurs", ebDemande.getExEbDemandeTransporteurs());
        result.put("listDemandeQuote", ebDemande.getListDemandeQuote());

        return result;
    }

    @Override
    public Map<String, Object> getListDemandeQuote(SearchCriteriaPricingBooking criteria) throws Exception {
        EbDemande ebDemande = daoPricing.getEbDemande(criteria, null);
        Map<String, Object> result = new HashMap<String, Object>();

        result = this.loadListDemandeQuote(criteria, ebDemande, null);

        return result;
    }

	@Override
	public Map<String, Object> addQuotation(ExEbDemandeTransporteur transporteur, Integer module,
			Boolean isQuotationRequest) throws InterruptedException, ExecutionException {
		Map<String, Object> result = addQuoteAndHandleGroupedTr(transporteur, isQuotationRequest);

		EbDemande transportRequest = (EbDemande) result.get("transportRequest");

		ExecutorService service = Executors.newCachedThreadPool();
		service.submit(() -> {
			try {
				kafkaObjectService.buildAndSendTransportRequestToKafka(transportRequest.getEbDemandeNum(),
						transportRequest.getUser(), false);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// Envoi d'email
			pricingAnnexMethodsService.sendEmailsAndAlertsForQuotation(transporteur, module, isQuotationRequest,
					transportRequest);
		});

		return result;
	}

    @Transactional
    public Map<String, Object>
        addQuoteAndHandleGroupedTr(ExEbDemandeTransporteur transporteur, Boolean isQuotationRequest) {
        Map<String, Object> result = addQuotationNormal(transporteur, isQuotationRequest);

        EbDemande transportRequest = (EbDemande) result.get("transportRequest");

        final boolean isConsolidation = NatureDemandeTransport.CONSOLIDATION
            .getCode().equals(transportRequest.getxEcNature());

        pricingAnnexMethodsService.cancelInitialTrs(transportRequest.getEbDemandeNum(), isConsolidation);

        this.controlRuleService.processDocumentRules(transportRequest.getEbDemandeNum(), true, null);

        return result;
    }

		@Transactional
    public Map<String, Object> addQuotationNormal(
            ExEbDemandeTransporteur exEbDemandeTransporteur,
            Boolean isQuotationRequest) {

        boolean toUpdate = false;
        Map<String, Object> result = new HashMap<String, Object>();
				EbDemande ebDemande = null;
				EbUser connectedUser = connectedUserService.getCurrentUser();
        
        // partie metier
        try {
            if (exEbDemandeTransporteur != null) {
							ebDemande = ebDemandeRepository
								.findOneByEbDemandeNum(exEbDemandeTransporteur.getxEbDemande().getEbDemandeNum());

                List<EbPlCostItem> listPlCostItem = exEbDemandeTransporteur.getListPlCostItem();
                if (listPlCostItem != null && !listPlCostItem.isEmpty()) for (EbPlCostItem costItem : listPlCostItem) {

                    if (costItem.getCurrency() == null) {

                        if (costItem.getCode() != null) {
                            EcCurrency currency = ecCurrencyRepository.findByCode(costItem.getCode());
                            costItem.setCurrency(currency);
                        }

                    }

                }

							toUpdate = handleQuoteAndTrStatus(exEbDemandeTransporteur, ebDemande);

                if (connectedUser.isControlTower()) {

                    if (exEbDemandeTransporteur.getCtCommunity() != null && exEbDemandeTransporteur.getCtCommunity()) {
                        EbRelation ebRelation = new EbRelation();
                        ebRelation.setEmail(exEbDemandeTransporteur.getxTransporteur().getEmail());
                        ebRelation.setTypeRelation(Enumeration.ServiceType.SERVICE_TRANSPORTEUR.getCode());
                        ebRelation.setHost(new EbUser(ebDemande.getUser().getEbUserNum()));

                        userService
                                .sendInvitationByMail(
                                        ebRelation,
                                        Enumeration.ServiceType.SERVICE_BROKER.getCode(),
                                        ebDemande.getUser().getEbUserNum());
                    }
                }

                if (exEbDemandeTransporteur.getxEbEtablissement() != null &&
                        exEbDemandeTransporteur.getxEbEtablissement().getEbEtablissementNum() != null) {
                    exEbDemandeTransporteur
                            .setxEbEtablissement(
                                    new EbEtablissement(exEbDemandeTransporteur.getxEbEtablissement().getEbEtablissementNum()));
                }

                if (exEbDemandeTransporteur.getxEbCompagnie() != null &&
                        exEbDemandeTransporteur.getxEbCompagnie().getEbCompagnieNum() != null) {
                    exEbDemandeTransporteur
                            .setxEbCompagnie(
                                    new EbCompagnie(exEbDemandeTransporteur.getxEbCompagnie().getEbCompagnieNum()));
                }

                exEbDemandeTransporteur.setxEbDemande(new EbDemande(ebDemande.getEbDemandeNum()));

                if (exEbDemandeTransporteur.getxTransporteur() != null &&
                        exEbDemandeTransporteur.getxTransporteur().getEbUserNum() != null) {
                    exEbDemandeTransporteur
                            .setxTransporteur(new EbUser(exEbDemandeTransporteur.getxTransporteur().getEbUserNum()));
                }

                if (exEbDemandeTransporteur.getxEbUserCt() != null &&
                        exEbDemandeTransporteur.getxEbUserCt().getEbUserNum()
                                == null) exEbDemandeTransporteur.setxEbUserCt(null);

                if (exEbDemandeTransporteur.getxEbCompagnieCurrency() != null &&
                        exEbDemandeTransporteur.getxEbCompagnieCurrency().getEbCompagnieCurrencyNum()
                                == null) exEbDemandeTransporteur.setxEbCompagnieCurrency(null);

                if (exEbDemandeTransporteur.getxEcCountryArrival() != null &&
                        exEbDemandeTransporteur.getxEcCountryArrival().getEcCountryNum() == null) {
                    exEbDemandeTransporteur.setxEcCountryArrival(null);
                }

                if (exEbDemandeTransporteur.getxEcCountryDelivery() != null &&
                        exEbDemandeTransporteur.getxEcCountryDelivery().getEcCountryNum() == null) {
                    exEbDemandeTransporteur.setxEcCountryDelivery(null);
                }

                if (exEbDemandeTransporteur.getxEcCountryCustoms() != null &&
                        exEbDemandeTransporteur.getxEcCountryCustoms().getEcCountryNum() == null) {
                    exEbDemandeTransporteur.setxEcCountryCustoms(null);
                }

                if (exEbDemandeTransporteur.getxEcCountryDeparture() != null &&
                        exEbDemandeTransporteur.getxEcCountryDeparture().getEcCountryNum() == null) {
                    exEbDemandeTransporteur.setxEcCountryDeparture(null);
                }

                if (exEbDemandeTransporteur.getxEcCountryPickup() != null &&
                        exEbDemandeTransporteur.getxEcCountryPickup().getEcCountryNum() == null) {
                    exEbDemandeTransporteur.setxEcCountryPickup(null);
                }

                if (toUpdate) {
                    ebDemande.setUpdateDate(new Date());
                    ebDemandeRepository.updateStatusEbDemande(ebDemande.getEbDemandeNum(), ebDemande.getxEcStatut());
                }
								exEbDemandeTransporteur.getxEbDemande().setxEcStatut(ebDemande.getxEcStatut());
								exEbDemandeTransporteur.getxEbDemande().setxEcNature(ebDemande.getxEcNature());
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

				result
					.put(
						"exEbDemandeTransporteur",
						exEbDemandeTransporteurRepository.save(exEbDemandeTransporteur)
					);
				result.put("transportRequest", ebDemande);
				result.put("ebChat", handleHistory(ebDemande, connectedUser));

        return result;
    }

		private EbChat handleHistory(EbDemande ebDemande, EbUser connectedUser)
		{
			if (!Statiques.UNKNOWN_USER_NUM.equals(connectedUser.getEbUserNum()))
			{
				String lang = connectedUser != null && connectedUser.getLanguage() != null ?
					connectedUser.getLanguage() :
					"en";
				Locale lcl = new Locale(lang);

				String username = connectedUser.getRealUserNum() != null ?
					connectedUser.getRealUserNomPrenom() :
					connectedUser.getNomPrenom();
				String usernameFor = connectedUser.getRealUserNum() != null ?
					connectedUser.getNomPrenom() :
					null;
				String action = messageSource
					.getMessage("tracing.message_rd.quotation_modified", null, lcl);
				Integer module = Enumeration.Module.PRICING.getCode();
				Integer chatCompId = Enumeration.IDChatComponent.PRICING.getCode();
				Integer idFiche = ebDemande.getEbDemandeNum();
				return listStatiqueService
					.historizeAction(idFiche, chatCompId, module, action, username, usernameFor);
			}
			return null;
		}

		private boolean handleQuoteAndTrStatus(
			ExEbDemandeTransporteur exEbDemandeTransporteur,
			EbDemande ebDemande
		)
		{
			boolean toUpdate = false;

			if (exEbDemandeTransporteur.getStatus() == null)
			{
				exEbDemandeTransporteur.setStatus(Enumeration.StatutCarrier.RRE.getCode());
			}

			if (ebDemande.getxEcStatut() < Enumeration.StatutDemande.FIN.getCode())
			{

				if (
					ebDemande.isFlagRecommendation() != null &&
						ebDemande.isFlagRecommendation() &&
						ebDemande.getxEcStatut() < Enumeration.StatutDemande.WRE.getCode()
				)
				{
					ebDemande.setxEcStatut(Enumeration.StatutDemande.WRE.getCode());
					toUpdate = true;
				}
				else if (
					exEbDemandeTransporteur.getPrice() != null &&
						ebDemande.getxEcStatut() < Enumeration.StatutDemande.REC.getCode()
				)
				{
					ebDemande.setxEcStatut(Enumeration.StatutDemande.REC.getCode());
					toUpdate = true;
					ebDemande.setWaitingForConf(new Date());
				}

				if (exEbDemandeTransporteur.getPrice() != null)
				{
					exEbDemandeTransporteur.setStatus(Enumeration.StatutCarrier.QSE.getCode());
				}
				else if (Enumeration.StatutDemande.PND.getCode().equals(ebDemande.getxEcStatut()))
				{
					ebDemande.setxEcStatut(Enumeration.StatutDemande.REC.getCode());
					toUpdate = true;
					ebDemande.setWaitingForConf(new Date());
				}
			}

			return toUpdate;
		}

		@Transactional
    EbDemande confirmQuotationPricing(ExEbDemandeTransporteur demandeTransporteur, EbDemande ebDemande) {
        Integer result = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();

        // partie metier
        try {

            if (demandeTransporteur.getStatus().equals(Enumeration.StatutCarrier.REC.getCode()) &&
                    ebDemande.isFlagRecommendation() ||
                    demandeTransporteur.getStatus().equals(Enumeration.StatutCarrier.QSE.getCode()) &&
                            !ebDemande.isFlagRecommendation()) {
                demandeTransporteur.getxEbDemande().setUpdateDate(new Date());
                ebDemandeRepository
                        .updateStatusEbDemande(
                                demandeTransporteur.getxEbDemande().getEbDemandeNum(),
                                Enumeration.StatutDemande.REC.getCode());
                ebDemande.setxEcStatut(Enumeration.StatutDemande.REC.getCode());
                ebDemande.setWaitingForConf(new Date());
            } else if (demandeTransporteur.getStatus().equals(Enumeration.StatutCarrier.QSE.getCode())) {
                demandeTransporteur.getxEbDemande().setUpdateDate(new Date());
                ebDemandeRepository
                        .updateStatusEbDemande(
                                demandeTransporteur.getxEbDemande().getEbDemandeNum(),
                                Enumeration.StatutDemande.WRE.getCode());
                ebDemande.setxEcStatut(Enumeration.StatutDemande.WRE.getCode());
            }

            result = exEbDemandeTransporteurRepository
                    .updateStatus(demandeTransporteur.getExEbDemandeTransporteurNum(), demandeTransporteur.getStatus());

            if (demandeTransporteur.getStatus().equals(Enumeration.StatutCarrier.FIN.getCode()) ||
                    demandeTransporteur.getStatus().equals(Enumeration.StatutCarrier.FIN_PLAN.getCode())) {
                EbUser transporteur = ebUserRepository
                        .getOneByEbUserNum(demandeTransporteur.getxTransporteur().getEbUserNum());

                if (demandeTransporteur.getStatus().equals(Enumeration.StatutCarrier.FIN.getCode())) {
                    ebDemandeRepository
                            .updateStatusAndDetailsTransporteurFinal(
                                    demandeTransporteur.getxEbDemande().getEbDemandeNum(),
                                    Enumeration.StatutDemande.WPU.getCode(),
                                    transporteur.getNom(),
                                    transporteur.getEbUserNum(),
                                    transporteur.getEbEtablissement().getEbEtablissementNum(),
                                    false);

                    LOGGER.info("---------------- TR : {} IS CONFIRMED AND SELECTED CARRIER IS : {} ----------------------", demandeTransporteur.getxEbDemande().getEbDemandeNum(), transporteur.getNom());
                    ebDemande.setxEcStatut(Enumeration.StatutDemande.WPU.getCode());
                    ebDemande.setConfirmed(new Date());
                    ebDemande.setNomTransporteur(transporteur.getNom());
                    ebDemande.setxEbTransporteur(transporteur.getEbUserNum());
                    ebDemande
                            .setxEbTransporteurEtablissementNum(transporteur.getEbEtablissement().getEbEtablissementNum());
                    ebDemande.setPrixTransporteurFinal(demandeTransporteur.getPrice());
                    exEbDemandeTransporteurRepository.save(demandeTransporteur);
                } else {
                    ebDemandeRepository
                            .updateStatusAndDetailsTransporteurFinal(
                                    demandeTransporteur.getxEbDemande().getEbDemandeNum(),
                                    Enumeration.StatutDemande.WPU.getCode(),
                                    transporteur.getNom(),
                                    transporteur.getEbUserNum(),
                                    transporteur.getEbEtablissement().getEbEtablissementNum(),
                                    true);
                    LOGGER.info("---------------- TR : {} IS CONFIRMED AND SELECTED CARRIER IS : {} WITH TRANSPORT PLAN----------------------", demandeTransporteur.getxEbDemande().getEbDemandeNum(), transporteur.getNom());

                    ebDemande.setxEcStatut(Enumeration.StatutDemande.WPU.getCode());
                    ebDemande.setConfirmed(new Date());
                }

                // EbEtablissement etabTransporteur =
                // ebEtablissementRepository.getOne(demandeTransporteur.getxEbEtablissementNum());
                // ebDemandeRepository.updateStatusAndNomTransporteurAndXEbTransporteurEbDemande(demandeTransporteur.getNumEntity(),
                // Enumeration.StatutDemande.WPU.getCode(),
                // etabTransporteur.getNom(),
                // etabTransporteur.getEbEtablissementNum());
                String lang = connectedUser != null && connectedUser.getLanguage() != null ?
                        connectedUser.getLanguage() :
                        "en";
                Locale lcl = new Locale(lang);
                // Historisation
                listStatiqueService
                        .historizeAction(
                                ebDemande.getEbDemandeNum(),
                                Enumeration.IDChatComponent.PRICING.getCode(),
                                Enumeration.Module.PRICING.getCode(),
                                messageSource.getMessage("tracing.message_rd.carrier_chosen", null, lcl),
                                connectedUser.getRealUserNum() != null ?
                                        connectedUser.getRealUserNomPrenom() :
                                        connectedUser.getNomPrenom(),
                                connectedUser.isControlTower() || connectedUser.getRealUserNum() != null ?
                                        ebDemande.getUser().getNomPrenom() :
                                        null);

                EbQrGroupeProposition ebQrGroupeProposition = ebQrGroupePropositionRepository
                        .getOneByEbDemandeResult_ebDemandeNum(ebDemande.getEbDemandeNum());

                if (ebQrGroupeProposition != null) {
                    ebQrGroupeProposition.setStatut(GroupePropositionStatut.CONFIRMED.getCode());
                    ebQrGroupePropositionRepository.save(ebQrGroupeProposition);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        if (result != null && result > 0) return ebDemande;

        return null;
    }

    private void sendEmailsForPricingConfirmation(ExEbDemandeTransporteur demandeTransporteur, EbDemande ebDemande) {
        // envoie d'emails
        EbUser connectedUser = connectedUserService.getCurrentUser();

        try {
            SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();
            criteria.setForEmail(true);
            criteria.setEbDemandeNum(ebDemande.getEbDemandeNum());
            criteria.setStatus(Enumeration.StatutCarrier.FIN.getCode());
            List<ExEbDemandeTransporteur> listExEbDemandeTransporteur = daoPricing.getListExEbTransporteur(criteria);

            if (listExEbDemandeTransporteur.size() > 0) demandeTransporteur = listExEbDemandeTransporteur.get(0);

            if (demandeTransporteur.getStatus().equals(Enumeration.StatutCarrier.REC.getCode()) &&
                    ebDemande.isFlagRecommendation() != null && ebDemande.isFlagRecommendation()) {
                // TODO Commentaire SOungalo : je ne sais pas pourquoi cette
                // ligne est commenté.
                // Quelqu'un
                // peut m'expliquer ?
                // emailServicePricingBooking.sendEmailQuotationRecommendation(ebDemande,
                // listExEbDemandeTransporteur, emailsCT);
            } else if (demandeTransporteur.getStatus().equals(Enumeration.StatutCarrier.FIN.getCode())) {
                // Envoi d'email selected ff
                List<String> listEmailForwarderSelected = emailServicePricingBooking
                        .sendEmailQuotationForwarderSelected(connectedUser, ebDemande, demandeTransporteur);

                emailServicePricingBooking.sendEmailQuotationForwarderNotSelected(ebDemande, demandeTransporteur);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    @Override
    public Integer
    updateStatusQuotation(ExEbDemandeTransporteur demandeTransporteur, Boolean sendMail, Integer module) {
        LOGGER.info("---------------- CONFIRM BEGIN FOR TR WITH ID : {} ----------------------", demandeTransporteur.getxEbDemande().getEbDemandeNum());
        EbDemande ebDemande = ebDemandeRepository.getOne(demandeTransporteur.getxEbDemande().getEbDemandeNum());
        ebDemande.setValuationType(demandeTransporteur.getStatus());
         Integer actualTrStatut = ebDemande.getxEcStatut();
        // ajout de la demande de quote dans le cas ou le status est confirmé
        // par plan
        // de transport
        if (demandeTransporteur != null && demandeTransporteur.getExEbDemandeTransporteurNum() == null) {

            if (demandeTransporteur.getStatus() == StatutCarrier.FIN_PLAN.getCode()) {
                EbUser connecteduser = connectedUserService.getCurrentUser();
                demandeTransporteur
                        .setxEbCompagnie(new EbCompagnie(connecteduser.getEbCompagnie().getEbCompagnieNum()));
                demandeTransporteur
                        .setxEbEtablissement(
                                new EbEtablissement(connecteduser.getEbEtablissement().getEbEtablissementNum()));

                if (demandeTransporteur.getxTransporteur().getSelected() != null &&
                        demandeTransporteur.getxTransporteur().getSelected()) {
                    ebDemande.setPrixTransporteurFinal(demandeTransporteur.getPrice());
                }

                ebDemande.setPrixTransporteurFinal(demandeTransporteur.getPrice());
                ebDemandeRepository.save(ebDemande);
                exEbDemandeTransporteurRepository.save(demandeTransporteur);
            } else {
                return 0;
            }

        }

        Integer xEcStatus = ebDemande.getxEcStatut();


        ebDemande = confirmQuotationPricing(demandeTransporteur, ebDemande);

        if (ebDemande == null) {
            return 0;
        }

        if (ebDemande.getxEcStatut() != null && !ebDemande.getxEcStatut().equals(xEcStatus)) { // status has been
            // updated -> check
            // doc rules
            this.controlRuleService.processDocumentRules(ebDemande.getEbDemandeNum(), true, null);
        }

        ebDemande.set_exEbDemandeTransporteurFinal(demandeTransporteur);

        if (ebDemande.get_exEbDemandeTransporteurFinal() != null) {

            try {
                // Génération des objets associés

                // Track&Trace
                List<EbTtTracing> ebTtTracings = ebTrackService.insertEbTrackTrace(ebDemande);

                // Receipt Scheduling
                if (NatureDemandeTransport.TRANSPORT_PRINCIPAL.getCode().equals(ebDemande.getxEcNature())) {
                    receiptSchedulingService.insertRschUnitSchedule(ebDemande, ebTtTracings);
                }

                // Compta matière
                comptaMatiereService.generateProcedures(ebDemande, ebTtTracings);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {

                if (sendMail) {
                    sendEmailsForPricingConfirmation(demandeTransporteur, ebDemande);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                freightAuditService.preInsertInvoice(ebDemande);
            } catch (Exception e) {
                e.printStackTrace();
            }

            // publication des messages kafka

            //
            if( actualTrStatut != null && actualTrStatut < Enumeration.StatutDemande.WPU.getCode()){
                try {
                    // Publier un message Booking sur un Topic Transport Request
                    kafkaObjectService.sendTransportRequestToKafkaTopic(ebDemande.getEbDemandeNum(), null, false, null);
                } catch (Exception e) {

                    logger.error("Error sending transport request kafka message", e);
                }
            }


            // TR confirmé statut delivery passe à Packed
            if (ebDemande.getEbDemandeNum() != null) {
                List<Long> listDeliveryNumFromConsolidate = ebLivraisonRepository
                        .findByEbDemandeNum(ebDemande.getEbDemandeNum());
                if (listDeliveryNumFromConsolidate != null) try {

                    for (Long ebLivraisonNum : listDeliveryNumFromConsolidate) {
                        deliveryService.updateStatusDelivery(ebLivraisonNum, StatusDelivery.SHIPPED);
                    }

                } catch (MyTowerException e) {
                    e.printStackTrace();
                }
            }

        }

        EbQrGroupeProposition propo = ebQrGroupePropositionRepository
                .getOneByEbDemandeResult_ebDemandeNum(ebDemande.getEbDemandeNum());

        if (propo != null && propo.getListDemandeNum() != null) {
            List<Integer> listDemandeNum = Arrays
                    .asList(propo.getListDemandeNum().split(":")).stream().filter(it -> it != null && !it.isEmpty())
                    .map(it -> {
                        it.replace(":", "");
                        return Integer.valueOf(it);
                    }).collect(Collectors.toList());

            if (listDemandeNum != null && !listDemandeNum.isEmpty()) {
                ebDemandeRepository
                        .updateCancelStatusAndGroupDemandeNumListEbDemande(
                                listDemandeNum,
                                CancelStatus.CANECLLED.getCode(),
                                ebDemande.getEbDemandeNum());

                if (ebDemande.getxEcStatutGroupage() != null) {
                    List<Integer> EbDemandeNum = Collections.singletonList(ebDemande.getEbDemandeNum());
                    this
                            .updatexEcStatutGroupageInEbDemandeByEbDemandeNum(
                                    EbDemandeNum,
                                    ConsolidationGroupage.PCO.getCode());
                }

            }

        }

        return 1;
    }

    @Override
    public Boolean generateAssociatedPricingObjects(Integer ebDemandeNum) {
        EbDemande ebDemande = ebDemandeRepository.findOneByEbDemandeNum(ebDemandeNum);

        if (ebDemande != null) {

            try {
                // Génération des objets associés
                // Track&Trace
                ebDemande.setFlagEbtrackTrace(false);
                List<EbTtTracing> ebTtTracings = ebTrackService.insertEbTrackTrace(ebDemande);

                if (NatureDemandeTransport.TRANSPORT_PRINCIPAL.getCode().equals(ebDemande.getxEcNature())) {
                    receiptSchedulingService.insertRschUnitSchedule(ebDemande, ebTtTracings);
                }

                // Compta matière
                comptaMatiereService.generateProcedures(ebDemande, ebTtTracings);
                freightAuditService.preInsertInvoice(ebDemande);
                return true;
            } catch (Exception e) {
                return false;
            }

        }

        return false;
    }

    @Override
    public Map<String, Object> deleteDocument(EbDemandeFichiersJoint doc) {
        EbUser connecteduser = connectedUserService.getCurrentUser();
        EbChat ebChat = null;
        boolean isDecremante = true;

        ebDemandeFichiersJointRepositorty.deleteById(doc.getEbDemandeFichierJointNum());

        updateListTypeDoc(doc.getxEbDemande(), null, doc.getIdCategorie(), isDecremante);

		EbDemande ebDemande = ebDemandeRepository.findById(doc.getxEbDemande())
				.orElseThrow(() -> new IllegalArgumentException(
						String.format("EbDemande with id %s not found", doc.getxEbDemande())));

		try {
			emailDocumentService.sendEmailDeleteDocument(connecteduser, null, ebDemande, doc.getNumEntity(),
					doc.getModule());
		} catch (Exception e) {
			e.printStackTrace();
		}

		try {
			EbTypeDocuments typeDoc = ebTypeDocumentsRepository.findFirstByCodeAndXEbCompagnie(doc.getIdCategorie(),
					doc.getxEbCompagnie());
			String action = typeDoc != null ? typeDoc.getNom() + " deleted" : "File deleted";

			Integer idFiche = doc.getNumEntity();

			if (doc.getModule() != null) {
				ebChat = listStatiqueService.historizeAction(idFiche,
						Enumeration.IDChatComponent.getCodeByLibelle(
								Enumeration.Module.getLibelleByCode(doc.getModule())),
						doc.getModule(), action, connecteduser.getConnectedUsername(),
						connecteduser.getDelegatedUsername(), connecteduser);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return Collections.singletonMap("ebChat", ebChat);
    }

    @Transactional
    @Override
    public EbChat cancelQuotation(Integer ebDemandeNum) {
        EbChat ebChat = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();

        try {
            ebDemandeRepository.updateCancelStatusEbDemande(ebDemandeNum, Enumeration.CancelStatus.CANECLLED.getCode());

            EbDemande d = ebDemandeRepository.findById(ebDemandeNum).orElse(null);

            if (d != null) {
                String lang = connectedUser != null && connectedUser.getLanguage() != null ?
                        connectedUser.getLanguage() :
                        "en";
                Locale lcl = new Locale(lang);
                removeDeliveryLink(d.getEbDemandeNum());

                String username = connectedUser.getRealUserNum() != null ?
                        connectedUser.getRealUserNomPrenom() :
                        connectedUser.getNomPrenom();
                String usernameFor = connectedUser.getEbUserNum() != null ? d.getUser().getNomPrenom() : null;
                String action = messageSource.getMessage("tracing.message_rd.pricing_cancelled", null, lcl);

                if (!connectedUser.isControlTower()) {
                    username = connectedUser.getEbUserNum() != null ? d.getUser().getNomPrenom() : null;
                    usernameFor = null;
                }

                ebChat = listStatiqueService
                        .historizeAction(
                                ebDemandeNum,
                                Enumeration.IDChatComponent.PRICING.getCode(),
                                Enumeration.Module.PRICING.getCode(),
                                action,
                                username,
                                usernameFor);
                if (d.getxEcStatut() > StatutDemande.FIN.getCode()) listStatiqueService
                        .historizeAction(
                                d.getEbDemandeNum(),
                                Enumeration.IDChatComponent.TRANSPORT_MANAGEMENT.getCode(),
                                Enumeration.Module.TRANSPORT_MANAGEMENT.getCode(),
                                "TM cancelled",
                                connectedUser.getRealUserNum() != null ?
                                        connectedUser.getRealUserNomPrenom() :
                                        connectedUser.getNomPrenom(),
                                connectedUser.getRealUserNum() != null ? connectedUser.getNomPrenom() : null);

                // update status delivery when tr is canceld before on_going tr
                // status
                List<EbLivraison> ListDeliveryFromConsolidate = ebLivraisonRepository
                        .findByEbDemande(d.getEbDemandeNum());

                if (ListDeliveryFromConsolidate != null && d.getxEcStatut() < StatutDemande.TO.getCode()) {

                    try {

                        for (EbLivraison ebLivraison : ListDeliveryFromConsolidate) {
                            ebLivraison = deliveryService
                                    .updateStatusDelivery(ebLivraison.getEbDelLivraisonNum(), StatusDelivery.CONFIRMED);
                            ebLivraison.setxEbDemande(null);
                            ebLivraisonRepository.save(ebLivraison);
                        }

                    } catch (MyTowerException e) {
                        e.printStackTrace();
                    }

                }

                emailServicePricingBooking.sendEmailPricingCancelled(d);
            }

        } catch (MyTowerException ex) {
            LOGGER.error("MyTower error during cancel", ex);
            throw ex;
        } catch (Exception ex) {
            LOGGER.error("Error during cancel", ex);
            throw ex;
        }

        return ebChat;
    }

    public void removeDeliveryLink(Integer ebDemandeNum) {
        List<EbLivraison> associatedLivraisons = ebLivraisonRepository.findByEbDemande(ebDemandeNum);

        for (EbLivraison livraison : associatedLivraisons) {
            livraison.setRefTransport(null);
            livraison.setxEbDemande(null);
            livraison.setStatusDelivery(StatusDelivery.CONFIRMED);
            ebLivraisonRepository.save(livraison);

            List<EbLivraisonLine> livraisonLines = ebLivraisonLineRepository
                    .findByxEbDelLivraison_EbDelLivraisonNum(livraison.getEbDelLivraisonNum());

            for (EbLivraisonLine line : livraisonLines) {
                line.setEbMarchandise(null);
                ebLivraisonLineRepository.save(line);
            }

            deliveryService.calculateParentOrder(livraison.getEbDelLivraisonNum(), true);
        }

    }

    @Transactional
    @Override
    public Integer confirmPickup(EbDemande ebDemande) {
        ebDemande.setUpdateDate(new Date());
        ebDemandeRepository.updateStatusEbDemande(ebDemande.getEbDemandeNum(), ebDemande.getxEcStatut());
        // ebDemandeRepository.updatePickupDateAndStatus(ebDemande.getEbDemandeNum(),
        // ebDemande.getDatePickup(), ebDemande.getxEcStatut());

        // ebTrackTraceRepository.updatePickupDateAndDateModification(ebDemande.getEbDemandeNum(),
        // ebDemande.getDatePickup(), new Date());
        this.controlRuleService.processDocumentRules(ebDemande.getEbDemandeNum(), true, null);
        return 1;
    }

    @Transactional
    @Override
    public Map<String, Object> updateEbDemandeTransportInfos(EbDemande ebDemande, Boolean sendMail, String action) {
        Map<String, Object> result = new HashMap<String, Object>();
        Boolean flag = true;
        Integer xEcStatut = null;
        String attrs = "NumAwbBol|FlightVessel|DatePickupTM|Etd|FinalDelivery|Eta|CustomsOffice|Mawb|carrierUniqRefNum";
        EbUser user = connectedUserService.getCurrentUser();

        try {

            // verifier s'il y a un champs non renseigné
            for (Method meth : EbDemande.class.getMethods()) {

                if (meth.getName().contains("get") &&
                        meth.getName().toLowerCase().matches(".+" + attrs.toLowerCase())) {

                    if (meth.invoke(ebDemande) == null ||
                            meth.invoke(ebDemande) instanceof String &&
                                    meth.invoke(ebDemande).toString().trim().length() == 0) {
                        flag = false;
                        break;
                    }

                }

            }

            ebDemande.setUpdateDate(new Date());

            ebDemandeRepository
                    .updateEbDemandeTransportInfos(
                            ebDemande.getNumAwbBol(),
                            ebDemande.getFlightVessel(),
                            ebDemande.getxEbSchemaPsl(),
                            ebDemande.getCustomsOffice(),
                            ebDemande.getMawb(),
                            flag,
                            ebDemande.getxEbUserTransportInfoMaj(),
                            ebDemande.getCarrierUniqRefNum(),
                            ebDemande.getCarrierComment(),
                            ebDemande.getEbDemandeNum());

            Integer ebInvoiceNum = ebInvoiceRepository.findEbInvoiceNumByEbdemandeNum(ebDemande.getEbDemandeNum());

            if (ebInvoiceNum != null) {
                ebInvoiceRepository.updateNumAwbBol(ebDemande.getNumAwbBol(), ebInvoiceNum);
            }

            xEcStatut = this.updateTTPsl(ebDemande, sendMail);

            List<EbTTPslApp> listPsl = null;

            for (EbTtCompanyPsl psl : ebDemande.getxEbSchemaPsl().getListPsl()) {

                if (psl.getDateAttendue() != null && psl.getDateAttendue()) {

                    if ((psl.getStartPsl() != null && psl.getStartPsl()) ||
                            (psl.getEndPsl() != null && psl.getEndPsl())) {

                        if (listPsl == null) {
                            listPsl = ebPslAppRepository.findAllByEbDemandeNum(ebDemande.getEbDemandeNum());
                        }

                        if (psl.getStartPsl() != null && psl.getStartPsl()) {
                            Boolean updateDatePickup = true;
                            List<EbTTPslApp> listStartPsl = listPsl
                                    .stream().filter(p -> p.getCodeAlpha().equalsIgnoreCase(psl.getCodeAlpha()))
                                    .collect(Collectors.toList());

                            if (listStartPsl != null && !listStartPsl.isEmpty()) {

                                for (EbTTPslApp p : listStartPsl) {

                                    if (p.getDateActuelle() != null) {
                                        updateDatePickup = false;
                                        break;
                                    }

                                }

                            }

                            if (updateDatePickup) {
                                ebDemandeRepository
                                        .updateDatePickup(ebDemande.getEbDemandeNum(), psl.getExpectedDate());
                            }

                        }

                        if (psl.getEndPsl() != null && psl.getEndPsl()) {
                            Boolean updateDateDelivery = true;
                            List<EbTTPslApp> listEndPsl = listPsl
                                    .stream().filter(p -> p.getCodeAlpha().equalsIgnoreCase(psl.getCodeAlpha()))
                                    .collect(Collectors.toList());

                            if (listEndPsl != null && !listEndPsl.isEmpty()) {

                                for (EbTTPslApp p : listEndPsl) {

                                    if (p.getDateActuelle() != null) {
                                        updateDateDelivery = false;
                                        break;
                                    }

                                }

                            }

                            if (updateDateDelivery) {
                                ebDemandeRepository
                                        .updateDateDelivery(ebDemande.getEbDemandeNum(), psl.getExpectedDate());
                            }

                        }

                    }

                }

            }

            String username = user.getRealUserNum() != null ? user.getRealUserNomPrenom() : user.getNomPrenom();
            String usernameFor = user.getRealUserNum() != null ? user.getNomPrenom() : null;
            Integer module = Enumeration.Module.TRANSPORT_MANAGEMENT.getCode();
            Integer chatCompId = Enumeration.IDChatComponent.TRANSPORT_MANAGEMENT.getCode();
            Integer idFiche = ebDemande.getEbDemandeNum();
            EbChat ebChat = listStatiqueService
                    .historizeAction(idFiche, chatCompId, module, action, username, usernameFor);

            result.put("ebChat", ebChat);
            result.put("xEcStatut", xEcStatut);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private Integer updateTTPsl(EbDemande ebDemande, Boolean sendMail) {
        Integer xEcStatut = -1;
        EbUser connectedUser = connectedUserService.getCurrentUser();

        Object[] objTracings = ebTrackTraceRepository
                .selectListEbTrackTraceNumByEbDemandeNum(ebDemande.getEbDemandeNum());
        List<Integer> tracings = new ArrayList<Integer>();

        if (objTracings != null && objTracings.length > 0) {
            for (Object c : objTracings) tracings.add((Integer) c);

            List<Date> listOldDateEvents = ebTtEventRepository
                    .findAllByEbTtTracingAndNatureDate(
                            tracings.get(0),
                            TtEnumeration.NatureDateEvent.DATE_ESTIMATED.getCode());
            List<EbTtEvent> listEvents = new ArrayList<EbTtEvent>();

            for (EbTtCompanyPsl psl : ebDemande.getxEbSchemaPsl().getListPsl()) {

                if (psl.getExpectedDate() != null) {
                    ebPslAppRepository.updateEstimatedDate(tracings, psl.getCodeAlpha(), psl.getExpectedDate());

                    // Vérifier si la date de psl est mis à jour afin d'éviter
                    // de créer des doublons
                    // d'event
                    boolean isUpdateDate = true;

                    if (listOldDateEvents != null && !listOldDateEvents.isEmpty()) {

                        for (Date oldDate : listOldDateEvents) {

                            if (oldDate != null && oldDate.getTime() == psl.getExpectedDate().getTime()) {
                                isUpdateDate = false;
                                break;
                            }

                        }

                    }

                    if (isUpdateDate) {

                        for (Integer numTracing : tracings) {
                            EbTtEvent ttEvent = new EbTtEvent();
                            ttEvent.setDateCreation(new Date());
                            ttEvent
                                    .setxEbTtPslApp(
                                            ebPslAppRepository
                                                    .findByCodePslCodeAndXEbTrackTrace_ebTtTracingNum(
                                                            psl.getEbTtCompanyPslNum(),
                                                            numTracing));
                            ttEvent.setDateEvent(psl.getExpectedDate());
                            ttEvent.setxEbTtTracing(new EbTtTracing(numTracing));
                            ttEvent.setNatureDate(TtEnumeration.NatureDateEvent.DATE_ESTIMATED.getCode());
                            ttEvent.setxEbUser(new EbUser(connectedUser.getEbUserNum()));
                            ttEvent
                                    .setxEbEtablissement(
                                            new EbEtablissement(connectedUser.getEbEtablissement().getEbEtablissementNum()));
                            ttEvent
                                    .setxEbCompagnie(new EbCompagnie(connectedUser.getEbCompagnie().getEbCompagnieNum()));
                            listEvents.add(ttEvent);
                        }

                    }

                }

            }

            ebTtEventRepository.saveAll(listEvents);

            try {
                Optional<EbDemande> demande = ebDemandeRepository.findById(ebDemande.getEbDemandeNum());
                xEcStatut = demande.get().getxEcStatut();

                if (sendMail) {
                    emailServicePricingBooking.sendEmailTranpsportInformation(demande.get(), connectedUser);
                }

            } catch (Exception e) {
            }

        }

        return xEcStatut;
    }

    @Override
    public Integer deleteQuotation(Integer ebDemandeTransporteurNum) {
        exEbDemandeTransporteurRepository.deleteById(ebDemandeTransporteurNum);
        return 1;
    }

    @Override
    public Integer updateGoodsPickup(EbDemande[] ebDemandes) {

        for (EbDemande ebDemande : ebDemandes) {
            ebDemandeRepository
                    .updateGoodsPickup(
                            ebDemande.getEbDemandeNum(),
                            ebDemande.getDatePickupTM(),
                            ebDemande.getDateOfGoodsAvailability(),
                            ebDemande.getFinalDelivery(),
                            ebDemande.getConfirmPickup(),
                            ebDemande.getConfirmDelivery());

            EbUser connectedUser = connectedUserService.getCurrentUser();
            if (connectedUser.isPrestataire()) this.updateTTPsl(ebDemande, true);
        }

        return 1;
    }

    public Map<String, Object> updateTransportRequestAndProcessPostUpdate(EbDemande ebDemande, Integer typeData, Integer module, EbUser connectedUser) throws Exception {
        if (module == null || typeData == null) return new HashMap<String, Object>();
        Map<String, Object> result = updateEbDemande(ebDemande, typeData, module, connectedUser);

        pricingAnnexMethodsService.postProcessUpdateEbDemande(ebDemande, connectedUser);

        return result;
    }

    @Transactional
    @Override
    public Map<String, Object> updateEbDemande(EbDemande ebDemande, Integer typeData, Integer module, EbUser connectedUser) throws Exception {
        EbDemande oldEbDemande = null;

        SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();
        criteria.setEbDemandeNum(ebDemande.getEbDemandeNum());
        oldEbDemande = daoPricing.getEbDemande(criteria, connectedUser);
        oldEbDemande.setExEbDemandeTransporteurs(ebDemande.getExEbDemandeTransporteurs());
        oldEbDemande.setListMarchandises(ebDemande.getListMarchandises());
        oldEbDemande.setxEbUserObserver(ebDemande.getxEbUserObserver());
        StringBuilder sb = new StringBuilder();
        Map<String, Object> result = historizeModifToEbchat(
                ebDemande,
                typeData,
                module,
                oldEbDemande,
                connectedUser,
                sb);
        updateEbDemandeWithTransactional(ebDemande, typeData, module, oldEbDemande, connectedUser, sb);
        return result;
    }

    private Map<String, Object> updateEbDemandeWithTransactional(
            EbDemande ebDemande,
            Integer typeData,
            Integer module,
            EbDemande oldEbDemande,
            EbUser connectedUser,
            StringBuilder sb)
            throws JsonProcessingException {
        Map<String, Object> result = new HashMap<String, Object>();
        String libelleTypeData = null;
        EbDemande repoEbDemande = null;
        Boolean isCreateTf = false;

        Map<String, Object> mapHistory = new HashMap<String, Object>();
        repoEbDemande = ebDemandeRepository.getOne(ebDemande.getEbDemandeNum());
        repoEbDemande.setUpdateDate(new Date());

        ebDemande.setxEcStatut(oldEbDemande.getxEcStatut());
        ebDemande.setUser(oldEbDemande.getUser());
        if (oldEbDemande == null) {
            oldEbDemande = repoEbDemande;
        }

        //delete observers old observers
        demandeObserversRepository.deleteTrsObservers(Collections.singletonList(ebDemande.getEbDemandeNum()));
        pricingCreateAndUpdateService.saveObservers(ebDemande);

        if ((TypeDataEbDemande.EXPEDITION_INFORMATION.getCode().equals(typeData) ||
                TypeDataEbDemande.ALL.getCode().equals(typeData)) &&
                !(oldEbDemande.getxEcStatut() != null &&
                        oldEbDemande.getxEcStatut() >= Enumeration.StatutDemande.WPU.getCode())) {

            if (oldEbDemande.getxEcStatut() != null &&
                    oldEbDemande.getxEcStatut().equals(StatutDemande.PND.getCode())) {
                oldEbDemande.setxEcStatut(StatutDemande.INP.getCode());
                oldEbDemande.setWaitingForQuote(new Date());
                repoEbDemande.setxEcStatut(StatutDemande.INP.getCode());
                repoEbDemande.setWaitingForQuote(new Date());
            }

            if (ebDemande.getUser() != null &&
                    (oldEbDemande.getUser() == null ||
                            !ebDemande.getUser().getEbUserNum().equals(oldEbDemande.getUser().getEbUserNum()))) {
                repoEbDemande.setUser(ebDemande.getUser());
                oldEbDemande.setUser(ebDemande.getUser());
            }

            updateParty(ebDemande, oldEbDemande, repoEbDemande, mapHistory);

            if (ebDemande.getEligibleForConsolidation() != null &&
                    !ebDemande.getEligibleForConsolidation().equals(oldEbDemande.getEligibleForConsolidation())) {
                repoEbDemande.setEligibleForConsolidation(ebDemande.getEligibleForConsolidation());
                oldEbDemande.setEligibleForConsolidation(ebDemande.getEligibleForConsolidation());
            }

            if (ebDemande.getDateOfGoodsAvailability() != null &&
                    !ebDemande.getDateOfGoodsAvailability().equals(oldEbDemande.getDateOfGoodsAvailability())) {
                repoEbDemande.setDateOfGoodsAvailability(ebDemande.getDateOfGoodsAvailability());
                oldEbDemande.setDateOfGoodsAvailability(ebDemande.getDateOfGoodsAvailability());
            }

            if (ebDemande.getDateOfArrival() != null &&
                    !ebDemande.getDateOfArrival().equals(oldEbDemande.getDateOfArrival())) {
                repoEbDemande.setDateOfArrival(ebDemande.getDateOfArrival());
                oldEbDemande.setDateOfArrival(ebDemande.getDateOfArrival());
            }

            if (ebDemande.getxEcModeTransport() != null &&
                    !ebDemande.getxEcModeTransport().equals(oldEbDemande.getxEcModeTransport())) {
                repoEbDemande.setxEcModeTransport(ebDemande.getxEcModeTransport());
                oldEbDemande.setxEcModeTransport(ebDemande.getxEcModeTransport());
            }

            if (ebDemande.getxEbSchemaPsl() != null &&
                    (oldEbDemande.getxEbSchemaPsl() == null ||
                            !ebDemande
                                    .getxEbSchemaPsl().getEbTtSchemaPslNum()
                                    .equals(oldEbDemande.getxEbSchemaPsl().getEbTtSchemaPslNum()))) {
                repoEbDemande.setxEbSchemaPsl(ebDemande.getxEbSchemaPsl());
                oldEbDemande.setxEbSchemaPsl(ebDemande.getxEbSchemaPsl());
            }

            if (ebDemande.getxEbIncotermNum() != null &&
                    !ebDemande.getxEbIncotermNum().equals(oldEbDemande.getxEbIncotermNum())) {
                repoEbDemande.setxEbIncotermNum(ebDemande.getxEbIncotermNum());
                repoEbDemande.setxEcIncotermLibelle(ebDemande.getxEcIncotermLibelle());

                oldEbDemande.setxEbIncotermNum(ebDemande.getxEbIncotermNum());
                oldEbDemande.setxEcIncotermLibelle(ebDemande.getxEcIncotermLibelle());
            }

            if (ebDemande.getCity() != null && !ebDemande.getCity().equals(oldEbDemande.getCity())) {
                repoEbDemande.setCity(ebDemande.getCity());
                oldEbDemande.setCity(ebDemande.getCity());
            }

            if (ebDemande.getxEbTypeTransport() != null &&
                    !ebDemande.getxEbTypeTransport().equals(oldEbDemande.getxEbTypeTransport())) {
                repoEbDemande.setxEbTypeTransport(ebDemande.getxEbTypeTransport());
                oldEbDemande.setxEbTypeTransport(ebDemande.getxEbTypeTransport());
            }

            if (ebDemande.getxEbTypeFluxNum() != null &&
                    !ebDemande.getxEbTypeFluxNum().equals(oldEbDemande.getxEbTypeFluxNum())) {
                repoEbDemande.setxEbTypeFluxNum(ebDemande.getxEbTypeFluxNum());
                oldEbDemande.setxEbTypeFluxNum(ebDemande.getxEbTypeFluxNum());
                repoEbDemande.setxEbTypeFluxDesignation(ebDemande.getxEbTypeFluxDesignation());
                oldEbDemande.setxEbTypeFluxDesignation(ebDemande.getxEbTypeFluxDesignation());
                repoEbDemande.setxEbTypeFluxCode(ebDemande.getxEbTypeFluxCode());
                oldEbDemande.setxEbTypeFluxCode(ebDemande.getxEbTypeFluxCode());
            }

        }

        if (TypeDataEbDemande.REFERENCE_INFORMATION.getCode().equals(typeData) ||
                TypeDataEbDemande.ALL.getCode().equals(typeData)) {
            libelleTypeData = TypeDataEbDemande.REFERENCE_INFORMATION.getLibelle();

            if (ebDemande.getCustomerReference() != null &&
                    !ebDemande.getCustomerReference().equals(oldEbDemande.getCustomerReference())) {
                repoEbDemande.setCustomerReference(ebDemande.getCustomerReference());
                oldEbDemande.setCustomerReference(ebDemande.getCustomerReference());
            }

            if (ebDemande.getxEbCostCenter() != null &&
                    (oldEbDemande.getxEbCostCenter() == null ||
                            !ebDemande
                                    .getxEbCostCenter().getEbCostCenterNum()
                                    .equals(oldEbDemande.getxEbCostCenter().getEbCostCenterNum()))

            ) {
                repoEbDemande.setxEbCostCenter(ebDemande.getxEbCostCenter());
                oldEbDemande.setxEbCostCenter(ebDemande.getxEbCostCenter());
            }

            {
                boolean exists = false;
                boolean modified = false;

                EbDemande d = ebDemandeRepository.selectListCategoriesByEbDemandeNum(oldEbDemande.getEbDemandeNum());
                List<EbCategorie> listOldCategories = d.getListCategories();

                if (ebDemande.getListCategories() != null && !ebDemande.getListCategories().isEmpty()) {

                    for (EbCategorie cat : ebDemande.getListCategories()) {
                        exists = false;

                        if (listOldCategories != null && !listOldCategories.isEmpty()) {

                            for (EbCategorie oldCat : listOldCategories) {

                                if (cat.getEbCategorieNum().equals(oldCat.getEbCategorieNum())) {
                                    exists = true;

                                    List<EbLabel> listLabel = new ArrayList<EbLabel>();
                                    if (cat.getLabels() != null) listLabel.addAll(cat.getLabels());
                                    List<EbLabel> listOldLabel = new ArrayList<EbLabel>();
                                    if (oldCat.getLabels() != null) listOldLabel.addAll(oldCat.getLabels());

                                    if (!listOldLabel.isEmpty() &&
                                            (listLabel.isEmpty() ||
                                                    !listLabel
                                                            .get(0).getEbLabelNum().equals(listOldLabel.get(0).getEbLabelNum()))) {
                                        modified = true;
                                    }

                                    if (!listLabel.isEmpty() && listOldLabel.isEmpty()) {
                                        modified = true;
                                    }

                                }

                            }

                        }

                        if (!exists) {
                            List<EbLabel> listLabel = new ArrayList<EbLabel>();
                            if (cat.getLabels() != null) listLabel.addAll(cat.getLabels());

                            if (!listLabel.isEmpty()) {
                                modified = true;
                            }

                        }

                    }

                }

                if (modified ||
                        ebDemande.getListCategories() != null &&
                                listOldCategories.size() != ebDemande.getListCategories().size()) {
                    ebDemande.setListCategories(ebDemande.getListCategories());

                    repoEbDemande.setListCategories(ebDemande.getListCategories());
                    oldEbDemande.setListCategories(ebDemande.getListCategories());
                    repoEbDemande.setListCategoriesStr(ebDemande.getListCategoriesStr());
                    oldEbDemande.setListCategoriesStr(ebDemande.getListCategoriesStr());
                    repoEbDemande.setListLabels(ebDemande.getListLabels());
                    oldEbDemande.setListLabels(ebDemande.getListLabels());
                }

            }

            if (ebDemande.getxEbTypeRequest() != null &&
                    !ebDemande.getxEbTypeRequest().equals(oldEbDemande.getxEbTypeRequest())) {
                repoEbDemande.setxEbTypeRequest(ebDemande.getxEbTypeRequest());
                repoEbDemande.setTypeRequestLibelle(ebDemande.getTypeRequestLibelle());

                oldEbDemande.setxEbTypeRequest(ebDemande.getxEbTypeRequest());
                oldEbDemande.setTypeRequestLibelle(ebDemande.getTypeRequestLibelle());
            }

            if (ebDemande.getCustomFields() != null &&
                    !ebDemande.getCustomFields().equals(oldEbDemande.getCustomFields())) {
                List<CustomFields> listCustomsFields = gson
                        .fromJson(ebDemande.getCustomFields(), new TypeToken<ArrayList<CustomFields>>() {
                        }.getType());
                List<CustomFields> oldListCustomsFields = gson
                        .fromJson(oldEbDemande.getCustomFields(), new TypeToken<ArrayList<CustomFields>>() {
                        }.getType());
                if (oldListCustomsFields == null) oldListCustomsFields = new ArrayList<CustomFields>();
                Boolean modified = false;
                Boolean exists = false;

                if (listCustomsFields.size() == oldListCustomsFields.size()) {

                    for (CustomFields field : listCustomsFields) {

                        for (CustomFields oldField : oldListCustomsFields) {

                            if (field.getName().equals(oldField.getName())) {
                                if (!modified) exists = true;

                                if (field.getValue() != null && !field.getValue().equals(oldField.getValue())) {
                                    modified = true;
                                }

                            }

                        }
                    }
                }

                if (modified || !exists) {
                    repoEbDemande.setCustomFields(ebDemande.getCustomFields());
                    oldEbDemande.setCustomFields(ebDemande.getCustomFields());
                }

            }

            if (ebDemande.getCommentChargeur() != null &&
                    !ebDemande.getCommentChargeur().equals(oldEbDemande.getCommentChargeur())) {
                repoEbDemande.setCommentChargeur(ebDemande.getCommentChargeur());
                oldEbDemande.setCommentChargeur(ebDemande.getCommentChargeur());
            }

            if (ebDemande.getInsurance() != null && !ebDemande.getInsurance().equals(oldEbDemande.getInsurance())) {
                repoEbDemande.setInsurance(ebDemande.getInsurance());
                oldEbDemande.setInsurance(ebDemande.getInsurance());
            }

            if (ebDemande.getInsuranceValue() != null &&
                    !ebDemande.getInsuranceValue().equals(oldEbDemande.getInsuranceValue())) {
                repoEbDemande.setInsuranceValue(ebDemande.getInsuranceValue());
                oldEbDemande.setInsuranceValue(ebDemande.getInsuranceValue());
            }

            if (ebDemande.getxEcTypeCustomBroker() != null &&
                    !ebDemande.getxEcTypeCustomBroker().equals(oldEbDemande.getxEcTypeCustomBroker())) {
                repoEbDemande.setxEcTypeCustomBroker(ebDemande.getxEcTypeCustomBroker());
                oldEbDemande.setxEcTypeCustomBroker(ebDemande.getxEcTypeCustomBroker());
            }

            if (ebDemande.getXecCurrencyInvoice() != null &&
                    ebDemande.getXecCurrencyInvoice().getEcCurrencyNum() != null &&
                    (oldEbDemande.getXecCurrencyInvoice() == null ||
                            !ebDemande
                                    .getXecCurrencyInvoice().getEcCurrencyNum()
                                    .equals(oldEbDemande.getXecCurrencyInvoice().getEcCurrencyNum()))) {
                repoEbDemande.setXecCurrencyInvoice(ebDemande.getXecCurrencyInvoice());
                oldEbDemande.setXecCurrencyInvoice(ebDemande.getXecCurrencyInvoice());
            }

            if (ebDemande.getxEbUserOrigin() != null &&
                    (oldEbDemande.getxEbUserOrigin() == null ||
                            !ebDemande
                                    .getxEbUserOrigin().getEbUserNum().equals(oldEbDemande.getxEbUserOrigin().getEbUserNum())) ||
                    ebDemande.getxEbUserOrigin() == null &&
                            oldEbDemande.getxEbUserOrigin() != null && oldEbDemande.getxEbUserOrigin().getEbUserNum() != null) {

                if (oldEbDemande.getxEbUserOrigin() != null && oldEbDemande.getxEbUserOrigin().getEbUserNum() != null) {
                    // add desaffectation alerte to old user
                    this.notificationService
                            .generateNotificationForUser(
                                    ebDemande,
                                    oldEbDemande.getxEbUserOrigin().getEbUserNum(),
                                    module,
                                    false);
                }

                oldEbDemande.setxEbUserOrigin(ebDemande.getxEbUserOrigin());

                if (ebDemande.getxEbUserOrigin() != null && ebDemande.getxEbUserOrigin().getEbUserNum() != null) {
                    repoEbDemande
                            .setxEbUserOrigin(ebUserRepository.getOne(ebDemande.getxEbUserOrigin().getEbUserNum()));
                    EbUser ebUserOrigin = ebUserRepository.getOne(ebDemande.getxEbUserOrigin().getEbUserNum());
                    repoEbDemande.setxEbUserOrigin(ebUserOrigin);
                    EbEtablissement ebEtablissement = ebUserOrigin.getEbEtablissement();
                    repoEbDemande
                            .setxEbEtabOrigin(ebEtablissementRepository.getOne(ebEtablissement.getEbEtablissementNum()));

                    // add affectation alerte to old user
                    this.notificationService
                            .generateNotificationForUser(
                                    ebDemande,
                                    ebDemande.getxEbUserOrigin().getEbUserNum(),
                                    module,
                                    true);
                }

            }

            if ((ebDemande.getTdcAcknowledge() == null || !ebDemande.getTdcAcknowledge()) &&
                    ebDemande.getTdcAcknowledgeDate() == null && oldEbDemande.getTdcAcknowledgeDate() != null) {
                repoEbDemande.setTdcAcknowledgeDate(ebDemande.getTdcAcknowledgeDate());
                oldEbDemande.setTdcAcknowledgeDate(ebDemande.getTdcAcknowledgeDate());

                repoEbDemande.setTdcAcknowledge(ebDemande.getTdcAcknowledge());
                oldEbDemande.setTdcAcknowledge(ebDemande.getTdcAcknowledge());
            }

            if (ebDemande.getxEbUserDest() != null &&
                    (oldEbDemande.getxEbUserDest() == null ||
                            !ebDemande.getxEbUserDest().getEbUserNum().equals(oldEbDemande.getxEbUserDest().getEbUserNum())) ||
                    ebDemande.getxEbUserDest() == null &&
                            oldEbDemande.getxEbUserDest() != null && oldEbDemande.getxEbUserDest().getEbUserNum() != null) {

                if (oldEbDemande.getxEbUserDest() != null && oldEbDemande.getxEbUserDest().getEbUserNum() != null) { // add
                    // desaffectation
                    // alerte
                    // to
                    // old
                    // user
                    this.notificationService
                            .generateNotificationForUser(
                                    ebDemande,
                                    oldEbDemande.getxEbUserDest().getEbUserNum(),
                                    module,
                                    false);
                }

                oldEbDemande.setxEbUserDest(ebDemande.getxEbUserDest());

                if (ebDemande.getxEbUserDest() != null && ebDemande.getxEbUserDest().getEbUserNum() != null) {
                    repoEbDemande.setxEbUserDest(ebUserRepository.getOne(ebDemande.getxEbUserDest().getEbUserNum()));
                    if (ebDemande.getxEbUserDest().getEbEtablissement().getEbEtablissementNum() != null) repoEbDemande
                            .setxEbEtabDest(
                                    ebEtablissementRepository
                                            .getOne(ebDemande.getxEbUserDest().getEbEtablissement().getEbEtablissementNum()));

                    // add affectation alert to new user
                    this.notificationService
                            .generateNotificationForUser(
                                    ebDemande,
                                    ebDemande.getxEbUserDest().getEbUserNum(),
                                    module,
                                    true);
                }

            }

            if (ebDemande.getxEbUserOriginCustomsBroker() != null &&
                    (oldEbDemande.getxEbUserOriginCustomsBroker() == null ||
                            !ebDemande
                                    .getxEbUserOriginCustomsBroker().getEbUserNum()
                                    .equals(oldEbDemande.getxEbUserOriginCustomsBroker().getEbUserNum())) ||
                    ebDemande.getxEbUserOriginCustomsBroker() == null &&
                            oldEbDemande.getxEbUserOriginCustomsBroker() != null &&
                            oldEbDemande.getxEbUserOriginCustomsBroker().getEbUserNum() != null) {

                if (oldEbDemande.getxEbUserOriginCustomsBroker() != null &&
                        oldEbDemande.getxEbUserOriginCustomsBroker().getEbUserNum() != null) { // add desaffectation
                    // alerte to old user
                    this.notificationService
                            .generateNotificationForUser(
                                    ebDemande,
                                    oldEbDemande.getxEbUserOriginCustomsBroker().getEbUserNum(),
                                    module,
                                    false);
                }

                oldEbDemande.setxEbUserOriginCustomsBroker(ebDemande.getxEbUserOriginCustomsBroker());
            }

            if (ebDemande.getxEbUserObserver() != null &&
                    !ebDemande.getxEbUserObserver().equals(oldEbDemande.getxEbUserObserver())

            ) {

                if ((ebDemande.getxEbUserObserver() == null || ebDemande.getxEbUserObserver().isEmpty()) && // desaffectation
                        // case
                        (oldEbDemande.getxEbUserObserver() != null && !oldEbDemande.getxEbUserObserver().isEmpty())) {
                    oldEbDemande.getxEbUserObserver().forEach(user -> {
                        this.notificationService
                                .generateNotificationForUser(ebDemande, user.getEbUserNum(), module, false);
                    });
                } else if (oldEbDemande.getxEbUserObserver() == null || oldEbDemande.getxEbUserObserver().isEmpty() // affectation
                    // case
                ) {
                    ebDemande.getxEbUserObserver().forEach(user -> {
                        this.notificationService
                                .generateNotificationForUser(ebDemande, user.getEbUserNum(), module, true);
                    });
                } else // mixed case
                {
                    List<Integer> newUsers = ebDemande
                            .getxEbUserObserver().stream().map(user -> user.getEbUserNum()).collect(Collectors.toList());

                    List<Integer> oldUsers = oldEbDemande
                            .getxEbUserObserver().stream().map(user -> user.getEbUserNum()).collect(Collectors.toList());

                    List<Integer> newAffectedUsers = newUsers
                            .stream().filter(userNum -> !oldUsers.contains(userNum)).collect(Collectors.toList());

                    List<Integer> desaffectedUsers = oldUsers
                            .stream().filter(userNum -> !newUsers.contains(userNum)).collect(Collectors.toList());

                    newAffectedUsers.forEach(userNum -> {
                        this.notificationService.generateNotificationForUser(ebDemande, userNum, module, true);
                    });

                    desaffectedUsers.forEach(userNum -> {
                        this.notificationService.generateNotificationForUser(ebDemande, userNum, module, false);
                    });
                }

                repoEbDemande.setxEbUserObserver(ebDemande.getxEbUserObserver());
                oldEbDemande.setxEbUserObserver(ebDemande.getxEbUserObserver());
            }

            if (ebDemande.getxEbUserDestCustomsBroker() != null &&
                    (oldEbDemande.getxEbUserDestCustomsBroker() == null ||
                            !ebDemande
                                    .getxEbUserDestCustomsBroker().getEbUserNum()
                                    .equals(oldEbDemande.getxEbUserDestCustomsBroker().getEbUserNum())) ||
                    ebDemande.getxEbUserDestCustomsBroker() == null &&
                            oldEbDemande.getxEbUserDestCustomsBroker() != null &&
                            oldEbDemande.getxEbUserDestCustomsBroker().getEbUserNum() != null) {

                if (oldEbDemande.getxEbUserDestCustomsBroker() != null &&
                        oldEbDemande.getxEbUserDestCustomsBroker().getEbUserNum() != null) { // add desaffectation alerte
                    // to old user
                    this.notificationService
                            .generateNotificationForUser(
                                    ebDemande,
                                    oldEbDemande.getxEbUserDestCustomsBroker().getEbUserNum(),
                                    module,
                                    false);
                }

                oldEbDemande.setxEbUserDestCustomsBroker(ebDemande.getxEbUserDestCustomsBroker());
                repoEbDemande.setxEbUserDestCustomsBroker(ebDemande.getxEbUserDestCustomsBroker());
                oldEbDemande.setxEbUserDestCustomsBroker(ebDemande.getxEbUserDestCustomsBroker());
            }

            /*
             * ObjectMapper oMapper = new ObjectMapper();
             * oMapper.configure(SerializationFeature.EAGER_SERIALIZER_FETCH,
             * false);
             * oMapper.configure(DeserializationFeature.
             * EAGER_DESERIALIZER_FETCH, false);
             * Map<String, Object> mapEbDemande =
             * oMapper.convertValue(ebDemande,
             * Map.class); Map<String, Object> mapOldEbDemande =
             * oMapper.convertValue(oldEbDemande, Map.class);
             * for(String key: mapOldEbDemande.keySet()) {
             * if(mapOldEbDemande.get(key) !=
             * null) { if(mapOldEbDemande.get(key) instanceof String ||
             * mapOldEbDemande.get(key) instanceof Integer ||
             * mapOldEbDemande.get(key)
             * instanceof Boolean) { if(mapOldEbDemande.get(key) != null &&
             * mapEbDemande.get(key) != null &&
             * !mapEbDemande.get(key).equals(mapOldEbDemande.get(key))) {
             * mapHistory.put(key, mapOldEbDemande.get(key)); } } } }
             */

            /*
             * if(ebDemande.getEbPartyOrigin() != null &&
             * ebDemande.getEbPartyOrigin().getEbPartyNum() != null) {
             * ebPartyRepository.save(ebDemande.getEbPartyOrigin());
             * mapHistory.put("ebPartyOrigin", ebDemande.getEbPartyOrigin()); }
             * if(ebDemande.getEbPartyDest() != null &&
             * ebDemande.getEbPartyDest().getEbPartyNum() != null) {
             * ebPartyRepository.save(ebDemande.getEbPartyDest());
             * mapHistory.put("ebPartyDest", ebDemande.getEbPartyDest()); }
             * if(ebDemande.getEbPartyNotif() != null) {
             * ebPartyRepository.save(ebDemande.getEbPartyNotif());
             * mapHistory.put("ebPartyNotif", oldEbDemande.getEbPartyNotif()); }
             */
        }

        if ((TypeDataEbDemande.GOODS_INFORMATION.getCode().equals(typeData) ||
                TypeDataEbDemande.ALL.getCode().equals(typeData)) &&
                !(oldEbDemande.getxEcStatut() != null &&
                        oldEbDemande.getxEcStatut() >= Enumeration.StatutDemande.WPU.getCode())) {
            List<Integer> listEbDemandeNum = new ArrayList<>();
            listEbDemandeNum.add(ebDemande.getEbDemandeNum());
            ebMarchandiseRepository.deleteListEbMarchandiseByEbDemandeNumIn(listEbDemandeNum);

            if (ebDemande.getListMarchandises() != null && ebDemande.getListMarchandises().size() > 0) {
                libelleTypeData = TypeDataEbDemande.GOODS_INFORMATION.getLibelle();

                Double ttWeight = 0.0;
                Double ttVolume = 0.0;
                Double ttUnits = 0.0;

                for (EbMarchandise ebMarchandise : ebDemande.getListMarchandises()) {
                    if (ebMarchandise.getWeight() != null) ttWeight += ebMarchandise.getWeight();
                    if (ebMarchandise.getVolume() != null) ttVolume += ebMarchandise.getVolume();

                    if (ebMarchandise.getNumberOfUnits() == null) ebMarchandise.setNumberOfUnits(1);
                    ttUnits += ebMarchandise.getNumberOfUnits();
                    ebMarchandise.setEbDemande(new EbDemande(ebDemande.getEbDemandeNum()));
                }

                repoEbDemande.setTotalNbrParcel(ttUnits);
                repoEbDemande.setTotalVolume(ttVolume);
                repoEbDemande.setTotalTaxableWeight(ttWeight);
                repoEbDemande.setTotalWeight(ttWeight);

                ebMarchandiseRepository.saveAll(ebDemande.getListMarchandises());
            }

        }

        if (TypeDataEbDemande.ALL.getCode().equals(typeData)) {
            exEbDemandeTransporteurRepository.deleteListEbDemandeQuoteByEbDemandeNum(ebDemande.getEbDemandeNum());

            if (ebDemande.getExEbDemandeTransporteurs() != null) {

                for (ExEbDemandeTransporteur it : ebDemande.getExEbDemandeTransporteurs()) {
                    it.setCityPickup(ebDemande.getEbPartyOrigin().getCity());
                    it.setxEcCountryPickup(ebDemande.getEbPartyOrigin().getxEcCountry());
                    it.setCityDelivery(ebDemande.getEbPartyDest().getCity());
                    it.setxEcCountryDelivery(ebDemande.getEbPartyDest().getxEcCountry());
                    it.setxEbUserCt(ebDemande.getxEbUserCt());
                    it.setxEbDemande(ebDemande);

                    if (it.getxTransporteur().getSelected() != null && it.getxTransporteur().getSelected()) {
                        EbEtablissement trEtablissement = ebEtablissementRepository
                                .getOne(it.getxEbEtablissement().getEbEtablissementNum());
                        it.getxTransporteur().setEbEtablissement(trEtablissement);
                    }

                    if (it.getStatus() != null && it.getStatus().equals(Enumeration.StatutCarrier.FIN.getCode()) ||
                            it.getStatus() != null && it.getStatus().equals(Enumeration.StatutCarrier.FIN_PLAN.getCode())) {
                        isCreateTf = true;
                        ebDemande.set_exEbDemandeTransporteurFinal(it);
                    }

                }

            }

            if (ebDemande.getExEbDemandeTransporteurs() != null && !ebDemande.getExEbDemandeTransporteurs().isEmpty()) {
                exEbDemandeTransporteurRepository.saveAll(ebDemande.getExEbDemandeTransporteurs());
            }

            if (isCreateTf) {
                repoEbDemande.setFlagEbtrackTrace(false);
                repoEbDemande.setxEcNature(NatureDemandeTransport.TRANSPORT_PRINCIPAL.getCode());
                ebDemandeRepository.save(repoEbDemande);
                this
                        .updateStatusQuotation(
                                ebDemande.get_exEbDemandeTransporteurFinal(),
                                true,
                                Enumeration.Module.TRANSPORT_MANAGEMENT.getCode());
            }

        }

        if (TypeDataEbDemande.EXTERNAL_USERS.getCode().equals(typeData) ||
                TypeDataEbDemande.ALL.getCode().equals(typeData)) {
            libelleTypeData = TypeDataEbDemande.EXTERNAL_USERS.getLibelle();

            if (ebDemande.getxEbUserOrigin() != null &&
                    (oldEbDemande.getxEbUserOrigin() == null ||
                            !ebDemande
                                    .getxEbUserOrigin().getEbUserNum().equals(oldEbDemande.getxEbUserOrigin().getEbUserNum())) ||
                    ebDemande.getxEbUserOrigin() == null && oldEbDemande.getxEbUserOrigin() != null) {
                repoEbDemande.setxEbUserOrigin(ebDemande.getxEbUserOrigin());
                oldEbDemande.setxEbUserOrigin(ebDemande.getxEbUserOrigin());
            }

            if (ebDemande.getxEbUserDest() != null &&
                    (oldEbDemande.getxEbUserDest() == null ||
                            !ebDemande.getxEbUserDest().getEbUserNum().equals(oldEbDemande.getxEbUserDest().getEbUserNum())) ||
                    ebDemande.getxEbUserDest() == null && oldEbDemande.getxEbUserDest() != null) {
                repoEbDemande.setxEbUserDest(ebDemande.getxEbUserDest());
                oldEbDemande.setxEbUserDest(ebDemande.getxEbUserDest());
            }


            if (ebDemande.getxEbUserOriginCustomsBroker() != null &&
                    (oldEbDemande.getxEbUserOriginCustomsBroker() == null ||
                            !ebDemande
                                    .getxEbUserOriginCustomsBroker().getEbUserNum()
                                    .equals(oldEbDemande.getxEbUserOriginCustomsBroker().getEbUserNum())) ||
                    ebDemande.getxEbUserOriginCustomsBroker() == null &&
                            oldEbDemande.getxEbUserOriginCustomsBroker() != null) {
                repoEbDemande.setxEbUserOriginCustomsBroker(ebDemande.getxEbUserOriginCustomsBroker());
                oldEbDemande.setxEbUserOriginCustomsBroker(ebDemande.getxEbUserOriginCustomsBroker());
            }

            if (

                    ebDemande.getxEbUserObserver() != null &&
                            !ebDemande.getxEbUserObserver().equals(oldEbDemande.getxEbUserObserver())

            ) {
                repoEbDemande.setxEbUserObserver(ebDemande.getxEbUserObserver());
                oldEbDemande.setxEbUserObserver(ebDemande.getxEbUserObserver());
            }

            if (ebDemande.getxEbUserDestCustomsBroker() != null &&
                    (oldEbDemande.getxEbUserDestCustomsBroker() == null ||
                            !ebDemande
                                    .getxEbUserDestCustomsBroker().getEbUserNum()
                                    .equals(oldEbDemande.getxEbUserDestCustomsBroker().getEbUserNum())) ||
                    ebDemande.getxEbUserDestCustomsBroker() == null && oldEbDemande.getxEbUserDestCustomsBroker() != null) {
                repoEbDemande.setxEbUserDestCustomsBroker(ebDemande.getxEbUserDestCustomsBroker());
                oldEbDemande.setxEbUserDestCustomsBroker(ebDemande.getxEbUserDestCustomsBroker());
            }

        }

        // update statut groupage
        if (oldEbDemande.getxEcNature() != null &&
                oldEbDemande.getxEcNature().equals(NatureDemandeTransport.CONSOLIDATION.getCode())) {

            if (oldEbDemande.getxEcStatutGroupage() == null) {
                oldEbDemande.setxEcStatutGroupage(ConsolidationGroupage.NP.getCode());
            }

            if (ebDemande.getxEcStatutGroupage() != null) {

                if (ebDemande.getxEcStatutGroupage() > oldEbDemande.getxEcStatutGroupage()) {
                    oldEbDemande.setxEcStatutGroupage(ebDemande.getxEcStatutGroupage());
                    repoEbDemande.setxEcStatutGroupage(ebDemande.getxEcStatutGroupage());
                } else {
                    repoEbDemande.setxEcStatutGroupage(oldEbDemande.getxEcStatutGroupage());
                }

            }

        }

        if (repoEbDemande.getxEcStatut() < StatutDemande.FIN.getCode()) {
			unitValuation(ebDemande);
        }
        repoEbDemande.setxEcStatut(ebDemande.getxEcStatut());
        repoEbDemande.setValorized(ebDemande.getValorized());
        ebDemandeRepository.save(repoEbDemande);
        return result;
    }

    private void updateParty(
            EbDemande ebDemande,
            EbDemande oldEbDemande,
            EbDemande repoEbDemande,
            Map<String, Object> mapHistory) {
        List<EbParty> listSavedParty = new ArrayList<>();
        Boolean isUpdateParty = false;
        // origin party
        EbParty ebPartyOrigin = ebDemande.getEbPartyOrigin();
        EbParty oldPartyOrigin = oldEbDemande.getEbPartyOrigin();

        isUpdateParty = checkPartyDatas(ebPartyOrigin, oldPartyOrigin);

        repoEbDemande.setEbPartyOrigin(oldPartyOrigin);
        oldEbDemande.setEbPartyOrigin(oldPartyOrigin);

        if (isUpdateParty) {
            repoEbDemande.setLibelleOriginCity(oldPartyOrigin.getCity());
            repoEbDemande.setLibelleOriginCountry(oldPartyOrigin.getxEcCountry().getLibelle());

            oldEbDemande.setLibelleOriginCity(oldPartyOrigin.getCity());
            oldEbDemande.setLibelleOriginCountry(oldPartyOrigin.getxEcCountry().getLibelle());

            listSavedParty.add(oldPartyOrigin);
        }

        // dest party
        EbParty ebPartyDest = ebDemande.getEbPartyDest();
        EbParty oldPartyDest = oldEbDemande.getEbPartyDest();

        isUpdateParty = checkPartyDatas(ebPartyDest, oldPartyDest);

        repoEbDemande.setEbPartyDest(oldPartyDest);
        oldEbDemande.setEbPartyDest(oldPartyDest);

        if (isUpdateParty) {
            repoEbDemande.setLibelleDestCity(oldPartyDest.getCity());
						repoEbDemande.setLibelleDestCountry(oldPartyDest.getxEcCountry().getLibelle());

            oldEbDemande.setLibelleDestCity(oldPartyDest.getCity());
						oldEbDemande.setLibelleDestCountry(oldPartyDest.getxEcCountry().getLibelle());

            listSavedParty.add(oldPartyDest);
        }

        // notif party
        EbParty ebPartyNotif = ebDemande.getEbPartyNotif();
        EbParty oldPartyNotif = oldEbDemande.getEbPartyNotif();

        isUpdateParty = checkPartyDatas(ebPartyNotif, oldPartyNotif);

        repoEbDemande.setEbPartyNotif(oldPartyNotif);
        oldEbDemande.setEbPartyNotif(oldPartyNotif);

        if (isUpdateParty) {
            listSavedParty.add(oldPartyNotif);
        }

        // point Intermediate party
        EbParty ebPartyIP = ebDemande.getPointIntermediate();
        EbParty oldPartyIP = oldEbDemande.getPointIntermediate();

        isUpdateParty = checkPartyDatas(ebPartyIP, oldPartyIP);

        repoEbDemande.setEbPartyNotif(oldPartyIP);
        oldEbDemande.setEbPartyNotif(oldPartyIP);

        if (isUpdateParty) {
            listSavedParty.add(oldPartyIP);
        }

        ebPartyRepository.saveAll(listSavedParty);
    }

    private Boolean checkPartyDatas(EbParty ebParty, EbParty oldParty) {
        Boolean isUpdateParty = false;

        if (ebParty != null) {

            if (oldParty != null) {

                if (ebParty.getAdresse() != null && !ebParty.getAdresse().equals(oldParty.getAdresse())) {
                    oldParty.setAdresse(ebParty.getAdresse());
                    isUpdateParty = true;
                }

                if (ebParty.getAirport() != null && !ebParty.getAirport().equals(oldParty.getAirport())) {
                    oldParty.setAirport(ebParty.getAirport());
                    isUpdateParty = true;
                }

                if (ebParty.getCity() != null && !ebParty.getCity().equals(oldParty.getCity())) {
                    oldParty.setCity(ebParty.getCity());
                    isUpdateParty = true;
                }

                if (ebParty.getCompany() != null && !ebParty.getCompany().equals(oldParty.getCompany())) {
                    oldParty.setCompany(ebParty.getCompany());
                    isUpdateParty = true;
                }

                if (ebParty.getxEcCountry() != null &&
                        !ebParty.getxEcCountry().getEcCountryNum().equals(oldParty.getxEcCountry().getEcCountryNum())) {
                    oldParty.setxEcCountry(ebParty.getxEcCountry());
                    isUpdateParty = true;
                }

                if (ebParty.getEmail() != null && !ebParty.getEmail().equals(oldParty.getEmail())) {
                    oldParty.setEmail(ebParty.getEmail());
                    isUpdateParty = true;
                }

                if (ebParty.getOpeningHours() != null &&
                        !ebParty.getOpeningHours().equals(oldParty.getOpeningHours())) {
                    oldParty.setOpeningHours(ebParty.getOpeningHours());
                    isUpdateParty = true;
                }

                if (ebParty.getPhone() != null && !ebParty.getPhone().equals(oldParty.getPhone())) {
                    oldParty.setPhone(ebParty.getPhone());
                    isUpdateParty = true;
                }

                if (ebParty.getReference() != null && !ebParty.getReference().equals(oldParty.getReference())) {
                    oldParty.setReference(ebParty.getReference());
                    isUpdateParty = true;
                }

                if (ebParty.getZipCode() != null && !ebParty.getZipCode().equals(oldParty.getZipCode())) {
                    oldParty.setZipCode(ebParty.getZipCode());
                    isUpdateParty = true;
                }

                if (ebParty.getZoneRef() != null && !ebParty.getZoneRef().equals(oldParty.getZoneRef())) {
                    oldParty.setZoneRef(ebParty.getZoneRef());
                    isUpdateParty = true;
                }

            } else {
                oldParty = ebParty;
                isUpdateParty = true;
            }

        }

        return isUpdateParty;
    }

    @Transactional
    @Override
    public Integer deleteEbDemande(Integer[] arrEbDemandeNum) {

        if (arrEbDemandeNum.length > 0) {

            for (Integer ebDemandeNum : arrEbDemandeNum) {

                if (ebDemandeNum != null) {
                    Object[] objTracings = ebTrackTraceRepository.selectListEbTracingNumByEbDemandeNum(ebDemandeNum);
                    List<Integer> listEbdemandeNum = new ArrayList<Integer>();
                    List<Integer> tracings = new ArrayList<Integer>();
                    List<Integer> listEbIncidentNum = new ArrayList<Integer>();

                    listEbdemandeNum.add(ebDemandeNum);

                    if (objTracings != null && objTracings.length > 0) {
                        for (Object tr : objTracings) tracings.add((Integer) tr);
                    }

                    ebInvoiceRepository.deleteListEbInvoiceByEbDemandeNum(ebDemandeNum);

                    if (tracings.size() > 0) {
                        ebTtEventRepository.deleteListEbEventByEbTracingNumIn(tracings);
                        ebPslAppRepository.deleteListEbPslAppByEbTracingNumIn(tracings);
                        ebCptmProcedureRepository.deleteByEbTracingNum(tracings);
                        ebTrackTraceRepository.deleteListEbTracingByEbTracingNumIn(tracings);
                        ebQmDeviationRepository.deleteByEbTracingNum(tracings);
                    }

                    if (listEbdemandeNum.size() > 0) {
                        ebMarchandiseRepository.deleteListEbMarchandiseByEbDemandeNumIn(listEbdemandeNum);
                        listEbIncidentNum = ebQmIncidentRepository.findByxEbDemandeEbQmIncident(ebDemandeNum);

                        if (listEbIncidentNum != null && !listEbIncidentNum.isEmpty()) {
                            ebQmIncidentLineRepository.deleteIncidentLineByIncident(listEbIncidentNum);
                            ebQmIncidentRepository.deleteByEbDemandeNum(listEbdemandeNum);
                        }

                        ebCustomDeclarationRepository.deleteByEbDemandeNum(listEbdemandeNum);
                    }

                    exEbDemandeTransporteurRepository.deleteListEbDemandeQuoteByEbDemandeNum(ebDemandeNum);
                    ebDemandeRepository.deleteById(ebDemandeNum);
                }

            }

            return 1;
        }

        return 0;
    }

    @Override
    public Integer getNextEbDemandeNum() {
        return daoPricing.nextValEbDemande();
    }

    @Override
    public Map<String, Object> updateCustomsInformation(EbCustomsInformation ebCustomsInformation) {
        Map<String, Object> result = new HashMap<String, Object>();
        EbCustomsInformation saved = new EbCustomsInformation();
        Boolean flag = true;
        String attrs = "(declarationNumber|control|customsDuty|costCustomsDuty|xEcCurrencyCustomsDuty|vat|costVat|xEcCurrencyVat|otherTaxes|costOtherTaxes|xEcCurrencyOtherTaxes)";
        EbUser user = connectedUserService.getCurrentUser();

        try {
            saved = ebCustomsInformationRepository.save(ebCustomsInformation);

            // verifier s'il y a un champs non renseigné
            for (Method meth : EbCustomsInformation.class.getMethods()) {

                if (meth.getName().contains("get") &&
                        meth.getName().toLowerCase().matches(".+" + attrs.toLowerCase())) {

                    if (meth.invoke(ebCustomsInformation) == null ||
                            meth.invoke(ebCustomsInformation) instanceof String &&
                                    meth.invoke(ebCustomsInformation).toString().trim().length() == 0) {
                        flag = false;
                        break;
                    }

                }

            }

            ebDemandeRepository
                    .updateFlagCustomsInformation(ebCustomsInformation.getxEbDemande().getEbDemandeNum(), flag);

            String username = user.getRealUserNum() != null ? user.getRealUserNomPrenom() : user.getNomPrenom();
            String usernameFor = user.getRealUserNum() != null ? user.getNomPrenom() : null;
            String action = "Mise à jour du block Customs Information";
            Integer module = Enumeration.Module.TRANSPORT_MANAGEMENT.getCode();
            Integer chatCompId = Enumeration.IDChatComponent.TRANSPORT_MANAGEMENT.getCode();
            Integer idFiche = ebCustomsInformation.getxEbDemande().getEbDemandeNum();
            EbChat ebChat = listStatiqueService
                    .historizeAction(idFiche, chatCompId, module, action, username, usernameFor);

            result.put("ebChat", ebChat);
            result.put("ebCustomsInformatioNum", saved.getEbCustomsInformationNum());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public EbCustomsInformation getCustomsInformation(Integer ebDemandeNum) {
        return daoPricing.getCustomsInformation(ebDemandeNum);
    }

    @Override
    public Integer requestCustomsBroker(Integer ebDemandeNum) {
        return ebDemandeRepository.updateFlagRequestCustomsBroker(ebDemandeNum, true);
    }

    @Override
    public EbDemande demande(SearchCriteriaPricingBooking criteria) {
        // TODO Auto-generated method stub
        criteria.setStatus(Enumeration.StatutCarrier.FIN.getCode());
        criteria.setForCotation(true);
        EbDemande ebDemande = ebDemandeRepository.findById(criteria.getEbDemandeNum()).get();
        List<ExEbDemandeTransporteur> listDemandeTrans = daoPricing.getListExEbTransporteur(criteria);

        ebDemande.setExEbDemandeTransporteurs(listDemandeTrans);

        return ebDemande;
    }

    public static String getStatutLibelle(EbDemande ebDemande, EbUser connectedUser, Integer module) {
        Integer status = null;
        String statusStr = "";

        if (ebDemande.getxEcCancelled() != null &&
                ebDemande.getxEcCancelled().equals(Enumeration.CancelStatus.CANECLLED.getCode())) {
            statusStr = Enumeration.StatutDemande.CAN.getLibelle();
        }

        if (module.equals(Enumeration.Module.PRICING.getCode())) {
            if (ebDemande.getxEcStatut() >= StatutDemande.WDCT.getCode()) status = StatutDemande.FIN.getCode();

            if (connectedUser.isTransporteur() ||
                    connectedUser.isTransporteurBroker() &&
                            (ebDemande.getxEbUserOriginCustomsBroker() == null ||
                                    !connectedUser
                                            .getEbUserNum().equals(ebDemande.getxEbUserOriginCustomsBroker().getEbUserNum())) &&
                            (ebDemande.getxEbUserDestCustomsBroker() == null ||
                                    !connectedUser.getEbUserNum().equals(ebDemande.getxEbUserDestCustomsBroker().getEbUserNum()))) {

                if (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.WDCT.getCode() &&
                        ebDemande.getxEbTransporteurEtablissementNum() != null &&
                        ebDemande.getxEbTransporteurEtablissementNum()
                                != connectedUser.getEbEtablissement().getEbEtablissementNum()) {
                    status = null;
                    statusStr = Enumeration.StatutDemande.NAW.getLibelle();
                } else if (ebDemande.getxEcStatut() < StatutDemande.FIN.getCode()) {
                    status = ebDemande.getStatusQuote() != null ?
                            ebDemande.getStatusQuote() :
                            Enumeration.StatutDemande.INP.getCode();
                }

            } else if (ebDemande.getxEcStatut()
                    < StatutDemande.FIN.getCode()) status = ebDemande.getStatusQuote() != null ?
                    ebDemande.getStatusQuote() :
                    StatutDemande.INP.getCode();

        }

        if (ebDemande.getCodeAlphaPslCourant() != null &&
                StatutDemande.DLVRY
                        .getCode()
                        .equals(ebDemande.getxEcStatut()))
            statusStr = "Completed (" + ebDemande.getLibellePslCourant() + ")";

        if ((statusStr == null || statusStr.isEmpty()) &&
                (status == null || status == 0)) statusStr = StatutDemande.getLibelle(ebDemande.getxEcStatut());
        else if (statusStr == null || statusStr.isEmpty()) statusStr = StatutDemande.getLibelle(status);

        return statusStr;
    }

    @Override
    public EbDemande saveFlags(Integer ebDemandeNum, String newFlags) {
        EbDemande demande = ebDemandeRepository.findOneByEbDemandeNum(ebDemandeNum);
        demande.setListFlag(newFlags);
        return ebDemandeRepository.save(demande);
    }

    @Override
    public EbDemande updatePropRdv(EbDemande ebDemande) {
        EbDemande oldEbDemande = ebDemandeRepository.findOneByEbDemandeNum(ebDemande.getEbDemandeNum());
        oldEbDemande.setListPropRdvPk(ebDemande.getListPropRdvPk());
        oldEbDemande.setListPropRdvDl(ebDemande.getListPropRdvDl());
        List<EbTtCompanyPsl> listPsl = oldEbDemande.getxEbSchemaPsl().getListPsl();
        Date dateStartPSL = null, dateEndPSL = null;

        if (oldEbDemande.getListPropRdvDl() != null) {

            for (ProposionRdvDto dl : oldEbDemande.getListPropRdvDl()) {

                if (dl.getStatut() == PriseRDVStatus.VALIDEE.getCode()) {
                    ebDemandeRepository.updateDateDelivery(oldEbDemande.getEbDemandeNum(), dl.getDate());
                    dateEndPSL = dl.getDate();
                    break;
                }

            }

        }

        if (oldEbDemande.getListPropRdvPk() != null) {

            for (ProposionRdvDto pk : oldEbDemande.getListPropRdvPk()) {

                if (pk.getStatut() == PriseRDVStatus.VALIDEE.getCode()) {
                    ebDemandeRepository.updateDatePickup(oldEbDemande.getEbDemandeNum(), pk.getDate());
                    dateStartPSL = pk.getDate();
                    break;
                }

            }

        }

        for (EbTtCompanyPsl psl : listPsl) {

            if (psl.getDateAttendue() != null && psl.getDateAttendue()) {

                if (psl.getStartPsl() != null && psl.getStartPsl() && dateStartPSL != null) {
                    psl.setExpectedDate(dateStartPSL);
                }

                if (psl.getEndPsl() != null && psl.getEndPsl() && dateEndPSL != null) {
                    psl.setExpectedDate(dateEndPSL);
                }

            }

        }

        oldEbDemande.getxEbSchemaPsl().setListPsl(listPsl);

        return ebDemandeRepository.save(oldEbDemande);
        // return
        // ebDemandeRepository.updatePropRdvEbDemande(ebDemande.getEbDemandeNum(),
        // JacksonUtil.toString(ebDemande.getListPropRdvDl()),
        // JacksonUtil.toString(ebDemande.getListPropRdvPk()));
    }

    @Override
    public List<EbCostCategorie> getCostCategorieByMode(Integer modeTrans) {
        List<EbCostCategorie> listEbCostCategorie = ebCostCategorieRepository.findAll();
        return listEbCostCategorie;
    }

    @Override
    public List<EbCostCategorie> getAllCostCategorie() {
        List<EbCostCategorie> listEbCostCategorie = ebCostCategorieRepository.findAll();

        for (EbCostCategorie categorie : listEbCostCategorie) {
            List<EbCost> listEbCost = categorie.getListEbCost();

            for (EbCost costItem : listEbCost) {
                costItem.setLibelleCostItem(costItem.getLibelle());

                if (costItem.getPriceReal() == null) {
                    costItem.setEuroExchangeRateReal(null);
                }

            }

        }

        return listEbCostCategorie;
    }

    // soufiane
    @Override
    public void updateListTypeDoc(Integer ebDemandeNum, Integer ebInvoiceNum, String codeDoc, boolean isDecremante) {
        List<EbTypeDocuments> listTypeDocument = null;
        EbDemande demande = null;
        EbInvoice invoice = null;

        // EbDemande demande =
        // ebDemandeRepository.selectListTypeDocumentsByEbDemandeNum(ebDemandeNum);
        if (ebDemandeNum != null) {
            demande = ebDemandeRepository.getDemande(ebDemandeNum);

            if (demande != null && demande.getListTypeDocuments() != null) {
                listTypeDocument = new ArrayList<>(demande.getListTypeDocuments());
            }

        } else if (ebInvoiceNum != null) {
            invoice = ebInvoiceRepository.getEbInvoice(ebInvoiceNum);

            if (invoice != null) listTypeDocument = JsonUtils
                    .checkJsonListObject(invoice.getListTypeDocuments(), EbTypeDocuments.class);
        }

        if (listTypeDocument == null || listTypeDocument.isEmpty()) listTypeDocument = new ArrayList<>();

        EbTypeDocuments doc = listTypeDocument
                .stream().filter(d -> d.getCode() != null && d.getCode().equals(codeDoc)).findFirst().orElse(null);

        if (doc == null) {

            try {
                // l'objet ebTypeDoc ne doit pas être utilisé pour incrémenter
                // le nombre de doc car il touche l'objet de paramétrage
                // Ca cause un bug lorsqu'un type de document est créé à
                // postériori d'une TR
                // lorsque l'utilisateur souhaite attaché un document sur une TR
                // alors que le type de document n'existe pas
                // C'est la raison pour laquelle nous tentons de récupérer un
                // nouvel objet qui sera incrémenté et setter dans la liste des
                // documents
                EbTypeDocuments ebTypeDoc = ebTypeDocumentsRepository.findFirstByCode(codeDoc);

                if (ebTypeDoc != null) {
                    SearchCriteria criteria = new SearchCriteria();
                    criteria.setTypeDocumentNum(ebTypeDoc.getEbTypeDocumentsNum());
                    List<EbTypeDocuments> listDocType = daoTypeDocuments.getListEbTypeDocument(criteria);

                    if (listDocType != null && !listDocType.isEmpty()) {
                        doc = listDocType.get(0);
                        listTypeDocument.add(doc);
                    }

                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        if (doc != null) {
            Integer incremant;

            if (doc.getNbrDoc() == null) // verifier si nbr doc est null et je
                // l'affecte 0 pour pas genere un
                // exception au niveau upload d'un doc
                doc.setNbrDoc(0);

            if (isDecremante) {
                incremant = doc.getNbrDoc() - 1;
            } else {
                incremant = doc.getNbrDoc() + 1;
            }

            doc.setNbrDoc(incremant);
        }

        if (demande != null) {
            demande.setListTypeDocuments(listTypeDocument);
            ebDemandeRepository.save(demande);
        } else if (invoice != null) {
            invoice.setListTypeDocuments(listTypeDocument);
            ebInvoiceRepository.save(invoice);
        }

    }

    @Override
    public void updateQuotesWithPslByCompanyNum(List<Integer> listEbCompagnieNum) throws Exception {
        List<EbDemande> listEbDemande = ebDemandeRepository
                .selectListDemandeSchemaPslByListDemandeNum(listEbCompagnieNum);

        List<ExEbDemandeTransporteur> listQuote = new ArrayList<ExEbDemandeTransporteur>();

        if (listEbDemande != null && !listEbDemande.isEmpty()) {

            for (EbDemande d : listEbDemande) {

                if (d.getxEbSchemaPsl() != null && d.getxEbSchemaPsl().getListPsl() != null) {
                    List<ExEbDemandeTransporteur> listDemandeQuote = exEbDemandeTransporteurRepository
                            .findAllByXEbDemande_ebDemandeNum(d.getEbDemandeNum());

                    List<EbTtCompanyPsl> listPsl = d
                            .getxEbSchemaPsl().getListPsl().stream()
                            .filter(
                                    it -> it.getSchemaActif() != null &&
                                            it.getSchemaActif() && it.getIsChecked() != null && it.getIsChecked() &&
                                            it.getDateAttendue() != null && it.getDateAttendue())
                            .collect(Collectors.toList());

                    // gérer les dates estimées pour les quotes
                    if (listPsl != null && !listPsl.isEmpty()) {

                        for (ExEbDemandeTransporteur q : listDemandeQuote) {
                            if (q.getListEbTtCompagniePsl()
                                    == null) q.setListEbTtCompagniePslFromListCompanyPsl(listPsl);

                            q.setExpectedDateByCodePsl(null, true, null, q.getPickupTime());
                            q.setExpectedDateByCodePsl(null, null, true, q.getDeliveryTime());
                        }

                        listQuote.addAll(listDemandeQuote);
                    }

                }

            }

            if (listQuote != null && !listQuote.isEmpty()) exEbDemandeTransporteurRepository.saveAll(listQuote);
        }

    }

    @Override
    public List<EbDemande> getListEbDemandeFavoris(SearchCriteriaPricingBooking criteria) throws Exception {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);
        List<EbDemande> listDemande = daoPricing.getListEbDemandeFavoris(criteria);

        if (!listDemande.isEmpty()) {
            List<Integer> listEbDemandeNum = listDemande
                    .stream().map(it -> it.getEbDemandeNum()).collect(Collectors.toList());
            List<ExEbDemandeTransporteur> listTranspoFinal = exEbDemandeTransporteurRepository
                    .findByStatusQuote(Enumeration.StatutCarrier.FIN.getCode(), listEbDemandeNum);

            for (EbDemande d : listDemande) {
                ExEbDemandeTransporteur tr = listTranspoFinal
                        .stream().filter(it -> it.getxEbDemande().getEbDemandeNum().equals(d.getEbDemandeNum())).findFirst()
                        .orElse(null);

                if (tr != null) {
                    d.setExEbDemandeTransporteurs(new ArrayList<ExEbDemandeTransporteur>());
                    d.getExEbDemandeTransporteurs().add(tr);
                }

            }

            listDemande.forEach(dem -> {
                String listFlagIcon = dem.getListFlag() != null ? dem.getListFlag().trim() : null;

                if (listFlagIcon != null && !listFlagIcon.isEmpty()) {
                    dem.setListEbFlagDTO(EbFlagDTO.sortByName(flagService.getListFlagDTOFromCodes(listFlagIcon)));
                }

            });
        }

        return listDemande;
    }

    @Override
    public Map<String, Object> editQuotation(ExEbDemandeTransporteur exEbDemandeTransporteur, Integer moduleNum) {
        ExEbDemandeTransporteur oldDemandeTrans = exEbDemandeTransporteurRepository
                .getOne(exEbDemandeTransporteur.getExEbDemandeTransporteurNum());
        Map<String, Object> result = new HashMap<String, Object>();

        if (oldDemandeTrans != null) {
            String action = "Quotation modified <br>";

            if (!StringUtils.equals(oldDemandeTrans.getExchangeRate(), exEbDemandeTransporteur.getExchangeRate())) {
                action += " * <strike> " +
                        (oldDemandeTrans.getExchangeRate() != null ? oldDemandeTrans.getExchangeRate() : "-") +
                        "</strike> ";
                action += (exEbDemandeTransporteur.getExchangeRate() != null ?
                        exEbDemandeTransporteur.getExchangeRate() :
                        "-") + "<br>";
            }

            if (!StringUtils
                    .equals(
                            oldDemandeTrans.getInfosForCustomsClearance(),
                            exEbDemandeTransporteur.getInfosForCustomsClearance())) {
                action += " * <strike> " +
                        (oldDemandeTrans.getInfosForCustomsClearance() != null ?
                                oldDemandeTrans.getInfosForCustomsClearance() :
                                "-") +
                        "</strike> ";
                action += (exEbDemandeTransporteur.getInfosForCustomsClearance() != null ?
                        exEbDemandeTransporteur.getInfosForCustomsClearance() :
                        "-") + "<br>";
            }

            oldDemandeTrans.setExchangeRate(exEbDemandeTransporteur.getExchangeRate());
            oldDemandeTrans.setInfosForCustomsClearance(exEbDemandeTransporteur.getInfosForCustomsClearance());
            exEbDemandeTransporteurRepository.save(oldDemandeTrans);
            EbUser connectedUser = connectedUserService.getCurrentUser();

            if (!connectedUser.getEbUserNum().equals(Statiques.UNKNOWN_USER_NUM)) {
                String username = connectedUser.getRealUserNum() != null ?
                        connectedUser.getRealUserNomPrenom() :
                        connectedUser.getNomPrenom();
                String usernameFor = connectedUser.getRealUserNum() != null ? connectedUser.getNomPrenom() : null;
                Integer chatCompId = Enumeration.IDChatComponent.PRICING.getCode();
                if (moduleNum == Enumeration.Module.TRANSPORT_MANAGEMENT
                        .getCode()) chatCompId = Enumeration.IDChatComponent.TRANSPORT_MANAGEMENT.getCode();

                Integer idFiche = exEbDemandeTransporteur.getxEbDemande().getEbDemandeNum();
                EbChat ebChat = listStatiqueService
                        .historizeAction(idFiche, chatCompId, moduleNum, action, username, usernameFor);
                result.put("ebChat", ebChat);
            }

        }

        return result;
    }

    @Override
    public List<EbCostCenter> getEbCostCenterByCompagnie(Integer ebCompagnieNum) {
        return ebCostCenterRepository.findByCompagnieEbCompagnieNum(ebCompagnieNum);
    }

    @Override
    public EbChat
    confirmeAskFortrResponsibility(Integer ebDemandeNum, Integer moduleNum, String refTransport, String action) {
        EbUser user = connectedUserService.getCurrentUser();

        String username = user.getRealUserNum() != null ? user.getRealUserNomPrenom() : null;
        String usernameFor = null;
        ebDemandeRepository.confirmeAskFortrResponsibility(ebDemandeNum);

        // emailTrackAndTrace.sendEmailUpdatePsl(ebPslApp, user, ttEvent,
        // libellePsl.toString());

        Integer module;
        Integer idFiche = ebDemandeNum;
        Integer chatCompId;
        EbChat ebChat = null;

        for (int i = 0; i < 2; i++) {
            module = (i == 0) ?
                    Enumeration.Module.PRICING.getCode() :
                    Enumeration.Module.TRANSPORT_MANAGEMENT.getCode();
            chatCompId = (i == 0) ?
                    Enumeration.IDChatComponent.PRICING.getCode() :
                    Enumeration.IDChatComponent.TRANSPORT_MANAGEMENT.getCode();

            EbChat ebChata = listStatiqueService
                    .historizeAction(idFiche, chatCompId, module, action, username, usernameFor);

            if (module == moduleNum) {
                ebChat = ebChata;
            }

        }

        if (ebDemandeNum != null) {
            EbDemande demande = ebDemandeRepository.findById(ebDemandeNum).get();

            if (demande != null) {
                List<ExEbDemandeTransporteur> listQuote = exEbDemandeTransporteurRepository
                        .findAllByStatusGreaterThanEqualAndXEbDemande_ebDemandeNum(
                                StatutCarrier.FIN.getCode(),
                                ebDemandeNum);
                demande.set_exEbDemandeTransporteurFinal(listQuote.get(0));
                emailServicePricingBooking.sendEmailDemandeConfirmationOfTransportSupport(demande);
            }

        }

        return ebChat;
    }

    @Override
    public EbChat
    confirmeProvideTransport(Integer ebDemandeNum, Integer moduleNum, String RefTransport, String action) {
        EbUser user = connectedUserService.getCurrentUser();

        ebDemandeRepository.confirmeProvideTransport(ebDemandeNum);

        // emailTrackAndTrace.sendEmailUpdatePsl(ebPslApp, user, ttEvent,
        // libellePsl.toString());

        String username = user.getRealUserNum() != null ? user.getRealUserNomPrenom() : null;
        String usernameFor = null;
        Integer module;
        Integer idFiche = ebDemandeNum;
        Integer chatCompId;
        EbChat ebChat = null;

        for (int i = 0; i < 2; i++) {
            module = (i == 0) ?
                    Enumeration.Module.PRICING.getCode() :
                    Enumeration.Module.TRANSPORT_MANAGEMENT.getCode();
            chatCompId = (i == 0) ?
                    Enumeration.IDChatComponent.PRICING.getCode() :
                    Enumeration.IDChatComponent.TRANSPORT_MANAGEMENT.getCode();

            EbChat ebChata = listStatiqueService
                    .historizeAction(idFiche, chatCompId, module, action, username, usernameFor);

            if (module == moduleNum) {
                ebChat = ebChata;
            }

        }

        // envoie d'email confirmation de prise en charge
        if (ebDemandeNum != null) {
            EbDemande demande = ebDemandeRepository.findById(ebDemandeNum).orElse(null);

            if (demande != null) {
                List<ExEbDemandeTransporteur> listQuote = exEbDemandeTransporteurRepository
                        .findAllByStatusGreaterThanEqualAndXEbDemande_ebDemandeNum(
                                StatutCarrier.FIN.getCode(),
                                ebDemandeNum);
                demande.set_exEbDemandeTransporteurFinal(listQuote.get(0));
                emailServicePricingBooking.sendEmailPricingConfirmationPriseEnCharge(demande);
            }

        }

        return ebChat;
    }

    @Override
    public EbChat acknowledgeTdc(Integer ebDemandeNum) {
        EbChat ebChat = null;

        ebDemandeRepository.updateTdcAcknowledgeAndDate(ebDemandeNum);
        EbUser connectedUser = connectedUserService.getCurrentUser();
        String lang = connectedUser != null && connectedUser.getLanguage() != null ? connectedUser.getLanguage() : "en";
        Locale lcl = new Locale(lang);

        EbDemande demande = ebDemandeRepository.findById(ebDemandeNum).get();
        demande.setTdcAcknowledge(true);
        demande.setTdcAcknowledgeDate(new Date());

        ebChat = listStatiqueService
                .historizeAction(
                        demande.getEbDemandeNum(),
                        Enumeration.IDChatComponent.PRICING.getCode(),
                        Enumeration.Module.PRICING.getCode(),
                        messageSource.getMessage("demande.action.accusereception", null, lcl),
                        connectedUser.getRealUserNum() != null ?
                                connectedUser.getRealUserNomPrenom() :
                                connectedUser.getNomPrenom(),
                        connectedUser.isControlTower() || connectedUser.getRealUserNum() != null ?
                                demande.getUser().getNomPrenom() :
                                null

                );

        // envoie d'email
        emailServicePricingBooking.sendEmailQuotationaCknowledgeTdc(demande);
        return ebChat;
    }

    @Override
    public ExEbDemandeTransporteur updateTransportPlan(
            Integer ebDemandeNum,
            Integer ebPartyNum,
            String oldZoneLabel,
            Integer ebZoneNum,
            String refPlan,
            BigDecimal calculatedPrice)
            throws Exception {
        ExEbDemandeTransporteur quote = null;
        SearchCriteriaPricingBooking demandeSearchCriteria = null;

        EbPlTransport plTr = this.daoPlanTransport.getPlanTransportByRef(refPlan);

        if (plTr != null) {
            demandeSearchCriteria = new SearchCriteriaPricingBooking();
            demandeSearchCriteria.setEbDemandeNum(ebDemandeNum);
						EbDemande demande = this
							.getEbDemande(demandeSearchCriteria, connectedUserService.getCurrentUser());

            List<Integer> finalStatus = new ArrayList<>();
            finalStatus.add(Enumeration.StatutCarrier.FIN.getCode());
            finalStatus.add(Enumeration.StatutCarrier.FIN_PLAN.getCode());

            ExEbDemandeTransporteur oldQuote = this.exEbDemandeTransporteurRepository
                    .findByXEbDemande_ebDemandeNumAndStatusIn(ebDemandeNum, finalStatus);

            quote = new ExEbDemandeTransporteur();
            quote.setxTransporteur(new EbUser(plTr.getGrilleTarif().getTransporteur().getEbUserNum()));

            if (plTr.getGrilleTarif().getTransporteur().getEbEtablissement() != null) {
                quote
                        .setxEbCompagnie(
                                new EbCompagnie(
                                        plTr
                                                .getGrilleTarif().getTransporteur().getEbEtablissement().getEbCompagnie()
                                                .getEbCompagnieNum()));

                quote
                        .setxEbEtablissement(
                                new EbEtablissement(
                                        plTr.getGrilleTarif().getTransporteur().getEbEtablissement().getEbEtablissementNum()));
            }

            quote.setPrice(calculatedPrice);

            quote.setTransitTime(plTr.getTransitTime());
            quote.setRefPlan(plTr.getReference());
            quote.setComment(plTr.getComment() != null ? plTr.getComment() : "");
            quote.setIsFromTransPlan(true);
            quote.setExchangeRateFound(plTr.getGrilleTarif().getTransporteur().getExchangeRateFound());
            quote.setStatus(Enumeration.StatutCarrier.FIN_PLAN.getCode());

            quote.setxEbDemande(new EbDemande(ebDemandeNum));

            if (oldQuote != null) {
                quote.setCityPickup(oldQuote.getCityPickup());
                quote.setxEcCountryPickup(oldQuote.getxEcCountryPickup());
                quote.setCityDelivery(oldQuote.getCityDelivery());
                quote.setxEcCountryDelivery(oldQuote.getxEcCountryDelivery());
            }

            // update zone
            this.ebPartyRepository.updateEbPartyZone(ebPartyNum, new EbPlZone(ebZoneNum));
            // cancel old quotes
            this.exEbDemandeTransporteurRepository
                    .cancelQuoteByDemandeNum(Enumeration.StatutCarrier.TRANSPORT_PLAN.getCode(), ebDemandeNum);
            // save new one
            quote = this.exEbDemandeTransporteurRepository.save(quote);

            this.ebDemandeRepository
                    .updateDetailsTransporteurFinal(
                            ebDemandeNum,
                            plTr.getGrilleTarif().getTransporteur().getNom(),
                            plTr.getGrilleTarif().getTransporteur().getEbUserNum(),
                            plTr.getGrilleTarif().getTransporteur().getEbEtablissement().getEbEtablissementNum());

            if (demande.getXecCurrencyInvoice() != null) this
                    .addUpdateTrPlanHistory(
                            ebZoneNum,
                            ebDemandeNum,
                            oldZoneLabel,
                            calculatedPrice,
                            demande.getXecCurrencyInvoice().getCode()); // adding
            // transaction
            // history
            return quote;
        }

        return null;
    }

    private void addUpdateTrPlanHistory(
            Integer ebZoneNum,
            Integer ebDemandeNum,
            String oldZoneLabel,
            BigDecimal calculatedPrice,
            String devise) {
        String action = null;
        EbPlZone newZone = this.ebZoneRepository.findById(ebZoneNum).get();

        String newZoneLabel = newZone.getDesignation() != null ?
                newZone.getRef() + "(" + newZone.getDesignation() + ")" :
                newZone.getRef();
        EbUser connectedUser = connectedUserService.getCurrentUser();

        action = "Shipment description updated: <br>";
        action += "Zone from / Zone To: -" + oldZoneLabel + " -" + newZoneLabel + "<br>";
        action += "Total Cost: " + calculatedPrice + " " + devise + "<br>";

        this.listStatiqueService
                .historizeAction(
                        ebDemandeNum,
                        Enumeration.IDChatComponent.TRANSPORT_MANAGEMENT.getCode(),
                        Enumeration.Module.TRANSPORT_MANAGEMENT.getCode(),
                        action,
                        connectedUser.getRealUserNum() != null ?
                                connectedUser.getRealUserNomPrenom() :
                                connectedUser.getNomPrenom(),
                        connectedUser.getDelegatedUsername());
    }

    @Override
    public Set<String> getFieldsByCompagnieNumAndTerm(String term, Integer ebCompagnieNum) {
        List<String> listFields = ebDemandeRepository.selectFieldsByCompagnieNumAndTerm(ebCompagnieNum, term);
        Set<String> listFieldsFinal = new HashSet<>();

        for (String field : listFields) {
            String[] list = field.split(";");

            listFieldsFinal.add(getValueFromCustomField(term, field, list));
        }

        return listFieldsFinal;
    }

    private String getValueFromCustomField(String term, String field, String[] list) {

        for (String str_ : list) {

            if (str_.contains(term)) {
                String[] list_ = str_.split(":");
                field = list_[1];
                field = field.replace("\"", "");
                break;
            }

        }

        return field;
    }

    @Override
    public List<EbDemande> ListDemandeFromTransfertUserObject(TransfertUserObject transfertUserObjet, Integer module)
            throws Exception {
        SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();

        EbEtablissement etablissement = ebEtablissementRepository
                .findOneByEbEtablissementNum(transfertUserObjet.getOldEtablissement());

        EbUser currentUser = userService.getEbUserByEbUserNum(transfertUserObjet.getEbUserNum());
        List<EbCategorie> listCategories = new ArrayList<EbCategorie>();

        for (EbCategorie categorie : transfertUserObjet.getOldListCategories()) {
            listCategories.add(categorie);
        }

        currentUser.setEbEtablissement(etablissement);
        currentUser.setListCategories(listCategories);
        currentUser.setListLabels(transfertUserObjet.getOldListLabels());

        if (transfertUserObjet.getTransportFiles().getAction() == TransfertActionChoice.CHOOSE.getCode()) {
            criteria.setListEbDemandeNum(transfertUserObjet.getTransportFiles().getListId());
        }

        criteria.setModule(module);
        List<EbDemande> listDemande = daoPricing.getListEbDemande(criteria, currentUser);
        return listDemande;
    }

    public EbDemande prepareQRForValorisation(Integer ebDemandeNum) {
        EbDemande demande = ebDemandeRepository.findOneByEbDemandeNum(ebDemandeNum);

        if (demande.getCustomFields() != null && !demande.getCustomFields().isEmpty()) {
            Gson gson = new Gson();
            demande
                    .setListCustomsFields(
                            gson.fromJson(demande.getCustomFields(), new TypeToken<ArrayList<CustomFields>>() {
                            }.getType()));
        }

        List<Integer> demNums = new ArrayList<Integer>();
        demNums.add(ebDemandeNum);
        demande.setListMarchandises(ebMarchandiseRepository.selectListEbMarchandiseByEbDemandeNumIn(demNums));
        return demande;
    }

		@Override
		public EbDemande unitValuation(EbDemande ebDemande)
		{

			if (
				ebDemande.getExEbDemandeTransporteurs() != null &&
					!ebDemande.getExEbDemandeTransporteurs().isEmpty()
			)
			{

				intializeParamsBeforeValorizeTrForNotPendingTr(ebDemande);

				List<PrMtcTransporteur> listMtcTransporteur = daoPrMtcTransporteur
					.getListPrMtcTransporteur(new SearchCriteria());

				ebDemande
					.getExEbDemandeTransporteurs()
					.stream()
					.filter(quote -> !hasManualQuote(quote))
					.forEach(quote -> {
						EbCompagnie carrierComp = quote.getxEbCompagnie() != null &&
							quote.getxEbCompagnie().getCode() != null ?
								quote.getxEbCompagnie() :
								quote.getxEbEtablissement() != null &&
									quote.getxEbEtablissement().getEbCompagnie() != null &&
									quote.getxEbEtablissement().getEbCompagnie().getCode() != null ?
										quote.getxEbEtablissement().getEbCompagnie() :
										null;

						PrMtcTransporteur mapTrp = null;

						if (quote.getxTransporteur() != null)
						{
							Optional<EbUser> trans = ebUserRepository
								.findById(quote.getxTransporteur().getEbUserNum());
							if (trans.isPresent())
								carrierComp = trans.get().getEbCompagnie();
						}

						if (carrierComp != null)
						{
							quote.setxEbCompagnie(carrierComp);

							if (ebDemande.getCodeConfigurationEDI() != null)
							{
									mapTrp = listMtcTransporteur.stream()
											.filter(pr -> isQuoteTransporteurMappedToMTC(ebDemande, pr, quote))
											.findFirst().orElse(null);
							}
							else
							{
								mapTrp = listMtcTransporteur
									.stream()
									.filter(
										pr -> pr
											.getxEbCompagnieTransporteur()
											.getEbCompagnieNum()
											.equals(quote.getxEbCompagnie().getEbCompagnieNum())
									)
									.findFirst()
									.orElse(null);
							}

							try
							{

								if (ebDemande.getxEbCompagnie() == null)
								{
									EbCompagnie compagnie = ebCompagnieRepository
										.selectCompagnieByEbUserNum(ebDemande.getUser().getEbUserNum());
									if (compagnie != null)
										ebDemande.setxEbCompagnie(compagnie);
								}

								if (mapTrp != null && StringUtils.isNotBlank(mapTrp.getCodeConfigurationMtc()))
								{

									setQuoteAsNotValorized(quote);
									List<PricingCarrierSearchResultDTO> pricingCarrierSearchResultDTOList = mtcService
										.manageResult(
											mtcService.valuationFromMTC(ebDemande, mapTrp.getCodeConfigurationMtc()),
											ebDemande,
											quote
										);

									if (
										pricingCarrierSearchResultDTOList != null &&
											!pricingCarrierSearchResultDTOList.isEmpty()
									)
									{
										// MTC renvoie une seule proposition
										// par (demande,transporteur)
										final PricingCarrierSearchResultDTO result = pricingCarrierSearchResultDTOList
											.get(0);

										// check valorization demande from
										// MTC
										if (
											ebDemande.getxEcStatut() >= Enumeration.StatutDemande.FIN.getCode() &&
												ebDemande.getFinalChoiceToBeCalculated() == true
										)
										{
											ebDemande
												.setxEcStatut(
													Enumeration.StatutDemande.WPU.getCode()
												);/**
													 * in this case quote.setStatus is already = FIN_PLAN
													 */
											ebDemande.setConfirmed(new Date());
										}
										else
										{

											if (
												!ebDemande
													.getxEcNature()
													.equals(Enumeration.NatureDemandeTransport.CONSOLIDATION.getCode())
											)
											{
												ebDemande.setxEcStatut(Enumeration.StatutDemande.REC.getCode());
												ebDemande.setWaitingForConf(new Date());
											}

											quote.setStatus(Enumeration.StatutCarrier.TRANSPORT_PLAN.getCode());
										}

										quote.setPrice(result.getCalculatedPrice());
										quote.setIsFromTransPlan(true);
										quote.setListPlCostItem(result.getListPlCostItem());
										ebDemande.setValorized(true);
										ebDemande.setDateMaj(new Date());

										/**
										 * mettre à jour le statut de
										 * groupage et tous les champs
										 * required sont renseignés
										 */
										if (
											ebDemande.getxEcStatutGroupage() != null &&
												ConsolidationGroupage.NP
													.getCode()
													.equals(ebDemande.getxEcStatutGroupage()) &&
												checkRequiredChamps(ebDemande)
										)
										{
											ebDemande.setxEcStatutGroupage(ConsolidationGroupage.PRS.getCode());

											/**
											 * mettre à jour le
											 * statut de groupage et
											 * tous les champs required sont renseignés
											 */
											if (
												ebDemande.getxEcStatutGroupage() != null &&
													ConsolidationGroupage.NP
														.getCode()
														.equals(ebDemande.getxEcStatutGroupage()) &&
													checkRequiredChamps(ebDemande)
											)
											{
												ebDemande.setxEcStatutGroupage(ConsolidationGroupage.PRS.getCode());
											}

										}
									}

								}

							}
							catch (JsonProcessingException e)
							{
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					});
			}

			return ebDemande;
		}

		private boolean isQuoteTransporteurMappedToMTC(EbDemande ebDemande, PrMtcTransporteur pr, ExEbDemandeTransporteur quote) {
			return ebDemande.getCodeConfigurationEDI().equalsIgnoreCase(pr.getCodeConfigurationEDI())
					&& quote.getxTransporteur().getEmail().equals(pr.getxEbUser().getEmail());
		}

		private boolean hasManualQuote(ExEbDemandeTransporteur demandeTransporteur)
		{
			return demandeTransporteur.getTransitTime() != null &&
				demandeTransporteur.getPrice() != null &&
				!Enumeration.StatutCarrier.TRANSPORT_PLAN.getCode().equals(demandeTransporteur.getStatus());
		}

		private boolean noneIsManualQuote(List<ExEbDemandeTransporteur> quotes){
		    return quotes.stream().noneMatch(this::hasManualQuote);
        }

		private void setQuoteAsNotValorized(ExEbDemandeTransporteur quote)
		{
			quote.setPrice(null);
			quote.setIsFromTransPlan(false);
			quote.setListPlCostItem(null);
			quote.setStatus(Enumeration.StatutCarrier.RRE.getCode());
		}

		/**
		 * Pour les cas des TransportRequests (TRs) non groupés, le statut est changé
		 * vers "In Process Waiting For Quote" (INP) et la valorisation est annulé
		 */
	private void intializeParamsBeforeValorizeTrForNotPendingTr(EbDemande transportRequest) {
		if (!StatutDemande.PND.getCode().equals(transportRequest.getxEcStatut()) && noneIsManualQuote(transportRequest.getExEbDemandeTransporteurs())) {
			transportRequest.setxEcStatut(StatutDemande.INP.getCode());
			transportRequest.setValorized(false);
		}
	}

    boolean checkRequiredChamps(EbDemande demande) {
        return demande.getUser() != null &&
                demande.getUser().getEbUserNum() != null && demande.getEbPartyOrigin() != null &&
                demande.getEbPartyOrigin().getxEcCountry() != null && demande.getEbPartyDest() != null &&
                demande.getEbPartyDest().getxEcCountry() != null && demande.getxEcModeTransport() != null &&
                demande.getxEbSchemaPsl() != null && demande.getxEbSchemaPsl().getEbTtSchemaPslNum() != null &&
                demande.getDateOfGoodsAvailability() != null && demande.getxEbTypeRequest() != null &&
                demande.getXecCurrencyInvoice() != null && demande.getXecCurrencyInvoice().getEcCurrencyNum() != null &&
                demande.getListMarchandises() != null && !demande.getListMarchandises().isEmpty();
    }

    public void
    updatexEcStatutGroupageInEbDemandeByEbDemandeNum(List<Integer> listDemandeNum, Integer xEcStatutGroupage) {
        ebDemandeRepository.updatexEcStatutGroupageInEbDemandeByEbDemandeNum(listDemandeNum, xEcStatutGroupage);
    }

    public EbDemande
    updatelistAdditionalCostInEbDemandeByEbDemandeNum(Integer ebDemandeNum, List<EbCost> additionalCost) {
        EbDemande ebdemand = ebDemandeRepository.findById(ebDemandeNum).get();

        if (additionalCost != null && additionalCost.isEmpty()) {
            additionalCost = null;
        }

        ebdemand.setListAdditionalCost(additionalCost);
        ebDemandeRepository.save(ebdemand);

        // Send kafka object
        try {
            kafkaObjectService
                    .buildAndSendTransportRequestToKafka(ebdemand.getEbDemandeNum(), ebdemand.getUser(), false);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ebdemand;
    }

    @Override
    public List<String> getListIncotermCity(Integer ebCompagnieNum) {
        return ebDemandeRepository.selectListIncotermCityByUserebCompagnieNum(ebCompagnieNum);
    }

    @Override
    public List<EbDemande> getListEbDemandeInit(SearchCriteriaPricingBooking criterias) {
        List<EbDemande> ebDemandes = daoPricing.getListEbDemandeInitials(criterias);
        ebDemandes.forEach(t -> {
            List<ExEbDemandeTransporteur> demandeTransporteurs = exEbDemandeTransporteurRepository
                    .findAllByXEbDemande_ebDemandeNum(t.getEbDemandeNum());
            demandeTransporteurs.forEach(k -> {
                k.setxEbCompagnie(null);
            });
            t.setExEbDemandeTransporteurs(demandeTransporteurs);
            t.setListMarchandises(ebMarchandiseRepository.findAllByEbDemande_ebDemandeNum(t.getEbDemandeNum()));
        });
        return ebDemandes;
    }

    public List<EbQrGroupe> getListQrGroupe(SearchCriteria criteria, boolean active) {
        return daoPricing.getListQrGroupe(criteria, active);
    }

    @Override
    public EbDemande saveGeneratedEbDemande(EbDemande demande, EbUser connectedUser) {
        connectedUser = connectedUser == null ? connectedUserService.getCurrentUser() : connectedUser;
        String codeCompany = connectedUser.getEbCompagnie().getCode();
        String refPrefix = pricingAnnexMethodsService.getReferenceNaturePrefix(NatureDemandeTransport.NORMAL);
        String refTransport = null;

        List<EbMarchandise> units = demande.getListMarchandises();
        EbParty originParty = demande.getEbPartyOrigin();
        EbParty destParty = demande.getEbPartyDest();

        if (originParty != null) {
            demande.setLibelleOriginCity(originParty.getCity());
            demande
                    .setLibelleOriginCountry(
                            originParty.getxEcCountry() != null ? originParty.getxEcCountry().getLibelle() : null);
            originParty = this.ebPartyRepository.save(originParty);
            demande.setEbPartyOrigin(new EbParty(originParty.getEbPartyNum()));
        }

        if (destParty != null) {
            demande.setLibelleDestCity(destParty.getCity());
            demande
                    .setLibelleDestCountry(
                            destParty.getxEcCountry() != null ? destParty.getxEcCountry().getLibelle() : null);
            destParty = this.ebPartyRepository.save(destParty);
            demande.setEbPartyDest(new EbParty(destParty.getEbPartyNum()));
        }

        demande.setListMarchandises(null);

        demande.setEbDemandeNum(daoPricing.nextValEbDemande());
        demande = this.ebDemandeRepository.save(demande);

        if (units != null && !units.isEmpty()) // saving units
        {
            List<String> unitsReferences = units
                    .stream().filter(u -> u.getUnitReference() != null).map(u -> u.getUnitReference())
                    .collect(Collectors.toList());

            demande.setUnitsReference(StringUtils.join(unitsReferences, ","));

            List<String> unitsPackingList = units
                    .stream().filter(u -> u.getPackingList() != null).map(u -> u.getPackingList())
                    .collect(Collectors.toList());
            demande.setUnitsPackingList(StringUtils.join(unitsPackingList, ","));

            for (EbMarchandise ebMarchandise : units) {
                ebMarchandise.setEbMarchandiseNum(null);
                ebMarchandise.setEbDemande(new EbDemande(demande.getEbDemandeNum()));
            }

            this.ebMarchandiseRepository.saveAll(units);
        }

        refTransport = StringUtils.leftPad(demande.getEbDemandeNum().toString().toUpperCase(), 5, "0");
        demande.setRefTransport(codeCompany.concat("-").concat(refPrefix).concat(refTransport));

        demande = this.ebDemandeRepository.save(demande); // update with refTr
        // and unitsref

        demande.setListMarchandises(units);
        demande.setEbPartyOrigin(originParty);
        demande.setEbPartyDest(destParty);
        return demande;
    }

    @Override
    public EbParty cloneEbParty(EbParty party) { // add missing fields
        if (party == null || party.getEbPartyNum() == null) return null;

        EbParty p = new EbParty();
        p.setAdresse(party.getAdresse());
        p.setCompany(party.getCompany());
        p.setZipCode(party.getZipCode());
        p.setCity(party.getCity());
        p.setAirport(party.getAirport());
        p.setPhone(party.getPhone());
        p.setOpeningHours(party.getOpeningHours());
        p.setReference(party.getReference());

        if (party.getxEcCountry() != null && party.getxEcCountry().getEcCountryNum() != null) {
            EcCountry xEcCountry = new EcCountry(party.getxEcCountry().getEcCountryNum());
            xEcCountry.setLibelle(party.getxEcCountry().getLibelle());
            p.setxEcCountry(xEcCountry);
        }

        if (party.getZone() != null &&
                party.getZone().getEbZoneNum() != null) p.setZone(new EbPlZone(party.getZone().getEbZoneNum()));
        if (party.getEtablissementAdresse() != null &&
                party.getEtablissementAdresse().getEbEtablissementNum() != null) p
                .setEtablissementAdresse(new EbEtablissement(party.getEtablissementAdresse().getEbEtablissementNum()));

        return p;
    }

    private Integer getExportControlStatusFromUnits(List<EbMarchandise> units) // change
    // to
    // number
    // return
    // type
    {

        if (units != null && !units.isEmpty()) {
            if (this.isUnitExistWithExportControlStatus(units, ExportControlEnum.LICENCE_NOK.getCode())) // units with
                // at
                // less
                // exportControlStatut
                // =
                // LICENCE_NOK
                return ExportControlEnum.LICENCE_NOK.getCode();
            if (this.isAllUnitWithExportControlStatut(units, ExportControlEnum.ACE.getCode())) // all units with
                // exportControlStatut =
                // ACE
                return ExportControlEnum.ACE.getCode();
            if (this.isAllUnitWithExportControlStatut(units, ExportControlEnum.ALD.getCode())) // all units with
                // exportControlStatut =
                // ALD
                return ExportControlEnum.ALD.getCode();
            if ( // units with exportControlStatut = ALD and LICENCE_NOK
                    this
                            .foundUnitsWithExportControlValues(
                                    units,
                                    ExportControlEnum.LICENCE_OK.getCode(),
                                    ExportControlEnum.ALD.getCode())) return ExportControlEnum.LICENCE_OK.getCode();
            if (this.isAllUnitWithExportControlStatut(units, units.get(0).getExportControlStatut())) // same export
                // control status
                return units.get(0).getExportControlStatut();
        }

        return null;
    }

    private boolean isUnitExistWithExportControlStatus(List<EbMarchandise> units, Integer exportControlStatut) {
        EbMarchandise unit = units
                .parallelStream()
                .filter(
                        un -> un.getExportControlStatut() != null && un.getExportControlStatut().equals(exportControlStatut))
                .findFirst().orElse(null);
        return unit != null;
    }

    private boolean isAllUnitWithExportControlStatut(List<EbMarchandise> units, Integer exportControlStatut) {
        List<EbMarchandise> unitsWithExportControlStatut = units
                .parallelStream()
                .filter(
                        un -> un.getExportControlStatut() != null && un.getExportControlStatut().equals(exportControlStatut))
                .collect(Collectors.toList());
        return unitsWithExportControlStatut != null && (unitsWithExportControlStatut.size() == units.size());
    }

    private boolean foundUnitsWithExportControlValues(
            List<EbMarchandise> units,
            Integer firstExportControlStatut,
            Integer secondExportControlStatut) {
        EbMarchandise unitWithFirstECS = units
                .parallelStream()
                .filter(
                        un -> un.getExportControlStatut() != null &&
                                un.getExportControlStatut().equals(firstExportControlStatut))
                .findFirst().orElse(null);
        EbMarchandise unitWithSecondECS = units
                .parallelStream()
                .filter(
                        un -> un.getExportControlStatut() != null &&
                                un.getExportControlStatut().equals(secondExportControlStatut))
                .findFirst().orElse(null);
        return unitWithFirstECS != null && unitWithSecondECS != null;
    }

    private String getExportControlStatutTranslatedByCode(Integer code, String language) {
        Locale local = new Locale(language);

        if (code.equals(ExportControlEnum.ACE.getCode())) {
            return messageSource.getMessage("exportControlStatut.ace", null, local);
        }

        if (code.equals(ExportControlEnum.LICENCE_OK.getCode())) {
            return messageSource.getMessage("exportControlStatut.licence_ok", null, local);
        }

        if (code.equals(ExportControlEnum.LICENCE_NOK.getCode())) {
            return messageSource.getMessage("exportControlStatut.licence_nok", null, local);
        }

        if (code.equals(ExportControlEnum.ALD.getCode())) {
            return messageSource.getMessage("exportControlStatut.ald", null, local);
        }

        return null;
    }

    @Override
    public List<EbUser> getAllUsersIntervenantInDemande(Integer ebDemandeNum) {
        return this.daoPricing.getAllUsersIntervenantInDemande(ebDemandeNum);
    }

    public Map<String, Object> updateListMarchandise(
            List<EbMarchandise> listMarchandise,
            Integer ebDemandeNum,
            Double totalTaxableWeight,
            Double totalWeight,
            Double totalVolume) {
        Map<String, Object> result = new HashMap<String, Object>();
        String action = "";
        List<EbMarchandise> oldListMarchandise = new ArrayList<>();
        oldListMarchandise = ebMarchandiseRepository.findAllByEbDemande_ebDemandeNum(ebDemandeNum);
        EbUser connectedUser = connectedUserService.getCurrentUser();

        String lang = connectedUser != null && connectedUser.getLanguage() != null ? connectedUser.getLanguage() : "en";

        // mise à jour des nouvelles valeurs des marchandises
        for (EbMarchandise oldEbMarchandise : oldListMarchandise) {

            for (EbMarchandise newEbMarchandise : listMarchandise) {
                boolean areEqualUnits = false;
                if (oldEbMarchandise.getCustomerReference() != null && // Some
                        // Generated
                        // TR
                        // pending
                        // files
                        // hase
                        // units
                        // without
                        // customer
                        // ref ->
                        // NPE
                        newEbMarchandise.getCustomerReference() != null) areEqualUnits = oldEbMarchandise
                        .getCustomerReference().equals(newEbMarchandise.getCustomerReference());
                else if (oldEbMarchandise.getEbMarchandiseNum() != null &&
                        newEbMarchandise.getEbMarchandiseNum() != null) areEqualUnits = oldEbMarchandise
                        .getEbMarchandiseNum().equals(newEbMarchandise.getEbMarchandiseNum());

                if (areEqualUnits) {

                    if ((newEbMarchandise.getxEbTypeUnit() != null && oldEbMarchandise.getxEbTypeUnit() == null) ||
                            (newEbMarchandise.getxEbTypeUnit() != null &&
                                    !oldEbMarchandise.getxEbTypeUnit().equals(newEbMarchandise.getxEbTypeUnit()))) {
                        // chercher les type of unit
                        SearchCriteriaTypeUnit criteriaTypeUnit = new SearchCriteriaTypeUnit();
                        criteriaTypeUnit.setSearchterm("true");
                        criteriaTypeUnit.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
                        List<EbTypeMarchandiseDto> resultList = mapper
                                .ebTypeUnitsToEbTypesMarchandise(daoTypeUnit.getListTypeUnit(criteriaTypeUnit));

                        if (action
                                .indexOf(

                                        "Part Number"

                                ) < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        String newUnitType = "";

                        if (newEbMarchandise.getxEbTypeUnit() != null) {
                            EbTypeMarchandiseDto newUnitTypeMarchandise = resultList
                                    .stream()
                                    .filter(typeUnit -> typeUnit.getxEbTypeUnit().equals(newEbMarchandise.getxEbTypeUnit()))
                                    .findFirst().orElse(null);

                            if (newUnitTypeMarchandise != null) {
                                newUnitType = newUnitTypeMarchandise.getLabel();
                            }

                        }

                        String oldUnitType = "";

                        if (oldEbMarchandise.getxEbTypeUnit() != null) {
                            EbTypeMarchandiseDto oldUnitTypeMarchandise = resultList
                                    .stream()
                                    .filter(typeUnit -> typeUnit.getxEbTypeUnit() == oldEbMarchandise.getxEbTypeUnit())
                                    .findFirst().orElse(null);

                            if (oldUnitTypeMarchandise != null) {
                                oldUnitType = oldUnitTypeMarchandise.getLabel();
                            }

                        }

                        action += "<table class='width-100-percent margin-top-5'><tr>" +
                                "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                oldEbMarchandise.getPartNumber() + "</div>" + "<div class='padding-4'>Unit Reference : " +
                                oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                        oldEbMarchandise.setxEbTypeUnit(newEbMarchandise.getxEbTypeUnit());
                    }

                    if ((newEbMarchandise.getUnitReference() != null && oldEbMarchandise.getUnitReference() == null) ||
                            (newEbMarchandise.getUnitReference() != null &&
                                    !oldEbMarchandise.getUnitReference().equals(newEbMarchandise.getUnitReference()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "Unit Reference :" + "</td>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newEbMarchandise.getUnitReference() + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldEbMarchandise.getUnitReference() + "</strike>" + "</td>" + "</tr>";

                        oldEbMarchandise.setUnitReference(newEbMarchandise.getUnitReference());
                    }

                    if ((newEbMarchandise.getPartNumber() != null && oldEbMarchandise.getPartNumber() == null) ||
                            (newEbMarchandise.getPartNumber() != null &&
                                    !oldEbMarchandise.getPartNumber().equals(newEbMarchandise.getPartNumber()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "Part Number :" + "</td>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newEbMarchandise.getPartNumber() + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldEbMarchandise.getPartNumber() + "</strike>" + "</td>" + "</tr>";

                        oldEbMarchandise.setPartNumber(newEbMarchandise.getPartNumber());
                    }

                    if ((newEbMarchandise.getSerialNumber() != null && oldEbMarchandise.getSerialNumber() == null) ||
                            (newEbMarchandise.getSerialNumber() != null &&
                                    !oldEbMarchandise.getSerialNumber().equals(newEbMarchandise.getSerialNumber()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "Serial Number :" + "</td>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newEbMarchandise.getSerialNumber() + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldEbMarchandise.getSerialNumber() + "</strike>" + "</td>" + "</tr>";

                        oldEbMarchandise.setSerialNumber(newEbMarchandise.getSerialNumber());
                    }

                    if ((newEbMarchandise.getPackingList() != null && oldEbMarchandise.getPackingList() == null) ||
                            (newEbMarchandise.getPackingList() != null &&
                                    !oldEbMarchandise.getPackingList().equals(newEbMarchandise.getPackingList()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "Packing List :" + "</td>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newEbMarchandise.getPackingList() + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldEbMarchandise.getPackingList() + "</strike>" + "</td>" + "</tr>";

                        oldEbMarchandise.setPackingList(newEbMarchandise.getPackingList());
                    }

                    if ((newEbMarchandise.getWeight() != null && oldEbMarchandise.getWeight() == null) ||
                            (newEbMarchandise.getWeight() != null &&
                                    !oldEbMarchandise.getWeight().equals(newEbMarchandise.getWeight()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        Double newWeight = newEbMarchandise.getWeight() != null ? newEbMarchandise.getWeight() : 0;
                        Double oldWeight = oldEbMarchandise.getWeight() != null ? oldEbMarchandise.getWeight() : 0;
                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" + "Weight :" +
                                "</td>" + "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                +newWeight + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldWeight + "</strike>" + "</td>" + "</tr>";
                        oldEbMarchandise.setWeight(newEbMarchandise.getWeight());
                    }

                    if ((newEbMarchandise.getWidth() != null && oldEbMarchandise.getWidth() == null) ||
                            (newEbMarchandise.getWidth() != null &&
                                    !oldEbMarchandise.getWidth().equals(newEbMarchandise.getWidth()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        Double newWidth = newEbMarchandise.getWidth() != null ? newEbMarchandise.getWidth() : 0;
                        Double oldWidth = oldEbMarchandise.getWidth() != null ? oldEbMarchandise.getWidth() : 0;
                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" + "Width :" +
                                "</td>" + "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newWidth + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldWidth + "</strike>" + "</td>" + "</tr>";
                        oldEbMarchandise.setWidth(newEbMarchandise.getWidth());
                    }

                    if ((newEbMarchandise.getHeigth() != null && oldEbMarchandise.getHeigth() == null) ||
                            (newEbMarchandise.getHeigth() != null &&
                                    !oldEbMarchandise.getHeigth().equals(newEbMarchandise.getHeigth()))) {

                        if (action
                                .indexOf(
                                        "<table class='width-100-percent margin-top-5'><tr>" +
                                                "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                                "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                                oldEbMarchandise.getPartNumber() + "</div>" +
                                                "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                                "</div>" + "</th></tr>")
                                < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        Double newHeigth = newEbMarchandise.getHeigth() != null ? newEbMarchandise.getHeigth() : 0;
                        Double oldHeigth = oldEbMarchandise.getHeigth() != null ? oldEbMarchandise.getHeigth() : 0;
                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" + "Height :" +
                                "</td>" + "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newHeigth + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldHeigth + "</strike>" + "</td>" + "</tr>";
                        oldEbMarchandise.setHeigth(newEbMarchandise.getHeigth());
                    }

                    if ((newEbMarchandise.getLength() != null && oldEbMarchandise.getLength() == null) ||
                            (newEbMarchandise.getLength() != null &&
                                    !oldEbMarchandise.getLength().equals(newEbMarchandise.getLength()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        Double newLength = newEbMarchandise.getLength() != null ? newEbMarchandise.getLength() : 0;
                        Double oldLength = oldEbMarchandise.getLength() != null ? oldEbMarchandise.getLength() : 0;
                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" + "Length :" +
                                "</td>" + "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newLength + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldLength + "</strike>" + "</td>" + "</tr>";
                        oldEbMarchandise.setLength(newEbMarchandise.getLength());
                    }

                    // Comment field
                    if ((newEbMarchandise.getComment() != null && oldEbMarchandise.getComment() == null) ||
                            (newEbMarchandise.getComment() != null &&
                                    !oldEbMarchandise.getComment().equals(newEbMarchandise.getComment()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        String newComment = newEbMarchandise.getComment() != null ? newEbMarchandise.getComment() : "";
                        String oldComment = oldEbMarchandise.getComment() != null ? oldEbMarchandise.getComment() : "";
                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" + "Length :" +
                                "</td>" + "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newComment + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldComment + "</strike>" + "</td>" + "</tr>";
                        oldEbMarchandise.setComment(newEbMarchandise.getComment());
                    }

                    // Item Name field
                    if ((newEbMarchandise.getItemName() != null && oldEbMarchandise.getItemName() == null) ||
                            (newEbMarchandise.getItemName() != null &&
                                    !oldEbMarchandise.getItemName().equals(newEbMarchandise.getItemName()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        String newName = newEbMarchandise.getItemName() != null ? newEbMarchandise.getItemName() : "";
                        String oldName = oldEbMarchandise.getItemName() != null ? oldEbMarchandise.getItemName() : "";
                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" + "Length :" +
                                "</td>" + "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newName + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldName + "</strike>" + "</td>" + "</tr>";
                        oldEbMarchandise.setItemName(newEbMarchandise.getItemName());
                    }

                    // Origin Country field
                    if ((newEbMarchandise.getOriginCountryName() != null &&
                            oldEbMarchandise.getOriginCountryName() == null) ||
                            (newEbMarchandise.getOriginCountryName() != null &&
                                    !oldEbMarchandise.getOriginCountryName().equals(newEbMarchandise.getOriginCountryName()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        String newName = newEbMarchandise.getOriginCountryName() != null ?
                                newEbMarchandise.getOriginCountryName() :
                                "";
                        String oldName = oldEbMarchandise.getOriginCountryName() != null ?
                                oldEbMarchandise.getOriginCountryName() :
                                "";
                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" + "Length :" +
                                "</td>" + "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newName + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldName + "</strike>" + "</td>" + "</tr>";
                        oldEbMarchandise.setOriginCountryName(newEbMarchandise.getOriginCountryName());
                    }

                    // Hs Code field
                    if ((newEbMarchandise.getHsCode() != null && oldEbMarchandise.getHsCode() == null) ||
                            (newEbMarchandise.getHsCode() != null &&
                                    !oldEbMarchandise.getHsCode().equals(newEbMarchandise.getHsCode()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        String newCode = newEbMarchandise.getHsCode() != null ? newEbMarchandise.getHsCode() : "";
                        String oldCode = oldEbMarchandise.getHsCode() != null ? oldEbMarchandise.getHsCode() : "";
                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" + "Length :" +
                                "</td>" + "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newCode + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldCode + "</strike>" + "</td>" + "</tr>";
                        oldEbMarchandise.setHsCode(newEbMarchandise.getHsCode());
                    }

                    // Price field
                    if ((newEbMarchandise.getPrice() != null && oldEbMarchandise.getPrice() == null) ||
                            (newEbMarchandise.getPrice() != null &&
                                    !oldEbMarchandise.getPrice().equals(newEbMarchandise.getPrice()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        Double newPrice = newEbMarchandise.getPrice() != null ? newEbMarchandise.getPrice() : 0;
                        Double oldPrice = oldEbMarchandise.getPrice() != null ? oldEbMarchandise.getPrice() : 0;
                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" + "Length :" +
                                "</td>" + "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newPrice + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldPrice + "</strike>" + "</td>" + "</tr>";
                        oldEbMarchandise.setPrice(newEbMarchandise.getPrice());
                    }

                    if ((newEbMarchandise.getVolume() != null && oldEbMarchandise.getVolume() == null) ||
                            (newEbMarchandise.getVolume() != null &&
                                    !oldEbMarchandise.getVolume().equals(newEbMarchandise.getVolume()))) {

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    oldEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        Double newVolume = newEbMarchandise.getVolume() != null ? newEbMarchandise.getVolume() : 0;
                        Double oldVolume = oldEbMarchandise.getVolume() != null ? oldEbMarchandise.getVolume() : 0;
                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" + "Volume :" +
                                "</td>" + "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newVolume + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldVolume + "</strike>" + "</td>" + "</tr>";
                        oldEbMarchandise.setVolume(newEbMarchandise.getVolume());
                    }

                    if ((newEbMarchandise.getExportControlStatut() != null &&
                            oldEbMarchandise.getExportControlStatut() == null) ||
                            (newEbMarchandise.getExportControlStatut() != null &&
                                    !oldEbMarchandise
                                            .getExportControlStatut().equals(newEbMarchandise.getExportControlStatut()))) {
                        String oldExportControlStatut = oldEbMarchandise.getExportControlStatut() != null ?
                                this
                                        .getExportControlStatutTranslatedByCode(
                                                oldEbMarchandise.getExportControlStatut(),
                                                lang) :
                                null;

                        if (action.indexOf("Part Number") < 0) {
                            action += "<table class='width-100-percent margin-top-5'><tr>" +
                                    "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                    "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                    newEbMarchandise.getPartNumber() + "</div>" +
                                    "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                    "</div>" + "</th></tr>";
                        }

                        String newExportControlStatut = newEbMarchandise.getExportControlStatut() != null ?
                                this
                                        .getExportControlStatutTranslatedByCode(
                                                newEbMarchandise.getExportControlStatut(),
                                                lang) :
                                null;

                        action += "<tr>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "Export control statut :" + "</td>" +
                                "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                newExportControlStatut + "</td>" +
                                "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                "<strike> " + oldExportControlStatut + "</strike>" + "</td>" + "</tr>";

                        oldEbMarchandise.setExportControlStatut(newEbMarchandise.getExportControlStatut());
                    }

                    if ((newEbMarchandise.getDangerousGood() != null && oldEbMarchandise.getDangerousGood() == null) ||
                            (newEbMarchandise.getDangerousGood() != null &&
                                    !oldEbMarchandise.getDangerousGood().equals(newEbMarchandise.getDangerousGood()))) {
                        oldEbMarchandise.setDangerousGood(newEbMarchandise.getDangerousGood());
                    }

                    if ((newEbMarchandise.getStackable() != null && oldEbMarchandise.getStackable() == null) ||
                            (newEbMarchandise.getStackable() != null &&
                                    !oldEbMarchandise.getStackable().equals(newEbMarchandise.getStackable()))) {
                        oldEbMarchandise.setStackable(newEbMarchandise.getStackable());
                    }

                    List<EbMarchandise> listChild = new ArrayList<EbMarchandise>();

                    if (newEbMarchandise.getChildren() != null && !newEbMarchandise.getChildren().isEmpty()) {
                        newEbMarchandise.getChildren().stream().forEach(child -> {
                            child.setParent(newEbMarchandise);
                            listChild.add(child);
                        });
                    }

                    if (!listChild.isEmpty()) {

                        for (EbMarchandise unit : oldListMarchandise) {
                            EbMarchandise children = listChild
                                    .stream()
                                    .filter(
                                            child -> child.getCustomerReference().equalsIgnoreCase(unit.getCustomerReference()))
                                    .findFirst().orElse(null);

                            if (children != null) {

                                if (action
                                        .indexOf(
                                                "<table class='width-100-percent margin-top-5'><tr>" +
                                                        "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                                        "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                                        oldEbMarchandise.getPartNumber() + "</div>" +
                                                        "<div class='padding-4'>Unit Reference : " +
                                                        oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>")
                                        < 0) {
                                    action += "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " +
                                            oldEbMarchandise.getUnitReference() + "</div>" + "</th></tr>";
                                }

                                action += "<tr>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "fils :" + "</td>" +
                                        "<td class='border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        children.getParent().getUnitReference() + "</td>" +
                                        "<td class='border-right-1-black border-left-1-black border-top-1-black border-bottom-1-black'>" +
                                        "<strike> " + unit.getUnitReference() + "</strike>" + "</td>" + "</tr>";
                                unit.setParent(children.getParent());
                            } else {
                                unit.setParent(null);
                            }

                        }

                    }

                    if (action
                            .indexOf(
                                    "<table class='width-100-percent margin-top-5'><tr>" +
                                            "<th colspan='3' class='padding-0 border-right-1-black border-left-1-black border-top-1-black'>" +
                                            "<div class='padding-4 border-bottom-1-black'>Part Number : " +
                                            oldEbMarchandise.getPartNumber() + "</div>" +
                                            "<div class='padding-4'>Unit Reference : " + oldEbMarchandise.getUnitReference() +
                                            "</div>" + "</th></tr>")
                            >= 0) {
                        action += "</table>";
                    }

                    break;
                }

            }

        }

        // relation fils parent

        // mise à jour des nouvelles valeurs des marchandises enregistré dans la
        // demande
        EbDemande oldDemande = new EbDemande();
        oldDemande = ebDemandeRepository.findOneByEbDemandeNum(ebDemandeNum);

        if (totalVolume != null && oldDemande.getTotalVolume() != totalVolume) {

            if (action.indexOf("units information updated :") < 0) {
                action += "units information updated : ";
            }
            totalVolume = pricingAnnexMethodsService.roundDecimals(totalVolume, Constants.VALEUR_DECIMAL_ARRONDI);
            action += "total volume : " +
                totalVolume + " <strike> " + pricingAnnexMethodsService
                    .roundDecimals(oldDemande.getTotalVolume(), Constants.VALEUR_DECIMAL_ARRONDI) +
                " </strike> <br> ";
            oldDemande.setTotalVolume(totalVolume);
        }

        if (totalWeight != null && oldDemande.getTotalWeight() != totalWeight) {

            if (action.indexOf("units information updated :") < 0) {
                action += "units information updated : ";
            }

            action += "total weight : " + totalWeight + " <strike> " + oldDemande.getTotalWeight() + " </strike> <br> ";
            oldDemande.setTotalWeight(totalWeight);
        }

        if (totalTaxableWeight != null && oldDemande.getTotalTaxableWeight() != totalTaxableWeight) {

            if (action.indexOf("units information updated :") < 0) {
                action += "units information updated : ";
            }

            action += "total taxable weight : " +
                    totalTaxableWeight + " <strike> " + oldDemande.getTotalTaxableWeight() + " </strike> <br> ";
            oldDemande.setTotalTaxableWeight(totalTaxableWeight);
        }

        oldDemande.setExportControlStatut(this.getExportControlStatusFromUnits(oldListMarchandise)); // recalcul
        // du
        // champ
        // export
        // control
        // statut

        this.reCalculateUnitsPackingList(oldDemande, oldListMarchandise);

        // Persist list des marchandises
        ebMarchandiseRepository.saveAll(oldListMarchandise);
        // Persist ebDemande
        EbDemande ebDemande = oldDemande;

        List<EbMarchandise> unitList = oldListMarchandise.size() > UnitRefSharingParams.MAX_UNITS_REFS_SUPPORTED ?
                oldListMarchandise.subList(0, UnitRefSharingParams.MAX_UNITS_REFS_SUPPORTED) :
                oldListMarchandise;

        String suffix = unitList.size() > UnitRefSharingParams.MAX_UNITS_REFS_SUPPORTED ?
                UnitRefSharingParams.UNITS_REF_SUFFIX :
                UnitRefSharingParams.EMPTY_STRING;
        String defaultUnitsRefs = unitList
            .stream().filter(u -> u.getUnitReference() != null).map(EbMarchandise::getUnitReference).collect(
                Collectors
                    .joining(UnitRefSharingParams.UNITS_REF_SEPARATOR, UnitRefSharingParams.EMPTY_STRING, suffix));

        List<EbTtTracing> tracings = ebTrackTraceRepository.findByXEbDemande_ebDemandeNum(ebDemandeNum);
        EbTypeFlux typeOfFlux = ebTypeFluxRepository.findOneByEbTypeFluxNum(ebDemande.getxEbTypeFluxNum());

        for (EbTtTracing tt : tracings) {
            if (TrackingLevel.TRANSPORT.getCode().equals(typeOfFlux.getTrackingLevel())) {
                tt.setCustomerReference(defaultUnitsRefs);
            } else {
                Optional<EbMarchandise> optionalUnit = oldListMarchandise
                        .stream().filter(it -> it.getEbMarchandiseNum() == tt.getxEbMarchandise().getEbMarchandiseNum())
                        .findFirst();
                if (optionalUnit.isPresent()) {

                    // FIXME rename customer ref to unitsRef
                    tt.setCustomerReference(optionalUnit.get().getUnitReference());
                }
            }
        }

        ebDemande.setUnitsReference(defaultUnitsRefs);
        oldDemande = ebDemandeRepository.save(ebDemande);

        if (!action.isEmpty()) {
            EbChat ebChat = listStatiqueService
                    .historizeAction(
                            oldDemande.getEbDemandeNum(),
                            Enumeration.IDChatComponent.PRICING.getCode(),
                            Enumeration.Module.PRICING.getCode(),
                            action,
                            connectedUser,
                            connectedUser.getRealUserNum() != null ?
                                    connectedUser.getRealUserNomPrenom() :
                                    connectedUser.getNomPrenom(),
                            connectedUser.getRealUserNum() != null ? connectedUser.getNomPrenom() : null,
                            null);
            result.put("ebChat", ebChat);
            result.put("listMarchandise", oldListMarchandise);

            oldListMarchandise = null;
            action = "";
        }

        return result;
    }

    public void reCalculateUnitsPackingList(EbDemande demande, List<EbMarchandise> newUnitsList) {
        List<String> unitsPackingList = newUnitsList
                .stream().filter(unit -> unit.getPackingList() != null && !unit.getPackingList().isEmpty())
                .map(unit -> unit.getPackingList()).collect(Collectors.toList());

        if (unitsPackingList != null &&
                !unitsPackingList.isEmpty()) demande.setUnitsPackingList(StringUtils.join(unitsPackingList, ","));
    }

    public List<String> selectListNumAwbBolFromDemandes(String term) {
        return ebDemandeRepository.selectListNumAwbBolFromDemandes(term);
    }

    public Set<EbUser> getListRequestorByUser(SearchCriteriaPricingBooking criteria) {
        Set<EbUser> list = new HashSet<EbUser>();

        try {
            this.getListEbDemande(criteria).forEach(demande -> list.add(demande.getUser()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    @Override
    public List<String> getListFlagForCarrier(Integer ebUserNum) {
        return ebDemandeRepository.selectListFlagForCarrier(ebUserNum);
    }

    @Override
    public List<String> getListFlagForControlTower(Integer ebUserNum) {
        return ebDemandeRepository.selectListFlagForControlTower(ebUserNum);
    }
}
