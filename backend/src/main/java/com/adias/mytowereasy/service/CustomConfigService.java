/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.custom.EbFormField;
import com.adias.mytowereasy.model.custom.EbFormFieldCompagnie;
import com.adias.mytowereasy.model.custom.EbFormView;


public interface CustomConfigService {
    EbFormFieldCompagnie getListChamp(Integer ebCompagnieNum);

    void updateListChamp(EbFormFieldCompagnie ebFormFieldCompagnie);

    List<EbFormField> getListChampFormFieldGlobal();

    List<EbFormView> getListView();
}
