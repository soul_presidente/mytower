package com.adias.mytowereasy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbVehicule;
import com.adias.mytowereasy.repository.EbVehiculeRepository;


@Service
public class VehiculeServiceImpl extends MyTowerService implements VehiculeService {
    @Autowired
    EbVehiculeRepository ebVehiculeRepository;

    @Override
    public List<EbVehicule> SavelistVehicule(List<EbVehicule> listVehicule) {
        // TODO Auto-generated method stub
        return ebVehiculeRepository.saveAll(listVehicule);
    }

    @Override
    public List<EbVehicule> getListVehicule(Integer compagnieNum) {
        // TODO Auto-generated method stub
        return ebVehiculeRepository.findByXEbCompagnie(compagnieNum);
    }

    @Override
    public Integer deleteVehicule(Integer vehiculeNum) {
        // TODO Auto-generated method stub
        return ebVehiculeRepository.deleteVehicule(vehiculeNum);
    }
}
