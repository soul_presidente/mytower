package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dto.EbBoutonActionDTO;
import com.adias.mytowereasy.model.EbBoutonAction;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class BoutonActionServiceImpl extends MyTowerService implements BoutonActionService {
    @Autowired
    BoutonActionService boutonActionService;

    @Override
    public EbBoutonAction addBoutonAction(EbBoutonAction ebBoutonAction) {
        ebBoutonAction.setUser(new EbUser(connectedUserService.getCurrentUser().getEbUserNum()));
        return ebBoutonActionRepository.save(ebBoutonAction);
    }

    @Override
    public List<EbBoutonAction> getListBoutonAction(SearchCriteria criteria) {
        return daoBoutonAction.listBoutonAction(criteria);
    }

    @Override
    public Long countListBoutonAction(SearchCriteria criteria) {
        return daoBoutonAction.countListBoutonAction(criteria);
    }

    @Override
    public void deleteBoutonAction(Integer ebBoutonActionNum) {
        ebBoutonActionRepository.deleteById(ebBoutonActionNum);
    }

    @Override
    public List<EbBoutonActionDTO> getListBoutonActionFromCodes(String listBoutonActionCodes) {

        if (listBoutonActionCodes != null) {
            List<EbBoutonActionDTO> listEbBoutonAction = new ArrayList<>();
            List<String> listBoutonActionId = Arrays.asList(listBoutonActionCodes.split(","));

            for (int i = 0; i < listBoutonActionId.size(); i++) {

                try {
                    Optional<EbBoutonAction> boutonAction = ebBoutonActionRepository
                        .findById(Integer.parseInt(listBoutonActionId.get(i)));

                    if (boutonAction.isPresent()) {
                        listEbBoutonAction.add(new EbBoutonActionDTO(boutonAction.get()));
                    }

                } catch (NumberFormatException e) {
                    System.err.println("Number format exception on flag id : " + listBoutonActionId.get(i));
                }

            }

            return listEbBoutonAction;
        }

        return null;
    }
}
