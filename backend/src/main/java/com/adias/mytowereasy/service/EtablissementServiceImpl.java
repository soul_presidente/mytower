/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.airbus.service.EbTtCompanyPslService;
import com.adias.mytowereasy.dao.DaoCurrency;
import com.adias.mytowereasy.dao.DaoTypeUnit;
import com.adias.mytowereasy.delivery.service.DeliveryService;
import com.adias.mytowereasy.delivery.service.OrderService;
import com.adias.mytowereasy.dock.model.EbEntrepot;
import com.adias.mytowereasy.dock.repository.EbEntrepotRepository;
import com.adias.mytowereasy.dto.EbTypeMarchandiseDto;
import com.adias.mytowereasy.dto.mapper.TransportMapper;
import com.adias.mytowereasy.enumeration.ExportControlEnum;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.ServiceType;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaTypeUnit;


@Service
public class EtablissementServiceImpl extends MyTowerService implements EtablissementService {
    @Autowired
    EbTtCompanyPslService ebTtCompanyPslService;

    @Autowired
    TypeRequestService typeRequestService;

    @Autowired
    DaoTypeUnit daoTypeUnit;

    @Autowired
    EbIncotermService incotermService;

    @Autowired
    CategorieService categorieService;

    @Autowired
    OrderService orderService;

    @Autowired
    DeliveryService deliveryService;

    @Autowired
    DaoCurrency daoCurrency;

    @Autowired
    TrackService trackService;

    @Autowired
    UserService userService;

    @Autowired
    SavedFormService savedFormService;

    @Autowired
    private TransportMapper mapper;

    @Autowired
    EbEntrepotRepository ebEntrepotRepository;

    @Override
    public EbEtablissement updateEbEtablissement(EbEtablissement ebEtablissement) {
        return ebEtablissementRepository.save(ebEtablissement);
    }

    @Override
    public EbEtablissement selectEbEtablissement(SearchCriteria criterias) {
        EbEtablissement etablissement = ebEtablissementRepository
            .findOneWithDetailsByEbEtablissementNum(criterias.getEbEtablissementNum());
        return etablissement;
    }

    @Override
    public List<EbEtablissement> selectListEbEtablissement(SearchCriteria criterias) {
        return daoEtablissement.selectListEbEtablissement(criterias);
    }

    @Override
    public List<EbEtablissement> selectListEbEtablissementCt(SearchCriteria criterias) {
        return daoEtablissement.selectListEbEtablissement(criterias);
    }

    @Override
    public Long getListEtablissementCount(SearchCriteria criteria) {
        return daoEtablissement.getListEtablissementCount(criteria);
    }

    // ----------------------------------------------------------------------------------
    // //

    @Override
    public List<EbAdresse> selectListEbAdresseEbEtablissement(SearchCriteria criterias) {
        return new ArrayList(
            ebAdresseRepository.getAllByEtablissementNum(":" + criterias.getEbEtablissementNum() + ":"));
    }

    @Override
    public EbAdresse addEbAdresseEbEtablissement(EbAdresse ebAdresse) {
        EbAdresse adress = new EbAdresse();

        if (ebAdresse.getEbAdresseNum() != null && ebAdresse.getIsUpdateUserVisibility()) {
            adress = ebAdresseRepository.getOne(ebAdresse.getEbAdresseNum());
            adress.setxEbUserVisibility(ebAdresse.getxEbUserVisibility());
        }
        else {
            adress = ebAdresse;
            Integer ebEntrepottNum = ebAdresse.getEntrepot().getEbEntrepotNum();
            EbEntrepot entrepot = ebEntrepotRepository.findByEbEntrepotNum(ebEntrepottNum);
            adress.setEntrepot(entrepot);
            EbUser userConnected = connectedUserService.getCurrentUser();

            if (userConnected.isAdmin()) {
                Integer ebEtablissementnum = userConnected.getEbEtablissement().getEbEtablissementNum();
                adress.setListEtablissementsNum(":" + ebEtablissementnum + ":");
            }

        }

        return ebAdresseRepository.save(adress);
    }

    @Override
    public EbAdresse updateEbAdresseEbEtablissement(EbAdresse ebAdresse) {

        if (ebAdresse.getDefaultZone() == null || ebAdresse.getDefaultZone().getEbZoneNum() == null) {
            ebAdresse.setDefaultZone(null);
        }

        if (ebAdresse.getxEbUserVisibility() == null || ebAdresse.getxEbUserVisibility().getEbUserNum() == null) {
            ebAdresse.setxEbUserVisibility(null);
        }

        if (ebAdresse.getCountry() != null && StringUtils.isNotEmpty(ebAdresse.getCountry().getCode())) {
            EcCountry country = ecCountryRepository.findFirstByCode(ebAdresse.getCountry().getCode());
            ebAdresse.setCountry(country);
        }

        return ebAdresseRepository.save(ebAdresse);
    }

    @Override
    public Boolean deleteEbAdresseEbEtablissement(EbAdresse ebAdresse) {
        ebAdresseRepository.delete(ebAdresse);
        return true;
    }

    // ----------------------------------------------------------------------------------

    @Override
    public EbEtablissement addEtablissement(EbEtablissement ebEtablissement) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        EbCompagnie gg = ebCompagnieRepository.getOne(ebEtablissement.getEbCompagnie().getEbCompagnieNum());
        ebEtablissement.setEbCompagnie(gg);
        ebEtablissement.setRole(connectedUser != null ? connectedUser.getRole() : gg.getCompagnieRole());
        return ebEtablissementRepository.save(ebEtablissement);
    }

    // ----------------------------------------------------------------------------------
    // //

    @Override
    public EbCustomField addEbCustomField(EbCustomField ebCustomField) {
        return ebCustomFieldRepository.save(ebCustomField);
    }

    @Override
    public EbCustomField selectEbCustomFieldByEbCompagnieNum(Integer ebCompagnieNum) {
        return ebCustomFieldRepository.findFirstByXEbCompagnie(ebCompagnieNum);
    }

    // ----------------------------------------------------------------------------------
    // //

    private List<EbCategorie>
        getConfigurableUserCategories(List<EbCategorie> listCategorie, EbUser chargeur, SearchCriteria criteria) {

        if (listCategorie != null && !listCategorie.isEmpty()) {
            Integer ebCompagnieNum = criteria.getEbCompagnieNum() != null ?
                criteria.getEbCompagnieNum() :
                chargeur.getEbCompagnie().getEbCompagnieNum();

            List<EbCategorie> listCategoriesCompany = ebCategorieRepository
                .findDistinctByEtablissementEbCompagnieEbCompagnieNum(ebCompagnieNum);

            try {
                Gson gson = new Gson();
                String json = gson.toJson(listCategorie);
                listCategorie = gson.fromJson(json, new TypeToken<ArrayList<EbCategorie>>() {
                }.getType());
            } catch (Exception e) {
                System.err
                    .println(
                        "EtablissementServiceImpl.getUserCategories : failed to convert object to listCategories.");
            }

            if (listCategoriesCompany != null && !listCategoriesCompany.isEmpty()) {

                for (EbCategorie ebCategorie: listCategorie) {

                    if (ebCategorie.getIsAll() != null && ebCategorie.getIsAll()) {
                        EbCategorie categorieCompany = listCategoriesCompany
                            .stream().filter(cat -> cat.getEbCategorieNum().equals(ebCategorie.getEbCategorieNum()))
                            .findFirst().orElse(null);

                        if (categorieCompany != null) ebCategorie.setLabels(categorieCompany.getLabels());
                    }

                }

            }

        }

        return listCategorie;
    }

    @Override
    public Map<String, Object> getDataPreference(SearchCriteria criteria) {
        Map<String, Object> result = new HashMap<String, Object>();
        List<EbCategorie> listCategorie = new ArrayList<EbCategorie>();

        criteria.setIncludeGlobalValues(true);

        // recuperer la liste des categories pour le chargeur
        if (criteria.getEbUserNum() != null) {
            EbUser connectedUser = ebUserRepository.findOneByEbUserNum(criteria.getConnectedUserNum());

            if ((connectedUser != null && !connectedUser.isSuperAdmin() && connectedUser.isChargeur())
                || criteria.getEbCompagnieNum() == null) {
                listCategorie = this
                    .getConfigurableUserCategories(connectedUser.getListCategories(), connectedUser, criteria);
            }
            else if (criteria.getEbCompagnieNum() != null) listCategorie = ebCategorieRepository
                .findDistinctByEtablissementEbCompagnieEbCompagnieNum(criteria.getEbCompagnieNum());

            // filtrage sur les labels non supprimée
            if (listCategorie != null && !listCategorie.isEmpty()) {
                listCategorie.forEach(cat -> {
                    Set<EbLabel> labels = new HashSet<>();

                    if (cat.getLabels() != null && !cat.getLabels().isEmpty()) {
                        cat.getLabels().forEach(lab -> {
                            if (lab.getDeleted() == null
                                || lab.getDeleted() != null && !lab.getDeleted()) labels.add(lab);
                        });
                        cat.setLabels(labels);
                    }

                });
            }

            // get only the user's specific Labels..
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                String json = objectMapper.writeValueAsString(connectedUser.getListCategories());
                List<EbCategorie> userCategories = objectMapper
                    .readValue(json, new TypeReference<ArrayList<EbCategorie>>() {
                    });

                List<Integer> userCategoriesIds = userCategories
                    .stream().flatMap(e -> e.getLabels().stream().map(l -> l.getEbLabelNum()))
                    .collect(Collectors.toList());

                listCategorie.forEach(cat -> {

                    if (userCategoriesIds.contains(cat.getEbCategorieNum())) {
                        EbCategorie categorie = userCategories
                            .stream().filter(uc -> uc.getEbCategorieNum() == cat.getEbCategorieNum()).findFirst()
                            .orElse(null);

                        cat.setLabels(categorie.getLabels());
                    }

                });
            } catch (Exception e) {
                System.err.println("ConnectedUser: failed to convert object to listCategories.");
            }

            result.put("listCategorie", listCategorie);
        }

        if (criteria.getEbCompagnieNum() != null) {
            criteria.setActivated(true);
            result.put("listTypeRequest", typeRequestService.searchTypeRequest(criteria));

            // chercher les type of unit
            SearchCriteriaTypeUnit criteriaTypeUnit = new SearchCriteriaTypeUnit();
            criteriaTypeUnit.setSearchterm("true");
            criteriaTypeUnit.setEbCompagnieNum(criteria.getEbCompagnieNum());
            List<EbTypeMarchandiseDto> resultList = mapper
                .ebTypeUnitsToEbTypesMarchandise(daoTypeUnit.getListTypeUnit(criteriaTypeUnit));

            if (criteria.getEbDemandeNum() != null) {
                resultList = daoTypeUnit.checkListTypeMarchandise(resultList, criteria.getEbDemandeNum());
            }

            result.put("listTypeUnit", resultList);
        }

        // recuperer la liste des cost centers
        if (criteria.getEbEtablissementNum() != null) result
            .put("listCostCenter", ebCostCenterRepository.findByCompagnieEbCompagnieNum(criteria.getEbCompagnieNum()));

        // recuperer la liste des champs parametrables
        if (criteria.getEbCompagnieNum() != null) {
            result.put("customField", ebCustomFieldRepository.findFirstByXEbCompagnie(criteria.getEbCompagnieNum()));

            criteria.setIncludeGlobalValues(true);
            result.put("typeOfFlux", ebTypeFluxService.getListTypeFlux(criteria));
            result.put("listIncoterm", incotermService.getListIncoterms(criteria));
            result.put("listSchemaPsl", daoEbTtCompanyPsl.getListSchemaPsl(criteria));
        }

        // si on n'a besoin que des categories/labels il faut mettre
        // isIgnoreContact à false (use case lors de la création de pricing/TF)
        if (!criteria.isIgnoreContact() && criteria.getEbUserNum() != null) {
            criteria.setEbEtablissementNum(null);
            criteria.setEbCompagnieNum(null);
            criteria.setRoleBroker(true);
            criteria.setUserService(ServiceType.SERVICE_BROKER.getCode());
            result.put("listBroker", daoUser.selectListContact(criteria));

            criteria.setRoleBroker(false);
            criteria.setRoleTransporteur(true);
            criteria.setUserService(ServiceType.SERVICE_TRANSPORTEUR.getCode());
            criteria.setRoleControlTower(true);
            criteria.setForChargeur(true);
            criteria.setContact(true);
            criteria.setWithActiveUsers(true);
            result.put("listTransporteur", daoUser.selectListContact(criteria));
        }

        List<EcCountry> countries = serviceListStatique.getListEcCountry();
        result.put("countries", countries);
        result.put("listExportControl", ExportControlEnum.getValues());

        return result;
    }

    /*----------------------------------------------*/
    public List<EbCategorie> selectListEbCategorieByEtablissementNum(Integer ebEtablissementNum) {
        return ebCategorieRepository.findDistinctByEtablissementEbEtablissementNum(ebEtablissementNum);
    }

    public Boolean transfertUser(TransfertUserObject transfertUserObject) throws Exception {
        // Affectation de l'etablissement et de la categorie labels
        userService.updateEtablissement(transfertUserObject);

        // Gestion des orders
        if (Enumeration.TransfertActionChoice.ALL.getCode() == transfertUserObject.getOrders().getAction()) {
            orderService.updateOrderEtablissement(transfertUserObject);
            deliveryService.updateDeliveryEtablissement(transfertUserObject);
        }

        // Gestion des documents des fichiers
        if (Enumeration.TransfertActionChoice.ALL.getCode() == transfertUserObject.getTransportFiles().getAction()) {
            updateTransportFilesEtablissement(transfertUserObject);
        }

        // Gestion des communautés
        if (Enumeration.TransfertActionChoice.ALL.getCode().equals(transfertUserObject.getCommunity().getAction())) {
            ebRelationRepository
                .updateHostCommunityEtablissement(
                    transfertUserObject.getEbUserNum(),
                    ebEtablissementRepository.getOne(transfertUserObject.getNewEtablissement()));

            ebRelationRepository
                .updateGuessCommunityEtablissement(
                    transfertUserObject.getEbUserNum(),
                    ebEtablissementRepository.getOne(transfertUserObject.getNewEtablissement()));
        }

        return true;
    }

    @Override
    public Boolean updateTransportFilesEtablissement(TransfertUserObject transfertUserObjet) throws Exception {
        // Mise à jour des transports et trackTrace concernés
        ebDemandeRepository
            .updateEbDemandeEtablissement(
                transfertUserObjet.getOldEtablissement(),
                transfertUserObjet.getNewEtablissement(),
                transfertUserObjet.getOldListLabels());
        ebTrackTraceRepository
            .updateTrackTraceEtablissement(
                transfertUserObjet.getOldEtablissement(),
                transfertUserObjet.getNewEtablissement(),
                transfertUserObjet.getOldListLabels());

        // Mise à jour des Freight Audit concerés (Invoice)
        ebInvoiceRepository
            .updateInvoiceEtablissement(
                transfertUserObjet.getOldEtablissement(),
                transfertUserObjet.getNewEtablissement(),
                transfertUserObjet.getOldListLabels());

        // Mise à jour des incidents concernés
        ebQmIncidentRepository
            .updateIncidentEtablissement(
                transfertUserObjet.getOldEtablissement(),
                transfertUserObjet.getNewEtablissement(),
                transfertUserObjet.getOldListLabels());

        // Mise à jour des Masks (SavedForm)
        userService.updateMaskEtablissement(transfertUserObjet);

        return true;
    }
}
