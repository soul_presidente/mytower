package com.adias.mytowereasy.service.storage;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.commons.io.FilenameUtils;

import com.adias.mytowereasy.model.Enumeration;


public class MyFileUtils {
    public static void copyFilesToDestination(Path filePath, InputStream fileInputStream) throws IOException {
        Files.createDirectories(filePath);
        Files.copy(fileInputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
    }

    public static String
        resolveFilePath(Enumeration.UploadDirectory uploadDirectory, String fileName, String subFolder) {
        subFolder = subFolder != null ? (subFolder + "/") : "";
        String path = uploadDirectory.getDirectory() + "/" + subFolder + fileName;
        String filePath = FilenameUtils.normalize(path);
        return filePath;
    }

    public static String resolveFileName(Enumeration.UploadDirectory uploadDirectory, String fileOriginalName) {
        // if there is a subFolder add "slash" separator after it to build the
        // final path correctly
        // otherwise empty string
        // get the prefix from uploadPrefixes enumeration
        String prefix = Enumeration.uploadPrefixes.get(uploadDirectory.getCode());
        // String fileNameWithoutExtension =
        // FilenameUtils.removeExtension(fileOriginalName);
        String datePrefix = "_dt_";
        long currentTimestamp = System.currentTimeMillis();
        String dot = ".";
        String extension = FilenameUtils.getExtension(fileOriginalName);

        return prefix + datePrefix + currentTimestamp + dot + extension;
    }
}
