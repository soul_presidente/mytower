package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dao.DaoCostItem;
import com.adias.mytowereasy.model.EbCost;
import com.adias.mytowereasy.model.EbCostCategorie;
import com.adias.mytowereasy.repository.EbCostCategorieRepository;
import com.adias.mytowereasy.repository.EbCostRepository;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class CostItemServiceImpl extends MyTowerService implements CostItemService {
    @Autowired
    DaoCostItem daoCostItem;

    @Autowired
    EbCostRepository ebCostRepository;

    @Autowired
    EbCostCategorieRepository ebCostCategorieRepository;

    @Override
    public List<EbCost> getListCostsItems(SearchCriteria criteria) {
        return daoCostItem.getListCostItemsByCompagnie(criteria);
    }

    public Long getCountListCostItems(SearchCriteria criteria) {
        return daoCostItem.getCountListCostItemByCompagnie(criteria);
    }

    public List<EbCostCategorie> getListCostCategorie(Integer compagnieNum, Boolean activedOnly) {
        List<EbCostCategorie> listCostCategorie = new ArrayList<>();
        listCostCategorie = ebCostCategorieRepository.selectEbCostCategorieByCompagnieNum(compagnieNum);
        listCostCategorie.forEach((costCategorie) -> {

            if (activedOnly) {
                costCategorie
                    .setListEbCost(
                        costCategorie
                            .getListEbCost().stream().filter(ebCost -> ebCost.getActived().equals(activedOnly))
                            .collect(Collectors.toList()));
            }

            costCategorie.getListEbCost().forEach(ebCost -> {
                ebCost.setLibelleCostItem(ebCost.getLibelle());

                if (ebCost.getPriceReal() == null) {
                    ebCost.setEuroExchangeRateReal(null);
                }

            });
        });

        return listCostCategorie;
    }

    public EbCost addCostItem(EbCost ebCost) {
        ebCost.setEbCostNum(daoCostItem.nextValEbCost());
        return ebCostRepository.save(ebCost);
    }

    public EbCost updateCostItem(EbCost newEbCost) {
        EbCost ebCost = ebCostRepository.findEbCostByEbCostNum(newEbCost.getEbCostNum());
        ebCost.setLibelle(newEbCost.getLibelle());
        ebCost.setActived(newEbCost.getActived());
        ebCost.setCode(newEbCost.getCode());
        ebCost.setListCostCategorie(newEbCost.getListCostCategorie());
        ebCost.setListModeTransport(newEbCost.getListModeTransport());
        return ebCostRepository.save(ebCost);
    }

    public EbCostCategorie addCostCategorie(EbCostCategorie ebCostCategorie) {
        ebCostCategorie.setEbCostCategorieNum(daoCostItem.nextValEbCostCategorie());
        return ebCostCategorieRepository.save(ebCostCategorie);
    }

    @Override
    public boolean deleteCostItem(Integer ebCostNum) {

        try {
            ebCostRepository.deleteById(ebCostNum);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean checkCodeExistance(String code, Integer ebCompagnieNum) {
        return ebCostRepository.existsByxEbCompagnie_ebCompagnieNumAndCodeIgnoreCase(ebCompagnieNum, code);
    }
}
