/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.custom.EbFormField;
import com.adias.mytowereasy.model.custom.EbFormFieldCompagnie;
import com.adias.mytowereasy.model.custom.EbFormView;


@Service
public class customConfigServiceImp extends MyTowerService implements CustomConfigService {
    @Override
    public EbFormFieldCompagnie getListChamp(Integer ebCompagnieNum) {
        EbFormFieldCompagnie formFieldCompagnie = new EbFormFieldCompagnie();
        formFieldCompagnie = ebDouaneRepository.findOneByxEbCompagnieEbCompagnieNum(ebCompagnieNum);

        if (formFieldCompagnie == null) {
            formFieldCompagnie = new EbFormFieldCompagnie();
        }

        return formFieldCompagnie;
    }

    @Override
    public void updateListChamp(EbFormFieldCompagnie ebFormFieldCompagnie) {
        List<EbFormField> listFormField = ebFormFieldCompagnie.getListFormField();

        EbFormFieldCompagnie entityToSave = null;

        if (ebFormFieldCompagnie.getEbFormFieldCompagnieNum() != null) {
            entityToSave = ebDouaneRepository.findById(ebFormFieldCompagnie.getEbFormFieldCompagnieNum()).orElse(null);
        }

        if (entityToSave == null) {
            entityToSave = ebDouaneRepository
                .findOneByxEbCompagnieEbCompagnieNum(ebFormFieldCompagnie.getxEbCompagnie().getEbCompagnieNum());
        }

        if (entityToSave == null) {
            entityToSave = new EbFormFieldCompagnie();
            entityToSave.setxEbCompagnie(ebFormFieldCompagnie.getxEbCompagnie());
        }

        entityToSave.setListFormField(listFormField);

        ebDouaneRepository.save(entityToSave);
    }

    @Override
    public List<EbFormField> getListChampFormFieldGlobal() {
        List<EbFormField> ListFormField = daoCustom.getListEbFormField();
        return ListFormField;
    }

    @Override
    public List<EbFormView> getListView() {
        List<EbFormView> ListFormView = ebFormViewRepository.findAll();
        return ListFormView;
    }
}
