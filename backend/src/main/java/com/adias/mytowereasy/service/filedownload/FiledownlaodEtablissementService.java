package com.adias.mytowereasy.service.filedownload;

import java.util.List;
import java.util.Map;

import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface FiledownlaodEtablissementService {
    public FileDownloadStateAndResult
        listFileDownLoadEtablissementInformation(SearchCriteria criteria, List<Map<String, String>> dtConfig)
            throws Exception;
}
