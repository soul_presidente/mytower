package com.adias.mytowereasy.service.filedownload;

import java.util.List;
import java.util.Map;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface FiledownloadComptaMatiereService {
    public FileDownloadStateAndResult
        listFileDownLoadComptaMatiere(List<Map<String, String>> dtConfig, EbUser connectedUser, SearchCriteria criteria)
            throws Exception;
}
