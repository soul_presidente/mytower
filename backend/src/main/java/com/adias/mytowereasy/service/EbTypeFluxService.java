package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.EbTypeFlux;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface EbTypeFluxService {
    List<EbTypeFlux> getListTypeFlux(SearchCriteria criteria);

    Long getListTypeFluxCount(SearchCriteria criteria);

    EbTypeFlux updateTypeFlux(EbTypeFlux typeFlux);

    Boolean deleteTypeFlux(Integer Id);

    void generateTypeOfFluxServiceByCompanyNum(List<Integer> listEbCompagnieNum) throws Exception;

    List<EbTypeFlux> getListTypeFluxByEbCompagnieNum(Integer ebCompagnieNum);
}
