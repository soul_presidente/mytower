/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.validation.Valid;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


public interface PricingService {
     List<EbDemande> getListEbDemandeInit(SearchCriteriaPricingBooking criterias);

     HashMap<String, Object> getEbAdresseByGlobalsFieldsAsEbParty(
        String term,
        Integer ebCompagnieNum,
        Integer ebEtablissementNum,
        String eligibilities,
        Integer ebUserNum,
        Integer ecCountryNum,
        String city,
        String zipCode,
        Integer ebZoneNum,
        Integer size,
        Integer page);

     List<EbParty> getEbPartyByCompany(String company);

     List<EbParty> getEbPartyByCity();

     List<EbParty> getAllEbParty();

     List<EbAdresse> getAllEbAdresse();

     List<EbDemande> getAllEbDemande(SearchCriteriaPricingBooking criteria);

     List<EbDemande> getListEbDemande(SearchCriteriaPricingBooking criteria) throws Exception;

     int getCountListEbDemande(SearchCriteriaPricingBooking criteria) throws Exception;

     List<EbCostCenter> getEbCostCenterByCompagnie(Integer ebCompagnieNum);


     EbDemande getEbDemande(SearchCriteriaPricingBooking criteria, EbUser userConnected) throws Exception;

     Integer updateStatusQuotation(ExEbDemandeTransporteur demandeTransporteur, Boolean sendMail, Integer module);

     Map<String, Object> deleteDocument(EbDemandeFichiersJoint doc);

     EbChat cancelQuotation(Integer ebDemandeNum);

     Integer confirmPickup(EbDemande ebDemande);

    Map<String, Object> updateEbDemandeTransportInfos(EbDemande ebDemande, Boolean sendMail, String action);

     List<EbCostCategorie> getCostCategorieByMode(Integer modeTrans);

     List<EbCostCategorie> getAllCostCategorie();

    Map<String, Object> getTaskProgress(String ident, InputStream file, String fileName, Integer ebUserNum);

    Map<String, Object>
			addQuotation(
				ExEbDemandeTransporteur ExEbDemandeTransporteur,
				Integer module,
				Boolean isQuotationRequest
			)
				throws InterruptedException,
				ExecutionException;

    Map<String, Object> editQuotation(ExEbDemandeTransporteur ExEbDemandeTransporteur, Integer moduleNum);

    List<EbAdresse> getEbAdresseByCompany(Integer ebCompagnieNum);

    Map<String, Object> getListDemandeQuote(SearchCriteriaPricingBooking criteria) throws Exception;

    Integer deleteQuotation(Integer ebDemandeTransporteurNum);

    Integer updateGoodsPickup(EbDemande[] ebDemandes);

    Map<String, Object> updateTransportRequestAndProcessPostUpdate(EbDemande ebDemande, Integer typeData, Integer module, EbUser connectedUser) throws Exception;

    Map<String, Object> updateEbDemande(EbDemande ebDemande, Integer typeData, Integer module, EbUser connectedUser) throws Exception;

    Integer deleteEbDemande(Integer[] listEbDemandeNum);

    void bulkImportDemande(
        InputStream file,
        String fileName,
        Integer ebUserNum,
        List<Map<String, String>> errors,
        String ident)
        throws Exception;

    Integer getNextEbDemandeNum();

    Map<String, Object> updateCustomsInformation(EbCustomsInformation ebCustomsInformation);

    EbCustomsInformation getCustomsInformation(Integer ebDemandeNum);

    Integer requestCustomsBroker(Integer ebDemandeNum);

    EbDemande demande(SearchCriteriaPricingBooking criteria) throws Exception;

    EbDemande saveFlags(Integer ebDemandeNum, String newFlags);

    EbDemande updatePropRdv(EbDemande ebDemande);

    List<EbDemande> getListEbDemandeFavoris(SearchCriteriaPricingBooking criteria) throws Exception;

    void setListDemandeQuote(SearchCriteriaPricingBooking criteria, List<EbDemande> listEbDemande, boolean onlyOneTr);

    void updateQuotesWithPslByCompanyNum(List<Integer> listEbCompagnieNum) throws Exception;

     Boolean generateAssociatedPricingObjects(Integer ebDemandeNum);

     EbChat
        confirmeAskFortrResponsibility(Integer ebDemandeNum, Integer moduleNum, String RefTransport, String action);

     EbChat confirmeProvideTransport(Integer ebDemandeNum, Integer moduleNum, String refTransport, String action);

     EbChat acknowledgeTdc(Integer ebDemandeNum);

     ExEbDemandeTransporteur updateTransportPlan(
        Integer ebDemandeNum,
        Integer ebPartyNum,
        String oldZoneLabel,
        Integer ebZoneNum,
        String refPlan,
        BigDecimal calculatedPrice)
        throws Exception;

     Set<String> getFieldsByCompagnieNumAndTerm(String term, Integer ebCompagnieNum);

     List<EbDemande> ListDemandeFromTransfertUserObject(TransfertUserObject transfertUserObjet, Integer module)
        throws Exception;

    List<EbAdresse> getEbAdresseByCriteria(SearchCriteria criteria);

    EbDemande prepareQRForValorisation(Integer ebDemandeNum);

    EbDemande saveAllInformations(EbDemande ebDemande, Integer ebModuleNum);

    void addListDemandeInitials(EbDemande ebDemande);

    EbDemande createEbDemande(EbDemande ebDemande, Integer ebModuleNum, boolean activateTasksAfterCreation, EbUser connectedUser);

    EbDemande confirmPendingDemande(EbDemande ebDemande);

    EbDemande addEbDemande(EbDemande ebDemande);

    EbDemande unitValuation(EbDemande ebDemande);

    void updatexEcStatutGroupageInEbDemandeByEbDemandeNum(List<Integer> listDemandeNum, Integer xEcStatutGroupage);

    EbDemande updatelistAdditionalCostInEbDemandeByEbDemandeNum(Integer ebDemandeNum, List<EbCost> additionalCost);

    List<String> getListIncotermCity(Integer ebCompagnieNum);

    List<EbQrGroupe> getListQrGroupe(SearchCriteria criteria, boolean active);

    void updateListTypeDoc(Integer ebDemandeNum, Integer ebInvoiceNum, String codeDoc, boolean isDecremante);

    EbDemande saveGeneratedEbDemande(EbDemande demande, EbUser connectedUser);

    EbParty cloneEbParty(EbParty party);

    List<String> selectListNumAwbBolFromDemandes(String term);

    List<EbUser> getAllUsersIntervenantInDemande(Integer ebDemandeNum);

    Map<String, Object> updateListMarchandise(
        List<EbMarchandise> listMarchandise,
        Integer ebDemandeNum,
        Double totalTaxableWeight,
        Double totalWeight,
        Double totalVolume);

     Set<EbUser> getListRequestorByUser(SearchCriteriaPricingBooking criteria);

			void saveInformationsBasedOnTrNature(@Valid EbDemande ebDemande, Integer module);

      void sendEmailAfterCreation(EbDemande ebDemande, EbUser connectedUser);
}
