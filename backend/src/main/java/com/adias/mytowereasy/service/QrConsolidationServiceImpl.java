package com.adias.mytowereasy.service;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;
import com.adias.mytowereasy.airbus.model.EbQrGroupeProposition;
import com.adias.mytowereasy.airbus.model.QrGroupeCategorie;
import com.adias.mytowereasy.airbus.model.QrGroupeObject;
import com.adias.mytowereasy.airbus.repository.EbQrGroupeRepository;
import com.adias.mytowereasy.api.service.PlanTransportApiService;
import com.adias.mytowereasy.api.wso.PlanTransportWSO;
import com.adias.mytowereasy.dao.DaoPrMtcTransporteur;
import com.adias.mytowereasy.delivery.service.DeliveryService;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.kafka.service.KafkaObjectService;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.*;
import com.adias.mytowereasy.model.enums.ConsolidationGroupage;
import com.adias.mytowereasy.mtc.service.ValorizationService;
import com.adias.mytowereasy.repository.ExEbDemandeTransporteurRepository;
import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.util.ConsolidationConstants;
import com.adias.mytowereasy.util.MyTowerCollectionUtils;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Service
public class QrConsolidationServiceImpl extends MyTowerService implements QrConsolidationService {
	private static final int					ONE_INITIAL_TR_IN_GROUP	= 1;

	private static final Logger				LOGGER									= LoggerFactory
		.getLogger(QrConsolidationService.class);

	@Autowired
	ExEbDemandeTransporteurRepository	exEbDemandeTransporteurRepository;
	@Autowired
	PlanTransportApiService						planTransportApiService;

	@Autowired
	DeliveryService										deliveryService;

	@Autowired
	KafkaObjectService								kafkaObjectService;

	@Autowired
	EbQrGroupeRepository							ebQrGroupeRepository;

	@Autowired
	ValorizationService								valorizationService;

	@Autowired
	private DaoPrMtcTransporteur			daoPrMtcTransporteur;

	private static Pattern						pattern;
	private static Matcher						matcher;

    @Override
    public List<EbQrGroupe> getListQrGroupe(SearchCriteria criteria) {
			return daoPricing.getGroupsAndPropositions(criteria, false);
    }

    @Override
    public Long getListQrGroupeCount(SearchCriteria criteria) {
        return daoPricing.getListQrGroupeCount(criteria);
    }

    @Transactional
    @Override
    public EbDemande consolidatePropositionDemande(EbQrGroupeProposition propo) {
        EbDemande res = null;
        EbUser connectedUser = connectedUserService.getCurrentUser();
        if (propo != null) {
            List<Integer> listDemandeNum = new ArrayList<Integer>();
            listDemandeNum.addAll(getListDemandeNum(propo.getListSelectedDemandeNum()));

            if (listDemandeNum.size() > 0) {
                propo.getEbDemandeResult().setFlagConsolidated(true);
                res = pricingService.createEbDemande(propo.getEbDemandeResult(), Enumeration.Module.PRICING.getCode(), true, connectedUser);

                EbQrGroupeProposition oldPropo = ebQrGroupePropositionRepository
                    .findById(propo.getEbQrGroupePropositionNum()).orElse(null);
                oldPropo.setStatut(GroupePropositionStatut.CONFIRMED.getCode());
                oldPropo.setListSelectedDemandeNum(propo.getListSelectedDemandeNum());
                oldPropo.setEbDemandeResult(new EbDemande(propo.getEbDemandeResult().getEbDemandeNum()));
                ebQrGroupePropositionRepository.save(oldPropo);

                ebDemandeRepository
                    .updateCancelStatusAndGroupDemandeNumListEbDemande(
                        listDemandeNum,
                        Enumeration.CancelStatus.CANECLLED.getCode(),
                        res.getEbDemandeNum());
            }

        }

        return res;
    }

    @Transactional
    @Override
    public int rejectPropositionDemande(Integer propoNum) {
        EbQrGroupeProposition propo = ebQrGroupePropositionRepository.findById(propoNum).orElse(null);

        if (propo != null) {
            propo.setStatut(GroupePropositionStatut.REJECTED.getCode());
            ebQrGroupePropositionRepository.save(propo);
        }

        return 1;
    }

    private List<Integer> getListEbDemandeNumInPropositions(List<EbQrGroupeProposition> listPropos) {
        List<Integer> listEbDemandeNum = new ArrayList<>();

        if (listPropos != null && !listPropos.isEmpty()) {

            for (EbQrGroupeProposition prop: listPropos) {
                int nbOccurenceDeuxPoints = compterOccurrences(prop.getListDemandeNum());

                if (prop.getListDemandeNum() != null && !prop.getListDemandeNum().isEmpty()
                    && nbOccurenceDeuxPoints > 3) {
                    if (!prop.getStatut().equals(GroupePropositionStatut.CONFIRMED.getCode())) listEbDemandeNum
                        .addAll(getListDemandeNum(prop.getListDemandeNum()));
                }
                else {
                    ebQrGroupePropositionRepository.deleteTrinitial(prop.getListDemandeNum());
                }

            }

            return listEbDemandeNum;
        }

        return null;
    }

    public static int compterOccurrences(String maChaine) {
        int nbOccurenceDeuxPoints = 0;
        pattern = Pattern.compile(":");
        matcher = pattern.matcher(maChaine);

        while (matcher.find()) {
            nbOccurenceDeuxPoints++;
        }

        return nbOccurenceDeuxPoints;
    }

	@Override
	public EbQrGroupe updateQrGroupeAndProcessGroupage(EbQrGroupe ebQrGroupe) throws JsonProcessingException {
		processingOfGroupageExecutorService(ebQrGroupe, !hasSameCompatibilityCriteria(ebQrGroupe));
		return updateQrGroupe(ebQrGroupe);
	}

	@Override
	public EbQrGroupe updateQrGroupe(EbQrGroupe ebQrGroupe) throws JsonProcessingException {
		EbQrGroupe dbEbQrGoupe = ebQrGroupeRepository.findById(ebQrGroupe.getEbQrGroupeNum())
				.orElseThrow(() -> new IllegalArgumentException(
						String.format("QrGroupe with %s not found", ebQrGroupe.getEbQrGroupeNum())));
		dbEbQrGoupe.setLibelle(ebQrGroupe.getLibelle());
		dbEbQrGoupe.setCompatibilityCriteria(ebQrGroupe.getCompatibilityCriteria());
		dbEbQrGoupe.setDescription(ebQrGroupe.getDescription());
		dbEbQrGoupe.setVentilation(ebQrGroupe.getVentilation());
		dbEbQrGoupe.setOutput(ebQrGroupe.getOutput());
		dbEbQrGoupe.setTriggers(ebQrGroupe.getTriggers());
		dbEbQrGoupe.setIsActive(ebQrGroupe.getIsActive());
		dbEbQrGoupe.setGrpOrder(ebQrGroupe.getGrpOrder());
		return ebQrGroupeRepository.save(dbEbQrGoupe);
	}

	public EbQrGroupe addQrGroupe(EbQrGroupe ebQrGroupe) {
		EbQrGroupe savedQrGroupe = ebQrGroupeRepository.save(ebQrGroupe);
		processingOfGroupageExecutorService(savedQrGroupe, false);
		return savedQrGroupe;
	}
	
	private void processingOfGroupageExecutorService(
		EbQrGroupe ebQrGroupe,
		boolean hasCriteriaChanged
	)
	{
		ExecutorService service = Executors.newCachedThreadPool();
		service.submit(() -> {
			try {
				processGroupageOfTheUpdatedQrGroupe(ebQrGroupe, hasCriteriaChanged);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			}
		});
	}

	private void processGroupageOfTheUpdatedQrGroupe(
		EbQrGroupe ebQrGroupe,
		boolean hasCriteriaChanged
	)
		throws JsonProcessingException
	{
		if (hasCriteriaChanged)
		{
			List<EbQrGroupeProposition> listPropo = null;
			List<EbMarchandise> listMarchandises = new ArrayList<>();
			List<Integer> triggers = null;

			if (ebQrGroupe.getTriggers() != null && ebQrGroupe.getTriggers().length() > 0)
			{
				triggers = Arrays
					.asList(ebQrGroupe.getTriggers().split(","))
					.stream()
					.map(Integer::parseInt)
					.collect(Collectors.toList());
			}

			// déflager la liste des demandes initiales dans les propositions de
			// groupage
			if (ebQrGroupe.getListPropositions() != null && !ebQrGroupe.getListPropositions().isEmpty())
			{
				updateIsGroupingInListDemandeByPropositions(ebQrGroupe.getListPropositions(), false);
			}

			daoPricing.setPropositionGroupe(ebQrGroupe, null);
			listPropo = ebQrGroupe.getListPropositions();
			resetGroupageForInitialTrsAndCancelGroupedTr(triggers, ebQrGroupe);

			if (listPropo != null && !listPropo.isEmpty())
			{
				List<ExEbDemandeTransporteur> listQuote = new ArrayList<>();

				ebQrGroupe.setListPropositions(null);

				for (EbQrGroupeProposition propo : listPropo)
				{

					try
					{
						int nbOccurenceDeuxPoints = compterOccurrences(propo.getListDemandeNum());

						if (propo.getEbQrGroupe() == null || propo.getEbQrGroupe().getEbQrGroupeNum() == null)
						{
							propo.setEbQrGroupe(new EbQrGroupe(ebQrGroupe.getEbQrGroupeNum()));
						}

						if (propo.getListDemandeNum() != null && nbOccurenceDeuxPoints > 3)
						{
							final EbDemande trResultante = createDemandeGroupe(
								propo,
								null,
								connectedUserService.getCurrentUser(),
									listPropo
							);
							trResultante.setConsolidationRuleLibelle(ebQrGroupe.getLibelle());
							propo.setEbDemandeResult(trResultante);
							propo.setListSelectedDemandeNum(propo.getListDemandeNum());

							ebDemandeRepository.save(trResultante);

							/**
							 * update initial trs with resultant id.
							 */
							ebDemandeRepository
								.updateGroupDemandeNumListEbDemande(
									getListDemandeNum(propo.getListDemandeNum()),
									trResultante.getEbDemandeNum()
								);

							// update the good date available by next pickup date.
							try
							{
								PlanTransportWSO transport = planTransportApiService
									.convertEbDemandeToTransport(trResultante);
								transport = planTransportApiService.getNextPickUpDateOfTransportWSO(transport);
								if (transport.getNextPickUpDate() != null)
									trResultante
										.setDateOfGoodsAvailability(transport.getNextPickUpDate());
							}
							catch (MyTowerException e)
							{
								e.printStackTrace();
							}

							listMarchandises.addAll(trResultante.getListMarchandises());

							if (trResultante.get_exEbDemandeTransporteurFinal() != null)
							{
								listQuote.add(trResultante.get_exEbDemandeTransporteurFinal());
							}
						}
						else
						{
							ebQrGroupePropositionRepository.deleteTrinitial(propo.getListDemandeNum());
						}
					}
					catch (Exception e)
					{
              e.printStackTrace();
					}
				}

				if (CollectionUtils.isNotEmpty(listMarchandises))
				{
					ebMarchandiseRepository.saveAll(listMarchandises);
				}

				if (CollectionUtils.isNotEmpty(listQuote))
				{
					exEbDemandeTransporteurRepository.saveAll(listQuote);
				}

				if (CollectionUtils.isNotEmpty(listPropo))
				{
					listPropo = listPropo
							.stream()
							.filter(
								p -> p.getEbDemandeResult() != null &&
									p.getEbDemandeResult().getEbDemandeNum() != null
							)
							.collect(Collectors.toList());
						ebQrGroupePropositionRepository.saveAll(listPropo);

					// flager la liste des demandes grouper
					updateIsGroupingInListDemandeByPropositions(listPropo, true);
				}
			}

			cancelTrsGroupedWithOneTr();
			valorizationService.groupedTrsValuation();
		}
	}

	private boolean hasSameCompatibilityCriteria(EbQrGroupe ebQrGroupe) {
		EbQrGroupe dbQrGroupe = ebQrGroupeRepository.getOne(ebQrGroupe.getEbQrGroupeNum());
		final Integer dbQrGroupeCompatibilityCriteriaSize = new Integer(ebQrGroupe.getCompatibilityCriteria().size());
		final Integer newQrGroupCompatibilityCriteriaSize = new Integer(dbQrGroupe.getCompatibilityCriteria().size());
		return dbQrGroupeCompatibilityCriteriaSize.equals(newQrGroupCompatibilityCriteriaSize)
				&& dbQrGroupe.getCompatibilityCriteria().containsAll(ebQrGroupe.getCompatibilityCriteria());
	}

	private void resetGroupageForInitialTrsAndCancelGroupedTr(List<Integer> triggers, EbQrGroupe ebQrGroupe) {
		if (isTriggerValid(ebQrGroupe, triggers)) {
			Set<Integer> listEbDemandeGroupedNum = ebQrGroupePropositionRepository
					.findEbQrGroupePropositionByebQrGroupe(Arrays.asList(ebQrGroupe.getEbQrGroupeNum()));

			if (listEbDemandeGroupedNum != null && listEbDemandeGroupedNum.size() > 0) {
				for (Integer trGroupedId : listEbDemandeGroupedNum) {
					if (trGroupedId != null)
						setxEbdemandeGroupToNullForInitialTrsAndCancelGroupedTr(trGroupedId);
				}
			}
		}
	}

	private void setxEbdemandeGroupToNullForInitialTrsAndCancelGroupedTr(Integer trGroupedId) {
		List<Integer> listIdsTrsUnitaires = ebDemandeRepository.selectDemandeNumInGroup(trGroupedId);
		if (listIdsTrsUnitaires != null && listIdsTrsUnitaires.size() > 0)
			ebDemandeRepository.setxEbDemandeGroupInTrsUnitairesToNull(listIdsTrsUnitaires);
		ebDemandeRepository.updateCancelStatusEbDemande(trGroupedId, CancelStatus.CANECLLED.getCode());
	}

	private boolean isTriggerValid(EbQrGroupe ebQrGroupe, List<Integer> triggers) {
		final boolean isUpdateTrigger = ebQrGroupe.getEbQrGroupeNum() != null
				&& triggers.contains(QrGroupeTrigger.ON_RULE_UPDATE.getCode());
		final boolean isCreationTrigger = ebQrGroupe.getEbQrGroupeNum() == null
				&& triggers.contains(QrGroupeTrigger.ON_RULE_CREATION.getCode());
		return triggers != null && (isUpdateTrigger || isCreationTrigger);
	}

	@Override
	public EbDemande createDemandeGroupe(
		EbQrGroupeProposition qrProp,
		Integer ebComagnieChargNum,
		EbUser connectedUser,
		List<EbQrGroupeProposition> props
	)
		throws JsonProcessingException
	{
		EbDemande resultQr = new EbDemande();
		List<Integer> trsInPropositionIds = getListDemandeNum(qrProp.getListDemandeNum());
		SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();
		criteria.setListEbDemandeNum(trsInPropositionIds);
		criteria.setEbUserNum(connectedUser.getEbUserNum());
		List<EbDemande> listDemande = daoPricing.getListDemandeGroupe(criteria);

		EbParty originParty = new EbParty();
		EbParty destParty = new EbParty();

		String codeCompany = connectedUser.getEbCompagnie().getCode();
		String refTransport = null;

		if (listDemande != null && !listDemande.isEmpty())
		{
			// Get list demande initiale id and
			List<Integer> listResultanteInGroup = listDemande
				.stream()
				.filter(d -> d.getxEbDemandeGroup() != null)
				.map(EbDemande::getxEbDemandeGroup)
				.collect(Collectors.toList());

			List<Integer> filterDuplicas = null;

			if (listResultanteInGroup != null && !listResultanteInGroup.isEmpty())
			{
				filterDuplicas = listResultanteInGroup.stream().distinct().collect(Collectors.toList());
			}

			if (filterDuplicas != null && filterDuplicas.size() == 1 && notOtherPropWithSameGroupedTrId(props, filterDuplicas.get(0)))
			{
				Integer demandeNum = filterDuplicas.get(0);
				resultQr.setEbDemandeNum(demandeNum);
				refTransport = StringUtils.leftPad(demandeNum.toString().toUpperCase(), 5, "0");
				setUnchangedfields(resultQr);

				// delete tr old propositions
				ebQrGroupePropositionRepository.deleteByXEbDemande(demandeNum);

				// Delete marchandises
				ebMarchandiseRepository.deleteListEbMarchandiseByEbDemandeNum(demandeNum);

				// Delete quote
				exEbDemandeTransporteurRepository.deleteListEbDemandeQuoteByEbDemandeNum(demandeNum);
			}
			else
			{
				resultQr.setEbDemandeNum(daoPricing.nextValEbDemande());
				refTransport = StringUtils
					.leftPad(resultQr.getEbDemandeNum().toString().toUpperCase(), 5, "0");
			}

			resultQr.setxEcStatut(StatutDemande.PND.getCode());
			resultQr.setxEcNature(NatureDemandeTransport.CONSOLIDATION.getCode());
			EbCompagnie comp = ebComagnieChargNum != null ?
				new EbCompagnie(ebComagnieChargNum) :
				new EbCompagnie(connectedUser.getEbCompagnie().getEbCompagnieNum());
			resultQr.setxEbCompagnie(comp);

			resultQr
				.setRefTransport(
					codeCompany
						.concat("-")
						.concat(NatureDemandeTransport.CONSOLIDATION.getLetter())
						.concat(refTransport)
				);
			resultQr.setxEcStatutGroupage(ConsolidationGroupage.NP.getCode());
			resultQr.setCodeConfigurationEDI(listDemande.get(0).getCodeConfigurationEDI());

			setCommonDataInDemandeResultante(resultQr, listDemande, originParty, destParty);

			/**
			 * create cotation
			 */
			createQuoteForGroupedTr(resultQr, listDemande);

			/**
			 * traiter units, total poids taxable, total weight, total volume et nombre de
			 * parcel
			 */
			computeUnitsMetricsForGroupedTrByInitialTrs(resultQr, listDemande);
		}

		return resultQr;
	}

	private boolean notOtherPropWithSameGroupedTrId(List<EbQrGroupeProposition> props, Integer trResultanteId) {
		Predicate<EbQrGroupeProposition> noneMatchPredicate = prop -> Objects.nonNull(prop.getEbDemandeResult()) &&
				prop.getEbDemandeResult().getEbDemandeNum().equals(trResultanteId);
    	return props.stream().noneMatch(noneMatchPredicate);
	}

	private void createQuoteForGroupedTr(EbDemande resultQr, List<EbDemande> listDemande)
	{
		List<ExEbDemandeTransporteur> listQuote = exEbDemandeTransporteurRepository
			.findAllByXEbDemande_ebDemandeNum(listDemande.get(0).getEbDemandeNum());

		if (listQuote != null && !listQuote.isEmpty())
		{
			ExEbDemandeTransporteur quote = new ExEbDemandeTransporteur();
			if (listQuote.get(0).getxTransporteur() != null)
				quote.setxTransporteur(new EbUser(listQuote.get(0).getxTransporteur().getEbUserNum()));
			if (listQuote.get(0).getxEbCompagnie() != null)
				quote
					.setxEbCompagnie(
						new EbCompagnie(listQuote.get(0).getxEbCompagnie().getEbCompagnieNum())
					);
			if (listQuote.get(0).getxEbEtablissement() != null)
				quote
					.setxEbEtablissement(
						new EbEtablissement(listQuote.get(0).getxEbEtablissement().getEbEtablissementNum())
					);
			quote.setStatus(StatutCarrier.RRE.getCode());
			quote.setxEbDemande(resultQr);
			resultQr.set_exEbDemandeTransporteurFinal(quote);
		}
	}

	private void setParty(EbDemande resultQr, EbParty originParty)
	{
		if (
			originParty.getCity() != null ||
				originParty.getAdresse() != null ||
				(originParty.getxEcCountry() != null &&
					originParty.getxEcCountry().getEcCountryNum() != null) ||
				(originParty.getZone() != null && originParty.getZone().getEbZoneNum() != null)
		)
			ebPartyRepository.save(originParty);
		else
			resultQr.setEbPartyOrigin(null);
	}

	private void setUnchangedfields(EbDemande demandeResult)
	{
		EbDemande demande = ebDemandeRepository
			.findById(demandeResult.getEbDemandeNum())
			.orElseThrow(
				() -> new MyTowerException(
					String.format("Tr not found for %s ", demandeResult.getEbDemandeNum())
				)
			);

			if (
				CollectionUtils.isNotEmpty(demande.getListAdditionalCost())
			)
			{
				//		set Additional cost
				demandeResult.setListAdditionalCost(demande.getListAdditionalCost());
			}
	}

	@Override
    public EbDemande setDataInResultingRequest(EbDemande resultQr, Integer checkedTrId, boolean isChecked) throws JsonProcessingException {
        String selectedTrIds = resultQr
            .getEbQrGroupe().getListPropositions().get(0).getListSelectedDemandeNum();

        List<EbDemande> checkedTrs = resultQr
            .getEbQrGroupe().getListPropositions().get(0).getListEbDemande().stream()
            .filter(d -> selectedTrIds.contains(":" + d.getEbDemandeNum() + ":"))
            .collect(Collectors.toList());

       final Integer resultanteTrId = isChecked ? resultQr.getEbDemandeNum() : null;
       ebDemandeRepository.updateTrGroupAndConsolidationEligibilityInInitialTr(checkedTrId,resultanteTrId,isChecked);

        if (resultQr.getEbPartyOrigin() == null) {
            resultQr.setEbPartyOrigin(new EbParty());
        }

        if (resultQr.getEbPartyDest() == null) {
            resultQr.setEbPartyDest(new EbParty());
        }

				if (CollectionUtils.isNotEmpty(checkedTrs))
				{
            setCommonDataInDemandeResultante(
                resultQr,
                checkedTrs,
                resultQr.getEbPartyOrigin(),
                resultQr.getEbPartyDest());
        }

        handleUnitInGroupedTr(resultQr, checkedTrs);
        resultQr = pricingService.unitValuation(resultQr);

        if (resultQr.getxEbCompagnie() != null) {
            resultQr.setxEbCompagnie(null);
        }

        return resultQr;
    }

	private ComputeTransportRequestUnitMetrics computeUnitsMetricsForGroupedTrByInitialTrs(EbDemande resultQr,
			List<EbDemande> listDemande) {
		if (CollectionUtils.isEmpty(listDemande))
		{
			throw new IllegalArgumentException("transport request list is empty");
		}

        // Delete grouped tr units.
        ebMarchandiseRepository.deleteListEbMarchandiseByEbDemandeNum(resultQr.getEbDemandeNum());

		listDemande.forEach(demande -> {
			demande.setListMarchandises(ebMarchandiseRepository
					.selectListEbMarchandiseByEbDemandeNumIn(Collections.singletonList(demande.getEbDemandeNum())));
		});
		
		List<EbMarchandise> listMarch = listDemande.stream().flatMap(demande -> demande.getListMarchandises().stream())
				.map(marchandise -> {
					EbMarchandise unitToSave = new EbMarchandise(marchandise);
					unitToSave.setEbMarchandiseNum(null);
					unitToSave.setEbDemande(new EbDemande(resultQr.getEbDemandeNum()));
					return unitToSave;
				}).collect(Collectors.toList());

		ComputeTransportRequestUnitMetrics unitsCount = new ComputeTransportRequestUnitMetrics(listMarch, resultQr);
		unitsCount.setUnits(listMarch);
        resultQr.setListMarchandises(listMarch);
        resultQr.setDg(isUnitsContainsDangerousGoods(listMarch));
		return unitsCount;
	}

    private Boolean isUnitsContainsDangerousGoods(List<EbMarchandise> listMarch) {
        return listMarch.stream().anyMatch(unit -> Objects.nonNull(unit.getDangerousGood()) &&
                !Enumeration.MarchandiseDangerousGood.NO.getCode().equals(unit.getDangerousGood()));
    }

    public void handleUnitInGroupedTr(EbDemande resultQr, List<EbDemande> listDemande) {
		ComputeTransportRequestUnitMetrics unitsMetrics = computeUnitsMetricsForGroupedTrByInitialTrs(resultQr,
				listDemande);
		ebMarchandiseRepository.saveAll(unitsMetrics.getUnits());
		ebDemandeRepository.updateTotalParcelAndVolumeAndTaxableWeightAndWeight(resultQr.getEbDemandeNum(),
				unitsMetrics.getTotalNbrParcel(), unitsMetrics.getTotalVolume(), unitsMetrics.getTotalTaxableWeight(),
				unitsMetrics.getTotalWeight());
	}

   
    public EbDemande setCommonDataInDemandeResultante(
        EbDemande resultQr,
        List<EbDemande> checkedTrs,
        EbParty originParty,
        EbParty destParty)
        throws JsonProcessingException {
        // restituer les éléments communs des demandes initiales
        boolean same = true;
        // si toutes les demandes cochées ont le meme owner alors je le mets
        // dans la demande resultante

        for (EbDemande d: checkedTrs) {
            if (d.getxEbCompagnie() != null) d.setxEbCompagnie(null);
        }

        for (EbDemande d: checkedTrs) {

            if (d.getUser() == null
                || (!d.getUser().getEbUserNum().equals(checkedTrs.get(0).getUser().getEbUserNum()))) {
                same = false;
                break;
            }

        }

        if (same) {
            EbUser user = new EbUser();
            user.setEmail(checkedTrs.get(0).getUser().getEmail());
            user.setEbUserNum(checkedTrs.get(0).getUser().getEbUserNum());
            resultQr.setUser(user);
        }

        same = true;

        for (EbDemande d: checkedTrs) {

            if (d.getxEbEtablissement() == null || (!d
                .getxEbEtablissement().getEbEtablissementNum()
                .equals(checkedTrs.get(0).getxEbEtablissement().getEbEtablissementNum()))) {
                same = false;
                break;
            }

        }

        if (same) {
            resultQr
                .setxEbEtablissement(
                    new EbEtablissement(checkedTrs.get(0).getxEbEtablissement().getEbEtablissementNum()));
        }

        same = true;

        for (EbDemande d: checkedTrs) {

            if (d.getxEcModeTransport() == null
                || !d.getxEcModeTransport().equals(checkedTrs.get(0).getxEcModeTransport())) {
                same = false;
                break;
            }

        }

        if (same) {
            resultQr.setxEcModeTransport(checkedTrs.get(0).getxEcModeTransport());
        }

        same = true;

        for (EbDemande d: checkedTrs) {

            if (d.getxEbTypeRequest() == null
                || !d.getxEbTypeRequest().equals(checkedTrs.get(0).getxEbTypeRequest())) {
                same = false;
                break;
            }

        }

        if (same) {
            resultQr.setxEbTypeRequest(checkedTrs.get(0).getxEbTypeRequest());
            resultQr.setTypeRequestLibelle(checkedTrs.get(0).getTypeRequestLibelle());
        }

        same = true;

        for (EbDemande d: checkedTrs) {

            if (d.getxEbTypeTransport() == null
                || !d.getxEbTypeTransport().equals(checkedTrs.get(0).getxEbTypeTransport())) {
                same = false;
                break;
            }

        }

        if (same) {
            resultQr.setxEbTypeTransport(checkedTrs.get(0).getxEbTypeTransport());
        }

        same = true;

        for (EbDemande d: checkedTrs) {

            if (d.getDateOfGoodsAvailability() == null
                || !d.getDateOfGoodsAvailability().equals(checkedTrs.get(0).getDateOfGoodsAvailability())) {
                same = false;
                break;
            }

        }

        if (same) {
            resultQr.setDateOfGoodsAvailability(checkedTrs.get(0).getDateOfGoodsAvailability());
        }

        same = true;

        for (EbDemande d: checkedTrs) {

            if (d.getDateOfArrival() == null
                || !d.getDateOfArrival().equals(checkedTrs.get(0).getDateOfArrival())) {
                same = false;
                break;
            }

        }

        if (same) {
            resultQr.setDateOfArrival(checkedTrs.get(0).getDateOfArrival());
        }

        if (checkedTrs.get(0).getxEbTypeFluxNum() != null) {
            same = true;

            for (EbDemande d: checkedTrs) {

                if (d.getxEbTypeFluxNum() == null
                    || (!d.getxEbTypeFluxNum().equals(checkedTrs.get(0).getxEbTypeFluxNum()))) {
                    same = false;
                    break;
                }

            }

            if (same) {
                resultQr.setxEbTypeFluxNum(checkedTrs.get(0).getxEbTypeFluxNum());
            }

            same = true;

            for (EbDemande d: checkedTrs) {

                if (d.getxEbIncotermNum() == null
                    || (!d.getxEbIncotermNum().equals(checkedTrs.get(0).getxEbIncotermNum()))) {
                    same = false;
                    break;
                }

            }

            if (same) {
                resultQr.setxEcIncotermLibelle(checkedTrs.get(0).getxEcIncotermLibelle());
                resultQr.setxEbIncotermNum(checkedTrs.get(0).getxEbIncotermNum());
            }

            same = true;

            for (EbDemande d: checkedTrs) {

                if (d.getxEbSchemaPsl() == null || !d
                    .getxEbSchemaPsl().getEbTtSchemaPslNum()
                    .equals(checkedTrs.get(0).getxEbSchemaPsl().getEbTtSchemaPslNum())) {
                    same = false;
                    break;
                }

            }

            if (same) {
                resultQr.setxEbSchemaPsl(checkedTrs.get(0).getxEbSchemaPsl());
                resultQr.setxEbTypeFluxNum(checkedTrs.get(0).getxEbTypeFluxNum());
                resultQr.setxEbTypeFluxDesignation(checkedTrs.get(0).getxEbTypeFluxDesignation());
            }

            same = true;

            for (EbDemande d: checkedTrs) {

                if (d.getCity() == null || !d.getCity().equals(checkedTrs.get(0).getCity())) {
                    same = false;
                    break;
                }

            }

            if (same) {
                resultQr.setCity(checkedTrs.get(0).getCity());
            }

        }

        same = true;

        for (EbDemande d: checkedTrs) {

            if (d.getXecCurrencyInvoice() == null || !d
                .getXecCurrencyInvoice().getEcCurrencyNum()
                .equals(checkedTrs.get(0).getXecCurrencyInvoice().getEcCurrencyNum())) {
                same = false;
                break;
            }

        }

        if (same) {
            resultQr.setXecCurrencyInvoice(checkedTrs.get(0).getXecCurrencyInvoice());
        }

        // origin
        if (checkedTrs.get(0).getEbPartyOrigin() != null) {
            originParty = this
                .getPartyCommonData(
                    checkedTrs,
                    originParty,
                    checkedTrs.get(0).getEbPartyOrigin(),
                    "ebPartyOrigin");
            resultQr.setEbPartyOrigin(originParty);
        }

        // Destination
        if (checkedTrs.get(0).getEbPartyDest() != null) {
            destParty = this
                .getPartyCommonData(
                    checkedTrs,
                    destParty,
                    checkedTrs.get(0).getEbPartyDest(),
                    "ebPartyDest");
            resultQr.setEbPartyDest(destParty);
        }

        // Catégories
        List<EbCategorie> listCateg = new ArrayList<>();
        List<Integer> listCategRemoved = new ArrayList<>();

        for (EbDemande d: checkedTrs) {

            if (d.getListCategories() != null && !d.getListCategories().isEmpty()) {

                if (!listCateg.isEmpty()) {

                    for (EbCategorie oldCatg: d.getListCategories()) {

                        if (!listCategRemoved.contains(oldCatg.getEbCategorieNum())) {
                            EbCategorie catg = listCateg
                                .stream()
                                .filter(newCatg -> oldCatg.getEbCategorieNum().equals(newCatg.getEbCategorieNum()))
                                .findFirst().orElse(null);

                            if (catg == null) {
                                listCateg.add(oldCatg);
                            }
                            else {
                                EbLabel newLabel = new EbLabel();

                                if (!catg.getLabels().isEmpty()) {
                                    newLabel = new ArrayList<EbLabel>(catg.getLabels()).get(0);
                                }

                                EbLabel oldLabel = new EbLabel();

                                if (!oldCatg.getLabels().isEmpty()) {
                                    oldLabel = new ArrayList<EbLabel>(oldCatg.getLabels()).get(0);
                                }

                                if (newLabel.getEbLabelNum() == null && oldLabel.getEbLabelNum() != null) {

                                    for (EbCategorie ca: listCateg) {

                                        if (!ca.getLabels().isEmpty() && new ArrayList<EbLabel>(ca.getLabels())
                                            .get(0).equals(newLabel.getEbLabelNum())) {
                                            Set<EbLabel> listLabels = new HashSet<>();
                                            listLabels.add(newLabel);
                                            ca.setLabels(listLabels);
                                        }

                                    }

                                }
                                else if (newLabel != null && oldLabel != null &&

                                    newLabel.getEbLabelNum() != null && oldLabel.getEbLabelNum() != null
                                    && !newLabel.getEbLabelNum().equals(oldLabel.getEbLabelNum())) {
                                    listCateg.remove(catg);
                                    listCategRemoved.add(catg.getEbCategorieNum());
                                }

                            }

                        }

                    }

                }
                else if (d.getListCategories() != null) {
                    listCateg.addAll(d.getListCategories());
                }

            }

        }

        resultQr.setListCategories(listCateg);

				// Champs paramétrable
				List<CustomFields> listCustomFields = new ArrayList<>();
				ObjectMapper objectMapper = new ObjectMapper();

				for (EbDemande transportRequest : checkedTrs)
				{
					if (transportRequest.getCustomFields() != null)
					{
						List<CustomFields> customFieldsOfTr = objectMapper
							.readValue(
								transportRequest.getCustomFields(),
								new TypeReference<ArrayList<CustomFields>>()
								{}
							);

						if (CollectionUtils.isNotEmpty(listCustomFields))
						{
							listCustomFields.retainAll(customFieldsOfTr);
						}

						if (CollectionUtils.isEmpty(listCustomFields))
						{
							listCustomFields.addAll(customFieldsOfTr);
						}
					}
				}

				resultQr.setCustomFields(objectMapper.writeValueAsString(listCustomFields));

        // customer ref
        String customerReference = null;

        if (checkedTrs != null && !checkedTrs.isEmpty()) {
            customerReference = "";

            for (EbDemande d: checkedTrs) {

                if (d.getCustomerReference() != null) {
                    customerReference += d.getCustomerReference() + ", ";
                }

            }

            if (customerReference.length() > 2) {
                customerReference = customerReference.substring(0, customerReference.length() - 2);
            }

        }

        resultQr.setCustomerReference(customerReference);

        // informations complémentaires

        String commentChargeur = null;

        if (checkedTrs != null && !checkedTrs.isEmpty()) {
            commentChargeur = "";

            for (EbDemande d: checkedTrs) {

                if (d.getCommentChargeur() != null) {
                    commentChargeur += d.getRefTransport() + " : " + d.getCommentChargeur() + ", ";
                }

            }

            if (commentChargeur.length() > 2) {
                commentChargeur = commentChargeur.substring(0, commentChargeur.length() - 2);
            }

        }

        resultQr.setCommentChargeur(commentChargeur);

        // CostCenter xEbCostCenter
        same = true;

        for (EbDemande d: checkedTrs) {

            if (d.getxEbCostCenter() == null
                || !d.getxEbCostCenter().equals(checkedTrs.get(0).getxEbCostCenter())) {
                same = false;
                break;
            }

        }

        if (same) {
            resultQr.setxEbCostCenter(checkedTrs.get(0).getxEbCostCenter());
        }

				setParty(resultQr, originParty);
				setParty(resultQr, destParty);

				//				listTypeDocuments
				if (CollectionUtils.isNotEmpty(checkedTrs.get(0).getListTypeDocuments()))
				{
					resultQr.setListTypeDocuments(checkedTrs.get(0).getListTypeDocuments());
				}

        return resultQr;
    }

    public EbParty getPartyCommonData(
        List<EbDemande> listDemandeChecked,
        EbParty party,
        EbParty initParty,
        String partyPropertyName) {
        boolean sameCity = true;
        boolean sameCountry = true;
        boolean sameZone = true;
        boolean sameAdr = true;
        boolean sameCompany = true;
        boolean sameZipCode = true;
        boolean sameAirport = true;

        for (EbDemande d: listDemandeChecked) {
            EbParty demandeParty = (EbParty) PropertyAccessorFactory
                .forBeanPropertyAccess(d).getPropertyValue(partyPropertyName);

            if (demandeParty == null ||
                demandeParty.getCity() == null || !demandeParty.getCity().equals(initParty.getCity())) {
                sameCity = false;
            }

            if (demandeParty == null ||
                demandeParty.getxEcCountry() == null ||
                !demandeParty.getxEcCountry().getEcCountryNum().equals(initParty.getxEcCountry().getEcCountryNum())) {
                sameCountry = false;
            }

            if (demandeParty == null ||
                demandeParty.getAirport() == null || !demandeParty.getAirport().equals(initParty.getAirport())) {
                sameAirport = false;
            }

            if (demandeParty == null || demandeParty.getZone() == null) {

                if (demandeParty.getZone() != null && initParty.getZone() != null) {

                    if (demandeParty.getZone().getEbZoneNum().equals(initParty.getZone().getEbZoneNum())) {
                        sameZone = false;
                    }

                }

            }

            if (demandeParty == null || demandeParty.getAdresse() == null
                || !demandeParty.getAdresse().equals(initParty.getAdresse())) {
                sameAdr = false;
            }

            if (demandeParty == null || demandeParty.getCompany() == null
                || !demandeParty.getCompany().equals(initParty.getCompany())) {
                sameCompany = false;
            }

            if (demandeParty == null || demandeParty.getZipCode() == null
                || !demandeParty.getZipCode().equals(initParty.getZipCode())) {
                sameZipCode = false;
            }

        }

        if (sameCity) party.setCity(initParty.getCity());
        if (sameCountry) party.setxEcCountry(initParty.getxEcCountry());

        if (sameZone) {

            if (initParty.getZone() != null && initParty.getZone().getEbZoneNum() != null) {
                party.setZone(new EbPlZone(initParty.getZone().getEbZoneNum()));
            }

        }

        if (sameAdr) party.setAdresse(initParty.getAdresse());
        if (sameCompany) party.setCompany(initParty.getCompany());
        if (sameZipCode) party.setZipCode(initParty.getZipCode());
        if (sameAirport) party.setAirport(initParty.getAirport());
        return party;
    }

		@Override
		public void refreshQrGroupe(
			List<Integer> listEbQrGroupeNum,
			Integer triggerType,
			Integer ebComagnieChargNum,
			EbUser connectedUser
		)
		{
			String consolidationLogs = "\n******************* START CONSOLIDATION *********************\n";
			consolidationLogs += "\nIn method refreshQrGroupe with :" +
				"\nusername : " + connectedUser.getNomPrenom() + "\ntriggerType : " + triggerType +
				"\nebComagnieChargNum : " + ebComagnieChargNum + "\nlist groupe rules : " +
				listEbQrGroupeNum;

			List<EbDemande> listTrResultante = new ArrayList<>();
			EbCompagnie xEbCompagnie;

			if (ebComagnieChargNum != null)
			{
				xEbCompagnie = ebCompagnieRepository.getOne(ebComagnieChargNum);
			}
			else
			{
				xEbCompagnie = ebCompagnieRepository
					.getOne(connectedUser.getEbCompagnie().getEbCompagnieNum());
				ebComagnieChargNum = xEbCompagnie.getEbCompagnieNum();
			}

			SearchCriteria criteria = new SearchCriteria();
			criteria.setListIdObject(listEbQrGroupeNum);
			criteria.setIncludeGlobalValues(false);
			criteria.setEbCompagnieNum(ebComagnieChargNum);

			List<EbQrGroupe> listGroupe = daoPricing
				.getGroupsAndPropositions(criteria, true)
				.stream()
				.filter(ebQrGroupe -> CollectionUtils.isNotEmpty(ebQrGroupe.getCompatibilityCriteria()))
				.collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(listGroupe))
			{
				List<EbQrGroupeProposition> listGroupPropo = new ArrayList<>();
				List<Integer> listGroupPropoToDelete = new ArrayList<>();

				// déflager la liste des demandes initiales dans les propositions de groupage
				for (EbQrGroupe ebQrGroupe : listGroupe)
				{
					List<Integer> triggers = null;

					if (ebQrGroupe.getTriggers() != null && ebQrGroupe.getTriggers().length() > 0)
					{
						triggers = Arrays
							.asList(ebQrGroupe.getTriggers().split(","))
							.stream()
							.map(Integer::parseInt)
							.collect(Collectors.toList());
					}

					if (
						triggers != null &&
							triggerType == null && triggers.contains(QrGroupeTrigger.ON_MANUAL_DEMANDE.getCode())
					)
					{
						listGroupPropo.addAll(ebQrGroupe.getListPropositions());
					}

				}

				if (!listGroupPropo.isEmpty())
				{
					updateIsGroupingInListDemandeByPropositions(listGroupPropo, false);
				}

				listGroupPropo = new ArrayList<>();

				for (EbQrGroupe ebQrGroupe : listGroupe)
				{
					consolidationLogs += "\n\n----------- CONSOLIDATION RULE NAME : " +
						ebQrGroupe.getLibelle() + " -----------";

					List<Integer> triggers = null;

					if (ebQrGroupe.getTriggers() != null && ebQrGroupe.getTriggers().length() > 0)
					{
						triggers = Arrays
							.asList(ebQrGroupe.getTriggers().split(","))
							.stream()
							.map(Integer::parseInt)
							.collect(Collectors.toList());
					}

					if (
						triggers != null &&
							((triggerType == null &&
								triggers.contains(QrGroupeTrigger.ON_MANUAL_DEMANDE.getCode())) ||
								(triggerType != null && triggers.contains(triggerType)))
					)
					{

						// cas de refrechissement apres creation/Maj d'une demande
						if (triggerType != null && triggers.contains(triggerType))
						{

							if (
								ebQrGroupe.getListPropositions() != null &&
									!ebQrGroupe.getListPropositions().isEmpty()
							)
							{
								updateIsGroupingInListDemandeByPropositions(
									ebQrGroupe.getListPropositions(),
									false
								);
								listGroupPropoToDelete.add(ebQrGroupe.getEbQrGroupeNum());
							}

						}

						daoPricing.setPropositionGroupe(ebQrGroupe, ebComagnieChargNum);

						List<EbQrGroupeProposition> listPropo = ebQrGroupe.getListPropositions();

						EbDemande trResultante = new EbDemande();

						if (listPropo != null && !listPropo.isEmpty())
						{
							consolidationLogs += "\n\n------------- LIST OF PROPOSITIONS -------------";
							int i = 1;
							for (EbQrGroupeProposition prop : listPropo)
							{
								consolidationLogs += "\n\n********** Proposition " + "(" + i++ + ")" + "**********";
								consolidationLogs += "\nListDemandeNum : " + prop.getListDemandeNum();
								try
								{
									List<EbMarchandise> listMarchandises = new ArrayList<>();

									prop.getEbQrGroupe().setxEbCompagnie(xEbCompagnie);

									if (prop.getListDemandeNum() != null && !prop.getListDemandeNum().isEmpty())
									{
										trResultante = createDemandeGroupe(prop, ebComagnieChargNum, connectedUser, listPropo);
										trResultante.setConsolidationRuleLibelle(ebQrGroupe.getLibelle());
										prop.setEbDemandeResult(new EbDemande(trResultante.getEbDemandeNum()));
										prop.setListSelectedDemandeNum(prop.getListDemandeNum());
										consolidationLogs += "\ntrResultante : " + trResultante.getEbDemandeNum();

										if (prop.getEbDemandeResult() != null)
										{
											listTrResultante.add(trResultante);

											if (trResultante.getListMarchandises() != null)
											{
												listMarchandises.addAll(trResultante.getListMarchandises());
											}

											ebDemandeRepository.save(trResultante);
											if (!listMarchandises.isEmpty())
											{
												ebMarchandiseRepository.saveAll(listMarchandises);
											}

											/**
											 * update initial trs with resultant id.
											 */
											ebDemandeRepository
												.updateGroupDemandeNumListEbDemande(
													getListDemandeNum(prop.getListDemandeNum()),
													trResultante.getEbDemandeNum()
												);

											if (trResultante.get_exEbDemandeTransporteurFinal() != null)
											{
												exEbDemandeTransporteurRepository
													.save(trResultante.get_exEbDemandeTransporteurFinal());
											}
										}
									}
								}
								catch (Exception e)
								{
									throw new MyTowerException(
										String
											.format(
												"can't create Tr for proposition with id %s",
												prop.getEbQrGroupePropositionNum()
											)
									);
								}

							}

							if (CollectionUtils.isNotEmpty(listPropo))
							{
								listGroupPropo = listPropo
									.stream()
									.filter(
										p -> p.getEbDemandeResult() != null &&
											p.getEbDemandeResult().getEbDemandeNum() != null
									)
									.collect(Collectors.toList());
								ebQrGroupePropositionRepository.saveAll(listGroupPropo);

								// flager la liste des demandes grouper
								updateIsGroupingInListDemandeByPropositions(listGroupPropo, true);
							}
						}

					}

					consolidationLogs += "\n--------------- END RULE -------------------\n";
				}
				cancelTrsGroupedWithOneTr();
				valorizationService.groupedTrsValuation();
			}
			consolidationLogs += "\n------------------ END OF CONSOLIDATION ------------------\n\n";

			LOGGER.info(consolidationLogs);
		}

		private void cancelTrsGroupedWithOneTr()
		{
			List<Integer> trsGroupedWithOneUnitTr = ebDemandeRepository
				.getTrsGroupedWithOneTr(
					Enumeration.NatureDemandeTransport.CONSOLIDATION.getCode(),
					Enumeration.StatutDemande.PND.getCode(),
					ONE_INITIAL_TR_IN_GROUP
				);

			if (CollectionUtils.isNotEmpty(trsGroupedWithOneUnitTr))
			{
				List<Integer> listUnitTrs = ebDemandeRepository
					.getInitialIdsByGroupedIds(trsGroupedWithOneUnitTr);

				ebDemandeRepository.setListOfInitialTrResultanteTrToNull(listUnitTrs);

				ebDemandeRepository
					.setCancelStatusByTrIds(
						trsGroupedWithOneUnitTr,
						Enumeration.CancelStatus.CANECLLED.getCode()
					);
			}
		}

		public void sendGroupedTrToKafka(Integer initialTrId, Integer idTrResult)
		{
			if (initialTrId != null)
			{
				EbDemande initialTr = ebDemandeRepository
					.findById(initialTrId)
					.orElseThrow(
						() -> new MyTowerException(
							String.format("TransportRequest with id %s not found", initialTrId)
						)
					);

				if (initialTr.getxEbDemandeGroup() != null)
				{
					kafkaObjectService
						.buildAndSendTransportRequestToKafka(
							initialTr.getxEbDemandeGroup(),
							initialTr.getUser(),
							false
						);
				}
				else
				{
					kafkaObjectService
						.buildAndSendTransportRequestToKafka(
							initialTr.getEbDemandeNum(),
							initialTr.getUser(),
							false
						);
				}
				if (idTrResult != null && !idTrResult.equals(initialTr.getxEbDemandeGroup()))
				{
					kafkaObjectService
						.buildAndSendTransportRequestToKafka(idTrResult, initialTr.getUser(), false);
				}
			}
		}

		@Override
		public boolean deleteQrGroupe(Integer ebQrGroupeNum) {
			EbQrGroupe dbGroupe = ebQrGroupeRepository.findById(ebQrGroupeNum).orElseThrow(
					() -> new IllegalArgumentException(String.format("QrGroupe with %s not found", ebQrGroupeNum)));
			dbGroupe.setIsDeleted(true);
			dbGroupe.setIsActive(false);
			return Objects.nonNull(ebQrGroupeRepository.save(dbGroupe));
		}

    @Override
    public Map<String, Object>
        getListQrGroupeData(SearchCriteria criteria) throws JsonProcessingException, IOException {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);
        Integer ebCompagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();
        Map<String, Object> result = new HashMap<String, Object>();

        result.put("listVentilation", Enumeration.QrGroupeVentilation.getListQrGroupeVentilation());

        result.put("listTrigger", Enumeration.QrGroupeTrigger.getListGroupeTrigger());

        result.put("listOutput", Enumeration.QrGroupeOutput.getListGroupeOutput());

        result.put("listField", Enumeration.GroupeField.getListGroupeField());

        result.put("listFieldPsl", daoEbTtCompanyPsl.listTtCompanyPsl(criteria));

        result.put("listFunction", Enumeration.GroupeFunction.getListGroupeFunction());

        result.put("listSousFunction", Enumeration.GroupeSousFunction.getListGroupeSousFunction());

        Map<String, String> mapValues = new HashMap<String, String>();
        result.put("fieldValues", mapValues);
        ObjectMapper mapper = new ObjectMapper();
        mapValues
            .put(
                "field" + GroupeField.MODE_DE_TRANSPORT.getCode(),
                Enumeration.ModeTransport.getListModeTransportCodeLibelle());

        mapValues
            .put(
                "field" + GroupeField.TYPE_DE_TRANSPORT.getCode(),
                mapper.writeValueAsString(ebTypeTransportRepository.findAllTypeTransportCodeLibelle()));

        mapValues
            .put(
                "field" + GroupeField.PART_NUMBER.getCode(),
                mapper
                    .writeValueAsString(
                        this.ebMarchandiseRepository
                            .getAllPartNumbersByCompagnieNum(ebCompagnieNum).stream()
                            .filter(MyTowerCollectionUtils.distinctByKey(QrGroupeObject::getLibelle))
                            .collect(Collectors.toList())));

        if (connectedUser != null && connectedUser.getEbCompagnie() != null) {
            mapValues
                .put(
                    "field" + GroupeField.NIVEAU_DE_SERVICE.getCode(),
                    mapper
                        .writeValueAsString(
                            ebTypeRequestRepository
                                .findAllTypeRequestByCompagnieCodeLibelle(
                                    connectedUser.getEbCompagnie().getEbCompagnieNum())));

            List<EbCategorie> listCategorieInit = ebCategorieRepository
                .findDistinctByEtablissementEbCompagnieEbCompagnieNum(
                    connectedUser.getEbCompagnie().getEbCompagnieNum());

            if (listCategorieInit != null && !listCategorieInit.isEmpty()) {
                List<QrGroupeCategorie> listCategorie = new ArrayList<QrGroupeCategorie>();

                for (EbCategorie categorie: listCategorieInit) {
                    listCategorie.add(new QrGroupeCategorie(categorie));
                }

                mapValues.put("field" + GroupeField.CATEGORIES.getCode(), mapper.writeValueAsString(listCategorie));
            }

            criteria.setConnectedUserNum(connectedUser.getEbUserNum());
            criteria.setEbUserNum(connectedUser.getEbUserNum());

            criteria.setContact(true);

            List<EbRelation> listRelations = userService.selectListEbEtablissementOfEbRelation(criteria);

            List<QrGroupeObject> listCompagnieEtabTransporteur = new ArrayList<>();

            if (listRelations != null && !listRelations.isEmpty()) {

                for (EbRelation relation: listRelations) {

                    if (relation.getxEbEtablissementHost() != null) {
                        listCompagnieEtabTransporteur
                            .add(
                                new QrGroupeObject(
                                    relation.getxEbEtablissementHost().getEbEtablissementNum(),
                                    relation.getxEbEtablissementHost().getEbCompagnie().getNom() + " - "
                                        + relation.getxEbEtablissementHost().getNom()));
                    }

                }

            }

            if (!listCompagnieEtabTransporteur.isEmpty()) {
                // get list de transporteurs par compagnie
                mapValues
                    .put(
                        "field" + GroupeField.TRANSPORTEUR.getCode(),
                        mapper.writeValueAsString(listCompagnieEtabTransporteur));
            }

            // get list des zones par compagnie
            List<QrGroupeObject> listEbPlZoneByCompagnieNum = ebZoneRepository
                .findListEbPlZoneByCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());

            if (listEbPlZoneByCompagnieNum != null && !listEbPlZoneByCompagnieNum.isEmpty()) {
                mapValues
                    .put(
                        "field" + GroupeField.ZONE_ORIGINE_GT.getCode(),
                        mapper.writeValueAsString(listEbPlZoneByCompagnieNum));
            }

            EbCustomField customField = ebCustomFieldRepository
                .findFirstByXEbCompagnie(connectedUser.getEbCompagnie().getEbCompagnieNum());

            if (customField != null && customField.getFields() != null) {
                List<CustomFields> listFields = new Gson()
                    .fromJson(customField.getFields(), new TypeToken<List<CustomFields>>() {
                    }.getType());

                if (listFields != null && !listFields.isEmpty()) {
                    List<QrGroupeObject> listCustomField = new ArrayList<>();

                    for (CustomFields field: listFields) {
                        listCustomField.add(new QrGroupeObject(field.getNum(), field.getLabel()));
                    }

                    mapValues
                        .put("field" + GroupeField.CUSTOM_FIELDS.getCode(), mapper.writeValueAsString(listCustomField));
                }

            }

        }

        mapValues.put("field" + GroupeField.ELIGIBILITE.getCode(), Enumeration.Insurance.toJsonList());
        mapValues.put("field" + GroupeField.DGR_TYPE.getCode(), Enumeration.MarchandiseDangerousGood.toJsonList());
        mapValues.put("field" + GroupeField.STATUS.getCode(), Enumeration.StatutDemande.toJsonList());
        return result;
    }

    @Override
    public List<EbQrGroupe> getListQrGroupeWithDemande(SearchCriteriaPricingBooking criteria) {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);
				List<EbQrGroupe> listEbQrGroupe = daoPricing
					.getGroupsAndPropositions(criteria, true);

        List<Integer> listDemandeNum = new ArrayList<>();

        for (EbQrGroupe grp: listEbQrGroupe) {

            if (grp.getListPropositions() != null) {

                for (EbQrGroupeProposition prop: grp.getListPropositions()) {
                    List<Integer> listDemandeInit = null;

                    if (prop.getListDemandeNum() != null && !prop.getListDemandeNum().isEmpty()) {
                        listDemandeInit = getListDemandeNum(prop.getListDemandeNum());

                        Integer nombreDemandes = listDemandeInit.size();
                        prop.setCurrentPage(ConsolidationConstants.DEFAULT_PAGE_NUMBER);
                        prop.setNombreDemandesPerPage(ConsolidationConstants.DEFAULT_SIZE);

                        prop.setNombreDemandes(nombreDemandes);

                        prop
                            .setNombrePages(
                                (nombreDemandes / prop.getNombreDemandesPerPage())
                                    + ((nombreDemandes % prop.getNombreDemandesPerPage()) != 0 ? 1 : 0));

                        Integer indexFrom = (prop.getCurrentPage() * prop.getNombreDemandesPerPage());

                        Integer indexTo = (prop.getCurrentPage() * prop.getNombreDemandesPerPage())
                            + prop.getNombreDemandesPerPage() < nombreDemandes ?

                                (prop.getCurrentPage() * prop.getNombreDemandesPerPage())
                                    + prop.getNombreDemandesPerPage() :
                                nombreDemandes;

                        listDemandeNum.addAll(listDemandeInit.subList(indexFrom, indexTo));
                    }

                    if (prop.getEbDemandeResult() != null
                        && prop.getEbDemandeResult().getEbDemandeNum() != null) listDemandeNum
                            .add(prop.getEbDemandeResult().getEbDemandeNum());
                }

            }

        }

        criteria.setModule(Enumeration.Module.PRICING.getCode());
        criteria.setListEbDemandeNum(listDemandeNum);
        criteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
        List<EbDemande> listEbDemande = daoPricing.getListDemandeGroupe(criteria);

        pricingService.setListDemandeQuote(criteria, listEbDemande, true);
        List<EbMarchandise> listMarchandise = new ArrayList<EbMarchandise>();
        if (listDemandeNum != null && listDemandeNum.size() > 0) listMarchandise = ebMarchandiseRepository
            .selectListEbMarchandiseByEbDemandeNumIn(listDemandeNum);

        for (EbDemande d: listEbDemande) {

            for (EbQrGroupe qrGroupe: listEbQrGroupe) {

                if (qrGroupe.getListPropositions() != null) {

                    for (EbQrGroupeProposition propo: qrGroupe.getListPropositions()) {
                        Integer indexOfDemande = propo.getListDemandeNum().indexOf(":" + d.getEbDemandeNum() + ":");
                        boolean b1 = indexOfDemande >= 0
                            && ((propo.getNombreDemandes() <= propo.getNombreDemandesPerPage())
                                || ((propo.getNombreDemandes() > propo.getNombreDemandesPerPage()) &&

                                    indexOfDemande < getLastIndexOfDemande(
                                        propo.getListDemandeNum(),
                                        propo.getNombreDemandesPerPage())));

                        boolean b2 = propo.getEbDemandeResult() != null
                            && propo.getEbDemandeResult().getEbDemandeNum().equals(d.getEbDemandeNum());

                        if (b1 || b2) {
                            if (propo.getListEbDemande() == null) propo.setListEbDemande(new ArrayList<EbDemande>());

                            if (d.getListMarchandises() == null || d.getListMarchandises().isEmpty()) {

                                for (EbMarchandise mr: listMarchandise) {

                                    if (mr.getEbDemande().getEbDemandeNum().equals(d.getEbDemandeNum())) {
                                        if (d.getListMarchandises() == null) d
                                            .setListMarchandises(new ArrayList<EbMarchandise>());
                                        d.getListMarchandises().add(mr);
                                    }

                                }

                            }

                        }

                        if (b1) {
                            EbDemande copy = new EbDemande(d);
                            propo.getListEbDemande().add(copy);
                        }

                        if (b2) {
                            EbDemande copy = new EbDemande(d);
                            propo.setEbDemandeResult(copy);
                        }

                    }

                }

            }

        }

        // mettre juste le id de la demande dans les marchandises pour ne plus
        // avoir de
        // probleme d'identifiants répétés dans le json
        for (EbDemande d: listEbDemande) {

            if (d.getListMarchandises() != null) {

                for (EbMarchandise mr: d.getListMarchandises()) {
                    mr.setEbDemande(new EbDemande(d.getEbDemandeNum()));
                }

            }

        }

        return listEbQrGroupe;
    }

    @Override
		public List<EbDemande> getListDemandeInProposition(
			EbQrGroupeProposition prop,
			EbUser connectedUser
		)
		{

        SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();
        criteria.setConnectedUserNum(connectedUser.getEbUserNum());
        criteria.setIncludeGlobalValues(false);

        List<Integer> listDemandeNum = new ArrayList<>();

        criteria.setModule(Enumeration.Module.PRICING.getCode());

        List<Integer> listDemandeInit = getListDemandeNum(prop.getListDemandeNum());

        Integer nombreDemandes = listDemandeInit.size();
        prop.setNombreDemandesPerPage(ConsolidationConstants.DEFAULT_SIZE);

        if (prop.getCurrentPage() == null) {
            prop.setCurrentPage(ConsolidationConstants.DEFAULT_PAGE_NUMBER);
        }

        prop.setNombreDemandes(nombreDemandes);

        prop
            .setNombrePages(
                (nombreDemandes / prop.getNombreDemandesPerPage())
                    + ((nombreDemandes % prop.getNombreDemandesPerPage()) != 0 ? 1 : 0));

        Integer indexFrom = (prop.getCurrentPage() * prop.getNombreDemandesPerPage());
        Integer indexTo = (prop.getCurrentPage() * prop.getNombreDemandesPerPage())
            + prop.getNombreDemandesPerPage() < nombreDemandes ?
                (prop.getCurrentPage() * prop.getNombreDemandesPerPage()) + prop.getNombreDemandesPerPage() :
                nombreDemandes;

        listDemandeNum.addAll(listDemandeInit.subList(indexFrom, indexTo));

        criteria.setListEbDemandeNum(listDemandeNum);
        criteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());

        List<EbDemande> listEbDemande = daoPricing.getListDemandeGroupe(criteria);

        pricingService.setListDemandeQuote(criteria, listEbDemande, true);
        List<EbMarchandise> listMarchandise = null;
				if (CollectionUtils.isNotEmpty(listDemandeNum))
					listMarchandise = ebMarchandiseRepository
            .selectListEbMarchandiseByEbDemandeNumIn(listDemandeNum);

        if (listMarchandise != null && !listMarchandise.isEmpty()) {

            for (EbDemande d: listEbDemande) {
                List<EbMarchandise> listMarchandiseFinal = new ArrayList<EbMarchandise>();

                for (EbMarchandise march: listMarchandise) {

                    if (march.getEbDemande() != null
                        && d.getEbDemandeNum().equals(march.getEbDemande().getEbDemandeNum())) {
                        march.setEbDemande(new EbDemande(d.getEbDemandeNum()));
                        listMarchandiseFinal.add(march);
                    }

                }

                d.setListMarchandises(listMarchandiseFinal);
            }

        }

        return listEbDemande;
    }

    public List<Integer> getListDemandeNum(String listDemandeNumStr) {
        List<Integer> listDemandeNum = new ArrayList<Integer>();

        String[] listDemandeStr = listDemandeNumStr.split("::");

        for (String str: listDemandeStr) {
            Integer num = Integer.valueOf(str.replaceAll("[^\\d.]", ""));

            boolean exists = listDemandeNum.stream().filter(it -> it.equals(num)).findFirst().isPresent();
            if (!exists) listDemandeNum.add(num);
        }

        return listDemandeNum;
    }

    private void
        updateIsGroupingInListDemandeByPropositions(List<EbQrGroupeProposition> listPropos, Boolean isGrouped) {
        List<Integer> listEbDemandeNum = getListEbDemandeNumInPropositions(listPropos);

        if (listEbDemandeNum != null && listEbDemandeNum.size() > 1) {
            ebDemandeRepository.updateIsGroupingInEbDemandeByListEbDemandeNum(listEbDemandeNum, isGrouped);
        }

    }

    public Integer getLastIndexOfDemande(String str, Integer nbrElemPerPage) {
        Integer indexOfLast2points = 2 * nbrElemPerPage;
        Integer nbr = 1, index = 0;

        for (int i = 1; i < str.length(); i++) {

            if (str.charAt(i) == ':' && nbr < 6) {
                nbr++;
            }

            if (nbr == indexOfLast2points) {
                index = i;
                break;
            }

        }

        return index;
    }

    @Override
    public void cancelConsolidation(Integer ebDemandeNum) {
        String listDemandeNumStr = ebQrGroupePropositionRepository
            .findEbQrGroupePropositionByEbDemandeNum(ebDemandeNum);

        List<Integer> listDemandeInitNum = listDemandeNumStr != null && !listDemandeNumStr.isEmpty() ?
            getListDemandeNum(listDemandeNumStr) :
            null;

        if (listDemandeInitNum != null) {
            ebDemandeRepository.updateIsGroupingInEbDemandeByListEbDemandeNum(listDemandeInitNum, false);
        }

        ebDemandeRepository.updateCancelStatusEbDemande(ebDemandeNum, Enumeration.CancelStatus.CANECLLED.getCode());
    }

    @Override
    public void updateEligibleForConsolidation(Integer ebDemandeNum) {
        cancelConsolidation(ebDemandeNum);
        String listDemandeNumStr = ebQrGroupePropositionRepository
            .findEbQrGroupePropositionByEbDemandeNum(ebDemandeNum);
        List<Integer> listDemandeInitNum = listDemandeNumStr != null && !listDemandeNumStr.isEmpty() ?
            getListDemandeNum(listDemandeNumStr) :
            null;

        if (listDemandeInitNum != null) {
            ebDemandeRepository.updateEligibleForConsolidation(listDemandeInitNum, true, false);
        }

    }

    @Override
    public List<Integer> getListDemandeByGroupingRule(Integer ebQrGroupeNum) {
        return daoPricing.getListDemandeByGroupingRule(ebQrGroupeNum);
    }

	@Override
	public EbQrGroupe activateQrGroupe(Integer ebQrGroupeNum) throws JsonProcessingException {
		EbQrGroupe dbGroupe = ebQrGroupeRepository.findById(ebQrGroupeNum).orElseThrow(
				() -> new IllegalArgumentException(String.format("QrGroupe with %s not found", ebQrGroupeNum)));
		dbGroupe.setIsActive(true);
		dbGroupe.setIsDeleted(false);
		return ebQrGroupeRepository.save(dbGroupe);
	}

	@Override
	public void deleteQrGroupeAndQrGroupeUnconfirmedPropositions(Integer ebQrGroupeNum) {
		try {

			EbQrGroupe qrGroupe = ebQrGroupeRepository.findById(ebQrGroupeNum).orElseThrow(
					() -> new IllegalArgumentException(String.format("QrGroupe with %s not found", ebQrGroupeNum)));

			List<EbQrGroupeProposition> propositions = qrGroupe.getListPropositions();
			propositions = propositions.stream()
					.filter(prop -> !prop.getStatut().equals(Enumeration.GroupePropositionStatut.CONFIRMED.getCode()))
					.collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(propositions)) {
				updateIsGroupingInListDemandeByPropositions(propositions, false);

				propositions.forEach(prop -> setxEbdemandeGroupToNullForInitialTrsAndCancelGroupedTr(
						prop.getEbDemandeResult().getEbDemandeNum()));

				List<Integer> propositionsIds = propositions.stream()
						.map(EbQrGroupeProposition::getEbQrGroupePropositionNum).collect(Collectors.toList());

				ebQrGroupePropositionRepository.deletePropositionsByIds(propositionsIds);

				qrConsolidationService.deleteQrGroupe(ebQrGroupeNum);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}

