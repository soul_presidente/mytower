package com.adias.mytowereasy.service.fileupload;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;


public interface FileUploadService {
    boolean handleFileUpload(Integer codeTypeFile, Integer ebEtablissementNum, MultipartFile[] files) throws Exception;

    Object handleFileImportUpload(Integer codeTypeFile, Integer ebUserNum, MultipartFile[] files) throws Exception;

    List<Map<String, Object>> handleFileUploadDemande(
        String fileTypeDemande,
        Integer codeTypeFile,
        Integer ebDemandeNum,
        Integer ebUserNum,
        Integer ebEtablissementNum,
        Integer module,
        String pslEvent,
        Integer ebQmIncidentNum,
        MultipartFile[] files,
        Boolean sendMails)
        throws Exception;

    Object handleFileUploadInvoice(
        Integer ebInvoiceNum,
        Integer codeTypeFile,
        Integer ebUserNum,
        String fileTypeDemande,
        MultipartFile[] files)
        throws Exception;

    boolean handleFileUploadDeclaration(
        Integer ebCustomDeclarationNum,
        Integer codeTypeFile,
        Integer ebUserNum,
        String typeDocCode,
        MultipartFile[] files)
        throws Exception;
}
