package com.adias.mytowereasy.service.fileupload.impl;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.adias.mytowereasy.dao.DaoPricing;
import com.adias.mytowereasy.dao.DaoUser;
import com.adias.mytowereasy.kafka.service.KafkaObjectService;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.IDChatComponent;
import com.adias.mytowereasy.model.Enumeration.Module;
import com.adias.mytowereasy.repository.EbChatRepository;
import com.adias.mytowereasy.repository.EbDemandeFichiersJointRepositorty;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EcMimetypeRepository;
import com.adias.mytowereasy.service.email.EmailDocumentService;
import com.adias.mytowereasy.service.fileupload.FileUploadService;
import com.adias.mytowereasy.service.storage.StorageService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class FileUploadServiceImpl implements FileUploadService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final EmailDocumentService emailDocumentService;
    private final DaoUser daoUser;

    private final EbDemandeRepository ebDemandeRepository;
    private final EbDemandeFichiersJointRepositorty ebDemandeFichiersJointRepositorty;

    private final StorageService storageService;

    private final MessageSource messageSource;

    private final EbChatRepository ebChatRepository;

    private final DaoPricing daoPricing;
    private final EcMimetypeRepository ecMimetypeRepository;

    private final KafkaObjectService kafkaObjectService;

    @Autowired
    public FileUploadServiceImpl(
        EmailDocumentService emailDocumentService,
        DaoUser daoUser,
        EbDemandeRepository ebDemandeRepository,
        EbDemandeFichiersJointRepositorty ebDemandeFichiersJointRepositorty,
        StorageService storageService,
        MessageSource messageSource,
        EbChatRepository ebChatRepository,
        DaoPricing daoPricing,
        EcMimetypeRepository ecMimetypeRepository,
        @Lazy KafkaObjectService kafkaObjectService) {
        this.emailDocumentService = emailDocumentService;
        this.daoUser = daoUser;
        this.ebDemandeRepository = ebDemandeRepository;
        this.ebDemandeFichiersJointRepositorty = ebDemandeFichiersJointRepositorty;
        this.storageService = storageService;
        this.messageSource = messageSource;
        this.ebChatRepository = ebChatRepository;
        this.daoPricing = daoPricing;
        this.ecMimetypeRepository = ecMimetypeRepository;
        this.kafkaObjectService = kafkaObjectService;
    }

    @Override
    public boolean handleFileUpload(Integer codeTypeFile, Integer ebEtablissementNum, MultipartFile[] files)
        throws Exception {

        if (files != null && codeTypeFile != null && ebEtablissementNum != null) {

            for (MultipartFile file: files) {
                storageService.store(file, codeTypeFile, ebEtablissementNum);
            }

        }

        return true;
    }

    @Override
    public Object handleFileImportUpload(Integer codeTypeFile, Integer ebUserNum, MultipartFile[] files)
        throws Exception {
        Object result = false;

        try {

            if (files != null && files.length > 0 && codeTypeFile != null && ebUserNum != null) {
                result = storageService.store(files[0], codeTypeFile, ebUserNum);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return result;
    }

    @Override
    public List<Map<String, Object>> handleFileUploadDemande(
        String fileTypeDemande,
        Integer codeTypeFile,
        Integer ebDemandeNum,
        Integer ebUserNum,
        Integer ebEtablissementNum,
        Integer module,
        String pslEvent,
        Integer ebQmIncidentNum,
        MultipartFile[] files,
        Boolean sendMails)
        throws Exception {
        List<Map<String, Object>> result = new ArrayList<>();
        Integer selectedEbDemandeNum = ebDemandeNum;
        Boolean isNewDemande = false;

        try {
            System.out.println(fileTypeDemande);

            if (files != null && codeTypeFile != null) {

                if (selectedEbDemandeNum == null || selectedEbDemandeNum == 0) {
                    // We are trying to upload file on a non persisted demande,
                    // generate nextId
                    selectedEbDemandeNum = daoPricing.nextValEbDemande();
                    isNewDemande = true;
                }

                Integer countFichier = ebDemandeFichiersJointRepositorty.findMaxId();

                List<String> mimetypeList = ecMimetypeRepository.findDescription();

                for (MultipartFile file: files) {

                    try {

                        if (Statiques.checkFileMimeType(file, mimetypeList)) {
                            int FileNumber = 1;
                            if (countFichier != null && countFichier != 0) FileNumber = countFichier + 1;

                            Map<String, Object> map = (Map<String, Object>) storageService
                                .store(

                                    file,
                                    codeTypeFile,
                                    ebQmIncidentNum,
                                    selectedEbDemandeNum,
                                    FileNumber,
                                    fileTypeDemande,
                                    ebUserNum,
                                    ebEtablissementNum,
                                    module,
                                    pslEvent);

                            map.put("ebDemandeNum", selectedEbDemandeNum);

                            result.add(map);

                            if (selectedEbDemandeNum != null && !isNewDemande && sendMails) {
                                EbDemande demande = ebDemandeRepository.findById(selectedEbDemandeNum).orElseThrow(() -> new IllegalArgumentException(
                						String.format("EbDemande not found")));
                                demande.setUpdateDate(new Date());
                                ebDemandeRepository.save(demande);
                                SearchCriteria criterias = new SearchCriteria();
                                criterias.setEbUserNum(ebUserNum);
                                EbUser user = daoUser.findUser(criterias);

								try {
									emailDocumentService.sendMailUploadFile(user, demande, module, ebQmIncidentNum);
								} catch (Exception e) {
									e.printStackTrace();
								}
                                
								try {
									EbDemandeFichiersJoint fichiersJoint = (EbDemandeFichiersJoint) map.get("fichier");
									// send kafka message
									kafkaObjectService.sendDocumentToKafkaTopic(selectedEbDemandeNum, fichiersJoint);
								} catch (Exception e) {
									e.printStackTrace();
								}
                            }

                        }
                        else {
                            throw new IllegalArgumentException("Mime type is not supported");
                        }

                    } catch (Exception e) {
                        logger.error("An error occured", e);
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return result;
    }

    @Override
    public Object handleFileUploadInvoice(
        Integer ebInvoiceNum,
        Integer codeTypeFile,
        Integer ebUserNum,
        String fileTypeDemande,
        MultipartFile[] files)
        throws Exception {
        List<EbDemandeFichiersJoint> listFichierAEnvoyer = new ArrayList<>();
        SearchCriteria criterias = new SearchCriteria();
        criterias.setEbUserNum(ebUserNum);
        EbUser user = daoUser.findUser(criterias);
        EbChat chat = new EbChat();
        List<EbChat> listChat = new ArrayList<EbChat>();
        Date dateActuel = new Date();
        Locale lcl = new Locale(user.getLangageUser());
        String txtCom = null;

        try {

            if (files != null && codeTypeFile != null) {
                Integer countFichier = ebDemandeFichiersJointRepositorty.findMaxId();

                for (MultipartFile file: files) {
                    int FileNumber = 1;
                    if (countFichier != null && countFichier != 0) FileNumber = countFichier + 1;
                    Object fichier = storageService
                        .store(file, codeTypeFile, ebInvoiceNum, FileNumber, ebUserNum, fileTypeDemande); // ebInvoiceNum
                                                                                                          // > 0 ?
                                                                                                          // ebInvoiceNum
                                                                                                          // : null,
                                                                                                          // FileNumber,

                    String[] args = {
                        file.getOriginalFilename()
                    };

                    txtCom = messageSource.getMessage("frieght.uploadfile", args, lcl);

                    chat = new EbChat(user);
                    chat.setIdChatComponent(IDChatComponent.FREIGHT_AUDIT.getCode());
                    chat.setModule(Module.FREIGHT_AUDIT.getCode());
                    chat.setUserName(user.getNomPrenom());
                    chat.setDateCreation(dateActuel);
                    chat.setText(txtCom);
                    chat.setIdFiche(ebInvoiceNum);
                    listChat.add(chat);
                    ebChatRepository.saveAll(listChat);

                    if (fichier instanceof EbDemandeFichiersJoint) listFichierAEnvoyer
                        .add((EbDemandeFichiersJoint) fichier);
                    else {
                        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
                        result.add((Map<String, Object>) fichier);
                        return result;
                    }

                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return listFichierAEnvoyer;
    }

    @Override
    public boolean handleFileUploadDeclaration(
        Integer ebCustomDeclarationNum,
        Integer codeTypeFile,
        Integer ebUserNum,
        String typeDocCode,
        MultipartFile[] files)
        throws Exception {

        if (files != null && codeTypeFile != null && ebCustomDeclarationNum != null) {

            for (MultipartFile file: files) {
                storageService.store(file, codeTypeFile, null, ebCustomDeclarationNum, typeDocCode, ebUserNum);
            }

        }

        return true;
    }
}
