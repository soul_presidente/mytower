package com.adias.mytowereasy.service.chat;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbChat;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.Role;
import com.adias.mytowereasy.model.custom.EbChatCustom;
import com.adias.mytowereasy.repository.EbChatRepository;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.repository.custom.EbChatCustomRepository;
import com.adias.mytowereasy.service.ConnectedUserService;


@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ChatFetcher {

    private Integer module;
    private Integer idFiche;
    private Integer idChatComponent;
    private EbUser carrier;
    private EbUser connectedUser;

    @Autowired
    private EbChatCustomRepository ebChatCustomRepository;

    @Autowired
    private EbChatRepository chatRepository;

    @Autowired
    private EbDemandeRepository ebDemandeRepository;

    @Autowired
    private ConnectedUserService connectedUserService;

    @Autowired
    private EbUserRepository ebUserRepository;

    public List<EbChat> getListChat(Integer module, Integer idFiche, Integer idChatComponent) {
        init(module, idFiche, idChatComponent);
        List<EbChat> result = new ArrayList<EbChat>();

        if (Enumeration.Module.CUSTOM.getCode().equals(module)) {
            result = getChatsForModuleCustoms();
        }
        else if (Enumeration.Module.PRICING.getCode().equals(module)) {
            result = getChatsForModulePricing();
        }
        else {
            result = getChatsForOtherModules();
        }

        List<EbChat> r = result
            .stream().distinct().collect(Collectors.toList());
        r.sort(Comparator.comparing(EbChat::getDateCreation).reversed());
        return r;
    }

    private List<EbChat> getChatsForOtherModules() {
        return chatRepository
            .findAllByModuleAndIdFicheAndIdChatComponentOrderByDateCreationDesc(module, idFiche, idChatComponent);
    }

    private List<EbChat> getChatsForModulePricing() {
        List<EbChat> result = new ArrayList<EbChat>();

        if (connectedUser.isChargeur() || connectedUser.isControlTower()) {
            result = getChatsForChargerORControlTower();
        }

        if (connectedUser.isTransporteur()) {
            result = getChatsForTranporteur();
        }

        return result;
    }

    private List<EbChat> getChatsForTranporteur() {
        Optional<EbDemande> optionalDemande = ebDemandeRepository.findById(idFiche);

        if (!optionalDemande.isPresent()) {
            throw new IllegalStateException(
                "Chat is not associated with an object, couldn't find object with id: " + idFiche);
        }

        EbDemande demande = optionalDemande.get();

        List<EbChat> result;

        if (connectedUser.isSuperAdmin()) {
            result = getChatsForTransporteurSuperAdmin();
        }
        else if (connectedUser.isAdmin()) {
            result = getChatsForTransporteurAdmin();
        }
        else {
            result = getChatsForTransporteurSimple();
        }

        if (Objects.nonNull(demande.getxEbTransporteur())) {
            carrier = ebUserRepository.findById(demande.getxEbTransporteur()).orElse(null);
        }

        if (isSuperAdminAndTRConfirmedAndNotSameCompanyAsCarrier() ||
            isAdminAndTRConfirmedAndNotSameEstablishementAsCarrier() ||
            isNotSuperAdminAndNotAdminAndTRConfirmedAndIsNotCarrier()) {
            result.addAll(getChargerOrControlTowerChatLessThanDateConfirmed(demande));
        }
        else {
            result.addAll(getChargerOrControlTowerChat());
        }

        result.addAll(getChatWhereOwnerIsNull());

        return result;
    }

    private List<EbChat> getChatWhereOwnerIsNull() {
        return chatRepository
            .findByModuleAndIdFicheAndOwnerIsNull(module, idFiche, idChatComponent);
    }

    private List<EbChat> getChargerOrControlTowerChat() {
        return chatRepository
                    .findByModuleAndIdFicheAndOwnerRole(
                Arrays.asList(Role.ROLE_CHARGEUR.getCode(), Role.ROLE_CONTROL_TOWER.getCode()),
                        module,
                        idFiche,
                idChatComponent);
    }

    private List<EbChat> getChargerOrControlTowerChatLessThanDateConfirmed(EbDemande demande) {
        return chatRepository
                    .findByModuleAndIdFicheAndOwnerRoleAndDate(
                Arrays.asList(Role.ROLE_CHARGEUR.getCode(), Role.ROLE_CONTROL_TOWER.getCode()),
                        module,
                        idFiche,
                        idChatComponent,
                demande.getConfirmed());
    }

    private List<EbChat> getChatsForTransporteurSimple() {
        return chatRepository
            .findByModuleAndIdFicheAndOwner(
                connectedUser.getEbUserNum(),
                module,
                idFiche,
                idChatComponent);
    }

    private List<EbChat> getChatsForTransporteurAdmin() {
        return chatRepository
            .findByModuleAndIdFicheAndEtablissement(
                connectedUser.getEbEtablissement().getEbEtablissementNum(),
                module,
                idFiche,
                idChatComponent);
    }

    private List<EbChat> getChatsForTransporteurSuperAdmin() {
        return chatRepository
            .findByModuleAndIdFicheAndCompagnie(
                connectedUser.getEbCompagnie().getEbCompagnieNum(),
                module,
                idFiche,
                idChatComponent);
    }

    private List<EbChat> getChatsForModuleCustoms() {
        List<EbChat> result = new ArrayList<>();
        List<EbChatCustom> listEbDemandechatCustom = ebChatCustomRepository
            .findAllByModuleAndIdFicheAndIdChatComponentOrderByDateCreationDesc(module, idFiche, idChatComponent);

        if (!listEbDemandechatCustom.isEmpty()) {

            for (EbChatCustom chatcustom: listEbDemandechatCustom) {
                EbChat chat = new EbChat();
                chat.setEbChatNum(chatcustom.getEbChatCustomNum());
                chat.setDateCreation(chatcustom.getDateCreation());
                chat.setIdChatComponent(chatcustom.getIdChatComponent());
                chat.setIsHistory(chatcustom.getIsHistory());
                chat.setModule(chatcustom.getModule());
                chat.setIdFiche(chatcustom.getIdFiche());
                chat.setText(chatcustom.getText());
                chat.setUserName(chatcustom.getUserName());
                chat.setUserAvatar(chatcustom.getUserAvatar());
                result.add(chat);
            }

        }

        return result;
    }

    private void init(Integer module, Integer idFiche, Integer idChatComponent) {
        this.module = module;
        this.idFiche = idFiche;
        this.idChatComponent = idChatComponent;
        this.connectedUser = connectedUserService.getCurrentUser();
    }

    private boolean isSuperAdminAndTRConfirmedAndNotSameCompanyAsCarrier() {
        return connectedUser.isSuperAdmin() &&
            Objects.nonNull(carrier) &&
            !connectedUser.getEbCompagnie().getEbCompagnieNum().equals(carrier.getEbCompagnie().getEbCompagnieNum());
    }

    private boolean isAdminAndTRConfirmedAndNotSameEstablishementAsCarrier() {
        return connectedUser.isAdmin() &&
            Objects.nonNull(carrier) &&
            !connectedUser
                .getEbEtablissement().getEbEtablissementNum()
                .equals(carrier.getEbEtablissement().getEbEtablissementNum());
    }

    private boolean isNotSuperAdminAndNotAdminAndTRConfirmedAndIsNotCarrier() {
        return !connectedUser.isSuperAdmin() &&
            !connectedUser.isAdmin() && Objects.nonNull(carrier) &&
            !connectedUser.getEbUserNum().equals(carrier.getEbUserNum());
    }

    private List<EbChat> getChatsForChargerORControlTower() {
        return chatRepository
            .findAllByModuleAndIdFicheAndIdChatComponentOrderByDateCreationDesc(module, idFiche, idChatComponent);
    }
}
