package com.adias.mytowereasy.service;

import java.math.BigDecimal;
import java.util.*;

import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.repository.DemandeObserversRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;
import com.adias.mytowereasy.airbus.model.EbTtCompanyPsl;
import com.adias.mytowereasy.airbus.repository.EbQrGroupePropositionRepository;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.NatureDemandeTransport;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.repository.EbCostCenterRepository;
import com.adias.mytowereasy.utils.search.SearchCriteria;


/*
 * cette classe a été ajouter pour corriger le soucis des des transactionnelles méthodes
 * vu que les annotation spring ne fonctionne si on appelle une méthode avec this, et que
 * le self injection n'est pas une bonne idée
 */
@Service
public class PricingCreateAndUpdateService extends MyTowerService {
    @Autowired
    PricingServiceImpl pricingService;

    @Autowired
    EbQrGroupePropositionRepository ebQrGroupePropositionRepository;

    @Autowired
    PricingAnnexMethodsService pricingAnnexMethodsService;

    @Autowired
    EbCostCenterRepository ebCostCenterRepository;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private DemandeObserversRepository demandeObserversRepository;

    @Transactional
    public EbDemande
        updateEbDemandeObjects(EbDemande ebDemande, Integer ebModuleNum, boolean activateTasksAfterCreation) {
        ebMarchandiseRepository.deleteListEbMarchandiseByEbDemandeNumIn(Arrays.asList(ebDemande.getEbDemandeNum()));
        exEbDemandeTransporteurRepository.deleteListEbDemandeQuoteByEbDemandeNum(ebDemande.getEbDemandeNum());
        //delete observers
        demandeObserversRepository.deleteTrsObservers(Collections.singletonList(ebDemande.getEbDemandeNum()));


        ebDemande.setListCategories(ebDemande.getListCategories());

        if (ebDemande.getxEcNature() != null &&
            ebDemande.getxEcNature().equals(NatureDemandeTransport.CONSOLIDATION.getCode())) {
            ebDemande.getListMarchandises().forEach(mar -> {
                mar.setEbMarchandiseNum(null);
                mar.setEbDemande(new EbDemande(ebDemande.getEbDemandeNum()));
            });

            if (ebDemande.getEbQrGroupe() != null &&
                ebDemande.getEbQrGroupe().getListPropositions() != null &&
                ebDemande.getEbQrGroupe().getListPropositions().get(0) != null) {
                EbQrGroupe ebQrGroupeOld = ebQrGroupePropositionRepository
                    .getEbQrGroupeWithPropositionByEbDemandeResult(ebDemande.getEbDemandeNum());

                if (ebQrGroupeOld != null) {
                    if (!ebDemande
                        .getEbQrGroupe().getListPropositions().get(0).getListSelectedDemandeNum().equals(
                            ebQrGroupeOld
                                .getListPropositions().get(0)
                                .getListSelectedDemandeNum())) ebQrGroupePropositionRepository
                                    .updateListSelectedDemandeNumByEbQrGroupePropositionNum(
                                        ebDemande
                                            .getEbQrGroupe().getListPropositions().get(0).getListSelectedDemandeNum(),
                                        ebDemande
                                            .getEbQrGroupe().getListPropositions().get(0).getEbQrGroupePropositionNum(),
                                        ebDemande.getEbQrGroupe().getListPropositions().get(0).getListDemandeNum());
                }

            }

            if (ebDemande.getExEbDemandeTransporteurs() != null && !ebDemande.getExEbDemandeTransporteurs().isEmpty()) {
                ebDemande
                    .getExEbDemandeTransporteurs().stream()
                    .forEach(it -> it.setxEbDemande(new EbDemande(ebDemande.getEbDemandeNum())));
            }

        }

        if (ebDemande.getExEbDemandeTransporteurs() != null && !ebDemande.getExEbDemandeTransporteurs().isEmpty()) {
            ebDemande.getExEbDemandeTransporteurs().stream().forEach(it -> it.setExEbDemandeTransporteurNum(null));
        }

        EbUser currentUser = connectedUserService.getCurrentUser();
        pricingService
            .createEbDemande(ebDemande, ebModuleNum, activateTasksAfterCreation, currentUser);

        if (!isNatureConsolidationOrEDI(ebDemande)) {
            emailServicePricingBooking.sendEmailPricingFileUpdated(ebDemande, currentUser);
        }

        return ebDemande;
    }

    private boolean isNatureConsolidationOrEDI(EbDemande ebDemande) {
        return ebDemande.getxEcNature().equals(NatureDemandeTransport.EDI.getCode()) ||
            ebDemande.getxEcNature().equals(NatureDemandeTransport.CONSOLIDATION.getCode());
    }

    @Transactional
    public EbDemande createEbDemandeNormalOrTF(
        EbDemande ebDemande,
        NatureDemandeTransport nature,
        Integer ebModuleNum,
        EbUser connectedUser) {
        String codeCompany = connectedUser.getEbCompagnie().getCode();
        Integer CompanyNum = connectedUser.getEbCompagnie().getEbCompagnieNum();
        String refPrefix = pricingAnnexMethodsService.getReferenceNaturePrefix(nature);
        String refTransport = null;

        if (ebDemande.getxEbCostCenter() != null && ebDemande.getxEbCostCenter().getEbCostCenterNum() != null) ebDemande
            .setxEbCostCenter(ebCostCenterRepository.getOne(ebDemande.getxEbCostCenter().getEbCostCenterNum()));
        else ebDemande.setxEbCostCenter(null);

        // Fill user field
        if (ebDemande.getUser() != null && ebDemande.getUser().getEbUserNum() != null) {
            ebDemande.setUser(ebUserRepository.getOne(ebDemande.getUser().getEbUserNum()));
            ebDemande.setxEbEtablissement(ebDemande.getUser().getEbEtablissement());
            ebDemande.setxEbCompagnie(ebDemande.getUser().getEbCompagnie());
        }
        else ebDemande.setUser(null);

        // Put next ebDemandeNum
        if (ebDemande.getEbDemandeNum() == null) {
            ebDemande.setEbDemandeNum(daoPricing.nextValEbDemande());
        }

        // Nature
        ebDemande.setxEcNature(nature.getCode());

        // Generate ref transport from connected user
        refTransport = StringUtils.leftPad(ebDemande.getEbDemandeNum().toString().toUpperCase(), 5, "0");
        ebDemande.setRefTransport(codeCompany.concat("-").concat(refPrefix).concat(refTransport));

        // Persist EbParty
        if (ebDemande.getEbPartyDest() != null) {
            if (ebDemande.getEbPartyDest().getxEbUserVisibility() == null ||
                ebDemande.getEbPartyDest().getxEbUserVisibility().getEbUserNum()
                    == null) ebDemande.getEbPartyDest().setxEbUserVisibility(null);
            else ebDemande
                .getEbPartyDest().setxEbUserVisibility(
                    ebUserRepository
                        .findOneByEbUserNum(ebDemande.getEbPartyDest().getxEbUserVisibility().getEbUserNum()));
            ebDemande.getEbPartyDest().setEtablissementAdresse(null);

            if (ebDemande.getEbPartyDest().getZone() != null &&
                ebDemande.getEbPartyDest().getZone().getEbZoneNum() != null) {
                ebDemande
                    .getEbPartyDest().setZone(
                        ebZoneRepository.findById(ebDemande.getEbPartyDest().getZone().getEbZoneNum()).orElse(null));
            }
            else ebDemande.getEbPartyDest().setZone(null);

            ebPartyRepository.save(ebDemande.getEbPartyDest());
        }

        if (ebDemande.getEbPartyOrigin() != null) {
            if (ebDemande.getEbPartyOrigin().getxEbUserVisibility() == null ||
                ebDemande.getEbPartyOrigin().getxEbUserVisibility().getEbUserNum()
                    == null) ebDemande.getEbPartyOrigin().setxEbUserVisibility(null);
            else ebDemande
                .getEbPartyOrigin().setxEbUserVisibility(
                    ebUserRepository
                        .findOneByEbUserNum(ebDemande.getEbPartyOrigin().getxEbUserVisibility().getEbUserNum()));
            ebDemande.getEbPartyOrigin().setEtablissementAdresse(null);

            if (ebDemande.getEbPartyOrigin().getZone() != null &&
                ebDemande.getEbPartyOrigin().getZone().getEbZoneNum() != null) {
                ebDemande
                    .getEbPartyOrigin()
                    .setZone(ebZoneRepository.getOne(ebDemande.getEbPartyOrigin().getZone().getEbZoneNum()));
            }
            else ebDemande.getEbPartyOrigin().setZone(null);

            ebPartyRepository.save(ebDemande.getEbPartyOrigin());
        }

        if (ebDemande.getEbPartyNotif() != null && ebDemande.getEbPartyNotif().getEbPartyNum() == null) {
            if (ebDemande.getEbPartyNotif().getxEbUserVisibility() == null ||
                ebDemande.getEbPartyNotif().getxEbUserVisibility().getEbUserNum()
                    == null) ebDemande.getEbPartyNotif().setxEbUserVisibility(null);
            else ebDemande
                .getEbPartyNotif().setxEbUserVisibility(
                    ebUserRepository.getOne(ebDemande.getEbPartyNotif().getxEbUserVisibility().getEbUserNum()));
            ebDemande.getEbPartyNotif().setEtablissementAdresse(null);

            if (ebDemande.getEbPartyNotif().getZone() != null &&
                ebDemande.getEbPartyNotif().getZone().getEbZoneNum() != null) {
                ebDemande
                    .getEbPartyNotif()
                    .setZone(ebZoneRepository.getOne(ebDemande.getEbPartyNotif().getZone().getEbZoneNum()));
            }
            else ebDemande.getEbPartyNotif().setZone(null);

            ebPartyRepository.save(ebDemande.getEbPartyNotif());
        }

        if (ebDemande.getPointIntermediate() != null) {

            if (ebDemande.getPointIntermediate().getEbPartyNum() == null) {
                ebDemande.getPointIntermediate().setEtablissementAdresse(null);

                if (ebDemande.getPointIntermediate().getZone() != null &&
                    ebDemande.getPointIntermediate().getZone().getEbZoneNum() != null) {
                    ebDemande
                        .getPointIntermediate()
                        .setZone(ebZoneRepository.getOne(ebDemande.getPointIntermediate().getZone().getEbZoneNum()));
                }
                else ebDemande.getPointIntermediate().setZone(null);

                ebPartyRepository.save(ebDemande.getPointIntermediate());
            }

        }

        // Fill fields with simple setter (no treatment required)
        if (ebDemande.isFlagRecommendation() == null) {
            ebDemande.setFlagRecommendation(false);
        }

        if (ebDemande.getxEcStatut() == null) {
            ebDemande.setxEcStatut(Enumeration.StatutDemande.INP.getCode());
            ebDemande.setWaitingForQuote(new Date());
        }

        ebDemande.setDateCreation(new Date());
        ebDemande.setFlagEbtrackTrace(false);
        ebDemande
            .setLibelleOriginCountry(
                ebDemande.getEbPartyOrigin() != null && ebDemande.getEbPartyOrigin().getxEcCountry() != null ?
                    ebDemande.getEbPartyOrigin().getxEcCountry().getLibelle() :
                    null);
        ebDemande
            .setLibelleOriginCity(ebDemande.getEbPartyOrigin() != null ? ebDemande.getEbPartyOrigin().getCity() : null);
        ebDemande
            .setLibelleDestCountry(
                ebDemande.getEbPartyDest() != null && ebDemande.getEbPartyDest().getxEcCountry() != null ?
                    ebDemande.getEbPartyDest().getxEcCountry().getLibelle() :
                    null);
        ebDemande.setLibelleDestCity(ebDemande.getEbPartyDest() != null ? ebDemande.getEbPartyDest().getCity() : null);

        // mis en commentaire pas SOUNG : ce code n'est plus valable suite à la
        // logique
        // de changement des incoterme
        /*
         * if (ebDemande.getxEcIncoterm() != null) { EbIncoterm inco =
         * listStatiqueService.getIncotermById(ebDemande.getxEcIncoterm()); if
         * (inco !=
         * null) ebDemande.setxEcIncotermLibelle(inco.getLibelle()); }
         */

        // nouveau code permettant de recupperer l'incoterm (le contenu du chmap
        // xEcIncotermLibelle est affiché sur le dashbaord)
        ebDemande.setxEcIncotermLibelle(ebDemande.getxEcIncotermLibelle());

        // Generate unitsReference string
        String unitsRef = "", shippingType = "", partNumber = "", orderNumber = "", unitsPackingList = "";

        for (EbMarchandise cmd: ebDemande.getListMarchandises()) {

            // concat unitRefs from demande
            if (cmd.getUnitReference() != null && StringUtils.isNotEmpty(cmd.getUnitReference())) {

                if (StringUtils.isNotEmpty(unitsRef)) {
                    unitsRef += ",";
                }

                unitsRef = unitsRef.concat(cmd.getUnitReference());
            }

            // Concat packingList for demande
            if (cmd.getPackingList() != null && StringUtils.isNotEmpty(cmd.getPackingList())) {

                if (StringUtils.isNotEmpty(unitsPackingList)) {
                    unitsPackingList += ",";
                }

                unitsPackingList = unitsPackingList.concat(cmd.getPackingList());
            }

            if (cmd.getShippingType() != null && StringUtils.isNotEmpty(cmd.getShippingType())) {

                if (StringUtils.isNotEmpty(shippingType)) {
                    shippingType += ",";
                }

                shippingType = shippingType.concat(cmd.getShippingType());
            }

            if (cmd.getPartNumber() != null && StringUtils.isNotEmpty(cmd.getPartNumber() + "")) {

                if (StringUtils.isNotEmpty(partNumber)) {
                    partNumber += ",";
                }

                partNumber = partNumber.concat(cmd.getPartNumber() + "");
            }

            if (cmd.getOrderNumber() != null && StringUtils.isNotEmpty(cmd.getOrderNumber() + "")) {

                if (StringUtils.isNotEmpty(orderNumber)) {
                    orderNumber += ",";
                }

                orderNumber = orderNumber.concat(cmd.getOrderNumber() + "");
            }

            // ajout du dossier T&T si la marchandise supprimé a été referencé
            // par le dossier.
            if (cmd.getxEbTracing() != null) {
                cmd.getxEbTracing().setxEbMarchandise(cmd);
            }

        }

        if (ebDemande.getListLabels() == null ||
            ebDemande.getListLabels().isEmpty()) ebDemande.setListCategories(ebDemande.getListCategories());

        ebDemande.setUnitsReference(unitsRef);
        ebDemande.setUnitsPackingList(unitsPackingList);
        ebDemande.setPartNumber(partNumber);
        ebDemande.setOrderNumber(orderNumber);
        ebDemande.setShippingType(shippingType);

        if (ebDemande.getXecCurrencyInvoice() != null &&
            ebDemande.getXecCurrencyInvoice().getEcCurrencyNum() == null) ebDemande.setXecCurrencyInvoice(null);
        if (ebDemande.getXecCurrencyInsurance() != null &&
            ebDemande.getXecCurrencyInsurance().getEcCurrencyNum() == null) ebDemande.setXecCurrencyInsurance(null);

        EbTtCompanyPsl psl = ebDemande.getxEbSchemaPsl() != null ?
            (ebDemande.getxEbSchemaPsl().getListPsl() != null ?
                ebDemande.getxEbSchemaPsl().getListPsl().stream().map(it -> {

                    if (it.getExpectedDate() != null && it.getExpectedDateTime() != null) {
                        Date dt = new Date();
                        dt.setTime(it.getExpectedDateTime());
                        it.setExpectedDate(dt);
                    }

                    return it;
                }).filter(it -> it.getEndPsl() != null && it.getEndPsl()).findFirst().orElse(null) :
                null) :
            null;

        // si aucun psl n'était à "END" on considère le dernien comme "END"
        if (psl == null && ebDemande.getxEbSchemaPsl() != null && ebDemande.getxEbSchemaPsl().getListPsl() != null) {
            EbTtSchemaPsl.sortListPsl(ebDemande.getxEbSchemaPsl().getListPsl());
            ebDemande
                .getxEbSchemaPsl().getListPsl().get(ebDemande.getxEbSchemaPsl().getListPsl().size() - 1)
                .setEndPsl(true);
        }

        if (ebDemande.getxEbSchemaPsl() != null && ebDemande.getxEbSchemaPsl().getListPsl() != null) psl = ebDemande
            .getxEbSchemaPsl().getListPsl().stream().filter(it -> it.getStartPsl() != null && it.getStartPsl())
            .findFirst().orElse(null);

        // si aucun psl n'était à "START" on considère le premier comme "START"
        if (psl == null && ebDemande.getxEbSchemaPsl() != null && ebDemande.getxEbSchemaPsl().getListPsl() != null) {
            ebDemande.getxEbSchemaPsl().getListPsl().sort((it1, it2) -> it1.getOrder() - it2.getOrder());
            ebDemande.getxEbSchemaPsl().getListPsl().get(0).setStartPsl(true);
        }

        // Incoterm PSL
        if (ebDemande.getxEbSchemaPsl() != null && ebDemande.getxEbSchemaPsl().getListPsl() != null) {
            psl = ebDemande
                .getxEbSchemaPsl().getListPsl().stream().filter(it -> it.getEndPsl() != null && it.getEndPsl())
                .findFirst().orElse(null);

            if (psl != null) {
                ebDemande.setCodeAlphaLastPsl(psl.getCodeAlpha());
                ebDemande.setCodeLastPsl(psl.getEbTtCompanyPslNum());
                ebDemande.setLibelleLastPsl(psl.getLibelle());
            }

        }

        // Link etablissement / company / user
        if (connectedUser.isControlTower()) {
            ebDemande.setxEbUserCt(new EbUser(connectedUser.getEbUserNum()));
            ebDemande.setxEbCompagnieCt(new EbCompagnie(CompanyNum));
            ebDemande
                .setxEbEtablissementCt(new EbEtablissement(connectedUser.getEbEtablissement().getEbEtablissementNum()));
        }
        else if (connectedUser.getRealUserNum() != null) {
            ebDemande.setxEbUserCt(new EbUser(connectedUser.getEbUserNum()));
            ebDemande.setxEbCompagnieCt(new EbCompagnie(CompanyNum));
            ebDemande
                .setxEbEtablissementCt(new EbEtablissement(connectedUser.getEbEtablissement().getEbEtablissementNum()));
            ebDemande.setFlagConnectedAsCt(true);
        }
        else {
            ebDemande.setxEbUserCt(null);
        }

        EbEtablissement solicitedTDCEtab = pricingService.findSolicitedTDCEtab(ebDemande.getExEbDemandeTransporteurs());
        if (solicitedTDCEtab != null) ebDemande.setxEbEtablissementCt(solicitedTDCEtab);

        // TODO Commenter ce code , pourquoi on a besoin de setter la company et
        // l'etablissement à cet endroit
        if (ebDemande.getxEbUserCt() == null) {
            ebDemande
                .setxEbEtablissement(new EbEtablissement(connectedUser.getEbEtablissement().getEbEtablissementNum()));
            ebDemande.setxEbCompagnie(new EbCompagnie(CompanyNum));
        }
        else if (ebDemande.getUser() != null) {
            ebDemande
                .setxEbEtablissement(
                    new EbEtablissement(ebDemande.getUser().getEbEtablissement().getEbEtablissementNum()));
            ebDemande.setxEbCompagnie(new EbCompagnie(ebDemande.getUser().getEbCompagnie().getEbCompagnieNum()));
        }

        if (ebDemande.getxEbUserOrigin() == null || ebDemande.getxEbUserOrigin().getEbUserNum() == null) {
            ebDemande.setxEbUserOrigin(null);
            ebDemande.setxEbEtabOrigin(null);
        }
        else {
            ebDemande.setxEbUserOrigin(ebUserRepository.getOne(ebDemande.getxEbUserOrigin().getEbUserNum()));
            if (ebDemande.getxEbUserOrigin().getEbEtablissement().getEbEtablissementNum() != null) ebDemande
                .setxEbEtabOrigin(
                    ebEtablissementRepository
                        .getOne(ebDemande.getxEbUserOrigin().getEbEtablissement().getEbEtablissementNum()));

            this.notificationService
                .generateNotificationForUser(ebDemande, ebDemande.getxEbUserOrigin().getEbUserNum(), ebModuleNum, true);
        }

        if (ebDemande.getxEbUserDest() == null || ebDemande.getxEbUserDest().getEbUserNum() == null) {
            ebDemande.setxEbEtabDest(null);
            ebDemande.setxEbUserDest(null);
        }
        else {
            ebDemande.setxEbUserDest(ebUserRepository.getOne(ebDemande.getxEbUserDest().getEbUserNum()));
            ebDemande
                .setxEbEtabDest(
                    ebEtablissementRepository
                        .getOne(ebDemande.getxEbUserDest().getEbEtablissement().getEbEtablissementNum()));

            EbNotification notif = this.notificationService
                .generateNotificationForUser(ebDemande, ebDemande.getxEbUserDest().getEbUserNum(), ebModuleNum, true);
        }

        if (ebDemande.getxEbUserOriginCustomsBroker() == null ||
            ebDemande.getxEbUserOriginCustomsBroker().getEbUserNum() == null) {
            ebDemande.setxEbUserOriginCustomsBroker(null);
            ebDemande.setxEbEtabOriginCustomsBroker(null);
        }

        if (ebDemande.getxEbUserOriginCustomsBroker()
            != null) ebDemande.getxEbUserOriginCustomsBroker().setEbEtablissement(null);

        if (ebDemande.getxEbUserDestCustomsBroker() == null ||
            ebDemande.getxEbUserDestCustomsBroker().getEbUserNum() == null) {
            ebDemande.setxEbEtabDestCustomsBroker(null);
            ebDemande.setxEbUserDestCustomsBroker(null);
        }

        if (ebDemande.getxEbUserDestCustomsBroker()
            != null) ebDemande.getxEbUserDestCustomsBroker().setEbEtablissement(null);

        if (ebDemande.getxEbUserObserver() != null && !ebDemande.getxEbUserObserver().isEmpty()) {
            ebDemande.getxEbUserObserver().forEach(user -> {
                this.notificationService.generateNotificationForUser(ebDemande, user.getEbUserNum(), ebModuleNum, true);
            });
        }

        // Prepend ref transport with demande user company code
        String codeCompany1 = null;

        if (ebDemande.getUser() != null) {
            codeCompany1 = ebDemande.getUser().getEbCompagnie().getCode();

            if (codeCompany1 != null) {
                ebDemande.setRefTransport(codeCompany1.concat("-").concat(refPrefix).concat(refTransport));
            }

        }

        // Persist type documents
        // List<EcTypeDocument> typesDocument =
        // typeDocumentService.getTypeDocumentForItinerary(ebDemande.getEbPartyOrigin().getxEcCountry().getCode(),
        // ebDemande.getEbPartyDest().getxEcCountry().getCode());
        // ebDemande.setListTypeDocuments(typesDocument);

        /*
         * recuperer la liste des documents selon les modules : PRCING && TT &&
         * TM .
         * l'insertion de la liste des document sur la liste qu'on a sur le
         * champs
         * ListTypeDocument de table eb_demande
         */
        List<EbTypeDocuments> docs = new ArrayList<EbTypeDocuments>();
        List<Integer> listModule = new ArrayList<Integer>();
        SearchCriteria criteria = new SearchCriteria();

        //
        Integer xEbCompagnieDemande = ebDemande.getxEbCompagnie() != null ?
            ebDemande.getxEbCompagnie().getEbCompagnieNum() :
            null;
        criteria.setEbCompagnieNum(xEbCompagnieDemande);

        listModule.add(Enumeration.Module.PRICING.getCode());
        listModule.add(Enumeration.Module.TRANSPORT_MANAGEMENT.getCode());
        listModule.add(Enumeration.Module.TRACK.getCode());

        criteria.setListModule(listModule);

        // criteria.setActivated(true);
        try {
            docs = daoTypeDocuments.getListEbTypeDocument(criteria);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // ce stream sert a extraire les document des type modules, 1 : pricing
        // , 2 : TM
        // , 3 : TT

        /*
         * docsfinal = docs .stream() .filter(doc ->
         * doc.getEbTypeDocumentsModule().contains(Enumeration.Module.PRICING.getCode()) ||
         * doc.getEbTypeDocumentsModule().contains(Enumeration.Module.TRANSPORT_MANAGEMENT.
         * getCode()
         * ) || doc.getEbTypeDocumentsModule().contains(Enumeration.Module.TRACK.getCode()))
         * .collect(Collectors.toList());
         */

        // l'affectation
        docs = CollectionUtils.isNotEmpty(docs) ? docs : null;
        ebDemande.setListTypeDocuments(docs);

        // Integrated Via MTG
        ebDemande.setIntegratedVia("mtg");

        if (ebDemande.getIsDraft() != null && ebDemande.getIsDraft() == true) {
            ebDemande.setxEcStatut(StatutDemande.PND.getCode());
        }

        if (ebDemande != null &&
            ebDemande.getExEbDemandeTransporteurs() != null && ebDemande.getExEbDemandeTransporteurs().size() > 0) {
            ebDemande.getExEbDemandeTransporteurs().stream().forEach(transporteur -> {

                if (transporteur.getxTransporteur() != null &&
                    transporteur.getxTransporteur().getSelected() != null && transporteur.getPrice() != null) {

                    if (transporteur.getPrice() != (BigDecimal.ZERO)) {
                        ebDemande.setxEcStatut(StatutDemande.REC.getCode());
                        ebDemande.setWaitingForConf(new Date());
                    }

                    if (transporteur.getListCostCategorie() != null && transporteur.getListCostCategorie().size() > 0) {
                        transporteur.getListCostCategorie().forEach(costCategorie -> {
                            costCategorie.getListEbCost().forEach(cost -> {

                                if (cost.getEuroExchangeRate() != null) {
                                    ebCostRepository
                                        .updateEuroExchangeRateReal(
                                            cost.getEbCostNum(),
                                            cost.getEuroExchangeRate().doubleValue());
                                }

                            });
                        });
                    }

                }

            });
        }

        // Persist ebDemande
        ebDemandeRepository.save(ebDemande);

        if (ebDemande.getEbDemandeFichierJointNum() != null && !ebDemande.getEbDemandeFichierJointNum().isEmpty()) {
            ebDemandeFichiersJointRepositorty
                .setDemande(ebDemande.getEbDemandeFichierJointNum(), ebDemande.getEbDemandeNum());
        }

        ebDemande.getListMarchandises().stream().forEach(it -> {

            if (it.getxEbTypeUnit() != null) {
                EbTypeUnit typeUnitDB = ebTypeUnitRepository.getOne(it.getxEbTypeUnit());

                if (typeUnitDB.getxEbTypeContainer() != null) {
                    it.setxEbTypeConteneur(typeUnitDB.getxEbTypeContainer());
                }

                it.setLabelTypeUnit(typeUnitDB.getLibelle());
            }

            it.setEbDemande(ebDemandeRepository.getOne(ebDemande.getEbDemandeNum()));
        });

        // set parent of unit
        List<EbMarchandise> listChild = new ArrayList<EbMarchandise>();
        ebDemande.getListMarchandises().stream().forEach(elm -> {

            if (elm.getChildren() != null && !elm.getChildren().isEmpty()) {
                elm.getChildren().stream().forEach(child -> {
                    child.setParent(elm);
                    listChild.add(child);
                });
            }

        });

        if (!listChild.isEmpty()) {
            ebDemande.getListMarchandises().stream().forEach(unit -> {
                EbMarchandise children = listChild
                    .stream()
                    .filter(child -> child.getCustomerReference().equalsIgnoreCase(unit.getCustomerReference()))
                    .findFirst().orElse(null);

                if (children != null) {
                    unit.setParent(children.getParent());
                }

            });
            ebMarchandiseRepository.saveAll(ebDemande.getListMarchandises());
        }
        else {
            ebDemande.getListMarchandises().forEach(unit -> unit.setParent(null));
            ebMarchandiseRepository.saveAll(ebDemande.getListMarchandises());
        }

        // Save observers
        saveObservers(ebDemande);

        return ebDemande;
    }

    public void saveObservers(EbDemande ebDemande) {
        Set<DemandeUserObservers> observers = new HashSet<>();
        if(CollectionUtils.isNotEmpty(ebDemande.getxEbUserObserver())) {
            for (EbUser user : ebDemande.getxEbUserObserver()) {
                DemandeUserObservers obs = new DemandeUserObservers(ebDemande, user);
                observers.add(obs);
            }
            demandeObserversRepository.saveAll(observers);
        }
    }
}
