package com.adias.mytowereasy.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbAdresse;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class AdresseServiceImpl extends MyTowerService implements AdresseService {
    @Override
    public List<EbPlZone> getListZonesByCompanyNumAndAdresseRef(Integer ebCompanieNum, String refAdresse) {
        EbAdresse adresse = null;

        if (ebCompanieNum != null && refAdresse != null) {
            List<EbAdresse> listAdr = this.ebAdresseRepository
                .getByEbCompagnie_ebCompagnieNumAndReference(ebCompanieNum, refAdresse);
            if (listAdr != null && !listAdr.isEmpty()) adresse = listAdr.get(0);
        }

        return adresse != null ? adresse.getZones() : null;
    }

    @Override
    public Long getListAddressesCount(SearchCriteria criteria) {
        return daoAdresse.getListAddressesCount(criteria);
    }

    @Override
    public List<EbAdresse> getEbAdresseByCriteria(SearchCriteria criteria) {
        return daoAdresse.getListEbAdresse(criteria);
    }

    @Override
    public boolean areSameAddresses(EbParty party1, EbParty party2) {
        if (party1 == null && party2 == null) return true;
        if (party1 == null || party2 == null) return false;

        boolean cityEquals = StringUtils.equals(party1.getCity(), party2.getCity());
        boolean adresseEquals = StringUtils.equals(party1.getAdresse(), party2.getAdresse());
        boolean companyEquals = StringUtils.equals(party1.getCompany(), party2.getCompany());
        boolean zipCodeEquals = StringUtils.equals(party1.getZipCode(), party2.getZipCode());

        return cityEquals && adresseEquals && companyEquals && zipCodeEquals;
    }
}
