package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.apache.commons.math3.util.Precision;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaContext;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dao.DaoPricing;
import com.adias.mytowereasy.kafka.aspects.KafkaObjectType;
import com.adias.mytowereasy.kafka.aspects.SendMessageToKafka;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbTypeRequest;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.CancelStatus;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EbMarchandiseRepository;
import com.adias.mytowereasy.repository.EbTypeRequestRepository;
import com.adias.mytowereasy.repository.ExEbDemandeTransporteurRepository;
import com.adias.mytowereasy.service.email.EmailServicePricingBooking;

@Service
public class PricingAnnexMethodsService {

    @Autowired
    ListStatiqueService listStatiqueService;

    @Autowired
    private JpaContext jpaContext;

    @Autowired
    protected EbDemandeRepository ebDemandeRepository;

    @Autowired
    protected ConnectedUserService connectedUserService;
    @Autowired
    protected DaoPricing daoPricing;
    @Autowired
    protected EbMarchandiseRepository ebMarchandiseRepository;
    @Autowired
    protected EbTypeRequestRepository ebTypeRequestRepository;

    @Autowired
    protected ExEbDemandeTransporteurRepository exEbDemandeTransporteurRepository;
    @Autowired
    protected EmailServicePricingBooking emailServicePricingBooking;

		@SendMessageToKafka(
			trigger = Enumeration.QrGroupeTrigger.ON_TRANSPORT_FILE_CREATION,
			objectToSendType = KafkaObjectType.PROCESS_GROUP_OBJECT
		)
    public EbDemande afterDemandeNormalOrTrCreated(EbDemande savedDemande, EbUser connectedUser) {

        // Nom checking for Post Acheminement eligibility and do conversion if
        // required
        if (isEligibleForTrPrincipalConversion(savedDemande)) {
            savedDemande = convertDemandeNormalToTrPrincipal(savedDemande);
        }

        return savedDemande;
    }

    public boolean isEligibleForTrPrincipalConversion(EbDemande demande) {
        if (demande == null) return false;
        if (demande.getxEcNature() == null) return false;
        if (demande.getEbPartyDest() == null) return false;
        if (demande.getxEbEntrepotUnloading() == null) return false;
        if (demande.getEbPartyDest().getZone() == null) return false;
        if (demande.getPointIntermediate() == null) return false;
        if (demande.getPointIntermediate().getZone() == null) return false;
        if (!Enumeration.NatureDemandeTransport.NORMAL.getCode().equals(demande.getxEcNature())) return false;

        return true;
    }

    public EbDemande convertDemandeNormalToTrPrincipal(EbDemande demandeNormal) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        final Integer originalDemandeNum = demandeNormal.getEbDemandeNum();
        EntityManager ebDemandeEntityManager = jpaContext.getEntityManagerByManagedType(EbDemande.class);
        EntityManager ebMarchandiseEntityManager = jpaContext.getEntityManagerByManagedType(EbMarchandise.class);

        // 01 : Convert the normal demande nature to TP
        EbDemande demandeTP = changeNatureOfDemande(demandeNormal, Enumeration.NatureDemandeTransport.TRANSPORT_PRINCIPAL, true);

        // 02 : Create the initial demande
        EbDemande demandeIN = ebDemandeRepository.findById(originalDemandeNum).get();
        ebDemandeEntityManager.detach(demandeIN);
        ebDemandeEntityManager.close();
        demandeIN.setEbDemandeNum(daoPricing.nextValEbDemande());
        demandeIN = changeNatureOfDemande(demandeIN, Enumeration.NatureDemandeTransport.INITIAL, false);
        demandeIN.setxEcStatut(Enumeration.StatutDemande.RPL.getCode());
        demandeIN.setCancelled(true);

        // set libelle type request
        if (demandeIN.getxEbTypeRequest() != null) {
            EbTypeRequest typeRequest = ebTypeRequestRepository.findById(demandeIN.getxEbTypeRequest()).get();
            demandeIN.setTypeRequestLibelle(typeRequest.getReference() + "-" + typeRequest.getLibelle());
        }

        ebDemandeRepository.save(demandeIN);

        // Copy marchandises
        demandeIN.setListMarchandises(new ArrayList<>());
        List<EbMarchandise> marchandises = ebMarchandiseRepository.findAllByEbDemande_ebDemandeNum(originalDemandeNum);

        for (EbMarchandise marchandise : marchandises) {
            ebMarchandiseEntityManager.detach(marchandise);
            marchandise.setEbMarchandiseNum(null);
            marchandise.setEbDemande(demandeIN);
        }

        ebMarchandiseEntityManager.close();
        ebMarchandiseRepository.saveAll(marchandises);

        // 03 : Create multi segment object
        EbDemande demandeMSO = new EbDemande();
        demandeMSO.setEbDemandeNum(daoPricing.nextValEbDemande());
        demandeMSO.setRefTransport(demandeIN.getRefTransport());
        demandeMSO.setxEcNature(demandeIN.getxEcNature());
        demandeMSO = changeNatureOfDemande(demandeMSO, Enumeration.NatureDemandeTransport.MASTER_OBJECT, false);
        demandeMSO.setxEcMasterObjectType(Enumeration.MasterObjectType.MULTI_SEGMENT_DOCK.getCode());
        demandeMSO.setxEcStatut(Enumeration.StatutDemande.RPL.getCode());
        demandeMSO.setDateCreation(demandeIN.getDateCreation());
        demandeMSO.setUser(demandeIN.getUser());
        demandeMSO.setxEbUserCt(demandeIN.getxEbUserCt());
        demandeMSO.setxEbUserCtRequired(demandeIN.getxEbUserCtRequired());
        demandeMSO.setFlagEbInvoice(demandeIN.getFlagEbInvoice());
        demandeMSO.setFlagEbtrackTrace(demandeIN.getFlagEbtrackTrace());
        demandeMSO.setxEbEtablissement(demandeIN.getxEbEtablissement());
        demandeMSO.setFlagConnectedAsCt(demandeIN.getFlagConnectedAsCt());
        demandeMSO.setxEbUserImport(demandeIN.getxEbUserImport());
        demandeMSO.setxEbCompagnie(demandeIN.getxEbCompagnie());
        demandeMSO.setxEbCompagnieCt(demandeIN.getxEbCompagnieCt());
        demandeMSO.setModuleCreator(demandeIN.getModuleCreator());
        ebDemandeRepository.save(demandeMSO);

        // 04 : Make link between objects
        demandeIN.setEbDemandeMasterObject(demandeMSO);
        ebDemandeRepository.save(demandeIN);

        demandeTP.setEbDemandeInitial(demandeIN);
        demandeTP.setEbDemandeMasterObject(demandeMSO);
        // Don't save demande TP here, persist libelle before at part 5

        // 05 : Update destination of transport principal
        demandeTP.setEbPartyDest(demandeTP.getPointIntermediate());
        demandeTP.setPointIntermediate(null);
        demandeTP.setLibelleDestCity(demandeTP.getEbPartyDest().getCity());
        demandeTP.setLibelleDestCountry(demandeTP.getEbPartyDest().getxEcCountry().getLibelle());
        ebDemandeRepository.save(demandeTP);

        // 06 : Add historization to chat
        listStatiqueService
                .historizeAction(
                        demandeIN.getEbDemandeNum(),
                        Enumeration.IDChatComponent.PRICING.getCode(),
                        Enumeration.Module.PRICING.getCode(),
                        String.format("Initial request created. Related principal transport : %s", demandeTP.getRefTransport()),
                        connectedUser.getRealUserNum() != null ?
                                connectedUser.getRealUserNomPrenom() :
                                connectedUser.getNomPrenom(),
                        connectedUser.isControlTower() || connectedUser.getRealUserNum() != null ?
                                demandeTP.getUser().getNomPrenom() :
                                null);
        listStatiqueService
                .historizeAction(
                        demandeMSO.getEbDemandeNum(),
                        Enumeration.IDChatComponent.PRICING.getCode(),
                        Enumeration.Module.PRICING.getCode(),
                        String.format("Multi segment object created. Related request : %s", demandeIN.getRefTransport()),
                        connectedUser.getRealUserNum() != null ?
                                connectedUser.getRealUserNomPrenom() :
                                connectedUser.getNomPrenom(),
                        connectedUser.isControlTower() || connectedUser.getRealUserNum() != null ?
                                demandeTP.getUser().getNomPrenom() :
                                null);

        return demandeTP;
    }

    public EbDemande changeNatureOfDemande(EbDemande demande, Enumeration.NatureDemandeTransport newNature, boolean persist) {
        final Enumeration.NatureDemandeTransport oldNature = Enumeration.NatureDemandeTransport.fromCode(demande.getxEcNature());
        final String oldNaturePrefix = getReferenceNaturePrefix(oldNature);
        final String newNaturePrefix = getReferenceNaturePrefix(newNature);

        String newReference = demande.getRefTransport().replace("-" + oldNaturePrefix, "-" + newNaturePrefix);
        demande.setRefTransport(newReference);
        demande.setxEcNature(newNature.getCode());

        if (persist) ebDemandeRepository.save(demande);

        return demande;
    }

    public String getReferenceNaturePrefix(Enumeration.NatureDemandeTransport nature) {
        if (nature == null) return "";

        return nature.getLetter();
    }

		@SendMessageToKafka(
			trigger = Enumeration.QrGroupeTrigger.ON_TRANSPORT_FILE_UPDATE,
			objectToSendType = KafkaObjectType.PROCESS_GROUP_OBJECT
		)
		protected void postProcessUpdateEbDemande(EbDemande ebDemande, EbUser connectedUser) throws Exception
		{
        emailServicePricingBooking.sendEmailPricingFileUpdated(ebDemande, connectedUser);
        if (ebDemande.getExEbDemandeTransporteurs() != null &&
                !ebDemande.getExEbDemandeTransporteurs().isEmpty()) {
            exEbDemandeTransporteurRepository.saveAll(ebDemande.getExEbDemandeTransporteurs());
        }
    }

    public double roundDecimals(double value, int scale) {
        return Precision.round(value, scale);
    }

		public void cancelInitialTrs(Integer ebDemandeNum, boolean isConsolidation)
		{
			if (isConsolidation)
			{
			ebDemandeRepository
				.setCancelStatusByTrIds(
					ebDemandeRepository.getInitialIdsByGroupedIds(Collections.singletonList(ebDemandeNum)),
					CancelStatus.CANECLLED.getCode()
				);
			}
		}

		public void sendEmailsAndAlertsForQuotation(
			ExEbDemandeTransporteur exEbDemandeTransporteur,
			Integer moduleNum,
			Boolean isQuotationRequest,
			EbDemande ebDemande
		)
		{
			try
			{
				EbUser connectedUser = connectedUserService.getCurrentUser();

				if (Boolean.TRUE.equals(ebDemande.getFlagRecommendation()))
				{
					emailServicePricingBooking
						.sendEmailQuotationForwarder(
							connectedUser,
							ebDemande,
							Arrays.asList(exEbDemandeTransporteur)
						);
				}

				if (Enumeration.Module.PRICING.getCode().equals(moduleNum))
				{
					if (
						(connectedUser.isControlTower() || connectedUser.isPrestataire()) && !isQuotationRequest
					)
					{
						emailServicePricingBooking
							.sendMailQuotationResponseTransporteur(
								connectedUser,
								ebDemande,
								Arrays.asList(exEbDemandeTransporteur)
							);
						emailServicePricingBooking
							.sendMailQuotationResponseChargeur(
								connectedUser,
								ebDemande,
								Arrays.asList(exEbDemandeTransporteur)
							);
						emailServicePricingBooking
							.sendMailQuotationAlerteCt(connectedUser, ebDemande, exEbDemandeTransporteur);
					}

					if ((connectedUser.isChargeur() || connectedUser.isControlTower()) && isQuotationRequest)
					{
						List<ExEbDemandeTransporteur> demandeTransporteurs = exEbDemandeTransporteurRepository
							.findAllByXEbDemande_ebDemandeNum(ebDemande.getEbDemandeNum());

						emailServicePricingBooking
							.sendEmailQuotationResquestChargeur(
								ebDemande,
								demandeTransporteurs
									.stream()
									.map(ExEbDemandeTransporteur::getxTransporteur)
									.collect(Collectors.toList())
							);
						emailServicePricingBooking
							.sendEmailQuotationRequest(ebDemande, exEbDemandeTransporteur.getxTransporteur());
					}
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
}
