package com.adias.mytowereasy.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;
import com.adias.mytowereasy.airbus.model.EbQrGroupeProposition;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


public interface QrConsolidationService {
    List<EbQrGroupe> getListQrGroupe(SearchCriteria criteria);

    Long getListQrGroupeCount(SearchCriteria criteria);

    EbQrGroupe updateQrGroupe(EbQrGroupe ebTtQrGroupe) throws JsonProcessingException;

	EbQrGroupe updateQrGroupeAndProcessGroupage(EbQrGroupe ebQrGroupe) throws JsonProcessingException;

    void refreshQrGroupe(List<Integer> listEbQrGroupeNum, Integer refrechByTriggerType, Integer ebComagnieChargNum, EbUser connectedUser);

    boolean deleteQrGroupe(Integer id);

    Map<String, Object> getListQrGroupeData(SearchCriteria criteria) throws JsonProcessingException, IOException;

    List<EbQrGroupe> getListQrGroupeWithDemande(SearchCriteriaPricingBooking criteria);

		List<EbDemande> getListDemandeInProposition(EbQrGroupeProposition prop, EbUser connectedUser);

    int rejectPropositionDemande(Integer propoNum);

    EbDemande consolidatePropositionDemande(EbQrGroupeProposition propo);

    EbDemande createDemandeGroupe(EbQrGroupeProposition qrProp, Integer ebComagnieChargNum, EbUser connectedUser, List<EbQrGroupeProposition> props)
        throws JsonProcessingException;

    void cancelConsolidation(Integer ebDemandeNum);

    EbDemande setDataInResultingRequest(EbDemande resultQr, Integer checkedTrId, boolean isChecked) throws JsonProcessingException;

    List<Integer> getListDemandeByGroupingRule(Integer ebQrGroupeNum);

    EbQrGroupe activateQrGroupe(Integer ebQrGroupeNum) throws JsonProcessingException;

    void updateEligibleForConsolidation(Integer ebDemandeNum);

		void sendGroupedTrToKafka(Integer trId, Integer idTrResult);

		EbQrGroupe addQrGroupe(EbQrGroupe ebQrGroupe);

	void deleteQrGroupeAndQrGroupeUnconfirmedPropositions(Integer ebQrGroupeNum);
	
}
