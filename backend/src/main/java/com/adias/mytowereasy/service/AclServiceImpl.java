package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.airbus.model.EbUserProfile;
import com.adias.mytowereasy.dao.DaoAcl;
import com.adias.mytowereasy.dao.DaoUser;
import com.adias.mytowereasy.dto.EbAclRightDTO;
import com.adias.mytowereasy.dto.EbAclSettingDTO;
import com.adias.mytowereasy.model.EbAclRelation;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcAclRule;
import com.adias.mytowereasy.repository.EbAclRelationRepository;
import com.adias.mytowereasy.repository.EcAclRuleRepository;
import com.adias.mytowereasy.utils.search.SearchCriteriaACL;


@Service
public class AclServiceImpl implements AclService {
    @Autowired
    EcAclRuleRepository aclRuleRepository;

    @Autowired
    EbAclRelationRepository aclRelationRepository;

    @Autowired
    DaoAcl daoAcl;

    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    DaoUser daoUser;

    @Override
    public List<EbAclRightDTO> getACLsForConnectedUser() {
        SearchCriteriaACL criteria = new SearchCriteriaACL();
        EbUser connectedUser = connectedUserService.getCurrentUserFromDB();
        List<EbAclRightDTO> listAclRightDTO = new ArrayList<EbAclRightDTO>();

        if (connectedUser != null) {

            if (connectedUser.getEbUserProfile() != null) {
                criteria.setEbUserProfileNum(connectedUser.getEbUserProfile().getEbUserProfileNum());
            }
            else {
                criteria.setEbUserNum(connectedUser.getEbUserNum());
            }

            List<EbAclRelation> relations = daoAcl.listRelationForSession(criteria);
            List<EcAclRule> rules = daoAcl.listRuleForSession(criteria);

            listAclRightDTO = generateRightsDTO(rules, relations);
        }

        return listAclRightDTO;
    }

    @Override
    public List<EbAclSettingDTO> getSettingsForUser(Integer ebUserNum) {
        List<EcAclRule> rules = aclRuleRepository.findAllVisible();
        List<EbAclRelation> relations = aclRelationRepository.findByUser(ebUserNum);
        return generateSettingsDTO(rules, relations);
    }

    @Override
    public List<EbAclSettingDTO> getSettingsForProfile(Integer ebUserProfileNum) {
        List<EcAclRule> rules = aclRuleRepository.findAllVisible();
        List<EbAclRelation> relations = aclRelationRepository.findByProfile(ebUserProfileNum);
        return generateSettingsDTO(rules, relations);
    }

    /**
     * Generate low weight items with db access optimizations<br/>
     * Used for connected user session rights loading
     */
    private List<EbAclRightDTO> generateRightsDTO(List<EcAclRule> rules, List<EbAclRelation> relations) {
        List<EbAclRightDTO> results = new ArrayList<>();

        // Generate rules
        for (EcAclRule rule: rules) {
            EbAclRightDTO item = new EbAclRightDTO();

            item.setCode(rule.getCode());
            item.setValue(rule.getDefaultValue());
            item.setType(rule.getType());

            results.add(item);
        }

        // Bind relations values
        for (EbAclRelation rel: relations) {
            Optional<EbAclRightDTO> rightItem = results
                .stream().filter(rule -> rule.getCode().equals(rel.getRule().getCode())).findFirst();

            if (rightItem.isPresent()) {
                rightItem.get().setValue(rel.getValue());
            }

        }

        return results;
    }

    /**
     * Generate full filled items<br/>
     * Used for rights definition settings
     */
    private List<EbAclSettingDTO> generateSettingsDTO(List<EcAclRule> rules, List<EbAclRelation> relations) {
        List<EbAclSettingDTO> results = new ArrayList<>();

        // Generate rules
        for (EcAclRule rule: rules) {
            EbAclSettingDTO setting = new EbAclSettingDTO();

            setting.setCode(rule.getCode());
            setting.setDefaultValue(rule.getDefaultValue());
            setting.setEcAclRuleNum(rule.getEcAclRuleNum());
            setting.setExperimental(rule.getExperimental());

            List<String> themes = new ArrayList<>();

            if (!StringUtils.isBlank(rule.getThemes())) {
                String[] splittedThemes = rule.getThemes().split(";");
                themes = Stream.of(splittedThemes).collect(Collectors.toList());
            }

            setting.setThemes(themes);

            setting.setType(rule.getType());

            List<String> values = new ArrayList<>();

            if (!StringUtils.isBlank(rule.getValues())) {
                String[] splittedValuess = rule.getValues().split(";");
                values = Stream.of(splittedValuess).collect(Collectors.toList());
            }

            setting.setValues(values);

            results.add(setting);
        }

        // Bind relations values
        for (EbAclRelation rel: relations) {
            Optional<EbAclSettingDTO> setting = results
                .stream().filter(rule -> rule.getCode().equals(rel.getRule().getCode())).findFirst();

            if (setting.isPresent()) {
                setting.get().setSelectedValue(rel.getValue());
            }

        }

        return results;
    }

    @Override
    public void saveSettings(List<EbAclSettingDTO> settings, Integer ebUserProfileNum, Integer ebUserNum) {

        // First clean all relations
        if (ebUserProfileNum != null) {
            aclRelationRepository.deleteByProfile(ebUserProfileNum);
        }
        else if (ebUserNum != null) {
            aclRelationRepository.deleteByUser(ebUserNum);
        }

        // Generate relations objects
        List<EbAclRelation> relations = new ArrayList<>();

        for (EbAclSettingDTO setting: settings) {

            if (StringUtils.isBlank(setting.getSelectedValue()) || "null".equals(setting.getSelectedValue())) {
                continue;
            }

            EbAclRelation relation = new EbAclRelation();

            relation.setRule(new EcAclRule(setting.getEcAclRuleNum()));
            relation.setValue(setting.getSelectedValue());

            if (ebUserProfileNum != null) {
                relation.setProfile(new EbUserProfile(ebUserProfileNum));
            }
            else if (ebUserNum != null) {
                relation.setUser(new EbUser(ebUserNum));
            }

            relations.add(relation);
        }

        // Persist relations object
        aclRelationRepository.saveAll(relations);
    }
}
