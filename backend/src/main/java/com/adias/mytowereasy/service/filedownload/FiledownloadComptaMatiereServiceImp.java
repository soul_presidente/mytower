package com.adias.mytowereasy.service.filedownload;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.adias.mytowereasy.cptm.dto.EbCptmProcedureDTO;
import com.adias.mytowereasy.cptm.model.CPTMEnumeration.ProcedureStatus;
import com.adias.mytowereasy.cptm.model.SearchCriteriaCPTM;
import com.adias.mytowereasy.cptm.service.ComptaMatiereService;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.category.CategoryService;
import com.adias.mytowereasy.util.DateUtils;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class FiledownloadComptaMatiereServiceImp extends MyTowerService implements FiledownloadComptaMatiereService {
    @Autowired
    ComptaMatiereService comptaMatiereService;
    @Autowired
    ConnectedUserService connectedUserService;
    @Autowired
    CategoryService categoryService;

    @Override
    public FileDownloadStateAndResult listFileDownLoadComptaMatiere(
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        SearchCriteria searchCriteria)
        throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();

        ObjectMapper objectMapper = new ObjectMapper();
        SearchCriteriaCPTM searchCriteriaCPTM = objectMapper.convertValue(searchCriteria, SearchCriteriaCPTM.class);

        List<EbCptmProcedureDTO> listEbCptmProcedureDTO = comptaMatiereService.listProcedureDto(searchCriteriaCPTM);

        for (EbCptmProcedureDTO ebCptmProcedureDTO: listEbCptmProcedureDTO) {
            Map<String, String[]> map = new LinkedHashMap<String, String[]>();

            if (isPropertyInDtConfig(dtConfig, "refProcedure")) map.put("refProcedure", new String[]{
                "Procedure reference", ebCptmProcedureDTO.getRefProcedure()
            });
            if (isPropertyInDtConfig(
                dtConfig,
                "xRegimeTemporaireLeg1Name")) map.put("xRegimeTemporaireLeg1Name", new String[]{
                    "Leg type",
                    ebCptmProcedureDTO.getxRegimeTemporaireLeg1Name() != null ?
                        ebCptmProcedureDTO.getxRegimeTemporaireLeg1Name() :
                        ""
            });
            if (isPropertyInDtConfig(
                dtConfig,
                "xRegimeTemporaireLeg2Name")) map.put("xRegimeTemporaireLeg2Name", new String[]{
                    "Leg type",
                    ebCptmProcedureDTO.getxRegimeTemporaireLeg2Name() != null ?
                        ebCptmProcedureDTO.getxRegimeTemporaireLeg2Name() :
                        ""
            });
            if (isPropertyInDtConfig(
                dtConfig,
                "xRegimeTemporaireAlert1Days")) map.put("xRegimeTemporaireAlert1Days", new String[]{
                    "Alert1",
                    ebCptmProcedureDTO.getxRegimeTemporaireAlert1Days() != null ?
                        ebCptmProcedureDTO.getxRegimeTemporaireAlert1Days().toString() :
                        ""
            });
            if (isPropertyInDtConfig(
                dtConfig,
                "xRegimeTemporaireAlert2Days")) map.put("xRegimeTemporaireAlert2Days", new String[]{
                    "Alert2",
                    ebCptmProcedureDTO.getxRegimeTemporaireAlert2Days() != null ?
                        ebCptmProcedureDTO.getxRegimeTemporaireAlert2Days().toString() :
                        ""
            });
            if (isPropertyInDtConfig(
                dtConfig,
                "xRegimeTemporaireAlert3Days")) map.put("xRegimeTemporaireAlert3Days", new String[]{
                    "Alert3",
                    ebCptmProcedureDTO.getxRegimeTemporaireAlert3Days() != null ?
                        ebCptmProcedureDTO.getxRegimeTemporaireAlert3Days().toString() :
                        ""
            });
            if (isPropertyInDtConfig(dtConfig, "xLeg1UnitRef")) map.put("xLeg1UnitRef", new String[]{
                " Unit reference",
                ebCptmProcedureDTO.getxLeg1UnitRef() != null ? ebCptmProcedureDTO.getxLeg1UnitRef() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "xLeg1DemandeRef")) map.put("xLeg1DemandeRef", new String[]{
                "Transport reference",
                ebCptmProcedureDTO.getxLeg1DemandeRef() != null ? ebCptmProcedureDTO.getxLeg1DemandeRef() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "xLeg1DemandeStatus")) map.put("xLeg1DemandeStatus", new String[]{
                "Leg1 demande status",
                ebCptmProcedureDTO.getxLeg1DemandeStatus() != null ?
                    ebCptmProcedureDTO.getxLeg1DemandeStatus().toString() :
                    ""
            });
            if (isPropertyInDtConfig(
                dtConfig,
                "leg1ExpectedCustomerDeclarationDate")) map.put("leg1ExpectedCustomerDeclarationDate", new String[]{
                    "Expected customer declaration date",
                    DateUtils.formatUTCDateOnly(ebCptmProcedureDTO.getLeg1ExpectedCustomerDeclarationDate())
            });
            if (isPropertyInDtConfig(
                dtConfig,
                "leg1ActualCustomerDeclarationDate")) map.put("leg1ActualCustomerDeclarationDate", new String[]{
                    "Actual customer declaration date",
                    DateUtils.formatUTCDateOnly(ebCptmProcedureDTO.getLeg1ActualCustomerDeclarationDate())
            });
            if (isPropertyInDtConfig(dtConfig, "leg1Location")) map.put("leg1Location", new String[]{
                "Location", ebCptmProcedureDTO.getLeg1Location() != null ? ebCptmProcedureDTO.getLeg1Location() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "leg1TransitNumber")) map.put("leg1TransitNumber", new String[]{
                "Transit number",
                ebCptmProcedureDTO.getLeg1TransitNumber() != null ? ebCptmProcedureDTO.getLeg1TransitNumber() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "leg1TransitStatus")) map.put("leg1TransitStatus", new String[]{
                "Transit status",
                ebCptmProcedureDTO.getLeg1TransitStatus() != null ?
                    ebCptmProcedureDTO.getLeg1TransitStatus().toString() :
                    ""
            });
            if (isPropertyInDtConfig(dtConfig, "leg1UnitIndex")) map.put("leg1UnitIndex", new String[]{
                "Unit index",
                ebCptmProcedureDTO.getLeg1UnitIndex() != null ? ebCptmProcedureDTO.getLeg1UnitIndex().toString() : ""
            });

            if (isPropertyInDtConfig(dtConfig, "xLeg2UnitRef")) map.put("xLeg2UnitRef", new String[]{
                "Unit reference",
                ebCptmProcedureDTO.getxLeg2UnitRef() != null ? ebCptmProcedureDTO.getxLeg2UnitRef() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "xLeg2DemandeRef")) map.put("xLeg2DemandeRef", new String[]{
                "Transport reference",
                ebCptmProcedureDTO.getxLeg2DemandeRef() != null ? ebCptmProcedureDTO.getxLeg2DemandeRef() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "xLeg2DemandeStatus")) map.put("xLeg2DemandeStatus", new String[]{
                "Leg2 demande status",
                ebCptmProcedureDTO.getxLeg2DemandeStatus() != null ?
                    ebCptmProcedureDTO.getxLeg2DemandeStatus().toString() :
                    ""
            });
            if (isPropertyInDtConfig(
                dtConfig,
                "leg2ExpectedCustomerDeclarationDate")) map.put("leg2ExpectedCustomerDeclarationDate", new String[]{
                    "Expected customer declaration date",
                    DateUtils.formatUTCDateOnly(ebCptmProcedureDTO.getLeg2ExpectedCustomerDeclarationDate())
            });
            if (isPropertyInDtConfig(
                dtConfig,
                "leg2ActualCustomerDeclarationDate")) map.put("leg2ActualCustomerDeclarationDate", new String[]{
                    "Actual customer declaration date ",
                    DateUtils.formatUTCDateOnly(ebCptmProcedureDTO.getLeg2ActualCustomerDeclarationDate())
            });
            if (isPropertyInDtConfig(dtConfig, "leg2Location")) map.put("leg2Location", new String[]{
                "Location", ebCptmProcedureDTO.getLeg2Location() != null ? ebCptmProcedureDTO.getLeg2Location() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "leg2TransitNumber")) map.put("leg2TransitNumber", new String[]{
                "Transit number",
                ebCptmProcedureDTO.getLeg1TransitNumber() != null ? ebCptmProcedureDTO.getLeg1TransitNumber() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "leg2TransitStatus")) map.put("leg2TransitStatus", new String[]{
                "Transit status",
                ebCptmProcedureDTO.getLeg2TransitStatus() != null ?
                    ebCptmProcedureDTO.getLeg2TransitStatus().toString() :
                    ""
            });
            if (isPropertyInDtConfig(dtConfig, "leg2UnitIndex")) map.put("leg2UnitIndex", new String[]{
                "Unit index",
                ebCptmProcedureDTO.getLeg2UnitIndex() != null ? ebCptmProcedureDTO.getLeg2UnitIndex().toString() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "comment")) map.put("comment", new String[]{
                "Comment", ebCptmProcedureDTO.getComment() != null ? ebCptmProcedureDTO.getComment() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "deadline")) map.put("deadline", new String[]{
                "Limit date",
                ebCptmProcedureDTO.getDeadline() != null ? ebCptmProcedureDTO.getDeadline().toString() : ""
            });

            if (isPropertyInDtConfig(dtConfig, "status")) {
                String status = "";

                if (ebCptmProcedureDTO.getStatus() != null) {

                    for (ProcedureStatus enumValue: ProcedureStatus.values()) {
                        if (enumValue.getCode().equals(ebCptmProcedureDTO.getStatus())) status = enumValue.getKey();
                    }

                }

                map.put("status", new String[]{
                    "Status", status
                });
            }

            if (isPropertyInDtConfig(dtConfig, "nbAlerts")) map.put("nbAlerts", new String[]{
                "Nb alerts", ebCptmProcedureDTO.getNbAlerts() != null ? ebCptmProcedureDTO.getNbAlerts().toString() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "scheduledAlert")) map.put("scheduledAlert", new String[]{
                "Scheduled Alert",
                ebCptmProcedureDTO.getScheduledAlert() != null ? ebCptmProcedureDTO.getScheduledAlert().toString() : ""
            });
            if (isPropertyInDtConfig(dtConfig, "daysSinceImport")) map.put("daysSinceImport", new String[]{
                "Time since import (days)",
                ebCptmProcedureDTO.getDaysSinceImport() != null ?
                    ebCptmProcedureDTO.getDaysSinceImport().toString() :
                    ""
            });
            if (isPropertyInDtConfig(dtConfig, "daysBeforeDeadline")) map.put("daysBeforeDeadline", new String[]{
                "Time  before limit date (days)",
                ebCptmProcedureDTO.getDaysBeforeDeadline() != null ?
                    ebCptmProcedureDTO.getDaysBeforeDeadline().toString() :
                    ""
            });
            if (isPropertyInDtConfig(dtConfig, "leg1DeclarationNumber")) map.put("leg1DeclarationNumber", new String[]{
                "Declarartion number",
                ebCptmProcedureDTO.getLeg1DeclarationNumber() != null ?
                    ebCptmProcedureDTO.getLeg1DeclarationNumber().toString() :
                    ""
            });
            if (isPropertyInDtConfig(dtConfig, "leg2DeclarationNumber")) map.put("leg2DeclarationNumber", new String[]{
                "Declarartion number",
                ebCptmProcedureDTO.getLeg2DeclarationNumber() != null ?
                    ebCptmProcedureDTO.getLeg2DeclarationNumber().toString() :
                    ""
            });
            if (isPropertyInDtConfig(dtConfig, "leg1TracingStatus")) map.put("leg1TracingStatus", new String[]{
                "Status tracking",
                ebCptmProcedureDTO.getLeg1TracingStatus() != null ?
                    ebCptmProcedureDTO.getLeg1TracingStatus().toString() :
                    ""
            });
            if (isPropertyInDtConfig(dtConfig, "leg2TracingStatus")) map.put("leg2TracingStatus", new String[]{
                "Status tracking",
                ebCptmProcedureDTO.getLeg2TracingStatus() != null ?
                    ebCptmProcedureDTO.getLeg2TracingStatus().toString() :
                    ""
            });

            listresult.add(map);
        }

        resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
        resultAndState.setListResult(listresult);
        return resultAndState;
    }
}
