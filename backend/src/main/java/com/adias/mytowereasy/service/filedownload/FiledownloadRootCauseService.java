package com.adias.mytowereasy.service.filedownload;

import java.util.List;
import java.util.Map;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbCustomField;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;


public interface FiledownloadRootCauseService {
    public FileDownloadStateAndResult listFileDownLoadRootCause(
        Map<String, Object> mapCriteria,
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        List<EbCategorie> listCategorie,
        EbCustomField customField,
        String strCriteria,
        Map<String, String> mapCustomField,
        Map<String, List<Integer>> mapCategoryFields)
        throws Exception;
}
