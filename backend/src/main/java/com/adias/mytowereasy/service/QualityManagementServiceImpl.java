/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.dao.DaoTypeUnit;
import com.adias.mytowereasy.dto.mapper.TransportMapper;
import com.adias.mytowereasy.enumeration.CriticalityIncident;
import com.adias.mytowereasy.enumeration.RootCauseStatusEnum;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.IncidentStatus;
import com.adias.mytowereasy.model.Enumeration.Insurance;
import com.adias.mytowereasy.model.Enumeration.MarchandiseDangerousGood;
import com.adias.mytowereasy.model.Enumeration.TransortPriorityEnum;
import com.adias.mytowereasy.model.qm.*;
import com.adias.mytowereasy.model.tt.EbTtCategorieDeviation;
import com.adias.mytowereasy.repository.*;
import com.adias.mytowereasy.util.email.Email;
import com.adias.mytowereasy.util.email.EmailHtmlSender;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


@Service
public class QualityManagementServiceImpl extends MyTowerService implements QualityManagementService, CommonFlagSevice {
    private static final Logger LOGGER = LoggerFactory.getLogger(QualityManagementServiceImpl.class);

    @Autowired
    RootCauseService rootCauseService;

    @Autowired
    EbQmIncidentLineRepository eQmIncidentLineRepository;

    @Autowired
    EbQmChatRepository ebChatRepository;

    @Autowired
    EbEmailHistoriqueRepository ebEmailHistoriqueRepository;

    @Autowired
    EbQmRootCauseRepository ebQmRootCauseRepository;

    @Autowired
    EbQmCategoryRepository categoryRepository;

    @Autowired
    TypeDocumentService typeDocumentService;

    @Autowired
    ListStatiqueService listStatiqueService;

    @Autowired
    private TransportMapper mapper;

    @Autowired
    DaoTypeUnit daoTypeUnit;

    @Autowired(required = false)
    private TemplateEngine templateEngine;

    private EmailHtmlSender emailHtmlSender;

    private EbQmRootCause rootCause;

    @Autowired
    EbQmIncidentRepository ebQmIncidentRepository;

    @Transactional
    public EbQmIncident addEbIncident(EbQmIncident ebIncident) {
        return ebIncident;
    }

    @Autowired
    public QualityManagementServiceImpl(EmailHtmlSender emailHtmlSender) {
        this.emailHtmlSender = emailHtmlSender;
    }

    @Override
    public List<EbQmIncident> getAllEbIncident(SearchCriteriaQM criteria) {
        LOGGER.info("getAllEbIncident, criteria = {}", criteria);
        return ebIncidentRepository.findAll();
    }

    @Override
    public int getCountListEbIncident(SearchCriteriaQM criteria) throws Exception {
        LOGGER.info("getCountListEbIncident, criteria = {}", criteria);

        long count = daoQualityManagement.getCountList(criteria);

        return (int) count;
    }

    @Override
    public List<EbQmIncident> getListEbIncident(SearchCriteriaQM criterias) {
        List<EbQmIncident> listEbQmIncident = daoQualityManagement.getListEbIncident(criterias);
        return listEbQmIncident.stream().map(incident -> {
            if (incident.getDateIncident() == null) {
                incident.setDateIncident(incident.getDateCreation());
            }
            return incident;
        }).collect(Collectors.toList());
    }

    @Override
    public List<EbQmCategory> listCategory() {
        // TODO Auto-generated method stub

        List<EbQmCategory> list = categoryRepository.findAll();
        return list;
    }

    @Transactional
    @Override
    public Map<Integer, Map<String, Object>>
        saveIncidents(List<EbQmIncident> incidents, Boolean isUpdate, EbUser connectedUser) {
        Map<Integer, Map<String, Object>> result = new HashMap<Integer, Map<String, Object>>();
        String action = "";
        connectedUser = connectedUser == null ? connectedUserService.getCurrentUser() : connectedUser;
        Integer moduleNum = Enumeration.Module.QUALITY_MANAGEMENT.getCode();
        EbQmCategory cat = null;
        EbEtablissement etablissement = null;
        EbUser userConnected = connectedUserService.getCurrentUser();
        SearchCriteriaQM criteria = new SearchCriteriaQM();
        EbQmIncident oldIncident = null;
        ObjectMapper objectMapper = new ObjectMapper();

        for (EbQmIncident incident: incidents) {

            if (incident.getxEbDemande() == null) throw new MyTowerException("Demande id must not be null");
                Optional<EbDemande> demande = ebDemandeRepository.findById(incident.getxEbDemande());
                incident.setEbDemande(new EbDemande(incident.getxEbDemande()));

                if (isUpdate) {
                    criteria.setEbQmIncidentNum(incident.getEbQmIncidentNum());
                    oldIncident = ebIncidentRepository.findById(incident.getEbQmIncidentNum()).orElse(null);
                }
                else {
                    oldIncident = new EbQmIncident(incident.getEbQmIncidentNum());
                    oldIncident.setCategory(new EbTtCategorieDeviation());
                }

                EbParty origin = demande.get().getEbPartyOrigin();
                EbParty dest = demande.get().getEbPartyDest();

                if (!isUpdate) {
                    incident.setxEbCompagnie(demande.get().getxEbCompagnie());
                    etablissement = demande.get().getxEbEtablissement();

                    if (incident.getCurrency() != null && incident.getCurrency().getEcCurrencyNum() != null) {
                        EcCurrency currency = new EcCurrency();
                        currency = ecCurrencyRepository.findById(incident.getCurrency().getEcCurrencyNum()).get();
                        incident.setCurrency(currency);
                        incident.setCurrencyLabel(currency.getLibelle());
                    }
                    else {
                        incident.setCurrency(demande.get().getXecCurrencyInvoice());
                        if (incident.getCurrency()
                            != null) incident.setCurrencyLabel(incident.getCurrency().getLibelle());
                    }

                    incident.setxEbEtablissement(etablissement);

                    if (demande.get().getxEbTransporteurEtablissementNum() != null) {
                        EbEtablissement etablissementCarrier = ebEtablissementRepository
                            .findOneByEbEtablissementNum(demande.get().getxEbTransporteurEtablissementNum());
                        incident.setCarrier(etablissementCarrier);
                        incident.setCarrierNom(etablissementCarrier.getNom());
                    }

                    else if (demande.get().getExEbDemandeTransporteurs() != null &&
                        demande.get().getExEbDemandeTransporteurs().size() > 0) {
                            EbEtablissement etablissementCarrier = demande
                                .get().getExEbDemandeTransporteurs().get(0).getxEbEtablissement();
                            incident.setCarrier(etablissementCarrier);
                            incident.setCarrierNom(etablissementCarrier.getNom());
                        }
                    else incident.setCarrier(null);

                    incident.setOriginCountryLibelle(demande.get().getLibelleOriginCountry());
                    incident.setDestinationCountryLibelle(demande.get().getLibelleDestCountry());

                    if (origin != null) {
                        incident.setOriginCountry(origin.getxEcCountry());
                    }

                    if (dest != null) {
                        incident.setDestinationCountry(dest.getxEcCountry());
                    }

                    // recuperation des customField
                    incident.setCustomFields(demande.get().getCustomFields());
                    incident.setListCustomsFields(demande.get().getListCustomsFields());
                    incident.setListCustomFieldsFlat(demande.get().getListCustomFieldsFlat());

                    // Recuperation de la list des categorie
                    incident.setListCategoriesStr(demande.get().getListCategoriesStr());
                    incident.setListCategoryFlat(demande.get().getListCategoryFlat());

                    if (demande.get().getListCategories() != null) {
                        Gson gson = new Gson();
                        String json = gson.toJson(demande.get().getListCategories());
                        incident.setListCategories(gson.fromJson(json, new TypeToken<ArrayList<EbCategorie>>() {
                        }.getType()));
                    }

                    incident.setListLabels(demande.get().getListLabels());

                    if (incident.getCategory() != null && incident.getCategory().getLibelle() != null) {
                        incident.setCategoryLabel(incident.getCategory().getLibelle());
                    }

                    incident.setModeTransport(demande.get().getxEcModeTransport());
                    incident.setQualificationLabel("Incident");

                    incident.setQualification(new EbQmQualification(1));// l'id du
                    // Incident
                    // dans la
                    // table ==
                    // 1

                    // donnéé header incident
                    incident.setDemandeTranportRef(demande.get().getRefTransport());

                    // set type documents
                    List<EbTypeDocuments> typesDocument = new ArrayList<>();

                    try {
                        typesDocument = typeDocumentService
                            .getTypeDocsByEbTypeDocumentsModule(moduleNum, connectedUser);
                    } catch (Exception e1) {
                        // TODO Auto-generated catch block
                        e1.printStackTrace();
                    }

                    incident.setListTypeDocuments(typesDocument);
                }

                if (demande.get().getTypeRequestLibelle() != null) {
                    incident.setTypeRequestLibelle(demande.get().getTypeRequestLibelle());
                    incident
                    .setTypeRequestNum(TransortPriorityEnum.getCodeByLibelle(demande.get().getTypeRequestLibelle()));
                }

                List<EbQmIncidentLine> newListIncidentLine = new ArrayList<>();

                for (EbQmIncidentLine il: incident.getIncidentLines()) {

                    if (!isUpdate) {
                        il.setIncident(incident);
                        il.setCarrier(etablissement);
                        il.setCarrierName(etablissement.getNom());
                    }
                    else {
                        EbQmIncidentLine oldIl = null;

                        if (oldIncident.getIncidentLines() != null) {
                            oldIl = oldIncident
                                .getIncidentLines().stream()
                                .filter(line -> line.getEbQmIncidentLineNum().equals(il.getEbQmIncidentLineNum()))
                                .findFirst().orElse(null);
                        }

                        if (il.getUnitReference() != null && !il.getUnitReference().equals(oldIl.getUnitReference())) {

                            if (action.indexOf("Incident information updated :") < 0) {
                                action += "Incident information updated : ";
                            }

                            action += "Unit reference : " +
                            il.getUnitReference() + " <strike> " + oldIl.getUnitReference() + " </strike> <br> ";
                            oldIl.setUnitReference(il.getUnitReference());
                        }

                        if (il.getTypeMarchandise() != null &&
                            !il.getTypeMarchandise().equals(oldIl.getTypeMarchandise())) {

                            if (action.indexOf("Incident information updated :") < 0) {
                                action += "Incident information updated : ";
                            }

                            action += "Type of unit : " +
                                il.getTypeMarchandise() + " <strike> " + oldIl.getTypeMarchandise() +
                                " </strike> <br> ";
                            oldIl.setTypeMarchandise(il.getTypeMarchandise());
                        }

                        if (il.getNumberOfUnits() != null && !il.getNumberOfUnits().equals(oldIl.getNumberOfUnits())) {

                            if (action.indexOf("Incident information updated :") < 0) {
                                action += "Incident information updated : ";
                            }

                            action += "Total Quantity of unit : " +
                                il.getNumberOfUnits() + " <strike> " + oldIl.getNumberOfUnits() + " </strike> <br> ";
                            oldIl.setNumberOfUnits(il.getNumberOfUnits());
                        }

                        if (il.getNumberOfUnitsImpacted() != null &&
                            !il.getNumberOfUnitsImpacted().equals(oldIl.getNumberOfUnitsImpacted())) {

                            if (action.indexOf("Incident information updated :") < 0) {
                                action += "Incident information updated : ";
                            }

                            action += "Impacted Quantity of unit : " +
                                il.getNumberOfUnitsImpacted() + " <strike> " + oldIl.getNumberOfUnitsImpacted() +
                                " </strike> <br> ";
                            oldIl.setNumberOfUnitsImpacted(il.getNumberOfUnitsImpacted());
                        }

                        if (il.getWeight() != null && !il.getWeight().equals(oldIl.getWeight())) {

                            if (action.indexOf("Incident information updated :") < 0) {
                                action += "Incident information updated : ";
                            }

                            action += "Total Gross Weight (kg) : " +
                                il.getTypeMarchandise() + " <strike> " + oldIl.getTypeMarchandise() +
                                " </strike> <br> ";
                            oldIl.setTypeMarchandise(il.getTypeMarchandise());
                        }

                        if (il.getWeightImpacted() != null &&
                            !il.getWeightImpacted().equals(oldIl.getWeightImpacted())) {

                            if (action.indexOf("Incident information updated :") < 0) {
                                action += "Incident information updated : ";
                            }

                            action += "Impacted Gross Weight (kg) : " +
                                il.getWeightImpacted() + " <strike> " + oldIl.getWeightImpacted() + " </strike> <br> ";
                            oldIl.setWeightImpacted(il.getWeightImpacted());
                        }

                        if (il.getEtd() != null && !il.getEtd().equals(oldIl.getEtd())) {

                            if (action.indexOf("Incident information updated :") < 0) {
                                action += "Incident information updated : ";
                            }

                        action += "Estimated Date of Departure : " +
                            il.getEtd() + " <strike> " + oldIl.getEtd() + " </strike> <br> ";
                            oldIl.setEtd(il.getEtd());
                        }

                        if (il.getAtd() != null && !il.getAtd().equals(oldIl.getAtd())) {

                            if (action.indexOf("Incident information updated :") < 0) {
                                action += "Incident information updated : ";
                            }

                        action += "Actual Date of Departure : " +
                            il.getAtd() + " <strike> " + oldIl.getAtd() + " </strike> <br> ";
                            oldIl.setAtd(il.getAtd());
                        }

                        if (il.getEta() != null && !il.getEta().equals(oldIl.getEta())) {

                            if (action.indexOf("Incident information updated :") < 0) {
                                action += "Incident information updated : ";
                            }

                        action += "Estimated Date of Arrival : " +
                            il.getEta() + " <strike> " + oldIl.getEta() + " </strike> <br> ";
                            oldIl.setEta(il.getEta());
                        }

                        if (il.getAta() != null && !il.getAta().equals(oldIl.getAta())) {

                            if (action.indexOf("Incident information updated :") < 0) {
                                action += "Incident information updated : ";
                            }

                        action += "Actual Date of Arrival : " +
                            il.getAta() + " <strike> " + oldIl.getAta() + " </strike> <br> ";
                            oldIl.setAta(il.getAta());
                        }

                        if (il.getDangerousGood() != null && !il.getDangerousGood().equals(oldIl.getDangerousGood())) {

                            if (action.indexOf("Incident information updated :") < 0) {
                                action += "Incident information updated : ";
                            }

                            MarchandiseDangerousGood dgr = MarchandiseDangerousGood.getByCode(il.getDangerousGood());
                            MarchandiseDangerousGood oldDgr = MarchandiseDangerousGood
                                .getByCode(oldIl.getDangerousGood());
                            action += "DGR(UN)	 : " + dgr != null ?
                                dgr.getLibelle() :
                                "null" + " <strike> " + oldDgr != null ?
                                    oldDgr.getLibelle() :
                                    "null" + " </strike> <br> ";

                            oldIl.setDangerousGood(il.getDangerousGood());
                        }

                        if (il.getComment() != null && !il.getComment().equals(oldIl.getComment())) {

                            if (action.indexOf("Incident information updated :") < 0) {
                                action += "Incident information updated : ";
                            }

                        action += "Comments : " +
                            il.getComment() + " <strike> " + oldIl.getComment() + " </strike> <br> ";
                            oldIl.setComment(il.getComment());
                        }

                        newListIncidentLine.add(oldIl);
                    }

                }

            if (isUpdate) {
                oldIncident.setCategoryLabel(incident.getCategory().getLibelle());
                oldIncident.setIncidentLines(newListIncidentLine);
            }

                if ((incident.getCategory() != null &&
                    incident.getCategory().getEbTtCategorieDeviationNum() != null &&
                    oldIncident.getCategory() == null) ||
                (incident.getCategory() != null &&
                    incident.getCategory().getEbTtCategorieDeviationNum() != null && !incident
                            .getCategory().getEbTtCategorieDeviationNum()
                            .equals(oldIncident.getCategory().getEbTtCategorieDeviationNum()))) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                    String libelle = "";

                    if (oldIncident.getCategory() == null) {
                        libelle = "null";
                    }
                    else {
                        libelle = oldIncident.getCategory().getLibelle();
                    }

                    action += "Incident category : " +
                        incident.getCategory().getLibelle() + " <strike> " + libelle + " </strike> <br> ";
                    oldIncident.setCategory(incident.getCategory());
                }

                if (incident.getIncidentDesc() != null &&
                    !incident.getIncidentDesc().equals(oldIncident.getIncidentDesc())) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                    action += "Incident description : " +
                        incident.getIncidentDesc() + " <strike> " + oldIncident.getIncidentDesc() + " </strike> <br> ";
                    oldIncident.setIncidentDesc(incident.getIncidentDesc());
                }

                if (incident.getIncidentDesc() != null &&
                    !incident.getIncidentDesc().equals(oldIncident.getIncidentDesc())) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                    action += "Additional impact : " +
                    incident.getAddImpact() + " <strike> " + oldIncident.getAddImpact() + " </strike> <br> ";
                    oldIncident.setAddImpact(incident.getAddImpact());
                }

                if (incident.getAddImpact() != null && !incident.getAddImpact().equals(oldIncident.getAddImpact())) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                    action += "Additional impact : " +
                    incident.getAddImpact() + " <strike> " + oldIncident.getAddImpact() + " </strike> <br> ";
                    oldIncident.setAddImpact(incident.getAddImpact());
                }

                if (incident.getInsurance() != null && !incident.getInsurance().equals(oldIncident.getInsurance())) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                action += "Insurance other than transport : " +
                    Insurance.getLibelleByCode(incident.getInsurance()) + " <strike> " +
                    Insurance.getLibelleByCode(oldIncident.getInsurance()) + " </strike> <br> ";
                    oldIncident.setInsurance(incident.getInsurance());
                }

                if (incident.getUnitDamagedAmount() != null &&
                    !incident.getUnitDamagedAmount().equals(oldIncident.getUnitDamagedAmount())) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                    action += "Impacted Goods amount : " +
                        incident.getUnitDamagedAmount() + " <strike> " + oldIncident.getUnitDamagedAmount() +
                        " </strike> <br> ";
                    oldIncident.setUnitDamagedAmount(incident.getUnitDamagedAmount());
                }

                if (incident.getImpactedTransportPercentage() != null &&
                    !incident.getImpactedTransportPercentage().equals(oldIncident.getImpactedTransportPercentage())) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                    action += "Impacted Transport amount : " +
                        incident.getImpactedTransportPercentage() + " <strike> " +
                        oldIncident.getImpactedTransportPercentage() + " </strike> <br> ";
                    oldIncident.setImpactedTransportPercentage(incident.getImpactedTransportPercentage());
                }

                if (incident.getClaimAmount() != null &&
                    !incident.getClaimAmount().equals(oldIncident.getClaimAmount())) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                    action += "Claim amount : " +
                    incident.getClaimAmount() + " <strike> " + oldIncident.getClaimAmount() + " </strike> <br> ";
                    oldIncident.setClaimAmount(incident.getClaimAmount());
                }

                if (incident.getRecovered() != null && !incident.getRecovered().equals(oldIncident.getRecovered())) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                    action += "Recovered from carrier : " +
                        incident.getRecovered() + " <strike> " + oldIncident.getRecovered() + " </strike> <br> ";
                    oldIncident.setRecovered(incident.getRecovered());
                }

                if (incident.getRecoveredInsurance() != null &&
                    !incident.getRecoveredInsurance().equals(oldIncident.getRecoveredInsurance())) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                    action += "Recovered from insurance : " +
                        incident.getRecoveredInsurance() + " <strike> " + oldIncident.getRecoveredInsurance() +
                        " </strike> <br> ";
                    oldIncident.setRecoveredInsurance(incident.getRecoveredInsurance());
                }

                if (incident.getBalance() != null && !incident.getBalance().equals(oldIncident.getBalance())) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                action += "Balance : " +
                    incident.getBalance() + " <strike> " + oldIncident.getBalance() + " </strike> <br> ";
                    oldIncident.setBalance(incident.getBalance());
                }

                if (incident.getImpactedTransportAmount() != null &&
                    !incident.getImpactedTransportAmount().equals(oldIncident.getImpactedTransportAmount())) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                    action += "Impacted Transport amount : " +
                        incident.getImpactedTransportAmount() + " <strike> " +
                        oldIncident.getImpactedTransportAmount() + " </strike> <br> ";
                    oldIncident.setImpactedTransportAmount(incident.getImpactedTransportAmount());
                }

                if (incident.getCriticality() != null &&
                    !incident.getCriticality().equals(oldIncident.getCriticality())) {

                    if (action.indexOf("Incident information updated :") < 0) {
                        action += "Incident information updated : ";
                    }

                    String oldCriticality = null, criticality = null;

                    for (CriticalityIncident ci: CriticalityIncident.getCriticalityIncident()) {

                        if (ci.getCode().equals(oldIncident.getCriticality())) {
                            oldCriticality = ci.getKey();
                        }

                        if (ci.getCode().equals(incident.getCriticality())) {
                            criticality = ci.getKey();
                        }

                    }

                    action += "Criticality : " + criticality + " <strike> " + oldCriticality + " </strike> <br> ";
                    oldIncident.setCriticality(incident.getCriticality());
                }

                // partie Root Causes Analysis
                if (incident.getRootCauseStatut() != null &&
                    !incident.getRootCauseStatut().equals(oldIncident.getRootCauseStatut())) {

                    if (action.indexOf("Root Causes Analysis updated :") < 0) {
                        action += "Root Causes Analysis updated : <br>";
                    }

                    String oldStatus = null, status = null;

                    for (RootCauseStatusEnum ci: RootCauseStatusEnum.getRootCauseStatusEnum()) {

                        if (ci.getCode().equals(oldIncident.getRootCauseStatut())) {
                            oldStatus = ci.getKey();
                        }

                        if (ci.getCode().equals(incident.getRootCauseStatut())) {
                            status = ci.getKey();
                        }

                    }

                    action += "Root causes status : " + status + " <strike> " + oldStatus + " </strike> <br> ";
                    oldIncident.setRootCauseStatut(incident.getRootCauseStatut());
                }

                if (incident.getWaitingForText() != null &&
                    !incident.getWaitingForText().equals(oldIncident.getWaitingForText())) {

                    if (action.indexOf("Root Causes Analysis updated :") < 0) {
                        action += "Root Causes Analysis updated : <br>";
                    }

                    action += "Waiting For Text : " +
                        incident.getWaitingForText() + " <strike> " + oldIncident.getWaitingForText() +
                        " </strike> <br> ";
                    oldIncident.setWaitingForText(incident.getWaitingForText());
                }

                if (incident.getSeverity() != null && !incident.getSeverity().equals(oldIncident.getSeverity())) {

                    if (action.indexOf("Root Causes Analysis updated :") < 0) {
                        action += "Root Causes Analysis updated : <br>";
                    }

                    String oldSeverity = null, severity = null;

                    for (CriticalityIncident ci: CriticalityIncident.getCriticalityIncident()) {

                        if (ci.getCode().equals(oldIncident.getSeverity())) {
                            oldSeverity = ci.getKey();
                        }

                        if (ci.getCode().equals(incident.getSeverity())) {
                            severity = ci.getKey();
                        }

                    }

                    action += "Severity : " + severity + " <strike> " + oldSeverity + " </strike> <br> ";
                    oldIncident.setSeverity(incident.getSeverity());
                }

                if (incident.getActions() != null && !incident.getActions().equals(oldIncident.getActions())) {

                    if (action.indexOf("Root Causes Analysis updated :") < 0) {
                        action += "Root Causes Analysis updated : <br>";
                    }

                action += "Actions : " +
                    incident.getActions() + " <strike> " + oldIncident.getActions() + " </strike> <br> ";
                    oldIncident.setActions(incident.getActions());
                }

                if (incident.getRootCauseCategory() != null &&
                    !incident.getRootCauseCategory().equals(oldIncident.getRootCauseCategory())) {

                    if (action.indexOf("Root Causes Analysis updated :") < 0) {
                        action += "Root Causes Analysis updated : <br>";
                    }

                    action += "Category : " +
                        incident.getRootCauseCategory() + " <strike> " + oldIncident.getRootCauseCategory() +
                        " </strike> <br> ";
                    oldIncident.setRootCauseCategory(incident.getRootCauseCategory());
                }

                if (incident.getResponsable() != null &&
                    !incident.getResponsable().equals(oldIncident.getResponsable())) {

                    if (action.indexOf("Root Causes Analysis updated :") < 0) {
                        action += "Root Causes Analysis updated : <br>";
                    }

                action += "Responsable : " +
                    incident.getResponsable() + " <strike> " + oldIncident.getResponsable() + " </strike> <br> ";
                    oldIncident.setResponsable(incident.getResponsable());
                }

                if (incident.getInsuranceComment() != null &&
                    !incident.getInsuranceComment().equals(oldIncident.getInsuranceComment())) {

                    if (action.indexOf("Root Causes Analysis updated :") < 0) {
                        action += "Root Causes Analysis updated : <br>";
                    }

                    action += "Comments : " +
                        incident.getInsuranceComment() + " <strike> " + oldIncident.getInsuranceComment() +
                        " </strike> <br> ";
                    oldIncident.setInsuranceComment(incident.getInsuranceComment());
                }

                List<EbQmRootCause> rootCausesToSave = new ArrayList<EbQmRootCause>();

                if (incident.getRootCauses() != null) {
                    Integer nbreRC = ebQmRootCauseRepository
                        .countListRoutCauseByRelatedIncident(incident.getIncidentRef());

                    // pour deserialiser la liste et eviter l'erreur lincked hashMap

                    List<String> rootCauses = new ArrayList<>();
                    List<String> oldRootCauses = null;

                    if (oldIncident.getRootCauses() != null) {
                        oldIncident
                        .setRootCauses(
                            Arrays
                                    .asList(
                                        objectMapper.convertValue(oldIncident.getRootCauses(), EbQmRootCause[].class)));

                        oldRootCauses = oldIncident
                            .getRootCauses().stream().map(EbQmRootCause::getLabel).collect(Collectors.toList());
                    }

                    for (EbQmRootCause e: incident.getRootCauses()) {
                        rootCauses.add(e.getLabel());
                        boolean isEqualOrNull = false;

                        try {
                            isEqualOrNull = e.getRelatedIncident().equals(incident.getIncidentRef());
                        } catch (Exception ex) {
                        }

                        e.setSeverite(incident.getSeverity());

                        if (e.getListIncidentNum() == null ||
                            !e.getListIncidentNum().contains(":" + incident.getEbQmIncidentNum() + ":")) e
                                .setListIncidentNum(
                                    (e.getListIncidentNum() != null ? ":" + e.getListIncidentNum() + ":" : "") +
                                        incident.getEbQmIncidentNum());

                        e.setEventType(incident.getEventType());

                        e.setRelatedActions(incident.getActions());
                    e.setActions(incident.getActions());

                        e.setGroupe(incident.getCategoryLabel()); // category de
                        // l'incident

                        e
                        .setPerimetreImpacte(
                            incident.getImpactedTransportPercentage() != null ?
                                incident.getImpactedTransportPercentage() + "%" :
                                    null);
                        e.setName(e.getLabel());

                        if (incident.getClaimAmount() != null ||
                            incident.getBalance() != null || incident.getRecoveredInsurance() != null) {
                            e.setStatut(IncidentStatus.CLAIM.getCode());
                        }
                        else {
                            e.setStatut(IncidentStatus.INTENT_TO_CLAIM.getCode());
                        }

                        e.setCreateur(userConnected.getPrenomNom());
                        e.setRelatedIncident(incident.getIncidentRef());

                        if (e.getQmRootCauseNum() == null) {
                            e.setRef("RC" + (++nbreRC) + "-" + incident.getIncidentRef());
                        }
                    e.setEtablissement(userConnected.getEbEtablissement());

                    rootCausesToSave.add(e);
                    ebQmRootCauseRepository.save(e);
                }

                    // historisation ROOTCAUSES
                    if (rootCauses != null) rootCauses = rootCauses.stream().sorted().collect(Collectors.toList());

                    if (oldRootCauses
                        != null) oldRootCauses = oldRootCauses.stream().sorted().collect(Collectors.toList());

                    if (rootCauses != null || oldRootCauses != null) {
                        Boolean isRcUpdeted = true;

                        if (rootCauses != null && oldRootCauses != null) {
                            isRcUpdeted = !oldRootCauses.equals(rootCauses);
                        }

                        if (isRcUpdeted) {

                            if (action.indexOf("Root Causes Analysis updated :") < 0) {
                                action += "Root Causes Analysis updated : <br>";
                            }

                            String newRc = rootCauses != null ? String.join(",", rootCauses) : null;
                            String oldRc = oldRootCauses != null ? String.join(",", oldRootCauses) : null;

                            action += "Root Causes : " + newRc + " <strike> " + oldRc + " </strike> <br> ";
                        }

                    }

                    if (rootCausesToSave != null && !rootCausesToSave.isEmpty()) {
                        String newRootCause = incident.getListRootCauses();

                        for (EbQmRootCause rootCause: rootCausesToSave) {

                            if (rootCause.getQmRootCauseNum() == null) {
                                rootCause.setQmRootCauseNum(ebQmRootCauseRepository.getNextSequenceId());
                                newRootCause += ":" + rootCause.getQmRootCauseNum() + ":";
                            }

                        }

                        ebQmRootCauseRepository.saveAll(rootCausesToSave);
                        incident.setListRootCauses(newRootCause);
                    }

                }

                // c'est pas la peine d'enregistrer l'etablissement dans le json
                // list routRauses

            /*
             * New comment for Syntiche
             * pourquoi setter l'etab à null ?
             * nous avons besoin de l'étab pour afficher la root cause sur
             * le dashboard Root Causes Analysis
             * for (EbQmRootCause rc: rootCausesToSave) {
             * rc.setEtablissement(null);
             * }
             */

                if (isUpdate) {
                    oldIncident.setRootCauses(rootCausesToSave);
                    oldIncident.setListRootCauses(incident.getListRootCauses());
                    incident = oldIncident;
                }
                else {
                    incident.setRootCauses(rootCausesToSave);
                }

                incident.setLastContributer(new EbUser(connectedUser.getEbUserNum()));
                incident.setLastContributerNomPremon(connectedUser.getNomPrenom());
                // Owner of the request
                incident.setOwnerNomPrenom(demande.get().getUser().getNomPrenom());
                incident.setOwnerNum(demande.get().getUser().getEbUserNum());
            if (incident.getEbQmIncidentNum()
                == null) incident.setEbQmIncidentNum(daoQualityManagement.nextValQuality());

            ebQmIncidentRepository.save(incident);

            if (!action.isEmpty()) {
                EbChat ebChat = listStatiqueService
                    .historizeAction(
                        incident.getEbQmIncidentNum(),
                        Enumeration.IDChatComponent.QUALITY_MANAGEMENT.getCode(),
                        moduleNum,
                        action,
                        connectedUser.getNom() + " " + connectedUser.getPrenom(),
                        connectedUser.getRealUserNum() != null ?
                            connectedUser.getRealUserNomPrenom() :
                            connectedUser.getNomPrenom(),
                        connectedUser.getRealUserNum() != null ? connectedUser.getNomPrenom() : null);
                Map<String, Object> Resultvalues = new HashMap<>();
                Resultvalues.put("ebChat", ebChat);
                Resultvalues.put("rootCauses", rootCausesToSave);

                result.put(incident.getEbQmIncidentNum(), Resultvalues);

                }

                oldIncident = null;
                action = "";


        }

        return result;
    }

    /**
     * @param label
     * @return
     */
    public EbQmRootCause getRootCauses(String label) {
        rootCause = null;
        List<EbQmRootCause> rootCauses = new ArrayList<EbQmRootCause>();
        rootCauses = rootCauseService.listRootCause(null);
        rootCauses.forEach(e -> {
            if (e.getLabel() == label) rootCause = e;
        });
        return rootCause;
    }

    /**
     * @param label
     * @return
     */
    public boolean rootCausesExist(String label) {
        rootCause = null;
        List<EbQmRootCause> rootCauses = new ArrayList<EbQmRootCause>();
        rootCauses = rootCauseService.listRootCause(null);
        rootCauses.forEach(e -> {
            if (e.getLabel().equals(label)) rootCause = e;
        });

        if (rootCause != null) {
            return true;
        }
        else {
            return false;
        }

    }

    @Override
    public List<EbQmQualification> listQmQualification() {
        // TODO Auto-generated method stub
        return ebQmQualificationRepository.findAll();
    }

    @Override
    public List<EbQmRootCause> listQmRootCause() {
        // TODO Auto-generated method stub
        return ebQmRootCauseRepository.findAll();
    }

    @Override
    public List<EbQmCloseReason> listQmCloseReason() {
        // TODO Auto-generated method stub
        return ebQmClosingReasonRepository.findAll();
    }

    /*
     * (non-Javadoc)
     * @see
     * com.adias.mytowereasy.service.QualityManagementService#getEbIncident(com.
     * adias.mytowereasy.utils.search.SearchCriteriaQM)
     */
    @Override
    public EbQmIncident getEbIncident(SearchCriteriaQM criteria) {
        // TODO Auto-generated method stub
        return daoQualityManagement.getIncident(criteria);
    }

    /*
     * (non-Javadoc)
     * @see
     * com.adias.mytowereasy.service.QualityManagementService#listMarchandise(
     * com.adias.mytowereasy.utils.search.SearchCriteriaQM)
     */
    @Override
    public List<EbMarchandise> listMarchandise(SearchCriteriaQM criteria) {
        // TODO Auto-generated method stub
        return daoQualityManagement.listMarchandise(criteria);
    }

    /**
     * @see com.adias.mytowereasy.service.QualityManagementService#getSemiAutoMail(com.adias.mytowereasy.utils.search.SearchCriteriaQM)
     *          methode retournant le template parsé pour affichage dans la
     *          popup des email semi auto sur
     *          le detail d'un incident
     * @param SearchCriteriaQM
     * @return SemiAutoMail
     */
    @Override
    public SemiAutoMail getSemiAutoMail(SearchCriteriaQM criteria) {
        SemiAutoMail mailSemiAuto = new SemiAutoMail();
        Context context = new Context();
        EbQmIncident incident = daoQualityManagement.getIncident(criteria);

        if (criteria != null &&
            criteria.getSemiAutoMailNum().equals(Enumeration.MailSemiAuto.QM_INTENT_TO_CLAIM.getCode())) {
            context.setVariable("incidentNum", incident.getEbQmIncidentNum());
        }

        if (criteria.getSemiAutoMailNum().equals(Enumeration.MailSemiAuto.QM_INCIDENT_QUALIFICATION_CLAIM.getCode())) {
            context.setVariable("incidentNum", incident.getEbQmIncidentNum());
        }

        if (criteria
            .getSemiAutoMailNum().equals(Enumeration.MailSemiAuto.QM_INCIDENT_QUALIFICATION_STATEMENT.getCode())) {
            context.setVariable("incidentNum", incident.getEbQmIncidentNum());
        }

        if (criteria.getSemiAutoMailNum().equals(Enumeration.MailSemiAuto.QM_FINANCE_REMINDER.getCode())) {
            context.setVariable("incidentNum", incident.getEbQmIncidentNum());
        }

        if (criteria.getSemiAutoMailNum().equals(Enumeration.MailSemiAuto.QM_CONFIRMATION_PAYMENT.getCode())) {
            context.setVariable("incidentNum", incident.getEbQmIncidentNum());
        }

        if (criteria.getSemiAutoMailNum().equals(Enumeration.MailSemiAuto.QM_QUALIFICATION_MODIF_TO_CLAIM.getCode())) {
            context.setVariable("incidentNum", incident.getEbQmIncidentNum());
        }

        if (criteria
            .getSemiAutoMailNum().equals(Enumeration.MailSemiAuto.QM_QUALIFICATION_MODIF_TO_STATEMENT.getCode())) {
            context.setVariable("incidentNum", incident.getEbQmIncidentNum());
        }

        String body = templateEngine
            .process(Enumeration.MailSemiAuto.auto(criteria.getSemiAutoMailNum()).getTemplate(), context);
        mailSemiAuto.setText(body);
        mailSemiAuto
            .setSubject(
                Enumeration.MailSemiAuto.auto(criteria.getSemiAutoMailNum()).getSubject() +
                    ":" + incident.getIncidentRef());

        return mailSemiAuto;
    }

    @Override
    public EbEmailHistorique sendMail(Email mailToSend, Integer incidentNum) {
        List<String> c = new ArrayList<>();

        if (mailToSend.getEnCopieCachee() != null && !mailToSend.getEnCopieCachee().isEmpty()) {
            String cc = mailToSend.getEnCopieCachee().get(0);
            c.addAll(Arrays.asList(cc.split(";")));
            mailToSend.setEnCopieCachee(c);
            mailToSend.setEnCopie(c);
        }

        emailHtmlSender.sendSemiAutoMail(mailToSend);
        EbEmailHistorique emailHistorique = new EbEmailHistorique();
        emailHistorique.setTto(mailToSend.getTo());
        emailHistorique.setText(mailToSend.getText());
        emailHistorique.setSubject(mailToSend.getSubject());
        Date actuelle = new Date();
        emailHistorique.setEbIncidentNum(incidentNum);

        emailHistorique.setDateEnvoi(actuelle);
        EbEmail ebEmail = new EbEmail();
        emailHistorique.setTypeEmail(Enumeration.EmailType.getByCode(mailToSend.getMailType()));
        // emailHistorique.setTypeEmail(Enumeration.EmailType.getByCode(40));

        if (mailToSend.getEnCopie() != null) emailHistorique.setEnCopie(String.join(";", mailToSend.getEnCopie()));
        if (mailToSend.getEnCopieCachee()
            != null) emailHistorique.setEnCopieCachee(String.join(";", mailToSend.getEnCopieCachee()));
        // emailHistorique.setEbIncidentNum(incident.getEbQmIncidentNum());

        ebEmailHistoriqueRepository.save(emailHistorique);

        EbQmIncident incidentConcerned = ebIncidentRepository.findById(incidentNum).get();

        if (incidentConcerned != null) {

            if (emailHistorique.getSubject().contains(Enumeration.MailSemiAuto.QM_INTENT_TO_CLAIM.getSubject())) {
                incidentConcerned.setIncidentStatus(Enumeration.IncidentStatus.INTENT_TO_CLAIM.getCode());
            }

            if (emailHistorique
                .getSubject().contains(Enumeration.MailSemiAuto.QM_QUALIFICATION_MODIF_TO_CLAIM.getSubject())) {
                incidentConcerned.setIncidentStatus(Enumeration.IncidentStatus.CLAIM.getCode());
            }

            if (emailHistorique.getSubject().contains(Enumeration.MailSemiAuto.QM_FINANCE_REMINDER.getSubject())) {
                incidentConcerned.setIncidentStatus(Enumeration.IncidentStatus.RECOVERED.getCode());
            }

            if (emailHistorique.getSubject().contains(Enumeration.MailSemiAuto.QM_CONFIRMATION_PAYMENT.getSubject())) {
                incidentConcerned.setIncidentStatus(Enumeration.IncidentStatus.CLOSED.getCode());
            }

            ebIncidentRepository
                .updateIncidentStatus(incidentConcerned.getEbQmIncidentNum(), incidentConcerned.getIncidentStatus());
        }

        return emailHistorique;
    }

    @Override
    public List<EbQmIncidentLine> updateListIncidentLine(List<EbQmIncidentLine> listIncidentLine) {
        // TODO Auto-generated method stub

        for (EbQmIncidentLine il: listIncidentLine) {
            EbEtablissement etab = ebEtablissementRepository.getOne(il.getCarrier().getEbEtablissementNum());
            EbQmIncident incident = ebIncidentRepository.getOne(il.getIncident().getEbQmIncidentNum());

            il.setCarrier(etab);
            il.setIncident(incident);
            ebQmIncidentLineRepository.save(il);
        }

        return listIncidentLine;
    }

    @Override
    public int updateIncidentComment(Integer ebQmIncidentNum, String etablissementComment, String thirdPartyComment) {
        // TODO Auto-generated method stub
        return ebQmIncidentRepository.updateIncidentComment(ebQmIncidentNum, etablissementComment, thirdPartyComment);
    }

    @Override
    public int updateIncidentDescription(Integer ebQmIncidentNum, String incidentDesc, String addImpact) {
        // TODO Auto-generated method stub
        this
            .historizeQmAction(
                ebQmIncidentNum,
                Enumeration.IDChatComponent.QUALITY_MANAGEMENT.getCode(),
                Enumeration.Module.QUALITY_MANAGEMENT.getCode(),
                "description mis a jour",
                connectedUserService.getCurrentUser().getNomPrenom(),
                null);
        return ebQmIncidentRepository.updateIncidentDescription(ebQmIncidentNum, incidentDesc, addImpact);
    }

    @Override
    public int updateIncidentShipment(EbQmIncident incident) {
        return ebQmIncidentRepository
            .updateIncidentShipment(
                incident.getEbQmIncidentNum(),
                incident.getModeTransport(),
                incident.getOriginCountryLibelle(),
                incident.getInsurance(),
                incident.getGoodValues());
    }

    @Override
    public int updateIncident(EbQmIncident incident) {
        EbDemande demande = ebDemandeRepository.findById(incident.getxEbDemande()).get();
        EbEtablissement etablissement = ebEtablissementRepository
            .findById(demande.getxEbEtablissement().getEbEtablissementNum()).get();
        // Delete Old incident lines
        eQmIncidentLineRepository.deleteIncidentLine(incident.getEbQmIncidentNum());

        // Getting incident after delete
        SearchCriteriaQM criteria = new SearchCriteriaQM();
        criteria.setEbQmIncidentNum(incident.getEbQmIncidentNum());
        EbQmIncident actualIncident = getEbIncident(criteria);

        // Setting other changes
        actualIncident.setCategoryLabel(incident.getCategoryLabel());
        actualIncident.setRootCauses(incident.getRootCauses());
        actualIncident.setIncidentDesc(incident.getIncidentDesc());
        actualIncident.setAddImpact(incident.getAddImpact());
        actualIncident.setDaysDelay(incident.getDaysDelay());
        actualIncident.setHoursDelay(incident.getHoursDelay());
        actualIncident.setUnitDamaged(incident.getUnitDamaged());
        actualIncident.setUnitLost(incident.getUnitLost());
        actualIncident.setUnitDamagedAmount(incident.getUnitDamagedAmount());
        actualIncident.setAmondOfunitLost(incident.getAmondOfunitLost());
        actualIncident.setCurrencyLabel(incident.getCurrencyLabel());
        actualIncident.setClaimAmount(incident.getClaimAmount());
        actualIncident.setRecovered(incident.getRecovered());
        actualIncident.setBalance(incident.getBalance());
        actualIncident.setIncidentLines(incident.getIncidentLines());
        actualIncident.getIncidentLines().forEach(e -> {
            e.setIncident(actualIncident);
            e.setCarrier(etablissement);
            e.setCarrierName(etablissement.getNom());
        });

        ebQmIncidentRepository.save(actualIncident);
        return 1;
    }

    @Override
    public void historizeQmAction(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        String username,
        String usernameFor) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        EbQmChat chat = new EbQmChat();
        chat.setUserName(connectedUser.getNomPrenom());
        chat.setDateCreation(new Date());
        chat.setModule(module);
        chat.setIdFiche(idFiche);
        chat.setIsHistory(true);
        chat.setIdChatComponent(chatCompId);
        chat.setText(action + " by <b>" + username + "</b>");
        if (usernameFor != null) chat.setText(chat.getText() + " for <b>" + usernameFor + "</b>");
        ebChatRepository.save(chat);
    }

    @Override
    public List<EbQmIncident> getListIncidentRef(String term) throws JsonProcessingException {
        SearchCriteriaQM criterias = new SearchCriteriaQM();
        List<EbQmIncident> listEbQmIncident = new ArrayList<EbQmIncident>();
        listEbQmIncident = daoQualityManagement.getIncidentRefByListIncident(criterias);
        return listEbQmIncident;
    }

    @Override
    public Integer getNextEbQmIncidentNum() {
        Integer nextval = daoQualityManagement.nextValQuality();
        return nextval;
    }

    @Override
    public EbQmIncident getListDoc(Integer incidentNum) {
        // TODO Auto-generated method stub
        return ebQmIncidentRepository.selectListDoc(incidentNum);
    }

    @Override
    public EbQmIncident saveFlags(Integer ebQmIncidentNum, String newFlags) {
        EbQmIncident incident = ebQmIncidentRepository.findById(ebQmIncidentNum).get();
        incident.setListFlag(newFlags);
        return ebQmIncidentRepository.save(incident);
    }

    @Override
    public EbQmRootCause saveRootCause(EbQmRootCause rootCause) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        String codeCompany = connectedUser.getEbCompagnie().getCode();
        String refRootCause = null;

        if (rootCause.getQmRootCauseNum() == null) {
            Integer num = daoQualityManagement.nextValEbQMRootCause();
            rootCause.setQmRootCauseNum(num);
        }

        // Generate ref root cause from connected user
        refRootCause = StringUtils.leftPad(rootCause.getQmRootCauseNum().toString().toUpperCase(), 5, "0");
        rootCause.setRef(codeCompany.concat("-").concat("RC").concat(refRootCause));
        return ebQmRootCauseRepository.save(rootCause);
    }

    @Override
    public Long countListMarchandises(SearchCriteriaQM criterias) {
        return this.daoQualityManagement.countlistMarchandise(criterias);
    }

    @Override
    public int updateEventTypeStatus(Integer ebQmIncidentNum, Integer incidentStatus) {
        int updated = ebQmIncidentRepository.updateIncidentStatus(ebQmIncidentNum, incidentStatus);
        LOGGER
            .debug(
                " Mise  à jour du statut de l'incident ou de l'inconformite {}.  le nouveau statut = {} ",
                ebQmIncidentNum,
                incidentStatus);
        return updated;
    }

    @Override
    public List<String> getListFlagForCarrier(Integer ebUserNum) {
        return ebQmIncidentRepository.selectListFlagForCarrier(ebUserNum);
    }

    @Override
    public List<String> getListFlagForControlTower(Integer ebUserNum) {
        return ebQmIncidentRepository.selectListFlagForControlTower(ebUserNum);
    }
}
