package com.adias.mytowereasy.service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dto.NotificationTypeCount;
import com.adias.mytowereasy.enumeration.TypeNotificationEnum;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbNotificationRepository;
import com.adias.mytowereasy.repository.EbUserNotificationRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.socket.controller.WebSocketController;
import com.adias.mytowereasy.util.email.Email;


@Service
public class NotificationService {
    static final Integer SIZE = 10;

    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    EbNotificationRepository ebNotificationRepository;

    @Autowired
    EbUserNotificationRepository ebUserNotificationRepository;

    @Autowired
    EbUserRepository ebUserRepository;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    WebSocketController webSocketController;

    private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

    public List<EbNotification>
        getAllNotification(Integer userNum, Integer typeNotification, Integer pageNumber) {
        pageNumber = (pageNumber - 1) * SIZE;
        LocalDateTime nowMinus6Month = LocalDateTime.now().minusMonths(6);
        return ebNotificationRepository
            .findByTypeAndUser(userNum, typeNotification, Timestamp.valueOf(nowMinus6Month), pageNumber, SIZE);
    }

    public NotificationTypeCount count(Integer userNum) {
        NotificationTypeCount res = new NotificationTypeCount();
        EbUserNotification ebUserNotification = this.ebUserNotificationRepository.findByEbUser_ebUserNum(userNum);

        if (ebUserNotification != null) {
            res.setNbrAlerts(ebUserNotification.getNbrAlerts() != null ? ebUserNotification.getNbrAlerts() : 0);
            res.setNbrNotifys(ebUserNotification.getNbrNotifys() != null ? ebUserNotification.getNbrNotifys() : 0);
            res.setNbrMsgs(ebUserNotification.getNbrMsgs() != null ? ebUserNotification.getNbrMsgs() : 0);
        }

        return res;
    }

    private List<EbNotification> getNotificationsByTypeFromList(List<EbNotification> notifications, Integer type) {
        return notifications
            .stream().filter(notif -> notif.getType() != null && notif.getType().equals(type))
            .collect(Collectors.toList());
    }

    public void addNotification(EbNotification notification) {
        ebNotificationRepository.save(notification);
    }

    public EbNotification
        generateNotificationForUser(EbDemande demande, Integer ebUserNum, Integer module, boolean isAffectation) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        EbUser user = this.ebUserRepository.getOne(ebUserNum);
        String header = null;
        EbNotification notif = new EbNotification();
        notif.setModule(module);
        notif.setNumEntity(demande.getEbDemandeNum());

        if (isAffectation) {
            header = "(" + demande.getRefTransport() + ")"
                + (connectedUser.getLanguage() != null && connectedUser.getLanguage().equals("fr") ?
                    "Affectation" :
                    "Assignement");
        }
        else {
            header = "(" + demande.getRefTransport() + ")"
                + (connectedUser.getLanguage() != null && connectedUser.getLanguage().equals("fr") ?
                    "Affectation annulée" :
                    "Assignement canceled");
        }

        notif.setHeader(header);
        notif.setEbUser(new EbUser(user.getEbUserNum()));
        notif.setEbCompagnie(new EbCompagnie(user.getEbCompagnie().getEbCompagnieNum()));
        notif.setType(TypeNotificationEnum.ALERT.getCode());
        notif.setBody(StringUtils.EMPTY);

        ebNotificationRepository.save(notif);
        return notif;
    }

    public void generateAlert(Email email, List<Integer> usersIds, Integer numEntity, Integer module) {
        if (usersIds == null || usersIds.isEmpty()) return;

        List<EbUser> usersList = connectedUserService.getAllUsersByIds(usersIds);

        for (EbUser user: usersList) {
            String lang = user.getLangageUser() != null ? user.getLangageUser() : "en";
            Locale lcl = new Locale(lang);
            EbNotification notification = new EbNotification();

            if (email.getSubject() != null && email.getArguments() != null) {
                String subjectMessage = messageSource.getMessage(email.getSubject(), email.getArguments(), lcl);
                notification.setHeader(subjectMessage);
                notification.setBody(subjectMessage.replace("null", ""));
            }

            notification.setType(TypeNotificationEnum.ALERT.getCode());
            notification.setEbUser(new EbUser(user.getEbUserNum()));
            notification.setNumEntity(numEntity);
            notification.setModule(module);
            this.addNotification(notification);
            this.updateNotificationNumberForUser(user.getEbUserNum(), TypeNotificationEnum.ALERT.getCode());
        }

    }

    public void updateNotificationNumberForUser(Integer ebUserNum, Integer notifType) {
        EbUserNotification userNotif = this.ebUserNotificationRepository.findByEbUser_ebUserNum(ebUserNum);

        if (notifType.equals(TypeNotificationEnum.ALERT.getCode())) {

            if (userNotif != null) {
                this.ebUserNotificationRepository.incNbrAlerts(ebUserNum);
            }
            else {
                userNotif = new EbUserNotification();
                userNotif.setEbUser(new EbUser(ebUserNum));
                userNotif.setNbrAlerts(1);
                userNotif.setNbrMsgs(0);
                userNotif.setNbrNotifys(0);
                this.ebUserNotificationRepository.save(userNotif);
            }

        }
        else if (notifType.equals(TypeNotificationEnum.NOTIFICATION.getCode())) {

            if (userNotif != null) this.ebUserNotificationRepository.incNbrNotifys(ebUserNum);
            else {
                userNotif = new EbUserNotification();
                userNotif.setEbUser(new EbUser(ebUserNum));
                userNotif.setNbrNotifys(1);
                userNotif.setNbrAlerts(0);
                userNotif.setNbrMsgs(0);
                this.ebUserNotificationRepository.save(userNotif);
            }

        }
        else if (notifType.equals(TypeNotificationEnum.MESSAGE.getCode())) {

            if (userNotif != null) {
                this.ebUserNotificationRepository.incNbrMsgs(ebUserNum);
            }
            else {
                userNotif = new EbUserNotification();
                userNotif.setEbUser(new EbUser(ebUserNum));
                userNotif.setNbrMsgs(1);
                userNotif.setNbrNotifys(0);
                userNotif.setNbrAlerts(0);
                this.ebUserNotificationRepository.save(userNotif);
            }

        }

        this.sendToUpdateNotifs();

    }

    public EbNotification generateMessageForUser(EbDemande ebDemande, EbChat chat, Integer module, Integer ebUserNum) {
        EbUser user = this.ebUserRepository.getOne(ebUserNum);
        EbUser connectedUser = connectedUserService.getCurrentUser();

        EbNotification notification = new EbNotification();
        notification.setModule(module);

        if (module == Enumeration.Module.TRACK.getCode()) notification.setNumEntity(chat.getIdFiche());
        else notification.setNumEntity(ebDemande.getEbDemandeNum());

        String header = null;

        if (connectedUser.getLanguage() != null) {
            header = connectedUser.getLanguage().equals("fr") ? "Message de" : "Message from";
        }

        header += " " + chat.getUserName();
        notification.setHeader(header);

        String body = null;
        body = ebDemande.getRefTransport() + " : " + chat.getText();
        notification.setBody(body);

        notification.setEbUser(new EbUser(user.getEbUserNum()));
        notification.setEbCompagnie(new EbCompagnie(user.getEbCompagnie().getEbCompagnieNum()));
        notification.setType(TypeNotificationEnum.MESSAGE.getCode());

        return notification;
    }

    public void generateMessages(EbDemande ebDemande, EbChat chat, Integer module, List<Integer> usersIds) {
        if (usersIds == null || usersIds.isEmpty()) return;

        List<EbNotification> allNotifs = new ArrayList<>();

        for (Integer userId: usersIds) {
            EbNotification notif = this.generateMessageForUser(ebDemande, chat, module, userId);
            allNotifs.add(notif);
        }

        this.ebNotificationRepository.saveAll(allNotifs);

        for (Integer userId: usersIds) this
            .updateNotificationNumberForUser(userId, TypeNotificationEnum.MESSAGE.getCode());
    }

    public void deleteNotification(Long ebNotificationNum) {
        ebNotificationRepository.deleteById(ebNotificationNum);
    }

    public void updateNotifNbrForUser(Integer ebUserNum, Integer notifType, Integer nbr) {
        if (notifType.equals(TypeNotificationEnum.ALERT.getCode())) this.ebUserNotificationRepository
            .updateNbrAlerts(ebUserNum, nbr);
        else if (notifType.equals(TypeNotificationEnum.NOTIFICATION.getCode())) this.ebUserNotificationRepository
            .updateNbrNotifys(ebUserNum, nbr);
        else if (notifType.equals(TypeNotificationEnum.MESSAGE.getCode())) this.ebUserNotificationRepository
            .updateNbrMsgs(ebUserNum, nbr);
    }

    public void sendToUpdateNotifs() {

        try {
            this.webSocketController.updateNotifications();
        } catch (Exception e) {
            logger.error("Error during update notifications", e);
        }

    }
}
