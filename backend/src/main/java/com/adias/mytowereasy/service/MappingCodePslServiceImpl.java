package com.adias.mytowereasy.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.cronjob.tt.model.EdiMapPsl;
import com.adias.mytowereasy.dao.DaoMappingCodePsl;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.Role;
import com.adias.mytowereasy.model.Enumeration.ServiceType;
import com.adias.mytowereasy.utils.search.SearchCriteriaMappingCodePsl;


@Service
public class MappingCodePslServiceImpl extends MyTowerService implements MappingCodePslService {
    @Autowired
    private DaoMappingCodePsl daoMappingCodePsl;

    @Autowired
    ConnectedUserService connectedUserService;

    @Override
    public List<EdiMapPsl> getListMappingCodePsl(SearchCriteriaMappingCodePsl criteria) {
        EbUser currentUser = connectedUserService.getCurrentUser();

        if (currentUser.getRole().equals(Role.ROLE_CHARGEUR.getCode())) {
            criteria.setxEbCompany(currentUser.getEbCompagnie());
        }

        if (currentUser.getRole().equals(Role.ROLE_PRESTATAIRE.getCode())
            && currentUser.getService().equals(ServiceType.SERVICE_TRANSPORTEUR.getCode())) {
            criteria.setxEbCompanyTransporteur(currentUser.getEbCompagnie());
        }

        return daoMappingCodePsl.getListMappingCodePsl(criteria);
    }

    @Override
    public Long getListMappingCodePslCount(SearchCriteriaMappingCodePsl criteria) {
        return daoMappingCodePsl.getListMappingCodePslCount(criteria);
    }

    @Override
    public EdiMapPsl updateMappingCodePsl(EdiMapPsl ediMapPsl) {
        return ediMapPslRepository.save(ediMapPsl);
    }

    @Override
    public Boolean deleteMappingCodePsl(Integer Id) {
        EdiMapPsl ediMapPsl = ediMapPslRepository.getOne(Id);
        ediMapPslRepository.delete(ediMapPsl);
        return true;
    }

    @Override
    public String getAutoCompleteListMappingCodePsl(String term, String field) throws JsonProcessingException {
        List<EdiMapPsl> listMappingCodePsl = daoMappingCodePsl.getAutoCompleteList(term, field);
        return serializationJson(listMappingCodePsl);
    }
}
