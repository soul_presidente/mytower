package com.adias.mytowereasy.service;

import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dto.EbTypeDocumentsDTO;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.FileType;
import com.adias.mytowereasy.model.enums.Modules;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class TypeDocumentServiceImpl extends MyTowerService implements TypeDocumentService {
    @Override
    public List<EbTypeDocuments> getTypeDocsByEbTypeDocumentsModule(Integer moduleNum, EbUser user) throws Exception {
        List<EbTypeDocuments> docs = new ArrayList<EbTypeDocuments>();

        try {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setModule(moduleNum);
            docs = daoTypeDocuments.findByModule(criteria, user);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
        }

        return docs;
    }

    /*
     * cette methode permet de recupere une list de type document selon le
     * module specifie et numEntity ( TM OR TT OR PRICING) et meme d'afficher
     * les new
     * enregistrement sur setting " type document " selon le module selectionner
     * en resumer cette methode return un list qui contient les type de document
     * enregister sur le champs ListTypeDoc
     * et les nouveau enregistrement de module
     * specifier
     */
    @Override
    public List<EbTypeDocuments>
        getListTypeDocumentFinal(Integer moduleNum, Integer ebDemandeNum, Integer ebCompagnieNum) throws Exception {
        List<EbTypeDocuments> typeDocumentSetting = new ArrayList<EbTypeDocuments>();
        List<EbTypeDocuments> typeDocumentDemande = new ArrayList<EbTypeDocuments>();
        List<EbTypeDocuments> docsDemandefinal = new ArrayList<EbTypeDocuments>();

        EbUser connectedUser = connectedUserService.getCurrentUser();

        EbDemande demande = ebDemandeRepository.selectListTypeDocumentsByEbDemandeNum(ebDemandeNum);

        SearchCriteria criteria = new SearchCriteria();
        criteria.setModule(moduleNum);

        criteria.setEbDemandeNum(ebDemandeNum);

        criteria.setEbCompagnieNum(ebCompagnieNum);

        typeDocumentSetting = daoTypeDocuments.findByModule(criteria, connectedUser);

        if (demande != null) {
            typeDocumentDemande = demande.getListTypeDocuments();
        }

        docsDemandefinal = this.getFinalTypeDocs(moduleNum, typeDocumentDemande, typeDocumentSetting);

        return docsDemandefinal;
    }

    private List<EbTypeDocuments> getFinalTypeDocs(
        Integer module,
        List<EbTypeDocuments> typeDocumentEntity,
        List<EbTypeDocuments> typeDocumentSetting) {
        List<EbTypeDocuments> docsDemandefinal = new ArrayList<EbTypeDocuments>();

        if (typeDocumentEntity != null && !typeDocumentEntity.isEmpty()) {
            typeDocumentEntity = typeDocumentEntity.stream().filter(d -> {
                boolean contains = d.getModule() != null ? d.getModule().contains(module) : false;

                return contains;
            }).collect(Collectors.toList());

            docsDemandefinal
                .addAll(typeDocumentEntity.stream().filter(d -> d.getActivated()).collect(Collectors.toList()));
        }

        for (EbTypeDocuments document: typeDocumentSetting) {
            boolean exists = false;

            if (typeDocumentEntity != null) {

                for (EbTypeDocuments d: typeDocumentEntity) {

                    if (d.getEbTypeDocumentsNum().equals(document.getEbTypeDocumentsNum())) {
                        exists = true;
                    }

                }

            }

            if (!exists) {
                docsDemandefinal.add(document);
            }

        }

        docsDemandefinal = docsDemandefinal
            .stream()
            .sorted(Comparator.comparing(EbTypeDocuments::getOrdre, Comparator.nullsLast(Comparator.naturalOrder())))
            .collect(Collectors.toList());

        return docsDemandefinal; // return liste qui contient les docs de module
                                 // actual et meme les nouveaux enregitrement de
                                 // ce module
    }

    @Override
    public List<EbTypeDocuments>
        getInvoiceListTypeDocument(Integer module, Integer ebCompagnieNum, Integer ebInvoiceNum) throws Exception {
        List<EbTypeDocuments> typeDocumentSetting = new ArrayList<EbTypeDocuments>();
        List<EbTypeDocuments> typeDocumentInvoice = new ArrayList<EbTypeDocuments>();

        EbUser connectedUser = connectedUserService.getCurrentUser();

        EbInvoice invoice = ebInvoiceRepository.getListTypeDocumentsByEbInvoiceNum(ebInvoiceNum);

        if (invoice != null) {
            typeDocumentInvoice = invoice.getListTypeDocuments();
        }

        SearchCriteria criteria = new SearchCriteria();
        criteria.setModule(module);

        criteria.setEbCompagnieNum(ebCompagnieNum);

        typeDocumentSetting = daoTypeDocuments.findByModule(criteria, connectedUser);

        List<EbTypeDocuments> docsDemandefinal = this
            .getFinalTypeDocs(module, typeDocumentInvoice, typeDocumentSetting);

        return docsDemandefinal;
    }

    @Override
    public EbTypeDocuments saveTypeDocument(EbTypeDocuments ebTypeDocuments) {
        String moduleStr = "";
        EbUser connectedUser = connectedUserService.getCurrentUser();

        EbCompagnie compagnie = connectedUser != null ? connectedUser.getEbCompagnie() : null;

        EbTypeDocuments entity = new EbTypeDocuments();

        if (ebTypeDocuments.getEbTypeDocumentsNum() != null) {
            entity = ebTypeDocumentsRepository
                .findById(ebTypeDocuments.getEbTypeDocumentsNum()).orElse(new EbTypeDocuments());
        }

        if (ebTypeDocuments.getModule() != null && !ebTypeDocuments.getModule().isEmpty()) {

            for (Integer em: ebTypeDocuments.getModule()) {
                moduleStr += em.toString().concat("|");
            }

        }

        entity.setCode(ebTypeDocuments.getCode());
        entity.setNom(ebTypeDocuments.getNom());
        entity.setOrdre(ebTypeDocuments.getOrdre());
        entity.setNbrDoc(new Integer(0));
        entity.setActivated(ebTypeDocuments.getActivated());
        entity.setIsexpectedDoc(ebTypeDocuments.getIsexpectedDoc());

        entity.setxEbCompagnie(compagnie != null ? compagnie.getEbCompagnieNum() : null);
        entity.setListModuleStr(moduleStr);
        entity.setModule(ebTypeDocuments.getModule());

        return ebTypeDocumentsRepository.save(entity);
    }

    @Override
    public EbTypeDocuments updateTypeDocument(EbTypeDocuments ebTypeDocuments) {
        EbTypeDocuments doc = ebTypeDocumentsRepository
            .update(
                ebTypeDocuments.getEbTypeDocumentsNum(),
                ebTypeDocuments.getNom(),
                ebTypeDocuments.getCode(),
                ebTypeDocuments.getOrdre(),
                ebTypeDocuments.getActivated(),
                ebTypeDocuments.getIsexpectedDoc());

        return doc;
    }

    @Override
    public void updateActivated(SearchCriteria criteria) {
        ebTypeDocumentsRepository.updateActivated(criteria.getTypeDocumentNum(), criteria.getActivated());
    }

    @Override
    public void deleteTypeDocument(Integer ebTypeDocuments) {
        ebTypeDocumentsRepository.deleteById(ebTypeDocuments);
    }

    @Override
    public List<EbTypeDocuments> getListTypeDoc(SearchCriteria criteria) throws Exception {
        List<EbTypeDocuments> docs = new ArrayList<EbTypeDocuments>();
        docs = daoTypeDocuments.getListEbTypeDocument(criteria);

        return docs;
    }

    @Override
    public List<EbTypeDocumentsDTO> convertEntitiesToDto(List<EbTypeDocuments> entities) {
        List<EbTypeDocumentsDTO> listDto = new ArrayList<>();
        entities.forEach(e -> listDto.add(new EbTypeDocumentsDTO(e)));
        return listDto;
    }

    @Override
    public void generateTypeOfDocumentByCompanyNum(List<Integer> listEbCompagnieNum) throws Exception {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        Timestamp dt = new Timestamp(new Date().getTime());

        List<EbTypeDocuments> listExistingTypeDocuments = ebTypeDocumentsRepository
            .findByListEbCompagnieNum(listEbCompagnieNum);

        List<EbTypeDocuments> listStandardEbTypeDocuments = new ArrayList<EbTypeDocuments>();
        listStandardEbTypeDocuments
            .add(
                new EbTypeDocuments(
                    FileType.INTERCO_INVOICE.getCammelLibelle(),
                    FileType.INTERCO_INVOICE.getCode() + "",
                    1,
                    Arrays.asList(Modules.PRICING.getValue(), Modules.TRANSPORT_MANAGEMENT.getValue()),
                    Modules.PRICING.getValue() + "|" + Modules.TRANSPORT_MANAGEMENT.getValue(),
                    new EbUser(connectedUser.getEbUserNum()),
                    null,
                    true,
                    true,
                    0));
        listStandardEbTypeDocuments
            .add(
                new EbTypeDocuments(
                    FileType.COMMERCIAL_INVOICE.getCammelLibelle(),
                    FileType.COMMERCIAL_INVOICE.getCode() + "",
                    2,
                    Arrays.asList(Modules.PRICING.getValue(), Modules.TRANSPORT_MANAGEMENT.getValue()),
                    Modules.PRICING.getValue() + "|" + Modules.TRANSPORT_MANAGEMENT.getValue(),
                    new EbUser(connectedUser.getEbUserNum()),
                    null,
                    true,
                    true,
                    0));
        listStandardEbTypeDocuments
            .add(
                new EbTypeDocuments(
                    FileType.CUSTOMS.getCammelLibelle(),
                    FileType.CUSTOMS.getCode() + "",
                    3,
                    Arrays.asList(Modules.PRICING.getValue(), Modules.TRANSPORT_MANAGEMENT.getValue()),
                    Modules.PRICING.getValue() + "|" + Modules.TRANSPORT_MANAGEMENT.getValue(),
                    new EbUser(connectedUser.getEbUserNum()),
                    null,
                    true,
                    true,
                    0));
        listStandardEbTypeDocuments
            .add(
                new EbTypeDocuments(
                    FileType.TRANSPORT_DOCUMENT.getCammelLibelle(),
                    FileType.TRANSPORT_DOCUMENT.getCode() + "",
                    4,
                    Arrays.asList(Modules.PRICING.getValue(), Modules.TRANSPORT_MANAGEMENT.getValue()),
                    Modules.PRICING.getValue() + "|" + Modules.TRANSPORT_MANAGEMENT.getValue(),
                    new EbUser(connectedUser.getEbUserNum()),
                    null,
                    true,
                    true,
                    0));
        listStandardEbTypeDocuments
            .add(
                new EbTypeDocuments(
                    FileType.OTHER.getCammelLibelle(),
                    FileType.OTHER.getCode() + "",
                    5,
                    Arrays.asList(Modules.PRICING.getValue(), Modules.TRANSPORT_MANAGEMENT.getValue()),
                    Modules.PRICING.getValue() + "|" + Modules.TRANSPORT_MANAGEMENT.getValue() + "|"
                        + Modules.TRACK.getValue(),
                    new EbUser(connectedUser.getEbUserNum()),
                    null,
                    true,
                    true,
                    0));
        listStandardEbTypeDocuments
            .add(
                new EbTypeDocuments(
                    FileType.POD.getCammelLibelle(),
                    FileType.POD.getCode() + "",
                    6,
                    Arrays
                        .asList(
                            Modules.PRICING.getValue(),
                            Modules.TRANSPORT_MANAGEMENT.getValue(),
                            Modules.TRACK.getValue()),
                    Modules.PRICING.getValue() + "|" + Modules.TRANSPORT_MANAGEMENT.getValue() + "|"
                        + Modules.TRACK.getValue(),
                    new EbUser(connectedUser.getEbUserNum()),
                    null,
                    true,
                    true,
                    0));

        for (Integer compagnieNum: listEbCompagnieNum) {
            List<EbTypeDocuments> listEbTypeDocuments = new ArrayList();

            List<EbTypeDocuments> listExistingCompagnieTypeDoc = listExistingTypeDocuments
                .stream().filter(it -> it.getxEbCompagnie().equals(compagnieNum)).collect(Collectors.toList());

            for (EbTypeDocuments stdTypeDocuments: listStandardEbTypeDocuments) {
                Boolean exists = false;

                for (EbTypeDocuments compTypeDocuments: listExistingCompagnieTypeDoc) {

                    if (compTypeDocuments.getCode() != null
                        && stdTypeDocuments.getCode().contentEquals(compTypeDocuments.getCode())) {
                        exists = true;
                    }

                }

                if (!exists) {
                    EbTypeDocuments td = new EbTypeDocuments(stdTypeDocuments);
                    td.setDateCreationSys(dt);
                    td.setxEbCompagnie(compagnieNum);
                    listEbTypeDocuments.add(td);
                }

            }

            if (!listEbTypeDocuments.isEmpty()) ebTypeDocumentsRepository.saveAll(listEbTypeDocuments);

            listEbTypeDocuments.addAll(listExistingCompagnieTypeDoc);

            List<EbDemande> listEbDemande = ebDemandeRepository.findByXEbCompagnie_ebCompagnieNum(compagnieNum);

            for (EbDemande d: listEbDemande) {
                d.setListTypeDocuments(listEbTypeDocuments);
            }

            ebDemandeRepository.saveAll(listEbDemande);
        }

    }

    @Override
    public Long countListTypeDocument(SearchCriteria criteria) {
        return daoTypeDocuments.listCount(criteria);
    }
}
