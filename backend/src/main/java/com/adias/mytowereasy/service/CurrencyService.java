package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.EbCompagnieCurrency;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface CurrencyService {
    public Boolean deleteExEtablissementCurrency(EbCompagnieCurrency etablissementCurrency);

    public List<EbCompagnieCurrency> selectListExEtablissementCurrencyByEtablissementNum(SearchCriteria criteria);

    public List<EbCompagnieCurrency>
        selectListEbCompagnieCurrencyByEbCompagnieAndEbCompagnieGuestNum(SearchCriteria criteria);

    public List<EcCurrency> selectLisCurrencyCompagnieAndStatique(SearchCriteria criteria);

    Long getListEbCompagnieCurrencyCount(SearchCriteria criteria);

    List<EbCompagnieCurrency> getListCompanieCurrencyBySetting(SearchCriteria criteria);

    EcCurrency getCurrencyByCode(String code);

    EbCompagnieCurrency getEbCompagnieCurrency(SearchCriteria criteria);

		EbCompagnieCurrency saveEbCompagnyCurrency(EbCompagnieCurrency ebCompagnieCurrency);

    Boolean updateEbCompanyCurrency(EbCompagnieCurrency ebCompagnieCurrency);

		boolean validCurrency(EbCompagnieCurrency ebCompagnieCurrency);
}
