package com.adias.mytowereasy.service.filedownload;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.model.Enumeration.Role;
import com.adias.mytowereasy.service.CurrencyService;
import com.adias.mytowereasy.service.FreightAuditService;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.category.CategoryService;
import com.adias.mytowereasy.trpl.model.EbPlCostItem;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaFreightAudit;


@Service
public class FiledownloadFreightAuditServiceImp extends MyTowerService implements FiledownloadFreightAuditService {
    @Autowired
    FreightAuditService freightAuditService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    CurrencyService currencyService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MessageSource messageSource;

    @Override
    public FileDownloadStateAndResult listFileDownLoadFreightAudit(
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        List<EbCategorie> listCategorie,
        SearchCriteria searchCriteria,
        Map<String, String> mapCustomField,
        Map<String, List<Integer>> mapCategoryFields)
        throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();
        Map<String, String> listTypeResult = new LinkedHashMap<String, String>();

        List<EbCustomField> fields = new ArrayList<EbCustomField>();

        Map<String, Object> listcatAndCustom = categoryService
            .getListCategoryAndCustomField(searchCriteria, connectedUser);
        SearchCriteriaFreightAudit criteriaFreightAudit = objectMapper
            .convertValue(searchCriteria, SearchCriteriaFreightAudit.class);

        fields = (List<EbCustomField>) listcatAndCustom.get("customField");
        listCategorie = (List<EbCategorie>) listcatAndCustom.get("listCategorie");

        if (!mapCustomField.isEmpty()) searchCriteria.setMapCustomFields(mapCustomField);

        if (!mapCategoryFields.isEmpty()) searchCriteria.setMapCategoryFields(mapCategoryFields);

        List<EbInvoice> listEbInvoice = freightAuditService.getListEbInvoice(criteriaFreightAudit);
        List<EcCurrency> listCurrency = serviceListStatique.getListEcCurrency();

        Locale locale = connectedUser.getLocaleFromLanguage();

        List<EbTypeDocuments> listTypeDocument = null;

        String costItemStr = messageSource.getMessage("pricing_booking.cost_item", null, locale);
        String negotiatedPrice = messageSource.getMessage("freight_audit.negotiated_price", null, locale);
        String costCurrency = messageSource.getMessage("freight_audit.currency", null, locale);
        String negociatedExchangeRate = messageSource
            .getMessage("freight_audit.negotiated_exchange_rate", null, locale);
        String negociatedPriceConverted = messageSource
            .getMessage("freight_audit.negotiated_price_converted", null, locale);
        String invoicedPrice = messageSource.getMessage("freight_audit.invoiced_price", null, locale);
        String costItemExchangeRate = messageSource.getMessage("freight_audit.invoiced_exchange_rate", null, locale);
        String invoicePriceConverted = messageSource.getMessage("freight_audit.invoiced_price_converted", null, locale);
        String costItemGap = messageSource.getMessage("freight_audit.item_gap", null, locale);
        String gapConverted = messageSource.getMessage("freight_audit.gap_converted", null, locale);

        for (EbInvoice ebInvoice: listEbInvoice) {
            List<EbCost> ebCosts = getEbCostsFromInvoice(ebInvoice);
            List<EbPlCostItem> plCostItems = getEbCostsFromInvoiceTP(ebInvoice);

            if (plCostItems != null && !plCostItems.isEmpty()) {

                for (EbPlCostItem costItem: plCostItems) {
                    Map<String, String[]> map = new LinkedHashMap<String, String[]>();
                    listTypeDocument = ebInvoice.getListTypeDocuments();
                    addColumnAndValues(
                        listTypeDocument,
                        connectedUser,
                        dtConfig,
                        listCategorie,
                        fields,
                        listCurrency,
                        ebInvoice,
                        map,
                        listTypeResult);
                    map.put("costItem", new String[]{
                        costItemStr, costItem == null ? "" : costItem.getLibelle()
                    });

                    map.put("negotiatedPrice", new String[]{
                        negotiatedPrice,
                        costItem.getPrice() == null || costItem.getEuroExchangeRate() == null ?
                            "0" :
                            getNegotiatedConvertedPrice(
                                costItem.getPrice(),
                                new BigDecimal(costItem.getEuroExchangeRate())).replace(".", ",")
                    });

                    map.put("costCurrency", new String[]{
                        costCurrency, costItem.getCurrency() == null ? "" : costItem.getCurrency().getLibelle()
                    });

                    map.put("negociatedExchangeRate", new String[]{
                        negociatedExchangeRate,
                        costItem.getEuroExchangeRate() == null ? "1" : formatValue(costItem.getEuroExchangeRate())
                    });

                    map.put("negociatedPriceConverted", new String[]{
                        negociatedPriceConverted, costItem.getPrice() == null ? "0" : formatValue(costItem.getPrice())
                    });

                    map.put("invoicedPrice", new String[]{
                        invoicedPrice, costItem.getPriceReal() == null ? "0" : formatValue(costItem.getPriceReal())
                    });

                    map.put("costItemExchangeRate", new String[]{
                        costItemExchangeRate,
                        costItem.getEuroExchangeRateReal() == null ?
                            "1" :
                            formatValue(costItem.getEuroExchangeRateReal())
                    });

                    map.put("invoicePriceConverted", new String[]{
                        invoicePriceConverted,
                        costItem.getPriceReal() == null || costItem.getEuroExchangeRateReal() == null ?
                            "0" :
                            getConvertedPriceDouble(costItem.getPriceReal(), costItem.getEuroExchangeRateReal())
                                .replace(".", ",")
                    });

                    map.put("costItemGap", new String[]{
                        costItemGap, costItem.getPriceGab() == null ? "0" : formatValue(costItem.getPriceGab())
                    });

                    map.put("gapConverted", new String[]{
                        gapConverted,
                        costItem.getPriceGab() == null || costItem.getEuroExchangeRateReal() == null ?
                            "0" :
                            getConvertedPriceDouble(costItem.getPriceGab(), costItem.getEuroExchangeRateReal())
                                .replace(".", ",")
                    });

                    listresult.add(map);
                }

            }
            else {

                for (EbCost ebCost: ebCosts) {
                    Map<String, String[]> map = new LinkedHashMap<String, String[]>();
                    listTypeDocument = ebInvoice.getListTypeDocuments();
                    addColumnAndValues(
                        listTypeDocument,
                        connectedUser,
                        dtConfig,
                        listCategorie,
                        fields,
                        listCurrency,
                        ebInvoice,
                        map,
                        listTypeResult);
                    map.put("costItem", new String[]{
                        costItemStr, ebCost == null ? "" : ebCost.getLibelle()
                    });

                    map.put("negotiatedPrice", new String[]{
                        negotiatedPrice, ebCost.getPrice() == null ? "0" : formatValue(ebCost.getPrice())
                    });

                    map.put("costCurrency", new String[]{
                        costCurrency,
                        ebCost.getxEcCurrencyNum() == null ?
                            "" :
                            getCurrencyLibelle(listCurrency, ebCost.getxEcCurrencyNum())
                    });

                    map.put("negociatedExchangeRate", new String[]{
                        negociatedExchangeRate,
                        ebCost.getEuroExchangeRate() == null ? "1" : formatValue(ebCost.getEuroExchangeRate())
                    });

                    map.put("negociatedPriceConverted", new String[]{
                        negociatedPriceConverted,
                        ebCost.getPrice() == null || ebCost.getEuroExchangeRate() == null ?
                            "0" :
                            getConvertedPriceBigDecimal(ebCost.getPrice(), ebCost.getEuroExchangeRate())
                                .replace(".", ",")
                    });

                    map.put("invoicedPrice", new String[]{
                        invoicedPrice, ebCost.getPriceReal() == null ? "0" : formatValue(ebCost.getPriceReal())
                    });

                    map.put("costItemExchangeRate", new String[]{
                        costItemExchangeRate,
                        ebCost.getEuroExchangeRateReal() == null ? "1" : formatValue(ebCost.getEuroExchangeRateReal())
                    });

                    map.put("invoicePriceConverted", new String[]{
                        invoicePriceConverted,
                        ebCost.getPriceReal() == null || ebCost.getEuroExchangeRateReal() == null ?
                            "0" :
                            getConvertedPriceDouble(ebCost.getPriceReal(), ebCost.getEuroExchangeRateReal())
                                .replace(".", ",")
                    });

                    map.put("costItemGap", new String[]{
                        costItemGap, ebCost.getPriceGab() == null ? "0" : formatValue(ebCost.getPriceGab())
                    });

                    map.put("gapConverted", new String[]{
                        gapConverted,
                        ebCost.getPriceGab() == null || ebCost.getEuroExchangeRateReal() == null ?
                            "0" :
                            getConvertedPriceDouble(ebCost.getPriceGab(), ebCost.getEuroExchangeRateReal())
                                .replace(".", ",")
                    });

                    listresult.add(map);
                }

            }

        }

        resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
        resultAndState.setListResult(listresult);
        resultAndState.setListTypeResult(listTypeResult);
        return resultAndState;
    }

    public List<EbCost> getEbCostsFromInvoice(EbInvoice ebInvoice) {
        List<EbCost> ebCostList = new ArrayList<>();
        List<EbCostCategorie> listCostCategories = objectMapper
            .convertValue(ebInvoice.getListCostCategorieCharger(), new TypeReference<List<EbCostCategorie>>() {
            });

        if (listCostCategories == null) {
            listCostCategories = objectMapper
                .convertValue(ebInvoice.getListCostCategorieTransporteur(), new TypeReference<List<EbCostCategorie>>() {
                });
        }

        if (listCostCategories != null && !listCostCategories.isEmpty()) listCostCategories.forEach(ebCostCategorie -> {
            ebCostCategorie.getListEbCost().forEach(ebCost -> {
                if (ebCost != null) ebCostList.add(ebCost);
            });
        });
        return ebCostList;
    }

    public List<EbPlCostItem> getEbCostsFromInvoiceTP(EbInvoice ebInvoice) {
        List<EbPlCostItem> plCostItemsList = objectMapper
            .convertValue(ebInvoice.getListPlCostItem(), new TypeReference<List<EbPlCostItem>>() {
            });
        ;
        return plCostItemsList;
    }

    public void addColumnAndValues(
        List<EbTypeDocuments> listTypeDocument,
        EbUser connectedUser,
        List<Map<String, String>> dtConfig,
        List<EbCategorie> listCategorie,
        List<EbCustomField> fields,
        List<EcCurrency> listCurrency,
        EbInvoice ebInvoice,
        Map<String, String[]> map,
        Map<String, String> listTypeResult) {
        Locale locale = connectedUser.getLocaleFromLanguage();

        DateFormat dateFormat = new SimpleDateFormat(connectedUser.getDateFormatFromLocale());

        if (isPropertyInDtConfig(dtConfig, "refDemande")) map.put("refDemande", new String[]{
            messageSource.getMessage("freight_audit.transport_ref", null, locale), ebInvoice.getRefDemande()
        });

        if (isPropertyInDtConfig(dtConfig, "totalWeight")) map.put("totalWeight", new String[]{
            messageSource.getMessage("freight_audit.taxable_weight", null, locale),
            ebInvoice.getTotalWeight() != null ? ebInvoice.getTotalWeight().toString() : null
        });

        if (isPropertyInDtConfig(dtConfig, "xEcModeTransport")) map.put("xEcModeTransport", new String[]{
            messageSource.getMessage("freight_audit.mode_transport", null, locale),
            Enumeration.ModeTransport.getLibelleByCode(ebInvoice.getxEcModeTransport())
        });

        if (isPropertyInDtConfig(dtConfig, "xEcIncotermLibelle")) map.put("xEcIncotermLibelle", new String[]{
            messageSource.getMessage("freight_audit.incoterms", null, locale), ebInvoice.getxEcIncotermLibelle()
        });

        if (isPropertyInDtConfig(dtConfig, "numAwbBol")) map.put("numAwbBol", new String[]{
            messageSource.getMessage("freight_audit.num_awb_bol", null, locale),
            ebInvoice.getxEbDemande() != null ? ebInvoice.getxEbDemande().getNumAwbBol() : null
        });

        if (isPropertyInDtConfig(dtConfig, "libelleOriginCountry")) map.put("libelleOriginCountry", new String[]{
            messageSource.getMessage("freight_audit.origi_of_country", null, locale),
            ebInvoice.getLibelleOriginCountry()
        });

        if (isPropertyInDtConfig(dtConfig, "libelleOriginCity")) map.put("libelleOriginCity", new String[]{
            messageSource.getMessage("freight_audit.orig_city", null, locale), ebInvoice.getLibelleOriginCity()
        });

        if (isPropertyInDtConfig(dtConfig, "libelleDestCountry")) map.put("libelleDestCountry", new String[]{
            messageSource.getMessage("freight_audit.dest_country", null, locale), ebInvoice.getLibelleDestCountry()
        });

        if (isPropertyInDtConfig(dtConfig, "libelleDestCity")) map.put("libelleDestCity", new String[]{
            messageSource.getMessage("freight_audit.dest_city", null, locale), ebInvoice.getLibelleDestCity()
        });

        if (isPropertyInDtConfig(dtConfig, "xEcTypeDemande")) map.put("xEcTypeDemande", new String[]{
            "Type Demande", Enumeration.TypeDemande.getLibelleByCode(ebInvoice.getxEcTypeDemande())
        });

        if (isPropertyInDtConfig(dtConfig, "preCarrierCost")) map.put("preCarrierCost", new String[]{
            messageSource.getMessage("freight_audit.negotiated_price", null, locale),
            ebInvoice.getPreCarrierCost() != null ? ebInvoice.getPreCarrierCost().toString() : null
        });

        if (isPropertyInDtConfig(dtConfig, "datePickup")) {
            map.put("datePickup", new String[]{
                messageSource.getMessage("freight_audit.date_pickup", null, locale),
                ebInvoice.getDatePickup() != null ? dateFormat.format(ebInvoice.getDatePickup()) : null
            });
            listTypeResult.put("datePickup", Date.class.getTypeName());
        }

        if (isPropertyInDtConfig(dtConfig, "dateDelivery")) {
            map.put("dateDelivery", new String[]{
                messageSource.getMessage("freight_audit.date_deliver", null, locale),
                ebInvoice.getDateDelivery() != null ? dateFormat.format(ebInvoice.getDateDelivery()) : null
            });
            listTypeResult.put("dateDelivery", Date.class.getName());
        }

        if (isPropertyInDtConfig(dtConfig, "finalCarrierCost")) map.put("finalCarrierCost", new String[]{
            "Carrier Cost", ebInvoice.getFinalCarrierCost() != null ? ebInvoice.getFinalCarrierCost().toString() : null
        });

        if (isPropertyInDtConfig(dtConfig, "xEbDemande")) map.put("xEbDemande", new String[]{
            "N° AWB/BOL",
            ebInvoice.getxEbDemande().getNumAwbBol() != null ?
                ebInvoice.getxEbDemande().getNumAwbBol().toString() :
                null
        });

        if (isPropertyInDtConfig(dtConfig, "currencyLabel")) map.put("currencyLabel", new String[]{
            messageSource.getMessage("freight_audit.invoice_currency", null, locale),
            ebInvoice.getCurrencyLabel() != null ? ebInvoice.getCurrencyLabel() : null
        });

        if (isPropertyInDtConfig(dtConfig, "listEbFlagDTO")) {
            String listFlagsLibelle = "";

            if (ebInvoice.getListEbFlagDTO() != null && ebInvoice.getListEbFlagDTO().size() > 0) {

                for (EbFlagDTO ebFlagDTO: ebInvoice.getListEbFlagDTO()) {
                    listFlagsLibelle += ebFlagDTO.getName() + ", ";
                }

            }

            if (listFlagsLibelle.length()
                > 0) listFlagsLibelle = listFlagsLibelle.substring(0, listFlagsLibelle.length() - 2);
            map.put("listEbFlagDTO", new String[]{
                messageSource.getMessage("pricing_booking.flag", null, locale), listFlagsLibelle
            });
        }

        if (isPropertyInDtConfig(dtConfig, "exchangeRate")) {
            StringBuffer exchangeRateBuffer = new StringBuffer();
            Set<String> exchangeRateSet = new HashSet<String>();

            ObjectMapper mapper = new ObjectMapper();

            List<EbCostCategorie> listEbCostCategories = new ArrayList<EbCostCategorie>();
            List<EbCostCategorie> listEbCostCategoriesChargeur = new ArrayList<EbCostCategorie>();
            List<EbPlCostItem> listCostItem = new ArrayList<EbPlCostItem>();

            if (ebInvoice != null &&
                ebInvoice.getListCostCategorieTransporteur() != null &&
                !ebInvoice.getListCostCategorieTransporteur().isEmpty()) {
                listEbCostCategories = mapper
                    .convertValue(
                        ebInvoice.getListCostCategorieTransporteur(),
                        new TypeReference<List<EbCostCategorie>>() {
                        });

                if (listEbCostCategories != null && !listEbCostCategories.isEmpty()) {
                    listEbCostCategories
                        .forEach(
                            (transporteur) -> {
                                // if()
                                transporteur.getListEbCost().forEach((cost) -> {

                                    if (cost.getEuroExchangeRate() != null && cost.getxEcCurrencyNum() != null) {
                                        listCurrency.forEach(currency -> {

                                            if (currency.getEcCurrencyNum().equals(cost.getxEcCurrencyNum())) {
                                                exchangeRateSet
                                                    .add(
                                                        currency.getCode() +
                                                            " -> " + ebInvoice.getCurrencyLabel() + " : " +
                                                            cost.getEuroExchangeRate());
                                            }

                                        });
                                    }

                                });
                            });

                    if (exchangeRateSet != null && !exchangeRateSet.isEmpty()) {

                        for (String c: exchangeRateSet) {
                            exchangeRateBuffer.append(c);
                            exchangeRateBuffer.append(System.lineSeparator());
                        }

                    }

                }

                map.put("exchangeRate", new String[]{
                    messageSource.getMessage("pricing_booking.exchange_rate", null, locale),
                    exchangeRateBuffer.toString()
                });
            }

            if (ebInvoice != null &&
                ebInvoice.getListCostCategorieCharger() != null && !ebInvoice.getListCostCategorieCharger().isEmpty()) {
                listEbCostCategoriesChargeur = mapper
                    .convertValue(ebInvoice.getListCostCategorieCharger(), new TypeReference<List<EbCostCategorie>>() {
                    });

                if (listEbCostCategoriesChargeur != null && !listEbCostCategoriesChargeur.isEmpty()) {
                    listEbCostCategoriesChargeur.forEach((chargeur) -> {
                        chargeur.getListEbCost().forEach((cost) -> {

                            if (cost.getEuroExchangeRate() != null && cost.getxEcCurrencyNum() != null) {
                                listCurrency.forEach(currency -> {

                                    if (currency.getEcCurrencyNum().equals(cost.getxEcCurrencyNum())) {
                                        exchangeRateSet
                                            .add(
                                                currency.getCode() +
                                                    " -> " + ebInvoice.getCurrencyLabel() + " : " +
                                                    cost.getEuroExchangeRate());
                                    }

                                });
                            }

                        });
                    });

                    if (exchangeRateSet != null && !exchangeRateSet.isEmpty()) {

                        for (String c: exchangeRateSet) {
                            exchangeRateBuffer.append(c);
                            exchangeRateBuffer.append(System.lineSeparator());
                        }

                    }

                }

                map.put("exchangeRate", new String[]{
                    messageSource.getMessage("pricing_booking.exchange_rate", null, locale),
                    exchangeRateBuffer.toString()
                });
            }

        }

        if (isPropertyInDtConfig(dtConfig, "listTypeDocuments")) {

            if (listTypeDocument != null && listTypeDocument.size() > 0) {
                String codeDoc = "";

                for (EbTypeDocuments cat: listTypeDocument) {
                    codeDoc += cat.getNom();
                    codeDoc += " \r\n";
                }

                map.put("listTypeDocuments", new String[]{
                    messageSource.getMessage("pricing_booking.statut_document", null, locale), codeDoc
                });
            }
            else {
                map.put("listTypeDocuments", new String[]{
                    messageSource.getMessage("pricing_booking.statut_document", null, locale), ""
                });
            }

        }

        String statutChargeur = messageSource.getMessage("freight_audit.statut_chargeur", null, locale);
        String flag = messageSource.getMessage("pricing_booking.flag", null, locale);
        String applyIvoiceNumber = messageSource.getMessage("freight_audit.invoice_number", null, locale);
        String applyIvoicePrice = messageSource.getMessage("freight_audit.invoice_price", null, locale);
        String gap = messageSource.getMessage("freight_audit.gap", null, locale);
        String memo = messageSource.getMessage("freight_audit.memo", null, locale);
        String invoiceDate = messageSource.getMessage("freight_audit.invoice_date", null, locale);
        String nomCompanyCarrier = messageSource.getMessage("freight_audit.carrier_company", null, locale);
        String nomCompanyChargeur = messageSource.getMessage("freight_audit.chargeur_company", null, locale);
        String statutCarrier = messageSource.getMessage("freight_audit.statut_carrier", null, locale);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        if (Role.ROLE_CHARGEUR.getCode().equals(connectedUser.getRole())) {
            if (isPropertyInDtConfig(dtConfig, "statutChargeur")) map.put("statutChargeur", new String[]{
                statutChargeur,
                ebInvoice.getStatutChargeur() != null ?
                    FAEnumeration.InvoiceStatus.getLibelleByCode(ebInvoice.getStatutChargeur()) :
                    FAEnumeration.InvoiceStatus.getLibelleByCode(ebInvoice.getStatut())
            });
            if (isPropertyInDtConfig(dtConfig, "uploadFileNbr")) map.put("uploadFileNbr", new String[]{
                "Documents", ebInvoice.getUploadFileNbr() != null ? ebInvoice.getUploadFileNbr().toString() : null
            });

            if (isPropertyInDtConfig(dtConfig, "flag")) map.put("flag", new String[]{
                flag, FAEnumeration.Flag.getLibelleByCode(ebInvoice.getFlag())
            });

            if (isPropertyInDtConfig(
                dtConfig,
                "applyChargeurIvoiceNumber")) map.put("applyChargeurIvoiceNumber", new String[]{
                    applyIvoiceNumber,
                    ebInvoice.getApplyChargeurIvoiceNumber() != null ?
                        ebInvoice.getApplyChargeurIvoiceNumber().toString() :
                        null
            });

            if (isPropertyInDtConfig(
                dtConfig,
                "applyChargeurIvoicePrice")) map.put("applyChargeurIvoicePrice", new String[]{
                    applyIvoicePrice,
                    ebInvoice.getApplyChargeurIvoicePrice() != null ?
                        ebInvoice.getApplyChargeurIvoicePrice().toString() :
                        null
            });

            if (isPropertyInDtConfig(dtConfig, "gapChargeur")) map.put("gapChargeur", new String[]{
                gap, ebInvoice.getGapChargeur() != null ? ebInvoice.getGapChargeur().toString() : null
            });

            if (isPropertyInDtConfig(dtConfig, "memoChageur")) map.put("memoChageur", new String[]{
                memo, ebInvoice.getMemoChageur()
            });

            if (isPropertyInDtConfig(dtConfig, "invoiceDateChargeur")) {
                map.put("invoiceDateChargeur", new String[]{
                    invoiceDate,
                    ebInvoice.getInvoiceDateChargeur() != null ?
                        dateFormat.format(ebInvoice.getInvoiceDateChargeur()) :
                        dateFormat.format(calendar.getTime())
                });
                listTypeResult.put("invoiceDateChargeur", Date.class.getName());
            }

            /*
             * if (isPropertyInDtConfig(dtConfig, "nomCompanyChargeur"))
             * map.put(
             * "nomCompanyChargeur",
             * new String[] {"nomCompany Chargeur", ebInvoice.getNomEtablissementCarrier()});
             * }
             */
            if (isPropertyInDtConfig(dtConfig, "nomCompanyCarrier")) map.put("nomCompanyCarrier", new String[]{
                nomCompanyCarrier, ebInvoice.getNomCompanyCarrier()
            });
            if (isPropertyInDtConfig(dtConfig, "nomCompanyChargeur")) map.put("nomCompanyChargeur", new String[]{
                nomCompanyChargeur, ebInvoice.getNomCompanyChargeur()
            });

            if (listCategorie != null && listCategorie.size() > 0) {

                for (EbCategorie cat: listCategorie) {
                    if (isPropertyInDtConfig(dtConfig, "category" + cat.getEbCategorieNum())) map
                        .put("category" + cat.getEbCategorieNum(), new String[]{
                            cat.getLibelle(), null
                        });
                }

            }

            if (fields != null && fields.size() > 0) {

                for (EbCustomField customFields: fields) {
                    List<CustomFields> field = new ArrayList<CustomFields>();

                    field = gson.fromJson(customFields.getFields(), new TypeToken<ArrayList<CustomFields>>() {
                    }.getType());

                    if (field != null && field.size() > 0) {

                        for (CustomFields fie: field) {
                            if (isPropertyInDtConfig(dtConfig, fie.getName())) map.put(fie.getName(), new String[]{
                                fie.getLabel(), null
                            });
                        }

                    }

                    if (field != null &&
                        ebInvoice.getListCustomsFields() != null &&
                        (ebInvoice
                            .getChargeur().getEbCompagnie().getEbCompagnieNum()
                            .equals(customFields.getxEbCompagnie()))) {

                        for (CustomFields fie: field) {

                            if (isPropertyInDtConfig(dtConfig, fie.getName())) {

                                for (CustomFields demField: ebInvoice.getListCustomsFields()) {

                                    if (demField.getName().contentEquals(fie.getName())) {
                                        map.put(fie.getName(), new String[]{
                                            fie.getLabel(), demField.getValue()
                                        });
                                        break;
                                    }

                                }

                            }

                        }

                    }

                }

            }

            if (listCategorie != null && ebInvoice.getxEbDemande().getListCategories() != null) {

                for (EbCategorie categorie: listCategorie) {

                    if (isPropertyInDtConfig(dtConfig, "category" + categorie.getEbCategorieNum())) {

                        for (EbCategorie demCategorie: ebInvoice.getxEbDemande().getListCategories()) {

                            if (demCategorie.getLabels() != null &&
                                demCategorie.getEbCategorieNum().equals(categorie.getEbCategorieNum())) {
                                String label = null;

                                for (EbLabel ebLabel: demCategorie.getLabels()) {
                                    label = ebLabel.getLibelle();
                                    break;
                                }

                                map.put("category" + categorie.getEbCategorieNum(), new String[]{
                                    categorie.getLibelle(), label
                                });
                                break;
                            }

                        }

                    }

                }

            }

        }

        if (Role.ROLE_CONTROL_TOWER.getCode().equals(connectedUser.getRole())) {
            if (isPropertyInDtConfig(dtConfig, "statutChargeur")) map.put("statutChargeur", new String[]{
                statutChargeur,
                ebInvoice.getStatutChargeur() != null ?
                    FAEnumeration.InvoiceStatus.getLibelleByCode(ebInvoice.getStatutChargeur()) :
                    FAEnumeration.InvoiceStatus.getLibelleByCode(ebInvoice.getStatut())
            });

            if (isPropertyInDtConfig(dtConfig, "statutCarrier")) map.put("statutCarrier", new String[]{
                statutCarrier,
                ebInvoice.getStatutCarrier() != null ?
                    FAEnumeration.InvoiceStatus.getLibelleByCode(ebInvoice.getStatutCarrier()) :
                    FAEnumeration.InvoiceStatus.getLibelleByCode(ebInvoice.getStatut())
            });

            if (isPropertyInDtConfig(dtConfig, "uploadFileNbr")) map.put("uploadFileNbr", new String[]{
                "Documents", ebInvoice.getUploadFileNbr() != null ? ebInvoice.getUploadFileNbr().toString() : null
            });

            if (isPropertyInDtConfig(dtConfig, "flag")) map.put("flag", new String[]{
                flag, FAEnumeration.Flag.getLibelleByCode(ebInvoice.getFlag())
            });

            if (isPropertyInDtConfig(
                dtConfig,
                "applyChargeurIvoiceNumber")) map.put("applyChargeurIvoiceNumber", new String[]{
                    applyIvoiceNumber,
                    ebInvoice.getApplyChargeurIvoiceNumber() != null ?
                        ebInvoice.getApplyChargeurIvoiceNumber().toString() :
                        null
            });

            if (isPropertyInDtConfig(
                dtConfig,
                "applyChargeurIvoicePrice")) map.put("applyChargeurIvoicePrice", new String[]{
                    applyIvoicePrice,
                    ebInvoice.getApplyChargeurIvoicePrice() != null ?
                        ebInvoice.getApplyChargeurIvoicePrice().toString() :
                        null
            });

            if (isPropertyInDtConfig(dtConfig, "gapChargeur")) map.put("gapChargeur", new String[]{
                gap, ebInvoice.getGapChargeur() != null ? ebInvoice.getGapChargeur().toString() : null
            });

            if (isPropertyInDtConfig(dtConfig, "memoChageur")) map.put("memoChageur", new String[]{
                memo, ebInvoice.getMemoChageur()
            });

            if (isPropertyInDtConfig(dtConfig, "invoiceDateChargeur")) {
                map.put("invoiceDateChargeur", new String[]{
                    invoiceDate,
                    ebInvoice.getInvoiceDateChargeur() != null ?
                        dateFormat.format(ebInvoice.getInvoiceDateChargeur()) :
                        dateFormat.format(calendar.getTime())
                });
                listTypeResult.put("invoiceDateChargeur", Date.class.getName());
            }

            if (isPropertyInDtConfig(dtConfig, "nomCompanyCarrier")) map.put("nomCompanyCarrier", new String[]{
                nomCompanyCarrier, ebInvoice.getNomCompanyCarrier()
            });
            if (isPropertyInDtConfig(dtConfig, "nomCompanyChargeur")) map.put("nomCompanyChargeur", new String[]{
                nomCompanyChargeur, ebInvoice.getNomCompanyChargeur()
            });
        }

        if (Role.ROLE_PRESTATAIRE.getCode().equals(connectedUser.getRole())) {
            if (isPropertyInDtConfig(dtConfig, "statutCarrier")) map.put("statutCarrier", new String[]{
                statutCarrier,
                ebInvoice.getStatutCarrier() != null ?
                    FAEnumeration.InvoiceStatus.getLibelleByCode(ebInvoice.getStatutCarrier()) :
                    FAEnumeration.InvoiceStatus.getLibelleByCode(ebInvoice.getStatut())
            });

            if (isPropertyInDtConfig(dtConfig, "uploadFileNbr")) map.put("uploadFileNbr", new String[]{
                "Documents", ebInvoice.getUploadFileNbr() != null ? ebInvoice.getUploadFileNbr().toString() : null
            });

            if (isPropertyInDtConfig(dtConfig, "flagCarrier")) map.put("flagCarrier", new String[]{
                flag, FAEnumeration.Flag.getLibelleByCode(ebInvoice.getFlagCarrier())
            });

            if (isPropertyInDtConfig(
                dtConfig,
                "applyCarrierIvoiceNumber")) map.put("applyCarrierIvoiceNumber", new String[]{
                    applyIvoiceNumber,
                    ebInvoice.getApplyCarrierIvoiceNumber() != null ?
                        ebInvoice.getApplyCarrierIvoiceNumber().toString() :
                        null
            });

            if (isPropertyInDtConfig(
                dtConfig,
                "applyCarrierIvoicePrice")) map.put("applyCarrierIvoicePrice", new String[]{
                    applyIvoicePrice,
                    ebInvoice.getApplyCarrierIvoicePrice() != null ?
                        ebInvoice.getApplyCarrierIvoicePrice().toString() :
                        null
            });

            if (isPropertyInDtConfig(dtConfig, "gapCarrier")) map.put("gapCarrier", new String[]{
                gap, ebInvoice.getGapCarrier() != null ? ebInvoice.getGapCarrier().toString() : null
            });

            if (isPropertyInDtConfig(dtConfig, "memoCarrier")) map.put("memoCarrier", new String[]{
                memo, ebInvoice.getMemoCarrier()
            });

            if (isPropertyInDtConfig(dtConfig, "invoiceDateCarrier")) {
                map.put("invoiceDateCarrier", new String[]{
                    invoiceDate,
                    ebInvoice.getInvoiceDateCarrier() != null ?
                        dateFormat.format(ebInvoice.getInvoiceDateCarrier()) :
                        dateFormat.format(calendar.getTime())
                });
                listTypeResult.put("invoiceDateCarrier", Date.class.getName());
            }

            if (isPropertyInDtConfig(dtConfig, "nomCompanyChargeur")) map.put("nomCompanyChargeur", new String[]{
                nomCompanyChargeur, ebInvoice.getNomCompanyChargeur()
            });
            if (isPropertyInDtConfig(dtConfig, "nomCompanyCarrier")) map.put("nomCompanyCarrier", new String[]{
                nomCompanyCarrier, ebInvoice.getNomCompanyCarrier()
            });
        }

    }

    public String getConvertedPriceBigDecimal(BigDecimal price, BigDecimal exchangeRate) {
        BigDecimal result = price.multiply(exchangeRate);
        return String.format("%.2f", result);
    }

    public String getConvertedPriceDouble(Double price, Double exchangeRate) {
        Double result = price * exchangeRate;
        return String.format("%.2f", result);
    }

    public String getNegotiatedConvertedPrice(BigDecimal price, BigDecimal exchangeRate) {
        BigDecimal result = BigDecimal.ZERO;

        if (exchangeRate != BigDecimal.ZERO) {
            result = price.divide(exchangeRate, 2, RoundingMode.HALF_UP);
        }

        return String.format("%.2f", result);
    }

    public String getCurrencyLibelle(List<EcCurrency> currencyList, Integer ecCurrecyNum) {
        Optional<EcCurrency> result = currencyList
            .stream().filter(currency -> currency.getEcCurrencyNum() == ecCurrecyNum).findFirst();

        if (result.isPresent()) {
            return result.get().getLibelle();
        }

        return "";
    }

    public String formatValue(Double val) {
        return String.format("%.2f", val).replace(".", ",");
    }

    public String formatValue(BigDecimal val) {
        return String.format("%.2f", val).replace(".", ",");
    }
}
