package com.adias.mytowereasy.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dao.DaoTypeGoods;
import com.adias.mytowereasy.model.EbTypeGoods;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class TypeGoodsServiceImpl extends MyTowerService implements TypeGoodsService {
    @Autowired
    CompagnieService compagnieService;

    @Autowired
    DaoTypeGoods typeGoodsDao;

    @Override
    public EbTypeGoods saveTypeGoods(EbTypeGoods ebTypeGood) {
        EbTypeGoods entity = new EbTypeGoods();

        if (ebTypeGood.getEbTypeGoodsNum() != null) {
            entity = ebTypeGoodsRepository.findById(ebTypeGood.getEbTypeGoodsNum()).orElse(new EbTypeGoods());
        }

        entity.setCode(ebTypeGood.getCode());
        entity.setLabel(ebTypeGood.getLabel());
        ;

        if (entity.getEbTypeGoodsNum() == null) {
            entity.setxEbCompany(connectedUserService.getCurrentUser().getEbCompagnie());
            entity.setDeleted(false);
        }

        return ebTypeGoodsRepository.save(entity);
    }

    @Override
    public Boolean deleteTypeGoods(Integer ebTypeGoodsNum) {
        Optional<EbTypeGoods> entityOpt = ebTypeGoodsRepository.findById(ebTypeGoodsNum);

        if (entityOpt.isPresent()) {
            EbTypeGoods entity = entityOpt.get();
            entity.setDeleted(true);
            ebTypeGoodsRepository.save(entity);
            return true;
        }

        return false;
    }

    @Override
    public List<EbTypeGoods> listWithCommunity() {
        SearchCriteria criteria = new SearchCriteria();

        EbUser connectedUser = connectedUserService.getCurrentUser();
        criteria.setCompanyNums(compagnieService.findContactsCompagniesNums(connectedUser.getEbUserNum(), true));
        return typeGoodsDao.list(criteria);
    }

    @Override
    public List<EbTypeGoods> listTypeGoods(SearchCriteria criteria) {
        return typeGoodsDao.list(criteria);
    }

    @Override
    public Long countListTypeGoods(SearchCriteria criteria) {
        return typeGoodsDao.listCount(criteria);
    }
}
