/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.*;

import org.apache.commons.lang.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.repository.EbLabelRepository;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class CategorieServiceImpl extends MyTowerService implements CategorieService {

    @Autowired
    EbLabelRepository ebLabelRepository;

    @Override
    public EbCategorie addEbCategorie(EbCategorie ebCategorie) {
        return ebCategorieRepository.save(ebCategorie);
    }

    @Override
    public List<EbCategorie> getAllEbCategorie(SearchCriteria criteria) {
        List<EbCategorie> listCategories = daoCategorie.findActiveCategories(criteria.getEbCompagnieNum());

        for (EbCategorie categorie: listCategories) {
            List<EbLabel> labels = ebLabelRepository.findByCategorieEbCategorieNum(categorie.getEbCategorieNum());

            Set<EbLabel> listLabels = new HashSet<EbLabel>(labels);
            categorie.setLabels(listLabels);
        }

        return listCategories;
    }

    @Override
    public EbCategorie updateEbCategorie(EbCategorie ebCategorie) {
        return ebCategorieRepository.save(ebCategorie);
    }

    @Override
    public void deleteEbCategorie(EbCategorie ebCategorie) {
        Optional<EbCategorie> savedEbCategorie = ebCategorieRepository.findById(ebCategorie.getEbCategorieNum());

        if (savedEbCategorie.isPresent()) {
            EbCategorie categorie = savedEbCategorie.get();
            categorie.setDeleted(true);
            ebCategorieRepository.save(categorie);
        }

    }

    @Override
    public EbLabel addEbLabel(EbLabel ebLabel) {
        ebLabel.setDeleted(false);
        return ebLabelRepository.save(ebLabel);
    }

    @Override
    public List<EbLabel> getAllEbLabel(SearchCriteria criteria) {
        return daoCategorie.getAllEbLabel(criteria);
    }

    public Long getLabelCount(SearchCriteria criteria) {
        return daoCategorie.getLabelCount(criteria);
    }

    @Override
    public EbLabel updateEbLabel(EbLabel ebLabel) {
        return ebLabelRepository.save(ebLabel);
    }

    @Override
    public void deleteEbLabel(EbLabel ebLabel) {
        // suppression avec historisation
        ebLabelRepository.deleteLabelWithHistorisation(ebLabel.getEbLabelNum());
    }

    @Override
    public List<EbCategorie> getAllEbCategorieWithLabels(Integer ebEtablissementNum) {
        return ebCategorieRepository.findDistinctByEtablissementEbEtablissementNum(ebEtablissementNum);
    }

    @Override
    public List<EbCategorie> getListEbCategorieByEbCompagnie(Integer ebCompagnieNum) {
        return ebCategorieRepository.findDistinctByEtablissementEbCompagnieEbCompagnieNum(ebCompagnieNum);
    }

    @Override
    public List<EbLabel> getByLibelle(SearchCriteria criteria) {
        List<EbLabel> labels = ebLabelRepository
            .findLabelsBycategorieAndLibelle(criteria.getEbCategorieNum(), criteria.getSearchterm());
        Integer maxLength = (labels.size() < 100) ? labels.size() : 100;
        return labels.subList(0, maxLength);
    }

    @Override
    public List<EbCategorie> getListCategoriesFromListLabels(EbDemande demande, EbCategorie categorie)
        throws Exception {
        List<EbCategorie> listCategories = new ArrayList<>();
        String listLabelStrings = demande.getListLabels();

        if (listLabelStrings != null) {
            String[] labels = listLabelStrings.split(",");

            // Delete duplicates
            LinkedHashSet<String> linkedHashSet = new LinkedHashSet<>(Arrays.asList(labels));
            String[] labelsWithoutDuplicates = linkedHashSet.toArray(new String[]{});

            categorie.setLabels(new HashSet<>());
            Set<EbLabel> listLabels = new HashSet<EbLabel>();

            for (String labelString: labelsWithoutDuplicates) {
                EbLabel label = ebLabelRepository.findByEbLabelNum(NumberUtils.toInt(labelString));

                if (label != null) {
                    listLabels.add(label);
                }

            }

            if (!listLabels.isEmpty()) {
                categorie.setLabels(listLabels);
            }

            listCategories.add(categorie);
        }

        return listCategories;
    }

    @Override
    public List<EbCategorie> getAllEbCategorieWithLabelsNonDeleted(Integer ebEtablissementNum) {
        List<EbCategorie> categories = ebCategorieRepository
            .findDistinctByEtablissementEbEtablissementNum(ebEtablissementNum);
        categories.forEach(cat -> {
            Set<EbLabel> labels = new HashSet<>();

            if (cat.getLabels() != null && !cat.getLabels().isEmpty()) {
                cat.getLabels().forEach(lab -> {
                    if (lab.getDeleted() == null || lab.getDeleted() != null && !lab.getDeleted()) labels.add(lab);
                });
                cat.setLabels(labels);
            }

        });
        return categories;
    }
}
