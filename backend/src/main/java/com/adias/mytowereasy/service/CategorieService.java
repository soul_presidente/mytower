/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface CategorieService {
    public EbCategorie addEbCategorie(EbCategorie ebCategorie);

    public List<EbCategorie> getAllEbCategorie(SearchCriteria criteria);

    public EbCategorie updateEbCategorie(EbCategorie ebCategorie);

    public void deleteEbCategorie(EbCategorie ebCategorie);

    public EbLabel addEbLabel(EbLabel ebLabel);

    public Long getLabelCount(SearchCriteria criteria);

    public List<EbLabel> getAllEbLabel(SearchCriteria criteria);

    public List<EbLabel> getByLibelle(SearchCriteria criteria);

    public EbLabel updateEbLabel(EbLabel ebLabel);

    public void deleteEbLabel(EbLabel ebLabel);

    public List<EbCategorie> getAllEbCategorieWithLabels(Integer ebEtablissementNum);

    public List<EbCategorie> getListEbCategorieByEbCompagnie(Integer ebCompagnieNum);

    public List<EbCategorie> getListCategoriesFromListLabels(EbDemande demande, EbCategorie categorie) throws Exception;

    List<EbCategorie> getAllEbCategorieWithLabelsNonDeleted(Integer ebEtablissementNum);
}
