package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.api.wso.PlanTransportWSO;
import com.adias.mytowereasy.model.EbPlanTransportNew;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


public interface PlanTransportNewService {
    public List<EbPlanTransportNew> listPlanTransport(SearchCriteria criteria);

    public Long countListPlanTransport(SearchCriteria criteria);

    public void deletePlanTransport(EbPlanTransportNew ebPlanTransportNew);

    public void save(EbPlanTransportNew ebPlanTransportNew);

    EbPlanTransportNew saveFlags(Integer ebPlanTransportNum, String newFlags);

    public List<EbPlanTransportNew>
        listLoadPlanIsConstraints(SearchCriteriaPricingBooking criteria, PlanTransportWSO planTransportWSO);
}
