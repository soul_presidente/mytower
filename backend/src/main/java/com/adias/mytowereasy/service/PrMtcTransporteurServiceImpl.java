package com.adias.mytowereasy.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.moteur.chiffrage.library.calcul.DataRow;
import com.adias.mytowereasy.dao.impl.DaoPrMtcTransporteurImpl;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.PrMtcTransporteur;
import com.adias.mytowereasy.mtc.model.PrMtcFieldDTO;
import com.adias.mytowereasy.mtc.service.MtcService;
import com.adias.mytowereasy.repository.EbCompagnieRepository;
import com.adias.mytowereasy.repository.PrMtcTransporteurRepository;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class PrMtcTransporteurServiceImpl implements PrMtcTransporteurService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PrMtcTransporteurService.class);

    @Autowired
    private DaoPrMtcTransporteurImpl daoPrMtcTransporteur;
    @Autowired
    private PrMtcTransporteurRepository prMtcTransporteurRepository;
    @Autowired
    private ConnectedUserService connectedUserService;
    @Autowired
    private MtcService mtcService;

    @Autowired
    private EbCompagnieRepository companyRepository;

    @Override
    public HashMap<String, Object> getListPrMtcTransporteur(SearchCriteria criteria) {
        HashMap<String, Object> result = new HashMap<String, Object>();

        List<PrMtcTransporteur> listMtcTransporteur = daoPrMtcTransporteur.getListPrMtcTransporteur(criteria);
        int counter = daoPrMtcTransporteur.getCountListMtgTransporteur(criteria);

        result.put("count", counter);
        result.put("data", listMtcTransporteur);

        return result;
    }

    @Override
    public Boolean deleteMtcTransporteur(Integer mtcTranspNum) {
        PrMtcTransporteur mtcTransporteur = prMtcTransporteurRepository.findById(mtcTranspNum).get();
        // Deleting
        prMtcTransporteurRepository.delete(mtcTransporteur);
        return true;
    }

    @Override
    public PrMtcTransporteur saveMtcTransporteur(PrMtcTransporteur prMtcTransporteur) {
        PrMtcTransporteur prMtc;

        if (prMtcTransporteur.getxEbUser() == null || prMtcTransporteur.getxEbUser().getEbUserNum() == null) {
            throw new IllegalArgumentException("carrier is empty");
        }

        if (prMtcTransporteur.getPrMtcTransporteurNum() != null) {
            prMtc = prMtcTransporteurRepository.findById(prMtcTransporteur.getPrMtcTransporteurNum()).get();
        }
        else {
            prMtc = new PrMtcTransporteur();
            EbUser connectedUser = connectedUserService.getCurrentUser();
            prMtc.setxEbCompagnieChargeur(connectedUser.getEbCompagnie());
        }

        if (prMtc.getCodeConfigurationMtc() == null) {
            prMtc.setAttributes(null);
        }
        else if (prMtc.getAttributes() == null
            || !prMtc.getCodeConfigurationMtc().equalsIgnoreCase(prMtcTransporteur.getCodeConfigurationMtc())) {

            try {
                DataRow row = mtcService.loadMtcColumns(prMtcTransporteur.getCodeConfigurationMtc());

                if (row != null) {
                    prMtc.setAttributes(PrMtcFieldDTO.fromColumns(row.getDataColumns()));
                }
                else {
                    prMtc.setAttributes(null);
                }

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

        }

        prMtc.setCodeConfigurationMtc(prMtcTransporteur.getCodeConfigurationMtc());
        EbCompagnie companyCarrier = companyRepository
            .selectCompagnieByEbUserNum(prMtcTransporteur.getxEbUser().getEbUserNum());
        prMtc.setxEbCompagnieTransporteur(companyCarrier);
        prMtc.setxEbUser(prMtcTransporteur.getxEbUser());
        prMtc.setCodeConfigurationEDI(prMtcTransporteur.getCodeConfigurationEDI());

        return prMtcTransporteurRepository.save(prMtc);
    }
}
