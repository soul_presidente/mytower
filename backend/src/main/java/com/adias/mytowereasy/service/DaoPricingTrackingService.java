package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


public interface DaoPricingTrackingService {
    List<EbDemande> getListEbDemande(SearchCriteriaPricingBooking criteria) throws Exception;
}
