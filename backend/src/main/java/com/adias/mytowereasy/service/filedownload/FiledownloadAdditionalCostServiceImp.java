package com.adias.mytowereasy.service.filedownload;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.service.AdditionalCostService;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class FiledownloadAdditionalCostServiceImp extends MyTowerService implements FiledownloadAdditionalCostService {
    @Autowired
    protected AdditionalCostService additionalCostService;

    @Override
    public FileDownloadStateAndResult
        listFileDownLoadAdditionalCost(List<Map<String, String>> dtConfig, EbUser connectedUser, SearchCriteria criteria

    ) throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();

        List<EbAdditionalCost> listAdditionalCosts = additionalCostService.getListAdditionalCosts(criteria);

        for (EbAdditionalCost ebAdditionalCost: listAdditionalCosts) {
            Map<String, String[]> map = new LinkedHashMap<String, String[]>();

            if (isPropertyInDtConfig(dtConfig, "code")) map.put("code", new String[]{
                "Code", ebAdditionalCost.getCode()
            });
            if (isPropertyInDtConfig(dtConfig, "libelle")) map.put("libelle", new String[]{
                "Libelle", ebAdditionalCost.getLibelle()
            });
            if (isPropertyInDtConfig(dtConfig, "actived")) map.put("actived", new String[]{
                "Actived", ebAdditionalCost.getActived().toString()
            });

            listresult.add(map);
        }

        resultAndState.setState(Enumeration.FileDownLoadStatus.ENCOURS.getCode());
        resultAndState.setListResult(listresult);

        return resultAndState;
    }
}
