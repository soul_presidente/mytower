package com.adias.mytowereasy.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;

import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EbWebHooks;
import com.adias.mytowereasy.model.QEbWebHooks;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class WebHooksServiceImpl extends MyTowerService implements WebHooksService {
    @PersistenceContext
    EntityManager em;

    QEbWebHooks qEbWebHooks = QEbWebHooks.ebWebHooks;

    @Override
    public Boolean addWebHooks(String user, String pwd, String msg) {
        EbUser userInfos = ebUserRepository.findByUsername(user);

        if (userInfos != null) {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setEbUserNum(userInfos.getEbUserNum());
            criteria.setEbUserPassword(pwd);

            if (userService.matchUserPassword(criteria)) {
                ebWebHooksRepository.save(new EbWebHooks(msg, userInfos));
                return true;
            }

        }

        return false;
    }

    @Override
    public List<EbWebHooks> listWebHooks() {
        List<EbWebHooks> result = null;

        try {
            JPAQuery<EbWebHooks> query = new JPAQuery<EbWebHooks>(em);
            query.from(qEbWebHooks);
            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
