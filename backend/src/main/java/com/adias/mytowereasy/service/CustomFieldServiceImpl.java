package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbCustomField;
import com.adias.mytowereasy.repository.EbCompagnieRepository;
import com.adias.mytowereasy.repository.EbCustomFieldRepository;
import com.adias.mytowereasy.util.JsonUtils;


@Service
public class CustomFieldServiceImpl implements CustomFieldService {
    @Autowired
    EbCompagnieRepository ebCompagnieRepository;

    @Autowired
    EbCustomFieldRepository ebCustomFieldRepository;

    @Override
    public List<CustomFields> getCustomFields(Integer ebUserNum) {
        List<CustomFields> customFields = null;
        EbCompagnie compagnie = ebCompagnieRepository.selectCompagnieByEbUserNum(ebUserNum);
        EbCustomField ebCustomField = ebCustomFieldRepository.findFirstByXEbCompagnie(compagnie.getEbCompagnieNum());
        if (ebCustomField != null) customFields = JsonUtils
            .<CustomFields> deserializeList(ebCustomField.getFields(), CustomFields.class, new ArrayList<>());
        return customFields;
    }
}
