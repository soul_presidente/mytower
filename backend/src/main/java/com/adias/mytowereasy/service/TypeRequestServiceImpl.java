package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dto.EbTypeRequestDTO;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbTypeRequest;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.TypeDemande;
import com.adias.mytowereasy.repository.EbTypeRequestRepository;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class TypeRequestServiceImpl extends MyTowerService implements TypeRequestService {
    @Autowired
    CompagnieService compagnieService;

    @Autowired
    EbTypeRequestRepository ebTypeRequestRepository;

    @Autowired
    UserService userService;

    @Override
    public List<EbTypeRequest> searchTypeRequest(SearchCriteria criteria) {
        return daoTypeRequest.searchEbTypeRequest(criteria);
    }

    @Override
    public Long countListEbTypeRequest(SearchCriteria criteria) {
        return daoTypeRequest.countListEbTypeRequest(criteria);
    }

    @Override
    public List<EbTypeRequestDTO> convertEntitiesToDto(List<EbTypeRequest> entities) {
        List<EbTypeRequestDTO> listDto = new ArrayList<>();
        entities.forEach(e -> listDto.add(new EbTypeRequestDTO(e)));
        return listDto;
    }

    @Override
    public EbTypeRequestDTO saveEbTypeRequest(EbTypeRequestDTO typeRequest) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        return new EbTypeRequestDTO(ebTypeRequestRepository.save(new EbTypeRequest(typeRequest, connectedUser)));
    }

    @Override
    public void deleteEbTypeRequest(Integer typeRequestNum) {
        ebTypeRequestRepository.deleteById(typeRequestNum);
    }

    /*
     * cette methode permet de generer automatiquement les types de demande
     * (urgent/standard), s'ils ne sont pas encore existants pour une compagnie
     * plus la reprise de données dans la liste des demandes de la memme
     * compagnie
     */
    @Override
    public void generateTypeOfRequestByCompanyNum(List<Integer> listEbCompagnieNum) throws Exception {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        SearchCriteria criteria = new SearchCriteria();

        for (Integer ebCompagnieNum: listEbCompagnieNum) {
            criteria.setEbCompagnieNum(ebCompagnieNum);

            List<EbTypeRequest> listTypeOfRequest = daoTypeRequest.searchEbTypeRequest(criteria);

            EbTypeRequest trStandard = new EbTypeRequest(
                "STD",
                TypeDemande.STANDARD.getLibelle(),
                48,
                TypeDemande.STANDARD.getCode(),
                null,
                connectedUser,
                new EbCompagnie(ebCompagnieNum));

            EbTypeRequest trUrgent = new EbTypeRequest(
                "URG",
                TypeDemande.URGENT.getLibelle(),
                24,
                TypeDemande.URGENT.getCode(),
                true,
                connectedUser,
                new EbCompagnie(ebCompagnieNum));

            // si la comapgnie n'a aucuns type de demande alors on va ajouter
            // les 2 TR
            if (listTypeOfRequest == null || listTypeOfRequest.isEmpty()) {
                listTypeOfRequest = new ArrayList<>();
                listTypeOfRequest.add(trUrgent);
                listTypeOfRequest.add(trStandard);
            }
            else {
                Boolean existTrUrgent = false, existTrStandard = false;

                for (EbTypeRequest tr: listTypeOfRequest) {

                    if (tr.getReference().equals("URG") && tr.getLibelle().equals(TypeDemande.URGENT.getLibelle())) {
                        existTrUrgent = true;
                    }
                    else if (tr.getReference().equals("STD")
                        && tr.getLibelle().equals(TypeDemande.STANDARD.getLibelle())) {
                        existTrStandard = true;
                    }

                }

                if (!existTrStandard) {
                    listTypeOfRequest.add(trStandard);
                }

                if (!existTrUrgent) {
                    listTypeOfRequest.add(trUrgent);
                }

            }

            if (listTypeOfRequest != null && !listTypeOfRequest.isEmpty()) {
                saveTypeRequestAndRefreshListDemande(listTypeOfRequest);
            }

        }

    }

    // cette methode permet de creer les types of request monquant puis faire la
    // maj des infos du TR dans les demandes de la meme compagnie
    void saveTypeRequestAndRefreshListDemande(List<EbTypeRequest> listTypeOfRequest) {

        if (listTypeOfRequest.size() > 1) // si la liste contient plus que deux
                                          // elements alors on va creer les 2
                                          // types
        {
            ebTypeRequestRepository.saveAll(listTypeOfRequest);

            for (EbTypeRequest tr: listTypeOfRequest) {

                if (tr.getLibelle().equals(TypeDemande.STANDARD.getLibelle())
                    || tr.getLibelle().equals(TypeDemande.URGENT.getLibelle())) {
                    Integer typeRequest = tr.getLibelle().equals(TypeDemande.STANDARD.getLibelle()) ?
                        TypeDemande.STANDARD.getCode() :
                        TypeDemande.URGENT.getCode();
                    ebDemandeRepository
                        .recoveryTypeOfRequestByEbCompanyNumAndTypeDemande(
                            tr.getxEbCompagnie().getEbCompagnieNum(),
                            typeRequest,
                            tr.getEbTypeRequestNum(),
                            tr.getLibelle(),
                            tr.getDelaiChronoPricing(),
                            tr.getSensChronoPricing());
                }

            }

        }
        else {
            Integer typeRequest = listTypeOfRequest.get(0).getLibelle().equals(TypeDemande.STANDARD.getLibelle()) ?
                TypeDemande.STANDARD.getCode() :
                TypeDemande.URGENT.getCode();

            ebTypeRequestRepository.save(listTypeOfRequest.get(0));
            ebDemandeRepository
                .recoveryTypeOfRequestByEbCompanyNumAndTypeDemande(
                    listTypeOfRequest.get(0).getxEbCompagnie().getEbCompagnieNum(),
                    typeRequest,
                    listTypeOfRequest.get(0).getEbTypeRequestNum(),
                    listTypeOfRequest.get(0).getLibelle(),
                    listTypeOfRequest.get(0).getDelaiChronoPricing(),
                    listTypeOfRequest.get(0).getSensChronoPricing());
        }

    }

    @Override
    public List<EbTypeRequest> listWithCommunity() {
        SearchCriteria criteria = new SearchCriteria();

        EbUser connectedUser = connectedUserService.getCurrentUser();
        criteria.setCompanyNums(compagnieService.findContactsCompagniesNums(connectedUser.getEbUserNum(), true));
        return daoTypeRequest.searchEbTypeRequest(criteria);
    }

    @Override
    public List<EbTypeRequest> searchTypeRequestByReference(String term) {
        return daoTypeRequest.searchTypeRequestByReference(term);
    }

    @Override
    public List<EbTypeRequest> getListServiceLevel(SearchCriteria criteria) {
        List<EbTypeRequest> listEbTypeRequests = new ArrayList<>();
        EbUser connectedUser = connectedUserService.getCurrentUser();
        criteria.setContact(true);
        criteria.setContactable(true);
        criteria.setEbUserNum(criteria.getConnectedUserNum());
        criteria.setSize(20);

        if (connectedUser.isControlTower() || connectedUser.isPrestataire()) {
            listEbTypeRequests = listWithCommunity();
        }
        else {
            listEbTypeRequests = daoTypeRequest.searchEbTypeRequest(criteria);
        }

        return new ArrayList<EbTypeRequest>(
            listEbTypeRequests
                .stream().collect(
                    Collectors
                        .toCollection(
                            () -> new TreeSet<EbTypeRequest>((p1, p2) -> p1.getLibelle().compareTo(p2.getLibelle())))));
    }
}
