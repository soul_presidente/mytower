/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class CompagnieServiceImpl extends MyTowerService implements CompagnieService {
    @Autowired
    CategorieService categorieService;

    @Override
    public Set<EbEtablissement> selectListEbEtablissementEbCompagnie(SearchCriteria criterias) {

        if (criterias.isWithEtablissementDetail()) return ebEtablissementRepository
            .findAllWithDetailsByEbCompagnie_ebCompagnieNum(criterias.getEbCompagnieNum());
        else {
            return ebEtablissementRepository.findAllByEbCompagnie_ebCompagnieNum(criterias.getEbCompagnieNum());
        }

    }

    @Override
    public EbCompagnie selectEbCompagnie(SearchCriteria criterias) {
        EbCompagnie comp = ebCompagnieRepository.findOneWithDetailsByEbCompagnieNum(criterias.getEbCompagnieNum());

        // Setting compagnie categorie
        comp.setCategories(null);
        List<EbCategorie> categories = categorieService.getAllEbCategorie(criterias);
        Set<EbCategorie> listCategories = new HashSet<EbCategorie>(categories);
        comp.setCategories(listCategories);
        return comp;
    }

    @Override
    public EbEtablissement updateEbEtablissement(EbEtablissement ebEtablissement) {
        return ebEtablissementRepository.save(ebEtablissement);
    }

    @Override
    public List<EbCompagnie> selectListContactCompagnie(SearchCriteria criteria) {
        return (List<EbCompagnie>) daoUser.selectListContact(criteria);
    }

    @Override
    public List<EbCompagnie> selectListCompagnie(SearchCriteria criteria) {
        return daoCompagnie.selectListCompagnie(criteria);
    }

    @Override
    public List<Integer> findContactsCompagniesNums(Integer ebUserNum) {
        SearchCriteria criteriaUser = new SearchCriteria();
        List<Integer> companyNums = new ArrayList<>();

        criteriaUser.setEbUserNum(ebUserNum);

        List<EbUser> contacts = userService.selectListContact(criteriaUser);

        for (EbUser contact: contacts) {
            companyNums.add(contact.getEbCompagnie().getEbCompagnieNum());
        }

        return companyNums;
    }

    @Override
    public List<Integer> findContactsCompagniesNums(Integer ebUserNum, Boolean addConnectedUserCompany) {
        List<Integer> rList = findContactsCompagniesNums(ebUserNum);

        if (addConnectedUserCompany) {
            EbUser connectedUser = connectedUserService.getCurrentUser();
            rList.add(connectedUser.getEbCompagnie().getEbCompagnieNum());
        }

        return rList;
    }
}
