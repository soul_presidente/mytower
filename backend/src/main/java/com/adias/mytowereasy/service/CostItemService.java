package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.EbCost;
import com.adias.mytowereasy.model.EbCostCategorie;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface CostItemService {
    public List<EbCost> getListCostsItems(SearchCriteria criteria);

    public List<EbCostCategorie> getListCostCategorie(Integer compagnieNum, Boolean activedOnly);

    public Long getCountListCostItems(SearchCriteria criteria);

    public EbCost addCostItem(EbCost ebCost);

    public EbCost updateCostItem(EbCost newEbCost);

    public EbCostCategorie addCostCategorie(EbCostCategorie ebCostCategorie);

    public boolean deleteCostItem(Integer ebCostNum);

    public boolean checkCodeExistance(String code, Integer ebCompagnieNum);
}
