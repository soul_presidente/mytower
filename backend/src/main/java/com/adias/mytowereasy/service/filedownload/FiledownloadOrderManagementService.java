package com.adias.mytowereasy.service.filedownload;

import java.util.List;
import java.util.Map;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface FiledownloadOrderManagementService {
    public FileDownloadStateAndResult listFileDownLoadOrderManagement(
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        SearchCriteria criteria,
        List<EbCategorie> listCategorie,
        Map<String, String> mapCustomField)
        throws Exception;

    public FileDownloadStateAndResult listFileDownLoadOrderLineManagement(
        SearchCriteria criteria,
        List<Map<String, String>> dtConfig,
        EbUser connectedUser,
        Map<String, String> mapCustomField)
        throws Exception;
}
