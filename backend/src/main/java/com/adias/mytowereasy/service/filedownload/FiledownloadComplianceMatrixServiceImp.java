package com.adias.mytowereasy.service.filedownload;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbCombinaison;
import com.adias.mytowereasy.model.EbDestinationCountry;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.repository.EbDestinationCountryRepository;
import com.adias.mytowereasy.service.ComplianceMatrixService;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class FiledownloadComplianceMatrixServiceImp implements FiledownloadComplianceMatrixService {
    @Autowired
    private ComplianceMatrixService complianceMatrixService;
    @Autowired
    private EbDestinationCountryRepository ebDestinationCountryRepository;
    @Autowired
    ConnectedUserService connectedUserService;

    @Override
    public FileDownloadStateAndResult listFileDownLoadComplianceMatrix(SearchCriteria criteria, EbUser connectedUser)
        throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();

        if (criteria != null && criteria.getEbEtablissementNum() == null) {
            criteria.setEbEtablissementNum(connectedUser.getEbEtablissement().getEbEtablissementNum());
        }

        criteria.setWithListCombinaison(true);
        List<EbCombinaison> listEbCombinaison = complianceMatrixService.selectListEbCombinaison(criteria);

        if (listEbCombinaison != null && !listEbCombinaison.isEmpty()) {

            for (EbCombinaison ebCombinaison: listEbCombinaison) {
                Map<String, String[]> map = new LinkedHashMap<String, String[]>();

                if (ebCombinaison.getDestinationCountry() != null) {
                    map.put("destinationCountry", new String[]{
                        "Destination country",
                        ebCombinaison.getDestinationCountry().getCountry() != null ?
                            ebCombinaison.getDestinationCountry().getCountry().getLibelle() :
                            null
                    });
                    map.put("generalComments", new String[]{
                        "General comments", ebCombinaison.getDestinationCountry().getGeneralComments()
                    });
                    map.put("preferedPartners", new String[]{
                        "Prefered partners", ebCombinaison.getDestinationCountry().getPreferredPartners()
                    });
                    map.put("processLeadTimes", new String[]{
                        "Process lead times", ebCombinaison.getDestinationCountry().getProcessLeadTimes()
                    });
                    map.put("documentationRequired", new String[]{
                        "Documentation required", ebCombinaison.getDestinationCountry().getDocumentationRequired()
                    });
                }
                else {
                    map.put("destinationCountry", new String[]{
                        "Destination country", null
                    });
                    map.put("generalComments", new String[]{
                        "General comments", null
                    });
                    map.put("preferedPartners", new String[]{
                        "Prefered partners", null
                    });
                    map.put("processLeadTimes", new String[]{
                        "Process lead times", null
                    });
                    map.put("documentationRequired", new String[]{
                        "Documentation required", null
                    });
                }

                map.put("hsCode", new String[]{
                    "HS Code", ebCombinaison.getHsCode()
                });
                map.put("madeInOrigin", new String[]{
                    "Made In Origin", ebCombinaison.getOrigin() != null ? ebCombinaison.getOrigin().getLibelle() : null
                });
                map.put("taxes", new String[]{
                    "Taxes", ebCombinaison.getTaxes() + "%"
                });
                map.put("duties", new String[]{
                    "Duties", ebCombinaison.getDuties()
                });
                map.put("otherTaxes", new String[]{
                    "Other taxes", ebCombinaison.getOtherTaxes()
                });
                if (ebCombinaison.getImportLicense() != null) map.put("importLicense", new String[]{
                    "Import license", ebCombinaison.getImportLicense() ? "Yes" : "No"
                });
                else map.put("importLicense", new String[]{
                    "Import license", null
                });
                map.put("requiredDocuments", new String[]{
                    "Required documents", ebCombinaison.getRequiredDocument()
                });

                if (ebCombinaison.getDestinationCountry() != null) {
                    if (ebCombinaison.getDestinationCountry().getCreatedBy() != null) map.put("createdBy", new String[]{
                        "Created by", ebCombinaison.getDestinationCountry().getCreatedBy().getNomPrenom()
                    });
                    else map.put("createdBy", new String[]{
                        "Created by", null
                    });
                    if (ebCombinaison.getDestinationCountry().getLastUserModified() != null) map
                        .put("modifiedBy", new String[]{
                            "Modified by", ebCombinaison.getDestinationCountry().getLastUserModified().getNomPrenom()
                        });
                    else map.put("modifiedBy", new String[]{
                        "Modified by", null
                    });

                    if (ebCombinaison.getDestinationCountry().getLastDateModified() != null) map
                        .put("lastModifiedDate", new String[]{
                            "Last modified date", ebCombinaison.getDestinationCountry().getLastDateModified().toString()
                        });
                    else map.put("lastModifiedDate", new String[]{
                        "Last modified date", null
                    });
                }
                else {
                    map.put("createdBy", new String[]{
                        "Created by", null
                    });
                    map.put("modifiedBy", new String[]{
                        "Modified by", null
                    });
                    map.put("lastModifiedDate", new String[]{
                        "Last modified date", null
                    });
                }

                listresult.add(map);
            }

        }

        List<EbDestinationCountry> listEbDestinationCountry = ebDestinationCountryRepository
            .findByEtablissement_ebEtablissementNum(
                connectedUserService.getCurrentUser().getEbEtablissement().getEbEtablissementNum());

        if (listEbDestinationCountry != null && !listEbDestinationCountry.isEmpty()) {

            for (EbDestinationCountry dest: listEbDestinationCountry) {

                if (listEbCombinaison != null) {
                    boolean found = false;

                    for (EbCombinaison ebCombinaison: listEbCombinaison) {

                        if (ebCombinaison
                            .getDestinationCountry().getEbDestinationCountryNum()
                            .equals(dest.getEbDestinationCountryNum())) {
                            found = true;
                            break;
                        }

                    }

                    if (found) continue;
                }

                Map<String, String[]> map = new LinkedHashMap<String, String[]>();

                map.put("destinationCountry", new String[]{
                    "Destination country", dest.getCountry() != null ? dest.getCountry().getLibelle() : null
                });
                map.put("generalComments", new String[]{
                    "General comments", dest.getGeneralComments()
                });
                map.put("preferedPartners", new String[]{
                    "Prefered partners", dest.getPreferredPartners()
                });
                map.put("processLeadTimes", new String[]{
                    "Process lead times", dest.getProcessLeadTimes()
                });
                map.put("documentationRequired", new String[]{
                    "Documentation required", dest.getDocumentationRequired()
                });

                map.put("hsCode", new String[]{
                    "HS Code", null
                });
                map.put("madeInOrigin", new String[]{
                    "Made In Origin", null
                });
                map.put("taxes", new String[]{
                    "Taxes", null
                });
                map.put("duties", new String[]{
                    "Duties", null
                });
                map.put("otherTaxes", new String[]{
                    "Other taxes", null
                });
                map.put("importLicense", new String[]{
                    "Import license", null
                });
                map.put("requiredDocuments", new String[]{
                    "Required documents", null
                });

                if (dest.getCreatedBy() != null) map.put("createdBy", new String[]{
                    "Created by", dest.getCreatedBy() != null ? dest.getCreatedBy().getNomPrenom() : null
                });
                else map.put("createdBy", new String[]{
                    "Created by", null
                });
                if (dest.getLastUserModified() != null) map.put("modifiedBy", new String[]{
                    "Modified by", dest.getLastUserModified() != null ? dest.getLastUserModified().getNomPrenom() : null
                });
                else map.put("modifiedBy", new String[]{
                    "Modified by", null
                });

                if (dest.getLastDateModified() != null) map.put("lastModifiedDate", new String[]{
                    "Last modified date",
                    dest.getLastUserModified() != null ? dest.getLastDateModified().toString() : null
                });
                else map.put("lastModifiedDate", new String[]{
                    "Last modified date", null
                });

                // adding data
                listresult.add(map);
            }

        }

        resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
        resultAndState.setListResult(listresult);
        return resultAndState;
    }
}
