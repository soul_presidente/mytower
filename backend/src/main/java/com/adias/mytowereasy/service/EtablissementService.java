/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.List;
import java.util.Map;

import com.adias.mytowereasy.model.EbAdresse;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbCustomField;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.TransfertUserObject;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface EtablissementService {
    public EbEtablissement selectEbEtablissement(SearchCriteria criterias);

    public List<EbEtablissement> selectListEbEtablissement(SearchCriteria criterias);

    public EbEtablissement updateEbEtablissement(EbEtablissement ebEtablissement);

    public EbEtablissement addEtablissement(EbEtablissement ebEtablissement);

    // ----------------------------------------------------------------------------------
    // //

    public List<EbAdresse> selectListEbAdresseEbEtablissement(SearchCriteria criterias);

    public EbAdresse addEbAdresseEbEtablissement(EbAdresse ebAdresse);

    public EbAdresse updateEbAdresseEbEtablissement(EbAdresse ebAdresse);

    public Boolean deleteEbAdresseEbEtablissement(EbAdresse ebAdresse);

    // ----------------------------------------------------------------------------------
    // //

    public EbCustomField addEbCustomField(EbCustomField ebCustomField);

    public EbCustomField selectEbCustomFieldByEbCompagnieNum(Integer ebEtablissementNum);

    public List<EbCategorie> selectListEbCategorieByEtablissementNum(Integer ebEtablissementNum);

    // ----------------------------------------------------------------------------------
    // //

    List<EbEtablissement> selectListEbEtablissementCt(SearchCriteria criterias);

    Map<String, Object> getDataPreference(SearchCriteria criteria);

    public Boolean transfertUser(TransfertUserObject transfertUserObjet) throws Exception;

    public Boolean updateTransportFilesEtablissement(TransfertUserObject transfertUserObjet) throws Exception;

    Long getListEtablissementCount(SearchCriteria criteria);
}
