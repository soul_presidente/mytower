package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.adias.mytowereasy.dao.DaoMarchandise;
import com.adias.mytowereasy.dao.DaoUnit;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbMarchandiseForExtraction;
import com.adias.mytowereasy.model.Enumeration.TypeContainer;
import com.adias.mytowereasy.repository.EbMarchandiseRepository;
import com.adias.mytowereasy.util.EbMarchardiseToTreeNode;


@Component
public class UnitServiceImpl implements UnitService {
    @Autowired
    DaoUnit daoUnit;
    @Autowired
    EbMarchandiseRepository ebMarchandiseRepository;
    @Autowired
    DaoMarchandise daoMarchandise;

    @Override
    public Map<String, Object> getListTree(Integer idDemande) {
        List<EbMarchandise> listMarchandise = ebMarchandiseRepository.findAllByEbDemande_ebDemandeNum(idDemande);
        Map<String, Object> listTransform = EbMarchardiseToTreeNode.listMarchandiseToListTreeNode(listMarchandise);
        HashMap<String, Object> result = new HashMap<String, Object>();

        Long nbrContainer = EbMarchardiseToTreeNode.countTypeMarchandise(listMarchandise, "1");
        Long nbrPalette = EbMarchardiseToTreeNode.countTypeMarchandise(listMarchandise, "2");
        Long nbrColis = EbMarchardiseToTreeNode.countTypeMarchandise(listMarchandise, "3");
        Long nbrArticle = EbMarchardiseToTreeNode.countTypeMarchandise(listMarchandise, "4");

        // Result after transfrom
        result.put("listMarchandise", listTransform.get("listEbMarchandise"));
        result.put("nbrContainer", nbrContainer);
        result.put("nbrPalette", nbrPalette);
        result.put("nbrColis", nbrColis);
        result.put("nbrArticle", nbrArticle);
        result.put("listTreeNode", listTransform.get("treeMarchandises"));
        return result;
    }

    @Override
    public List<Map<String, String[]>>
        listMapForExtractionConsolider(List<EbMarchandiseForExtraction> listMarchandiseAExporter) {
        List<Map<String, String[]>> result = new ArrayList<Map<String, String[]>>();

        for (EbMarchandiseForExtraction mr: listMarchandiseAExporter) {
            Map<String, String[]> map = new LinkedHashMap<String, String[]>();
            map.put("unitNum", new String[]{
                "Unit num", mr.getEbMarchandiseNum().toString()
            });

            // CAS ARTCICLE
            map.put("unitReferenceArticle", new String[]{
                "Unit Reference", mr.getUnitReferenceArticle() != null ? mr.getUnitReferenceArticle() : ""
            });
            map.put("nomArticle", new String[]{
                "Name Article", mr.getNomArticle() != null ? mr.getNomArticle() : ""
            });
            map.put("originCountryName", new String[]{
                "Origin Country", mr.getOriginCountryName() != null ? mr.getOriginCountryName() : ""
            });
            map.put("lotArticle", new String[]{
                "Lot", mr.getLotArticle() != null ? mr.getLotArticle() : ""
            });
            map.put("lot_quantiteArticle", new String[]{
                "Lot Quantité", mr.getLotQuantiteArticle() != null ? mr.getLotQuantiteArticle() : ""
            });
            map.put("batchArticle", new String[]{
                "Batch", mr.getBatchArticle() != null ? mr.getBatchArticle().toString() : ""
            });

            // CAS PARCEL
            map.put("unitReferenceParcel", new String[]{
                "Unit Reference", mr.getUnitReferenceParcel() != null ? mr.getUnitReferenceParcel() : ""
            });
            map.put("lotParcel", new String[]{
                "Lot", mr.getLotParcel() != null ? mr.getLotParcel() : ""
            });
            map.put("lot_quantiteParcel", new String[]{
                "Lot Quantité", mr.getLotQuantiteParcel() != null ? mr.getLotQuantiteParcel() : ""
            });
            map.put("BatchParcel", new String[]{
                "Batch", mr.getBatchParcel() != null ? mr.getBatchParcel().toString() : ""
            });

            // CAS PALETTE
            map.put("unitReferencePalette", new String[]{
                "Unit Reference", mr.getUnitReferencePalette() != null ? mr.getUnitReferencePalette() : ""
            });
            map.put("lotPalette", new String[]{
                "Lot", mr.getLotPalette() != null ? mr.getLotPalette() : ""
            });
            map.put("lot_quantitePalette", new String[]{
                "Lot Quantité", mr.getLotQuantitePalette() != null ? mr.getLotQuantitePalette() : ""
            });
            map.put("BatchPalette", new String[]{
                "Batch", mr.getBatchPalette() != null ? mr.getBatchPalette().toString() : ""
            });

            // CAS CONTAINER
            map.put("unitReferenceContainer", new String[]{
                "Unit Reference", mr.getUnitReferenceContainer() != null ? mr.getUnitReferenceContainer() : ""
            });
            map.put("lotContainer", new String[]{
                "Lot", mr.getLotContainer() != null ? mr.getLotContainer() : ""
            });
            map.put("lot_quantiteContainer", new String[]{
                "Lot Quantité", mr.getLotQuantiteContainer() != null ? mr.getLotQuantiteContainer() : ""
            });
            map.put("BatchContainer", new String[]{
                "Batch", mr.getBatchContainer() != null ? mr.getBatchContainer().toString() : ""
            });

            result.add(map);
        }

        return result;
    }

    @Override
    public List<Map<String, String[]>>
        listMapForExcelUtils(List<EbMarchandise> listMarchandiseAExporter, Integer typeContener) {
        List<Map<String, String[]>> result = new ArrayList<Map<String, String[]>>();

        for (EbMarchandise mr: listMarchandiseAExporter) {
            Map<String, String[]> map = new LinkedHashMap<String, String[]>();
            map.put("unitNum", new String[]{
                "Unit num", mr.getEbMarchandiseNum().toString()
            });
            map.put("unitRef", new String[]{
                "Unit Reference", mr.getUnitReference() != null ? mr.getUnitReference() : ""
            });

            if (typeContener == TypeContainer.ARTICLE.getKey() || typeContener == TypeContainer.ALL.getKey()) {
                map.put("nomArticle", new String[]{
                    "Name Article", mr.getNomArticle() != null ? mr.getNomArticle() : ""
                });

                map.put("originCountryName", new String[]{
                    "Origin Country", mr.getOriginCountryName() != null ? mr.getOriginCountryName() : ""
                });
            }

            map.put("lot", new String[]{
                "Lot", mr.getLot() != null ? mr.getLot() : ""
            });
            map.put("numberOfUnit", new String[]{
                "Number of unit", mr.getNumberOfUnits() != null ? mr.getNumberOfUnits().toString() : ""
            });
            map.put("classGood", new String[]{
                "Class", mr.getClassGood() != null ? mr.getClassGood() : ""
            });
            map.put("packaging", new String[]{
                "Packaging", mr.getPackaging() != null ? mr.getPackaging().toString() : ""
            });

            map.put("un", new String[]{
                "UN", mr.getUn() != null ? mr.getUn().toString() : ""
            });

            if (typeContener == TypeContainer.ALL.getKey()) {
                map.put("labelTypeUnit", new String[]{
                    "Type of Unit",
                    mr.getxEbTypeConteneur() != null ?
                        TypeContainer.getTypeContainerByKey(mr.getxEbTypeConteneur()).getValue() :
                        ""
                });
            }

            map.put("olpn", new String[]{
                "OLPN", mr.getOlpn() != null ? mr.getOlpn().toString() : ""
            });

            map.put("lenght", new String[]{
                "LENGTH", mr.getLength() != null ? mr.getLength().toString() : ""
            });

            map.put("width", new String[]{
                "WIDTH", mr.getWidth() != null ? mr.getWidth().toString() : ""
            });

            map.put("volume", new String[]{
                "Volume / unit (m3)", mr.getWidth() != null ? mr.getWidth().toString() : ""
            });

            if (typeContener == TypeContainer.PARCEL.getKey() || typeContener == TypeContainer.PALETTE.getKey()) {
                map.put("sscc", new String[]{
                    "SSCC", mr.getSscc() != null ? mr.getSscc() : ""
                });
            }

            result.add(map);
        }

        return result;
    }

    @Override
    public Integer getNextMarchandiseNum() {
        return daoMarchandise.nextValEbMarchandise();
    }

    @Override
    public List<EbMarchandise> getListMarchandises(Integer idDemande) {
        List<EbMarchandise> listMarchandise = ebMarchandiseRepository.findAllByEbDemande_ebDemandeNum(idDemande);

        return listMarchandise;
    }
}
