package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.dto.EbBoutonActionDTO;
import com.adias.mytowereasy.model.EbBoutonAction;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface BoutonActionService {
    public EbBoutonAction addBoutonAction(EbBoutonAction ebBoutonAction);

    public List<EbBoutonAction> getListBoutonAction(SearchCriteria criteria);

    public Long countListBoutonAction(SearchCriteria criteria);

    public void deleteBoutonAction(Integer ebBoutonActionNum);

    public List<EbBoutonActionDTO> getListBoutonActionFromCodes(String listBoutonActionCodes);
}
