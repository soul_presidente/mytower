/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dto.EbDestinationCountryDTO;
import com.adias.mytowereasy.model.EbCombinaison;
import com.adias.mytowereasy.model.EbDestinationCountry;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class ComplianceMatrixServiceImpl extends MyTowerService implements ComplianceMatrixService {
    @Override
    public EbDestinationCountry selectEbDestinationCountry(SearchCriteria criteria) {
        return daoComplianceMatrix.selectEbDestinationCountry(criteria);
    }

    @Override
    @Transactional
    public EbDestinationCountry insertEbDestinationCountry(EbDestinationCountry ebDestinationCountry) {
        ebDestinationCountry.setCreatedBy(new EbUser(connectedUserService.getCurrentUser().getEbUserNum()));
        ebDestinationCountry.setLastUserModified(new EbUser(connectedUserService.getCurrentUser().getEbUserNum()));

        if (ebDestinationCountry.getEtablissement() != null
            && ebDestinationCountry.getEtablissement().getEbEtablissementNum() != null) {
            ebDestinationCountry
                .setEtablissement(new EbEtablissement(ebDestinationCountry.getEtablissement().getEbEtablissementNum()));
        }
        else {
            ebDestinationCountry
                .setEtablissement(
                    new EbEtablissement(
                        connectedUserService.getCurrentUser().getEbEtablissement().getEbEtablissementNum()));
        }

        ebDestinationCountry.setDateAjout(new Date());
        ebDestinationCountry.setLastDateModified(ebDestinationCountry.getDateAjout());
        return ebDestinationCountryRepository.save(ebDestinationCountry);
    }

    @Override
    public List<EbDestinationCountryDTO> selectListEbDestinationCountry(SearchCriteria criteria) {
        List<EbDestinationCountryDTO> dcs = new ArrayList<>();
        List<EbDestinationCountry> dcList = daoComplianceMatrix.selectListEbDestinationCountry(criteria);

        for (EbDestinationCountry ebDestinationCountry: dcList) {
            dcs.add(new EbDestinationCountryDTO(ebDestinationCountry));
        }

        return dcs;
    }

    @Override
    @Transactional
    public EbDestinationCountry updateEbDestinationCountry(EbDestinationCountry ebDestinationCountry) {
        ebDestinationCountry.setLastUserModified(new EbUser(connectedUserService.getCurrentUser().getEbUserNum()));
        ebDestinationCountry.setLastDateModified(new Date());
        EbDestinationCountry dest = ebDestinationCountryRepository.save(ebDestinationCountry);
        EbUser user = new EbUser(dest.getLastUserModified().getEbUserNum());
        user.setNom(dest.getLastUserModified().getNom());
        dest.setLastUserModified(user);
        return dest;
    }

    @Override
    @Transactional
    public Boolean deleteEbDestinationCountry(EbDestinationCountry ebDestinationCountry) {
        Boolean result = false;

        try {
            ebCombinaisonRepository.deleteByDestinationCountry(ebDestinationCountry);
            ebDestinationCountryRepository.delete(ebDestinationCountry);
            result = true;
        } catch (Exception e) {
            System.out.println("Erreur");
            result = false;
            throw e;
        }

        return result;
    }

    @Override
    @Transactional
    public EbCombinaison insertEbCombinaison(EbCombinaison ebCombinaison) {
        SearchCriteria criteria = new SearchCriteria();
        criteria.setEbDestinationCountryNum(ebCombinaison.getDestinationCountry().getEbDestinationCountryNum());
        criteria.setEcCountryNum(ebCombinaison.getOrigin().getEcCountryNum());
        criteria.setHsCode(ebCombinaison.getHsCode());
        List<EbCombinaison> listCombinaison = this.selectListEbCombinaison(criteria);

        if (listCombinaison != null && listCombinaison.size() > 0
            && (ebCombinaison.getEbCombinaisonNum() == null || !listCombinaison
                .get(0).getEbCombinaisonNum().equals(ebCombinaison.getEbCombinaisonNum()))) return new EbCombinaison();

        return ebCombinaisonRepository.save(ebCombinaison);
    }

    @Override
    @Transactional
    public void deleteEbCombinaison(EbCombinaison ebCombinaison) {
        ebCombinaisonRepository.delete(ebCombinaison);
    }

    @Override
    public List<EbCombinaison> selectListEbCombinaison(SearchCriteria criteria) {
        return daoComplianceMatrix.selectListEbCombinaison(criteria);
    }

    @Override
    public List<EbDestinationCountryDTO> selectListEbDestinationCountryByCompany(SearchCriteria criterias) {
        List<EbDestinationCountryDTO> dcs = new ArrayList<>();

        if (criterias.getEbCompagnieNum() != null) {
            List<EbDestinationCountry> dcList = this.daoComplianceMatrix
                .selectListEbDestinationCountryByCompany(criterias);

            for (EbDestinationCountry ebDestinationCountry: dcList) {
                dcs.add(new EbDestinationCountryDTO(ebDestinationCountry));
            }

        }

        return dcs;
    }
}
