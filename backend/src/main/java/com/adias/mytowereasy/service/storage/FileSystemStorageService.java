/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service.storage;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.adias.mytowereasy.enumeration.SupportedMimeTypes;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.UploadDirectory;
import com.adias.mytowereasy.model.custom.EbCustomDeclaration;
import com.adias.mytowereasy.repository.*;
import com.adias.mytowereasy.repository.custom.EbDeclarationRepository;
import com.adias.mytowereasy.service.ListStatiqueService;
import com.adias.mytowereasy.service.PricingService;

import fr.mytower.lib.storage.api.StorageProvider;


@Service
public class FileSystemStorageService implements StorageService {

    private final EbEtablissementRepository ebEtablissementRepository;

    private final EbQmIncidentRepository ebIncidentRepository;

    private final EbCompagnieRepository ebCompagnieRepository;

    private final EbDemandeFichiersJointRepositorty ebDemandeFichiersJointRepositorty;

    private final EbDemandeRepository ebDemandeRepository;

    private final EbInvoiceRepository ebInvoiceRepository;

    private final EbUserRepository ebUserRepository;

    private final PricingService pricingService;

    private final ListStatiqueService listStatiqueService;

    private final EbTrackTraceRepository ebEbTrackTraceRepository;

    private final EbTypeDocumentsRepository ebTypeDocumentsRepository;

    private final EbDeclarationRepository ebCustomDeclarationRepository;

    private final StorageProvider storageProvider;

    @Autowired
    public FileSystemStorageService(
        EbCompagnieRepository ebCompagnieRepository,
        EbEtablissementRepository ebEtablissementRepository,
        EbDemandeFichiersJointRepositorty ebDemandeFichiersJointRepositorty,
        EbQmIncidentRepository ebIncidentRepository,
        EbDemandeRepository ebDemandeRepository,
        EbTypeDocumentsRepository ebTypeDocumentsRepository,
        EbInvoiceRepository ebInvoiceRepository,
        EbUserRepository ebUserRepository,
        PricingService pricingService,
        ListStatiqueService listStatiqueService,
        EbTrackTraceRepository ebEbTrackTraceRepository,
        EbDeclarationRepository ebCustomDeclarationRepository,
        StorageProvider storageProvider) {
        this.ebIncidentRepository = ebIncidentRepository;
        this.ebCompagnieRepository = ebCompagnieRepository;
        this.ebEtablissementRepository = ebEtablissementRepository;
        this.ebDemandeFichiersJointRepositorty = ebDemandeFichiersJointRepositorty;
        this.ebDemandeRepository = ebDemandeRepository;
        this.ebTypeDocumentsRepository = ebTypeDocumentsRepository;
        this.ebInvoiceRepository = ebInvoiceRepository;
        this.ebUserRepository = ebUserRepository;
        this.pricingService = pricingService;
        this.listStatiqueService = listStatiqueService;
        this.ebEbTrackTraceRepository = ebEbTrackTraceRepository;
        this.ebCustomDeclarationRepository = ebCustomDeclarationRepository;
        this.storageProvider = storageProvider;
    }

    @Override
    @Transactional
    public Object store(MultipartFile file, Integer codeTypeFile, Object... params) {
        Integer ebCompagnieNum = null;
        Integer ebEtablissementNum = null;
        boolean isDecremante = false;
        Integer fileNumber = null;
        Integer numEntity = null;
        Integer demandeNum = null;
        String fileTypeDemande = null;
        Integer ebUserNum = null;
        Object result = null;
        String filePath = null;
        Integer module = null;
        String pslEvent = null;
        String fileName;
        EbUser ebUser = null;
        EbCompagnie ebCompagnie;
        Integer id = null;
        EbChat ebChat = null;
        Integer chatCompId = null;
        Map<String, Object> resultMap = new HashMap<>();

        try {
            UploadDirectory typeUpload = UploadDirectory.getUploadDirectoryByCode(codeTypeFile);
            // get variables from ...params

            if (typeUpload
                .getCode().equals(
                    UploadDirectory.PRICING_BOOKING
                        .getCode())
                || typeUpload.getCode().equals(UploadDirectory.QUALITY_MANAGEMENT.getCode())) {
                numEntity = (Integer) params[0];
                demandeNum = (Integer) params[1];
                fileNumber = (Integer) params[2];
                fileTypeDemande = (String) params[3];
                ebUserNum = (Integer) params[4];
                ebEtablissementNum = (Integer) params[5];
                module = (Integer) params[6];
                pslEvent = (String) params[7];

                // EbDemande demande =
                // ebDemandeRepository.selectListTypeDocumentsByEbDemandeNum(demandeNum);
                pricingService.updateListTypeDoc(demandeNum, numEntity, fileTypeDemande, isDecremante);

                if (numEntity == null) {
                    numEntity = demandeNum;
                }
                else {
                    if (demandeNum == null) demandeNum = ebIncidentRepository
                        .findxEbDemandeByebQmIncidentNum(numEntity);
                }

            }

            if (typeUpload.getCode().equals(UploadDirectory.PRICING_BOOKING.getCode())) {
                id = numEntity != null ? numEntity : demandeNum;
            }
            else if (typeUpload.getCode().equals(UploadDirectory.FREIGHT_AUDIT.getCode())) {
                fileNumber = (int) (new Date()).getTime();
                ebUserNum = (Integer) params[2];

                module = Enumeration.Module.FREIGHT_AUDIT.getCode();

                // when uploading from action bar invoiceNum (numEntity) is
                // set to 0
                // to avoid ArgumentException because the controller is
                // expecting an Integer
                numEntity = (Integer) params[0];
                fileTypeDemande = (String) params[3];

                if (numEntity == 0) {
                    numEntity = null;
                }
                else {
                    id = numEntity;
                    pricingService.updateListTypeDoc(null, numEntity, fileTypeDemande, isDecremante);

                    if (demandeNum == null) demandeNum = ebInvoiceRepository.findxEbDemandeNumByEbInvoiceNum(numEntity);
                }

            }
            else if (typeUpload.getCode().equals(UploadDirectory.BULK_IMPORT.getCode())) {
                ebUserNum = (Integer) params[0];

                module = Enumeration.Module.PRICING.getCode();
            }
            else if (typeUpload.getCode().equals(UploadDirectory.QUALITY_MANAGEMENT.getCode())) {
                id = fileNumber;
            }
            else if (typeUpload.getCode().equals(UploadDirectory.CUSTOM.getCode())) {
                id = (Integer) params[1]; // ebCustomDeclarationNum
                module = Enumeration.Module.CUSTOM.getCode();
                fileTypeDemande = (String) params[2];
                ebUserNum = (Integer) params[3];
            }

            // if it's a logo or template save in separate company id folders
            if (UploadDirectory.LOGO.getCode().equals(typeUpload.getCode()) ||
                UploadDirectory.TEMPLATE.getCode().equals(typeUpload.getCode())) {
                ebCompagnieNum = (Integer) params[0];
            }

            // if compagnieNum is null try to get it from the module
            if (ebCompagnieNum == null) {
                ebUser = ebUserRepository.getOneByEbUserNum(ebUserNum);
                ebCompagnie = getEbCompagnieByIdFileAndModule(id, module, ebEtablissementNum, ebUser, typeUpload);

                ebCompagnieNum = ebCompagnie != null ?
                    ebCompagnie.getEbCompagnieNum() :
                    ebUser.getEbCompagnie().getEbCompagnieNum();
            }

            // generate a filename from input
            fileName = MyFileUtils.resolveFileName(typeUpload, file.getOriginalFilename());

            // create the path
            filePath = MyFileUtils.resolveFilePath(typeUpload, fileName, String.valueOf(ebCompagnieNum));
            // copy to final destination
            if (StringUtils.isBlank(filePath)) {
                throw new MyTowerException("Can't upload, file path is blank");
            }

            storageProvider.put(Paths.get(filePath), file.getInputStream(), file.getSize());

            if (UploadDirectory.TEMPLATE.getCode().equals(typeUpload.getCode())) {
                return filePath;
            }
            // ______________________________________________________________________________________________________________________________________
            // Chat persistance

            if (ebUserNum != null) {

                if (module != null &&
                    id != null && (typeUpload.getCode().equals(UploadDirectory.PRICING_BOOKING.getCode())
                    || typeUpload.getCode().equals(UploadDirectory.FREIGHT_AUDIT.getCode()))) {
                    String username = ebUser.getConnectedUsername();
                    String usernameFor = ebUser.getDelegatedUsername();
                    String action = "Upload file";

                    if (fileTypeDemande != null) {
                        EbTypeDocuments ebTypeDocument = ebTypeDocumentsRepository
                            .findFirstByCodeAndXEbCompagnie(fileTypeDemande, ebCompagnieNum);
                        if (ebTypeDocument != null) action = ebTypeDocument.getNom();
                        else action = "Document";
                        action += " uploaded";
                    }

                    if (module.equals(Enumeration.Module.PRICING.getCode())) {
                        chatCompId = Enumeration.IDChatComponent.PRICING.getCode();

                        ebChat = listStatiqueService
                            .historizeAction(id, chatCompId, module, action, username, usernameFor, ebUser);
                    }
                    else if (module.equals(Enumeration.Module.TRANSPORT_MANAGEMENT.getCode())) {
                        chatCompId = Enumeration.IDChatComponent.TRANSPORT_MANAGEMENT.getCode();
                        ebChat = listStatiqueService
                            .historizeAction(id, chatCompId, module, action, username, usernameFor, ebUser);
                    }
                    else if (module.equals(Enumeration.Module.TRACK.getCode())) {
                        chatCompId = Enumeration.IDChatComponent.TRACK.getCode();
                        ebChat = listStatiqueService
                            .historizeAction(id, chatCompId, module, action, username, usernameFor, ebUser);
                    }

                    else if (module.equals(Enumeration.Module.FREIGHT_AUDIT.getCode())) {
                        chatCompId = Enumeration.IDChatComponent.FREIGHT_AUDIT.getCode();
                        ebChat = listStatiqueService
                            .historizeAction(id, chatCompId, module, action, username, usernameFor, ebUser);
                    }

                    else if (module.equals(Enumeration.Module.ORDER_MANAGEMENT.getCode())) {
                        chatCompId = Enumeration.IDChatComponent.ORDER_MANAGEMENT.getCode();
                        ebChat = listStatiqueService
                            .historizeAction(id, chatCompId, module, action, username, usernameFor, ebUser);
                    }

                    else if (module.equals(Enumeration.Module.DELIVERY_MANAGEMENT.getCode())) {
                        chatCompId = Enumeration.IDChatComponent.DELIVERY.getCode();
                        ebChat = listStatiqueService
                            .historizeAction(id, chatCompId, module, action, username, usernameFor, ebUser);
                    }

                }

            }

            // _______________________________________________________________________________

            // gestion de la persistance de l'upload en base de données
            if (typeUpload.getCode().equals(UploadDirectory.LOGO.getCode())) {
                ebCompagnieRepository.updateCompagnieLogo(filePath, ebCompagnieNum);
            }

            if (typeUpload.getCode().equals(UploadDirectory.QUALITY_MANAGEMENT.getCode())
                ||
                typeUpload
                    .getCode().equals(
                        UploadDirectory.PRICING_BOOKING
                            .getCode())
                || typeUpload.getCode().equals(UploadDirectory.FREIGHT_AUDIT.getCode())
                || typeUpload.getCode().equals(UploadDirectory.CUSTOM.getCode())) {
                EbDemandeFichiersJoint fileReference = new EbDemandeFichiersJoint();
                fileReference.setNumEntity(numEntity);
                fileReference.setxEbDemande(demandeNum);
                fileReference.setAttachmentDate(new Date());
                fileReference.setOriginFileName(file.getOriginalFilename());
                fileReference.setFileName(fileName);
                fileReference.setIdCategorie(fileTypeDemande);
                fileReference.setEbDemandeFichierJointNum(fileNumber);
                fileReference.setChemin(filePath);
                fileReference.setxEbUser(new EbUser(ebUserNum));
                fileReference.setxEbCompagnie(ebUser.getEbCompagnie().getEbCompagnieNum());
                fileReference.setxEbEtablissemnt(ebEtablissementNum);
                fileReference.setModule(module);
                fileReference.setPslEvent(pslEvent);

                SupportedMimeTypes mimeType = SupportedMimeTypes.getByFileNameOrThrow(fileName);
                fileReference.setMimeType(mimeType.getMimeType());

                if (typeUpload.getCode().equals(UploadDirectory.CUSTOM.getCode())) {
                    fileReference.setxEbCustomDeclaration(id);

                    EbCustomDeclaration dec = ebCustomDeclarationRepository.findById(id).orElse(null);

                    if (dec != null) {
                        int countDocs = dec.getAttachedFilesCount() != null ? dec.getAttachedFilesCount() : 0;
                        countDocs++;
                        dec.setAttachedFilesCount(countDocs);
                        ebCustomDeclarationRepository.save(dec);
                    }

                }

                fileReference = ebDemandeFichiersJointRepositorty.save(fileReference);

                resultMap.put("ebChat", ebChat);
                resultMap.put("fichier", fileReference);

                result = resultMap;
            }

            // Special treatements
            switch (typeUpload) {
                case PRICING_BOOKING:
                    if (numEntity != null) {
                        // gestion des status transport management
                        Optional<EbDemande> ebDemande = ebDemandeRepository.findById(numEntity);

                        if (ebDemande.isPresent()) {

                            if (fileTypeDemande.equals(Enumeration.FileType.TRANSPORT_DOCUMENT.getCode())) {
                                ebDemande.get().setFlagDocumentTransporteur(true);
                                ebDemandeRepository.updateFlagDocumentTransporteur(numEntity, true);
                            }
                            else if (fileTypeDemande.equals(Enumeration.FileType.CUSTOMS.getCode())) {
                                ebDemande.get().setFlagDocumentCustoms(true);
                                ebDemandeRepository.updateFlagDocumentCustoms(numEntity, true);
                            }

                        }

                    }
                    break;

                case FREIGHT_AUDIT:
                    // numEntity (InvoiceNumber) isn't set (set to 0) when
                    // uploading from the actionBar
                    // in Freight audit :
                    // https://mytower-recette.adias.fr/app/freight-audit
                    if (numEntity != null && numEntity != 0) ebInvoiceRepository.updateUploadFileNbr(numEntity);
                    break;

                case BULK_IMPORT:
                    Map<String, Object> res = pricingService
                        .getTaskProgress("-", file.getInputStream(), fileName, ebUserNum);
                    resultMap.put("ident", res.get("ident"));
                    result = resultMap;
                    break;

                default:
                    break;
            }

        } catch (IOException e) {
            throw new StorageException("Failed to store file " + file.getOriginalFilename(), e);
        }

        return result;
    }

    private EbCompagnie getEbCompagnieByIdFileAndModule(
        Integer id,
        Integer module,
        Integer ebEtablissementNum,
        EbUser ebUser,
        UploadDirectory typeUpload) {
        EbCompagnie ebCompagnie = null;
        boolean found = true;

        if (ebUser.isControlTower() || ebUser.isPrestataire()) {

            if (module != null && id != null) {

                if (Enumeration.Module.PRICING.getCode().equals(module)
                    || Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(module)) {
                    ebCompagnie = ebDemandeRepository.findEbCompagnieChargeurByEbDemandeNum(id);
                }
                else if (Enumeration.Module.TRACK.getCode().equals(module)) {
                    ebCompagnie = ebEbTrackTraceRepository.findEbCompagnieChargeurByEbDemandeNum(id);
                }
                else if (Enumeration.Module.FREIGHT_AUDIT.getCode().equals(module)) {
                    ebCompagnie = ebInvoiceRepository.findEbCompagnieChargeurByEbInvoiceNum(id);
                }
                else {
                    found = false;
                }

            }
            else if (ebEtablissementNum != null) {
                Integer ebCompagnieNum = ebEtablissementRepository
                    .selectNumCompagnieByEtablissement(ebEtablissementNum);
                ebCompagnie = new EbCompagnie(ebCompagnieNum);
            }
            else {
                found = false;
            }

        }
        else found = false;

        if (!found) {
            System.err
                .println(
                    "Compagnie non trouvé pour cet upload: {id: " + id + ", module: " + module + ", file: "
                        + typeUpload.getDirectory() + "} -- La compagnie de l'user connecté sera utilisée.");
            ebCompagnie = ebUser.getEbCompagnie();
        }

        return ebCompagnie;
    }
}
