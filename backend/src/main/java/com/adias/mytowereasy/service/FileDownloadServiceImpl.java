package com.adias.mytowereasy.service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.jasperreports.JasperReportsUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.adias.mytowereasy.cptm.model.SearchCriteriaCPTM;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaDelivery;
import com.adias.mytowereasy.delivery.util.search.SearchCriteriaOrder;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.Enumeration.Module;
import com.adias.mytowereasy.model.Enumeration.UploadDirectory;
import com.adias.mytowereasy.properties.FrontProperties;
import com.adias.mytowereasy.service.storage.MyFileUtils;
import com.adias.mytowereasy.util.ExcelUtils;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaCustom;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;
import com.adias.mytowereasy.utils.search.SearchCriteriaTrackTrace;

import fr.mytower.lib.storage.api.StorageProvider;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.fill.JRSwapFileVirtualizer;
import net.sf.jasperreports.engine.util.JRSwapFile;
import net.sf.jasperreports.engine.xml.JRXmlLoader;


@Service
public class FileDownloadServiceImpl extends MyTowerService implements FileDownloadService {
    public static Long EXTRACTION_LIMITE = 1_000_000l;
    private static final Logger logger = LoggerFactory.getLogger(FileDownloadServiceImpl.class);

    public static Map<String, FileDownloadStateAndResult> DownloadStates = new HashMap<String, FileDownloadStateAndResult>();

    @Autowired
    private StorageProvider storageProvider;

    @Autowired
    FrontProperties frontProperties;

    private SXSSFWorkbook workbook;
    private CellStyle boldCellStyle;
    private CellStyle cellBorderStyle;
    private CellStyle dateCellStyle;
    CellStyle enteteCellStyle;
    CellStyle lignesCellStyle;

    void initWorkbook() {
        workbook = new SXSSFWorkbook();

        // border color

        XSSFColor borderColor = new XSSFColor(new java.awt.Color(128, 128, 128));
        short palIndex = borderColor.getIndex();
        // Cell style
        cellBorderStyle = workbook.createCellStyle();
        cellBorderStyle.setBorderBottom(BorderStyle.THIN);
        cellBorderStyle.setBorderTop(BorderStyle.THIN);
        cellBorderStyle.setBorderRight(BorderStyle.THIN);
        cellBorderStyle.setBorderLeft(BorderStyle.THIN);

        // settings borders color
        cellBorderStyle.setBottomBorderColor(palIndex);
        cellBorderStyle.setTopBorderColor(palIndex);
        cellBorderStyle.setLeftBorderColor(palIndex);
        cellBorderStyle.setRightBorderColor(palIndex);

        Font fontArial = workbook.createFont();
        fontArial.setFontName(HSSFFont.FONT_ARIAL);
        fontArial.setFontHeightInPoints((short) 10);
        cellBorderStyle.setFont(fontArial);

        // Bold Cell style
        boldCellStyle = workbook.createCellStyle();
        boldCellStyle.setBorderBottom(BorderStyle.THIN);
        boldCellStyle.setBorderTop(BorderStyle.THIN);
        boldCellStyle.setBorderRight(BorderStyle.THIN);
        boldCellStyle.setBorderLeft(BorderStyle.THIN);

        // settings borders color
        boldCellStyle.setBottomBorderColor(palIndex);
        boldCellStyle.setTopBorderColor(palIndex);
        boldCellStyle.setLeftBorderColor(palIndex);
        boldCellStyle.setRightBorderColor(palIndex);

        Font fontArialBold = workbook.createFont();
        fontArialBold.setFontName(HSSFFont.FONT_ARIAL);
        fontArialBold.setFontHeightInPoints((short) 10);
        fontArialBold.setBold(true);
        boldCellStyle.setFont(fontArialBold);

        // Cell Colord
        enteteCellStyle = workbook.createCellStyle();
        enteteCellStyle.setBorderBottom(BorderStyle.THIN);
        enteteCellStyle.setBorderTop(BorderStyle.THIN);
        enteteCellStyle.setBorderRight(BorderStyle.THIN);
        enteteCellStyle.setBorderLeft(BorderStyle.THIN);

        // settings borders color
        enteteCellStyle.setBottomBorderColor(palIndex);
        enteteCellStyle.setTopBorderColor(palIndex);
        enteteCellStyle.setLeftBorderColor(palIndex);
        enteteCellStyle.setRightBorderColor(palIndex);
        enteteCellStyle.setFillForegroundColor(IndexedColors.BLUE.getIndex());
        enteteCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        enteteCellStyle.setAlignment(HorizontalAlignment.CENTER);

        // Cell Colord
        lignesCellStyle = workbook.createCellStyle();
        lignesCellStyle.setBorderBottom(BorderStyle.THIN);
        lignesCellStyle.setBorderTop(BorderStyle.THIN);
        lignesCellStyle.setBorderRight(BorderStyle.THIN);
        lignesCellStyle.setBorderLeft(BorderStyle.THIN);

        // settings borders color
        lignesCellStyle.setBottomBorderColor(palIndex);
        lignesCellStyle.setTopBorderColor(palIndex);
        lignesCellStyle.setLeftBorderColor(palIndex);
        lignesCellStyle.setRightBorderColor(palIndex);
        lignesCellStyle.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
        lignesCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        lignesCellStyle.setAlignment(HorizontalAlignment.CENTER);

        // Date Cell Style
        dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setBorderBottom(BorderStyle.THIN);
        dateCellStyle.setBorderTop(BorderStyle.THIN);
        dateCellStyle.setBorderLeft(BorderStyle.THIN);
        dateCellStyle.setBorderRight(BorderStyle.THIN);

        // Date Cell settings borders color
        dateCellStyle.setBottomBorderColor(palIndex);
        dateCellStyle.setTopBorderColor(palIndex);
        dateCellStyle.setLeftBorderColor(palIndex);
        dateCellStyle.setRightBorderColor(palIndex);
        dateCellStyle.setFont(fontArial);
    }

    private void autoSizeColumns() {
        int numberOfSheets = workbook.getNumberOfSheets();

        for (int i = 0; i < numberOfSheets; i++) {
            Sheet sheet = workbook.getSheetAt(i);

            if (sheet.getPhysicalNumberOfRows() > 0) {
                Row row = sheet.getRow(sheet.getFirstRowNum());
                Iterator<Cell> cellIterator = row.cellIterator();

                while (cellIterator.hasNext()) {
                    Cell cell = cellIterator.next();
                    int columnIndex = cell.getColumnIndex();
                    sheet.autoSizeColumn(columnIndex);
                }

            }

        }

    }

    @Override
    @Async
    public void exportData(@RequestBody Map<String, Object> criteria, HttpServletResponse response) throws Exception {

        try {
            this.initWorkbook();
            Map<String, Integer> cfg = new LinkedHashMap<String, Integer>();
            SXSSFSheet sheet = null;

            String downloadCode = criteria.get("username").toString();
            logger.info("Loading data started ...");

            criteria.put("size", EXTRACTION_LIMITE);

            Integer module = -1;
            String refUrl = "";
            FileDownloadStateAndResult resultAndState = loadResultMapByModuleNum(criteria, cfg);

            if (resultAndState.getState() != FileDownLoadStatus.LIMITE_DEPASSE.getCode()) {
                List<Map<String, String[]>> result = resultAndState.getListResult();
                Map<String, String> listTypeResult = resultAndState.getListTypeResult();

                for (Module mod: Module.values()) {

                    if (mod.getCode().equals(criteria.get("module"))) {
                        sheet = workbook.createSheet(mod.getLibelle());
                        module = mod.getCode();
                        refUrl = mod.getRefUrl();
                        break;
                    }

                }

                boolean generateTopHeaderColumn = isTopHeaderColumExist(module);
                List<ExcelToHeaderConfig> headerCfg = getTopHeadColumAndStyleByModule(module);

                EbUser connectedUser = connectedUserService.getCurrentUser();

                CreationHelper createHelper = workbook.getCreationHelper();
                String dateFormat = connectedUser.getDateFormatFromLocale();

                dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat(dateFormat));

                logger.info("Start building excel file");
                sheet = ExcelUtils
                    .loadSheetWithData(
                        sheet,
                        result,
                        cfg,
                        headerCfg,
                        cellBorderStyle,
                        boldCellStyle,
                        dateCellStyle,
                        generateTopHeaderColumn,
                        Optional.ofNullable(listTypeResult),
                        Optional.ofNullable(dateFormat));

                logger.info("End of building excel file");

                String relativeFilePath = getFilePathFromModuleAndCode(refUrl, downloadCode);

                // Write temporary file on running machine
                Path tmpExportFile = Files.createTempFile(null, ".xlsx");
                workbook.write(Files.newOutputStream(tmpExportFile));

                // Store generated output on real storage system
                storageProvider.put(Paths.get(relativeFilePath), tmpExportFile);

                // Clean temporary file
                Files.deleteIfExists(tmpExportFile);

                DownloadStates
                    .put(
                        downloadCode,
                        new FileDownloadStateAndResult(
                            FileDownLoadStatus.FIN.getCode(),
                            getFileName(refUrl, downloadCode),
                            null));

                logger.info("File is saved");

            }
            else {
                DownloadStates
                    .put(
                        downloadCode,
                        new FileDownloadStateAndResult(
                            FileDownLoadStatus.LIMITE_DEPASSE.getCode(),
                            getFileName(refUrl, downloadCode),
                            null));
            }

        } catch (Exception e) {
            e.printStackTrace();
            DownloadStates
                .put(
                    criteria.get("username").toString(),
                    new FileDownloadStateAndResult(FileDownLoadStatus.ERROR.getCode(), null, null));
            workbook.close();
            throw e;
        }

    }

    @Override
    public Integer getExtractionRunningCode(String code) {
        DownloadStates.put(code, new FileDownloadStateAndResult(FileDownLoadStatus.ENCOURS.getCode(), null, null));
        return FileDownLoadStatus.ENCOURS.getCode();
    }

    String getFileName(String refUrl, String code) {
        String[] codes = code.split("_");
        return refUrl + "-" + codes[1] + ".xlsx";
    }

    String getFilleFullLink(String fileName) {
        return frontProperties.getRealFileUrl(UploadDirectory.EXPORT.getDirectory() + "/" + fileName);
    }

    String getFilePathFromModuleAndCode(String refUrl, String code) {
        UploadDirectory typeUpload = UploadDirectory.EXPORT;
        String fileName = getFileName(refUrl, code);
        String filePath = MyFileUtils.resolveFilePath(typeUpload, fileName, null);
        return filePath;
    }

    @Override
    public FileDownloadStateAndResult checkExportState(@RequestBody String code) throws Exception {
        FileDownloadStateAndResult actualState = new FileDownloadStateAndResult();

        if (DownloadStates.containsKey(code)) {
            actualState = DownloadStates.get(code);

            if (DownloadStates.get(code).getState() == FileDownLoadStatus.FIN.getCode()) {
                DownloadStates.entrySet().removeIf(key -> key.equals(code));
                String fullLink = getFilleFullLink(actualState.getFileUrl());
                actualState.setFileUrl(fullLink);
            }

        }

        return actualState;
    }

    boolean isTopHeaderColumExist(Integer module) {
        boolean exist = false;

        if (module == Module.FREIGHT_AUDIT.getCode()) {
            exist = true;
        }

        return exist;
    }

    List<ExcelToHeaderConfig> getTopHeadColumAndStyleByModule(Integer module) {
        List<ExcelToHeaderConfig> headerCfg = new ArrayList<ExcelToHeaderConfig>();

        if (module == Module.FREIGHT_AUDIT.getCode()) {
            headerCfg.add(new ExcelToHeaderConfig(29, "HEADER DATA", enteteCellStyle));
            headerCfg.add(new ExcelToHeaderConfig(10, "LINE DATA", lignesCellStyle));
        }

        return headerCfg;
    }

    EbUser getConnectedUser(Map<String, Object> mapCriteria) {
        Gson gson = new GsonBuilder().setDateFormat(Statiques.DATE_FORMAT1).create();
        String strCriteria = gson.toJson(mapCriteria);
        SearchCriteria criteria = gson.fromJson(strCriteria, SearchCriteria.class);
        EbUser currentUser = connectedUserService.getCurrentUserFromDBWithCategories(criteria.getConnectedUserNum());
        return currentUser;
    }

    @SuppressWarnings("unchecked")
    public FileDownloadStateAndResult
        loadResultMapByModuleNum(Map<String, Object> mapCriteria, Map<String, Integer> cfg) throws Exception {
        logger.info("Received request to process in ProcessServiceImpl.process()");
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();

        List<EbCategorie> listCategorie = null;
        List<Map<String, String>> dtConfig = (List<Map<String, String>>) mapCriteria.get("dtConfig");
        Map<String, String> mapCustomFields = new HashMap<String, String>();
        Map<String, List<Integer>> mapCategoryFields = new HashMap<String, List<Integer>>();
        boolean extracteEntete = false;

        Integer module = (Integer) mapCriteria.get("module");;
        
        if (module == null) return null;
        else {

            if (module.equals(Module.TRACK.getCode()) ||
                module.equals(Module.PRICING.getCode()) ||
                module.equals(Module.TRANSPORT_MANAGEMENT.getCode()) ||
                module.equals(Module.FREIGHT_AUDIT.getCode()) ||
                module.equals(Module.QUALITY_MANAGEMENT.getCode()) ||
                module.equals(Module.USER_MANAGEMENT.getCode()) ||
                module.equals(Module.ORDER_MANAGEMENT.getCode()) ||
                module.equals(Module.DELIVERY_MANAGEMENT.getCode()) ||
                module.equals(Module.COMPTA_MATIERE.getCode()) ||
                module.equals(Module.CUSTOM.getCode())) {
                mapCustomFields = new HashMap<String, String>();
                mapCategoryFields = new HashMap<String, List<Integer>>();

                for (String key: mapCriteria.keySet()) {

                    if (key != null && !key.isEmpty() && mapCriteria.get(key) != null) {

                        if (key.matches("^field[0-9]+$")) {
                            mapCustomFields.put(key, (String) mapCriteria.get(key));
                        }
                        else if (key.matches("^categoryField[0-9]+$")) {
                            String tmpArrStr = gson.toJson(mapCriteria.get(key), new TypeToken<List<Integer>>() {
                            }.getType());
                            List<Integer> arr = gson.fromJson(tmpArrStr, new TypeToken<List<Integer>>() {
                            }.getType());
                            mapCategoryFields.put(key, arr);
                        }

                    }

                }

            }

        }

        EbUser connectedUser = connectedUserService
                .getCurrentUserFromDBWithCategories((Integer) mapCriteria.get("connectedUserNum"));

        if (module.equals(Module.COMPLIANCE_MATRIX.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            resultAndState = filedownloadComplianceMatrixService
                .listFileDownLoadComplianceMatrix(searchCriteria, connectedUser);
        }

        else if (module.equals(Module.PRICING.getCode()) ||
            module.equals(Module.TRANSPORT_MANAGEMENT.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

                resultAndState = filedonwloadpricing
                    .listFileDownLoadPricing(dtConfig, connectedUser, listCategorie, searchCriteria);
            }
        else if (module.equals(Module.ADDITIONAL_COSTS.getCode())) {
    		String strCriteria = gson.toJson(mapCriteria);
    		SearchCriteria searchCriteria = gson.fromJson(strCriteria, SearchCriteria.class);
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            resultAndState = filedownloadAdditionalCostService
                .listFileDownLoadAdditionalCost(dtConfig, connectedUser, searchCriteria);
        }

        else if (module.equals(Module.TRACK.getCode())) {	
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            resultAndState = filedownloadTrackService
                .listFileDownLoadTracing(
                    dtConfig,
                    connectedUser,
                    listCategorie,
                    searchCriteria,
                    mapCustomFields,
                    mapCategoryFields);
        }
        else if (module.equals(Module.QUALITY_MANAGEMENT.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            resultAndState = filedownloadQualityManagementService
                .listFileDownLoadQualityManagement(
                    connectedUser,
                    dtConfig,
                    listCategorie,
                    searchCriteria,
                    mapCustomFields,
                    mapCategoryFields);
        }
        else if (module.equals(Module.CUSTOM.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            resultAndState = filedownloadCustomDeclarationService
                .listFileDownLoadCustomDeclaration(connectedUser, searchCriteria);
        }
        else if (module.equals(Module.FREIGHT_AUDIT.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            extracteEntete = true;
            resultAndState = filedownloadFreightAuditService
                .listFileDownLoadFreightAudit(
                    dtConfig,
                    connectedUser,
                    listCategorie,
                    searchCriteria,
                    mapCustomFields,
                    mapCategoryFields);
        }
        else if (module.equals(Module.COMPTA_MATIERE.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            resultAndState = filedownloadComptaMatiereService
                .listFileDownLoadComptaMatiere(dtConfig, connectedUser, searchCriteria);
        }
        else if (module.equals(Module.ORDER_MANAGEMENT.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            extracteEntete = true;
            resultAndState = filedownloadOrderManagementService
                .listFileDownLoadOrderManagement(
                    dtConfig,
                    connectedUser,
                    searchCriteria,
                    listCategorie,
                    mapCustomFields);
            logger.info("Loading data complete");
        }
        else if (module.equals(Module.DELIVERY_MANAGEMENT.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            extracteEntete = true;
            resultAndState = filedownloadDeliveryManagementService
                .listFileDownLoadDeliveryManagement(
                    dtConfig,
                    connectedUser,
                    searchCriteria,
                    listCategorie,
                    mapCustomFields);
            logger.info("Loading data complete");
        }
        else if (module.equals(Module.USER_MANAGEMENT.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            resultAndState = filedownloadUserManagementService.listFileDownLoadUserManagement(searchCriteria);
        }
        else if (module.equals(Module.ETABLISSEMENT_INFORMATION.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            resultAndState = filedownlaodEtablissementService
                .listFileDownLoadEtablissementInformation(searchCriteria, dtConfig);
        }
        else if (module.equals(Module.ORDER_LINE.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            extracteEntete = true;
            resultAndState = filedownloadOrderManagementService
                .listFileDownLoadOrderLineManagement(searchCriteria, dtConfig, connectedUser, mapCustomFields);
            logger.info("Loading data complete");
        }

        else if (module.equals(Module.LOAD_PLANNING.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            extracteEntete = true;
            resultAndState = filedownloadLoadPlanningService
                .listFileDownloadLoadPlanning(
                    searchCriteria,
                    connectedUser,
                    dtConfig,
                    listCategorie,
                    mapCustomFields,
                    mapCategoryFields);
            logger.info("Loading data complete");
        }
        else if (module.equals(Module.COSTS_ITEMS.getCode())) {
        	SearchCriteria searchCriteria = new SearchCriteria();
            searchCriteria = mapToSearchCriteriaTypeBasedOnModule(mapCriteria, mapCustomFields, mapCategoryFields, searchCriteria);

            List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();
            List<EbCost> costItems = costItemService.getListCostsItems(searchCriteria);
            Locale localLanguage = connectedUser.getLocaleFromLanguage();
            for (EbCost cost: costItems) {
                Map<String, String[]> map = new LinkedHashMap<String, String[]>();
                if (isPropertyInDtConfig(dtConfig, "code")) map.put("code", new String[]{
                    messageSource.getMessage("costItem.code", null, localLanguage), cost.getCode()
                });
                if (isPropertyInDtConfig(dtConfig, "libelle")) map.put("libelle", new String[]{
                    messageSource.getMessage("costItem.label", null, localLanguage), cost.getLibelle()
                });
                if (isPropertyInDtConfig(dtConfig, "actived")) map.put("actived", new String[]{
                    messageSource.getMessage("costItem.actived", null, localLanguage), cost.getActived().toString()
                });
                Set<String> costCategorieLabel = getCostCategorie(cost);
                if (isPropertyInDtConfig(dtConfig, "listCostCategorie")) map.put("listCostCategorie", new String[]{
                    messageSource.getMessage("costItem.cost_category", null, localLanguage),
                    costCategorieLabel.toString()
                });
                
                if (isPropertyInDtConfig(dtConfig, "listModeTransport")) {
                    String transportModeLibelle = "";

                    List<ModeTransport> listModeTransport = ModeTransport.getValues();
                    if (CollectionUtils.isNotEmpty(listModeTransport)) {
                        for (ModeTransport modeTransport: listModeTransport) {
                            if (cost.getListModeTransport().indexOf(":" + modeTransport.getCode() + ":") >= 0) {
                                if (!transportModeLibelle.isEmpty()) transportModeLibelle += ", ";
                                transportModeLibelle += modeTransport.getLibelle();
                            }
                        }
                    }

                    map.put("listModeTransport", new String[]{

                        messageSource.getMessage("costItem.transport_mode", null, localLanguage), transportModeLibelle

                    });
                }

                listresult.add(map);

            }
            resultAndState.setState(Enumeration.FileDownLoadStatus.ENCOURS.getCode());
            resultAndState.setListResult(listresult);
            logger.info("Loading data complete");
        }

        List<Map<String, String[]>> result = resultAndState.getListResult();

        if (!result.isEmpty() && dtConfig != null && !dtConfig.isEmpty()) {

            if (extracteEntete) {
                int ordre = 1;

                for (String key: result.get(0).keySet()) {
                    cfg.put(key, ordre++);
                }

            }
            else {
                boolean exists = false;

                for (Map<String, String> config: dtConfig) {

                    if (config.get("name") != null) {
                        exists = false;

                        for (String key: result.get(0).keySet()) {

                            if (key.contentEquals(config.get("name"))) {
                                exists = true;
                                cfg.put(config.get("name"), Integer.parseInt(String.valueOf(config.get("order"))));
                                break;
                            }

                        }

                        if (!exists) {
                            System.err
                                .println(
                                    "ADIAS-WARNING: " +
                                        config.get("name") +
                                        " exists in Dashboard and was required but was not found for extract.");
                        }

                    }

                }

            }

        }

        return resultAndState;
    }

	/**
	 * 
	 * @param <T>
	 * @param mapCriteria
	 * @param mapCustomFields
	 * @param mapCategoryFields
	 * @param searchCriteria
	 * @return T extends SearchCriteria
	 * 
	 *         this method is meant to map the input coming from the FrontEnd to
	 *         the appropriate searchCriteria Type (depends one the module) but with
	 *         such implementation we have to test all the export mechanism in all
	 *         modules. so it still need some work to be done here.
	 */
	private <T extends SearchCriteria> T mapToSearchCriteriaTypeBasedOnModule(Map<String, Object> mapCriteria,
			Map<String, String> mapCustomFields, Map<String, List<Integer>> mapCategoryFields, T searchCriteria) {
		String strCriteria = gson.toJson(mapCriteria);
		searchCriteria = (T) gson.fromJson(strCriteria, SearchCriteria.class);

		if (!mapCustomFields.isEmpty())
			searchCriteria.setMapCustomFields(mapCustomFields);

		if (!mapCategoryFields.isEmpty())
			searchCriteria.setMapCategoryFields(mapCategoryFields);

		if (searchCriteria.getSearchterm() != null && !searchCriteria.getSearchterm().trim().isEmpty()) {
			Map<String, String> map = new HashMap<String, String>();
			map.put("value", searchCriteria.getSearchterm());
			searchCriteria.setSearch(map);
		}
		return searchCriteria;
	}

    private Set<String> getCostCategorie(EbCost cost) {
        Set<String> costCategorieLabel = new HashSet<String>();

        if (CollectionUtils.isNotEmpty(cost.getListCostCategorie())) {
            cost.getListCostCategorie().forEach((categorie) -> {
                costCategorieLabel.add(categorie.getLibelle());
            });
        }

        return costCategorieLabel;

    }

    @Override
    public void exportFacture(
        @PathVariable("ebDestinationCountryNum") Integer ebDestinationCountryNum,
        HttpServletResponse response)
        throws Exception {
        Path reportPdfPath = Paths.get(config.tempFileLocation, "exportFacture", System.currentTimeMillis() + ".pdf");
        File reportPdf = new File(reportPdfPath.toString());
        reportPdf.getParentFile().mkdirs();
        reportPdf.createNewFile();

        try (FileOutputStream pos = new FileOutputStream(reportPdf)) {
            // Load the template
            final InputStream reportInputStream = new ClassPathResource("jasper/ficheDestinationCountry.jrxml")
                .getInputStream();
            final JasperDesign jasperDesign = JRXmlLoader.load(reportInputStream);
            final JasperReport report = JasperCompileManager.compileReport(jasperDesign);

            // Template parameters
            final Map<String, Object> params = new HashMap<>();

            SimpleDateFormat df = new SimpleDateFormat(Statiques.DATE_FORMAT);
            String currentDate = df.format(new Date());
            SearchCriteria criteria = new SearchCriteria();
            List<EbDestinationCountry> listDestinationCountry = new ArrayList<>();
            List<EbCombinaison> listCombinaison = new ArrayList<>();
            criteria.setEbDestinationCountryNum(ebDestinationCountryNum);
            EbDestinationCountry ebDestinationCountry = complianceMatrixService.selectEbDestinationCountry(criteria);

            listDestinationCountry.add(ebDestinationCountry);
            listCombinaison.addAll(ebDestinationCountry.getCombinaisons());
            JRBeanCollectionDataSource dsCombinaison = new JRBeanCollectionDataSource(listCombinaison);
            JRBeanCollectionDataSource dsDestinationCountry = new JRBeanCollectionDataSource(listDestinationCountry);
            params.put("dataSourceCombinaison", dsCombinaison);
            params.put("ficheDestinationCountry", listDestinationCountry);
            FileVirtualizer(params);

            JasperReportsUtils.renderAsPdf(report, params, dsDestinationCountry, pos);
            // TODO try to pass directly to the HttpResponse output stream
            // instead of temp file

            // Insert to response
            response.setContentType("application/pdf");
            response
                .setHeader(
                    "Content-disposition",
                    "attachment; filename=" +
                        ebDestinationCountry.getCountry().getLibelle() + " " + currentDate + ".pdf");
            OutputStream out = response.getOutputStream();
            FileInputStream in = new FileInputStream(reportPdf);
            IOUtils.copy(in, out);
            out.close();
            in.close();
        } catch (final Exception e) {
            e.printStackTrace();
        } finally {

            if (reportPdf.exists()) {
                reportPdf.delete();
            }

        }

    }

    private void FileVirtualizer(Map<String, Object> params) throws Exception {
        // Générer l'export dans un fichier temporaire dans le disque dur au
        // lieu de la mémoire pour éviter de surcharger la JVM
        JRSwapFileVirtualizer virtualizer = null;

        try {
            Path tmpFile = Paths.get(config.tempFileLocation);
            Resource resource = new UrlResource(tmpFile.toUri());
            virtualizer = new JRSwapFileVirtualizer(
                3,
                new JRSwapFile(resource.getFile().getAbsolutePath(), 2048, 1024),
                true);
            params.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    @Override
    public void extractPricing(@RequestParam Integer type, HttpServletResponse response) {
        SXSSFWorkbook workbook = new SXSSFWorkbook();
        SXSSFSheet sheet = null;
        Map<String, Integer> cfg = new LinkedHashMap<String, Integer>();

        Map<String, List<Map<String, String[]>>> globalData = null;

        try {
            List<Map<String, String[]>> result = null;

            if (!Enumeration.Extractors.GLOBAL_DATA.getCode().equals(type)) {
                if (Enumeration.Extractors.PRICING
                    .getCode().equals(type)) result = filedonwloadpricing.extractPricing();
                else if (Enumeration.Extractors.TRANSPORT_MANAGEMENT
                    .getCode().equals(type)) result = filedonwloadpricing.extractTM();
                else if (Enumeration.Extractors.INCOERENCE_STATUT_TM_TT
                    .getCode().equals(type)) result = filedonwloadpricing.extractIncoherenceStatutTMTT();
                else if (Enumeration.Extractors.INCOERENCE_STATUT_TT_PSL
                    .getCode().equals(type)) result = filedonwloadpricing.extractIncoherenceStatutTTPSL();
                else if (Enumeration.Extractors.SAVINGS
                    .getCode().equals(type)) result = filedonwloadpricing.extractSavings();

                sheet = workbook.createSheet(Enumeration.Extractors.getLibelleByCode(type));
                sheet = ExcelUtils
                    .loadSheetWithData(
                        sheet,
                        result,
                        cfg,
                        null,
                        cellBorderStyle,
                        boldCellStyle,
                        dateCellStyle,
                        false,
                        Optional.empty(),
                        Optional.empty());
            }
            else {
                List<CompletableFuture<Void>> listCmpF = new ArrayList();
                String[] fileNames = new String[]{
                    "users",
                    "pricing",
                    "transport_management",
                    "access_right",
                    "adress",
                    "categories",
                    "labels",
                    "champs_param",
                    "currencies",
                    "documents",
                    "incoterms",
                    "community",
                    "masques",
                    "dates_psl"
                };

                for (String key: fileNames) {
                    List<Map<String, String[]>> data = filedonwloadpricing.extractGlobalData(key);
                    sheet = workbook.createSheet(key);
                    cfg = new LinkedHashMap<String, Integer>();
                    sheet = ExcelUtils
                        .loadSheetWithData(
                            sheet,
                            data,
                            cfg,
                            null,
                            cellBorderStyle,
                            boldCellStyle,
                            dateCellStyle,
                            false,
                            Optional.empty(),
                            Optional.empty());
                }

                CompletableFuture.allOf(listCmpF.toArray(new CompletableFuture[0])).exceptionally(ex -> null).join();
            }

            workbook.write(response.getOutputStream());
            response.getOutputStream().flush();
            workbook.close();

            response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            response.setHeader("Content-Disposition", "attachment; filename=data.xlsx");
        } catch (Exception e) {
            e.printStackTrace();

            try {
                workbook.close();
            } catch (Exception ei) {
            }

        }

    }

    @Override
    public void setExtractionLimit(Long size) {
        EXTRACTION_LIMITE = size;
    }

    @Override
    public long getExtractionLimit() {
        return EXTRACTION_LIMITE;
    }
}
