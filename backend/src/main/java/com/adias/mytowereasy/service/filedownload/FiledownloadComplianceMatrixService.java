package com.adias.mytowereasy.service.filedownload;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface FiledownloadComplianceMatrixService {
    public FileDownloadStateAndResult
        listFileDownLoadComplianceMatrix(SearchCriteria searchCriteria, EbUser connectedUser) throws Exception;
}
