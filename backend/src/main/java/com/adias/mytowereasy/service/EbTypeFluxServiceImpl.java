package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbIncoterm;
import com.adias.mytowereasy.model.EbTypeDocuments;
import com.adias.mytowereasy.model.EbTypeFlux;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.Enumeration.TrackingLevel;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Service
public class EbTypeFluxServiceImpl extends MyTowerService implements EbTypeFluxService {
    @Override
    public List<EbTypeFlux> getListTypeFlux(SearchCriteria criteria) {
        return daoEbTypeFlux.getListTypeFlux(criteria);
    }

    @Override
    public Long getListTypeFluxCount(SearchCriteria criteria) {
        return daoEbTypeFlux.getListTypeFluxCount(criteria);
    }

    @Override
    public EbTypeFlux updateTypeFlux(EbTypeFlux typeFlux) {
        return ebTypeFluxRepository.save(typeFlux);
    }

    @Override
    public Boolean deleteTypeFlux(Integer Id) {
        EbTypeFlux typeFlux = ebTypeFluxRepository.getOne(Id);
        typeFlux.setIsDeleted(true);
        ebTypeFluxRepository.save(typeFlux);
        return true;
    }

    @Override
    public void generateTypeOfFluxServiceByCompanyNum(List<Integer> listEbCompagnieNum) throws Exception {
        SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();

        for (Integer ebCompagnieNum: listEbCompagnieNum) {
            // Recupération des données liées à la compagnie
            EbCompagnie compagnie = ebCompagnieRepository.findOneByEbCompagnieNum(ebCompagnieNum);
            String listTypeDocuments = getListTypeDocumentsByCompagnie(ebCompagnieNum);
            String listModeTransport = getListMeansOfTransport();

            // Verification si le type de flux exist, sinon on crée un pour le
            // compagnie
            EbTypeFlux typeFlux;
            List<EbTypeFlux> listTypeOfFlux = ebTypeFluxRepository.selectByCompagnieNum(ebCompagnieNum);

            if (!listTypeOfFlux.isEmpty()) {
                typeFlux = listTypeOfFlux.get(0);
            }
            else {
                typeFlux = new EbTypeFlux();
                typeFlux.setDesignation("Flux transport");
                typeFlux.setCode("Transport");
                typeFlux.setListEbTypeDocuments(listTypeDocuments);
                typeFlux.setListModeTransport(listModeTransport);
                typeFlux.setxEbCompagnie(compagnie);
                typeFlux.setTrackingLevel(TrackingLevel.UNITS.getCode());
                typeFlux = ebTypeFluxRepository.save(typeFlux);
            }

            String listIncoterms = typeFlux.getListEbIncoterm() != null ? typeFlux.getListEbIncoterm() : "";
            String listSchemaPsl = typeFlux.getListEbTtSchemaPsl() != null ? typeFlux.getListEbTtSchemaPsl() : "";

            criteria.setEbCompagnieNum(ebCompagnieNum);
            List<EbDemande> listDemande = ebDemandeRepository.selectAllDemandeByCompagnie(ebCompagnieNum);

            for (EbDemande demande: listDemande) {

                if (demande.getxEbSchemaPsl() != null) {
                    EbTtSchemaPsl schemaPsl = demande.getxEbSchemaPsl();
                    Integer incotermNum = getEbincotermNumByLibelle(demande.getxEcIncotermLibelle(), compagnie);
                    // Affectation du type de flux à la demande
                    ebDemandeRepository
                        .updateEbDemandeTypeOfFluxAndIncoterm(
                            typeFlux.getEbTypeFluxNum(),
                            incotermNum,
                            demande.getEbDemandeNum());
                }

            }

            // Mise à jour du type de flux
            List<EbIncoterm> listEbIncoterm = ebIncotermRepository.findByXEbCompagnie_ebCompagnieNum(ebCompagnieNum);

            List<EbTtSchemaPsl> listTtSchemaPsl = ebTtSchemaPslRepository
                .findByXEbCompagnie_ebCompagnieNum(ebCompagnieNum);

            for (EbIncoterm it: listEbIncoterm) {
                if (!listIncoterms
                    .contains(":" + it.getEbIncotermNum() + ":")) listIncoterms += ":" + it.getEbIncotermNum() + ":";
            }

            for (EbTtSchemaPsl it: listTtSchemaPsl) {
                if (!listSchemaPsl
                    .contains(
                        ":" + it.getEbTtSchemaPslNum() + ":")) listSchemaPsl += ":" + it.getEbTtSchemaPslNum() + ":";
            }

            typeFlux.setListEbIncoterm(listIncoterms);
            typeFlux.setListEbTtSchemaPsl(listSchemaPsl);
            ebTypeFluxRepository.save(typeFlux);
        }

    }

    String getListMeansOfTransport() {
        String listModeTransportCode = "";
        List<Integer> listModeTransport = ModeTransport.getListModeTransportCode();

        for (Integer code: listModeTransport) {
            listModeTransportCode += ":" + code + ":";
        }

        return listModeTransportCode;
    }

    String getListTypeDocumentsByCompagnie(Integer ebCompagnieNum) {
        List<Integer> listEbCompagnieNum = new ArrayList<Integer>();
        listEbCompagnieNum.add(ebCompagnieNum);
        List<EbTypeDocuments> listTypeDocuments = ebTypeDocumentsRepository
            .findByListEbCompagnieNum(listEbCompagnieNum);

        String listTypeDocumentsNum = "";

        for (EbTypeDocuments typeDocument: listTypeDocuments) {
            listTypeDocumentsNum += ":" + typeDocument.getEbTypeDocumentsNum() + ":";
        }

        return listTypeDocumentsNum;
    }

    Integer getEbincotermNumByLibelle(String libelle, EbCompagnie compagnie) {
        EbIncoterm incoterm;
        List<EbIncoterm> listEbIncoterm = ebIncotermRepository
            .findOneByLibelleAndCompagnie(libelle, compagnie.getEbCompagnieNum());

        if (!listEbIncoterm.isEmpty()) {
            incoterm = listEbIncoterm.get(0);
        }
        else {
            incoterm = new EbIncoterm();
            incoterm.setLibelle(libelle);
            incoterm.setCode(libelle);
            incoterm.setxEbCompagnie(compagnie);
            ebIncotermRepository.save(incoterm);
        }

        return incoterm.getEbIncotermNum();
    }

    @Override
    public List<EbTypeFlux> getListTypeFluxByEbCompagnieNum(Integer ebCompagnieNum) {
        return this.ebTypeFluxRepository.selectByCompagnieNum(ebCompagnieNum);
    }
}
