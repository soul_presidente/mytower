package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.EbIncoterm;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface EbIncotermService {
    List<EbIncoterm> getListIncoterms(SearchCriteria criteria);

    Long getListIncotermsCount(SearchCriteria criteria);

    EbIncoterm updateIncoterm(EbIncoterm typeFlux);

    Boolean deleteIncoterm(Integer Id);
}
