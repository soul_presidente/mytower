/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.model.EbEmailHistorique;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.qm.*;
import com.adias.mytowereasy.util.email.Email;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


public interface QualityManagementService {
    public List<EbQmIncident> getAllEbIncident(SearchCriteriaQM criteria);

    public int getCountListEbIncident(SearchCriteriaQM criteria) throws Exception;

    public List<EbQmIncident> getListEbIncident(SearchCriteriaQM criterias);

    public List<EbQmCategory> listCategory();

    public List<EbQmQualification> listQmQualification();

    public List<EbQmRootCause> listQmRootCause();

    public List<EbQmCloseReason> listQmCloseReason();

    public Map<Integer, Map<String, Object>>
        saveIncidents(List<EbQmIncident> incidents, Boolean isUpdate, EbUser connectedUser);

    public EbQmIncident getEbIncident(SearchCriteriaQM criteria);

    public List<EbMarchandise> listMarchandise(SearchCriteriaQM criteria);

    public SemiAutoMail getSemiAutoMail(SearchCriteriaQM criteria);

    public EbEmailHistorique sendMail(Email mailToSend, Integer incidentNum);

    public List<EbQmIncidentLine> updateListIncidentLine(List<EbQmIncidentLine> listIncidentLine);

    public int updateIncidentComment(Integer ebQmIncidentNum, String etablissementComment, String thirdPartyComment);

    public int updateIncidentDescription(Integer ebQmIncidentNum, String incidentDesc, String addImpact);

    public int updateIncidentShipment(EbQmIncident incident);

    public int updateIncident(EbQmIncident incident);

    void historizeQmAction(
        Integer idFiche,
        Integer chatCompId,
        Integer module,
        String action,
        String username,
        String usernameFor);

    List<EbQmIncident> getListIncidentRef(String term) throws JsonProcessingException;

    Integer getNextEbQmIncidentNum();

    EbQmIncident getListDoc(Integer incidentNum);

    EbQmIncident saveFlags(Integer ebQmIncidentNum, String newFlags);

    EbQmRootCause saveRootCause(EbQmRootCause rootCause);

    Long countListMarchandises(SearchCriteriaQM criterias);

    int updateEventTypeStatus(Integer ebQmIncidentListNum, Integer iStatus);
}
