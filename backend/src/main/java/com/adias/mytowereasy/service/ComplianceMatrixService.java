/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.dto.EbDestinationCountryDTO;
import com.adias.mytowereasy.model.EbCombinaison;
import com.adias.mytowereasy.model.EbDestinationCountry;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface ComplianceMatrixService {
    public EbDestinationCountry selectEbDestinationCountry(SearchCriteria criteria);

    public EbDestinationCountry insertEbDestinationCountry(EbDestinationCountry ebDestinationCountry);

    public List<EbDestinationCountryDTO> selectListEbDestinationCountry(SearchCriteria criteria);

    public EbDestinationCountry updateEbDestinationCountry(EbDestinationCountry ebDestinationCountry);

    public Boolean deleteEbDestinationCountry(EbDestinationCountry ebDestinationCountry);

    public EbCombinaison insertEbCombinaison(EbCombinaison ebCombinaison);

    public void deleteEbCombinaison(EbCombinaison ebCombinaison);

    public List<EbCombinaison> selectListEbCombinaison(SearchCriteria criteria);

    public List<EbDestinationCountryDTO> selectListEbDestinationCountryByCompany(SearchCriteria criterias);
}
