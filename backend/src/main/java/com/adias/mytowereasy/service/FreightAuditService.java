/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbDemandeFichiersJoint;
import com.adias.mytowereasy.model.EbInvoice;
import com.adias.mytowereasy.model.InvoiceWrapper;
import com.adias.mytowereasy.utils.search.SearchCriteriaFreightAudit;


public interface FreightAuditService {
    EbInvoice insertEbInvoice(EbInvoice ebInvoice);

    List<EbInvoice> getListEbInvoice(SearchCriteriaFreightAudit criterias);

    EbInvoice updateEbInvoice(EbInvoice ebInvoice);

    int getCountListEbInvoice(SearchCriteriaFreightAudit criteria);

    List<EbDemandeFichiersJoint> getListEbInvoiceByEbInvoiceNum(Integer ebInvoiceNum);

    void preInsertInvoice(EbDemande ebDemand);

    Integer deleteFile(Integer ebFichierJointNum);

    EbDemande getDemande(Integer ebDemandeNum);

    Integer deleteNbr(Integer ebInvoiceNum);

    Integer setTag(EbDemandeFichiersJoint inv);

    int generateInvoice(Integer ebDemandNum);

    Integer generateAllInvoice();

    public EbInvoice updateListCostCategorieAndPrice(EbInvoice ebInvoice);

    public EbInvoice saveFlags(Integer ebInvoiceNum, String newFlags);

    int actionInvoice(InvoiceWrapper invoiceWrap);

    List getListStatus();

    EbInvoice updateListTypeDocuments(EbInvoice ebInvoice);
}
