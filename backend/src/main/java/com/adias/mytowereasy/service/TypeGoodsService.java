package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.model.EbTypeGoods;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface TypeGoodsService {
    public EbTypeGoods saveTypeGoods(EbTypeGoods ebTypeGood);

    public Boolean deleteTypeGoods(Integer ebTypeGoodsNum);

    public List<EbTypeGoods> listTypeGoods(SearchCriteria criteria);

    public Long countListTypeGoods(SearchCriteria criteria);

    List<EbTypeGoods> listWithCommunity();
}
