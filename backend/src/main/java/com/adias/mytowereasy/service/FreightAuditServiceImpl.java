/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.ActionFreightAudit;
import com.adias.mytowereasy.model.Enumeration.IDChatComponent;
import com.adias.mytowereasy.model.Enumeration.Module;
import com.adias.mytowereasy.model.Enumeration.Role;
import com.adias.mytowereasy.model.FAEnumeration.InvoiceStatus;
import com.adias.mytowereasy.model.enums.Modules;
import com.adias.mytowereasy.util.JsonUtils;
import com.adias.mytowereasy.utils.search.SearchCriteriaFreightAudit;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Service
public class FreightAuditServiceImpl extends MyTowerService implements FreightAuditService, CommonFlagSevice {
    @Autowired
    ConnectedUserService connectedUserService;
    @Autowired
    ListStatiqueService listStatiqueService;
    @Autowired
    private MessageSource messageSource;
    @Autowired
    FlagService flagService;

    @Transactional
    @Override
    public EbInvoice insertEbInvoice(EbInvoice ebInvoice) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        Integer ebInvoiceNum = daoInvoice.nextValEbInvoice();
        ebInvoice.setEbInvoiceNum(ebInvoiceNum);
        ebInvoice.setRef("FA_" + ebInvoiceNum);

        if (ebInvoice.getCurrency() != null) if (ebInvoice.getCurrency().getEcCurrencyNum() != null) {
            EcCurrency currency = new EcCurrency();
            currency = ecCurrencyRepository.findById(ebInvoice.getCurrency().getEcCurrencyNum()).get();
            ebInvoice.setCurrencyLabel(currency.getLibelle());
        }

        ebInvoice.setStatut(InvoiceStatus.WAITING_FOR_CONFIRMATION.getCode());

        List<EbTypeDocuments> listTypeDoc = this.ebTypeDocumentsRepository
            .findByXEbCompagnie(connectedUser.getEbCompagnie().getEbCompagnieNum());

        if (listTypeDoc != null) {
            List<EbTypeDocuments> listTypeDocs = listTypeDoc
                .stream()
                .filter(
                    typeDoc -> typeDoc.getModule() != null
                        && typeDoc.getModule().contains(Enumeration.Module.FREIGHT_AUDIT.getCode()))
                .collect(Collectors.toList());
            ebInvoice.setListTypeDocuments(listTypeDocs);
        }

        ebInvoiceRepository.save(ebInvoice);
        return ebInvoice;
    }

    @Override
    public List<EbInvoice> getListEbInvoice(SearchCriteriaFreightAudit criterias) {
        List<EbInvoice> listEbInvoice = daoInvoice.getListEbInvoice(criterias);
        listEbInvoice.forEach(inv -> {
            String listFlagIcon = inv.getListFlag() != null ? inv.getListFlag().trim() : null;

            if (listFlagIcon != null && !listFlagIcon.isEmpty()) {
                inv.setListEbFlagDTO(EbFlagDTO.sortByName(flagService.getListFlagDTOFromCodes(listFlagIcon)));
            }

        });

        if (!listEbInvoice.isEmpty()) {
            listEbInvoice.forEach(inv -> {

                if (inv != null && inv.getxEbDemande() != null && inv.getxEbDemande().getListTypeDocuments() != null) {
                    inv
                        .getxEbDemande().setListTypeDocuments(
                            inv
                                .getxEbDemande().getListTypeDocuments().stream()
                                .filter(
                                    it -> (it != null && it.getModule() != null)
                                        && it.getModule().contains(Modules.FREIGHT_AUDIT.getValue()))
                                .collect(Collectors.toList()));
                }

            });
        }

        return listEbInvoice;
    }

    @Transactional
    @Override
    public EbInvoice updateEbInvoice(EbInvoice ebInvoice) {
        return ebInvoiceRepository.save(ebInvoice);
    }

    @Override
    public int getCountListEbInvoice(SearchCriteriaFreightAudit criteria) {
        long count = daoInvoice.getCountListEbInvoice(criteria);

        return (int) count;
    }

    @Override
    public List<EbDemandeFichiersJoint> getListEbInvoiceByEbInvoiceNum(Integer ebInvoiceNum) {
        return ebInvoiceRepository.listEbInvoice_ebInvoiceNum(ebInvoiceNum);
    }

    public List<EbInvoice> updateListInvoice(List<EbInvoice> listInvoice) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void preInsertInvoice(@NonNull EbDemande ebDemande) {
        EbInvoice ebInvoiceTest = ebInvoiceRepository.findEbInvoiceByEbDemandeNum(ebDemande.getEbDemandeNum());


        if (ebInvoiceTest == null) {
            SearchCriteriaPricingBooking transportCriteria = new SearchCriteriaPricingBooking();
            transportCriteria.setStatus(Enumeration.StatutDemande.WPU.getCode());
            transportCriteria.setEbDemandeNum(ebDemande.getEbDemandeNum());
            transportCriteria.setForCotation(true);

            if (ebDemande.get_exEbDemandeTransporteurFinal() == null) {
                ebDemande.set_exEbDemandeTransporteurFinal(daoPricing.getListExEbTransporteur(transportCriteria).get(0));
            }

            if (ebDemande.get_exEbDemandeTransporteurFinal() != null) {
                /*
                 * * carrier (pour le chargeur et le user TDC)
                 * Type demand
                 * Real pickup up date (mise à jour dans le TT)
                 * Delivery date
                 */
                /*
                 * correction du bug concerant la creation de FA d'un demande
                 * confirme
                 * il reste la test de genere la facteur par url
                 */
                // if
                // (ebDemande.get_exEbDemandeTransporteurFinal().getxEbCompagnie()
                // != null)
                // {
                // List<ExEbDemandeTransporteur> listDemandeTrans = daoPricing
                // .getListExEbTransporteur(transportCriteria);
                // //
                // ebDemande.set_exEbDemandeTransporteurFinal(listDemandeTrans.get(0));
                //
                // }
                EbInvoice ebInvoice = new EbInvoice(ebDemande);

                ebInvoice.setUploadFileNbr(0);
                insertEbInvoice(ebInvoice);
            }

        } else {throw new MyTowerException(String.format("Invoice exist"));
        }

    }

    @Override
    @Transactional
    public Integer deleteFile(Integer ebFichierJointNum) {
        Integer result = 0;

        result = ebDemandeFichiersJointRepositorty.deleteFile(ebFichierJointNum);

        return result;
    }

    public EbDemande getDemande(Integer ebDemandeNum) {
        return ebDemandeRepository.getDemande(ebDemandeNum);
    }

    @Override
    @Transactional
    public Integer deleteNbr(Integer ebInvoiceNum) {
        Integer result = 0;
        result = ebInvoiceRepository.updateUploadFileNbr2(ebInvoiceNum);
        return result;
    }

    @Override
    public Integer setTag(EbDemandeFichiersJoint inv) {
        String tag = inv.getTag();
        Integer ebDemandeFichierJointNum = inv.getEbDemandeFichierJointNum();
        return ebDemandeFichiersJointRepositorty.setTag(ebDemandeFichierJointNum, tag);
    }

    @Override
    @Transactional
    public int generateInvoice(Integer ebDemandeNum) {
        Integer result = 0;

        EbDemande ebDemande = ebDemandeRepository.findOneByEbDemandeNum(ebDemandeNum);
        preInsertInvoice(ebDemande);
        result = 1;

        return result;
    }

    @Override
    public Integer generateAllInvoice() {
        List<EbDemande> listDemande = ebDemandeRepository.selectAllDemandeFromTm();

        listDemande.forEach(demande -> {
            preInsertInvoice(demande);
        });
        return 1;
    }

    @Override
    public EbInvoice updateListCostCategorieAndPrice(EbInvoice ebInvoice) {
        BigDecimal invoicePrix = null;
        String txtCom = null;
        EbChat chat = new EbChat();
        Date dateActuel = new Date();
        EbUser connectedUser = connectedUserService.getCurrentUser();
        List<EcCurrency> listCurrency = ecCurrencyRepository.findAll();
        EbInvoice oldInvoice = ebInvoiceRepository.findById(ebInvoice.getEbInvoiceNum()).orElse(null);
        List<EbCostCategorie> listCostCategorie = null, listCostCategorieOld = null;
        Locale locale = connectedUser.getLocaleFromLanguage();

        if (Role.ROLE_CHARGEUR.getCode().equals(connectedUserService.getCurrentUser().getRole())
            || Role.ROLE_CONTROL_TOWER.getCode().equals(connectedUserService.getCurrentUser().getRole())) {
            invoicePrix = ebInvoice.getApplyChargeurIvoicePrice();
            ebInvoice.setPriceArraw(0);

            if (ebInvoice.getListCostCategorieCharger() != null && !ebInvoice.getListCostCategorieCharger().isEmpty()) {
                listCostCategorie = ebInvoice.getListCostCategorieCharger();
                listCostCategorieOld = JsonUtils
                    .checkJsonListObject(oldInvoice.getListCostCategorieCharger(), EbCostCategorie.class);
            }

        }

        if (Role.ROLE_PRESTATAIRE.getCode().equals(connectedUserService.getCurrentUser().getRole())) {
            invoicePrix = ebInvoice.getApplyCarrierIvoicePrice();
            ebInvoice.setPriceArraw(0);

            if (ebInvoice.getListCostCategorieTransporteur() != null
                && !ebInvoice.getListCostCategorieTransporteur().isEmpty()) {
                listCostCategorie = ebInvoice.getListCostCategorieTransporteur();
                listCostCategorieOld = JsonUtils
                    .checkJsonListObject(oldInvoice.getListCostCategorieTransporteur(), EbCostCategorie.class);
            }

        }

        String[] args = new String[1];
        if (invoicePrix != null) args[0] = invoicePrix.toString();

        if (ebInvoice != null && ebInvoice.getListCostCategorieTransporteur() != null) {
            ebInvoice.getListCostCategorieTransporteur().forEach(costCategorie -> {
                costCategorie.getListEbCost().forEach(cost -> {
                    if (cost.getEbCostNum() != null && cost.getEuroExchangeRateReal() != null) ebCostRepository
                        .updateEuroExchangeRateReal(cost.getEbCostNum(), cost.getEuroExchangeRateReal());
                });
            });
        }

        txtCom = null;

        if (listCostCategorie != null) {

            // historize added & updated costs
            for (EbCostCategorie cc: listCostCategorie) {
                EbCostCategorie oldCc = null;
                if (listCostCategorieOld != null) oldCc = listCostCategorieOld
                    .stream().filter(it -> it.getEbCostCategorieNum().equals(cc.getEbCostCategorieNum())).findFirst()
                    .orElse(null);

                for (EbCost ct: cc.getListEbCost()) {
                    EbCost oldCt = null;
                    String name = ct.getLibelle(), oldCur = null, newCur = null, oldVal = null, newVal = null,
                        oldRate = null, newRate = null;

                    if (name == null || name.isEmpty()) name = "Untitled";

                    try {
                        oldCt = oldCc
                            .getListEbCost().stream().filter(it -> it.getEbCostNum().equals(ct.getEbCostNum()))
                            .findFirst().orElse(null);
                    } catch (Exception e) {
                    }

                    if (ct.getPriceReal() != null) newVal = ct.getPriceReal().toString();
                    if (oldCt != null && oldCt.getPriceReal() != null) oldVal = oldCt.getPriceReal().toString();

                    try {
                        newCur = listCurrency
                            .stream().filter(it -> it.getEcCurrencyNum().equals(ct.getxEcCurrencyNum())).findFirst()
                            .orElse(null).getCode();
                    } catch (Exception e) {
                    }

                    try {
                        final EbCost tmpOldCt = oldCt;
                        oldCur = listCurrency
                            .stream().filter(it -> it.getEcCurrencyNum().equals(tmpOldCt.getxEcCurrencyNum()))
                            .findFirst().orElse(null).getCode();
                    } catch (Exception e) {
                    }

                    if (ct.getEuroExchangeRateReal() != null) newRate = ct.getEuroExchangeRateReal().toString();
                    if (oldCt != null && oldCt.getEuroExchangeRateReal() != null) oldRate = oldCt
                        .getEuroExchangeRateReal().toString();

                    if (oldVal == null) oldVal = " - ";
                    if (newVal == null) newVal = " - ";
                    if (oldRate == null) oldRate = " - ";
                    if (newRate == null) newRate = " - ";
                    if (oldCur == null) oldCur = " - ";
                    if (newCur == null) newCur = " - ";

                    if (!newVal.equals(oldVal) || !newCur.equals(oldCur) || !newRate.equals(oldRate)) {
                        String line = "";
                        if (newCur != oldCur) line += "<ul><li>" +
                            messageSource.getMessage("freight_audit.chat.cost_currency", null, locale) + ":" + oldCur +
                            " " + newCur + "</ul></li>";

                        if (newRate != oldRate) {
                            if(oldCt != null)
                                line += "<br/>";
                            line += (!line.isEmpty() ? "<ul><li>" : "") +
                                messageSource.getMessage("freight_audit.chat.old_rate", null, locale) + "(" + oldRate +
                                "), " + messageSource.getMessage("freight_audit.chat.new_rate", null, locale) + "(" +
                                newRate + ")" + "</ul></li>";
                        }

                        if (newVal != oldVal) {
                            line += (!line.isEmpty() ? "<ul><li> " : "") +
                                messageSource.getMessage("freight_audit.chat.old_price", null, locale) + "(" + oldVal +
                                "), " + messageSource.getMessage("freight_audit.chat.new_price", null, locale) + "(" +
                                newVal + ")" + "</ul></li>";
                        }

                        if (oldCt == null) line += " <b>" +
                            messageSource.getMessage("freight_audit.chat.cost_added", null, locale) + "</b>";
                        else line += " <b>" +
                            messageSource.getMessage("freight_audit.chat.cost_updated", null, locale) + "</b>";

                        if (!line.isEmpty()) {
                            line = "<b>" + name + "</b>: " + line;
                            if (txtCom == null) txtCom = "";
                            txtCom += line + "<br>";
                        }

                    }

                }

            }

            // historize deleted costs
            if (listCostCategorieOld != null) {

                for (EbCostCategorie oldCc: listCostCategorieOld) {
                    EbCostCategorie cc = listCostCategorie
                        .stream().filter(it -> it.getEbCostCategorieNum().equals(oldCc.getEbCostCategorieNum()))
                        .findFirst().orElse(null);

                    if (cc != null) {

                        for (EbCost oldCt: oldCc.getListEbCost()) {
                            final EbCost tmpOldCt = oldCt;
                            boolean ctOpt = cc.getListEbCost().stream().filter(it -> {

                                if (it.getEbCostNum() != null && tmpOldCt.getEbCostNum() != null) {
                                    return it.getEbCostNum().equals(tmpOldCt.getEbCostNum());
                                }
                                else return false;

                            }).findFirst().isPresent();

                            if (!ctOpt) {
                                String name = oldCt.getLibelle(), oldCur = null, oldVal = null, oldRate = null;

                                if (name == null || name.isEmpty()) name = "Untitled";

                                try {
                                    oldCt = oldCc
                                        .getListEbCost().stream()
                                        .filter(it -> it.getEbCostNum().equals(tmpOldCt.getEbCostNum())).findFirst()
                                        .orElse(null);
                                } catch (Exception e) {
                                }

                                if (oldCt.getPriceReal() != null) oldVal = oldCt.getPriceReal().toString();

                                try {
                                    oldCur = listCurrency
                                        .stream()
                                        .filter(it -> it.getEcCurrencyNum().equals(tmpOldCt.getxEcCurrencyNum()))
                                        .findFirst().orElse(null).getCode();
                                } catch (Exception e) {
                                }

                                if (oldCt.getEuroExchangeRateReal() != null) oldRate = oldCt
                                    .getEuroExchangeRateReal().toString();

                                if (oldVal != null || oldCur != null || oldRate != null) {
                                    if (oldVal == null) oldVal = " - ";
                                    if (oldRate == null) oldRate = " - ";
                                    if (oldCur == null) oldCur = " - ";

                                    String line = "";
                                    if (oldCur != null) line += oldCur;

                                    if (oldRate != null) {
                                        line += (!line.isEmpty() ? ", " : "") + "taux (" + oldRate + ")";
                                    }

                                    if (oldVal != null) {
                                        line += (!line.isEmpty() ? ", " : "") + "prix (" + oldVal + ")";
                                    }

                                    line += " <b>[Deleted]</b>";

                                    if (!line.isEmpty()) {
                                        line = "<b>" + name + "</b>: " + line;
                                        if (txtCom == null) txtCom = "";
                                        txtCom += line + "<br>";
                                    }

                                }

                            }

                        }

                    }

                }

            }

        }
        else {
            txtCom = messageSource.getMessage("frieght.prixapplique", args, Locale.FRANCE);
        }

        ebInvoice.setxEbDemande(null);

        if (txtCom != null) {
            chat.setIdChatComponent(IDChatComponent.FREIGHT_AUDIT.getCode());
            chat.setModule(Module.FREIGHT_AUDIT.getCode());
            chat.setUserName(connectedUser.getNomPrenom());
            chat.setDateCreation(dateActuel);
            chat.setText(txtCom);
            chat.setIdFiche(ebInvoice.getEbInvoiceNum());
            listStatiqueService.addChat(chat);
        }

        return ebInvoiceRepository.save(ebInvoice);
    }

    @Override
    public EbInvoice updateListTypeDocuments(EbInvoice ebInvoice) {
        return ebInvoiceRepository.save(ebInvoice);
    }

    @Override
    public EbInvoice saveFlags(Integer ebInvoiceNum, String newFlags) {
        EbInvoice invoice = ebInvoiceRepository.findById(ebInvoiceNum).get();
        invoice.setListFlag(newFlags);
        return ebInvoiceRepository.save(invoice);
    }

    @Transactional
    @Override
    public int actionInvoice(InvoiceWrapper invoiceWraper) {
        Integer result = 0;
        EbInvoice invoiceCopy = new EbInvoice();
        String txtCom = null;
        String dateCom = null;
        Date invoiceDate = null;
        Integer invoiceStatut = null;
        String invoiceNumber = null, invoiceMemo = null;
        BigDecimal invoicePrix = null;
        EbChat chat = new EbChat();
        EbUser connectedUser = connectedUserService.getCurrentUser();
        Date dateActuel = new Date();
        List<EbChat> listChat = new ArrayList<EbChat>();
        List<EbInvoice> listUpdateInvoice = new ArrayList<EbInvoice>();
        BigDecimal gapConvertBigDecimal = null;
        BigDecimal gapDouble = null;

        Locale lcl = new Locale(connectedUser.getLangageUser());

        if (invoiceWraper.getlistEbInvoiceNumber() != null && !invoiceWraper.getlistEbInvoiceNumber().isEmpty()) {

            for (Integer numberInvoice: invoiceWraper.getlistEbInvoiceNumber()) {
                invoiceCopy = new EbInvoice();
                invoiceCopy = ebInvoiceRepository.getOne(numberInvoice);

                if (invoiceWraper.getLisAction() != null && !invoiceWraper.getLisAction().isEmpty()) {

                    for (Integer action: invoiceWraper.getLisAction()) {

                        if (ActionFreightAudit.APPLYLISTINVOICEPRICE.getCode().equals(action)
                            && invoiceWraper.getEbInvoice().getApplyCarrierIvoicePrice().toString() != null) {

                            if (connectedUser.isChargeur() || connectedUser.isControlTower()) {
                                invoiceCopy
                                    .setApplyChargeurIvoicePrice(
                                        invoiceWraper.getEbInvoice().getApplyCarrierIvoicePrice());
                                gapDouble = invoiceCopy
                                    .getApplyChargeurIvoicePrice().subtract(invoiceCopy.getPreCarrierCost());

                                invoiceCopy.setGapChargeur(gapDouble);
                                invoiceCopy
                                    .setListCostCategorieCharger(
                                        invoiceWraper.getEbInvoice().getListCostCategorieCharger());
                            }

                            if (connectedUser.isPrestataire()) {
                                invoiceCopy
                                    .setApplyCarrierIvoicePrice(
                                        invoiceWraper.getEbInvoice().getApplyCarrierIvoicePrice());
                                gapDouble = invoiceCopy
                                    .getApplyCarrierIvoicePrice().subtract(invoiceCopy.getPreCarrierCost());
                                gapConvertBigDecimal = gapDouble;
                                invoiceCopy.setGapCarrier(gapConvertBigDecimal);
                                invoiceCopy
                                    .setListCostCategorieTransporteur(
                                        invoiceWraper.getEbInvoice().getListCostCategorieTransporteur());
                            }

                            invoiceCopy.setPriceArraw(0);
                            String[] args = {
                                invoiceWraper.getEbInvoice().getApplyCarrierIvoicePrice().toString()
                            };
                            txtCom = messageSource.getMessage("frieght.prixapplique", args, lcl);
                        }

                        if (ActionFreightAudit.APPLYLISTINVOICENUMBER.getCode().equals(action)
                            && invoiceWraper.getEbInvoice().getApplyCarrierIvoiceNumber().toString() != null) {

                            if (connectedUser.isChargeur() || connectedUser.isControlTower()) {
                                invoiceCopy
                                    .setApplyChargeurIvoiceNumber(
                                        invoiceWraper.getEbInvoice().getApplyCarrierIvoiceNumber());
                            }

                            if (connectedUser.isPrestataire()) {
                                invoiceCopy
                                    .setApplyCarrierIvoiceNumber(
                                        invoiceWraper.getEbInvoice().getApplyCarrierIvoiceNumber());
                            }

                            invoiceCopy.setNumberArraw(0);
                            String[] args = {
                                invoiceWraper.getEbInvoice().getApplyCarrierIvoiceNumber().toString()
                            };
                            txtCom = messageSource.getMessage("frieght.nombreapplique", args, lcl);
                        }

                        if (ActionFreightAudit.APPLYLISTSTATUT.getCode().equals(action)
                            && invoiceWraper.getEbInvoice().getStatutCarrier().toString() != null) {

                            if (connectedUser.isChargeur() || connectedUser.isControlTower()) {
                                invoiceCopy.setStatutChargeur(invoiceWraper.getEbInvoice().getStatutCarrier());
                            }

                            if (connectedUser.isPrestataire()) {
                                invoiceCopy.setStatutCarrier(invoiceWraper.getEbInvoice().getStatutCarrier());
                            }

                            invoiceCopy.setStatutArraw(0);
                            String[] args = {
                                InvoiceStatus.getLibelleByCode(invoiceWraper.getEbInvoice().getStatutCarrier())
                            };
                            txtCom = messageSource.getMessage("frieght.statutapplique", args, lcl);
                        }

                        if (ActionFreightAudit.APPLYLISTDATE.getCode().equals(action)
                            && invoiceWraper.getEbInvoice().getInvoiceDateCarrier().toString() != null) {

                            if (connectedUser.isChargeur() || connectedUser.isControlTower()) {
                                invoiceCopy
                                    .setInvoiceDateChargeur(invoiceWraper.getEbInvoice().getInvoiceDateCarrier());
                            }

                            if (connectedUser.isPrestataire()) {
                                invoiceCopy.setInvoiceDateCarrier(invoiceWraper.getEbInvoice().getInvoiceDateCarrier());
                            }

                            invoiceCopy.setDateArraw(0);
                            SimpleDateFormat simpleDateFormatMonth = new SimpleDateFormat("MMMM");

                            SimpleDateFormat simpleDateFormatYear = new SimpleDateFormat("YYYY");

                            dateCom = "MONTH  "
                                + simpleDateFormatMonth.format(invoiceWraper.getEbInvoice().getInvoiceDateCarrier())
                                + "  YEAR "
                                + simpleDateFormatYear.format(invoiceWraper.getEbInvoice().getInvoiceDateCarrier());
                            String[] args = {
                                dateCom
                            };
                            txtCom = messageSource.getMessage("frieght.dateapplique", args, lcl);
                        }

                        if (ActionFreightAudit.APPLYLISTMEMO.getCode().equals(action)
                            && invoiceWraper.getEbInvoice().getMemoCarrier().toString() != null) {

                            if (connectedUser.isChargeur() || connectedUser.isControlTower()) {
                                invoiceCopy.setMemoChageur(invoiceWraper.getEbInvoice().getMemoCarrier());
                                invoiceMemo = invoiceCopy.getMemoChageur();
                            }

                            if (connectedUser.isPrestataire()) {
                                invoiceCopy.setMemoCarrier(invoiceWraper.getEbInvoice().getMemoCarrier());
                                invoiceMemo = invoiceCopy.getMemoCarrier();
                            }

                            String[] args = {
                                invoiceMemo.toString()
                            };
                            txtCom = messageSource.getMessage("frieght.memoapplique", args, lcl);
                        }

                        if (ActionFreightAudit.SHAREDPRICE.getCode().equals(action)) {

                            if (connectedUser.isChargeur() || connectedUser.isControlTower()) {
                                invoiceCopy.setApplyCarrierIvoicePrice(invoiceCopy.getApplyChargeurIvoicePrice());
                                invoiceCopy.setListCostCategorieTransporteur(invoiceCopy.getListCostCategorieCharger());
                                invoiceCopy.setGapCarrier(invoiceCopy.getGapChargeur());
                                invoiceCopy.setPriceArraw(1);
                                invoicePrix = invoiceCopy.getApplyCarrierIvoicePrice();
                            }

                            if (connectedUser.isPrestataire()) {
                                invoiceCopy.setApplyChargeurIvoicePrice(invoiceCopy.getApplyCarrierIvoicePrice());
                                invoiceCopy.setListCostCategorieCharger(invoiceCopy.getListCostCategorieTransporteur());
                                invoiceCopy.setGapChargeur(invoiceCopy.getGapCarrier());
                                invoiceCopy.setPriceArraw(2);
                                invoicePrix = invoiceCopy.getApplyChargeurIvoicePrice();
                            }

                            String[] args = {
                                invoicePrix != null ? invoicePrix.toString() : null
                            };
                            txtCom = messageSource.getMessage("frieght.prixpartage", args, lcl);
                        }

                        if (ActionFreightAudit.SHAREDNUMBER.getCode().equals(action)) {

                            if (connectedUser.isChargeur() || connectedUser.isControlTower()) {
                                invoiceCopy.setApplyCarrierIvoiceNumber(invoiceCopy.getApplyChargeurIvoiceNumber());
                                invoiceNumber = invoiceCopy.getApplyCarrierIvoiceNumber();
                                invoiceCopy.setNumberArraw(1);
                            }

                            if (connectedUser.isPrestataire()) {
                                invoiceCopy.setApplyChargeurIvoiceNumber(invoiceCopy.getApplyCarrierIvoiceNumber());
                                invoiceNumber = invoiceCopy.getApplyChargeurIvoiceNumber();
                                invoiceCopy.setNumberArraw(2);
                            }

                            String[] args = null;

                            if (invoiceNumber != null) args = new String[]{
                                invoiceNumber.toString()
                            };
                            txtCom = messageSource.getMessage("frieght.nombrepartage", args, lcl);
                        }

                        if (ActionFreightAudit.SHAREDSTATUS.getCode().equals(action)) {

                            if (connectedUser.isChargeur() || connectedUser.isControlTower()) {
                                invoiceCopy.setStatutCarrier(invoiceCopy.getStatutChargeur());
                                invoiceStatut = invoiceCopy.getStatutCarrier();
                                invoiceCopy.setStatutArraw(1);
                            }

                            if (Role.ROLE_PRESTATAIRE.getCode().equals(connectedUser.getRole())) {
                                invoiceCopy.setStatutChargeur(invoiceCopy.getStatutCarrier());
                                invoiceStatut = invoiceCopy.getStatutChargeur();
                                invoiceCopy.setStatutArraw(2);
                            }

                            String[] args = {
                                InvoiceStatus.getLibelleByCode(invoiceStatut)
                            };
                            txtCom = messageSource.getMessage("frieght.statutpartage", args, lcl);
                        }

                        if (ActionFreightAudit.SHAREDMONTHANDYEAR.getCode().equals(action)) {

                            if (connectedUser.isChargeur() || connectedUser.isControlTower()) {
                                invoiceCopy.setInvoiceDateCarrier(invoiceCopy.getInvoiceDateChargeur());
                                invoiceDate = invoiceCopy.getInvoiceDateCarrier();
                                invoiceCopy.setDateArraw(1);
                            }

                            if (connectedUser.isPrestataire()) {
                                invoiceCopy.setInvoiceDateChargeur(invoiceCopy.getInvoiceDateCarrier());
                                invoiceDate = invoiceCopy.getInvoiceDateChargeur();
                                invoiceCopy.setDateArraw(2);
                            }

                            String[] args = {
                                invoiceDate != null ? invoiceDate.toString() : null
                            };
                            txtCom = messageSource.getMessage("frieght.datepartage", args, lcl);
                        }

                        chat = new EbChat(connectedUser);
                        chat.setIdChatComponent(IDChatComponent.FREIGHT_AUDIT.getCode());
                        chat.setModule(Module.FREIGHT_AUDIT.getCode());
                        chat.setUserName(connectedUser.getNomPrenom());
                        chat.setDateCreation(dateActuel);
                        chat.setText(txtCom);
                        chat.setIdFiche(numberInvoice);
                        listChat.add(chat);
                    }

                }

                listUpdateInvoice.add(invoiceCopy);

                if (invoiceWraper.getLisAction() != null && !invoiceWraper.getLisAction().isEmpty()) {

                    if (invoiceWraper.getLisAction().contains(ActionFreightAudit.APPLYLISTINVOICEPRICE.getCode())
                        || invoiceWraper.getLisAction().contains(ActionFreightAudit.APPLYLISTINVOICENUMBER.getCode())
                        || invoiceWraper.getLisAction().contains(ActionFreightAudit.APPLYLISTSTATUT.getCode())
                        || invoiceWraper.getLisAction().contains(ActionFreightAudit.APPLYLISTDATE.getCode())) {
                        emailServicePricingBooking
                            .sendEmailApplyingInformation(invoiceCopy, invoiceWraper.getLisAction());
                    }

                    if (invoiceWraper.getLisAction().contains(ActionFreightAudit.SHAREDPRICE.getCode())
                        || invoiceWraper.getLisAction().contains(ActionFreightAudit.SHAREDNUMBER.getCode())
                        || invoiceWraper.getLisAction().contains(ActionFreightAudit.SHAREDSTATUS.getCode())
                        || invoiceWraper.getLisAction().contains(ActionFreightAudit.SHAREDMONTHANDYEAR.getCode())) {
                        emailServicePricingBooking
                            .sendEmailSharingInformation(invoiceCopy, invoiceWraper.getLisAction());
                    }

                }

            }

        }

        ebInvoiceRepository.saveAll(listUpdateInvoice);
        ebChatRepository.saveAll(listChat);
        return result;
    }

    @Override
    public List getListStatus() {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        List listStatus = new ArrayList<InvoiceStatus>();

        for (InvoiceStatus status: InvoiceStatus.getListStatus()) {

            if (connectedUser.isChargeur() && status.getUserRole().contains(connectedUser.getRole())) {
                listStatus.add(status);
            }
            else if (connectedUser.isControlTower()
                && status.getUserRole().contains(connectedUser.getRole())) listStatus.add(status);
            else if (connectedUser.isPrestataire()
                && status.getUserRole().contains(connectedUser.getRole())) listStatus.add(status);

        }

        return listStatus;
    }

    @Override
    public List<String> getListFlagForCarrier(Integer ebUserNum) {
        return ebInvoiceRepository.selectListFlagForCarrier(ebUserNum);
    }

    @Override
    public List<String> getListFlagForControlTower(Integer ebUserNum) {
        return ebInvoiceRepository.selectListFlagForControlTower(ebUserNum);
    }
}
