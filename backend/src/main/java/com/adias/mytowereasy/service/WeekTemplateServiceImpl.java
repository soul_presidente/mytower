package com.adias.mytowereasy.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.chanel.model.EbWeekTemplate;
import com.adias.mytowereasy.chanel.model.EbWeekTemplateDay;
import com.adias.mytowereasy.chanel.model.EbWeekTemplateHour;
import com.adias.mytowereasy.dao.DaoWeekTemplate;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class WeekTemplateServiceImpl extends MyTowerService implements WeekTemplateService {
    @Autowired
    DaoWeekTemplate daoWeekTemplate;

    @Override
    public List<EbWeekTemplate> listWeekTemplate(SearchCriteria criteria) {
        List<EbWeekTemplate> listweekTemp = daoWeekTemplate.list(criteria);

        if (listweekTemp != null && !listweekTemp.isEmpty()) {

            for (EbWeekTemplate week: listweekTemp) {
                List<EbWeekTemplateDay> weekTemplateDays = ebWeekTemplateDayRepository
                    .findByWeek_ebWeektemplateNum(week.getEbWeektemplateNum());
                weekTemplateDays = sortDays(weekTemplateDays);
                week.setDays(weekTemplateDays);
            }

        }

        return listweekTemp;
    }

    @Override
    public Long countListWeekTemplate(SearchCriteria criteria) {
        return daoWeekTemplate.listCount(criteria);
    }

    @Override
    public int deleteWeekTemplate(EbWeekTemplate ebWeekTemplate) {
        List<EbWeekTemplateDay> days = null;
        List<EbWeekTemplateHour> hours = null;
        days = ebWeekTemplate.getDays();

        if (days != null) {
            List<Integer> daysIds = days
                .stream().map(day -> day.getEbWeektemplateDayNum()).collect(Collectors.toList());

            ebWeekTemplateHourRepository.deleteByEbWeekTempleDayList(daysIds);
        }

        ebWeekTemplateDayRepository.deleteByEbWeekTemple(ebWeekTemplate.getEbWeektemplateNum());

        ebWeekTemplateRepository.deleteById(ebWeekTemplate.getEbWeektemplateNum());
        return 0;
    }

    @Override
    public EbWeekTemplate saveWeekTemplate(EbWeekTemplate ebWeekTemplate)

    {
        EbWeekTemplate oldWeek = null;

        if (ebWeekTemplate != null && ebWeekTemplate.getEbWeektemplateNum() != null) {
            oldWeek = ebWeekTemplateRepository.getOne(ebWeekTemplate.getEbWeektemplateNum());
        }

        if (oldWeek != null || (ebWeekTemplate != null && ebWeekTemplate.getEbWeektemplateNum() == null
            && ebWeekTemplate.getType() == 0)) {

            if (oldWeek != null) // delete all previous old hours
            {

                for (EbWeekTemplateDay day: oldWeek.getDays()) {

                    for (EbWeekTemplateHour hour: day.getHours()) {

                        if (hour.getEbWeektemplateHourNum() != null) {
                            EbWeekTemplateHour hourToDelete = this.ebWeekTemplateHourRepository
                                .getOne(hour.getEbWeektemplateHourNum());

                            if (hourToDelete != null) {
                                hour.setDay(null);
                                this.ebWeekTemplateHourRepository.delete(hour);
                            }

                        }

                    }

                    day.setHours(null);
                    day = this.ebWeekTemplateDayRepository.save(day); // saving
                                                                      // day
                }

            }

            List<EbWeekTemplateDay> days = null;
            List<EbWeekTemplateHour> hours = null;
            days = ebWeekTemplate.getDays();
            ebWeekTemplate.setDays(null);
            ebWeekTemplateRepository.save(ebWeekTemplate);// saving week

            for (EbWeekTemplateDay day: days) {
                day.setWeek(ebWeekTemplate);
                hours = day.getHours();
                day.setHours(null);
                day = this.ebWeekTemplateDayRepository.save(day); // saving day

                if (hours != null) {

                    for (EbWeekTemplateHour hour: hours) {
                        hour.setDay(new EbWeekTemplateDay(day.getEbWeektemplateDayNum()));
                        this.ebWeekTemplateHourRepository.save(hour); // saving
                                                                      // hour
                    }

                }

            }

        }
        else if (oldWeek == null || (ebWeekTemplate != null && ebWeekTemplate.getEbWeektemplateNum() == null
            && ebWeekTemplate.getType() == 1)) {

            if (ebWeekTemplate.getDateDebut().before(ebWeekTemplate.getDateFin())) {
                List<EbWeekTemplate> List = daoWeekTemplate.getListEbWeekTemplateWithChauvauchementDate(ebWeekTemplate);

                if (!List.isEmpty()) {
                    return null;
                }

            }

            List<EbWeekTemplateDay> days = null;
            List<EbWeekTemplateHour> hours = null;
            days = ebWeekTemplate.getDays();
            ebWeekTemplate.setDays(null);
            ebWeekTemplateRepository.save(ebWeekTemplate);// saving week

            for (EbWeekTemplateDay day: days) {
                day.setWeek(ebWeekTemplate);
                hours = day.getHours();
                day = this.ebWeekTemplateDayRepository.save(day); // saving
                                                                  // day

                if (hours != null) {

                    for (EbWeekTemplateHour hour: hours) {
                        hour.setDay(new EbWeekTemplateDay(day.getEbWeektemplateDayNum()));
                        this.ebWeekTemplateHourRepository.save(hour); // saving
                                                                      // hour
                    }

                }

            }

        }

        return ebWeekTemplate;
    }

    @Override
    public List<EbWeekTemplate> listWeekTemplate() {
        return ebWeekTemplateRepository.findAll();
    }

    @Override
    public List<EbWeekTemplateDay> findByWeek_ebWeektemplateNum(Integer ebWeekTemplateNum) {
        return sortDays(ebWeekTemplateDayRepository.findByWeek_ebWeektemplateNum(ebWeekTemplateNum));
    }

    public List<EbWeekTemplateDay> sortDays(List<EbWeekTemplateDay> days) {
        Collections.sort(days, new Comparator<EbWeekTemplateDay>() {
            @Override
            public int compare(EbWeekTemplateDay d1, EbWeekTemplateDay d2) {
                return d1.getCode().compareTo(d2.getCode());
            }
        });
        return days;
    }
}
