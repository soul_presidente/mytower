package com.adias.mytowereasy.service.filedownload;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.CustomsDeclarationStatus;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.model.custom.EbCustomDeclaration;
import com.adias.mytowereasy.service.CustomDeclarationService;
import com.adias.mytowereasy.util.DateUtils;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaCustom;


@Service
public class FiledownloadCustomDeclarationServiceImp implements FiledownloadCustomDeclarationService {
    @Autowired
    CustomDeclarationService customService;

    @Override
    public FileDownloadStateAndResult
        listFileDownLoadCustomDeclaration(EbUser connectedUser, SearchCriteria customCriteria) throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();

        if (customCriteria.getSearchterm() != null && !customCriteria.getSearchterm().trim().isEmpty()) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("value", customCriteria.getSearchterm());
            customCriteria.setSearch(map);
        }

        ObjectMapper objectMapper = new ObjectMapper();
        SearchCriteriaCustom criteriaCustom = objectMapper.convertValue(customCriteria, SearchCriteriaCustom.class);

        List<EbCustomDeclaration> listEbDeclaration = customService.getListEbDeclaration(criteriaCustom);

        for (EbCustomDeclaration ebDeclaration: listEbDeclaration) {
            Map<String, String[]> map = new LinkedHashMap<String, String[]>();

            map.put("refDemandeDedouanement", new String[]{
                "Customs request Ref", ebDeclaration.getRefDemandeDedouanement()
            });
            map.put("idDedouanement", new String[]{
                "Clearance ID", ebDeclaration.getIdDedouanement()
            });
            map.put("demandeDate", new String[]{
                "Request date",
                ebDeclaration.getDemandeDate() != null ? ebDeclaration.getDemandeDate().toString() : null
            });
            map.put("demandeur", new String[]{
                "Requestor", ebDeclaration.getDemandeur().getNomPrenom()
            });
            map.put("operation", new String[]{
                "Flow", ebDeclaration.getDemandeDate() != null ? ebDeclaration.getOperation().toString() : null
            });
            map.put("projet", new String[]{
                "Project", ebDeclaration.getProjet()
            });
            map.put("codeBAU", new String[]{
                "BAU code", ebDeclaration.getCodeBAU()
            });
            map.put("eori", new String[]{
                "EORI", ebDeclaration.getEori()
            });
            map.put("typeDeFlux", new String[]{
                "Type of flow", ebDeclaration.getTypeDeFlux()
            });
            map.put("sitePlant", new String[]{
                "Site / Plant", ebDeclaration.getSitePlant()
            });
            map.put("destinataire", new String[]{
                "Consignee", ebDeclaration.getDestinataire()
            });
            // map.put("xEbIncoterm", new String[] {"Incoterm",
            // ebDeclaration.getxEbIncoterm().getLibelle()});
            map.put("villeIncoterm", new String[]{
                "Incoterm city", ebDeclaration.getVilleIncoterm()
            });
            map.put("charger", new String[]{
                "Shipper", ebDeclaration.getCharger().getNomPrenom()
            });
            map.put("xEcModeTransport", new String[]{
                "Transport mode",
                ebDeclaration.getxEcModeTransport() != null ?
                    ModeTransport.getLibelleByCode(ebDeclaration.getxEcModeTransport()) :
                    null
            });
            map.put("immatriculation", new String[]{
                "Mean of transport identification", ebDeclaration.getImmatriculation()
            });
            map.put("dateArrivalEsstimed", new String[]{
                "Expected date of arrival",
                ebDeclaration.getDateArrivalEsstimed() != null ?
                    ebDeclaration.getDateArrivalEsstimed().toString() :
                    null
            });
            map.put("airportPortOfArrival", new String[]{
                "Airport/Port of arrival", ebDeclaration.getAirportPortOfArrival()
            });
            map.put("xEcCountryProvenances", new String[]{
                "Country of provenance", ebDeclaration.getxEcCountryProvenances().getLibelle()
            });
            map.put("agentBroker", new String[]{
                "Agent / Customs Broker", ebDeclaration.getAgentBroker().getNomPrenom()
            });
            map.put("xEcCountryDedouanement", new String[]{
                "Clearance country", ebDeclaration.getxEcCountryDedouanement().getLibelle()
            });
            map.put("lieuDedouanement", new String[]{
                "Location of Customs clearance", ebDeclaration.getLieuDedouanement()
            });
            map.put("biensDoubleUsage", new String[]{
                "Dual-use goods", ebDeclaration.getBiensDoubleUsage()
            });
            map.put("licenceDoubleUsage", new String[]{
                "Dual-use licence reference", ebDeclaration.getLicenceDoubleUsage()
            });
            map.put("Transit", new String[]{
                "Transit", ebDeclaration.getTransit()
            });
            map.put("bureauDeTransit", new String[]{
                "Customs office", ebDeclaration.getBureauDeTransit()
            });
            map.put("warGoods", new String[]{
                "War goods", ebDeclaration.getWarGoods()
            });
            map.put("warGoodLicenceReference", new String[]{
                "War good licence reference", ebDeclaration.getWarGoodLicenceReference()
            });
            map.put("customOffice", new String[]{
                "Customs office", ebDeclaration.getCustomOffice()
            });
            map.put("nomTransporteur", new String[]{
                "Carrier name", ebDeclaration.getCarrier().getNomPrenom()
            });
            map.put("telephoneTransporteur", new String[]{
                "Carrier phone", ebDeclaration.getCarrier().getTelephone()
            });
            map.put("emailTransporteur", new String[]{
                "Carrier email", ebDeclaration.getCarrier().getEmail()
            });
            map.put("freightPart", new String[]{
                "EEC Freight part",
                ebDeclaration.getFreightPart() != null ? ebDeclaration.getFreightPart().toString() : null
            });
            map.put("freightPartCurrency", new String[]{
                "EEC Freight part currency", ebDeclaration.getFreightPartCurrency().getCode()
            });
            map.put("insuranceCost", new String[]{
                "Insurance cost",
                ebDeclaration.getInsuranceCost() != null ? ebDeclaration.getInsuranceCost().toString() : null
            });
            map.put("insuranceCostCurrency", new String[]{
                "Insurance cost currency", ebDeclaration.getInsuranceCostCurrency().getCode()
            });
            map.put("invoiceAmount", new String[]{
                "Invoice amount",
                ebDeclaration.getInvoiceAmount() != null ? ebDeclaration.getInvoiceAmount().toString() : null
            });
            map.put("invoiceCurrency", new String[]{
                "Invoice Currency", ebDeclaration.getInvoiceCurrency().getCode()
            });
            map.put("toolingCost", new String[]{
                "Tooling cost",
                ebDeclaration.getToolingCost() != null ? ebDeclaration.getToolingCost().toString() : null
            });
            map.put("toolingCostCurrency", new String[]{
                "Tooling cost currency", ebDeclaration.getToolingCostCurrency().getCode()
            });
            map.put("totalFreightCost", new String[]{
                "Total freight cost",
                ebDeclaration.getTotalFreightCost() != null ? ebDeclaration.getTotalFreightCost().toString() : null
            });
            map.put("totalFreightCostCurrency", new String[]{
                "Total freight cost currency", ebDeclaration.getTotalFreightCostCurrency().getCode()
            });
            map.put("numTva", new String[]{
                "VAT Number", ebDeclaration.getNumTva() != null ? ebDeclaration.getNumTva().toString() : null
            });
            map.put("traficAtBorder", new String[]{
                "Traffic at border", ebDeclaration.getTraficAtBorder()
            });
            map.put("importerOfRecord", new String[]{
                "Importer of Records", ebDeclaration.getImporterOfRecord()
            });
            map.put("delay", new String[]{
                "Delay", ebDeclaration.getDelay()
            });
            map.put("comment", new String[]{
                "Comment", ebDeclaration.getComment()
            });
            map.put("grossWeight", new String[]{
                "Gross weight ",
                ebDeclaration.getGrossWeight() != null ? ebDeclaration.getGrossWeight().toString() : null
            });
            map.put("grossWeightUnit", new String[]{
                "Gross weight unit", ebDeclaration.getGrossWeightUnit()
            });
            map.put("numberOfPack", new String[]{
                "Number of pack",
                ebDeclaration.getNumberOfPack() != null ? ebDeclaration.getNumberOfPack().toString() : null
            });
            map.put("numberOfLines", new String[]{
                "Number of lines",
                ebDeclaration.getNumberOfLines() != null ? ebDeclaration.getNumberOfLines().toString() : null
            });
            map.put("totalQuantity", new String[]{
                "Total quantity",
                ebDeclaration.getTotalQuantity() != null ? ebDeclaration.getTotalQuantity().toString() : null
            });
            map.put("valueAmountTotal", new String[]{
                "Value Amount total",
                ebDeclaration.getValueAmountTotal() != null ? ebDeclaration.getValueAmountTotal().toString() : null
            });
            map.put("business", new String[]{
                "Business", ebDeclaration.getBusiness()
            });
            map.put("liquidation", new String[]{
                "Duty relief", ebDeclaration.getLiquidation()
            });
            map.put("euCode", new String[]{
                "EU Code", ebDeclaration.getEuCode()
            });
            map.put("hsCode", new String[]{
                "HS code", ebDeclaration.getHsCode()
            });
            map.put("partNumber", new String[]{
                "Part Number", ebDeclaration.getPartNumber()
            });
            map.put("procedure", new String[]{
                "Procedure", ebDeclaration.getProcedure()
            });
            map.put("quantity", new String[]{
                "Quantity", ebDeclaration.getQuantity()
            });
            map.put("valueAmount", new String[]{
                "ValueAmount", ebDeclaration.getValueAmount() != null ? ebDeclaration.getValueAmount().toString() : null
            });
            map.put("valueCurrency", new String[]{
                "ValueCurrency", ebDeclaration.getValueCurrency().getCode()
            });
            map.put("exchangeRate", new String[]{
                "Exchange rate",
                ebDeclaration.getExchangeRate() != null ? ebDeclaration.getExchangeRate().toString() : null
            });
            map.put("dateUpload", new String[]{
                "Upload date", ebDeclaration.getDateUpload() != null ? ebDeclaration.getDateUpload().toString() : null
            });
            map.put("typeDoc", new String[]{
                "Doc type", ebDeclaration.getTypeDoc()
            });
            map.put("numberOrReference1", new String[]{
                "Number or reference 1", ebDeclaration.getNumberOrReference1()
            });
            map.put("numberOrReference2", new String[]{
                "Number or reference 2", ebDeclaration.getNumberOrReference2()
            });
            map.put("attroneyText", new String[]{
                "Attorney text", ebDeclaration.getAttroneyText()
            });
            map.put("cancellationReason", new String[]{
                "Cancellation reason", ebDeclaration.getCancellationReason()
            });
            map.put("dateCancellation", new String[]{
                "Cancellation date",
                ebDeclaration.getDateCancellation() != null ? ebDeclaration.getDateCancellation().toString() : null
            });
            map.put("customRegime", new String[]{
                "Customs Regime", ebDeclaration.getCustomRegime()
            });
            map.put("leg", new String[]{
                "Leg", ebDeclaration.getLeg() != null ? ebDeclaration.getLeg().toString() : null
            });
            map.put("nbOfTemporaryRegime", new String[]{
                "Nb of temporary regime linked",
                ebDeclaration.getNbOfTemporaryRegime() != null ?
                    ebDeclaration.getNbOfTemporaryRegime().toString() :
                    null
            });
            map.put("dateEcs", new String[]{
                "Date ECS", DateUtils.formatUTCDateOnly(ebDeclaration.getDateEcs())
            });
            map.put("dateDeclaration", new String[]{
                "Date Declaration", DateUtils.formatUTCDateOnly(ebDeclaration.getDateDeclaration())
            });

            String status = "";

            if (ebDeclaration.getStatut() != null) {

                for (CustomsDeclarationStatus enumValue: CustomsDeclarationStatus.values()) {
                    if (enumValue.getCode().equals(ebDeclaration.getStatut())) status = enumValue.getKey();
                }

            }

            map.put("statut", new String[]{
                " Status", status
            });

            map.put("declarationNumber", new String[]{
                "Declaration number", ebDeclaration.getStatut() != null ? ebDeclaration.getStatut().toString() : null
            });
            map.put("declarationReference", new String[]{
                "Declaration Reference",
                ebDeclaration.getDeclarationReference() != null ?
                    ebDeclaration.getDeclarationReference().toString() :
                    null
            });
            map.put("transit", new String[]{
                "Transit", ebDeclaration.getTransit() != null ? ebDeclaration.getTransit().toString() : null
            });
            map.put("dateCreationSys", new String[]{
                "Creation date",
                ebDeclaration.getDateCreationSys() != null ? ebDeclaration.getDateCreationSys().toString() : null
            });
            map.put("nomFournisseur", new String[]{
                "Supplier name",
                ebDeclaration.getNomFournisseur() != null ? ebDeclaration.getNomFournisseur().toString() : null
            });
            map.put("xEbUserDeclarant", new String[]{
                "Declarer",
                ebDeclaration.getxEbUserDeclarant() != null ? ebDeclaration.getxEbUserDeclarant().getNomPrenom() : null
            });
            map.put("documents", new String[]{
                "documents",
                ebDeclaration.getAttachedFilesCount() != null ? ebDeclaration.getAttachedFilesCount().toString() : null
            });
            listresult.add(map);
        }

        resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
        resultAndState.setListResult(listresult);
        return resultAndState;
    }
}
