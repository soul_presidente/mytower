/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbUser;


public interface InscriptionService {
    public EbUser inscrire(EbUser ebUser);

    public EbUser inscriptionPublic(EbUser ebUser);

    public boolean checkUserByEmail(String email);

    public EbEtablissement getEbEtablissementBySiret(String siret);

    public EbCompagnie getEbCompagnieBySiren(String siren);

    boolean checkUserByUsername(String username);
}
