package com.adias.mytowereasy.service;

import java.util.List;
import java.util.Map;


public interface DataRecoveryService {
    Boolean generateData(Map<String, List<Integer>> data);// cette methode
                                                          // permet de generer
                                                          // les données par
                                                          // type
                                                          // (psl,typeRequest,typeUnit...)
                                                          // pour une liste de
                                                          // compagnies
}
