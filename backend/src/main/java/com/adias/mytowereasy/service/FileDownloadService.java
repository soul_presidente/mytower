package com.adias.mytowereasy.service;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import com.adias.mytowereasy.model.FileDownloadStateAndResult;


public interface FileDownloadService {
    public void exportData(@RequestBody Map<String, Object> criteria, HttpServletResponse response) throws Exception;

    public void exportFacture(
        @PathVariable("ebDestinationCountryNum") Integer ebDestinationCountryNum,
        HttpServletResponse response)
        throws Exception;

    public void extractPricing(@RequestParam Integer type, HttpServletResponse response);

    public FileDownloadStateAndResult checkExportState(@RequestBody String code) throws Exception;

    public Integer getExtractionRunningCode(String code);

    public void setExtractionLimit(Long size);

    public long getExtractionLimit();
}
