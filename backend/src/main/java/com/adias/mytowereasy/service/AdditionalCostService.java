package com.adias.mytowereasy.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbAdditionalCost;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class AdditionalCostService extends MyTowerService {
    public List<EbAdditionalCost> getEbAdditionalCostByebCompagnieNum(Integer ebCompagnieNum) {
        return ebAdditionalCostRepository.selectEbAdditionalCostByebCompagnieNum(ebCompagnieNum);
    }

    public List<EbAdditionalCost> getListAdditionalCosts(SearchCriteria criteria) {
        return daoAdditionalCost.getListAdditionalCosts(criteria);
    }

    public Long getCountListAdditionalCosts(SearchCriteria criteria) {
        return daoAdditionalCost.getCountListAdditionalCosts(criteria);
    }

    public EbAdditionalCost upsertAdditionalCost(EbAdditionalCost cost) {
        return ebAdditionalCostRepository.save(cost);
    }

    public boolean desactivateAdditionalCost(Integer ebAdditionalCostNum) {

        try {
            ebAdditionalCostRepository.desactivateAdditionalCost(ebAdditionalCostNum);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean checkCodeExistance(String code, Integer ebCompagnieNum) {
        return ebAdditionalCostRepository.existsByxEbCompagnie_ebCompagnieNumAndCodeIgnoreCase(ebCompagnieNum, code);
    }
}
