package com.adias.mytowereasy.service;

import java.util.List;


public interface TypeUnitService {
    public void generateTypeOfUnitServiceByCompanyNum(List<Integer> listEbCompagnieNum) throws Exception;
}
