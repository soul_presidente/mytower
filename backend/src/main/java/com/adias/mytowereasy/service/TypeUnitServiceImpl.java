package com.adias.mytowereasy.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dao.DaoTypeUnit;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbTypeUnit;
import com.adias.mytowereasy.model.PricingBookingEnumeration.MarchandiseTypeOfUnit;
import com.adias.mytowereasy.repository.EbMarchandiseRepository;
import com.adias.mytowereasy.repository.EbTypeUnitRepository;
import com.adias.mytowereasy.utils.search.SearchCriteriaTypeUnit;


@Service
public class TypeUnitServiceImpl implements TypeUnitService {
    @Autowired
    private DaoTypeUnit daoTypeUnit;

    @Autowired
    private EbTypeUnitRepository typeUnitRepository;

    @Autowired
    private EbMarchandiseRepository ebMarchandiseRepository;

    @Override
    public void generateTypeOfUnitServiceByCompanyNum(List<Integer> listEbCompagnieNum) throws Exception {
        // Enregistrement des types Unit pour les compagnies
        SearchCriteriaTypeUnit criteria = new SearchCriteriaTypeUnit();

        for (Integer ebCompagnieNum: listEbCompagnieNum) {
            List<EbTypeUnit> listTypeOfUnitToSave;
            // On récupère la configuration actuelle pour la compagnie
            criteria.setEbCompagnieNum(ebCompagnieNum);
            List<EbTypeUnit> listTypeOfUnitFromDB = daoTypeUnit.getListTypeUnit(criteria);

            // On récupère la liste d'enumaration
            List<MarchandiseTypeOfUnit> listTypeMarchandise = MarchandiseTypeOfUnit.getListMarchandiseTypeOfUnit();

            // si la comapgnie n'a aucuns type de marchandise alors on va
            // ajouter la liste de l'enumaration
            if (listTypeOfUnitFromDB == null || listTypeOfUnitFromDB.isEmpty()) {
                listTypeOfUnitToSave = new ArrayList<>();

                for (MarchandiseTypeOfUnit typeMarchandise: listTypeMarchandise) {
                    EbTypeUnit unit = new EbTypeUnit();
                    unit.setLibelle(typeMarchandise.getLibelle());
                    unit.setCode(typeMarchandise.getCode().toString());
                    unit.setxEbTypeContainer(typeMarchandise.getTypeConteneur());
                    unit.setxEbCompagnie(new EbCompagnie(ebCompagnieNum));
                    unit.setActivated(true);
                    listTypeOfUnitToSave.add(unit);
                }

            }
            else {
                listTypeOfUnitToSave = new ArrayList<>();

                for (MarchandiseTypeOfUnit typeMarchandise: listTypeMarchandise) {
                    Boolean typeExist = false;

                    for (EbTypeUnit typeUnit: listTypeOfUnitFromDB) {
                        String unitCode = typeUnit.getCode();

                        if (unitCode != null
                            && unitCode.toLowerCase().equals(typeMarchandise.getCode().toString().toLowerCase())
                            && typeUnit.getLibelle().toLowerCase().equals(typeMarchandise.getLibelle().toLowerCase())) {
                            typeExist = true;
                        }

                    }

                    if (!typeExist) {
                        EbTypeUnit unit = new EbTypeUnit();
                        unit.setLibelle(typeMarchandise.getLibelle());
                        unit.setCode(typeMarchandise.getCode().toString());
                        unit.setxEbCompagnie(new EbCompagnie(ebCompagnieNum));
                        unit.setxEbTypeContainer(typeMarchandise.getTypeConteneur());
                        unit.setActivated(true);
                        listTypeOfUnitToSave.add(unit);
                    }

                }

            }

            // Transfert des valeurs de TypeMarchandise vers xEbTypeUnit
            if (listTypeOfUnitToSave != null && !listTypeOfUnitToSave.isEmpty()) {
                saveTypeUnitAndRefreshListMarchandise(listTypeOfUnitToSave);
            }

        }

    }

    // cette methode permet de creer les types of unit manquant et met à jour la
    // liste des marchandise
    void saveTypeUnitAndRefreshListMarchandise(List<EbTypeUnit> listTypeOfUnit) {
        typeUnitRepository.saveAll(listTypeOfUnit);

        for (EbTypeUnit type: listTypeOfUnit) {
            Integer typeMarchandise = MarchandiseTypeOfUnit.getCodeByLibelle(type.getLibelle());
            ebMarchandiseRepository
                .recoveryTypeOfUnitByEbCompanyNumAndTypeMarchandise(
                    type.getxEbCompagnie().getEbCompagnieNum(),
                    typeMarchandise.toString(),
                    type.getEbTypeUnitNum(),
                    type.getLibelle());
        }

    }
}
