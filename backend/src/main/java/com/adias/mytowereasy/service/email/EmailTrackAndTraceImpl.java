/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service.email;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import com.adias.mytowereasy.dao.DaoUser;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.model.tt.EbTtEvent;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.repository.EbCompagnieRepository;
import com.adias.mytowereasy.repository.EbTrackTraceRepository;
import com.adias.mytowereasy.service.ListStatiqueService;
import com.adias.mytowereasy.service.NotificationService;
import com.adias.mytowereasy.util.email.Email;
import com.adias.mytowereasy.util.email.EmailHtmlSender;


@Service
public class EmailTrackAndTraceImpl implements EmailTrackAndTrace {
    @Autowired
    ListStatiqueService serviceListStatique;

    private EmailHtmlSender emailHtmlSender;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private EbTrackTraceRepository ebTrackTraceRepository;

    @Autowired
    private EbCompagnieRepository ebCompagnieRepository;

    @Autowired
    private NotificationService notificationService;

    @Value("${mytowereasy.url.front}")
    private String url;

    @Autowired
    DaoUser daoUser;

    @Autowired
    public EmailTrackAndTraceImpl(EmailHtmlSender emailHtmlSender) {
        this.emailHtmlSender = emailHtmlSender;
    }

    @Override
    public void sendEmailDeviationTT(EbTtEvent deviation, EbUser user, EbTtEvent ttEvent) {
        Context params = new Context();
        Email email = new Email();

        String refTra = deviation.getxEbTtTracing().getRefTransport();

        String[] args = {
            refTra
        };

        String subject = "declarationdunedeviation.subject";
        email.setArguments(args);
        email.setSubject(subject);

        params
            .setVariable(
                "url",
                url
                    .concat("app/track-trace/details/")
                    .concat(deviation.getxEbTtTracing().getEbTtTracingNum().toString()));

        String status = deviation.getxEbTtPslApp() != null && deviation.getxEbTtPslApp().getCompanyPsl() != null ?
            deviation.getxEbTtPslApp().getCompanyPsl().getLibelle() :
            "";
        String cat = null;

        if (deviation.getCategorie() != null) {
            cat = deviation.getCategorie().getLibelle();
        }

        Date date = deviation.getDateEvent();

        params.setVariable("nomCompUser", user.getEbCompagnie().getNom());
        params.setVariable("nomEtabUser", user.getEbEtablissement().getNom());

        EbCompagnie cp = ebCompagnieRepository.getOne(user.getEbCompagnie().getEbCompagnieNum());
        String cpName = cp.getNom();

        params.setVariable("refTra", refTra);
        params.setVariable("cpName", cpName);
        params.setVariable("status", status);
        params.setVariable("cat", cat);
        params.setVariable("date", date);

        Map<String, Object> mapParams = new HashMap<String, Object>();
        mapParams.put("ebTtEventNum", ttEvent.getEbTtEventNum());
        mapParams.put("ebTtEventUserNum", ttEvent.getxEbUser().getEbUserNum());
        mapParams.put("ebTtTracingNum", ttEvent.getxEbTtTracing().getEbTtTracingNum());
        Integer tracingNum = ttEvent.getxEbTtTracing().getEbTtTracingNum();
        EbDemande ebDemande = ebTrackTraceRepository.selectEbDemandeDetailsByEbTracingNum(tracingNum);
        params.setVariable("refCustomer", ebDemande.getCustomerReference());
        params.setVariable("ebdemande", ebDemande);

        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                user.getEbCompagnie().getEbCompagnieNum(),
                Enumeration.EmailConfig.DECLARATION_DUNE_DEVIATION.getCode(),
                Enumeration.Module.TRACK.getCode(),
                ebDemande.getEbDemandeNum(),
                mapParams);

        if (res != null && !res.isEmpty()) notificationService
            .generateAlert(
                email,
                (List<Integer>) res.get("ids"),
                ttEvent.getxEbTtTracing().getEbTtTracingNum(),
                Enumeration.Module.TRACK.getCode());

        email.setEmailConfig(Enumeration.EmailConfig.DECLARATION_DUNE_DEVIATION);

        emailHtmlSender
            .sendListEmail(
                (List<String>) res.get("emails"),
                email,
                "trackAndTrace/30-email-declaration-dune-deviation",
                params,
                subject);
    }

    @Override
    public void sendEmailUpdatePsl(EbTTPslApp ebPslApp, EbUser user, EbTtEvent ttEvent, String libellePsl) {
        // TODO Auto-generated method stub

        Context params = new Context();
        Email email = new Email();

        Integer tracingNum = ebPslApp.getxEbTrackTrace().getEbTtTracingNum();
        Optional<EbTtTracing> tracing = ebTrackTraceRepository.findById(tracingNum);
        EbTtTracing ebTracing = tracing.get();
        EbDemande ebDemande = ebTrackTraceRepository
            .selectEbDemandeDetailsByEbTracingNum(ebTracing.getEbTtTracingNum());

        String refTra = ebTracing.getRefTransport();
        libellePsl = ebPslApp.getCompanyPsl().getLibelle();

        String[] args = {
            refTra, libellePsl
        };

        String subject = "miseajourpsl.subject";
        email.setArguments(args);
        email.setSubject(subject);
        params
            .setVariable(
                "url",
                url.concat("app/track-trace/details/").concat(tracing.get().getEbTtTracingNum().toString()));

        String status = libellePsl;

        String pays = ebPslApp.getxEcCountry() != null ? ebPslApp.getxEcCountry().getLibelle() : "";

        String ville = ebPslApp.getCity();

        String unitRef = ebDemande.getUnitsReference();

        Date date = ebPslApp.getDateActuelle();

        EbCompagnie cp = ebCompagnieRepository.getOne(user.getEbCompagnie().getEbCompagnieNum());
        String cpName = cp.getNom();

        params.setVariable("refCustomer", ebDemande.getCustomerReference());
        params.setVariable("cpName", cpName);

        params.setVariable("nomCompUser", user.getEbCompagnie().getNom());
        params.setVariable("nomEtabUser", user.getEbEtablissement().getNom());

        params.setVariable("refTra", refTra);
        params.setVariable("unitRef", unitRef);
        params.setVariable("status", status);
        params.setVariable("pays", pays);
        params.setVariable("ville", ville);
        params.setVariable("date", date);
        params.setVariable("ebdemande", ebDemande);

        Map<String, Object> mapParams = new HashMap<String, Object>();
        mapParams.put("ebTtEventNum", ttEvent.getEbTtEventNum());
        mapParams.put("ebTtEventUserNum", ttEvent.getxEbUser().getEbUserNum());
        mapParams.put("ebTtTracingNum", ebTracing.getEbTtTracingNum());

        Map<String, Object> res = serviceListStatique
            .getEmailUsers(
                user.getEbCompagnie().getEbCompagnieNum(),
                Enumeration.EmailConfig.MISE_A_JR_UN_PSL.getCode(),
                Enumeration.Module.TRACK.getCode(),
                ebDemande.getEbDemandeNum(),
                mapParams);

        notificationService
            .generateAlert(
                email,
                (List<Integer>) res.get("ids"),
                ttEvent.getxEbTtTracing().getEbTtTracingNum(),
                Enumeration.Module.TRACK.getCode());

        email.setEmailConfig(Enumeration.EmailConfig.MISE_A_JR_UN_PSL);

        emailHtmlSender
            .sendListEmail(
                (List<String>) res.get("emails"),
                email,
                "trackAndTrace/33-email-mise-a-jr-un-psl",
                params,
                subject);
    }
}
