package com.adias.mytowereasy.service;

import java.util.List;

import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.model.EbFlag;
import com.adias.mytowereasy.utils.search.PageableSearchCriteria;


public interface FlagService {
    public EbFlag addFlag(EbFlag ebFlag);

    public List<EbFlag> getListFlagByCompagnieNum(PageableSearchCriteria flagSearchCriteria);

    public List<EbFlag> getListFlagByCompagnieNum();

    public List<EbFlag> getListFlag(Integer moduleNum);

    public Long countListFlag(PageableSearchCriteria flagSearchCriteria);

    public void deleteFlag(Integer ebFlagNum);

    public List<EbFlagDTO> getListFlagDTOFromCodes(String listFlagCodes);
}
