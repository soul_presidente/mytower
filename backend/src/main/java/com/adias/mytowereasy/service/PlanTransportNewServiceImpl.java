package com.adias.mytowereasy.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.api.wso.PlanTransportWSO;
import com.adias.mytowereasy.dao.DaoPlanTransportNew;
import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.model.EbPlanTransportNew;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Service
public class PlanTransportNewServiceImpl extends MyTowerService implements PlanTransportNewService {
    @Autowired
    DaoPlanTransportNew daoPlanTransportNew;
    @Autowired
    FlagService flagService;

    public List<EbPlanTransportNew> listPlanTransport(SearchCriteria criteria) {
        List<EbPlanTransportNew> listPlanTransportNew = daoPlanTransportNew.list(criteria);
        listPlanTransportNew.forEach(plan -> {
            SearchCriteriaPricingBooking input = plan.getInput();
            String listFlagIcon = input.getListFlag() != null ? input.getListFlag().trim() : null;

            if (listFlagIcon != null && !listFlagIcon.isEmpty()) {
                plan.setListEbFlagDTO(EbFlagDTO.sortByName(flagService.getListFlagDTOFromCodes(listFlagIcon)));
            }

        });
        return listPlanTransportNew;
    }

    @Override
    public List<EbPlanTransportNew>
        listLoadPlanIsConstraints(SearchCriteriaPricingBooking criteria, PlanTransportWSO planTransportWSO) {
        List<EbPlanTransportNew> listPlanTransportNew = daoPlanTransportNew
            .listLoadPlanIsConstraints(criteria, planTransportWSO);
        return listPlanTransportNew.stream().peek(plan -> {
            SearchCriteriaPricingBooking input = plan.getInput();

            if (input != null) {
                String listFlagIcon = input.getListFlag() != null ? input.getListFlag().trim() : null;

                if (listFlagIcon != null && !listFlagIcon.isEmpty()) {
                    plan.setListEbFlagDTO(EbFlagDTO.sortByName(flagService.getListFlagDTOFromCodes(listFlagIcon)));
                }

            }

        }).collect(Collectors.toList());
    }

    public Long countListPlanTransport(SearchCriteria criteria) {
        return daoPlanTransportNew.listCount(criteria);
    }

    @Override
    public void deletePlanTransport(EbPlanTransportNew ebPlanTransportNew) {
        ebPlanTransportNewRepository.delete(ebPlanTransportNew);
    }

    public void save(EbPlanTransportNew ebPlanTransportNew) {
        ebPlanTransportNewRepository.save(ebPlanTransportNew);
    }

    @Override
    public EbPlanTransportNew saveFlags(Integer ebPlanTransportNum, String newFlags) {
        EbPlanTransportNew planTransportNew = ebPlanTransportNewRepository
            .findOneByEbPlanTransportNum(ebPlanTransportNum);
        SearchCriteriaPricingBooking input = planTransportNew.getInput();
        input.setListFlag(newFlags);
        planTransportNew.setInput(input);
        return ebPlanTransportNewRepository.save(planTransportNew);
    }
}
