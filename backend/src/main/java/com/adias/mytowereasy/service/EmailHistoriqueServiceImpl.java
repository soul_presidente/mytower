package com.adias.mytowereasy.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.model.EbEmailHistorique;


@Service
public class EmailHistoriqueServiceImpl extends MyTowerService implements EmailHistoriqueService {

    /*
     * @Override
     * public List<EbEmailHistorique> getAllEmailHistorique1() {
     * return ebEmailHistoriqueRepository.findAll();
     * // return ebEmailHistoriqueRepository.findMail(ebIncidentNum);
     * }
     */
    @Override
    @Transactional
    public List<EbEmailHistorique> getAllEmailHistorique(Integer ebIncidentNum) {
        // return ebEmailHistoriqueRepository.findAll();
        List<EbEmailHistorique> mails = ebEmailHistoriqueRepository.findByEbIncidentNum(ebIncidentNum);
        return mails;
    }
}
