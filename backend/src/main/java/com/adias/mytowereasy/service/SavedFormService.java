/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.io.IOException;
import java.util.List;

import com.adias.mytowereasy.dto.EbSavedFormSearchCriteriaDto;
import com.adias.mytowereasy.model.EbSavedForm;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface SavedFormService {
    public boolean deleteEbSavedForm(EbSavedForm ebSavedForm);

    public EbSavedForm upsertEbSavedForm(EbSavedForm ebSavedForm);

    public List<EbSavedForm> selectListEbSavedForm(SearchCriteria criterias);

    public void applyActionToHistory(Integer ebSavedFormNum) throws Exception;

    public void applyActionToHistory(EbSavedForm ebSavedForm) throws Exception;

    public List<EbSavedForm> selectListEbSavedFormByCompanie() throws IOException;

    public List<EbSavedFormSearchCriteriaDto>
        parseSavedSearch(Integer ebSavedFormNum, Boolean bindValueLabels) throws IOException, ClassNotFoundException;

    public List<EbSavedFormSearchCriteriaDto>
        parseSavedSearch(EbSavedForm savedForm, Boolean bindValueLabels) throws IOException, ClassNotFoundException;

    public SearchCriteria parseSavedSearchToSearchCriteria(Integer ebSavedFormNum)
        throws IOException,
        ClassNotFoundException,
        InstantiationException,
        IllegalAccessException;

    public SearchCriteria parseSavedSearchToSearchCriteria(EbSavedForm ebSavedForm)
        throws IOException,
        ClassNotFoundException,
        InstantiationException,
        IllegalAccessException;
}
