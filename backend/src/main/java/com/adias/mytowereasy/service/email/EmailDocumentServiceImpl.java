/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service.email;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.EmailConfig;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.ListStatiqueService;
import com.adias.mytowereasy.service.NotificationService;
import com.adias.mytowereasy.util.email.Email;
import com.adias.mytowereasy.util.email.EmailHtmlSender;


@Service
public class EmailDocumentServiceImpl implements EmailDocumentService {
    @Autowired
    ListStatiqueService listStatiqueService;

    private EmailHtmlSender emailHtmlSender;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ConnectedUserService connectedUserService;

    @Autowired
    private NotificationService notificationService;

    @Value("${mytowereasy.url.front}")
    private String url;

    @Autowired
    public EmailDocumentServiceImpl(EmailHtmlSender emailHtmlSender) {
        this.emailHtmlSender = emailHtmlSender;
    }

    @Override
    public void sendEmailDeleteDocument(
        EbUser user,
        List<String> emailToSend,
        EbDemande ebDemande,
        Integer numEntity,
        Integer module) {
        Context params = new Context();
        Email email = new Email();

        String refTran = ebDemande.getRefTransport();
        String nom = user.getNom();
        String prenom = user.getPrenom();

        String[] args = {
            refTran, prenom, nom
        };

        String subject = "suppressiondoc.subject";
        email.setArguments(args);

        email.setSubject(subject);

        params.setVariable("url", url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString()));
        params.setVariable("ebdemande", ebDemande);
        params.setVariable("user", user);

        Map<String, Object> res = listStatiqueService
            .getEmailUsers(
                user.getEbCompagnie().getEbCompagnieNum(),
                EmailConfig.SUPPRESSION_DOCUMENT.getCode(),
                Enumeration.Module.TRANSPORT_MANAGEMENT.getCode(),
                ebDemande.getEbDemandeNum());

        email.setEmailConfig(Enumeration.EmailConfig.SUPPRESSION_DOCUMENT);
        String connectedUserMail = user.getEmail();
        

		List<Integer> usersIds = (List<Integer>) res.get("ids");
		List<String> listDestinataireEmails = (List<String>) res.get("emails");

		if (Enumeration.StatutDemande.PND.getCode().equals(ebDemande.getxEcStatut())) {
			List<Integer> transporteursIds = ebDemande.getExEbDemandeTransporteurs().stream()
					.map(ExEbDemandeTransporteur::getxTransporteur).map(EbUser::getEbUserNum)
					.collect(Collectors.toList());

			usersIds = usersIds.stream().filter(userId -> !transporteursIds.contains(userId))
					.collect(Collectors.toList());

			List<String> transporteurEmails = ebDemande.getExEbDemandeTransporteurs().stream()
					.map(ExEbDemandeTransporteur::getxTransporteur)
					.map(transporteur -> transporteur.getEmail() + ";" + transporteur.getLanguage())
					.collect(Collectors.toList());

			listDestinataireEmails = listDestinataireEmails.stream().filter(mail -> !transporteurEmails.contains(mail))
					.collect(Collectors.toList());
		}

        notificationService.generateAlert(email, usersIds, numEntity, module);

        emailHtmlSender
            .sendListEmail(
            	listDestinataireEmails,
                email,
                "quotation/26-email-suppression-document",
                params,
                subject,
                connectedUserMail);
    }

    @Override
    public void sendMailUploadFile(EbUser user, EbDemande ebDemande, Integer module, Integer ebQmIncidentNum) {
        Context params = new Context();
        Email email = new Email();

        String refTran = ebDemande.getRefTransport();
        String nom = user.getNom();
        String prenom = user.getPrenom();

        String[] args = {
            refTran, prenom, nom
        };

        String subject = "ajoutdoc.subject";
        email.setArguments(args);

        email.setSubject(subject);

        email.setEmailConfig(EmailConfig.CHARGE_DOC_TM_TT);

        Integer numEntity = null;

        if (module != null && module.equals(Enumeration.Module.PRICING.getCode())) {
            params
                .setVariable("url", url.concat("app/pricing/details/").concat(ebDemande.getEbDemandeNum().toString()));
            numEntity = ebDemande.getEbDemandeNum();
        }
        else if (module != null && module.equals(Enumeration.Module.TRANSPORT_MANAGEMENT.getCode())) {
            params
                .setVariable(
                    "url",
                    url.concat("app/transport-management/details/").concat(ebDemande.getEbDemandeNum().toString()));
            numEntity = ebDemande.getEbDemandeNum();
        }
        else if (module != null && module.equals(Enumeration.Module.TRACK.getCode())) {
            params
                .setVariable(
                    "url",
                    url.concat("app/track-trace/details/").concat(ebDemande.getEbDemandeNum().toString()));
            numEntity = ebQmIncidentNum;
        }
        else if (module != null && module.equals(Enumeration.Module.QUALITY_MANAGEMENT.getCode())) {
            numEntity = ebQmIncidentNum;
        }

        params.setVariable("ebdemande", ebDemande);
        params.setVariable("user", user);

        Map<String, Object> res = listStatiqueService
            .getEmailUsers(
                user.getEbCompagnie().getEbCompagnieNum(),
                EmailConfig.CHARGE_DOC_TM_TT.getCode(),
                Enumeration.Module.TRANSPORT_MANAGEMENT.getCode(),
                ebDemande.getEbDemandeNum());


		List<Integer> usersIds = (List<Integer>) res.get("ids");
		List<String> listDestinataireEmails = (List<String>) res.get("emails");

		if (Enumeration.StatutDemande.PND.getCode().equals(ebDemande.getxEcStatut())) {
			List<Integer> transporteursIds = ebDemande.getExEbDemandeTransporteurs().stream()
					.map(ExEbDemandeTransporteur::getxTransporteur).map(EbUser::getEbUserNum)
					.collect(Collectors.toList());

			usersIds = usersIds.stream().filter(userId -> !transporteursIds.contains(userId))
					.collect(Collectors.toList());

			List<String> transporteurEmails = ebDemande.getExEbDemandeTransporteurs().stream()
					.map(ExEbDemandeTransporteur::getxTransporteur)
					.map(transporteur -> transporteur.getEmail() + ";" + transporteur.getLanguage())
					.collect(Collectors.toList());

			listDestinataireEmails = listDestinataireEmails.stream().filter(mail -> !transporteurEmails.contains(mail))
					.collect(Collectors.toList());
		}
		
		notificationService.generateAlert(email, usersIds, numEntity, module);

        String connectedUserMail = user.getEmail();
        params.setVariable("demandeId", ebDemande.getEbDemandeNum());
        emailHtmlSender
            .sendListEmail(
            	listDestinataireEmails,
                email,
                "document/25-email-charge-doc-tm-tt",
                params,
                subject,
                connectedUserMail);

    }
}
