/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.service;

import java.math.BigInteger;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import com.adias.mytowereasy.airbus.dao.UserProfileDao;
import com.adias.mytowereasy.airbus.model.EbUserProfile;
import com.adias.mytowereasy.dto.ExUserModuleDTO;
import com.adias.mytowereasy.dto.mapper.EcUserModuleMapper;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.EtatRelation;
import com.adias.mytowereasy.model.Enumeration.UserStatus;
import com.adias.mytowereasy.repository.EbAclRelationRepository;
import com.adias.mytowereasy.repository.ExEbUserModuleRepository;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Service
public class UserServiceImpl extends MyTowerService implements UserService {
    public static int tokenVersion;

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ConnectedUserService connectedUserService;

    @PersistenceContext
    EntityManager em;

    @Autowired
    ExEbUserModuleRepository exEbUserModuleRepository;

    @Autowired
    EcUserModuleMapper ecUserModuleMapper;

    @Autowired
    UserProfileDao userProfileDao;

    @Autowired
    EtablissementService etablissementService;

    @Autowired
    EbAclRelationRepository ebAclRelationRepository;

    public UserServiceImpl() {
    }

    @Override
    public Set<ExUserModuleDTO> getUserAccessRights(Integer ebUserNum) {
        Set<ExUserModule> accessRights = exEbUserModuleRepository.findAllByEbUserNum(ebUserNum);
        return ecUserModuleMapper.listUserModuleToListUserModuleDTO(accessRights);
    }

    @Override
    public EbUser getEbUserByEmail(String email) {
        EbUser user = ebUserRepository.findOneByEmailIgnoreCase(email);
        return user;
    }

    @Override
    public EbUser getEbUserByResetToken(String resetToken) {
        EbUser user = ebUserRepository.findOneByResetToken(resetToken);
        return user;
    }

    @Override
    public EbUser changeUserPassword(EbUser ebUser) {

        if (connectedUserService.getCurrentUser() != null && ebUser.getPassword() != null) {
            ebUser.setPassword(passwordEncoder.encode(ebUser.getPassword()));
            connectedUserService.getCurrentUser().setPassword(ebUser.getPassword());
            ebUserRepository.updatePasswordEbUser(ebUser.getEbUserNum(), ebUser.getPassword());
            return ebUser;
        }
        else return null;

    }

    @Override
    public EbUser updateEbUser(EbUser ebUser) {
        return ebUserRepository.save(ebUser);
    }

    @Override
    public Object updateEbUserLanguage(EbUser ebUser) {
        if (ebUser.getFuseauHoraire() != null) ebUserRepository
            .updateUserLanguageAndFuseauHoraire(ebUser.getEbUserNum(), ebUser.getLanguage(), ebUser.getFuseauHoraire());
        else ebUserRepository.updateUserLanguage(ebUser.getEbUserNum(), ebUser.getLanguage());

        return 1;
    }

    @Override
    public EbUser updateStatusEbUser(EbUser ebUser) {
        EbUser oldUser = ebUserRepository.findOneByEbUserNum(ebUser.getEbUserNum());

        if (ebUser.getStatus().equals(UserStatus.ACTIF.getCode())) {
            if (oldUser != null
                && oldUser.getStatus().equals(UserStatus.NON_ACTIF.getCode())) setRelationsByEmail(oldUser);
        }

        if (ebUser.getStatus().equals(UserStatus.NON_ACTIF.getCode())) {
            ebUser.setStatus(UserStatus.ACCEPTER_PAR_ADMIN.getCode());
            ebUserRepository.updateStatusEbUser(ebUser.getEbUserNum(), ebUser.getStatus());

            sendStatutChangeMail(oldUser);
        }
        else if (ebUser.getStatus().equals(UserStatus.ACCEPTER_PAR_ADMIN.getCode())) {
            sendStatutChangeMail(oldUser);
        }
        else if (ebUser.getStatus().equals(UserStatus.DISACTIVE.getCode())) {
            ebUser.setStatus(UserStatus.ACTIF.getCode());
            ebUserRepository.updateStatusEbUser(ebUser.getEbUserNum(), ebUser.getStatus());
        }
        else if (ebUser.getStatus().equals(UserStatus.ACTIF.getCode())) {
            Integer numberOfAdminForEtablissement = ebUserRepository
                .CountEbUserAdminOfEtablissement(ebUser.getEbEtablissement().getEbEtablissementNum());

            if (numberOfAdminForEtablissement > 1) {

                if (oldUser.getPassword() == null) {
                    String resetToken = Base64
                        .getEncoder().encodeToString((ebUser.getEmail() + System.currentTimeMillis()).getBytes());
                    ebUser.setResetToken(resetToken);
                }
                else if (oldUser.getPassword() != null) {
                    ebUser.setStatus(UserStatus.DISACTIVE.getCode());
                    ebUserRepository.updateStatusEbUser(ebUser.getEbUserNum(), ebUser.getStatus());
                    // ne peut pas fonctionner a cause de la condition ligne 180
                    // if (numberOfAdminForEtablissement > 1)
                    /*
                     * else if (numberOfAdminForEtablissement == 1)
                     * {
                     * if (ebUser.isAdmin())
                     * {
                     * ebUser.setUniqueAdminOfEtablissement(true);
                     * }
                     * else
                     * {
                     * ebUser.setStatus(Enumeration.UserStatus.DISACTIVE.getCode
                     * ());
                     * ebUserRepository.updateStatusEbUser(ebUser.getEbUserNum()
                     * , ebUser.getStatus());
                     * }
                     * }
                     */
                }

            }
            else if (numberOfAdminForEtablissement == 1) {

                if (ebUser.isAdmin()) {
                    ebUser.setUniqueAdminOfEtablissement(true);
                }
                else {
                    ebUser.setStatus(UserStatus.DISACTIVE.getCode());
                    ebUserRepository.updateStatusEbUser(ebUser.getEbUserNum(), ebUser.getStatus());
                }

            }
            else {
            }

        }

        return ebUser;
    }

    void sendStatutChangeMail(EbUser ebUser) {

        if (ebUser.isSuperAdmin()) {
            emailService.sendEmailActivePassword(ebUser);
        }
        else {
            emailService.sendEmailCompteActiverParAdminSocieter(ebUser);
        }

    }

    @Override
    public Integer updateAdminAndSuperAdminUser(EbUser ebUser) {
        return ebUserRepository
            .updateAdminAndSuperAdminUser(ebUser.getEbUserNum(), ebUser.isAdmin(), ebUser.isSuperAdmin());
    }

    @Override
    public EbUser updateUserDetails(EbUser ebUser) {
        Optional<EbUser> oldUser = ebUserRepository.findUserByEbUserNum(ebUser.getEbUserNum());

        if (oldUser.isPresent()) {
            // deleting old access rights
            Set<ExUserModule> oldListAccessRights = exEbUserModuleRepository.findAllByEbUserNum(ebUser.getEbUserNum());
            exEbUserModuleRepository.deleteAll(oldListAccessRights);

            // saving new access rights
            Set<ExUserModule> listExUserModule = ebUser.getListAccessRights();

            for (ExUserModule exUserModule: listExUserModule) {
                exUserModule.setEbUser(new EbUser(ebUser.getEbUserNum()));
                exUserModule.setEbUserNum(ebUser.getEbUserNum());
                exUserModule.setModule(new EcModule(exUserModule.getEcModuleNum()));
            }

            exEbUserModuleRepository.saveAll(listExUserModule);

            if (ebUser.getListCategories() != null) {
                String labels = "";

                for (EbCategorie categorie: ebUser.getListCategories()) {

                    if (CollectionUtils.isNotEmpty(categorie.getLabels())) {

                        for (EbLabel label: categorie.getLabels()) {
                            if (label.getEbLabelNum()
                                != null) labels += (!labels.isEmpty() ? "," : "") + label.getEbLabelNum();
                        }
                    }
                }

                labels = labels.equals("") ? null : labels;
                ebUser.setListLabels(labels);
            }

            oldUser.get().setNom(ebUser.getNom());
            oldUser.get().setUsername(ebUser.getUsername());
            oldUser.get().setPrenom(ebUser.getPrenom());
            oldUser.get().setEmail(ebUser.getEmail());
            oldUser.get().setAdmin(ebUser.isAdmin());
            oldUser.get().setSuperAdmin(ebUser.isSuperAdmin());
            oldUser.get().setStatus(ebUser.getStatus());
            oldUser.get().setListCategories(ebUser.getListCategories());
            oldUser.get().setTelephone(ebUser.getTelephone());
            oldUser.get().setListAccessRights(null);
            oldUser.get().setListLabels(ebUser.getListLabels());
            oldUser.get().setGroupes(ebUser.getGroupes());
            oldUser.get().setEnabled(ebUser.getEnabled());

            if (ebUser.getEbUserProfile() != null && ebUser.getEbUserProfile().getEbUserProfileNum() != null
                && ebUser.getEbUserProfile().getEbUserProfileNum() > 0) {
                oldUser.get().setEbUserProfile(ebUser.getEbUserProfile());
            }
            else {
                oldUser.get().setEbUserProfile(null);
            }

            if (Enumeration.Role.ROLE_PRESTATAIRE.getCode().equals(ebUser.getRole())) {
                if (ebUser.getService() != null) oldUser.get().setService(ebUser.getService());
            }

            if (ebUser.getPassword() == null && ebUser.getStatus() == UserStatus.ACTIF.getCode()) {
                String resetToken = Base64
                    .getEncoder().encodeToString((ebUser.getEmail() + System.currentTimeMillis()).getBytes());
                oldUser.get().setResetToken(resetToken);

                // C'est quoi l'utilité de setter activeToken si lors du
                // changement de mot de passe on se base sur resetToken
                // oldUser.get().setActiveToken(null);
            }

            // saving user
            return ebUserRepository.save(oldUser.get());
        }

        return null;
    }

    @Override
    public void resetPassword(EbUser ebUser) {
        String resetToken = Base64
            .getEncoder().encodeToString((ebUser.getEmail() + System.currentTimeMillis()).getBytes());

        ebUser.setResetToken(resetToken);

        ebUserRepository.updateUserResetToken(ebUser.getEbUserNum(), ebUser.getResetToken());

        emailService.sendEmailResetPassword(ebUser);
    }

    @Override
    public void updateEbUserPasswordAndResetToken(EbUser ebUser) {
        ebUser.setPassword(passwordEncoder.encode(ebUser.getPassword()));
        ebUser.setStatus(Enumeration.UserStatus.ACTIF.getCode());
        ebUserRepository
            .updatePasswordAndResetTokenEbUser(
                ebUser.getEbUserNum(),
                ebUser.getPassword(),
                ebUser.getResetToken(),
                ebUser.getStatus());
        // daoUser.updatePasswordEbUser(ebUser);

        emailService.sendEmailUpdatedPassword(ebUser);
    }

    @Override
    public EbUser getEbUserByEbUserNum(Integer ebUserNum) {
        // System.out.println("test");
        // TODO Auto-generated method stub
        return ebUserRepository.findOneByEbUserNum(ebUserNum);
    }

    @Override
    public EbEtablissement updateEbEtablissement(EbEtablissement ebEtablissement) {
        // TODO Auto-generated method stub
        return ebEtablissementRepository.save(ebEtablissement);
    }

    @Override
    public boolean matchUserPassword(SearchCriteria criteria) {
        // TODO Auto-generated method stub

        EbUser ebUser = ebUserRepository.findOneByEbUserNum(criteria.getEbUserNum());

        if (ebUser != null && passwordEncoder.matches(criteria.getEbUserPassword(), ebUser.getPassword())) {
            return true;
        }
        else return false;

    }

    // @Override
    // public List<EbUser> getEbUserByEtablissement(EbEtablissement
    // ebEtablissement) {
    // // TODO Auto-generated method stub
    // return ebUserRepository.findByEbEtablissement(ebEtablissement);
    // }

    @Override
    public List<EbUser> getAllUser() {
        // TODO Auto-generated method stub
        return ebUserRepository.findAll();
    }

    @Override
    public EbUser getEbUserByActiveToken(String activeToken) {
        return ebUserRepository.findOneByActiveToken(activeToken);
    }

    @Override
    public Boolean activateAccount(String token) {
        Boolean result = false;

        try {
            EbUser ebUser = ebUserRepository.findOneByActiveToken(token);

            if (ebUser != null) {
                // verifier si l'user a des MER en attente et les appliquer44
                setRelationsByEmail(ebUser);

                ebUser.setListAccessRights(null);
                ebUser.setStatus(UserStatus.ACTIF.getCode());
                ebUser.setActiveToken(null);
                ebUserRepository.save(ebUser);
                result = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            result = false;
            throw e;
        }

        return result;
    }

    @Transactional
    public Boolean setRelationsByEmail(EbUser ebUser) {

        if (ebUser.getStatus().equals(Enumeration.UserStatus.NON_ACTIF.getCode())) {
            List<EbRelation> listEbRelation = ebRelationRepository.findAllByEmail(ebUser.getEmail());

            for (EbRelation ebRelation: listEbRelation) {
                boolean condition = false;
                boolean subCondition = false;

                subCondition = ebRelation.getTypeRelation() != null;
                subCondition = subCondition && ebUser.getRole().equals(Enumeration.Role.ROLE_PRESTATAIRE.getCode());
                subCondition = subCondition && (ebRelation.getTypeRelation().equals(ebUser.getService())
                    || ebUser.getService().equals(Enumeration.ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode()));

                condition = (ebRelation.getTypeRelation() == null
                    && ebUser.getRole().equals(Enumeration.Role.ROLE_CHARGEUR.getCode()));
                condition = ebRelation.getEtat().equals(EtatRelation.EN_COURS.getCode()) && (condition || subCondition);

                if (condition) {
                    ebRelation
                        .setxEbEtablissement(new EbEtablissement(ebUser.getEbEtablissement().getEbEtablissementNum()));
                    ebRelation.setGuest(ebUser);
                    ebRelation
                        .setEtat(
                            ebRelation.getTypeRelation() != null ?
                                Enumeration.EtatRelation.ACTIF.getCode() :
                                Enumeration.EtatRelation.EN_COURS.getCode());
                    setLastModifiedByConnectedUser(ebRelation);
                    ebRelationRepository.save(ebRelation);

                    SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();
                    criteria.setEmail(ebUser.getEmail());
                    criteria.setForEmail(true);
                    List<ExEbDemandeTransporteur> listExDemandeTransporteur = daoPricing
                        .getListExEbTransporteur(criteria);

                    if (listExDemandeTransporteur != null && listExDemandeTransporteur.size() > 0) {
                        listExDemandeTransporteur.get(0).setxTransporteur(new EbUser(ebUser.getEbUserNum()));
                        listExDemandeTransporteur
                            .get(0).setxEbCompagnie(new EbCompagnie(ebUser.getEbCompagnie().getEbCompagnieNum()));
                        listExDemandeTransporteur
                            .get(0).setxEbEtablissement(
                                new EbEtablissement(ebUser.getEbEtablissement().getEbEtablissementNum()));

                        exEbDemandeTransporteurRepository
                            .updateTransporteurEtablissement(
                                listExDemandeTransporteur.get(0).getxTransporteur(),
                                listExDemandeTransporteur.get(0).getxEbEtablissement(),
                                listExDemandeTransporteur.get(0).getxEbCompagnie(),
                                listExDemandeTransporteur.get(0).getExEbDemandeTransporteurNum());
                    }

                    return true;
                }

            }

        }

        return false;
    }

    private void setLastModifiedByConnectedUser(EbRelation ebRelation) {
        Integer connectedUserId = connectedUserService.getCurrentUser() != null ?
            connectedUserService.getCurrentUser().getEbUserNum() :
            ebRelation.getGuest().getEbUserNum();
        Integer hostId = ebRelation.getHost() != null ? ebRelation.getHost().getEbUserNum() : null;
        Integer guestId = ebRelation.getGuest() != null ? ebRelation.getGuest().getEbUserNum() : null;

        if (hostId != null && hostId.equals(connectedUserId) || guestId != null
            && guestId.equals(connectedUserId)) ebRelation.setLastModifiedBy(connectedUserService.getCurrentUser());
    }

    @Override
    public EbDelegation createDelegation(EbDelegation ebDelegation) {
        EbUser userBackup = new EbUser(ebDelegation.getUserBackUp());
        EbUser userConnected = new EbUser(ebDelegation.getUserConnect());

        EbDelegation deleg = ebDelegationRepository.save(ebDelegation);

        emailService.sendEmailBackup(userBackup, userConnected);
        return deleg;
    }

    @Override
    public Integer deleteDelegation(Integer ebDelegationNum) {
        ebDelegationRepository.deleteById(ebDelegationNum);
        return 1;
    }

    @Override
    public EbDelegation getDelegation(SearchCriteria criteria) {
        EbDelegation ebDelegation = ebDelegationRepository
            .findFirstByUserConnect_ebUserNumOrderByEbDelegationNumDesc(criteria.getEbUserNum());
        return ebDelegation != null ? ebDelegation : new EbDelegation();
    }

    @Override
    public EbUser selectEbUser(SearchCriteria criterias) {
        if (criterias == null) criterias = new SearchCriteria();

        Optional<EbUser> ebUserOptional = ebUserRepository.findUserByEbUserNum(criterias.getEbUserNum());
        EbUser ebUser = null;

        if (ebUserOptional.isPresent()) {
            ebUser = ebUserOptional.get();

            if (criterias.isWithAccessRights()) {
                Set<ExUserModule> listExUserModule = this.exEbUserModuleRepository
                    .findAllByEbUserNum(ebUser.getEbUserNum());
                ebUser.setListAccessRights(listExUserModule);
            }

            if (criterias.isWithEtablissement()) {
                EbEtablissement ebEtablissement = ebEtablissementRepository
                    .findOneWithDetailsByEbEtablissementNum(ebUser.getEbEtablissement().getEbEtablissementNum());
                ebEtablissement.setEbCompagnie(null);
                ebUser.setEbEtablissement(ebEtablissement);
            }

            EbCompagnie ebCompagnie = new EbCompagnie();
            ebCompagnie.setEbCompagnieNum(ebUser.getEbCompagnie().getEbCompagnieNum());
            ebUser.setEbCompagnie(ebCompagnie);
        }

        return ebUser;
    }

    @Override
    public List<EbUser> selectListContact(SearchCriteria criteria) {
        return (List<EbUser>) daoUser.selectListContact(criteria);
    }

    @Override
    public BigInteger selectCountListContact(SearchCriteria criteria) {
        return daoUser.selectCountListContact(criteria);
    }

    @Override
    public EbRelation updateEbRelation(EbRelation ebRelation) {
        setLastModifiedByConnectedUser(ebRelation);
        return ebRelationRepository.saveAndFlush(ebRelation);
    }

    // @Override
    // public List<EbContactRequest>
    // selectListSendingRequestByUser(SearchCriteria criteria)
    // {
    // // TODO Auto-generated method stub
    // EbUser ebUser = new EbUser();
    // ebUser.setEbUserNum(criteria.getEbUserNum());
    // return ebContactRequestRepository.findByExpediteurAndStatus(ebUser,
    // criteria.getStatus());
    // }
    //
    // @Override
    // public List<EbContactRequest>
    // selectListReceivingRequestByUser(SearchCriteria criteria)
    // {
    // // TODO Auto-generated method stub
    // EbUser ebUser = new EbUser();
    // ebUser.setEbUserNum(criteria.getEbUserNum());
    // return ebContactRequestRepository.findByDestinataireAndStatus(ebUser,
    // criteria.getStatus());
    // }

    @Override
    @Transactional
    public Boolean accepterContactRequest(EbRelation ebRelation) {
        boolean result = false;

        try {
            // TODO Auto-generated method stub
            ebRelation.setEtat(EtatRelation.ACTIF.getCode());
            setLastModifiedByConnectedUser(ebRelation);
            ebRelationRepository.save(ebRelation);
            result = true;
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Erreur");
            result = false;
            throw e;
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EbRelation> selectListEbEtablissementOfEbRelation(SearchCriteria criterias) {
        criterias.setCommunityEstablishment(true);
        return (List<EbRelation>) daoUser.selectListContact(criterias);
    }

    @Override
    public BigInteger selectCountListEbEtablissementOfEbRelation(SearchCriteria criterias) {
        criterias.setCommunityEstablishment(true);
        return daoUser.selectCountListContact(criterias);
    }

    @Transactional
    @Override
    public Boolean ignorerContactRequest(EbRelation ebRelation) {
        boolean result = false;

        try {
            ebRelation.setEtat(EtatRelation.IGNORER.getCode());
            setLastModifiedByConnectedUser(ebRelation);
            ebRelationRepository.save(ebRelation);
            result = true;
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Erreur");
            result = false;
            throw e;
        }

        return result;
    }

    @Override
    public Boolean annulerContactRequest(EbRelation ebRelation) {
        boolean result = false;

        try {
            Optional<EbRelation> oldRel = ebRelationRepository.findById(ebRelation.getEbRelationNum());
            oldRel.get().setEtat(EtatRelation.ANNULER.getCode());
            setLastModifiedByConnectedUser(oldRel.get());
            ebRelationRepository.save(oldRel.get());
            // ebRelationRepository.delete(ebRelation.getEbRelationNum());
            result = true;
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println("Erreur : UserServiceImpl > annulerContactRequest");
            e.printStackTrace();
            result = false;
            throw e;
        }

        return result;
    }

    // @Override
    // @Transactional
    // public List<EbContactRequest> selectListDemandesRecues(SearchCriteria
    // criteria)
    // {
    // // TODO Auto-generated method stub
    // return daoUser.selectListDemandesRecues(criteria);
    // }
    //
    // @Override
    // public List<EbContactRequest> selectListDemandesEnvoyees(SearchCriteria
    // criteria)
    // {
    // // TODO Auto-generated method stub
    // return daoUser.selectListDemandeEnvoyees(criteria);
    // }

    @Override
    public List<EbUserGroup> getListUserGroup(EbUser ebUser) {
        return daoGroupUser.getListUserGroup(ebUser);
    }

    @Override
    public Boolean updateUserGroup(SearchCriteria criterias, Integer ebUserNum) {
        return daoUser.updateUserGroup(criterias, ebUserNum);
    }

    @Override
    public List<EbUser> selectListEbUser(SearchCriteria criterias) {
        return daoUser.selectListEbUser(criterias);
    }

    @Override
    public long selectCountListUser(SearchCriteria criteria) {
        return daoUser.selectCountListUser(criteria);
    }

    @Override
    public EbRelation sendInvitation(EbRelation ebRelation) {
        EbRelation ebRelationNew = new EbRelation();
        setLastModifiedByConnectedUser(ebRelation);

        ebRelationNew.setEtat(ebRelation.getEtat());
        ebRelationNew.setEmail(ebRelation.getEmail());

        ebRelationNew.setTypeRelation(ebRelation.getTypeRelation());

        ebRelationNew.setLastModifiedBy(ebRelation.getLastModifiedBy());

        ebRelationNew.setLastModifiedBy(ebRelation.getLastModifiedBy());
        if (ebRelation.getxEbEtablissementHost() != null) ebRelationNew
            .setxEbEtablissementHost(new EbEtablissement(ebRelation.getxEbEtablissementHost().getEbEtablissementNum()));
        if (ebRelation.getxEbEtablissement() != null) ebRelationNew
            .setxEbEtablissement(new EbEtablissement(ebRelation.getxEbEtablissement().getEbEtablissementNum()));
        if (ebRelation.getHost() != null) ebRelationNew.setHost(new EbUser(ebRelation.getHost().getEbUserNum()));
        if (ebRelation.getGuest() != null) ebRelationNew.setGuest(new EbUser(ebRelation.getGuest().getEbUserNum()));
        if (ebRelation.getxEbEtablissement() != null) ebRelationNew
            .setxEbEtablissement(new EbEtablissement(ebRelation.getxEbEtablissement().getEbEtablissementNum()));

        if (ebRelation.getGuest() != null
            && Enumeration.Role.ROLE_CONTROL_TOWER.getCode().equals(ebRelation.getGuest().getRole())) ebRelationNew
                .setEtat(Enumeration.EtatRelation.EN_COURS.getCode());

        EbRelation rel = ebRelationRepository.save(ebRelationNew);

        try {
            Optional<EbUser> opt = ebUserRepository.findById(ebRelationNew.getHost().getEbUserNum());
            EbUser expediteur = opt.isPresent() ? opt.get() : null;
            List<EbUser> destinataires = null;

            if (ebRelationNew.getFlagEtablissement() == null || !ebRelationNew.getFlagEtablissement()) {
                opt = ebUserRepository.findById(ebRelationNew.getGuest().getEbUserNum());
                destinataires = opt.isPresent() ? Arrays.asList(opt.get()) : new ArrayList<EbUser>();
            }
            else {
                destinataires = ebUserRepository
                    .findAllByAdminAndEbEtablissement(
                        true,
                        ebRelationNew.getxEbEtablissement().getEbEtablissementNum());
            }

            emailService.sendEmailInviterMyCommunity(destinataires, expediteur, "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rel;
    }

    @Override
    public List<EbUser> selectListBroker(SearchCriteria criteria) {
        return (List<EbUser>) daoUser.selectListContact(criteria);
    }

    @Override
    public List<EbUser> selectListTransporteur(SearchCriteria criteria) {
        return (List<EbUser>) daoUser.selectListContact(criteria);
    }

    @Override
    public List<EbUser> listTransporteurControlTower(SearchCriteria criteria) {
        return (List<EbUser>) daoUser.selectListContact(criteria);
    }

    @Override
    public Map sendInvitationByMail(EbRelation ebRelation, Integer serviceToIgnore, Integer ebUserNum) {
        Map result = new HashMap<String, Object>();
        String message = "";
        Integer etat = null;
        boolean inRelation = false;

        try {
            EbUser user = ebUserRepository.findOneByEmailIgnoreCase(ebRelation.getEmail());
            EbUser connectedUser = ebUserRepository
                .getOneByEbUserNum(
                    ebUserNum != null ? ebUserNum : connectedUserService.getCurrentUser().getEbUserNum());

            setLastModifiedByConnectedUser(ebRelation);

            // si guest exist
            if (user != null) {
                // on cherche s'il y a deja une relation avec le guest en tant
                // que host
                EbRelation oldEbRelation = ebRelationRepository
                    .findOneByGuest_ebUserNumAndHost_ebUserNum(user.getEbUserNum(), connectedUser.getEbUserNum());
                if (oldEbRelation == null) // sinon on cherche une relation en
                                           // tant que guest
                    oldEbRelation = ebRelationRepository
                        .findOneByGuest_ebUserNumAndHost_ebUserNum(connectedUser.getEbUserNum(), user.getEbUserNum());

                if (oldEbRelation == null) { // sinon on cherche si on a déjà
                                             // envoyé un mail au guest
                    List<EbRelation> listEbRelation = ebRelationRepository
                        .findAllByEmailAndHost_ebUserNum(user.getEmail(), connectedUser.getEbUserNum());
                    if (listEbRelation != null && listEbRelation.size() > 0) oldEbRelation = listEbRelation.get(0);
                }

                // s'il y a une relation
                if (oldEbRelation != null) {
                    if (oldEbRelation
                        .getEtat().equals(
                            Enumeration.EtatRelation.ACTIF
                                .getCode())) message = "This user is already in your contact list";
                    else if (oldEbRelation.getEtat().equals(Enumeration.EtatRelation.BLACKLIST.getCode())) {
                        if (oldEbRelation
                            .getLastModifiedBy().getEbUserNum()
                            .equals(connectedUser.getEbUserNum())) message = "This user is in your blacklist";
                        else message = "Your request cannot be processed";
                    }
                    else if (oldEbRelation.getEtat().equals(Enumeration.EtatRelation.EN_COURS.getCode())) {
                        if (oldEbRelation
                            .getLastModifiedBy().getEbUserNum()
                            .equals(connectedUser.getEbUserNum())) message = "This user is still in your request list";
                        else message = "This user is in your received request list";
                    }
                    else if (oldEbRelation.getEtat().equals(Enumeration.EtatRelation.ANNULER.getCode())) {
                        oldEbRelation.setHost(ebRelation.getHost());
                        oldEbRelation.setGuest(user);
                        oldEbRelation
                            .setxEbEtablissement(
                                new EbEtablissement(user.getEbEtablissement().getEbEtablissementNum()));
                        oldEbRelation.setTypeRelation(ebRelation.getTypeRelation());

                        if (connectedUser.getRole().equals(Enumeration.Role.ROLE_CHARGEUR.getCode())
                            && !oldEbRelation
                                .getTypeRelation().equals(Enumeration.ServiceType.SERVICE_CONTROL_TOWER.getCode())
                            || connectedUser.getRole().equals(Enumeration.Role.ROLE_CONTROL_TOWER.getCode())) {
                            oldEbRelation.setEtat(Enumeration.EtatRelation.ACTIF.getCode());
                            message = "This user is now in your contact list";
                        }
                        else {
                            oldEbRelation.setEtat(Enumeration.EtatRelation.EN_COURS.getCode());
                            message = "Your request is being processed";
                        }

                        ebRelation.setxEbEtablissementHost(ebRelation.getHost().getEbEtablissement());
                        ebRelation.setLastModifiedBy(new EbUser(ebRelation.getHost().getEbUserNum()));
                        ebRelationRepository.save(oldEbRelation);
                    }
                    else message = "Your request cannot be processed";
                }
                // s'il n'y a aucune relation existante
                else {

                    if (connectedUser.getRole().equals(Enumeration.Role.ROLE_CHARGEUR.getCode())) {

                        // si la type de service demandé est OK
                        if (user.getRole().equals(Enumeration.Role.ROLE_PRESTATAIRE.getCode())
                            && (ebRelation.getTypeRelation() != null && user.getService() != null
                                && ebRelation.getTypeRelation().equals(user.getService())
                            // || user.getService() == null &&
                            // ebRelation.getTypeRelation() == null
                            ) || Enumeration.ServiceType.SERVICE_BROKER_TRANSPORTEUR
                                .getCode().equals(user.getService())) {
                            ebRelation.setxEbEtablissementHost(ebRelation.getHost().getEbEtablissement());
                            ebRelation
                                .setxEbEtablissement(
                                    new EbEtablissement(user.getEbEtablissement().getEbEtablissementNum()));
                            ebRelation.setGuest(new EbUser(user.getEbUserNum()));
                            ebRelation.setEmail(null);
                            ebRelation.setEtat(Enumeration.EtatRelation.ACTIF.getCode());
                            ebRelation = ebRelationRepository.save(ebRelation);
                            message = "This user is now in your contact list";
                            inRelation = true;
                        }
                        // si le service demandé n'est pas ok
                        else if (user.getRole().equals(Enumeration.Role.ROLE_PRESTATAIRE.getCode())
                            && (serviceToIgnore == null || serviceToIgnore != user.getService())) {
                            ebRelation.setxEbEtablissementHost(ebRelation.getHost().getEbEtablissement());
                            ebRelation
                                .setxEbEtablissement(
                                    new EbEtablissement(user.getEbEtablissement().getEbEtablissementNum()));
                            ebRelation.setGuest(new EbUser(user.getEbUserNum()));
                            ebRelation.setEmail(null);
                            ebRelation.setTypeRelation(null);
                            ebRelation.setEtat(Enumeration.EtatRelation.EN_COURS.getCode());
                            ebRelation = ebRelationRepository.save(ebRelation);
                            message = "The service you requested for is not applicable for this user, please check your sent request";
                            inRelation = true;
                        }
                        else if (user.getRole().equals(Enumeration.Role.ROLE_CONTROL_TOWER.getCode())) {
                            ebRelation.setxEbEtablissementHost(ebRelation.getHost().getEbEtablissement());
                            ebRelation
                                .setxEbEtablissement(
                                    new EbEtablissement(user.getEbEtablissement().getEbEtablissementNum()));
                            ebRelation.setGuest(new EbUser(user.getEbUserNum()));
                            ebRelation.setEmail(null);
                            ebRelation.setTypeRelation(Enumeration.ServiceType.SERVICE_CONTROL_TOWER.getCode());
                            ebRelation.setEtat(Enumeration.EtatRelation.EN_COURS.getCode());
                            ebRelation = ebRelationRepository.save(ebRelation);
                            message = "Your request is being processed";
                            inRelation = true;
                        }
                        // si guest est un chargeur
                        else {
                            message = "You cannot add another shipper to your community";
                        }

                    }
                    else if (connectedUser.getRole().equals(Enumeration.Role.ROLE_CONTROL_TOWER.getCode())) {

                        // si guest n'est pas un control tower
                        if (!user.getRole().equals(Enumeration.Role.ROLE_CONTROL_TOWER.getCode())) {
                            ebRelation.setxEbEtablissementHost(ebRelation.getHost().getEbEtablissement());
                            ebRelation
                                .setxEbEtablissement(
                                    new EbEtablissement(user.getEbEtablissement().getEbEtablissementNum()));
                            ebRelation.setGuest(new EbUser(user.getEbUserNum()));
                            ebRelation.setEmail(null);
                            ebRelation.setTypeRelation(Enumeration.ServiceType.SERVICE_CONTROL_TOWER.getCode());
                            ebRelation.setEtat(Enumeration.EtatRelation.ACTIF.getCode());
                            ebRelation = ebRelationRepository.save(ebRelation);
                            message = "This user is now in your contact list";
                            inRelation = true;
                        }
                        // si guest est un control tower
                        else {
                            message = "You cannot add another shipper to your community";
                        }

                    }
                    // si host est un prestataire
                    else {

                        // si guest est un chargeur
                        if (user.getRole().equals(Enumeration.Role.ROLE_CHARGEUR.getCode())) {
                            ebRelation
                                .setxEbEtablissementHost(
                                    new EbEtablissement(connectedUser.getEbEtablissement().getEbEtablissementNum()));
                            ebRelation
                                .setxEbEtablissement(
                                    new EbEtablissement(user.getEbEtablissement().getEbEtablissementNum()));
                            ebRelation.setGuest(new EbUser(user.getEbUserNum()));
                            ebRelation.setEmail(null);
                            ebRelation.setTypeRelation(user.getService());
                            ebRelation.setEtat(Enumeration.EtatRelation.EN_COURS.getCode());
                            ebRelation = ebRelationRepository.save(ebRelation);
                            message = "Your request is being processed";
                            inRelation = true;

                            // envoie de mail
                            String link = "app/user/settings?route=mycommunity";
                            emailService.sendEmailInviterMyCommunity(Arrays.asList(user), connectedUser, link);
                        }

                        // si guest est un control tower
                        if (user.getRole().equals(Enumeration.Role.ROLE_CONTROL_TOWER.getCode())) {
                            ebRelation
                                .setxEbEtablissementHost(
                                    new EbEtablissement(connectedUser.getEbEtablissement().getEbEtablissementNum()));
                            ebRelation
                                .setxEbEtablissement(
                                    new EbEtablissement(user.getEbEtablissement().getEbEtablissementNum()));
                            ebRelation.setGuest(new EbUser(user.getEbUserNum()));
                            ebRelation.setEmail(null);
                            ebRelation.setTypeRelation(Enumeration.ServiceType.SERVICE_CONTROL_TOWER.getCode());
                            ebRelation.setEtat(Enumeration.EtatRelation.EN_COURS.getCode());
                            ebRelation = ebRelationRepository.save(ebRelation);
                            message = "Your request is being processed";
                            inRelation = true;
                        }
                        // si guest est un prestataire
                        else {
                            message = "You cannot add another service provider to your community";
                        }

                    }

                }

            }
            // si guest n'existe pas
            else {
                List<EbRelation> listOldRel = ebRelationRepository.findAllByEmail(ebRelation.getEmail());

                if (listOldRel == null || listOldRel.isEmpty()) {
                    ebRelation.setxEbEtablissementHost(connectedUser.getEbEtablissement());
                    ebRelation.setEtat(Enumeration.EtatRelation.EN_COURS.getCode());
                    EbUser guest = null;
                    ebRelation.setGuest(guest);
                    ebRelation = ebRelationRepository.save(ebRelation);
                    inRelation = true;
                    user = new EbUser();
                    user.setStatus(Enumeration.UserStatus.NON_ACTIF.getCode());
                    emailService.sendEmailInviteUtiliserMyTower(connectedUser, ebRelation.getEmail());
                    message = "Your request is being processed";
                }
                else {
                    message = "Your already sent this request, it is being processed";
                    inRelation = false;
                }

            }

            if (user != null && inRelation && ebRelation != null) user.setEbRelation(ebRelation);

            if (ebRelation.getGuest() == null) ebRelation.setGuest(new EbUser());

            if (inRelation && user != null && user.getEbUserNum() != null) {
                EbEtablissement etab = daoEtablissement.selectEbEtablissementByEbUser(user.getEbUserNum());
                ebRelation.getGuest().setEbEtablissement(etab);
            }

            result.put("message", message);
            result.put("inRelation", inRelation);
            result.put("ebRelation", ebRelation);
            result.put("user", user);

        } catch (Exception e) {
            throw e;
        }

        return result;
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = false)
    public Boolean updatePasswordEbUser(EbUser ebUser) {
        boolean updated = false;

        if (ebUser.getPassword() != null) {
            ebUser.setPassword(passwordEncoder.encode(ebUser.getPassword()));
        }

        if (daoUser.updatePasswordEbUser(ebUser)) {
            updated = true;
            emailService.sendEmailUpdatedPassword(ebUser);
        }

        return updated;
    }

    @Transactional
    @Override
    public EbUser deleteUser(EbUser user) {
        // TODO Auto-generated method stub

        List<EbUser> adminsUser = ebUserRepository
            .selectSuperAdmin(
                user.getEbCompagnie().getEbCompagnieNum(),
                user.getEbEtablissement().getEbEtablissementNum());

        EbUser adminUser = adminsUser.get(0);
        Integer nbreUtilisateurRestant = ebUserRepository
            .CountEbUserOfEtablissement(user.getEbEtablissement().getEbEtablissementNum());

        emailService.sendEmailCompteRefuserParAdminSocieter(user, adminUser);

        ebUserRepository.deleteById(user.getEbUserNum());

        if (nbreUtilisateurRestant == 1) {
            ebEtablissementRepository.deleteById(user.getEbEtablissement().getEbEtablissementNum());
        }

        return user;
    }

    @Override
    public List<EbCompagnie> selectListOneListEbCompagnieByOneContact(SearchCriteria criteria) {
        List<EbCompagnie> listEbCompagnie = new ArrayList<EbCompagnie>();
        List<EbUser> listUser = (List<EbUser>) daoUser.selectListContact(criteria);

        for (EbUser user: listUser) {

            if (!listEbCompagnie.contains(user.getEbCompagnie())) {
                listEbCompagnie.add(user.getEbCompagnie());
            }

        }

        /*
         * criteria.setCompagnie(true); List<EbRelation> listEbRelation =
         * daoEtablissement.selectListEbEtablissementOfEbRelation(criteria);
         * List<EbEtablissement> listEbEtablissement = new
         * ArrayList<EbEtablissement>(); for(EbRelation ebrelat: listEbRelation)
         * { if(!listEbEtablissement.contains(ebrelat.getxEbEtablissement()))
         * listEbEtablissement.add(ebrelat.getxEbEtablissement()); }
         * for(EbEtablissement etab : listEbEtablissement) {
         * etab.setEbCompagnie(new EbCompagnie()); EbCompagnie compagnie = new
         * EbCompagnie(); compagnie =
         * ebCompagnieRepository.selectNumCompagnieAndNomByEtablissement(etab.
         * getEbEtablissementNum());
         * etab.getEbCompagnie().setEbCompagnieNum(ebEtablissementRepository.
         * selectNumCompagnieByEtablissement(etab.getEbEtablissementNum()));
         * nomCompagnie =
         * ebCompagnieRepository.recupNomCompagnie(etab.getEbCompagnie().
         * getEbCompagnieNum()); etab.getEbCompagnie().setNom(nomCompagnie);
         * if(!listEbCompagnie.contains(compagnie)) {
         * listEbCompagnie.add(compagnie); } }
         */
        return listEbCompagnie;
    }

    @Override
    public EbUser saveConfigEmail(EbUser user) {

        try {
            Optional<EbUser> newUser = ebUserRepository.findById(user.getEbUserNum());

            newUser.get().setEbUserNum(user.getEbUserNum());
            String listParamsMailStr = "";

            // List<EbParamsMail> listParams = new ArrayList<EbParamsMail>();
            for (EbParamsMail param: user.getListParamsMail()) {
                EbParamsMail params = new EbParamsMail();
                params.setEmailId(param.getEmailId());
                param.setCreatedByMe(param.getCreatedByMe() == null ? false : param.getCreatedByMe());
                param
                    .setCreatedByOtherUser(
                        param.getCreatedByOtherUser() == null ? false : param.getCreatedByOtherUser());
                params.setCreatedByMe(param.getCreatedByMe());
                params.setCreatedByOtherUser(param.getCreatedByOtherUser());

                if (!listParamsMailStr.isEmpty()) listParamsMailStr += "|";
                Integer mailId = params.getEmailId();
                Boolean created = params.getCreatedByMe();
                Boolean other = params.getCreatedByOtherUser();
                listParamsMailStr += mailId + ":" + created + ":" + other;
            }

            newUser.get().setListParamsMail(user.getListParamsMail());

            newUser.get().setListParamsMailStr(listParamsMailStr);

            ebUserRepository.save(newUser.get());
        } catch (Exception e) {
            // TODO: handle exception
        }

        return user;
    }

    @Override
    public List<EbUser> getAllTransporteur() {
        return ebUserRepository.findAllTransporteur();
    }

    @Override
    public List<EbUser> getByProfile(Integer ebUserProfileNum) {
        return ebUserRepository.getbyEbUserProfileNum(ebUserProfileNum);
    }

    @Override
    public boolean canEditUser(Integer ebUserNum) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        SearchCriteria criteria = new SearchCriteria();

        if (connectedUser.isControlTower()) criteria.setRoleControlTower(true);
        else if (connectedUser.isSuperAdmin()) {
            criteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
            connectedUser.setSuperAdmin(true);
        }
        else if (connectedUser.isAdmin()) {
            criteria.setEbEtablissementNum(connectedUser.getEbEtablissement().getEbEtablissementNum());
            connectedUser.setAdmin(true);
        }

        criteria.setUserNum(ebUserNum);
        List<EbUser> users = this.daoUser.selectListEbUser(criteria);

        return !users.isEmpty() ? true : false;
    }

    @Override
    public boolean canEditProfil(Integer ebUserProfileNum) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        SearchCriteria criteria = new SearchCriteria();

        if (connectedUser.isControlTower()) criteria.setRoleControlTower(true);
        else if (connectedUser.isSuperAdmin()) {
            criteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
            connectedUser.setSuperAdmin(true);
        }
        else if (connectedUser.isAdmin()) {
            criteria.setEbEtablissementNum(connectedUser.getEbEtablissement().getEbEtablissementNum());
            connectedUser.setAdmin(true);
        }

        criteria.setEbUserProfileNum(ebUserProfileNum);
        criteria.setUserProfilNum(ebUserProfileNum);
        List<EbUserProfile> userProfils = this.userProfileDao.getUserProfils(criteria);

        return !userProfils.isEmpty() ? true : false;
    }

    @Override
    public String deleteSelectedUsers(List<EbUser> users) {
        ArrayList<Integer> ids = new ArrayList<Integer>();

        for (EbUser user: users) {

            if (user.getEbUserNum() != null) {
                Integer tt = user.getEbUserNum();
                ids.add(tt);
                exEbUserModuleRepository.deleteSelectedEbUserModule(user);
                ebAclRelationRepository.deleteByUser(user.getEbUserNum());
            }

        }

        ebUserRepository.deleteSelectedUsers(ids);

        return "Done";
    }

    @Override
    public List<String> checkAbilityToDeleteUers(List<EbUser> users) {
        Locale lcl = null;
        String lang = connectedUserService.getCurrentUserFromDB().getLanguage();

        if (Strings.isNotEmpty(lang)) {
            lcl = new Locale(lang);
        }
        else {
            lcl = new Locale("en");
        }

        String subject = "deleteUser.message";
        String msg = null;

        ArrayList<String> message = new ArrayList<String>();

        Long result;

        for (EbUser user: users) {
            Object[] arg = new Object[2];
            arg[0] = user.getNom();
            arg[1] = user.getEmail();
            msg = messageSource.getMessage(subject, arg, lcl);
            msg = msg + "\n";

            result = ebDemandeRepository.checkIfUserExistOnEbDemande(user);

            if (result > 0) {
                // itemTables.put("table","EbDemande");
                msg = msg + " Ebdemande \n";
            }

            result = ebTrackTraceRepository.checkIfUserExistOnEbTtTracing(user);

            if (result > 0) {
                msg = msg + " EbTtTracing \n";
            }

            if (msg.contains("Ebdemande") || msg.contains("EbTtTracing")) {
                message.add(msg);
            }

        }

        return message;
    }

    @Override
    public void updateEtablissement(TransfertUserObject transfertUserObjet) {
        EbUser user = getEbUserByEbUserNum(transfertUserObjet.getEbUserNum());

        EbEtablissement etablissement = ebEtablissementRepository.getOne(transfertUserObjet.getNewEtablissement());

        user.setEbEtablissement(etablissement);

        ebUserRepository.save(user);
    }

    // cette methode permet de remplacer l'établissement de la demande et de
    // l'user
    // par la nouvelle établissement au moment de transfert d'un user d'une
    // établissement vers une autre.
    public void updateMaskEtablissement(TransfertUserObject transfertUserObjet)
        throws JsonMappingException,
        JsonProcessingException {
        List<EbSavedForm> ebSavedForm = ebSavedFormRepository.findByEbUser_ebUserNum(transfertUserObjet.getEbUserNum());

        EbUser ebUser = ebUserRepository.getOne(transfertUserObjet.getEbUserNum());
        ebUser.setEbEtablissement(new EbEtablissement(transfertUserObjet.getNewEtablissement()));
        ebUser.getEbCompagnie().setListEtablissements(null);
        ObjectMapper mapper = new ObjectMapper();

        if (ebSavedForm != null) {

            for (EbSavedForm e: ebSavedForm) {

                try {
                    EbDemande d = mapper.readValue(e.getFormValues(), EbDemande.class);
                    d.setxEbEtablissement(ebUser.getEbEtablissement());
                    d.setUser(ebUser);
                    e.setFormValues(mapper.writeValueAsString(d));
                } catch (Exception execption) {
                    logger
                        .debug(
                            " Numero du mask: {} , Numero d'user: {} , La date de création du mask: {} ",
                            e.getEbSavedFormNum(),
                            e.getEbUserNum(),
                            e.getDateAjout());

                    execption.printStackTrace();
                }

            }

            ebSavedFormRepository.saveAll(ebSavedForm);
        }

    }

    @Override
    public List<EbCompagnie> selectListContactCompagnie(SearchCriteria criteria) {
        criteria.setCommunityEstablishment(true);
        List<EbUser> users = this.selectListContact(criteria);
        List<EbCompagnie> compagnies = new ArrayList<EbCompagnie>();

        for (EbUser user: users) {
            EbCompagnie c = compagnies
                .stream().filter(item -> item.getEbCompagnieNum().equals(user.getEbCompagnie().getEbCompagnieNum()))
                .findFirst().orElse(null);

            if (c == null) {
                compagnies.add(user.getEbCompagnie());
            }

        }

        return compagnies;
    }

    @Override
    public List<EbCompagnie> selectListContactCompagnieOfRelation(SearchCriteria criteria) {
        criteria.setCommunityEstablishment(true);
        List<EbRelation> relations = (List<EbRelation>) daoUser.selectListContact(criteria);
        List<EbCompagnie> compagnies = new ArrayList<EbCompagnie>();

        for (EbRelation relation: relations) {

            if (relation.getxEbEtablissementHost() != null
                && relation.getxEbEtablissementHost().getEbCompagnie() != null
                  ) {
                EbCompagnie c = compagnies
                    .stream()
                    .filter(
                        item -> item
                            .getEbCompagnieNum()
                            .equals(relation.getxEbEtablissementHost().getEbCompagnie().getEbCompagnieNum()))
                    .findFirst().orElse(null);

                if (c == null) {
                    compagnies.add(relation.getxEbEtablissementHost().getEbCompagnie());
                }

            }

        }

        return compagnies;
    }

    public String selectListCarrier(SearchCriteria criteria) throws JsonProcessingException {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        criteria.setEbUserNum(connectedUser.getEbUserNum());
        criteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
        criteria.setUserService(Enumeration.ServiceType.SERVICE_TRANSPORTEUR.getCode());
        criteria.setEcRoleNum(Enumeration.Role.ROLE_PRESTATAIRE.getCode());
        criteria.setContact(true);
        List<EbUser> listEbTransporteur = (List<EbUser>) daoUser.selectListContact(criteria);
        return serializationJson(listEbTransporteur);
    }

		public boolean isCompanyInMyContactByListEtablissementIds(Integer idComponyGuest, List<Integer> idsEtablissement)
		{
			return ebEtablissementRepository.isCompanyInMyContactByListEtablissementIds(idComponyGuest, idsEtablissement);
		}
}
