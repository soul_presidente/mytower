package com.adias.mytowereasy.service.filedownload;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface FiledownloadCustomDeclarationService

{
    public FileDownloadStateAndResult listFileDownLoadCustomDeclaration(EbUser connectedUser, SearchCriteria criteria)
        throws Exception;
}
