package com.adias.mytowereasy.service.filedownload;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.reflect.TypeToken;

import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.model.Enumeration.IncidentStatus;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.qm.EbQmIncident;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.service.QualityManagementService;
import com.adias.mytowereasy.service.category.CategoryService;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


@Service
public class FiledownloadQualityManagementServiceImp extends MyTowerService
    implements FiledownloadQualityManagementService {
    @Autowired
    ConnectedUserService connectedUserService;
    @Autowired
    QualityManagementService qualityManagementService;
    @Autowired
    CategoryService categoryService;

    @Autowired
    MessageSource messageSource;

    @Override
    public FileDownloadStateAndResult listFileDownLoadQualityManagement(
        EbUser connectedUser,
        List<Map<String, String>> dtConfig,
        List<EbCategorie> listCategorie,
        SearchCriteria searchCriteria,
        Map<String, String> mapCustomField,
        Map<String, List<Integer>> mapCategoryFields)
        throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();
        Map<String, String> listTypeResult = new LinkedHashMap<String, String>();

        List<EbCustomField> fields = new ArrayList<EbCustomField>();

        Map<String, Object> listcatAndCustom = categoryService
            .getListCategoryAndCustomField(searchCriteria, connectedUser);

        fields = (List<EbCustomField>) listcatAndCustom.get("customField");
        listCategorie = (List<EbCategorie>) listcatAndCustom.get("listCategorie");

        if (!mapCustomField.isEmpty()) searchCriteria.setMapCustomFields(mapCustomField);

        if (!mapCategoryFields.isEmpty()) searchCriteria.setMapCategoryFields(mapCategoryFields);

        ObjectMapper objectMapper = new ObjectMapper();
        SearchCriteriaQM searchCriteriaQM = objectMapper.convertValue(searchCriteria, SearchCriteriaQM.class);

        List<EbQmIncident> listIncident = qualityManagementService.getListEbIncident(searchCriteriaQM);

        Locale locale = connectedUser.getLocaleFromLanguage();

        DateFormat dateFormat = new SimpleDateFormat(connectedUser.getDateFormatFromLocale());

        if (searchCriteria.getEbCompagnieNum() == null) {
            searchCriteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
        }

        if (searchCriteria.getEbEtablissementNum() == null) {
            searchCriteria.setEbEtablissementNum(connectedUser.getEbEtablissement().getEbEtablissementNum());
        }

        for (EbQmIncident incident: listIncident) {
            Map<String, String[]> map = new LinkedHashMap<String, String[]>();

            map.put("qualificationLabel", new String[]{
                messageSource.getMessage("quality_management.incident_qualif", null, locale),
                incident.getQualificationLabel()
            });
            map.put("incidentRef", new String[]{
                messageSource.getMessage("quality_management.incident_ref_label", null, locale),
                incident.getIncidentRef()
            });
            map.put("incidentDesc", new String[]{
                messageSource.getMessage("quality_management.incident_description", null, locale),
                incident.getIncidentDesc()
            });
            map.put("originCountryLibelle", new String[]{
                messageSource.getMessage("quality_management.origin_country", null, locale),
                incident.getOriginCountryLibelle()
            });
            map.put("destinationCountryLibelle", new String[]{
                messageSource.getMessage("quality_management.destination_country", null, locale),
                incident.getDestinationCountryLibelle()
            });
            map.put("modeTransport", new String[]{
                messageSource.getMessage("track_trace.mode_of_transport", null, locale),
                incident.getModeTransport() != null ? ModeTransport.getLibelleByCode(incident.getModeTransport()) : null
            });
            map.put("currencyLabel", new String[]{
                messageSource.getMessage("quality_management.currency", null, locale), incident.getCurrencyLabel()
            });
            map.put("claimAmount", new String[]{
                messageSource.getMessage("quality_management.claim_ammount", null, locale),
                incident.getClaimAmount() != null ? incident.getClaimAmount().toString() : null
            });
            map.put("recovered", new String[]{
                messageSource.getMessage("quality_management.recovered", null, locale),
                incident.getRecovered() != null ? incident.getRecovered().toString() : null
            });
            map.put("balance", new String[]{
                messageSource.getMessage("quality_management.balance", null, locale),
                incident.getBalance() != null ? incident.getBalance().toString() : null
            });
            String dateCreation = null;

            if (incident.getDateCreation() != null) {
                dateCreation = getTimeSinceCreation(locale, incident);
            }
            map.put("dateCreation", new String[]{
                messageSource.getMessage("quality_management.timesinceinsuance", null, locale),
                dateCreation
            });
            map.put("demandeTranportRef", new String[]{
                messageSource.getMessage("quality_management.transport_ref", null, locale),
                incident.getDemandeTranportRef()
            });
            map.put("carrierNom", new String[]{
                messageSource.getMessage("quality_management.carrier", null, locale), incident.getCarrierNom()
            });
            map.put("dateIncident", new String[]{
                messageSource.getMessage("quality_management.incident_date", null, locale),
                incident.getDateIncident() != null ? dateFormat.format(incident.getDateIncident()) : null
            });
            listTypeResult.put("dateIncident", Date.class.getTypeName());
            map.put("category", new String[]{
                messageSource.getMessage("quality_management.incident_cat", null, locale),
                incident.getCategory() != null ? incident.getCategory().getLibelle() : null
            });
            map.put("DateCloture", new String[]{
                "Date cloture", incident.getDateCloture() != null ? dateFormat.format(incident.getDateCloture()) : null
            });
            listTypeResult.put("DateCloture", Date.class.getTypeName());

            map.put("incidentStatus", new String[]{
                messageSource.getMessage("quality_management.status", null, locale),
                incident.getIncidentStatus() != null ?
                    IncidentStatus.getLibelleByCode(incident.getIncidentStatus()) :
                    null
            });
            map.put("typeRequestLibelle", new String[]{
                messageSource.getMessage("quality_management.service_level", null, locale),
                incident.getTypeRequestLibelle() != null ? incident.getTypeRequestLibelle().toString() : null
            });
            map.put("psl", new String[]{
                messageSource.getMessage("quality_management.timestamp_related", null, locale),
                incident.getPsl() != null ? incident.getPsl().toString() : null
            });
            map.put("criticality", new String[]{
                messageSource.getMessage("control_rules.criticality", null, locale),
                incident.getCriticality() != null ? incident.getCriticality().toString() : null
            });
            map.put("ownerNomPrenom", new String[]{
                messageSource.getMessage("pricing_booking.requestor", null, locale), incident.getOwnerNomPrenom()
            });
            map.put("dateMaj", new String[]{
                messageSource.getMessage("quality_management.lastupdate", null, locale),
                incident.getDateMaj() != null ? dateFormat.format(incident.getDateMaj()) : null
            });
            listTypeResult.put("dateMaj", Date.class.getTypeName());

            map.put("lastContributerNomPremon", new String[]{
                messageSource.getMessage("quality_management.last_contributor", null, locale),
                incident.getLastContributerNomPremon()
            });

            StringBuffer rootCauseBuffer = getRootCause(incident);

            map.put("rootCauses", new String[]{
                messageSource.getMessage("quality_management.root_cause", null, locale), rootCauseBuffer.toString()
            });

            if (listCategorie != null && listCategorie.size() > 0) {

                for (EbCategorie cat: listCategorie) {
                    if (isPropertyInDtConfig(dtConfig, "category" + cat.getEbCategorieNum())) map
                        .put("category" + cat.getEbCategorieNum(), new String[]{
                            cat.getLibelle(), null
                        });
                }

            }

            if (fields != null && fields.size() > 0) {

                for (EbCustomField customFields: fields) {
                    List<CustomFields> field = new ArrayList<CustomFields>();

                    field = gson.fromJson(customFields.getFields(), new TypeToken<ArrayList<CustomFields>>() {
                    }.getType());

                    if (field != null && field.size() > 0) {

                        for (CustomFields fie: field) {
                            if (isPropertyInDtConfig(dtConfig, fie.getName())) map.put(fie.getName(), new String[]{
                                fie.getLabel(), null
                            });
                        }

                    }

                    if (field != null &&
                        incident.getListCustomsFields() != null &&
                        (incident.getxEbCompagnie().getEbCompagnieNum().equals(customFields.getxEbCompagnie()))) {

                        for (CustomFields fie: field) {

                            if (isPropertyInDtConfig(dtConfig, fie.getName())) {

                                for (CustomFields demField: incident.getListCustomsFields()) {

                                    if (demField.getName().contentEquals(fie.getName())) {
                                        map.put(fie.getName(), new String[]{
                                            fie.getLabel(), demField.getValue()
                                        });
                                        break;
                                    }

                                }

                            }

                        }

                    }

                }

            }

            if (listCategorie != null && incident.getListCategories() != null) {

                for (EbCategorie categorie: listCategorie) {

                    if (isPropertyInDtConfig(dtConfig, "category" + categorie.getEbCategorieNum())) {

                        for (EbCategorie demCategorie: incident.getListCategories()) {

                            if (demCategorie.getLabels() != null &&
                                demCategorie.getEbCategorieNum().equals(categorie.getEbCategorieNum())) {
                                String label = null;

                                for (EbLabel ebLabel: demCategorie.getLabels()) {
                                    label = ebLabel.getLibelle();
                                    break;
                                }

                                map.put("category" + categorie.getEbCategorieNum(), new String[]{
                                    categorie.getLibelle(), label
                                });
                                break;
                            }

                        }

                    }

                }

            }

            if (isPropertyInDtConfig(dtConfig, "listEbFlagDTO")) {
                String listFlagsLibelle = "";

                if (incident.getListEbFlagDTO() != null && incident.getListEbFlagDTO().size() > 0) {

                    for (EbFlagDTO ebFlagDTO: incident.getListEbFlagDTO()) {
                        listFlagsLibelle += ebFlagDTO.getName() + ", ";
                    }

                }

                if (listFlagsLibelle.length()
                    > 0) listFlagsLibelle = listFlagsLibelle.substring(0, listFlagsLibelle.length() - 2);
                map.put("listEbFlagDTO", new String[]{
                    messageSource.getMessage("pricing_booking.flag", null, locale), listFlagsLibelle
                });
            }

            // adding data
            listresult.add(map);
        }

        resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
        resultAndState.setListResult(listresult);
        resultAndState.setListTypeResult(listTypeResult);
        return resultAndState;
    }

    private StringBuffer getRootCause(EbQmIncident incident) {
        StringBuffer rootCauseBuffer = new StringBuffer();

        if (incident.getRootCauses() != null && !incident.getRootCauses().isEmpty()) {
            Set<String> rootCausesLabel = new HashSet<String>();
            incident.getRootCauses().forEach((rootCause) -> {
                rootCausesLabel.add(rootCause.getLabel());
            });

            if (rootCausesLabel != null && !rootCausesLabel.isEmpty()) {

                for (String c: rootCausesLabel) {
                    rootCauseBuffer.append(c);
                    rootCauseBuffer.append(System.lineSeparator());
                }

            }

        }
        return rootCauseBuffer;
    }

    private String getTimeSinceCreation(Locale locale, EbQmIncident incident) {
        String dateCreation;
        Date today = new Date();
        long timeSinceInsuance = today.getTime() - incident.getDateCreation().getTime();

        long days = TimeUnit.MILLISECONDS.toDays(timeSinceInsuance);
        timeSinceInsuance -= TimeUnit.DAYS.toMillis(days);
        long hours = TimeUnit.MILLISECONDS.toHours(timeSinceInsuance);
        timeSinceInsuance -= TimeUnit.HOURS.toMillis(hours);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(timeSinceInsuance);

        dateCreation = days +
            " " + messageSource.getMessage("quality_management.day_shortcut", null, locale) + " " + hours +
            " " + messageSource.getMessage("quality_management.hour_shortcut", null, locale) + " " + minutes +
            " " + messageSource.getMessage("quality_management.month_shortcut", null, locale);
        return dateCreation;
    }
}
