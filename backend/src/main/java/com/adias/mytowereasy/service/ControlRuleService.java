package com.adias.mytowereasy.service;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.transaction.Transactional;

import org.apache.commons.lang.time.DateUtils;
import org.bouncycastle.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.PropertyAccessor;
import org.springframework.beans.PropertyAccessorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.adias.mytowereasy.dao.DaoTypeUnit;
import com.adias.mytowereasy.dto.EbTemplateDocumentsDTO;
import com.adias.mytowereasy.dto.EbTypeMarchandiseDto;
import com.adias.mytowereasy.dto.TemplateGenParamsDTO;
import com.adias.mytowereasy.dto.mapper.TransportMapper;
import com.adias.mytowereasy.enumeration.ControlRuleOperationTypesEnum;
import com.adias.mytowereasy.enumeration.ExportControlEnum;
import com.adias.mytowereasy.enumeration.FileExtentionEnum;
import com.adias.mytowereasy.enumeration.TypeNotificationEnum;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.kafka.service.KafkaObjectService;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.CREnumeration.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.Enumeration.ServiceType;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.Enumeration.UploadDirectory;
import com.adias.mytowereasy.model.TtEnumeration.NatureDateEvent;
import com.adias.mytowereasy.model.tt.*;
import com.adias.mytowereasy.repository.EbCustomFieldRepository;
import com.adias.mytowereasy.repository.EbPslAppRepository;
import com.adias.mytowereasy.repository.EbTemplateDocumentsRepository;
import com.adias.mytowereasy.util.document.DocumentUtils;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaTypeUnit;


@Service
public class ControlRuleService extends MyTowerService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    KafkaObjectService kafkaObjectService;

    @Autowired
    EmailControlRuleService emailControlRuleService;

    @Autowired
    CategorieService categorieService;

    @Autowired
    protected EbIncotermService ebIncotermService;

    @Autowired
    protected TypeRequestService typeRequestService;

    @Autowired
    EbPslAppRepository ebPslAppRepository;

    @Autowired
    protected EbCustomFieldRepository ebCustomFieldRepository;

    @Autowired
    protected DaoTypeUnit daoTypeUnit;

    @Autowired
    EbTemplateDocumentsRepository ebTemplateDocumentsRepository;

    @Autowired
    CostCenterService costCenterService;

    @Autowired
    private TransportMapper mapper;

    @Autowired
    private NotificationService notificationService;

    // @Value("${tracing.process_control_rules.active}")
    // private Boolean cronActive;

    public List<EbControlRule> getListControlRule(Integer ebCompagnieNum, Integer ruleCategory) {
        List<EbControlRule> listCr = null;

        if (ebCompagnieNum != null) listCr = ebControlRuleRepository
            .getByXEbCompagnie_ebCompagnieNumAndRuleCategoryAndActivated(ebCompagnieNum, ruleCategory, true);
        else listCr = ebControlRuleRepository.findAll();

        if (listCr != null) {

            for (EbControlRule cr: listCr) {
                List<EbControlRuleOperation> oprs = cr
                    .getListRuleOperations().stream()
                    .sorted((it1, it2) -> it1.getEbControlRuleOperationNum() - it2.getEbControlRuleOperationNum())
                    .collect(Collectors.toList());
                cr.setListRuleOperations(oprs);
            }

        }

        return listCr;
    }

    public EbControlRule getControlRule(Integer ebControlRuleNum) {
        return ebControlRuleRepository.getOneByEbControlRuleNum(ebControlRuleNum);
    }

    public List<EbControlRule> getListControlRuleDashboard(SearchCriteria searchCriteria) throws Exception {
        return daoControlRule.getListEbControlRule(searchCriteria);
    }

    public long listCount(SearchCriteria searchCriteria) {
        return daoControlRule.listCount(searchCriteria);
    }

    @Transactional
    public EbControlRule addControlRule(EbControlRule ebControlRule) {
        if (ebControlRule == null) throw new MyTowerException("Rule is null");

        if (ebControlRule.getListRuleOperations() == null || ebControlRule
            .getListRuleOperations().isEmpty()) throw new MyTowerException("Rule operations not provided");

        saveRuleAndOperations(ebControlRule);

        return ebControlRule;
    }

    @Transactional
    public EbControlRule updateControlRule(EbControlRule ebControlRule) {
        if (ebControlRule == null
            || ebControlRule.getEbControlRuleNum() == null) throw new MyTowerException("Rule identifier not found");

        if (ebControlRule.getListRuleOperations() == null || ebControlRule
            .getListRuleOperations().isEmpty()) throw new MyTowerException("Rule operations not provided");

        ebControlRuleOperationRepository.deleteByXEbControlRule_ebControlRuleNum(ebControlRule.getEbControlRuleNum());

        saveRuleAndOperations(ebControlRule);

        return ebControlRule;
    }

    private void saveRuleAndOperations(EbControlRule ebControlRule) {
        List<EbControlRuleOperation> ops = ebControlRule.getListRuleOperations();
        ebControlRule.setListRuleOperations(null);
        ebControlRule.setActivated(true);

        ebControlRule = ebControlRuleRepository.save(ebControlRule);

        for (EbControlRuleOperation op: ops) op.setxEbControlRule(ebControlRule);

        ebControlRuleOperationRepository.deleteByXEbControlRule_ebControlRuleNum(ebControlRule.getEbControlRuleNum());
        ebControlRuleOperationRepository.saveAll(ops);

        ebControlRule.setListRuleOperations(ops);
    }

    @Async
    public void executeControlRulesWithAsyn(EbControlRule ebControlRule, EbUser connectedUser) {

        try {
            processControlRulesCommon(
                ebControlRule.getxEbCompagnie().getEbCompagnieNum(),
                null,
                null,
                null,
                ebControlRule.getRuleCategory(),
                ebControlRule,
                null,
                false,
                null,
                connectedUser);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Transactional
    public boolean deactivateControlRule(Integer ebControlRuleNum, Boolean activated) {
        ebControlRuleRepository.updateDeactivated(ebControlRuleNum, activated);

        return true;
    }

    private List<Map<String, Object>> processControlRulesCommon(
        Integer ebCompagnieNum,
        Integer ebTtTracingNum,
        Integer ebTtPslAppNum,
        Integer natureEventDate,
        Integer ruleCategory,
        EbControlRule ebControlRule,
        Integer ebDemandeNum,
        Boolean isCreation,
        Boolean isStatusUpdate,
        EbUser connectedUser)
        throws Exception {
        List<Map<String, Object>> res = new ArrayList<Map<String, Object>>();
        List<EbControlRule> listControlRule = null;

        if (ebControlRule != null && ebControlRule.getEbControlRuleNum() != null) {
            listControlRule = new ArrayList<>();
            listControlRule.add(this.getControlRule(ebControlRule.getEbControlRuleNum()));
        }
        else {
            listControlRule = getListControlRule(ebCompagnieNum, ruleCategory);
        }

        if (listControlRule == null || listControlRule.isEmpty()) return null;

        Boolean lunch = null;

        for (EbControlRule rule: listControlRule) {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("rule", rule);

            rule.setxEbCompagnie(new EbCompagnie(ebCompagnieNum));

            if (RuleCategory.DATE.getCode().equals(rule.getRuleCategory())) {
                List<EbDemande> listEbDemande = daoControlRule
                    .getListDemandeWithRule(rule, ebDemandeNum, connectedUser);

                if (!listEbDemande.isEmpty()) {
                    List<EbTTPslApp> listPsl = daoControlRule.getListPslWithRule(rule, ebTtTracingNum);
                    List<EbTtTracing> listTracing = applyRuleOperations(listPsl, rule);

                    if (listTracing != null && !listTracing.isEmpty()) {
                        postProcessControlRules(
                            rule,
                            listTracing,
                            ebTtPslAppNum,
                            natureEventDate,
                            listEbDemande.get(0).getEbDemandeNum(),
                            connectedUser);

                        map.put("listTracing", listTracing);
                        res.add(map);
                    }

                }

            }
            else if (RuleCategory.DOCUMENT.getCode().equals(rule.getRuleCategory())) {
                lunch = true;

                if (rule.getTriggers() != null) {
                    lunch = this.lunchRuleExection(rule, ebDemandeNum, isCreation);
                }
                else lunch = false;

                if (lunch) {

                    if (isStatusUpdate != null && isStatusUpdate) {
                        rule.setxEbCompagnie(new EbCompagnie(ebCompagnieNum));
                        List<EbDemande> listDemande = daoControlRule
                            .getListDemandeWithRule(rule, ebDemandeNum, connectedUser);

                        if (listDemande != null && !listDemande.isEmpty()) {

                            if (this.isRuleWithStatutCondition(rule)) {
                                listDemande = postProcessDocumentRules(
                                    rule,
                                    listDemande,
                                    connectedUser,
                                    ebTtTracingNum);
                                map.put("listDemande", listDemande);
                                res.add(map);
                            }

                        }

                    }
                    else {
                        rule.setxEbCompagnie(new EbCompagnie(ebCompagnieNum));
                        List<EbDemande> listDemande = daoControlRule
                            .getListDemandeWithRule(rule, ebDemandeNum, connectedUser);

                        if (listDemande != null && !listDemande.isEmpty()) {
                            listDemande = postProcessDocumentRules(rule, listDemande, connectedUser, ebTtTracingNum);
                            map.put("listDemande", listDemande);
                            res.add(map);
                        }

                    }

                }

            }

        }

        return res;
    }

    private boolean lunchRuleExection(EbControlRule rule, Integer ebDemandeNum, Boolean isCreation) {
        boolean lunch = true;

        if (ebDemandeNum != null && isCreation != null && isCreation) {
            if (!rule
                .getTriggers().contains("" + CREnumeration.RuleTrigger.AT_TR_FILE_CREATION.getCode())) lunch = false;
        }
        else if (ebDemandeNum != null && (isCreation == null || !isCreation)) {
            if (!rule.getTriggers().contains("" + CREnumeration.RuleTrigger.AT_TR_FILE_UPDATE.getCode())) lunch = false;
        }
        else {
            if (!rule.getTriggers().contains("" + CREnumeration.RuleTrigger.NOW.getCode())) lunch = false;
        }

        return lunch;
    }

    private List<EbControlRule> getRulesWithStatusInConditions(List<EbControlRule> rules) {
        return rules.stream().filter(rule -> this.isRuleWithStatutCondition(rule)).collect(Collectors.toList());
    }

    public List<EbControlRule> getRulesWithEmailId(Integer ebCompagnieNum, Integer ruleCategory, Integer emailId) {
        return this.ebControlRuleRepository
            .getByXEbCompagnie_ebCompagnieNumAndRuleCategoryAndActivatedAndSendEmailAlertAndSelectedMailNumsContaining(
                ebCompagnieNum,
                ruleCategory,
                true,
                true,
                ":" + emailId + ":");
    }

    public EbControlRule executeRulesWithSendAlertActionOnDemande(
        Integer emailId,
        Integer ruleCategory,
        Integer ebDemandeNum,
        List<EbControlRule> rules) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        for (EbControlRule ebControlRule: rules) {

            if (ebControlRule.getSelectedMailNums() != null && !ebControlRule.getSelectedMailNums().isEmpty()
                && ebControlRule.getSelectedMailNums().contains(":" + emailId + ":")
                && ebControlRule.getEmailDestinataires() != null && !ebControlRule.getEmailDestinataires().isEmpty()) {
                List<EbDemande> listDemande = daoControlRule
                    .getListDemandeWithRule(ebControlRule, ebDemandeNum, connectedUser);

                if (listDemande != null && !listDemande.isEmpty()) return ebControlRule;
            }

        }

        return null;
    }

    private boolean isRuleWithStatutCondition(EbControlRule rule) {
        Optional<EbControlRuleOperation> op = rule
            .getListRuleOperations().stream()
            .filter(
                it -> (it.getIsOperation() == null || !it.getIsOperation()) && it.getOperationType() != null
                    && (it.getOperationType() == ControlRuleOperationTypesEnum.IS_FILE_CONDITION.getCode())
                    && (it.getLeftOperands() != null
                        && Integer.valueOf(it.getLeftOperands()).equals(RuleOperandType.STATUT_DOSSIER.getCode())))
            .findAny();
        return op.isPresent();
    }

    @Transactional
    public List<Map<String, Object>> processControlRules(
        Integer ebCompagnieNum,
        Integer ebTtTracingNum,
        Integer ebTtPslAppNum,
        Integer natureEventDate,
        Integer ruleCategory,
        EbControlRule rule,
        Integer ebDemandeNum,
        Boolean isCreation,
        Boolean isStatusUpdate,
        EbUser connectedUser)
        throws Exception {
        return processControlRulesCommon(
            ebCompagnieNum,
            ebTtTracingNum,
            ebTtPslAppNum,
            natureEventDate,
            ruleCategory,
            rule,
            ebDemandeNum,
            isCreation,
            isStatusUpdate,
            connectedUser);
    }

    // @Transactional
    // @Scheduled(cron = "${tracing.process_control_rules.cron:-}")
    // public void processControlRules() throws Exception
    // {
    // if (cronActive)
    // {
    // processControlRulesCommon(null, null, null, null, null, null, null, null,
    // null, null);
    // }
    // }

    public void postProcessControlRules(
        EbControlRule rule,
        List<EbTtTracing> listTracing,
        Integer ebTtPslAppNum,
        Integer natureEventDate,
        Integer ebDemandeNum,
        EbUser connectedUser)
        throws Exception {
        Date dt = new Date();

        if (rule.getGenererIncident() != null && rule.getGenererIncident()
            || rule.getGenererNotif() != null && rule.getGenererNotif()
            || rule.getEnvoyerEmail() != null && rule.getEnvoyerEmail()) {
            EbTtEvent evt = null;

            EbTtCategorieDeviation categoryDeviation = null;

            EbTTPslApp pslAppCurrent = null;

            if (ebTtPslAppNum != null) {
                pslAppCurrent = ebPslAppRepository.findById(ebTtPslAppNum).orElse(null);
            }

            for (EbTtTracing tt: listTracing) {
                EbTTPslApp ttPsl = null;

                if (tt.getCodeAlphaPslCourant() != null) {

                    for (EbTTPslApp psl: tt.getListEbPslApp()) {

                        if (psl.getCodeAlpha().equalsIgnoreCase(tt.getCodeAlphaPslCourant())) {
                            ttPsl = psl;
                            break;
                        }

                    }

                }

                if (ttPsl == null) ttPsl = tt.getListEbPslApp().get(0);

                if (pslAppCurrent != null) {
                    ttPsl = pslAppCurrent;
                    ttPsl.setxEbTrackTrace(tt.getListEbPslApp().get(0).getxEbTrackTrace());
                }

                if (rule.getGenererIncident() != null && rule.getGenererIncident()) {
                    evt = new EbTtEvent();

                    evt.setTypeEvent(TtEnumeration.TypeEvent.EVENT_DEVIATION.getCode());
                    evt.setxEbCompagnie(rule.getxEbCompagnie());
                    evt.setxEbTtTracing(new EbTtTracing(tt.getEbTtTracingNum()));
                    evt.setDateEvent(dt);
                    evt.setxEbTtPslApp(new EbTTPslApp(ttPsl.getEbTtPslAppNum()));
                    evt.setNatureDate(natureEventDate);

                    if (rule.getRuleType() != null) {
                        categoryDeviation = ebTtCategorieDeviationRepository.findById(rule.getRuleType()).orElse(null);

                        evt.setCategorie(categoryDeviation);
                    }

                    ebTrackService.addDeviation(evt, connectedUser, false);
                }

                if (rule.getGenererNotif() != null && rule.getGenererNotif()) {

                    if (evt == null) {
                        evt = new EbTtEvent();
                        evt.setTypeEvent(TtEnumeration.TypeEvent.EVENT_PSL.getCode());
                        evt.setxEbCompagnie(rule.getxEbCompagnie());
                        evt.setxEbTtTracing(new EbTtTracing(tt.getEbTtTracingNum()));
                        evt.setDateEvent(dt);
                        evt.setxEbTtPslApp(new EbTTPslApp(ttPsl.getEbTtPslAppNum()));
                        evt.setNatureDate(natureEventDate);
                        ebTrackService.addDeviation(evt, connectedUser, false);
                    }

                    List<EbUser> listUser = getListUserVisibilite(
                        evt,
                        tt.getEbTtTracingNum(),
                        connectedUser,
                        ebDemandeNum);

                    for (EbUser user: listUser) {
                        EbNotification notification = new EbNotification();
                        notification.setType(TypeNotificationEnum.NOTIFICATION.getCode());
                        notification.setEbUser(user);
                        notification.setEbCompagnie(user.getEbCompagnie());
                        notification.setHeader(rule.getEmailObjet());
                        notification.setNumEntity(tt.getEbTtTracingNum());
                        notification.setModule(Enumeration.Module.TRACK.getCode());
                        String msg = "";

                        if (rule.getEmailMessage() != null) {
                            String refControl = ttPsl.getxEbTrackTrace().getRefTransport();
                            String statusPsl = ttPsl.getxEbTrackTrace().getCodeAlphaPslCourant();
                            String statusControl = StatutDemande
                                .getLibelle(ttPsl.getxEbTrackTrace().getxEbDemande().getxEcStatut());
                            String notifBody = rule.getEmailMessage();
                            notifBody = notifBody.replace("\n", "<br>");
                            notifBody = notifBody.replace("${transport.reference}", refControl);
                            notifBody = notifBody.replace("${timestamp}", statusPsl);
                            notifBody = notifBody.replace("${transport.status}", statusControl);
                            msg += "<span>" + notifBody + "</span>";
                        }

                        notification.setBody(msg);

                        notificationService.addNotification(notification);
                        notificationService
                            .updateNotificationNumberForUser(
                                user.getEbUserNum(),
                                TypeNotificationEnum.NOTIFICATION.getCode());
                    }

                }

                if (rule.getEnvoyerEmail() != null && rule.getEnvoyerEmail()) {
                    if (rule.getEmailDestinataires() == null) throw new MyTowerException(
                        "Send email activated for rule" + rule.getCode() + ", but no destination provided");

                    emailControlRuleService.sendEmailControlRule(rule, ttPsl, null);
                }

                String listCr = tt.getListControlRule();
                if (listCr == null) listCr = "";
                if (!listCr
                    .contains(":" + rule.getEbControlRuleNum() + ":")) listCr += ":" + rule.getEbControlRuleNum() + ":";
                ebTrackTraceRepository.updateListControlRule(tt.getEbTtTracingNum(), listCr);
            }

        }

    }

    private List<EbUser>
        getListUserVisibilite(EbTtEvent evt, Integer tracingNum, EbUser connectedUser, Integer ebDemandeNum) {
        Map<String, Object> mapParams = new HashMap<String, Object>();
        mapParams.put("ebTtEventNum", evt.getEbTtEventNum());
        mapParams.put("ebTtEventUserNum", evt.getxEbUser().getEbUserNum());
        mapParams.put("ebTtTracingNum", tracingNum);

        List<EbUser> listEbUser = serviceListStatique
            .listUserByEmailId(
                Enumeration.EmailConfig.MISE_A_JR_UN_PSL.getCode(),
                Enumeration.Module.TRACK.getCode(),
                ebDemandeNum,
                true,
                mapParams);
        listEbUser.add(connectedUser);

        return listEbUser;
    }

    /*
     * ALGO d'execution des dates rules, n'est pas demandé dans la release 1.6
     * => Possible cases:
     * - CASE 1: [actual, estimated] [PIC,DEP,..] [>, =, different] [actual,
     * estimated] [PIC,DEP,..] - CASE 2: now() [>, =, different] [actual,
     * estimated]
     * [PIC,DEP,..] <=> CASE 3 with reverse operator - CASE 3: [actual,
     * estimated]
     * [PIC,DEP,..] [>, =, different] now() - CASE 4: [actual, estimated]
     * [PIC,DEP,..] [absent]
     * => Rules:
     * - leftOperands should be always present - rightOperands can be absent
     * only if
     * the operator is "absent" - second operand in the left or right operands
     * can
     * be absent if the first operand is "now()"
     */
    private List<EbTtTracing> applyRuleOperations(List<EbTTPslApp> listPsl, EbControlRule rule) throws Exception {
        List<EbTtTracing> result = new ArrayList<>();
        List<EbTtTracing> listTracing = new ArrayList<>();
        ScriptEngine engine = new ScriptEngineManager().getEngineByExtension("js");
        List<EbControlRuleOperation> listOperations = rule.getListRuleOperations();
        Date nw = new Date();

        // grouping psl by tracing
        EbTtTracing tracing = null;

        for (EbTTPslApp psl: listPsl) {

            if (tracing == null || !tracing.getEbTtTracingNum().equals(psl.getxEbTrackTrace().getEbTtTracingNum())) {
                if (tracing != null) listTracing.add(tracing);
                tracing = new EbTtTracing(psl.getxEbTrackTrace().getEbTtTracingNum());
                tracing.setListControlRule(psl.getxEbTrackTrace().getListControlRule());
                tracing.setListEbPslApp(new ArrayList<>());
            }

            tracing.getListEbPslApp().add(psl);
        }

        if (tracing != null && !tracing.getListEbPslApp().isEmpty()) listTracing.add(tracing);

        for (EbTtTracing tt: listTracing) {
            String globalBoolExpr = "";
            boolean isFirstExpr = true;
            String codeAlpha = null;

            if (tt.getListEbPslApp() == null) continue;

            for (EbControlRuleOperation expr: listOperations) {

                if (ControlRuleOperationTypesEnum.IS_DATE_CONDITION.getCode().equals(expr.getOperationType())) {
                    String[] leftOps = null, rightOps = null;
                    Integer leftPslType = null, rightPslType = null;
                    String leftPslCode = null, rightPslCode = null;
                    Integer opr = expr.getOperator();
                    int condCase = 0;
                    boolean isReverseOpr = false;
                    String boolExpr = null;

                    // managing and controling left operands
                    if (expr.getLeftOperands() == null) throw new MyTowerException("Left operands not provided");
                    leftOps = expr.getLeftOperands().split(";");

                    if (leftOps.length == 0) throw new MyTowerException("Left operands not provided");
                    leftPslType = Integer.valueOf(leftOps[0]);

                    if (!NatureDateEvent.NOW.getCode().equals(leftPslType)
                        && leftOps.length < 2) throw new MyTowerException(
                            "Left Psl date type available but Psl code not provided");

                    if (leftOps.length > 1) {
                        leftPslCode = leftOps[1];
                        condCase = 1;
                    }
                    else condCase = 2;

                    // managing and controling right operands
                    if (opr == null) throw new MyTowerException("Operator not provided");

                    if (!Operator.ABSENT.getCode().equals(opr)) {
                        if (expr.getRightOperands() == null) throw new MyTowerException(
                            "Operator is not 'ABSENT' but no right operands are provided");
                        rightOps = expr.getRightOperands().split(";");

                        if (rightOps.length == 0) throw new MyTowerException(
                            "Operator is not 'ABSENT' but no right operands are provided");
                        rightPslType = Integer.valueOf(rightOps[0]);

                        if (!NatureDateEvent.NOW.getCode().equals(rightPslType)
                            && rightOps.length < 2) throw new MyTowerException(
                                "Right Psl date type available but Psl code not provided");

                        if (rightOps.length > 1) rightPslCode = rightOps[1];
                        else {
                            if (condCase == 2) throw new MyTowerException(
                                "Semantic error: you cannot use 'NOW' operand in both left and right side of the expression");
                            condCase = 3;
                        }

                    }
                    else {
                        if (condCase == 2) throw new MyTowerException(
                            "Semantic error: you cannot use 'ABSENT' operator with NOW()");
                        condCase = 4;
                    }

                    // reversing left & right operands if we are in the second
                    // case
                    if (condCase == 2) {
                        Integer tmpLeftPslType = leftPslType;
                        String tmpLeftPslCode = leftPslCode;
                        leftPslType = rightPslType;
                        leftPslCode = rightPslCode;
                        rightPslType = tmpLeftPslType;
                        rightPslCode = tmpLeftPslCode;
                        isReverseOpr = true;
                    }

                    if (codeAlpha == null) codeAlpha = leftPslCode;
                    if (!codeAlpha.equalsIgnoreCase(leftPslCode)) codeAlpha = "";

                    Date leftDt = null, rightDt = null;

                    for (EbTTPslApp psl: tt.getListEbPslApp()) {

                        if (psl.getCodeAlpha().equalsIgnoreCase(leftPslCode)) {
                            if (NatureDateEvent.DATE_ACTUAL
                                .getCode().equals(leftPslType)) leftDt = psl.getDateActuelle();
                            else if (NatureDateEvent.DATE_ESTIMATED
                                .getCode().equals(leftPslType)) leftDt = psl.getDateEstimee();
                            else if (NatureDateEvent.DATE_CHAMP
                                .getCode().equals(leftPslType)) leftDt = psl.getDateChamps();
                        }
                        else if (rightPslCode != null && psl.getCodeAlpha().equalsIgnoreCase(rightPslCode)) {

                            // initialize right operands for querydsl
                            if (NatureDateEvent.DATE_ACTUAL.getCode().equals(rightPslType)) {
                                if (rule.getMargin() != null && psl.getDateActuelle() != null) rightDt = DateUtils
                                    .addHours(psl.getDateActuelle(), (int) rule.getMargin().toHours());
                                else rightDt = psl.getDateActuelle();
                            }
                            else if (NatureDateEvent.DATE_ESTIMATED.getCode().equals(rightPslType)) {
                                if (rule.getMargin() != null && psl.getDateActuelle() != null) rightDt = DateUtils
                                    .addHours(psl.getDateEstimee(), (int) rule.getMargin().toHours());
                                else rightDt = psl.getDateEstimee();
                            }
                            else if (NatureDateEvent.DATE_CHAMP.getCode().equals(rightPslType)) {
                                if (rule.getMargin() != null && psl.getDateChamps() != null) rightDt = DateUtils
                                    .addHours(psl.getDateChamps(), (int) rule.getMargin().toHours());
                                else rightDt = psl.getDateChamps();
                            }
                            else if (NatureDateEvent.NOW.getCode().equals(rightPslType)) {
                                if (rule.getMargin() != null) rightDt = DateUtils
                                    .addHours(nw, (int) rule.getMargin().toHours());
                                else rightDt = nw;
                            }

                        }

                        if (leftDt != null && (leftPslCode == null || rightDt != null)) break;
                    }

                    // build boolean operation with the operator
                    if (Operator.GREATER_THAN.getCode().equals(opr) && !isReverseOpr) boolExpr = ""
                        + (leftDt != null && (rightDt == null || leftDt.after(rightDt)));
                    else if (Operator.GREATER_THAN.getCode().equals(opr)
                        && isReverseOpr) boolExpr = "" + (leftDt != null && (DateUtils
                            .addHours(leftDt, -1 * (rule.getMargin() != null ? (int) rule.getMargin().toHours() : 0))
                            .before(rightDt)));
                    else if (Operator.EQUALS.getCode().equals(opr)) boolExpr = ""
                        + (leftDt == null && rightDt == null || leftDt != null && leftDt.equals(rightDt));
                    else if (Operator.DIFFERENT.getCode().equals(opr)) boolExpr = ""
                        + (leftDt == null && rightDt != null || leftDt != null && !leftDt.equals(rightDt));

                    // build the boolean expression
                    if (condCase == 4) {
                        boolExpr = "" + (leftDt == null);
                    }
                    else if (!Arrays.contains(new int[]{
                        1, 2, 3, 4
                    }, condCase)) throw new MyTowerException("Provided expression did not match any predefined case");

                    if (boolExpr == null || boolExpr
                        .isEmpty()) throw new MyTowerException("Error: BooleanBuilder is empty for some reason..");

                    if (!isFirstExpr && expr.getRuleOperator() == null) throw new MyTowerException(
                        "Syntax error: rule operator is mandatory but was not provided");

                    if (!isFirstExpr) {
                        if (isFirstExpr
                            || RuleOperator.AND.getCode().equals(expr.getRuleOperator())) globalBoolExpr += " && ";
                        else if (RuleOperator.OR.getCode().equals(expr.getRuleOperator())) globalBoolExpr += " || ";
                    }

                    globalBoolExpr += boolExpr;

                    isFirstExpr = false;
                }

            }

            if (codeAlpha != null && codeAlpha.length() > 0) tt.setCodeAlphaPslCourant(codeAlpha);

            if (globalBoolExpr != null && !globalBoolExpr.isEmpty()) {
                // System.out.println("===> " + globalBoolExpr);
                Boolean resBoolExpr = (Boolean) engine.eval(globalBoolExpr);
                if (resBoolExpr != null && resBoolExpr) result.add(tt);
            }

        }

        return result;
    }

    public List<EbDemande> postProcessDocumentRules(
        EbControlRule rule,
        List<EbDemande> listDemande,
        EbUser connectedUser,
        Integer ebTtTracingNum)
        throws Exception {
        List<EbTypeDocuments> listDocumentStatus = new ArrayList<>();

        if (rule.getListTypeDocStatus() != null && rule.getListTypeDocStatus().length() > 0) {
            String[] listDoc = rule.getListTypeDocStatus().split("\\|");

            for (String doc: listDoc) {
                String[] listDocStatus = doc.split(":");
                EbTypeDocuments dc = new EbTypeDocuments();
                dc.setEbTypeDocumentsNum(Integer.valueOf(listDocStatus[0]));
                dc.setStatus(Integer.valueOf(listDocStatus[1]));
                listDocumentStatus.add(dc);
            }

        }

        if (rule.getChangeDocumentStatus() != null && rule.getChangeDocumentStatus() && !listDocumentStatus.isEmpty()) {
            connectedUser = connectedUser == null ? connectedUserService.getCurrentUser() : connectedUser;
            Integer ebCompagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

            List<EbTypeDocuments> typeDocumentSetting = this.ebTypeDocumentsRepository
                .findAllByXEbCompagnie(ebCompagnieNum);
            List<EbDemande> demandesToUpdate = new ArrayList<>();

            for (EbDemande d: listDemande) {
                List<EbTypeDocuments> listTypeDocDemande = new ArrayList<>();
                d.getListTypeDocuments().stream().forEach(dc -> listTypeDocDemande.add(dc));

                for (EbTypeDocuments docStatus: listDocumentStatus) {

                    if (listTypeDocDemande != null && !listTypeDocDemande.isEmpty()) {
                        boolean statusUpdated = false;

                        EbTypeDocuments typeDoc = listTypeDocDemande // searching
                                                                     // typedoc
                                                                     // in
                                                                     // demande
                                                                     // list of
                                                                     // typedocs
                            .stream()
                            .filter(doc -> doc.getEbTypeDocumentsNum().equals(docStatus.getEbTypeDocumentsNum()))
                            .findFirst().orElse(null);

                        if (typeDoc == null) { // type doc does not exist in
                                               // demande, getting it from
                                               // settings
                            typeDoc = typeDocumentSetting
                                .stream()
                                .filter(doc -> doc.getEbTypeDocumentsNum().equals(docStatus.getEbTypeDocumentsNum()))
                                .findFirst().orElse(null);
                            if (typeDoc != null) listTypeDocDemande.add(typeDoc); // add
                                                                                  // typedoc
                                                                                  // to
                                                                                  // demande
                                                                                  // list
                                                                                  // of
                                                                                  // typedocs
                        }

                        if (typeDoc != null) {
                            // update doc status

                            if (CREnumeration.DocumentStatus.EXPECTED.getCode().equals(docStatus.getStatus())) {
                                typeDoc.setIsexpectedDoc(true);
                                statusUpdated = true;
                            }
                            else if (CREnumeration.DocumentStatus.DEACTIVATED.getCode().equals(docStatus.getStatus())) {
                                typeDoc.setActivated(false);
                                statusUpdated = true;
                            }
                            else if (CREnumeration.DocumentStatus.REQUIRED.getCode().equals(docStatus.getStatus())) {
                                typeDoc.setIsexpectedDoc(true);
                                typeDoc.setRequired(true);
                                statusUpdated = true;
                            }
                            else if (CREnumeration.DocumentStatus.ACTIVATED.getCode().equals(docStatus.getStatus())) {
                                typeDoc.setActivated(true);
                                statusUpdated = true;
                            }
                            else if (CREnumeration.DocumentStatus.NO.getCode().equals(docStatus.getStatus())) {
                                typeDoc.setIsexpectedDoc(false);
                                typeDoc.setRequired(false);
                                statusUpdated = true;
                            }

                            if (statusUpdated) {
                                EbDemande demandeToUpdate = this.ebDemandeRepository.getOne(d.getEbDemandeNum());
                                demandeToUpdate.setListTypeDocuments(listTypeDocDemande);
                                demandesToUpdate.add(demandeToUpdate);
                            }

                        }

                    }

                }

            }

            if (!demandesToUpdate.isEmpty()) {
                this.ebDemandeRepository.saveAll(demandesToUpdate);
            }

        }

        if (rule.getExportControl() != null && rule.getExportControl()) {
            this.setExportControl(listDemande);
        }

        if (rule.getSendCotation() != null && rule.getSendCotation()) {
            this.sendQuotations(listDemande, connectedUser);
        }

        if (rule.getChangeDemandesStatus() != null && rule.getChangeDemandesStatus()) {
            this.changeStatusDemandes(rule, listDemande);
        }

        if (rule.getGenerateDocument() != null && rule.getGenerateDocument()) {
            this.generateDocuments(rule, listDemande, connectedUser);
        }

        if (rule.getGenerateTr() != null && rule.getGenerateTr()) {
            listDemande = this.applyOperationForTrGeneration(rule, listDemande, connectedUser);
        }

        if (rule.getGenererNotif() != null && rule.getGenererNotif()) {
            this.sendNotification(rule, listDemande, connectedUser, ebTtTracingNum);
        }

        if (rule.getEnvoyerEmail() != null && rule.getEnvoyerEmail()) {
            this.sendMail(rule, listDemande, connectedUser);
        }

        return listDemande;
    }

    private void
        sendNotification(
            EbControlRule rule,
            List<EbDemande> listDemande,
            EbUser connectedUser,
            Integer ebTtTracingNum) {
        if (rule.getEmailDestinataires() == null) throw new MyTowerException(
            "Send email activated for rule" + rule.getCode() + ", but no destination provided");
        List<EbUser> listUser = new ArrayList<EbUser>();
        List<String> emails = java.util.Arrays.asList(rule.getEmailDestinataires().split(";"));

        for (String email: emails) {
            EbUser userExists = ebUserRepository.findByEmail(email);
            if (userExists != null) listUser.add(userExists);
        }

        if (!listUser.isEmpty()) {

            for (EbDemande ebDemande: listDemande) {

                for (EbUser user: listUser) {
                    EbNotification notification = new EbNotification();
                    notification.setType(TypeNotificationEnum.NOTIFICATION.getCode());
                    notification.setEbUser(user);
                    notification.setEbCompagnie(user.getEbCompagnie());
                    notification.setHeader(rule.getEmailObjet());
                    notification.setNumEntity(ebTtTracingNum);
                    notification.setModule(Enumeration.Module.TRACK.getCode());
                    String msg = "";

                    if (rule.getEmailMessage() != null) {
                        String refControl = ebDemande.getRefTransport();
                        String statusControl = StatutDemande.getLibelle(ebDemande.getxEcStatut());
                        String notifBody = rule.getEmailMessage();
                        notifBody = notifBody.replace("\n", "<br>");
                        notifBody = notifBody.replace("${transport.reference}", refControl);
                        notifBody = notifBody.replace("${transport.status}", statusControl);
                        msg += "<span>" + notifBody + "</span>";
                    }

                    notification.setBody(msg);

                    notificationService.addNotification(notification);
                    notificationService
                        .updateNotificationNumberForUser(
                            user.getEbUserNum(),
                            TypeNotificationEnum.NOTIFICATION.getCode());
                }

            }

        }

    }

    private void sendMail(EbControlRule rule, List<EbDemande> listDemande, EbUser connectedUser) {

        if (rule.getEmailDestinataires() == null) throw new MyTowerException(
            "Send email activated for rule" + rule.getCode() + ", but no destination provided");
        else {

            for (EbDemande ebDemande: listDemande) {
                emailControlRuleService.sendEmailControlRule(rule, null, ebDemande);
            }

        }

    }

    public void generateDocuments(EbControlRule rule, List<EbDemande> listDemande, EbUser connectedUser)
        throws Exception {
        TemplateGenParamsDTO params = rule.getDocGenParams();

        if (params == null) throw new MyTowerException("No parameters was defined for document generation !!");
        if (params.getSelectedTemplate() == null) throw new MyTowerException(
            "No template was defined for document generation !!");
        if (params.getSelectedFormat() == null) throw new MyTowerException(
            "No format was defined for document generation !!");
        if (params
            .getAttachChecked() == null) throw new MyTowerException("No Action was defined for document generation !!");
        if (params.getSelectedTypeDoc() == null) throw new MyTowerException(
            "No Doc Type was defined for document generation !!");

        connectedUser = connectedUser == null ? connectedUserService.getCurrentUser() : connectedUser;
        Integer ebUserNum = connectedUser.getEbUserNum();
        Integer ebEtablissementNum = connectedUser.getEbEtablissement().getEbEtablissementNum();

        EbTypeDocuments selectedTypeOfDoc = this.ebTypeDocumentsRepository.getOne(params.getSelectedTypeDoc());

        if (selectedTypeOfDoc == null) throw new MyTowerException("Unkown type of doc !!!");

        EbTemplateDocuments template = ebTemplateDocumentsRepository.findById(params.getSelectedTemplate()).get();

        String fileName = template.getOriginFileName().replace(".docx", "")
            + FileExtentionEnum.getFileExtentionByCode(params.getSelectedFormat());
        params.setGeneratedFileName(fileName);

        for (EbDemande ebDemande: listDemande) {
            List<String> demandeNums = new ArrayList<>();

            logger.info("Will generate doc for " + ebDemande.getEbDemandeNum());
            demandeNums.add(String.valueOf(ebDemande.getEbDemandeNum()));
            params.setTemplateModelParams(demandeNums);

            Path generatedFile = this.templateDocumentsService.templateGeneration(params, connectedUser); // generate
                                                                                                          // the file
                                                                                                          // based on
                                                                                                          // selected
                                                                                                          // template

            if (generatedFile != null && Files.exists(generatedFile)) {

                if (params.getAttachChecked()) {
                    MultipartFile[] files = {
                        DocumentUtils.convertFileToMultipartFileObject(generatedFile, fileName) // convert generated
                                                                                                // file to
                                                                                                // multipartfile object
                                                                                                // type
                    };

                    this.fileUploadService
                        .handleFileUploadDemande(
                            // store file and append it to demande docs
                            selectedTypeOfDoc.getCode(),
                            UploadDirectory.PRICING_BOOKING.getCode(),
                            ebDemande.getEbDemandeNum(),
                            ebUserNum,
                            ebEtablissementNum,
												Enumeration.Module.PRICING.getCode(),
                            null,
                            null,
                            files,
                            false);
                }

                Files.deleteIfExists(generatedFile); // delete the old file
            }
            else {
                throw new MyTowerException("GeneratedFile does not exist");
            }

            logger.info("Generated doc for demandes : " + demandeNums.toString());
        }

    }

    public List<EbDemande>
        applyOperationForTrGeneration(EbControlRule rule, List<EbDemande> listDemande, EbUser connectedUser)
            throws Exception {
        List<EbControlRuleOperation> listActions = null;
        EbDemande newDemande = null;
        List<EbDemande> listNewDemandes = null;
        connectedUser = connectedUser == null ? connectedUserService.getCurrentUser() : connectedUser;
        Integer ebCompagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        if (listDemande == null || listDemande.isEmpty()) return null;
        listNewDemandes = new ArrayList<>();

        if (rule.getListRuleOperations() != null && !rule.getListRuleOperations().isEmpty()) {
            listActions = rule
                .getListRuleOperations().stream()
                .filter(
                    op -> op.getOperationType() != null
                        && (op.getOperationType() == ControlRuleOperationTypesEnum.IS_TR_GENERATION.getCode()))
                .collect(Collectors.toList());
        }

        if (listActions == null || listActions.isEmpty()) {
            // no action found

            for (EbDemande ebDemande: listDemande) {

                if (!ebDemande.getxEcNature().equals(Enumeration.NatureDemandeTransport.CONSOLIDATION.getCode())) {
                    ebDemande
                        .setListMarchandises(
                            this.ebMarchandiseRepository.findAllByEbDemandeEbDemandeNum(ebDemande.getEbDemandeNum()));

                    newDemande = this.initEbDemandeDefaultFields(ebDemande);
                    listNewDemandes.add(newDemande);
                }

            }

        }
        else {
            SearchCriteria criteria = new SearchCriteria();
            criteria.setIncludeGlobalValues(true);
            criteria.setEbCompagnieNum(ebCompagnieNum);
            criteria.setActivated(true);
            List<EbTypeRequest> listTypeRequests = typeRequestService.searchTypeRequest(criteria);

            for (EbDemande ebDemande: listDemande) {

                if (!ebDemande.getxEcNature().equals(Enumeration.NatureDemandeTransport.CONSOLIDATION.getCode())) {

                    if (ebDemande.getEbPartyOrigin() != null && ebDemande.getEbPartyOrigin().getEbPartyNum() != null) {
                        ebDemande
                            .setEbPartyOrigin(
                                this.ebPartyRepository.findByEbPartyNum(ebDemande.getEbPartyOrigin().getEbPartyNum()));
                    }

                    if (ebDemande.getEbPartyDest() != null && ebDemande.getEbPartyDest().getEbPartyNum() != null) {
                        ebDemande
                            .setEbPartyDest(
                                this.ebPartyRepository.findByEbPartyNum(ebDemande.getEbPartyDest().getEbPartyNum()));
                    }

                    ebDemande
                        .setListMarchandises(
                            this.ebMarchandiseRepository.findAllByEbDemandeEbDemandeNum(ebDemande.getEbDemandeNum()));

                    newDemande = this.initEbDemandeDefaultFields(ebDemande);

                    for (EbControlRuleOperation action: listActions) {

                        if (action.getOperator() != null) {

                            if (action.getOperator().equals(Operator.INTERCHANGE.getCode())) {
                                newDemande = this.applyInterchange(newDemande, ebDemande, action, listTypeRequests);
                            }
                            else if (action.getOperator().equals(Operator.BECOMES.getCode())) {
                                newDemande = this
                                    .applyBecomes(newDemande, ebDemande, action, ebCompagnieNum, listTypeRequests);
                            }
                            else if (action.getOperator().equals(Operator.IS_CONSERVED.getCode())) // just
                                                                                                   // in
                                                                                                   // case
                                                                                                   // if
                                                                                                   // the
                                                                                                   // field
                                                                                                   // was
                                                                                                   // not
                                                                                                   // treated
                            {
                                newDemande = this.applyIsConserved(newDemande, ebDemande, action, listActions);
                            }
                            else if (action.getOperator().equals(Operator.IS_DELETED.getCode())) {
                                newDemande = this.applyIsDeleted(newDemande, ebDemande, action, listActions);
                            }
                            else throw new MyTowerException("Unkown Action Operand Type !!");

                        }
                        else throw new MyTowerException("Action without Operator !!");

                    }

                    listNewDemandes.add(newDemande);
                }

            }

        }

        if (listNewDemandes != null && !listNewDemandes.isEmpty()) {
            for (EbDemande d: listNewDemandes) d = this.pricingService.saveGeneratedEbDemande(d, connectedUser);
        }

        return listNewDemandes;
    }

    public EbDemande applyInterchange(
        EbDemande newDemande,
        EbDemande ebDemande,
        EbControlRuleOperation action,
        List<EbTypeRequest> listTypeRequests) {
        Integer leftOpr = Integer.valueOf(action.getLeftOperands());
        Integer rightOpr = Integer.valueOf(action.getRightOperands());

        if (leftOpr != null && rightOpr != null) {

            if ((leftOpr.equals(RuleOperandType.PAYS_ORIGIN.getCode())
                && rightOpr.equals(RuleOperandType.DESTINATION_COUNTRY.getCode()))
                || (leftOpr.equals(RuleOperandType.DESTINATION_COUNTRY.getCode())
                    && rightOpr.equals(RuleOperandType.PAYS_ORIGIN.getCode()))) {
                newDemande.setEbPartyOrigin(this.pricingService.cloneEbParty(ebDemande.getEbPartyDest())); // org
                                                                                                           // to
                                                                                                           // dest
                newDemande.setEbPartyDest(this.pricingService.cloneEbParty(ebDemande.getEbPartyOrigin())); // dest
                                                                                                           // to
                                                                                                           // org
            }
            else if (newDemande.getListMarchandises() != null && !newDemande.getListMarchandises().isEmpty()
                && leftOpr.equals(RuleOperandType.UNITS.getCode())) {
                Integer leftUnitCode = action.getLeftUnitFieldCode();
                if (leftUnitCode == null) throw new MyTowerException("Unknown left unit field property");

                List<EbMarchandise> units = this
                    .interchangeValuesInUnit(
                        newDemande.getListMarchandises(),
                        ebDemande.getListMarchandises(),
                        leftUnitCode,
                        rightOpr);
                newDemande.setListMarchandises(units);
            }
            else if (leftOpr.equals(RuleOperandType.CUSTOM_FIELD.getCode())
                && rightOpr.equals(RuleOperandType.CUSTOM_FIELD.getCode())) {
                Integer ebLeftCustomField = action.getEbLeftCustomFieldNum();
                if (ebLeftCustomField == null) throw new MyTowerException("No Left CustomField was defined !!!");

                Integer ebRightCustomField = action.getEbRightCustomFieldNum();
                if (ebRightCustomField == null) throw new MyTowerException("No Right CustomField was defined !!!");

                newDemande = this.interchange_CustomFields(newDemande, ebLeftCustomField, ebRightCustomField);
            }

        }

        return newDemande;
    }

    private EbDemande
        interchange_CustomFields(EbDemande newDemande, Integer ebLeftCustomField, Integer ebRightCustomField) {
        CustomFields leftCF = newDemande
            .getListCustomsFields().stream().filter(c -> c.getNum().equals(ebLeftCustomField)).findFirst().orElse(null);

        CustomFields rightCF = newDemande
            .getListCustomsFields().stream().filter(c -> c.getNum().equals(ebRightCustomField)).findFirst()
            .orElse(null);

        if (leftCF != null && rightCF != null) {
            String leftCfValue = leftCF.getValue();
            String rightCfValue = rightCF.getValue();

            leftCF.setValue(rightCfValue);
            rightCF.setValue(leftCfValue);

            newDemande.setCustomFields(newDemande.getListCustomsFields());
        }

        return newDemande;
    }

    private List<EbMarchandise> interchangeValuesInUnit(
        List<EbMarchandise> units,
        List<EbMarchandise> origineUnits,
        Integer leftUnitCode,
        Integer rightUnitCode) {
        CrUnitFields leftUnit = CrUnitFields.getUnitInfoByCode(leftUnitCode);
        CrUnitFields rightUnit = CrUnitFields.getUnitInfoByCode(rightUnitCode);

        PropertyAccessor marchandiseClassAccessor = null;

        for (EbMarchandise m: units) {
            // getting fields values from current object
            Object leftValueObj = this.getFieldPropertyValue(leftUnit, origineUnits, m);
            Object rightValueObj = this.getFieldPropertyValue(rightUnit, origineUnits, m);

            String rightValue = rightValueObj != null ? rightValueObj.toString() : null;
            String leftValue = leftValueObj != null ? leftValueObj.toString() : null;

            // interchanging values
            marchandiseClassAccessor = PropertyAccessorFactory.forBeanPropertyAccess(m);

            marchandiseClassAccessor.setPropertyValue(leftUnit.getFieldName(), rightValue);
            marchandiseClassAccessor.setPropertyValue(rightUnit.getFieldName(), leftValue);
        }

        return units;
    }

    private Object getFieldPropertyValue(CrUnitFields unitField, List<EbMarchandise> origineUnits, EbMarchandise m) {

        if (unitField.isDefault()) {
            return PropertyAccessorFactory.forBeanPropertyAccess(m).getPropertyValue(unitField.getFieldName());
        }
        else {
            return this
                .getFieldPropertyValueFromOriginUnit(origineUnits, m.getEbMarchandiseNum(), unitField.getFieldName());
        }

    }

    private Object getFieldPropertyValueFromOriginUnit(
        List<EbMarchandise> orgUnits,
        Integer ebMarchandiseNum,
        String propertyName) {
        EbMarchandise originUnit = orgUnits
            .stream().filter(un -> un.getEbMarchandiseNum().equals(ebMarchandiseNum)).findFirst().orElse(null);

        if (originUnit == null) return null;

        return PropertyAccessorFactory.forBeanPropertyAccess(originUnit).getPropertyValue(propertyName);
    }

    private EbDemande applyBecomes(
        EbDemande newDemande,
        EbDemande ebDemande,
        EbControlRuleOperation action,
        Integer ebCompagnieNum,
        List<EbTypeRequest> listTypeRequests) {
        Integer leftOpr = Integer.valueOf(action.getLeftOperands());
        String value = action.getRightOperands();

        if (leftOpr.equals(RuleOperandType.UNITS.getCode()) && newDemande.getListMarchandises() != null
            && !newDemande.getListMarchandises().isEmpty()) {
            Integer unitFieldCode = action.getLeftUnitFieldCode();
            CrUnitFields unitField = CrUnitFields.getUnitInfoByCode(unitFieldCode);

            List<EbMarchandise> units = this
                .handleUnitsBecomesOperator(newDemande.getListMarchandises(), unitField, value);
            newDemande.setListMarchandises(units);
        }
        else if (leftOpr.equals(RuleOperandType.MODE_DE_TRANSPORT.getCode())) {
            Integer xEcModeTransport = Integer.valueOf(value);
            newDemande.setxEcModeTransport(xEcModeTransport);
        }
        else if (leftOpr.equals(RuleOperandType.FLOW_TYPE.getCode())) {
            Integer xEbTypeFluxNum = Integer.valueOf(value);
            newDemande.setxEbTypeFluxNum(xEbTypeFluxNum);
        }
        else if (leftOpr.equals(RuleOperandType.TYPE_OF_REQUEST.getCode())) {

            if (listTypeRequests != null && !listTypeRequests.isEmpty()) {
                Integer xEbTypeRequest = Integer.valueOf(value);
                EbTypeRequest typeRequest = listTypeRequests
                    .stream().filter(r -> r.getEbTypeRequestNum().equals(xEbTypeRequest)).findFirst().orElse(null);

                if (typeRequest != null) {
                    newDemande.setxEbTypeRequest(typeRequest.getEbTypeRequestNum());
                    newDemande.setTypeRequestLibelle(typeRequest.getLibelle());
                }

            }

        }
        else if (leftOpr.equals(RuleOperandType.LEG.getCode())) {
            newDemande.setScenario(value);
        }
        else if (leftOpr.equals(RuleOperandType.REF_ADR_ORG.getCode())) {
            newDemande.getEbPartyOrigin().setReference(value);
        }
        else if (leftOpr.equals(RuleOperandType.REF_ADR_DEST.getCode())) {
            newDemande.getEbPartyDest().setReference(value);
        }
        else if (leftOpr.equals(RuleOperandType.PSL_SCHEMA.getCode())) {
            Integer ebTtSchemaPslNum = Integer.valueOf(value);
            EbTtSchemaPsl schemaPsl = this.ebTtSchemaPslRepository.findOneByEbTtSchemaPslNum(ebTtSchemaPslNum);
            newDemande.setxEbSchemaPsl(schemaPsl);
        }
        else if (leftOpr.equals(RuleOperandType.COST_CENTER.getCode())) {
            Integer ebCostCenterNum = Integer.valueOf(value);
            newDemande.setxEbCostCenter(new EbCostCenter(ebCostCenterNum));
        }
        else {
            RuleOperandType field = RuleOperandType.getRuleOperandTypeEnumByKey(leftOpr);

            if (field != null) {

                if (field.getType().equals(CrFieldTypes.CATEGORIE.getCode())) // if
                                                                              // field
                                                                              // is
                                                                              // categorie
                {
                    Integer ebLabelNum = Integer.valueOf(value);
                    newDemande = this.handleCategoriesBecomeOperator(newDemande, action, ebLabelNum);
                }
                else if (field.getType().equals(CrFieldTypes.CUSTOMFIELD.getCode())) {
                    newDemande = this.handleCustomFieldsBecomeOperator(newDemande, action, value, ebCompagnieNum);
                }

            }
            else throw new MyTowerException("Unkown field !!!");

        }

        return newDemande;
    }

    private EbDemande
        handleCategoriesBecomeOperator(EbDemande newDemande, EbControlRuleOperation action, Integer ebLabelNum) {
        Integer ebCategorieNum = action.getEbCategorieNum();

        if (ebLabelNum != null) {
            List<EbCategorie> listCategories = newDemande.getListCategories();

            if (listCategories == null || listCategories.isEmpty()) listCategories = new ArrayList<>();

            EbLabel lb = new EbLabel(), selectedLabel = this.ebLabelRepository.findByEbLabelNum(ebLabelNum); // getting
                                                                                                             // label

            lb.setEbLabelNum(selectedLabel.getEbLabelNum());
            lb.setLibelle(selectedLabel.getLibelle());
            lb.setDeleted(selectedLabel.getDeleted());

            Set<EbLabel> labels = new HashSet<>();
            labels.add(lb);

            EbCategorie categorie = null, c = listCategories
                .stream().filter(ctg -> ctg.getEbCategorieNum().equals(ebCategorieNum)).findFirst().orElse(null);

            if (c == null) {
                c = this.ebCategorieRepository.findById(ebCategorieNum).orElse(null);

                if (c == null) throw new MyTowerException("Categorie not found for compagnie!!");

                categorie = new EbCategorie();
                categorie.setEbCategorieNum(c.getEbCategorieNum());
                categorie.setLibelle(c.getLibelle());
                categorie.setLabels(labels);

                listCategories.add(categorie);
                newDemande.setListCategories(listCategories);
            }
            else {
                c.setLabels(labels);
                newDemande.setListCategories(listCategories);
            }

        }
        else throw new MyTowerException("No Label was specified for Categorie !!!");

        return newDemande;
    }

    private EbDemande handleCustomFieldsBecomeOperator(
        EbDemande newDemande,
        EbControlRuleOperation action,
        String value,
        Integer ebCompagnieNum) {
        Integer ebLeftCustomFieldNum = action.getEbLeftCustomFieldNum();
        if (ebLeftCustomFieldNum == null) throw new MyTowerException("No Left CustomField was defined !!!");

        if (newDemande.getListCustomsFields() != null && !newDemande.getListCustomsFields().isEmpty()) {
            CustomFields cf = newDemande
                .getListCustomsFields().stream().filter(c -> c.getNum().equals(ebLeftCustomFieldNum)).findFirst()
                .orElse(null);

            if (cf != null) {
                cf.setValue(value);
                newDemande.setCustomFields(newDemande.getListCustomsFields());
            }
            else // CF does not exist in demande's CFs
            {
                EbCustomField ebCustomField = this.ebCustomFieldRepository.getCustomFieldByXEbCompagnie(ebCompagnieNum);

                if (ebCustomField.getListCustomsFields() != null && !ebCustomField.getListCustomsFields().isEmpty()) {
                    cf = ebCustomField
                        .getListCustomsFields().stream().filter(c -> c.getNum().equals(ebLeftCustomFieldNum))
                        .findFirst().orElse(null);

                    if (cf != null) {
                        cf.setValue(value);
                        newDemande.getListCustomsFields().add(cf);
                        newDemande.setCustomFields(newDemande.getListCustomsFields());
                    }

                }

            }

        }
        else // demande with empty CFs
        {
            List<CustomFields> cfs = new ArrayList<>();
            EbCustomField ebCustomField = this.ebCustomFieldRepository.getCustomFieldByXEbCompagnie(ebCompagnieNum);

            if (ebCustomField.getListCustomsFields() != null && !ebCustomField.getListCustomsFields().isEmpty()) {
                CustomFields cf = ebCustomField
                    .getListCustomsFields().stream().filter(c -> c.getNum().equals(ebLeftCustomFieldNum)).findFirst()
                    .orElse(null);

                if (cf != null) {
                    cf.setValue(value);
                    cfs.add(cf);
                    newDemande.setCustomFields(cfs);
                } // else Current compagnie does not have current CF or it was
                  // deleted

            }

        }

        return newDemande;
    }

    private EbIncoterm getIncoterm(Integer ebIncotermNum, Integer ebCompagnieNum) {
        SearchCriteria criteria = new SearchCriteria();
        criteria.setEbCompagnieNum(ebCompagnieNum);
        List<EbIncoterm> incoterms = this.ebIncotermService.getListIncoterms(criteria);
        EbIncoterm inc = incoterms
            .stream().filter(i -> i.getEbIncotermNum().equals(ebIncotermNum)).findFirst().orElse(null);
        return inc;
    }

    private List<EbMarchandise>
        handleUnitsBecomesOperator(List<EbMarchandise> units, CrUnitFields unitField, String value) {

        if (this.isSimpleField(unitField)) {
            for (EbMarchandise ebMarchandise: units) PropertyAccessorFactory
                .forBeanPropertyAccess(ebMarchandise).setPropertyValue(unitField.getFieldName(), value);
        }
        else if (unitField.getCode().equals(CrUnitFields.ORIGIN_COUNTRY.getCode())) // country
                                                                                    // stand
                                                                                    // for
                                                                                    // num,
                                                                                    // code
                                                                                    // and
                                                                                    // label
        {
            Integer ecCountryNum = Integer.valueOf(value);
            if (ecCountryNum == null) throw new MyTowerException("No Country choosed !!!!");

            EcCountry country = this.ecCountryRepository.findByEcCountryNum(ecCountryNum);

            for (EbMarchandise ebMarchandise: units) {
                ebMarchandise.setxEcCountryOrigin(country.getEcCountryNum());
                ebMarchandise.setOriginCountryName(country.getLibelle());
                ebMarchandise.setOriginCountryCode(country.getCode());
            }

        }
        else if (unitField.getCode().equals(CrUnitFields.UNIT_TYPE.getCode())) // unit
                                                                               // type
                                                                               // stand
                                                                               // for
                                                                               // xEbTypeUnit
                                                                               // and
                                                                               // labeltypeunit
        {
            Long ebTypeUnitNum = Long.valueOf(value);
            if (ebTypeUnitNum == null) throw new MyTowerException("No Unit Type selected !!!!");

            EbTypeUnit unitType = this.ebTypeUnitRepository.findByEbTypeUnitNum(ebTypeUnitNum);

            for (EbMarchandise ebMarchandise: units) {
                ebMarchandise.setxEbTypeUnit(unitType.getEbTypeUnitNum());
                ebMarchandise.setLabelTypeUnit(unitType.getLibelle());
            }

        }

        return units;
    }

    private boolean isSimpleField(CrUnitFields unitField) { // those fields are
                                                            // simple types and
                                                            // they are
                                                            // independents,
                                                            // each one stand
                                                            // for it self
  // @formatter:off
		return 
			unitField.getCode().equals(CrUnitFields.HS_CODE.getCode()) || 
			unitField.getCode().equals(CrUnitFields.PART_NUMBER.getCode()) || 
			unitField.getCode().equals(CrUnitFields.SERIAL_NUMBER.getCode()) || 
			unitField.getCode().equals(CrUnitFields.ARTICLE_REF.getCode()) || 
			unitField.getCode().equals(CrUnitFields.BIN_LOCALTION.getCode()) || 
			unitField.getCode().equals(CrUnitFields.EXPORT_REF.getCode()) || 
			unitField.getCode().equals(CrUnitFields.ARTICE_NAME.getCode()) || 
			unitField.getCode().equals(CrUnitFields.UN.getCode()) || 
			unitField.getCode().equals(CrUnitFields.PRICE.getCode()) ||  
			unitField.getCode().equals(CrUnitFields.DGR.getCode()) ||  
			unitField.getCode().equals(CrUnitFields.CLASS.getCode()) ||  
			unitField.getCode().equals(CrUnitFields.VOLUME.getCode()) ||  
			unitField.getCode().equals(CrUnitFields.WEIGHT.getCode()) ||  
			unitField.getCode().equals(CrUnitFields.LENGHT.getCode()) ||  
			unitField.getCode().equals(CrUnitFields.WIDTH.getCode()) ||  
			unitField.getCode().equals(CrUnitFields.HEIGTH.getCode()) ||  
			unitField.getCode().equals(CrUnitFields.PACKAGING.getCode());
	// @formatter:on
    }

    private EbDemande applyIsConserved(
        EbDemande newDemande,
        EbDemande ebDemande,
        EbControlRuleOperation action,
        List<EbControlRuleOperation> listActions) {
        Integer leftOpr = Integer.valueOf(action.getLeftOperands());

        if (leftOpr.equals(RuleOperandType.PAYS_ORIGIN.getCode()) && // to
                                                                     // verify
                                                                     // if
                                                                     // parties
                                                                     // was not
                                                                     // treated
                                                                     // in
                                                                     // previous
                                                                     // action
                                                                     // (Interchange)
            !this.isFieldTreated(RuleOperandType.PAYS_ORIGIN.getCode(), action, listActions)) {
            newDemande.setEbPartyOrigin(this.pricingService.cloneEbParty(ebDemande.getEbPartyOrigin()));
        }

        if (leftOpr.equals(RuleOperandType.DESTINATION_COUNTRY.getCode())
            && !this.isFieldTreated(RuleOperandType.DESTINATION_COUNTRY.getCode(), action, listActions)) {
            newDemande.setEbPartyDest(this.pricingService.cloneEbParty(ebDemande.getEbPartyDest()));
        }

        if (leftOpr.equals(RuleOperandType.MODE_DE_TRANSPORT.getCode())
            && !this.isFieldTreated(RuleOperandType.MODE_DE_TRANSPORT.getCode(), action, listActions)) {
            newDemande.setxEcModeTransport(ebDemande.getxEcModeTransport());
        }

        if (leftOpr.equals(RuleOperandType.FLOW_TYPE.getCode())
            && !this.isFieldTreated(RuleOperandType.FLOW_TYPE.getCode(), action, listActions)) {
            newDemande.setxEbTypeFluxNum(ebDemande.getxEbTypeFluxNum());
            newDemande.setxEbTypeFluxDesignation(ebDemande.getxEbTypeFluxDesignation());
        }

        if (leftOpr.equals(RuleOperandType.TYPE_OF_REQUEST.getCode())
            && !this.isFieldTreated(RuleOperandType.TYPE_OF_REQUEST.getCode(), action, listActions)) {
            newDemande.setxEbTypeRequest(ebDemande.getxEbTypeRequest());
            newDemande.setTypeRequestLibelle(ebDemande.getTypeRequestLibelle());
        }

        if (leftOpr.equals(RuleOperandType.LEG.getCode())
            && !this.isFieldTreated(RuleOperandType.LEG.getCode(), action, listActions)) {
            newDemande.setScenario(ebDemande.getScenario());
        }

        if (leftOpr.equals(RuleOperandType.COST_CENTER.getCode())
            && !this.isFieldTreated(RuleOperandType.COST_CENTER.getCode(), action, listActions)) {
            newDemande.setxEbCostCenter(ebDemande.getxEbCostCenter());
        }

        if (leftOpr.equals(RuleOperandType.UNITS.getCode())) {
            Integer leftUnitFieldCode = action.getLeftUnitFieldCode();
            if (leftUnitFieldCode == null) throw new MyTowerException("Unit field not found !!!");

            CrUnitFields unitField = CrUnitFields.getUnitInfoByCode(leftUnitFieldCode);

            List<EbMarchandise> units = this
                .handleUnitsIsConservedOperator(
                    newDemande.getListMarchandises(),
                    ebDemande.getListMarchandises(),
                    unitField,
                    action,
                    listActions);
            newDemande.setListMarchandises(units);
        }

        if (leftOpr.equals(RuleOperandType.MANDATORY_FIELDS.getCode())) {
            newDemande = this.handleMandatoryFieldsIsConservedOperator(newDemande, ebDemande, action, listActions);
        }

        return newDemande;
    }

    private List<EbMarchandise> handleUnitsIsConservedOperator(
        List<EbMarchandise> units,
        List<EbMarchandise> orgUnits,
        CrUnitFields unitField,
        EbControlRuleOperation action,
        List<EbControlRuleOperation> listActions) {

        if (unitField.getCode().equals(CrUnitFields.ALL.getCode())) {
            List<CrUnitFields> unitFields = CrUnitFields.getUnitFields();

            for (CrUnitFields field: unitFields) {
                if (!this.isFieldTreated(field.getCode(), action, listActions)) units = this
                    .conserveUnitField(units, orgUnits, field);
            }

        }
        else if (!this.isFieldTreated(unitField.getCode(), action, listActions)) {
            units = this.conserveUnitField(units, orgUnits, unitField);
        }

        return units;
    }

    private List<EbMarchandise>
        conserveUnitField(List<EbMarchandise> units, List<EbMarchandise> orgUnits, CrUnitFields unitField) {

        if (!unitField.isDefault()) // default field are conserved by default,
                                    // see initEbDemandeDefaultFields(..)
        {

            if (this.isSimpleField(unitField)) {

                for (EbMarchandise ebMarchandise: units) {
                    Object fieldValue = this
                        .getFieldPropertyValueFromOriginUnit(
                            orgUnits,
                            ebMarchandise.getEbMarchandiseNum(),
                            unitField.getFieldName());
                    String value = fieldValue != null ? fieldValue.toString() : null;
                    PropertyAccessorFactory
                        .forBeanPropertyAccess(ebMarchandise).setPropertyValue(unitField.getFieldName(), value);
                }

            }
            else if (unitField.getCode().equals(CrUnitFields.ORIGIN_COUNTRY.getCode())) {

                for (EbMarchandise ebMarchandise: units) {
                    EbMarchandise orgUnit = orgUnits
                        .stream().filter(u -> u.getEbMarchandiseNum().equals(ebMarchandise.getEbMarchandiseNum()))
                        .findFirst().orElse(null);

                    ebMarchandise.setxEcCountryOrigin(orgUnit.getxEcCountryOrigin());
                    ebMarchandise.setOriginCountryName(orgUnit.getOriginCountryName());
                    ebMarchandise.setOriginCountryCode(orgUnit.getOriginCountryCode());
                }

            }
            else if (unitField.getCode().equals(CrUnitFields.UNIT_TYPE.getCode())) {

                for (EbMarchandise ebMarchandise: units) {
                    EbMarchandise orgUnit = orgUnits
                        .stream().filter(u -> u.getEbMarchandiseNum().equals(ebMarchandise.getEbMarchandiseNum()))
                        .findFirst().orElse(null);

                    ebMarchandise.setxEbTypeUnit(orgUnit.getxEbTypeUnit());
                    ebMarchandise.setLabelTypeUnit(orgUnit.getLabelTypeUnit());
                }

            }

        }

        return units;
    }

    private EbDemande handleMandatoryFieldsIsConservedOperator(
        EbDemande newDemande,
        EbDemande ebDemande,
        EbControlRuleOperation currentAction,
        List<EbControlRuleOperation> allActions) {
        List<RuleOperandType> requiredTrFields = RuleOperandType.getEbDemandeRequiredFields();

        for (RuleOperandType field: requiredTrFields) {

            if (!this.isFieldTreated(field.getCode(), currentAction, allActions)) {

                if (field.getCode().equals(RuleOperandType.PAYS_ORIGIN.getCode())) {
                    newDemande.setEbPartyOrigin(this.pricingService.cloneEbParty(ebDemande.getEbPartyOrigin()));
                }
                else if (field.getCode().equals(RuleOperandType.DESTINATION_COUNTRY.getCode())) {
                    newDemande.setEbPartyDest(this.pricingService.cloneEbParty(ebDemande.getEbPartyDest()));
                }
                else {
                    Object value = PropertyAccessorFactory
                        .forBeanPropertyAccess(ebDemande).getPropertyValue(field.getFieldName());

                    PropertyAccessorFactory
                        .forBeanPropertyAccess(newDemande).setPropertyValue(field.getFieldName(), value);
                }

            }

        }

        return newDemande;
    }

    private EbDemande applyIsDeleted(
        EbDemande newDemande,
        EbDemande ebDemande,
        EbControlRuleOperation action,
        List<EbControlRuleOperation> listActions) {
        Integer leftOpr = Integer.valueOf(action.getLeftOperands());

        if (leftOpr.equals(RuleOperandType.PAYS_ORIGIN.getCode())) {
            if (this.isFieldTreated(RuleOperandType.PAYS_ORIGIN.getCode(), action, listActions)) // delete field from
                                                                                                 // new demande
                newDemande.setEbPartyOrigin(null);
            else ebDemande.setEbPartyOrigin(null); // delete if from origin
                                                   // demande
        }
        else if (leftOpr.equals(RuleOperandType.DESTINATION_COUNTRY.getCode())) {
            if (this.isFieldTreated(RuleOperandType.DESTINATION_COUNTRY.getCode(), action, listActions)) newDemande
                .setEbPartyOrigin(null);
            else ebDemande.setEbPartyOrigin(null);
        }
        else if (leftOpr.equals(RuleOperandType.UNITS.getCode())) {
            Integer leftUnitFieldCode = action.getLeftUnitFieldCode();
            if (leftUnitFieldCode == null) throw new MyTowerException("No Unit field found !");

            CrUnitFields unitField = CrUnitFields.getUnitInfoByCode(leftUnitFieldCode);
            List<EbMarchandise> units = this.handleUnitsIsDeletedOperator(newDemande.getListMarchandises(), unitField);

            newDemande.setListMarchandises(units);
        }
        else if (leftOpr.equals(RuleOperandType.MODE_DE_TRANSPORT.getCode())) {
            if (this.isFieldTreated(RuleOperandType.MODE_DE_TRANSPORT.getCode(), action, listActions)) newDemande
                .setxEcModeTransport(null);
            else ebDemande.setxEcModeTransport(null);
        }
        else if (leftOpr.equals(RuleOperandType.FLOW_TYPE.getCode())) {
            if (this.isFieldTreated(RuleOperandType.FLOW_TYPE.getCode(), action, listActions)) newDemande
                .setxEbTypeFluxNum(null);
            else ebDemande.setxEbTypeFluxNum(null);
        }
        else if (leftOpr.equals(RuleOperandType.TYPE_OF_REQUEST.getCode())) {

            if (this.isFieldTreated(RuleOperandType.TYPE_OF_REQUEST.getCode(), action, listActions)) {
                newDemande.setxEbTypeRequest(null);
                newDemande.setTypeRequestLibelle(null);
            }
            else {
                ebDemande.setxEbTypeRequest(null);
                ebDemande.setTypeRequestLibelle(null);
            }

        }
        else if (leftOpr.equals(RuleOperandType.LEG.getCode())) {
            if (this.isFieldTreated(RuleOperandType.LEG.getCode(), action, listActions)) newDemande.setScenario(null);
            else ebDemande.setScenario(null);
        }
        else if (leftOpr.equals(RuleOperandType.MANDATORY_FIELDS.getCode())) {
            newDemande = this.handleMandatoryFieldsDeleteOperator(newDemande, ebDemande, action, listActions);
        }
        else if (leftOpr.equals(RuleOperandType.COST_CENTER.getCode())) {
            if (this.isFieldTreated(RuleOperandType.COST_CENTER.getCode(), action, listActions)) newDemande
                .setxEbCostCenter(null);
            else ebDemande.setxEbCostCenter(null);
        }
        else {
            RuleOperandType field = RuleOperandType.getRuleOperandTypeEnumByKey(leftOpr);

            if (field != null) {

                if (field.getType().equals(CrFieldTypes.CATEGORIE.getCode())) {
                    Integer ebCategorieNum = action.getEbCategorieNum();

                    if (ebCategorieNum == null) throw new MyTowerException("No Categorie was specified !!!");
                    else {
                        List<EbCategorie> newListCategories = newDemande
                            .getListCategories().stream().filter(c -> !c.getEbCategorieNum().equals(ebCategorieNum))
                            .collect(Collectors.toList());
                        newDemande.setListCategories(newListCategories);
                    }

                }
                else if (field.getType().equals(CrFieldTypes.CUSTOMFIELD.getCode())) {
                    Integer ebLeftCustomFieldNum = action.getEbLeftCustomFieldNum();
                    if (ebLeftCustomFieldNum == null) throw new MyTowerException("No Left CustomField was defined !!!");

                    if (newDemande.getListCustomsFields() != null && !newDemande.getListCustomsFields().isEmpty()) {
                        List<CustomFields> cfs = newDemande
                            .getListCustomsFields().stream().filter(c -> !c.getNum().equals(ebLeftCustomFieldNum))
                            .collect(Collectors.toList());
                        newDemande.setCustomFields(cfs);
                    } // else nothing to delete

                }

            }

        }

        return newDemande;
    }

    private List<EbMarchandise> handleUnitsIsDeletedOperator(List<EbMarchandise> units, CrUnitFields unitField) {

        if (unitField.getCode().equals(CrUnitFields.ALL.getCode())) {
            List<CrUnitFields> unitFields = CrUnitFields.getUnitFields();

            for (CrUnitFields field: unitFields) {
                units = this.deleteUnitField(units, field);
            }

        }
        else {
            units = this.deleteUnitField(units, unitField);
        }

        return units;
    }

    private List<EbMarchandise> deleteUnitField(List<EbMarchandise> units, CrUnitFields unitField) {

        if (this.isSimpleField(unitField)) {
            units
                .forEach(
                    un -> PropertyAccessorFactory
                        .forBeanPropertyAccess(un).setPropertyValue(unitField.getFieldName(), null));
        }
        else if (unitField.getCode().equals(CrUnitFields.ORIGIN_COUNTRY.getCode())) {
            units.forEach(un -> {
                un.setxEcCountryOrigin(null);
                un.setOriginCountryName(null);
                un.setOriginCountryCode(null);
            });
        }
        else if (unitField.getCode().equals(CrUnitFields.UNIT_TYPE.getCode())) {
            units.forEach(un -> {
                un.setxEbTypeUnit(null);
                un.setLabelTypeUnit(null);
            });
        }

        return units;
    }

    private EbDemande handleMandatoryFieldsDeleteOperator(
        EbDemande newDemande,
        EbDemande ebDemande,
        EbControlRuleOperation currentAction,
        List<EbControlRuleOperation> allActions) {
        List<RuleOperandType> requiredTrFields = RuleOperandType.getEbDemandeRequiredFields();

        for (RuleOperandType field: requiredTrFields) {

            if (this.isFieldTreated(field.getCode(), currentAction, allActions)) // removing
                                                                                 // field
                                                                                 // from
                                                                                 // new
                                                                                 // demande
            {
                PropertyAccessorFactory.forBeanPropertyAccess(newDemande).setPropertyValue(field.getFieldName(), null);
            }
            else // removing field from origin demande
            {
                PropertyAccessorFactory.forBeanPropertyAccess(ebDemande).setPropertyValue(field.getFieldName(), null);
            }

        }

        return newDemande;
    }

    private boolean isFieldTreated(
        Integer fieldCode,
        EbControlRuleOperation currentAction,
        List<EbControlRuleOperation> listActions) {
        if (currentAction == null || currentAction.getEbControlRuleOperationNum() == null) return false;

        for (EbControlRuleOperation action: listActions) {

            if (action.getEbControlRuleOperationNum() < currentAction.getEbControlRuleOperationNum()) {

                if (action.getOperator().equals(Operator.INTERCHANGE.getCode())) {
                    Integer leftOpr = Integer
                        .valueOf(action.getLeftOperands()).equals(RuleOperandType.UNITS.getCode()) ?
                            action.getLeftUnitFieldCode() :
                            Integer.valueOf(action.getLeftOperands());

                    Integer rightOpr = Integer.valueOf(action.getRightOperands());
                    if (leftOpr.equals(fieldCode) || rightOpr.equals(fieldCode)) return true;
                }
                else {
                    Integer leftOpr = Integer
                        .valueOf(action.getLeftOperands()).equals(RuleOperandType.UNITS.getCode()) ?
                            action.getLeftUnitFieldCode() :
                            Integer.valueOf(action.getLeftOperands());

                    if (leftOpr.equals(fieldCode)) return true;
                }

            }

        }

        return false;
    }

    private EbDemande initEbDemandeDefaultFields(EbDemande demande) // default
                                                                    // fields :
                                                                    // ,
                                                                    // CustomFiels,
                                                                    // LiestCatgs,
                                                                    // Status,
                                                                    // Units(PN,
                                                                    // SN, URef)
    {
        EbDemande newDemande = new EbDemande();
        List<EbMarchandise> newListMarchandises = null;
        EbMarchandise unit = null;

        newDemande.setListCategories(demande.getListCategories());
        newDemande.setCustomFields(demande.getCustomFields());
        newDemande.setListCustomsFields(demande.getListCustomsFields());
        newDemande.setxEcStatut(Enumeration.StatutDemande.PND.getCode());

        if (demande.getListMarchandises() != null && !demande.getListMarchandises().isEmpty()) {
            newListMarchandises = new ArrayList<>();

            for (EbMarchandise ebMarchandise: demande.getListMarchandises()) {
                unit = new EbMarchandise();
                unit.setEbMarchandiseNum(ebMarchandise.getEbMarchandiseNum()); // i
                                                                               // need
                                                                               // it
                                                                               // in
                                                                               // the
                                                                               // Tr
                                                                               // gen
                                                                               // algorithme
                                                                               // to
                                                                               // get
                                                                               // unit
                                                                               // rest
                                                                               // of
                                                                               // field,
                                                                               // set
                                                                               // to
                                                                               // null
                                                                               // when
                                                                               // saving
                unit.setUnitReference(ebMarchandise.getUnitReference());
                unit.setPartNumber(ebMarchandise.getPartNumber());
                unit.setSerialNumber(ebMarchandise.getSerialNumber());
                unit.setHsCode(ebMarchandise.getHsCode());
                unit.setNumberOfUnits(ebMarchandise.getNumberOfUnits());
                newListMarchandises.add(unit);
            }

        }

        newDemande.setListMarchandises(newListMarchandises);
        newDemande.setxEbCompagnie(new EbCompagnie(demande.getxEbCompagnie().getEbCompagnieNum()));
        newDemande.setxEbEtablissement(new EbEtablissement(demande.getxEbEtablissement().getEbEtablissementNum()));
        return newDemande;
    }

    public void processDocumentRules(Integer ebDemandeNum, Boolean isStatusUpdate, EbUser connectedUser) {
        EbDemande demande = ebDemandeRepository.getOne(ebDemandeNum);

        // processing control rules
        try {
            this
                .processControlRules(
                    demande.getxEbCompagnie().getEbCompagnieNum(),
                    null,
                    null,
                    null,
                    RuleCategory.DOCUMENT.getCode(),
                    null,
                    demande.getEbDemandeNum(),
                    false,
                    isStatusUpdate,
                    connectedUser);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public Map<String, Object> getDocRulesInputsData() throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();
        EbUser connectedUser = connectedUserService.getCurrentUser();
        Integer ebEtablissementNum = connectedUser.getEbEtablissement().getEbEtablissementNum();
        Integer ebCompagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        // getting categories
        List<EbCategorie> categories = this.categorieService.getAllEbCategorieWithLabelsNonDeleted(ebEtablissementNum);
        result.put("categories", categories);

        // getting flow types
        SearchCriteria criteria = new SearchCriteria();
        criteria.setEbCompagnieNum(ebCompagnieNum);
        List<EbTypeFlux> typesFlux = this.ebTypeFluxService.getListTypeFlux(criteria);
        result.put("typeFlux", typesFlux);

        // getting list incoterms
        List<EbIncoterm> incoterms = this.ebIncotermService.getListIncoterms(criteria);
        result.put("incoterms", incoterms);

        // getting transport modes
        List<ModeTransport> listModesTr = Enumeration.ModeTransport.getValues();
        result.put("modesTr", listModesTr);

        // getting all demadne statut, commenté temporairement
        // ObjectMapper mapper = new ObjectMapper();
        // ArrayNode dmdStatut = mapper
        // .readValue(
        // Enumeration.StatutDemande.getListStatutDemandePricing(),
        // new TypeReference<ArrayNode>()
        // {}
        // );
        List<StatutDemande> dmdStatut = StatutDemande.getListStatus();
        result.put("dmdStatut", dmdStatut);

        // getting request types
        criteria = new SearchCriteria();
        criteria.setIncludeGlobalValues(true);
        criteria.setEbCompagnieNum(ebCompagnieNum);
        criteria.setActivated(true);
        List<EbTypeRequest> listTypeRequests = typeRequestService.searchTypeRequest(criteria);
        result.put("requestTypes", listTypeRequests);

        // getting custom field
        EbCustomField cf = ebCustomFieldRepository.findFirstByXEbCompagnie(ebCompagnieNum);
        result.put("customField", cf);

        criteria = new SearchCriteria();
        criteria.setOrderedColumn("libelle");
        criteria.setShowGlobals(true);
        criteria.setAscendant(true);

        List<EbTemplateDocuments> listTemplateDocuments = templateDocumentsService.searchTemplateDocuments(criteria);
        List<EbTemplateDocumentsDTO> docGenTemplates = templateDocumentsService
            .convertEntitiesToDto(listTemplateDocuments);
        result.put("docGenTemplates", docGenTemplates);

        List<String> refAdresses = this.ebAdresseRepository.getAllAdrReferencesByCompagnieNum(ebCompagnieNum);
        result.put("refAdresses", refAdresses);

        criteria = new SearchCriteria();
        criteria.setEbCompagnieNum(ebCompagnieNum);
        criteria.setIncludeGlobalValues(true);
        criteria.setActivated(true);
        result.put("listSchemaPsl", daoEbTtCompanyPsl.getListSchemaPsl(criteria));

        // list of unit properties
        List<CrUnitFields> unitsFields = CrUnitFields.getValues();
        result.put("unitsFields", unitsFields);

        List<EcCountry> countries = serviceListStatique.getListEcCountry();
        result.put("countries", countries);

        // list type units
        SearchCriteriaTypeUnit criteriaTypeUnit = new SearchCriteriaTypeUnit();
        criteriaTypeUnit.setSearchterm("true");
        criteriaTypeUnit.setEbCompagnieNum(criteria.getEbCompagnieNum());
        List<EbTypeMarchandiseDto> resultList = mapper
            .ebTypeUnitsToEbTypesMarchandise(daoTypeUnit.getListTypeUnit(criteriaTypeUnit));
        result.put("listTypeUnit", resultList);

        // list type goods
        List<EbTypeGoods> listTypeGoods = typeGoodsService.listTypeGoods(new SearchCriteria());
        result.put("listTypeGoods", listTypeGoods);

        // getting DGR
        result.put("marchandiseDangerousGood", Enumeration.MarchandiseDangerousGood.getValues());

        // getting class goods
        result.put("marchandiseClassGood", Enumeration.MarchandiseClassGood);

        // getting list cost centers
        result.put("listCostCenter", costCenterService.getListCostCenterCompagnie(ebCompagnieNum));

        // getting connected user object (needed to load list mails alertes)
        EbUser user = userService.getEbUserByEmail(connectedUser.getEmail());
        result
            .put(
                "user",
                new EbUser(
                    user.getEbUserNum(),
                    user.getListParamsMail(),
                    user.getEbEtablissement().getEbEtablissementNum(),
                    user.getEbCompagnie().getEbCompagnieNum()));

        // getting list emails alertes
        List<EbParamsMail> listMails = serviceListStatique.getListEmailParam();
        result.put("listMails", listMails);

        return result;
    }

    @Transactional
    private void sendQuotations(List<EbDemande> listDemande, EbUser connectedUser) {
        if (listDemande == null || listDemande.isEmpty()) return;

        connectedUser = connectedUser == null ? connectedUserService.getCurrentUser() : connectedUser;
        SearchCriteria criteria = new SearchCriteria();
        criteria.setRoleBroker(false);
        criteria.setRoleTransporteur(true);
        criteria.setUserService(ServiceType.SERVICE_TRANSPORTEUR.getCode());
        criteria.setRoleControlTower(true);
        criteria.setForChargeur(true);
        criteria.setContact(true);

        for (EbDemande ebDemande: listDemande) {
            EbUser chargeur = ebDemande.getUser();
            List<ExEbDemandeTransporteur> foundQuotes = this.exEbDemandeTransporteurRepository
                .findAllByXEbDemande_ebDemandeNum(ebDemande.getEbDemandeNum());

            if (foundQuotes == null || foundQuotes.isEmpty()) {

                if (ebDemande.getEbPartyOrigin() != null && ebDemande.getEbPartyOrigin().getEbPartyNum() != null) {
                    ebDemande
                        .setEbPartyOrigin(
                            this.ebPartyRepository.findByEbPartyNum(ebDemande.getEbPartyOrigin().getEbPartyNum()));
                }

                if (ebDemande.getEbPartyDest() != null && ebDemande.getEbPartyDest().getEbPartyNum() != null) {
                    ebDemande
                        .setEbPartyDest(
                            this.ebPartyRepository.findByEbPartyNum(ebDemande.getEbPartyDest().getEbPartyNum()));
                }

                if (chargeur != null && chargeur.getEbUserNum() != null) {
                    criteria.setEbUserNum(chargeur.getEbUserNum());

                    if (ebDemande.getxEbCompagnie() != null
                        && ebDemande.getxEbCompagnie().getEbCompagnieNum() != null) criteria
                            .setEbCompagnieNum(ebDemande.getxEbCompagnie().getEbCompagnieNum());

                    if (ebDemande.getxEbEtablissement() != null
                        && ebDemande.getxEbEtablissement().getEbEtablissementNum() != null) criteria
                            .setEbEtablissementNum(ebDemande.getxEbEtablissement().getEbEtablissementNum());

                    criteria.setConnectedUserNum(connectedUser.getEbUserNum());
                    List<EbUser> listTransporteur = (List<EbUser>) this.daoUser.selectListContact(criteria);

                    List<ExEbDemandeTransporteur> listQuotes = new ArrayList<>();

                    if (listTransporteur != null && !listTransporteur.isEmpty()) {

                        for (EbUser transporteur: listTransporteur) {
                            ExEbDemandeTransporteur quote = this.createQuotationForUser(transporteur, ebDemande);
                            listQuotes.add(quote);
                        }

                        this.exEbDemandeTransporteurRepository.saveAll(listQuotes);
                    }

                }
                else throw new MyTowerException("Demande with no user !!");

            }

        }

    }

    private ExEbDemandeTransporteur createQuotationForUser(EbUser transporteur, EbDemande ebDemande) {
        if (transporteur == null || transporteur.getEbUserNum() == null) return null;
        ExEbDemandeTransporteur quote = new ExEbDemandeTransporteur();
        quote.setxTransporteur(new EbUser(transporteur.getEbUserNum()));

        if (transporteur.getEbEtablissement() != null) quote
            .setxEbEtablissement(new EbEtablissement(transporteur.getEbEtablissement().getEbEtablissementNum()));

        if (transporteur.getEbCompagnie() != null) quote
            .setxEbCompagnie(new EbCompagnie(transporteur.getEbCompagnie().getEbCompagnieNum()));

        quote.setStatus(Enumeration.StatutCarrier.RRE.getCode());

        quote.setxEbDemande(new EbDemande(ebDemande.getEbDemandeNum()));
        quote.setExchangeRateFound(transporteur.getExchangeRateFound());
        quote.setIsFromTransPlan(transporteur.getIsFromTransPlan());
        quote.setCityPickup(ebDemande.getEbPartyOrigin().getCity());
        quote.setxEcCountryPickup(ebDemande.getEbPartyOrigin().getxEcCountry());
        quote.setCityDelivery(ebDemande.getEbPartyDest().getCity());
        quote.setxEcCountryDelivery(ebDemande.getEbPartyDest().getxEcCountry());
        if (ebDemande.getxEbUserCt() != null) quote.setxEbUserCt(new EbUser(ebDemande.getxEbUserCt().getEbUserNum()));
        else quote.setxEbUserCt(new EbUser(ebDemande.getUser().getEbUserNum()));

        return quote;
    }

    @Transactional
    public boolean deleteControlRule(Integer ebControlRuleNum) {

        if (ebControlRuleNum != null) {
            ebControlRuleOperationRepository.deleteByXEbControlRule_ebControlRuleNum(ebControlRuleNum);

            this.ebControlRuleRepository.deleteByEbControlRuleNum(ebControlRuleNum);

            return true;
        }

        return false;
    }

    private void setExportControl(List<EbDemande> listDemande) {
        if (listDemande == null || listDemande.isEmpty()) return;

        List<Integer> demandeIds = listDemande.stream().map(dmd -> dmd.getEbDemandeNum()).collect(Collectors.toList());

        this.ebDemandeRepository.updateExportControlStatut(ExportControlEnum.ACE.getCode(), demandeIds);

        this.ebMarchandiseRepository.updateExportControlStatut(ExportControlEnum.ACE.getCode(), demandeIds);
    }

    public boolean applyRulesWithExportControlEnabled(EbDemande demande) throws Exception {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        Integer ebCompagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

        demande = initDemandeFields(demande);

        List<EbControlRule> rulesWithExportControlEnabled = getDocumentRulesWithExportControlEnabled(ebCompagnieNum);

        boolean ruleIsSuccessfullyVerified = false;

        for (EbControlRule rule: rulesWithExportControlEnabled) {
            ruleIsSuccessfullyVerified = this.executeDocumentRuleConditions(demande, rule);
            if (ruleIsSuccessfullyVerified) return true;
        }

        return false;
    }

    private List<EbControlRule> getDocumentRulesWithExportControlEnabled(Integer ebCompagnieNum) {
        List<EbControlRule> listControlRule = getListControlRule(ebCompagnieNum, RuleCategory.DOCUMENT.getCode());

        List<EbControlRule> rulesWithExportControlEnabled = listControlRule
            .stream().filter(r -> r.getExportControl() != null && r.getExportControl()).collect(Collectors.toList());
        return rulesWithExportControlEnabled;
    }

    private EbDemande initDemandeFields(EbDemande demande) {
        if (demande.getCustomFields() != null) demande.setCustomFields(demande.getCustomFields());

        if (demande.getListCategories() != null && !demande.getListCategories().isEmpty()) {
            demande
                .setListCategories(
                    demande
                        .getListCategories().stream().filter(c -> c.getLabels() != null && !c.getLabels().isEmpty())
                        .collect(Collectors.toList()));
        }

        return demande;
    }

    private boolean executeDocumentRuleConditions(EbDemande demande, EbControlRule rule) throws Exception {
        List<EbControlRuleOperation> listCondition = null, listOpr = rule.getListRuleOperations();
        boolean globalConditionResult = false;

        if (listOpr != null && !listOpr.isEmpty()) {
            listCondition = listOpr
                .stream()
                .filter(
                    it -> (it.getIsOperation() == null || !it.getIsOperation()) && it.getOperationType() != null
                        && (it.getOperationType() == ControlRuleOperationTypesEnum.IS_FILE_CONDITION.getCode()))
                .collect(Collectors.toList());
        }

        if (listCondition != null && !listCondition.isEmpty()) {
            boolean isFirst = true;

            for (EbControlRuleOperation condition: listCondition) {
                boolean singleConditionResult = false;

                try {
                    singleConditionResult = this.executeSingleCondition(demande, condition);
                } catch (Exception e) {
                    singleConditionResult = false;
                    e.printStackTrace();
                }

                if (isFirst) globalConditionResult = singleConditionResult;
                else if (condition
                    .getRuleOperator() == null) throw new MyTowerException("Rule operator is not provided");
                else {
                    if (RuleOperator.AND
                        .getCode().equals(condition.getRuleOperator())) globalConditionResult = globalConditionResult
                            && singleConditionResult;
                    else globalConditionResult = globalConditionResult || singleConditionResult;
                }
                isFirst = false;
            }

        }

        return globalConditionResult;
    }

    private boolean executeSingleCondition(EbDemande demande, EbControlRuleOperation condition) throws Exception {
        String leftOpStr = condition.getLeftOperands();
        String rightOpStr = condition.getRightOperands();
        Integer operator = condition.getOperator();

        if (leftOpStr == null) throw new MyTowerException("Left operands not provided");
        if (rightOpStr == null) throw new MyTowerException("Right operands not provided");
        if (operator == null) throw new MyTowerException("Operator not provided");

        RuleOperandType leftOp = RuleOperandType.getRuleOperandTypeEnumByKey(Integer.valueOf(leftOpStr));

        boolean conditionResult = false;

        if (leftOp.getCode().equals(RuleOperandType.UNITS.getCode())) {
            Integer leftUnitFieldCode = condition.getLeftUnitFieldCode();
            if (leftUnitFieldCode == null) throw new MyTowerException("No unit field found !!!");

            CrUnitFields unitField = CrUnitFields.getUnitInfoByCode(leftUnitFieldCode);

            conditionResult = this.checkUnitFieldValue(demande.getListMarchandises(), unitField, rightOpStr, operator);
        }

        if (leftOp.getCode().equals(RuleOperandType.STATUT_DOSSIER.getCode())) {
            conditionResult = this.compareIntValues(demande.getxEcStatut(), Integer.valueOf(rightOpStr), operator);
        }

        if (leftOp.getCode().equals(RuleOperandType.MODE_DE_TRANSPORT.getCode())) {
            conditionResult = this
                .compareIntValues(demande.getxEcModeTransport(), Integer.valueOf(rightOpStr), operator);
        }

        if (leftOp.getCode().equals(RuleOperandType.FLOW_TYPE.getCode())) {
            conditionResult = this.compareIntValues(demande.getxEbTypeFluxNum(), Integer.valueOf(rightOpStr), operator);
        }

        if (leftOp.getCode().equals(RuleOperandType.TYPE_OF_REQUEST.getCode())) {
            conditionResult = this.compareIntValues(demande.getxEbTypeRequest(), Integer.valueOf(rightOpStr), operator);
        }

        if (leftOp.getCode().equals(RuleOperandType.LEG.getCode())) {
            conditionResult = this.compareStrValues(demande.getScenario(), rightOpStr, operator);
        }

        if (leftOp.getCode().equals(RuleOperandType.REF_ADR_ORG.getCode())) {

            if (demande.getEbPartyOrigin() != null) {
                conditionResult = this
                    .compareStrValues(demande.getEbPartyOrigin().getReference(), rightOpStr, operator);
            }

        }

        if (leftOp.getCode().equals(RuleOperandType.REF_ADR_DEST.getCode())) {

            if (demande.getEbPartyDest() != null) {
                conditionResult = this.compareStrValues(demande.getEbPartyDest().getReference(), rightOpStr, operator);
            }

        }

        if (leftOp.getCode().equals(RuleOperandType.PSL_SCHEMA.getCode())) {

            if (demande.getxEbSchemaPsl() != null) {
                conditionResult = this
                    .compareIntValues(
                        demande.getxEbSchemaPsl().getEbTtSchemaPslNum(),
                        Integer.valueOf(rightOpStr),
                        operator);
            }

        }

        if (leftOp.getCode().equals(RuleOperandType.PAYS_ORIGIN.getCode())) {

            if (demande.getEbPartyOrigin() != null && demande.getEbPartyOrigin().getxEcCountry() != null) {
                conditionResult = this
                    .compareStrValues(demande.getEbPartyOrigin().getxEcCountry().getLibelle(), rightOpStr, operator);
            }

        }

        if (leftOp.getCode().equals(RuleOperandType.DESTINATION_COUNTRY.getCode())) {

            if (demande.getEbPartyDest() != null && demande.getEbPartyDest().getxEcCountry() != null) {
                conditionResult = this
                    .compareStrValues(demande.getEbPartyDest().getxEcCountry().getLibelle(), rightOpStr, operator);
            }

        }

        if (leftOp.getType().equals(CrFieldTypes.CUSTOMFIELD.getCode())) {
            conditionResult = this.compareCustomField(condition, rightOpStr, operator, demande);
        }

        if (leftOp.getType().equals(CrFieldTypes.CATEGORIE.getCode())) {
            conditionResult = this.compareCategorie(condition, demande, Integer.valueOf(rightOpStr), operator);
        }

        return conditionResult;
    }

    private boolean
        checkUnitFieldValue(List<EbMarchandise> units, CrUnitFields unitField, String condValue, Integer operator) {
        if (units == null || units.isEmpty()) return false;

        PropertyAccessor unitClassAccessor = null;

        for (EbMarchandise unit: units) {
            unitClassAccessor = PropertyAccessorFactory.forBeanPropertyAccess(unit); // getting spring bean accessor
            Object fieldValueAsObj = unitClassAccessor.getPropertyValue(unitField.getFieldName()); // getting field
                                                                                                   // value
            String fieldValue = fieldValueAsObj != null ? fieldValueAsObj.toString() : null;
            if (this.compareStrValues(fieldValue, condValue, operator)) return true;
        }

        return false;
    }

    private boolean compareStrValues(String fieldValue, String condValue, Integer operator) {
        if (fieldValue == null) return false;

        if (Operator.BEGINS_WITH.getCode().equals(operator)) {
            return fieldValue.startsWith(condValue);
        }
        else if (Operator.IS_IN.getCode().equals(operator)) {
            return condValue.contains(fieldValue);
        }
        else if (Operator.IS_NOT_IN.getCode().equals(operator)) {
            return !condValue.contains(fieldValue);
        }

        return false;
    }

    private boolean compareIntValues(Integer fieldValue, Integer condValue, Integer operator) {
        if (fieldValue == null) return false;

        if (Operator.IS_IN.getCode().equals(operator)) {
            return fieldValue.equals(condValue);
        }
        else if (Operator.IS_NOT_IN.getCode().equals(operator)) {
            return !fieldValue.equals(condValue);
        }

        return false;
    }

    private boolean
        compareCustomField(EbControlRuleOperation condition, String condValue, Integer operator, EbDemande demande)
            throws Exception {
        if (demande.getListCustomsFields() == null || demande.getListCustomsFields().isEmpty()) return false;

        Integer ebLeftCustomField = condition.getEbLeftCustomFieldNum();
        if (ebLeftCustomField == null) throw new MyTowerException("No Left CustomField was defined for action!!!");

        CustomFields cf = demande
            .getListCustomsFields().stream().filter(f -> f.getNum().equals(ebLeftCustomField)).findFirst().orElse(null);

        if (cf != null) {
            return this.compareStrValues(cf.getValue(), condValue, operator);
        }

        return false;
    }

    private boolean
        compareCategorie(EbControlRuleOperation condition, EbDemande demande, Integer condLabelNum, Integer operator)
            throws Exception {
        if (demande.getListCategories() == null || demande.getListCategories().isEmpty()) return false;

        Integer ebCategorieNum = condition.getEbCategorieNum();
        if (ebCategorieNum == null) throw new MyTowerException("No Categorie was defined for condition !!!");

        EbCategorie categorie = demande
            .getListCategories().stream().filter(c -> c.getEbCategorieNum().equals(ebCategorieNum)).findFirst()
            .orElse(null);

        if (categorie != null && categorie.getEbCategorieNum() != null) {
            List<EbLabel> labels = new ArrayList<EbLabel>(categorie.getLabels());

            if (labels != null && !labels.isEmpty()) {
                EbLabel selectedLabel = labels.get(0);
                if (selectedLabel.getEbLabelNum() != null) return this
                    .compareIntValues(selectedLabel.getEbLabelNum(), condLabelNum, operator);
            }

        }

        return false;
    }

    private void changeStatusDemandes(EbControlRule rule, List<EbDemande> demandes) {
        List<Integer> demandeIds = demandes.stream().map(d -> d.getEbDemandeNum()).collect(Collectors.toList());
        this.ebDemandeRepository.updateStatusInListDemande(rule.getDestStatus(), demandeIds);
    }
}
