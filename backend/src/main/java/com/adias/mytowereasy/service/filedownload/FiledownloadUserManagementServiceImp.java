package com.adias.mytowereasy.service.filedownload;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dao.DaoUser;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.FileDownLoadStatus;
import com.adias.mytowereasy.model.Enumeration.Role;
import com.adias.mytowereasy.model.Enumeration.UserStatus;
import com.adias.mytowereasy.model.FileDownloadStateAndResult;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class FiledownloadUserManagementServiceImp implements FiledownloadUserManagementService {
    @Autowired
    DaoUser daoUser;

    @Override
    public FileDownloadStateAndResult listFileDownLoadUserManagement(SearchCriteria userCriteria) throws Exception {
        FileDownloadStateAndResult resultAndState = new FileDownloadStateAndResult();
        List<Map<String, String[]>> listresult = new ArrayList<Map<String, String[]>>();
        userCriteria.setSize(null);
        List<EbUser> listUser = daoUser.selectListEbUser(userCriteria);

        for (EbUser user: listUser) {
            Map<String, String[]> map = new LinkedHashMap<String, String[]>();
            map.put("nomPrenom", new String[]{
                "Nom", user.getNom().concat(" ").concat(user.getPrenom())
            });
            map.put("email", new String[]{
                "Email", user.getEmail()
            });
            map.put("username", new String[]{
                "Username", user.getUsername()
            });
            map.put("groupes", new String[]{
                "Groupes", user.getGroupes()
            });
            map.put("telephone", new String[]{
                "Telephone", user.getTelephone()
            });
            map.put("ebUserProfile", new String[]{
                "Profile", user.getTelephone()
            });
            map.put("compagnie", new String[]{
                "Compagnie", user.getEbCompagnie().getNom()
            });
            map.put("ebEtablissement", new String[]{
                "Etablissement", user.getEbEtablissement().getNom()
            });
            map.put("role", new String[]{
                "Role", Role.getLibelleByCode(user.getRole())
            });
            map.put("status", new String[]{
                "Status", UserStatus.getLibelleByCode(user.getStatus())
            });
            map.put("isAdmin", new String[]{
                "Admin", user.isAdmin() ? "Oui" : "Non"
            });
            map.put("superAdmin", new String[]{
                "Super Admin", user.isSuperAdmin() ? "Oui" : "Non"
            });
            listresult.add(map);
        }

        resultAndState.setState(FileDownLoadStatus.ENCOURS.getCode());
        resultAndState.setListResult(listresult);
        return resultAndState;
    }
}
