/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.custom;

import java.util.List;

import com.adias.mytowereasy.model.custom.EbCustomDeclaration;
import com.adias.mytowereasy.utils.search.SearchCriteriaCustom;


public interface DaoDeclaration {
    public List<EbCustomDeclaration> getListEbDeclaration(SearchCriteriaCustom criteria);

    public Long getCountListEbDeclaration(SearchCriteriaCustom criteria);

    List<EbCustomDeclaration> getListClearanceId(String term);

    EbCustomDeclaration getEbDeclaration(SearchCriteriaCustom criteria);
}
