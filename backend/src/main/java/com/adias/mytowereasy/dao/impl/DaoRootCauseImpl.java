package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoRootCause;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.qm.EbQmRootCause;
import com.adias.mytowereasy.model.qm.QEbQmRootCause;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


@Component
@Transactional
public class DaoRootCauseImpl implements DaoRootCause {
    @PersistenceContext
    EntityManager em;

    @Autowired
    private ConnectedUserService connectedUserService;

    QEbQmRootCause qEbQmRootCause = QEbQmRootCause.ebQmRootCause;

    QEbCompagnie qEbCompagnie = QEbCompagnie.ebCompagnie;

    private Map<String, OrderSpecifier<?>> orderMapASC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, OrderSpecifier<?>> orderMapDESC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, Path<?>> orderMap = new HashMap<String, Path<?>>();

    private void Initialize() {
        orderMapASC.clear();

        orderMap.put("label", qEbQmRootCause.label);
        orderMapASC.put("label", qEbQmRootCause.label.asc());
        orderMapDESC.put("label", qEbQmRootCause.label.desc());

        orderMap.put("name", qEbQmRootCause.name);
        orderMapASC.put("name", qEbQmRootCause.name.asc());
        orderMapDESC.put("name", qEbQmRootCause.name.desc());

        orderMap.put("ref", qEbQmRootCause.ref);
        orderMapASC.put("ref", qEbQmRootCause.ref.asc());
        orderMapDESC.put("ref", qEbQmRootCause.ref.desc());

        orderMap.put("groupe", qEbQmRootCause.groupe);
        orderMapASC.put("groupe", qEbQmRootCause.groupe.asc());
        orderMapDESC.put("groupe", qEbQmRootCause.groupe.desc());

        orderMap.put("relatedIncident", qEbQmRootCause.relatedIncident);
        orderMapASC.put("relatedIncident", qEbQmRootCause.relatedIncident.asc());
        orderMapDESC.put("relatedIncident", qEbQmRootCause.relatedIncident.desc());

        orderMap.put("perimetreImpacte", qEbQmRootCause.perimetreImpacte);
        orderMapASC.put("perimetreImpacte", qEbQmRootCause.perimetreImpacte.asc());
        orderMapDESC.put("perimetreImpacte", qEbQmRootCause.perimetreImpacte.desc());

        orderMap.put("montantCumuleCIL", qEbQmRootCause.montantCumuleCIL);
        orderMapASC.put("montantCumuleCIL", qEbQmRootCause.montantCumuleCIL.asc());
        orderMapDESC.put("montantCumuleCIL", qEbQmRootCause.montantCumuleCIL.desc());

        orderMap.put("userImpacter", qEbQmRootCause.userImpacter);
        orderMapASC.put("userImpacter", qEbQmRootCause.userImpacter.asc());
        orderMapDESC.put("userImpacter", qEbQmRootCause.userImpacter.desc());

        orderMap.put("severite", qEbQmRootCause.severite);
        orderMapASC.put("severite", qEbQmRootCause.severite.asc());
        orderMapDESC.put("severite", qEbQmRootCause.severite.desc());

        orderMap.put("relatedActions", qEbQmRootCause.relatedActions);
        orderMapASC.put("relatedActions", qEbQmRootCause.relatedActions.asc());
        orderMapDESC.put("relatedActions", qEbQmRootCause.relatedActions.desc());

        orderMap.put("createur", qEbQmRootCause.createur);
        orderMapASC.put("createur", qEbQmRootCause.createur.asc());
        orderMapDESC.put("createur", qEbQmRootCause.createur.desc());

        orderMap.put("relatedIncident", qEbQmRootCause.relatedIncident);
        orderMapASC.put("relatedIncident", qEbQmRootCause.relatedIncident.asc());
        orderMapDESC.put("relatedIncident", qEbQmRootCause.relatedIncident.desc());

        orderMap.put("statut", qEbQmRootCause.statut);
        orderMapASC.put("statut", qEbQmRootCause.statut.asc());
        orderMapDESC.put("statut", qEbQmRootCause.statut.desc());

        orderMap.put("dateCreationSys", qEbQmRootCause.dateCreationSys);
        orderMapASC.put("dateCreationSys", qEbQmRootCause.dateCreationSys.asc());
        orderMapDESC.put("dateCreationSys", qEbQmRootCause.dateCreationSys.desc());
    }

    @Override
    public List<EbQmRootCause> getListEbQmRootCause(SearchCriteriaQM criterias) {
        List<EbQmRootCause> results = new ArrayList<EbQmRootCause>();
        BooleanBuilder where = new BooleanBuilder();
        Initialize();

        try {
            JPAQuery<EbQmRootCause> query = new JPAQuery<EbQmRootCause>(em);
            query.distinct();
            query
                .select(
                    QEbQmRootCause
                        .create(
                            qEbQmRootCause.qmRootCauseNum,
                            qEbQmRootCause.label,
                            qEbQmRootCause.name,
                            qEbQmRootCause.ref,
                            qEbQmRootCause.groupe,
                            qEbQmRootCause.relatedIncident,
                            qEbQmRootCause.perimetreImpacte,
                            qEbQmRootCause.userImpacter,
                            qEbQmRootCause.severite,
                            qEbQmRootCause.relatedActions,
                            qEbQmRootCause.createur,
                            qEbQmRootCause.montantCumuleCIL,
                            qEbQmRootCause.statut,
                            qEbQmRootCause.responsable,
                            qEbQmRootCause.description,
                            qEbQmRootCause.dateCreationSys,
                            qEbQmRootCause.eventType,
                            qEbQmRootCause.listIncidentNum,
                            qEbQmRootCause.actions,
                            qEbQmRootCause.rootCauseCategory));
            query.from(qEbQmRootCause);
            query.leftJoin(qEbQmRootCause.etablissement().ebCompagnie(), qEbCompagnie);
            query = getGlobalWhere(query, where, criterias);

            if (criterias.getStart() != null) {
                query.offset(criterias.getStart());
            }

            if (criterias.getSize() != null && criterias.getSize() >= 0) {
                query.limit(criterias.getSize());
            }

            if (criterias.getPageNumber() != null) {
                query.offset(criterias.getPageNumber() * (criterias.getSize() != null ? criterias.getSize() : 1));
            }

            if (criterias.getOrderedColumn() != null) {

                if (criterias.getAscendant()) {
                    query.orderBy(this.orderMapASC.get(criterias.getOrderedColumn()));
                }
                else {
                    query.orderBy(this.orderMapDESC.get(criterias.getOrderedColumn()));
                }

            }
            else {
                query.orderBy(this.orderMapDESC.get("label"));
            }

            query.where(where);
            results = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteriaQM criterias) throws Exception {
        if (criterias.getEbCompagnieNum() != null) where
            .and(qEbCompagnie.ebCompagnieNum.eq(criterias.getEbCompagnieNum()));

        if (criterias.getEventType() != null) where.and(qEbQmRootCause.eventType.eq(criterias.getEventType()));

        return query;
    }

    @Override
    public Long listCount(SearchCriteriaQM criterias) {
        Long result = new Long(0);
        BooleanBuilder where = new BooleanBuilder();

        Initialize();

        try {
            JPAQuery<EbQmRootCause> query = new JPAQuery<EbQmRootCause>(em);
            query.distinct();
            query = getGlobalWhere(query, where, criterias);
            query.from(qEbQmRootCause);

            query.leftJoin(qEbQmRootCause.etablissement().ebCompagnie(), qEbCompagnie);

            query.where(where);
            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
