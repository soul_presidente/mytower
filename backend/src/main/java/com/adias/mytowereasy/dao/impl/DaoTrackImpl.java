/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import java.text.SimpleDateFormat;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoTrack;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.TransitStatus;
import com.adias.mytowereasy.model.Enumeration.TransportConfirmation;
import com.adias.mytowereasy.model.PricingBookingEnumeration.DemandeStatus;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.model.tt.QEbTtTracing;
import com.adias.mytowereasy.repository.EbTypeTransportRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria.OrderCriterias;
import com.adias.mytowereasy.utils.search.SearchCriteriaTrackTrace;


@Component
@Transactional
public class DaoTrackImpl extends Dao implements DaoTrack {
    @PersistenceContext
    EntityManager em;

    @Autowired
    private EbUserRepository ebUserRepository;

    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    EbTypeTransportRepository ebTypeTransportRepository;

    QEbTtTracing qEbTrackTrace = QEbTtTracing.ebTtTracing;
    QEbMarchandise qEbMarchandise = QEbMarchandise.ebMarchandise;
    QEbDemande qEbDemande = QEbDemande.ebDemande;
    QEbCompagnie qEbCompagnieChargeur = new QEbCompagnie("qEbCompagnieChargeur");
    QEbEtablissement qEbEtablissementChargeur = new QEbEtablissement("qEbEtablissementChargeur");

    QEbEtablissement qEtablissementQuote = new QEbEtablissement("qEtablissementQuote");
    QEbEtablissement qEtabQuoteCt = new QEbEtablissement("qEtabQuoteCt");
    QEbUser qEbChargeur = new QEbUser("qEbChargeur");
    QEcCountry qEcCountryOrigin = new QEcCountry("qEcCountryOrigin");
    QEcCountry qEcCountryDestination = new QEcCountry("qEcCountryDestination");

    QEbUser qUserOrigin = new QEbUser("qUserOrigin");
    QEbEtablissement qUserOriginEtab = new QEbEtablissement("qUserOriginEtab");

    QEbCompagnie qCompagnie = new QEbCompagnie("qCompagnie");
    QEbUser qUserDest = new QEbUser("qUserDest");
    QEbEtablissement qUserDestEtab = new QEbEtablissement("qUserDestEtab");
    QEbUser qUserOriginCB = new QEbUser("qUserOriginCB");
    QEbEtablissement qUserOriginCBEtab = new QEbEtablissement("qUserOriginCBEtab");
    QEbUser qUserDestCB = new QEbUser("qUserDestCB");
    QEbEtablissement qUserDestCBEtab = new QEbEtablissement("qUserDestCBEtab");
    QEbCompagnie qUserOriginCBComp = new QEbCompagnie("qUserDestCBComp");
    QEbCompagnie qUserDestCBComp = new QEbCompagnie("qUserDestCBComp");
    QEcCurrency qEcCurrency = new QEcCurrency("qEcCurrency");
    QEbCostCenter qEbCostCenter = new QEbCostCenter("qEbCostCenter");
    QEbUser qEbUserCt = new QEbUser("qEbUserCt");
    QEbUser qEbuserOwner = new QEbUser("qEbUserOwner");

    QEbParty qPartyOrigin = new QEbParty("qEbPartyOrigin");

    QEbParty qPartyDest = new QEbParty("partyDest");

    private static Map<String, Object> orderHashMap = new HashMap<>();

    private void Initialiaze() {
        orderHashMap.put("ebTtTracingNum", qEbTrackTrace.ebTtTracingNum);
        orderHashMap.put("refTransport", qEbTrackTrace.refTransport);
        orderHashMap.put("customerReference", qEbTrackTrace.customerReference);
        orderHashMap.put("xEcCountryOrigin", qEcCountryOrigin.libelle);
        orderHashMap.put("xEcCountryDestination", qEcCountryDestination.libelle);
        orderHashMap.put("datePickup", qEbTrackTrace.datePickup);
        orderHashMap.put("nomEtablissementTransporteur", qEbTrackTrace.nomEtablissementTransporteur);
        orderHashMap.put("totalWeight", qEbTrackTrace.totalWeight);
        orderHashMap.put("dateModification", qEbTrackTrace.dateModification);
        orderHashMap.put("late", qEbTrackTrace.late);
        orderHashMap.put("codeAlphaPslCourant", qEbTrackTrace.codeAlphaPslCourant);
        orderHashMap.put("configPsl", qEbTrackTrace.configPsl);
        orderHashMap.put("customRef", qEbDemande.customerReference);
        orderHashMap.put("dateCreation", qEbDemande.dateCreation);
        orderHashMap.put("xEbDemande.numAwbBol", qEbDemande.numAwbBol);
        orderHashMap.put("xEbDemande.carrierUniqRefNum", qEbDemande.carrierUniqRefNum);
        orderHashMap.put("libellePslCourant", qEbTrackTrace.libellePslCourant);
        orderHashMap.put("xEcModeTransport", qEbDemande.xEcModeTransport);
        orderHashMap.put("libelleOriginCity", qEbTrackTrace.libelleOriginCity);
        orderHashMap.put("libelleDestCity", qEbTrackTrace.libelleDestCity);
        orderHashMap.put("libelleLastPsl", qEbDemande.libelleLastPsl);
        orderHashMap.put("dateLastPsl", qEbDemande.dateLastPsl);
        orderHashMap.put("numAwbBol", qEbDemande.numAwbBol);
        orderHashMap.put("companyOrigin", qPartyOrigin.company);
        orderHashMap.put("carrierUniqRefNum", qEbDemande.carrierUniqRefNum);
    }

    @Override
    public EbTtTracing getEbTrackTrace(SearchCriteriaTrackTrace criteria) {
        EbTtTracing resultat = null;
        Initialiaze();
        BooleanBuilder where = new BooleanBuilder();

        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);

        try {
            JPAQuery<EbTtTracing> query = new JPAQuery<EbTtTracing>(em);

            query
                .select(
                    QEbTtTracing
                        .create(
                            qEbTrackTrace.ebTtTracingNum,
                            qEbTrackTrace.configPsl,
                            qEbTrackTrace.refTransport,
                            qEbTrackTrace.customerReference,
                            qEcCountryOrigin.ecCountryNum,
                            qEcCountryOrigin.libelle,
                            qEcCountryDestination.ecCountryNum,
                            qEcCountryDestination.libelle,
                            qEbTrackTrace.datePickup,
                            qEbTrackTrace.nomEtablissementTransporteur,
                            qEbTrackTrace.totalWeight,
                            qEbTrackTrace.dateModification,
                            qEbTrackTrace.late,
                            qEbTrackTrace.leadtime,
                            qEbTrackTrace.nomEtablissementChargeur,
                            qEbCompagnieChargeur.nom,
                            qEbCompagnieChargeur.ebCompagnieNum,
                            qEbChargeur.nom,
                            qEbChargeur.prenom,
                            qEbChargeur.ebUserNum,
                            qEbTrackTrace.xEbMarchandise().ebMarchandiseNum,
                            qEbTrackTrace.xEbTransporteur().nom,
                            qEbTrackTrace.xEbTransporteur().prenom,
                            qEbTrackTrace.xEbTransporteur().ebUserNum,
                            qEbTrackTrace.xEbCompagnieTransporteur().nom,
                            qEbTrackTrace.xEbCompagnieTransporteur().ebCompagnieNum,
                            qEbDemande.ebDemandeNum,
                            qEbTrackTrace.dateCreation,
                            qEbDemande.xEcStatut,
                            qEbDemande.xEcTypeDemande,
                            qEbDemande.xEbTypeTransport,
                            qEbDemande.xEcModeTransport,
                            qEbTrackTrace.originPlace,
                            qEbTrackTrace.destinationPlace,
                            qEbDemande.customerReference,
                            qEbTrackTrace.xEbEtablissementTransporteur().ebEtablissementNum,
                            qEbDemande.numAwbBol,
                            qEbTrackTrace.listFlag,
                            qEbDemande.xEbSchemaPsl(),

                            qEbDemande.totalNbrParcel,
                            qEbDemande.totalVolume,
                            qEbDemande.totalWeight,
                            qEbDemande.numAwbBol,
                            qEbDemande.mawb,
                            qEbDemande.etd,
                            qEbDemande.eta,
                            qEbDemande.xEcIncotermLibelle,

                            qEbDemande.libelleOriginCountry,
                            qEbDemande.libelleDestCountry,
                            qEbDemande.listCategories,

                            qEcCurrency,
                            qEbCostCenter,
                            qEbTrackTrace.codeAlphaPslCourant,

                            qEbDemande.dateOfGoodsAvailability,
                            qEbDemande.typeRequestLibelle,
                            qEbDemande.xEbTypeRequest,
                            qEbuserOwner.ebUserNum,
                            qCompagnie.ebCompagnieNum,

                            qPartyOrigin.city,
                            qPartyDest.city,
                            qEbDemande.refTransport));

            query = getGlobalWhere(query, where, criteria, connectedUser);
            where.and(qEbTrackTrace.ebTtTracingNum.eq(criteria.getEbTtTracingNum()));

            query.leftJoin(qEbTrackTrace.listEbPslApp);

            query.distinct();
            query.from(qEbTrackTrace).where(where);

            resultat = query.fetchFirst();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public List<EbTtTracing> getListEbTrackTrace(SearchCriteriaTrackTrace criteria, EbUser connectedUser) {
        List<EbTtTracing> resultat = new ArrayList<EbTtTracing>();
        Initialiaze();
        BooleanBuilder where = new BooleanBuilder();
        StringExpression stringOrderWithExpression = qEbDemande.refTransport;
        NumberExpression<Integer> numberOrderWithExpression = qEbDemande.xEcStatut;
        Path<String> alias = ExpressionUtils.path(String.class, "stringOrderWithExpression");
        Path<Integer> numberOrderAlias = ExpressionUtils.path(Integer.class, "numberOrderExpression");

        try {
            JPAQuery<EbTtTracing> query = new JPAQuery<EbTtTracing>(em);

            if (criteria.getOrder() != null && criteria.getOrder().size() > 0) {
                String columnName = criteria.getOrder().get(0).get(OrderCriterias.columnName);
                String fieldType = null;
                StringExpression fieldColumn = null;
                NumberExpression indexOfCategExpr = null;

                if (columnName != null && columnName.matches("^category[0-9]+$")) {
                    fieldType = "category";
                    fieldColumn = qEbDemande.listCategoryFlat;
                    columnName = columnName.substring(fieldType.length());
                    indexOfCategExpr = fieldColumn.indexOf(columnName + ":").add(columnName.length() + 2 + 1);
                }
                else if (columnName != null && columnName.matches("^field[0-9]+$")) {
                    fieldType = "field";
                    fieldColumn = qEbDemande.listCustomFieldsFlat;
                    indexOfCategExpr = fieldColumn.indexOf(columnName + "\":").add(columnName.length() + 2 + 2);
                }

                if (columnName != null && fieldType != null && fieldColumn != null) {
                    StringExpression restStringExpr = fieldColumn.substring(indexOfCategExpr);
                    NumberExpression endIndexExpr = restStringExpr.indexOf("\"");

                    /*
                     * quand on écrie dans querydsl: substring(a, b) cela se
                     * traduit par substring(a+1, b-a) dans sql
                     * donc on doit diminuer 1 de l'expr de gauche et on ajoute
                     * 'a' dans l'expr de droite
                     */
                    stringOrderWithExpression = new CaseBuilder()
                        .when(
                            new BooleanBuilder()
                                .orAllOf(fieldColumn.isNotNull(), fieldColumn.indexOf(columnName).goe(0)))
                        .then(
                            fieldColumn
                                .substring(
                                    indexOfCategExpr.subtract(1), // diminuer 1
                                    endIndexExpr.add(indexOfCategExpr) // rajouter
                                                                       // l'expr
                                                                       // de
                                                                       // gauche
                                                                       // 'a'
                                )).otherwise(Expressions.nullExpression());

                    stringOrderWithExpression = new CaseBuilder()
                        .when(
                            new BooleanBuilder()
                                .orAllOf(stringOrderWithExpression.isNotNull(), stringOrderWithExpression.isNotEmpty()))
                        .then(stringOrderWithExpression).otherwise(Expressions.nullExpression());
                }
                else if (columnName != null && columnName.contentEquals("xEcModeTransport")) {
                    numberOrderWithExpression = new CaseBuilder()
                        .when(
                            new BooleanBuilder()
                                .orAllOf(
                                    qEbDemande.xEcModeTransport.isNotNull(),
                                    qEbDemande.xEcModeTransport.eq(Enumeration.ModeTransport.AIR.getCode())))
                        .then(Expressions.ONE)
                        .when(
                            new BooleanBuilder()
                                .orAllOf(
                                    qEbDemande.xEcModeTransport.isNotNull(),
                                    qEbDemande.xEcModeTransport.eq(Enumeration.ModeTransport.INTEGRATOR.getCode())))
                        .then(Expressions.TWO)
                        .when(
                            new BooleanBuilder()
                                .orAllOf(
                                    qEbDemande.xEcModeTransport.isNotNull(),
                                    qEbDemande.xEcModeTransport.eq(Enumeration.ModeTransport.ROAD.getCode())))
                        .then(Expressions.THREE)
                        .when(
                            new BooleanBuilder()
                                .orAllOf(
                                    qEbDemande.xEcModeTransport.isNotNull(),
                                    qEbDemande.xEcModeTransport.eq(Enumeration.ModeTransport.SEA.getCode())))
                        .then(Expressions.FOUR).otherwise(Expressions.nullExpression(Integer.class));
                }

            }

            query
                .select(
                    QEbTtTracing
                        .create(
                            qEbTrackTrace.ebTtTracingNum,
                            qEbTrackTrace.refTransport,
                            qEbTrackTrace.customerReference,
                            qEbTrackTrace.partNumber,
                            qEbTrackTrace.serialNumber,
                            qEbTrackTrace.shippingType,
                            qEbTrackTrace.orderNumber,
                            qEcCountryOrigin.libelle,
                            qEcCountryDestination.libelle,
                            qEbTrackTrace.datePickup,
                            qEbTrackTrace.nomEtablissementTransporteur,
                            qEbTrackTrace.totalWeight,
                            qEbTrackTrace.dateModification,
                            qEbTrackTrace.late,
                            qEbTrackTrace.configPsl,
                            qEbDemande.xEcModeTransport,
                            qEbDemande.ebDemandeNum,
                            qEbDemande.customFields,
                            qEbDemande.listCategories,
                            qEbChargeur.ebUserNum,
                            qEbCompagnieChargeur.ebCompagnieNum,
                            qEbDemande.customerReference,
                            qEbDemande.dateCreation,
                            qEbDemande.numAwbBol,
                            qEbDemande.carrierUniqRefNum,
                            stringOrderWithExpression.as("stringOrderWithExpression"),
                            numberOrderWithExpression.as("numberOrderExpression"),
                            qEbTrackTrace.libelleOriginCity,
                            qEbTrackTrace.libelleDestCity,
                            qEbTrackTrace.xEcIncotermLibelle,
                            qEbCompagnieChargeur.nom,
                            qEbTrackTrace.libellePslCourant,
                            qEbDemande.libelleLastPsl,
                            qEbDemande.codeAlphaLastPsl,
                            qEbTrackTrace.dateLastPsl,
                            qEbTrackTrace.listFlag,
                            qEbDemande.xEbSchemaPsl(),
                            qEbTrackTrace.codeAlphaPslCourant,
                            qEbDemande.datePickupTM,
                            qEbDemande.finalDelivery,
                            qEbChargeur.nom,
                            qEbChargeur.prenom,
                            qEbUserCt.ebUserNum,
                            qEbUserCt.nom,
                            qEbUserCt.prenom,
                            qEbDemande.listTypeDocuments,
                            qPartyOrigin.company,
                            qEbDemande.xEcStatut,
                            qEbDemande.askForTransportResponsibility,
                            qEbDemande.provideTransport,
                            qEbDemande.dateLastPsl,
                            qEbTrackTrace.packingList));

            query = getGlobalWhere(query, where, criteria, connectedUser);

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            if (criteria.getOrder() != null && criteria.getOrder().size() > 0) {

                for (Map<OrderCriterias, String> map: criteria.getOrder()) {
                    String columnName = map.get(OrderCriterias.columnName);

                    if (columnName != null && columnName.matches("^(category|field)[0-9]+$")) {
                        if (map
                            .get(OrderCriterias.dir)
                            .equals("asc")) query.orderBy(new OrderSpecifier(Order.ASC, alias).nullsLast());
                        else query.orderBy(new OrderSpecifier(Order.DESC, alias).nullsLast());
                    }
                    else if (columnName != null && (columnName.contentEquals("codeAlphaPslCourant")
                        || columnName.contentEquals("xEcModeTransport"))) {
                        if (map
                            .get(OrderCriterias.dir)
                            .equals("asc")) query.orderBy(new OrderSpecifier(Order.ASC, numberOrderAlias).nullsLast());
                        else query.orderBy(new OrderSpecifier(Order.DESC, numberOrderAlias).nullsLast());
                    }
                    else if (columnName != null && orderHashMap.get(columnName) != null) {
                        if (map.get(OrderCriterias.dir).equals("asc")) query
                            .orderBy(
                                ((ComparableExpressionBase<String>) orderHashMap.get(columnName)).asc().nullsFirst());
                        else query
                            .orderBy(
                                ((ComparableExpressionBase<String>) orderHashMap.get(columnName)).desc().nullsLast());
                    }

                }

            }
            else {
                query.orderBy(((ComparableExpressionBase<String>) orderHashMap.get("refTransport")).desc().nullsLast());
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    private JPAQuery
        getGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteriaTrackTrace criteria, EbUser connectedUser) {
        QEbEtablissement qEbEtablissementCt = new QEbEtablissement("qEbEtablissementCt");
        QExEbDemandeTransporteur qExDemandeTransporteur = QExEbDemandeTransporteur.exEbDemandeTransporteur;
        SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");

        if (criteria != null) {

            if (criteria.getListEbTtTracingNum() != null && !criteria.getListEbTtTracingNum().isEmpty()) {
                where.and(qEbTrackTrace.ebTtTracingNum.in(criteria.getListEbTtTracingNum()));
            }

            if (criteria.getEbDemandeNum() != null) {
                where.and(qEbDemande.ebDemandeNum.eq(criteria.getEbDemandeNum()));
            }

            if (criteria.getEbTtTracingNum() != null) {
                where.and(qEbTrackTrace.ebTtTracingNum.eq(criteria.getEbTtTracingNum()));
            }

            if (criteria.getListunitsPackingList() != null && !criteria.getListunitsPackingList().isEmpty()) {
                where.and(qEbDemande.unitsPackingList.in(criteria.getListunitsPackingList()));
            }

            if (criteria.getListCustomerRefAndtransportRef() != null
                && !criteria.getListCustomerRefAndtransportRef().isEmpty()) {
                where
                    .andAnyOf(
                        qEbTrackTrace.refTransport.in(criteria.getListCustomerRefAndtransportRef()),
                        qEbTrackTrace.customerReference.in(criteria.getListCustomerRefAndtransportRef()));
            }

            if (criteria.getListEbTtTracingNum() != null && !criteria.getListEbTtTracingNum().isEmpty()) {
                where.and(qEbTrackTrace.ebTtTracingNum.in(criteria.getListEbTtTracingNum()));
            }

            if (criteria.getListDestinations() != null && !criteria.getListDestinations().isEmpty()) {
                BooleanBuilder b = new BooleanBuilder();
                criteria.getListDestinations().stream().forEach(it -> {
                    b.or(qEcCountryDestination.ecCountryNum.eq(it));
                });
                where.and(b);
            }

            if (criteria.getListOrigins() != null && !criteria.getListOrigins().isEmpty()) {
                BooleanBuilder b = new BooleanBuilder();
                criteria.getListOrigins().stream().forEach(it -> {
                    b.or(qEcCountryOrigin.ecCountryNum.eq(it));
                });
                where.and(b);
            }

            if (criteria.getDatePickUpFrom() != null) {
                where = where
                    .andAnyOf(
                        qEbTrackTrace.datePickup.eq(criteria.getDatePickUpFrom()),
                        qEbTrackTrace.datePickup.after(criteria.getDatePickUpFrom()));
            }

            if (criteria.getDatePickUpTo() != null) {
                where = where
                    .andAnyOf(
                        qEbTrackTrace.datePickup.eq(criteria.getDatePickUpTo()),
                        qEbTrackTrace.datePickup.before(criteria.getDatePickUpTo()));
            }

            if (criteria.getListIncoterm() != null && !criteria.getListIncoterm().isEmpty()) {
                where.and(qEbDemande.xEcIncotermLibelle.in(criteria.getListIncoterm()));
            }

            if (criteria.getListModeTransport() != null && !criteria.getListModeTransport().isEmpty()) {
                where.and(qEbDemande.xEcModeTransport.in(criteria.getListModeTransport()));
            }

            if (criteria.getCompanyOrigin() != null && !criteria.getCompanyOrigin().trim().isEmpty()) {
                where.and(qPartyOrigin.company.toLowerCase().contains(criteria.getCompanyOrigin()));
            }

            if (criteria.getListCustomerReference() != null && !criteria.getListCustomerReference().isEmpty()) {
                where.and(qEbDemande.customerReference.in(criteria.getListCustomerReference()));
            }

            if (criteria.getListEbUserCt() != null && !criteria.getListEbUserCt().isEmpty()) {
                where.and(qEbDemande.xEbUserCt().ebUserNum.in(criteria.getListEbUserCt()));
            }

            if (criteria.getListUser() != null && !criteria.getListUser().isEmpty()) {
                where.and(qEbDemande.user().ebUserNum.in(criteria.getListUser()));
            }

            if (criteria.getListDestinationCity() != null && !criteria.getListDestinationCity().isEmpty()) {
                criteria.getListDestinationCity().replaceAll(String::toUpperCase);
                where.and(qPartyDest.city.toUpperCase().in(criteria.getListDestinationCity()));
            }

            if (criteria.getListOriginCity() != null && !criteria.getListOriginCity().isEmpty()) {
                criteria.getListOriginCity().replaceAll(String::toUpperCase);
                where.and(qPartyOrigin.city.toUpperCase().in(criteria.getListOriginCity()));
            }

            if (criteria.getLate() != null) {

                if (Enumeration.YesOrNo.No.getCode().equals(criteria.getLate())) {
                    where.and(qEbTrackTrace.late.in(TtEnumeration.Late.NOT_LATE).or(qEbTrackTrace.late.isNull()));
                }
                else {
                    where.and(qEbTrackTrace.late.in(TtEnumeration.Late.LATE_TRACK));
                }

            }

            if (criteria.getLibelleLastPsl() != null && !criteria.getLibelleLastPsl().isEmpty()) {
                where.and(qEbTrackTrace.codeAlphaLastPsl.in(criteria.getLibelleLastPsl()));
            }

            if (criteria.getDateLastPsl() != null) {
                where
                    .andAnyOf(
                        qEbTrackTrace.dateLastPsl
                            .stringValue().contains(toPostgresDate(formater.format(criteria.getDateLastPsl()))));
            }

            if (criteria.getFinalDelivery() != null) {
                where
                    .andAnyOf(
                        qEbDemande.finalDelivery
                            .stringValue().contains(toPostgresDate(formater.format(criteria.getFinalDelivery()))));
            }

            if (criteria.getDatePickupTM() != null) {
                where
                    .andAnyOf(
                        qEbDemande.datePickupTM
                            .stringValue().contains(toPostgresDate(formater.format(criteria.getDatePickupTM()))));
            }

            if (criteria.getTotalWeight() != null) {
                where.and(qEbTrackTrace.totalWeight.in(criteria.getTotalWeight()));
            }

            if (criteria.getListNumAwbBol() != null && !criteria.getListNumAwbBol().isEmpty()) {
                where.and(qEbDemande.numAwbBol.in(criteria.getListNumAwbBol()));
            }

            if (criteria.getListStatutStr() != null && !criteria.getListStatutStr().isEmpty()) {
                BooleanBuilder b = new BooleanBuilder();
                List<String> listStatut = new ArrayList();

                if (criteria.getListStatutStr().contains("WPU")) {
                    b.or(qEbTrackTrace.codeAlphaPslCourant.isNull());
                }

                b.or(qEbTrackTrace.codeAlphaPslCourant.in(criteria.getListStatutStr()));

                where.and(b);
            }

            if (criteria.getListTransportStatus() != null && !criteria.getListTransportStatus().isEmpty()) {
                BooleanBuilder whereTrStatus = new BooleanBuilder();

                for (Integer st: criteria.getListTransportStatus()) {
                    if (TransitStatus.WAITINGFORPICKUP
                        .getCode().equals(st)) whereTrStatus.or(qEbTrackTrace.codeAlphaPslCourant.isNull());

                    if (TransitStatus.ONGOING.getCode().equals(st)) {
                        whereTrStatus
                            .orAllOf(
                                qEbDemande.codeAlphaLastPsl.isNotNull(),
                                qEbTrackTrace.codeAlphaPslCourant.ne(qEbDemande.codeAlphaLastPsl));
                    }

                    if (TransitStatus.COMPLETED.getCode().equals(st)) {
                        whereTrStatus
                            .orAllOf(
                                qEbDemande.codeAlphaLastPsl.isNotNull(),
                                qEbTrackTrace.codeAlphaPslCourant.eq(qEbDemande.codeAlphaLastPsl));
                    }

                }

                where.and(whereTrStatus);
            }

            if (criteria.getListCarrier() != null && !criteria.getListCarrier().isEmpty()) {
                where = where.and(qEbTrackTrace.xEbTransporteur().ebUserNum.in(criteria.getListCarrier()));
            }

            /*
             * CAS DE LA RECHERCHE DE LA DEMANDE PAR LES DONNEES DE LA
             * MARCHANDISE
             */
            if (criteria.getListUnitReference() != null && !criteria.getListUnitReference().isEmpty()) {
                criteria.setSearchDemandeByMarchandiseData(true);
                where.and(qEbMarchandise.unitReference.in(criteria.getListUnitReference()));
            }

            criteria.applyAdvancedSearchForCustomFields(where, qEbDemande.customFields);

            if (criteria.getCarrierUniqRefNum() != null && !criteria.getCarrierUniqRefNum().isEmpty()) {
                where.and(qEbDemande.carrierUniqRefNum.contains(criteria.getCarrierUniqRefNum()));
            }

            if (criteria.getTransportConfirmation() != null) {

                if (criteria.getTransportConfirmation().size() == TransportConfirmation.values().length) {
                    where
                        .and(
                            qEbDemande.askForTransportResponsibility
                                .isTrue()
                                .and(qEbDemande.provideTransport.isFalse().or(qEbDemande.provideTransport.isNull())))
                        .or(
                            qEbDemande.askForTransportResponsibility
                                .isTrue().and(qEbDemande.provideTransport.isTrue()));
                }
                else {

                    if (criteria
                        .getTransportConfirmation()
                        .contains(TransportConfirmation.WAITING_FOR_CONFIRMATION.getCode())) {
                        where
                            .and(qEbDemande.askForTransportResponsibility.isTrue())
                            .and(qEbDemande.provideTransport.isFalse().or(qEbDemande.provideTransport.isNull()));
                    }

                    if (criteria.getTransportConfirmation().contains(TransportConfirmation.CONFIRMED.getCode())) {
                        where
                            .and(qEbDemande.askForTransportResponsibility.isTrue())
                            .and(qEbDemande.provideTransport.isTrue());
                    }

                }

            }

            BooleanBuilder boolLabels1 = new BooleanBuilder();

            if (criteria.getMapCategoryFields() != null && !criteria.getMapCategoryFields().isEmpty()) {
                BooleanBuilder orBoolLabels = null;

                List<Integer> ebLabels = null;

                for (String key: criteria.getMapCategoryFields().keySet()) {
                    ebLabels = criteria.getMapCategoryFields().get(key);

                    if (ebLabels != null) {
                        orBoolLabels = new BooleanBuilder();
                        int i, n = ebLabels.size();

                        for (i = 0; i < n; i++) {
                            Long ebLabelNum = Math.round(Double.parseDouble(ebLabels.get(i) + ""));
                            orBoolLabels
                                .or(
                                    new BooleanBuilder()
                                        .andAnyOf(
                                            qEbDemande.listLabels.startsWith(ebLabelNum + ","),
                                            qEbDemande.listLabels.contains("," + ebLabelNum + ","),
                                            qEbDemande.listLabels.endsWith("," + ebLabelNum),
                                            qEbDemande.listLabels.eq(ebLabelNum + "")));
                        }

                        if (orBoolLabels.hasValue()) boolLabels1.and(orBoolLabels);
                    }

                }

                /*
                 * if (!boolLabels.hasValue())
                 * boolLabels.and(qEbDemande.listLabels.isEmpty());
                 */
                if (boolLabels1.hasValue()) where.andAnyOf(qEbDemande.listLabels.isNotNull().andAnyOf(boolLabels1));
            }

            if (criteria.getSearch() != null) {
                String value = criteria.getSearch().get("value");
                String dt = null;
                String valueModeTr = "";
                String valueTypeTr = "";

                if (value != null && !value.isEmpty()) {
                    value = value.trim().toLowerCase();

                    dt = Statiques.convertSubDateToEngFormat(value);

                    if (value.contains("(")) {

                        try {
                            valueModeTr = value.replace(")", "");
                            String[] vs = valueModeTr.split("\\(");
                            valueModeTr = vs[0].trim();
                            valueTypeTr = vs[1].trim();
                        } catch (Exception e) {
                        }

                    }
                    else {
                        valueModeTr = value.toString();
                    }

                    List<Integer> listCodeModeTransport = Enumeration.ModeTransport.getListCodeByLibelle(valueModeTr);

                    if (CollectionUtils
                        .isEmpty(listCodeModeTransport)) listCodeModeTransport = new ArrayList<Integer>();
                    Integer modeTrCode = Enumeration.ModeTransport.getCodeByLibelle(valueModeTr);

                    BooleanBuilder codeAlphaCriteria = new BooleanBuilder();
                    codeAlphaCriteria.or(qEbTrackTrace.codeAlphaPslCourant.toLowerCase().contains(value));
                    if ("waiting for pickup"
                        .contains(value)) codeAlphaCriteria.or(qEbTrackTrace.codeAlphaPslCourant.isNull());
                    BooleanBuilder transportConfirmation = new BooleanBuilder();
                    if (DemandeStatus.CONFIRMED.getLibelle().contains(value)) {
                        transportConfirmation
                            .and(qEbDemande.askForTransportResponsibility.isTrue())
                            .and(qEbDemande.provideTransport.isTrue());
                    }
                    else if (DemandeStatus.WAITING_FOR_CONFIRMATION.getLibelle().contains(value)) {
                        transportConfirmation
                            .and(qEbDemande.askForTransportResponsibility.isTrue())
                            .and(qEbDemande.provideTransport.isFalse().or(qEbDemande.provideTransport.isNull()));
                    }

                    BooleanBuilder w = new BooleanBuilder();
                    w
                        .andAnyOf(
                            qEbTrackTrace.refTransport.toLowerCase().contains(value),
                            qEbTrackTrace.customerReference.toLowerCase().contains(value),
                            qEbTrackTrace.partNumber.toLowerCase().contains(value),
                            qEbTrackTrace.orderNumber.toLowerCase().contains(value),
                            qEbTrackTrace.shippingType.toLowerCase().contains(value),
                            qEbTrackTrace.serialNumber.toLowerCase().contains(value),
                            qEcCountryOrigin.libelle.toLowerCase().contains(value),
                            qEcCountryDestination.libelle.toLowerCase().contains(value),
                            qEbTrackTrace.totalWeight.stringValue().toLowerCase().contains(value),
                            qEbTrackTrace.datePickup.stringValue().contains(toPostgresDate(value)),
                            qEbTrackTrace.dateModification.stringValue().contains(toPostgresDate(value)),
                            qEbDemande.customerReference.toLowerCase().contains(value),
                            qEbTrackTrace.libelleOriginCity.stringValue().toLowerCase().contains(value),
                            qEbTrackTrace.libelleDestCity.stringValue().toLowerCase().contains(value),
                            qEbDemande.numAwbBol.stringValue().toLowerCase().contains(value),
                            qEbDemande.carrierUniqRefNum.stringValue().toLowerCase().contains(value),
                            qEbTrackTrace.xEcIncotermLibelle.stringValue().toLowerCase().contains(value),
                            qEbDemande.listCustomFieldsFlat.toLowerCase().like("%:\"%" + value + "%\"%"),
                            qEbDemande.listCategoryFlat.toLowerCase().like("%:\"%" + value + "%\"%"),
                            qEbDemande.datePickupTM.stringValue().contains(toPostgresDate(value)),
                            qEbDemande.finalDelivery.stringValue().contains(toPostgresDate(value)),
                            qEbChargeur.nom.toLowerCase().contains(value),
                            qEbChargeur.prenom.toLowerCase().contains(value),
                            qEbUserCt.nom.toLowerCase().contains(value),
                            qEbUserCt.prenom.toLowerCase().contains(value),
                            qEbTrackTrace.nomEtablissementTransporteur.toLowerCase().contains(value),
                            codeAlphaCriteria,
                            qEbTrackTrace.libelleLastPsl.toLowerCase().contains(value),
                            qEbDemande.ebPartyOrigin().company.toLowerCase().contains(value),
                            qEbTrackTrace.customerReference.containsIgnoreCase(value),
                            qEbTrackTrace.xEbChargeur().nom.containsIgnoreCase(value),
                            qEbTrackTrace.xEbChargeur().prenom.containsIgnoreCase(value),
                            qEbTrackTrace.packingList.containsIgnoreCase(value),
                            transportConfirmation);
                    if (dt != null) w.or(qEbTrackTrace.datePickup.stringValue().toLowerCase().contains(dt));
                    if (dt != null) w.or(qEbTrackTrace.dateModification.stringValue().toLowerCase().contains(dt));

                    if (dt != null) w.or(qEbTrackTrace.dateCreation.stringValue().toLowerCase().contains(dt));

                    if (modeTrCode == null && !listCodeModeTransport.isEmpty()) w
                        .or(qEbTrackTrace.xEcModeTransport.in(listCodeModeTransport));

                    if (modeTrCode != null) {
                        w.or(qEbTrackTrace.xEcModeTransport.eq(modeTrCode));
                    }

                    w.or(qEbTrackTrace.codeAlphaPslCourant.toLowerCase().contains(value));
                    w.or(qEbTrackTrace.libellePslCourant.toLowerCase().contains(value));

                    where.and(w);
                }

            }

            where.and(qEbDemande.xEcCancelled.isNull());

            // TODO : à vérifier si c'est le bon endroit d'appliquer cette
            // visibilité du T&T pour les transporteurs
            // A noter que la visibilité n'était pas géré avant ce code et que
            // les transporteurs qui ont le module T&T actif ont la visibilité
            // sur
            // toute la base de données (C'est un trou de sécurité)
            if (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker()) {

                if (connectedUser.isSuperAdmin()) {
                    where
                        .and(
                            qEbTrackTrace.xEbCompagnieTransporteur().ebCompagnieNum
                                .eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
                }
                else if (connectedUser.isAdmin()) {
                    where
                        .and(
                            qEbTrackTrace.xEbEtablissementTransporteur().ebEtablissementNum
                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));
                }
                else {
                    BooleanBuilder whereNotAdminAndSuperAdmin = new BooleanBuilder();
                    whereNotAdminAndSuperAdmin
                        .andAnyOf(
                            new BooleanBuilder()
                                .orAllOf(qEbTrackTrace.xEbTransporteur().ebUserNum.eq(connectedUser.getEbUserNum())),
                            new BooleanBuilder()
                                .orAllOf(
                                    qEbTrackTrace.xEbEtablissementTransporteur().ebEtablissementNum
                                        .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())));
                    where.and(whereNotAdminAndSuperAdmin);
                }

            }

            BooleanBuilder whereBroker = new BooleanBuilder();
            whereBroker
                .andAnyOf(
                    new BooleanBuilder()
                        .orAllOf(
                            qUserOriginCB.isNotNull(),
                            qUserOriginCBEtab.ebEtablissementNum
                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())),
                    new BooleanBuilder()
                        .orAllOf(
                            qUserDestCB.isNotNull(),
                            qUserDestCBEtab.ebEtablissementNum
                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())));

            /* VISIBLITE DES USERS ------------------------------------ */
            BooleanBuilder whereVisibilite = applyVisibilityRules(
                connectedUser,
                criteria.getModule(),
                qEbEtablissementCt,
                qEtablissementQuote,
                qEbEtablissementChargeur,
                qEbCompagnieChargeur,
                qEbDemande,
                qUserDestEtab,
                qUserOriginEtab,
                qUserDestCBEtab,
                qUserOriginCBEtab,
                qCompagnie,
                qExDemandeTransporteur);

            if (whereVisibilite.hasValue()) where.and(whereVisibilite);

            // recherche par flags
            if (criteria.getListFlag() != null && !criteria.getListFlag().trim().isEmpty()) {
                List<String> listFlagId = Arrays.asList(criteria.getListFlag().trim().split(","));
                Predicate[] predicats = new Predicate[listFlagId.size() * 4];
                int j = 0;

                for (int i = 0; i < listFlagId.size(); i++) {
                    predicats[i + j] = qEbTrackTrace.listFlag
                        .contains(listFlagId.get(i))
                        .and(qEbTrackTrace.listFlag.trim().length().eq(listFlagId.get(i).length()));
                    predicats[i + j + 1] = qEbTrackTrace.listFlag.endsWith("," + listFlagId.get(i));
                    predicats[i + j + 2] = qEbTrackTrace.listFlag.startsWith(listFlagId.get(i) + ",");
                    predicats[i + j + 3] = qEbTrackTrace.listFlag.contains("," + listFlagId.get(i) + ",");
                    j += 3;
                }

                where.andAnyOf(predicats);
            }

        }

        query.distinct();
        query.from(qEbTrackTrace).where(where);

        if (criteria.isSearchDemandeByMarchandiseData()) {
            query.leftJoin(qEbTrackTrace.xEbMarchandise(), qEbMarchandise);
        }

        query.leftJoin(qEbTrackTrace.xEbDemande(), qEbDemande);
        query.leftJoin(qEbDemande.user(), qEbuserOwner);
        query.leftJoin(qEbTrackTrace.xEbChargeur(), qEbChargeur);
        query.leftJoin(qEbTrackTrace.xEbChargeurCompagnie(), qEbCompagnieChargeur);
        query.leftJoin(qEbTrackTrace.xEbEtablissementChargeur(), qEbEtablissementChargeur);
        query.leftJoin(qEbTrackTrace.xEcCountryOrigin(), qEcCountryOrigin);
        query.leftJoin(qEbTrackTrace.xEcCountryDestination(), qEcCountryDestination);

        query.leftJoin(qEbDemande.xEbUserOrigin(), qUserOrigin);
        query.leftJoin(qUserOrigin.ebEtablissement(), qUserOriginEtab);
        query.leftJoin(qEbDemande.xEbUserDest(), qUserDest);
        query.leftJoin(qUserDest.ebEtablissement(), qUserDestEtab);
        query.leftJoin(qEbDemande.xEbUserOriginCustomsBroker(), qUserOriginCB);
        query.leftJoin(qUserOriginCB.ebEtablissement(), qUserOriginCBEtab);
        query.leftJoin(qEbDemande.xEbUserDestCustomsBroker(), qUserDestCB);
        query.leftJoin(qUserDestCB.ebEtablissement(), qUserDestCBEtab);

        query.leftJoin(qEbDemande.xecCurrencyInvoice(), qEcCurrency);
        query.leftJoin(qEbDemande.xEbCostCenter(), qEbCostCenter);

        query.leftJoin(qEbTrackTrace.xEbUserCt(), qEbUserCt);

        query.leftJoin(qEbDemande.ebPartyOrigin(), qPartyOrigin);

        query.leftJoin(qEbDemande.ebPartyDest(), qPartyDest);

        query.leftJoin(qEbDemande.xEbCompagnie(), qCompagnie);
        query.leftJoin(qEbDemande.exEbDemandeTransporteurs, qExDemandeTransporteur);
        query.leftJoin(qExDemandeTransporteur.xEbEtablissement(), qEtablissementQuote);

        if (connectedUser.isControlTower()) query.leftJoin(qEbUserCt.ebEtablissement(), qEbEtablissementCt);

        return query;
    }

    @Override
    public Long getCountListEbTrackTrace(SearchCriteriaTrackTrace criteria) {
        Long count = new Long(0);
        BooleanBuilder where = new BooleanBuilder();

        try {
            JPAQuery<Long> query = new JPAQuery<Long>(em);

            EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);
            query = getGlobalWhere(query, where, criteria, connectedUser);
            // query.distinct();

            count = (Long) query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return count;
    }

    @Override
    public List<EbTtTracing> getListCustomerReference(String term) {
        // TODO Auto-generated method stub

        List<EbTtTracing> resultat = new ArrayList<EbTtTracing>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbTtTracing> query = new JPAQuery<EbTtTracing>(em);

            query
                .select(
                    QEbTtTracing
                        .create(Expressions.asNumber(0), qEbTrackTrace.customerReference, Expressions.asString("")));

            SearchCriteriaTrackTrace criteria = new SearchCriteriaTrackTrace();
            EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);
            query = getGlobalWhere(query, where, criteria, connectedUser);

            if (term != null) {
                term = term.trim().toLowerCase();
                where.andAnyOf(qEbTrackTrace.customerReference.toLowerCase().contains(term));
            }

            query.distinct().from(qEbTrackTrace).where(where);

            resultat = (List<EbTtTracing>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public List<EbTtTracing> getListRefTransport(String term) {
        // TODO Auto-generated method stub

        List<EbTtTracing> resultat = new ArrayList<EbTtTracing>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbTtTracing> query = new JPAQuery<EbTtTracing>(em);

            query
                .select(
                    QEbTtTracing.create(Expressions.asNumber(0), Expressions.asString(""), qEbTrackTrace.refTransport));

            SearchCriteriaTrackTrace criteria = new SearchCriteriaTrackTrace();
            EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);
            query = getGlobalWhere(query, where, criteria, connectedUser);

            if (term != null) {
                term = term.trim().toLowerCase();
                where.andAnyOf(qEbTrackTrace.refTransport.toLowerCase().contains(term));
            }

            query.distinct().from(qEbTrackTrace).where(where);

            resultat = (List<EbTtTracing>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public List<EbTtTracing> getListTracing(SearchCriteriaTrackTrace criteria) {
        // TODO Auto-generated method stub

        List<EbTtTracing> resultat = new ArrayList<EbTtTracing>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbTtTracing> query = new JPAQuery<EbTtTracing>(em);

            query.select(qEbTrackTrace);

            if (criteria.getSearchterm() != null && criteria.getSearchterm() != "") {
                where
                    .andAnyOf(
                        qEbTrackTrace.refTransport.toLowerCase().contains(criteria.getSearchterm().toLowerCase()),
                        qEbTrackTrace.customRef.toLowerCase().contains(criteria.getSearchterm().toLowerCase()),
                        qEbTrackTrace.customerReference.toLowerCase().contains(criteria.getSearchterm().toLowerCase()),
                        qEbTrackTrace.xEbMarchandise().unitReference
                            .toLowerCase().contains(criteria.getSearchterm().toLowerCase()));
            }

            query.distinct().from(qEbTrackTrace).where(where);

            resultat = (List<EbTtTracing>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public List<EbTtTracing> getListTracingByRefTransportAndUnitRef(
        List<String> listRefTransport,
        List<String> listRefUnit,
        Boolean isTransportRef,
        List<String> listCodeAlphaPsl) {
        List<EbTtTracing> resultat = new ArrayList<EbTtTracing>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbTtTracing> query = new JPAQuery<EbTtTracing>(em);

            query
                .select(
                    QEbTtTracing
                        .create(
                            qEbTrackTrace.ebTtTracingNum,
                            qEbTrackTrace.codeAlphaPslCourant,
                            qEbTrackTrace.xEbDemande().ebDemandeNum,
                            qEbTrackTrace.configPsl,
                            qEbTrackTrace.customerReference,
                            qEbTrackTrace.refTransport,
                            qEbTrackTrace.customRef));

            query.distinct();
            query.from(qEbTrackTrace);

            query = getWhereRefTransportAndUnitRef(query, where, listRefTransport, listRefUnit, isTransportRef);
            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    private JPAQuery getWhereRefTransportAndUnitRef(
        JPAQuery query,
        BooleanBuilder where,
        List<String> listRefTransport,
        List<String> listRefUnit,
        Boolean isTransportRef) {
        final Integer conditionNbr = listRefTransport.size();

        Predicate[] predicats = new Predicate[conditionNbr];

        for (int i = 0; i < conditionNbr; i++) {

            if (isTransportRef) {
                predicats[i] = qEbTrackTrace.refTransport
                    .eq(listRefTransport.get(i)).and(qEbTrackTrace.customerReference.eq(listRefUnit.get(i)));
            }
            else {
                predicats[i] = qEbTrackTrace.customRef
                    .eq(listRefTransport.get(i)).and(qEbTrackTrace.customerReference.eq(listRefUnit.get(i)));
            }

        }

        where.andAnyOf(predicats);

        query.where(where);

        return query;
    }
}
