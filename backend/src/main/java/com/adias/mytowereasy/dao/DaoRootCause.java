package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.qm.EbQmRootCause;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


public interface DaoRootCause {
    public List<EbQmRootCause> getListEbQmRootCause(SearchCriteriaQM criterias);

    public Long listCount(SearchCriteriaQM criterias);
}
