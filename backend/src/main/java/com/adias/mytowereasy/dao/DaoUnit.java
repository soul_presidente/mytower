package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.TreeNode;


public interface DaoUnit {
    public void buildTree(TreeNode<EbMarchandise> node, Integer idDemande);

    public List<TreeNode<EbMarchandise>> getTrees(Integer idDemande);
}
