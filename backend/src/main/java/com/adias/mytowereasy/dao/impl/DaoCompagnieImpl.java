/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.ConstructorExpression;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.api.wso.TrackingEventWSO;
import com.adias.mytowereasy.dao.DaoCompagnie;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbCompagnieRepository;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoCompagnieImpl extends Dao implements DaoCompagnie {

    @PersistenceContext
    EntityManager em;

    @Autowired
    private EbCompagnieRepository ebCompagnieRepository;

    QEbUser qEbUser = QEbUser.ebUser;
    QEbCompagnie qEbCompagnie = QEbCompagnie.ebCompagnie;
    QEbEtablissement qEbEtablissement = QEbEtablissement.ebEtablissement;

    @Override
    public List<EbEtablissement> selectListEbEtablissementOfCompagnie(SearchCriteria criterias) {
        List<EbEtablissement> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPQLQuery query = new JPAQuery(em);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public List<EbCompagnie> selectListCompagnie(SearchCriteria criteria) {
        List<EbCompagnie> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPQLQuery query = new JPAQuery(em);

            ConstructorExpression<EbCompagnie> selection = null;
            selection = QEbCompagnie.create(qEbCompagnie.ebCompagnieNum, qEbCompagnie.nom, qEbCompagnie.code);

            if (criteria != null) {

                if (criteria.getEcRoleNum() != null) {
                    where.and(qEbCompagnie.compagnieRole.eq(criteria.getEcRoleNum()));
                }

            }

            query.distinct();
            query.select(selection);
            query.from(qEbCompagnie);
            query.where(where);

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public EbCompagnie getCarrierCompagnie(TrackingEventWSO event) {
        EbCompagnie carrierCompagnie = new EbCompagnie();

        QExEbDemandeTransporteur qEbDemandeTransporteur = new QExEbDemandeTransporteur("qEbDemandeQuote");
        QEbDemande qEbDemande = new QEbDemande("qEbDemande");
        QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbCompagnie");

        BooleanBuilder where = new BooleanBuilder();

        JPAQuery<EbCompagnie> query = new JPAQuery<EbCompagnie>(em);

        try {

            if (event.getRefTransport() != null || event.getCarrierCode() != null) {
                query.distinct();

                query
                    .select(
                        QEbCompagnie
                            .create(
                                qEbCompagnie.ebCompagnieNum,
                                qEbCompagnie.nom,
                                qEbCompagnie.code,
                                qEbCompagnie.siren,
                                qEbCompagnie.compagnieRole,
                                qEbCompagnie.groupeName,
                                qEbCompagnie.logo));

                query.from(qEbDemande).where(where);
                query.leftJoin(qEbDemande.exEbDemandeTransporteurs, qEbDemandeTransporteur);
                query.leftJoin(qEbDemandeTransporteur.xEbCompagnie(), qEbCompagnie);

                if (event.getRefTransport() != null) {
                    where.and(qEbDemande.refTransport.in(event.getRefTransport()));
                }

                if (event.getCarrierCode() != null) {
                    where.and(qEbCompagnie.code.in(event.getCarrierCode()));
                }

            }

            query.where(where);
            carrierCompagnie = (EbCompagnie) query.fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return carrierCompagnie;
    }
}
