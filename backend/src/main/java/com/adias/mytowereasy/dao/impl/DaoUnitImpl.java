package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoUnit;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.QEbDemande;
import com.adias.mytowereasy.model.QEbMarchandise;
import com.adias.mytowereasy.model.TreeNode;


@Component
@Transactional
public class DaoUnitImpl implements DaoUnit {
    @PersistenceContext
    EntityManager em;

    QEbMarchandise qEbMarchandise = QEbMarchandise.ebMarchandise;

    QEbDemande qEbDemande = QEbDemande.ebDemande;

    @Override
    public void buildTree(TreeNode<EbMarchandise> node, Integer idDemande) {
        TreeNode<EbMarchandise> currentNode = node;

        try {

            if (currentNode.getData() == null) {
                return;
            }

            JPAQuery<EbMarchandise> query = new JPAQuery<EbMarchandise>(em);
            query.from(qEbMarchandise);
            query.join(qEbMarchandise.ebDemande(), qEbDemande);
            query.where(qEbMarchandise.ebDemande().ebDemandeNum.eq(idDemande));
            query.where(qEbMarchandise.parent().ebMarchandiseNum.eq(currentNode.getData().getEbMarchandiseNum()));
            List<EbMarchandise> result = query.fetch();

            for (EbMarchandise child: result) {
                TreeNode<EbMarchandise> childNode = currentNode.addChild(child);
                childNode.setLabel(child.getTypeContainer() + " " + child.getUnitReference());
                String collapsedIcon = "";
                String expandedIcon = "";

                // Conditions pour remplir les icônes.

                childNode.setCollapsedIcon(collapsedIcon);
                childNode.setExpandedIcon(expandedIcon);

                buildTree(childNode, idDemande);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<TreeNode<EbMarchandise>> getTrees(Integer idDemande) {
        List<TreeNode<EbMarchandise>> resultTree = new ArrayList<TreeNode<EbMarchandise>>();

        try {
            JPAQuery<EbMarchandise> query = new JPAQuery<EbMarchandise>(em);
            query.from(qEbMarchandise);
            query.join(qEbMarchandise.ebDemande(), qEbDemande);
            query.where(qEbMarchandise.ebDemande().ebDemandeNum.eq(idDemande));
            query.where(qEbMarchandise.parent().isNull());
            List<EbMarchandise> result = query.fetch();

            for (EbMarchandise value: result) {
                TreeNode<EbMarchandise> node = new TreeNode<EbMarchandise>(value);
                buildTree(node, idDemande);
                resultTree.add(node);
            }

        } catch (Exception e) {
            resultTree = null;
            e.printStackTrace();
        }

        return resultTree;
    }
}
