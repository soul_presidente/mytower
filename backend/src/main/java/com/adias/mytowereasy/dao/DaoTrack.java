/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.tt.EbTtTracing;
import com.adias.mytowereasy.utils.search.SearchCriteriaTrackTrace;


public interface DaoTrack {
    List<EbTtTracing> getListEbTrackTrace(SearchCriteriaTrackTrace criteria, EbUser connectedUser);

    Long getCountListEbTrackTrace(SearchCriteriaTrackTrace criteria);

    public EbTtTracing getEbTrackTrace(SearchCriteriaTrackTrace criteria);

    List<EbTtTracing> getListCustomerReference(String term);

    List<EbTtTracing> getListRefTransport(String term);

    List<EbTtTracing> getListTracing(SearchCriteriaTrackTrace criteria);

    List<EbTtTracing> getListTracingByRefTransportAndUnitRef(
        List<String> listRefTransport,
        List<String> listRefUnit,
        Boolean isTransportRef,
        List<String> listCodeAlphaPsl);
}
