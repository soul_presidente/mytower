/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;


public interface DaoSchemaPsl {
    public List<EbTtSchemaPsl> getListSchemaPsl();
}
