package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbIncoterm;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoIncoterm {
    List<EbIncoterm> getListIncoterms(SearchCriteria criteria);

    Long getListIncotermsCount(SearchCriteria criteria);
}
