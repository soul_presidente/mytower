package com.adias.mytowereasy.dao.impl;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.dao.DaoFileDownloader;


@Component
@Transactional
public class DaoFileDownloaderImpl implements DaoFileDownloader {
    @PersistenceContext
    EntityManager em;

    @Override
    public List<Map<String, String[]>> extractTM() {
        List<Map<String, String[]>> result = new ArrayList<Map<String, String[]>>();

        try {
            String sqlString = "";

            sqlString += "WITH pslapp as (";
            sqlString += "		select ";
            sqlString += "		ttpslapp.eb_tt_psl_app_num, ttpslapp.code_psl, ttpslapp.date_actuelle, ttpslapp.date_estimee, ttpslapp.date_negotiation, ttpslapp.date_creation, ttpslapp.x_eb_tt_tracing, ttpslapp.code_alpha AS acode ";
            sqlString += "		from work.eb_tt_psl_app ttpslapp order  by ttpslapp.eb_tt_psl_app_num asc";
            sqlString += "	)";
            sqlString += "	";
            sqlString += "	, grouppsl as (	";
            sqlString += "	select 	";
            sqlString += "		date_creation, ";
            sqlString += "		x_eb_tt_tracing, ";
            sqlString += "		'{\"x_eb_tt_tracing\":' || x_eb_tt_tracing\\:\\:text || ',' || string_agg ( '\"' || lower(acode) || '\":\"' || cast(date_actuelle as TEXT)||'\"' , ',' ) || '}' as date_actuelle, ";
            sqlString += "		'{\"x_eb_tt_tracing\":' || x_eb_tt_tracing\\:\\:text || ',' || string_agg ( '\"' || lower(acode) || '\":\"' || cast(date_estimee as TEXT)||'\"' , ',' ) || '}' as date_estimee, ";
            sqlString += "		'{\"x_eb_tt_tracing\":' || x_eb_tt_tracing\\:\\:text || ',' || string_agg ( '\"' || lower(acode) || '\":\"' || cast(date_negotiation as TEXT)||'\"' , ',' ) || '}' as date_negotiation,";
            sqlString += "		(select config_psl  from  work.eb_tt_tracing tt  where tt.eb_tt_tracing_num = pt.x_eb_tt_tracing) as orgconfig ";
            sqlString += "	from pslapp pt ";
            sqlString += "	group by x_eb_tt_tracing, date_creation";
            sqlString += ")";

            sqlString += ", splitedpsldates as (";
            sqlString += "SELECT ";
            sqlString += "	t.x_eb_tt_tracing, ";
            sqlString += "	";
            sqlString += "	da.pic as \"Pickup Actual date\",";
            sqlString += "	da.dep as \"Departure Actual date\",";
            sqlString += "	da.arr as \"Arrival Actual date\",";
            sqlString += "	da.cus as \"Customs Actual date\",";
            sqlString += "	da.del as \"Delivery Actual date\"";
            sqlString += "	";
            sqlString += "FROM   ";
            sqlString += "	grouppsl t";
            sqlString += "  left join json_to_record(cast(t.date_actuelle AS json)) da(x_eb_tt_tracing int, PIC text, DEP text, ARR text, CUS text, DEL text) on (da.x_eb_tt_tracing = t.x_eb_tt_tracing)";
            sqlString += "	left join json_to_record(cast(t.date_estimee AS json)) de(x_eb_tt_tracing int, PIC text, DEP text, ARR text, CUS text, DEL text) on (de.x_eb_tt_tracing = t.x_eb_tt_tracing)";
            sqlString += "	left join json_to_record(cast(t.date_negotiation AS json)) dn(x_eb_tt_tracing int, PIC text, DEP text, ARR text, CUS text, DEL text) on (dn.x_eb_tt_tracing = t.x_eb_tt_tracing)";
            sqlString += ")";

            sqlString += "SELECT";
            sqlString += "	d.ref_transport,";
            sqlString += "	d.date_creation,";
            sqlString += "	compch.nom as \"Chargeur: Société\",";
            sqlString += "	etch.nom as \"Chargeur: Etablissement\",";
            sqlString += "	ch.email AS \"Chargeur: Email\",";
            sqlString += "	";
            sqlString += "	d.x_eb_schema_psl ->> 'xEcIncotermLibelle' AS \"Incoterm\", ";
            sqlString += "	CASE";
            sqlString += "		WHEN d.x_ec_mode_transport = 1 THEN 'Air' ";
            sqlString += "		WHEN d.x_ec_mode_transport = 2 THEN 'Sea' ";
            sqlString += "		WHEN d.x_ec_mode_transport = 3 THEN 'Road' ";
            sqlString += "		WHEN d.x_ec_mode_transport = 4 THEN 'Integrator' ";
            sqlString += "		WHEN d.x_ec_mode_transport = 5 THEN 'Rail' ";
            sqlString += "		ELSE'' || d.x_ec_mode_transport ";
            sqlString += "	END AS \"mode transport\",";
            sqlString += "	";
            sqlString += "	d.libelle_last_psl,";
            sqlString += "	";
            sqlString += "	CASE";
            sqlString += "		when x_ec_cancelled = 1 then 'Cancelled'";
            sqlString += "		WHEN x_ec_statut = 1 THEN 'In process waiting for quote' ";
            sqlString += "		WHEN x_ec_statut = 2 THEN 'Waiting for recommendation' ";
            sqlString += "		WHEN x_ec_statut = 3 THEN 'Waiting for confirmation' ";
            sqlString += "		WHEN x_ec_statut >= 4 THEN 'Confirmed'";
            sqlString += "		WHEN x_ec_statut = 6 THEN 'Cancellation of quotation request ' ";
            sqlString += "		WHEN x_ec_statut = 11 THEN 'Waiting for pick up' ";
            sqlString += "		WHEN x_ec_statut = 12 THEN 'Transport on going'";
            sqlString += "		WHEN x_ec_statut = 13 THEN 'Delivery' ";
            sqlString += "		WHEN x_ec_statut = 14 THEN 'Waiting for transportation request'";
            sqlString += "	END AS \"statut QR \",";
            sqlString += "	CASE";
            sqlString += "		when x_ec_cancelled = 1 then 'Cancelled'";
            sqlString += "		WHEN x_ec_statut = 11 THEN 'Waiting for pick up'";
            sqlString += "		WHEN x_ec_statut = 12 THEN 'Transport on going' ";
            sqlString += "		WHEN x_ec_statut = 13 THEN 'Completed'";
            sqlString += "	END AS \"statut TR \",";
            sqlString += "	compq.nom as \"Transporteur sollicité : Société\",";
            sqlString += "	etq.nom as \"Transporteur sollicité : Etablissement\",";
            sqlString += "	tr.email as \"Transporteur sollicité : Email\",";
            sqlString += "	CASE";
            sqlString += "		WHEN q.status = 1 THEN 'Request received by FF' ";
            sqlString += "		WHEN q.status = 2 THEN 'Quote sent by FF' ";
            sqlString += "		WHEN q.status = 3 THEN 'Recommendation by CT' ";
            sqlString += "		WHEN q.status = 4 THEN 'Final choice'";
            sqlString += "		else q.status|| ''";
            sqlString += "	END AS \"statut quotation\", ";
            sqlString += "	";
            sqlString += "	trac.customer_reference as \"Unit ref\", ";
            sqlString += "	case ";
            sqlString += "		WHEN trac.libelle_psl_courant IS NOT NULL THEN trac.libelle_psl_courant";
            sqlString += "		else 'Waiting for pickup'";
            sqlString += "	end as \"Status TT du Unit\",";
            sqlString += "	d.date_last_psl \"Date last psl\",";
            sqlString += "	spd.*";

            sqlString += "FROM";
            sqlString += "	WORK.eb_demande_quote  q";
            sqlString += "	left JOIN WORK.eb_demande d ON ( q.x_eb_demande = d.eb_demande_num)";
            sqlString += "	LEFT JOIN WORK.eb_user ch ON ( ch.eb_user_num = d.x_eb_user )";
            sqlString += "	LEFT JOIN WORK.eb_user tr ON ( tr.eb_user_num = q.x_transporteur )";
            sqlString += "	LEFT JOIN WORK.eb_etablissement etq ON ( etq.eb_etablissement_num = q.x_eb_etablissement )";
            sqlString += "	LEFT JOIN WORK.eb_compagnie compq ON ( compq.eb_compagnie_num = etq.x_eb_compagnie )";
            sqlString += "	LEFT JOIN WORK.eb_etablissement etch ON ( etch.eb_etablissement_num = d.x_eb_etablissement )";
            sqlString += "	LEFT JOIN WORK.eb_compagnie compch ON ( compch.eb_compagnie_num = etch.x_eb_compagnie )";
            sqlString += "	right join work.eb_tt_tracing trac on trac.x_eb_demande = d.eb_demande_num";
            sqlString += "	left join splitedpsldates spd on (spd.x_eb_tt_tracing = trac.eb_tt_tracing_num)";
            sqlString += "	";
            sqlString += "	where q.status = 4 ";
            sqlString += "	 ";
            sqlString += "	ORDER BY ";
            sqlString += "	d.eb_demande_num DESC;";

            sqlString += " ";

            Query query = em.createNativeQuery(sqlString);
            List<Object[]> resultSet = query.getResultList();

            for (Object[] data: resultSet) {
                Map<String, String[]> map = new LinkedHashMap<String, String[]>();

                map.put("refTransport", new String[]{
                    "Ref transport", (String) data[0]
                });
                map.put("dateCreation", new String[]{
                    "Date creation", data[1] != null ? ((Date) data[1]).toString() : null
                });
                map.put("chargeurSociété", new String[]{
                    "Chargeur: Société", (String) data[2]
                });
                map.put("chargeurEtablissement", new String[]{
                    "Chargeur: Etablissement", (String) data[3]
                });
                map.put("chargeurEmail", new String[]{
                    "Chargeur: Email", (String) data[4]
                });
                map.put("incoterm", new String[]{
                    "Incoterm", (String) data[5]
                });
                map.put("modeTransport", new String[]{
                    "mode transport", (String) data[6]
                });
                map.put("libelleLastPsl", new String[]{
                    "Libelle last PSL", (String) data[7]
                });
                map.put("statutQr", new String[]{
                    "statut QR ", (String) data[8]
                });
                map.put("statutTr", new String[]{
                    "statut TR ", (String) data[9]
                });
                map.put("transporteurSollicitéSociété", new String[]{
                    "Transporteur sollicité : Société", (String) data[10]
                });
                map.put("transporteurSollicitéEtablissement", new String[]{
                    "Transporteur sollicité : Etablissement", (String) data[11]
                });
                map.put("transporteurSollicitéEmail", new String[]{
                    "Transporteur sollicité : Email", (String) data[12]
                });
                map.put("statutQuotation", new String[]{
                    "statut quotation ", (String) data[13]
                });
                map.put("unitRef", new String[]{
                    "Unit ref ", (String) data[14]
                });
                map.put("statusTtDuUnit", new String[]{
                    "Status TT du Unit", (String) data[15]
                });
                map.put("dateLastPsl", new String[]{
                    "Date last psl", data[16] != null ? ((Date) data[16]).toString() : null
                });
                map.put("tracingNum", new String[]{
                    "Tracing Num", "" + (Integer) data[17]
                });
                map.put("pickupActualDate", new String[]{
                    "Pickup Actual date", data[18] != null ? data[18].toString() : null
                });
                map.put("departureActualDate", new String[]{
                    "Departure Actual date", data[19] != null ? data[19].toString() : null
                });
                map.put("arrivalActualDate", new String[]{
                    "Arrival Actual date", data[20] != null ? data[20].toString() : null
                });
                map.put("customsActualDate", new String[]{
                    "Customs Actual date", data[21] != null ? data[21].toString() : null
                });
                map.put("deliveryActualDate", new String[]{
                    "Delivery Actual date", data[22] != null ? data[22].toString() : null
                });

                result.add(map);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Map<String, String[]>> extractIncoherenceStatutTTPSL() {
        List<Map<String, String[]>> result = new ArrayList<Map<String, String[]>>();

        try {
            String sqlString = "";

            sqlString += " with allpsl as ( ";
            sqlString += " 	select distinct on (psl.x_eb_tt_tracing) ";
            sqlString += " 		psl.x_eb_tt_tracing, psl.code_alpha, psl.date_actuelle  ";
            sqlString += " 	from work.eb_tt_psl_app psl ";
            sqlString += " 	where psl.validated is not null and psl.validated = true ";
            sqlString += " 	order by psl.x_eb_tt_tracing desc, psl.code_alpha desc ";
            sqlString += " ) ";
            sqlString += " select  ";
            sqlString += " 	d.eb_demande_num, ";
            sqlString += " 	d.ref_transport, ";
            sqlString += " 	u.email as \"chargeur\", ";
            sqlString += " 	ttr.eb_tt_tracing_num,  ";
            sqlString += " 	psl.date_actuelle, ";
            sqlString += " 	case  ";
            sqlString += "     WHEN ttr.libelle_psl_courant IS NOT NULL THEN libelle_psl_courant ";
            sqlString += "     else 'Waiting for pickup' ";
            sqlString += " 	end \"current status\",   ";
            sqlString += " 	case  ";
            sqlString += " 		when psl.libelle IS NOT NULL THEN psl.libelle ";
            sqlString += " 		else 'Waiting for pickup' ";
            sqlString += " 	end \"expected status\", ";
            sqlString += " 	d.date_creation \"Création QR\" ";
            sqlString += " from work.eb_tt_tracing ttr  ";
            sqlString += " left join allpsl psl on psl.x_eb_tt_tracing = ttr.eb_tt_tracing_num ";
            sqlString += " left join work.eb_demande d on d.eb_demande_num = ttr.x_eb_demande ";
            sqlString += " left join work.eb_user u on u.eb_user_num = d.x_eb_user ";
            sqlString += " where  ";
            sqlString += " 	psl.code_alpha is null  ";
            sqlString += " 	and ttr.code_alpha_psl_courant is not null  ";
            sqlString += " 	or psl.code_alpha <> ttr.code_alpha_psl_courant; ";

            Query query = em.createNativeQuery(sqlString);
            List<Object[]> resultSet = query.getResultList();

            for (Object[] data: resultSet) {
                Map<String, String[]> map = new LinkedHashMap<String, String[]>();

                map.put("demandeNum", new String[]{
                    "Demande Num", "" + (Integer) data[0]
                });
                map.put("refTransport", new String[]{
                    "Ref transport", (String) data[1]
                });
                map.put("chargeurEmail", new String[]{
                    "Chargeur: Email", (String) data[2]
                });
                map.put("tracingNum", new String[]{
                    "Tracing Num", "" + (Integer) data[3]
                });
                map.put("dateActuelle", new String[]{
                    "Date Actuelle", data[4] != null ? data[4].toString() : null
                });
                map.put("currentStatus", new String[]{
                    "Current status", (String) data[5]
                });
                map.put("expectedStatus", new String[]{
                    "Expected status", (String) data[6]
                });
                map.put("créationQr", new String[]{
                    "Création QR", data[7] != null ? data[7].toString() : null
                });

                result.add(map);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Map<String, String[]>> extractIncoherenceStatutTMTT() {
        int i = 1;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Ref transport", "string", "d.ref_transport");
        addColumnMap(
            cols,
            "col_" + (i++),
            "constatée",
            "string",
            "(CASE WHEN d.x_ec_statut = 13 THEN 'Completed' WHEN d.x_ec_statut = 12 THEN 'Ongoing' ELSE 'Waiting for pickup' END)",
            true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "Attendue",
            "string",
            "(CASE WHEN tt.x_ec_statut = 13 THEN 'Completed' WHEN tt.x_ec_statut = 12 THEN 'Ongoing' WHEN tt.x_ec_statut = 11 THEN 'Waiting for pickup' ELSE 'Waiting for pickup (TRACINGS NOT GENERATED)' END)",
            true);

        String sqlString = "";
        sqlString += " FROM work.eb_demande d ";
        sqlString += " LEFT JOIN ( ";
        sqlString += " 		WITH tracings AS ( ";
        sqlString += " 				SELECT tt.x_eb_demande, tt.eb_tt_tracing_num  ";
        sqlString += " 				FROM work.eb_tt_tracing tt  ";
        sqlString += " 			) ";

        // list demande that are not completed
        sqlString += " 		, notvalidated AS (  ";
        sqlString += " 				SELECT tt.x_eb_demande ";
        sqlString += " 				FROM work.eb_tt_psl_app pslapp  ";
        sqlString += " 				LEFT JOIN tracings tt ON tt.eb_tt_tracing_num = pslapp.x_eb_tt_tracing ";
        sqlString += " 				WHERE  ";
        sqlString += " 					(pslapp.validated = false OR pslapp.validated IS NULL)  ";
        sqlString += " 					AND (pslapp.company_psl->>'endPsl')\\:\\:boolean = true ";
        sqlString += " 				GROUP BY tt.x_eb_demande ";
        sqlString += " 		) ";

        // list demande that has at least one psl validated
        sqlString += " 		, validated AS ( ";
        sqlString += " 				SELECT tt.x_eb_demande  ";
        sqlString += " 				FROM work.eb_tt_psl_app pslapp  ";
        sqlString += " 				LEFT JOIN tracings tt ON tt.eb_tt_tracing_num = pslapp.x_eb_tt_tracing ";
        sqlString += " 				WHERE  ";
        sqlString += " 					pslapp.validated = true  ";
        // sqlString += " AND (pslapp.company_psl->>'startPsl')::boolean = true
        // ";
        sqlString += " 				GROUP BY tt.x_eb_demande ";
        sqlString += " 		) ";
        sqlString += " 		SELECT  ";
        sqlString += " 			tt.x_eb_demande eb_demande_num, ";
        sqlString += " 			CASE  ";
        sqlString += " 				WHEN nv.x_eb_demande IS NULL THEN 13  ";
        sqlString += " 				WHEN vv.x_eb_demande IS NOT NULL THEN 12 ";
        sqlString += " 				ELSE 11 ";
        sqlString += " 			END x_ec_statut ";
        sqlString += " 		FROM tracings tt ";
        sqlString += " 		LEFT JOIN notvalidated nv ON nv.x_eb_demande = tt.x_eb_demande		 ";
        sqlString += " 		LEFT JOIN validated vv ON vv.x_eb_demande = tt.x_eb_demande ";
        sqlString += " ) tt on tt.eb_demande_num = d.eb_demande_num ";
        sqlString += " WHERE d.x_ec_statut >= 11 ";
        sqlString += " AND (tt.x_ec_statut IS NULL OR d.x_ec_statut <> tt.x_ec_statut) ";

        return getSqlResult(cols, sqlString);
    }

    private void addColumnMap(
        List<Map<String, String>> cols,
        String name,
        String title,
        String type,
        String colSql,
        boolean... autoAlias) {
        Map<String, String> map = new HashMap<String, String>();

        /**
         * = [0: {name: string, title: string, colSql: string, type:
         * string/date/integer/double}]
         */
        map.put("name", name);
        map.put("title", title);
        map.put("type", type);

        if (autoAlias != null && autoAlias.length > 0 && autoAlias[0]) {
            colSql += " AS " + name;
        }

        map.put("colSql", colSql);

        cols.add(map);
    }

    private List<Map<String, String[]>>
        getSqlResult(List<Map<String, String>> cols, String postSelectSql, String... preSelectSql) {
        List<Map<String, String[]>> result = new ArrayList<Map<String, String[]>>();
        int i = 1;

        try {
            String sqlString = "";

            if (preSelectSql != null && preSelectSql.length > 0) {
                sqlString += preSelectSql[0];
            }

            sqlString += " SELECT ";

            boolean first = true;

            for (Map<String, String> col: cols) {
                if (first) first = false;
                else sqlString += " , ";
                sqlString += col.get("colSql");
            }

            sqlString += postSelectSql;

            Query query = em.createNativeQuery(sqlString);
            List<Object[]> resultSet = query.getResultList();

            for (Object[] data: resultSet) {
                Map<String, String[]> map = new LinkedHashMap<String, String[]>();

                i = 0;

                for (Map<String, String> col: cols) {
                    if (col.get("type").contentEquals("integer")) map.put(col.get("name"), new String[]{
                        col.get("title"), "" + (Integer) data[i]
                    });
                    else if (col.get("type").contentEquals("double")) map.put(col.get("name"), new String[]{
                        col.get("title"), "" + (Double) data[i]
                    });
                    else if (col.get("type").contentEquals("date")) map.put(col.get("name"), new String[]{
                        col.get("title"), data[i] != null ? data[i].toString() : ""
                    });
                    else map.put(col.get("name"), new String[]{
                        col.get("title"), (String) data[i]
                    });

                    i++;
                }

                result.add(map);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<Map<String, String[]>> extractSavings() {
        int i = 1;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Ref transport", "string", "d.ref_transport");
        addColumnMap(cols, "col_" + (i++), "Libelle origin country", "string", "d.libelle_origin_country");
        addColumnMap(cols, "col_" + (i++), "Libelle dest country", "string", "d.libelle_dest_country");
        addColumnMap(cols, "col_" + (i++), "Date creation", "date", "d.date_creation");
        addColumnMap(cols, "col_" + (i++), "Customer Reference", "string", "d.customer_reference");
        addColumnMap(cols, "col_" + (i++), "Unit Reference", "string", "d.units_reference");
        addColumnMap(
            cols,
            "col_" + (i++),
            "DGR",
            "string",
            " CASE WHEN dg IS NOT NULL AND dg THEN 'Oui' ELSE 'Non' END");
        addColumnMap(
            cols,
            "col_" + (i++),
            "Classe goods",
            "string",
            "(select string_agg ( distinct class_good  , ',' ) from work.eb_marchandise m  where m.x_eb_demande =  d.eb_demande_num) as class_good ");
        addColumnMap(
            cols,
            "col_" + (i++),
            "Un",
            "string",
            "(select string_agg ( distinct un  , ',' ) from work.eb_marchandise m  where m.x_eb_demande =  d.eb_demande_num) as un ");
        addColumnMap(cols, "col_" + (i++), "Nb parcel", "double", "d.total_nbr_parcel");
        addColumnMap(cols, "col_" + (i++), "Volume", "double", "d.total_volume");
        addColumnMap(cols, "col_" + (i++), "Total weight", "double", "d.total_weight");
        addColumnMap(cols, "col_" + (i++), "Taxable weight", "double", "d.total_taxable_weight");
        addColumnMap(cols, "col_" + (i++), "Price", "double", "q.price");
        addColumnMap(cols, "col_" + (i++), "Chargeur etablissement", "string", "etch.nom as nometabch");
        addColumnMap(cols, "col_" + (i++), "Chargeur company", "string", "compch.nom as nomcompch");
        addColumnMap(cols, "col_" + (i++), "Chargeur nom", "string", "ch.nom as nomch");
        addColumnMap(cols, "col_" + (i++), "Chargeur prenom", "string", "ch.prenom as prenomch");
        addColumnMap(cols, "col_" + (i++), "Chargeur email", "string", "ch.email as emailch");
        addColumnMap(cols, "col_" + (i++), "Transporteur etablissement", "string", "ettr.nom as nometabtr");
        addColumnMap(cols, "col_" + (i++), "Transporteur company", "string", "comptransporteur.nom as nomcomptr");
        addColumnMap(cols, "col_" + (i++), "Transporteur nom", "string", "tr.nom as nomtr");
        addColumnMap(cols, "col_" + (i++), "Transporteur prenom", "string", "tr.prenom as prenomtr");
        addColumnMap(cols, "col_" + (i++), "Transporteur email", "string", "tr.email as emailtr");
        addColumnMap(
            cols,
            "col_" + (i++),
            "Statut pricing",
            "string",
            "CASE " + "WHEN x_ec_statut = 1 THEN 'In process waiting for quote'  "
                + "WHEN x_ec_statut = 2 THEN 'Waiting for recommendation'  "
                + "WHEN x_ec_statut = 3 THEN 'Waiting for confirmation'  " + "WHEN x_ec_statut = 4 THEN 'Confirmed' "
                + "WHEN x_ec_statut = 6 THEN 'Cancellation of quotation request '  "
                + "WHEN x_ec_statut = 11 THEN 'Waiting for pick up'  "
                + "WHEN x_ec_statut = 12 THEN 'Transport on going' " + "WHEN x_ec_statut = 13 THEN 'Delivery'  "
                + "WHEN x_ec_statut = 14 THEN 'Waiting for transportation request' " + " END as statut_pricing");
        addColumnMap(
            cols,
            "col_" + (i++),
            "Mode transport",
            "string",
            "CASE " + "WHEN d.x_ec_mode_transport = 1 THEN 'Air' " + "WHEN d.x_ec_mode_transport = 2 THEN 'Sea' "
                + "WHEN d.x_ec_mode_transport = 3 THEN 'Road' " + "WHEN d.x_ec_mode_transport = 4 THEN 'Integrator'"
                + "WHEN d.x_ec_mode_transport = 5 THEN 'Rail' " + "ELSE'' || d.x_ec_mode_transport "
                + " END as mode_transport");
        addColumnMap(cols, "col_" + (i++), "Pickup time", "date", "q.pickup_time");
        addColumnMap(cols, "col_" + (i++), "Delivery time", "date", "q.delivery_time");
        addColumnMap(
            cols,
            "col_" + (i++),
            "Statut quotation",
            "string",
            "CASE " + "WHEN q.status = 1 THEN 'Request received by FF' " + "WHEN q.status = 2 THEN 'Quote sent by FF' "
                + "WHEN q.status = 3 THEN 'Recommendation by CT' " + "WHEN q.status = 4 THEN 'Final choice' "
                + "WHEN q.status = 5 THEN 'Not Awarded' " + "END as statut_quotation");

        String sqlString = "";
        sqlString += " FROM ";
        sqlString += " 	WORK.eb_demande_quote  q ";
        sqlString += " 	left JOIN WORK.eb_demande d ON ( q.x_eb_demande = d.eb_demande_num) ";
        sqlString += " 	LEFT JOIN WORK.eb_user ch ON ( ch.eb_user_num = d.x_eb_user ) ";
        sqlString += " 	LEFT JOIN WORK.eb_user tr ON ( tr.eb_user_num = q.x_transporteur )  ";
        sqlString += " 	LEFT JOIN WORK.eb_etablissement etch ON ( etch.eb_etablissement_num = d.x_eb_etablissement ) ";
        sqlString += " 	LEFT JOIN WORK.eb_compagnie compch ON ( compch.eb_compagnie_num = etch.x_eb_compagnie ) ";
        sqlString += " 	LEFT JOIN WORK.eb_etablissement ettr ON ( ettr.eb_etablissement_num = d.x_eb_etablissement_transporteur ) ";
        sqlString += " 	LEFT JOIN WORK.eb_compagnie comptransporteur ON ( comptransporteur.eb_compagnie_num = ettr.x_eb_compagnie ) ";
        sqlString += " 	  ";
        sqlString += " ORDER BY ";
        sqlString += " d.eb_demande_num DESC, d.x_eb_etablissement ; ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDPricing() {
        int i = 1;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Ref transport", "string", "d.ref_transport", true);
        addColumnMap(cols, "col_" + (i++), "Date création", "date", "d.date_creation", true);
        addColumnMap(cols, "col_" + (i++), "Nom compagnie", "string", "compch.nom", true);
        addColumnMap(cols, "col_" + (i++), "Nom etablissement", "string", "etch.nom", true);
        addColumnMap(cols, "col_" + (i++), "Email Chargeur", "string", "ch.email", true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "statut QR",
            "string",
            "CASE when x_ec_cancelled = 1 then 'Cancelled' WHEN x_ec_statut = 1 THEN 'In process waiting for quote'  WHEN x_ec_statut = 2 THEN 'Waiting for recommendation'  WHEN x_ec_statut = 3 THEN 'Waiting for confirmation'  WHEN x_ec_statut >= 4 THEN 'Confirmed' WHEN x_ec_statut = 6 THEN 'Cancellation of quotation request '  WHEN x_ec_statut = 11 THEN 'Waiting for pick up'  WHEN x_ec_statut = 12 THEN 'Transport on going' WHEN x_ec_statut = 13 THEN 'Delivery'  WHEN x_ec_statut = 14 THEN 'Waiting for transportation request' END",
            true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "statut TR",
            "string",
            "CASE when x_ec_cancelled = 1 then 'Cancelled' WHEN x_ec_statut = 11 THEN 'Waiting for pick up' WHEN x_ec_statut = 12 THEN 'Transport on going'  WHEN x_ec_statut = 13 THEN 'Completed' END",
            true);
        addColumnMap(cols, "col_" + (i++), "Transporteur sollicité : Société", "string", "compq.nom", true);
        addColumnMap(cols, "col_" + (i++), "Transporteur sollicité : Etablissement", "string", "etq.nom", true);
        addColumnMap(cols, "col_" + (i++), "Transporteur sollicité : Email", "string", "tr.email", true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "Statut Quotation",
            "string",
            "CASE WHEN q.status = 1 THEN 'Request received by FF'  WHEN q.status = 2 THEN 'Quote sent by FF'  WHEN q.status = 3 THEN 'Recommendation by CT'  WHEN q.status = 4 THEN 'Final choice' else q.status||'' END",
            true);

        String sqlString = "";
        sqlString += " FROM ";
        sqlString += " 	WORK.eb_demande_quote  q ";
        sqlString += " 	left JOIN WORK.eb_demande d ON ( q.x_eb_demande = d.eb_demande_num) ";
        sqlString += " 	LEFT JOIN WORK.eb_user ch ON ( ch.eb_user_num = d.x_eb_user ) ";
        sqlString += " 	LEFT JOIN WORK.eb_user tr ON ( tr.eb_user_num = q.x_transporteur ) ";
        sqlString += " 	LEFT JOIN WORK.eb_etablissement etq ON ( etq.eb_etablissement_num = q.x_eb_etablissement ) ";
        sqlString += " 	LEFT JOIN WORK.eb_compagnie compq ON ( compq.eb_compagnie_num = etq.x_eb_compagnie ) ";
        sqlString += " 	LEFT JOIN WORK.eb_etablissement etch ON ( etch.eb_etablissement_num = d.x_eb_etablissement ) ";
        sqlString += " 	LEFT JOIN WORK.eb_compagnie compch ON ( compch.eb_compagnie_num = etch.x_eb_compagnie ) ";
        sqlString += " 	  ";
        sqlString += " ORDER BY ";
        sqlString += " d.eb_demande_num DESC; ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDUsers() {
        int i = 1;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Email", "string", "u.email", true);
        addColumnMap(cols, "col_" + (i++), "Nom", "string", "u.nom", true);
        addColumnMap(cols, "col_" + (i++), "Prenom", "string", "u.prenom", true);
        addColumnMap(cols, "col_" + (i++), "Nom compagnie", "string", "c.nom", true);
        addColumnMap(cols, "col_" + (i++), "Nom etablissement", "string", "e.nom", true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "Role",
            "string",
            "CASE WHEN u.role=1 THEN 'Chargeur' WHEN u.role=2 THEN 'Transporteur' WHEN u.role=3 THEN 'Control tower' END",
            true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "Admin",
            "string",
            "CASE WHEN u.admin=true THEN 'oui' WHEN u.admin=false THEN 'non' END",
            true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "Super admin",
            "string",
            "CASE WHEN u.super_admin=true THEN 'oui' WHEN u.super_admin=false THEN 'non' END",
            true);
        addColumnMap(cols, "col_" + (i++), "Config email", "string", "list_params_mail_str", true);

        String sqlString = "";

        sqlString += " FROM work.eb_user u  ";
        sqlString += " left join work.eb_etablissement e on(u.x_eb_etablissement = e.eb_etablissement_num)  ";
        sqlString += " left join work.eb_compagnie c on(u.x_eb_compagnie = c.eb_compagnie_num); ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDAccessRight() {
        int i = 1;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Email", "string", "u.email", true);
        addColumnMap(cols, "col_" + (i++), "Module", "string", "mm.libelle", true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "Access right",
            "string",
            "case  when um.access_right = 2 then 'Non visible' when um.access_right = 1 then 'Visible' when um.access_right = 0 then 'Contribution' end",
            true);

        String sqlString = "";
        sqlString += " FROM ";
        sqlString += " 	WORK.eb_user_module um ";
        sqlString += " 	LEFT JOIN WORK.ec_module mm ON mm.ec_module_num = um.x_ec_module ";
        sqlString += " 	LEFT JOIN WORK.eb_user u ON u.eb_user_num = um.x_eb_user ";
        sqlString += " order by email, mm.libelle, um.access_right; ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDAdress() {
        int i = 1;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Airport", "string", "airport", true);
        addColumnMap(cols, "col_" + (i++), "City", "string", "city", true);
        addColumnMap(cols, "col_" + (i++), "Company", "string", "company", true);
        addColumnMap(cols, "col_" + (i++), "Date d'ajout", "date", "date_ajout", true);
        addColumnMap(cols, "col_" + (i++), "Email", "string", "email", true);
        addColumnMap(cols, "col_" + (i++), "Phone", "string", "phone", true);
        addColumnMap(cols, "col_" + (i++), "Reference", "string", "reference", true);
        addColumnMap(cols, "col_" + (i++), "Street", "string", "street", true);
        addColumnMap(cols, "col_" + (i++), "Zip code", "string", "zip_code", true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "Country",
            "string",
            "( SELECT code FROM WORK.ec_country WHERE ec_country_num = adr.x_ec_country )",
            true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "Etablissement",
            "string",
            "( SELECT nom FROM WORK.eb_etablissement WHERE eb_etablissement_num = adr.x_eb_etablissement )",
            true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "User",
            "string",
            "( SELECT email FROM WORK.eb_user WHERE eb_user_num = adr.x_eb_user )",
            true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "Compagnie",
            "string",
            "( SELECT nom FROM WORK.eb_compagnie WHERE eb_compagnie_num = adr.x_eb_company )",
            true);
        addColumnMap(cols, "col_" + (i++), "Commentaire", "string", "commentaire", true);
        addColumnMap(cols, "col_" + (i++), "Opening hours", "string", "opening_hours_free_text", true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "Etablissement adresse",
            "string",
            "( SELECT nom FROM WORK.eb_etablissement WHERE eb_etablissement_num = adr.x_eb_etablissement_adresse )",
            true);
        addColumnMap(
            cols,
            "col_" + (i++),
            "Zone",
            "string",
            "( SELECT reference FROM WORK.eb_pl_zone WHERE eb_zone_num = adr.x_eb_zone ) ",
            true);

        String sqlString = "";
        sqlString += " FROM ";
        sqlString += " 	WORK.eb_adresse adr; ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDLabels() {
        int i = 1;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Etablissement", "string", "et.nom", true);
        addColumnMap(cols, "col_" + (i++), "Categorie", "string", "cc.libelle", true);
        addColumnMap(cols, "col_" + (i++), "Label", "string", "ll.libelle", true);

        String sqlString = "";
        sqlString += " FROM ";
        sqlString += " 	WORK.eb_label ll ";
        sqlString += " 	LEFT JOIN WORK.eb_categorie cc ON cc.eb_categorie_num = ll.x_eb_categorie ";
        sqlString += " 	LEFT JOIN WORK.eb_etablissement et ON et.eb_etablissement_num = cc.x_eb_etablissement  ";
        sqlString += " ORDER BY ";
        sqlString += " 	et.nom, ";
        sqlString += " 	cc.libelle, ";
        sqlString += " 	ll.libelle ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDCategories() {
        int i = 1;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Etablissement", "string", "et.nom", true);
        addColumnMap(cols, "col_" + (i++), "Libelle", "string", "cc.libelle", true);

        String sqlString = "";
        sqlString += " FROM WORK.eb_categorie cc ";
        sqlString += " 	LEFT JOIN WORK.eb_etablissement et ON et.eb_etablissement_num = cc.x_eb_etablissement ";
        sqlString += " order by et.nom, cc.libelle; ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDChampsParam() {
        int i = 1;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Compagnie", "string", "cmp.nom", true);
        addColumnMap(cols, "col_" + (i++), "Email", "string", "u.email", true);
        addColumnMap(cols, "col_" + (i++), "Fields", "string", "cf.fields ", true);

        String sqlString = "";
        sqlString += " FROM WORK.eb_custom_field cf ";
        sqlString += " 	LEFT JOIN WORK.eb_compagnie cmp ON cmp.eb_compagnie_num = cf.x_eb_compagnie ";
        sqlString += " 	LEFT JOIN WORK.eb_user u ON u.eb_user_num = cf.x_eb_user ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDCurrencies() {
        int i = 1;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Code", "string", "code", true);
        addColumnMap(cols, "col_" + (i++), "Libelle", "string", "libelle", true);
        addColumnMap(cols, "col_" + (i++), "Symbole", "string", "symbol", true);

        String sqlString = "";
        sqlString += " FROM work.ec_currency ec ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDDocuments() {
        int i = 1;
        String str;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Date d'upload", "date", "attachment_date", true);
        addColumnMap(cols, "col_" + (i++), "file", "string", "'https://tcd.mytower.fr//' || chemin", true);
        // addColumnMap(cols, "col_"+(i++), "Numero categorie", "integer",
        // "id_categorie", true);
        addColumnMap(cols, "col_" + (i++), "Categorie", "string", "dd.nom", true);
        addColumnMap(cols, "col_" + (i++), "Numero de dossier", "integer", "x_eb_demande", true);
        str = " case  ";
        str += "     when ff.module = 1 then 'Pricing' ";
        str += "     when ff.module = 2 then 'Transport management' ";
        str += "     when ff.module = 3 then 'Quality management' ";
        str += "     when ff.module = 4 then 'Compliance Matrix' ";
        str += "     when ff.module = 5 then 'Track & Trace' ";
        str += "     else 'Pricing' ";
        str += " end";
        addColumnMap(cols, "col_" + (i++), "Module", "string", str, true);

        String sqlString = "";
        sqlString += " FROM ";
        sqlString += " 	WORK.eb_demande_fichiers_joint ff ";
        sqlString += " 	LEFT JOIN WORK.eb_demande d ON d.eb_demande_num = ff.x_eb_demande ";
        sqlString += "  LEFT JOIN WORK.eb_type_documents dd ON dd.code = ff.id_categorie AND dd.x_eb_compagnie = d.x_eb_compagnie  ";
        sqlString += " ORDER BY ";
        sqlString += " 	x_eb_demande; ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDIncoterms() {
        int i = 1;
        String str;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Ref transport", "string", "d.ref_transport", true);

        str = " case  ";
        str += "     when x_ec_mode_transport = 1 then 'Air'  ";
        str += "     when x_ec_mode_transport = 2 then 'Sea' ";
        str += "     when x_ec_mode_transport = 3 then 'Road' ";
        str += "     when x_ec_mode_transport = 4 then 'Integrator' ";
        str += " else null ";
        str += " end ";
        addColumnMap(cols, "col_" + (i++), "Mode transport", "string", str, true);

        addColumnMap(
            cols,
            "col_" + (i++),
            "Incoterm",
            "string",
            "replace((d.x_eb_schema_psl->'xEcIncotermLibelle')\\:\\:text, '\"', '')",
            true);

        str = " case  ";
        str += "     when d.x_ec_statut is null or  d.x_ec_statut = 11 then 'Waiting for pickup' ";
        str += "     when d.x_ec_statut = 12 then 'Transport on going' ";
        str += "     when d.x_ec_statut = 13 then 'Completed' ";
        str += "     else null ";
        str += " end ";
        addColumnMap(cols, "col_" + (i++), "Statut", "string", str, true);

        String sqlString = "";
        sqlString += " from work.eb_demande d  ";
        sqlString += " where d.x_ec_statut >= 11 ";
        sqlString += " order by d.eb_demande_num ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDCommunity() {
        int i = 1;
        String str;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Email host", "string", "uh.email", true);
        addColumnMap(cols, "col_" + (i++), "Etablissement host", "string", "eh.nom", true);
        addColumnMap(cols, "col_" + (i++), "Email guest", "string", "ug.email", true);
        addColumnMap(cols, "col_" + (i++), "Etablissement guest", "string", "eg.nom", true);

        str = " case  ";
        str += "     when r.type_relation = 0 then 'Transporteur' ";
        str += "     when r.type_relation = 1 then 'Broker' ";
        str += "     when r.type_relation = 2 then 'Transporteur / broker' ";
        str += "     when r.type_relation = 3 then 'CT' ";
        str += "     else NULL ";
        str += " end ";
        addColumnMap(cols, "col_" + (i++), "Type relation", "string", str, true);

        str = " case  ";
        str += "     when r.etat = 0 then 'En Cours' ";
        str += "     when r.etat = 1 then 'Actif' ";
        str += "     when r.etat = 2 then 'Ignoré' ";
        str += "     when r.etat = 3 then 'Annulé' ";
        str += "     when r.etat = 4 then 'Blacklist' ";
        str += " end ";
        addColumnMap(cols, "col_" + (i++), "Statut", "string", str, true);

        String sqlString = "";
        sqlString += " FROM ";
        sqlString += " 	WORK.eb_relation r ";
        sqlString += " 	LEFT JOIN WORK.eb_user uh ON r.x_host = uh.eb_user_num ";
        sqlString += " 	LEFT JOIN WORK.eb_etablissement eh ON r.x_eb_etablissement_host = eh.eb_etablissement_num ";
        sqlString += " 	LEFT JOIN WORK.eb_user ug ON r.x_guest = ug.eb_user_num ";
        sqlString += " 	LEFT JOIN WORK.eb_etablissement eg ON r.x_eb_etablissement = eg.eb_etablissement_num  ";
        sqlString += " ORDER BY ";
        sqlString += " 	uh.email, ";
        sqlString += " 	r.etat, ";
        sqlString += " 	r.type_relation; ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDMasques() {
        int i = 1;
        String str;

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        str = " case  ";
        str += "     when sf.eb_comp_num = 2 then 'TRANSPORT_MANAGEMENT' ";
        str += "     when sf.eb_comp_num = 4 then 'COMPLIANCE_MATRIX' ";
        str += "     when sf.eb_comp_num = 3 then 'QUALITY_MANAGEMENT' ";
        str += "     when sf.eb_comp_num = 8 then 'FREIGHT_ANALYTICS' ";
        str += "     when sf.eb_comp_num = 12 then 'SETTINGS_ZONE' ";
        str += "     when sf.eb_comp_num = 13 then 'SETTINGS_TRANCHE' ";
        str += "     when sf.eb_comp_num = 14 then 'SETTINGS_GRILLE' ";
        str += "     when sf.eb_comp_num = 15 then 'SETTINGS_TRANSPORT_PLAN' ";
        str += "     when sf.eb_comp_num = 17 then 'SETTINGS_ENTREPOT' ";
        str += "     when sf.eb_comp_num = 16 then 'USER_MANAGEMENT' ";
        str += "     when sf.eb_comp_num = 2 then 'BOOKING' ";
        str += "     when sf.eb_comp_num = 1 then 'PRICING' ";
        str += "     when sf.eb_comp_num = 3 then 'QUALITY' ";
        str += "     when sf.eb_comp_num = 4 then 'FREIGHT_AUDIT' ";
        str += "     when sf.eb_comp_num = 5 then 'TRACK' ";
        str += "     when sf.eb_comp_num = 9 then 'CUSTOM' ";
        str += "     when sf.eb_comp_num = 11 then 'ORDER' ";
        str += "     when sf.eb_comp_num = 12 then 'DELIVERY' ";
        str += "     when sf.eb_comp_num = 18 then 'RECEIPT_SCHEDULING' ";
        str += "     when sf.eb_comp_num = 19 then 'SHIP_TO_MATRIX' ";
        str += " end ";
        addColumnMap(cols, "col_" + (i++), "Composant", "string", str, true);

        addColumnMap(cols, "col_" + (i++), "Module", "string", "m.libelle", true);
        addColumnMap(cols, "col_" + (i++), "Form name", "string", "form_name", true);
        addColumnMap(cols, "col_" + (i++), "Form values", "string", "form_values", true);
        addColumnMap(cols, "col_" + (i++), "Email", "string", "u.email ", true);

        String sqlString = "";
        sqlString += " FROM ";
        sqlString += " 	WORK.eb_saved_form sf ";
        sqlString += " 	LEFT JOIN WORK.ec_module M ON M.ec_module_num = sf.eb_module_num ";
        sqlString += " 	LEFT JOIN WORK.eb_user u ON u.eb_user_num = sf.x_eb_user ";

        return getSqlResult(cols, sqlString);
    }

    @Override
    public List<Map<String, String[]>> extractGDDatesPsl() {
        int i = 1;
        String preSqlStr, str;

        preSqlStr = " WITH select_flatcustomfields AS ( ";
        preSqlStr += " 	SELECT ";
        preSqlStr += " 		d.eb_demande_num, ";
        preSqlStr += " 		d.custom_fields, ";
        preSqlStr += " 		jsonb_array_elements ( d.custom_fields \\:\\: jsonb ) AS flatcustomfields  ";
        preSqlStr += " 	FROM ";
        preSqlStr += " 		WORK.eb_demande d  ";
        preSqlStr += " 	WHERE ";
        preSqlStr += " 		d.custom_fields IS NOT NULL ";
        preSqlStr += " 		 ";
        preSqlStr += " ), extractquery AS ( ";
        preSqlStr += " 	SELECT ";
        preSqlStr += " 		sub.eb_demande_num, ";
        preSqlStr += " 		( sub.flatcustomfields ) -> 'name' AS NAME, ";
        preSqlStr += " 		( sub.flatcustomfields ) -> 'value' AS  ";
        preSqlStr += " 	VALUE ";
        preSqlStr += " 		 ";
        preSqlStr += " 	FROM ";
        preSqlStr += " 		select_flatcustomfields sub  ";
        preSqlStr += " 		 ";
        preSqlStr += " ), converted AS ( ";
        preSqlStr += " 		SELECT ";
        preSqlStr += " 			q.eb_demande_num, ";
        preSqlStr += " 			REPLACE ( q.NAME \\:\\: TEXT, '\"', '' ) AS NAME, ";
        preSqlStr += " 			REPLACE ( q.VALUE \\:\\: TEXT, '\"', '' ) AS  ";
        preSqlStr += " 		VALUE ";
        preSqlStr += " 			 ";
        preSqlStr += " 		FROM ";
        preSqlStr += " 			extractquery q  ";
        preSqlStr += " 			 ";
        preSqlStr += " ), customfield as ( ";
        preSqlStr += " 		SELECT ";
        preSqlStr += " 				* ";
        preSqlStr += " 		FROM ";
        preSqlStr += " 		converted cv where cv.name = 'field2' ";
        preSqlStr += " )  ";

        preSqlStr += " , pslapp as ( ";
        preSqlStr += " 		select  ";
        preSqlStr += " 		ttpslapp.eb_tt_psl_app_num, ttpslapp.code_alpha, ttpslapp.date_actuelle, ttpslapp.date_estimee, ttpslapp.date_negotiation, ttpslapp.date_creation, ttpslapp.x_eb_tt_tracing, ttpslapp.code_alpha acode  ";
        preSqlStr += " 		from work.eb_tt_psl_app ttpslapp order  by ttpslapp.eb_tt_psl_app_num asc ";
        preSqlStr += " 	) ";
        preSqlStr += " 	 ";
        preSqlStr += " 	, grouppsl as (	 ";
        preSqlStr += " 	select 	 ";
        preSqlStr += " 		date_creation,  ";
        preSqlStr += " 		x_eb_tt_tracing,  ";
        preSqlStr += " 		'{\"x_eb_tt_tracing\":' || x_eb_tt_tracing\\:\\:text || ',' || string_agg ( '\"' || lower(acode) || '\":\"' || cast(date_actuelle as TEXT)||'\"' , ',' ) || '}' as date_actuelle,  ";
        preSqlStr += " 		'{\"x_eb_tt_tracing\":' || x_eb_tt_tracing\\:\\:text || ',' || string_agg ( '\"' || lower(acode) || '\":\"' || cast(date_estimee as TEXT)||'\"' , ',' ) || '}' as date_estimee,  ";
        preSqlStr += " 		'{\"x_eb_tt_tracing\":' || x_eb_tt_tracing\\:\\:text || ',' || string_agg ( '\"' || lower(acode) || '\":\"' || cast(date_negotiation as TEXT)||'\"' , ',' ) || '}' as date_negotiation, ";
        preSqlStr += " 		(select config_psl  from  work.eb_tt_tracing tt  where tt.eb_tt_tracing_num = pt.x_eb_tt_tracing) as orgconfig  ";
        preSqlStr += " 	from pslapp pt  ";
        preSqlStr += " 	group by x_eb_tt_tracing, date_creation ";
        preSqlStr += " ) ";

        preSqlStr += " , splitedpsldates as ( ";
        preSqlStr += " SELECT  ";
        preSqlStr += " 	t.x_eb_tt_tracing,  ";
        preSqlStr += " 	 ";
        preSqlStr += " 	da.pic as actual_pic, ";
        preSqlStr += " 	da.dep as actual_dep, ";
        preSqlStr += " 	da.arr as actual_arr, ";
        preSqlStr += " 	da.cus as actual_cus, ";
        preSqlStr += " 	da.del as actual_del, ";

        preSqlStr += " 	de.pic as estimated_pic, ";
        preSqlStr += " 	de.dep as estimated_dep, ";
        preSqlStr += " 	de.arr as estimated_arr, ";
        preSqlStr += " 	de.cus as estimated_cus, ";
        preSqlStr += " 	de.del as estimated_del, ";

        preSqlStr += " 	dn.pic as negotiated_pic, ";
        preSqlStr += " 	dn.dep as negotiated_dep, ";
        preSqlStr += " 	dn.arr as negotiated_arr, ";
        preSqlStr += " 	dn.cus as negotiated_cus, ";
        preSqlStr += " 	dn.del as negotiated_del ";
        preSqlStr += " 	 ";
        preSqlStr += " FROM    ";
        preSqlStr += " 	grouppsl t ";
        preSqlStr += "   left join json_to_record(cast(t.date_actuelle AS json)) da(x_eb_tt_tracing int, PIC text, DEP text, ARR text, CUS text, DEL text) on (da.x_eb_tt_tracing = t.x_eb_tt_tracing) ";
        preSqlStr += " 	left join json_to_record(cast(t.date_estimee AS json)) de(x_eb_tt_tracing int, PIC text, DEP text, ARR text, CUS text, DEL text) on (de.x_eb_tt_tracing = t.x_eb_tt_tracing) ";
        preSqlStr += " 	left join json_to_record(cast(t.date_negotiation AS json)) dn(x_eb_tt_tracing int, PIC text, DEP text, ARR text, CUS text, DEL text) on (dn.x_eb_tt_tracing = t.x_eb_tt_tracing) ";
        preSqlStr += " ) ";

        List<Map<String, String>> cols = new ArrayList<Map<String, String>>();
        addColumnMap(cols, "col_" + (i++), "Ref transport", "string", "d.ref_transport", true);
        addColumnMap(cols, "col_" + (i++), "Customer reference", "string", "tt.customer_reference", true);
        addColumnMap(cols, "col_" + (i++), "Units reference", "string", "d.units_reference", true);
        addColumnMap(cols, "col_" + (i++), "Ville d'origine", "string", "po.city", true);
        addColumnMap(cols, "col_" + (i++), "Pays d'origine", "string", "cno.libelle", true);
        addColumnMap(cols, "col_" + (i++), "Ville de destination", "string", "pd.city", true);
        addColumnMap(cols, "col_" + (i++), "Pays de destination", "string", "cnd.libelle", true);
        addColumnMap(cols, "col_" + (i++), "Vendor PO", "string", "cf.value", true);
        str = " CASE  ";
        str += "     WHEN d.x_ec_mode_transport = 1 THEN 'Air'  ";
        str += "     WHEN d.x_ec_mode_transport = 2 THEN 'Sea'  ";
        str += "     WHEN d.x_ec_mode_transport = 3 THEN 'Road'  ";
        str += "     WHEN d.x_ec_mode_transport = 4 THEN 'Integrator' ELSE'' || d.x_ec_mode_transport  ";
        str += " END ";
        addColumnMap(cols, "col_" + (i++), "mode transport", "string", str, true);
        addColumnMap(cols, "col_" + (i++), "Incoterm", "string", "d.x_eb_schema_psl->>'xEcIncotermLibelle'", true);

        addColumnMap(cols, "col_" + (i++), "Pickup Actual date", "date", "spd.actual_pic", true);
        addColumnMap(cols, "col_" + (i++), "Departure Actual date", "date", "spd.actual_dep", true);
        addColumnMap(cols, "col_" + (i++), "Arrival Actual date", "date", "spd.actual_arr", true);
        addColumnMap(cols, "col_" + (i++), "Customs Actual date", "date", "spd.actual_cus", true);
        addColumnMap(cols, "col_" + (i++), "Delivery Actual date", "date", "spd.actual_del", true);
        addColumnMap(cols, "col_" + (i++), "Pickup Estimated date", "date", "spd.estimated_pic", true);
        addColumnMap(cols, "col_" + (i++), "Departure Estimated date", "date", "spd.estimated_dep", true);
        addColumnMap(cols, "col_" + (i++), "Arrival Estimated date", "date", "spd.estimated_arr", true);
        addColumnMap(cols, "col_" + (i++), "Customs Estimated date", "date", "spd.estimated_cus", true);
        addColumnMap(cols, "col_" + (i++), "Delivery Estimated date", "date", "spd.estimated_del", true);
        addColumnMap(cols, "col_" + (i++), "Pickup Negotiated date", "date", "spd.negotiated_pic", true);
        addColumnMap(cols, "col_" + (i++), "Departure Negotiated date", "date", "spd.negotiated_dep", true);
        addColumnMap(cols, "col_" + (i++), "Arrival Negotiated date", "date", "spd.negotiated_arr", true);
        addColumnMap(cols, "col_" + (i++), "Customs Negotiated date", "date", "spd.negotiated_cus", true);
        addColumnMap(cols, "col_" + (i++), "Delivery Negotiated date", "date", "spd.negotiated_del", true);

        String sqlString = "";

        sqlString += " FROM ";
        sqlString += " 	WORK.eb_tt_tracing tt ";
        sqlString += " 	LEFT JOIN WORK.eb_demande d ON ( d.eb_demande_num = tt.x_eb_demande ) ";
        sqlString += " 	LEFT join work.eb_party po on (d.eb_party_origin = po.eb_party_num) ";
        sqlString += " 	left join work.eb_party pd on (d.eb_party_dest = pd.eb_party_num) ";
        sqlString += " 	left join work.ec_country cno on (cno.ec_country_num = po.x_ec_country) ";
        sqlString += " 	left join work.ec_country cnd on (cnd.ec_country_num = pd.x_ec_country) ";
        sqlString += " 	left join customfield cf on (cf.eb_demande_num = tt.x_eb_demande) ";
        sqlString += " 	left join splitedpsldates spd on (spd.x_eb_tt_tracing = tt.eb_tt_tracing_num); ";

        return getSqlResult(cols, sqlString, preSqlStr);
    }
}
