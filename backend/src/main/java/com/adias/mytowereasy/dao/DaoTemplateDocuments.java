package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbTemplateDocuments;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoTemplateDocuments {
    public List<EbTemplateDocuments> searchEbTemplateDocuments(SearchCriteria criteria);

    public Long countListEbTemplateDocuments(SearchCriteria criteria);
}
