package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.BoutonActionDao;
import com.adias.mytowereasy.model.EbBoutonAction;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbBoutonAction;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class BoutonActionDaoImpl extends Dao implements BoutonActionDao {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbBoutonAction qEbBoutonAction = QEbBoutonAction.ebBoutonAction;

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbBoutonAction> listBoutonAction(SearchCriteria criteria) {
        List<EbBoutonAction> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbBoutonAction> query = new JPAQuery<>(em);

            query
                .select(
                    QEbBoutonAction
                        .create(
                            qEbBoutonAction.ebBoutonActionNum,
                            qEbBoutonAction.type,
                            qEbBoutonAction.module,
                            qEbBoutonAction.active,
                            qEbBoutonAction.libelle,
                            qEbBoutonAction.icon,
                            qEbBoutonAction.ordre,
                            qEbBoutonAction.color,
                            qEbBoutonAction.action));

            query = getFlagGlobalWhere(query, where, criteria);
            query.from(qEbBoutonAction);
            query.where(where);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbBoutonAction, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbBoutonAction.libelle.asc());
            }

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long countListBoutonAction(SearchCriteria criteria) {
        Long result = 0L;

        try {
            JPAQuery<EbBoutonAction> query = new JPAQuery<EbBoutonAction>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getFlagGlobalWhere(query, where, criteria);
            query.from(qEbBoutonAction);
            query.where(where);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getFlagGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        if (connectedUser.getEbCompagnie() != null && connectedUser.getEbCompagnie().getEbCompagnieNum() != null) {
            where.and(qEbBoutonAction.user().ebUserNum.eq(connectedUser.getEbUserNum()));
        }

        if (criteria.isBoutonAction()) {
            where.and(qEbBoutonAction.active.eq(criteria.isBoutonAction()));
        }

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qEbBoutonAction.type.toLowerCase().contains(term),
                    qEbBoutonAction.libelle.toLowerCase().contains(term));
        }

        return query;
    }
}
