package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.airbus.model.QEbUserProfile;
import com.adias.mytowereasy.dao.DaoAcl;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.utils.search.SearchCriteriaACL;


@Component
@Transactional
public class DaoAclImpl implements DaoAcl {

    @PersistenceContext
    EntityManager em;

    QEcAclRule qAclRule = QEcAclRule.ecAclRule;
    QEbAclRelation qAclRelation = QEbAclRelation.ebAclRelation;
    QEbUser qUser = QEbUser.ebUser;
    QEbUserProfile qUserProfile = QEbUserProfile.ebUserProfile;

    /**
     * List rule with only fields required for session rights loading
     */
    @Override
    public List<EcAclRule> listRuleForSession(SearchCriteriaACL criteria) {
        List<EcAclRule> resultat = new ArrayList<>();

        try {
            JPAQuery<EcAclRule> query = new JPAQuery<>(em);

            query.select(QEcAclRule.create(qAclRule.ecAclRuleNum, qAclRule.type, qAclRule.code, qAclRule.defaultValue));
            query.distinct().from(qAclRule);

            resultat = query.fetch();
            return resultat;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    /**
     * List relations with only fields required for session rights loading
     */
    @Override
    public List<EbAclRelation> listRelationForSession(SearchCriteriaACL criteria) {
        List<EbAclRelation> resultat = new ArrayList<>();
        BooleanBuilder where = new BooleanBuilder();

        try {
            JPAQuery<EbAclRelation> query = new JPAQuery<>(em);

            query
                .select(
                    QEbAclRelation
                        .create(
                            qAclRelation.ebAclRelationNum,
                            qAclRelation.value,
                            qAclRule.ecAclRuleNum,
                            qAclRule.code));
            query.distinct().from(qAclRelation);
            query.leftJoin(qAclRelation.rule(), qAclRule);

            if (criteria.getEbUserProfileNum() != null) {
                where.and(qUserProfile.ebUserProfileNum.eq(criteria.getEbUserProfileNum()));
                query.leftJoin(qAclRelation.profile(), qUserProfile);
            }

            if (criteria.getEbUserNum() != null) {
                where.and(qUser.ebUserNum.eq(criteria.getEbUserNum()));
                query.leftJoin(qAclRelation.user(), qUser);
            }

            query.where(where);

            resultat = query.fetch();
            return resultat;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }
}
