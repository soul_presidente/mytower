package com.adias.mytowereasy.dao;

import java.util.List;
import java.util.Map;


public interface DaoFileDownloader {
    List<Map<String, String[]>> extractIncoherenceStatutTMTT();

    List<Map<String, String[]>> extractIncoherenceStatutTTPSL();

    List<Map<String, String[]>> extractSavings();

    List<Map<String, String[]>> extractGDUsers();

    List<Map<String, String[]>> extractGDAccessRight();

    List<Map<String, String[]>> extractGDAdress();

    List<Map<String, String[]>> extractGDMasques();

    List<Map<String, String[]>> extractGDIncoterms();

    List<Map<String, String[]>> extractGDCurrencies();

    List<Map<String, String[]>> extractGDCommunity();

    List<Map<String, String[]>> extractGDChampsParam();

    List<Map<String, String[]>> extractGDLabels();

    List<Map<String, String[]>> extractGDCategories();

    List<Map<String, String[]>> extractGDPricing();

    List<Map<String, String[]>> extractGDDocuments();

    List<Map<String, String[]>> extractGDDatesPsl();

    List<Map<String, String[]>> extractTM();
}
