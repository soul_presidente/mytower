package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbTypeGoods;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoTypeGoods {
    public List<EbTypeGoods> list(SearchCriteria criteria);

    public Long listCount(SearchCriteria criteria);
}
