package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoPricingTracking;
import com.adias.mytowereasy.dock.model.QEbEntrepot;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.QEbCostCenter;
import com.adias.mytowereasy.model.QEbDemande;
import com.adias.mytowereasy.model.QEbEtablissement;
import com.adias.mytowereasy.model.QEbParty;
import com.adias.mytowereasy.model.QEbUser;
import com.adias.mytowereasy.model.QExEbDemandeTransporteur;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Component
@Transactional
public class DaoPricingTrackingImpl implements DaoPricingTracking {
    @PersistenceContext
    EntityManager em;

    QEbUser qChargeur = new QEbUser("qChargeur");
    QEbUser qTransporteur = new QEbUser("transporteur");
    QEbDemande qEbDemande = QEbDemande.ebDemande;
    QEbCostCenter qNamedCostCenter = new QEbCostCenter("costCenter");
    QEbCompagnie qCompagnieChargeur = new QEbCompagnie("qCompagnieChargeur");
    QEbUser qEbUserCt = new QEbUser("qEbUserCt");
    QExEbDemandeTransporteur qExDemandeTransporteur = QExEbDemandeTransporteur.exEbDemandeTransporteur;
    QEbCompagnie qChargeurComp = new QEbCompagnie("qChargeurComp");
    QEbParty qPartyDest = new QEbParty("partyDest");
    QEbParty qPartyOrigin = new QEbParty("partyOrigin");
    QEbCompagnie qCompagnie = new QEbCompagnie("compagnie");
    QEbEtablissement qEtablissement = new QEbEtablissement("etablissement");
    QEbEtablissement qChargeurEtab = new QEbEtablissement("qChargeurEtab");
    QEbEntrepot qEbEntrepot = new QEbEntrepot("qEbEntrepot");
    QEbUser qUserOrigin = new QEbUser("qUserOrigin");
    QEbEtablissement qUserOriginEtab = new QEbEtablissement("qUserOriginEtab");
    QEbUser qUserDest = new QEbUser("qUserDest");
    QEbEtablissement qUserDestEtab = new QEbEtablissement("qUserDestEtab");
    QEbUser qUserOriginCB = new QEbUser("qUserOriginCB");
    QEbEtablissement qUserOriginCBEtab = new QEbEtablissement("qUserOriginCBEtab");
    QEbUser qUserDestCB = new QEbUser("qUserDestCB");
    QEbEtablissement qUserDestCBEtab = new QEbEtablissement("qUserDestCBEtab");

    QEbDemande qEbDemandeMultiSegmentObject = new QEbDemande("qEbDemandeMultiSegmentObject");

    @Override
    public List<EbDemande> getListEbDemande(SearchCriteriaPricingBooking criterias) throws Exception {
        // get all demandes for transports from edi cron job
        List<EbDemande> resultat = new ArrayList<>();

        StringExpression stringOrderWithExpression = qEbDemande.refTransport;
        NumberExpression<Integer> numberOrderWithExpression = qEbDemande.xEcStatut;
        EbUser connectedUser = new EbUser();

        BooleanBuilder where = new BooleanBuilder();

        try {
            JPAQuery<EbDemande> query = new JPAQuery<EbDemande>(em);

            query
                .select(
                    QEbDemande
                        .create(
                            qEbDemande.ebDemandeNum,
                            qEbDemande.refTransport,
                            qNamedCostCenter.libelle,
                            qEbDemande.customerReference,
                            qEbDemande.partNumber,
                            qEbDemande.serialNumber,
                            qEbDemande.shippingType,
                            qEbDemande.orderNumber,
                            qEbDemande.totalWeight,
                            qEbDemande.xEcModeTransport,
                            qEbDemande.dateCreation,
                            qEbDemande.updateDate,
                            qEbDemande.xEcStatut,
                            qEbDemande.xEcTypeDemande,
                            qEbDemande.xEbTypeRequest,
                            qEbDemande.typeRequestLibelle,
                            qEbDemande.libelleDestCountry,
                            qEbDemande.libelleOriginCountry,
                            qEbDemande.xEbTypeTransport,
                            qEbDemande.xEcTypeCustomBroker,
                            qEbDemande.nbrDoc,
                            qEbDemande.flagRecommendation,
                            qEbDemande.flagDocumentCustoms,
                            qEbDemande.flagDocumentTransporteur,
                            qEbDemande.flagTransportInformation,
                            qEbDemande.flagCustomsInformation,
                            qEbDemande.flagRequestCustomsBroker,
                            qEbDemande.xEbTransporteur,
                            qEbDemande.customFields,
                            qChargeur.ebUserNum,
                            qChargeur.nom,
                            qChargeur.prenom,
                            qChargeur.email,
                            qCompagnieChargeur.ebCompagnieNum,
                            qCompagnieChargeur.nom,
                            qCompagnieChargeur.code,
                            qEbDemande.listCategories,
                            qEbDemande.datePickupTM,
                            qEbDemande.unitsReference,
                            qEbDemande.xecCurrencyInvoice().ecCurrencyNum,
                            qEbDemande.confirmPickup,
                            qEbDemande.confirmDelivery,
                            qEbDemande.dateOfGoodsAvailability,
                            qEbDemande.finalDelivery,
                            qEbDemande.ebPartyOrigin().company,
                            qEbDemande.ebPartyDest().company,
                            stringOrderWithExpression.as("stringOrderExpression"),
                            numberOrderWithExpression.as("numberOrderExpression"),
                            qEbDemande.xEcCancelled,
                            qEbDemande.libelleOriginCity,
                            qEbDemande.libelleDestCity,
                            qEbDemande.xEbTypeFluxNum,
                            qEbDemande.xEbTypeFluxCode,
                            qEbDemande.xEbTypeFluxDesignation,
                            qEbDemande.xEbIncotermNum,
                            qEbDemande.xEcIncotermLibelle,
                            qEbDemande.numAwbBol,
                            qEbDemande.carrierUniqRefNum,
                            qEbDemande.libelleLastPsl,
                            qEbDemande.codeAlphaLastPsl,
                            qEbDemande.dateLastPsl,
                            qEbDemande.codeAlphaPslCourant,
                            qEbDemande.xEcNature,
                            qEbDemande.ebDemandeMasterObject().ebDemandeNum,
                            qEbDemande.ebDemandeMasterObject().refTransport,
                            qEbDemande.listFlag,
                            Expressions.asSimple(connectedUser),
                            Expressions.asNumber(criterias.getModule()),
                            qEbDemande.listPropRdvPk,
                            qEbDemande.listPropRdvDl,
                            qEbUserCt.ebUserNum,
                            qEbUserCt.nom,
                            qEbUserCt.prenom,
                            qEbDemande.xEbSchemaPsl(),
                            qEbDemande.listTypeDocuments,
                            qEbDemande.askForTransportResponsibility,
                            qEbDemande.provideTransport,
                            qEbDemande.tdcAcknowledge,
                            qEbDemande.tdcAcknowledgeDate,
                            qEbDemande.customerCreationDate,
                            qEbDemande.unitsPackingList,
                            qEbDemande.prixTransporteurFinal));

            query = getGlobalWhere(query, where, criterias);

            // query.createQuery();
            query.distinct();

            resultat = query.fetch();

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    private JPAQuery getGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteriaPricingBooking criterias)
        throws Exception {

        // dateTraitementCPB
        if (criterias.isFlagDateTraitementCPB()) {
            where.and(qEbDemande.dateTraitementCPB.isNull());
        }

        // dateTraitementCPB
        if (criterias.isFlagintegratedVia()) {
            where.and(qEbDemande.integratedVia.isNull());
        }

        // dateTraitementCPB
        if (criterias.getEbDemandeNum() != null) {
            where.and(qEbDemande.ebDemandeNum.eq(criterias.getEbDemandeNum()));
        }

        /** ******************JOINTURES****************** */
        query.from(qEbDemande).where(where);
        query.leftJoin(qEbDemande.xEbCostCenter(), qNamedCostCenter);
        query.leftJoin(qEbDemande.exEbDemandeTransporteurs, qExDemandeTransporteur);
        query.leftJoin(qExDemandeTransporteur.xEbEtablissement(), qEtablissement);
        query.leftJoin(qExDemandeTransporteur.xEbCompagnie(), qCompagnie);
        query.leftJoin(qExDemandeTransporteur.xTransporteur(), qTransporteur);

        query.leftJoin(qEbDemande.user(), qChargeur);
        query.leftJoin(qEbDemande.xEbEtablissement(), qChargeurEtab);
        query.leftJoin(qEbDemande.xEbCompagnie(), qChargeurComp);
        query.leftJoin(qEbDemande.ebPartyDest(), qPartyDest);
        query.leftJoin(qEbDemande.ebPartyOrigin(), qPartyOrigin);
        query.leftJoin(qChargeur.ebCompagnie(), qCompagnieChargeur);
        query.leftJoin(qEbDemande.ebDemandeMasterObject(), qEbDemandeMultiSegmentObject);
        query.leftJoin(qEbDemande.xEbEntrepotUnloading(), qEbEntrepot);
        query.leftJoin(qEbDemande.xEbUserOrigin(), qUserOrigin);
        query.leftJoin(qUserOrigin.ebEtablissement(), qUserOriginEtab);
        query.leftJoin(qEbDemande.xEbUserDest(), qUserDest);
        query.leftJoin(qUserDest.ebEtablissement(), qUserDestEtab);
        query.leftJoin(qEbDemande.xEbUserOriginCustomsBroker(), qUserOriginCB);
        query.leftJoin(qUserOriginCB.ebEtablissement(), qUserOriginCBEtab);
        query.leftJoin(qEbDemande.xEbUserDestCustomsBroker(), qUserDestCB);
        query.leftJoin(qUserDestCB.ebEtablissement(), qUserDestCBEtab);
        query.leftJoin(qEbDemande.xEbUserCt(), qEbUserCt);

        return query;
    }
}
