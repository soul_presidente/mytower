package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.cronjob.tt.model.EdiMapPsl;
import com.adias.mytowereasy.utils.search.SearchCriteriaMappingCodePsl;


public interface DaoMappingCodePsl {
    List<EdiMapPsl> getListMappingCodePsl(SearchCriteriaMappingCodePsl criteria);

    Long getListMappingCodePslCount(SearchCriteriaMappingCodePsl criteria);

    public List<EdiMapPsl> getAutoCompleteList(String term, String field);
}
