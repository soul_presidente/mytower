/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.adias.mytowereasy.model.*;
import com.querydsl.core.types.dsl.*;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;
import com.adias.mytowereasy.airbus.model.EbQrGroupeProposition;
import com.adias.mytowereasy.airbus.model.QEbQrGroupe;
import com.adias.mytowereasy.airbus.model.QrGroupeCriteria;
import com.adias.mytowereasy.airbus.model.QrGroupeValue;
import com.adias.mytowereasy.airbus.model.SqlQueryGroupedAndWhereFields;
import com.adias.mytowereasy.airbus.repository.EbQrGroupePropositionRepository;
import com.adias.mytowereasy.dao.DaoPricing;
import com.adias.mytowereasy.delivery.model.QEbLivraison;
import com.adias.mytowereasy.dock.model.QEbEntrepot;
import com.adias.mytowereasy.model.Enumeration.CancelStatus;
import com.adias.mytowereasy.model.Enumeration.CarrierStatus;
import com.adias.mytowereasy.model.Enumeration.GroupeField;
import com.adias.mytowereasy.model.Enumeration.GroupeFunction;
import com.adias.mytowereasy.model.Enumeration.GroupePropositionStatut;
import com.adias.mytowereasy.model.Enumeration.GroupeSousFunction;
import com.adias.mytowereasy.model.Enumeration.GroupeTypeTraitemant;
import com.adias.mytowereasy.model.Enumeration.InfosDocsStatus;
import com.adias.mytowereasy.model.Enumeration.Insurance;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.Enumeration.StatutOkMissing;
import com.adias.mytowereasy.model.Enumeration.TransportConfirmation;
import com.adias.mytowereasy.model.Enumeration.YesOrNo;
import com.adias.mytowereasy.repository.EbTypeTransportRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.trpl.model.QEbPlZone;
import com.adias.mytowereasy.util.DateUtils;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteria.OrderCriterias;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Component
@Transactional
public class DaoPricingImpl extends Dao implements DaoPricing {
    @Autowired
    EbUserRepository ebUserRepository;

    @Autowired
    EbQrGroupePropositionRepository ebQrGroupePropositionRepository;

    @PersistenceContext
    EntityManager em;

    @Autowired
    EbTypeTransportRepository ebTypeTransportRepository;

    @Autowired
    private ConnectedUserService connectedUserService;

    QEbDemande qEbDemande = QEbDemande.ebDemande;
    QEbDemande qEbDemandeLiv = new QEbDemande("qEbDemandeLiv");
    QEbDemande qEbDemandeMarch = new QEbDemande("qEbDemandeMarch");

    QEbMarchandise qEbMarchandise = QEbMarchandise.ebMarchandise;
    QEbLivraison qEbLivraison = QEbLivraison.ebLivraison;

    QExEbDemandeTransporteur qExDemandeTransporteur = QExEbDemandeTransporteur.exEbDemandeTransporteur;

    QEbCostCenter qEbCostCenter = QEbCostCenter.ebCostCenter;
    QEbCostCenter qNamedCostCenter = new QEbCostCenter("costCenter");

    QEbParty qPartyDest = new QEbParty("partyDest");
    QEbParty qPartyOrigin = new QEbParty("partyOrigin");
    QEbCategorie qEbCategorie = new QEbCategorie("categorie");
    QEbParty qPartyNotif = new QEbParty("partyNotif");
    QEbParty qPointeIntermediate = new QEbParty("pointeIntermediate");

    QEbCompagnie qCompagnie = new QEbCompagnie("compagnie");
    QEbEtablissement qEtablissement = new QEbEtablissement("etablissement");
    QEbUser qTransporteur = new QEbUser("transporteur");

    // QEbEtablissement qBrokerEtab = new QEbEtablissement("brokerEtab");
    // QEbCompagnie qBrokerCompagnie = new QEbCompagnie("brokerCompagnie");
    // QEbUser qBroker = new QEbUser("broker");
    QEbUser qChargeur = new QEbUser("qChargeur");
    QEbCompagnie qCompagnieChargeur = new QEbCompagnie("qCompagnieChargeur");
    QEbDemande qEbDemandeMultiSegmentObject = new QEbDemande("qEbDemandeMultiSegmentObject");
    QEbDemande qEbDemandeInitial = new QEbDemande("qEbDemandeInitial");
    QEbEtablissement qChargeurEtab = new QEbEtablissement("qChargeurEtab");
    QEbCompagnie qChargeurComp = new QEbCompagnie("qChargeurComp");

    QEbUser qUserOrigin = new QEbUser("qUserOrigin");
    QEbEtablissement qUserOriginEtab = new QEbEtablissement("qUserOriginEtab");
    QEbUser qUserDest = new QEbUser("qUserDest");
    QEbEtablissement qUserDestEtab = new QEbEtablissement("qUserDestEtab");
    QEbUser qUserOriginCB = new QEbUser("qUserOriginCB");
    QEbEtablissement qUserOriginCBEtab = new QEbEtablissement("qUserOriginCBEtab");
    QEbUser qUserDestCB = new QEbUser("qUserDestCB");
    QEbEtablissement qUserDestCBEtab = new QEbEtablissement("qUserDestCBEtab");
    QEbCompagnie qUserOriginCBComp = new QEbCompagnie("qUserDestCBComp");
    QEbCompagnie qUserDestCBComp = new QEbCompagnie("qUserDestCBComp");

    QEbQrGroupe qEbQrGroupe = new QEbQrGroupe("qEbQrGroupe");
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbCompagnie");
    QEbUser qEbUserCt = new QEbUser("qEbUserCt");
    QEbEntrepot qEbEntrepot = new QEbEntrepot("qEbEntrepot");
    QEbAdresse qEbAdresse = new QEbAdresse("qEbAdresse");

    QEbUser qEbUserObserver = new QEbUser("qEbUserObserver");

    QEcCurrency qEcCurrencyInvoice = new QEcCurrency("qCurrencyInvoice");

    QEbPlZone qZoneOrigin = new QEbPlZone("qZoneOrigin");
    QEbPlZone qZoneDest = new QEbPlZone("qZoneDest");
    QEcCountry qCountryOrigin = new QEcCountry("qCountryOrigin");
    QEcCountry qCountryDest = new QEcCountry("qCountryDest");
    QEcCountry qCountryNotif = new QEcCountry("qCountryNotif");
    QEbEtablissement qEbEtablissementCt = new QEbEtablissement("qEbEtablissementCt");

    QDemandeUserObservers qDemandeUserObservers = new QDemandeUserObservers("qDemandeUserObservers");

    private static Map<String, Object> orderHashMap = new HashMap<>();

    private static final Logger LOGGER = LoggerFactory.getLogger(DaoPricingImpl.class);

    private void Initialiaze() {
        orderHashMap.put("demandeNum", qEbDemande.ebDemandeNum);
        orderHashMap.put("refTransport", qEbDemande.refTransport);
        orderHashMap.put("customerReference", qEbDemande.customerReference);
        orderHashMap.put("libelleOriginCountry", qEbDemande.libelleOriginCountry);
        orderHashMap.put("libelleDestCountry", qEbDemande.libelleDestCountry);
        orderHashMap.put("totalWeight", qEbDemande.totalWeight);
        orderHashMap.put("xEcModeTransport", qEbDemande.xEcModeTransport);
        orderHashMap.put("xEbCostCenter", qEbDemande.xEbCostCenter().libelle);
        orderHashMap.put("dateCreation", qEbDemande.dateCreation);
        orderHashMap.put("xEcTypeDemande", qEbDemande.xEcTypeDemande);
        orderHashMap.put("xEcStatut", qEbDemande.xEcStatut);
        orderHashMap.put("flagDocumentTransporteur", qEbDemande.flagDocumentTransporteur);
        orderHashMap.put("flagTransportInformation", qEbDemande.flagTransportInformation);
        orderHashMap.put("flagDocumentCustoms", qEbDemande.flagDocumentCustoms);
        orderHashMap.put("unitsReference", qEbDemande.unitsReference);
        orderHashMap.put("price", qExDemandeTransporteur.price);
        orderHashMap.put("etablissementTransporteur", qEtablissement.nom);
        orderHashMap.put("companyTransporteur", qCompagnie.nom);
        orderHashMap.put("exEbDemandeTransporteurFinal", qTransporteur.nom);
        orderHashMap.put("emailTransporteur", qTransporteur.email);
        orderHashMap.put("datePickup", qExDemandeTransporteur.pickupTime);
        orderHashMap.put("dateDelivery", qExDemandeTransporteur.deliveryTime);
        orderHashMap.put("datePickupTM", qEbDemande.datePickupTM);
        orderHashMap.put("dateOfGoodsAvailability", qEbDemande.dateOfGoodsAvailability);
        orderHashMap.put("finalDelivery", qEbDemande.finalDelivery);
        orderHashMap.put("confirmPickup", qEbDemande.confirmPickup);
        orderHashMap.put("confirmDelivery", qEbDemande.confirmDelivery);
        orderHashMap.put("libelleOriginCity", qEbDemande.libelleOriginCity);
        orderHashMap.put("libelleDestCity", qEbDemande.libelleDestCity);
        orderHashMap.put("xEcIncotermLibelle", qEbDemande.xEcIncotermLibelle);
        orderHashMap.put("libelleLastPsl", qEbDemande.libelleLastPsl);
        orderHashMap.put("dateLastPsl", qEbDemande.dateLastPsl);
        orderHashMap.put("numAwbBol", qEbDemande.numAwbBol);
        orderHashMap.put("carrierUniqRefNum", qEbDemande.carrierUniqRefNum);
        orderHashMap.put("typeRequestLibelle", qEbDemande.typeRequestLibelle);
        orderHashMap.put("companyOrigin", qPartyOrigin.company);
        orderHashMap.put("typeFluxNum", qEbDemande.xEbTypeFluxNum);
        orderHashMap.put("typeFluxNum", qEbDemande.xEbTypeFluxDesignation);
        orderHashMap.put("totalVolume", qEbDemande.totalVolume);
        orderHashMap.put("totalWeight", qEbDemande.totalWeight);
        orderHashMap.put("waitingForConf", qEbDemande.waitingForConf);
        orderHashMap.put("waitingForQuote", qEbDemande.waitingForQuote);
        orderHashMap.put("confirmed", qEbDemande.confirmed);
        orderHashMap.put("valuationType", qEbDemande.valuationType);
        orderHashMap.put("city", qEbDemande.city);
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List<EbDemande> getListEbDemande(SearchCriteriaPricingBooking criterias, EbUser connectedUser)
        throws Exception {
        // get all demandes for transports from edi cron job
        List<EbDemande> resultat = new ArrayList<>();

        /*
         * if (criterias == null) { JPQLQuery query = new JPAQuery(em);
         * QEbDemande
         * demande = QEbDemande.ebDemande; List<EbDemande> demandes =
         * query.from(demande).fetch(); return demandes; }
         */
        // other wise criterias is pass do the filter
        QExEbDemandeTransporteur qEbDemandeQuote = new QExEbDemandeTransporteur("qEbDemandeQuote");
        QExEbDemandeTransporteur qEbDemandeQuoteEtab = new QExEbDemandeTransporteur("qEbDemandeQuoteEtab");
        QEcCurrency qEcCurrencyInvoice = new QEcCurrency("qCurrencyInvoice");

        List<EbMarchandise> allMarchandise = new ArrayList<EbMarchandise>();
        BooleanBuilder where = new BooleanBuilder();
        StringExpression stringOrderWithExpression = qEbDemande.refTransport;
        NumberExpression<Integer> numberOrderWithExpression = qEbDemande.xEcStatut;
        Path<String> stringOrderAlias = ExpressionUtils.path(String.class, "stringOrderExpression");
        Path<Integer> numberOrderAlias = ExpressionUtils.path(Integer.class, "numberOrderExpression");

        Initialiaze();

        try {
            JPAQuery<EbDemande> query = new JPAQuery<EbDemande>(em);

            if (criterias != null && criterias.getModule() != null) {
                query.distinct();

                if (criterias.getOrder() != null && criterias.getOrder().size() > 0) {
                    String columnName = criterias.getOrder().get(0).get(OrderCriterias.columnName);
                    String fieldType = null;
                    StringExpression fieldColumn = null;
                    NumberExpression indexOfCategExpr = null;

                    if (columnName != null && columnName.matches("^category[0-9]+$")) {
                        fieldType = "category";
                        fieldColumn = qEbDemande.listCategoryFlat;
                        columnName = columnName.substring(fieldType.length());
                        indexOfCategExpr = fieldColumn.indexOf(columnName + ":").add(columnName.length() + 2 + 1);
                    }
                    else if (columnName != null && columnName.matches("^field[0-9]+$")) {
                        fieldType = "field";
                        fieldColumn = qEbDemande.listCustomFieldsFlat;
                        indexOfCategExpr = fieldColumn.indexOf(columnName + "\":").add(columnName.length() + 2 + 2);
                    }

                    if (fieldType != null && fieldColumn != null) {
                        StringExpression restStringExpr = fieldColumn.substring(indexOfCategExpr);
                        NumberExpression endIndexExpr = restStringExpr.indexOf("\"");

                        /*
                         * quand on Ã©crie dans querydsl: substring(a, b) cela
                         * se traduit par
                         * substring(a+1, b-a) dans sql donc on doit diminuer 1
                         * de l'expr de gauche et
                         * on ajoute 'a' dans l'expr de droite
                         */
                        stringOrderWithExpression = new CaseBuilder()
                            .when(
                                new BooleanBuilder()
                                    .orAllOf(fieldColumn.isNotNull(), fieldColumn.indexOf(columnName).goe(0)))
                            .then(
                                fieldColumn
                                    .substring(
                                        indexOfCategExpr.subtract(1), // diminuer
                                                                      // 1
                                        endIndexExpr.add(indexOfCategExpr) // rajouter
                                                                           // l'expr
                                                                           // de
                                                                           // gauche
                                                                           // 'a'
                                    )).otherwise(Expressions.nullExpression());

                        stringOrderWithExpression = new CaseBuilder()
                            .when(
                                new BooleanBuilder()
                                    .orAllOf(
                                        stringOrderWithExpression.isNotNull(),
                                        stringOrderWithExpression.isNotEmpty()))
                            .then(stringOrderWithExpression).otherwise(Expressions.nullExpression());
                    }
                    else if (columnName != null && columnName.contentEquals("curentTimeRemainingInMilliSecond")) {
                        Date currentDate = new Date();
                        numberOrderWithExpression = new CaseBuilder()
                            .when(qEbDemande.xEcTypeDemande.eq(Enumeration.TypeDemande.URGENT.getCode()))
                            .then(
                                Expressions
                                    .numberTemplate(
                                        Integer.class,
                                        "EXTRACT('epoch' FROM ({0} - {1}))",
                                        qEbDemande.dateCreation,
                                        currentDate)
                                    .add(Enumeration.TypeDemande.URGENT.getTimeRemaining() * 60))
                            .otherwise(
                                Expressions
                                    .numberTemplate(
                                        Integer.class,
                                        "EXTRACT('epoch' FROM ({0} - {1}))",
                                        qEbDemande.dateCreation,
                                        currentDate)
                                    .add(Enumeration.TypeDemande.STANDARD.getTimeRemaining() * 60));
                        numberOrderWithExpression = new CaseBuilder()
                            .when(numberOrderWithExpression.loe(0)).then(0).otherwise(numberOrderWithExpression);
                    }
                    else if (columnName != null && columnName.contentEquals("xEcStatut")) {

                        if (Enumeration.Module.PRICING.getCode().equals(criterias.getModule())) {
                            numberOrderWithExpression = new CaseBuilder()
                                .when(
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qEbDemande.xEcCancelled.isNotNull(),
                                            qEbDemande.xEcCancelled.eq(Enumeration.CancelStatus.CANECLLED.getCode())))
                                .then(Expressions.ONE)
                                .when(
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qEbDemande.xEcStatut.isNotNull(),
                                            qEbDemande.xEcStatut.goe(Enumeration.StatutDemande.FIN.getCode())))
                                .then(Expressions.TWO)
                                .when(
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qEbDemande.xEcStatut.isNotNull(),
                                            qEbDemande.xEcStatut.eq(Enumeration.StatutDemande.INP.getCode())))
                                .then(Expressions.THREE)
                                .when(
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qEbDemande.xEcStatut.isNotNull(),
                                            qEbDemande.xEcStatut.eq(Enumeration.StatutDemande.REC.getCode())))
                                .then(Expressions.FOUR)
                                .when(
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qEbDemande.xEcStatut.isNotNull(),
                                            qEbDemande.xEcStatut.eq(Enumeration.StatutDemande.WRE.getCode())))
                                .then(Expressions.constant(5)).otherwise(Expressions.nullExpression(Integer.class));
                        }
                        else {
                            numberOrderWithExpression = new CaseBuilder()
                                .when(
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qEbDemande.xEcCancelled.isNotNull(),
                                            qEbDemande.xEcCancelled.eq(Enumeration.CancelStatus.CANECLLED.getCode())))
                                .then(Expressions.ONE)
                                .when(
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qEbDemande.xEcStatut.isNotNull(),
                                            qEbDemande.xEcStatut.eq(Enumeration.StatutDemande.DLVRY.getCode())))
                                .then(Expressions.TWO)
                                .when(
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qEbDemande.xEcStatut.isNotNull(),
                                            qEbDemande.xEcStatut.eq(Enumeration.StatutDemande.TO.getCode())))
                                .then(Expressions.THREE)
                                .when(
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qEbDemande.xEcStatut.isNotNull(),
                                            qEbDemande.xEcStatut.eq(Enumeration.StatutDemande.WPU.getCode())))
                                .then(Expressions.FOUR).otherwise(Expressions.nullExpression(Integer.class));
                        }

                    }
                    else if (columnName != null && columnName.contentEquals("xEcModeTransport")) {
                        numberOrderWithExpression = new CaseBuilder()
                            .when(
                                new BooleanBuilder()
                                    .orAllOf(
                                        qEbDemande.xEcModeTransport.isNotNull(),
                                        qEbDemande.xEcModeTransport.eq(Enumeration.ModeTransport.AIR.getCode())))
                            .then(Expressions.ONE)
                            .when(
                                new BooleanBuilder()
                                    .orAllOf(
                                        qEbDemande.xEcModeTransport.isNotNull(),
                                        qEbDemande.xEcModeTransport.eq(Enumeration.ModeTransport.INTEGRATOR.getCode())))
                            .then(Expressions.TWO)
                            .when(
                                new BooleanBuilder()
                                    .orAllOf(
                                        qEbDemande.xEcModeTransport.isNotNull(),
                                        qEbDemande.xEcModeTransport.eq(Enumeration.ModeTransport.ROAD.getCode())))
                            .then(Expressions.THREE)
                            .when(
                                new BooleanBuilder()
                                    .orAllOf(
                                        qEbDemande.xEcModeTransport.isNotNull(),
                                        qEbDemande.xEcModeTransport.eq(Enumeration.ModeTransport.SEA.getCode())))
                            .then(Expressions.FOUR).otherwise(Expressions.nullExpression(Integer.class));
                    }

                }

                if (criterias.getModule() == Enumeration.Module.PRICING.getCode()) {

                    if (criterias.getSearchByWS() != null && criterias.getSearchByWS()) {
                        query
                            .select(
                                QEbDemande
                                    .create(
                                        qEbDemande.ebDemandeNum,
                                        qEbDemande.refTransport,
                                        qNamedCostCenter.ebCostCenterNum,
                                        qNamedCostCenter.libelle,
                                        qEbDemande.customerReference,
                                        qEbDemande.totalWeight,
                                        qEbDemande.xEcModeTransport,
                                        qEbDemande.dateCreation,
                                        qEbDemande.xEcStatut,
                                        qEbDemande.xEcTypeDemande,
                                        qEbDemande.xEbTypeRequest,
                                        qEbDemande.typeRequestLibelle,
                                        qEbDemande.libelleDestCountry,
                                        qEbDemande.libelleOriginCountry,
                                        qEbDemande.dateOfGoodsAvailability,
                                        qEbDemande.dateOfArrival,
                                        qEbDemande.datePickup,
                                        qPartyOrigin,
                                        qPartyDest,
                                        qPartyNotif,
                                        qEbDemande.xEbTypeTransport,
                                        qEbDemande.flagRecommendation,
                                        qEbDemande.commentChargeur,
                                        qEbDemande.totalVolume,
                                        qEbDemande.totalTaxableWeight,
                                        qEbDemande.insurance,
                                        qEbDemande.insuranceValue,
                                        qEbDemande.city,
                                        qEbDemande.listCategories,
                                        qEbDemande.listTypeDocuments,
                                        qEbDemande.totalNbrParcel,
                                        qUserOriginCB.ebUserNum,
                                        qUserOriginCB.email,
                                        qUserOriginCBEtab.ebEtablissementNum,
                                        qUserDestCB.ebUserNum,
                                        qUserDestCB.email,
                                        qUserDestCBEtab.ebEtablissementNum,
                                        qEbDemande.xEcTypeCustomBroker,
                                        qEbDemande.xEbTransporteur,
                                        qEbDemande.xEbTransporteurEtablissementNum,
                                        qEbDemande.flagDocumentCustoms,
                                        qEbDemande.flagDocumentTransporteur,
                                        qEbDemande.flagTransportInformation,
                                        qEbDemande.flagCustomsInformation,
                                        qEbDemande.flagRequestCustomsBroker,
                                        qEbDemande.customFields,
                                        qEbDemande.configPsl,
                                        qEbDemande.numAwbBol,
                                        qEbDemande.carrierUniqRefNum,
                                        qEbDemande.flightVessel,
                                        qEbDemande.datePickupTM,
                                        qEbDemande.etd,
                                        qEbDemande.finalDelivery,
                                        qEbDemande.eta,
                                        qEbDemande.customsOffice,
                                        qEbDemande.mawb,
                                        qEbDemande.flagEbtrackTrace,
                                        qEbDemande.flagEbInvoice,
                                        qChargeur,
                                        qChargeurEtab.ebEtablissementNum,
                                        qChargeurEtab.nom,
                                        qChargeurComp.ebCompagnieNum,
                                        qChargeurComp.nom,
                                        qChargeurComp.code,
                                        qEcCurrencyInvoice.ecCurrencyNum,
                                        qEcCurrencyInvoice.code,
                                        qEbDemande.ebDemandeHistory,
                                        qEbDemande.moduleCreator,
                                        qEbDemande.listLabels,
                                        qEbDemande.xEcCancelled,
                                        qEbDemande.xEcNature,
                                        qEbDemande.pointIntermediate().ebPartyNum,
                                        qPointeIntermediate.reference,
                                        qPointeIntermediate.company,
                                        qEbDemande.ebDemandeMasterObject().ebDemandeNum,
                                        qEbDemande.ebDemandeMasterObject().refTransport,
                                        qEbDemande.ebDemandeInitial().ebDemandeNum,
                                        qEbDemande.ebDemandeInitial().refTransport,
                                        qUserOrigin.ebUserNum,
                                        qUserOrigin.nom,
                                        qUserOriginEtab.ebEtablissementNum,
                                        qUserOriginEtab.nom,
                                        qUserDest.ebUserNum,
                                        qUserDest.nom,
                                        qUserDestEtab.ebEtablissementNum,
                                        qUserDestEtab.nom,
                                        qUserOriginCB.ebUserNum,
                                        qUserOriginCB.nom,
                                        qUserOriginCBEtab.ebEtablissementNum,
                                        qUserOriginCBEtab.nom,
                                        qUserDestCB.ebUserNum,
                                        qUserDestCB.nom,
                                        qUserDestCBEtab.ebEtablissementNum,
                                        qUserDestCBEtab.nom,
                                        qEbDemande.listFlag,
                                        qEbDemande.xEbTypeFluxNum,
                                        qEbDemande.xEbTypeFluxCode,
                                        qEbDemande.xEbTypeFluxDesignation,
                                        qEbDemande.xEbIncotermNum,
                                        qEbDemande.xEcIncotermLibelle,
                                        qEbDemande.xEbSchemaPsl(),
                                        qEbEntrepot,
                                        qEbDemande.xEbDemandeGroup,
                                        qEbDemande.carrierComment,
                                        qEbDemande.askForTransportResponsibility,
                                        qEbDemande.provideTransport,
                                        qEbDemande.tdcAcknowledge,
                                        qEbDemande.tdcAcknowledgeDate,
                                        qEbDemande.eligibleForConsolidation,
                                        qEbDemande.customerCreationDate,
                                        qEbDemande.xEcStatutGroupage,
                                        qEbDemande.listAdditionalCost,
                                        qEbDemande.dateMaj,
                                        qEbDemande.finalChoiceToBeCalculated,
                                        qEbDemande.exportControlStatut,
										qEbDemande.isValorized,
										qEbDemande.codeConfigurationEDI
										));
                    }
                    else {
                        query
                            .select(
                                QEbDemande
                                    .create(
                                        qEbDemande.ebDemandeNum,
                                        qEbDemande.refTransport,
                                        qNamedCostCenter.libelle,
                                        qEbDemande.customerReference,
                                        qEbDemande.partNumber,
                                        qEbDemande.serialNumber,
                                        qEbDemande.shippingType,
                                        qEbDemande.orderNumber,
                                        qEbDemande.totalWeight,
                                        qEbDemande.totalVolume,
                                        qEbDemande.totalNbrParcel,
                                        qEbDemande.xEcModeTransport,
                                        qEbDemande.dateCreation,
                                        qEbDemande.updateDate,
                                        qEbDemande.xEcStatut,
                                        qEbDemande.xEcTypeDemande,
                                        qEbDemande.xEbTypeRequest,
                                        qEbDemande.typeRequestLibelle,
                                        qEbDemande.libelleDestCountry,
                                        qEbDemande.libelleOriginCountry,
                                        qEbDemande.city,
                                        qEbDemande.flagRecommendation,
                                        qEbDemande.xEbTypeTransport,
                                        qEbDemande.xEcTypeCustomBroker,
                                        qEbDemande.xEbTransporteur,
                                        qEbDemande.xEbTransporteurEtablissementNum,
                                        qEbDemande.customFields,
                                        qChargeur.ebUserNum,
                                        qChargeur.nom,
                                        qChargeur.prenom,
                                        qChargeur.email,
                                        qEbDemande.xEbCompagnie().ebCompagnieNum,
                                        qEbDemande.xEbSchemaPsl(),
                                        qEbDemande.waitingForQuote,
                                        qEbDemande.waitingForConf,
                                        qEbDemande.confirmed,
                                        qEbDemande.listCategories,
                                        qEbDemande.unitsReference,
                                        qEbDemande.xecCurrencyInvoice().ecCurrencyNum,
                                        qEbDemande.xEcCancelled,
                                        qEbDemande.confirmedFromTransptPlan,
                                        qEbDemande.ebPartyOrigin().company,
                                        qEbDemande.ebPartyDest().company,
                                        stringOrderWithExpression.as("stringOrderExpression"),
                                        numberOrderWithExpression.as("numberOrderExpression"),
                                        qEbDemande.libelleOriginCity,
                                        qEbDemande.libelleDestCity,
                                        qEbDemande.xEcIncotermLibelle,
                                        qEbDemande.numAwbBol,
                                        qEbDemande.xEbCompagnie().nom,
                                        qEbDemande.xEcNature,
                                        qEbDemande.ebDemandeMasterObject().ebDemandeNum,
                                        qEbDemande.ebDemandeMasterObject().refTransport,
                                        qEbDemande.listFlag,
                                        Expressions.asSimple(connectedUser),
                                        Expressions.asNumber(criterias.getModule()),
                                        qEbDemande.flagConsolidated,
                                        qEbDemande.xEbDemandeGroup,
                                        qEbUserCt.ebUserNum,
                                        qEbUserCt.nom,
                                        qEbUserCt.prenom,
                                        qEbDemande.listTypeDocuments,
                                        qEbDemande.askForTransportResponsibility,
                                        qEbDemande.provideTransport,
                                        qEbDemande.tdcAcknowledge,
                                        qEbDemande.tdcAcknowledgeDate,
                                        qEbDemande.eligibleForConsolidation,
                                        qEbDemande.customerCreationDate,
                                        qEbDemande.carrierUniqRefNum,
                                        qEbDemande.unitsPackingList,
                                        qEbDemande.torType,
                                        qEbDemande.torDuration,
																qEbDemande.valuationType,
																qEbDemande.consolidationRuleLibelle
															)
													);
                    }

                }
                else if (Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(criterias.getModule())
                    || criterias.getModule() == Enumeration.Module.ORDRE_TRANSPORT.getCode()) {
                    query
                        .select(
                            QEbDemande
                                .create(
                                    qEbDemande.ebDemandeNum,
                                    qEbDemande.refTransport,
                                    qNamedCostCenter.libelle,
                                    qEbDemande.customerReference,
                                    qEbDemande.partNumber,
                                    qEbDemande.serialNumber,
                                    qEbDemande.shippingType,
                                    qEbDemande.orderNumber,
                                    qEbDemande.totalWeight,
                                    qEbDemande.xEcModeTransport,
                                    qEbDemande.dateCreation,
                                    qEbDemande.updateDate,
                                    qEbDemande.xEcStatut,
                                    qEbDemande.xEcTypeDemande,
                                    qEbDemande.xEbTypeRequest,
                                    qEbDemande.typeRequestLibelle,
                                    qEbDemande.libelleDestCountry,
                                    qEbDemande.libelleOriginCountry,
                                    qEbDemande.xEbTypeTransport,
                                    qEbDemande.xEcTypeCustomBroker,
                                    qEbDemande.nbrDoc,
                                    qEbDemande.flagRecommendation,
                                    qEbDemande.flagDocumentCustoms,
                                    qEbDemande.flagDocumentTransporteur,
                                    qEbDemande.flagTransportInformation,
                                    qEbDemande.flagCustomsInformation,
                                    qEbDemande.flagRequestCustomsBroker,
                                    qEbDemande.xEbTransporteur,
                                    qEbDemande.customFields,
                                    qChargeur.ebUserNum,
                                    qChargeur.nom,
                                    qChargeur.prenom,
                                    qChargeur.email,
                                    qCompagnieChargeur.ebCompagnieNum,
                                    qCompagnieChargeur.nom,
                                    qCompagnieChargeur.code,
                                    qEbDemande.listCategories,
                                    qEbDemande.datePickupTM,
                                    qEbDemande.unitsReference,
                                    qEbDemande.xecCurrencyInvoice().ecCurrencyNum,
                                    qEbDemande.confirmPickup,
                                    qEbDemande.confirmDelivery,
                                    qEbDemande.dateOfGoodsAvailability,
                                    qEbDemande.finalDelivery,
                                    qEbDemande.ebPartyOrigin().company,
                                    qEbDemande.ebPartyDest().company,
                                    stringOrderWithExpression.as("stringOrderExpression"),
                                    numberOrderWithExpression.as("numberOrderExpression"),
                                    qEbDemande.xEcCancelled,
                                    qEbDemande.libelleOriginCity,
                                    qEbDemande.libelleDestCity,
                                    qEbDemande.xEbTypeFluxNum,
                                    qEbDemande.xEbTypeFluxCode,
                                    qEbDemande.xEbTypeFluxDesignation,
                                    qEbDemande.xEbIncotermNum,
                                    qEbDemande.xEcIncotermLibelle,
                                    qEbDemande.numAwbBol,
                                    qEbDemande.carrierUniqRefNum,
                                    qEbDemande.libelleLastPsl,
                                    qEbDemande.codeAlphaLastPsl,
                                    qEbDemande.dateLastPsl,
                                    qEbDemande.codeAlphaPslCourant,
                                    qEbDemande.xEcNature,
                                    qEbDemande.ebDemandeMasterObject().ebDemandeNum,
                                    qEbDemande.ebDemandeMasterObject().refTransport,
                                    qEbDemande.listFlag,
                                    Expressions.asSimple(connectedUser),
                                    Expressions.asNumber(criterias.getModule()),
                                    qEbDemande.listPropRdvPk,
                                    qEbDemande.listPropRdvDl,
                                    qEbUserCt.ebUserNum,
                                    qEbUserCt.nom,
                                    qEbUserCt.prenom,
                                    qEbDemande.xEbSchemaPsl(),
                                    qEbDemande.listTypeDocuments,
                                    qEbDemande.askForTransportResponsibility,
                                    qEbDemande.provideTransport,
                                    qEbDemande.tdcAcknowledge,
                                    qEbDemande.tdcAcknowledgeDate,
                                    qEbDemande.customerCreationDate,
                                    qEbDemande.unitsPackingList,
                                    qEbDemande.prixTransporteurFinal));
                }

            }

            if (criterias.getSearchByWS() != null && criterias.getSearchByWS()) {
                query.from(qEbDemande).where(where);
                query.leftJoin(qEbDemande.ebPartyDest(), qPartyDest);
                query.leftJoin(qEbDemande.ebPartyOrigin(), qPartyOrigin);
                query.leftJoin(qEbDemande.ebPartyNotif(), qPartyNotif);
                query.leftJoin(qEbDemande.pointIntermediate(), qPointeIntermediate);
                query.leftJoin(qEbDemande.xEbCostCenter(), qNamedCostCenter);
                query.leftJoin(qEbDemande.xecCurrencyInvoice(), qEcCurrencyInvoice);
                query.leftJoin(qEbDemande.ebDemandeMasterObject(), qEbDemandeMultiSegmentObject);
                query.leftJoin(qEbDemande.ebDemandeInitial(), qEbDemandeInitial);
                query.leftJoin(qEbDemande.xEbEntrepotUnloading(), qEbEntrepot);
                query.leftJoin(qEbDemande.observers, qDemandeUserObservers);
            }

            if (criterias.getSearchByWS() != null) {

                if (StringUtils.isNotBlank(criterias.getTransporterEmail())) {
                    where.andAnyOf(qExDemandeTransporteur.xTransporteur().email.eq(criterias.getTransporterEmail()));
                }
                else if (StringUtils.isNotBlank(criterias.getTransporterCompanyCode())) {
                    where
                        .andAnyOf(qExDemandeTransporteur.xEbCompagnie().code.eq(criterias.getTransporterCompanyCode()));
                }

                if (criterias.getxEcNature() != null) {
                    where.andAnyOf(qEbDemande.xEcNature.eq(criterias.getxEcNature()));
                }

                if (criterias.getValorized() != null) {

                    if (criterias.getValorized()) {
                        where.and(qEbDemande.isValorized.eq(criterias.getValorized()));
                    }
                    else {
                        where
                            .and(
                                qEbDemande.isValorized
                                    .eq(criterias.getValorized()).or(qEbDemande.isValorized.isNull()));
                    }

                }

            }

            query = getGlobalWhere(query, where, criterias, connectedUser);

            /*
             * if(Enumeration.Module.PRICING.getCode().equals(criterias.
             * getModule())) {
             * query.leftJoin(qEbDemande.exEbDemandeTransporteurs,
             * qEbDemandeQuoteEtab).on(new BooleanBuilder().orAllOf(
             * qEbDemandeQuoteEtab.xEbEtablissement().ebEtablissementNum.eq(
             * connectedUser.
             * getEbEtablissement().getEbEtablissementNum()) ));
             * query.leftJoin(qEbDemande.exEbDemandeTransporteurs,
             * qEbDemandeQuote).on(new
             * BooleanBuilder().orAllOf(
             * qEbDemandeQuote.xTransporteur().ebUserNum.eq(connectedUser.
             * getEbUserNum())
             * )); }
             */

            if (criterias.getSize() != null && criterias.getSize() >= 0) {
                query.limit(criterias.getSize());
            }

            if (criterias.getPageNumber() != null) {
                query.offset(criterias.getPageNumber() * (criterias.getSize() != null ? criterias.getSize() : 1));
            }

            if (criterias.getOrder() != null && criterias.getOrder().size() > 0) {

                for (Map<OrderCriterias, String> map: criterias.getOrder()) {
                    String columnName = map.get(OrderCriterias.columnName);

                    if (columnName != null && columnName.matches("^(category|field)[0-9]+$")) {
                        if (map
                            .get(OrderCriterias.dir)
                            .equals("asc")) query.orderBy(new OrderSpecifier(Order.ASC, stringOrderAlias).nullsLast());
                        else query.orderBy(new OrderSpecifier(Order.DESC, stringOrderAlias).nullsLast());
                    }

                    if (columnName != null && (columnName.contentEquals("curentTimeRemainingInMilliSecond")
                        || columnName.contentEquals("xEcModeTransport"))) {

                        if (map.get(OrderCriterias.dir).equals("asc")) {
                            query.orderBy(new OrderSpecifier(Order.ASC, numberOrderAlias).nullsLast());
                            if (columnName
                                .contentEquals(
                                    "xEcModeTransport")) query.orderBy(qEbDemande.xEbTypeTransport.asc().nullsLast());
                        }

                        else {
                            query.orderBy(new OrderSpecifier(Order.DESC, numberOrderAlias).nullsLast());
                            if (columnName
                                .contentEquals(
                                    "xEcModeTransport")) query.orderBy(qEbDemande.xEbTypeTransport.desc().nullsLast());
                        }

                    }
                    else if (columnName.contentEquals("statutStr")) {

                        if (map.get(OrderCriterias.dir).equals("asc")) {
                            query
                                .orderBy(qEbDemande.xEcCancelled.asc().nullsFirst())
                                .orderBy(qEbDemande.xEcStatut.asc().nullsFirst());
                        }
                        else {
                            query
                                .orderBy(qEbDemande.xEcCancelled.desc().nullsLast())
                                .orderBy(qEbDemande.xEcStatut.desc().nullsLast());
                        }

                    }
                    else if (columnName != null && columnName.contentEquals("xEbUserCt")) {

                        if (map.get(OrderCriterias.dir).equals("asc")) {
                            query
                                .orderBy(qEbUserCt.nom.asc().nullsFirst()).orderBy(qEbUserCt.prenom.asc().nullsFirst());
                        }
                        else {
                            query
                                .orderBy(qEbUserCt.nom.desc().nullsLast()).orderBy(qEbUserCt.prenom.desc().nullsLast());
                        }

                    }
                    else if (columnName != null && columnName.contentEquals("user")) {

                        if (map.get(OrderCriterias.dir).equals("asc")) {
                            query
                                .orderBy(qChargeur.nom.asc().nullsFirst()).orderBy(qChargeur.prenom.asc().nullsFirst());
                        }
                        else {
                            query
                                .orderBy(qChargeur.nom.desc().nullsLast()).orderBy(qChargeur.prenom.desc().nullsLast());
                        }

                    }
                    else if (orderHashMap.get(columnName) != null) {

                        if (map.get(OrderCriterias.dir).equals("asc")) {
                            query
                                .orderBy(
                                    ((ComparableExpressionBase<String>) orderHashMap.get(columnName))
                                        .asc().nullsFirst());
                        }

                        else {
                            query
                                .orderBy(
                                    ((ComparableExpressionBase<String>) orderHashMap.get(columnName))
                                        .desc().nullsLast());
                            if (columnName
                                .equals(
                                    "xEcModeTransport")) query.orderBy(qEbDemande.xEbTypeTransport.desc().nullsLast());
                        }

                    }

                }

            }
            else {
                query.orderBy(((ComparableExpressionBase<String>) orderHashMap.get("demandeNum")).desc().nullsLast());
            }

            // query.createQuery();
            query.distinct();

            resultat = query.fetch();

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public EbDemande getEbDemande(SearchCriteriaPricingBooking criterias, EbUser user) throws Exception {
        EbDemande resultat = null;
        Initialiaze();
        QEcCurrency qEcCurrencyInvoice = new QEcCurrency("qCurrencyInvoice");
        EbUser connectedUser = user != null ? user : connectedUserService.getCurrentUserWithCategories(criterias);

        try {
            JPAQuery<EbDemande> query = new JPAQuery<EbDemande>(em);
            BooleanBuilder where = new BooleanBuilder();

            query
                .select(
                    QEbDemande
                        .create(
                            qEbDemande.ebDemandeNum,
                            qEbDemande.refTransport,
                            qNamedCostCenter.ebCostCenterNum,
                            qNamedCostCenter.libelle,
                            qEbDemande.customerReference,
                            qEbDemande.totalWeight,
                            qEbDemande.xEcModeTransport,
                            qEbDemande.dateCreation,
                            qEbDemande.xEcStatut,
                            qEbDemande.xEcTypeDemande,
                            qEbDemande.xEbTypeRequest,
                            qEbDemande.typeRequestLibelle,
                            qEbDemande.libelleDestCountry,
                            qEbDemande.libelleOriginCountry,
                            qEbDemande.dateOfGoodsAvailability,
                            qEbDemande.dateOfArrival,
                            qEbDemande.datePickup,
                            qPartyOrigin,
                            qPartyDest,
                            qPartyNotif,
                            qEbDemande.xEbTypeTransport,
                            qEbDemande.flagRecommendation,
                            qEbDemande.commentChargeur,
                            qEbDemande.totalVolume,
                            qEbDemande.totalTaxableWeight,
                            qEbDemande.insurance,
                            qEbDemande.insuranceValue,
                            qEbDemande.city,
                            qEbDemande.listCategories,
                            qEbDemande.listTypeDocuments,
                            qEbDemande.totalNbrParcel,
                            qUserOriginCB.ebUserNum,
                            qUserOriginCB.email,
                            qUserOriginCBEtab.ebEtablissementNum,
                            qUserDestCB.ebUserNum,
                            qUserDestCB.email,
                            qUserDestCBEtab.ebEtablissementNum,
                            qEbDemande.xEcTypeCustomBroker,
                            qEbDemande.xEbTransporteur,
                            qEbDemande.xEbTransporteurEtablissementNum,
                            qEbDemande.flagDocumentCustoms,
                            qEbDemande.flagDocumentTransporteur,
                            qEbDemande.flagTransportInformation,
                            qEbDemande.flagCustomsInformation,
                            qEbDemande.flagRequestCustomsBroker,
                            qEbDemande.customFields,
                            qEbDemande.configPsl,
                            qEbDemande.numAwbBol,
                            qEbDemande.carrierUniqRefNum,
                            qEbDemande.flightVessel,
                            qEbDemande.datePickupTM,
                            qEbDemande.etd,
                            qEbDemande.finalDelivery,
                            qEbDemande.eta,
                            qEbDemande.customsOffice,
                            qEbDemande.mawb,
                            qEbDemande.flagEbtrackTrace,
                            qEbDemande.flagEbInvoice,
                            qChargeur,
                            qChargeurEtab.ebEtablissementNum,
                            qChargeurEtab.nom,
                            qChargeurComp.ebCompagnieNum,
                            qChargeurComp.nom,
                            qChargeurComp.code,
                            qEcCurrencyInvoice.ecCurrencyNum,
                            qEcCurrencyInvoice.code,
                            qEbDemande.ebDemandeHistory,
                            qEbDemande.moduleCreator,
                            qEbDemande.listLabels,
                            qEbDemande.xEcCancelled,
                            qEbDemande.xEcNature,
                            qEbDemande.pointIntermediate().ebPartyNum,
                            qPointeIntermediate.reference,
                            qPointeIntermediate.company,
                            qEbDemande.ebDemandeMasterObject().ebDemandeNum,
                            qEbDemande.ebDemandeMasterObject().refTransport,
                            qEbDemande.ebDemandeInitial().ebDemandeNum,
                            qEbDemande.ebDemandeInitial().refTransport,
                            qUserOrigin.ebUserNum,
                            qUserOrigin.nom,
                            qUserOriginEtab.ebEtablissementNum,
                            qUserOriginEtab.nom,
                            qUserDest.ebUserNum,
                            qUserDest.nom,
                            qUserDestEtab.ebEtablissementNum,
                            qUserDestEtab.nom,
                            qUserOriginCB.ebUserNum,
                            qUserOriginCB.nom,
                            qUserOriginCBEtab.ebEtablissementNum,
                            qUserOriginCBEtab.nom,
                            qUserDestCB.ebUserNum,
                            qUserDestCB.nom,
                            qUserDestCBEtab.ebEtablissementNum,
                            qUserDestCBEtab.nom,
                            qEbDemande.listFlag,
                            qEbDemande.xEbTypeFluxNum,
                            qEbDemande.xEbTypeFluxCode,
                            qEbDemande.xEbTypeFluxDesignation,
                            qEbDemande.xEbIncotermNum,
                            qEbDemande.xEcIncotermLibelle,
                            qEbDemande.xEbSchemaPsl(),
                            qEbEntrepot,
                            qEbDemande.xEbDemandeGroup,
                            qEbDemande.carrierComment,
                            qEbDemande.askForTransportResponsibility,
                            qEbDemande.provideTransport,
                            qEbDemande.tdcAcknowledge,
                            qEbDemande.tdcAcknowledgeDate,
                            qEbDemande.eligibleForConsolidation,
                            qEbDemande.customerCreationDate,
                            qEbDemande.xEcStatutGroupage,
                            qEbDemande.listAdditionalCost,
                            qEbDemande.dateMaj,
                            qEbDemande.finalChoiceToBeCalculated,
                            qEbDemande.exportControlStatut,
							qEbDemande.isValorized,
							qEbDemande.codeConfigurationEDI
							));

            query = getGlobalWhere(query, where, criterias, connectedUser);

            query.leftJoin(qEbDemande.ebPartyDest(), qPartyDest);
            query.leftJoin(qEbDemande.ebPartyOrigin(), qPartyOrigin);
            query.leftJoin(qEbDemande.ebPartyNotif(), qPartyNotif);
            query.leftJoin(qEbDemande.pointIntermediate(), qPointeIntermediate);
            query.leftJoin(qEbDemande.xEbCostCenter(), qNamedCostCenter);
            query.leftJoin(qEbDemande.xecCurrencyInvoice(), qEcCurrencyInvoice);
            query.leftJoin(qEbDemande.ebDemandeMasterObject(), qEbDemandeMultiSegmentObject);
            query.leftJoin(qEbDemande.ebDemandeInitial(), qEbDemandeInitial);
            query.leftJoin(qEbDemande.xEbEntrepotUnloading(), qEbEntrepot);


            query.leftJoin(qEbDemande.observers, qDemandeUserObservers);

            resultat = query.fetchFirst();

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    private JPAQuery getGlobalWhere(
        JPAQuery query,
        BooleanBuilder where,
        SearchCriteriaPricingBooking criterias,
        EbUser connectedUser)
        throws Exception {
        QEbParty qCmdDestParty = new QEbParty("destParty");
        // QEbParty qCmdOriginParty = new QEbParty("originParty");
        QEbEtablissement qEtabHost = new QEbEtablissement("etabHost");
        QEbEtablissement qEtabGuest = new QEbEtablissement("etabGuest");
        QEbRelation qRelation = new QEbRelation("rel");
        QEbEtablissement qEbEtablissementCt = new QEbEtablissement("qEbEtablissementCt");
        QEbEtablissement qPartyOriginEtab = new QEbEtablissement("qPartyOriginEtab");
        QEbEtablissement qPartyDestEtab = new QEbEtablissement("qPartyDestEtab");
        QEbRelation qRelationGuest = new QEbRelation("relGuest");
        QEbRelation qRelationHost = new QEbRelation("relHost");
        QExEbDemandeTransporteur qExDemandeTransporteurCt = new QExEbDemandeTransporteur("exDemandeQuoteCt");
        QEbEtablissement qEtabQuoteCt = new QEbEtablissement("etabQuoteCt");
        QEbCompagnie qCompagnieRequestor = new QEbCompagnie("qCompagnieRequestor");

        BooleanExpression isConnectedUserIsObserverInTr = qDemandeUserObservers.observer().ebUserNum.eq(connectedUser.getEbUserNum());
        where.and(qEbDemande.xEcStatut.isNotNull());


        /** ******************CRITERES DE RECHERCHE****************** */
        if (criterias != null) {

            if (criterias.getTotalTaxableWeight() != null) {
                where.and(qEbDemande.totalTaxableWeight.eq(criterias.getTotalTaxableWeight()));
            }

            if (criterias.getGrouping() != null) {

                if (criterias.getGrouping()) {
                    where.and(qEbDemande.isGrouping.isTrue());
                }

                else {
                    where.andAnyOf(qEbDemande.isGrouping.isFalse(), qEbDemande.isGrouping.isNull());
                }

            }

            // flagEbtrackTrace
            // if (criterias.isFlagEbtrackTrace() == false)
            // {
            // where.and(qEbDemande.flagEbtrackTrace.eq(criterias.isFlagEbtrackTrace()));
            // }

            // List Destionation Country
            if (criterias.getListDestinations() != null && !criterias.getListDestinations().isEmpty()) {
                BooleanBuilder b = new BooleanBuilder();
                criterias.getListDestinations().stream().forEach(it -> {
                    b.or(qPartyDest.xEcCountry().ecCountryNum.eq(it));
                });
                where.and(b);
            }

            // List Origin Country
            if (criterias.getListOrigins() != null && !criterias.getListOrigins().isEmpty()) {
                BooleanBuilder b = new BooleanBuilder();
                criterias.getListOrigins().stream().forEach(it -> {
                    b.or(qPartyOrigin.xEcCountry().ecCountryNum.eq(it));
                });
                where.and(b);
            }

            if (criterias.getListCategorie() != null && !criterias.getListCategorie().isEmpty()) {
                BooleanBuilder b = new BooleanBuilder();
                criterias.getListCategorie().stream().forEach(it -> {
                    b.or(qEbCategorie.ebCategorieNum.eq(it));
                });
                where.and(b);
            }

            if (criterias.getListOriginCity() != null && !criterias.getListOriginCity().isEmpty()) {
                criterias.getListOriginCity().replaceAll(String::toUpperCase);
                where.and(qPartyOrigin.city.toUpperCase().in(criterias.getListOriginCity()));
            }

            if (criterias.getListDestinationCity() != null && !criterias.getListDestinationCity().isEmpty()) {
                criterias.getListDestinationCity().replaceAll(String::toUpperCase);
                where.and(qPartyDest.city.toUpperCase().in(criterias.getListDestinationCity()));
            }

            // List TransportRef
            if (criterias.getListTransportRef() != null && !criterias.getListTransportRef().isEmpty()) {
                where = where.and(qEbDemande.refTransport.in(criterias.getListTransportRef()));
            }

            if (criterias.getNumAwbBol() != null && !criterias.getNumAwbBol().isEmpty()) {
                where.and(qEbDemande.numAwbBol.toLowerCase().contains(criterias.getNumAwbBol().toLowerCase()));
            }

            // List Type of Flux
            if (criterias.getListTypeFlux() != null && !criterias.getListTypeFlux().isEmpty()) {
                where.and(qEbDemande.xEbTypeFluxNum.in(criterias.getListTypeFlux()));
            }

            // List Types demande
            if (criterias.getListTypeDemande() != null && !criterias.getListTypeDemande().isEmpty()) {
                where = where.and(qEbDemande.xEbTypeRequest.in(criterias.getListTypeDemande()));
            }

            if (criterias.getStatutGroupage() != null) {
                where.and(qEbDemande.xEcStatutGroupage.eq(criterias.getStatutGroupage()));
            }

            if (criterias.getTransportInfostatus() != null) {
                if (criterias.getTransportInfostatus() == StatutOkMissing.MISSING.getCode()) where = where
                    .andAnyOf(
                        qEbDemande.flagTransportInformation.isFalse(),
                        qEbDemande.flagTransportInformation.isNull());
                if (criterias.getTransportInfostatus() == StatutOkMissing.OK
                    .getCode()) where = where.and(qEbDemande.flagTransportInformation.isTrue());
            }

            if (criterias.getCustomDocumentStatus() != null) {
                if (criterias.getCustomDocumentStatus() == StatutOkMissing.MISSING.getCode()) where = where
                    .andAnyOf(qEbDemande.flagDocumentCustoms.isFalse(), qEbDemande.flagDocumentCustoms.isNull());
                if (criterias.getCustomDocumentStatus() == StatutOkMissing.OK
                    .getCode()) where = where.and(qEbDemande.flagDocumentCustoms.isTrue());
            }

            if (criterias.getTransportDocumentStatus() != null) {
                if (criterias.getTransportDocumentStatus() == StatutOkMissing.MISSING.getCode()) where = where
                    .andAnyOf(
                        qEbDemande.flagDocumentTransporteur.isFalse(),
                        qEbDemande.flagDocumentTransporteur.isNull());
                if (criterias.getTransportDocumentStatus() == StatutOkMissing.OK
                    .getCode()) where = where.and(qEbDemande.flagDocumentTransporteur.isTrue());
            }

            if (criterias.getListInfosDocsStatus() != null && !criterias.getListInfosDocsStatus().isEmpty()) {
                BooleanBuilder wDocs = new BooleanBuilder();

                for (Integer st: criterias.getListInfosDocsStatus()) {
                    /*
                     * désactivé pour répondre aux critères de ce jira
                     * https://jira.adias.fr/browse/MTGR-392
                     * if
                     * (st.equals(InfosDocsStatus.TRANSPORT_INFO_MISSING.getCode
                     * ()))
                     * wDocs
                     * .or(
                     * new BooleanBuilder()
                     * .andAnyOf(
                     * qEbDemande.flagTransportInformation.isFalse(),
                     * qEbDemande.flagTransportInformation.isNull()
                     * )
                     * );
                     * else if
                     * (st.equals(InfosDocsStatus.TRANSPORT_INFO_OK.getCode()))
                     * wDocs.or(qEbDemande.flagTransportInformation.isTrue());
                     * else
                     */
                    if (st.equals(InfosDocsStatus.CUSTOM_DOCUMENT_MISSING.getCode())) wDocs
                        .or(
                            new BooleanBuilder()
                                .andAnyOf(
                                    qEbDemande.flagDocumentCustoms.isFalse(),
                                    qEbDemande.flagDocumentCustoms.isNull()));
                    else if (st.equals(InfosDocsStatus.CUSTOM_DOCUMENT_OK.getCode())) wDocs
                        .or(qEbDemande.flagDocumentCustoms.isTrue());

                    else if (st.equals(InfosDocsStatus.TRANSPORT_DOCUMENT_MISSING.getCode())) wDocs
                        .or(
                            new BooleanBuilder()
                                .andAnyOf(
                                    qEbDemande.flagDocumentTransporteur.isFalse(),
                                    qEbDemande.flagDocumentTransporteur.isNull()));
                    else if (st.equals(InfosDocsStatus.TRANSPORT_DOCUMENT_OK.getCode())) wDocs
                        .or(qEbDemande.flagDocumentTransporteur.isTrue());

                    else if (st >= StatutDemande.WPU.getCode()) {
                        wDocs.or(qEbDemande.xEcStatut.eq(st));
                    }
                }

                where.and(wDocs);
            }

            // Liste commandes (customer references )
            if (criterias.getListCustomerReference() != null && !criterias.getListCustomerReference().isEmpty()) {
                where.and(qEbDemande.customerReference.in(criterias.getListCustomerReference()));
            }

            // Date Pickup From To
            if (criterias.getDatePickUpFrom() != null) {
                where
                    .andAnyOf(
                        qExDemandeTransporteur.pickupTime.eq(criterias.getDatePickUpFrom()),
                        qExDemandeTransporteur.pickupTime.after(criterias.getDatePickUpFrom()));
            }

            if (criterias.getDatePickUpTo() != null) {
                where
                    .andAnyOf(
                        qExDemandeTransporteur.pickupTime.eq(criterias.getDatePickUpTo()),
                        qExDemandeTransporteur.pickupTime.before(criterias.getDatePickUpTo()));
            }

            // Date Pickup From To
            if (criterias.getFinalDeliveryFrom() != null) {
                where
                    .andAnyOf(
                        qExDemandeTransporteur.deliveryTime.eq(criterias.getFinalDeliveryFrom()),
                        qExDemandeTransporteur.deliveryTime.after(criterias.getFinalDeliveryFrom()));
            }

            if (criterias.getFinalDeliveryTo() != null) {
                where
                    .andAnyOf(
                        qExDemandeTransporteur.deliveryTime.eq(criterias.getFinalDeliveryTo()),
                        qExDemandeTransporteur.deliveryTime.before(criterias.getFinalDeliveryTo()));
            }

            // Date Pickup From To
            if (criterias.getExpectedDateFrom() != null) {
                where
                    .andAnyOf(
                        qExDemandeTransporteur.pickupTime.eq(criterias.getExpectedDateFrom()),
                        qExDemandeTransporteur.pickupTime.after(criterias.getExpectedDateFrom()));
            }

            if (criterias.getExpectedDateTo() != null) {
                where
                    .andAnyOf(
                        qExDemandeTransporteur.pickupTime.eq(criterias.getExpectedDateTo()),
                        qExDemandeTransporteur.pickupTime.before(criterias.getExpectedDateTo()));
            }

            if (criterias.getDateCreationFrom() != null) {
                where
                    .andAnyOf(
                        qEbDemande.dateCreation.eq(criterias.getDateCreationFrom()),
                        qEbDemande.dateCreation.after(criterias.getDateCreationFrom()));
            }

            if (criterias.getDateCreationTo() != null) {
                where
                    .andAnyOf(
                        qEbDemande.dateCreation.eq(criterias.getDateCreationTo()),
                        qEbDemande.dateCreation.before(criterias.getDateCreationTo()));
            }

            if (criterias.getDateOfGoodsAvailability() != null) {
                where
                    .andAnyOf(
                        qEbDemande.dateOfGoodsAvailability.eq(criterias.getDateOfGoodsAvailability()),
                        qEbDemande.dateOfGoodsAvailability.after(criterias.getDateOfGoodsAvailability()));
            }

            // dateTraitementCPB
            if (criterias.isFlagDateTraitementCPB()) {
                where.and(qEbDemande.dateTraitementCPB.isNotNull());
            }

            // List ModeTransporteur
            if (criterias.getListModeTransport() != null && !criterias.getListModeTransport().isEmpty()) {
                where.and(qEbDemande.xEcModeTransport.in(criterias.getListModeTransport()));
            }

            if (criterias.getListCostCenter() != null && !criterias.getListCostCenter().isEmpty()) {
                where.and(qEbDemande.xEbCostCenter().libelle.in(criterias.getListCostCenter()));
            }

            if (criterias.getListEbDemandeNum() != null && !criterias.getListEbDemandeNum().isEmpty()) where
                .and(qEbDemande.ebDemandeNum.in(criterias.getListEbDemandeNum()));

            if (criterias.getxEcNature() != null) where.and(qEbDemande.xEcNature.in(criterias.getxEcNature()));

            if (criterias.getListUnitsReference() != null && !criterias.getListUnitsReference().isEmpty()) {
                where.and(qEbDemande.unitsReference.in(criterias.getListUnitsReference()));
            }

            if (criterias.getListunitsPackingList() != null && !criterias.getListunitsPackingList().isEmpty()) {
                where.and(qEbDemande.unitsPackingList.in(criterias.getListunitsPackingList()));
            }

            if (criterias.getListNumAwbBol() != null && !criterias.getListNumAwbBol().isEmpty()) {
                where.and(qEbDemande.numAwbBol.in(criterias.getListNumAwbBol()));
            }

            if (criterias.getListCarrierUniqRefNum() != null && !criterias.getListCarrierUniqRefNum().isEmpty()) {
                where.and(qEbDemande.carrierUniqRefNum.in(criterias.getListCarrierUniqRefNum()));
            }

            if (criterias.getTotalWeight() != null) {
                where.and(qEbDemande.totalWeight.eq(criterias.getTotalWeight()));
            }

            if (criterias.getListEbCostCenter() != null && !criterias.getListEbCostCenter().isEmpty()) {
                where.and(qEbDemande.xEbCostCenter().ebCostCenterNum.in(criterias.getListEbCostCenter()));
            }

            if (criterias.getUpdateDateFrom() != null) {
                where
                    .andAnyOf(
                        qEbDemande.updateDate.eq(criterias.getUpdateDateFrom()),
                        qEbDemande.updateDate.after(criterias.getUpdateDateFrom()));
            }

            if (criterias.getUpdateDateTo() != null) {
                where
                    .andAnyOf(
                        qEbDemande.updateDate.eq(criterias.getUpdateDateTo()),
                        qEbDemande.updateDate.before(criterias.getUpdateDateTo()));
            }

            if (criterias.getListEbUserCt() != null && !criterias.getListEbUserCt().isEmpty()) {
                where.and(qEbDemande.xEbUserCt().ebUserNum.in(criterias.getListEbUserCt()));
            }

            if (criterias.getListUser() != null && !criterias.getListUser().isEmpty()) {
                where.and(qEbDemande.user().ebUserNum.in(criterias.getListUser()));
            }

            if (criterias.getPrice() != null) {
                where.and(qExDemandeTransporteur.price.eq(criterias.getPrice()));
            }

            if (criterias.getLibelleLastPsl() != null) {
                where.and(qEbDemande.libelleLastPsl.equalsIgnoreCase(criterias.getLibelleLastPsl()));
            }

            if (criterias.getDateOfGoodsAvailabilityFrom() != null) {
                where
                    .andAnyOf(
                        qEbDemande.dateOfGoodsAvailability.eq(criterias.getDateOfGoodsAvailabilityFrom()),
                        qEbDemande.dateOfGoodsAvailability.after(criterias.getDateOfGoodsAvailabilityFrom()));
            }

            if (criterias.getDateOfGoodsAvailabilityTo() != null) {
                where
                    .andAnyOf(
                        qEbDemande.dateOfGoodsAvailability.eq(criterias.getDateOfGoodsAvailabilityTo()),
                        qEbDemande.dateOfGoodsAvailability.before(criterias.getDateOfGoodsAvailabilityTo()));
            }

            if (criterias.getListunitsPackingList() != null && !criterias.getListunitsPackingList().isEmpty()) {
                where.and(qEbDemande.unitsPackingList.in(criterias.getListunitsPackingList()));
            }

            if (criterias.getCustomerCreationDateFrom() != null) {
                where
                    .andAnyOf(
                        qEbDemande.customerCreationDate.eq(criterias.getCustomerCreationDateFrom()),
                        qEbDemande.customerCreationDate.after(criterias.getCustomerCreationDateFrom()));
            }

            if (criterias.getCustomerCreationDateTo() != null) {
                where
                    .andAnyOf(
                        qEbDemande.customerCreationDate.eq(criterias.getCustomerCreationDateTo()),
                        qEbDemande.customerCreationDate.before(criterias.getCustomerCreationDateTo()));
            }

            if (criterias.getListEtablissementNum() != null && !criterias.getListEtablissementNum().isEmpty()) {
                where
                    .and(
                        qExDemandeTransporteur.xEbEtablissement().ebEtablissementNum
                            .in(criterias.getListEtablissementNum()));
            }

            if (criterias.getListCompanyName() != null && !criterias.getListCompanyName().isEmpty()) {
                where.and(qExDemandeTransporteur.xEbCompagnie().nom.in(criterias.getListCompanyName()));
            }

            if (criterias.getListEmailTransporteur() != null && !criterias.getListEmailTransporteur().isEmpty()) {
                where.and(qExDemandeTransporteur.xTransporteur().email.in(criterias.getListEmailTransporteur()));
            }

            if (criterias.getDatePickupTMFrom() != null) {
                where
                    .andAnyOf(
                        qEbDemande.datePickupTM.eq(criterias.getDatePickupTMFrom()),
                        qEbDemande.datePickupTM.after(criterias.getDatePickupTMFrom()));
            }

            if (criterias.getDatePickupTMTo() != null) {
                where
                    .andAnyOf(
                        qEbDemande.datePickupTM.eq(criterias.getDatePickupTMTo()),
                        qEbDemande.datePickupTM.before(criterias.getDatePickupTMTo()));
            }

            if (criterias.getFinalDeliveryFrom() != null) {
                where
                    .andAnyOf(
                        qEbDemande.finalDelivery.eq(criterias.getFinalDeliveryFrom()),
                        qEbDemande.finalDelivery.after(criterias.getFinalDeliveryFrom()));
            }

            if (criterias.getFinalDeliveryTo() != null) {
                where
                    .andAnyOf(
                        qEbDemande.finalDelivery.eq(criterias.getFinalDeliveryTo()),
                        qEbDemande.finalDelivery.before(criterias.getFinalDeliveryTo()));
            }

            // List status
            if (criterias.getListStatut() != null && !criterias.getListStatut().isEmpty()) {
                BooleanBuilder w = new BooleanBuilder();
                List<Integer> listCodeStatus = new ArrayList<Integer>();
                for (Integer st: criterias.getListStatut()) listCodeStatus.add(st);
                Integer statusConsolidated = null, statusNotAwarded = null, statusConfirmed = null,
                    statusArchived = null, statusNotArchived = null, statusCancelled = null;

                for (Integer st: listCodeStatus) {
                    if (Enumeration.StatutDemande.FIN.getCode().equals(st)) statusConfirmed = st;
                    else if (Enumeration.StatutDemande.NAW.getCode().equals(st)) {
                        if (connectedUser.isTransporteur()
                            || connectedUser.isTransporteurBroker()) statusNotAwarded = st;
                        else statusConfirmed = Enumeration.StatutDemande.FIN.getCode();
                    }
                    else if (Enumeration.StatutDemande.ARC.getCode().equals(st)) statusArchived = st;
                    else if (Enumeration.StatutDemande.NARC.getCode().equals(st)) statusNotArchived = st;
                    else if (Enumeration.StatutDemande.CAN.getCode().equals(st)) statusCancelled = st;
                    else if (Enumeration.StatutDemande.CSL.getCode().equals(st)) statusConsolidated = st;
                    if (statusConsolidated != null && statusCancelled != null && statusNotAwarded != null
                        && statusConfirmed != null && statusArchived != null && statusNotArchived != null) break;
                }

                if (statusConsolidated != null) {
                    w.orAllOf(qEbDemande.flagConsolidated.isNotNull(), qEbDemande.flagConsolidated.isTrue());
                }

                if (statusNotAwarded != null || statusArchived != null) {
                    if (statusNotAwarded != null) listCodeStatus.remove(statusNotAwarded);

                    if (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker()) {
                        w
                            .orAllOf(
                                qEbDemande.xEbTransporteurEtablissementNum.isNotNull(),
                                qEbDemande.xEbTransporteurEtablissementNum
                                    .ne(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                qEbDemande.xEcStatut.gt(Enumeration.StatutDemande.FIN.getCode()));
                    }

                }

                if (statusConfirmed != null) {
                    if (statusConfirmed != null) listCodeStatus.remove(statusConfirmed);
                    if (statusArchived != null) listCodeStatus.remove(statusArchived);

                    if (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker()) {
                        w
                            .orAllOf(
                                qEbDemande.xEbTransporteurEtablissementNum.isNotNull(),
                                qEbDemande.xEbTransporteurEtablissementNum
                                    .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));
                    }
                    else {
                        w.or(qEbDemande.xEbTransporteurEtablissementNum.isNotNull());
                    }

                }

                if (statusCancelled != null) {
                    listCodeStatus.remove(statusCancelled);
                    w.or(qEbDemande.xEcCancelled.isNotNull());
                }

                if (statusArchived != null) {
                    listCodeStatus.remove(statusArchived);

                    if (criterias.getModule().equals(Enumeration.Module.TRANSPORT_MANAGEMENT.getCode())) {
                        w
                            .andAnyOf(
                                qEbDemande.xEcStatut.eq(StatutDemande.DLVRY.getCode()),
                                qEbDemande.xEcCancelled.isNotNull());
                    }
                    else {
                        w
                            .andAnyOf(
                                qEbDemande.xEcStatut.goe(Enumeration.StatutDemande.WPU.getCode()),
                                qEbDemande.xEcCancelled.isNotNull());
                    }

                }

                if (statusNotArchived != null) {
                    listCodeStatus.remove(statusNotArchived);

                    if (criterias.getModule().equals(Enumeration.Module.TRANSPORT_MANAGEMENT.getCode())) {
                        w
                            .orAllOf(
                                qEbDemande.xEcStatut
                                    .between(Enumeration.StatutDemande.NARC.getCode(), StatutDemande.TO.getCode()),
                                qEbDemande.xEcCancelled.isNull());
                    }
                    else {
                        // w.or(qEbDemande.xEcStatut.lt(Enumeration.StatutDemande.WPU.getCode()));
                        w
                            .orAllOf(
                                qEbDemande.xEcStatut.lt(Enumeration.StatutDemande.WPU.getCode()),
                                qEbDemande.xEcCancelled.isNull());
                    }

                }

                if (!listCodeStatus.isEmpty()) {
                    w
                        .or(
                            new BooleanBuilder()
                                .orAllOf(qEbDemande.xEcStatut.in(listCodeStatus), qEbDemande.xEcCancelled.isNull()));
                }

                where.and(w);
            }
            else if (criterias.getEbDemandeNum() == null
                && (criterias.getListEbDemandeNum() == null || criterias.getListEbDemandeNum().isEmpty())) {

                if (Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(criterias.getModule())) {
                    where.and(qEbDemande.xEcStatut.goe(StatutDemande.WPU.getCode()));
                }
                else {
                    where.and(qEbDemande.xEcStatut.goe(StatutDemande.INP.getCode()));
                }

            }

            // Liste carriers
            if (criterias.getListCarrier() != null && !criterias.getListCarrier().isEmpty()) {
                where.and(qTransporteur.ebUserNum.in(criterias.getListCarrier()));
            }

            if (criterias.getEbEtablissementNum() != null) {
                where.and(qEtablissement.ebEtablissementNum.eq(criterias.getEbEtablissementNum()));
            }

            if (criterias.getEbDemandeNum() != null) {
                where.and(qEbDemande.ebDemandeNum.eq(criterias.getEbDemandeNum()));
            }

            if(connectedUser.isChargeur() && !connectedUser.isSuperAdmin()) {
                where.or(isConnectedUserIsObserverInTr);
            }

            if (criterias.getListIncoterm() != null && !criterias.getListIncoterm().isEmpty()) {
                where.and(qEbDemande.xEcIncotermLibelle.in(criterias.getListIncoterm()));
            }

            if (criterias.getCompanyOrigin() != null && !criterias.getCompanyOrigin().trim().isEmpty()) {
                where.and(qPartyOrigin.company.toLowerCase().contains(criterias.getCompanyOrigin()));
            }

            criterias.applyAdvancedSearchForCustomFields(where, qEbDemande.customFields);

            if (criterias.getListCustomFieldsValues() != null && !criterias.getListCustomFieldsValues().isEmpty()) {
                Predicate[] predicats = new Predicate[criterias.getListCustomFieldsValues().size()];

                for (int i = 0; i < criterias.getListCustomFieldsValues().size(); i++) {
                    predicats[i] = qEbDemande.listCustomFieldsValueFlat
                        .contains(criterias.getListCustomFieldsValues().get(i));
                }

                where.andAnyOf(predicats);
            }

            if (criterias.getCarrierUniqRefNum() != null && !criterias.getCarrierUniqRefNum().isEmpty()) {
                where.and(qEbDemande.carrierUniqRefNum.contains(criterias.getCarrierUniqRefNum()));
            }

            /*
             * where.and(new BooleanBuilder().orAllOf(
             * qEbDemande.listCategoriesStr.toLowerCase().like(
             * "%\"libelle\":%\"country\"%")
             * ));
             */

            /*
             * CAS DE LA RECHERCHE DE LA DEMANDE PAR LES DONNEES DE LA LIVRAISON
             */
            if (criterias.getRequestedDeliveryDateFrom() != null) {
                criterias.setSearchDemandeByLivraisonData(true);
                where
                    .andAnyOf(
                        qEbLivraison.requestedDeliveryDate.eq(criterias.getRequestedDeliveryDateFrom()),
                        qEbLivraison.requestedDeliveryDate.after(criterias.getDateCreationFrom()));
            }

            if (criterias.getRequestedDeliveryDateTo() != null) {
                criterias.setSearchDemandeByLivraisonData(true);
                where
                    .andAnyOf(
                        qEbLivraison.requestedDeliveryDate.eq(criterias.getRequestedDeliveryDateTo()),
                        qEbLivraison.requestedDeliveryDate.before(criterias.getRequestedDeliveryDateTo()));
            }

            if (criterias.getListNumOrderSAP() != null && !criterias.getListNumOrderSAP().isEmpty()) {
                criterias.setSearchDemandeByLivraisonData(true);
                where.and(qEbLivraison.numOrderSAP.in(criterias.getListNumOrderSAP()));
            }

            if (criterias.getListNumOrderEDI() != null && !criterias.getListNumOrderEDI().isEmpty()) {
                criterias.setSearchDemandeByLivraisonData(true);
                where.and(qEbLivraison.numOrderEDI.in(criterias.getListNumOrderEDI()));
            }

            if (criterias.getListNumOrderCustomer() != null && !criterias.getListNumOrderCustomer().isEmpty()) {
                criterias.setSearchDemandeByLivraisonData(true);
                where.and(qEbLivraison.numOrderCustomer.in(criterias.getListNumOrderCustomer()));
            }

            if (criterias.getListCampaignCode() != null && !criterias.getListCampaignCode().isEmpty()) {
                criterias.setSearchDemandeByLivraisonData(true);
                where.and(qEbLivraison.campaignCode.in(criterias.getListCampaignCode()));
            }

            if (criterias.getListCampaignName() != null && !criterias.getListCampaignName().isEmpty()) {
                criterias.setSearchDemandeByLivraisonData(true);
                where.and(qEbLivraison.campaignName.in(criterias.getListCampaignName()));
            }

            if (criterias.getListReferenceDelivery() != null && !criterias.getListReferenceDelivery().isEmpty()) {
                criterias.setSearchDemandeByLivraisonData(true);
                where.and(qEbLivraison.refDelivery.in(criterias.getListReferenceDelivery()));
            }

            if (criterias.getListRefCustomerDelivery() != null && !criterias.getListRefCustomerDelivery().isEmpty()) {
                criterias.setSearchDemandeByLivraisonData(true);
                where.and(qEbLivraison.refClientDelivered.in(criterias.getListRefCustomerDelivery()));
            }

            if (criterias.getListNameCustomerDelivery() != null && !criterias.getListNameCustomerDelivery().isEmpty()) {
                criterias.setSearchDemandeByLivraisonData(true);
                where.and(qEbLivraison.nameClientDelivered.in(criterias.getListNameCustomerDelivery()));
            }
            /*
             * FIN CAS DE LA RECHERCHE DE LA DEMANDE PAR LES DONNEES DE LA
             * LIVRAISON
             */

            /* CAS DE LA RECHERCHE DE LA DEMANDE PAR LES DONNEES DE */
            if (criterias.getListUnitReference() != null && !criterias.getListUnitReference().isEmpty()) {
                criterias.setSearchDemandeByMarchandisenData(true);
                where.and(qEbMarchandise.unitReference.in(criterias.getListUnitReference()));
            }

            if (criterias.getListLot() != null && !criterias.getListLot().isEmpty()) {
                criterias.setSearchDemandeByMarchandisenData(true);
                where.and(qEbMarchandise.lot.in(criterias.getListLot()));
            }
            /* FIN CAS DE LA RECHERCHE DE LA DEMANDE PAR LES DONNEES DE */

            BooleanBuilder boolLabels = new BooleanBuilder();

            if (criterias.getMapCategoryFields() != null && !criterias.getMapCategoryFields().isEmpty()) {
                BooleanBuilder orBoolLabels = null;

                List<Integer> ebLabels = null;

                for (String key: criterias.getMapCategoryFields().keySet()) {
                    ebLabels = criterias.getMapCategoryFields().get(key);

                    if (ebLabels != null) {
                        orBoolLabels = new BooleanBuilder();
                        int i, n = ebLabels.size();

                        for (i = 0; i < n; i++) {
                            Long ebLabelNum = Math.round(Double.parseDouble(ebLabels.get(i) + ""));
                            orBoolLabels
                                .or(
                                    new BooleanBuilder()
                                        .andAnyOf(
                                            qEbDemande.listLabels.startsWith(ebLabelNum + ","),
                                            qEbDemande.listLabels.contains("," + ebLabelNum + ","),
                                            qEbDemande.listLabels.endsWith("," + ebLabelNum),
                                            qEbDemande.listLabels.eq(ebLabelNum + "")));
                        }

                        if (orBoolLabels.hasValue()) boolLabels.and(orBoolLabels);
                    }

                }

                /*
                 * if (!boolLabels.hasValue())
                 * boolLabels.and(qEbDemande.listLabels.isEmpty());
                 */
                if (boolLabels.hasValue()) where.andAnyOf(qEbDemande.listLabels.isNotNull().andAnyOf(boolLabels));
            }

            if (criterias.getTransportConfirmation() != null) {

                if (criterias.getTransportConfirmation().size() == TransportConfirmation.values().length) {
                    where
                        .and(
                            qEbDemande.askForTransportResponsibility
                                .isTrue()
                                .and(qEbDemande.provideTransport.isFalse().or(qEbDemande.provideTransport.isNull())))
                        .or(
                            qEbDemande.askForTransportResponsibility
                                .isTrue().and(qEbDemande.provideTransport.isTrue()));
                }
                else {

                    if (criterias
                        .getTransportConfirmation()
                        .contains(TransportConfirmation.WAITING_FOR_CONFIRMATION.getCode())) {
                        where
                            .and(qEbDemande.askForTransportResponsibility.isTrue())
                            .and(qEbDemande.provideTransport.isFalse().or(qEbDemande.provideTransport.isNull()));
                    }

                    if (criterias.getTransportConfirmation().contains(TransportConfirmation.CONFIRMED.getCode())) {
                        where
                            .and(qEbDemande.askForTransportResponsibility.isTrue())
                            .and(qEbDemande.provideTransport.isTrue());
                    }

                }

            }

            if (criterias.getAcknoledgeByCt() != null) {

                if (criterias.getAcknoledgeByCt() == YesOrNo.No.getCode()) {
                    where.and(qEbDemande.tdcAcknowledge.isFalse().or(qEbDemande.tdcAcknowledge.isNull()));
                }
                else {
                    where.and(qEbDemande.tdcAcknowledge.isTrue());
                }

            }

            if (criterias.getAcknoledgeByCtDate() != null) {
                query
                    .where(
                        Expressions
                            .comparableTemplate(Date.class, "DATE({0})", qEbDemande.tdcAcknowledgeDate)
                            .eq(criterias.getAcknoledgeByCtDate()));
            }

            if (criterias.getEligibleForConsolidation() != null) {

                if (criterias.getEligibleForConsolidation() == YesOrNo.No.getCode()) {
                    where
                        .and(
                            qEbDemande.eligibleForConsolidation
                                .isFalse().or(qEbDemande.eligibleForConsolidation.isNull()));
                }
                else {
                    where.and(qEbDemande.eligibleForConsolidation.isTrue());
                }

            }

            if (criterias.getInsurance() != null) {

                if (criterias.getInsurance() == YesOrNo.No.getCode()) {
                    where.and(qEbDemande.insurance.isFalse().or(qEbDemande.insurance.isNull()));
                }
                else {
                    where.and(qEbDemande.insurance.isTrue());
                }

            }

            if (criterias.getInvoiceCurrency() != null) {
                where.and(qEbDemande.xecCurrencyInvoice().ecCurrencyNum.eq(criterias.getInvoiceCurrency()));
            }

            // if (criterias.getxEbSchemaPslDesignation() != null)
            // {
            // where.and(qEbDemande.xEbSchemaPsl().designation.eq(criterias.getxEbSchemaPslDesignation()));
            // }

            if (criterias.getListValuationType() != null) {
                where.and(qEbDemande.valuationType.in(criterias.getListValuationType()));
            }

            if (criterias.getIncotermCity() != null) {
                where.and(qEbDemande.city.equalsIgnoreCase(criterias.getIncotermCity()));
            }

            if (criterias.getTotalVolume() != null) {
                where.and(qEbDemande.totalVolume.eq(criterias.getTotalVolume()));
            }

            if (criterias.getTotal_weight() != null) {
                where.and(qEbDemande.totalWeight.eq(criterias.getTotal_weight()));
            }

            if (criterias.getTotalNbrParcel() != null) {
                where.and(qEbDemande.totalNbrParcel.eq(criterias.getTotalNbrParcel()));
            }

            if (criterias.getWaitingForConfDateFrom() != null) {
                where
                    .and(
                        qEbDemande.waitingForConf
                            .goe(
                                DateUtils
                                    .setDate_To_The_Start_Of_The_Day_Month(
                                        criterias.getWaitingForConfDateFrom(),
                                        false)));
            }

            if (criterias.getWaitingForConfDateTo() != null) {
                where
                    .and(
                        qEbDemande.waitingForConf
                            .loe(
                                DateUtils
                                    .setDate_To_The_End_Of_The_Day_And_Month(
                                        criterias.getWaitingForConfDateTo(),
                                        false)));
            }

            if (criterias.getWaitingForQuoteDateFrom() != null) {
                where
                    .and(
                        qEbDemande.waitingForQuote
                            .goe(
                                DateUtils
                                    .setDate_To_The_Start_Of_The_Day_Month(
                                        criterias.getWaitingForQuoteDateFrom(),
                                        false)));
            }

            if (criterias.getWaitingForQuoteDateTo() != null) {
                where
                    .and(
                        qEbDemande.waitingForQuote
                            .loe(
                                DateUtils
                                    .setDate_To_The_End_Of_The_Day_And_Month(
                                        criterias.getWaitingForQuoteDateTo(),
                                        false)));
            }

            if (criterias.getConfirmationDateFrom() != null) {
                where
                    .and(
                        qEbDemande.confirmed
                            .goe(
                                DateUtils
                                    .setDate_To_The_Start_Of_The_Day_Month(
                                        criterias.getConfirmationDateFrom(),
                                        false)));
            }

            if (criterias.getConfirmationDateTo() != null) {
                where
                    .and(
                        qEbDemande.confirmed
                            .loe(
                                DateUtils
                                    .setDate_To_The_End_Of_The_Day_And_Month(
                                        criterias.getConfirmationDateTo(),
                                        false)));
            }

            if (criterias.getSearch() != null) {
                String value = criterias.getSearch().get("value");
                String dt = null;
                String valueModeTr = "";
                String valueTypeTr = "";
                Boolean testBoolean = false;
                String missingDoc = "missing";
                String docOk = "ok";
                Boolean verifExisteBoolean = false;

                if (value != null && !value.isEmpty()) {
                    value = value.trim().toLowerCase();

                    dt = Statiques.convertSubDateToEngFormat(value);

                    if (missingDoc.toLowerCase().contains(value)) {
                        testBoolean = false;
                        verifExisteBoolean = true;
                    }
                    else if (docOk.toLowerCase().contains(value)) {
                        testBoolean = true;
                        verifExisteBoolean = true;
                    }

                    if (value.contains("(")) {

                        try {
                            valueModeTr = value.replace(")", "");
                            String[] vs = valueModeTr.split("\\(");
                            valueModeTr = vs[0].trim();
                            valueTypeTr = vs[1].trim();
                        } catch (Exception e) {
                        }

                    }
                    else {
                        valueModeTr = value.toString();
                    }

                    List<Integer> listCodeModeTransport = Enumeration.ModeTransport.getListCodeByLibelle(valueModeTr);
                    Integer modeTrCode = Enumeration.ModeTransport.getCodeByLibelle(valueModeTr);
                    EbTypeTransport typeTransport = ebTypeTransportRepository
                        .findEbTypeTransportByLibeleAndModeTransport(valueTypeTr, modeTrCode);
                    Integer codeTypeTransport = typeTransport != null ? typeTransport.getEbTypeTransportNum() : null;

                    Integer codeTypeDemande = Enumeration.TypeDemande.getCodeByLibelle(value);
                    List<Integer> listCodeStatus = Enumeration.StatutDemande.getListCodeByLibelle(value);

                    if (listCodeModeTransport == null
                        || listCodeModeTransport.isEmpty()) listCodeModeTransport = new ArrayList<Integer>();
                    if (listCodeStatus == null || listCodeStatus.isEmpty()) listCodeStatus = new ArrayList<Integer>();
                    if (codeTypeDemande == null) codeTypeDemande = -1;

                    BooleanBuilder w = new BooleanBuilder();
                    w
                        .andAnyOf(
                            qEbDemande.unitsReference.toLowerCase().contains(value),
                            qEbDemande.refTransport.toLowerCase().contains(value),
                            qEbDemande.customerReference.toLowerCase().contains(value),
                            qEbDemande.libelleOriginCountry.toLowerCase().contains(value),
                            qEbDemande.libelleDestCountry.toLowerCase().contains(value),
                            qEbDemande.libelleLastPsl.toLowerCase().contains(value),
                            qEbDemande.totalWeight.stringValue().toLowerCase().contains(value),
                            qEbDemande.dateCreation.stringValue().toLowerCase().contains(value),
                            qEbDemande.xEcTypeDemande.eq(codeTypeDemande),
                            qEbDemande.ebPartyOrigin().company.toLowerCase().contains(value),
                            qEbDemande.ebPartyDest().company.toLowerCase().contains(value),
                            qEbDemande.xEcIncotermLibelle.stringValue().toLowerCase().contains(value),
                            qEbDemande.numAwbBol.stringValue().toLowerCase().contains(value),
                            qEbDemande.carrierUniqRefNum.stringValue().toLowerCase().contains(value),
                            qEbDemande.libelleOriginCity.stringValue().toLowerCase().contains(value),
                            qEbDemande.libelleDestCity.stringValue().toLowerCase().contains(value),
                            qEbDemande.partNumber.toLowerCase().contains(value),
                            qEbDemande.orderNumber.toLowerCase().contains(value),
                            qEbDemande.shippingType.toLowerCase().contains(value),
                            qEbDemande.serialNumber.toLowerCase().contains(value),
                            qEbDemande.listCustomFieldsFlat.toLowerCase().like("%:\"%" + value + "%\"%"),
                            qEbDemande.listCategoryFlat.toLowerCase().like("%:\"%" + value + "%\"%"),
                            qEbDemande.typeRequestLibelle.toLowerCase().contains(value),
                            qEbDemande.totalWeight.stringValue().toLowerCase().contains(value),
                            qEbDemande.dateCreation.stringValue().contains(toPostgresDate(value)),
                            qChargeur.nom.toLowerCase().contains(value),
                            qChargeur.prenom.toLowerCase().contains(value),
                            qEbUserCt.nom.toLowerCase().contains(value),
                            qEbUserCt.prenom.toLowerCase().contains(value),
                            qEbEntrepot.reference.toLowerCase().contains(value),
                            qEbEntrepot.libelle.toLowerCase().contains(value),
                            qPartyOrigin.company.toLowerCase().contains(value)

                        );

                    if (verifExisteBoolean) {
                        BooleanBuilder whereBoolean = new BooleanBuilder();

                        if (testBoolean) {
                            whereBoolean
                                .andAnyOf(
                                    qEbDemande.flagDocumentTransporteur.isTrue(),
                                    qEbDemande.flagTransportInformation.isTrue(),
                                    qEbDemande.flagDocumentCustoms.isTrue());
                        }
                        else if (!testBoolean) {
                            whereBoolean
                                .andAnyOf(
                                    qEbDemande.flagDocumentTransporteur
                                        .isNull().or(qEbDemande.flagDocumentTransporteur.isFalse()));
                            whereBoolean
                                .andAnyOf(
                                    qEbDemande.flagTransportInformation
                                        .isNull().or(qEbDemande.flagTransportInformation.isFalse()));
                            whereBoolean
                                .andAnyOf(
                                    qEbDemande.flagDocumentCustoms
                                        .isNull().or(qEbDemande.flagDocumentCustoms.isFalse()));
                        }

                        w.or(whereBoolean);
                    }

                    if (dt != null) {
                        w.or(qEbDemande.dateCreation.stringValue().toLowerCase().contains(dt));
                        w.or(qEbDemande.dateOfGoodsAvailability.stringValue().toLowerCase().contains(dt));
                        w.or(qEbDemande.finalDelivery.stringValue().toLowerCase().contains(dt));
                        w.or(qEbDemande.dateLastPsl.stringValue().toLowerCase().contains(dt));
                        w.or(qEbDemande.datePickupTM.stringValue().toLowerCase().contains(dt));
                    }

                    if (!listCodeStatus.isEmpty()) {
                        Integer statusNotAwarded = null, statusConfirmed = null, statusArchived = null,
                            statusNotArchived = null;

                        for (Integer st: listCodeStatus) {
                            if (Enumeration.StatutDemande.FIN.getCode().equals(st)) statusConfirmed = st;
                            else if (Enumeration.StatutDemande.NAW.getCode().equals(st)) statusNotAwarded = st;
                            else if (Enumeration.StatutDemande.ARC.getCode().equals(st)) statusArchived = st;
                            else if (Enumeration.StatutDemande.NARC.getCode().equals(st)) statusNotArchived = st;
                            if (statusNotAwarded != null && statusConfirmed != null && statusArchived != null
                                && statusNotArchived != null) break;
                        }

                        if (statusNotAwarded != null || statusArchived != null) {
                            if (statusNotAwarded != null) listCodeStatus.remove(statusNotAwarded);

                            if (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker()) {
                                w
                                    .orAllOf(
                                        qEbDemande.xEbTransporteurEtablissementNum.isNotNull(),
                                        qEbDemande.xEbTransporteurEtablissementNum
                                            .ne(connectedUser.getEbEtablissement().getEbEtablissementNum()));
                            }

                        }

                        if (statusConfirmed != null || statusArchived != null) {
                            if (statusConfirmed != null) listCodeStatus.remove(statusConfirmed);
                            if (statusArchived != null) listCodeStatus.remove(statusArchived);

                            if (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker()) {
                                w
                                    .orAllOf(
                                        qEbDemande.xEbTransporteurEtablissementNum.isNotNull(),
                                        qEbDemande.xEbTransporteurEtablissementNum
                                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));
                            }
                            else {
                                w.or(qEbDemande.xEbTransporteurEtablissementNum.isNotNull());
                            }

                        }

                        if (statusArchived != null) {
                            w.or(qEbDemande.xEcCancelled.isNotNull());
                        }

                        if (statusNotArchived != null) {
                            listCodeStatus.remove(statusNotArchived);
                            w.or(qEbDemande.xEcStatut.lt(Enumeration.StatutDemande.WPU.getCode()));
                        }

                        if (!listCodeStatus.isEmpty()) {
                            w.or(qEbDemande.xEcStatut.in(listCodeStatus));
                        }

                    }

                    if (codeTypeTransport == null && !listCodeModeTransport.isEmpty()) w
                        .or(qEbDemande.xEcModeTransport.in(listCodeModeTransport));

                    if (codeTypeTransport != null && !listCodeModeTransport.isEmpty()) {
                        w
                            .orAllOf(
                                qEbDemande.xEcModeTransport.in(listCodeModeTransport),
                                qEbDemande.xEbTypeTransport.eq(codeTypeTransport));
                    }

                    if (Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(criterias.getModule())
                        || criterias.getModule() == Enumeration.Module.ORDRE_TRANSPORT.getCode()) {
                        w
                            .or(
                                new BooleanBuilder()
                                    .andAnyOf(
                                        qEbDemande.refTransport.toLowerCase().contains(value),
                                        qEbDemande.xEbCostCenter().libelle.toLowerCase().contains(value)));

                        w
                            .or(
                                new BooleanBuilder()
                                    .andAnyOf(
                                        qTransporteur.email.toLowerCase().contains(value),
                                        qTransporteur.nom.toLowerCase().contains(value),
                                        qTransporteur.prenom.toLowerCase().contains(value),
                                        qEtablissement.nom.toLowerCase().contains(value),
                                        qCompagnie.nom.toLowerCase().contains(value),
                                        qExDemandeTransporteur.price.stringValue().toLowerCase().contains(value)));
                    }

                    where.and(w);
                }

            }

            /******************** VISIBLITE DES USERS *******************/
            BooleanBuilder whereVisibilite = applyVisibilityRules(
                connectedUser,
                criterias.getModule(),
                qEbEtablissementCt,
                qEtablissement,
                qChargeurEtab,
                qChargeurComp,
                qEbDemande,
                qUserDestEtab,
                qUserOriginEtab,
                qUserOriginCBEtab,
                qUserDestCBEtab,
                qCompagnie,
                qExDemandeTransporteur);

            if (whereVisibilite.hasValue()) where.and(whereVisibilite);

            // recherche par flags
            if (criterias.getListFlag() != null && !criterias.getListFlag().trim().isEmpty()) {
                List<String> listFlagId = Arrays.asList(criterias.getListFlag().trim().split(","));
                Predicate[] predicats = new Predicate[listFlagId.size() * 4];
                int j = 0;

                for (int i = 0; i < listFlagId.size(); i++) {
                    predicats[i + j] = qEbDemande.listFlag
                        .contains(listFlagId.get(i))
                        .and(qEbDemande.listFlag.trim().length().eq(listFlagId.get(i).length()));
                    predicats[i + j + 1] = qEbDemande.listFlag.endsWith("," + listFlagId.get(i));
                    predicats[i + j + 2] = qEbDemande.listFlag.startsWith(listFlagId.get(i) + ",");
                    predicats[i + j + 3] = qEbDemande.listFlag.contains("," + listFlagId.get(i) + ",");
                    j += 3;
                }

                where.andAnyOf(predicats);
            }

        }

        /** ******************JOINTURES****************** */

        if (criterias.isSearchDemandeByLivraisonData() && criterias.isSearchDemandeByMarchandisenData()) {
            query.from(qEbLivraison, qEbMarchandise);
            query.leftJoin(qEbLivraison.xEbDemande(), qEbDemande);
            where.and(qEbLivraison.xEbDemande().ebDemandeNum.eq(qEbMarchandise.ebDemande().ebDemandeNum));
            query.where(where);
        }
        else if (criterias.isSearchDemandeByLivraisonData()) {
            query.from(qEbLivraison).where(where);
            query.leftJoin(qEbLivraison.xEbDemande(), qEbDemande);
        }
        else if (criterias.isSearchDemandeByMarchandisenData()) {
            query.from(qEbMarchandise).where(where);
            query.leftJoin(qEbMarchandise.ebDemande(), qEbDemande);
        }
        else {
            query.from(qEbDemande).where(where);
        }

        query.leftJoin(qEbDemande.xEbCostCenter(), qNamedCostCenter);
        query.leftJoin(qEbDemande.exEbDemandeTransporteurs, qExDemandeTransporteur);
        query.leftJoin(qExDemandeTransporteur.xEbEtablissement(), qEtablissement);
        query.leftJoin(qExDemandeTransporteur.xEbCompagnie(), qCompagnie);

        query.leftJoin(qEbDemande.user(), qChargeur);
        query.leftJoin(qEbDemande.xEbEtablissement(), qChargeurEtab);
        query.leftJoin(qEbDemande.xEbCompagnie(), qChargeurComp);
        query.leftJoin(qEbDemande.ebPartyDest(), qPartyDest);
        query.leftJoin(qEbDemande.ebPartyOrigin(), qPartyOrigin);
        query.leftJoin(qChargeur.ebCompagnie(), qCompagnieChargeur);
        query.leftJoin(qEbDemande.ebDemandeMasterObject(), qEbDemandeMultiSegmentObject);
        query.leftJoin(qEbDemande.xEbEntrepotUnloading(), qEbEntrepot);
        query.leftJoin(qEbDemande.observers, qDemandeUserObservers);

        // List Destionation Country
        if (criterias.getListDestinations() != null && !criterias.getListDestinations().isEmpty()
            || criterias.getListCustomerReference() != null && !criterias.getListCustomerReference().isEmpty()) {

            if (criterias.getListDestinations() != null && !criterias.getListDestinations().isEmpty()) {
                query.leftJoin(qEbDemande.ebPartyDest(), qPartyDest);
            }

        }

        query.leftJoin(qEbDemande.xEbUserOrigin(), qUserOrigin);
        query.leftJoin(qUserOrigin.ebEtablissement(), qUserOriginEtab);
        query.leftJoin(qEbDemande.xEbUserDest(), qUserDest);
        query.leftJoin(qUserDest.ebEtablissement(), qUserDestEtab);
        query.leftJoin(qEbDemande.xEbUserOriginCustomsBroker(), qUserOriginCB);
        query.leftJoin(qUserOriginCB.ebEtablissement(), qUserOriginCBEtab);
        query.leftJoin(qEbDemande.xEbUserDestCustomsBroker(), qUserDestCB);
        query.leftJoin(qUserDestCB.ebEtablissement(), qUserDestCBEtab);
        query.leftJoin(qEbDemande.xEbUserCt(), qEbUserCt);
        query.leftJoin(qEbUserCt.ebEtablissement(), qEbEtablissementCt);

        if (connectedUser.isControlTower()
            || connectedUser.isChargeur()) query.leftJoin(qEbUserCt.ebCompagnie(), qCompagnieRequestor);

        if (connectedUser != null) {

            if ((connectedUser.isTransporteur() || connectedUser.isTransporteurBroker())
                || criterias.getListCarrier() != null || connectedUser.getService() != null
                || criterias.getEbEtablissementNum() != null || connectedUser.isControlTower()
                || criterias.getModule() == Enumeration.Module.TRANSPORT_MANAGEMENT.getCode()
                || criterias.getModule() == Enumeration.Module.ORDRE_TRANSPORT.getCode()
                || criterias.getListCarrier() != null && criterias.getListCarrier().size() > 0) {

                if (criterias.getModule() == Enumeration.Module.TRANSPORT_MANAGEMENT.getCode()
                    || criterias.getModule() == Enumeration.Module.ORDRE_TRANSPORT.getCode()
                    || criterias.getListCarrier() != null && criterias.getListCarrier().size() > 0) {
                    query
                        .leftJoin(qExDemandeTransporteur.xTransporteur(), qTransporteur).where(
                            qExDemandeTransporteur.status
                                .in(
                                    Arrays
                                        .asList(
                                            CarrierStatus.FIN_PLAN.getCode(),
                                            CarrierStatus.FINAL_CHOICE.getCode())));
                    ;
                }

            }

        }

        return query;
    }

    @Override
    public Long getCountListEbDemande(SearchCriteriaPricingBooking criterias) throws Exception {
        Long count = new Long(0);
        BooleanBuilder where = new BooleanBuilder();

        try {
            JPAQuery<Long> query = new JPAQuery<Long>(em);

            EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criterias);

            if (criterias.getSearchByWS() != null) {

                if (StringUtils.isNotBlank(criterias.getTransporterEmail())) {
                    where.andAnyOf(qExDemandeTransporteur.xTransporteur().email.eq(criterias.getTransporterEmail()));
                }
                else if (StringUtils.isNotBlank(criterias.getTransporterCompanyCode())) {
                    where
                        .andAnyOf(qExDemandeTransporteur.xEbCompagnie().code.eq(criterias.getTransporterCompanyCode()));
                }

            }

            query = getGlobalWhere(query, where, criterias, connectedUser);
            query.distinct();

            count = (Long) query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return count;
    }

    @Override
    public List<EbDemande> getListTransportRef(String term, Integer module) {
        List<EbDemande> resultat = new ArrayList<EbDemande>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbDemande> query = new JPAQuery<EbDemande>(em);

            query.select(QEbDemande.create(Expressions.asNumber(0), qEbDemande.refTransport, Expressions.asString("")));

            if (term != null && !term.isEmpty()) {
                term = term.trim().toLowerCase();
                where.and(qEbDemande.refTransport.toLowerCase().contains(term));
            }

            SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();
            criteria.setModule(module);
            EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);

            query = getGlobalWhere(query, where, criteria, connectedUser);

            query.limit(1000);
            query.distinct().where(where);

            resultat = (List<EbDemande>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public List<EbDemande> getListCustomerReference(String term, Integer module) {
        // TODO Auto-generated method stub
        List<EbDemande> resultat = new ArrayList<EbDemande>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbDemande> query = new JPAQuery<EbDemande>(em);

            query
                .select(
                    QEbDemande.create(Expressions.asNumber(0), Expressions.asString(""), qEbDemande.customerReference));

            if (term != null) where = where
                .and(qEbDemande.customerReference.toLowerCase().contains(term.toLowerCase()));

            SearchCriteriaPricingBooking criteria = new SearchCriteriaPricingBooking();
            criteria.setModule(module);
            EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);

            query = getGlobalWhere(query, where, criteria, connectedUser);

            query.distinct().from(qEbDemande).where(where);

            resultat = (List<EbDemande>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public List<ExEbDemandeTransporteur> getListExEbTransporteur(SearchCriteriaPricingBooking transportcriteria) {
        List<ExEbDemandeTransporteur> resultat = new ArrayList<ExEbDemandeTransporteur>();
        QEbDemande qDemande = new QEbDemande("demande");
        QEbCompagnie qCompay = new QEbCompagnie("company");
        QEbEtablissement qEtablissement = new QEbEtablissement("etablissement");
        QEbUser qTransporteur = new QEbUser("transporteur");
        QEbCompagnieCurrency qCompagnieCurrency = new QEbCompagnieCurrency("currency_transporteur");
        QEcCurrency qCurrency = new QEcCurrency("qcurrency");
        QEcCountry qCountryPickup = new QEcCountry("qcountryPickup");
        QEcCountry qCountryDeparture = new QEcCountry("qcountryDeparture");
        QEcCountry qCountryArrival = new QEcCountry("qcountryArrival");
        QEcCountry qCountryCustoms = new QEcCountry("qcountryCustoms");
        QEcCountry qCountryDelivery = new QEcCountry("qcountryDelivery");

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<ExEbDemandeTransporteur> query = new JPAQuery<ExEbDemandeTransporteur>(em);

            if (transportcriteria.isForEmail()) {
                query
                    .select(
                        QExEbDemandeTransporteur
                            .create(
                                qExDemandeTransporteur.exEbDemandeTransporteurNum,
                                qTransporteur.ebUserNum,
                                qTransporteur.nom,
                                qTransporteur.prenom,
                                new CaseBuilder()
                                    .when(
                                        qExDemandeTransporteur.xEbEtablissement().ebEtablissementNum
                                            .eq(Statiques.UNKNOWN_USER_NUM))
                                    .then(qExDemandeTransporteur.emailTransporteur).otherwise(qTransporteur.email),
                                qExDemandeTransporteur.transitTime,
                                qExDemandeTransporteur.price,
                                qCompay.code,
                                qExDemandeTransporteur.status,
                                qEtablissement.ebEtablissementNum,
                                qCompay.ebCompagnieNum,
                                qEtablissement.nom,
                                qCompay.nom,
                                qExDemandeTransporteur.pickupTime,
                                qExDemandeTransporteur.deliveryTime,
                                qCompagnieCurrency.ebCompagnieCurrencyNum,
                                qCurrency.code,
                                qExDemandeTransporteur.listCostCategorie,
                                qExDemandeTransporteur.isFromTransPlan,
                                qExDemandeTransporteur.refPlan,
                                qExDemandeTransporteur.exchangeRateFound,
                                qDemande.ebDemandeNum,
                                qTransporteur.role,
                                qExDemandeTransporteur.exchangeRate,
                                qExDemandeTransporteur.infosForCustomsClearance,
                                qExDemandeTransporteur.listEbTtCompagniePsl,
                                qExDemandeTransporteur.listPlCostItem,
                                qExDemandeTransporteur.xEbCompagnie(),
												qExDemandeTransporteur.isChecked, qExDemandeTransporteur.horsGrille));

                if (transportcriteria.getEmail() == null) where.and(qExDemandeTransporteur.price.isNotNull());

                if (transportcriteria.getEmail() != null) where
                    .and(
                        qExDemandeTransporteur.emailTransporteur
                            .trim().toLowerCase().contains(transportcriteria.getEmail().toLowerCase()));

                if (transportcriteria.getStatus() != null) where
                    .and(qExDemandeTransporteur.status.eq(transportcriteria.getStatus()));
            }
            else if (transportcriteria.isForCotation()) {
                query
                    .select(
                        QExEbDemandeTransporteur
                            .create(
                                qTransporteur.ebUserNum,
                                new CaseBuilder()
                                    .when(
                                        qExDemandeTransporteur.xEbEtablissement().ebEtablissementNum
                                            .eq(Statiques.UNKNOWN_USER_NUM))
                                    .then("").otherwise(qTransporteur.nom),
                                new CaseBuilder()
                                    .when(
                                        qExDemandeTransporteur.xEbEtablissement().ebEtablissementNum
                                            .eq(Statiques.UNKNOWN_USER_NUM))
                                    .then("").otherwise(qTransporteur.prenom),
                                new CaseBuilder()
                                    .when(
                                        qExDemandeTransporteur.xEbEtablissement().ebEtablissementNum
                                            .eq(Statiques.UNKNOWN_USER_NUM))
                                    .then(qExDemandeTransporteur.emailTransporteur).otherwise(qTransporteur.email),
                                qExDemandeTransporteur.transitTime,
                                qExDemandeTransporteur.price,
                                qCompay.code,
                                qExDemandeTransporteur.status,
                                qEtablissement.ebEtablissementNum,
                                qCompay.ebCompagnieNum,
                                new CaseBuilder()
                                    .when(
                                        qExDemandeTransporteur.xEbEtablissement().ebEtablissementNum
                                            .eq(Statiques.UNKNOWN_USER_NUM))
                                    .then("").otherwise(qEtablissement.nom),
                                new CaseBuilder()
                                    .when(
                                        qExDemandeTransporteur.xEbEtablissement().ebEtablissementNum
                                            .eq(Statiques.UNKNOWN_USER_NUM))
                                    .then("").otherwise(qCompay.nom),
                                qExDemandeTransporteur.exEbDemandeTransporteurNum,
                                qExDemandeTransporteur.pickupTime,
                                qExDemandeTransporteur.deliveryTime,
                                qExDemandeTransporteur.leadTimeDeparture,
                                qExDemandeTransporteur.cityDeparture,
                                qCountryDeparture,
                                qExDemandeTransporteur.leadTimeDelivery,
                                qExDemandeTransporteur.cityDelivery,
                                qCountryDelivery,
                                qExDemandeTransporteur.leadTimePickup,
                                qExDemandeTransporteur.cityPickup,
                                qCountryPickup,
                                qExDemandeTransporteur.leadTimeCustoms,
                                qExDemandeTransporteur.cityCustoms,
                                qCountryCustoms,
                                qExDemandeTransporteur.leadTimeArrival,
                                qExDemandeTransporteur.cityArrival,
                                qCountryArrival,
                                qExDemandeTransporteur.listCostCategorie,
                                qExDemandeTransporteur.comment,
                                qExDemandeTransporteur.xEbUserCt().ebUserNum,
                                qExDemandeTransporteur.isFromTransPlan,
                                qExDemandeTransporteur.refPlan,
                                qExDemandeTransporteur.refGrille,
                                qExDemandeTransporteur.exchangeRateFound,
                                qDemande.ebDemandeNum,
                                qTransporteur.role,
                                qTransporteur.status,
                                qExDemandeTransporteur.exchangeRate,
                                qExDemandeTransporteur.infosForCustomsClearance,
                                qExDemandeTransporteur.listEbTtCompagniePsl,
                                qExDemandeTransporteur.xEcModeTransport,
                                qExDemandeTransporteur.listPlCostItem,
                                qExDemandeTransporteur.xEbCompagnie(),
												qExDemandeTransporteur.isChecked, qExDemandeTransporteur.horsGrille));
            }
            else {
                query.select(QExEbDemandeTransporteur.create(qTransporteur.nom, qTransporteur.prenom));
            }

            if (transportcriteria.getEbDemandeNum() != null) where
                .and(qDemande.ebDemandeNum.eq(transportcriteria.getEbDemandeNum()));
            if (transportcriteria.getListEbDemandeNum() != null
                && !transportcriteria.getListEbDemandeNum().isEmpty()) where
                    .and(qDemande.ebDemandeNum.in(transportcriteria.getListEbDemandeNum()));

            if (transportcriteria.isForCotation()) {

                // transport plan
                if (transportcriteria.getEcRoleNum() != null
                    && transportcriteria.getEcRoleNum().equals(Enumeration.Role.ROLE_PRESTATAIRE.getCode())) {

                    if (transportcriteria.getStatus() != null
                        && transportcriteria.getStatus() >= Enumeration.StatutCarrier.FIN.getCode()) {
                        where.and(qExDemandeTransporteur.status.goe(Enumeration.StatutCarrier.FIN.getCode()));
                        where.and(qExDemandeTransporteur.status.ne(Enumeration.StatutCarrier.TRANSPORT_PLAN.getCode()));
                    }

                }

                if (transportcriteria.getEbEtablissementNum() != null) {
                    where
                        .and(
                            qTransporteur.ebEtablissement().ebEtablissementNum
                                .eq(transportcriteria.getEbEtablissementNum()));
                }
                else if (transportcriteria.getEbCompagnieNum() != null) {
                    where.and(qTransporteur.ebCompagnie().ebCompagnieNum.eq(transportcriteria.getEbCompagnieNum()));
                }
                else if (transportcriteria.getEbUserNum() != null) {
                    where.and(qTransporteur.ebUserNum.eq(transportcriteria.getEbUserNum()));
                }
                else if (transportcriteria.getStatus() != null) {

                    if (transportcriteria.getStatus() >= Enumeration.StatutCarrier.FIN.getCode()
                        && transportcriteria.getGrouping() == null) {
                        where.and(qExDemandeTransporteur.status.goe(Enumeration.StatutCarrier.FIN.getCode()));
                        where.and(qExDemandeTransporteur.status.ne(Enumeration.StatutCarrier.TRANSPORT_PLAN.getCode()));
                    }
                    else if (transportcriteria.getGrouping() == null && transportcriteria.getEcRoleNum() != null
                        && transportcriteria.getEcRoleNum().equals(Enumeration.Role.ROLE_CONTROL_TOWER.getCode())) {
                        where.and(qExDemandeTransporteur.status.goe(transportcriteria.getStatus()));
                        where.and(qExDemandeTransporteur.status.ne(Enumeration.StatutCarrier.TRANSPORT_PLAN.getCode()));
                    }

                }

            }

            if (transportcriteria.getGrouping() != null && transportcriteria.getGrouping()) {
                where.and(qExDemandeTransporteur.status.goe(Enumeration.StatutCarrier.FIN.getCode()));
                where.and(qExDemandeTransporteur.status.eq(Enumeration.StatutCarrier.TRANSPORT_PLAN.getCode()));
            }

            if (transportcriteria.getEmail() == null) where.and(qExDemandeTransporteur.xTransporteur().isNotNull());

            if (transportcriteria.getEmailUnknownUser() != null) where
                .and(qExDemandeTransporteur.emailTransporteur.eq(transportcriteria.getEmailUnknownUser()));

            query.from(qExDemandeTransporteur).where(where);

            query.leftJoin(qExDemandeTransporteur.xTransporteur(), qTransporteur);
            query.leftJoin(qExDemandeTransporteur.xEbDemande(), qDemande);

            if (transportcriteria.isForEmail() || transportcriteria.isForCotation()) {
                query.leftJoin(qExDemandeTransporteur.xEbEtablissement(), qEtablissement);
                query.leftJoin(qExDemandeTransporteur.xEbEtablissement().ebCompagnie(), qCompay);
            }

            if (transportcriteria.isForEmail()) {
                query.leftJoin(qExDemandeTransporteur.xEbCompagnieCurrency(), qCompagnieCurrency);
                query.leftJoin(qExDemandeTransporteur.xEbCompagnieCurrency().ecCurrency(), qCurrency);
            }

            if (transportcriteria.isForCotation()) {
                query.leftJoin(qExDemandeTransporteur.xEcCountryPickup(), qCountryPickup);
                query.leftJoin(qExDemandeTransporteur.xEcCountryDeparture(), qCountryDeparture);
                query.leftJoin(qExDemandeTransporteur.xEcCountryArrival(), qCountryArrival);
                query.leftJoin(qExDemandeTransporteur.xEcCountryCustoms(), qCountryCustoms);
                query.leftJoin(qExDemandeTransporteur.xEcCountryDelivery(), qCountryDelivery);
                query.leftJoin(qExDemandeTransporteur.xEbCompagnie(), qCompay);
            }

            if (transportcriteria.isForCotation()) {
                query.orderBy(((ComparableExpressionBase<Integer>) qExDemandeTransporteur.status).desc());
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public List<EbCostCenter> getListCostcenter(String term) {
        List<EbCostCenter> resultat = new ArrayList<EbCostCenter>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbCostCenter> query = new JPAQuery<EbCostCenter>(em);

            query.select(QEbCostCenter.create(Expressions.ZERO, qEbCostCenter.libelle));

            if (term != null
                && !term.isEmpty()) where.and(qEbCostCenter.libelle.toLowerCase().contains(term.trim().toLowerCase()));

            query.distinct().from(qEbCostCenter).where(where);

            resultat = (List<EbCostCenter>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public Integer nextValEbDemande() {
        Integer result = null;

        try {
            result = ((BigInteger) em
                .createNativeQuery("SELECT NEXTVAL('work.eb_demande_eb_demande_num_seq')").getSingleResult())
                    .intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<EbDemande> getListModeTransport(String term) {
        // TODO Auto-generated method stub
        List<EbDemande> resultat = new ArrayList<EbDemande>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbDemande> query = new JPAQuery<EbDemande>(em);

            query.select(QEbDemande.create(qEbDemande.ebDemandeNum, qEbDemande.xEcModeTransport));

            if (term != null) {
                if (Enumeration.ModeTransport.getCodeByLibelle(term) != null) where
                    .and(qEbDemande.xEcModeTransport.eq(Enumeration.ModeTransport.getCodeByLibelle(term)));
                else where.and(qEbDemande.xEcModeTransport.isNull());
            }

            resultat = (List<EbDemande>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public Integer deleteDemande(SearchCriteriaPricingBooking criterias) throws Exception {
        return null;
    }

    @Override
    public List<EbUser> getListCarrier(String term) {
        List<EbUser> resultat = new ArrayList<EbUser>();
        EbUser connectedUser = connectedUserService.getCurrentUser();

        QEbCompagnie qCompagnie = new QEbCompagnie("qCompagnie");
        QEbEtablissement qEtablissement = new QEbEtablissement("qEtablissement");
        QEbEtablissement qEtablissementChargeur = new QEbEtablissement("qEtablissementChargeur");
        QEbUser qTransporteur = new QEbUser("qTransporteur");
        QEbUser qChargeur = new QEbUser("qChargeur");
        QEbEtablissement qEtabHost = new QEbEtablissement("etabHost");
        QEbEtablissement qEtabGuest = new QEbEtablissement("etabGuest");
        QEbRelation qRelation = new QEbRelation("rel");
        QEbEtablissement qEbEtablissementCt = new QEbEtablissement("qEbEtablissementCt");

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbUser> query = new JPAQuery<EbUser>(em);

            BooleanBuilder whereBroker = new BooleanBuilder();
            whereBroker
                .andAnyOf(
                    new BooleanBuilder()
                        .orAllOf(
                            qUserOriginCB.isNotNull(),
                            qUserOriginCBEtab.ebEtablissementNum
                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())),
                    new BooleanBuilder()
                        .orAllOf(
                            qUserDestCB.isNotNull(),
                            qUserDestCBEtab.ebEtablissementNum
                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())));

            query
                .select(
                    QEbUser
                        .create(
                            qTransporteur.ebUserNum,
                            qTransporteur.nom,
                            qTransporteur.prenom,
                            qTransporteur.username,
                            qTransporteur.email,
                            qTransporteur.service,
                            qTransporteur.status,
                            qTransporteur.admin,
                            qTransporteur.superAdmin,
                            qTransporteur.role,
                            qTransporteur.groupes,
                            qEtablissement.ebEtablissementNum,
                            qEtablissement.nom,
                            qEtablissement.canShowPrice,
                            qCompagnie.ebCompagnieNum,
                            qCompagnie.nom,
                            qCompagnie.code));

            where
                .andAnyOf(
                    /*
                     * new BooleanBuilder().orAllOf( new
                     * BooleanBuilder().andAnyOf(qTransporteur.superAdmin.isNull
                     * (),
                     * qTransporteur.superAdmin.isFalse()),
                     * qEtablissement.ebEtablissementNum.isNotNull() ),
                     */
                    // new
                    // BooleanBuilder().orAllOf(qTransporteur.superAdmin.isTrue(),
                    // qCompagnie.ebCompagnieNum.isNotNull())
                    new BooleanBuilder()
                        .orAllOf(qEbDemande.ebDemandeNum.isNotNull(), qTransporteur.ebUserNum.isNotNull()));

            where.and(qExDemandeTransporteur.status.eq(Enumeration.StatutCarrier.FIN.getCode()));
            where.and(qEbDemande.xEcStatut.goe(Enumeration.StatutDemande.WPU.getCode()));

            if (connectedUser.isChargeur()) {

                if (connectedUser.isSuperAdmin()) where
                    .and(qCompagnieChargeur.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
                else {
                    where
                        .orAllOf(
                            qEtablissementChargeur.ebEtablissementNum
                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));
                }

            }
            else if (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker()) {

                if (connectedUser.isSuperAdmin()) {
                    where
                        .andAnyOf(
                            whereBroker,
                            new BooleanBuilder()
                                .orAllOf(
                                    qCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()),
                                    qExDemandeTransporteur.status.eq(Enumeration.StatutCarrier.FIN.getCode())));
                }
                else {
                    where
                        .andAnyOf(
                            whereBroker,
                            new BooleanBuilder()
                                .orAllOf(
                                    qEtablissement.ebEtablissementNum
                                        .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                    qExDemandeTransporteur.status.eq(Enumeration.StatutCarrier.FIN.getCode())));
                }

            }
            else if (connectedUser.isBroker()) {
                where.and(whereBroker);
            }
            else if (connectedUser.isControlTower()) {
                where
                    .andAnyOf(
                        new BooleanBuilder()
                            .orAllOf(
                                qRelation.etat.eq(Enumeration.EtatRelation.ACTIF.getCode()),
                                new BooleanBuilder()
                                    .andAnyOf(
                                        qEtabHost.ebEtablissementNum
                                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                        qEtabGuest.ebEtablissementNum
                                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()))),
                        qEbEtablissementCt.ebEtablissementNum
                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                        qEtablissement.ebEtablissementNum
                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));
            }

            if (term != null && !term.isEmpty()) {
                term = term.trim().toLowerCase();
                where
                    .andAnyOf(
                        qTransporteur.email.toLowerCase().contains(term),
                        qTransporteur.nom.toLowerCase().contains(term),
                        qTransporteur.prenom.toLowerCase().contains(term),
                        new BooleanBuilder()
                            .orAllOf(
                                qTransporteur.nom.isNotNull(),
                                qTransporteur.prenom.isNotNull(),
                                qTransporteur.nom
                                    .concat(Expressions.asString(" ").concat(qTransporteur.prenom)).toLowerCase()
                                    .contains(term)));
            }

            query.distinct();
            query.from(qExDemandeTransporteur).where(where);

            query.leftJoin(qExDemandeTransporteur.xEbDemande(), qEbDemande);
            query.leftJoin(qExDemandeTransporteur.xTransporteur(), qTransporteur);
            query.leftJoin(qExDemandeTransporteur.xEbCompagnie(), qCompagnie);
            query.leftJoin(qExDemandeTransporteur.xEbEtablissement(), qEtablissement);
            query.leftJoin(qExDemandeTransporteur.xEbEtablissement().ebCompagnie(), qCompagnie);
            query.leftJoin(qEbDemande.user(), qChargeur);
            query.leftJoin(qChargeur.ebCompagnie(), qCompagnieChargeur);

            if (connectedUser.isChargeur()) {
                query.leftJoin(qChargeur.ebEtablissement(), qEtablissementChargeur);
            }

            if (connectedUser.isControlTower()) {
                query.leftJoin(qChargeur.contactsGuests, qRelation);
                query.leftJoin(qChargeur.contactsHost, qRelation);
                query.leftJoin(qRelation.guest().ebEtablissement(), qEtabGuest);
                query.leftJoin(qRelation.host().ebEtablissement(), qEtabHost);
                query.leftJoin(qEbDemande.xEbUserCt().ebEtablissement(), qEbEtablissementCt);
            }

            if (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker()) {
                query.leftJoin(qEbDemande.xEbUserOriginCustomsBroker(), qUserOriginCB);
                query.leftJoin(qUserOriginCB.ebEtablissement(), qUserOriginCBEtab);
                query.leftJoin(qEbDemande.xEbUserDestCustomsBroker(), qUserDestCB);
                query.leftJoin(qUserDestCB.ebEtablissement(), qUserDestCBEtab);
            }

            resultat = (List<EbUser>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public List<EbUser> getListChargeur(String term) {
        List<EbUser> resultat = new ArrayList<EbUser>();
        EbUser connectedUser = connectedUserService.getCurrentUser();

        QEbCompagnie qCompagnie = new QEbCompagnie("qCompagnie");
        QEbEtablissement qEtablissement = new QEbEtablissement("qEtablissement");
        QEbEtablissement qEtablissementChargeur = new QEbEtablissement("qEtablissementChargeur");
        QEbUser qTransporteur = new QEbUser("qTransporteur");
        QEbUser qChargeur = new QEbUser("qChargeur");
        QEbEtablissement qEtabHost = new QEbEtablissement("etabHost");
        QEbEtablissement qEtabGuest = new QEbEtablissement("etabGuest");
        QEbRelation qRelation = new QEbRelation("rel");
        QEbEtablissement qEbEtablissementCt = new QEbEtablissement("qEbEtablissementCt");

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbUser> query = new JPAQuery<EbUser>(em);

            BooleanBuilder whereBroker = new BooleanBuilder();
            whereBroker
                .andAnyOf(
                    new BooleanBuilder()
                        .orAllOf(
                            qUserOriginCB.isNotNull(),
                            qUserOriginCBEtab.ebEtablissementNum
                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())),
                    new BooleanBuilder()
                        .orAllOf(
                            qUserDestCB.isNotNull(),
                            qUserDestCBEtab.ebEtablissementNum
                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())));

            query
                .select(
                    QEbUser
                        .create(
                            qChargeur.ebUserNum,
                            qChargeur.nom,
                            qChargeur.prenom,
                            qChargeur.username,
                            qChargeur.email,
                            qChargeur.service,
                            qChargeur.status,
                            qChargeur.admin,
                            qChargeur.superAdmin,
                            qChargeur.role,
                            qChargeur.password,
                            qEtablissementChargeur.ebEtablissementNum,
                            qEtablissementChargeur.nom,
                            qEtablissementChargeur.canShowPrice,
                            qCompagnieChargeur.ebCompagnieNum,
                            qCompagnieChargeur.nom,
                            qCompagnieChargeur.code));

            where
                .andAnyOf(
                    new BooleanBuilder()
                        .orAllOf(qEbDemande.ebDemandeNum.isNotNull(), qTransporteur.ebUserNum.isNotNull()));

            where.and(qEbDemande.xEcStatut.goe(Enumeration.StatutDemande.WPU.getCode()));

            if (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker()) {

                if (connectedUser.isSuperAdmin()) {
                    where
                        .andAnyOf(
                            whereBroker,
                            new BooleanBuilder()
                                .orAllOf(
                                    qCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()),
                                    qExDemandeTransporteur.status.eq(Enumeration.StatutCarrier.FIN.getCode())));
                }
                else {
                    where
                        .andAnyOf(
                            whereBroker,
                            new BooleanBuilder()
                                .orAllOf(
                                    qEtablissement.ebEtablissementNum
                                        .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                    qExDemandeTransporteur.status.eq(Enumeration.StatutCarrier.FIN.getCode())));
                }

            }
            else if (connectedUser.isBroker()) {
                where.and(whereBroker);
            }
            else if (connectedUser.isControlTower()) {
                where
                    .andAnyOf(
                        new BooleanBuilder().orAllOf(qRelation.etat.eq(Enumeration.EtatRelation.ACTIF.getCode())));
            }

            if (term != null && !term.isEmpty()) {
                BooleanBuilder bb = new BooleanBuilder();

                bb
                    .andAnyOf(
                        qChargeur.email.containsIgnoreCase(term),
                        qChargeur.nom.containsIgnoreCase(term),
                        qChargeur.prenom.containsIgnoreCase(term),
                        qCompagnieChargeur.nom.containsIgnoreCase(term),
                        qChargeur.nom
                            .concat(Expressions.asString(" ").concat(qChargeur.prenom)).containsIgnoreCase(term));
                where.and(bb);
            }

            query.distinct();
            query.from(qEbDemande).where(where);

            query.leftJoin(qEbDemande.exEbDemandeTransporteurs, qExDemandeTransporteur);
            query.leftJoin(qExDemandeTransporteur.xTransporteur(), qTransporteur);
            query.leftJoin(qExDemandeTransporteur.xEbEtablissement(), qEtablissement);
            query.leftJoin(qExDemandeTransporteur.xEbEtablissement().ebCompagnie(), qCompagnie);
            query.leftJoin(qEbDemande.user(), qChargeur);

            query.leftJoin(qChargeur.ebEtablissement(), qEtablissementChargeur);
            query.leftJoin(qChargeur.ebCompagnie(), qCompagnieChargeur);

            if (connectedUser.isControlTower()) {
                query.leftJoin(qChargeur.contactsGuests, qRelation);
                query.leftJoin(qChargeur.contactsHost, qRelation);
                query.leftJoin(qRelation.guest().ebEtablissement(), qEtabGuest);
                query.leftJoin(qRelation.host().ebEtablissement(), qEtabHost);
                query.leftJoin(qEbDemande.xEbUserCt().ebEtablissement(), qEbEtablissementCt);
            }

            if (connectedUser.isPrestataire()) {
                query.leftJoin(qEbDemande.xEbUserOriginCustomsBroker(), qUserOriginCB);
                query.leftJoin(qUserOriginCB.ebEtablissement(), qUserOriginCBEtab);
                query.leftJoin(qEbDemande.xEbUserDestCustomsBroker(), qUserDestCB);
                query.leftJoin(qUserDestCB.ebEtablissement(), qUserDestCBEtab);
            }

            resultat = (List<EbUser>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public EbCustomsInformation getCustomsInformation(Integer ebDemandeNum) {
        EbCustomsInformation resultat = new EbCustomsInformation();
        QEbCustomsInformation qEbCustomsInformation = QEbCustomsInformation.ebCustomsInformation;

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbCustomsInformation> query = new JPAQuery<EbCustomsInformation>(em);

            query
                .select(
                    QEbCustomsInformation
                        .create(
                            qEbCustomsInformation.ebCustomsInformationNum,
                            qEbCustomsInformation.declarationNumber,
                            qEbCustomsInformation.control,
                            qEbCustomsInformation.customsDuty,
                            qEbCustomsInformation.costCustomsDuty,
                            qEbCustomsInformation.xEcCurrencyCustomsDuty(),
                            qEbCustomsInformation.Vat,
                            qEbCustomsInformation.costVat,
                            qEbCustomsInformation.xEcCurrencyVat(),
                            qEbCustomsInformation.OtherTaxes,
                            qEbCustomsInformation.costOtherTaxes,
                            qEbCustomsInformation.xEcCurrencyOtherTaxes(),
                            qUserOriginCB.ebUserNum,
                            qUserOriginCB.nom,
                            qUserOriginCB.prenom,
                            qUserOriginCBComp.ebCompagnieNum,
                            qUserOriginCBComp.nom,
                            qUserOriginCBComp.code,
                            qEtablissement.ebEtablissementNum,
                            qEtablissement.nom,
                            qEbDemande.ebDemandeNum));

            where.and(qEbDemande.ebDemandeNum.eq(ebDemandeNum));

            query.distinct().where(where);
            query.from(qEbCustomsInformation);
            query.leftJoin(qEbCustomsInformation.xEbEtablissement(), qEtablissement);
            query.leftJoin(qEbCustomsInformation.xEbDemande(), qEbDemande);
            query.leftJoin(qEbCustomsInformation.xEbUser(), qUserOriginCB);
            query.leftJoin(qEbCustomsInformation.xEbCompagnie(), qUserOriginCBComp);

            resultat = (EbCustomsInformation) query.fetchOne();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public List<EbAdresse> getListEbAdresseEligibility(SearchCriteria criteria, String eligibility) {
        List<EbAdresse> resultat = new ArrayList<EbAdresse>();
        QEbAdresse qEbAdresse = new QEbAdresse("qEbAdresse");
        QEcCountry qCountry = new QEcCountry("qCountry");
        QEbCompagnie qEbCompagnie = new QEbCompagnie("qCompagnie");
        QEbPlZone qEbZone = new QEbPlZone("qEbZone");
        QEbEtablissement qEtablissementAdresse = new QEbEtablissement("etablissementAdresse");

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbAdresse> query = new JPAQuery<EbAdresse>(em);

            query
                .select(
                    QEbAdresse
                        .create(
                            qEbAdresse.airport,
                            qEbAdresse.city,
                            qEbAdresse.commentaire,
                            qEbAdresse.company,
                            qEbAdresse.dateAjout,
                            qEbAdresse.ebAdresseNum,
                            qEbAdresse.email,
                            qEbAdresse.openingHoursFreeText,
                            qEbAdresse.phone,
                            qEbAdresse.reference,
                            qEbAdresse.street,
                            qEbAdresse.type,
                            qEbAdresse.zipCode,
                            qEbAdresse.listEgibility,
                            qEbAdresse.ebCompagnie().ebCompagnieNum,
                            qEbCompagnie.nom,
                            qEbCompagnie.code,
                            qEbAdresse.state));

            query.distinct();

            if (criteria.getEbCompagnieNum() != null) {
                where.and(qEbAdresse.ebCompagnie().ebCompagnieNum.eq(criteria.getEbCompagnieNum()));
            }

            if (criteria.getEbEtablissementNum() != null) {
                where.andAnyOf(qEbAdresse.listEtablissementsNum.contains(":" + criteria.getEbEtablissementNum() + ":"));
            }

            if (eligibility != null) {
                where.and(qEbAdresse.listEgibility.trim().like("%" + eligibility + "%"));
            }

            query.from(qEbAdresse).where(where);
            query.leftJoin(qEbAdresse.ebCompagnie(), qEbCompagnie);

            resultat = (List<EbAdresse>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
		public List<EbQrGroupe> getGroupsAndPropositions(
			SearchCriteria criteria,
			boolean isWithPropositions
		)
		{
        List<EbQrGroupe> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbQrGroupe> query = new JPAQuery<>(em);

						if (isWithPropositions)
						{
							query.select(qEbQrGroupe);

							EntityGraph graphs = em.createEntityGraph(EbQrGroupe.class);
							graphs.addSubgraph("listPropositions");
							query.setHint("javax.persistence.loadgraph", graphs);
						}

						if (!isWithPropositions)
						{
							query
							.select(
								QEbQrGroupe
									.create(
										qEbQrGroupe.ebQrGroupeNum,
										qEbQrGroupe.libelle,
										qEbQrGroupe.grpOrder,
										qEbQrGroupe.isDeleted,
										qEbQrGroupe.isActive,
										qEbQrGroupe.xEbCompagnie(),
										qEbQrGroupe.compatibilityCriteria,
										qEbQrGroupe.exclusionCriteria,
										qEbQrGroupe.triggers,
										qEbQrGroupe.ventilation,
										qEbQrGroupe.output,
										qEbQrGroupe.description
									)
							);
            }


            query = getQrGroupeGlobalWhere(query, where, criteria);

            query.from(qEbQrGroupe);
            query.where(where);

            query = getQrGroupeGlobalJoin(query, where, criteria);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbQrGroupe, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbQrGroupe.grpOrder.asc());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public List<EbQrGroupe> getListQrGroupe(SearchCriteria criteria, boolean active) {
        List<EbQrGroupe> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbQrGroupe> query = new JPAQuery<>(em);

            query.select(qEbQrGroupe);

            query = getQrGroupeGlobalWhere(query, where, criteria, active);

            EntityGraph graphs = em.createEntityGraph(EbQrGroupe.class);
            graphs.addSubgraph("listPropositions");
            query.setHint("javax.persistence.loadgraph", graphs);

            query.from(qEbQrGroupe);
            query.where(where);

            query = getQrGroupeGlobalJoin(query, where, criteria);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbQrGroupe, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbQrGroupe.grpOrder.asc());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public Long getListQrGroupeCount(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbQrGroupe> query = new JPAQuery<EbQrGroupe>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getQrGroupeGlobalWhere(query, where, criteria);

            query.from(qEbQrGroupe);
            query.where(where);

            query = getQrGroupeGlobalJoin(query, where, criteria);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private JPAQuery getQrGroupeGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {

        if (criteria.getEbCompagnieNum() == null) {
            EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);
            where.and(qEbCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }
        else {
            where.and(qEbCompagnie.ebCompagnieNum.eq(criteria.getEbCompagnieNum()));
        }

        if (criteria.getIdObject() != null) where.and(qEbQrGroupe.ebQrGroupeNum.eq(criteria.getIdObject()));

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where.andAnyOf(qEbQrGroupe.libelle.toLowerCase().contains(term));
        }

        if (criteria.getListIdObject() != null) {
            where.and(qEbQrGroupe.ebQrGroupeNum.in(criteria.getListIdObject()));
        }

        return query;
    }

    private JPAQuery
        getQrGroupeGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria, boolean active) {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);
        where.and(qEbCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        where.and(qEbQrGroupe.isActive.eq(active));
        if (criteria.getIdObject() != null) where.and(qEbQrGroupe.ebQrGroupeNum.eq(criteria.getIdObject()));

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where.andAnyOf(qEbQrGroupe.libelle.toLowerCase().contains(term));
        }

        if (criteria.getListIdObject() != null) {
            where.and(qEbQrGroupe.ebQrGroupeNum.in(criteria.getListIdObject()));
        }

        return query;
    }

    private JPAQuery getQrGroupeGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        query.leftJoin(qEbQrGroupe.xEbCompagnie(), qEbCompagnie);

        return query;
    }

    @Override
    public List<EbDemande> getListEbDemandeFavoris(SearchCriteriaPricingBooking criteria) throws Exception {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);
        NumberExpression<Integer> numberOrderWithExpression = qEbDemande.xEcStatut;
        List<EbDemande> resultat = null;
        Initialiaze();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbDemande> query = new JPAQuery<EbDemande>(em);
            query
                .select(
                    QEbDemande
                        .create(
                            qEbDemande.ebDemandeNum,
                            qEbDemande.listFlag,
                            qEbDemande.xEbCompagnie(),
                            qEbDemande.refTransport,
                            qEbDemande.xEcModeTransport,
                            qEbDemande.libelleOriginCity,
                            qEbDemande.libelleOriginCountry,
                            qEbDemande.libelleDestCity,
                            qEbDemande.libelleDestCountry,
                            Expressions.asSimple(connectedUser),
                            Expressions.asNumber(criteria.getModule()),
                            qEbDemande.xEcStatut,
                            numberOrderWithExpression.as("numberOrderExpression")

                        ));
            if (criteria.getListEbDemandeNum() != null && !criteria.getListEbDemandeNum().isEmpty()) where
                .and(qEbDemande.ebDemandeNum.in(criteria.getListEbDemandeNum()));

            query.where(where);
            query.from(qEbDemande);
            query.distinct();

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public List<EbDemande> getListDemandeGroupe(SearchCriteriaPricingBooking criteria) {
        // fais pour gerer le contexte hors spring, exécution via message kafka par exemple
        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria) != null
                ? connectedUserService.getCurrentUserWithCategories(criteria)
                :ebUserRepository.findOneByEbUserNum(criteria.getEbUserNum());

        //set to null (par precaution)
        criteria.setEbUserNum(null);
        List<EbDemande> resultat = null;
        Initialiaze();
        criteria.setModule(Enumeration.Module.PRICING.getCode());

        try {
            JPAQuery<EbDemande> query = new JPAQuery<EbDemande>(em);
            BooleanBuilder where = new BooleanBuilder();

            // query
            // .select(
            // QEbDemande
            // .create(
            // qEbDemande.ebDemandeNum,
            // qEbDemande.refTransport,
            // qNamedCostCenter.ebCostCenterNum,
            // qNamedCostCenter.libelle,
            // qEbDemande.customerReference,
            // qEbDemande.totalWeight,
            // qEbDemande.xEcModeTransport,
            // qEbDemande.dateCreation,
            // qEbDemande.xEcStatut,
            // qEbDemande.xEcTypeDemande,
            // qEbDemande.xEbTypeRequest,
            // qEbDemande.libelleDestCountry,
            // qEbDemande.libelleOriginCountry,
            // qEbDemande.dateOfGoodsAvailability,
            // qEbDemande.dateOfArrival,
            // qEbDemande.datePickup,
            // qPartyOrigin,
            // qCountryOrigin,
            // qPartyDest,
            // qCountryDest,
            // qPartyNotif,
            // qCountryNotif,
            // qEbDemande.xEbTypeTransport,
            // qEbDemande.flagRecommendation,
            // qEbDemande.commentChargeur,
            // qEbDemande.totalVolume,
            // qEbDemande.totalTaxableWeight,
            // qEbDemande.insurance,
            // qEbDemande.insuranceValue,
            // qEbDemande.city,
            // qEbDemande.listCategories,
            // qEbDemande.totalNbrParcel,
            // qUserOriginCB.ebUserNum,
            // qUserOriginCB.email,
            // qUserOriginCBEtab.ebEtablissementNum,
            // qUserDestCB.ebUserNum,
            // qUserDestCB.email,
            // qUserDestCBEtab.ebEtablissementNum,
            // qEbDemande.xEcTypeCustomBroker,
            // qEbDemande.xEbTransporteur,
            // qEbDemande.xEbTransporteurEtablissementNum,
            // qEbDemande.flagDocumentCustoms,
            // qEbDemande.flagDocumentTransporteur,
            // qEbDemande.flagTransportInformation,
            // qEbDemande.flagCustomsInformation,
            // qEbDemande.flagRequestCustomsBroker,
            // qEbDemande.customFields,
            // qEbDemande.configPsl,
            // qEbDemande.numAwbBol,
            // qEbDemande.carrierUniqRefNum,
            // qEbDemande.flightVessel,
            // qEbDemande.datePickupTM,
            // qEbDemande.etd,
            // qEbDemande.finalDelivery,
            // qEbDemande.eta,
            // qEbDemande.customsOffice,
            // qEbDemande.mawb,
            // qEbDemande.flagEbtrackTrace,
            // qEbDemande.flagEbInvoice,
            // qChargeur,
            // qChargeurEtab.ebEtablissementNum,
            // qChargeurEtab.nom,
            // qChargeurComp.ebCompagnieNum,
            // qChargeurComp.nom,
            // qChargeurComp.code,
            // qEcCurrencyInvoice.ecCurrencyNum,
            // qEcCurrencyInvoice.code,
            // qEbDemande.ebDemandeHistory,
            // qEbDemande.moduleCreator,
            // qEbDemande.listLabels,
            // qEbDemande.xEcCancelled,
            // qEbDemande.xEcNature,
            // qEbDemande.pointIntermediate().ebPartyNum,
            // qPointeIntermediate.reference,
            // qPointeIntermediate.company,
            // qEbDemande.ebDemandeMasterObject().ebDemandeNum,
            // qEbDemande.ebDemandeMasterObject().refTransport,
            // qEbDemande.ebDemandeInitial().ebDemandeNum,
            // qEbDemande.ebDemandeInitial().refTransport,
            // qUserOrigin.ebUserNum,
            // qUserOrigin.nom,
            // qUserOriginEtab.ebEtablissementNum,
            // qUserOriginEtab.nom,
            // qUserDest.ebUserNum,
            // qUserDest.nom,
            // qUserDestEtab.ebEtablissementNum,
            // qUserDestEtab.nom,
            // qUserOriginCB.ebUserNum,
            // qUserOriginCB.nom,
            // qUserOriginCBEtab.ebEtablissementNum,
            // qUserOriginCBEtab.nom,
            // qUserDestCB.ebUserNum,
            // qUserDestCB.nom,
            // qUserDestCBEtab.ebEtablissementNum,
            // qUserDestCBEtab.nom,
            // qEbDemande.listFlag,
            // qEbDemande.xEbSchemaPsl(),
            // qEbDemande.dg,
            // qEbDemande.eligibleForConsolidation,
            // qZoneOrigin,
            // qZoneDest,
            // qEbDemande.xEbTypeFluxNum,
            // qEbDemande.typeRequestLibelle,
            // qEbDemande.xEbIncotermNum,
            // qEbDemande.unitsReference
            // )
            // );

            if (criteria.isIncludeGlobalValues()) {
                where
                    .and(
                        qEbDemande.xEcCancelled
                            .isNull().or(qEbDemande.xEcCancelled.ne(Enumeration.CancelStatus.CANECLLED.getCode())));
            }

            query = getGlobalWhere(query, where, criteria, connectedUser);

            query.leftJoin(qEbDemande.ebPartyDest(), qPartyDest);
            query.leftJoin(qEbDemande.ebPartyOrigin(), qPartyOrigin);
            query.leftJoin(qEbDemande.ebPartyNotif(), qPartyNotif);
            query.leftJoin(qPartyOrigin.xEcCountry(), qCountryOrigin);
            query.leftJoin(qPartyDest.xEcCountry(), qCountryDest);
            query.leftJoin(qPartyNotif.xEcCountry(), qCountryNotif);
            query.leftJoin(qEbDemande.pointIntermediate(), qPointeIntermediate);
            query.leftJoin(qEbDemande.xEbCostCenter(), qNamedCostCenter);
            query.leftJoin(qEbDemande.xecCurrencyInvoice(), qEcCurrencyInvoice);
            query.leftJoin(qEbDemande.ebDemandeMasterObject(), qEbDemandeMultiSegmentObject);
            query.leftJoin(qEbDemande.ebDemandeInitial(), qEbDemandeInitial);
            query.leftJoin(qPartyOrigin.zone(), qZoneOrigin);
            query.leftJoin(qPartyDest.zone(), qZoneDest);
            query.leftJoin(qEbDemande.xEbCompagnie(), qEbCompagnie);

            query.distinct();

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public List<EbAdresse> getAllAdresseByZone(Integer ebZoneNum) {
        QEbAdresse qEbAdresse = new QEbAdresse("qEbAdresse");
        List<EbAdresse> results = new ArrayList<>();

        try {
            JPAQuery<EbAdresse> query = new JPAQuery<EbAdresse>(em);
            BooleanBuilder where = new BooleanBuilder();

            query.select(QEbAdresse.create(qEbAdresse.ebAdresseNum, qEbAdresse.zones, qEbAdresse.listZonesNum));
            if (ebZoneNum != null) where.and(qEbAdresse.listZonesNum.contains(":" + ebZoneNum + ":"));

            query.from(qEbAdresse).where(where);
            results = (List<EbAdresse>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results;
    }

	private boolean isSqlQueryContainCategorieOrCustomFields(List<QrGroupeCriteria> criteres) {
		return criteres.stream().anyMatch(criteria -> hasCriteriaCategorieOrCustomField(criteria));
	}

	private boolean hasCriteriaCategorieOrCustomField(QrGroupeCriteria criteria) {

		final boolean areCompatible = criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode());
		final boolean areExcluded = criteria.getCode().equals(GroupeFunction.ARE_EXCLUDED.getCode());
		final boolean hasCategories = GroupeField.CATEGORIES.getCode().equals(criteria.getField().getCode());
		final boolean hasCustomFields = GroupeField.CUSTOM_FIELDS.getCode().equals(criteria.getField().getCode());

		return (areCompatible || areExcluded) && (hasCategories || hasCustomFields);
	}

    @Override
    public void setPropositionGroupe(EbQrGroupe ebQrGroupe, Integer ebComagnieChargNum) {
        ebQrGroupe.setListPropositions(null);

        EbUser connectedUser = connectedUserService.getCurrentUser();


        Query query;
        String sqlQuery, sqlQuerySupplementaire;

        Set<String> res = new HashSet<String>();

		// separation des criteres par "OR"
		List<List<QrGroupeCriteria>> criteriaList = new ArrayList<>();

		ebQrGroupe.getCompatibilityCriteria().get(0).setIsAnd(true);
		List<QrGroupeCriteria> criteriaWithAnd = ebQrGroupe.getCompatibilityCriteria().stream()
				.filter(groupCriteria -> Boolean.TRUE.equals(groupCriteria.getIsAnd())).collect(Collectors.toList());

		criteriaList.add(criteriaWithAnd);

		List<QrGroupeCriteria> criteriaWithOr = ebQrGroupe.getCompatibilityCriteria().stream()
				.filter(groupCriteria -> BooleanUtils.isNotTrue(groupCriteria.getIsAnd())).collect(Collectors.toList());

		criteriaList.add(criteriaWithOr);


		for (List<QrGroupeCriteria> criteres : criteriaList) {

            try {
                query = null;
                sqlQuery = "";
                sqlQuerySupplementaire = "";

                /**
                 * *********************************** SELECT
                 * **************************
                 */
                sqlQuery += "SELECT  STRING_AGG ( distinct ':'|| demande.eb_demande_num ||':', '') AS list_propos ";
                sqlQuery += "FROM  work.eb_demande demande ";

                String groupedFields = "";
                String whereFields = "";
                String joinFields = "";
                String excludeFields = "";

				if (isSqlQueryContainCategorieOrCustomFields(criteres)) {
                    sqlQuerySupplementaire = "SELECT STRING_AGG (distinct ':'|| eb_demande_num ||':', '') AS list_propos, ";
                }

                for (QrGroupeCriteria criteria: criteres) {

                    if (criteria.getCode().equals(GroupeFunction.ARE_EQUAL.getCode())) {
                        // pour la fonction EQUALS

                        if (GroupeField.DGR_TYPE.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "demande.dg,");
                            whereFields = setUniqueValue(whereFields, "and demande.dg is not null ");
                        }
                        else if (GroupeField.MODE_DE_TRANSPORT.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "demande.x_ec_mode_transport,");
                            whereFields = setUniqueValue(whereFields, "and demande.x_ec_mode_transport is not null ");
                        }
                        else if (GroupeField.TYPE_DE_TRANSPORT.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "demande.x_eb_type_transport,");
                            whereFields = setUniqueValue(whereFields, "and demande.x_eb_type_transport is not null ");
                        }
                        else if (GroupeField.NIVEAU_DE_SERVICE.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "demande.x_eb_type_request,");
                            whereFields = setUniqueValue(whereFields, "and demande.x_eb_type_request is not null ");
                        }
                        else if (GroupeField.DATE_AVAILABILITY.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "demande.date_of_goods_availability,");
                            whereFields = setUniqueValue(
                                whereFields,
                                "and demande.date_of_goods_availability is not null ");
                        }
                        else if (GroupeField.DATE_ARRIVAL.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "demande.date_of_arrival,");
                            whereFields = setUniqueValue(whereFields, "and demande.date_of_arrival is not null ");
                        }
                        else if (GroupeField.INCOTERM.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "demande.x_ec_incoterm_libelle,");
                            whereFields = setUniqueValue(whereFields, "and demande.x_ec_incoterm_libelle is not null ");
                        }
                        else if (GroupeField.VILLE_INCOTERM.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "demande.city,");
                            whereFields = setUniqueValue(whereFields, "and demande.city is not null ");
                        }
                        else if (GroupeField.ELIGIBILITE.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "demande.eligible_for_consolidation,");
                            whereFields = setUniqueValue(
                                whereFields,
                                "and demande.eligible_for_consolidation is not null ");
                        }

                        else if (GroupeField.ORIGINE.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "party_origin.city,");
                            whereFields = setUniqueValue(whereFields, "and party_origin.city is not null ");
                            joinFields = setUniqueValue(
                                joinFields,
                                "LEFT JOIN  work.eb_party party_origin ON party_origin.eb_party_num = demande.eb_party_origin ");
                        }
                        else if (GroupeField.DESTINATION.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "party_dest.city,");
                            whereFields = setUniqueValue(whereFields, "and party_dest.city is not null ");
                            joinFields = setUniqueValue(
                                joinFields,
                                "LEFT JOIN  work.eb_party party_dest ON party_dest.eb_party_num = demande.eb_party_dest ");
                        }
                        else if (GroupeField.ZONE_DESTINATION.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "party_dest1.x_ec_country,");
                            whereFields = setUniqueValue(whereFields, "and party_dest1.x_ec_country is not null ");
                            joinFields = setUniqueValue(
                                joinFields,
                                "LEFT JOIN  work.eb_party party_dest1 ON party_dest1.eb_party_num = demande.eb_party_dest ");
                        }
                        else if (GroupeField.ZONE_ORIGINE.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "party_origin1.x_ec_country,");
                            whereFields = setUniqueValue(whereFields, "and party_origin1.x_ec_country is not null ");
                            joinFields = setUniqueValue(
                                joinFields,
                                "LEFT JOIN  work.eb_party party_origin1 ON party_origin1.eb_party_num = demande.eb_party_origin ");
                        }
                        else if (GroupeField.CATEGORIES.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "demande.list_labels,");
                            whereFields = setUniqueValue(
                                whereFields,
                                "and demande.list_labels is not null and demande.list_labels != '' ");
                        }
                        else if (GroupeField.TRANSPORTEUR.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "demande_quote.x_eb_etablissement,");
                            whereFields = setUniqueValue(
                                whereFields,
                                "and demande_quote.x_eb_etablissement is not null  ");
                            joinFields = setUniqueValue(
                                joinFields,
                                "LEFT JOIN work.eb_demande_quote  demande_quote  ON demande_quote.x_eb_demande = demande.eb_demande_num ");
                        }
                        else if (GroupeField.ZONE_ORIGINE_GT.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "party_origin2.x_eb_zone,");
                            whereFields = setUniqueValue(whereFields, "and party_origin2.x_eb_zone is not null ");
                            joinFields = setUniqueValue(
                                joinFields,
                                "LEFT JOIN  work.eb_party party_origin2 ON party_origin2.eb_party_num = demande.eb_party_origin ");
                        }
                        else if (GroupeField.ZONE_DESTINATION_GT.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "party_dest2.x_eb_zone,");
                            whereFields = setUniqueValue(whereFields, "and party_dest2.x_eb_zone is not null ");
                            joinFields = setUniqueValue(
                                joinFields,
                                "LEFT JOIN  work.eb_party party_dest2 ON party_dest2.eb_party_num = demande.eb_party_dest ");
                        }
                        else if (GroupeField.CUSTOM_FIELDS.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "demande.list_custom_fields_flat,");
                            whereFields = setUniqueValue(
                                whereFields,
                                "and demande.list_custom_fields_flat is not null and demande.list_custom_fields_flat != '' ");
                        }
                        else if (GroupeField.PART_NUMBER.getCode().equals(criteria.getField().getCode())) {
                            groupedFields = setUniqueValue(groupedFields, "marchandise.part_number,");
                            whereFields = setUniqueValue(
                                whereFields,
                                "and marchandise.part_number is not null and marchandise.part_number != '' ");
                            joinFields = setUniqueValue(
                                joinFields,
                                "LEFT JOIN  work.eb_marchandise marchandise ON marchandise.x_eb_demande = demande.eb_demande_num ");
                        }

                    }
                    else if (criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode())
                        || criteria.getCode().equals(GroupeFunction.ARE_EXCLUDED.getCode())) {
                        SqlQueryGroupedAndWhereFields whereAndGroupedFields = null;

                        if (GroupeField.DGR_TYPE.getCode().equals(criteria.getField().getCode())) {
                            joinFields = setUniqueValue(
                                joinFields,
                                "LEFT JOIN work.eb_marchandise marchandise on marchandise.x_eb_demande = demande.eb_demande_num ");

                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "marchandise.dangerous_good",
                                GroupeTypeTraitemant.BY_CODE.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.PART_NUMBER.getCode().equals(criteria.getField().getCode())) {

                            if (joinFields
                                .indexOf(
                                    "LEFT JOIN work.eb_marchandise marchandise on marchandise.x_eb_demande ") < 0) {
                                joinFields += "LEFT JOIN work.eb_marchandise marchandise on marchandise.x_eb_demande = demande.eb_demande_num ";
                            }

                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "marchandise.part_number",
                                GroupeTypeTraitemant.BY_LABEL.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.MODE_DE_TRANSPORT.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande.x_ec_mode_transport",
                                GroupeTypeTraitemant.BY_CODE.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.STATUS.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                    groupedFields,
                                    whereFields,
                                    joinFields,
                                    excludeFields,
                                    criteria.getGroupValues(),
                                    "demande.x_ec_statut",
                                    GroupeTypeTraitemant.BY_CODE.getCode(),
                                    null,
                                    isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                    criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                            GroupeFunction.ARE_COMPATIBLE.getCode() :
                                            GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.TYPE_DE_TRANSPORT.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande.x_eb_type_transport",
                                GroupeTypeTraitemant.BY_CODE.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.NIVEAU_DE_SERVICE.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande.x_eb_type_request",
                                GroupeTypeTraitemant.BY_CODE.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.DATE_AVAILABILITY.getCode().equals(criteria.getField().getCode())) {
                            groupedFields += "demande.date_of_goods_availability,";
                            whereFields += "and demande.date_of_goods_availability is not null ";
                        }
                        else if (GroupeField.DATE_ARRIVAL.getCode().equals(criteria.getField().getCode())) {
                            groupedFields += "demande.date_of_arrival,";
                            whereFields += "and demande.date_of_arrival is not null ";
                        }
                        else if (GroupeField.INCOTERM.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande.x_ec_incoterm_libelle",
                                GroupeTypeTraitemant.BY_LABEL.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.VILLE_INCOTERM.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande.city",
                                GroupeTypeTraitemant.BY_LABEL.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.ELIGIBILITE.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande.eligible_for_consolidation",
                                GroupeTypeTraitemant.BY_BOOL.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }

                        else if (GroupeField.ORIGINE.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande.libelle_origin_city",
                                GroupeTypeTraitemant.BY_LABEL.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.DESTINATION.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande.libelle_dest_city",
                                GroupeTypeTraitemant.BY_LABEL.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.ZONE_DESTINATION.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande.libelle_dest_country",
                                GroupeTypeTraitemant.BY_LABEL.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.ZONE_ORIGINE.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande.libelle_origin_country",
                                GroupeTypeTraitemant.BY_LABEL.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }

                        else if (GroupeField.CATEGORIES.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande.list_labels",
                                GroupeTypeTraitemant.BY_CATEGORIE.getCode(),
                                criteres.indexOf(criteria),
                                sqlQuerySupplementaire,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.TRANSPORTEUR.getCode().equals(criteria.getField().getCode())) {
                            joinFields = setUniqueValue(
                                joinFields,
                                "LEFT JOIN work.eb_demande_quote  demande_quote  ON demande_quote.x_eb_demande = demande.eb_demande_num ");

                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande_quote.x_eb_etablissement",
                                GroupeTypeTraitemant.BY_CODE.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.ZONE_ORIGINE_GT.getCode().equals(criteria.getField().getCode())) {
                            joinFields = setUniqueValue(
                                joinFields,
                                "LEFT JOIN  work.eb_party party_origin3 ON party_origin3.eb_party_num = demande.eb_party_origin ");
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "party_origin3.x_eb_zone",
                                GroupeTypeTraitemant.BY_CODE.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.ZONE_DESTINATION_GT.getCode().equals(criteria.getField().getCode())) {
                            joinFields = setUniqueValue(
                                joinFields,
                                "LEFT JOIN  work.eb_party party_dest3 ON party_dest3.eb_party_num = demande.eb_party_dest ");

                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "party_dest3.x_eb_zone",
                                GroupeTypeTraitemant.BY_CODE.getCode(),
                                null,
									isSqlQueryContainCategorieOrCustomFields(criteres) ? sqlQuerySupplementaire : null,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }
                        else if (GroupeField.CUSTOM_FIELDS.getCode().equals(criteria.getField().getCode())) {
                            whereAndGroupedFields = setWhereAndGroupedFields(
                                groupedFields,
                                whereFields,
                                joinFields,
                                excludeFields,
                                criteria.getGroupValues(),
                                "demande.list_custom_fields_flat",
                                GroupeTypeTraitemant.BY_CUSTOM_FIELDS.getCode(),
                                criteres.indexOf(criteria),
                                sqlQuerySupplementaire,
                                criteria.getCode().equals(GroupeFunction.ARE_COMPATIBLE.getCode()) ?
                                    GroupeFunction.ARE_COMPATIBLE.getCode() :
                                    GroupeFunction.ARE_EXCLUDED.getCode());
                        }

                        if (whereAndGroupedFields != null) {
                            groupedFields = whereAndGroupedFields.getGroupedFields();
                            whereFields = whereAndGroupedFields.getWhereFields();
                            joinFields = whereAndGroupedFields.getJoinFields();
                            excludeFields = whereAndGroupedFields.getExcludeFields();
                            sqlQuerySupplementaire = whereAndGroupedFields.getSqlQueryWithCategorie();
                        }

                    }

                }

                if (groupedFields.length() > 0) {
                    groupedFields = groupedFields.substring(0, groupedFields.length() - 1);
                }

				if (isSqlQueryContainCategorieOrCustomFields(criteres) && sqlQuerySupplementaire != null
                    && sqlQuerySupplementaire != "") {
                    sqlQuerySupplementaire += "FROM  work.eb_demande demande ";
                    sqlQuery = sqlQuerySupplementaire;
                }

                sqlQuery += joinFields;

                if (ebComagnieChargNum != null) {
                    sqlQuery += "WHERE demande.x_eb_compagnie = " + ebComagnieChargNum + " ";
                }
                else {
                    sqlQuery += "WHERE demande.x_eb_compagnie = " + connectedUser.getEbCompagnie().getEbCompagnieNum()
                        + " ";
                }

                sqlQuery += "and demande.eligible_for_consolidation is true ";
                sqlQuery += "and demande.x_ec_statut < " + StatutDemande.FIN.getCode()
                    + " ";
                sqlQuery += "and demande.x_ec_statut != " + StatutDemande.PND.getCode()
                        + " ";

                sqlQuery += "and demande.is_grouping is not TRUE ";

                sqlQuery += "and (demande.x_ec_cancelled is null OR demande.x_ec_cancelled != 1) ";

                sqlQuery += whereFields;

                sqlQuery += excludeFields;

                sqlQuery += "GROUP BY (";

                sqlQuery += groupedFields;

                sqlQuery += ") ";

								sqlQuery += "HAVING STRING_AGG (DISTINCT ':'|| eb_demande_num ||':', '') like '%::%'";

                LOGGER.trace(sqlQuery);

                query = em.createNativeQuery(sqlQuery);

				if (!isSqlQueryContainCategorieOrCustomFields(criteres)) {

                    if (groupedFields.length() > 0) {
                        res.addAll(query.getResultList());
                    }

                }
                else {
                    List<Object[]> list = query.getResultList();

                    for (Object[] obj: list) {
                        String resultat = (String) obj[0];
                        Integer size = obj.length;
                        Boolean isValide = true;

                        if (size != null && size > 0 && resultat != null && resultat != "") {

                            for (int i = 1; i < size; i++) {
                                Boolean code = (Boolean) obj[i];

                                if (code == null || !code) {
                                    isValide = false;
                                    break;
                                }

                            }

                            if (isValide) {
                                res.add(resultat);
                            }

                        }

                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (res != null && !res.isEmpty())

        {
            ebQrGroupe.setListPropositions(new ArrayList<>());

            for (String r: res) {
                ebQrGroupe
                    .getListPropositions().add(
                        new EbQrGroupeProposition(
                            GroupePropositionStatut.WAITING_FOR_CONFIRMATION.getCode(),
                            r,
                            new EbQrGroupe(ebQrGroupe.getEbQrGroupeNum())));
            }

        }
        else {// si pas de proposition
            ebQrGroupe.setListPropositions(null);
        }

    }

    private SqlQueryGroupedAndWhereFields setWhereAndGroupedFields(
        String groupedFields,
        String whereFields,
        String joinFields,
        String excludeFields,
        List<QrGroupeValue> groupValues,
        String libelle,
        Integer typeTraitement,
        Integer criteriaNumber,
        String sqlQuerySupplementaire,
        Integer typeField) {

        if (groupedFields != null && !typeTraitement.equals(GroupeTypeTraitemant.BY_CATEGORIE.getCode())
            && !typeTraitement.equals(GroupeTypeTraitemant.BY_CUSTOM_FIELDS.getCode())) {
            groupedFields = setUniqueValue(groupedFields, libelle + ",");
        }

        if (whereFields != null && whereFields.indexOf("and " + libelle + " is not null") < 0
            && !typeTraitement.equals(GroupeTypeTraitemant.BY_CATEGORIE.getCode())
            && !typeTraitement.equals(GroupeTypeTraitemant.BY_CUSTOM_FIELDS.getCode())) {
            whereFields += "and " + libelle + " is not null ";
        }

        for (int index = 0; index < groupValues.size(); index++) {

            if (typeTraitement.equals(GroupeTypeTraitemant.BY_BOOL.getCode())) {

                if (!GroupeFunction.ARE_EXCLUDED.getCode().equals(typeField)) {

                    if (groupValues.get(index).getValue().getCode().equals(Insurance.Yes.getCode())) {
                        whereFields += "and " + libelle + " is  true ";
                    }
                    else {
                        whereFields += "and " + libelle + " is not true ";
                    }

                }
                else {

                    if (groupValues.get(index).getValue().getCode().equals(Insurance.Yes.getCode())) {
                        excludeFields += "and " + libelle + " is not true ";
                    }
                    else {
                        excludeFields += "and " + libelle + " is  true ";
                    }

                }

            }
            else if (typeTraitement.equals(GroupeTypeTraitemant.BY_LABEL.getCode())) {

                if (groupValues.get(index).getValue().getLibelle() != null
                    && !groupValues.get(index).getValue().getLibelle().isEmpty()) {
                    groupValues
                        .get(index).getValue().setLibelle(groupValues.get(index).getValue().getLibelle().toLowerCase());
                }

                if (!GroupeFunction.ARE_EXCLUDED.getCode().equals(typeField)) {

                    if (groupValues
                        .get(index).getSousFunction().getCode().equals(GroupeSousFunction.CONTAINS_VALUE.getCode())) {
                        whereFields += "and lower(" + libelle + ") like '%"
                            + groupValues.get(index).getValue().getLibelle() + "%' ";
                    }
                    else if (groupValues
                        .get(index).getSousFunction().getCode().equals(GroupeSousFunction.START_BY.getCode())) {
                        whereFields += "and " + libelle + " like '" + groupValues.get(index).getValue().getLibelle()
                            + "%' ";
                    }
                    else if (groupValues
                        .get(index).getSousFunction().getCode().equals(GroupeSousFunction.FINISH_BY.getCode())) {
                        whereFields += "and " + libelle + " like '%" + groupValues.get(index).getValue().getLibelle()
                            + " ";
                    }

                }
                else {
                    excludeFields += "and " + libelle + " !='" + groupValues.get(index).getValue().getLibelle() + "' ";
                }

            }
            else if (typeTraitement.equals(GroupeTypeTraitemant.BY_CODE.getCode())) {

                if (!GroupeFunction.ARE_EXCLUDED.getCode().equals(typeField)) {
                    whereFields += "and " + libelle + " = " + groupValues.get(index).getValue().getCode() + " ";
                }
                else {
                    excludeFields += "and " + libelle + " != " + groupValues.get(index).getValue().getCode() + " ";
                }

            }
            else if (typeTraitement.equals(GroupeTypeTraitemant.BY_CATEGORIE.getCode())) {

                if (index == 0) {
                    groupedFields += "categorie" + criteriaNumber + ",";

                    if (sqlQuerySupplementaire.indexOf("CASE WHEN") > 0) {
                        sqlQuerySupplementaire += ", ";
                    }

                    sqlQuerySupplementaire += "CASE WHEN ";
                }
                else if (index > 0) {
                    sqlQuerySupplementaire += "AND ";
                }

                if (!GroupeFunction.ARE_EXCLUDED.getCode().equals(typeField)) {
                    sqlQuerySupplementaire += "(list_labels LIKE '" + groupValues.get(index).getValue().getCode()
                        + ",%' "
                        + "OR list_labels LIKE '%," + groupValues.get(index).getValue().getCode() + "' "

                        + "OR list_labels LIKE '%," + groupValues.get(index).getValue().getCode() + ",%') ";
                }
                else {
                    sqlQuerySupplementaire += "(list_labels NOT LIKE '" + groupValues.get(index).getValue().getCode()
                            + ",%' "
                            + "OR list_labels NOT LIKE '%," + groupValues.get(index).getValue().getCode() + "' "

                            + "OR list_labels NOT LIKE '%," + groupValues.get(index).getValue().getCode() + ",%') ";
                }

                if (index == groupValues.size() - 1) {
                    sqlQuerySupplementaire += "THEN true  END AS categorie" + criteriaNumber + " ";
                }

            }
            else if (typeTraitement.equals(GroupeTypeTraitemant.BY_CUSTOM_FIELDS.getCode())) {

                if (index == 0) {
                    groupedFields += "custom_fields" + criteriaNumber + ",";

                    if (sqlQuerySupplementaire.indexOf("CASE WHEN") > 0) {
                        sqlQuerySupplementaire += ", ";
                    }

                    sqlQuerySupplementaire += "CASE WHEN ";
                }
                else if (index > 0) {
                    sqlQuerySupplementaire += "AND ";
                }

                if (!GroupeFunction.ARE_EXCLUDED.getCode().equals(typeField))

                {
                    sqlQuerySupplementaire += "(list_custom_fields_flat LIKE '%field"
                        + groupValues.get(index).getCategorieValue().getCode() + "\":\""
                        + groupValues.get(index).getValue().getLibelle() + "%') ";
                }
                else {
                    sqlQuerySupplementaire += "(list_custom_fields_flat NOT LIKE '%field"
                        + groupValues.get(index).getCategorieValue().getCode() + "\":\""
                        + groupValues.get(index).getValue().getLibelle() + "%') ";
                }

                if (index == groupValues.size() - 1) {
                    sqlQuerySupplementaire += "THEN true  END AS custom_fields" + criteriaNumber + " ";
                }

            }

        }

        return new SqlQueryGroupedAndWhereFields(
            groupedFields,
            whereFields,
            joinFields,
            excludeFields,
            sqlQuerySupplementaire);
    }

    private String setUniqueValue(String listValues, String value) {

        if (listValues != null && listValues.indexOf(value) < 0) {
            listValues += value;
        }

        return listValues;
    }

    /**
     * this method allows us to return a list of initial requests (EbDemande)
     * for a given transport
     * reference or
     * customer reference.
     * The returned requests have a status (strictly between pending and
     * confirmed) and are not
     * canceled or belong to a grouping proposal
     */
    @Override
    public List<EbDemande> getListEbDemandeInitials(SearchCriteriaPricingBooking criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);
        List<EbDemande> resultat = null;
        Initialiaze();
        criteria.setModule(Enumeration.Module.PRICING.getCode());

        try {
            String searchTerm = criteria.getSearchterm();
            JPAQuery<EbDemande> query = new JPAQuery<EbDemande>(em);
            BooleanBuilder where = new BooleanBuilder();

            /******************** VISIBLITE DES USERS *******************/
            BooleanBuilder whereVisibilite = applyVisibilityRules(
                connectedUser,
                criteria.getModule(),
                qEbEtablissementCt,
                qEtablissement,
                qChargeurEtab,
                qChargeurComp,
                qEbDemande,
                qUserDestEtab,
                qUserOriginEtab,
                qUserOriginCBEtab,
                qUserDestCBEtab,
                qCompagnie,
                qExDemandeTransporteur);

            query
                .select(
                    QEbDemande
                        .create(
                            qEbDemande.ebDemandeNum,
                            qEbDemande.refTransport,
                            qNamedCostCenter.ebCostCenterNum,
                            qNamedCostCenter.libelle,
                            qEbDemande.customerReference,
                            qEbDemande.totalWeight,
                            qEbDemande.xEcModeTransport,
                            qEbDemande.dateCreation,
                            qEbDemande.xEcStatut,
                            qEbDemande.xEcTypeDemande,
                            qEbDemande.xEbTypeRequest,
                            qEbDemande.libelleDestCountry,
                            qEbDemande.libelleOriginCountry,
                            qEbDemande.dateOfGoodsAvailability,
                            qEbDemande.dateOfArrival,
                            qEbDemande.datePickup,
                            qPartyOrigin,
                            qCountryOrigin,
                            qPartyDest,
                            qCountryDest,
                            qPartyNotif,
                            qCountryNotif,
                            qEbDemande.xEbTypeTransport,
                            qEbDemande.flagRecommendation,
                            qEbDemande.commentChargeur,
                            qEbDemande.totalVolume,
                            qEbDemande.totalTaxableWeight,
                            qEbDemande.insurance,
                            qEbDemande.insuranceValue,
                            qEbDemande.city,
                            qEbDemande.listCategories,
                            qEbDemande.totalNbrParcel,
                            qUserOriginCB.ebUserNum,
                            qUserOriginCB.email,
                            qUserOriginCBEtab.ebEtablissementNum,
                            qUserDestCB.ebUserNum,
                            qUserDestCB.email,
                            qUserDestCBEtab.ebEtablissementNum,
                            qEbDemande.xEcTypeCustomBroker,
                            qEbDemande.xEbTransporteur,
                            qEbDemande.xEbTransporteurEtablissementNum,
                            qEbDemande.flagDocumentCustoms,
                            qEbDemande.flagDocumentTransporteur,
                            qEbDemande.flagTransportInformation,
                            qEbDemande.flagCustomsInformation,
                            qEbDemande.flagRequestCustomsBroker,
                            qEbDemande.customFields,
                            qEbDemande.configPsl,
                            qEbDemande.numAwbBol,
                            qEbDemande.carrierUniqRefNum,
                            qEbDemande.flightVessel,
                            qEbDemande.datePickupTM,
                            qEbDemande.etd,
                            qEbDemande.finalDelivery,
                            qEbDemande.eta,
                            qEbDemande.customsOffice,
                            qEbDemande.mawb,
                            qEbDemande.flagEbtrackTrace,
                            qEbDemande.flagEbInvoice,
                            qChargeur,
                            qChargeurEtab.ebEtablissementNum,
                            qChargeurEtab.nom,
                            qChargeurComp.ebCompagnieNum,
                            qChargeurComp.nom,
                            qChargeurComp.code,
                            qEcCurrencyInvoice.ecCurrencyNum,
                            qEcCurrencyInvoice.code,
                            qEbDemande.ebDemandeHistory,
                            qEbDemande.moduleCreator,
                            qEbDemande.listLabels,
                            qEbDemande.xEcCancelled,
                            qEbDemande.xEcNature,
                            qEbDemande.pointIntermediate().ebPartyNum,
                            qPointeIntermediate.reference,
                            qPointeIntermediate.company,
                            qEbDemande.ebDemandeMasterObject().ebDemandeNum,
                            qEbDemande.ebDemandeMasterObject().refTransport,
                            qEbDemande.ebDemandeInitial().ebDemandeNum,
                            qEbDemande.ebDemandeInitial().refTransport,
                            qUserOrigin.ebUserNum,
                            qUserOrigin.nom,
                            qUserOriginEtab.ebEtablissementNum,
                            qUserOriginEtab.nom,
                            qUserDest.ebUserNum,
                            qUserDest.nom,
                            qUserDestEtab.ebEtablissementNum,
                            qUserDestEtab.nom,
                            qUserOriginCB.ebUserNum,
                            qUserOriginCB.nom,
                            qUserOriginCBEtab.ebEtablissementNum,
                            qUserOriginCBEtab.nom,
                            qUserDestCB.ebUserNum,
                            qUserDestCB.nom,
                            qUserDestCBEtab.ebEtablissementNum,
                            qUserDestCBEtab.nom,
                            qEbDemande.listFlag,
                            qEbDemande.xEbSchemaPsl(),
                            qEbDemande.dg,
                            qEbDemande.eligibleForConsolidation,
                            qZoneOrigin,
                            qZoneDest,
                            qEbDemande.xEbTypeFluxNum,
                            qEbDemande.xEbTypeFluxCode,
                            qEbDemande.xEbTypeFluxDesignation,
                            qEbDemande.typeRequestLibelle,
                            qEbDemande.xEbIncotermNum,
                            qEbDemande.unitsReference,
                            qEbDemande.unitsPackingList));

            if (whereVisibilite.hasValue()) where.and(whereVisibilite);

            if (searchTerm != null && !(searchTerm = criteria.getSearchterm().toLowerCase().trim()).isEmpty()) {
                where
                    .andAnyOf(
                        qEbDemande.refTransport.toLowerCase().trim().contains(searchTerm),
                        qEbDemande.customerReference.toLowerCase().trim().contains(searchTerm));
            }

            where.andAnyOf(qEbDemande.isGrouping.eq(false), qEbDemande.isGrouping.isNull());
            where.and(qEbDemande.eligibleForConsolidation.eq(true));
            where.andAnyOf(qEbDemande.cancelled.eq(false), qEbDemande.cancelled.isNull());
            where
                .andAnyOf(
                    qEbDemande.xEcCancelled.ne(CancelStatus.CANECLLED.getCode()),
                    qEbDemande.xEcCancelled.isNull());
            where
                .and(
                    qEbDemande.xEcStatut
                        .between(Enumeration.StatutDemande.INP.getCode(), Enumeration.StatutDemande.REC.getCode()));

            query.from(qEbDemande).where(where);
            query.leftJoin(qEbDemande.ebPartyDest(), qPartyDest);
            query.leftJoin(qEbDemande.ebPartyOrigin(), qPartyOrigin);
            query.leftJoin(qEbDemande.ebPartyNotif(), qPartyNotif);
            query.leftJoin(qPartyOrigin.xEcCountry(), qCountryOrigin);
            query.leftJoin(qPartyDest.xEcCountry(), qCountryDest);
            query.leftJoin(qPartyNotif.xEcCountry(), qCountryNotif);
            query.leftJoin(qEbDemande.pointIntermediate(), qPointeIntermediate);
            query.leftJoin(qEbDemande.xEbCostCenter(), qNamedCostCenter);
            query.leftJoin(qEbDemande.xecCurrencyInvoice(), qEcCurrencyInvoice);
            query.leftJoin(qEbDemande.ebDemandeMasterObject(), qEbDemandeMultiSegmentObject);
            query.leftJoin(qEbDemande.ebDemandeInitial(), qEbDemandeInitial);
            query.leftJoin(qPartyOrigin.zone(), qZoneOrigin);
            query.leftJoin(qPartyDest.zone(), qZoneDest);
            query.leftJoin(qEbDemande.xEbCompagnie(), qEbCompagnie);
            query.leftJoin(qEbDemande.xEbCostCenter(), qNamedCostCenter);
            query.leftJoin(qEbDemande.exEbDemandeTransporteurs, qExDemandeTransporteur);
            query.leftJoin(qExDemandeTransporteur.xEbEtablissement(), qEtablissement);
            query.leftJoin(qExDemandeTransporteur.xEbCompagnie(), qCompagnie);

            query.leftJoin(qEbDemande.user(), qChargeur);
            query.leftJoin(qEbDemande.xEbEtablissement(), qChargeurEtab);
            query.leftJoin(qEbDemande.xEbCompagnie(), qChargeurComp);
            query.leftJoin(qEbDemande.ebPartyDest(), qPartyDest);
            query.leftJoin(qEbDemande.ebPartyOrigin(), qPartyOrigin);
            query.leftJoin(qChargeur.ebCompagnie(), qCompagnieChargeur);
            query.leftJoin(qEbDemande.ebDemandeMasterObject(), qEbDemandeMultiSegmentObject);
            query.leftJoin(qEbDemande.xEbEntrepotUnloading(), qEbEntrepot);
            query.leftJoin(qEbDemande.xEbUserOrigin(), qUserOrigin);
            query.leftJoin(qUserOrigin.ebEtablissement(), qUserOriginEtab);
            query.leftJoin(qEbDemande.xEbUserDest(), qUserDest);
            query.leftJoin(qUserDest.ebEtablissement(), qUserDestEtab);
            query.leftJoin(qEbDemande.xEbUserOriginCustomsBroker(), qUserOriginCB);
            query.leftJoin(qUserOriginCB.ebEtablissement(), qUserOriginCBEtab);
            query.leftJoin(qEbDemande.xEbUserDestCustomsBroker(), qUserDestCB);
            query.leftJoin(qUserDestCB.ebEtablissement(), qUserDestCBEtab);
            query.leftJoin(qEbDemande.xEbUserCt(), qEbUserCt);
            query.leftJoin(qEbDemande.xEbEtablissementCt(), qEbEtablissementCt);
            query.distinct();
            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public List<Integer> getListDemandeByGroupingRule(Integer ebQrGroupeNum) {
        return ebQrGroupePropositionRepository.getListEbDemandeByEbQrGroupeNum(ebQrGroupeNum);
    }

    @Override
    public List<EbUser> getAllUsersIntervenantInDemande(Integer ebDemandeNum) {
        EbDemande demande = null;
        List<EbUser> users = new ArrayList<>();
        SearchCriteriaPricingBooking criterias = new SearchCriteriaPricingBooking();
        List<Integer> relatedEtablissementNums = new ArrayList<>();
        criterias.setEbDemandeNum(ebDemandeNum);

        try {
            if (ebDemandeNum != null) demande = this.getEbDemande(criterias, null);

            if (demande != null) {
                if (demande.getxEbTransporteurEtablissementNum() != null) relatedEtablissementNums
                    .add(demande.getxEbTransporteurEtablissementNum());

                if (demande.getUser() != null && demande.getUser().getEbEtablissement() != null
                    && demande.getUser().getEbEtablissement().getEbEtablissementNum() != null) relatedEtablissementNums
                        .add(demande.getUser().getEbEtablissement().getEbEtablissementNum());

                if (demande.getxEbUserOrigin() != null && demande.getxEbUserOrigin().getEbEtablissement() != null
                    && demande
                        .getxEbUserOrigin().getEbEtablissement()
                        .getEbEtablissementNum() != null) relatedEtablissementNums
                            .add(demande.getxEbUserOrigin().getEbEtablissement().getEbEtablissementNum());

                if (demande.getxEbUserDest() != null && demande.getxEbUserDest().getEbEtablissement() != null
                    && demande
                        .getxEbUserDest().getEbEtablissement().getEbEtablissementNum() != null) relatedEtablissementNums
                            .add(demande.getxEbUserDest().getEbEtablissement().getEbEtablissementNum());

                if (demande.getxEbUserOriginCustomsBroker() != null
                    && demande.getxEbUserOriginCustomsBroker().getEbEtablissement() != null
                    && demande
                        .getxEbUserOriginCustomsBroker().getEbEtablissement()
                        .getEbEtablissementNum() != null) relatedEtablissementNums
                            .add(demande.getxEbUserOriginCustomsBroker().getEbEtablissement().getEbEtablissementNum());

                if (demande.getxEbUserDestCustomsBroker() != null
                    && demande.getxEbUserDestCustomsBroker().getEbEtablissement() != null
                    && demande
                        .getxEbUserDestCustomsBroker().getEbEtablissement()
                        .getEbEtablissementNum() != null) relatedEtablissementNums
                            .add(demande.getxEbUserDestCustomsBroker().getEbEtablissement().getEbEtablissementNum());

                users = this.ebUserRepository.findByEbEtablissement_ebEtablissementNumIn(relatedEtablissementNums);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return users;
    }
}
