package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoSavedForm;
import com.adias.mytowereasy.model.EbSavedForm;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.QEbSavedForm;
import com.adias.mytowereasy.model.QEbUser;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoSavedFormImpl extends Dao implements DaoSavedForm {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbSavedForm qEbSavedForm = QEbSavedForm.ebSavedForm;
    QEbUser qEbSavedFormUser = new QEbUser("qEbSavedFormUser");
    QEbCompagnie qEbSavedFormCompagnie = new QEbCompagnie("qEbSavedFormCompagnie");

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbSavedForm> listRules(SearchCriteria criteria) {
        List<EbSavedForm> resultat = new ArrayList<>();

        try {
            JPAQuery<EbSavedForm> query = new JPAQuery<EbSavedForm>(em);
            BooleanBuilder where = new BooleanBuilder();

						query
							.select(
								QEbSavedForm
									.create(
										qEbSavedForm.ebSavedFormNum,
										qEbSavedForm.formName,
										qEbSavedForm.formValues,
										qEbSavedForm.ebModuleNum,
										qEbSavedForm.ebCompNum,
										qEbSavedForm.dateAjout,
										qEbSavedForm.action,
										qEbSavedForm.actionTrigger,
										qEbSavedForm.actionTargets,
										qEbSavedForm.actionParameters,
										qEbSavedForm.ordre,
										qEbSavedFormUser.ebUserNum,
										qEbSavedFormCompagnie.ebCompagnieNum,
										qEbSavedForm.url
									)
							);

            query = getRulesGlobalWhere(query, where, criteria);
            query.from(qEbSavedForm);
            query.where(where);
            query = getGlobalJoin(query, where, criteria);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbSavedForm, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbSavedForm.ordre.desc());
            }

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long countListRules(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbSavedForm> query = new JPAQuery<EbSavedForm>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getRulesGlobalWhere(query, where, criteria);
            query.from(qEbSavedForm);
            query.where(where);
            query = getGlobalJoin(query, where, criteria);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getRulesGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {

        if (criteria.getEbUserNum() != null) {
            where.and(qEbSavedFormUser.ebUserNum.eq(criteria.getEbUserNum()));
        }

        if (criteria.getEbCompNum() != null) {
            where.and(qEbSavedForm.ebCompNum.eq(criteria.getEbCompNum()));
        }

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where.andAnyOf(qEbSavedForm.formName.toLowerCase().contains(term));
        }

        return query;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    private JPAQuery getGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        query.leftJoin(qEbSavedForm.compagnie(), qEbSavedFormCompagnie);
        query.leftJoin(qEbSavedForm.ebUser(), qEbSavedFormUser);
        return query;
    }
}
