package com.adias.mytowereasy.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoCostItem;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoCostItemImpl implements DaoCostItem {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbCostCategorie qEbCostCategorie = QEbCostCategorie.ebCostCategorie;
    QEbCost qEbCost = QEbCost.ebCost;
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbZoneCompagnie");

    @Override
    public List<EbCost> getListCostItemsByCompagnie(SearchCriteria criteria) {
        List<EbCost> results = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbCost> query = new JPAQuery<>(em);
            query.select(qEbCost);
            query.from(qEbCost);
            where = this.getGlobalWhere(criteria);
            query.leftJoin(qEbCost.listCostCategorie, qEbCostCategorie).fetchJoin();
            query.where(where);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbCost, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbCost.libelle.asc());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            results = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results;
    }

    public Long getCountListCostItemByCompagnie(SearchCriteria criteria) {
        Long results = new Long(0);

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbCostCategorie> query = new JPAQuery<>(em);

            query.select(qEbCost);
            query.from(qEbCost);
            where = this.getGlobalWhere(criteria);
            query.where(where);

            results = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results;
    }

    private BooleanBuilder getGlobalWhere(SearchCriteria criteria) {
        BooleanBuilder where = new BooleanBuilder();

        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);

        Integer ebCompagnieNum = criteria.getEbCompagnieNum() != null ?
            criteria.getEbCompagnieNum() :
            connectedUser.getEbCompagnie().getEbCompagnieNum();

        where.and(qEbCost.xEbCompagnie().ebCompagnieNum.eq(ebCompagnieNum));

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where.andAnyOf(qEbCost.libelle.toLowerCase().contains(term), qEbCost.code.toLowerCase().contains(term));
        }

        return where;
    }

    @Override
    public Integer nextValEbCost() {
        Integer result = null;

        try {
            result = ((BigInteger) em
                .createNativeQuery("SELECT NEXTVAL('work.eb_cost_eb_cost_num_seq')").getSingleResult()).intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public Integer nextValEbCostCategorie() {
        Integer result = null;

        try {
            result = ((BigInteger) em
                .createNativeQuery("SELECT NEXTVAL('work.eb_cost_categorie_eb_cost_categorie_num_seq')")
                .getSingleResult()).intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
