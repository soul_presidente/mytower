package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoIncoterm;
import com.adias.mytowereasy.model.EbIncoterm;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.QEbIncoterm;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoIncotermImpl implements DaoIncoterm {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbIncoterm qEbIncoterm = QEbIncoterm.ebIncoterm;
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbZoneCompagnie");

    @Override
    public List<EbIncoterm> getListIncoterms(SearchCriteria criteria) {
        List<EbIncoterm> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbIncoterm> query = new JPAQuery<>(em);

            query.select(qEbIncoterm);
            query = getTypeFluxGlobalWhere(query, where, criteria);
            query.from(qEbIncoterm);
            query.where(where);

            query = getTypeFluxGlobalJoin(query, where, criteria);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbIncoterm, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbIncoterm.libelle.asc());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public Long getListIncotermsCount(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbIncoterm> query = new JPAQuery<EbIncoterm>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getTypeFluxGlobalWhere(query, where, criteria);
            query.from(qEbIncoterm);
            query.where(where);

            query = getTypeFluxGlobalJoin(query, where, criteria);
            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private JPAQuery getTypeFluxGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);

        Integer ebCompagnieNum = criteria.getEbCompagnieNum() != null ?
            criteria.getEbCompagnieNum() :
            connectedUser.getEbCompagnie().getEbCompagnieNum();

        where.and(qEbCompagnie.ebCompagnieNum.eq(ebCompagnieNum));

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qEbIncoterm.libelle.toLowerCase().contains(term),
                    qEbIncoterm.code.toLowerCase().contains(term));
        }

        return query;
    }

    private JPAQuery getTypeFluxGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        query.leftJoin(qEbIncoterm.xEbCompagnie(), qEbCompagnie);
        return query;
    }
}
