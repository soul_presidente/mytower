/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao;

import org.json.JSONException;
import org.json.JSONObject;

import com.adias.mytowereasy.utils.search.SearchCriteriaFreightAnalytics;


public interface DaoAnalytics {
    public JSONObject getTransportData(SearchCriteriaFreightAnalytics criteria) throws JSONException;

    public JSONObject getActivityData(SearchCriteriaFreightAnalytics criteria) throws JSONException;
}
