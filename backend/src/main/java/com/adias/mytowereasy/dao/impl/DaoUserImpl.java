/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.ConstructorExpression;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.airbus.model.QEbUserProfile;
import com.adias.mytowereasy.dao.DaoUser;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.EmailConfig;
import com.adias.mytowereasy.model.Enumeration.EtatRelation;
import com.adias.mytowereasy.model.Enumeration.Role;
import com.adias.mytowereasy.model.Enumeration.UserStatus;
import com.adias.mytowereasy.repository.EbGroupUserRepository;
import com.adias.mytowereasy.repository.EbRelationRepository;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.repository.ExEbUserModuleRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Repository
public class DaoUserImpl extends Dao implements DaoUser {
    @Autowired
    private EbUserRepository ebUserRepository;

    @Autowired
    private EbRelationRepository ebRelationRepository;

    @Autowired
    private ExEbUserModuleRepository exEbUserModuleRepository;

    @Autowired
    private ConnectedUserService connectedUserService;

    @PersistenceContext
    EntityManager em;

    @Autowired
    EbGroupUserRepository ebGroupUserRepository;

    QEbUser qEbUser = QEbUser.ebUser;
    QEbRelation qEbRelation = QEbRelation.ebRelation;
    QEbEtablissement qEbEtablissement = QEbEtablissement.ebEtablissement;
    QEbUserProfile qEbUserProfile = QEbUserProfile.ebUserProfile;
    QEbCompagnie qEbCompagnie = QEbCompagnie.ebCompagnie;
    QEbUser qEbUserHost = new QEbUser("userHost");
    QEbUser qEbUserGuest = new QEbUser("userGuest");
    QEbUser qUser = new QEbUser("user");
    QEbRelation qRel = new QEbRelation("relation");
    QEbEtablissement qEbEtabHost = new QEbEtablissement("etabHost");
    QEbEtablissement qEbEtabGuest = new QEbEtablissement("etabGuest");
    QEbDemande qEbDemande = QEbDemande.ebDemande;

    private static final Logger LOGGER = LoggerFactory.getLogger(DaoUserImpl.class);

    private Map<String, OrderSpecifier<?>> orderMapASC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, OrderSpecifier<?>> orderMapDESC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, Path<?>> orderMap = new HashMap<String, Path<?>>();

    private void Initialize() {
        orderMapASC.clear();
        orderMap.put("ebUserNum", qEbUser.ebUserNum);
        orderMapASC.put("ebUserNum", qEbUser.ebUserNum.asc());
        orderMapDESC.put("ebUserNum", qEbUser.ebUserNum.desc());

        orderMap.put("ebEtablissement", qEbEtablissement.nom);
        orderMapASC.put("ebEtablissement", qEbEtablissement.nom.asc());
        orderMapDESC.put("ebEtablissement", qEbEtablissement.nom.desc());

        orderMap.put("ebUserProfile", qEbUserProfile.nom);
        orderMapASC.put("ebUserProfile", qEbUserProfile.nom.asc());
        orderMapDESC.put("ebUserProfile", qEbUserProfile.nom.desc());

        orderMap.put("email", qEbUser.email);
        orderMapASC.put("email", qEbUser.email.asc());
        orderMapDESC.put("email", qEbUser.email.desc());

        orderMap.put("username", qEbUser.username);
        orderMapASC.put("username", qEbUser.username.asc());
        orderMapDESC.put("username", qEbUser.username.desc());

        orderMap.put("telephone", qEbUser.telephone);
        orderMapASC.put("telephone", qEbUser.telephone.asc());
        orderMapDESC.put("telephone", qEbUser.telephone.desc());

        orderMap.put("groupes", qEbUser.groupes);
        orderMapASC.put("groupes", qEbUser.groupes.asc());
        orderMapDESC.put("groupes", qEbUser.groupes.desc());

        orderMap.put("status", qEbUser.status);
        orderMapASC.put("status", qEbUser.status.asc());
        orderMapDESC.put("status", qEbUser.status.desc());

        orderMap.put("nomPrenom", qEbUser.nom);
        orderMapASC.put("nomPrenom", qEbUser.nom.asc());
        orderMapDESC.put("nomPrenom", qEbUser.nom.desc());

        orderMap.put("superAdmin", qEbUser.superAdmin);
        orderMapASC.put("superAdmin", qEbUser.superAdmin.asc());
        orderMapDESC.put("superAdmin", qEbUser.superAdmin.desc());
    }

    @Override
    public BigInteger selectCountListContact(SearchCriteria criteria) {
        BigInteger cnt = null;

        try {
            Query query = null;

            String sqlQuery = "";

            if (criteria.isCommunityEstablishment()) sqlQuery += " SELECT DISTINCT etab.eb_etablissement_num ";
            else if (criteria.isCommunityCompany()) sqlQuery += " SELECT DISTINCT comp.eb_compagnie_num ";
            else sqlQuery += " SELECT DISTINCT ebuser.eb_user_num ";

            sqlQuery += this.getGlobalJoinListContact(criteria);

            sqlQuery += this.getGlobalWhereListContact(criteria);

            this.setUnionSelectListContact(criteria, sqlQuery);

            sqlQuery = " WITH globalQuery AS ( " + sqlQuery + " ) SELECT ";

            if (criteria.isCommunityEstablishment()) sqlQuery += " COUNT (eb_etablissement_num) ";
            else if (criteria.isCommunityCompany()) sqlQuery += " COUNT (eb_compagnie_num) ";
            else sqlQuery += " COUNT (eb_user_num) ";

            sqlQuery += " FROM globalQuery ";

            query = this.getMapping(criteria, sqlQuery, em.createNativeQuery(sqlQuery));

            cnt = (BigInteger) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cnt;
    }

    private String setUnionSelectListContact(SearchCriteria criteria, String globalSqlQuery) {
        String sqlQuery = "";

        if (criteria.isSentRequests() ||
            !(criteria.isContact() || criteria.isBlackList() || criteria.isReceivedRequest())) {
            sqlQuery = " SELECT * FROM ( " + globalSqlQuery;
            // DEBUT UNION
            sqlQuery += " UNION ";

            sqlQuery += " SELECT ";

            if (criteria.isCommunityEstablishment()) {
                sqlQuery += " 	rel.eb_relation_num, ";
                sqlQuery += " 	rel.type_relation, ";
                sqlQuery += " 	rel.etat, ";
                sqlQuery += " 	rel.date_ajout, ";
                sqlQuery += " 	rel.email AS rel_email, ";
                sqlQuery += " 	rel.x_eb_etablissement, ";
                sqlQuery += " 	rel.x_eb_etablissement_host, ";
                sqlQuery += " 	rel.flag_etablissement, ";
                sqlQuery += " 	rel.x_last_modified_by, ";

                sqlQuery += " 	NULL AS eb_etablissement_num, ";
                sqlQuery += " 	NULL AS etab_nom, ";
                sqlQuery += " 	NULL AS etab_email, ";
                sqlQuery += " 	NULL AS etab_service, ";
                sqlQuery += " 	NULL AS etab_telephone, ";
                sqlQuery += " 	NULL AS etab_logo, ";
                sqlQuery += " 	NULL AS num_compagnie, ";

                sqlQuery += " 	rel.x_host AS x_host, ";
                sqlQuery += " 	rel.x_guest AS x_guest, ";

                sqlQuery += " 	NULL as host_nom, ";
                sqlQuery += " 	NULL as host_prenom, ";
                sqlQuery += " 	NULL as host_email, ";

                sqlQuery += " 	NULL AS etab_host_eb_etablissement_num, ";
                sqlQuery += " 	NULL AS etab_host_nom, ";

                if (criteria.isEtablissementRequest()) sqlQuery += " 	'true' AS is_establishment_request ";
                else sqlQuery += " 	'false' AS is_establishment_request ";
            }
            else if (criteria.isCommunityCompany()) {
                sqlQuery += " 	rel.eb_relation_num, ";
                sqlQuery += " 	rel.type_relation, ";
                sqlQuery += " 	rel.etat, ";
                sqlQuery += " 	rel.date_ajout, ";
                sqlQuery += " 	rel.email AS rel_email, ";
                sqlQuery += " 	rel.x_eb_etablissement, ";
                sqlQuery += " 	rel.x_eb_etablissement_host, ";
                sqlQuery += " 	rel.flag_etablissement, ";
                sqlQuery += " 	rel.x_last_modified_by, ";

                sqlQuery += " 	NULL AS eb_compagnie_num, ";
                sqlQuery += " 	NULL AS comp_nom, ";
                sqlQuery += " 	NULL AS compagnie_role, ";
                sqlQuery += " 	NULL AS comp_siren, ";
                sqlQuery += " 	NULL AS comp_code, ";

                sqlQuery += " 	rel.x_host AS x_host, ";
                sqlQuery += " 	rel.x_guest AS x_guest, ";

                sqlQuery += " 	NULL as host_nom, ";
                sqlQuery += " 	NULL as host_prenom, ";
                sqlQuery += " 	NULL as host_email ";
            }
            else {
                sqlQuery += " 	rel.eb_relation_num, ";
                sqlQuery += " 	rel.type_relation, ";
                sqlQuery += " 	rel.etat AS etat, ";
                sqlQuery += " 	rel.email AS rel_email, ";
                sqlQuery += " 	rel.x_eb_etablissement, ";
                sqlQuery += " 	rel.x_eb_etablissement_host, ";
                sqlQuery += " 	rel.flag_etablissement, ";
                sqlQuery += " 	rel.x_host AS x_host, ";
                sqlQuery += " 	rel.x_guest AS x_host, ";

                sqlQuery += " 	NULL AS eb_user_num, ";
                sqlQuery += " 	NULL AS nom, ";
                sqlQuery += " 	NULL AS prenom, ";
                sqlQuery += " 	NULL AS service, ";
                sqlQuery += " 	NULL AS role, ";
                sqlQuery += " 	NULL AS email, ";
                sqlQuery += " 	NULL AS telephone, ";

                sqlQuery += " 	NULL AS eb_etablissement_num, ";
                sqlQuery += " 	NULL AS etab_nom, ";
                sqlQuery += " 	NULL AS etab_email, ";
                sqlQuery += " 	NULL AS etab_service, ";
                sqlQuery += " 	NULL AS etab_telephone, ";

                sqlQuery += " 	NULL AS eb_compagnie_num, ";
                sqlQuery += " 	NULL AS comp_nom ";
            }

            sqlQuery += " FROM ";
            sqlQuery += " 	WORK .eb_relation rel ";

            sqlQuery += " WHERE ";

            sqlQuery += " 	rel.email IS NOT NULL ";
            sqlQuery += " AND rel.x_guest IS NULL ";

            if (criteria.getEbEtablissementNum() == null) {
                sqlQuery += " AND ( ";
                sqlQuery += " 	rel.x_guest = :iduser ";
                sqlQuery += " 	OR rel.x_host = :iduser ";
                sqlQuery += " ) ";
            }

            sqlQuery += " AND rel.etat = :encours ";

            // FIN UNION
            sqlQuery += ") AS subQuery ";
        }

        return sqlQuery;
    }

    private String getGlobalWhereListContact(SearchCriteria criteria) {
        String sqlQuery = " WHERE ";
        String subWhereQuery = "";
        EbUser connectedUser = null;
        String term = criteria.getSearchterm();

        connectedUser = ebUserRepository.findOneByEbUserNum(criteria.getEbUserNum());

        sqlQuery += " ( ";
        sqlQuery += " 	rel.etat <> :blacklist ";
        sqlQuery += " 	OR ( ";
        sqlQuery += " 		rel.etat = :blacklist ";
        if (criteria.getEbEtablissementNum() == null) sqlQuery += " 		AND rel.x_last_modified_by = :iduser ";
        sqlQuery += " 	) ";
        sqlQuery += " 	OR rel.eb_relation_num IS NULL ";
        sqlQuery += " ) ";

        if (criteria.isSentRequests()) {
            sqlQuery += " AND rel.email IS NOT NULL ";
        }
        else {
            sqlQuery += " AND ebuser.eb_user_num <> :iduser ";
        }

        if (!(criteria.isRoleBroker() ||
            criteria.isForControlTower() || criteria.isRoleChargeur() || criteria.isRoleTransporteur())) {

            if (connectedUser != null &&
                Enumeration.Role.ROLE_CONTROL_TOWER.getCode().equals(connectedUser.getRole()) &&
                criteria.getEbDemandeNum() == null && !criteria.isSentRequests()) {
                sqlQuery += " AND ( ";
                sqlQuery += " 	ebuser. ROLE = :roleguestprestataire ";
                sqlQuery += " 	OR ebuser. ROLE = :roleguestchargeur ";
                sqlQuery += " ) ";
            }
            else if (connectedUser != null &&
                Enumeration.Role.ROLE_CHARGEUR.getCode().equals(connectedUser.getRole()) &&
                criteria.getEbDemandeNum() == null && !criteria.isSentRequests()) {
                    sqlQuery += " AND ( ";
                    sqlQuery += " 	ebuser. ROLE = :roleguestprestataire ";
                    sqlQuery += " 	OR ebuser. ROLE = :rolecontroltower ";
                    sqlQuery += " ) ";
                }

        }

        if (criteria.isContact()) {
            subWhereQuery += " rel.etat = :actif ";
        }

        if (criteria.getCompanyRole() != null) {
            sqlQuery += "   AND comp.compagnie_role = :companyRole";
        }

        if (criteria.isBlackList()) {
            if (!subWhereQuery.isEmpty()) subWhereQuery += " OR ";
            subWhereQuery += " rel.etat = :blacklist ";
        }

        if (criteria.isContactable()) {
            if (!subWhereQuery.isEmpty()) subWhereQuery += " OR ";
            subWhereQuery += " rel.eb_relation_num IS NULL ";
        }

        if (criteria.isSentRequests()) {
            if (!subWhereQuery.isEmpty()) subWhereQuery += " OR ";
            subWhereQuery += " ( ";
            subWhereQuery += " 	rel.etat = :encours ";
            subWhereQuery += " 	AND rel.x_host = :iduser ";
            subWhereQuery += " ) ";
        }

        if (criteria.isReceivedRequest()) {
            if (!subWhereQuery.isEmpty()) subWhereQuery += " OR ";
            subWhereQuery += " ( ";
            subWhereQuery += " 	rel.etat = :encours ";
            subWhereQuery += " 	AND rel.x_guest = :iduser ";
            subWhereQuery += " ) ";
        }

        if (!subWhereQuery.isEmpty()) sqlQuery += " AND ( " + subWhereQuery + " ) ";
        subWhereQuery = "";

        if (connectedUser != null && connectedUser.getRole().equals(Enumeration.Role.ROLE_PRESTATAIRE.getCode())) {
            sqlQuery += " AND rel.eb_relation_num IS NOT NULL ";
        }

        if (criteria.isRoleTransporteur() || criteria.isRoleBroker() || criteria.isRoleControlTower()) {
            sqlQuery += " AND ( ";
            sqlQuery += " ( ";
            sqlQuery += " 		ebuser.service IS NOT NULL ";
            sqlQuery += "  AND   ebuser.enabled IS TRUE ";
            sqlQuery += "  AND   ebuser.status = :actif ";
            sqlQuery += " 		AND ( ";
            sqlQuery += " 		ebuser.service = :transporteur_broker ";
            if (criteria.isRoleTransporteur()) sqlQuery += " 	OR		ebuser.service = :transporteur ";
            if (criteria.isRoleBroker()) sqlQuery += " 	OR		ebuser.service = :broker ";
            sqlQuery += " 		) ";
            sqlQuery += " 	) ";
            if (criteria.isRoleControlTower()) sqlQuery += " OR ebuser.role = :rolecontroltower";
            sqlQuery += " ) ";
        }

        if (criteria.isNotBlackList()) {
            sqlQuery += " AND  rel.etat != :blacklist ";
        }

        if (criteria.getEbEtablissementNum() != null) {
            sqlQuery += " AND ( ";
            sqlQuery += " 	rel.x_eb_etablissement  = :useretab ";
            sqlQuery += " 	OR rel.x_eb_etablissement_host  = :useretab ";
            sqlQuery += " ) ";
        }

        if (criteria.isEtablissementRequest()) {
            sqlQuery += " AND ( ";
            sqlQuery += " 	rel.flag_etablissement IS NOT NULL ";
            sqlQuery += " 	AND rel.x_eb_etablissement = :useretab ";
            sqlQuery += " 	AND rel.x_guest IS NULL ";
            sqlQuery += " ) ";
        }

        if (criteria.isExcludeCt()) {
            sqlQuery += " 	AND ebuser.role <> :rolecontroltower ";
        }

        if (criteria.isWithActiveUsers()) {
            sqlQuery += " 	AND ebuser.status = :activeStatus ";
        }

        // recherche de terme
        if (term != null && !term.isEmpty()) {

            if (criteria.isCommunityEstablishment()) {
                sqlQuery += " AND ( ";
                sqlQuery += " 	LOWER(etab.nom) LIKE :term ";
                sqlQuery += " 	OR LOWER(etab.email) LIKE :term ";
                sqlQuery += " 	OR LOWER(etab.telephone) LIKE :term ";
                sqlQuery += " 	OR LOWER(etab.siret) LIKE :term ";
                sqlQuery += " ) ";
            }
            else if (criteria.isCommunityCompany()) {
                sqlQuery += " AND ( ";
                sqlQuery += " 	LOWER(comp.nom) LIKE :term ";
                sqlQuery += " 	OR LOWER(comp.code) LIKE :term ";
                sqlQuery += " 	OR LOWER(comp.siren) LIKE :term ";
                sqlQuery += " ) ";
            }
            else {
                sqlQuery += " AND ( ";
                sqlQuery += " 	LOWER(ebuser.nom) LIKE LOWER(:term) ";
                sqlQuery += " 	OR LOWER(ebuser.prenom) LIKE LOWER(:term) ";
                sqlQuery += " 	OR LOWER(ebuser.email) LIKE LOWER(:term) ";
                sqlQuery += " 	OR LOWER(ebuser.telephone) LIKE :term ";
                sqlQuery += " 	OR LOWER(tag.comment) LIKE LOWER(:term) ";
                sqlQuery += " 	OR LOWER(tag.tags) LIKE LOWER(:term) ";
                sqlQuery += " 	OR LOWER(etab.nom) LIKE LOWER(:term) ";
                sqlQuery += " ) ";
            }

        }

        return sqlQuery;
    }

    private String getGlobalJoinListContact(SearchCriteria criteria) {
        String sqlQuery = "";
        String orClause = "";

        sqlQuery += " FROM ";
        sqlQuery += " 	WORK.eb_user ebuser ";
        sqlQuery += " LEFT JOIN WORK.eb_relation rel ON ( ";
        sqlQuery += " 	( ";

        if (criteria.isSentRequests()) {
            sqlQuery += " rel.x_guest = ebuser.eb_user_num ";
            orClause = " OR ";
        }

        if (criteria.isReceivedRequest()) {
            sqlQuery += orClause + " rel.x_host = ebuser.eb_user_num ";
        }
        else if (!criteria.isSentRequests()) {
            orClause = " OR ";
            sqlQuery += " rel.x_guest = ebuser.eb_user_num " + orClause + " rel.x_host = ebuser.eb_user_num ";
        }

        sqlQuery += " 	) ";

        if (criteria.getEbEtablissementNum() == null) {
            sqlQuery += " 	AND ( ";
            sqlQuery += " 		rel.x_guest = :iduser ";
            sqlQuery += " 		OR rel.x_host = :iduser ";
            sqlQuery += " 	) ";
        }

        sqlQuery += " 	AND ( ";
        sqlQuery += " 		rel.etat <> :annule ";
        sqlQuery += " 	) ";
        sqlQuery += " ) ";

        if (criteria.isEtablissementRequest()) {
            sqlQuery += " 	RIGHT JOIN WORK .eb_etablissement etab ON ( ";
            sqlQuery += " 		etab.eb_etablissement_num = rel.x_eb_etablissement ";
            sqlQuery += " 	) ";
        }
        else {
            sqlQuery += " LEFT JOIN WORK.eb_etablissement etab ON ( ";
            sqlQuery += " 	ebuser.x_eb_etablissement = etab.eb_etablissement_num ";
            sqlQuery += " ) ";
        }

        sqlQuery += " LEFT JOIN WORK.eb_compagnie comp ON ( ";
        sqlQuery += " 	comp.eb_compagnie_num = etab.x_eb_compagnie ";
        sqlQuery += " ) ";

        if (criteria.getEbCompagnieNum() != null) {
            sqlQuery += " LEFT JOIN WORK.eb_user_tag tag ON ( ";
            sqlQuery += " 	(tag.x_guest = ebuser.eb_user_num) AND ( tag.x_eb_compagnie_host = :idUserCompagnie AND tag.activated = true) ";
            sqlQuery += " ) ";
        }

        if (criteria.isCommunityEstablishment()) {
            sqlQuery += " 	LEFT JOIN WORK .eb_etablissement etabHost ON ( ";
            sqlQuery += " 		etabHost.eb_etablissement_num = ebuser.x_eb_etablissement ";
            sqlQuery += " 	) ";
        }

        return sqlQuery;
    }

    private Query getMapping(SearchCriteria criteria, String sqlQuery, Query query) {
        EbUser user = new EbUser();
        Integer idUser = null;
        Integer idUserCompagnie = null;
        String term = criteria.getSearchterm();

        if (criteria.getEbUserNum() != null) user.setEbUserNum(criteria.getEbUserNum());

        if (criteria.isForChargeur()) {
            user.setRole(1);
        }

        idUser = user.getEbUserNum();

        if (criteria.getEbCompagnieNum() != null) {
            idUserCompagnie = criteria.getEbCompagnieNum();
        }

        if (sqlQuery.contains(":iduser")) query.setParameter("iduser", idUser);

        if (sqlQuery.contains(":idUserCompagnie")) query.setParameter("idUserCompagnie", idUserCompagnie);

        if (criteria.getPageNumber() != null && criteria.getSize() != null) {
            if (criteria.getSize() > 0 && sqlQuery.contains(":limit")) query.setParameter("limit", criteria.getSize());
            if (criteria.getPageNumber() <= 0 && sqlQuery.contains(":offset")) query.setParameter("offset", 0);
            else if (sqlQuery
                .contains(":offset")) query.setParameter("offset", (criteria.getPageNumber() - 1) * criteria.getSize());
        }

        if (sqlQuery.contains(":encours")) query.setParameter("encours", Enumeration.EtatRelation.EN_COURS.getCode());
        if (sqlQuery
            .contains(":blacklist")) query.setParameter("blacklist", Enumeration.EtatRelation.BLACKLIST.getCode());

        if (criteria.isContact() && sqlQuery.contains(":actif")) {
            query.setParameter("actif", Enumeration.EtatRelation.ACTIF.getCode());
        }
        if (criteria.isWithActiveUsers() && sqlQuery.contains(":activeStatus")) {
            query.setParameter("activeStatus", Enumeration.UserStatus.ACTIF.getCode());
        }

        if (sqlQuery.contains(":annule")) query.setParameter("annule", Enumeration.EtatRelation.ANNULER.getCode());

        if (sqlQuery.contains("rolecontroltower")) query
            .setParameter("rolecontroltower", Enumeration.Role.ROLE_CONTROL_TOWER.getCode());
        if (sqlQuery.contains("roleguestprestataire")) query
            .setParameter("roleguestprestataire", Enumeration.Role.ROLE_PRESTATAIRE.getCode());
        if (sqlQuery
            .contains(
                "roleguestchargeur")) query.setParameter("roleguestchargeur", Enumeration.Role.ROLE_CHARGEUR.getCode());

        if (term != null && !term.isEmpty()) {
            query.setParameter("term", "%" + term + "%");
        }

        if (criteria.isRoleTransporteur() || criteria.isRoleBroker() || criteria.isRoleControlTower()) {
            if (sqlQuery.contains(":transporteur_broker")) query
                .setParameter("transporteur_broker", Enumeration.ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
            if (criteria.isRoleBroker() && sqlQuery.contains(":broker")) query
                .setParameter("broker", Enumeration.ServiceType.SERVICE_BROKER.getCode());
            if (criteria.isRoleTransporteur() && sqlQuery.contains(":transporteur")) query
                .setParameter("transporteur", Enumeration.ServiceType.SERVICE_TRANSPORTEUR.getCode());
            if (criteria.isRoleControlTower() && sqlQuery.contains(":rolecontroltower")) query
                .setParameter("rolecontroltower", Enumeration.Role.ROLE_CONTROL_TOWER.getCode());
        }

        if (sqlQuery.contains(":useretab")) query.setParameter("useretab", criteria.getEbEtablissementNum());
        if (sqlQuery.contains(":companyRole")) query.setParameter("companyRole", criteria.getCompanyRole());

        return query;
    }

    @Override
    public Object selectListContact(SearchCriteria criteria) {
        List<EbUser> resultatUser = new ArrayList<EbUser>();
        List<EbRelation> resultatRelation = new ArrayList<EbRelation>();
        List<EbCompagnie> resultatCompagnie = new ArrayList<EbCompagnie>();
        String sqlQuery = "";
        Query query = null;
        Object resultat = null;

        try {

            /**
             * ********************************PARTIE
             * SELECT*****************************
             */
            if (criteria.isCommunityEstablishment()) {
                sqlQuery += " SELECT DISTINCT ON (etab.eb_etablissement_num) ";

                sqlQuery += " 	rel.eb_relation_num, ";
                sqlQuery += " 	rel.type_relation, ";
                sqlQuery += " 	rel.etat, ";
                sqlQuery += " 	rel.date_ajout, ";
                sqlQuery += " 	rel.email AS rel_email, ";
                sqlQuery += " 	rel.x_eb_etablissement, ";
                sqlQuery += " 	rel.x_eb_etablissement_host, ";
                sqlQuery += " 	rel.flag_etablissement, ";
                sqlQuery += " 	rel.x_last_modified_by, ";

                sqlQuery += " 	etab.eb_etablissement_num, ";
                sqlQuery += " 	etab.nom AS etab_nom, ";
                sqlQuery += " 	etab.email AS etab_email, ";
                sqlQuery += " 	etab.service AS etab_service, ";
                sqlQuery += " 	etab.telephone AS etab_telephone, ";
                sqlQuery += " 	etab.logo AS etab_logo, ";
                sqlQuery += " 	etab.x_eb_compagnie as num_compagnie, ";

                sqlQuery += " 	rel.x_host AS x_host, ";
                sqlQuery += " 	rel.x_guest AS x_guest, ";

                sqlQuery += " 	ebuser.nom as host_nom, ";
                sqlQuery += " 	ebuser.prenom as host_prenom, ";
                sqlQuery += " 	ebuser.email as host_email, ";

                sqlQuery += " 	etabHost.eb_etablissement_num as etab_host_eb_etablissement_num, ";
                sqlQuery += " 	etabHost.nom as etab_host_nom, ";

                sqlQuery += " 	comp.eb_compagnie_num, ";
                sqlQuery += " 	comp.nom AS comp_nom,";

                if (criteria.isEtablissementRequest()) sqlQuery += " 	'true' AS is_establishment_request ";
                else sqlQuery += " 	'false' AS is_establishment_request ";
            }
            else if (criteria.isCommunityCompany()) {
                sqlQuery += " SELECT DISTINCT ON (comp.eb_compagnie_num) ";

                sqlQuery += " 	rel.eb_relation_num, ";
                sqlQuery += " 	rel.type_relation, ";
                sqlQuery += " 	rel.etat, ";
                sqlQuery += " 	rel.date_ajout, ";
                sqlQuery += " 	rel.email AS rel_email, ";
                sqlQuery += " 	rel.x_eb_etablissement, ";
                sqlQuery += " 	rel.x_eb_etablissement_host, ";
                sqlQuery += " 	rel.flag_etablissement, ";
                sqlQuery += " 	rel.x_last_modified_by, ";

                sqlQuery += " 	rel.x_host AS x_host, ";
                sqlQuery += " 	rel.x_guest AS x_guest, ";

                sqlQuery += " 	comp.eb_compagnie_num, ";
                sqlQuery += " 	comp.nom AS comp_nom, ";
                sqlQuery += " 	comp.compagnie_role, ";
                sqlQuery += " 	comp.siren AS comp_siren, ";
                sqlQuery += " 	comp.code AS comp_code, ";

                sqlQuery += " 	ebuser.nom as guest_nom, ";
                sqlQuery += " 	ebuser.prenom as guest_prenom, ";
                sqlQuery += " 	ebuser.email as guest_email ";
            }
            else {

                if (criteria.isSentRequests()) {
                    sqlQuery += " SELECT DISTINCT";
                }
                else {
                    sqlQuery += " SELECT DISTINCT ON (ebuser.eb_user_num) ";
                }

                sqlQuery += " 	rel.eb_relation_num, ";
                sqlQuery += " 	rel.type_relation, ";
                sqlQuery += " 	rel.etat, ";
                sqlQuery += " 	rel.email AS rel_email, ";
                sqlQuery += " 	rel.x_eb_etablissement, ";
                sqlQuery += " 	rel.x_eb_etablissement_host, ";
                sqlQuery += " 	rel.flag_etablissement, ";
                sqlQuery += " 	rel.x_host, ";
                sqlQuery += " 	rel.x_guest, ";

                if (criteria.getEbCompagnieNum() != null) {
                    sqlQuery += " 	tag.tags, ";
                    sqlQuery += " 	tag.comment,";
                    sqlQuery += " 	tag.activated,";
                    sqlQuery += " 	tag.eb_user_tag_num,";
                    sqlQuery += " 	tag.x_guest as tag_guest ,";
                    sqlQuery += " 	tag.x_host as tag_host,";
                    sqlQuery += " 	tag.x_eb_compagnie_guest as tag_compagnie_guest,";
                    sqlQuery += " 	tag.x_eb_compagnie_host as tag_compagnie_host,";
                    sqlQuery += " 	tag.x_eb_etablissement_guest as tag_etablissement_guest,";
                    sqlQuery += " 	tag.x_eb_etablissement_host as tag_etablissement_host,";
                }
                else {
                    sqlQuery += " NULL as	tags, ";
                    sqlQuery += " NULL as	comment,";
                    sqlQuery += " false as	activated,";
                    sqlQuery += " 0 as	eb_user_tag_num,";
                    sqlQuery += " 0 as tag_guest ,";
                    sqlQuery += " 0 as tag_host,";
                    sqlQuery += " 0 as	tag_compagnie_guest,";
                    sqlQuery += " 0 as	tag_compagnie_host,";
                    sqlQuery += " 0 as	tag_etablissement_guest,";
                    sqlQuery += " 0 as	tag_etablissement_host,";
                }

                sqlQuery += " 	ebuser.eb_user_num, ";
                sqlQuery += " 	ebuser.nom, ";
                sqlQuery += " 	ebuser.prenom, ";
                sqlQuery += " 	ebuser.service, ";
                sqlQuery += " 	ebuser.role, ";
                sqlQuery += " 	ebuser.email, ";
                sqlQuery += " 	ebuser.telephone, ";
                sqlQuery += "   ebuser.enabled, ";
                sqlQuery += "   ebuser.status, ";

                sqlQuery += " 	etab.eb_etablissement_num, ";
                sqlQuery += " 	etab.nom AS etab_nom, ";
                sqlQuery += " 	etab.email AS etab_email, ";
                sqlQuery += " 	etab.service AS etab_service, ";
                sqlQuery += " 	etab.telephone AS etab_telephone, ";

                sqlQuery += " 	comp.eb_compagnie_num, ";
                sqlQuery += " 	comp.nom AS comp_nom, ";
                sqlQuery += " 	comp.compagnie_role AS comp_role, ";
                sqlQuery += " 	comp.siren AS comp_siren ";
            }

            /**
             * ********************************JOITURES*****************************
             */
            sqlQuery += this.getGlobalJoinListContact(criteria);

            /**
             * ********************************PARTIE
             * WHERE*****************************
             */
            sqlQuery += this.getGlobalWhereListContact(criteria);

            // UNION pour prendre en compte les relations ou l'user n'existe pas
            // Le FULL JOIN aurait pu remplacer ça, mais ça ne marche pas en
            // Postgres quand il y a
            // plusieurs criteres dans le ON(...)
            this.setUnionSelectListContact(criteria, sqlQuery);

            /**
             * ********************************PARTIE ORDER BY ET
             * PAGINATION*****************************
             */
            if (criteria
                .isCommunityEstablishment()) sqlQuery += " ORDER BY eb_etablissement_num, etat IS NOT NULL DESC ";
            else if (criteria.isCommunityCompany()) sqlQuery += " ORDER BY eb_compagnie_num, etat IS NOT NULL DESC ";
            else {
                if (criteria.getPageNumber() == null ||
                    criteria.getPageNumber().compareTo(-1) > 0) sqlQuery += " ORDER BY eb_user_num ASC ";
                else sqlQuery += " ORDER BY eb_user_num DESC ";
            }

            if (criteria.getPageNumber() != null && criteria.getSize() != null) {
                sqlQuery += " OFFSET :offset ";
                if (criteria.getSize().compareTo(0) > 0) sqlQuery += " LIMIT :limit ";
            }

            /*
             * if(criteria.getPageNumber() != null &&
             * criteria.getPageNumber().equals(-1)) {
             * sqlQuery = " WITH query AS ( " + sqlQuery + " ) " +
             * " SELECT * FROM query ORDER BY eb_user_num ASC ";
             * }
             */

            /**
             * ********************************MAPPING DES
             * RESULTATS*****************************
             */
            // voir le mapping @SqlResultSetMapping de la classe EbUser
            String mapper = "";
            if (criteria.isCommunityEstablishment()) mapper = "EtablissementContactMapping";
            else if (criteria.isCommunityCompany()) mapper = "CompagnieContactMapping";
            else mapper = "ContactMapping";

            query = this.getMapping(criteria, sqlQuery, em.createNativeQuery(sqlQuery, mapper));

            System.out.println(getResultQuery(query, sqlQuery));

            if (criteria.isCommunityEstablishment()) resultatRelation = query.getResultList();
            else if (criteria.isCommunityCompany()) resultatCompagnie = query.getResultList();
            else resultatUser = query.getResultList();

            if (criteria.isCommunityEstablishment()) resultat = resultatRelation;
            else if (criteria.isCommunityCompany()) resultat = resultatCompagnie;
            else resultat = resultatUser;

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public boolean updateUserGroup(SearchCriteria criteria, Integer ebUserNum) {
        EbUser ebUser = ebUserRepository.findOneByEbUserNum(ebUserNum);
        String listUserGroup = "|";

        if (criteria.getListGroupeNum() != null && !criteria.getListGroupeNum().isEmpty()) {

            for (Long ebUserGroupNum: criteria.getListGroupeNum()) {
                listUserGroup.concat(ebUserGroupNum + "|");
            }

        }

        ebUser.setGroupes(listUserGroup);
        ebUserRepository.save(ebUser);
        return true;
    }

    @Override
    public List<EbUser> selectListEbUser(SearchCriteria criterias) {
        List<EbUser> resultat;
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criterias);
        QEbRelation qRelHost = new QEbRelation("qRelHost");

        Initialize();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbUser> query = new JPAQuery<EbUser>(em);

            if (criterias.getEmailNomPrenom() != null && !criterias.getEmailNomPrenom().trim().isEmpty()) {
                where
                    .andAnyOf(
                        qEbUser.nom.toLowerCase().trim().contains(criterias.getEmailNomPrenom().toLowerCase().trim()),
                        qEbUser.prenom
                            .toLowerCase().trim().contains(criterias.getEmailNomPrenom().toLowerCase().trim()),
                        qEbUser.email
                            .toLowerCase().trim().contains(criterias.getEmailNomPrenom().toLowerCase().trim()));
            }

            String searchTerm = criterias.getSearchterm();

            if (searchTerm != null && !(searchTerm = criterias.getSearchterm().toLowerCase().trim()).isEmpty()) {

                if (connectedUser.isControlTower()) {
                    BooleanBuilder userWhere = new BooleanBuilder();
                    userWhere
                        .andAnyOf(
                            qEbUser.nom.toLowerCase().trim().contains(searchTerm),
                            qEbUser.prenom.toLowerCase().trim().contains(searchTerm),
                            qEbUser.email.toLowerCase().trim().contains(searchTerm));

                    if (criterias.isSimpleSearch()) where.and(userWhere);
                    else {
                        where
                            .andAnyOf(
                                userWhere,
                                qEbEtablissement.nom.toLowerCase().trim().contains(searchTerm),
                                qEbCompagnie.nom.toLowerCase().trim().contains(searchTerm));
                    }

                }
                else {
                    where
                        .andAnyOf(
                            qEbUser.nom.toLowerCase().trim().contains(searchTerm),
                            qEbUser.email.toLowerCase().trim().contains(searchTerm),
                            qEbUser.prenom.toLowerCase().trim().contains(searchTerm),
                            qEbEtablissement.nom.toLowerCase().trim().contains(searchTerm));
                }

            }

            if (criterias.getEbUserNum() != null) where.and(qEbUser.ebUserNum.ne(criterias.getEbUserNum()));
            else if (criterias.getUserNum() != null) // for access controle
                                                     // verification, to check
                                                     // if the connectedUser can
                                                     // show this user
                where.and(qEbUser.ebUserNum.eq(criterias.getUserNum()));

            if (connectedUser.isChargeur() ||
                connectedUser.isPrestataire() ||
                (connectedUser.isControlTower() &&
                    Arrays
                        .asList(Role.ROLE_CHARGEUR.getCode(), Role.ROLE_PRESTATAIRE.getCode())
                        .contains(connectedUser.getEbCompagnie().getCompagnieRole()))) {

                Integer compagnieNum = criterias.getEbCompagnieNum() != null ?
                    criterias.getEbCompagnieNum() :
                    connectedUser.getEbCompagnie().getEbCompagnieNum();
                Integer etablissementNum = criterias.getEbEtablissementNum() != null ?
                    criterias.getEbEtablissementNum() :
                    connectedUser.getEbEtablissement().getEbEtablissementNum();

                    if (connectedUser.isSuperAdmin()) {
                    where.and(qEbCompagnie.ebCompagnieNum.eq(compagnieNum));
                    }
                    else if (connectedUser.isAdmin()) {
                        where
                        .and(
                            qEbEtablissement.ebEtablissementNum
                                .eq(etablissementNum));
                    }
                

            }
            else if (criterias.getEbEtablissementNum()
                != null) where.and(qEbEtablissement.ebEtablissementNum.eq(criterias.getEbEtablissementNum()));

            if (criterias.isFromCommunity()) {
                where
                    .andAnyOf(
                        qEbUserGuest.ebUserNum.eq(connectedUser.getEbUserNum()),
                        qEbUserHost.ebUserNum.eq(connectedUser.getEbUserNum()));
                where.and(qEbUser.ebUserNum.ne(connectedUser.getEbUserNum()));
            }

            if (criterias.isRoleChargeur() && !criterias.isRoleControlTower()) where.and(qEbUser.role.eq(Enumeration.Role.ROLE_CHARGEUR.getCode()));

            if (criterias.isRoleChargeur() && criterias.isRoleControlTower()) {
                where.and(qEbUser.role.eq(Enumeration.Role.ROLE_CHARGEUR.getCode()))
                        .or(qEbUser.role.eq(Role.ROLE_CONTROL_TOWER.getCode()));
            }

            if (criterias.isRoleBroker()) {
                where
                    .andAnyOf(
                        qEbUser.service.eq(Enumeration.ServiceType.SERVICE_BROKER.getCode()),
                        qEbUser.service.eq(Enumeration.ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode()));
            }

            if (criterias.isRoleTransporteur()) {
                where
                    .andAnyOf(
                        qEbUser.service.eq(Enumeration.ServiceType.SERVICE_TRANSPORTEUR.getCode()),
                        qEbUser.service.eq(Enumeration.ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode()));
            }

            if (criterias.getListStatut() != null && !criterias.getListStatut().isEmpty()) {
                where.and(qEbUser.status.in(criterias.getListStatut()));
            }

            if (criterias.getListOfExistingTransporteur() != null &&
                !criterias.getListOfExistingTransporteur().isEmpty()) {
                where.and(qEbUser.ebUserNum.notIn(criterias.getListOfExistingTransporteur()));
            }

            if (criterias.getListGroupeNum() != null && !criterias.getListGroupeNum().isEmpty()) {
                BooleanBuilder boolGroups = new BooleanBuilder();

                for (Long ebUserGroupNum: criterias.getListGroupeNum()) {
                    BooleanBuilder orBoolGroups = new BooleanBuilder();
                    orBoolGroups.or(qEbUser.groupes.contains("|" + ebUserGroupNum + "|"));
                    if (orBoolGroups.hasValue()) boolGroups.or(orBoolGroups);
                }

                where.and(qEbUser.groupes.isNotNull().andAnyOf(boolGroups));
            }

            if (criterias.getOrderedColumn() != null) {

                if (criterias.getAscendant()) {
                    query.orderBy(this.orderMapASC.get(criterias.getOrderedColumn()));
                }
                else {
                    query.orderBy(this.orderMapDESC.get(criterias.getOrderedColumn()));
                }

            }
            else {
                query.orderBy(this.orderMapDESC.get("ebUserNum"));
            }

            ConstructorExpression<EbUser> selectUser = null;

            if (criterias.getConnectedControlTower() != null) {
                where
                    .andAnyOf(
                        qEbUserGuest.ebUserNum.eq(criterias.getConnectedControlTower()),
                        qEbUserHost.ebUserNum.eq(criterias.getConnectedControlTower()));
                where.and(qEbUser.ebUserNum.ne(criterias.getConnectedControlTower()));
            }

            if (criterias.isWithPassword()) {
                selectUser = QEbUser
                    .create(
                        qEbUser.ebUserNum,
                        qEbUser.nom,
                        qEbUser.prenom,
                        qEbUser.username,
                        qEbUser.email,
                        qEbUser.service,
                        qEbUser.status,
                        qEbUser.admin,
                        qEbUser.superAdmin,
                        qEbUser.role,
                        qEbUser.groupes,
                        qEbUser.password,
                        qEbEtablissement.ebEtablissementNum,
                        qEbEtablissement.nom,
                        qEbEtablissement.canShowPrice,
                        qEbCompagnie.ebCompagnieNum,
                        qEbCompagnie.nom,
                        qEbCompagnie.code,
                        qEbUserProfile.ebUserProfileNum,
                        qEbUserProfile.nom,
                        qEbUserProfile.descriptif,
                        qEbUser.telephone);
            }
            else {
                selectUser = QEbUser
                    .create(
                        qEbUser.ebUserNum,
                        qEbUser.nom,
                        qEbUser.prenom,
                        qEbUser.username,
                        qEbUser.email,
                        qEbUser.service,
                        qEbUser.status,
                        qEbUser.admin,
                        qEbUser.superAdmin,
                        qEbUser.role,
                        qEbUser.groupes,
                        qEbEtablissement.ebEtablissementNum,
                        qEbEtablissement.nom,
                        qEbEtablissement.canShowPrice,
                        qEbCompagnie.ebCompagnieNum,
                        qEbCompagnie.nom,
                        qEbCompagnie.code);
            }

            query.distinct().select(selectUser);
            query.from(qEbUser);
            query.leftJoin(qEbUser.ebEtablissement(), qEbEtablissement);
            query.leftJoin(qEbUser.ebCompagnie(), qEbCompagnie);
            query.leftJoin(qEbUser.ebUserProfile(), qEbUserProfile);

            if (criterias.isFromCommunity()) {
                query.leftJoin(qEbUser.contactsGuests, qRel).on(qRel.etat.eq(EtatRelation.ACTIF.getCode()));
                ;
                query.leftJoin(qEbUser.contactsHost, qRelHost).on(qRelHost.etat.eq(EtatRelation.ACTIF.getCode()));
                query.leftJoin(qRelHost.guest(), qEbUserGuest);
                query.leftJoin(qRel.host(), qEbUserHost);
            }

            if (criterias.isWithActiveUsers()) where.and(qEbUser.status.eq(UserStatus.ACTIF.getCode()));
            query.where(where);

            if (criterias.getPageNumber() != null && criterias.getSize() != null) {
                query
                    .limit(criterias.getSize())
                    .offset(criterias.getPageNumber() * (criterias.getSize() != null ? criterias.getSize() : 1));
            }

            resultat = query.fetch();

            if (criterias.getConnectedControlTower() != null) {
                JPAQuery<EbUser> queryTDC = new JPAQuery<EbUser>(em);
                BooleanBuilder whereDTC = new BooleanBuilder();
                ConstructorExpression<EbUser> selectUserMyTower = null;
                selectUserMyTower = QEbUser
                    .create(
                        qEbUser.ebUserNum,
                        qEbUser.nom,
                        qEbUser.prenom,
                        qEbUser.username,
                        qEbUser.email,
                        qEbUser.service,
                        qEbUser.status,
                        qEbUser.admin,
                        qEbUser.superAdmin,
                        qEbUser.role,
                        qEbUser.groupes,
                        qEbEtablissement.ebEtablissementNum,
                        qEbEtablissement.nom,
                        qEbEtablissement.canShowPrice,
                        qEbCompagnie.ebCompagnieNum,
                        qEbCompagnie.nom,
                        qEbCompagnie.code);

                queryTDC.distinct().select(selectUserMyTower);
                queryTDC.from(qEbUser);
                queryTDC.leftJoin(qEbUser.ebEtablissement(), qEbEtablissement);
                queryTDC.leftJoin(qEbUser.ebCompagnie(), qEbCompagnie);
                queryTDC.leftJoin(qEbUser.ebUserProfile(), qEbUserProfile);

                queryTDC.leftJoin(qEbUser.contactsGuests, qRel).on(qRel.etat.eq(EtatRelation.ACTIF.getCode()));
                ;
                queryTDC.leftJoin(qEbUser.contactsHost, qRelHost).on(qRelHost.etat.eq(EtatRelation.ACTIF.getCode()));
                queryTDC.leftJoin(qRelHost.guest(), qEbUserGuest);
                queryTDC.leftJoin(qRel.host(), qEbUserHost);

                whereDTC
                    .andAnyOf(
                        qEbUserGuest.ebUserNum.eq(criterias.getConnectedControlTower()),
                        qEbUserHost.ebUserNum.eq(criterias.getConnectedControlTower()));
                whereDTC.and(qEbUser.ebUserNum.ne(criterias.getConnectedControlTower()));

                if (criterias.getListOfExistingTransporteur() != null &&
                    !criterias.getListOfExistingTransporteur().isEmpty()) {
                    whereDTC.and(qEbUser.ebUserNum.notIn(criterias.getListOfExistingTransporteur()));
                }

                queryTDC.where(whereDTC);

                resultat.addAll(queryTDC.fetch());
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public List<EbUser> selectListPrestataireCommunity(SearchCriteria criterias) {
        BooleanBuilder where = new BooleanBuilder();

        int service = -1;

        if (criterias.isRoleTransporteur()) {
            service = Enumeration.ServiceType.SERVICE_TRANSPORTEUR.getCode();
        }
        else if (criterias.isRoleBroker()) {
            service = Enumeration.ServiceType.SERVICE_BROKER.getCode();
        }

        if (criterias.getEbUserNum() != null) {
            where
                .andAnyOf(
                    qEbRelation.host().ebUserNum
                        .eq(criterias.getEbUserNum()).and(
                            qEbRelation.guest().service
                                .in(service, Enumeration.ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode())),
                    qEbRelation.guest().ebUserNum
                        .eq(criterias.getEbUserNum()).and(
                            qEbRelation.host().service
                                .in(service, Enumeration.ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode())));
        }

        List<EbRelation> listRelation = (List<EbRelation>) ebRelationRepository.findAll(where);

        return listRelation.stream().map(relation -> {

            if (relation.getHost().getEbUserNum().equals(criterias.getEbUserNum())) {
                return relation.getGuest();
            }
            else {
                return relation.getHost();
            }

        }).collect(Collectors.toList());
    }

    @Override
    public List<EbUser> selectListEtablissementResquest(SearchCriteria criterias) {
        List<EbUser> resultat = new ArrayList();

        try {
            BooleanBuilder where = new BooleanBuilder();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    @Transactional
    public Boolean updatePasswordEbUser(EbUser ebUser) {
        Boolean updated = false;

        try {
            Integer result = em
                .createNativeQuery(
                    "UPDATE " + Statiques.SCHEMA + "eb_user SET password = :pwd WHERE eb_user_num = :idUser")
                .setParameter("pwd", ebUser.getPassword()).setParameter("idUser", ebUser.getEbUserNum())
                .executeUpdate();

            if (result != null && result.equals(1)) {
                updated = true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            updated = false;
            throw e;
        }

        return updated;
    }

    @Override
    public EbUser findUser(SearchCriteria criterias) {
        EbUser user = null;

        BooleanBuilder where = new BooleanBuilder();

        try {
            JPAQuery<EbUser> query = new JPAQuery<EbUser>(em);

            query
                .select(
                    QEbUser
                        .create(
                            qEbUser.ebUserNum,
                            qEbUser.nom,
                            qEbUser.prenom,
                            qEbUser.username,
                            qEbUser.email,
                            qEbUser.service,
                            qEbUser.status,
                            qEbUser.admin,
                            qEbUser.superAdmin,
                            qEbUser.role,
                            qEbUser.groupes,
                            qEbEtablissement.ebEtablissementNum,
                            qEbEtablissement.nom,
                            qEbEtablissement.canShowPrice,
                            qEbCompagnie.ebCompagnieNum,
                            qEbCompagnie.nom,
                            qEbCompagnie.code));

            where.and(qEbUser.ebUserNum.eq(criterias.getEbUserNum()));

            query.distinct();
            query.from(qEbUser).where(where);
            query.leftJoin(qEbUser.ebEtablissement(), qEbEtablissement);
            query.leftJoin(qEbUser.ebCompagnie(), qEbCompagnie);

            user = query.fetchFirst();

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return user;
    }

    private String
        getEmailSelectReq(Integer entityId, Integer mailId, Integer module, Boolean isProp, boolean ignoreMailIdCheck) {
        String sqlQuery = "";

        sqlQuery += " SELECT DISTINCT ON ( u.eb_user_num )  ";
        sqlQuery += " 	u.eb_user_num, ";
        sqlQuery += " 	u.nom, ";
        sqlQuery += " 	u.prenom, ";
        sqlQuery += " 	u.email, ";
        sqlQuery += " 	u.ROLE, ";
        sqlQuery += " 	u.language, ";
        sqlQuery += " 	et.eb_etablissement_num, ";
        sqlQuery += " 	et.nom AS \"etablissement_nom\", ";
        sqlQuery += " 	cmp.eb_compagnie_num, ";
        sqlQuery += " 	cmp.nom AS \"compagnie_nom\", ";
        sqlQuery += " 	cmp.code, ";
        sqlQuery += " 	u.list_params_mail, ";
        sqlQuery += " 	u.list_params_mail_str ";

        if (Enumeration.Module.PRICING.getCode().equals(module) ||
            Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(module) ||
            Enumeration.Module.FREIGHT_AUDIT.getCode().equals(module)) {
            sqlQuery += ", ";

            if (isProp) {
                sqlQuery += " 	CASE WHEN ( ";

                sqlQuery += " 		(u.role = :role_prestataire OR u.role = :role_ct) ";
                sqlQuery += " 		AND ( ";
                sqlQuery += "			q.x_transporteur = u.eb_user_num ";

                if (("" + EmailConfig.REPONSE_TRANSPORTEUR.getCode()).contains(mailId + "")) {
                    sqlQuery += " 		AND q.x_transporteur =  :iduser ";
                }

                sqlQuery += " 			OR ( ";
                sqlQuery += " 					d.x_eb_user_dest_customs_broker = u.eb_user_num ";
                sqlQuery += " 					OR d.x_eb_user_origin_customs_broker = u.eb_user_num ";
                sqlQuery += " 			) ";
                sqlQuery += "		) ";
                sqlQuery += "		OR u.role = :role_chargeur ";
                sqlQuery += "		AND  ( ";
                sqlQuery += "			d.x_eb_user = u.eb_user_num ";
                sqlQuery += " 			OR ( ";
                sqlQuery += " 				d.x_eb_user_dest = u.eb_user_num ";
                sqlQuery += " 				OR d.x_eb_user_origin = u.eb_user_num ";
                sqlQuery += " 			) ";
                sqlQuery += "		) ";

                // j'ai mis ce code en commentaire parce que il provoque un
                // probele de lenteur JIRA-1848

                /*
                 * if (
                 * Module.TRANSPORT_MANAGEMENT.getCode().equals(module) &&
                 * (EmailConfig.CHARGE_DOC_TM_TT.getCode() +
                 * "," + EmailConfig.SUPPRESSION_DOCUMENT.getCode()).contains(""
                 * + mailId)
                 * )
                 * {
                 * sqlQuery += "	OR fj.x_eb_user = u.eb_user_num ";
                 * }
                 */
                sqlQuery += " 	)	THEN TRUE ELSE FALSE END AS \"created_by_me\" ";
            }
            else {
                sqlQuery += " FALSE AS \"created_by_me\" ";
            }

        }
        else {
            sqlQuery += " 	NULL AS \"created_by_me\" ";
        }

        /**
         * ************************************** JOINTURES
         * *******************************************
         */
        sqlQuery += " FROM ";
        sqlQuery += " 	WORK.eb_user u ";
        sqlQuery += " 	LEFT JOIN WORK.eb_etablissement et ON ( u.x_eb_etablissement = et.eb_etablissement_num ) ";
        sqlQuery += " 	LEFT JOIN WORK.eb_compagnie cmp ON ( u.x_eb_compagnie = cmp.eb_compagnie_num ) ";

        if (Enumeration.Module.PRICING.getCode().equals(module) ||
            Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(module) ||
            Enumeration.Module.FREIGHT_AUDIT.getCode().equals(module)) {
            sqlQuery += " 	LEFT JOIN WORK.eb_demande_quote q ON ( ";

            if (isProp) {
                sqlQuery += "	( q.x_transporteur = u.eb_user_num )	";
            }
            else {
                sqlQuery += "	( ";
                sqlQuery += "		(u.super_admin = TRUE AND  q.x_eb_compagnie = u.x_eb_compagnie ) 	";
                sqlQuery += "		OR q.x_eb_etablissement = u.x_eb_etablissement	";
                sqlQuery += "	)	";
            }

            if (Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(module)) {
                sqlQuery += " AND (q.status >= :carrierfinalchoice OR u.role = :role_ct) ";
            }

            sqlQuery += "	) ";

            // j'ai mis ce code en commentaire parce que il provoque un probele
            // de lenteur JIRA-1848

            /*
             * if (
             * Module.TRANSPORT_MANAGEMENT.getCode().equals(module) &&
             * (EmailConfig.CHARGE_DOC_TM_TT.getCode() +
             * "," + EmailConfig.SUPPRESSION_DOCUMENT.getCode()).contains("" +
             * mailId)
             * )
             * {
             * sqlQuery +=
             * " 	LEFT JOIN WORK.eb_demande_fichiers_joint fj ON (  ";
             * if (isProp)
             * {
             * sqlQuery += "		fj.x_eb_user = u.eb_user_num ";
             * }
             * else
             * {
             * sqlQuery +=
             * "		(u.super_admin = TRUE AND fj.x_eb_compagnie = u.x_eb_compagnie OR fj.x_eb_etablissement = u.x_eb_etablissement ) "
             * ;
             * }
             * sqlQuery += " ) ";
             * }
             */

            sqlQuery += " 	LEFT JOIN WORK.eb_demande d ON ( ";

            if (isProp) {
                sqlQuery += "		(d.x_eb_user = u.eb_user_num) ";
                sqlQuery += "		OR (d.x_eb_user_ct = u.eb_user_num) ";
                sqlQuery += "		OR q.x_eb_demande = d.eb_demande_num ";
                sqlQuery += " 		OR ( ";
                sqlQuery += " 			d.x_eb_user_dest = u.eb_user_num "; // AND
                                                                        // u.eb_user_num
                                                                        // <>
                                                                        // d.eb_user_num
                sqlQuery += " 			OR d.x_eb_user_origin = u.eb_user_num "; // AND
                                                                             // u.eb_user_num
                                                                             // <>
                                                                             // d.eb_user_num
                sqlQuery += " 			OR d.x_eb_user_dest_customs_broker = u.eb_user_num ";
                sqlQuery += " 			OR d.x_eb_user_origin_customs_broker = u.eb_user_num ";
                sqlQuery += " 		) ";
            }
            else {
                sqlQuery += "		(u.super_admin = TRUE AND d.x_eb_compagnie = u.x_eb_compagnie OR d.x_eb_etablissement = u.x_eb_etablissement) ";
                // sqlQuery += " OR (u.super_admin = TRUE AND
                // d.x_eb_compagnie_ct = u.x_eb_compagnie OR
                // d.x_eb_etablissement_ct = u.x_eb_etablissement) ";
                sqlQuery += "		OR (u.super_admin = TRUE AND d.x_eb_compagnie_broker = u.x_eb_compagnie OR d.x_eb_etablissement_broker = u.x_eb_etablissement) ";
                sqlQuery += "		OR q.x_eb_demande = d.eb_demande_num ";

                // sqlQuery += " OR ( ";
                // sqlQuery += " d.x_eb_etab_dest = u.x_eb_etablissement "; //
                // AND u.x_eb_etablissement <> d.x_eb_etablissement
                // sqlQuery += " OR d.x_eb_etab_origin = u.x_eb_etablissement ";
                // // AND u.x_eb_etablissement <> d.x_eb_etablissement
                // sqlQuery += " OR d.x_eb_etab_dest_customs_broker =
                // u.x_eb_etablissement ";
                // sqlQuery += " OR d.x_eb_etab_origin_customs_broker =
                // u.x_eb_etablissement ";
                // sqlQuery += " ) ";
            }

            // j'ai mis ce code en commentaire parce que il provoque un probele
            // de lenteur JIRA-1848

            // if (Module.TRANSPORT_MANAGEMENT.getCode().equals(module) &&
            // (EmailConfig.CHARGE_DOC_TM_TT.getCode() + "," +
            // EmailConfig.SUPPRESSION_DOCUMENT.getCode()).contains("" +
            // mailId))
            // sqlQuery += " OR fj.x_eb_demande = d.eb_demande_num ";

            sqlQuery += ") ";
        }

        /**
         * ************************************** CONDITIONS (VISIBILITY)
         * **********************************
         */
        sqlQuery += " WHERE  ";

        if (Enumeration.Module.PRICING.getCode().equals(module) ||
            Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(module) ||
            Enumeration.Module.FREIGHT_AUDIT.getCode().equals(module)) {

					sqlQuery += " d.eb_demande_num = :entitynum ";
					if (Enumeration.EmailConfig.QUOTATION_REQUEST.getCode().equals(mailId))
					{
						sqlQuery += " AND u.x_eb_etablissement in (:idetabtr, :idetabCt)";
					}

            sqlQuery += " 	AND (  ";
            sqlQuery += " 		( ";

            sqlQuery += " 			u.role = :role_chargeur ";
            sqlQuery += " 			AND (";
            sqlQuery += " 				( u.super_admin = FALSE OR u.super_admin IS NULL ) ";
            sqlQuery += " 				AND (";
            sqlQuery += " 					(d.x_eb_etab_dest = u.x_eb_etablissement OR d.x_eb_etab_origin = u.x_eb_etablissement)"; // AND
                                                                                                                             // d.x_eb_etablissement
                                                                                                                             // <>
                                                                                                                             // u.x_eb_etablissement
            sqlQuery += " 					OR d.x_eb_etablissement = u.x_eb_etablissement ";
            sqlQuery += " 					AND (";
            sqlQuery += " 						u.admin = TRUE OR d.list_labels IS NULL	OR LENGTH (TRIM ( d.list_labels )) = 0 ";
            sqlQuery += " 						OR ( ";
            sqlQuery += "	 						(d.list_labels IS NOT NULL	OR LENGTH (TRIM ( d.list_labels )) > 0 )  ";
            sqlQuery += " 						 	AND (u.list_labels IS NOT NULL	OR LENGTH (TRIM ( u.list_labels )) > 0	)";
            sqlQuery += " 							AND (string_to_array( d.list_labels, ',' ) && string_to_array( u.list_labels, ',' ))";
            sqlQuery += " 						) ";
            sqlQuery += " 						OR u.list_categories IS NOT NULL ";
            sqlQuery += " 					) ";
            sqlQuery += " 				) ";
            sqlQuery += " 				OR (u.role = :role_chargeur and u.super_admin = TRUE) ";
            sqlQuery += " 			) ";
            sqlQuery += " 		) ";

            sqlQuery += " 		OR ( ";
            sqlQuery += " 			u.role = :role_prestataire ";

            if (Enumeration.EmailConfig.QUOTATION_REQUEST.getCode().equals(mailId) && isProp) {
                sqlQuery += " 			OR ( ";
            }
            else {
                sqlQuery += " 			AND ( ";
            }

            // sqlQuery += " (u.super_admin IS NOT NULL AND u.super_admin =
            // TRUE) OR (q.x_eb_etablissement = u.x_eb_etablissement)";
            sqlQuery += "				(q.x_eb_etablissement = u.x_eb_etablissement)";

            if ((Enumeration.EmailConfig.QUOTATION_FORWARDER_SELECTED.getCode() +
                "," + Enumeration.EmailConfig.REPONSE_TRANSPORTEUR.getCode()).contains((mailId + "").trim())) {

                if (isProp) sqlQuery += "  			AND ( q.x_transporteur = :iduser ) ";
                else {
                    sqlQuery += " 			AND ( ";
                    sqlQuery += " 				( (u.super_admin IS NULL OR u.super_admin = FALSE ) AND u.x_eb_etablissement = :idetabtr ) ";
                    sqlQuery += " 				OR ( u.super_admin = TRUE AND u.x_eb_compagnie = :idcomptr ) ";
                    sqlQuery += " 			) ";
                }

            }

            sqlQuery += " 				)";
            sqlQuery += " 			OR ( ";
            sqlQuery += " 				d.x_eb_etab_dest_customs_broker = u.x_eb_etablissement OR d.x_eb_etab_origin_customs_broker = u.x_eb_etablissement ";
            sqlQuery += " 			) AND d.x_ec_statut >= :status_dem_fin";
            sqlQuery += " 		) ";
            sqlQuery += " 		OR (u.role = :role_ct and u.super_admin = TRUE) ";
            sqlQuery += " 		OR (u.role = :role_ct and u.super_admin = FALSE and (q.x_eb_etablissement = u.x_eb_etablissement OR d.x_eb_etablissement_ct = u.x_eb_etablissement)) ";

            // j'ai mis ce code en commentaire parce que il provoque un probele
            // de lenteur JIRA-1848
            /*
             * if (
             * Module.TRANSPORT_MANAGEMENT.getCode().equals(module) &&
             * (EmailConfig.CHARGE_DOC_TM_TT.getCode() +
             * "," + EmailConfig.SUPPRESSION_DOCUMENT.getCode()).contains("" +
             * mailId)
             * )
             * {
             * sqlQuery += "	OR u.x_eb_compagnie = fj.x_eb_compagnie ";
             * }
             */

            sqlQuery += " 	) ";
        }

        /** ******* CONDITIONS sur le mail ******* */
        if (!ignoreMailIdCheck) {
            sqlQuery += " AND ( ";
            sqlQuery += "     (u.list_params_mail_str LIKE '%" + mailId + ":true:false%') ";
            sqlQuery += "     or (u.list_params_mail_str LIKE '%" + mailId + ":false:true%') ";
            sqlQuery += "     or (u.list_params_mail_str LIKE '%" + mailId + ":true:true%') ";
            sqlQuery += " ) ";
        }

        if (!isProp) {
            sqlQuery += " AND u.eb_user_num NOT IN ( SELECT eb_user_num FROM propusers )  ";
        }

        if (("" + Enumeration.EmailConfig.QUOTATION_FORWARDER_NOT_SELECTED.getCode()).contains(mailId + "")) {
            sqlQuery += "			AND u.role = :role_prestataire ";
            sqlQuery += "			AND (u.service = :transporteur OR u.service = :transpobroker) ";
            sqlQuery += " 			AND ( u.x_eb_compagnie <> :idcomptr ) ";
        }

        sqlQuery += " AND u.status = :useractive ";

        // on doit gerer les roles auquels est envoyé l'email ID. L'email #13
        // par exemple ne doit etre
        // envoyé qu'au chargeur alors que l'email #12 peut etre envoyé soit au
        // à la ct soit au
        // transporteur.
        // Sans ce test les chargeur risque de recevoir l'email 12 (envoyé
        // seulement au transporteur)
        // TODO: Il ne le risque jamais, vu que l'email 12 n'est dispo qu'aux
        // chargeurs et que l'email
        // 13 n'est dispo qu'aux transpo & ct
        String destinataireRoles = DaoEmailImpl.getEmailDestinataireRoleByMailId(mailId);

        if (StringUtils.isNotEmpty(destinataireRoles)) {
            sqlQuery += " and u.role in ( " + destinataireRoles + ") ";
        }

        return sqlQuery;
    }

    private String getEmailSelectReqTt(Integer mailId, Boolean isEvent, Boolean isProp, boolean ignoreMailIdCheck) {
        String sqlQuery = "";

        sqlQuery += " SELECT DISTINCT ON ( u.eb_user_num )  ";
        sqlQuery += " 	u.eb_user_num, ";
        sqlQuery += " 	u.nom, ";
        sqlQuery += " 	u.prenom, ";
        sqlQuery += " 	u.email, ";
        sqlQuery += " 	u.ROLE, ";
        sqlQuery += " 	u.language, ";
        sqlQuery += " 	et.eb_etablissement_num, ";
        sqlQuery += " 	et.nom AS \"etablissement_nom\", ";
        sqlQuery += " 	cmp.eb_compagnie_num, ";
        sqlQuery += " 	cmp.nom AS \"compagnie_nom\", ";
        sqlQuery += " 	cmp.code, ";
        sqlQuery += " 	u.list_params_mail, ";
        sqlQuery += " 	u.list_params_mail_str, ";

        if (isProp) {
            sqlQuery += " 	CASE WHEN ( ";

            sqlQuery += " 		(u.role = :role_ct AND (q.x_transporteur = u.eb_user_num OR trc.x_eb_user_ct = u.eb_user_num)) ";
            sqlQuery += "		OR (u.role = :role_chargeur AND trc.x_eb_chargeur = u.eb_user_num) ";
            sqlQuery += "		OR (u.role = :role_prestataire AND ( ";
            sqlQuery += "			trc.x_eb_transporteur = u.eb_user_num ";
            if (isEvent) sqlQuery += "			OR evt.x_eb_user = u.eb_user_num ";
            sqlQuery += "		)) ";

            sqlQuery += " 	)	THEN TRUE ELSE FALSE END AS \"created_by_me\" ";
        }
        else {
            sqlQuery += " FALSE AS \"created_by_me\" ";
        }

        /**
         * ************************************** JOINTURES
         * *******************************************
         */
        sqlQuery += " FROM ";
        sqlQuery += " 	WORK.eb_user u ";
        sqlQuery += " 	LEFT JOIN WORK.eb_etablissement et ON ( u.x_eb_etablissement = et.eb_etablissement_num ) ";
        sqlQuery += " 	LEFT JOIN WORK.eb_compagnie cmp ON ( u.x_eb_compagnie = cmp.eb_compagnie_num ) ";

        if (isEvent) {
            sqlQuery += " 	LEFT JOIN WORK.eb_tt_event evt ON ( ";
            sqlQuery += "	evt.eb_tt_event_num = :eventnum ";

            if (isProp) {
                sqlQuery += "	AND evt.x_eb_user = u.eb_user_num ";
            }
            else {
                sqlQuery += "	AND ((u.super_admin = TRUE AND evt.x_eb_compagnie = u.x_eb_compagnie) OR evt.x_eb_etablissement = u.x_eb_etablissement) ";
            }

            sqlQuery += "	) ";
        }

        sqlQuery += " 	LEFT JOIN WORK.eb_tt_tracing trc ON ( ";
        sqlQuery += "		trc.eb_tt_tracing_num = :tracingnum ";

        if (isProp) {
            sqlQuery += "	AND (";
            sqlQuery += "		(trc.x_eb_chargeur = u.eb_user_num) ";
            sqlQuery += "		OR (trc.x_eb_transporteur = u.eb_user_num) ";
            sqlQuery += "		OR (trc.x_eb_user_ct = u.eb_user_num) ";
            sqlQuery += "	)	";
        }
        else {
            sqlQuery += "	AND (";
            sqlQuery += "		(u.super_admin = TRUE AND trc.x_eb_company_chargeur = u.x_eb_compagnie OR trc.x_eb_etablissement_chargeur = u.x_eb_etablissement) ";
            sqlQuery += "		OR (u.super_admin = TRUE AND trc.x_eb_company_transporteur = u.x_eb_compagnie OR trc.x_eb_etablissement_transporteur = u.x_eb_etablissement) ";
            sqlQuery += "		OR (u.super_admin = TRUE AND trc.x_eb_compagnie_ct = u.x_eb_compagnie OR trc.x_eb_etablissement_ct = u.x_eb_etablissement) ";
            sqlQuery += "   OR( trc.x_eb_company_chargeur = u.x_eb_compagnie OR trc.x_eb_etablissement_chargeur = u.x_eb_etablissement) ";
            sqlQuery += "	) ";
        }

        sqlQuery += "	) ";

        sqlQuery += " 	LEFT JOIN WORK.eb_demande_quote q ON ( ";
        sqlQuery += "		q.x_eb_demande = :entitynum ";

        if (isProp) {
            sqlQuery += "	AND u.eb_user_num = q.x_transporteur ";
        }
        else {
            sqlQuery += "	AND ( (u.super_admin = TRUE AND  q.x_eb_compagnie = u.x_eb_compagnie ) OR ( q.x_eb_etablissement = u.x_eb_etablissement) )	";
        }

        sqlQuery += "	) ";

        /**
         * ************************************** CONDITIONS (VISIBILITY)
         * **********************************
         */
        sqlQuery += " WHERE  ";
        sqlQuery += " 	(  ";
        sqlQuery += " 		trc.eb_tt_tracing_num IS NOT NULL ";
        sqlQuery += " 		OR q.ex_eb_demande_transporteur_num IS NOT NULL   ";
        if (isEvent) sqlQuery += " 		OR evt.eb_tt_event_num IS NOT NULL  ";
        sqlQuery += " 	)  ";
        sqlQuery += " 	AND (  ";
        sqlQuery += " 		( ";
        sqlQuery += " 			u.role = :role_chargeur ";
        sqlQuery += " 			AND ( u.super_admin = FALSE and trc.x_eb_etablissement_chargeur = u.x_eb_etablissement ) ";
        sqlQuery += " 			AND ( ";
        sqlQuery += " 				u.admin IS NOT NULL OR u.admin = FALSE OR trc.list_labels IS NULL	OR LENGTH (TRIM ( trc.list_labels )) = 0";
        sqlQuery += " 				OR ( ";
        sqlQuery += "	 			 	(trc.list_labels IS NOT NULL	OR LENGTH (TRIM ( trc.list_labels )) > 0 )  ";
        sqlQuery += " 				 	AND (u.list_labels IS NOT NULL	OR LENGTH (TRIM ( u.list_labels )) > 0	)";
        sqlQuery += " 				 	AND (string_to_array( trc.list_labels, ',' ) && string_to_array( u.list_labels, ',' ))";
        sqlQuery += " 				) ";
        sqlQuery += " 			) OR (u.role = :role_chargeur and u.super_admin = TRUE)";
        sqlQuery += " 		) ";

        sqlQuery += " 		OR ( ";
        sqlQuery += " 			(u.role = :role_prestataire AND (u.service = :transporteur OR u.service = :transpobroker)) ";

        /*
         * if(isEvent) { if(isProp) sqlQuery +=
         * "  		AND	( evt.x_eb_user = u.eb_user_num ) "; else { sqlQuery +=
         * " 		AND ( "; sqlQuery +=
         * " 				( u.super_admin = TRUE AND u.x_eb_compagnie = evt.x_eb_compagnie) "
         * ; sqlQuery +=
         * " 				OR ( u.x_eb_etablissement = evt.x_eb_etablissement ) ";
         * sqlQuery += " 			) "; } } else
         */ {

            if (isProp) {
                sqlQuery += " 	AND ( ";
                sqlQuery += "		(trc.x_eb_chargeur = u.eb_user_num) ";
                sqlQuery += "		OR (trc.x_eb_transporteur = u.eb_user_num) ";
                sqlQuery += "		OR (trc.x_eb_user_ct = u.eb_user_num) ";
                sqlQuery += " 	) ";
            }
            else {
                sqlQuery += "	AND (";
                sqlQuery += "		(u.super_admin = TRUE AND trc.x_eb_company_chargeur = u.x_eb_compagnie OR trc.x_eb_etablissement_chargeur = u.x_eb_etablissement) ";
                sqlQuery += "		OR (u.super_admin = TRUE AND trc.x_eb_company_transporteur = u.x_eb_compagnie OR trc.x_eb_etablissement_transporteur = u.x_eb_etablissement) ";
                sqlQuery += "		OR (u.super_admin = TRUE AND trc.x_eb_compagnie_ct = u.x_eb_compagnie OR trc.x_eb_etablissement_ct = u.x_eb_etablissement) ";
                sqlQuery += "	) ";
            }

        }

        sqlQuery += " 		) ";
        sqlQuery += " 		OR (u.role = :role_ct AND ( u.super_admin = TRUE OR (q.x_eb_etablissement = u.x_eb_etablissement OR trc.x_eb_etablissement_ct = u.x_eb_etablissement))) ";
        sqlQuery += " 	) ";

        /** ******* CONDITIONS sur le mail ******* */
        if (!ignoreMailIdCheck) {
            sqlQuery += " AND ( ";
            sqlQuery += "     (u.list_params_mail_str LIKE '%" + mailId + ":true:false%') ";
            sqlQuery += "     or (u.list_params_mail_str LIKE '%" + mailId + ":false:true%') ";
            sqlQuery += "     or (u.list_params_mail_str LIKE '%" + mailId + ":true:true%') ";
            sqlQuery += " ) ";
        }

        sqlQuery += " AND u.status = :useractive ";

        // on doit gerer les roles auquels est envoyé l'email ID. L'email #13
        // par exemple ne doit etre
        // envoyé qu'au chargeur alors que l'email #12 peut etre envoyé soit au
        // à la ct soit au
        // transporteur.
        // Sans ce test les chargeur risque de recevoir l'email 12 (envoyé
        // seulement au transporteur)
        // TODO: Il ne le risque jamais, vu que l'email 12 n'est dispo qu'aux
        // chargeurs et que l'email
        // 13 n'est dispo qu'aux transpo & ct
        String destinataireRoles = DaoEmailImpl.getEmailDestinataireRoleByMailId(mailId);

        if (StringUtils.isNotEmpty(destinataireRoles)) {
            sqlQuery += " and u.role in ( " + destinataireRoles + ") ";
        }

        return sqlQuery;
    }

    @Override
    public List<EbUser> listDestinataireByEmailId(
        Integer entityId,
        Integer mailId,
        Integer module,
        EbUser transporteur,
        Integer ebTtEventNum,
        Integer ebTtTracingNum,
        Integer ebTtEventUserNum,
        boolean ignoreMailParamCheck) {

			EbUser connectedUser = connectedUserService.getCurrentUser();

        Query query = null;

        String sqlQuery = "";

        List<EbUser> res = new ArrayList<EbUser>();

        try {
            /**
             * *********************************** SELECT
             * **************************
             */
            // selection des utilisateurs concernés directement (proprietaires)
            sqlQuery += " WITH propusers AS ( ";

            if (Enumeration.Module.TRACK
                .getCode().equals(
                    module)) sqlQuery += getEmailSelectReqTt(mailId, ebTtEventNum != null, true, ignoreMailParamCheck);
            else sqlQuery += getEmailSelectReq(entityId, mailId, module, true, ignoreMailParamCheck);

            sqlQuery += " ), collusers AS ( ";

            if (Enumeration.Module.TRACK
                .getCode().equals(
                    module)) sqlQuery += getEmailSelectReqTt(mailId, ebTtEventNum != null, false, ignoreMailParamCheck);
            else sqlQuery += getEmailSelectReq(entityId, mailId, module, false, ignoreMailParamCheck);

            sqlQuery += " 		AND u.eb_user_num NOT IN ( SELECT eb_user_num FROM propusers )  ";

            sqlQuery += " ), lastquery AS ( ";
            sqlQuery += " 		( SELECT * FROM propusers ) UNION ( SELECT * FROM collusers ) ";
            sqlQuery += " ) ";

            sqlQuery += " SELECT * FROM lastquery WHERE 1 = 1 ";

            // eliminer l'user qui a créé l'evenement dans le cas du T&T
            if (ebTtEventUserNum != null) {
                sqlQuery += " AND eb_user_num <> :eventuser ";
            }

            if (mailId.equals(Enumeration.EmailConfig.NOUVEAU_COMMENTAIRE.getCode()) &&
                Enumeration.Module.PRICING.getCode().equals(module)) {
                sqlQuery += " OR role = " + (Enumeration.Role.ROLE_PRESTATAIRE.getCode());
            }

            if ((EmailConfig.PRICING_DATA_CHANGED.getCode() +
                "," + EmailConfig.QUOTATION_RENSEIGNEMENT_INFORMATIONS_TRANSPORT.getCode()).contains(mailId + "")) {
                sqlQuery += " AND eb_user_num <> :iduser ";
            }

            query = em.createNativeQuery(sqlQuery, "UserMailDemandeMapping");

            if (sqlQuery.contains(":entitynum")) query.setParameter("entitynum", entityId);
            if (sqlQuery.contains(":mailid")) query.setParameter("mailid", mailId);
            if (sqlQuery
                .contains(
                    ":role_chargeur")) query.setParameter("role_chargeur", Enumeration.Role.ROLE_CHARGEUR.getCode());
            if (sqlQuery.contains(":role_prestataire")) query
                .setParameter("role_prestataire", Enumeration.Role.ROLE_PRESTATAIRE.getCode());
            if (sqlQuery
                .contains(":role_ct")) query.setParameter("role_ct", Enumeration.Role.ROLE_CONTROL_TOWER.getCode());

            if (sqlQuery.contains(":useractive")) {
                query.setParameter("useractive", Enumeration.UserStatus.ACTIF.getCode());
            }

            if (sqlQuery.contains(":transporteur")) query
                .setParameter("transporteur", Enumeration.ServiceType.SERVICE_TRANSPORTEUR.getCode());
            if (sqlQuery.contains(":transpobroker")) query
                .setParameter("transpobroker", Enumeration.ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());

						if (
							sqlQuery.contains(":idetabtr") &&
								transporteur != null && transporteur.getEbEtablissement() != null &&
								transporteur.getEbEtablissement().getEbEtablissementNum() != null
						)
							query
								.setParameter(
									"idetabtr",
									transporteur.getEbEtablissement().getEbEtablissementNum()
								);

						if (
							sqlQuery.contains(":idetabCt") &&
								connectedUser.getEbEtablissement() != null &&
								connectedUser.getEbEtablissement().getEbEtablissementNum() != null
						)
							query
								.setParameter(
									"idetabCt",
									connectedUser.getEbEtablissement().getEbEtablissementNum()
								);

            if (sqlQuery.contains(":idcomptr") &&
                transporteur.getEbCompagnie() != null && transporteur.getEbCompagnie().getEbCompagnieNum()
                    != null) query.setParameter("idcomptr", transporteur.getEbCompagnie().getEbCompagnieNum());
            if (sqlQuery.contains(":iduser")) query.setParameter("iduser", transporteur.getEbUserNum());

            if (ebTtEventNum != null) {
                if (sqlQuery.contains(":eventnum")) query.setParameter("eventnum", ebTtEventNum);
                if (sqlQuery.contains(":eventuser")) query.setParameter("eventuser", ebTtEventUserNum);
            }

            if (sqlQuery.contains(":tracingnum")) query.setParameter("tracingnum", ebTtTracingNum);
            if (sqlQuery.contains(":carrierfinalchoice")) query
                .setParameter("carrierfinalchoice", Enumeration.StatutCarrier.FIN.getCode());

            if (sqlQuery
                .contains(
                    ":status_dem_fin")) query.setParameter("status_dem_fin", Enumeration.StatutDemande.FIN.getCode());

            String req = getResultQuery(query, sqlQuery);

            LOGGER.info("*** {}", req);

            res = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return res;
    }

    @Override
    public long selectCountListUser(SearchCriteria criterias) {
        List<EbUser> resultat = new ArrayList<EbUser>();
        EbUser connectedUser = connectedUserService.getCurrentUser();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbUser> query = new JPAQuery<EbUser>(em);

            if (criterias.getEmailNomPrenom() != null) {
                where
                    .andAnyOf(
                        qEbUser.nom.contains(criterias.getEmailNomPrenom()),
                        qEbUser.prenom.contains(criterias.getEmailNomPrenom()),
                        qEbUser.email.contains(criterias.getEmailNomPrenom()));
            }

            if (criterias.getSearchterm() != null) {

                if (connectedUser.isControlTower()) {
                    BooleanBuilder userWhere = new BooleanBuilder();
                    userWhere
                        .andAnyOf(
                            qEbUser.nom.toLowerCase().contains(criterias.getSearchterm().toLowerCase()),
                            qEbUser.prenom.toLowerCase().contains(criterias.getSearchterm().toLowerCase()),
                            qEbUser.email.toLowerCase().contains(criterias.getSearchterm().toLowerCase()));

                    if (criterias.isSimpleSearch()) where.and(userWhere);
                    else {
                        where
                            .andAnyOf(
                                userWhere,
                                qEbUser.ebEtablissement().nom
                                    .toLowerCase().contains(criterias.getSearchterm().toLowerCase()),
                                qEbUser.ebCompagnie().nom
                                    .toLowerCase().contains(criterias.getSearchterm().toLowerCase()));
                    }

                }
                else {
                    where
                        .andAnyOf(
                            qEbUser.nom.contains(criterias.getSearchterm()),
                            qEbUser.prenom.contains(criterias.getSearchterm()));
                }

            }

            if (criterias.getListStatut() != null && !criterias.getListStatut().isEmpty()) {
                where.and(qEbUser.status.in(criterias.getListStatut()));
            }

            if (connectedUser.isChargeur() ||
                connectedUser.isPrestataire() ||
                (connectedUser.isControlTower() &&
                    Arrays
                        .asList(Role.ROLE_CHARGEUR.getCode(), Role.ROLE_PRESTATAIRE.getCode())
                        .contains(connectedUser.getEbCompagnie().getCompagnieRole()))) {

                if (connectedUser.isSuperAdmin()) {
                    where.and(qEbCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
                }
                else if (connectedUser.isAdmin()) {
                    where
                        .and(
                            qEbEtablissement.ebEtablissementNum
                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));
                }

            }
            else {
                if (criterias.getEbEtablissementNum() != null) where
                    .and(qEbUser.ebEtablissement().ebEtablissementNum.eq(criterias.getEbEtablissementNum()));

                if (criterias.getEbCompagnieNum()
                    != null) where.and(qEbUser.ebCompagnie().ebCompagnieNum.eq(criterias.getEbCompagnieNum()));
            }

            if (criterias.getListGroupeNum() != null && !criterias.getListGroupeNum().isEmpty()) {
                BooleanBuilder boolGroups = new BooleanBuilder();

                for (Long ebUserGroupNum: criterias.getListGroupeNum()) {
                    BooleanBuilder orBoolGroups = new BooleanBuilder();
                    orBoolGroups.or(qEbUser.groupes.contains("|" + ebUserGroupNum + "|"));
                    if (orBoolGroups.hasValue()) boolGroups.or(orBoolGroups);
                }

                where.and(qEbUser.groupes.isNotNull().andAnyOf(boolGroups));
            }

            ConstructorExpression<EbUser> selectUser = null;
            selectUser = QEbUser
                .create(
                    qEbUser.ebUserNum,
                    qEbUser.nom,
                    qEbUser.prenom,
                    qEbUser.username,
                    qEbUser.email,
                    qEbUser.service,
                    qEbUser.status,
                    qEbUser.admin,
                    qEbUser.superAdmin,
                    qEbUser.role,
                    qEbUser.groupes,
                    qEbEtablissement.ebEtablissementNum,
                    qEbEtablissement.nom,
                    qEbEtablissement.canShowPrice,
                    qEbCompagnie.ebCompagnieNum,
                    qEbCompagnie.nom,
                    qEbCompagnie.code);

            query.distinct().select(selectUser);
            query.from(qEbUser);
            query.where(where);
            query.leftJoin(qEbUser.ebEtablissement(), qEbEtablissement);
            query.leftJoin(qEbUser.ebCompagnie(), qEbCompagnie);

            resultat = query.fetch();

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat.size();
    }
}
