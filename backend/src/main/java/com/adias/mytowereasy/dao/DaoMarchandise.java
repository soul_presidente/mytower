package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbMarchandise;


public interface DaoMarchandise {
    List<EbMarchandise> getAutoCompleteList(String term, String field);

    Integer nextValEbMarchandise();
}
