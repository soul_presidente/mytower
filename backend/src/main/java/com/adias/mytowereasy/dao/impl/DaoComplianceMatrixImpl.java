/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.ConstructorExpression;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoComplianceMatrix;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbCombinaisonRepository;
import com.adias.mytowereasy.repository.EbDestinationCountryRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoComplianceMatrixImpl extends Dao implements DaoComplianceMatrix {
    @PersistenceContext
    EntityManager em;

    @Autowired
    private ConnectedUserService connectedUserService;

    @Autowired
    private EbCombinaisonRepository ebCombinaisonRepository;

    @Autowired
    private EbDestinationCountryRepository destinationCountryRepository;

    QEbDestinationCountry qEbDestinationCountry = QEbDestinationCountry.ebDestinationCountry;
    QEbCombinaison qEbCombinaison = QEbCombinaison.ebCombinaison;
    QEbUser qEbUser = QEbUser.ebUser;
    QEcCountry qEcCountry = QEcCountry.ecCountry;
    QEbCompagnie qEbCompagnie = QEbCompagnie.ebCompagnie;
    QEbEtablissement qEbEtablissement = QEbEtablissement.ebEtablissement;

    @Override
    public List<EbDestinationCountry> selectListEbDestinationCountry(SearchCriteria criterias) {
        List<EbDestinationCountry> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPQLQuery query = new JPAQuery(em);

            if (criterias.getEbEtablissementNum() != null) {
                where
                    .and(
                        qEbDestinationCountry.etablissement().ebEtablissementNum.eq(criterias.getEbEtablissementNum()));
            }

            if (criterias.getPageNumber() != null && criterias.getSize() != null) {
                query.offset(criterias.getPageNumber() * criterias.getSize()).limit(criterias.getSize());
            }

            query
                .select(
                    qEbDestinationCountry
                        .create(
                            qEbDestinationCountry.ebDestinationCountryNum,
                            qEbDestinationCountry.country(),
                            qEbDestinationCountry.etablissement()));
            query.from(qEbDestinationCountry).where(where);
            query.orderBy(qEbDestinationCountry.country().libelle.asc());

            resultat = (List<EbDestinationCountry>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public EbDestinationCountry selectEbDestinationCountry(SearchCriteria criterias) {
        EbDestinationCountry resultat = null;

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPQLQuery query = new JPAQuery(em);
            ConstructorExpression<EbDestinationCountry> select = null;

            if (criterias.getEbDestinationCountryNum() != null) {
                where.and(qEbDestinationCountry.ebDestinationCountryNum.eq(criterias.getEbDestinationCountryNum()));
            }

            select = qEbDestinationCountry
                .create(
                    qEbDestinationCountry.ebDestinationCountryNum,
                    qEbDestinationCountry.generalComments,
                    qEbDestinationCountry.processLeadTimes,
                    qEbDestinationCountry.preferredPartners,
                    qEbDestinationCountry.documentationRequired,
                    qEbDestinationCountry.lastDateModified,
                    qEbDestinationCountry.lastUserModified().nom,
                    qEbDestinationCountry.createdBy().nom,
                    qEbDestinationCountry.dateAjout,
                    qEbDestinationCountry.country().libelle);

            query.select(select);
            query.from(qEbDestinationCountry).where(where);
            query.leftJoin(qEbDestinationCountry.lastUserModified());
            resultat = (EbDestinationCountry) query.fetchOne();

            resultat
                .setCombinaisons(
                    ebCombinaisonRepository
                        .findByDestinationCountry_ebDestinationCountryNum(resultat.getEbDestinationCountryNum()));
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public List<EbCombinaison> selectListEbCombinaison(SearchCriteria criterias) {
        BooleanBuilder where = new BooleanBuilder();
        EbUser connectedUser = connectedUserService.getCurrentUser();

        if (criterias.getEbEtablissementNum() == null
            && !connectedUser.getRole().equals(Enumeration.Role.ROLE_CONTROL_TOWER.getCode())) criterias
                .setEbEtablissementNum(connectedUser.getEbEtablissement().getEbEtablissementNum());

        if (criterias.getEbEtablissementNum() != null) {
            where
                .and(
                    qEbCombinaison.destinationCountry().etablissement().ebEtablissementNum
                        .eq(criterias.getEbEtablissementNum()));
        }

        if (criterias.getDateDeb() != null) {
            where.and(qEbCombinaison.destinationCountry().dateAjout.after(criterias.getDateDeb()));
        }

        if (criterias.getDateFin() != null) {
            where.and(qEbCombinaison.destinationCountry().dateAjout.before(criterias.getDateFin()));
        }

        if (criterias.getEbDestinationCountryNum() != null) {
            where
                .and(
                    qEbCombinaison.destinationCountry().ebDestinationCountryNum
                        .eq(criterias.getEbDestinationCountryNum()));
        }

        if (criterias.getHsCode() != null) {
            where.and(qEbCombinaison.hsCode.contains(criterias.getHsCode()));
        }

        if (criterias.getEcCountryNum() != null) {
            where.and(qEbCombinaison.origin().ecCountryNum.eq(criterias.getEcCountryNum()));
        }

        return (List<EbCombinaison>) ebCombinaisonRepository.findAll(where);
    }

    @Override
    public List<EbDestinationCountry> selectListEbDestinationCountryByCompany(SearchCriteria criterias) {
        List<EbDestinationCountry> results = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPQLQuery query = new JPAQuery(em);

            if (criterias.getEbCompagnieNum() != null) {
                where.and(qEbCompagnie.ebCompagnieNum.eq(criterias.getEbCompagnieNum()));
            }

            query
                .select(
                    qEbDestinationCountry
                        .create(
                            qEbDestinationCountry.ebDestinationCountryNum,
                            qEbDestinationCountry.country(),
                            qEbDestinationCountry.etablissement()));

            query.from(qEbDestinationCountry);

            query.leftJoin(qEbDestinationCountry.etablissement(), qEbEtablissement);
            query.leftJoin(qEbEtablissement.ebCompagnie(), qEbCompagnie);

            query.where(where);
            query.orderBy(qEbDestinationCountry.country().libelle.asc());

            results = (List<EbDestinationCountry>) query.fetch();
            return results;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }
}
