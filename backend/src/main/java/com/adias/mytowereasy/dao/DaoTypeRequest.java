package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbTypeRequest;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoTypeRequest {
    public List<EbTypeRequest> searchEbTypeRequest(SearchCriteria criteria);

    public Long countListEbTypeRequest(SearchCriteria criteria);

    public List<EbTypeRequest> searchTypeRequestByReference(String reference);
}
