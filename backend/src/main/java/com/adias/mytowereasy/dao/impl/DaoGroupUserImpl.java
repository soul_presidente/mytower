package com.adias.mytowereasy.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoGroupUser;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EbUserGroup;
import com.adias.mytowereasy.model.QEbUserGroup;
import com.adias.mytowereasy.repository.EbGroupUserRepository;
import com.adias.mytowereasy.service.UserService;


@Component
@Transactional
public class DaoGroupUserImpl implements DaoGroupUser {
    @Autowired
    EbGroupUserRepository ebGroupUserRepository;

    @PersistenceContext
    EntityManager em;

    @Autowired
    UserService userService;

    QEbUserGroup qEbUserGroup = QEbUserGroup.ebUserGroup;

    @Override
    public List<EbUserGroup> getListUserGroup(EbUser ebUser) {
        BooleanBuilder where = new BooleanBuilder();
        List<EbUserGroup> listUserGroups = null;

        try {
            JPAQuery<EbUserGroup> query = new JPAQuery<EbUserGroup>(em);

            if (ebUser.getEbCompagnie() != null && !ebUser.isControlTower()) where
                .and(qEbUserGroup.ebCompagnie().ebCompagnieNum.eq(ebUser.getEbCompagnie().getEbCompagnieNum()));

            query.from(qEbUserGroup);
            query.leftJoin(qEbUserGroup.ebCompagnie());
            query.where(where);
            listUserGroups = query.from(qEbUserGroup).fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return listUserGroups;
    }
}
