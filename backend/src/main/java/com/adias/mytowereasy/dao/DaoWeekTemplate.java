package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.chanel.model.EbWeekTemplate;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoWeekTemplate {
    public List<EbWeekTemplate> list(SearchCriteria criteria);

    public Long listCount(SearchCriteria criteria);

    public List<EbWeekTemplate> getListEbWeekTemplateWithChauvauchementDate(EbWeekTemplate ebWeekTemplate);
}
