package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.ExpressionUtils;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.airbus.model.QEbTtCompanyPsl;
import com.adias.mytowereasy.cronjob.tt.model.EdiMapPsl;
import com.adias.mytowereasy.cronjob.tt.model.QEdiMapPsl;
import com.adias.mytowereasy.dao.DaoMappingCodePsl;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.tt.QEbTtCategorieDeviation;
import com.adias.mytowereasy.utils.search.SearchCriteriaMappingCodePsl;


@Component
@Transactional
public class DaoMappingCodePslImp implements DaoMappingCodePsl {
    @PersistenceContext
    EntityManager em;

    private Map<String, StringPath> autoCompleteFields = new HashMap<String, StringPath>();

    QEdiMapPsl qEdiMapPsl = QEdiMapPsl.ediMapPsl;
    Path<String> stringOrderAlias = ExpressionUtils.path(String.class, "stringOrderExpression");
    QEbCompagnie qEbCompagnie = QEbCompagnie.ebCompagnie;
    QEbTtCompanyPsl qEbTtCompanyPsl = QEbTtCompanyPsl.ebTtCompanyPsl;
    QEbTtCategorieDeviation qEbTtCategorieDeviation = QEbTtCategorieDeviation.ebTtCategorieDeviation;

    public DaoMappingCodePslImp() {
        this.initialize();
    }

    private void initialize() {
        autoCompleteFields.put("codePslTransporteur", qEdiMapPsl.codePslTransporteur);
        autoCompleteFields.put("codePslNormalise", qEdiMapPsl.codePslNormalise);
        autoCompleteFields.put("codeRaison", qEdiMapPsl.codeRaison);
        autoCompleteFields.put("categorieDeviationLibelle", qEdiMapPsl.categorieDeviationLibelle);
        autoCompleteFields.put("codePslTransporteurDescriptif", qEdiMapPsl.codePslTransporteurDescriptif);
        autoCompleteFields.put("codeRaisonDescriptif", qEdiMapPsl.codeRaisonDescriptif);
    }

    @Override
    public List<EdiMapPsl> getListMappingCodePsl(SearchCriteriaMappingCodePsl criteria) {
        List<EdiMapPsl> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EdiMapPsl> query = new JPAQuery<>(em);

            query
                .select(
                    QEdiMapPsl
                        .create(
                            qEdiMapPsl.ediMapPslNum,
                            qEdiMapPsl.xEbCompany(),
                            qEdiMapPsl.xEbCompanyPsl(),
                            qEdiMapPsl.xEbCompanyTransporteur(),
                            qEdiMapPsl.xEbCategorieDeviation(),
                            qEdiMapPsl.codePslTransporteur,
                            qEdiMapPsl.codePslNormalise,
                            qEdiMapPsl.typeEvent,
                            qEdiMapPsl.numCodeRaison,
                            qEdiMapPsl.codeRaison,
                            qEdiMapPsl.categorieDeviationLibelle,
                            qEdiMapPsl.numCodeTransporteur,
                            qEdiMapPsl.codePslTransporteurDescriptif,
                            qEdiMapPsl.codeRaisonDescriptif,
                            qEdiMapPsl.dateCreationSys,
                            qEdiMapPsl.dateMajSys));
            query = getCodePslGlobalWhere(query, where, criteria);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEdiMapPsl, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEdiMapPsl.codePslNormalise.asc());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public Long getListMappingCodePslCount(SearchCriteriaMappingCodePsl criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EdiMapPsl> query = new JPAQuery<EdiMapPsl>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getCodePslGlobalWhere(query, where, criteria);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private JPAQuery
        getCodePslGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteriaMappingCodePsl criteria) {

        if (criteria.getListCodePslTransporteur() != null && !criteria.getListCodePslTransporteur().isEmpty()) {
            where.and(qEdiMapPsl.codePslTransporteur.in(criteria.getListCodePslTransporteur()));
        }

        if (criteria.getListCodePslNormalise() != null && !criteria.getListCodePslNormalise().isEmpty()) {
            where.and(qEdiMapPsl.codePslNormalise.in(criteria.getListCodePslNormalise()));
        }

        if (criteria.getListTypeEvent() != null && !criteria.getListTypeEvent().isEmpty()) {
            where.and(qEdiMapPsl.typeEvent.in(criteria.getListTypeEvent()));
        }

        if (criteria.getListCodeRaison() != null && !criteria.getListCodeRaison().isEmpty()) {
            where.and(qEdiMapPsl.codeRaison.in(criteria.getListCodeRaison()));
        }

        if (criteria.getListCategorieDeviationLibelle() != null
            && !criteria.getListCategorieDeviationLibelle().isEmpty()) {
            where.and(qEdiMapPsl.categorieDeviationLibelle.in(criteria.getListCategorieDeviationLibelle()));
        }

        if (criteria.getListCodePslTransporteurDescriptif() != null
            && !criteria.getListCodePslTransporteurDescriptif().isEmpty()) {
            where.and(qEdiMapPsl.codePslTransporteurDescriptif.in(criteria.getListCodePslTransporteurDescriptif()));
        }

        if (criteria.getNumCodeTransporteur() != null) {
            where.and((qEdiMapPsl.numCodeTransporteur).eq(criteria.getNumCodeTransporteur()));
        }

        if (criteria.getNumCodeRaison() != null) {
            where.and((qEdiMapPsl.numCodeRaison).eq(criteria.getNumCodeRaison()));
        }

        if (criteria.getListCodeRaisonDescriptif() != null && !criteria.getListCodeRaisonDescriptif().isEmpty()) {
            where.and(qEdiMapPsl.codeRaisonDescriptif.in(criteria.getListCodeRaisonDescriptif()));
        }

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();

            where
                .andAnyOf(
                    qEdiMapPsl.codePslTransporteur.toLowerCase().contains(term),
                    qEdiMapPsl.codePslNormalise.toLowerCase().contains(term),
                    qEdiMapPsl.typeEvent.stringValue().toLowerCase().contains(term),
                    qEdiMapPsl.codeRaison.toLowerCase().contains(term),
                    qEdiMapPsl.categorieDeviationLibelle.toLowerCase().contains(term),
                    qEdiMapPsl.numCodeTransporteur.stringValue().contains(term),
                    qEdiMapPsl.codePslTransporteurDescriptif.toLowerCase().contains(term),
                    qEdiMapPsl.codeRaisonDescriptif.toLowerCase().contains(term),
                    qEdiMapPsl.numCodeRaison.stringValue().contains(term));
        }

        query.from(qEdiMapPsl);
        query.where(where);
        query.leftJoin(qEdiMapPsl.xEbCompany(), qEbCompagnie);
        query.leftJoin(qEdiMapPsl.xEbCompanyTransporteur(), qEbCompagnie);
        query.leftJoin(qEdiMapPsl.xEbCompanyPsl(), qEbTtCompanyPsl);
        query.leftJoin(qEdiMapPsl.xEbCategorieDeviation(), qEbTtCategorieDeviation);

        return query;
    }

    @Override
    public List<EdiMapPsl> getAutoCompleteList(String term, String field) {
        List<String> result = null;
        List<EdiMapPsl> listResult = new ArrayList<>();

        try {
            SearchCriteriaMappingCodePsl criteria = new SearchCriteriaMappingCodePsl();
            // criteria = criteria.setProperty(field, term, criteria);

            // autoCompleteFields est une map contenant les path queryDSL vers
            // les attributs de l'entité EdiMapPSL
            // le nom (l'identifiant) du champ surlequel nous faisons de
            // l'autocompletion est envoyé à partir du front.
            // on utilise cet identifiant pour aller cherchr directement le
            // stringPath de l'attribut correspondant à l'autocomplete
            // nous selectionnons, filtrons pui trions sur cet attribut
            StringPath autoCompleteWhereField = autoCompleteFields.get(field);

            JPAQuery query = new JPAQuery<>(em);
            query.select(autoCompleteWhereField);
            query.from(qEdiMapPsl);

            query.where(autoCompleteWhereField.toLowerCase().contains(term.toLowerCase()));
            // query.leftJoin(qEdiMapPsl.xEbCompagnie(), qEbCompagnie);
            criteria.Initialize(qEdiMapPsl);
            OrderSpecifier<?> orederField = criteria.getOrderMapASC().get(field);

            if (orederField != null) {
                query.orderBy(orederField);
            }

            query.distinct();
            query.limit(500);
            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (String value: result) {
            EdiMapPsl liv = new EdiMapPsl();
            liv.setProperty(field, value);
            listResult.add(liv);
        }

        return listResult;
    }
}
