/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl.custom;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.custom.DaoDeclaration;
import com.adias.mytowereasy.dao.impl.Dao;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.custom.EbCustomDeclaration;
import com.adias.mytowereasy.model.custom.QEbCustomDeclaration;
import com.adias.mytowereasy.model.custom.QEbFormField;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteriaCustom;


@Component
@Transactional
public class DaoDeclarationImpl extends Dao implements DaoDeclaration {

    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbCustomDeclaration qEbDeclaration = new QEbCustomDeclaration("ebCustomDeclaration");
    QEbFormField qEbFormField = new QEbFormField("ebFormField");
    QEcCountry qxEcCountryProvenances = new QEcCountry("qxEcCountryProvenances");
    QEcCountry qxEcCountryDedouanement = new QEcCountry("qxEcCountryDedouanement");
    QEbCompagnie qCompagnie = new QEbCompagnie("qCompagnie");
    QEbUser qDeclarant = new QEbUser("qDeclarant");
    QEbUser qDemandeur = new QEbUser("qDemandeur");
    QEbUser qAgentBroker = new QEbUser("qAgentBroker");
    QEbUser qCharger = new QEbUser("qCharger");
    QEbUser qCarrier = new QEbUser("qCarrier");
    QEcCurrency qfreightPartCurrency = new QEcCurrency("qfreightPartCurrency");
    QEcCurrency qInsuranceCostCurrency = new QEcCurrency("qInsuranceCostCurrency");
    QEcCurrency qInvoiceCurrency = new QEcCurrency("qInvoiceCurrency");
    QEcCurrency qToolingCostCurrency = new QEcCurrency("qToolingCostCurrency");
    QEcCurrency qTotalFreightCostCurrency = new QEcCurrency("qTotalFreightCostCurrency");
    QEcCurrency qValueCurrency = new QEcCurrency("qValueCurrency");
    QEbEtablissement qEtablissementCharger = new QEbEtablissement("qEtablissementCharger");
    QEbEtablissement qEtablissementCarrier = new QEbEtablissement("qEtablissementCarrier");
    QEbEtablissement qEtablissementBroker = new QEbEtablissement("qEtablissementBroker");
    QEbDemande qDemande = new QEbDemande("qDemande");

    private static Map<String, Object> orderHashMap = new HashMap<>();

    private void Initialiaze() {
        orderHashMap.put("ebCustomDeclarationNum", qEbDeclaration.ebCustomDeclarationNum);
        orderHashMap.put("refDemandeDedouanement", qEbDeclaration.refDemandeDedouanement);
        orderHashMap.put("idDedouanement", qEbDeclaration.idDedouanement);
        orderHashMap.put("demandeDate", qEbDeclaration.demandeDate);
        orderHashMap.put("demandeur", qDemandeur.nom);
        orderHashMap.put("operation", qEbDeclaration.operation);
        orderHashMap.put("projet", qEbDeclaration.projet);
        orderHashMap.put("codeBAU", qEbDeclaration.codeBAU);
        orderHashMap.put("eori", qEbDeclaration.eori);
        orderHashMap.put("typeDeFlux", qEbDeclaration.typeDeFlux);
        orderHashMap.put("sitePlant", qEbDeclaration.sitePlant);
        orderHashMap.put("destinataire", qEbDeclaration.destinataire);
        orderHashMap.put("villeIncoterm", qEbDeclaration.villeIncoterm);
        orderHashMap.put("nomFournisseur", qEbDeclaration.nomFournisseur);
        orderHashMap.put("xEcModeTransport", qEbDeclaration.xEcModeTransport);
        orderHashMap.put("immatriculation", qEbDeclaration.immatriculation);
        orderHashMap.put("dateArrivalEsstimed", qEbDeclaration.dateArrivalEsstimed);
        orderHashMap.put("airportPortOfArrival", qEbDeclaration.airportPortOfArrival);
        orderHashMap.put("xEcCountryProvenances", qxEcCountryProvenances.libelle);
        orderHashMap.put("agentBroker", qAgentBroker.nom);
        orderHashMap.put("xEcCountryDedouanement", qxEcCountryDedouanement.libelle);
        orderHashMap.put("lieuDedouanement", qEbDeclaration.lieuDedouanement);
        orderHashMap.put("biensDoubleUsage", qEbDeclaration.biensDoubleUsage);
        orderHashMap.put("licenceDoubleUsage", qEbDeclaration.licenceDoubleUsage);
        orderHashMap.put("transit", qEbDeclaration.transit);
        orderHashMap.put("bureauDeTransit", qEbDeclaration.bureauDeTransit);
        orderHashMap.put("bureauDeTransit", qEbDeclaration.warGoods);
        orderHashMap.put("warGoodLicenceReference", qEbDeclaration.warGoodLicenceReference);
        orderHashMap.put("customOffice", qEbDeclaration.customOffice);
        orderHashMap.put("nomTransporteur", qEbDeclaration.nomTransporteur);
        orderHashMap.put("telephoneTransporteur", qEbDeclaration.telephoneTransporteur);
        orderHashMap.put("nomContactTransporteur", qEbDeclaration.nomContactTransporteur);
        orderHashMap.put("emailTransporteur", qEbDeclaration.emailTransporteur);
        orderHashMap.put("freightPart", qEbDeclaration.freightPart);
        orderHashMap.put("freightPartCurrency", qfreightPartCurrency.code);
        orderHashMap.put("insuranceCost", qEbDeclaration.insuranceCost);
        orderHashMap.put("insuranceCostCurrency", qInsuranceCostCurrency.code);
        orderHashMap.put("invoiceAmount", qEbDeclaration.invoiceAmount);
        orderHashMap.put("invoiceCurrency", qInvoiceCurrency.code);
        orderHashMap.put("toolingCost", qEbDeclaration.toolingCost);
        orderHashMap.put("toolingCostCurrency", qToolingCostCurrency.code);
        orderHashMap.put("totalFreightCost", qEbDeclaration.totalFreightCost);
        orderHashMap.put("totalFreightCostCurrency", qTotalFreightCostCurrency.code);
        orderHashMap.put("numTva", qEbDeclaration.numTva);
        orderHashMap.put("traficAtBorder", qEbDeclaration.traficAtBorder);
        orderHashMap.put("importerOfRecord", qEbDeclaration.importerOfRecord);
        orderHashMap.put("delay", qEbDeclaration.delay);
        orderHashMap.put("comment", qEbDeclaration.comment);
        orderHashMap.put("grossWeight", qEbDeclaration.grossWeight);
        orderHashMap.put("grossWeightUnit", qEbDeclaration.grossWeightUnit);
        orderHashMap.put("numberOfPack", qEbDeclaration.numberOfPack);
        orderHashMap.put("numberOfLines", qEbDeclaration.numberOfLines);
        orderHashMap.put("totalQuantity", qEbDeclaration.totalQuantity);
        orderHashMap.put("valueAmountTotal", qEbDeclaration.valueAmountTotal);
        orderHashMap.put("business", qEbDeclaration.business);
        orderHashMap.put("hsCode", qEbDeclaration.hsCode);
        orderHashMap.put("partNumber", qEbDeclaration.partNumber);
        orderHashMap.put("procedure", qEbDeclaration.procedure);
        orderHashMap.put("quantity", qEbDeclaration.quantity);
        orderHashMap.put("valueAmount", qEbDeclaration.valueAmount);
        orderHashMap.put("valueCurrency", qValueCurrency.code);
        orderHashMap.put("exchangeRate", qEbDeclaration.exchangeRate);
        orderHashMap.put("dateUpload", qEbDeclaration.dateUpload);
        orderHashMap.put("typeDoc", qEbDeclaration.typeDoc);
        orderHashMap.put("numberOrReference1", qEbDeclaration.numberOrReference1);
        orderHashMap.put("numberOrReference2", qEbDeclaration.numberOrReference2);
        orderHashMap.put("attroneyText", qEbDeclaration.attroneyText);
        orderHashMap.put("cancellationReason", qEbDeclaration.cancellationReason);
        orderHashMap.put("dateCancellation", qEbDeclaration.dateCancellation);
        orderHashMap.put("customRegime", qEbDeclaration.customRegime);
        orderHashMap.put("etablissementCarrier", qEtablissementCarrier.nom);
        orderHashMap.put("etablissementbroker", qEtablissementBroker.nom);
        orderHashMap.put("etablissementCharger", qEtablissementCharger.nom);
        orderHashMap.put("etablissementCharger", qCharger.nom);
        orderHashMap.put("etablissementCharger", qCarrier.nom);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EbCustomDeclaration> getListEbDeclaration(SearchCriteriaCustom criteria) {
        List<EbCustomDeclaration> resultat = new ArrayList<EbCustomDeclaration>();
        Initialiaze();

        try {
            JPAQuery<EbCustomDeclaration> query = new JPAQuery<EbCustomDeclaration>(em);

            query
                .select(
                    QEbCustomDeclaration
                        .create(
                            qEbDeclaration.ebCustomDeclarationNum,
                            qEbDeclaration.declarationReference,
                            qEbDeclaration.refDemandeDedouanement,
                            qEbDeclaration.idDedouanement,
                            qEbDeclaration.demandeDate,
                            qDemandeur.ebUserNum,
                            qDemandeur.nom,
                            qDemandeur.prenom,
                            qEbDeclaration.operation,
                            qEbDeclaration.projet,
                            qEbDeclaration.codeBAU,
                            qEbDeclaration.eori,
                            qEbDeclaration.typeDeFlux,
                            qEbDeclaration.sitePlant,
                            qEbDeclaration.destinataire,
                            qEbDeclaration.villeIncoterm,
                            qEbDeclaration.nomFournisseur,
                            qEbDeclaration.xEcModeTransport,
                            qEbDeclaration.immatriculation,
                            qEbDeclaration.dateArrivalEsstimed,
                            qEbDeclaration.airportPortOfArrival,
                            qxEcCountryProvenances.ecCountryNum,
                            qxEcCountryProvenances.libelle,
                            qAgentBroker.ebUserNum,
                            qAgentBroker.nom,
                            qxEcCountryDedouanement.ecCountryNum,
                            qxEcCountryDedouanement.libelle,
                            qEbDeclaration.lieuDedouanement,
                            qEbDeclaration.biensDoubleUsage,
                            qEbDeclaration.licenceDoubleUsage,
                            qEbDeclaration.transit,
                            qEbDeclaration.bureauDeTransit,
                            qEbDeclaration.warGoods,
                            qEbDeclaration.warGoodLicenceReference,
                            qEbDeclaration.customOffice,
                            qEbDeclaration.nomTransporteur,
                            qEbDeclaration.telephoneTransporteur,
                            qEbDeclaration.nomContactTransporteur,
                            qEbDeclaration.emailTransporteur,
                            qEbDeclaration.freightPart,
                            qfreightPartCurrency.ecCurrencyNum,
                            qfreightPartCurrency.code,
                            qEbDeclaration.insuranceCost,
                            qInsuranceCostCurrency.ecCurrencyNum,
                            qInsuranceCostCurrency.code,
                            qEbDeclaration.invoiceAmount,
                            qInvoiceCurrency.ecCurrencyNum,
                            qInvoiceCurrency.code,
                            qEbDeclaration.toolingCost,
                            qToolingCostCurrency.ecCurrencyNum,
                            qToolingCostCurrency.code,
                            qEbDeclaration.totalFreightCost,
                            qTotalFreightCostCurrency.ecCurrencyNum,
                            qTotalFreightCostCurrency.code,
                            qEbDeclaration.numTva,
                            qEbDeclaration.traficAtBorder,
                            qEbDeclaration.importerOfRecord,
                            qEbDeclaration.delay,
                            qEbDeclaration.comment,
                            qEbDeclaration.grossWeight,
                            qEbDeclaration.grossWeightUnit,
                            qEbDeclaration.numberOfPack,
                            qEbDeclaration.numberOfLines,
                            qEbDeclaration.totalQuantity,
                            qEbDeclaration.valueAmountTotal,
                            qEbDeclaration.business,
                            qEbDeclaration.hsCode,
                            qEbDeclaration.partNumber,
                            qEbDeclaration.procedure,
                            qEbDeclaration.quantity,
                            qEbDeclaration.valueAmount,
                            qValueCurrency.ecCurrencyNum,
                            qValueCurrency.code,
                            qEbDeclaration.exchangeRate,
                            qEbDeclaration.dateUpload,
                            qEbDeclaration.typeDoc,
                            qEbDeclaration.numberOrReference1,
                            qEbDeclaration.numberOrReference2,
                            qEbDeclaration.attroneyText,
                            qEbDeclaration.cancellationReason,
                            qEbDeclaration.dateCancellation,
                            qEbDeclaration.customRegime,
                            qCharger.ebUserNum,
                            qCharger.nom,
                            qCharger.prenom,
                            qEtablissementCharger.ebEtablissementNum,
                            qEtablissementCharger.nom,
                            qCarrier.ebUserNum,
                            qCarrier.nom,
                            qCarrier.prenom,
                            qEtablissementCarrier.ebEtablissementNum,
                            qEtablissementCarrier.nom,
                            qEtablissementBroker.ebEtablissementNum,
                            qEtablissementBroker.nom,
                            qEbDeclaration.listUnit,
                            qEbDeclaration.leg,
                            qEbDeclaration.nbOfTemporaryRegime,
                            qEbDeclaration.dateEcs,
                            qEbDeclaration.dateDeclaration,
                            qEbDeclaration.statut,
                            qEbDeclaration.declarationNumber,
                            qDeclarant.ebUserNum,
                            qDeclarant.nom,
                            qDeclarant.prenom,
                            qEbDeclaration.attachedFilesCount,
                            qDemande.ebDemandeNum,
                            qDemande.refTransport,
                            qEbDeclaration.dateCreationSys));

            query = getGlobalWhere(query, criteria);
            query = getGlobalJoin(query, criteria);

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbDeclaration, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbDeclaration.ebCustomDeclarationNum.desc());
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    private JPAQuery getGlobalJoin(JPAQuery query, SearchCriteriaCustom criteria) {
        query.leftJoin(qEbDeclaration.demandeur(), qDemandeur);
        query.leftJoin(qEbDeclaration.xEbUserDeclarant(), qDeclarant);
        query.leftJoin(qEbDeclaration.carrier(), qCarrier);
        query.leftJoin(qEbDeclaration.charger(), qCharger);
        query.leftJoin(qEbDeclaration.agentBroker(), qAgentBroker);
        query.leftJoin(qEbDeclaration.xEcCountryProvenances(), qxEcCountryProvenances);
        query.leftJoin(qEbDeclaration.xEcCountryDedouanement(), qxEcCountryDedouanement);
        query.leftJoin(qEbDeclaration.freightPartCurrency(), qfreightPartCurrency);
        query.leftJoin(qEbDeclaration.insuranceCostCurrency(), qInsuranceCostCurrency);
        query.leftJoin(qEbDeclaration.invoiceCurrency(), qInvoiceCurrency);
        query.leftJoin(qEbDeclaration.toolingCostCurrency(), qToolingCostCurrency);
        query.leftJoin(qEbDeclaration.totalFreightCostCurrency(), qTotalFreightCostCurrency);
        query.leftJoin(qEbDeclaration.valueCurrency(), qValueCurrency);
        query.leftJoin(qEbDeclaration.etablissementCharger(), qEtablissementCharger);
        query.leftJoin(qEbDeclaration.etablissementCarrier(), qEtablissementCarrier);
        query.leftJoin(qEbDeclaration.etablissementbroker(), qEtablissementBroker);
        query.leftJoin(qEbDeclaration.ebCompagnie(), qCompagnie);
        query.leftJoin(qEbDeclaration.xEbDemande(), qDemande);

        return query;
    }

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    private JPAQuery getGlobalWhere(JPAQuery query, SearchCriteriaCustom criteria) {
        BooleanBuilder where = new BooleanBuilder();
        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);

        // Filter by company
        where.and(qCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));

        if (criteria != null) {

            if (criteria.getListAgentBroker() != null && !criteria.getListAgentBroker().isEmpty()) {
                where = where.and(qAgentBroker.ebUserNum.in(criteria.getListAgentBroker()));
            }

            if (criteria.getListClearanceCountry() != null && !criteria.getListClearanceCountry().isEmpty()) {
                BooleanBuilder b = new BooleanBuilder();
                criteria.getListClearanceCountry().stream().forEach(it -> {
                    b.or(qxEcCountryDedouanement.ecCountryNum.eq(it));
                });
                where.and(b);
            }

            if (criteria.getEbCustomDeclarationNum() != null) {
                where.and(qEbDeclaration.ebCustomDeclarationNum.eq(criteria.getEbCustomDeclarationNum()));
            }

            if (criteria.getListclearanceId() != null && !criteria.getListclearanceId().isEmpty()) {
                where.and(qEbDeclaration.ebCustomDeclarationNum.in(criteria.getListclearanceId()));
            }

            if (criteria.getListvilleIncoterm() != null && !criteria.getListvilleIncoterm().isEmpty()) {
                where.and(qEbDeclaration.ebCustomDeclarationNum.in(criteria.getListvilleIncoterm()));
            }

            if (criteria.getListShipper() != null && !criteria.getListShipper().isEmpty()) {
                where = where.and(qCharger.ebUserNum.in(criteria.getListShipper()));
            }

            if (criteria.getRequestDateFrom() != null) {
                where = where
                    .andAnyOf(
                        qEbDeclaration.demandeDate.eq(criteria.getRequestDateFrom()),
                        qEbDeclaration.demandeDate.after(criteria.getRequestDateFrom()));
            }

            if (criteria.getRequestDateTo() != null) {
                where = where
                    .andAnyOf(
                        qEbDeclaration.demandeDate.eq(criteria.getRequestDateTo()),
                        qEbDeclaration.demandeDate.before(criteria.getRequestDateTo()));
            }

            if (StringUtils.isNotBlank(criteria.getSearchterm())) {
                String value = criteria.getSearchterm();
                String searchTerm = criteria.getSearchterm().toLowerCase();
                Date dt = null;
                String valueModeTr = "";

                if (value != null && !value.isEmpty()) {
                    value = value.trim().toLowerCase();

                    if (Pattern.compile("([0-9]{2}/){2}[0-9]{4}").matcher(value).matches()) {
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                        try {
                            dt = formatter.parse(value);
                        } catch (Exception e) {
                        }

                    }

                    if (value.contains("(")) {

                        try {
                            valueModeTr = value.replace(")", "");
                            String[] vs = valueModeTr.split("\\(");
                            valueModeTr = vs[0].trim();
                        } catch (Exception e) {
                        }

                    }
                    else {
                        valueModeTr = value.toString();
                    }

                    List<Integer> listCodeModeTransport = Enumeration.ModeTransport.getListCodeByLibelle(valueModeTr);

                    if (listCodeModeTransport == null
                        || listCodeModeTransport.isEmpty()) listCodeModeTransport = new ArrayList<Integer>();

                    BooleanBuilder w = new BooleanBuilder();
                    w // @formatter:off
						.andAnyOf(
							qEbDeclaration.refDemandeDedouanement.lower().contains(searchTerm),
							qEbDeclaration.declarationReference.lower().contains(searchTerm),
							qDemandeur.nom.lower().contains(searchTerm), 
							qDemandeur.prenom.lower().contains(searchTerm),
							qEbDeclaration.operation.stringValue().lower().contains(searchTerm),
							qEbDeclaration.projet.lower().contains(searchTerm),
							qEbDeclaration.codeBAU.lower().contains(searchTerm),
							qEbDeclaration.eori.lower().contains(searchTerm),
							qEbDeclaration.typeDeFlux.lower().contains(searchTerm),
							qEbDeclaration.sitePlant.lower().contains(searchTerm),
							qEbDeclaration.destinataire.lower().contains(searchTerm),
							qEbDeclaration.villeIncoterm.lower().contains(searchTerm),
							qEbDeclaration.nomFournisseur.lower().contains(searchTerm),
							qEbDeclaration.immatriculation.lower().contains(searchTerm),
							qEbDeclaration.airportPortOfArrival.lower().contains(searchTerm),
							qxEcCountryProvenances.libelle.lower().contains(searchTerm),
							qAgentBroker.nom.lower().contains(searchTerm),
							qxEcCountryDedouanement.libelle.lower().contains(searchTerm),
							qEbDeclaration.lieuDedouanement.lower().contains(searchTerm),
							qEbDeclaration.biensDoubleUsage.lower().contains(searchTerm),
							qEbDeclaration.licenceDoubleUsage.lower().contains(searchTerm),
							qEbDeclaration.transit.lower().contains(searchTerm), 
							qEbDeclaration.bureauDeTransit.lower().contains(searchTerm),
							qEbDeclaration.warGoods.lower().contains(searchTerm),
							qEbDeclaration.warGoodLicenceReference.lower().contains(searchTerm),
							qEbDeclaration.customOffice.lower().contains(searchTerm), 
							qEbDeclaration.nomTransporteur.lower().contains(searchTerm),
							qEbDeclaration.telephoneTransporteur.lower().contains(searchTerm),
							qEbDeclaration.nomContactTransporteur.lower().contains(searchTerm),
							qEbDeclaration.emailTransporteur.lower().contains(searchTerm),
							qEbDeclaration.freightPart.stringValue().lower().contains(searchTerm), 
							qfreightPartCurrency.code.lower().contains(searchTerm), 
							qEbDeclaration.insuranceCost.stringValue().lower().contains(searchTerm),
							qInsuranceCostCurrency.code.lower().contains(searchTerm),
							qEbDeclaration.invoiceAmount.stringValue().lower().contains(searchTerm),
							qInvoiceCurrency.code.lower().contains(searchTerm),
							qEbDeclaration.toolingCost.stringValue().lower().contains(searchTerm),
							qToolingCostCurrency.code.lower().contains(searchTerm), 
							qEbDeclaration.totalFreightCost.stringValue().lower().contains(searchTerm), 
							qTotalFreightCostCurrency.code.lower().contains(searchTerm), 
							qEbDeclaration.numTva.stringValue().lower().contains(searchTerm), 
							qEbDeclaration.traficAtBorder.lower().contains(searchTerm),
							qEbDeclaration.importerOfRecord.lower().contains(searchTerm), 
							qEbDeclaration.delay.lower().contains(searchTerm), 
							qEbDeclaration.comment.lower().contains(searchTerm), 
							qEbDeclaration.grossWeight.stringValue().lower().contains(searchTerm), 
							qEbDeclaration.grossWeightUnit.lower().contains(searchTerm), 
							qEbDeclaration.numberOfPack.stringValue().lower().contains(searchTerm), 
							qEbDeclaration.numberOfLines.stringValue().lower().contains(searchTerm), 
							qEbDeclaration.totalQuantity.stringValue().lower().contains(searchTerm), 
							qEbDeclaration.valueAmountTotal.stringValue().lower().contains(searchTerm),
							qEbDeclaration.business.lower().contains(searchTerm), 
							qEbDeclaration.hsCode.lower().contains(searchTerm),
							qEbDeclaration.partNumber.lower().contains(searchTerm),
							qEbDeclaration.procedure.lower().contains(searchTerm), 
							qEbDeclaration.quantity.lower().contains(searchTerm), 
							qEbDeclaration.valueAmount.stringValue().lower().contains(searchTerm),
							qValueCurrency.code.lower().contains(searchTerm), 
							qEbDeclaration.exchangeRate.stringValue().lower().contains(searchTerm), 
							qEbDeclaration.typeDoc.lower().contains(searchTerm), 
							qEbDeclaration.numberOrReference1.lower().contains(searchTerm), 
							qEbDeclaration.numberOrReference2.lower().contains(searchTerm), 
							qEbDeclaration.attroneyText.lower().contains(searchTerm), 
							qEbDeclaration.cancellationReason.lower().contains(searchTerm),
							qEbDeclaration.customRegime.lower().contains(searchTerm),
							qEbDeclaration.dateCancellation.stringValue().lower().contains(searchTerm),
							qEbDeclaration.demandeDate.stringValue().lower().contains(searchTerm), 
							qEbDeclaration.dateArrivalEsstimed.stringValue().lower().contains(searchTerm),
							qEbDeclaration.dateUpload.stringValue().lower().contains(searchTerm), 
							qDeclarant.nom.lower().contains(searchTerm), 
							qDeclarant.prenom.lower().contains(searchTerm), 
							qEbDeclaration.leg.lower().contains(searchTerm), 
							qEbDeclaration.customRegime.lower().contains(searchTerm), 
							qEbDeclaration.declarationNumber.lower().contains(searchTerm), 
							qEbDeclaration.nbOfTemporaryRegime.stringValue().lower().contains(searchTerm)
							);
				// @formatter:on
                    if (dt != null) {
                        w
                            .or(
                                qEbDeclaration.demandeDate
                                    .stringValue().lower()
                                    .contains((new SimpleDateFormat(Statiques.DATE_FORMAT_ORDER)).format(dt)));

                        w
                            .or(
                                qEbDeclaration.dateArrivalEsstimed
                                    .stringValue().lower()
                                    .contains((new SimpleDateFormat(Statiques.DATE_FORMAT_ORDER)).format(dt)));

                        w
                            .or(
                                qEbDeclaration.dateUpload
                                    .stringValue().lower()
                                    .contains((new SimpleDateFormat(Statiques.DATE_FORMAT_ORDER)).format(dt)));

                        w
                            .or(
                                qEbDeclaration.dateCancellation
                                    .stringValue().lower()
                                    .contains((new SimpleDateFormat(Statiques.DATE_FORMAT_ORDER)).format(dt)));

                        w
                            .or(
                                qEbDeclaration.dateDeclaration
                                    .stringValue().lower()
                                    .contains((new SimpleDateFormat(Statiques.DATE_FORMAT_ORDER)).format(dt)));

                        w
                            .or(
                                qEbDeclaration.dateEcs
                                    .stringValue().lower()
                                    .contains((new SimpleDateFormat(Statiques.DATE_FORMAT_ORDER)).format(dt)));
                    }

                    if (!listCodeModeTransport.isEmpty()) {
                        w.or(qEbDeclaration.xEcModeTransport.in(listCodeModeTransport));
                    }

                    where.and(w);
                }

            }

        }

        query.distinct();
        query.from(qEbDeclaration).where(where);

        return query;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getCountListEbDeclaration(SearchCriteriaCustom criteria) {
        Long count = new Long(0);

        try {
            JPAQuery<Long> query = new JPAQuery<Long>(em);

            query = getGlobalWhere(query, criteria);
            query = getGlobalJoin(query, criteria);

            count = (Long) query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return count;
    }

    @SuppressWarnings("unchecked")
    @Override
    public EbCustomDeclaration getEbDeclaration(SearchCriteriaCustom criteria) {
        EbCustomDeclaration resultat = null;
        Initialiaze();
        BooleanBuilder where = new BooleanBuilder();

        try {
            JPAQuery<EbCustomDeclaration> query = new JPAQuery<EbCustomDeclaration>(em);

            query
                .select(
                    QEbCustomDeclaration
                        .create(
                            qEbDeclaration.ebCustomDeclarationNum,
                            qEbDeclaration.declarationReference,
                            qEbDeclaration.refDemandeDedouanement,
                            qEbDeclaration.idDedouanement,
                            qEbDeclaration.demandeDate,
                            qDemandeur.ebUserNum,
                            qDemandeur.nom,
                            qDemandeur.prenom,
                            qEbDeclaration.operation,
                            qEbDeclaration.projet,
                            qEbDeclaration.codeBAU,
                            qEbDeclaration.eori,
                            qEbDeclaration.typeDeFlux,
                            qEbDeclaration.sitePlant,
                            qEbDeclaration.destinataire,
                            qEbDeclaration.villeIncoterm,
                            qEbDeclaration.nomFournisseur,
                            qEbDeclaration.xEcModeTransport,
                            qEbDeclaration.immatriculation,
                            qEbDeclaration.dateArrivalEsstimed,
                            qEbDeclaration.airportPortOfArrival,
                            qxEcCountryProvenances.ecCountryNum,
                            qxEcCountryProvenances.libelle,
                            qAgentBroker.ebUserNum,
                            qAgentBroker.nom,
                            qxEcCountryDedouanement.ecCountryNum,
                            qxEcCountryDedouanement.libelle,
                            qEbDeclaration.lieuDedouanement,
                            qEbDeclaration.biensDoubleUsage,
                            qEbDeclaration.licenceDoubleUsage,
                            qEbDeclaration.transit,
                            qEbDeclaration.bureauDeTransit,
                            qEbDeclaration.warGoods,
                            qEbDeclaration.warGoodLicenceReference,
                            qEbDeclaration.customOffice,
                            qEbDeclaration.nomTransporteur,
                            qEbDeclaration.telephoneTransporteur,
                            qEbDeclaration.nomContactTransporteur,
                            qEbDeclaration.emailTransporteur,
                            qEbDeclaration.freightPart,
                            qfreightPartCurrency.ecCurrencyNum,
                            qfreightPartCurrency.code,
                            qEbDeclaration.insuranceCost,
                            qInsuranceCostCurrency.ecCurrencyNum,
                            qInsuranceCostCurrency.code,
                            qEbDeclaration.invoiceAmount,
                            qInvoiceCurrency.ecCurrencyNum,
                            qInvoiceCurrency.code,
                            qEbDeclaration.toolingCost,
                            qToolingCostCurrency.ecCurrencyNum,
                            qToolingCostCurrency.code,
                            qEbDeclaration.totalFreightCost,
                            qTotalFreightCostCurrency.ecCurrencyNum,
                            qTotalFreightCostCurrency.code,
                            qEbDeclaration.numTva,
                            qEbDeclaration.traficAtBorder,
                            qEbDeclaration.importerOfRecord,
                            qEbDeclaration.delay,
                            qEbDeclaration.comment,
                            qEbDeclaration.grossWeight,
                            qEbDeclaration.grossWeightUnit,
                            qEbDeclaration.numberOfPack,
                            qEbDeclaration.numberOfLines,
                            qEbDeclaration.totalQuantity,
                            qEbDeclaration.valueAmountTotal,
                            qEbDeclaration.business,
                            qEbDeclaration.hsCode,
                            qEbDeclaration.partNumber,
                            qEbDeclaration.procedure,
                            qEbDeclaration.quantity,
                            qEbDeclaration.valueAmount,
                            qValueCurrency.ecCurrencyNum,
                            qValueCurrency.code,
                            qEbDeclaration.exchangeRate,
                            qEbDeclaration.dateUpload,
                            qEbDeclaration.typeDoc,
                            qEbDeclaration.numberOrReference1,
                            qEbDeclaration.numberOrReference2,
                            qEbDeclaration.attroneyText,
                            qEbDeclaration.cancellationReason,
                            qEbDeclaration.dateCancellation,
                            qEbDeclaration.customRegime,
                            qCharger.ebUserNum,
                            qCharger.nom,
                            qCharger.prenom,
                            qEtablissementCharger.ebEtablissementNum,
                            qEtablissementCharger.nom,
                            qCarrier.ebUserNum,
                            qCarrier.nom,
                            qCarrier.prenom,
                            qEtablissementCarrier.ebEtablissementNum,
                            qEtablissementCarrier.nom,
                            qEtablissementBroker.ebEtablissementNum,
                            qEtablissementBroker.nom,
                            qEbDeclaration.listUnit,
                            qEbDeclaration.leg,
                            qEbDeclaration.nbOfTemporaryRegime,
                            qEbDeclaration.dateEcs,
                            qEbDeclaration.dateDeclaration,
                            qEbDeclaration.statut,
                            qEbDeclaration.declarationNumber,
                            qDeclarant.ebUserNum,
                            qDeclarant.nom,
                            qDeclarant.prenom,
                            qEbDeclaration.attachedFilesCount,
                            qDemande.ebDemandeNum,
                            qDemande.refTransport,
                            qEbDeclaration.dateCreationSys));

            query = getGlobalWhere(query, criteria);
            query = getGlobalJoin(query, criteria);

            query.distinct();
            query.from(qEbDeclaration).where(where);

            resultat = query.fetchFirst();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public List<EbCustomDeclaration> getListClearanceId(String term) {

        List<EbCustomDeclaration> resultat = new ArrayList<EbCustomDeclaration>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbCustomDeclaration> query = new JPAQuery<EbCustomDeclaration>(em);

            query
                .select(
                    QEbCustomDeclaration
                        .create(
                            qEbDeclaration.ebCustomDeclarationNum,
                            qEbDeclaration.idDedouanement,
                            qEbDeclaration.villeIncoterm));

            if (term != null) where = where
                .andAnyOf(
                    qEbDeclaration.idDedouanement.lower().contains(term.trim().toLowerCase()),
                    qEbDeclaration.villeIncoterm.lower().contains(term.trim().toLowerCase()));
            query.from(qEbDeclaration).where(where);

            resultat = (List<EbCustomDeclaration>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }
}
