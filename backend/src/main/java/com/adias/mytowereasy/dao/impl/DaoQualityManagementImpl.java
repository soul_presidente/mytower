/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.*;
import com.querydsl.core.types.dsl.*;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoQualityManagement;
import com.adias.mytowereasy.dto.EbFlagDTO;
import com.adias.mytowereasy.enumeration.CriticalityIncident;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.qm.EbQmIncident;
import com.adias.mytowereasy.model.qm.EbQmIncidentLine;
import com.adias.mytowereasy.model.qm.QEbQmIncident;
import com.adias.mytowereasy.model.qm.QEbQmIncidentLine;
import com.adias.mytowereasy.model.tt.QEbTtCategorieDeviation;
import com.adias.mytowereasy.repository.EbDemandeRepository;
import com.adias.mytowereasy.repository.EbEtablissementRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.FlagService;
import com.adias.mytowereasy.utils.search.SearchCriteria.OrderCriterias;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


@Component
@Transactional
public class DaoQualityManagementImpl extends Dao implements DaoQualityManagement {
    private static final Logger log = LoggerFactory.getLogger(DaoQualityManagementImpl.class);
    private static final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    @PersistenceContext
    EntityManager em;

    @Autowired
    private ConnectedUserService connectedUserService;

    @Autowired
    EbDemandeRepository ebDemandeRepository;

    @Autowired
    EbEtablissementRepository ebEtablissementRepository;
    @Autowired
    FlagService flagService;

    Path<String> alias = ExpressionUtils.path(String.class, "stringOrderWithExpression");
    Path<Integer> numberOrderAlias = ExpressionUtils.path(Integer.class, "numberOrderExpression");
    QExEbDemandeTransporteur qExDemandeTransporteur = QExEbDemandeTransporteur.exEbDemandeTransporteur;

    static QEbQmIncident qEbQmIncident = QEbQmIncident.ebQmIncident;
    QEbDemande qEbDemande = QEbDemande.ebDemande;
    QEbMarchandise qEbMarchandise = QEbMarchandise.ebMarchandise;
    QEbQmIncidentLine qEbQmIncidentLine = QEbQmIncidentLine.ebQmIncidentLine;
    QEbEtablissement qetablissementCarrier = QEbEtablissement.ebEtablissement;
    QEcCurrency qEcCurrency = new QEcCurrency("qEcCurrency");
    QEbTtCategorieDeviation qEbTtCategorieDeviation = new QEbTtCategorieDeviation("qEbTtCategorieDeviation");
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbCompagnie");

    QEbUser qEbuser = new QEbUser("qEbUser");

    private static Map<String, Object> orderHashMap = new HashMap<>();

    private Map<String, OrderSpecifier<?>> orderMapASC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, OrderSpecifier<?>> orderMapDESC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, Path<?>> orderMap = new HashMap<String, Path<?>>();

    @PostConstruct
    public void init() {
        initColumnsOrderMap();
        initUnitsOrderMap();
    }

    private void initColumnsOrderMap() {
        orderHashMap.put("category", qEbTtCategorieDeviation.libelle);
        orderHashMap.put("incidentQualif", qEbQmIncident.qualificationLabel);
        orderHashMap.put("incidentRef", qEbQmIncident.incidentRef);
        orderHashMap.put("dateSinceCreation", qEbQmIncident.dateCreation);
        orderHashMap.put("demandeTranportRef", qEbQmIncident.demandeTranportRef);
        orderHashMap.put("carrieretab", qEbQmIncident.carrierNom);
        orderHashMap.put("dateCreation", qEbQmIncident.dateCreation);
        orderHashMap.put("incidentCat", qEbQmIncident.categoryLabel);
        orderHashMap.put("originCountryLibelle", qEbQmIncident.originCountryLibelle);
        orderHashMap.put("destinationCountryLibelle", qEbQmIncident.destinationCountryLibelle);
        orderHashMap.put("modeTransport", qEbQmIncident.modeTransport);
        orderHashMap.put("assurance", qEbQmIncident.insurance);
        orderHashMap.put("currencyLabel", qEbQmIncident.currencyLabel);
        orderHashMap.put("montantReclamation", qEbQmIncident.claimAmount);
        orderHashMap.put("recu", qEbQmIncident.recovered);
        orderHashMap.put("incidentStatus", qEbQmIncident.incidentStatus);
        orderHashMap.put("eventTypeStatus", qEbQmIncident.eventTypeStatus);
        orderHashMap.put("incidentIssuer", qEbQmIncident.issuerNomPrenom);
        orderHashMap.put("criticality", qEbQmIncident.criticality);
        orderHashMap.put("dateMaj", qEbQmIncident.dateMaj);
        orderHashMap.put("lastContributerNomPremon", qEbQmIncident.lastContributerNomPremon);
        orderHashMap.put("carrierNom", qEbQmIncident.carrierNom);
        orderHashMap.put("incidentDesc", qEbQmIncident.incidentDesc);
        orderHashMap.put("dateIncident", qEbQmIncident.dateIncident);
        orderHashMap.put("ownerNomPrenom", qEbQmIncident.ownerNomPrenom);
        orderHashMap.put("psl", qEbQmIncident.psl);
        orderHashMap.put("typeRequestLibelle", qEbQmIncident.typeRequestLibelle);
    }

    private void initUnitsOrderMap() {
        orderMapASC.clear();

        orderMap.put("refTransport", qEbMarchandise.ebDemande().refTransport);
        orderMapASC.put("refTransport", qEbMarchandise.ebDemande().refTransport.asc());
        orderMapDESC.put("refTransport", qEbMarchandise.ebDemande().refTransport.desc());

        orderMap.put("customerReference", qEbMarchandise.customerReference);
        orderMapASC.put("customerReference", qEbMarchandise.customerReference.asc());
        orderMapDESC.put("customerReference", qEbMarchandise.customerReference.desc());

        orderMap.put("partNumber", qEbMarchandise.partNumber);
        orderMapASC.put("partNumber", qEbMarchandise.partNumber.asc());
        orderMapDESC.put("partNumber", qEbMarchandise.partNumber.desc());

        orderMap.put("serialNumber", qEbMarchandise.serialNumber);
        orderMapASC.put("serialNumber", qEbMarchandise.serialNumber.asc());
        orderMapDESC.put("serialNumber", qEbMarchandise.serialNumber.desc());

        orderMap.put("numAwbBol", qEbMarchandise.ebDemande().numAwbBol);
        orderMapASC.put("numAwbBol", qEbMarchandise.ebDemande().numAwbBol.asc());
        orderMapDESC.put("numAwbBol", qEbMarchandise.ebDemande().numAwbBol.desc());

        orderMap.put("dmdCustomerReference", qEbMarchandise.ebDemande().customerReference);
        orderMapASC.put("dmdCustomerReference", qEbMarchandise.ebDemande().customerReference.asc());
        orderMapDESC.put("dmdCustomerReference", qEbMarchandise.ebDemande().customerReference.desc());

        orderMap.put("libelleOriginCountry", qEbMarchandise.ebDemande().libelleOriginCountry);
        orderMapASC.put("libelleOriginCountry", qEbMarchandise.ebDemande().libelleOriginCountry.asc());
        orderMapDESC.put("libelleOriginCountry", qEbMarchandise.ebDemande().libelleOriginCountry.desc());
    }

    private void assertCriteriaMatching(SearchCriteriaQM criteria) {

        if (criteria == null || criteria.getModule() == null) {
            throw new IllegalArgumentException("Criteria or module not specified");
        }

    }

    @Override
    public List<EbQmIncident> getListEbIncident(SearchCriteriaQM criterias) {
        assertCriteriaMatching(criterias);

        List<EbQmIncident> result = new ArrayList<>();

        initColumnsOrderMap();

        String defaultOrderColumnName = "dateCreation";

        StringExpression stringExpression = qEbQmIncident.incidentRef;

        NumberExpression<Integer> numberExpression = qEbQmIncident.incidentStatus;

        try {
            JPAQuery<EbQmIncident> query = new JPAQuery<>(em);

            // Columns order
            if (criterias.getOrder() != null && criterias.getOrder().size() > 0) {
                String columnName = criterias.getOrder().get(0).get(OrderCriterias.columnName);

                OrderColumnDefinition colDef = new OrderColumnDefinition(columnName);

                stringExpression = colDef.getStringOrderWithExpression();
                numberExpression = colDef.getNumberOrderWithExpression();

                applyColumnsOrder(query, criterias.getOrder());
            }
            else {
                ComparableExpressionBase<String> defaultOrderExpression = (ComparableExpressionBase<String>) orderHashMap
                    .get(defaultOrderColumnName);

                query.orderBy(defaultOrderExpression.desc().nullsLast());
            }

            // Select
            if (criterias.getModule().equals(Enumeration.Module.QUALITY_MANAGEMENT.getCode())) {
                query = getQmSelectQuery(query, stringExpression, numberExpression);
            }

            query.from(qEbQmIncident);

            query.leftJoin(qEbQmIncident.xEbCompagnie(), qEbCompagnie);
            query.leftJoin(qEbQmIncident.currency(), qEcCurrency);
            query.leftJoin(qEbQmIncident.category(), qEbTtCategorieDeviation);
            // query.leftJoin(qEbQmIncident.xEbUserOwnerRequest(),
            // qEbUserOwnerRequest);

            query.leftJoin(qEbQmIncident.ebDemande(), qEbDemande);
            query.leftJoin(qEbDemande.exEbDemandeTransporteurs, qExDemandeTransporteur);
            query.leftJoin(qEbQmIncident.carrier(), qetablissementCarrier);

            query.distinct();

            // Where
            query = applySearchCriteriaIncident(query, criterias);

            // Pagination
            applyPagination(query, criterias);

            // Fetch
            result = query.fetch();

            // Flags
            applyPostFetchFlagFilter(result);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private void applyPostFetchFlagFilter(List<EbQmIncident> result) {
        result.forEach(incident -> {
            String listFlagIcon = incident.getListFlag() != null ? incident.getListFlag().trim() : null;

            if (listFlagIcon != null && !listFlagIcon.isEmpty()) {
                incident.setListEbFlagDTO(EbFlagDTO.sortByName(flagService.getListFlagDTOFromCodes(listFlagIcon)));
            }

        });
    }

    private void applyPagination(JPAQuery<EbQmIncident> query, SearchCriteriaQM criterias) {

        if (criterias.getPageNumber() != null) {
            query.offset(criterias.getPageNumber() * (criterias.getSize() != null ? criterias.getSize() : 1));
        }

        if (criterias.getSize() != null && criterias.getSize() >= 0) {
            query.limit(criterias.getSize());
        }

    }

    private void applyColumnsOrder(JPAQuery<EbQmIncident> query, List<Map<OrderCriterias, String>> order) {

        for (Map<OrderCriterias, String> map: order) {
            applyOneColumnOrder(query, map);
        }

    }

    private void applyOneColumnOrder(JPAQuery<EbQmIncident> query, Map<OrderCriterias, String> map) {
        String columnName = map.get(OrderCriterias.columnName);
        if (columnName == null) return;

        OrderSpecifier orderSpecifier = null;

        boolean isAsc = map.get(OrderCriterias.dir).equals("asc");

        Order direction = isAsc ? Order.ASC : Order.DESC;

        boolean columnIsCategoryOrField = columnName.matches("^(category|field)[0-9]+$");
        boolean columnIsModeTransport = columnName.contentEquals("xEcModeTransport");

        orderSpecifier = getOrderSpecifier(
            orderSpecifier,
            isAsc,
            direction,
            columnIsCategoryOrField,
            columnIsModeTransport);

        orderSpecifier = getOrderSpecifierByColumnName(
            columnName,
            orderSpecifier,
            isAsc,
            columnIsCategoryOrField,
            columnIsModeTransport);

        if (orderSpecifier != null) {
            query.orderBy(orderSpecifier.nullsLast());
        }

    }

    private OrderSpecifier getOrderSpecifierByColumnName(
        String columnName,
        OrderSpecifier orderSpecifier,
        boolean isAsc,
        boolean columnIsCategoryOrField,
        boolean columnIsModeTransport) {

        if (!columnIsModeTransport && !columnIsCategoryOrField && orderHashMap.get(columnName) != null) {
            ComparableExpressionBase<String> comparableExpression = (ComparableExpressionBase<String>) orderHashMap
                .get(columnName);

            orderSpecifier = isAsc ? comparableExpression.asc() : comparableExpression.desc();
        }

        return orderSpecifier;
    }

    private OrderSpecifier getOrderSpecifier(
        OrderSpecifier orderSpecifier,
        boolean isAsc,
        Order direction,
        boolean columnIsCategoryOrField,
        boolean columnIsModeTransport) {
        Path<?> target;

        if (columnIsCategoryOrField && isAsc) {
            target = alias;
            orderSpecifier = new OrderSpecifier(direction, target);
        }
        else if (columnIsModeTransport && isAsc) {
            target = numberOrderAlias;
            orderSpecifier = new OrderSpecifier(direction, target);
        }

        return orderSpecifier;
    }

    private JPAQuery<EbQmIncident> getQmSelectQuery(
        JPAQuery<EbQmIncident> query,
        StringExpression stringExpression,
        NumberExpression<Integer> numberExpression) {
        return query
            .select(
                QEbQmIncident
                    .create(
                        qEbQmIncident.ebQmIncidentNum,
                        qEbQmIncident.incidentRef,
                        qEbQmIncident.insurance,
                        qEbQmIncident.modeTransport,
                        qEbQmIncident.partNumber,
                        qEbQmIncident.serialNumber,
                        qEbQmIncident.shippingType,
                        qEbQmIncident.orderNumber,
                        qEbQmIncident.dateCreation,
                        qEbQmIncident.dateMaj,
                        qEbQmIncident.dateCloture,
                        qEbQmIncident.dateIncident,
                        qEbQmIncident.incidentStatus,
                        qEbQmIncident.originCountryLibelle,
                        qEbQmIncident.destinationCountryLibelle,
                        qEbQmIncident.currencyLabel,
                        qEbQmIncident.lastContributerNomPremon,
                        qEbQmIncident.ownerNomPrenom,
                        qEbQmIncident.issuerNomPrenom,
                        qEbQmIncident.carrierNom,
                        qEbQmIncident.categoryLabel,
                        qEbQmIncident.qualificationLabel,
                        qEbQmIncident.claimAmount,
                        qEbQmIncident.recovered,
                        qEbQmIncident.balance,
                        qEbQmIncident.demandeTranportRef,
                        qEbQmIncident.flowType,
                        qEbQmIncident.thirdPartyComment,
                        qEbQmIncident.etablissementComment,
                        qEbQmIncident.incidentDesc,
                        qEbQmIncident.goodValues,
                        qEbQmIncident.demandeTranportRef,
                        qEbQmIncident.qualificationLabel,
                        qEbQmIncident.carrier().ebEtablissementNum,
                        qEbQmIncident.addImpact,
                        qEbQmIncident.xEbDemande,
                        qEbQmIncident.rootCauses,
                        qEbQmIncident.daysDelay,
                        qEbQmIncident.hoursDelay,
                        qEbQmIncident.unitDamaged,
                        qEbQmIncident.unitLost,
                        qEbQmIncident.amondOfunitLost,
                        qEbQmIncident.customFields,
                        qEbQmIncident.listCategories,
                        stringExpression.as("stringOrderWithExpression"),
                        numberExpression.as("numberOrderExpression"),
                        qEbQmIncident.waitingForText,
                        qEbQmIncident.rootCauseStatut,
                        qEbQmIncident.unitDamagedAmount,
                        qEbQmIncident.impactedTransportAmount,
                        qEbQmIncident.othersCost,
                        qEbCompagnie.ebCompagnieNum,
                        qEbCompagnie.nom,
                        qEcCurrency.ecCurrencyNum,
                        qEcCurrency.libelle,
                        qEcCurrency.codeLibelle,
                        qEbTtCategorieDeviation.ebTtCategorieDeviationNum,
                        qEbTtCategorieDeviation.libelle,
                        qEbQmIncident.recoveredInsurance,
                        qEbQmIncident.impactedTransportPercentage,
                        qEcCurrency.symbol,
                        qEcCurrency.code,
                        qEbQmIncident.listFlag,
                        qEbQmIncident.severity,
                        qEbQmIncident.actions,
                        qEbQmIncident.insuranceComment,
                        qEbQmIncident.criticality,
                        qEbQmIncident.responsable,
                        qEbQmIncident.rootCauseCategory,
                        qEbQmIncident.listRootCauses,
                        qEbQmIncident.psl,
                        qEbQmIncident.typeRequestLibelle,
                        qEbQmIncident.typeRequestNum,
                        qEbQmIncident.eventType,
                        qEbQmIncident.eventTypeStatus
                    // qEbUserOwnerRequest.ebUserNum,
                    // qEbUserOwnerRequest.nom,
                    // qEbUserOwnerRequest.prenom

                    ));
    }

    private JPAQuery applySearchCriteriaIncident(JPAQuery query, SearchCriteriaQM criterias) {
        if (criterias == null) return query;

        BooleanBuilder where = new BooleanBuilder();

        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criterias);

        applyVisibilityByCategoryLabels(criterias, where);

        query.leftJoin(qEbQmIncident.carrier(), qetablissementCarrier);
        query.leftJoin(qEbQmIncident.xEbCompagnie(), qEbCompagnie);
        query.leftJoin(qEbQmIncident.ebDemande(), qEbDemande);
        query.leftJoin(qEbQmIncident.currency(), qEcCurrency);
        query.leftJoin(qEbQmIncident.category(), qEbTtCategorieDeviation);
        query.leftJoin(qEbDemande.xEbUserCt(), qEbuser);

        applyAdvancedSearch(criterias, where);

        applyFlagSearch(criterias, where);

        applyUserRoleVisibility(where, connectedUser);

        query.where(where);

        return query;
    }

    private void applyUserRoleVisibility(BooleanBuilder where, EbUser connectedUser) {
        BooleanBuilder whereOfOr = new BooleanBuilder();

        if (connectedUser.getService() != null
            && (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker())) {
            QEbEtablissement qetablissementCarrier = QEbEtablissement.ebEtablissement;

            BooleanExpression connectedUserIsFromCarrierEtablissement = qExDemandeTransporteur.status
                .eq(Enumeration.CarrierStatus.FINAL_CHOICE.getCode())
                .and(
                    qetablissementCarrier.ebEtablissementNum
                        .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()))
                .and(qExDemandeTransporteur.xEbDemande().ebDemandeNum.eq(qEbQmIncident.xEbDemande))
                .and(qetablissementCarrier.ebEtablissementNum.eq(qEbQmIncident.carrier().ebEtablissementNum));

            where
                .and(
                    connectedUserIsFromCarrierEtablissement
                        .or(
                            qEbQmIncident.xEbEtablissement().ebEtablissementNum
                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())));
        }
        else if (connectedUser.isChargeur()
            ||
            (connectedUser.isControlTower() &&
                Enumeration.Role.ROLE_CHARGEUR.getCode().equals(connectedUser.getEbCompagnie().getCompagnieRole()))) {
            applyVisibilityForCharger(where, connectedUser, whereOfOr);
        }
        else if (connectedUser.isControlTower() &&
            !Enumeration.Role.ROLE_CHARGEUR.getCode().equals(connectedUser.getEbCompagnie().getCompagnieRole())) {
            applyVisibilityForControlTower(where, connectedUser, whereOfOr);
        }

    }

    private void applyVisibilityForCharger(BooleanBuilder where, EbUser connectedUser, BooleanBuilder whereOfOr) {
        whereOfOr
            .andAnyOf(
                qEbQmIncident.xEbCompagnie().ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()))
            .or(
                qEbQmIncident.xEbEtablissement().ebEtablissementNum
                    .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));

        if (!connectedUser.isSuperAdmin()) {
            BooleanBuilder boolLabels = applyVisibilityByCategoryLabels(connectedUser);

            if (boolLabels.hasValue()) {
                where.andAnyOf(qEbQmIncident.listLabels.isNotNull().andAnyOf(boolLabels));
            }

        }

        where.and(whereOfOr);
    }

    private void applyVisibilityForControlTower(BooleanBuilder where, EbUser connectedUser, BooleanBuilder whereOfOr) {

        if (connectedUser.isAdmin()) {

            if (connectedUser.isAdmin()) {
                whereOfOr
                    .andAnyOf(
                        qEbDemande.xEbUserCt().ebEtablissement().ebEtablissementNum
                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()))
                    .or(
                        qExDemandeTransporteur.xEbEtablissement().ebEtablissementNum
                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));
            }
            else if (connectedUser.isSuperAdmin()) {
                whereOfOr
                    .andAnyOf(
                        qEbDemande.xEbUserCt().ebCompagnie().ebCompagnieNum
                            .eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
            }

            where.and(whereOfOr);
        }

    }

    private BooleanBuilder applyVisibilityByCategoryLabels(EbUser connectedUser) {
        BooleanBuilder boolLabels = new BooleanBuilder();

        if (connectedUser.getListCategories() != null) {

            for (EbCategorie cat: connectedUser.getListCategories()) {

                if (cat.getIsAll() != null && !cat.getIsAll() && cat.getLabels() != null) {

                    for (EbLabel label: cat.getLabels()) {
                        boolLabels
                            .or(
                                new BooleanBuilder()
                                    .andAnyOf(
                                        qEbQmIncident.listLabels.startsWith(label.getEbLabelNum() + ","),
                                        qEbQmIncident.listLabels.contains("," + label.getEbLabelNum() + ","),
                                        qEbQmIncident.listLabels.endsWith("," + label.getEbLabelNum()),
                                        qEbQmIncident.listLabels.eq("" + label.getEbLabelNum())));
                    }

                }

            }

        }

        return boolLabels;
    }

    private void applyFlagSearch(SearchCriteriaQM criterias, BooleanBuilder where) {
        // recherche par flags
        if (criterias.getListFlag() == null || criterias.getListFlag().trim().isEmpty()) return;

        List<String> listFlagId = Arrays.asList(criterias.getListFlag().trim().split(","));
        Predicate[] predicats = new Predicate[listFlagId.size() * 4];
        int j = 0;

        for (int i = 0; i < listFlagId.size(); i++) {
            predicats[i + j] = qEbQmIncident.listFlag
                .contains(listFlagId.get(i)).and(qEbQmIncident.listFlag.trim().length().eq(listFlagId.get(i).length()));
            predicats[i + j + 1] = qEbQmIncident.listFlag.endsWith("," + listFlagId.get(i));
            predicats[i + j + 2] = qEbQmIncident.listFlag.startsWith(listFlagId.get(i) + ",");
            predicats[i + j + 3] = qEbQmIncident.listFlag.contains("," + listFlagId.get(i) + ",");
            j += 3;
        }

        where.andAnyOf(predicats);
    }

    private void applySimpleSearch(SearchCriteriaQM criterias, BooleanBuilder where) {
        if (criterias.getSearch() == null) return;

        String value = criterias.getSearch().get("value");

        if (value == null || value.isEmpty()) return;

        value = value.trim().toLowerCase();

        BooleanBuilder simpleSearchBooleanExp = new BooleanBuilder();

        applySimpleSearchFilterForDate(value, simpleSearchBooleanExp);

        applySimpleSearchFilterForModeTransport(value, simpleSearchBooleanExp);

        applySimpleSearchFilterForFields(value, simpleSearchBooleanExp);

        where.and(simpleSearchBooleanExp);
    }

    private void applySimpleSearchFilterForFields(String value, BooleanBuilder simpleSearchBooleanExp) {
        List<Integer> criticalityIncidentIds = CriticalityIncident.getCriticalityIncidentByKey(value) != null ?
            CriticalityIncident
                .getCriticalityIncidentByKey(value).stream().map(CriticalityIncident::getCode)
                .collect(Collectors.toList()) :
            Collections.emptyList();

        simpleSearchBooleanExp
            .andAnyOf(
                qEbQmIncident.demandeTranportRef.toLowerCase().contains(value),
                qEbQmIncident.criticality.in(criticalityIncidentIds),
                qEbQmIncident.partNumber.toLowerCase().contains(value),
                qEbQmIncident.orderNumber.toLowerCase().contains(value),
                qEbQmIncident.shippingType.toLowerCase().contains(value),
                qEbQmIncident.serialNumber.toLowerCase().contains(value),
                qEbQmIncident.destinationCountryLibelle.toLowerCase().contains(value),
                qEbQmIncident.originCountryLibelle.toLowerCase().contains(value),
                qEbQmIncident.qualificationLabel.toLowerCase().contains(value),
                qEbQmIncident.claimAmount.stringValue().toLowerCase().contains(value),
                qEbQmIncident.incidentRef.toLowerCase().contains(value),
                qEbQmIncident.dateMaj.stringValue().toLowerCase().contains(value),
                qEbQmIncident.dateCreation.stringValue().toLowerCase().contains(value),
                qEbQmIncident.dateCloture.stringValue().toLowerCase().contains(value),
                qEbQmIncident.flowType.stringValue().toLowerCase().contains(value),
                qEbQmIncident.balance.stringValue().toLowerCase().contains(value),
                qEbQmIncident.categoryLabel.stringValue().toLowerCase().contains(value),
                qEbQmIncident.currencyLabel.toLowerCase().contains(value),
                qEbQmIncident.issuerNomPrenom.toLowerCase().contains(value),
                qEbQmIncident.xEbEtablissement().nom.toLowerCase().contains(value),
                qEbQmIncident.lastContributerNomPremon.toLowerCase().contains(value),
                qEbQmIncident.carrierNom.toLowerCase().contains(value),
                qEbQmIncident.recovered.stringValue().toLowerCase().contains(value),
                qEbQmIncident.listCustomFieldsFlat.toLowerCase().like("%:\"%" + value + "%\"%"),
                qEbQmIncident.listCategoryFlat.toLowerCase().like("%:\"%" + value + "%\"%"));
    }

    private void applySimpleSearchFilterForModeTransport(String value, BooleanBuilder simpleSearchBooleanExp) {
        String modeTransportSearchValue = value;

        if (value.contains("(")) {
            modeTransportSearchValue = value.replace(")", "");
            String[] vs = modeTransportSearchValue.split("\\(");
            modeTransportSearchValue = vs[0].trim();
        }

        List<Integer> listCodeModeTransport = Enumeration.ModeTransport.getListCodeByLibelle(modeTransportSearchValue);

        if (listCodeModeTransport == null || listCodeModeTransport.isEmpty()) listCodeModeTransport = new ArrayList<>();

        if (!listCodeModeTransport.isEmpty()) simpleSearchBooleanExp
            .or(qEbQmIncident.modeTransport.in(listCodeModeTransport));
    }

    private void applySimpleSearchFilterForDate(String value, BooleanBuilder simpleSearchBooleanExp) {

        if (Pattern.compile("([0-9]{2}/){2}[0-9]{4}").matcher(value).matches()) {

            try {
                Date dateSearchValue = formatter.parse(value);

                if (dateSearchValue != null) {
                    simpleSearchBooleanExp
                        .or(
                            qEbQmIncident.dateCreation
                                .stringValue().toLowerCase()
                                .contains((new SimpleDateFormat("yyyy-MM-dd")).format(dateSearchValue)));

                    simpleSearchBooleanExp
                        .or(
                            qEbQmIncident.dateCloture
                                .stringValue().toLowerCase()
                                .contains((new SimpleDateFormat("yyyy-MM-dd")).format(dateSearchValue)));

                    simpleSearchBooleanExp
                        .or(
                            qEbQmIncident.dateMaj
                                .stringValue().toLowerCase()
                                .contains((new SimpleDateFormat("yyyy-MM-dd")).format(dateSearchValue)));
                }

            } catch (ParseException e) {
                log.warn(String.format("Unable to parse date value for simple search: %s", value));
            }

        }

    }

    private void applyVisibilityByCategoryLabels(SearchCriteriaQM criterias, BooleanBuilder where) {

        if (criterias.getMapCategoryFields() != null && !criterias.getMapCategoryFields().isEmpty()) {
            BooleanBuilder boolLabels = new BooleanBuilder();

            BooleanBuilder orBoolLabels;

            List<Integer> ebLabels;

            for (String key: criterias.getMapCategoryFields().keySet()) {
                ebLabels = criterias.getMapCategoryFields().get(key);
                orBoolLabels = new BooleanBuilder();
                int i, n = ebLabels.size();

                for (i = 0; i < n; i++) {
                    long ebLabelNum = Math.round(Double.parseDouble(ebLabels.get(i) + ""));
                    orBoolLabels
                        .or(
                            new BooleanBuilder()
                                .andAnyOf(
                                    qEbQmIncident.listLabels.like(ebLabelNum + ",%"),
                                    qEbQmIncident.listLabels.like("%," + ebLabelNum + ",%"),
                                    qEbQmIncident.listLabels.like("%," + ebLabelNum),
                                    qEbQmIncident.listLabels.eq("" + ebLabelNum)));
                }

                if (orBoolLabels.hasValue()) boolLabels.and(orBoolLabels);
            }

            where.and(qEbQmIncident.listLabels.isNotNull().andAnyOf(boolLabels));
        }

    }

    private void applyAdvancedSearch(SearchCriteriaQM criterias, BooleanBuilder where) {
        if (criterias.getListIdObject() != null && !criterias.getListIdObject().isEmpty()) where
            .and(qEbQmIncident.ebQmIncidentNum.in(criterias.getListIdObject()));

        if (criterias.getEventType() != null) where.and(qEbQmIncident.eventType.eq(criterias.getEventType()));

        if (criterias.getEbQmIncidentNum() != null) {
            where.and(qEbQmIncident.ebQmIncidentNum.eq(criterias.getEbQmIncidentNum()));
        }

        if (criterias.getEbDemandeNum() != null) where.and(qEbQmIncident.xEbDemande.eq(criterias.getEbDemandeNum()));

        if (criterias.getListDestinations() != null && !criterias.getListDestinations().isEmpty()) {
            where = where.and(qEbQmIncident.destinationCountry().ecCountryNum.in(criterias.getListDestinations()));
        }

        if (criterias.getListOrigins() != null && !criterias.getListOrigins().isEmpty()) {
            where = where.and(qEbQmIncident.originCountry().ecCountryNum.in(criterias.getListOrigins()));
        }

        if (criterias.getListCriticality() != null && !criterias.getListCriticality().isEmpty()) {
            where = where.and(qEbQmIncident.criticality.in(criterias.getListCriticality()));
        }

        if (criterias.getListTransportRef() != null && !criterias.getListTransportRef().isEmpty()) {
            where = where.and(qEbDemande.refTransport.in(criterias.getListTransportRef()));
        }

        if (criterias.getDateCreationTo() != null) {
            where = where
                .andAnyOf(
                    qEbQmIncident.dateCreation.eq(criterias.getDateCreationFrom()),
                    qEbQmIncident.dateCreation.after(criterias.getDateCreationFrom()));
        }

        if (criterias.getDateCreationTo() != null) {
            where = where
                .andAnyOf(
                    qEbQmIncident.dateCreation.eq(criterias.getDateCreationFrom()),
                    qEbQmIncident.dateCreation.before(criterias.getDateCreationFrom()));
        }

        if (criterias.getListInsuranceIncident() != null && !criterias.getListInsuranceIncident().isEmpty()) {
            where = where.and(qEbQmIncident.insurance.in(criterias.getListInsuranceIncident()));
        }

        if (criterias.getListStatusIncident() != null && !criterias.getListStatusIncident().isEmpty()) {
            where = where.and(qEbQmIncident.incidentStatus.in(criterias.getListStatusIncident()));
        }

        if (criterias.getListIncidentRefs() != null && !criterias.getListIncidentRefs().isEmpty()) {
            where = where.and(qEbQmIncident.ebQmIncidentNum.in(criterias.getListIncidentRefs()));
        }

        if (criterias.getListCarrierRefs() != null && !criterias.getListCarrierRefs().isEmpty()) {
            where = where.and(qEbQmIncident.carrier().ebEtablissementNum.in(criterias.getListCarrierRefs()));
        }

        if (criterias.getListTransportPriority() != null && !criterias.getListTransportPriority().isEmpty()) {
            where = where.and(qEbQmIncident.typeRequestNum.in(criterias.getListTransportPriority()));
        }

        if (criterias.getListPsl() != null && !criterias.getListPsl().isEmpty()) {
            where = where.and(qEbQmIncident.psl.in(criterias.getListPsl()));
        }

        if (criterias.getListEbUserOwnerRequest() != null && !criterias.getListEbUserOwnerRequest().isEmpty()) {
            where.and(qEbQmIncident.ownerNum.in(criterias.getListEbUserOwnerRequest()));
        }

        if (criterias.getListIncidentCategories() != null && !criterias.getListIncidentCategories().isEmpty()) {
            where = where.and(qEbQmIncident.categoryLabel.in(criterias.getListIncidentCategories()));
        }

        if (criterias.getEbQmRootCauseNum() != null) {
            where = where.and(qEbQmIncident.listRootCauses.contains(":" + criterias.getEbQmRootCauseNum() + ":"));
        }

        if (criterias.getListEventTypeStatus() != null && !criterias.getListEventTypeStatus().isEmpty()) {
            where = where.and(qEbQmIncident.eventTypeStatus.in(criterias.getListEventTypeStatus()));
        }

        criterias.applyAdvancedSearchForCustomFields(where, qEbQmIncident.customFields);
        applySimpleSearch(criterias, where);
    }

    @Override
    public EbQmIncident getIncident(SearchCriteriaQM criteria) {
        EbQmIncident incident = null;
        initColumnsOrderMap();
        BooleanBuilder where = new BooleanBuilder();

        try {
            JPAQuery<EbQmIncident> query = new JPAQuery<EbQmIncident>(em);
            StringExpression stringOrderWithExpression = qEbQmIncident.incidentRef;
            NumberExpression<Integer> numberOrderWithExpression = qEbQmIncident.incidentStatus;
            query
                .select(
                    QEbQmIncident
                        .create(
                            qEbQmIncident.ebQmIncidentNum,
                            qEbQmIncident.incidentRef,
                            qEbQmIncident.insurance,
                            qEbQmIncident.modeTransport,
                            qEbQmIncident.partNumber,
                            qEbQmIncident.serialNumber,
                            qEbQmIncident.shippingType,
                            qEbQmIncident.orderNumber,
                            qEbQmIncident.dateCreation,
                            qEbQmIncident.dateMaj,
                            qEbQmIncident.dateCloture,
                            qEbQmIncident.dateIncident,
                            qEbQmIncident.incidentStatus,
                            qEbQmIncident.originCountryLibelle,
                            qEbQmIncident.destinationCountryLibelle,
                            qEbQmIncident.currencyLabel,
                            qEbQmIncident.lastContributerNomPremon,
                            qEbQmIncident.ownerNomPrenom,
                            qEbQmIncident.issuerNomPrenom,
                            qEbQmIncident.carrierNom,
                            qEbQmIncident.categoryLabel,
                            qEbQmIncident.qualificationLabel,
                            qEbQmIncident.claimAmount,
                            qEbQmIncident.recovered,
                            qEbQmIncident.balance,
                            qEbQmIncident.demandeTranportRef,
                            qEbQmIncident.flowType,
                            qEbQmIncident.thirdPartyComment,
                            qEbQmIncident.etablissementComment,
                            qEbQmIncident.incidentDesc,
                            qEbQmIncident.goodValues,
                            qEbQmIncident.demandeTranportRef,
                            qEbQmIncident.qualificationLabel,
                            qEbQmIncident.carrier().ebEtablissementNum,
                            qEbQmIncident.addImpact,
                            qEbQmIncident.xEbDemande,
                            qEbQmIncident.rootCauses,
                            qEbQmIncident.daysDelay,
                            qEbQmIncident.hoursDelay,
                            qEbQmIncident.unitDamaged,
                            qEbQmIncident.unitLost,
                            qEbQmIncident.amondOfunitLost,
                            qEbQmIncident.customFields,
                            qEbQmIncident.listCategories,
                            stringOrderWithExpression.as("stringOrderWithExpression"),
                            numberOrderWithExpression.as("numberOrderExpression"),
                            qEbQmIncident.waitingForText,
                            qEbQmIncident.rootCauseStatut,
                            qEbQmIncident.unitDamagedAmount,
                            qEbQmIncident.impactedTransportAmount,
                            qEbQmIncident.othersCost,
                            qEbQmIncident.xEbCompagnie().ebCompagnieNum,
                            qEbQmIncident.xEbCompagnie().nom,
                            qEcCurrency.ecCurrencyNum,
                            qEcCurrency.libelle,
                            qEcCurrency.codeLibelle,
                            qEbTtCategorieDeviation.ebTtCategorieDeviationNum,
                            qEbTtCategorieDeviation.libelle,
                            qEbQmIncident.recoveredInsurance,
                            qEbQmIncident.impactedTransportPercentage,
                            qEcCurrency.symbol,
                            qEcCurrency.code,
                            qEbQmIncident.listFlag,
                            qEbQmIncident.severity,
                            qEbQmIncident.actions,
                            qEbQmIncident.insuranceComment,
                            qEbQmIncident.criticality,
                            qEbQmIncident.responsable,
                            qEbQmIncident.rootCauseCategory,
                            qEbQmIncident.listRootCauses,
                            qEbQmIncident.psl,
                            qEbQmIncident.typeRequestLibelle,
                            qEbQmIncident.typeRequestNum,
                            qEbQmIncident.eventTypeStatus,
                            qEbQmIncident.eventType
                        // qEbQmIncident.xEbUserOwnerRequest().ebUserNum,
                        // qEbQmIncident.xEbUserOwnerRequest().nom,
                        // qEbQmIncident.xEbUserOwnerRequest().prenom
                        ));

            where.and(qEbQmIncident.ebQmIncidentNum.eq(criteria.getEbQmIncidentNum()));
            query.distinct();
            query.from(qEbQmIncident).where(where);

            query.leftJoin(qEbQmIncident.currency(), qEcCurrency);
            query.leftJoin(qEbQmIncident.category(), qEbTtCategorieDeviation);
            incident = query.fetchOne();

            List<EbQmIncidentLine> listIncidentLine = this.listIncidentLineByIncidentNum(criteria);
            incident.setIncidentLines(listIncidentLine);

            Optional<EbDemande> demande = ebDemandeRepository.findById(incident.getxEbDemande());

            if (demande.get().getxEbTransporteurEtablissementNum() != null) {
                Optional<EbEtablissement> etablissement = ebEtablissementRepository
                    .findById(demande.get().getxEbTransporteurEtablissementNum());
                incident.setCarrier(etablissement != null ? etablissement.get() : null);
            }

        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }

        String listFlagIcon = incident.getListFlag() != null ? incident.getListFlag().trim() : null;
        incident
            .setListEbFlagDTO(
                listFlagIcon != null && !listFlagIcon.isEmpty() ?
                    EbFlagDTO.sortByName(flagService.getListFlagDTOFromCodes(listFlagIcon)) :
                    null);

        return incident;
    }

    @Override
    public List<EbMarchandise> listMarchandise(SearchCriteriaQM criteria) {
        this.initUnitsOrderMap();
        List<EbMarchandise> listMarchandises = null;

        try {
            JPAQuery<EbMarchandise> query = new JPAQuery<EbMarchandise>(em);
            BooleanBuilder where = new BooleanBuilder();

            query.from(qEbMarchandise);

            query
                .select(
                    QEbMarchandise
                        .create(
                            qEbMarchandise.ebMarchandiseNum,
                            qEbMarchandise.classGood,
                            qEbMarchandise.comment,
                            qEbMarchandise.dangerousGood,
                            qEbMarchandise.dg,
                            qEbMarchandise.dryIce,
                            qEbMarchandise.equipment,
                            qEbMarchandise.heigth,
                            qEbMarchandise.length,
                            qEbMarchandise.numberOfUnits,
                            qEbMarchandise.packaging,
                            qEbMarchandise.quantityDryIce,
                            qEbMarchandise.sensitive,
                            qEbMarchandise.storageTemperature,
                            qEbMarchandise.xEbTypeUnit,
                            qEbMarchandise.un,
                            qEbMarchandise.volume,
                            qEbMarchandise.width,
                            qEbMarchandise.customerReference,
                            qEbDemande,
                            qEbMarchandise.weight,
                            qEbMarchandise.unitReference,
                            qEcCurrency.ecCurrencyNum,
                            qEcCurrency.libelle,
                            qEcCurrency.codeLibelle,
                            qEcCurrency.symbol,
                            qEcCurrency.code,
                            qEbMarchandise.cptmOperationNumber,
                            qEbMarchandise.partNumber,
                            qEbMarchandise.serialNumber,
                            qEbMarchandise.price,
                            qEbMarchandise.binLocation,
                            qEbMarchandise.exportControl,
                            qEbMarchandise.exportRef,
                            qEbMarchandise.oversizedItem,
                            qEbMarchandise.largeItem,
                            qEbMarchandise.exportControlDelay,
                            qEbMarchandise.packingList,
                            qEbMarchandise.typeOfGoodCode));

            query = applySearchCriteriaMarchandise(query, where, criteria);

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            query.distinct();

            query.leftJoin(qEbMarchandise.ebDemande(), qEbDemande);
            query.leftJoin(qEbDemande.xecCurrencyInvoice(), qEcCurrency);

            query.where(where);

            if (criteria.getOrderedColumn() != null) {

                if (criteria.getAscendant()) {
                    query.orderBy(this.orderMapASC.get(criteria.getOrderedColumn()));
                }
                else {
                    query.orderBy(this.orderMapDESC.get(criteria.getOrderedColumn()));
                }

            }
            else {
                query.orderBy(this.orderMapDESC.get("refTransport"));
            }

            query.from(qEbMarchandise).where(where);

            query.distinct();

            query.leftJoin(qEbMarchandise.ebDemande(), qEbDemande);
            query.leftJoin(qEbDemande.xecCurrencyInvoice(), qEcCurrency);

            listMarchandises = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listMarchandises;
    }

    @Override
    public List<EbQmIncidentLine> listIncidentLineByIncidentNum(SearchCriteriaQM criteria) {
        List<EbQmIncidentLine> listIncidentLine = null;

        try {
            JPAQuery<EbQmIncidentLine> query = new JPAQuery<EbQmIncidentLine>(em);
            BooleanBuilder where = new BooleanBuilder();

            query
                .select(
                    QEbQmIncidentLine
                        .create(
                            qEbQmIncidentLine.ebQmIncidentLineNum,
                            qEbQmIncidentLine.unitReference,
                            qEbQmIncidentLine.numberOfUnits,
                            qEbQmIncidentLine.weight,
                            qEbQmIncidentLine.carrierName,
                            qEbQmIncidentLine.atd,
                            qEbQmIncidentLine.eta,
                            qEbQmIncidentLine.ata,
                            qEbQmIncidentLine.etd,
                            qEbQmIncidentLine.typeMarchandise,
                            qEbQmIncidentLine.ebMarchandiseNum,
                            qEbQmIncidentLine.dangerousGood,
                            qEbQmIncidentLine.comment,
                            qEbQmIncidentLine.weightImpacted,
                            qEbQmIncidentLine.numberOfUnitsImpacted,
                            qEbQmIncidentLine.delayDepartureDate,
                            qEbQmIncidentLine.delayArrivalDate));

            where.and(qEbQmIncidentLine.incident().ebQmIncidentNum.eq(criteria.getEbQmIncidentNum()));

            query.distinct();
            query.from(qEbQmIncidentLine).where(where);
            listIncidentLine = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listIncidentLine;
    }

    public List<EbQmIncident> getIncidentRefByListIncident(SearchCriteriaQM criterias) {
        // TODO Auto-generated method stub
        List<EbQmIncident> resultat = new ArrayList<EbQmIncident>();
        initColumnsOrderMap();

        try {
            JPAQuery<EbQmIncident> query = new JPAQuery<EbQmIncident>(em);

            query.select(QEbQmIncident.create(qEbQmIncident.ebQmIncidentNum, qEbQmIncident.incidentRef));

            query.from(qEbQmIncident);
            query.distinct();
            query = applySearchCriteriaIncident(query, criterias);

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public Integer nextValQuality() {
        Integer result = null;

        try {
            result = ((BigInteger) em
                .createNativeQuery("SELECT NEXTVAL('work.eb_qm_incident_eb_qm_incident_num_seq')").getSingleResult())
                    .intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Long getCountList(SearchCriteriaQM criterias) {
        Long count = 0L;

        try {
            JPAQuery<Long> query = new JPAQuery<>(em);

            query.from(qEbQmIncident);

            query.leftJoin(qEbQmIncident.ebDemande(), qEbDemande);
            query.leftJoin(qEbDemande.exEbDemandeTransporteurs, qExDemandeTransporteur);
            query.leftJoin(qEbQmIncident.carrier(), qetablissementCarrier);

            query = applySearchCriteriaIncident(query, criterias);
            query.distinct();

            count = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return count;
    }

    @Override
    public Long countlistMarchandise(SearchCriteriaQM criteria) {
        long result = 0L;

        try {
            JPAQuery<EbMarchandise> query = new JPAQuery<EbMarchandise>(em);
            BooleanBuilder where = new BooleanBuilder();
            query = applySearchCriteriaMarchandise(query, where, criteria);
            query.distinct();

            query.from(qEbMarchandise);

            query.leftJoin(qEbMarchandise.ebDemande(), qEbDemande);
            query.leftJoin(qEbDemande.xecCurrencyInvoice(), qEcCurrency);

            query.where(where);
            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private JPAQuery applySearchCriteriaMarchandise(JPAQuery query, BooleanBuilder where, SearchCriteriaQM criterias)
        throws Exception {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criterias);

        if (criterias.getEbDemandeNum() != null) where.and(qEbDemande.ebDemandeNum.eq(criterias.getEbDemandeNum()));

        if (criterias.getSearchterm() != null && !criterias.getSearchterm().isEmpty()) {
            String searchTerm = criterias.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qEbDemande.refTransport.toLowerCase().contains(searchTerm),
                    qEbMarchandise.customerReference.toLowerCase().contains(searchTerm),
                    qEbMarchandise.partNumber.toLowerCase().contains(searchTerm),
                    qEbMarchandise.serialNumber.toLowerCase().contains(searchTerm),
                    qEbDemande.numAwbBol.toLowerCase().contains(searchTerm),
                    qEbDemande.libelleOriginCountry.toLowerCase().contains(searchTerm),
                    qEbDemande.customerReference.toLowerCase().contains(searchTerm));
        }

        if (connectedUser != null && connectedUser.getService() != null
            && (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker())) {
            where
                .and(
                    qEbDemande.xEbEtablissement().ebEtablissementNum
                        .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));
        }
        else if (connectedUser != null && connectedUser.isChargeur()) {
            if (connectedUser.isSuperAdmin()) where
                .and(qEbDemande.xEbCompagnie().ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
            else where
                .and(
                    qEbDemande.xEbCompagnie().ebCompagnieNum
                        .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));

            if (connectedUser.isChargeur() && !connectedUser.isSuperAdmin()) {
                BooleanBuilder boolLabels = new BooleanBuilder();

                if (connectedUser.getListCategories() != null) {

                    for (EbCategorie cat: connectedUser.getListCategories()) {

                        if (cat.getLabels() != null) {

                            for (EbLabel label: cat.getLabels()) {
                                boolLabels
                                    .or(
                                        new BooleanBuilder()
                                            .andAnyOf(
                                                qEbDemande.listLabels.startsWith(label.getEbLabelNum() + ","),
                                                qEbDemande.listLabels.contains("," + label.getEbLabelNum() + ","),
                                                qEbDemande.listLabels.endsWith("," + label.getEbLabelNum()),
                                                qEbDemande.listLabels.eq("" + label.getEbLabelNum())));
                            }

                        }

                    }

                }

                if (!boolLabels.hasValue()) {
                    boolLabels.andAnyOf(qEbDemande.listLabels.isNull(), qEbDemande.listLabels.isEmpty());
                }

                where
                    .andAnyOf(
                        qEbDemande.listLabels.isNull(),
                        qEbDemande.listLabels.isEmpty(),
                        qEbDemande.listLabels.isNotNull().andAnyOf(boolLabels));
            }

        }

        if (connectedUser.isControlTower()) where.and(qEbDemande.refTransport.isNotNull());

        if (criterias.getMarchandiseIdNumbers() != null && !criterias.getMarchandiseIdNumbers().isEmpty()) {
            where.and(qEbMarchandise.ebMarchandiseNum.notIn(criterias.getMarchandiseIdNumbers()));
        }

        return query;
    }

    @Override
    public Integer nextValEbQMRootCause() {
        Integer result = null;

        try {
            result = ((BigInteger) em
                .createNativeQuery("SELECT NEXTVAL('work.eb_qm_root_cause_qm_root_cause_num_seq')").getSingleResult())
                    .intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private class OrderColumnDefinition {
        private StringExpression stringOrderWithExpression = qEbQmIncident.incidentRef;
        private NumberExpression<Integer> numberOrderWithExpression = qEbQmIncident.incidentStatus;

        private boolean empty;
        private boolean colIsModeTransport;

        OrderColumnDefinition(String columnName) {
            String fieldType;
            StringExpression fieldColumn = null;
            NumberExpression indexOfCategExpr = null;
            empty = true;

            if (columnName.matches("^category[0-9]+$")) {
                fieldType = "category";
                fieldColumn = qEbQmIncident.listCategoryFlat;
                columnName = columnName.substring(fieldType.length());
                indexOfCategExpr = fieldColumn.indexOf(columnName + ":").add(columnName.length() + 2 + 1);
                empty = false;
            }
            else if (columnName.matches("^field[0-9]+$")) {
                fieldColumn = qEbQmIncident.listCustomFieldsFlat;
                indexOfCategExpr = fieldColumn.indexOf(columnName + "\":").add(columnName.length() + 2 + 2);
                empty = false;
            }
            else if (columnName.contentEquals("modeTransport")) {
                this.colIsModeTransport = true;
                numberOrderWithExpression = new CaseBuilder()
                    .when(
                        new BooleanBuilder()
                            .orAllOf(
                                qEbQmIncident.modeTransport.isNotNull(),
                                qEbQmIncident.modeTransport.eq(Enumeration.ModeTransport.AIR.getCode())))
                    .then(Expressions.ONE)
                    .when(
                        new BooleanBuilder()
                            .orAllOf(
                                qEbQmIncident.modeTransport.isNotNull(),
                                qEbQmIncident.modeTransport.eq(Enumeration.ModeTransport.INTEGRATOR.getCode())))
                    .then(Expressions.TWO)
                    .when(
                        new BooleanBuilder()
                            .orAllOf(
                                qEbQmIncident.modeTransport.isNotNull(),
                                qEbQmIncident.modeTransport.eq(Enumeration.ModeTransport.ROAD.getCode())))
                    .then(Expressions.THREE)
                    .when(
                        new BooleanBuilder()
                            .orAllOf(
                                qEbQmIncident.modeTransport.isNotNull(),
                                qEbQmIncident.modeTransport.eq(Enumeration.ModeTransport.SEA.getCode())))
                    .then(Expressions.FOUR).otherwise(Expressions.nullExpression(Integer.class));
            }

            /*
             * quand on écrie dans querydsl: substring(a, b) cela se traduit par
             * substring(a+1, b-a) dans sql
             * donc on doit diminuer 1 de l'expr de gauche et on ajoute 'a' dans
             * l'expr de droite
             */

            if (!isEmpty()) {
                StringExpression restStringExpr = fieldColumn.substring(indexOfCategExpr);
                NumberExpression endIndexExpr = restStringExpr.indexOf("\"");

                stringOrderWithExpression = new CaseBuilder()
                    .when(new BooleanBuilder().orAllOf(fieldColumn.isNotNull(), fieldColumn.indexOf(columnName).goe(0)))
                    .then(
                        fieldColumn
                            .substring(
                                indexOfCategExpr.subtract(1), // diminuer 1
                                endIndexExpr.add(indexOfCategExpr) // rajouter
                                                                   // l'expr de
                                                                   // gauche 'a'
                            )).otherwise(Expressions.nullExpression());

                stringOrderWithExpression = new CaseBuilder()
                    .when(
                        new BooleanBuilder()
                            .orAllOf(stringOrderWithExpression.isNotNull(), stringOrderWithExpression.isNotEmpty()))
                    .then(stringOrderWithExpression).otherwise(Expressions.nullExpression());
            }

        }

        public boolean isEmpty() {
            return empty;
        }

        public boolean columnIsModeTransport() {
            return colIsModeTransport;
        }

        public NumberExpression<Integer> getNumberOrderWithExpression() {
            return numberOrderWithExpression;
        }

        public StringExpression getStringOrderWithExpression() {
            return stringOrderWithExpression;
        }
    }
}
