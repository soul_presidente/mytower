/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.api.wso.TrackingEventWSO;
import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoCompagnie {
    List<EbEtablissement> selectListEbEtablissementOfCompagnie(SearchCriteria criterias);

    List<EbCompagnie> selectListCompagnie(SearchCriteria criterias);

    EbCompagnie getCarrierCompagnie(TrackingEventWSO event);
}
