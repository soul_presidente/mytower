package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoTypeDocuments;
import com.adias.mytowereasy.model.EbTypeDocuments;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbTypeDocuments;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoTypeDocumentsImpl extends Dao implements DaoTypeDocuments {
    @PersistenceContext
    EntityManager em;

    @Autowired
    private ConnectedUserService connectedUserService;

    QEbTypeDocuments qEbTypeDocuments = QEbTypeDocuments.ebTypeDocuments;

    @SuppressWarnings("unchecked")
    @Override
    public List<EbTypeDocuments> getListEbTypeDocument(SearchCriteria criteria) throws Exception {
        List<EbTypeDocuments> result = new ArrayList<EbTypeDocuments>();
        BooleanBuilder where = new BooleanBuilder();

        try {
            JPAQuery<EbTypeDocuments> query = new JPAQuery<EbTypeDocuments>(em);
            query.distinct();
            query = getGlobalWhere(query, where, criteria, null);
            query.from(qEbTypeDocuments);
            query.where(where);
            query.orderBy(qEbTypeDocuments.ordre.asc());

            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criterias, EbUser user)
        throws Exception {
        EbUser connectedUser = user != null ? user : connectedUserService.getCurrentUser();

        if (StringUtils.isNotBlank(criterias.getSearchterm())) {
            String searchTerm = criterias.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qEbTypeDocuments.code.lower().contains(searchTerm),
                    qEbTypeDocuments.nom.lower().contains(searchTerm));
        }

        if (criterias != null) {

            if (criterias.getListModule() != null && !criterias.getListModule().contains(null)) {
                Predicate[] predicats = new Predicate[criterias.getListModule().size()];

                for (int i = 0; i < criterias.getListModule().size(); i++) {
                    predicats[i] = qEbTypeDocuments.listModuleStr.contains(criterias.getListModule().get(i).toString());
                }

                where.andAnyOf(predicats);
            }

            if (criterias.getModule() != null) {
                where.and(where.and(qEbTypeDocuments.listModuleStr.contains(criterias.getModule().toString())));
            }

        }

        if (criterias.getEbCompagnieNum() != null) where
            .and(qEbTypeDocuments.xEbCompagnie.eq(criterias.getEbCompagnieNum()));
        else {
            where.and(qEbTypeDocuments.xEbCompagnie.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }

        if (BooleanUtils.isNotTrue(criterias.getWithDeactive())) {
            where.and(qEbTypeDocuments.activated.eq(true));
        }

        if (criterias.getTypeDocumentNum() != null) {
            where.and(qEbTypeDocuments.ebTypeDocumentsNum.eq(criterias.getTypeDocumentNum()));
        }

        return query;
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EbTypeDocuments> findByModule(SearchCriteria criteria, EbUser user) throws Exception {
        List<EbTypeDocuments> result = new ArrayList<EbTypeDocuments>();
        BooleanBuilder where = new BooleanBuilder();
        JPAQuery<EbTypeDocuments> query = new JPAQuery<EbTypeDocuments>(em);

        try {
            query.distinct();
            query = getGlobalWhere(query, where, criteria, user);
            query.from(qEbTypeDocuments);
            query.where(where);
            query.orderBy(qEbTypeDocuments.ordre.asc());

            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Long listCount(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbTypeDocuments> query = new JPAQuery<EbTypeDocuments>(em);
            BooleanBuilder where = new BooleanBuilder();

            getGlobalWhere(query, where, criteria, null);
            query.from(qEbTypeDocuments);
            query.distinct().where(where);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
