package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.dto.EbTypeMarchandiseDto;
import com.adias.mytowereasy.model.EbContainerType;
import com.adias.mytowereasy.model.EbTypeUnit;
import com.adias.mytowereasy.utils.search.SearchCriteriaContainerType;
import com.adias.mytowereasy.utils.search.SearchCriteriaTypeUnit;


public interface DaoTypeUnit {
    public List<EbTypeUnit> getListTypeUnit(SearchCriteriaTypeUnit criteria);

    public Long getListTypeUnitCount(SearchCriteriaTypeUnit criteria);

    public List<EbContainerType> getListContainerType(SearchCriteriaContainerType criteria);

    public EbTypeUnit saveTypeUnit(EbTypeUnit typeUnit);

    public boolean deleteTypeUnit(EbTypeUnit typeUnit);

    public Boolean checkIfExistTypeUnitByCompagnie(SearchCriteriaTypeUnit criteria);

    public void activateDeactivateTypeUnit(Long ebTypeUnitNum, boolean activated);

    public List<EbTypeMarchandiseDto>
        checkListTypeMarchandise(List<EbTypeMarchandiseDto> resultList, Integer idDemande);
}
