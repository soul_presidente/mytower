package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoCategorie {
    List<EbCategorie> findActiveCategories(Integer ebEtablissement);

    List<EbLabel> getAllEbLabel(SearchCriteria criteria);

    Long getLabelCount(SearchCriteria criteria);
}
