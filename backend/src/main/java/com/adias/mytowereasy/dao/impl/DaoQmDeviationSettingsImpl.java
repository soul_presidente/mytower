package com.adias.mytowereasy.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoQmDeviationSettings;
import com.adias.mytowereasy.model.EbQmDeviationSettings;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.QEbQmDeviationSettings;
import com.adias.mytowereasy.repository.EbQmDeviationSettingsRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


@Component
@Transactional
public class DaoQmDeviationSettingsImpl implements DaoQmDeviationSettings {
    @PersistenceContext
    EntityManager em;

    @Autowired
    private ConnectedUserService userService;

    @Autowired
    private EbQmDeviationSettingsRepository ebQmDeviationSettingsRepository;

    QEbQmDeviationSettings qEbQmDeviationSettings = QEbQmDeviationSettings.ebQmDeviationSettings;
    QEbCompagnie qEbCompagnie = QEbCompagnie.ebCompagnie;

    @Override
    public List<EbQmDeviationSettings> listDeviation(SearchCriteriaQM criteria) {
        List<EbQmDeviationSettings> result = null;

        try {
            JPAQuery<EbQmDeviationSettings> query = new JPAQuery<EbQmDeviationSettings>(em);
            query.from(qEbQmDeviationSettings);
            EbUser user = userService.getCurrentUserFromDB();
            query.where(qEbQmDeviationSettings.xEbCompagnie.eq(user.getEbCompagnie().getEbCompagnieNum()));
            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Long getListEbQmDeviationSettingsCount(SearchCriteriaQM criteria) {
        // TODO Auto-generated method stub
        Long result = new Long(0);

        try {
            JPAQuery<EbQmDeviationSettings> query = new JPAQuery<EbQmDeviationSettings>(em);
            query.from(qEbQmDeviationSettings);
            EbUser user = userService.getCurrentUserFromDB();
            query.where(qEbQmDeviationSettings.xEbCompagnie.eq(user.getEbCompagnie().getEbCompagnieNum()));
            List<EbQmDeviationSettings> resultat = query.fetch();
            result = Long.valueOf(resultat.size());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
