/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbInvoice;
import com.adias.mytowereasy.utils.search.SearchCriteriaFreightAudit;


public interface DaoInvoice {
    Integer nextValEbInvoice();

    List<EbInvoice> getListEbInvoice(SearchCriteriaFreightAudit criterias);

    Long getCountListEbInvoice(SearchCriteriaFreightAudit criteria);
}
