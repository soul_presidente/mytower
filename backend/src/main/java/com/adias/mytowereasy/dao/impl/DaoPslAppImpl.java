package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoPslApp;
import com.adias.mytowereasy.model.QEbDemande;
import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.model.tt.QEbTTPslApp;
import com.adias.mytowereasy.model.tt.QEbTtTracing;
import com.adias.mytowereasy.service.ConnectedUserService;


@Component
@Transactional
public class DaoPslAppImpl implements DaoPslApp {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbTTPslApp qEbTTPslApp = QEbTTPslApp.ebTTPslApp;

    QEbTtTracing qEbTrackTrace = QEbTtTracing.ebTtTracing;
    QEbDemande qEbDemande = QEbDemande.ebDemande;

    @Override
    public List<EbTTPslApp>
        getListPslAppByRefAndCodeAlpha(List<String> listRef, List<String> listcodeAlpha, Boolean isTransportRef) {
        List<EbTTPslApp> resultat = new ArrayList<EbTTPslApp>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbTTPslApp> query = new JPAQuery<EbTTPslApp>(em);

            query
                .select(
                    QEbTTPslApp
                        .create(
                            qEbTTPslApp.ebTtPslAppNum,
                            qEbTTPslApp.companyPsl(),
                            qEbTrackTrace.ebTtTracingNum,
                            qEbTTPslApp.codeAlpha,
                            qEbTrackTrace.codeAlphaPslCourant,
                            qEbDemande.ebDemandeNum,
                            qEbTrackTrace.configPsl,
                            qEbTrackTrace.customerReference,
                            qEbTrackTrace.refTransport,
                            qEbTrackTrace.customRef));

            query.distinct();
            query.from(qEbTTPslApp);

            query.innerJoin(qEbTTPslApp.xEbTrackTrace(), qEbTrackTrace);
            query.innerJoin(qEbTrackTrace.xEbDemande(), qEbDemande);
            query = getGlobalWhere(query, where, listRef, listcodeAlpha, isTransportRef, null);
            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    private JPAQuery getGlobalWhere(
        JPAQuery query,
        BooleanBuilder where,
        List<String> listRef,
        List<String> listcodeAlpha,
        Boolean isTransportRef,
        List<String> listRefUnit) {
        final Integer conditionNbr = listRef.size();

        Predicate[] predicats = new Predicate[conditionNbr];
        BooleanExpression exp = null;

        for (int i = 0; i < conditionNbr; i++) {

            if (isTransportRef) {
                exp = qEbTrackTrace.refTransport.eq(listRef.get(i)).and(qEbTTPslApp.codeAlpha.eq(listcodeAlpha.get(i)));
            }
            else {
                exp = qEbTrackTrace.customRef.eq(listRef.get(i)).and(qEbTTPslApp.codeAlpha.eq(listcodeAlpha.get(i)));
            }

            if (listRefUnit != null) {
                exp.and(qEbTrackTrace.customerReference.eq(listRefUnit.get(i)));
            }

            predicats[i] = exp;
        }

        where.andAnyOf(predicats);

        query.where(where);

        return query;
    }

    @Override
    public List<EbTTPslApp> getListPslAppByRefTransportAndRefUnitAndCodeAlpha(
        List<String> listRefTransport,
        List<String> listcodeAlpha,
        Boolean isTransportRef,
        List<String> listRefUnit) {
        List<EbTTPslApp> resultat = new ArrayList<EbTTPslApp>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbTTPslApp> query = new JPAQuery<EbTTPslApp>(em);

            query
                .select(
                    QEbTTPslApp
                        .create(
                            qEbTTPslApp.ebTtPslAppNum,
                            qEbTTPslApp.companyPsl(),
                            qEbTrackTrace.ebTtTracingNum,
                            qEbTTPslApp.codeAlpha,
                            qEbTrackTrace.codeAlphaPslCourant,
                            qEbDemande.ebDemandeNum,
                            qEbTrackTrace.configPsl,
                            qEbTrackTrace.customerReference,
                            qEbTrackTrace.refTransport,
                            qEbTrackTrace.customRef));

            query.distinct();
            query.from(qEbTTPslApp);

            query.innerJoin(qEbTTPslApp.xEbTrackTrace(), qEbTrackTrace);
            query.innerJoin(qEbTrackTrace.xEbDemande(), qEbDemande);
            query = getGlobalWhere(query, where, listRefTransport, listcodeAlpha, isTransportRef, listRefUnit);
            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }
}
