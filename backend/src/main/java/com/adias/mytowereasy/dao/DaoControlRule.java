package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbControlRule;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.tt.EbTTPslApp;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoControlRule {
    List<EbControlRule> getListEbControlRule(SearchCriteria criteria) throws Exception;

    public Long listCount(SearchCriteria criteria);

    public List<EbTTPslApp> getListPslWithRule(EbControlRule ebControlRule, Integer ebTtTracingNum);

    List<EbDemande> getListDemandeWithRule(EbControlRule rule, Integer ebDemandeNum, EbUser connectedUser);
}
