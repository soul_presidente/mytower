package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Expression;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Predicate;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoAdresse;
import com.adias.mytowereasy.dock.model.QEbEntrepot;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.trpl.model.QEbPlZone;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoAdresseImpl implements DaoAdresse {
    @PersistenceContext
    EntityManager em;

    QEbAdresse qEbAdresse = new QEbAdresse("" + "qEbAdresse");
    QEbEtablissement qEtablissement = new QEbEtablissement("etablissement");
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qCompagnie");
    QEbEntrepot qEbEntrepot = new QEbEntrepot("qEbEntrepot");
    QEcCountry qCountry = new QEcCountry("qCountry");
    QEbUser qEbUser = new QEbUser("qEbUser");

    private static Map<String, Object> orderHashMap = new HashMap<>();

    private void initColumnsOrderMap() {
        orderHashMap.put("reference", qEbAdresse.reference);
        orderHashMap.put("company", qEbAdresse.company);
        orderHashMap.put("street", qEbAdresse.street);
        orderHashMap.put("city", qEbAdresse.city);
        orderHashMap.put("zipCode", qEbAdresse.zipCode);
        orderHashMap.put("state", qEbAdresse.state);
        orderHashMap.put("airport", qEbAdresse.airport);
        orderHashMap.put("phone", qEbAdresse.phone);
        orderHashMap.put("openingHoursFreeText", qEbAdresse.openingHoursFreeText);
        orderHashMap.put("email", qEbAdresse.email);
        orderHashMap.put("country", qCountry.libelle);
        orderHashMap.put("xEbUserVisibility", qEbUser.nom);
        orderHashMap.put("entrepot", qEbEntrepot.libelle);
    }

    @Override
    public List<EbAdresse> getListEbAdresse(SearchCriteria criteria) {
        List<EbAdresse> resultat = new ArrayList<EbAdresse>();
        initColumnsOrderMap();
        QEbPlZone qEbZone = new QEbPlZone("qEbZone");

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbAdresse> query = new JPAQuery<EbAdresse>(em);

            query
                .select(
                    QEbAdresse
                        .create(
                            qEbAdresse.airport,
                            qEbAdresse.city,
                            qEbAdresse.commentaire,
                            qEbAdresse.company,
                            qEbAdresse.dateAjout,
                            qEbAdresse.ebAdresseNum,
                            qEbAdresse.email,
                            // qEbAdresse.openingHours,
                            qEbAdresse.openingHoursFreeText,
                            qEbAdresse.phone,
                            qEbAdresse.reference,
                            qEbAdresse.street,
                            qEbAdresse.type,
                            qEbAdresse.zipCode,
                            qEbAdresse.listEgibility,
                            qEbAdresse.etablissementAdresse,
                            qCountry.ecCountryNum,
                            qCountry.code,
                            qCountry.codeLibelle,
                            qCountry.libelle,
                            qEbCompagnie.ebCompagnieNum,
                            qEbCompagnie.nom,
                            qEbCompagnie.code,
                            qEbUser.ebUserNum,
                            qEbUser.nom,
                            qEtablissement.ebEtablissementNum,
                            qEtablissement.nom,
                            qEbAdresse.zones,
                            qEbZone.ebZoneNum,
                            qEbZone.ref,
                            qEbZone.designation,
                            qEbAdresse.state,
                            qEbAdresse.listEtablissementsNum,
                            qEbAdresse.listZonesNum,
                            qEbEntrepot.ebEntrepotNum,
                            qEbEntrepot.libelle));

            query.distinct();

            if (criteria.getCompanyName() != null) {
                where.and(qEbAdresse.company.containsIgnoreCase(criteria.getCompanyName()));
            }

            // Ajout des predicats
            buildGlobalSearchCriteria(criteria, where);

            query.from(qEbAdresse).where(where);

            query.leftJoin(qEbAdresse.xEbUserVisibility(), qEbUser);
            query.leftJoin(qEbUser.ebEtablissement(), qEtablissement);
            query.leftJoin(qEbAdresse.ebCompagnie(), qEbCompagnie);
            query.leftJoin(qEbAdresse.country(), qCountry);
            query.leftJoin(qEbAdresse.defaultZone(), qEbZone);
            query.leftJoin(qEbAdresse.entrepot(), qEbEntrepot);
            query = setPagination(criteria, query);
            resultat = (List<EbAdresse>) query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    private void buildGlobalSearchCriteria(SearchCriteria criteria, BooleanBuilder where) {

        if (criteria.getEbCompagnieNum() != null) {
            where.and(qEbCompagnie.ebCompagnieNum.eq(criteria.getEbCompagnieNum()));
        }

        if (criteria.getEbEtablissementNum() != null) {
            where.and(qEbAdresse.listEtablissementsNum.contains(":" + criteria.getEbEtablissementNum() + ":"));
        }

        if (criteria.getAdresseEligibilities() != null) {
            Predicate[] eligibilityPredicates = new Predicate[criteria.getAdresseEligibilities().size()];

            for (int elIndex = 0; elIndex < criteria.getAdresseEligibilities().size(); elIndex++) {
                eligibilityPredicates[elIndex] = qEbAdresse.listEgibility
                    .contains(criteria.getAdresseEligibilities().get(elIndex));
            }

            where.andAnyOf(eligibilityPredicates);
        }

        if (criteria.getSearchterm() != null && !criteria.getSearchterm().trim().isEmpty()) {
            String[] listTerm = criteria.getSearchterm().trim().split("\\s+");

            Predicate[] predicats = new Predicate[listTerm.length * 7];
            int j = 0;

            for (int i = 0; i < listTerm.length; i++) {
                predicats[j + i] = qEbAdresse.company.trim().likeIgnoreCase("%" + listTerm[i] + "%");
                predicats[j + i + 1] = qEbAdresse.reference.trim().likeIgnoreCase("%" + listTerm[i] + "%");

                predicats[j + i + 2] = qEbAdresse.city.trim().likeIgnoreCase("%" + listTerm[i] + "%");
                predicats[j + i + 3] = qEbAdresse.zipCode.trim().likeIgnoreCase("%" + listTerm[i] + "%");
                predicats[j + i + 4] = qEbAdresse.street.trim().likeIgnoreCase("%" + listTerm[i] + "%");
                predicats[j + i + 5] = qCountry.code.trim().likeIgnoreCase("%" + listTerm[i] + "%");
                predicats[j + i + 6] = qCountry.libelle.trim().likeIgnoreCase("%" + listTerm[i] + "%");
                j += 6;
            }

            where.andAnyOf(predicats);
        }

    }

    private JPAQuery<EbAdresse> setPagination(SearchCriteria criteria, JPAQuery<EbAdresse> query) {
        String columnName = criteria.getOrderedColumn();

        if (columnName != null) {
            query
                .orderBy(
                    new OrderSpecifier(
                        criteria.getAscendant() ? Order.ASC : Order.DESC,
                        (Expression) orderHashMap.get(columnName)));
        }
        else {
            query.orderBy(qEbAdresse.ebAdresseNum.desc());
        }

        if (criteria.getSize() != null && criteria.getSize() >= 0) {
            query.limit(criteria.getSize());
        }

        if (criteria.getPageNumber() != null) {
            query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
        }

        return query;
    }

    @Override
    public Long getListAddressesCount(SearchCriteria criteria) {
        Long result = new Long(0);

        QEcCountry qCountry = new QEcCountry("qCountry");
        QEbCompagnie qEbCompagnie = new QEbCompagnie("qCompagnie");

        try {
            JPAQuery<EbAdresse> query = new JPAQuery<EbAdresse>(em);
            BooleanBuilder where = new BooleanBuilder();

            buildGlobalSearchCriteria(criteria, where);

            query.from(qEbAdresse).where(where);
            query = getAddressGlobalJoin(query, where, criteria);
            query = setPagination(criteria, query);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private JPAQuery getAddressGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        QEcCountry qCountry = new QEcCountry("qCountry");
        query.leftJoin(qEbAdresse.ebCompagnie(), qEbCompagnie);
        query.leftJoin(qEbAdresse.country(), qCountry);

        return query;
    }
}
