package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbTypeFlux;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoTypeFlux {
    List<EbTypeFlux> getListTypeFlux(SearchCriteria criteria);

    Long getListTypeFluxCount(SearchCriteria criteria);
}
