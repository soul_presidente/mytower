package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbCompagnieCurrency;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoCurrency {
    List<EbCompagnieCurrency> selectListEbCompagnieCurrencyAndEbCompagnieGuest(SearchCriteria criterias);

    List<EbCompagnieCurrency> selectListEbCompagnieCurrency(SearchCriteria criterias);

    List<EbCompagnieCurrency> getListNonComplaiantCompagniecurrency(EbCompagnieCurrency compagnieCurrency);

    Long getListEbCompagnieCurrencyCount(SearchCriteria criteria);
}
