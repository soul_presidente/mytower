package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.PrMtcTransporteur;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoPrMtcTransporteur {
    public List<PrMtcTransporteur> getListPrMtcTransporteur(SearchCriteria criteria);

    public Long getCountPrMtcTransporteur(SearchCriteria criteria);

    public PrMtcTransporteur savePrMtcTransporteur(PrMtcTransporteur prMtcTransporteur);

    public int getCountListMtgTransporteur(SearchCriteria criteria);
}
