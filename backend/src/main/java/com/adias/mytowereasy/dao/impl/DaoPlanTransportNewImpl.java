package com.adias.mytowereasy.dao.impl;

import java.util.*;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.api.wso.PlanTransportWSO;
import com.adias.mytowereasy.chanel.model.EbWeekTemplate;
import com.adias.mytowereasy.chanel.model.QEbWeekTemplate;
import com.adias.mytowereasy.dao.DaoPlanTransportNew;
import com.adias.mytowereasy.dao.DaoPricing;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.repository.*;
import com.adias.mytowereasy.service.TypeRequestService;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


@Component
@Transactional
public class DaoPlanTransportNewImpl implements DaoPlanTransportNew {
    @PersistenceContext
    EntityManager em;
    @Autowired
    private EcCountryRepository ecCountryRepository;
    @Autowired
    private DaoPricing daoPricing;
    @Autowired
    protected TypeRequestService typeRequestService;
    @Autowired
    EbPlanTransportNewRepository ebPlanTransportNewRepository;
    @Autowired
    EbUserRepository ebUserRepository;
    @Autowired
    EbCategorieRepository ebCategorieRepository;

    @Autowired
    EbCompagnieRepository ebCompagnieRepository;
    QEbPlanTransportNew qEbPlanTransportNew = com.adias.mytowereasy.model.QEbPlanTransportNew.ebPlanTransportNew;
    QEbCompagnie qEbCompagnie = QEbCompagnie.ebCompagnie;
    QEbCompagnie qEbCompagnieChargeur = QEbCompagnie.ebCompagnie;
    QEbWeekTemplate qEbWeekTemplate = QEbWeekTemplate.ebWeekTemplate;

    @Override
    public List<EbPlanTransportNew> list(SearchCriteria criteria) {
        List<EbPlanTransportNew> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbPlanTransportNew> query = new JPAQuery<>(em);
            query
                .select(
                    QEbPlanTransportNew
                        .create(
                            qEbPlanTransportNew.ebPlanTransportNum,
                            qEbPlanTransportNew.reference,
                            qEbPlanTransportNew.label,
                            qEbCompagnie.ebCompagnieNum,
                            qEbCompagnie.code,
                            qEbCompagnie.nom,
                            qEbCompagnieChargeur.ebCompagnieNum,
                            qEbCompagnieChargeur.code,
                            qEbCompagnieChargeur.nom,
                            qEbPlanTransportNew.input,
                            qEbPlanTransportNew.weekTemplate(),
                            qEbPlanTransportNew.listCategories));

            query.from(qEbPlanTransportNew);
            defineQueryGlobals(query, where, criteria, true);
            query.distinct().where(where);
            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    public List<EbPlanTransportNew>
        listLoadPlanIsConstraints(SearchCriteriaPricingBooking criteria, PlanTransportWSO planTransportWSO) {
        List<EbPlanTransportNew> resultat = new ArrayList<>();

        try {
            StringBuilder requet = new StringBuilder();
            String select = "select ptn.eb_plan_transport_num, ptn.deleted, ptn.label, cast(ptn.input as text),  ptn.reference,\r\n"
                + "			ptn.x_eb_compagnie_carrier, cast(ptn.week as text), cast(ptn.week_template as text)\r\n"
                + "			FROM work.eb_plan_transport_new as ptn";
            StringBuilder selectConditions = new StringBuilder();

            if (criteria.getListIncoterm() != null) {
                String requeteSQL = "(select (input ->>'listIncoterm')\\:\\:jsonb @> '"
                    + getString(criteria.getListIncoterm()) + "'\\:\\:jsonb)";
                selectConditions.append(requeteSQL);
            }

            if (criteria.getListOrigins() != null) {
                if (!selectConditions.toString().equals("")) selectConditions.append(" and ");
                String requeteSQL = "(select (input ->>'listOrigins')\\:\\:jsonb @> '" + criteria.getListOrigins()
                    + "'\\:\\:jsonb)";
                selectConditions.append(requeteSQL);
            }

            if (criteria.getListDestinations() != null) {
                if (!selectConditions.toString().equals("")) selectConditions.append(" and ");
                String requeteSQL = "(select (input ->>'listDestinations')\\:\\:jsonb @> '"
                    + criteria.getListDestinations() + "'\\:\\:jsonb)";
                selectConditions.append(requeteSQL);
            }

            if (criteria.getListOriginCity() != null) {
                if (!selectConditions.toString().equals("")) selectConditions.append(" and ");
                String requeteSQL = "(select (input ->>'listOriginCity')\\:\\:jsonb @> '"
                    + getString(criteria.getListOriginCity()) + "'\\:\\:jsonb)";
                selectConditions.append(requeteSQL);
            }

            if (criteria.getListDestinationCity() != null) {
                if (!selectConditions.toString().equals("")) selectConditions.append(" and ");
                String requeteSQL = "(select (input ->>'listDestinationCity')\\:\\:jsonb @> '"
                    + getString(criteria.getListDestinationCity()) + "'\\:\\:jsonb)";
                selectConditions.append(requeteSQL);
            }

            if (criteria.getListModeTransport() != null) {
                if (!selectConditions.toString().equals("")) selectConditions.append(" and ");
                String requeteSQL = "(select (input ->>'listModeTransport')\\:\\:jsonb @> '"
                    + criteria.getListModeTransport() + "'\\:\\:jsonb)";
                selectConditions.append(requeteSQL);
            }

            /**
             * next pickup date takes into account the totalWeight, totalVolume,
             * taxableWeight.
             */
            if (planTransportWSO.getTotalWeight() != null) {
                if (!selectConditions.toString().equals("")) selectConditions.append(" and ");
                String requeteSQL = "(select coalesce( case when (CAST(nullif(input ->>'maxWeight' , '0') AS float)) IS NULL then true else CAST(nullif(input ->>'maxWeight' , '0') AS float)  > "
                    + planTransportWSO.getTotalWeight() + "  end))";

                selectConditions.append(requeteSQL);
            }

            if (planTransportWSO.getTotalVolume() != null) {
                if (!selectConditions.toString().equals("")) selectConditions.append(" and ");
                String requeteSQL = "(select coalesce( case when (CAST(nullif(input ->>'maxVolume' , '0') AS float)) IS NULL then true else CAST(nullif(input ->>'maxVolume' , '0') AS float)  > "
                    + planTransportWSO.getTotalVolume() + "  end))";

                selectConditions.append(requeteSQL);
            }

            if (planTransportWSO.getTaxableWeight() != null) {
                if (!selectConditions.toString().equals("")) selectConditions.append(" and ");
                String requeteSQL = "(select coalesce( case when (CAST(nullif(input ->>'maxTaxableWeight' , '0') AS float)) IS NULL then true else CAST(nullif(input ->>'maxTaxableWeight' , '0') AS float)  > "
                    + planTransportWSO.getTaxableWeight() + "  end))";

                selectConditions.append(requeteSQL);
            }

            /**
             * next pickup date takes into account the DGR
             */

            if (criteria.getDangerousGood() != null && !criteria.getDangerousGood().isEmpty()) {
                List<Integer> dangrousGoods = criteria
                    .getDangerousGood().stream().distinct().collect(Collectors.toList());
                if (!selectConditions.toString().equals("")) selectConditions.append(" and ");
                String requeteDg = "(select coalesce( case when (input ->>'dangerousGood') IS NULL then true else '"
                    + dangrousGoods + "'\\:\\:jsonb <@ (input ->>'dangerousGood')\\:\\:jsonb  end))";

                selectConditions.append(requeteDg);
            }

            /**
             * next pickup date takes into account the ClassGood.
             */
            if (criteria.getClassGoods() != null && !criteria.getClassGoods().isEmpty()) {
                List<Double> classGoods = criteria.getClassGoods().stream().distinct().collect(Collectors.toList());
                if (!selectConditions.toString().equals("")) selectConditions.append(" and ");
                String reqClassGood = "(select coalesce( case when (input ->>'classGoods') IS NULL then true else (input ->>'classGoods')\\:\\:jsonb @> '"
                    + "" + classGoods + "'\\:\\:jsonb end))";
                selectConditions.append(reqClassGood);
            }

            /**
             * next pickup date takes into account the un.
             */

            if (criteria.getUns() != null) {
                if (!selectConditions.toString().equals("")) selectConditions.append(" and ");
                String reqUN = "(select coalesce( case when (input ->>'uns') IS NULL then true else (input ->>'uns')\\:\\:jsonb @> '"
                    + "" + criteria.getUns() + "'\\:\\:jsonb end))";

                selectConditions.append(reqUN);
            }

            requet.append(select).append(" where ").append(selectConditions.toString());
            Query query = em.createNativeQuery(requet.toString());
            List<Object[]> resultSet = query.getResultList();

            ObjectMapper mapper = new ObjectMapper();
			EbCompagnie ebCompagnieCarrier = null;
            for (Object[] data: resultSet) {
                EbPlanTransportNew ptn = new EbPlanTransportNew();

                ptn.setEbPlanTransportNum((Integer) data[0]);

                ptn.setDeleted((boolean) (data[1] != null ? data[1] : false));

                ptn.setLabel((String) (data[2] != null ? data[2] : ""));

                /*if (data[3] != null) {
                    SearchCriteriaPricingBooking input;

                    try {
                        input = mapper.readValue((String) data[3], SearchCriteriaPricingBooking.class);
                        ptn.setInput(input);
                    } catch (JsonProcessingException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                }*/


                ptn.setReference((String) (data[4] != null ? data[4] : ""));

				if (ebCompagnieCarrier == null && data[5] != null) {
					ebCompagnieCarrier = ebCompagnieRepository.findOneByEbCompagnieNum((Integer) data[5]);
                    ptn.setxEbCompagnieCarrier(ebCompagnieCarrier);
                }

                if (data[7] != null) {
                    EbWeekTemplate weekTemplate;

                    try {
                        weekTemplate = mapper.readValue((String) data[7], EbWeekTemplate.class);
                        ptn.setWeekTemplate(weekTemplate);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }

                }

                resultat.add(ptn);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    private String getString(List<String> objects) {
        StringBuilder builder = new StringBuilder();
        builder.append("[");

        for (int i = 0; i < objects.size(); i++) {

            if (objects.get(i) != null) {
                String str = objects.get(i).replaceAll("'", "''");
                if (i < objects.size() - 1
                    && objects.size() > 1) builder.append("\"").append(str.toUpperCase()).append("\",");
                else builder.append("\"").append(str.toUpperCase()).append("\"");
            }

        }

        builder.append("]");

        return builder.toString();
    }

    @Override
    public Long listCount(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbPlanTransportNew> query = new JPAQuery<EbPlanTransportNew>(em);
            BooleanBuilder where = new BooleanBuilder();
            query.distinct().from(qEbPlanTransportNew);
            defineQueryGlobals(query, where, criteria, false);
            query.where(where);
            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    private void
        defineQueryGlobals(JPAQuery query, BooleanBuilder where, SearchCriteria criteria, boolean enablePagination) {
        // Join
        query.leftJoin(qEbPlanTransportNew.xEbCompagnieChargeur(), qEbCompagnieChargeur);
        query.leftJoin(qEbPlanTransportNew.xEbCompagnieCarrier(), qEbCompagnie);

        EbUser connectedUser = ebUserRepository.findOneByEbUserNum(criteria.getConnectedUserNum());
        EbCompagnie compagnie = ebCompagnieRepository.selectCompagnieByEbUserNum(criteria.getConnectedUserNum());
        connectedUser.setEbCompagnie(compagnie);

        // Global Where
        where.andAnyOf(qEbPlanTransportNew.deleted.isNull(), qEbPlanTransportNew.deleted.isFalse());

        if (criteria.getCompanyNums() == null || criteria.getCompanyNums().size() == 0) {
            where.and(qEbCompagnieChargeur.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }
        else {
            where.and(qEbCompagnieChargeur.ebCompagnieNum.in(criteria.getCompanyNums()));
        }

        if (StringUtils.isNotBlank(criteria.getSearchterm())) {
            // the SearchCriteria input field properties with text values
            List<String> paramsToSearchBy = Arrays
                .asList(
                    "listIncoterm",
                    "requestedDeliveryDateFrom",
                    "requestedDeliveryDateTo",
                    "listRefCustomerDelivery",
                    "listReferenceDelivery",
                    "listCustomerReference",
                    "acknoledgeByCtDate",
                    "listUnitReference",
                    "carrierUniqRefNum",
                    "listCampaignName",
                    "listCampaignCode",
                    "listNumOrderSAP",
                    "listNumOrderEDI",
                    "listCostCenter",
                    "datePickUpFrom",
                    "companyOrigin",
                    "datePickUpTo",
                    "listLot",
                    "listCategorie",
                    "listTransportRef",
                    "listNumOrderCustomer",
                    "listNameCustomerDelivery",
                    "dateCreationFrom",
                    "dateCreationTo",
                    "maxHeight",
                    "maxLength",
                    "maxLength",
                    "maxWidth",
                    "unitNbr",
                    "classGood");

            List<Integer> searchedByTextIds = mapPlanTansportListToIds(
                searchByJsonInputTextFields(paramsToSearchBy, criteria.getSearchterm()));

            List<Integer> searchedByOriginIds = mapPlanTansportListToIds(
                searchByJsonInputOriginField(criteria.getSearchterm()));
            List<Integer> searchedByDestinationIds = mapPlanTansportListToIds(
                searchByJsonInputDestinationField(criteria.getSearchterm()));
            List<Integer> searchedByModTransportIds = mapPlanTansportListToIds(
                searchByJsonInputModeTransportField(criteria.getSearchterm()));
            List<Integer> searchedByCategorieIds = mapPlanTansportListToIds(
                searchByJsonInputCategorieField(criteria.getSearchterm()));
            List<Integer> searchedByCarrierIds = mapPlanTansportListToIds(
                searchByJsonInputCarrierField(criteria.getSearchterm()));

            List<Integer> searchedByServiceLevelIds = mapPlanTansportListToIds(
                searchByJsonInputServiceLevelField(criteria.getSearchterm()));

            String searchTerm = criteria.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qEbPlanTransportNew.reference.lower().contains(searchTerm),
                    qEbPlanTransportNew.label.lower().contains(searchTerm),
                    qEbPlanTransportNew.ebPlanTransportNum.in(searchedByTextIds),
                    qEbPlanTransportNew.ebPlanTransportNum.in(searchedByOriginIds),
                    qEbPlanTransportNew.ebPlanTransportNum.in(searchedByDestinationIds),
                    qEbPlanTransportNew.ebPlanTransportNum.in(searchedByModTransportIds),
                    qEbPlanTransportNew.ebPlanTransportNum.in(searchedByCarrierIds),
                    qEbPlanTransportNew.ebPlanTransportNum.in(searchedByServiceLevelIds),
                    qEbPlanTransportNew.ebPlanTransportNum.in(searchedByCategorieIds));
        }

        // Order
        if (criteria.getOrderedColumn() != null) {
            Path<Object> fieldPath = Expressions.path(Object.class, qEbPlanTransportNew, criteria.getOrderedColumn());
            query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
        }
        else {
            query.orderBy(qEbPlanTransportNew.label.asc());
        }

        // Pagination
        if (enablePagination) {

            if (criteria.getPageNumber() != null && criteria.getSize() != null) {
                query
                    .limit(criteria.getSize())
                    .offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

        }

    }

    public List<Integer> mapPlanTansportListToIds(Collection<EbPlanTransportNew> planTransportList) {
        return planTransportList.stream().map(d -> d.getEbPlanTransportNum()).collect(Collectors.toList());
    }

    public Set<EbPlanTransportNew> searchByJsonInputTextFields(List<String> jsonFieldsList, String searchedValue) {
        Set<EbPlanTransportNew> totalSearchedPlans = new HashSet<EbPlanTransportNew>();

        for (String param: jsonFieldsList) {
            List<EbPlanTransportNew> searchedPlans = ebPlanTransportNewRepository
                .selectListEbDemandeByInputStringValues(param, searchedValue);
            totalSearchedPlans.addAll(searchedPlans);
        }

        return totalSearchedPlans;
    }

    public List<EbPlanTransportNew> searchByJsonInputOriginField(String searchedValue) {
        List<Integer> countryIds = ecCountryRepository
            .findAllByLibelleLike(searchedValue).stream().map(country -> country.getEcCountryNum())
            .collect(Collectors.toList());

        String countriesString = convertIntegerListToSqlList(countryIds);

        return ebPlanTransportNewRepository.selectListEbDemandeByInputReferenceValues("listOrigins", countriesString);
    }

    public List<EbPlanTransportNew> searchByJsonInputDestinationField(String searchedValue) {
        List<Integer> countryIds = ecCountryRepository
            .findAllByLibelleLike(searchedValue).stream().map(country -> country.getEcCountryNum())
            .collect(Collectors.toList());

        String countriesString = convertIntegerListToSqlList(countryIds);

        return ebPlanTransportNewRepository
            .selectListEbDemandeByInputReferenceValues("listDestinations", countriesString);
    }

    public List<EbPlanTransportNew> searchByJsonInputCategorieField(String searchedValue) {
        List<Integer> categoryIds = ebCategorieRepository
            .findAllByLibelleLike(searchedValue).stream().map(category -> category.getEbCategorieNum())
            .collect(Collectors.toList());

        String categoriesString = convertIntegerListToSqlList(categoryIds);

        return ebPlanTransportNewRepository
            .selectListEbDemandeByInputReferenceValues("listCategorie", categoriesString);
    }

    public List<EbPlanTransportNew> searchByJsonInputModeTransportField(String searchedValue) {
        List<Integer> modeTransportValues = Enumeration.ModeTransport.getListCodeByLibelle(searchedValue);

        String modeTransportString = convertIntegerListToSqlList(modeTransportValues);

        return ebPlanTransportNewRepository
            .selectListEbDemandeByInputReferenceValues("listModeTransport", modeTransportString);
    }

    public String convertIntegerListToSqlList(List<Integer> list) {
        // [80]|[80,|, 80,|, 80]
        List<String> stringList = list
            .stream()
            .map(
                element -> "\\[" + element + "\\]|\\[" + element + ",%|%,\\s" + element + ",%|%,\\s" + element + "\\]|")// \[81\]|\[81\,%|%\,\s81\,%|%\,\s81\]
            .collect(Collectors.toList());
        String resultString = Arrays.toString(stringList.toArray());
        resultString = resultString.substring(1, resultString.length() - 1);

        if (list.size() > 0) {
            resultString = resultString.substring(0, resultString.length() - 1);
            resultString = resultString.replace(",", "\\,").replace(" ", "\\s");
            resultString = "%(" + resultString + ")%";
        }

        return resultString;
    }

    public List<EbPlanTransportNew> searchByJsonInputServiceLevelField(String searchedValue) {
        List<Integer> listTypeRequests = typeRequestService
            .searchTypeRequestByReference(searchedValue).stream().map(type -> type.getEbTypeRequestNum())
            .collect(Collectors.toList());

        String transporteursString = convertIntegerListToSqlList(listTypeRequests);

        return ebPlanTransportNewRepository
            .selectListEbDemandeByInputReferenceValues("listTypeDemande", transporteursString);
    }

    public List<EbPlanTransportNew> searchByJsonInputCarrierField(String searchedValue) {
        List<Integer> listEbTransporteur = daoPricing
            .getListCarrier(searchedValue).stream().map(user -> user.getEbUserNum()).collect(Collectors.toList());

        String transporteursString = convertIntegerListToSqlList(listEbTransporteur);

        return ebPlanTransportNewRepository
            .selectListEbDemandeByInputReferenceValues("listCarrier", transporteursString);
    }
}
