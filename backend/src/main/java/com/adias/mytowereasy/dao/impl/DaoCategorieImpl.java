package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoCategorie;
import com.adias.mytowereasy.model.EbCategorie;
import com.adias.mytowereasy.model.EbLabel;
import com.adias.mytowereasy.model.QEbCategorie;
import com.adias.mytowereasy.model.QEbLabel;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoCategorieImpl implements DaoCategorie {
    @PersistenceContext
    EntityManager em;

    QEbCategorie qEbCategorie = QEbCategorie.ebCategorie;

    QEbLabel qEbLabel = QEbLabel.ebLabel;

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbCategorie> findActiveCategories(Integer ebCompagnie) {
        List<EbCategorie> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPQLQuery query = new JPAQuery(em);

            where.and(qEbCategorie.compagnie().ebCompagnieNum.eq(ebCompagnie));
            where.andAnyOf(qEbCategorie.deleted.isFalse(), qEbCategorie.deleted.isNull());

            query.distinct();
            query.select(qEbCategorie);
            query.from(qEbCategorie);
            query.where(where);

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public List<EbLabel> getAllEbLabel(SearchCriteria criteria) {
        List<EbLabel> result = new ArrayList<EbLabel>();

        try {
            JPAQuery<EbLabel> query = new JPAQuery<EbLabel>(em);
            query.from(qEbLabel).where(qEbLabel.categorie().ebCategorieNum.eq(criteria.getEbCategorieNum()));

            if (criteria.getSearchterm() != null) {
                query
                    .where(
                        qEbLabel.libelle.stringValue().toLowerCase().contains(criteria.getSearchterm().toLowerCase()));
            }

            query.where(qEbLabel.deleted.isFalse());

            query
                .limit(criteria.getSize())
                .offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Long getLabelCount(SearchCriteria criteria) {
        Long result = 0l;

        try {
            JPAQuery<EbLabel> query = new JPAQuery<EbLabel>(em);
            query.from(qEbLabel).where(qEbLabel.categorie().ebCategorieNum.eq(criteria.getEbCategorieNum()));

            if (criteria.getSearchterm() != null) {
                query
                    .where(
                        qEbLabel.libelle.stringValue().toLowerCase().contains(criteria.getSearchterm().toLowerCase()));
            }

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
