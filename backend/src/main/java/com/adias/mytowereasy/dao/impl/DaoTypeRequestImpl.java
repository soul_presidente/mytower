package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoTypeRequest;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbTypeRequestRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoTypeRequestImpl implements DaoTypeRequest {
    @PersistenceContext
    EntityManager em;

    @Autowired
    EbTypeRequestRepository ebTypeRequestRepository;
    @Autowired
    ConnectedUserService connectedUserService;

    QEbTypeRequest qEbTypeRequest = QEbTypeRequest.ebTypeRequest;
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbCompagnie");
    QEbUser qEbUser = new QEbUser("qEbUser");

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbTypeRequest> searchEbTypeRequest(SearchCriteria criteria) {
        List<EbTypeRequest> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbTypeRequest> query = new JPAQuery<>(em);

            query
                .select(
                    QEbTypeRequest
                        .create(
                            qEbTypeRequest.ebTypeRequestNum,
                            qEbTypeRequest.reference,
                            qEbTypeRequest.libelle,
                            qEbTypeRequest.dateCreationSys,
                            qEbTypeRequest.dateMajSys,
                            qEbTypeRequest.delaiChronoPricing,
                            qEbTypeRequest.sensChronoPricing,
                            qEbTypeRequest.blocageResponsePricing,
                            qEbTypeRequest.activated,
                            qEbUser.ebUserNum,
                            qEbUser.email,
                            qEbCompagnie));
            query = getTypeRequestGlobalWhere(query, where, criteria);
            query.distinct().from(qEbTypeRequest);
            query = getTypeRequestGlobalJoin(query, where, criteria);
            query.where(where);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbTypeRequest, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbTypeRequest.ebTypeRequestNum.asc());
            }

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long countListEbTypeRequest(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbTypeRequest> query = new JPAQuery<EbTypeRequest>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getTypeRequestGlobalWhere(query, where, criteria);
            query.from(qEbTypeRequest);
            query = getTypeRequestGlobalJoin(query, where, criteria);
            query.distinct().where(where);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    private JPAQuery getTypeRequestGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        query.leftJoin(qEbTypeRequest.xEbCompagnie(), qEbCompagnie);
        query.leftJoin(qEbTypeRequest.xEbUser(), qEbUser);
        return query;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getTypeRequestGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);

        /* User Visibility Rights */
        if (criteria.getEbCompagnieNum() == null
            && (criteria.getCompanyNums() == null || criteria.getCompanyNums().size() == 0)
            && connectedUser.getEbCompagnie() != null) {
            criteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
        }

        if (criteria.getCompanyNums() != null && criteria.getCompanyNums().size() > 0) {
            where.and(qEbCompagnie.ebCompagnieNum.in(criteria.getCompanyNums()));
        }
        else if (criteria.isIncludeGlobalValues()) {
            where
                .andAnyOf(
                    qEbCompagnie.ebCompagnieNum.eq(criteria.getEbCompagnieNum()),
                    qEbCompagnie.ebCompagnieNum.isNull());
        }
        else {
            where.and(qEbCompagnie.ebCompagnieNum.eq(criteria.getEbCompagnieNum()));
        }

        /* END User Visibility Rights */

        if (criteria != null && criteria.getSearchterm() != null) {
            where
                .andAnyOf(
                    qEbTypeRequest.libelle.toLowerCase().contains(criteria.getSearchterm()),
                    qEbTypeRequest.reference.toLowerCase().contains(criteria.getSearchterm()));
        }

        if (criteria != null && criteria.getActivated() != null) {
            where.andAnyOf(qEbTypeRequest.activated.eq(criteria.getActivated()));
        }

        return query;
    }

    @Override
    public List<EbTypeRequest> searchTypeRequestByReference(String term) {
        return ebTypeRequestRepository.findAllWhereReferenceLike(term);
    }
}
