/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.DateTimePath;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoInvoice;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.CarrierStatus;
import com.adias.mytowereasy.repository.EbTypeRequestRepository;
import com.adias.mytowereasy.repository.EbTypeTransportRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.util.DateUtils;
import com.adias.mytowereasy.utils.search.SearchCriteriaFreightAudit;


@Component
@Transactional
public class DaoInvoiceImpl extends Dao implements DaoInvoice {
    @PersistenceContext
    EntityManager em;

    @Autowired
    private EbTypeTransportRepository ebTypeTransportRepository;

    @Autowired
    EbTypeRequestRepository ebTypeRequestRepository;

    QEbInvoice qEbInvoice = QEbInvoice.ebInvoice;

    QEbDemande qEbDemande = QEbDemande.ebDemande;
    QEbUser qCarrier = new QEbUser("qCarrier");
    QEbUser qChargeur = new QEbUser("qChargeur");
    QExEbDemandeTransporteur qEbDemandeTransporteur = QExEbDemandeTransporteur.exEbDemandeTransporteur;
    QEbUser qEbUserOwnerRequest = QEbUser.ebUser;

    @Autowired
    ConnectedUserService connectedUserService;

    private static Map<String, Object> orderHashMap = new HashMap<>();

    private void Initialiaze() {
        orderHashMap.put("ebInvoiceNum", qEbInvoice.ebInvoiceNum);
        orderHashMap.put("refDemande", qEbInvoice.refDemande);
        orderHashMap.put("nomCompanyCarrier", qEbInvoice.nomCompanyCarrier);
        orderHashMap.put("nomCompanyChargeur", qEbInvoice.nomCompanyChargeur);
        orderHashMap.put("totalWeight", qEbInvoice.totalWeight);
        orderHashMap.put("xEcModeTransport", qEbInvoice.xEcModeTransport);
        orderHashMap.put("listCategorieStr", qEbInvoice.listCategorieStr);
        orderHashMap.put("libelleDestCountry", qEbInvoice.libelleDestCountry);
        orderHashMap.put("libelleOriginCountry", qEbInvoice.libelleOriginCountry);
        orderHashMap.put("xEbTypeDemande", qEbInvoice.xEcTypeDemande);
        orderHashMap.put("preCarrierCost", qEbInvoice.preCarrierCost);
        orderHashMap.put("datePickup", qEbInvoice.datePickup);
        orderHashMap.put("dateDelivery", qEbInvoice.dateDelivery);
        orderHashMap.put("finalCarrierCost", qEbInvoice.finalCarrierCost);
        orderHashMap.put("flagCarrier", qEbInvoice.flagCarrier);
        orderHashMap.put("flag", qEbInvoice.flag);
        orderHashMap.put("memoChageur", qEbInvoice.memoChageur);
        orderHashMap.put("memoCarrier", qEbInvoice.memoCarrier);
        orderHashMap.put("applyCarrierIvoiceNumber", qEbInvoice.applyCarrierIvoiceNumber);
        orderHashMap.put("applyChargeurIvoiceNumber", qEbInvoice.applyChargeurIvoiceNumber);
        orderHashMap.put("applyChargeurIvoicePrice", qEbInvoice.applyChargeurIvoicePrice);
        orderHashMap.put("applyCarrierIvoicePrice", qEbInvoice.applyCarrierIvoicePrice);
        orderHashMap.put("statutChargeur", qEbInvoice.statutChargeur);
        orderHashMap.put("statutCarrier", qEbInvoice.statutCarrier);
        orderHashMap.put("gapChargeur", qEbInvoice.gapChargeur);
        orderHashMap.put("gapCarrier", qEbInvoice.gapCarrier);
        orderHashMap.put("invoiceDateChargeur", qEbInvoice.invoiceDateChargeur);
        orderHashMap.put("invoiceDateCarrier", qEbInvoice.invoiceDateCarrier);
        orderHashMap.put("xEbDemande.numAwbBol", qEbDemande.numAwbBol);
        orderHashMap.put("libelleDestCity", qEbInvoice.libelleDestCity);
        orderHashMap.put("xEcIncotermLibelle", qEbInvoice.xEcIncotermLibelle);
        orderHashMap.put("xEbDemande", qEbDemande.numAwbBol);
        orderHashMap.put("libelleOriginCity", qEbInvoice.libelleOriginCity);
        orderHashMap.put("xEcTypeDemande", qEbInvoice.xEcTypeDemande);
        orderHashMap.put("currencyLabel", qEbInvoice.currencyLabel);
        orderHashMap.put("uploadFileNbr", qEbInvoice.uploadFileNbr);
        orderHashMap.put("qEbDemandeTransporteur", qEbDemandeTransporteur.listCostCategorie);
        orderHashMap.put("xEbUserOwnerRequest.nom", qEbUserOwnerRequest.nom);
        orderHashMap.put("xEbUserOwnerRequest.prenom", qEbUserOwnerRequest.prenom);
    }

    @Override
    public Integer nextValEbInvoice() {
        Integer result = null;

        try {
            result = ((BigInteger) em
                .createNativeQuery("SELECT NEXTVAL('work.eb_invoice_eb_invoice_num_seq')").getSingleResult())
                    .intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public List<EbInvoice> getListEbInvoice(SearchCriteriaFreightAudit criterias) {
        List<EbInvoice> resultat = new ArrayList<EbInvoice>();
        Initialiaze();

        try {
            JPAQuery<EbInvoice> query = new JPAQuery<EbInvoice>(em);

            if (criterias != null && criterias.getModule() != null) {
                query
                    .select(
                        QEbInvoice
                            .create(
                                qEbInvoice.ebInvoiceNum,
                                qEbInvoice.ref,
                                qEbInvoice.statut,
                                qEbInvoice.xEcTypeDemande,
                                qEbInvoice.xEcModeTransport,
                                qEbInvoice.xEbTypeTransport,
                                qEbInvoice.typeRequestLibelle,
                                qEbInvoice.refDemande,
                                qEbInvoice.insurance,
                                qEbInvoice.insuranceValue,
                                qEbInvoice.libelleOriginCountry,
                                qEbInvoice.libelleOriginCity,
                                qEbInvoice.libelleDestCountry,
                                qEbInvoice.libelleDestCity,
                                qEbInvoice.totalNbrParcel,
                                qEbInvoice.totalVolume,
                                qEbInvoice.totalWeight,
                                qEbInvoice.totalTaxableWeight,
                                qEbInvoice.preCarrierCost,
                                qEbInvoice.finalCarrierCost,
                                qEbInvoice.datePickup,
                                qEbInvoice.transitTime,
                                qEbInvoice.dateDelivery,
                                qEbInvoice.xEbDemandeNum,
                                qEbInvoice.dateRequest,
                                qEbInvoice.listLabelStr,
                                qEbInvoice.listCategorieStr,
                                qEbInvoice.nomEtablissementCarrier,
                                qEbInvoice.nomCompanyCarrier,
                                qEbInvoice.nomEtablissementChargeur,
                                qEbInvoice.nomCompanyChargeur,
                                qEbInvoice.dateAjout,
                                qEbInvoice.dateModification,
                                qEbInvoice.memoCarrier,
                                qEbInvoice.memoChageur,
                                qEbInvoice.flagCarrier,
                                qEbInvoice.applyChargeurIvoicePrice,
                                qEbInvoice.applyCarrierIvoicePrice,
                                qEbInvoice.applyChargeurIvoiceNumber,
                                qEbInvoice.applyCarrierIvoiceNumber,
                                qEbInvoice.uploadFileNbr,
                                qEbInvoice.invoiceMonth,
                                qEbInvoice.invoiceYear,
                                qEbInvoice.invoiceMonthCarrier,
                                qEbInvoice.invoiceYearCarrier,
                                qEbInvoice.flag,
                                qCarrier.ebUserNum,
                                qCarrier.nom,
                                qCarrier.prenom,
                                qChargeur.ebUserNum,
                                qChargeur.nom,
                                qChargeur.prenom,
                                qEbInvoice.numberArraw,
                                qEbInvoice.priceArraw,
                                qEbInvoice.dateArraw,
                                qEbInvoice.statutChargeur,
                                qEbInvoice.statutCarrier,
                                qEbInvoice.invoiceDateChargeur,
                                qEbInvoice.invoiceDateCarrier,
                                qEbInvoice.gapChargeur,
                                qEbInvoice.gapCarrier,
                                qEbDemande.ebDemandeNum,
                                qEbDemande.customFields,
                                qEbDemande.listCategories,
                                qEbInvoice.xEcIncotermLibelle,
                                qEbInvoice.xEbTypeRequest,
                                qEbDemande.numAwbBol,
                                qEbDemande.xEbSchemaPsl(),
                                qEbInvoice.chargeur().ebCompagnie().ebCompagnieNum,
                                qEbInvoice.chargeur().ebCompagnie().nom,
                                qEbInvoice.listCostCategorieCharger,
                                qEbInvoice.listCostCategorieTransporteur,
                                qEbInvoice.listFlag,
                                qEbInvoice.currencyLabel,
                                qEbInvoice.commentInvoiceCarrier,
                                qEbInvoice.commentInvoiceCharger,
                                qEbInvoice.commentGapCarrier,
                                qEbInvoice.commentGapCharger,
                                qEbInvoice.statutArraw,
                                qEbInvoice.listTypeDocuments,
                                qEbInvoice.numAwbBol,
                                qEbDemandeTransporteur.listPlCostItem,
                                qEbUserOwnerRequest.ebUserNum,
                                qEbUserOwnerRequest.nom,
                                qEbUserOwnerRequest.prenom

                            ));
            }

            query = getGlobalWhere(query, criterias);

            if (criterias.getSize() != null && criterias.getSize() >= 0) {
                query.limit(criterias.getSize());
            }

            if (criterias.getPageNumber() != null) {
                query.offset(criterias.getPageNumber() * (criterias.getSize() != null ? criterias.getSize() : 1));
            }

            if (criterias.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbInvoice, criterias.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criterias.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbInvoice.ebInvoiceNum.desc());
            }

            if (criterias.getSize() != null && criterias.getSize() >= 0) {
                query.limit(criterias.getSize());
            }

            if (criterias.getPageNumber() != null) {
                query.offset(criterias.getPageNumber() * (criterias.getSize() != null ? criterias.getSize() : 1));
            }

            resultat = query.fetch();

            if (criterias.getOrderedColumn() != null && criterias.getOrderedColumn().equals("dateDelivery")) {
                resultat = this.sortListByDateDelivery(resultat, criterias.getAscendant());
            }

            if (criterias.getOrderedColumn() != null && criterias.getOrderedColumn().equals("datePickup")) {
                resultat = this.sortListByDatePickup(resultat, criterias.getAscendant());
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    private List<EbInvoice> sortListByDateDelivery(List<EbInvoice> invoices, boolean asc) {

        if (!asc) {
            invoices = invoices.stream().sorted(new Comparator<EbInvoice>() {
                @Override
                public int compare(EbInvoice o1, EbInvoice o2) {
                    if (o1.getDateDelivery() == null && o2.getDateDelivery() == null) return 0;

                    if (o1.getDateDelivery() == null) {
                        return -1;
                    }

                    if (o2.getDateDelivery() == null) {
                        return 1;
                    }

                    return o2.getDateDelivery().compareTo(o1.getDateDelivery());
                }
            }).collect(Collectors.toList());
        }
        else {
            invoices = invoices.stream().sorted(new Comparator<EbInvoice>() {
                @Override
                public int compare(EbInvoice o1, EbInvoice o2) {
                    if (o1.getDateDelivery() == null && o2.getDateDelivery() == null) return 0;

                    if (o1.getDateDelivery() == null) {
                        return 1;
                    }

                    if (o2.getDateDelivery() == null) {
                        return -1;
                    }

                    return o1.getDateDelivery().compareTo(o2.getDateDelivery());
                }
            }).collect(Collectors.toList());
        }

        return invoices;
    }

    private List<EbInvoice> sortListByDatePickup(List<EbInvoice> invoices, boolean asc) {

        if (!asc) {
            invoices = invoices.stream().sorted(new Comparator<EbInvoice>() {
                @Override
                public int compare(EbInvoice o1, EbInvoice o2) {
                    if (o1.getDatePickup() == null && o2.getDatePickup() == null) return 0;

                    if (o1.getDatePickup() == null) {
                        return -1;
                    }

                    if (o2.getDatePickup() == null) {
                        return 1;
                    }

                    return o2.getDatePickup().compareTo(o1.getDatePickup());
                }
            }).collect(Collectors.toList());
        }
        else {
            invoices = invoices.stream().sorted(new Comparator<EbInvoice>() {
                @Override
                public int compare(EbInvoice o1, EbInvoice o2) {
                    if (o1.getDatePickup() == null && o2.getDatePickup() == null) return 0;

                    if (o1.getDatePickup() == null) {
                        return 1;
                    }

                    if (o2.getDatePickup() == null) {
                        return -1;
                    }

                    return o1.getDatePickup().compareTo(o2.getDatePickup());
                }
            }).collect(Collectors.toList());
        }

        return invoices;
    }

    private JPAQuery getGlobalWhere(JPAQuery query, SearchCriteriaFreightAudit criterias) {
        BooleanBuilder where = new BooleanBuilder();
        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criterias);

        QEbRelation qRelation = new QEbRelation("rel");
        QEbEtablissement qEbEtablissementCt = new QEbEtablissement("qEbEtablissementCt");
        QEbEtablissement qEtablissementQuote = new QEbEtablissement("qEtablissementQuote");
        QEbEtablissement qEtab = new QEbEtablissement("qEtab");
        QEbUser qEbChargeur = new QEbUser("qEbChargeur");
        QExEbDemandeTransporteur qExDemandeTransporteur = QExEbDemandeTransporteur.exEbDemandeTransporteur;
        Integer ebCompagnieNum = criterias.getEbCompagnieNum() != null ?
            criterias.getEbCompagnieNum() :
            connectedUser.getEbCompagnie().getEbCompagnieNum();

        if (criterias != null) {

            if (criterias.getIdObject() != null) {
                where.and(qEbInvoice.ebInvoiceNum.eq(criterias.getIdObject()));
            }

            if (criterias.getEbDemandeNum() != null) {
                where.and(qEbInvoice.xEbDemandeNum.eq(criterias.getEbDemandeNum()));
            }

            if (criterias.getStatutChargeur() != null) {
                where.and(qEbInvoice.statutChargeur.eq(criterias.getStatutChargeur()));
            }

            if (criterias.getListChargeur() != null) {
                where.and(qEbInvoice.chargeur().ebUserNum.in(criterias.getListChargeur()));
            }

            /*
             * if(criterias.getListModeTransport() != null &&
             * !criterias.getListModeTransport().isEmpty()) {
             * where.and(qEbInvoice.xEcModeTransport.in(criterias.
             * getListModeTransport()));
             * }
             */
            if (criterias.getListPriority() != null && !criterias.getListPriority().isEmpty()) {
                List<Integer> listPriority = ebTypeRequestRepository
                    .getEbTypeRequestIds(criterias.getListPriority(), ebCompagnieNum);

                if (listPriority != null) {
                    where.and(qEbDemande.xEbTypeRequest.in(listPriority));
                }

            }

            if (criterias.getListModeTransport() != null && !criterias.getListModeTransport().isEmpty()) {
                where.and(qEbDemande.xEcModeTransport.in(criterias.getListModeTransport()));
            }

            if (criterias.getListTypeDemande() != null && !criterias.getListTypeDemande().isEmpty()) {
                BooleanBuilder b = new BooleanBuilder();
                criterias.getListTypeDemande().stream().forEach(typeDemande -> {
                    b.or(qEbInvoice.typeRequestLibelle.endsWithIgnoreCase(typeDemande));
                });
                where.and(b);
            }

            if (criterias.getListStatut() != null && !criterias.getListStatut().isEmpty()) {

                if (connectedUser.isChargeur() || connectedUser.isControlTower()) {
                    if (criterias
                        .getListStatut()
                        .contains(3)) where
                            .andAnyOf(
                                qEbInvoice.statutChargeur.in(criterias.getListStatut()),
                                qEbInvoice.statutChargeur.isNull());
                    else where.and(qEbInvoice.statutChargeur.in(criterias.getListStatut()));
                }
                else if (connectedUser.isPrestataire()) {
                    if (criterias
                        .getListStatut()
                        .contains(1)) where
                            .andAnyOf(
                                qEbInvoice.statutCarrier.in(criterias.getListStatut()),
                                qEbInvoice.statutCarrier.isNull());
                    else where.and(qEbInvoice.statutCarrier.in(criterias.getListStatut()));
                }

            }

            if (criterias.getListIncoterm() != null && !criterias.getListIncoterm().isEmpty()) {
                where.and(qEbInvoice.xEcIncotermLibelle.in(criterias.getListIncoterm()));
            }

            if (criterias.getListTransportRef() != null && !criterias.getListTransportRef().isEmpty()) {
                where.and(qEbDemande.refTransport.in(criterias.getListTransportRef()));
            }

            if (criterias.getListNumAwbBol() != null && !criterias.getListNumAwbBol().isEmpty()) {
                where.and(qEbDemande.numAwbBol.in(criterias.getListNumAwbBol()));
            }

            if (criterias.getListOriginCity() != null && !criterias.getListOriginCity().isEmpty()) {
                criterias.getListOriginCity().replaceAll(String::toUpperCase);
                where.and(qEbInvoice.libelleOriginCity.toUpperCase().in(criterias.getListOriginCity()));
            }

            if (criterias.getListDestinationCity() != null && !criterias.getListDestinationCity().isEmpty()) {
                criterias.getListDestinationCity().replaceAll(String::toUpperCase);
                where.and(qEbInvoice.libelleDestCity.toUpperCase().in(criterias.getListDestinationCity()));
            }

            if (criterias.getMaxTaxableWeight() != null) {
                where.and(qEbInvoice.totalTaxableWeight.eq(criterias.getMaxTaxableWeight()));
            }

            if (criterias.getPreCarrierCost() != null) {
                where.and(qEbInvoice.preCarrierCost.eq(criterias.getPreCarrierCost()));
            }

            if (criterias.getInvoiceCurrency() != null) {
                where.and(qEbDemande.xecCurrencyInvoice().ecCurrencyNum.eq(criterias.getInvoiceCurrency()));
            }

            if (criterias.getListEbUserOwnerRequest() != null && !criterias.getListEbUserOwnerRequest().isEmpty()) {
                where.and(qEbUserOwnerRequest.ebUserNum.in(criterias.getListEbUserOwnerRequest()));
            }

            if (criterias.getApplyCarrierIvoiceNumber() != null) {
                where.and(qEbInvoice.applyCarrierIvoiceNumber.eq(criterias.getApplyCarrierIvoiceNumber()));
            }

            if (criterias.getExchangeRate() != null) {
                where.and(qExDemandeTransporteur.exchangeRate.eq(criterias.getExchangeRate()));
            }

            if (criterias.getListInfosDocsStatus() != null && !criterias.getListInfosDocsStatus().isEmpty()) {
                where.and(getListDocumentStatusWhere(criterias.getListInfosDocsStatus()));
            }

            BooleanBuilder booleanBuilderForInvoiceDate = new BooleanBuilder();

            DateTimePath<Date> carrierOrCharger = connectedUser.isChargeur() ?
                qEbInvoice.invoiceDateChargeur :
                qEbInvoice.invoiceDateCarrier;

            if (criterias.getInvoiceDateFrom() != null) {
                Date invoiceDateFrom = DateUtils
                    .setDate_To_The_Start_Of_The_Day_Month(criterias.getInvoiceDateFrom(), true);
                booleanBuilderForInvoiceDate = booleanBuilderForInvoiceDate.and(carrierOrCharger.goe(invoiceDateFrom));
                if (isInSameMonthAndYearWithCurrentDay(
                    invoiceDateFrom.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()) ||
                    (criterias.getInvoiceDateTo() == null &&
                        !isDateInFuture(
                            invoiceDateFrom
                                .toInstant().atZone(ZoneId.systemDefault())
                                .toLocalDate()))) booleanBuilderForInvoiceDate = booleanBuilderForInvoiceDate
                                    .or(carrierOrCharger.isNull());
            }

            if (criterias.getInvoiceDateTo() != null) {
                Date invoiceDateTo = DateUtils
                    .setDate_To_The_End_Of_The_Day_And_Month(criterias.getInvoiceDateTo(), true);
                booleanBuilderForInvoiceDate = booleanBuilderForInvoiceDate.and(carrierOrCharger.loe(invoiceDateTo));

                if (isDateInFuture(invoiceDateTo.toInstant().atZone(ZoneId.systemDefault()).toLocalDate())) {

                    if (!isDateInFuture(
                        new Date(criterias.getInvoiceDateFrom().getTime())
                            .toInstant().atZone(ZoneId.systemDefault()).toLocalDate())) {
                        booleanBuilderForInvoiceDate = booleanBuilderForInvoiceDate.or(carrierOrCharger.isNull());
                    }

                }

                if (isInSameMonthAndYearWithCurrentDay(
                    invoiceDateTo.toInstant().atZone(ZoneId.systemDefault()).toLocalDate())) {
                    booleanBuilderForInvoiceDate = booleanBuilderForInvoiceDate.or(carrierOrCharger.isNull());
                }

            }

            where.and(booleanBuilderForInvoiceDate);

            if (criterias.getGap() != null) {
                if (connectedUser.isChargeur()) where.and(qEbInvoice.gapChargeur.eq(criterias.getGap()));
                else where.and(qEbInvoice.gapCarrier.eq(criterias.getGap()));
            }

            if (criterias.getPriceCharged() != null) {
                if (connectedUser
                    .isChargeur()) where.and(qEbInvoice.applyChargeurIvoicePrice.eq(criterias.getPriceCharged()));
                else where.and(qEbInvoice.applyCarrierIvoicePrice.eq(criterias.getPriceCharged()));
            }

            if (criterias.getMemo() != null) {
                if (connectedUser.isChargeur()) where.and(qEbInvoice.memoChageur.eq(criterias.getMemo()));
                else where.and(qEbInvoice.memoCarrier.eq(criterias.getMemo()));
            }

            if (criterias.getDatePickup() != null) {
                TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
                Calendar calendarLocal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                calendarLocal.setTime(criterias.getDatePickup());
                calendarLocal.set(Calendar.HOUR_OF_DAY, 0);
                calendarLocal.set(Calendar.MINUTE, 00);
                calendarLocal.set(Calendar.SECOND, 00);
                Date datePickup = calendarLocal.getTime();
                where.and(qEbInvoice.datePickup.eq(datePickup));
            }

            if (criterias.getDatePickUpFrom() != null) {
                TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
                Calendar calendarLocal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                calendarLocal.setTime(criterias.getDatePickUpFrom());
                calendarLocal.set(Calendar.HOUR_OF_DAY, 0);
                calendarLocal.set(Calendar.MINUTE, 00);
                calendarLocal.set(Calendar.SECOND, 00);
                Date datePickupFrom = calendarLocal.getTime();
                where.and(qEbInvoice.datePickup.goe(datePickupFrom));
            }

            if (criterias.getDatePickUpTo() != null) {
                TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
                Calendar calendarLocal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                calendarLocal.setTime(criterias.getDatePickUpTo());
                calendarLocal.set(Calendar.HOUR_OF_DAY, 23);
                calendarLocal.set(Calendar.MINUTE, 59);
                calendarLocal.set(Calendar.SECOND, 00);
                Date datePickupTo = calendarLocal.getTime();
                where.and(qEbInvoice.datePickup.loe(datePickupTo));
            }

            if (criterias.getDateDelivery() != null) {
                TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
                Calendar calendarLocal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                calendarLocal.setTime(criterias.getDateDelivery());
                calendarLocal.set(Calendar.HOUR_OF_DAY, 0);
                calendarLocal.set(Calendar.MINUTE, 00);
                calendarLocal.set(Calendar.SECOND, 00);
                Date dateDelivery = calendarLocal.getTime();
                where.and(qEbInvoice.dateDelivery.eq(dateDelivery));
            }

            if (criterias.getDateDeliveryFrom() != null) {
                TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
                Calendar calendarLocal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                calendarLocal.setTime(criterias.getDateDeliveryFrom());
                calendarLocal.set(Calendar.HOUR_OF_DAY, 0);
                calendarLocal.set(Calendar.MINUTE, 00);
                calendarLocal.set(Calendar.SECOND, 00);
                Date dateDeliveryFrom = calendarLocal.getTime();

                where.and(qEbInvoice.dateDelivery.goe(dateDeliveryFrom));
            }

            if (criterias.getDateDeliveryTo() != null) {
                TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
                Calendar calendarLocal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
                calendarLocal.setTime(criterias.getDateDeliveryTo());
                calendarLocal.set(Calendar.HOUR_OF_DAY, 23);
                calendarLocal.set(Calendar.MINUTE, 59);
                calendarLocal.set(Calendar.SECOND, 00);
                Date dateDeliveryTo = calendarLocal.getTime();

                where.and(qEbInvoice.dateDelivery.loe(dateDeliveryTo));
            }

            if (criterias.getListCarrier() != null && !criterias.getListCarrier().isEmpty()) {
                where.and(qEbInvoice.carrier().ebUserNum.in(criterias.getListCarrier()));
            }

            if (criterias.getListEtablissementNum() != null && !criterias.getListEtablissementNum().isEmpty()) {
                where.and(qEbInvoice.nomEtablissementCarrier.in(criterias.getListEtablissementNum()));
            }

            if (criterias.getListCompanyName() != null && !criterias.getListCompanyName().isEmpty()) {
                where.and(qEbInvoice.nomCompanyCarrier.in(criterias.getListCompanyName()));
            }

            if (criterias.getListDestCountry() != null && !criterias.getListDestCountry().isEmpty()) {
                BooleanBuilder b = new BooleanBuilder();
                criterias.getListDestCountry().stream().forEach(it -> {
                    b.or(qEbInvoice.libelleDestCountry.contains(it));
                });
                where.and(b);
            }

            if (criterias.getListOrigineCountry() != null && !criterias.getListOrigineCountry().isEmpty()) {
                BooleanBuilder b = new BooleanBuilder();
                criterias.getListOrigineCountry().stream().forEach(it -> {
                    b.or(qEbInvoice.libelleOriginCountry.contains(it));
                });
                where.and(b);
            }

            criterias.applyAdvancedSearchForCustomFields(where, qEbDemande.customFields);

            // search
            if (criterias.getSearch() != null) {
                String value = criterias.getSearch().get("value");
                Date dt = null;
                String valueModeTr = "";
                String valueTypeTr = "";

                if (value != null && !value.isEmpty()) {
                    value = value.trim().toLowerCase();

                    if (Pattern.compile("([0-9]{2}/){2}[0-9]{4}").matcher(value).matches()) {
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

                        try {
                            dt = formatter.parse(value);
                        } catch (Exception e) {
                        }

                    }

                    ;

                    if (value.contains("(")) {

                        try {
                            valueModeTr = value.replace(")", "");
                            String[] vs = valueModeTr.split("\\(");
                            valueModeTr = vs[0].trim();
                            valueTypeTr = vs[1].trim();
                        } catch (Exception e) {
                        }

                    }
                    else {
                        valueModeTr = value.toString();
                    }

                    // Recuperez
                    Integer modeTrCode = Enumeration.ModeTransport.getCodeByLibelle(valueModeTr);
                    EbTypeTransport typeTransport = ebTypeTransportRepository
                        .findEbTypeTransportByLibeleAndModeTransport(valueTypeTr, modeTrCode);
                    Integer codeTypeTransport = null;

                    if (typeTransport != null) {
                        codeTypeTransport = typeTransport.getEbTypeTransportNum();
                    }

                    Integer codeTypeDemande = Enumeration.TypeDemande.getCodeByLibelle(value);

                    List<Integer> listCodeModeTransport = Enumeration.ModeTransport.getListCodeByLibelle(valueModeTr);

                    List<Integer> listCodeStatus = Enumeration.StatutDemande.getListCodeByLibelle(value);
                    Integer InvoiceStatus = FAEnumeration.InvoiceStatus.getCodeByLibelle(value);
                    // Integer valueDate =
                    // FAEnumeration.dateMonth.getCodeByLibelle(value);

                    if (listCodeModeTransport == null ||
                        listCodeModeTransport.isEmpty()) listCodeModeTransport = new ArrayList<Integer>();
                    if (listCodeStatus == null || listCodeStatus.isEmpty()) listCodeStatus = new ArrayList<Integer>();
                    if (codeTypeDemande == null) codeTypeDemande = -1;

                    if (connectedUser != null && connectedUser.isControlTower()) {
                        BooleanBuilder w = new BooleanBuilder();
                        w
                            .andAnyOf(

                                qEbInvoice.refDemande.toLowerCase().contains(value),
                                qEbInvoice.totalWeight.stringValue().toLowerCase().contains(value),
                                qEbInvoice.xEcModeTransport.stringValue().toLowerCase().contains(value),
                                qEbInvoice.libelleDestCountry.stringValue().toLowerCase().contains(value),
                                qEbInvoice.libelleOriginCountry.stringValue().toLowerCase().contains(value),
                                qEbInvoice.xEcTypeDemande.stringValue().toLowerCase().contains(value),
                                qEbInvoice.preCarrierCost.stringValue().toLowerCase().contains(value),
                                qEbInvoice.datePickup.stringValue().toLowerCase().contains(value),
                                qEbInvoice.dateDelivery.stringValue().toLowerCase().contains(value),
                                qEbDemande.xEcTypeDemande.eq(codeTypeDemande),

                                qEbInvoice.applyChargeurIvoiceNumber.stringValue().toLowerCase().contains(value),

                                qEbInvoice.applyChargeurIvoicePrice.stringValue().toLowerCase().contains(value),
                                qEbInvoice.nomCompanyCarrier.stringValue().toLowerCase().contains(value),
                                qEbInvoice.nomCompanyChargeur.stringValue().toLowerCase().contains(value),
                                qEbInvoice.xEcIncotermLibelle.stringValue().toLowerCase().contains(value),
                                qEbInvoice.libelleOriginCity.stringValue().toLowerCase().contains(value),
                                qEbInvoice.libelleDestCity.stringValue().toLowerCase().contains(value),
                                qEbDemande.numAwbBol.stringValue().toLowerCase().contains(value),
                                qEbInvoice.gapChargeur.stringValue().toLowerCase().contains(value),
                                qEbDemande.listCustomFieldsFlat.toLowerCase().like("%:\"%" + value + "%\"%"),
                                qEbDemande.listCategoryFlat.toLowerCase().like("%:\"%" + value + "%\"%"),
                                qEbUserOwnerRequest.nom.stringValue().containsIgnoreCase(value),
                                qEbUserOwnerRequest.prenom.stringValue().containsIgnoreCase(value)

                            );

                        if (FAEnumeration.Flag.getCodeByLibelle(value) != null) {
                            w.or(qEbInvoice.flag.eq(FAEnumeration.Flag.getCodeByLibelle(value)));
                        }

                        if (qEbInvoice.uploadFileNbr.stringValue().toLowerCase().contains(value) != null) {
                            w.or(qEbInvoice.uploadFileNbr.stringValue().toLowerCase().contains(value));
                        }

                        if (qEbInvoice.gapChargeur.stringValue().toLowerCase().contains(value) != null) {
                            w.or(qEbInvoice.gapChargeur.stringValue().toLowerCase().contains(value));
                        }

                        if (qEbInvoice.memoChageur.stringValue().toLowerCase().contains(value) != null) {
                            w.or(qEbInvoice.memoChageur.stringValue().toLowerCase().contains(value));
                        }

                        if (qEbDemande.numAwbBol.stringValue().toLowerCase().contains(value) != null) {
                            w.or(qEbDemande.numAwbBol.stringValue().toLowerCase().contains(value));
                        }

                        if (codeTypeTransport == null && !listCodeModeTransport.isEmpty()) w
                            .or(qEbDemande.xEcModeTransport.in(listCodeModeTransport));
                        if (InvoiceStatus != null) w.or(qEbInvoice.statutChargeur.eq(InvoiceStatus));

                        if (codeTypeTransport != null && !listCodeModeTransport.isEmpty()) {
                            w
                                .orAllOf(
                                    qEbDemande.xEcModeTransport.in(listCodeModeTransport),
                                    qEbDemande.xEbTypeTransport.eq(codeTypeTransport));
                        }

                        where.and(w);
                    }

                    if (connectedUser != null && connectedUser.isChargeur()) {
                        BooleanBuilder w = new BooleanBuilder();
                        w
                            .andAnyOf(

                                qEbInvoice.refDemande.toLowerCase().contains(value),
                                qEbInvoice.totalWeight.stringValue().toLowerCase().contains(value),
                                qEbInvoice.xEcModeTransport.stringValue().toLowerCase().contains(value),
                                qEbInvoice.libelleDestCountry.stringValue().toLowerCase().contains(value),
                                qEbInvoice.libelleOriginCountry.stringValue().toLowerCase().contains(value),
                                qEbInvoice.xEcTypeDemande.stringValue().toLowerCase().contains(value),
                                qEbInvoice.preCarrierCost.stringValue().toLowerCase().contains(value),
                                qEbInvoice.datePickup.stringValue().toLowerCase().contains(value),
                                qEbInvoice.dateDelivery.stringValue().toLowerCase().contains(value),

                                qEbDemande.xEcTypeDemande.eq(codeTypeDemande),

                                qEbInvoice.applyChargeurIvoiceNumber.stringValue().toLowerCase().contains(value),

                                qEbInvoice.applyChargeurIvoicePrice.stringValue().toLowerCase().contains(value),
                                qEbInvoice.nomCompanyCarrier.stringValue().toLowerCase().contains(value),
                                qEbInvoice.xEcIncotermLibelle.stringValue().toLowerCase().contains(value),
                                qEbInvoice.libelleOriginCity.stringValue().toLowerCase().contains(value),
                                qEbInvoice.libelleDestCity.stringValue().toLowerCase().contains(value),
                                // qEbInvoice.invoiceDateChargeur.stringValue().toLowerCase().contains("valueDate"),
                                qEbDemande.numAwbBol.stringValue().toLowerCase().contains(value),
                                qEbInvoice.gapChargeur.stringValue().toLowerCase().contains(value),
                                qEbDemande.listCustomFieldsFlat.toLowerCase().like("%:\"%" + value + "%\"%"),
                                qEbDemande.listCategoryFlat.toLowerCase().like("%:\"%" + value + "%\"%"),
                                qEbUserOwnerRequest.nom.stringValue().containsIgnoreCase(value),
                                qEbUserOwnerRequest.prenom.stringValue().containsIgnoreCase(value)

                            );

                        if (FAEnumeration.Flag.getCodeByLibelle(value) != null) {
                            w.or(qEbInvoice.flag.eq(FAEnumeration.Flag.getCodeByLibelle(value)));
                        }

                        if (qEbInvoice.uploadFileNbr.stringValue().toLowerCase().contains(value) != null) {
                            w.or(qEbInvoice.uploadFileNbr.stringValue().toLowerCase().contains(value));
                        }

                        if (qEbInvoice.gapChargeur.stringValue().toLowerCase().contains(value) != null) {
                            w.or(qEbInvoice.gapChargeur.stringValue().toLowerCase().contains(value));
                        }

                        if (qEbInvoice.memoChageur.stringValue().toLowerCase().contains(value) != null) {
                            w.or(qEbInvoice.memoChageur.stringValue().toLowerCase().contains(value));
                        }

                        if (codeTypeTransport == null && !listCodeModeTransport.isEmpty()) w
                            .or(qEbDemande.xEcModeTransport.in(listCodeModeTransport));
                        if (InvoiceStatus != null) w.or(qEbInvoice.statutChargeur.eq(InvoiceStatus));

                        if (codeTypeTransport != null && !listCodeModeTransport.isEmpty()) {
                            w
                                .orAllOf(
                                    qEbDemande.xEcModeTransport.in(listCodeModeTransport),
                                    qEbDemande.xEbTypeTransport.eq(codeTypeTransport));
                        }

                        where.and(w);
                    }

                    if (connectedUser != null && connectedUser.isPrestataire()) {
                        BooleanBuilder w = new BooleanBuilder();
                        w
                            .andAnyOf(

                                qEbInvoice.refDemande.toLowerCase().contains(value),
                                qEbInvoice.totalWeight.stringValue().toLowerCase().contains(value),
                                qEbInvoice.xEcModeTransport.stringValue().toLowerCase().contains(value),
                                qEbInvoice.libelleDestCountry.stringValue().toLowerCase().contains(value),
                                qEbInvoice.libelleOriginCountry.stringValue().toLowerCase().contains(value),
                                qEbInvoice.xEcTypeDemande.stringValue().toLowerCase().contains(value),
                                qEbInvoice.preCarrierCost.stringValue().toLowerCase().contains(value),
                                qEbInvoice.datePickup.stringValue().toLowerCase().contains(value),
                                qEbInvoice.dateDelivery.stringValue().toLowerCase().contains(value),
                                qEbDemande.xEcTypeDemande.eq(codeTypeDemande),
                                qEbInvoice.applyCarrierIvoiceNumber.stringValue().toLowerCase().contains(value),

                                qEbInvoice.applyCarrierIvoicePrice.stringValue().toLowerCase().contains(value),

                                qEbInvoice.nomCompanyChargeur.stringValue().toLowerCase().contains(value),
                                qEbInvoice.xEcIncotermLibelle.stringValue().toLowerCase().contains(value),
                                qEbInvoice.libelleOriginCity.stringValue().toLowerCase().contains(value),
                                qEbInvoice.libelleDestCity.stringValue().toLowerCase().contains(value),
                                qEbDemande.numAwbBol.stringValue().toLowerCase().contains(value),
                                qEbInvoice.gapCarrier.stringValue().toLowerCase().contains(value),
                                qEbDemande.listCustomFieldsFlat.toLowerCase().like("%:\"%" + value + "%\"%"),
                                qEbDemande.listCategoryFlat.toLowerCase().like("%:\"%" + value + "%\"%"),
                                qEbUserOwnerRequest.nom.stringValue().containsIgnoreCase(value),
                                qEbUserOwnerRequest.prenom.stringValue().containsIgnoreCase(value)

                            );

                        if (FAEnumeration.Flag.getCodeByLibelle(value) != null) {
                            w.or(qEbInvoice.flagCarrier.eq(FAEnumeration.Flag.getCodeByLibelle(value)));
                        }

                        if (qEbInvoice.uploadFileNbr.stringValue().toLowerCase().contains(value) != null) {
                            w.or(qEbInvoice.uploadFileNbr.stringValue().toLowerCase().contains(value));
                        }

                        if (qEbInvoice.gapCarrier.stringValue().toLowerCase().contains(value) != null) {
                            w.or(qEbInvoice.gapCarrier.stringValue().toLowerCase().contains(value));
                        }

                        if (qEbInvoice.memoCarrier.stringValue().toLowerCase().contains(value) != null) {
                            w.or(qEbInvoice.memoCarrier.stringValue().toLowerCase().contains(value));
                        }

                        if (dt != null) w
                            .or(
                                qEbInvoice.dateDelivery
                                    .stringValue().toLowerCase()
                                    .contains((new SimpleDateFormat("yyyy-MM-dd")).format(dt)));

                        if (codeTypeTransport == null && !listCodeModeTransport.isEmpty()) w
                            .or(qEbDemande.xEcModeTransport.in(listCodeModeTransport));
                        if (InvoiceStatus != null) w.or(qEbInvoice.statutCarrier.eq(InvoiceStatus));

                        if (codeTypeTransport != null && !listCodeModeTransport.isEmpty()) {
                            w
                                .orAllOf(
                                    qEbDemande.xEcModeTransport.in(listCodeModeTransport),
                                    qEbDemande.xEbTypeTransport.eq(codeTypeTransport));
                        }

                        where.and(w);
                    }

                }

            }

            BooleanBuilder boolLabels = new BooleanBuilder();

            if (criterias.getMapCategoryFields() != null && !criterias.getMapCategoryFields().isEmpty()) {
                BooleanBuilder orBoolLabels = null;

                List<Integer> ebLabels = null;

                for (String key: criterias.getMapCategoryFields().keySet()) {
                    ebLabels = criterias.getMapCategoryFields().get(key);

                    if (ebLabels != null) {
                        orBoolLabels = new BooleanBuilder();
                        int i, n = ebLabels.size();

                        for (i = 0; i < n; i++) {
                            Long ebLabelNum = Math.round(Double.parseDouble(ebLabels.get(i) + ""));
                            orBoolLabels
                                .or(
                                    new BooleanBuilder()
                                        .andAnyOf(
                                            qEbDemande.listLabels.startsWith(ebLabelNum + ","),
                                            qEbDemande.listLabels.contains("," + ebLabelNum + ","),
                                            qEbDemande.listLabels.endsWith("," + ebLabelNum),
                                            qEbDemande.listLabels.eq(ebLabelNum + "")));
                        }

                        if (orBoolLabels.hasValue()) boolLabels.and(orBoolLabels);
                    }

                }

                if (boolLabels.hasValue()) where.andAnyOf(qEbDemande.listLabels.isNotNull().andAnyOf(boolLabels));
            }

            // recherche par flags
            if (criterias.getListFlag() != null && !criterias.getListFlag().trim().isEmpty()) {
                List<String> listFlagId = Arrays.asList(criterias.getListFlag().trim().split(","));
                Predicate[] predicats = new Predicate[listFlagId.size() * 4];
                int j = 0;

                for (int i = 0; i < listFlagId.size(); i++) {
                    predicats[i + j] = qEbInvoice.listFlag
                        .contains(listFlagId.get(i))
                        .and(qEbInvoice.listFlag.trim().length().eq(listFlagId.get(i).length()));
                    predicats[i + j + 1] = qEbInvoice.listFlag.endsWith("," + listFlagId.get(i));
                    predicats[i + j + 2] = qEbInvoice.listFlag.startsWith(listFlagId.get(i) + ",");
                    predicats[i + j + 3] = qEbInvoice.listFlag.contains("," + listFlagId.get(i) + ",");
                    j += 3;
                }

                where.andAnyOf(predicats);
            }

        }

        if (connectedUser != null && (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker())) {
            if (!connectedUser.isSuperAdmin()) where
                .and(
                    qCarrier.ebEtablissement().ebEtablissementNum
                        .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));
            else where
                .and(qCarrier.ebCompagnie().ebCompagnieNum.in(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }

        if (connectedUser.isChargeur() ||
            (connectedUser.getEbCompagnie().getCompagnieRole() == Enumeration.Role.ROLE_CHARGEUR.getCode())) {
            if (connectedUser.isSuperAdmin()) where
                .and(qChargeur.ebCompagnie().ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
            else if (connectedUser.isAdmin()) {
                where
                    .and(
                        qChargeur.ebEtablissement().ebEtablissementNum
                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));
            }
            else {
                where.and(qEtab.ebCompagnie().ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
                BooleanBuilder boolLabels = new BooleanBuilder();

                if (connectedUser.getListCategories() != null) {
                    Gson gson = new Gson();
                    String categories = gson.toJson(connectedUser.getListCategories());

                    if (categories != null && !categories.isEmpty()) {
                        boolLabels
                            .and(
                                Expressions
                                    .booleanTemplate(
                                        Dao.SCHEMA + "is_userlabels_contains_demlabels({0}, {1}) = true",
                                        categories,
                                        qEbDemande.listCategories));
                    }

                }

                if (!boolLabels.hasValue()) {
                    boolLabels.andAnyOf(qEbDemande.listLabels.isNull(), qEbDemande.listLabels.isEmpty());
                }

                where.and(qEtab.ebEtablissementNum.eq(connectedUser.getEbEtablissement().getEbEtablissementNum()));
                where
                    .andAnyOf(
                        qEbDemande.listLabels.isNull(),
                        qEbDemande.listLabels.isEmpty(),
                        new BooleanBuilder()
                            .orAllOf(qEbDemande.listLabels.isNotNull(), new BooleanBuilder().andAnyOf(boolLabels)));
            }
        }
        else if (connectedUser.isControlTower() &&
            connectedUser.getEbCompagnie().getCompagnieRole() != Enumeration.Role.ROLE_CHARGEUR.getCode()) {
                where
                    .andAnyOf(
                        qEbEtablissementCt.ebEtablissementNum
                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                        // objets pour les quelles il est sollicité
                        qEtablissementQuote.ebEtablissementNum
                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                        qExDemandeTransporteur.xTransporteur().ebUserNum.eq(connectedUser.getEbUserNum()));

            }

        query.distinct();
        query.from(qEbInvoice).where(where);
        query.leftJoin(qEbInvoice.xEbDemande(), qEbDemande);
        query.leftJoin(qEbInvoice.chargeur(), qChargeur);
        query.leftJoin(qEbInvoice.carrier(), qCarrier);
        query.leftJoin(qEbInvoice.xEbUserOwnerRequest(), qEbUserOwnerRequest);
        query
            .leftJoin(qEbDemande.exEbDemandeTransporteurs, qExDemandeTransporteur).where(
                qExDemandeTransporteur.status
                    .in(Arrays.asList(CarrierStatus.FIN_PLAN.getCode(), CarrierStatus.FINAL_CHOICE.getCode())));

        if (connectedUser.isControlTower()) {
            query.leftJoin(qEbDemande.xEbUserCt().ebEtablissement(), qEbEtablissementCt);
            query.leftJoin(qEbDemande.exEbDemandeTransporteurs, qExDemandeTransporteur);
            query.leftJoin(qExDemandeTransporteur.xEbEtablissement(), qEtablissementQuote);
        }

        if (connectedUser.isChargeur()) {
            query.leftJoin(qEbInvoice.chargeur(), qEbChargeur);
            query.leftJoin(qEbChargeur.ebEtablissement(), qEtab);
        }

        return query;
    }

    @Override
    public Long getCountListEbInvoice(SearchCriteriaFreightAudit criteria) {
        Long count = new Long(0);

        try {
            JPAQuery<Long> query = new JPAQuery<Long>(em);

            query = getGlobalWhere(query, criteria);
            // query.distinct();

            count = (Long) query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return count;
    }

    public BooleanBuilder getListDocumentStatusWhere(List<Integer> listDocsStatus) {
        BooleanBuilder whereDocs = new BooleanBuilder();

        if (listDocsStatus != null && !listDocsStatus.isEmpty()) {

            for (Integer status: listDocsStatus) {
                /*
                 * désactivé pour répondre aux critères de ce jira
                 * https://jira.adias.fr/browse/MTGR-392
                 * if (status.equals(Enumeration.InfosDocsStatus.
                 * TRANSPORT_INFO_MISSING.getCode()))
                 * whereDocs
                 * .or(
                 * new BooleanBuilder()
                 * .andAnyOf(
                 * qEbDemande.flagTransportInformation.isFalse(),
                 * qEbDemande.flagTransportInformation.isNull()
                 * )
                 * );
                 * else if
                 * (status.equals(Enumeration.InfosDocsStatus.TRANSPORT_INFO_OK.
                 * getCode()))
                 * whereDocs.or(qEbDemande.flagTransportInformation.isTrue());
                 * else
                 */
                if (status.equals(Enumeration.InfosDocsStatus.CUSTOM_DOCUMENT_MISSING.getCode())) whereDocs
                    .or(
                        new BooleanBuilder()
                            .andAnyOf(
                                qEbDemande.flagDocumentCustoms.isFalse(),
                                qEbDemande.flagDocumentCustoms.isNull()));
                else if (status.equals(Enumeration.InfosDocsStatus.CUSTOM_DOCUMENT_OK.getCode())) whereDocs
                    .or(qEbDemande.flagDocumentCustoms.isTrue());

                else if (status.equals(Enumeration.InfosDocsStatus.TRANSPORT_DOCUMENT_MISSING.getCode())) whereDocs
                    .or(
                        new BooleanBuilder()
                            .andAnyOf(
                                qEbDemande.flagDocumentTransporteur.isFalse(),
                                qEbDemande.flagDocumentTransporteur.isNull()));
                else if (status.equals(Enumeration.InfosDocsStatus.TRANSPORT_DOCUMENT_OK.getCode())) whereDocs
                    .or(qEbDemande.flagDocumentTransporteur.isTrue());

                else if (status >= Enumeration.StatutDemande.WPU.getCode()) {
                    whereDocs.or(qEbDemande.xEcStatut.eq(status));
                }
            }

        }

        return whereDocs;
    }

    private boolean isInSameMonthAndYearWithCurrentDay(LocalDate date) {
        LocalDate now = LocalDate.now();
        return date != null ? (now.getYear() == date.getYear() && now.getMonth().equals(date.getMonth())) : false;
    }

    private boolean isDateInFuture(LocalDate date) {
        return date != null ? date.withDayOfMonth(1).isAfter(LocalDate.now().withDayOfMonth(1)) : false;
    }
}
