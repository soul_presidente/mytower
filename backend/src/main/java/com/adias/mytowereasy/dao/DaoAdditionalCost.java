package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbAdditionalCost;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoAdditionalCost {
    public List<EbAdditionalCost> getListAdditionalCosts(SearchCriteria criteria);

    public Long getCountListAdditionalCosts(SearchCriteria criteria);
}
