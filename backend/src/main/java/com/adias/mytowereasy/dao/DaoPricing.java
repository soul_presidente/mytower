/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.airbus.model.EbQrGroupe;
import com.adias.mytowereasy.model.EbAdresse;
import com.adias.mytowereasy.model.EbCostCenter;
import com.adias.mytowereasy.model.EbCustomsInformation;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


public interface DaoPricing {
    List<EbDemande> getListEbDemandeInitials(SearchCriteriaPricingBooking criterias);

    List<EbDemande> getListEbDemande(SearchCriteriaPricingBooking criteria, EbUser connecter) throws Exception;

    List<EbDemande> getListEbDemandeFavoris(SearchCriteriaPricingBooking criteria) throws Exception;

    Long getCountListEbDemande(SearchCriteriaPricingBooking criteria) throws Exception;

    Integer deleteDemande(SearchCriteriaPricingBooking criteria) throws Exception;

    List<ExEbDemandeTransporteur> getListExEbTransporteur(SearchCriteriaPricingBooking transportcriteria);

    List<EbCostCenter> getListCostcenter(String term);

    EbDemande getEbDemande(SearchCriteriaPricingBooking criteria, EbUser userConnected) throws Exception;

    Integer nextValEbDemande();

    List<EbDemande> getListModeTransport(String term);

    List<EbUser> getListCarrier(String term);

    EbCustomsInformation getCustomsInformation(Integer ebDemandeNum);

    List<EbDemande> getListTransportRef(String term, Integer module);

    List<EbDemande> getListCustomerReference(String term, Integer module);

    List<EbUser> getListChargeur(String term);

    List<EbAdresse> getListEbAdresseEligibility(SearchCriteria criteria, String eligibility);

    List<EbQrGroupe> getListQrGroupe(SearchCriteria criteria, boolean active);

    Long getListQrGroupeCount(SearchCriteria criteria);

    List<EbDemande> getListDemandeGroupe(SearchCriteriaPricingBooking criteria);

    List<EbAdresse> getAllAdresseByZone(Integer ebZoneNum);

    void setPropositionGroupe(EbQrGroupe ebQrGroupe, Integer ebComagnieChargNum);

    List<Integer> getListDemandeByGroupingRule(Integer ebQrGroupeNum);

    List<EbUser> getAllUsersIntervenantInDemande(Integer ebDemandeNum);

		List<EbQrGroupe> getGroupsAndPropositions(SearchCriteria criteria, boolean isWithPropositions);
}
