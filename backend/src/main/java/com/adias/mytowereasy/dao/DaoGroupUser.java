package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EbUserGroup;


public interface DaoGroupUser {
    public List<EbUserGroup> getListUserGroup(EbUser ebUser);
}
