/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;

import javax.persistence.Parameter;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.dsl.Expressions;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.QEbDemande;
import com.adias.mytowereasy.model.QEbEtablissement;
import com.adias.mytowereasy.model.QExEbDemandeTransporteur;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Transactional
public class Dao {
    @Autowired
    private ConnectedUserService connectedUserService;

    public static final String SCHEMA = "work.";

    public static String toPostgresDate(String value) {
        String sepr = "";

        if (value.contains("/")) {
            sepr = "/";
        }
        else if (value.contains("-")) {
            sepr = "-";
        }

        String[] splitDate = value.split(sepr);

        if (sepr != "" && splitDate.length > 1) {

            switch (splitDate.length) {
                case 2:
                    if (splitDate[1].length() == 4) return splitDate[1] + "-" + splitDate[0];
                    else return splitDate[0] + "-" + splitDate[1];
                case 3:
                    if (splitDate[2].length() == 4) return splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];
                    else return splitDate[0] + "-" + splitDate[1] + "-" + splitDate[2];
                default:
                    return value;
            }

        }
        else {
            return value;
        }

    }

    protected Pageable createPageRequest(SearchCriteria criteria) {
        if (criteria.getPageNumber() != null
            && criteria.getSize() != null) return PageRequest.of(criteria.getPageNumber(), criteria.getSize());
        else return null;
    }

    protected String convert(Object value) {
        if (value instanceof Timestamp) return convert((Timestamp) value);
        if (value instanceof Date) return convert((Date) value);
        if (value instanceof String) return convert((String) value);
        if (value instanceof Integer) return convert((Integer) value);
        if (value instanceof Long) return convert((Long) value);
        if (value instanceof Short) return convert((Short) value);
        if (value instanceof Character) return convert((Character) value);
        if (value instanceof Double) return convert((Double) value);

        if (value instanceof BigDecimal) {
            Double d = null;
            if (value != null) d = ((BigDecimal) value).doubleValue();
            return convert(d);
        }

        if (value instanceof Float) return convert((Float) value);
        if (value instanceof Time) return convert((Time) value);

        return null;
    }

    protected String convert(Timestamp timestamp) {
        return convertTimeStamp(timestamp);
    }

    protected String convert(Date date) {
        return convertDate(date);
    }

    protected String convert(Boolean bool) {
        return convertBoolean(bool);
    }

    protected String convert(String str) {
        if (str == null) return "null";
        else return convertString(str);
    }

    protected String convert(Integer integer) {
        if (integer == null) return "null";
        return "" + integer;
    }

    protected String convert(Long lg) {
        if (lg == null) return "null";
        return "" + lg;
    }

    protected String convert(Short shrt) {
        if (shrt == null) return "null";
        return "" + shrt;
    }

    protected String convert(Character chr) {
        if (chr == null) return "null";
        return "'" + chr + "'";
    }

    protected String convert(Double dbl) {
        if (dbl == null || Double.isNaN(dbl)) return "null";

        String sTemp = "" + dbl;
        return sTemp.replace(',', '.');
    }

    protected String convert(BigDecimal dbl) {
        if (dbl == null) return "null";

        String sTemp = "" + dbl;
        return sTemp.replace(',', '.');
    }

    protected String convert(Float dbl) {
        if (dbl == null || Double.isNaN(dbl)) return "null";

        String sTemp = "" + dbl;
        return sTemp.replace(',', '.');
    }

    public String convertTimeStamp(Timestamp timestamp) {
        String sTimestamp = "";

        if (timestamp == null) sTimestamp = "null";
        else {
            sTimestamp = timestamp.toString();

            sTimestamp = sTimestamp.substring(0, sTimestamp.indexOf('.'));
            sTimestamp = "'" + sTimestamp + "'";
        }

        return sTimestamp;
    }

    public String convertTime(Time time) {
        String sDate = "";

        if (time == null) sDate = "null";
        else {
            sDate = time.toString();
            // sDate = sDate.substring(0, sDate.indexOf('.'));
            sDate = "'" + sDate + "'";
        }

        return sDate;
    }

    public String convertDate(Date date) {
        String sDate = "";

        if (date == null) sDate = "null";
        else {
            sDate = date.toString();
            // sDate = sDate.substring(0, sDate.indexOf('.'));
            sDate = "'" + sDate + "'";
        }

        return sDate;
    }

    private String convertBoolean(Boolean bool) {
        String sBoolean = "null";

        if (bool == null) return "null";
        if (bool) sBoolean = "true";
        else sBoolean = "false";

        return sBoolean;
    }

    public String convertString(String str) {
        if (str == null) return "null";
        else return "'" + str.replaceAll("'", "''") + "'";
    }

    public String convertStringForLike(String str) {
        return str.replaceAll("'", "''");
    }

    protected String discardNull(String str) {
        String sResult = "";

        if (str != null) sResult = str;
        return sResult;
    }

    protected static String getResultQuery(Query query, String sqlQuery) {
        String req = sqlQuery + "";
        java.util.Set<Parameter<?>> params = query.getParameters();

        for (Parameter<?> p: params) {
            String paramName = p.getName();
            req = req.replaceAll(":" + paramName, query.getParameterValue(paramName) + "");
        }

        return req;
    }

    protected BooleanBuilder applyVisibilityRules(
        EbUser connectedUser,
        Integer module,
        QEbEtablissement qEbEtablissementCt,
        QEbEtablissement qEtablissementQuote,
        QEbEtablissement qChargeurEtab,
        QEbCompagnie qChargeurComp,
        QEbDemande qEbDemande,
        QEbEtablissement qUserDestEtab,
        QEbEtablissement qUserOriginEtab,
        QEbEtablissement qUserOriginCBEtab,
        QEbEtablissement qUserDestCBEtab,
        QEbCompagnie qCompagnieTransporteur,
        QExEbDemandeTransporteur qExDemandeTransporteur) {
        BooleanBuilder boolLabels, where = new BooleanBuilder();
        if (connectedUser == null
            || connectedUser.getEbUserNum() == null) connectedUser = connectedUserService.getCurrentUser();

        // cas spécial: si le control tower est créé dans la compagnie du chargeur
        if (connectedUser.isControlTower() && connectedUser.getEbCompagnie().getEbCompagnieNum() != -1) {
					if (connectedUser.isSuperAdmin())
						where
							.andAnyOf(
								qChargeurComp.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum())
							);

					else if (connectedUser.isAdmin())
					//else
						where
							.and(
								qChargeurEtab.ebEtablissementNum
									.eq(connectedUser.getEbEtablissement().getEbEtablissementNum())
							);
					else
						where
	                    .andAnyOf(
	                        // objets que le CT/collegue a créé pour le compte d'un
	                        // chargeur de sa
	                        // communauté
	                        qEbEtablissementCt.ebEtablissementNum
	                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
	                        // objets pour les quelles il est sollicité
	                        qEtablissementQuote.ebEtablissementNum
	                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
	                        qExDemandeTransporteur.xTransporteur().ebUserNum.eq(connectedUser.getEbUserNum()));
				}

        // si l'user connecté est un chargeur et n'est pas un super Admin
        if (connectedUser != null && !(connectedUser.isControlTower() && connectedUser.isSuperCt())) {

            if (connectedUser.isChargeur()) {
                where.andAnyOf(qChargeurComp.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));

                if (!connectedUser.isSuperAdmin()) {
                    boolLabels = new BooleanBuilder();

                    if (connectedUser.getListCategories() != null) {
                        Gson gson = new Gson();
                        String categories = gson.toJson(connectedUser.getListCategories());

                        if (categories != null && !categories.isEmpty()) {
                            boolLabels
                                .and(
                                    Expressions
                                        .booleanTemplate(
                                            Dao.SCHEMA + "is_userlabels_contains_demlabels({0}, {1}) = true",
                                            categories,
                                            qEbDemande.listCategories))
                                .or(qEbDemande.user().ebUserNum.eq(connectedUser.getEbUserNum()));
                        }

                    }

                    BooleanBuilder etabConditions = new BooleanBuilder()
                        .orAllOf(
                            qChargeurEtab.ebEtablissementNum
                                .ne(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                            new BooleanBuilder()
                                .andAnyOf(
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qUserOriginEtab.ebEtablissementNum.isNotNull(),
                                            qUserOriginEtab.ebEtablissementNum
                                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())),
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qUserDestEtab.ebEtablissementNum.isNotNull(),
                                            qUserDestEtab.ebEtablissementNum
                                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()))));

                    // la visibilite du module T&T etait gere comme l'admin (a
                    // rediscute)
                    if (connectedUser.isAdmin() || (Enumeration.Module.TRACK.getCode().equals(module))) {
                        where
                            .andAnyOf(
                                new BooleanBuilder()
                                    .orAllOf(
                                        qChargeurEtab.ebEtablissementNum
                                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                        boolLabels),
                                etabConditions);
                    }
                    else {
                        where
                            .andAnyOf(
                                qChargeurEtab.ebEtablissementNum
                                    .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                etabConditions)
                            .and(boolLabels);
                    }

                }

            }
						else if (
							connectedUser.isControlTower() &&
								connectedUser.getEbCompagnie().getEbCompagnieNum() == -1
						)
						{
                where
                    .andAnyOf(
                        // objets que le CT/collegue a créé pour le compte d'un
                        // chargeur de sa
                        // communauté
                        qEbEtablissementCt.ebEtablissementNum
                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                        // objets pour les quelles il est sollicité
                        qEtablissementQuote.ebEtablissementNum
                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                        qExDemandeTransporteur.xTransporteur().ebUserNum.eq(connectedUser.getEbUserNum()));
            }

        }

        if (module != null) {
            BooleanBuilder whereBroker = new BooleanBuilder();
            whereBroker
                .andAnyOf(
                    new BooleanBuilder()
                        .orAllOf(
                            new BooleanBuilder()
                                .andAnyOf(
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qUserOriginCBEtab.ebEtablissementNum.isNotNull(),
                                            qUserOriginCBEtab.ebEtablissementNum
                                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())),
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qUserDestCBEtab.ebEtablissementNum.isNotNull(),
                                            qUserDestCBEtab.ebEtablissementNum
                                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())))));

						if (
							Enumeration.Module.PRICING.getCode().equals(module) && connectedUser.isPrestataire()
						)
						{

                if (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker()) {

                    if (connectedUser.isSuperAdmin()) {
                        where
                            .andAnyOf(
                                whereBroker,
                                new BooleanBuilder()
                                    .or(
                                        qCompagnieTransporteur.ebCompagnieNum
                                            .eq(connectedUser.getEbCompagnie().getEbCompagnieNum())));
                    }
                    else {
                        where
                            .andAnyOf(
                                whereBroker,
                                new BooleanBuilder()
                                    .orAllOf(
                                        qEtablissementQuote.ebEtablissementNum
                                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                        qExDemandeTransporteur.isFromTransPlan.isNull()),
                                new BooleanBuilder()
                                    .orAllOf(
                                        qEtablissementQuote.ebEtablissementNum
                                            .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                        qExDemandeTransporteur.isFromTransPlan.isNotNull(),
                                        qExDemandeTransporteur.isFromTransPlan.isFalse()),
                                qEtablissementQuote.ebEtablissementNum
                                    .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                qExDemandeTransporteur.xTransporteur().ebUserNum.eq(connectedUser.getEbUserNum()));
                    }

                }

                if (connectedUser.isBroker()) {
                    where.andAnyOf(whereBroker);
                }

								where.and(qEbDemande.xEcStatut.ne(Enumeration.StatutDemande.PND.getCode()));

            }

            if (Enumeration.Module.TRANSPORT_MANAGEMENT.getCode().equals(module)
                || Enumeration.Module.ORDRE_TRANSPORT.getCode().equals(module)) {

                if (connectedUser.isTransporteur() || connectedUser.isTransporteurBroker()) {

                    if (connectedUser.isSuperAdmin()) {

                        if (Enumeration.Module.ORDRE_TRANSPORT.getCode().equals(module)) {
                            where
                                .andAnyOf(
                                    whereBroker,
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qCompagnieTransporteur.ebCompagnieNum
                                                .eq(connectedUser.getEbCompagnie().getEbCompagnieNum()),
                                            qExDemandeTransporteur.status.eq(Enumeration.StatutCarrier.FIN.getCode())));
                        }
                        else {
                            where
                                .andAnyOf(
                                    whereBroker,
                                    new BooleanBuilder()
                                        .or(
                                            qCompagnieTransporteur.ebCompagnieNum
                                                .eq(connectedUser.getEbCompagnie().getEbCompagnieNum())));
                        }

                    }
                    else {

                        if (Enumeration.Module.ORDRE_TRANSPORT.getCode().equals(module)) {
                            where
                                .andAnyOf(
                                    whereBroker,
                                    new BooleanBuilder()
                                        .orAllOf(
                                            qEtablissementQuote.ebEtablissementNum
                                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum()),
                                            qExDemandeTransporteur.status.eq(Enumeration.StatutCarrier.FIN.getCode())));
                        }
                        else {
                            where
                                .andAnyOf(
                                    whereBroker,
                                    new BooleanBuilder()
                                        .or(
                                            qEtablissementQuote.ebEtablissementNum
                                                .eq(connectedUser.getEbEtablissement().getEbEtablissementNum())),
                                    qExDemandeTransporteur.xTransporteur().ebUserNum.eq(connectedUser.getEbUserNum()));
                        }

                    }

                }

                if (connectedUser.isBroker()) where.and(whereBroker);

                // ce code est commenter pour fixer un soucis lié a la vu d'un
                // ct sur TM
                /*
                 * if
                 * (!Enumeration.Module.ORDRE_TRANSPORT.getCode().equals(module)
                 * )
                 * {
                 * where
                 * .and(
                 * qExDemandeTransporteur.status
                 * .in(
                 * Enumeration.StatutCarrier.FIN.getCode(),
                 * Enumeration.StatutCarrier.FIN_PLAN.getCode()
                 * )
                 * );
                 * }
                 */
            }

        }

        return where;
    }
}
