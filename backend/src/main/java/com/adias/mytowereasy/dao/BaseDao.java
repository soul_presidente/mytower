package com.adias.mytowereasy.dao;

import java.lang.reflect.Field;

import com.adias.mytowereasy.dto.EntityProjectionField;


public abstract class BaseDao {
    public String getOriginQueryDSLProjectionField(Class<?> projectionObjectClass, String projectionField) {

        try {
            Field f = projectionObjectClass.getDeclaredField(projectionField);

            EntityProjectionField annot = f.getAnnotation(EntityProjectionField.class);
            if (annot == null) return projectionField;

            return annot.value();
        } catch (NoSuchFieldException e) {
            return projectionField;
        }

    }
}
