/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao;

import java.math.BigInteger;
import java.util.List;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoUser {
    public List<EbUser> selectListEbUser(SearchCriteria criterias);

    public boolean updateUserGroup(SearchCriteria criterias, Integer ebUserNum);

    public long selectCountListUser(SearchCriteria criteria);

    public Object selectListContact(SearchCriteria criteria);

    public List<EbUser> selectListEtablissementResquest(SearchCriteria criterias);

    public List<EbUser> selectListPrestataireCommunity(SearchCriteria criterias);

    public Boolean updatePasswordEbUser(EbUser ebUser);

    public BigInteger selectCountListContact(SearchCriteria criterias);

    public EbUser findUser(SearchCriteria criterias);

    List<EbUser> listDestinataireByEmailId(
        Integer entityId,
        Integer mailId,
        Integer module,
        EbUser transporteur,
        Integer ebTtEventNum,
        Integer ebTtTracingNum,
        Integer ebTtEventUserNum,
        boolean ignoreMailParamCheck);
}
