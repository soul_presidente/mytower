package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoAdditionalCost;
import com.adias.mytowereasy.model.EbAdditionalCost;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbAdditionalCost;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoAdditionalCostImpl implements DaoAdditionalCost {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbAdditionalCost qEbAdditionalCost = QEbAdditionalCost.ebAdditionalCost;
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbZoneCompagnie");

    @Override
    public List<EbAdditionalCost> getListAdditionalCosts(SearchCriteria criteria) {
        List<EbAdditionalCost> results = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbAdditionalCost> query = new JPAQuery<>(em);

            query.select(qEbAdditionalCost);
            query.from(qEbAdditionalCost);
            where = this.getGlobalWhere(criteria);
            query.where(where);
            query = this.getQueryGlobalJoin(query, criteria);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbAdditionalCost, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbAdditionalCost.libelle.asc());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            results = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results;
    }

    @Override
    public Long getCountListAdditionalCosts(SearchCriteria criteria) {
        Long results = new Long(0);

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbAdditionalCost> query = new JPAQuery<>(em);

            query.select(qEbAdditionalCost);
            query.from(qEbAdditionalCost);
            where = this.getGlobalWhere(criteria);
            query.where(where);
            query = this.getQueryGlobalJoin(query, criteria);

            results = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return results;
    }

    private BooleanBuilder getGlobalWhere(SearchCriteria criteria) {
        BooleanBuilder where = new BooleanBuilder();

        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);

        Integer ebCompagnieNum = criteria.getEbCompagnieNum() != null ?
            criteria.getEbCompagnieNum() :
            connectedUser.getEbCompagnie().getEbCompagnieNum();

        where.and(qEbCompagnie.ebCompagnieNum.eq(ebCompagnieNum));

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qEbAdditionalCost.libelle.toLowerCase().contains(term),
                    qEbAdditionalCost.code.toLowerCase().contains(term));
        }

        return where;
    }

    private JPAQuery getQueryGlobalJoin(JPAQuery query, SearchCriteria criteria) {
        query.leftJoin(qEbAdditionalCost.xEbCompagnie(), qEbCompagnie);
        return query;
    }
}
