/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoSchemaPsl;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.model.tt.QEbTtSchemaPsl;
import com.adias.mytowereasy.service.ConnectedUserService;


@Component
@Transactional
public class DaoShemaPslImp extends Dao implements DaoSchemaPsl {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbTtSchemaPsl qEbTtSchemaPsl = QEbTtSchemaPsl.ebTtSchemaPsl;

    @Override
    public List<EbTtSchemaPsl> getListSchemaPsl() {
        // TODO Auto-generated method stub
        List<EbTtSchemaPsl> resultat = new ArrayList<EbTtSchemaPsl>();

        try {
            JPAQuery<EbTtSchemaPsl> query = new JPAQuery<EbTtSchemaPsl>(em);

            query
                .select(
                    QEbTtSchemaPsl
                        .create(
                            qEbTtSchemaPsl.ebTtSchemaPslNum,
                            qEbTtSchemaPsl.configPsl,
                            qEbTtSchemaPsl.xEcModeTransport));

            query.distinct();
            query.from(qEbTtSchemaPsl);

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }
}
