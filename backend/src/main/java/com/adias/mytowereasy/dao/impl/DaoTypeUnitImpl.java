package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoTypeUnit;
import com.adias.mytowereasy.dto.EbTypeMarchandiseDto;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbMarchandiseRepository;
import com.adias.mytowereasy.repository.EbTypeUnitRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteriaContainerType;
import com.adias.mytowereasy.utils.search.SearchCriteriaTypeUnit;


@Component
@Transactional
public class DaoTypeUnitImpl implements DaoTypeUnit {
    @PersistenceContext
    EntityManager em;

    @Autowired
    private ConnectedUserService userService;

    @Autowired
    private EbTypeUnitRepository typeUnitRepository;

    @Autowired
    private EbMarchandiseRepository ebMarchandiseRepository;

    QEbTypeUnit qEbTypeUnit = QEbTypeUnit.ebTypeUnit;
    QEbCompagnie qEbCompagnie = QEbCompagnie.ebCompagnie;
    QEbMarchandise qEbMarchandise = QEbMarchandise.ebMarchandise;
    QEbDemande qEbDemande = QEbDemande.ebDemande;

    private Map<String, OrderSpecifier<?>> orderMapASC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, OrderSpecifier<?>> orderMapDESC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, Path<?>> orderMap = new HashMap<String, Path<?>>();

    private void initialize() {
        orderMap.clear();
        orderMapASC.clear();
        orderMapDESC.clear();

        orderMap.put("ebTypeUnitNum", qEbTypeUnit.ebTypeUnitNum);
        orderMap.put("libelle", qEbTypeUnit.libelle);
        orderMap.put("weight", qEbTypeUnit.weight);
        orderMap.put("length", qEbTypeUnit.length);
        orderMap.put("width", qEbTypeUnit.width);
        orderMap.put("height", qEbTypeUnit.height);
        orderMap.put("gerbable", qEbTypeUnit.gerbable);
        orderMap.put("containerLabel", qEbTypeUnit.xEbTypeContainer);
        orderMap.put("activated", qEbTypeUnit.activated);

        orderMapASC.put("ebTypeUnitNum", qEbTypeUnit.ebTypeUnitNum.asc());
        orderMapASC.put("libelle", qEbTypeUnit.libelle.asc());
        orderMapASC.put("weight", qEbTypeUnit.weight.asc());
        orderMapASC.put("length", qEbTypeUnit.length.asc());
        orderMapASC.put("width", qEbTypeUnit.width.asc());
        orderMapASC.put("height", qEbTypeUnit.height.asc());
        orderMapASC.put("gerbable", qEbTypeUnit.gerbable.asc());
        orderMapASC.put("containerLabel", qEbTypeUnit.xEbTypeContainer.asc());
        orderMapASC.put("activated", qEbTypeUnit.activated.asc());

        orderMapDESC.put("ebTypeUnitNum", qEbTypeUnit.ebTypeUnitNum.desc());
        orderMapDESC.put("libelle", qEbTypeUnit.libelle.desc());
        orderMapDESC.put("weight", qEbTypeUnit.weight.desc());
        orderMapDESC.put("length", qEbTypeUnit.length.desc());
        orderMapDESC.put("width", qEbTypeUnit.width.desc());
        orderMapDESC.put("height", qEbTypeUnit.height.desc());
        orderMapDESC.put("gerbable", qEbTypeUnit.gerbable.desc());
        orderMapDESC.put("containerLabel", qEbTypeUnit.xEbTypeContainer.desc());
        orderMapDESC.put("activated", qEbTypeUnit.activated.desc());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EbTypeUnit> getListTypeUnit(SearchCriteriaTypeUnit criteria) {
        List<EbTypeUnit> result = null;
        BooleanBuilder where = new BooleanBuilder();

        try {
            JPAQuery<EbTypeUnit> query = new JPAQuery<EbTypeUnit>(em);
            query.from(qEbTypeUnit);
            query = getGlobalWhere(query, where, criteria);
            query = getGlobalJoin(query, where, criteria);
            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Boolean checkIfExistTypeUnitByCompagnie(SearchCriteriaTypeUnit criteria) {
        Boolean result = false;

        try {
            EbUser user = userService.getCurrentUserFromDB();
            JPAQuery<EbTypeUnit> query = new JPAQuery<EbTypeUnit>(em);
            query.from(qEbTypeUnit);
            query.join(qEbTypeUnit.xEbCompagnie(), qEbCompagnie);
            if (user != null) query.where(qEbCompagnie.ebCompagnieNum.eq(user.getEbCompagnie().getEbCompagnieNum()));

            query.where(qEbTypeUnit.code.eq(criteria.getTypeUnitCode()));

            List<EbTypeUnit> listUnit = query.fetch();

            // On teste si le code qui existe est lié au unit en cours d'edition
            // ou pas
            // Si oui (false) pas de message code existe dejà
            // si non (true) alors, envoie de message code existe
            if (listUnit.size() > 0) {
                EbTypeUnit unit = listUnit.get(0);
                result = (unit.getEbTypeUnitNum().equals(criteria.getEbUnitNum())) ? false : true;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getListTypeUnitCount(SearchCriteriaTypeUnit criteria) {
        Long result = new Long(0);
        BooleanBuilder where = new BooleanBuilder();

        try {
            JPAQuery<EbTypeUnit> query = new JPAQuery<EbTypeUnit>(em);
            query.from(qEbTypeUnit);
            query = getGlobalWhere(query, where, criteria);
            query = getGlobalJoin(query, where, criteria);
            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public EbTypeUnit saveTypeUnit(EbTypeUnit typeUnit) {

        try {

            if (typeUnit.getEbTypeUnitNum() == null) {
                typeUnit.setxEbCompagnie(userService.getCurrentUser().getEbCompagnie());
            }

            typeUnitRepository.save(typeUnit);
            return typeUnit;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public boolean deleteTypeUnit(EbTypeUnit typeUnit) {

        try {
            typeUnitRepository.delete(typeUnit);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public List<EbContainerType> getListContainerType(SearchCriteriaContainerType criteria) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void activateDeactivateTypeUnit(Long ebTypeUnitNum, boolean activated) {

        try {
            typeUnitRepository.activateDeactivateTypeUnit(ebTypeUnitNum, activated);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<EbTypeMarchandiseDto>
        checkListTypeMarchandise(List<EbTypeMarchandiseDto> resultList, Integer idDemande) {
        List<EbMarchandise> marchandises = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbMarchandise> query = new JPAQuery<EbMarchandise>(em);

            query
                .select(
                    QEbMarchandise
                        .create(
                            qEbMarchandise.xEbTypeUnit,
                            qEbMarchandise.labelTypeUnit,
                            qEbMarchandise.xEbTypeConteneur))
                .from(qEbMarchandise).leftJoin(qEbMarchandise.ebDemande(), qEbDemande);

            where.and(qEbDemande.ebDemandeNum.eq(idDemande));
            query.where(where);
            marchandises = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (EbMarchandise march: marchandises) {

            if ((march.getxEbTypeUnit() != null && march.getLabelTypeUnit() != null)) {
                boolean verify = false;
                int indexSelected = -1;

                for (EbTypeMarchandiseDto type: resultList) {

                    if (march.getxEbTypeUnit().equals(type.getxEbTypeUnit())) {
                        verify = true;
                        indexSelected = resultList.indexOf(type);
                        break;
                    }

                }

                if (!verify) {
                    EbTypeMarchandiseDto newEbTypeMarchandiseDto = new EbTypeMarchandiseDto();
                    newEbTypeMarchandiseDto.setLabel(march.getLabelTypeUnit());
                    newEbTypeMarchandiseDto.setxEbTypeUnit(march.getxEbTypeUnit());

                    if (march.getxEbTypeConteneur() != null) {
                        newEbTypeMarchandiseDto.setxEbTypeConteneur(march.getxEbTypeConteneur());
                    }

                    resultList.add(newEbTypeMarchandiseDto);
                }
                else {
                    EbTypeMarchandiseDto typeSelected = resultList.get(indexSelected);

                    if (!march.getLabelTypeUnit().equals(typeSelected.getLabel())) {
                        typeSelected.setLabel(march.getLabelTypeUnit());
                    }

                }

            }

        }

        return resultList;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    private JPAQuery getGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteriaTypeUnit criteria) {
        initialize();

        if (criteria.getSearchterm() != null) {
            String searchterm = criteria.getSearchterm().trim().toLowerCase();

            try {

                if (searchterm.equals("true") || searchterm.equals("false")) {
                    Boolean activeValue = Boolean.parseBoolean(searchterm);
                    where.andAnyOf(qEbTypeUnit.activated.eq(activeValue));
                }
                else {
                    Double value = Double.parseDouble(searchterm);
                    where
                        .andAnyOf(
                            qEbTypeUnit.libelle.toLowerCase().contains(searchterm),
                            qEbTypeUnit.weight.eq(value),
                            qEbTypeUnit.length.eq(value),
                            qEbTypeUnit.width.eq(value),
                            qEbTypeUnit.height.eq(value),
                            qEbTypeUnit.code.contains(searchterm));
                }

            } catch (NumberFormatException e) {
                where
                    .andAnyOf(
                        qEbTypeUnit.libelle.toLowerCase().contains(searchterm),
                        qEbTypeUnit.code.contains(searchterm));
            }

        }

        if (criteria.getEbCompagnieNum() != null) {
            where.and(qEbCompagnie.ebCompagnieNum.eq(criteria.getEbCompagnieNum()));
        }

        if (criteria.getLibelle() != null) {
            where.and(qEbTypeUnit.libelle.eq(criteria.getLibelle()));
        }

        if (criteria.getCode() != null) {
            where.and(qEbTypeUnit.code.eq(criteria.getCode()));
        }

        query.where(where);

        if (criteria.getPageNumber() != null && criteria.getSize() != null && !criteria.isCount()) {
            query
                .limit(criteria.getSize())
                .offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
        }

        if (criteria.getOrderedColumn() != null && !criteria.isCount()) {
            query.groupBy(qEbTypeUnit.ebTypeUnitNum);

            if (criteria.getAscendant() == true) {
                query.groupBy(qEbTypeUnit.ebTypeUnitNum);
                query.groupBy(this.orderMap.get(criteria.getOrderedColumn()));
                query.orderBy(this.orderMapASC.get(criteria.getOrderedColumn()));
            }
            else {
                query.groupBy(qEbTypeUnit.ebTypeUnitNum);
                query.groupBy(this.orderMap.get(criteria.getOrderedColumn()));
                query.orderBy(this.orderMapDESC.get(criteria.getOrderedColumn()));
            }

        }
        else {
            query.groupBy(qEbTypeUnit.ebTypeUnitNum);
            query.orderBy(this.orderMapDESC.get("ebTypeUnitNum"));
        }

        return query;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    private JPAQuery getGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteriaTypeUnit criteria) {
        query.leftJoin(qEbTypeUnit.xEbCompagnie(), qEbCompagnie);
        return query;
    }
}
