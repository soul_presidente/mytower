package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoTemplateDocuments;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoTemplateDocumentsImpl implements DaoTemplateDocuments {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbTemplateDocuments qEbTemplateDocuments = QEbTemplateDocuments.ebTemplateDocuments;
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbCompagnie");
    QEbTypeDocuments qEbTypeDocuments = new QEbTypeDocuments("qEbTypeDocuments");

    @Override
    public List<EbTemplateDocuments> searchEbTemplateDocuments(SearchCriteria criteria) {
        List<EbTemplateDocuments> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbTemplateDocuments> query = new JPAQuery<>(em);

            // temporary deleted to fix error in settings
            /*
             * query.select(QEbTemplateDocuments.create(
             * qEbTemplateDocuments.ebTemplateDocumentsNum,
             * qEbTemplateDocuments.libelle,
             * qEbTemplateDocuments.reference,
             * qEbTemplateDocuments.dateAjout,
             * qEbTemplateDocuments.dateMaj,
             * qEbTemplateDocuments.typesDocument,
             * qEbTemplateDocuments.originFileName,
             * qEbTemplateDocuments.cheminDocument,
             * qEbTemplateDocuments.xEbUser(),
             * qEbCompagnie
             * ));
             */
            query = getTemplateDocumentsGlobalWhere(query, where, criteria);
            query.distinct().from(qEbTemplateDocuments);
            query = getTemplateDocumentsGlobalJoin(query, where, criteria);
            query.where(where);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions
                    .path(Object.class, qEbTemplateDocuments, criteria.getOrderedColumn());

                query
                    .orderBy(
                        qEbTemplateDocuments.isGlobal.asc(),
                        new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbTemplateDocuments.ebTemplateDocumentsNum.asc());
            }

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public Long countListEbTemplateDocuments(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbTemplateDocuments> query = new JPAQuery<EbTemplateDocuments>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getTemplateDocumentsGlobalWhere(query, where, criteria);
            query.from(qEbTemplateDocuments);
            query = getTemplateDocumentsGlobalJoin(query, where, criteria);
            query.distinct().where(where);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    private JPAQuery getTemplateDocumentsGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);
        // join with ebDocument
        query.innerJoin(qEbTemplateDocuments.xEbCompagnie(), qEbCompagnie);
        return query;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getTemplateDocumentsGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUserWithCategories(criteria);

        /* User Visibility Rights */
        where.and(qEbCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));

        /* END User Visibility Rights */
        if (!criteria.showGlobals()) {
            where.and(qEbTemplateDocuments.isGlobal.eq(false));
        }

        if (criteria != null && criteria.getSearchterm() != null) {
            where
                .andAnyOf(
                    qEbTemplateDocuments.libelle.contains(criteria.getSearchterm()),
                    qEbTemplateDocuments.reference.contains(criteria.getSearchterm()),
                    qEbTemplateDocuments.originFileName.contains(criteria.getSearchterm()));
        }

        return query;
    }
}
