/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import org.springframework.stereotype.Component;

import com.adias.mytowereasy.dao.DaoDouane;


@Component
public class DaoDouaneImpl extends Dao implements DaoDouane {
}
