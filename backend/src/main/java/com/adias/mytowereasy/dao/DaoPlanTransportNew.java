package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.api.wso.PlanTransportWSO;
import com.adias.mytowereasy.model.EbPlanTransportNew;
import com.adias.mytowereasy.utils.search.SearchCriteria;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


public interface DaoPlanTransportNew {
    public List<EbPlanTransportNew> list(SearchCriteria criteria);

    public Long listCount(SearchCriteria criteria);

    public List<EbPlanTransportNew>
        listLoadPlanIsConstraints(SearchCriteriaPricingBooking criteria, PlanTransportWSO planTransportWSO);
}
