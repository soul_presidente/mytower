/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbCombinaison;
import com.adias.mytowereasy.model.EbDestinationCountry;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoComplianceMatrix {
    List<EbDestinationCountry> selectListEbDestinationCountry(SearchCriteria criterias);

    EbDestinationCountry selectEbDestinationCountry(SearchCriteria criterias);

    List<EbCombinaison> selectListEbCombinaison(SearchCriteria criterias);

    List<EbDestinationCountry> selectListEbDestinationCountryByCompany(SearchCriteria criterias);
}
