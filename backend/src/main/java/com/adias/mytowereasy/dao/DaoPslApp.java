package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.tt.EbTTPslApp;


public interface DaoPslApp {
    public List<EbTTPslApp>
        getListPslAppByRefAndCodeAlpha(List<String> listRef, List<String> listcodeAlpha, Boolean isTransportRef

    );

    public List<EbTTPslApp> getListPslAppByRefTransportAndRefUnitAndCodeAlpha(
        List<String> listRefTransport,
        List<String> listcodeAlpha,
        Boolean isTransportRef,
        List<String> listRefUnit);
}
