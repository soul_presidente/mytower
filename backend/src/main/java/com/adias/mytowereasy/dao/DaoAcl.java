package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbAclRelation;
import com.adias.mytowereasy.model.EcAclRule;
import com.adias.mytowereasy.utils.search.SearchCriteriaACL;


public interface DaoAcl {
    List<EcAclRule> listRuleForSession(SearchCriteriaACL criteria);

    List<EbAclRelation> listRelationForSession(SearchCriteriaACL criteria);
}
