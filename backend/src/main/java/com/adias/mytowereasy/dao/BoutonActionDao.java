package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbBoutonAction;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface BoutonActionDao {
    List<EbBoutonAction> listBoutonAction(SearchCriteria criteria);

    Long countListBoutonAction(SearchCriteria criteria);
}
