package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbTypeDocuments;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoTypeDocuments {
    List<EbTypeDocuments> getListEbTypeDocument(SearchCriteria criteria) throws Exception;

    List<EbTypeDocuments> findByModule(SearchCriteria criteria, EbUser user) throws Exception;

    public Long listCount(SearchCriteria criteria);
}
