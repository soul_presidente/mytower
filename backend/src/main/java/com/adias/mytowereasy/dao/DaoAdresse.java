package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbAdresse;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoAdresse {
    public List<EbAdresse> getListEbAdresse(SearchCriteria criteria);

    public Long getListAddressesCount(SearchCriteria criteria);
}
