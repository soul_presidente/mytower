package com.adias.mytowereasy.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.types.Path;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoMarchandise;
import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.QEbMarchandise;
import com.adias.mytowereasy.utils.search.SearchCriteriaMarchandise;


@Component
@Transactional
public class DaoMarchandiseImpl extends Dao implements DaoMarchandise {
    @PersistenceContext
    EntityManager em;

    QEbMarchandise qEbMarchandise = QEbMarchandise.ebMarchandise;

    private Map<String, Path<?>> autoCompleteFields = new HashMap<String, Path<?>>();

    public DaoMarchandiseImpl() {
        this.initialize();
    }

    private void initialize() {
        autoCompleteFields.put("unitReference", qEbMarchandise.unitReference);
        autoCompleteFields.put("lot", qEbMarchandise.lot);
    }

    @Override
    public List<EbMarchandise> getAutoCompleteList(String term, String field) {
        List<String> result = null;
        List<EbMarchandise> listResult = new ArrayList<>();

        try {
            SearchCriteriaMarchandise criteria = new SearchCriteriaMarchandise();
            criteria = criteria.setProperty(field, term, criteria);
            JPAQuery query = new JPAQuery(em);

            query.select(autoCompleteFields.get(field));
            query.from(qEbMarchandise);
            query = criteria.applyCriteriaAutoComplete(query);

            query.distinct();
            query.limit(500);

            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (String value: result) {
            EbMarchandise marchandise = new EbMarchandise();
            marchandise.setProperty(field, value);
            listResult.add(marchandise);
        }

        return listResult;
    }

    @Override
    public Integer nextValEbMarchandise() {
        Integer result = null;

        try {
            result = ((BigInteger) em
                .createNativeQuery("SELECT NEXTVAL('work.eb_marchandise_eb_marchandise_num_seq')").getSingleResult())
                    .intValue();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
}
