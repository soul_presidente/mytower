package com.adias.mytowereasy.dao.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.DateTemplate;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoCurrency;
import com.adias.mytowereasy.model.EbCompagnieCurrency;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.QEbCompagnieCurrency;
import com.adias.mytowereasy.model.QEbEtablissement;
import com.adias.mytowereasy.model.QEbRelation;
import com.adias.mytowereasy.model.QEbUser;
import com.adias.mytowereasy.model.QEcCurrency;
import com.adias.mytowereasy.model.Statiques;
import com.adias.mytowereasy.repository.EbEtablissementRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Transactional
public class DaoCurrencyImpl implements DaoCurrency {
    @Autowired
    private EbEtablissementRepository ebEtablissementRepository;

    @PersistenceContext
    EntityManager em;

    @Autowired
    private ConnectedUserService connectedUserService;

    QEbEtablissement qEbEtablissement = QEbEtablissement.ebEtablissement;
    QEbRelation qEbRelation = QEbRelation.ebRelation;
    QEbUser qEbUser = QEbUser.ebUser;
    QEbUser qEbUserHost = new QEbUser("userHost");
    QEbUser qEbUserGuest = new QEbUser("userGuest");
    QEbEtablissement qEbEtabHost = new QEbEtablissement("etabHost");
    QEbEtablissement qEbEtabGuest = new QEbEtablissement("etabGuest");
    QEcCurrency qEcCurrency = new QEcCurrency("qEcCurrency");
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbCompagnie");
    QEbCompagnieCurrency qEbCompagnieCurrency = QEbCompagnieCurrency.ebCompagnieCurrency;
    QEbCompagnie qEbCompagnieGuest = new QEbCompagnie("qEbCompagnieGuest");

    @Override
    public List<EbCompagnieCurrency> selectListEbCompagnieCurrency(SearchCriteria criterias) {
        List<EbCompagnieCurrency> resultat = new ArrayList<EbCompagnieCurrency>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbCompagnieCurrency> query = new JPAQuery<EbCompagnieCurrency>(em);

            if (criterias.getModule() != null) {

							inValidDateRangeWhere(where);

							query
                    .select(
                        QEbCompagnieCurrency
                            .create(
                                qEbCompagnieCurrency.ebCompagnieCurrencyNum,
                                qEbCompagnieCurrency.euroExchangeRate,
                                qEbCompagnieCurrency.dateDebut,
                                qEbCompagnieCurrency.dateFin,
                                qEbCompagnieCurrency.dateCreation,
                                qEbCompagnieCurrency.ecCurrency().ecCurrencyNum,
                                qEbCompagnieCurrency.ecCurrency().libelle,
                                qEbCompagnieCurrency.ecCurrency().code,
                                qEbCompagnieCurrency.ecCurrency().codeLibelle,
                                qEbCompagnie.ebCompagnieNum,
                                qEbCompagnie.nom,
                                qEbCompagnie.code,
                                qEbCompagnieCurrency.orderSort,
                                qEbCompagnieCurrency.xecCurrencyCible().ecCurrencyNum,
                                qEbCompagnieCurrency.xecCurrencyCible().libelle,
                                qEbCompagnieCurrency.xecCurrencyCible().code,
                                qEbCompagnieCurrency.ebCompagnieGuest().ebCompagnieNum,
                                qEbCompagnieGuest.nom,
                                qEbCompagnieGuest.code,
                                qEbCompagnieCurrency.isUsed

                            ));
            }
            else {
							query
								.select(
									QEbCompagnieCurrency
										.create(
											qEbCompagnieCurrency.ebCompagnieCurrencyNum,
											qEbCompagnieCurrency.euroExchangeRate,
											qEbCompagnieCurrency.dateDebut,
											qEbCompagnieCurrency.dateFin,
											qEbCompagnieCurrency.dateCreation,
											qEbCompagnieCurrency.ecCurrency().ecCurrencyNum,
											qEbCompagnieCurrency.ecCurrency().libelle,
											qEbCompagnieCurrency.ecCurrency().code,
											qEbCompagnieCurrency.ecCurrency().codeLibelle,
											qEbCompagnie.ebCompagnieNum,
											qEbCompagnie.nom,
											qEbCompagnie.code,
											qEbCompagnieCurrency.orderSort,
											qEbCompagnieCurrency.xecCurrencyCible().ecCurrencyNum,
											qEbCompagnieCurrency.xecCurrencyCible().libelle,
											qEbCompagnieCurrency.xecCurrencyCible().code,
											qEbCompagnieCurrency.xecCurrencyCible().codeLibelle,
											qEbCompagnieCurrency.ebCompagnieGuest().ebCompagnieNum,
											qEbCompagnieGuest.nom,
											qEbCompagnieGuest.code,
											qEbCompagnieCurrency.isUsed
										)
								);
						}

            query.distinct();
            query = getEbCompagnieCurrencyGlobalWhere(query, where, criterias);
            query.from(qEbCompagnieCurrency).where(where);
            query = getEbCompagnieCurrencyGlobalJoin(query, where, criterias);
            query = setPagination(criterias, query);
            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

		private void inValidDateRangeWhere(BooleanBuilder where)
		{
			BooleanExpression isWithDateFin = DateTemplate
				.currentDate()
				.between(qEbCompagnieCurrency.dateDebut, qEbCompagnieCurrency.dateFin);
			
			BooleanExpression isWithoutDateFin = DateTemplate
				.currentDate()
				.after(qEbCompagnieCurrency.dateDebut)
				.and(qEbCompagnieCurrency.dateFin.isNull());
			
			where.andAnyOf(isWithoutDateFin, isWithDateFin);
		}

		@Override
    public List<EbCompagnieCurrency> getListNonComplaiantCompagniecurrency(EbCompagnieCurrency compagnieCurrency) {
        List<EbCompagnieCurrency> resultat = new ArrayList<EbCompagnieCurrency>();

        try {

            if (compagnieCurrency.getDateDebut() != null && compagnieCurrency.getDateFin() != null) {
                SimpleDateFormat datew = new SimpleDateFormat(Statiques.DATE_FORMAT);
                String strDateDebut = datew.format(compagnieCurrency.getDateDebut());
                String strDateFin = datew.format(compagnieCurrency.getDateFin());
            }

            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbCompagnieCurrency> query = new JPAQuery<EbCompagnieCurrency>(em);

            query
                .select(qEbCompagnieCurrency).from(qEbCompagnieCurrency).where(
                    (qEbCompagnieCurrency.ebCompagnie().ebCompagnieNum
                        .eq(compagnieCurrency.getEbCompagnie().getEbCompagnieNum())
                        .and(
                            qEbCompagnieCurrency.ebCompagnieGuest().ebCompagnieNum
                                .eq(compagnieCurrency.getEbCompagnieGuest().getEbCompagnieNum()))
                        .and(
                            qEbCompagnieCurrency.ecCurrency().ecCurrencyNum
                                .eq(compagnieCurrency.getEcCurrency().getEcCurrencyNum()))
                        .and(
                            qEbCompagnieCurrency.xecCurrencyCible().ecCurrencyNum
                                .eq(compagnieCurrency.getXecCurrencyCible().getEcCurrencyNum()))
                        .and(
                            ((qEbCompagnieCurrency.dateDebut
                                .between(compagnieCurrency.getDateDebut(), compagnieCurrency.getDateFin()))
                                    .or(
                                        qEbCompagnieCurrency.dateFin
                                            .between(compagnieCurrency.getDateDebut(), compagnieCurrency.getDateFin())
                                            .or(
                                                (qEbCompagnieCurrency.dateDebut
                                                    .before(compagnieCurrency.getDateDebut()).and(
                                                        qEbCompagnieCurrency.dateFin
                                                            .after(compagnieCurrency.getDateDebut())))))))));

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public List<EbCompagnieCurrency> selectListEbCompagnieCurrencyAndEbCompagnieGuest(SearchCriteria criterias) {
        List<EbCompagnieCurrency> resultat = new ArrayList<EbCompagnieCurrency>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbCompagnieCurrency> query = new JPAQuery<EbCompagnieCurrency>(em);
            BooleanBuilder myCurrency = new BooleanBuilder();

            if (criterias.getEbCompagnieNum() != null && criterias.getEbCompagnieGuestNum() != null) {
                myCurrency
                    .and(
                        qEbCompagnieCurrency.ebCompagnie().ebCompagnieNum
                            .eq(criterias.getEbCompagnieNum()).and(
                                qEbCompagnieCurrency.ebCompagnieGuest().ebCompagnieNum
                                    .eq(criterias.getEbCompagnieGuestNum())));
                myCurrency
                    .or(
                        qEbCompagnieCurrency.ebCompagnie().ebCompagnieNum
                            .eq(criterias.getEbCompagnieGuestNum()).and(
                                qEbCompagnieCurrency.ebCompagnieGuest().ebCompagnieNum
                                    .eq(criterias.getEbCompagnieNum())));

                myCurrency.and(qEbCompagnieCurrency.ecCurrency().ecCurrencyNum.notIn(criterias.getCurrencyCibleNum()));

                myCurrency
                    .and(qEbCompagnieCurrency.xecCurrencyCible().ecCurrencyNum.eq(criterias.getCurrencyCibleNum()));
                where.and(myCurrency);
            }

            if (criterias.getModule() != null) {
                where
                    .and(
                        DateTemplate
                            .currentDate().between(qEbCompagnieCurrency.dateDebut, qEbCompagnieCurrency.dateFin));
                // where.and(qEbCompagnieCurrency.dateDebut.goe(now).and(qEbCompagnieCurrency.dateFin.loe(now)));
                query
                    .select(
                        QEbCompagnieCurrency
                            .create(
                                qEbCompagnieCurrency.ebCompagnieCurrencyNum,
                                qEbCompagnieCurrency.euroExchangeRate,
                                qEbCompagnieCurrency.dateDebut,
                                qEbCompagnieCurrency.dateFin,
                                qEbCompagnieCurrency.dateCreation,
                                qEbCompagnieCurrency.ecCurrency().ecCurrencyNum,
                                qEbCompagnieCurrency.ecCurrency().libelle,
                                qEbCompagnieCurrency.ecCurrency().code,
                                qEbCompagnieCurrency.ecCurrency().codeLibelle,
                                qEbCompagnieCurrency.ebCompagnie().ebCompagnieNum,
                                qEbCompagnieCurrency.ebCompagnie().nom,
                                qEbCompagnieCurrency.ebCompagnie().code,
                                qEbCompagnieCurrency.orderSort,
                                qEbCompagnieCurrency.xecCurrencyCible().ecCurrencyNum,
                                qEbCompagnieCurrency.xecCurrencyCible().libelle,
                                qEbCompagnieCurrency.xecCurrencyCible().code,
                                qEbCompagnieCurrency.ebCompagnieGuest().ebCompagnieNum,
                                qEbCompagnieCurrency.ebCompagnieGuest().nom,
                                qEbCompagnieCurrency.ebCompagnieGuest().code,
                                qEbCompagnieCurrency.isUsed

                            ));
            }
            else {
							query
								.select(
									QEbCompagnieCurrency
										.create(
											qEbCompagnieCurrency.ebCompagnieCurrencyNum,
											qEbCompagnieCurrency.euroExchangeRate,
											qEbCompagnieCurrency.dateDebut,
											qEbCompagnieCurrency.dateFin,
											qEbCompagnieCurrency.dateCreation,
											qEbCompagnieCurrency.ecCurrency().ecCurrencyNum,
											qEbCompagnieCurrency.ecCurrency().libelle,
											qEbCompagnieCurrency.ecCurrency().code,
											qEbCompagnieCurrency.ecCurrency().codeLibelle,
											qEbCompagnieCurrency.ebCompagnie().ebCompagnieNum,
											qEbCompagnieCurrency.ebCompagnie().nom,
											qEbCompagnieCurrency.ebCompagnie().code,
											qEbCompagnieCurrency.orderSort,
											qEbCompagnieCurrency.xecCurrencyCible().ecCurrencyNum,
											qEbCompagnieCurrency.xecCurrencyCible().libelle,
											qEbCompagnieCurrency.xecCurrencyCible().code,
											qEbCompagnieCurrency.xecCurrencyCible().codeLibelle,
											qEbCompagnieCurrency.ebCompagnieGuest().ebCompagnieNum,
											qEbCompagnieCurrency.ebCompagnieGuest().nom,
											qEbCompagnieCurrency.ebCompagnieGuest().code,
											qEbCompagnieCurrency.isUsed

										)
								);
            }

            query.from(qEbCompagnieCurrency);
            query.where(where);

            query.orderBy(qEbCompagnieCurrency.dateCreation.asc());

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public Long getListEbCompagnieCurrencyCount(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbCompagnieCurrency> query = new JPAQuery<EbCompagnieCurrency>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getEbCompagnieCurrencyGlobalWhere(query, where, criteria);

            query.from(qEbCompagnieCurrency).where(where);
            query = getEbCompagnieCurrencyGlobalJoin(query, where, criteria);
            query = setPagination(criteria, query);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private JPAQuery getEbCompagnieCurrencyGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criterias) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

				where.and(qEbCompagnieCurrency.isDeleted.isFalse());

        if (connectedUser == null) {
            connectedUser = connectedUserService.getAvailableCurrentUser(criterias);
        }

        if (criterias.getEbCompagnieNum() != null) {

            if (connectedUser.isChargeur() || connectedUser.isControlTower()) where
                .and(qEbCompagnie.ebCompagnieNum.eq(criterias.getEbCompagnieNum()));
            else {
                where.and(qEbCompagnieGuest.ebCompagnieNum.eq(criterias.getEbCompagnieNum()));
            }

        }

        if (criterias.getEbCompagnieGuestNum()
            != null) where.and(qEbCompagnieGuest.ebCompagnieNum.eq(criterias.getEbCompagnieGuestNum()));

        if (criterias.getEbCompagnieCibleNum()
            != null) where.and(qEbCompagnie.ebCompagnieNum.eq(criterias.getEbCompagnieCibleNum()));

        if (criterias.getCurrencyCibleNum() != null) where
            .and(qEbCompagnieCurrency.xecCurrencyCible().ecCurrencyNum.eq(criterias.getCurrencyCibleNum()));

        if (criterias.getCurrencyGuestNum()
            != null) where.and(qEbCompagnieCurrency.ecCurrency().ecCurrencyNum.eq(criterias.getCurrencyGuestNum()));

				if (criterias.getSearchterm() != null)
				{
					where
						.andAnyOf(
							qEbCompagnieCurrency.ebCompagnie().nom.containsIgnoreCase(criterias.getSearchterm()),
							qEbCompagnieCurrency.ebCompagnieGuest().nom
								.containsIgnoreCase(criterias.getSearchterm()),
							qEbCompagnieCurrency.ebCompagnieGuest().siren
								.containsIgnoreCase(criterias.getSearchterm()),
							qEbCompagnieCurrency.ebCompagnie().siren
								.containsIgnoreCase(criterias.getSearchterm()),
							qEbCompagnieCurrency.ecCurrency().code.containsIgnoreCase(criterias.getSearchterm()),
							qEbCompagnieCurrency.ecCurrency().codeLibelle
								.containsIgnoreCase(criterias.getSearchterm()),
							qEbCompagnieCurrency.ecCurrency().libelle
								.containsIgnoreCase(criterias.getSearchterm()),
							qEbCompagnieCurrency.xecCurrencyCible().code
								.containsIgnoreCase(criterias.getSearchterm()),
							qEbCompagnieCurrency.xecCurrencyCible().codeLibelle
								.containsIgnoreCase(criterias.getSearchterm()),
							qEbCompagnieCurrency.xecCurrencyCible().libelle
								.containsIgnoreCase(criterias.getSearchterm())
						);
				}

        return query;
    }

    private JPAQuery getEbCompagnieCurrencyGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        query.leftJoin(qEbCompagnieCurrency.ecCurrency(), qEcCurrency);
        query.leftJoin(qEbCompagnieCurrency.xecCurrencyCible(), qEcCurrency);

        query.leftJoin(qEbCompagnieCurrency.ebCompagnie(), qEbCompagnie);
        query.leftJoin(qEbCompagnieCurrency.ebCompagnieGuest(), qEbCompagnieGuest);

        return query;
    }

    private JPAQuery<EbCompagnieCurrency> setPagination(SearchCriteria criteria, JPAQuery<EbCompagnieCurrency> query) {

        if (criteria.getOrderedColumn() != null) {
            Path<Object> fieldPath = Expressions.path(Object.class, qEbCompagnieCurrency, criteria.getOrderedColumn());
            query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
        }
        else {
            query.orderBy(qEbCompagnieCurrency.ebCompagnieCurrencyNum.desc());
        }

        if (criteria.getSize() != null && criteria.getSize() >= 0) {
            query.limit(criteria.getSize());
        }

        if (criteria.getPageNumber() != null) {
            query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
        }

        return query;
    }
}
