package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoFlag;
import com.adias.mytowereasy.model.EbFlag;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.QEbFlag;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.PageableSearchCriteria;


@Component
@Transactional
public class DaoFlagImpl extends Dao implements DaoFlag {
    @PersistenceContext
    EntityManager em;
    @Autowired
    ConnectedUserService connectedUserService;

    QEbFlag qEbFlag = QEbFlag.ebFlag;
    QEbCompagnie qEbFlagCompagnie = new QEbCompagnie("qEbFlagCompagnie");

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbFlag> getFlagsByCriteria(PageableSearchCriteria flagSearchCriteria) {
        List<EbFlag> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbFlag> query = new JPAQuery<>(em);

            query
                .select(
                    QEbFlag.create(qEbFlag.ebFlagNum, qEbFlag.name, qEbFlag.reference, qEbFlag.icon, qEbFlag.color));

            query = getFlagGlobalWhere(query, where, flagSearchCriteria);
            query.from(qEbFlag);
            query.where(where);
            query = getFlagGlobalJoin(query, where, flagSearchCriteria);

            if (flagSearchCriteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbFlag, flagSearchCriteria.getOrderedColumn());
                query
                    .orderBy(new OrderSpecifier(flagSearchCriteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbFlag.name.asc());
            }

            if (flagSearchCriteria.getStart() != null) {
                query.offset(flagSearchCriteria.getStart());
            }

            if (flagSearchCriteria.getSize() != null && flagSearchCriteria.getSize() >= 0) {
                query.limit(flagSearchCriteria.getSize());
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long countListFlag(PageableSearchCriteria flagSearchCriteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbFlag> query = new JPAQuery<EbFlag>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getFlagGlobalWhere(query, where, flagSearchCriteria);
            query.from(qEbFlag);
            query.where(where);
            query = getFlagGlobalJoin(query, where, flagSearchCriteria);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private JPAQuery getFlagGlobalWhere(JPAQuery query, BooleanBuilder where, PageableSearchCriteria flagSearchCriteria) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        if (connectedUser.getEbCompagnie() != null && connectedUser.getEbCompagnie().getEbCompagnieNum() != null) {
            where.and(qEbFlagCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }

        if (flagSearchCriteria.getSearchterm() != null) {
            String term = flagSearchCriteria.getSearchterm().toLowerCase();
            where.andAnyOf(qEbFlag.reference.toLowerCase().contains(term), qEbFlag.name.toLowerCase().contains(term));
        }

        return query;
    }

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    private JPAQuery getFlagGlobalJoin(JPAQuery query, BooleanBuilder where, PageableSearchCriteria flagSearchCriteria) {
        query.leftJoin(qEbFlag.compagnie(), qEbFlagCompagnie);
        return query;
    }
}
