package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.utils.search.SearchCriteriaPricingBooking;


public interface DaoPricingTracking {
    List<EbDemande> getListEbDemande(SearchCriteriaPricingBooking criteria) throws Exception;
}
