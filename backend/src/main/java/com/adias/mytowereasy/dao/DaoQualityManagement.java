/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbMarchandise;
import com.adias.mytowereasy.model.qm.EbQmIncident;
import com.adias.mytowereasy.model.qm.EbQmIncidentLine;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


public interface DaoQualityManagement {
    List<EbQmIncident> getListEbIncident(SearchCriteriaQM criterias);

    EbQmIncident getIncident(SearchCriteriaQM criteria);

    List<EbMarchandise> listMarchandise(SearchCriteriaQM criteria);

    Long countlistMarchandise(SearchCriteriaQM criteria);

    List<EbQmIncidentLine> listIncidentLineByIncidentNum(SearchCriteriaQM criteria);

    List<EbQmIncident> getIncidentRefByListIncident(SearchCriteriaQM criterias);

    Integer nextValQuality();

    Long getCountList(SearchCriteriaQM criterias) throws Exception;

    Integer nextValEbQMRootCause();
}
