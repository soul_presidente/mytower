/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl.custom;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.custom.DaoCustom;
import com.adias.mytowereasy.dao.impl.Dao;
import com.adias.mytowereasy.model.custom.EbFormField;
import com.adias.mytowereasy.model.custom.QEbFormField;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.adias.mytowereasy.service.ConnectedUserService;


@Component
@Transactional
public class DaoCustomImpl extends Dao implements DaoCustom {
    @PersistenceContext
    EntityManager em;

    @Autowired
    private EbUserRepository ebUserRepository;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbFormField qEbFormField = QEbFormField.ebFormField;

    @Override
    public List<EbFormField> getListEbFormField() {
        List<EbFormField> resultat = new ArrayList<EbFormField>();

        try {
            JPAQuery<EbFormField> query = new JPAQuery<EbFormField>(em);

            query
                .select(
                    QEbFormField
                        .create(
                            qEbFormField.ebFormFieldNum,
                            qEbFormField.name,
                            qEbFormField.code,
                            qEbFormField.visibilite,
                            qEbFormField.ordre,
                            qEbFormField.xEbFormType().ebFormTypeNum,
                            qEbFormField.xEbFormType().ebFormTypeName,
                            qEbFormField.motTechnique,
                            qEbFormField.translate,
                            qEbFormField.valueAvailable,
                            qEbFormField.xEbViewForm().ebFormViewNum,
                            qEbFormField.xEbViewForm().ebFormView,
                            qEbFormField.xEbViewForm().xEbFormZone().ebFormZoneNum,
                            qEbFormField.xEbViewForm().xEbFormZone().ebFormName,
                            qEbFormField.srcListItems,
                            qEbFormField.fetchAttribute,
                            qEbFormField.fetchValue));

            query.distinct();
            query.from(qEbFormField);

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }
}
