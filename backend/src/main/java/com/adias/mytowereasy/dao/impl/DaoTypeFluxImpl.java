package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoTypeFlux;
import com.adias.mytowereasy.model.EbTypeFlux;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.QEbTypeFlux;
import com.adias.mytowereasy.model.tt.EbTtSchemaPsl;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoTypeFluxImpl implements DaoTypeFlux {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbTypeFlux qEbTypeFlux = QEbTypeFlux.ebTypeFlux;
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbZoneCompagnie");

    @Override
    public List<EbTypeFlux> getListTypeFlux(SearchCriteria criteria) {
        List<EbTypeFlux> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbTypeFlux> query = new JPAQuery<>(em);

            query.select(qEbTypeFlux);

            query = getTypeFluxGlobalWhere(query, where, criteria);

            query.from(qEbTypeFlux);
            query.where(where);

            query = getTypeFluxGlobalJoin(query, where, criteria);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbTypeFlux, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbTypeFlux.designation.asc());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public Long getListTypeFluxCount(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbTtSchemaPsl> query = new JPAQuery<EbTtSchemaPsl>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getTypeFluxGlobalWhere(query, where, criteria);

            query.from(qEbTypeFlux);
            query.where(where);

            query = getTypeFluxGlobalJoin(query, where, criteria);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private JPAQuery getTypeFluxGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);

        if (!criteria.isIncludeGlobalValues() || connectedUser.isControlTower()) {
            where.and(qEbCompagnie.ebCompagnieNum.eq(criteria.getEbCompagnieNum()));
        }
        else {
            Integer ebCompagnieNum = criteria.getEbCompagnieNum() != null ?
                criteria.getEbCompagnieNum() :
                connectedUser.getEbCompagnie().getEbCompagnieNum();
            where.andAnyOf(qEbCompagnie.ebCompagnieNum.isNull(), qEbCompagnie.ebCompagnieNum.eq(ebCompagnieNum));
        }

        where.and(qEbTypeFlux.isDeleted.isNull().or(qEbTypeFlux.isDeleted.isFalse()));

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where.andAnyOf(qEbTypeFlux.designation.toLowerCase().contains(term));
        }

        return query;
    }

    private JPAQuery getTypeFluxGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        query.leftJoin(qEbTypeFlux.xEbCompagnie(), qEbCompagnie);
        return query;
    }
}
