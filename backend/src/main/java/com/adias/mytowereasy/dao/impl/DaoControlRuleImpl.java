package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberExpression;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoControlRule;
import com.adias.mytowereasy.enumeration.ControlRuleOperationTypesEnum;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.CREnumeration.*;
import com.adias.mytowereasy.model.tt.*;
import com.adias.mytowereasy.repository.EbCategorieRepository;
import com.adias.mytowereasy.repository.EbCustomFieldRepository;
import com.adias.mytowereasy.repository.EbTtSchemaPslRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.EbTypeFluxService;
import com.adias.mytowereasy.service.TypeRequestService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoControlRuleImpl extends Dao implements DaoControlRule {
    @PersistenceContext
    EntityManager em;

    @Autowired
    private ConnectedUserService connectedUserService;

    @Autowired
    private TypeRequestService typeRequestService;

    @Autowired
    private EbTypeFluxService typeFluxService;

    @Autowired
    private EbTtSchemaPslRepository schemaPslReository;

    @Autowired
    protected EbCategorieRepository ebCategorieRepository;

    @Autowired
    protected EbCustomFieldRepository ebCustomFieldRepository;

    QEbControlRule qEbControlRule = QEbControlRule.ebControlRule;

    QEbControlRuleOperation qEbControlRuleOperation = QEbControlRuleOperation.ebControlRuleOperation;

    QEbTypeRequest qEbTypeRequest = QEbTypeRequest.ebTypeRequest;
    QEbTypeFlux qEbTypeFlux = QEbTypeFlux.ebTypeFlux;
    QEbTtSchemaPsl qEbschamPsl = QEbTtSchemaPsl.ebTtSchemaPsl;

    private Map<String, OrderSpecifier<?>> orderMapASC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, OrderSpecifier<?>> orderMapDESC = new HashMap<String, OrderSpecifier<?>>();
    private Map<String, Path<?>> orderMap = new HashMap<String, Path<?>>();

    private QEbTTPslApp qPslApp = QEbTTPslApp.ebTTPslApp;

    private QEbTTPslApp qRightPslApp = new QEbTTPslApp("qRightPslApp");

    private QEbTtTracing qTracing = new QEbTtTracing("qTracing");

    private QEbDemande qEbDemande = new QEbDemande("qEbDemande");
    QEbParty qPartyDest = new QEbParty("partyDest");
    QEbParty qPartyOrigin = new QEbParty("partyOrigin");

    QEcCountry qEcCountryOrigin = new QEcCountry("qEcCountryOrigin");
    QEcCountry qEcCountryDest = new QEcCountry("qEcCountryDest");

    private QEbMarchandise qEbMarchandise = new QEbMarchandise("qEbMarchandise");

    private QEbCompagnie qCompagnie = new QEbCompagnie("qEbCompagnie");

    private QEbEtablissement qEtablissement = new QEbEtablissement("qEbEtablissement");

    private QEbUser qEbUser = new QEbUser("qEbUser");

    private QEbUser qEbUserCt = new QEbUser("qEbUserCt");

    private QEcCurrency qEcCurrency = new QEcCurrency("qEcCurrency");

    private QEbCostCenter qEbCostCenter = new QEbCostCenter("qEbCostCenter");

    private void Initialize() {
        orderMapASC.clear();

        orderMap.put("code", qEbControlRule.code);
        orderMapASC.put("code", qEbControlRule.code.asc());
        orderMapDESC.put("code", qEbControlRule.code.desc());

        orderMap.put("libelle", qEbControlRule.libelle);
        orderMapASC.put("libelle", qEbControlRule.libelle.asc());
        orderMapDESC.put("libelle", qEbControlRule.libelle.desc());

        orderMap.put("margin", qEbControlRule.margin);
        orderMapASC.put("margin", qEbControlRule.margin.asc());
        orderMapDESC.put("margin", qEbControlRule.margin.desc());

        orderMap.put("priority", qEbControlRule.priority);
        orderMapASC.put("priority", qEbControlRule.priority.asc());
        orderMapDESC.put("priority", qEbControlRule.priority.desc());

        orderMap.put("ruleType", qEbControlRule.ruleType);
        orderMapASC.put("ruleType", qEbControlRule.ruleType.asc());
        orderMapDESC.put("ruleType", qEbControlRule.ruleType.desc());

        orderMap.put("genererIncident", qEbControlRule.genererIncident);
        orderMapASC.put("genererIncident", qEbControlRule.genererIncident.asc());
        orderMapDESC.put("genererIncident", qEbControlRule.genererIncident.desc());

        orderMap.put("genererNotif", qEbControlRule.genererNotif);
        orderMapASC.put("genererNotif", qEbControlRule.genererNotif.asc());
        orderMapDESC.put("genererNotif", qEbControlRule.genererNotif.desc());

        orderMap.put("envoyerEmail", qEbControlRule.envoyerEmail);
        orderMapASC.put("envoyerEmail", qEbControlRule.envoyerEmail.asc());
        orderMapDESC.put("envoyerEmail", qEbControlRule.envoyerEmail.desc());

        orderMap.put("xEbTypeFlux", qEbControlRule.xEbTypeFlux().code);
        orderMapASC.put("xEbTypeFlux", qEbControlRule.xEbTypeFlux().code.asc());
        orderMapDESC.put("xEbTypeFlux", qEbControlRule.xEbTypeFlux().code.desc());

        orderMap.put("xEbTypeRequest", qEbControlRule.xEbTypeRequest().reference);
        orderMapASC.put("xEbTypeRequest", qEbControlRule.xEbTypeRequest().reference.asc());
        orderMapDESC.put("xEbTypeRequest", qEbControlRule.xEbTypeRequest().reference.desc());

        // xEbSchemaPsl

        orderMap.put("xEbSchemaPsl", qEbControlRule.xEbSchemaPsl().code);
        orderMapASC.put("xEbSchemaPsl", qEbControlRule.xEbSchemaPsl().code.asc());
        orderMapDESC.put("xEbSchemaPsl", qEbControlRule.xEbSchemaPsl().code.desc());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<EbControlRule> getListEbControlRule(SearchCriteria criteria) throws Exception {
        List<EbControlRule> result = null;
        BooleanBuilder where = new BooleanBuilder();

        Initialize();

        try {
            JPAQuery<EbControlRule> query = new JPAQuery<EbControlRule>(em);
            query
                .select(
                    QEbControlRule
                        .create(
                            qEbControlRule.ebControlRuleNum,
                            qEbControlRule.xEbCompagnie().ebCompagnieNum,
                            qEbschamPsl.code,
                            qEbTypeFlux.code,
                            qEbTypeRequest.reference,
                            qEbTypeRequest.libelle,
                            qEbControlRule.code,
                            qEbControlRule.libelle,
                            qEbControlRule.genererIncident,
                            qEbControlRule.genererNotif,
                            qEbControlRule.generateTr,
                            qEbControlRule.changeDocumentStatus,
                            qEbControlRule.envoyerEmail,
                            qEbControlRule.emailObjet,
                            qEbControlRule.emailMessage,
                            qEbControlRule.emailDestinataires,
                            qEbControlRule.margin,
                            qEbControlRule.priority,
                            qEbControlRule.ruleType,
                            qEbControlRule.ruleCategory,
                            qEbControlRule.listTypeDocStatus,
                            qEbControlRule.activated,
                            qEbControlRule.commentaire,
                            qEbControlRule.exportControl,
                            qEbControlRule.destStatus,
                            qEbControlRule.changeDemandesStatus));

            query.distinct();
            query = getGlobalWhere(query, where, criteria);
            query.from(qEbControlRule);
            query.leftJoin(qEbControlRule.xEbSchemaPsl(), qEbschamPsl);
            query.leftJoin(qEbControlRule.xEbTypeFlux(), qEbTypeFlux);
            query.leftJoin(qEbControlRule.xEbTypeRequest(), qEbTypeRequest);

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            if (criteria.getOrderedColumn() != null) {

                if (criteria.getAscendant()) {
                    query.orderBy(this.orderMapASC.get(criteria.getOrderedColumn()));
                }
                else {
                    query.orderBy(this.orderMapDESC.get(criteria.getOrderedColumn()));
                }

            }
            else {
                query.orderBy(this.orderMapDESC.get("code"));
            }

            query.where(where);
            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criterias) throws Exception {
        if (criterias.getEbCompagnieNum() != null) where
            .and(qEbControlRule.xEbCompagnie().ebCompagnieNum.eq(criterias.getEbCompagnieNum()));

        if (criterias.getRuleCategory() != null) where.and(qEbControlRule.ruleCategory.eq(criterias.getRuleCategory()));

        String searchTerm = criterias.getSearchterm();
        Boolean bool = null;

        if (searchTerm != null && !searchTerm.trim().isEmpty()) {
            searchTerm = searchTerm.toLowerCase().trim();
            SearchCriteria criteriaRequest = new SearchCriteria();
            criteriaRequest.setSearchterm(searchTerm);

            List<Integer> listTypeDemande = typeRequestService
                .searchTypeRequest(criteriaRequest).stream().map(EbTypeRequest::getEbTypeRequestNum)
                .collect(Collectors.toList());

            List<Integer> listTypeFluxNum = typeFluxService
                .getListTypeFluxByEbCompagnieNum(criterias.getEbCompagnieNum()).stream()
                .filter(
                    typeFlux -> typeFlux
                        .getCode().toLowerCase().trim().contains(criterias.getSearchterm().toLowerCase().trim()))
                .map(EbTypeFlux::getEbTypeFluxNum).collect(Collectors.toList());

            List<Integer> listSchemaPslNum = schemaPslReository
                .findByXEbCompagnie_ebCompagnieNum(criterias.getEbCompagnieNum()).stream()
                .filter(
                    schemaPsl -> schemaPsl
                        .getCode().toLowerCase().trim().contains(criterias.getSearchterm().toLowerCase().trim()))
                .map(EbTtSchemaPsl::getEbTtSchemaPslNum).collect(Collectors.toList());

            if (searchTerm.contains("yes") || searchTerm.contains("oui")) bool = true;
            else if (searchTerm.contains("no") || searchTerm.contains("non")) bool = false;
            BooleanBuilder bb = new BooleanBuilder();
            List<Integer> ruleType = RuleType.getRuleTypeEnumByKey(searchTerm);
            List<Integer> priority = Priority.getPriorityEnumByKey(searchTerm);
      // @formatter:off
 bb
				.andAnyOf(qEbControlRule.code.toLowerCase().trim().contains(searchTerm), qEbControlRule.ruleType.in(ruleType), 
					qEbControlRule.priority.in(priority), qEbControlRule.libelle.toLowerCase().trim().contains(searchTerm), 
					 qEbControlRule.xEbTypeRequest().ebTypeRequestNum.in(listTypeDemande) , 
					qEbControlRule.xEbTypeFlux().ebTypeFluxNum.in(listTypeFluxNum) , qEbControlRule.xEbSchemaPsl().ebTtSchemaPslNum.in(listSchemaPslNum));
// @formatter:on

            if (bool != null) {
                BooleanBuilder b = new BooleanBuilder();
                if (!bool) b
                    .andAnyOf(
                        qEbControlRule.genererIncident.isFalse(),
                        qEbControlRule.genererNotif.isFalse(),
                        qEbControlRule.envoyerEmail.isFalse());
                else b
                    .andAnyOf(
                        qEbControlRule.genererIncident.isTrue(),
                        qEbControlRule.genererNotif.isTrue(),
                        qEbControlRule.envoyerEmail.isTrue());

                bb.or(b);
            }

            where.and(bb);
        }

        return query;
    }

    @Override
    public Long listCount(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbControlRule> query = new JPAQuery<EbControlRule>(em);
            BooleanBuilder where = new BooleanBuilder();

            getGlobalWhere(query, where, criteria);
            query.from(qEbControlRule);
            query.distinct().where(where);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public List<EbTTPslApp> getListPslWithRule(EbControlRule ebControlRule, Integer ebTtTracingNum) {
        List<EbTTPslApp> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPQLQuery<EbTTPslApp> query = new JPAQuery<EbTTPslApp>(em);

            query
                .select(
                    QEbTTPslApp
                        .create(
                            qPslApp.ebTtPslAppNum,

                            qPslApp.codeAlpha,
                            qPslApp.libelle,
                            qPslApp.dateActuelle,
                            qPslApp.dateEstimee,
                            qPslApp.dateNegotiation,
                            qPslApp.dateChamps,
                            qPslApp.validated,

                            qEbDemande.ebDemandeNum,
                            qEbDemande.xEcStatut,
                            qTracing.ebTtTracingNum,
                            qTracing.refTransport,
                            qTracing.listControlRule,

                            qTracing.codeAlphaPslCourant,
                            qTracing.codeAlphaLastPsl,
                            qTracing.nomEtablissementChargeur,
                            qTracing.nomEtablissementTransporteur));

            where
                .and(
                    qTracing.xEbChargeurCompagnie().ebCompagnieNum
                        .eq(ebControlRule.getxEbCompagnie().getEbCompagnieNum()));
            where.and(qEbDemande.xEbTypeFluxNum.eq(ebControlRule.getxEbTypeFlux().getEbTypeFluxNum()));
            where
                .and(
                    Expressions
                        .booleanTemplate(
                            Dao.SCHEMA + "check_json_property({0}, {1}, {2}) = true",
                            qEbDemande.xEbSchemaPsl(),
                            "ebTtSchemaPslNum",
                            ebControlRule.getxEbSchemaPsl().getEbTtSchemaPslNum().toString()));

            if (ebTtTracingNum != null) where.and(qTracing.ebTtTracingNum.eq(ebTtTracingNum));

            query.distinct();
            query.from(qPslApp).where(where);
            query.leftJoin(qPslApp.xEbTrackTrace(), qTracing);
            query.leftJoin(qTracing.xEbDemande(), qEbDemande);
            query.orderBy(qTracing.ebTtTracingNum.desc());

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public List<EbDemande> getListDemandeWithRule(EbControlRule rule, Integer ebDemandeNum, EbUser connectedUser) {
        connectedUser = connectedUser == null ? connectedUserService.getCurrentUser() : connectedUser;
        List<EbDemande> resultat = new ArrayList<EbDemande>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbDemande> query = new JPAQuery<EbDemande>(em);

            query
                .select(
                    QEbDemande
                        .create(
                            qEbDemande.ebDemandeNum,
                            qEbDemande.customFields,
                            qEbDemande.listCategories,
                            qEbDemande.listTypeDocuments,
                            qEbDemande.xEbTypeFluxNum,
                            qEbDemande.xEbTypeFluxDesignation,
                            qEbDemande.typeRequestLibelle,
                            qEbDemande.xEbTypeRequest,
                            qEbDemande.xEcModeTransport,
                            qEbDemande.xEcStatut,
                            qEbDemande.flagRecommendation,
                            qEbDemande.xEcNature,
                            qEbDemande.dateOfGoodsAvailability,
                            qEbDemande.xEbSchemaPsl(),
                            qEbDemande.listCategoriesStr,
                            qEbDemande.scenario,
                            qPartyOrigin.ebPartyNum,
                            qPartyDest.ebPartyNum,
                            qCompagnie.ebCompagnieNum,
                            qEtablissement.ebEtablissementNum,
                            qEcCurrency.ecCurrencyNum,
                            qEbUser.ebUserNum,
                            qEbUserCt.ebUserNum,
                            qEbDemande.refTransport))
                .from(qEbMarchandise, qEbDemande);

            query.join(qEbDemande.ebPartyOrigin(), qPartyOrigin);
            query.join(qPartyOrigin.xEcCountry(), qEcCountryOrigin);
            query.join(qEbDemande.ebPartyDest(), qPartyDest);
            query.join(qPartyDest.xEcCountry(), qEcCountryDest);
            query.join(qEbDemande.xEbCompagnie(), qCompagnie);
            query.join(qEbDemande.xEbEtablissement(), qEtablissement);
            query.join(qEbDemande.xecCurrencyInvoice(), qEcCurrency);
            query.join(qEbDemande.user(), qEbUser);
            query.leftJoin(qEbDemande.xEbCostCenter(), qEbCostCenter);
            query.leftJoin(qEbDemande.xEbUserCt(), qEbUserCt);

            where.and(qEbMarchandise.ebDemande().ebDemandeNum.eq(qEbDemande.ebDemandeNum));
            where.and(qCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));

            if (ebDemandeNum != null) where.and(qEbDemande.ebDemandeNum.eq(ebDemandeNum));

            BooleanBuilder bb = applyRuleOperations(rule, connectedUser);
            if (bb.hasValue()) where.and(bb);

            query.distinct().where(where);

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    private BooleanBuilder applyRuleOperations(EbControlRule rule, EbUser connectedUser) throws Exception {
        List<EbControlRuleOperation> listCondition = null, listOpr = rule.getListRuleOperations();
        BooleanBuilder bb = new BooleanBuilder();

        if (listOpr != null && !listOpr.isEmpty()) {
            listCondition = listOpr
                .stream()
                .filter(
                    it -> (it.getIsOperation() == null || !it.getIsOperation()) && it.getOperationType() != null
                        && (it.getOperationType() == ControlRuleOperationTypesEnum.IS_FILE_CONDITION.getCode()))
                .collect(Collectors.toList());
        }

        if (listCondition != null && !listCondition.isEmpty()) {
            boolean isFirst = true;
            connectedUser = connectedUser == null ? connectedUserService.getCurrentUser() : connectedUser;
            Integer ebCompagnieNum = connectedUser.getEbCompagnie().getEbCompagnieNum();

            for (EbControlRuleOperation cond: listCondition) {
                String leftOpStr = cond.getLeftOperands();
                String rightOpStr = cond.getRightOperands();
                Integer opr = cond.getOperator();
                BooleanBuilder curCond = new BooleanBuilder();

                if (leftOpStr == null) throw new MyTowerException("Left operands not provided");

                RuleOperandType leftOp = RuleOperandType.getRuleOperandTypeEnumByKey(Integer.valueOf(leftOpStr));

                if (rightOpStr == null && !leftOp.getCode().equals(RuleOperandType.MANDATORY_FIELDS.getCode())
                    && !leftOp.getCode().equals(RuleOperandType.TYPE_OF_DOC.getCode())) throw new MyTowerException(
                        "Right operands not provided");

                if (opr == null) throw new MyTowerException("Operator not provided");

                StringExpression qrLeftOpStr = null;
                BooleanExpression qrLeftOpBool = null;
                NumberExpression<Integer> qrLeftOpNumber = null;
                List<Object> allTrFieldsExpr = null;

                if (leftOp.getCode().equals(RuleOperandType.UNITS.getCode())) {
                    Integer leftUnitFieldCode = cond.getLeftUnitFieldCode();
                    CrUnitFields unitField = CrUnitFields.getUnitInfoByCode(leftUnitFieldCode);

                    Object m = QEbMarchandise.class.getField(unitField.getFieldName()).get(qEbMarchandise); // get
                                                                                                            // field
                                                                                                            // from
                                                                                                            // QEbMarchandise

                    if (this.isTypeNumber(unitField.getType())) {
                        qrLeftOpNumber = (NumberExpression<Integer>) m;
                    }
                    else if (this.isStringType(unitField.getType())) {
                        qrLeftOpStr = (StringExpression) m;
                    }
                    else if (this.isBooleanType(unitField.getType())) {
                        qrLeftOpBool = (BooleanExpression) m;
                    }

                }
                else if (leftOp.getCode().equals(RuleOperandType.STATUT_DOSSIER.getCode())) {
                    qrLeftOpNumber = qEbDemande.xEcStatut;
                }
                else if (leftOp.getCode().equals(RuleOperandType.MODE_DE_TRANSPORT.getCode())) {
                    qrLeftOpNumber = qEbDemande.xEcModeTransport;
                }
                else if (leftOp.getCode().equals(RuleOperandType.FLOW_TYPE.getCode())) {
                    qrLeftOpNumber = qEbDemande.xEbTypeFluxNum;
                }
                else if (leftOp.getCode().equals(RuleOperandType.TYPE_OF_REQUEST.getCode())) {
                    qrLeftOpNumber = qEbDemande.xEbTypeRequest;
                }
                else if (leftOp.getCode().equals(RuleOperandType.PAYS_ORIGIN.getCode())) {
                    qrLeftOpStr = qEcCountryOrigin.libelle;
                }
                else if (leftOp.getCode().equals(RuleOperandType.DESTINATION_COUNTRY.getCode())) {
                    qrLeftOpStr = qEcCountryDest.libelle;
                }
                else if (leftOp.getCode().equals(RuleOperandType.REF_ADR_ORG.getCode())) {
                    qrLeftOpStr = qPartyOrigin.reference;
                }
                else if (leftOp.getCode().equals(RuleOperandType.REF_ADR_ORG.getCode())) {
                    qrLeftOpStr = qPartyDest.reference;
                }
                else if (leftOp.getCode().equals(RuleOperandType.PSL_SCHEMA.getCode())) {
                    qrLeftOpStr = this.getPropertyInJsonField(qEbDemande.xEbSchemaPsl(), "ebTtSchemaPslNum");
                }
                else if (leftOp.getCode().equals(RuleOperandType.MANDATORY_FIELDS.getCode())) {
                    allTrFieldsExpr = this.getAllTrFieldsExpressions();
                }
                else if (leftOp.getCode().equals(RuleOperandType.TYPE_OF_DOC.getCode())) {
                    if (cond
                        .getSelectedTypeDocNum() == null) throw new MyTowerException("No Type doc was selected !!!!");

                    String selectedTypeDocNum = String.valueOf(cond.getSelectedTypeDocNum());
                    allTrFieldsExpr = new ArrayList<>();
                    allTrFieldsExpr.add(this.getNbrDocExpressionInTypeDoc(selectedTypeDocNum));
                }
                else if (leftOp.getCode().equals(RuleOperandType.COST_CENTER.getCode())) {
                    qrLeftOpNumber = qEbCostCenter.ebCostCenterNum;
                }
                else // custom fields and categories
                {

                    if (leftOp.getType().equals(CrFieldTypes.CUSTOMFIELD.getCode())) {
                        Integer ebLeftCustomField = cond.getEbLeftCustomFieldNum();
                        if (ebLeftCustomField == null) throw new MyTowerException(
                            "No Left CustomField was defined for action!!!");

                        EbCustomField ebCustomField = this.ebCustomFieldRepository
                            .getCustomFieldByXEbCompagnie(ebCompagnieNum);

                        if (ebCustomField.getListCustomsFields() != null
                            && !ebCustomField.getListCustomsFields().isEmpty()) {
                            CustomFields cf = ebCustomField
                                .getListCustomsFields().stream().filter(f -> f.getNum().equals(ebLeftCustomField))
                                .findFirst().orElse(null);

                            if (cf != null) {
                                qrLeftOpStr = this.getExpressionForCustomField(cf.getLabel());
                            } // else CF not found in compagnie's CFs

                        } // else no CFs was found for compagnie

                    }
                    else if (leftOp.getType().equals(CrFieldTypes.CATEGORIE.getCode())) {
                        int SELECTED_LABEL_INDEX = 0;
                        Integer ebCategorieNum = cond.getEbCategorieNum();
                        if (ebCategorieNum == null) throw new MyTowerException(
                            "No Categorie was defined for condition !!!");
                        EbCategorie categorie = this.ebCategorieRepository.findById(ebCategorieNum).orElse(null);
                        if (categorie == null) throw new MyTowerException("Categorie not found !!!");
                        qrLeftOpNumber = this
                            .getLabelExpressionForCategorie(categorie.getLibelle(), SELECTED_LABEL_INDEX); // INDEX 0
                                                                                                           // (selected
                                                                                                           // labedl)
                    }

                }

                if (qrLeftOpNumber != null) {

                    if (Operator.IS_IN.getCode().equals(opr)) {
                        curCond.and(qrLeftOpNumber.eq(Integer.valueOf(rightOpStr)));
                    }
                    else if (Operator.IS_NOT_IN.getCode().equals(opr)) {
                        curCond.and(qrLeftOpNumber.ne(Integer.valueOf(rightOpStr)));
                    }

                }
                else if (qrLeftOpStr != null) {

                    if (Operator.BEGINS_WITH.getCode().equals(opr)) {
                        curCond.and(qrLeftOpStr.startsWithIgnoreCase(rightOpStr));
                    }
                    else if (Operator.IS_IN.getCode().equals(opr)) {
                        curCond.and(qrLeftOpStr.in(rightOpStr));
                    }
                    else if (Operator.IS_NOT_IN.getCode().equals(opr)) {
                        curCond.and(qrLeftOpStr.notIn(rightOpStr));
                    }

                }
                else if (qrLeftOpBool != null) {

                    if (Operator.IS_IN.getCode().equals(opr)) {
                        curCond.and(qrLeftOpBool.eq(Boolean.valueOf(rightOpStr)));
                    }
                    else if (Operator.IS_NOT_IN.getCode().equals(opr)) {
                        curCond.and(qrLeftOpBool.ne(Boolean.valueOf(rightOpStr)));
                    }

                }
                else if (allTrFieldsExpr != null && !allTrFieldsExpr.isEmpty()) {
                    BooleanBuilder allFieldCond = new BooleanBuilder();

                    if (Operator.AVAILABLE.getCode().equals(opr)) {

                        for (Object exp: allTrFieldsExpr) {

                            if (exp instanceof NumberExpression) {
                                qrLeftOpNumber = (NumberExpression<Integer>) exp;
                                allFieldCond.and(qrLeftOpNumber.isNotNull());
                            }
                            else if (exp instanceof StringExpression) {
                                qrLeftOpStr = (StringExpression) exp;
                                allFieldCond.and(qrLeftOpStr.isNotNull());
                            }

                        }

                        curCond.and(allFieldCond);
                    }
                    else if (Operator.NOT_AVAILABLE.getCode().equals(opr)) {

                        for (Object exp: allTrFieldsExpr) {

                            if (exp instanceof NumberExpression) {
                                qrLeftOpNumber = (NumberExpression<Integer>) exp;
                                allFieldCond.or(qrLeftOpNumber.isNull());
                            }
                            else if (exp instanceof StringExpression) {
                                qrLeftOpStr = (StringExpression) exp;
                                allFieldCond.or(qrLeftOpStr.isNull());
                            }

                        }

                        curCond.andAnyOf(allFieldCond);
                    }

                }

                if (curCond.hasValue()) {
                    if (isFirst) bb.and(curCond);
                    else if (cond
                        .getRuleOperator() == null) throw new MyTowerException("Rule operator is not provided");
                    else {
                        if (RuleOperator.AND.getCode().equals(cond.getRuleOperator())) bb.andAnyOf(curCond);
                        else bb.orAllOf(curCond);
                    }
                }

                isFirst = false;
            }

        }

        return bb;
    }

    private List<Object> getAllTrFieldsExpressions() {
        List<Object> exps = new ArrayList<Object>();

        List<RuleOperandType> requiredTrFields = RuleOperandType.getAllRequiredFields();

        requiredTrFields.forEach(field -> {
            Object fieldExp = null;

            try {

                if (RuleOperandType.isPartyField(field.getCode())) {
                    fieldExp = this.getEbPartyFieldExpression(field);
                }
                else if (RuleOperandType.isTrField(field.getCode())) {
                    fieldExp = this.getEbDemandeFieldExpression(field);
                }

                if (fieldExp != null) exps.add(fieldExp);
            } catch (Exception e) {
                e.printStackTrace();
            }

        });

        return exps;
    }

    private Object getEbPartyFieldExpression(RuleOperandType field) throws Exception {
        Object fieldExp = null;

        if (field.getCode().equals(RuleOperandType.PAYS_ORIGIN.getCode())) {
            fieldExp = QEbParty.class.getField("ebPartyNum").get(qPartyOrigin);
            return (NumberExpression<Integer>) fieldExp;
        }
        else if (field.getCode().equals(RuleOperandType.DESTINATION_COUNTRY.getCode())) {
            fieldExp = QEbParty.class.getField("ebPartyNum").get(qPartyDest);
            return (NumberExpression<Integer>) fieldExp;
        }
        else if (field.getCode().equals(RuleOperandType.REF_ADR_ORG.getCode())) {
            fieldExp = QEbParty.class.getField(field.getFieldName()).get(qPartyOrigin);
            return (StringExpression) fieldExp;
        }
        else if (field.getCode().equals(RuleOperandType.REF_ADR_DEST.getCode())) {
            fieldExp = QEbParty.class.getField(field.getFieldName()).get(qPartyDest);
            return (StringExpression) fieldExp;
        }

        return null;
    }

    private Object getEbDemandeFieldExpression(RuleOperandType field) throws Exception {
        Object fieldExp = null;

        if (field.getCode().equals(RuleOperandType.PSL_SCHEMA.getCode())) {
            fieldExp = this.getPropertyInJsonField(qEbDemande.xEbSchemaPsl(), "ebTtSchemaPslNum");
            return (StringExpression) fieldExp;
        }
        else if (field.getCode().equals(RuleOperandType.INVOICE_CURRENCY.getCode())) {
            return (NumberExpression<Integer>) qEcCurrency.ecCurrencyNum;
        }
        else {
            fieldExp = QEbDemande.class.getField(field.getFieldName()).get(qEbDemande);

            if (this.isTypeNumber(field.getType()) && !field.getCode().equals(RuleOperandType.LEG.getCode())) {
                return (NumberExpression<Integer>) fieldExp;
            }
            else if (this.isStringType(field.getType())) {
                return (StringExpression) fieldExp;
            }

        }

        return null;
    }

    private boolean isTypeNumber(Integer type) {
  // @formatter:off
		return 
			type.equals(CrFieldTypes.EXPORT_CONTROL.getCode()) ||  
			type.equals(CrFieldTypes.NUMBER.getCode()) ||  
			type.equals(CrFieldTypes.COUNTRY.getCode()) ||  
			type.equals(CrFieldTypes.DGR.getCode()) ||  
			type.equals(CrFieldTypes.TYPEDEFLUX.getCode()) ||  
			type.equals(CrFieldTypes.MODE_TR.getCode()) ||  
			type.equals(CrFieldTypes.INCOTERM.getCode()) ||  
			type.equals(CrFieldTypes.STATUS.getCode()) ||  
			type.equals(CrFieldTypes.TYPE_OF_REQ.getCode()) ||  
			type.equals(CrFieldTypes.UNIT_TYPE.getCode());
	// @formatter:on
    }

    private boolean isStringType(Integer type) {
  // @formatter:off
		return 
			type.equals(CrFieldTypes.STRING.getCode()) ||
			type.equals(CrFieldTypes.GOOD_TYPE.getCode()) || 
			type.equals(CrFieldTypes.COMPANY_CODE.getCode()) || 
			type.equals(CrFieldTypes.REF_ADDRESS.getCode()) || 
			type.equals(CrFieldTypes.CATEGORIE.getCode()) || 
			type.equals(CrFieldTypes.CUSTOMFIELD.getCode()) || 
			type.equals(CrFieldTypes.CLASS.getCode());
	// @formatter:on
    }

    private boolean isBooleanType(Integer type) {
        return type.equals(CrFieldTypes.OVERSIZED_ITEM.getCode()) || type.equals(CrFieldTypes.LARGE_ITEM.getCode());
    }

    private StringExpression getExpressionForCustomField(String customFieldLabel) {
        return Expressions
            .stringTemplate(
                Dao.SCHEMA + "get_value_by_keyvalue({0}, {1}, {2}, {3})",
                qEbDemande.customFields,
                "label",
                customFieldLabel,
                "value");
    }

    private <T> StringExpression getPropertyInJsonField(T field, String propertyName) { // T should be
                                                                                        // your jsonb
                                                                                        // object type
        return Expressions
            .stringTemplate(Dao.SCHEMA + "get_property_value_from_jsonb_field({0}, {1})", field, propertyName);
    }

    private NumberExpression<Integer> getLabelExpressionForCategorie(String ebCategorieLibelle, Integer labelIndice) {
        return Expressions
            .numberTemplate(
                Integer.class,
                Dao.SCHEMA + "get_selected_label_for_categorie({0}, {1}, {2}, {3}, {4}, {5})",
                qEbDemande.listCategories,
                "libelle",
                ebCategorieLibelle,
                "labels",
                labelIndice,
                "ebLabelNum");
    }

    private StringExpression getNbrDocExpressionInTypeDoc(String selectedTypeDocNum) {
        return Expressions
            .stringTemplate(
                Dao.SCHEMA + "get_value_by_keyvalue_from_json_field({0}, {1}, {2}, {3})",
                qEbDemande.listTypeDocuments,
                "ebTypeDocumentsNum",
                selectedTypeDocNum,
                "nbrDoc");
    }
}
