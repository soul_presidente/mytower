package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbCostCenter;


public interface DaoCostCenter {
    List<EbCostCenter> findActiveCostCenters(Integer ebCompagnieNum);
}
