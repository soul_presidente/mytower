package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbQmDeviationSettings;
import com.adias.mytowereasy.utils.search.SearchCriteriaQM;


public interface DaoQmDeviationSettings {
    List<EbQmDeviationSettings> listDeviation(SearchCriteriaQM criteria);

    Long getListEbQmDeviationSettingsCount(SearchCriteriaQM criteria);
}
