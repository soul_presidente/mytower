package com.adias.mytowereasy.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.chanel.model.EbWeekTemplate;
import com.adias.mytowereasy.chanel.model.QEbWeekTemplate;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.Statiques;
import com.adias.mytowereasy.repository.EbWeekTemplateRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoWeekTemplateImpl implements DaoWeekTemplate {
    @PersistenceContext
    EntityManager em;
    @Autowired
    ConnectedUserService connectedUserService;

    QEbWeekTemplate qEbWeekTemplate = QEbWeekTemplate.ebWeekTemplate;
    QEbCompagnie qEbCompagnie = QEbCompagnie.ebCompagnie;
    @Autowired
    EbWeekTemplateRepository ebWeekTemplateRepository;

    @Override
    public List<EbWeekTemplate> list(SearchCriteria criteria) {
        List<EbWeekTemplate> resultat = new ArrayList<>();
        EbUser connectedUser = connectedUserService.getCurrentUser();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbWeekTemplate> query = new JPAQuery<>(em);

            query
                .select(
                    QEbWeekTemplate
                        .create(
                            qEbWeekTemplate.ebWeektemplateNum,
                            qEbWeekTemplate.label,
                            qEbWeekTemplate.type,
                            qEbWeekTemplate.dateDebut,
                            qEbWeekTemplate.dateFin,
                            qEbWeekTemplate.isDefault)

                );

            query.from(qEbWeekTemplate);

            defineQueryGlobals(query, where, criteria, true);
            query.where(where);

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public Long listCount(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbWeekTemplate> query = new JPAQuery<EbWeekTemplate>(em);
            BooleanBuilder where = new BooleanBuilder();
            query.distinct().from(qEbWeekTemplate);
            defineQueryGlobals(query, where, criteria, false);
            query.where(where);
            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    private void
        defineQueryGlobals(JPAQuery query, BooleanBuilder where, SearchCriteria criteria, boolean enablePagination) {
        // Join
        query.leftJoin(qEbWeekTemplate.xEbCompagnie(), qEbCompagnie);

        EbUser connectedUser = connectedUserService.getCurrentUser();

        // Global Where
        where.andAnyOf(qEbWeekTemplate.deleted.isNull(), qEbWeekTemplate.deleted.isFalse());

        if (criteria.getCompanyNums() == null || criteria.getCompanyNums().size() == 0) {
            where.and(qEbCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }
        else {
            where.and(qEbCompagnie.ebCompagnieNum.in(criteria.getCompanyNums()));
        }

        if (StringUtils.isNotBlank(criteria.getSearchterm())) {
            String searchTerm = criteria.getSearchterm().toLowerCase();
            where.andAnyOf(qEbWeekTemplate.label.lower().contains(searchTerm));
        }

        // Order
        if (criteria.getOrderedColumn() != null) {
            Path<Object> fieldPath = Expressions.path(Object.class, qEbWeekTemplate, criteria.getOrderedColumn());
            query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
        }
        else {
            query.orderBy(qEbWeekTemplate.label.asc());
        }

        // Pagination
        if (enablePagination) {

            if (criteria.getPageNumber() != null && criteria.getSize() != null) {
                query
                    .limit(criteria.getSize())
                    .offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

        }

    }

    @Override
    public List<EbWeekTemplate> getListEbWeekTemplateWithChauvauchementDate(EbWeekTemplate ebWeekTemplate) {
        List<EbWeekTemplate> resultat = new ArrayList<EbWeekTemplate>();
        EbUser connectedUser = connectedUserService.getCurrentUser();

        try {

            if (ebWeekTemplate.getDateDebut() != null && ebWeekTemplate.getDateFin() != null) {
                SimpleDateFormat datew = new SimpleDateFormat(Statiques.DATE_FORMAT);
                String strDateDebut = datew.format(ebWeekTemplate.getDateDebut());
                String strDateFin = datew.format(ebWeekTemplate.getDateFin());
            }

            List<EbWeekTemplate> oldWeek = null;
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbWeekTemplate> query = new JPAQuery<EbWeekTemplate>(em);

            query
                .select(qEbWeekTemplate).from(qEbWeekTemplate).where(
                    (qEbWeekTemplate.dateDebut.eq(ebWeekTemplate.getDateDebut()))
                        .and(qEbWeekTemplate.dateFin.eq(ebWeekTemplate.getDateFin()))

                );

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }
}
