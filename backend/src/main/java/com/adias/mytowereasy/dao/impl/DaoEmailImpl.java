/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import com.adias.mytowereasy.model.EbParamsMail;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.Role;
import com.adias.mytowereasy.model.Enumeration.ServiceType;


public class DaoEmailImpl {
    public static List<EbParamsMail> listEmail;

    static {
        EbParamsMail param = null;
        listEmail = new ArrayList<EbParamsMail>();

        param = new EbParamsMail();
        param.setEmailName("Quotation request");
        param.setEmailId(12);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param.setRoles(Role.ROLE_PRESTATAIRE.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Quotation request");
        param.setEmailId(13);
        param.setServices(null);
        param.setRoles(Role.ROLE_CHARGEUR.getCode() + "");
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Quotation");
        param.setEmailId(14);
        param.setServices(null);
        param.setRoles(Role.ROLE_CHARGEUR.getCode() + "");
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Response from carrier");
        param.setEmailId(15);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param.setRoles(Role.ROLE_PRESTATAIRE.getCode() + "");
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Quotation request and recommendation request");
        param.setEmailId(16);
        param.setServices(null);
        param.setRoles(Role.ROLE_CONTROL_TOWER.getCode() + "");
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Quotation");
        param.setEmailId(17);
        param.setServices(null);
        param.setRoles(Role.ROLE_CONTROL_TOWER.getCode() + "");
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Final choice");
        param.setEmailId(20);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_PRESTATAIRE.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_CHARGEUR.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Quotation not selected");
        param.setEmailId(21);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param.setRoles(Role.ROLE_PRESTATAIRE.getCode() + "");
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("New comment on folder");
        param.setEmailId(22);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER.getCode() + ","
                    + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Information carrier");
        param.setEmailId(24);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER.getCode() + ","
                    + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Upload document");
        param.setEmailId(25);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER.getCode() + ","
                    + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Deleting document");
        param.setEmailId(26);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER.getCode() + ","
                    + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Update PSL");
        param.setEmailId(30);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Declaration of a deviation");
        param.setEmailId(33);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Alerte de quotation CT");
        param.setEmailId(34);
        param.setServices(null);
        param.setRoles("" + Role.ROLE_CONTROL_TOWER.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Transport File creation");
        param.setEmailId(37);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("PRICING/TM file updated");
        param.setEmailId(38);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("PRICING/TM cancelled");
        param.setEmailId(39);
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER.getCode() + ","
                    + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Invoice information apply");
        param.setEmailId(Enumeration.EmailConfig.INFORMATION_APPLY.getCode()); // 41
        param.setServices(null);
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);
        // 64
        param = new EbParamsMail();
        param.setEmailName("information mandatory prefacture");
        param.setEmailId(Enumeration.EmailConfig.INFO_MANDATORY_PREFACTURE.getCode());
        param.setServices(null);
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);
        // 65
        param = new EbParamsMail();
        param.setEmailName("information mandatory PROFORMA");
        param.setEmailId(Enumeration.EmailConfig.INFO_MANDATORY_PROFORMA.getCode());
        param.setServices(null);
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Information sharing");
        param.setEmailId(Enumeration.EmailConfig.INFORMATION_SHARING.getCode()); // 41
        param.setServices(null);
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        // alert de confirmation de prise en charge d'une TR par la TDC
        param = new EbParamsMail();
        param.setEmailName("PRICING/TM Acknowledged by CT");
        param.setEmailId(Enumeration.EmailConfig.QUOTATION_CKNOWLEDGE_TDC.getCode());
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER.getCode() + ","
                    + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        // alert de confirmation de prise en charge
        param = new EbParamsMail();
        param.setEmailName("PRICING/ Confirmation de prise en charge ");
        param.setEmailId(Enumeration.EmailConfig.PRICING_CONFIRMATION_PRISE_EN_CHARGE.getCode());
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER.getCode() + ","
                    + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        // CPTM Relance automatique
        param = new EbParamsMail();
        param.setEmailName("CPTM Reminder");
        param.setEmailId(Enumeration.EmailConfig.CPTM_REMINDER.getCode());
        param
            .setServices(
                ServiceType.SERVICE_BROKER.getCode() + "," + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());
        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());
        param.setHidden(false);
        listEmail.add(param);

        /*
         * TODO: A gérer plus tard
         */
        param = new EbParamsMail();
        param.setEmailName("Activate email account");
        param.setEmailId(1);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Nouveau compte société");
        param.setEmailId(2);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Invite friend");
        param.setEmailId(3);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Invite collegue");
        param.setEmailId(4);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Backup");
        param.setEmailId(5);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Reset password");
        param.setEmailId(6);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Update password");
        param.setEmailId(7);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Invite");
        param.setEmailId(8);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Accept invitation");
        param.setEmailId(9);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Compte activé par l'admin");
        param.setEmailId(10);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Utilisateur ajouté par l'admin");
        param.setEmailId(11);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Utilisateur refusé par l'admin");
        param.setEmailId(100);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Pricing data changed");
        param.setEmailId(41);
        param.setServices(null);
        param.setRoles(null);
        param.setHidden(true);
        listEmail.add(param);

        param = new EbParamsMail();
        param.setEmailName("Demande confirmation de prise en charge du transport");
        param.setEmailId(Enumeration.EmailConfig.DEMANDE_CONFIRMATION_DE_PRISE_EN_CHARGE_DU_TRANSPORT.getCode());
        param
            .setServices(
                ServiceType.SERVICE_TRANSPORTEUR.getCode() + "," + ServiceType.SERVICE_BROKER.getCode() + ","
                    + ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode());

        param
            .setRoles(
                Role.ROLE_CHARGEUR.getCode() + "," + Role.ROLE_CONTROL_TOWER.getCode() + ","
                    + Role.ROLE_PRESTATAIRE.getCode());

        param.setHidden(false);
        listEmail.add(param);
    }

    static public String getEmailDestinataireRoleByMailId(Integer mailId) {
        StringJoiner rolesJoiner = new StringJoiner(",");
        listEmail.forEach(ebEmailParamMails -> {

            if (mailId.equals(ebEmailParamMails.getEmailId())) {
                rolesJoiner.add(ebEmailParamMails.getRoles());
            }

        });
        return rolesJoiner.toString();
    }
}
