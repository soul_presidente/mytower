/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoEtablissement;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.EtatRelation;
import com.adias.mytowereasy.model.Enumeration.ServiceType;
import com.adias.mytowereasy.repository.EbEtablissementRepository;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoEtablissementImpl extends DaoCurrencyImpl implements DaoEtablissement {
    @Autowired
    private EbEtablissementRepository ebEtablissementRepository;

    @PersistenceContext
    EntityManager em;

    @Autowired
    private ConnectedUserService connectedUserService;

    QEbEtablissement qEbEtablissement = QEbEtablissement.ebEtablissement;
    QEbRelation qEbRelation = QEbRelation.ebRelation;
    QEbUser qEbUser = QEbUser.ebUser;
    QEbUser qEbUserHost = new QEbUser("userHost");
    QEbUser qEbUserGuest = new QEbUser("userGuest");
    QEbEtablissement qEbEtabHost = new QEbEtablissement("etabHost");
    QEbEtablissement qEbEtabGuest = new QEbEtablissement("etabGuest");
    QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbCompagnie");
    QEbCompagnieCurrency qEbCompagnieCurrency = QEbCompagnieCurrency.ebCompagnieCurrency;

    private SearchCriteria criteria;

    @Override
    public EbEtablissement selectEbEtablissement(SearchCriteria criterias) // method
                                                                           // not
                                                                           // used
    {
        if (criterias == null) criterias = new SearchCriteria();

        Optional<EbEtablissement> ebEtablissement = ebEtablissementRepository
            .findById(criterias.getEbEtablissementNum());

        if (ebEtablissement.isPresent()) {

            if (criterias.isWithListAdresse()) {
            }

        }

        return ebEtablissement.get();
    }

    @Override
    public EbEtablissement selectEbEtablissementByEbUser(Integer ebUserNum) {
        JPAQuery<EbEtablissement> query = new JPAQuery<EbEtablissement>(em);
        BooleanBuilder b = new BooleanBuilder();
        List<EbEtablissement> listEbEtablissement = new ArrayList<EbEtablissement>();

        QEbEtablissement etab = new QEbEtablissement("etab");
        QEbUser user = new QEbUser("user");

        query
            .select(user.ebEtablissement()).from(user).innerJoin(user.ebEtablissement(), etab)
            .where(b.and(user.ebUserNum.eq(ebUserNum)));

        listEbEtablissement = query.fetch();
        if (listEbEtablissement != null && listEbEtablissement.size() > 0) return listEbEtablissement.get(0);

        return new EbEtablissement();
    }

    @Override
    public BigInteger selectCountListEbEtablissementOfEbRelation(SearchCriteria criteria) {
        Query query = null;
        BigInteger cnt = null;

        try {
            String sqlQuery = "";

            sqlQuery = " SELECT DISTINCT ON (etab.eb_etablissement_num) * ";

            sqlQuery += this.getGlobalJoinListEtablissement();

            sqlQuery += this.getGlobalWhereListEtablissement(criteria);

            sqlQuery = " WITH globalQuery AS ( " + sqlQuery + " ) SELECT COUNT(*) FROM globalQuery ";

            query = this.getMapping(criteria, sqlQuery, em.createNativeQuery(sqlQuery));

            cnt = (BigInteger) query.getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cnt;
    }

    private String getGlobalWhereListEtablissement(SearchCriteria criteria) {
        String sqlQuery = " WHERE ";
        String subWhereQuery = "";
        String term = criteria.getSearchterm();

        sqlQuery += " 	ebuser.service IS NOT NULL ";
        sqlQuery += " 	AND (rel.etat IS NULL OR rel.etat <> :annule ) ";

        if (criteria.isContact()) {
            subWhereQuery += " rel.etat = :actif ";
        }

        if (criteria.isBlackList()) {
            if (!subWhereQuery.isEmpty()) subWhereQuery += " OR ";
            subWhereQuery += " ( ";
            subWhereQuery += " 	rel.etat = :blacklist ";
            subWhereQuery += " 	AND rel.x_last_modified_by = :iduser ";
            subWhereQuery += " ) ";
        }

        if (criteria.isSentRequests()) {
            if (!subWhereQuery.isEmpty()) subWhereQuery += " OR ";
            subWhereQuery += " ( ";
            subWhereQuery += " 	rel.etat = :encours ";
            subWhereQuery += " 	AND rel.x_host = :iduser ";
            subWhereQuery += " ) ";
        }

        if (!subWhereQuery.isEmpty()) sqlQuery += " AND ( " + subWhereQuery + " ) ";
        subWhereQuery = "";

        if (criteria.isEtablissementRequest()) {
            sqlQuery += " AND ( ";
            sqlQuery += " 	rel.flag_etablissement IS NOT NULL ";
            sqlQuery += " 	AND rel.x_eb_etablissement = :etabuser ";
            sqlQuery += " 	AND rel.x_guest IS NULL ";
            sqlQuery += " ) ";
        }

        // recherche de terme
        if (term != null && !term.isEmpty()) {
            sqlQuery += " AND ( ";
            sqlQuery += " 	LOWER(etab.nom) LIKE :term ";
            sqlQuery += " 	OR LOWER(etab.email) LIKE :term ";
            sqlQuery += " 	OR LOWER(etab.telephone) LIKE :term ";
            sqlQuery += " 	OR LOWER(etab.siret) LIKE :term ";
            sqlQuery += " ) ";
        }

        return sqlQuery;
    }

    private String getGlobalJoinListEtablissement() {
        String sqlQuery = "";

        sqlQuery += " FROM ";

        sqlQuery += " 	WORK.eb_relation rel ";
        sqlQuery += " 	LEFT JOIN WORK .eb_user ebuser ON ( ";
        sqlQuery += " 		ebuser.eb_user_num = rel.x_host ";
        sqlQuery += " 		OR ebuser.eb_user_num = rel.x_guest ";
        sqlQuery += " 	) ";
        sqlQuery += " 	RIGHT JOIN WORK .eb_etablissement etab ON ( ";
        sqlQuery += " 		etab.eb_etablissement_num = ebuser.x_eb_etablissement ";
        sqlQuery += " 		OR etab.eb_etablissement_num = rel.x_eb_etablissement ";
        sqlQuery += " 	) ";
        sqlQuery += " 	LEFT JOIN WORK .eb_etablissement etabHost ON ( ";
        sqlQuery += " 		etabHost.eb_etablissement_num = ebuser.x_eb_etablissement ";
        sqlQuery += " 	) ";

        return sqlQuery;
    }

    private Query getMapping(SearchCriteria criteria, String sqlQuery, Query query) {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);
        Integer idUser = connectedUser.getEbUserNum();
        String term = criteria.getSearchterm();

        query.setParameter("annule", Enumeration.EtatRelation.ANNULER.getCode());

        if (criteria.isSentRequests()) {
            query.setParameter("iduser", idUser);
            query.setParameter("encours", Enumeration.EtatRelation.EN_COURS.getCode());
        }

        if (criteria.isBlackList()) {
            query.setParameter("iduser", idUser);
            query.setParameter("blacklist", Enumeration.EtatRelation.BLACKLIST.getCode());
        }

        if (criteria.isEtablissementRequest()) {
            query.setParameter("etabuser", connectedUser.getEbEtablissement().getEbEtablissementNum());
        }

        if (criteria.getPageNumber() != null && criteria.getSize() != null) {
            if (criteria.getSize() > 0 && sqlQuery.contains(":limit")) query.setParameter("limit", criteria.getSize());
            if (criteria.getPageNumber() <= 0 && sqlQuery.contains(":offset")) query.setParameter("offset", 0);
            else if (sqlQuery
                .contains(":offset")) query.setParameter("offset", (criteria.getPageNumber() - 1) * criteria.getSize());
        }

        if (criteria.isContact()) {
            query.setParameter("actif", Enumeration.EtatRelation.ACTIF.getCode());
        }

        if (term != null && !term.isEmpty()) {
            query.setParameter("term", "%" + term + "%");
        }

        return query;
    }

    @Override
    public List<EbRelation> selectListEbEtablissementOfEbRelation(SearchCriteria criteria) {
        List<EbRelation> resultat = new ArrayList<EbRelation>();
        String sqlQuery = "";
        Query query = null;

        try {
            /**
             * ******************************** PARTIE SELECT
             * *****************************
             */
            sqlQuery += " SELECT ";

            sqlQuery += " 	rel.eb_relation_num, ";
            sqlQuery += " 	rel.type_relation, ";
            sqlQuery += " 	rel.etat, ";
            sqlQuery += " 	rel.date_ajout, ";
            sqlQuery += " 	rel.email AS rel_email, ";
            sqlQuery += " 	rel.x_eb_etablissement, ";
            sqlQuery += " 	rel.x_eb_etablissement_host, ";
            sqlQuery += " 	rel.flag_etablissement, ";
            sqlQuery += " 	rel.x_last_modified_by, ";

            sqlQuery += " 	etab.eb_etablissement_num, ";
            sqlQuery += " 	etab.nom AS etab_nom, ";
            sqlQuery += " 	etab.email AS etab_email, ";
            sqlQuery += " 	etab.service AS etab_service, ";
            sqlQuery += " 	etab.telephone AS etab_telephone, ";
            sqlQuery += " 	etab.logo AS etab_logo, ";
            sqlQuery += " 	etab.x_eb_compagnie as num_compagnie, ";

            sqlQuery += " 	rel.x_host AS x_host, ";
            sqlQuery += " 	rel.x_guest AS x_guest, ";

            sqlQuery += " 	ebuser.nom as host_nom, ";
            sqlQuery += " 	ebuser.prenom as host_prenom, ";
            sqlQuery += " 	ebuser.email as host_email, ";

            sqlQuery += " 	etabHost.eb_etablissement_num as etab_host_eb_etablissement_num, ";
            sqlQuery += " 	etabHost.nom as etab_host_nom, ";

            if (criteria.isEtablissementRequest()) sqlQuery += " 	'true' AS is_establishment_request ";
            else sqlQuery += " 	'false' AS is_establishment_request ";

            /**
             * ******************************** PARTIE JOINTURE
             * *****************************
             */
            sqlQuery += this.getGlobalJoinListEtablissement();

            /**
             * ******************************** PARTIE WHERE
             * *****************************
             */
            sqlQuery += this.getGlobalWhereListEtablissement(criteria);

            /**
             * ******************************** ORDER BY
             * *****************************
             */
            sqlQuery += " ORDER BY ";
            sqlQuery += " 	rel.etat IS NOT NULL DESC ";

            /**
             * ******************************** QUERY GLOBALE
             * *****************************
             */
            sqlQuery = " SELECT DISTINCT ON (eb_etablissement_num) * FROM ( " + sqlQuery + " ) AS subQuery ";

            /**
             * ******************************** PARTIE ORDER BY ET PAGINATION
             * *****************************
             */
            sqlQuery += " ORDER BY eb_etablissement_num, etat IS NOT NULL DESC ";

            if (criteria.getPageNumber() != null && criteria.getSize() != null) {
                sqlQuery += " OFFSET :offset ";
                if (criteria.getSize().compareTo(0) > 0) sqlQuery += " LIMIT :limit ";
            }

            /**
             * ******************************** MAPPING DES RESULTATS
             * *****************************
             */
            // voir le mapping @SqlResultSetMapping de la classe EbRelation

            query = this.getMapping(criteria, sqlQuery, em.createNativeQuery(sqlQuery, "EtablissementContactMapping"));

            resultat = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    private JPAQuery<EbEtablissement> setPaginationAndOrder(SearchCriteria criteria, JPAQuery<EbEtablissement> query) {

        if (criteria.getOrderedColumn() != null) {
            Path<Object> fieldPath = Expressions.path(Object.class, qEbEtablissement, criteria.getOrderedColumn());
            query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
        }

        if (criteria.getPageNumber() != null && criteria.getSize() != null) {
            query.offset(criteria.getPageNumber() * criteria.getSize()).limit(criteria.getSize());
        }
        else {
            query.orderBy(qEbEtablissement.ebEtablissementNum.desc());
        }

        if (criteria.getSize() != null && criteria.getSize() >= 0) {
            query.limit(criteria.getSize());
        }

        if (criteria.getPageNumber() != null) {
            query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
        }

        return query;
    }

    @Override
    public List<EbEtablissement> selectListEbEtablissement(SearchCriteria criteria) {
        List<EbEtablissement> resultat = new ArrayList<EbEtablissement>();

        QEbRelation qRelation = new QEbRelation("rel");
        QEbRelation qRelationHost = new QEbRelation("relHost");
        QEcCountry qCountry = new QEcCountry("qCountry");
        QEcRegion qRegion = new QEcRegion("qRegion");
        QEcOperationalGrouping qecOperationalGrouping = new QEcOperationalGrouping("qecOperationalGrouping");

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbEtablissement> query = new JPAQuery<EbEtablissement>(em);
            EbUser connectedUser = connectedUserService.getCurrentUser();

            if (criteria.showGlobals()) {
                query
                    .select(
                        QEbEtablissement
                            .create(
                                qEbEtablissement.ebEtablissementNum,
                                qEbEtablissement.nom,
                                qEbEtablissement.adresse,
                                qEbEtablissement.telephone,
                                qEbEtablissement.email,
                                qEbEtablissement.service,
                                qEbEtablissement.role,
                                qEbCompagnie.ebCompagnieNum,
                                qEbCompagnie.nom,
                                qEbCompagnie.code,
                                qEbEtablissement.canShowPrice,
                                qEbEtablissement.city,
                                qEbEtablissement.nic,
                                qEbEtablissement.siteWeb,
                                qEbEtablissement.typeEtablissement,
                                qCountry.codeLibelle,
                                qCountry.commercialMandatory,
                                qCountry.latitude,
                                qCountry.longitude,
                                qCountry.libelle,
                                qCountry.ecCountryNum,
                                qCountry.ecRegion().ecRegionNum,
                                qCountry.ecOperationalGrouping().ecOperationalGroupingNum

                            ));
            }
            else {
                query
                    .select(
                        QEbEtablissement
                            .create(
                                qEbEtablissement.ebEtablissementNum,
                                qEbEtablissement.nom,
                                qEbEtablissement.adresse,
                                qEbEtablissement.telephone,
                                qEbEtablissement.email,
                                qEbEtablissement.service,
                                qEbEtablissement.role,
                                qEbEtablissement.siteWeb,
                                qEbEtablissement.typeEtablissement,
                                qEbCompagnie.ebCompagnieNum,
                                qEbCompagnie.nom,
                                qEbCompagnie.code));
            }

            where = gettEtablissementGlobalWhere(where, criteria);

            query.from(qEbEtablissement);
            query = getEtablissementGlobalJoin(query, where, criteria);
            query.where(where);
            query = setPaginationAndOrder(criteria, query);
            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @Override
    public Long getListEtablissementCount(SearchCriteria criteria) {
        Long result = new Long(0);

        QEcCountry qCountry = new QEcCountry("qCountry");
        QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbCompagnie");

        try {
            JPAQuery<EbEtablissement> query = new JPAQuery<EbEtablissement>(em);
            BooleanBuilder where = new BooleanBuilder();

            where = gettEtablissementGlobalWhere(where, criteria);
            query.from(qEbEtablissement).where(where);
            query = getEtablissementGlobalJoin(query, where, criteria);
            query = setPaginationAndOrder(criteria, query);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private BooleanBuilder gettEtablissementGlobalWhere(BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        QEcCountry qCountry = new QEcCountry("qCountry");
        QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbCompagnie");

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm();
            BooleanBuilder bb = new BooleanBuilder();

            bb
                .andAnyOf(
                    qEbEtablissement.nic.trim().likeIgnoreCase("%" + term + "%"),
                    qEbEtablissement.ecCountry().codeLibelle.trim().likeIgnoreCase("%" + term + "%"),
                    qEbEtablissement.city.trim().likeIgnoreCase("%" + term + "%"),
                    qEbEtablissement.typeEtablissement.trim().likeIgnoreCase("%" + term + "%"),
                    qEbEtablissement.telephone.trim().likeIgnoreCase("%" + term + "%"));

            if (connectedUser.isChargeur() || connectedUser.isControlTower()) {
                where
                    .andAnyOf(
                        bb,
                        qEbEtablissement.nom.containsIgnoreCase(term),
                        qEbEtablissement.email.containsIgnoreCase(term),
                        qEbEtablissement.telephone.containsIgnoreCase(term),
                        qEbCompagnie.code.containsIgnoreCase(term),
                        qEbCompagnie.nom.containsIgnoreCase(term));
            }
            else if (connectedUser.isPrestataire()) {
                where.andAnyOf(bb, qEbEtablissement.email.eq(term));
            }

        }

        if (criteria.isRoleBroker() && criteria.isRoleTransporteur()) {
            where.and(qEbEtablissement.service.eq(ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode()));
        }
        else if (criteria.isRoleBroker()) {
            where.and(qEbEtablissement.service.eq(ServiceType.SERVICE_BROKER.getCode()));
        }
        else if (criteria.isRoleTransporteur()) {
            where.and(qEbEtablissement.service.eq(ServiceType.SERVICE_TRANSPORTEUR.getCode()));
        }
        else if (criteria.isRoleChargeur()) {
            where.and(qEbEtablissement.service.isNull());
        }

        if (criteria.isRoleChargeur()) {
            where.and(qEbEtablissement.role.eq(Enumeration.Role.ROLE_CHARGEUR.getCode()));
        }

        if (criteria.isFromCommunity()) {
            where
                .andAnyOf(
                    qEbUserGuest.ebUserNum.eq(connectedUser.getEbUserNum()),
                    qEbUserHost.ebUserNum.eq(connectedUser.getEbUserNum()));
            where
                .and(
                    qEbEtablissement.ebEtablissementNum.ne(connectedUser.getEbEtablissement().getEbEtablissementNum()));
        }

        if (criteria.getEbEtablissementNum() != null) where
            .and(qEbEtablissement.ebEtablissementNum.eq(criteria.getEbEtablissementNum()));
        if (criteria.getEbCompagnieNum() != null) where
            .and(qEbCompagnie.ebCompagnieNum.eq(criteria.getEbCompagnieNum()));

        return where;
    }

    private JPAQuery getEtablissementGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        QEbRelation qRelation = new QEbRelation("rel");
        QEbRelation qRelationHost = new QEbRelation("relHost");
        QEcCountry qCountry = new QEcCountry("qCountry");
        QEcRegion qRegion = new QEcRegion("qRegion");
        QEcOperationalGrouping qecOperationalGrouping = new QEcOperationalGrouping("qecOperationalGrouping");
        QEbCompagnie qEbCompagnie = new QEbCompagnie("qEbCompagnie");

        query.leftJoin(qEbEtablissement.ebCompagnie(), qEbCompagnie);
        query.leftJoin(qEbEtablissement.ecCountry(), qCountry);
        query.leftJoin(qEbEtablissement.ecCountry().ecRegion(), qRegion);
        query.leftJoin(qEbEtablissement.ecCountry().ecOperationalGrouping(), qecOperationalGrouping);

        if (criteria.isFromCommunity()) {
            query
                .leftJoin(qEbEtablissement.etablissementsGuests, qRelation)
                .on(qRelation.etat.eq(EtatRelation.ACTIF.getCode()));
            query
                .leftJoin(qEbEtablissement.etablissementsHost, qRelationHost)
                .on(qRelationHost.etat.eq(EtatRelation.ACTIF.getCode()));
            query.leftJoin(qRelationHost.guest(), qEbUserGuest);
            query.leftJoin(qRelation.host(), qEbUserHost);
        }

        return query;
    }
}
