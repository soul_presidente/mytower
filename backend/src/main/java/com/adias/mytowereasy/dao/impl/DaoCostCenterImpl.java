package com.adias.mytowereasy.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.JPQLQuery;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoCostCenter;
import com.adias.mytowereasy.model.EbCostCenter;
import com.adias.mytowereasy.model.QEbCostCenter;


@Component
@Transactional
public class DaoCostCenterImpl implements DaoCostCenter {
    @PersistenceContext
    EntityManager em;

    QEbCostCenter qEbCostCenter = QEbCostCenter.ebCostCenter;

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbCostCenter> findActiveCostCenters(Integer ebCompagnieNum) {
        List<EbCostCenter> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPQLQuery query = new JPAQuery(em);

            where.and(qEbCostCenter.compagnie().ebCompagnieNum.eq(ebCompagnieNum));
            where.andAnyOf(qEbCostCenter.deleted.isFalse(), qEbCostCenter.deleted.isNull());

            query.distinct();
            query.select(qEbCostCenter);
            query.from(qEbCostCenter);
            query.where(where);

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }
}
