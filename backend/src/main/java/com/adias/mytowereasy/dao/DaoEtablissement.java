/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao;

import java.math.BigInteger;
import java.util.List;

import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbRelation;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoEtablissement {
    EbEtablissement selectEbEtablissement(SearchCriteria criterias);

    List<EbEtablissement> selectListEbEtablissement(SearchCriteria criterias);

    List<EbRelation> selectListEbEtablissementOfEbRelation(SearchCriteria criterias);

    EbEtablissement selectEbEtablissementByEbUser(Integer ebUserNum);

    BigInteger selectCountListEbEtablissementOfEbRelation(SearchCriteria criteria);

    Long getListEtablissementCount(SearchCriteria criteria);
}
