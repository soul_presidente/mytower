/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.custom;

import java.util.List;

import com.adias.mytowereasy.model.custom.EbFormField;


public interface DaoCustom {
    List<EbFormField> getListEbFormField();
}
