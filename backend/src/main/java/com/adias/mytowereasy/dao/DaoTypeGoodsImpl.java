package com.adias.mytowereasy.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.model.EbTypeGoods;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.model.QEbTypeGoods;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoTypeGoodsImpl implements DaoTypeGoods {
    @PersistenceContext
    EntityManager em;
    @Autowired
    ConnectedUserService connectedUserService;

    QEbTypeGoods qTypeGoods = QEbTypeGoods.ebTypeGoods;
    QEbCompagnie qEbCompagnie = QEbCompagnie.ebCompagnie;

    @Override
    public List<EbTypeGoods> list(SearchCriteria criteria) {
        List<EbTypeGoods> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbTypeGoods> query = new JPAQuery<>(em);

            query
                .select(
                    QEbTypeGoods
                        .create(
                            qTypeGoods.ebTypeGoodsNum,
                            qTypeGoods.code,
                            qTypeGoods.label,
                            qEbCompagnie.ebCompagnieNum,
                            qEbCompagnie.nom)

                );
            query.distinct().from(qTypeGoods);
            defineQueryGlobals(query, where, criteria, true);
            query.where(where);

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @Override
    public Long listCount(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbTypeGoods> query = new JPAQuery<EbTypeGoods>(em);
            BooleanBuilder where = new BooleanBuilder();

            query.distinct().from(qTypeGoods);
            defineQueryGlobals(query, where, criteria, false);
            query.where(where);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    private void
        defineQueryGlobals(JPAQuery query, BooleanBuilder where, SearchCriteria criteria, boolean enablePagination) {
        // Join
        query.leftJoin(qTypeGoods.xEbCompany(), qEbCompagnie);

        EbUser connectedUser = connectedUserService.getCurrentUser();

        // Global Where
        where.and(qTypeGoods.deleted.isFalse());

        if (criteria.getCompanyNums() == null || criteria.getCompanyNums().size() == 0) {
            where.and(qEbCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }
        else {
            where.and(qEbCompagnie.ebCompagnieNum.in(criteria.getCompanyNums()));
        }

        if (StringUtils.isNotBlank(criteria.getSearchterm())) {
            String searchTerm = criteria.getSearchterm().toLowerCase();
            where.andAnyOf(qTypeGoods.code.lower().contains(searchTerm), qTypeGoods.label.lower().contains(searchTerm));
        }

        // Order
        if (criteria.getOrderedColumn() != null) {
            Path<Object> fieldPath = Expressions.path(Object.class, qTypeGoods, criteria.getOrderedColumn());
            query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
        }
        else {
            query.orderBy(qTypeGoods.ebTypeGoodsNum.asc());
        }

        // Pagination
        if (enablePagination) {

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

        }

    }
}
