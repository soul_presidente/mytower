/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.dao.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.Tuple;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoAnalytics;
import com.adias.mytowereasy.model.Enumeration.ModeTransport;
import com.adias.mytowereasy.model.analytics.*;
import com.adias.mytowereasy.utils.search.SearchCriteriaFreightAnalytics;


@Component
@Transactional
public class DaoAnalyticsImpl implements DaoAnalytics {
    @PersistenceContext
    EntityManager em;

    private QDwrFShipments qDwrFShipments = QDwrFShipments.dwrFShipments;
    private QDwrDCountry qDwrDCountry = QDwrDCountry.dwrDCountry;
    private QDwrDMonth qDwrDMonth = QDwrDMonth.dwrDMonth;
    private QDwrDCarrier qDwrDCarrier = QDwrDCarrier.dwrDCarrier;
    private QDwrDCharger qDwrDCharger = QDwrDCharger.dwrDCharger;

    private boolean isMonthJoined = false;
    private boolean isCarrierJoined = false;
    private boolean isChargerJoined = false;
    private boolean isOriginCountryJoined = false;
    private boolean isDestinationCountryJoined = false;
    private boolean isTransportTypeGrouped = false;

    public JSONObject getTransportData(SearchCriteriaFreightAnalytics criteria) throws JSONException {
        JSONObject result = null;

        try {
            // Déclaration des queries
            JPAQuery<DwrFShipments> queryTopOrigin = new JPAQuery<DwrFShipments>(em);
            JPAQuery<DwrFShipments> queryTopDestination = new JPAQuery<DwrFShipments>(em);
            JPAQuery<DwrFShipments> queryOverallCostWeight = new JPAQuery<DwrFShipments>(em);
            JPAQuery<DwrFShipments> queryTotalCosts = new JPAQuery<DwrFShipments>(em);
            JPAQuery<DwrFShipments> queryCostWeightRatios = new JPAQuery<DwrFShipments>(em);
            JPAQuery<DwrFShipments> queryTransportActivity = new JPAQuery<DwrFShipments>(em);
            // Application des critères
            queryTopOrigin = applyCriteria(queryTopOrigin, criteria);
            queryTopDestination = applyCriteria(queryTopDestination, criteria);
            queryOverallCostWeight = applyCriteria(queryOverallCostWeight, criteria);
            queryTotalCosts = applyCriteria(queryTotalCosts, criteria);
            queryCostWeightRatios = applyCriteria(queryCostWeightRatios, criteria);
            queryTransportActivity = applyCriteria(queryTransportActivity, criteria);

            // Top origin Country
            if (!isOriginCountryJoined) {
                queryTopOrigin.join(qDwrFShipments.originCountry(), qDwrDCountry);
                queryTopOrigin.groupBy(qDwrFShipments.originCountry().dwrDCountryNum);
            }

            queryTopOrigin.groupBy(qDwrFShipments.originCountry().name);
            queryTopOrigin.orderBy(qDwrFShipments.nbExpeditions.sum().desc()).limit(5);
            List<Tuple> resultQuery = queryTopOrigin
                .select(qDwrFShipments.nbExpeditions.sum(), qDwrFShipments.originCountry().name).fetch();
            JSONArray resultTopOrigin = new JSONArray();

            for (Tuple tuple: resultQuery) {
                JSONObject row = new JSONObject();
                row.put("nbExpedition", tuple.get(qDwrFShipments.nbExpeditions.sum()));
                row.put("countryName", tuple.get(qDwrFShipments.originCountry().name));
                resultTopOrigin.put(row);
            }

            // Top destination Country
            if (!isDestinationCountryJoined) {
                queryTopOrigin.join(qDwrFShipments.destinationCountry(), qDwrDCountry);
                queryTopOrigin.groupBy(qDwrFShipments.destinationCountry().dwrDCountryNum);
            }

            queryTopDestination.groupBy(qDwrFShipments.destinationCountry().name);
            queryTopDestination.orderBy(qDwrFShipments.nbExpeditions.sum().desc()).limit(5);
            resultQuery = queryTopDestination
                .select(qDwrFShipments.nbExpeditions.sum(), qDwrFShipments.destinationCountry().name).fetch();
            JSONArray resultTopDestination = new JSONArray();

            for (Tuple tuple: resultQuery) {
                JSONObject row = new JSONObject();
                row.put("nbExpedition", tuple.get(qDwrFShipments.nbExpeditions.sum()));
                row.put("countryName", tuple.get(qDwrFShipments.destinationCountry().name));
                resultTopDestination.put(row);
            }

            // Overall Cost/Weight Ratio
            Tuple resultSingleQuery = queryOverallCostWeight
                .select(qDwrFShipments.totalCost.sum(), qDwrFShipments.totalWeight.sum()).fetchOne();
            Double totalCost = resultSingleQuery.get(qDwrFShipments.totalCost.sum());
            Double totalWeight = resultSingleQuery.get(qDwrFShipments.totalWeight.sum());
            JSONObject resultOverallCostWeightRatio = new JSONObject();

            if (totalWeight == 0) {
                resultOverallCostWeightRatio.put("value", 0);
            }
            else {
                resultOverallCostWeightRatio.put("value", totalCost / totalWeight);
            }

            // Total costs
            Double resultTotalCostsQuery = queryTotalCosts.select(qDwrFShipments.totalCost.sum()).fetchOne();
            JSONObject resultTotalCosts = new JSONObject();
            resultTotalCosts.put("value", resultTotalCostsQuery);

            // Cost/Weight Ratios
            if (!isTransportTypeGrouped) {
                queryCostWeightRatios.groupBy(qDwrFShipments.transportType);
            }

            resultQuery = queryCostWeightRatios
                .select(qDwrFShipments.totalCost.sum(), qDwrFShipments.totalWeight.sum(), qDwrFShipments.transportType)
                .fetch();
            JSONArray resultCostWeightRatios = new JSONArray();

            for (Tuple tuple: resultQuery) {
                JSONObject row = new JSONObject();
                Double cost = tuple.get(qDwrFShipments.totalCost.sum());
                Double weight = tuple.get(qDwrFShipments.totalWeight.sum());

                if (weight == 0) {
                    row.put("value", 0);
                }
                else {
                    row.put("value", cost / weight);
                }

                row.put("transportType", ModeTransport.getLibelleByCode(tuple.get(qDwrFShipments.transportType)));
                resultCostWeightRatios.put(row);
            }

            // Transport Activity
            if (!isTransportTypeGrouped) {
                queryTransportActivity.groupBy(qDwrFShipments.transportType);
            }

            resultQuery = queryTransportActivity
                .select(qDwrFShipments.nbExpeditions.sum(), qDwrFShipments.transportType).fetch();
            JSONArray resultTransportActivity = new JSONArray();

            for (Tuple tuple: resultQuery) {
                JSONObject row = new JSONObject();
                row.put("nbExpedition", tuple.get(qDwrFShipments.nbExpeditions.sum()));
                row.put("transportType", ModeTransport.getLibelleByCode(tuple.get(qDwrFShipments.transportType)));
                resultTransportActivity.put(row);
            }

            // Wrapping results
            result = new JSONObject();
            result.put("topOriginCountries", resultTopOrigin);
            result.put("transportsActivity", resultTransportActivity);
            result.put("overallCostWeightRatio", resultOverallCostWeightRatio);
            result.put("totalCosts", resultTotalCosts);
            result.put("topDestinationCountries", resultTopDestination);
            result.put("costWeightRatios", resultCostWeightRatios);

            clearState();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    public JSONObject getActivityData(SearchCriteriaFreightAnalytics criteria) throws JSONException {
        JSONObject result = new JSONObject();

        try {
            // Déclaration des queries
            JPAQuery<DwrFShipments> queryActivityCostShipments = new JPAQuery<DwrFShipments>(em);
            JPAQuery<DwrFShipments> queryActivityPerCarrier = new JPAQuery<DwrFShipments>(em);
            // Apply criterias
            queryActivityCostShipments = applyCriteria(queryActivityCostShipments, criteria);
            queryActivityPerCarrier = applyCriteria(queryActivityPerCarrier, criteria);

            // Transport activity cost and shipments current year
            if (!isMonthJoined) {
                LocalDateTime now = LocalDateTime.now();
                Integer currentYear = Integer.valueOf(now.getYear());
                queryActivityCostShipments.join(qDwrFShipments.datePickup(), qDwrDMonth);
                queryActivityCostShipments.groupBy(qDwrFShipments.datePickup().month);
                queryActivityCostShipments.groupBy(qDwrFShipments.datePickup().year);
                queryActivityCostShipments.where(qDwrFShipments.datePickup().year.eq(currentYear));
            }

            queryActivityCostShipments.orderBy(qDwrFShipments.datePickup().month.asc());
            List<Tuple> resultQuery = queryActivityCostShipments
                .select(
                    qDwrFShipments.nbExpeditions.sum(),
                    qDwrFShipments.totalCost.sum(),
                    qDwrFShipments.datePickup().month,
                    qDwrFShipments.datePickup().year)
                .fetch();
            JSONArray resultActivityCostCurrentYear = new JSONArray();
            JSONArray resultActivityShipmentsCurrentYear = new JSONArray();

            for (Tuple tuple: resultQuery) {
                JSONObject rowShipments = new JSONObject();
                JSONObject rowCost = new JSONObject();
                rowShipments.put("value", tuple.get(qDwrFShipments.nbExpeditions.sum()));
                rowShipments.put("month", tuple.get(qDwrFShipments.datePickup().month));
                rowShipments.put("year", tuple.get(qDwrFShipments.datePickup().year));
                rowCost.put("value", tuple.get(qDwrFShipments.totalCost.sum()));
                rowCost.put("month", tuple.get(qDwrFShipments.datePickup().month));
                rowCost.put("year", tuple.get(qDwrFShipments.datePickup().year));
                resultActivityShipmentsCurrentYear.put(rowShipments);
                resultActivityCostCurrentYear.put(rowCost);
            }

            // Transport activity cost and shipments last year
            JSONArray resultActivityCostLastYear = new JSONArray();
            JSONArray resultActivityShipmentsLastYear = new JSONArray();

            if (!isMonthJoined) {
                LocalDateTime now = LocalDateTime.now();
                Integer currentYear = Integer.valueOf(now.getYear());
                queryActivityCostShipments.join(qDwrFShipments.datePickup(), qDwrDMonth);
                queryActivityCostShipments.groupBy(qDwrFShipments.datePickup().month);
                queryActivityCostShipments.groupBy(qDwrFShipments.datePickup().year);
                queryActivityCostShipments.where(qDwrFShipments.datePickup().year.eq(currentYear - 1));
                queryActivityCostShipments.orderBy(qDwrFShipments.datePickup().month.asc());
                resultQuery = queryActivityCostShipments
                    .select(
                        qDwrFShipments.nbExpeditions.sum(),
                        qDwrFShipments.totalCost.sum(),
                        qDwrFShipments.datePickup().month,
                        qDwrFShipments.datePickup().year)
                    .fetch();

                for (Tuple tuple: resultQuery) {
                    JSONObject rowShipments = new JSONObject();
                    JSONObject rowCost = new JSONObject();
                    rowShipments.put("value", tuple.get(qDwrFShipments.nbExpeditions.sum()));
                    rowShipments.put("month", tuple.get(qDwrFShipments.datePickup().month));
                    rowShipments.put("year", tuple.get(qDwrFShipments.datePickup().year));
                    rowCost.put("value", tuple.get(qDwrFShipments.totalCost.sum()));
                    rowCost.put("month", tuple.get(qDwrFShipments.datePickup().month));
                    rowCost.put("year", tuple.get(qDwrFShipments.datePickup().year));
                    resultActivityShipmentsLastYear.put(rowShipments);
                    resultActivityCostLastYear.put(rowCost);
                }

            }

            // Transport Activity Per Carrier
            if (!isCarrierJoined) {
                queryActivityPerCarrier.join(qDwrFShipments.carrier(), qDwrDCarrier);
                queryActivityPerCarrier.groupBy(qDwrFShipments.carrier().dwrDCarrierNum);
                queryActivityPerCarrier.groupBy(qDwrFShipments.carrier().name);
            }

            resultQuery = queryActivityPerCarrier
                .select(qDwrFShipments.nbExpeditions.sum(), qDwrFShipments.carrier().name).fetch();
            JSONArray resultActivityPerCarrier = new JSONArray();

            for (Tuple tuple: resultQuery) {
                JSONObject row = new JSONObject();
                row.put("value", tuple.get(qDwrFShipments.nbExpeditions.sum()));
                row.put("name", tuple.get(qDwrFShipments.carrier().name));
                resultActivityPerCarrier.put(row);
            }

            result.put("activityShipmentsCurrentYear", resultActivityShipmentsCurrentYear);
            result.put("activityCostCurrentYear", resultActivityCostCurrentYear);
            result.put("activityShipmentsLastYear", resultActivityShipmentsLastYear);
            result.put("activityCostLastYear", resultActivityCostLastYear);
            result.put("activityPerCarrier", resultActivityPerCarrier);
            clearState();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private JPAQuery<DwrFShipments>
        applyCriteria(JPAQuery<DwrFShipments> query, SearchCriteriaFreightAnalytics criteria) {
        BooleanBuilder where = new BooleanBuilder();
        boolean isWhere = false;
        query.from(qDwrFShipments);

        if (criteria.getMonth() != null && criteria.getYear() != null) {
            where.and(qDwrFShipments.datePickup().month.eq(criteria.getMonth()));
            where.and(qDwrFShipments.datePickup().year.eq(criteria.getYear()));
            query.join(qDwrFShipments.datePickup(), qDwrDMonth);
            query.groupBy(qDwrFShipments.datePickup().month);
            query.groupBy(qDwrFShipments.datePickup().year);
            isMonthJoined = true;
            isWhere = true;
        }

        if (criteria.getCarrier() != null) {
            where.and(qDwrDCarrier.dwrDCarrierNum.eq(criteria.getCarrier()));
            query.join(qDwrFShipments.carrier(), qDwrDCarrier);
            query.groupBy(qDwrFShipments.carrier().dwrDCarrierNum);
            isCarrierJoined = true;
            isWhere = true;
        }

        if (criteria.getShipper() != null) {
            where.and(qDwrDCharger.dwrDChargerNum.eq(criteria.getShipper()));
            query.join(qDwrFShipments.charger(), qDwrDCharger);
            query.groupBy(qDwrFShipments.charger().dwrDChargerNum);
            isChargerJoined = true;
            isWhere = true;
        }

        if (criteria.getOriginCountry() != null) {
            where.and(qDwrDCountry.dwrDCountryNum.eq(criteria.getOriginCountry()));
            query.join(qDwrFShipments.originCountry(), qDwrDCountry);
            query.groupBy(qDwrFShipments.originCountry().dwrDCountryNum);
            isOriginCountryJoined = true;
            isWhere = true;
        }

        if (criteria.getDestinationCountry() != null) {
            where.and(qDwrDCountry.dwrDCountryNum.eq(criteria.getDestinationCountry()));
            query.join(qDwrFShipments.destinationCountry(), qDwrDCountry);
            query.groupBy(qDwrFShipments.destinationCountry().dwrDCountryNum);
            isDestinationCountryJoined = true;
            isWhere = true;
        }

        if (criteria.getTransportType() != null) {
            where.and(qDwrFShipments.transportType.eq(criteria.getTransportType()));
            query.groupBy(qDwrFShipments.transportType);
            isTransportTypeGrouped = true;
            isWhere = true;
        }

        if (criteria.getStartMonth() != null && criteria.getEndMonth() != null && criteria.getStartYear() != null
            && criteria.getEndYear() != null) {
            where.and(qDwrFShipments.datePickup().month.goe(criteria.getStartMonth()));
            where.and(qDwrFShipments.datePickup().year.goe(criteria.getStartYear()));
            where.and(qDwrFShipments.datePickup().month.loe(criteria.getEndMonth()));
            where.and(qDwrFShipments.datePickup().year.loe(criteria.getEndYear()));

            if (!isMonthJoined) {
                query.join(qDwrFShipments.datePickup(), qDwrDMonth);
                query.groupBy(qDwrFShipments.datePickup().month);
                query.groupBy(qDwrFShipments.datePickup().year);
                isMonthJoined = true;
            }

            isWhere = true;
        }

        if (isWhere) {
            query.where(where);
        }

        return query;
    }

    private void clearState() {
        isMonthJoined = false;
        isCarrierJoined = false;
        isChargerJoined = false;
        isOriginCountryJoined = false;
        isDestinationCountryJoined = false;
        isTransportTypeGrouped = false;
    }
}
