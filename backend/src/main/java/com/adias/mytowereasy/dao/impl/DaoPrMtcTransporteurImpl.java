package com.adias.mytowereasy.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.DaoPrMtcTransporteur;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoPrMtcTransporteurImpl extends Dao implements DaoPrMtcTransporteur {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QPrMtcTransporteur qPrMtcTransporteur = QPrMtcTransporteur.prMtcTransporteur;

    @Override
    public List<PrMtcTransporteur> getListPrMtcTransporteur(SearchCriteria criteria) {
        List<PrMtcTransporteur> result = null;
        BooleanBuilder where = new BooleanBuilder();
        EbUser connectedUser = connectedUserService.getCurrentUser();

        QEbCompagnie qEbCompCarrier = new QEbCompagnie("qEbCompCarrier");
        QEbCompagnie qEbCompChargeur = new QEbCompagnie("qEbCompChargeur");
        QEbUser qEbUser = new QEbUser("qEbUser");
        QEbEtablissement qEbEtablissement = new QEbEtablissement("qEbEtablissement");

        try {
            JPAQuery<PrMtcTransporteur> query = new JPAQuery<PrMtcTransporteur>(em);

            query
                .select(
                    QPrMtcTransporteur
                        .create(
                            qPrMtcTransporteur.prMtcTransporteurNum,
                            qEbCompCarrier.ebCompagnieNum,
                            qEbCompCarrier.code,
                            qEbCompCarrier.nom,
                            qPrMtcTransporteur.codeTransporteurMtc,
                            qEbUser.ebUserNum,
                            qEbUser.nom,
                            qEbUser.prenom,
                            qEbUser.email,
                            qEbUser.username,
                            qPrMtcTransporteur.codeConfigurationMtc,
                            qEbEtablissement.ebEtablissementNum,
                            qEbEtablissement.code,
                            qEbEtablissement.nom,
                            qPrMtcTransporteur.codeConfigurationEDI));

            if (criteria.getEbCompagnieNum() != null) where
                .and(qEbCompCarrier.ebCompagnieNum.eq(criteria.getEbCompagnieNum()));

            if (StringUtils.isNotBlank(criteria.getCompanyCode())) {
                where.and(qEbCompCarrier.code.eq(criteria.getCompanyCode()));
            }

            if (connectedUser != null && connectedUser.getEbCompagnie().getEbCompagnieNum() != null) where
                .and(qEbCompChargeur.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));

            query.from(qPrMtcTransporteur);
            query.where(where);
            query.leftJoin(qPrMtcTransporteur.xEbCompagnieTransporteur(), qEbCompCarrier);
            query.leftJoin(qPrMtcTransporteur.xEbCompagnieChargeur(), qEbCompChargeur);
            query.leftJoin(qPrMtcTransporteur.xEbUser(), qEbUser);
            query.leftJoin(qEbUser.ebEtablissement(), qEbEtablissement);

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            result = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public Long getCountPrMtcTransporteur(SearchCriteria criteria) {
        return null;
    }

    @Override
    public PrMtcTransporteur savePrMtcTransporteur(PrMtcTransporteur prMtcTransporteur) {
        return null;
    }

    @Override
    public int getCountListMtgTransporteur(SearchCriteria criteria) {
        long count = 0l;

        try {
            JPAQuery<PrMtcTransporteur> query = new JPAQuery<PrMtcTransporteur>(em);
            query.from(qPrMtcTransporteur);
            query.distinct();
            count = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return (int) count;
    }
}
