package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbSavedForm;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoSavedForm {
    List<EbSavedForm> listRules(SearchCriteria criteria);

    Long countListRules(SearchCriteria criteria);
}
