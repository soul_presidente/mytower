package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbCost;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoCostItem {
    public List<EbCost> getListCostItemsByCompagnie(SearchCriteria criteria);

    public Long getCountListCostItemByCompagnie(SearchCriteria criteria);

    public Integer nextValEbCost();

    public Integer nextValEbCostCategorie();
}
