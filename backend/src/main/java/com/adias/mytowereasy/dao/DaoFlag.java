package com.adias.mytowereasy.dao;

import java.util.List;

import com.adias.mytowereasy.model.EbFlag;
import com.adias.mytowereasy.utils.search.PageableSearchCriteria;


public interface DaoFlag {
    List<EbFlag> getFlagsByCriteria(PageableSearchCriteria flagSearchCriteria);

    Long countListFlag(PageableSearchCriteria flagSearchCriteria);
}
