package com.adias.mytowereasy.mtc.utils;

import java.util.ArrayList;
import java.util.function.Function;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.WebClient;

import com.adias.moteur.chiffrage.library.shared.Statiques;

import reactor.core.publisher.Mono;

@Component
public class TokenProviderMtc {
    private String token;
    private AccessMtc accessMtc;
    private WebClient webClient;

    public TokenProviderMtc(@Autowired AccessMtc accessMtc) {
        this.webClient = WebClient.create(accessMtc.getApiUrl());
        this.accessMtc = accessMtc;
    }
    public Mono<String> getAccessToken(ClientRequest request) {
        Mono<String> result;
        if (token == null) result = getAccessTokenWithWebClient();
        else result = Mono.just(token);
        return result;
    }

    public Mono<String> getAccessTokenWithWebClient() {
        UserMtc user = new UserMtc(accessMtc.getUsername(), accessMtc.getPassword(), true);
        Mono<String> result = webClient
            .post().uri(accessMtc.getAuthUrl()).body(BodyInserters.fromObject(user)).exchange().map(response -> {
                String authorizationHeader = response.headers().asHttpHeaders().getFirst("authorization");

                if (StringUtils.isNotEmpty(authorizationHeader)) {
                    String parts[] = authorizationHeader.split(StringUtils.SPACE);
                    if (parts != null && parts.length > 1 && "Bearer".equals(parts[0])) return parts[1];
                }

                return StringUtils.EMPTY;
            });
        return result;
    }
    public Function<String, ClientRequest> setBearerTokenInHeader(ClientRequest request) {
        return token -> {
            this.saveToken(token);
            ClientRequest clientRequest = ClientRequest
                .from(request)
                // Clear Header Token Before Put The New One
                .headers(header -> {
                    if (header
                        .containsKey(
                            Statiques.HEADER_STRING)) header.put(Statiques.HEADER_STRING, new ArrayList<String>());
                }).header(Statiques.HEADER_STRING, "Bearer " + token).build();
            return clientRequest;
        };
    }
    private void saveToken(String token) {
        this.token = token;
    }
}