package com.adias.mytowereasy.mtc.service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import com.adias.moteur.chiffrage.library.shared.ResultEntity;
import com.adias.mytowereasy.dao.DaoPrMtcTransporteur;
import com.adias.mytowereasy.dto.PricingCarrierSearchResultDTO;
import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.model.Enumeration;
import com.adias.mytowereasy.model.Enumeration.StatutDemande;
import com.adias.mytowereasy.model.ExEbDemandeTransporteur;
import com.adias.mytowereasy.model.PrMtcTransporteur;
import com.adias.mytowereasy.model.enums.ConsolidationGroupage;
import com.adias.mytowereasy.repository.*;
import com.adias.mytowereasy.service.PricingServiceImpl;
import com.adias.mytowereasy.trpl.model.SearchCriteriaPlanTransport;
import com.adias.mytowereasy.utils.search.SearchCriteria;



@Service
public class ValorizationService {
    @Autowired
    private EbDemandeRepository ebDemandeRepository;
    @Autowired
    private ExEbDemandeTransporteurRepository exEbDemandeTransporteurRepository;

    @Autowired
    private MtcService mtcService;

    @Autowired
    private EbMarchandiseRepository ebMarchandiseRepository;

    @Autowired
    private EbUserRepository ebUserRepository;

    @Autowired
    private EbCompagnieRepository ebCompagnieRepository;

    @Autowired
    private DaoPrMtcTransporteur daoPrMtcTransporteur;

		Logger																	logger	= LoggerFactory
			.getLogger(PricingServiceImpl.class);

    public List<PricingCarrierSearchResultDTO> searchCarrierFromMtc(SearchCriteriaPlanTransport searchCriteria)
        throws JsonProcessingException {
        EbDemande demande = searchCriteria.getEbDemande();

        List<PricingCarrierSearchResultDTO> list = new ArrayList<PricingCarrierSearchResultDTO>();

        List<PrMtcTransporteur> listMtcTransporteur = daoPrMtcTransporteur
            .getListPrMtcTransporteur(new SearchCriteria());

        if (listMtcTransporteur != null && !listMtcTransporteur.isEmpty()) {

            for (PrMtcTransporteur prMtcTr: listMtcTransporteur) {

                if (isValidTranporteur( prMtcTr)) {
                    ExEbDemandeTransporteur exEbDemandeTransporteur = new ExEbDemandeTransporteur();
                    exEbDemandeTransporteur.setEmailTransporteur(prMtcTr.getxEbUser().getEmail());
                    exEbDemandeTransporteur.setxEbCompagnie(prMtcTr.getxEbCompagnieTransporteur());
                    exEbDemandeTransporteur.setxEbEtablissement(prMtcTr.getxEbUser().getEbEtablissement());
                    List<PricingCarrierSearchResultDTO> pricingCarrierSearchResultDTOList = getPricingList(demande, prMtcTr, exEbDemandeTransporteur);

                     pricingCarrierSearchResultDTOList.stream()
                            .peek(pricing -> {
                                pricing.setCarrierName(prMtcTr.getxEbUser().getNom());
                                pricing.setCarrierUserNum(prMtcTr.getxEbUser().getEbUserNum());
                            })
                            .findFirst()
                            .ifPresent(list::add);
                }

            }

        }

        return list;
    }

    private List<PricingCarrierSearchResultDTO> getPricingList(EbDemande demande, PrMtcTransporteur prMtcTr, ExEbDemandeTransporteur exEbDemandeTransporteur) throws JsonProcessingException {
        return mtcService.manageResult(
                mtcService
                    .valuationFromMTC(
                        demande,
                        prMtcTr.getCodeConfigurationMtc()),
                demande,
                exEbDemandeTransporteur);
    }

    private boolean isValidTranporteur(PrMtcTransporteur prMtcTr) {
        return StringUtils.isNotBlank(prMtcTr.getCodeConfigurationMtc())
                && prMtcTr.getxEbCompagnieTransporteur() != null
                && prMtcTr.getxEbCompagnieTransporteur().getEbCompagnieNum() != null && prMtcTr.getxEbUser() != null &&
                prMtcTr.getxEbUser().getEbUserNum() != null && prMtcTr.getxEbUser().getEbEtablissement() != null
                && prMtcTr.getxEbUser().getEbEtablissement().getEbEtablissementNum() != null;
    }

		private List<EbDemande> getNotValorizedTrs()
		{
			List<EbDemande> notValorizedTrsWithNatureEDI = ebDemandeRepository
				.selectListEbDemandeNotValorized(false, Enumeration.NatureDemandeTransport.EDI.getCode());

			// demande created with draft mode
			List<EbDemande> notValorizedDraftTrs = ebDemandeRepository
				.selectDraftEbDemandeNotValorized(
					false,
					Enumeration.NatureDemandeTransport.NORMAL.getCode(),
					Enumeration.StatutDemande.PND.getCode()
				);

			return Lists
				.newArrayList(Iterables.concat(notValorizedDraftTrs, notValorizedTrsWithNatureEDI));
		}

	public List<EbDemande> demandeValorization() throws Exception
	{
		List<PricingCarrierSearchResultDTO> pricingCarrierSearchResultDTOList;
		List<EbDemande> listTrsToBeUpdated = new ArrayList<>();
		List<Integer> listEbDemandeConfirmedFromEDIToBeValorized = new ArrayList<>();
		List<PrMtcTransporteur> listMtcTransporteur = new ArrayList<PrMtcTransporteur>();
		List<EbDemande> listDemandeValorized = new ArrayList<>();

		List<EbDemande> listEbDemandeNotValorized = getNotValorizedTrs();

		if (CollectionUtils.isEmpty(listEbDemandeNotValorized))
			return null;

		listMtcTransporteur = daoPrMtcTransporteur.getListPrMtcTransporteur(new SearchCriteria());

		for (EbDemande transportRequest : listEbDemandeNotValorized)
		{
			intializeParamsBeforeValorizeTrForNotPendingTr(transportRequest);
			List<ExEbDemandeTransporteur> exEbDemandeTransporteurList = new ArrayList<>();
			transportRequest
				.setListMarchandises(
					ebMarchandiseRepository
						.findAllByEbDemande_ebDemandeNum(transportRequest.getEbDemandeNum())
				);

			// convertir les mappings en quotes pour valorisation
			exEbDemandeTransporteurList = convertMappingToQuoteForValuation(
				listMtcTransporteur,
				transportRequest
			);

			exEbDemandeTransporteurList = exEbDemandeTransporteurList
				.stream()
				.filter(quote -> !hasManualQuote(quote))
				.collect(Collectors.toList());

			for (ExEbDemandeTransporteur demandeTransporteur : exEbDemandeTransporteurList)
			{
				PrMtcTransporteur mapTrp = getMtcTransporteur(demandeTransporteur, listMtcTransporteur, transportRequest);

				if (mapTrp != null && StringUtils.isNotBlank(mapTrp.getCodeConfigurationMtc()))
				{
					try
					{
						ResultEntity resultEntity = mtcService
							.valuationFromMTC(transportRequest, mapTrp.getCodeConfigurationMtc());

						pricingCarrierSearchResultDTOList = mtcService
							.manageResult(resultEntity, transportRequest, demandeTransporteur);

						if (isFinalChoiceTobeCaluculatedAndStatusIsFIN(transportRequest))
							listEbDemandeConfirmedFromEDIToBeValorized.add(transportRequest.getEbDemandeNum());
						else
							listTrsToBeUpdated.add(transportRequest);

						if (CollectionUtils.isNotEmpty(pricingCarrierSearchResultDTOList))
						{
							setValuationResultOfTransportReuquest(
								transportRequest,
								demandeTransporteur,
								pricingCarrierSearchResultDTOList.get(0)
							);
							addToValorizedTrs(transportRequest, listDemandeValorized);
						}
					}
					catch (Exception e)
					{
						logger
							.error(
								"Error in the process of MTC valuation of tr with id = " +
									transportRequest.getEbDemandeNum(),
								e
							);
					}
				}
			}
		}
		updateIsValorizedAndStatusInTrs(listTrsToBeUpdated);
		updateIsValorizedInTrsConfirmedFromEDIToBeValorized(listEbDemandeConfirmedFromEDIToBeValorized);
		return listDemandeValorized;
	}

	private boolean hasManualQuote(ExEbDemandeTransporteur demandeTransporteur)
	{
		return demandeTransporteur.getTransitTime() != null &&
			demandeTransporteur.getPrice() != null &&
			!Enumeration.StatutCarrier.TRANSPORT_PLAN.getCode().equals(demandeTransporteur.getStatus());
	}

	private boolean isFinalChoiceTobeCaluculatedAndStatusIsFIN(EbDemande transportRequest) {
		return transportRequest.getxEcStatut() >= Enumeration.StatutDemande.FIN.getCode()
				&& transportRequest.getFinalChoiceToBeCalculated() == true;
	}

private void addToValorizedTrs(EbDemande transportRequest, List<EbDemande> listDemandeValorized) {
	if (!listDemandeValorized.isEmpty()) {
		EbDemande found = listDemandeValorized.stream()
				.filter(item -> item.getRefTransport().equals(transportRequest.getRefTransport())).findFirst()
				.orElse(null);

		if (found == null) {
			listDemandeValorized.add(transportRequest);
		}
	} else {
		listDemandeValorized.add(transportRequest);
	}
}

private void updateIsValorizedInTrsConfirmedFromEDIToBeValorized(
		List<Integer> listEbDemandeConfirmedFromEDIToBeValorized) {
	if (listEbDemandeConfirmedFromEDIToBeValorized.size() > 0) {
		Integer demandeStatus = Enumeration.StatutDemande.WPU.getCode();
		ebDemandeRepository.updateIsValorizedInEbDemandeByListEbDemandeNum(listEbDemandeConfirmedFromEDIToBeValorized,
				true, demandeStatus);
	}
}

private void updateIsValorizedAndStatusInTrs(List<EbDemande> listTrsToBeUpdated) {
	if (listTrsToBeUpdated.size() > 0) {
		List<Integer> listNormalTrsNum = listTrsToBeUpdated.stream().map(tr -> {
			return !checkGroupedTransportRequestStatusAndFields(tr) ? tr.getEbDemandeNum() : null;
		}).collect(Collectors.toList());

		List<Integer> listNumTrsGrouped = listTrsToBeUpdated.stream().map(tr -> {
			return checkGroupedTransportRequestStatusAndFields(tr) ? tr.getEbDemandeNum() : null;
		}).collect(Collectors.toList());

		ebDemandeRepository.updateIsValorizedInEbDemandeByListEbDemandeNum(listNormalTrsNum, true,
				Enumeration.StatutDemande.REC.getCode());

		// mettre à jour le statut de
		// groupage et tous les champs
		// required sont renseignés
		ebDemandeRepository.updateIsValorizedInGroupedTrByListTrNum(listNumTrsGrouped, true,
				ConsolidationGroupage.PRS.getCode());
	}
}

private PrMtcTransporteur getMtcTransporteur(
    ExEbDemandeTransporteur demandeTransporteur,
    List<PrMtcTransporteur> listMtcTransporteur,
    EbDemande transportRequest) {

    if (transportRequest.getCodeConfigurationEDI() != null) {
        return listMtcTransporteur
            .stream().filter(pr -> isQuoteTransporteurMappedToMTC(transportRequest, pr, demandeTransporteur))
            .findFirst().orElse(null);
    }

    if (demandeTransporteur.getxEbCompagnie() != null && demandeTransporteur.getxEbCompagnie().getCode() != null) {
        return listMtcTransporteur
            .stream()
            .filter(
                pr -> pr
                    .getxEbCompagnieTransporteur().getEbCompagnieNum()
                    .equals(demandeTransporteur.getxEbCompagnie().getEbCompagnieNum()))
            .findFirst().orElse(null);
    }

    return null;
}

private boolean
    isQuoteTransporteurMappedToMTC(EbDemande ebDemande, PrMtcTransporteur pr, ExEbDemandeTransporteur quote) {
    return ebDemande.getCodeConfigurationEDI().equalsIgnoreCase(pr.getCodeConfigurationEDI()) &&
        quote.getxTransporteur().getEmail().equals(pr.getxEbUser().getEmail());
}

	private void setValuationResultOfTransportReuquest(EbDemande transportRequest,
			ExEbDemandeTransporteur demandeTransporteur,
			PricingCarrierSearchResultDTO pricingCarrierSearchResultDTO) {

		demandeTransporteur.setStatus(Enumeration.StatutCarrier.TRANSPORT_PLAN.getCode());
		demandeTransporteur.setPrice(pricingCarrierSearchResultDTO.getCalculatedPrice());
		demandeTransporteur.setIsFromTransPlan(true);
		demandeTransporteur.setListPlCostItem(pricingCarrierSearchResultDTO.getListPlCostItem());
		transportRequest.setValorized(true);
		transportRequest.setDateMaj(new Date());
		exEbDemandeTransporteurRepository.save(demandeTransporteur);
	}

	private boolean checkGroupedTransportRequestStatusAndFields(EbDemande transportRequest) {
		return transportRequest.getxEcStatutGroupage() != null
				&& ConsolidationGroupage.NP.getCode().equals(transportRequest.getxEcStatutGroupage())
				&& checkRequiredChamps(transportRequest)
				&& transportRequest.getxEcNature().equals(Enumeration.NatureDemandeTransport.CONSOLIDATION.getCode());
	}

	/*
	 * Pour les cas des TransportRequests (TRs) non groupés, le statut est changé
	 * vers "In Process Waiting For Quote" (INP) et la valorisation est annulé
	 */
	private void intializeParamsBeforeValorizeTrForNotPendingTr(EbDemande transportRequest) {
		if (!StatutDemande.PND.getCode().equals(transportRequest.getxEcStatut())) {
			transportRequest.setxEcStatut(StatutDemande.INP.getCode());
			transportRequest.setValorized(false);
		}
	}

	boolean checkRequiredChamps(EbDemande demande) {

		boolean userExist = demande.getUser() != null && demande.getUser().getEbUserNum() != null;
		boolean partyOriginexist = demande.getEbPartyOrigin() != null
				&& demande.getEbPartyOrigin().getxEcCountry() != null;
		boolean partyDestExist = demande.getEbPartyDest() != null && demande.getEbPartyDest().getxEcCountry() != null;
		boolean transportModeExist = demande.getxEcModeTransport() != null;
		boolean schemaPslExist = demande.getxEbSchemaPsl() != null
				&& demande.getxEbSchemaPsl().getEbTtSchemaPslNum() != null;
		boolean dateOfGoodsAvailabilityExist = demande.getDateOfGoodsAvailability() != null;
		boolean typeRequestExist = demande.getxEbTypeRequest() != null;
		boolean currencyInvoiceExist = demande.getXecCurrencyInvoice() != null
				&& demande.getXecCurrencyInvoice().getEcCurrencyNum() != null;
		boolean listMarchandiseExsit = demande.getListMarchandises() != null
				&& !demande.getListMarchandises().isEmpty();

		return userExist && partyOriginexist && partyDestExist && transportModeExist && schemaPslExist
				&& dateOfGoodsAvailabilityExist && typeRequestExist && currencyInvoiceExist && listMarchandiseExsit;
	}

    private List<ExEbDemandeTransporteur>
        convertMappingToQuoteForValuation(List<PrMtcTransporteur> listMtcTransporteur, EbDemande ebDemande) {
        List<ExEbDemandeTransporteur> exEbDemandeTransporteurList = new ArrayList<>();

        if (listMtcTransporteur != null && !listMtcTransporteur.isEmpty()) {
            exEbDemandeTransporteurList = listTransporteursThatHasQuotationRequest(
                listMtcTransporteur,
                ebDemande.getExEbDemandeTransporteurs());

            if (exEbDemandeTransporteurList.isEmpty()) for (PrMtcTransporteur pr: listMtcTransporteur) {

                if (pr.getxEbUser() != null && pr.getxEbUser().getEbUserNum() != null
                    || (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.FIN.getCode()
                        && ebDemande.getFinalChoiceToBeCalculated() == true)) {
                    ExEbDemandeTransporteur qr = new ExEbDemandeTransporteur();
                    qr.setxEbDemande(ebDemandeRepository.getOne(ebDemande.getEbDemandeNum()));

                    if (pr.getxEbUser() != null && pr.getxEbUser().getEbUserNum() != null) qr
                        .setxTransporteur(ebUserRepository.getOne(pr.getxEbUser().getEbUserNum()));
                    else qr.setxTransporteur(ebUserRepository.getOne(ebDemande.getUser().getEbUserNum()));

                    qr
                        .setxEbCompagnie(
                            ebCompagnieRepository.getOne(pr.getxEbCompagnieTransporteur().getEbCompagnieNum()));
                    qr.setxEbEtablissement(qr.getxTransporteur().getEbEtablissement());
                    if (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.FIN.getCode()
                        && ebDemande.getFinalChoiceToBeCalculated() == true) qr
                            .setStatus(Enumeration.StatutCarrier.FIN_PLAN.getCode());
                    else qr.setStatus(Enumeration.StatutCarrier.RRE.getCode());

                    exEbDemandeTransporteurList.add(qr);
                }

            }
        }

        return exEbDemandeTransporteurList;
    }

		private List<ExEbDemandeTransporteur> listTransporteursThatHasQuotationRequest(
			List<PrMtcTransporteur> listMtcTransporteur,
			List<ExEbDemandeTransporteur> tr_Transporteurs
		)
		{
			List<ExEbDemandeTransporteur> existQuote = new ArrayList<>();

			for (ExEbDemandeTransporteur tr : tr_Transporteurs)
			{

				for (PrMtcTransporteur mtcTr : listMtcTransporteur)
				{

					if (
						tr.getxEbCompagnie() != null &&
							mtcTr
								.getxEbCompagnieTransporteur()
								.getEbCompagnieNum()
								.equals(tr.getxEbCompagnie().getEbCompagnieNum())
					)
					{
						existQuote.add(tr);
					}
				}
			}

			return existQuote
				.stream()
				.filter(distinctByKey(p -> p.getxTransporteur().getEbUserNum()))
				.collect(Collectors.toList());
		}

		private static <
			T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor)
		{
			Map<Object, Boolean> seen = new ConcurrentHashMap<>();
			return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
		}

		public void checkForTrValorization(
			EbDemande transportRequest,
			List<PrMtcTransporteur> listMtcTransporteur
		)
			throws JsonProcessingException
		{
			intializeParamsBeforeValorizeTrForNotPendingTr(transportRequest);

			transportRequest
				.setListMarchandises(
					ebMarchandiseRepository
						.findAllByEbDemande_ebDemandeNum(transportRequest.getEbDemandeNum())
				);

			// convertir les mappings en quotes pour valorisation
			List<ExEbDemandeTransporteur> listQuote = convertMappingToQuoteForValuation(
				listMtcTransporteur,
				transportRequest
			);

			for (ExEbDemandeTransporteur quote : listQuote)
			{
				PrMtcTransporteur mapTrp = getMtcTransporteur(quote, listMtcTransporteur, transportRequest);

				if (mapTrp != null && StringUtils.isNotBlank(mapTrp.getCodeConfigurationMtc()))
				{
					ResultEntity resultEntity = mtcService
						.valuationFromMTC(transportRequest, mapTrp.getCodeConfigurationMtc());

					List<PricingCarrierSearchResultDTO> pricingCarrierSearchResultDTOList = mtcService
						.manageResult(resultEntity, transportRequest, quote);

					if (CollectionUtils.isNotEmpty(pricingCarrierSearchResultDTOList))
					{
						setValuationResultOfTransportReuquest(
							transportRequest,
							quote,
							pricingCarrierSearchResultDTOList.get(0)
						);

						ebDemandeRepository
							.updateIsValorizedInGroupedTrByListTrNum(
								Arrays.asList(transportRequest.getEbDemandeNum()),
								true,
								ConsolidationGroupage.PRS.getCode()
							);
					}
				}
			}
		}

		public void groupedTrsValuation()
		{
			List<EbDemande> notValorizedGroupedTrs = ebDemandeRepository
				.findByNatureAndStatutAndIsNotValorisedAndNotCanceled(
					Enumeration.NatureDemandeTransport.CONSOLIDATION.getCode(),
					Enumeration.StatutDemande.PND.getCode()
				);

			List<PrMtcTransporteur> listMtcTransporteur = daoPrMtcTransporteur
				.getListPrMtcTransporteur(new SearchCriteria());

			notValorizedGroupedTrs.parallelStream().forEach(transportRequest -> {
				try
				{
					checkForTrValorization(transportRequest, listMtcTransporteur);
				}
				catch (JsonProcessingException e)
				{
					logger.error("Error while valorizing grouped trs" + e);
				}
			});
		}
}
