package com.adias.mytowereasy.mtc.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.adias.mytowereasy.mtc.service.ValorizationService;


@Component
public class CronjobValorisationTrInitiale {
    @Autowired
    private ValorizationService valorizationService;

		//    @Scheduled(cron = "0 0/5 * * * ?") // A Cron expression consists of six
                                       // sequential fields :: second, minute,
                                       // hour, day of month, month, day(s) of
                                       // week
    public void executeJob() throws Exception {
        valorizationService.demandeValorization();
    }
}
