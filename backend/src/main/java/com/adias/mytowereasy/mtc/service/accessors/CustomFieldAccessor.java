package com.adias.mytowereasy.mtc.service.accessors;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.expression.AccessException;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.PropertyAccessor;
import org.springframework.expression.TypedValue;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.model.CustomFields;



public abstract class CustomFieldAccessor implements PropertyAccessor {
    public static final String CUSTOM_FIELD_MATCHER = "custom_.*";
    public static final String CUSTOM_FIELD_SEPERATOR = "_";

    private Logger log = LoggerFactory.getLogger(CustomFieldAccessor.class);

    protected abstract List<CustomFields> getFields(Object target) throws JsonProcessingException;

    @Override
    public boolean canRead(EvaluationContext context, Object target, String name) throws AccessException {

        if (name.matches(CUSTOM_FIELD_MATCHER)) {
            List<CustomFields> fields = null;
            try {
                fields = getFields(target);
            } catch (JsonProcessingException e) {
                log.error("Error while parsing custom fields", e);
                throw new AccessException("Error while parsing custom fields");
            }
			String fieldName = name.substring(name.indexOf(CUSTOM_FIELD_SEPERATOR) + 1);
			return fields.stream().map(CustomFields::getName).distinct().anyMatch(k -> k.equals(fieldName));
        }

        return false;
    }

    @Override
    public TypedValue read(EvaluationContext context, Object target, String name) throws AccessException {

        if (name.matches(CUSTOM_FIELD_MATCHER)) {
            List<CustomFields> fields = null;
            try {
                fields = getFields(target);
            } catch (JsonProcessingException e) {
                log.error("Error while parsing custom fields", e);
                throw new AccessException("Error while parsing custom fields");
            }
			String fieldName = name.substring(name.indexOf(CUSTOM_FIELD_SEPERATOR) + 1);

            for (CustomFields field: fields) {

                if (field.getName().equals(fieldName)) {
                    return new TypedValue(field.getValue());
                }

            }

        }

        return null;
    }

    @Override
    public boolean canWrite(EvaluationContext context, Object target, String name) throws AccessException {
        return false;
    }

    @Override
    public void write(EvaluationContext context, Object target, String name, Object newValue) throws AccessException {
        throw new UnsupportedOperationException();
    }
}
