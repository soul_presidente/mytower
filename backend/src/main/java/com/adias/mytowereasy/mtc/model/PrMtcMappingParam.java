package com.adias.mytowereasy.mtc.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

import com.adias.moteur.chiffrage.library.shared.Enumeration;



@Entity
public class PrMtcMappingParam {
    @Id
    private Long prMtcMappingParamNum;
    private String configCode;
    private String mtcAttribute;
    private String mtgAttribute;
    @Enumerated(EnumType.STRING)
    private Enumeration.TypeData type;

    public PrMtcMappingParam() {
    }

    public PrMtcMappingParam(Long prMtcMappingParamNum,
                             String configCode,
                             String mtcAttribute,
                             String mtgAttribute,
                             Enumeration.TypeData type) {
        this.prMtcMappingParamNum = prMtcMappingParamNum;
        this.configCode = configCode;
        this.mtcAttribute = mtcAttribute;
        this.mtgAttribute = mtgAttribute;
        this.type = type;
    }

    public Long getPrMtcMappingParamNum() {
        return prMtcMappingParamNum;
    }

    public void setPrMtcMappingParamNum(Long prMtcMappingParamNum) {
        this.prMtcMappingParamNum = prMtcMappingParamNum;
    }

    public String getConfigCode() {
        return configCode;
    }

    public void setConfigCode(String configCode) {
        this.configCode = configCode;
    }

    public String getMtcAttribute() {
        return mtcAttribute;
    }

    public void setMtcAttribute(String mtcAttribute) {
        this.mtcAttribute = mtcAttribute;
    }

    public String getMtgAttribute() {
        return mtgAttribute;
    }

    public void setMtgAttribute(String mtgAttribute) {
        this.mtgAttribute = mtgAttribute;
    }

    public Enumeration.TypeData getType() {
        return type;
    }

    public void setType(Enumeration.TypeData type) {
        this.type = type;
    }
}
