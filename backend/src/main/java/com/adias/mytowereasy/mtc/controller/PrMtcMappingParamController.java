package com.adias.mytowereasy.mtc.controller;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.mtc.model.PrMtcMappingParam;
import com.adias.mytowereasy.mtc.service.PrMtcMappingParamService;

@RestController
@RequestMapping("api/v1/mtc/mapping-params")
public class PrMtcMappingParamController {
    @Autowired
    private PrMtcMappingParamService mappingParamService;


    @GetMapping("by-code/{configCode}")
    public ResponseEntity<List<PrMtcMappingParam>> getMappingParamsByConfigCode(@PathVariable String configCode) throws JsonProcessingException {
        List<PrMtcMappingParam> params = mappingParamService.getParamsByConfigCode(configCode);
        return ResponseEntity.ok(params.isEmpty() ?
                mappingParamService.saveAll(mappingParamService.getNewParams(configCode)) :
                params);
    }

    @PostMapping
    public ResponseEntity<List<PrMtcMappingParam>> saveAll(@RequestBody List<PrMtcMappingParam> params) {
        return ResponseEntity.ok(mappingParamService.saveAll(params));
    }
}
