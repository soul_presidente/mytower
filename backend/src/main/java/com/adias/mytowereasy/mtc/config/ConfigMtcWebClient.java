package com.adias.mytowereasy.mtc.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClient;

import com.adias.mytowereasy.mtc.utils.TokenProviderMtc;

import reactor.core.publisher.Mono;

@Configuration
public class ConfigMtcWebClient {
    @Value("${mtc.base_url}")
    private String mtcUrl;

    @Bean
    public WebClient retryWebClient(WebClient.Builder builder, TokenProviderMtc tokenProvider) {
        return builder
            .baseUrl(mtcUrl)
            // Set the header before the exchange
            .filter(
                ((request, next) -> tokenProvider
                    .getAccessToken(request).map(tokenProvider.setBearerTokenInHeader(request))
                    .flatMap(next::exchange)))
            // Do the exchange
            .filter((request, next) -> next.exchange(request).flatMap(clientResponse -> {

                if (clientResponse.statusCode().value() == HttpStatus.FORBIDDEN.value() ||
                    clientResponse.statusCode().value() == HttpStatus.UNAUTHORIZED.value()) {
                    // Refresh Token In Case The Token Expired Or
                    // Removed
                    return tokenProvider
                        .getAccessTokenWithWebClient().map(tokenProvider.setBearerTokenInHeader(request))
                        .flatMap(next::exchange);
                }
                else {
                    return Mono.just(clientResponse);
                }

            })).build();
    }
}