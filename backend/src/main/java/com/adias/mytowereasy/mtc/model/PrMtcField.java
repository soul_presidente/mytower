/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.mtc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
public class PrMtcField {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer prMtcFieldNum;

    private Integer code;

    @Column(columnDefinition = "TEXT")
    private String mtcAttribute;

    @Column(columnDefinition = "TEXT")
    private String mtgAttribute;

    public Integer getPrMtcFieldNum() {
        return prMtcFieldNum;
    }

    public Integer getCode() {
        return code;
    }

    public String getMtcAttribute() {
        return mtcAttribute;
    }

    public String getMtgAttribute() {
        return mtgAttribute;
    }

    public void setPrMtcFieldNum(Integer prMtcFieldNum) {
        this.prMtcFieldNum = prMtcFieldNum;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setMtcAttribute(String mtcAttribute) {
        this.mtcAttribute = mtcAttribute;
    }

    public void setMtgAttribute(String mtgAttribute) {
        this.mtgAttribute = mtgAttribute;
    }
}
