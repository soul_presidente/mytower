package com.adias.mytowereasy.mtc.model;

public class UnitInfo {
    private Integer id;
    private Object value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
