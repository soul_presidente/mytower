package com.adias.mytowereasy.mtc;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adias.mytowereasy.model.EbDemande;
import com.adias.mytowereasy.mtc.service.ValorizationService;


@RestController
@RequestMapping("/valorization/")
public class ValorizationController {
    @Autowired
    ValorizationService valorizationService;

		@GetMapping("tr-initiaux")
    public List<EbDemande> valorization() throws Exception {
        return valorizationService.demandeValorization();
    }

		@GetMapping("grouped-trs")
		public void groupedTrsValuation() throws Exception
		{
			valorizationService.groupedTrsValuation();
		}
}
