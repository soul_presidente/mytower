package com.adias.mytowereasy.mtc.service.accessors;

import java.util.List;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;

import com.adias.mytowereasy.model.CustomFields;
import com.adias.mytowereasy.model.EbMarchandise;


@Component
public class MarchandiseCustomFieldAccessor extends CustomFieldAccessor {
    @Override
    protected List<CustomFields> getFields(Object target) throws JsonProcessingException {
        final EbMarchandise marchandise = (EbMarchandise) target;
        return marchandise.getListCustomsFields();
    }

    @Override
    public Class<?>[] getSpecificTargetClasses() {
        return new Class[]{
            EbMarchandise.class
        };
    }
}
