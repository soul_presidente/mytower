package com.adias.mytowereasy.mtc.repository;

import com.adias.mytowereasy.mtc.model.PrMtcField;
import com.adias.mytowereasy.repository.CommonRepository;


public interface PrMtcFieldRepository extends CommonRepository<PrMtcField, Integer> {

}
