package com.adias.mytowereasy.mtc.repository;

import com.adias.mytowereasy.mtc.model.PrMtcMappingParam;
import com.adias.mytowereasy.repository.CommonRepository;

import java.util.List;

public interface PrMtcMappingParamRepository extends CommonRepository<PrMtcMappingParam, Long> {
    List<PrMtcMappingParam> findAllByConfigCodeOrderByPrMtcMappingParamNum(String configCode);
}
