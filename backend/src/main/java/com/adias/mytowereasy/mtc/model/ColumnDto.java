package com.adias.mytowereasy.mtc.model;

public class ColumnDto {
    String libelle;
    Object value;

    // getter && setter
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}
