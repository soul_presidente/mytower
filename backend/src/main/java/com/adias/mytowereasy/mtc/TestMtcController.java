package com.adias.mytowereasy.mtc;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import com.adias.moteur.chiffrage.library.shared.ResultEntity;
import com.adias.mytowereasy.dao.DaoPrMtcTransporteur;
import com.adias.mytowereasy.dto.PricingCarrierSearchResultDTO;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.mtc.service.MtcService;
import com.adias.mytowereasy.mtc.utils.AccessMtc;
import com.adias.mytowereasy.repository.*;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.PricingService;
import com.adias.mytowereasy.util.GenericObjectUtils;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping(value = "/test-mtc/api")
public class TestMtcController {
    @Autowired
    MtcService mtcService;
    @Autowired
    EbDemandeRepository ebDemandeRepository;
    @Autowired
    EbMarchandiseRepository ebMarchandiseRepository;
    @Autowired
    PricingService pricingService;
    @Autowired
    private AccessMtc accessMtc;
    @Autowired
    ConnectedUserService connectedUserService;
    @Autowired
    EbCompagnieRepository ebCompagnieRepository;
    @Autowired
    DaoPrMtcTransporteur daoPrMtcTransporteur;

    @Autowired
    EbUserRepository ebUserRepository;

    @Autowired
    ExEbDemandeTransporteurRepository exEbDemandeTransporteurRepository;

    @RequestMapping("/valorisation/{numDemande}/{shipperCompNum}/{codeConfigMtc}")
    public String calculBlocking(
        @PathVariable Integer numDemande,
        @PathVariable Integer shipperCompNum,
        @PathVariable String codeConfigMtc)
        throws JsonProcessingException {

        // SearchCriteria criteria = new SearchCriteria();
        // criteria.setEbCompagnieNum(carrierCompNum);
        if (StringUtils.isNotBlank(codeConfigMtc)) {
            EbDemande demande = ebDemandeRepository.findOneByEbDemandeNum(numDemande);
            EbCompagnie ebCompagnie = ebCompagnieRepository.findById(shipperCompNum).orElse(null);

            if (demande != null && ebCompagnie != null) {
                Gson gson = new Gson();

                if (demande.getCustomFields() != null) {
                    demande
                        .setListCustomsFields(
                            gson.fromJson(demande.getCustomFields(), new TypeToken<ArrayList<CustomFields>>() {
                            }.getType()));
                }

                List<Integer> demNums = new ArrayList<Integer>();
                demNums.add(demande.getEbDemandeNum());
                demande.setListMarchandises(ebMarchandiseRepository.selectListEbMarchandiseByEbDemandeNumIn(demNums));

                ResultEntity response = mtcService.valuationFromMTC(demande, codeConfigMtc);
                return new Gson().toJson(response);
            }
            else {
                return "Num demande or num carrier not found";
            }

        }
        else {
            return "Aucune config trouvé ";
        }

    }

    @RequestMapping("/getStructure/{numConfig}")
    public String calculNonBlocking(@PathVariable String numConfig) throws JsonProcessingException {
        ResultEntity response = this.mtcService.getStructureByConfig(numConfig).block();
        return new Gson().toJson(response);
    }

    @RequestMapping("/value")
    public Object getValueByPath() throws Exception {
        EbDemande d = new EbDemande();
        d.setListCategories(new ArrayList<>());
        d.getListCategories().add(new EbCategorie());
        d.getListCategories().get(0).setLibelle("Champ complément 4");
        d.getListCategories().get(0).setLabels(new HashSet<>());
        d.getListCategories().add(new EbCategorie());
        d.getListCategories().get(1).setLibelle("Champ complément 5");
        d.getListCategories().get(1).setLabels(new HashSet<>());
        d.getListCategories().add(new EbCategorie());
        d.getListCategories().get(2).setLibelle("Champ complément 7");
        d.getListCategories().get(2).setLabels(new HashSet<>());
        EbLabel label = new EbLabel();
        label.setLibelle("xxxxx");
        d.getListCategories().get(0).getLabels().add(label);
        return GenericObjectUtils
            .getValueByObjectPath(
                EbDemande.class,
                d,
                "ListCategories:find-libelle='Champ complément 4':labels:first-set:libelle");
    }

    @RequestMapping("/params")
    public String getParamsMTC() {
        return accessMtc.getMtcUrl() + "\n" + accessMtc.getApiUrl() + "" + accessMtc.getAuthUrl() + " "
            + accessMtc.getUsername() + " " + accessMtc.getPassword();
    }

    // Permet de valoriser une seule tr en connaissant son l'id de la demande
    @GetMapping("/valorization/{idDemande}")
    public List<EbDemande> demandeValorization(@PathVariable("idDemande") Integer idDemande) throws Exception {
        EbDemande d = ebDemandeRepository.findOneByEbDemandeNum(idDemande);

        List<EbDemande> listEbDemandeNotValorized = new ArrayList<>();

        listEbDemandeNotValorized.add(d);

        List<PricingCarrierSearchResultDTO> pricingCarrierSearchResultDTOList;
        List<Integer> listEbDemadeNum = new ArrayList<>();
        List<Integer> listEbDemandeConfirmedFromEDIToBeValorized = new ArrayList<>();
        List<PrMtcTransporteur> listMtcTransporteur = new ArrayList<PrMtcTransporteur>();
        List<EbDemande> listDemandeValorized = new ArrayList<>();

        if (listEbDemandeNotValorized != null && !listEbDemandeNotValorized.isEmpty()) {
            listMtcTransporteur = daoPrMtcTransporteur.getListPrMtcTransporteur(new SearchCriteria());
        }

        for (EbDemande ebDemande: listEbDemandeNotValorized) {
            List<ExEbDemandeTransporteur> exEbDemandeTransporteurList = new ArrayList<>();
            ebDemande
                .setListMarchandises(
                    ebMarchandiseRepository.findAllByEbDemande_ebDemandeNum(ebDemande.getEbDemandeNum()));

            // convertir les mappings en quotes pour valorisation
            if (listMtcTransporteur != null && !listMtcTransporteur.isEmpty()) {

                for (PrMtcTransporteur pr: listMtcTransporteur) {
                    ExEbDemandeTransporteur existQuote = null;

                    for (ExEbDemandeTransporteur tr: ebDemande.getExEbDemandeTransporteurs()) {

                        if (tr.getxEbCompagnie() != null && pr
                            .getxEbCompagnieTransporteur().getEbCompagnieNum()
                            .equals(tr.getxEbCompagnie().getEbCompagnieNum())) {
                            existQuote = tr;
                            break;
                        }

                    }

                    if (existQuote == null && pr.getxEbUser() != null && pr.getxEbUser().getEbUserNum() != null
                        || (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.FIN.getCode()
                            && ebDemande.getFinalChoiceToBeCalculated() == true)) {
                        ExEbDemandeTransporteur qr = new ExEbDemandeTransporteur();
                        qr.setxEbDemande(ebDemandeRepository.getOne(ebDemande.getEbDemandeNum()));

                        if (pr.getxEbUser() != null && pr.getxEbUser().getEbUserNum() != null) qr
                            .setxTransporteur(ebUserRepository.getOne(pr.getxEbUser().getEbUserNum()));
                        else qr.setxTransporteur(ebUserRepository.getOne(ebDemande.getUser().getEbUserNum()));

                        qr
                            .setxEbCompagnie(
                                ebCompagnieRepository.getOne(pr.getxEbCompagnieTransporteur().getEbCompagnieNum()));
                        qr.setxEbEtablissement(qr.getxTransporteur().getEbEtablissement());
                        if (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.FIN.getCode()
                            && ebDemande.getFinalChoiceToBeCalculated() == true) qr
                                .setStatus(Enumeration.StatutCarrier.FIN_PLAN.getCode());
                        else qr.setStatus(Enumeration.StatutCarrier.RRE.getCode());

                        exEbDemandeTransporteurList.add(qr);
                    }
                    else if (existQuote != null) {
                        exEbDemandeTransporteurList.add(existQuote);
                    }

                }

            }

            for (ExEbDemandeTransporteur exEbDemandeTransporteur: exEbDemandeTransporteurList) {

                if (exEbDemandeTransporteur.getxEbCompagnie() != null
                    && exEbDemandeTransporteur.getxEbCompagnie().getCode() != null) {
                    PrMtcTransporteur mapTrp = listMtcTransporteur
                        .stream()
                        .filter(
                            pr -> pr
                                .getxEbCompagnieTransporteur().getEbCompagnieNum()
                                .equals(exEbDemandeTransporteur.getxEbCompagnie().getEbCompagnieNum()))
                        .findFirst().orElse(null);

                    if (mapTrp != null && StringUtils.isNotBlank(mapTrp.getCodeConfigurationMtc())) {
                        ResultEntity resultEntity = mtcService
                            .valuationFromMTC(
                                ebDemande,
                                mapTrp.getCodeConfigurationMtc());

                        pricingCarrierSearchResultDTOList = mtcService
                            .manageResult(resultEntity, ebDemande, exEbDemandeTransporteur);

                            if (ebDemande.getxEcStatut() >= Enumeration.StatutDemande.FIN.getCode() && ebDemande
                                .getFinalChoiceToBeCalculated() == true) listEbDemandeConfirmedFromEDIToBeValorized
                                    .add(ebDemande.getEbDemandeNum());
                            else {
                                listEbDemadeNum.add(ebDemande.getEbDemandeNum());
                                exEbDemandeTransporteur.setStatus(Enumeration.StatutCarrier.TRANSPORT_PLAN.getCode());
                            }

                            exEbDemandeTransporteur
                                .setPrice(pricingCarrierSearchResultDTOList.get(0).getCalculatedPrice());
                            exEbDemandeTransporteur.setIsFromTransPlan(true);
                            exEbDemandeTransporteur
                                .setListPlCostItem(pricingCarrierSearchResultDTOList.get(0).getListPlCostItem());

                            exEbDemandeTransporteurRepository.save(exEbDemandeTransporteur);

                            if (!listDemandeValorized.isEmpty()) {
                                EbDemande found = listDemandeValorized
                                    .stream().filter(item -> item.getRefTransport().equals(ebDemande.getRefTransport()))
                                    .findFirst().orElse(null);

                                if (found == null) {
                                    listDemandeValorized.add(ebDemande);
                                }

                            }
                            else {
                                listDemandeValorized.add(ebDemande);
                            }
                    }

                }

            }

        }

        if (listEbDemadeNum.size() > 0) {
            Integer demandeStatus = Enumeration.StatutDemande.REC.getCode();
            ebDemandeRepository.updateIsValorizedInEbDemandeByListEbDemandeNum(listEbDemadeNum, true, demandeStatus);
        }

        if (listEbDemandeConfirmedFromEDIToBeValorized.size() > 0) {
            Integer demandeStatus = Enumeration.StatutDemande.WPU.getCode();
            ebDemandeRepository
                .updateIsValorizedInEbDemandeByListEbDemandeNum(
                    listEbDemandeConfirmedFromEDIToBeValorized,
                    true,
                    demandeStatus);
        }

        return listDemandeValorized;
    }
}
