package com.adias.mytowereasy.mtc.service;

import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.mtc.model.PrMtcMappingParam;
import com.adias.mytowereasy.mtc.repository.PrMtcMappingParamRepository;

@Service
public class PrMtcMappingParamService {
    @Autowired
    private MtcService mtcService;
    @Autowired
    private PrMtcMappingParamRepository mappingParamRepository;

    public List<PrMtcMappingParam> getNewParams(String configCode) throws JsonProcessingException {
        return mtcService.loadMtcColumns(configCode).getDataColumns().stream()
                .map(column -> new PrMtcMappingParam(
                        column.getId().longValue(),
                                configCode,
                                column.getLibelle(),
                                null,
                                column.getType())
                )
                .collect(Collectors.toList());
    }

    public List<PrMtcMappingParam> getParamsByConfigCode(String configCode) {
        return mappingParamRepository.findAllByConfigCodeOrderByPrMtcMappingParamNum(configCode);
    }

    public List<PrMtcMappingParam> saveAll(List<PrMtcMappingParam> params) {
        return mappingParamRepository.saveAll(params);
    }
}
