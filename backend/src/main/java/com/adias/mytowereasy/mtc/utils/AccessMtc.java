package com.adias.mytowereasy.mtc.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;


@Service
public class AccessMtc {
    @Value("${mtc.username}")
    private String username;
    @Value("${mtc.pwd}")
    private String password;
    @Value("${mtc.base_url}")
    private String apiUrl;
    @Value("${mtc.url}")
    private String mtcUrl;
    @Value("${mtc.url.auth}")
    private String authUrl;

    public String getFullUrl() {
        return apiUrl + mtcUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getAuthUrl() {
        return authUrl;
    }

    public void setAuthUrl(String authUrl) {
        this.authUrl = authUrl;
    }

    public String getMtcUrl() {
        return mtcUrl;
    }

    public void setMtcUrl(String mtcUrl) {
        this.mtcUrl = mtcUrl;
    }
}
