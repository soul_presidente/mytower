package com.adias.mytowereasy.mtc.model;

import java.util.ArrayList;
import java.util.List;

import com.adias.moteur.chiffrage.library.calcul.Column;
import com.adias.moteur.chiffrage.library.shared.Enumeration.TypeData;


public class PrMtcFieldDTO {
    private Integer idColumn;

    private String mtcAttribute;

    private String mtgAttribute;

    private String type;

    public String getMtcAttribute() {
        return mtcAttribute;
    }

    public void setMtcAttribute(String mtcAttribute) {
        this.mtcAttribute = mtcAttribute;
    }

    public String getMtgAttribute() {
        return mtgAttribute;
    }

    public void setMtgAttribute(String mtgAttribute) {
        this.mtgAttribute = mtgAttribute;
    }

    public static List<Column> toColumns(List<PrMtcFieldDTO> fields) {
        List<Column> columns = new ArrayList<>();

        for (PrMtcFieldDTO field: fields) {
            Column col = new Column();
            col.setLibelle(field.getMtcAttribute());
            col.setId(field.getIdColumn());

            for (TypeData val: TypeData.values()) {

                if (val.getLibelle().equalsIgnoreCase(field.getType())) {
                    col.setType(val);
                    break;
                }

            }

            columns.add(col);
        }

        return columns;
    }

    public static List<PrMtcFieldDTO> fromColumns(List<Column> cols) {
        List<PrMtcFieldDTO> fields = new ArrayList<>();

        for (Column col: cols) {
            PrMtcFieldDTO field = new PrMtcFieldDTO();
            field.setMtcAttribute(col.getLibelle());
            field.setType(col.getType().getLibelle());
            field.setIdColumn(col.getId());

            fields.add(field);
        }

        return fields;
    }

    public Integer getIdColumn() {
        return idColumn;
    }

    public void setIdColumn(Integer idColumn) {
        this.idColumn = idColumn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
