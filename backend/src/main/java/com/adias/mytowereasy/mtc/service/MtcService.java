package com.adias.mytowereasy.mtc.service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.expression.PropertyAccessor;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import com.adias.moteur.chiffrage.library.calcul.Column;
import com.adias.moteur.chiffrage.library.calcul.DataRow;
import com.adias.moteur.chiffrage.library.calcul.PostCout;
import com.adias.moteur.chiffrage.library.shared.ResultEntity;
import com.adias.moteur.chiffrage.library.shared.SearchCriteria;
import com.adias.moteur.chiffrage.library.shared.Statiques;
import com.adias.mytowereasy.dao.DaoPrMtcTransporteur;
import com.adias.mytowereasy.dto.PricingCarrierSearchResultDTO;
import com.adias.mytowereasy.exception.MyTowerException;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.model.Enumeration.MarchandiseDangerousGood;
import com.adias.mytowereasy.model.Enumeration.YesOrNo;
import com.adias.mytowereasy.mtc.model.*;
import com.adias.mytowereasy.mtc.repository.PrMtcFieldRepository;
import com.adias.mytowereasy.mtc.service.accessors.DemandeCustomFieldAccessor;
import com.adias.mytowereasy.mtc.service.accessors.MarchandiseCustomFieldAccessor;
import com.adias.mytowereasy.mtc.utils.AccessMtc;
import com.adias.mytowereasy.repository.EbCompagnieCurrencyRepository;
import com.adias.mytowereasy.repository.EbCompagnieRepository;
import com.adias.mytowereasy.repository.ExEbDemandeTransporteurRepository;
import com.adias.mytowereasy.repository.PrMtcTransporteurRepository;
import com.adias.mytowereasy.service.CurrencyService;
import com.adias.mytowereasy.trpl.model.EbPlCostItem;
import com.adias.mytowereasy.util.GenericObjectUtils;
import com.adias.mytowereasy.util.JsonUtils;
import com.adias.mytowereasy.util.ObjectDeserializer;

import reactor.core.publisher.Mono;


@Service
public class MtcService {
    public static final String SUCCESS_LABEL = "SUCCESS";
    private final List<PropertyAccessor> propertyAccessorsRegistry = Lists
        .newArrayList(new DemandeCustomFieldAccessor(), new MarchandiseCustomFieldAccessor());
    @Autowired
    protected PrMtcFieldRepository prMtcFieldRepository;

    @Autowired
    protected EbCompagnieRepository ebCompagnieRepository;

    @Autowired
    protected DaoPrMtcTransporteur daoPrMtcTransporteur;

    @Autowired
    WebClient webClient;

    @Autowired
    private AccessMtc accessMtc;
    @Autowired
    ExEbDemandeTransporteurRepository exEbDemandeTransporteurRepository;
    @Autowired
    PrMtcTransporteurRepository prMtcTransporteurRepository;
    @Autowired
    CurrencyService currencyService;
    @Autowired
    EbCompagnieCurrencyRepository ebCompagnieCurrencyRepository;

    @Autowired
    private PrMtcMappingParamService mappingParamService;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public ResultEntity valuationFromMTC(EbDemande demande, String codeConfig) throws JsonProcessingException {
        EbMarchandise unit = consolidateUnitsForMTC(demande);
        unit.setEbDemande(demande);
        return getValuation(unit, codeConfig);
    }

    public EbMarchandise consolidateUnitsForMTC(EbDemande demande) {
        List<EbMarchandise> units = new ArrayList<>(demande.getListMarchandises());
        EbMarchandise result = new EbMarchandise();
        result.setHeigth(1.0);
        result.setWidth(1.0);
        result.setLength(0.0);
        result.setWeight(0.0);
		if (units.size() > 0) {
            result.setLabelTypeUnit(units.get(0).getLabelTypeUnit());
            result.setPackaging(units.get(0).getPackaging());
        }
        result.setStackable(false);
        result.setVolume(0.0);
        result.setDangerousGood(MarchandiseDangerousGood.NO.getCode());
        result.setDg(YesOrNo.No.getCode().toString());

				if (!units.isEmpty())
				{

					for (EbMarchandise unit : units)
					{
                // Envoyer le volume dans l'attribu Longeur d'une manière
                // temporaire pour s'adapter à MTC qui s'attends à avoir des
                // dimensions
                // todo check if this is a bug
                result.setLength(unit.getVolume() + result.getLength());
                result.setWeight(unit.getWeight() + result.getWeight());
                result.setVolume(unit.getVolume() + result.getVolume());
                result.setStackable(Boolean.TRUE.equals(unit.getStackable()));
                if (isDangerousGood(unit)) {
                    result.setDg(YesOrNo.Yes.getCode().toString());
                }
            }

        }

        result.setTaxableWeight(demande.getTotalTaxableWeight());

        return result;
    }

    private boolean isDangerousGood(EbMarchandise unit) {
        return unit != null
            && (!MarchandiseDangerousGood.NO.getCode().equals(unit.getDangerousGood()));
    }

    private DataRow getValorisationColumnsByShipperAndConfigCode(Integer ebCompagnieNum, String configCode)
        throws JsonProcessingException {
        DataRow row = null;

        PrMtcTransporteur mtcInput = prMtcTransporteurRepository
            .findFirstByXEbCompagnieChargeur_ebCompagnieNumAndCodeConfigurationMtc(ebCompagnieNum, configCode);

        if (mtcInput != null) {

            if (mtcInput.getAttributes() == null || mtcInput.getAttributes().isEmpty()) {
                row = loadMtcColumns(configCode);

                if (row != null) {
                    mtcInput = prMtcTransporteurRepository.getOne(mtcInput.getPrMtcTransporteurNum());
                    mtcInput.setAttributes(PrMtcFieldDTO.fromColumns(row.getDataColumns()));
                    prMtcTransporteurRepository.save(mtcInput);
                }

            }
            else {
                List<PrMtcFieldDTO> fields = ObjectDeserializer
                    .checkJsonListObject(mtcInput.getAttributes(), PrMtcFieldDTO.class);

                row = new DataRow();
                row.setListPostCout(new ArrayList<>());
                row.setDataColumns(PrMtcFieldDTO.toColumns(fields));
            }

        }

        return row;
    }

    @SuppressWarnings("unchecked")
    public List<PricingCarrierSearchResultDTO>
        manageResult(ResultEntity resultMTC, EbDemande demande, ExEbDemandeTransporteur exEbDemandeTransporteur) {
        List<PricingCarrierSearchResultDTO> resultDto = new ArrayList<>();

        if (columnHasSuccessLabel( resultMTC)) {
            List<PostCout> listPostCout = new ArrayList<>();
            List<DataRow> rows = new ArrayList<>();
            List<EbPlCostItem> listPlCostItem = new ArrayList<>();
            ObjectMapper mapper = new ObjectMapper();
            List<DataRow> rowsAsObject = mapper.convertValue(resultMTC.getValue(), ArrayList.class);

			// fix error linkedHashMap
			if (rowsAsObject != null) {
				for (Object obj : rowsAsObject) {
					rows.add(mapper.convertValue(obj, DataRow.class));
				}
			}
            rows.forEach(r -> {

                if (r.getRowNum() != null) {
                    r.getListPostCout().forEach(pc -> {
                        PostCout found = listPostCout
                            .stream().filter(e -> pc.getIdPC().intValue() == e.getIdPC()).findFirst().orElse(null);

                        if (found != null) {
                            BigDecimal bd = BigDecimal.valueOf(found.getCost() + pc.getCost());
                            bd = bd.setScale(2, BigDecimal.ROUND_DOWN);
                            listPostCout.get(listPostCout.indexOf(found)).setCost(bd.doubleValue());
                        }
                        else {
                            listPostCout.add(pc);
                        }

                    });
                }

            });
            PricingCarrierSearchResultDTO searchResDto = new PricingCarrierSearchResultDTO();
            searchResDto.setCarrierEmail(exEbDemandeTransporteur.getEmailTransporteur());

            if (exEbDemandeTransporteur.getxEbEtablissement() != null) {
                searchResDto
                    .setCarrierEtabName(
                        exEbDemandeTransporteur.getxEbEtablissement().getCode() != null ?
                            exEbDemandeTransporteur.getxEbEtablissement().getCode() :
                            exEbDemandeTransporteur.getxEbEtablissement().getNom());
                searchResDto.setCarrierEtabNum(exEbDemandeTransporteur.getxEbEtablissement().getEbEtablissementNum());
            }

            EbCompagnie comp = exEbDemandeTransporteur.getxEbCompagnie() != null
                && exEbDemandeTransporteur.getxEbCompagnie().getEbCompagnieNum() != null ?
                    exEbDemandeTransporteur.getxEbCompagnie() :
                    exEbDemandeTransporteur.getxEbEtablissement().getEbCompagnie();

            if (comp != null) {
                searchResDto.setCarrierCompanyName(comp.getNom());
                searchResDto.setCarrierCompanyNum(comp.getEbCompagnieNum());
            }

            com.adias.mytowereasy.utils.search.SearchCriteria criteria = new com.adias.mytowereasy.utils.search.SearchCriteria();
            criteria
                .setEbCompagnieGuestNum(
                    exEbDemandeTransporteur
                        .getxEbCompagnie().getEbCompagnieNum());
            if(demande.getxEbCompagnie() == null) {
            	EbCompagnie compagnie = ebCompagnieRepository.selectCompagnieByEbUserNum(demande.getUser().getEbUserNum());
            	if(compagnie != null) {

               	 criteria
                    .setEbCompagnieNum(
                    		 compagnie.getEbCompagnieNum());
            	}
            } else {
            	criteria
                .setEbCompagnieNum(
                		 demande.getxEbCompagnie().getEbCompagnieNum());
            }
            criteria
                .setEbEtablissementNum(
                    exEbDemandeTransporteur
                        .getxEbEtablissement().getEbEtablissementNum());
            List<EbCompagnieCurrency> ebCompagnieCurrencies = ebCompagnieCurrencyRepository
                .findAllByCompany(criteria.getEbCompagnieNum(), criteria.getEbCompagnieGuestNum());
            listPostCout.stream().forEach(e -> {
                EbCompagnieCurrency compagnieCurrency = checkExchangeRateExists(e, ebCompagnieCurrencies, demande);

                Double priceConverted = e.getCost() * compagnieCurrency.getEuroExchangeRate();
                priceConverted = BigDecimal.valueOf(priceConverted).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
                e.setCost(priceConverted);

				BigDecimal bd = searchResDto.getCalculatedPrice().add(BigDecimal.valueOf(e.getCost()));
				bd = bd.setScale(2, BigDecimal.ROUND_DOWN);
				searchResDto.setCalculatedPrice(bd);
				EbPlCostItem costItem = new EbPlCostItem();
				costItem.setPrice(BigDecimal.valueOf(e.getCost()));
				costItem.setCurrency(compagnieCurrency.getEcCurrency());
				costItem.setEuroExchangeRate(compagnieCurrency.getEuroExchangeRate());
				costItem.setLibelle(e.getLibellePC());
				costItem.setHorsGrille(e.getHorsGrille());
				listPlCostItem.add(costItem);
            });

			boolean isHorsGrille = listPlCostItem.stream().anyMatch(costItem -> costItem.isHorsGrille());
			exEbDemandeTransporteur.setHorsGrille(isHorsGrille);
			searchResDto.setHorsGrille(isHorsGrille);
			searchResDto.setListPlCostItem(listPlCostItem);

			resultDto.add(searchResDto);
        }

        return resultDto;
    }

    private EbCompagnieCurrency checkExchangeRateExists(
        PostCout postCout,
        List<EbCompagnieCurrency> ebCompagnieCurrencies,
        EbDemande ebDemande) {
        final Integer devise = -1;
		final Double cost = new Double("0.0");

        EbCompagnieCurrency compagnieCurrencyDefault = new EbCompagnieCurrency();
        compagnieCurrencyDefault.setEuroExchangeRate(1.0);

        if (postCout.getCurrency() != null && !postCout.getCurrency().equals(devise)
				&& postCout.getCost() != null && !postCout.getCost().equals(cost)) {
            EcCurrency currency = currencyService.getCurrencyByCode((String) postCout.getCurrency());

            Optional<EbCompagnieCurrency> compagnieCurrency = ebCompagnieCurrencies
                .stream()
                .filter(
							item -> ebDemande.getXecCurrencyInvoice() != null && checkCurrencyValidity(item, currency,
									ebDemande.getXecCurrencyInvoice().getEcCurrencyNum()))
                .findFirst();

            return (compagnieCurrency != null && compagnieCurrency.isPresent()) ?
                compagnieCurrency.get() :
                compagnieCurrencyDefault;
        }

        return compagnieCurrencyDefault;
    }

	private boolean checkCurrencyValidity(EbCompagnieCurrency compagnieCurrency, EcCurrency currency,
			Integer demandeCurrency) {
		if (compagnieCurrency.getEcCurrency() == null || compagnieCurrency.getXecCurrencyCible() == null
				|| compagnieCurrency.getDateDebut() == null)
			throw new MyTowerException("Currency , CurrencyCible or currency beginning date must not be null");

		final boolean isCompanyCurrencySameAsCostItemCurrency = compagnieCurrency.getEcCurrency().getEcCurrencyNum()
				.equals(currency.getEcCurrencyNum());

		final boolean isCompanyCurrencySameAsDemandeCurrency = compagnieCurrency.getXecCurrencyCible()
				.getEcCurrencyNum()
				.equals(demandeCurrency);

		final Date todayDate = new Date();
		boolean isGoodRange = todayDate.after(compagnieCurrency.getDateDebut());
		if (compagnieCurrency.getDateFin() != null)
			isGoodRange = isGoodRange && todayDate.before(compagnieCurrency.getDateFin());

		return isCompanyCurrencySameAsCostItemCurrency && isCompanyCurrencySameAsDemandeCurrency && isGoodRange;
	}

    List<Column> cloneDataColumn(List<Column> listOldClmn) {
        List<Column> listNewClmn = new ArrayList<>();
        listOldClmn.forEach(oldClm -> {
            Column clmn = new Column();
            clmn.setId(oldClm.getId());
            clmn.setLibelle(oldClm.getLibelle());
            clmn.setType(oldClm.getType());
            clmn.setValue(oldClm.getValue());
            listNewClmn.add(clmn);
        });
        return listNewClmn;
    }

    @SuppressWarnings("unchecked")
    public List<DataRow> makeInputForMtc(DataRow row, EbDemande demande) {
        List<EbMarchandise> listMarchandises = demande.getListMarchandises();

        List<DataRow> list = new ArrayList<>();

        // preparation de la structure
        List<PrMtcField> listField = prMtcFieldRepository.findAll();

        List<ColumnDto> listColumn = prepareColumn(demande, listField);

        if (listColumn != null && !listColumn.isEmpty()) {
            row.getDataColumns().forEach(dc -> {
                ColumnDto res = listColumn
                    .stream().filter(c -> c.getLibelle().equalsIgnoreCase(dc.getLibelle())).findFirst().orElse(null);
                dc.setValue(res != null ? res.getValue() : null);
            });

            if (listMarchandises != null) {

                for (EbMarchandise march: listMarchandises) {
                    DataRow dataRow = new DataRow();
                    dataRow.setDataColumns(cloneDataColumn(row.getDataColumns()));

                    for (PrMtcField field: listField) {

                        if (field.getMtgAttribute().equalsIgnoreCase("idColonne")) {
                            dataRow.getDataColumns().stream()
                                    .filter(c -> c.getLibelle().equalsIgnoreCase(field.getMtcAttribute()))
                                    .findFirst()
                                    .ifPresent(column -> column.setValue(listMarchandises.indexOf(march)));
                        }
                        else if (field.getMtgAttribute().toLowerCase().startsWith("listmarchandises")) {
                            Column col = row
                                .getDataColumns().stream()
                                .filter(c -> c.getLibelle().equalsIgnoreCase(field.getMtcAttribute())).findFirst()
                                .orElse(null);

                            List<UnitInfo> unitInfos = new ArrayList<>();
                            if (col != null) {
                                unitInfos = new ArrayList<>((Collection<UnitInfo>) col.getValue());
                            }

                            if (!unitInfos.isEmpty()) {
                                dataRow.getDataColumns().stream()
                                    .filter(c -> c.getLibelle().equalsIgnoreCase(field.getMtcAttribute())).findFirst()
                                    .get().setValue(
                                        unitInfos
                                            .stream()
                                            .filter(
                                                type -> type
                                                    .getId().intValue() == (listMarchandises.indexOf(march) + 1))
                                            .findFirst().get().getValue());
                            }

                        }

                    }

                    list.add(dataRow);
                }

            }
            else list.add(row);

        }

        return list;
    }

    public List<ColumnDto> prepareColumn(EbDemande demande, List<PrMtcField> listField) {
        List<ColumnDto> listColumn = new ArrayList<>();

        for (PrMtcField item: listField) {
            ColumnDto clmn = new ColumnDto();
            clmn.setLibelle(item.getMtcAttribute());
            clmn.setValue(GenericObjectUtils.getValueByObjectPath(EbDemande.class, demande, item.getMtgAttribute()));
            listColumn.add(clmn);
        }

        return listColumn;
    }

    public static List<Object> getUnitDangerous(List<EbMarchandise> list) {
        List<Object> collect = list.stream().map(item -> {
            UnitInfo unitInfo = new UnitInfo();
            unitInfo.setId(list.indexOf(item) + 1);
            unitInfo
                .setValue(
                    item.getDangerousGood() != null ?
                            MarchandiseDangerousGood.NO.getCode().equals(item.getDangerousGood()) ? "0" : "1" :
                        null);
            return unitInfo;
        }).collect(Collectors.toList());
        return collect;
    }

    public static List<Object> getMarchandiseAttribute(List<EbMarchandise> list, String methName) {
        List<Object> collect = list.stream().map(item -> {
            UnitInfo unitInfo = new UnitInfo();
            unitInfo.setId(list.indexOf(item) + 1);
            Object val = null;

            try {
                val = GenericObjectUtils.callMethod(EbMarchandise.class, item, "get" + methName);
            } catch (Exception e) {
                e.printStackTrace();
            }

            unitInfo.setValue(val);
            return unitInfo;
        }).collect(Collectors.toList());
        return collect;
    }

    public Mono<ResultEntity> calcul(List<DataRow> list, int numConfig) {
        SearchCriteria criteria = new SearchCriteria();
        criteria.setListDataRow(list);
        Mono<ResultEntity> result = webClient
            .post()
            .uri(
                uriBuilder -> uriBuilder
                    .path(accessMtc.getApiUrl() + "/config/run/{ebConfigurationNum}").build(numConfig))
            .body(BodyInserters.fromObject(criteria)).retrieve().bodyToMono(ResultEntity.class);

        return result;
    }

    public Mono<ResultEntity> calcul(List<DataRow> list, String codeConfig) throws JsonProcessingException {
        SearchCriteria criteria = new SearchCriteria();
        criteria.setListDataRow(list);
        log.info("MTC : Start method 'calcul(List, String)'");
        final String url = accessMtc.getFullUrl() + "/config/run/code/" + codeConfig;
        log.info("MTC RQ url : {}", url);
        log.info("MTC RQ body : {}", JsonUtils.serializeToString(criteria));
        Mono<ResultEntity> result = webClient
            .post().uri(url).contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromObject(criteria)).exchange()
            .flatMap(clientResponse -> clientResponse.bodyToMono(ResultEntity.class));

        return result;
    }

    public DataRow loadMtcColumns(String configCode) throws JsonProcessingException {
        DataRow row = null;

        try {
            ResultEntity colum = getStructureByConfig(configCode).block();
            log.info("MTC RS getStructureByConfigCode body : {}", JsonUtils.serializeToString(colum));

            if (columnHasSuccessLabel( colum)) {
                ObjectMapper mapper = new ObjectMapper();
                String columAsString = mapper.writeValueAsString(colum.getValue());

                String rowAsString = "{ \"listPostCout\":[], \"type\":{\"code\":1,\"libelle\":\"OrdinaryRow\"}, \"parentId\":null, \"dataColumns\":"
                    + columAsString + "}";
                row = Statiques.convert(rowAsString, DataRow.class);
            }

        } catch (Exception e) {
            log.info("Error getStructureByConfigCode" + e);
            return null;
        }

        return row;
    }

    private boolean columnHasSuccessLabel(ResultEntity colum) {
        return colum != null && colum.getType() != null
                && SUCCESS_LABEL.equalsIgnoreCase(colum.getType().getLibelle());
    }

    public Mono<ResultEntity> getStructureByCarrierCode(String carrierCode) {
        Mono<ResultEntity> result = webClient
            .get().uri(accessMtc.getApiUrl() + "/data-structure/columns-list/" + carrierCode).retrieve()
            .bodyToMono(ResultEntity.class);

        return result;
    }

    public Mono<ResultEntity> getStructureByConfig(String numConfig) {
        log.info("MTC : Start method 'getStructureByConfig(String)'");
        Mono<ResultEntity> result = webClient
            .get().uri(accessMtc.getFullUrl() + "/data-structure/list-columns/" + numConfig)
            .accept(MediaType.APPLICATION_JSON).retrieve().bodyToMono(ResultEntity.class);

        return result;
    }

    public ResultEntity getValuation(final EbMarchandise marchandise, final String configCode) throws JsonProcessingException {
        if (!isValidValuationArgs(marchandise, configCode)) {
            throw new IllegalArgumentException("Try to call getValuation method with invalid args");
        }
        List<PrMtcMappingParam> mappingParams = mappingParamService.getParamsByConfigCode(configCode);

        List<DataRow> dataRows = getRequestMapped(mappingParams, marchandise);

        return calcul(dataRows, configCode).block();
    }

    private List<DataRow> getRequestMapped(List<PrMtcMappingParam> mappingParams, EbMarchandise marchandise) {
        List<Column> columns = mappingParams.stream()
                .map(param -> mapToMtcInput(param, marchandise))
                .collect(Collectors.toList());
        DataRow row = new DataRow();
        row.setDataColumns(columns);

        return Arrays.asList(row);
    }

    protected Column mapToMtcInput(PrMtcMappingParam param, EbMarchandise marchandise) {
        if (!validMapToMtcInputArgs(param, marchandise)) {
            throw new IllegalArgumentException("Try to call mapToMtcInput method with invalid args");
        }
        Column column = new Column();
        column.setId(param.getPrMtcMappingParamNum().intValue());
        column.setType(param.getType());
        column.setLibelle(param.getMtcAttribute());
        column.setValue(getValue(marchandise, param.getMtgAttribute()));
        return column;
    }

    private boolean validMapToMtcInputArgs(PrMtcMappingParam param, EbMarchandise marchandise) {
        return param != null && marchandise != null && marchandise.getEbDemande() != null;
    }

    private Object getValue(EbMarchandise marchandise, String mtgAttribute) {

        if (StringUtils.isBlank(mtgAttribute)) {
            return null;
        }
try {
        StandardEvaluationContext context = new StandardEvaluationContext(marchandise);
        propertyAccessorsRegistry.forEach(context::addPropertyAccessor);
        return new SpelExpressionParser().parseExpression(mtgAttribute).getValue(context);
	} catch (Exception e) {
		log.debug("Field needed " + e);
	}

	return null;
    }

    private boolean isValidValuationArgs(EbMarchandise marchandise, String configCode) {
        return marchandise != null && configCode != null;
    }
}
