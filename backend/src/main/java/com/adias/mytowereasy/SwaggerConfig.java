package com.adias.mytowereasy;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@ConditionalOnProperty(prefix = "swagger", name = "enabled", havingValue = "true")
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {
    @Value("${mytower.front.api-url}")
    private String url;

    @Value("${adias.support.email}")
    private String supporEmail;

    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
            .groupName("MyTower Transport API")
            .apiInfo(
                new ApiInfoBuilder()
                    .contact(new Contact("MyTower", url, supporEmail)).version("1.0").title("Mytower Transport API")
                    .description("Interact with MyTower Transport platform").build())
            .select().apis(RequestHandlerSelectors.any()).paths(PathSelectors.ant("/api/v1/**")).build()
            .forCodeGeneration(true).useDefaultResponseMessages(false);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
