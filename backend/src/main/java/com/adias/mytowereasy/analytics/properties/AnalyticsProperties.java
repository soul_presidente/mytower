package com.adias.mytowereasy.analytics.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties("mytower.analytics")
public class AnalyticsProperties {
    private String kibanaUrl;
    private Boolean kibanaSecured;

    private Dashboards dashboardId;

    public AnalyticsProperties() {
    }

    public String getKibanaUrl() {
        return kibanaUrl;
    }

    public void setKibanaUrl(String kibanaUrl) {
        this.kibanaUrl = kibanaUrl;
    }

    public Boolean getKibanaSecured() {
        return kibanaSecured;
    }

    public void setKibanaSecured(Boolean kibanaSecured) {
        this.kibanaSecured = kibanaSecured;
    }

    public Dashboards getDashboardId() {
        return dashboardId;
    }

    public void setDashboardId(Dashboards dashboardId) {
        this.dashboardId = dashboardId;
    }

    public static class Dashboards {
        private String transportOverview;
        private String pricing;
        private String activity;
        private String trackTrace;
        private String qualityManagement;
        private String freightAudit;
        private String carrierPerformance;

        public Dashboards() {
        }

        public String getTransportOverview() {
            return transportOverview;
        }

        public void setTransportOverview(String transportOverview) {
            this.transportOverview = transportOverview;
        }

        public String getPricing() {
            return pricing;
        }

        public void setPricing(String pricing) {
            this.pricing = pricing;
        }

        public String getActivity() {
            return activity;
        }

        public void setActivity(String activity) {
            this.activity = activity;
        }

        public String getTrackTrace() {
            return trackTrace;
        }

        public void setTrackTrace(String trackTrace) {
            this.trackTrace = trackTrace;
        }

        public String getQualityManagement() {
            return qualityManagement;
        }

        public void setQualityManagement(String qualityManagement) {
            this.qualityManagement = qualityManagement;
        }

        public String getFreightAudit() {
            return freightAudit;
        }

        public void setFreightAudit(String freightAudit) {
            this.freightAudit = freightAudit;
        }

        public String getCarrierPerformance() {
            return carrierPerformance;
        }

        public void setCarrierPerformance(String carrierPerformance) {
            this.carrierPerformance = carrierPerformance;
        }
    }
}
