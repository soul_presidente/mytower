package com.adias.mytowereasy.analytics.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.adias.mytowereasy.analytics.dto.AnalyticsConfigDTO;
import com.adias.mytowereasy.analytics.properties.AnalyticsProperties;


@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AnalyticsConfigMapper {
    AnalyticsConfigDTO toConfigDto(AnalyticsProperties properties);
}
