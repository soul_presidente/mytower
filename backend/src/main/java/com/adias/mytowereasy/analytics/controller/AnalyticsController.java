package com.adias.mytowereasy.analytics.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.adias.mytowereasy.analytics.dto.AnalyticsConfigDTO;
import com.adias.mytowereasy.analytics.mapper.AnalyticsConfigMapper;
import com.adias.mytowereasy.analytics.properties.AnalyticsProperties;
import com.adias.mytowereasy.dao.impl.DaoAnalyticsImpl;
import com.adias.mytowereasy.utils.search.SearchCriteriaFreightAnalytics;


@RestController
@RequestMapping("/api/analytics")
public class AnalyticsController {
    @Autowired
    DaoAnalyticsImpl daoAnalytics;

    @Autowired
    AnalyticsConfigMapper configMapper;

    @Autowired
    AnalyticsProperties analyticsProperties;

    @GetMapping("config")
    public AnalyticsConfigDTO getConfig() {
        return configMapper.toConfigDto(analyticsProperties);
    }

    @RequestMapping(value = "transport", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String
        getTransportData(@RequestBody(required = false) SearchCriteriaFreightAnalytics criteria) {
        JSONObject result = new JSONObject();

        if (criteria == null) {
            criteria = new SearchCriteriaFreightAnalytics();
        }

        try {
            result = daoAnalytics.getTransportData(criteria);
        } catch (Exception e) {
            e.getStackTrace();
        }

        return result.toString();
    }

    @RequestMapping(value = "activity", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody String
        getActivityData(@RequestBody(required = false) SearchCriteriaFreightAnalytics criteria) {
        JSONObject result = new JSONObject();

        if (criteria == null) {
            criteria = new SearchCriteriaFreightAnalytics();
        }

        try {
            result = daoAnalytics.getActivityData(criteria);
        } catch (Exception e) {
            e.getStackTrace();
        }

        return result.toString();
    }
}
