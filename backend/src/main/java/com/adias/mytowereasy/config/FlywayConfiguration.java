package com.adias.mytowereasy.config;

import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConditionalOnProperty(prefix = "mytower.flyway", name = "enabled", havingValue = "true")
public class FlywayConfiguration {
    @Autowired
    public FlywayConfiguration(DataSource dataSource) {
        Flyway
            .configure().baselineOnMigrate(false).outOfOrder(true).ignoreMissingMigrations(true)
            .ignoreFutureMigrations(true).schemas("flyway").dataSource(dataSource).load().migrate();
    }
}
