/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy.connectedUser;

import com.adias.mytowereasy.dto.EbCompagnieDTO;
import com.adias.mytowereasy.dto.EbEtablissementDTO;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.Role;
import com.adias.mytowereasy.model.Enumeration.ServiceType;


public class ConnectedUser {
    private Integer ebUserNum;
    private String nom;
    private String prenom;
    private String username;
    private String telephone;
    private String email;

    private EbEtablissementDTO ebEtablissement;
    private EbCompagnieDTO ebCompagnie;

    private boolean admin;
    private Integer service;
    private boolean superAdmin;
    private Integer role;
    private String roleLibelle;

    private String language;
    private Integer fuseauHoraire;

    private boolean superCt;

    private Integer realUserNum;
    private String realUserNom;
    private String realUserPrenom;
    private String realUserEmail;
    private String realUserPassword;
    private Integer realUserEtabNum;
    private String realUserEtabNom;
    private Integer realUserCompNum;
    private String realUserCompNom;

    public EbCompagnieDTO getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnieDTO ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    public String getRoleLibelle() {
        return roleLibelle;
    }

    public void setRoleLibelle(String roleLibelle) {
        this.roleLibelle = roleLibelle;
    }

    public Integer getService() {
        return service;
    }

    public void setService(Integer service) {
        this.service = service;
    }

    public ConnectedUser(EbUser ebUser) {
        this.ebUserNum = ebUser.getEbUserNum();
        this.nom = ebUser.getNom();
        this.prenom = ebUser.getPrenom();
        this.telephone = ebUser.getTelephone();
        this.email = ebUser.getEmail();

        this.realUserNum = ebUser.getRealUserNum();
        this.realUserNom = ebUser.getRealUserNom();
        this.realUserPrenom = ebUser.getRealUserPrenom();
        this.realUserEmail = ebUser.getRealUserEmail();
        this.realUserPassword = ebUser.getRealUserPassword();
        this.realUserEtabNum = ebUser.getRealUserEtabNum();
        this.realUserEtabNom = ebUser.getRealUserEtabNom();
        this.realUserCompNum = ebUser.getRealUserCompNum();
        this.realUserCompNom = ebUser.getRealUserCompNom();

        this.ebEtablissement = new EbEtablissementDTO();
        this.ebEtablissement.setEbEtablissementNum(ebUser.getEbEtablissement().getEbEtablissementNum());
        this.ebEtablissement.setNom(ebUser.getEbEtablissement().getNom());
        this.ebEtablissement.setSiret(ebUser.getEbEtablissement().getSiret());
        this.ebEtablissement.setLogo(ebUser.getEbEtablissement().getLogo());
        this.ebEtablissement.setTypeEtablissement(ebUser.getEbEtablissement().getTypeEtablissement());
        this.ebEtablissement.setCanShowPrice(ebUser.getEbEtablissement().getCanShowPrice());

        this.admin = ebUser.isAdmin();
        this.service = ebUser.getService();
        this.superAdmin = ebUser.isSuperAdmin();
        this.superCt = ebUser.isSuperCt();
        this.role = ebUser.getRole();

        if (ebUser.getEbCompagnie() != null) {
            this.ebCompagnie = new EbCompagnieDTO(
                ebUser.getEbCompagnie().getEbCompagnieNum(),
                ebUser.getEbCompagnie().getCode(),
                ebUser.getEbCompagnie().getNom(),
                ebUser.getEbCompagnie().getLogo(),
                ebUser.getEbCompagnie().getCompagnieRole());
        }

        this.roleLibelle = ebUser.getRoleLibelle();
        this.language = ebUser.getLanguage();
        this.fuseauHoraire = ebUser.getFuseauHoraire();
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public Integer getEbUserNum() {
        return ebUserNum;
    }

    public void setEbUserNum(Integer ebUserNum) {
        this.ebUserNum = ebUserNum;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public EbEtablissementDTO getEbEtablissement() {
        return ebEtablissement;
    }

    public void setEbEtablissement(EbEtablissementDTO ebEtablissement) {
        this.ebEtablissement = ebEtablissement;
    }

    public boolean isChargeur() {
        return (this.role != null && this.role.equals(Role.ROLE_CHARGEUR.getCode()));
    }

    public boolean isPrestataire() {
        return (this.role != null && this.role.equals(Role.ROLE_PRESTATAIRE.getCode()));
    }

    public boolean isBroker() {
        return isPrestataire() && (this.service != null && this.service.equals(ServiceType.SERVICE_BROKER.getCode()));
    }

    public boolean isTransporteur() {
        return isPrestataire() &&
            (this.service != null && this.service.equals(ServiceType.SERVICE_TRANSPORTEUR.getCode()));
    }

    public boolean isTransporteurBroker() {
        return isPrestataire() &&
            (this.service != null && this.service.equals(ServiceType.SERVICE_BROKER_TRANSPORTEUR.getCode()));
    }

    public boolean isSuperAdmin() {
        return superAdmin;
    }

    public void setSuperAdmin(boolean superAdmin) {
        this.superAdmin = superAdmin;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public Integer getRealUserNum() {
        return realUserNum;
    }

    public void setRealUserNum(Integer realUserNum) {
        this.realUserNum = realUserNum;
    }

    public String getRealUserNom() {
        return realUserNom;
    }

    public void setRealUserNom(String realUserNom) {
        this.realUserNom = realUserNom;
    }

    public String getRealUserPrenom() {
        return realUserPrenom;
    }

    public void setRealUserPrenom(String realUserPrenom) {
        this.realUserPrenom = realUserPrenom;
    }

    public String getRealUserEmail() {
        return realUserEmail;
    }

    public void setRealUserEmail(String realUserEmail) {
        this.realUserEmail = realUserEmail;
    }

    public Integer getRealUserEtabNum() {
        return realUserEtabNum;
    }

    public void setRealUserEtabNum(Integer realUserEtabNum) {
        this.realUserEtabNum = realUserEtabNum;
    }

    public String getRealUserEtabNom() {
        return realUserEtabNom;
    }

    public void setRealUserEtabNom(String realUserEtabNom) {
        this.realUserEtabNom = realUserEtabNom;
    }

    public Integer getRealUserCompNum() {
        return realUserCompNum;
    }

    public void setRealUserCompNum(Integer realUserCompNum) {
        this.realUserCompNum = realUserCompNum;
    }

    public String getRealUserCompNom() {
        return realUserCompNom;
    }

    public void setRealUserCompNom(String realUserCompNom) {
        this.realUserCompNom = realUserCompNom;
    }

    public String getRealUserPassword() {
        return realUserPassword;
    }

    public void setRealUserPassword(String realUserPassword) {
        this.realUserPassword = realUserPassword;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getFuseauHoraire() {
        return fuseauHoraire;
    }

    public void setFuseauHoraire(Integer fuseauHoraire) {
        this.fuseauHoraire = fuseauHoraire;
    }

    public boolean isSuperCt() {
        return superCt;
    }

    public void setSuperCt(boolean superCt) {
        this.superCt = superCt;
    }
}
