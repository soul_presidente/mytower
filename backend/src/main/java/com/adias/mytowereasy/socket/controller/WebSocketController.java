package com.adias.mytowereasy.socket.controller;

import java.util.HashMap;
import java.util.Map;

import com.adias.mytowereasy.service.ConnectedUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

import com.adias.mytowereasy.socket.service.WebSocketService;
import com.adias.mytowereasy.util.JsonUtils;
import com.adias.mytowereasy.util.ResultEntity;


@Controller
public class WebSocketController {
    @Autowired
    WebSocketService webSocketService;

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    ConnectedUserService connectedUserService;

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @MessageMapping("/pricing/edit")
    @SendTo("/topics/pricing/edit")
    public ResultEntity lockUnlockPricingDetails(String message) throws Exception {
        ResultEntity res = new ResultEntity();

        Map<String, Object> param = JsonUtils.deserializeMap(message);

        if (param != null) {
            LOGGER.debug("lockUnlockPricingDetails > Data : " + param.get("data") + " Lock : " + param.get("lock"));
            Object val = webSocketService
                .lockUnlockPricingDetails(
                    (Integer) param.get("data"),
                    (boolean) param.get("lock"),
                    (String) param.get("sessionId"),
                    (boolean) param.get("free"));
            res.setValue(val);
        }

        return res;
    }

    public void updateNotifications() throws Exception {
        Map<String, Object> headers = new HashMap<>();
        headers.put("user",connectedUserService.getCurrentUser().getEbUserNum());
        simpMessagingTemplate.convertAndSend("/topics/notifications/update", "reloadNotifications", headers);
    }
}
