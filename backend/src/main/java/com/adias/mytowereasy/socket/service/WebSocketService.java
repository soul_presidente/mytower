package com.adias.mytowereasy.socket.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.Enumeration.Module;
import com.adias.mytowereasy.util.ResultEntity;
import com.adias.mytowereasy.util.SocketSessionData;


@Service
public class WebSocketService {
    public List<SocketSessionData> listSessionData = new ArrayList<>();

    public SocketSessionData
        lockUnlockPricingDetails(Integer ebDemandeNum, boolean lock, String sessionId, boolean free) {
        boolean toLock = false;
        Integer module = Module.PRICING.getCode();

        ResultEntity res = new ResultEntity();
        res.setValue(false);

        SocketSessionData data = getLockedSessionData(ebDemandeNum, module);

        if (data == null || sessionId.contentEquals(data.getSessionId())) {

            if (data == null && lock) {
                data = new SocketSessionData();
                data.setSessionId(sessionId);
                data.setModule(module);
                listSessionData.add(data);
                toLock = true;
            }
            else if (data != null) {
                if (data.isPricingDetailsLocked() && !lock) data.setPricingDetailsLocked(false);
                else if (lock) toLock = true;
            }

            if (data != null && toLock) {
                data.setPricingDetailsLocked(true);
                data.setRecordId(ebDemandeNum);
            }

            if (data != null && data.canUnload()) listSessionData.remove(data);
        }

        if (data == null) {
            data = new SocketSessionData();
            data.setSessionId(sessionId);
            data.setModule(module);
            data.setRecordId(ebDemandeNum);
            data.setPricingDetailsLocked(false);
        }

        data.setFree(free);

        return data;
    }

    public void cleanSessionsData(String sessionId) {
        List<SocketSessionData> filtered = listSessionData
            .stream().filter(it -> sessionId != null && sessionId.equals(it.getSessionId()))
            .collect(Collectors.toList());
        filtered.stream().forEach(it -> listSessionData.remove(it));
    }

    private SocketSessionData getLockedSessionData(Integer recordId, Integer module) {
        return listSessionData
            .stream()
            .filter(
                it -> module.equals(it.getModule()) && recordId.equals(it.getRecordId()) && it.isPricingDetailsLocked()
                    && !it.isFree())
            .findFirst().orElse(null);
    }
}
