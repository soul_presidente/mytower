/*
 * Copyright (C) Adias - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
package com.adias.mytowereasy;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;


@EntityScan({
    "com.adias.mytowereasy.model",
    "com.adias.mytowereasy.delivery.model",
    "com.adias.mytowereasy.chanel.model",
    "com.adias.mytowereasy.trpl.model",
    "com.adias.mytowereasy.rsch.model",
    "com.adias.mytowereasy.dock.model",
    "com.adias.mytowereasy.airbus.model",
    "com.adias.mytowereasy.cptm.model",
    "com.adias.mytowereasy.cronjob.tt.model",
    "com.adias.mytowereasy.mtc.model",
})
@SpringBootApplication
@EnableScheduling
public class MyTowerEasyApplication extends SpringBootServletInitializer implements CommandLineRunner {
    @Autowired
    DataSource datasource;

    @Value("${mtc.base.url}")
    private String mtcUrl;

    public static void main(String[] args) {
        SpringApplication.run(MyTowerEasyApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("DATASOURCE = " + datasource);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @SuppressWarnings("unchecked")
    @Bean
    public Jackson2ObjectMapperBuilder configureObjectMapper() {
        return new Jackson2ObjectMapperBuilder().modulesToInstall(Hibernate5Module.class);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(MyTowerEasyApplication.class);
    }
}
