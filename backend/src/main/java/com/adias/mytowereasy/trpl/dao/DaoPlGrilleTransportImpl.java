package com.adias.mytowereasy.trpl.dao;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.impl.Dao;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.trpl.model.EbPlGrilleTransport;
import com.adias.mytowereasy.trpl.model.QEbPlGrilleTransport;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoPlGrilleTransportImpl extends Dao implements DaoPlGrilleTransport {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbPlGrilleTransport qEbPlGrilleTransport = QEbPlGrilleTransport.ebPlGrilleTransport;
    QEbUser qEbTransporteur = new QEbUser("qEbTransporteur");
    QEbCompagnie qEbGrilleCompagnie = new QEbCompagnie("qEbGrilleCompagnie");
    QEcCurrency qEbGrilleCurrency = new QEcCurrency("qEbGrilleCurrency");

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbPlGrilleTransport> listGrilleTransport(SearchCriteria criteria) {
        List<EbPlGrilleTransport> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbPlGrilleTransport> query = new JPAQuery<>(em);

            // TODO select field to retrieve
            query.select(qEbPlGrilleTransport);

            query = getGrilleGlobalWhere(query, where, criteria);
            query.from(qEbPlGrilleTransport);
            query.where(where);
            query = getGrilleGlobalJoin(query, where, criteria);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions
                    .path(Object.class, qEbPlGrilleTransport, criteria.getOrderedColumn());

                if (criteria.getOrderedColumn().contains("tranche")) {
                    String[] trancheData = criteria.getOrderedColumn().split("-");

                    if (trancheData.length > 1) {
                        // Integer index = Integer.getInteger(trancheData[1]);
                        query
                            .orderBy(
                                new OrderSpecifier(
                                    criteria.getAscendant() ? Order.ASC : Order.DESC,
                                    qEbPlGrilleTransport.costItems.any().trancheValues.any().value));
                    }

                }
                else if (criteria.getOrderedColumn().contains("currency")) {
                    query
                        .orderBy(
                            new OrderSpecifier(
                                criteria.getAscendant() ? Order.ASC : Order.DESC,
                                qEbGrilleCurrency.code));
                }
                else if (criteria.getOrderedColumn().contains("transporteurNum")) {
                    query
                        .orderBy(
                            new OrderSpecifier(
                                criteria.getAscendant() ? Order.ASC : Order.DESC,
                                qEbTransporteur.ebUserNum));
                }
                else {
                    fieldPath = Expressions.path(Object.class, qEbPlGrilleTransport, criteria.getOrderedColumn());
                    query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
                }

            }
            else {
                query.orderBy(qEbPlGrilleTransport.reference.asc());
            }

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long countListGrilleTransport(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbPlGrilleTransport> query = new JPAQuery<EbPlGrilleTransport>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getGrilleGlobalWhere(query, where, criteria);
            query.where(where);
            query.from(qEbPlGrilleTransport);
            query = getGrilleGlobalJoin(query, where, criteria);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({
        "rawtypes"
    })
    private JPAQuery getGrilleGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        LocalDate localDateNow = LocalDate.now();

        if (connectedUser.getEbCompagnie().getEbCompagnieNum() != null) {
            where.and(qEbGrilleCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }

        if (criteria.getGridVersion() != null
            && criteria.getGridVersion() == Enumeration.VersionGrid.PREVIOUS.getCode()) {
            where.and(qEbPlGrilleTransport.dateFin.isNotNull());
            where.and(qEbPlGrilleTransport.dateFin.lt(localDateNow));
        }
        else if (criteria.getGridVersion() != null
            && criteria.getGridVersion() == Enumeration.VersionGrid.UPCOMING.getCode()) {
            where.and(qEbPlGrilleTransport.dateDebut.isNotNull());
            where.and(qEbPlGrilleTransport.dateDebut.gt(localDateNow));
        }
        else {
            where.and(qEbPlGrilleTransport.dateDebut.isNotNull());
            where.and(qEbPlGrilleTransport.dateDebut.loe(localDateNow));
            where
                .andAnyOf(
                    qEbPlGrilleTransport.dateFin.isNull(),
                    qEbPlGrilleTransport.dateFin.goe(localDateNow));
        }

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();

            where
                .andAnyOf(
                    qEbPlGrilleTransport.reference.toLowerCase().contains(term),
                    qEbPlGrilleTransport.currency().code.toLowerCase().contains(term),
                    qEbPlGrilleTransport.designation.toLowerCase().contains(term),
                    qEbPlGrilleTransport.transporteur().nom.toLowerCase().contains(term),
                    qEbPlGrilleTransport.transporteur().nom
                        .concat(" ").concat(qEbPlGrilleTransport.transporteur().prenom).toLowerCase().contains(term));
        }

        // TODO adapt
        /*
         * if (criteria.getSearchterm() != null)
         * {
         * String term = criteria.getSearchterm().toLowerCase();
         * List<Integer> listGrilleTransportCalculationMode =
         * Enumeration.GrilleTransportCalculationMode.getCodesByLibelle(term);
         * //
         * where.andAnyOf(qEbGrilleTransport.reference.toLowerCase().contains(
         * term),
         * // qEbGrilleTransport.fixedFees.stringValue().contains(term),
         * // qEbGrilleTransport.currency().code.toLowerCase().contains(term),
         * // qEbGrilleTransport.calculationMode.in(
         * listGrilleTransportCalculationMode),
         * //
         * qEbGrilleTransport.trancheValues.any().value.stringValue().contains(
         * term),
         * // qEbGrilleTransport.designation.toLowerCase().contains(term));
         * }
         */

        return query;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    private JPAQuery getGrilleGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        query.leftJoin(qEbPlGrilleTransport.ebCompagnie(), qEbGrilleCompagnie);
        query.leftJoin(qEbPlGrilleTransport.currency(), qEbGrilleCurrency);
        query.leftJoin(qEbPlGrilleTransport.transporteur(), qEbTransporteur);
        return query;
    }
}
