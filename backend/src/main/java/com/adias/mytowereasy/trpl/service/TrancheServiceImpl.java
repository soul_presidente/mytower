package com.adias.mytowereasy.trpl.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.trpl.dto.EbPlTrancheDTO;
import com.adias.mytowereasy.trpl.model.EbPlTranche;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class TrancheServiceImpl extends MyTowerService implements TrancheService {
    @Override
    public List<EbPlTranche> listTranche(SearchCriteria criteria) {
        return daoPlTranche.listTranche(criteria);
    }

    @Override
    public EbPlTranche saveTranche(EbPlTrancheDTO trancheDto) {
        EbPlTranche tranche;

        if (trancheDto.getEbTrancheNum() != null) {
            tranche = ebTrancheRepository.findById(trancheDto.getEbTrancheNum()).get();
        }
        else {
            tranche = new EbPlTranche();
        }

        tranche.setMaximum(trancheDto.getMaximum());
        tranche.setMinimum(trancheDto.getMinimum());

        EbUser connectedUser = connectedUserService.getCurrentUser();

        tranche.setEbCompagnie(connectedUser.getEbCompagnie());

        return ebTrancheRepository.save(tranche);
    }

    @Override
    @Transactional
    public Boolean deleteTranche(Integer trancheNum) {
        // Delete all associated tranches values
        ebPlLigneCostItemTrancheRepository.deleteByTrancheEbTrancheNum(trancheNum);

        ebTrancheRepository.deleteById(trancheNum);
        return true;
    }

    @Override
    public Long countTranche(SearchCriteria criteria) {
        return daoPlTranche.countListTranche(criteria);
    }

    @Override
    public List<EbPlTrancheDTO> convertEntitiesToDTOs(List<EbPlTranche> dbList) {
        List<EbPlTrancheDTO> dtos = new ArrayList<>();

        if (dbList != null) {
            dbList.forEach(dbi -> {
                dtos.add(new EbPlTrancheDTO(dbi));
            });
        }

        return dtos;
    }
}
