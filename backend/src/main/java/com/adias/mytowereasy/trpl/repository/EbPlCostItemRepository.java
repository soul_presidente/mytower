package com.adias.mytowereasy.trpl.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.adias.mytowereasy.repository.CommonRepository;
import com.adias.mytowereasy.trpl.model.EbPlCostItem;


@Repository
public interface EbPlCostItemRepository extends CommonRepository<EbPlCostItem, Integer> {
    @Transactional
    @Modifying
    @Query("update EbPlCostItem d set d.euroExchangeRate = :euroExchangeRate where d.costItemNum = :costItemNum")
    public int updateEuroExchangeRateEbPlCostItem(
        @Param("costItemNum") Integer costItemNum,
        @Param("euroExchangeRate") Double euroExchangeRate);
}
