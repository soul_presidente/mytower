package com.adias.mytowereasy.trpl.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.repository.CommonRepository;
import com.adias.mytowereasy.trpl.model.EbPlLigneCostItemTranche;


@Repository
public interface EbPlLigneCostItemTrancheRepository extends CommonRepository<EbPlLigneCostItemTranche, Integer> {

    // List<EbPlLigneCostItemTranche> findByCostItemCostItemNum(Integer
    // costItemNum);
    List<EbPlLigneCostItemTranche> findByTrancheEbTrancheNum(Integer ebTrancheNum);

    void deleteByTrancheEbTrancheNum(Integer ebTrancheNum);
}
