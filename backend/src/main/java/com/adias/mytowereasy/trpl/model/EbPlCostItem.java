package com.adias.mytowereasy.trpl.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import com.adias.mytowereasy.model.EbTypeUnit;
import com.adias.mytowereasy.model.EcCurrency;


@Entity
@Table(name = "eb_pl_cost_item", schema = "work")
public class EbPlCostItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer costItemNum;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JsonIgnore
    private EbPlGrilleTransport grilleTransport;

    @Column
    private BigDecimal fixedFees;

    @Column
    @ColumnDefault("0")
    private Integer calculationMode;

    @OneToMany(mappedBy = "costItem", cascade = {
        CascadeType.REFRESH, CascadeType.REMOVE
    }, fetch = FetchType.EAGER)
    private List<EbPlLigneCostItemTranche> trancheValues;

    private Integer type;
    private String libelle;

    private Integer startingPsl;

    private Integer endPsl;
    @Column(columnDefinition = "TEXT")
    private String comment;
    private Double minFrees;
    private Double maxFrees;
    @ManyToOne(fetch = FetchType.EAGER)
    private EbTypeUnit typeUnit;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private BigDecimal price;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "currency")
    private EcCurrency currency;

    private String code;

    @Transient
    @JsonDeserialize
    @JsonSerialize
    private Double priceReal;

    private Double euroExchangeRateReal;

    private Double euroExchangeRate;

    private Double priceGab;

	@Column(columnDefinition = "boolean default false")
	private boolean horsGrille;

    public Double getEuroExchangeRate() {
        return euroExchangeRate;
    }

    public void setEuroExchangeRate(Double euroExchangeRate) {
        this.euroExchangeRate = euroExchangeRate;
    }

    private Double priceRealConverted;

    private Double priceGabConverted;

    public EbPlCostItem() {
    }

    public Integer getCostItemNum() {
        return costItemNum;
    }

    public void setCostItemNum(Integer costItemNum) {
        this.costItemNum = costItemNum;
    }

    public EbPlGrilleTransport getGrilleTransport() {
        return grilleTransport;
    }

    public void setGrilleTransport(EbPlGrilleTransport grilleTransport) {
        this.grilleTransport = grilleTransport;
    }

    public BigDecimal getFixedFees() {
        return fixedFees;
    }

    public void setFixedFees(BigDecimal fixedFees) {
        this.fixedFees = fixedFees;
    }

    public Integer getCalculationMode() {
        return calculationMode;
    }

    public void setCalculationMode(Integer calculationMode) {
        this.calculationMode = calculationMode;
    }

    public List<EbPlLigneCostItemTranche> getTrancheValues() {
        return trancheValues;
    }

    public void setTrancheValues(List<EbPlLigneCostItemTranche> trancheValues) {
        this.trancheValues = trancheValues;
    }

    public Integer getStartingPsl() {
        return startingPsl;
    }

    public void setStartingPsl(Integer startingPsl) {
        this.startingPsl = startingPsl;
    }

    public Integer getEndPsl() {
        return endPsl;
    }

    public void setEndPsl(Integer endPsl) {
        this.endPsl = endPsl;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getMinFrees() {
        return minFrees;
    }

    public void setMinFrees(Double minFrees) {
        this.minFrees = minFrees;
    }

    public Double getMaxFrees() {
        return maxFrees;
    }

    public void setMaxFrees(Double maxFrees) {
        this.maxFrees = maxFrees;
    }

    public EbTypeUnit getTypeUnit() {
        return typeUnit;
    }

    public void setTypeUnit(EbTypeUnit typeUnit) {
        this.typeUnit = typeUnit;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public EcCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(EcCurrency currency) {
        this.currency = currency;
    }

    public Double getPriceReal() {
        return priceReal;
    }

    public void setPriceReal(Double priceReal) {
        this.priceReal = priceReal;
    }

    public Double getEuroExchangeRateReal() {
        return euroExchangeRateReal;
    }

    public void setEuroExchangeRateReal(Double euroExchangeRateReal) {
        this.euroExchangeRateReal = euroExchangeRateReal;
    }

    public Double getPriceGab() {
        return priceGab;
    }

    public void setPriceGab(Double priceGab) {
        this.priceGab = priceGab;
    }

    public Double getPriceRealConverted() {
        return priceRealConverted;
    }

    public void setPriceRealConverted(Double priceRealConverted) {
        this.priceRealConverted = priceRealConverted;
    }

    public Double getPriceGabConverted() {
        return priceGabConverted;
    }

    public void setPriceGabConverted(Double priceGabConverted) {
        this.priceGabConverted = priceGabConverted;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

	public boolean isHorsGrille() {
		return horsGrille;
	}

	public void setHorsGrille(boolean horsGrille) {
		this.horsGrille = horsGrille;
	}

}
