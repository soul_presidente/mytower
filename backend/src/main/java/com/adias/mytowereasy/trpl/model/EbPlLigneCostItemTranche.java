package com.adias.mytowereasy.trpl.model;

import javax.persistence.*;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name = "eb_pl_ligne_cost_item_tranche", schema = "work")
public class EbPlLigneCostItemTranche {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebPlLigneCostItemTrancheNum;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    @JsonIgnore
    private EbPlCostItem costItem;

    @ManyToOne(fetch = FetchType.EAGER)
    @OnDelete(action = OnDeleteAction.NO_ACTION)
    private EbPlTranche tranche;

    private Double value;

    public EbPlTranche getTranche() {
        return tranche;
    }

    public void setTranche(EbPlTranche tranche) {
        this.tranche = tranche;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public EbPlCostItem getCostItem() {
        return costItem;
    }

    public void setCostItem(EbPlCostItem costItem) {
        this.costItem = costItem;
    }

    public Integer getEbPlLigneCostItemTrancheNum() {
        return ebPlLigneCostItemTrancheNum;
    }

    public void setEbPlLigneCostItemTrancheNum(Integer ebPlLigneCostItemTranche) {
        this.ebPlLigneCostItemTrancheNum = ebPlLigneCostItemTranche;
    }
}
