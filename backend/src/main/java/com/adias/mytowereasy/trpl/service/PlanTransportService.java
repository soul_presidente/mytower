package com.adias.mytowereasy.trpl.service;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.dto.PricingCarrierSearchResultDTO;
import com.adias.mytowereasy.trpl.dto.EbPlTransportDTO;
import com.adias.mytowereasy.trpl.model.EbPlTransport;
import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.trpl.model.SearchCriteriaPlanTransport;


public interface PlanTransportService {
    public List<EbPlTransport> listPlan(SearchCriteriaPlanTransport searchCriteria);

    public EbPlTransport savePlan(EbPlTransportDTO ebPlanTransport);

    public Boolean deletePlan(Integer planTransportNum);

    public List<EbPlTransportDTO> convertEntitiesToDTOs(List<EbPlTransport> dbs);

    public List<PricingCarrierSearchResultDTO> searchCarrier(SearchCriteriaPlanTransport searchCriteria);

    public List<PricingCarrierSearchResultDTO>
        searchCarrier(SearchCriteriaPlanTransport searchCriteria, boolean isfromMtc) throws JsonProcessingException;

    Long getListEbPlanTransportCount(SearchCriteriaPlanTransport criteria);

    public List<EbPlZone> getListZones();
}
