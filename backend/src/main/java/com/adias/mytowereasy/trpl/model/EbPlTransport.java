package com.adias.mytowereasy.trpl.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbTypeGoods;


@Entity
@Table(name = "eb_pl_transport", schema = "work")
public class EbPlTransport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebPlanTransportNum;

    private String reference;

    private String designation;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "zone_origine_id")
    private EbPlZone origine;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "zone_destination_id")
    private EbPlZone destination;

    @Column
    private Integer typeDemande;

    @Column
    private Integer modeTransport;
    @Column
    private Integer typeTransport;

    @Column
    private Boolean isDangerous;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_type_goods")
    private EbTypeGoods xEbTypeGoods;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "grille_id")
    private EbPlGrilleTransport grilleTarif;

    private Integer transitTime;

    private String comment;

    private Float tptt;
    private Float patt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company", insertable = true)
    private EbCompagnie ebCompagnie;

    public EbPlTransport() {
    }

    @QueryProjection
    public EbPlTransport(
        Integer ebPlanTransportNum,
        String reference,
        String designation,
        EbPlZone origine,
        EbPlZone destination,
        Integer typeDemande,
        Integer modeTransport,
        Integer typeTransport,
        Boolean isDangerous,
        Integer xEbTypeGoodsNum,
        EbPlGrilleTransport ebPlGrilleTransport,
        Integer transitTime,
        String comment,
        Float tptt,
        Float patt,
        Integer ebCompagnieNum) {
        this.ebPlanTransportNum = ebPlanTransportNum;
        this.reference = reference;
        this.designation = designation;
        this.origine = origine;
        this.destination = destination;
        this.typeDemande = typeDemande;
        this.modeTransport = modeTransport;
        this.typeTransport = typeTransport;
        this.isDangerous = isDangerous;
        this.xEbTypeGoods = new EbTypeGoods();
        this.xEbTypeGoods.setEbTypeGoodsNum(xEbTypeGoodsNum);
        this.grilleTarif = ebPlGrilleTransport;
        this.transitTime = transitTime;
        this.comment = comment;
        this.tptt = tptt;
        this.patt = patt;
        this.ebCompagnie = new EbCompagnie(ebCompagnieNum);
    }

    public Float getTptt() {
        return tptt;
    }

    public void setTptt(Float tptt) {
        this.tptt = tptt;
    }

    public Float getPatt() {
        return patt;
    }

    public void setPatt(Float patt) {
        this.patt = patt;
    }

    public Integer getEbPlanTransportNum() {
        return ebPlanTransportNum;
    }

    public void setEbPlanTransportNum(Integer ebPlanTransportNum) {
        this.ebPlanTransportNum = ebPlanTransportNum;
    }

    public EbPlZone getOrigine() {
        return origine;
    }

    public EbPlZone getDestination() {
        return destination;
    }

    public Boolean getIsDangerous() {
        return isDangerous;
    }

    public EbPlGrilleTransport getGrilleTarif() {
        return grilleTarif;
    }

    public void setOrigine(EbPlZone origine) {
        this.origine = origine;
    }

    public void setDestination(EbPlZone destination) {
        this.destination = destination;
    }

    public void setIsDangerous(Boolean isDangerous) {
        this.isDangerous = isDangerous;
    }

    public void setGrilleTarif(EbPlGrilleTransport grilleTarif) {
        this.grilleTarif = grilleTarif;
    }

    public Integer getTypeDemande() {
        return typeDemande;
    }

    public Integer getModeTransport() {
        return modeTransport;
    }

    public void setTypeDemande(Integer typeDemande) {
        this.typeDemande = typeDemande;
    }

    public void setModeTransport(Integer modeTransport) {
        this.modeTransport = modeTransport;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    public EbTypeGoods getxEbTypeGoods() {
        return xEbTypeGoods;
    }

    public void setxEbTypeGoods(EbTypeGoods xEbTypeGoods) {
        this.xEbTypeGoods = xEbTypeGoods;
    }

    public Integer getTypeTransport() {
        return typeTransport;
    }

    public void setTypeTransport(Integer typeTransport) {
        this.typeTransport = typeTransport;
    }
}
