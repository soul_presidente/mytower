package com.adias.mytowereasy.trpl.dto;

import com.adias.mytowereasy.trpl.model.EbPlTranche;


public class EbPlTrancheDTO {
    private Integer ebTrancheNum;

    private Double minimum;

    private Double maximum;

    public EbPlTrancheDTO() {
    }

    public EbPlTrancheDTO(EbPlTranche db) {
        this.ebTrancheNum = db.getEbTrancheNum();
        this.maximum = db.getMaximum();
        this.minimum = db.getMinimum();
    }

    public Integer getEbTrancheNum() {
        return ebTrancheNum;
    }

    public void setEbTrancheNum(Integer ebTrancheNum) {
        this.ebTrancheNum = ebTrancheNum;
    }

    public Double getMinimum() {
        return minimum;
    }

    public void setMinimum(Double minimum) {
        this.minimum = minimum;
    }

    public Double getMaximum() {
        return maximum;
    }

    public void setMaximum(Double maximum) {
        this.maximum = maximum;
    }
}
