package com.adias.mytowereasy.trpl.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dao.DaoPricing;
import com.adias.mytowereasy.model.EbAdresse;
import com.adias.mytowereasy.model.EbParty;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.service.CompagnieService;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.trpl.dao.DaoPlZone;
import com.adias.mytowereasy.trpl.dto.EbPlZoneDTO;
import com.adias.mytowereasy.trpl.model.EbPlTransport;
import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class ZoneServiceImpl extends MyTowerService implements ZoneService {
    @Autowired
    CompagnieService compagnieService;

    @Autowired
    DaoPlZone daoZone;

    @Autowired
    DaoPricing daoPricing;

    @Override
    public EbPlZone saveZone(EbPlZoneDTO zoneDto) {
        EbPlZone zone;

        if (zoneDto.getEbZoneNum() != null) {
            zone = ebZoneRepository.findById(zoneDto.getEbZoneNum()).get();
        }
        else {
            zone = new EbPlZone();
        }

        zone.setDesignation(zoneDto.getDesignation());
        zone.setRef(zoneDto.getRef());

        EbUser connectedUser = connectedUserService.getCurrentUser();
        zone.setEbCompagnie(connectedUser.getEbCompagnie());

        return ebZoneRepository.save(zone);
    }

    @Override
    public Boolean deleteZone(Integer zoneNum) {
        /** Before deleting zone, cleaning links * */
        EbPlZone zone = ebZoneRepository.findById(zoneNum).get();

        // Cleaning EbAdresse
        List<EbAdresse> adresses = this.daoPricing.getAllAdresseByZone(zoneNum);

        for (EbAdresse adresse: adresses) {
            adresse.getZones().removeIf(z -> (z.getEbZoneNum() == zoneNum));
            adresse.setListZonesNum(this.removeZoneNumFromList(adresse.getListZonesNum(), zoneNum));
            this.ebAdresseRepository
                .updateListZone(adresse.getZones(), adresse.getListZonesNum(), adresse.getEbAdresseNum());
        }

        // Cleaning EbParty
        List<EbParty> parties = ebPartyRepository.findAllByZone(zone);

        for (EbParty party: parties) {
            party.setZone(null);
            ebPartyRepository.save(party);
        }

        // Cleaning EbPlTransport
        List<EbPlTransport> plans = ebPlanTransportRepository.findAllByOrigineOrDestination(zone, zone);

        for (EbPlTransport plan: plans) {
            if (plan.getOrigine() != null
                && plan.getOrigine().getEbZoneNum() == zone.getEbZoneNum()) plan.setOrigine(null);
            if (plan.getDestination() != null
                && plan.getDestination().getEbZoneNum() == zone.getEbZoneNum()) plan.setDestination(null);

            ebPlanTransportRepository.save(plan);
        }
        /** END Clearing Links * */

        // Deleting
        ebZoneRepository.delete(zone);

        return true;
    }

    @Override
    public List<EbPlZone> listZone(SearchCriteria criteria) {
        return daoPlZone.listZone(criteria);
    }

    @Override
    public List<EbPlZone> listZoneWithCommunity() {
        SearchCriteria criteriaZone = new SearchCriteria();

        EbUser connectedUser = connectedUserService.getCurrentUser();
        criteriaZone.setCompanyNums(compagnieService.findContactsCompagniesNums(connectedUser.getEbUserNum(), true));
        return daoZone.listZone(criteriaZone);
    }

    @Override
    public Long countZone(SearchCriteria criteria) {
        return daoPlZone.countListZone(criteria);
    }

    @Override
    public List<EbPlZoneDTO> convertEntitiesToDTOs(List<EbPlZone> dbList) {
        List<EbPlZoneDTO> dtos = new ArrayList<>();

        if (dbList != null) {
            dbList.forEach(dbi -> {
                dtos.add(new EbPlZoneDTO(dbi));
            });
        }

        return dtos;
    }

    private String removeZoneNumFromList(String listZonesNum, Integer ebZoneNum) {
        String[] result = listZonesNum.split(":");
        List<String> newListZonesNum = new ArrayList<>();

        for (String r: result) {
            if (!r.isEmpty() && !r.equals(String.valueOf(ebZoneNum))) newListZonesNum.add(r);
        }

        return ":" + String.join(":", newListZonesNum) + ":";
    }
}
