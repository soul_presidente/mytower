package com.adias.mytowereasy.trpl.service;

import java.util.List;

import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.trpl.dto.EbPlGrilleTransportDTO;
import com.adias.mytowereasy.trpl.model.EbPlGrilleTransport;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface GrilleTransportService {
    Boolean deleteGrille(EbPlGrilleTransport ebGrilleTransport);

    Boolean deleteGrille(Integer ebGrilleTransportNum);

    Integer addOrUpdateGrilleTransportFromTable(EbPlGrilleTransportDTO grilleTransport);

    List<EbPlGrilleTransport> getAllGrilleTransportOrdered(SearchCriteria criteria);

    List<EbPlGrilleTransportDTO> getAllGrilleTransportForTable(SearchCriteria criteria);

    Long getListEbGrilleCount(SearchCriteria criteria);

    Long getAciveGridVersionCount(EbPlGrilleTransportDTO grDTO);

    List<EcCurrency> selectListCurrencyByeCompagnieCurrency(SearchCriteria criteria);
}
