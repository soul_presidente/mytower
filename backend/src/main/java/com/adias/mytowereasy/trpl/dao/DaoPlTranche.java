package com.adias.mytowereasy.trpl.dao;

import java.util.List;

import com.adias.mytowereasy.trpl.model.EbPlTranche;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoPlTranche {
    List<EbPlTranche> listTranche(SearchCriteria criteria);

    Long countListTranche(SearchCriteria criteria);
}
