package com.adias.mytowereasy.trpl.model;

import javax.persistence.*;

import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.model.EbCompagnie;


@Entity
@Table(name = "eb_pl_tranche", schema = "work")
public class EbPlTranche {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer ebTrancheNum;

    private Double minimum;

    private Double maximum;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_company", insertable = true)
    private EbCompagnie ebCompagnie;

    public EbPlTranche() {
    }

    // Constructors for QueryDSL

    @QueryProjection
    public EbPlTranche(
        Integer ebTrancheNum,
        Double minimum,
        Double maximum,
        Integer ebCompangieNum,
        String ebCompagnieName,
        String ebCompagnieCode) {
        this.ebTrancheNum = ebTrancheNum;
        this.minimum = minimum;
        this.maximum = maximum;

        this.ebCompagnie = new EbCompagnie(ebCompangieNum, ebCompagnieName, ebCompagnieCode);
    }

    // End Constructors for QueryDSL

    public Integer getEbTrancheNum() {
        return ebTrancheNum;
    }

    public EbPlTranche setEbTrancheNum(Integer ebTrancheNum) {
        this.ebTrancheNum = ebTrancheNum;
        return this;
    }

    public Double getMinimum() {
        return minimum;
    }

    public EbPlTranche setMinimum(Double minimum) {
        this.minimum = minimum;
        return this;
    }

    public Double getMaximum() {
        return maximum;
    }

    public EbPlTranche setMaximum(Double maximum) {
        this.maximum = maximum;
        return this;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }
}
