package com.adias.mytowereasy.trpl.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.impl.Dao;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.trpl.model.QEbPlZone;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoPlZoneImpl extends Dao implements DaoPlZone {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbPlZone qEbPlZone = QEbPlZone.ebPlZone;
    QEbCompagnie qEbZoneCompagnie = new QEbCompagnie("qEbZoneCompagnie");

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbPlZone> listZone(SearchCriteria criteria) {
        List<EbPlZone> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbPlZone> query = new JPAQuery<>(em);

            query
                .select(
                    QEbPlZone
                        .create(
                            qEbPlZone.ebZoneNum,
                            qEbPlZone.ref,
                            qEbPlZone.designation,
                            qEbZoneCompagnie.ebCompagnieNum,
                            qEbZoneCompagnie.nom,
                            qEbZoneCompagnie.code));

            query = getZoneGlobalWhere(query, where, criteria);

            query.from(qEbPlZone);
            query.where(where);

            query = getZoneGlobalJoin(query, where, criteria);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbPlZone, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbPlZone.ref.asc());
            }

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long countListZone(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbPlZone> query = new JPAQuery<EbPlZone>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getZoneGlobalWhere(query, where, criteria);

            query.from(qEbPlZone);
            query.where(where);

            query = getZoneGlobalJoin(query, where, criteria);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getZoneGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getAvailableCurrentUser(criteria);

        if (criteria.getCompanyNums() != null && criteria.getCompanyNums().size() > 0) {
            where.and(qEbZoneCompagnie.ebCompagnieNum.in(criteria.getCompanyNums()));
        }
        else if (criteria.getEbCompagnieNum() != null) {
            where.and(qEbZoneCompagnie.ebCompagnieNum.eq(criteria.getEbCompagnieNum()));
        }
        else if (connectedUser != null && connectedUser.getEbCompagnie() != null
            && connectedUser.getEbCompagnie().getEbCompagnieNum() != null) {
            where.and(qEbZoneCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qEbPlZone.ref.toLowerCase().contains(term),
                    qEbPlZone.designation.toLowerCase().contains(term));
        }

        return query;
    }

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    private JPAQuery getZoneGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        query.leftJoin(qEbPlZone.ebCompagnie(), qEbZoneCompagnie);
        return query;
    }
}
