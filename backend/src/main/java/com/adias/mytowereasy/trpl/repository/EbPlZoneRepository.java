package com.adias.mytowereasy.trpl.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.adias.mytowereasy.airbus.model.QrGroupeObject;
import com.adias.mytowereasy.trpl.model.EbPlZone;


public interface EbPlZoneRepository extends JpaRepository<EbPlZone, Integer> {
    @Query("select new com.adias.mytowereasy.airbus.model.QrGroupeObject(z.ebZoneNum, z.designation)  from EbPlZone z where z.ebCompagnie.ebCompagnieNum = ?1")
    public List<QrGroupeObject> findListEbPlZoneByCompagnieNum(Integer CompagnyNum);

    public EbPlZone findFirstByRefIgnoreCaseAndEbCompagnie_ebCompagnieNum(String ref, Integer ebCompagnieNum);
}
