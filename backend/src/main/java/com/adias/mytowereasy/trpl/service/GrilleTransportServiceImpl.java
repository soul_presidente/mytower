package com.adias.mytowereasy.trpl.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.adias.mytowereasy.dto.EcCurrencyDTO;
import com.adias.mytowereasy.model.EbCompagnieCurrency;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.service.CurrencyService;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.trpl.dto.EbPlCostItemDTO;
import com.adias.mytowereasy.trpl.dto.EbPlGrilleTransportDTO;
import com.adias.mytowereasy.trpl.dto.EbPlLigneCostItemTrancheDTO;
import com.adias.mytowereasy.trpl.model.*;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class GrilleTransportServiceImpl extends MyTowerService implements GrilleTransportService {
    @Autowired
    TrancheService trancheService;
    @Autowired
    CurrencyService currencyService;

    @Override
    public List<EbPlGrilleTransport> getAllGrilleTransportOrdered(SearchCriteria criteria) {
        return daoPlGrilleTransport.listGrilleTransport(criteria);
    }

    @Override
    @Transactional
    public Boolean deleteGrille(EbPlGrilleTransport ebGrilleTransport) {
        return deleteGrille(ebGrilleTransport.getEbGrilleTransportNum());
    }

    @Override
    @Transactional
    public Boolean deleteGrille(Integer ebGrilleTransportNum) {
        // Cleaning links before deleting
        List<EbPlTransport> plans = ebPlanTransportRepository
            .findAllByGrilleTarifEbGrilleTransportNum(ebGrilleTransportNum);

        for (EbPlTransport plan: plans) {
            plan.setGrilleTarif(null);
            ebPlanTransportRepository.save(plan);
        }

        ebPlGrilleTransportRepository.deleteById(ebGrilleTransportNum);
        return true;
    }

    @Override
    public List<EbPlGrilleTransportDTO> getAllGrilleTransportForTable(SearchCriteria criteria) {
        List<EbPlTranche> tranches = trancheService.listTranche(new SearchCriteria());
        List<EbPlGrilleTransport> allGrilles = getAllGrilleTransportOrdered(criteria);
        List<EbPlGrilleTransportDTO> rList = new ArrayList<>();

        for (EbPlGrilleTransport grille: allGrilles) {
            EbPlGrilleTransportDTO grDTO = new EbPlGrilleTransportDTO();
            grDTO.setCurrency(new EcCurrencyDTO(grille.getCurrency()));
            grDTO.setDesignation(grille.getDesignation());
            grDTO.setEbGrilleTransportNum(grille.getEbGrilleTransportNum());
            grDTO
                .setOrigineGrlNum(
                    grille.getOrigineGrl() != null ? grille.getOrigineGrl().getEbGrilleTransportNum() : null);
            grDTO.setReference(grille.getReference());
            grDTO.setDateDebut(grille.getDateDebut());
            grDTO.setDateFin(grille.getDateFin());
            grDTO.setTransporteurNum(grille.getTransporteur() != null ? grille.getTransporteur().getEbUserNum() : null);

            for (EbPlCostItem cost: grille.getCostItems()) {
                EbPlCostItemDTO costDTO = new EbPlCostItemDTO();
                costDTO.setCalculationMode(cost.getCalculationMode());
                costDTO.setCostItemNum(cost.getCostItemNum());
                costDTO.setFixedFees(cost.getFixedFees());
                costDTO.setType(cost.getType());
                costDTO.setStartingPsl(cost.getStartingPsl());
                costDTO.setEndPsl(cost.getEndPsl());
                costDTO.setComment(cost.getComment());
                costDTO.setMinFrees(cost.getMinFrees());
                costDTO.setMaxFrees(cost.getMaxFrees());
                costDTO.setTypeUnit(cost.getTypeUnit());
                costDTO.setCurrency(cost.getCurrency());

                for (EbPlTranche tranche: tranches) {
                    EbPlLigneCostItemTrancheDTO lineDTO = new EbPlLigneCostItemTrancheDTO();
                    lineDTO.setEbTrancheNum(tranche.getEbTrancheNum());

                    Optional<EbPlLigneCostItemTranche> line = cost
                        .getTrancheValues().stream()
                        .filter(v -> v.getTranche().getEbTrancheNum().equals(tranche.getEbTrancheNum())).findFirst();

                    if (line.isPresent()) {
                        lineDTO.setValue(line.get().getValue());
                        lineDTO.setEbPlLigneCostItemTrancheNum(line.get().getEbPlLigneCostItemTrancheNum());
                    }

                    costDTO.getTrancheValues().add(lineDTO);
                }

                grDTO.getCostItems().add(costDTO);
            }

            rList.add(grDTO);
        }

        return rList;
    }

    @Override
    public Long getAciveGridVersionCount(EbPlGrilleTransportDTO grDTO) {
        return ebPlGrilleTransportRepository.foundActiveGridVersion(grDTO.getOrigineGrlNum(), grDTO.getDateDebut());
    }

    @Override
    public Integer addOrUpdateGrilleTransportFromTable(EbPlGrilleTransportDTO grDTO) {
        Long count = 0l;

        // Vérifier le chevauchement des dates par rapport aux anciennes
        // versions de grille
        if (grDTO.getOrigineGrlNum() != null) {
            count = getAciveGridVersionCount(grDTO);
        }

        if (count == 0) {
            // Retrieving grille
            boolean isNew = grDTO.getEbGrilleTransportNum() == null;
            EbPlGrilleTransport grDB = new EbPlGrilleTransport();

            if (!isNew) {
                Optional<EbPlGrilleTransport> dbGrilleTransportOption = ebPlGrilleTransportRepository
                    .findById(grDTO.getEbGrilleTransportNum());
                if (dbGrilleTransportOption.isPresent()) grDB = dbGrilleTransportOption.get();
            }

            EbUser connectedUser = connectedUserService.getCurrentUser();
            grDB.setEbCompagnie(connectedUser.getEbCompagnie());

            // Retrieving tranches
            List<EbPlTranche> tranches = trancheService.listTranche(new SearchCriteria());

            // Update / create grille
            grDB.setCurrency(new EcCurrency(grDTO.getCurrency().getEcCurrencyNum()));
            grDB.setDesignation(grDTO.getDesignation());
            grDB.setReference(grDTO.getReference());
            grDB.setDateDebut(grDTO.getDateDebut());
            grDB.setDateFin(grDTO.getDateFin());

            if (grDTO.getOrigineGrlNum() != null) {
                grDB.setOrigineGrl(new EbPlGrilleTransport(grDTO.getOrigineGrlNum()));
            }

            EbUser tr = null;

            if (grDTO.getTransporteur() != null && grDTO.getTransporteur().getEbUserNum() != null) {
                tr = ebUserRepository.findById(grDTO.getTransporteur().getEbUserNum()).orElse(null);
            }

            grDB.setTransporteur(tr);

            if (grDB.getCostItems() == null) grDB.setCostItems(new ArrayList<>());
            ebPlGrilleTransportRepository.save(grDB);

            // Cost Item
            for (EbPlCostItemDTO costDTO: grDTO.getCostItems()) {
                EbPlCostItem costDB;

                if (costDTO.getCostItemNum() == null) {
                    costDB = new EbPlCostItem();
                }
                else {
                    costDB = ebPlCostItemRepository.findById(costDTO.getCostItemNum()).orElse(new EbPlCostItem());
                }

                costDB.setCalculationMode(costDTO.getCalculationMode());
                costDB.setFixedFees(costDTO.getFixedFees());
                costDB.setType(costDTO.getType());
                costDB.setStartingPsl(costDTO.getStartingPsl());
                costDB.setEndPsl(costDTO.getEndPsl());
                costDB.setLibelle(costDTO.getLibelle());

                if (costDB.getTrancheValues() == null) costDB.setTrancheValues(new ArrayList<>());
                costDB.setGrilleTransport(grDB);
                costDB.setComment(costDTO.getComment());
                costDB.setMinFrees(costDTO.getMinFrees());
                costDB.setMaxFrees(costDTO.getMaxFrees());
                if (costDTO.getTypeUnit() != null
                    && costDTO.getTypeUnit().getEbTypeUnitNum() != null) costDB.setTypeUnit(costDTO.getTypeUnit());
                if (costDTO.getCurrency() != null
                    && costDTO.getCurrency().getEcCurrencyNum() != null) costDB.setCurrency(costDTO.getCurrency());
                ebPlCostItemRepository.save(costDB);

                // Tranche
                for (EbPlLigneCostItemTrancheDTO trDTO: costDTO.getTrancheValues()) {
                    EbPlLigneCostItemTranche trDB;

                    if (trDTO.getEbPlLigneCostItemTrancheNum() == null) {
                        trDB = new EbPlLigneCostItemTranche();
                    }
                    else {
                        trDB = ebPlLigneCostItemTrancheRepository
                            .findById(trDTO.getEbPlLigneCostItemTrancheNum()).orElse(new EbPlLigneCostItemTranche());
                    }

                    trDB.setValue(trDTO.getValue());
                    trDB.setCostItem(costDB);
                    trDB
                        .setTranche(
                            tranches
                                .stream().filter(t -> t.getEbTrancheNum().equals(trDTO.getEbTrancheNum())).findFirst()
                                .get());
                    ebPlLigneCostItemTrancheRepository.save(trDB);
                }

            }

            // Drop deleted items
            for (EbPlCostItem costDB: grDB.getCostItems().stream().distinct().collect(Collectors.toList())) {
                boolean willDelete = true;

                for (EbPlCostItemDTO costDTO: grDTO.getCostItems()) {

                    if (costDTO.getCostItemNum() == null || costDB.getCostItemNum().equals(costDTO.getCostItemNum())) {
                        willDelete = false;
                    }

                }

                if (willDelete) {
                    ebPlCostItemRepository.delete(costDB);
                }

            }

            return grDB.getEbGrilleTransportNum();
        }

        return -1;
    }

    @Override
    public Long getListEbGrilleCount(SearchCriteria criteria) {
        return daoPlGrilleTransport.countListGrilleTransport(criteria);
    }

    @Override
    public List<EcCurrency> selectListCurrencyByeCompagnieCurrency(SearchCriteria criteria) {
        List<EbCompagnieCurrency> compagniesCurrency = currencyService
            .selectListExEtablissementCurrencyByEtablissementNum(criteria);
        List<EcCurrency> currencys = new ArrayList<EcCurrency>();

        for (EbCompagnieCurrency compagnieCurrency: compagniesCurrency) {
            EcCurrency c = currencys
                .stream()
                .filter(item -> item.getEcCurrencyNum().equals(compagnieCurrency.getEcCurrency().getEcCurrencyNum()))
                .findFirst().orElse(null);

            if (c == null) {
                currencys.add(compagnieCurrency.getEcCurrency());
            }

        }

        return currencys;
    }
}
