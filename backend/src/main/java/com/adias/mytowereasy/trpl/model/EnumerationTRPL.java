package com.adias.mytowereasy.trpl.model;

import com.adias.mytowereasy.util.GenericEnum;


public class EnumerationTRPL {
    public enum GrilleTransportCalculationMode implements GenericEnum {
        WEIGHT_SLICE_FIXED(0, "WEIGHT_SLICE_FIXED"),
        WEIGHT_SLICE(1, "WEIGHT_SLICE"),
        QUANTITY_SLICE(2, "QUANTITY_SLICE"),
        FIXED_ONLY(3, "FIXED_ONLY"),
        VOLUME_SLICE(4, "VOLUME_SLICE"),
        QUANTITY_SLICE_FIXED(5, "QUANTITY_SLICE_FIXED"),
        DAY_SLICE(6, "DAY_SLICE"),
        VOLUME_SLICE_FIXED(7, "VOLUME_SLICE_FIXED"),
        TYPE_UNIT(8, "TYPE_UNIT");

        private Integer code;
        private String key;

        private GrilleTransportCalculationMode(Integer code, String key) {
            this.code = code;
            this.key = key;
        }

        @Override
        public Integer getCode() {
            return code;
        }

        @Override
        public String getKey() {
            return key;
        }
    }
}
