package com.adias.mytowereasy.trpl.service;

import java.util.List;

import com.adias.mytowereasy.trpl.dto.EbPlTrancheDTO;
import com.adias.mytowereasy.trpl.model.EbPlTranche;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface TrancheService {
    EbPlTranche saveTranche(EbPlTrancheDTO trancheDto);

    Boolean deleteTranche(Integer trancheNum);

    List<EbPlTranche> listTranche(SearchCriteria criteria);

    Long countTranche(SearchCriteria criteria);

    List<EbPlTrancheDTO> convertEntitiesToDTOs(List<EbPlTranche> dbList);
}
