package com.adias.mytowereasy.trpl.dao;

import java.util.List;

import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoPlZone {
    List<EbPlZone> listZone(SearchCriteria criteria);

    Long countListZone(SearchCriteria criteria);
}
