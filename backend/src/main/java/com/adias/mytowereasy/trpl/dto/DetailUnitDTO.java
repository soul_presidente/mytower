package com.adias.mytowereasy.trpl.dto;

public class DetailUnitDTO {
    private Double weight;
    private Double volume;
    private Integer nombre;
    private Integer day;
    private Long ebTypeUnitNum;
    private Integer currencyCibleNum;
    private Double calculatedPrice = 0.0;

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getVolume() {
        return volume;
    }

    public void setVolume(Double volume) {
        this.volume = volume;
    }

    public Integer getNombre() {
        return nombre;
    }

    public void setNombre(Integer nombre) {
        this.nombre = nombre;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Long getEbTypeUnitNum() {
        return ebTypeUnitNum;
    }

    public void setEbTypeUnitNum(Long ebTypeUnitNum) {
        this.ebTypeUnitNum = ebTypeUnitNum;
    }

    public Integer getCurrencyCibleNum() {
        return currencyCibleNum;
    }

    public void setCurrencyCibleNum(Integer currencyCibleNum) {
        this.currencyCibleNum = currencyCibleNum;
    }

    public Double getCalculatedPrice() {
        return calculatedPrice;
    }

    public void setCalculatedPrice(Double calculatedPrice) {
        this.calculatedPrice = calculatedPrice;
    }
}
