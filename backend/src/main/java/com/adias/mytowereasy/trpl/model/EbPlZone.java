package com.adias.mytowereasy.trpl.model;

import javax.persistence.*;

import com.querydsl.core.annotations.QueryProjection;

import com.adias.mytowereasy.model.EbCompagnie;


@Entity
@Table(name = "eb_pl_zone", schema = "work")
public class EbPlZone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "eb_zone_num")
    private Integer ebZoneNum;

    @Column
    private String ref;

    @Column
    private String designation;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "x_eb_company", insertable = true)
    private EbCompagnie ebCompagnie;

    private Boolean deleted = false;

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public EbPlZone() {
    }

    // Constructors for QueryDSL

    public EbPlZone(Integer ebZoneNum) {
        this.ebZoneNum = ebZoneNum;
    }

    @QueryProjection
    public EbPlZone(
        Integer ebZoneNum,
        String ref,
        String designation,
        Integer ebCompangieNum,
        String ebCompagnieName,
        String ebCompagnieCode) {
        this.ebZoneNum = ebZoneNum;
        this.ref = ref;
        this.designation = designation;

        this.ebCompagnie = new EbCompagnie(ebCompangieNum, ebCompagnieName, ebCompagnieCode);
    }

    // End Constructors for QueryDSL

    public Integer getEbZoneNum() {
        return ebZoneNum;
    }

    public String getRef() {
        return ref;
    }

    public void setEbZoneNum(Integer ebZoneNum) {
        this.ebZoneNum = ebZoneNum;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((designation == null) ? 0 : designation.hashCode());
        result = prime * result + ((ebZoneNum == null) ? 0 : ebZoneNum.hashCode());
        result = prime * result + ((ref == null) ? 0 : ref.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        EbPlZone other = (EbPlZone) obj;

        if (designation == null) {
            if (other.designation != null) return false;
        }
        else if (!designation.equals(other.designation)) return false;

        if (ebZoneNum == null) {
            if (other.ebZoneNum != null) return false;
        }
        else if (!ebZoneNum.equals(other.ebZoneNum)) return false;

        if (ref == null) {
            if (other.ref != null) return false;
        }
        else if (!ref.equals(other.ref)) return false;

        return true;
    }
}
