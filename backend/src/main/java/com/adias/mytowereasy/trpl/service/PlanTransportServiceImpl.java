package com.adias.mytowereasy.trpl.service;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.dto.PricingCarrierSearchResultDTO;
import com.adias.mytowereasy.model.EbCompagnieCurrency;
import com.adias.mytowereasy.model.EbRelation;
import com.adias.mytowereasy.model.EbTypeGoods;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.EtatRelation;
import com.adias.mytowereasy.mtc.service.ValorizationService;
import com.adias.mytowereasy.service.CurrencyService;
import com.adias.mytowereasy.service.MyTowerService;
import com.adias.mytowereasy.trpl.dto.DetailUnitDTO;
import com.adias.mytowereasy.trpl.dto.EbPlTransportDTO;
import com.adias.mytowereasy.trpl.model.*;
import com.adias.mytowereasy.trpl.model.EnumerationTRPL.GrilleTransportCalculationMode;
import com.adias.mytowereasy.trpl.repository.EbPlCostItemRepository;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Service
public class PlanTransportServiceImpl extends MyTowerService implements PlanTransportService {
    @Autowired
    CurrencyService currencyService;
    @Autowired
    ValorizationService valorizationService;
    @Autowired
    EbPlCostItemRepository ebPlCostItemRepository;

    @Override
    public EbPlTransport savePlan(EbPlTransportDTO dto) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        EbPlTransport ebPlanTransport = new EbPlTransport();
        ebPlanTransport.setEbCompagnie(connectedUser.getEbCompagnie());
        ebPlanTransport.setComment(dto.getComment());
        ebPlanTransport.setDesignation(dto.getDesignation());

        ebPlanTransport.setDestination(null);

        if (dto.getDestination() != null) {
            ebPlanTransport.setDestination(new EbPlZone(dto.getDestination().getEbZoneNum()));
        }

        ebPlanTransport.setEbPlanTransportNum(dto.getEbPlanTransportNum());

        ebPlanTransport.setGrilleTarif(null);

        if (dto.getGrilleTarif() != null && dto.getGrilleTarif().getEbGrilleTransportNum() != null) {
            ebPlanTransport.setGrilleTarif(new EbPlGrilleTransport(dto.getGrilleTarif().getEbGrilleTransportNum()));
        }

        ebPlanTransport.setIsDangerous(dto.getIsDangerous());

        ebPlanTransport.setModeTransport(dto.getModeTransport());
        ebPlanTransport.setTypeTransport(dto.getTypeTransport());

        ebPlanTransport.setOrigine(null);

        if (dto.getOrigine() != null) {
            ebPlanTransport.setOrigine(new EbPlZone(dto.getOrigine().getEbZoneNum()));
        }

        ebPlanTransport.setxEbTypeGoods(null);

        if (dto.getTypeGoodsNum() != null) {
            ebPlanTransport.setxEbTypeGoods(new EbTypeGoods(dto.getTypeGoodsNum()));
        }

        ebPlanTransport.setReference(dto.getReference());
        ebPlanTransport.setTransitTime(dto.getTransitTime());
        ebPlanTransport.setTypeDemande(dto.getTypeDemande());

        return ebPlanTransportRepository.save(ebPlanTransport);
    }

    @Override
    public Boolean deletePlan(Integer planTransportNum) {
        ebPlanTransportRepository.deleteById(planTransportNum);
        return true;
    }

    @Override
    public List<EbPlTransport> listPlan(SearchCriteriaPlanTransport searchCriteria) {
        EbUser connectedUser = connectedUserService.getCurrentUser();
        searchCriteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
        searchCriteria.setDatePickup(LocalDate.now());
        List<EbPlTransport> listPl = daoPlanTransport.searchPlanTransport(searchCriteria);
        listPl.forEach(p -> selectValidGrid(searchCriteria, p));
        return listPl;
    }

    @Override
    public List<EbPlTransportDTO> convertEntitiesToDTOs(List<EbPlTransport> dbs) {
        List<EbPlTransportDTO> dtos = new ArrayList<>();

        if (dbs != null) {
            dbs.forEach(dbi -> {
                dtos.add(new EbPlTransportDTO(dbi));
            });
        }

        return dtos;
    }

    @Override
    public List<PricingCarrierSearchResultDTO>
        searchCarrier(SearchCriteriaPlanTransport searchCriteria, boolean isFromMtc) throws JsonProcessingException {

        if (isFromMtc) {
            return valorizationService.searchCarrierFromMtc(searchCriteria);
        }
        else {
            return searchCarrier(searchCriteria);
        }

    }

    @Override
    public List<PricingCarrierSearchResultDTO> searchCarrier(SearchCriteriaPlanTransport searchCriteria) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        List<EbPlTransport> transportPlans = daoPlanTransport.searchPlanTransport(searchCriteria);

        List<EbCompagnieCurrency> currencies = getCompanyCurrencies(connectedUser);

        List<Long> typeUnitNumList = searchCriteria.getListUnit()
                .stream()
                .map(DetailUnitDTO::getEbTypeUnitNum)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        List<PricingCarrierSearchResultDTO> carrierPricingDTOList = transportPlans.stream()
                .filter(transportPlan -> isTransportationPlanZonesMatchTransportRequestZone(transportPlan, searchCriteria)
                        && Objects.nonNull(transportPlan.getGrilleTarif()))
                .peek(transportPlan -> selectValidGrid(searchCriteria, transportPlan))
                .filter(this::hasCalculatedPrice)
                .map(transportPlan -> getCarrierPricing(transportPlan, searchCriteria, currencies, typeUnitNumList))
                .filter(pricing -> pricing.getCalculatedPrice().compareTo(BigDecimal.ZERO) > 0)
                .collect(Collectors.toList());

        return filterCarriersOnRelation(searchCriteria, carrierPricingDTOList).stream().distinct().collect(Collectors.toList());
    }

    private void selectValidGrid(SearchCriteriaPlanTransport searchCriteria, EbPlTransport transportPlan) {
        List<EbPlGrilleTransport> grids = ebPlGrilleTransportRepository
                .getValidGridByOriginGridNum(transportPlan.getGrilleTarif().getEbGrilleTransportNum(), searchCriteria.getDatePickup());

        if (!grids.isEmpty()) {
            transportPlan.setGrilleTarif(grids.get(0));
        }
    }

    private PricingCarrierSearchResultDTO getCarrierPricing(
            EbPlTransport transportPlan,
            SearchCriteriaPlanTransport searchCriteria,
            List<EbCompagnieCurrency> currencies,
            List<Long> typeUnitNumList) {
        PricingCarrierSearchResultDTO carrierPricingDTO = buildCarrierPricingDTO(transportPlan);

        // First check for currency
        Double defaultExchangeRate = 0d;

        defaultExchangeRate = manageCurrencyRate(searchCriteria, currencies, transportPlan, carrierPricingDTO, defaultExchangeRate);
        List<EbPlCostItem> listEbPlCost = new ArrayList<>();

        for (EbPlCostItem costItem: transportPlan.getGrilleTarif().getCostItems()) {
            Double newExchangeRate = defaultExchangeRate;

            newExchangeRate = findNewExchangeRate(searchCriteria, currencies, transportPlan, carrierPricingDTO, costItem, newExchangeRate);

            costItem.setEuroExchangeRate(newExchangeRate);
            ebPlCostItemRepository
                    .updateEuroExchangeRateEbPlCostItem(
                            costItem.getCostItemNum(),
                            costItem.getEuroExchangeRate());

            if (costItem.getTypeUnit() == null || (costItem.getTypeUnit() != null && !typeUnitNumList.isEmpty()
                    && typeUnitNumList.contains(costItem.getTypeUnit().getEbTypeUnitNum()))) {
                final int calculationMode = costItem.getCalculationMode();
                BigDecimal amountPlanTransport = new BigDecimal("0.0");
                BigDecimal amountCostItem = new BigDecimal("0.0");
                BigDecimal amountCostItemWithoutFixedFees = new BigDecimal("0.0");

                DetailUnitDTO dtUnit = new DetailUnitDTO();

                if (Objects.isNull(costItem.getTypeUnit())) {
                    // Calcul par raport au totaux
                    dtUnit.setVolume(searchCriteria.getTotalVolume());
                    dtUnit.setWeight(searchCriteria.getTotalWeight());
                    dtUnit.setNombre(searchCriteria.getTotalUnit());
                    dtUnit.setDay(searchCriteria.getTotalDay());
                    findCostItemPriceAndApplyExchangeRate(typeUnitNumList, listEbPlCost, costItem, newExchangeRate, calculationMode, amountPlanTransport, amountCostItem, amountCostItemWithoutFixedFees, dtUnit);
                }
                else {
                    // Calcul par ligne de colisage
                    for (DetailUnitDTO dtUnitDto: searchCriteria.getListUnit()) {
                        dtUnit = dtUnitDto;
                        amountCostItem = BigDecimal.ZERO;
                        amountPlanTransport = BigDecimal.ZERO;
                        findCostItemPriceAndApplyExchangeRate(typeUnitNumList, listEbPlCost, costItem, newExchangeRate, calculationMode, amountPlanTransport, amountCostItem, amountCostItemWithoutFixedFees, dtUnit);
                    }
                }
            }

        }

					carrierPricingDTO.setListPlCostItem(listEbPlCost);
					BigDecimal finalCost = BigDecimal.ZERO;

					if (checkPriceValidity(listEbPlCost))
					{
						// sum
						finalCost = listEbPlCost
							.stream()
							.map(EbPlCostItem::getPrice)
							.reduce(BigDecimal.ZERO, BigDecimal::add);
					}
					carrierPricingDTO.setCalculatedPrice(finalCost);

        return carrierPricingDTO;
    }

		/**
		 * TODO : Revenir sur l'utilité de cette fonction, faire evoluer le modele et la supprimé.
		 * 
		 * @param listEbPlCost
		 * @return
		 */
		private boolean checkPriceValidity(List<EbPlCostItem> listEbPlCost)
		{
			boolean allCostItemIsFixedAndNotHaveTypeOfUnit = false;
			
			if (listEbPlCost.size() > 1)
			{
				allCostItemIsFixedAndNotHaveTypeOfUnit = listEbPlCost
					.stream()
					.anyMatch(
						costItem -> (costItem
							.getCalculationMode()
							.equals(GrilleTransportCalculationMode.FIXED_ONLY.getCode()) &&
							Objects.isNull(costItem.getTypeUnit()))
					);
			}
					
			boolean anyCostItemIsFixedAndHavePriceOrFixedAndHaveTypeOfUnit =  listEbPlCost
				.stream()
				.anyMatch(
					costItem -> (!costItem
						.getCalculationMode()
						.equals(GrilleTransportCalculationMode.FIXED_ONLY.getCode()) &&
						costItem.getPrice().compareTo(BigDecimal.ZERO) > 0) ||
						(costItem
							.getCalculationMode()
							.equals(GrilleTransportCalculationMode.FIXED_ONLY.getCode()) &&
							Objects.nonNull(costItem.getTypeUnit()))
				);
			
			return allCostItemIsFixedAndNotHaveTypeOfUnit ||
				anyCostItemIsFixedAndHavePriceOrFixedAndHaveTypeOfUnit;
		}

    private boolean hasCalculatedPrice(EbPlTransport transportPlan) {
        return Objects.nonNull(transportPlan.getGrilleTarif()) && Objects.nonNull(transportPlan.getGrilleTarif().getTransporteur())
                && CollectionUtils.isNotEmpty(transportPlan.getGrilleTarif().getCostItems());
    }

    private boolean isTransportationPlanZonesMatchTransportRequestZone(EbPlTransport transportPlan, SearchCriteriaPlanTransport searchCriteria) {

        return transportPlan.getOrigine().getEbZoneNum().equals(searchCriteria.getEbZoneNumOrigin())
                && transportPlan.getDestination().getEbZoneNum().equals(searchCriteria.getEbZoneNumDest());
    }

    private void findCostItemPriceAndApplyExchangeRate(List<Long> listTypeUnitNum, List<EbPlCostItem> listEbPlCost, EbPlCostItem costItem, Double newExchangeRate, int calculationMode, BigDecimal amountPlanTransport, BigDecimal amountCostItem, BigDecimal amountCostItemWithoutFixedFees, DetailUnitDTO dtUnit) {
        amountCostItem = computeCostItemPrice(
                listTypeUnitNum,
                costItem,
                calculationMode,
                amountCostItem,
                amountCostItemWithoutFixedFees,
                dtUnit);
        amountPlanTransport = setCostItemPriceAndApplyExchangeRate(
                amountPlanTransport,
                listEbPlCost,
                costItem,
                newExchangeRate,
                amountCostItem);
    }

    private List<PricingCarrierSearchResultDTO> filterCarriersOnRelation(SearchCriteriaPlanTransport searchCriteria, List<PricingCarrierSearchResultDTO> carrierPricingDTOs) {
        if (searchCriteria.getEbUserNum() != null) {
            List<Integer> listUserNum = carrierPricingDTOs
                .stream().map(PricingCarrierSearchResultDTO::getCarrierUserNum).collect(Collectors.toList());

            if (CollectionUtils.isNotEmpty(listUserNum)) {
                List<EbRelation> listRelation = ebRelationRepository
                    .findRelationsWithUser(searchCriteria.getEbUserNum(), listUserNum, EtatRelation.ACTIF.getCode());
                List<PricingCarrierSearchResultDTO> finalCarriersPricingList = new ArrayList<>();

                for (PricingCarrierSearchResultDTO carr: carrierPricingDTOs) {

                    for (EbRelation r: listRelation) {

                        if (r.getHost() != null && r.getHost().getEbUserNum().equals(carr.getCarrierUserNum())
                            || r.getGuest() != null && r.getGuest().getEbUserNum().equals(carr.getCarrierUserNum())) {
                            finalCarriersPricingList.add(carr);
                            break;
                        }
                    }
                }
                carrierPricingDTOs = finalCarriersPricingList;
            }
        }
        return carrierPricingDTOs;
    }

    private Double findNewExchangeRate(SearchCriteriaPlanTransport searchCriteria, List<EbCompagnieCurrency> currencies, EbPlTransport transportPlan, PricingCarrierSearchResultDTO carrierPricingDTO, EbPlCostItem costItem, Double newExchangeRate) {
        if (costItem.getCurrency() != null && !costItem
            .getCurrency().getEcCurrencyNum().equals(transportPlan.getGrilleTarif().getCurrency().getEcCurrencyNum())) {

            for (EbCompagnieCurrency companyCurrency: currencies) {
                if (isCurrencyConstraints(searchCriteria, transportPlan, costItem, companyCurrency)) {
                    newExchangeRate = companyCurrency.getEuroExchangeRate();
                    carrierPricingDTO.setExchangeRateFound(true);
                }
            }

        }
        return newExchangeRate;
    }

    private Double manageCurrencyRate(SearchCriteriaPlanTransport searchCriteria, List<EbCompagnieCurrency> currencies, EbPlTransport transportPlan, PricingCarrierSearchResultDTO carrierPricingDTO, Double defaultExchangeRate) {
        if (transportPlan.getGrilleTarif().getCurrency().getEcCurrencyNum().equals(searchCriteria.getCurrencyCibleNum())) {
            defaultExchangeRate = 1d;
            carrierPricingDTO.setExchangeRateFound(true);
        }
        else {
            for (EbCompagnieCurrency c: currencies) {
                if (isCurrencyConstraint(searchCriteria, transportPlan, c)) {
                    defaultExchangeRate = c.getEuroExchangeRate();
                    carrierPricingDTO.setExchangeRateFound(true);
                }
            }
        }
        return defaultExchangeRate;
    }

    private boolean isCurrencyConstraint(SearchCriteriaPlanTransport searchCriteria, EbPlTransport transportPlan, EbCompagnieCurrency ebCompagnieCurrency) {
		    final boolean hasSameCurrancy = ebCompagnieCurrency.getEcCurrency().getEcCurrencyNum().equals(transportPlan.getGrilleTarif().getCurrency().getEcCurrencyNum()) ;
		    final boolean hasSameCurrancyCible = ebCompagnieCurrency.getXecCurrencyCible().getEcCurrencyNum().equals(searchCriteria.getCurrencyCibleNum()) ;
		    final boolean hasSameCompagnie = ebCompagnieCurrency.getEbCompagnieGuest().getEbCompagnieNum().equals(transportPlan.getGrilleTarif().getTransporteur().getEbCompagnie().getEbCompagnieNum()) ;
		    final boolean hasInRange = ebCompagnieCurrency.getDateDebut().after(Date.from(Instant.from(transportPlan.getGrilleTarif().getDateDebut().atStartOfDay(ZoneId.systemDefault()))));
        return hasSameCurrancy && hasSameCurrancyCible && hasSameCompagnie && hasInRange ;
    }

    private PricingCarrierSearchResultDTO buildCarrierPricingDTO(EbPlTransport p) {
        PricingCarrierSearchResultDTO rItem = new PricingCarrierSearchResultDTO();

        rItem.setModeTransport(p.getModeTransport());
        rItem.setRefGrille(p.getGrilleTarif().getReference());
        rItem.setCarrierUserNum(p.getGrilleTarif().getTransporteur().getEbUserNum());
        rItem.setCarrierName(p.getGrilleTarif().getTransporteur().getNom());
        rItem.setCarrierEmail(p.getGrilleTarif().getTransporteur().getEmail());

        if (p.getGrilleTarif().getTransporteur().getEbEtablissement() != null) {
            rItem
                .setCarrierEtabNum(
                    p.getGrilleTarif().getTransporteur().getEbEtablissement().getEbEtablissementNum());
            rItem.setCarrierEtabName(p.getGrilleTarif().getTransporteur().getEbEtablissement().getNom());
            rItem
                .setCarrierCompanyNum(
                    p
                        .getGrilleTarif().getTransporteur().getEbEtablissement().getEbCompagnie()
                        .getEbCompagnieNum());
            rItem
                .setCarrierCompanyName(
                    p.getGrilleTarif().getTransporteur().getEbEtablissement().getEbCompagnie().getNom());
        }

        rItem.setTransitTime(p.getTransitTime());
        rItem.setPlanNum(p.getEbPlanTransportNum());
        rItem.setPlanRef(p.getReference());
        rItem.setExchangeRateFound(false);
        rItem.setComment(p.getComment() != null ? p.getComment() : "");
        rItem.setDangerous(p.getIsDangerous());
        rItem.setTypeDemande(p.getTypeDemande());
        return rItem;
    }

    private List<EbCompagnieCurrency> getCompanyCurrencies(EbUser connectedUser) {
        SearchCriteria currencySearchCriteria = new SearchCriteria();
        currencySearchCriteria.setCommunityCompany(true);
        currencySearchCriteria.setEbEtablissementNum(connectedUser.getEbUserNum());
        currencySearchCriteria.setEbCompagnieNum(connectedUser.getEbCompagnie().getEbCompagnieNum());
        return currencyService
            .selectListExEtablissementCurrencyByEtablissementNum(currencySearchCriteria);
    }

    private BigDecimal setCostItemPriceAndApplyExchangeRate(
        BigDecimal amountPlanTransport,
        List<EbPlCostItem> listEbPlCost,
        EbPlCostItem costItem,
        Double exchangeRate,
        BigDecimal amountCostItem) {

        if (amountCostItem.compareTo(BigDecimal.ZERO) > 0) {
            boolean found = false;

            for (EbPlCostItem ebPlCost: listEbPlCost) {

                if (ebPlCost.getType().equals(costItem.getType())) {
                    ebPlCost
                        .setPrice(
                            (ebPlCost
                                .getPrice()
                                .add((amountCostItem.multiply(BigDecimal.valueOf(exchangeRate)))))
                                    .multiply(new BigDecimal(100)).divide(new BigDecimal(100)));
                    found = true;
                    break;
                }

            }

            if (!found) {
                costItem.setPrice(amountCostItem.multiply(BigDecimal.valueOf(exchangeRate)));
                listEbPlCost.add(costItem);
            }
        }

        return amountPlanTransport.add((amountCostItem.multiply(BigDecimal.valueOf(exchangeRate))));
    }

    private BigDecimal computeCostItemPrice(
        List<Long> listTypeUnitNum,
        EbPlCostItem costItem,
        int calculationMode,
        BigDecimal amountCostItem,
        BigDecimal amountCostItemWithoutFixedFees,
        DetailUnitDTO dtUnit) {

        if (!GrilleTransportCalculationMode.FIXED_ONLY.getCode().equals(calculationMode)) {
            amountCostItem = costItem.getFixedFees();

            for (EbPlLigneCostItemTranche trancheValue: costItem.getTrancheValues()) {
                Double tmpAmount = null;

                final boolean isWeightSliceFixed = GrilleTransportCalculationMode.WEIGHT_SLICE_FIXED.getCode().equals(calculationMode);
                final boolean isWeightSlice = GrilleTransportCalculationMode.WEIGHT_SLICE.getCode().equals(calculationMode);
                final boolean isQuantitySlice = GrilleTransportCalculationMode.QUANTITY_SLICE.getCode().equals(calculationMode);
                final boolean isQuantitySliceFixed = GrilleTransportCalculationMode.QUANTITY_SLICE_FIXED.getCode().equals(calculationMode);
                final boolean isVolumeSlice = GrilleTransportCalculationMode.VOLUME_SLICE.getCode().equals(calculationMode);
                final boolean isFixedVolumeSlice = GrilleTransportCalculationMode.VOLUME_SLICE_FIXED.getCode().equals(calculationMode);
                final boolean isDaySlice = GrilleTransportCalculationMode.DAY_SLICE.getCode().equals(calculationMode);


                final Double minimumTrancheValue = trancheValue.getTranche().getMinimum();
                final Double maximumTrancheValue = trancheValue.getTranche().getMaximum();
                // Calcul par poids
                if (dtUnit.getWeight() != null && isWeightCalculationModeAndWeightIsInTrancheRange(dtUnit, trancheValue, isWeightSliceFixed, isWeightSlice, minimumTrancheValue, maximumTrancheValue)) {
                    if (isWeightSlice) tmpAmount = trancheValue.getValue() * dtUnit.getWeight();
                    if (isWeightSliceFixed) tmpAmount = trancheValue.getValue();
                }
                // Calcul par colis
                else if (dtUnit.getNombre() != null && isQuantityCalculationModeAndQuantityIsInTrancheRange(dtUnit, trancheValue, isQuantitySlice, isQuantitySliceFixed, minimumTrancheValue, maximumTrancheValue)) {
                        if (isQuantitySlice) tmpAmount = trancheValue.getValue() * dtUnit.getNombre();
                        if (isQuantitySliceFixed) tmpAmount = trancheValue.getValue();
                    }
                // Calcul par volume
                else if (dtUnit.getVolume() != null && isVolumeCalculationModeAndVolumeIsInTrancheRange(dtUnit, trancheValue, isVolumeSlice, isFixedVolumeSlice, minimumTrancheValue, maximumTrancheValue)) {
                            if (isVolumeSlice) tmpAmount = trancheValue.getValue() * dtUnit.getVolume();
                            if (isFixedVolumeSlice) tmpAmount = trancheValue.getValue();
                        }
                // Calcul par jour
                else if (dtUnit.getDay() != null && isDayCalculationModeAndDayIsInTrancheRange(dtUnit, trancheValue, isDaySlice, minimumTrancheValue, maximumTrancheValue)) {
                                tmpAmount = trancheValue.getValue() * dtUnit.getDay();
                            }
                // Calcul par type unit
                else if (dtUnit.getEbTypeUnitNum() != null && dtUnit.getNombre() != null
                                && trancheValue.getCostItem().getTypeUnit() != null && trancheValue.getValue() != null
                                && dtUnit.getEbTypeUnitNum().equals(trancheValue.getCostItem().getTypeUnit().getEbTypeUnitNum())
                                && GrilleTransportCalculationMode.TYPE_UNIT.getCode().equals(calculationMode)) {
                                tmpAmount = trancheValue.getValue() * dtUnit.getNombre();
                            }

                // La valeur doit être entre le min et le max
                if (tmpAmount != null && (BigDecimal.valueOf(tmpAmount).compareTo(amountCostItemWithoutFixedFees) < 0
                    || amountCostItemWithoutFixedFees.compareTo(BigDecimal.ZERO) == 0)) {

                    if (trancheValue.getCostItem().getMinFrees() != null && tmpAmount < trancheValue.getCostItem().getMinFrees()) {
                        amountCostItemWithoutFixedFees = BigDecimal.valueOf(trancheValue.getCostItem().getMinFrees());
                    }
                    else if (trancheValue.getCostItem().getMaxFrees() != null && tmpAmount > trancheValue.getCostItem().getMaxFrees()) {
                        amountCostItemWithoutFixedFees = BigDecimal.valueOf(trancheValue.getCostItem().getMaxFrees());
                    }
                    else {
                        amountCostItemWithoutFixedFees = BigDecimal.valueOf(tmpAmount);
                    }

                }

            }

        }
        else {
            // Le plan est défini à fixed price only
            if (costItem.getTypeUnit() == null || (costItem.getTypeUnit() != null && !listTypeUnitNum.isEmpty()
                && listTypeUnitNum.contains(costItem.getTypeUnit().getEbTypeUnitNum()))) {
                if(costItem.getTypeUnit() != null && costItem.getTypeUnit().getEbTypeUnitNum().equals(dtUnit.getEbTypeUnitNum()))
                amountCostItem = costItem.getFixedFees().multiply(BigDecimal.valueOf(dtUnit.getNombre()));
                if(costItem.getTypeUnit() == null) amountCostItem = costItem.getFixedFees();
            }
        }

        return amountCostItem.add(amountCostItemWithoutFixedFees);
    }

    private boolean isDayCalculationModeAndDayIsInTrancheRange(DetailUnitDTO dtUnit, EbPlLigneCostItemTranche trancheValue, boolean isDaySlice, Double minimumTrancheValue, Double maximumTrancheValue) {
        return dtUnit.getDay() >= minimumTrancheValue
                && dtUnit.getDay() <= maximumTrancheValue && trancheValue.getValue() != null
                && isDaySlice;
    }

    private boolean isVolumeCalculationModeAndVolumeIsInTrancheRange(DetailUnitDTO dtUnit, EbPlLigneCostItemTranche trancheValue, boolean isVolumeSlice, boolean isFixedVolumeSlice, Double minimumTrancheValue, Double maximumTrancheValue) {
        return dtUnit.getVolume() >= minimumTrancheValue
                && dtUnit.getVolume() <= maximumTrancheValue && trancheValue.getValue() != null
                && (isVolumeSlice || isFixedVolumeSlice);
    }

    private boolean isQuantityCalculationModeAndQuantityIsInTrancheRange(DetailUnitDTO dtUnit, EbPlLigneCostItemTranche trancheValue, boolean isQuantitySlice, boolean isQuantitySliceFixed, Double minimumTrancheValue, Double maximumTrancheValue) {
        return dtUnit.getNombre() >= minimumTrancheValue
                && dtUnit.getNombre() <= maximumTrancheValue && trancheValue.getValue() != null
                && (isQuantitySlice || isQuantitySliceFixed);
    }

    private boolean isWeightCalculationModeAndWeightIsInTrancheRange(DetailUnitDTO dtUnit, EbPlLigneCostItemTranche trancheValue, boolean isWeightSliceFixed, boolean isWeightSlice, Double minimumTrancheValue, Double maximumTrancheValue) {
        return dtUnit.getWeight() >= minimumTrancheValue
                && dtUnit.getWeight() <= maximumTrancheValue && trancheValue.getValue() != null
                && (isWeightSliceFixed || isWeightSlice);
    }

    private boolean isCurrencyConstraints(
        SearchCriteriaPlanTransport searchCriteria,
        EbPlTransport p,
        EbPlCostItem costItem,
        EbCompagnieCurrency c) {
        final boolean isSameCurrencyWithCostCenter = c
            .getEcCurrency().getEcCurrencyNum().equals(costItem.getCurrency().getEcCurrencyNum());
        final boolean targetCurrencyIsSameWithTR = c
            .getXecCurrencyCible().getEcCurrencyNum()
            .equals(searchCriteria.getEbDemande().getXecCurrencyInvoice().getEcCurrencyNum());

        final boolean isSameCompanyWithTR = c
            .getEbCompagnie().getEbCompagnieNum()
            .equals(searchCriteria.getEbDemande().getUser().getEbCompagnie().getEbCompagnieNum());

        final boolean guestCompanyIsSameWithCarrierCompany = c
            .getEbCompagnieGuest().getEbCompagnieNum()
            .equals(p.getGrilleTarif().getTransporteur().getEbCompagnie().getEbCompagnieNum());
        // today date is in range of currency begin date and end date.
        final Date beginCurrencyDate = c.getDateDebut();
        final Date endedCurrencyDate = c.getDateFin();
        final Date todayDate = new Date();
        boolean isGoodDateRange = todayDate.after(beginCurrencyDate);
        if (endedCurrencyDate != null) isGoodDateRange = isGoodDateRange && todayDate.before(endedCurrencyDate);

        return isSameCurrencyWithCostCenter && targetCurrencyIsSameWithTR && isSameCompanyWithTR
            && guestCompanyIsSameWithCarrierCompany && isGoodDateRange;
    }

    private Boolean checkValidityOfPrice(List<EbPlCostItem> listCalculatedCost, List<EbPlCostItem> listGridCost) {
        boolean found = false;

        for (EbPlCostItem gridCost: listGridCost) {

            for (EbPlCostItem calculatedCost: listCalculatedCost) {

                if (gridCost.getType().equals(calculatedCost.getType())) {
                    found = true;
                    break;
                }

            }

            if (found) {
                found = false;
            }
            else return false;

        }

        return true;
    }

    @Override
    public Long getListEbPlanTransportCount(SearchCriteriaPlanTransport criteria) {
        return daoPlanTransport.getListEbPlanTransportCount(criteria);
    }

    @Override
    public List<EbPlZone> getListZones() {
        return ebZoneRepository.findAll();
    }

}
