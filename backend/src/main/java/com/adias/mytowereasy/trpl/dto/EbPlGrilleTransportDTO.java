package com.adias.mytowereasy.trpl.dto;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.adias.mytowereasy.dto.EcCurrencyDTO;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.trpl.model.EbPlGrilleTransport;


public class EbPlGrilleTransportDTO {
    private Integer ebGrilleTransportNum;
    private String reference;
    private String designation;
    private EcCurrencyDTO currency;
    private List<EbPlCostItemDTO> costItems;
    private Integer transporteurNum;
    private String transporteurNom;
    private String transporteurPrenom;
    private EbUser transporteur;
    private LocalDate dateDebut;
    private LocalDate  dateFin;
    private Integer origineGrlNum;

    public EbPlGrilleTransportDTO(EbPlGrilleTransport gt) {
        this.ebGrilleTransportNum = gt.getEbGrilleTransportNum();
        this.reference = gt.getReference();
        this.designation = gt.getDesignation();
        this.dateDebut = gt.getDateDebut();
        this.dateFin = gt.getDateFin();

        if (gt.getCurrency() != null) {
            this.currency = new EcCurrencyDTO(gt.getCurrency());
        }

        this.costItems = new ArrayList<>();

        if (gt.getCostItems() != null && !gt.getCostItems().isEmpty()) {
            gt.getCostItems().forEach(ci -> {
                this.costItems.add(new EbPlCostItemDTO(ci));
            });
        }

        if (gt.getTransporteur() != null) {
            this.transporteurNum = gt.getTransporteur().getEbUserNum();
            this.transporteurNom = gt.getTransporteur().getNom();
            this.transporteurPrenom = gt.getTransporteur().getPrenom();
        }

    }

    public EbPlGrilleTransportDTO() {
        setCostItems(new ArrayList<>());
    }

    public LocalDate  getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate  dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate  getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate  dateFin) {
        this.dateFin = dateFin;
    }

    public Integer getTransporteurNum() {
        return transporteurNum;
    }

    public void setTransporteurNum(Integer transporteurNum) {
        this.transporteurNum = transporteurNum;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Integer getEbGrilleTransportNum() {
        return ebGrilleTransportNum;
    }

    public void setEbGrilleTransportNum(Integer ebGrilleTransportNum) {
        this.ebGrilleTransportNum = ebGrilleTransportNum;
    }

    public EcCurrencyDTO getCurrency() {
        return currency;
    }

    public void setCurrency(EcCurrencyDTO currency) {
        this.currency = currency;
    }

    public List<EbPlCostItemDTO> getCostItems() {
        return costItems;
    }

    public void setCostItems(List<EbPlCostItemDTO> costItems) {
        this.costItems = costItems;
    }

    public String getTransporteurNom() {
        return transporteurNom;
    }

    public void setTransporteurNom(String transporteurNom) {
        this.transporteurNom = transporteurNom;
    }

    public String getTransporteurPrenom() {
        return transporteurPrenom;
    }

    public void setTransporteurPrenom(String transporteurPrenom) {
        this.transporteurPrenom = transporteurPrenom;
    }

    public EbUser getTransporteur() {
        return transporteur;
    }

    public void setTransporteur(EbUser transporteur) {
        this.transporteur = transporteur;
    }

    public Integer getOrigineGrlNum() {
        return origineGrlNum;
    }

    public void setOrigineGrlNum(Integer origineGrlNum) {
        this.origineGrlNum = origineGrlNum;
    }
}
