package com.adias.mytowereasy.trpl.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.adias.mytowereasy.model.EbTypeUnit;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.trpl.model.EbPlCostItem;


public class EbPlCostItemDTO {
    private Integer costItemNum;
    private BigDecimal fixedFees;
    private Integer calculationMode;

    private List<EbPlLigneCostItemTrancheDTO> trancheValues;

    private Integer type;

    private Integer startingPsl;

    private Integer endPsl;
    private Double minFrees;
    private Double maxFrees;
    private String comment;
    private EbTypeUnit typeUnit;
    private EcCurrency currency;
    private String code;

    private Double priceReal;

    private Double euroExchangeRateReal;

    private Double euroExchangeRate;

    private Double priceGab;

    private Double priceRealConverted;

    private Double priceGabConverted;

    private String libelle;

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public EbPlCostItemDTO(EbPlCostItem cost) {
        this.costItemNum = cost.getCostItemNum();
        this.fixedFees = cost.getFixedFees();
        this.calculationMode = cost.getCalculationMode();
        this.trancheValues = new ArrayList<>();
        this.type = cost.getType();
        this.minFrees = cost.getMinFrees();
        this.maxFrees = cost.getMaxFrees();
        this.comment = cost.getComment();
        this.typeUnit = cost.getTypeUnit();

        if (cost.getTrancheValues() != null && !cost.getTrancheValues().isEmpty()) {
            cost.getTrancheValues().forEach(tv -> {
                this.trancheValues.add(new EbPlLigneCostItemTrancheDTO(tv));
            });
        }

        this.currency = cost.getCurrency();

        if (cost.getCurrency() != null) {
            this.code = cost.getCurrency().getCode();
        }

        this.priceReal = cost.getPriceReal();
        this.euroExchangeRateReal = cost.getEuroExchangeRateReal();
        this.priceGab = cost.getPriceGab();
        this.priceRealConverted = cost.getPriceRealConverted();
        this.priceGabConverted = cost.getPriceGabConverted();
        this.euroExchangeRate = cost.getEuroExchangeRate();
    }

    public Integer getStartingPsl() {
        return startingPsl;
    }

    public void setStartingPsl(Integer startingPsl) {
        this.startingPsl = startingPsl;
    }

    public Integer getEndPsl() {
        return endPsl;
    }

    public void setEndPsl(Integer endPsl) {
        this.endPsl = endPsl;
    }

    public EbPlCostItemDTO() {
        this.trancheValues = new ArrayList<>();
    }

    public Integer getCostItemNum() {
        return costItemNum;
    }

    public void setCostItemNum(Integer costItemNum) {
        this.costItemNum = costItemNum;
    }

    public BigDecimal getFixedFees() {
        return fixedFees;
    }

    public void setFixedFees(BigDecimal fixedFees) {
        this.fixedFees = fixedFees;
    }

    public Integer getCalculationMode() {
        return calculationMode;
    }

    public void setCalculationMode(Integer calculationMode) {
        this.calculationMode = calculationMode;
    }

    public List<EbPlLigneCostItemTrancheDTO> getTrancheValues() {
        return trancheValues;
    }

    public void setTrancheValues(List<EbPlLigneCostItemTrancheDTO> trancheValues) {
        this.trancheValues = trancheValues;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Double getMinFrees() {
        return minFrees;
    }

    public void setMinFrees(Double minFrees) {
        this.minFrees = minFrees;
    }

    public Double getMaxFrees() {
        return maxFrees;
    }

    public void setMaxFrees(Double maxFrees) {
        this.maxFrees = maxFrees;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public EbTypeUnit getTypeUnit() {
        return typeUnit;
    }

    public void setTypeUnit(EbTypeUnit typeUnit) {
        this.typeUnit = typeUnit;
    }

    public EcCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(EcCurrency currency) {
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Double getPriceReal() {
        return priceReal;
    }

    public void setPriceReal(Double priceReal) {
        this.priceReal = priceReal;
    }

    public Double getEuroExchangeRateReal() {
        return euroExchangeRateReal;
    }

    public void setEuroExchangeRateReal(Double euroExchangeRateReal) {
        this.euroExchangeRateReal = euroExchangeRateReal;
    }

    public Double getPriceGab() {
        return priceGab;
    }

    public void setPriceGab(Double priceGab) {
        this.priceGab = priceGab;
    }

    public Double getPriceRealConverted() {
        return priceRealConverted;
    }

    public void setPriceRealConverted(Double priceRealConverted) {
        this.priceRealConverted = priceRealConverted;
    }

    public Double getPriceGabConverted() {
        return priceGabConverted;
    }

    public void setPriceGabConverted(Double priceGabConverted) {
        this.priceGabConverted = priceGabConverted;
    }

    public Double getEuroExchangeRate() {
        return euroExchangeRate;
    }

    public void setEuroExchangeRate(Double euroExchangeRate) {
        this.euroExchangeRate = euroExchangeRate;
    }
}
