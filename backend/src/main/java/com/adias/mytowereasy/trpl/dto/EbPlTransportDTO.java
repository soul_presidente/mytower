package com.adias.mytowereasy.trpl.dto;

import com.adias.mytowereasy.trpl.model.EbPlTransport;


public class EbPlTransportDTO {
    private Integer ebPlanTransportNum;
    private String reference;
    private String designation;
    private EbPlZoneDTO origine;
    private EbPlZoneDTO destination;
    private Integer typeDemande;
    private Integer modeTransport;
    private Integer typeTransport;
    private Boolean isDangerous;
    private Integer transitTime;
    private String comment;
    private EbPlGrilleTransportDTO grilleTarif;
    private Integer typeGoodsNum;

    public EbPlTransportDTO() {
    }

    public EbPlTransportDTO(EbPlTransport db) {
        this.ebPlanTransportNum = db.getEbPlanTransportNum();
        this.reference = db.getReference();
        this.designation = db.getDesignation();

        if (db.getOrigine() != null) {
            this.origine = new EbPlZoneDTO(db.getOrigine());
        }

        if (db.getDestination() != null) {
            this.destination = new EbPlZoneDTO(db.getDestination());
        }

        this.typeDemande = db.getTypeDemande();
        this.typeTransport = db.getTypeTransport();
        this.modeTransport = db.getModeTransport();
        this.isDangerous = db.getIsDangerous();
        this.transitTime = db.getTransitTime();
        this.comment = db.getComment();

        if (db.getGrilleTarif() != null) {
            this.grilleTarif = new EbPlGrilleTransportDTO(db.getGrilleTarif());
        }

        if (db.getxEbTypeGoods() != null) {
            this.typeGoodsNum = db.getxEbTypeGoods().getEbTypeGoodsNum();
        }

    }

    public Integer getEbPlanTransportNum() {
        return ebPlanTransportNum;
    }

    public void setEbPlanTransportNum(Integer ebPlanTransportNum) {
        this.ebPlanTransportNum = ebPlanTransportNum;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Integer getTypeDemande() {
        return typeDemande;
    }

    public void setTypeDemande(Integer typeDemande) {
        this.typeDemande = typeDemande;
    }

    public Integer getModeTransport() {
        return modeTransport;
    }

    public void setModeTransport(Integer modeTransport) {
        this.modeTransport = modeTransport;
    }

    public Boolean getIsDangerous() {
        return isDangerous;
    }

    public void setIsDangerous(Boolean isDangerous) {
        this.isDangerous = isDangerous;
    }

    public Integer getTransitTime() {
        return transitTime;
    }

    public void setTransitTime(Integer transitTime) {
        this.transitTime = transitTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public EbPlZoneDTO getOrigine() {
        return origine;
    }

    public void setOrigine(EbPlZoneDTO origine) {
        this.origine = origine;
    }

    public EbPlZoneDTO getDestination() {
        return destination;
    }

    public void setDestination(EbPlZoneDTO destination) {
        this.destination = destination;
    }

    public EbPlGrilleTransportDTO getGrilleTarif() {
        return grilleTarif;
    }

    public void setGrilleTarif(EbPlGrilleTransportDTO grilleTarif) {
        this.grilleTarif = grilleTarif;
    }

    public Integer getTypeGoodsNum() {
        return typeGoodsNum;
    }

    public void setTypeGoodsNum(Integer typeGoodsNum) {
        this.typeGoodsNum = typeGoodsNum;
    }

    public Integer getTypeTransport() {
        return typeTransport;
    }

    public void setTypeTransport(Integer typeTransport) {
        this.typeTransport = typeTransport;
    }
}
