package com.adias.mytowereasy.trpl.repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.repository.CommonRepository;
import com.adias.mytowereasy.trpl.model.EbPlGrilleTransport;


@Repository
public interface EbPlGrilleTransportRepository extends CommonRepository<EbPlGrilleTransport, Integer> {
    @Query("select count(1) from EbPlGrilleTransport gl  where (gl.ebGrilleTransportNum= ?1 or gl.origineGrl= ?1) and (gl.dateDebut is null or gl.dateFin is null or gl.dateDebut >= ?2 or gl.dateFin>= ?2 ) ")
    Long foundActiveGridVersion(Integer originGlNum, LocalDate dateDebut);

    // could find more than one grid for given date => we must return list
    @Query("select gl from EbPlGrilleTransport gl where (gl.ebGrilleTransportNum= ?1 or gl.origineGrl= ?1) and (gl.dateDebut is not null  and gl.dateDebut  <= ?2 and (gl.dateFin is null or (gl.dateFin is not null and gl.dateFin >= ?2 )))")
    List<EbPlGrilleTransport> getValidGridByOriginGridNum(Integer originGlNum, LocalDate datePickup);
}
