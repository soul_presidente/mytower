package com.adias.mytowereasy.trpl.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.impl.Dao;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.QEbCompagnie;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.trpl.model.EbPlTranche;
import com.adias.mytowereasy.trpl.model.QEbPlTranche;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoPlTrancheImpl extends Dao implements DaoPlTranche {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    QEbPlTranche qEbPlTranche = QEbPlTranche.ebPlTranche;
    QEbCompagnie qEbTrancheCompagnie = new QEbCompagnie("qEbTrancheCompagnie");

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbPlTranche> listTranche(SearchCriteria criteria) {
        List<EbPlTranche> resultat = new ArrayList<>();

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbPlTranche> query = new JPAQuery<>(em);

            query
                .select(
                    QEbPlTranche
                        .create(
                            qEbPlTranche.ebTrancheNum,
                            qEbPlTranche.minimum,
                            qEbPlTranche.maximum,
                            qEbTrancheCompagnie.ebCompagnieNum,
                            qEbTrancheCompagnie.nom,
                            qEbTrancheCompagnie.code));

            query = getTrancheGlobalWhere(query, where, criteria);
            query.where(where);
            query.from(qEbPlTranche);
            query = getTrancheGlobalJoin(query, where, criteria);

            if (criteria.getOrderedColumn() != null) {
                Path<Object> fieldPath = Expressions.path(Object.class, qEbPlTranche, criteria.getOrderedColumn());
                query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
            }
            else {
                query.orderBy(qEbPlTranche.ebTrancheNum.asc());
            }

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long countListTranche(SearchCriteria criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbPlTranche> query = new JPAQuery<EbPlTranche>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getTrancheGlobalWhere(query, where, criteria);
            query.where(where);
            query.from(qEbPlTranche);
            query = getTrancheGlobalJoin(query, where, criteria);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getTrancheGlobalWhere(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        EbUser connectedUser = connectedUserService.getCurrentUser();

        if (connectedUser.getEbCompagnie().getEbCompagnieNum() != null) {
            where.and(qEbTrancheCompagnie.ebCompagnieNum.eq(connectedUser.getEbCompagnie().getEbCompagnieNum()));
        }

        if (criteria.getSearchterm() != null) {
            String term = criteria.getSearchterm().toLowerCase();
            where
                .andAnyOf(
                    qEbPlTranche.minimum.stringValue().contains(term),
                    qEbPlTranche.maximum.stringValue().contains(term));
        }

        return query;
    }

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    private JPAQuery getTrancheGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteria criteria) {
        query.leftJoin(qEbPlTranche.ebCompagnie(), qEbTrancheCompagnie);
        return query;
    }
}
