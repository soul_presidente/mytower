package com.adias.mytowereasy.trpl.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.querydsl.core.BooleanBuilder;
import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.jpa.impl.JPAQuery;

import com.adias.mytowereasy.dao.impl.Dao;
import com.adias.mytowereasy.model.*;
import com.adias.mytowereasy.repository.EbTypeTransportRepository;
import com.adias.mytowereasy.service.CompagnieService;
import com.adias.mytowereasy.service.ConnectedUserService;
import com.adias.mytowereasy.service.TypeGoodsService;
import com.adias.mytowereasy.service.TypeRequestService;
import com.adias.mytowereasy.trpl.model.*;
import com.adias.mytowereasy.util.StringUtil;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@Component
@Transactional
public class DaoPlanTransportImpl extends Dao implements DaoPlanTransport {
    @PersistenceContext
    EntityManager em;

    @Autowired
    ConnectedUserService connectedUserService;

    @Autowired
    TypeRequestService typeRequestService;

    @Autowired
    CompagnieService compagnieService;

    @Autowired
    TypeGoodsService typeGoodsSearvice;

    @Autowired
    EbTypeTransportRepository ebTypeTransportRepository;

    QEbUser qEbUser = QEbUser.ebUser;
    QEbPlZone origine = new QEbPlZone("Origin");
    QEbPlZone destination = new QEbPlZone("Destination");
    QEbPlTransport qEbPlanTransport = QEbPlTransport.ebPlTransport;
    QEbPlGrilleTransport qebPlGrille = QEbPlGrilleTransport.ebPlGrilleTransport;
    QEbPlCostItem qEbPlCostItem = QEbPlCostItem.ebPlCostItem;
    QEbParty qEbParty = QEbParty.ebParty;
    QEcCurrency qEcCurrency = QEcCurrency.ecCurrency;
    QEbTypeGoods qEbTypeGoods = QEbTypeGoods.ebTypeGoods;
    QEbCompagnie qEbPlCompagnie = new QEbCompagnie("qEbPlCompagnie");

    QEbCompagnie qEbZoneOrigineCompagnie = new QEbCompagnie("qEbZoneOrigineCompagnie");
    QEbCompagnie qEbZoneDestinationCompagnie = new QEbCompagnie("qEbZoneDestinationCompagnie");

    @SuppressWarnings({
        "unchecked", "rawtypes"
    })
    @Override
    public List<EbPlTransport> searchPlanTransport(SearchCriteriaPlanTransport criteria) {
        List<EbPlTransport> resultat = new ArrayList<>();
        Boolean isFromPlTransportSetting = true;

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbPlTransport> query = new JPAQuery<>(em);

            query
                .select(
                    QEbPlTransport
                        .create(
                            qEbPlanTransport.ebPlanTransportNum,
                            qEbPlanTransport.reference,
                            qEbPlanTransport.designation,
                            origine,
                            destination,
                            qEbPlanTransport.typeDemande,
                            qEbPlanTransport.modeTransport,
                            qEbPlanTransport.typeTransport,
                            qEbPlanTransport.isDangerous,
                            qEbTypeGoods.ebTypeGoodsNum,
                            qEbPlanTransport.grilleTarif(),
                            qEbPlanTransport.transitTime,
                            qEbPlanTransport.comment,
                            qEbPlanTransport.tptt,
                            qEbPlanTransport.patt,
                            qEbPlanTransport.ebCompagnie().ebCompagnieNum));

            if (criteria.getModeTransport() != null) {
                isFromPlTransportSetting = false;
            }

            query = getPlanTransportGlobalWhere(query, where, criteria, isFromPlTransportSetting);
            query.distinct().from(qEbPlanTransport);
            query = getPlanTransportGlobalJoin(query, where, criteria);

            if (criteria.getOrderedColumn() != null) {

                if (criteria.getOrderedColumn().contains("typeGoodsNum")) {
                    query
                        .orderBy(
                            new OrderSpecifier(
                                criteria.getAscendant() ? Order.ASC : Order.DESC,
                                qEbTypeGoods.ebTypeGoodsNum));
                }
                else {
                    Path<Object> fieldPath = Expressions
                        .path(Object.class, qEbPlanTransport, criteria.getOrderedColumn());
                    query.orderBy(new OrderSpecifier(criteria.getAscendant() ? Order.ASC : Order.DESC, fieldPath));
                }

            }
            else {
                query.orderBy(qEbPlanTransport.reference.asc());
            }

            if (criteria.getStart() != null) {
                query.offset(criteria.getStart());
            }

            if (criteria.getSize() != null && criteria.getSize() >= 0) {
                query.limit(criteria.getSize());
            }

            if (criteria.getPageNumber() != null) {
                query.offset(criteria.getPageNumber() * (criteria.getSize() != null ? criteria.getSize() : 1));
            }

            resultat = query.fetch();
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

        return resultat;
    }

    @SuppressWarnings("unchecked")
    @Override
    public Long getListEbPlanTransportCount(SearchCriteriaPlanTransport criteria) {
        Long result = new Long(0);

        try {
            JPAQuery<EbPlTransport> query = new JPAQuery<>(em);
            BooleanBuilder where = new BooleanBuilder();

            query = getPlanTransportGlobalWhere(query, where, criteria, true);
            query.where(where).distinct();
            query.from(qEbPlanTransport);
            query = getPlanTransportGlobalJoin(query, where, criteria);

            result = query.fetchCount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    @SuppressWarnings({
        "rawtypes", "unchecked"
    })
    private JPAQuery
        getPlanTransportGlobalJoin(JPAQuery query, BooleanBuilder where, SearchCriteriaPlanTransport criteria) {
        query.leftJoin(qEbPlanTransport.ebCompagnie(), qEbPlCompagnie);
        query.leftJoin(qEbPlanTransport.origine(), origine);
        query.leftJoin(qEbPlanTransport.destination(), destination);
        query.leftJoin(qEbPlanTransport.xEbTypeGoods(), qEbTypeGoods);
        query.leftJoin(origine.ebCompagnie(), qEbZoneOrigineCompagnie);
        query.leftJoin(destination.ebCompagnie(), qEbZoneDestinationCompagnie);
        query.leftJoin(qEbPlanTransport.grilleTarif(), qebPlGrille);
        query.leftJoin(qebPlGrille.transporteur(), qEbUser);

        return query;
    }

    @SuppressWarnings("rawtypes")
    private JPAQuery getPlanTransportGlobalWhere(
        JPAQuery query,
        BooleanBuilder where,
        SearchCriteriaPlanTransport criteria,
        Boolean isFromPlTransportSetting) {

        if (criteria.getEbCompagnieNum() != null) {
            where.and(qEbPlCompagnie.ebCompagnieNum.eq(criteria.getEbCompagnieNum()));
        }

        if (criteria.getDangerous() != null) {
            where
                .and(
                    qEbPlanTransport.isDangerous.eq(criteria.getDangerous()).or(qEbPlanTransport.isDangerous.isNull()));
        }

        if (criteria.getEbDemande() != null && criteria.getEbDemande().getxEbTypeRequest() != null) {
            where
                .and(
                    qEbPlanTransport.typeDemande
                        .eq(criteria.getEbDemande().getxEbTypeRequest()).or(qEbPlanTransport.typeDemande.isNull()));
        }

        if (criteria.getEbZoneNumOrigin() != null) {
            where.and(qEbPlanTransport.origine().ebZoneNum.eq(criteria.getEbZoneNumOrigin()));
        }

        if (criteria.getEbZoneNumDest() != null) {
            where.and(qEbPlanTransport.destination().ebZoneNum.eq(criteria.getEbZoneNumDest()));
        }

        if (criteria.getListCarrier() != null && !criteria.getListCarrier().isEmpty()) {
            where.and(qebPlGrille.transporteur().ebUserNum.in(criteria.getListCarrier()));
        }

        if (criteria.getTypeGoodsNum() != null) {
            where.andAnyOf(qEbTypeGoods.ebTypeGoodsNum.eq(criteria.getTypeGoodsNum()),
                    qEbTypeGoods.ebTypeGoodsNum.isNull());
        }

        if (criteria.getTypeGoodsNum() == null && criteria.isForValorisation()) {
            where.and(qEbTypeGoods.ebTypeGoodsNum.isNull());
        }

        if (criteria.getModeTransportNonNullOnly() != null && criteria.getModeTransportNonNullOnly()) {
            where.and(qEbPlanTransport.modeTransport.isNotNull());
        }

        if (criteria.getModeTransport() != null) {
            where
                .and(
                    qEbPlanTransport.modeTransport
                        .eq(criteria.getModeTransport()).or(qEbPlanTransport.modeTransport.isNull()));
        }

        if (criteria.getTypeTransport() != null) {
            where
                .and(
                    qEbPlanTransport.typeTransport
                        .eq(criteria.getTypeTransport()).or(qEbPlanTransport.typeTransport.isNull()));
        }

        if (criteria.getEbTransporteurNum() != null) {
            where.and(qEbUser.ebUserNum.eq(criteria.getEbTransporteurNum()));
        }

        if (!isFromPlTransportSetting) {

            if (criteria.getTypeTransport() == null) {
                where.and(qEbPlanTransport.typeTransport.isNull());
            }

            if (criteria.getDangerous() == null) {
                where.and(qEbPlanTransport.isDangerous.isNull());
            }

        }

        if (StringUtils.isNotBlank(criteria.getSearchterm())) {
            String term = criteria.getSearchterm().toLowerCase();
            SearchCriteria criteriaRequest = new SearchCriteria();
            criteriaRequest.setSearchterm(term);
            List<EbTypeRequest> listTypeRequest = typeRequestService.searchTypeRequest(criteriaRequest);

            List<EbTypeGoods> listTypeGoods = typeGoodsSearvice.listTypeGoods(criteriaRequest);

            List<Integer> listTypeGoodsNum = listTypeGoods
                .stream().map(EbTypeGoods::getEbTypeGoodsNum).collect(Collectors.toList());

            List<Integer> listTypeTransport = ebTypeTransportRepository.findAllTypeTransportByLibelle(term);

            List<Integer> listTypeDemande = listTypeRequest
                .stream().map(EbTypeRequest::getEbTypeRequestNum).collect(Collectors.toList());
            List<Integer> listModeTransport = Enumeration.ModeTransport.getListCodeByLibelle(term);

            BooleanBuilder searchTermWhere = new BooleanBuilder();
            searchTermWhere
                .andAnyOf(
                    qEbPlanTransport.reference.containsIgnoreCase(term),
                    qEbPlanTransport.designation.containsIgnoreCase(term),
                    qEbPlanTransport.origine().ref.containsIgnoreCase(term),
                    qEbPlanTransport.origine().designation.containsIgnoreCase(term),
                    qEbPlanTransport.destination().ref.containsIgnoreCase(term),
                    qEbPlanTransport.destination().designation.containsIgnoreCase(term),
                    qEbPlanTransport.typeDemande.in(listTypeDemande),
                    qEbPlanTransport.transitTime.stringValue().contains(term),
                    qEbPlanTransport.comment.containsIgnoreCase(term),
                    qEbPlanTransport.modeTransport.in(listModeTransport),
                    qEbPlanTransport.typeTransport.in(listTypeTransport),
                    qEbTypeGoods.ebTypeGoodsNum.in(listTypeGoodsNum),
                    qEbPlanTransport.grilleTarif().reference.containsIgnoreCase(term),
                    qEbPlanTransport.grilleTarif().designation.containsIgnoreCase(term),
                    qEbPlanTransport.grilleTarif().transporteur().nom.containsIgnoreCase(term),
                    qEbPlanTransport.grilleTarif().transporteur().prenom.containsIgnoreCase(term));

            if (StringUtil.isStringYes(term)) searchTermWhere.or(qEbPlanTransport.isDangerous.isTrue());
            if (StringUtil.isStringNo(term)) searchTermWhere.or(qEbPlanTransport.isDangerous.isFalse());

            where.and(searchTermWhere);
        }

        query.where(where);
        return query;
    }

    @Override
    public EbPlTransport getPlanTransportByRef(String refPlan) {
        EbPlTransport plTr = null;

        try {
            BooleanBuilder where = new BooleanBuilder();
            JPAQuery<EbPlTransport> query = new JPAQuery<>(em);
            SearchCriteriaPlanTransport searchCriteriaPlanTransport = new SearchCriteriaPlanTransport();
            query.from(qEbPlanTransport).distinct();
            if (refPlan != null) where.and(qEbPlanTransport.reference.eq(refPlan));
            query.where(where);
            query = getPlanTransportGlobalJoin(query, where, searchCriteriaPlanTransport);
            plTr = query.fetchFirst();
            return plTr;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
