package com.adias.mytowereasy.trpl.dto;

import com.adias.mytowereasy.trpl.model.EbPlLigneCostItemTranche;


public class EbPlLigneCostItemTrancheDTO {
    private Integer ebPlLigneCostItemTrancheNum;
    private Double value;
    private Integer ebTrancheNum;

    public Double getValue() {
        return value;
    }

    public EbPlLigneCostItemTrancheDTO() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public EbPlLigneCostItemTrancheDTO(EbPlLigneCostItemTranche tranche) {
        this.ebPlLigneCostItemTrancheNum = tranche.getEbPlLigneCostItemTrancheNum();
        this.value = tranche.getValue();
        this.ebTrancheNum = tranche.getTranche().getEbTrancheNum();
    }

    public Integer getEbPlLigneCostItemTrancheNum() {
        return ebPlLigneCostItemTrancheNum;
    }

    public void setEbPlLigneCostItemTrancheNum(Integer ebPlLigneCostItemTranche) {
        this.ebPlLigneCostItemTrancheNum = ebPlLigneCostItemTranche;
    }

    public Integer getEbTrancheNum() {
        return ebTrancheNum;
    }

    public void setEbTrancheNum(Integer ebTrancheNum) {
        this.ebTrancheNum = ebTrancheNum;
    }
}
