package com.adias.mytowereasy.trpl.dto;

import com.adias.mytowereasy.trpl.model.EbPlZone;


public class EbPlZoneDTO {
    private Integer ebZoneNum;
    private String ref;
    private String designation;
    private String companyName;

    public EbPlZoneDTO() {
    }

    public EbPlZoneDTO(EbPlZone db) {
        this.ebZoneNum = db.getEbZoneNum();
        this.ref = db.getRef();
        this.designation = db.getDesignation();

        this.setCompanyName(db.getEbCompagnie() != null ? db.getEbCompagnie().getNom() : null);
    }

    public Integer getEbZoneNum() {
        return ebZoneNum;
    }

    public void setEbZoneNum(Integer ebZoneNum) {
        this.ebZoneNum = ebZoneNum;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
