package com.adias.mytowereasy.trpl.model;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCurrency;


@Entity
@Table(name = "eb_pl_grille_transport", schema = "work")
public class EbPlGrilleTransport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, unique = true)
    private Integer ebGrilleTransportNum;

    @Column
    private String designation;

    @Column(nullable = false, unique = true)
    private String reference;

    @Column private LocalDate  dateDebut;

    @Column private LocalDate  dateFin;

    @ManyToOne
    @JoinColumn(name = "origine_grl")
    private EbPlGrilleTransport origineGrl;

    @OneToMany(mappedBy = "grilleTransport", cascade = {
        CascadeType.REFRESH, CascadeType.REMOVE
    }, fetch = FetchType.LAZY)
    private List<EbPlCostItem> costItems;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "currency_id")
    private EcCurrency currency;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "x_eb_company", insertable = true)
    private EbCompagnie ebCompagnie;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "transporteur_id")
    private EbUser transporteur;

    public EbUser getTransporteur() {
        return transporteur;
    }

    public void setTransporteur(EbUser transporteur) {
        this.transporteur = transporteur;
    }

    public EbPlGrilleTransport() {
    }

    public EbPlGrilleTransport(Integer ebGrilleTransportNum) {
        this.ebGrilleTransportNum = ebGrilleTransportNum;
    }

    public Integer getEbGrilleTransportNum() {
        return ebGrilleTransportNum;
    }

    public void setEbGrilleTransportNum(Integer ebGrilleTransportNum) {
        this.ebGrilleTransportNum = ebGrilleTransportNum;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public EcCurrency getCurrency() {
        return currency;
    }

    public void setCurrency(EcCurrency currency) {
        this.currency = currency;
    }

    public EbCompagnie getEbCompagnie() {
        return ebCompagnie;
    }

    public void setEbCompagnie(EbCompagnie ebCompagnie) {
        this.ebCompagnie = ebCompagnie;
    }

    public List<EbPlCostItem> getCostItems() {
        return costItems;
    }

    public void setCostItems(List<EbPlCostItem> costItems) {
        this.costItems = costItems;
    }

    public LocalDate getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(LocalDate  dateDebut) {
        this.dateDebut = dateDebut;
    }

    public LocalDate  getDateFin() {
        return dateFin;
    }

    public void setDateFin(LocalDate  dateFin) {
        this.dateFin = dateFin;
    }

    public EbPlGrilleTransport getOrigineGrl() {
        return origineGrl;
    }

    public void setOrigineGrl(EbPlGrilleTransport origineGrl) {
        this.origineGrl = origineGrl;
    }
}
