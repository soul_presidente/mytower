package com.adias.mytowereasy.trpl.repository;

import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.repository.CommonRepository;
import com.adias.mytowereasy.trpl.model.EbPlTranche;


@Repository
public interface EbPlTrancheRepository extends CommonRepository<EbPlTranche, Integer> {

}
