package com.adias.mytowereasy.trpl.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.adias.mytowereasy.repository.CommonRepository;
import com.adias.mytowereasy.trpl.model.EbPlTransport;
import com.adias.mytowereasy.trpl.model.EbPlZone;


@Repository
public interface EbPlanTransportRepository extends CommonRepository<EbPlTransport, Integer> {
    List<EbPlTransport> findAllByEbCompagnie_EbCompagnieNum(Integer ebCompagnieNum);

    List<EbPlTransport> findAllByOrigineOrDestination(EbPlZone origine, EbPlZone destination);

    List<EbPlTransport> findAllByGrilleTarifEbGrilleTransportNum(Integer ebGrilleTransportNum);
}
