package com.adias.mytowereasy.trpl.service;

import java.util.List;

import com.adias.mytowereasy.trpl.dto.EbPlZoneDTO;
import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface ZoneService {
    public EbPlZone saveZone(EbPlZoneDTO ebZone);

    public Boolean deleteZone(Integer zoneNum);

    List<EbPlZone> listZone(SearchCriteria criteria);

    Long countZone(SearchCriteria criteria);

    List<EbPlZoneDTO> convertEntitiesToDTOs(List<EbPlZone> dbList);

    List<EbPlZone> listZoneWithCommunity();
}
