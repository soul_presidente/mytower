package com.adias.mytowereasy.trpl.dao;

import java.util.List;

import com.adias.mytowereasy.trpl.model.EbPlGrilleTransport;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public interface DaoPlGrilleTransport {
    List<EbPlGrilleTransport> listGrilleTransport(SearchCriteria criteria);

    Long countListGrilleTransport(SearchCriteria criteria);
}
