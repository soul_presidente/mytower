package com.adias.mytowereasy.trpl.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import com.adias.mytowereasy.controller.SuperControler;
import com.adias.mytowereasy.dao.DaoPrMtcTransporteur;
import com.adias.mytowereasy.dto.PricingCarrierSearchResultDTO;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.EcCurrency;
import com.adias.mytowereasy.mtc.service.MtcService;
import com.adias.mytowereasy.mtc.utils.AccessMtc;
import com.adias.mytowereasy.repository.EbCompagnieRepository;
import com.adias.mytowereasy.repository.PrMtcTransporteurRepository;
import com.adias.mytowereasy.service.PricingService;
import com.adias.mytowereasy.trpl.dto.EbPlGrilleTransportDTO;
import com.adias.mytowereasy.trpl.dto.EbPlTrancheDTO;
import com.adias.mytowereasy.trpl.dto.EbPlTransportDTO;
import com.adias.mytowereasy.trpl.dto.EbPlZoneDTO;
import com.adias.mytowereasy.trpl.model.EbPlTranche;
import com.adias.mytowereasy.trpl.model.EbPlTransport;
import com.adias.mytowereasy.trpl.model.EbPlZone;
import com.adias.mytowereasy.trpl.model.SearchCriteriaPlanTransport;
import com.adias.mytowereasy.utils.search.SearchCriteria;


@RestController
@RequestMapping("api/transportation-plan/")
public class TransportationPlanController extends SuperControler {
    @Autowired
    private MtcService mtcService;
    @Autowired
    private PricingService pricingService;
    @Autowired
    private AccessMtc accessMtc;

    @Autowired
    private PrMtcTransporteurRepository prMtcTransporteurRepository;
    @Autowired
    private DaoPrMtcTransporteur daoPrMtcTransporteur;
    @Autowired
    private EbCompagnieRepository ebCompagnieRepository;

    // ZONE
    @PostMapping(value = "list-zone")
    public ResponseEntity<List<EbPlZoneDTO>> getAllZones(SearchCriteria criteria) {
        List<EbPlZone> zones = zoneService.listZone(criteria);
        List<EbPlZoneDTO> zonesDTOs = zoneService.convertEntitiesToDTOs(zones);
        return new ResponseEntity<>(zonesDTOs, HttpStatus.OK);
    }

    @GetMapping(value = "list-zone-contact")
    public @ResponseBody List<EbPlZoneDTO> getZoneContacts() {
        List<EbPlZone> zones = zoneService.listZoneWithCommunity();
        List<EbPlZoneDTO> zonesDTOs = zoneService.convertEntitiesToDTOs(zones);
        return zonesDTOs;
    }

    @RequestMapping(value = "list-zone-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object> getListZoneTable(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbPlZone> zones = zoneService.listZone(criteria);
        List<EbPlZoneDTO> zonesDTOs = zoneService.convertEntitiesToDTOs(zones);
        Long count = zoneService.countZone(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", zonesDTOs);
        return result;
    }

    @PostMapping(value = "update-zone")
    public EbPlZoneDTO updateZone(@RequestBody EbPlZoneDTO ebZone) {
        return new EbPlZoneDTO(zoneService.saveZone(ebZone));
    }

    @DeleteMapping(value = "delete-zone")
    public Boolean deleteZone(@RequestParam Integer zoneNum) {
        return zoneService.deleteZone(zoneNum);
    }

    // Tranche

    @GetMapping(value = "list-tranche")
    public List<EbPlTrancheDTO> getAllTranches(SearchCriteria criteria) {
        List<EbPlTranche> dbs = trancheService.listTranche(criteria);
        List<EbPlTrancheDTO> dtos = trancheService.convertEntitiesToDTOs(dbs);
        return dtos;
    }

    @RequestMapping(value = "list-tranche-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object>
        getListTrancheTable(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbPlTranche> tranches = trancheService.listTranche(criteria);
        List<EbPlTrancheDTO> tranchesDTO = trancheService.convertEntitiesToDTOs(tranches);
        Long count = trancheService.countTranche(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", tranchesDTO);
        return result;
    }

    @PostMapping(value = "save-tranche")
    public EbPlTrancheDTO saveTranche(@RequestBody EbPlTrancheDTO ebTranche) {
        return new EbPlTrancheDTO(trancheService.saveTranche(ebTranche));
    }

    @DeleteMapping(value = "delete-tranche")
    public Boolean deleteTranche(@RequestParam Integer trancheNum) {
        return trancheService.deleteTranche(trancheNum);
    }

    // Grille

    @RequestMapping(value = "grille-list-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object>
        getAllGrillesForTurboTable(@RequestBody(required = false) SearchCriteria criteria) {
        List<EbPlGrilleTransportDTO> grilles = grilleTransportService.getAllGrilleTransportForTable(criteria);
        Long count = grilleTransportService.getListEbGrilleCount(criteria);
        HashMap<String, Object> result = new HashMap<String, Object>();
        result.put("count", count);
        result.put("data", grilles);
        return result;
    }

    @RequestMapping(value = "all-grille", method = RequestMethod.GET)
    public @ResponseBody List<EbPlGrilleTransportDTO> getAllGrilles() {
        List<EbPlGrilleTransportDTO> grilles = grilleTransportService
            .getAllGrilleTransportForTable(new SearchCriteria());
        return grilles;
    }

    @DeleteMapping(value = "delete-grille-transport")
    public Boolean deleteGrilleTransport(@RequestParam("grilleTransportNum") Integer grilleTransportNum) {
        return grilleTransportService.deleteGrille(grilleTransportNum);
    }

    @PostMapping(value = "save-grille-transport")
    public Integer saveGrilleTransport(@RequestBody EbPlGrilleTransportDTO grilleTransportJsonView) {
        return grilleTransportService.addOrUpdateGrilleTransportFromTable(grilleTransportJsonView);
    }

    // Plan
    @RequestMapping(value = "list-plan-table", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody Map<String, Object>
        getAllPlansTransport(@RequestBody(required = false) SearchCriteriaPlanTransport criteria) {
        List<EbPlTransport> listPlanTransport = planTransportService.listPlan(criteria);
        List<EbPlTransportDTO> dtos = planTransportService.convertEntitiesToDTOs(listPlanTransport);
        Long count = planTransportService.getListEbPlanTransportCount(criteria);
        HashMap<String, Object> result = new HashMap<>();
        result.put("count", count);
        result.put("data", dtos);
        return result;
    }

    @PostMapping(value = "update-plan")
    public EbPlTransportDTO updatePlan(@RequestBody EbPlTransportDTO ebPlanTransport) {
        return new EbPlTransportDTO(planTransportService.savePlan(ebPlanTransport));
    }

    @DeleteMapping(value = "delete-plan")
    public Boolean deletePlan(@RequestParam Integer planTransportNum) {
        return planTransportService.deletePlan(planTransportNum);
    }

    @GetMapping(value = "list-transporteur")
    public List<EbUser> getListTransporteurs() {
        return userService.getAllTransporteur();
    }

    @PostMapping(value = "plan/search")
    public List<EbPlTransport> searchPlanTransport(@RequestBody SearchCriteriaPlanTransport criteria) {
        return planTransportService.listPlan(criteria);
    }

    @PostMapping(value = "carrier/search")
    public List<PricingCarrierSearchResultDTO>
        searchCarrier(@RequestBody SearchCriteriaPlanTransport planTransportJsonView) throws JsonProcessingException {
        List<PricingCarrierSearchResultDTO> list = new ArrayList<PricingCarrierSearchResultDTO>();

        if (StringUtils.isNotBlank(accessMtc.getApiUrl())) {
            // valorisation from MTC
            list = planTransportService.searchCarrier(planTransportJsonView, true);
        }

        if (list == null || list.isEmpty()) {
            // valorisation from plan de transport
            list = planTransportService.searchCarrier(planTransportJsonView, false);
        }

        return list;
    }

    @GetMapping(value = "list-zone-adresse")
    public List<EbPlZone> getListZonesByCompanyNumAndAdresseRef(
        @RequestParam("ebCompagnieNum") Integer ebCompagnieNum,
        @RequestParam("refAdresse") String refAdresse) {
        return this.adresseService.getListZonesByCompanyNumAndAdresseRef(ebCompagnieNum, refAdresse);
    }

    @PostMapping(value = "/get-list-currency-by-stetting")
    public List<EcCurrency> selectListCurrencyByeCompagnieCurrency(@RequestBody SearchCriteria criteria) {
        return grilleTransportService.selectListCurrencyByeCompagnieCurrency(criteria);
    }

    @GetMapping(value = "mtc-enabled")
    public boolean isMTCEnabled() {
        return StringUtils.isNotBlank(accessMtc.getApiUrl());
    }
}
