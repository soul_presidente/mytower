package com.adias.mytowereasy.trpl.dao;

import java.util.List;

import com.adias.mytowereasy.trpl.model.EbPlTransport;
import com.adias.mytowereasy.trpl.model.SearchCriteriaPlanTransport;


public interface DaoPlanTransport {
    Long getListEbPlanTransportCount(SearchCriteriaPlanTransport criteria);

    List<EbPlTransport> searchPlanTransport(SearchCriteriaPlanTransport criteria);

    EbPlTransport getPlanTransportByRef(String ref);
}
