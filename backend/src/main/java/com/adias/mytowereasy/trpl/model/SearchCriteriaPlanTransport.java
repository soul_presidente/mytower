package com.adias.mytowereasy.trpl.model;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.adias.mytowereasy.trpl.dto.DetailUnitDTO;
import com.adias.mytowereasy.utils.search.SearchCriteria;


public class SearchCriteriaPlanTransport extends SearchCriteria {
    private Integer ebZoneNumOrigin;
    private Integer ebZoneNumDest;
    private Integer typeDemande;
    private Integer modeTransport;
    private Integer typeTransport;
    private Boolean modeTransportNonNullOnly;
    private Integer typeGoodsNum;
    private Boolean dangerous;
    private Double totalWeight;
    private Double totalVolume;
    private Integer currencyCibleNum;
    private Integer ebEtablissementNum;
    private Integer totalUnit;
    private Integer totalDay;

    private Date dateWaitingForPickup;
    private LocalDate datePickup;
    private Date dateDeparture;
    private Date dateArrival;
    private Date dateCustoms;
    private Date dateDeliery;
    private Date dateUnloading;
    private Long ebTypeUnitNum;
    private List<DetailUnitDTO> listUnit;
    private Integer ebTransporteurNum;
    private String emailTransporteur;
    private boolean isForValorisation;

    public Date getDateWaitingForPickup() {
        return dateWaitingForPickup;
    }

    public void setDateWaitingForPickup(Date dateWaitingForPickup) {
        this.dateWaitingForPickup = dateWaitingForPickup;
    }

    public LocalDate getDatePickup() {
        return datePickup;
    }

    public void setDatePickup(LocalDate datePickup) {
        this.datePickup = datePickup;
    }

    public Date getDateDeparture() {
        return dateDeparture;
    }

    public void setDateDeparture(Date dateDeparture) {
        this.dateDeparture = dateDeparture;
    }

    public Date getDateArrival() {
        return dateArrival;
    }

    public void setDateArrival(Date dateArrival) {
        this.dateArrival = dateArrival;
    }

    public Date getDateCustoms() {
        return dateCustoms;
    }

    public void setDateCustoms(Date dateCustoms) {
        this.dateCustoms = dateCustoms;
    }

    public Date getDateDeliery() {
        return dateDeliery;
    }

    public void setDateDeliery(Date dateDeliery) {
        this.dateDeliery = dateDeliery;
    }

    public Date getDateUnloading() {
        return dateUnloading;
    }

    public void setDateUnloading(Date dateUnloading) {
        this.dateUnloading = dateUnloading;
    }

    public Integer getEbZoneNumOrigin() {
        return ebZoneNumOrigin;
    }

    public void setEbZoneNumOrigin(Integer ebZoneNumOrigin) {
        this.ebZoneNumOrigin = ebZoneNumOrigin;
    }

    public Integer getEbZoneNumDest() {
        return ebZoneNumDest;
    }

    public void setEbZoneNumDest(Integer ebZoneNumDest) {
        this.ebZoneNumDest = ebZoneNumDest;
    }

    public Integer getTypeDemande() {
        return typeDemande;
    }

    public void setTypeDemande(Integer typeDemande) {
        this.typeDemande = typeDemande;
    }

    public Integer getModeTransport() {
        return modeTransport;
    }

    public void setModeTransport(Integer modeTransport) {
        this.modeTransport = modeTransport;
    }

    public Boolean getDangerous() {
        return dangerous;
    }

    public void setDangerous(Boolean dangerous) {
        this.dangerous = dangerous;
    }

    public Double getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Double totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Integer getCurrencyCibleNum() {
        return currencyCibleNum;
    }

    public void setCurrencyCibleNum(Integer currencyCibleNum) {
        this.currencyCibleNum = currencyCibleNum;
    }

    public Integer getEbEtablissementNum() {
        return ebEtablissementNum;
    }

    public void setEbEtablissementNum(Integer ebEtablissementNum) {
        this.ebEtablissementNum = ebEtablissementNum;
    }

    public Integer getTotalUnit() {
        return totalUnit;
    }

    public void setTotalUnit(Integer totalUnit) {
        this.totalUnit = totalUnit;
    }

    public Double getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(Double totalVolume) {
        this.totalVolume = totalVolume;
    }

    public Integer getTotalDay() {
        return totalDay;
    }

    public void setTotalDay(Integer totalDay) {
        this.totalDay = totalDay;
    }

    public Boolean getModeTransportNonNullOnly() {
        return modeTransportNonNullOnly;
    }

    public void setModeTransportNonNullOnly(Boolean modeTransportNonNullOnly) {
        this.modeTransportNonNullOnly = modeTransportNonNullOnly;
    }

    public Long getEbTypeUnitNum() {
        return ebTypeUnitNum;
    }

    public void setEbTypeUnitNum(Long ebTypeUnitNum) {
        this.ebTypeUnitNum = ebTypeUnitNum;
    }

    public Integer getTypeGoodsNum() {
        return typeGoodsNum;
    }

    public void setTypeGoodsNum(Integer typeGoodsNum) {
        this.typeGoodsNum = typeGoodsNum;
    }

    public Integer getTypeTransport() {
        return typeTransport;
    }

    public void setTypeTransport(Integer typeTransport) {
        this.typeTransport = typeTransport;
    }

    public List<DetailUnitDTO> getListUnit() {
        return listUnit;
    }

    public void setListUnit(List<DetailUnitDTO> listUnit) {
        this.listUnit = listUnit;
    }

    public Integer getEbTransporteurNum() {
        return ebTransporteurNum;
    }

    public void setEbTransporteurNum(Integer ebTranspoteurNum) {
        this.ebTransporteurNum = ebTranspoteurNum;
    }

    public String getEmailTransporteur() {
        return emailTransporteur;
    }

    public void setEmailTransporteur(String emailTransporteur) {
        this.emailTransporteur = emailTransporteur;
    }

    public boolean isForValorisation() {
        return isForValorisation;
    }

    public void setIsForValorisation(boolean isForValorisation) {
        this.isForValorisation = isForValorisation;
    }
}
