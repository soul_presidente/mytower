delete from work.eb_acl_relation where eb_acl_relation_num in (
select rel.eb_acl_relation_num from work.eb_acl_relation rel
left join work.ec_acl_rule ru on code = 'settings.customfields.v2.enable'
where rel.x_ec_acl_rule = ru.ec_acl_rule_num);
delete from work.ec_acl_rule where code = 'settings.customfields.v2.enable';