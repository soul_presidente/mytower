WITH ref_livraison as(
  select 
    eb_del_livraison_num, 
    concat(
      code, 
      '-V', 
      lpad(
        CAST (eb_del_livraison_num AS varchar), 
        5, 
        'O'
      )
    ) as ref_concat 
  from 
    work.eb_del_livraison 
    left join work.eb_compagnie on eb_compagnie_num = x_eb_compagnie 
  where 
    eb_del_reference is null
) 
update 
  work.eb_del_livraison 
set 
  eb_del_reference = ref_livraison.ref_concat 
from 
  ref_livraison 
where 
  eb_del_livraison.eb_del_livraison_num = ref_livraison.eb_del_livraison_num 
  and eb_del_livraison.eb_del_reference is null
