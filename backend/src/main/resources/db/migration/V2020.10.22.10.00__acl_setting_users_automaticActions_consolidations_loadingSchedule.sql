INSERT INTO work.ec_acl_rule (code, default_value, themes, type, experimental)
VALUES ('setting.company.automatic.actions.enable', 'false', 'SETTINGS;COMPANY', 0, false) ON CONFLICT DO NOTHING;

INSERT INTO work.ec_acl_rule (code, default_value, themes, type, experimental)
VALUES ('setting.company.consolidations.enable', 'false', 'SETTINGS;COMPANY', 0, false) ON CONFLICT DO NOTHING;

INSERT INTO work.ec_acl_rule (code, default_value, themes, type, experimental)
VALUES ('setting.company.loading.schedule.enable', 'false', 'SETTINGS;COMPANY', 0, false) ON CONFLICT DO NOTHING;
