do $$
begin
	
WHILE (select count(1) from (select  edq.ex_eb_demande_transporteur_num,
		arr.object as list_cost_item from work.eb_demande_quote edq, jsonb_array_elements(list_pl_cost_item) 
		with ordinality arr(object, index)
		where arr.object ->> 'euroExchangeRate' is null) as demande_transporteur) > 0 	LOOP
	
		with cost_item3 as (
			select ('{' || index - 1 || ',"euroExchangeRate"}')::TEXT[] AS euroExchangeRate_path,
				edq.ex_eb_demande_transporteur_num,
				arr.object as list_cost_item from work.eb_demande_quote edq, jsonb_array_elements(list_pl_cost_item) 
			with ordinality arr(object, index)
			where arr.object ->> 'euroExchangeRate' is null
			)
			
		update work.eb_demande_quote edq2
		set list_pl_cost_item = jsonb_set(list_pl_cost_item, cost_item3.euroExchangeRate_path, '1')
		from cost_item3
		where edq2.ex_eb_demande_transporteur_num = cost_item3.ex_eb_demande_transporteur_num;

    END LOOP;
end; $$