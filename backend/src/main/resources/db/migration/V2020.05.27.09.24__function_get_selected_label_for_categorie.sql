CREATE OR REPLACE FUNCTION "work"."get_selected_label_for_categorie"("pjson" jsonb, "pkey" text, "pkeyvalue" text, "pkeyres" text, "indice" int4, "labelkey" text)
  RETURNS "pg_catalog"."int4" AS $BODY$
		SELECT
			((pp->>pkeyres)::jsonb->indice->>labelkey)::int
		FROM jsonb_array_elements(pjson::jsonb) pp
		WHERE pp->>pkey = pkeyvalue
$BODY$
  LANGUAGE sql VOLATILE
  COST 100