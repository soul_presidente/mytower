ALTER TABLE   IF EXISTS  work.eb_del_order_line
ALTER COLUMN  item_number TYPE character varying(255) USING item_number::character varying(255);

ALTER TABLE  IF EXISTS   work.eb_del_livraison_line
ALTER COLUMN  item_number TYPE character varying(255) USING item_number::character varying(255);