CREATE OR REPLACE FUNCTION "work"."get_value_by_keyvalue"("pjson" text, "pkey" text, "pkeyvalue" text, "pkeyres" text)
  RETURNS "pg_catalog"."text" AS $BODY$
      SELECT
         pp->>pkeyres
      FROM jsonb_array_elements(case jsonb_typeof(pjson::jsonb) when 'array' then pjson::jsonb else null end) pp
      WHERE pp->>pkey = pkeyvalue
$BODY$
  LANGUAGE sql VOLATILE
  COST 100