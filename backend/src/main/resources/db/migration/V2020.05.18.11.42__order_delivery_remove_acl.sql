delete from work.eb_acl_relation where eb_acl_relation_num in (
select rel.eb_acl_relation_num from work.eb_acl_relation rel
left join work.ec_acl_rule ru on code = 'order.createdelivery.enable'
where rel.x_ec_acl_rule = ru.ec_acl_rule_num);
delete from work.ec_acl_rule where code = 'order.createdelivery.enable';

delete from work.eb_acl_relation where eb_acl_relation_num in (
select rel.eb_acl_relation_num from work.eb_acl_relation rel
left join work.ec_acl_rule ru on code = 'delivery.createtransport.enable'
where rel.x_ec_acl_rule = ru.ec_acl_rule_num);
delete from work.ec_acl_rule where code = 'delivery.createtransport.enable';

delete from work.eb_acl_relation where eb_acl_relation_num in (
select rel.eb_acl_relation_num from work.eb_acl_relation rel
left join work.ec_acl_rule ru on code = 'delivery.edition.enable'
where rel.x_ec_acl_rule = ru.ec_acl_rule_num);
delete from work.ec_acl_rule where code = 'delivery.edition.enable';
