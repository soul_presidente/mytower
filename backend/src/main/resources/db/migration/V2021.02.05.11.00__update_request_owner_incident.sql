UPDATE work.eb_qm_incident inc
SET owner_nom_prenom = CONCAT(( 
SELECT nom from work.eb_user join work.eb_demande on eb_user_num=x_eb_user
						where inc.x_eb_demande= eb_demande_num), ' ' ,
(SELECT prenom from work.eb_user join work.eb_demande on eb_user_num=x_eb_user
						where inc.x_eb_demande= eb_demande_num)),

owner_num = (SELECT eb_user_num from work.eb_user join work.eb_demande on eb_user_num=x_eb_user
						where inc.x_eb_demande= eb_demande_num);