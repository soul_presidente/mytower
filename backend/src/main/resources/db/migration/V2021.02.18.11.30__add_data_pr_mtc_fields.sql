INSERT INTO "work"."pr_mtc_field"("pr_mtc_field_num", "code", "mtc_attribute", "mtg_attribute") VALUES (41, 41, 'Niveau de service', 'TypeRequestLibelle') ON CONFLICT DO NOTHING;
INSERT INTO "work"."pr_mtc_field"("pr_mtc_field_num", "code", "mtc_attribute", "mtg_attribute") VALUES (40, 40, 'Airport de l''adresse de destination', 'ebPartyDest:airport') ON CONFLICT DO NOTHING;
INSERT INTO "work"."pr_mtc_field"("pr_mtc_field_num", "code", "mtc_attribute", "mtg_attribute") VALUES (39, 39, 'Airport de l''adresse d''origine', 'ebPartyOrigin:airport') ON CONFLICT DO NOTHING;

update work.pr_mtc_field  set mtg_attribute = 'ListMarchandises:call-com.adias.mytowereasy.mtc.service.MtcService.getMarchandiseAttribute-Dg' where pr_mtc_field_num in (27,36);
