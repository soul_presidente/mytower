WITH ref_order as(
  select 
    eb_del_order_num, 
    concat(
      comp.code, 
      '-O', 
      LPAD(
        CAST(ord.eb_del_order_num AS VARCHAR), 
        5, 
        '0'
      )
    ) as ref_concat 
  from 
    work.eb_del_order ord 
    left join work.eb_compagnie comp on eb_compagnie_num = ord.x_eb_compagnie 
  where 
    ord.reference is null
) 
update 
  work.eb_del_order ord 
set 
  reference = ref_order.ref_concat 
from 
  ref_order 
where 
  ord.eb_del_order_num = ref_order.eb_del_order_num 
  and ord.reference is null
