ALTER TABLE "work"."eb_demande" 
ALTER COLUMN "part_number" TYPE text COLLATE "pg_catalog"."default" USING "part_number"::text,
ALTER COLUMN "order_number" TYPE text COLLATE "pg_catalog"."default" USING "order_number"::text,
ALTER COLUMN "serial_number" TYPE text COLLATE "pg_catalog"."default" USING "serial_number"::text;