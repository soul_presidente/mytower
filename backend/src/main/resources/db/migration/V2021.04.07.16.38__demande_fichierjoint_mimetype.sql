update work.eb_demande_fichiers_joint
set mimetype = 'application/msword'
where LOWER(file_name) like LOWER('%.doc');

update work.eb_demande_fichiers_joint
set mimetype = 'application/vnd.ms-excel'
where LOWER(file_name) like LOWER('%.xls');

update work.eb_demande_fichiers_joint
set mimetype = 'application/vnd.ms-powerpoint'
where LOWER(file_name) like LOWER('%.ppt');

update work.eb_demande_fichiers_joint
set mimetype = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
where LOWER(file_name) like LOWER('%.docx');

update work.eb_demande_fichiers_joint
set mimetype = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
where LOWER(file_name) like LOWER('%.xlsx');

update work.eb_demande_fichiers_joint
set mimetype = 'application/vnd.openxmlformats-officedocument.presentationml.presentation'
where LOWER(file_name) like LOWER('%.pptx');

update work.eb_demande_fichiers_joint
set mimetype = 'application/pdf'
where LOWER(file_name) like LOWER('%.pdf');

update work.eb_demande_fichiers_joint
set mimetype = 'text/html'
where LOWER(file_name) like LOWER('%.html') or LOWER(file_name) like LOWER('%.htm');

update work.eb_demande_fichiers_joint
set mimetype = 'text/plain'
where LOWER(file_name) like LOWER('%.txt');

update work.eb_demande_fichiers_joint
set mimetype = 'image/jpeg'
where LOWER(file_name) like LOWER('%.jpg') or LOWER(file_name) like LOWER('%.jpeg');

update work.eb_demande_fichiers_joint
set mimetype = 'image/png'
where LOWER(file_name) like LOWER('%.png');

update work.eb_demande_fichiers_joint
set mimetype = 'image/gif'
where LOWER(file_name) like LOWER('%.gif');

update work.eb_demande_fichiers_joint
set mimetype = 'image/bmp'
where LOWER(file_name) like LOWER('%.bmp');

update work.eb_demande_fichiers_joint
set mimetype = 'image/tiff'
where LOWER(file_name) like LOWER('%.tif') or LOWER(file_name) like LOWER('%.tiff');

update work.eb_demande_fichiers_joint
set mimetype = 'video/x-msvideo'
where LOWER(file_name) like LOWER('%.avi');

update work.eb_demande_fichiers_joint
set mimetype = 'text/csv'
where LOWER(file_name) like LOWER('%.csv');

update work.eb_demande_fichiers_joint
set mimetype = 'video/mpeg'
where LOWER(file_name) like LOWER('%.mpeg');

update work.eb_demande_fichiers_joint
set mimetype = 'video/webm'
where LOWER(file_name) like LOWER('%.webm');

update work.eb_demande_fichiers_joint
set mimetype = 'application/rtf'
where LOWER(file_name) like LOWER('%.rtf');

update work.eb_demande_fichiers_joint
set mimetype = 'application/vnd.ms-xpsdocument'
where LOWER(file_name) like LOWER('%.xps');

update work.eb_demande_fichiers_joint
set mimetype = 'application/vnd.ms-excel.sheet.macroEnabled.12'
where LOWER(file_name) like LOWER('%.xlsm');

update work.eb_demande_fichiers_joint
set mimetype = 'application/vnd.ms-outlook'
where LOWER(file_name) like LOWER('%.msg');

update work.eb_demande_fichiers_joint
set mimetype = 'application/vnd.ms-word.document.macroEnabled.12'
where LOWER(file_name) like LOWER('%.docm');

