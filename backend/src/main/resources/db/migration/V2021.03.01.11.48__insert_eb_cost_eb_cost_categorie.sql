do $$ DECLARE cmp Work.eb_compagnie % rowtype;
ebcostcat Work.eb_cost_categorie % rowtype;
ebcost Work.eb_cost_categorie % rowtype;
cost_categorie_id integer;
compagnie_num integer;
nextebcost_id integer;
nextcostcat_id integer;
pre_cost_id integer;
frei_cost_id integer;
cost_a_d_id integer;
other_c_id integer;
cost_id integer;

BEGIN
delete from work.eb_cost;
delete from work.eb_cost_categorie;
FOR cmp IN
SELECT * FROM Work.eb_compagnie where compagnie_role = 1
  LOOP
  compagnie_num = cmp.eb_compagnie_num;

  -- insert cost_categorie
  INSERT INTO work.eb_cost_categorie ( code, libelle,x_eb_compagnie)
  VALUES ( 'C1', 'Precarriage Costs',compagnie_num) returning eb_cost_categorie_num into pre_cost_id;

  INSERT INTO work.eb_cost_categorie (code, libelle,x_eb_compagnie)
  VALUES ( 'C2', 'Freight costs',compagnie_num) returning eb_cost_categorie_num into frei_cost_id;

  INSERT INTO work.eb_cost_categorie (code, libelle,x_eb_compagnie)
  VALUES ('C3', 'Costs at destination',compagnie_num) returning eb_cost_categorie_num into cost_a_d_id;

  INSERT INTO work.eb_cost_categorie (code, libelle,x_eb_compagnie)
  VALUES ('C4', 'Other costs',compagnie_num) returning eb_cost_categorie_num into other_c_id  ;

  -- insert picku up

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
  'EC1', 'Pick up',TRUE,':1::2::5:',compagnie_num
  ) returning eb_cost_num into cost_id;

INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,pre_cost_id);

----- insert loading cost

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC2', 'Loading',TRUE,':2::5:',compagnie_num
  ) returning eb_cost_num into cost_id;

INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,pre_cost_id);

-- insert Export Customs formalities

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC3', 'Export Customs formalities',TRUE,':1::2::3::4::5:',compagnie_num
  ) returning eb_cost_num into cost_id;

INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,pre_cost_id);
INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,cost_a_d_id);
INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,other_c_id);

-- insert Documentation

  INSERT INTO work.eb_cost (
  code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
    'EC4', 'Documentation',TRUE,':1::2::5:',compagnie_num
  ) returning eb_cost_num into cost_id;

INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,pre_cost_id);
INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,cost_a_d_id);
-- insert DGR

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC5', 'DGR',TRUE,':1::2::3::4::5:',compagnie_num
  ) returning eb_cost_num into cost_id;

INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,pre_cost_id);
INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);
-- insert Other

  INSERT INTO work.eb_cost (
  code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC6', 'Other',TRUE,':1::2::3::4::5:',compagnie_num
  ) returning eb_cost_num into cost_id;
-- Insert cost categorie
INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);
INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,cost_a_d_id);
INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,pre_cost_id);
INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,other_c_id);
-- Insert EbCosts
-- insert Port to port

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC7', 'Port to port',TRUE,':2:',compagnie_num
  ) returning eb_cost_num into cost_id;

INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);
-- insert Surcharges

  INSERT INTO work.eb_cost (
 code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC8', 'Surcharges',TRUE,':2::5:',compagnie_num
  ) returning eb_cost_num into cost_id;

INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);
-- insert Unloading

  INSERT INTO work.eb_cost (
  code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
    'EC9', 'Unloading',TRUE,':2::5:',compagnie_num
  ) returning eb_cost_num into cost_id;

INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,cost_a_d_id);
-- insert Post-shipment, delivery

  INSERT INTO work.eb_cost (
  code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC10', 'Post-shipment, delivery',TRUE,':1::2::5:',compagnie_num
  ) returning eb_cost_num into cost_id;

INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,cost_a_d_id);

-- insert Storage

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC11', 'Storage',TRUE,':1::2::5:',compagnie_num
  ) returning eb_cost_num into cost_id;

INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,cost_a_d_id);

-- insert Demurrage / Detention

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC12', 'Demurrage / Detention',TRUE,':2::5:',compagnie_num
  ) returning eb_cost_num into cost_id;

INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,cost_a_d_id);

-- Insert Insurance

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC13', 'Insurance',TRUE,':1::2::3::4::5:',compagnie_num
  ) returning eb_cost_num into cost_id;

INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,other_c_id);
-- Insert Airfreight

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC14', 'Airfreight',TRUE,':1:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);
-- Insert Fuel surcharge

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC15', 'Fuel surcharge',TRUE,':1::3::4:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);
-- Insert IRC surcharge

  INSERT INTO work.eb_cost (
  code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC16', 'IRC surcharge',TRUE,':1:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);
-- Insert Import Customs formalities

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC17', 'Import Customs formalities',TRUE,':1::3::4::5:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,cost_a_d_id);
    INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,other_c_id);
-- Insert Door to door

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC18', 'Door to door',TRUE,':3:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);
-- Insert Other documentation

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC19', 'Other documentation',TRUE,':3:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,other_c_id);
 -- Insert Rate

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC20', 'Rate',TRUE,':4:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC21', 'Oversize',TRUE,':4:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);
-- Insert Overweight

  INSERT INTO work.eb_cost (
  code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC22', 'Overweight',TRUE,':4:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);
-- Insert Station to Station

  INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC23', 'Station to Station',TRUE,':5:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);

    -- insert Origing handling
      INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC24', 'Origin handling',TRUE,':1:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,pre_cost_id);

    -- Insert Main Transport
          INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC25', 'Main transport',TRUE,':1::2::3::4::5:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,pre_cost_id);

    -- Insert Destination Handling
              INSERT INTO work.eb_cost (
   code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC26', 'Destination Handling',TRUE,':2:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,cost_a_d_id);

    -- Insert Post carriage
    INSERT INTO work.eb_cost (
       code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC27', 'Post carriage',TRUE,':2:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,cost_a_d_id);

    -- Insert Management Fees
    INSERT INTO work.eb_cost (
           code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC28', 'Management Fees',TRUE,':2:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,other_c_id);

    -- Insert Fee AWB
      INSERT INTO work.eb_cost (
           code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC29', 'Fee AWB',TRUE,':1:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);

  -- insert Fee Security Screenling
          INSERT INTO work.eb_cost (
           code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC30', 'Fee Security Screenling',TRUE,':1:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);
-- Insert Fee AOG
          INSERT INTO work.eb_cost (
           code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC31', 'Fee AOG',TRUE,':1:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,other_c_id);

-- Insert Fee Airline Express
          INSERT INTO work.eb_cost (
           code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC32', 'Fee Airline Express',TRUE,':1:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,other_c_id);

-- Insert Transit Fee
          INSERT INTO work.eb_cost (
           code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC33', 'Transit Fee',TRUE,':1:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);

    -- Insert OFR
              INSERT INTO work.eb_cost (
           code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC34', 'OFR',TRUE,':2:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,frei_cost_id);

    -- Insert NSTI
              INSERT INTO work.eb_cost (
           code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC35', 'NSTI',TRUE,':1::2:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,other_c_id);

    -- Insert BL Fee
             INSERT INTO work.eb_cost (
           code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC36', 'BL Fee',TRUE,':1:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,other_c_id);

-- Insert Transport document Fee
        INSERT INTO work.eb_cost (
           code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC37', 'Transport document Fee',TRUE,':2:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,other_c_id);
-- Insert ISPS
 INSERT INTO work.eb_cost (
           code, libelle,actived,list_mode_transport,x_eb_compagnie
)
VALUES
  (
     'EC38', 'ISPS',TRUE,':2:',compagnie_num
  ) returning eb_cost_num into cost_id;
  INSERT INTO work.eb_cost_list_cost_categorie (list_eb_cost_eb_cost_num,list_cost_categorie_eb_cost_categorie_num)
    VALUES (cost_id,other_c_id);

 -- end loop compagnie
END LOOP;

RETURN;
end $$ language plpgsql
