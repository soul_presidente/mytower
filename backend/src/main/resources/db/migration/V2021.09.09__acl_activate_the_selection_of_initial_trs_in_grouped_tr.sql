INSERT INTO work.ec_acl_rule (
  code, default_value, themes, type, 
  experimental
) 
VALUES 
  (
    'demande.initial_trs_checkbox_select', 'false', 'PRICING', 
    0, false
  ) ON CONFLICT DO NOTHING;
