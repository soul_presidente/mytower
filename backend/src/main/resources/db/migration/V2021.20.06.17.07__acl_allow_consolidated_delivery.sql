INSERT INTO work.ec_acl_rule (
  code, default_value, themes, type, 
  experimental
) 
VALUES 
  (
    'delivery.consolidated', 'false', 'DELIVERY', 
    0, false
  ) ON CONFLICT DO NOTHING;
