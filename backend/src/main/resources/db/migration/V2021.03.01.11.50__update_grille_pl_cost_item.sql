do $$ DECLARE
cmp Work.eb_compagnie % rowtype;
grille Work.eb_pl_grille_transport % rowtype;
plcost_item Work.eb_pl_cost_item % rowtype;
compagnie_num integer;
cost_num integer;
BEGIN
FOR cmp IN SELECT  * FROM Work.eb_compagnie where compagnie_role = 1
  LOOP
  compagnie_num = cmp.eb_compagnie_num;
    FOR grille IN SELECT * FROM Work.eb_pl_grille_transport where x_eb_company= compagnie_num
    LOOP
        FOR plcost_item IN SELECT * FROM Work.eb_pl_cost_item where grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num
        LOOP
        IF plcost_item.type = 1 then
        select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Other';
        update work.eb_pl_cost_item set type = cost_num ,libelle = 'Other' where type =1 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 2 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Origin handling';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Origin handling' where type =2 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 3 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Main transport';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Main transport' where type =3 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 4 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Fuel surcharge';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Fuel surcharge' where type =4 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 5 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'IRC surcharge';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'IRC surcharge' where type =5 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 6 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Insurance';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Insurance' where type =6 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 7 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Destination Handling';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Destination Handling' where type =7 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 8 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Import Customs formalities	';
        update work.eb_pl_cost_item set type = cost_num ,libelle = 'Import Customs formalities' where type =8 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 9 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Storage';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Storage' where type =9 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 10 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Demurrage / Detention';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Demurrage / Detention' where type =10 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

       IF plcost_item.type = 11 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Demurrage / Detention';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Demurrage / Detention' where type =11 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 12 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'DGR';
        update work.eb_pl_cost_item set type = cost_num, libelle = 'DGR' where type =12 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 13 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Post carriage';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Post carriage' where type =13 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 14 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Export Customs formalities';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Export Customs formalities' where type =14 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 15 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Management Fees';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Management Fees' where type =15 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 16 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Fee AWB';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Fee AWB' where type =16 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 17 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Fee Security Screenling';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Fee Security Screenling' where type =17 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 18 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Fee AOG';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Fee AOG' where type =18 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 19 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Fee Airline Express';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Fee Airline Express' where type =19 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

         IF plcost_item.type = 20 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Pick up';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Pick up' where type =20 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 21 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Transit Fee';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Transit Fee' where type =21 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 22 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'OFR';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'OFR' where type =22 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 23 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Post-shipment, delivery';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Post-shipment, delivery' where type =23 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 24 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'NSTI';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'NSTI' where type =24 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 25 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'BL Fee';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'BL Fee' where type =25 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 26 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'Transport document Fee';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'Transport document Fee' where type =26 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;

        IF plcost_item.type = 27 then
          select eb_cost_num into cost_num from work.eb_cost where x_eb_compagnie = compagnie_num and libelle = 'ISPS';
        update work.eb_pl_cost_item set type = cost_num,libelle = 'ISPS' where type =27 and grille_transport_eb_grille_transport_num = grille.eb_grille_transport_num;
        END IF;





END LOOP;
END LOOP;
END LOOP;

RETURN;
end $$ language plpgsql
