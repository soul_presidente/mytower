do $$
DECLARE
    r Work.eb_demande_quote%rowtype;
	x_transporteur integer;
	etablissement integer;
BEGIN
    FOR r IN SELECT * FROM Work.eb_demande_quote
    LOOP
        x_transporteur = r.x_transporteur;
		if x_transporteur is not null then
		select Work.eb_user.x_eb_etablissement into etablissement from Work.eb_user where eb_user_num = x_transporteur;
		if r.x_eb_etablissement <> etablissement then
		UPDATE Work.eb_demande_quote SET x_eb_etablissement = etablissement where ex_eb_demande_transporteur_num = r.ex_eb_demande_transporteur_num;
		end if;
		end if;
    END LOOP;
    RETURN;
end
$$ language plpgsql