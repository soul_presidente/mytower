UPDATE WORK.eb_demande_quote q
SET x_eb_compagnie = ( SELECT e.x_eb_compagnie FROM WORK.eb_etablissement e WHERE e.eb_etablissement_num = q.x_eb_etablissement )
WHERE
    q.x_eb_compagnie IS NULL;


ALTER TABLE WORK.eb_demande_quote ALTER COLUMN x_eb_compagnie
    SET NOT NULL;