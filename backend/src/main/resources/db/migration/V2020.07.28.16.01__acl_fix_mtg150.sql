INSERT INTO work.ec_acl_rule (code, default_value, themes, type, experimental) VALUES ('demande.dashboard.scheduling.enable', 'false', 'PRICING', 0, true) ON CONFLICT DO NOTHING;
INSERT INTO work.ec_acl_rule (code, default_value, themes, type, "values") VALUES ('settings.mycommunity.mytowernetwork', 'WRITE', 'SETTINGS;COMMUNITY', 1, 'NOT_VISIBLE;VISIBLE;WRITE') ON CONFLICT DO NOTHING;
INSERT INTO work.ec_acl_rule (code, default_value, themes, type, experimental) VALUES ('demande.visualisation.trpl.enable', 'false', 'PRICING;TRPL', 0, true) ON CONFLICT DO NOTHING;
INSERT INTO work.ec_acl_rule (code, default_value, themes, type, experimental) VALUES ('demande.step.documents.enable', 'false', 'PRICING;DOCUMENT', 0, true) ON CONFLICT DO NOTHING;
INSERT INTO work.ec_acl_rule (code, default_value, themes, type, "values", experimental) VALUES ('module.rsch', 'NOT_VISIBLE', 'MODULE;RSCH', 1, 'NOT_VISIBLE;VISIBLE;WRITE', true) ON CONFLICT DO NOTHING;
INSERT INTO work.ec_acl_rule (code, default_value, themes, type, experimental) VALUES ('pricing.carrier.show.allow', 'true', 'PRICING', 0, true) ON CONFLICT DO NOTHING;
