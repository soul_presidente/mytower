CREATE OR REPLACE FUNCTION "work"."get_value_by_keyvalue"("pjson" text, "pkey" text, "pkeyvalue" text, "pkeyres" text)
  RETURNS "pg_catalog"."text" AS $BODY$
		SELECT
			pp->>pkeyres
		FROM jsonb_array_elements(pjson::jsonb) pp
		WHERE pp->>pkey = pkeyvalue
$BODY$
  LANGUAGE sql VOLATILE
  COST 100