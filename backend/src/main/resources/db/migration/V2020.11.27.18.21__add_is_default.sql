do $$
DECLARE

field json;
newfields  json;
updatedjson Work.eb_custom_field.fields%TYPE;
ebuser Work.eb_custom_field.x_eb_user%TYPE;


begin
 For ebuser IN select x_eb_user from Work.eb_custom_field
 LOOP
     newfields = jsonb_build_array();
	FOR field IN select json_array_elements(fields::json) from Work.eb_custom_field where x_eb_user=ebuser

   LOOP
	  if field->>'isdefault' is null then
	  	updatedjson := ((field::jsonb || '{"isdefault":true}'))::text;
		 select fields || updatedjson::jsonb as newjosn from (
    select newfields::jsonb as fields into newfields
) t ;

	  end if;
	  if field->>'isdefault' is not null then
	  select fields || field::jsonb as newjosn from (
    select newfields::jsonb as fields into newfields
) t ;
	  end if;
   END LOOP;
   UPDATE Work.eb_custom_field SET fields = newfields where x_eb_user = ebuser;
   END LOOP;
end;
$$ language plpgsql