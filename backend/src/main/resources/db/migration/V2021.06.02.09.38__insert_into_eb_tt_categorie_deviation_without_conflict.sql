INSERT INTO  work.eb_tt_categorie_deviation (eb_tt_categorie_deviation_num,libelle, type_event, code)
VALUES (1,'Missing or damaged goods', 1, '1'),
 (2,'Information flow issues', 1, '2'),
 (3,'Operational issues', 1, '3'),
 (4,'Other issues', 1, '4'),
 (5,'Customs issue', 1, '5'),
 (6,'Goods availibility issues', 1, '6'),
 (7,'Hazardous goods issues', 1, '7'),
 (8,'Force majeure', 1, '8'),
 (9,'Cold chain issues', 1, '9'),
 (10,'Missing update',null , '10'),
 (11,'Delay',null , '11'),
 (21,'Missing document',3 , '21')
    ON CONFLICT (eb_tt_categorie_deviation_num) DO NOTHING ;