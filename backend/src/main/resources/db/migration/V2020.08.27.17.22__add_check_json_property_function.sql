CREATE OR REPLACE FUNCTION "work"."check_json_property"("obj" jsonb, "prop" text, "val" text)
  RETURNS "pg_catalog"."bool" AS $BODY$
	
	SELECT (obj->>prop)::text = val;
			
$BODY$
  LANGUAGE sql VOLATILE
  COST 100