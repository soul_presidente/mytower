do $$ DECLARE cmp Work.eb_compagnie % rowtype;
ebcostcat Work.eb_cost_categorie % rowtype;
ebcost Work.eb_cost_categorie % rowtype;
cost_categorie_id integer;
compagnie_num integer;
nextebcost_id integer;
nextcostcat_id integer;
BEGIN
FOR cmp IN
SELECT
  *
FROM
  Work.eb_compagnie where compagnie_role = 1
  LOOP
  compagnie_num = cmp.eb_compagnie_num;
FOR ebcostcat IN
SELECT
  *
FROM
  Work.eb_cost_categorie
where
  x_eb_compagnie IS NULL LOOP
SELECT
  max(eb_cost_categorie_num)
FROM
  work.eb_cost_categorie into nextcostcat_id;
nextcostcat_id = nextcostcat_id + 1;
INSERT INTO work.eb_cost_categorie (
  eb_cost_categorie_num, code, libelle,
  x_ec_mode_transport, x_eb_compagnie
)
VALUES
  (
    nextcostcat_id, ebcostcat.code, ebcostcat.libelle,
    ebcostcat.x_ec_mode_transport,
    compagnie_num
  ) returning eb_cost_categorie_num into cost_categorie_id;
FOR ebcost IN
SELECT
  *
FROM
  Work.eb_cost
where
  x_eb_cost_categorie = ebcostcat.eb_cost_categorie_num LOOP
SELECT
  max(eb_cost_num)
FROM
  work.eb_cost into nextebcost_id;
nextebcost_id = nextebcost_id + 1;
INSERT INTO work.eb_cost (
  eb_cost_num, code, libelle, x_eb_cost_categorie,
  euro_exchange_rate, euro_exchange_rate_real,
  libelle_cost_item, price_gab, actived
)
VALUES
  (
    nextebcost_id, ebcost.code, ebcost.libelle,
    cost_categorie_id, NULL,
    NULL,
    NULL, NULL,
    TRUE
  );
END LOOP;
END LOOP;
END LOOP;
delete from
  work.eb_cost
where
  actived is NULL;
delete from
  work.eb_cost_categorie
where
  x_eb_compagnie is NULL;
RETURN;
end $$ language plpgsql
