CREATE OR REPLACE FUNCTION "work"."get_property_value_from_jsonb_field"("obj" jsonb, "prop" text)
  RETURNS "pg_catalog"."text" AS $BODY$
	
	SELECT (obj->>prop)::text;
			
$BODY$
  LANGUAGE sql VOLATILE
  COST 100