update work.eb_demande 
set list_type_documents = '[]'
where list_type_documents isnull;

ALTER TABLE work.eb_demande ALTER COLUMN list_type_documents SET NOT NULL;
