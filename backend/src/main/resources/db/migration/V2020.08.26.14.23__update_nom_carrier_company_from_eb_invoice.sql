update work.eb_invoice set nom_company_carrier = comp.nom
from work.eb_invoice inv 
left join work.eb_demande d on inv.x_eb_demande = d.eb_demande_num
left join work.eb_demande_quote q on q.x_eb_demande = d.eb_demande_num
left join work.eb_user u on q.x_transporteur = u.eb_user_num
left join work.eb_compagnie comp on u.x_eb_compagnie = comp.eb_compagnie_num
where d.valuation_type = 7