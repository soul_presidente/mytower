insert into work.eb_demande(eb_demande_num, list_type_documents)
values(-1, '[]'); 

update work.eb_marchandise 
set x_eb_demande = -1
where x_eb_demande isnull;

ALTER TABLE work.eb_marchandise ALTER COLUMN x_eb_demande SET NOT NULL;