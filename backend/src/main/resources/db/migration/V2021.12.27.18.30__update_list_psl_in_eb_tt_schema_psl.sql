
do $$
begin
	
WHILE (select count(1) from (select etsp1.eb_tt_schema_psl_num, arr.object as list_psl 
		from work.eb_tt_schema_psl etsp1, jsonb_array_elements(list_psl)
		with ordinality arr(object, index)
		where arr.object ->> 'isChecked' is null) as schema_psl) > 0 	LOOP
		
		with psl as(
					select ('{' || index - 1 || ',"isChecked"}')::TEXT[] AS checked_path, etsp1.eb_tt_schema_psl_num, arr.object as list_psl 
					from work.eb_tt_schema_psl etsp1, jsonb_array_elements(list_psl)
					with ordinality arr(object, index)
					where arr.object ->> 'isChecked' is null	
		)
		
		update work.eb_tt_schema_psl etsp2
		set list_psl = jsonb_set(etsp2.list_psl, psl.checked_path, to_jsonb(true))
		from psl
		where etsp2.eb_tt_schema_psl_num = psl.eb_tt_schema_psl_num;

    END LOOP;
end; $$