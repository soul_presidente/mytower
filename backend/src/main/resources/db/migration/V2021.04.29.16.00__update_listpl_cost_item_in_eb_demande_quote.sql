do $$
begin
	
WHILE (select count(1) from (select  edq.ex_eb_demande_transporteur_num,
		arr.object as list_cost_item from work.eb_demande_quote edq, jsonb_array_elements(list_pl_cost_item) 
		with ordinality arr(object, index)
		where arr.object -> 'currency' ->> 'code' is null) as demande_transporteur) > 0 	LOOP
		
		with cost_item as (
			select ('{' || index - 1 || ',"currency"}')::TEXT[] AS currency_path,
				edq.ex_eb_demande_transporteur_num,
				(select to_jsonb(ec.*) from work.eb_demande ed left outer join work.ec_currency ec on ed.x_ec_currency_invoice = ec.ec_currency_num where ed.eb_demande_num = edq.x_eb_demande) as currency,
				arr.object as list_cost_item from work.eb_demande_quote edq, jsonb_array_elements(list_pl_cost_item) 
			with ordinality arr(object, index)
			where arr.object -> 'currency' ->> 'code' is null
			)
   
		update work.eb_demande_quote edq2
		set list_pl_cost_item = jsonb_set(list_pl_cost_item, cost_item.currency_path, cost_item.currency)
		from cost_item
		where edq2.ex_eb_demande_transporteur_num = cost_item.ex_eb_demande_transporteur_num;
		
		with cost_item2 as (
			select ('{' || index - 1 || ',"xEcCurrencyNum"}')::TEXT[] AS currencyNum_path,
				edq.ex_eb_demande_transporteur_num,
				(select to_jsonb(ec.ec_currency_num) from work.eb_demande ed left outer join work.ec_currency ec on ed.x_ec_currency_invoice = ec.ec_currency_num where ed.eb_demande_num = edq.x_eb_demande) as currency_num,
				arr.object as list_cost_item from work.eb_demande_quote edq, jsonb_array_elements(list_pl_cost_item) 
			with ordinality arr(object, index)
			where arr.object ->> 'xEcCurrencyNum' is null
			)
			
		update work.eb_demande_quote edq2
		set list_pl_cost_item = jsonb_set(list_pl_cost_item, cost_item2.currencyNum_path, cost_item2.currency_num)
		from cost_item2
		where edq2.ex_eb_demande_transporteur_num = cost_item2.ex_eb_demande_transporteur_num;

    END LOOP;
end; $$