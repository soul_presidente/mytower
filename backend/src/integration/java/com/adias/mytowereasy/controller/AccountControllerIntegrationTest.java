package com.adias.mytowereasy.controller;

import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.Enumeration.UserStatus;
import com.adias.mytowereasy.repository.EbUserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Ignore
@RunWith(JUnitParamsRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class AccountControllerIntegrationTest {
	
	@ClassRule
    public static final SpringClassRule SCR = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();
	
	@Autowired
    private MockMvc mockMvc;
	
	@Autowired
	EbUserRepository ebUserRepository;
	
	private ObjectMapper mapper = new ObjectMapper();
	
	String email = "email@adias.fr";
	String resetToken = "reset-token";
	String password = "password";
	EbUser ebUser;
	
	@Before
	public void setUp() {		
		
		ebUser = new EbUser();
		ebUser.setEmail(email);
		ebUser.setNom("name");
		ebUser.setPassword("ttte");
		ebUser.setPrenom("prenom");
		
	}

	@Test
	public void shouldResetPassword() throws Exception {
		
		ebUser.setStatus(UserStatus.ACTIF.getCode());
		ebUserRepository.save(ebUser);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/account/reset-password").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
																	   .content(email);	
				
		mockMvc.perform(requestBuilder);
		assertNotNull(ebUserRepository.findOneByEmail(email).getResetToken());
	}
	
	@Test
	@Parameters
	public void shouldReturnAppropriateCode(Integer code) throws Exception {
		
		ebUser.setStatus(code);
		ebUserRepository.save(ebUser);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/account/reset-password").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
																	   .content(email);	
				
		mockMvc.perform(requestBuilder).andExpect(jsonPath("$", is(code)));
	}
	
	@Test
	public void shouldNotResetPassword() throws Exception {	
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/account/reset-password").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
																	   .content(email);	
				
		mockMvc.perform(requestBuilder).andExpect(jsonPath("$", is(-1)));
	}

	@Test
	public void shouldChangePasswordWithResetToken() throws Exception {
		
		ebUser.setStatus(UserStatus.ACTIF.getCode());
		ebUser.setResetToken(resetToken);		
		ebUserRepository.save(ebUser);
		
		EbUser user = new EbUser();
		user.setResetToken(resetToken);
		user.setPassword(password);		
		String jsonContent = mapper.writeValueAsString(user);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/account/change-password-token").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
																	   .content(jsonContent);	
				
		mockMvc.perform(requestBuilder).andExpect(status().isOk());
		EbUser updated = ebUserRepository.findOneByEmail(email);
		assertThat(updated.getResetToken()).isNull();
		assertEquals(updated.getPassword(), password);
		
	}
	
	@Test
	public void shouldNotChangePasswordWithResetToken() throws Exception {
		
		ebUser.setStatus(UserStatus.ACTIF.getCode());
		ebUserRepository.save(ebUser);
		
		EbUser user = new EbUser();
		user.setResetToken(resetToken);
		user.setPassword(password);		
		String jsonContent = mapper.writeValueAsString(user);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/api/account/change-password-token").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
																	   .content(jsonContent);	
				
		mockMvc.perform(requestBuilder).andExpect(status().isNoContent());
		
	}
	
	public Collection<Integer> parametersForShouldReturnAppropriateCode() {
		
        return Arrays.asList(UserStatus.ACTIF.getCode(), UserStatus.DISACTIVE.getCode(), UserStatus.NON_ACTIF.getCode());
       
    }

}
