package com.adias.mytowereasy.rest;

import com.adias.mytowereasy.model.EbCompagnie;
import com.adias.mytowereasy.model.EbEtablissement;
import com.adias.mytowereasy.model.EbUser;
import com.adias.mytowereasy.model.InscriptionWrapper;
import com.adias.mytowereasy.repository.EbEtablissementRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class InscriptionControllerIntagrationTest {
	
	@Autowired
    private MockMvc mockMvc;
	
	@Autowired
	EbEtablissementRepository ebEtablissementRepository;
	
	EbCompagnie ebCompagnie;
	EbEtablissement ebEtablissement;
	EbUser ebUser;
	InscriptionWrapper inscriptionWrapper;

	HttpHeaders headers = new HttpHeaders();
	
	ObjectMapper mapper = new ObjectMapper();
 
	@Before
	public void setUp() {
		
		ebCompagnie =  new EbCompagnie();
		ebCompagnie.setSiren("siren");
		
		ebEtablissement = new EbEtablissement();
		ebEtablissement.setSiret("siret");
		ebEtablissement.setNom("nom");
		ebEtablissement.setEbCompagnie(ebCompagnie);
		
		ebUser = new EbUser();
		ebUser.setNom("name");
		ebUser.setEmail("email@email.fr");
		ebUser.setPassword("ttte");
		ebUser.setPrenom("prenom");
		ebUser.setEbEtablissement(ebEtablissement);	
		
		inscriptionWrapper = new InscriptionWrapper();
		inscriptionWrapper.setEbUser(ebUser);
		inscriptionWrapper.setCollegueInvitationEmails(Arrays.asList("email"));

		
	}

	@Test
	public void testGetEbEtablissementBySiret() throws Exception {	
		
		ebEtablissementRepository.save(ebEtablissement);		
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/api/inscription/getEtablissement").param("siret", ebEtablissement.getSiret())
				.contentType(MediaType.APPLICATION_JSON_UTF8_VALUE);		
		
		mockMvc.perform(requestBuilder)
			.andExpect(status().isFound())
			.andExpect(jsonPath("$.nom", is(ebEtablissement.getNom())))
			.andExpect(jsonPath("$.siret", is(ebEtablissement.getSiret())));
		
	}
	
	@Test
	public void testSinscrire() throws Exception {
		
	    MockMultipartFile mockFile = new MockMultipartFile("logo", "logo.png", "image/png", getClass().getClassLoader().getResource("logo.png").getFile().getBytes());
		
		String jsonContent = mapper.writeValueAsString(inscriptionWrapper);
		
	    MockMultipartFile jsonFile = new MockMultipartFile("inscriptionWrapper", "", "application/json",jsonContent.getBytes());
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.fileUpload("/api/inscription/")
																.file(jsonFile)
																.file(mockFile)
																;
		
		mockMvc.perform(requestBuilder)
		.andExpect(status().isCreated())
		.andExpect(jsonPath("$.nom", is(ebUser.getNom())));

	}

}
