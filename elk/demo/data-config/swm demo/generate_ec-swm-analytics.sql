DROP TABLE work.ec_swm_analytics;

CREATE TABLE work.ec_swm_analytics
(	
	ec_swm_analytics_num integer,
	shipper_company text,
	carrier_company text,
	month_pickup integer,
	year_pickup integer,
	transport_type text,
	transport_mode text,
	origin_country text,
	destination_country text,
	nb_expeditions integer,
	total_weight numeric(19,2),
	total_cost numeric(19,2),
	pt_order_shipped numeric(19,2),
	pt_order_packed numeric(19,2) ,
	pt_order_picked numeric(19,2),
	pt_order_pending numeric(19,2),
	total_pt_order numeric(19,2),
	pt_pricing_waiting_confirmation numeric(19,2),
	pt_pricing_waiting_quotation numeric(19,2),
	total_pt_pricing numeric(19,2),
	pt_transport_ongoing numeric(19,2),
	pt_transport_waiting_pickup numeric(19,2),
	total_pt_transport numeric(19,2),
	pt_tracking_customs numeric(19,2),
	pt_tracking_arrival numeric(19,2),
	pt_tracking_departure numeric(19,2),
	pt_tracking_pickup numeric(19,2),
	total_pt_tracking numeric(19,2),
	pt_freight_audit_approval numeric(19,2),
	pt_freight_audit_invoice numeric(19,2),
	total_pt_freight_audit numeric(19,2),
	pt_quality_a numeric(19,2),
	pt_quality_b numeric(19,2),
	total_pt_quality numeric(19,2)
)																															  																												 
	
INSERT INTO work.ec_swm_analytics(
	ec_swm_analytics_num,
	shipper_company,carrier_company,month_pickup,year_pickup,--transport_type,
	transport_mode,origin_country,destination_country,nb_expeditions,total_weight,total_cost,
	pt_order_shipped,pt_order_packed,pt_order_picked,pt_order_pending,total_pt_order,
	pt_pricing_waiting_confirmation,pt_pricing_waiting_quotation,total_pt_pricing,
	pt_transport_ongoing,pt_transport_waiting_pickup,total_pt_transport,
	pt_tracking_customs,pt_tracking_arrival,pt_tracking_departure,	pt_tracking_pickup,total_pt_tracking,
	pt_freight_audit_approval,pt_freight_audit_invoice,total_pt_freight_audit,
	pt_quality_a,pt_quality_b,total_pt_quality
	)--33
	--31
SELECT id,'SWM',(array['carrier1','carrier2', 'carrier3','carrier4','carrier5'])[floor(random() * 5 + 1)],(floor(random()*(12-1+1)+1) ),2022,(array['SEA','ROAD', 'INTEGRATOR','AIR','RAIL'])[floor(random() * 5 + 1)],
	'FR', (select code FROM work.ec_country where ec_country_num = (SELECT floor(random()*(200-1+1))+1) ),1,
	(random()*(25000-1+1)+1),
	(random()*(1000-1+1)+1),(random()*(1000-1+1)+1),(random()*(1000-1+1)+1),(random()*(1000-1+1)+1),(random()*(1000-1+1)+1),(random()*(1000-1+1)+1),
	(floor(random()*(10-1+1))+1),(floor(random()*(10-1+1))+1),(floor(random()*(10-1+1))+1),
	(random()*(1000-1+1)+1),floor(random()*(10-1+1)+1),
	(random()*(1000-1+1)+1),(random()*(1000-1+1)+1),(random()*(1000-1+1)+1),(random()*(1000-1+1)+1),(random()*(1000-1+1)+1),
	(random()*(1000-1+1)+1),(random()*(1000-1+1)+1),(random()*(1000-1+1)+1),(random()*(1000-1+1)+1),(random()*(1000-1+1)+1)
	 ,(random()*(1000-1+1)+1),(random()*(1000-1+1)+1)
  FROM generate_series(1, 1000) AS 	id
  
  --maj des totaux
update work.ec_swm_analytics set
	total_pt_order = pt_order_shipped + pt_order_packed + pt_order_picked + pt_order_pending ,
	total_pt_pricing = pt_pricing_waiting_confirmation + pt_pricing_waiting_quotation ,
	total_pt_transport = pt_transport_ongoing + pt_transport_waiting_pickup ,
	total_pt_tracking = pt_tracking_customs + pt_tracking_arrival + pt_tracking_departure +	pt_tracking_pickup ,
	total_pt_freight_audit = pt_freight_audit_approval + pt_freight_audit_invoice ,
	total_pt_quality = pt_quality_a +pt_quality_b,

	total_cost = total_pt_order + total_pt_pricing + total_pt_transport + total_pt_tracking + total_pt_freight_audit + total_pt_quality;
	
	
update work.ec_swm_analytics set
	destination_country ='GD'
	where ec_swm_analytics_num<100 ;

update work.ec_swm_analytics set
	destination_country ='MW'
	where ec_swm_analytics_num  between 100  and 300;

update work.ec_swm_analytics set
	destination_country ='PL'
	where ec_swm_analytics_num  between 300  and 540;

update work.ec_swm_analytics set
	destination_country ='TD'
	where ec_swm_analytics_num  between 540 and 600;

update work.ec_swm_analytics set
	destination_country ='BG'
	where ec_swm_analytics_num > 600;