DROP TABLE work.ec_trend_analytics;

CREATE TABLE work.ec_trend_analytics
(	
	transporteur text,
	nombre_operation integer,
	nombre_incident integer,
	respect_performance  text,
	coordinates  text,
	leg  text,
	saving numeric(19,3),
	cout_aircraft_charges  numeric(19,2),
	cout_fee_surcharge_fuel numeric(19,2),
	cout_fee_awb numeric(19,2),
	cout_fee_security numeric(19,2),
	cout_fee_security_screening  numeric(19,2),
	cout_fee_application numeric(19,2),
	cout_fee_dangerous_good  numeric(19,2),
	cout_fee_dangerous_handling  numeric(19,2),
	cout_fee_customs  numeric(19,2),
	cout_fee_delivery_charges  numeric(19,2),
	cout_fee_inland_fuel  numeric(19,2),
	cout_fee_handling  numeric(19,2),
	cout_fee_all_in_rates  numeric(19,2),
	cout_fee_aog numeric(19,2),
	cout_fee_destination_charges numeric(19,2),
	cout_fee_terminal_handling numeric(19,2),
	cout_fee_airline_express numeric(19,2),
	cout_fee_fumigation numeric(19,2),
	cout_fee_pickup_charges numeric(19,2),
	cout_fee_others numeric(19,2),
	cout_total numeric(19,2),
	cout_structure text
)






INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)
 
VALUES ('SINGAPORE AIRLINES LTD', 264, 60, 'not_respected','4.210484#101.975769', 'Leg1', 5.85, 117, 11.14, 7.24, 0, 14.47, 0, 0, 0, 0, 0,
   0, 12.76, 0, 0, 0, 6.07, 0, 0, 0,13.92, 182.6, 'fee_security' );
   
INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)
VALUES ('CHINA SOUTHERN AIRLINES CO. LTD.', 132, 13, false,'35.861660#104.195396', 'Leg1', 21.8, 218, 0, 0, 0, 3.85, 0, 0, 0, 0, 0,
   0, 13.7, 0, 0, 0, 6.42, 0, 0, 0,14.95, 256.92, 'aircraft_charges' );
   
 INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)  
VALUES ('SINGAPORE AIRLINES LTD', 180, 23, false,'35.907757#127.766922', 'Leg2', 0, 207, 0, 0, 0, 3.85, 0, 0, 0, 0, 0,
   0, 13.7, 0, 0, 0, 6.42, 0, 0, 0,14.95, 245.92, 'aircraft_charges' );
   
  INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure) 
VALUES ('MALAYSIA AIRLINES BERHAD MAB', 235, 5, true,'35.907757#127.766922', 'Leg2', 35.4, 354, 0, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 13.67, 0, 0, 0, 6.45, 0, 0, 0,14.92, 389.04, 'aircraft_charges' );
   
INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)   
VALUES ('MALAYSIA AIRLINES BERHAD MAB', 264, 10, true,'35.907757#127.766922', 'Leg3', 19.25, 385, 0, 0, 0, 3.81, 0, 0, 0, 0, 0,
   0, 13.16, 0, 25.37, 0, 6.34, 2.54, 0, 20.3,15.12, 471.64, 'aircraft_charges' );
   
INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)   
VALUES ('CHINA SOUTHERN AIRLINES CO. LTD.', 235, 33, true,'4.210484#101.975769', 'Leg1', 7.45, 149, 0, 0, 0, 3.81, 0, 0, 0, 0, 0,
   0, 13.16, 0, 25.37, 0, 6.34, 2.54, 0, 20.3,14.35, 234.87, 'aircraft_charges' );
   
 INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)  
VALUES ('AIRASIA X SDN BHD', 264, 10, true,'4.210484#101.975769', 'Leg4', 6.1, 122, 0, 0, 0, 3.88, 0, 0, 0, 0, 0,
   0, 13.56, 0, 0, 0, 6.47, 0, 0, 0,14.79, 160.7, 'aircraft_charges' );
   
 INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)  
VALUES ('AIRASIA X SDN BHD', 235, 10, true,'4.210484#101.975769', 'Leg4', 12.85, 247, 0, 10, 0, 0, 0, 0, 0, 0, 0,
   0, 13.56, 0, 0, 0, 6.47, 0, 0, 0,14.79, 291.82, 'aircraft_charges' );
   
 INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)  
VALUES ('AIRASIA X SDN BHD', 264, 23, false,'4.210484#101.975769', 'Leg4', 29.3, 586, 26.77, 7.64, 0, 15.26, 0, 0, 0, 0, 0,
   0, 13.59, 0, 0, 0, 0, 0, 0, 0,0, 649.26, 'aircraft_charges' );
   
  INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure) 
VALUES ('ROYAL BRUNEI AIRLINES SDN BHD', 235, 5, true,'55.378052#-3.435973', 'Leg4', 7.8, 156, 7.44, 7.44, 0, 14.87, 0, 0, 0, 0, 0,
   0, 13.16, 0, 0, 0, 6.34, 0, 0, 0,14.35, 219.6, 'aircraft_charges' );
   
   INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)
VALUES ('MALAYSIA AIRLINES BERHAD MAB', 264, 10, true,'1.352083#103.819839', 'Leg1', 6, 120, 12.8, 7.62, 0, 15.24, 0, 0, 0, 0, 0,
   0, 13.16, 0, 0, 0, 6.34, 0, 0, 0,14.35, 189.51, 'aircraft_charges' );
   
 INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)  
VALUES ('MALAYSIA AIRLINES BERHAD MAB', 235, 15, true,'1.352083#103.819839', 'Leg1', 6.05, 111, 0, 10, 0, 0, 0, 0, 0, 0, 0,
   0, 13.16, 0, 0, 0, 6.34, 0, 0, 0,14.35, 154.85, 'aircraft_charges' );
   
 INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)  
VALUES ('MALAYSIA AIRLINES BERHAD MAB', 264, 20, true,'1.352083#103.819839', 'Leg5', 8.45, 169, 78.28, 0, 0, 0, 0, 0, 0, 0, 0,
   0, 13.16, 0, 0, 0, 6.34, 0, 0, 0,82.69, 349.47, 'aircraft_charges' );
   
   INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)
VALUES ('ETIHAD AIRWAYS', 235, 5, true,'4.210484#101.975769', 'Leg5', 16.9, 338, 0, 0, 0, 3.68, 0, 0, 0, 0, 0,
   0, 12.72, 0, 0, 0, 6.13, 0, 0, 0,14.62, 375.15, 'fee_security' );
   
   
VALUES ('SINGAPORE AIRLINES LTD', 264, 60, true,'4.210484#101.975769', 'Leg2', 7.2, 144, 0, 0, 0, 3.64, 0, 0, 0, 0, 0,
   0, 12.76, 0, 24.3, 0, 6.07, 2.43, 0, 19.44,13.92, 226.56, 'fee_security_screening' );
   
   INSERT INTO work.ec_trend_analytics(
 transporteur, nombre_operation, nombre_incident, respect_performance, coordinates, leg, saving,
 cout_aircraft_charges, cout_fee_surcharge_fuel, cout_fee_awb, cout_fee_security, cout_fee_security_screening,
 cout_fee_application, cout_fee_dangerous_good, cout_fee_dangerous_handling, cout_fee_customs, cout_fee_delivery_charges,
 cout_fee_inland_fuel, cout_fee_handling, cout_fee_all_in_rates, cout_fee_aog, cout_fee_destination_charges, cout_fee_terminal_handling,
 cout_fee_airline_express, cout_fee_fumigation, cout_fee_pickup_charges, cout_fee_others, cout_total, cout_structure)
VALUES ('SINGAPORE AIRLINES LTD', 235, 30, true,'4.210484#101.975769', 'Leg2', 5.9, 108, 0, 10, 0, 0, 0, 0, 0, 0, 0,
   0, 12.72, 0, 0, 0, 6.13, 0, 0, 0,13.88, 150.73, 'fee_surcharge_fuel' );
   
   



select * from work.ec_trend_analytics